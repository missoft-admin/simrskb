var totalharga, qty, diskonpersen, total_sementara, pdiskon, hasil_diskon, x, i, tuslah, jumlahObat, pi, charCode, rgx, totalnominal;
pi = parseInt;
pf = parseFloat;

function hitungnonracikan(d, e, f, g, h) {
	totalharga = removeCommas($(d).text());
	qty = $(e).val();
	diskonpersen = $(f).val();
	tuslah = removeCommas($(g).val());
	jumlahObat = $(h);
	if (diskonpersen > 0 || diskonpersen !== '') {
		total_sementara = totalharga * qty;
		pdiskon = (total_sementara * diskonpersen) / 100;
		hasil_diskon = total_sementara - pdiskon;
		jumlahObat.val(addCommas(hasil_diskon + pi(tuslah)))
	} else {
		jumlahObat.val((addCommas(pi(totalharga) + pi(tuslah)) * qty));
	}
}

function hitungracikan(j, k, l, m) {
	totalharga = removeCommas($(j).text());
	qty = $(k).val();
	diskonpersen = $(l).val();
	jumlahObat = $(m);
	if (diskonpersen > 0 || diskonpersen !== '') {
		total_sementara = totalharga * qty;
		pdiskon = (total_sementara * diskonpersen) / 100;
		hasil_diskon = total_sementara - pdiskon;
		jumlahObat.val(addCommas(hasil_diskon))
	} else {
		jumlahObat.val(addCommas(totalharga * qty));
	}
}

function hitungracikandetail(n, o, p, q) {
	totalharga = removeCommas($(n).text()); /** qty=$(o).val(); **/
	tuslah = $(o).val();
	ht = pi(totalharga);
	var ioio = $(p).text(addCommas(ht + pi(tuslah)));
	$(q).val(ioio)
}

function hitungtotal(idtbody, idtotal, td, counter = null, txthidettl) {
	totalnominal = 0;
	if (counter == null) {
		if (idtbody !== '' || idtotal !== '' || td !== '') {
			$('tbody#' + idtbody + ' tr').each(function() {
				totalnominal += pf($(this).find('td:eq(' + td + ')').text().replace(/\,/g, ""));
			});
		}
		idtotal.text(addCommas(totalnominal));
	} else {
		for (var xyz = 1; xyz <= counter; xyz++) {
			$('tbody tr#' + idtbody + xyz).each(function() {
				totalnominal += pf($(this).find('td:eq(' + td + ')').text().replace(/\,/g, ""));
			});
		}
		idtotal.text(addCommas(totalnominal));
		txthidettl.val(idtotal.text())
	}
	getTotal()
}

function inMask(a) {
	$(a).inputmask("9x9", {
		"placeholder": "_x_"
	});
}

function hanyaAngka(evt) {
	charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
	return true;
}

function alertDelete(row, idtbody, idtotal, td, jmlTR, btn1, btn2) {
	swal({
		title: '',
		text: "Apakah anda yakin akan menghapus data ini?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya!",
		cancelButtonText: "Batalkan!"
	}).then(function() {
		if (row.remove()) {
			hitungtotal(idtbody, idtotal, td);
			var JAR = $(jmlTR).length;
			if (JAR == 0 || btn1 !== '' || btn2 !== '') {
				$(btn1).show();
				$(btn2).hide();
			}
		}
	});
	return false;
}

function AjaxTabel(a, b, c, d, e, f, g, h, j, k, l, m, Z, Z1) {
	x = c.length;
	$.ajax({
		url: a,
		data: b,
		dataType: "html",
		beforeSend: function() {
			for (i = 0; i < x; i++) {
				c[i].empty();
				c[i].append('<tr><td colspan="' + Z1 + '"><center><img src="../assets/ajax/img/ajax-loader.gif"></center></td></tr>');
			}
		},
		success: function(data) {
			for (i = 0; i < x; i++) {
				c[i].empty();
				c[i].append(data);
			}
			if (Z == 'racikan') {
				return hitungracikan(j, k, l, m);
			} else {
				return hitungnonracikan(d, e, f, g, h);
			}
		}
	});
}

function addCommas(nStr) {
	nStr += '';
	x3 = nStr.split('.');
	x1 = x3[0];
	x2 = x3.length > 1 ? '.' + x3[1] : '';
	rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function removeCommas(str) {
	return (str.replace(/,/g, ''));
}

function getCounter(tr) {
	var id = tr.find('input').attr('id');
	var aId = id.split('_');
	return parseInt(aId[aId.length - 1]);
}

function getTotal() {
	var total_non_racikan = removeCommas($('#manage-fullTotal-nonRacikan').text());
	var total_racikan = removeCommas($('#disp_total_racikan').text());
	var grand_total = parseInt(total_non_racikan) + parseInt(total_racikan);
	$('#disp_total').text(addCommas(grand_total));
	$('#total').val(grand_total);
}

function DataTabel(id, url) {
	$(id).DataTable({
		"oLanguage": {
			"sProcessing": "Sedang memproses...",
			"sLengthMenu": "Tampilkan _MENU_ entri",
			"sZeroRecords": "Tidak ada data pada periode ini",
			"sInfo": "",
			"sInfoEmpty": "",
			"sInfoFiltered": "",
			"sInfoPostFix": "",
			"sSearch": "Cari:",
			"sUrl": "",
			"oPaginate": {
				"sFirst": "Pertama",
				"sPrevious": "Sebelumnya",
				"sNext": "Selanjutnya",
				"sLast": "Terakhir"
			},
		},
		"pageLength": 10,
		"ordering": true,
		"processing": true,
		"serverSide": true,
		"order": [],
		"ajax": {
			"url": url
		},
		"columnDefs": [{
			"targets": [0],
			"visible": false
		}, {
			"targets": [1],
			"orderable": true
		}]
	})
}

function getDataSSP(id, url, rownya = '', filter = null) {
	$(id).dataTable({
		"oLanguage": {
			"sProcessing": "Sedang memproses...",
			"sLengthMenu": "Tampilkan _MENU_ entri",
			"sZeroRecords": "Data masih kosong / Tidak ada data",
			"sInfo": "",
			"sInfoEmpty": "",
			"sInfoFiltered": "",
			"sInfoPostFix": "",
			"sSearch": "Cari:",
			"sUrl": "",
			"oPaginate": {
				"sFirst": "Pertama",
				"sPrevious": "Sebelumnya",
				"sNext": "Selanjutnya",
				"sLast": "Terakhir"
			},
		},
		"processing": true,
		"serverSide": true,
		"ajax": {
			"data": {
				"filter": filter
			},
			"url": url
		},
		// "ajax": "./"+url,
		"fnCreatedRow": function(row, data, index) {
			$('td', row).eq(0).html(index + 1);
		},
		"footerCallback": function(row, data, start, end, display) {
			api = this.api(), data;

			// Remove the formatting to get integer data for summation
			intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
					typeof i === 'number' ?
					i : 0;
			};

			// Total over all pages
			total = api
				.column(rownya)
				.data()
				.reduce(function(a, b) {
					return intVal(a) + intVal(b);
				}, 0);

			// Total over this page
			pageTotal = api
				.column(rownya, {
					page: 'current'
				})
				.data()
				.reduce(function(a, b) {
					return intVal(a) + intVal(b);
				}, 0);

			// Update footer
			$(api.column(rownya).footer()).html('Rp. ' + addCommas(pageTotal) + ' (Rp. ' + addCommas(total) + ' total)');
		}
	});
}

function Toastr(msg, title) {
	toastr.options.timeOut = 4000;
	toastr.options.showDuration = 500;
	toastr.options.closeButton = true;
	toastr.options.showMethod = "slideDown";
	toastr.options.positionClass = "toast-top-right";
	toastr.warning(msg, title).css("width", "400px");
}

function ToastrSukses(msg, title) {
	toastr.options.timeOut = 5000;
	toastr.options.showDuration = 500;
	toastr.options.closeButton = true;
	toastr.options.showMethod = "slideDown";
	toastr.options.positionClass = "toast-top-right";
	toastr.success(msg, title).css("width", "400px");
}

function cloneTabel() {

	var $tr = $('#row_racikan_detail_clone #tr_racikan').clone();
	var $tr2 = $('#row_racikan_detail_clone #tr_racikan_obat').clone();

	var counter = parseInt($("#counter_racikan").val());
	counter++;

	$("#counter_racikan").val(counter);

	$tr.attr('id', 'tr_racikan_' + counter);

	$tr.find('#clone_racikan_detail_id')
		.attr('id', 'racikan_detail_id_' + counter)
		.attr('name', 'racikan_detail_id[]')
		.attr('value', counter);

	$tr.find('#clone_racikan')
		.attr('id', 'racikan_' + counter)
		.attr('name', 'racikan[]')
		.attr('value', 1);

	$tr.find('#clone_tmp_counter_racikan')
		.attr('id', 'tmp_counter_racikan_' + counter)
		.attr('name', 'tmp_counter_racikan[]')
		.attr('value', counter);

	$tr.find('#clone_div_racikan_obat')
		.attr('id', 'div_racikan_obat_' + counter);

	$tr.find('#clone_nama_racikan')
		.attr('id', 'nama_racikan_' + counter)
		.attr('name', 'nama_racikan[]');

	$tr.find('#clone_jenis_racikan')
		.attr('id', 'jenis_racikan_' + counter)
		.attr('name', 'jenis_racikan[]');

	$tr.find('#clone_jenis_racikan_lainnya')
		.attr('id', 'jenis_racikan_lainnya_' + counter)
		.attr('name', 'jenis_racikan_lainnya[]');

	$tr.find('#clone_cara_pakai_racikan')
		.attr('id', 'cara_pakai_racikan_' + counter)
		.attr('name', 'cara_pakai_racikan[]');

	$tr.find('#clone_disp_quantity_racikan')
		.attr('id', 'disp_quantity_racikan_' + counter);

	$tr.find('#clone_quantity_racikan')
		.attr('id', 'quantity_racikan_' + counter)
		.attr('name', 'quantity_racikan[]');

	$tr.find('#clone_label_harga_racikan')
		.attr('id', 'label_harga_racikan_' + counter);

	$tr.find('#clone_harga_racikan')
		.attr('id', 'harga_racikan_' + counter)
		.attr('name', 'harga_racikan[]');

	$tr.find('#clone_disp_racikan_tuslah_r')
		.attr('id', 'disp_racikan_tuslah_r_' + counter)
		.attr('value', 1500);

	$tr.find('#clone_racikan_tuslah_r')
		.attr('id', 'racikan_tuslah_r_' + counter)
		.attr('name', 'racikan_tuslah_r[]')
		.attr('value', 1500);

	$tr.find('#clone_label_racikan_jumlah')
		.attr('id', 'label_racikan_jumlah_' + counter);
	// .autoNumeric('init', {aSep: '.', aDec:',', aPad: false});

	$tr.find('#clone_racikan_jumlah')
		.attr('id', 'racikan_jumlah_' + counter)
		.attr('name', 'racikan_jumlah[]');

	$tr.find('#clone_list_obat')
		.attr('id', 'racikan-list_obat_' + counter)
		.attr('name', 'racikan-list_obat[]')
		.attr('value', 0);

	$("#table_racikan_detail").find("#tbody_racikan").append($tr);

	$tr2.attr('id', 'tr_racikan_obat_' + counter);

	$("#table_racikan_detail").find("#tbody_racikan").append($tr2);

	$('#nama_racikan_' + counter).focus();

	// $('#close').hide();

	return false;
}

function cloningtabelRacikan() {
	var $tr = $('#row_racikan_detail_clone #tr_racikan').clone();
	var $tr2 = $('#row_racikan_detail_clone #tr_racikan_obat').clone();

	var counter = parseInt($("#counter_racikan").val());
	counter++;

	$("#counter_racikan").val(counter);

	$tr.attr('id', 'tr_racikan_' + counter);

	$tr.find('#clone_racikan_detail_id')
		.attr('id', 'racikan_detail_id_' + counter)
		.attr('name', 'racikan_detail_id[]')
		.attr('value', counter);

	$tr.find('#clone_idracikan')
		.attr('id', 'hide-idracikan_' + counter)
		.attr('name', 'hide-idracikan[]')

	$tr.find('#clone_racikan')
		.attr('id', 'racikan_' + counter)
		.attr('name', 'racikan[]')
		.attr('value', 1);

	$tr.find('#clone_tmp_counter_racikan')
		.attr('id', 'tmp_counter_racikan_' + counter)
		.attr('name', 'tmp_counter_racikan[]')
		.attr('value', counter);

	$tr.find('#clone_div_racikan_obat')
		.attr('id', 'div_racikan_obat_' + counter);

	$tr.find('#clone_nama_racikan')
		.attr('id', 'nama_racikan_' + counter)
		.attr('name', 'nama_racikan[]');

	$tr.find('#clone_span_jenis_racikan')
		.attr('id', 'span_jenis_racikan_' + counter)
	// .attr('name', 'jenis_racikan[]');

	$tr.find('#clone_jenis_racikan')
		.attr('id', 'jenis_racikan_' + counter)
		.attr('name', 'jenis_racikan[]')
		.attr('type', 'hidden');

	$tr.find('#clone_jenis_racikan_lainnya')
		.attr('id', 'jenis_racikan_lainnya_' + counter)
		.attr('name', 'jenis_racikan_lainnya[]');

	$tr.find('#clone_span_cara_pakai_racikan')
		.attr('id', 'span_cara_pakai_racikan_' + counter)
	// .attr('name', 'span_cara_pakai_racikan[]');

	$tr.find('#clone_disp_quantity_racikan')
		.attr('id', 'disp_quantity_racikan_' + counter);

	$tr.find('#clone_span_harga_racikan')
		.attr('id', 'span_harga_racikan_' + counter);

	$tr.find('#clone_disp_racikan_jmlharga')
		.attr('id', 'disp_racikan_jmlharga_' + counter);

	$tr.find('#clone_span_sisa_obat')
		.attr('id', 'span_sisa_obat_' + counter);
	// .autoNumeric('init', {aSep: '.', aDec:',', aPad: false});

	$tr.find('#clone_disp_qty_retur')
		.attr('id', 'disp_qty_retur_' + counter)
		.attr('name', 'qty_retur[]')
		.attr('value', 0);

	$tr.find('#clone_disp_subtotal_racikan')
		.attr('id', 'disp_subtotal_racikan' + counter)
		.attr('name', 'disp_subtotal_racikan[]');

	$tr.find('#clone_hide-subtotal-racik')
		.attr('id', 'hide-subtotal-racik_' + counter)
		.attr('name', 'hide-subtotal-racik[]');

	$tr.find('#clone_list_obat')
		.attr('id', 'racikan-list_obat_' + counter)
		.attr('name', 'racikan-list_obat[]')
		.attr('value', 0);

	$("#table_racikan_detail").find("#tbody_racikan").append($tr);

	$tr2.attr('id', 'tr_racikan_obat_' + counter);

	$("#table_racikan_detail").find("#tbody_racikan").append($tr2);

	// $('#close').hide();

	return false;
}
