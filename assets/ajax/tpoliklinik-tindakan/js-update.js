        var An=0;
// Start Fungsi Edit
    // Fungsi Readonly
    function addReadonly(){
        An=0;
        $('#input-edit tr td input').attr('readonly','readonly');
        $('#input-edit tr td textarea').attr('readonly','readonly');
        $('#input-edit tr td select').attr('disabled','disabled');
        $('#Ta-Ti').hide();
        $('#Ta-Ob').hide();
        $('#Ta-Al').hide();
        $('tr td#Re-Ak-1').empty();
        $('tr td#Re-Ak-2').empty();
        $('tr td#Re-Ak-3').empty();
        $('tr td#Re-Ak-1').append('-');
        $('tr td#Re-Ak-2').append('-');
        $('tr td#Re-Ak-3').append('-');
    }
    // Fungsi Remove Readonly
    function remReadonly(){
        An=1;
        $('#input-edit tr td input').removeAttr('readonly');
        $('#input-edit tr td textarea').removeAttr('readonly');
        $('#input-edit tr td select').removeAttr('disabled');
        $('#Ta-Ti').show();
        $('#Ta-Ob').show();
        $('#Ta-Al').show();
        $('tr td#Re-Ak-1').empty();
        $('tr td#Re-Ak-2').empty();
        $('tr td#Re-Ak-3').empty();
        $('tr td#Re-Ak-1').append('<a href="javascript:void(0)" id="hapusdet_tindakan" ><i class="fa fa-trash-o"></i></a>');
        $('tr td#Re-Ak-2').append('<a href="javascript:void(0)" id="hapusdet-obat" ><i class="fa fa-trash-o"></i></a>');
        $('tr td#Re-Ak-3').append('<a href="javascript:void(0)" id="hapusdet-alkes" ><i class="fa fa-trash-o"></i></a>');
    }

  $('#btn-EditTindakan').click(function() {
  	$(this).hide();
  	remReadonly();
  	$('#btnBatalTindakan').attr('disabled','disabled')
    $('#btnKembaliTindakan').text('Batal Edit');
    $('#btnSubmitTindakan').show();
  });

    $('#btnKembaliTindakan').click(function(){
        if(An==1){
            addReadonly()
            $('#btnBatalTindakan').removeAttr('disabled')
            $(this).text('Kembali');
            $('#btnSubmitTindakan').hide();
            $('#btn-EditTindakan').show();
        }else
        if(An==0){
           window.location.replace('../../tpoliklinik_tindakan');
       }
    })
	$('#btnBatalTindakan').click(function(){
		var id =$('input#getIDbatal').val();
		swal({
                title: '',
                text: "Apakah anda yakin akan menghapus data ini?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya!",
                cancelButtonText: "Batalkan!"
            }).then(function () {
		$.ajax({
			before:function(){
				$('#loading-button-ajax').show()
			},
			url:"../../tpoliklinik_tindakan/membatalkan",
			type:"post",
			data:{"id":id},
			success:function(data){
				window.location.replace('../../tpoliklinik_tindakan');
				}
		})
            })
            return false;
	})
addReadonly()