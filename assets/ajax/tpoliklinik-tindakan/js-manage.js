/**
 * Tindakan Poliklinik & IGD controller.
 * Developer @Deni Purnama
 * Email: denipurnama371@gmail.com
 */

$(function() {

	// Setup AJAX
	$.ajaxSetup({
		type: "post",
		cache: false,
		dataType: "json"
	});
	// End Setup AJAX
	$(document).ready(function() {
		// select option tindak_lanjut
		$(document).on("change", "select[id~='tindak_lanjut']", function() {
			if ($('select#tindak_lanjut option:selected').val() == 4) {
				$('select#menolak_dirawat option').val(0);
				$('tr#men_di-Rawat').show()
			} else {
				$('tr#men_di-Rawat').hide();
			}
		});
		// End select option tindak_lanjut

		// Fungsi onFocus
		$(document).on("focus", "tr td input", function() {
			$(this).select();
		});
		$(document).on("focus", "tr td textarea", function() {
			$(this).select();
		});
		$('textarea').keyup(function() {
			$(this).val($(this).val().toUpperCase());
		});
		// Fungsi alert Delete
		function alertDelete(row) {
			// var row = $(this).closest('td').parent();
			swal({
				title: '',
				text: "Apakah anda yakin akan menghapus data ini?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya!",
				cancelButtonText: "Batalkan!"
			}).then(function() {
				return row.remove();
			})
			return false;
		}

		// Fungsi Hitung
		// hitung Pelayanan
		function hitungpelayanan() {
			var totalharga = removeCommas($("span[id~='tot_sem']").text());
			var qty = $("input[id~='quantityTindakan']").val();
			var diskonpersen = $("input[id~='diskonTindakan']").val();
			if (diskonpersen > 0 || diskonpersen !== '') {
				total_sementara = totalharga * qty;
				pdiskon = (total_sementara * diskonpersen) / 100;
				hasil_diskon = total_sementara - pdiskon;
				$("input[id~='jumlahTindakan']").val(addCommas(hasil_diskon))
			} else {
				var hasilnya = totalharga * qty;
				$("input[id~='jumlahTindakan']").val(addCommas(hasilnya));
			}
		}

		// hitung obat
		function hitungobat() {
			var totalharga = removeCommas($("span[id~='harga-beli-obat']").text());
			var qty = $("input[id~='quantity-obat']").val();
			// Hitung data
			$("input[id~='jumlah-obat']").val(addCommas(totalharga * qty));
		}
		// hitung alkes
		function hitungalkes() {
			var totalharga = removeCommas($("span[id~='harga-beli-alkes']").text());
			var qty = $("input[id~='quantity-alkes']").val();
			// Hitung data
			$("input[id~='jumlah-alkes']").val(addCommas(totalharga * qty));
		}
		// End Fungsi Hitung

		// Fungsi detail
		// menghitung jumlah dan diskon detailtindakan
		$(document).on("keyup", "tr#det_view_tin td input", function() {
			hitungpelayanan();
		});
		// Detail Obat
		$(document).on("keyup", "tr#tr-det-obat td input", function() {
			hitungobat();
		})
		// Detail Alkes
		$(document).on("keyup", "tr#tr-det-alkes td input", function() {
			hitungalkes();
		})
		// End Fungsi Detail

		// Fungsi CRUD Sementara dari Modal dialog
		// Simpan Detail Tindakan
		$('#btnSubmitDet_tindakan').click(function() {
			var duplicate = false;
			var disk = $("tr#det_view_tin td input[id~='diskonTindakan']").val();
			if (disk == '') {
				disk = 0;
			}
			// Mengambil data dari tabel modal tindakan
			var td0 = $("tr#det_view_tin td span[id~='nama_tindakannya']").text();
			var td1 = $("tr#det_view_tin td span[id~='tot_sem']").text();
			var td2 = $("tr#det_view_tin td input[id~='quantityTindakan']").val();
			var td3 = disk;
			var td4 = $("tr#det_view_tin td input[id~='jumlahTindakan']").val();
			var td5 = $("tr#det_view_tin td span[id~='ambiljasaSarana']").text();
			var td6 = $("tr#det_view_tin td span[id~='ambiljasapelayanan']").text();
			var td7 = $("tr#det_view_tin td span[id~='ambilBHP']").text();
			var td8 = $("tr#det_view_tin td span[id~='ambiljasaperawatan']").text();
			var td9 = $("tr#det_view_tin td span[id~='ambilID']").text();
			var det_td = $('table#table_tindakan tbody#view_detail-tindakan');
			// menampilkan data yg diinput dari modal tindakan
			var content = "<tr>";
			$('#table_tindakan tbody#view_detail-tindakan tr').filter(function() {
				var $cells = $(this).children('td');
				if ($cells.eq(0).text() === td0) {
					toastr.options.timeOut = 4000;
					toastr.options.showDuration = 500;
					toastr.options.closeButton = true;
					toastr.options.showMethod = "slideDown";
					toastr.options.positionClass = "toast-top-center";
					toastr.error('Tindakan ' + td0 + ' sudah ditambahkan.', 'Maaf...').css("width", "600px");
					duplicate = true;
					return false;
				}
			});
			// $('#detail_list tbody tr').filter(function() {
			//         var $cells = $(this).children('td');
			//     });
			if (duplicate == false) {
				content += '<td>' + td0 + '</td>';
				content += '<td>' + td1 + '</td>';
				content += '<td>' + td2 + '</td>';
				content += '<td>' + td3 + '</td>';
				content += '<td>' + td4 + '</td>';
				content += '<td><a href="javascript:void(0)" id="hapusdet_tindakan" data-toggle="tooltip" title="" data-original-title="Hapus"><i class="fa fa-trash-o"></i></a></td>';
				content += '<td style="display:none">' + td5 + '</td>';
				content += '<td style="display:none">' + td6 + '</td>';
				content += '<td style="display:none">' + td7 + '</td>';
				content += '<td style="display:none">' + td8 + '</td>';
				content += '<td style="display:none">' + td9 + '</td>';
				det_td.append(content);
			}
		})

		// tombol hapusdet_tindakan
		$(document).on('click', '#hapusdet_tindakan', function() {
			var row = $(this).closest('td').parent();
			alertDelete(row);
		})

		// Simpan Detail Obat
		$('#btnSubmitDet_obat').click(function() {
			var duplicate = false;
			// Mengambil data dari tabel modal obat
			var td0 = $("tr#tr-det-obat td span[id~='nama-obat']").text();
			var td1 = $("tr#tr-det-obat td span[id~='singkatan-obat']").text();
			var td2 = $("tr#tr-det-obat td input[id~='quantity-obat']").val();
			var td3 = $("tr#tr-det-obat td span[id~='harga-beli-obat']").text();
			var td4 = $("tr#tr-det-obat td input[id~='jumlah-obat']").val();
			var td5 = $("tr#tr-det-obat td span[id~='id-obat']").text();
			var td6 = $("#select-obat option:selected").val();
			var det_td = $('table#table-obat tbody#view_detail-obat');
			// menampilkan data yg diinput dari modal obat
			var content = "<tr>";
			$('#table-obat tbody#view_detail-obat tr').filter(function() {
				var $cells = $(this).children('td');
				if ($cells.eq(0).text() === td0) {
					// sweetAlert("Maaf...", "Tindakan " + $("tr#det_view_tin td span[id~='nama_tindakannya']").text() + " sudah ditambahkan.", "error");
					toastr.options.timeOut = 4000;
					toastr.options.showDuration = 500;
					toastr.options.closeButton = true;
					toastr.options.showMethod = "slideDown";
					toastr.options.positionClass = "toast-top-center";
					toastr.error(td0 + ' sudah ditambahkan.', 'Maaf...').css("width", "600px");
					duplicate = true;
					return false;
				}
			});
			if (duplicate == false) {
				content += '<td>' + td0 + '</td>';
				content += '<td>' + td1 + '</td>';
				content += '<td>' + td2 + '</td>';
				content += '<td style="display:none">' + td3 + '</td>';
				content += '<td style="display:none">' + td4 + '</td>';
				content += '<td><a href="javascript:void(0)" id="hapusdet-obat" data-toggle="tooltip" title="" data-original-title="Hapus"><i class="fa fa-trash-o"></i></a></td>';
				content += '<td style="display:none">' + td5 + '</td>';
				content += '<td style="display:none">' + td6 + '</td>';
				det_td.append(content);
			}
		})

		// tombol hapusdet-obat
		$(document).on('click', '#hapusdet-obat', function() {
			var row = $(this).closest('td').parent();
			alertDelete(row);
		})

		// Simpan Detail Alkes
		$('#btnSubmitDet_alkes').click(function() {
			var duplicate = false;
			// Mengambil data dari tabel modal alkes
			var td0 = $("tr#tr-det-alkes td span[id~='nama-alkes']").text();
			var td1 = $("tr#tr-det-alkes td span[id~='harga-beli-alkes']").text();
			var td2 = $("tr#tr-det-alkes td span[id~='singkatan-alkes']").text();
			var td3 = $("tr#tr-det-alkes td input[id~='quantity-alkes']").val();
			var td4 = $("tr#tr-det-alkes td input[id~='jumlah-alkes']").val();
			var td5 = $("tr#tr-det-alkes td span[id~='id-alkes']").text();
			var td6 = $("#select-alkes option:selected").val();
			var det_td = $('table#table-alkes tbody#view_detail-alkes');
			// menampilkan data yg diinput dari modal alkes
			var content = "<tr>";
			$('#table-alkes tbody#view_detail-alkes tr').filter(function() {
				var $cells = $(this).children('td');
				if ($cells.eq(0).text() === td0) {
					// sweetAlert("Maaf...", "Tindakan " + $("tr#det_view_tin td span[id~='nama_tindakannya']").text() + " sudah ditambahkan.", "error");
					toastr.options.timeOut = 4000;
					toastr.options.showDuration = 500;
					toastr.options.closeButton = true;
					toastr.options.showMethod = "slideDown";
					toastr.options.positionClass = "toast-top-center";
					toastr.error(td0 + ' sudah ditambahkan.', 'Maaf...').css("width", "600px");
					duplicate = true;
					return false;
				}
			});
			if (duplicate == false) {
				content += '<td>' + td0 + '</td>';
				content += '<td>' + td1 + '</td>';
				content += '<td>' + td2 + '</td>';
				content += '<td>' + td3 + '</td>';
				content += '<td>' + td4 + '</td>';
				content += '<td><a href="javascript:void(0)" id="hapusdet-alkes" data-toggle="tooltip" title="" data-original-title="Hapus"><i class="fa fa-trash-o"></i></a></td>';
				content += '<td style="display:none">' + td5 + '</td>';
				content += '<td style="display:none">' + td6 + '</td>';
				det_td.append(content);
			}
		})

		// tombol hapusdet-alkes
		$(document).on('click', '#hapusdet-alkes', function() {
			var row = $(this).closest('td').parent();
			alertDelete(row);
		})

		// Tombol Tambah tindakan
		$('#tambah_tindakan').click(function() {
			$('#tindakan_modal').modal('show');
			$('#btnSubmitDet_tindakan').attr('disabled', 'disabled');
			$('#tabeldepanpelayanan').fadeIn('slow');
			$('#dt').hide();
		})

		// Tombol Tambah Obat
		$('#tambah_obat').click(function() {
			$('#obat_modal').modal('show');
			$('#btnSubmitDet_obat').attr('disabled', 'disabled');
			$('#tabeldepanobat').fadeIn('slow');
			$('#tabel-det-obat').hide();
			$("#detail-obat").empty();
			$('#detail-obat').append('<tr><td colspan="3"><center><i>belum memilih data</i></center></td></tr>');
			selectOptionObat();
		})

		// Tombol Tambah Alkes
		$('#tambah_alkes').click(function() {
			$('#Alkes_modal').modal('show');
			$('#tabel-det-alkes').fadeOut();
			$("input[id~='quantityAlkes']").val(0);
			$('#btnSubmitDet_alkes').attr('disabled', 'disabled');
			$('#tabeldepanalkes').fadeIn('slow');
			$("#detail-alkes").empty();
			$('#detail-alkes').append('<tr><td colspan="3"><center><i>belum memilih data</i></center></td></tr>');
			selectOptionAlkes();
		})

		// Tombol Kembali
		$('#back').click(function() {
			$('#dt').fadeOut();
			$("input[id~='quantityTindakan']").val('');
			$("input[id~='diskonTindakan']").val('');
			$('#btnSubmitDet_tindakan').attr('disabled', 'disabled');
			$('#tabeldepanpelayanan').fadeIn('slow');
		})
		$('#back-obat').click(function() {
			$('#tabel-det-obat').fadeOut();
			$("input[id~='quantityObat']").val(0);
			$('#btnSubmitDet_obat').attr('disabled', 'disabled');
			$('#tabeldepanobat').fadeIn('slow');
		})
		$('#back-alkes').click(function() {
			$('#tabel-det-alkes').fadeOut();
			$("input[id~='quantityAlkes']").val(0);
			$('#btnSubmitDet_alkes').attr('disabled', 'disabled');
			$('#tabeldepanalkes').fadeIn('slow');
		})

		// Table Raw td Pelayanan OnClick
		$(document).on("click", "tr#idnyatindakan td#tin", function() {
			var L = $(this).attr('data-id');
			$('#tabeldepanpelayanan').fadeOut();
			$('#btnSubmitDet_tindakan').removeAttr('disabled');
			$('#dt').fadeIn('slow');
			$('#detailtindakan').empty();
			$('#detailtindakan').append('<tr><td colspan="5"><center><img src="../../assets/ajax/img/ajax-loader.gif"></center></td></tr>');
			$.ajax({
				url: "../tindakan_detail",
				data: {
					"id": L
				},
				dataType: "html",
				success: function(data) {
					$("#detailtindakan").empty();
					$('#detailtindakan').append(data);
					hitungpelayanan();
				},
				error: function(error) {
					$("#detailtindakan").empty();
					$('#detailtindakan').append(error);
				}
			})
		});

		// Fungsi getdata selectOption
		function selectOptionObat() {
			$.ajax({
				url: "../selectviewUnitPelayanan",
				dataType: "text",
				success: function(data) {
					$("#select-obat").empty();
					$('#select-obat').append('<option>-- Pilih --</option>');
					$('#select-obat').append(data)
					// $('#loading-search').hide(5000);
				}
			});
		}

		function selectOptionAlkes() {
			$.ajax({
				url: "../selectviewUnitPelayanan",
				dataType: "text",
				success: function(data) {
					// $('#loading-search-alkes').show();

					$('#select-alkes').append('<option>Loading ...</option>');
					$("#select-alkes").empty();
					$('#select-alkes').append('<option>-- Pilih --</option>');
					$('#select-alkes').append(data)
					// $('#loading-search-alkes').hide(5000);
				}
			});
		}
		// End getdata selectOption

		// Start Fungsi Onchange selectOption
		// selectOption Obat
		$("#select-obat").change(function() {
			$('#btnSubmitDet_obat').attr('disabled', 'disabled');
			$('#tabeldepanobat').fadeIn('slow');
			$('#tabel-det-obat').hide();
			$('#detail-obat').empty();
			$('#detail-obat').append('<tr><td colspan="5"><center><img src="../../assets/ajax/img/ajax-loader.gif"></center></td></tr>');
			var idunit = $("#select-obat option:selected").val();
			$.ajax({
				url: "../getDetailSelectObat",
				data: {
					"idunit": idunit
				},
				dataType: "html",
				success: function(data) {
					$("#detail-obat").empty();
					$('#detail-obat').append(data);
				}
			})
		})

		// Fungsi Tabel Raw td Obat OnClick
		$(document).on("click", "tr#det-select-obat td#obt", function() {
			var O = $(this).attr('data-id');
			var idunit = $('#select-obat option:selected').val();
			$('#tabeldepanobat').fadeOut();
			$('#btnSubmitDet_obat').removeAttr('disabled');
			$('#tabel-det-obat').fadeIn('slow');
			$('#input-detail-obat').empty();
			$('#input-detail-obat').append('<tr><td colspan="3">' +
				'<center><img src="../../assets/ajax/img/ajax-loader.gif">' +
				'</center></td></tr>');
			$.ajax({
				url: "../getDetailSelectObat",
				data: {
					"id": O,
					"idunit": idunit
				},
				dataType: "html",
				success: function(data) {
					$("#input-detail-obat").empty();
					$('#input-detail-obat').append(data);
					hitungobat();
				}
			})
		});

		// selectOption Alkes
		$("#select-alkes").change(function() {
			$('#btnSubmitDet_alkes').attr('disabled', 'disabled');
			$('#tabeldepanalkes').fadeIn('slow');
			$('#tabel-det-alkes').hide();
			$('#detail-alkes').empty();
			$('#detail-alkes').append('<tr><td colspan="3"><center><img src="../../assets/ajax/img/ajax-loader.gif"></center></td></tr>');
			var id = $("#select-alkes option:selected").val();
			$.ajax({
				url: "../getDetailSelectAlkes",
				data: {
					"idunit": id
				},
				dataType: "html",
				success: function(data) {
					$("#detail-alkes").empty();
					$('#detail-alkes').append(data);
				}
			})
		})

		// Fungsi Tabel Raw td Alkes OnClick
		$(document).on("click", "tr#det-select-alkes td#alkes", function() {
			var A = $(this).attr('data-id');
			var idunit = $('#select-alkes option:selected').val();
			$('#tabeldepanalkes').fadeOut();
			$('#btnSubmitDet_alkes').removeAttr('disabled');
			$('#tabel-det-alkes').fadeIn('slow');
			$('#input-detail-alkes').empty();
			$('#input-detail-alkes').append('<tr><td colspan="5">' +
				'<center><img src="../../assets/ajax/img/ajax-loader.gif">' +
				'</center></td></tr>');
			$.ajax({
				url: "../getDetailSelectAlkes",
				data: {
					"id": A,
					"idunit": idunit
				},
				dataType: "html",
				success: function(data) {
					$("#input-detail-alkes").empty();
					$('#input-detail-alkes').append(data);
					hitungalkes();
				}
			})
		});

		// Aksi insert tindakan
		// Submit to database
		document.querySelector('#form1').addEventListener('submit', function(e) {
			// CSS visual button
			// $('#btnSubmitTindakan').attr('disabled','disabled');
			$('#loading-button-ajax').show();
			// Convert to JSON detail tindakan
			var detail_pelayanan = $('table#table_tindakan tbody#view_detail-tindakan tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					return $(cell).html();
				});
			});
			// Convert to JSON detail obat
			var detail_obat = $('table#table-obat tbody#view_detail-obat tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					return $(cell).html();
				});
			});
			// Convert to JSON detail alkes
			var detail_alkes = $('table#table-alkes tbody#view_detail-alkes tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					return $(cell).html();
				});
			});
			// Hasil Convert tindakan
			$("#detail_pelayanan").val(JSON.stringify(detail_pelayanan));
			// Hasil Convert obat
			$("#detail_obat").val(JSON.stringify(detail_obat));
			// Hasil Convert alkes
			$("#detail_alkes").val(JSON.stringify(detail_alkes));
		});

	});
});
