$(function(){
var O,c,dataAjax,dataAjax2,Z,duplicate,td0,td1,td2,td3,td4,td5,td6,td7,td8,td9,td10,td11,td12,td13,td14,td15,det_td,content,jumlahTR;

// variabel untuk non Racikan
d="span[id~='harga-beli-obat']";e="input[id~='quantity-obat']";f="input[id~='diskon-obat']";g="input[id~='tuslah-obat']";h="input[id~='jumlah-obat']";
// variabel untuk Racikan
j="span[id~='det-harga-beli-racikan']";k="input[id~='det-quantity-racikan']";l="input[id~='det-diskon-racikan']";m="input[id~='det-jumlah-racikan']";

// Setup AJAX
$.ajaxSetup({
type:"post",
cache:false,
dataType: "json"
});


	$(document).ready(function(){
		$('#simpan_nonracikan_obat').click(function(){
			var copydata=$('table#manage-tabel-list-nonRacikan').find('tbody').html()
			$('table#manage-tabel-nonRacikan').find('tbody').empty()
			$('table#manage-tabel-nonRacikan').find('tbody').append(copydata)
				var totalFull=$('#manage-fullTotal-nonRacikan');
				hitungtotal('manage-tbody-nonRacikan',totalFull,7);
			
		})
				// fungsi tombol simpan modal Racikan
		$('#btnSubmit-modal-penjualan2').click(function(){
				duplicate=false;
				// Mengambil data dari tabel modal racikan
				td0=$("tr#tr-det-racikan td span[id~='det-nama-racikan']").text();
				td1=$("tr#tr-det-racikan td span[id~='det-harga-beli-racikan']").text();
				td2=$("tr#tr-det-racikan td span[id~='det-singkatan-racikan']").text();
				td3=$("tr#tr-det-racikan td input[id~='det-quantity-racikan']").val();
				td4=$("tr#tr-det-racikan td input[id~='det-diskon-racikan']").val();
				td5=$("tr#tr-det-racikan td input[id~='det-jumlah-racikan']").val();
				td6=$("tr#tr-det-racikan td span[id~='id-racikan']").text();
				det_td=$('table#manage-tabel-list-Racikan tbody#manage-tbody-list-Racikan');
				// det_td2=$('#manage-tabel-list-sementara-Racikan tbody#manage-tbody-list-sementara-Racikan_'+KL);
				// menampilkan data yg diinput dari modal racikan
				content = "<tr class='tr_list_obat'>";
				$('table#manage-tabel-list-Racikan tbody#manage-tbody-list-Racikan tr').filter(function (){
						var $cells = $(this).children('td');
						if ($cells.eq(0).text() === td0) {
														toastr.options.timeOut = 4000;
														toastr.options.showDuration = 500;
														toastr.options.closeButton = true;
														toastr.options.showMethod = "slideDown";
														toastr.options.positionClass = "toast-top-right";
														toastr.error(td0 +' sudah ditambahkan.','Maaf...').css("width","400px");
										duplicate = true;
										return false;
								}
				});
				if(duplicate==false){
				content += '<td>'+td0+'<input type="hidden" value="'+td0+'" name="det-racikan-namaobat[]"></td>';
				content += '<td>'+td1+'<input type="hidden" value="'+td1+'" name="det-racikan-harga[]"></td>';
				content += '<td>'+td2+'<input type="hidden" value="'+td2+'" name="det-racikan-satuan[]"></td>';
				content += '<td>'+td3+'<input type="hidden" value="'+td3+'" name="det-racikan-qty[]"></td>';
				content += '<td>'+td4+'<input type="hidden" value="'+td4+'" name="det-racikan-diskon[]"></td>';
				content += '<td>'+td5+'<input type="hidden" value="'+td5+'" name="det-racikan-jumlah[]"></td>';
				content += '<td><a href="javascript:void(0)" id="manage-hapusdet-Racikan" data-toggle="tooltip" title="" data-original-title="Hapus"><i class="fa fa-trash-o"></i></a></td>';
				content += '<td style="display:none">'+td6+'<input type="hidden" value="'+td6+'" name="det-racikan-idobat[]"></td>';
				content += '</tr>';
				det_td.append(content);
				// det_td2.append(content);
				var totalFull=$('#manage-Totalsem-Racikan');
				hitungtotal('manage-tbody-list-Racikan',totalFull,5);

		}
		})

// fungsi tombol open modal Non-Racikan 
		$('#manage-tambah-nonRacikan').click(function(){
			$('#manage-modal-list-nonRacikan').modal('show')
		})

		$('#manage-tambah-list-obat2').click(function(){
			$('#manage-modal-nonRacikan').modal('show')
			$('#manage-tabelbelakang-nonRacikan').hide();
			$('#manage-tabeldepan-nonRacikan').fadeIn('slow')
			$('#btnSubmit-modal-penjualan').attr('disabled','disabled');
		})
		$('#link-back-nonRacikan').click(function(){
			$('#manage-tabeldepan-nonRacikan').fadeIn('slow')
			$('#btnSubmit-modal-penjualan').attr('disabled','disabled');
			$('#manage-tabelbelakang-nonRacikan').hide();
		})
// fungsi Klik Row td Non-Racikan 
		$('table#tabel-daftar-nonRacikan').on("click","#manage-klik-data",function(){
				O       =$(this).attr('data-id');
				c 		=$("#manage-detail-nonRacikan");
				$('#manage-tabeldepan-nonRacikan').hide();
				$('#btnSubmit-modal-penjualan').removeAttr('disabled');
				$('#manage-tabelbelakang-nonRacikan').fadeIn('slow');
				dataAjax = {id:O,tipe:'nonracikan'};
				dataAjax2 =[c];
				Z='nonracikan';
					AjaxTabel('getRacikanObat',dataAjax,dataAjax2,d,e,f,g,h,j,k,l,m,Z,8)
				});
// fungsi tombol open modal Racikan 
		$('#manage-tambah-list-obat').click(function(){
			$('#manage-modal-Racikan').modal('show')
			$('#manage-tabelbelakang-Racikan').hide();
			$('#manage-tabeldepan-Racikan').fadeIn('slow')
			$('#btnSubmit-modal-penjualan2').attr('disabled','disabled');
		})
		$('#link-back-Racikan').click(function(){
			$('#manage-tabeldepan-Racikan').fadeIn('slow')
			$('#btnSubmit-modal-penjualan2').attr('disabled','disabled');
			$('#manage-tabelbelakang-Racikan').hide();
		})

// fungsi Klik Row td Racikan 
		$('table#tabel-daftar-Racikan').on("click","#manage-klik-data",function(){
				O       =$(this).attr('data-id');
				c 		=$("#manage-detail-Racikan");
				$('#manage-tabeldepan-Racikan').hide();
				$('#btnSubmit-modal-penjualan2').removeAttr('disabled');
				$('#manage-tabelbelakang-Racikan').fadeIn('slow');
				dataAjax = {id:O,tipe:'racikan'};
				dataAjax2 =[c];
				Z='racikan';
						AjaxTabel('getRacikanObat',dataAjax,dataAjax2,d,e,f,g,h,j,k,l,m,Z,6)
				});
// Fungsi onFocus
$('table').on("focus","tr td input,textarea",function() { $(this).select();} );
$('textarea, input').keyup(function() {
    $(this).val($(this).val().toUpperCase());
});
$('table').on("keyup","tr#tr-det-obat td input",function(){
		hitungnonracikan(d,e,f,g,h);
})
$('table').on("keyup","tr#tr-det-racikan td input",function(){
		hitungracikan(j,k,l,m);
})

$('#manage-input-notelp-pasien').keypress(function(){
	return hanyaAngka(event);
})
 
		// fungsi tombol simpan modal non Racikan
		$('#btnSubmit-modal-penjualan').click(function(){
				duplicate=false;
				// Mengambil data dari tabel modal obat
				td0=$("tr#tr-det-obat td span[id~='nama-obat']").text();
				td1=$("tr#tr-det-obat td span[id~='harga-beli-obat']").text();
				td2=$("tr#tr-det-obat td span[id~='singkatan-obat']").text();
				td3=$("tr#tr-det-obat td input[id~='cara-pakai-obat']").val();
				td4=$("tr#tr-det-obat td input[id~='quantity-obat']").val();
				td5=$("tr#tr-det-obat td input[id~='diskon-obat']").val();
				td6=$("tr#tr-det-obat td input[id~='tuslah-obat']").val();
				td7=$("tr#tr-det-obat td input[id~='jumlah-obat']").val();
				td8=$("tr#tr-det-obat td span[id~='id-obat']").text();
				det_td=$('table#manage-tabel-list-nonRacikan tbody#manage-tbody-list-nonRacikan');
				// menampilkan data yg diinput dari modal obat
				content = "<tr>";
				$('#manage-tabel-list-nonRacikan tbody#manage-tbody-list-nonRacikan tr').filter(function (){
						var $cells = $(this).children('td');
						if ($cells.eq(0).text() === td0) {
														toastr.options.timeOut = 4000;
														toastr.options.showDuration = 500;
														toastr.options.closeButton = true;
														toastr.options.showMethod = "slideDown";
														toastr.options.positionClass = "toast-top-right";
														toastr.error(td0 +' sudah ditambahkan.','Maaf...').css("width","400px");
										duplicate = true;
										return false;
								}
				});
				if(duplicate==false){
				content += '<td>'+td0+'</td>';
				content += '<td>'+td1+'</td>';
				content += '<td>'+td2+'</td>';
				content += '<td>'+td3+'</td>';
				content += '<td>'+td4+'</td>';
				content += '<td>'+td5+'</td>';
				content += '<td>'+td6+'</td>';
				content += '<td><span id="jum-sem-nr">'+td7+'</span></td>';
				content += '<td><a href="javascript:void(0)" id="manage-hapusdet-nonRacikan" data-toggle="tooltip" title="" data-original-title="Hapus"><i class="fa fa-trash-o"></i></a></td>';
				content += '<td style="display:none">'+td8+'</td>';
				content += '</tr>';
				det_td.append(content);
				var totalFull=$('#manage-Totalsem-nonRacikan');
				hitungtotal('manage-tbody-list-nonRacikan',totalFull,7);
		}
		})

		// tombol hapusdet-obat
		$(document).on('click','#manage-hapusdet-nonRacikan',function(){
                var row = $(this).closest('td').parent();
				var totalFull=$('#manage-fullTotal-nonRacikan');
                alertDelete(row,'manage-tbody-nonRacikan',totalFull,7);
		})

		// tombol hapusdet-racikan
		$(document).on('click','#manage-hapusdet-Racikan',function(){
                 var row = $(this).closest('td').parent();
				var totalFull=$('#manage-Totalsem-Racikan');
                alertDelete(row,'manage-tbody-list-Racikan',totalFull,5);
		})
 
// Aksi insert penjualan obat
// Submit to database
document.querySelector('#form1').addEventListener('submit', function(e) {
		// CSS visual button
		$('#loading-button-ajax').show();
// Convert to JSON detail Non Racikan
		var detail_nonracikan = $('table tbody#manage-tbody-nonRacikan tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
						return $(cell).html();
				});
		});
				$("#json-detail-nonracikan").val(JSON.stringify(detail_nonracikan));

		var qtytotal1=0;
		$('tbody#manage-tbody-nonRacikan tr').each(function() {
			qtytotal1 += parseFloat($(this).find('td:eq(4)').text().replace(/\,/g, ""));
		});
		var qtytotal2=0;
		// var ctr=$('#counter_racikan').val();
		var ctr =$('#tbody_racikan tr.c_tr_racikan').length;
	for(var xxyz=1;xxyz<=ctr;xxyz++){
    $('tbody tr#tr_racikan_'+xxyz).each(function() {
        qtytotal2 += parseFloat($(this).find('.quantity_racikan').val().replace(/\,/g, ""));
    });
    }
		$('#post-totalbarang').val(parseInt(qtytotal1)+parseInt(qtytotal2))
});
	
	})
	})