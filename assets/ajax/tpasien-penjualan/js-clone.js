
		$("#button_tambah_racikan").click(function() {
			cloneTabel();
		});


		$('#table_racikan_detail').on('click', '.button_racikan_batal', function() {
			$tr = $(this).parent().parent();
			var counter = getCounter($tr);

			var jumlah = removeCommas($('#label_racikan_jumlah_' + counter).text());

			$('#tr_racikan_' + counter).remove();
			$('#tr_racikan_obat_' + counter).remove();

			var old_total_racikan = removeCommas($('#disp_total_racikan').text());

			total_racikan = parseInt(old_total_racikan) - parseInt(jumlah);

			$('#disp_total_racikan').text(addCommas(total_racikan));
			$('#total_racikan').val(total_racikan);

			getTotal();

			$('#total').val($('#disp_total_racikan').autoNumeric('set'));
		});


		$('#table_racikan_detail').on('click', '.list_obat_racikan', function() {
			$('#manage-tabel-list-Racikan').find('tbody').html('');
			$tr = $(this).parent().parent();
			var counter = getCounter($tr);

			var racikan_nama = $tr.find('#nama_racikan_' + counter).val();
			$('#list_racikan_modal_nama').val(racikan_nama);
			$('#tmp_counter_racikan').val(counter);

			$tr2 = $('#tr_racikan_obat_' + counter).clone();				
			$tr2.attr('style', '');
			var a = $tr2.children().length;
			if(a) {
				var $tr_obat = $tr2.find('tr');
				$tr_obat.each(function() {
					$('#manage-modal-list-Racikan').find('tbody').append($(this));
				});
			}
				var totalFull=$('#manage-Totalsem-Racikan');
			var $txthidettl =$('#total_racikan');
				hitungtotal('manage-tbody-list-Racikan',totalFull,5);
			$('#manage-modal-list-Racikan').modal('show');

		});

		$('#table_racikan_detail').on('keyup', '.quantity_racikan', function() {
			$tr = $(this).parent().parent();
			var counter = getCounter($tr);
			$('#quantity_racikan_'+counter).val($(this).val())
			// var hrg='#label_harga_racikan_' + counter;
			// var qty='#disp_quantity_racikan_' + counter;
			// var tuslahR='#disp_racikan_tuslah_r_' + counter;
			// var jml='#label_racikan_jumlah_' + counter;
			// var txthidejml ='#racikan_jumlah_'+counter;
			// hitungracikandetail(hrg,qty,tuslahR,jml,txthidejml)
			// var $txtTotal =$('#disp_total_racikan');
			// var lengthtr =$('#tbody_racikan tr.c_tr_racikan').length;
			// var $txthidettl =$('#total_racikan');
			// hitungtotal('tr_racikan_',$txtTotal,6,lengthtr,$txthidettl)
		});
		$('#table_racikan_detail').on('keyup', '.racikan_tuslah_r', function() {
			$tr = $(this).parent().parent();
			var counter = getCounter($tr);
			$('#racikan_tuslah_r_'+counter).val($(this).val())
			var hrg='#label_harga_racikan_'+counter;
			// var qty='#disp_quantity_racikan_'+counter;
			var tuslahR='#disp_racikan_tuslah_r_'+counter;
			var jml='#label_racikan_jumlah_'+counter;
			var txthidejml ='#racikan_jumlah_'+counter;
			hitungracikandetail(hrg,tuslahR,jml,txthidejml)
			var $txtTotal =$('#disp_total_racikan');
			var lengthtr =$('#tbody_racikan tr.c_tr_racikan').length;
			var $txthidettl =$('#total_racikan');
			hitungtotal('tr_racikan_',$txtTotal,6,lengthtr,$txthidettl)
		});
		
		$('#table_racikan_detail').on('keyup', '.nama_racikan', function() {
		    $(this).val($(this).val().toUpperCase());
		});

		$('#simpan_racikan_obat').click(function() {
			var totalFull=$('#manage-Totalsem-Racikan').text();
			var counter = $('#tmp_counter_racikan').val();
			$('#tr_racikan_obat_' + counter).html('');
			$tr = $('#manage-tabel-list-Racikan').find('tbody');
			$('#tr_racikan_obat_' + counter).html($tr.html());
			var $txtTotal =$('#disp_total');
			var $txthidettl =$('#total_racikan');
			var ctr =$('#tbody_racikan tr.c_tr_racikan').length;
			var ctr2 =$('#tbody_racikan tr#tr_racikan_obat_'+counter).length;
				$('#post-Trow').val(ctr)
				// $('#racikan-tr_racikan_obat_'+counter).val($tr.children().length)

			$('#racikan-list_obat_' + counter).val($tr.children().length);
			hitungtotal('tr_racikan_',$txtTotal,6,counter,$txthidettl)
			var harga = 0;
			if($tr.children().length) {
				$('#tr_racikan_' + counter + ' .list_obat_racikan').text('List Obat (' + $tr.children().length + ')');				
				var $tr_obat_detail = $tr.find('tr');
			} else {
				$('#tr_racikan_' + counter + ' .list_obat_racikan').text('List Obat (0)');
			}
				var hrg=$('#label_harga_racikan_'+counter).text(totalFull)
				$('#harga_racikan_'+counter).val(totalFull)
			$('#manage-tabel-list-Racikan').find('tbody').html('');
			// $tr = $(this).parent().parent();
			// var counter = getCounter($tr);
			// $('#quantity_racikan_'+counter).val($(this).val())
			// var hrg='#label_harga_racikan_' + counter;
			// var qty='#disp_quantity_racikan_' + counter;
			var tuslahR='#disp_racikan_tuslah_r_' + counter;
			var jml='#label_racikan_jumlah_' + counter;
			var txthidejml ='#racikan_jumlah_'+counter;
			hitungracikandetail(hrg,tuslahR,jml,txthidejml)
			var $txtTotal =$('#disp_total_racikan');
			var lengthtr =$('#tbody_racikan tr.c_tr_racikan').length;
			var $txthidettl =$('#total_racikan');
			hitungtotal('tr_racikan_',$txtTotal,6,lengthtr,$txthidettl)
		})

