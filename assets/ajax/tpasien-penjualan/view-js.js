
function cloneTabel2(){
    
            var $tr = $('#row_racikan_detail_clone #tr_racikan').clone();
            var $tr2 = $('#row_racikan_detail_clone #tr_racikan_obat').clone();
            
            var counter = parseInt($("#counter_racikan").val());
            counter++;

            $("#counter_racikan").val(counter);
            
            $tr.attr('id', 'tr_racikan_' + counter);

            $tr.find('#clone_racikan_detail_id')
                .attr('id', 'racikan_detail_id_' + counter)
                .attr('name', 'racikan_detail_id[]')
                .attr('value', counter);

            $tr.find('#clone_racikan')
                .attr('id', 'racikan_' + counter)
                .attr('name', 'racikan[]')
                .attr('value', 1);

            $tr.find('#clone_tmp_counter_racikan')
                .attr('id', 'tmp_counter_racikan_' + counter)
                .attr('name', 'tmp_counter_racikan[]')
                .attr('value', counter);

            $tr.find('#clone_div_racikan_obat')
                .attr('id', 'div_racikan_obat_' + counter);

            $tr.find('#clone_nama_racikan')
                .attr('id', 'nama_racikan_' + counter)
                .attr('name', 'nama_racikan[]');

            $tr.find('#clone_span_jenis_racikan')
                .attr('id', 'span_jenis_racikan_' + counter)
                .attr('name', 'jenis_racikan[]');

            $tr.find('#clone_jenis_racikan_lainnya')
                .attr('id', 'jenis_racikan_lainnya_' + counter)
                .attr('name', 'jenis_racikan_lainnya[]');

            $tr.find('#clone_span_cara_pakai_racikan')
                .attr('id', 'span_cara_pakai_racikan_' + counter)
                .attr('name', 'cara_pakai_racikan[]');
            
            $tr.find('#clone_disp_quantity_racikan')
                .attr('id', 'disp_quantity_racikan_' + counter);

            $tr.find('#clone_quantity_racikan')
                .attr('id', 'quantity_racikan_' + counter)
                .attr('name', 'quantity_racikan[]');

            $tr.find('#clone_label_harga_racikan')
                .attr('id', 'label_harga_racikan_' + counter);

            $tr.find('#clone_harga_racikan')
                .attr('id', 'harga_racikan_' + counter)
                .attr('name', 'harga_racikan[]');

            $tr.find('#clone_disp_racikan_tuslah_r')
                .attr('id', 'disp_racikan_tuslah_r_' + counter);

            $tr.find('#clone_disp_racikan_jmlharga')
                .attr('id', 'disp_racikan_jmlharga_' + counter);
                // .autoNumeric('init', {aSep: '.', aDec:',', aPad: false});

            $tr.find('#clone_disp_subtotal_racikan')
                .attr('id', 'disp_subtotal_racikan_' + counter);

            $tr.find('#clone_list_obat')
                .attr('id', 'racikan-list_obat_' + counter)
                .attr('name', 'racikan-list_obat[]')
                .attr('value', 0);

            $("#table_racikan_detail").find("#tbody_racikan").append($tr);
            
            $tr2.attr('id', 'tr_racikan_obat_' + counter);

            $("#table_racikan_detail").find("#tbody_racikan").append($tr2);

            // $('#close').hide();

            return false;
}