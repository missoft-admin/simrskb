var table;

$(document).ready(function(){
    table = $('#index_list').DataTable({
        "pageLength": 50,  
        "serverSide": true, 
        "order": [],
        "ajax": { "url": './tgudang_pemesanan/ajax_list', "type": "POST" },
        "columns": [
            {"data": "id", visible: false},
            {"data": "nopemesanan"},
            {"data": "nama"},
            {"data": "totalbarang"},
            {"data": "totalharga"},
            {"data": "status"},
            {"data": "action"},
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            var action = '<div class="btn-group" data-toggle="buttons">';
            action += link_action('info','view','tgudang_pemesanan/view/'+aData['id'],'glyphicon glyphicon-search','View');
            
            if(aData['status'] == 1) 
                action += link_action('success','acc','tgudang_pemesanan/acc/'+aData['id'],'glyphicon glyphicon-check','ACC');
            if(aData['status'] == 1 || aData['status'] == 2)
                action += link_action('danger','cancel','tgudang_pemesanan/cancel/'+aData['id'],'fa fa-times','Cancel');
            
            action += '</div>';

            if(aData['status'] == 1){
                $("td:eq(4)", nRow).html('<span class="label label-default">Pending</span>');
                $("td:eq(5)", nRow).html(action);
            }else if(aData['status'] == 2) {
                $("td:eq(4)", nRow).html('<span class="label label-info">Sudah Dipesan</span>');
                $("td:eq(5)", nRow).html(action);
            } else if(aData['status'] == 3) {
                $("td:eq(4)", nRow).html('<span class="label label-warning">Penerimaan</span>');                
                $("td:eq(5)", nRow).html(action);
            } else if(aData['status'] == 4) {
                $("td:eq(4)", nRow).html('<span class="label label-success">Selesai</span>');                
                $("td:eq(5)", nRow).html(action);
            } else {
                $("td:eq(4)", nRow).html('<span class="label label-danger">Dibatalkan</span>');                
                $("td:eq(5)", nRow).html(action);
            }
            return nRow;            
        }
    })
})


$(document).on('click','.view', function(){
    var url = $(this).attr('url')
    window.location = url
})

$(document).on('click','.acc', function(){
    var url = $(this).attr('url')
    $.ajax({
        url: url,
        success: function(data) {
            sweetAlert("Sukses!", 'Data berhasil diacc', "success");
            table = $('#index_list').DataTable().ajax.reload()
        }
    })
})

$(document).on('click','.cancel', function(){
    var url = $(this).attr('url')
    $.ajax({
        url: url,
        success: function(data) {
            sweetAlert("Sukses!", 'Data berhasil dibatalkan', "success");
            table = $('#index_list').DataTable().ajax.reload()
        }
    })
})

function link_action(btn,id,url,icon,tooltip='') {
    $html = '<button class="btn btn-'+btn+' btn-xs '+id+'" data-toggle="tooltip" data-placement="top" title="'+tooltip+'" url="'+url+'"><i class="'+icon+'"></i></button>';
    return $html;
}