var table;

$('#detail_list tbody').on('click','td', function(){
	table = $('#detail_list').DataTable({paging: false, searching: false, retrieve: true, destroy: true }).destroy();

	$('#modal-penerimaan-barang').fadeIn()
	removeClass('#frowindex')
	add_input('#form_detail','Row Index','rowindex', table.row( this ).index() )
	hidden('#frowindex')

	// harga beli
	removeClass('#fhargabeli')
	add_input('#form_detail','Harga Beli','hargabeli', table.cell( this,2 ).data() ) 

	// kuantitas
	removeClass('#fkuantitas')
	add_input('#form_detail','Kuantitas','kuantitas', table.cell( this,3 ).data() )

	// total harga
	removeClass('#ftotalharga_det')
	add_input('#form_detail','Total Harga','totalharga_det', table.cell( this,10 ).data() )	
	disabled('#totalharga_det')

	// no batch
	removeClass('#fnobatch')
	add_input('#form_detail','No Batch','nobatch', table.cell( this,9 ).data() )

	// kuantitas pemesanan
	removeClass('#fkuantitas_sisa')
	add_input('#form_detail','Kuantitas Sisa','kuantitas_sisa', table.cell( this,8 ).data() )		
	disabled('#kuantitas_sisa')
})

$(document).on('keyup','#hargabeli, #kuantitas', function(){
	var hargabeli 		= $('#hargabeli').val()
	var kuantitas 		= $('#kuantitas').val()

	if(hargabeli !== '' && kuantitas !== '' ) {		
		$('#totalharga_det').val( parseInt(hargabeli)*parseInt(kuantitas) )
	} else {
		$('#totalharga_det').val( 0 )
	}
})

$(document).on('click','#edit-penerimaan-barang', function(){
	table = $('#detail_list').DataTable({paging: false, searching: false, retrieve: true, destroy: true }).destroy();

	var idx = $('#rowindex').val();

	var valid = validateDetail();
	if (!valid) return false;

	$.toaster({priority : 'success', title : 'Sukses!', message : 'Data berhasil diupdate'});
	table.cell( idx ,2 ).data( $('#hargabeli').val() ).draw()
	table.cell( idx ,3 ).data( $('#kuantitas').val() ).draw()
	table.cell( idx ,9 ).data( $('#nobatch').val() ).draw()
	table.cell( idx ,10 ).data( $('#totalharga_det').val() ).draw()
	$('#modal-penerimaan-barang').hide()
	grand_total()
})

$(document).on('click','#close', function(){
	$('#modal-penerimaan-barang').hide()
})

$(document).on('click','#submit', function(){
	detail_value()
})

$(document).ready(function() {
	
	$('.hidden').attr('hidden',true)

	// get nopemesanan
	$.getJSON('./../tgudang_penerimaan/get_nopemesanan', function(data){
		var dataoption = '<option value="#">Pilih Opsi</option>';
		$.each(data, function(i,item){
			dataoption += '<option value="'+item.id+'">'+item.nopemesanan+' - '+item.nama+'</option>';
		})
		$('#nopemesanan').empty()
		$('#nopemesanan').append(dataoption)
	})
})


$(document).on('change', '#nopemesanan', function(){
	var n = $(this).val()

	if( n !== '#' ) {		
	    table = $('#detail_list').DataTable({
	        ajax: "./../tgudang_penerimaan/get_nopemesanan_detail/"+n,
	        searching: false,
	        paging: false,
			retrieve: true,
	        "columns": [ 
	        	{data: 'tipebarang'},
	        	{data: 'namabarang'},
	        	{data: 'harga'},
	        	{data: 'kuantitas'},
	        	{data: 'edit'},
	        	{data: 'idtipe', className: "hidden"},
	        	{data: 'idbarang', className: "hidden"},
	        	{data: 'iddistributor', className: "hidden"},
	        	{data: 'kuantitas_sisa', className: "hidden"},
	        	{data: 'nobatch', className: "hidden"},
	        	{data: 'totalharga', className: "hidden"},
	        ]
	    }).destroy();

	    grand_total()
	}
})


function detail_value() {
    var detail_tbl = $('table#detail_list tbody tr').get().map(function(row) {
        return $(row).find('td').get().map(function(cell) {
            return $(cell).html();
        });
    });

    $("#detail_value").val(JSON.stringify(detail_tbl))
}

function grand_total(){
    var totalbarang  = 0;
    var totalharga  = 0;
    $('#detail_list tbody tr').each(function() {
        totalbarang      	+= parseFloat($(this).find('td:eq(3)').text().replace(/\,/g,""));
        totalharga     		+= parseFloat($(this).find('td:eq(10)').text().replace(/\,/g,""));
    });

    $("#totalbarang").val(totalbarang);
    $("#totalharga").val(totalharga);
}

function validateDetail() {
	var kuantitas 			= $("#kuantitas").val();
	var kuantitas_sisa 		= $("#kuantitas_sisa").val();
	var harga 				= $("#hargabeli").val();
	var nobatch 			= $("#nobatch").val();

	if (kuantitas == "" || kuantitas == "0") {
		sweetAlert("Maaf...", "Kuantitas tidak boleh kosong!", "error");
		return false;
	}
	if (parseInt(kuantitas) > parseInt(kuantitas_sisa) ) {
		sweetAlert("Maaf...", "Kuantitas tidak boleh melebihi kuantitas sisa!", "error");
		return false;
	}
	if (harga == "" || harga == "0") {
		sweetAlert("Maaf...", "Harga tidak boleh kosong!", "error");
		return false;
	}
	if (nobatch == "" || nobatch == "0") {
		sweetAlert("Maaf...", "Nobatch tidak boleh kosong!", "error");
		return false;
	}

	return true;
}