var show;

// @statusko
function statusko(status) {
  switch (status) {
    case 0:
      var ketko = '<span class="label label-danger">Dibatalkan</span>';
    break;
    case 1:
      var ketko = '<span class="label label-default">Pasien Belum Datang</span>';
    break;
    case 2:
      var ketko = '<span class="label label-primary">Pasien Sudah Datang</span>';
    break;
    case 3:
      var ketko = '<span class="label label-success">Siap Di Operasi</span>';
    break;
    case 4:
      var ketko = '<span class="label label-info">Sudah Di Operasi</span>';
    break;
    case 5:
      var ketko = '<span class="label label-warning">Pasien Belum Datang</span>';
    break;
  }
  return ketko;
}

// @recana
function rencana(status) {
  switch (status) {
    case 1:
      var ketre = '<span class="label label-danger">ODS</span>';
    break;
    case 2:
      var ketre = '<span class="label label-primary">Rawat Inap</span>';
    break;
  }
  return ketre;
}

$(document).ready(function() {

  // ---------------------------------------------
  // $.ajax({
  //   url: './tkamar_operasi/gettanggalko',
  //   type: 'POST',
  //   dataType: 'json',
  //   data: {choose:1},
  //   success: function(data) {
  //     for (var x = 0; x < data.length; x++) {
  //       $('#tglakhir').val(data[x].tanggaloperasi);
  //     }
  //   }
  // });
  //
  // $.ajax({
  //   url: './tkamar_operasi/gettanggalko',
  //   type: 'POST',
  //   dataType: 'json',
  //   data: {choose:2},
  //   success: function(data) {
  //     for (var x = 0; x < data.length; x++) {
  //       $('#tglawal').val(data[x].tanggaloperasi);
  //     }
  //   }
  // });
  // ---------------------------------------------

  // @set format tanggal
  $('#tanggaloperasi, #tglawal, #tglakhir').datepicker({
    format: "yyyy-mm-dd",
    autoclose: true
  });

  // @set jam operasi to 24 Hours
  $('#jamoperasi').datetimepicker({
    format: "HH:mm",
    stepping: 30
  });
});
