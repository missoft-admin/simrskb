function number_format(number, decimals, decPoint, thousandsSep) {
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
	var n = !isFinite(+number) ? 0 : +number
	var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
	var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
	var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
	var s = ''

	var toFixedFix = function (n, prec) {
		var k = Math.pow(10, prec)
		return '' + (Math.round(n * k) / k)
			.toFixed(prec)
	}

	// @todo: for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || ''
		s[1] += new Array(prec - s[1].length + 1).join('0')
	}

	return s.join(dec)
}

function savetindakan(check) {
	switch (check) {
		case 1:
			var detail_table = $('#tempnarcosetable tbody .rows').get().map(function (row) {
				return $(row).find('td').get().map(function (cell) {
					return $(cell).html();
				});
			});
			$("#detailnarcose").val(JSON.stringify(detail_table));
			break;
		case 2:
			var detail_table = $('#tempobattable tbody .rows').get().map(function (row) {
				return $(row).find('td').get().map(function (cell) {
					return $(cell).html();
				});
			});
			$("#detailobat").val(JSON.stringify(detail_table));
			break;
		case 3:
			var detail_table = $('#tempalkestable tbody .rows').get().map(function (row) {
				return $(row).find('td').get().map(function (cell) {
					return $(cell).html();
				});
			});
			$("#detailalkes").val(JSON.stringify(detail_table));
			break;
		case 4:
			var detail_table = $('#tempimplanttable tbody .rows').get().map(function (row) {
				return $(row).find('td').get().map(function (cell) {
					return $(cell).html();
				});
			});
			$("#detailimplan").val(JSON.stringify(detail_table));
			break;
		case 5:
			var detail_table = $('#tempalattable tbody .rows').get().map(function (row) {
				return $(row).find('td').get().map(function (cell) {
					return $(cell).html();
				});
			});
			$("#detailsewaalat").val(JSON.stringify(detail_table));
			break;
	}
}

$(document).ready(function () {
	// ---------------------------------------------
	// ---------------------------------------------


	$("#simpan").click(function () {
		var tanggaloperasi = $("#tanggaloperasi").val();
		var jamoperasi = $("#jamoperasi").val();
	});

	// ---------------------------------------------
	$("#lihatimplant").click(function () {
		$("#detailestimasiomodal").modal("hide");
		$("#listestimasimodal").modal("hide");
	});

	$("#closelihatimplanko").click(function () {
		$("#listimplantko").modal("hide");
		$("#detailestimasiomodal").modal("show");
		$("#listestimasimodal").modal("show");
	});

	$("#closedetailestimasi").click(function () {
		$("#listestimasimodal").modal("show");
		$("#detailestimasiomodal").modal("hide");
	});

	// --------------------------------------
	$("#satu").click(function () {
		$("#tindakanoo").modal("hide");
	});

	$("#dua").click(function () {
		$("#tindakanoo").modal("hide");
	});

	$("#tiga").click(function () {
		$("#tindakanoo").modal("hide");
	});

	$("#empat").click(function () {
		$("#tindakanoo").modal("hide");
	});

	$("#lima").click(function () {
		$("#tindakanoo").modal("hide");
	});

	// --------------------------------------
	$('#closetnarcose').click(function () {
		$('#tnarcosemodal').modal("hide");
		$('#tindakanoo').modal("show");
	});

	$('#closetobat').click(function () {
		$('#tobatmodal').modal("hide");
		$('#tindakanoo').modal("show");
	});

	$('#closetalkes').click(function () {
		$('#talkesmodal').modal("hide");
		$('#tindakanoo').modal("show");
	});

	$('#closetimplant').click(function () {
		$('#timplantmodal').modal("hide");
		$('#tindakanoo').modal("show");
	});

	$('#closetalat').click(function () {
		$('#tsewaalatmodal').modal("hide");
		$('#tindakanoo').modal("show");
	});

	// ----------------------------------

	$('#searchnarcose').click(function () {
		$('#tnarcosemodal').modal("hide");
		$('#daftarnarcose').modal("show");
	});

	$('#searchobat').click(function () {
		$('#tobatmodal').modal("hide");
		$('#daftarobat').modal("show");
	});

	$('#searchalkes').click(function () {
		$('#talkesmodal').modal("hide");
		$('#daftaralkes').modal("show");
	});

	$('#searchimplant').click(function () {
		$('#timplantmodal').modal("hide");
		$('#daftarimplant').modal("show");
	});

	$('#searchalat').click(function () {
		$('#tsewaalatmodal').modal("hide");
		$('#daftarsewaalat').modal("show");
	});

	// --------------------------------------

	// --------------------------------------
	$('#closednarcose').click(function () {
		$('#daftarnarcose').modal("hide");
		$('#tnarcosemodal').modal("show");
	});

	$('#closedobat').click(function () {
		$('#daftarobat').modal("hide");
		$('#tobatmodal').modal("show");
	});

	$('#closedalkes').click(function () {
		$('#daftaralkes').modal("hide");
		$('#talkesmodal').modal("show");
	});

	$('#closedimplant').click(function () {
		$('#daftarimplant').modal("hide");
		$('#timplantmodal').modal("show");
	});

	$('#closedalat').click(function () {
		$('#daftarsewaalat').modal("hide");
		$('#tsewaalatmodal').modal("show");
	});
	// --------------------------------------

	// @save tindakan

	// @variable-global
	var idpasien = $('#idpasien').val();

	// @revision 29 &start
	function checktindakan(nama, kuantitas, check, what) {
		if (nama === "") {
			var message_u = 'Pilih ' + what;
			return $.toaster({
				priority: 'warning',
				title: 'Peringatan!',
				message: message_u
			});
		} else if (kuantitas === "") {
			return $.toaster({
				priority: 'warning',
				title: 'Peringatan!',
				message: 'Isi kuantitas .'
			});
		} else {
			return true;
		}
	}
	// @revision 29 &end

	// @revision 30 &start
	function elemen(kode, nama, idunit, harga, kuantitas, total, hargadasar, margin, idpasien, check, id) {
		var temp = "<tr class='rows'>";
		temp += "<td>" + kode + "</td>";
		temp += "<td>" + nama + "</td>";
		temp += "<td hidden>" + idunit + "</td>";
		temp += "<td>" + harga + "</td>";
		temp += "<td>" + kuantitas + "</td>";
		temp += "<td>" + total + "</td>";
		temp += "<td class='text-center'>" + "<a href='#' class='temporary-delete'><i class='fa fa-close'></i></a>" + "</td>";
		temp += "<td hidden>" + hargadasar + "</td>";
		temp += "<td hidden>" + margin + "</td>";
		temp += "<td hidden>" + idpasien + "</td>";
		temp += "<td hidden>" + check + "</td>";
		temp += "<td hidden>" + id + "</td>";
		temp += "</tr>";

		return temp;
	}
	// @revision 30 &end

	// @tambah penggunaan narcose
	$(document).on('click', '#tambahtnarcose', function () {
		var id, kode, nama, idunit, harga, kuantitas, total, check, what;
		id = $('#idumum').val();
		idunit = $('#idunitumum').val();
		kode = $('#kodeobatn').val();
		nama = $('#narcose').val();
		harga = $('#hargase').val();
		kuantitas = $('#kuantitasse').val();
		total = $('#totalse').val();
		// @revision 28 &start
		hargadasar = $('#hargadasar').val();
		margin = $('#margin').val();
		// @revision 28 &end
		check = parseInt(1);
		what = "Narcose";
		$('#narcose-empty').empty();

		var result = elemen(kode, nama, idunit, harga, kuantitas, total, hargadasar, margin, idpasien, check, id);
		var con = checktindakan(nama, kuantitas, check, what);

		if (con === true) {
			$('#tempnarcosetable tbody').append(result);
			savetindakan(check);
			$('#tnarcosemodal input').val('');
		}
	});

	// @tambah penggunaan obat
	$(document).on('click', '#tambahtobat', function () {
		var id, kode, nama, idunit, harga, kuantitas, total, check, what;
		id = $('#idumum').val();
		idunit = $('#idunitumum').val();
		kode = $('#kodeobat').val();
		nama = $('#nobat').val();
		harga = $('#hargaso').val();
		kuantitas = $('#kuantitaso').val();
		total = $('#totalso').val();
		// @revision 28 &start
		hargadasar = $('#hargadasar').val();
		margin = $('#margin').val();
		// @revision 28 &end
		check = parseInt(2);
		what = "Obat";
		$('#obat-empty').empty();

		var result = elemen(kode, nama, idunit, harga, kuantitas, total, hargadasar, margin, idpasien, check, id);
		var con = checktindakan(nama, kuantitas, check, what);

		if (con === true) {
			$('#tempobattable tbody').append(result);
			savetindakan(check);
			$('#tobatmodal input').val('');
		}
	});

	// @tambah penggunaan alkes
	$(document).on('click', '#tambahtalkes', function () {
		var id, kode, nama, idunit, harga, kuantitas, total, check, what;
		id = $('#idumum').val();
		idunit = $('#idunitumum').val();
		kode = $('#kodealkes').val();
		nama = $('#nalkes').val();
		harga = $('#hargaal').val();
		kuantitas = $('#kuantitasal').val();
		total = $('#totalal').val();
		// @revision 28 &start
		hargadasar = $('#hargadasar').val();
		margin = $('#margin').val();
		// @revision 28 &end
		check = parseInt(3);
		what = "Alat Kesehatan";
		$('#alkes-empty').empty();

		var result = elemen(kode, nama, idunit, harga, kuantitas, total, hargadasar, margin, idpasien, check, id);
		var con = checktindakan(nama, kuantitas, check, what);

		if (con === true) {
			$('#tempalkestable tbody').append(result);
			savetindakan(check);
			$('#talkesmodal input').val('');
		}
	});

	// @tambah penggunaan implant
	$(document).on('click', '#tambahtimplant', function () {
		var id, kode, nama, idunit, harga, kuantitas, total, check, what;
		id = $('#idumum').val();
		idunit = $('#idunitumum').val();
		kode = $('#kodeimplant').val();
		nama = $('#nimplant').val();
		harga = $('#hargaim').val();
		kuantitas = $('#kuantitasim').val();
		total = $('#totalim').val();
		// @revision 28 &start
		hargadasar = $('#hargadasar').val();
		margin = $('#margin').val();
		// @revision 28 &end
		check = parseInt(4);
		what = "Implan";
		$('#implant-empty').empty();

		var result = elemen(kode, nama, idunit, harga, kuantitas, total, hargadasar, margin, idpasien, check, id);
		var con = checktindakan(nama, kuantitas, check, what);

		if (con === true) {
			$('#tempimplanttable tbody').append(result);
			savetindakan(check);
			$('#timplantmodal input').val('');
		}
	});

	// @tambah penggunaan showsewaalat
	$(document).on('click', '#tambahtalat', function () {
		var id, kode, nama, idunit, harga, kuantitas, total, check, what;
		bhp = $('#bhp').val();
		biaya = $('#biayaperawatan').val();
		kode = $('#kodealat').val();
		nama = $('#nalat').val();
		hargaalat = $('#hargalat').val();
		kuantitas = $('#kuantitaslat').val();
		total = $('#totallat').val();
		// @revision 28 &start
		jasasarana = $('#jasasarana').val();
		jasapelayanan = $('#jasapelayanan').val();
		// @revision 28 &end
		check = parseInt(5);
		what = "Sewa Alat";
		$('#alat-empty').empty();

		var result = elemen(kode, nama, biaya, hargaalat, kuantitas, total, jasasarana, jasapelayanan, idpasien, check, bhp);
		var con = checktindakan(nama, kuantitas, check, what);

		if (con === true) {
			$('#tempalattable tbody').append(result);
			savetindakan(check);
			$('#tsewaalatmodal input').val('');
		}
	});

	// @bag narcose, obat, alkes, implant & sewaalat
	var kode, namaobat, satuan, harga, idobat;
	$(document).on('click', '.getrownarcose, .getrowobat, .getrowalkes, .getrowimplant, .getrowsewaalat', function (event) {
		$(event.target).closest('tr').each(function () {
			var check = parseInt($(this).find('td:eq(5)').text());
			kode = $(this).find('td:eq(0)').text();
			namaobat = $(this).find('td:eq(1)').text();
			hargajual = parseInt($(this).find('td:eq(2)').attr('data-value'));
			hargadasar = parseInt($(this).find('td:eq(3)').text());
			margin = parseInt($(this).find('td:eq(4)').text());
			idobat = parseInt($(this).find('td:eq(6)').text());
			idunit = parseInt($(this).find('td:eq(7)').text());
			if (check === 1) {
				$('#kodeobatn').val(kode)
				$('#narcose').val(namaobat);
				$('#hargase').val(hargajual);
				$('#hargadasar').val(hargadasar);
				$('#kuantitasse').val(1);
				$('#totalse').val(hargajual);
				$('#margin').val(margin);
				$('#idumum').val(idobat);
				$('#idunitumum').val(idunit);
				$('#daftarnarcose').modal("hide");
				$('#tnarcosemodal').modal("show");
			} else if (check == 2) {
				$('#kodeobat').val(kode)
				$('#nobat').val(namaobat);
				$('#hargaso').val(hargajual);
				$('#hargadasar').val(hargadasar);
				$('#kuantitaso').val(1);
				$('#totalso').val(hargajual);
				$('#margin').val(margin);
				$('#idumum').val(idobat);
				$('#idunitumum').val(idunit);
				$('#daftarobat').modal("hide");
				$('#tobatmodal').modal("show");
			} else if (check == 3) {
				$('#kodealkes').val(kode)
				$('#nalkes').val(namaobat);
				$('#hargaal').val(hargajual);
				$('#hargadasar').val(hargadasar);
				$('#kuantitasal').val(1);
				$('#totalal').val(hargajual);
				$('#margin').val(margin);
				$('#idumum').val(idobat);
				$('#idunitumum').val(idunit);
				$('#daftaralkes').modal("hide");
				$('#talkesmodal').modal("show");
			} else if (check == 4) {
				$('#kodeimplant').val(kode)
				$('#nimplant').val(namaobat);
				$('#hargaim').val(hargajual);
				$('#hargadasar').val(hargadasar);
				$('#kuantitasim').val(1);
				$('#totalim').val(hargajual);
				$('#margin').val(margin);
				$('#idumum').val(idobat);
				$('#idunitumum').val(idunit);
				$('#daftarimplant').modal("hide");
				$('#timplantmodal').modal("show");
			} else if (check == 5) {
				$('#kodealat').val(kode);
				$('#nalat').val(namaobat);
				$('#hargalat').val(hargajual);
				$('#kuantitaslat').val(1);
				$('#totallat').val(hargajual);
				$('#jasasarana').val(hargadasar);
				$('#jasapelayanan').val(margin);
				$('#bhp').val(idobat);
				$('#biayaperawatan').val(idunit);
				$('#daftarsewaalat').modal("hide");
				$('#tsewaalatmodal').modal("show");
			}
		});
	});

	function total(x, y) {
		var result = x * y;
		return result;
	}

	var hasil;
	$('#kuantitasse, #kuantitaso, #kuantitasim, #kuantitasal, #kuantitaslat').bind('keyup click', function () {
		if ($('#hargase').val() !== null) {
			hasil = total($('#kuantitasse').val(), $('#hargase').val());
			$('#totalse').val(hasil);
		}
		if ($('#hargaso').val() !== null) {
			hasil = total($('#kuantitaso').val(), $('#hargaso').val());
			$('#totalso').val(hasil);
		}
		if ($('#hargaal').val() !== null) {
			hasil = total($('#kuantitasal').val(), $('#hargaal').val());
			$('#totalal').val(hasil);
		}
		if ($('#hargaim').val() !== null) {
			hasil = total($('#kuantitasim').val(), $('#hargaim').val());
			$('#totalim').val(hasil);
		}
		if ($('#hargalat').val() !== null) {
			hasil = total($('#kuantitaslat').val(), $('#hargalat').val());
			$('#totallat').val(hasil);
		}
	});

	$(document).on('click', '#closetnarcose', function () {
		var dnarcose = $('#detailnarcose').val();
		var check = 1;
		if (dnarcose == "[]" || dnarcose == "") {
			$('#tnarcosemodal input').val('');
		} else {
			swal({
				title: "Anda Yakin ?",
				text: "Ingin menambahkan data narcose ?",
				type: "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function () {
				$.ajax({
					url: './../../../tkamar_operasi/savepenggunaan',
					type: 'POST',
					dataType: 'json',
					data: {
						check: check,
						detail: dnarcose
					},
					success: function (data) {
						$('#idumum').val('');
						$('#idunitumum').val('');
						$('#hargadasar').val('');
						$('#margin').val('');
						$('#detailnarcose').val('');
						$('#tempnarcosetable tbody').empty().append('<tr id="narcose-empty"><td class="text-center" colspan="6">-</td></tr>');
						swal("Berhasil!", "Data baru narcose tersimpan!", "success");
						shownarcose.ajax.reload();
					},
					error: function (data) {
						console.log(data);
						swal("Oops", "We couldn't connect to the server!", "error");
					}
				});
			});
		}
	});

	$(document).on('click', '#closetobat', function () {
		var dobat = $('#detailobat').val();
		var check = 2;

		if (dobat == "[]" || dobat == "") {
			$('#tobatmodal input').val('');
		} else {
			swal({
				title: "Anda Yakin ?",
				text: "Ingin menambahkan data obat ?",
				type: "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function () {
				$.ajax({
					url: './../../../tkamar_operasi/savepenggunaan',
					type: 'POST',
					dataType: 'json',
					data: {
						check: check,
						detail: dobat
					},
					success: function (data) {
						$('#idumum').val('');
						$('#idunitumum').val('');
						$('#hargadasar').val('');
						$('#margin').val('');
						$('#detailobat').val('');
						$('#tempobattable tbody').empty().append('<tr id="obat-empty"><td class="text-center" colspan="6">-</td></tr>');
						swal("Berhasil!", "Data baru obat tersimpan!", "success");
						showobat.ajax.reload();
					},
					error: function () {
						console.log(data);
						swal("Oops", "We couldn't connect to the server!", "error");
					}
				});
			});
		}
	});

	$(document).on('click', '#closetalkes', function () {
		var dalkes = $('#detailalkes').val();
		var check = 3;

		if (dalkes == "[]" || dalkes == "") {
			$('#talkesmodal').val('');
		} else {
			swal({
				title: "Anda Yakin ?",
				text: "Ingin menambahkan data alkes ?",
				type: "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function () {
				$.ajax({
					url: './../../../tkamar_operasi/savepenggunaan',
					type: 'POST',
					dataType: 'json',
					data: {
						check: check,
						detail: dalkes
					},
					success: function (data) {
						$('#idumum').val('');
						$('#idunitumum').val('');
						$('#hargadasar').val('');
						$('#margin').val('');
						$('#detailalkes').val('');
						$('#tempalkestable tbody').empty().append('<tr id="alkes-empty"><td class="text-center" colspan="6">-</td></tr>');
						swal("Berhasil!", "Data baru alkes tersimpan!", "success");
						showalkes.ajax.reload();
					},
					error: function () {
						console.log(data);
						swal("Oops", "We couldn't connect to the server!", "error");
					}
				});
			});
		}
	});

	$(document).on('click', '#closetimplant', function () {
		var dimplan = $('#detailimplan').val();
		var check = 4;

		if (dimplan == "[]" || dimplan == "") {
			$('#timplantmodal input').val('');
		} else {
			swal({
				title: "Anda Yakin ?",
				text: "Ingin menambahkan data implan ?",
				type: "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function () {
				$.ajax({
					url: './../../../tkamar_operasi/savepenggunaan',
					type: 'POST',
					dataType: 'json',
					data: {
						check: check,
						detail: dimplan
					},
					success: function (data) {
						$('#idumum').val('');
						$('#idunitumum').val('');
						$('#hargadasar').val('');
						$('#margin').val('');
						$('#detailimplan').val('');
						$('#tempimplanttable tbody').empty().append('<tr id="implant-empty"><td class="text-center" colspan="6">-</td></tr>');
						swal("Berhasil!", "Data baru implan tersimpan!", "success");
						showimplant.ajax.reload();
					},
					error: function (data) {
						console.log(data);
						swal("Oops", "We couldn't connect to the server!", "error");
					}
				});
			});
		}
	});

	$(document).on('click', '#closetalat', function () {
		var dalat = $('#detailsewaalat').val();
		var check = 5;

		if (dalat == "[]" || dalat == "") {
			$('#tsewaalatmodal input').val('');
		} else {
			swal({
				title: "Anda Yakin ?",
				text: "Ingin menambahkan data sewa alat ?",
				type: "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function () {
				$.ajax({
					url: './../../../tkamar_operasi/savepenggunaan',
					type: 'POST',
					dataType: 'json',
					data: {
						check: check,
						detail: dalat
					},
					success: function (data) {
						$('#idumum').val('');
						$('#idunitumum').val('');
						$('#hargadasar').val('');
						$('#margin').val('');
						$('#detailsewaalat').val('');
						$('#tempalattable tbody').empty().append('<tr id="alat-empty"><td class="text-center" colspan="6">-</td></tr>');
						swal("Berhasil!", "Data baru sewa alat tersimpan!", "success");
						showsewaalat.ajax.reload();
					},
					error: function () {
						console.log(data);
						swal("Oops", "We couldn't connect to the server!", "error");
					}
				});
			});
		}
	});

});

// @global-variabel
var show, shownarcose, showobat, showimplant, showalkes, showbhp;

function getidkelompok(nama) {
	// 0 : Umum | 1 : Perusahaan Asuransi | 2 : Jasa Raharja | 3 : BPJS Kesehatan | 4 BPJS Ketenagakerjaan
	var jenis;
	if (nama === "Umum") {
		jenis = 0;
	} else if (nama === "Perusahaan Asuransi") {
		jenis = 1;
	} else if (nama === "Jasa Raharja") {
		jenis = 2;
	} else if (nama === "BPJS Kesehatan") {
		jenis = 3;
	} else if (nama === "BPJS Ketenagakerjaan") {
		jenis = 4;
	}
	return jenis;
}

function formula(hargadasar, margin) {
	if (margin == 0) {
		var hargajual = hargadasar;
	} else {
		var hargajual = (parseInt(hargadasar) * (parseInt(margin) / 100)) + parseInt(hargadasar);
	}
	return hargajual;
}

$(document).ready(function () {
	var nama = $('#kelompokpasien').val();
	var kelompokpasien = getidkelompok(nama);
});


function getkelas(romawi) {
	// 1 : III | 2 : II | 3 : I
	var kelas;
	if (romawi === "III") {
		kelas = 1;
	} else if (romawi === "II") {
		kelas = 2;
	} else if (romawi === "I") {
		kelas = 3;
	}
	return kelas;
}

// @delete
$(document).on('click', '.delete', function (event) {
	$(event.target).closest('tr').each(function () {
		var check = parseInt($(this).find('td:eq(8)').text());
		var id = parseInt($(this).find('td:eq(6)').text());

		swal({
			title: "Anda Yakin ?",
			text: "Ingin menghapus data ini ?",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#ec6c62",
			cancelButtonText: "Batalkan",
		}).then(function () {
			$.ajax({
				url: './../../../tkamar_operasi/deletepenggunaan',
				type: 'POST',
				dataType: 'json',
				data: {
					id: id,
					check: check,
					idtindakan: segment3
				},
				success: function (data) {
					switch (check) {
						case 1:
							shownarcose.ajax.reload();
							break;
						case 2:
							showobat.ajax.reload();
							break;
						case 3:
							showalkes.ajax.reload();
							break;
						case 4:
							showimplant.ajax.reload();
							break;
						case 5:
							showsewaalat.ajax.reload();
							break;
					}

					swal("Berhasil!", "Data berhasil terhapus!", "success");
				},
				error: function () {
					swal("Oops", "We couldn't connect to the server!", "error");
				}
			});
		});
	});
});

$(document).on('click', '.edit-narcose', function (event) {
	$(event.target).closest('tr').each(function () {
		var kuantitas = $(this).find('td:eq(3)').text();
		$(this).find('td:eq(3)').text('').append('<input type="number" class="form-control" value="' + kuantitas + '" style="width:80px;"></input>');
		$(this).find('td:eq(5)').text('').append('<button class="btn btn-sm btn-success" data-value="' + 0 + '"><i class="fa fa-check"></i></button>&nbsp;<button class="btn btn-sm btn-primary"><i class="fa fa-undo"></i></button>');
		$('.edit-narcose, .delete').attr('disabled', 'disabled');
	});
});

$(document).on('click', '.edit-obat', function (event) {
	$(event.target).closest('tr').each(function () {
		var kuantitas = $(this).find('td:eq(3)').text();
		$(this).find('td:eq(3)').text('').append('<input type="number" class="form-control" value="' + kuantitas + '" style="width:80px;"></input>');
		$(this).find('td:eq(5)').text('').append('<button class="btn btn-sm btn-success" data-value="' + 0 + '"><i class="fa fa-check"></i></button>&nbsp;<button class="btn btn-sm btn-primary"><i class="fa fa-undo"></i></button>');
		$('.edit-obat, .delete').attr('disabled', 'disabled');
	});
});

$(document).on('click', '.edit-alkes', function (event) {
	$(event.target).closest('tr').each(function () {
		var kuantitas = $(this).find('td:eq(3)').text();
		$(this).find('td:eq(3)').text('').append('<input type="number" class="form-control" value="' + kuantitas + '" style="width:80px;"></input>');
		$(this).find('td:eq(5)').text('').append('<button class="btn btn-sm btn-success" data-value="' + 0 + '"><i class="fa fa-check"></i></button>&nbsp;<button class="btn btn-sm btn-primary"><i class="fa fa-undo"></i></button>');
		$('.edit-alkes, .delete').attr('disabled', 'disabled');
	});
});

$(document).on('click', '.edit-implan', function (event) {
	$(event.target).closest('tr').each(function () {
		var kuantitas = $(this).find('td:eq(3)').text();
		$(this).find('td:eq(3)').text('').append('<input type="number" class="form-control" value="' + kuantitas + '" style="width:80px;"></input>');
		$(this).find('td:eq(5)').text('').append('<button class="btn btn-sm btn-success" data-value="' + 0 + '"><i class="fa fa-check"></i></button>&nbsp;<button class="btn btn-sm btn-primary"><i class="fa fa-undo"></i></button>');
		$('.edit-implan, .delete').attr('disabled', 'disabled');
	});
});

$(document).on('click', '.edit-sewaalat', function (event) {
	$(event.target).closest('tr').each(function () {
		var kuantitas = $(this).find('td:eq(3)').text();
		$(this).find('td:eq(3)').text('').append('<input type="number" class="form-control" value="' + kuantitas + '" style="width:80px;"></input>');
		$(this).find('td:eq(5)').text('').append('<button class="btn btn-sm btn-success" data-value="' + 0 + '"><i class="fa fa-check"></i></button>&nbsp;<button class="btn btn-sm btn-primary"><i class="fa fa-undo"></i></button>');
		$('.edit-sewaalat, .delete').attr('disabled', 'disabled');
	});
});

$(document).ready(function () {
	var id = parseInt($('#idpasien').val());
	// @show-narcose &start
	shownarcose = $('#table-narcose-a').DataTable({
		"ajax": "./../../../tkamar_operasi/getallnarcose/" + id,
		"oLanguage": {
			"sEmptyTable": "<i>Belum Ada Tindakan Penggunaan</i>",
		},
		"autoWidth": false,
		"searching": false,
		"lengthChange": false,
		"pageLength": 10,
		columns: [{
				"data": "tanggal"
			},
			{
				"data": "nama"
			},
			{
				"data": "harga"
			},
			{
				"data": "kuantitas"
			},
			{
				"data": "subtotal"
			},
			{
				"data": "id"
			},
			{
				"data": "id"
			},
			{
				"data": "statuskasir"
			},
			{
				"data": "id"
			},
		],
		"columnDefs": [{
			"width": "10%",
			"targets": 3
		}],
		"rowCallback": function (row, data, index) {
			$('td:eq(2)', row).text(number_format(data["harga"]));
			$('td:eq(3)', row).text(number_format(data["kuantitas"]));
			$('td:eq(4)', row).text(number_format(data["subtotal"]));
			$('td:eq(6)', row).attr('hidden', '');
			$('td:eq(7)', row).attr('hidden', '');
			$('td:eq(8)', row).html(1).attr('hidden', '');
			$('td:eq(5)', row).addClass('text-center');
			if (data["statuskasir"] == 1 || data["statusverifikasi"] == 1) {
				var showend = '<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>';
				$('td:eq(5)', row).html(showend);
			} else {
				// var showone = '<button type="button" class="btn btn-sm btn-primary aksi btn-kasir edit-narcose"><i class="fa fa-pencil"></i></button>';
				var showone = '&nbsp;<button type="button" class="btn btn-sm btn-danger aksi btn-kasir delete"><i class="fa fa-trash"></i></button>';
				$('td:eq(5)', row).html(showone);
			}
		}
	});
	// @show-narcose &end

	// @show-obat &start
	showobat = $('#table-obat-a').DataTable({
		"ajax": "./../../../tkamar_operasi/getallobat/" + id,
		"oLanguage": {
			"sEmptyTable": "<i>Belum Ada Tindakan Penggunaan</i>",
		},
		"autoWidth": false,
		"searching": false,
		"lengthChange": false,
		"pageLength": 10,
		columns: [{
				"data": "tanggal"
			},
			{
				"data": "nama"
			},
			{
				"data": "harga"
			},
			{
				"data": "kuantitas"
			},
			{
				"data": "subtotal"
			},
			{
				"data": "id"
			},
			{
				"data": "id"
			},
			{
				"data": "statuskasir"
			},
			{
				"data": "id"
			}
		],
		"columnDefs": [{
			"width": "10%",
			"targets": 3
		}],
		"rowCallback": function (row, data, index) {
			$('td:eq(2)', row).text(number_format(data["harga"]));
			$('td:eq(3)', row).text(number_format(data["kuantitas"]));
			$('td:eq(4)', row).text(number_format(data["subtotal"]));
			$('td:eq(6)', row).attr('hidden', '');
			$('td:eq(7)', row).attr('hidden', '');
			$('td:eq(8)', row).html(2).attr('hidden', '');
			$('td:eq(5)', row).addClass('text-center');
			if (data["statuskasir"] == 1 || data["statusverifikasi"] == 1) {
				var showend = '<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>';
				$('td:eq(5)', row).html(showend);
			} else {
				// var showone = '<button type="button" class="btn btn-sm btn-primary aksi btn-kasir edit-obat"><i class="fa fa-pencil"></i></button>';
				var showone = '&nbsp;<button type="button" class="btn btn-sm btn-danger aksi btn-kasir delete"><i class="fa fa-trash"></i></button>';
				$('td:eq(5)', row).html(showone);
			}
		}
	});
	// @show-obat &end

	// @show-alkes &start
	showalkes = $('#table-alkes-a').DataTable({
		"ajax": "./../../../tkamar_operasi/getallalkes/" + id,
		"oLanguage": {
			"sEmptyTable": "<i>Belum Ada Tindakan Penggunaan</i>",
		},
		"autoWidth": false,
		"searching": false,
		"lengthChange": false,
		"pageLength": 10,
		columns: [{
				"data": "tanggal"
			},
			{
				"data": "nama"
			},
			{
				"data": "harga"
			},
			{
				"data": "kuantitas"
			},
			{
				"data": "subtotal"
			},
			{
				"data": "id"
			},
			{
				"data": "id"
			},
			{
				"data": "statuskasir"
			},
			{
				"data": "id"
			}
		],
		"columnDefs": [{
			"width": "10%",
			"targets": 3
		}],
		"rowCallback": function (row, data, index) {
			$('td:eq(2)', row).text(number_format(data["harga"]));
			$('td:eq(3)', row).text(number_format(data["kuantitas"]));
			$('td:eq(4)', row).text(number_format(data["subtotal"]));
			$('td:eq(6)', row).attr('hidden', '');
			$('td:eq(7)', row).attr('hidden', '');
			$('td:eq(8)', row).html(3).attr('hidden', '');
			$('td:eq(5)', row).addClass('text-center');
			if (data["statuskasir"] == 1 || data["statusverifikasi"] == 1) {
				var showend = '<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>';
				$('td:eq(5)', row).html(showend);
			} else {
				// var showone = '<button type="button" class="btn btn-sm btn-primary aksi btn-kasir edit-alkes"><i class="fa fa-pencil"></i></button>';
				var showone = '&nbsp;<button type="button" class="btn btn-sm btn-danger aksi btn-kasir delete"><i class="fa fa-trash"></i></button>';
				$('td:eq(5)', row).html(showone);
			}
		}
	});
	// @show-alkes &end

	// @show-implant &start
	showimplant = $('#table-implant-a').DataTable({
		"ajax": "./../../../tkamar_operasi/getallimplant/" + id,
		"oLanguage": {
			"sEmptyTable": "<i>Belum Ada Tindakan Penggunaan</i>",
		},
		"autoWidth": false,
		"searching": false,
		"lengthChange": false,
		"pageLength": 10,
		columns: [{
				"data": "tanggal"
			},
			{
				"data": "nama"
			},
			{
				"data": "harga"
			},
			{
				"data": "kuantitas"
			},
			{
				"data": "subtotal"
			},
			{
				"data": "id"
			},
			{
				"data": "id"
			},
			{
				"data": "statuskasir"
			},
			{
				"data": "id"
			}
		],
		"columnDefs": [{
			"width": "10%",
			"targets": 3
		}],
		"rowCallback": function (row, data, index) {
			$('td:eq(2)', row).text(number_format(data["harga"]));
			$('td:eq(3)', row).text(number_format(data["kuantitas"]));
			$('td:eq(4)', row).text(number_format(data["subtotal"]));
			$('td:eq(6)', row).attr('hidden', '');
			$('td:eq(7)', row).attr('hidden', '');
			$('td:eq(8)', row).html(4).attr('hidden', '');
			$('td:eq(5)', row).addClass('text-center');
			if (data["statuskasir"] == 1 || data["statusverifikasi"] == 1) {
				var showend = '<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>';
				$('td:eq(5)', row).html(showend);
			} else {
				// var showone = '<button type="button" class="btn btn-sm btn-primary aksi btn-kasir edit-implan"><i class="fa fa-pencil"></i></button>';
				var showone = '&nbsp;<button type="button" class="btn btn-sm btn-danger aksi btn-kasir delete"><i class="fa fa-trash"></i></button>';
				$('td:eq(5)', row).html(showone);
			}
		}
	});
	// @show-implant &end

	// @show-sewaalat &start
	showsewaalat = $('#table-sewaalat-a').DataTable({
		"ajax": "./../../../tkamar_operasi/getallsewaalat/" + id,
		"oLanguage": {
			"sEmptyTable": "<i>Belum Ada Tindakan Penggunaan</i>",
		},
		"autoWidth": false,
		"searching": false,
		"lengthChange": false,
		"pageLength": 10,
		columns: [{
				"data": "tanggal"
			},
			{
				"data": "nama"
			},
			{
				"data": "harga"
			},
			{
				"data": "kuantitas"
			},
			{
				"data": "subtotal"
			},
			{
				"data": "id"
			},
			{
				"data": "id"
			},
			{
				"data": "statuskasir"
			},
			{
				"data": "id"
			}
		],
		"columnDefs": [{
			"width": "10%",
			"targets": 3
		}],
		"rowCallback": function (row, data, index) {
			$('td:eq(2)', row).text(number_format(data["harga"]));
			$('td:eq(3)', row).text(number_format(data["kuantitas"]));
			$('td:eq(4)', row).text(number_format(data["subtotal"]));
			$('td:eq(6)', row).attr('hidden', '');
			$('td:eq(7)', row).attr('hidden', '');
			$('td:eq(8)', row).html(5).attr('hidden', '');
			$('td:eq(5)', row).addClass('text-center');
			if (data["statuskasir"] == 1 || data["statusverifikasi"] == 1) {
				var showend = '<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>';
				$('td:eq(5)', row).html(showend);
			} else {
				// var showone = '<button type="button" class="btn btn-sm btn-primary aksi btn-kasir edit-sewaalat"><i class="fa fa-pencil"></i></button>';
				var showone = '&nbsp;<button type="button" class="btn btn-sm btn-danger aksi btn-kasir delete"><i class="fa fa-trash"></i></button>';
				$('td:eq(5)', row).html(showone);
			}
		}
	});
	// @show-sewaalat &end

	// @set tanggal(kamaroperasi)
	$("#tanggaloperasi").datepicker({
		format: "yyyy-mm-dd"
	});
	// @set waktuawal
	$("#waktuawal").datetimepicker({
		format: "HH:mm",
		stepping: 30
	});

	// @searching area &start
	// @search-narcose
	$('#narcose').easyAutocomplete({
		url: "./../../../tkamar_operasi/searchstok_na",
		dataType: 'json',
		getValue: "namaobat",
		list: {
			showAnimation: {
				type: "slide",
				time: 400,
			},
			hideAnimation: {
				type: "slide",
				time: 400
			},
			match: {
				enabled: true
			}
		},
		template: {
			type: "custom",
			method: function (value, item) {
				return "<span>" + (item.kode) + " - " + (item.namaobat) + "</span>"
			}
		},
		theme: "custom"
	});
	// @search-obat
	$('#nobat').easyAutocomplete({
		url: "./../../../tkamar_operasi/searchstok_ob",
		dataType: 'json',
		getValue: "namaobat",
		list: {
			showAnimation: {
				type: "slide",
				time: 400,
			},
			hideAnimation: {
				type: "slide",
				time: 400
			},
			match: {
				enabled: true
			}
		},
		template: {
			type: "custom",
			method: function (value, item) {
				return "<span>" + (item.kode) + " - " + (item.namaobat) + "</span>"
			}
		},
		theme: "custom"
	});
	// @search-alkes
	$('#nalkes').easyAutocomplete({
		url: "./../../../tkamar_operasi/searchstok_al",
		dataType: 'json',
		getValue: "namaalkes",
		list: {
			showAnimation: {
				type: "slide",
				time: 400,
			},
			hideAnimation: {
				type: "slide",
				time: 400
			},
			match: {
				enabled: true
			}
		},
		template: {
			type: "custom",
			method: function (value, item) {
				return "<span>" + (item.kode) + " - " + (item.namaalkes) + "</span>"
			}
		},
		theme: "custom"
	});
	// @search-implan
	$('#nimplant').easyAutocomplete({
		url: "./../../../tkamar_operasi/searchstok_im",
		dataType: 'json',
		getValue: "namaimplan",
		list: {
			showAnimation: {
				type: "slide",
				time: 400,
			},
			hideAnimation: {
				type: "slide",
				time: 400
			},
			match: {
				enabled: true
			}
		},
		template: {
			type: "custom",
			method: function (value, item) {
				return "<span>" + (item.kode) + " - " + (item.namaimplan) + "</span>"
			}
		},
		theme: "custom"
	});
	// @search-alat
	$('#nalat').easyAutocomplete({
		url: "./../../../tkamar_operasi/searchstok_at",
		dataType: 'json',
		getValue: "nama",
		ajaxSettings: {
			dataType: "json",
			method: "POST",
			data: {
				kelas: kelas,
				jenis: jenis
			}
		},
		list: {
			showAnimation: {
				type: "slide",
				time: 400,
			},
			hideAnimation: {
				type: "slide",
				time: 400
			},
			match: {
				enabled: true
			}
		},
		template: {
			type: "custom",
			method: function (value, item) {
				return "<span>" + (item.nama) + "</span>"
			}
		},
		theme: "custom"
	});
	// @searching area &end

	// @result area &start

	// @narcose
	$('#narcose').bind('keyup keypress', function () {
		var nama = $(this).val();
		$.ajax({
			url: './../../../tkamar_operasi/getstok',
			type: 'POST',
			dataType: 'json',
			data: {
				nama: nama,
				idunit: 3,
				idtipe: 3,
				idkategori: 1
			},
			success: function (data) {
				if (data.length === 1) {
					for (var x = 0; x < data.length; x++) {
						$('#kodeobatn').val(data[x].kode);
						$('#hargase').val(formula(data[x].harga, data[x].margin));
						$('#kuantitasse').val(1);
						$('#totalse').val(formula(data[x].harga, data[x].margin));
						$('#hargadasar').val(data[x].harga);
						$('#margin').val(data[x].margin);
						$('#idumum').val(data[x].idobat);
						$('#idunitumum').val(3);
					}
				} else {
					$('#kodeobatn').val('');
					$('#hargase').val('');
					$('#kuantitasse').val('');
					$('#totalse').val('');
				}
			}
		});
	});
	// @obat
	$('#nobat').bind('keyup keypress', function () {
		var nama = $(this).val();
		$.ajax({
			url: './../../../tkamar_operasi/getstok',
			type: 'POST',
			dataType: 'json',
			data: {
				nama: nama,
				idunit: 3,
				idtipe: 3,
				idkategori: 2
			},
			success: function (data) {
				if (data.length === 1) {
					for (var x = 0; x < data.length; x++) {
						$('#kodeobat').val(data[x].kode);
						$('#hargaso').val(formula(data[x].harga, data[x].margin));
						$('#kuantitaso').val(1);
						$('#totalso').val(formula(data[x].harga, data[x].margin));
						$('#hargadasar').val(data[x].harga);
						$('#margin').val(data[x].margin);
						$('#idumum').val(data[x].idobat);
						$('#idunitumum').val(3);
					}
				} else {
					$('#kodeobat').val('');
					$('#hargaso').val('');
					$('#kuantitaso').val('');
					$('#totalso').val('');
				}
			}
		});
	});
	// @alkes
	$('#nalkes').bind('keyup keypress', function () {
		var nama = $(this).val();
		$.ajax({
			url: './../../../tkamar_operasi/getstok',
			type: 'POST',
			dataType: 'json',
			data: {
				nama: nama,
				idunit: 3,
				idtipe: 1
			},
			success: function (data) {
				if (data.length === 1) {
					for (var x = 0; x < data.length; x++) {
						$('#kodealkes').val(data[x].kode);
						$('#hargaal').val(formula(data[x].harga, data[x].margin));
						$('#kuantitasal').val(1);
						$('#totalal').val(formula(data[x].harga, data[x].margin));
						$('#hargadasar').val(data[x].harga);
						$('#margin').val(data[x].margin);
						$('#idumum').val(data[x].idalkes);
						$('#idunitumum').val(3);
					}
				} else {
					$('#kodealkes').val('');
					$('#hargaal').val('');
					$('#kuantitasal').val('');
					$('#totalal').val('');
				}
			}
		});
	});
	// @implan
	$('#nimplant').bind('keyup keypress', function () {
		var nama = $(this).val();
		$.ajax({
			url: './../../../tkamar_operasi/getstok',
			type: 'POST',
			dataType: 'json',
			data: {
				nama: nama,
				idunit: 3,
				idtipe: 2
			},
			success: function (data) {
				if (data.length === 1) {
					for (var x = 0; x < data.length; x++) {
						$('#kodeimplant').val(data[x].kode);
						$('#hargaim').val(formula(data[x].hargadasar, data[x].margin));
						$('#kuantitasim').val(1);
						$('#totalim').val(formula(data[x].hargadasar, data[x].margin));
						$('#hargadasar').val(data[x].hargadasar);
						$('#margin').val(data[x].margin);
						$('#idumum').val(data[x].id);
						$('#idunitumum').val(3);
					}
				} else {
					$('#kodeimplant').val('');
					$('#hargaim').val('');
					$('#kuantitasim').val('');
					$('#totalim').val('');
				}
			}
		});
	});
	// @sewaalat
	$('#nalat').bind('keyup keypress', function () {
		var nama = $(this).val();
		$.ajax({
			url: './../../../tkamar_operasi/searchalat',
			type: 'POST',
			dataType: 'json',
			data: {
				kelas: kelas,
				jenis: jenis,
				nama: nama
			},
			success: function (data) {
				if (data.length === 1) {
					for (var x = 0; x < data.length; x++) {
						$('#kodealat').val(data[x].id);
						$('#hargalat').val(data[x].total);
						$('#kuantitaslat').val(1);
						$('#totallat').val(data[x].total);
						('#jasasarana').val(data[x].jasasarana);
						$('#jasapelayanan').val(data[x].jasapelayanan);
						$('#bhp').val(data[x].bhp);
						$('#biayaperawatan').val(data[x].biayaperawatan);
						$('#idumum').val(data[x].id);
						$('#idunitumum').val(0);
					}
				} else {
					$('#kodealat').val('');
					$('#hargalat').val('');
					$('#kuantitaslat').val('');
					$('#totallat').val('');
				}
			}
		});
	});

	// @result area &end
	$(document).on('click', '.temporary-delete', function () {
		$(this).closest('tr').remove();
		savetindakan(1);
		savetindakan(2);
		savetindakan(3);
		savetindakan(4);
		savetindakan(5);
		swal("Deleted!", "Your file was successfully deleted!", "success");
	});

});
