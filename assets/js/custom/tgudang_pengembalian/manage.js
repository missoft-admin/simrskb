var table;

$(document).on('click','#detail_pengembalian_add', function(){
    table = $('#list-barang').DataTable().destroy()

    var option      = JSON.parse('{"1":"Alkes","2":"Implan","3":"Obat","4":"Logistik"}');
    var dataoption  = '<option value="">Pilih Opsi</option>';
    $.each(option, function(i,item){
        dataoption += '<option value="'+i+'">'+item+'</option>';
    })

    $('#namabarang,#hargabeli,#kuantitas,#kuantitas_pemesanan').val('')
    $('#idtipe').empty().append('#idtipe',dataoption).selectpicker('refresh')
    $('#idpemesanan, #nobatch').empty().selectpicker('refresh')
})

$(document).on('change','#idtipe', function(){

    var n = $('#idtipe').val()

    $('#modal_detail_pengembalian').hide()
    table = $('#list-barang').DataTable({
        pageLength: 50,  
        serverSide: true,        
        autoWidth: false,        
        order: [],
        "ajax": { "url": './../tgudang_pemesanan/get_list_barang/'+n, "type": "POST" },
        "columns": [
            {data: "id", visible: false},
            {data: "kode", width: '10%', className: 'link-kodebarang'},
            {data: "nama", width: '10%'},
            {data: "hargabeli", width: '10%',className: 'link-hargabeli'},
        ],
    })
    $('#modal_list_barang').fadeIn()

})

$('#list-barang').on( 'click', 'td', function(){
    table = $('#list-barang').DataTable()

    // nobatch
    $.ajax({
        url: './../tgudang_pengembalian/get_nobatch',
        data: { idtipe: $('#idtipe').val(), idbarang: table.cell(this,0).data() },
        method: 'POST',
        dataType: 'json',
        success: function(data) {
            var dataoption  = '<option value="">Pilih Opsi</option>';
            $.each(data, function(i,item){
                dataoption += '<option value="'+item.nobatch+'">'+item.nobatch+'</option>';
            })            
            $('#nobatch').empty().append(this,dataoption).selectpicker('refresh')
        }
    })

    table.destroy()
    $('#idbarang').val( table.cell(this,0).data() )
    $('#namabarang').val( table.cell(this,2).data() )
    $('#hargabeli').val( table.cell(this,3).data() )

    $('#kuantitas_pemesanan').val('')
    $('#idpemesanan, #nobatch').empty().selectpicker('refresh')
    

    $('#modal_list_barang').hide()
    $('#modal_detail_pengembalian').fadeIn()


})

$(document).on( 'change', '#nobatch', function(){
    var n = $(this).val()
    // idpemesanan
    $.ajax({
        url: './../tgudang_pengembalian/get_idpemesanan/'+n,
        method: 'POST',
        dataType: 'json',
        success: function(data) {
            var dataoption = '<option value="'+data.id+'">'+data.nopemesanan+' - '+data.nama+'</option>';
            $('#idpemesanan').empty().append(this,dataoption).selectpicker('refresh')

            $.ajax({
                url: './../tgudang_pengembalian/get_kuantitas_barang/',
                data: {idpemesanan: data.id, idbarang: $('#idbarang').val(), idtipe: $('#idtipe').val()},
                method: 'POST',
                dataType: 'text',
                success: function(d) {
                    $('#kuantitas_pemesanan').val(d)
                    $('#kuantitas').focus()
                }
            })
        }
    })    
})

$(document).on('click','#btnback', function(){
    table = $('#list-barang').DataTable().destroy()
    $('#modal_detail_pengembalian').fadeIn()
    $('#modal_list_barang').hide()

    var option      = JSON.parse('{"1":"Alkes","2":"Implan","3":"Obat","4":"Logistik"}');
    var dataoption  = '<option value="">Pilih Opsi</option>';
    $.each(option, function(i,item){
        dataoption += '<option value="'+i+'">'+item+'</option>';
    })

    $('#namabarang,#hargabeli,#kuantitas').val('')
    $('#idtipe').empty().append('#idtipe',dataoption).selectpicker('refresh')
    $('#idpemesanan').empty().selectpicker('refresh')
})


