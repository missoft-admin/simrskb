var table;

$(document).on('change','#idtipe', function(){

	var n = $('#idtipe').val()

	$('#modal-detail-pemesanan').hide()
    table = $('#list-barang').DataTable({
        pageLength: 50,  
        serverSide: true,        
        autoWidth: false,        
        order: [],
        "ajax": { "url": './../tgudang_pemesanan/get_list_barang/'+n, "type": "POST" },
        "columns": [
            {data: "id", visible: false},
            {data: "kode", width: '10%', className: 'link-kodebarang'},
            {data: "nama", width: '10%'},
            {data: "hargabeli", width: '10%',className: 'link-hargabeli'},
        ],
    })
    $('#modal-list-barang').fadeIn()

})


$('#list-barang').on( 'click', 'td', function(){
	table = $('#list-barang').DataTable()

	// distributor
	$.ajax({
		url: './../tgudang_pemesanan/get_distributor',
		success: function(data) {
			$('#iddistributor').empty().append(this,data).selectpicker('refresh')
		}
	})

    table.destroy()
    $('#idbarang').val( table.cell(this,0).data() )
    $('#namabarang').val( table.cell(this,2).data() )
    $('#hargabeli').val( table.cell(this,3).data() )
    

	$('#modal-list-barang').hide()
	$('#modal-detail-pemesanan').fadeIn()
})


$(document).on('click','#detail-pemesanan-add', function(){
    table = $('#list-barang').DataTable().destroy()

	var option 		= JSON.parse('{"1":"Alkes","2":"Implan","3":"Obat","4":"Logistik"}');
	var dataoption 	= '<option value="">Pilih Opsi</option>';
	$.each(option, function(i,item){
		dataoption += '<option value="'+i+'">'+item+'</option>';
	})

	$('#namabarang,#hargabeli,#kuantitas').val('')
	$('#idtipe').empty().append('#idtipe',dataoption).selectpicker('refresh')
    $('#iddistributor').empty().selectpicker('refresh')
})

$(document).on('click','#btnback', function(){
	table = $('#list-barang').DataTable().destroy()
	$('#modal-detail-pemesanan').fadeIn()
	$('#modal-list-barang').hide()

	var option 		= JSON.parse('{"1":"Alkes","2":"Implan","3":"Obat","4":"Logistik"}');
	var dataoption 	= '<option value="">Pilih Opsi</option>';
	$.each(option, function(i,item){
		dataoption += '<option value="'+i+'">'+item+'</option>';
	})

	$('#namabarang,#hargabeli,#kuantitas').val('')
    $('#idtipe').empty().append('#idtipe',dataoption).selectpicker('refresh')
	$('#iddistributor').empty().selectpicker('refresh')
})



$(document).on( 'click', '#submit-form_detail', function(){

    var validate = validateDetail();
    if(!validate) {
        $.toaster({priority : 'danger', title : 'Error!', message : 'Data gagal ditambahkan'});
        return false;
    }

    var table = "<tr>";
    table += "<td>"+$("#idtipe :selected").text()+"</td>"; //0
    table += "<td>"+$("#namabarang").val()+"</td>"; //1
    table += "<td>"+$("#iddistributor :selected").text()+"</td>"; //2
    table += "<td>"+$("#hargabeli").val()+"</td>"; //3
    table += "<td>"+$("#kuantitas").val()+"</td>"; //4
    table += "<td><a href='#' class='on-default detail_remove' title='Remove'><i class='fa fa-trash-o'></i></a></td>"; //5

    table += "<td hidden>"+$("#idtipe :selected").val()+"</td>"; //6
    table += "<td hidden>"+$("#idbarang").val()+"</td>"; //7
    table += "<td hidden>"+$("#iddistributor :selected").val()+"</td>"; //8

    table += "</tr>";


    $('#detail_list tbody').append(table);
    detail_value()
})

$(document).on("click",".detail_remove", function() {
    $(this).closest('tr').remove();
    detail_value()

    $.toaster({priority : 'success', title : 'Sukses!', message : 'Data berhasil dihapus'});
})



function detail_value() {
    var detail_tbl = $('table#detail_list tbody tr').get().map(function(row) {
        return $(row).find('td').get().map(function(cell) {
            return $(cell).html();
        });
    });

    $("#detail_value").val(JSON.stringify(detail_tbl))
}

function validateDetail(){
    if($("#idtipe").val() == ""){
        return false;
    }
    if($("#kodebarang").val() == ""){
        return false;
    }
    if($("#iddistributor").val() == ""){
        return false;
    }
    if($("#kuantitas").val() == ""){
        return false;
    }

    $.toaster({priority : 'success', title : 'Sukses!', message : 'Data berhasil ditambahkan'});
    return true;
}