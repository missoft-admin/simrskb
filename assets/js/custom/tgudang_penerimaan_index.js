var table;

$(document).ready(function(){
    table = $('#index_list').DataTable({
        "pageLength": 50,  
        "serverSide": true, 
        "order": [ [0,'desc'] ],
        "ajax": { "url": './tgudang_penerimaan/ajax_list', "type": "POST" },
        "columns": [
            {"data": "nopenerimaan"},
            {"data": "nopemesanan"},
            {"data": "totalbarang"},
            {"data": "totalharga"},
            {"data": "option"},
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            if(aData['nopemesanan']){
                $("td:eq(1)", nRow).text(aData['nopemesanan']+' - '+aData['nama']);
            }
            $('td:eq(4)', nRow).html('<button class="btn btn-info btn-xs view" url="tgudang_penerimaan/view/'+aData['id']+'" data-toggle="tooltip" data-placement="top" title="view"><i class="glyphicon glyphicon-search"></i></button>');
            return nRow;            
        }        
    })
})

$(document).on('click','.view', function(){
    var url = $(this).attr('url')
    window.location = url
})