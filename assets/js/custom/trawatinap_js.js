$(document).ready(function(){

    /**
     * Rawat Inap JQUERY.
     * Developer @RendyIchtiarSaputra
     */

	$("#kelompokpasien").change(function(){
    var kp = $("#kelompokpasien").val();
    if(kp == "1"){
      $("#rekanan").css("display","block");
      $("#bpjskesehatan").css("display","none");
      $("#bpjstenagakerja").css("display","none");
      $("#jp").css("display","block");
      $.ajax({
        url:'./../trawatinap_pendaftaran/getRekanan',
        dataType:"json",
        success:function(data){
          $("#rekanan1").empty();
          $("#rekanan1").append("<option>Pilih Opsi</option>");
          for(var i = 0;i < data.rekanan.length;i++){
            $("#rekanan1").append("<option value='"+data.rekanan[i].id+"'>"+data.rekanan[i].nama+"</option>");
          }
          $("#rekanan1").selectpicker("refresh");
        }
      });
    }
    else if(kp == "3"){
      $("#bpjskesehatan").css("display","block");
      $("#rekanan").css("display","none");
      $("#bpjstenagakerja").css("display","none");
      $("#jp").css("display","block");
      $.ajax({
        url:'./../trawatinap_pendaftaran/getBpjsKesehatan',
        dataType:"json",
        success:function(data){
          $("#bpjskesehatan1").empty();
          $("#bpjskesehatan1").append("<option>Pilih Opsi</option>");
          for(var i = 0;i < data.bpjskesehatan.length;i++){
            $("#bpjskesehatan1").append("<option value='"+data.bpjskesehatan[i].id+"'>"+data.bpjskesehatan[i].kode+"</option>");
          }
          $("#bpjskesehatan1").selectpicker("refresh");
        }
      });
    }
    else if(kp == "4"){
      $("#bpjstenagakerja").css("display","block");
      $("#bpjskesehatan").css("display","none");
      $("#rekanan").css("display","none");
      $("#jp").css("display","block");
      $.ajax({
        url:'./../trawatinap_pendaftaran/getBpjsTenaga',
        dataType:"json",
        success:function(data){
          $("#bpjstenagakerja1").empty();
          $("#bpjstenagakerja1").append("<option>Pilih Opsi</option>");
          for(var i = 0;i < data.bpjstenaga.length;i++){
            $("#bpjstenagakerja1").append("<option value='"+data.bpjstenaga[i].id+"'>"+data.bpjstenaga[i].id+"</option>");
          }
          $("#bpjstenagakerja1").selectpicker("refresh");
        }
      });
    }
    else if(kp == "0"){
      $("#rekanan").css("display","none");
      $("#bpjskesehatan").css("display","none");
      $("#bpjstenagakerja").css("display","none");
      $("#jp").css("display","none");
    }
    else{
      $("#rekanan").css("display","none");
      $("#bpjskesehatan").css("display","none");
      $("#bpjstenagakerja").css("display","none");
      $("#jp").css("display","block");
    }
  });

	$("#jenispasien").change(function(){
    var kelompok = $("#kelompokpasien").val();
    var jenispasien = $("#jenispasien").val();
    if(jenispasien == "1"){
      $("#kelompok1").css("display","none");
    }else{
      $("#kelompok1").css("display","block");
      $.ajax({
        url:'./../trawatinap_pendaftaran/getKelompok',
        dataType:"json",
        method:"POST",
        data:{"kelompok":kelompok},
        success:function(data){
          $("#kelompokpasien1").empty();
          $("#kelompokpasien1").append("<option>Pilih Opsi</option>");
          for(var i = 0;i < data.kelompok.length;i++){
            $("#kelompokpasien1").append("<option value='"+data.kelompok[i].id+"'>"+data.kelompok[i].nama+"</option>");
          }
          $("#kelompokpasien1").selectpicker('refresh');
        }
      });
    }
  });

  $("#kelompokpasien1").change(function(){
    var kp = $("#kelompokpasien1").val();
    if(kp == "1"){
      $("#rekanan2").css("display","block");
      $("#bpjskesehatan2").css("display","none");
      $("#bpjstenagakerja2").css("display","none");
      $.ajax({
        url:'./../trawatinap_pendaftaran/getRekanan',
        dataType:"json",
        success:function(data){
          $("#rekanan3").empty();
          $("#rekanan3").append("<option>Pilih Opsi</option>");
          for(var i = 0;i < data.rekanan.length;i++){
            $("#rekanan3").append("<option value='"+data.rekanan[i].id+"'>"+data.rekanan[i].nama+"</option>");
          }
          $("#rekanan3").selectpicker("refresh");
        }
      });
    }
    else if(kp == "3"){
      $("#bpjskesehatan2").css("display","block");
      $("#rekanan2").css("display","none");
      $("#bpjstenagakerja2").css("display","none");
      $.ajax({
        url:'./../trawatinap_pendaftaran/getBpjsKesehatan',
        dataType:"json",
        success:function(data){
          $("#bpjskesehatan3").empty();
          $("#bpjskesehatan3").append("<option>Pilih Opsi</option>");
          for(var i = 0;i < data.bpjskesehatan.length;i++){
            $("#bpjskesehatan3").append("<option value='"+data.bpjskesehatan[i].id+"'>"+data.bpjskesehatan[i].kode+"</option>");
          }
          $("#bpjskesehatan3").selectpicker("refresh");
        }
      });
    }
    else if(kp == "4"){
      $("#bpjstenagakerja2").css("display","block");
      $("#bpjskesehatan2").css("display","none");
      $("#rekanan2").css("display","none");
      $.ajax({
        url:'./../trawatinap_pendaftaran/getBpjsTenaga',
        dataType:"json",
        success:function(data){
          $("#bpjstenagakerja3").empty();
          $("#bpjstenagakerja3").append("<option>Pilih Opsi</option>");
          for(var i = 0;i < data.bpjstenaga.length;i++){
            $("#bpjstenagakerja3").append("<option value='"+data.bpjstenaga[i].id+"'>"+data.bpjstenaga[i].id+"</option>");
          }
          $("#bpjstenagakerja3").selectpicker("refresh");
        }
      });
    }
    else{
      $("#rekanan").css("display","none");
      $("#bpjskesehatan").css("display","none");
      $("#bpjstenagakerja").css("display","none");
    }
  });

  $("#kelas").change(function(){
  	var ruangan = $("#ruangan").val();
  	var kelas = $("#kelas").val();
  	$.ajax({
  		url:"./../trawatinap_pendaftaran/getBed",
  		method:"POST",
  		dataType:"json",
  		data:{ruangan:ruangan,kelas:kelas},
  		success:function(data){
  			$("#bed").empty();
  			$("#bed").append("<option>Pilih Opsi</option");
  			for(var i = 0;i < data.length;i++){
  				$("#bed").append("<option value='"+data[i].id+"'>"+data[i].nama+"</option>");
  			}
  			$("#bed").selectpicker('refresh');
  		}
  	});
  });

  $("#ruangan").change(function(){
        $.ajax({
          url:"./../trawatinap_pendaftaran/getKelas",
          dataType:"json",
          success:function(data){
            $("#kelas").empty();
            $("#kelas").append("<option>Pilih Opsi</option>");
            for(var i = 0;i < data.length;i++){
              $("#kelas").append("<option value='"+data[i].id+"'>"+data[i].nama+"</option>");
            }
            $("#kelas").selectpicker('refresh');
            $("#bed").empty();
            $("#bed").append("<option>Pilih Opsi</option>");
          }
        });
      });

      $("#tambahTindakan").click(function(){
        $("#aksiModal").hide();
      });
      $("#closeTambahModal").click(function(){
        $("#aksiModal").show();
      });
      $("#cariTindakan").click(function(){
        $("#cariTindakanModal").show();
        $("#tambahTindakanModal").hide();
      });
      $("#tambahJasaTindakan").click(function(){
        $("#tambahTindakanModal").show();
      });
      $('input[name="quantity"]').bind('keypress', function(e){
        var keyCode = (e.which)?e.which:event.keyCode
        return !(keyCode>31 && (keyCode<48 || keyCode>57));
      });

      $("#quantityModal").keyup(function(){
        var tarif = $("#tarifModal").val();
        var quantity = $("#quantityModal").val();
        var total = tarif * quantity;
        $("#jumlahModal").val(total);
    });

});


//Klik Cari Pasien Auto
$(document).on("click", ".selectPasien", function() {
      var nopendaftaran = ($(this).closest('tr').find("td:eq(1)").html());
      // alert(nomedrecpasien);
      $('#nomedrec').val(($(this).closest('tr').find("td:eq(2)").html()));
      $('#nama').val(($(this).closest('tr').find("td:eq(3)").html()));
      $('#alamat').val(($(this).closest('tr').find("td:eq(4)").html()));
      $('#noregister').val(($(this).closest('tr').find("td:eq(1)").html()));
      $('#rujukan').val(($(this).closest('tr').find("td:eq(5)").html()));
      $.ajax({
        url:"./../trawatinap_pendaftaran/getDataPasien/"+nopendaftaran,
        method:"POST",
        dataType:"json",
        success:function(data){
          $('#umur').val(data[0].umur_tahun+" Th "+data[0].umur_bulan+" Bln "+data[0].umur_hari+" Hr");
          $('#rujukans').val(data[0].idtipe);
          if(data[0].idtipe == '1'){
            $('#polix').empty();
            $('#polix').append('<div class="form-group" style="margin-bottom: 5px;"><label class="col-md-4 control-label" for="tglpendaftaran">Poliklinik<span style="color:red;">*</span></label><div class="col-md-8"><input type="text" class="form-control" placeholder="Poliklinik" id="poliklinik" required readonly="true"></div></div>');
            $('#poliklinik').val(data[0].namapoliklinik);
            $('#dokterPerujuk').val("Dokter Luar");
            $.ajax({
              url:"./../trawatinap_pendaftaran/getDokterPoli",
              method:"POST",
              dataType:"json",
              success:function(data){
                $("#dokterpenanggungjawab").empty();
                $("#dokterpenanggungjawab").append("<option value=''>Pilih Opsi</option>");
                for(var i = 0;i < data.length;i++){
                  $("#dokterpenanggungjawab").append("<option value='"+data[i].iddokter+"'>"+data[i].nama+"</option>");
                }
                $("#dokterpenanggungjawab").selectpicker('refresh');
              }
            });
          }else{
            $('#polix').empty();
            $('#dokterPerujuk').val(data[0].namadokter);
            var dokter = data[0].id_dokter;
            $.ajax({
              url:"./../trawatinap_pendaftaran/getDokterIgd/"+dokter,
              method:"POST",
              dataType:"json",
              success:function(data){
                $("#dokterpenanggungjawab").empty();
                $("#dokterpenanggungjawab").append("<option>Pilih Opsi</option>");
                for(var i = 0;i < data.length;i++){
                  $("#dokterpenanggungjawab").append("<option value='"+data[i].iddokter+"'>"+data[i].nama+"</option>");
                }
                $("#dokterpenanggungjawab").selectpicker('refresh');
              }
            });
          }

            $.ajax({ //History Daftar Pasien
                url:'./../trawatinap_pendaftaran/getHistory/'+data[0].idpasien,
                dataType:'json',
                success:function(data){
                  $('.push').empty();
                  for(var i = 0;i < data.length;i++){
                    var push = '<li>';
                        push += '<i class="si si-pencil text-info"></i>';
                        push += '<div class="font-w600">Rawat Inap | ODS - '+data[i].tipe+'</div>';
                        push += '<div><a href="javascript:void(0)">Dokter - '+data[i].namadokter+'</a></div>';
                        push += '<div><small class="text-muted">Tanggal - '+data[i].tanggaldaftar+'</small></div>';
                        push += '</li>';
                    $('.push').append(push);
                  }
                }
            });
        }
      });

      return false;
    });

// TINDAKAN
function number_format (number, decimals, decPoint, thousandsSep) {
      number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
      var n = !isFinite(+number) ? 0 : +number
      var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
      var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
      var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
      var s = ''

      var toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec)
        return '' + (Math.round(n * k) / k)
          .toFixed(prec)
      }

       s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
       if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
       }
       if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
       }
     return s.join(dec)
    }

		$(document).on("click",".detail_remove", function() {

      swal({
      title: "Are you sure ?",
      text : "Are you sure that you want to delete this pasien ?",
      type : "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, delete it!",
      confirmButtonColor: "#ec6c62",
      });
      $(this).closest('tr').remove();
		});

   //    $(this).closest('tr').remove();
      // tindakanCalculate();
      // bhpCalculate();
      // visiteCalculate();
      // // $.toaster({priority : 'success', title : 'Sukses!', message : 'Data berhasil dihapus'});
   //    swal("Good job!", "Data Berhasil Dihapus", "success");
