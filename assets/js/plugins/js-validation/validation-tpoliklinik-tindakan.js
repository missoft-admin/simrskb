/*
 *  Document   : validation-tpoliklinik-tindakan.js
 *  Author     : Deni Purnama
 *  Description: Custom JS code used in Form Validation Page
 */

var BaseFormValidation = function() {

    var ValidationDeni = function(){
        jQuery('.js-validation-deni').validate({
            ignore: [],
            errorClass: 'help-block animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-validnya > div').append(error);
            },
            highlight: function(e) {
                var elem = jQuery(e);

                elem.closest('.form-validnya').removeClass('has-error').addClass('has-error');
                elem.closest('.help-block').remove();
            },
            success: function(e) {
                var elem = jQuery(e);

                elem.closest('.form-validnya').removeClass('has-error');
                elem.closest('.help-block').remove();
            },
            rules: {
                'select-alkes': {
                    required: true
                },
                'select-obat': {
                    required: true
                },
                'tindak_lanjut': {
                    required: true
                },
                'sebab_luar': {
                    required: true
                },
                'menolak_dirawat': {
                    required: true
                },
                'keluhan': {
                    required: true,
                    minlength: 5
                },
                'keterangan': {
                    required: true,
                    minlength: 5
                },
                'diagnosa': {
                    required: true,
                    minlength: 5
                },
                'alergi_obat': {
                    required: true,
                    minlength: 5
                },
                'tinggi_badan': {
                    required: true,
                    number: true
                },
                'suhu_badan': {
                    required: true,
                    number: true
                },
                'berat_badan': {
                    required: true,
                    number: true
                },
                'lingkar_kepala': {
                    required: true,
                    number: true
                },
                'sistole': {
                    required: true,
                    number: true
                },
                'disastole': {
                    required: true,
                    number: true
                },
                'kasus': {
                    required: true
                }
            },
            // Pesan error validasi
            messages: {
                'select-obat': 'Anda harus pilih salah satu!',
                'select-alkes': 'Anda harus pilih salah satu!',
                'tindak_lanjut': 'Anda harus pilih salah satu!',
                'sebab_luar': 'Anda harus pilih salah satu!',
                'menolak_dirawat': 'Anda harus pilih salah satu!',
                'keluhan': 'Apa keluhannya?',
                'diagnosa': 'Apa diagnosanya?',
                'alergi_obat': 'Apa alergi obatnya?',
                'keterangan': 'Apa keterangannya?',
                'tinggi_badan': 'Hanya angka saja!',
                'suhu_badan': 'Hanya angka saja!',
                'berat_badan': 'Hanya angka saja!',
                'lingkar_kepala': 'Hanya angka saja!',
                'sistole': 'Hanya angka saja!',
                'disastole': 'Hanya angka saja!',
                'kasus': 'Anda harus pilih salah satu!'
            }
        });
    };


    return {
        init: function () {
            // Panggil fungsi untuk Forms Validation
            ValidationDeni();

            // Validation jika Select2 change
            // jQuery('.js-select2').on('change', function(){
            //     jQuery(this).valid();
            // });
        }
    };
}();

// Meng-Analisa jika page diload
jQuery(function(){ BaseFormValidation.init(); });
