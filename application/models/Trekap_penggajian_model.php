<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trekap_penggajian_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('trekap_penggajian.id', $id);
        $query = $this->db->get('trekap_penggajian');
        return $query->row();
    }

    public function getDetailData($id)
    {
        $this->db->select('trekap_penggajian_detail.*, mvariable_rekapan.nama, mvariable_rekapan.idtipe');
        $this->db->join('mvariable_rekapan', 'mvariable_rekapan.id = trekap_penggajian_detail.idvariable');
        $this->db->where('trekap_penggajian_detail.idrekap', $id);
        $this->db->where('trekap_penggajian_detail.idsub', 0);
        $query = $this->db->get('trekap_penggajian_detail');
        return $query->result();
    }

    public function getReviewData($id)
    {
        $this->db->select('trekap_penggajian_detail.*,
          (
            CASE WHEN trekap_penggajian_detail.idsub != 0 THEN
              mvariable_rekapan_sub.nama
            ELSE
              mvariable_rekapan.nama
            END
          ) AS nama,
          mvariable_rekapan.idtipe,
          mvariable_rekapan.idsub AS subrekapan');
        $this->db->join('mvariable_rekapan', 'mvariable_rekapan.id = trekap_penggajian_detail.idvariable', 'LEFT');
        $this->db->join('mvariable_rekapan_sub', 'mvariable_rekapan_sub.id = trekap_penggajian_detail.idsub', 'LEFT');
        $this->db->where('trekap_penggajian_detail.idrekap', $id);
        $this->db->order_by('trekap_penggajian_detail.idvariable');
        $this->db->order_by('trekap_penggajian_detail.idsub');
        $query = $this->db->get('trekap_penggajian_detail');
        return $query->result();
    }

    public function getAkunData($id, $idtipeakun, $idtipevariable)
    {
        $this->db->select('trekap_penggajian_akun.*, makun_nomor.namaakun');
        $this->db->join('makun_nomor', 'makun_nomor.noakun = trekap_penggajian_akun.noakun');
        $this->db->where('trekap_penggajian_akun.idvariable', $id);
        $this->db->where('trekap_penggajian_akun.tipeakun', $idtipeakun);
        $this->db->where('trekap_penggajian_akun.tipevariable', $idtipevariable);
        $query = $this->db->get('trekap_penggajian_akun');
        return $query->result();
    }

    public function getVariableRekapan($idtipe)
    {
        if ($idtipe != 0) {
            $this->db->where('idtipe', $idtipe);
        }
        $this->db->where('status', 1);
        $query = $this->db->get('mvariable_rekapan');
        return $query->result();
    }

    public function saveData()
    {
        $this->tahun  = $_POST['tahun'];
        $this->bulan  = $_POST['bulan'];
        $this->subyek = $_POST['subyek'];

        if ($this->db->insert('trekap_penggajian', $this)) {
            $idrekap = $this->db->insert_id();

            $detaillist = json_decode($_POST['detail_value']);
            foreach ($detaillist as $row) {
                $data = array();
                $data['idrekap']    = $idrekap;
                $data['idvariable'] = $row[0];


                if ($this->db->insert('trekap_penggajian_detail', $data)) {
                    $this->saveDataDetail($idrekap, $row[0]);
                }
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function saveDataDetail($idrekap, $idvariable)
    {
        $this->db->where('mvariable_rekapan_sub.idvariable', $idvariable);
        $query = $this->db->get('mvariable_rekapan_sub');
        $result = $query->result();

        foreach ($result as $row) {
            $data = array();
            $data['idrekap']    = $idrekap;
            $data['idvariable'] = $row->idvariable;
            $data['idsub']      = $row->id;

            $this->db->insert('trekap_penggajian_detail', $data);
        }

        return true;
    }

    public function updateData()
    {
        $this->tahun  = $_POST['tahun'];
        $this->bulan  = $_POST['bulan'];
        $this->subyek = $_POST['subyek'];

        if ($this->db->update('trekap_penggajian', $this, array('id' => $_POST['id']))) {
            $idrekap = $_POST['id'];

            $this->db->where('idrekap', $idrekap);
            if ($this->db->delete('trekap_penggajian_detail')) {
                $detaillist = json_decode($_POST['detail_value']);
                foreach ($detaillist as $row) {
                    $data = array();
                    $data['idrekap']    = $idrekap;
                    $data['idvariable'] = $row[0];

                    $this->db->insert('trekap_penggajian_detail', $data);
                }
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
        return $status;
    }

    public function updateReview()
    {
        $detaillist = json_decode($_POST['detail_value']);
        foreach ($detaillist as $row) {
            $data = array();
            $data['nominal'] = RemoveComma($row[2]);

            $this->db->where('id', $row[0]);
            $this->db->update('trekap_penggajian_detail', $data);
        }

        return true;
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('trekap_penggajian', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
