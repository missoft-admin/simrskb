<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mdata_piutang_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $q="SELECT 
			*
			from mdata_piutang M
					
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	public function list_akun(){
	   $q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		return $list;
   }
    public function saveData()
    {
        $this->nama 	= $_POST['nama'];		
        $this->idakun 	= $_POST['idakun'];		
        $this->nominal_default 			= RemoveComma($_POST['nominal_default']);
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mdata_piutang', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama 	= $_POST['nama'];
		$this->idakun 	= $_POST['idakun'];	
        $this->nominal_default 			= RemoveComma($_POST['nominal_default']);
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mdata_piutang', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mdata_piutang', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mdata_piutang', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
