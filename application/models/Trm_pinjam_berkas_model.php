<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Trm_pinjam_berkas_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

	public function js_pasien(){
		$search=$this->input->post('search');
		
		$q="SELECT id,CONCAT(no_medrec,' - ',title,'. ',nama) as text FROM mfpasien 
			WHERE (nama LIKE '%".$search."%' OR no_medrec LIKE '%".$search."%' ) LIMIT 100";
		$query=$this->db->query($q);
		$result= $query->result();
		
		return $result;
			
	}
	public function get_pasien($id){
		$q="SELECT DATE_FORMAT(tanggal_lahir,'%d-%m-%Y') as tanggal_lahir,alamat_jalan,nama,no_medrec,title FROM mfpasien 
			WHERE id='$id'";
		$query=$this->db->query($q);
		$result= $query->row();
		
		return $result;
			
	}
	public function get_pasien_detail($id){
		$q="SELECT no_medrec,title,nama,tanggal_lahir,alamat_jalan FROM mfpasien WHERE id='$id'";
		$query=$this->db->query($q);
		$result= $query->row();
		
		return $result;
			
	}
	
	public function get_dokter_pegawai(){
		$search=$this->input->post('search');
		$q="SELECT *from (
			SELECT D.id,CONCAT(D.nama,' [DOKTER]') as nama,'1' as tipe from mdokter D
			
			UNION
			SELECT P.id,CONCAT(P.nama,' [PEGAWAI]') as nama, '2' tipe From mpegawai P 
			) TBL WHERE nama LIKE '%".$search."%'";
		$query=$this->db->query($q);
		$result= $query->result();
		
		return $result;
			
	}
	public function list_dokter(){
		$q="SELECT id,nama from mdokter WHERE `STATUS`='1'";
		$query=$this->db->query($q);
		return $query->result();
	}
	
	//BATAS DELETE
	
}
