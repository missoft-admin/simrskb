<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mdokter_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getSpecified($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('mdokter');
		return $query->row();
	}

	public function getSpecifiedHeader($id)
	{
		$q = "SELECT H.id,H.nama,MD.nama as kategori FROM `mdokter` H
			LEFT JOIN mdokter_kategori MD ON MD.id=H.idkategori
			WHERE H.id='$id'";
		$query = $this->db->query($q);
		return $query->row_array();
	}

	public function getSpecifiedSettingPembayaran($id)
	{
		$this->db->where('id', $id);
		$this->db->where('status', 1);
		$query = $this->db->get('mdokter_pembayaran');
		return $query->row();
	}

	public function getListSettingPembayaran($iddokter)
	{
		$this->db->select('mdokter_pembayaran.*, COALESCE(user_updated.name, user_created.name) AS updated_by,
            COALESCE(mdokter_pembayaran.updated_at, mdokter_pembayaran.created_at) AS updated_at');
		$this->db->join('musers user_created', 'user_created.id = mdokter_pembayaran.created_by', 'LEFT');
		$this->db->join('musers user_updated', 'user_updated.id = mdokter_pembayaran.updated_by', 'LEFT');
		$this->db->where('mdokter_pembayaran.iddokter', $iddokter);
		$this->db->where('mdokter_pembayaran.status', 1);
		$query = $this->db->get('mdokter_pembayaran');
		return $query->result();
	}

	public function saveData()
	{
		$this->idkategori = $_POST['idkategori'];
		$this->nip = $_POST['nip'];
		$this->nama = $_POST['nama'];
		$this->jeniskelamin = $_POST['jeniskelamin'];
		$this->alamat = $_POST['alamat'];
		$this->tempatlahir = $_POST['tempatlahir'];
		$this->tanggallahir = YMDFormat($_POST['tanggallahir']);
		$this->telepon = $_POST['telepon'];
		$this->email = $_POST['email'];
		$this->npwp = $_POST['npwp'];
		$this->pajak = $_POST['pajak'];
		$this->potonganrspagi = $_POST['potonganrspagi'];
		$this->potonganrssiang = $_POST['potonganrssiang'];
		$this->potonganusg = $_POST['potonganusg'];
		$this->potonganrsranap = $_POST['potonganrsranap'];
		$this->potonganrsranapsiang = $_POST['potonganrsranapsiang'];
		$this->pajakranap = $_POST['pajakranap'];
		$this->potonganrsods = $_POST['potonganrsods'];
		$this->potonganrsodssiang = $_POST['potonganrsodssiang'];
		$this->potonganfeers = $_POST['potonganfeers'];
		$this->potonganfeerspribadi = $_POST['potonganfeerspribadi'];
		$this->pajakods = $_POST['pajakods'];
		$this->nominaltransport = RemoveComma($_POST['nominaltransport']);
		$this->persentaseperujuk = $_POST['persentaseperujuk'];
		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');

		if ($this->uploadPhoto()) {
			$this->db->insert('mdokter', $this);
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function updateData()
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form, ['12'])) {
			$this->idkategori = $_POST['idkategori'];
		}

		$this->nip = $_POST['nip'];
		$this->nama = $_POST['nama'];
		$this->jeniskelamin = $_POST['jeniskelamin'];
		$this->alamat = $_POST['alamat'];
		$this->tempatlahir = $_POST['tempatlahir'];
		$this->tanggallahir = YMDFormat($_POST['tanggallahir']);
		$this->telepon = $_POST['telepon'];
		$this->email = $_POST['email'];
		$this->npwp = $_POST['npwp'];
		$this->pajak = $_POST['pajak'];
		$this->potonganrspagi = $_POST['potonganrspagi'];
		$this->potonganrssiang = $_POST['potonganrssiang'];
		$this->potonganusg = $_POST['potonganusg'];
		$this->potonganrsranap = $_POST['potonganrsranap'];
		$this->potonganrsranapsiang = $_POST['potonganrsranapsiang'];
		$this->pajakranap = $_POST['pajakranap'];
		$this->potonganrsods = $_POST['potonganrsods'];
		$this->potonganrsodssiang = $_POST['potonganrsodssiang'];
		$this->potonganfeers = $_POST['potonganfeers'];
		$this->potonganfeerspribadi = $_POST['potonganfeerspribadi'];
		$this->pajakods = $_POST['pajakods'];
		$this->nominaltransport = RemoveComma($_POST['nominaltransport']);
		$this->persentaseperujuk = $_POST['persentaseperujuk'];
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');

		if ($this->uploadPhoto(true)) {
			$this->db->update('mdokter', $this, ['id' => $_POST['id']]);
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function changeStatus($id, $status)
	{
		$this->status = $status;
		$this->deleted_by = $this->session->userdata('user_id');
		$this->deleted_date = date('Y-m-d H:i:s');

		if ($this->db->update('mdokter', $this, ['id' => $id])) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function saveSettingPembayaran()
	{
		$this->iddokter = $_POST['iddokter'];
		$this->tanggal = $_POST['tanggal'];
		$this->jatuh_tempo = $_POST['jatuh_tempo'];
		$this->created_at = date('Y-m-d H:i:s');
		$this->created_by = $this->session->userdata('user_id');

		if ($this->db->insert('mdokter_pembayaran', $this)) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function updateSettingPembayaran()
	{
		$this->iddokter = $_POST['iddokter'];
		$this->tanggal = $_POST['tanggal'];
		$this->jatuh_tempo = $_POST['jatuh_tempo'];
		$this->updated_at = date('Y-m-d H:i:s');
		$this->updated_by = $this->session->userdata('user_id');

		if ($this->db->update('mdokter_pembayaran', $this, ['id' => $_POST['id']])) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function deleteSetting($id, $status)
	{
		$this->status = $status;
		$this->deleted_by = $this->session->userdata('user_id');
		$this->deleted_at = date('Y-m-d H:i:s');

		if ($this->db->update('mdokter_pembayaran', $this, ['id' => $id])) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function uploadPhoto($update = false)
	{
		if (!file_exists('assets/upload/dokter')) {
			mkdir('assets/upload/dokter', 0755, true);
		}

		if (isset($_FILES['foto'])) {
			if ($_FILES['foto']['name'] != '') {
				$config['upload_path'] = './assets/upload/dokter';
				$config['allowed_types'] = 'jpg|jpeg|png|bmp';
				$config['encrypt_name'] = true;

				$this->upload->initialize($config);

				if ($this->upload->do_upload('foto')) {
					$image_upload = $this->upload->data();
					$this->foto = $image_upload['file_name'];

					if ($update == true) {
						$this->removeImage($_POST['id']);
					}

					return true;
				} else {
					$this->error_message = $this->upload->display_errors();
					print_r($this->error_message);
					exit();
					return false;
				}
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	public function removeImage($id)
	{
		$row = $this->getSpecified($id);
		if (file_exists('./assets/upload/dokter/' . $row->foto) && $row->foto != '') {
			unlink('./assets/upload/dokter/' . $row->foto);
		}
	}
}
