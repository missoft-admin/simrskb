
<?php
defined('BASEPATH') || exit('No direct script access allowed');

class Mpemeriksaan_radiologi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->select('merm_pemeriksaan_radiologi.*,
            merm_referensi.ref AS tarif_radiologi_tipe,
            mtarif_radiologi.nama AS tarif_radiologi_nama');
        $this->db->join('mtarif_radiologi', 'mtarif_radiologi.id = merm_pemeriksaan_radiologi.tarif_radiologi_id');
        $this->db->join('merm_referensi', 'merm_referensi.nilai = mtarif_radiologi.idtipe AND merm_referensi.ref_head_id = "245" AND merm_referensi.status = "1"');
        $this->db->where('merm_pemeriksaan_radiologi.id', $id);
        $query = $this->db->get('merm_pemeriksaan_radiologi');

        return $query->row();
    }

    public function saveData()
    {
        $this->tipe_pemeriksaan = $_POST['tipe_pemeriksaan'];
        $this->jenis_pemeriksaan = $_POST['jenis_pemeriksaan'];
        $this->anatomi_tubuh = $_POST['anatomi_tubuh'];
        $this->kode = $_POST['kode'];
        $this->nama_pemeriksaan = $_POST['nama_pemeriksaan'];
        $this->nama_pemeriksaan_eng = $_POST['nama_pemeriksaan_eng'];
        $this->posisi_pemeriksaan = $_POST['posisi_pemeriksaan'];
        $this->urutan_tampil = $_POST['urutan_tampil'];
        $this->tarif_radiologi_id = $_POST['tarif_radiologi_id'];

        if ($this->db->insert('merm_pemeriksaan_radiologi', $this)) {
            return true;
        } else {
            $this->error_message = 'Penyimpanan Gagal';
            return false;
        }
    }

    public function updateData()
    {
        $this->tipe_pemeriksaan = $_POST['tipe_pemeriksaan'];
        $this->jenis_pemeriksaan = $_POST['jenis_pemeriksaan'];
        $this->anatomi_tubuh = $_POST['anatomi_tubuh'];
        $this->kode = $_POST['kode'];
        $this->nama_pemeriksaan = $_POST['nama_pemeriksaan'];
        $this->nama_pemeriksaan_eng = $_POST['nama_pemeriksaan_eng'];
        $this->posisi_pemeriksaan = $_POST['posisi_pemeriksaan'];
        $this->urutan_tampil = $_POST['urutan_tampil'];
        $this->tarif_radiologi_id = $_POST['tarif_radiologi_id'];

        if ($this->db->update('merm_pemeriksaan_radiologi', $this, ['id' => $_POST['id']])) {
            return true;
        } else {
            $this->error_message = 'Penyimpanan Gagal';
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('merm_pemeriksaan_radiologi', $this, ['id' => $id])) {
            return true;
        } else {
            $this->error_message = 'Penyimpanan Gagal';
            return false;
        }
    }

    public function getTarifPemeriksaan($tarif_id)
    {
        $this->db->select('mtarif_radiologi_detail.*, mkelas.nama AS kelas');
        $this->db->join('mkelas', 'mkelas.id = mtarif_radiologi_detail.kelas');
        $this->db->where('mtarif_radiologi_detail.idtarif', $tarif_id);
        return $this->db->get('mtarif_radiologi_detail')->result();
    }
}
