<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tklaim_umur_model extends CI_Model
{
  
   public function list_kelompok_pasien(){
	   $q="SELECT id,nama from mpasien_kelompok 
				WHERE mpasien_kelompok.`status`='1' AND id !='5'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function get_nama_asuransi($idkelompokpasien,$idrekanan){
	   if ($idkelompokpasien=='1'){
		   $q="SELECT M.nama FROM `mrekanan` M WHERE M.id='$idrekanan'";
		   
	   }else{
			$q="SELECT M.nama FROM `mpasien_kelompok` M WHERE M.id='$idkelompokpasien'";
	   
	   }
	  
		$query=$this->db->query($q);
		return $query->row('nama');
   }
   public function list_rekanan(){
	   $q="SELECT id,nama from mrekanan 
				WHERE mrekanan.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   function get_total_piutang($jenis_rekanan,$idkelompokpasien,$idrekanan,$bulan){
	   $where1='';
	   if ($jenis_rekanan=='1'){//ASURANSI
			$where1 .="AND H.idrekanan='$idrekanan'";			
		}else{
			$where1 .="AND H.idkelompokpasien='$idkelompokpasien'";	
		}
		if ($bulan <12){
			$where1 .=" AND (ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30)=".$bulan.")";
		
		}elseif($bulan==12){
			$where1 .="AND (ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30)=".$bulan.")";
		}elseif($bulan==13){
			$where1 .="AND ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) >12 AND ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) <24";
		}elseif($bulan==24){
			$where1 .="AND (ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) > 24)";
		}
		$q="SELECT SUM(H.tidak_terbayar) as sisa from tklaim H WHERE H.status_kirim='1' ".$where1;
		return $this->db->query($q)->row('sisa');
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
