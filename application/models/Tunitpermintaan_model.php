<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tunitpermintaan_model extends CI_Model
{
    public function dtpermintaan()
    {
        $this->load->library('Datatables');
        $this->datatables->select('id, nopermintaan, dariunit, keunit, totalpesan, totalalih, totalkirim, status');
        $this->datatables->from('v_index_tunit_permintaan');
        return print($this->datatables->generate());
    }

    public function dtpermintaan_detail()
    {
        $id = $this->uri->segment(3);
        if ($id) {
            $this->load->library('Datatables');
            $this->datatables->select('
				tdet.id,
				tdet.idtipe,
				tdet.idbarang,
				up1.nama as keunit,
				up2.nama as dariunit,
				tdet.kuantitas,
				tdet.kuantitassisa,
				tdet.kuantitaskirim,
				thead.idunitpeminta,
				thead.idunitpenerima,
			');
            $this->datatables->join('tunit_permintaan thead', 'thead.id = tdet.idpermintaan', 'left');
            $this->datatables->join('munitpelayanan up1', 'thead.idunitpeminta = up1.id', 'left');
            $this->datatables->join('munitpelayanan up2', 'thead.idunitpenerima = up2.id', 'left');
            $this->datatables->where('tdet.idpermintaan', $id);
            $this->datatables->from('tunit_permintaan_detail tdet');
            return print($this->datatables->generate());
        }
    }

    public function dtpermintaan_detail_penerimaan()
    {
        $id = $this->uri->segment(3);
        if ($id) {
            $this->load->library('Datatables');
            $this->datatables->select('
				tdet.id,
				tdet.idtipe,
				tdet.idbarang,
				up2.nama as dariunit,
				up1.nama as keunit,
				tdet.kuantitas,
				tdet.kuantitassisa,
				tdet.kuantitaskirim,
				tdet.kuantitasalih,
				thead.idunitpeminta,
				thead.idunitpenerima,
			');
            $this->datatables->join('tunit_permintaan thead', 'thead.id = tdet.idpermintaan', 'left');
            $this->datatables->join('munitpelayanan up1', 'thead.idunitpeminta = up1.id', 'left');
            $this->datatables->join('munitpelayanan up2', 'thead.idunitpenerima = up2.id', 'left');
            $this->datatables->where('tdet.idpermintaan', $id);
            $this->datatables->from('tunit_permintaan_detail tdet');
            return print($this->datatables->generate());
        }
    }

    public function dtbarang()
    {
        $idtipe = $this->uri->segment(3);
        if ($idtipe) {
            $table = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
            $tipe = array(null, 'Alkes','Implan','Obat','Logistik');
            $this->load->library('Datatables');
            $this->datatables->add_column('tipe', $tipe[$idtipe]);
            $this->datatables->select('id, kode, nama');
            $this->datatables->from($table[$idtipe]);
            return print($this->datatables->generate());
        }
    }

    public function selectdariunit()
    {
        $this->db->select('id, nama as text');
        $res = $this->db->get('munitpelayanan')->result();
        $this->output->set_output(json_encode($res));
    }
    public function selectdariunit_alihan()
    {
        $idunitdari = $this->uri->segment(3);
        $this->db->select('id, nama as text');
        $this->db->where('id !=', $idunitdari);
        $res = $this->db->get('munitpelayanan')->result();
        $this->output->set_output(json_encode($res));
    }

    public function ajax_stokunit()
    {
        $idunit = $this->input->get('idunit');
        $idtipe = $this->input->get('idtipe');
        $idbarang = $this->input->get('idbarang');
        if ($idunit && $idtipe && $idbarang) {
            $this->db->where('idunitpelayanan', $idunit);
            $this->db->where('idtipe', $idtipe);
            $this->db->where('idbarang', $idbarang);
            $row = $this->db->get('mgudang_stok', 1);
            if ($row->num_rows() > 0) {
                $res = $row->row()->stok;
            } else {
                $res = 0;
            }
            $this->output->set_output(json_encode($res));
        }
    }

    public function selectkeunit()
    {
        $idunitdari = $this->uri->segment(3);
        $this->db->select('id, nama as text');
        $this->db->where('id !=', 0);
        $this->db->where('id !=', $idunitdari);
        $res = $this->db->get('munitpelayanan')->result();
        $this->output->set_output(json_encode($res));
    }

    public function save()
    {
        $detailvalue = json_decode($this->input->post('detailvalue'));

        $get_unique = array_map('unserialize', array_unique(array_map(function ($arr) {
            return serialize(array(0 => $arr[9], 1 => $arr[10]));
        }, $detailvalue)));

        foreach ($get_unique as $row_reff) {
            $totalbarang           = 0;

            $this->db->set('nopermintaan', '-');
            $this->db->set('idunitpeminta', $row_reff[0]);
            $this->db->set('idunitpenerima', $row_reff[1]);
            $this->db->set('status', 1);

            if ($this->db->insert('tunit_permintaan')) {
                $idpermintaan = $this->db->insert_id();

                foreach ($detailvalue as $r) {
                    if ($row_reff[0] == $r[9]) {
                        $this->db->set('idpermintaan', $idpermintaan);
                        $this->db->set('idtipe', $r[7]);
                        $this->db->set('idbarang', $r[8]);
                        $this->db->set('kuantitas', $r[5]);
                        $this->db->set('kuantitaskirim', 0);
                        $this->db->set('kuantitassisa', $r[5]);
                        $this->db->set('kuantitasalih', 0);
                        $this->db->set('status', 1);
                        if ($this->db->insert('tunit_permintaan_detail')) {
                            $totalbarang = $totalbarang + RemoveComma($r[5]);
                        }
                    }
                }
            }

            // update total transaksi
            $this->db->set('totalbarang', $totalbarang);
            $this->db->where('id', $idpermintaan);
            if ($this->db->update('tunit_permintaan')) {
                $status = true;
            } else {
                $status = false;
            }
        }
        return $status;
    }

    public function ajax_namabarang()
    {
        $idtipe = $this->input->post('idtipe');
        $idbarang = $this->input->post('idbarang');
        if ($idtipe && $idbarang) {
            $table = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
            $this->db->select('nama');
            $this->db->where('id', $idbarang);
            $res = $this->db->get($table[$idtipe], 1)->row()->nama;
            $this->output->set_output(json_encode($res));
        }
    }

    public function save_acc_pengiriman()
    {
        $idpermintaan = $this->uri->segment(3);
        if ($idpermintaan) {
            $id 			= $this->input->post('id');
            $kuantitaskirim = $this->input->post('kuantitaskirim');
            foreach ($id as $key => $val) {
                $this->db->set('kuantitaskirim', $kuantitaskirim[$key]);
                $this->db->where('id', $id[$key]);
                $this->db->where('idpermintaan', $idpermintaan);
                $this->db->update('tunit_permintaan_detail');
            }
            return true;
        }
    }

    public function save_pengalihan_permintaan()
    {
        $iddaripermintaandetail = $this->input->post('iddaripermintaandetail');
        $idtipe = $this->input->post('idtipe');
        $idbarang = $this->input->post('idbarang');
        $kuantitasalih = $this->input->post('kuantitasalih');
        $dariunit = $this->input->post('dariunit');
        $keunit = $this->input->post('keunit');

        $this->db->set('kuantitasalih', $kuantitasalih);
        $this->db->where('idtipe', $idtipe);
        $this->db->where('idbarang', $idbarang);
        $this->db->where('id', $iddaripermintaandetail);
        if ($this->db->update('tunit_permintaan_detail')) {
            $this->db->set('nopermintaan', '-');
            $this->db->set('idunitpeminta', $keunit);
            $this->db->set('idunitpenerima', $dariunit);
            $this->db->set('totalbarang', $kuantitasalih);
            $this->db->set('status', 1);
            if ($this->db->insert('tunit_permintaan')) {
                $idpermintaan = $this->db->insert_id();
                $this->db->set('idpermintaan', $idpermintaan);
                $this->db->set('idtipe', $idtipe);
                $this->db->set('idbarang', $idbarang);
                $this->db->set('kuantitas', $kuantitasalih);
                $this->db->set('kuantitaskirim', 0);
                $this->db->set('kuantitassisa', $kuantitasalih);
                $this->db->set('kuantitasalih', 0);
                $this->db->set('status', 1);
                $this->db->insert('tunit_permintaan_detail');
            }
            return true;
        }
    }

    public function ajax_pernah_terima()
    {
        $idpermintaan = $this->input->post('idpermintaan');
        $idtipe = $this->input->post('idtipe');
        $idbarang = $this->input->post('idbarang');

        $this->db->select('COALESCE( SUM(tdet.kuantitas), 0 ) AS diterima');
        $this->db->join('tunit_penerimaan thead', 'thead.id = tdet.idpenerimaan', 'left');
        $this->db->where('thead.idpermintaan', $idpermintaan);
        $this->db->where('tdet.idbarang', $idtipe);
        $this->db->where('tdet.idtipe', $idbarang);
        $res = $this->db->get('tunit_penerimaan_detail tdet', 1)->row()->diterima;
        $this->output->set_output(json_encode($res));
    }

    public function save_penerimaan()
    {
        $idpermintaan = $this->uri->segment(3);
        if ($idpermintaan) {
            $idtipe 			= $this->input->post('idtipe');
            $idbarang 			= $this->input->post('idbarang');
            $kuantitasterima 	= $this->input->post('kuantitasterima');

            $totalbarang = 0;
            $this->db->set('nopenerimaan', '-');
            $this->db->set('idpermintaan', $idpermintaan);
            $this->db->set('status', 1);
            if ($this->db->insert('tunit_penerimaan')) {
                $idpenerimaan = $this->db->insert_id();

                foreach ($idtipe as $key => $val) {
                    $this->db->set('idpenerimaan', $idpenerimaan);
                    $this->db->set('idtipe', $idtipe[$key]);
                    $this->db->set('idbarang', $idbarang[$key]);
                    $this->db->set('kuantitas', $kuantitasterima[$key]);
                    $this->db->set('status', 1);
                    $this->db->insert('tunit_penerimaan_detail');

                    $totalbarang = $totalbarang + $kuantitasterima[$key];
                }

                $this->db->set('totalbarang', $totalbarang);
                $this->db->where('id', $idpenerimaan);
                if ($this->db->update('tunit_penerimaan')) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
}

/* End of file Tunitpermintaan_model.php */
/* Location: ./application/models/Tunitpermintaan_model.php */
