<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Lvalidasi_jurnal_umum_model extends CI_Model
{
   public function list_akun(){
	   $q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		return $list;
   }
   public function getHeader($id){
	   $q="SELECT * from tvalidasi_hutang 
				WHERE tvalidasi_hutang.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   
   function get_nama_akun($id){
	   $q="SELECT *from makun_nomor A WHERE A.id='$id'";
	   return $this->db->query($q)->row();
   }
   function get_nama_tipe($id){
	   $q="SELECT nama from ref_trx_jurnal_umum A WHERE A.id='$id'";
	   return $this->db->query($q)->row('nama');
   }
   public function list_jenis(){
	   $q="SELECT id,nama from msetting_jurnal_umum_rekon M
				WHERE M.status='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
