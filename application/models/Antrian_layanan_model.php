<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Antrian_layanan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function list_hari_null(){
		$q="SELECT '' as id,H.kodehari,H.namahari,0 as st_24_jam,'' as jam_buka,'' as jam_tutup,0 as auto_open,0 as kuota_harian,0 as status FROM merm_hari H";
		return $this->db->query($q)->result();
	}
	public function list_hari($id){
		$q="SELECT * FROM antrian_pelayanan_hari H WHERE H.antrian_id='$id'";
		return $this->db->query($q)->result();
	}
	public function list_sound(){
		$q="SELECT * FROM antrian_asset_sound H ORDER BY id";
		return $this->db->query($q)->result();
	}
    public function list_head()
    {
        $q="SELECT 
			*
			from antrian_pelayanan M
					
			WHERE M.status='1'";
        $query = $this->db->query($q);
        return $query->result();
    }
	public function getSpecified($id)
    {
        $q="SELECT 
			*
			from antrian_pelayanan M
					
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	
    public function saveData()
    {
		// print_r($this->input->post());exit;
        $this->kode 	= $_POST['kode'];		
        $this->nama_pelayanan 	= $_POST['nama_pelayanan'];		
        $this->urutan 			= ($_POST['urutan']);
        $this->kuota 			= ($_POST['kuota']);
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('antrian_pelayanan', $this)) {
			$antrian_id=$this->db->insert_id();
			$kodehari=$this->input->post('kodehari');
			$namahari=$this->input->post('namahari');
			$jam_buka=$this->input->post('jam_buka');
			$jam_tutup=$this->input->post('jam_tutup');
			$st_24_jam=$this->input->post('st_24_jam');
			$kuota_harian=$this->input->post('kuota_harian');
			$status=$this->input->post('status');
			$auto_open=$this->input->post('auto_open');
			foreach($kodehari as $index=>$val){
				$data['antrian_id']=$antrian_id;
				$data['kodehari']=$val;
				$data['namahari']=$namahari[$val];
				$data['jam_buka']=$jam_buka[$val];
				$data['jam_tutup']=$jam_tutup[$val];
				$data['kuota_harian']=$kuota_harian[$val];
				$data['st_24_jam']=(isset($st_24_jam[$val]))?1:0;
				$data['auto_open']=(isset($auto_open[$val]))?1:0;
				$data['status']=(isset($status[$val]))?1:0;
				$this->db->insert('antrian_pelayanan_hari',$data);
			}
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->kode 	= $_POST['kode'];		
        $this->nama_pelayanan 	= $_POST['nama_pelayanan'];		
        $this->urutan 			= ($_POST['urutan']);
        $this->kuota 			= ($_POST['kuota']);
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('antrian_pelayanan', $this, array('id' => $_POST['id']))) {
			$id_det=$this->input->post('id_det');
			$kodehari=$this->input->post('kodehari');
			$namahari=$this->input->post('namahari');
			$jam_buka=$this->input->post('jam_buka');
			$jam_tutup=$this->input->post('jam_tutup');
			$st_24_jam=$this->input->post('st_24_jam');
			$kuota_harian=$this->input->post('kuota_harian');
			$status=$this->input->post('status');
			$auto_open=$this->input->post('auto_open');
			foreach($kodehari as $index=>$val){
				$data['antrian_id']=$_POST['id'];
				$data['kodehari']=$val;
				$data['namahari']=$namahari[$val];
				$data['jam_buka']=$jam_buka[$val];
				$data['jam_tutup']=$jam_tutup[$val];
				$data['kuota_harian']=$kuota_harian[$val];
				$data['st_24_jam']=(isset($st_24_jam[$val]))?1:0;
				$data['auto_open']=(isset($auto_open[$val]))?1:0;
				$data['status']=(isset($status[$val]))?1:0;
				$this->db->where('id',$id_det[$val]);
				$this->db->update('antrian_pelayanan_hari',$data);
			}
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('antrian_pelayanan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('antrian_pelayanan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
