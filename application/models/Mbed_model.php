<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mbed_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {
        $this->db->select('mbed.*, mruangan.nama AS namaruangan, mkelas.nama AS namakelas');
        $this->db->join('mruangan', 'mruangan.id = mbed.idruangan');
        $this->db->join('mkelas', 'mkelas.id = mbed.idkelas');
        $this->db->order_by('id', 'desc');
        $this->db->get('mbed');
        return $this->db->last_query();
    }

    public function getAllRuangan()
    {
        $this->db->where('status', '1');
        $query = $this->db->get('mruangan');
        return $query->result();
    }

    public function getAllKelas()
    {
        $this->db->where('status', '1');
        $query = $this->db->get('mkelas');
        return $query->result();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mbed');
        return $query->row();
    }

    public function saveData()
    {
        $this->idruangan   = $_POST['idruangan'];
        $this->idkelas	   = $_POST['idkelas'];
        $this->nama		     = $_POST['nama'];
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		
		
        if ($this->db->insert('mbed', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->idruangan   = $_POST['idruangan'];
        $this->idkelas     = $_POST['idkelas'];
        $this->nama        = $_POST['nama'];
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
        if ($this->db->update('mbed', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function changeStatus($id, $status)
    {
        $this->status = $status;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
        if ($this->db->update('mbed', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
