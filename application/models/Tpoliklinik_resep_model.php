<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpoliklinik_resep_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function get_telaah($assesmen_id,$var='st_resep'){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *FROM tpoliklinik_e_resep_telaah WHERE assesmen_id='$assesmen_id'";
		$data=$this->db->query($q)->result();
		if (!$data){
			$q="INSERT INTO tpoliklinik_e_resep_telaah
				(assesmen_id,nourut_no,nourut,lev,nama,opsi_nilai,st_resep,st_obat,mtelaah_id,opsi_default,jawaban_resep,jawaban_obat,created_date,created_ppa)

				SELECT '$assesmen_id', nourut_no,nourut,lev,nama,opsi_nilai,st_resep,st_obat,mtelaah_id,opsi_default
				,CASE WHEN T.st_resep=1 THEN T.opsi_default ELSE null END as jawaban_resep 
				,CASE WHEN T.st_obat=1 THEN T.opsi_default ELSE null END as jawaban_obat 
				,NOW(),'$login_ppa_id'
				FROM (
					SELECT H.id as id_det,H.nourut as nourut_no,LPAD(H.nourut,3,'0') as nourut, '0' as lev,H.nama,'' as opsi_nilai,'' as st_resep,'' as st_obat,H.id as mtelaah_id,'' as  opsi_default FROM mtelaah H
					WHERE H.staktif='1'
					UNION ALL
					SELECT D.id as id_det,D.nourut as nourut_no,CONCAT(LPAD(H.nourut,3,'0'),'-',LPAD(D.nourut,3,'0')) as nourut, '1' as lev,D.nama_detail as nama,D.opsi_jawab as opsi_nilai,D.st_resep as st_resep,D.st_obat as st_obat,D.mtelaah_id 
					,D.opsi_default
					FROM  mtelaah H
					INNER JOIN mtelaah_detail D ON D.mtelaah_id=H.id
					WHERE H.staktif='1' AND D.staktif='1'
				) T 
				ORDER BY T.nourut ASC";
			$this->db->query($q);
				$q="SELECT *FROM (
						SELECT H.* FROM tpoliklinik_e_resep_telaah H
						INNER JOIN tpoliklinik_e_resep_telaah D ON D.assesmen_id=H.assesmen_id AND SUBSTR(D.nourut,1,3)=H.nourut AND D.st_resep='1'
						WHERE H.assesmen_id='$assesmen_id' AND H.lev='0'
						GROUP BY H.id

						UNION ALL

						SELECT *FROM tpoliklinik_e_resep_telaah H
						WHERE H.assesmen_id='$assesmen_id' AND H.lev='1' AND H.st_resep='1'
					) T ORDER BY T.nourut ASC";
			
		}else{
			$q="SELECT *FROM (
						SELECT H.* FROM tpoliklinik_e_resep_telaah H
						INNER JOIN tpoliklinik_e_resep_telaah D ON D.assesmen_id=H.assesmen_id AND SUBSTR(D.nourut,1,3)=H.nourut AND D.st_resep='1'
						WHERE H.assesmen_id='$assesmen_id' AND H.lev='0'
						GROUP BY H.id

						UNION ALL

						SELECT *FROM tpoliklinik_e_resep_telaah H
						WHERE H.assesmen_id='$assesmen_id' AND H.lev='1' AND H.st_resep='1'
					) T ORDER BY T.nourut ASC";
		}
		$data=$this->db->query($q)->result();
		return $data;
	}
	function get_telaah_obat($assesmen_id,$var='st_obat'){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *FROM tpoliklinik_e_resep_telaah WHERE assesmen_id='$assesmen_id'";
		$data=$this->db->query($q)->result();
		if (!$data){
			$q="INSERT INTO tpoliklinik_e_resep_telaah
				(assesmen_id,nourut_no,nourut,lev,nama,opsi_nilai,st_resep,st_obat,mtelaah_id,opsi_default,jawaban_resep,jawaban_obat,created_date,created_ppa)

				SELECT '$assesmen_id', nourut_no,nourut,lev,nama,opsi_nilai,st_resep,st_obat,mtelaah_id,opsi_default
				,CASE WHEN T.st_resep=1 THEN T.opsi_default ELSE null END as jawaban_resep 
				,CASE WHEN T.st_obat=1 THEN T.opsi_default ELSE null END as jawaban_obat 
				,NOW(),'$login_ppa_id'
				FROM (
					SELECT H.id as id_det,H.nourut as nourut_no,LPAD(H.nourut,3,'0') as nourut, '0' as lev,H.nama,'' as opsi_nilai,'' as st_resep,'' as st_obat,H.id as mtelaah_id,'' as  opsi_default FROM mtelaah H
					WHERE H.staktif='1'
					UNION ALL
					SELECT D.id as id_det,D.nourut as nourut_no,CONCAT(LPAD(H.nourut,3,'0'),'-',LPAD(D.nourut,3,'0')) as nourut, '1' as lev,D.nama_detail as nama,D.opsi_jawab as opsi_nilai,D.st_resep as st_resep,D.st_obat as st_obat,D.mtelaah_id 
					,D.opsi_default
					FROM  mtelaah H
					INNER JOIN mtelaah_detail D ON D.mtelaah_id=H.id
					WHERE H.staktif='1' AND D.staktif='1'
				) T 
				ORDER BY T.nourut ASC";
			$this->db->query($q);
				$q="
					SELECT *FROM (
						SELECT H.* FROM tpoliklinik_e_resep_telaah H
						INNER JOIN tpoliklinik_e_resep_telaah D ON D.assesmen_id=H.assesmen_id AND SUBSTR(D.nourut,1,3)=H.nourut AND D.st_obat='1'
						WHERE H.assesmen_id='$assesmen_id' AND H.lev='0'
						GROUP BY H.id

						UNION ALL

						SELECT *FROM tpoliklinik_e_resep_telaah H
						WHERE H.assesmen_id='$assesmen_id' AND H.lev='1' AND H.st_obat='1'
					) T ORDER BY T.nourut ASC
				";
			
		}else{
			$q="SELECT *FROM (
						SELECT H.* FROM tpoliklinik_e_resep_telaah H
						INNER JOIN tpoliklinik_e_resep_telaah D ON D.assesmen_id=H.assesmen_id AND SUBSTR(D.nourut,1,3)=H.nourut AND D.st_obat='1'
						WHERE H.assesmen_id='$assesmen_id' AND H.lev='0'
						GROUP BY H.id

						UNION ALL

						SELECT *FROM tpoliklinik_e_resep_telaah H
						WHERE H.assesmen_id='$assesmen_id' AND H.lev='1' AND H.st_obat='1'
					) T ORDER BY T.nourut ASC";
		}
		$data=$this->db->query($q)->result();
		return $data;
	}
	public function get_data_assesmen_trx($assesmen_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT 
			UT.nama as nama_telaah,
			UP.nama as nama_proses,
			H.*,T.nopenjualan

			FROM tpoliklinik_e_resep H 
			LEFT JOIN  mppa UT ON UT.id=H.user_telaah_resep
			LEFT JOIN  mppa UP ON UP.id=H.user_proses
			LEFT JOIN tpasien_penjualan T ON T.id=H.idpenjualan
			WHERE assesmen_id='$assesmen_id'";
		 $hasil=$this->db->query($q)->row_array();
		// print_r($hasil);exit;
		return $hasil;
	}
	public function get_data_assesmen($pendaftaran_id,$tipe_rj_ri='1'){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		if ($tipe_rj_ri=='3'){
			$q="SELECT * FROM tpoliklinik_e_resep 
			WHERE pendaftaran_id_ranap='$pendaftaran_id' AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}else{
			$q="SELECT * FROM tpoliklinik_e_resep 
			WHERE pendaftaran_id='$pendaftaran_id' AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	function get_dianosa_utama($pendaftaran_id,$tipe_poli){
		$q="SELECT *FROM (
				SELECT 1 as tipe_poli,H.diagnosa_utama FROM tpoliklinik_asmed H WHERE H.status_assemen='2' AND H.pendaftaran_id='$pendaftaran_id'
				UNION ALL
				SELECT 2 as tipe_poli,H.diagnosa_utama FROM tpoliklinik_asmed_igd H WHERE H.status_assemen='2' AND H.pendaftaran_id='$pendaftaran_id'
				) T WHERE T.tipe_poli='$tipe_poli' LIMIT 1
				";
		$data=$this->db->query($q)->row('diagnosa_utama');
		if ($data){
			return $data;
		}else{
			return '';
			
		}
	}
	function get_dianosa_utama_ranap($pendaftaran_id){
		$q="SELECT H.diagnosa FROM `trawatinap_pendaftaran` H WHERE H.id='$pendaftaran_id'
				";
		$data=$this->db->query($q)->row('diagnosa');
		if ($data){
			return $data;
		}else{
			return '';
			
		}
	}
	function list_tujuan_eresep($asal_pasien,$idpoli_kelas,$iddokter_ppa){
		$q="SELECT M.* FROM (
			SELECT 1 as lev,H.tujuan_id FROM mtujuan_farmasi_logic H
			WHERE H.jenis_order='1' AND H.asal_pasien='$asal_pasien' AND H.idpoli_kelas='$idpoli_kelas' AND H.iddokter_ppa='$iddokter_ppa'
			UNION ALL
			SELECT 2 as lev,H.tujuan_id FROM mtujuan_farmasi_logic H
			WHERE H.jenis_order='1' AND H.asal_pasien='$asal_pasien' AND H.idpoli_kelas='$idpoli_kelas' AND H.iddokter_ppa='0'
			UNION ALL
			SELECT 3 as lev,H.tujuan_id FROM mtujuan_farmasi_logic H
			WHERE H.jenis_order='1' AND H.asal_pasien='$asal_pasien' AND H.idpoli_kelas='0' AND H.iddokter_ppa='0'
			) T 
			INNER JOIN mtujuan_farmasi M ON M.id=T.tujuan_id
			GROUP BY T.tujuan_id";
		return $this->db->query($q)->result();
	}
	function list_tujuan_eresep_all(){
		$q="SELECT * FROM mtujuan_farmasi WHERE status='1'";
		return $this->db->query($q)->result();
	}
	public function setting_label(){
		$q="SELECT 
		pesan_informasi,judul_ina,judul_eng,waktu_ina,waktu_eng,waktu_req,tujuan_ina,tujuan_eng,tujuan_req,dokter_ina,dokter_eng,dokter_req
		,bb_ina,bb_eng,bb_req,tb_ina,tb_eng,tb_req,luas_ina,luas_eng,luas_req,diagnosa_ina,diagnosa_eng,diagnosa_req,menyusui_ina,menyusui_eng,menyusui_req
		,gangguan_ina,gangguan_eng,gangguan_req,puasa_ina,puasa_eng,puasa_req,alergi_ina,alergi_eng,alergi_req,alergi_detail_ina,alergi_detail_eng,alergi_detail_req
		,prioritas_ina,prioritas_eng,prioritas_req
		,pulang_ina,pulang_eng,pulang_req,catatan_ina,catatan_eng,catatan_req,ket_ina,ket_eng,ket_req

		FROM setting_eresep_label";
		return $this->db->query($q)->row_array();
	}
	public function getListTipe()
	{
		$iduser=$this->session->userdata('user_id');
		$q="SELECT H.idtipe as id,M.nama_tipe  as nama
		FROM musers_tipebarang H 
		INNER JOIN mdata_tipebarang M ON M.id=H.idtipe
		WHERE H.iduser='$iduser' AND M.id NOT IN (4)";
		$query=$this->db->query($q);
		return $query->result();
	}
	public function list_ppa($login_profesi_id)
	{
		$iduser=$this->session->userdata('user_id');
		$q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='$login_profesi_id' AND H.staktif='1'";
		$query=$this->db->query($q);
		return $query->result();
	}
	public function list_ppa_all()
	{
		$iduser=$this->session->userdata('user_id');
		$q="SELECT *FROM mppa H WHERE H.staktif='1'";
		$query=$this->db->query($q);
		return $query->result();
	}
	public function get_obat($searchText)
    {
        $this->db->select("view_barang_farmasi.*,mgudang_stok.stok");
        $this->db->from('view_barang_farmasi');
        $this->db->join('mgudang_stok', 'mgudang_stok.idbarang=view_barang_farmasi.id AND mgudang_stok.idunitpelayanan=1 AND mgudang_stok.idtipe=view_barang_farmasi.idtipe');
        $this->db->like('view_barang_farmasi.nama', $searchText);
        $this->db->or_like('view_barang_farmasi.kode', $searchText);
        $query = $this->db->get();
        // print_r($this->db->last_query());exit();
        return $query->result_array();
    }
	public function getDefaultUnitPelayananUser()
	{
		$this->db->select('unitpelayananiddefault as idunitpelayanan');
		$this->db->from('musers');
		$this->db->where('id', $this->session->userdata('user_id'));
		$query = $this->db->get();
		return $query->row('idunitpelayanan');
	}
	public function get_obat_detail($id, $kelompok_pasien=0, $idtipe,$idunit)
    {

        $q="SELECT view_barang_farmasi.*, msatuan.singkatan, msatuan.nama as nama_satuan,(CASE
				WHEN $kelompok_pasien = 5 THEN
					marginumum
				WHEN $kelompok_pasien = 0 THEN
					marginumum
				WHEN $kelompok_pasien = 1 THEN
					marginasuransi
				WHEN $kelompok_pasien = 2 THEN
					marginjasaraharja
				WHEN $kelompok_pasien = 3 THEN
					marginbpjskesehatan
				WHEN $kelompok_pasien = 4 THEN
					marginbpjstenagakerja
			END) AS margin,mgudang_stok.stok
			 FROM view_barang_farmasi
			 INNER JOIN msatuan ON msatuan.id=view_barang_farmasi.idsatuankecil
			 INNER JOIN mgudang_stok ON mgudang_stok.idbarang = view_barang_farmasi.id AND mgudang_stok.idtipe=view_barang_farmasi.idtipe AND idunitpelayanan='$idunit'
			 WHERE view_barang_farmasi.id = '$id' AND view_barang_farmasi.idtipe='$idtipe'";
        $query=$this->db->query($q);
		// print_r($q);exit;
        return $query->row();
    }
	
	function setting_eresep(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT st_edit_catatan,lama_edit,orang_edit,st_hapus_catatan,lama_hapus,orang_hapus,st_duplikasi_catatan,lama_duplikasi,orang_duplikasi,st_iter_default,max_iter,jml_iter FROM setting_eresep H LIMIT 1";
		return $this->db->query($q)->row_array();
		

	}
	function data_paket_layanan(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT H.id as paket_id,H.nama_paket,H.status_paket,H.st_edited FROM mpaket_layanan H WHERE H.created_by='$user_id' AND H.status_paket='1' LIMIT 1";
		return $this->db->query($q)->row_array();
	}
	function list_header_path($idtipe,$idkelompokpasien,$idrekanan=0){
		$idrekanan=($idkelompokpasien==1?$idrekanan:0);
		$q="SELECT *FROM (
			SELECT  M.id as kp,0 as idrekanan,M.trawatjalan,M.tigd FROM mpasien_kelompok M
			UNION ALL
			SELECT 1 as kp,M.id as idrekanan,M.trawatjalan,M.tigd FROM mrekanan M WHERE (M.trawatjalan > 0 AND M.tigd > 0) 
			) T WHERE T.kp='$idkelompokpasien' AND (T.idrekanan='0' OR T.idrekanan='$idrekanan')

			ORDER BY T.idrekanan DESC";
		$row=$this->db->query($q)->row();
		$idlayanan='';
		if ($idtipe=='1'){
			$idlayanan=$row->trawatjalan;
		}else{
			$idlayanan=$row->tigd;
		}
		// print_r($row);exit;
		$q="SELECT *FROM mtarif_rawatjalan WHERE id='$idlayanan'";
		return $this->db->query($q)->result();
	}
}
