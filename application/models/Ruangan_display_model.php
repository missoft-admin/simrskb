<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ruangan_display_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function get_index_setting($id){
		$q="SELECT *
			 FROM ruangan_display
			WHERE id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_sound($display_id){
		$q="SELECT *FROM ruangan_display_sound H WHERE H.display_id='$display_id' AND H.status='1' ORDER BY H.nourut ASC";
		return $this->db->query($q)->result();
	}
	function list_counter(){
		$q="SELECT *FROM antrian_pelayanan_counter H WHERE H.status=1 ORDER BY H.nama_counter ASC";
		return $this->db->query($q)->result();
	}
	function save_general(){
		$id = $this->input->post('id');
		
		$this->footer = $this->input->post('footer');
		$this->bg_color = $this->input->post('bg_color');
		$this->judul_header = $this->input->post('judul_header');
		$this->judul_sub_header = $this->input->post('judul_sub_header');
		$this->alamat = $this->input->post('alamat');
		$this->telepone = $this->input->post('telepone');
		$this->email = $this->input->post('email');
		$this->website = $this->input->post('website');
		$this->upload_header_logo(true);
		// print_r($this);exit;	
		if ($id){
			$this->db->where('id', $id);
			if ($this->db->update('ruangan_display', $this)) {
				return true;
			} else {
				return false;
			}
		}else{
			if ($this->db->insert('ruangan_display', $this)) {
				return true;
			} else {
				return false;
			}
		}
		
			
	}
	 public function softDelete($id)
    {
        $this->status = 0;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('ruangan_display', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('ruangan_display', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function upload_header_logo($update = false)
    {
        if (!file_exists('assets/upload/logo_setting')) {
            mkdir('assets/upload/logo_setting', 0755, true);
        }

        if (isset($_FILES['header_logo'])) {
            if ($_FILES['header_logo']['name'] != '') {
                $config['upload_path'] = './assets/upload/logo_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('header_logo')) {
                    $image_upload = $this->upload->data();
                    $this->header_logo = $image_upload['file_name'];

                    if ($update == true) {
                        $this->remove_image_header_logo($this->input->post('id'));
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
			// print_r('TIDAK ADA');exit;
                return true;
            }
					// print_r($this->foto);exit;
        } else {
			// print_r('TIDAK ADA');exit;
            return true;
        }
		
    }
	
	public function remove_image_header_logo($id)
    {
		$q="select header_logo From ruangan_display H WHERE H.id='$id'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/logo_setting/'.$row->header_logo) && $row->header_logo !='') {
            unlink('./assets/upload/logo_setting/'.$row->header_logo);
        }
    }
	public function hapus_sound($id){
		$this->remove_sound($id);
		$this->status=0;
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		$this->db->where('id',$id);
		return $this->db->update('ruangan_display_sound',$this);
		
	}
	
	function get_file_name($id){
		$q="select file_name from ruangan_display_video where id='$id'
			";
		return $this->db->query($q)->row();
	}
}
