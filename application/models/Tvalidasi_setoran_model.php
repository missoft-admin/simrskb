<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tvalidasi_setoran_model extends CI_Model
{
   public function list_rekanan(){
	   $q="SELECT id,nama from mrekanan 
				WHERE mrekanan.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_kp(){
	   $q="SELECT id,nama from mpasien_kelompok 
				WHERE mpasien_kelompok.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getHeader($id){
	   $q="SELECT tvalidasi_setoran.* from tvalidasi_setoran 
				WHERE tvalidasi_setoran.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function saveData(){
	   $id=($this->input->post('id'));
		$iddet_bayar=($this->input->post('iddet_bayar'));
		$idakun=($this->input->post('idakun'));
		$iddet=($this->input->post('iddet'));
		$idakun_tunai=($this->input->post('idakun_tunai'));

		$st_posting=($this->input->post('st_posting'));
		$btn_simpan=($this->input->post('btn_simpan'));
		foreach($iddet as $index => $val){
			
			$detail=array(
				'idakun_tunai'=>$idakun_tunai[$index],
				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_setoran',$detail);
		}
		foreach($iddet_bayar as $index => $val){
			
			$detail=array(
				'idakun'=>$idakun[$index],
				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_setoran_bayar',$detail);
		}
		if ($st_posting=='1'){
			$this->db->where('id',$id);
			$this->db->update('tvalidasi_setoran',array('st_posting'=>0));
		}
		if ($btn_simpan=='2'){
			$data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $id);
			$result=$this->db->update('tvalidasi_setoran', $data);
		}
		
		return true;
   }
   function get_nama_akun($id){
	   $q="SELECT *from makun_nomor A WHERE A.id='$id'";
	   return $this->db->query($q)->row();
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
