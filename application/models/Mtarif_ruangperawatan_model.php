<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mtarif_ruangperawatan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllTarif($id)
    {
        $this->db->where('idtarif', $id);
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_ruangperawatan_detail');
        return $query->result();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mtarif_ruangperawatan');
        return $query->row();
    }

    public function saveData()
    {
        $this->idtipe          = $_POST['idtipe'];
        $this->nama            = $_POST['nama'];
        $this->status          = 1;
        $this->created_by  = $this->session->userdata('user_id');
        $this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mtarif_ruangperawatan', $this)) {
            $idtarif = $this->db->insert_id();
            $tariflist = json_decode($_POST['totaltarif_value']);
            foreach ($tariflist as $row) {
                $detail = array();
                $detail['idtarif']          = $idtarif;
                $detail['kelas']            = $row[1];
                $detail['jasasarana']       = RemoveComma($row[2]);
                $detail['jasapelayanan']    = RemoveComma($row[3]);
                $detail['bhp']              = RemoveComma($row[4]);
                $detail['biayaperawatan']   = RemoveComma($row[5]);
                $detail['total']            = RemoveComma($row[6]);
                $detail['status']           = '1';

                $this->db->insert('mtarif_ruangperawatan_detail', $detail);
            }
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->idtipe          = $_POST['idtipe'];
        $this->nama            = $_POST['nama'];
        $this->status          = 1;
        $this->edited_by  = $this->session->userdata('user_id');
        $this->edited_date  = date('Y-m-d H:i:s');
        if ($this->db->update('mtarif_ruangperawatan', $this, array('id' => $_POST['id']))) {
            $idtarif = $_POST['id'];
            $tariflist = json_decode($_POST['totaltarif_value']);

            $this->db->where('idtarif', $idtarif);
            if ($this->db->delete('mtarif_ruangperawatan_detail')) {
                foreach ($tariflist as $row) {
                    $detail = array();
                    $detail['idtarif']          = $idtarif;
                    $detail['kelas']            = $row[1];
                    $detail['jasasarana']       = RemoveComma($row[2]);
                    $detail['jasapelayanan']    = RemoveComma($row[3]);
                    $detail['bhp']              = RemoveComma($row[4]);
                    $detail['biayaperawatan']   = RemoveComma($row[5]);
                    $detail['total']            = RemoveComma($row[6]);
                    $detail['status']           = '1';

                    $this->db->insert('mtarif_ruangperawatan_detail', $detail);
                }
            }
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
        return true;
    }

    public function softDelete($id)
    {
        $this->status = 0;
        $this->deleted_by  = $this->session->userdata('user_id');
        $this->deleted_date  = date('Y-m-d H:i:s');
        if ($this->db->update('mtarif_ruangperawatan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateSetting()
    {
        $settingList = json_decode($_POST['setting_value']);
        foreach ($settingList as $row) {
            $data = array();
            $data['group_jasasarana']     = $row[3];
            $data['group_jasapelayanan']  = $row[4];
            $data['group_bhp']            = $row[5];
            $data['group_biayaperawatan'] = $row[6];

            $this->db->update(
                'mtarif_ruangperawatan_detail',
                $data,
                array(
                'idtarif' => $row[0],
                'kelas' => $row[1]
              )
            );
        }
		$settingDiskonList = json_decode($_POST['setting_diskon_value']);
		foreach ($settingDiskonList as $row) {
			$data = array();
			$data['group_jasasarana_diskon']     = $row[3];
			$data['group_jasapelayanan_diskon']  = $row[4];
			$data['group_bhp_diskon']            = $row[5];
			$data['group_biayaperawatan_diskon'] = $row[6];

			$this->db->update('mtarif_ruangperawatan_detail', $data,
			  array(
				'idtarif' => $row[0],
				'kelas' => $row[1]
			  )
			);
		}
		$data_edit=array('group_diskon_all' => $this->input->post('group_diskon_all'));
		$this->db->where('id',$this->input->post('id'));
		$this->db->update('mtarif_ruangperawatan',$data_edit);
        return true;
    }
}
