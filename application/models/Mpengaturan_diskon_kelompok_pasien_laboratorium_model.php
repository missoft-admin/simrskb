<?php

defined('BASEPATH') || exit('No direct script access allowed');

class Mpengaturan_diskon_kelompok_pasien_laboratorium_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mpasien_kelompok');

        return $query->row();
    }

    public function getTarifPelayananHeadParent(){
        $q = "SELECT * FROM mtarif_rawatjalan WHERE level = 0 AND status = 1 ORDER BY path ASC";
        $query = $this->db->query($q);
        return $query->result();
    }
	public function getTarifPelayananHeadParent_ri(){
        $q = "SELECT * FROM mtarif_rawatinap WHERE level = 0 AND status = 1 ORDER BY path ASC";
        $query = $this->db->query($q);
        return $query->result();
    }

    public function getTarifRadiologiHeadParent($idtipe){
        if ($idtipe != 0) {
            $q = "SELECT * FROM mtarif_rawatjalan WHERE idtipe = ' . $idtipe . ' AND level = 0 AND status = 1 ORDER BY path ASC";
        } else {
            $q = "SELECT * FROM mtarif_rawatjalan WHERE level = 0 AND status = 1 ORDER BY path ASC";
        }
        $query = $this->db->query($q);
        return $query->result();
    }

    public function getSettingDiskonAll($tipe, $idkelompokpasien){
        $this->db->select('merm_pengaturan_diskon_rajal_all.id,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_all.tujuan_pasien = "0" THEN "Semua Tujuan"
            WHEN merm_pengaturan_diskon_rajal_all.tujuan_pasien = "1" THEN "Poliklinik"
            WHEN merm_pengaturan_diskon_rajal_all.tujuan_pasien = "2" THEN "Instalasi Gawat Darurat (IGD)"
        END) AS tujuan_pasien,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_all.status_tujuan_poliklinik = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_all.status_tujuan_poliklinik = "1" AND merm_pengaturan_diskon_rajal_all.tujuan_poliklinik = "0" THEN "Semua Poliklinik"
            WHEN merm_pengaturan_diskon_rajal_all.status_tujuan_poliklinik = "1" AND merm_pengaturan_diskon_rajal_all.tujuan_poliklinik != "0" THEN mpoliklinik.nama
        END) AS tujuan_poliklinik,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_all.status_pasien = "0" THEN "Semua Status"
            WHEN merm_pengaturan_diskon_rajal_all.status_pasien = "1" THEN "Pasien Baru"
            WHEN merm_pengaturan_diskon_rajal_all.status_pasien = "2" THEN "Pasien Lama"
        END) AS status_pasien,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_all.kasus = "0" THEN "Semua Kasus"
            WHEN merm_pengaturan_diskon_rajal_all.kasus = "1" THEN "Kasus Baru"
            WHEN merm_pengaturan_diskon_rajal_all.kasus = "2" THEN "Kasus Lama"
        END) AS kasus,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_all.status_diskon_rp = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_all.status_diskon_rp = "1" THEN FORMAT(merm_pengaturan_diskon_rajal_all.diskon_rp, 0)
        END) AS diskon_rp,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_all.status_diskon_persen = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_all.status_diskon_persen = "1" THEN CONCAT(FORMAT(merm_pengaturan_diskon_rajal_all.diskon_persen, 2), "%")
        END) AS diskon_persen');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = merm_pengaturan_diskon_rajal_all.tujuan_poliklinik', 'LEFT');
        $this->db->where('merm_pengaturan_diskon_rajal_all.tipe', $tipe);
        $this->db->where('merm_pengaturan_diskon_rajal_all.idkelompokpasien', $idkelompokpasien);
        $query = $this->db->get('merm_pengaturan_diskon_rajal_all');

        return $query->result();
    }

    public function getSettingDiskonAdm($tipe, $idkelompokpasien){
        $this->db->select('merm_pengaturan_diskon_rajal_adm.id,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_adm.tujuan_pasien = "0" THEN "Semua Tujuan"
            WHEN merm_pengaturan_diskon_rajal_adm.tujuan_pasien = "1" THEN "Poliklinik"
            WHEN merm_pengaturan_diskon_rajal_adm.tujuan_pasien = "2" THEN "Instalasi Gawat Darurat (IGD)"
        END) AS tujuan_pasien,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_adm.status_tujuan_poliklinik = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_adm.status_tujuan_poliklinik = "1" AND merm_pengaturan_diskon_rajal_adm.tujuan_poliklinik = "0" THEN "Semua Poliklinik"
            WHEN merm_pengaturan_diskon_rajal_adm.status_tujuan_poliklinik = "1" AND merm_pengaturan_diskon_rajal_adm.tujuan_poliklinik != "0" THEN mpoliklinik.nama
        END) AS tujuan_poliklinik,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_adm.status_pasien = "0" THEN "Semua Status"
            WHEN merm_pengaturan_diskon_rajal_adm.status_pasien = "1" THEN "Pasien Baru"
            WHEN merm_pengaturan_diskon_rajal_adm.status_pasien = "2" THEN "Pasien Lama"
        END) AS status_pasien,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_adm.kasus = "0" THEN "Semua Kasus"
            WHEN merm_pengaturan_diskon_rajal_adm.kasus = "1" THEN "Kasus Baru"
            WHEN merm_pengaturan_diskon_rajal_adm.kasus = "2" THEN "Kasus Lama"
        END) AS kasus,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_adm.jenis_tarif = "0" THEN "Semua"
            WHEN merm_pengaturan_diskon_rajal_adm.jenis_tarif = "1" THEN "Administrasi"
            WHEN merm_pengaturan_diskon_rajal_adm.jenis_tarif = "2" THEN "Cetak Kartu"
        END) AS jenis_tarif,
        mtarif_administrasi.idjenis AS idjenis_tarif_pelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_adm.tarif_pelayanan = "0" THEN "Semua"
            WHEN merm_pengaturan_diskon_rajal_adm.tarif_pelayanan != "0" THEN mtarif_administrasi.nama
        END) AS tarif_pelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_adm.status_diskon_all = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_adm.status_diskon_all = "1" THEN
                IF (merm_pengaturan_diskon_rajal_adm.tipe_diskon_all = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_adm.diskon_all, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_adm.diskon_all, 0))
        END) AS diskon_all,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_adm.status_diskon_jasasarana = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_adm.status_diskon_jasasarana = "1" THEN
                IF (merm_pengaturan_diskon_rajal_adm.tipe_diskon_jasasarana = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_adm.diskon_jasasarana, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_adm.diskon_jasasarana, 0))
        END) AS diskon_jasasarana,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_adm.status_diskon_jasapelayanan = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_adm.status_diskon_jasapelayanan = "1" THEN 
                IF (merm_pengaturan_diskon_rajal_adm.tipe_diskon_jasapelayanan = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_adm.diskon_jasapelayanan, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_adm.diskon_jasapelayanan, 0))
        END) AS diskon_jasapelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_adm.status_diskon_bhp = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_adm.status_diskon_bhp = "1" THEN 
                IF (merm_pengaturan_diskon_rajal_adm.tipe_diskon_bhp = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_adm.diskon_bhp, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_adm.diskon_bhp, 0))
        END) AS diskon_bhp,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_adm.status_diskon_biaya_perawatan = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_adm.status_diskon_biaya_perawatan = "1" THEN 
                IF (merm_pengaturan_diskon_rajal_adm.tipe_diskon_biaya_perawatan = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_adm.diskon_biaya_perawatan, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_adm.diskon_biaya_perawatan, 0))
        END) AS diskon_biaya_perawatan');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = merm_pengaturan_diskon_rajal_adm.tujuan_poliklinik', 'LEFT');
        $this->db->join('mtarif_administrasi', 'mtarif_administrasi.id = merm_pengaturan_diskon_rajal_adm.tarif_pelayanan', 'LEFT');
        $this->db->where('merm_pengaturan_diskon_rajal_adm.tipe', $tipe);
        $this->db->where('merm_pengaturan_diskon_rajal_adm.idkelompokpasien', $idkelompokpasien);
        $query = $this->db->get('merm_pengaturan_diskon_rajal_adm');

        return $query->result();
    }
	public function getSettingDiskonPelayananRanap($tipe, $idkelompokpasien){
		$q="
			SELECT 
			CASE WHEN H.idruangan='0' THEN 'Semua Ruangan' ELSE MR.nama END as nama_ruangan
			,CASE WHEN H.idkelas='0' THEN 'Semua Kelas' ELSE MK.nama END as nama_kelas
			,CASE WHEN H.tujuan_pasien='0' THEN 'Semua Tujuan' WHEN H.tujuan_pasien='1' THEN 'Rawat Inap' WHEN H.tujuan_pasien='2' THEN 'ODS' END as nama_tujuan
			,CASE WHEN H.head_parent_tarif='0' THEN 'Semua' ELSE TH.nama END as nama_header
			,CASE WHEN H.tarif_pelayanan='0' THEN 'Semua' ELSE TD.nama END as nama_tarif
			,CASE WHEN H.tarif_pelayanan='0' THEN 'Semua' ELSE TD.`level` END as level_tarif_pelayanan
			,H.* 
			FROM merm_pengaturan_diskon_ranap_pelayanan H
			LEFT JOIN mruangan MR ON MR.id=H.idruangan
			LEFT JOIN mkelas MK ON MK.id=H.idkelas
			LEFT JOIN mtarif_rawatinap TH ON TH.id=H.head_parent_tarif
			LEFT JOIN mtarif_rawatinap TD ON TD.id=H.tarif_pelayanan
			WHERE H.idkelompokpasien='$idkelompokpasien'
		";
       $query=$this->db->query($q);

        return $query->result();
    }
	public function getSettingDiskonPelayananRanapAsuransi($tipe, $idrekanan){
		$q="
			SELECT 
			CASE WHEN H.idruangan='0' THEN 'Semua Ruangan' ELSE MR.nama END as nama_ruangan
			,CASE WHEN H.idkelas='0' THEN 'Semua Kelas' ELSE MK.nama END as nama_kelas
			,CASE WHEN H.tujuan_pasien='0' THEN 'Semua Tujuan' WHEN H.tujuan_pasien='1' THEN 'Rawat Inap' WHEN H.tujuan_pasien='2' THEN 'ODS' END as nama_tujuan
			,CASE WHEN H.head_parent_tarif='0' THEN 'Semua' ELSE TH.nama END as nama_header
			,CASE WHEN H.tarif_pelayanan='0' THEN 'Semua' ELSE TD.nama END as nama_tarif
			,CASE WHEN H.tarif_pelayanan='0' THEN 'Semua' ELSE TD.`level` END as level_tarif_pelayanan
			,H.* 
			FROM merm_pengaturan_diskon_ranap_pelayanan H
			LEFT JOIN mruangan MR ON MR.id=H.idruangan
			LEFT JOIN mkelas MK ON MK.id=H.idkelas
			LEFT JOIN mtarif_rawatinap TH ON TH.id=H.head_parent_tarif
			LEFT JOIN mtarif_rawatinap TD ON TD.id=H.tarif_pelayanan
			WHERE H.idkelompokpasien='1' AND H.idrekanan='$idrekanan'
		";
       $query=$this->db->query($q);

        return $query->result();
    }
	public function getSettingDiskonRanapVisite($tipe, $idkelompokpasien){
        $this->db->select('merm_pengaturan_diskon_ranap_visite.id,
        (CASE
            WHEN merm_pengaturan_diskon_ranap_visite.tujuan_pasien = "0" THEN "Semua Tujuan"
            WHEN merm_pengaturan_diskon_ranap_visite.tujuan_pasien = "1" THEN "Rawat Inap"
            WHEN merm_pengaturan_diskon_ranap_visite.tujuan_pasien = "2" THEN "ODS"
        END) AS tujuan_pasien,
        
        (CASE
            WHEN merm_pengaturan_diskon_ranap_visite.idruangan = "0" THEN "Semua Ruangan"
            WHEN merm_pengaturan_diskon_ranap_visite.idruangan != "0" THEN mruangan.nama
        END) AS nama_ruangan,
		(CASE
            WHEN merm_pengaturan_diskon_ranap_visite.idkelas = "0" THEN "Semua Kelas"
            WHEN merm_pengaturan_diskon_ranap_visite.idkelas != "0" THEN mkelas.nama
        END) AS nama_kelas,
        (CASE
            WHEN merm_pengaturan_diskon_ranap_visite.tarif_pelayanan = "0" THEN "Semua"
            WHEN merm_pengaturan_diskon_ranap_visite.tarif_pelayanan != "0" THEN mtarif_visitedokter.nama
        END) AS tarif_pelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_ranap_visite.status_diskon_all = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_ranap_visite.status_diskon_all = "1" THEN
                IF (merm_pengaturan_diskon_ranap_visite.tipe_diskon_all = "1", CONCAT(FORMAT(merm_pengaturan_diskon_ranap_visite.diskon_all, 2), "%"), FORMAT(merm_pengaturan_diskon_ranap_visite.diskon_all, 0))
        END) AS diskon_all,
        (CASE
            WHEN merm_pengaturan_diskon_ranap_visite.status_diskon_jasasarana = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_ranap_visite.status_diskon_jasasarana = "1" THEN
                IF (merm_pengaturan_diskon_ranap_visite.tipe_diskon_jasasarana = "1", CONCAT(FORMAT(merm_pengaturan_diskon_ranap_visite.diskon_jasasarana, 2), "%"), FORMAT(merm_pengaturan_diskon_ranap_visite.diskon_jasasarana, 0))
        END) AS diskon_jasasarana,
        (CASE
            WHEN merm_pengaturan_diskon_ranap_visite.status_diskon_jasapelayanan = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_ranap_visite.status_diskon_jasapelayanan = "1" THEN 
                IF (merm_pengaturan_diskon_ranap_visite.tipe_diskon_jasapelayanan = "1", CONCAT(FORMAT(merm_pengaturan_diskon_ranap_visite.diskon_jasapelayanan, 2), "%"), FORMAT(merm_pengaturan_diskon_ranap_visite.diskon_jasapelayanan, 0))
        END) AS diskon_jasapelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_ranap_visite.status_diskon_bhp = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_ranap_visite.status_diskon_bhp = "1" THEN 
                IF (merm_pengaturan_diskon_ranap_visite.tipe_diskon_bhp = "1", CONCAT(FORMAT(merm_pengaturan_diskon_ranap_visite.diskon_bhp, 2), "%"), FORMAT(merm_pengaturan_diskon_ranap_visite.diskon_bhp, 0))
        END) AS diskon_bhp,
        (CASE
            WHEN merm_pengaturan_diskon_ranap_visite.status_diskon_biaya_perawatan = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_ranap_visite.status_diskon_biaya_perawatan = "1" THEN 
                IF (merm_pengaturan_diskon_ranap_visite.tipe_diskon_biaya_perawatan = "1", CONCAT(FORMAT(merm_pengaturan_diskon_ranap_visite.diskon_biaya_perawatan, 2), "%"), FORMAT(merm_pengaturan_diskon_ranap_visite.diskon_biaya_perawatan, 0))
        END) AS diskon_biaya_perawatan');
        $this->db->join('mruangan', 'mruangan.id = merm_pengaturan_diskon_ranap_visite.idruangan', 'LEFT');
        $this->db->join('mkelas', 'mkelas.id = merm_pengaturan_diskon_ranap_visite.idkelas', 'LEFT');
        $this->db->join('mtarif_visitedokter', 'mtarif_visitedokter.id = merm_pengaturan_diskon_ranap_visite.tarif_pelayanan', 'LEFT');
        $this->db->where('merm_pengaturan_diskon_ranap_visite.tipe', $tipe);
        $this->db->where('merm_pengaturan_diskon_ranap_visite.idkelompokpasien', $idkelompokpasien);
        $query = $this->db->get('merm_pengaturan_diskon_ranap_visite');

        return $query->result();
    }

    public function getSettingDiskonPelayanan($tipe, $idkelompokpasien){
        $this->db->select('merm_pengaturan_diskon_rajal_pelayanan.id,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_pelayanan.tujuan_pasien = "0" THEN "Semua Tujuan"
            WHEN merm_pengaturan_diskon_rajal_pelayanan.tujuan_pasien = "1" THEN "Poliklinik"
            WHEN merm_pengaturan_diskon_rajal_pelayanan.tujuan_pasien = "2" THEN "Instalasi Gawat Darurat (IGD)"
        END) AS tujuan_pasien,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_tujuan_poliklinik = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_tujuan_poliklinik = "1" AND merm_pengaturan_diskon_rajal_pelayanan.tujuan_poliklinik = "0" THEN "Semua Poliklinik"
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_tujuan_poliklinik = "1" AND merm_pengaturan_diskon_rajal_pelayanan.tujuan_poliklinik != "0" THEN mpoliklinik.nama
        END) AS tujuan_poliklinik,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_pasien = "0" THEN "Semua Status"
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_pasien = "1" THEN "Pasien Baru"
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_pasien = "2" THEN "Pasien Lama"
        END) AS status_pasien,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_pelayanan.kasus = "0" THEN "Semua Kasus"
            WHEN merm_pengaturan_diskon_rajal_pelayanan.kasus = "1" THEN "Kasus Baru"
            WHEN merm_pengaturan_diskon_rajal_pelayanan.kasus = "2" THEN "Kasus Lama"
        END) AS kasus,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_pelayanan.head_parent_tarif = "0" THEN "Semua"
            WHEN merm_pengaturan_diskon_rajal_pelayanan.head_parent_tarif != "0" THEN mtarif_rawatjalan_head_parent.nama
        END) AS head_parent_tarif,
        mtarif_rawatjalan.level AS level_tarif_pelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_pelayanan.tarif_pelayanan = "0" THEN "Semua"
            WHEN merm_pengaturan_diskon_rajal_pelayanan.tarif_pelayanan != "0" THEN mtarif_rawatjalan.nama
        END) AS tarif_pelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_diskon_all = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_diskon_all = "1" THEN
                IF (merm_pengaturan_diskon_rajal_pelayanan.tipe_diskon_all = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_pelayanan.diskon_all, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_pelayanan.diskon_all, 0))
        END) AS diskon_all,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_diskon_jasasarana = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_diskon_jasasarana = "1" THEN
                IF (merm_pengaturan_diskon_rajal_pelayanan.tipe_diskon_jasasarana = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_pelayanan.diskon_jasasarana, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_pelayanan.diskon_jasasarana, 0))
        END) AS diskon_jasasarana,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_diskon_jasapelayanan = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_diskon_jasapelayanan = "1" THEN 
                IF (merm_pengaturan_diskon_rajal_pelayanan.tipe_diskon_jasapelayanan = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_pelayanan.diskon_jasapelayanan, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_pelayanan.diskon_jasapelayanan, 0))
        END) AS diskon_jasapelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_diskon_bhp = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_diskon_bhp = "1" THEN 
                IF (merm_pengaturan_diskon_rajal_pelayanan.tipe_diskon_bhp = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_pelayanan.diskon_bhp, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_pelayanan.diskon_bhp, 0))
        END) AS diskon_bhp,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_diskon_biaya_perawatan = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_pelayanan.status_diskon_biaya_perawatan = "1" THEN 
                IF (merm_pengaturan_diskon_rajal_pelayanan.tipe_diskon_biaya_perawatan = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_pelayanan.diskon_biaya_perawatan, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_pelayanan.diskon_biaya_perawatan, 0))
        END) AS diskon_biaya_perawatan');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = merm_pengaturan_diskon_rajal_pelayanan.tujuan_poliklinik', 'LEFT');
        $this->db->join('mtarif_rawatjalan mtarif_rawatjalan_head_parent', 'mtarif_rawatjalan_head_parent.id = merm_pengaturan_diskon_rajal_pelayanan.head_parent_tarif', 'LEFT');
        $this->db->join('mtarif_rawatjalan', 'mtarif_rawatjalan.id = merm_pengaturan_diskon_rajal_pelayanan.tarif_pelayanan', 'LEFT');
        $this->db->where('merm_pengaturan_diskon_rajal_pelayanan.tipe', $tipe);
        $this->db->where('merm_pengaturan_diskon_rajal_pelayanan.idkelompokpasien', $idkelompokpasien);
        $query = $this->db->get('merm_pengaturan_diskon_rajal_pelayanan');

        return $query->result();
    }
	

    public function getSettingDiskonRadiologi($tipe, $idkelompokpasien){
        $this->db->select('merm_pengaturan_diskon_rajal_radiologi.id,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_radiologi.tujuan_pasien = "0" THEN "Semua Tujuan"
            WHEN merm_pengaturan_diskon_rajal_radiologi.tujuan_pasien = "1" THEN "Poliklinik"
            WHEN merm_pengaturan_diskon_rajal_radiologi.tujuan_pasien = "2" THEN "Instalasi Gawat Darurat (IGD)"
        END) AS tujuan_pasien,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_tujuan_poliklinik = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_tujuan_poliklinik = "1" AND merm_pengaturan_diskon_rajal_radiologi.tujuan_poliklinik = "0" THEN "Semua Poliklinik"
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_tujuan_poliklinik = "1" AND merm_pengaturan_diskon_rajal_radiologi.tujuan_poliklinik != "0" THEN mpoliklinik.nama
        END) AS tujuan_poliklinik,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_pasien = "0" THEN "Semua Status"
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_pasien = "1" THEN "Pasien Baru"
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_pasien = "2" THEN "Pasien Lama"
        END) AS status_pasien,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_radiologi.kasus = "0" THEN "Semua Kasus"
            WHEN merm_pengaturan_diskon_rajal_radiologi.kasus = "1" THEN "Kasus Baru"
            WHEN merm_pengaturan_diskon_rajal_radiologi.kasus = "2" THEN "Kasus Lama"
        END) AS kasus,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_radiologi.jenis_tarif = "0" THEN "Semua"
            WHEN merm_pengaturan_diskon_rajal_radiologi.jenis_tarif = "1" THEN "X-Ray"
            WHEN merm_pengaturan_diskon_rajal_radiologi.jenis_tarif = "2" THEN "USG"
            WHEN merm_pengaturan_diskon_rajal_radiologi.jenis_tarif = "3" THEN "CT Scan"
            WHEN merm_pengaturan_diskon_rajal_radiologi.jenis_tarif = "4" THEN "MRI"
            WHEN merm_pengaturan_diskon_rajal_radiologi.jenis_tarif = "5" THEN "BMD"
        END) AS jenis_tarif,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_radiologi.head_parent_tarif = "0" THEN "Semua"
            WHEN merm_pengaturan_diskon_rajal_radiologi.head_parent_tarif != "0" THEN mtarif_radiologi_head_parent.nama
        END) AS head_parent_tarif,
        mtarif_radiologi.level AS level_tarif_pelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_radiologi.tarif_pelayanan = "0" THEN "Semua"
            WHEN merm_pengaturan_diskon_rajal_radiologi.tarif_pelayanan != "0" THEN 
                (
                    CASE 
                        WHEN mtarif_radiologi.idtipe = 1 AND mtarif_radiologi.idkelompok = 0 THEN
                            CONCAT(mtarif_radiologi.nama,  " (", mtarif_radiologi_expose.nama, mtarif_radiologi_film.nama, ")")
                    ELSE
                        mtarif_radiologi.nama
                    END
                )
        END) AS tarif_pelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_diskon_all = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_diskon_all = "1" THEN
                IF (merm_pengaturan_diskon_rajal_radiologi.tipe_diskon_all = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_radiologi.diskon_all, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_radiologi.diskon_all, 0))
        END) AS diskon_all,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_diskon_jasasarana = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_diskon_jasasarana = "1" THEN
                IF (merm_pengaturan_diskon_rajal_radiologi.tipe_diskon_jasasarana = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_radiologi.diskon_jasasarana, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_radiologi.diskon_jasasarana, 0))
        END) AS diskon_jasasarana,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_diskon_jasapelayanan = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_diskon_jasapelayanan = "1" THEN 
                IF (merm_pengaturan_diskon_rajal_radiologi.tipe_diskon_jasapelayanan = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_radiologi.diskon_jasapelayanan, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_radiologi.diskon_jasapelayanan, 0))
        END) AS diskon_jasapelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_diskon_bhp = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_diskon_bhp = "1" THEN 
                IF (merm_pengaturan_diskon_rajal_radiologi.tipe_diskon_bhp = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_radiologi.diskon_bhp, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_radiologi.diskon_bhp, 0))
        END) AS diskon_bhp,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_diskon_biaya_perawatan = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_radiologi.status_diskon_biaya_perawatan = "1" THEN 
                IF (merm_pengaturan_diskon_rajal_radiologi.tipe_diskon_biaya_perawatan = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_radiologi.diskon_biaya_perawatan, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_radiologi.diskon_biaya_perawatan, 0))
        END) AS diskon_biaya_perawatan');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = merm_pengaturan_diskon_rajal_radiologi.tujuan_poliklinik', 'LEFT');
        $this->db->join('mtarif_radiologi mtarif_radiologi_head_parent', 'mtarif_radiologi_head_parent.id = merm_pengaturan_diskon_rajal_radiologi.head_parent_tarif', 'LEFT');
        $this->db->join('mtarif_radiologi', 'mtarif_radiologi.id = merm_pengaturan_diskon_rajal_radiologi.tarif_pelayanan', 'LEFT');
        $this->db->join('mtarif_radiologi_film', 'mtarif_radiologi_film.id = mtarif_radiologi.idfilm', 'LEFT');
        $this->db->join('mtarif_radiologi_expose', 'mtarif_radiologi_expose.id = mtarif_radiologi.idexpose', 'LEFT');
        $this->db->where('merm_pengaturan_diskon_rajal_radiologi.tipe', $tipe);
        $this->db->where('merm_pengaturan_diskon_rajal_radiologi.idkelompokpasien', $idkelompokpasien);
        $query = $this->db->get('merm_pengaturan_diskon_rajal_radiologi');

        return $query->result();
    }

    public function getSettingDiskonLaboratorium($tipe, $idkelompokpasien){
        $this->db->select('merm_pengaturan_diskon_rajal_laboratorium.id,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_laboratorium.tujuan_pasien = "0" THEN "Semua Tujuan"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.tujuan_pasien = "1" THEN "Poliklinik"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.tujuan_pasien = "2" THEN "Instalasi Gawat Darurat (IGD)"
        END) AS tujuan_pasien,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_tujuan_poliklinik = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_tujuan_poliklinik = "1" AND merm_pengaturan_diskon_rajal_laboratorium.tujuan_poliklinik = "0" THEN "Semua Poliklinik"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_tujuan_poliklinik = "1" AND merm_pengaturan_diskon_rajal_laboratorium.tujuan_poliklinik != "0" THEN mpoliklinik.nama
        END) AS tujuan_poliklinik,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_pasien = "0" THEN "Semua Status"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_pasien = "1" THEN "Pasien Baru"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_pasien = "2" THEN "Pasien Lama"
        END) AS status_pasien,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_laboratorium.kasus = "0" THEN "Semua Kasus"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.kasus = "1" THEN "Kasus Baru"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.kasus = "2" THEN "Kasus Lama"
        END) AS kasus,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_laboratorium.jenis_tarif = "0" THEN "Semua"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.jenis_tarif = "1" THEN "Umum"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.jenis_tarif = "2" THEN "Pathologi Anatomi"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.jenis_tarif = "3" THEN "PMI"
        END) AS jenis_tarif,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_laboratorium.head_parent_tarif = "0" THEN "Semua"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.head_parent_tarif != "0" THEN mtarif_laboratorium_head_parent.nama
        END) AS head_parent_tarif,
        mtarif_laboratorium.level AS level_tarif_pelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_laboratorium.tarif_pelayanan = "0" THEN "Semua"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.tarif_pelayanan != "0" THEN mtarif_laboratorium.nama
        END) AS tarif_pelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_diskon_all = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_diskon_all = "1" THEN
                IF (merm_pengaturan_diskon_rajal_laboratorium.tipe_diskon_all = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_laboratorium.diskon_all, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_laboratorium.diskon_all, 0))
        END) AS diskon_all,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_diskon_jasasarana = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_diskon_jasasarana = "1" THEN
                IF (merm_pengaturan_diskon_rajal_laboratorium.tipe_diskon_jasasarana = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_laboratorium.diskon_jasasarana, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_laboratorium.diskon_jasasarana, 0))
        END) AS diskon_jasasarana,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_diskon_jasapelayanan = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_diskon_jasapelayanan = "1" THEN 
                IF (merm_pengaturan_diskon_rajal_laboratorium.tipe_diskon_jasapelayanan = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_laboratorium.diskon_jasapelayanan, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_laboratorium.diskon_jasapelayanan, 0))
        END) AS diskon_jasapelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_diskon_bhp = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_diskon_bhp = "1" THEN 
                IF (merm_pengaturan_diskon_rajal_laboratorium.tipe_diskon_bhp = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_laboratorium.diskon_bhp, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_laboratorium.diskon_bhp, 0))
        END) AS diskon_bhp,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_diskon_biaya_perawatan = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_laboratorium.status_diskon_biaya_perawatan = "1" THEN 
                IF (merm_pengaturan_diskon_rajal_laboratorium.tipe_diskon_biaya_perawatan = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_laboratorium.diskon_biaya_perawatan, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_laboratorium.diskon_biaya_perawatan, 0))
        END) AS diskon_biaya_perawatan');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = merm_pengaturan_diskon_rajal_laboratorium.tujuan_poliklinik', 'LEFT');
        $this->db->join('mtarif_laboratorium mtarif_laboratorium_head_parent', 'mtarif_laboratorium_head_parent.id = merm_pengaturan_diskon_rajal_laboratorium.head_parent_tarif', 'LEFT');
        $this->db->join('mtarif_laboratorium', 'mtarif_laboratorium.id = merm_pengaturan_diskon_rajal_laboratorium.tarif_pelayanan', 'LEFT');
        $this->db->where('merm_pengaturan_diskon_rajal_laboratorium.tipe', $tipe);
        $this->db->where('merm_pengaturan_diskon_rajal_laboratorium.idkelompokpasien', $idkelompokpasien);
        $query = $this->db->get('merm_pengaturan_diskon_rajal_laboratorium');

        return $query->result();
    }

    public function getSettingDiskonFisioterapi($tipe, $idkelompokpasien){
        $this->db->select('merm_pengaturan_diskon_rajal_fisioterapi.id,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.tujuan_pasien = "0" THEN "Semua Tujuan"
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.tujuan_pasien = "1" THEN "Poliklinik"
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.tujuan_pasien = "2" THEN "Instalasi Gawat Darurat (IGD)"
        END) AS tujuan_pasien,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_tujuan_poliklinik = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_tujuan_poliklinik = "1" AND merm_pengaturan_diskon_rajal_fisioterapi.tujuan_poliklinik = "0" THEN "Semua Poliklinik"
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_tujuan_poliklinik = "1" AND merm_pengaturan_diskon_rajal_fisioterapi.tujuan_poliklinik != "0" THEN mpoliklinik.nama
        END) AS tujuan_poliklinik,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_pasien = "0" THEN "Semua Status"
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_pasien = "1" THEN "Pasien Baru"
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_pasien = "2" THEN "Pasien Lama"
        END) AS status_pasien,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.kasus = "0" THEN "Semua Kasus"
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.kasus = "1" THEN "Kasus Baru"
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.kasus = "2" THEN "Kasus Lama"
        END) AS kasus,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.head_parent_tarif = "0" THEN "Semua"
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.head_parent_tarif != "0" THEN mtarif_fisioterapi_head_parent.nama
        END) AS head_parent_tarif,
        mtarif_fisioterapi.level AS level_tarif_pelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.tarif_pelayanan = "0" THEN "Semua"
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.tarif_pelayanan != "0" THEN mtarif_fisioterapi.nama
        END) AS tarif_pelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_diskon_all = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_diskon_all = "1" THEN
                IF (merm_pengaturan_diskon_rajal_fisioterapi.tipe_diskon_all = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_fisioterapi.diskon_all, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_fisioterapi.diskon_all, 0))
        END) AS diskon_all,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_diskon_jasasarana = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_diskon_jasasarana = "1" THEN
                IF (merm_pengaturan_diskon_rajal_fisioterapi.tipe_diskon_jasasarana = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_fisioterapi.diskon_jasasarana, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_fisioterapi.diskon_jasasarana, 0))
        END) AS diskon_jasasarana,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_diskon_jasapelayanan = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_diskon_jasapelayanan = "1" THEN 
                IF (merm_pengaturan_diskon_rajal_fisioterapi.tipe_diskon_jasapelayanan = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_fisioterapi.diskon_jasapelayanan, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_fisioterapi.diskon_jasapelayanan, 0))
        END) AS diskon_jasapelayanan,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_diskon_bhp = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_diskon_bhp = "1" THEN 
                IF (merm_pengaturan_diskon_rajal_fisioterapi.tipe_diskon_bhp = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_fisioterapi.diskon_bhp, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_fisioterapi.diskon_bhp, 0))
        END) AS diskon_bhp,
        (CASE
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_diskon_biaya_perawatan = "0" THEN "Tidak Ditentukan"
            WHEN merm_pengaturan_diskon_rajal_fisioterapi.status_diskon_biaya_perawatan = "1" THEN 
                IF (merm_pengaturan_diskon_rajal_fisioterapi.tipe_diskon_biaya_perawatan = "1", CONCAT(FORMAT(merm_pengaturan_diskon_rajal_fisioterapi.diskon_biaya_perawatan, 2), "%"), FORMAT(merm_pengaturan_diskon_rajal_fisioterapi.diskon_biaya_perawatan, 0))
        END) AS diskon_biaya_perawatan');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = merm_pengaturan_diskon_rajal_fisioterapi.tujuan_poliklinik', 'LEFT');
        $this->db->join('mtarif_fisioterapi mtarif_fisioterapi_head_parent', 'mtarif_fisioterapi_head_parent.id = merm_pengaturan_diskon_rajal_fisioterapi.head_parent_tarif', 'LEFT');
        $this->db->join('mtarif_fisioterapi', 'mtarif_fisioterapi.id = merm_pengaturan_diskon_rajal_fisioterapi.tarif_pelayanan', 'LEFT');
        $this->db->where('merm_pengaturan_diskon_rajal_fisioterapi.tipe', $tipe);
        $this->db->where('merm_pengaturan_diskon_rajal_fisioterapi.idkelompokpasien', $idkelompokpasien);
        $query = $this->db->get('merm_pengaturan_diskon_rajal_fisioterapi');

        return $query->result();
    }
	public function get_obat($idtipe,$searchText)
    {
        $this->db->select("view_barang_farmasi.*,mgudang_stok.stok");
        $this->db->from('view_barang_farmasi');
        $this->db->join('mgudang_stok', 'mgudang_stok.idbarang=view_barang_farmasi.id AND mgudang_stok.idunitpelayanan=1 AND mgudang_stok.idtipe=view_barang_farmasi.idtipe');
        $this->db->where('view_barang_farmasi.idtipe', $idtipe);
        $this->db->like('view_barang_farmasi.nama', $searchText);
        $this->db->or_like('view_barang_farmasi.kode', $searchText);
        $query = $this->db->get();
        // print_r($this->db->last_query());exit();
        return $query->result_array();
    }
}
