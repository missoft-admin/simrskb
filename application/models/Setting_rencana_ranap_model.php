<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_rencana_ranap_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_assesmen_setting(){
		$q="SELECT * FROM setting_rencana_ranap H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_label(){
		$q="SELECT * FROM setting_rencana_ranap_label";
		return $this->db->query($q)->row_array();
	}

	function save_assesmen(){
		$id =1;
		$this->judul_header = $this->input->post('judul_header');
		$this->judul_footer = $this->input->post('judul_footer');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->lama_edit = $this->input->post('lama_edit');
		$this->orang_edit = $this->input->post('orang_edit');
		$this->st_hapus_catatan = $this->input->post('st_hapus_catatan');
		$this->lama_hapus = $this->input->post('lama_hapus');
		$this->orang_hapus = $this->input->post('orang_hapus');
		$this->st_duplikasi_catatan = $this->input->post('st_duplikasi_catatan');
		$this->lama_duplikasi = $this->input->post('lama_duplikasi');
		$this->orang_duplikasi = $this->input->post('orang_duplikasi');
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_rencana_ranap', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_label(){
		$id =1;
		$this->judul_per_ina = $this->input->post('judul_per_ina');
		$this->judul_per_eng = $this->input->post('judul_per_eng');
		$this->paragraph_1_ina = $this->input->post('paragraph_1_ina');
		$this->paragraph_1_eng = $this->input->post('paragraph_1_eng');
		$this->paragraph_2_ina = $this->input->post('paragraph_2_ina');
		$this->paragraph_2_eng = $this->input->post('paragraph_2_eng');
		$this->nama_pasien_ina = $this->input->post('nama_pasien_ina');
		$this->nama_pasien_eng = $this->input->post('nama_pasien_eng');
		$this->ttl_ina = $this->input->post('ttl_ina');
		$this->ttl_eng = $this->input->post('ttl_eng');
		$this->diagnosa_per_ina = $this->input->post('diagnosa_per_ina');
		$this->diagnosa_per_eng = $this->input->post('diagnosa_per_eng');
		$this->catatan_ina = $this->input->post('catatan_ina');
		$this->catatan_eng = $this->input->post('catatan_eng');
		$this->tipe_ina = $this->input->post('tipe_ina');
		$this->tipe_eng = $this->input->post('tipe_eng');
		$this->dpjp_ina = $this->input->post('dpjp_ina');
		$this->dpjp_eng = $this->input->post('dpjp_eng');
		$this->rencana_masuk_ina = $this->input->post('rencana_masuk_ina');
		$this->rencana_masuk_eng = $this->input->post('rencana_masuk_eng');
		$this->dilakukan_pemeriksaan_ina = $this->input->post('dilakukan_pemeriksaan_ina');
		$this->dilakukan_pemeriksaan_eng = $this->input->post('dilakukan_pemeriksaan_eng');
		$this->rencana_pelayanan_ina = $this->input->post('rencana_pelayanan_ina');
		$this->rencana_pelayanan_eng = $this->input->post('rencana_pelayanan_eng');
		$this->paragraph_3_ina = $this->input->post('paragraph_3_ina');
		$this->paragraph_3_eng = $this->input->post('paragraph_3_eng');
		$this->footer_ina = $this->input->post('footer_ina');
		$this->footer_eng = $this->input->post('footer_eng');

		if ($this->db->update('setting_rencana_ranap_label', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_defaultsave_default(){
		$id =1;
		$this->st_spesifik_rencana_ranap = $this->input->post('st_spesifik_rencana_ranap');
		if (!empty($this->input->post('st_kunci_default_rencana_ranap'))){
		$this->st_kunci_default_rencana_ranap =($this->input->post('st_spesifik_rencana_ranap')=='0'?'0':$this->input->post('st_kunci_default_rencana_ranap'));
			
		}else{
			$this->st_kunci_default_rencana_ranap =0;
		}
		
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('msetting_rencana_ranap_default', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


