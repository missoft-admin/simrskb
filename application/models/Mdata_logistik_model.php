<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mdata_logistik_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getKode()
    {
        $this->db->like('kode', '8-', 'after');
        $this->db->from('mdata_logistik');
        $query = $this->db->count_all_results();

        if ($query > 0) {
            $autono = $query + 1;
            $autono = '8-'.str_pad($autono, 4, '0', STR_PAD_LEFT);
        } else {
            $autono = '8-'.'0001';
        }

        return $autono;
    }
	public function aktifkan($id)
    {
        $this->status = 1;
		
        if ($this->db->update('mdata_logistik', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function getKategori()
    {
        // $this->db->where('idtipe', '4');
        // $this->db->where('status', '1');
        // $query = $this->db->get('mdata_kategori');
        // return $query->result();
		$q="SELECT id,`level`,path,
				CASE 
					WHEN nama_L1 IS NOT NULL THEN nama_L1
					WHEN nama_L2 IS NOT NULL THEN nama_L2
					WHEN nama_L3 IS NOT NULL THEN nama_L3
					WHEN nama_L4 IS NOT NULL THEN nama_L4
					WHEN nama_L5 IS NOT NULL THEN nama_L5
					WHEN nama_L6 IS NOT NULL THEN nama_L6
					WHEN nama_L7 IS NOT NULL THEN nama_L7
				  ELSE NULL
				  
				END as nama
				 FROM view_kategori_logistik";
			$query=$this->db->query($q);
			return $query->result();
    }
	public function get_array_kategori($path){
		$q="SELECT id FROM view_kategori_logistik T1
			where T1.path like '$path%'";
		$query=$this->db->query($q);
		$query=$query->result();
		$array=array();
		foreach($query as $row){
			$array[]=$row->id;
		}
		
		return array_values($array);;
	}
    public function getSatuan()
    {
        $this->db->where('idkategori', '1');
        $this->db->where('status', '1');
        $query = $this->db->get('msatuan');
		// print_r($this->db->last_query());exit();
        return $query->result();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mdata_logistik');
        return $query->row();
    }

    public function saveData()
    {
        $this->kode						= $this->getKode();
        $this->idkategori			= $_POST['idkategori'];
        $this->nama						= $_POST['nama'];
        $this->idsatuan				= $_POST['idsatuan'];
        $this->lokasirak			= $_POST['lokasirak'];
        $this->ppn					  = RemoveComma($_POST['ppn']);
        $this->hargabeli			= RemoveComma($_POST['hargabeli']);
        $this->hargadasar			= RemoveComma($_POST['hargadasar']);
        $this->margin			    = RemoveComma($_POST['margin']);
        $this->stokreorder		= RemoveComma($_POST['stokreorder']);
        $this->stokminimum		= RemoveComma($_POST['stokminimum']);
        $this->catatan				= $_POST['catatan'];
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		$this->idsatuanbesar		= $_POST['idsatuanbesar'];
        $this->hargasatuanbesar		= RemoveComma($_POST['hargasatuanbesar']);
        $this->jumlahsatuanbesar	= RemoveComma($_POST['jumlahsatuanbesar']);
		
        if ($this->db->insert('mdata_logistik', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->kode						= $_POST['kode'];
        $this->idkategori			= $_POST['idkategori'];
        $this->nama						= $_POST['nama'];
        $this->idsatuan				= $_POST['idsatuan'];
        $this->lokasirak			= $_POST['lokasirak'];
        $this->ppn					  = RemoveComma($_POST['ppn']);
        $this->hargabeli			= RemoveComma($_POST['hargabeli']);
        $this->hargadasar			= RemoveComma($_POST['hargadasar']);
        $this->margin			    = RemoveComma($_POST['margin']);
        $this->stokreorder			= RemoveComma($_POST['stokreorder']);
        $this->stokminimum			= RemoveComma($_POST['stokminimum']);
        $this->catatan				= $_POST['catatan'];
		$this->edited_by  			= $this->session->userdata('user_id');
		$this->edited_date 		 	= date('Y-m-d H:i:s');
		$this->idsatuanbesar		= $_POST['idsatuanbesar'];
        $this->hargasatuanbesar		= RemoveComma($_POST['hargasatuanbesar']);
        $this->jumlahsatuanbesar	= RemoveComma($_POST['jumlahsatuanbesar']);
		
        if ($this->db->update('mdata_logistik', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		// print_r($this);exit();
        if ($this->db->update('mdata_logistik', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
