<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mdata_obat_alkes_default_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mdata_obat_alkes_default');
        return $query->row_array();
    }

    public function updateData()
    {
        $this->alokasiumum 							= (isset($_POST['alokasiumum']) ? 1:0);
        $this->alokasiasuransi 					= (isset($_POST['alokasiasuransi']) ? 1:0);
        $this->alokasijasaraharja 			= (isset($_POST['alokasijasaraharja']) ? 1:0);
        $this->alokasibpjskesehatan 		= (isset($_POST['alokasibpjskesehatan']) ? 1:0);
        $this->alokasibpjstenagakerja 	= (isset($_POST['alokasibpjstenagakerja']) ? 1:0);
        $this->marginumum 							= RemoveComma($_POST['marginumum']);
        $this->marginasuransi 					= RemoveComma($_POST['marginasuransi']);
        $this->marginjasaraharja 				= RemoveComma($_POST['marginjasaraharja']);
        $this->marginbpjskesehatan 			= RemoveComma($_POST['marginbpjskesehatan']);
        $this->marginbpjstenagakerja 		= RemoveComma($_POST['marginbpjstenagakerja']);
        $this->stokreorder							= RemoveComma($_POST['stokreorder']);
        $this->stokminimum							= RemoveComma($_POST['stokminimum']);
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mdata_obat_alkes_default', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

   
}
