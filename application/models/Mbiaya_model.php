<?php

class Mbiaya_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getNoAkun()
    {
        $this->db->order_by('makun_nomor.noakun', 'ASC');
        $this->db->where('makun_nomor.noakun LIKE', '5%');
        $this->db->or_where('makun_nomor.noakun LIKE', '11%');
        $this->db->or_where('makun_nomor.noakun LIKE', '12%');
        $this->db->or_where('makun_nomor.noakun LIKE', '13%');
        $this->db->or_where('makun_nomor.noakun LIKE', '14%');
        // $this->db->where('makun_nomor.stbayar', '1');
        $this->db->where('makun_nomor.status', '1');
        $query = $this->db->get('makun_nomor');
        return $query->result();
    }

    public function getAll()
    {
        $this->db->select('mbiaya.*, makun_nomor.noakun, makun_nomor.namaakun');
        $this->db->join('makun_nomor', 'makun_nomor.noakun = mbiaya.noakun');
        $this->db->where('mbiaya.status', 1);
        $this->db->order_by('mbiaya.keterangan', 'ASC');
        $query = $this->db->get('mbiaya');
        return $query->result();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mbiaya');
        return $query->row();
    }

    function saveData(){
      $this->keterangan   = $_POST['keterangan'];
      $this->noakun       = $_POST['noakun'];
  		$this->status 		  = 1;

  		if($this->db->insert('mbiaya', $this)){
  			return true;
  		}else{
  			$this->error_message = "Penyimpanan Gagal";
  			return false;
  		}
    }

    function updateData(){
      $this->keterangan   = $_POST['keterangan'];
      $this->noakun       = $_POST['noakun'];
  		$this->status 		  = 1;

  		if($this->db->update('mbiaya', $this, array('id' => $_POST['id']))){
  			return true;
  		}else{
  			$this->error_message = "Penyimpanan Gagal";
  			return false;
  		}
    }

  	function softDelete($id){
  		$this->status = 0;

  		if($this->db->update('mbiaya', $this, array('id' => $id))){
  			return true;
  		}else{
  			$this->error_message = "Penyimpanan Gagal";
  			return false;
  		}
    }
}
