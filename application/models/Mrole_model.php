<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mrole_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
    }

    public function getPathLevel($headerpath)
    {
        $strquery = "SELECT LEVEL + 1 AS level, CASE WHEN ( SELECT COUNT(headerpath) FROM mroles WHERE headerpath = t1.headerpath ) = 0 THEN CONCAT(t1.path, '.1') ELSE  CONCAT(t1.path,'.',  IF ( ( SELECT COUNT(headerpath) FROM mroles WHERE headerpath = t1.path ) = 0, ( SELECT COUNT(headerpath) FROM mroles WHERE headerpath = t1.path ) + 1, IF ( LENGTH(t1.path) = '1',( SELECT COUNT(headerpath) FROM mroles WHERE headerpath = t1.path ), ( SELECT COUNT(headerpath) FROM mroles WHERE headerpath = t1.path ) + 1 ) ) ) END AS path FROM mroles t1 WHERE t1.path = '".$headerpath."'";

        $query = $this->db->query($strquery);
        $row = $query->row();

        if ($query->num_rows() > 0) {
            $data['path']  = $row->path;
            $data['level'] = $row->level;
        } else {
            $data['path']  = 1;
            $data['level'] = 0;
        }

        return $data;
    }

    public function getAllParent($headerpath = 0, $level = 999)
    {
        # Get ID Root
        $this->db->select('MAX(CAST(headerpath AS SIGNED)) + 1 AS idroot');
        $this->db->where('status', '1');
        $query = $this->db->get('mroles');
        $row = $query->row();

        if ($headerpath != 0) {
            $idroot = $headerpath;
        } else {
            $idroot = ($row->idroot ? $row->idroot : 1);
        }

        # Get Option
        $this->db->where('status', '1');
        $this->db->order_by('path', 'ASC');
        $query  = $this->db->get('mroles');
        $result = $query->result();

        $selected = ($level == 0 ? 'selected' : '');
        $data = '<option value="'.$idroot.'" '.$selected.'>Root</option>';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                if ($headerpath != 0 && $level != 0) {
                    $selected = ($headerpath == $row->path ? 'selected' : '');
                } else {
                    $selected = '';
                }
                $data .= '<option value="'.$row->path.'" '.$selected.'>'.TreeView($row->level, $row->nama).'</option>';
            }
        }
        return $data;
    }

    public function saveData($data)
    {
        $this->db->insert('mroles', $data);
        return true;
    }

    public function deleteDetail($id)
    {
        $this->db->where('idrole', $id);

        $this->db->delete('mroles_detail');
        return true;
    }

    public function saveDetailData($id, $data)
    {
        $this->deleteDetail($id);
        $detail = json_decode($data);

        foreach ($detail as $rows) {
            $this->db->set(
        array(
          'idrole'     => $id,
          'idresource' => $rows[6],
          'status'     => $rows[5],
        )
      );

            $this->db->insert('mroles_detail');
        }

        return true;
    }

    public function getAllRoles()
    {
        $this->db->order_by('path');

        $query = $this->db->get('mroles');
        return $query->result();
    }
	public function getAllRoles2($id)
    {
        $q="SELECT mroles.id,mroles.nama,SUM(CASE WHEN mroles_akses.pilih='1' THEN 1 ELSE 0 END ) jml from mroles
			LEFT JOIN mroles_akses ON mroles.id=mroles_akses.role_id
			WHERE mroles.id !='$id'
			GROUP BY mroles.id";

        $query = $this->db->query($q);
        return $query->result();
    }
	public function getAllAkses()
    {
        // $this->db->order_by('path');
        // $query = $this->db->get('mroles_akses');
		$q="SELECT M.menu,S.menu2,AC.action,A.pilih,M.id,S.id2 FROM mroles_akses A
			INNER JOIN mmenu_action AC ON AC.id3=A.id3
			INNER JOIN mmenu_sub S ON S.id2=AC.id_menu_sub
			INNER JOIN mmenu M ON M.id=S.id_menu
			WHERE A.role_id='1'
			ORDER BY M.`index`,S.index2,AC.index3";
        return $query->result();
    }
	public function get_data($id)
    {
        $this->db->where('id',$id);

        $query = $this->db->get('mroles');
        return $query->row_array();
    }
	public function insert_init($id)
    {
       $q="INSERT INTO mroles_akses
			SELECT '$id' as role_id,M.id3,'0' as pilih from mmenu_action M
			WHERE M.id3 NOT IN (SELECT A.id3 FROM mroles_akses A WHERE A.role_id='$id') AND M.`status`='1'";
        $query=$this->db->query($q);
		return true;
    }

    public function getAllRules($id = null)
    {
        if ($id != null) {
            $this->db->where('idrole', $id);
        }

        $this->db->order_by('path');

        if ($id != null) {
            $query = $this->db->get('view_roles');
        } else {
            $query = $this->db->get('mresources');
        }

        return $query->result();
    }

    public function deleteRole($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('mroles');

        return true;
    }
}
