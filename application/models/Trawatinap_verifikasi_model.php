<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trawatinap_verifikasi_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function viewRincianRanapRuanganValidasi($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT trawatinap_ruangan.*,
        trawatinap_pendaftaran.nopendaftaran
        FROM trawatinap_ruangan
        JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_ruangan.idrawatinap
        WHERE idrawatinap = $idpendaftaran AND trawatinap_ruangan.status !='0'
      ");
		return $query->result();
	}
	public function viewRincianRanapRuangan($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT H.id as iddetail,H.idtarif,H.namatarif,H.jasasarana,H.jasapelayanan,H.bhp,H.biayaperawatan
								,H.jasasarana_disc,H.jasapelayanan_disc,H.bhp_disc,H.biayaperawatan_disc,H.total as subtotal
								,H.jumlahhari as kuantitas,H.total * H.jumlahhari as total,H.diskon,H.diskon as diskon_rp,H.totalkeseluruhan
								FROM `trawatinap_ruangan` H
								WHERE H.idrawatinap='$idpendaftaran'
								");
		return $query->result();
	}

	public function viewRincianRanapRuanganGroup($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	view_rincian_ranap_ruangan.id AS id,
      	view_rincian_ranap_ruangan.idrawatinap AS idrawatinap,
      	view_rincian_ranap_ruangan.nopendaftaran AS nopendaftaran,
      	view_rincian_ranap_ruangan.tanggaldari AS tanggaldari,
      	view_rincian_ranap_ruangan.tanggalsampai AS tanggalsampai,
      	view_rincian_ranap_ruangan.idkelompokpasien AS idkelompokpasien,
      	view_rincian_ranap_ruangan.idruangan AS idruangan,
      	view_rincian_ranap_ruangan.namaruangan AS namaruangan,
      	view_rincian_ranap_ruangan.idkelas AS idkelas,
      	view_rincian_ranap_ruangan.namakelas AS namakelas,
      	view_rincian_ranap_ruangan.idbed AS idbed,
      	SUM(
      		CASE
      			WHEN ( view_rincian_ranap_ruangan.jumlahhari = 0 ) THEN
      			1 ELSE view_rincian_ranap_ruangan.jumlahhari
      		END
      	) AS jumlahhari,
      	SUM(
      		CASE
      			WHEN ( view_rincian_ranap_ruangan.jumlahhari = 0 ) THEN
      			1 ELSE view_rincian_ranap_ruangan.jumlahhari
      		END
      	) AS tarif,
      	27 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      	(
      		SELECT
      			view_ruangan_ranap__.id AS id,
      			view_ruangan_ranap__.idrawatinap AS idrawatinap,
      			view_ruangan_ranap__.nopendaftaran AS nopendaftaran,
      			view_ruangan_ranap__.tanggaldari AS tanggaldari,
      			view_ruangan_ranap__.tanggalsampai AS tanggalsampai,
      			view_ruangan_ranap__.idkelompokpasien AS idkelompokpasien,
      			view_ruangan_ranap__.idruangan AS idruangan,
      			mruangan.nama AS namaruangan,
      			view_ruangan_ranap__.idkelas AS idkelas,
      			mkelas.nama AS namakelas,
      			view_ruangan_ranap__.idbed AS idbed,
      			view_ruangan_ranap__.jumlahhari AS jumlahhari
      		FROM
      			(
      				SELECT
      					view_ruangan_ranap_.id AS id,
      					view_ruangan_ranap_.idrawatinap AS idrawatinap,
      					view_ruangan_ranap_.nopendaftaran AS nopendaftaran,
      					view_ruangan_ranap_.tanggaldari AS tanggaldari,
      					view_ruangan_ranap_.tanggalsampai AS tanggalsampai,
      					view_ruangan_ranap_.idkelompokpasien AS idkelompokpasien,
      					view_ruangan_ranap_.idruangan AS idruangan,
      					view_ruangan_ranap_.idkelas AS idkelas,
      					view_ruangan_ranap_.idbed AS idbed,(
      					to_days( view_ruangan_ranap_.tanggalsampai ) - to_days( view_ruangan_ranap_.tanggaldari )) AS jumlahhari
      				FROM
      					(
      						SELECT
      							0 AS id,
      							trawatinap_pendaftaran.id AS idrawatinap,
      							trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      							(CASE
      								WHEN ( trawatinap_pendaftaran.tanggalperubahanbed IS NOT NULL ) THEN
      								trawatinap_pendaftaran.tanggalperubahanbed ELSE trawatinap_pendaftaran.tanggaldaftar
      							END) AS tanggaldari,
      							cast( now() AS date ) AS tanggalsampai,
      							trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      							trawatinap_pendaftaran.idruangan AS idruangan,
      							trawatinap_pendaftaran.idkelas AS idkelas,
      							trawatinap_pendaftaran.idbed AS idbed
      						FROM
      							trawatinap_pendaftaran
                  WHERE trawatinap_pendaftaran.id = $idpendaftaran
      					) AS view_ruangan_ranap_
      			) AS view_ruangan_ranap__
      			JOIN mruangan ON mruangan.id = view_ruangan_ranap__.idruangan
      			JOIN mkelas ON mkelas.id = view_ruangan_ranap__.idkelas

      			UNION ALL

      			SELECT
      				trawatinap_history_bed.id AS id,
      				trawatinap_history_bed.idrawatinap AS idrawatinap,
      				trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      				trawatinap_history_bed.tanggaldari AS tanggaldari,
      				trawatinap_history_bed.tanggalsampai AS tanggalsampai,
      				trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      				trawatinap_history_bed.idruangan AS idruangan,
      				mruangan.nama AS namaruangan,
      				trawatinap_history_bed.idkelas AS idkelas,
      				mkelas.nama AS namakelas,
      				trawatinap_history_bed.idbed AS idbed,(
      				to_days( trawatinap_history_bed.tanggalsampai ) - to_days( trawatinap_history_bed.tanggaldari )) AS jumlahhari
      			FROM
      				trawatinap_history_bed
      			JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_history_bed.idrawatinap
      			JOIN mruangan ON mruangan.id = trawatinap_history_bed.idruangan
      			JOIN mkelas ON mkelas.id = trawatinap_history_bed.idkelas
            WHERE trawatinap_history_bed.idrawatinap = $idpendaftaran
      	) AS view_rincian_ranap_ruangan
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_rincian_ranap_ruangan.id AND tverifikasi_transaksi_detail.id_kelompok = 27
      WHERE
      	view_rincian_ranap_ruangan.idrawatinap = $idpendaftaran
      GROUP BY
      	view_rincian_ranap_ruangan.idrawatinap,
      	view_rincian_ranap_ruangan.idruangan,
      	view_rincian_ranap_ruangan.idkelas
      ");
		return $query->result();
	}

	public function viewRincianRanapTindakan($idpendaftaran, $idtipe)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	trawatinap_tindakan.id AS iddetail,
      	trawatinap_tindakan.idrawatinap AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_tindakan.tanggal AS tanggal,
      	mtarif_rawatinap.idtipe AS idtipe,
        mdokter_kategori.id AS idkategori,
        mdokter_kategori.nama AS namakategori,
      	trawatinap_tindakan.iddokter AS iddokter,
      	COALESCE ( mdokter.nama, '-' ) AS namadokter,
      	trawatinap_tindakan.idpelayanan AS idpelayanan,
      	mtarif_rawatinap.id AS idtarif,
      	mtarif_rawatinap.nama AS namatarif,
        trawatinap_pendaftaran.idkelas,
      	trawatinap_tindakan.jasasarana AS jasasarana,
      	trawatinap_tindakan.jasasarana_disc AS jasasarana_disc,
      	trawatinap_tindakan.jasapelayanan AS jasapelayanan,
      	trawatinap_tindakan.jasapelayanan_disc AS jasapelayanan_disc,
      	(trawatinap_tindakan.jasapelayanan - trawatinap_tindakan.jasapelayanan_disc) * trawatinap_tindakan.kuantitas AS jasamedis,
      	trawatinap_tindakan.bhp AS bhp,
      	trawatinap_tindakan.bhp_disc AS bhp_disc,
      	trawatinap_tindakan.biayaperawatan AS biayaperawatan,
      	trawatinap_tindakan.biayaperawatan_disc AS biayaperawatan_disc,
      	trawatinap_tindakan.total AS subtotal,
      	trawatinap_tindakan.kuantitas AS kuantitas,
      	trawatinap_tindakan.total * trawatinap_tindakan.kuantitas AS total,
      	trawatinap_tindakan.diskon AS diskon,
		trawatinap_tindakan.totalkeseluruhan AS totalkeseluruhan,
      	trawatinap_tindakan.status AS status,
      	trawatinap_tindakan.statusverifikasi AS statusverifikasi,
        trawatinap_tindakan.potongan_rs AS potongan_rs,
        trawatinap_tindakan.pajak_dokter AS pajak_dokter,
        trawatinap_tindakan.periode_pembayaran AS periode_pembayaran,
        trawatinap_tindakan.periode_jatuhtempo AS periode_jatuhtempo,
        trawatinap_tindakan.status_jasamedis AS status_jasamedis
      FROM
      	trawatinap_tindakan
      LEFT JOIN mdokter ON mdokter.id = trawatinap_tindakan.iddokter
      LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
      LEFT JOIN mtarif_rawatinap ON mtarif_rawatinap.id = trawatinap_tindakan.idpelayanan
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_tindakan.idrawatinap
      WHERE
      	trawatinap_tindakan.status = 1 AND
      	trawatinap_tindakan.idrawatinap = $idpendaftaran AND
      	mtarif_rawatinap.idtipe = $idtipe
      ");
		return $query->result();
	}

	public function viewRincianRanapVisite($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	trawatinap_visite.id AS iddetail,
      	trawatinap_visite.idrawatinap AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_visite.tanggal AS tanggal,
      	trawatinap_visite.idpelayanan AS idtarif,
      	mtarif_visitedokter.nama AS namatarif,
      	trawatinap_visite.idruangan AS idruangan,
      	trawatinap_pendaftaran.idkelas AS idkelas,
      	mruangan.nama AS namaruangan,
      	trawatinap_visite.iddokter AS iddokter,
      	mdokter_kategori.id AS idkategori,
      	mdokter_kategori.nama AS namakategori,
      	mdokter.nama AS namadokter,
        mdokter.persentaseperujuk AS persentaseperujuk,
      	trawatinap_visite.idpelayanan AS idpelayanan,
      	trawatinap_visite.jasasarana AS jasasarana,
      	trawatinap_visite.jasasarana_disc AS jasasarana_disc,
      	trawatinap_visite.jasapelayanan AS jasapelayanan,
      	trawatinap_visite.jasapelayanan_disc AS jasapelayanan_disc,
      	(trawatinap_visite.jasapelayanan - trawatinap_visite.jasapelayanan_disc) * trawatinap_visite.kuantitas AS jasamedis,
      	trawatinap_visite.bhp AS bhp,
      	trawatinap_visite.bhp_disc AS bhp_disc,
      	trawatinap_visite.biayaperawatan AS biayaperawatan,
      	trawatinap_visite.biayaperawatan_disc AS biayaperawatan_disc,
      	trawatinap_visite.total AS subtotal,
      	trawatinap_visite.kuantitas AS kuantitas,
      	trawatinap_visite.total * trawatinap_visite.kuantitas AS total,
      	trawatinap_visite.diskon AS diskon,
      	trawatinap_visite.totalkeseluruhan AS totalkeseluruhan,
      	trawatinap_visite.status AS status,
      	trawatinap_visite.statusverifikasi AS statusverifikasi,
      	13 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail,
        trawatinap_visite.potongan_rs AS potongan_rs,
		IF(trawatinap_pendaftaran.idtipepasien = 1, mdokter.potonganfeers, mdokter.potonganfeerspribadi) AS potonganfeers,
        trawatinap_visite.pajak_dokter AS pajak_dokter,
        trawatinap_visite.periode_pembayaran AS periode_pembayaran,
        trawatinap_visite.periode_jatuhtempo AS periode_jatuhtempo,
        trawatinap_visite.status_jasamedis AS status_jasamedis
      FROM
      	trawatinap_visite
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_visite.idrawatinap
      LEFT JOIN mtarif_visitedokter ON mtarif_visitedokter.id = trawatinap_visite.idpelayanan
      LEFT JOIN mdokter ON mdokter.id = trawatinap_visite.iddokter
      LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
      LEFT JOIN mruangan ON mruangan.id = trawatinap_visite.idruangan
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = trawatinap_visite.id AND tverifikasi_transaksi_detail.id_kelompok = 13
      WHERE
      	trawatinap_visite.status = 1 AND
      	trawatinap_visite.idrawatinap = $idpendaftaran
	  ORDER BY trawatinap_visite.tanggal ASC, mdokter.nama ASC
      ");

		return $query->result();
	}

	public function viewRincianRanapVisiteGlobal($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	view_rincian_ranap_visite.idrawatinap AS idrawatinap,
      	view_rincian_ranap_visite.namadokter AS namadokter,
      	sum( view_rincian_ranap_visite.totalkeseluruhan ) AS totalkeseluruhan,
      	view_rincian_ranap_visite.statusverifikasi AS statusverifikasi
      FROM
      	(
        SELECT
        	trawatinap_visite.id AS iddetail,
        	trawatinap_visite.idrawatinap AS idrawatinap,
        	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
        	trawatinap_visite.tanggal AS tanggal,
        	trawatinap_visite.idruangan AS idruangan,
        	mruangan.nama AS namaruangan,
        	trawatinap_visite.iddokter AS iddokter,
        	mdokter.nama AS namadokter,
        	trawatinap_visite.idpelayanan AS idpelayanan,
        	trawatinap_visite.jasasarana AS jasasarana,
        	trawatinap_visite.jasasarana_disc AS jasasarana_disc,
        	trawatinap_visite.jasapelayanan AS jasapelayanan,
        	trawatinap_visite.jasapelayanan_disc AS jasapelayanan_disc,
        	trawatinap_visite.bhp AS bhp,
        	trawatinap_visite.bhp_disc AS bhp_disc,
        	trawatinap_visite.biayaperawatan AS biayaperawatan,
        	trawatinap_visite.biayaperawatan_disc AS biayaperawatan_disc,
        	trawatinap_visite.total AS subtotal,
        	trawatinap_visite.kuantitas AS kuantitas,
        	trawatinap_visite.total * trawatinap_visite.kuantitas AS total,
        	trawatinap_visite.diskon AS diskon,
			trawatinap_visite.totalkeseluruhan AS totalkeseluruhan,
        	trawatinap_visite.status AS status,
        	trawatinap_visite.statusverifikasi AS statusverifikasi,
        	13 AS kelompok,
        	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
        	tverifikasi_transaksi_detail.id AS idverifdetail
        FROM
        	trawatinap_visite
      	LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_visite.idrawatinap
      	LEFT JOIN mdokter ON mdokter.id = trawatinap_visite.iddokter
      	LEFT JOIN mruangan ON mruangan.id = trawatinap_visite.idruangan
      	LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = trawatinap_visite.id AND tverifikasi_transaksi_detail.id_kelompok = 13
        WHERE
        	trawatinap_visite.status = 1 AND
        	trawatinap_visite.idrawatinap = $idpendaftaran
        ) AS view_rincian_ranap_visite
      GROUP BY
      	view_rincian_ranap_visite.idrawatinap,
      	view_rincian_ranap_visite.iddokter
      ");
		return $query->result();
	}

	public function viewRincianFarmasiReturRanapObat($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	view_union_farmasi_retur.iddetail AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
        trawatinap_pendaftaran.idkelompokpasien,
      	view_union_farmasi_retur.nopengembalian AS nopengembalian,
      	view_union_farmasi_retur.nopengembalian AS nopenjualan,
      	cast( view_union_farmasi_retur.tanggal AS date ) AS tanggal,
      	view_union_farmasi_retur.idtarif AS idtarif,
      	view_union_farmasi_retur.nama AS namatarif,
      	view_union_farmasi_retur.idunitpelayanan AS idunitpelayanan,
      	view_union_farmasi_retur.unitpelayanan AS unitpelayanan,
      	view_union_farmasi_retur.idtipe AS idtipe,
      	view_union_farmasi_retur.idkategori AS idkategori,
      	(view_union_farmasi_retur.kuantitas * -1) AS kuantitas,
      	view_union_farmasi_retur.hargadasar AS hargadasar,
      	view_union_farmasi_retur.margin AS margin,
      	view_union_farmasi_retur.harga AS hargajual,
      	view_union_farmasi_retur.diskon AS diskon,
      	view_union_farmasi_retur.tuslah AS tuslah,
      	(view_union_farmasi_retur.totalharga * -1) AS totalkeseluruhan,
      	view_union_farmasi_retur.statusracikan AS statusracikan,
      	view_union_farmasi_retur.statusverifikasi AS statusverifikasi,
      	29 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      	(
      		SELECT
      			tpasien_pengembalian.id AS id,
      			tpasien_pengembalian.nopengembalian AS nopengembalian,
      			tpasien_pengembalian_detail.id AS iddetail,
      			tpasien_penjualan.idtindakan AS idtindakan,
      			tpasien_pengembalian.tanggal AS tanggal,
      			tpasien_pengembalian_detail.idunit AS idunitpelayanan,
      			munitpelayanan.nama AS unitpelayanan,
      			tpasien_penjualan.asalrujukan AS asalrujukan,
      		  (
      			 CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      					   mdata_alkes.id
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      					   mdata_obat.id
      				END
      			) AS idtarif,
      		  (
      			 CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      					   mdata_alkes.nama
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      					   mdata_obat.nama
      				END
      			) AS nama,
      		  (
      				CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      						mdata_alkes.idkategori
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      						mdata_obat.idkategori
      				END
      			) AS idkategori,
      		  (
      				CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      						mdata_alkes.kode
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      						mdata_obat.kode
      				END
      			) AS kode,
      			tpasien_pengembalian_detail.idtipe AS idtipe,
      			tpasien_pengembalian_detail.kuantitas AS kuantitas,
      			tpasien_pengembalian_detail.harga_dasar AS hargadasar,
      			tpasien_pengembalian_detail.margin AS margin,
      			tpasien_pengembalian_detail.harga AS harga,
      			tpasien_pengembalian_detail.diskon_rp AS diskon,
      			0 AS tuslah,
      			tpasien_pengembalian_detail.totalharga AS totalharga,
      			tpasien_pengembalian_detail.statusverifikasi AS statusverifikasi,
      			tpasien_pengembalian_detail.stracikan AS statusracikan
      		FROM
      			tpasien_pengembalian
      		JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan
      		JOIN tpasien_pengembalian_detail ON tpasien_pengembalian_detail.idpengembalian = tpasien_pengembalian.id
      		JOIN munitpelayanan ON munitpelayanan.id = tpasien_pengembalian_detail.idunit
      		LEFT JOIN mdata_obat ON tpasien_pengembalian_detail.idbarang = mdata_obat.id
      		LEFT JOIN mdata_alkes ON tpasien_pengembalian_detail.idbarang = mdata_alkes.id
      		WHERE
      			tpasien_pengembalian_detail.kuantitas > 0
      	) AS view_union_farmasi_retur
      JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = view_union_farmasi_retur.idtindakan AND view_union_farmasi_retur.asalrujukan = 3
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_union_farmasi_retur.iddetail AND tverifikasi_transaksi_detail.id_kelompok = 18
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran AND
      	view_union_farmasi_retur.idtipe = 3
      ");
		return $query->result();
	}

	public function viewRincianFarmasiReturRanapAlkes($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	view_union_farmasi_retur.iddetail AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      	view_union_farmasi_retur.nopengembalian AS nopengembalian,
      	view_union_farmasi_retur.nopengembalian AS nopenjualan,
      	cast( view_union_farmasi_retur.tanggal AS date ) AS tanggal,
      	view_union_farmasi_retur.idtarif AS idtarif,
      	view_union_farmasi_retur.nama AS namatarif,
      	view_union_farmasi_retur.idunitpelayanan AS idunitpelayanan,
      	view_union_farmasi_retur.unitpelayanan AS unitpelayanan,
      	view_union_farmasi_retur.idtipe AS idtipe,
      	view_union_farmasi_retur.idkategori AS idkategori,
      	(view_union_farmasi_retur.kuantitas * -1) AS kuantitas,
      	view_union_farmasi_retur.hargadasar AS hargadasar,
      	view_union_farmasi_retur.margin AS margin,
      	view_union_farmasi_retur.harga AS hargajual,
      	view_union_farmasi_retur.diskon AS diskon,
      	view_union_farmasi_retur.tuslah AS tuslah,
      	(view_union_farmasi_retur.totalharga * -1) AS totalkeseluruhan,
      	view_union_farmasi_retur.statusracikan AS statusracikan,
      	view_union_farmasi_retur.statusverifikasi AS statusverifikasi,
      	29 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      	(
      		SELECT
      			tpasien_pengembalian.id AS id,
      			tpasien_pengembalian.nopengembalian AS nopengembalian,
      			tpasien_pengembalian_detail.id AS iddetail,
      			tpasien_penjualan.idtindakan AS idtindakan,
      			tpasien_pengembalian.tanggal AS tanggal,
      			tpasien_pengembalian_detail.idunit AS idunitpelayanan,
      			munitpelayanan.nama AS unitpelayanan,
      			tpasien_penjualan.asalrujukan AS asalrujukan,
      		  (
      			 CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      					   mdata_alkes.id
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      					   mdata_obat.id
      				END
      			) AS idtarif,
      		  (
      			 CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      					   mdata_alkes.nama
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      					   mdata_obat.nama
      				END
      			) AS nama,
      		  (
      				CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      						mdata_alkes.idkategori
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      						mdata_obat.idkategori
      				END
      			) AS idkategori,
      		  (
      				CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      						mdata_alkes.kode
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      						mdata_obat.kode
      				END
      			) AS kode,
      			tpasien_pengembalian_detail.idtipe AS idtipe,
      			tpasien_pengembalian_detail.kuantitas AS kuantitas,
      			tpasien_pengembalian_detail.harga_dasar AS hargadasar,
      			tpasien_pengembalian_detail.margin AS margin,
      			tpasien_pengembalian_detail.harga AS harga,
      			tpasien_pengembalian_detail.diskon_rp AS diskon,
      			0 AS tuslah,
      			tpasien_pengembalian_detail.totalharga AS totalharga,
      			tpasien_pengembalian_detail.statusverifikasi AS statusverifikasi,
      			tpasien_pengembalian_detail.stracikan AS statusracikan
      		FROM
      			tpasien_pengembalian
      		JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan
      		JOIN tpasien_pengembalian_detail ON tpasien_pengembalian_detail.idpengembalian = tpasien_pengembalian.id
      		JOIN munitpelayanan ON munitpelayanan.id = tpasien_pengembalian_detail.idunit
      		LEFT JOIN mdata_obat ON tpasien_pengembalian_detail.idbarang = mdata_obat.id
      		LEFT JOIN mdata_alkes ON tpasien_pengembalian_detail.idbarang = mdata_alkes.id
      		WHERE
      			tpasien_pengembalian_detail.kuantitas > 0
      	) AS view_union_farmasi_retur
      JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = view_union_farmasi_retur.idtindakan AND view_union_farmasi_retur.asalrujukan IN ( 2, 3 )
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_union_farmasi_retur.iddetail AND tverifikasi_transaksi_detail.id_kelompok = 18
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran AND
      	view_union_farmasi_retur.idtipe = 1 AND
      	view_union_farmasi_retur.idkategori != 2
      ");
		return $query->result();
	}

	public function viewRincianFarmasiReturRanapAlkesBantu($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	view_union_farmasi_retur.iddetail AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	view_union_farmasi_retur.nopengembalian AS nopengembalian,
      	view_union_farmasi_retur.nopengembalian AS nopenjualan,
      	cast( view_union_farmasi_retur.tanggal AS date ) AS tanggal,
      	view_union_farmasi_retur.nama AS namatarif,
      	view_union_farmasi_retur.idunitpelayanan AS idunitpelayanan,
      	view_union_farmasi_retur.unitpelayanan AS unitpelayanan,
      	view_union_farmasi_retur.idtipe AS idtipe,
      	view_union_farmasi_retur.idkategori AS idkategori,
      	(view_union_farmasi_retur.kuantitas * -1) AS kuantitas,
      	view_union_farmasi_retur.hargadasar AS hargadasar,
      	view_union_farmasi_retur.margin AS margin,
      	view_union_farmasi_retur.harga AS hargajual,
      	view_union_farmasi_retur.diskon AS diskon,
      	view_union_farmasi_retur.tuslah AS tuslah,
      	(view_union_farmasi_retur.totalharga * -1) AS totalkeseluruhan,
      	view_union_farmasi_retur.statusracikan AS statusracikan,
      	view_union_farmasi_retur.statusverifikasi AS statusverifikasi,
      	29 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      	(
      		SELECT
      			tpasien_pengembalian.id AS id,
      			tpasien_pengembalian.nopengembalian AS nopengembalian,
      			tpasien_pengembalian_detail.id AS iddetail,
      			tpasien_penjualan.idtindakan AS idtindakan,
      			tpasien_pengembalian.tanggal AS tanggal,
      			tpasien_pengembalian_detail.idunit AS idunitpelayanan,
      			munitpelayanan.nama AS unitpelayanan,
      			tpasien_penjualan.asalrujukan AS asalrujukan,
      		  (
      			 CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      					   mdata_alkes.nama
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      					   mdata_obat.nama
      				END
      			) AS nama,
      		  (
      				CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      						mdata_alkes.idkategori
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      						mdata_obat.idkategori
      				END
      			) AS idkategori,
      		  (
      				CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      						mdata_alkes.kode
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      						mdata_obat.kode
      				END
      			) AS kode,
      			tpasien_pengembalian_detail.idtipe AS idtipe,
      			tpasien_pengembalian_detail.kuantitas AS kuantitas,
      			tpasien_pengembalian_detail.harga_dasar AS hargadasar,
      			tpasien_pengembalian_detail.margin AS margin,
      			tpasien_pengembalian_detail.harga AS harga,
      			tpasien_pengembalian_detail.diskon_rp AS diskon,
      			0 AS tuslah,
      			tpasien_pengembalian_detail.totalharga AS totalharga,
      			tpasien_pengembalian_detail.statusverifikasi AS statusverifikasi,
      			tpasien_pengembalian_detail.stracikan AS statusracikan
      		FROM
      			tpasien_pengembalian
      		JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan
      		JOIN tpasien_pengembalian_detail ON tpasien_pengembalian_detail.idpengembalian = tpasien_pengembalian.id
      		JOIN munitpelayanan ON munitpelayanan.id = tpasien_pengembalian_detail.idunit
      		LEFT JOIN mdata_obat ON tpasien_pengembalian_detail.idbarang = mdata_obat.id
      		LEFT JOIN mdata_alkes ON tpasien_pengembalian_detail.idbarang = mdata_alkes.id
      		WHERE
      			tpasien_pengembalian_detail.kuantitas > 0
      	) AS view_union_farmasi_retur
      JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = view_union_farmasi_retur.idtindakan AND view_union_farmasi_retur.asalrujukan IN ( 2, 3 )
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_union_farmasi_retur.iddetail AND tverifikasi_transaksi_detail.id_kelompok = 18
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran AND
      	view_union_farmasi_retur.idtipe = 1 AND
      	view_union_farmasi_retur.idkategori = 2
      ");
		return $query->result();
	}

	public function viewRincianRanapAlkes($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	trawatinap_alkes.id AS iddetail,
      	trawatinap_alkes.idrawatinap AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      	trawatinap_alkes.tanggal AS tanggal,
      	trawatinap_alkes.idunit AS idunit,
      	COALESCE ( munitpelayanan.nama, '-' ) AS namaunit,
      	trawatinap_alkes.idtipe AS idtipe,
      	mdata_alkes.idkategori AS idkategori,
      	trawatinap_alkes.idalkes AS idalkes,
      	CASE WHEN trawatinap_alkes.idtipe=1 THEN mdata_alkes.nama ELSE mdata_implan.nama END AS namatarif,
      	trawatinap_alkes.hargadasar AS hargadasar,
      	trawatinap_alkes.margin AS margin,
      	trawatinap_alkes.hargajual AS hargajual,
      	trawatinap_alkes.kuantitas AS kuantitas,
      	IFNULL(trawatinap_alkes.diskon,0) AS diskon,
      	trawatinap_alkes.totalkeseluruhan AS totalkeseluruhan,
      	trawatinap_alkes.dari AS dari,
      	trawatinap_alkes.status AS status,
      	trawatinap_alkes.statusverifikasi AS statusverifikasi
      FROM
      	trawatinap_alkes
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_alkes.idrawatinap
      LEFT JOIN mdata_alkes ON mdata_alkes.id = trawatinap_alkes.idalkes AND trawatinap_alkes.idtipe='1'
      LEFT JOIN mdata_implan ON mdata_implan.id = trawatinap_alkes.idalkes AND trawatinap_alkes.idtipe='2'
      LEFT JOIN munitpelayanan ON munitpelayanan.id = trawatinap_alkes.idunit
      WHERE
      	trawatinap_alkes.status = 1 AND
      	trawatinap_alkes.idrawatinap = $idpendaftaran AND
      	(mdata_alkes.idkategori != 2 OR mdata_implan.idkategori != 2)
      ");
		return $query->result();
	}

	public function viewRincianRanapAlkesBantu($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	trawatinap_alkes.id AS iddetail,
      	trawatinap_alkes.idrawatinap AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      	trawatinap_alkes.tanggal AS tanggal,
      	trawatinap_alkes.idunit AS idunit,
      	COALESCE ( munitpelayanan.nama, '-' ) AS namaunit,
      	trawatinap_alkes.idtipe AS idtipe,
      	mdata_alkes.idkategori AS idkategori,
      	trawatinap_alkes.idalkes AS idalkes,
      	mdata_alkes.nama AS namatarif,
      	trawatinap_alkes.hargadasar AS hargadasar,
      	trawatinap_alkes.margin AS margin,
      	trawatinap_alkes.hargajual AS hargajual,
      	trawatinap_alkes.kuantitas AS kuantitas,
      	trawatinap_alkes.diskon AS diskon,
      	trawatinap_alkes.totalkeseluruhan AS totalkeseluruhan,
      	trawatinap_alkes.dari AS dari,
      	trawatinap_alkes.status AS status,
      	trawatinap_alkes.statusverifikasi AS statusverifikasi
      FROM
      	trawatinap_alkes
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_alkes.idrawatinap
      LEFT JOIN mdata_alkes ON mdata_alkes.id = trawatinap_alkes.idalkes
      LEFT JOIN munitpelayanan ON munitpelayanan.id = trawatinap_alkes.idunit
      WHERE
      	trawatinap_alkes.status = 1 AND
      	trawatinap_alkes.idrawatinap = $idpendaftaran AND
      	mdata_alkes.idkategori = 2
      ");
		return $query->result();
	}

	public function viewRincianRanapObat($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	trawatinap_obat.id AS iddetail,
      	trawatinap_obat.idrawatinap AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      	trawatinap_obat.tanggal AS tanggal,
      	trawatinap_obat.idunit AS idunit,
      	COALESCE ( munitpelayanan.nama, '-' ) AS namaunit,
      	trawatinap_obat.idtipe AS idtipe,
      	mdata_obat.idkategori AS idkategori,
      	trawatinap_obat.idobat AS idobat,
      	mdata_obat.nama AS namatarif,
      	trawatinap_obat.hargadasar AS hargadasar,
      	trawatinap_obat.margin AS margin,
      	trawatinap_obat.hargajual AS hargajual,
      	trawatinap_obat.kuantitas AS kuantitas,
      	IFNULL(trawatinap_obat.diskon,0) AS diskon,
      	trawatinap_obat.totalkeseluruhan AS totalkeseluruhan,
      	trawatinap_obat.dari AS dari,
      	trawatinap_obat.status AS status,
      	trawatinap_obat.statusverifikasi AS statusverifikasi
      FROM
      	trawatinap_obat
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_obat.idrawatinap
      LEFT JOIN mdata_obat ON mdata_obat.id = trawatinap_obat.idobat
      LEFT JOIN munitpelayanan ON munitpelayanan.id = trawatinap_obat.idunit
      WHERE
      	trawatinap_obat.status = 1 AND
      	trawatinap_obat.idrawatinap = $idpendaftaran
      ");
		return $query->result();
	}

	public function viewRincianRajalTindakanFilteredIGD($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	view_rincian_rajal_tindakan.iddetail AS iddetail,
      	view_rincian_rajal_tindakan.idpoliklinik AS idpoliklinik,
      	view_rincian_rajal_tindakan.idtindakan AS idtindakan,
      	view_rincian_rajal_tindakan.idrawatinap AS idrawatinap,
      	view_rincian_rajal_tindakan.idtipe AS idtipe,
      	view_rincian_rajal_tindakan.nopendaftaran AS nopendaftaran,
      	view_rincian_rajal_tindakan.tanggal AS tanggal,
        view_rincian_rajal_tindakan.idkategori AS idkategori,
        view_rincian_rajal_tindakan.namakategori AS namakategori,
      	view_rincian_rajal_tindakan.iddokter AS iddokter,
      	view_rincian_rajal_tindakan.namadokter AS namadokter,
      	view_rincian_rajal_tindakan.idpelayanan AS idpelayanan,
      	view_rincian_rajal_tindakan.idtarif AS idtarif,
      	view_rincian_rajal_tindakan.namatarif AS namatarif,
      	view_rincian_rajal_tindakan.jasasarana AS jasasarana,
      	view_rincian_rajal_tindakan.jasasarana_disc AS jasasarana_disc,
      	view_rincian_rajal_tindakan.jasapelayanan AS jasapelayanan,
      	view_rincian_rajal_tindakan.jasapelayanan_disc AS jasapelayanan_disc,
      	view_rincian_rajal_tindakan.jasamedis AS jasamedis,
      	view_rincian_rajal_tindakan.bhp AS bhp,
      	view_rincian_rajal_tindakan.bhp_disc AS bhp_disc,
      	view_rincian_rajal_tindakan.biayaperawatan AS biayaperawatan,
      	view_rincian_rajal_tindakan.biayaperawatan_disc AS biayaperawatan_disc,
      	view_rincian_rajal_tindakan.total AS subtotal,
      	view_rincian_rajal_tindakan.kuantitas AS kuantitas,
      	view_rincian_rajal_tindakan.total * view_rincian_rajal_tindakan.kuantitas AS total,
      	view_rincian_rajal_tindakan.diskon AS diskon,
      	view_rincian_rajal_tindakan.totalkeseluruhan AS totalkeseluruhan,
      	view_rincian_rajal_tindakan.statusverifikasi AS statusverifikasi,
      	view_rincian_rajal_tindakan.status AS status,
      	view_rincian_rajal_tindakan.kelompok AS kelompok,
      	view_rincian_rajal_tindakan.harga_revisi AS harga_revisi,
      	view_rincian_rajal_tindakan.idverifdetail AS idverifdetail,
        view_rincian_rajal_tindakan.potongan_rs AS potongan_rs,
        view_rincian_rajal_tindakan.pajak_dokter AS pajak_dokter,
        view_rincian_rajal_tindakan.periode_pembayaran AS periode_pembayaran,
        view_rincian_rajal_tindakan.periode_jatuhtempo AS periode_jatuhtempo,
        view_rincian_rajal_tindakan.status_jasamedis AS status_jasamedis
      FROM
      	(
      		SELECT
      			tpoliklinik_pelayanan.id AS iddetail,
      			tpoliklinik_pendaftaran.id AS idpoliklinik,
      			tpoliklinik_pelayanan.idtindakan AS idtindakan,
      			trawatinap_pendaftaran.id AS idrawatinap,
      			tpoliklinik_pendaftaran.idtipe AS idtipe,
      			tpoliklinik_pendaftaran.nopendaftaran AS nopendaftaran,
      			cast( tpoliklinik_pendaftaran.tanggaldaftar AS date ) AS tanggal,
            mdokter_kategori.id AS idkategori,
            mdokter_kategori.nama AS namakategori,
      			tpoliklinik_pelayanan.iddokter AS iddokter,
      			mdokter.nama AS namadokter,
      			tpoliklinik_pelayanan.idpelayanan AS idpelayanan,
      			mtarif_rawatjalan.id AS idtarif,
      			mtarif_rawatjalan.nama AS namatarif,
      			tpoliklinik_pelayanan.jasasarana AS jasasarana,
      			tpoliklinik_pelayanan.jasasarana_disc AS jasasarana_disc,
      			tpoliklinik_pelayanan.jasapelayanan AS jasapelayanan,
      			tpoliklinik_pelayanan.jasapelayanan_disc AS jasapelayanan_disc,
      			(tpoliklinik_pelayanan.jasapelayanan - tpoliklinik_pelayanan.jasapelayanan_disc) * tpoliklinik_pelayanan.kuantitas AS jasamedis,
      			tpoliklinik_pelayanan.bhp AS bhp,
      			tpoliklinik_pelayanan.bhp_disc AS bhp_disc,
      			tpoliklinik_pelayanan.biayaperawatan AS biayaperawatan,
      			tpoliklinik_pelayanan.biayaperawatan_disc AS biayaperawatan_disc,
      			tpoliklinik_pelayanan.total AS total,
      			tpoliklinik_pelayanan.kuantitas AS kuantitas,
      			tpoliklinik_pelayanan.diskon AS diskon,
      			tpoliklinik_pelayanan.totalkeseluruhan AS totalkeseluruhan,
      			tpoliklinik_pelayanan.statusverifikasi AS statusverifikasi,
      			tpoliklinik_pelayanan.status AS status,
      			9 AS kelompok,
      			tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      			tverifikasi_transaksi_detail.id AS idverifdetail,
      			tkasir.status AS status_kasir,
            tpoliklinik_pelayanan.pot_rs AS potongan_rs,
          	tpoliklinik_pelayanan.pajak_dokter AS pajak_dokter,
          	tpoliklinik_pelayanan.periode_pembayaran AS periode_pembayaran,
          	tpoliklinik_pelayanan.periode_jatuhtempo AS periode_jatuhtempo,
          	tpoliklinik_pelayanan.status_jasamedis AS status_jasamedis
      		FROM
      			tpoliklinik_pelayanan
      		LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpoliklinik_pelayanan.idtindakan AND tpoliklinik_tindakan.status = 1
      		LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
      		LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
      		LEFT JOIN trawatinap_tindakan ON trawatinap_tindakan.idrawatinap = trawatinap_pendaftaran.id
      		LEFT JOIN mdokter ON mdokter.id = tpoliklinik_pelayanan.iddokter
      		LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
      		LEFT JOIN mtarif_rawatjalan ON mtarif_rawatjalan.id = tpoliklinik_pelayanan.idpelayanan
      		LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tpoliklinik_pelayanan.id AND tverifikasi_transaksi_detail.id_kelompok = 9
      		JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
      		WHERE
      			tpoliklinik_pelayanan.status = 1 AND
            trawatinap_pendaftaran.id = $idpendaftaran
      		GROUP BY
      			trawatinap_tindakan.idrawatinap,
      			tpoliklinik_pelayanan.id
      	) AS view_rincian_rajal_tindakan
      JOIN tkasir ON tkasir.idtindakan = view_rincian_rajal_tindakan.idtindakan AND tkasir.idtipe IN ( 1, 2 ) AND ( tkasir.status <> 2 )
      WHERE
       view_rincian_rajal_tindakan.idrawatinap = $idpendaftaran AND
       view_rincian_rajal_tindakan.idtipe = 2
      ");
		return $query->result();
	}

	public function viewRincianRajalObatFilteredIGD($idpendaftaran, $idtipe)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	tpoliklinik_obat.id AS iddetail,
      	tpoliklinik_pendaftaran.id AS idpoliklinik,
      	tpoliklinik_tindakan.id AS idtindakan,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      	tpoliklinik_pendaftaran.idtipe AS idtipe,
      	tpoliklinik_pendaftaran.nopendaftaran AS nopendaftaran,
      	cast( tpoliklinik_pendaftaran.tanggaldaftar AS date ) AS tanggal,
      	mdata_obat.id AS idtarif,
      	mdata_obat.nama AS namatarif,
      	mdata_obat.kode AS kode,
      	munitpelayanan.id AS idunit,
      	munitpelayanan.nama AS namaunit,
      	tpoliklinik_obat.hargadasar AS hargadasar,
      	tpoliklinik_obat.margin AS margin,
      	tpoliklinik_obat.hargajual AS hargajual,
      	tpoliklinik_obat.kuantitas AS kuantitas,
      	tpoliklinik_obat.diskon AS diskon,
      	tpoliklinik_obat.totalkeseluruhan AS totalkeseluruhan,
      	tpoliklinik_obat.statusverifikasi AS statusverifikasi,
      	7 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail,
      	tkasir.status AS status_kasir
      FROM
      	tpoliklinik_obat
      LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpoliklinik_obat.idtindakan AND tpoliklinik_tindakan.status = 1
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
      LEFT JOIN trawatinap_tindakan ON trawatinap_tindakan.idrawatinap = trawatinap_pendaftaran.id
      LEFT JOIN mdata_obat ON mdata_obat.id = tpoliklinik_obat.idobat
      LEFT JOIN munitpelayanan ON munitpelayanan.id = tpoliklinik_obat.idunit
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tpoliklinik_obat.id AND tverifikasi_transaksi_detail.id_kelompok = 7
      JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
      WHERE
      	tpoliklinik_obat.status = 1 AND
        trawatinap_pendaftaran.id = $idpendaftaran AND
        tpoliklinik_pendaftaran.idtipe = $idtipe
      GROUP BY
      	trawatinap_tindakan.idrawatinap,
      	tpoliklinik_obat.id
      ");
		return $query->result();
	}

	public function viewRincianRajalAlkesFilteredIGD($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	view_rincian_rajal_alkes.iddetail AS iddetail,
      	view_rincian_rajal_alkes.idpoliklinik AS idpoliklinik,
      	view_rincian_rajal_alkes.idtindakan AS idtindakan,
      	view_rincian_rajal_alkes.idrawatinap AS idrawatinap,
      	view_rincian_rajal_alkes.idkelompokpasien AS idkelompokpasien,
      	view_rincian_rajal_alkes.idtipe AS idtipe,
      	view_rincian_rajal_alkes.nopendaftaran AS nopendaftaran,
      	view_rincian_rajal_alkes.tanggal AS tanggal,
      	view_rincian_rajal_alkes.idtarif AS idtarif,
      	view_rincian_rajal_alkes.namatarif AS namatarif,
      	view_rincian_rajal_alkes.kode AS kode,
      	view_rincian_rajal_alkes.idunit AS idunit,
      	view_rincian_rajal_alkes.namaunit AS namaunit,
      	view_rincian_rajal_alkes.hargadasar AS hargadasar,
      	view_rincian_rajal_alkes.margin AS margin,
      	view_rincian_rajal_alkes.hargajual AS hargajual,
      	view_rincian_rajal_alkes.kuantitas AS kuantitas,
      	view_rincian_rajal_alkes.diskon AS diskon,
      	view_rincian_rajal_alkes.totalkeseluruhan AS totalkeseluruhan,
      	view_rincian_rajal_alkes.statusverifikasi AS statusverifikasi,
      	view_rincian_rajal_alkes.kelompok AS kelompok,
      	view_rincian_rajal_alkes.harga_revisi AS harga_revisi,
      	view_rincian_rajal_alkes.idverifdetail AS idverifdetail,
      	tkasir.id AS id
      FROM
      	(
        SELECT
          tpoliklinik_alkes.id AS iddetail,
          tpoliklinik_pendaftaran.id AS idpoliklinik,
          tpoliklinik_tindakan.id AS idtindakan,
          trawatinap_pendaftaran.id AS idrawatinap,
          trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
          tpoliklinik_pendaftaran.idtipe AS idtipe,
          tpoliklinik_pendaftaran.nopendaftaran AS nopendaftaran,
          cast( tpoliklinik_pendaftaran.tanggaldaftar AS date ) AS tanggal,
          CASE WHEN tpoliklinik_alkes.idtipe='1' THEN  mdata_alkes.id ELSE mdata_implan.id END  AS idtarif,
          CASE WHEN tpoliklinik_alkes.idtipe='1' THEN  mdata_alkes.nama ELSE mdata_implan.nama END  AS namatarif,
          CASE WHEN tpoliklinik_alkes.idtipe='1' THEN  mdata_alkes.kode ELSE mdata_implan.kode END  AS kode,
          munitpelayanan.id AS idunit,
          munitpelayanan.nama AS namaunit,
          tpoliklinik_alkes.hargadasar AS hargadasar,
          tpoliklinik_alkes.margin AS margin,
          tpoliklinik_alkes.hargajual AS hargajual,
          tpoliklinik_alkes.kuantitas AS kuantitas,
          tpoliklinik_alkes.diskon AS diskon,
          tpoliklinik_alkes.totalkeseluruhan AS totalkeseluruhan,
          tpoliklinik_alkes.statusverifikasi AS statusverifikasi,
          2 AS kelompok,
          tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
          tverifikasi_transaksi_detail.id AS idverifdetail,
          tkasir.status AS status_kasir
        FROM
          tpoliklinik_alkes
        LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpoliklinik_alkes.idtindakan AND tpoliklinik_tindakan.status = 1
        LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
        LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
        LEFT JOIN trawatinap_tindakan ON trawatinap_tindakan.idrawatinap = trawatinap_pendaftaran.id
        LEFT JOIN mdata_alkes ON mdata_alkes.id = tpoliklinik_alkes.idalkes AND tpoliklinik_alkes.idtipe='1'
        LEFT JOIN mdata_implan ON mdata_implan.id = tpoliklinik_alkes.idalkes AND tpoliklinik_alkes.idtipe='2'
        LEFT JOIN munitpelayanan ON munitpelayanan.id = tpoliklinik_alkes.idunit
        LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tpoliklinik_alkes.id AND tverifikasi_transaksi_detail.id_kelompok = 2
        JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
        WHERE
          tpoliklinik_alkes.status = 1 AND
          trawatinap_pendaftaran.id = $idpendaftaran
        GROUP BY
          trawatinap_tindakan.idrawatinap,
          tpoliklinik_alkes.id
        ) AS view_rincian_rajal_alkes
      JOIN tkasir ON tkasir.idtindakan = view_rincian_rajal_alkes.idtindakan AND tkasir.idtipe IN ( 1, 2 ) AND ( tkasir.status <> 2 )
      WHERE
        view_rincian_rajal_alkes.idrawatinap = $idpendaftaran AND
        view_rincian_rajal_alkes.idtipe = 2
      ");
		return $query->result();
	}

	public function viewRincianFarmasiReturRajalObat($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	view_union_farmasi_retur.iddetail AS iddetail,
      	tpoliklinik_pendaftaran.id AS idpoliklinik,
		tpoliklinik_tindakan.id AS idtindakan,
		trawatinap_pendaftaran.id AS idrawatinap,
		trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      	view_union_farmasi_retur.nopengembalian AS nopengembalian,
      	view_union_farmasi_retur.nopengembalian AS nopenjualan,
      	cast( view_union_farmasi_retur.tanggal AS date ) AS tanggal,
      	view_union_farmasi_retur.idtarif AS idtarif,
      	view_union_farmasi_retur.nama AS namatarif,
      	view_union_farmasi_retur.idunitpelayanan AS idunitpelayanan,
      	view_union_farmasi_retur.unitpelayanan AS unitpelayanan,
      	view_union_farmasi_retur.idtipe AS idtipe,
      	view_union_farmasi_retur.idkategori AS idkategori,
      	(view_union_farmasi_retur.kuantitas * -1) AS kuantitas,
      	view_union_farmasi_retur.hargadasar AS hargadasar,
      	view_union_farmasi_retur.margin AS margin,
      	view_union_farmasi_retur.harga AS hargajual,
      	view_union_farmasi_retur.diskon AS diskon,
      	view_union_farmasi_retur.tuslah AS tuslah,
      	(view_union_farmasi_retur.totalharga * -1) AS totalkeseluruhan,
      	view_union_farmasi_retur.statusracikan AS statusracikan,
      	view_union_farmasi_retur.statusverifikasi AS statusverifikasi,
      	29 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      	(
      		SELECT
      			tpasien_pengembalian.id AS id,
      			tpasien_pengembalian.nopengembalian AS nopengembalian,
      			tpasien_pengembalian_detail.id AS iddetail,
      			tpasien_penjualan.idtindakan AS idtindakan,
      			tpasien_pengembalian.tanggal AS tanggal,
      			tpasien_pengembalian_detail.idunit AS idunitpelayanan,
      			munitpelayanan.nama AS unitpelayanan,
      			tpasien_penjualan.asalrujukan AS asalrujukan,
      		  (
      			 CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      					   mdata_alkes.id
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      					   mdata_obat.id
      				END
      			) AS idtarif,
      		  (
      			 CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      					   mdata_alkes.nama
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      					   mdata_obat.nama
      				END
      			) AS nama,
      		  (
      				CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      						mdata_alkes.idkategori
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      						mdata_obat.idkategori
      				END
      			) AS idkategori,
      		  (
      				CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      						mdata_alkes.kode
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      						mdata_obat.kode
      				END
      			) AS kode,
      			tpasien_pengembalian_detail.idtipe AS idtipe,
      			tpasien_pengembalian_detail.kuantitas AS kuantitas,
      			tpasien_pengembalian_detail.harga_dasar AS hargadasar,
      			tpasien_pengembalian_detail.margin AS margin,
      			tpasien_pengembalian_detail.harga AS harga,
      			tpasien_pengembalian_detail.diskon_rp AS diskon,
      			0 AS tuslah,
      			tpasien_pengembalian_detail.totalharga AS totalharga,
      			tpasien_pengembalian_detail.statusverifikasi AS statusverifikasi,
      			tpasien_pengembalian_detail.stracikan AS statusracikan
      		FROM
      			tpasien_pengembalian
      		JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan
      		JOIN tpasien_pengembalian_detail ON tpasien_pengembalian_detail.idpengembalian = tpasien_pengembalian.id
      		JOIN munitpelayanan ON munitpelayanan.id = tpasien_pengembalian_detail.idunit
      		LEFT JOIN mdata_obat ON tpasien_pengembalian_detail.idbarang = mdata_obat.id
      		LEFT JOIN mdata_alkes ON tpasien_pengembalian_detail.idbarang = mdata_alkes.id
      		WHERE
      			tpasien_pengembalian_detail.kuantitas > 0
      	) AS view_union_farmasi_retur
	  LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = view_union_farmasi_retur.idtindakan AND view_union_farmasi_retur.asalrujukan IN (1, 2) AND tpoliklinik_tindakan.status = 1
	  LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
	  LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_union_farmasi_retur.iddetail AND tverifikasi_transaksi_detail.id_kelompok = 18
	  JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 ) AND ( tkasir.status <> 2 )
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran AND
      	view_union_farmasi_retur.idtipe = 3
      ");
		return $query->result();
	}

	public function viewRincianFarmasiReturRajalAlkes($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	view_union_farmasi_retur.iddetail AS iddetail,
      	tpoliklinik_pendaftaran.id AS idpoliklinik,
		tpoliklinik_tindakan.id AS idtindakan,
		trawatinap_pendaftaran.id AS idrawatinap,
		trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      	view_union_farmasi_retur.nopengembalian AS nopengembalian,
      	view_union_farmasi_retur.nopengembalian AS nopenjualan,
      	cast( view_union_farmasi_retur.tanggal AS date ) AS tanggal,
      	view_union_farmasi_retur.idtarif AS idtarif,
      	view_union_farmasi_retur.nama AS namatarif,
      	view_union_farmasi_retur.idunitpelayanan AS idunitpelayanan,
      	view_union_farmasi_retur.unitpelayanan AS unitpelayanan,
      	view_union_farmasi_retur.idtipe AS idtipe,
      	view_union_farmasi_retur.idkategori AS idkategori,
      	(view_union_farmasi_retur.kuantitas * -1) AS kuantitas,
      	view_union_farmasi_retur.hargadasar AS hargadasar,
      	view_union_farmasi_retur.margin AS margin,
      	view_union_farmasi_retur.harga AS hargajual,
      	view_union_farmasi_retur.diskon AS diskon,
      	view_union_farmasi_retur.tuslah AS tuslah,
      	(view_union_farmasi_retur.totalharga * -1) AS totalkeseluruhan,
      	view_union_farmasi_retur.statusracikan AS statusracikan,
      	view_union_farmasi_retur.statusverifikasi AS statusverifikasi,
      	29 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      	(
      		SELECT
      			tpasien_pengembalian.id AS id,
      			tpasien_pengembalian.nopengembalian AS nopengembalian,
      			tpasien_pengembalian_detail.id AS iddetail,
      			tpasien_penjualan.idtindakan AS idtindakan,
      			tpasien_pengembalian.tanggal AS tanggal,
      			tpasien_pengembalian_detail.idunit AS idunitpelayanan,
      			munitpelayanan.nama AS unitpelayanan,
      			tpasien_penjualan.asalrujukan AS asalrujukan,
      		  (
      			 CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      					   mdata_alkes.id
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      					   mdata_obat.id
      				END
      			) AS idtarif,
      		  (
      			 CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      					   mdata_alkes.nama
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      					   mdata_obat.nama
      				END
      			) AS nama,
      		  (
      				CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      						mdata_alkes.idkategori
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      						mdata_obat.idkategori
      				END
      			) AS idkategori,
      		  (
      				CASE
      					WHEN ( tpasien_pengembalian_detail.idtipe = 1 ) THEN
      						mdata_alkes.kode
      					WHEN ( tpasien_pengembalian_detail.idtipe = 3 ) THEN
      						mdata_obat.kode
      				END
      			) AS kode,
      			tpasien_pengembalian_detail.idtipe AS idtipe,
      			tpasien_pengembalian_detail.kuantitas AS kuantitas,
      			tpasien_pengembalian_detail.harga_dasar AS hargadasar,
      			tpasien_pengembalian_detail.margin AS margin,
      			tpasien_pengembalian_detail.harga AS harga,
      			tpasien_pengembalian_detail.diskon_rp AS diskon,
      			0 AS tuslah,
      			tpasien_pengembalian_detail.totalharga AS totalharga,
      			tpasien_pengembalian_detail.statusverifikasi AS statusverifikasi,
      			tpasien_pengembalian_detail.stracikan AS statusracikan
      		FROM
      			tpasien_pengembalian
      		JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan
      		JOIN tpasien_pengembalian_detail ON tpasien_pengembalian_detail.idpengembalian = tpasien_pengembalian.id
      		JOIN munitpelayanan ON munitpelayanan.id = tpasien_pengembalian_detail.idunit
      		LEFT JOIN mdata_obat ON tpasien_pengembalian_detail.idbarang = mdata_obat.id
      		LEFT JOIN mdata_alkes ON tpasien_pengembalian_detail.idbarang = mdata_alkes.id
      		WHERE
      			tpasien_pengembalian_detail.kuantitas > 0
      	) AS view_union_farmasi_retur
      LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = view_union_farmasi_retur.idtindakan AND view_union_farmasi_retur.asalrujukan IN (1, 2) AND tpoliklinik_tindakan.status = 1
	  LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
	  LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_union_farmasi_retur.iddetail AND tverifikasi_transaksi_detail.id_kelompok = 18
	  JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 ) AND ( tkasir.status <> 2 )
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran AND
      	view_union_farmasi_retur.idtipe = 1 AND
      	view_union_farmasi_retur.idkategori != 2
      ");
		return $query->result();
	}

	public function viewRincianLaboratorium($idpendaftaran, $idtipe)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}
$q="SELECT
      	view_rincian_laboratorium_rajal.iddetail AS iddetail,
      	view_rincian_laboratorium_rajal.idrawatinap AS idrawatinap,
      	view_rincian_laboratorium_rajal.idkelas AS idkelas,
      	view_rincian_laboratorium_rajal.idrujukan AS idrujukan,
      	view_rincian_laboratorium_rajal.norujukan AS norujukan,
      	view_rincian_laboratorium_rajal.tanggal AS tanggal,
      	view_rincian_laboratorium_rajal.idtipe AS idtipe,
      	view_rincian_laboratorium_rajal.idlaboratorium AS idtarif,
      	view_rincian_laboratorium_rajal.idlaboratorium AS idlaboratorium,
        view_rincian_laboratorium_rajal.idkategori AS idkategori,
        view_rincian_laboratorium_rajal.namakategori AS namakategori,
      	view_rincian_laboratorium_rajal.iddokter AS iddokter,
      	view_rincian_laboratorium_rajal.namadokter AS namadokter,
      	view_rincian_laboratorium_rajal.namatarif AS namatarif,
      	view_rincian_laboratorium_rajal.jasasarana AS jasasarana,
      	view_rincian_laboratorium_rajal.jasasarana_disc AS jasasarana_disc,
      	view_rincian_laboratorium_rajal.jasapelayanan AS jasapelayanan,
      	view_rincian_laboratorium_rajal.jasapelayanan_disc AS jasapelayanan_disc,
      	view_rincian_laboratorium_rajal.jasamedis AS jasamedis,
      	view_rincian_laboratorium_rajal.bhp AS bhp,
      	view_rincian_laboratorium_rajal.bhp AS bhp_disc,
      	view_rincian_laboratorium_rajal.biayaperawatan AS biayaperawatan,
      	view_rincian_laboratorium_rajal.biayaperawatan_disc AS biayaperawatan_disc,
      	view_rincian_laboratorium_rajal.total AS subtotal,
      	view_rincian_laboratorium_rajal.kuantitas AS kuantitas,
      	view_rincian_laboratorium_rajal.diskon AS diskon,
      	view_rincian_laboratorium_rajal.total * view_rincian_laboratorium_rajal.kuantitas AS total,
      	view_rincian_laboratorium_rajal.totalkeseluruhan AS totalkeseluruhan,
      	view_rincian_laboratorium_rajal.STATUS AS STATUS,
      	view_rincian_laboratorium_rajal.statusrincianpaket AS statusrincianpaket,
      	view_rincian_laboratorium_rajal.statusverifikasi AS statusverifikasi,
        view_rincian_laboratorium_rajal.potongan_rs AS potongan_rs,
        view_rincian_laboratorium_rajal.pajak_dokter AS pajak_dokter,
        view_rincian_laboratorium_rajal.periode_pembayaran AS periode_pembayaran,
        view_rincian_laboratorium_rajal.periode_jatuhtempo AS periode_jatuhtempo,
        view_rincian_laboratorium_rajal.status_jasamedis AS status_jasamedis
      FROM
      	(
      		SELECT
      		  trujukan_laboratorium_detail.id AS iddetail,
      		  trawatinap_pendaftaran.id AS idrawatinap,
      		  trawatinap_pendaftaran.idkelas AS idkelas,
      		  tpoliklinik_pendaftaran.id AS idpoliklinik,
      		  trujukan_laboratorium_detail.idrujukan AS idrujukan,
      		  trujukan_laboratorium.norujukan AS norujukan,
      		  cast( trujukan_laboratorium.tanggal AS date ) AS tanggal,
      		  mtarif_laboratorium.idtipe AS idtipe,
      		  trujukan_laboratorium_detail.idlaboratorium AS idlaboratorium,
      		  mtarif_laboratorium.nama AS namatarif,
            mdokter_kategori.id AS idkategori,
            mdokter_kategori.nama AS namakategori,
      		  COALESCE(trujukan_laboratorium_detail.iddokter, trujukan_laboratorium.iddokterperujuk) AS iddokter,
      		  mdokter.nama AS namadokter,
      		  trujukan_laboratorium_detail.jasasarana AS jasasarana,
      		  trujukan_laboratorium_detail.jasasarana_disc AS jasasarana_disc,
      		  trujukan_laboratorium_detail.jasapelayanan AS jasapelayanan,
      		  trujukan_laboratorium_detail.jasapelayanan_disc AS jasapelayanan_disc,
          	(trujukan_laboratorium_detail.jasapelayanan - trujukan_laboratorium_detail.jasapelayanan_disc) * trujukan_laboratorium_detail.kuantitas AS jasamedis,
      		  trujukan_laboratorium_detail.bhp AS bhp,
      		  trujukan_laboratorium_detail.bhp_disc AS bhp_disc,
      		  trujukan_laboratorium_detail.biayaperawatan AS biayaperawatan,
      		  trujukan_laboratorium_detail.biayaperawatan_disc AS biayaperawatan_disc,
      		  trujukan_laboratorium_detail.total AS total,
      		  trujukan_laboratorium_detail.kuantitas AS kuantitas,
      		  trujukan_laboratorium_detail.diskon AS diskon,
      		  trujukan_laboratorium_detail.totalkeseluruhan AS totalkeseluruhan,
      		  trujukan_laboratorium_detail.status AS STATUS,
      		  trujukan_laboratorium_detail.statusrincianpaket AS statusrincianpaket,
      		  trujukan_laboratorium_detail.statusverifikasi AS statusverifikasi,
      		  6 AS kelompok,
      		  tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      		  tverifikasi_transaksi_detail.id AS idverifdetail,
      		  tkasir.status AS status_kasir,
            trujukan_laboratorium_detail.potongan_rs AS potongan_rs,
          	trujukan_laboratorium_detail.pajak_dokter AS pajak_dokter,
          	trujukan_laboratorium_detail.periode_pembayaran AS periode_pembayaran,
          	trujukan_laboratorium_detail.periode_jatuhtempo AS periode_jatuhtempo,
          	trujukan_laboratorium_detail.status_jasamedis AS status_jasamedis
      		FROM
      		  tpoliklinik_pendaftaran
      		LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
      		JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
      		JOIN trujukan_laboratorium ON tpoliklinik_tindakan.id = trujukan_laboratorium.idtindakan AND trujukan_laboratorium.asalrujukan IN ( 1, 2 )
      		JOIN trujukan_laboratorium_detail ON trujukan_laboratorium_detail.idrujukan = trujukan_laboratorium.id
      		LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_laboratorium_detail.iddokter, trujukan_laboratorium.iddokterperujuk)
      		LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
      		JOIN mtarif_laboratorium ON trujukan_laboratorium_detail.idlaboratorium = mtarif_laboratorium.id
      		LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = trujukan_laboratorium_detail.id AND tverifikasi_transaksi_detail.id_kelompok = 6
      		JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
      		WHERE
      		  trujukan_laboratorium.status <> 0 AND
            trawatinap_pendaftaran.id = $idpendaftaran AND
            mtarif_laboratorium.idtipe = $idtipe AND
            trujukan_laboratorium_detail.statusrincianpaket = 0 AND trujukan_laboratorium_detail.status <> 0
      	) AS view_rincian_laboratorium_rajal
      WHERE
        view_rincian_laboratorium_rajal.status_kasir <> 2

      UNION ALL

      SELECT
      	view_rincian_laboratorium_ranap.iddetail AS iddetail,
      	view_rincian_laboratorium_ranap.idrawatinap AS idrawatinap,
      	view_rincian_laboratorium_ranap.idkelas AS idkelas,
      	view_rincian_laboratorium_ranap.idrujukan AS idrujukan,
      	view_rincian_laboratorium_ranap.norujukan AS norujukan,
      	view_rincian_laboratorium_ranap.tanggal AS tanggal,
      	view_rincian_laboratorium_ranap.idtipe AS idtipe,
      	view_rincian_laboratorium_ranap.idlaboratorium AS idtarif,
      	view_rincian_laboratorium_ranap.idlaboratorium AS idlaboratorium,
      	view_rincian_laboratorium_ranap.idkategori AS idkategori,
      	view_rincian_laboratorium_ranap.namakategori AS namakategori,
      	view_rincian_laboratorium_ranap.iddokter AS iddokter,
      	view_rincian_laboratorium_ranap.namadokter AS namadokter,
      	view_rincian_laboratorium_ranap.namatarif AS namatarif,
      	view_rincian_laboratorium_ranap.jasasarana AS jasasarana,
      	view_rincian_laboratorium_ranap.jasasarana_disc AS jasasarana_disc,
      	view_rincian_laboratorium_ranap.jasapelayanan AS jasapelayanan,
      	view_rincian_laboratorium_ranap.jasapelayanan_disc AS jasapelayanan_disc,
      	view_rincian_laboratorium_ranap.jasamedis AS jasamedis,
      	view_rincian_laboratorium_ranap.bhp AS bhp,
      	view_rincian_laboratorium_ranap.bhp AS bhp_disc,
      	view_rincian_laboratorium_ranap.biayaperawatan AS biayaperawatan,
      	view_rincian_laboratorium_ranap.biayaperawatan_disc AS biayaperawatan_disc,
      	view_rincian_laboratorium_ranap.total AS subtotal,
      	view_rincian_laboratorium_ranap.kuantitas AS kuantitas,
      	view_rincian_laboratorium_ranap.diskon AS diskon,
      	view_rincian_laboratorium_ranap.total * view_rincian_laboratorium_ranap.kuantitas AS total,
      	view_rincian_laboratorium_ranap.totalkeseluruhan AS totalkeseluruhan,
      	view_rincian_laboratorium_ranap.status AS status,
      	view_rincian_laboratorium_ranap.statusrincianpaket AS statusrincianpaket,
      	view_rincian_laboratorium_ranap.statusverifikasi AS statusverifikasi,
        view_rincian_laboratorium_ranap.potongan_rs AS potongan_rs,
        view_rincian_laboratorium_ranap.pajak_dokter AS pajak_dokter,
        view_rincian_laboratorium_ranap.periode_pembayaran AS periode_pembayaran,
        view_rincian_laboratorium_ranap.periode_jatuhtempo AS periode_jatuhtempo,
        view_rincian_laboratorium_ranap.status_jasamedis AS status_jasamedis
      FROM
      	(
      		SELECT
      			trujukan_laboratorium_detail.id AS iddetail,
      			trawatinap_pendaftaran.id AS idrawatinap,
      			trawatinap_pendaftaran.idkelas AS idkelas,
      			trujukan_laboratorium_detail.idrujukan AS idrujukan,
      			trujukan_laboratorium.norujukan AS norujukan,
      			cast( trujukan_laboratorium.tanggal AS date ) AS tanggal,
      			mtarif_laboratorium.idtipe AS idtipe,
      			trujukan_laboratorium_detail.idlaboratorium AS idlaboratorium,
      			mtarif_laboratorium.nama AS namatarif,
            mdokter_kategori.id AS idkategori,
            mdokter_kategori.nama AS namakategori,
      			COALESCE(trujukan_laboratorium_detail.iddokter, trujukan_laboratorium.iddokterperujuk) AS iddokter,
      			mdokter.nama AS namadokter,
      			trujukan_laboratorium_detail.jasasarana AS jasasarana,
      			trujukan_laboratorium_detail.jasasarana_disc AS jasasarana_disc,
      			trujukan_laboratorium_detail.jasapelayanan AS jasapelayanan,
      			trujukan_laboratorium_detail.jasapelayanan_disc AS jasapelayanan_disc,
          	(trujukan_laboratorium_detail.jasapelayanan - trujukan_laboratorium_detail.jasapelayanan_disc) * trujukan_laboratorium_detail.kuantitas AS jasamedis,
      			trujukan_laboratorium_detail.bhp AS bhp,
      			trujukan_laboratorium_detail.bhp_disc AS bhp_disc,
      			trujukan_laboratorium_detail.biayaperawatan AS biayaperawatan,
      			trujukan_laboratorium_detail.biayaperawatan_disc AS biayaperawatan_disc,
      			trujukan_laboratorium_detail.total AS total,
      			trujukan_laboratorium_detail.kuantitas AS kuantitas,
      			trujukan_laboratorium_detail.diskon AS diskon,
      			trujukan_laboratorium_detail.totalkeseluruhan AS totalkeseluruhan,
      			trujukan_laboratorium_detail.status AS status,
      			trujukan_laboratorium_detail.statusrincianpaket AS statusrincianpaket,
      			trujukan_laboratorium_detail.statusverifikasi AS statusverifikasi,
            trujukan_laboratorium_detail.potongan_rs AS potongan_rs,
          	trujukan_laboratorium_detail.pajak_dokter AS pajak_dokter,
          	trujukan_laboratorium_detail.periode_pembayaran AS periode_pembayaran,
          	trujukan_laboratorium_detail.periode_jatuhtempo AS periode_jatuhtempo,
          	trujukan_laboratorium_detail.status_jasamedis AS status_jasamedis
      		FROM
      			trawatinap_pendaftaran
      		JOIN trujukan_laboratorium ON trawatinap_pendaftaran.id = trujukan_laboratorium.idtindakan AND trujukan_laboratorium.asalrujukan = 3
      		JOIN trujukan_laboratorium_detail ON trujukan_laboratorium_detail.idrujukan = trujukan_laboratorium.id
      		LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_laboratorium_detail.iddokter, trujukan_laboratorium.iddokterperujuk)
      		LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
      		JOIN mtarif_laboratorium ON trujukan_laboratorium_detail.idlaboratorium = mtarif_laboratorium.id
      		WHERE
      			trujukan_laboratorium.status <> 0 AND
            trawatinap_pendaftaran.id = $idpendaftaran AND
            mtarif_laboratorium.idtipe = $idtipe AND 
            trujukan_laboratorium_detail.statusrincianpaket = 0  AND trujukan_laboratorium_detail.status <> 0
      	) AS view_rincian_laboratorium_ranap

        ORDER BY tanggal";
		// print_r($q);exit;
		$query = $this->db->query($q);
		return $query->result();
	}

	public function viewRincianRadiologi($idpendaftaran, $idtipe)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	view_rincian_radiologi_rajal.iddetail AS iddetail,
      	view_rincian_radiologi_rajal.idrawatinap AS idrawatinap,
      	view_rincian_radiologi_rajal.idkelas AS idkelas,
      	view_rincian_radiologi_rajal.idrujukan AS idrujukan,
      	view_rincian_radiologi_rajal.norujukan AS norujukan,
      	view_rincian_radiologi_rajal.tanggal AS tanggal,
      	view_rincian_radiologi_rajal.idtipe AS idtipe,
      	view_rincian_radiologi_rajal.idradiologi AS idtarif,
      	view_rincian_radiologi_rajal.idradiologi AS idradiologi,
      	view_rincian_radiologi_rajal.namatarif AS namatarif,
        view_rincian_radiologi_rajal.idkategori AS idkategori,
        view_rincian_radiologi_rajal.namakategori AS namakategori,
      	view_rincian_radiologi_rajal.iddokter AS iddokter,
      	view_rincian_radiologi_rajal.namadokter AS namadokter,
      	view_rincian_radiologi_rajal.jasasarana AS jasasarana,
      	view_rincian_radiologi_rajal.jasasarana_disc AS jasasarana_disc,
      	view_rincian_radiologi_rajal.jasapelayanan AS jasapelayanan,
      	view_rincian_radiologi_rajal.jasapelayanan_disc AS jasapelayanan_disc,
      	view_rincian_radiologi_rajal.bhp AS bhp,
      	view_rincian_radiologi_rajal.bhp_disc AS bhp_disc,
      	view_rincian_radiologi_rajal.biayaperawatan AS biayaperawatan,
      	view_rincian_radiologi_rajal.biayaperawatan_disc AS biayaperawatan_disc,
      	view_rincian_radiologi_rajal.total AS subtotal,
      	view_rincian_radiologi_rajal.kuantitas AS kuantitas,
      	view_rincian_radiologi_rajal.diskon AS diskon,
      	view_rincian_radiologi_rajal.total * view_rincian_radiologi_rajal.kuantitas AS total,
      	view_rincian_radiologi_rajal.totalkeseluruhan AS totalkeseluruhan,
      	view_rincian_radiologi_rajal.status AS status,
      	view_rincian_radiologi_rajal.statusverifikasi AS statusverifikasi,
        view_rincian_radiologi_rajal.status_tindakan AS status_tindakan,
        view_rincian_radiologi_rajal.jasamedis AS jasamedis,
        view_rincian_radiologi_rajal.potongan_rs AS potongan_rs,
        view_rincian_radiologi_rajal.pajak_dokter AS pajak_dokter,
        view_rincian_radiologi_rajal.periode_pembayaran AS periode_pembayaran,
        view_rincian_radiologi_rajal.periode_jatuhtempo AS periode_jatuhtempo,
        view_rincian_radiologi_rajal.status_jasamedis AS status_jasamedis,
      	view_rincian_radiologi_rajal.status_tindakan AS status_expertise
      FROM
      	(
      		SELECT
      			trujukan_radiologi_detail.id AS iddetail,
      			trawatinap_pendaftaran.id AS idrawatinap,
      			trawatinap_pendaftaran.idkelas AS idkelas,
      			tpoliklinik_pendaftaran.id AS idpoliklinik,
      			trujukan_radiologi_detail.idrujukan AS idrujukan,
      			trujukan_radiologi.norujukan AS norujukan,
      			cast( trujukan_radiologi.tanggal AS date ) AS tanggal,
      			mtarif_radiologi.idtipe AS idtipe,
      			trujukan_radiologi_detail.idradiologi AS idradiologi,(
      			CASE
      				WHEN ( mtarif_radiologi.idtipe = 1 ) THEN
      					concat(
      						mtarif_radiologi.nama,
      						' (',
      						mtarif_radiologi_expose.nama,
      						' ',
      						mtarif_radiologi_film.nama,
      						')'
      					) ELSE mtarif_radiologi.nama
      				END
      			) AS namatarif,
            mdokter_kategori.id AS idkategori,
            mdokter_kategori.nama AS namakategori,
      			COALESCE(trujukan_radiologi_detail.iddokter, trujukan_radiologi.iddokterradiologi) AS iddokter,
      			mdokter.nama AS namadokter,
      			trujukan_radiologi_detail.jasasarana AS jasasarana,
      			trujukan_radiologi_detail.jasasarana_disc AS jasasarana_disc,
      			trujukan_radiologi_detail.jasapelayanan AS jasapelayanan,
      			trujukan_radiologi_detail.jasapelayanan_disc AS jasapelayanan_disc,
      			trujukan_radiologi_detail.bhp AS bhp,
      			trujukan_radiologi_detail.bhp_disc AS bhp_disc,
      			trujukan_radiologi_detail.biayaperawatan AS biayaperawatan,
      			trujukan_radiologi_detail.biayaperawatan_disc AS biayaperawatan_disc,
      			trujukan_radiologi_detail.total AS total,
      			trujukan_radiologi_detail.kuantitas AS kuantitas,
      			trujukan_radiologi_detail.diskon AS diskon,
      			trujukan_radiologi_detail.totalkeseluruhan AS totalkeseluruhan,
      			trujukan_radiologi_detail.status AS status,
      			trujukan_radiologi_detail.statusverifikasi AS statusverifikasi,
      			8 AS kelompok,
      			tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      			tverifikasi_transaksi_detail.id AS idverifdetail,
      			tkasir.status AS status_kasir,
            COALESCE(trujukan_radiologi_detail.status_tindakan, trujukan_radiologi.statuspasien) AS status_tindakan,
            (
          	CASE
          		WHEN mtarif_radiologi.idtipe = '1' THEN
          		  trujukan_radiologi_detail.nominal_jasamedis * trujukan_radiologi_detail.kuantitas
              ELSE trujukan_radiologi_detail.jasapelayanan
          	END
            ) AS jasamedis,
            trujukan_radiologi_detail.potongan_rs AS potongan_rs,
          	trujukan_radiologi_detail.pajak_dokter AS pajak_dokter,
          	trujukan_radiologi_detail.periode_pembayaran AS periode_pembayaran,
          	trujukan_radiologi_detail.periode_jatuhtempo AS periode_jatuhtempo,
          	trujukan_radiologi_detail.status_jasamedis AS status_jasamedis,
          	trujukan_radiologi_detail.status_tindakan AS status_expertise
      		FROM
      			tpoliklinik_pendaftaran
      		LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
      		JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
      		JOIN trujukan_radiologi ON tpoliklinik_tindakan.id = trujukan_radiologi.idtindakan AND trujukan_radiologi.asalrujukan IN ( 1, 2 )
      		JOIN trujukan_radiologi_detail ON trujukan_radiologi_detail.idrujukan = trujukan_radiologi.id
      		LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_radiologi_detail.iddokter, trujukan_radiologi.iddokterradiologi)
      		LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
      		JOIN mtarif_radiologi ON trujukan_radiologi_detail.idradiologi = mtarif_radiologi.id
      		LEFT JOIN mtarif_radiologi_expose ON mtarif_radiologi.idexpose = mtarif_radiologi_expose.id
      		LEFT JOIN mtarif_radiologi_film ON mtarif_radiologi.idfilm = mtarif_radiologi_film.id
      		LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = trujukan_radiologi_detail.id AND tverifikasi_transaksi_detail.id_kelompok = 8
      		JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
      		WHERE
      			trujukan_radiologi.status <> 0 AND
            trawatinap_pendaftaran.id = $idpendaftaran AND
            mtarif_radiologi.idtipe = $idtipe AND trujukan_radiologi_detail.status <> 0
      	) AS view_rincian_radiologi_rajal
      WHERE
      	view_rincian_radiologi_rajal.status_kasir <> 2

      UNION ALL

      SELECT
      	view_rincian_radiologi_ranap.iddetail AS iddetail,
      	view_rincian_radiologi_ranap.idrawatinap AS idrawatinap,
      	view_rincian_radiologi_ranap.idkelas AS idkelas,
      	view_rincian_radiologi_ranap.idrujukan AS idrujukan,
      	view_rincian_radiologi_ranap.norujukan AS norujukan,
      	view_rincian_radiologi_ranap.tanggal AS tanggal,
      	view_rincian_radiologi_ranap.idtipe AS idtipe,
      	view_rincian_radiologi_ranap.idradiologi AS idtarif,
      	view_rincian_radiologi_ranap.idradiologi AS idradiologi,
      	view_rincian_radiologi_ranap.namatarif AS namatarif,
      	view_rincian_radiologi_ranap.idkategori AS idkategori,
      	view_rincian_radiologi_ranap.namakategori AS namakategori,
      	view_rincian_radiologi_ranap.iddokter AS iddokter,
      	view_rincian_radiologi_ranap.namadokter AS namadokter,
      	view_rincian_radiologi_ranap.jasasarana AS jasasarana,
      	view_rincian_radiologi_ranap.jasasarana_disc AS jasasarana_disc,
      	view_rincian_radiologi_ranap.jasapelayanan AS jasapelayanan,
      	view_rincian_radiologi_ranap.jasapelayanan_disc AS jasapelayanan_disc,
      	view_rincian_radiologi_ranap.bhp AS bhp,
      	view_rincian_radiologi_ranap.bhp_disc AS bhp_disc,
      	view_rincian_radiologi_ranap.biayaperawatan AS biayaperawatan,
      	view_rincian_radiologi_ranap.biayaperawatan_disc AS biayaperawatan_disc,
      	view_rincian_radiologi_ranap.total AS subtotal,
      	view_rincian_radiologi_ranap.kuantitas AS kuantitas,
      	view_rincian_radiologi_ranap.diskon AS diskon,
      	view_rincian_radiologi_ranap.total * view_rincian_radiologi_ranap.kuantitas AS total,
      	view_rincian_radiologi_ranap.totalkeseluruhan AS totalkeseluruhan,
      	view_rincian_radiologi_ranap.status AS status,
      	view_rincian_radiologi_ranap.statusverifikasi AS statusverifikasi,
        view_rincian_radiologi_ranap.status_tindakan AS status_tindakan,
        view_rincian_radiologi_ranap.jasamedis AS jasamedis,
        view_rincian_radiologi_ranap.potongan_rs AS potongan_rs,
        view_rincian_radiologi_ranap.pajak_dokter AS pajak_dokter,
        view_rincian_radiologi_ranap.periode_pembayaran AS periode_pembayaran,
        view_rincian_radiologi_ranap.periode_jatuhtempo AS periode_jatuhtempo,
        view_rincian_radiologi_ranap.status_jasamedis AS status_jasamedis,
        view_rincian_radiologi_ranap.status_tindakan AS status_expertise
      FROM
      	(
      		SELECT
      			trujukan_radiologi_detail.id AS iddetail,
      			trawatinap_pendaftaran.id AS idrawatinap,
      			trawatinap_pendaftaran.idkelas AS idkelas,
      			trujukan_radiologi_detail.idrujukan AS idrujukan,
      			trujukan_radiologi.norujukan AS norujukan,
      			cast( trujukan_radiologi.tanggal AS date ) AS tanggal,
      			mtarif_radiologi.idtipe AS idtipe,
      			trujukan_radiologi_detail.idradiologi AS idradiologi,
      		  (
      		  	CASE
      		  		WHEN ( mtarif_radiologi.idtipe = 1 ) THEN
      		  		concat(
      		  			mtarif_radiologi.nama,
      		  			' (',
      		  			mtarif_radiologi_expose.nama,
      		  			' ',
      		  			mtarif_radiologi_film.nama,
      		  			')'
      		  		) ELSE mtarif_radiologi.nama
      		  	END
      			) AS namatarif,
            mdokter_kategori.id AS idkategori,
            mdokter_kategori.nama AS namakategori,
      			COALESCE(trujukan_radiologi_detail.iddokter, trujukan_radiologi.iddokterradiologi) AS iddokter,
      			mdokter.nama AS namadokter,
      			trujukan_radiologi_detail.jasasarana AS jasasarana,
      			trujukan_radiologi_detail.jasasarana_disc AS jasasarana_disc,
      			trujukan_radiologi_detail.jasapelayanan AS jasapelayanan,
      			trujukan_radiologi_detail.jasapelayanan_disc AS jasapelayanan_disc,
      			trujukan_radiologi_detail.bhp AS bhp,
      			trujukan_radiologi_detail.bhp_disc AS bhp_disc,
      			trujukan_radiologi_detail.biayaperawatan AS biayaperawatan,
      			trujukan_radiologi_detail.biayaperawatan_disc AS biayaperawatan_disc,
      			trujukan_radiologi_detail.total AS total,
      			trujukan_radiologi_detail.kuantitas AS kuantitas,
      			trujukan_radiologi_detail.diskon AS diskon,
      			trujukan_radiologi_detail.totalkeseluruhan AS totalkeseluruhan,
      			trujukan_radiologi_detail.status AS status,
      			trujukan_radiologi_detail.statusverifikasi AS statusverifikasi,
            COALESCE(trujukan_radiologi_detail.status_tindakan, trujukan_radiologi.statuspasien) AS status_tindakan,
            (
          	CASE
          		WHEN mtarif_radiologi.idtipe = '1' THEN
          		  trujukan_radiologi_detail.nominal_jasamedis * trujukan_radiologi_detail.kuantitas
              ELSE trujukan_radiologi_detail.jasapelayanan
          	END
            ) AS jasamedis,
            trujukan_radiologi_detail.potongan_rs AS potongan_rs,
          	trujukan_radiologi_detail.pajak_dokter AS pajak_dokter,
          	trujukan_radiologi_detail.periode_pembayaran AS periode_pembayaran,
          	trujukan_radiologi_detail.periode_jatuhtempo AS periode_jatuhtempo,
          	trujukan_radiologi_detail.status_jasamedis AS status_jasamedis,
            trujukan_radiologi_detail.status_tindakan AS status_expertise
      		FROM
      		  trawatinap_pendaftaran
      		JOIN trujukan_radiologi ON trawatinap_pendaftaran.id = trujukan_radiologi.idtindakan AND trujukan_radiologi.asalrujukan = 3
      		JOIN trujukan_radiologi_detail ON trujukan_radiologi_detail.idrujukan = trujukan_radiologi.id
      		LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_radiologi_detail.iddokter, trujukan_radiologi.iddokterradiologi)
      		LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
      		JOIN mtarif_radiologi ON trujukan_radiologi_detail.idradiologi = mtarif_radiologi.id
      		LEFT JOIN mtarif_radiologi_expose ON mtarif_radiologi.idexpose = mtarif_radiologi_expose.id
      		LEFT JOIN mtarif_radiologi_film ON mtarif_radiologi.idfilm = mtarif_radiologi_film.id
      		WHERE
      		  trujukan_radiologi.status <> 0 AND
            trawatinap_pendaftaran.id = $idpendaftaran AND
            mtarif_radiologi.idtipe = $idtipe  AND trujukan_radiologi_detail.status <> 0
      	) AS view_rincian_radiologi_ranap

        ORDER BY tanggal
      ");
		return $query->result();
	}

	public function viewRincianFisioterapi($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	view_rincian_fisioterapi_rajal.iddetail AS iddetail,
      	view_rincian_fisioterapi_rajal.idrawatinap AS idrawatinap,
      	view_rincian_fisioterapi_rajal.idkelas AS idkelas,
      	view_rincian_fisioterapi_rajal.idrujukan AS idrujukan,
      	view_rincian_fisioterapi_rajal.norujukan AS norujukan,
      	view_rincian_fisioterapi_rajal.tanggal AS tanggal,
      	view_rincian_fisioterapi_rajal.idfisioterapi AS idfisioterapi,
      	view_rincian_fisioterapi_rajal.idtarif AS idtarif,
      	view_rincian_fisioterapi_rajal.namatarif AS namatarif,
      	view_rincian_fisioterapi_rajal.idkategori AS idkategori,
      	view_rincian_fisioterapi_rajal.namakategori AS namakategori,
      	view_rincian_fisioterapi_rajal.iddokter AS iddokter,
      	view_rincian_fisioterapi_rajal.namadokter AS namadokter,
      	view_rincian_fisioterapi_rajal.jasasarana AS jasasarana,
      	view_rincian_fisioterapi_rajal.jasasarana_disc AS jasasarana_disc,
      	view_rincian_fisioterapi_rajal.jasapelayanan AS jasapelayanan,
      	view_rincian_fisioterapi_rajal.jasapelayanan_disc AS jasapelayanan_disc,
      	view_rincian_fisioterapi_rajal.jasamedis AS jasamedis,
      	view_rincian_fisioterapi_rajal.bhp AS bhp,
      	view_rincian_fisioterapi_rajal.bhp_disc AS bhp_disc,
      	view_rincian_fisioterapi_rajal.biayaperawatan AS biayaperawatan,
      	view_rincian_fisioterapi_rajal.biayaperawatan_disc AS biayaperawatan_disc,
      	view_rincian_fisioterapi_rajal.total AS subtotal,
      	view_rincian_fisioterapi_rajal.kuantitas AS kuantitas,
      	view_rincian_fisioterapi_rajal.diskon AS diskon,
      	view_rincian_fisioterapi_rajal.total * view_rincian_fisioterapi_rajal.kuantitas AS total,
      	view_rincian_fisioterapi_rajal.totalkeseluruhan AS totalkeseluruhan,
      	view_rincian_fisioterapi_rajal.STATUS AS STATUS,
      	view_rincian_fisioterapi_rajal.statusverifikasi AS statusverifikasi,
        view_rincian_fisioterapi_rajal.potongan_rs AS potongan_rs,
        view_rincian_fisioterapi_rajal.pajak_dokter AS pajak_dokter,
        view_rincian_fisioterapi_rajal.periode_pembayaran AS periode_pembayaran,
        view_rincian_fisioterapi_rajal.periode_jatuhtempo AS periode_jatuhtempo,
        view_rincian_fisioterapi_rajal.status_jasamedis AS status_jasamedis
      FROM
      	(
      		SELECT
      			trujukan_fisioterapi_detail.id AS iddetail,
      			trawatinap_pendaftaran.id AS idrawatinap,
      			trawatinap_pendaftaran.idkelas AS idkelas,
      			tpoliklinik_pendaftaran.id AS idpoliklinik,
      			trujukan_fisioterapi_detail.idrujukan AS idrujukan,
      			trujukan_fisioterapi.norujukan AS norujukan,
      			cast( trujukan_fisioterapi.tanggal AS date ) AS tanggal,
      			trujukan_fisioterapi_detail.idfisioterapi AS idfisioterapi,
      			mtarif_fisioterapi.id AS idtarif,
      			mtarif_fisioterapi.nama AS namatarif,
          	mdokter_kategori.id AS idkategori,
          	mdokter_kategori.nama AS namakategori,
      			COALESCE(trujukan_fisioterapi_detail.iddokter, trujukan_fisioterapi.iddokterperujuk) AS iddokter,
      			mdokter.nama AS namadokter,
      			trujukan_fisioterapi_detail.jasasarana AS jasasarana,
      			trujukan_fisioterapi_detail.jasasarana_disc AS jasasarana_disc,
      			trujukan_fisioterapi_detail.jasapelayanan AS jasapelayanan,
      			trujukan_fisioterapi_detail.jasapelayanan_disc AS jasapelayanan_disc,
      			(trujukan_fisioterapi_detail.jasapelayanan - trujukan_fisioterapi_detail.jasapelayanan_disc) * trujukan_fisioterapi_detail.kuantitas AS jasamedis,
      			trujukan_fisioterapi_detail.bhp AS bhp,
      			trujukan_fisioterapi_detail.bhp_disc AS bhp_disc,
      			trujukan_fisioterapi_detail.biayaperawatan AS biayaperawatan,
      			trujukan_fisioterapi_detail.biayaperawatan_disc AS biayaperawatan_disc,
      			trujukan_fisioterapi_detail.total AS total,
      			trujukan_fisioterapi_detail.kuantitas AS kuantitas,
      			trujukan_fisioterapi_detail.diskon AS diskon,
      			trujukan_fisioterapi_detail.totalkeseluruhan AS totalkeseluruhan,
      			trujukan_fisioterapi_detail.status AS STATUS,
      			trujukan_fisioterapi_detail.statusverifikasi AS statusverifikasi,
      			5 AS kelompok,
      			tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      			tverifikasi_transaksi_detail.id AS idverifdetail,
      			tkasir.status AS status_kasir,
            trujukan_fisioterapi_detail.potongan_rs AS potongan_rs,
          	trujukan_fisioterapi_detail.pajak_dokter AS pajak_dokter,
          	trujukan_fisioterapi_detail.periode_pembayaran AS periode_pembayaran,
          	trujukan_fisioterapi_detail.periode_jatuhtempo AS periode_jatuhtempo,
          	trujukan_fisioterapi_detail.status_jasamedis AS status_jasamedis
      		FROM
      			tpoliklinik_pendaftaran
      		LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
      		JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
      		JOIN trujukan_fisioterapi ON tpoliklinik_tindakan.id = trujukan_fisioterapi.idtindakan AND trujukan_fisioterapi.asalrujukan IN ( 1, 2 )
      		JOIN trujukan_fisioterapi_detail ON trujukan_fisioterapi_detail.idrujukan = trujukan_fisioterapi.id
      		LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_fisioterapi_detail.iddokter, trujukan_fisioterapi.iddokterperujuk)
          LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
      		JOIN mtarif_fisioterapi ON trujukan_fisioterapi_detail.idfisioterapi = mtarif_fisioterapi.id
      		LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = trujukan_fisioterapi_detail.id AND tverifikasi_transaksi_detail.id_kelompok = 9
      		JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
          WHERE
            trawatinap_pendaftaran.id = $idpendaftaran AND trujukan_fisioterapi_detail.status <> 0
      	) AS view_rincian_fisioterapi_rajal
      WHERE
      	view_rincian_fisioterapi_rajal.status_kasir <> 2 AND
        view_rincian_fisioterapi_rajal.idrawatinap = $idpendaftaran

      UNION ALL

      SELECT
      	view_rincian_fisioterapi_ranap.iddetail AS iddetail,
      	view_rincian_fisioterapi_ranap.idrawatinap AS idrawatinap,
      	view_rincian_fisioterapi_ranap.idkelas AS idkelas,
      	view_rincian_fisioterapi_ranap.idrujukan AS idrujukan,
      	view_rincian_fisioterapi_ranap.norujukan AS norujukan,
      	view_rincian_fisioterapi_ranap.tanggal AS tanggal,
      	view_rincian_fisioterapi_ranap.idfisioterapi AS idfisioterapi,
      	view_rincian_fisioterapi_ranap.idtarif AS idtarif,
      	view_rincian_fisioterapi_ranap.namatarif AS namatarif,
      	view_rincian_fisioterapi_ranap.idkategori AS idkategori,
      	view_rincian_fisioterapi_ranap.namakategori AS namakategori,
      	view_rincian_fisioterapi_ranap.iddokter AS iddokter,
      	view_rincian_fisioterapi_ranap.namadokter AS namadokter,
      	view_rincian_fisioterapi_ranap.jasasarana AS jasasarana,
      	view_rincian_fisioterapi_ranap.jasasarana_disc AS jasasarana_disc,
      	view_rincian_fisioterapi_ranap.jasapelayanan AS jasapelayanan,
      	view_rincian_fisioterapi_ranap.jasapelayanan_disc AS jasapelayanan_disc,
      	view_rincian_fisioterapi_ranap.jasamedis AS jasamedis,
      	view_rincian_fisioterapi_ranap.bhp AS bhp,
      	view_rincian_fisioterapi_ranap.bhp_disc AS bhp_disc,
      	view_rincian_fisioterapi_ranap.biayaperawatan AS biayaperawatan,
      	view_rincian_fisioterapi_ranap.biayaperawatan_disc AS biayaperawatan_disc,
      	view_rincian_fisioterapi_ranap.total AS subtotal,
      	view_rincian_fisioterapi_ranap.kuantitas AS kuantitas,
      	view_rincian_fisioterapi_ranap.diskon AS diskon,
      	view_rincian_fisioterapi_ranap.total * view_rincian_fisioterapi_ranap.kuantitas AS total,
      	view_rincian_fisioterapi_ranap.totalkeseluruhan AS totalkeseluruhan,
      	view_rincian_fisioterapi_ranap.status AS status,
      	view_rincian_fisioterapi_ranap.statusverifikasi AS statusverifikasi,
        view_rincian_fisioterapi_ranap.potongan_rs AS potongan_rs,
        view_rincian_fisioterapi_ranap.pajak_dokter AS pajak_dokter,
        view_rincian_fisioterapi_ranap.periode_pembayaran AS periode_pembayaran,
        view_rincian_fisioterapi_ranap.periode_jatuhtempo AS periode_jatuhtempo,
        view_rincian_fisioterapi_ranap.status_jasamedis AS status_jasamedis
      FROM
      	(
      		SELECT
      			trujukan_fisioterapi_detail.id AS iddetail,
      			trawatinap_pendaftaran.id AS idrawatinap,
      			trawatinap_pendaftaran.idkelas AS idkelas,
      			trujukan_fisioterapi_detail.idrujukan AS idrujukan,
      			trujukan_fisioterapi.norujukan AS norujukan,
      			cast( trujukan_fisioterapi.tanggal AS date ) AS tanggal,
      			trujukan_fisioterapi_detail.idfisioterapi AS idfisioterapi,
      			mtarif_fisioterapi.id AS idtarif,
      			mtarif_fisioterapi.nama AS namatarif,
          	mdokter_kategori.id AS idkategori,
          	mdokter_kategori.nama AS namakategori,
      			COALESCE(trujukan_fisioterapi_detail.iddokter, trujukan_fisioterapi.iddokterperujuk) AS iddokter,
      			mdokter.nama AS namadokter,
      			trujukan_fisioterapi_detail.jasasarana AS jasasarana,
      			trujukan_fisioterapi_detail.jasasarana_disc AS jasasarana_disc,
      			trujukan_fisioterapi_detail.jasapelayanan AS jasapelayanan,
      			trujukan_fisioterapi_detail.jasapelayanan_disc AS jasapelayanan_disc,
            (trujukan_fisioterapi_detail.jasapelayanan - trujukan_fisioterapi_detail.jasapelayanan_disc) * trujukan_fisioterapi_detail.kuantitas AS jasamedis,
      			trujukan_fisioterapi_detail.bhp AS bhp,
      			trujukan_fisioterapi_detail.bhp_disc AS bhp_disc,
      			trujukan_fisioterapi_detail.biayaperawatan AS biayaperawatan,
      			trujukan_fisioterapi_detail.biayaperawatan_disc AS biayaperawatan_disc,
      			trujukan_fisioterapi_detail.total AS total,
      			trujukan_fisioterapi_detail.kuantitas AS kuantitas,
      			trujukan_fisioterapi_detail.diskon AS diskon,
      			trujukan_fisioterapi_detail.totalkeseluruhan AS totalkeseluruhan,
      			trujukan_fisioterapi_detail.status AS status,
      			trujukan_fisioterapi_detail.statusverifikasi AS statusverifikasi,
            trujukan_fisioterapi_detail.potongan_rs AS potongan_rs,
          	trujukan_fisioterapi_detail.pajak_dokter AS pajak_dokter,
          	trujukan_fisioterapi_detail.periode_pembayaran AS periode_pembayaran,
          	trujukan_fisioterapi_detail.periode_jatuhtempo AS periode_jatuhtempo,
          	trujukan_fisioterapi_detail.status_jasamedis AS status_jasamedis
      		FROM
      			trawatinap_pendaftaran
      		JOIN trujukan_fisioterapi ON trawatinap_pendaftaran.id = trujukan_fisioterapi.idtindakan AND trujukan_fisioterapi.asalrujukan = 3
      		JOIN trujukan_fisioterapi_detail ON trujukan_fisioterapi_detail.idrujukan = trujukan_fisioterapi.id
      		LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_fisioterapi_detail.iddokter, trujukan_fisioterapi.iddokterperujuk)
      		LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
      		JOIN mtarif_fisioterapi ON trujukan_fisioterapi_detail.idfisioterapi = mtarif_fisioterapi.id
          WHERE
            trawatinap_pendaftaran.id = $idpendaftaran  AND trujukan_fisioterapi_detail.status <> 0
      	) AS view_rincian_fisioterapi_ranap
      WHERE
      	idrawatinap = $idpendaftaran
      ORDER BY
      	tanggal
      	AND iddetail
      ");
		return $query->result();
	}

	public function viewRincianOperasiSewaAlat($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
        	tkamaroperasi_sewaalat.id AS iddetail,
        	trawatinap_pendaftaran.id AS idrawatinap,
        	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
        	trawatinap_pendaftaran.idkelas AS idkelas,
        	tkamaroperasi_sewaalat.tanggal_transaksi AS tanggal,
        	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
        	mtarif_operasi_sewaalat.id AS idtarif,
        	mtarif_operasi_sewaalat.nama AS namatarif,
        	tkamaroperasi_sewaalat.jasasarana AS jasasarana,
        	tkamaroperasi_sewaalat.jasasarana_disc AS jasasarana_disc,
        	tkamaroperasi_sewaalat.jasapelayanan AS jasapelayanan,
        	tkamaroperasi_sewaalat.jasapelayanan_disc AS jasapelayanan_disc,
        	tkamaroperasi_sewaalat.bhp AS bhp,
        	tkamaroperasi_sewaalat.bhp_disc AS bhp_disc,
        	tkamaroperasi_sewaalat.biayaperawatan AS biayaperawatan,
        	tkamaroperasi_sewaalat.biayaperawatan_disc AS biayaperawatan_disc,
        	tkamaroperasi_sewaalat.total AS subtotal,
        	tkamaroperasi_sewaalat.kuantitas AS kuantitas,
        	tkamaroperasi_sewaalat.total * tkamaroperasi_sewaalat.kuantitas AS total,
        	tkamaroperasi_sewaalat.diskon AS diskon,
			tkamaroperasi_sewaalat.totalkeseluruhan AS totalkeseluruhan,
        	tkamaroperasi_sewaalat.statusverifikasi AS statusverifikasi,
        	20 AS kelompok,
        	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
        	tverifikasi_transaksi_detail.id AS idverifdetail
        FROM
        	tkamaroperasi_sewaalat
        LEFT JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_sewaalat.idpendaftaranoperasi
        LEFT JOIN tpoliklinik_pendaftaran ON (tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran) AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
        LEFT JOIN trawatinap_pendaftaran ON (trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 2) 
				
        LEFT JOIN mtarif_operasi_sewaalat ON mtarif_operasi_sewaalat.id = tkamaroperasi_sewaalat.idalat
        LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tkamaroperasi_sewaalat.id AND tverifikasi_transaksi_detail.id_kelompok = 20
        WHERE trawatinap_pendaftaran.id = $idpendaftaran
				
				
				UNION 
				
				SELECT
        	tkamaroperasi_sewaalat.id AS iddetail,
        	trawatinap_pendaftaran.id AS idrawatinap,
        	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
        	trawatinap_pendaftaran.idkelas AS idkelas,
        	tkamaroperasi_sewaalat.tanggal_transaksi AS tanggal,
        	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
        	mtarif_operasi_sewaalat.id AS idtarif,
        	mtarif_operasi_sewaalat.nama AS namatarif,
        	tkamaroperasi_sewaalat.jasasarana AS jasasarana,
        	tkamaroperasi_sewaalat.jasasarana_disc AS jasasarana_disc,
        	tkamaroperasi_sewaalat.jasapelayanan AS jasapelayanan,
        	tkamaroperasi_sewaalat.jasapelayanan_disc AS jasapelayanan_disc,
        	tkamaroperasi_sewaalat.bhp AS bhp,
        	tkamaroperasi_sewaalat.bhp_disc AS bhp_disc,
        	tkamaroperasi_sewaalat.biayaperawatan AS biayaperawatan,
        	tkamaroperasi_sewaalat.biayaperawatan_disc AS biayaperawatan_disc,
        	tkamaroperasi_sewaalat.total AS subtotal,
        	tkamaroperasi_sewaalat.kuantitas AS kuantitas,
        	tkamaroperasi_sewaalat.total * tkamaroperasi_sewaalat.kuantitas AS total,
        	tkamaroperasi_sewaalat.diskon AS diskon,
			tkamaroperasi_sewaalat.totalkeseluruhan AS totalkeseluruhan,
        	tkamaroperasi_sewaalat.statusverifikasi AS statusverifikasi,
        	20 AS kelompok,
        	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
        	tverifikasi_transaksi_detail.id AS idverifdetail
        FROM
        	tkamaroperasi_sewaalat
        LEFT JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_sewaalat.idpendaftaranoperasi
        LEFT JOIN tpoliklinik_pendaftaran ON (tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran) AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
        LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
				
        LEFT JOIN mtarif_operasi_sewaalat ON mtarif_operasi_sewaalat.id = tkamaroperasi_sewaalat.idalat
        LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tkamaroperasi_sewaalat.id AND tverifikasi_transaksi_detail.id_kelompok = 20
        WHERE trawatinap_pendaftaran.id = $idpendaftaran
      ");
		return $query->result();
	}

	public function viewRincianOperasiImplan($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	tkamaroperasi_implan.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	tkamaroperasi_implan.tanggal_transaksi AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	munitpelayanan.id AS idunitpelayanan,
      	munitpelayanan.nama AS unitpelayanan,
      	mdata_implan.id AS idtarif,
      	mdata_implan.nama AS namatarif,
      	mdata_implan.kode AS kode,
      	tkamaroperasi_implan.hargadasar AS hargadasar,
      	tkamaroperasi_implan.margin AS margin,
      	tkamaroperasi_implan.hargajual AS hargajual,
      	tkamaroperasi_implan.kuantitas AS kuantitas,
      	tkamaroperasi_implan.totalkeseluruhan AS totalkeseluruhan,
      	tkamaroperasi_implan.diskon AS diskon,
      	0 AS ppn,
      	tkamaroperasi_implan.statusverifikasi AS statusverifikasi,
      	26 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      	tkamaroperasi_implan
      LEFT JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_implan.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 2 
      LEFT JOIN mdata_implan ON mdata_implan.id = tkamaroperasi_implan.idobat
      LEFT JOIN munitpelayanan ON munitpelayanan.id = tkamaroperasi_implan.idunit
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tkamaroperasi_implan.id AND tverifikasi_transaksi_detail.id_kelompok = 26
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran
		UNION 
		
		SELECT
      	tkamaroperasi_implan.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	tkamaroperasi_implan.tanggal_transaksi AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	munitpelayanan.id AS idunitpelayanan,
      	munitpelayanan.nama AS unitpelayanan,
      	mdata_implan.id AS idtarif,
      	mdata_implan.nama AS namatarif,
      	mdata_implan.kode AS kode,
      	tkamaroperasi_implan.hargadasar AS hargadasar,
      	tkamaroperasi_implan.margin AS margin,
      	tkamaroperasi_implan.hargajual AS hargajual,
      	tkamaroperasi_implan.kuantitas AS kuantitas,
      	tkamaroperasi_implan.totalkeseluruhan AS totalkeseluruhan,
      	tkamaroperasi_implan.diskon AS diskon,
      	0 AS ppn,
      	tkamaroperasi_implan.statusverifikasi AS statusverifikasi,
      	26 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      	tkamaroperasi_implan
      LEFT JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_implan.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
      LEFT JOIN mdata_implan ON mdata_implan.id = tkamaroperasi_implan.idobat
      LEFT JOIN munitpelayanan ON munitpelayanan.id = tkamaroperasi_implan.idunit
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tkamaroperasi_implan.id AND tverifikasi_transaksi_detail.id_kelompok = 26
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran
      ");
		return $query->result();
	}

	public function viewRincianOperasiKamar($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("
		
		SELECT
      	tkamaroperasi_ruangan.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.idkelas AS idkelas,
      	tkamaroperasi_pendaftaran.tanggal_trx AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	mtarif_operasi.id AS idtarif,
      	mtarif_operasi.nama AS namatarif,
      	tkamaroperasi_ruangan.jasasarana AS jasasarana,
      	tkamaroperasi_ruangan.jasasarana_disc AS jasasarana_disc,
      	tkamaroperasi_ruangan.jasapelayanan AS jasapelayanan,
      	tkamaroperasi_ruangan.jasapelayanan_disc AS jasapelayanan_disc,
      	tkamaroperasi_ruangan.bhp AS bhp,
      	tkamaroperasi_ruangan.bhp_disc AS bhp_disc,
      	tkamaroperasi_ruangan.biayaperawatan AS biayaperawatan,
      	tkamaroperasi_ruangan.biayaperawatan_disc AS biayaperawatan_disc,
      	tkamaroperasi_ruangan.total AS subtotal,
      	1 AS kuantitas,
      	tkamaroperasi_ruangan.total * 1 AS total,
      	tkamaroperasi_ruangan.diskon AS diskon,
      	tkamaroperasi_ruangan.totalkeseluruhan AS totalkeseluruhan,
      	tkamaroperasi_ruangan.statusverif AS statusverifikasi
      FROM
      	tkamaroperasi_ruangan
      JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_ruangan.idpendaftaranoperasi
      JOIN tkamaroperasi_tindakan ON tkamaroperasi_tindakan.idpendaftaranoperasi = tkamaroperasi_tindakan.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON (trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 2) 
      JOIN mtarif_operasi ON mtarif_operasi.id = tkamaroperasi_ruangan.id_tarif
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran AND tkamaroperasi_ruangan.status<>0
      GROUP BY
      	tkamaroperasi_ruangan.id
	UNION 	
	SELECT
      	tkamaroperasi_ruangan.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.idkelas AS idkelas,
      	tkamaroperasi_pendaftaran.tanggal_trx AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	mtarif_operasi.id AS idtarif,
      	mtarif_operasi.nama AS namatarif,
      	tkamaroperasi_ruangan.jasasarana AS jasasarana,
      	tkamaroperasi_ruangan.jasasarana_disc AS jasasarana_disc,
      	tkamaroperasi_ruangan.jasapelayanan AS jasapelayanan,
      	tkamaroperasi_ruangan.jasapelayanan_disc AS jasapelayanan_disc,
      	tkamaroperasi_ruangan.bhp AS bhp,
      	tkamaroperasi_ruangan.bhp_disc AS bhp_disc,
      	tkamaroperasi_ruangan.biayaperawatan AS biayaperawatan,
      	tkamaroperasi_ruangan.biayaperawatan_disc AS biayaperawatan_disc,
      	tkamaroperasi_ruangan.total AS subtotal,
      	1 AS kuantitas,
      	tkamaroperasi_ruangan.total * 1 AS total,
      	tkamaroperasi_ruangan.diskon AS diskon,
      	tkamaroperasi_ruangan.totalkeseluruhan AS totalkeseluruhan,
      	tkamaroperasi_ruangan.statusverif AS statusverifikasi
      FROM
      	tkamaroperasi_ruangan
      JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_ruangan.idpendaftaranoperasi
      JOIN tkamaroperasi_tindakan ON tkamaroperasi_tindakan.idpendaftaranoperasi = tkamaroperasi_tindakan.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON ( trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id )
      JOIN mtarif_operasi ON mtarif_operasi.id = tkamaroperasi_ruangan.id_tarif
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran  AND tkamaroperasi_ruangan.status<>0
      GROUP BY
      	tkamaroperasi_ruangan.id
		
		UNION 
		
		
		SELECT
      	tkamaroperasi_ruangan.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.idkelas AS idkelas,
      	tkamaroperasi_pendaftaran.tanggal_trx AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	mtarif_operasi.id AS idtarif,
      	mtarif_operasi.nama AS namatarif,
      	tkamaroperasi_ruangan.jasasarana AS jasasarana,
      	tkamaroperasi_ruangan.jasasarana_disc AS jasasarana_disc,
      	tkamaroperasi_ruangan.jasapelayanan AS jasapelayanan,
      	tkamaroperasi_ruangan.jasapelayanan_disc AS jasapelayanan_disc,
      	tkamaroperasi_ruangan.bhp AS bhp,
      	tkamaroperasi_ruangan.bhp_disc AS bhp_disc,
      	tkamaroperasi_ruangan.biayaperawatan AS biayaperawatan,
      	tkamaroperasi_ruangan.biayaperawatan_disc AS biayaperawatan_disc,
      	tkamaroperasi_ruangan.total AS subtotal,
      	1 AS kuantitas,
      	tkamaroperasi_ruangan.total * 1 AS total,
      	tkamaroperasi_ruangan.diskon AS diskon,
      	tkamaroperasi_ruangan.totalkeseluruhan AS totalkeseluruhan,
      	tkamaroperasi_ruangan.statusverif AS statusverifikasi
      FROM
      	tkamaroperasi_ruangan
      JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_ruangan.idpendaftaranoperasi
      JOIN tkamaroperasi_tindakan ON tkamaroperasi_tindakan.idpendaftaranoperasi = tkamaroperasi_tindakan.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON (trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 2)
      JOIN mtarif_operasi ON mtarif_operasi.id = tkamaroperasi_ruangan.id_tarif
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran  AND tkamaroperasi_ruangan.status<>0
      GROUP BY
      	tkamaroperasi_ruangan.id
		
		UNION 
		
		
		SELECT
      	tkamaroperasi_ruangan.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.idkelas AS idkelas,
      	tkamaroperasi_pendaftaran.tanggal_trx AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	mtarif_operasi.id AS idtarif,
      	mtarif_operasi.nama AS namatarif,
      	tkamaroperasi_ruangan.jasasarana AS jasasarana,
      	tkamaroperasi_ruangan.jasasarana_disc AS jasasarana_disc,
      	tkamaroperasi_ruangan.jasapelayanan AS jasapelayanan,
      	tkamaroperasi_ruangan.jasapelayanan_disc AS jasapelayanan_disc,
      	tkamaroperasi_ruangan.bhp AS bhp,
      	tkamaroperasi_ruangan.bhp_disc AS bhp_disc,
      	tkamaroperasi_ruangan.biayaperawatan AS biayaperawatan,
      	tkamaroperasi_ruangan.biayaperawatan_disc AS biayaperawatan_disc,
      	tkamaroperasi_ruangan.total AS subtotal,
      	1 AS kuantitas,
      	tkamaroperasi_ruangan.total * 1 AS total,
      	tkamaroperasi_ruangan.diskon AS diskon,
      	tkamaroperasi_ruangan.totalkeseluruhan AS totalkeseluruhan,
      	tkamaroperasi_ruangan.statusverif AS statusverifikasi
      FROM
      	tkamaroperasi_ruangan
      JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_ruangan.idpendaftaranoperasi
      JOIN tkamaroperasi_tindakan ON tkamaroperasi_tindakan.idpendaftaranoperasi = tkamaroperasi_tindakan.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON ( trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id )
      JOIN mtarif_operasi ON mtarif_operasi.id = tkamaroperasi_ruangan.id_tarif
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran  AND tkamaroperasi_ruangan.status<>0
      GROUP BY
      	tkamaroperasi_ruangan.id
      ");
		return $query->result();
	}

	public function viewRincianOperasiAlkes($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	tkamaroperasi_alkes.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      	tkamaroperasi_alkes.tanggal_transaksi AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	tkamaroperasi_alkes.idunit AS idunitpelayanan,
      	munitpelayanan.nama AS unitpelayanan,
      	mdata_alkes.id AS idtarif,
      	mdata_alkes.nama AS namatarif,
      	tkamaroperasi_alkes.hargadasar AS hargadasar,
      	tkamaroperasi_alkes.margin AS margin,
      	tkamaroperasi_alkes.hargajual AS hargajual,
      	tkamaroperasi_alkes.kuantitas AS kuantitas,
      	tkamaroperasi_alkes.totalkeseluruhan AS totalkeseluruhan,
      	tkamaroperasi_alkes.diskon AS diskon,
      	tkamaroperasi_alkes.statusverifikasi AS statusverifikasi,
      	21 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      tkamaroperasi_alkes
      LEFT JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_alkes.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON (trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 2) 
      LEFT JOIN mdata_alkes ON mdata_alkes.id = tkamaroperasi_alkes.idobat
      LEFT JOIN munitpelayanan ON munitpelayanan.id = tkamaroperasi_alkes.idunit
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tkamaroperasi_alkes.id AND tverifikasi_transaksi_detail.id_kelompok = 21
      WHERE trawatinap_pendaftaran.id = $idpendaftaran
	  UNION 
	  
	  SELECT
      	tkamaroperasi_alkes.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      	tkamaroperasi_alkes.tanggal_transaksi AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	tkamaroperasi_alkes.idunit AS idunitpelayanan,
      	munitpelayanan.nama AS unitpelayanan,
      	mdata_alkes.id AS idtarif,
      	mdata_alkes.nama AS namatarif,
      	tkamaroperasi_alkes.hargadasar AS hargadasar,
      	tkamaroperasi_alkes.margin AS margin,
      	tkamaroperasi_alkes.hargajual AS hargajual,
      	tkamaroperasi_alkes.kuantitas AS kuantitas,
      	tkamaroperasi_alkes.totalkeseluruhan AS totalkeseluruhan,
      	tkamaroperasi_alkes.diskon AS diskon,
      	tkamaroperasi_alkes.statusverifikasi AS statusverifikasi,
      	21 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      tkamaroperasi_alkes
      LEFT JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_alkes.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
      LEFT JOIN mdata_alkes ON mdata_alkes.id = tkamaroperasi_alkes.idobat
      LEFT JOIN munitpelayanan ON munitpelayanan.id = tkamaroperasi_alkes.idunit
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tkamaroperasi_alkes.id AND tverifikasi_transaksi_detail.id_kelompok = 21
      WHERE trawatinap_pendaftaran.id = $idpendaftaran
	  ");
		return $query->result();
	}

	public function viewRincianOperasiObat($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	tkamaroperasi_obat.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      	tkamaroperasi_obat.tanggal_transaksi AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	munitpelayanan.id AS idunitpelayanan,
      	munitpelayanan.nama AS unitpelayanan,
      	mdata_obat.id AS idtarif,
      	mdata_obat.nama AS namatarif,
      	tkamaroperasi_obat.hargadasar AS hargadasar,
      	tkamaroperasi_obat.margin AS margin,
      	tkamaroperasi_obat.hargajual AS hargajual,
      	tkamaroperasi_obat.kuantitas AS kuantitas,
      	tkamaroperasi_obat.totalkeseluruhan AS totalkeseluruhan,
      	tkamaroperasi_obat.diskon AS diskon,
      	tkamaroperasi_obat.statusverifikasi AS statusverifikasi,
      	22 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      	tkamaroperasi_obat
      LEFT JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_obat.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON (trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 2) 
      LEFT JOIN mdata_obat ON mdata_obat.id = tkamaroperasi_obat.idobat
      LEFT JOIN munitpelayanan ON munitpelayanan.id = tkamaroperasi_obat.idunit
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tkamaroperasi_obat.id AND tverifikasi_transaksi_detail.id_kelompok = 22
      WHERE trawatinap_pendaftaran.id = $idpendaftaran
	  UNION 
	  SELECT
      	tkamaroperasi_obat.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      	tkamaroperasi_obat.tanggal_transaksi AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	munitpelayanan.id AS idunitpelayanan,
      	munitpelayanan.nama AS unitpelayanan,
      	mdata_obat.id AS idtarif,
      	mdata_obat.nama AS namatarif,
      	tkamaroperasi_obat.hargadasar AS hargadasar,
      	tkamaroperasi_obat.margin AS margin,
      	tkamaroperasi_obat.hargajual AS hargajual,
      	tkamaroperasi_obat.kuantitas AS kuantitas,
      	tkamaroperasi_obat.totalkeseluruhan AS totalkeseluruhan,
      	tkamaroperasi_obat.diskon AS diskon,
      	tkamaroperasi_obat.statusverifikasi AS statusverifikasi,
      	22 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      	tkamaroperasi_obat
      LEFT JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_obat.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
      LEFT JOIN mdata_obat ON mdata_obat.id = tkamaroperasi_obat.idobat
      LEFT JOIN munitpelayanan ON munitpelayanan.id = tkamaroperasi_obat.idunit
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tkamaroperasi_obat.id AND tverifikasi_transaksi_detail.id_kelompok = 22
      WHERE trawatinap_pendaftaran.id = $idpendaftaran");
		return $query->result();
	}

	public function viewRincianOperasiNarcose($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	tkamaroperasi_narcose.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      	tkamaroperasi_narcose.tanggal_transaksi AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	munitpelayanan.id AS idunitpelayanan,
      	munitpelayanan.nama AS unitpelayanan,
      	mdata_obat.id AS idtarif,
      	mdata_obat.nama AS namatarif,
      	tkamaroperasi_narcose.hargadasar AS hargadasar,
      	tkamaroperasi_narcose.margin AS margin,
      	tkamaroperasi_narcose.hargajual AS hargajual,
      	tkamaroperasi_narcose.kuantitas AS kuantitas,
      	tkamaroperasi_narcose.totalkeseluruhan AS totalkeseluruhan,
      	tkamaroperasi_narcose.diskon AS diskon,
      	tkamaroperasi_narcose.statusverifikasi AS statusverifikasi,
      	23 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
        tkamaroperasi_narcose
      LEFT JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_narcose.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON (trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 2)
      LEFT JOIN mdata_obat ON mdata_obat.id = tkamaroperasi_narcose.idobat
      LEFT JOIN munitpelayanan ON munitpelayanan.id = tkamaroperasi_narcose.idunit
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tkamaroperasi_narcose.id AND tverifikasi_transaksi_detail.id_kelompok = 23
      WHERE trawatinap_pendaftaran.id = $idpendaftaran
	  
	  UNION 
	  
	  SELECT
      	tkamaroperasi_narcose.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      	tkamaroperasi_narcose.tanggal_transaksi AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	munitpelayanan.id AS idunitpelayanan,
      	munitpelayanan.nama AS unitpelayanan,
      	mdata_obat.id AS idtarif,
      	mdata_obat.nama AS namatarif,
      	tkamaroperasi_narcose.hargadasar AS hargadasar,
      	tkamaroperasi_narcose.margin AS margin,
      	tkamaroperasi_narcose.hargajual AS hargajual,
      	tkamaroperasi_narcose.kuantitas AS kuantitas,
      	tkamaroperasi_narcose.totalkeseluruhan AS totalkeseluruhan,
      	tkamaroperasi_narcose.diskon AS diskon,
      	tkamaroperasi_narcose.statusverifikasi AS statusverifikasi,
      	23 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
        tkamaroperasi_narcose
      LEFT JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_narcose.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
      LEFT JOIN mdata_obat ON mdata_obat.id = tkamaroperasi_narcose.idobat
      LEFT JOIN munitpelayanan ON munitpelayanan.id = tkamaroperasi_narcose.idunit
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tkamaroperasi_narcose.id AND tverifikasi_transaksi_detail.id_kelompok = 23
      WHERE trawatinap_pendaftaran.id = $idpendaftaran");
		return $query->result();
	}

	public function viewRincianOperasiJasaDokterOpertor($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	tkamaroperasi_jasado.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	tkamaroperasi_pendaftaran.tanggal_trx AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
        tkamaroperasi_jasado.id_tarif AS idtarif,
      	'Jasa Dokter Operator' AS namatarif,
      	'operator' AS idtipedokter,
      	tkamaroperasi_jasado.iddokteroperator AS iddokter,
      	mdokter.nama AS namadokter,
        tkamaroperasi_jasado.kelas_tarif AS idkelas,
      	tkamaroperasi_jasado.jasasarana AS jasasarana,
      	tkamaroperasi_jasado.jasasarana AS jasasarana_disc,
      	tkamaroperasi_jasado.jasapelayanan AS jasapelayanan,
      	tkamaroperasi_jasado.jasapelayanan_disc AS jasapelayanan_disc,
      	tkamaroperasi_jasado.bhp AS bhp,
      	tkamaroperasi_jasado.bhp_disc AS bhp_disc,
      	tkamaroperasi_jasado.biayaperawatan AS biayaperawatan,
      	tkamaroperasi_jasado.biayaperawatan_disc AS biayaperawatan_disc,
      	tkamaroperasi_jasado.total AS subtotal,
      	1 AS kuantitas,
      	tkamaroperasi_jasado.total * 1 AS total,
      	tkamaroperasi_jasado.diskon AS diskon,
		tkamaroperasi_jasado.totalkeseluruhan AS totalkeseluruhan,
      	tkamaroperasi_jasado.statusverif AS statusverifikasi,
        tkamaroperasi_jasado.potongan AS potongan_rs,
        tkamaroperasi_jasado.pajak AS pajak_dokter,
        tkamaroperasi_jasado.periode_pembayaran AS periode_pembayaran,
        tkamaroperasi_jasado.status_jasamedis AS status_jasamedis
      FROM
      	tkamaroperasi_jasado
      JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_jasado.idpendaftaranoperasi
      JOIN tkamaroperasi_tindakan ON tkamaroperasi_tindakan.idpendaftaranoperasi = tkamaroperasi_tindakan.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON (trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 2) 
      JOIN mjenis_operasi ON mjenis_operasi.id = tkamaroperasi_pendaftaran.jenis_operasi_id
      JOIN mdokter ON mdokter.id = tkamaroperasi_jasado.iddokteroperator
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran
      GROUP BY
      	tkamaroperasi_jasado.id,
      	tkamaroperasi_jasado.iddokteroperator
		
		UNION 
		
		SELECT
      	tkamaroperasi_jasado.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	tkamaroperasi_pendaftaran.tanggal_trx AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
        tkamaroperasi_jasado.id_tarif AS idtarif,
      	'Jasa Dokter Operator' AS namatarif,
      	'operator' AS idtipedokter,
      	tkamaroperasi_jasado.iddokteroperator AS iddokter,
      	mdokter.nama AS namadokter,
        tkamaroperasi_jasado.kelas_tarif AS idkelas,
      	tkamaroperasi_jasado.jasasarana AS jasasarana,
      	tkamaroperasi_jasado.jasasarana AS jasasarana_disc,
      	tkamaroperasi_jasado.jasapelayanan AS jasapelayanan,
      	tkamaroperasi_jasado.jasapelayanan_disc AS jasapelayanan_disc,
      	tkamaroperasi_jasado.bhp AS bhp,
      	tkamaroperasi_jasado.bhp_disc AS bhp_disc,
      	tkamaroperasi_jasado.biayaperawatan AS biayaperawatan,
      	tkamaroperasi_jasado.biayaperawatan_disc AS biayaperawatan_disc,
      	tkamaroperasi_jasado.total AS subtotal,
      	1 AS kuantitas,
      	tkamaroperasi_jasado.total * 1 AS total,
      	tkamaroperasi_jasado.diskon AS diskon,
		tkamaroperasi_jasado.totalkeseluruhan AS totalkeseluruhan,
      	tkamaroperasi_jasado.statusverif AS statusverifikasi,
        tkamaroperasi_jasado.potongan AS potongan_rs,
        tkamaroperasi_jasado.pajak AS pajak_dokter,
        tkamaroperasi_jasado.periode_pembayaran AS periode_pembayaran,
        tkamaroperasi_jasado.status_jasamedis AS status_jasamedis
      FROM
      	tkamaroperasi_jasado
      JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_jasado.idpendaftaranoperasi
      JOIN tkamaroperasi_tindakan ON tkamaroperasi_tindakan.idpendaftaranoperasi = tkamaroperasi_tindakan.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON ( trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id )
      JOIN mjenis_operasi ON mjenis_operasi.id = tkamaroperasi_pendaftaran.jenis_operasi_id
      JOIN mdokter ON mdokter.id = tkamaroperasi_jasado.iddokteroperator
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran
      GROUP BY
      	tkamaroperasi_jasado.id,
      	tkamaroperasi_jasado.iddokteroperator
      ");
		return $query->result();
	}

	public function viewRincianOperasiJasaDokterAnesthesi($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	tkamaroperasi_jasada.id AS iddetail,
      	trawatinap_pendaftaran.idtipe,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	tkamaroperasi_pendaftaran.tanggal_trx AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	0 AS idtarif,
      	'Jasa Dokter Anesthesi' AS namatarif,
      	'anaesthesi' AS idtipedokter,
        mdokter_kategori.id AS idkategori,
        mdokter_kategori.nama AS namakategori,
      	tkamaroperasi_jasada.iddokteranastesi AS iddokter,
      	mdokter.nama AS namadokter,
      	tkamaroperasi_jasada.total_acuan AS subtotal,
      	tkamaroperasi_jasada.persen AS persen,
      	tkamaroperasi_jasada.totalkeseluruhan AS total,
      	tkamaroperasi_jasada.diskon AS diskon,
		tkamaroperasi_jasada.totalkeseluruhan - tkamaroperasi_jasada.diskon AS grandtotal,
      	1 AS kuantitas,
      	tkamaroperasi_jasada.statusverif AS statusverifikasi,
      	tkamaroperasi_pendaftaran.total_do AS total_operator,
      	tkamaroperasi_pendaftaran.persen_da AS persentase_anesthesi,
      	tkamaroperasi_pendaftaran.total_acuan_da AS total_anesthesi,
      	tkamaroperasi_pendaftaran.jenis_operasi_id,
      	tkamaroperasi_jasada.persen AS persentase,
        tkamaroperasi_jasada.potongan AS potongan_rs,
        tkamaroperasi_jasada.pajak AS pajak_dokter,
        tkamaroperasi_jasada.periode_pembayaran AS periode_pembayaran,
        tkamaroperasi_jasada.periode_jatuhtempo AS periode_jatuhtempo,
        tkamaroperasi_jasada.status_jasamedis AS status_jasamedis
      FROM
      	tkamaroperasi_jasada
      JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_jasada.idpendaftaranoperasi
      JOIN tkamaroperasi_tindakan ON tkamaroperasi_tindakan.idpendaftaranoperasi = tkamaroperasi_tindakan.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON (trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 2) 
      JOIN mjenis_operasi ON mjenis_operasi.id = tkamaroperasi_pendaftaran.jenis_operasi_id
      JOIN mdokter ON mdokter.id = tkamaroperasi_jasada.iddokteranastesi
      JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran
      GROUP BY
      	tkamaroperasi_jasada.id,
      	tkamaroperasi_jasada.iddokteranastesi
		
		UNION 
		
		SELECT
      	tkamaroperasi_jasada.id AS iddetail,
      	trawatinap_pendaftaran.idtipe,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	tkamaroperasi_pendaftaran.tanggal_trx AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	0 AS idtarif,
      	'Jasa Dokter Anesthesi' AS namatarif,
      	'anaesthesi' AS idtipedokter,
        mdokter_kategori.id AS idkategori,
        mdokter_kategori.nama AS namakategori,
      	tkamaroperasi_jasada.iddokteranastesi AS iddokter,
      	mdokter.nama AS namadokter,
      	tkamaroperasi_jasada.total_acuan AS subtotal,
      	tkamaroperasi_jasada.persen AS persen,
      	tkamaroperasi_jasada.totalkeseluruhan AS total,
      	tkamaroperasi_jasada.diskon AS diskon,
		tkamaroperasi_jasada.totalkeseluruhan - tkamaroperasi_jasada.diskon AS grandtotal,
      	1 AS kuantitas,
      	tkamaroperasi_jasada.statusverif AS statusverifikasi,
      	tkamaroperasi_pendaftaran.total_do AS total_operator,
      	tkamaroperasi_pendaftaran.persen_da AS persentase_anesthesi,
      	tkamaroperasi_pendaftaran.total_acuan_da AS total_anesthesi,
      	tkamaroperasi_pendaftaran.jenis_operasi_id,
      	tkamaroperasi_jasada.persen AS persentase,
        tkamaroperasi_jasada.potongan AS potongan_rs,
        tkamaroperasi_jasada.pajak AS pajak_dokter,
        tkamaroperasi_jasada.periode_pembayaran AS periode_pembayaran,
        tkamaroperasi_jasada.periode_jatuhtempo AS periode_jatuhtempo,
        tkamaroperasi_jasada.status_jasamedis AS status_jasamedis
      FROM
      	tkamaroperasi_jasada
      JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_jasada.idpendaftaranoperasi
      JOIN tkamaroperasi_tindakan ON tkamaroperasi_tindakan.idpendaftaranoperasi = tkamaroperasi_tindakan.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON ( trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id )
      JOIN mjenis_operasi ON mjenis_operasi.id = tkamaroperasi_pendaftaran.jenis_operasi_id
      JOIN mdokter ON mdokter.id = tkamaroperasi_jasada.iddokteranastesi
      JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran
      GROUP BY
      	tkamaroperasi_jasada.id,
      	tkamaroperasi_jasada.iddokteranastesi
      ");
		return $query->result();
	}

	public function viewRincianOperasiJasaAsisten($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	tkamaroperasi_jasaao.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.idtipe,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	tkamaroperasi_pendaftaran.tanggal_trx AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	0 AS idtarif,
      	'Jasa Asisten Operator' AS namatarif,
      	tkamaroperasi_jasaao.id_asisten_dao AS idpegawai,
      	(
      		CASE
      			WHEN ( tkamaroperasi_jasaao.jenis_asisten_dao = 1 ) THEN
      			mdokter.nama
      			WHEN ( tkamaroperasi_jasaao.jenis_asisten_dao = 2 ) THEN
      			mpegawai.nama
      		END
      	) AS namapegawai,
      	1 AS kuantitas,
      	tkamaroperasi_jasaao.jenis_operasi_id,
      	tkamaroperasi_jasaao.jenis_asisten_dao AS jenis_asisten,
      	tkamaroperasi_jasaao.total_acuan AS subtotal,
      	tkamaroperasi_jasaao.persen AS persen,
      	tkamaroperasi_jasaao.totalkeseluruhan AS total,
      	tkamaroperasi_jasaao.diskon AS diskon,
		tkamaroperasi_jasaao.totalkeseluruhan - tkamaroperasi_jasaao.diskon AS grandtotal,
      	tkamaroperasi_jasaao.statusverif AS statusverifikasi,
      	25 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      	FROM
      		tkamaroperasi_jasaao
      JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_jasaao.idpendaftaranoperasi
      JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran
      JOIN mjenis_operasi ON mjenis_operasi.id = tkamaroperasi_pendaftaran.jenis_operasi_id
      LEFT JOIN mdokter ON mdokter.id = tkamaroperasi_jasaao.id_asisten_dao
      LEFT JOIN mpegawai ON mpegawai.id = tkamaroperasi_jasaao.id_asisten_dao
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tkamaroperasi_jasaao.id AND tverifikasi_transaksi_detail.id_kelompok = 25
      WHERE
        trawatinap_pendaftaran.id = $idpendaftaran

      UNION 

      SELECT
      	tkamaroperasi_jasadaa.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.idtipe,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	tkamaroperasi_pendaftaran.tanggal_trx AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	0 AS idtarif,
      	'Jasa Asisten Anasthesi' AS namatarif,
      	tkamaroperasi_jasadaa.id_asisten_da AS idpegawai,
      	(
      		CASE
      			WHEN ( tkamaroperasi_jasadaa.jenis_asisten_da = 1 ) THEN
      			mdokter.nama
      			WHEN ( tkamaroperasi_jasadaa.jenis_asisten_da = 2 ) THEN
      			mpegawai.nama
      		END
      	) AS namapegawai,
      	1 AS kuantitas,
      	tkamaroperasi_jasadaa.jenis_operasi_id,
      	tkamaroperasi_jasadaa.jenis_asisten_da AS jenis_asisten,
      	tkamaroperasi_jasadaa.total_acuan AS subtotal,
      	tkamaroperasi_jasadaa.persen AS persen,
      	tkamaroperasi_jasadaa.totalkeseluruhan AS total,
      	tkamaroperasi_jasadaa.diskon AS diskon,
      	tkamaroperasi_jasadaa.totalkeseluruhan - tkamaroperasi_jasadaa.diskon AS grandtotal,
      	tkamaroperasi_jasadaa.statusverif AS statusverifikasi,
      	25 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      	tkamaroperasi_jasadaa
      JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_jasadaa.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON (trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 2)
      JOIN mjenis_operasi ON mjenis_operasi.id = tkamaroperasi_pendaftaran.jenis_operasi_id
      LEFT JOIN mdokter ON mdokter.id = tkamaroperasi_jasadaa.id_asisten_da
      LEFT JOIN mpegawai ON mpegawai.id = tkamaroperasi_jasadaa.id_asisten_da
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tkamaroperasi_jasadaa.id AND tverifikasi_transaksi_detail.id_kelompok = 25
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran
		UNION 

      SELECT
      	tkamaroperasi_jasadaa.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.idtipe,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	tkamaroperasi_pendaftaran.tanggal_trx AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	0 AS idtarif,
      	'Jasa Asisten Anasthesi' AS namatarif,
      	tkamaroperasi_jasadaa.id_asisten_da AS idpegawai,
      	(
      		CASE
      			WHEN ( tkamaroperasi_jasadaa.jenis_asisten_da = 1 ) THEN
      			mdokter.nama
      			WHEN ( tkamaroperasi_jasadaa.jenis_asisten_da = 2 ) THEN
      			mpegawai.nama
      		END
      	) AS namapegawai,
      	1 AS kuantitas,
      	tkamaroperasi_jasadaa.jenis_operasi_id,
      	tkamaroperasi_jasadaa.jenis_asisten_da AS jenis_asisten,
      	tkamaroperasi_jasadaa.total_acuan AS subtotal,
      	tkamaroperasi_jasadaa.persen AS persen,
      	tkamaroperasi_jasadaa.totalkeseluruhan AS total,
      	tkamaroperasi_jasadaa.diskon AS diskon,
      	tkamaroperasi_jasadaa.totalkeseluruhan - tkamaroperasi_jasadaa.diskon AS grandtotal,
      	tkamaroperasi_jasadaa.statusverif AS statusverifikasi,
      	25 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      	tkamaroperasi_jasadaa
      JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_jasadaa.idpendaftaranoperasi
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
      JOIN mjenis_operasi ON mjenis_operasi.id = tkamaroperasi_pendaftaran.jenis_operasi_id
      LEFT JOIN mdokter ON mdokter.id = tkamaroperasi_jasadaa.id_asisten_da
      LEFT JOIN mpegawai ON mpegawai.id = tkamaroperasi_jasadaa.id_asisten_da
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tkamaroperasi_jasadaa.id AND tverifikasi_transaksi_detail.id_kelompok = 25
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran
      ");
		return $query->result();
	}

	public function viewRincianOperasiJasaMedisPrint($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	tkamaroperasi_jasado.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	tkamaroperasi_pendaftaran.tanggal_trx AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	concat( 'Jasa Dokter Operator (', mdokter.nama, ')' ) AS namatarif,
      	'operator' AS idtipedokter,
      	tkamaroperasi_jasado.iddokteroperator AS iddokter,
      	mdokter.nama AS namadokter,
      	tkamaroperasi_jasado.totalkeseluruhan AS total,
      	1 AS kuantitas,
      	tkamaroperasi_jasado.totalkeseluruhan AS totalkeseluruhan,
      	tkamaroperasi_jasado.statusverif AS statusverifikasi
      FROM
      	tkamaroperasi_jasado
      JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_jasado.idpendaftaranoperasi
      JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran
      JOIN mjenis_operasi ON mjenis_operasi.id = tkamaroperasi_pendaftaran.jenis_operasi_id
      JOIN mdokter ON mdokter.id = tkamaroperasi_jasado.iddokteroperator
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran
      GROUP BY
      	tkamaroperasi_jasado.id,
      	tkamaroperasi_jasado.iddokteroperator

      UNION ALL

      SELECT
      	tkamaroperasi_jasada.id AS iddetail,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	tkamaroperasi_pendaftaran.tanggal_trx AS tanggal,
      	tkamaroperasi_pendaftaran.tanggaloperasi AS tanggaloperasi,
      	'Jasa Dokter Anesthesi ' AS namatarif,
      	'anaesthesi' AS idtipedokter,
      	tkamaroperasi_jasada.iddokteranastesi AS iddokter,
      	mdokter.nama AS namadokter,
      	sum( DISTINCT tkamaroperasi_jasada.totalkeseluruhan ) AS total,
      	1 AS kuantitas,
      	sum( DISTINCT ( tkamaroperasi_jasada.totalkeseluruhan )) - sum( DISTINCT ( tkamaroperasi_jasada.diskon )) AS totalkeseluruhan,
      	tkamaroperasi_jasada.statusverif AS statusverifikasi
      FROM
      	tkamaroperasi_jasada
      JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_jasada.idpendaftaranoperasi
      JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran
      JOIN mjenis_operasi ON mjenis_operasi.id = tkamaroperasi_pendaftaran.jenis_operasi_id
      JOIN mdokter ON mdokter.id = tkamaroperasi_jasada.iddokteranastesi
      WHERE
      	trawatinap_pendaftaran.id = $idpendaftaran
      GROUP BY
      	tkamaroperasi_jasada.id,
      	tkamaroperasi_jasada.iddokteranastesi
      ");
		return $query->result();
	}

	public function viewRincianAdministrasiRawatJalan($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.tanggaldaftar AS tanggal,
      	tpoliklinik_pendaftaran.id AS idpoliklinik,
      	tpoliklinik_administrasi.idadministrasi AS idtarif,
      	tpoliklinik_administrasi.namatarif AS namatarif,
      	mtarif_administrasi.idtipe AS idtipe,
      	mtarif_administrasi.idjenis AS idjenis,
      	tpoliklinik_administrasi.id AS iddetail,
      	tpoliklinik_administrasi.jasasarana AS jasasarana,
      	tpoliklinik_administrasi.jasasarana_disc AS jasasarana_disc,
      	tpoliklinik_administrasi.jasapelayanan AS jasapelayanan,
      	tpoliklinik_administrasi.jasapelayanan_disc AS jasapelayanan_disc,
      	tpoliklinik_administrasi.bhp AS bhp,
      	tpoliklinik_administrasi.bhp_disc AS bhp_disc,
      	tpoliklinik_administrasi.biayaperawatan AS biayaperawatan,
      	tpoliklinik_administrasi.biayaperawatan_disc AS biayaperawatan_disc,
      	tpoliklinik_administrasi.total AS subtotal,
      	tpoliklinik_administrasi.kuantitas AS kuantitas,
      	tpoliklinik_administrasi.total * tpoliklinik_administrasi.kuantitas AS total,
      	tpoliklinik_administrasi.diskon AS diskon,
      	tpoliklinik_administrasi.totalkeseluruhan AS totalkeseluruhan,
      	tpoliklinik_administrasi.statusverifikasi AS statusverifikasi,
      	1 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail,
      	tkasir.status AS status_kasir
      FROM
      	tpoliklinik_pendaftaran
      LEFT JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
      LEFT JOIN mtarif_administrasi ON mtarif_administrasi.id = mpasien_kelompok.tadm_rawatjalan
      LEFT JOIN trawatinap_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = trawatinap_pendaftaran.id AND tverifikasi_transaksi_detail.id_kelompok = 9
      JOIN tpoliklinik_administrasi ON tpoliklinik_administrasi.idpendaftaran = tpoliklinik_pendaftaran.id
      JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id AND tpoliklinik_tindakan.status = 1
      JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
      WHERE
      	mtarif_administrasi.idjenis = 2 AND
      	tpoliklinik_administrasi.status = '1' AND
      	tkasir.status != '2' AND
      	trawatinap_pendaftaran.id = $idpendaftaran
		
		UNION
		
		SELECT
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.tanggaldaftar AS tanggal,
      	tpoliklinik_pendaftaran.id AS idpoliklinik,
      	tpoliklinik_administrasi.idadministrasi AS idtarif,
      	tpoliklinik_administrasi.namatarif AS namatarif,
      	mtarif_administrasi.idtipe AS idtipe,
      	mtarif_administrasi.idjenis AS idjenis,
      	tpoliklinik_administrasi.id AS iddetail,
      	tpoliklinik_administrasi.jasasarana AS jasasarana,
      	tpoliklinik_administrasi.jasasarana_disc AS jasasarana_disc,
      	tpoliklinik_administrasi.jasapelayanan AS jasapelayanan,
      	tpoliklinik_administrasi.jasapelayanan_disc AS jasapelayanan_disc,
      	tpoliklinik_administrasi.bhp AS bhp,
      	tpoliklinik_administrasi.bhp_disc AS bhp_disc,
      	tpoliklinik_administrasi.biayaperawatan AS biayaperawatan,
      	tpoliklinik_administrasi.biayaperawatan_disc AS biayaperawatan_disc,
      	tpoliklinik_administrasi.total AS subtotal,
      	tpoliklinik_administrasi.kuantitas AS kuantitas,
      	tpoliklinik_administrasi.total * tpoliklinik_administrasi.kuantitas AS total,
      	tpoliklinik_administrasi.diskon AS diskon,
      	tpoliklinik_administrasi.totalkeseluruhan AS totalkeseluruhan,
      	tpoliklinik_administrasi.statusverifikasi AS statusverifikasi,
      	1 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail,
      	tkasir.status AS status_kasir
      FROM
      	tpoliklinik_pendaftaran
      LEFT JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
      LEFT JOIN mtarif_administrasi ON mtarif_administrasi.id = mpasien_kelompok.tkartu_rawatjalan
      LEFT JOIN trawatinap_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
      LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = trawatinap_pendaftaran.id AND tverifikasi_transaksi_detail.id_kelompok = 9
      JOIN tpoliklinik_administrasi ON tpoliklinik_administrasi.idpendaftaran = tpoliklinik_pendaftaran.id
      JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id AND tpoliklinik_tindakan.status = 1
      JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
      WHERE
      	mtarif_administrasi.idjenis = 2 AND
      	tpoliklinik_administrasi.status = '1' AND
      	tkasir.status != '2' AND
      	trawatinap_pendaftaran.id = $idpendaftaran
      ");
		return $query->result();
	}
	public function viewRincianAdministrasiRawatInap($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.nopendaftaran AS nopendaftaran,
      	trawatinap_pendaftaran.tanggaldaftar AS tanggal,
      	trawatinap_administrasi.idadministrasi AS idtarif,
      	trawatinap_administrasi.namatarif AS namatarif,
      	trawatinap_administrasi.id AS iddetail,
      	trawatinap_administrasi.jasasarana AS jasasarana,
      	trawatinap_administrasi.jasasarana_disc AS jasasarana_disc,
      	trawatinap_administrasi.jasapelayanan AS jasapelayanan,
      	trawatinap_administrasi.jasapelayanan_disc AS jasapelayanan_disc,
      	trawatinap_administrasi.bhp AS bhp,
      	trawatinap_administrasi.bhp_disc AS bhp_disc,
      	trawatinap_administrasi.biayaperawatan AS biayaperawatan,
      	trawatinap_administrasi.biayaperawatan_disc AS biayaperawatan_disc,
      	trawatinap_administrasi.tarif AS subtotal,
      	trawatinap_administrasi.kuantitas AS kuantitas,
      	trawatinap_administrasi.tarif * trawatinap_administrasi.kuantitas AS total,
      	trawatinap_administrasi.diskon AS diskon,
      	trawatinap_administrasi.tarifsetelahdiskon AS totalkeseluruhan,
      	trawatinap_administrasi.statusverifikasi AS statusverifikasi
      FROM
      	trawatinap_administrasi
     LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id=trawatinap_administrasi.idpendaftaran
      WHERE
      	trawatinap_administrasi.idpendaftaran = '$idpendaftaran' AND trawatinap_administrasi.`status`='1'
      ");
		return $query->result();
	}

	public function viewRincianFarmasiRanapObat($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	view_union_farmasi.iddetail AS iddetail,
      	view_union_farmasi.idrawatinap AS idrawatinap,
      	view_union_farmasi.idkelompokpasien AS idkelompokpasien,
      	view_union_farmasi.nopenjualan AS nopenjualan,
      	cast( view_union_farmasi.tanggal AS date ) AS tanggal,
      	view_union_farmasi.idbarang AS idbarang,
      	view_union_farmasi.nama AS namatarif,
      	view_union_farmasi.idtipe AS idtipe,
      	view_union_farmasi.idkategori AS idkategori,
      	view_union_farmasi.kuantitas AS kuantitas,
      	view_union_farmasi.hargadasar AS hargadasar,
      	view_union_farmasi.margin AS margin,
      	view_union_farmasi.harga AS hargajual,
      	view_union_farmasi.diskon AS diskon,
      	view_union_farmasi.tuslah AS tuslah,
      	view_union_farmasi.totalharga AS totalkeseluruhan,
      	view_union_farmasi.idunitpelayanan AS idunitpelayanan,
      	view_union_farmasi.unitpelayanan AS unitpelayanan,
      	view_union_farmasi.statusracikan AS statusracikan,
      	view_union_farmasi.statusverifikasi AS statusverifikasi,
      	29 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      	(
      	SELECT
      		tpasien_penjualan.id AS id,
      		trawatinap_pendaftaran.id AS idrawatinap,
      		trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      		tpasien_penjualan.STATUS AS STATUS,
      		tpasien_penjualan.nopenjualan AS nopenjualan,
      		tpasien_penjualan_nonracikan.id AS iddetail,
      		tpasien_penjualan.idtindakan AS idtindakan,
      		tpasien_penjualan.tanggal AS tanggal,
      		tpasien_penjualan.asalrujukan AS asalrujukan,
      		(CASE
      			WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
      			mdata_alkes.id
      			WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
      			mdata_obat.id
      		END) AS idbarang,
      		(CASE
      			WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
      			mdata_alkes.nama
      			WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
      			mdata_obat.nama
      		END) AS nama,
      		(CASE
      			WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
      			mdata_alkes.idkategori
      			WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
      			mdata_obat.idkategori
      		END) AS idkategori,
      		(CASE
      			WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
      			mdata_alkes.kode
      			WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
      			mdata_obat.kode
      		END) AS kode,
      		tpasien_penjualan_nonracikan.idtipe AS idtipe,
      		tpasien_penjualan_nonracikan.kuantitas AS kuantitas,
      		tpasien_penjualan_nonracikan.harga_dasar AS hargadasar,
      		tpasien_penjualan_nonracikan.margin AS margin,
      		tpasien_penjualan_nonracikan.harga AS harga,
      		tpasien_penjualan_nonracikan.diskon_rp AS diskon,
      		tpasien_penjualan_nonracikan.tuslah AS tuslah,
      		tpasien_penjualan_nonracikan.totalharga AS totalharga,
      		munitpelayanan.id AS idunitpelayanan,
      		munitpelayanan.nama AS unitpelayanan,
      		tpasien_penjualan_nonracikan.statusverifikasi AS statusverifikasi,
      		0 AS statusracikan
      	FROM
      		tpasien_penjualan
      		JOIN tpasien_penjualan_nonracikan ON tpasien_penjualan_nonracikan.idpenjualan = tpasien_penjualan.id
      		JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tpasien_penjualan.idtindakan
      		AND tpasien_penjualan.asalrujukan = 3
      		LEFT JOIN mdata_obat ON tpasien_penjualan_nonracikan.idbarang = mdata_obat.id
      		LEFT JOIN munitpelayanan ON tpasien_penjualan_nonracikan.idunit = munitpelayanan.id
      		LEFT JOIN mdata_alkes ON tpasien_penjualan_nonracikan.idbarang = mdata_alkes.id
      	WHERE
      		trawatinap_pendaftaran.id = $idpendaftaran
      		AND tpasien_penjualan.STATUS <> 0

      	UNION ALL

      	SELECT
      		tpasien_penjualan.id AS id,
      		trawatinap_pendaftaran.id AS idrawatinap,
      		trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      		tpasien_penjualan.STATUS AS STATUS,
      		tpasien_penjualan.nopenjualan AS nopenjualan,
      		tpasien_penjualan_racikan.id AS iddetail,
      		tpasien_penjualan.idtindakan AS idtindakan,
      		tpasien_penjualan.tanggal AS tanggal,
      		tpasien_penjualan.asalrujukan AS asalrujukan,
      		0 AS idbarang,
      		tpasien_penjualan_racikan.namaracikan AS nama,
      		0 AS idkategori,
      		'' AS kode,
      		3 AS idtipe,
      		1 AS kuantitas,
      		tpasien_penjualan_racikan.totalharga AS hargadasar,
      		0 AS margin,
      		tpasien_penjualan_racikan.totalharga AS harga,
      		0 AS diskon,
      		tpasien_penjualan_racikan.tuslah AS tuslah,
      		tpasien_penjualan_racikan.totalharga AS totalharga,
      		1 AS idunitpelayanan,
      		'Farmasi' AS unitpelayanan,
      		tpasien_penjualan_racikan.statusverifikasi AS statusverifikasi,
      		1 AS statusracikan
      	FROM
      		tpasien_penjualan
      		JOIN tpasien_penjualan_racikan ON tpasien_penjualan_racikan.idpenjualan = tpasien_penjualan.id
      		JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tpasien_penjualan.idtindakan
      		AND tpasien_penjualan.asalrujukan = 3
      	WHERE
      		trawatinap_pendaftaran.id = $idpendaftaran
      		AND tpasien_penjualan.STATUS <> 0
      	) AS view_union_farmasi
      	LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_union_farmasi.iddetail
      	AND tverifikasi_transaksi_detail.id_kelompok = 18
      WHERE
      view_union_farmasi.idtipe = 3");
	  // print_r($this->db->last_query());exit();
		return $query->result();
	}

	public function viewRincianFarmasiRanapAlkes($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	view_union_farmasi.iddetail AS iddetail,
      	view_union_farmasi.idrawatinap AS idrawatinap,
      	view_union_farmasi.idkelompokpasien AS idkelompokpasien,
      	view_union_farmasi.nopenjualan AS nopenjualan,
      	cast( view_union_farmasi.tanggal AS date ) AS tanggal,
      	view_union_farmasi.idtarif AS idtarif,
      	view_union_farmasi.nama AS namatarif,
      	view_union_farmasi.idtipe AS idtipe,
      	view_union_farmasi.idkategori AS idkategori,
      	view_union_farmasi.kuantitas AS kuantitas,
      	view_union_farmasi.hargadasar AS hargadasar,
      	view_union_farmasi.margin AS margin,
      	view_union_farmasi.harga AS hargajual,
      	view_union_farmasi.diskon AS diskon,
      	view_union_farmasi.tuslah AS tuslah,
      	view_union_farmasi.totalharga AS totalkeseluruhan,
      	view_union_farmasi.idunitpelayanan AS idunitpelayanan,
      	view_union_farmasi.unitpelayanan AS unitpelayanan,
      	view_union_farmasi.statusracikan AS statusracikan,
      	view_union_farmasi.statusverifikasi AS statusverifikasi,
      	29 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      	(
      	SELECT
      		tpasien_penjualan.id AS id,
      		trawatinap_pendaftaran.id AS idrawatinap,
      		trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      		tpasien_penjualan.STATUS AS STATUS,
      		tpasien_penjualan.nopenjualan AS nopenjualan,
      		tpasien_penjualan_nonracikan.id AS iddetail,
      		tpasien_penjualan.idtindakan AS idtindakan,
      		tpasien_penjualan.tanggal AS tanggal,
      		tpasien_penjualan.asalrujukan AS asalrujukan,
      		(CASE
    				WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
    				mdata_alkes.id
    				WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
    				mdata_obat.id
    			END) AS idtarif,
      		(CASE
    				WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
    				mdata_alkes.nama
    				WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
    				mdata_obat.nama
    			END) AS nama,
          (CASE
  					WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
  					mdata_alkes.idkategori
  					WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
  					mdata_obat.idkategori
  				END) AS idkategori,
				  (CASE
						WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
						mdata_alkes.kode
						WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
						mdata_obat.kode
					END) AS kode,
					tpasien_penjualan_nonracikan.idtipe AS idtipe,
					tpasien_penjualan_nonracikan.kuantitas AS kuantitas,
					tpasien_penjualan_nonracikan.harga_dasar AS hargadasar,
					tpasien_penjualan_nonracikan.margin AS margin,
					tpasien_penjualan_nonracikan.harga AS harga,
					tpasien_penjualan_nonracikan.diskon_rp AS diskon,
					tpasien_penjualan_nonracikan.tuslah AS tuslah,
					tpasien_penjualan_nonracikan.totalharga AS totalharga,
					munitpelayanan.id AS idunitpelayanan,
					munitpelayanan.nama AS unitpelayanan,
					tpasien_penjualan_nonracikan.statusverifikasi AS statusverifikasi,
					0 AS statusracikan
				FROM
					tpasien_penjualan
					JOIN tpasien_penjualan_nonracikan ON tpasien_penjualan_nonracikan.idpenjualan = tpasien_penjualan.id
					JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tpasien_penjualan.idtindakan
					AND tpasien_penjualan.asalrujukan = 3
					LEFT JOIN mdata_obat ON tpasien_penjualan_nonracikan.idbarang = mdata_obat.id
					LEFT JOIN munitpelayanan ON tpasien_penjualan_nonracikan.idunit = munitpelayanan.id
					LEFT JOIN mdata_alkes ON tpasien_penjualan_nonracikan.idbarang = mdata_alkes.id
				WHERE
					trawatinap_pendaftaran.id = $idpendaftaran
					AND tpasien_penjualan.STATUS <> 0

        UNION ALL

				SELECT
					tpasien_penjualan.id AS id,
					trawatinap_pendaftaran.id AS idrawatinap,
					trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
					tpasien_penjualan.STATUS AS STATUS,
					tpasien_penjualan.nopenjualan AS nopenjualan,
					tpasien_penjualan_racikan.id AS iddetail,
					tpasien_penjualan.idtindakan AS idtindakan,
					tpasien_penjualan.tanggal AS tanggal,
					tpasien_penjualan.asalrujukan AS asalrujukan,
					0 AS idtarif,
					tpasien_penjualan_racikan.namaracikan AS nama,
					0 AS idkategori,
					'' AS kode,
					3 AS idtipe,
					1 AS kuantitas,
					tpasien_penjualan_racikan.totalharga AS hargadasar,
					0 AS margin,
					tpasien_penjualan_racikan.totalharga AS harga,
					0 AS diskon,
					tpasien_penjualan_racikan.tuslah AS tuslah,
					tpasien_penjualan_racikan.totalharga AS totalharga,
					1 AS idunitpelayanan,
					'Farmasi' AS unitpelayanan,
					tpasien_penjualan_racikan.statusverifikasi AS statusverifikasi,
					1 AS statusracikan
				FROM
					tpasien_penjualan
					JOIN tpasien_penjualan_racikan ON tpasien_penjualan_racikan.idpenjualan = tpasien_penjualan.id
					JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tpasien_penjualan.idtindakan
					AND tpasien_penjualan.asalrujukan = 3
				WHERE
					trawatinap_pendaftaran.id = $idpendaftaran
					AND tpasien_penjualan.STATUS <> 0
				) AS view_union_farmasi
				LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_union_farmasi.iddetail
				AND tverifikasi_transaksi_detail.id_kelompok = 18
			WHERE
			view_union_farmasi.idtipe = 1 AND
      view_union_farmasi.idkategori != 2");
		return $query->result();
	}

	public function viewRincianFarmasiRanapAlkesBantu($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	view_union_farmasi.iddetail AS iddetail,
      	view_union_farmasi.idrawatinap AS idrawatinap,
      	view_union_farmasi.idkelompokpasien AS idkelompokpasien,
      	view_union_farmasi.nopenjualan AS nopenjualan,
      	cast( view_union_farmasi.tanggal AS date ) AS tanggal,
      	view_union_farmasi.idbarang AS idbarang,
      	view_union_farmasi.nama AS namatarif,
      	view_union_farmasi.idtipe AS idtipe,
      	view_union_farmasi.idkategori AS idkategori,
      	view_union_farmasi.kuantitas AS kuantitas,
      	view_union_farmasi.hargadasar AS hargadasar,
      	view_union_farmasi.margin AS margin,
      	view_union_farmasi.harga AS hargajual,
      	view_union_farmasi.diskon AS diskon,
      	view_union_farmasi.tuslah AS tuslah,
      	view_union_farmasi.totalharga AS totalkeseluruhan,
      	view_union_farmasi.idunitpelayanan AS idunitpelayanan,
      	view_union_farmasi.unitpelayanan AS unitpelayanan,
      	view_union_farmasi.statusracikan AS statusracikan,
      	view_union_farmasi.statusverifikasi AS statusverifikasi,
      	29 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail
      FROM
      	(
      	SELECT
      		tpasien_penjualan.id AS id,
      		trawatinap_pendaftaran.id AS idrawatinap,
      		trawatinap_pendaftaran.idkelompokpasien,
      		tpasien_penjualan.STATUS AS STATUS,
      		tpasien_penjualan.nopenjualan AS nopenjualan,
      		tpasien_penjualan_nonracikan.id AS iddetail,
      		tpasien_penjualan.idtindakan AS idtindakan,
      		tpasien_penjualan.tanggal AS tanggal,
      		tpasien_penjualan.asalrujukan AS asalrujukan,
          (CASE
    				WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
    				mdata_alkes.id
    				WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
    				mdata_obat.id
    			END) AS idbarang,
          (CASE
    				WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
    				mdata_alkes.nama
    				WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
    				mdata_obat.nama
    			END) AS nama,
          (CASE
  					WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
  					mdata_alkes.idkategori
  					WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
  					mdata_obat.idkategori
  				END) AS idkategori,
				  (CASE
						WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
						mdata_alkes.kode
						WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
						mdata_obat.kode
					END) AS kode,
					tpasien_penjualan_nonracikan.idtipe AS idtipe,
					tpasien_penjualan_nonracikan.kuantitas AS kuantitas,
					tpasien_penjualan_nonracikan.harga_dasar AS hargadasar,
					tpasien_penjualan_nonracikan.margin AS margin,
					tpasien_penjualan_nonracikan.harga AS harga,
					tpasien_penjualan_nonracikan.diskon_rp AS diskon,
					tpasien_penjualan_nonracikan.tuslah AS tuslah,
					tpasien_penjualan_nonracikan.totalharga AS totalharga,
					munitpelayanan.id AS idunitpelayanan,
					munitpelayanan.nama AS unitpelayanan,
					tpasien_penjualan_nonracikan.statusverifikasi AS statusverifikasi,
					0 AS statusracikan
				FROM
					tpasien_penjualan
					JOIN tpasien_penjualan_nonracikan ON tpasien_penjualan_nonracikan.idpenjualan = tpasien_penjualan.id
					JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tpasien_penjualan.idtindakan
					AND tpasien_penjualan.asalrujukan = 3
					LEFT JOIN mdata_obat ON tpasien_penjualan_nonracikan.idbarang = mdata_obat.id
					LEFT JOIN munitpelayanan ON tpasien_penjualan_nonracikan.idunit = munitpelayanan.id
					LEFT JOIN mdata_alkes ON tpasien_penjualan_nonracikan.idbarang = mdata_alkes.id
				WHERE
					trawatinap_pendaftaran.id = $idpendaftaran
					AND tpasien_penjualan.STATUS <> 0

        UNION ALL

				SELECT
					tpasien_penjualan.id AS id,
					trawatinap_pendaftaran.id AS idrawatinap,
      		trawatinap_pendaftaran.idkelompokpasien,
					tpasien_penjualan.STATUS AS STATUS,
					tpasien_penjualan.nopenjualan AS nopenjualan,
					tpasien_penjualan_racikan.id AS iddetail,
					tpasien_penjualan.idtindakan AS idtindakan,
					tpasien_penjualan.tanggal AS tanggal,
					tpasien_penjualan.asalrujukan AS asalrujukan,
          0 AS idbarang,
					tpasien_penjualan_racikan.namaracikan AS nama,
					0 AS idkategori,
					'' AS kode,
					3 AS idtipe,
					1 AS kuantitas,
					tpasien_penjualan_racikan.totalharga AS hargadasar,
					0 AS margin,
					tpasien_penjualan_racikan.totalharga AS harga,
					0 AS diskon,
					tpasien_penjualan_racikan.tuslah AS tuslah,
					tpasien_penjualan_racikan.totalharga AS totalharga,
					1 AS idunitpelayanan,
					'Farmasi' AS unitpelayanan,
					tpasien_penjualan_racikan.statusverifikasi AS statusverifikasi,
					1 AS statusracikan
				FROM
					tpasien_penjualan
					JOIN tpasien_penjualan_racikan ON tpasien_penjualan_racikan.idpenjualan = tpasien_penjualan.id
					JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tpasien_penjualan.idtindakan
					AND tpasien_penjualan.asalrujukan = 3
				WHERE
					trawatinap_pendaftaran.id = $idpendaftaran
					AND tpasien_penjualan.STATUS <> 0
				) AS view_union_farmasi
				LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_union_farmasi.iddetail
				AND tverifikasi_transaksi_detail.id_kelompok = 18
			WHERE
			view_union_farmasi.idtipe = 1 AND
      view_union_farmasi.idkategori = 2");
		return $query->result();
	}

	public function viewRincianFarmasiRajal($idpendaftaran, $idtipe)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
      	view_union_farmasi.iddetail AS iddetail,
      	tpoliklinik_pendaftaran.id AS idpoliklinik,
      	trawatinap_pendaftaran.id AS idrawatinap,
      	trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      	view_union_farmasi.nopenjualan AS nopenjualan,
      	cast( view_union_farmasi.tanggal AS date ) AS tanggal,
      	view_union_farmasi.idtarif AS idtarif,
      	view_union_farmasi.namatarif AS namatarif,
      	view_union_farmasi.kode AS kode,
      	view_union_farmasi.idtipe AS idtipe,
      	view_union_farmasi.kuantitas AS kuantitas,
      	view_union_farmasi.hargadasar AS hargadasar,
      	view_union_farmasi.margin AS margin,
      	view_union_farmasi.harga AS hargajual,
      	view_union_farmasi.diskon AS diskon,
      	view_union_farmasi.tuslah AS tuslah,
      	view_union_farmasi.idunitpelayanan AS idunitpelayanan,
      	view_union_farmasi.unitpelayanan AS unitpelayanan,
      	view_union_farmasi.totalharga AS totalkeseluruhan,
      	view_union_farmasi.statusracikan AS statusracikan,
      	view_union_farmasi.statusverifikasi AS statusverifikasi,
      	3 AS kelompok,
      	tverifikasi_transaksi_detail.harga_revisi AS harga_revisi,
      	tverifikasi_transaksi_detail.id AS idverifdetail,
      	tkasir.STATUS AS status_kasir
      FROM
      	(
      	SELECT
      		tpasien_penjualan.id AS id,
      		trawatinap_pendaftaran.id AS idrawatinap,
        	trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
      		tpasien_penjualan.STATUS AS STATUS,
      		tpasien_penjualan.nopenjualan AS nopenjualan,
      		tpasien_penjualan_nonracikan.id AS iddetail,
      		tpasien_penjualan.idtindakan AS idtindakan,
      		tpasien_penjualan.tanggal AS tanggal,
      		tpasien_penjualan.asalrujukan AS asalrujukan,
          (CASE
    				WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
    				mdata_alkes.id
    				WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
    				mdata_obat.id
    			END) AS idtarif,
          (CASE
    				WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
    				mdata_alkes.nama
    				WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
    				mdata_obat.nama
    			END) AS namatarif,
          (CASE
  					WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
  					mdata_alkes.idkategori
  					WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
  					mdata_obat.idkategori
  				END) AS idkategori,
				  (CASE
						WHEN tpasien_penjualan_nonracikan.idtipe = 1 THEN
						mdata_alkes.kode
						WHEN tpasien_penjualan_nonracikan.idtipe = 3 THEN
						mdata_obat.kode
					END) AS kode,
					tpasien_penjualan_nonracikan.idtipe AS idtipe,
					tpasien_penjualan_nonracikan.kuantitas AS kuantitas,
					tpasien_penjualan_nonracikan.harga_dasar AS hargadasar,
					tpasien_penjualan_nonracikan.margin AS margin,
					tpasien_penjualan_nonracikan.harga AS harga,
					tpasien_penjualan_nonracikan.diskon_rp AS diskon,
					tpasien_penjualan_nonracikan.tuslah AS tuslah,
					tpasien_penjualan_nonracikan.totalharga AS totalharga,
					munitpelayanan.id AS idunitpelayanan,
					munitpelayanan.nama AS unitpelayanan,
					tpasien_penjualan_nonracikan.statusverifikasi AS statusverifikasi,
					0 AS statusracikan
				FROM
					tpasien_penjualan
          JOIN tpasien_penjualan_nonracikan ON tpasien_penjualan_nonracikan.idpenjualan = tpasien_penjualan.id
					JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpasien_penjualan.idtindakan AND tpasien_penjualan.asalrujukan IN (1, 2)
					JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
					JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
					LEFT JOIN mdata_obat ON tpasien_penjualan_nonracikan.idbarang = mdata_obat.id
					LEFT JOIN munitpelayanan ON tpasien_penjualan_nonracikan.idunit = munitpelayanan.id
					LEFT JOIN mdata_alkes ON tpasien_penjualan_nonracikan.idbarang = mdata_alkes.id
				WHERE
					trawatinap_pendaftaran.id = $idpendaftaran AND
          tpasien_penjualan_nonracikan.idtipe = $idtipe

        UNION ALL

				SELECT
					tpasien_penjualan.id AS id,
					trawatinap_pendaftaran.id AS idrawatinap,
        	trawatinap_pendaftaran.idkelompokpasien AS idkelompokpasien,
					tpasien_penjualan.STATUS AS STATUS,
					tpasien_penjualan.nopenjualan AS nopenjualan,
					tpasien_penjualan_racikan.id AS iddetail,
					tpasien_penjualan.idtindakan AS idtindakan,
					tpasien_penjualan.tanggal AS tanggal,
					tpasien_penjualan.asalrujukan AS asalrujukan,
          0 AS idtarif,
					tpasien_penjualan_racikan.namaracikan AS namatarif,
					0 AS idkategori,
					'' AS kode,
					3 AS idtipe,
					1 AS kuantitas,
					tpasien_penjualan_racikan.totalharga AS hargadasar,
					0 AS margin,
					tpasien_penjualan_racikan.totalharga AS harga,
					0 AS diskon,
					tpasien_penjualan_racikan.tuslah AS tuslah,
					tpasien_penjualan_racikan.totalharga AS totalharga,
					1 AS idunitpelayanan,
					'Farmasi' AS unitpelayanan,
					tpasien_penjualan_racikan.statusverifikasi AS statusverifikasi,
					1 AS statusracikan
				FROM
					tpasien_penjualan
					JOIN tpasien_penjualan_racikan ON tpasien_penjualan_racikan.idpenjualan = tpasien_penjualan.id
					JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpasien_penjualan.idtindakan AND tpasien_penjualan.asalrujukan IN (1, 2)
					JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
					JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
				WHERE
					trawatinap_pendaftaran.id = $idpendaftaran
					AND 3 = 3
				) AS view_union_farmasi
				JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = view_union_farmasi.idtindakan
				AND view_union_farmasi.asalrujukan IN ( 1, 2 )
				JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
				LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
				LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = view_union_farmasi.iddetail
				AND tverifikasi_transaksi_detail.id_kelompok = 3
				JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id
				AND tkasir.idtipe IN ( 1, 2 )
			WHERE
			view_union_farmasi.STATUS <> 0 AND tkasir.STATUS != 2");
		return $query->result();
	}

	public function viewFeeJasaDokterVisite($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("CALL FeeJasaDokterVisite($idpendaftaran)");

		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		return $query->result();
	}

	public function viewFeeOperasiJasaDokterOpertor($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("CALL FeeOperasiJasaDokterOperator($idpendaftaran)");

		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		return $query->result();
	}
}

/* End of file Trawatinap_tindakan_model.php */
/* Location: ./application/models/Trawatinap_tindakan_model.php */
