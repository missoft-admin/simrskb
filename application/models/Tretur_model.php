<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tretur_model extends CI_Model
{
    public function getListTransaksi() {
        /* @asalrujukan
         * 0 : Pasien Non Rujukan
         * 1 : Rawat Jalan
         * 2 : Instalasi Gawat Darurat
         * 3 : Rawat Inap
         */

        $this->db->select('tpasien_penjualan.asalrujukan, tpasien_penjualan.id, tpasien_penjualan.nopenjualan, tpasien_penjualan.nomedrec, tpasien_penjualan.nama, tpasien_penjualan.totalbarang, tpasien_penjualan.totalharga, tpasien_penjualan.idbarang' );
        $this->db->join('tpasien_penjualan_nonracikan', 'tpasien_penjualan_nonracikan.idpenjualan = tpasien_penjualan.id', 'left');
        $query = $this->db->get('tpasien_penjualan');
        return $query->result();
    }
}

?>