<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpengaturan_form_laboratorium_bank_darah_hasil_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('merm_pengaturan_form_laboratorium_bank_darah_hasil');
        return $query->row();
    }

    public function getJenisPetugasProses()
    {
        $this->db->select('jenis_petugas_proses_id');
        $this->db->where('pengaturan_id', '1');
        $query = $this->db->get('merm_pengaturan_form_laboratorium_bank_darah_hasil_petugas');
        
        $result = $query->result_array();

        // Menghasilkan array yang berisi hanya nilai jenis_petugas_proses_id
        $jenisPetugasArray = array_column($result, 'jenis_petugas_proses_id');

        return $jenisPetugasArray;
    }

    public function saveData()
    {
        $this->edit_nomor_laboratorium = $_POST['edit_nomor_laboratorium'];
        $this->edit_prioritas_pemeriksaan = $_POST['edit_prioritas_pemeriksaan'];
        $this->edit_waktu_pengambilan = $_POST['edit_waktu_pengambilan'];
        $this->edit_petugas_pengambilan = $_POST['edit_petugas_pengambilan'];
        $this->action_tidak_normal = $_POST['action_tidak_normal'];
        $this->action_flag = $_POST['action_flag'];
        $this->warna_flag = $_POST['warna_flag'];
        $this->edit_pasien_puasa = $_POST['edit_pasien_puasa'];
        $this->edit_pengiriman_hasil = $_POST['edit_pengiriman_hasil'];
        $this->action_nilai_kritis = $_POST['action_nilai_kritis'];
        $this->action_periksa_keluar = $_POST['action_periksa_keluar'];
        $this->warna_tidak_normal = $_POST['warna_tidak_normal'];
        $this->warna_nilai_kritis = $_POST['warna_nilai_kritis'];

        $this->db->where('id', 1);
        if ($this->db->update('merm_pengaturan_form_laboratorium_bank_darah_hasil', $this)) {
            $jenis_petugas_proses = $_POST['jenis_petugas_proses'];
            
            $this->db->where('pengaturan_id', 1);
            $this->db->delete('merm_pengaturan_form_laboratorium_bank_darah_hasil_petugas');
            foreach ($jenis_petugas_proses as $jenis_id) {
                $data = [
                    'pengaturan_id' => '1',
                    'jenis_petugas_proses_id' => $jenis_id
                ];
                $this->db->insert('merm_pengaturan_form_laboratorium_bank_darah_hasil_petugas', $data);
            }
            
            return true;
        }

        return false;
    }
}
