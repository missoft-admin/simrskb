<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mvariable_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('mvariable.id', $id);
        $query = $this->db->get('mvariable');
        return $query->row();
    }

    public function getSubData($id)
    {
        $this->db->where('idvariable', $id);
        $query = $this->db->get('mvariable_sub');
        return $query->result();
    }

    public function getAkunData($id, $idtipeakun, $idtipevariable)
    {
        $this->db->select('mvariable_akun.*, makun_nomor.namaakun');
        $this->db->join('makun_nomor', 'makun_nomor.noakun = mvariable_akun.noakun');
        $this->db->where('mvariable_akun.idvariable', $id);
        $this->db->where('mvariable_akun.tipeakun', $idtipeakun);
        $this->db->where('mvariable_akun.tipevariable', $idtipevariable);
        $query = $this->db->get('mvariable_akun');
        return $query->result();
    }

    public function saveData()
    {
        $this->nourut   = $_POST['nourut'];
        $this->nama   = $_POST['nama'];
        $this->idtipe = $_POST['idtipe'];
        $this->idsub  = $_POST['idsub'];
		$this->created_by  = $this->session->userdata('user_id');
        $this->created_date  =date('Y-m-d H:i:s');
        if ($this->db->insert('mvariable', $this)) {
            $id=$this->db->insert_id();
            return $id;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nourut   = $_POST['nourut'];
        $this->nama   = $_POST['nama'];
        $this->idtipe = $_POST['idtipe'];
        $this->idsub  = $_POST['idsub'];
        $this->edited_by  = $this->session->userdata('user_id');
        $this->edited_date  =date('Y-m-d H:i:s');
        if ($this->db->update('mvariable', $this, array('id' => $_POST['id']))) {
           
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
        return $status;
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('mvariable', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
