<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Antrian_tiket_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function get_index_setting(){
		$q="SELECT *
			 FROM antrian_tiket_setting
			WHERE id='1'";
		return $this->db->query($q)->row_array();
	}
	function list_running(){
		$q="SELECT * FROM antrian_tiket_setting_running_text M WHERE M.`status`='1' ORDER BY M.id";
		return $this->db->query($q)->result();
	}
	function last_data(){
		$q="SELECT H.id,H.kodeantrian,M.nama_pelayanan ,H.waktu_ambil 
			FROM antrian_harian H 
			INNER JOIN antrian_pelayanan M ON M.id=H.antrian_id
			WHERE H.tanggal=CURRENT_DATE()
			ORDER BY H.id DESC
			LIMIT 1";
		return $this->db->query($q)->row_array();
	}
	
}
