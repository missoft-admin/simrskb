<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mtarif_laboratorium_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllTarif($id)
    {
        $this->db->where('idtarif', $id);
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_laboratorium_detail');

        return $query->result();
    }

    public function getSpecified($id)
    {
        $this->db->select('mtarif_laboratorium.*, mtlabparent.idkelompok AS idkelompokparent, mtlabparent.idpaket AS idpaketparent');
        $this->db->join('mtarif_laboratorium mtlabparent', 'mtlabparent.path = mtarif_laboratorium.headerpath', 'LEFT');
        $this->db->where('mtarif_laboratorium.id', $id);
        $query = $this->db->get('mtarif_laboratorium');

        return $query->row();
    }

    public function saveData()
    {
        $this->kode = $_POST['kode'];
        $this->nama = $_POST['nama'];
        $this->nama_english = $_POST['nama_english'];
        $this->metode = $_POST['metode'];
        $this->sumber_spesimen = $_POST['sumber_spesimen'];
        $this->group_test = $_POST['group_test'];
        $this->idtipe = $_POST['idtipe'];
        $this->idkelompok = $_POST['idkelompok'];
        $this->idpaket = $_POST['idpaket'];
        $this->level0 = $_POST['level0'];
        $this->headerpath = $_POST['headerpath'];
        $this->path = $_POST['path'];
        $this->level = $_POST['level'];
        $this->status = 1;
        $this->created_by = $this->session->userdata('user_id');
        $this->created_date = date('Y-m-d H:i:s');

        if ($this->db->insert('mtarif_laboratorium', $this)) {
            $idtarif = $this->db->insert_id();
            $tariflist = json_decode($_POST['data_tarif']);
            foreach ($tariflist as $row) {
                $detail = [];
                $detail['idtarif'] = $idtarif;
                $detail['kelas'] = $row->kelas;

                $detail['jasasarana'] = RemoveComma($row->jasasarana);
                $detail['group_jasasarana'] = $row->group_jasasarana;
                $detail['tarif_cito_persentase_jasasarana'] = RemoveComma($row->tarif_cito_persentase_jasasarana);
                $detail['tarif_cito_rp_jasasarana'] = RemoveComma($row->tarif_cito_rp_jasasarana);
                $detail['tarif_diskon_persentase_jasasarana'] = RemoveComma($row->tarif_diskon_persentase_jasasarana);
                $detail['tarif_diskon_rp_jasasarana'] = RemoveComma($row->tarif_diskon_rp_jasasarana);

                $detail['jasapelayanan'] = RemoveComma($row->jasapelayanan);
                $detail['group_jasapelayanan'] = $row->group_jasapelayanan;
                $detail['tarif_cito_persentase_jasapelayanan'] = RemoveComma($row->tarif_cito_persentase_jasapelayanan);
                $detail['tarif_cito_rp_jasapelayanan'] = RemoveComma($row->tarif_cito_rp_jasapelayanan);
                $detail['tarif_diskon_persentase_jasapelayanan'] = RemoveComma($row->tarif_diskon_persentase_jasapelayanan);
                $detail['tarif_diskon_rp_jasapelayanan'] = RemoveComma($row->tarif_diskon_rp_jasapelayanan);

                $detail['bhp'] = RemoveComma($row->bhp);
                $detail['group_bhp'] = $row->group_bhp;
                $detail['tarif_cito_persentase_bhp'] = RemoveComma($row->tarif_cito_persentase_bhp);
                $detail['tarif_cito_rp_bhp'] = RemoveComma($row->tarif_cito_rp_bhp);
                $detail['tarif_diskon_persentase_bhp'] = RemoveComma($row->tarif_diskon_persentase_bhp);
                $detail['tarif_diskon_rp_bhp'] = RemoveComma($row->tarif_diskon_rp_bhp);

                $detail['biayaperawatan'] = RemoveComma($row->biayaperawatan);
                $detail['group_biayaperawatan'] = $row->group_biayaperawatan;
                $detail['tarif_cito_persentase_biayaperawatan'] = RemoveComma($row->tarif_cito_persentase_biayaperawatan);
                $detail['tarif_cito_rp_biayaperawatan'] = RemoveComma($row->tarif_cito_rp_biayaperawatan);
                $detail['tarif_diskon_persentase_biayaperawatan'] = RemoveComma($row->tarif_diskon_persentase_biayaperawatan);
                $detail['tarif_diskon_rp_biayaperawatan'] = RemoveComma($row->tarif_diskon_rp_biayaperawatan);

                $detail['total'] = RemoveComma($row->total);
                $detail['status'] = '1';

                $this->db->insert('mtarif_laboratorium_detail', $detail);
            }

            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;
    }

    public function updateData()
    {
        $this->kode = $_POST['kode'];
        $this->nama = $_POST['nama'];
        $this->nama_english = $_POST['nama_english'];
        $this->metode = $_POST['metode'];
        $this->sumber_spesimen = $_POST['sumber_spesimen'];
        $this->group_test = $_POST['group_test'];
        $this->idtipe = $_POST['idtipe'];
        $this->idkelompok = $_POST['idkelompok'];
        $this->idpaket = $_POST['idpaket'];
        $this->level0 = $_POST['level0'];
        $this->headerpath = $_POST['headerpath'];
        $this->path = $_POST['path'];
        $this->level = $_POST['level'];
        $this->status = 1;
        $this->edited_by = $this->session->userdata('user_id');
        $this->edited_date = date('Y-m-d H:i:s');

        if ($this->db->update('mtarif_laboratorium', $this, ['id' => $_POST['id']])) {
            $idtarif = $_POST['id'];
            $tariflist = json_decode($_POST['data_tarif']);

            $this->db->where('idtarif', $idtarif);
            if ($this->db->delete('mtarif_laboratorium_detail')) {
                foreach ($tariflist as $row) {
                    $detail = [];
                    $detail['idtarif'] = $idtarif;
                    $detail['kelas'] = $row->kelas;

                    $detail['jasasarana'] = RemoveComma($row->jasasarana);
                    $detail['group_jasasarana'] = $row->group_jasasarana;
                    $detail['tarif_cito_persentase_jasasarana'] = RemoveComma($row->tarif_cito_persentase_jasasarana);
                    $detail['tarif_cito_rp_jasasarana'] = RemoveComma($row->tarif_cito_rp_jasasarana);
                    $detail['tarif_diskon_persentase_jasasarana'] = RemoveComma($row->tarif_diskon_persentase_jasasarana);
                    $detail['tarif_diskon_rp_jasasarana'] = RemoveComma($row->tarif_diskon_rp_jasasarana);

                    $detail['jasapelayanan'] = RemoveComma($row->jasapelayanan);
                    $detail['group_jasapelayanan'] = $row->group_jasapelayanan;
                    $detail['tarif_cito_persentase_jasapelayanan'] = RemoveComma($row->tarif_cito_persentase_jasapelayanan);
                    $detail['tarif_cito_rp_jasapelayanan'] = RemoveComma($row->tarif_cito_rp_jasapelayanan);
                    $detail['tarif_diskon_persentase_jasapelayanan'] = RemoveComma($row->tarif_diskon_persentase_jasapelayanan);
                    $detail['tarif_diskon_rp_jasapelayanan'] = RemoveComma($row->tarif_diskon_rp_jasapelayanan);

                    $detail['bhp'] = RemoveComma($row->bhp);
                    $detail['group_bhp'] = $row->group_bhp;
                    $detail['tarif_cito_persentase_bhp'] = RemoveComma($row->tarif_cito_persentase_bhp);
                    $detail['tarif_cito_rp_bhp'] = RemoveComma($row->tarif_cito_rp_bhp);
                    $detail['tarif_diskon_persentase_bhp'] = RemoveComma($row->tarif_diskon_persentase_bhp);
                    $detail['tarif_diskon_rp_bhp'] = RemoveComma($row->tarif_diskon_rp_bhp);

                    $detail['biayaperawatan'] = RemoveComma($row->biayaperawatan);
                    $detail['group_biayaperawatan'] = $row->group_biayaperawatan;
                    $detail['tarif_cito_persentase_biayaperawatan'] = RemoveComma($row->tarif_cito_persentase_biayaperawatan);
                    $detail['tarif_cito_rp_biayaperawatan'] = RemoveComma($row->tarif_cito_rp_biayaperawatan);
                    $detail['tarif_diskon_persentase_biayaperawatan'] = RemoveComma($row->tarif_diskon_persentase_biayaperawatan);
                    $detail['tarif_diskon_rp_biayaperawatan'] = RemoveComma($row->tarif_diskon_rp_biayaperawatan);

                    $detail['total'] = RemoveComma($row->total);
                    $detail['status'] = '1';

                    $this->db->insert('mtarif_laboratorium_detail', $detail);
                }
            }

            // IF CHANGE PARENT
            if ($_POST['headerpath'] != $_POST['old_headerpath']) {
                $this->db->query("call updateTarifLaboratorium('".$_POST['id']."','".$_POST['headerpath']."')");
            } else if ($_POST['level0'] != $_POST['old_level0']) {
                $this->db->query("call updateTarifLaboratorium('".$_POST['id']."','".$_POST['level0']."')");
            }

            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;
    }

    public function softDelete($id)
    {
        $this->status = 0;
        $this->deleted_by = $this->session->userdata('user_id');
        $this->deleted_date = date('Y-m-d H:i:s');

        if ($this->db->update('mtarif_laboratorium', $this, ['id' => $id])) {
            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;
    }

    public function getAllParent($idtipe)
    {
        $this->db->where('idtipe', $idtipe);
        $this->db->where('status', '1');
        $this->db->order_by('path', 'ASC');
        $query = $this->db->get('mtarif_laboratorium');

        return $query->result();
    }

    public function getParent($idtipe)
    {
        // Get ID Root
        $this->db->select('MAX(CAST(headerpath AS SIGNED)) + 1 AS idroot');
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_laboratorium');
        $idroot = $query->row()->idroot;

        // Get Option
        $this->db->where('mtarif_laboratorium.idtipe', $idtipe);
        $this->db->where('mtarif_laboratorium.status', '1');
        $this->db->order_by('mtarif_laboratorium.path', 'ASC');
        $query = $this->db->get('mtarif_laboratorium');
        $result = $query->result();

        $data = '<option value="'.($idroot ?: 1).'">Root</option>';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data .= '<option value="'.$row->path.'">'.TreeView($row->level, $row->nama).'</option>';
            }
        }

        return $data;
    }

    public function getPathLevel($headerpath)
    {
        $strquery = "SELECT idpaket, LEVEL + 1 AS level, CASE WHEN ( SELECT COUNT(headerpath) FROM mtarif_laboratorium WHERE headerpath = t1.headerpath ) = 0 THEN CONCAT(t1.path, '.1') ELSE  CONCAT(t1.path,'.',  IF ( ( SELECT COUNT(headerpath) FROM mtarif_laboratorium WHERE headerpath = t1.path ) = 0, ( SELECT COUNT(headerpath) FROM mtarif_laboratorium WHERE headerpath = t1.path ) + 1, IF ( LENGTH(t1.path) = '1',( SELECT COUNT(headerpath) FROM mtarif_laboratorium WHERE headerpath = t1.path ), ( SELECT COUNT(headerpath) FROM mtarif_laboratorium WHERE headerpath = t1.path ) + 1 ) ) ) END AS path FROM mtarif_laboratorium t1 WHERE t1.path = '".$headerpath."'";
        $query = $this->db->query($strquery);
        $row = $query->row();

        if ($query->num_rows() > 0) {
            $data['idpaket'] = $row->idpaket;
            $data['path'] = $row->path;
            $data['level'] = $row->level;
        } else {
            $data['idpaket'] = 0;
            $data['path'] = 1;
            $data['level'] = 0;
        }

        return $data;
    }

    public function getLevel0($idtipe = '0')
    {
        $q = "select * from mtarif_laboratorium where status=1 AND idkelompok = 1 AND idtipe={$idtipe} ORDER BY path ASC";
        $query = $this->db->query($q);

        return $query->result();
    }

    public function updateSetting()
    {
        $settingList = json_decode($_POST['setting_value']);
        foreach ($settingList as $row) {
            $data = [];
            $data['group_jasasarana'] = $row[3];
            $data['group_jasapelayanan'] = $row[4];
            $data['group_bhp'] = $row[5];
            $data['group_biayaperawatan'] = $row[6];

            $this->db->update(
                'mtarif_laboratorium_detail',
                $data,
                [
                    'idtarif' => $row[0],
                    'kelas' => $row[1],
                ]
            );
        }
        $settingDiskonList = json_decode($_POST['setting_diskon_value']);
        foreach ($settingDiskonList as $row) {
            $data = [];
            $data['group_jasasarana_diskon'] = $row[3];
            $data['group_jasapelayanan_diskon'] = $row[4];
            $data['group_bhp_diskon'] = $row[5];
            $data['group_biayaperawatan_diskon'] = $row[6];

            $this->db->update(
                'mtarif_laboratorium_detail',
                $data,
                [
                    'idtarif' => $row[0],
                    'kelas' => $row[1],
                ]
            );
        }
        $id = $this->input->post('id');
        $group_diskon_all = $this->input->post('group_diskon_all');
        $this->db->where('id', $id);
        $this->db->update('mtarif_laboratorium', ['group_diskon_all' => $group_diskon_all]);

        return true;
    }

    public function find_headparent($idtipe, $idkelompokpasien, $idrekanan)
    {
        $this->db->select('mtarif_laboratorium.path, mtarif_laboratorium.level, mtarif_laboratorium.nama');
        if (1 == $idkelompokpasien) {
            $this->db->select('mtarif_laboratorium.path');
            $this->db->join('mrekanan', 'mrekanan.tlaboratorium_umum = mtarif_laboratorium.id');
            $this->db->where('mrekanan.id', $idrekanan);
            $query = $this->db->get('mtarif_laboratorium');

            if ($query->num_rows() > 0) {
                if (1 == $idtipe) {
                    $this->db->join('mrekanan', 'mrekanan.tlaboratorium_umum = mtarif_laboratorium.id');
                } elseif (2 == $idtipe) {
                    $this->db->join('mrekanan', 'mrekanan.tlaboratorium_pa = mtarif_laboratorium.id');
                } else {
                    $this->db->join('mrekanan', 'mrekanan.tlaboratorium_pmi = mtarif_laboratorium.id');
                }
                $this->db->where('mrekanan.id', $idrekanan);
            } else {
                if (1 == $idtipe) {
                    $this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_umum = mtarif_laboratorium.id');
                } elseif (2 == $idtipe) {
                    $this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_pa = mtarif_laboratorium.id');
                } else {
                    $this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_pmi = mtarif_laboratorium.id');
                }
                $this->db->where('mpasien_kelompok.id', $idkelompokpasien);
            }
        } else {
            if (1 == $idtipe) {
                $this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_umum = mtarif_laboratorium.id');
            } elseif (2 == $idtipe) {
                $this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_pa = mtarif_laboratorium.id');
            } else {
                $this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_pmi = mtarif_laboratorium.id');
            }
            $this->db->where('mpasien_kelompok.id', $idkelompokpasien);
        }

        $this->db->where('mtarif_laboratorium.idkelompok', '1');
        $this->db->where('mtarif_laboratorium.status', '1');
        $this->db->order_by('mtarif_laboratorium.path', 'ASC');
        $query = $this->db->get('mtarif_laboratorium');
        $result = $query->result();

        $arr = '';
        foreach ($result as $row) {
            $arr = $arr.'<option value="'.$row->path.'">'.TreeView($row->level, $row->nama).'</option>';
        }

        return $arr;
    }

    public function find_headparent_all($idtipe)
    {
        $q = "select * from mtarif_laboratorium where status=1 AND idkelompok = 1 AND idtipe={$idtipe} ORDER BY path ASC";
        $query = $this->db->query($q);
        $result = $query->result();

        $arr = '';
        foreach ($result as $row) {
            $arr = $arr.'<option value="'.$row->path.'">'.TreeView($row->level, $row->nama).'</option>';
        }

        return $arr;
    }

    // # Update Document ERM
    public function find_index_parent($idtipe = '0')
    {
        $this->db->where('mtarif_laboratorium.status', '1');
        $this->db->where('mtarif_laboratorium.idkelompok', '1');
        $this->db->where('mtarif_laboratorium.level', '0');
        
        if ('0' != $idtipe) {
            $this->db->where('mtarif_laboratorium.idtipe', $idtipe);
        }

        $this->db->order_by('mtarif_laboratorium.path', 'ASC');
        $query = $this->db->get('mtarif_laboratorium');
        
        return $query->result();
    }

    public function find_index_subparent($idparent = '0')
    {
        $this->db->where('mtarif_laboratorium.status', '1');
        $this->db->where('mtarif_laboratorium.idkelompok', '1');
        $this->db->where('mtarif_laboratorium.level !=', '0');
        
        if ('0' != $idparent) {
            $this->db->where('left(mtarif_laboratorium.path,' . strlen($idparent) . ')', $idparent);
        }

        $this->db->order_by('mtarif_laboratorium.path', 'ASC');
        $query = $this->db->get('mtarif_laboratorium');
        
        return $query->result();
    }

    public function find_manage_parent($idtipe)
    {
        // Get ID Root
        $this->db->select('MAX(CAST(headerpath AS SIGNED)) + 1 AS idroot');
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_laboratorium');
        $idroot = $query->row()->idroot;

        // Get Option
        $this->db->where('mtarif_laboratorium.idtipe', $idtipe);
        $this->db->where('mtarif_laboratorium.level', '0');
        $this->db->where('mtarif_laboratorium.status', '1');
        $this->db->order_by('mtarif_laboratorium.path', 'ASC');
        $query = $this->db->get('mtarif_laboratorium');
        $result = $query->result();

        $data = '<option value="'.($idroot ?: 1).'">Root</option>';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data .= '<option value="'.$row->path.'">'.TreeView($row->level, $row->nama).'</option>';
            }
        }

        return $data;
    }

    public function find_manage_subparent($idparent)
    {
        // Get Option
        $this->db->where('left(mtarif_laboratorium.path,' . strlen($idparent) . ')', $idparent);
        $this->db->where('mtarif_laboratorium.level !=', '0');
        $this->db->where('mtarif_laboratorium.status', '1');
        $this->db->order_by('mtarif_laboratorium.path', 'ASC');
        $query = $this->db->get('mtarif_laboratorium');
        $result = $query->result();

        $data = '';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data .= '<option value="'.$row->path.'">'.TreeView($row->level, $row->nama).'</option>';
            }
        }

        return $data;
    }

    public function find_update_parent($idtipe)
    {
        // Get ID Root
        $this->db->select('MAX(CAST(headerpath AS SIGNED)) + 1 AS idroot');
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_laboratorium');
        $idroot = $query->row()->idroot;

        // Get Option
        $this->db->where('mtarif_laboratorium.idtipe', $idtipe);
        $this->db->where('mtarif_laboratorium.level', '0');
        $this->db->where('mtarif_laboratorium.status', '1');
        $this->db->order_by('mtarif_laboratorium.path', 'ASC');
        $query = $this->db->get('mtarif_laboratorium');
        return $query->result();
    }

    public function find_update_subparent($idparent)
    {
        // Get Option
        $this->db->where('left(mtarif_laboratorium.path,' . strlen($idparent) . ')', $idparent);
        $this->db->where('mtarif_laboratorium.level !=', '0');
        $this->db->where('mtarif_laboratorium.status', '1');
        $this->db->order_by('mtarif_laboratorium.path', 'ASC');
        $query = $this->db->get('mtarif_laboratorium');
        return $query->result();
    }
}
