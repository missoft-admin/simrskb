<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mlogic_honor_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mlogic');
        return $query->row();
    }
	
	public function list_unit($id)
    {
        $q="SELECT S.idunit as id,U.nama FROM munitpelayanan_user_setting S
			LEFT JOIN munitpelayanan U ON U.id=S.idunit
			WHERE S.idunit NOT IN (SELECT idunit from mlogic_unit)
			GROUP BY
			S.idunit";
        return $this->db->query($q)->result();
    }
	
	public function list_user($step,$idtipe)
    {
        $q="SELECT *FROM musers M
				WHERE M.id NOT IN (SELECT iduser from mlogic_honor WHERE mlogic_honor.step='$step' 
										AND mlogic_honor.idtipe='$idtipe' 
										AND mlogic_honor.status='1') AND M.status='1'";
		// print_r($q);
        return $this->db->query($q)->result();
    }
    
    
    function list_tipe_dokter()
    {
        return $this->db->get('mdokter_kategori')->result_array();
    }
	
    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mlogic_honor', $this, array('id' => $id))) {
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;

        if ($this->db->update('mlogic_honor', $this, array('id' => $id))) {
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function load_all()
    {
        $query = $this->db->get('mlogic_honor');
        return $query->result();
    }
}
