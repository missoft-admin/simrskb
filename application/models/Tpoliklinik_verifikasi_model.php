<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpoliklinik_verifikasi_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function viewRincianAdministrasi($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			tpoliklinik_administrasi.id AS iddetail,
			tpoliklinik_pendaftaran.id AS idpendaftaran,
			tpoliklinik_pendaftaran.nopendaftaran AS nopendaftaran,
			tpoliklinik_pendaftaran.tanggaldaftar AS tanggal,
			tpoliklinik_administrasi.idadministrasi AS idtarif,
			tpoliklinik_administrasi.namatarif,
			tpoliklinik_administrasi.jasasarana,
			tpoliklinik_administrasi.jasasarana_disc,
			tpoliklinik_administrasi.jasapelayanan,
			tpoliklinik_administrasi.jasapelayanan_disc,
			(tpoliklinik_administrasi.jasapelayanan - tpoliklinik_administrasi.jasapelayanan_disc) * tpoliklinik_administrasi.kuantitas AS jasamedis,
			tpoliklinik_administrasi.bhp,
			tpoliklinik_administrasi.bhp_disc,
			tpoliklinik_administrasi.biayaperawatan,
			tpoliklinik_administrasi.biayaperawatan_disc,
			tpoliklinik_administrasi.total AS subtotal,
			tpoliklinik_administrasi.kuantitas,
			(tpoliklinik_administrasi.total * tpoliklinik_administrasi.kuantitas) AS total,
			tpoliklinik_administrasi.diskon,
			tpoliklinik_administrasi.totalkeseluruhan AS totalkeseluruhan
		FROM
			tpoliklinik_administrasi
		JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_administrasi.idpendaftaran
		WHERE
			tpoliklinik_administrasi.idpendaftaran = $idpendaftaran
			AND tpoliklinik_administrasi.status = 1
		");
		return $query->result();
	}

	public function viewRincianRajalTindakan($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			tpoliklinik_pelayanan.id AS iddetail,
			tpoliklinik_pendaftaran.id AS idpendaftaran,
			tpoliklinik_pendaftaran.nopendaftaran AS nopendaftaran,
			tpoliklinik_pendaftaran.tanggaldaftar AS tanggal,
			tpoliklinik_pelayanan.idpelayanan AS idtarif,
			mtarif_rawatjalan.nama AS namatarif,
			mdokter_kategori.id AS idkategori,
			mdokter_kategori.nama AS namakategori,
			tpoliklinik_pelayanan.iddokter AS iddokter,
			mdokter.nama AS namadokter,
			tpoliklinik_pelayanan.jasasarana,
			tpoliklinik_pelayanan.jasasarana_disc,
			tpoliklinik_pelayanan.jasapelayanan,
			tpoliklinik_pelayanan.jasapelayanan_disc,
			(tpoliklinik_pelayanan.jasapelayanan - tpoliklinik_pelayanan.jasapelayanan_disc) * tpoliklinik_pelayanan.kuantitas AS jasamedis,
			tpoliklinik_pelayanan.bhp,
			tpoliklinik_pelayanan.bhp_disc,
			tpoliklinik_pelayanan.biayaperawatan,
			tpoliklinik_pelayanan.biayaperawatan_disc,
			tpoliklinik_pelayanan.total AS subtotal,
			tpoliklinik_pelayanan.kuantitas,
			(tpoliklinik_pelayanan.total * tpoliklinik_pelayanan.kuantitas) AS total,
			tpoliklinik_pelayanan.diskon,
			tpoliklinik_pelayanan.totalkeseluruhan AS totalkeseluruhan,
			tpoliklinik_pelayanan.pot_rs AS potongan_rs,
			tpoliklinik_pelayanan.pajak_dokter,
			tpoliklinik_pelayanan.periode_pembayaran,
			tpoliklinik_pelayanan.periode_jatuhtempo,
			tpoliklinik_pelayanan.status_jasamedis,
			IF(mrumahsakit_tarif.id, 1, 0) AS status_rujukan
		FROM
			tpoliklinik_pendaftaran
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			LEFT JOIN tpoliklinik_pelayanan ON tpoliklinik_pelayanan.idtindakan = tpoliklinik_tindakan.id
			LEFT JOIN mtarif_rawatjalan ON mtarif_rawatjalan.id = tpoliklinik_pelayanan.idpelayanan
			LEFT JOIN mdokter ON mdokter.id = tpoliklinik_pelayanan.iddokter
			LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
			LEFT JOIN mrumahsakit_tarif ON mrumahsakit_tarif.idrumahsakit = tpoliklinik_pendaftaran.idrujukan AND mrumahsakit_tarif.idtarif = mtarif_rawatjalan.id AND mrumahsakit_tarif.reference_table = 'mtarif_rawatjalan'
		WHERE
			tpoliklinik_pendaftaran.id = $idpendaftaran
			AND tpoliklinik_pelayanan.status != 0
		");
		return $query->result();
	}

	public function viewRincianRajalObat($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			tpoliklinik_obat.id AS iddetail,
			tpoliklinik_pendaftaran.id AS idpendaftaran,
			tpoliklinik_pendaftaran.nopendaftaran AS nopendaftaran,
			tpoliklinik_pendaftaran.tanggaldaftar AS tanggal,
			tpoliklinik_pendaftaran.idkelompokpasien AS idkelompokpasien,
			tpoliklinik_obat.idunit AS idunit,
			munitpelayanan.nama AS namaunit,
			mdata_obat.id AS idtarif,
			mdata_obat.nama AS namatarif,
			tpoliklinik_obat.hargadasar,
			tpoliklinik_obat.margin,
			tpoliklinik_obat.hargajual,
			tpoliklinik_obat.kuantitas,
			tpoliklinik_obat.diskon,
			tpoliklinik_obat.totalkeseluruhan
		FROM
			tpoliklinik_pendaftaran
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			INNER JOIN tpoliklinik_obat ON tpoliklinik_obat.idtindakan = tpoliklinik_tindakan.id
			INNER JOIN munitpelayanan ON munitpelayanan.id = tpoliklinik_obat.idunit
			LEFT JOIN mdata_obat ON mdata_obat.id = tpoliklinik_obat.idobat
		WHERE
			tpoliklinik_pendaftaran.id = $idpendaftaran
		");
		return $query->result();
	}

	public function viewRincianRajalAlkes($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			tpoliklinik_alkes.id AS iddetail,
			tpoliklinik_pendaftaran.id AS idpendaftaran,
			tpoliklinik_pendaftaran.nopendaftaran AS nopendaftaran,
			tpoliklinik_pendaftaran.tanggaldaftar AS tanggal,
			tpoliklinik_pendaftaran.idkelompokpasien AS idkelompokpasien,
			tpoliklinik_alkes.idunit AS idunit,
			munitpelayanan.nama AS namaunit,
			mdata_alkes.id AS idtarif,
			mdata_alkes.nama AS namatarif,
			tpoliklinik_alkes.hargadasar,
			tpoliklinik_alkes.margin,
			tpoliklinik_alkes.hargajual,
			tpoliklinik_alkes.kuantitas,
			tpoliklinik_alkes.diskon,
			tpoliklinik_alkes.totalkeseluruhan
		FROM
			tpoliklinik_pendaftaran
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			INNER JOIN tpoliklinik_alkes ON tpoliklinik_alkes.idtindakan = tpoliklinik_tindakan.id
			INNER JOIN munitpelayanan ON munitpelayanan.id = tpoliklinik_alkes.idunit
			LEFT JOIN mdata_alkes ON mdata_alkes.id = tpoliklinik_alkes.idalkes
		WHERE
			tpoliklinik_pendaftaran.id = $idpendaftaran
		");
		return $query->result();
	}

	public function viewRincianLaboratorium($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			trujukan_laboratorium_detail.id AS iddetail,
			tpoliklinik_pendaftaran.id AS idpendaftaran,
			trujukan_laboratorium.norujukan AS norujukan,
			trujukan_laboratorium.tanggal AS tanggal,
			trujukan_laboratorium_detail.idlaboratorium AS idtarif,
			mtarif_laboratorium.nama AS namatarif,
			mdokter_kategori.id AS idkategori,
			mdokter_kategori.nama AS namakategori,
			COALESCE(trujukan_laboratorium_detail.iddokter, trujukan_laboratorium.iddokterperujuk) AS iddokter,
			mdokter.nama AS namadokter,
			trujukan_laboratorium_detail.jasasarana,
			trujukan_laboratorium_detail.jasasarana_disc,
			trujukan_laboratorium_detail.jasapelayanan,
			trujukan_laboratorium_detail.jasapelayanan_disc,
			(trujukan_laboratorium_detail.jasapelayanan - trujukan_laboratorium_detail.jasapelayanan_disc) * trujukan_laboratorium_detail.kuantitas AS jasamedis,
			trujukan_laboratorium_detail.bhp,
			trujukan_laboratorium_detail.bhp_disc,
			trujukan_laboratorium_detail.biayaperawatan,
			trujukan_laboratorium_detail.biayaperawatan_disc,
			trujukan_laboratorium_detail.total AS subtotal,
			trujukan_laboratorium_detail.kuantitas,
			(trujukan_laboratorium_detail.total * trujukan_laboratorium_detail.kuantitas) AS total,
			trujukan_laboratorium_detail.diskon,
			trujukan_laboratorium_detail.totalkeseluruhan AS totalkeseluruhan,
			trujukan_laboratorium_detail.potongan_rs,
			trujukan_laboratorium_detail.pajak_dokter,
			trujukan_laboratorium_detail.periode_pembayaran,
			trujukan_laboratorium_detail.periode_jatuhtempo,
			trujukan_laboratorium_detail.status_jasamedis,
			IF(mrumahsakit_tarif.id, 1, 0) AS status_rujukan
		FROM
			tpoliklinik_pendaftaran
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			LEFT JOIN trujukan_laboratorium ON trujukan_laboratorium.idtindakan = tpoliklinik_tindakan.id AND trujukan_laboratorium.asalrujukan IN ( 1, 2 )
			LEFT JOIN trujukan_laboratorium_detail ON trujukan_laboratorium_detail.idrujukan = trujukan_laboratorium.id
			LEFT JOIN mtarif_laboratorium ON mtarif_laboratorium.id = trujukan_laboratorium_detail.idlaboratorium
			LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_laboratorium_detail.iddokter, trujukan_laboratorium.iddokterperujuk)
			LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
			LEFT JOIN mrumahsakit_tarif ON mrumahsakit_tarif.idrumahsakit = tpoliklinik_pendaftaran.idrujukan AND mrumahsakit_tarif.idtarif = mtarif_laboratorium.id AND mrumahsakit_tarif.reference_table = 'mtarif_laboratorium'
		WHERE
			tpoliklinik_pendaftaran.id = $idpendaftaran
			AND trujukan_laboratorium_detail.statusrincianpaket = 0
			AND trujukan_laboratorium.status != 0
		");
		return $query->result();
	}

	public function viewRincianRadiologi($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			trujukan_radiologi_detail.id AS iddetail,
			tpoliklinik_pendaftaran.id AS idpendaftaran,
			trujukan_radiologi.norujukan AS norujukan,
			trujukan_radiologi.tanggal AS tanggal,
			trujukan_radiologi_detail.idradiologi AS idtarif,
			mtarif_radiologi.idtipe,
			(
			CASE
				WHEN mtarif_radiologi.idtipe = '1' THEN
				CONCAT(mtarif_radiologi.nama, ' (', mtarif_radiologi_expose.nama, ' ', mtarif_radiologi_film.nama, ')')
			ELSE mtarif_radiologi.nama
			END
			) AS namatarif,
			mdokter_kategori.id AS idkategori,
			mdokter_kategori.nama AS namakategori,
			COALESCE(trujukan_radiologi_detail.iddokter, trujukan_radiologi.iddokterradiologi) AS iddokter,
			mdokter.nama AS namadokter,
			trujukan_radiologi_detail.jasasarana,
			trujukan_radiologi_detail.jasasarana_disc,
			trujukan_radiologi_detail.jasapelayanan,
			trujukan_radiologi_detail.jasapelayanan_disc,
			(
			CASE
				WHEN mtarif_radiologi.idtipe = '1' THEN
					trujukan_radiologi_detail.nominal_jasamedis * trujukan_radiologi_detail.kuantitas
				ELSE 
					(trujukan_radiologi_detail.jasapelayanan - trujukan_radiologi_detail.jasapelayanan_disc) * trujukan_radiologi_detail.kuantitas
			END
			) AS jasamedis,
			trujukan_radiologi_detail.bhp,
			trujukan_radiologi_detail.bhp_disc,
			trujukan_radiologi_detail.biayaperawatan,
			trujukan_radiologi_detail.biayaperawatan_disc,
			trujukan_radiologi_detail.total AS subtotal,
			trujukan_radiologi_detail.kuantitas,
			(trujukan_radiologi_detail.total * trujukan_radiologi_detail.kuantitas) AS total,
			trujukan_radiologi_detail.diskon,
			trujukan_radiologi_detail.totalkeseluruhan AS totalkeseluruhan,
			COALESCE(trujukan_radiologi_detail.status_tindakan, trujukan_radiologi.statuspasien) AS status_tindakan,
			trujukan_radiologi_detail.potongan_rs,
			trujukan_radiologi_detail.pajak_dokter,
			trujukan_radiologi_detail.periode_pembayaran,
			trujukan_radiologi_detail.periode_jatuhtempo,
			trujukan_radiologi_detail.status_jasamedis,
			trujukan_radiologi_detail.status_tindakan AS status_expertise,
			IF(mrumahsakit_tarif.id, 1, 0) AS status_rujukan
		FROM
			tpoliklinik_pendaftaran
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			LEFT JOIN trujukan_radiologi ON trujukan_radiologi.idtindakan = tpoliklinik_tindakan.id AND trujukan_radiologi.asalrujukan IN ( 1, 2 )
			LEFT JOIN trujukan_radiologi_detail ON trujukan_radiologi_detail.idrujukan = trujukan_radiologi.id
			LEFT JOIN mtarif_radiologi ON mtarif_radiologi.id = trujukan_radiologi_detail.idradiologi
			LEFT JOIN mtarif_radiologi_expose ON mtarif_radiologi.idexpose = mtarif_radiologi_expose.id
			LEFT JOIN mtarif_radiologi_film ON mtarif_radiologi.idfilm = mtarif_radiologi_film.id
			LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_radiologi_detail.iddokter, trujukan_radiologi.iddokterradiologi)
			LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
			LEFT JOIN mrumahsakit_tarif ON mrumahsakit_tarif.idrumahsakit = tpoliklinik_pendaftaran.idrujukan AND mrumahsakit_tarif.idtarif = mtarif_radiologi.id AND mrumahsakit_tarif.reference_table = 'mtarif_radiologi'
		WHERE
			tpoliklinik_pendaftaran.id = $idpendaftaran
			AND trujukan_radiologi.status != 0
			AND trujukan_radiologi_detail.id IS NOT NULL
		");
		return $query->result();
	}

	public function viewRincianFisioterapi($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			trujukan_fisioterapi_detail.id AS iddetail,
			tpoliklinik_pendaftaran.id AS idpendaftaran,
			trujukan_fisioterapi.norujukan AS norujukan,
			trujukan_fisioterapi.tanggal AS tanggal,
			trujukan_fisioterapi_detail.idfisioterapi AS idtarif,
			mtarif_fisioterapi.nama AS namatarif,
			mdokter_kategori.id AS idkategori,
			mdokter_kategori.nama AS namakategori,
			COALESCE(trujukan_fisioterapi_detail.iddokter, trujukan_fisioterapi.iddokterperujuk) AS iddokter,
			mdokter.nama AS namadokter,
			trujukan_fisioterapi_detail.jasasarana,
			trujukan_fisioterapi_detail.jasasarana_disc,
			trujukan_fisioterapi_detail.jasapelayanan,
			trujukan_fisioterapi_detail.jasapelayanan_disc,
			(trujukan_fisioterapi_detail.jasapelayanan - trujukan_fisioterapi_detail.jasapelayanan_disc) * trujukan_fisioterapi_detail.kuantitas AS jasamedis,
			trujukan_fisioterapi_detail.bhp,
			trujukan_fisioterapi_detail.bhp_disc,
			trujukan_fisioterapi_detail.biayaperawatan,
			trujukan_fisioterapi_detail.biayaperawatan_disc,
			trujukan_fisioterapi_detail.total AS subtotal,
			trujukan_fisioterapi_detail.kuantitas,
			( trujukan_fisioterapi_detail.total * trujukan_fisioterapi_detail.kuantitas ) AS total,
			trujukan_fisioterapi_detail.diskon,
			trujukan_fisioterapi_detail.totalkeseluruhan AS totalkeseluruhan,
			trujukan_fisioterapi_detail.potongan_rs,
			trujukan_fisioterapi_detail.pajak_dokter,
			trujukan_fisioterapi_detail.periode_pembayaran,
			trujukan_fisioterapi_detail.periode_jatuhtempo,
			trujukan_fisioterapi_detail.status_jasamedis,
			IF(mrumahsakit_tarif.id, 1, 0) AS status_rujukan
		FROM
			tpoliklinik_pendaftaran
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			LEFT JOIN trujukan_fisioterapi ON trujukan_fisioterapi.idtindakan = tpoliklinik_tindakan.id AND trujukan_fisioterapi.asalrujukan IN ( 1, 2 )
			JOIN trujukan_fisioterapi_detail ON trujukan_fisioterapi_detail.idrujukan = trujukan_fisioterapi.id
			LEFT JOIN mtarif_fisioterapi ON mtarif_fisioterapi.id = trujukan_fisioterapi_detail.idfisioterapi
			LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_fisioterapi_detail.iddokter, trujukan_fisioterapi.iddokterperujuk)
			LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
			LEFT JOIN mrumahsakit_tarif ON mrumahsakit_tarif.idrumahsakit = tpoliklinik_pendaftaran.idrujukan AND mrumahsakit_tarif.idtarif = mtarif_fisioterapi.id AND mrumahsakit_tarif.reference_table = 'mtarif_fisioterapi'
		WHERE
			tpoliklinik_pendaftaran.id = $idpendaftaran
			AND trujukan_fisioterapi.STATUS != 0
		");
		return $query->result();
	}

	public function viewRincianFarmasi($idpendaftaran, $idtipe)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			tpasien_penjualan_nonracikan.id AS iddetail,
			tpoliklinik_pendaftaran.id AS idpendaftaran,
			tpoliklinik_pendaftaran.idkelompokpasien AS idkelompokpasien,
			tpasien_penjualan.nopenjualan AS nopenjualan,
			tpasien_penjualan.tanggal AS tanggal,
			tpasien_penjualan_nonracikan.idunit AS idunit,
			munitpelayanan.nama AS namaunit,
			view_barang.id AS idtarif,
			view_barang.nama AS namatarif,
			tpasien_penjualan_nonracikan.kuantitas,
			tpasien_penjualan_nonracikan.harga_dasar AS hargadasar,
			tpasien_penjualan_nonracikan.margin,
			tpasien_penjualan_nonracikan.harga AS hargajual,
			tpasien_penjualan_nonracikan.diskon_rp,
			tpasien_penjualan_nonracikan.tuslah,
			tpasien_penjualan_nonracikan.diskon_rp AS diskon,
			tpasien_penjualan_nonracikan.totalharga AS totalkeseluruhan
		FROM
			tpoliklinik_pendaftaran
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			LEFT JOIN tpasien_penjualan ON tpasien_penjualan.idtindakan = tpoliklinik_tindakan.id AND tpasien_penjualan.asalrujukan IN ( 1, 2 )
			LEFT JOIN tpasien_penjualan_nonracikan ON tpasien_penjualan_nonracikan.idpenjualan = tpasien_penjualan.id AND tpasien_penjualan_nonracikan.status = 1
			LEFT JOIN view_barang ON view_barang.id = tpasien_penjualan_nonracikan.idbarang AND view_barang.idtipe = tpasien_penjualan_nonracikan.idtipe
			LEFT JOIN munitpelayanan ON munitpelayanan.id = tpasien_penjualan_nonracikan.idunit
		WHERE
			tpoliklinik_pendaftaran.id = $idpendaftaran
			AND tpasien_penjualan_nonracikan.idtipe = $idtipe
			AND tpasien_penjualan.status != 0
		");
		return $query->result();
	}

	public function viewRincianFarmasiObatRacikan($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			tpasien_penjualan_racikan.id AS iddetail,
			tpoliklinik_pendaftaran.id AS idpendaftaran,
			tpasien_penjualan.nopenjualan AS nopenjualan,
			tpasien_penjualan.tanggal AS tanggal,
			0 AS idtarif,
			tpasien_penjualan_racikan.namaracikan AS namatarif,
			tpasien_penjualan_racikan.kuantitas,
			tpasien_penjualan_racikan.totalharga AS hargadasar,
			0 AS margin,
			tpasien_penjualan_racikan.harga AS harga,
			tpasien_penjualan_racikan.totalharga AS hargajual,
			tpasien_penjualan_racikan.tuslah AS tuslah,
			0 AS diskon,
			tpasien_penjualan_racikan.totalharga AS totalkeseluruhan
		FROM
			tpoliklinik_pendaftaran
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			LEFT JOIN tpasien_penjualan ON tpasien_penjualan.idtindakan = tpoliklinik_tindakan.id AND tpasien_penjualan.asalrujukan IN ( 1, 2 )
			LEFT JOIN tpasien_penjualan_racikan ON tpasien_penjualan_racikan.idpenjualan = tpasien_penjualan.id AND tpasien_penjualan_racikan.`status` = '1'
		WHERE
			tpoliklinik_pendaftaran.id = $idpendaftaran
			AND tpasien_penjualan.status != 0
			AND tpasien_penjualan_racikan.id IS NOT NULL
		");
		return $query->result();
	}

	public function viewRincianOB($idpendaftaran, $idtipe)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			tpasien_penjualan_nonracikan.id AS iddetail,
			tpasien_penjualan.id AS idpenjualan,
			tpasien_penjualan.idkelompokpasien AS idkelompokpasien,
			tpasien_penjualan.nopenjualan AS nopenjualan,
			tpasien_penjualan.tanggal AS tanggal,
			tpasien_penjualan_nonracikan.idunit AS idunit,
			munitpelayanan.nama AS namaunit,
			view_barang.id AS idtarif,
			view_barang.nama AS namatarif,
			tpasien_penjualan_nonracikan.kuantitas,
			tpasien_penjualan_nonracikan.harga_dasar AS hargadasar,
			tpasien_penjualan_nonracikan.margin,
			tpasien_penjualan_nonracikan.harga AS hargajual,
			tpasien_penjualan_nonracikan.diskon_rp AS diskon,
			tpasien_penjualan_nonracikan.totalharga AS totalkeseluruhan
		FROM
			tpasien_penjualan_nonracikan
		LEFT JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_penjualan_nonracikan.idpenjualan AND tpasien_penjualan_nonracikan.status = 1
		LEFT JOIN view_barang ON view_barang.id = tpasien_penjualan_nonracikan.idbarang AND view_barang.idtipe = tpasien_penjualan_nonracikan.idtipe
		LEFT JOIN munitpelayanan ON munitpelayanan.id = tpasien_penjualan_nonracikan.idunit
		WHERE
			tpasien_penjualan_nonracikan.idpenjualan = $idpendaftaran AND
			tpasien_penjualan_nonracikan.idtipe = $idtipe
		ORDER BY
			tpasien_penjualan_nonracikan.id ASC
		");
		return $query->result();
	}

	public function viewRincianOBObatRacikan($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			tpasien_penjualan_racikan.id AS iddetail,
			tpasien_penjualan.id AS idpenjualan,
			tpasien_penjualan.nopenjualan AS nopenjualan,
			tpasien_penjualan.tanggal AS tanggal,
			0 AS idtarif,
			tpasien_penjualan_racikan.namaracikan AS namatarif,
			tpasien_penjualan_racikan.kuantitas,
			tpasien_penjualan_racikan.totalharga AS hargadasar,
			0 AS margin,
			tpasien_penjualan_racikan.totalharga AS hargajual,
			0 AS diskon,
			tpasien_penjualan_racikan.totalharga AS totalkeseluruhan
		FROM
			tpasien_penjualan_racikan
		LEFT JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_penjualan_racikan.idpenjualan AND tpasien_penjualan_racikan.status = 1
		WHERE
			tpasien_penjualan_racikan.idpenjualan = $idpendaftaran
		ORDER BY
			tpasien_penjualan_racikan.id ASC
		");
		return $query->result();
	}
}

/* End of file Trawatinap_tindakan_model.php */
/* Location: ./application/models/Trawatinap_tindakan_model.php */
