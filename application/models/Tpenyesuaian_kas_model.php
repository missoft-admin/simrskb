<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpenyesuaian_kas_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
	public function insert_validasi_penyesuaian($id){
		$q="SELECT H.id as idtransaksi 
		,H.tanggal_trx as tanggal_transaksi,H.jenis_kas_id,H.sumber_kas_id,JK.nama as jenis_nama
		,SK.nama as sumber_nama,H.saldo_awal,H.saldo_akhir,H.deskripsi as keterangan
		,CASE WHEN H.saldo_akhir > H.saldo_awal THEN H.saldo_akhir-H.saldo_awal ELSE H.saldo_awal-H.saldo_akhir END as nominal
		,SK.idakun as idakun1
		,compare_value_2(MAX(IF(SD.sumber_kas_id = H.sumber_kas_id,SD.idakun,NULL)),
												MAX(IF(SD.sumber_kas_id = 0 AND SD.jenis_kas_id = H.jenis_kas_id,SD.idakun,NULL))) idakun2
		,CASE WHEN H.saldo_akhir > H.saldo_awal THEN 'D' ELSE 'K' END as posisi_akun1								
		,CASE WHEN H.saldo_akhir > H.saldo_awal THEN 'K' ELSE 'D' END as posisi_akun2,S.st_auto_posting			
		,U.`name` as nama_user
		FROM tpenyesuaian_kas H
		LEFT JOIN mjenis_kas JK ON JK.id=H.jenis_kas_id
		LEFT JOIN msumber_kas SK ON SK.id=H.sumber_kas_id
		LEFT JOIN msetting_jurnal_penyesuaian_detail SD ON SD.jenis_kas_id=H.jenis_kas_id
		LEFT JOIN msetting_jurnal_penyesuaian S ON S.id='1' 
		LEFT JOIN musers U ON U.id=H.user_created
		WHERE H.id='$id'";
		$row=$this->db->query($q)->row();
		$data_header=array(
			'id_reff' => 7,
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'keterangan' => 'PENYESUAIAN KAS '.$row->sumber_nama.' ('.strip_tags($row->keterangan).')',
			'nominal' => $row->nominal,
			// 'idakun' => $row->idakun,
			// 'posisi_akun' => $row->posisi_akun,
			'status' => 1,
			'st_auto_posting' => $row->st_auto_posting,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
            'created_date'=>date('Y-m-d H:i:s')
		);
		// print_r($data_header);exit();
		$st_auto_posting=$row->st_auto_posting;
		$this->db->insert('tvalidasi_kas',$data_header);
		$idvalidasi=$this->db->insert_id();
		$data_detail=array(
			'idvalidasi' =>$idvalidasi,
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'jenis_kas_id' => $row->jenis_kas_id,
			'sumber_kas_id' => $row->sumber_kas_id,
			'jenis_nama' => $row->jenis_nama,
			'sumber_nama' => $row->sumber_nama,
			'saldo_awal' => $row->saldo_awal,
			'saldo_akhir' => $row->saldo_akhir,
			'keterangan' => $row->keterangan,
			'nama_user' => $row->nama_user,
			'nominal' => $row->nominal,
			'idakun1' => $row->idakun1,
			'posisi_akun1' => $row->posisi_akun1,
			'idakun2' => $row->idakun2,
			'posisi_akun2' => $row->posisi_akun2,
			'status' =>1,
			'st_posting' =>0,
		);
		$this->db->insert('tvalidasi_kas_07_penyesuaian',$data_detail);
		
		
		if ($st_auto_posting=='1'){
			$data_header=array(
				
				'st_posting' => 1,
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
				

			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_kas',$data_header);
		}
		return true;
		// print_r('BERAHSIL');
	}
    public function getSpecified($id) {
		$q="SELECT M.id as sumber_kas_id,M.id,J.nama as jenis_kas,M.nama,B.nama as bank,M.saldo as saldo_awal,0 as saldo_akhir
					,M.jenis_kas_id,M.bank_id,CONCAT(A.noakun,' - ',A.namaakun) as noakun
				 FROM msumber_kas M
				LEFT JOIN mjenis_kas J ON J.id=M.jenis_kas_id
				LEFT JOIN mbank B ON B.id=M.bank_id
				LEFT JOIN makun_nomor A ON A.id=M.idakun
				WHERE  M.id='$id' ";
        // $this->db->where('id', $id);
        $query = $this->db->query($q);
        return $query->row_array();
    }
	public function get_header_report($id) {
		$q="SELECT J.nama as jenis_kas,M.nama as sumber_kas,B.nama as bank
				,CONCAT(A.noakun,' - ',A.namaakun) as noakun
				 FROM msumber_kas M
				LEFT JOIN mjenis_kas J ON J.id=M.jenis_kas_id
				LEFT JOIN mbank B ON B.id=M.bank_id
				LEFT JOIN makun_nomor A ON A.id=M.idakun
				WHERE  M.id='$id'";
        // $this->db->where('id', $id);
        $query = $this->db->query($q);
        return $query->row_array();
    }
	public function get_header($id) {
		$q="SELECT * FROM `tpenyesuaian_kas` H
WHERE H.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	public function list_sumber_kas(){
		$this->db->where('status', '1');
        $query = $this->db->get('msumber_kas');
        return $query->result();
	}
	public function list_jenis_kas(){
		$this->db->where('status', '1');
        $query = $this->db->get('mjenis_kas');
        return $query->result();
	}
	public function list_bank(){
		$this->db->where('status', '1');
        $query = $this->db->get('mbank');
        return $query->result();
	}
	public function list_user(){
		$q="SELECT H.user_created as id,U.`name` as nama
			FROM tpenyesuaian_kas H
			LEFT JOIN musers U ON U.id=H.user_created
			GROUP BY H.user_created";
        $query = $this->db->query($q);
        return $query->result();
	}
	
    public function saveData() {
		// print_r($this->input->post());exit();
		$saldo=RemoveComma($_POST['saldo_akhir']);
        $this->tanggal_trx        = YMDFormat($_POST['tanggal_trx']);
        $this->jenis_kas_id        = $_POST['jenis_kas_id'];
        $this->sumber_kas_id        = $_POST['sumber_kas_id'];
        $this->saldo_awal         = $this->db->query("SELECT H.saldo FROM `msumber_kas` H WHERE H.id='".$_POST['sumber_kas_id']."'")->row('saldo');
        $this->saldo_akhir         = RemoveComma($_POST['saldo_akhir']);
        $this->noakun        = $_POST['noakun'];
        $this->bank_id        = $_POST['bank_id'];       
        $this->deskripsi         = $_POST['deskripsi'];
        $this->status         = 1;
		// if ($_POST['btn_simpan']=='2'){
			$this->st_verifikasi         = 1;
		// }
        // $this->statusjurnal   = 0;
        $this->created_at  = date("Y-m-d H:i:s");
        $this->user_created  = $this->session->userdata('user_id');

        if ($this->db->insert('tpenyesuaian_kas', $this)) {
			$id=$this->db->insert_id();
			$this->insert_validasi_penyesuaian($id);
			// $q="UPDATE msumber_kas SET saldo='$saldo' WHERE id='".$_POST['sumber_kas_id']."'";
			// return $this->db->query($q);
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
		
    }
	
}
