<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mrka_bendahara_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
	public function list_jenis_all(){
		$q="SELECT M.id,M.nama 
			FROM mlogic_detail L 
			LEFT JOIN mjenis_pengajuan_rka M ON M.id=L.idjenis
			WHERE L.`status`='1'
			GROUP BY L.idjenis";
		return $this->db->query($q)->result();
	}
    public function saveData() {
		// print_r($this->input->post());exit;
		$data_header=array(
			// 'nama_pemohon'=>$this->input->post('nama_pemohon'),
			'tanggal_pengajuan'=>YMDFormat($this->input->post('tanggal_pengajuan')),
			'tanggal_dibutuhkan'=>YMDFormat($this->input->post('tanggal_dibutuhkan')),
			// // 'tipe_rka'=>$this->input->post('tipe_rka'),
			// 'idunit'=>$this->input->post('idunit'),
			// 'idunit_pengaju'=>$this->input->post('idunit_pengaju'),
			'catatan'=>$this->input->post('catatan'),
			'idvendor'=>$this->input->post('idvendor'),
			// 'idrka_kegiatan'=>$this->input->post('idrka_kegiatan'),
			// 'nama_kegiatan'=>$this->input->post('nama_kegiatan'),
			'grand_total'=> RemoveComma($this->input->post('grand_total')),
			'total_jenis_barang'=> RemoveComma($this->input->post('total_jenis_barang')),
			'total_item'=> RemoveComma($this->input->post('total_item')),
			'cara_pembayaran'=>$this->input->post('cara_pembayaran'),
			'jenis_pembayaran'=>$this->input->post('jenis_pembayaran'),			
			'total_dp_cicilan'=> RemoveComma($this->input->post('total_dp_cicilan')),
			'sisa_cicilan'=> RemoveComma($this->input->post('sisa_cicilan')),
			'jml_kali_cicilan'=> RemoveComma($this->input->post('jml_kali_cicilan')),
			'per_bulan_cicilan'=> RemoveComma($this->input->post('per_bulan_cicilan')),
			'sisa_termin'=> RemoveComma($this->input->post('sisa_termin')),
			'tanggal_kontrabon'=>($this->input->post('tanggal_kontrabon')?YMDFormat($this->input->post('tanggal_kontrabon')):null),
			'xtanggal_cicilan_pertama'=>($this->input->post('xtanggal_cicilan_pertama')?YMDFormat($this->input->post('xtanggal_cicilan_pertama')):null),
			'total_dp_termin'=> RemoveComma($this->input->post('total_dp_termin')),
			// 'idjenis'=>$this->input->post('idjenis'),
			'norek'=>$this->input->post('norek'),
			'bank'=>$this->input->post('bank'),
			'keterangan_bank'=>$this->input->post('keterangan_bank'),
			'rekening_id'=>$this->input->post('rekening_id'),
			'atasnama'=>$this->input->post('atasnama'),
			'edited_by'=> $this->session->userdata('user_id'),
			'edited_nama'=> $this->session->userdata('user_name'),
			'edited_date'=>  date('Y-m-d H:i:s'),
		);
		// print_r($data_header);exit;
		$xnama_barang=$this->input->post('xnama_barang');
		$xmerk_barang=$this->input->post('xmerk_barang');
		$xkuantitas=$this->input->post('xkuantitas');
		$xsatuan=$this->input->post('xsatuan');
		$xharga_satuan=$this->input->post('xharga_satuan');
		$xtotal_harga=$this->input->post('xtotal_harga');
		$xketerangan=$this->input->post('xketerangan');
		$xstatus=$this->input->post('xstatus');
		$xiddet=$this->input->post('xiddet');
		
		//btn_simpan_value
		if ($this->input->post('btn_simpan_value')=='2'){
			$data_header['status']=4;
			$data_header['st_validasi']=1;
			// $q="":
		}
		if ($this->input->post('cara_pembayaran')=='1'){
			$data_header['sisa_termin']=0;
			// $data_header['total_generate_cicilan']=0;
			$data_header['total_dp_termin']=0;
			$data_header['per_bulan_cicilan']=0;
			$data_header['jml_kali_cicilan']=0;
			$data_header['sisa_cicilan']=0;
			$data_header['total_dp_cicilan']=0;
			$arr_bayar=array('1','2','3');
			if (in_array($this->input->post('jenis_pembayaran'), $arr_bayar)) {
				if ($this->input->post('st_berjenjang')=='1'){
					// $q="DELETE FROM rka_pengajuan_detail_asal WHERE idpengajuan='".$_POST['id']."'";
					// $this->db->query($q);
				}
			}
			
		}
		// print_r($this->input->post());exit();
        if ($this->db->update('rka_pengajuan', $data_header, ['id' => $_POST['id']])) {
			if ($this->input->post('btn_simpan_value')=='2'){//Validasi
				if ($this->input->post('cara_pembayaran')=='1'){
					//Hapus tabel cicilan yang tidak perlu karena pembayaran tunai
					$q="delete from rka_pengajuan_cicilan where idpengajuan='".$_POST['id']."'";
					$this->db->query($q);
					$q="delete from rka_pengajuan_termin where idpengajuan='".$_POST['id']."'";
					$this->db->query($q);
					$this->db->query("UPDATE rka_pengajuan_pembayaran SET st_verifikasi='1' WHERE idrka='".$_POST['id']."'");
					$this->insert_validasi_rka($_POST['id']);
			
				}
			}
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function insert_validasi_rka($id){
		$q="SELECT H.id as idvalidasi FROM `tvalidasi_kas` H WHERE H.id_reff='6' AND H.idtransaksi='$id'";
		$id_val=$this->db->query($q)->row('idvalidasi');
		if ($id_val){
			$q="update tvalidasi_kas set st_posting='0',status='0' WHERE id='$id_val'";
			$this->db->query($q);
		}
		$q="SELECT 
			H.id as idtransaksi,CURRENT_DATE as tanggal_transaksi
			,H.no_pengajuan as notransaksi,H.tipe_rka,CASE WHEN H.tipe_rka='1' THEN 'RKA' ELSE 'NON RKA' END as tipe_nama
			,H.idunit,UD.nama as nama_unit
			,H.idunit_pengaju,UP.nama as nama_pengaju
			,H.idrka_kegiatan,H.nama_kegiatan,H.bulan
			,H.idjenis,MJ.nama	as nama_jenis,H.idklasifikasi,MK.nama as klasifikasi,MK.idakun
			,H.idvendor,MV.nama as nama_vendor,H.cara_pembayaran,H.jenis_pembayaran,H.grand_total as nominal
			,H.catatan,'D' as posisi_akun,S.st_auto_posting_pengajuan as st_auto_posting
			FROM rka_pengajuan H
			LEFT JOIN mklasifikasi MK ON MK.id=H.idklasifikasi
			LEFT JOIN mvendor MV ON MV.id=H.idvendor
			LEFT JOIN mjenis_pengajuan_rka MJ ON MJ.id=H.idjenis
			LEFT JOIN munitpelayanan UD ON UD.id=H.idunit
			LEFT JOIN munitpelayanan UP ON UP.id=H.idunit_pengaju
			LEFT JOIN msetting_jurnal_kas_lain S ON S.id='1'
			WHERE H.cara_pembayaran='1' AND H.jenis_pembayaran !='4' AND H.id='$id' ";
		$row=$this->db->query($q)->row();
		if ($row){
			
		
			$data_header=array(
				'id_reff' => '6',
				'idtransaksi' => $row->idtransaksi,
				'tanggal_transaksi' => $row->tanggal_transaksi,
				'notransaksi' => $row->notransaksi,
				'keterangan' => $row->tipe_nama.' : '.$row->nama_kegiatan.' ('.$row->notransaksi.')',
				'nominal' => $row->nominal,
				'idakun' => $row->idakun,
				'posisi_akun' => $row->posisi_akun,
				'status' => 1,
				'st_auto_posting' => $row->st_auto_posting,
				'created_by'=>$this->session->userdata('user_id'),
				'created_nama'=>$this->session->userdata('user_name'),
				'created_date'=>date('Y-m-d H:i:s')
			);
			// print_r($data_header);exit();
			$st_auto_posting=$row->st_auto_posting;
			$this->db->insert('tvalidasi_kas',$data_header);
			$idvalidasi=$this->db->insert_id();
			$data_kasbon=array(
				'idvalidasi' =>$idvalidasi,
				'idtransaksi' =>$row->idtransaksi,
				'tanggal_transaksi' =>$row->tanggal_transaksi,
				'notransaksi' =>$row->notransaksi,
				'tipe_rka' =>$row->tipe_rka,
				'tipe_nama' =>$row->tipe_nama,
				'idunit' =>$row->idunit,
				'nama_unit' =>$row->nama_unit,
				'idunit_pengaju' =>$row->idunit_pengaju,
				'nama_pengaju' =>$row->nama_pengaju,
				'idrka_kegiatan' =>$row->idrka_kegiatan,
				'nama_kegiatan' =>$row->nama_kegiatan,
				'bulan' =>$row->bulan,
				'idjenis' =>$row->idjenis,
				'nama_jenis' =>$row->nama_jenis,
				'idklasifikasi' =>$row->idklasifikasi,
				'klasifikasi' =>$row->klasifikasi,
				'idvendor' =>$row->idvendor,
				'nama_vendor' =>$row->nama_vendor,
				'cara_pembayaran' =>$row->cara_pembayaran,
				'jenis_pembayaran' =>$row->jenis_pembayaran,
				'catatan' =>$row->catatan,
				'nominal' =>$row->nominal,
				'idakun' =>$row->idakun,
				'posisi_akun' =>$row->posisi_akun,
				'st_posting' =>0,
			);
			$this->db->insert('tvalidasi_kas_06_pengajuan',$data_kasbon);
			
			$q="SELECT H.id as iddet
				,(H.harga_pokok*H.kuantitas) as nominal_beli,M.idakun as idakun_beli,'D' as posisi_beli
				,(H.harga_diskon*H.kuantitas) as nominal_diskon,M.idakun_diskon as idakun_diskon,'K' as posisi_diskon
				,(H.harga_ppn*H.kuantitas) as nominal_ppn,M.idakun_ppn as idakun_ppn,'D' as posisi_ppn

				,H.idpengajuan,H.idklasifikasi_det,H.nama_barang,H.merk_barang,H.kuantitas,H.satuan,H.harga_pokok,H.harga_diskon,H.harga_ppn,H.harga_satuan,H.total_harga,H.keterangan

				FROM rka_pengajuan_detail H
				LEFT JOIN mklasifikasi M ON M.id=H.idklasifikasi_det
				WHERE H.idpengajuan='$id' AND H.`status`='1'
";
			$rows=$this->db->query($q)->result();
			foreach($rows as $r){
				$data_detail=array(
					'idvalidasi' =>$idvalidasi,
					'iddet' => $r->iddet,
					'nominal_beli' => $r->nominal_beli,
					'nominal_ppn' => $r->nominal_ppn,
					'nominal_diskon' => $r->nominal_diskon,
					'idakun_beli' => $r->idakun_beli,
					'idakun_ppn' => $r->idakun_ppn,
					'idakun_diskon' => $r->idakun_diskon,
					'posisi_beli' => $r->posisi_beli,
					'posisi_ppn' => $r->posisi_ppn,
					'posisi_diskon' => $r->posisi_diskon,
					'idpengajuan' => $r->idpengajuan,
					'idklasifikasi_det' => $r->idklasifikasi_det,
					'nama_barang' => $r->nama_barang,
					'merk_barang' => $r->merk_barang,
					'kuantitas' => $r->kuantitas,
					'satuan' => $r->satuan,
					'harga_pokok' => $r->harga_pokok,
					'harga_diskon' => $r->harga_diskon,
					'harga_ppn' => $r->harga_ppn,
					'harga_satuan' => $r->harga_satuan,
					'total_harga' => $r->total_harga,
					'keterangan' => $r->keterangan,
				);
				$this->db->insert('tvalidasi_kas_06_pengajuan_detail',$data_detail);
			}
			$q="SELECT 
				H.id as idbayar_id,H.jenis_kas_id,J.nama as jenis_kas_nama
				,H.sumber_kas_id,S.nama as sumber_nama
				,S.bank_id as bankid,mbank.nama as bank
				,H.idmetode,RM.metode_bayar as metode_nama,H.nominal_bayar, S.idakun,'K' as posisi_akun
				FROM rka_pengajuan_pembayaran H
				LEFT JOIN mjenis_kas J ON J.id=H.jenis_kas_id
				LEFT JOIN msumber_kas S ON S.id=H.sumber_kas_id
				LEFT JOIN ref_metode RM ON RM.id=H.idmetode
				LEFT JOIN mbank ON mbank.id=S.bank_id
				WHERE H.idrka='$id'";
			$rows=$this->db->query($q)->result();
			foreach($rows as $r){
				$data_bayar=array(
					'idvalidasi' =>$idvalidasi,
					'idbayar_id' =>$r->idbayar_id,
					'jenis_kas_id' =>$r->jenis_kas_id,
					'jenis_kas_nama' =>$r->jenis_kas_nama,
					'sumber_kas_id' =>$r->sumber_kas_id,
					'sumber_nama' =>$r->sumber_nama,
					'bank' =>$r->bank,
					'bankid' =>$r->bankid,
					'idmetode' =>$r->idmetode,
					'metode_nama' =>$r->metode_nama,
					'nominal_bayar' =>$r->nominal_bayar,
					'idakun' =>$r->idakun,
					'posisi_akun' =>$r->posisi_akun,

				);
				$this->db->insert('tvalidasi_kas_06_pengajuan_bayar',$data_bayar);
			}
			if ($st_auto_posting=='1'){
				$data_header=array(
					
					'st_posting' => 1,
					'posting_by'=>$this->session->userdata('user_id'),
					'posting_nama'=>$this->session->userdata('user_name'),
					'posting_date'=>date('Y-m-d H:i:s')
					

				);
				$this->db->where('id',$idvalidasi);
				$this->db->update('tvalidasi_kas',$data_header);
			}
		}
		return true;
		// print_r('BERAHSIL');
	}
	public function insert_validasi_rka_realisasi($id){
		$q="UPDATE rka_pengajuan_pengembalian SET st_verifikasi='1' WHERE idrka='$id' AND st_verifikasi='0'";
		$this->db->query($q);
		
		$q="SELECT 
			H.id as idtransaksi,CURRENT_DATE as tanggal_transaksi
			,H.no_pengajuan as notransaksi,H.tipe_rka,CASE WHEN H.tipe_rka='1' THEN 'RKA' ELSE 'NON RKA' END as tipe_nama
			,H.idunit,UD.nama as nama_unit
			,H.idunit_pengaju,UP.nama as nama_pengaju
			,H.idrka_kegiatan,H.nama_kegiatan,H.bulan
			,H.idjenis,MJ.nama	as nama_jenis,H.idklasifikasi,MK.nama as klasifikasi,MK.idakun
			,H.idvendor,MV.nama as nama_vendor,H.cara_pembayaran,H.jenis_pembayaran,H.grand_total as nominal
			,H.catatan,'D' as posisi_akun,S.st_auto_posting_pengajuan as st_auto_posting
			FROM rka_pengajuan H
			LEFT JOIN mklasifikasi MK ON MK.id=H.idklasifikasi
			LEFT JOIN mvendor MV ON MV.id=H.idvendor
			LEFT JOIN mjenis_pengajuan_rka MJ ON MJ.id=H.idjenis
			LEFT JOIN munitpelayanan UD ON UD.id=H.idunit
			LEFT JOIN munitpelayanan UP ON UP.id=H.idunit_pengaju
			LEFT JOIN msetting_jurnal_kas_lain S ON S.id='1'
			WHERE H.cara_pembayaran='1' AND H.jenis_pembayaran !='4' AND H.id='$id' ";
		$row=$this->db->query($q)->row();
		if ($row){
			
		
			$data_header=array(
				'id_reff' => '6',
				'idtransaksi' => $row->idtransaksi,
				'tanggal_transaksi' => $row->tanggal_transaksi,
				'notransaksi' => $row->notransaksi,
				'keterangan' => $row->tipe_nama.' : '.$row->nama_kegiatan.' ('.$row->notransaksi.')',
				'nominal' => $row->nominal,
				'idakun' => $row->idakun,
				'posisi_akun' => $row->posisi_akun,
				'status' => 1,
				'st_auto_posting' => $row->st_auto_posting,
				'created_by'=>$this->session->userdata('user_id'),
				'created_nama'=>$this->session->userdata('user_name'),
				'created_date'=>date('Y-m-d H:i:s')
			);
			// print_r($data_header);exit();
			$st_auto_posting=$row->st_auto_posting;
			$this->db->insert('tvalidasi_kas',$data_header);
			$idvalidasi=$this->db->insert_id();
			$data_kasbon=array(
				'idvalidasi' =>$idvalidasi,
				'idtransaksi' =>$row->idtransaksi,
				'tanggal_transaksi' =>$row->tanggal_transaksi,
				'notransaksi' =>$row->notransaksi,
				'tipe_rka' =>$row->tipe_rka,
				'tipe_nama' =>$row->tipe_nama,
				'idunit' =>$row->idunit,
				'nama_unit' =>$row->nama_unit,
				'idunit_pengaju' =>$row->idunit_pengaju,
				'nama_pengaju' =>$row->nama_pengaju,
				'idrka_kegiatan' =>$row->idrka_kegiatan,
				'nama_kegiatan' =>$row->nama_kegiatan,
				'bulan' =>$row->bulan,
				'idjenis' =>$row->idjenis,
				'nama_jenis' =>$row->nama_jenis,
				'idklasifikasi' =>$row->idklasifikasi,
				'klasifikasi' =>$row->klasifikasi,
				'idvendor' =>$row->idvendor,
				'nama_vendor' =>$row->nama_vendor,
				'cara_pembayaran' =>$row->cara_pembayaran,
				'jenis_pembayaran' =>$row->jenis_pembayaran,
				'catatan' =>$row->catatan,
				'nominal' =>$row->nominal,
				'idakun' =>$row->idakun,
				'posisi_akun' =>$row->posisi_akun,
				'st_posting' =>0,
			);
			$this->db->insert('tvalidasi_kas_06_pengajuan',$data_kasbon);
			
			$q="SELECT H.id as iddet
				,(H.harga_pokok*H.kuantitas) as nominal_beli,M.idakun as idakun_beli,'D' as posisi_beli
				,(H.harga_diskon*H.kuantitas) as nominal_diskon,M.idakun_diskon as idakun_diskon,'K' as posisi_diskon
				,(H.harga_ppn*H.kuantitas) as nominal_ppn,M.idakun_ppn as idakun_ppn,'D' as posisi_ppn

				,H.idpengajuan,H.idklasifikasi_det,H.nama_barang,H.merk_barang,H.kuantitas,H.satuan,H.harga_pokok,H.harga_diskon,H.harga_ppn,H.harga_satuan,H.total_harga,H.keterangan

				FROM rka_pengajuan_detail H
				LEFT JOIN mklasifikasi M ON M.id=H.idklasifikasi_det
				WHERE H.idpengajuan='$id' AND H.`status`='1'
";
			$rows=$this->db->query($q)->result();
			foreach($rows as $r){
				$data_detail=array(
					'idvalidasi' =>$idvalidasi,
					'iddet' => $r->iddet,
					'nominal_beli' => $r->nominal_beli,
					'nominal_ppn' => $r->nominal_ppn,
					'nominal_diskon' => $r->nominal_diskon,
					'idakun_beli' => $r->idakun_beli,
					'idakun_ppn' => $r->idakun_ppn,
					'idakun_diskon' => $r->idakun_diskon,
					'posisi_beli' => $r->posisi_beli,
					'posisi_ppn' => $r->posisi_ppn,
					'posisi_diskon' => $r->posisi_diskon,
					'idpengajuan' => $r->idpengajuan,
					'idklasifikasi_det' => $r->idklasifikasi_det,
					'nama_barang' => $r->nama_barang,
					'merk_barang' => $r->merk_barang,
					'kuantitas' => $r->kuantitas,
					'satuan' => $r->satuan,
					'harga_pokok' => $r->harga_pokok,
					'harga_diskon' => $r->harga_diskon,
					'harga_ppn' => $r->harga_ppn,
					'harga_satuan' => $r->harga_satuan,
					'total_harga' => $r->total_harga,
					'keterangan' => $r->keterangan,
				);
				$this->db->insert('tvalidasi_kas_06_pengajuan_detail',$data_detail);
			}
			
			$q="SELECT * FROM `tvalidasi_kas_06_pengajuan_piutang` H WHERE H.idtransaksi='$id' AND posisi_akun='D'";
			$rows=$this->db->query($q)->result();
			foreach($rows as $r){
				$data_tampung=array(
					'idvalidasi' =>$idvalidasi,
					'tanggal_transaksi' =>$row->tanggal_transaksi,
					'nominal' =>$r->nominal,
					'idakun' =>$r->idakun,
					'posisi_akun' =>'K',
					'idtransaksi' => $row->idtransaksi,
					
				);
				$this->db->insert('tvalidasi_kas_06_pengajuan_piutang',$data_tampung);
			}
			$q="SELECT 
				H.id as idbayar_id,H.jenis_kas_id,J.nama as jenis_kas_nama
				,H.sumber_kas_id,S.nama as sumber_nama
				,S.bank_id as bankid,mbank.nama as bank
				,H.idmetode,RM.metode_bayar as metode_nama,H.nominal_bayar, S.idakun,'D' as posisi_akun
				FROM rka_pengajuan_pengembalian H
				LEFT JOIN mjenis_kas J ON J.id=H.jenis_kas_id
				LEFT JOIN msumber_kas S ON S.id=H.sumber_kas_id
				LEFT JOIN ref_metode RM ON RM.id=H.idmetode
				LEFT JOIN mbank ON mbank.id=S.bank_id
				WHERE H.idrka='$id' AND H.st_verifikasi='1'";
			$rows=$this->db->query($q)->result();
			foreach($rows as $r){
				$data_bayar=array(
					'idvalidasi' =>$idvalidasi,
					'idbayar_id' =>$r->idbayar_id,
					'jenis_kas_id' =>$r->jenis_kas_id,
					'jenis_kas_nama' =>$r->jenis_kas_nama,
					'sumber_kas_id' =>$r->sumber_kas_id,
					'sumber_nama' =>$r->sumber_nama,
					'bank' =>$r->bank,
					'bankid' =>$r->bankid,
					'idmetode' =>$r->idmetode,
					'metode_nama' =>$r->metode_nama,
					'nominal_bayar' =>$r->nominal_bayar,
					'idakun' =>$r->idakun,
					'posisi_akun' =>$r->posisi_akun,

				);
				$this->db->insert('tvalidasi_kas_06_pengajuan_pengembalian',$data_bayar);
			}
			if ($st_auto_posting=='1'){
				$data_header=array(
					
					'st_posting' => 1,
					'posting_by'=>$this->session->userdata('user_id'),
					'posting_nama'=>$this->session->userdata('user_name'),
					'posting_date'=>date('Y-m-d H:i:s')
					

				);
				$this->db->where('id',$idvalidasi);
				$this->db->update('tvalidasi_kas',$data_header);
			}
		}
		
		$q="UPDATE rka_pengajuan set st_validasi='1',status='4' WHERE id='".$row->idtransaksi."'";
		$this->db->query($q);
		// print_r('BERAHSIL');
		return true;
	}
	public function insert_validasi_rka_berjenjang($id,$total_trx,$list_bayar,$idakun,$tanggal_pencairan){
		$arr_id=implode(",",$list_bayar);
		$tanggal_pencairan=YMDFormat($tanggal_pencairan);
		// print_r($tanggal_pencairan);exit;
		$q="SELECT 
			H.id as idtransaksi,'$tanggal_pencairan' as tanggal_transaksi
			,H.no_pengajuan as notransaksi,H.tipe_rka,CASE WHEN H.tipe_rka='1' THEN 'RKA' ELSE 'NON RKA' END as tipe_nama
			,H.idunit,UD.nama as nama_unit
			,H.idunit_pengaju,UP.nama as nama_pengaju
			,H.idrka_kegiatan,H.nama_kegiatan,H.bulan
			,H.idjenis,MJ.nama	as nama_jenis,H.idklasifikasi,MK.nama as klasifikasi,$idakun idakun
			,H.idvendor,MV.nama as nama_vendor,H.cara_pembayaran,H.jenis_pembayaran,$total_trx as nominal
			,H.catatan,'D' as posisi_akun,S.st_auto_posting_pengajuan as st_auto_posting,H.st_berjenjang
			FROM rka_pengajuan H
			LEFT JOIN mklasifikasi MK ON MK.id=H.idklasifikasi
			LEFT JOIN mvendor MV ON MV.id=H.idvendor
			LEFT JOIN mjenis_pengajuan_rka MJ ON MJ.id=H.idjenis
			LEFT JOIN munitpelayanan UD ON UD.id=H.idunit
			LEFT JOIN munitpelayanan UP ON UP.id=H.idunit_pengaju
			LEFT JOIN msetting_jurnal_kas_lain S ON S.id='1'
			WHERE H.cara_pembayaran='1' AND H.jenis_pembayaran !='4' AND H.id='$id' ";
		$row=$this->db->query($q)->row();
		// print_r($row);
		// exit;
		if ($row){
			
		
			$data_header=array(
				'id_reff' => '6',
				'idtransaksi' => $row->idtransaksi,
				'tanggal_transaksi' => $row->tanggal_transaksi,
				'notransaksi' => $row->notransaksi,
				'keterangan' => $row->tipe_nama.' : '.$row->nama_kegiatan.' ('.$row->notransaksi.')',
				'nominal' => $row->nominal,
				'idakun' => $row->idakun,
				'posisi_akun' => $row->posisi_akun,
				'status' => 1,
				'st_auto_posting' => $row->st_auto_posting,
				'created_by'=>$this->session->userdata('user_id'),
				'created_nama'=>$this->session->userdata('user_name'),
				'created_date'=>date('Y-m-d H:i:s')
			);
			// print_r($data_header);exit();
			$st_auto_posting=$row->st_auto_posting;
			$this->db->insert('tvalidasi_kas',$data_header);
			$idvalidasi=$this->db->insert_id();
			$data_kasbon=array(
				'idvalidasi' =>$idvalidasi,
				'idtransaksi' =>$row->idtransaksi,
				'tanggal_transaksi' =>$row->tanggal_transaksi,
				'notransaksi' =>$row->notransaksi,
				'tipe_rka' =>$row->tipe_rka,
				'tipe_nama' =>$row->tipe_nama,
				'idunit' =>$row->idunit,
				'nama_unit' =>$row->nama_unit,
				'idunit_pengaju' =>$row->idunit_pengaju,
				'nama_pengaju' =>$row->nama_pengaju,
				'idrka_kegiatan' =>$row->idrka_kegiatan,
				'nama_kegiatan' =>$row->nama_kegiatan,
				'bulan' =>$row->bulan,
				'idjenis' =>$row->idjenis,
				'nama_jenis' =>$row->nama_jenis,
				'idklasifikasi' =>$row->idklasifikasi,
				'klasifikasi' =>$row->klasifikasi,
				'idvendor' =>$row->idvendor,
				'nama_vendor' =>$row->nama_vendor,
				'cara_pembayaran' =>$row->cara_pembayaran,
				'jenis_pembayaran' =>$row->jenis_pembayaran,
				'catatan' =>$row->catatan,
				'nominal' =>$row->nominal,
				'idakun' =>$row->idakun,
				'posisi_akun' =>$row->posisi_akun,
				'st_posting' =>0,
				'st_berjenjang' =>$row->st_berjenjang,
			);
			$this->db->insert('tvalidasi_kas_06_pengajuan',$data_kasbon);
			
			$data_tampung=array(
				'idvalidasi' =>$idvalidasi,
				'tanggal_transaksi' =>$row->tanggal_transaksi,
				'nominal' =>$total_trx,
				'idakun' =>$idakun,
				'posisi_akun' =>'D',
				'idtransaksi' => $row->idtransaksi,
				
			);
			$this->db->insert('tvalidasi_kas_06_pengajuan_piutang',$data_tampung);
			
			
			$q="SELECT 
				H.id as idbayar_id,H.jenis_kas_id,J.nama as jenis_kas_nama
				,H.sumber_kas_id,S.nama as sumber_nama
				,S.bank_id as bankid,mbank.nama as bank
				,H.idmetode,RM.metode_bayar as metode_nama,H.nominal_bayar, S.idakun,'K' as posisi_akun
				FROM rka_pengajuan_pembayaran H
				LEFT JOIN mjenis_kas J ON J.id=H.jenis_kas_id
				LEFT JOIN msumber_kas S ON S.id=H.sumber_kas_id
				LEFT JOIN ref_metode RM ON RM.id=H.idmetode
				LEFT JOIN mbank ON mbank.id=S.bank_id
				WHERE H.idrka='$id' AND H.id IN (".$arr_id.")";
			$rows=$this->db->query($q)->result();
			// print_r($rows);exit;
			foreach($rows as $r){
				$data_bayar=array(
					'idvalidasi' =>$idvalidasi,
					'idbayar_id' =>$r->idbayar_id,
					'jenis_kas_id' =>$r->jenis_kas_id,
					'jenis_kas_nama' =>$r->jenis_kas_nama,
					'sumber_kas_id' =>$r->sumber_kas_id,
					'sumber_nama' =>$r->sumber_nama,
					'bank' =>$r->bank,
					'bankid' =>$r->bankid,
					'idmetode' =>$r->idmetode,
					'metode_nama' =>$r->metode_nama,
					'nominal_bayar' =>$r->nominal_bayar,
					'idakun' =>$r->idakun,
					'posisi_akun' =>$r->posisi_akun,

				);
				$this->db->insert('tvalidasi_kas_06_pengajuan_bayar',$data_bayar);
			}
			if ($st_auto_posting=='1'){
				$data_header=array(
					
					'st_posting' => 1,
					'posting_by'=>$this->session->userdata('user_id'),
					'posting_nama'=>$this->session->userdata('user_name'),
					'posting_date'=>date('Y-m-d H:i:s')
					

				);
				$this->db->where('id',$idvalidasi);
				$this->db->update('tvalidasi_kas',$data_header);
			}
		}
		// print_r('BERAHSIL');
		return true;
	}
    function list_unit_lain(){
		$q="SELECT L.idunit,M.nama FROM mlogic_unit L
			LEFT JOIN munitpelayanan M ON M.id=L.idunit";
		return $this->db->query($q)->result();
	}
	function refresh_image($id){
		$q="SELECT  H.* from rka_pengajuan_upload H

			WHERE H.idrka='$id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/rka_pengajuan/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">'.$r->user_upload.'-'.HumanDateLong($r->tanggal_upload).'</td>';
			$tabel .='<td class="text-left"><button class="btn btn-xs btn-danger hapus_file" title="Hapus"><li class="fa fa-trash-o"></li></button></td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
		}
		return $tabel;
	}
	function refresh_image_bukti($id){
		$q="SELECT  H.* from rka_pengajuan_upload_bukti H

			WHERE H.idrka='$id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/rka_pengajuan/upload_bukti/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">'.$r->user_upload.'-'.HumanDateLong($r->tanggal_upload).'</td>';
			$tabel .='<td class="text-left"><button class="btn btn-xs btn-danger hapus_file_bukti" title="Hapus"><li class="fa fa-trash-o"></li></button></td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
		}
		return $tabel;
	}
	function refresh_pembayaran($id){
		$q="SELECT  H.* from rka_pengajuan_upload H
			WHERE H.idrka='$id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/bukti_penyerahan/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">'.$r->user_upload.'-'.HumanDateLong($r->tanggal_upload).'</td>';
			$tabel .='<td class="text-left"><button class="btn btn-xs btn-danger hapus_file" title="Hapus"><li class="fa fa-trash-o"></li></button></td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
		}
		return $tabel;
	}
	function get_file_name($id){
		$q="select file_name from rka_pengajuan_upload where id='$id'
			";
		return $this->db->query($q)->row();
	}
	function get_file_name_bukti($id){
		$q="select file_name from rka_pengajuan_upload_bukti where id='$id'
			";
		return $this->db->query($q)->row();
	}
	function get_pembayaran($id){
		$q="select * from rka_pengajuan_pembayaran where id='$id'
			";
		return $this->db->query($q)->row_array();
	}
	function get_pengembalian($id){
		$q="select * from rka_pengajuan_pengembalian where id='$id'
			";
		return $this->db->query($q)->row_array();
	}
	function list_jenis_kas(){
		$q="SELECT H.id,H.nama,H.st_default FROM `mjenis_kas` H
			WHERE H.`status`='1'";
		return $this->db->query($q)->result();
	}
}
