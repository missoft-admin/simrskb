<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_gabung_transaksi_history_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getUser()
    {
        $this->db->where('status', 1);
        $query = $this->db->get('musers');
        return $query->result();
    }
}
