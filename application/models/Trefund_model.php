<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trefund_model extends CI_Model
{
	public function getAllIndex($nomedrec = null, $namapasien = null, $tiperefund = null, $tanggalawal = null, $tanggalakhir = null)
	{
		if ($tanggalawal != null) {
			$tanggalawal = date('Y-m-d', strtotime(str_replace('/', '-', (string)$tanggalawal)));
		}

		if ($tanggalakhir != null) {
			$tanggalakhir = date('Y-m-d', strtotime(str_replace('/', '-', (string)$tanggalakhir)));
		}

		if ($nomedrec != null) {
			$this->db->where('view_refund_index.nomedrec', $nomedrec);
		}

		if ($namapasien != '') {
			$this->db->like('view_refund_index.namapasien', $namapasien);
		}

		if ($tiperefund != null) {
			$this->db->where('view_refund_index.tipe', $tiperefund);
		}

		if ($tanggalawal != null && $tanggalakhir != null) {
			$this->db->where('DATE(view_refund_index.tanggal) >=', $tanggalawal);
			$this->db->where('DATE(view_refund_index.tanggal) <=', $tanggalakhir);
		} elseif ($tanggalawal != null) {
			$this->db->where('DATE(view_refund_index.tanggal)', $tanggalawal);
		} elseif ($tanggalakhir != null) {
			$this->db->where('DATE(view_refund_index.tanggal)', $tanggalakhir);
		}

		$this->db->where('status', 1);
		$query = $this->db->get('view_refund_index');
		return $query->result();
	}

	public function removeRefund($id, $alasan)
	{
		$this->db->where('id', $id);
		if ($this->db->update('trefund', ['alasan' => $alasan, 'status' => 0])) {
			return true;
		}
	}

	// public function saveRefundTransaksi($data, $detail)
	// {
	// 	$this->db->insert('trefund', $data);
	// 	$insert_id = $this->db->insert_id();

	// 	if ($this->saveRefundTransaksiDetail($detail, $insert_id)) {
	// 		return true;
	// 	}
	// }

	// public function saveRefundTransaksiDetail($detail, $id)
	// {
	// 	$data = json_decode($detail);

	// 	foreach ($data as $rows) {
	// 		$transaksi = explode('.', $rows[0]);

	// 		$this->db->set([
	// 			'idrefund' => $id,
	// 			'idtransaksi' => $transaksi[0],
	// 			'tipetransaksi' => $transaksi[1],
	// 			'idpendaftaran' => $transaksi[3],
	// 			'idpelayanan' => $transaksi[4],
	// 			'idkelas' => $transaksi[5],
	// 			'idtipe' => $transaksi[2],
	// 			'tarif' => $transaksi[6],
	// 			'kuantitas' => $transaksi[7],
	// 			'totalkeseluruhan' => $transaksi[8],
	// 			'status' => 1
	// 		]);

	// 		$this->db->insert('trefund_detail');
	// 	}

	// 	return true;
	// }

	public function getInfoRefund($id)
	{
		$this->db->select('trefund.id,
		trefund.notransaksi,
		trefund.tanggal AS tanggaltransaksi,
		trefund.norefund,
		trefund.tanggal AS tanggalrefund,
		(CASE
		WHEN trefund.tipe = 0 THEN "Deposit"
		WHEN trefund.tipe = 1 THEN "Retur Obat"
		WHEN trefund.tipe = 2 THEN "Transaksi"
		END) AS tiperefund,
		mfpasien.no_medrec AS nomedrec,
		mfpasien.nama AS namapasien,
		mpasien_kelompok.nama AS kelompokpasien,
		trefund.totaltransaksi,
		trefund.totaldeposit,
		trefund.totalrefund,
		trefund.alasan,
		trefund.metode AS idmetode,
		(CASE
		WHEN trefund.metode = 1 THEN "Tunai"
		WHEN trefund.metode = 2 THEN "Debit"
		WHEN trefund.metode = 3 THEN "Kredit"
		WHEN trefund.metode = 4 THEN "Transfer"
		END) AS metode,
		trefund.bank,
		trefund.norekening,
		trefund.created_by,
		musers.name AS namauser');

		$this->db->join('mfpasien', 'mfpasien.id = trefund.idpasien', 'LEFT');
		$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = trefund.idkelompokpasien', 'LEFT');
		$this->db->join('musers', 'musers.id = trefund.created_by', 'LEFT');

		$this->db->where('trefund.id', $id);
		$query = $this->db->get('trefund');

		return $query->row();
	}

	public function getFakturRefundDeposit($id)
	{
		$this->db->select('trawatinap_deposit.id,
        DATE_FORMAT(trawatinap_deposit.tanggal, "%d/%m/%Y") AS tanggal,
        (CASE
			WHEN trawatinap_deposit.idmetodepembayaran = 1 THEN "Tunai"
			WHEN trawatinap_deposit.idmetodepembayaran = 2 THEN "Debit"
			WHEN trawatinap_deposit.idmetodepembayaran = 3 THEN "Kredit"
			WHEN trawatinap_deposit.idmetodepembayaran = 4 THEN "Transfer"
        END) AS metodepembayaran,
        trawatinap_deposit.nominal');
		$this->db->join('trefund', 'trefund.idtransaksi = trawatinap_deposit.idrawatinap');
		$this->db->where('trefund.id', $id);
		$this->db->where('trawatinap_deposit.status', 1);
		$query = $this->db->get('trawatinap_deposit');
		return $query->result();
	}

	public function getFakturRefundObat($id)
	{
		$query = $this->db->query("SELECT
			tpasien_pengembalian.tanggal,
			(
			CASE
				WHEN tpasien_pengembalian_detail.idtipe = 1 THEN
				mdata_alkes.nama
				WHEN tpasien_pengembalian_detail.idtipe = 3 THEN
				mdata_obat.nama
			END
			) AS namatarif,
			tpasien_pengembalian_detail.kuantitas,
			tpasien_pengembalian_detail.harga,
			tpasien_pengembalian_detail.totalharga
        FROM
			tpasien_pengembalian_detail
        JOIN tpasien_pengembalian ON tpasien_pengembalian.id = tpasien_pengembalian_detail.idpengembalian
		JOIN trefund ON trefund.idtransaksi = tpasien_pengembalian.id
        LEFT JOIN mdata_alkes ON mdata_alkes.id = tpasien_pengembalian_detail.idbarang AND tpasien_pengembalian_detail.idtipe = 1
        LEFT JOIN mdata_obat ON mdata_obat.id = tpasien_pengembalian_detail.idbarang AND tpasien_pengembalian_detail.idtipe = 3
        WHERE
			trefund.id = $id");

		return $query->result();
	}

	public function getFakturRefundTransaksi($id)
	{
		$this->db->where('trefund_detail.idrefund', $id);
		$query = $this->db->get('trefund_detail');

		return $query->result();
	}

	public function getRefundDeposit($tanggalawal = '', $tanggalakhir = '', $idtipetransaksi = '')
	{
		if ($tanggalawal != '' && $tanggalakhir != '') {
			if ($idtipetransaksi != '0') {
				$query = $this->db->query("SELECT * FROM
				(
					SELECT
						trawatinap_pendaftaran.id AS id,
						trawatinap_pendaftaran.nopendaftaran AS notransaksi,
						trawatinap_pendaftaran.tanggaldaftar AS tanggal,
						mfpasien.id AS idpasien,
						mfpasien.no_medrec AS nomedrec,
						mfpasien.nama AS namapasien,
						CONCAT(trawatinap_pendaftaran.nopendaftaran, ' ', '(', mfpasien.nama, ')') AS text,
						mpasien_kelompok.id AS idkelompokpasien,
						mpasien_kelompok.nama AS namakelompokpasien,
						mdokter.nama AS namadokter,
						( CASE WHEN trawatinap_pendaftaran.idtipe = 1 THEN 2 WHEN trawatinap_pendaftaran.idtipe = 2 THEN 3 END ) AS idtipetransaksi,
						( CASE WHEN trawatinap_pendaftaran.idtipe = 1 THEN 'Rawat Inap' WHEN trawatinap_pendaftaran.idtipe = 2 THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi,
						trefund.id AS strefund
					FROM
						trawatinap_deposit
						JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_deposit.idrawatinap AND trawatinap_deposit.status = 1
						JOIN mfpasien ON mfpasien.id = trawatinap_pendaftaran.idpasien
						JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
						LEFT JOIN mrekanan ON mrekanan.id = trawatinap_pendaftaran.idrekanan
						LEFT JOIN mkelas ON mkelas.id = trawatinap_pendaftaran.idkelas
						LEFT JOIN mbed ON mbed.id = trawatinap_pendaftaran.idbed
						JOIN mdokter ON mdokter.id = trawatinap_pendaftaran.iddokterpenanggungjawab
						JOIN trawatinap_tindakan_pembayaran ON trawatinap_tindakan_pembayaran.idtindakan = trawatinap_pendaftaran.id AND trawatinap_tindakan_pembayaran.statusbatal = 0
						LEFT JOIN trefund ON trefund.tipe = 0 AND trefund.idtransaksi = trawatinap_pendaftaran.id AND trefund.status = 1
					WHERE
						trawatinap_pendaftaran.statuspembayaran = 1 AND
						trawatinap_tindakan_pembayaran.deposit > (trawatinap_tindakan_pembayaran.total - trawatinap_tindakan_pembayaran.pembayaran)
					GROUP BY trawatinap_pendaftaran.id
				) AS result
				WHERE result.strefund IS NULL AND result.idtipetransaksi = $idtipetransaksi AND (DATE(result.tanggal) >= '$tanggalawal' AND DATE(result.tanggal) <= '$tanggalakhir')");
			} else {
				$query = $this->db->query("SELECT * FROM
				(
					SELECT
						trawatinap_pendaftaran.id AS id,
						trawatinap_pendaftaran.nopendaftaran AS notransaksi,
						trawatinap_pendaftaran.tanggaldaftar AS tanggal,
						mfpasien.id AS idpasien,
						mfpasien.no_medrec AS nomedrec,
						mfpasien.nama AS namapasien,
						CONCAT(trawatinap_pendaftaran.nopendaftaran, ' ', '(', mfpasien.nama, ')') AS text,
						mpasien_kelompok.id AS idkelompokpasien,
						mpasien_kelompok.nama AS namakelompokpasien,
						mdokter.nama AS namadokter,
						( CASE WHEN ( trawatinap_pendaftaran.idtipe = 1 ) THEN 'Rawat Inap' WHEN ( trawatinap_pendaftaran.idtipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi,
						trefund.id AS strefund
					FROM
						trawatinap_deposit
						JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_deposit.idrawatinap AND trawatinap_deposit.status = 1
						JOIN mfpasien ON mfpasien.id = trawatinap_pendaftaran.idpasien
						JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
						LEFT JOIN mrekanan ON mrekanan.id = trawatinap_pendaftaran.idrekanan
						LEFT JOIN mkelas ON mkelas.id = trawatinap_pendaftaran.idkelas
						LEFT JOIN mbed ON mbed.id = trawatinap_pendaftaran.idbed
						JOIN mdokter ON mdokter.id = trawatinap_pendaftaran.iddokterpenanggungjawab
						JOIN trawatinap_tindakan_pembayaran ON trawatinap_tindakan_pembayaran.idtindakan = trawatinap_pendaftaran.id AND trawatinap_tindakan_pembayaran.statusbatal = 0
						LEFT JOIN trefund ON trefund.tipe = 0 AND trefund.idtransaksi = trawatinap_pendaftaran.id AND trefund.status = 1
					WHERE
						trawatinap_pendaftaran.statuspembayaran = 1 AND
						trawatinap_tindakan_pembayaran.deposit > (trawatinap_tindakan_pembayaran.total - trawatinap_tindakan_pembayaran.pembayaran)
					GROUP BY trawatinap_pendaftaran.id
				) AS result
				WHERE result.strefund IS NULL AND (DATE(result.tanggal) >= '$tanggalawal' AND DATE(result.tanggal) <= '$tanggalakhir')");
			}
		} else {
			$query = $this->db->query("SELECT * FROM
			(
				SELECT
				trawatinap_pendaftaran.id AS id,
				trawatinap_pendaftaran.nopendaftaran AS notransaksi,
				trawatinap_pendaftaran.tanggaldaftar AS tanggal,
				mfpasien.id AS idpasien,
				mfpasien.no_medrec AS nomedrec,
				mfpasien.nama AS namapasien,
				CONCAT(trawatinap_pendaftaran.nopendaftaran, ' ', '(', mfpasien.nama, ')') AS text,
				mpasien_kelompok.id AS idkelompokpasien,
				mpasien_kelompok.nama AS namakelompokpasien,
				mdokter.nama AS namadokter,
				( CASE WHEN ( trawatinap_pendaftaran.idtipe = 1 ) THEN 'Rawat Inap' WHEN ( trawatinap_pendaftaran.idtipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi,
				trefund.id AS strefund
				FROM
				trawatinap_deposit
				JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_deposit.idrawatinap AND trawatinap_deposit.status = 1
				JOIN mfpasien ON mfpasien.id = trawatinap_pendaftaran.idpasien
				JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
				LEFT JOIN mrekanan ON mrekanan.id = trawatinap_pendaftaran.idrekanan
				LEFT JOIN mkelas ON mkelas.id = trawatinap_pendaftaran.idkelas
				LEFT JOIN mbed ON mbed.id = trawatinap_pendaftaran.idbed
				JOIN mdokter ON mdokter.id = trawatinap_pendaftaran.iddokterpenanggungjawab
				JOIN trawatinap_tindakan_pembayaran ON trawatinap_tindakan_pembayaran.idtindakan = trawatinap_pendaftaran.id AND trawatinap_tindakan_pembayaran.statusbatal = 0
				LEFT JOIN trefund ON trefund.tipe = 0 AND trefund.idtransaksi = trawatinap_pendaftaran.id AND trefund.status = 1
				WHERE
				trawatinap_pendaftaran.statuspembayaran = 1 AND
				trawatinap_tindakan_pembayaran.deposit > (trawatinap_tindakan_pembayaran.total - trawatinap_tindakan_pembayaran.pembayaran)
				GROUP BY trawatinap_pendaftaran.id
			) AS result
			WHERE result.strefund IS NULL");
		}

		return $query->result();
	}

	public function getInfoRefundDeposit($id)
	{
		$query = $this->db->query("SELECT
			'rawatinap' AS reference,
			trawatinap_pendaftaran.id AS id,
			mfpasien.no_medrec AS nomedrec,
			mfpasien.nama AS namapasien,
			mfpasien.alamat_jalan AS alamatpasien,
			trawatinap_pendaftaran.tanggaldaftar AS tanggaltransaksi,
			( CASE WHEN ( trawatinap_pendaftaran.idtipe = 1 ) THEN 'Rawat Inap' WHEN ( trawatinap_pendaftaran.idtipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi,
			mpasien_kelompok.nama AS namakelompokpasien,
			COALESCE(mrekanan.nama, '-') AS namaperusahaan,
			mkelas.nama AS namakelas,
			mbed.nama AS namabed,
			mdokter.nama AS namadokter,
			trawatinap_tindakan_pembayaran.total AS totaltransaksi,
			trawatinap_tindakan_pembayaran.pembayaran AS totalpembayaran,
			trawatinap_tindakan_pembayaran.deposit AS totaldeposit,
			((trawatinap_tindakan_pembayaran.total - trawatinap_tindakan_pembayaran.pembayaran) - trawatinap_tindakan_pembayaran.deposit) * -1 AS totalrefund
		FROM
			trawatinap_deposit
			JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_deposit.idrawatinap AND trawatinap_deposit.status = 1
			JOIN mfpasien ON mfpasien.id = trawatinap_pendaftaran.idpasien
			JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
			LEFT JOIN mrekanan ON mrekanan.id = trawatinap_pendaftaran.idrekanan
			LEFT JOIN mkelas ON mkelas.id = trawatinap_pendaftaran.idkelas
			LEFT JOIN mbed ON mbed.id = trawatinap_pendaftaran.idbed
			JOIN mdokter ON mdokter.id = trawatinap_pendaftaran.iddokterpenanggungjawab
			JOIN trawatinap_tindakan_pembayaran ON trawatinap_tindakan_pembayaran.idtindakan = trawatinap_pendaftaran.id AND trawatinap_tindakan_pembayaran.statusbatal = 0
		WHERE
			trawatinap_pendaftaran.id = $id AND
			trawatinap_pendaftaran.statuspembayaran = 1 AND
			trawatinap_tindakan_pembayaran.deposit > (trawatinap_tindakan_pembayaran.total - trawatinap_tindakan_pembayaran.pembayaran)");

		return $query->row();
	}

	public function getRefundObat($tanggalawal = '', $tanggalakhir = '', $idtipetransaksi = '')
	{
		if ($tanggalawal != '' && $tanggalakhir != '') {
			if ($idtipetransaksi != '0') {
				$query = $this->db->query("SELECT * FROM
				(
				SELECT
					tpasien_pengembalian.id AS id,
					tpasien_pengembalian.nopengembalian AS notransaksi,
					tpasien_pengembalian.tanggal,
					mfpasien.id AS idpasien,
					mfpasien.no_medrec AS nomedrec,
					mfpasien.nama AS namapasien,
					CONCAT(tpasien_pengembalian.nopengembalian, ' ', '(', mfpasien.nama, ')') AS text,
					mpasien_kelompok.id AS idkelompokpasien,
					mpasien_kelompok.nama AS namakelompokpasien,
					mdokter.nama AS namadokter,
					( CASE WHEN tpasien_penjualan.asalrujukan = 1 THEN 1 WHEN tpasien_penjualan.asalrujukan = 2 THEN 1 END ) AS idtipetransaksi,
					( CASE WHEN tpasien_penjualan.asalrujukan = 1 THEN 'Poliklinik' WHEN tpasien_penjualan.asalrujukan = 2 THEN 'Instalasi Gawat Darurat (IGD)' END ) AS tipetransaksi,
					trefund.id AS strefund
				FROM
					tpasien_pengembalian
					JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan AND tpasien_pengembalian.status = 1
					JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpasien_penjualan.idtindakan
					JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
					JOIN mfpasien ON mfpasien.id = tpasien_pengembalian.idpasien
					JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
					JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
					LEFT JOIN mrekanan ON mrekanan.id = tpoliklinik_pendaftaran.idrekanan
					JOIN mdokter ON mdokter.id = tpoliklinik_pendaftaran.iddokter
					LEFT JOIN trefund ON trefund.tipe = 1 AND trefund.idtransaksi = tpasien_pengembalian.id AND trefund.status = 1
					JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 ) AND ( tkasir.status = 2 )
				WHERE
					tpasien_penjualan.asalrujukan IN (1,2)
				) AS result
				WHERE result.strefund IS NULL AND result.idtipetransaksi = $idtipetransaksi AND (DATE(result.tanggal) >= '$tanggalawal' AND DATE(result.tanggal) <= '$tanggalakhir')");
			} else {
				$query = $this->db->query("SELECT * FROM
				(
				SELECT
					tpasien_pengembalian.id AS id,
					tpasien_pengembalian.nopengembalian AS notransaksi,
					tpasien_pengembalian.tanggal,
					mfpasien.id AS idpasien,
					mfpasien.no_medrec AS nomedrec,
					mfpasien.nama AS namapasien,
					CONCAT(tpasien_pengembalian.nopengembalian, ' ', '(', mfpasien.nama, ')') AS text,
				mpasien_kelompok.id AS idkelompokpasien,
					mpasien_kelompok.nama AS namakelompokpasien,
					mdokter.nama AS namadokter,
					( CASE WHEN ( tpasien_penjualan.asalrujukan = 1 ) THEN 'Poliklinik' WHEN ( tpasien_penjualan.asalrujukan = 2 ) THEN 'Instalasi Gawat Darurat (IGD)' END ) AS tipetransaksi,
					trefund.id AS strefund
				FROM
					tpasien_pengembalian
					JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan AND tpasien_pengembalian.status = 1
					JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpasien_penjualan.idtindakan
					JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
					JOIN mfpasien ON mfpasien.id = tpasien_pengembalian.idpasien
					JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
					JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
					LEFT JOIN mrekanan ON mrekanan.id = tpoliklinik_pendaftaran.idrekanan
					JOIN mdokter ON mdokter.id = tpoliklinik_pendaftaran.iddokter
					LEFT JOIN trefund ON trefund.tipe = 1 AND trefund.idtransaksi = tpasien_pengembalian.id AND trefund.status = 1
					JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 ) AND ( tkasir.status = 2 )
				WHERE
					tpasien_penjualan.asalrujukan IN (1,2)
				) AS result
				WHERE result.strefund IS NULL AND (DATE(result.tanggal) >= '$tanggalawal' AND DATE(result.tanggal) <= '$tanggalakhir')");
			}
		} else {
			$query = $this->db->query("SELECT * FROM
			(
			SELECT
				tpasien_pengembalian.id AS id,
				tpasien_pengembalian.nopengembalian AS notransaksi,
				tpasien_pengembalian.tanggal,
				mfpasien.id AS idpasien,
				mfpasien.no_medrec AS nomedrec,
				mfpasien.nama AS namapasien,
				CONCAT(tpasien_pengembalian.nopengembalian, ' ', '(', mfpasien.nama, ')') AS text,
				mpasien_kelompok.id AS idkelompokpasien,
				mpasien_kelompok.nama AS namakelompokpasien,
				mdokter.nama AS namadokter,
				( CASE WHEN ( tpasien_penjualan.asalrujukan = 1 ) THEN 'Poliklinik' WHEN ( tpasien_penjualan.asalrujukan = 2 ) THEN 'Instalasi Gawat Darurat (IGD)' END ) AS tipetransaksi,
				trefund.id AS strefund
			FROM
				tpasien_pengembalian
				JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan AND tpasien_pengembalian.status = 1
				JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpasien_penjualan.idtindakan
				JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
				JOIN mfpasien ON mfpasien.id = tpasien_pengembalian.idpasien
				JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
				JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
				LEFT JOIN mrekanan ON mrekanan.id = tpoliklinik_pendaftaran.idrekanan
				JOIN mdokter ON mdokter.id = tpoliklinik_pendaftaran.iddokter
				LEFT JOIN trefund ON trefund.tipe = 1 AND trefund.idtransaksi = tpasien_pengembalian.id AND trefund.status = 1
				JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 ) AND ( tkasir.status = 2 )
			WHERE
				tpasien_penjualan.asalrujukan IN (1,2)
			) AS result
			WHERE result.strefund IS NULL");
		}

		return $query->result();
	}

	public function getInfoRefundObat($id)
	{
		$query = $this->db->query("SELECT
			'rawatjalan' AS reference,
			tpasien_pengembalian.id AS id,
			mfpasien.no_medrec AS nomedrec,
			mfpasien.nama AS namapasien,
			mfpasien.alamat_jalan AS alamatpasien,
			tpasien_pengembalian.tanggal AS tanggaltransaksi,
			( CASE WHEN ( tpasien_penjualan.asalrujukan = 1 ) THEN 'Poliklinik' WHEN ( tpasien_penjualan.asalrujukan = 2 ) THEN 'Instalasi Gawat Darurat (IGD)' END ) AS tipetransaksi,
			mpoliklinik.nama AS namapoliklinik,
			mpasien_kelompok.nama AS namakelompokpasien,
			COALESCE(mrekanan.nama, '-') AS namaperusahaan,
			mdokter.nama AS namadokter,
			SUM(tpasien_pengembalian.totalharga) AS totalretur
		FROM
			tpasien_pengembalian
			JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan AND tpasien_pengembalian.status = 1
			JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpasien_penjualan.idtindakan
			JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
			JOIN mfpasien ON mfpasien.id = tpasien_pengembalian.idpasien
			JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
			JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
			LEFT JOIN mrekanan ON mrekanan.id = tpoliklinik_pendaftaran.idrekanan
			JOIN mdokter ON mdokter.id = tpoliklinik_pendaftaran.iddokter
		WHERE
			tpasien_pengembalian.id = $id AND
			tpasien_penjualan.asalrujukan IN (1,2)");

		return $query->row();
	}

	public function getDetailRefundObat($id)
	{
		$query = $this->db->query("SELECT
			tpasien_pengembalian.id,
			tpasien_pengembalian.nopengembalian,
			tpasien_pengembalian.tanggal,
			(
				CASE
					WHEN tpasien_pengembalian_detail.idtipe = 1 THEN
						mdata_alkes.nama
					WHEN tpasien_pengembalian_detail.idtipe = 3 THEN
						mdata_obat.nama
				END
			) AS namatarif,
			tpasien_pengembalian_detail.harga,
			tpasien_pengembalian_detail.kuantitas,
			tpasien_pengembalian_detail.totalharga,
			munitpelayanan.nama AS namaunitpelayanan
		FROM
			tpasien_pengembalian_detail
		JOIN tpasien_pengembalian ON tpasien_pengembalian.id = tpasien_pengembalian_detail.idpengembalian AND tpasien_pengembalian.status = 1
		JOIN munitpelayanan ON munitpelayanan.id = tpasien_pengembalian_detail.idunit
		LEFT JOIN mdata_alkes ON mdata_alkes.id = tpasien_pengembalian_detail.idbarang AND tpasien_pengembalian_detail.idtipe = 1
		LEFT JOIN mdata_obat ON mdata_obat.id = tpasien_pengembalian_detail.idbarang AND tpasien_pengembalian_detail.idtipe = 3
		WHERE
			tpasien_pengembalian_detail.idpengembalian = $id");

		return $query->result();
	}

	public function getRefundTransaksi($tanggalawal = '', $tanggalakhir = '', $idtipetransaksi = '')
	{
		if ($tanggalawal != '' && $tanggalakhir != '') {
			if ($idtipetransaksi == '0') {
				$query = $this->db->query("SELECT
					'rawatinap' AS reference,
					view_refund_ranap.id AS id,
					view_refund_ranap.noregis AS notransaksi,
					view_refund_ranap.idpasien,
					view_refund_ranap.nomedrec,
					view_refund_ranap.nama AS namapasien,
					CONCAT( view_refund_ranap.noregis, ' ', '(', view_refund_ranap.nama, ')' ) AS text,
					view_refund_ranap.idkelompokpasien,
					view_refund_ranap.kelompokpasien AS namakelompokpasien,
					view_refund_ranap.namadokter AS namadokter,
					( CASE WHEN ( view_refund_ranap.tipe = 1 ) THEN 'Rawat Inap' WHEN ( view_refund_ranap.tipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi
				FROM
					view_refund_ranap
				WHERE view_refund_ranap.status IS NULL AND DATE(view_refund_ranap.tanggal) >= '$tanggalawal' AND DATE(view_refund_ranap.tanggal) <= '$tanggalakhir'

				UNION ALL

				SELECT
					'rawatjalan' AS reference,
					view_refund_rajal.id AS id,
					view_refund_rajal.noregis AS notransaksi,
					view_refund_rajal.idpasien,
					view_refund_rajal.nomedrec,
					view_refund_rajal.nama AS namapasien,
					CONCAT( view_refund_rajal.noregis, ' ', '(', view_refund_rajal.nama, ')' ) AS text,
					view_refund_rajal.idkelompokpasien,
					view_refund_rajal.kelompokpasien AS namakelompokpasien,
					view_refund_rajal.namadokter AS namadokter,
					'Rawat Jalan' AS tipetransaksi
				FROM
					view_refund_rajal
				WHERE view_refund_rajal.status IS NULL AND DATE(view_refund_rajal.tanggal) >= '$tanggalawal' AND DATE(view_refund_rajal.tanggal) <= '$tanggalakhir'");
			} elseif ($idtipetransaksi == '1') {
				$query = $this->db->query("SELECT
					'rawatjalan' AS reference,
					view_refund_rajal.id AS id,
					view_refund_rajal.noregis AS notransaksi,
					view_refund_rajal.idpasien,
					view_refund_rajal.nomedrec,
					view_refund_rajal.nama AS namapasien,
					CONCAT( view_refund_rajal.noregis, ' ', '(', view_refund_rajal.nama, ')' ) AS text,
					view_refund_rajal.idkelompokpasien,
					view_refund_rajal.kelompokpasien AS namakelompokpasien,
					view_refund_rajal.namadokter AS namadokter,
					'Rawat Jalan' AS tipetransaksi
				FROM
					view_refund_rajal
				WHERE view_refund_rajal.status IS NULL AND DATE(view_refund_rajal.tanggal) >= '$tanggalawal' AND DATE(view_refund_rajal.tanggal) <= '$tanggalakhir'");
			} elseif ($idtipetransaksi == '2') {
				$query = $this->db->query("SELECT
					'rawatinap' AS reference,
					view_refund_ranap.id AS id,
					view_refund_ranap.noregis AS notransaksi,
					view_refund_ranap.idpasien,
					view_refund_ranap.nomedrec,
					view_refund_ranap.nama AS namapasien,
					CONCAT( view_refund_ranap.noregis, ' ', '(', view_refund_ranap.nama, ')' ) AS text,
					view_refund_ranap.idkelompokpasien,
					view_refund_ranap.kelompokpasien AS namakelompokpasien,
					view_refund_ranap.namadokter AS namadokter,
					( CASE WHEN ( view_refund_ranap.tipe = 1 ) THEN 'Rawat Inap' WHEN ( view_refund_ranap.tipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi
				FROM
					view_refund_ranap
				WHERE view_refund_ranap.status IS NULL AND view_refund_ranap.tipe = 1 AND DATE(view_refund_ranap.tanggal) >= '$tanggalawal' AND DATE(view_refund_ranap.tanggal) <= '$tanggalakhir'");
			} elseif ($idtipetransaksi == '3') {
				$query = $this->db->query("SELECT
					'rawatinap' AS reference,
					view_refund_ranap.id AS id,
					view_refund_ranap.noregis AS notransaksi,
					view_refund_ranap.idpasien,
					view_refund_ranap.nomedrec,
					view_refund_ranap.nama AS namapasien,
					CONCAT( view_refund_ranap.noregis, ' ', '(', view_refund_ranap.nama, ')' ) AS text,
					view_refund_ranap.idkelompokpasien,
					view_refund_ranap.kelompokpasien AS namakelompokpasien,
					view_refund_ranap.namadokter AS namadokter,
					( CASE WHEN ( view_refund_ranap.tipe = 1 ) THEN 'Rawat Inap' WHEN ( view_refund_ranap.tipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi
				FROM
					view_refund_ranap
				WHERE view_refund_ranap.status IS NULL AND view_refund_ranap.tipe = 2 AND DATE(view_refund_ranap.tanggal) >= '$tanggalawal' AND DATE(view_refund_ranap.tanggal) <= '$tanggalakhir'");
			}
		} else {
			$query = $this->db->query("SELECT
				'rawatinap' AS reference,
				view_refund_ranap.id AS id,
				view_refund_ranap.noregis AS notransaksi,
				view_refund_ranap.idpasien,
				view_refund_ranap.nomedrec,
				view_refund_ranap.nama AS namapasien,
				CONCAT( view_refund_ranap.noregis, ' ', '(', view_refund_ranap.nama, ')' ) AS text,
				view_refund_ranap.idkelompokpasien,
				view_refund_ranap.kelompokpasien AS namakelompokpasien,
				view_refund_ranap.namadokter AS namadokter,
				( CASE WHEN ( view_refund_ranap.tipe = 1 ) THEN 'Rawat Inap' WHEN ( view_refund_ranap.tipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi
			FROM
				view_refund_ranap
			WHERE view_refund_ranap.status IS NULL

			UNION ALL

			SELECT
				'rawatjalan' AS reference,
				view_refund_rajal.id AS id,
				view_refund_rajal.noregis AS notransaksi,
				view_refund_rajal.idpasien,
				view_refund_rajal.nomedrec,
				view_refund_rajal.nama AS namapasien,
				CONCAT( view_refund_rajal.noregis, ' ', '(', view_refund_rajal.nama, ')' ) AS text,
				view_refund_rajal.idkelompokpasien,
				view_refund_rajal.kelompokpasien AS namakelompokpasien,
				view_refund_rajal.namadokter AS namadokter,
				'Rawat Jalan' AS tipetransaksi
			FROM
				view_refund_rajal
			WHERE view_refund_rajal.status IS NULL");
		}

		return $query->result();
	}

	public function getInfoRefundTransaksi($reference, $id)
	{
		$query = $this->db->query("SELECT * FROM
        (
			SELECT
				'rawatinap' AS reference,
				view_refund_ranap.id AS id,
				view_refund_ranap.nomedrec,
				view_refund_ranap.nama AS namapasien,
				view_refund_ranap.alamat AS alamatpasien,
				view_refund_ranap.tanggal AS tanggaltransaksi,
				( CASE WHEN ( view_refund_ranap.tipe = 1 ) THEN 'Rawat Inap' WHEN ( view_refund_ranap.tipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi,
				'' AS namapoliklinik,
				view_refund_ranap.kelompokpasien AS namakelompokpasien,
				view_refund_ranap.asuransi AS namaperusahaan,
				view_refund_ranap.kelas AS namakelas,
				view_refund_ranap.bed AS namabed,
				view_refund_ranap.namadokter
			FROM
				view_refund_ranap UNION ALL
			SELECT
				'rawatjalan' AS reference,
				view_refund_rajal.id AS id,
				view_refund_rajal.nomedrec,
				view_refund_rajal.nama AS namapasien,
				view_refund_rajal.alamat AS alamatpasien,
				view_refund_rajal.tanggal AS tanggaltransaksi,
				'Rawat Jalan' AS tipetransaksi,
				view_refund_rajal.namapoliklinik AS namapoliklinik,
				view_refund_rajal.kelompokpasien AS namakelompokpasien,
				view_refund_rajal.asuransi AS namaperusahaan,
				'' AS namakelas,
				'' AS namabed,
				view_refund_rajal.namadokter
			FROM
				view_refund_rajal
		) AS result
		WHERE result.reference = '$reference' AND result.id = $id");

		return $query->row();
	}

	// public function getInfoPendaftaran($idtindakan)
	// {
	// 	$this->db->select('tpoliklinik_pendaftaran.nopendaftaran, tpoliklinik_pendaftaran.tanggaldaftar AS tanggal');
	// 	$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran');
	// 	$this->db->where('tpoliklinik_tindakan.id', $idtindakan);
	// 	$query = $this->db->get('tpoliklinik_tindakan');

	// 	return $query->row();
	// }

	// public function getInfoPendaftaranById($idpendaftaran)
	// {
	// 	$this->db->select('nopendaftaran, tanggaldaftar AS tanggal');
	// 	$this->db->where('id', $idpendaftaran);
	// 	$query = $this->db->get('tpoliklinik_pendaftaran');

	// 	return $query->row();
	// }

	public function getListFarmasi($id)
	{
		$this->db->select('*');
		$this->db->where('idpoliklinik', $id);
		$query = $this->db->get('view_rincian_farmasi_rajal');
		return $query->result();
	}

	// public function getListLaboratorium($id)
	// {
	// 	$this->db->select('*');
	// 	$this->db->where('idpoliklinik', $id);
	// 	$this->db->where('statusrincianpaket', '0');
	// 	$query = $this->db->get('view_rincian_laboratorium_rajal');
	// 	return $query->result();
	// }

	// public function getListRadiologi($id)
	// {
	// 	$this->db->select('*');
	// 	$this->db->where('idpoliklinik', $id);
	// 	$query = $this->db->get('view_rincian_radiologi_rajal');
	// 	return $query->result();
	// }

	// public function getListFisioterapi($id)
	// {
	// 	$this->db->select('*');
	// 	$this->db->where('idpoliklinik', $id);
	// 	$query = $this->db->get('view_rincian_fisioterapi_rajal');
	// 	return $query->result();
	// }

	// public function getListAdministrasi($id)
	// {
	// 	$this->db->select('*');
	// 	$this->db->where('idpoliklinik', $id);
	// 	$query = $this->db->get('view_rincian_administrasi_rajal');
	// 	return $query->result();
	// }
}
