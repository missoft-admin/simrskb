<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tlaba_rugi_setting_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function list_user_akses($id=''){
		$q="SELECT M.id,M.`name`,CASE WHEN S.iduser THEN 'selected' ELSE '' END as pilih 
		FROM musers M
		LEFT JOIN tlaba_rugi_setting_user S ON S.iduser=M.id AND S.template_id='$id'
		WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
    public function getSpecified($id)
    {
        $q="SELECT 
			*
			from tlaba_rugi_setting M
					
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	public function saveData()
    {
        $this->nama 	= $_POST['nama'];		
       
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		$this->status  = 1;
		$hasil=$this->db->insert('tlaba_rugi_setting', $this);
        if ($hasil) {
			$id= $this->db->insert_id();
			$iduser 	= $this->input->post('iduser');		
			if ($iduser){				
				foreach($iduser as $index=>$val){
					$data_user=array(
						'template_id' => $id,
						'iduser' => $val,
					);
					$this->db->insert('tlaba_rugi_setting_user',$data_user);
				}
			}
            return $id;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
		$id=$this->input->post('id');
		$this->nama 	= $_POST['nama'];		
		$this->edited_by 	= $this->session->userdata('user_id');		
		$this->edited_date 	= date('Y-m-d H:i:s');		
		
        if ($this->db->update('tlaba_rugi_setting', $this, array('id' => $id))) {
			$iduser 	= $this->input->post('iduser');	
			$this->db->delete('tlaba_rugi_setting_user',array('template_id'=>$id));
			if ($iduser){				
				foreach($iduser as $index=>$val){
					$data_user=array(
						'template_id' => $id,
						'iduser' => $val,
					);
					$this->db->insert('tlaba_rugi_setting_user',$data_user);
				}
			}
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	
    public function softDelete($id)
    {
        $this->status = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('tlaba_rugi_setting', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;		
		
        if ($this->db->update('tlaba_rugi_setting', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	//BATAS DELETE
	

}
