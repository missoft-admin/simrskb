<?php

declare(strict_types=1);

class Mpengaturan_penginputan_ekpertise_radiologi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getListSetting()
    {
        $this->db->select('merm_pengaturan_penginputan_ekpertise_radiologi.*,
            merm_pemeriksaan_radiologi.nama_pemeriksaan,
            (CASE
                WHEN merm_pengaturan_penginputan_ekpertise_radiologi.asal_pasien IN (1, 2) THEN
                    mpoliklinik.nama
                ELSE
                    mkelas.nama
            END) AS poliklinik_kelas_label,
            mdokter.nama AS nama_dokter');
        $this->db->join('merm_pemeriksaan_radiologi', 'merm_pemeriksaan_radiologi.tarif_radiologi_id = merm_pengaturan_penginputan_ekpertise_radiologi.pemeriksaan_id', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = merm_pengaturan_penginputan_ekpertise_radiologi.poliklinik_kelas AND merm_pengaturan_penginputan_ekpertise_radiologi.asal_pasien IN (1, 2)', 'LEFT');
        $this->db->join('mkelas', 'mkelas.id = merm_pengaturan_penginputan_ekpertise_radiologi.poliklinik_kelas AND merm_pengaturan_penginputan_ekpertise_radiologi.asal_pasien IN (3, 4)', 'LEFT');
        $this->db->join('mdokter', 'mdokter.id = merm_pengaturan_penginputan_ekpertise_radiologi.dokter_radiologi', 'LEFT');
        $this->db->where('merm_pengaturan_penginputan_ekpertise_radiologi.status', '1');
        $query = $this->db->get('merm_pengaturan_penginputan_ekpertise_radiologi');

        return $query->result();
    }

    public function saveData()
    {
        $this->tipe_layanan = $this->input->post('tipe_layanan');
        $this->pemeriksaan_id = $this->input->post('pemeriksaan_id');
        $this->asal_pasien = $this->input->post('asal_pasien');
        $this->poliklinik_kelas = $this->input->post('poliklinik_kelas');
        $this->dokter_radiologi = $this->input->post('dokter_radiologi');
        $this->klinis = $this->input->post('klinis');
        $this->kesan = $this->input->post('kesan');
        $this->usul = $this->input->post('usul');
        $this->hasil = $this->input->post('hasil');
        $this->created_by = $this->session->userdata('user_id');
        $this->created_date = date('Y-m-d H:i:s');

        if ($this->db->insert('merm_pengaturan_penginputan_ekpertise_radiologi', $this)) {
            return true;
        } else {
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
        $this->deleted_by = $this->session->userdata('user_id');
        $this->deleted_date = date('Y-m-d H:i:s');

        if ($this->db->update('merm_pengaturan_penginputan_ekpertise_radiologi', $this, ['id' => $id])) {
            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;
    }
}
