<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
class Tews_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	function get_data_ews($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='1'){
			$q="SELECT *
			 FROM tpoliklinik_ews 
			 WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND st_ranap='1' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}else{
			$q="SELECT *
			 FROM tpoliklinik_ews 
			 WHERE pendaftaran_id='$pendaftaran_id_ranap' AND st_ranap='0' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function list_template_ews(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_ews H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function get_data_ews_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_ews WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_ews_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_ews_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_ews H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_ews_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_input as tanggal_input_asal,tanggal_ews as tanggal_ews_asal
				,ttd_hubungan as ttd_hubungan_asal,jenis_ttd as jenis_ttd_asal,total_skor_ews as total_skor_ews_asal,nama_kajian as nama_kajian_asal,isi_header as isi_header_asal,isi_footer as isi_footer_asal,nilai_tertimbang as nilai_tertimbang_asal,nilai_satuan as nilai_satuan_asal,nama_profile as nama_profile_asal,jk_profile as jk_profile_asal,mengetahui_rs as mengetahui_rs_asal,ttl_profile as ttl_profile_asal,pendidikan_profile as pendidikan_profile_asal,lainnya_profile as lainnya_profile_asal,umur_profile as umur_profile_asal,pekerjaan_profile as pekerjaan_profile_asal,tanggal_ews as tanggal_ews_asal,kritik as kritik_asal,hasil_penilaian as hasil_penilaian_asal,nilai_per_unsur as nilai_per_unsur_asal,jumlah_jawaban as jumlah_jawaban_asal,nrr_per_unsur as nrr_per_unsur_asal,nrr_tertimbang as nrr_tertimbang_asal,kepuasan_mas as kepuasan_mas_asal,flag_nilai_mas as flag_nilai_mas_asal,hasil_flag as hasil_flag_asal
				FROM tpoliklinik_ews_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function logic_akses_ews($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_ews,H.st_lihat as st_lihat_ews,H.st_edit as st_edit_ews,H.st_hapus as st_hapus_ews,H.st_cetak as st_cetak_ews
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_ews_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_ews_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_ews_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_ews'=>'0',
				'st_input_ews'=>'0',
				'st_edit_ews'=>'0',
				'st_hapus_ews'=>'0',
				'st_cetak_ews'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_ews(){
		$q="SELECT H.judul_header_ina,H.judul_footer_ina,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.judul_header_eng,H.judul_footer_eng
		FROM setting_ews H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function setting_ews_label(){
		$q="SELECT 
		logo,alamat_rs,phone_rs,web_rs,label_identitas_ina,pendidikan_profile_ina
		,mengetahui_lain_ina,mengetahui_lain_eng
		,pendidikan_profile_eng,label_identitas_eng,no_reg_ina,no_reg_eng,no_rm_ina,no_rm_eng,nama_pasien_ina,nama_pasien_eng,ttl_ina,ttl_eng,umur_ina,umur_eng,jk_ina,jk_eng,asal_pasien_ina,asal_pasien_eng,detail_ina,detail_eng,dokter_ina,dokter_eng,profile_ina,profile_eng,profile_nama_ina,profile_nama_eng,profile_nama_ina_ina,profile_nama_ina_eng,ttl_profile_ina,ttl_profile_eng,umur_profile_ina,umur_profile_eng,jk_profile_ina,jk_profile_eng,pekerjaan_profile_ina,pekerjaan_profile_eng,mengetahui_rs_ina,mengetahui_rs_eng,waktu_ews_ina,waktu_ews_eng,nama_ews_ina,nama_ews_eng,total_nilai_ina,total_nilai_eng,nilai_perunsur_ina,nilai_perunsur_eng,nrr_perunsur_ina,nrr_perunsur_eng,nrr_tertimbang_ina,nrr_tertimbang_eng,satuan_kepuasan_ina,satuan_kepuasan_eng,hasil_ews_ina,hasil_ews_eng,pertanyaan_ina,pertanyaan_eng,jawaban_ina,jawaban_eng,total_penilaian_ina,total_penilaian_eng,kritik_ina,kritik_eng,responden_ina,responden_eng,notes_footer_ina,notes_footer_eng
		FROM setting_ews_label H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function get_data_pews($pendaftaran_id_ranap,$st_ranap='1'){
		if ($st_ranap=='1'){
			$q="SELECT *
			 FROM tranap_pews WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND st_ranap='$st_ranap' 
			 AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}else{
			$q="SELECT *
			 FROM tranap_pews WHERE pendaftaran_id='$pendaftaran_id_ranap' AND st_ranap='$st_ranap' 
			 AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function setting_pews(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			FROM `setting_pews` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function list_template_pews(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_pews H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	function list_template_assesmen_lokasi_operasi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_lokasi_operasi H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function get_data_pews_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_pews WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_pews_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_pews_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_pews H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_pews_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				mpews_id as mpews_id_asal,tanggal_pengkajian as tanggal_pengkajian_asal,ref_nilai_id as ref_nilai_id_asal,total_skor_pews as total_skor_pews_asal,nama_hasil_pengkajian as nama_hasil_pengkajian_asal,nama_tindakan as nama_tindakan_asal
				FROM tranap_pews_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
}
