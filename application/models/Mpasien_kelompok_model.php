<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpasien_kelompok_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mpasien_kelompok');
        return $query->row();
    }

    public function getJenisOperasi($id)
    {
        $this->db->select('idjenisoperasi');
        $this->db->where('idkelompokpasien', $id);
        $query = $this->db->get('mpasien_kelompok_operasi');

        $result = $query->result();

        $data = "";
        $numItems = count($result);
        $i = 0;
        foreach ($query->result() as $row) {
            $data .= $row->idjenisoperasi;
            if (++$i != $numItems) {
                $data .= ',';
            }
        }

        return explode(',', $data);
    }

    public function updateData()
    {
        $this->tadm_rawatjalan		        = $_POST['tadm_rawatjalan'];
        $this->tkartu_rawatjalan		      = $_POST['tkartu_rawatjalan'];
        $this->tadm_rawatinap		          = $_POST['tadm_rawatinap'];
        $this->tkartu_rawatinap		        = $_POST['tkartu_rawatinap'];
        $this->toperasi_sewaalat					= $_POST['toperasi_sewaalat'];
        $this->toperasi_tindakan					= $_POST['toperasi_tindakan'];
        $this->tradiologi_xray						= $_POST['tradiologi_xray'];
        $this->tradiologi_usg							= $_POST['tradiologi_usg'];
        $this->tradiologi_ctscan					= $_POST['tradiologi_ctscan'];
        $this->tradiologi_mri							= $_POST['tradiologi_mri'];
        $this->tradiologi_bmd							= $_POST['tradiologi_bmd'];
        $this->tlaboratorium_umum					= $_POST['tlaboratorium_umum'];
        $this->tlaboratorium_pa						= $_POST['tlaboratorium_pa'];
        $this->tlaboratorium_pmi				  = $_POST['tlaboratorium_pmi'];
        $this->tfisioterapi				        = $_POST['tfisioterapi'];
        $this->tigd						    = $_POST['tigd'];
        $this->trawatjalan						    = $_POST['trawatjalan'];
        $this->trawatinap_fullcare1				= $_POST['trawatinap_fullcare1'];
        $this->trawatinap_ecg1						= $_POST['trawatinap_ecg1'];
        $this->trawatinap_visite1					= $_POST['trawatinap_visite1'];
        $this->trawatinap_visite1_spesial					= $_POST['trawatinap_visite1_spesial'];
        $this->trawatinap_sewaalat1				= $_POST['trawatinap_sewaalat1'];
        $this->trawatinap_ambulance1			= $_POST['trawatinap_ambulance1'];
        $this->trawatinap_fullcare2				= $_POST['trawatinap_fullcare2'];
        $this->trawatinap_ecg2						= $_POST['trawatinap_ecg2'];
        $this->trawatinap_visite2					= $_POST['trawatinap_visite2'];
        $this->trawatinap_visite2_spesial					= $_POST['trawatinap_visite2_spesial'];
        $this->trawatinap_sewaalat2				= $_POST['trawatinap_sewaalat2'];
        $this->trawatinap_ambulance2			= $_POST['trawatinap_ambulance2'];
        $this->trawatinap_fullcare3				= $_POST['trawatinap_fullcare3'];
        $this->trawatinap_ecg3						= $_POST['trawatinap_ecg3'];
        $this->trawatinap_visite3					= $_POST['trawatinap_visite3'];
        $this->trawatinap_visite3_spesial					= $_POST['trawatinap_visite3_spesial'];
        $this->trawatinap_sewaalat3				= $_POST['trawatinap_sewaalat3'];
        $this->trawatinap_ambulance3			= $_POST['trawatinap_ambulance3'];
        $this->trawatinap_fullcare4				= $_POST['trawatinap_fullcare4'];
        $this->trawatinap_ecg4						= $_POST['trawatinap_ecg4'];
        $this->trawatinap_visite4					= $_POST['trawatinap_visite4'];
        $this->trawatinap_visite4_spesial					= $_POST['trawatinap_visite4_spesial'];
        $this->trawatinap_sewaalat4				= $_POST['trawatinap_sewaalat4'];
        $this->trawatinap_ambulance4			= $_POST['trawatinap_ambulance4'];
        $this->truang_perawatan			      = $_POST['truang_perawatan'];
        $this->trawatinap_fullcare5			      = $_POST['trawatinap_fullcare5'];
        $this->truang_hcu			            = $_POST['truang_hcu'];
        $this->truang_icu			            = $_POST['truang_icu'];
        $this->truang_isolasi			        = $_POST['truang_isolasi'];

    		$this->edited_by  = $this->session->userdata('user_id');
    		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mpasien_kelompok', $this, array('id' => $_POST['id']))) {

            $idkelompokpasien = $_POST['id'];
            $this->db->where('idkelompokpasien', $idkelompokpasien);
            if ($this->db->delete('mpasien_kelompok_operasi')) {
              foreach ($_POST['toperasi_jenis'] as $idjenisoperasi) {
                $data = array();
                $data['idkelompokpasien'] = $idkelompokpasien;
                $data['idjenisoperasi'] = $idjenisoperasi;
                $this->db->insert('mpasien_kelompok_operasi', $data);
              }
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
