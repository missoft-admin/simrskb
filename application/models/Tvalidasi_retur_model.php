<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tvalidasi_retur_model extends CI_Model
{
   public function list_distributor(){
	   $q="SELECT id,nama from mdistributor 
				WHERE mdistributor.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getHeader($id){
	   $q="SELECT * from tvalidasi_retur 
				WHERE tvalidasi_retur.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function saveData(){
	   // print_r($this->input->post());exit();
		$id=($this->input->post('id'));
		$st_posting=($this->input->post('st_posting'));
		$btn_simpan=($this->input->post('btn_simpan'));
		
		$iddet=($this->input->post('iddet'));
		$idakun_beli=($this->input->post('idakun_beli'));
		$idakun_ppn=($this->input->post('idakun_ppn'));
		$idakun_diskon=($this->input->post('idakun_diskon'));
		foreach($iddet as $index => $val){
			$detail=array(
				'idakun_beli'=>$idakun_beli[$index],
				'idakun_ppn'=>$idakun_ppn[$index],
				'idakun_diskon'=>$idakun_diskon[$index],
				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_retur_barang',$detail);
		}
		
		$iddet_ganti=($this->input->post('iddet_ganti'));
		$idakun_beli_ganti=($this->input->post('idakun_beli_ganti'));
		$idakun_ppn_ganti=($this->input->post('idakun_ppn_ganti'));
		$idakun_diskon_ganti=($this->input->post('idakun_diskon_ganti'));
		foreach($iddet_ganti as $index => $val){
			$detail=array(
				'idakun_beli'=>$idakun_beli_ganti[$index],
				'idakun_ppn'=>$idakun_ppn_ganti[$index],
				'idakun_diskon'=>$idakun_diskon_ganti[$index],
				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_retur_ganti',$detail);
		}
		
		$iddet_bayar=($this->input->post('iddet_bayar'));
		$idakun_bayar=($this->input->post('idakun_bayar'));
		$idakun_ppn_ganti=($this->input->post('idakun_ppn_ganti'));
		$idakun_diskon_ganti=($this->input->post('idakun_diskon_ganti'));
		foreach($iddet_bayar as $index => $val){
			$detail=array(
				'idakun'=>$idakun_bayar[$index],
				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_retur_bayar',$detail);
		}
		
		if ($st_posting=='1'){
			$this->db->where('id',$id);
			$this->db->update('tvalidasi_retur',array('st_posting'=>0));
		}
		if ($btn_simpan=='2'){
			$data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $id);
			$result=$this->db->update('tvalidasi_retur', $data);
		}
		
		return true;
   }
   function get_nama_akun($id){
	   $q="SELECT *from makun_nomor A WHERE A.id='$id'";
	   return $this->db->query($q)->row();
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
