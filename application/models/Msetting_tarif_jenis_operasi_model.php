<?php

defined('BASEPATH') || exit('No direct script access allowed');

class Msetting_tarif_jenis_operasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function get_detail($idkelompokpasien,$idrekanan='0'){
		$nama_kelompok='';
		$nama_rekanan='';
		$q="SELECT *FROM mpasien_kelompok H WHERE H.id='$idkelompokpasien'";
		$nama_kelompok=$this->db->query($q)->row('nama');
		if ($idkelompokpasien==1){
			$q="SELECT *FROM mrekanan H WHERE H.id='$idrekanan'";
			// print_r($q);exit;
			$nama_rekanan=$this->db->query($q)->row('nama');
		}else{
			$nama_rekanan='';
		}
		
		$data=array(
			'idkelompokpasien' => $idkelompokpasien,
			'idrekanan' => $idrekanan,
			'nama_kelompok' => $nama_kelompok,
			'nama_rekanan' => $nama_rekanan,
		);
		return $data;
	}
	function get_tarif_kelompok_pasien($idkelompokpasien,$idrekanan){
		if ($idrekanan==0){
			$q="SELECT M.id,M.nama 
				FROM mpasien_kelompok_operasi H 
				INNER JOIN mjenis_operasi M ON M.id=H.idjenisoperasi
				WHERE H.idkelompokpasien='$idkelompokpasien' AND M.`status`='1'";
		}else{
			$q="SELECT M.id,M.nama 
			FROM mrekanan_operasi H 
			INNER JOIN mjenis_operasi M ON M.id=H.idjenisoperasi
			WHERE H.idrekanan='$idrekanan' AND M.`status`='1'";
		}
		return $this->db->query($q)->result();
	}
}
