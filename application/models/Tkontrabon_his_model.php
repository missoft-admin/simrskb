<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tkontrabon_his_model extends CI_Model
{
   public function list_distributor(){
	   $q="SELECT H.iddistributor as id,M.nama,M.alamat,M.telepon from tkontrabon H
		LEFT JOIN tkontrabon_info I ON I.id=H.kontrabon_info_id 
		LEFT JOIN mdistributor M ON M.id=H.iddistributor
		WHERE I.`status` > 3
		GROUP BY H.iddistributor ";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function get_distributor($iddistributor){
	   $q="SELECT M.id as iddistributor,M.nama,M.alamat,M.telepon from mdistributor M
			WHERE M.id='$iddistributor'";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function load_left($iddistributor,$tahun){
	   $where='';
	   if ($tahun !='#'){
		   $where .=" AND YEAR(H.tanggalkontrabon)='$tahun'";
	   }
	   $q="SELECT H.tanggalkontrabon,DATE_FORMAT(H.tanggalkontrabon,'%Y') as y,DATE_FORMAT(H.tanggalkontrabon,'%m') as m,DATE_FORMAT(H.tanggalkontrabon,'%Y%m') as ym from tkontrabon H
			WHERE H.iddistributor='$iddistributor' ".$where."
			GROUP BY DATE_FORMAT(H.tanggalkontrabon,'%y%m')";
		// print_r($q);exit();
		$query=$this->db->query($q);
		return $query->result();
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
