<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tpiutang_history_model extends CI_Model
{
  
   public function get_header($tipepegawai,$id){
	   if ($tipepegawai=='1'){
		   $q="SELECT '1' as tipepegawai,M.id,M.nip,M.nama FROM mpegawai M WHERE M.id='$id'";
	   }else{
		   $q="SELECT '2' as tipepegawai,M.id,M.nip,M.nama FROM mdokter M WHERE M.id='$id'";
	   }
	   return $this->db->query($q)->row_array();
   }
   public function js_pegawai_dokter(){
		$tipe=$this->input->post('tipe');
		$search=$this->input->post('search');
		if ($tipe=='1'){//PEgawai
			$q="SELECT M.id,M.nama from mpegawai M
			WHERE nama LIKE '%".$search."%' AND M.status='1'";
		}else{
			$q="SELECT M.id,M.nama from mdokter M
			WHERE nama LIKE '%".$search."%' AND M.status='1'";
			
		}
		// print_r($q);exit();
		$query=$this->db->query($q);
		$result= $query->result();		
		return $result;			
	}
	public function load_left($tipepegawai,$id_peg,$tahun){
	   $where='';
	   $group='';
	   if ($tahun !='#'){
		   $where .=" AND YEAR(H.tanggal_tagihan)='$tahun'";
	   }
	   if ($tipepegawai =='1'){
		   $where .=" AND H.idpegawai='$id_peg'";
		   
	   }else{
		  $where .=" AND H.iddokter='$id_peg'"; 
	   }
	   $q="SELECT H.tanggal_tagihan,DATE_FORMAT(H.tanggal_tagihan,'%Y') as y,DATE_FORMAT(H.tanggal_tagihan,'%m') as m,DATE_FORMAT(H.tanggal_tagihan,'%Y%m') as ym 
			from tpiutang_detail H
			WHERE H.tipepegawai='$tipepegawai' ".$where."
			GROUP BY DATE_FORMAT(H.tanggal_tagihan,'%y%m')";
		// print_r($q);exit();
		$query=$this->db->query($q);
		return $query->result();
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
