<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpengelolaan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $q="SELECT 
			*
			from tpengelolaan M
					
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	public function list_akun(){
	   $q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		return $list;
   }
   
	function insert_validasi_pengelolaan($id){
		$q="SELECT H.id as idtransaksi,DATE(NOW()) as tanggal_transaksi,H.tanggal_hitung,H.notransaksi,H.idpengelolaan 
			,H.nama_pengelolaan,H.deskripsi_pengelolaan,H.st_harga_jual,H.nominal,S.st_auto_posting
			from tpengelolaan H
			LEFT JOIN msetting_jurnal_pengelolaan S ON S.id='1'
			WHERE H.id='$id'";
		$row=$this->db->query($q)->row();
		$st_auto_posting=$row->st_auto_posting;
		$data_header=array(
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'tanggal_hitung' => $row->tanggal_hitung,
			'notransaksi' => $row->notransaksi,
			'idpengelolaan' => $row->idpengelolaan,
			'nama_pengelolaan' => $row->nama_pengelolaan,
			'deskripsi_pengelolaan' => $row->deskripsi_pengelolaan,
			'st_harga_jual' => $row->st_harga_jual,
			'nominal' => $row->nominal,
			'st_auto_posting' => $row->st_auto_posting,
			'created_by' => $this->session->userdata('user_id'),
			'created_nama' => $this->session->userdata('user_name'),
			'created_date' => date('Y-m-d H:i:s'),
		);
		$this->db->insert('tvalidasi_pengelolaan',$data_header);
		$idvalidasi=$this->db->insert_id();
		$q_bayar="SELECT 
			H.id as idbayar_id,H.jenis_kas_id,J.nama as jenis_kas_nama
			,H.sumber_kas_id,S.nama as sumber_nama
			,S.bank_id as bankid,mbank.nama as bank
			,H.idmetode,RM.metode_bayar as metode_nama,H.nominal_bayar, S.idakun,'K' as posisi_akun,'0' as jenis_id,'PEMBAYARAN' as jenis_nama
			FROM `tpengelolaan_bayar` H
			LEFT JOIN mjenis_kas J ON J.id=H.jenis_kas_id
			LEFT JOIN msumber_kas S ON S.id=H.sumber_kas_id
			LEFT JOIN ref_metode RM ON RM.id=H.idmetode
			LEFT JOIN mbank ON mbank.id=S.bank_id
			WHERE H.tpengelolaan_id='$id'
			UNION ALL
			SELECT 
			H.id as idbayar_id,H.jenis_kas_id,J.nama as jenis_kas_nama
			,H.sumber_kas_id,S.nama as sumber_nama
			,S.bank_id as bankid,mbank.nama as bank
			,H.idmetode,RM.metode_bayar as metode_nama,H.nominal_bayar, S.idakun,'D' as posisi_akun,'0' as jenis_id,'TERIMA PEMBAYARAN' as jenis_nama
			FROM `tpengelolaan_bayar_terima` H
			LEFT JOIN mjenis_kas J ON J.id=H.jenis_kas_id
			LEFT JOIN msumber_kas S ON S.id=H.sumber_kas_id
			LEFT JOIN ref_metode RM ON RM.id=H.idmetode
			LEFT JOIN mbank ON mbank.id=S.bank_id
			WHERE H.tpengelolaan_id='$id'";
		$rows=$this->db->query($q_bayar)->result();
		foreach($rows as $r){
			$data_bayar=array(
				'idvalidasi' =>$idvalidasi,
				'idbayar_id' =>$r->idbayar_id,
				'jenis_kas_id' =>$r->jenis_kas_id,
				'jenis_kas_nama' =>$r->jenis_kas_nama,
				'sumber_kas_id' =>$r->sumber_kas_id,
				'sumber_nama' =>$r->sumber_nama,
				'bank' =>$r->bank,
				'bankid' =>$r->bankid,
				'idmetode' =>$r->idmetode,
				'metode_nama' =>$r->metode_nama,
				'nominal_bayar' =>$r->nominal_bayar,
				'idakun' =>$r->idakun,
				'posisi_akun' =>$r->posisi_akun,
				'jenis_id' =>$r->jenis_id,
				'jenis_nama' =>$r->jenis_nama,

			);
			$this->db->insert('tvalidasi_pengelolaan_bayar',$data_bayar);
		}
		
		$q="SELECT 
			H.id as idtransaksi,H.mdata_hutang_id,H.nama_transaksi,H.tanggal_transaksi,H.nominal,H.kuantitas
			,H.totalkeseluruhan,H.keterangan,'1' as jenis_id,'HUTANG' as jenis_nama,MH.idakun as idakun,'D' as posisi_akun
			FROM tpengelolaan_hutang H
			LEFT JOIN mdata_hutang MH ON MH.id=H.mdata_hutang_id
			WHERE H.tpengelolaan_id='$id'";//HUTANG
		$rows=$this->db->query($q)->result();
		foreach($rows as $row){
			$data_detail=array(
				'idvalidasi' =>$idvalidasi,
				'idtransaksi' => $row->idtransaksi,
				'mdata_hutang_id' => $row->mdata_hutang_id,
				'nama_transaksi' => $row->nama_transaksi,
				'tanggal_transaksi' => $row->tanggal_transaksi,
				'nominal' => $row->nominal,
				'kuantitas' => $row->kuantitas,
				'totalkeseluruhan' => $row->totalkeseluruhan,
				'keterangan' => $row->keterangan,
				'jenis_id' => $row->jenis_id,
				'jenis_nama' => $row->jenis_nama,
				'idakun' => $row->idakun,
				'posisi_akun' => $row->posisi_akun,
			);
			$this->db->insert('tvalidasi_pengelolaan_hutang',$data_detail);
		}
		$q="SELECT 
			H.id as idtransaksi,H.mdata_piutang_id,H.nama_transaksi,H.tanggal_transaksi,H.nominal,H.kuantitas
			,H.totalkeseluruhan,H.keterangan,'2' as jenis_id,'PIUTANG' as jenis_nama,MH.idakun as idakun,'K' as posisi_akun
			FROM tpengelolaan_piutang H
			LEFT JOIN mdata_piutang MH ON MH.id=H.mdata_piutang_id
			WHERE H.tpengelolaan_id='$id'";//PIUTANG
		$rows=$this->db->query($q)->result();
		foreach($rows as $row){
			$data_detail=array(
				'idvalidasi' =>$idvalidasi,
				'idtransaksi' => $row->idtransaksi,
				'mdata_piutang_id' => $row->mdata_piutang_id,
				'nama_transaksi' => $row->nama_transaksi,
				'tanggal_transaksi' => $row->tanggal_transaksi,
				'nominal' => $row->nominal,
				'kuantitas' => $row->kuantitas,
				'totalkeseluruhan' => $row->totalkeseluruhan,
				'keterangan' => $row->keterangan,
				'jenis_id' => $row->jenis_id,
				'jenis_nama' => $row->jenis_nama,
				'idakun' => $row->idakun,
				'posisi_akun' => $row->posisi_akun,
			);
			$this->db->insert('tvalidasi_pengelolaan_piutang',$data_detail);
		}
		$q="SELECT H.id as idtransaksi,H.tpengelolaan_id,H.tbendahara_pendapatan_id,H.notransaksi,H.nama_transaksi 
			,H.tanggal,H.idpendapatan,H.keterangan,H.terimadari,H.terimadari_nama,H.nominal,'1' as jenis_id,'PENDAPATAN LAIN-LAIN' as jenis_nama
			,M.idakun,'D' as posisi_akun
			FROM tpengelolaan_pendapatan H
			LEFT JOIN mpendapatan M ON M.id=H.idpendapatan
			WHERE H.tpengelolaan_id='$id'";//HUTANG PENDAPTAN LAIN LAIN
		$rows=$this->db->query($q)->result();
		foreach($rows as $row){
			$data_detail=array(
				'idvalidasi' =>$idvalidasi,
				'idtransaksi' => $row->idtransaksi,
				'tpengelolaan_id' => $row->tpengelolaan_id,
				'tbendahara_pendapatan_id' => $row->tbendahara_pendapatan_id,
				'notransaksi' => $row->notransaksi,
				'nama_transaksi' => $row->nama_transaksi,
				'tanggal' => $row->tanggal,
				'idpendapatan' => $row->idpendapatan,
				'keterangan' => $row->keterangan,
				'terimadari' => $row->terimadari,
				'terimadari_nama' => $row->terimadari_nama,
				'nominal' => $row->nominal,
				'jenis_id' => $row->jenis_id,
				'jenis_nama' => $row->jenis_nama,
				'idakun' => $row->idakun,
				'posisi_akun' => $row->posisi_akun,
			);
			$this->db->insert('tvalidasi_pengelolaan_pendapatan',$data_detail);
		}
		$q="SELECT H.id as idtransaksi,H.tpermintaan_id as idpermintaan 
			,D.id as idet,D.idtipe,D.idbarang,B.idkategori,B.nama as nama_barang
			,HH.st_harga_jual,CASE WHEN HH.st_harga_jual='1' THEN B.hargadasar ELSE (B.hargadasar)+(B.hargadasar*B.marginumum/100) END as harga
			,D.kuantitaskirim as kuantitas
			,compare_value(MAX(IF(SB.idbarang = D.idbarang,SB.idakun,NULL)),
													MAX(IF(SB.idbarang = 0 AND SB.idkategori = B.idkategori,SB.idakun,NULL)),
												  MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe = D.idtipe ,SB.idakun,NULL))) idakun
			,'K' as posisi_akun,'2' as jenis_id,'PERMINTAAN BARANG' as jenis_nama
			,H.notransaksi as nopermintaan
			FROM tpengelolaan_permintaan H
			LEFT JOIN tpengelolaan HH ON HH.id=H.tpengelolaan_id
			LEFT JOIN tunit_permintaan_detail D ON D.idpermintaan=H.tpermintaan_id
			LEFT JOIN view_barang_all B ON B.id=D.idbarang AND B.idtipe=D.idtipe
			LEFT JOIN msetting_jurnal_pengelolaan_barang SB ON SB.idtipe=D.idtipe
			WHERE H.tpengelolaan_id='$id'
			GROUP BY D.id";//PIUTANG PERMINATAAN
		$rows=$this->db->query($q)->result();
		foreach($rows as $row){
			$totalkeseluruhan=$row->kuantitas * $row->harga;
			$data_detail=array(
				'idvalidasi' =>$idvalidasi,
				'idtransaksi' => $row->idtransaksi,
				'idpermintaan' => $row->idpermintaan,
				'nopermintaan' => $row->nopermintaan,
				'idet' => $row->idet,
				'idtipe' => $row->idtipe,
				'idkategori' => $row->idkategori,
				'idbarang' => $row->idbarang,
				'nama_barang' => $row->nama_barang,
				'harga' => $row->harga,
				'kuantitas' => $row->kuantitas,
				'totalkeseluruhan' => $totalkeseluruhan,
				'jenis_id' => $row->jenis_id,
				'jenis_nama' => $row->jenis_nama,
				'idakun' => $row->idakun,
				'posisi_akun' => $row->posisi_akun,
			);
			$this->db->insert('tvalidasi_pengelolaan_permintaan',$data_detail);
		}
		
		if ($st_auto_posting=='1'){//otomatis
			$data_update=array(
				'st_posting' =>1,
				'posting_by' => $this->session->userdata('user_id'),
				'posting_nama' => $this->session->userdata('user_id'),
				'posting_date' => date('Y-m-d H:i:s'),
			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_pengelolaan',$data_update);
		}
		// print_r($idvalidasi);exit();
		return true;
	}
	
    public function updateData()
    {
        $this->nama_pengelolaan 	= $_POST['nama_pengelolaan'];
        $this->deskripsi_pengelolaan 	= $_POST['deskripsi_pengelolaan'];
		if ($this->input->post('btn_simpan_all')=='2'){			
			$this->st_proses 	= 1;
		}
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('tpengelolaan', $this, array('id' => $_POST['tpengelolaan_id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mdata_piutang', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mdata_piutang', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function simpan_proses_peretujuan($id) {
		$q="SELECT S.id,S.step,S.proses_setuju,S.proses_tolak,U.`name` as user_nama,S.iduser
          FROM tpengelolaan H          
          LEFT JOIN mdata_pengelolaan_approval S ON S.idpengelolaan=H.idpengelolaan
          LEFT JOIN musers U ON U.id=S.iduser
          WHERE H.id='$id'  AND calculate_logic(S.operand,H.nominal,S.nominal) = 1
          ORDER BY S.step ASC,S.id ASC";
		$list_user=$this->db->query($q)->result();
		$step=999;
		$idlogic='';
		$this->db->delete('tpengelolaan_approval',array('tpengelolaan_id'=>$id));
		foreach ($list_user as $row){
			if ($row->step < $step){
				$step=$row->step;
			}
			$data=array(
				'tpengelolaan_id' => $id,
				'step' => $row->step,
				// 'idlogic' => $row->idlogic,
				'iduser' => $row->iduser,
				'user_nama' => $row->user_nama,
				'proses_setuju' => $row->proses_setuju,
				'proses_tolak' => $row->proses_tolak,
				'approve' => 0,
			);
			// $idlogic=$row->idlogic;
			$this->db->insert('tpengelolaan_approval', $data);
		}
		
		$this->db->update('tpengelolaan',array('st_proses'=>2,'st_approval'=>1),array('id'=>$id));
		return $this->db->update('tpengelolaan_approval',array('st_aktif'=>1),array('tpengelolaan_id'=>$id,'step'=>$step));
		
    }
	public function save_terima(){

		$xjenis_kas_id= $_POST['xjenis_kas_id'];
		$xsumber_kas_id= $_POST['xsumber_kas_id'];
		$xidmetode= $_POST['xidmetode'];
		$ket= $_POST['ket'];
		$xnominal_bayar= $_POST['xnominal_bayar'];
		$xiddet= $_POST['xiddet'];
		$xstatus= $_POST['xstatus'];
		$xtanggal_pencairan= $_POST['xtanggal_pencairan'];
		foreach ($xjenis_kas_id as $index => $val){
			if ($xiddet[$index]!=''){
				$data_detail=array(
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'idmetode'=>$xidmetode[$index],
					'ket_pembayaran'=>$ket[$index],
					'tanggal_pencairan'=>YMDFormat($xtanggal_pencairan[$index]),
					'status'=>$xstatus[$index],
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);

				$this->db->where('id',$xiddet[$index]);
				$this->db->update('tpengelolaan_bayar_terima', $data_detail);
			}else{
				$tpengelolaan_id = $this->input->post('tpengelolaan_id');
				$data_detail=array(
					'tpengelolaan_id'=>$tpengelolaan_id,
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'idmetode'=>$xidmetode[$index],
					'ket_pembayaran'=>$ket[$index],
					'tanggal_pencairan'=>YMDFormat($xtanggal_pencairan[$index]),
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->insert('tpengelolaan_bayar_terima', $data_detail);
				// print_r($data_detail);exit();
			}
		}

		$this->bayar_by  = $this->session->userdata('user_id');
		$this->bayar_date  = date('Y-m-d H:i:s');
		if ($this->input->post('btn_simpan')=='2') {
			$this->st_proses  = 4;
			$this->st_verifikasi  = 1;
		}
		$this->db->where('id', $_POST['tpengelolaan_id']);
		if ($this->db->update('tpengelolaan', $this)) {
			if ($this->input->post('btn_simpan')=='2') {
				$this->verifikasi($_POST['tpengelolaan_id']);
			}
			return true;
		}else{
			return false;
		}
	}
	public function save_bayar(){

		$xjenis_kas_id= $_POST['xjenis_kas_id'];
		$xsumber_kas_id= $_POST['xsumber_kas_id'];
		$xidmetode= $_POST['xidmetode'];
		$ket= $_POST['ket'];
		$xnominal_bayar= $_POST['xnominal_bayar'];
		$xiddet= $_POST['xiddet'];
		$xstatus= $_POST['xstatus'];
		$xtanggal_pencairan= $_POST['xtanggal_pencairan'];
		foreach ($xjenis_kas_id as $index => $val){
			if ($xiddet[$index]!=''){
				$data_detail=array(
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'idmetode'=>$xidmetode[$index],
					'ket_pembayaran'=>$ket[$index],
					'tanggal_pencairan'=>YMDFormat($xtanggal_pencairan[$index]),
					'status'=>$xstatus[$index],
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);

				$this->db->where('id',$xiddet[$index]);
				$this->db->update('tpengelolaan_bayar', $data_detail);
			}else{
				$tpengelolaan_id = $this->input->post('tpengelolaan_id');
				$data_detail=array(
					'tpengelolaan_id'=>$tpengelolaan_id,
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'idmetode'=>$xidmetode[$index],
					'ket_pembayaran'=>$ket[$index],
					'tanggal_pencairan'=>YMDFormat($xtanggal_pencairan[$index]),
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->insert('tpengelolaan_bayar', $data_detail);
				// print_r($data_detail);exit();
			}
		}

		$this->bayar_by  = $this->session->userdata('user_id');
		$this->bayar_date  = date('Y-m-d H:i:s');
		
		$this->db->where('id', $_POST['tpengelolaan_id']);
		if ($this->db->update('tpengelolaan', $this)) {
			if ($this->input->post('btn_simpan')=='2') {
				$this->verifikasi($_POST['tpengelolaan_id']);
			}
			return true;
		}else{
			return false;
		}
	}
	function verifikasi($id){
		$this->insert_validasi_pengelolaan($id);
		$data=array(
			'st_proses'=>4,
			'st_verifikasi'=>1,
		);
		
		$this->db->where('id', $id);
		if ($this->db->update('tpengelolaan', $data)) {
			return true;
		}else{
			return false;
		}
	}
	public function list_pembayaran($id) {
        // $this->db->where('id', $id);
        // $query = $this->db->get('tsetoran_kas_detail');
		$q="
		SELECT JK.nama as jenis_kas,SK.nama as sumber_kas,D.* FROM tpengelolaan_bayar_terima D
		LEFT JOIN mjenis_kas JK ON JK.id=D.jenis_kas_id
		LEFT JOIN msumber_kas SK ON SK.id=D.sumber_kas_id
		WHERE D.tpengelolaan_id='$id' AND D.status='1'
		UNION ALL
		SELECT JK.nama as jenis_kas,SK.nama as sumber_kas,D.* FROM tpengelolaan_bayar D
		LEFT JOIN mjenis_kas JK ON JK.id=D.jenis_kas_id
		LEFT JOIN msumber_kas SK ON SK.id=D.sumber_kas_id
		WHERE D.tpengelolaan_id='$id' AND D.status='1'
		";
        return $this->db->query($q)->result();
    }
}
