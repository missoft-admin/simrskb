<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tgudang_approval_model extends CI_Model
{
  
   public function get_header($id){
	   $q="SELECT H.id,H.nama_bagi_hasil,H.tanggal_trx,H.notransaksi,H.mbagi_hasil_id,H.list_tbagi_hasil_id as list_trx
			,H.pendapatan_bersih_rs,H.pendapatan_bersih_ps,H.`status`
			FROM `tbagi_hasil_bayar_head` H
			WHERE H.id='$id'
			";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function list_mbagi_hasil(){
	   $q="SELECT id,nama from mbagi_hasil 
				WHERE mbagi_hasil.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_bagi_hasil(){
	   $q="SELECT id,nama from mbagi_hasil 
				WHERE mbagi_hasil.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function saveData(){
	   $list_trx=$this->input->post('list_trx');
	   $q="SELECT H.mbagi_hasil_id,H.nama_bagi_hasil,SUM(H.pendapatan_bersih_rs) as pendapatan_bersih_rs,SUM(pendapatan_bersih_ps) as pendapatan_bersih_ps,SUM(jumlah_lembar) as jumlah_lembar FROM tbagi_hasil H
			WHERE H.id IN (".$list_trx.")";
		$head=$this->db->query($q)->row_array();
		$head['list_tbagi_hasil_id']=$list_trx;
		$head['tanggal_trx']=date('Y-m-d H:i:s');
		$head['created_date']=date('Y-m-d H:i:s');
		$head['created_by']=$this->session->userdata('user_id');
		
		$this->db->insert('tbagi_hasil_bayar_head',$head);
		
		$tbagi_hasil_bayar_id= $this->db->insert_id();
		$id_pemilik_saham=$this->input->post('id_pemilik_saham');
		$nama_pemilik=$this->input->post('nama_pemilik');
		$tipe_pemilik=$this->input->post('tipe_pemilik');
		$total_pendapatan=($this->input->post('total_pendapatan'));
		$idmetode=($this->input->post('idmetode'));
		$pemilik_rek_id=($this->input->post('pemilik_rek_id'));
		$tanggal_bayar=($this->input->post('tanggal_bayar'));
		$jumlah_lembar=($this->input->post('jumlah_lembar'));
		foreach($id_pemilik_saham as $index => $val){
			$rek=getRekeningPS_ID($pemilik_rek_id[$index]);
			// if ($rek){}else{$rek[]=(object) array('idbank'=>null,'bank'=>null,'norek'=>null,'atas_nama'=>null);}
			$detail=array(
				'tbagi_hasil_bayar_id'=>$tbagi_hasil_bayar_id,
				'id_pemilik_saham'=>$id_pemilik_saham[$index],
				'nama_pemilik'=>$nama_pemilik[$index],
				'tipe_pemilik'=>$tipe_pemilik[$index],
				'tipe_pemilik_nama'=>GetTipePemilikSaham($tipe_pemilik[$index]),
				'total_pendapatan'=>RemoveComma($total_pendapatan[$index]),
				'idmetode'=>$idmetode[$index],
				'jumlah_lembar'=>$jumlah_lembar[$index],
				'pemilik_rek_id'=>$pemilik_rek_id[$index],
				'idbank'=>($rek)?$rek->idbank:null,
				'bank'=>($rek)?$rek->bank:null,
				'norek'=>($rek)?$rek->norek:null,
				'atas_nama'=>($rek)?$rek->atas_nama:null,
				'tanggal_bayar'=>YMDFormat($tanggal_bayar[$index]),
			);
			$result=$this->db->insert('tbagi_hasil_bayar_detail',$detail);
		}
		$arr=explode(',',$list_trx);
		foreach($arr as $index => $val){
			$data=array(
				'tbagi_hasil_bayar_id'=>$tbagi_hasil_bayar_id,
				'tbagi_hasil_id'=>$val,
			);
			$result=$this->db->insert('tbagi_hasil_bayar_head_trx',$data);
		}
		
		return $result;
   }
   public function updateData(){
	   $id=$this->input->post('id');
	   $list_trx=$this->input->post('list_trx');
	   $q="SELECT H.mbagi_hasil_id,H.nama_bagi_hasil,SUM(H.pendapatan_bersih_rs) as pendapatan_bersih_rs,SUM(pendapatan_bersih_ps) as pendapatan_bersih_ps,SUM(jumlah_lembar) as jumlah_lembar FROM tbagi_hasil H
			WHERE H.id IN (".$list_trx.")";
		$head=$this->db->query($q)->row_array();
		$head['list_tbagi_hasil_id']=$list_trx;
		// $head['tanggal_trx']=date('Y-m-d H:i:s');
		$head['edited_date']=date('Y-m-d H:i:s');
		$head['edited_by']=$this->session->userdata('user_id');
		
		$this->db->where('id',$id);
		$this->db->update('tbagi_hasil_bayar_head',$head);
		
		$tbagi_hasil_bayar_id= $id;
		
		// $this->db->where('tbagi_hasil_bayar_id',$tbagi_hasil_bayar_id);
		// $this->db->delete('tbagi_hasil_bayar_detail');
		$this->db->where('tbagi_hasil_bayar_id',$tbagi_hasil_bayar_id);
		$this->db->delete('tbagi_hasil_bayar_head_trx');
		
		$id_pemilik_saham=$this->input->post('id_pemilik_saham');
		$nama_pemilik=$this->input->post('nama_pemilik');
		$tipe_pemilik=$this->input->post('tipe_pemilik');
		$total_pendapatan=($this->input->post('total_pendapatan'));
		$idmetode=($this->input->post('idmetode'));
		$pemilik_rek_id=($this->input->post('pemilik_rek_id'));
		$tanggal_bayar=($this->input->post('tanggal_bayar'));
		$jumlah_lembar=($this->input->post('jumlah_lembar'));
		foreach($id_pemilik_saham as $index => $val){
			$rek=getRekeningPS_ID($pemilik_rek_id[$index]);
			// if ($rek){}else{$rek[]=(object) array('idbank'=>null,'bank'=>null,'norek'=>null,'atas_nama'=>null);}
			$detail=array(
				'tbagi_hasil_bayar_id'=>$tbagi_hasil_bayar_id,
				'id_pemilik_saham'=>$id_pemilik_saham[$index],
				'nama_pemilik'=>$nama_pemilik[$index],
				'tipe_pemilik'=>$tipe_pemilik[$index],
				'tipe_pemilik_nama'=>GetTipePemilikSaham($tipe_pemilik[$index]),
				'total_pendapatan'=>RemoveComma($total_pendapatan[$index]),
				'idmetode'=>$idmetode[$index],
				'jumlah_lembar'=>$jumlah_lembar[$index],
				'pemilik_rek_id'=>$pemilik_rek_id[$index],
				'idbank'=>($rek)?$rek->idbank:null,
				'bank'=>($rek)?$rek->bank:null,
				'norek'=>($rek)?$rek->norek:null,
				'atas_nama'=>($rek)?$rek->atas_nama:null,
				'tanggal_bayar'=>YMDFormat($tanggal_bayar[$index]),
			);
			$result=$this->db->insert('tbagi_hasil_bayar_detail',$detail);
		}
			// print_r($detail);exit();
		
		$arr=explode(',',$list_trx);
		foreach($arr as $index => $val){
			$data=array(
				'tbagi_hasil_bayar_id'=>$tbagi_hasil_bayar_id,
				'tbagi_hasil_id'=>$val,
			);
			$result=$this->db->insert('tbagi_hasil_bayar_head_trx',$data);
		}
		
		return $result;
   }
   public function detail($id){
	   $q="SELECT * from tbagi_hasil 
				WHERE tbagi_hasil.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function simpan_proses_peretujuan($id) {
		$q="SELECT S.id,S.step,S.proses_setuju,S.proses_tolak,U.`name` as user_nama,S.iduser FROM tbagi_hasil_bayar_head H
			LEFT JOIN mbagi_hasil_logic S ON S.mbagi_hasil_id=H.mbagi_hasil_id AND S.`status`='1'
			LEFT JOIN musers U ON U.id=S.iduser
			WHERE H.id='$id'  AND calculate_logic(S.operand,H.nominal_dibagikan,S.nominal) = 1
			ORDER BY S.step ASC,S.id ASC";
		$list_user=$this->db->query($q)->result();
		$step=999;
		$idlogic='';
		foreach ($list_user as $row){
			if ($row->step < $step){
				$step=$row->step;
			}
			$data=array(
				'tbagi_hasil_bayar_id' => $id,
				'step' => $row->step,
				// 'idlogic' => $row->idlogic,
				'iduser' => $row->iduser,
				'user_nama' => $row->user_nama,
				'proses_setuju' => $row->proses_setuju,
				'proses_tolak' => $row->proses_tolak,
				'approve' => 0,
			);
			// $idlogic=$row->idlogic;
			$this->db->insert('tbagi_hasil_bayar_approval', $data);
		}
		
		$this->db->update('tbagi_hasil_bayar_head',array('status'=>2),array('id'=>$id));
		return $this->db->update('tbagi_hasil_bayar_approval',array('st_aktif'=>1),array('tbagi_hasil_bayar_id'=>$id,'step'=>$step));
		
		// $data=array(
			// 'idlogic' => $idlogic,
			// 'status' => '2',
			// 'proses_by' => $this->session->userdata('user_id'),
			// 'proses_nama' => $this->session->userdata('user_name'),
			// 'proses_date' => date('Y-m-d H:i:s'),
		// );
		// $this->db->where('id',$id);
		return $this->db->update('rka_pengajuan', $data);
		
		
    }

}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
