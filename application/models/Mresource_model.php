<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Mresource_model extends CI_Model
{
    public function save_resource()
    {
        $this->db->set('resource', $this->input->post('resource'));
        $this->db->set('tipe', $this->input->post('tipe'));
        $this->db->set('jenis', $this->input->post('jenis'));
        $this->db->set('headerpath', $this->input->post('path'));
        $simpan = $this->db->insert('mresource');
        if ($simpan) {
            $data['status'] = 'success';
            $data['msg'] = 'Data telah berhasil disimpan';
        } else {
            $data['status'] = 'error';
            $data['msg'] = 'Data telah gagal disimpan';
        }
        $this->output->set_output(json_encode($data));
    }
}

/* End of file Mresource_model.php */
/* Location: ./application/models/Mresource_model.php */
