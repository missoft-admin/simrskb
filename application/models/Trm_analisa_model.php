<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Trm_analisa_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function GetInfoBerkas($idberkas)
    {
        $query = $this->db->query("SELECT
          (CASE 
            WHEN trm_layanan_berkas.tujuan IN (1, 2) THEN
              'rajal'
            WHEN trm_layanan_berkas.tujuan IN (3, 4) THEN
              'ranap'
          END) AS jenis_transaksi,
          (CASE 
            WHEN trm_layanan_berkas.tujuan IN (1, 2) THEN
              tpoliklinik_pendaftaran.nopendaftaran
            WHEN trm_layanan_berkas.tujuan IN (3, 4) THEN
              trawatinap_pendaftaran.nopendaftaran
          END) AS notransaksi,
          trm_layanan_berkas.no_medrec AS nomedrec,
          trm_layanan_berkas.namapasien,
          trm_layanan_berkas.user_nama_kodefikasi AS petugas_analisa,
          trm_layanan_berkas.tanggal_lahir,
          mdokter.nama AS dokter_pj,
          CONCAT(mkelas.nama, '-', mbed.nama) AS ruangan_perawatan
        FROM
          trm_layanan_berkas
        LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = trm_layanan_berkas.id_trx AND trm_layanan_berkas.tujuan IN (1, 2)
        LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trm_layanan_berkas.id_trx AND trm_layanan_berkas.tujuan IN (3, 4)
        LEFT JOIN mdokter ON mdokter.id = trm_layanan_berkas.iddokter
        LEFT JOIN mkelas ON mkelas.id = trm_layanan_berkas.kelas
        LEFT JOIN mbed ON mbed.id = trm_layanan_berkas.bed
        WHERE trm_layanan_berkas.id = $idberkas");
        return $query->row();
	  }

    public function getInfoSetting($idsetting)
    {
      $this->db->select('*');
      $this->db->from('msetting_analisa');
      $this->db->where('id', $idsetting);
      $query = $this->db->get();
      return $query->row();
    }
    
    public function getSettingAnalisa($data)
    {
        $array = json_decode($data, true);

        function compareUrutanTampilanBerkas($a, $b) {
          return $a['urutan_tampil'] - $b['urutan_tampil'];
        }
        
        usort($array, 'compareUrutanTampilanBerkas');

        $dataSorted = json_encode($array);

        $dataAnalisa = array_map(function ($item) {
            $lembaran = getByQuery(
                array(
                  'output_type' => 'single',
                  'table' => 'mlembaran_rm',
                  'select' => array('*'),
                  'where' => array(
                    ['id', $item->lembaran_id]
                  )
                )
            );

            $listDokter = array();
            $listPegawai = array();
            if (isset($lembaran->referensi)) {
              if (in_array('master_dokter', json_decode($lembaran->referensi))) {
                  $listDokter = getByQuery(
                      array(
                        'output_type' => 'all',
                        'table' => 'mdokter',
                        'select' => array('id', 'nama', '"dokter" AS tipe'),
                        'where' => array(
                          ['status', 1]
                        )
                      )
                  );
              }
              
              if (in_array('master_pegawai', json_decode($lembaran->referensi))) {
                  $listPegawai = getByQuery(
                      array(
                        'output_type' => 'all',
                        'table' => 'mpegawai',
                        'select' => array('id', 'nama', '"pegawai" AS tipe'),
                        'where' => array(
                          ['status', 1]
                        )
                      )
                  );
              }
            }

            $dataDokterOrPegawai = array_merge($listDokter, $listPegawai);

            return array(
              'id' => $item->lembaran_id,
              'nama' => $lembaran->nama,
              'kriteria' => json_decode($lembaran->kriteria),
              'detail' => json_decode($lembaran->detail),
              'dokter' => $dataDokterOrPegawai,
              'urutan_tampil' => $item->urutan_tampil,
              'variable_hold' => $item->variable_hold,
            );
        }, json_decode($dataSorted));

        return $dataAnalisa;
    }

    public function runLogic($analisaBerkas, $idberkas)
    {
        $berkas = getByQuery(
            array(
            'output_type' => 'single',
            'table' => 'trm_layanan_berkas',
            'select' => array('*'),
            'where' => array(
              ['id', $idberkas]
            )
          )
        );

        if ($berkas) {
            $operasi = $this->getPendaftaranOperasi($berkas->tujuan, $berkas->id_trx);
            if ($operasi) {
              $queryKelTindakan = getByQuery(
                  array(
                  'output_type' => 'all',
                  'table' => 'tkamaroperasi_tindakan',
                  'select' => array('kelompok_tindakan_id'),
                  'where' => array(
                    ['id', $operasi->id]
                  )
                )
              );
              $kelompokTindakan = isset($queryKelTindakan) ? array_column($queryKelTindakan, 'kelompok_tindakan_id') : array();
              $kelompokTindakanOperasi = $operasi->kelompok_operasi_id;
            } else {
              $kelompokTindakan = array();
              $kelompokTindakanOperasi = array();
            }

            // End Of File queryOperasi

            if (in_array($berkas->tujuan, array('1', '2'))) {
                // Jenis Kunjungan RAWAT JALAN
                $jenisKunjungan = 'rawat_jalan';
                $poliklinikKelas = $berkas->idpoliklinik;

                $queryKelDiagnosa = getByQuery(
                    array(
                    'output_type' => 'all',
                    'table' => 'tpoliklinik_diagnosa',
                    'select' => array('idkelompok_diagnosa'),
                    'where' => array(
                      ['idpendaftaran', $berkas->id_trx]
                    )
                  )
                );

                $kelompokDiagnosa = isset($queryKelDiagnosa) ? array_column($queryKelDiagnosa, 'idkelompok_diagnosa') : array();
            } elseif (in_array($berkas->tujuan, array('3', '4'))) {
                // Jenis Kunjungan RAWAT INAP
                $jenisKunjungan = 'rawat_inap';
                $poliklinikKelas = $berkas->kelas;

                $queryKelDiagnosa = getByQuery(
                    array(
                    'output_type' => 'all',
                    'table' => 'trawatinap_diagnosa',
                    'select' => array('idkelompok_diagnosa'),
                    'where' => array(
                      ['idpendaftaran', $berkas->id_trx]
                    )
                  )
                );

                $kelompokDiagnosa = isset($queryKelDiagnosa) ? array_column($queryKelDiagnosa, 'idkelompok_diagnosa') : array();
            }

            $dataBerkas = array(
              'jenisKunjungan' => $jenisKunjungan,
              'tipeKunjungan' => BerkasTipeKunjungan($berkas->tujuan),
              'listDokter' => $berkas->iddokter,
              'poliklinikKelas' => $poliklinikKelas,
              'kelompokDiagnosa' => $kelompokDiagnosa,
              'kelompokTindakan' => $kelompokTindakan,
              'kelompokTindakanOperasi' => $kelompokTindakanOperasi,
            );

            return logicAnalisaRekamMedis($analisaBerkas, $dataBerkas);
        } else {
            return false;
        }
    }

    public function getPendaftaranOperasi($tujuan, $idtransaksi) {
      if (in_array($tujuan, array('1', '2'))) {
        $idAsalPendaftaran = '1';
      } else if (in_array($tujuan, array('3', '4'))) {
        $idAsalPendaftaran = '2';
      }

      $pendaftaranOperasi = getByQuery(
          array(
          'output_type' => 'single',
          'table' => 'tkamaroperasi_pendaftaran',
          'select' => array('id', 'kelompok_operasi_id'),
          'where' => array(
            ['idasalpendaftaran', $idAsalPendaftaran],
            ['idpendaftaran', $idtransaksi],
          )
        )
      );

      return $pendaftaranOperasi;
    }
}
