<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mtarif_rawatjalan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mtarif_rawatjalan');
        return $query->row();
    }

    public function saveData()
    {
        $this->nama            = $_POST['nama'];
        $this->jasasarana      = RemoveComma($_POST['jasasarana']);
        $this->jasapelayanan   = RemoveComma($_POST['jasapelayanan']);
        $this->bhp             = RemoveComma($_POST['bhp']);
        $this->biayaperawatan  = RemoveComma($_POST['biayaperawatan']);
        $this->total           = RemoveComma($_POST['total']);
        $this->idkelompok      = $_POST['idkelompok'];
        $this->headerpath      = $_POST['headerpath'];
        $this->path            = $_POST['path'];
        $this->level           = $_POST['level'];
        $this->status          = 1;
    		$this->created_by  = $this->session->userdata('user_id');
    		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mtarif_rawatjalan', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama            = $_POST['nama'];
        $this->jasasarana      = RemoveComma($_POST['jasasarana']);
        $this->jasapelayanan   = RemoveComma($_POST['jasapelayanan']);
        $this->bhp             = RemoveComma($_POST['bhp']);
        $this->biayaperawatan  = RemoveComma($_POST['biayaperawatan']);
        $this->total           = RemoveComma($_POST['total']);
        $this->idkelompok      = $_POST['idkelompok'];
        $this->status          = 1;
    		$this->edited_by  = $this->session->userdata('user_id');
    		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mtarif_rawatjalan', $this, array('id' => $_POST['id']))) {
            // IF CHANGE PARENT
            if ($_POST['headerpath'] != $_POST['old_headerpath']) {
                $this->db->query("call updateTarifRawatJalan('".$_POST['id']."','".$_POST['headerpath']."')");
            }
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
        return true;
    }

    public function softDelete($id)
    {
        $this->status = 0;
    		$this->deleted_by  = $this->session->userdata('user_id');
    		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mtarif_rawatjalan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

  	public function getLevel0(){
  		$q="select *from mtarif_rawatjalan where status=1 ORDER BY path ASC";
  		$query=$this->db->query($q);
  		return $query->result();
  	}

    public function getAllParent($headerpath=0)
    {
        # Get ID Root
        $this->db->select('MAX(CAST(headerpath AS SIGNED)) + 1 AS idroot');
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_rawatjalan');
        $idroot = $query->row()->idroot;
        # Get Option
        $this->db->where('status', '1');
        $this->db->order_by('path', 'ASC');
        $query = $this->db->get('mtarif_rawatjalan');
        $result = $query->result();
	      // print_r($this->db->last_query());exit();

        $data = '<option value="'.($idroot ? $idroot:1).'">Root</option>';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                if ($headerpath != 0) {
                    $selected = ($headerpath == $row->path ? 'selected' : '');
                } else {
                    $selected = '';
                }
                $data .= '<option value="'.$row->path.'" '.$selected.'>'.TreeView($row->level, $row->nama).'</option>';
            }
        }
        return $data;
    }

    public function getPathLevel($headerpath)
    {
        $strquery = "SELECT LEVEL + 1 AS level, CASE WHEN ( SELECT COUNT(headerpath) FROM mtarif_rawatjalan WHERE headerpath = t1.headerpath ) = 0 THEN CONCAT(t1.path, '.1') ELSE  CONCAT(t1.path,'.',  IF ( ( SELECT COUNT(headerpath) FROM mtarif_rawatjalan WHERE headerpath = t1.path ) = 0, ( SELECT COUNT(headerpath) FROM mtarif_rawatjalan WHERE headerpath = t1.path ) + 1, IF ( LENGTH(t1.path) = '1',( SELECT COUNT(headerpath) FROM mtarif_rawatjalan WHERE headerpath = t1.path ), ( SELECT COUNT(headerpath) FROM mtarif_rawatjalan WHERE headerpath = t1.path ) + 1 ) ) ) END AS path FROM mtarif_rawatjalan t1 WHERE t1.path = '".$headerpath."'";
        $query = $this->db->query($strquery);
        $row = $query->row();

        if ($query->num_rows() > 0) {
            $data['path']  = $row->path;
            $data['level'] = $row->level;
        } else {
            $data['path']  = 1;
            $data['level'] = 0;
        }
        return $data;
    }

    public function getGroupPembayaran()
    {
        $this->db->where('status', 1);
        $query = $this->db->get('mgroup_pembayaran');
        return $query->result();
    }

    public function updateSetting()
    {
        $this->group_jasasarana      = RemoveComma($_POST['group_jasasarana']);
        $this->group_jasapelayanan   = RemoveComma($_POST['group_jasapelayanan']);
        $this->group_bhp             = RemoveComma($_POST['group_bhp']);
        $this->group_biayaperawatan  = RemoveComma($_POST['group_biayaperawatan']);
		
		$this->group_jasasarana_diskon      = RemoveComma($_POST['group_jasasarana_diskon']);
        $this->group_jasapelayanan_diskon   = RemoveComma($_POST['group_jasapelayanan_diskon']);
        $this->group_bhp_diskon             = RemoveComma($_POST['group_bhp_diskon']);
        $this->group_biayaperawatan_diskon  = RemoveComma($_POST['group_biayaperawatan_diskon']);
        $this->group_diskon_all  = RemoveComma($_POST['group_diskon_all']);

        if ($this->db->update('mtarif_rawatjalan', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
        return $status;
    }
}
