<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mtarif_radiologi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($idtipe)
    {
        $this->db->select('mtarif_radiologi.*,
        (CASE WHEN mtarif_radiologi.idtipe = 1 AND mtarif_radiologi.idkelompok = 0 THEN
          CONCAT(mtarif_radiologi.nama,  "(", mtarif_radiologi_expose.nama, mtarif_radiologi_film.nama, ")")
        ELSE
          mtarif_radiologi.nama
        END) AS namatarif');
        $this->db->join('mtarif_radiologi_expose', 'mtarif_radiologi_expose.id = mtarif_radiologi.idexpose', 'LEFT');
        $this->db->join('mtarif_radiologi_film', 'mtarif_radiologi_film.id = mtarif_radiologi.idfilm', 'LEFT');
        $this->db->where('mtarif_radiologi.idtipe', $idtipe);
        $this->db->where('mtarif_radiologi.status', '1');
        $this->db->order_by('mtarif_radiologi.path', 'ASC');
        $this->db->get('mtarif_radiologi');

        return $this->db->last_query();
    }

    public function getAllParent($idtipe)
    {
        $this->db->select('mtarif_radiologi.*, (CASE WHEN mtarif_radiologi.idtipe = 1 AND mtarif_radiologi.idkelompok = 0 THEN
          CONCAT(mtarif_radiologi.nama,  " (", mtarif_radiologi_expose.nama, mtarif_radiologi_film.nama, ")")
        ELSE
          mtarif_radiologi.nama
        END) AS namatarif');
        $this->db->join('mtarif_radiologi_expose', 'mtarif_radiologi_expose.id = mtarif_radiologi.idexpose', 'LEFT');
        $this->db->join('mtarif_radiologi_film', 'mtarif_radiologi_film.id = mtarif_radiologi.idfilm', 'LEFT');
        $this->db->where('mtarif_radiologi.idtipe', $idtipe);
        $this->db->where('mtarif_radiologi.status', '1');
        $this->db->order_by('mtarif_radiologi.path', 'ASC');
        $query = $this->db->get('mtarif_radiologi');

        return $query->result();
    }

    public function getAllExpose()
    {
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_radiologi_expose');

        return $query->result();
    }

    public function getAllFilm()
    {
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_radiologi_film');

        return $query->result();
    }

    public function getDetailTarif($id)
    {
        $this->db->where('idtarif', $id);
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_radiologi_detail');

        return $query->result();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mtarif_radiologi');

        return $query->row();
    }

    public function saveData()
    {
        $this->kode = $_POST['kode'];
        $this->nama = $_POST['nama'];
        $this->nama_english = $_POST['nama_english'];
        $this->idexpose = (isset($_POST['idexpose']) ? $_POST['idexpose'] : 0);
        $this->idfilm = (isset($_POST['idfilm']) ? $_POST['idfilm'] : 0);
        $this->idtipe = $_POST['idtipe'];
        $this->idkelompok = $_POST['idkelompok'];
        $this->level0 = $_POST['level0'];
        $this->headerpath = $_POST['headerpath'];
        $this->path = $_POST['path'];
        $this->level = $_POST['level'];
        $this->status = 1;
        $this->created_by = $this->session->userdata('user_id');
        $this->created_date = date('Y-m-d H:i:s');

        if ($this->db->insert('mtarif_radiologi', $this)) {
            $idtarif = $this->db->insert_id();
            if (1 === $this->idtipe && 0 === $this->idkelompok) {
                $xray_value = json_decode($_POST['xray_value']);
                foreach ($xray_value as $row) {
                    $detail = [];
                    $detail['idtarif'] = $idtarif;
                    $detail['kelas'] = $row[1];
                    $detail['jasasarana'] = RemoveComma($row[2]);
                    $detail['group_jasasarana'] = $row[3];
                    $detail['jasapelayanan'] = 0;
                    $detail['group_jasapelayanan'] = '';
                    $detail['bhp'] = RemoveComma($row[4]);
                    $detail['group_bhp'] = $row[5];
                    $detail['biayaperawatan'] = RemoveComma($row[6]);
                    $detail['group_biayaperawatan'] = $row[7];
                    $detail['total'] = RemoveComma($row[8]);
                    $detail['nominallangsung'] = RemoveComma($row[9]);
                    $detail['nominalmenolak'] = RemoveComma($row[10]);
                    $detail['nominalkembali'] = RemoveComma($row[11]);
                    $detail['status'] = '1';

                    $this->db->insert('mtarif_radiologi_detail', $detail);
                }
            } elseif (2 === $this->idtipe && 0 === $this->idkelompok) {
                $usg_value = json_decode($_POST['usg_value']);
                foreach ($usg_value as $row) {
                    $detail = [];
                    $detail['idtarif'] = $idtarif;
                    $detail['kelas'] = $row[1];
                    $detail['jasasarana'] = RemoveComma($row[2]);
                    $detail['group_jasasarana'] = $row[3];
                    $detail['jasapelayanan'] = RemoveComma($row[4]);
                    $detail['group_jasapelayanan'] = $row[5];
                    $detail['bhp'] = RemoveComma($row[6]);
                    $detail['group_bhp'] = $row[7];
                    $detail['biayaperawatan'] = RemoveComma($row[8]);
                    $detail['group_biayaperawatan'] = $row[9];
                    $detail['total'] = RemoveComma($row[10]);
                    $detail['nominallangsung'] = 0;
                    $detail['nominalmenolak'] = 0;
                    $detail['nominalkembali'] = 0;
                    $detail['status'] = '1';

                    $this->db->insert('mtarif_radiologi_detail', $detail);
                }
            } elseif ((3 === $this->idtipe || 4 === $this->idtipe || 5 === $this->idtipe) && 0 === $this->idkelompok) {
                $cmb_value = json_decode($_POST['cmb_value']);
                foreach ($cmb_value as $row) {
                    $detail = [];
                    $detail['idtarif'] = $idtarif;
                    $detail['kelas'] = $row[1];
                    $detail['jasasarana'] = RemoveComma($row[2]);
                    $detail['group_jasasarana'] = $row[3];
                    $detail['jasapelayanan'] = RemoveComma($row[4]);
                    $detail['group_jasapelayanan'] = $row[5];
                    $detail['bhp'] = RemoveComma($row[6]);
                    $detail['group_bhp'] = $row[7];
                    $detail['biayaperawatan'] = RemoveComma($row[8]);
                    $detail['group_biayaperawatan'] = $row[9];
                    $detail['total'] = RemoveComma($row[10]);
                    $detail['nominallangsung'] = 0;
                    $detail['nominalmenolak'] = 0;
                    $detail['nominalkembali'] = 0;
                    $detail['status'] = '1';

                    $this->db->insert('mtarif_radiologi_detail', $detail);
                }
            }

            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;
    }

    public function updateData()
    {
        $this->kode = $_POST['kode'];
        $this->nama = $_POST['nama'];
        $this->nama_english = $_POST['nama_english'];
        $this->idexpose = $_POST['idexpose'];
        $this->idfilm = $_POST['idfilm'];
        $this->idtipe = $_POST['idtipe'];
        $this->idkelompok = $_POST['idkelompok'];
        $this->level0 = $_POST['level0'];
        $this->headerpath = $_POST['headerpath'];
        $this->path = $_POST['path'];
        $this->level = $_POST['level'];
        $this->status = 1;
        $this->edited_by = $this->session->userdata('user_id');
        $this->edited_date = date('Y-m-d H:i:s');

        if ($this->db->update('mtarif_radiologi', $this, ['id' => $_POST['id']])) {
            $idtarif = $_POST['id'];
            if (1 === $this->idtipe && 0 === $this->idkelompok) {
                $this->db->where('idtarif', $idtarif);
                if ($this->db->delete('mtarif_radiologi_detail')) {
                    $xray_value = json_decode($_POST['xray_value']);
                    foreach ($xray_value as $row) {
                        $detail = [];
                        $detail['idtarif'] = $idtarif;
                        $detail['kelas'] = $row[1];
                        $detail['jasasarana'] = RemoveComma($row[2]);
                        $detail['group_jasasarana'] = $row[3];
                        $detail['jasapelayanan'] = 0;
                        $detail['group_jasapelayanan'] = '';
                        $detail['bhp'] = RemoveComma($row[4]);
                        $detail['group_bhp'] = $row[5];
                        $detail['biayaperawatan'] = RemoveComma($row[6]);
                        $detail['group_biayaperawatan'] = $row[7];
                        $detail['total'] = RemoveComma($row[8]);
                        $detail['nominallangsung'] = RemoveComma($row[9]);
                        $detail['nominalmenolak'] = RemoveComma($row[10]);
                        $detail['nominalkembali'] = RemoveComma($row[11]);
                        $detail['status'] = '1';

                        $this->db->insert('mtarif_radiologi_detail', $detail);
                    }
                }
            } elseif (2 === $this->idtipe && 0 === $this->idkelompok) {
                $this->db->where('idtarif', $idtarif);
                if ($this->db->delete('mtarif_radiologi_detail')) {
                    $usg_value = json_decode($_POST['usg_value']);
                    foreach ($usg_value as $row) {
                        $detail = [];
                        $detail['idtarif'] = $idtarif;
                        $detail['kelas'] = $row[1];
                        $detail['jasasarana'] = RemoveComma($row[2]);
                        $detail['group_jasasarana'] = $row[3];
                        $detail['jasapelayanan'] = RemoveComma($row[4]);
                        $detail['group_jasapelayanan'] = $row[5];
                        $detail['bhp'] = RemoveComma($row[6]);
                        $detail['group_bhp'] = $row[7];
                        $detail['biayaperawatan'] = RemoveComma($row[8]);
                        $detail['group_biayaperawatan'] = $row[9];
                        $detail['total'] = RemoveComma($row[10]);
                        $detail['nominallangsung'] = 0;
                        $detail['nominalmenolak'] = 0;
                        $detail['nominalkembali'] = 0;
                        $detail['status'] = '1';

                        $this->db->insert('mtarif_radiologi_detail', $detail);
                    }
                }
            } elseif ((3 === $this->idtipe || 4 === $this->idtipe || 5 === $this->idtipe) && 0 === $this->idkelompok) {
                $this->db->where('idtarif', $idtarif);
                if ($this->db->delete('mtarif_radiologi_detail')) {
                    $cmb_value = json_decode($_POST['cmb_value']);
                    foreach ($cmb_value as $row) {
                        $detail = [];
                        $detail['idtarif'] = $idtarif;
                        $detail['kelas'] = $row[1];
                        $detail['jasasarana'] = RemoveComma($row[2]);
                        $detail['group_jasasarana'] = $row[3];
                        $detail['jasapelayanan'] = RemoveComma($row[4]);
                        $detail['group_jasapelayanan'] = $row[5];
                        $detail['bhp'] = RemoveComma($row[6]);
                        $detail['group_bhp'] = $row[7];
                        $detail['biayaperawatan'] = RemoveComma($row[8]);
                        $detail['group_biayaperawatan'] = $row[9];
                        $detail['total'] = RemoveComma($row[10]);
                        $detail['nominallangsung'] = 0;
                        $detail['nominalmenolak'] = 0;
                        $detail['nominalkembali'] = 0;
                        $detail['status'] = '1';

                        $this->db->insert('mtarif_radiologi_detail', $detail);
                    }
                }
            }

            // IF CHANGE PARENT
            if ($_POST['headerpath'] !== $_POST['old_headerpath']) {
                $this->db->query("call updateTarifRadiologi('".$_POST['id']."','".$_POST['headerpath']."')");
            }

            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;
    }

    public function softDelete($id)
    {
        $this->status = 0;
        $this->deleted_by = $this->session->userdata('user_id');
        $this->deleted_date = date('Y-m-d H:i:s');
        if ($this->db->update('mtarif_radiologi', $this, ['id' => $id])) {
            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;
    }

    public function getParent($idtipe)
    {
        // Get ID Root
        $this->db->select('MAX(CAST(headerpath AS SIGNED)) + 1 AS idroot');
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_radiologi');
        $idroot = $query->row()->idroot;

        // Get Option
        $this->db->select('mtarif_radiologi.*, (CASE WHEN mtarif_radiologi.idtipe = 1 AND mtarif_radiologi.idkelompok = 0 THEN
          CONCAT(mtarif_radiologi.nama,  " (", mtarif_radiologi_expose.nama, mtarif_radiologi_film.nama, ")")
        ELSE
          mtarif_radiologi.nama
        END) AS namatarif');
        $this->db->join('mtarif_radiologi_expose', 'mtarif_radiologi_expose.id = mtarif_radiologi.idexpose', 'LEFT');
        $this->db->join('mtarif_radiologi_film', 'mtarif_radiologi_film.id = mtarif_radiologi.idfilm', 'LEFT');
        $this->db->where('mtarif_radiologi.idtipe', $idtipe);
        $this->db->where('mtarif_radiologi.status', '1');
        $this->db->order_by('mtarif_radiologi.path', 'ASC');
        $query = $this->db->get('mtarif_radiologi');
        $result = $query->result();

        $data = '<option value="'.($idroot ?: 1).'">Root</option>';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data .= '<option value="'.$row->path.'">'.TreeView($row->level, $row->namatarif).'</option>';
            }
        }

        return $data;
    }

    public function getPathLevel($headerpath)
    {
        $strquery = "SELECT LEVEL + 1 AS level, CASE WHEN ( SELECT COUNT(headerpath) FROM mtarif_radiologi WHERE headerpath = t1.headerpath ) = 0 THEN CONCAT(t1.path, '.1') ELSE  CONCAT(t1.path,'.',  IF ( ( SELECT COUNT(headerpath) FROM mtarif_radiologi WHERE headerpath = t1.path ) = 0, ( SELECT COUNT(headerpath) FROM mtarif_radiologi WHERE headerpath = t1.path ) + 1, IF ( LENGTH(t1.path) = '1',( SELECT COUNT(headerpath) FROM mtarif_radiologi WHERE headerpath = t1.path ), ( SELECT COUNT(headerpath) FROM mtarif_radiologi WHERE headerpath = t1.path ) + 1 ) ) ) END AS path FROM mtarif_radiologi t1 WHERE t1.path = '".$headerpath."'";
        $query = $this->db->query($strquery);
        $row = $query->row();

        if ($query->num_rows() > 0) {
            $data['path'] = $row->path;
            $data['level'] = $row->level;
        } else {
            $data['path'] = 1;
            $data['level'] = 0;
        }

        return $data;
    }

    public function getLevel0($idtipe = '0')
    {
        $q = 'select R.id,R.path,R.level,(CASE WHEN R.idtipe = 1 AND R.idkelompok = 0 THEN
                  CONCAT(R.nama,  " (", E.nama, F.nama, ")")
                ELSE
                  R.nama
                END) AS nama from mtarif_radiologi R
        LEFT JOIN mtarif_radiologi_expose E ON E.id=R.idexpose
        LEFT JOIN mtarif_radiologi_film F ON F.id=R.idfilm
        where R.status=1 AND R.idtipe='.$idtipe.' ORDER BY path ASC';
        $query = $this->db->query($q);

        return $query->result();
    }

    public function find_headparent($idtipe)
    {
        $q = 'select R.id,R.path,R.level,(CASE WHEN R.idtipe = 1 AND R.idkelompok = 0 THEN
                  CONCAT(R.nama,  " (", E.nama, F.nama, ")")
                ELSE
                  R.nama
                END) AS nama from mtarif_radiologi R
        LEFT JOIN mtarif_radiologi_expose E ON E.id=R.idexpose
        LEFT JOIN mtarif_radiologi_film F ON F.id=R.idfilm
        where R.status=1 AND R.idtipe='.$idtipe.' ORDER BY path ASC';
        $query = $this->db->query($q);
        $query = $query->result();
        $arr = '';
        foreach ($query as $row) {
            $arr = $arr.'<option value="'.$row->path.'">'.TreeView($row->level, $row->nama).'</option>';
        }

        return $arr;
    }

    public function updateSetting()
    {
        $settingList = json_decode($_POST['setting_value']);
        foreach ($settingList as $row) {
            $data = [];
            $data['group_jasasarana'] = $row[3];
            $data['group_jasapelayanan'] = $row[4];
            $data['group_bhp'] = $row[5];
            $data['group_biayaperawatan'] = $row[6];

            $this->db->update(
                'mtarif_radiologi_detail',
                $data,
                [
                    'idtarif' => $row[0],
                    'kelas' => $row[1],
                ]
            );
        }
        $settingDiskonList = json_decode($_POST['setting_diskon_value']);
        foreach ($settingDiskonList as $row) {
            $data = [];
            $data['group_jasasarana_diskon'] = $row[3];
            $data['group_jasapelayanan_diskon'] = $row[4];
            $data['group_bhp_diskon'] = $row[5];
            $data['group_biayaperawatan_diskon'] = $row[6];

            $this->db->update(
                'mtarif_radiologi_detail',
                $data,
                [
                    'idtarif' => $row[0],
                    'kelas' => $row[1],
                ]
            );
        }
        $id = $this->input->post('id');
        $group_diskon_all = $this->input->post('group_diskon_all');
        $this->db->where('id', $id);
        $this->db->update('mtarif_radiologi', ['group_diskon_all' => $group_diskon_all]);

        return true;
    }

    // # Update Document ERM
    public function find_index_parent($idtipe = '0')
    {
        $this->db->select('mtarif_radiologi.id,
            mtarif_radiologi.path,
            mtarif_radiologi.level,
            (CASE
                WHEN mtarif_radiologi.idtipe = 1 AND mtarif_radiologi.idkelompok = 0 THEN
                    CONCAT(mtarif_radiologi.nama,  " (", mtarif_radiologi_expose.nama, mtarif_radiologi_film.nama, ")")
                ELSE
                    mtarif_radiologi.nama
            END) AS nama');
        $this->db->join('mtarif_radiologi_expose', 'mtarif_radiologi_expose.id = mtarif_radiologi.idexpose', 'left');
        $this->db->join('mtarif_radiologi_film', 'mtarif_radiologi_film.id = mtarif_radiologi.idfilm', 'left');
        $this->db->where('mtarif_radiologi.status', '1');
        $this->db->where('mtarif_radiologi.level', '0');

        if ('0' !== $idtipe) {
            $this->db->where('mtarif_radiologi.idtipe', $idtipe);
        }

        $this->db->order_by('mtarif_radiologi.path', 'ASC');
        $query = $this->db->get('mtarif_radiologi');

        return $query->result();
    }

    public function find_index_subparent($idparent = '0')
    {
        $this->db->select('mtarif_radiologi.id,
            mtarif_radiologi.path,
            mtarif_radiologi.level,
            (CASE
                WHEN mtarif_radiologi.idtipe = 1 AND mtarif_radiologi.idkelompok = 0 THEN
                    CONCAT(mtarif_radiologi.nama,  " (", mtarif_radiologi_expose.nama, mtarif_radiologi_film.nama, ")")
                ELSE
                    mtarif_radiologi.nama
            END) AS nama');
        $this->db->join('mtarif_radiologi_expose', 'mtarif_radiologi_expose.id = mtarif_radiologi.idexpose', 'left');
        $this->db->join('mtarif_radiologi_film', 'mtarif_radiologi_film.id = mtarif_radiologi.idfilm', 'left');
        $this->db->where('mtarif_radiologi.status', '1');
        $this->db->where('mtarif_radiologi.level !=', '0');

        if ('0' !== $idparent) {
            $this->db->where('left(mtarif_radiologi.path,'.strlen($idparent).')', $idparent);
        }

        $this->db->order_by('mtarif_radiologi.path', 'ASC');
        $query = $this->db->get('mtarif_radiologi');

        return $query->result();
    }

    public function find_manage_parent($idtipe)
    {
        // Get ID Root
        $this->db->select('MAX(CAST(headerpath AS SIGNED)) + 1 AS idroot');
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_radiologi');
        $idroot = $query->row()->idroot;

        // Get Option
        $this->db->where('mtarif_radiologi.idtipe', $idtipe);
        $this->db->where('mtarif_radiologi.level', '0');
        $this->db->where('mtarif_radiologi.status', '1');
        $this->db->order_by('mtarif_radiologi.path', 'ASC');
        $query = $this->db->get('mtarif_radiologi');
        $result = $query->result();

        $data = '<option value="'.($idroot ?: 1).'">Root</option>';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data .= '<option value="'.$row->path.'">'.TreeView($row->level, $row->nama).'</option>';
            }
        }

        return $data;
    }

    public function find_manage_subparent($idparent)
    {
        // Get Option
        $this->db->where('left(mtarif_radiologi.path,' . strlen($idparent) . ')', $idparent);
        $this->db->where('mtarif_radiologi.level !=', '0');
        $this->db->where('mtarif_radiologi.status', '1');
        $this->db->order_by('mtarif_radiologi.path', 'ASC');
        $query = $this->db->get('mtarif_radiologi');
        $result = $query->result();

        $data = '';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data .= '<option value="'.$row->path.'">'.TreeView($row->level, $row->nama).'</option>';
            }
        }

        return $data;
    }

    public function find_update_parent($idtipe)
    {
        // Get ID Root
        $this->db->select('MAX(CAST(headerpath AS SIGNED)) + 1 AS idroot');
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_radiologi');
        $idroot = $query->row()->idroot;

        // Get Option
        $this->db->where('mtarif_radiologi.idtipe', $idtipe);
        $this->db->where('mtarif_radiologi.level', '0');
        $this->db->where('mtarif_radiologi.status', '1');
        $this->db->order_by('mtarif_radiologi.path', 'ASC');
        $query = $this->db->get('mtarif_radiologi');
        return $query->result();
    }

    public function find_update_subparent($idparent)
    {
        // Get Option
        $this->db->where('left(mtarif_radiologi.path,' . strlen($idparent) . ')', $idparent);
        $this->db->where('mtarif_radiologi.level !=', '0');
        $this->db->where('mtarif_radiologi.status', '1');
        $this->db->order_by('mtarif_radiologi.path', 'ASC');
        $query = $this->db->get('mtarif_radiologi');
        return $query->result();
    }
}
