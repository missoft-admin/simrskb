<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mtarif_bpjskesehatan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mtarif_bpjskesehatan');
        return $query->row();
    }

    public function saveData()
    {
        $this->kode 						= $_POST['kode'];
        $this->nama 						= $_POST['nama'];
        $this->idtipe 					= $_POST['idtipe'];
        $this->tarifrawatjalan 	= RemoveComma($_POST['tarifrawatjalan']);
        $this->tarifkelas1 			= RemoveComma($_POST['tarifkelas1']);
        $this->tarifkelas2 			= RemoveComma($_POST['tarifkelas2']);
        $this->tarifkelas3 			= RemoveComma($_POST['tarifkelas3']);
        $this->status 			  	= 1;
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		
        if ($this->db->insert('mtarif_bpjskesehatan', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->kode 						= $_POST['kode'];
        $this->nama 						= $_POST['nama'];
        $this->idtipe 					= $_POST['idtipe'];
        $this->tarifrawatjalan 	= RemoveComma($_POST['tarifrawatjalan']);
        $this->tarifkelas1 			= RemoveComma($_POST['tarifkelas1']);
        $this->tarifkelas2 			= RemoveComma($_POST['tarifkelas2']);
        $this->tarifkelas3 			= RemoveComma($_POST['tarifkelas3']);
        $this->status 			  	= 1;
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mtarif_bpjskesehatan', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mtarif_bpjskesehatan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
