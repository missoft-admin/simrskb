<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_ringkasan_pulang_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_ringkasan_pulang_setting_label(){
		$q="SELECT * FROM setting_ringkasan_pulang_label H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	public function get_ringkasan_pulang_setting(){
		$q="SELECT * FROM setting_ringkasan_pulang H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function save_ringkasan_pulang(){
		$id =1;
		// $this->judul_header = $this->input->post('judul_header');
		// $this->judul_footer = $this->input->post('judul_footer');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->lama_edit = $this->input->post('lama_edit');
		$this->orang_edit = $this->input->post('orang_edit');
		$this->st_hapus_catatan = $this->input->post('st_hapus_catatan');
		$this->lama_hapus = $this->input->post('lama_hapus');
		$this->orang_hapus = $this->input->post('orang_hapus');
		$this->st_duplikasi_catatan = $this->input->post('st_duplikasi_catatan');
		$this->lama_duplikasi = $this->input->post('lama_duplikasi');
		$this->orang_duplikasi = $this->input->post('orang_duplikasi');
		$this->judul_header_ina = $this->input->post('judul_header_ina');
		$this->judul_footer_ina = $this->input->post('judul_footer_ina');
		$this->judul_header_eng = $this->input->post('judul_header_eng');
		$this->judul_footer_eng = $this->input->post('judul_footer_eng');
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_ringkasan_pulang', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_ringkasan_pulang_label(){
		// print_r($this->input->post());exit;
		$id =1;
		$this->alamat_rs = $this->input->post('alamat_rs');
		$this->phone_rs = $this->input->post('phone_rs');
		$this->web_rs = $this->input->post('web_rs');
		$this->label_identitas_ina= $this->input->post('label_identitas_ina');
		$this->label_identitas_eng= $this->input->post('label_identitas_eng');
		$this->no_reg_ina= $this->input->post('no_reg_ina');
		$this->no_reg_eng= $this->input->post('no_reg_eng');
		$this->no_rm_ina= $this->input->post('no_rm_ina');
		$this->no_rm_eng= $this->input->post('no_rm_eng');
		$this->nama_pasien_ina= $this->input->post('nama_pasien_ina');
		$this->nama_pasien_eng= $this->input->post('nama_pasien_eng');
		$this->ttl_ina= $this->input->post('ttl_ina');
		$this->ttl_eng= $this->input->post('ttl_eng');
		$this->umur_ina= $this->input->post('umur_ina');
		$this->umur_eng= $this->input->post('umur_eng');
		$this->jk_ina= $this->input->post('jk_ina');
		$this->jk_eng= $this->input->post('jk_eng');
		$this->indikasi_rawat_ina= $this->input->post('indikasi_rawat_ina');
		$this->indikasi_rawat_eng= $this->input->post('indikasi_rawat_eng');
		$this->ringkasan_ina= $this->input->post('ringkasan_ina');
		$this->ringkasan_eng= $this->input->post('ringkasan_eng');
		$this->pemeriksaan_ina= $this->input->post('pemeriksaan_ina');
		$this->pemeriksaan_eng= $this->input->post('pemeriksaan_eng');
		$this->penunjang_ina= $this->input->post('penunjang_ina');
		$this->penunjang_eng= $this->input->post('penunjang_eng');
		$this->konsultasi_ina= $this->input->post('konsultasi_ina');
		$this->konsultasi_eng= $this->input->post('konsultasi_eng');
		$this->therapi_ina= $this->input->post('therapi_ina');
		$this->therapi_eng= $this->input->post('therapi_eng');
		$this->therapi_pulang_ina= $this->input->post('therapi_pulang_ina');
		$this->therapi_pulang_eng= $this->input->post('therapi_pulang_eng');
		$this->komplikasi_ina= $this->input->post('komplikasi_ina');
		$this->komplikasi_eng= $this->input->post('komplikasi_eng');
		$this->prognis_ina= $this->input->post('prognis_ina');
		$this->prognis_eng= $this->input->post('prognis_eng');
		$this->cara_pulang_ina= $this->input->post('cara_pulang_ina');
		$this->cara_pulang_eng= $this->input->post('cara_pulang_eng');
		$this->intuksi_fu_ina= $this->input->post('intuksi_fu_ina');
		$this->intuksi_fu_eng= $this->input->post('intuksi_fu_eng');
		$this->keterangan_ina= $this->input->post('keterangan_ina');
		$this->keterangan_eng= $this->input->post('keterangan_eng');
		$this->diagnosa_utama_ina= $this->input->post('diagnosa_utama_ina');
		$this->diagnosa_utama_eng= $this->input->post('diagnosa_utama_eng');
		$this->diagnosa_tambahan_ina= $this->input->post('diagnosa_tambahan_ina');
		$this->diagnosa_tambahan_eng= $this->input->post('diagnosa_tambahan_eng');
		$this->prosedure_ina= $this->input->post('prosedure_ina');
		$this->prosedure_eng= $this->input->post('prosedure_eng');
		$this->icd_10_ina= $this->input->post('icd_10_ina');
		$this->icd_10_eng= $this->input->post('icd_10_eng');
		$this->icd_9_ina= $this->input->post('icd_9_ina');
		$this->icd_9_eng= $this->input->post('icd_9_eng');
		$this->dpjp_ina= $this->input->post('dpjp_ina');
		$this->dpjp_eng= $this->input->post('dpjp_eng');
		$this->keluarga_ina= $this->input->post('keluarga_ina');
		$this->keluarga_eng= $this->input->post('keluarga_eng');
		$this->notes_ina= $this->input->post('notes_ina');
		$this->notes_eng= $this->input->post('notes_eng');
		$this->upload_login_logo(true);
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_ringkasan_pulang_label', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	public function upload_login_logo($update = false)
    {
        if (!file_exists('assets/upload/app_setting')) {
            mkdir('assets/upload/app_setting', 0755, true);
        }
        if (isset($_FILES['logo'])) {
		// print_r('sini');exit;
            if ($_FILES['logo']['name'] != '') {
                $config['upload_path'] = './assets/upload/app_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('logo')) {
                    $image_upload = $this->upload->data();
                    $this->logo = $image_upload['file_name'];
					
                    if ($update == true) {
                        // $this->remove_image_logo(1);
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
                return true;
            }
					// print_r($this->foto);exit;
        } else {
            return true;
        }
		
    }
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


