<?php

class Reservasi_poliklinik_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_dokter_poliklinik($idpoli)
    {
        $this->db->select('mdokter.id AS value, mdokter.nama AS label');
        $this->db->join('mdokter', 'mdokter.id = mjadwal_dokter.iddokter', 'LEFT');
        $this->db->where('mdokter.status', 1);
        $this->db->where('mjadwal_dokter.kuota >', 0);
        $this->db->where('mjadwal_dokter.idpoliklinik', $idpoli);
        $this->db->group_by('mjadwal_dokter.iddokter');
        $query = $this->db->get('mjadwal_dokter');

        return $query->result();
    }

    public function get_jadwal_dokter_poliklinik($idpoli, $iddokter)
    {
        $this->db->select("date_row.tanggal,
            (CASE
                WHEN mcuti_dokter.id IS NOT NULL THEN mcuti_dokter.sebab
                WHEN mholiday.id IS NOT NULL THEN 'LIBUR NASIONAL' ELSE ''
            END) AS sebab,
            (CASE
                WHEN mcuti_dokter.id IS NOT NULL THEN mcuti_dokter.id
                WHEN mholiday.id IS NOT NULL THEN mholiday.id ELSE ''
            END) cuti_id,
            (CASE
                WHEN mcuti_dokter.id IS NOT NULL THEN 'disabled'
                WHEN mholiday.id IS NOT NULL THEN 'disabled' ELSE ''
            END) st_disabel,
            CONCAT(merm_hari.namahari, ', ', DATE_FORMAT(date_row.tanggal, '%d-%m-%Y')) as tanggal_nama,
            date_row.hari AS kodehari,
            UPPER(SUBSTRING(merm_hari.namahari, 1, 3)) AS label_day,
            UPPER(DATE_FORMAT(date_row.tanggal, '%d %b')) AS label_date");
        $this->db->join('app_reservasi_tanggal', "app_reservasi_tanggal.tanggal = date_row.tanggal AND app_reservasi_tanggal.reservasi_tipe_id = '1' AND app_reservasi_tanggal.iddokter = '{$iddokter}' AND app_reservasi_tanggal.idpoli = '{$idpoli}' ", 'left');
        $this->db->join('mcuti_dokter', "mcuti_dokter.iddokter = app_reservasi_tanggal.iddokter AND app_reservasi_tanggal.tanggal BETWEEN mcuti_dokter.tanggal_dari AND mcuti_dokter.tanggal_sampai AND mcuti_dokter.status = '1'", 'left');
        $this->db->join('merm_hari', 'merm_hari.kodehari = date_row.hari', 'left');
        $this->db->join('mholiday', 'date_row.tanggal BETWEEN mholiday.tgl_libur_awal AND mholiday.tgl_libur_akhir', 'LEFT');
        $this->db->where('date_row.tanggal >= DATE(NOW())');
        $this->db->where('date_row.tanggal < DATE_ADD(DATE(NOW()), INTERVAL 7 DAY)');
        $this->db->group_by('date_row.tanggal');
        $query = $this->db->get('date_row');

        return $query->result();
    }

    public function get_jam_dokter_poliklinik($idpoli, $iddokter, $tanggal)
    {
        $this->db->select("app_reservasi_tanggal.jadwal_id,
            CONCAT(DATE_FORMAT(mjadwal_dokter.jam_dari, '%H:%i'), ' - ', DATE_FORMAT(mjadwal_dokter.jam_sampai, '%H:%i')) as jam_nama,
            app_reservasi_tanggal.saldo_kuota,
            (CASE
                WHEN app_reservasi_tanggal.saldo_kuota > 0 AND app_reservasi_tanggal.st_reservasi_online = 1 THEN ''
                ELSE 'disabled'
            END) as st_disabel,
            (CASE
                WHEN app_reservasi_tanggal.catatan != '' THEN app_reservasi_tanggal.catatan
                WHEN app_reservasi_tanggal.saldo_kuota <= 0 AND (app_reservasi_tanggal.catatan IS NULL OR app_reservasi_tanggal.catatan = '') THEN 'TIDAK TERSEDIA'
                WHEN app_reservasi_tanggal.st_reservasi_online = 0 AND (app_reservasi_tanggal.catatan IS NULL OR app_reservasi_tanggal.catatan = '') THEN 'TIDAK TERSEDIA'
                ELSE ''
            END) AS catatan");
        $this->db->join('mjadwal_dokter', 'mjadwal_dokter.id = app_reservasi_tanggal.jadwal_id', 'left');
        $this->db->where('app_reservasi_tanggal.reservasi_tipe_id', '1');
        $this->db->where('app_reservasi_tanggal.iddokter', $iddokter);
        $this->db->where('app_reservasi_tanggal.idpoli', $idpoli);
        $this->db->where('app_reservasi_tanggal.tanggal', $tanggal);
        $this->db->group_by('app_reservasi_tanggal.tanggal, app_reservasi_tanggal.jadwal_id');
        $query = $this->db->get('app_reservasi_tanggal');

        return $query->result();
    }

    public function get_pasien($akun_anggota_id)
    {
        $this->db->select('mfpasien.*');
        $this->db->join('merm_akun_anggota', 'merm_akun_anggota.pasien_id = mfpasien.id');
        $this->db->where('merm_akun_anggota.id', $akun_anggota_id);
        $query = $this->db->get('mfpasien');

        return $query->row();
    }

    public function get_informasi_reservasi($reservasi_id)
    {
        $this->db->select('app_reservasi_tanggal_pasien.id,
        DATE_FORMAT(app_reservasi_tanggal_pasien.tanggal, "%d %M %Y") AS tanggal_reservasi,
        CONCAT(DATE_FORMAT(mjadwal_dokter.jam_dari,"%H:%i"), " - ", DATE_FORMAT(mjadwal_dokter.jam_sampai, "%H:%i")) AS waktu_reservasi,
        mfpasien.no_medrec,
        app_reservasi_tanggal_pasien.title AS title_pasien,
        app_reservasi_tanggal_pasien.namapasien AS nama_pasien,
        app_reservasi_tanggal_pasien.tanggal_lahir,
        tpoliklinik_pendaftaran.noantrian,
        mpoliklinik.nama AS tujuan_poliklinik,
        CONCAT("'.base_url().'", "assets/upload/dokter/", mdokter.foto) AS foto_dokter,
        mdokter.nama AS nama_dokter,
        mdokter_kategori.nama AS kategori_dokter,
        app_reservasi_tanggal_pasien.idkelompokpasien,
        mpasien_kelompok.nama AS nama_kelompok_pasien,
        mrekanan.nama AS nama_asuransi,
        app_reservasi_tanggal_pasien.noantrian AS nomor_antrian,
        tpoliklinik_pendaftaran.noantrian AS nomor_antrian_pendaftaran,
        app_reservasi_tanggal_pasien.kode_booking,
        tpoliklinik_pendaftaran.nopendaftaran AS nomor_pendaftaran');
        $this->db->join('mjadwal_dokter', 'mjadwal_dokter.id = app_reservasi_tanggal_pasien.jadwal_id', 'LEFT');
        $this->db->join('mdokter', 'mdokter.id = app_reservasi_tanggal_pasien.iddokter', 'LEFT');
        $this->db->join('mdokter_kategori', 'mdokter_kategori.id = mdokter.idkategori', 'LEFT');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = app_reservasi_tanggal_pasien.idkelompokpasien', 'LEFT');
        $this->db->join('mrekanan', 'mrekanan.id = app_reservasi_tanggal_pasien.idrekanan', 'LEFT');
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.reservasi_id = app_reservasi_tanggal_pasien.id', 'LEFT');
        $this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->where('app_reservasi_tanggal_pasien.id', $reservasi_id);
        $query = $this->db->get('app_reservasi_tanggal_pasien');

        return $query->row();
    }

    public function post_reservasi_poli($dataReservasi)
    {
        if ($this->db->insert('app_reservasi_tanggal_pasien', $dataReservasi)) {
            return $this->db->insert_id();
        }

        return false;
    }

    public function get_general_consent($reservasi_id)
    {
        $this->db->select('*, jawaban_perssetujuan AS jawaban_persetujuan');
        $this->db->where('app_id', $reservasi_id);
        $query = $this->db->get('app_reservasi_tanggal_pasien_gc_head');
        $head = $query->row();

        $this->db->select('id, no, pertanyaan, jawaban_ttd');
        $this->db->where('app_id', $reservasi_id);
        $this->db->order_by('no', 'ASC');
        $query = $this->db->get('app_reservasi_tanggal_pasien_gc');
        $detail = $query->result();

        return [
            'head' => $head,
            'detail' => $detail,
        ];
    }

    public function get_skrining_pasien($reservasi_id)
    {
        $this->db->select('app_reservasi_tanggal_pasien_sp.id,
            app_reservasi_tanggal_pasien_sp.no,
            app_reservasi_tanggal_pasien_sp.pertanyaan,
            app_reservasi_tanggal_pasien_sp.group_jawaban_id,
            app_reservasi_tanggal_pasien_sp.group_jawaban_nama,
            app_reservasi_tanggal_pasien_sp.jenis_isi,
            merm_referensi.ref AS jawaban_nama,
            (CASE
                WHEN app_reservasi_tanggal_pasien_sp.jenis_isi = 1 THEN jawaban_id
                WHEN app_reservasi_tanggal_pasien_sp.jenis_isi = 2 THEN jawaban_freetext
                WHEN app_reservasi_tanggal_pasien_sp.jenis_isi = 3 THEN jawaban_ttd
            END) AS jawaban');
        $this->db->join('merm_referensi', 'merm_referensi.id = app_reservasi_tanggal_pasien_sp.jawaban_id', 'LEFT');
        $this->db->where('app_id', $reservasi_id);
        $this->db->order_by('no', 'ASC');
        $query = $this->db->get('app_reservasi_tanggal_pasien_sp');
        $result = $query->result();

        foreach ($result as &$row) {
            if (1 == $row->jenis_isi) {
                $options = [];
                $group_jawaban_id = explode(',', $row->group_jawaban_id);
                $group_jawaban_nama = explode(',', $row->group_jawaban_nama);

                for ($i = 0; $i < count($group_jawaban_id); ++$i) {
                    $option = [
                        'value' => $group_jawaban_id[$i],
                        'label' => $group_jawaban_nama[$i],
                    ];
                    $options[] = $option;
                }

                $row->options = $options;
            }
        }

        return $result;
    }

    public function get_skrining_covid($reservasi_id)
    {
        $this->db->select('app_reservasi_tanggal_pasien_sc.id,
            app_reservasi_tanggal_pasien_sc.no,
            app_reservasi_tanggal_pasien_sc.pertanyaan,
            app_reservasi_tanggal_pasien_sc.group_jawaban_id,
            app_reservasi_tanggal_pasien_sc.group_jawaban_nama,
            app_reservasi_tanggal_pasien_sc.jenis_isi,
            merm_referensi.ref AS jawaban_nama,
            (CASE
                WHEN app_reservasi_tanggal_pasien_sc.jenis_isi = 1 THEN jawaban_id
                WHEN app_reservasi_tanggal_pasien_sc.jenis_isi = 2 THEN jawaban_freetext
                WHEN app_reservasi_tanggal_pasien_sc.jenis_isi = 3 THEN jawaban_ttd
            END) AS jawaban');
        $this->db->join('merm_referensi', 'merm_referensi.id = app_reservasi_tanggal_pasien_sc.jawaban_id', 'LEFT');
        $this->db->where('app_id', $reservasi_id);
        $this->db->order_by('no', 'ASC');
        $query = $this->db->get('app_reservasi_tanggal_pasien_sc');
        $result = $query->result();

        foreach ($result as &$row) {
            if (1 == $row->jenis_isi) {
                $options = [];
                $group_jawaban_id = explode(',', $row->group_jawaban_id);
                $group_jawaban_nama = explode(',', $row->group_jawaban_nama);

                for ($i = 0; $i < count($group_jawaban_id); ++$i) {
                    $option = [
                        'value' => $group_jawaban_id[$i],
                        'label' => $group_jawaban_nama[$i],
                    ];
                    $options[] = $option;
                }

                $row->options = $options;
            }
        }

        return $result;
    }

    public function post_form_general_consent($gc_id, $jawaban)
    {
        $this->db->set('jawaban_ttd', $jawaban);
        $this->db->where('id', $gc_id);
        if ($this->db->update('app_reservasi_tanggal_pasien_gc')) {
            return true;
        }

        return false;
    }

    public function post_general_consent($dataGC)
    {
        $this->db->where('id', $dataGC['reservasi_id']);
        $this->db->update('app_reservasi_tanggal_pasien', [
            'st_general' => 1,
        ]);

        $this->db->where('app_id', $dataGC['reservasi_id']);
        $this->db->update('app_reservasi_tanggal_pasien_gc_head', [
            'jawaban_perssetujuan' => $dataGC['persetujuan'],
            'pasien_dapat_menandatangani' => $dataGC['pasien_dapat_menandatangani'],
            'siapa_yang_menandatangani' => $dataGC['siapa_yang_menandatangani'],
            'nama' => $dataGC['nama'],
            'hubungan' => $dataGC['hubungan'],
            'jawaban_ttd' => $dataGC['tanda_tangan'],
        ]);

        $this->db->where('app_id', $dataGC['reservasi_id']);
        $this->db->where('jawaban_ttd', 1);
        $this->db->update('app_reservasi_tanggal_pasien_gc', [
            'jawaban_ttd' => $dataGC['tanda_tangan'],
        ]);

        return true;
    }

    public function post_form_skrining_pasien($sp_id, $jenis_isi, $jawaban)
    {
        if ('1' == $jenis_isi) {
            $this->db->set('jawaban_id', $jawaban);
        }

        if ('2' == $jenis_isi) {
            $this->db->set('jawaban_freetext', $jawaban);
        }

        $this->db->where('id', $sp_id);
        if ($this->db->update('app_reservasi_tanggal_pasien_sp')) {
            return true;
        }

        return false;
    }

    public function post_skrining_pasien($dataSP)
    {
        $this->db->where('id', $dataSP['reservasi_id']);
        $this->db->update('app_reservasi_tanggal_pasien', [
            'st_skrining' => 1,
        ]);

        return true;
    }

    public function post_form_skrining_covid($sc_id, $jenis_isi, $jawaban)
    {
        if ('1' == $jenis_isi) {
            $this->db->set('jawaban_id', $jawaban);
        }

        if ('2' == $jenis_isi) {
            $this->db->set('jawaban_freetext', $jawaban);
        }

        $this->db->where('id', $sc_id);
        if ($this->db->update('app_reservasi_tanggal_pasien_sc')) {
            return true;
        }

        return false;
    }

    public function post_skrining_covid($dataSC)
    {
        $this->db->where('id', $dataSC['reservasi_id']);
        $this->db->update('app_reservasi_tanggal_pasien', [
            'st_covid' => 1,
        ]);

        return true;
    }

    public function check_if_registered($pasien_id, $poliklinik_id, $dokter_id, $tanggal, $jadwal_id)
    {
        $this->db->where('idpasien', $pasien_id);
        $this->db->where('idpoli', $poliklinik_id);
        $this->db->where('iddokter', $dokter_id);
        $this->db->where('tanggal', $tanggal);
        $this->db->where('jadwal_id', $jadwal_id);
        $query = $this->db->get('app_reservasi_tanggal_pasien');
        
        if ($query->num_rows() > 0) {
            return true;
        }

        return false;
    }
}
