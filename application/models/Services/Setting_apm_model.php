<?php

class Setting_apm_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_data_gallery()
    {
        $this->db->select("CONCAT('".base_url()."', 'assets/upload/apm_setting/', filename) AS file_url");
        $this->db->from('apm_setting_file');
        $this->db->where('status', 1);
        $query = $this->db->get();

        return $query->result();
    }

    public function get_setting_apm()
    {
        $this->db->select("
            judul_header AS header_title,
            judul_sub_header AS header_subtitle,
            CONCAT('".base_url()."', 'assets/upload/apm_setting/', header_logo) AS header_logo,
            bg_color AS background_color
        ");
        $this->db->from('apm_setting');
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->row();
    }

    public function get_setting_running_text()
    {
        $this->db->select("isi AS content");
        $this->db->from('apm_setting_running_text');
        $this->db->where('status', 1);
        $this->db->order_by('nourut', 'ASC');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_data_poliklinik($idpoliklinik, $iddokter)
    {
        $this->db->select('mpoliklinik.id AS value, mpoliklinik.nama AS label');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = mjadwal_dokter.idpoliklinik', 'LEFT');
        if (0 != $idpoliklinik) {
            $this->db->where('mpoliklinik.id', $idpoliklinik);
        }
        if (0 != $iddokter) {
            $this->db->where('mjadwal_dokter.iddokter', $iddokter);
        }
        $this->db->where('mjadwal_dokter.status', '1');
        $this->db->group_by('mjadwal_dokter.idpoliklinik');
        $query = $this->db->get('mjadwal_dokter');

        return $query->result();
    }

    public function get_pembayaran()
    {
        $this->db->select('mpasien_kelompok.id AS value,
            mpasien_kelompok.nama AS label');
        $this->db->from('mpasien_kelompok');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_asuransi()
    {
        $this->db->select('mrekanan.id AS value,
            mrekanan.nama AS label');
        $this->db->from('mrekanan');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_poliklinik()
    {
        $this->db->select('mpoliklinik.id AS value,
            mpoliklinik.nama AS label');
        $query = $this->db->get('mpoliklinik');

        return $query->result();
    }

    public function get_dokter()
    {
        $this->db->select('mdokter.id AS value,
            mdokter.nama AS label');
        $query = $this->db->get('mdokter');

        return $query->result();
    }
}
