<?php

class Antrian_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_antrian_seluruh_dokter()
    {
        $this->db->select("mtujuan.nama_tujuan,
            mdokter.nama AS nama_dokter, mtujuan.kode_antrian");
        $this->db->join("mdokter", "mdokter.id = mtujuan.iddokter");
        $this->db->where("mtujuan.jenis", "2");
        $this->db->where("mtujuan.status", "1");
        $this->db->where("mtujuan.antrian_tanggal", date("Y-m-d"));
        $query = $this->db->get('mtujuan');

        return $query->result();
    }

    public function get_antrian_berdasarkan_dokter()
    {
        $this->db->select('mdokter.nama AS nama_dokter,
            CONCAT("'.base_url().'", "assets/upload/dokter/", mdokter.foto) AS foto_dokter,
            mdokter_kategori.nama AS kategori_dokter,
            mtujuan.kode_antrian AS antrian_saat_ini,
            COUNT(noantrian_tmp.id) AS jumlah_antrian');
        $this->db->join("mdokter", "mdokter.id = mtujuan.iddokter");
        $this->db->join("mdokter_kategori", "mdokter_kategori.id = mdokter.idkategori");
        $this->db->join("noantrian_tmp", "noantrian_tmp.tanggal = mtujuan.antrian_tanggal AND
            noantrian_tmp.idpoli = mtujuan.idpoli AND
            noantrian_tmp.iddokter = mtujuan.iddokter");
        $this->db->where("mtujuan.antrian_tanggal", date("Y-m-d"));
        $this->db->group_by("mdokter.id");
        $query = $this->db->get('mtujuan');

        return $query->result();
    }

    public function get_antrian_penunjang()
    {
        $this->db->select("mtujuan.nama_tujuan,
            mtujuan.kode_antrian");
        $this->db->where("mtujuan.jenis", "1");
        $this->db->where("mtujuan.status", "1");
        $this->db->where("mtujuan.antrian_tanggal", date("Y-m-d"));
        $query = $this->db->get('mtujuan');

        return $query->result();
    }

    public function get_antrian_pendaftaran()
    {
        $this->db->select("antrian_pelayanan_hari.antrian_id,
            antrian_pelayanan.nama_pelayanan,
            antrian_pelayanan.kode,
            antrian_pelayanan.kuota,
            antrian_pelayanan.kodantrian_dilayani,
            MAX(antrian_harian.kodeantrian) AS kode_antrian_akhir,
            SUM(CASE WHEN antrian_harian.st_panggil = '0' THEN 1 ELSE 0 END) AS sisa_antrian,
            SUM(CASE WHEN antrian_harian.st_panggil IS NOT NULL THEN 1 ELSE 0 END) AS total_antrian,
            COALESCE(MAX(antrian_harian.noantrian),0) +1 AS antrian_ahir");
        $this->db->join("antrian_pelayanan_hari", "antrian_pelayanan_hari.kodehari = date_row.hari AND antrian_pelayanan_hari.status = 1");
        $this->db->join("antrian_pelayanan", "antrian_pelayanan.id = antrian_pelayanan_hari.antrian_id AND antrian_pelayanan.st_open = '1'");
        $this->db->join("antrian_harian", "antrian_harian.antrian_id = antrian_pelayanan.id AND antrian_harian.tanggal = date_row.tanggal");
        $this->db->where("date_row.tanggal = CURRENT_DATE()");
        $this->db->group_by("antrian_pelayanan.id");
        $query = $this->db->get('date_row');

        return $query->result();
    }
}
