<?php

class Riwayat_transaksi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function data_riwayat_transaksi($akun_id)
    {
        $this->db->select('app_reservasi_tanggal_pasien.id,
            DATE_FORMAT(app_reservasi_tanggal_pasien.tanggal, "%d %M %Y") AS tanggal_reservasi,
            app_reservasi_tanggal_pasien.reservasi_tipe_id AS jenis_reservasi_id,
            (CASE
                WHEN app_reservasi_tanggal_pasien.reservasi_tipe_id = "1" THEN
                    "POLIKLINIK"
                WHEN app_reservasi_tanggal_pasien.reservasi_tipe_id = "2" THEN
                    "REHABILITASI MEDIS"
            END) AS jenis_reservasi,
            (CASE
                WHEN app_reservasi_tanggal_pasien.reservasi_tipe_id = "1" THEN
                    CONCAT(DATE_FORMAT(mjadwal_dokter.jam_dari,"%H:%i"), " - ", DATE_FORMAT(mjadwal_dokter.jam_sampai, "%H:%i"))
                WHEN app_reservasi_tanggal_pasien.reservasi_tipe_id = "2" THEN
                    CONCAT(merm_jam.jam, ".00 - ", merm_jam.jam_akhir, ".00" )
            END) AS waktu_reservasi,
            CONCAT(app_reservasi_tanggal_pasien.title, " ", app_reservasi_tanggal_pasien.namapasien) AS nama_pasien,
            CONCAT("'.base_url().'", "assets/upload/dokter/", mdokter.foto) AS foto_dokter,
            mdokter.nama AS nama_dokter,
            mdokter_kategori.nama AS kategori_dokter,
            mpasien_kelompok.nama AS nama_kelompok_pasien,
            mrekanan.nama AS nama_asuransi,
            app_reservasi_tanggal_pasien.noantrian AS nomor_antrian,
            app_reservasi_tanggal_pasien.kode_booking,
            app_reservasi_tanggal_pasien.status_reservasi,
            (CASE
                WHEN app_reservasi_tanggal_pasien.idkelompokpasien != "5" THEN "Pembayaran Asuransi" ELSE "Pembayaran"
            END) AS jenis_pembayaran,
            (CASE
                WHEN app_reservasi_tanggal_pasien.status_reservasi = 0 THEN "Hapus"
                WHEN app_reservasi_tanggal_pasien.status_reservasi = 1 THEN "Belum Verifikasi"
                WHEN app_reservasi_tanggal_pasien.status_reservasi = 2 THEN "Sudah Verifikasi"
                WHEN app_reservasi_tanggal_pasien.status_reservasi = 3 THEN "Datang"
            END) AS status_reservasi_label');
        $this->db->join('app_reservasi_tanggal_pasien', 'app_reservasi_tanggal_pasien.idpasien = merm_akun_anggota.pasien_id');
        $this->db->join('mjadwal_dokter', 'mjadwal_dokter.id = app_reservasi_tanggal_pasien.jadwal_id', 'LEFT');
        $this->db->join('merm_jam', 'merm_jam.jam_id = app_reservasi_tanggal_pasien.jam_id', 'LEFT');
        $this->db->join('mdokter', 'mdokter.id = app_reservasi_tanggal_pasien.iddokter', 'LEFT');
        $this->db->join('mdokter_kategori', 'mdokter_kategori.id = mdokter.idkategori', 'LEFT');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = app_reservasi_tanggal_pasien.idkelompokpasien');
        $this->db->join('mrekanan', 'mrekanan.id = app_reservasi_tanggal_pasien.idrekanan', 'LEFT');
        $this->db->where('merm_akun_anggota.akun_id', $akun_id);
        $this->db->order_by('app_reservasi_tanggal_pasien.tanggal', 'DESC');
        $query = $this->db->get('merm_akun_anggota');

        return $query->result();
    }
}
