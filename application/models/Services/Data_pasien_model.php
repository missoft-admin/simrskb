<?php

class Data_pasien_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function title_pasien()
    {
        $this->db->select('singkatan AS value, singkatan AS label');
        $query = $this->db->get('mpasien_title');

        return $query->result();
    }

    public function search_pasien($nomedrec, $tanggalLahir)
    {
        $this->db->select('mfpasien.id,
            mfpasien.nama,
            mfpasien.no_medrec,
            DATE(mfpasien.tanggal_lahir) AS tanggal_lahir,
            mfpasien.hubungan_dengan_pasien,
            merm_akun_anggota.id AS akun_id');
        $this->db->from('mfpasien');
        $this->db->join('merm_akun_anggota', 'merm_akun_anggota.pasien_id = mfpasien.id', 'LEFT');
        $this->db->where('mfpasien.no_medrec', $nomedrec);
        $this->db->where('DATE(mfpasien.tanggal_lahir)', $tanggalLahir);
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->row();
    }
}
