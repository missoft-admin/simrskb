<?php

class Data_akun_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function registrasi_akun($data)
    {
        if ($this->db->insert('merm_akun', $data)) {
            $akun_id = $this->db->insert_id();

            // # Proses insert ke mfpasien
            $pasien_id = $this->registrasi_pasien($data);

            // # Proses insert ke makun_anggota
            $this->registrasi_anggota($akun_id, $pasien_id, $data);

            return $akun_id;
        }

        return false;
    }

    public function registrasi_pasien_apm($data)
    {
        // # Proses insert ke mfpasien
        $pasien_id = $this->registrasi_pasien($data);
        if ($pasien_id) {
            return $pasien_id;
        } else {
            return false;
        }
    }

    public function registrasi_pasien($data)
    {
        $nomedrec = $this->get_nomedrec();

        $dataPasien = [];
        $dataPasien['no_medrec'] = $nomedrec;
        $dataPasien['title'] = $data['title'];
        $dataPasien['nama'] = $data['nama'];
        $dataPasien['jenis_kelamin'] = $data['jenis_kelamin'];
        $dataPasien['alamat_jalan'] = $data['alamat'];
        $dataPasien['provinsi_id'] = $data['provinsi_id'];
        $dataPasien['kabupaten_id'] = $data['kabupaten_id'];
        $dataPasien['kecamatan_id'] = $data['kecamatan_id'];
        $dataPasien['kelurahan_id'] = $data['kelurahan_id'];
        $dataPasien['telepon'] = $data['telepon'];
        // $dataPasien['email'] = $data['email'];
        $dataPasien['tempat_lahir'] = $data['tempat_lahir'];
        $dataPasien['tanggal_lahir'] = $data['tanggal_lahir'];

        // $dataPasien['umur_tahun'] = $data['umur_tahun'];
        // $dataPasien['umur_bulan'] = $data['umur_bulan'];
        // $dataPasien['umur_hari'] = $data['umur_hari'];

        $dataPasien['golongan_darah'] = $data['golongan_darah'];
        $dataPasien['agama_id'] = $data['agama_id'];
        $dataPasien['warganegara'] = $data['warganegara'];
        $dataPasien['suku'] = $data['suku'];
        $dataPasien['status_kawin'] = $data['status_kawin'];
        $dataPasien['pendidikan_id'] = $data['pendidikan_id'];
        $dataPasien['pekerjaan_id'] = $data['pekerjaan_id'];
        $dataPasien['nama_keluarga'] = $data['nama_keluarga'];
        $dataPasien['hubungan_dengan_pasien'] = $data['hubungan_dengan_pasien'];
        $dataPasien['alamat_keluarga'] = $data['alamat_keluarga'];
        $dataPasien['telepon_keluarga'] = $data['telepon_keluarga'];

        $dataPasien['created'] = date('Y-m-d H:i:s');

        if ($this->db->replace('mfpasien', $dataPasien)) {
            return $this->db->insert_id();
        }
    }

    public function registrasi_anggota($akun_id, $pasien_id, $data, $isAnggota = false)
    {
        $dataAnggota = [
            'akun_id' => $akun_id,
            'pasien_id' => $pasien_id,
            'foto' => '',
            'nama' => $data['nama'],
            'keterangan' => $isAnggota ? $data['hubungan_dengan_pasien'] : 'Saya Sendiri',
        ];

        if ($this->db->insert('merm_akun_anggota', $dataAnggota)) {
            return true;
        }

        return false;
    }

    public function tambah_anggota_baru($data)
    {
        $akun_id = $data['parent_akun_id'];

        // # Proses insert ke mfpasien
        $pasien_id = $this->registrasi_pasien($data);

        // # Proses insert ke makun_anggota
        if ($this->registrasi_anggota($akun_id, $pasien_id, $data, true)) {
            return true;
        }

        return false;
    }

    public function tambah_anggota_lama($data)
    {
        // # Proses insert ke makun_anggota
        if ($this->registrasi_anggota($data['akun_id'], $data['pasien_id'], $data, true)) {
            return true;
        }

        return false;
    }

    public function login($email, $password)
    {
        $this->db->select('id, nama, tanggal_lahir, email');
        $this->db->from('merm_akun');
        $this->db->where('email', $email);
        $this->db->where('password', md5($password));
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() >= 0) {
            return $query->row();
        }

        return false;
    }

    public function verifikasi_akun($akun_id, $kode_verifikasi)
    {
        $this->db->from('merm_akun');
        $this->db->where('id', $akun_id);
        $this->db->where('kode_verifikasi', $kode_verifikasi);
        $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row();

        if ($query->num_rows() >= 0) {
            $this->db->update('merm_akun', ['status_verifikasi' => 1], ['id' => $akun_id]);
        }

        return $row;
    }

    public function get_nomedrec()
    {
        $nomedrec = '';
        $init = 'H'.date('y');
        $query = $this->db->query('SELECT no_medrec FROM mfpasien WHERE no_medrec IS NOT NULL ORDER BY id DESC LIMIT 1');

        if ($row = $query->row()) {
            $hasil = $row->no_medrec;
			$panjang = strlen($hasil);
			$hasil = substr($hasil, 3, ($panjang - 3));
			$panjang = strlen($hasil);
			$hasil = $hasil + 1;
			$nomedrec = str_repeat('0', $panjang);
			$hasil = $init . substr($nomedrec, 0, $panjang - strlen($hasil)) . $hasil;
        } else {
            $hasil = $init . '000001';
        }

        return $hasil;
    }

    public function get_akun_anggota($akun_id)
    {
        $this->db->from('merm_akun_anggota');
        $this->db->where('akun_id', $akun_id);
        $query = $this->db->get();

        return $query->result();
    }

    public function get_detail_akun_anggota($akun_anggota_id)
    {
        $this->db->select('mfpasien.nama, mfpasien.no_medrec, DATE(mfpasien.tanggal_lahir) AS tanggal_lahir');
        $this->db->from('merm_akun_anggota');
        $this->db->join('mfpasien', 'mfpasien.id = merm_akun_anggota.pasien_id');
        $this->db->where('merm_akun_anggota.id', $akun_anggota_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function get_detail_akun($akun_id)
    {
        $this->db->select('id, nama, email, kode_verifikasi');
        $this->db->from('merm_akun');
        $this->db->where('id', $akun_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function ubah_akun($data_akun)
    {
        $data = [
            'nama' => $data_akun['nama'],
            'email' => $data_akun['email'],
        ];

        $this->db->where('id', $data_akun['id']);
        if ($this->db->update('merm_akun', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function check_password_valid($akun_id, $password)
    {
        $this->db->where('id', $akun_id);
        $this->db->where('password', md5($password));
        $this->db->limit(1);
        $query = $this->db->get('merm_akun');

        if ($query->num_rows() > 0) {
            return true;
        }

        return false;
    }

    public function ubah_password($data_akun)
    {   
        if ($this->check_password_valid($data_akun['id'], $data_akun['password_lama'])) {
            $data = [
                'password' => md5($data_akun['password_baru']),
            ];
    
            $this->db->where('id', $data_akun['id']);
            if ($this->db->update('merm_akun', $data)) {
                return 'success';
            } else {
                return 'error';
            }
        } else {
            return 'old_password_not_match';
        }
    }
}
