<?php

class Reservasi_rehabilitasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_jadwal_rehab($idpoli)
    {
        $this->db->select("date_row.tanggal,
            CONCAT(merm_hari.namahari, ', ', DATE_FORMAT(date_row.tanggal, '%d-%m-%Y')) as tanggal_nama,
            date_row.hari AS kodehari,
            UPPER(SUBSTRING(merm_hari.namahari, 1, 3)) AS label_day,
            UPPER(DATE_FORMAT(date_row.tanggal, '%d %b')) AS label_date");
        $this->db->join('app_reservasi_tanggal', "app_reservasi_tanggal.tanggal = date_row.tanggal AND app_reservasi_tanggal.reservasi_tipe_id = '2' AND app_reservasi_tanggal.idpoli = '{$idpoli}' ", 'left');
        $this->db->join('merm_hari', 'merm_hari.kodehari = date_row.hari', 'left');
        $this->db->where('date_row.tanggal >= DATE(NOW())');
        $this->db->where('date_row.tanggal < DATE_ADD(DATE(NOW()), INTERVAL 7 DAY)');
        $this->db->group_by('date_row.tanggal');
        $query = $this->db->get('date_row');

        return $query->result();
    }

    public function get_jam_rehab($idpoli, $tanggal)
    {
        $this->db->select("app_reservasi_tanggal.jam_id,
            CONCAT(merm_jam.jam, '.00 - ', merm_jam.jam_akhir, '.00' ) AS jam_nama,
            app_reservasi_tanggal.saldo_kuota,
            (CASE
                WHEN app_reservasi_tanggal.saldo_kuota > 0 AND app_reservasi_tanggal.st_reservasi_online = 1 THEN ''
                ELSE 'disabled'
            END) AS st_disabel,
            (CASE
                WHEN app_reservasi_tanggal.catatan != '' THEN app_reservasi_tanggal.catatan
                WHEN app_reservasi_tanggal.saldo_kuota <= 0 AND (app_reservasi_tanggal.catatan IS NULL OR app_reservasi_tanggal.catatan = '') THEN 'TIDAK TERSEDIA'
                WHEN app_reservasi_tanggal.st_reservasi_online = 0 AND (app_reservasi_tanggal.catatan IS NULL OR app_reservasi_tanggal.catatan = '') THEN 'TIDAK TERSEDIA'
                ELSE ''
            END) AS catatan");
        $this->db->join('merm_jam', 'merm_jam.jam_id = app_reservasi_tanggal.jam_id', 'left');
        $this->db->where('app_reservasi_tanggal.reservasi_tipe_id', '2');
        $this->db->where('app_reservasi_tanggal.idpoli', $idpoli);
        $this->db->where('app_reservasi_tanggal.tanggal', $tanggal);
        $this->db->where('app_reservasi_tanggal.saldo_kuota IS NOT NULL');
        $this->db->group_by('app_reservasi_tanggal.tanggal, app_reservasi_tanggal.jam_id');
        $query = $this->db->get('app_reservasi_tanggal');

        return $query->result();
    }

    public function get_pasien($akun_anggota_id)
    {
        $this->db->select('mfpasien.*');
        $this->db->join('merm_akun_anggota', 'merm_akun_anggota.pasien_id = mfpasien.id');
        $this->db->where('merm_akun_anggota.id', $akun_anggota_id);
        $query = $this->db->get('mfpasien');

        return $query->row();
    }

    public function get_informasi_reservasi($reservasi_id)
    {
        $this->db->select('app_reservasi_tanggal_pasien.id,
        DATE_FORMAT(app_reservasi_tanggal_pasien.tanggal, "%d %M %Y") AS tanggal_reservasi,
        CONCAT(merm_jam.jam, ".00 - ", merm_jam.jam_akhir, ".00" ) AS waktu_reservasi,
        mfpasien.no_medrec,
        app_reservasi_tanggal_pasien.title AS title_pasien,
        app_reservasi_tanggal_pasien.namapasien AS nama_pasien,
        app_reservasi_tanggal_pasien.tanggal_lahir,
        tpoliklinik_pendaftaran.noantrian,
        mpoliklinik.nama AS tujuan_poliklinik,
        CONCAT("'.base_url().'", "assets/upload/dokter/", mdokter.foto) AS foto_dokter,
        mdokter.nama AS nama_dokter,
        mdokter_kategori.nama AS kategori_dokter,
        app_reservasi_tanggal_pasien.idkelompokpasien,
        mpasien_kelompok.nama AS nama_kelompok_pasien,
        mrekanan.nama AS nama_asuransi,
        app_reservasi_tanggal_pasien.noantrian AS nomor_antrian,
        tpoliklinik_pendaftaran.noantrian AS nomor_antrian_pendaftaran,
        app_reservasi_tanggal_pasien.kode_booking,
        tpoliklinik_pendaftaran.nopendaftaran AS nomor_pendaftaran');
        $this->db->join('merm_jam', 'merm_jam.jam_id = app_reservasi_tanggal_pasien.jam_id', 'LEFT');
        $this->db->join('mdokter', 'mdokter.id = app_reservasi_tanggal_pasien.iddokter', 'LEFT');
        $this->db->join('mdokter_kategori', 'mdokter_kategori.id = mdokter.idkategori', 'LEFT');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = app_reservasi_tanggal_pasien.idkelompokpasien');
        $this->db->join('mrekanan', 'mrekanan.id = app_reservasi_tanggal_pasien.idrekanan', 'LEFT');
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.reservasi_id = app_reservasi_tanggal_pasien.id', 'LEFT');
        $this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->where('app_reservasi_tanggal_pasien.id', $reservasi_id);
        $query = $this->db->get('app_reservasi_tanggal_pasien');

        return $query->row();
    }

    public function post_reservasi_rehab($dataReservasi)
    {
        if ($this->db->insert('app_reservasi_tanggal_pasien', $dataReservasi)) {
            return $this->db->insert_id();
        }

        return false;
    }

    public function check_if_registered($pasien_id, $poliklinik_id, $tanggal, $jam_id)
    {
        $this->db->where('idpasien', $pasien_id);
        $this->db->where('idpoli', $poliklinik_id);
        $this->db->where('tanggal', $tanggal);
        $this->db->where('jam_id', $jam_id);
        $query = $this->db->get('app_reservasi_tanggal_pasien');
        
        if ($query->num_rows() > 0) {
            return true;
        }

        return false;
    }
}
