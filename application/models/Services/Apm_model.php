<?php

class Apm_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function search_booking_code($booking_code)
    {
        $this->db->select('app_reservasi_tanggal_pasien.*,
            merm_akun_anggota.id AS akun_id,
            CONCAT("'.base_url().'", "assets/upload/foto_kartu/", app_reservasi_tanggal_pasien.foto_kartu) AS foto_kartu_asuransi,
            (CASE
                WHEN app_reservasi_tanggal_pasien.reservasi_cara = 1 THEN "Online"
                WHEN app_reservasi_tanggal_pasien.reservasi_cara = 2 THEN "Web"
                WHEN app_reservasi_tanggal_pasien.reservasi_cara = 3 THEN "FO : Whatsapp"
                WHEN app_reservasi_tanggal_pasien.reservasi_cara = 4 THEN "FO : Phone"
                WHEN app_reservasi_tanggal_pasien.reservasi_cara = 5 THEN "Datang Langsung"
                WHEN app_reservasi_tanggal_pasien.reservasi_cara = 6 THEN "APM"
            END) AS jenis_reservasi,
            (CASE
                WHEN app_reservasi_tanggal_pasien.statuspasienbaru = 0 THEN "Pasien Lama"
                WHEN app_reservasi_tanggal_pasien.statuspasienbaru = 1 THEN "Pasien Baru"
            END) AS status_pasien,
            IF(DATE(NOW()) > DATE(app_reservasi_tanggal_pasien.tanggal), "true", "false") status_expired
        ');
        $this->db->join('merm_akun_anggota', 'merm_akun_anggota.pasien_id = app_reservasi_tanggal_pasien.idpasien', 'LEFT');
        $this->db->where('app_reservasi_tanggal_pasien.kode_booking', $booking_code);
        $query = $this->db->get('app_reservasi_tanggal_pasien');

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function get_pasien($pasien_id)
    {
        $this->db->where('id', $pasien_id);
        $query = $this->db->get('mfpasien');

        return $query->row();
    }

    public function get_informasi_pendaftaran($pendaftaran_id)
    {
        $this->db->select('tpoliklinik_pendaftaran.id,
        mfpasien.no_medrec,
        tpoliklinik_pendaftaran.title AS title_pasien,
        tpoliklinik_pendaftaran.namapasien AS nama_pasien,
        tpoliklinik_pendaftaran.tanggal_lahir,
        mpoliklinik.nama AS tujuan_poliklinik,
        CONCAT("'.base_url().'", "assets/upload/dokter/", mdokter.foto) AS foto_dokter,
        mdokter.nama AS nama_dokter,
        mdokter_kategori.nama AS kategori_dokter,
        tpoliklinik_pendaftaran.idkelompokpasien,
        mpasien_kelompok.nama AS nama_kelompok_pasien,
        mrekanan.nama AS nama_asuransi,
        tpoliklinik_pendaftaran.noantrian AS nomor_antrian,
        tpoliklinik_pendaftaran.noantrian AS nomor_antrian_pendaftaran,
        tpoliklinik_pendaftaran.nopendaftaran AS nomor_pendaftaran');
        $this->db->join('mdokter', 'mdokter.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
        $this->db->join('mdokter_kategori', 'mdokter_kategori.id = mdokter.idkategori', 'LEFT');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
        $this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
        $this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->where('tpoliklinik_pendaftaran.id', $pendaftaran_id);
        $query = $this->db->get('tpoliklinik_pendaftaran');

        return $query->row();
    }

    public function post_pendaftaran_poli($data_pendaftaran)
    {
        if ($this->db->insert('tpoliklinik_pendaftaran', $data_pendaftaran)) {
            return $this->db->insert_id();
        }

        return false;
    }

    public function get_general_consent($pendaftaran_id)
    {
        $this->db->select('tpoliklinik_pendaftaran_gc_head.*, 
            tpoliklinik_pendaftaran_gc_head.jawaban_perssetujuan AS jawaban_persetujuan');
        $this->db->where('tpoliklinik_pendaftaran_gc_head.pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get('tpoliklinik_pendaftaran_gc_head');
        $head = $query->row();

        $this->db->select('tpoliklinik_pendaftaran_gc.id, 
            tpoliklinik_pendaftaran_gc.no, tpoliklinik_pendaftaran_gc.pertanyaan, 
            tpoliklinik_pendaftaran_gc.jawaban_ttd');
        $this->db->where('tpoliklinik_pendaftaran_gc.pendaftaran_id', $pendaftaran_id);
        $this->db->order_by('tpoliklinik_pendaftaran_gc.no', 'ASC');
        $query = $this->db->get('tpoliklinik_pendaftaran_gc');
        $detail = $query->result();

        return [
            'head' => $head,
            'detail' => $detail,
        ];
    }

    public function get_skrining_pasien($pendaftaran_id)
    {
        $this->db->select("tpoliklinik_pendaftaran_sp.id,
            tpoliklinik_pendaftaran_sp.no,
            tpoliklinik_pendaftaran_sp.pertanyaan,
            tpoliklinik_pendaftaran_sp.group_jawaban_id,
            tpoliklinik_pendaftaran_sp.group_jawaban_nama,
            tpoliklinik_pendaftaran_sp.jenis_isi,
            merm_referensi.ref AS jawaban_nama,
            (CASE
                WHEN tpoliklinik_pendaftaran_sp.jenis_isi = 1 THEN jawaban_id
                WHEN tpoliklinik_pendaftaran_sp.jenis_isi = 2 THEN jawaban_freetext
                WHEN tpoliklinik_pendaftaran_sp.jenis_isi = 3 THEN jawaban_ttd
            END) AS jawaban");
        $this->db->join('merm_referensi', "merm_referensi.id = tpoliklinik_pendaftaran_sp.jawaban_id", 'LEFT');
        $this->db->where('tpoliklinik_pendaftaran_sp.pendaftaran_id', $pendaftaran_id);
        $this->db->order_by('no', 'ASC');
        $query = $this->db->get('tpoliklinik_pendaftaran_sp');
        $result = $query->result();

        foreach ($result as &$row) {
            if ('1' == $row->jenis_isi) {
                $options = [];
                $group_jawaban_id = explode(',', $row->group_jawaban_id);
                $group_jawaban_nama = explode(',', $row->group_jawaban_nama);

                for ($i = 0; $i < count($group_jawaban_id); ++$i) {
                    $option = [
                        'value' => $group_jawaban_id[$i],
                        'label' => $group_jawaban_nama[$i],
                    ];
                    $options[] = $option;
                }

                $row->options = $options;
            }
        }

        return $result;
    }

    public function get_skrining_covid($pendaftaran_id)
    {
        $this->db->select("tpoliklinik_pendaftaran_sc.id,
            tpoliklinik_pendaftaran_sc.no,
            tpoliklinik_pendaftaran_sc.pertanyaan,
            tpoliklinik_pendaftaran_sc.group_jawaban_id,
            tpoliklinik_pendaftaran_sc.group_jawaban_nama,
            tpoliklinik_pendaftaran_sc.jenis_isi,
            merm_referensi.ref AS jawaban_nama,
            (CASE
                WHEN tpoliklinik_pendaftaran_sc.jenis_isi = 1 THEN jawaban_id
                WHEN tpoliklinik_pendaftaran_sc.jenis_isi = 2 THEN jawaban_freetext
                WHEN tpoliklinik_pendaftaran_sc.jenis_isi = 3 THEN jawaban_ttd
            END) AS jawaban");
        $this->db->join('merm_referensi', "merm_referensi.id = tpoliklinik_pendaftaran_sc.jawaban_id", 'LEFT');
        $this->db->where('tpoliklinik_pendaftaran_sc.pendaftaran_id', $pendaftaran_id);
        $this->db->order_by('no', 'ASC');
        $query = $this->db->get('tpoliklinik_pendaftaran_sc');
        $result = $query->result();

        foreach ($result as &$row) {
            if ('1' == $row->jenis_isi) {
                $options = [];
                $group_jawaban_id = explode(',', $row->group_jawaban_id);
                $group_jawaban_nama = explode(',', $row->group_jawaban_nama);

                for ($i = 0; $i < count($group_jawaban_id); ++$i) {
                    $option = [
                        'value' => $group_jawaban_id[$i],
                        'label' => $group_jawaban_nama[$i],
                    ];
                    $options[] = $option;
                }

                $row->options = $options;
            }
        }

        return $result;
    }

    public function post_form_general_consent($gc_id, $jawaban)
    {
        $this->db->set('jawaban_ttd', $jawaban);
        $this->db->where('id', $gc_id);

        if ($this->db->update('tpoliklinik_pendaftaran_gc')) {
            return true;
        }

        return false;
    }

    public function post_general_consent($dataGC)
    {
        $this->db->where('id', $dataGC['pendaftaran_id']);
        $this->db->update('tpoliklinik_pendaftaran', [
            'st_general' => 1,
        ]);

        $this->db->where('pendaftaran_id', $dataGC['pendaftaran_id']);
        $this->db->update('tpoliklinik_pendaftaran_gc_head', [
            'jawaban_perssetujuan' => $dataGC['persetujuan'],
            'pasien_dapat_menandatangani' => $dataGC['pasien_dapat_menandatangani'],
            'siapa_yang_menandatangani' => $dataGC['siapa_yang_menandatangani'],
            'nama' => $dataGC['nama'],
            'hubungan' => $dataGC['hubungan'],
        ]);

        $this->db->where('pendaftaran_id', $dataGC['pendaftaran_id']);
        $this->db->where('jawaban_ttd', 1);
        $this->db->update('tpoliklinik_pendaftaran_gc', [
            'jawaban_ttd' => $dataGC['tanda_tangan'],
        ]);

        return true;
    }

    public function post_form_skrining_pasien($sp_id, $jenis_isi, $jawaban)
    {
        if ('1' === $jenis_isi) {
            $this->db->set('jawaban_id', $jawaban);
        }

        if ('2' === $jenis_isi) {
            $this->db->set('jawaban_freetext', $jawaban);
        }

        $this->db->where('id', $sp_id);
        if ($this->db->update('tpoliklinik_pendaftaran_sp')) {
            return true;
        }

        return false;
    }

    public function post_skrining_pasien($dataSP)
    {
        $this->db->where('id', $dataSP['pendaftaran_id']);
        $this->db->update('tpoliklinik_pendaftaran', [
            'st_skrining' => 1,
        ]);

        return true;
    }

    public function post_form_skrining_covid($sc_id, $jenis_isi, $jawaban)
    {
        if ('1' === $jenis_isi) {
            $this->db->set('jawaban_id', $jawaban);
        }

        if ('2' === $jenis_isi) {
            $this->db->set('jawaban_freetext', $jawaban);
        }

        $this->db->where('id', $sc_id);
        if ($this->db->update('tpoliklinik_pendaftaran_sc')) {
            return true;
        }

        return false;
    }

    public function post_skrining_covid($dataSC)
    {
        $this->db->where('id', $dataSC['pendaftaran_id']);
        $this->db->update('tpoliklinik_pendaftaran', [
            'st_covid' => 1,
        ]);

        return true;
    }

    public function check_if_registered($pasien_id, $poliklinik_id, $dokter_id, $tanggal, $jadwal_id)
    {
        $this->db->where('idpasien', $pasien_id);
        $this->db->where('idpoliklinik', $poliklinik_id);
        $this->db->where('iddokter', $dokter_id);
        $this->db->where('tanggal', $tanggal);
        $this->db->where('jadwal_id', $jadwal_id);
        $query = $this->db->get('tpoliklinik_pendaftaran+');
        
        if ($query->num_rows() > 0) {
            return true;
        }

        return false;
    }

    public function post_checkin($data)
    {
        $this->db->where('id', $data['reservasi_id']);
        $this->db->update('app_reservasi_tanggal_pasien', [
            'status_reservasi' => 3,
        ]);

        return true;
    }
}
