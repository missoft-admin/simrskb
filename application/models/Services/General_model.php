<?php

class General_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function data_referensi($ref_head_id)
    {
        $this->db->select('nilai AS value, ref AS label, st_default AS is_default');
        $this->db->where('ref_head_id', $ref_head_id);
        $this->db->where('status', 1);
        $query = $this->db->get('merm_referensi');

        return $query->result();
    }
}
