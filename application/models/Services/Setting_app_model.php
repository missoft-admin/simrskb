<?php

class Setting_app_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_data_gallery()
    {
        $this->db->select("CONCAT('".base_url()."', 'assets/upload/app_setting/', filename) AS file_url");
        $this->db->from('app_setting_file');
        $this->db->where('status', 1);
        $query = $this->db->get();

        return $query->result();
    }

    public function get_data_event()
    {
        $this->db->select("id,
            judul, CONCAT('".base_url()."', 'assets/upload/app_setting/', gambar) AS file_url,
            isi, date_start, date_end, published_date, IF(DATE(date_end) <= DATE(NOW()), 'not_active', 'active') AS status");
        $this->db->from('app_event');
        $this->db->where('status', 2);
        $this->db->or_where('status', 3);
        $query = $this->db->get();

        return $query->result();
    }

    public function get_data_event_detail($event_id)
    {
        $this->db->select("id,
            judul, CONCAT('".base_url()."', 'assets/upload/app_setting/', gambar) AS file_url,
            isi, date_start, date_end, published_date, IF(DATE(date_end) <= DATE(NOW()), 'not_active', 'active') AS status");
        $this->db->from('app_event');
        $this->db->where('id', $event_id);
        $this->db->where('status', 2);
        $this->db->or_where('status', 3);
        $query = $this->db->get();

        return $query->row();
    }

    public function get_data_informasi()
    {
        $this->db->select("id,
            judul, CONCAT('".base_url()."', 'assets/upload/app_setting/', gambar) AS file_url, isi, date_start, date_end, published_date,
            IF(DATE(date_end) <= DATE(NOW()), 'not_active', 'active') AS status");
        $this->db->from('app_informasi');
        $this->db->where('status', 2);
        $this->db->or_where('status', 3);
        $this->db->order_by('published_date', 'DESC');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_data_informasi_detail($informasi_id)
    {
        $this->db->select("id,
            judul, CONCAT('".base_url()."', 'assets/upload/app_setting/', gambar) AS file_url, isi, date_start, date_end, published_date,
            IF(DATE(date_end) <= DATE(NOW()), 'not_active', 'active') AS status");
        $this->db->from('app_informasi');
        $this->db->where('id', $informasi_id);
        $this->db->where('status', 2);
        $this->db->or_where('status', 3);
        $this->db->order_by('published_date', 'DESC');
        $query = $this->db->get();

        return $query->row();
    }

    public function get_setting_app()
    {
        $this->db->select("
            login_judul AS message_greetings_login,
            index_pesan AS message_greetings_dashboard,
            login_salah AS message_signin_failed,
            CONCAT('".base_url()."', 'assets/upload/app_setting/', login_logo) AS login_logo,
            CONCAT('".base_url()."', 'assets/upload/app_setting/', index_logo) AS index_logo,
            CONCAT('".base_url()."', 'assets/upload/app_setting/', index_icon_1) AS icon_queue,
            CONCAT('".base_url()."', 'assets/upload/app_setting/', index_icon_2) AS icon_registration,
            CONCAT('".base_url()."', 'assets/upload/app_setting/', index_icon_3) AS icon_schedule,
            CONCAT('".base_url()."', 'assets/upload/app_setting/', index_icon_4) AS icon_transaction,
            CONCAT('".base_url()."', 'assets/upload/app_setting/', index_icon_5) AS icon_rehabilitation,
            CONCAT('".base_url()."', 'assets/upload/app_setting/', index_icon_6) AS icon_reservation,
            CONCAT('".base_url()."', 'assets/upload/app_setting/', index_icon_7) AS icon_information,
            CONCAT('".base_url()."', 'assets/upload/app_setting/', index_icon_8) AS icon_patient
        ");
        $this->db->from('app_setting');
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->row();
    }

    public function get_setting_pendaftaran_rajal()
    {
        $this->db->select('peraturan AS aggreement,
            pesan_berhasil AS message_registration_success,
            st_auto_validasi AS status_auto_validation,
            st_antrian_tampil AS status_queue');
        $this->db->from('app_pendaftaran');
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->row();
    }

    public function get_setting_pendaftaran_rajal_pembayaran()
    {
        $this->db->select('mpasien_kelompok.id AS value,
            mpasien_kelompok.nama AS label');
        $this->db->from('app_pendaftaran_bayar');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = app_pendaftaran_bayar.idkelompokpasien');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_setting_pendaftaran_rajal_asuransi()
    {
        $this->db->select('mrekanan.id AS value,
            mrekanan.nama AS label');
        $this->db->from('app_pendaftaran_asuransi');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = app_pendaftaran_asuransi.idkelompokpasien');
        $this->db->join('mrekanan', 'mrekanan.id = app_pendaftaran_asuransi.idrekanan');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_setting_pendaftaran_rajal_poliklinik()
    {
        $this->db->select('mpoliklinik.id AS value,
            mpoliklinik.nama AS label');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = app_pendaftaran_poli.idpoli');
        $this->db->join('mjadwal_dokter', 'mjadwal_dokter.idpoliklinik = mpoliklinik.id');
        $this->db->where('mpoliklinik.status', 1);
        $this->db->where('mjadwal_dokter.kuota >', 0);
        $this->db->group_by('mjadwal_dokter.idpoliklinik');
        $query = $this->db->get('app_pendaftaran_poli');

        return $query->result();
    }

    public function get_setting_pendaftaran_rehab()
    {
        $this->db->select('peraturan AS aggreement,
            pesan_berhasil AS message_registration_success,
            st_auto_validasi AS status_auto_validation,
            st_antrian_tampil AS status_queue');
        $this->db->from('app_pendaftaran_rm');
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->row();
    }

    public function get_setting_pendaftaran_rehab_pembayaran()
    {
        $this->db->select('mpasien_kelompok.id AS value,
            mpasien_kelompok.nama AS label');
        $this->db->from('app_pendaftaran_rm_bayar');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = app_pendaftaran_rm_bayar.idkelompokpasien');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_setting_pendaftaran_rehab_asuransi()
    {
        $this->db->select('mrekanan.id AS value,
            mrekanan.nama AS label');
        $this->db->from('app_pendaftaran_rm_asuransi');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = app_pendaftaran_rm_asuransi.idkelompokpasien');
        $this->db->join('mrekanan', 'mrekanan.id = app_pendaftaran_rm_asuransi.idrekanan');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_setting_pendaftaran_rehab_poliklinik()
    {
        $this->db->select('mpoliklinik.id AS value,
            mpoliklinik.nama AS label');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = app_pendaftaran_rm_poli.idpoli');
        $query = $this->db->get('app_pendaftaran_rm_poli');

        return $query->result();
    }
}
