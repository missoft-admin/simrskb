<?php

class Data_dokter_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_data_poliklinik($iddokter)
    {
        $this->db->select('mpoliklinik.id AS value, mpoliklinik.nama AS label');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = mjadwal_dokter.idpoliklinik', 'LEFT');
        if (0 != $iddokter) {
            $this->db->where('mjadwal_dokter.iddokter', $iddokter);
        }
        $this->db->where('mjadwal_dokter.status', '1');
        $this->db->group_by('mjadwal_dokter.idpoliklinik');
        $query = $this->db->get('mjadwal_dokter');

        return $query->result();
    }

    public function get_data_dokter($idpoliklinik, $iddokter)
    {
        $this->db->select('mdokter.id,
            CONCAT("'.base_url().'", "assets/upload/dokter/", mdokter.foto) AS foto_dokter,
            mdokter.nama AS nama_dokter,
            mdokter.nip AS nip_dokter,
            mdokter_kategori.nama AS kategori_dokter,
            mdokter.id AS value,
            mdokter.nama AS label');
        $this->db->join('mdokter', 'mdokter.id = mjadwal_dokter.iddokter', 'LEFT');
        $this->db->join('mdokter_kategori', 'mdokter_kategori.id = mdokter.idkategori');
        if (0 != $idpoliklinik) {
            $this->db->where('mjadwal_dokter.idpoliklinik', $idpoliklinik);
        }
        if (0 != $iddokter) {
            $this->db->where('mjadwal_dokter.iddokter', $iddokter);
        }
        $this->db->where('mjadwal_dokter.status', '1');
        $this->db->group_by('mjadwal_dokter.iddokter');
        $this->db->order_by('mdokter.nama', 'ASC');
        $query = $this->db->get('mjadwal_dokter');

        return $query->result();
    }
}
