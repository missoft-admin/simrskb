<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Msetting_tarif_pengajuan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('msetting_tarif_pengajuan');
        return $query->row();
    }
	public function save_setting(){
		$id=($this->input->post('id'));
		
		$iddet=($this->input->post('iddet'));
		$group_rs=($this->input->post('group_rs'));
		$group_dokter=($this->input->post('group_dokter'));
		$group_diskon=($this->input->post('group_diskon'));

		foreach($iddet as $index => $val){
			$detail=array(
				'group_rs'=>$group_rs[$index],				
				'group_dokter'=>$group_dokter[$index],				
				'group_diskon'=>$group_diskon[$index],				
			);
			$this->db->where('id',$val);
			$this->db->update('msetting_tarif_pengajuan_detail',$detail);
		}	
		
		return true;
   }
    public function getDetailSetting($idsetting)
    {
        $this->db->where('idsetting', $idsetting);
        $this->db->group_by('idpengajuan');
        $query = $this->db->get('msetting_tarif_pengajuan_detail');
        return $query->result();
    }

    public function saveData()
    {
        $this->nama = $_POST['nama'];
        $this->idpengajuan = json_encode($_POST['idpengajuan']);

        if ($this->db->insert('msetting_tarif_pengajuan', $this)) {
            $idsetting = $this->db->insert_id();
            $dataPengajuan = json_decode($_POST['detail_value']);
            foreach ($dataPengajuan as $row) {
              if ($row[0] != 0) {
                foreach ($_POST['idpengajuan'] as $pengajuan) {
                  $data = array();
                  $data['idsetting']      = $idsetting;
                  $data['idpengajuan']    = $pengajuan;
                  $data['permintaan_ke']  = $row[0];
                  $data['tarif_rs']       = RemoveComma($row[1]);
                  $data['tarif_dokter']   = RemoveComma($row[2]);
                  $data['total_tarif']    = RemoveComma($row[3]);

                  $this->db->insert('msetting_tarif_pengajuan_detail', $data);
                }
              }
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama = $_POST['nama'];
        $this->idpengajuan = json_encode($_POST['idpengajuan']);

        if ($this->db->update('msetting_tarif_pengajuan', $this, array('id' => $_POST['id']))) {
          
          $idsetting = $_POST['id'];
          $this->db->where('idsetting', $idsetting);

          if ($this->db->delete('msetting_tarif_pengajuan_detail')) {
            $dataPengajuan = json_decode($_POST['detail_value']);
            foreach ($dataPengajuan as $row) {
              if ($row[0] != 0) {
                foreach ($_POST['idpengajuan'] as $pengajuan) {
                  $data = array();
                  $data['idsetting']      = $idsetting;
                  $data['idpengajuan']    = $pengajuan;
                  $data['permintaan_ke']  = $row[0];
                  $data['tarif_rs']       = RemoveComma($row[1]);
                  $data['tarif_dokter']   = RemoveComma($row[2]);
                  $data['total_tarif']    = RemoveComma($row[3]);

                  $this->db->insert('msetting_tarif_pengajuan_detail', $data);
                }
              }
            }
          }

          return true;

        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('msetting_tarif_pengajuan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
