<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Msetting_jurnal_hutang_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('msetting_jurnal_hutang');
        return $query->row_array();
    }
	
	public function list_unit($id)
    {
        $q="SELECT S.idunit as id,U.nama FROM munitpelayanan_user_setting S
			LEFT JOIN munitpelayanan U ON U.id=S.idunit
			WHERE S.idunit NOT IN (SELECT idunit from mlogic_unit)
			GROUP BY
			S.idunit";
        return $this->db->query($q)->result();
    }
	public function list_akun()
    {
        $q="SELECT *FROM makun_nomor  ORDER BY noakun ASC";
        return $this->db->query($q)->result();
    }
	
	public function list_user($step,$idtipe)
    {
        $q="SELECT *FROM musers M
				WHERE M.id NOT IN (SELECT iduser from mlogic_kasbon WHERE mlogic_kasbon.step='$step' 
										AND mlogic_kasbon.idtipe='$idtipe' 
										AND mlogic_kasbon.status='1') AND M.status='1'";
		// print_r($q);
        return $this->db->query($q)->result();
    }
	
}
