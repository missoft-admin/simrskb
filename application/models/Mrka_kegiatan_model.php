<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mrka_kegiatan_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getSpecified($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('mrka');
        return $query->row();
    }
	public function list_unit(){
		$userid=$this->session->userdata('user_id');
		// $q="SELECT S.idunit,M.nama from munitpelayanan_user_setting S
			// LEFT JOIN munitpelayanan M ON M.id=S.idunit
			// WHERE S.userid='$userid'";
		$q="SELECT *FROM (
				SELECT M.id as idunit,M.nama,CASE WHEN U.userid IS NULL THEN 'disabled' ELSE '' END as akses FROM munitpelayanan M
				LEFT JOIN munitpelayanan_user_setting U ON U.idunit=M.id AND U.userid='$userid'
				WHERE M.`status`='1'
				) T 
				ORDER BY T.akses ASC,T.nama ASC";
				// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	public function list_kegiatan(){
		$q="SELECT M.id,M.nama from mkegiatan_rka M
			WHERE M.`status`='1'
			ORDER BY M.urutan ASC";
		return $this->db->query($q)->result();
	}
	public function get_edit_kegiatan($id){
		$q="SELECT *from mrka_kegiatan 
				WHERE id='$id'";
		return $this->db->query($q)->row_array();
	}
	public function list_unit_kolab($id=''){
		// if ($id==''){
			// // $q="SELECT S.idunit,M.nama from munitpelayanan_user_setting S
			// // LEFT JOIN munitpelayanan M ON M.id=S.idunit
			// // ";
		// }
		$q="SELECT M.id as idunit,M.nama,CASE WHEN U.idunit IS NOT NULL THEN 'selected' ELSE '' END as pilih
				FROM munitpelayanan M
				LEFT JOIN mrka_kegiatan_unit_kolab U ON  U.idunit=M.id AND U.idrka_kegiatan='$id'
				WHERE M.`status`='1' ";
		return $this->db->query($q)->result();
	}
	public function list_prespektif($id){
		$q="SELECT H.idprespektif,M.nama from mrka_program H
			LEFT JOIN mprespektif_rka M ON M.id=H.idprespektif
			WHERE H.idrka='$id'
			GROUP BY H.idprespektif";
		return $this->db->query($q)->result();
	}
	public function list_program($id){
		$q="SELECT H.idprogram,M.nama from mrka_program H
			LEFT JOIN mprogram_rka M ON M.id=H.idprogram
			WHERE H.idrka='$id'
			GROUP BY H.idprogram";
		return $this->db->query($q)->result();
	}
    
    
	public function save_kegiatan() {
		// print_r($_POST['nominal'][1]);exit();
        $this->idrka       = $_POST['idrka'];
        $this->idprespektif       = $_POST['idprespektif'];
        $this->idprogram  = $_POST['idprogram'];
        $this->idkegiatan  = $_POST['idkegiatan'];
        $this->kpi  = $_POST['kpi'];
        $this->bobot  =RemoveComma($_POST['bobot']);
        $this->total_anggaran  =RemoveComma($_POST['total_anggaran']);
        $this->idunit  = $_POST['idunit'];
        $idunit_kolab  = $_POST['idunit_kolab'];
		// print_r($idunit_kolab);exit();
        $this->keterangan  = $_POST['keterangan'];
        $this->target  = $_POST['target'];
        $this->initiative  = $_POST['initiative'];
        $this->b1  =RemoveComma($_POST['nominal'][1]);
        $this->b2  =RemoveComma($_POST['nominal'][2]);
        $this->b3  =RemoveComma($_POST['nominal'][3]);
        $this->b4  =RemoveComma($_POST['nominal'][4]);
        $this->b5  =RemoveComma($_POST['nominal'][5]);
        $this->b6  =RemoveComma($_POST['nominal'][6]);
        $this->b7  =RemoveComma($_POST['nominal'][7]);
        $this->b8  =RemoveComma($_POST['nominal'][8]);
        $this->b9  =RemoveComma($_POST['nominal'][9]);
        $this->b10  =RemoveComma($_POST['nominal'][10]);
        $this->b11  =RemoveComma($_POST['nominal'][11]);
        $this->b12  =RemoveComma($_POST['nominal'][12]);
        $this->created_by  = $this->session->userdata('user_id');
        $this->created_nama  = $this->session->userdata('user_name');
        $this->created_date  = date('Y-m-d H:i:s');
		
        if ($this->db->insert('mrka_kegiatan', $this)) {
           $id=$this->db->insert_id();
		   for ($i=1;$i<=12;$i++){
			   $data_det=array(
				'idrka_kegiatan'=>$id,
				'idrka'=>$_POST['idrka'],
				'bulan'=>$i,
				'nominal'=>RemoveComma($_POST['nominal'][$i])
				
			   );
			  $this->db->insert('mrka_kegiatan_nominal',$data_det);
		   }
		  
		   $this->db->where('idrka_kegiatan',$id);
		   $this->db->delete('mrka_kegiatan_unit_kolab');
		   if ($idunit_kolab){
				foreach ($idunit_kolab as $row=>$val){
					$data_unit=array(
						'idrka_kegiatan' =>$id,
						'idunit' =>$val,
					);
					$this->db->insert('mrka_kegiatan_unit_kolab',$data_unit);
				}
			}
			
            return $id;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	
    public function update_kegiatan() {
        $this->idrka       = $_POST['idrka'];
        $this->idprespektif       = $_POST['idprespektif'];
        $this->idprogram  = $_POST['idprogram'];
        $this->idkegiatan  = $_POST['idkegiatan'];
        $this->kpi  = $_POST['kpi'];
        $this->bobot  =RemoveComma($_POST['bobot']);
        $this->total_anggaran  =RemoveComma($_POST['total_anggaran']);
        $this->idunit  = $_POST['idunit'];
        $idunit_kolab  = $_POST['idunit_kolab'];
        $this->keterangan  = $_POST['keterangan'];
        $this->target  = $_POST['target'];
        $this->initiative  = $_POST['initiative'];
        $this->b1  =RemoveComma($_POST['nominal'][1]);
        $this->b2  =RemoveComma($_POST['nominal'][2]);
        $this->b3  =RemoveComma($_POST['nominal'][3]);
        $this->b4  =RemoveComma($_POST['nominal'][4]);
        $this->b5  =RemoveComma($_POST['nominal'][5]);
        $this->b6  =RemoveComma($_POST['nominal'][6]);
        $this->b7  =RemoveComma($_POST['nominal'][7]);
        $this->b8  =RemoveComma($_POST['nominal'][8]);
        $this->b9  =RemoveComma($_POST['nominal'][9]);
        $this->b10  =RemoveComma($_POST['nominal'][10]);
        $this->b11  =RemoveComma($_POST['nominal'][11]);
        $this->b12  =RemoveComma($_POST['nominal'][12]);
		
        $this->edited_by  = $this->session->userdata('user_id');
        $this->edited_nama  = $this->session->userdata('user_name');
        $this->edited_date  = date('Y-m-d H:i:s');
		// print_r($idunit_kolab);exit();
        if ($this->db->update('mrka_kegiatan', $this, ['id' => $_POST['id']])) {
			// mrka_kegiatan_unit_kolab
			$id=$_POST['id'];
			$this->db->where('idrka_kegiatan',$id);
			$this->db->delete('mrka_kegiatan_unit_kolab');
			if ($idunit_kolab){
				foreach ($idunit_kolab as $row=>$val){
					$data_unit=array(
						'idrka_kegiatan' =>$id,
						'idunit' =>$val,
					);
					$this->db->insert('mrka_kegiatan_unit_kolab',$data_unit);
				}
			}
			
			$this->db->where('idrka_kegiatan',$_POST['id']);
			$this->db->delete('mrka_kegiatan_nominal');
             for ($i=1;$i<=12;$i++){
			   $data_det=array(
				'idrka_kegiatan'=>$_POST['id'],
				'idrka'=>$_POST['idrka'],
				'bulan'=>$i,
				'nominal'=>RemoveComma($_POST['nominal'][$i])
				
			   );
			  $this->db->insert('mrka_kegiatan_nominal',$data_det);
		   }
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function hapus_kegiatan() {
        $this->nama       = $_POST['nama'];
		$this->periode  = $_POST['periode'];

        $this->edited_by  = $this->session->userdata('user_id');
        $this->edited_nama  = $this->session->userdata('user_name');
        $this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mrka', $this, ['id' => $_POST['id']])) {
            
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function update_status($id,$status) {
		
		if ($status=='0'){
			$this->status  = $status;
			$this->deleted_by  = $this->session->userdata('user_id');
			$this->deleted_nama  = $this->session->userdata('user_name');
			$this->deleted_date  = date('Y-m-d H:i:s');
		}
		if ($status=='2'){
			$this->status  = $status;
			$this->publish_by  = $this->session->userdata('user_id');
			$this->publish_nama  = $this->session->userdata('user_name');
			$this->publish_date  = date('Y-m-d H:i:s');
		}
		if ($status=='3'){
			$this->status  = $status;
			$this->actived_by  = $this->session->userdata('user_id');
			$this->actived_nama  = $this->session->userdata('user_name');
			$this->actived_date  = date('Y-m-d H:i:s');
		}
		if ($status=='4'){
			$this->status  = $status;
			$this->selesai_by  = $this->session->userdata('user_id');
			$this->selesai_nama  = $this->session->userdata('user_name');
			$this->selesai_date  = date('Y-m-d H:i:s');
		}
       
        if ($this->db->update('mrka_kegiatan', $this, ['id' => $id])) {
            
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id) {
        $this->status = 0;

        $this->deleted_by  = $this->session->userdata('user_id');
        $this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mrka', $this, ['id' => $id])) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    
}
