<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Term_laboratorium_umum_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

		function get_data_transaksi($id, $asal_rujukan = '', $status = '') {
				$this->db->select('term_laboratorium_umum.*,
					(CASE
							WHEN term_laboratorium_umum.asal_rujukan = 1 THEN "Poliklinik"
							WHEN term_laboratorium_umum.asal_rujukan = 2 THEN "Instalasi Gawat Darurat"
							WHEN term_laboratorium_umum.asal_rujukan = 3 THEN "Rawat Inap"
							WHEN term_laboratorium_umum.asal_rujukan = 4 THEN "One Day Surgery"
					END) AS tujuan_pendaftaran,
					(CASE
							WHEN term_laboratorium_umum.asal_rujukan IN (1, 2) THEN
									tpoliklinik_pendaftaran.tanggaldaftar
							WHEN term_laboratorium_umum.asal_rujukan IN (3, 4) THEN
									poliklinik_ranap.tanggaldaftar
					END) AS tanggal_daftar,
					(CASE
							WHEN term_laboratorium_umum.asal_rujukan IN (1, 2) THEN
									tpoliklinik_pendaftaran.nopendaftaran
							WHEN term_laboratorium_umum.asal_rujukan IN (3, 4) THEN
									poliklinik_ranap.nopendaftaran
					END) AS nomor_registrasi,
					mdokter_perujuk.nama AS dokter_perujuk,
					(CASE
							WHEN term_laboratorium_umum.asal_rujukan IN (1, 2) THEN
									mpoliklinik.nama
							WHEN term_laboratorium_umum.asal_rujukan IN (3, 4) THEN
									mkelas.nama
					END) AS poliklinik,
					COALESCE ( mfpasien.no_medrec, poliklinik_ranap.nomor_medrec) AS nomedrec,
					COALESCE ( mfpasien.nama, poliklinik_ranap.nama_pasien) AS nama_pasien,
					COALESCE ( mfpasien.title, poliklinik_ranap.title_pasien) AS title,
					COALESCE ( mfpasien.jenis_kelamin, poliklinik_ranap.jenis_kelamin) AS jenis_kelamin,
					COALESCE ( mfpasien.alamat_jalan, poliklinik_ranap.alamat_pasien) AS alamat_jalan,
					COALESCE ( mfpasien.tanggal_lahir, poliklinik_ranap.tanggal_lahir) AS tanggal_lahir,
					COALESCE ( mfpasien.umur_tahun, poliklinik_ranap.umur_tahun) AS umur_tahun,
					COALESCE ( mfpasien.umur_bulan, poliklinik_ranap.umur_bulan) AS umur_bulan,
					COALESCE ( mfpasien.umur_hari, poliklinik_ranap.umur_hari) AS umur_hari,
					COALESCE ( mfpasien.telepon, poliklinik_ranap.telepon) AS telepon,
					COALESCE ( mpasien_kelompok.nama, poliklinik_ranap.kelompok_pasien) AS kelompok_pasien,
					mpekerjaan.nama AS pekerjaan,
					mdokter_peminta.id AS dokter_peminta_id,
					mdokter_peminta.nama AS dokter_peminta,
					merm_pengaturan_tujuan_laboratorium.nama AS tujuan_laboratorium_nama,
					referensi_prioritas.ref AS prioritas_label,
					referensi_pemeriksaan.ref AS status_pemeriksaan_label,
					mppa.nama AS created_ppa,
					mppa.nip AS nip_ppa');
				$this->db->join('(SELECT
            trawatinap_pendaftaran.id,
            trawatinap_pendaftaran.tanggaldaftar,
            trawatinap_pendaftaran.nopendaftaran,
            tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
            tpoliklinik_pendaftaran.namapasien AS nama_pasien,
            tpoliklinik_pendaftaran.title AS title_pasien,
            tpoliklinik_pendaftaran.jenis_kelamin AS jenis_kelamin,
            tpoliklinik_pendaftaran.alamatpasien AS alamat_pasien,
            tpoliklinik_pendaftaran.tanggal_lahir,
            tpoliklinik_pendaftaran.umurhari AS umur_hari,
            tpoliklinik_pendaftaran.umurbulan AS umur_bulan,
            tpoliklinik_pendaftaran.umurtahun AS umur_tahun,
            tpoliklinik_pendaftaran.telepon,
            mpasien_kelompok.nama AS kelompok_pasien,
            trawatinap_pendaftaran.idkelas
        FROM
            trawatinap_pendaftaran
        INNER JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
        INNER JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien) AS poliklinik_ranap', 'term_laboratorium_umum.pendaftaran_id = poliklinik_ranap.id AND term_laboratorium_umum.asal_rujukan IN (3, 4)', 'LEFT');
				$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_umum.pendaftaran_id AND term_laboratorium_umum.asal_rujukan IN (1, 2)', 'LEFT');
				$this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
				$this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
				$this->db->join('mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_laboratorium_umum.dokter_perujuk_id', 'LEFT');
				$this->db->join('mdokter mdokter_peminta', 'mdokter_peminta.id = term_laboratorium_umum.dokter_peminta_id', 'LEFT');
				$this->db->join('merm_pengaturan_tujuan_laboratorium', 'merm_pengaturan_tujuan_laboratorium.id = term_laboratorium_umum.tujuan_laboratorium', 'LEFT');
				$this->db->join('merm_referensi referensi_prioritas', 'referensi_prioritas.ref_head_id = 85 AND referensi_prioritas.nilai = term_laboratorium_umum.prioritas', 'LEFT');
				$this->db->join('merm_referensi referensi_pemeriksaan', 'referensi_pemeriksaan.ref_head_id = 89 AND referensi_pemeriksaan.nilai = term_laboratorium_umum.status_pemeriksaan', 'LEFT');
				$this->db->join('mppa', 'mppa.id = term_laboratorium_umum.created_ppa', 'LEFT');
				$this->db->join('mpekerjaan', 'mpekerjaan.id = mfpasien.pekerjaan_id', 'LEFT');
				$this->db->join('mkelas', 'mkelas.id = poliklinik_ranap.idkelas', 'LEFT');
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
				
				if ($status == 'draft') {
					if ($asal_rujukan == 'rawat_inap') {
						$this->db->where_in('term_laboratorium_umum.asal_rujukan', [3, 4]);
					} else {
						$this->db->where_in('term_laboratorium_umum.asal_rujukan', [1, 2]);
					}
					$this->db->where('term_laboratorium_umum.pendaftaran_id', $id);
					$this->db->where('term_laboratorium_umum.status_pemeriksaan', 1);
					$this->db->order_by('term_laboratorium_umum.id', 'DESC');
					$this->db->limit(1);
				} else {
					$this->db->where('term_laboratorium_umum.id', $id);
				}

				$query = $this->db->get('term_laboratorium_umum');
				return $query->row();
		}

		public function getListPemeriksaan($transaksi_id)
		{
			$this->db->select('mtarif_laboratorium.idkelompok, mtarif_laboratorium.idpaket, mtarif_laboratorium.path,
					mtarif_laboratorium.level, mtarif_laboratorium.nama, term_laboratorium_umum_pemeriksaan.*');
			$this->db->join('mtarif_laboratorium', 'term_laboratorium_umum_pemeriksaan.idlaboratorium = mtarif_laboratorium.id', 'left');
			$this->db->where('term_laboratorium_umum_pemeriksaan.statusrincianpaket', 0);
			$this->db->where('term_laboratorium_umum_pemeriksaan.status', 1);
			$this->db->where('term_laboratorium_umum_pemeriksaan.transaksi_id', $transaksi_id);
			$this->db->order_by('mtarif_laboratorium.path', 'ASC');
			$this->db->from('term_laboratorium_umum_pemeriksaan');
			$query = $this->db->get();
			return $query->result();
		}

		public function getSelectedPemeriksaan($transaksiId) {
				$this->db->select('term_laboratorium_umum_pemeriksaan.id,
					term_laboratorium_umum_pemeriksaan.idlaboratorium AS pemeriksaan_id,
					term_laboratorium_umum_pemeriksaan.kelas,
					term_laboratorium_umum_pemeriksaan.namatarif AS nama_pemeriksaan,
					mtarif_laboratorium.path,
					term_laboratorium_umum_pemeriksaan.jasasarana AS jasa_sarana,
					term_laboratorium_umum_pemeriksaan.jasapelayanan AS jasa_pelayanan,
					term_laboratorium_umum_pemeriksaan.bhp,
					term_laboratorium_umum_pemeriksaan.biayaperawatan AS biaya_perawatan,
					term_laboratorium_umum_pemeriksaan.total,
					term_laboratorium_umum_pemeriksaan.kuantitas,
					term_laboratorium_umum_pemeriksaan.diskon,
					term_laboratorium_umum_pemeriksaan.totalkeseluruhan AS total_keseluruhan,
					term_laboratorium_umum_pemeriksaan.statusverifikasi AS status_verifikasi,
					IF(term_laboratorium_umum_pemeriksaan.status, 0, 1) AS status_delete,
					"1" AS status_database');
				$this->db->join('mtarif_laboratorium', 'mtarif_laboratorium.id = term_laboratorium_umum_pemeriksaan.idlaboratorium');
				$query = $this->db->get_where('term_laboratorium_umum_pemeriksaan', array('term_laboratorium_umum_pemeriksaan.transaksi_id' => $transaksiId, 'term_laboratorium_umum_pemeriksaan.statusrincianpaket' => 0, 'term_laboratorium_umum_pemeriksaan.status' => 1));

				return $query->result();
		}

		function logicTujuanLaboratorium($tipe_layanan, $asal_pasien, $poliklinik, $dokter) {
				$this->db->select('merm_pengaturan_tujuan_laboratorium.id,
					merm_pengaturan_tujuan_laboratorium.nama');
				$this->db->from('merm_pengaturan_tujuan_laboratorium_unit_akses');
				$this->db->join('merm_pengaturan_tujuan_laboratorium', 'merm_pengaturan_tujuan_laboratorium_unit_akses.idpengaturan = merm_pengaturan_tujuan_laboratorium.id');
				$this->db->where('merm_pengaturan_tujuan_laboratorium_unit_akses.tipe_layanan', $tipe_layanan);
				$this->db->where("(merm_pengaturan_tujuan_laboratorium_unit_akses.asal_pasien = '0' OR merm_pengaturan_tujuan_laboratorium_unit_akses.asal_pasien = '$asal_pasien')");
				$this->db->where("(merm_pengaturan_tujuan_laboratorium_unit_akses.idpoliklinik = '0' OR merm_pengaturan_tujuan_laboratorium_unit_akses.idpoliklinik = '$poliklinik')");
				$this->db->where("(merm_pengaturan_tujuan_laboratorium_unit_akses.iddokter = '0' OR merm_pengaturan_tujuan_laboratorium_unit_akses.iddokter = '$dokter')");
				$this->db->order_by('merm_pengaturan_tujuan_laboratorium.id', 'ASC');

				$query = $this->db->get();
				return $query->result();
		}

		public function insertPemeriksaan()
		{
				$transaksi_id = $this->input->post('transaksi_id');

				if ($this->input->post('status_form') == 'edit_permintaan') {
					// Clone data to history tables
					$this->cloneToHistory($transaksi_id);

					// Update term_laboratorium table
					$this->updateTermLaboratorium($transaksi_id);

					// Delete records from term_laboratorium_umum_pemeriksaan table
					$this->deleteTermLaboratoriumPemeriksaan($transaksi_id);
				} else {
					$data = [
							'pendaftaran_id' => $this->input->post('pendaftaran_id'),
							'pasien_id' => $this->input->post('pasien_id'),
							'jenis_pemeriksaan' => $this->input->post('jenis_pemeriksaan'),
							'rencana_pemeriksaan' => YMDFormat($this->input->post('rencana_pemeriksaan')),
							'tujuan_laboratorium' => $this->input->post('tujuan_laboratorium'),
							'dokter_perujuk_id' => $this->input->post('dokter_perujuk_id'),
							'dokter_peminta_id' => $this->input->post('dokter_peminta_id'),
							'diagnosa' => $this->input->post('diagnosa'),
							'catatan_permintaan' => $this->input->post('catatan_permintaan'),
							'waktu_permintaan' => YMDTimeFormat($this->input->post('tanggal_permintaan') . ' ' . $this->input->post('waktu_permintaan')),
							'prioritas' => $this->input->post('prioritas'),
							'pasien_puasa' => $this->input->post('pasien_puasa'),
							'pengiriman_hasil' => $this->input->post('pengiriman_hasil'),
							'status_pemeriksaan' => 2, // Menunggu Pemeriksaan
							'created_ppa' => $this->input->post('ppa_id'),
							'created_at' => date("Y-m-d H:i:s"),
							'petugas_proses_transaksi' => $this->input->post('ppa_id'),
							'waktu_proses_transaksi' => date("Y-m-d H:i:s"),
					];

					if ($this->input->post('ref_asal_rujukan')) {
						$data['asal_rujukan'] = $this->input->post('ref_asal_rujukan');
					}

					if ($this->input->post('ref_asal_rujukan')) {
						$data['asal_rujukan'] = $this->input->post('ref_asal_rujukan');
					}
					
					// Update data into term_laboratorium table
					$this->db->where('id', $transaksi_id);
					$this->db->update('term_laboratorium_umum', $data);
				}
				
				// Get dokter information
				$dokter = $this->getDokterInfo($transaksi_id);
				
				$data_pemeriksaan = json_decode($_POST['data_pemeriksaan']);

				// Fungsi untuk menyaring data yang memiliki status_delete = 0
				// $filtered_data_pemeriksaan = array_filter($data_pemeriksaan, function($pemeriksaan) {
				// 		return isset($pemeriksaan->status_delete) && $pemeriksaan->status_delete == 0;
				// });

        // print_r($data_pemeriksaan);exit();

				// foreach ($filtered_data_pemeriksaan as $pemeriksaan) {
				foreach ($data_pemeriksaan as $pemeriksaan) {
					$index = '';

					// CASE : CHILD GET PARENT HEADER
					$query = $this->db->query("call spGetParentFromChild('" . $pemeriksaan->path . "')");
					$result = $query->result();

					$query->next_result();
					$query->free_result();

					foreach ($result as $parent) {
						$tindakan = [];
						$tindakan['transaksi_id'] = $transaksi_id;
						$tindakan['idlaboratorium'] = $parent->id;
						$tindakan['kelas'] = 0;
						$tindakan['namatarif'] = $parent->nama;
						$tindakan['jasasarana'] = 0;
						$tindakan['jasapelayanan'] = 0;
						$tindakan['bhp'] = 0;
						$tindakan['biayaperawatan'] = 0;
						$tindakan['total'] = 0;
						$tindakan['kuantitas'] = 0;
						$tindakan['diskon'] = 0;
						$tindakan['totalkeseluruhan'] = 0;
						$tindakan['statusrincianpaket'] = 2;
						$tindakan['index'] = 1;
						$tindakan['st_paket'] = 0;

						$tindakan['iddokter'] = $_POST['dokter_perujuk_id'];
						$tindakan['potongan_rs'] = $dokter->potongan_rs;
						$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

						$this->db->on_duplicate('term_laboratorium_umum_pemeriksaan', $tindakan);
					}

					// NORMAL CASE
					$tindakan = [];
					$tindakan['transaksi_id'] = $transaksi_id;
					$tindakan['idlaboratorium'] = $pemeriksaan->pemeriksaan_id;
					$tindakan['kelas'] = $pemeriksaan->kelas;
					$tindakan['namatarif'] = $pemeriksaan->nama_pemeriksaan;
					$tindakan['jasasarana'] = RemoveComma($pemeriksaan->jasa_sarana);
					$tindakan['jasapelayanan'] = RemoveComma($pemeriksaan->jasa_pelayanan);
					$tindakan['bhp'] = RemoveComma($pemeriksaan->bhp);
					$tindakan['biayaperawatan'] = RemoveComma($pemeriksaan->biaya_perawatan);
					$tindakan['total'] = RemoveComma($pemeriksaan->total);
					$tindakan['kuantitas'] = RemoveComma($pemeriksaan->kuantitas);
					$tindakan['diskon'] = RemoveComma($pemeriksaan->diskon);
					$tindakan['totalkeseluruhan'] = RemoveComma($pemeriksaan->total_keseluruhan);
					$tindakan['statusverifikasi'] = $pemeriksaan->status_verifikasi;
					$tindakan['index'] = $index;
					$tindakan['st_paket'] = 0;

					$tindakan['iddokter'] = $_POST['dokter_perujuk_id'];
					$tindakan['potongan_rs'] = $dokter->potongan_rs;
					$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

					if ($this->db->on_duplicate('term_laboratorium_umum_pemeriksaan', $tindakan)) {
						// GET KELOMPOK, PAKET, PATH ID LAB
						$tarif = get_by_field('id', $pemeriksaan->pemeriksaan_id, 'mtarif_laboratorium');
						$idkelompok = $tarif->idkelompok;
						$idpaket = $tarif->idpaket;
						$path = $tarif->path;

						// CASE : CHILD KELOMPOK & NON PAKET
						if ($idkelompok == 1 && $idpaket == 0) {
							$rincianPaket = $this->getListRincianPaket($path);
							foreach ($rincianPaket as $row) {
								$rincian = [];
								$rincian['transaksi_id'] = $transaksi_id;
								$rincian['idlaboratorium'] = $row->id;
								$rincian['kelas'] = $row->kelas;
								$rincian['namatarif'] = $row->nama;
								$rincian['jasasarana'] = $row->jasasarana;
								$rincian['jasapelayanan'] = $row->jasapelayanan;
								$rincian['bhp'] = $row->bhp;
								$rincian['biayaperawatan'] = $row->biayaperawatan;
								$rincian['total'] = $row->total;
								$rincian['kuantitas'] = 1;
								$rincian['diskon'] = 0;
								$rincian['totalkeseluruhan'] = $row->total;
								$rincian['statusrincianpaket'] = 1;
								$rincian['index'] = $index;
								$rincian['st_paket'] = 1;

								$tindakan['iddokter'] = $_POST['dokter_perujuk_id'];
								$tindakan['potongan_rs'] = $dokter->potongan_rs;
								$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

								$this->db->on_duplicate('term_laboratorium_umum_pemeriksaan', $rincian);
							}
						}

						// CASE : CHILD KELOMPOK & PAKET
						if ($idkelompok == 1 && $idpaket == 1) {
							$rincianPaket = $this->getListRincianPaket($path);
							foreach ($rincianPaket as $row) {
								$rincian = [];
								$rincian['transaksi_id'] = $transaksi_id;
								$rincian['idlaboratorium'] = $row->id;
								$rincian['kelas'] = $row->kelas;
								$rincian['namatarif'] = $row->nama;
								$rincian['jasasarana'] = 0;
								$rincian['jasapelayanan'] = 0;
								$rincian['bhp'] = 0;
								$rincian['biayaperawatan'] = 0;
								$rincian['total'] = 0;
								$rincian['kuantitas'] = 0;
								$rincian['diskon'] = 0;
								$rincian['totalkeseluruhan'] = 0;
								$rincian['statusrincianpaket'] = 1;
								$rincian['index'] = $index;
								$rincian['st_paket'] = 1;

								$tindakan['iddokter'] = $_POST['dokter_perujuk_id'];
								$tindakan['potongan_rs'] = $dokter->potongan_rs;
								$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

								$this->db->on_duplicate('term_laboratorium_umum_pemeriksaan', $rincian);
							}
						}
					}
				}

				return true;
		}

		public function updatePemeriksaan()
		{
				$transaksi_id = $this->input->post('transaksi_id');

				$data = [
						'pendaftaran_id' => $this->input->post('pendaftaran_id'),
						'pasien_id' => $this->input->post('pasien_id'),
						'jenis_pemeriksaan' => $this->input->post('jenis_pemeriksaan'),
						'rencana_pemeriksaan' => YMDFormat($this->input->post('rencana_pemeriksaan')),
						'tujuan_laboratorium' => $this->input->post('tujuan_laboratorium'),
						'dokter_perujuk_id' => $this->input->post('dokter_perujuk_id'),
						'dokter_peminta_id' => $this->input->post('dokter_peminta_id'),
						'diagnosa' => $this->input->post('diagnosa'),
						'catatan_permintaan' => $this->input->post('catatan_permintaan'),
						'waktu_permintaan' => YMDTimeFormat($this->input->post('tanggal_permintaan') . ' ' . $this->input->post('waktu_permintaan')),
						'prioritas' => $this->input->post('prioritas'),
						'pasien_puasa' => $this->input->post('pasien_puasa'),
						'pengiriman_hasil' => $this->input->post('pengiriman_hasil'),
						'waktu_pengambilan_sample' => YMDTimeFormat($this->input->post('tanggal_pengambilan_sample') . ' ' . $this->input->post('waktu_pengambilan_sample')),
						'petugas_pengambilan_sample' => $this->input->post('petugas_pengambilan_sample'),
						'nomor_laboratorium' => $this->input->post('nomor_laboratorium'),
						'dokter_laboratorium' => $this->input->post('dokter_laboratorium'),
						'dokter_penanggung_jawab' => $this->input->post('dokter_penanggung_jawab'),
						'edited_ppa' => $this->input->post('ppa_id'),
						'edited_at' => date("Y-m-d H:i:s"),
				];

				$formSubmit = $this->input->post('form_submit');
				if ($formSubmit == 'form-submit-process') {
					$data['status_pemeriksaan'] = 4;
					$data['status_panggil'] = 3;
				}

				// Update data into term_laboratorium table
				$this->db->where('id', $transaksi_id);
				$this->db->update('term_laboratorium_umum', $data);

				// Delete records from term_laboratorium_umum_pemeriksaan table
				$this->deleteTermLaboratoriumPemeriksaan($transaksi_id);
				
				// Get dokter information
				$dokter = $this->getDokterInfo($transaksi_id);
				
				$data_pemeriksaan = json_decode($_POST['data_pemeriksaan']);

				// Fungsi untuk menyaring data yang memiliki status_delete = 0
				$filtered_data_pemeriksaan = array_filter($data_pemeriksaan, function($pemeriksaan) {
						return isset($pemeriksaan->status_delete) && $pemeriksaan->status_delete == 0;
				});

				$this->db->where('transaksi_id', $transaksi_id);
				$this->db->delete('term_laboratorium_umum_pemeriksaan');

				foreach ($filtered_data_pemeriksaan as $pemeriksaan) {
					$index = '';

					$statusPemeriksaan = isset($pemeriksaan->status_delete) ? ($pemeriksaan->status_delete ? 0 : 1) : 1;

					// CASE : CHILD GET PARENT HEADER
					$query = $this->db->query("call spGetParentFromChild('" . $pemeriksaan->path . "')");
					$result = $query->result();

					$query->next_result();
					$query->free_result();

					foreach ($result as $parent) {
						$tindakan = [];
						$tindakan['transaksi_id'] = $transaksi_id;
						$tindakan['idlaboratorium'] = $parent->id;
						$tindakan['kelas'] = 0;
						$tindakan['namatarif'] = $parent->nama;
						$tindakan['jasasarana'] = 0;
						$tindakan['jasapelayanan'] = 0;
						$tindakan['bhp'] = 0;
						$tindakan['biayaperawatan'] = 0;
						$tindakan['total'] = 0;
						$tindakan['kuantitas'] = 0;
						$tindakan['diskon'] = 0;
						$tindakan['totalkeseluruhan'] = 0;
						$tindakan['statusrincianpaket'] = 2;
						$tindakan['index'] = 1;
						$tindakan['st_paket'] = 0;
						$tindakan['status'] = $statusPemeriksaan;

						$tindakan['iddokter'] = $_POST['dokter_perujuk_id'];
						$tindakan['potongan_rs'] = $dokter->potongan_rs;
						$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

						$this->db->on_duplicate('term_laboratorium_umum_pemeriksaan', $tindakan);
					}
					
					// NORMAL CASE
					$tindakan = [];
					$tindakan['transaksi_id'] = $transaksi_id;
					$tindakan['idlaboratorium'] = $pemeriksaan->pemeriksaan_id;
					$tindakan['kelas'] = $pemeriksaan->kelas;
					$tindakan['namatarif'] = $pemeriksaan->nama_pemeriksaan;
					$tindakan['jasasarana'] = RemoveComma($pemeriksaan->jasa_sarana);
					$tindakan['jasapelayanan'] = RemoveComma($pemeriksaan->jasa_pelayanan);
					$tindakan['bhp'] = RemoveComma($pemeriksaan->bhp);
					$tindakan['biayaperawatan'] = RemoveComma($pemeriksaan->biaya_perawatan);
					$tindakan['total'] = RemoveComma($pemeriksaan->total);
					$tindakan['kuantitas'] = RemoveComma($pemeriksaan->kuantitas);
					$tindakan['diskon'] = RemoveComma($pemeriksaan->diskon);
					$tindakan['totalkeseluruhan'] = RemoveComma($pemeriksaan->total_keseluruhan);
					$tindakan['statusverifikasi'] = $pemeriksaan->status_verifikasi;
					$tindakan['index'] = $index;
					$tindakan['st_paket'] = 0;
					$tindakan['status'] = $statusPemeriksaan;

					$tindakan['iddokter'] = $_POST['dokter_perujuk_id'];
					$tindakan['potongan_rs'] = $dokter->potongan_rs;
					$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

					if ($this->db->on_duplicate('term_laboratorium_umum_pemeriksaan', $tindakan)) {
						// GET KELOMPOK, PAKET, PATH ID LAB
						$tarif = get_by_field('id', $pemeriksaan->pemeriksaan_id, 'mtarif_laboratorium');
						$idkelompok = $tarif->idkelompok;
						$idpaket = $tarif->idpaket;
						$path = $tarif->path;

						// CASE : CHILD KELOMPOK & NON PAKET
						if ($idkelompok == 1 && $idpaket == 0) {
							$rincianPaket = $this->getListRincianPaket($path);
							foreach ($rincianPaket as $row) {
								$rincian = [];
								$rincian['transaksi_id'] = $transaksi_id;
								$rincian['kelas'] = $row->kelas;
								$rincian['namatarif'] = $row->nama;
								$rincian['idlaboratorium'] = $row->id;
								$rincian['jasasarana'] = $row->jasasarana;
								$rincian['jasapelayanan'] = $row->jasapelayanan;
								$rincian['bhp'] = $row->bhp;
								$rincian['biayaperawatan'] = $row->biayaperawatan;
								$rincian['total'] = $row->total;
								$rincian['kuantitas'] = 1;
								$rincian['diskon'] = 0;
								$rincian['totalkeseluruhan'] = $row->total;
								$rincian['statusrincianpaket'] = 1;
								$rincian['index'] = $index;
								$rincian['st_paket'] = 1;
								$rincian['status'] = $statusPemeriksaan;

								$tindakan['iddokter'] = $_POST['dokter_perujuk_id'];
								$tindakan['potongan_rs'] = $dokter->potongan_rs;
								$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

								$this->db->on_duplicate('term_laboratorium_umum_pemeriksaan', $rincian);
							}
						}

						// CASE : CHILD KELOMPOK & PAKET
						if ($idkelompok == 1 && $idpaket == 1) {
							$rincianPaket = $this->getListRincianPaket($path);
							foreach ($rincianPaket as $row) {
								$rincian = [];
								$rincian['transaksi_id'] = $transaksi_id;
								$rincian['idlaboratorium'] = $row->id;
								$rincian['kelas'] = $row->kelas;
								$rincian['namatarif'] = $row->nama;
								$rincian['jasasarana'] = 0;
								$rincian['jasapelayanan'] = 0;
								$rincian['bhp'] = 0;
								$rincian['biayaperawatan'] = 0;
								$rincian['total'] = 0;
								$rincian['kuantitas'] = 0;
								$rincian['diskon'] = 0;
								$rincian['totalkeseluruhan'] = 0;
								$rincian['statusrincianpaket'] = 1;
								$rincian['index'] = $index;
								$rincian['st_paket'] = 1;
								$rincian['status'] = $statusPemeriksaan;

								$tindakan['iddokter'] = $_POST['dokter_perujuk_id'];
								$tindakan['potongan_rs'] = $dokter->potongan_rs;
								$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

								$this->db->on_duplicate('term_laboratorium_umum_pemeriksaan', $rincian);
							}
						}
					}
				}

				return true;
		}

		private function cloneToHistory($transaksiId) {
				// Clone data from term_laboratorium to history tables
				$this->db->where('id', $transaksiId);
				$data = $this->db->get('term_laboratorium_umum')->row_array();

				if (!empty($data)) {
						// Set version information
						$version = $this->getVersionInfo($transaksiId);
						$data['id'] = ''; // Assuming 'id' is an auto-incremented primary key
						$data['versi_edit'] = $version;

						// Insert data into term_laboratorium_umum_history
						$this->db->insert('term_laboratorium_umum_history', $data);

						// Clone related data from term_laboratorium_umum_pemeriksaan to history table
						$this->db->where('transaksi_id', $transaksiId);
						$pemeriksaanData = $this->db->get('term_laboratorium_umum_pemeriksaan')->result_array();

						foreach ($pemeriksaanData as $row) {
								$row['id'] = ''; // Assuming 'id' is an auto-incremented primary key
								$row['versi_edit'] = $version;
								$this->db->on_duplicate('term_laboratorium_umum_pemeriksaan_history', $row);
						}
				}
		}

    private function updateTermLaboratorium($transaksiId) {
        $data = [
            'jumlah_edit' => $this->getVersionInfo($transaksiId) + 1,
						'pendaftaran_id' => $this->input->post('pendaftaran_id'),
						'pasien_id' => $this->input->post('pasien_id'),
						'jenis_pemeriksaan' => $this->input->post('jenis_pemeriksaan'),
						'rencana_pemeriksaan' => YMDFormat($this->input->post('rencana_pemeriksaan')),
						'tujuan_laboratorium' => $this->input->post('tujuan_laboratorium'),
						'dokter_perujuk_id' => $this->input->post('dokter_perujuk_id'),
						'dokter_peminta_id' => $this->input->post('dokter_peminta_id'),
						'diagnosa' => $this->input->post('diagnosa'),
						'catatan_permintaan' => $this->input->post('catatan_permintaan'),
						'waktu_permintaan' => YMDTimeFormat($this->input->post('tanggal_permintaan') . ' ' . $this->input->post('waktu_permintaan')),
						'prioritas' => $this->input->post('prioritas'),
						'pasien_puasa' => $this->input->post('pasien_puasa'),
						'pengiriman_hasil' => $this->input->post('pengiriman_hasil'),
						'status_pemeriksaan' => 2, // Menunggu Pemeriksaan
						'edited_ppa' => $this->input->post('ppa_id'),
						'edited_at' => date("Y-m-d H:i:s"),
        ];

        $this->db->where('id', $transaksiId);
        $this->db->update('term_laboratorium_umum', $data);
    }

    private function deleteTermLaboratoriumPemeriksaan($transaksiId) {
        $this->db->where('transaksi_id', $transaksiId);
        $this->db->delete('term_laboratorium_umum_pemeriksaan');
    }

		private function getVersionInfo($transaksiId) {
				$this->db->select('jumlah_edit');
				$this->db->where('id', $transaksiId);
				$result = $this->db->get('term_laboratorium_umum')->row();

				return (!empty($result)) ? $result->jumlah_edit : 0;
		}

		public function splitOrder($transaksi_id)
		{
				$originalHeader = $this->db->get_where('term_laboratorium_umum', ['id' => $transaksi_id])->row_array();
				unset($originalHeader['id']);

				// Panggil stored procedure GetNoLaboratorium untuk mendapatkan nomor_laboratorium baru
				$query = $this->db->query("SELECT GetNoLaboratorium() AS nomor_laboratorium");
				$result = $query->row_array();
				$query->next_result();
				$query->free_result();

				$originalHeader['nomor_laboratorium'] = $result['nomor_laboratorium'];

				// Insert data baru ke tabel term_laboratorium dan dapatkan id baru
				$this->db->insert('term_laboratorium_umum', $originalHeader);
				$newHeaderId = $this->db->insert_id();

				return $newHeaderId;
		}

		public function updatePemeriksaanWithSplit($transaksiId, $newTransaksiId, $selectedOrderIds)
		{
				foreach ($selectedOrderIds as $orderId) {
						$index = '';

						// Get dokter information
						$dokter = $this->getDokterInfo($newTransaksiId);
						$pemeriksaan = get_by_field('id', $orderId, 'term_laboratorium_umum_pemeriksaan');
						$tarif = get_by_field('id', $pemeriksaan->idlaboratorium, 'mtarif_laboratorium');
						$statusPemeriksaan = $pemeriksaan->status;

						// CASE : CHILD GET PARENT HEADER
						$query = $this->db->query("call spGetParentFromChild('" . $tarif->path . "')");
						$result = $query->result();

						$query->next_result();
						$query->free_result();

						foreach ($result as $parent) {
							$tindakan = [];
							$tindakan['transaksi_id'] = $newTransaksiId;
							$tindakan['idlaboratorium'] = $parent->id;
							$tindakan['kelas'] = 0;
							$tindakan['namatarif'] = $parent->nama;
							$tindakan['jasasarana'] = 0;
							$tindakan['jasapelayanan'] = 0;
							$tindakan['bhp'] = 0;
							$tindakan['biayaperawatan'] = 0;
							$tindakan['total'] = 0;
							$tindakan['kuantitas'] = 0;
							$tindakan['diskon'] = 0;
							$tindakan['totalkeseluruhan'] = 0;
							$tindakan['statusrincianpaket'] = 2;
							$tindakan['index'] = 1;
							$tindakan['st_paket'] = 0;
							$tindakan['status'] = $statusPemeriksaan;

							$tindakan['iddokter'] = $dokter->id;
							$tindakan['potongan_rs'] = $dokter->potongan_rs;
							$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

							$this->db->on_duplicate('term_laboratorium_umum_pemeriksaan', $tindakan);
						}
						
						// NORMAL CASE
						$tindakan = [];
						$tindakan['transaksi_id'] = $newTransaksiId ;
						$tindakan['idlaboratorium'] = $pemeriksaan->idlaboratorium;
						$tindakan['kelas'] = $pemeriksaan->kelas;
						$tindakan['namatarif'] = $pemeriksaan->namatarif;
						$tindakan['jasasarana'] = RemoveComma($pemeriksaan->jasasarana);
						$tindakan['jasapelayanan'] = RemoveComma($pemeriksaan->jasapelayanan);
						$tindakan['bhp'] = RemoveComma($pemeriksaan->bhp);
						$tindakan['biayaperawatan'] = RemoveComma($pemeriksaan->biayaperawatan);
						$tindakan['total'] = RemoveComma($pemeriksaan->total);
						$tindakan['kuantitas'] = RemoveComma($pemeriksaan->kuantitas);
						$tindakan['diskon'] = RemoveComma($pemeriksaan->diskon);
						$tindakan['totalkeseluruhan'] = RemoveComma($pemeriksaan->totalkeseluruhan);
						$tindakan['statusverifikasi'] = $pemeriksaan->statusverifikasi;
						$tindakan['index'] = $index;
						$tindakan['st_paket'] = 0;
						$tindakan['status'] = $statusPemeriksaan;

						$tindakan['iddokter'] = $dokter->id;
						$tindakan['potongan_rs'] = $dokter->potongan_rs;
						$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

						if ($this->db->on_duplicate('term_laboratorium_umum_pemeriksaan', $tindakan)) {
							// GET KELOMPOK, PAKET, PATH ID LAB
							$tarif = get_by_field('id', $pemeriksaan->idlaboratorium, 'mtarif_laboratorium');
							$idkelompok = $tarif->idkelompok;
							$idpaket = $tarif->idpaket;
							$path = $tarif->path;

							// CASE : CHILD KELOMPOK & NON PAKET
							if ($idkelompok == 1 && $idpaket == 0) {
								$rincianPaket = $this->getListRincianPaket($path);
								foreach ($rincianPaket as $row) {
									$rincian = [];
									$rincian['transaksi_id'] = $newTransaksiId ;
									$rincian['kelas'] = $row->kelas;
									$rincian['namatarif'] = $row->nama;
									$rincian['idlaboratorium'] = $row->id;
									$rincian['jasasarana'] = $row->jasasarana;
									$rincian['jasapelayanan'] = $row->jasapelayanan;
									$rincian['bhp'] = $row->bhp;
									$rincian['biayaperawatan'] = $row->biayaperawatan;
									$rincian['total'] = $row->total;
									$rincian['kuantitas'] = 1;
									$rincian['diskon'] = 0;
									$rincian['totalkeseluruhan'] = $row->total;
									$rincian['statusrincianpaket'] = 1;
									$rincian['index'] = $index;
									$rincian['st_paket'] = 1;
									$rincian['status'] = $statusPemeriksaan;

									$tindakan['iddokter'] = $dokter->id;
									$tindakan['potongan_rs'] = $dokter->potongan_rs;
									$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

									$this->db->on_duplicate('term_laboratorium_umum_pemeriksaan', $rincian);
								}
							}

							// CASE : CHILD KELOMPOK & PAKET
							if ($idkelompok == 1 && $idpaket == 1) {
								$rincianPaket = $this->getListRincianPaket($path);
								foreach ($rincianPaket as $row) {
									$rincian = [];
									$rincian['transaksi_id'] = $newTransaksiId ;
									$rincian['idlaboratorium'] = $row->id;
									$rincian['kelas'] = $row->kelas;
									$rincian['namatarif'] = $row->nama;
									$rincian['jasasarana'] = 0;
									$rincian['jasapelayanan'] = 0;
									$rincian['bhp'] = 0;
									$rincian['biayaperawatan'] = 0;
									$rincian['total'] = 0;
									$rincian['kuantitas'] = 0;
									$rincian['diskon'] = 0;
									$rincian['totalkeseluruhan'] = 0;
									$rincian['statusrincianpaket'] = 1;
									$rincian['index'] = $index;
									$rincian['st_paket'] = 1;
									$rincian['status'] = $statusPemeriksaan;

									$tindakan['iddokter'] = $dokter->id;
									$tindakan['potongan_rs'] = $dokter->potongan_rs;
									$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

									$this->db->on_duplicate('term_laboratorium_umum_pemeriksaan', $rincian);
								}
							}
						}
						
						$this->db->where('id', $orderId);
						$this->db->where('transaksi_id', $transaksiId);
						$this->db->delete('term_laboratorium_umum_pemeriksaan');
				}
		}

		private function getDokterInfo($transaksi_id)
		{
				$query = $this->db->query("SELECT
							mdokter.id,
							(CASE
									WHEN DATE_FORMAT(term_laboratorium_umum.rencana_pemeriksaan, '%H:%i') < '12:00' THEN
											mdokter.potonganrspagi
									ELSE mdokter.potonganrssiang
							END) AS potongan_rs,
							mdokter.pajak AS pajak_dokter
					FROM term_laboratorium_umum
					JOIN mdokter ON mdokter.id = term_laboratorium_umum.dokter_laboratorium
					WHERE term_laboratorium_umum.id = " . $transaksi_id);
				
				if ($query->num_rows() > 0) {
							return $query->row();
				} else {
					$default_result = (object) [
							'id' => 0,
							'potongan_rs' => 0,
							'pajak_dokter' => 0,
					];

					return $default_result;
				}
		}

		private function getListRincianPaket($path)
		{
			$this->db->select('mtarif_laboratorium.*, mtarif_laboratorium_detail.*');
			$this->db->join('mtarif_laboratorium_detail', 'mtarif_laboratorium_detail.idtarif = mtarif_laboratorium.id', 'LEFT');
			$this->db->where('mtarif_laboratorium.idpaket', 0);
			$this->db->where('mtarif_laboratorium.path LIKE', $path . '.%');
			$this->db->group_by('mtarif_laboratorium.id');
			$query = $this->db->get('mtarif_laboratorium');
			return $query->result();
		}

		public function getPetugasPengambilSampleByTujuanLaboratorium($tujuan_laboratorium) {
        $this->db->select('merm_pengaturan_tujuan_laboratorium_user_akses.iduser,
					mppa.nama AS nama_petugas');
        $this->db->from('merm_pengaturan_tujuan_laboratorium_user_akses');
        $this->db->join('mppa', 'mppa.id = merm_pengaturan_tujuan_laboratorium_user_akses.iduser');
        $this->db->where('merm_pengaturan_tujuan_laboratorium_user_akses.idpengaturan', $tujuan_laboratorium);
        
        return $this->db->get()->result();
    }

		public function getPetugasPengujianByTujuanLaboratorium($tujuan_laboratorium) {
        $this->db->select('merm_pengaturan_tujuan_laboratorium_user_akses.iduser,
					mppa.nama AS nama_petugas');
        $this->db->from('merm_pengaturan_tujuan_laboratorium_user_akses');
        $this->db->join('mppa', 'mppa.id = merm_pengaturan_tujuan_laboratorium_user_akses.iduser');
        $this->db->where('merm_pengaturan_tujuan_laboratorium_user_akses.idpengaturan', $tujuan_laboratorium);
        
        return $this->db->get()->result();
    }

		public function getPetugasPenginputanHasilByTujuanLaboratorium($tujuan_laboratorium) {
        $this->db->select('merm_pengaturan_tujuan_laboratorium_user_akses.iduser,
					mppa.nama AS nama_petugas');
        $this->db->from('merm_pengaturan_tujuan_laboratorium_user_akses');
        $this->db->join('mppa', 'mppa.id = merm_pengaturan_tujuan_laboratorium_user_akses.iduser');
        $this->db->where('merm_pengaturan_tujuan_laboratorium_user_akses.idpengaturan', $tujuan_laboratorium);
        
        return $this->db->get()->result();
    }

		public function getPetugasValidasiHasilByTujuanLaboratorium($tujuan_laboratorium) {
        $this->db->select('merm_pengaturan_tujuan_laboratorium_user_akses.iduser,
					mppa.nama AS nama_petugas');
        $this->db->from('merm_pengaturan_tujuan_laboratorium_user_akses');
        $this->db->join('mppa', 'mppa.id = merm_pengaturan_tujuan_laboratorium_user_akses.iduser');
        $this->db->where('merm_pengaturan_tujuan_laboratorium_user_akses.idpengaturan', $tujuan_laboratorium);
        
        return $this->db->get()->result();
    }

		public function getPetugasPengirimanHasilByTujuanLaboratorium($tujuan_laboratorium) {
        $this->db->select('merm_pengaturan_tujuan_laboratorium_user_akses.iduser,
					mppa.nama AS nama_petugas');
        $this->db->from('merm_pengaturan_tujuan_laboratorium_user_akses');
        $this->db->join('mppa', 'mppa.id = merm_pengaturan_tujuan_laboratorium_user_akses.iduser');
        $this->db->where('merm_pengaturan_tujuan_laboratorium_user_akses.idpengaturan', $tujuan_laboratorium);
        
        return $this->db->get()->result();
    }

		public function getPetugasPenerimaanHasilByTujuanLaboratorium($tujuan_laboratorium) {
        $this->db->select('merm_pengaturan_tujuan_laboratorium_user_akses.iduser,
					mppa.nama AS nama_petugas');
        $this->db->from('merm_pengaturan_tujuan_laboratorium_user_akses');
        $this->db->join('mppa', 'mppa.id = merm_pengaturan_tujuan_laboratorium_user_akses.iduser');
        $this->db->where('merm_pengaturan_tujuan_laboratorium_user_akses.idpengaturan', $tujuan_laboratorium);
        
        return $this->db->get()->result();
    }

		public function getDokterByTujuanLaboratorium($tujuan_laboratorium) {
        $this->db->select('merm_pengaturan_tujuan_laboratorium_dokter.iddokter,
					mdokter.nama AS nama_dokter,
					merm_pengaturan_tujuan_laboratorium_dokter.status_default');
        $this->db->from('merm_pengaturan_tujuan_laboratorium_dokter');
        $this->db->join('mdokter', 'mdokter.id = merm_pengaturan_tujuan_laboratorium_dokter.iddokter');
        $this->db->where('merm_pengaturan_tujuan_laboratorium_dokter.idpengaturan', $tujuan_laboratorium);
        
        return $this->db->get()->result();
    }

		public function getDokterPenanggungJawabByTujuanLaboratorium($tujuan_laboratorium) {
        $this->db->select('merm_pengaturan_tujuan_laboratorium.dokter_penanggung_jawab AS id');
        $this->db->from('merm_pengaturan_tujuan_laboratorium');
        $this->db->where('merm_pengaturan_tujuan_laboratorium.id', $tujuan_laboratorium);
        
        return $this->db->get()->row();
    }

		public function getPemeriksaanData($transaksi_id) {
        $this->db->select('namatarif AS nama_pemeriksaan,
					total AS harga, kuantitas, diskon, totalkeseluruhan AS total_keseluruhan');
        $this->db->where('statusrincianpaket', 0);
        $this->db->where('transaksi_id', $transaksi_id);
        $query = $this->db->get('term_laboratorium_umum_pemeriksaan');
        
        return $query->result_array();
    }

		public function getIdTindakanByPendaftaran($idpendaftaran) {
        $this->db->select('id');
        $this->db->where('idpendaftaran', $idpendaftaran);
        $query = $this->db->get('tpoliklinik_tindakan');

        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        }

        return false;
    }

		public function getIdRujukanByIdTindakan($idtindakan) {
        $this->db->select('id');
        $this->db->where('idtindakan', $idtindakan);
        $query = $this->db->get('trujukan_laboratorium');

        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        }

        return false;
    }

		public function getDataToClone($transaksi_id) {
        $this->db->where('transaksi_id', $transaksi_id);
        $query = $this->db->get('term_laboratorium_umum_pemeriksaan');

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return false;
    }

		public function getTarifByKelas($transaksi_id, $kelas_id) {
			$query = $this->db->select('SUM(mtarif_laboratorium_detail.total * term_laboratorium_umum_pemeriksaan.kuantitas) AS total')
            ->from('mtarif_laboratorium_detail')
            ->join('term_laboratorium_umum_pemeriksaan', 'mtarif_laboratorium_detail.idtarif = term_laboratorium_umum_pemeriksaan.idlaboratorium', 'left')
            ->where('term_laboratorium_umum_pemeriksaan.transaksi_id', $transaksi_id)
            ->where('mtarif_laboratorium_detail.kelas', $kelas_id)
            ->get();

        return $query->row();
		}
		
		public function getHasilPemeriksaanTerakhir($transaksi_id) {
				$this->db->select('MAX(id) AS hasil_pemeriksaan_id');
				$this->db->from('term_laboratorium_umum_hasil_pemeriksaan');
				$this->db->where('transaksi_id', $transaksi_id);
				$this->db->group_by('transaksi_id');

				$query = $this->db->get();

        if ($query->num_rows() > 0) {
						return $query->row()->hasil_pemeriksaan_id;
				} else {
						return null;
				}
		}

		public function getDataPasien($pasien_id)
    {
        $this->db->select('mfpasien.*, referensi_jenis_kelamin.ref AS jenis_kelamin');
        $this->db->join('merm_referensi referensi_jenis_kelamin', 'referensi_jenis_kelamin.nilai = mfpasien.jenis_kelamin AND referensi_jenis_kelamin.ref_head_id = 1', 'LEFT');
        $this->db->where('mfpasien.id', $pasien_id);

        $query = $this->db->get('mfpasien');
        return $query->row();
    }
}
