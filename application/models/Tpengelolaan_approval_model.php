<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tpengelolaan_approval_model extends CI_Model
{
  public function list_kategori(){
	   $q="SELECT id,nama from mdokter_kategori 
				
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
  
   public function get_header($id){
	   $q="SELECT *
			FROM `thonor_approval_head` H
			WHERE H.id='$id'
			";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   
   public function list_bagi_hasil(){
	   $q="SELECT id,nama from mbagi_hasil 
				WHERE mbagi_hasil.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   
   public function simpan_proses_peretujuan($id) {
		$q="SELECT S.id,S.step,S.proses_setuju,S.proses_tolak,U.`name` as user_nama,S.iduser FROM tbagi_hasil_bayar_head H
			LEFT JOIN mbagi_hasil_logic S ON S.mbagi_hasil_id=H.mbagi_hasil_id AND S.`status`='1'
			LEFT JOIN musers U ON U.id=S.iduser
			WHERE H.id='$id'  AND calculate_logic(S.operand,H.nominal_dibagikan,S.nominal) = 1
			ORDER BY S.step ASC,S.id ASC";
		$list_user=$this->db->query($q)->result();
		$step=999;
		$idlogic='';
		foreach ($list_user as $row){
			if ($row->step < $step){
				$step=$row->step;
			}
			$data=array(
				'tbagi_hasil_bayar_id' => $id,
				'step' => $row->step,
				// 'idlogic' => $row->idlogic,
				'iduser' => $row->iduser,
				'user_nama' => $row->user_nama,
				'proses_setuju' => $row->proses_setuju,
				'proses_tolak' => $row->proses_tolak,
				'approve' => 0,
			);
			// $idlogic=$row->idlogic;
			$this->db->insert('tbagi_hasil_bayar_approval', $data);
		}
		
		$this->db->update('tbagi_hasil_bayar_head',array('status'=>2),array('id'=>$id));
		return $this->db->update('tbagi_hasil_bayar_approval',array('st_aktif'=>1),array('tbagi_hasil_bayar_id'=>$id,'step'=>$step));
		
		
		return $this->db->update('rka_pengajuan', $data);
		
		
    }

}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
