<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_eresep_print_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_eresep_setting_label(){
		$q="SELECT * FROM setting_eresep_print_label H ";
		return $this->db->query($q)->row_array();
	}
	public function get_eresep_setting(){
		$q="SELECT * FROM setting_eresep_print H";
		return $this->db->query($q)->row_array();
	}
	function save_eresep(){
		$id =1;
		// $this->judul_header = $this->input->post('judul_header');
		// $this->judul_footer = $this->input->post('judul_footer');
		$this->st_edit_catatan = ($this->input->post('st_edit_catatan')=='on'?1:0);
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->lama_edit = $this->input->post('lama_edit');
		$this->orang_edit = $this->input->post('orang_edit');
		$this->st_hapus_catatan = $this->input->post('st_hapus_catatan');
		$this->lama_hapus = $this->input->post('lama_hapus');
		$this->orang_hapus = $this->input->post('orang_hapus');
		$this->st_duplikasi_catatan = $this->input->post('st_duplikasi_catatan');
		$this->lama_duplikasi = $this->input->post('lama_duplikasi');
		$this->orang_duplikasi = $this->input->post('orang_duplikasi');
		$this->st_iter_default = $this->input->post('st_iter_default');
		$this->max_iter = $this->input->post('max_iter');
		$this->jml_iter = $this->input->post('jml_iter');
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_eresep_print', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_eresep_label(){
		// print_r($this->input->post());exit;
		$id =1;
		$this->nama_rumah_sakit = $this->input->post('nama_rumah_sakit');
		$this->alamat = $this->input->post('alamat');
		$this->telepone = $this->input->post('telepone');
		$this->email = $this->input->post('email');
		$this->website = $this->input->post('website');
		$this->judul_header_ina = $this->input->post('judul_header_ina');
		$this->judul_header_eng = $this->input->post('judul_header_eng');
		$this->apoteker = $this->input->post('apoteker');
		$this->sipa = $this->input->post('sipa');
		$this->footer_ina = $this->input->post('footer_ina');
		$this->footer_eng = $this->input->post('footer_eng');
		$this->st_presciber = ($this->input->post('st_presciber')=='on'?1:0);
		$this->presciber_ina = $this->input->post('presciber_ina');
		$this->presciber_eng = $this->input->post('presciber_eng');
		$this->st_pasien = ($this->input->post('st_pasien')=='on'?1:0);
		$this->pasien_ina = $this->input->post('pasien_ina');
		$this->pasien_eng = $this->input->post('pasien_eng');
		$this->st_noreg = ($this->input->post('st_noreg')=='on'?1:0);
		$this->noreg_ina = $this->input->post('noreg_ina');
		$this->noreg_eng = $this->input->post('noreg_eng');
		$this->st_tl = ($this->input->post('st_tl')=='on'?1:0);
		$this->tl_ina = $this->input->post('tl_ina');
		$this->tl_eng = $this->input->post('tl_eng');
		$this->st_noresep = ($this->input->post('st_noresep')=='on'?1:0);
		$this->noresep_ina = $this->input->post('noresep_ina');
		$this->noresep_eng = $this->input->post('noresep_eng');
		$this->st_jenis_pasien = ($this->input->post('st_jenis_pasien')=='on'?1:0);
		$this->jenis_pasien_ina = $this->input->post('jenis_pasien_ina');
		$this->jenis_pasien_eng = $this->input->post('jenis_pasien_eng');
		$this->st_alergi = ($this->input->post('st_alergi')=='on'?1:0);
		$this->alergi_ina = $this->input->post('alergi_ina');
		$this->alergi_eng = $this->input->post('alergi_eng');
		$this->st_diagnosa = ($this->input->post('st_diagnosa')=='on'?1:0);
		$this->diagnosa_ina = $this->input->post('diagnosa_ina');
		$this->diagnosa_eng = $this->input->post('diagnosa_eng');
		$this->st_bb = ($this->input->post('st_bb')=='on'?1:0);
		$this->bb_ina = $this->input->post('bb_ina');
		$this->bb_eng = $this->input->post('bb_eng');
		$this->st_tb = ($this->input->post('st_tb')=='on'?1:0);
		$this->tb_ina = $this->input->post('tb_ina');
		$this->tb_eng = $this->input->post('tb_eng');
		$this->st_luas_permukaan = ($this->input->post('st_luas_permukaan')=='on'?1:0);
		$this->luas_permukaan_ina = $this->input->post('luas_permukaan_ina');
		$this->luas_permukaan_eng = $this->input->post('luas_permukaan_eng');
		$this->st_menyusui = ($this->input->post('st_menyusui')=='on'?1:0);
		$this->menyusui_ina = $this->input->post('menyusui_ina');
		$this->menyusui_eng = $this->input->post('menyusui_eng');
		$this->st_puasa = ($this->input->post('st_puasa')=='on'?1:0);
		$this->puasa_ina = $this->input->post('puasa_ina');
		$this->puasa_eng = $this->input->post('puasa_eng');
		$this->st_prioritas = ($this->input->post('st_prioritas')=='on'?1:0);
		$this->prioritas_ina = $this->input->post('prioritas_ina');
		$this->prioritas_eng = $this->input->post('prioritas_eng');
		$this->st_pulang = ($this->input->post('st_pulang')=='on'?1:0);
		$this->pulang_ina = $this->input->post('pulang_ina');
		$this->pulang_eng = $this->input->post('pulang_eng');
		$this->st_ginjal = ($this->input->post('st_ginjal')=='on'?1:0);
		$this->ginjal_ina = $this->input->post('ginjal_ina');
		$this->ginjal_eng = $this->input->post('ginjal_eng');
		$this->st_catatan = ($this->input->post('st_catatan')=='on'?1:0);
		$this->catatan_ina = $this->input->post('catatan_ina');
		$this->catatan_eng = $this->input->post('catatan_eng');
		$this->st_interval = ($this->input->post('st_interval')=='on'?1:0);
		$this->interval_ina = $this->input->post('interval_ina');
		$this->interval_eng = $this->input->post('interval_eng');
		$this->st_dosis = ($this->input->post('st_dosis')=='on'?1:0);
		$this->dosis_ina = $this->input->post('dosis_ina');
		$this->dosis_eng = $this->input->post('dosis_eng');
		$this->st_rute = ($this->input->post('st_rute')=='on'?1:0);
		$this->rute_ina = $this->input->post('rute_ina');
		$this->rute_eng = $this->input->post('rute_eng');
		$this->st_aturan = ($this->input->post('st_aturan')=='on'?1:0);
		$this->aturan_ina = $this->input->post('aturan_ina');
		$this->aturan_eng = $this->input->post('aturan_eng');
		$this->st_waktu = ($this->input->post('st_waktu')=='on'?1:0);
		$this->waktu_ina = $this->input->post('waktu_ina');
		$this->waktu_eng = $this->input->post('waktu_eng');
		$this->st_nama_racikan = ($this->input->post('st_nama_racikan')=='on'?1:0);
		$this->nama_racikan_ina = $this->input->post('nama_racikan_ina');
		$this->nama_racikan_eng = $this->input->post('nama_racikan_eng');
		$this->st_qty = ($this->input->post('st_qty')=='on'?1:0);
		$this->qty_ina = $this->input->post('qty_ina');
		$this->qty_eng = $this->input->post('qty_eng');
		$this->st_qty_obat = ($this->input->post('st_qty_obat')=='on'?1:0);
		$this->qty_obat_ina = $this->input->post('qty_obat_ina');
		$this->qty_obat_eng = $this->input->post('qty_obat_eng');
		$this->st_pembagian = ($this->input->post('st_pembagian')=='on'?1:0);
		$this->pembagian_ina = $this->input->post('pembagian_ina');
		$this->pembagian_eng = $this->input->post('pembagian_eng');
		$this->st_dosis_rac = ($this->input->post('st_dosis_rac')=='on'?1:0);
		$this->dosis_rac_ina = $this->input->post('dosis_rac_ina');
		$this->dosis_rac_eng = $this->input->post('dosis_rac_eng');
		$this->st_interval_rac = ($this->input->post('st_interval_rac')=='on'?1:0);
		$this->interval_rac_ina = $this->input->post('interval_rac_ina');
		$this->interval_rac_eng = $this->input->post('interval_rac_eng');
		$this->st_waktu_rac = ($this->input->post('st_waktu_rac')=='on'?1:0);
		$this->waktu_rac_ina = $this->input->post('waktu_rac_ina');
		$this->waktu_rac_eng = $this->input->post('waktu_rac_eng');
		$this->st_rute_rac = ($this->input->post('st_rute_rac')=='on'?1:0);
		$this->rute_rac_ina = $this->input->post('rute_rac_ina');
		$this->rute_rac_eng = $this->input->post('rute_rac_eng');
		$this->st_aturan_rac = ($this->input->post('st_aturan_rac')=='on'?1:0);
		$this->aturan_rac_ina = $this->input->post('aturan_rac_ina');
		$this->aturan_rac_eng = $this->input->post('aturan_rac_eng');
		$this->st_jenis_rac = ($this->input->post('st_jenis_rac')=='on'?1:0);
		$this->jenis_rac_ina = $this->input->post('jenis_rac_ina');
		$this->jenis_rac_eng = $this->input->post('jenis_rac_eng');
		$this->st_ttd = ($this->input->post('st_ttd')=='on'?1:0);
		$this->ttd_ina = $this->input->post('ttd_ina');
		$this->ttd_eng = $this->input->post('ttd_eng');
		$this->st_waktu_cetak = ($this->input->post('st_waktu_cetak')=='on'?1:0);
		$this->waktu_cetak_ina = $this->input->post('waktu_cetak_ina');
		$this->waktu_cetak_eng = $this->input->post('waktu_cetak_eng');
		$this->st_user = ($this->input->post('st_user')=='on'?1:0);
		$this->user_ina = $this->input->post('user_ina');
		$this->user_eng = $this->input->post('user_eng');
		$this->st_jml_cetak = ($this->input->post('st_jml_cetak')=='on'?1:0);
		$this->jml_cetak_ina = $this->input->post('jml_cetak_ina');
		$this->jml_cetak_eng = $this->input->post('jml_cetak_eng');
		$this->st_tanggal_cetak = ($this->input->post('st_tanggal_cetak')=='on'?1:0);
		$this->tanggal_cetak_ina = $this->input->post('tanggal_cetak_ina');
		$this->tanggal_cetak_eng = $this->input->post('tanggal_cetak_eng');
		$this->footer_notes_ina = $this->input->post('footer_notes_ina');
		$this->footer_notes_eng = $this->input->post('footer_notes_eng');
		$this->st_penerima = ($this->input->post('st_penerima')=='on'?1:0);
		$this->penerima_ina = $this->input->post('penerima_ina');
		$this->penerima_eng = $this->input->post('penerima_eng');
		$this->st_konfirmas = ($this->input->post('st_konfirmas')=='on'?1:0);
		$this->konfirmas_ina = $this->input->post('konfirmas_ina');
		$this->konfirmas_eng = $this->input->post('konfirmas_eng');
		$this->st_ambil = ($this->input->post('st_ambil')=='on'?1:0);
		$this->ambil_ina = $this->input->post('ambil_ina');
		$this->ambil_eng = $this->input->post('ambil_eng');
		$this->st_etiket = ($this->input->post('st_etiket')=='on'?1:0);
		$this->etiket_ina = $this->input->post('etiket_ina');
		$this->etiket_eng = $this->input->post('etiket_eng');
		$this->st_validasi = ($this->input->post('st_validasi')=='on'?1:0);
		$this->validasi_ina = $this->input->post('validasi_ina');
		$this->validasi_eng = $this->input->post('validasi_eng');
		$this->st_penyerah = ($this->input->post('st_penyerah')=='on'?1:0);
		$this->penyerah_ina = $this->input->post('penyerah_ina');
		$this->penyerah_eng = $this->input->post('penyerah_eng');
		$this->st_hasil = ($this->input->post('st_hasil')=='on'?1:0);
		$this->hasil_ina = $this->input->post('hasil_ina');
		$this->hasil_eng = $this->input->post('hasil_eng');
		$this->st_perubahan = ($this->input->post('st_perubahan')=='on'?1:0);
		$this->perubahan_ina = $this->input->post('perubahan_ina');
		$this->perubahan_eng = $this->input->post('perubahan_eng');
		$this->upload_header_logo(true);
			
		if ($this->db->update('setting_eresep_print', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	public function upload_header_logo($update = false)
    {
        if (!file_exists('assets/upload/antrian')) {
            mkdir('assets/upload/antrian', 0755, true);
        }

        if (isset($_FILES['header_logo'])) {
            if ($_FILES['header_logo']['name'] != '') {
                $config['upload_path'] = './assets/upload/antrian/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('header_logo')) {
                    $image_upload = $this->upload->data();
                    $this->header_logo = $image_upload['file_name'];

                    if ($update == true) {
                        $this->remove_image_header_logo($this->input->post('id'));
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
			// print_r('TIDAK ADA');exit;
                return true;
            }
					// print_r($this->foto);exit;
        } else {
			// print_r('TIDAK ADA');exit;
            return true;
        }
		
    }
	
	public function remove_image_header_logo($id)
    {
		$q="select header_logo From antrian_display H WHERE H.id='$id'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/antrian/'.$row->header_logo) && $row->header_logo !='') {
            unlink('./assets/upload/antrian/'.$row->header_logo);
        }
    }
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


