<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_reservasi_petugas_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_pendaftaran_setting(){
		$q="SELECT * FROM setting_reservasi_petugas H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function save_pendaftaran(){
		$id =1;
		// $this->peraturan = $this->input->post('peraturan');
		$this->pesan_berhasil = $this->input->post('pesan_berhasil');
		$this->st_auto_validasi = $this->input->post('st_auto_validasi');
		$this->st_antrian_tampil = $this->input->post('st_antrian_tampil');
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_reservasi_petugas', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	public function get_pendaftaran_rm_setting(){
		$q="SELECT * FROM setting_reservasi_petugas_rm H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function save_pendaftaran_rm(){
		$id =1;
		// $this->peraturan = $this->input->post('peraturan');
		$this->pesan_berhasil = $this->input->post('pesan_berhasil');
		$this->st_auto_validasi = $this->input->post('st_auto_validasi');
		$this->st_antrian_tampil = $this->input->post('st_antrian_tampil');
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
		// print_r($this);exit;
		if ($this->db->update('setting_reservasi_petugas_rm', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}
