<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpengaturan_printout_radiologi_ekspertise_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($tipe)
    {
        $this->db->where('tipe', $tipe);
        $query = $this->db->get('merm_pengaturan_printout_radiologi_expertise');
        return $query->row();
    }

    public function saveData()
    {
        $this->tipe = $_POST['tipe'];

        if ($_POST['tipe'] == 'FORMAT_2') {
            $this->alamat = $_POST['alamat'];
            $this->phone = $_POST['phone'];
            $this->website = $_POST['website'];
        }

        $this->nama_file = $_POST['nama_file'];
        $this->tampilkan_noregister = $_POST['tampilkan_noregister'];
        $this->tampilkan_nomedrec = $_POST['tampilkan_nomedrec'];
        $this->tampilkan_nama = $_POST['tampilkan_nama'];
        $this->tampilkan_kelompok_pasien = $_POST['tampilkan_kelompok_pasien'];
        $this->tampilkan_nama_asuransi = $_POST['tampilkan_nama_asuransi'];
        $this->tampilkan_alamat = $_POST['tampilkan_alamat'];
        $this->tampilkan_dokter_perujuk = $_POST['tampilkan_dokter_perujuk'];
        $this->tampilkan_nomor_radiologi = $_POST['tampilkan_nomor_radiologi'];
        $this->tampilkan_tanggal_lahir = $_POST['tampilkan_tanggal_lahir'];
        $this->tampilkan_umur = $_POST['tampilkan_umur'];
        $this->tampilkan_rujukan = $_POST['tampilkan_rujukan'];
        $this->tampilkan_barcode = $_POST['tampilkan_barcode'];
        $this->tampilkan_nama_pemeriksaan = $_POST['tampilkan_nama_pemeriksaan'];
        $this->tampilkan_klinis = $_POST['tampilkan_klinis'];
        $this->tampilkan_kesan = $_POST['tampilkan_kesan'];
        $this->tampilkan_usul = $_POST['tampilkan_usul'];
        $this->tampilkan_hasil = $_POST['tampilkan_hasil'];
        $this->tampilkan_flag_kritis = $_POST['tampilkan_flag_kritis'];
        $this->tampilkan_penanggung_jawab = $_POST['tampilkan_penanggung_jawab'];
        $this->tampilkan_waktu_input_order = $_POST['tampilkan_waktu_input_order'];
        $this->tampilkan_waktu_pemeriksaan = $_POST['tampilkan_waktu_pemeriksaan'];
        $this->tampilkan_waktu_expertise = $_POST['tampilkan_waktu_expertise'];
        $this->tampilkan_waktu_cetak = $_POST['tampilkan_waktu_cetak'];
        $this->tampilkan_jumlah_cetak = $_POST['tampilkan_jumlah_cetak'];
        $this->tampilkan_user_input_order = $_POST['tampilkan_user_input_order'];
        $this->tampilkan_user_pemeriksaan = $_POST['tampilkan_user_pemeriksaan'];
        $this->tampilkan_user_expertise = $_POST['tampilkan_user_expertise'];
        $this->tampilkan_user_cetak = $_POST['tampilkan_user_cetak'];
        $this->footer_notes = $_POST['footer_notes'];
        $this->footer_notes_eng = $_POST['footer_notes_eng'];

        if ($this->upload(true)) {
            $this->db->where('tipe', $_POST['tipe']);
            if ($this->db->update('merm_pengaturan_printout_radiologi_expertise', $this)) {
                return true;
            }
        }

        return false;
    }

    public function updateDataLabel($tipe, $data) {
        // Lakukan update ke dalam tabel Anda
        $this->db->where('tipe', $tipe);
        $this->db->update('merm_pengaturan_printout_radiologi_expertise', $data);

        // Kembalikan hasil update
        return $this->db->affected_rows() > 0;
    }

    public function upload($update = false)
    {
        if (!file_exists('assets/upload/pengaturan_printout_radiologi_expertise')) {
            mkdir('assets/upload/pengaturan_printout_radiologi_expertise', 0755, true);
        }

        if ($_POST['tipe'] == 'FORMAT_1') {
            // Logo Header
            if (isset($_FILES['logo_header'])) {
                if ($_FILES['logo_header']['name'] != '') {
                    $config['upload_path'] = './assets/upload/pengaturan_printout_radiologi_expertise/';
                    $config['allowed_types'] = 'jpg|jpeg|bmp|png';
                    $config['encrypt_name']  = true;

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('logo_header')) {
                        $image_upload = $this->upload->data();
                        $this->logo_header = $image_upload['file_name'];

                        if ($update == true) {
                            $this->removeImageLogoHeader($_POST['tipe']);
                        }

                        // Continue to the next upload section (Logo Footer)
                    } else {
                        $this->error_message = $this->upload->display_errors();
                        print_r($this->error_message);exit();
                        return false;
                    }
                }
            }

            // Logo Footer
            if (isset($_FILES['logo_footer'])) {
                if ($_FILES['logo_footer']['name'] != '') {
                    $config['upload_path'] = './assets/upload/pengaturan_printout_radiologi_expertise/';
                    $config['allowed_types'] = 'jpg|jpeg|bmp|png';
                    $config['encrypt_name']  = true;

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('logo_footer')) {
                        $image_upload = $this->upload->data();
                        $this->logo_footer = $image_upload['file_name'];

                        if ($update == true) {
                            $this->removeImageLogoFooter($_POST['tipe']);
                        }
                    } else {
                        $this->error_message = $this->upload->display_errors();
                        print_r($this->error_message);exit();
                        return false;
                    }
                }
            }
        }

        if ($_POST['tipe'] == 'FORMAT_2') {
            // Logo
            if (isset($_FILES['logo'])) {
                if ($_FILES['logo']['name'] != '') {
                    $config['upload_path'] = './assets/upload/pengaturan_printout_radiologi_expertise/';
                    $config['allowed_types'] = 'jpg|jpeg|bmp|png';
                    $config['encrypt_name']  = true;

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('logo')) {
                        $image_upload = $this->upload->data();
                        $this->logo = $image_upload['file_name'];

                        if ($update == true) {
                            $this->removeImageLogo($_POST['tipe']);
                        }

                        // Continue to the next upload section (Logo Footer)
                    } else {
                        $this->error_message = $this->upload->display_errors();
                        print_r($this->error_message);exit();
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public function removeImageLogo($tipe)
    {
        $row = $this->getSpecified($tipe);
        if (file_exists('./assets/upload/pengaturan_printout_radiologi_expertise/'.$row->logo) && $row->logo !='') {
            unlink('./assets/upload/pengaturan_printout_radiologi_expertise/'.$row->logo);
        }
    }

    public function removeImageLogoHeader($tipe)
    {
        $row = $this->getSpecified($tipe);
        if (file_exists('./assets/upload/pengaturan_printout_radiologi_expertise/'.$row->logo_header) && $row->logo_header !='') {
            unlink('./assets/upload/pengaturan_printout_radiologi_expertise/'.$row->logo_header);
        }
    }

    public function removeImageLogoFooter($tipe)
    {
        $row = $this->getSpecified($tipe);
        if (file_exists('./assets/upload/pengaturan_printout_radiologi_expertise/'.$row->logo_footer) && $row->logo_footer !='') {
            unlink('./assets/upload/pengaturan_printout_radiologi_expertise/'.$row->logo_footer);
        }
    }
}
