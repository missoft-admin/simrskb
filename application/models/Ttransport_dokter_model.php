<?php

class Ttransport_dokter_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllDokterUmum()
    {
        $this->db->where('idkategori', '1');
        $query = $this->db->get('mdokter');
        return $query->result();
    }

    public function getListJadwal($periode)
    {
        $this->db->select('mdokter_jadwal.*,
          dokterpagi.id AS iddokterpagi,
          dokterpagi.nama AS namadokterpagi,
          dokterpagi.nominaltransport AS transportpagi,
          doktersiang.id AS iddoktersiang,
          doktersiang.nama AS namadoktersiang,
          doktersiang.nominaltransport AS transportsiang,
          doktermalam.id AS iddoktermalam,
          doktermalam.nama AS namadoktermalam,
          doktermalam.nominaltransport AS transportmalam');
        $this->db->join('mdokter AS dokterpagi', 'dokterpagi.id = mdokter_jadwal.iddokter_pagi', 'LEFT');
        $this->db->join('mdokter AS doktersiang', 'doktersiang.id = mdokter_jadwal.iddokter_siang', 'LEFT');
        $this->db->join('mdokter AS doktermalam', 'doktermalam.id = mdokter_jadwal.iddokter_malam', 'LEFT');
        $this->db->where('periode', $periode);
        $query = $this->db->get('mdokter_jadwal');
        return $query->result();
    }
}
