<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Msetting_verifikasi_deposit_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {
        $this->db->select('msetting_verifikasi_deposit.*, makun_debit.namaakun AS namaakun_debit, makun_kredit.namaakun AS namaakun_kredit');
        $this->db->join('makun_nomor makun_debit', 'makun_debit.noakun = msetting_verifikasi_deposit.noakun_debit', 'LEFT');
        $this->db->join('makun_nomor makun_kredit', 'makun_kredit.noakun = msetting_verifikasi_deposit.noakun_kredit', 'LEFT');
        $this->db->where('msetting_verifikasi_deposit.status', '1');
        $query = $this->db->get('msetting_verifikasi_deposit');
        return $query->result();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('msetting_verifikasi_deposit');
        return $query->row();
    }

    public function getAkunSubmited()
    {
        $this->db->select('idmetode');
        $query = $this->db->get('msetting_verifikasi_deposit');
        $result = $query->result();

        $data = "";
        $numItems = count($result);
        $i = 0;
        foreach ($query->result() as $row) {
            $data .= $row->idmetode;
            if (++$i != $numItems) {
                $data .= ',';
            }
        }

        return explode(',', $data);
    }

    public function saveData()
    {
        $this->idmetode = $_POST['idmetode'];
        $this->noakun_debit = $_POST['noakun_debit'];
        $this->noakun_kredit = $_POST['noakun_kredit'];
	      $this->created_at = date('Y-m-d H:i:s');

        if ($this->db->insert('msetting_verifikasi_deposit', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->idmetode = $_POST['idmetode'];
        $this->noakun_debit = $_POST['noakun_debit'];
        $this->noakun_kredit = $_POST['noakun_kredit'];
	      $this->updated_at = date('Y-m-d H:i:s');

        if ($this->db->update('msetting_verifikasi_deposit', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->db->where('id', $id);
        if ($this->db->delete('msetting_verifikasi_deposit')) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
