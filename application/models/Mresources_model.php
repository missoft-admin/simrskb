<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mresources_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
    }

    public function getPathLevel($headerpath)
    {
        $strquery = "SELECT LEVEL + 1 AS level, CASE WHEN ( SELECT COUNT(headerpath) FROM mresources WHERE headerpath = t1.headerpath ) = 0 THEN CONCAT(t1.path, '.1') ELSE  CONCAT(t1.path,'.',  IF ( ( SELECT COUNT(headerpath) FROM mresources WHERE headerpath = t1.path ) = 0, ( SELECT COUNT(headerpath) FROM mresources WHERE headerpath = t1.path ) + 1, IF ( LENGTH(t1.path) = '1',( SELECT COUNT(headerpath) FROM mresources WHERE headerpath = t1.path ), ( SELECT COUNT(headerpath) FROM mresources WHERE headerpath = t1.path ) + 1 ) ) ) END AS path FROM mresources t1 WHERE t1.path = '".$headerpath."'";

        $query = $this->db->query($strquery);
        $row = $query->row();

        if ($query->num_rows() > 0) {
            $data['path']  = $row->path;
            $data['level'] = $row->level;
        } else {
            $data['path']  = 1;
            $data['level'] = 0;
        }

        return $data;
    }
	public function get_sub($id){
		$q="SELECT *from mmenu_sub S WHERE S.id_menu='$id' ORDER BY S.index2";
		$query=$this->db->query($q);
		return $query->result();
	}
    public function get_action($id){
		$q="SELECT *from mmenu_action A WHERE A.id_menu_sub='$id' AND status='1' ORDER BY A.index3";
		$query=$this->db->query($q);
		return $query->result();
	}
    public function get_utama(){
		$q="SELECT *from mmenu ORDER BY mmenu.`index` asc";
		$query=$this->db->query($q);
		return $query->result();
	}
    public function getAllParent($headerpath = 0, $level = 999)
    {
        # Get ID Root
        $this->db->select('MAX(CAST(headerpath AS SIGNED)) + 1 AS idroot');
        $this->db->where('status', '1');
        $query = $this->db->get('mresources');
        $row = $query->row();

        if ($headerpath != 0) {
            $idroot = $headerpath;
        } else {
            $idroot = ($row->idroot ? $row->idroot : 1);
        }

        # Get Option
        $this->db->where('status', '1');
        $this->db->order_by('path', 'ASC');
        $query  = $this->db->get('mresources');
        $result = $query->result();

        $selected = ($level == 0 ? 'selected' : '');
        $data = '<option value="'.$idroot.'" '.$selected.'>Root</option>';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                if ($headerpath != 0 && $level != 0) {
                    $selected = ($headerpath == $row->path ? 'selected' : '');
                } else {
                    $selected = '';
                }
                $data .= '<option value="'.$row->path.'" '.$selected.'>'.TreeView($row->level, $row->nama).'</option>';
            }
        }
        return $data;
    }

    public function saveData($data)
    {
        $this->db->insert('mresources', $data);
        return true;
    }
	public function simpan_sub($data)
    {
        $this->db->insert('mmenu_sub', $data);
        return true;
    }
	public function simpan_action($data)
    {
        $this->db->insert('mmenu_action', $data);
        return true;
    }
	public function simpan_menu($data)
    {
        $this->db->insert('mmenu', $data);
        return true;
    }
	public function edit_menu($data,$id)
    {
		$this->db->where('id',$id);
        $this->db->update('mmenu', $data);
        return true;
    }
	public function edit_sub($data,$id2)
    {
		$this->db->where('id2',$id2);
        $this->db->update('mmenu_sub', $data);
        return true;
    }
	public function edit_action($data,$id3)
    {
		$this->db->where('id3',$id3);
        $this->db->update('mmenu_action', $data);
        return true;
    }
	public function delete_action($data,$id3)
    {
		$this->db->where('id3',$id3);
        $this->db->update('mmenu_action', $data);
        return true;
    }

    public function getAllResource()
    {
        $this->db->order_by('path');

        $query = $this->db->get('mresources');
        return $query->result();
    }

    public function deleteResource($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('mresources');

        return true;
    }
}
