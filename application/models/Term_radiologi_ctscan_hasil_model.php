<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Term_radiologi_ctscan_hasil_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_tujuan_radiologi()
    {
        $user_id = $this->session->userdata('login_ppa_id');

        $this->db->select('merm_pengaturan_tujuan_radiologi.*');
        $this->db->from('merm_pengaturan_tujuan_radiologi');
        $this->db->join('merm_pengaturan_tujuan_radiologi_user_akses', 'merm_pengaturan_tujuan_radiologi_user_akses.idpengaturan = merm_pengaturan_tujuan_radiologi.id');
        $this->db->where('merm_pengaturan_tujuan_radiologi.status', '1');
        $this->db->where('merm_pengaturan_tujuan_radiologi.tipe_layanan', '3');
        $this->db->where('merm_pengaturan_tujuan_radiologi_user_akses.iduser', $user_id);

        $query = $this->db->get();
        return $query->result();
    }

    public function get_data_transaksi($transaksi_id)
    {
        $this->db->select('
            term_radiologi_ctscan.*,
            COALESCE ( tpoliklinik_pendaftaran.no_medrec, poliklinik_ranap.nomor_medrec) AS nomor_medrec,
            COALESCE ( tpoliklinik_pendaftaran.namapasien, poliklinik_ranap.nama_pasien) AS nama_pasien,
            COALESCE ( tpoliklinik_pendaftaran.nik, poliklinik_ranap.nomor_ktp) AS nomor_ktp,
            COALESCE ( referensi_jenis_kelamin.ref, poliklinik_ranap.jenis_kelamin) AS jenis_kelamin,
            COALESCE ( tpoliklinik_pendaftaran.email, poliklinik_ranap.email) AS email,
            COALESCE ( tpoliklinik_pendaftaran.alamatpasien, poliklinik_ranap.alamat_pasien) AS alamatpasien,
            COALESCE ( tpoliklinik_pendaftaran.tanggal_lahir, poliklinik_ranap.tanggal_lahir) AS tanggal_lahir,
            COALESCE ( tpoliklinik_pendaftaran.umurhari, poliklinik_ranap.umur_hari) AS umur_hari,
            COALESCE ( tpoliklinik_pendaftaran.umurbulan, poliklinik_ranap.umur_bulan) AS umur_bulan,
            COALESCE ( tpoliklinik_pendaftaran.umurtahun, poliklinik_ranap.umur_tahun) AS umur_tahun,
            COALESCE ( mpasien_kelompok.nama, poliklinik_ranap.kelompok_pasien) AS kelompok_pasien,
            COALESCE ( mrekanan.nama, poliklinik_ranap.rekanan_asuransi) AS nama_asuransi,
            (CASE
                WHEN term_radiologi_ctscan.asal_rujukan = 1 THEN "Poliklinik"
                WHEN term_radiologi_ctscan.asal_rujukan = 2 THEN "Instalasi Gawat Darurat"
                WHEN term_radiologi_ctscan.asal_rujukan = 3 THEN "Rawat Inap"
                WHEN term_radiologi_ctscan.asal_rujukan = 4 THEN "One Day Surgery"
            END) AS tipe_kunjungan,
            mpoliklinik.nama AS nama_poliklinik,
            (CASE
                WHEN term_radiologi_ctscan.asal_rujukan IN (1, 2) THEN
                    tpoliklinik_pendaftaran.nopendaftaran
                WHEN term_radiologi_ctscan.asal_rujukan IN (3, 4) THEN
                    poliklinik_ranap.nopendaftaran
            END) AS nomor_pendaftaran,
            term_radiologi_ctscan.prioritas AS prioritas_pemeriksaan,
            mdokter_perujuk.nama AS dokter_perujuk,
            mdokter_radiologi.nama AS dokter_radiologi,
            mdokter_radiologi.id AS dokter_radiologi_id,
            mdokter_radiologi.nip AS dokter_radiologi_nip,
            muser_created.nama AS user_created,
            muser_pemeriksa.nama AS user_input_pemeriksaan,
            muser_penginputan_foto.nama AS user_penginputan_foto,
            muser_penginputan_expertise.nama AS user_penginputan_expertise,
            term_radiologi_ctscan.created_at AS waktu_pembuatan'
        );
        $this->db->from('term_radiologi_ctscan');
        $this->db->join('(SELECT
            trawatinap_pendaftaran.id,
            trawatinap_pendaftaran.nopendaftaran,
            tpoliklinik_pendaftaran.kode_antrian,
            tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
            tpoliklinik_pendaftaran.idpasien AS pasien_id,
            tpoliklinik_pendaftaran.namapasien AS nama_pasien,
            tpoliklinik_pendaftaran.nik AS nomor_ktp,
            referensi_jenis_kelamin.ref AS jenis_kelamin,
            tpoliklinik_pendaftaran.email,
            tpoliklinik_pendaftaran.alamatpasien AS alamat_pasien,
            tpoliklinik_pendaftaran.tanggal_lahir,
            tpoliklinik_pendaftaran.umurhari AS umur_hari,
            tpoliklinik_pendaftaran.umurbulan AS umur_bulan,
            tpoliklinik_pendaftaran.umurtahun AS umur_tahun,
            mpasien_kelompok.nama AS kelompok_pasien,
            mrekanan.nama AS rekanan_asuransi
        FROM
            trawatinap_pendaftaran
        INNER JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
        INNER JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
        LEFT JOIN mrekanan ON mrekanan.id = trawatinap_pendaftaran.idrekanan
        INNER JOIN merm_referensi referensi_jenis_kelamin ON referensi_jenis_kelamin.nilai = tpoliklinik_pendaftaran.jenis_kelamin AND referensi_jenis_kelamin.ref_head_id = 1) AS poliklinik_ranap', 'term_radiologi_ctscan.pendaftaran_id = poliklinik_ranap.id AND term_radiologi_ctscan.asal_rujukan IN (3, 4)', 'LEFT');
		$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_radiologi_ctscan.pendaftaran_id AND term_radiologi_ctscan.asal_rujukan IN (1, 2)', 'LEFT');
        $this->db->join('merm_referensi referensi_jenis_kelamin', 'referensi_jenis_kelamin.nilai = tpoliklinik_pendaftaran.jenis_kelamin AND referensi_jenis_kelamin.ref_head_id = 1', 'LEFT');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
        $this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_radiologi_ctscan.dokter_perujuk_id', 'LEFT');
		$this->db->join('mdokter mdokter_radiologi', 'mdokter_radiologi.id = term_radiologi_ctscan.dokter_radiologi', 'LEFT');
        $this->db->join('mppa muser_created', 'muser_created.id = term_radiologi_ctscan.created_ppa', 'LEFT');
        $this->db->join('mppa muser_pemeriksa', 'muser_pemeriksa.id = term_radiologi_ctscan.petugas_pemeriksaan', 'LEFT');
		$this->db->join('mppa muser_penginputan_foto', 'muser_penginputan_foto.id = term_radiologi_ctscan.petugas_penginputan_foto', 'LEFT');
		$this->db->join('mppa muser_penginputan_expertise', 'muser_penginputan_expertise.id = term_radiologi_ctscan.petugas_penginputan_expertise', 'LEFT');
        $this->db->where('term_radiologi_ctscan.id', $transaksi_id);

        $query = $this->db->get();
        return $query->row();
    }

    public function update()
	{
		$id = $_POST['id'];
        $currentStatus = $this->db->get_where('term_radiologi_ctscan', array('id' => $id))->row()->status_pemeriksaan;

        if ($currentStatus <= 5) {
            $this->db->set('status_pemeriksaan', '5');
            $this->db->where('id', $id);
            $this->db->update('term_radiologi_ctscan');
        }

        return true;
	}

    public function get_daftar_pemeriksaan($transaksi_id)
	{
		$this->db->where('term_radiologi_ctscan_pemeriksaan.transaksi_id', $transaksi_id);
		$this->db->where('term_radiologi_ctscan_pemeriksaan.status', '1');
		$query = $this->db->get('term_radiologi_ctscan_pemeriksaan');
		return $query->result();
	}

    public function get_foto_radiologi_pemeriksaan($pemeriksaanId)
    {
		$q = "SELECT * from term_radiologi_ctscan_pemeriksaan_file WHERE pemeriksaan_id = '$pemeriksaanId'";
		$row= $this->db->query($q)->result();

		$html = '';
		foreach ($row as $r){
            $html .= '<div class="col-md-6 mb-4">';
            $html .= '<div style="position: relative; padding: 10px; border: 1px solid #dedede; border-radius: 10px;">';

            $fileExtension = pathinfo($r->filename, PATHINFO_EXTENSION);
            if (strtolower($fileExtension) === 'dcm') {
                $html .= '<img src="'.base_url().'assets/upload/foto_radiologi_ctscan/DICOM.png" width="100%"><br><br>';
            } else {
                $html .= '<img src="'.base_url().'assets/upload/foto_radiologi_ctscan/'.$r->filename.'" width="100%"><br><br>';
            }
            
            $html .= '<div style="position: absolute; bottom: 80px; left: 50%; transform: translateX(-50%);">';
            
            // Button Preview
            if (strtolower($fileExtension) === 'dcm') {
                $html .= '<a href="#" data-toggle="tooltip" title="Preview" class="btn btn-primary btn-sm" onclick="previewDICOM(' . $r->id . ', 1)"><i class="fa fa-search"></i><br>Preview</a>';
            } else {
                $html .= '<a href="#" data-toggle="tooltip" title="Preview" class="btn btn-primary btn-sm" onclick="previewPhoto(' . $r->id . ', 3, ' . $r->pemeriksaan_id . ')"><i class="fa fa-search"></i><br>Preview</a>';
                // $html .= '<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" style="display: contents;">
                //     <a href="'.base_url().'assets/upload/foto_radiologi_xray/'.$r->filename.'" class="btn btn-primary btn-sm" data-caption="" data-width="1200" data-height="900" itemprop="contentUrl">
                //         <i class="fa fa-search"></i><br>Preview
                //     </a>
                // </figure>';
            }
            
            $html .= '&nbsp;';

            // Button Hapus
            $html .= '<a href="#" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm" onclick="removeFotoRadiologi(' . $r->id . ', ' . $r->pemeriksaan_id . ')"><i class="fa fa-trash-o"></i><br>Hapus</a>';
            $html .= '</div>';
            $html .= '<h6>' . $r->filename .'</h6>';
            $html .= '<i class="text-primary">' . HumanDateLong($r->upload_at).'</i>';
            $html .= '</div>';
            $html .= '</div>';
        }

		return $html;
	}
}
