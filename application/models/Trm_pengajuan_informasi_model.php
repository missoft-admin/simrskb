<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_pengajuan_informasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $row = $this->db->select("trm_pengajuan_informasi.*, musers.name AS petugas")
            ->join("musers", 'musers.id = trm_pengajuan_informasi.created_by')
            ->where("trm_pengajuan_informasi.id", $id)
            ->get("trm_pengajuan_informasi")
            ->row();

        return $row;
    }

    public function getWaktuEstimasi($jenisPengajuan)
    {
        $row = $this->db->select("estimasi")
            ->where("id", $jenisPengajuan)
            ->get("mpengajuan_skd")
            ->row();

        if (isset($row->estimasi)) {
            return $row->estimasi;
        } else {
            return 0;
        }
    }

    public function getUrutanPengajuan($idBerkas, $jenisPengajuan)
    {
        $row = $this->db->select("COUNT(id) + 1 AS nomor_urut")
            ->where("idberkas", $idBerkas)
            ->where("jenis_pengajuan", $jenisPengajuan)
            ->where("status", '1')
            ->get("trm_pengajuan_informasi_detail")
            ->row();

        return $row->nomor_urut;
    }

    public function getSettingTarifPengajuan($jenisPengajuan, $nomorUrutPengajuan)
    {
        $row = $this->db->select("msetting_tarif_pengajuan_detail.idsetting, 
            msetting_tarif_pengajuan.group_diskon_all, 
            msetting_tarif_pengajuan_detail.tarif_rs, 
            msetting_tarif_pengajuan_detail.tarif_dokter, 
            msetting_tarif_pengajuan_detail.total_tarif")
            ->join("msetting_tarif_pengajuan", "msetting_tarif_pengajuan.id = msetting_tarif_pengajuan_detail.idsetting")
            ->where("msetting_tarif_pengajuan_detail.idpengajuan", $jenisPengajuan)
            ->where("msetting_tarif_pengajuan_detail.permintaan_ke", $nomorUrutPengajuan)
            ->get("msetting_tarif_pengajuan_detail")
            ->row();

        return $row;
    }

    public function getOptionBerkasMedis()
    {
        $this->db->select(
            'trm_layanan_berkas.id,
            trm_layanan_berkas.no_medrec,
            trm_layanan_berkas.namapasien AS nama_pasien,
            trm_layanan_berkas.tanggal_lahir,
            trm_layanan_berkas.tanggal_trx AS tanggal_kunjungan,
            trm_layanan_berkas.tujuan AS layanan,
            (CASE 
                WHEN trm_layanan_berkas.tujuan IN (1, 2) THEN
                    mpoliklinik.nama
                WHEN trm_layanan_berkas.tujuan IN (3, 4) THEN
                    mkelas.nama
                ELSE ""
            END) AS poliklinik_kelas,
            mdokter.id AS id_dokter,
            mdokter.nama AS nama_dokter'
        );
        $this->db->join('mdokter', 'mdokter.id = trm_layanan_berkas.iddokter', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = trm_layanan_berkas.idpoliklinik AND trm_layanan_berkas.tujuan IN (1, 2)', 'LEFT');
        $this->db->join('mkelas', 'mkelas.id = trm_layanan_berkas.kelas AND trm_layanan_berkas.tujuan IN (3, 4)', 'LEFT');
        $this->db->order_by('id', 'DESC');

        $this->db->limit(500);
        $query = $this->db->get('trm_layanan_berkas');
        return $query->result();
    }

    public function getDataKunjungan($idtransaksi)
    {
        $result = $this->db->select("trm_pengajuan_informasi_detail.id,
                trm_layanan_berkas.no_medrec, 
                trm_layanan_berkas.namapasien AS nama_pasien, 
                trm_layanan_berkas.tanggal_trx AS tanggal_kunjungan,
                (CASE 
                    WHEN trm_layanan_berkas.tujuan = 1 THEN 'Rawat Jalan'
                    WHEN trm_layanan_berkas.tujuan = 2 THEN 'Instalasi Gawat Darurat (IGD)'
                    WHEN trm_layanan_berkas.tujuan = 3 THEN 'Rawat Inap'
                    WHEN trm_layanan_berkas.tujuan = 4 THEN 'One Day Surgery (ODS)'
                END) AS jenis_kunjungan,
                mdokter.nama AS nama_dokter,
                mpengajuan_skd.nama AS jenis_pengajuan,
                trm_pengajuan_informasi_detail.estimasi_selesai,
                trm_pengajuan_informasi_detail.nominal,
                trm_pengajuan_informasi_detail.diskon_rp,
                (trm_pengajuan_informasi_detail.nominal - trm_pengajuan_informasi_detail.diskon_rp) AS nominal_akhir")
            ->join("mpengajuan_skd", "mpengajuan_skd.id = trm_pengajuan_informasi_detail.jenis_pengajuan")
            ->join("trm_layanan_berkas", "trm_layanan_berkas.id = trm_pengajuan_informasi_detail.idberkas")
            ->join("mdokter", "mdokter.id = trm_layanan_berkas.iddokter")
            ->where("trm_pengajuan_informasi_detail.idtransaksi", $idtransaksi)
            ->where("trm_pengajuan_informasi_detail.status", '1')
            ->get("trm_pengajuan_informasi_detail")
            ->result();

        return $result;
    }

    public function removeDataKunjungan($id)
    {
        $this->db->set("status", '0');
        $this->db->where("id", $id);
        if ($this->db->update("trm_pengajuan_informasi_detail")) {
            return true;
        } else {
            return false;
        }
    }

    public function updateDataKunjungan($id, $diskonRupiah, $nominalAkhir)
    {
        $this->db->set("diskon_rp", RemoveComma($diskonRupiah));
        $this->db->set("nominal_akhir", RemoveComma($nominalAkhir));
        $this->db->where("id", $id);
        if ($this->db->update("trm_pengajuan_informasi_detail")) {
            return true;
        } else {
            return false;
        }
    }

    public function saveData()
    {
        $idtemporary = $this->input->post('idtemp');

        $this->tanggal = YMDFormat($this->input->post('tanggal'));
        $this->nama_pemohon = $this->input->post('nama_pemohon');
        $this->keterangan = $this->input->post('keterangan');
        $this->created_at = date("Y-m-d H:i:s");
        $this->created_by = $this->session->userdata('user_id');

        if ($this->db->insert('trm_pengajuan_informasi', $this)) {
            $idtransaksi = $this->db->insert_id();
            $this->db->set('idtransaksi', $idtransaksi);
            $this->db->where('idtransaksi', $idtemporary); // ID TEMP
            $this->db->update('trm_pengajuan_informasi_detail');
        }

        return true;
    }

    public function updateData()
    {
        $idtransaksi = $this->input->post('id');
        $idtemporary = $this->input->post('idtemp');

        $this->tanggal = YMDFormat($this->input->post('tanggal'));
        $this->nama_pemohon = $this->input->post('nama_pemohon');
        $this->keterangan = $this->input->post('keterangan');

        if ($this->db->update('trm_pengajuan_informasi', $this, array('id' => $idtransaksi))) {
            $this->db->set('idtransaksi', $idtransaksi);
            $this->db->where('idtransaksi', $idtemporary); // ID TEMP
            $this->db->update('trm_pengajuan_informasi_detail');
        }

        return true;
    }
}
