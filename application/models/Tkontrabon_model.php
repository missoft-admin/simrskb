<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tkontrabon_model extends CI_Model
{
	public function saveData(){		
		
		$xjenis_kas_id= $_POST['xjenis_kas_id'];
		$xbiaya_transfer= $_POST['xbiaya_transfer'];
		$xsumber_kas_id= $_POST['xsumber_kas_id'];
		$xidmetode= $_POST['xidmetode'];
		$ket= $_POST['ket'];
		$xnominal_bayar= $_POST['xnominal_bayar'];
		$xiddet= $_POST['xiddet'];
		$xstatus= $_POST['xstatus'];
		$tanggal_pencairan= $_POST['xtanggal_pencairan'];
		foreach ($xjenis_kas_id as $index => $val){
			if ($xiddet[$index]!=''){
				$data_detail=array(
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'biaya_tf'=>RemoveComma($xbiaya_transfer[$index]),
					'idmetode'=>$xidmetode[$index],
					'ket_pembayaran'=>$ket[$index],
					'status'=>$xstatus[$index],
					'tanggal_pencairan'=>YMDFormat($tanggal_pencairan[$index]),
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
				
				$this->db->where('id',$xiddet[$index]);
				$this->db->update('tkontrabon_pembayaran', $data_detail);
			}else{
				$idtransaksi=$this->input->post('tkontrabon_id');
				$data_detail=array(
					'idkontrabon'=>$idtransaksi,
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'biaya_tf'=>RemoveComma($xbiaya_transfer[$index]),
					'idmetode'=>$xidmetode[$index],
					'ket_pembayaran'=>$ket[$index],
					'tanggal_pencairan'=>YMDFormat($tanggal_pencairan[$index]),
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->insert('tkontrabon_pembayaran', $data_detail);
				// print_r($data_detail);exit();
			}				
		}
		
		// $this->stpencairan  = 1;
		// // $this->metode  = $_POST['metode'];
		// $this->bayar_by  = $this->session->userdata('user_id');
		// $this->bayar_date  = date('Y-m-d H:i:s');
		// if ($this->input->post('btn_simpan')=='2'){
			// $this->st_verifikasi  = 1;			
		// }
		// $this->db->where('id',$_POST['id']);
		// return $this->db->update('tkontrabon', $this);	
		return true;
	}
    function get_st_kb($tanggal){
		$id=$this->db->query("SELECT H.id FROM tkontrabon_info H WHERE H.tanggal_kbo='$tanggal'")->row('id');
		if ($id){
			$this->db->query("call sp_update_kbo_info(".$id.")");
		}
		
		$q="SELECT H.id, H.tanggal_kbo,H.nominal_alkes,H.nominal_implan,H.nominal_obat,H.nominal_logistik,H.nominal_pengajuan,H.retur_uang, SUM(D.totalnominal) as totalnominal,SUM(D.nominal_tunai) as nominal_tunai,SUM(D.nominal_tunai_tf) as nominal_tunai_tf,SUM(D.nominal_cheq) as nominal_cheq,SUM(D.nominal_tf) as nominal_tf,H.`status`,SUM(1) as jml_dist
			,SUM(CASE WHEN D.nominal_tunai > 0 THEN 1 ELSE 0 END) as jml_tunai
			,SUM(CASE WHEN D.nominal_tf > 0 THEN 1 ELSE 0 END) as jml_tf
			,SUM(CASE WHEN D.nominal_cheq > 0 THEN 1 ELSE 0 END) as jml_cheq
			,SUM(CASE WHEN D.nominal_tunai_tf > 0 THEN 1 ELSE 0 END) as jml_tunai_tf,COUNT(DISTINCT D.id) jml_detail,SUM(D.st_trx_kas) as jml_trx_kas
			from tkontrabon_info H
			LEFT JOIN tkontrabon D ON D.kontrabon_info_id=H.id
			WHERE H.tanggal_kbo='$tanggal'
			GROUP BY H.id";
		return $this->db->query($q)->row();
	}
	public function list_pembayaran($id) {
        // $this->db->where('id', $id);
        // $query = $this->db->get('tsetoran_kas_detail');
		$q="SELECT JK.nama as jenis_kas,SK.nama as sumber_kas,D.* FROM tkontrabon_pembayaran D
		LEFT JOIN mjenis_kas JK ON JK.id=D.jenis_kas_id
		LEFT JOIN msumber_kas SK ON SK.id=D.sumber_kas_id
		WHERE D.idkontrabon='$id' AND D.status='1'";
        return $this->db->query($q)->result();
    }
	function insert_tkbo($tanggal){
		// print_r('INSERT');exit();
		$data_info=array(
			'tanggal_kbo' =>$tanggal,
			'tanggal_create' =>date('Y-m-d H:i:s'),
			'user_created' =>$this->session->userdata('user_id'),
			'user_nama_created' =>$this->session->userdata('user_name'),
			'status' =>'1',
		);
		$this->db->insert('tkontrabon_info', $data_info);
		$info_id=$this->db->insert_id();
		//tkontrabon_info
		$q="SELECT tipe,tanggaljatuhtempo,iddistributor,nama_distributor,MAX(group_id) as group_id,MAX(group_retur) as group_retur FROM
				(
				SELECT '1' as tipe,H.tanggaljatuhtempo,H.iddistributor,MD.nama as nama_distributor,GROUP_CONCAT(H.id) as group_id,null as group_retur
									from tgudang_penerimaan as H
									LEFT JOIN mdistributor MD ON MD.id=H.iddistributor
									WHERE DATE(H.tanggaljatuhtempo)='$tanggal' AND H.stverif_kbo IN (1,2) AND H.`status` = '1' AND H.tipe_kembali !='1'
									GROUP BY H.tanggaljatuhtempo,H.iddistributor
				UNION ALL

				SELECT '1' as tipe,H.tanggalkontrabon as tanggaljatuhtempo,H.iddistributor,M.nama as nama_distributor,NULL as group_id,GROUP_CONCAT(H.id) as group_retur
									from tgudang_pengembalian H
									LEFT JOIN tgudang_penerimaan P ON P.id=H.idpenerimaan					
									LEFT JOIN mdistributor M ON M.id=H.iddistributor
									WHERE H.jenis='1' AND H.jenis_retur='1' AND H.`status`='2' AND H.stverif_kbo='1' AND H.tanggalkontrabon='$tanggal'
									GROUP BY H.iddistributor,tipe
				UNION ALL
				
				SELECT  '3' as tipe,H.tanggal_kontrabon as  tanggaljatuhtempo,H.idvendor as iddistributor,M.nama as namadistributor,GROUP_CONCAT(H.id) as group_id,'' as group_retur
					from rka_pengajuan H
					LEFT JOIN mvendor M ON M.id=H.idvendor
					WHERE DATE(H.tanggal_kontrabon)='$tanggal' AND H.stverif_kbo IN (1,2) 
					GROUP BY H.tanggal_kontrabon,H.idvendor					
				) xxx
				GROUP BY tipe,iddistributor
				ORDER BY nama_distributor";
		$row=$this->db->query($q)->result();
		// print_r($row);exit();
		
		$verifikasi_date=date('Y-m-d H:i:s');
		foreach ($row as $r){
			$data_header=array(
				'kontrabon_info_id'=>$info_id,
				'tipe_distributor'=>$r->tipe,
				'iddistributor'=>$r->iddistributor,
				'tanggalkontrabon'=>$r->tanggaljatuhtempo,
				'totalnominal'=>0,				
				'verifikasi_id'=>$this->session->userdata('user_id'),
				'verifikasi_nama'=>$this->session->userdata('user_name'),
				'verifikasi_date'=>$verifikasi_date,
				'status'=>'1',
			);
			$this->db->insert('tkontrabon', $data_header);
			$idkontrabon=$this->db->insert_id();
			$idpenerimaan=explode(',',$r->group_id);
			$idretur=explode(',',$r->group_retur);
			// print_r($idpenerimaan);exit();
			if ($idpenerimaan){
				foreach ($idpenerimaan as $key => $value){
					if ($value){
						$data_det=array(
							'idkontrabon'=>$idkontrabon,
							'tipe'=>$r->tipe,
							'idpenerimaan'=>$value,
							'status'=>'1',
						);
						$this->db->insert('tkontrabon_detail', $data_det);
					}
				}
			}
			if ($idretur){
				foreach ($idretur as $key => $value){
					if ($value){
						$data_det=array(
							'idkontrabon'=>$idkontrabon,
							'tipe'=>'2',
							'idpenerimaan'=>$value,
							'status'=>'1',
						);
						$this->db->insert('tkontrabon_detail', $data_det);
					}
				}
			}
			
			// //Cara Bayar
			// if ($r->tot_cheq>0){
				// $data_bayar=array(
					// 'idkontrabon'=>$idkontrabon,
					// 'cara_bayar'=>'1',
					// 'nominal_total'=>$r->tot_tagihan,
					// 'nominal'=>($r->tot_tagihan-$r->biayamaterai-$r->biayaceq),
					// 'biayamaterai'=>($r->biayamaterai),
					// 'biayaceq'=>($r->biayaceq),
					// 'biaya_tf'=>0,
					// 'status'=>'1',
				// );
				// $this->db->insert('tkontrabon_bayar', $data_bayar);
			// }
			// if ($r->tot_tunai>0){
				// $data_bayar=array(
					// 'idkontrabon'=>$idkontrabon,
					// 'cara_bayar'=>'2',
					// 'nominal_total'=>$r->tot_tagihan,
					// 'nominal'=>$r->tot_tagihan,
					// 'biayamaterai'=>0,
					// 'biayaceq'=>0,
					// 'biaya_tf'=>0,
					// 'status'=>'1',
				// );
				// $this->db->insert('tkontrabon_bayar', $data_bayar);
			// }
			
		}
		//INSERT RETUR KEMBALI UANG
		
	}
	function get_jumlah_belum_verifikasi($tanggal){
		$q="SELECT COUNT(*) as jml FROM
				(
				SELECT H.tanggaljatuhtempo,H.iddistributor,MD.nama as nama_distributor,GROUP_CONCAT(H.id) as group_id,null as group_retur
									from tgudang_penerimaan as H
									LEFT JOIN mdistributor MD ON MD.id=H.iddistributor
									WHERE DATE(H.tanggaljatuhtempo)='$tanggal' AND H.stverif_kbo IN (1,2) AND H.`status` = '1'
									GROUP BY H.tanggaljatuhtempo,H.iddistributor
				UNION ALL

				SELECT H.tanggalkontrabon as tanggaljatuhtempo,H.iddistributor,M.nama as nama_distributor,NULL as group_id,GROUP_CONCAT(H.id) as group_retur
									from tgudang_pengembalian H
									LEFT JOIN tgudang_penerimaan P ON P.id=H.idpenerimaan					
									LEFT JOIN mdistributor M ON M.id=H.iddistributor
									WHERE H.jenis='1' AND H.jenis_retur='1' AND H.`status`='2' AND H.stverif_kbo='1' AND H.tanggalkontrabon='$tanggal'									
									GROUP BY H.iddistributor
				UNION ALL
				
				SELECT  H.tanggal_kontrabon as tanggaljatuhtempo,H.idvendor as iddistributor,M.nama as nama_distributor,NULL as group_id,GROUP_CONCAT(H.id) as group_retur
					from rka_pengajuan H
					LEFT JOIN mvendor M ON M.id=H.idvendor
					WHERE DATE(H.tanggal_kontrabon)='$tanggal' AND H.stverif_kbo IN (1,2) 
					GROUP BY H.tanggal_kontrabon,H.idvendor
					
				) xxx
				GROUP BY tanggaljatuhtempo
			";
			$row=$this->db->query($q)->row('jml');
			if ($row){
				$row=$row;
			}else{
				$row='0';
			}
			return $row;
	}
	function get_jml_minus($id){
		$q="SELECT COUNT(*) jml_minus FROM tkontrabon
WHERE kontrabon_info_id='$id' AND totalnominal < 0
			";
			$row=$this->db->query($q)->row('jml_minus');
			if ($row){
				$row=$row;
			}else{
				$row='0';
			}
			return $row;
	}
	function list_distributor(){
		$q="SELECT M.id,M.nama FROM mdistributor M
			WHERE M.`status`='1'
			ORDER BY M.nama ASC";
		return $this->db->query($q)->result();
	}
	function list_bank_dist($iddistributor){
		$q="SELECT H.id,
			M.kodebank,M.bank,H.cabang,H.atasnama,H.norekening,H.staktif
			FROM mdistributor_bank H
			LEFT JOIN ref_bank M ON M.id=H.bankid
			WHERE H.iddistributor='$iddistributor' ORDER BY staktif ASC";
		return $this->db->query($q)->result();
	}
	function list_bank(){
		$q="SELECT M.id,M.bank FROM ref_bank M
			ORDER BY M.id ASC";
		return $this->db->query($q)->result();
	}
	function get_header_kbo_detail($id){
		$q="SELECT H.id,H.tipe_distributor,H.nokontrabon,H.iddistributor
			,CASE WHEN H.tipe_distributor='3' THEN MV.nama ELSE M.nama END as nama_dist
			,CASE WHEN H.tipe_distributor='3' THEN MV.alamat ELSE M.alamat END as alamat
			,CASE WHEN H.tipe_distributor='3' THEN MV.telepon ELSE M.telepon END as telepon
			,H.kontrabon_info_id,H.totalnominal,H.grandtotalnominal 
				,H.st_cara_bayar as carabayar,H.biaya_tf,H.biayamaterai,H.biayacek,H.tanggalkontrabon,I.status as st_kbo
				,H.rekening_id,B.norekening,BI.nama as nama_bank
				from tkontrabon H
				LEFT JOIN tkontrabon_info I ON I.id=H.kontrabon_info_id
				LEFT JOIN mdistributor M ON M.id=H.iddistributor  AND H.tipe_distributor='1'
				LEFT JOIN mvendor MV ON MV.id=H.iddistributor AND H.tipe_distributor='3'
				LEFT JOIN mdistributor_bank B ON B.id=H.rekening_id
				LEFT JOIN mbank BI ON BI.id=H.bankid
				WHERE H.id='$id'";
		return $this->db->query($q)->row_array();
	}
	
	function header_rekap_report($id){
			$q="SELECT I.tanggal_kbo
				from tkontrabon_info I
				
				WHERE I.id='$id'";
			return $this->db->query($q)->row_array();
	}
	function list_rekap_report($id){
			$q="SELECT H.id,I.tanggal_kbo,H.nokontrabon,H.iddistributor
				,CASE WHEN H.tipe_distributor='3' THEN MV.nama ELSE M.nama END as nama_dist
				,CASE WHEN H.tipe_distributor='3' THEN MV.alamat ELSE M.alamat END as alamat
				,CASE WHEN H.tipe_distributor='3' THEN MV.telepon ELSE M.telepon END as telepon
				,CASE WHEN H.tipe_distributor='3' THEN MV.id ELSE M.kode END as kode
				,H.kontrabon_info_id,H.totalnominal,H.grandtotalnominal 
				,H.st_cara_bayar as carabayar,H.biaya_tf,H.biayamaterai,H.biayacek,H.tanggalkontrabon,H.kontrabon_info_id,I.status as st_kbo
				,H.rekening_id,B.norekening,BI.bank as nama_bank
				from tkontrabon H
				LEFT JOIN tkontrabon_info I ON I.id=H.kontrabon_info_id
				LEFT JOIN mdistributor M ON M.id=H.iddistributor AND H.tipe_distributor='1'
				LEFT JOIN mvendor MV ON MV.id=H.iddistributor AND H.tipe_distributor='3'
				LEFT JOIN mdistributor_bank B ON B.id=H.rekening_id
				LEFT JOIN ref_bank BI ON BI.id=H.bankid
				WHERE I.id='$id'";
			return $this->db->query($q)->result();
	}
	function list_rekap_detail_report($id){
			$q="SELECT H.id,I.tanggal_kbo,M.kode,
			CASE WHEN H.tipe_distributor='3' THEN RKA.no_pengajuan ELSE P.nopenerimaan END nopenerimaan
			,P.nofakturexternal,H.nokontrabon
			,CONCAT(H.tipe_distributor,'-',H.iddistributor) as iddistributor
			,CASE WHEN H.tipe_distributor='3' THEN MV.nama ELSE M.nama END as nama_dist,D.nominal_bayar
				from tkontrabon H
				LEFT JOIN tkontrabon_detail D ON D.idkontrabon=H.id
				LEFT JOIN tgudang_penerimaan P ON P.id=D.idpenerimaan AND D.tipe !='3'
				LEFT JOIN rka_pengajuan RKA ON RKA.id=D.idpenerimaan AND D.tipe ='3'
				LEFT JOIN tkontrabon_info I ON I.id=H.kontrabon_info_id
				LEFT JOIN mdistributor M ON M.id=H.iddistributor AND H.tipe_distributor='1'
				LEFT JOIN mvendor MV ON MV.id=H.iddistributor AND H.tipe_distributor='3'				
				LEFT JOIN mdistributor_bank B ON B.id=H.rekening_id
				LEFT JOIN ref_bank BI ON BI.id=H.bankid
				WHERE I.id='$id'
				ORDER BY H.nokontrabon ASC";
				// print_r($q);exit;
			return $this->db->query($q)->result();
	}
		
	function get_detail_kbo($id){
		$q="SELECT D.idpenerimaan,D.tipe,T.nopenerimaan,P.nopemesanan,T.tanggalpenerimaan,T.tgl_terima,T.nofakturexternal,D.nominal_bayar as totalharga,D.st_retur,
			D.jenis_retur,D.tipe_kembali,COUNT(L.id) as file,D.id 
			FROM tkontrabon_detail D
			LEFT JOIN tgudang_penerimaan T ON T.id=D.idpenerimaan AND D.tipe='1'
			LEFT JOIN tgudang_pemesanan P ON P.id=T.idpemesanan
			LEFT JOIN tgudang_penerimaan_lampiran L ON L.idpenerimaan=T.id

			WHERE D.idkontrabon='$id' AND D.tipe='1'
			GROUP BY T.id

			UNION ALL

			SELECT D.idpenerimaan,D.tipe,T.nopenerimaan,P.nopemesanan,T.tanggalpenerimaan,T.tgl_terima,T.nofakturexternal,
			D.nominal_bayar as totalharga,D.st_retur,D.jenis_retur,D.tipe_kembali,'0' as file,D.id 
			FROM tkontrabon_detail D
			LEFT JOIN tgudang_pengembalian H ON H.id=D.idpenerimaan
			LEFT JOIN tgudang_penerimaan T ON T.id=H.idpenerimaan
			LEFT JOIN tgudang_pemesanan P ON P.id=T.idpemesanan

			WHERE D.idkontrabon='$id' AND D.tipe='2'
			
			UNION ALL
			
			SELECT D.idpenerimaan,D.tipe,T.no_pengajuan as nopenerimaan,'-' as nopemesanan,T.tanggal_pengajuan as tanggalpenerimaan,T.tanggal_dibutuhkan as tgl_terima,'' as nofakturexternal
			,D.nominal_bayar as totalharga,D.st_retur,D.jenis_retur,D.tipe_kembali,0 as file,D.id 
			FROM tkontrabon_detail D
			LEFT JOIN rka_pengajuan T ON T.id=D.idpenerimaan AND D.tipe='3'
			
			WHERE D.idkontrabon='$id' AND D.tipe='3'
					";
		return $this->db->query($q)->result();
	}
	function get_data_serahkan($id){
		$q="SELECT H.nama_penerima,DATE_FORMAT(H.tanggal_penyerahan,'%d-%m-%Y') as tanggal_penyerahan from tkontrabon H
			WHERE H.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function get_file_serahkan($id){
		$q="SELECT  H.* from tkontrabon_upload H

			WHERE H.idkontrabon='$id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/bukti_penyerahan/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">'.$r->user_upload.'-'.HumanDateLong($r->tanggal_upload).'</td>';
			$tabel .='<td class="text-left"><button class="btn btn-xs btn-danger hapus_file" title="Hapus"><li class="fa fa-trash-o"></li></button></td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
		}
		return $tabel;
	}
	function get_header_kbo_detail_nv($iddistributor,$tanggal){
		// $q="SELECT H.id,H.nokontrabon,H.iddistributor,M.nama as nama_dist,M.alamat,M.telepon,H.kontrabon_info_id,H.totalnominal,H.grandtotalnominal 
				// ,H.st_cara_bayar as carabayar,H.biaya_tf,H.biayamaterai,H.biayacek,H.tanggalkontrabon,H.kontrabon_info_id
				// from tkontrabon H
				// LEFT JOIN mdistributor M ON M.id=H.iddistributor
				// WHERE H.id='$id'";
		$q="SELECT T1.id,T1.nokontrabon,T1.tanggaljatuhtempo as tanggalkontrabon,T1.iddistributor,T1.nama_dist,T1.alamat,T1.telepon,'' as kontrol_id,SUM(T1.jml_faktur) as jml_faktur
			,SUM(T1.tot_tagihan) as totalnominal
			,SUM(T1.tot_tagihan) as grandtotalnominal
			,CASE WHEN T1.tot_tagihan > S.minnominalceq THEN 1 ELSE 2 END as carabayar
			,0 as tot_tf,99 as status,T1.group_id,T1.tipe_distributor
			FROM (
			SELECT H.id,'' as nokontrabon,H.tanggaljatuhtempo,H.iddistributor,MD.nama as nama_dist,MD.alamat, MD.telepon, COUNT(H.id) as jml_faktur,SUM(H.totalharga-IFNULL(H.nominal_asal,0)) as tot_tagihan,GROUP_CONCAT(H.id) as group_id,'1' as tipe_distributor
			from tgudang_penerimaan as H
			LEFT JOIN mdistributor MD ON MD.id=H.iddistributor
			WHERE DATE(H.tanggaljatuhtempo)='$tanggal' AND H.stverif_kbo IN (1,2) AND H.`status` = '1' AND H.iddistributor='$iddistributor'
			GROUP BY H.tanggaljatuhtempo,H.iddistributor
			UNION ALL
			SELECT H.id,'' as nokontrabon,H.tanggalkontrabon as tanggaljatuhtempo,H.iddistributor,MD.nama as nama_dist,MD.alamat,MD.telepon, COUNT(H.id) as jml_faktur,SUM(H.totalharga * -1) as tot_tagihan 	
			,GROUP_CONCAT(H.id) as group_id,'1' as tipe_distributor
			from tgudang_pengembalian H
			LEFT JOIN tgudang_penerimaan P ON P.id=H.idpenerimaan					
			LEFT JOIN mdistributor MD ON MD.id=H.iddistributor
			WHERE H.jenis='1' AND H.jenis_retur='1' AND H.`status`='2' AND H.stverif_kbo='1' AND H.tanggalkontrabon='$tanggal' AND H.iddistributor='$iddistributor'
			UNION ALL
			SELECT  H.id,'' as nokontrabon,H.tanggal_kontrabon as  tanggaljatuhtempo,H.idvendor as iddistributor,M.nama as nama_dist,M.alamat,M.telepon,COUNT(H.id) jml_faktur
				,SUM(H.grand_total) as tot_tagihan,GROUP_CONCAT(H.id) as group_id,'2' as tipe_distributor
					from rka_pengajuan H
					LEFT JOIN mvendor M ON M.id=H.idvendor
					WHERE DATE(H.tanggal_kontrabon)='$tanggal' AND H.stverif_kbo IN (1,2) AND H.idvendor='$iddistributor'
					GROUP BY H.tanggal_kontrabon,H.idvendor
					
			) T1,mkontrabon S";
			// print_r($q);exit();
		return $this->db->query($q)->row_array();
	}
	function get_mkontrabon(){
		$q="SELECT *from mkontrabon";
		return $this->db->query($q)->row();
	}
	function get_user_aktifasi(){
		$q="SELECT U.user_id,M.`name` FROM mkontrabon_user_aktivasi U
				LEFT JOIN musers M ON M.id=U.user_id";
		return $this->db->query($q)->result();
	}
	function list_user_aktifasi($id){
		$q="SELECT T.user_id,M.`name`,T.approve FROM tkontrabon_user T
			LEFT JOIN musers M ON M.id=T.user_id
			WHERE T.kontrabon_info_id='$id'
			";
		return $this->db->query($q)->result();
	}
	function get_file_name($id){
		$q="select file_name from tkontrabon_upload where id='$id'
			";
		return $this->db->query($q)->row();
	}
	function delete_tkbo($tanggal){
		
		$q="
			SELECT GROUP_CONCAT(D.id) as group_delete from tkontrabon H
			LEFT JOIN tkontrabon_detail D ON D.idkontrabon=H.id 
			WHERE H.tanggalkontrabon='$tanggal'
			GROUP BY H.tanggalkontrabon";
		// print_r($q);exit();
		$row=$this->db->query($q)->row('group_delete');
		$row_delete=explode(',',$row);
		if ($row_delete){
			foreach ($row_delete as $key => $value){
				if ($value){
					$this->db->where('id',$value);
					$this->db->delete('tkontrabon_detail');
				}
			}
		}
		return true;
	}
	public function insert_jurnal_hutang($id){
	   
	   $q="SELECT 
			AM.noakun as noakun_materai,  
			AM.namaakun as namaakun_materai, 
			'K' as posisi_materai,
			AH.noakun as noakun_hutang,  
			AH.namaakun as namaakun_hutang,  
			'D' as posisi_hutang,
			AC.noakun as noakun_cheq,  
			AC.namaakun as namaakun_cheq,
			'K' as posisi_cheq,
			T.* FROM (
			SELECT H.id as idtransaksi,H.st_cara_bayar,H.tipe_distributor,H.iddistributor,H.nokontrabon as notransaksi,H.tanggalkontrabon as tanggal_transaksi
						,CASE WHEN H.tipe_distributor='1' THEN MD.nama ELSE MV.nama END as distributor
						,S.batas_batal,S.st_auto_posting
						,H.totalnominal as nominal_hutang,H.grandtotalnominal as nominal_bayar
						,H.biayacek as nominal_cheq,H.biayamaterai as nominal_materai,H.biaya_tf as nominal_tf
						,compare_value_2(MAX(IF(SH.iddistributor=H.iddistributor,SH.idakun,NULL)), MAX(IF(SH.iddistributor=0,SH.idakun,NULL))) as idakun_hutang
						,compare_value_2(MAX(IF(SC.iddistributor=H.iddistributor,SC.idakun,NULL)), MAX(IF(SC.iddistributor=0,SC.idakun,NULL))) as idakun_cheq
						,compare_value_2(MAX(IF(SM.iddistributor=H.iddistributor,SM.idakun,NULL)), MAX(IF(SM.iddistributor=0,SM.idakun,NULL))) as idakun_materai
						,H.rekening_id
						,H.bankid
						FROM tkontrabon H
						LEFT JOIN mdistributor MD ON MD.id=H.iddistributor AND H.tipe_distributor='1'
						LEFT JOIN mvendor MV ON MV.id=H.iddistributor AND H.tipe_distributor='3'
						LEFT JOIN msetting_jurnal_hutang_cheq SC ON SC.tipe_distributor=H.tipe_distributor
						LEFT JOIN msetting_jurnal_hutang_hutang SH ON SH.tipe_distributor=H.tipe_distributor
						LEFT JOIN msetting_jurnal_hutang_materai SM ON SM.tipe_distributor=H.tipe_distributor
						LEFT JOIN msetting_jurnal_hutang S ON S.id='1'
						WHERE H.id='$id'
				) T
				LEFT JOIN makun_nomor AM ON AM.id=T.idakun_materai
				LEFT JOIN makun_nomor AC ON AC.id=T.idakun_cheq
				LEFT JOIN makun_nomor AH ON AH.id=T.idakun_hutang";
		$row=$this->db->query($q)->row();
		$st_auto_posting=$row->st_auto_posting;
		// $tipe_bayar=$row->tipe_bayar;
		$data_header=array(
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'tipe_distributor' => $row->tipe_distributor,
			'st_cara_bayar' => $row->st_cara_bayar,
			'iddistributor' => $row->iddistributor,
			'distributor' => $row->distributor,
			'rekening_id' => $row->rekening_id,
			'bankid' => $row->bankid,
			'nominal_hutang'=>$row->nominal_hutang,
			'nominal_bayar'=>$row->nominal_bayar,
			'nominal_cheq'=>$row->nominal_cheq,
			'nominal_materai'=>$row->nominal_materai,
			'nominal_tf'=>$row->nominal_tf,
			'idakun_hutang'=>$row->idakun_hutang,
			'idakun_cheq'=>$row->idakun_cheq,
			'idakun_materai'=>$row->idakun_materai,
			'noakun_hutang'=>$row->noakun_hutang,
			'noakun_cheq'=>$row->noakun_cheq,
			'noakun_materai'=>$row->noakun_materai,
			'namaakun_hutang'=>$row->namaakun_hutang,
			'namaakun_cheq'=>$row->namaakun_cheq,
			'namaakun_materai'=>$row->namaakun_materai,
			'posisi_hutang'=>$row->posisi_hutang,
			'posisi_cheq'=>$row->posisi_cheq,
			'posisi_materai'=>$row->posisi_materai,
			'status' => 1,
			'st_posting' => 0,
			'st_auto_posting' => $row->st_auto_posting,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
            'created_date'=>date('Y-m-d H:i:s')
			

		);
		// print_r($data_header);exit();
		$st_auto_posting=$row->st_auto_posting;
		$this->db->insert('tvalidasi_hutang',$data_header);
		$idvalidasi=$this->db->insert_id();
		
		
		// //INSERT PEMBAYARAN
			$q="SELECT
				SK.idakun,
				SK.noakun,
				A.namaakun,
				B.nominal_bayar AS nominal,
				'K' AS posisi_akun,'PEMBAYARAN' as keterangan,'0' as st_biaya_tf 
			FROM
				tkontrabon_pembayaran B
				INNER JOIN msumber_kas SK ON SK.id = B.sumber_kas_id
				INNER JOIN makun_nomor A ON A.id = SK.idakun 
			WHERE
				B.idkontrabon = '$id' 
				AND B.STATUS = '1'
				
				UNION ALL
				
				SELECT B.idakun,
				A.noakun,
				A.namaakun,
				B.biaya_tf AS nominal,
				'K' AS posisi_akun,'BIAYA TRANSFER' as keterangan,'1' as st_biaya_tf  
						FROM (
							SELECT
								B.sumber_kas_id,compare_value_2(MAX(IF(S.sumber_kas_id=B.sumber_kas_id,S.idakun,NULL)), MAX(IF(S.sumber_kas_id=0,S.idakun,NULL))) as idakun,B.biaya_tf
								FROM
								tkontrabon_pembayaran B	
								INNER JOIN tkontrabon H ON H.id=B.idkontrabon 
								LEFT JOIN msetting_jurnal_hutang_kas S ON S.setting_id=1
								WHERE
								B.idkontrabon = '$id' 
								AND B.STATUS = '1'
								GROUP BY B.id
							) B
			INNER JOIN msumber_kas SK ON SK.id = B.sumber_kas_id
			INNER JOIN makun_nomor A ON A.id = B.idakun 
			";			
		
		// print_r($q);exit();
		$rows=$this->db->query($q)->result();
		foreach($rows as $r){
			$data_detail=array(
				'idvalidasi' => $idvalidasi ,
				'idkontrabon' => $id,
				'idakun' => $r->idakun,
				'noakun' => $r->noakun,
				'namaakun' => $r->namaakun,
				'nominal' => $r->nominal,
				'posisi_akun' => $r->posisi_akun,
				'keterangan' => $r->keterangan,
				'st_biaya_tf' => $r->st_biaya_tf,
				
			);
			$this->db->insert('tvalidasi_hutang_bayar',$data_detail);
		}
		if ($st_auto_posting=='1'){
			$data_header=array(
				
				'st_posting' => 1,
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
				

			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_hutang',$data_header);
		}
		return true;
   }
}

/* End of file Tkontrabon_model.php */
/* Location: ./application/models/Tkontrabon_model.php */
