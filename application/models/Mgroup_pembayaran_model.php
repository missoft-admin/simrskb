<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mgroup_pembayaran_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('mgroup_pembayaran.id', $id);
        $query = $this->db->get('mgroup_pembayaran');
        return $query->row();
    }

    public function getAkunData($id)
    {
        $this->db->select('mgroup_pembayaran_detail.*, makun_nomor.namaakun,makun_nomor.noakun as noakun2');
        $this->db->join('makun_nomor', 'makun_nomor.id = mgroup_pembayaran_detail.idakun','LEFT');
        $this->db->where('mgroup_pembayaran_detail.idgroup', $id);
        $query = $this->db->get('mgroup_pembayaran_detail');
		// print_r($this->db->last_query());exit();
        return $query->result();
    }

    public function saveData()
    {
        $this->nama   = $_POST['nama'];
    		$this->created_at  = date('Y-m-d H:i:s');
    		$this->created_by  = $this->session->userdata('user_id');

        if ($this->db->insert('mgroup_pembayaran', $this)) {
            $idgroup = $this->db->insert_id();

            $akunList = json_decode($_POST['akun_list_value']);
            foreach ($akunList as $row) {
                $data = array();
                $data['idgroup'] = $idgroup;
                $data['idakun']  = $row[0];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['created_by'] = $this->session->userdata('user_id');

                $this->db->insert('mgroup_pembayaran_detail', $data);
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
		// print_r($this->input->post());exit();
        $this->nama   = $_POST['nama'];
    		$this->updated_at  = date('Y-m-d H:i:s');
    		$this->updated_by  = $this->session->userdata('user_id');

        if ($this->db->update('mgroup_pembayaran', $this, array('id' => $_POST['id']))) {
            $idgroup = $_POST['id'];

            $this->db->where('idgroup', $idgroup);
            if ($this->db->delete('mgroup_pembayaran_detail')) {
                $akunList = json_decode($_POST['akun_list_value']);
                foreach ($akunList as $row) {
                    $data = array();
                    $data['idgroup'] = $idgroup;
                    $data['idakun']  = $row[0];
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $data['updated_by'] = $this->session->userdata('user_id');

                    $this->db->insert('mgroup_pembayaran_detail', $data);
                }
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
        return $status;
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('mgroup_pembayaran', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
