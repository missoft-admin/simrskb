<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpiutang_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mpiutang');
        return $query->row();
    }
	public function lokasi_tubuh_list()
    {
        $this->db->where('status','1');
        $query = $this->db->get('mlokasi_tubuh');
        return $query->result();
    }
	public function kelompok_operasi_list()
    {
        $this->db->where('status','1');
        $query = $this->db->get('mkelompok_operasi');
        return $query->result();
    }
	public function js_icd(){
		$search=$this->input->post('search');
		$q="SELECT *from icd_9
			WHERE  (deskripsi LIKE '%".$search."%' OR kode LIKE '%".$search."%')";
		$query=$this->db->query($q);
		$result= $query->result();		
		return $result;			
	}
	public function jd_pegawai_dokter(){
		$tipe=$this->input->post('tipe');
		$search=$this->input->post('search');
		if ($tipe=='1'){//PEgawai
			$q="SELECT M.id,M.nama from mpegawai M
			WHERE M.id NOT IN (SELECT idpegawai from mpiutang_pegawai) AND nama LIKE '%".$search."%' AND M.status='1'";
		}else{
			$q="SELECT M.id,M.nama from mdokter M
			WHERE M.id NOT IN (SELECT iddokter from mpiutang_dokter) AND nama LIKE '%".$search."%' AND M.status='1'";
			
		}
		// print_r($q);exit();
		$query=$this->db->query($q);
		$result= $query->result();		
		return $result;			
	}
    public function saveData()
    {
        $this->nama 		= $_POST['nama'];
        
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mpiutang', $this)) {
            return  $this->db->insert_id();
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama 		= $_POST['nama'];
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mpiutang', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mpiutang', $this, array('id' => $id))) {
			$this->db->where('idpengaturan',$id);
			$this->db->update('mpiutang_setting',array('status'=>0));
			
			$this->db->where('idpengaturan',$id);
			$this->db->delete('mpiutang_pegawai');
			
			$this->db->where('idpengaturan',$id);
			$this->db->delete('mpiutang_dokter');
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function load_all()
    {
        $query = $this->db->get('mpiutang');
        return $query->result();
    }
}
