<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER     : IYAN ISYANTO
| EMAIL         : POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tunit_permintaan_model extends CI_Model {
    public function detail_permintaan($id) {
        
		$string_query="SELECT D.*,H.idunitpeminta as id_ke,H.idunitpenerima as id_dari,COALESCE(SD.stok,0)  as stok_dari,TP.nama_tipe,
		COALESCE(SK.stok,0) as stok_ke,H.idunitpeminta,H.idunitpenerima,
		(SELECT SUM(tunit_pengiriman.kuantitas_terima) from tunit_pengiriman where tunit_pengiriman.iddet=D.id) AS sudah_terima,B.jumlahsatuanbesar
		 from tunit_permintaan_detail D
		
		LEFT JOIN tunit_permintaan H On H.id=D.idpermintaan
		LEFT JOIN mgudang_stok SD ON SD.idunitpelayanan=H.idunitpenerima AND SD.idtipe=D.idtipe AND SD.idbarang=D.idbarang
		LEFT JOIN mgudang_stok SK ON SK.idunitpelayanan=H.idunitpeminta AND SK.idtipe=D.idtipe AND SK.idbarang=D.idbarang		
		LEFT JOIN mdata_tipebarang TP on TP.id=D.idtipe
		LEFT JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe 
		WHERE D.idpermintaan='$id'";
		// print_r($string_query);exit();
        return $this->db->query($string_query)->result();
    }
	public function detail_penerimaan($id) {
        
		$string_query="SELECT 
			TK.id,TK.idtipe,T.nama_tipe, TM.namabarang,TM.kuantitas,TM.kuantitaskirim as sudah_dikirim,
			TK.kuantitas as kirim_sekarang,(SELECT SUM(tunit_pengiriman.kuantitas_terima) from tunit_pengiriman where tunit_pengiriman.iddet=TM.id) AS sudah_terima,TM.kuantitasalih,
			TK.kuantitas as terima_skrg,TM.kuantitassisa
			from tunit_pengiriman TK 
			LEFT JOIN tunit_permintaan_detail TM ON TK.iddet=TM.id
			LEFT JOIN mdata_tipebarang T on T.id=TK.idtipe
			WHERE TM.idpermintaan='$id'  AND  TK.kuantitas_terima=0 ORDER BY TK.tanggal_kirim ASC,TK.id ASC
			";
        return $this->db->query($string_query)->result();
    }
	
	public function detail_kirim($idpermintaan){
		$q="SELECT *from tunit_pengiriman WHERE idpermintaan='$idpermintaan' ORDER BY tanggal_kirim DESC,iddet ASC";
		$query=$this->db->query($q);
		return $query->result();
	}
    public function histori_terima($idpermintaan){
		$q="SELECT *from tunit_pengiriman WHERE idpermintaan='$idpermintaan' AND kuantitas_terima >0 ORDER BY tanggal_kirim DESC,iddet ASC";
		$query=$this->db->query($q);
		return $query->result();
	}
    public function list_barang_alihan($idpermintaan){
		$q="SELECT D.id,D.idtipe,D.idbarang,T.nama_tipe,CASE 
			WHEN D.idtipe='1' THEN A.nama
			WHEN D.idtipe='2' THEN I.nama
			WHEN D.idtipe='3' THEN O.nama
			WHEN D.idtipe='4' THEN L.nama
			END as namabarang,D.kuantitassisa,H.idunitpenerima as iddari
			from tunit_permintaan H
			LEFT JOIN tunit_permintaan_detail D ON H.id=D.idpermintaan
			LEFT JOIN mdata_tipebarang T ON T.id=D.idtipe
			LEFT JOIN mdata_obat O ON O.id=D.idbarang
			LEFT JOIN mdata_alkes A ON A.id=D.idbarang
			LEFT JOIN mdata_logistik L ON L.id=D.idbarang
			LEFT JOIN mdata_implan I ON I.id=D.idbarang
			Where H.id='$idpermintaan'  AND kuantitassisa > 0";
		$query=$this->db->query($q);
		return $query->result();
	}
	public function get_list_tipe($unit_dari,$unit_ke){
		// print_r($unit_dari);exit();
		$user_id=$this->session->userdata('user_id');
		$q="SELECT T.idtipe as id,M.nama_tipe as text FROM (
			SELECT U.idtipe from musers_tipebarang U
			WHERE U.iduser='$user_id'
			UNION ALL
			SELECT UD.idtipe from munitpelayanan_tipebarang UD
			WHERE UD.idunitpelayanan='$unit_dari'
			UNION  ALL
			SELECT UK.idtipe from munitpelayanan_tipebarang UK
			WHERE UK.idunitpelayanan='$unit_ke') T
			LEFT JOIN mdata_tipebarang M ON M.id=T.idtipe
			GROUP BY T.idtipe
			HAVING COUNT(T.idtipe)>=3";
			// print_r($q);exit();
		return $this->db->query($q)->result();
	}
	public function get_report_detail_permintaan($id){
		$q="SELECT D.id,D.idtipe,D.idbarang,T.nama_tipe,D.kuantitassisa,H.idunitpenerima as iddari,B.kode,B.nama_kategori,B.nama as namabarang,msatuan.singkatan as satuan,D.kuantitas,D.sisa_stok
			,D.sisa_stok_unit,CASE WHEN D.sisa_stok_unit IS NULL THEN 1 ELSE 0 END as st_update
			from tunit_permintaan H
			LEFT JOIN tunit_permintaan_detail D ON H.id=D.idpermintaan
			LEFT JOIN mdata_tipebarang T ON T.id=D.idtipe
			LEFT JOIN 
			(
				SELECT view_barang_all.*,mdata_kategori.nama as nama_kategori from view_barang_all
				LEFT JOIN mdata_kategori ON mdata_kategori.id=view_barang_all.idkategori
			) B ON B.id=D.idbarang AND B.idtipe=D.idtipe
			LEFT JOIN msatuan ON msatuan.id=B.idsatuan
			Where H.id='$id'  ";
		$query=$this->db->query($q);
		return $query->result();
	}
    public function get_user_kirim_terima($id,$tanggal){
		$q="SELECT K.user_nama_kirim,K.user_nama_terima from tunit_pengiriman K
				WHERE K.idpermintaan='$id' AND DATE_FORMAT(tanggal_kirim,'%Y-%m-%d %H:%i')='$tanggal' LIMIT 1";
				// print_r($q);exit();
		$query=$this->db->query($q);
		return $query->row();
	}
    public function get_report_detail_pengiriman($id,$tanggal){
		$q="SELECT K.idpermintaan,T.nama_tipe,
			K.tanggal_kirim,K.user_nama_kirim,K.kuantitas,B.kode,B.nama_kategori,B.nama as namabarang,msatuan.singkatan as satuan,D.sisa_stok
			from tunit_pengiriman K
			LEFT JOIN tunit_permintaan_detail D ON D.id=K.iddet
			LEFT JOIN mdata_tipebarang T ON T.id=D.idtipe
			LEFT JOIN 
						(
							SELECT view_barang.*,mdata_kategori.nama as nama_kategori from view_barang
							LEFT JOIN mdata_kategori ON mdata_kategori.id=view_barang.idkategori
						) B ON B.id=K.idbarang AND B.idtipe=K.idtipe
			LEFT JOIN msatuan ON msatuan.id=B.idsatuan
			WHERE K.idpermintaan='$id' AND DATE_FORMAT(tanggal_kirim,'%Y-%m-%d %H:%i') ='$tanggal'";
		$query=$this->db->query($q);
		return $query->result();
	}
    public function get_report_head_permintaan($id){
		$q="SELECT H.nopermintaan
			,CASE WHEN H.idunitpenerima='0' THEN 'Gudang Farmasi' ELSE  UD.nama END as unit_dari
			,CASE WHEN H.idunitpeminta='0' THEN 'Gudang Farmasi' ELSE UK.nama END as unit_ke
			,H.datetime,H.iduser,H.namauser 
			
			From tunit_permintaan H
			LEFT JOIN munitpelayanan UD ON UD.id=H.idunitpenerima
			LEFT JOIN munitpelayanan UK ON UK.id=H.idunitpeminta
			WHERE H.id='$id'";
		$query=$this->db->query($q);
		return $query->row();
	}
    public function head_permintaan($id) {
        $this->db->select('thead.*, up1.nama as peminta, up2.nama as penerima,us.name as requester');
        $this->db->where('thead.id', $id);
        $this->db->join('munitpelayanan up1', 'thead.idunitpeminta = up1.id', 'left');
        $this->db->join('munitpelayanan up2', 'thead.idunitpenerima = up2.id', 'left');
        $this->db->join('musers us', 'thead.iduser = us.id', 'left');
        return $this->db->get('tunit_permintaan thead')->row_array();
    }

    public function dtpermintaan() {
        $item = array();
        $this->db->select('idunitpelayanan');
        $this->db->from('munitpelayanan_user');
        $this->db->where('munitpelayanan_user.userid', $this->session->userdata('user_id'));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $r) {
                $item[] = $r['idunitpelayanan'];
            }
        }

        $this->load->library('Datatables');
        $this->datatables->select('id, nopermintaan, dariunit, keunit, totalpesan, totalalih, totalkirim, status, totalterima');
        $this->datatables->where_in('iddariunit', $item);
        $this->datatables->from('v_index_tunit_permintaan');
        return print($this->datatables->generate());
    }

    public function dtpermintaan_detail() {
        $id = $this->uri->segment(3);
        if ($id) {
            $this->load->library('Datatables');
            $this->datatables->select('
                tdet.id,
                tdet.idtipe,
                tdet.idbarang,
                up1.nama as keunit,
                up2.nama as dariunit,
                tdet.kuantitas,
                tdet.kuantitassisa,
                tdet.kuantitaskirim,
                thead.idunitpeminta,
                thead.idunitpenerima,
            ');
            $this->datatables->join('tunit_permintaan thead', 'thead.id = tdet.idpermintaan', 'left');
            $this->datatables->join('munitpelayanan up1', 'thead.idunitpeminta = up1.id', 'left');
            $this->datatables->join('munitpelayanan up2', 'thead.idunitpenerima = up2.id', 'left');
            $this->datatables->where('tdet.idpermintaan', $id);
            $this->datatables->from('tunit_permintaan_detail tdet');
            return print($this->datatables->generate());
        }
    }

    public function dtpermintaan_detail_penerimaan() {
        $id = $this->uri->segment(3);
        if ($id) {
            $this->load->library('Datatables');
            $this->datatables->select('
                tdet.id,
                tdet.idtipe,
                tdet.idbarang,
                up2.nama as dariunit,
                up1.nama as keunit,
                us.name as requester,
                tdet.kuantitas,
                tdet.kuantitassisa,
                tdet.kuantitaskirim,
                tdet.kuantitasalih,
                thead.idunitpeminta,
                thead.idunitpenerima,
                thead.iduser,
            ');
            $this->datatables->join('tunit_permintaan thead', 'thead.id = tdet.idpermintaan', 'left');
            $this->datatables->join('munitpelayanan up1', 'thead.idunitpeminta = up1.id', 'left');
            $this->datatables->join('munitpelayanan up2', 'thead.idunitpenerima = up2.id', 'left');
            $this->datatables->join('musers us', 'thead.iduser = us.id', 'left');
            $this->datatables->where('tdet.idpermintaan', $id);
            $this->datatables->from('tunit_permintaan_detail tdet');
            return print($this->datatables->generate());
        }
    }

    public function dtbarang() {
        $idtipe = $this->uri->segment(3);
        $idunitpelayanan = $this->input->post('idunitpelayanan');
        if ($idtipe) {
            $table = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
            $tipe = array(null, 'Alkes','Implan','Obat','Logistik');
            $this->load->library('Datatables');
            $this->datatables->add_column('tipe', $tipe[$idtipe]);
            $this->datatables->select('barang.id, barang.kode, barang.nama, COALESCE(gs.stok,0) as stok, barang.hargadasar,barang.marginumum');
            $this->db->join('mgudang_stok as gs', 'barang.id=gs.idbarang AND gs.idunitpelayanan='.$idunitpelayanan.' AND gs.idtipe='.$idtipe, 'left');
            $this->datatables->from($table[$idtipe].' as barang');
            return print($this->datatables->generate());
        }
    }
	public function filter_distributor() {
        // $idtipe = $this->uri->segment(3);
        // $idunitpelayanan = $this->input->post('idunitpelayanan');
        // if ($idtipe) {
            // $table = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
            // $tipe = array(null, 'Alkes','Implan','Obat','Logistik');
            $this->load->library('Datatables');
            // $this->datatables->add_column('tipe', $tipe[$idtipe]);
            $this->datatables->select('mdistributor.id,mdistributor.nama,mdistributor.alamat,mdistributor.telepon');
            // $this->db->join('mgudang_stok as gs', 'barang.id=gs.idbarang AND gs.idunitpelayanan='.$idunitpelayanan.' AND gs.idtipe='.$idtipe, 'left');
            $this->datatables->from('mdistributor');
            return print($this->datatables->generate());
        // }
    }

    public function selectdariunit() {
        $id = $this->uri->segment(3);
        $res = array();
        if ($id) {
            $this->db->select('munitpelayanan.id, munitpelayanan.nama as text');
            $this->db->where('munitpelayanan_user.userid', $this->session->userdata('user_id'));
            $this->db->join('munitpelayanan', 'munitpelayanan_user.idunitpelayanan = munitpelayanan.id', 'left');
            $this->db->where('munitpelayanan.id', $id);
            $res = $this->db->get('munitpelayanan_user')->row();
        } else {
            $this->db->select('munitpelayanan.id, munitpelayanan.nama as text');
            $this->db->where('munitpelayanan_user.userid', $this->session->userdata('user_id'));
            $this->db->join('munitpelayanan', 'munitpelayanan_user.idunitpelayanan = munitpelayanan.id', 'left');
            if ($this->input->get('q')) {
                $this->db->like('munitpelayanan.nama', $this->input->get('q'), 'BOTH');
            }
            $res = $this->db->get('munitpelayanan_user')->result();
        }
        $this->output->set_output(json_encode($res));
    }
    public function selectdariunit_index() {
        
            $this->db->select('munitpelayanan.id, munitpelayanan.nama as text');
            $this->db->where('munitpelayanan_user.userid', $this->session->userdata('user_id'));
            $this->db->join('munitpelayanan', 'munitpelayanan_user.idunitpelayanan = munitpelayanan.id', 'left');
            // $this->db->where('munitpelayanan.id', $id);
            $res = $this->db->get('munitpelayanan_user')->result();
      // print_r($this->db->last_query());exit();
			return $res;
    }
    public function selectkeunit() {
        
		  $idunitdari = $this->uri->segment(3);
		  $idtipe = $this->uri->segment(4);
		  $idbarang = $this->uri->segment(5);
		
		$q="SELECT IF(T.available IS NULL,'00',T.id) as id,IF(T.available IS NULL,CONCAT(T.text,' [TIDAK TERSEDIA]'),IF(T.stok>0, CONCAT(T.text,' *'), T.text)) as text
				FROM (SELECT
				`munitpelayanan`.`id`,
				`munitpelayanan`.`nama` AS `text`,
				COALESCE((SELECT mgudang_stok.stok FROM mgudang_stok 
				WHERE mgudang_stok.idunitpelayanan = munitpelayanan.id AND mgudang_stok.idtipe='$idtipe' AND mgudang_stok.idbarang='$idbarang'),0) as stok,
				(SELECT munitpelayanan_tipebarang.id from munitpelayanan_tipebarang WHERE munitpelayanan_tipebarang.idtipe='$idtipe' AND munitpelayanan_tipebarang.idunitpelayanan=munitpelayanan.id limit 1) as available
			FROM
				`munitpelayanan_permintaan`
				LEFT JOIN `munitpelayanan` ON `munitpelayanan_permintaan`.`idunitpenerima` = `munitpelayanan`.`id` 
			WHERE
				`idunitpeminta` = '$idunitdari') T";
		$query=$this->db->query($q);
		$res=$query->result();
        $this->output->set_output(json_encode($res));
    }
	public function get_tipe($idunit) {
        
		$q="SELECT H.idtipe as id,T.nama_tipe as text from munitpelayanan_tipebarang H
LEFT JOIN mdata_tipebarang T ON T.id=H.idtipe
WHERE H.idunitpelayanan='$idunit'
";
		$query=$this->db->query($q);
		$res=$query->result();
        $this->output->set_output(json_encode($res));
    }
	 public function selectkeunit_array() {
        
		$iddariunit = $this->input->post('idunittampil');		
		// print_r($iddariunit);exit();
		// $dariunit=implode(",", $iddariunit);
		$q="SELECT U.id,U.nama as text FROM munitpelayanan_permintaan Unit
				LEFT JOIN munitpelayanan U ON U.id=Unit.idunitpenerima
				WHERE Unit.idunitpeminta IN (".$iddariunit.")
				GROUP BY U.id";
		$query=$this->db->query($q);
		$res=$query->result();
			
		if ($iddariunit !=''){
			$this->output->set_output(json_encode($res));
			// return $res;
		}else{
			$this->output->set_output(json_encode(array()));
		}
    }
	public function list_tampil(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT `munitpelayanan`.`id`, `munitpelayanan`.`nama` as `text` FROM `munitpelayanan_user` 
			LEFT JOIN `munitpelayanan` ON `munitpelayanan_user`.`idunitpelayanan` = `munitpelayanan`.`id` 
			WHERE `munitpelayanan_user`.`userid` = '$user_id'

			UNION

			SELECT id,nama as text from munitpelayanan WHERE munitpelayanan.id 
			IN (
				SELECT D.idunitpenerima as id FROM `munitpelayanan_user` 
			LEFT JOIN `munitpelayanan` ON `munitpelayanan_user`.`idunitpelayanan` = `munitpelayanan`.`id` 
			LEFT JOIN munitpelayanan_permintaan D ON D.idunitpeminta=munitpelayanan_user.idunitpelayanan
			WHERE `munitpelayanan_user`.`userid` = '$user_id'
			)";
			return $this->db->query($q)->result();
	}
	public function selectkeunit_index($idunitdari,$tipe='0') {
        
		  // $idunitdari = $this->uri->segment(3);		
		
		$q="SELECT U.id,U.nama as text FROM munitpelayanan_permintaan Unit
			INNER JOIN munitpelayanan U ON U.id=Unit.idunitpenerima
			WHERE Unit.idunitpeminta='$idunitdari'";
		$query=$this->db->query($q);
		$res=$query->result();
			
		if ($tipe){
			return $res;
		}else{
			$this->output->set_output(json_encode($res));
		}
    }
	public function selectkeunit_index2($idunitdari,$tipe='0') {
        
		  // $idunitdari = $this->uri->segment(3);		
		
		$q="SELECT U.id,U.nama as text FROM munitpelayanan_permintaan Unit
			LEFT JOIN munitpelayanan U ON U.id=Unit.idunitpenerima
			WHERE Unit.idunitpeminta IN (".$idunitdari.")";
		$query=$this->db->query($q);
		$res=$query->result();
			
		if ($tipe){
			return $res;
		}else{
			$this->output->set_output(json_encode($res));
		}
    }
	public function user_array() {
        
		  // $idunitdari = $this->uri->segment(3);		
		$user_id=$this->session->userdata('user_id');
		$q="SELECT dari_unit FROM musers where id='$user_id'";
		$query=$this->db->query($q)->row('dari_unit');
		// print_r($query);exit();
		if ($query!=''){
		$query=explode(',',$query);
		}else{
			$query=array();
		}
		return $query;
    }
	public function user_array_dari_unit() {
        
		  // $idunitdari = $this->uri->segment(3);		
		$user_id=$this->session->userdata('user_id');
		$q="SELECT dari_unit FROM musers where id='$user_id'";
		$query=$this->db->query($q)->row('dari_unit');
		// if ($query!=''){
		// $query=explode(',',$query);
		// }else{
			// $query=array();
		// }
		// // print_r($query);exit();
		return $query;
    }
	public function select_ke_unit($id,$idasal){
		$q="SELECT munitpelayanan.id,munitpelayanan.nama from munitpelayanan_permintaan 
		LEFT JOIN munitpelayanan ON munitpelayanan_permintaan.idunitpenerima=munitpelayanan.id
		WHERE munitpelayanan_permintaan.idunitpeminta='$id' AND munitpelayanan.id <> '$idasal'";
		$query=$this->db->query($q);
		return $query->result();
	}
	public function history_alihan($id){
		$q="SELECT H.id,H.nopermintaan,D.tanggal,UD.nama as dari,UK.nama as ke,musers.`name` FROM tunit_permintaan H
			LEFT JOIN tunit_permintaan_alihan D ON H.nopermintaan=D.nopermintaan
			LEFT JOIN munitpelayanan UD ON UD.id=D.id_dari
			LEFT JOIN munitpelayanan UK ON UK.id=D.id_ke
			LEFT JOIN musers ON musers.id=D.iduser_peminta
			WHERE H.id='$id' 
			ORDER BY D.tanggal DESC";
			$query=$this->db->query($q);
			return $query->result();
	}
    public function list_cetak_pengiriman($id){
		$q="SELECT K.idpermintaan as id,
			K.tanggal_kirim,K.user_nama_kirim,GROUP_CONCAT(K.namabarang,' (',K.kuantitas,')<br>') as namabarang,sum(K.kuantitas) as kuantitas
			from tunit_pengiriman K 
			WHERE K.idpermintaan='$id'
			GROUP BY DATE_FORMAT(K.tanggal_kirim,'%Y-%m-%d %H:%i') ";
			$query=$this->db->query($q);
			return $query->result();
	}
    public function selectkeunit_validasi() {
       
		  $idunitdari = $this->uri->segment(3);
		  $idtipe = $this->uri->segment(4);
		  $idbarang = $this->uri->segment(5);
		  $idunitke = $this->uri->segment(6);
		
		$q="SELECT 
				`munitpelayanan`.`id`,
				`munitpelayanan`.`nama` AS `text`,
				COALESCE((SELECT mgudang_stok.stok FROM mgudang_stok 
				WHERE mgudang_stok.idunitpelayanan = munitpelayanan.id AND mgudang_stok.idtipe='$idtipe' AND mgudang_stok.idbarang='$idbarang'),0) as stok,
				(SELECT munitpelayanan_tipebarang.id from munitpelayanan_tipebarang WHERE munitpelayanan_tipebarang.idtipe='$idtipe' AND munitpelayanan_tipebarang.idunitpelayanan=munitpelayanan.id limit 1) as available
			FROM
				`munitpelayanan_permintaan`
				LEFT JOIN `munitpelayanan` ON `munitpelayanan_permintaan`.`idunitpenerima` = `munitpelayanan`.`id` 
			WHERE
				`idunitpeminta` = '$idunitdari' AND munitpelayanan.id='$idunitke'";
		$query=$this->db->query($q);
		$res=$query->row();
        $this->output->set_output(json_encode($res));
    }
    public function ke_unit_list_detail($idunitdari='0',$idtipe='0',$idbarang='0',$idunitke='0') {
       
		  // $idunitdari = $this->uri->segment(3);
		  // $idtipe = $this->uri->segment(4);
		  // $idbarang = $this->uri->segment(5);
		  // $idunitke = $this->uri->segment(6);
		
		$q="SELECT 
				`munitpelayanan`.`id`,
				`munitpelayanan`.`nama` AS `nama`,
				COALESCE((SELECT mgudang_stok.stok FROM mgudang_stok 
				WHERE mgudang_stok.idunitpelayanan = munitpelayanan.id AND mgudang_stok.idtipe='$idtipe' AND mgudang_stok.idbarang='$idbarang'),0) as stok,
				COALESCE((SELECT munitpelayanan_tipebarang.id from munitpelayanan_tipebarang WHERE munitpelayanan_tipebarang.idtipe='$idtipe' AND munitpelayanan_tipebarang.idunitpelayanan=munitpelayanan.id limit 1),'00') as available
			FROM
				`munitpelayanan_permintaan`
				LEFT JOIN `munitpelayanan` ON `munitpelayanan_permintaan`.`idunitpenerima` = `munitpelayanan`.`id` 
			WHERE
				`idunitpeminta` = '$idunitdari' AND munitpelayanan.id <> '$idunitke'";
		$query=$this->db->query($q);
		$res=$query->result();
        // $this->output->set_output(json_encode($res));
		// print_r($q);exit();
		return $res;
    }
    public function selectkeunit_tipe() {
        
		  $idunitdari = $this->uri->segment(3);
		  $idtipe = $this->uri->segment(4);
		$q="SELECT IF(T.available IS NULL,'00',T.id) as id,IF(T.available IS NULL,CONCAT(T.text,' [TIDAK TERSEDIA]'),T.T.text) as text FROM (SELECT
				`munitpelayanan`.`id`,
				`munitpelayanan`.`nama` AS `text`,(SELECT munitpelayanan_tipebarang.id from munitpelayanan_tipebarang WHERE munitpelayanan_tipebarang.idtipe='$idtipe' AND munitpelayanan_tipebarang.idunitpelayanan=munitpelayanan.id limit 1) as available
			FROM
				`munitpelayanan_permintaan`
				LEFT JOIN `munitpelayanan` ON `munitpelayanan_permintaan`.`idunitpenerima` = `munitpelayanan`.`id` 
				
			WHERE
				`idunitpeminta` = '$idunitdari') T
				";
		$query=$this->db->query($q);
		$res=$query->result();
        $this->output->set_output(json_encode($res));
    }
    public function selectdariunit_alihan() {
        $idunitdari = $this->uri->segment(3);
        $this->db->select('id, nama as text');
        $this->db->where('id !=', $idunitdari);
        if ($this->input->get('q')) {
            $this->db->like('nama', $this->input->get('q'), 'BOTH');
        }
        $res = $this->db->get('munitpelayanan')->result();
        $this->output->set_output(json_encode($res));
    }
    public function selectdistributor() {
        if ($this->uri->segment(3)) {
            $this->db->select('id, nama as text');
            $res = $this->db->get('mdistributor')->row();
        } else {
            $this->db->select('id, nama as text');
            if ($this->input->get('q')) {
                $this->db->like('nama', $this->input->get('q'), 'BOTH');
            }
            $res = $this->db->get('mdistributor')->result();
        }
        $this->output->set_output(json_encode($res));
    }
    public function selectbarang() {
        $idtipe             = $this->input->post('idtipe');
		$idtipe=implode(",", $idtipe);
		// print_r($idtipe);exit();
        $idunitpelayanan    = $this->input->post('idunitpelayanan');
        $idunitke    = $this->input->post('idunitke');
		$term = $this->input->post('search');
       
			$q="SELECT B.idtipe,B.namatipe, B.kode,B.id,B.nama,S.stok from view_barang B
			LEFT JOIN mgudang_stok S ON S.idtipe=B.idtipe AND B.id=S.idbarang AND S.idunitpelayanan='$idunitpelayanan'
			WHERE  B.idtipe IN ($idtipe)  AND  (B.nama LIKE '%".$term."%' OR B.kode LIKE '%".$term."%' )
			LIMIT 100";
			// print_r($q);exit();
			$res=$this->db->query($q)->result_array();	
            $this->output->set_output(json_encode($res));
    }
	public function selectbarang_edit() {
        $idtipe             = $this->input->post('idtipe');
        $idunitpelayanan    = $this->input->post('idunitpelayanan');
        $term = $this->input->post('search');
		$panjang=strlen($term);
		 if ($idtipe) {
			// $q="SELECT B.id,B.nama,B.namatipe,
			// COALESCE((SELECT mgudang_stok.stok from mgudang_stok where mgudang_stok.idunitpelayanan='$idunitpelayanan' AND mgudang_stok.idtipe=B.idtipe 
			// AND mgudang_stok.idbarang=B.id),0) as stok FROM view_barang B
			// WHERE B.idtipe='$idtipe' AND (B.nama like '%".$term."%')";
			$q="SELECT T.idbarang as id,IF(T.stok>0,CONCAT(T.nama,' *'),T.nama) as nama FROM (SELECT G.idbarang,B.nama,G.stok FROM mgudang_stok G
				LEFT JOIN view_barang B ON B.idtipe=G.idtipe AND B.id=G.idbarang
				WHERE G.idunitpelayanan='$idunitpelayanan' AND G.idtipe='$idtipe' AND B.nama like '%".$term."%'
				) T ";
			$query=$this->db->query($q);
			$res=$query->result();
			$this->output->set_output(json_encode($res));
		 }
    }
	public function view_barang() {
        $idtipe             = $this->input->post('idtipe');
        $idunitpelayanan    = $this->input->post('idunitpelayanan');
		 if ($idtipe) {
			
			$q="(SELECT B.id,B.nama,B.namatipe,B.kode,B.hargabeli,
			COALESCE((SELECT mgudang_stok.stok from mgudang_stok 
			where mgudang_stok.idunitpelayanan='$idunitpelayanan' AND mgudang_stok.idtipe=B.idtipe AND mgudang_stok.idbarang=B.id),0) as stok 
			FROM view_barang B
			WHERE B.idtipe='$idtipe')T ";
			$this->load->library('datatables'); 
			$this->datatables->select('id,nama,namatipe,kode,stok,hargabeli'); 
			$this->datatables->from($q); 
			$this->datatables->add_column("Actions", "<center></center>", "id");
			$this->datatables->unset_column('id');
			// echo $this->datatables->generate();
			
			 return print($this->datatables->generate());
			// $query=$this->db->query($q);
			// $res=$query->result();
			// $this->output->set_output(json_encode($res));
		 }
    }
	public function selected_barang() {
        $idtipe             = $this->input->post('idtipe');
        $idunitpelayanan    = $this->input->post('idunitpelayanan');
        $idbarang    = $this->input->post('idbarang');
		 if ($idtipe) {
			
			$q="SELECT T.stok FROM (SELECT G.idbarang,B.nama,G.stok FROM mgudang_stok G
				LEFT JOIN view_barang B ON B.idtipe=G.idtipe AND B.id=G.idbarang
				WHERE G.idunitpelayanan='$idunitpelayanan' AND G.idtipe='$idtipe' AND G.idbarang='$idbarang'
				) T ";
			$query=$this->db->query($q);
			$res=$query->row();
			$res=($res!=null)?$res->stok:0;
			$this->output->set_output(json_encode($res));
		 }
    }
	public function selected_barang_kode() {
        $idtipe             = $this->input->post('idtipe');
        $idunitpelayanan    = $this->input->post('idunitpelayanan');
        $idbarang    = $this->input->post('idbarang');
		 if ($idtipe) {
			
			$q="SELECT T.stok,T.kode FROM (SELECT G.idbarang,B.nama, G.stok,B.kode FROM mgudang_stok G
				LEFT JOIN view_barang B ON B.idtipe=G.idtipe AND B.id=G.idbarang
				WHERE G.idunitpelayanan='$idunitpelayanan' AND G.idtipe='$idtipe' AND G.idbarang='$idbarang'
				) T ";
			$query=$this->db->query($q);
			$res=$query->row();
			// $res=($res!=null)?$res->stok:0;
			$this->output->set_output(json_encode($res));
		 }
    }
	public function selected_barang_harga() {
        $idtipe             = $this->input->post('idtipe');
        $idunitpelayanan    = $this->input->post('idunitpelayanan');
        $idbarang    = $this->input->post('idbarang');
		 if ($idtipe) {
			
			$q="SELECT B.id,B.nama,B.kode,COALESCE(B.hargabeli,0) as hargabeli,
				COALESCE(B.hargabeli_besar,0) as hargabeli_besar,B.jumlahsatuanbesar,sk.nama as satuan_kecil,sb.nama as satuan_besar 
				FROM view_barang B
				LEFT JOIN msatuan sk ON sk.id=B.idsatuan
				LEFT JOIN msatuan sb ON sb.id=B.idsatuanbesar
				WHERE  B.idtipe='$idtipe' AND B.id='$idbarang'";
			$query=$this->db->query($q);
			$res=$query->row();
			// $res=($res!=null)?$res->stok:0;
			$this->output->set_output(json_encode($res));
		 }
    }
	
    public function ajax_distributor_tersering() {
        $idtipe = $this->input->post('idtipe');
        $idbarang = $this->input->post('idbarang');
        $table = "(SELECT
        iddistributor, total FROM (
        SELECT
        g.iddistributor,
        COUNT(g.iddistributor) total
        FROM tgudang_pemesanan_detail gdet
        INNER JOIN tgudang_pemesanan g ON g.id = gdet.idpemesanan
        WHERE idbarang = ".$idbarang." AND idtipe = ".$idtipe."
        AND g.status != 1 AND g.status != 0
        GROUP BY g.iddistributor ) t
        ORDER BY total DESC LIMIT 1) tbl";
        $this->db->select('iddistributor');
        $res = $this->db->get($table);
        if ($res->num_rows() > 0) {
            $iddistributor = $res->row()->iddistributor;
        } else {
            $iddistributor = '';
        }
        $this->output->set_output(json_encode($iddistributor));
    }

    public function ajax_cekstatuspermintaan() {
        $id = $this->input->post('id');
        if ($id) {
            $this->db->where('id', $id);
            $status = $this->db->get('tunit_permintaan', 1)->row()->status;
            $this->output->set_output(json_encode($status));
        }
    }

    public function ajax_stokunit() {
        $dariunit = $this->input->get('dari');
        $idunit = $this->input->get('idunit');
        $idbarang = $this->input->get('idbarang');
        $idtipe = $this->input->get('idtipe');

        if ($idtipe && $idbarang) {
            // $qry="
                // SELECT COUNT(*) as 'count'
                // FROM munitpelayanan u, munitpelayanan x
                // WHERE u.id IS NOT NULL
                // AND u.id=$dariunit
                // AND x.id=$idunit
                // AND (
                    // (u.statuslock=0 and x.statuslock=0)
                    // OR
                    // (u.statuslock=1 and x.statuslock=1)
                    // OR
                    // (u.statuslock=1 and x.statuslock=0)
                // )";
            // $found=$this->db->query($qry)->row()->count;
            // if (intval($found)!=0) {
                // $this->db->where('idunitpelayanan', $idunit);
                // $this->db->where('idtipe', $idtipe);
                // $this->db->where('idbarang', $idbarang);
                // $row = $this->db->get('mgudang_stok', 1);
                // $res=$row->row();
                // $res=($res!=null)?$res->stok:0;
            // } else {
                // $res=0;
            // }
            // $this->output->set_output(json_encode($res));
			
			$q="SELECT IF(CONCAT(D.statuslock,K.statuslock) <> '01', 1, 0) as st_tampil FROM munitpelayanan D, munitpelayanan K
					WHERE D.id IS NOT NULL
					AND D.id='$dariunit'
					AND K.id='$idunit'";
			$query=$this->db->query($q);
			$arr['st_tampil'] =$query->row('st_tampil');
			$this->db->where('idunitpelayanan', $idunit);
			$this->db->where('idtipe', $idtipe);
			$this->db->where('idbarang', $idbarang);
			$row = $this->db->get('mgudang_stok', 1);
			$res=$row->row();
			$res=($res!=null)?$res->stok:0;
			$arr['stok'] =$res;
			$this->output->set_output(json_encode($arr));
        }
    }
	public function ajax_cek_stok() {
        $iddari = $this->input->post('iddari');
        $idke = $this->input->post('idke');
        $idbarang = $this->input->post('idbarang');
        $idtipe = $this->input->post('idtipe');

        // if ($idtipe && $idbarang) {
           $q="SELECT IF(CONCAT(D.statuslock,K.statuslock) <> '01', 1, 0) as st_tampil FROM munitpelayanan D, munitpelayanan K
					WHERE D.id IS NOT NULL
					AND D.id='$iddari'
					AND K.id='$idke'";
			$query=$this->db->query($q);
			$arr['st_tampil'] =$query->row('st_tampil');
			
			$q="SELECT G.*,CASE WHEN U.id IS NOT NULL THEN 1 ELSE 0 END  as available FROM mgudang_stok G
				LEFT JOIN munitpelayanan_tipebarang U ON U.idunitpelayanan='$idke' AND U.idtipe='$idtipe'
				WHERE G.idbarang='$idbarang' AND G.idtipe='$idtipe' AND G.idunitpelayanan='$idke'";
				// print_r($q);exit();
			$row=$this->db->query($q)->row();
			if ($row){
				$arr['available']=$row->available;
				$arr['stok']=($row->stok!=null)?$row->stok:0;
			}else{
				$arr['available']=0;
				$arr['stok']=0;
			}
			$this->output->set_output(json_encode($arr));
        // }
    }
	public function ajax_cek_stok2() {
        $iddari = $this->input->post('iddari');
        $idke = $this->input->post('idke');
        $idbarang = $this->input->post('idbarang');
        $idtipe = $this->input->post('idtipe');

        // if ($idtipe && $idbarang) {
           $q="SELECT IF(CONCAT(D.statuslock,K.statuslock) <> '01', 1, 0) as st_tampil FROM munitpelayanan D, munitpelayanan K
					WHERE D.id IS NOT NULL
					AND D.id='$iddari'
					AND K.id='$idke'";
			$query=$this->db->query($q);
			$arr['st_tampil'] =$query->row('st_tampil');
			
			$q="SELECT G.*,CASE WHEN U.id IS NOT NULL THEN 1 ELSE 0 END  as available FROM mgudang_stok G
				LEFT JOIN munitpelayanan_tipebarang U ON U.idunitpelayanan='$idke' AND U.idtipe='$idtipe'
				WHERE G.idbarang='$idbarang' AND G.idtipe='$idtipe' AND G.idunitpelayanan='$idke'";
				// print_r($q);exit();
			$row=$this->db->query($q)->row();
			if ($row){
				$arr['available']=$row->available;
				$arr['stok']=($row->stok!=null)?$row->stok:0;
			}else{
				$arr['available']=0;
				$arr['stok']=0;
			}
			$stok_dari=detail_stok($idtipe,$idbarang,$iddari);
			$arr['stok_dari']=($stok_dari != null)?$stok_dari:0;
			$this->output->set_output(json_encode($arr));
        // }
    }

    public function save() {
        $status=false;
        $totalbarang = 0;
        
        $dariunit=$this->input->post("d_dariunit");
        $keunit=$this->input->post("d_keunit");
		// print_r($keunit);exit();
        $namakeunit=$this->input->post("d_namakeunit");
        $iduser=$this->input->post("d_iduser");
        $namauser=$this->input->post("d_namauser");
        $idbarang=$this->input->post("d_idbarang");
        $idtipe=$this->input->post("d_idtipe");
        $kuantitas=$this->input->post("d_kuantitas");
        $sisa=$this->input->post("d_stokunit");
        $hargabarang=$this->input->post("d_harga");
        $marginbarang=$this->input->post("d_margin");
        $namabarang=$this->input->post("d_namabarang");
		$tgl=YMDTimeFormat($this->input->post('tanggal2'));
		
        $xmaps=array();
        for ($i=0;$i<count($dariunit);$i++) {
            $maps['dariunit']=$dariunit[$i];
            $maps['keunit']=$keunit[$i];
            $maps['iduser']=$iduser[$i];
            $maps['namauser']=$namauser[$i];
            $maps['namakeunit']=$namakeunit[$i];
            $xmaps[$i]=$maps;
        }
        $maps=array_map("unserialize", array_unique(array_map("serialize", $xmaps)));
        foreach ($maps as $val) {
            $map=$val;
			
			// print_r($tgl);exit();
            $this->db->set('datetime', $tgl);
            $this->db->set('nopermintaan', '-');
            $this->db->set('idunitpenerima', $map['dariunit']);
            $this->db->set('idunitpeminta', $map['keunit']);
            $this->db->set('iduser', $map['iduser']);
            $this->db->set('refid', '0');
            $this->db->set('status', 1);
            $this->db->set('namauser', $map['namauser']);
            $this->db->set('namaunit_peminta', RemoveBintang($map['namakeunit']));
			// $data_alihan=array();
			// $data_alihan['tanggal']=date("Y-m-d H:i:s");
			$data_alihan['tanggal']=date("Y-m-d H:i:s");
            if ($this->db->insert('tunit_permintaan')) {
                $idpermintaan = $this->db->insert_id();
                for ($i=0;$i<count($dariunit);$i++) {
                    if (($map['dariunit']==$dariunit[$i]) && ($map['keunit']==$keunit[$i]) && ($map['iduser']==$iduser[$i])) {
                        $this->db->set('idpermintaan', $idpermintaan);
                        $this->db->set('idtipe', $idtipe[$i]);
                        $this->db->set('idbarang', $idbarang[$i]);
                        $this->db->set('kuantitas', $kuantitas[$i]);
                        $this->db->set('kuantitaskirim', 0);
                        $this->db->set('kuantitassisa', $kuantitas[$i]);
                        $this->db->set('kuantitasalih', 0);
                        $this->db->set('status', 1);
                        $this->db->set('sisa_stok', $sisa[$i]);
                        $this->db->set('hargabarang', $hargabarang[$i]);
                        $this->db->set('marginbarang', $marginbarang[$i]);
                        $this->db->set('namabarang', $namabarang[$i]);
    
                        if ($this->db->insert('tunit_permintaan_detail')) {
                            $totalbarang = $totalbarang + RemoveComma($kuantitas[$i]);
                        }

                        $this->db->set('totalbarang', $totalbarang);
                        $this->db->where('id', $idpermintaan);
                        if ($this->db->update('tunit_permintaan')) {
                            $status = true;
                        } else {
                            $status = false;
                        }
                    }
                }
            }
        }
        return $status;
    }

    public function ajax_namabarang() {
        $idtipe = $this->input->post('idtipe');
        $idbarang = $this->input->post('idbarang');
        if ($idtipe && $idbarang) {
            $table = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
            $this->db->select('nama');
            $this->db->where('id', $idbarang);
            $res = $this->db->get($table[$idtipe], 1)->row()->nama;
            $this->output->set_output(json_encode($res));
        }
    }

    public function save_acc_pengiriman() {
        $idpermintaan = $this->uri->segment(3);
		// $tgl_kirim=date("Y-m-d H:i:s");
			$tgl_kirim=YMDTimeFormat($this->input->post('tanggal'));
			// print_r($tgl_kirim);exit();
		$data=array();
        if ($idpermintaan) {
			
            $id             = $this->input->post('id');
            $kuantitaskirim = $this->input->post('kuantitaskirim');
			// $tgl_kirim=date("Y-m-d H:i:s");
			foreach ($id as $key => $val) {//INSERT KE TABEL PENGIRIMAN
				if ($kuantitaskirim[$key]>0){
					
					$row=$this->get_detail_barang($id[$key]);
					if ($row){
						$data['tanggal_kirim']=$tgl_kirim;
						$data['idpermintaan']=$idpermintaan;
						$data['iddet']=$id[$key];
						$data['idtipe']=$row['idtipe'];
						$data['idbarang']=$row['idbarang'];
						$data['namabarang']=$row['namabarang'];
						$data['kuantitas']=$kuantitaskirim[$key];
						$data['kuantitas_terima']='0';
						$data['user_kirim']=$this->session->userdata("user_id");
						$data['user_nama_kirim']=$this->session->userdata("user_name");
					}
					// print_r($this);exit();					
					// $this->db->insert('tunit_pengiriman',$data);
					$this->db->insert('tunit_pengiriman', $data);
					// print_r($this->db->last_insert_id());exit();
				}
            }
			
            $this->db->set('iduser_delivery', $this->session->userdata("user_id"));
            $this->db->set('namauser_delivery', $this->session->userdata("user_name"));
            $this->db->set('datetime_delivery', date("Y-m-d H:i:s"));
            $this->db->where('id', $idpermintaan);
            $this->db->update('tunit_permintaan');

            foreach ($id as $key => $val) {
				if ($kuantitaskirim[$key]>0){
                $this->db->set('kuantitaskirim',$kuantitaskirim[$key],'FALSE');
                $this->db->where('id', $id[$key]);
                $this->db->where('idpermintaan', $idpermintaan);
                $this->db->update('tunit_permintaan_detail');
				}
            }
            return true;
        }
    }
	public function save_edit(){
		$idpermintaan=$this->input->post('idpermit');
		$idbarang=$this->input->post('idbarang');
		$st_edit=$this->input->post('st_edit');
		$st_add=$this->input->post('st_add');
		$qty=$this->input->post('qty');
		$id_det=$this->input->post('id_det');
		$namabarang=$this->input->post('nama_barang');
		$idtipe=$this->input->post('e_idtipe');
		foreach ($st_edit as $key => $val) {
			if ($st_edit[$key]=='1'){
				if ($st_add[$key]=='1'){
					$data_insert=array();
					$data_insert['idpermintaan']=$idpermintaan;
					$data_insert['idbarang']=$idbarang[$key];
					$data_insert['idtipe']=$id_det[$key];
					$data_insert['kuantitas']=$qty[$key];
					$data_insert['status']=1;
					$data_insert['kuantitasalih']=0;
					$data_insert['kuantitaskirim']=0;
					$data_insert['kuantitassisa']=$qty[$key];
					$data_insert['namabarang']=RemoveBintang($namabarang[$key]);
					
					// print_r($data_insert);exit();
					// $data_update['kuantitas']=$qty[$key];
					// $this->db->where('id', $id_det[$key]);
					$this->db->insert('tunit_permintaan_detail',$data_insert);
				}else{
					$data_update=array();
					$data_update['idbarang']=$idbarang[$key];
					$data_update['idtipe']=$idtipe[$key];
					$data_update['kuantitas']=$qty[$key];
					$data_update['kuantitassisa']=$qty[$key];
					$data_update['namabarang']=RemoveBintang($namabarang[$key]);
					// print_r($id_det[$key]);exit();
					$this->db->where('id', $id_det[$key]);
					$this->db->update('tunit_permintaan_detail',$data_update); 
				}
			}
		}
		return true;
	}
	public function save_alihkan() {
        $idpermintaan = $this->input->post('idpermintaan');
		$data=array();
		$data_header=array();
		$notrx = $this->input->post('notrx');
		$keunit_detail = $this->input->post('keunit_detail');
		$dari_unit = $this->input->post('dariunit');
		$id_ke_asal = $this->input->post('id_ke_asal');
		asort($keunit_detail);
		// print_r($this->input->post());exit();
        if ($idpermintaan) {
			
            $id             = $this->input->post('iddet');
            $kuantitaskirim = $this->input->post('kuantitaskirim');
            // $keunit_detail = $this->input->post('keunit_detail');
			$id_keunit='';
			$last_insert_id;
			$tgl_alihkan=date("Y-m-d H:i:s");
			foreach ($keunit_detail as $key => $val) {//INSERT KE HEADER
				if ($kuantitaskirim[$key]>0){
					if ($id_keunit <> $keunit_detail[$key]){
						$data_header['datetime']=$tgl_alihkan;
						$data_header['nopermintaan']=$notrx;
						$data_header['idunitpeminta']=$val;//KE
						$data_header['idunitpenerima']=$dari_unit;//Dari
						$data_header['totalbarang']=0;//Dari
						$data_header['totalbarangalih']=0;
						$data_header['refid']=$idpermintaan;
						$data_header['status']=1;
						$data_header['iduser']=$this->session->userdata("user_id");
						$data_header['namauser']=$this->session->userdata("user_name");
						$data_header['iduser_forward']=$this->session->userdata("user_id");
						$data_header['namauser_forward']=$this->session->userdata("user_name");
						// $data_header['']=
						$this->db->insert('tunit_permintaan',$data_header);
						$last_insert_id=$this->db->insert_id();
						$id_keunit=$keunit_detail[$key];
						
						$data_alihan=array();
						$data_alihan['tanggal']=$tgl_alihkan;
						$data_alihan['idpermintaan']=$last_insert_id;
						$data_alihan['nopermintaan']=$notrx;
						$data_alihan['id_dari_awal']=$dari_unit;
						$data_alihan['id_dari']=$id_ke_asal;
						$data_alihan['id_ke']=$val;
						$data_alihan['st_alihkan']='1';
						$data_alihan['iduser_peminta']='1';
						$data_alihan['iduser_peminta']=$this->session->userdata("user_id");
						$data_alihan['iduser_alihkan']=$this->session->userdata("user_id");
						$this->db->insert('tunit_permintaan_alihan',$data_alihan);
					}
					// print_r($data_header);exit();
					$row=$this->get_detail_barang($id[$key]);
					if ($row){
						// $data['tanggal_kirim']=date("Y-m-d H:i:s");
						$data['idpermintaan']=$last_insert_id;
						// $data['iddet']=$id[$key];
						$data['idtipe']=$row['idtipe'];
						$data['idbarang']=$row['idbarang'];
						$data['namabarang']=$row['namabarang'];
						$data['kuantitas']=$kuantitaskirim[$key];
						$data['kuantitaskirim']='0';
						$data['kuantitassisa']=$kuantitaskirim[$key];
						$data['kuantitasalih']='0';
						$data['stpemesanan']='0';
						$data['status']='1';						
						$this->db->insert('tunit_permintaan_detail', $data);
						
						//Update Total Barang
						$this->db->set('totalbarang', 'totalbarang+'.$kuantitaskirim[$key], FALSE);
						$this->db->where('id', $last_insert_id);
						$this->db->update('tunit_permintaan'); 
						
						
						//Update Alihkan
						$this->db->set('kuantitasalih', 'kuantitasalih+'.$kuantitaskirim[$key], FALSE);
						$this->db->set('kuantitassisa', 'kuantitassisa-'.$kuantitaskirim[$key], FALSE);
						$this->db->where('id', $id[$key]);
						$this->db->update('tunit_permintaan_detail'); 
						
					}
					
				}
            }
			
            return true;
        }
    }
	public function get_detail_barang($id){//GetDetailPermintaan
		$q="SELECT *from tunit_permintaan_detail WHERE id='$id'";
		$query=$this->db->query($q);
		$query=$query->row_array();
		return $query;
	}
    public function save_pengalihan_permintaan() {
        $idpermintaanhead = $this->input->post('idpermintaanhead');
        $detailvalue_alihan = $this->input->post('detailvalue_alihan');
        $idunitpeminta = $this->input->post('idunitpeminta');
        $idunitpenerima = $this->input->post('idunitpenerima');
        $detailvalue_alihan = json_decode($detailvalue_alihan);

        $totalbarang = 0;

        $this->db->set('nopermintaan', '-');
        $this->db->set('idunitpeminta', $idunitpeminta);
        $this->db->set('idunitpenerima', $idunitpenerima);
        $this->db->set('totalbarangalih', 0);
        $this->db->set('refid', $idpermintaanhead);
        $this->db->set('status', 1);
        $this->db->set('iduser_forward', $this->session->userdata("user_id"));
        $this->db->set('namauser_forward', $this->session->userdata("user_name"));

        if ($this->db->insert('tunit_permintaan')) {
            $idpermintaanbaru = $this->db->insert_id();
            foreach ($detailvalue_alihan as $r) {
                if ($r[9] != 0) {
                    $this->db->set('kuantitasalih', 'kuantitasalih + '.$r[9], false);
                    $this->db->where('id', $r[8]);
                    if ($this->db->update('tunit_permintaan_detail')) {
                        $this->db->set('idpermintaan', $idpermintaanbaru);
                        $this->db->set('idtipe', $r[6]);
                        $this->db->set('idbarang', $r[7]);
                        $this->db->set('kuantitas', $r[9]);
                        $this->db->set('kuantitaskirim', 0);
                        $this->db->set('kuantitassisa', $r[9]);
                        $this->db->set('kuantitasalih', 0);
                        $this->db->set('status', 1);
                        if ($this->db->insert('tunit_permintaan_detail')) {
                            $totalbarang = $totalbarang + $r[9];
                        }
                    }
                }
            }
        }

        // update totalbarang
        $this->db->set('totalbarang', $totalbarang);
        $this->db->where('id', $idpermintaanbaru);
        if ($this->db->update('tunit_permintaan')) {
            $status = true;
        } else {
            $status = false;
        }

        return $status;
    }

    public function ajax_pernah_terima() {
        $idpermintaan = $this->input->post('idpermintaan');
        $idtipe = $this->input->post('idtipe');
        $idbarang = $this->input->post('idbarang');

        $this->db->select('COALESCE( SUM(tdet.kuantitas), 0 ) AS diterima');
        $this->db->join('tunit_penerimaan thead', 'thead.id = tdet.idpenerimaan', 'left');
        $this->db->where('thead.idpermintaan', $idpermintaan);
        $this->db->where('tdet.idbarang', $idtipe);
        $this->db->where('tdet.idtipe', $idbarang);
        $res = $this->db->get('tunit_penerimaan_detail tdet', 1)->row()->diterima;
        $this->output->set_output(json_encode($res));
    }

    // public function save_penerimaan() {
        // $idpermintaan = $this->uri->segment(3);
        // if ($idpermintaan) {
            // $idtipe             = $this->input->post('idtipe');
            // $idbarang           = $this->input->post('idbarang');
            // $kuantitasterima    = $this->input->post('kuantitasterima');
            // $hargabarang           = $this->input->post('hargabarang');
            // $marginbarang    = $this->input->post('marginbarang');

            // $totalbarang = 0;
            // $this->db->set('nopenerimaan', '-');
            // $this->db->set('idpermintaan', $idpermintaan);
            // $this->db->set('status', 1);
            // $this->db->set('datetime_penerimaan', date("Y-m-d H:i:s"));
            // $this->db->set('iduser_penerima', $this->session->userdata("user_id"));
            // $this->db->set('namauser_penerima', $this->session->userdata("user_name"));

            // if ($this->db->insert('tunit_penerimaan')) {
                // $idpenerimaan = $this->db->insert_id();

                // foreach ($idtipe as $key => $val) {
                    // $this->db->set('idpenerimaan', $idpenerimaan);
                    // $this->db->set('idtipe', $idtipe[$key]);
                    // $this->db->set('idbarang', $idbarang[$key]);
                    // $this->db->set('kuantitas', $kuantitasterima[$key]);
                    // $this->db->set('status', 1);
                    // $this->db->set('total', $kuantitasterima[$key]*$hargabarang[$key]);
                    // $this->db->set('hargabarang', $hargabarang[$key]);
                    // $this->db->set('marginbarang', $marginbarang[$key]);
                    // $this->db->insert('tunit_penerimaan_detail');
                    

                    // $totalbarang = $totalbarang + $kuantitasterima[$key];
                // }

                // $this->db->set('totalbarang', $totalbarang);
                // $this->db->where('id', $idpenerimaan);
                // if ($this->db->update('tunit_penerimaan')) {
                    // return true;
                // } else {
                    // return false;
                // }
            // }
        // }
    // }
	public function save_penerimaan() {
        $idpermintaan = $this->uri->segment(3);
		$id             = $this->input->post('id');
		$kuantitasterima = $this->input->post('kuantitasterima');
		$tgl_terima=date("Y-m-d H:i:s");
		foreach ($id as $key => $val) {//INSERT KE TABEL PENGIRIMAN
			if ($kuantitasterima[$key]>0){				
					$data['tanggal_terima']=$tgl_terima;
					$data['kuantitas_terima']=$kuantitasterima[$key];
					$data['user_terima']=$this->session->userdata("user_id");
					$data['user_nama_terima']=$this->session->userdata("user_name");
					// print_r($id[$key]);exit();
					
					$this->db->where('id',$id[$key]);
					$this->db->update('tunit_pengiriman', $data);
			}
		}
		return true;
        
    }

    private function gethargabarang($idtipe, $idbarang) {
        $table = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        $this->db->select('hargabeli');
        $this->db->where('id', $idbarang);
        return $this->db->get($table[$idtipe], 1)->row()->hargabeli;
    }

    public function save_pemesanan_distributor_copy() {
		$data_his=array();
        $iddet_permintaan = $this->input->post('iddet_permintaan');
        $unit_peminta = $this->input->post('unit_peminta');
        $no_up_permintaan = $this->input->post('no_up_permintaan');
		
        $iddistributor = $this->input->post('iddistributor');
        $idtipe = $this->input->post('idtipe_pemesanan');
        $idbarang = $this->input->post('idbarang_pemesanan');
        $kuantitas = $this->input->post('kuantitas_pemesanan');
        $idpermintaan = $this->input->post('idpermintaan');
        $hargabeli = $this->gethargabarang($idtipe, $idbarang);


		//MEMBUAT HISTORY PERMINTAAN
		$data_his['iddet_permintaan']=$iddet_permintaan;
		$data_his['unit_peminta']=$unit_peminta;
		$data_his['no_up_permintaan']=$no_up_permintaan;
		
		
        $this->db->where('iddistributor', $iddistributor);
        $this->db->where('status', 1);
        $cekDist = $this->db->get('tgudang_pemesanan');
        if ($cekDist->num_rows() > 0) {
            $rDist          = $cekDist->row();
            $totalHarga     = $hargabeli*$kuantitas;
            $this->db->set('totalbarang', '`totalbarang` + '.$kuantitas, false);
            $this->db->set('totalharga', '`totalharga` + '.$totalHarga, false);
            $this->db->where('id', $rDist->id);
            
            if ($this->db->update('tgudang_pemesanan')) {
                $this->db->where('idpemesanan', $rDist->id);
                $this->db->where('idtipe', $idtipe);
                $this->db->where('idbarang', $idbarang);
                $cekBarang = $this->db->get('tgudang_pemesanan_detail');
                if ($cekBarang->num_rows() > 0) {
                    $rBarang        = $cekBarang->row();
                    $totalHarga     = $hargabeli*$kuantitas;
                    $this->db->set('kuantitas', '`kuantitas` + '.$kuantitas, false);
                    $this->db->set('harga', '`harga` + '.$totalHarga, false);
                    $this->db->where('idpemesanan', $rDist->id);
                    $this->db->where('idtipe', $idtipe);
                    $this->db->where('idbarang', $idbarang);
                    if ($this->db->update('tgudang_pemesanan_detail')) {
                        $status = true;
                    }
                } else {
                    $this->db->set('idpemesanan', $rDist->id);
                    $this->db->set('idtipe', $idtipe);
                    $this->db->set('idbarang', $idbarang);
                    $this->db->set('kuantitas', $kuantitas);
                    $this->db->set('harga', $hargabeli);
                    $this->db->set('status', 1);
                    if ($this->db->insert('tgudang_pemesanan_detail')) {
                        $status = true;
                    }
                }
            }
        } else {
            $this->db->set('nopemesanan', '-');
            $this->db->set('tanggal', date('Y-m-d'));
            $this->db->set('iddistributor', $iddistributor);
            $this->db->set('totalbarang', $kuantitas);
            $this->db->set('totalharga', $hargabeli*$kuantitas);
            $this->db->set('status', 1);
            if ($this->db->insert('tgudang_pemesanan')) {
                $idpemesanan = $this->db->insert_id();
                $this->db->set('idpemesanan', $idpemesanan);
                $this->db->set('idtipe', $idtipe);
                $this->db->set('idbarang', $idbarang);
                $this->db->set('kuantitas', $kuantitas);
                $this->db->set('harga', $hargabeli);
                $this->db->set('status', 1);
                if ($this->db->insert('tgudang_pemesanan_detail')) {
                    $status = true;
                }
            }
        }

        $this->db->set('stpemesanan', 1);
        $this->db->where('idpermintaan', $idpermintaan);
        $this->db->where('idtipe', $idtipe);
        $this->db->where('idbarang', $idbarang);
        if ($this->db->update('tunit_permintaan_detail')) {
            $status = true;
        } else {
            $status = false;
        }
        return $status;
    }

    public function save_pemesanan_distributor() {
		
        $iddet_permintaan = $this->input->post('iddet_permintaan');
        $unit_peminta = $this->input->post('unit_peminta');
        $no_up_permintaan = $this->input->post('no_up_permintaan');
		// print_r($unit_peminta);exit();
        $iddistributor = $this->input->post('iddistributor');
        $idtipe = $this->input->post('idtipe_pemesanan');
        $idbarang = $this->input->post('idbarang_pemesanan');
        $kuantitas = $this->input->post('kuantitas_pemesanan');
        $kuantitas_kecil = $this->input->post('kuantitas_kecil');
        $idpermintaan = $this->input->post('idpermintaan');
        $opsisatuan = $this->input->post('opsisatuan');
        $hargabeli = $this->gethargabarang($idtipe, $idbarang);

        $tipepemesanan = 1;
        if ($idtipe == 4) {
            $tipepemesanan = 2;
        }

		//MEMBUAT HISTORY PERMINTAAN
		$data_his=array();
		$data_his['iddet_permintaan']=$iddet_permintaan;
		$data_his['unit_peminta']=$unit_peminta;
		$data_his['no_up_permintaan']=$no_up_permintaan;
		$data_his['tipepemesanan']=$tipepemesanan;
		$data_his['idtipe']=$idtipe;
		$data_his['idbarang']=$idbarang;
		$data_his['opsisatuan']=$opsisatuan;
		$data_his['kuantitas']=$kuantitas;
		$data_his['kuantitas_kecil']=$kuantitas_kecil;
		$data_his['iduser_peminta']=$this->session->userdata('user_id');
		$data_his['tanggal']=date('Y-m-d H:i:s');
		
		
        $this->db->where('head.stdraft', 1);
        $this->db->where('head.status', 1);
        $this->db->where('head.tipepemesanan', $tipepemesanan);
        $this->db->where('head.iddistributor', $iddistributor);
        $this->db->join('tgudang_pemesanan head', 'head.id = det.idpemesanan', 'left');
        $cekdraft = $this->db->get('tgudang_pemesanan_detail det');
        if ($cekdraft->num_rows() > 0) {//JIka PERNAH ADA
            $cekdraftrow = $cekdraft->row();
            $this->db->where('idpemesanan', $cekdraftrow->id);
            $this->db->where('idtipe', $idtipe);
            $this->db->where('idbarang', $idbarang);
            $cekbarang = $this->db->get('tgudang_pemesanan_detail');
			// print_r($cekBarang);exit();
            if ($cekbarang->num_rows() > 0) {
                $totalharga     = $hargabeli*$kuantitas;
                $this->db->set('kuantitas', '`kuantitas` + '.$kuantitas, false);
                $this->db->set('kuantitas_kecil', '`kuantitas_kecil` + '.$kuantitas_kecil, false);
                $this->db->set('harga', '`harga` + '.$totalharga, false);
                $this->db->where('idpemesanan', $cekdraftrow->id);
                $this->db->where('idtipe', $idtipe);
                $this->db->where('idbarang', $idbarang);
                if ($this->db->update('tgudang_pemesanan_detail')) {
                    $this->db->set('totalbarang', '`totalbarang` + '.$kuantitas, false);
                    $this->db->where('id', $cekdraftrow->id);
                    if ($this->db->update('tgudang_pemesanan')) {
						$idnya=$this->get_id_gudang($cekdraftrow->id,$idtipe,$idbarang);
						$data_his['iddet_gudang']=$idnya;
                        $status = true;
                    } else {
                        $status = false;
                    }
                }
            } else {
                $this->db->set('idpemesanan', $cekdraftrow->id);
                $this->db->set('idtipe', $idtipe);
                $this->db->set('idbarang', $idbarang);
                $this->db->set('kuantitas', $kuantitas);
                $this->db->set('kuantitas_kecil', $kuantitas_kecil);
                $this->db->set('harga', $hargabeli);
                $this->db->set('status', 1);
                $this->db->set('opsisatuan', $opsisatuan);
                if ($this->db->insert('tgudang_pemesanan_detail')) {
					$data_his['iddet_gudang']=$this->db->insert_id();
                    $status = true;
                }
            }
        } else {//CREATE BARU
            $this->db->set('nopemesanan', '-');
            $this->db->set('tipepemesanan', $tipepemesanan);
            $this->db->set('tanggal', date('Y-m-d H:i:s'));
            $this->db->set('iddistributor', $iddistributor);
            $this->db->set('totalbarang', $kuantitas_kecil);
            $this->db->set('totalharga', $hargabeli*$kuantitas_kecil);
            $this->db->set('status', 1);
            $this->db->set('stdraft', 1);
            if ($this->db->insert('tgudang_pemesanan')) {
                $idpemesanan = $this->db->insert_id();
                $this->db->set('idpemesanan', $idpemesanan);
                $this->db->set('idtipe', $idtipe);
                $this->db->set('idbarang', $idbarang);
                $this->db->set('kuantitas', $kuantitas);
                $this->db->set('kuantitas_kecil', $kuantitas_kecil);
                $this->db->set('harga', $hargabeli);
                $this->db->set('opsisatuan', $opsisatuan);
                $this->db->set('status', 1);
                if ($this->db->insert('tgudang_pemesanan_detail')) {
					$data_his['iddet_gudang']=$this->db->insert_id();
                    $status = true;
                }
            }
        }
		$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
		
        $this->db->set('stpemesanan', 1);
        $this->db->where('id', $iddet_permintaan);
        // $this->db->where('idtipe', $idtipe);
        // $this->db->where('idbarang', $idbarang);
        if ($this->db->update('tunit_permintaan_detail')) {
            $status = true;
        } else {
            $status = false;
        }
        return $status;
    }
	public function get_id_gudang($idpemesanan,$idtipe,$idbarang){
		$q="SELECT D.id from tgudang_pemesanan_detail D 
			Where D.idpemesanan='$idpemesanan' AND D.idtipe='$idtipe' AND D.idbarang='$idbarang'";
		$query=$this->db->query($q);
		return $query->row('id');
	}
    public function get_namabarang($idtipe, $idbarang) {
        if ($idtipe && $idbarang) {
            $tipe = array(null, 'Alkes','Implan','Obat','Logistik');
            $table = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
            $this->db->select('nama, concat("'.$tipe[$idtipe].'") as tipe');
            $this->db->where('id', $idbarang);
            $res = $this->db->get($table[$idtipe], 1)->row();
            return $res;
        }
    }

    public function konversi_satuan() {
        $idbarang = $this->input->post('idbarang');
        $idtipe = $this->input->post('idtipe');
        $xkuantitas = $this->input->post('xkuantitas');
        $opsisatuan = $this->input->post('opsisatuan');

        $jumlahpesan = 0;

        if ($opsisatuan == 2) {
            // konversi jumlah satuan
            $table = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
            $this->db->select('jumlahsatuanbesar');
            $this->db->where('id', $idbarang);
            $this->db->from($table[$idtipe]);
            $jumlahsatuanbesar = $this->db->get()->row()->jumlahsatuanbesar;
            $jumlahsatuankecil = $xkuantitas;
            $var1 = $jumlahsatuankecil/$jumlahsatuanbesar;
            if ($var1 < 1) {
                $jumlahpesan = 1;
            } else {
                $jumlahpesan = ceil($var1);
            }
        } else {
            $jumlahpesan = $xkuantitas;
        }
        $this->output->set_output(json_encode($jumlahpesan));
    }

    public function getDefaultUnitPelayananUser() {
        $this->db->select('unitpelayananiddefault as idunitpelayanan');
        $this->db->from('musers');
        $this->db->where('id', $this->session->userdata('user_id'));
        $query = $this->db->get();
        return $query->row_array()['idunitpelayanan'];
    }
	public function list_unit_pelayanan_user() {
        $this->db->select('munitpelayanan.id,munitpelayanan.nama as text');
        $this->db->from('munitpelayanan_user');
        $this->db->join('munitpelayanan','munitpelayanan.id=munitpelayanan_user.idunitpelayanan');
        $this->db->where('munitpelayanan_user.userid', $this->session->userdata('user_id'));
        $query = $this->db->get();
        return $query->result();
    }
	public function list_tipe_barang($idunit='') {
		$q="SELECT H.idtipe as id,T.nama_tipe as text from munitpelayanan_tipebarang H
			LEFT JOIN mdata_tipebarang T ON T.id=H.idtipe
			WHERE H.idunitpelayanan='$idunit'";        
        return $this->db->query($q)->result();
    }
	public function list_unit_ke($idunit='') {
		$q="SELECT M.id,M.nama as text from munitpelayanan_permintaan H
			INNER JOIN munitpelayanan M ON M.id=H.idunitpenerima
			WHERE H.idunitpeminta='$idunit'";        
        return $this->db->query($q)->result();
    }
	public function list_unit_ke_json($idunit='') {
		$q="SELECT M.id,M.nama as text from munitpelayanan_permintaan H
			INNER JOIN munitpelayanan M ON M.id=H.idunitpenerima
			WHERE H.idunitpeminta='$idunit'";        
        $res= $this->db->query($q)->result();
		$this->output->set_output(json_encode($res));
    }

    public function select_tipebarang() {
        $id = $this->uri->segment(3);
        $this->db->select("
            idtipe as id,
            CASE WHEN idtipe = 1 THEN 'Alkes' 
            WHEN idtipe = 2 THEN 'Implant'
            WHEN idtipe = 3 THEN 'Obat'
            ELSE 'Logistik' END AS text            
        ");
        $this->db->where('idunitpelayanan', $id);
        $res = $this->db->get('munitpelayanan_tipebarang')->result();
        $this->output->set_output(json_encode($res));
    }
    public function delete($id) {
		
        // $this->db->where("id", $id);
        // $this->db->delete("tunit_permintaan");

        // $this->db->where("idpermintaan", $id);
        // $this->db->delete("tunit_permintaan_detail");
		
		$data = array(
               'status' =>0,
               'st_hapus' =>1,
               'iduser_deleted' =>$this->session->userdata('user_id'),
               'datetime_deleted' =>date('Y-d-m H:i:s')
            );

		$this->db->where('id', $id);
		$this->db->update('tunit_permintaan', $data); 

    }
	public function list_unit_alihan($dari,$ke){
		$q="SELECT munitpelayanan.id,munitpelayanan.nama from munitpelayanan_permintaan 
			LEFT JOIN munitpelayanan ON munitpelayanan_permintaan.idunitpenerima=munitpelayanan.id
			WHERE munitpelayanan_permintaan.idunitpeminta='$dari' AND munitpelayanan.id != '$ke'";
		$query=$this->db->query($q);
		return $query->result();
	}
	public function tolak($id) {
		
        // $this->db->where("id", $id);
        // $this->db->delete("tunit_permintaan");

        // $this->db->where("idpermintaan", $id);
        // $this->db->delete("tunit_permintaan_detail");
		
		$data = array(
               'status' =>0,
               'st_hapus' =>2,
               'iduser_deleted' =>$this->session->userdata('user_id'),
               'datetime_deleted' =>date('Y-d-m H:i:s')
            );

		$this->db->where('id', $id);
		$this->db->update('tunit_permintaan', $data); 

    }
	public function selesaikan($id) {
		
        // $this->db->where("id", $id);
        // $this->db->delete("tunit_permintaan");

        // $this->db->where("idpermintaan", $id);
        // $this->db->delete("tunit_permintaan_detail");
		
		$data = array(
               'status' =>4
            );

		$this->db->where('id', $id);
		$this->db->update('tunit_permintaan', $data); 

    }
	public function get_user() {
		$user=$this->session->userdata('user_id');
		$q="SELECT id, name as text, TRUE as selected from musers WHERE id='$user'";
		// print_r($q);exit();
		$query=$this->db->query($q);
		return $query->result();		
	}
}

/* End of file Tunitpermintaan_model.php */
/* Location: ./application/models/Tunitpermintaan_model.php */
