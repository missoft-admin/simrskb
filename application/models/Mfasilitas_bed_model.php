<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mfasilitas_bed_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	public function getSpecified($id)
    {
        $q="SELECT 
			*
			from mfasilitas_bed M
					
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	function list_bed($id){
		$q="SELECT H.id,H.nama,CASE WHEN D.mbed_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM mbed H
			LEFT JOIN mfasilitas_bed_detail D ON D.mbed_id=H.id AND H.id AND D.mfasilitas_id='$id'
			WHERE H.`status`='1' 
			AND H.id NOT IN (SELECT mfasilitas_bed_detail.mbed_id FROM mfasilitas_bed_detail WHERE mfasilitas_bed_detail.mfasilitas_id != '$id')";
	return $this->db->query($q)->result();
	}
    public function saveData()
    {
		// print_r($this->input->post());exit;
        $this->nama 	= $_POST['nama'];		
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mfasilitas_bed', $this)) {
			$mfasilitas_id=$this->db->insert_id();
			$mbed_id = $this->input->post('mbed_id');
			if ($mbed_id){
				foreach($mbed_id as $index=>$val){
					$data_detail=array(
						'mfasilitas_id' => $mfasilitas_id,	
						'mbed_id' => $val,
					);
					$this->db->insert('mfasilitas_bed_detail',$data_detail);
				}
			}
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
		$mfasilitas_id=$_POST['id'];
        $this->nama 	= $_POST['nama'];	
		
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mfasilitas_bed', $this, array('id' => $_POST['id']))) {
			$mbed_id = $this->input->post('mbed_id');
			$this->db->where('mfasilitas_id',$mfasilitas_id);
			$this->db->delete('mfasilitas_bed_detail');
			if ($mbed_id){
				foreach($mbed_id as $index=>$val){
					$data_detail=array(
						'mfasilitas_id' => $mfasilitas_id,	
						'mbed_id' => $val,
					);
					$this->db->insert('mfasilitas_bed_detail',$data_detail);
				}
			}
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	function refresh_image($id=''){
		$q="SELECT  H.* from mfasilitas_bed_foto H

			WHERE H.mfasilitas_id='$id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/foto_fasilitas/'.$r->filename.'" target="_blank">'.substr($r->filename, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">'.$r->upload_by_nama.'-'.HumanDateLong($r->upload_date).'</td>';
			$tabel .='<td class="text-left"><button class="btn btn-xs btn-danger hapus_file" type="button" title="Hapus"><li class="fa fa-trash-o"></li></button></td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
		}
		return $tabel;
	}
	function get_file_name($id){
		$q="SELECT *FROM mfasilitas_bed_foto WHERE id='$id'";
		return $this->db->query($q)->row();
	}
    public function softDelete($id)
    {
        $this->staktif = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mfasilitas_bed', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mfasilitas_bed', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
