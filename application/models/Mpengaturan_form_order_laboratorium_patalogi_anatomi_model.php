<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpengaturan_form_order_laboratorium_patalogi_anatomi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('merm_pengaturan_form_order_laboratorium_patologi_anatomi');
        return $query->row();
    }

    public function saveData()
    {
        $this->pesan_informasi_berhasil = $_POST['pesan_informasi_berhasil'];
        
        $this->label_judul = $_POST['label_judul'];
        $this->label_judul_eng = $_POST['label_judul_eng'];
        
        $this->label_nomor_spesimen = $_POST['label_nomor_spesimen'];
        $this->label_nomor_spesimen_eng = $_POST['label_nomor_spesimen_eng'];
        if (isset($_POST['required_nomor_spesimen'])) {
            $this->required_nomor_spesimen = $_POST['required_nomor_spesimen'];
        }
        
        $this->label_sumber_spesimen_klinis = $_POST['label_sumber_spesimen_klinis'];
        $this->label_sumber_spesimen_klinis_eng = $_POST['label_sumber_spesimen_klinis_eng'];
        if (isset($_POST['required_sumber_spesimen_klinis'])) {
            $this->required_sumber_spesimen_klinis = $_POST['required_sumber_spesimen_klinis'];
        }
        
        $this->label_lokasi_pengambilan_spesimen_klinis = $_POST['label_lokasi_pengambilan_spesimen_klinis'];
        $this->label_lokasi_pengambilan_spesimen_klinis_eng = $_POST['label_lokasi_pengambilan_spesimen_klinis_eng'];
        if (isset($_POST['required_lokasi_pengambilan_spesimen_klinis'])) {
            $this->required_lokasi_pengambilan_spesimen_klinis = $_POST['required_lokasi_pengambilan_spesimen_klinis'];
        }
        
        $this->label_jumlah_spesimen = $_POST['label_jumlah_spesimen'];
        $this->label_jumlah_spesimen_eng = $_POST['label_jumlah_spesimen_eng'];
        if (isset($_POST['required_jumlah_spesimen'])) {
            $this->required_jumlah_spesimen = $_POST['required_jumlah_spesimen'];
        }
        
        $this->label_volume_spesimen = $_POST['label_volume_spesimen'];
        $this->label_volume_spesimen_eng = $_POST['label_volume_spesimen_eng'];
        if (isset($_POST['required_volume_spesimen'])) {
            $this->required_volume_spesimen = $_POST['required_volume_spesimen'];
        }
        
        $this->label_cara_pengambilan_spesimen = $_POST['label_cara_pengambilan_spesimen'];
        $this->label_cara_pengambilan_spesimen_eng = $_POST['label_cara_pengambilan_spesimen_eng'];
        if (isset($_POST['required_cara_pengambilan_spesimen'])) {
            $this->required_cara_pengambilan_spesimen = $_POST['required_cara_pengambilan_spesimen'];
        }
        
        $this->label_waktu_pengambilan = $_POST['label_waktu_pengambilan'];
        $this->label_waktu_pengambilan_eng = $_POST['label_waktu_pengambilan_eng'];
        if (isset($_POST['required_waktu_pengambilan'])) {
            $this->required_waktu_pengambilan = $_POST['required_waktu_pengambilan'];
        }
        
        $this->label_kondisi_spesimen = $_POST['label_kondisi_spesimen'];
        $this->label_kondisi_spesimen_eng = $_POST['label_kondisi_spesimen_eng'];
        if (isset($_POST['required_kondisi_spesimen'])) {
            $this->required_kondisi_spesimen = $_POST['required_kondisi_spesimen'];
        }
        
        $this->label_waktu_fiksasi_spesimen_klinis = $_POST['label_waktu_fiksasi_spesimen_klinis'];
        $this->label_waktu_fiksasi_spesimen_klinis_eng = $_POST['label_waktu_fiksasi_spesimen_klinis_eng'];
        if (isset($_POST['required_waktu_fiksasi_spesimen_klinis'])) {
            $this->required_waktu_fiksasi_spesimen_klinis = $_POST['required_waktu_fiksasi_spesimen_klinis'];
        }
        
        $this->label_cairan_fiksasi = $_POST['label_cairan_fiksasi'];
        $this->label_cairan_fiksasi_eng = $_POST['label_cairan_fiksasi_eng'];
        if (isset($_POST['required_cairan_fiksasi'])) {
            $this->required_cairan_fiksasi = $_POST['required_cairan_fiksasi'];
        }
        
        $this->label_volume_cairan_fiksasi = $_POST['label_volume_cairan_fiksasi'];
        $this->label_volume_cairan_fiksasi_eng = $_POST['label_volume_cairan_fiksasi_eng'];
        if (isset($_POST['required_volume_cairan_fiksasi'])) {
            $this->required_volume_cairan_fiksasi = $_POST['required_volume_cairan_fiksasi'];
        }
        
        $this->label_nama_pengambil_spesimen = $_POST['label_nama_pengambil_spesimen'];
        $this->label_nama_pengambil_spesimen_eng = $_POST['label_nama_pengambil_spesimen_eng'];
        if (isset($_POST['required_nama_pengambil_spesimen'])) {
            $this->required_nama_pengambil_spesimen = $_POST['required_nama_pengambil_spesimen'];
        }
        
        $this->label_nama_pengantar_spesimen = $_POST['label_nama_pengantar_spesimen'];
        $this->label_nama_pengantar_spesimen_eng = $_POST['label_nama_pengantar_spesimen_eng'];
        if (isset($_POST['required_nama_pengantar_spesimen'])) {
            $this->required_nama_pengantar_spesimen = $_POST['required_nama_pengantar_spesimen'];
        }
        
        $this->label_catatan = $_POST['label_catatan'];
        $this->label_catatan_eng = $_POST['label_catatan_eng'];
        if (isset($_POST['required_catatan'])) {
            $this->required_catatan = $_POST['required_catatan'];
        }


        $this->db->where('id', 1);
        if ($this->db->update('merm_pengaturan_form_order_laboratorium_patologi_anatomi', $this)) {
            return true;
        }

        return false;
    }
}
