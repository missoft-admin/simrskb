<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tbilling_ranap_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function user_form($idpendaftaran)
	{
		$q="SELECT M1.nama as user_validasi
			,M2.nama as user_bayar
			,M3.nama as user_kwitansi
			,M4.nama as user_car_pass 
			FROM trawatinap_pendaftaran H
			LEFT JOIN mppa M1 ON M1.user_id=H.user_id_statusvalidasi
			LEFT JOIN mppa M2 ON M2.user_id=H.user_id_statusbayar
			LEFT JOIN mppa M3 ON M3.user_id=H.user_id_proses_kwitansi
			LEFT JOIN mppa M4 ON M4.user_id=H.user_id_proses_card_pass
			WHERE H.id='$idpendaftaran'";
		// print_r($q);exit;
		return $this->db->query($q)->row_array();
	}
	public function getTotalDeposit($idpendaftaran)
	{
		$q="SELECT COALESCE(SUM(H.nominal),0) as totaldeposit FROM `trawatinap_deposit` H
			WHERE H.idrawatinap='$idpendaftaran' AND H.`status` != '0' AND H.st_proses='1'";
		// print_r($q);exit;
		return $this->db->query($q)->row();
	}

	function get_logic_card_pass($kelompok_id,$idrekanan){
		$status_card='1';
		$tipe_rekanan='0';
		if ($kelompok_id=='1'){
			$q="SELECT H.tipe_rekanan FROM mrekanan H  WHERE H.id='$idrekanan'";
			$tipe_rekanan=$this->db->query($q)->row('tipe_rekanan');
			
			$q="
				SELECT *FROM (
					SELECT 1 as nourut, H.id,H.status_card FROM `mlogic_trx_ranap_card` H
					WHERE H.`status`='1' AND H.kelompok_id='$$kelompok_id' AND H.tipe_rekanan='$tipe_rekanan' AND H.idrekanan='$idrekanan'

					UNION ALL

					SELECT 2 as nourut, H.id,H.status_card FROM `mlogic_trx_ranap_card` H
					WHERE H.`status`='1' AND H.kelompok_id='$$kelompok_id' AND H.tipe_rekanan='$tipe_rekanan' AND H.idrekanan='0' 

					UNION ALL

					SELECT 3 as nourut, H.id,H.status_card FROM `mlogic_trx_ranap_card` H
					WHERE H.`status`='1' AND H.kelompok_id='$kelompok_id' AND H.tipe_rekanan='0' AND H.idrekanan='0' 
					) T ORDER BY T.nourut ASC LIMIT 1
			";
			$status_card=$this->db->query($q)->row('status_card');
		}else{
			$q="SELECT H.status_card FROM `mlogic_trx_ranap_card` H
				WHERE H.`status`='1' AND H.kelompok_id='$kelompok_id' LIMIT 1";
			$status_card=$this->db->query($q)->row('status_card');
		}
		return $status_card;
	}
	public function detail_pembayaran($idtindakan)
	{
		// $this->db->select('trawatinap_tindakan_pembayaran_detail.*');
		// $this->db->join('trawatinap_tindakan_pembayaran', 'trawatinap_tindakan_pembayaran.id = trawatinap_tindakan_pembayaran_detail.idtindakan');
		// $this->db->where('trawatinap_tindakan_pembayaran_detail.idtindakan', $idtindakan);
		// // $this->db->where('trawatinap_tindakan_pembayaran.statusbatal', 0);
		// $query = $this->db->get('trawatinap_tindakan_pembayaran_detail');
		// // print_r($this->db->last_query());exit;
		// return $query->result();
		$q="SELECT
				COUNT(D.id) as jml_file,`trawatinap_tindakan_pembayaran_detail`.* 
			FROM
				`trawatinap_tindakan_pembayaran_detail`
				INNER JOIN `trawatinap_tindakan_pembayaran` ON `trawatinap_tindakan_pembayaran`.`id` = `trawatinap_tindakan_pembayaran_detail`.`idtindakan` AND trawatinap_tindakan_pembayaran.statusbatal='0'
				LEFT JOIN trawatinap_tindakan_pembayaran_detail_upload D ON D.transaksi_id=trawatinap_tindakan_pembayaran_detail.id
			WHERE
				`trawatinap_tindakan_pembayaran`.`idtindakan` = '$idtindakan'
				
			GROUP BY trawatinap_tindakan_pembayaran_detail.id";
			// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	public function get_data_pembayaran($idtindakan)
	{
		
		$q="SELECT IFNULL(SUM(D.nominal),0) as nominal_sudah_dibayar 
			FROM `trawatinap_tindakan_pembayaran` H
			LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON H.id=D.idtindakan
			
			WHERE H.statusbatal='0' AND H.idtindakan='$idtindakan'";
		
		$nominal_sudah_dibayar= $this->db->query($q)->row('nominal_sudah_dibayar');
		return $nominal_sudah_dibayar;
	}

	public function simpan_transaksi()
	{
		$this->db->where('id', $this->input->post('idpendaftaran'));
		$this->db->set('statuspembayaran', 1);
		$status_proses_kwitansi = ($this->input->post('status_proses_kwitansi'));
		if ($status_proses_kwitansi=='1'){
			$this->db->set('status_proses_kwitansi', 1);
			$this->db->set('user_id_proses_kwitansi', $this->session->userdata('user_id'));
			$this->db->set('tanggal_proses_kwitansi', date('Y-m-d H:i:s'));
		}

		$subTotal = RemoveComma($this->input->post('subtotal'));
		$diskonRp = RemoveComma($this->input->post('diskon_rp'));
		$diskonPersen = RemoveComma($this->input->post('diskon_persen'));
		$totalTransaksi = RemoveComma($this->input->post('total'));
		$nominalDeposit = RemoveComma($this->input->post('deposit'));
		$totalHarusDibayar = RemoveComma($this->input->post('totalharusdibayar'));
		$nominalPembayaran = RemoveComma($this->input->post('pembayaran'));
		$sisaPembayaran = RemoveComma($this->input->post('sisa'));

		if ($this->db->update('trawatinap_pendaftaran')) {
			$dataPembayaran = [
				'tanggal' => date('Y-m-d H:i:s'),
				'idtindakan' => $this->input->post('idtindakan'),
				'subtotal' => $subTotal,
				'diskonrp' => $diskonRp,
				'diskonpersen' => $diskonPersen,
				'total' => $totalTransaksi,
				'deposit' => $nominalDeposit,
				'totalharusdibayar' => $totalHarusDibayar,
				'pembayaran' => $nominalPembayaran,
				'sisa' => $sisaPembayaran,
				'nominal_round' => ($totalTransaksi + $diskonRp) - $subTotal,
				'iduser_input' => $this->session->userdata('user_id')
			];

			// Check History Detail Pembayaran
			$this->db->where('idtindakan', $this->input->post('idtindakan'));
			$this->db->where('statusbatal', 0);
			$this->db->order_by('id', 'DESC');
			$this->db->limit(1);
			$pembayaran = $this->db->get('trawatinap_tindakan_pembayaran');
			if ($pembayaran->num_rows() > 0) {
				$idtindakan = $pembayaran->row()->id;
				$this->db->where('idtindakan', $idtindakan);
				$this->db->delete('trawatinap_tindakan_pembayaran_detail');
			} else {
				$idtindakan = 0;
			}

			if ($this->db->insert('trawatinap_tindakan_pembayaran', $dataPembayaran)) {
				if ($idtindakan == 0) {
					$idtindakan = $this->db->insert_id();
				}

				$detailPembayaran = json_decode($this->input->post('pos_tabel_pembayaran'));
				foreach ($detailPembayaran as $row) {
					$dataPembayaranDetail = [
						'idtindakan' => $idtindakan,
						'idmetode' => $row[0],
						'nominal' => RemoveComma($row[4]),
						'idpegawai' => $row[8],
						'idbank' => $row[9],
						'ket_cc' => $row[10],
						'idkontraktor' => $row[11],
						'jaminan' => $row[14],
						'tracenumber' => $row[15],
						'tipekontraktor' => $row[16],
						'keterangan' => $row[3],
					];

					$this->db->insert('trawatinap_tindakan_pembayaran_detail', $dataPembayaranDetail);
				}

				$data['status'] = 200;
			} else {
				$data['status'] = 500;
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	function list_poli($idtipe=''){
		$q="SELECT *FROM mpoliklinik H WHERE H.`status`='1' AND H.idtipe='$idtipe'";
		return $this->db->query($q)->result();
	}
	function list_dokter(){
		$q="SELECT MP.id,MP.nama FROM `mjadwal_dokter` H
			LEFT JOIN mdokter MP ON MP.id=H.iddokter
			WHERE MP.`status`='1'
			GROUP BY H.iddokter
";
		return $this->db->query($q)->result();
	}
	public function getHistoryBatal($idpendaftaran)
	{
		$this->db->select('
        malasan_batal.keterangan AS alasanbatal_label,
        musers.name AS namauser,musers2.name AS namauserinput,trawatinap_tindakan_pembayaran.*');
		$this->db->join('musers', 'musers.id = trawatinap_tindakan_pembayaran.iduser_batal');
		$this->db->join('musers as musers2', 'musers2.id = trawatinap_tindakan_pembayaran.iduser_input');
		$this->db->join('malasan_batal', 'malasan_batal.id = trawatinap_tindakan_pembayaran.alasanbatal', 'LEFT');
		$this->db->where('idtindakan', $idpendaftaran);
		$this->db->where('statusbatal', '1');
		$query = $this->db->get('trawatinap_tindakan_pembayaran');
		// print_r($this->db->last_query());exit;
		return $query->result();
	}
	public function getDetailKwitansi($id)
	{
		$this->db->select('trawatinap_kwitansi.*,
        musers.name AS namauser');
		$this->db->join('musers', 'musers.id = trawatinap_kwitansi.iduser_input','LEFT');
		$this->db->where('trawatinap_kwitansi.id', $id);
		$query = $this->db->get('trawatinap_kwitansi');
		// print_r($this->db->last_query());exit;
		return $query->row_array();
	}
	public function getDetailPasien($id)
	{
		$this->db->select('trawatinap_pendaftaran.*,
        musers.name AS namauser');
		$this->db->join('musers', 'musers.id = trawatinap_pendaftaran.user_id_proses_card_pass');
		$this->db->where('trawatinap_pendaftaran.id', $id);
		$query = $this->db->get('trawatinap_pendaftaran');
		return $query->row_array();
	}
	public function simpan_proses_peretujuan($id) {

		$this->db->where('kwitansi_id',$id);
		$this->db->delete('trawatinap_kwitansi_approval');
		$q="SELECT S.step,U.`name` as user_nama,S.proses_setuju,S.proses_tolak,S.deskrpisi,S.iduser 
				FROM trawatinap_kwitansi H
				INNER JOIN mlogic_trx_ranap S ON S.jenis=H.tipe_proses
				INNER JOIN musers U ON U.id=S.iduser
				WHERE H.id='$id' AND S.status='1'
				ORDER BY S.step,S.id";
		$list_user=$this->db->query($q)->result();
		$step=999;
		$idlogic='';
		foreach ($list_user as $row){
			if ($row->step < $step){
				$step=$row->step;
			}
			$data=array(
				'kwitansi_id' => $id,
				'step' => $row->step,
				// 'idlogic' => $row->idlogic,
				'iduser' => $row->iduser,
				'user_nama' => $row->user_nama,
				'proses_setuju' => $row->proses_setuju,
				'proses_tolak' => $row->proses_tolak,
				'deskrpisi' => $row->deskrpisi,
				'approve' => 0,
			);
			// $idlogic=$row->idlogic;
			$this->db->insert('trawatinap_kwitansi_approval', $data);
		}

		$this->db->update('trawatinap_kwitansi_approval',array('st_aktif'=>1),array('kwitansi_id'=>$id,'step'=>$step));
		$alasanhapus=$this->input->post('alasanhapus');
		$data=array(
			'hapus_proses' => '1',
			'alasanhapus' => $alasanhapus,
			'iduserdelete' => $this->session->userdata('user_id'),
			'deleted_date' => date('Y-m-d H:i:s'),
			
		);
		$this->db->where('id',$id);
		return $this->db->update('trawatinap_kwitansi', $data);


    }
	
}
