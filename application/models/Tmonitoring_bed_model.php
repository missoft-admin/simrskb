<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tmonitoring_bed_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function list_ruang(){
		$q="SELECT *FROM mruangan H
			WHERE H.`status`='1' AND H.idtipe='1'
			ORDER BY H.id ASC";
		return $this->db->query($q)->result();
	}
	function list_kelas(){
		$q="SELECT *FROM mkelas H
			WHERE H.`status`='1'
			ORDER BY H.id ASC";
		return $this->db->query($q)->result();
	}
}
