<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ttindakan_ranap_jasa_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function get_kota($kode,$id="")
	{
		// $kode = $this->input->post('kodeprovinsi');
		$data = get_all('mfwilayah', ['parent_id' => $kode]);
		$opsi="<option value='' ".($id==""?'selected':'').">Pilih Opsi</option>";
		foreach($data as $r){
			$opsi .="<option value='".$r->id."' ".($r->id==$id?'selected':'').">".$r->nama."</option>";
		}
		
		return $opsi;
	}
	function setting_lm(){
		$q="SELECT 
			logo_form as logo_form_lm,alamat_form as alamat_form_lm,telepone_form as telepone_form_lm,email_form as email_form_lm,judul_ina as judul_ina_lm,judul_eng as judul_eng_lm,header_ina as header_ina_lm,header_eng as header_eng_lm,no_register_ina as no_register_ina_lm,no_register_eng as no_register_eng_lm,no_rekam_medis_ina as no_rekam_medis_ina_lm,no_rekam_medis_eng as no_rekam_medis_eng_lm,nama_pasien_ina as nama_pasien_ina_lm,nama_pasien_eng as nama_pasien_eng_lm,umur_ina as umur_ina_lm,umur_eng as umur_eng_lm,ttl_ina as ttl_ina_lm,ttl_eng as ttl_eng_lm,jk_ina as jk_ina_lm,jk_eng as jk_eng_lm,pekerjaan_ina as pekerjaan_ina_lm,pekerjaan_eng as pekerjaan_eng_lm,pendidikan_ina as pendidikan_ina_lm,pendidikan_eng as pendidikan_eng_lm,agama_ina as agama_ina_lm,agama_eng as agama_eng_lm,alamat_ina as alamat_ina_lm,alamat_eng as alamat_eng_lm,provinsi_ina as provinsi_ina_lm,provinsi_eng as provinsi_eng_lm,kab_ina as kab_ina_lm,kab_eng as kab_eng_lm,kec_ina as kec_ina_lm,kec_eng as kec_eng_lm,kel_ina as kel_ina_lm,kel_eng as kel_eng_lm,kode_pos_ina as kode_pos_ina_lm,kode_pos_eng as kode_pos_eng_lm,rt_ina as rt_ina_lm,rt_eng as rt_eng_lm,rw_ina as rw_ina_lm,rw_eng as rw_eng_lm,wn_ina as wn_ina_lm,wn_eng as wn_eng_lm,telephone_ina as telephone_ina_lm,telephone_eng as telephone_eng_lm,nohp_ina as nohp_ina_lm,nohp_eng as nohp_eng_lm,nik_ina as nik_ina_lm,nik_eng as nik_eng_lm,label_header_pj_ina as label_header_pj_ina_lm,label_header_pj_eng as label_header_pj_eng_lm,nama_pj_ina as nama_pj_ina_lm,nama_pj_eng as nama_pj_eng_lm,ttl_pj_ina as ttl_pj_ina_lm,ttl_pj_eng as ttl_pj_eng_lm,umur_pj_ina as umur_pj_ina_lm,umur_pj_eng as umur_pj_eng_lm,jk_pj_ina as jk_pj_ina_lm,jk_pj_eng as jk_pj_eng_lm,pekerjaan_pj_ina as pekerjaan_pj_ina_lm,pekerjaan_pj_eng as pekerjaan_pj_eng_lm,pendidikan_pj_ina as pendidikan_pj_ina_lm,pendidikan_pj_eng as pendidikan_pj_eng_lm,agama_pj_ina as agama_pj_ina_lm,agama_pj_eng as agama_pj_eng_lm,alamat_pj_ina as alamat_pj_ina_lm,alamat_pj_eng as alamat_pj_eng_lm,provinsi_pj_ina as provinsi_pj_ina_lm,provinsi_pj_eng as provinsi_pj_eng_lm,kab_pj_ina as kab_pj_ina_lm,kab_pj_eng as kab_pj_eng_lm,kec_pj_ina as kec_pj_ina_lm,kec_pj_eng as kec_pj_eng_lm,kel_pj_ina as kel_pj_ina_lm,kel_pj_eng as kel_pj_eng_lm,kode_pos_pj_ina as kode_pos_pj_ina_lm,kode_pos_pj_eng as kode_pos_pj_eng_lm,rt_pj_ina as rt_pj_ina_lm,rt_pj_eng as rt_pj_eng_lm,rw_pj_ina as rw_pj_ina_lm,rw_pj_eng as rw_pj_eng_lm,wn_pj_ina as wn_pj_ina_lm,wn_pj_eng as wn_pj_eng_lm,telephone_pj_ina as telephone_pj_ina_lm,telephone_pj_eng as telephone_pj_eng_lm,nohp_pj_ina as nohp_pj_ina_lm,nohp_pj_eng as nohp_pj_eng_lm,nik_pj_ina as nik_pj_ina_lm,nik_pj_eng as nik_pj_eng_lm,hubungan_pj_ina as hubungan_pj_ina_lm,hubungan_pj_eng as hubungan_pj_eng_lm,detail_pendaftaran_ina as detail_pendaftaran_ina_lm,detail_pendaftaran_eng as detail_pendaftaran_eng_lm,tipe_pasien_ina as tipe_pasien_ina_lm,tipe_pasien_eng as tipe_pasien_eng_lm,asal_pasien_ina as asal_pasien_ina_lm,asal_pasien_eng as asal_pasien_eng_lm,poliklinik_ina as poliklinik_ina_lm,poliklinik_eng as poliklinik_eng_lm,tgl_pendaftaran_ina as tgl_pendaftaran_ina_lm,tgl_pendaftaran_eng as tgl_pendaftaran_eng_lm,kasus_ina as kasus_ina_lm,kasus_eng as kasus_eng_lm,dirawat_ke_ina as dirawat_ke_ina_lm,dirawat_ke_eng as dirawat_ke_eng_lm,cara_masuk_ina as cara_masuk_ina_lm,cara_masuk_eng as cara_masuk_eng_lm,kel_pasien_ina as kel_pasien_ina_lm,kel_pasien_eng as kel_pasien_eng_lm,dpjp_utama_ina as dpjp_utama_ina_lm,dpjp_utama_eng as dpjp_utama_eng_lm,ruangan_ina as ruangan_ina_lm,ruangan_eng as ruangan_eng_lm,kelas_ina as kelas_ina_lm,kelas_eng as kelas_eng_lm,bed_ina as bed_ina_lm,bed_eng as bed_eng_lm,pernyataan_persetujuan_ina as pernyataan_persetujuan_ina_lm,pernyataan_persetujuan_eng as pernyataan_persetujuan_eng_lm,identifikasi_privasi_ina as identifikasi_privasi_ina_lm,identifikasi_privasi_eng as identifikasi_privasi_eng_lm,khusus_asuransi_ina as khusus_asuransi_ina_lm,khusus_asuransi_eng as khusus_asuransi_eng_lm,lain_lain_ina as lain_lain_ina_lm,lain_lain_eng as lain_lain_eng_lm,ttd_pendaftaran_ina as ttd_pendaftaran_ina_lm,ttd_pendaftaran_eng as ttd_pendaftaran_eng_lm,ttd_pasien_ina as ttd_pasien_ina_lm,ttd_pasien_eng as ttd_pasien_eng_lm
			FROM setting_ranap_lembar_masuk
		";
		return $this->db->query($q)->row_array();
	}
	function setting_sp(){
		$q="SELECT 
			logo_form as logo_form_sp,alamat_form as alamat_form_sp,telepone_form as telepone_form_sp,email_form as email_form_sp,judul_ina as judul_ina_sp,judul_eng as judul_eng_sp,paragraf_1_ina as paragraf_1_ina_sp,paragraf_1_eng as paragraf_1_eng_sp,header_ina as header_ina_sp,header_eng as header_eng_sp,no_register_ina as no_register_ina_sp,no_register_eng as no_register_eng_sp,no_rekam_medis_ina as no_rekam_medis_ina_sp,no_rekam_medis_eng as no_rekam_medis_eng_sp,nama_pasien_ina as nama_pasien_ina_sp,nama_pasien_eng as nama_pasien_eng_sp,umur_ina as umur_ina_sp,umur_eng as umur_eng_sp,ttl_ina as ttl_ina_sp,ttl_eng as ttl_eng_sp,jk_ina as jk_ina_sp,jk_eng as jk_eng_sp,pekerjaan_ina as pekerjaan_ina_sp,pekerjaan_eng as pekerjaan_eng_sp,pendidikan_ina as pendidikan_ina_sp,pendidikan_eng as pendidikan_eng_sp,agama_ina as agama_ina_sp,agama_eng as agama_eng_sp,alamat_ina as alamat_ina_sp,alamat_eng as alamat_eng_sp,provinsi_ina as provinsi_ina_sp,provinsi_eng as provinsi_eng_sp,kab_ina as kab_ina_sp,kab_eng as kab_eng_sp,kec_ina as kec_ina_sp,kec_eng as kec_eng_sp,kel_ina as kel_ina_sp,kel_eng as kel_eng_sp,kode_pos_ina as kode_pos_ina_sp,kode_pos_eng as kode_pos_eng_sp,rt_ina as rt_ina_sp,rt_eng as rt_eng_sp,rw_ina as rw_ina_sp,rw_eng as rw_eng_sp,wn_ina as wn_ina_sp,wn_eng as wn_eng_sp,telephone_ina as telephone_ina_sp,telephone_eng as telephone_eng_sp,nohp_ina as nohp_ina_sp,nohp_eng as nohp_eng_sp,nik_ina as nik_ina_sp,nik_eng as nik_eng_sp,paragraf_2_ina as paragraf_2_ina_sp,paragraf_2_eng as paragraf_2_eng_sp,label_header_pj_ina as label_header_pj_ina_sp,label_header_pj_eng as label_header_pj_eng_sp,nama_pj_ina as nama_pj_ina_sp,nama_pj_eng as nama_pj_eng_sp,ttl_pj_ina as ttl_pj_ina_sp,ttl_pj_eng as ttl_pj_eng_sp,umur_pj_ina as umur_pj_ina_sp,umur_pj_eng as umur_pj_eng_sp,jk_pj_ina as jk_pj_ina_sp,jk_pj_eng as jk_pj_eng_sp,pekerjaan_pj_ina as pekerjaan_pj_ina_sp,pekerjaan_pj_eng as pekerjaan_pj_eng_sp,pendidikan_pj_ina as pendidikan_pj_ina_sp,pendidikan_pj_eng as pendidikan_pj_eng_sp,agama_pj_ina as agama_pj_ina_sp,agama_pj_eng as agama_pj_eng_sp,alamat_pj_ina as alamat_pj_ina_sp,alamat_pj_eng as alamat_pj_eng_sp,provinsi_pj_ina as provinsi_pj_ina_sp,provinsi_pj_eng as provinsi_pj_eng_sp,kab_pj_ina as kab_pj_ina_sp,kab_pj_eng as kab_pj_eng_sp,kec_pj_ina as kec_pj_ina_sp,kec_pj_eng as kec_pj_eng_sp,kel_pj_ina as kel_pj_ina_sp,kel_pj_eng as kel_pj_eng_sp,kode_pos_pj_ina as kode_pos_pj_ina_sp,kode_pos_pj_eng as kode_pos_pj_eng_sp,rt_pj_ina as rt_pj_ina_sp,rt_pj_eng as rt_pj_eng_sp,rw_pj_ina as rw_pj_ina_sp,rw_pj_eng as rw_pj_eng_sp,wn_pj_ina as wn_pj_ina_sp,wn_pj_eng as wn_pj_eng_sp,telephone_pj_ina as telephone_pj_ina_sp,telephone_pj_eng as telephone_pj_eng_sp,nohp_pj_ina as nohp_pj_ina_sp,nohp_pj_eng as nohp_pj_eng_sp,nik_pj_ina as nik_pj_ina_sp,nik_pj_eng as nik_pj_eng_sp,hubungan_pj_ina as hubungan_pj_ina_sp,hubungan_pj_eng as hubungan_pj_eng_sp,detail_pendaftaran_ina as detail_pendaftaran_ina_sp,detail_pendaftaran_eng as detail_pendaftaran_eng_sp,tipe_pasien_ina as tipe_pasien_ina_sp,tipe_pasien_eng as tipe_pasien_eng_sp,kel_pasien_ina as kel_pasien_ina_sp,kel_pasien_eng as kel_pasien_eng_sp,dpjp_utama_ina as dpjp_utama_ina_sp,dpjp_utama_eng as dpjp_utama_eng_sp,ruangan_ina as ruangan_ina_sp,ruangan_eng as ruangan_eng_sp,kelas_ina as kelas_ina_sp,kelas_eng as kelas_eng_sp,bed_ina as bed_ina_sp,bed_eng as bed_eng_sp,paragraf_3_ina as paragraf_3_ina_sp,paragraf_3_eng as paragraf_3_eng_sp,ttd_pasien_ina as ttd_pasien_ina_sp,ttd_pasien_eng as ttd_pasien_eng_sp
			FROM setting_ranap_pernyataan
		";
		return $this->db->query($q)->row_array();
	}
	function setting_gc(){
		$q="SELECT 
			logo as logo_gc,judul as judul_gc,sub_header as sub_header_gc,sub_header_side as sub_header_side_gc,footer_1 as footer_1_gc,footer_2 as footer_2_gc,ttd_1 as ttd_1_gc,ttd_2 as ttd_2_gc,footer_form_1 as footer_form_1_gc,footer_form_2 as footer_form_2_gc
			FROM merm_general_ranap
		";
		return $this->db->query($q)->row_array();
	}
	function setting_hk(){
		$q="SELECT 
			logo as logo_hk,judul as judul_hk,sub_header as sub_header_hk,sub_header_side as sub_header_side_hk,footer_1 as footer_1_hk,footer_2 as footer_2_hk,ttd_1 as ttd_1_hk,ttd_2 as ttd_2_hk,judul_eng as judul_eng_hk,footer_form_2 as footer_form_2_hk,alamat_form as alamat_form_hk,telepone_form as telepone_form_hk,email_form as email_form_hk
			FROM merm_general_ranap_hak
		";
		return $this->db->query($q)->row_array();
	}
	public function getBed($idruangan, $idkelas, $id)
    {
		$query_add="";
		// if ($id !=''){
			$query_add="LEFT JOIN `trawatinap_pendaftaran` ON `trawatinap_pendaftaran`.`idbed` = `mbed`.`id` AND `trawatinap_pendaftaran`.`statuscheckout` = 0 AND `trawatinap_pendaftaran`.`status` = 1 AND `trawatinap_pendaftaran`.`id` != 2818";
		// }
		$q="
		SELECT (CASE WHEN trawatinap_pendaftaran.id IS NULL THEN mbed.id ELSE '0' END) AS id, (CASE
            WHEN trawatinap_pendaftaran.id IS NULL THEN mbed.nama
            WHEN trawatinap_pendaftaran.statuskasir = '1' THEN CONCAT(mbed.nama, ' (NOT READY)')
            WHEN trawatinap_pendaftaran.statuskasir = '0' THEN CONCAT(mbed.nama, ' (TERISI)')
          END) AS nama
			FROM `mbed`
			".$query_add."
			WHERE `mbed`.`status` = 1
			AND `mbed`.`idruangan` = '$idruangan'
			AND (`mbed`.`idkelas` = '$idkelas' OR mbed.idkelas='0')";
        // $this->db->select("(CASE WHEN trawatinap_pendaftaran.id IS NULL THEN mbed.id ELSE '0' END) AS id,
          // (CASE
            // WHEN trawatinap_pendaftaran.id IS NULL THEN mbed.nama
            // WHEN trawatinap_pendaftaran.statuskasir = '1' THEN CONCAT(mbed.nama, ' (NOT READY)')
            // WHEN trawatinap_pendaftaran.statuskasir = '0' THEN CONCAT(mbed.nama, ' (TERISI)')
          // END) AS nama");

        // if ($id != '') {
          // $this->db->join("trawatinap_pendaftaran", "trawatinap_pendaftaran.idbed = mbed.id AND trawatinap_pendaftaran.statuscheckout = 0 AND trawatinap_pendaftaran.status = 1 AND trawatinap_pendaftaran.id != ".$id, "LEFT");
        // } else {
          // $this->db->join("trawatinap_pendaftaran", "trawatinap_pendaftaran.idbed = mbed.id AND trawatinap_pendaftaran.statuscheckout = 0 AND trawatinap_pendaftaran.status = 1", "LEFT");
        // }

        // $this->db->where("mbed.status", 1);
        // $this->db->where("mbed.idruangan", $idruangan);
        // $this->db->where("mbed.idkelas", $idkelas);
		// print_r($this->db->last_query());exit;
        $query = $this->db->query($q);
        return $query->result();
    }
	public function getBedAll($idruangan, $idkelas, $id)
    {
		$where='';
		if ($idkelas!='0' && $idkelas !='#'){
			$where=" AND (`mbed`.`idkelas` = '$idkelas' OR mbed.idkelas='0')";
		}
       $query_add="";
		// if ($id !=''){
			$query_add="LEFT JOIN `trawatinap_pendaftaran` ON `trawatinap_pendaftaran`.`idbed` = `mbed`.`id` AND `trawatinap_pendaftaran`.`statuscheckout` = 0 AND `trawatinap_pendaftaran`.`status` = 1 AND `trawatinap_pendaftaran`.`id` != 2818";
		// }
		$q="
		SELECT mbed.id AS id, (CASE
            WHEN trawatinap_pendaftaran.id IS NULL THEN mbed.nama
            WHEN trawatinap_pendaftaran.statuskasir = '1' THEN CONCAT(mbed.nama, ' (NOT READY)')
            WHEN trawatinap_pendaftaran.statuskasir = '0' THEN CONCAT(mbed.nama, ' (TERISI)')
          END) AS nama
			FROM `mbed`
			".$query_add."
			WHERE `mbed`.`status` = 1
			AND `mbed`.`idruangan` = '$idruangan' ".$where."
			";
        // print_r($q);exit;
        $query = $this->db->query($q);
        return $query->result();
    }
	public function getKecamatan()
	{
		$kode = $this->input->post('kodekab');
		$data = get_all('mfwilayah', ['parent_id' => $kode]);
		$this->output->set_output(json_encode($data));
	}

	public function getKelurahan()
	{
		$kode = $this->input->post('kodekec');
		$data = get_all('mfwilayah', ['parent_id' => $kode]);
		$this->output->set_output(json_encode($data));
	}

	public function getKodepos()
	{
		$kode = $this->input->post('kodekel');
		$data = get_all('mfwilayah', ['id' => $kode]);
		$this->output->set_output(json_encode($data));
	}

	public function saveData()
	{
		$st_terima_ranap='0';
		$st_play='0';
		$sound_play='';
		$st_notif_sound='1';
		$idruangan=$this->input->post('idruangan');
		$idkelas=$this->input->post('idkelas');
		$q="SELECT st_play FROM setting_ranap";
		$st_play=$this->db->query($q)->row('st_play');
		$q="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as sound_play FROM setting_ranap_terima_sound H LEFT JOIN antrian_asset_sound A ON A.id=H.sound_id";
		$sound_play=$this->db->query($q)->row('sound_play');
		$id_tujuan_terima=($this->input->post('idtipe')=='1'?3:4);
		$q="
			SELECT  
				compare_value_4(
				MAX(IF(H.idruangan = '$idruangan' AND idkelas='$idkelas',H.st_terima_ranap,NULL))
				,MAX(IF(H.idruangan = '$idruangan' AND idkelas='0',H.st_terima_ranap,NULL))
				,MAX(IF(H.idruangan = '0' AND idkelas='$idkelas',H.st_terima_ranap,NULL))
				,MAX(IF(H.idruangan = '0' AND idkelas='0',H.st_terima_ranap,NULL))
				) as st_terima_ranap
				FROM setting_ranap_terima H
			WHERE H.idtujuan='$id_tujuan_terima'
		";
		$st_terima_ranap=$this->db->query($q)->row('st_terima_ranap');
		if ($st_terima_ranap=='1'){
			$st_terima_ranap=1;
		}
		// print_r($st_play);exit;
		// exit;
		// print_r($this->input->post());exit();
		
		$tahun = $this->input->post('tahun_lahir');
		$bulan = $this->input->post('bulan_lahir');
		$hari = $this->input->post('tgl_lahir');
		$tanggallahir = date("$tahun-$bulan-$hari");

			$idpasien = $this->input->post('idpasien');
			
			$kontak_tambahan=[];
			$id_kontak_tambahan = $this->input->post('id_kontak_tambahan');
			$nama_kontak = $this->input->post('nama_kontak');
			$jenis_kontak = $this->input->post('jenis_kontak');
			$level_kontak = $this->input->post('level_kontak');
			$bahasa = $this->input->post('bahasa');
			$bahasa_array='';
			if ($bahasa){
				
			$bahasa_array=implode(", ", $bahasa);
			}
			if ($nama_kontak){
				foreach($id_kontak_tambahan as $index=>$val){
					if ($nama_kontak[$index]!=''){
						$data_kontak=array(
							'nama_kontak' =>$nama_kontak[$index],
							'jenis_kontak' =>$jenis_kontak[$index],
							'level_kontak' =>$level_kontak[$index],
							'idpasien' =>$idpasien,
						);
						if ($id_kontak_tambahan[$index]==''){//INSERT
							$data_kontak['created_by'] = $this->session->userdata('user_id');
							$data_kontak['created_date'] = date('Y-m-d H:i:s');
							$this->db->insert('mfpasien_kontak',$data_kontak);
						}else{//UPDATE
							$data_kontak['edited_by'] = $this->session->userdata('user_id');
							$data_kontak['edited_date'] = date('Y-m-d H:i:s');
							$this->db->where('id',$id_kontak_tambahan[$index]);
							$this->db->update('mfpasien_kontak',$data_kontak);
						}
					}
				}
			}
			$bahasa = $this->input->post('bahasa');
			// print_r($bahasa);exit;
			$this->db->where('idpasien',$idpasien);
			$this->db->delete('mfpasien_bahasa');
			if ($bahasa){
				
				foreach($bahasa as $index=>$val){
					$data_bahasa=array(
						'idpasien' =>$idpasien,
						'bahasa_id' =>$val,
					);
					$this->db->insert('mfpasien_bahasa',$data_bahasa);
				}
			}
			$tanggaldaftar= YMDFormat($this->input->post('tanggaldaftar')) . ' ' . $this->input->post('waktudaftar');
			$tahun = $this->input->post('tahun_lahir');$bulan = $this->input->post('bulan_lahir');$hari = $this->input->post('tgl_lahir');
			$tanggallahir = date("$tahun-$bulan-$hari");
			$tahun = $this->input->post('tahun_lahirpenanggungjawab');$bulan = $this->input->post('bulan_lahirpenanggungjawab');$hari = $this->input->post('tgl_lahirpenanggungjawab');
			$tanggal_lahirpenanggungjawab = date("$tahun-$bulan-$hari");
			$tahun = $this->input->post('tahun_lahirpengantar');$bulan = $this->input->post('bulan_lahirpengantar');$hari = $this->input->post('tgl_lahirpengantar');
			$tanggal_lahirpengantar = date("$tahun-$bulan-$hari");
			
			$data=array(
				'idtipe' => $this->input->post('idtipe'),
				'idpoliklinik' => $this->input->post('pendaftaran_poli_id'),
				'tanggaldaftar' => $tanggaldaftar,
				'noantrian' => $this->input->post('noantrian'),
				'idtipepasien' => $this->input->post('idtipepasien'),
				'idjenispasien' => $this->input->post('idjenispasien'),
				'idkelompokpasien' => $this->input->post('idkelompokpasien'),
				'idrekanan' => $this->input->post('idrekanan'),
				'idtarifbpjskesehatan' => $this->input->post('idtarifbpjskesehatan'),
				'idtarifbpjstenagakerja' => $this->input->post('idtarifbpjstenagakerja'),
				'idkelompokpasien2' => $this->input->post('idkelompokpasien2'),
				'idrekanan2' => $this->input->post('idrekanan2'),
				'idtarifbpjskesehatan2' => $this->input->post('idtarifbpjskesehatan2'),
				'idtarifbpjstenagakerja2' => $this->input->post('idtarifbpjstenagakerja2'),
				'iddokterpenanggungjawab' => $this->input->post('iddokterpenanggungjawab'),
				'idruangan' => $this->input->post('idruangan'),
				'idkelas' => $this->input->post('idkelas'),
				'idbed' => $this->input->post('idbed'),
				'rujukfarmasi' => 0,
				'rujuklaboratorium' => 0,
				'rujukradiologi' => 0,
				'rujukfisioterapi' => 0,
				'iduserinput' => $this->session->userdata('user_id'),				
				'idpasien' => $this->input->post('idpasien'),
				'title' => $this->input->post('title'),
				'bahasa' => $bahasa_array,
				'namapasien' => $this->input->post('namapasien'),
				'no_medrec' => $this->input->post('no_medrec'),
				'nohp' => $this->input->post('nohp'),
				'telepon' => $this->input->post('telepon'),
				'tempat_lahir' => $this->input->post('tempat_lahir'),
				'tanggal_lahir' => $tanggallahir,
				'alamatpasien' => $this->input->post('alamatpasien'),
				'provinsi_id' => $this->input->post('provinsi_id'),
				'kabupaten_id' => $this->input->post('kabupaten_id'),
				'kecamatan_id' => $this->input->post('kecamatan_id'),
				'kelurahan_id' => $this->input->post('kelurahan_id'),
				'kodepos' => $this->input->post('kodepos'),
				'namapenanggungjawab' => $this->input->post('namapenanggungjawab'),
				'hubungan' => $this->input->post('hubungan'),
				'umurhari' => $this->input->post('umurhari'),
				'umurbulan' => $this->input->post('umurbulan'),
				'umurtahun' => $this->input->post('umurtahun'),
				'idalasan' => $this->input->post('idalasan'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'catatan' => $this->input->post('catatan'),
				'created_by' => $this->session->userdata('user_id'),
				'created_date' => date('Y-m-d H:i:s'),
				'antrian_id' => $this->input->post('antrian_id'),
				'pendaftaran_poli_id' => $this->input->post('pendaftaran_poli_id'),
				'nopendaftaran_poli' => $this->input->post('nopendaftaran_poli'),
				'perencanaan_id' => $this->input->post('perencanaan_id'),
				'nopermintaan' => $this->input->post('nopermintaan'),
				'idtipe_asal' => $this->input->post('idtipe_asal'),
				'idpoliklinik_asal' => $this->input->post('idpoliklinik_asal'),
				'diagnosa' => $this->input->post('diagnosa'),
				'iddokter_perujuk' => $this->input->post('iddokter_perujuk'),
				'nik' => $this->input->post('nik'),
				'nama_panggilan' => $this->input->post('nama_panggilan'),
				'email' => $this->input->post('email'),
				'rw' => $this->input->post('rw'),
				'rt' => $this->input->post('rt'),
				'nama_ibu_kandung' => $this->input->post('nama_ibu_kandung'),
				'golongan_darah' => $this->input->post('golongan_darah'),
				'agama_id' => $this->input->post('agama_id'),
				'warganegara' => $this->input->post('warganegara'),
				'suku_id' => $this->input->post('suku_id'),
				'pendidikan' => $this->input->post('pendidikan'),
				'pekerjaan' => $this->input->post('pekerjaan'),
				'statuskawin' => $this->input->post('statuskawin'),
				'jenis_id' => $this->input->post('jenis_id'),
				'noidentitas' => $this->input->post('noidentitas'),
				'chk_st_domisili' => $this->input->post('chk_st_domisili'),
				'alamat_jalan_ktp' => $this->input->post('alamat_jalan_ktp'),
				'rw_ktp' => $this->input->post('rw_ktp'),
				'rt_ktp' => $this->input->post('rt_ktp'),
				'provinsi_id_ktp' => $this->input->post('provinsi_id_ktp'),
				'kabupaten_id_ktp' => $this->input->post('kabupaten_id_ktp'),
				'kecamatan_id_ktp' => $this->input->post('kecamatan_id_ktp'),
				'kelurahan_id_ktp' => $this->input->post('kelurahan_id_ktp'),
				'kodepos_ktp' => $this->input->post('kodepos_ktp'),
				'hubunganpenanggungjawab' => $this->input->post('hubunganpenanggungjawab'),
				'tanggal_lahirpenanggungjawab' => $tanggal_lahirpenanggungjawab,
				'umur_tahunpenanggungjawab' => $this->input->post('umur_tahunpenanggungjawab'),
				'umur_bulanpenanggungjawab' => $this->input->post('umur_bulanpenanggungjawab'),
				'umur_haripenanggungjawab' => $this->input->post('umur_haripenanggungjawab'),
				'pekerjaannpenanggungjawab' => $this->input->post('pekerjaannpenanggungjawab'),
				'pendidikanpenanggungjawab' => $this->input->post('pendidikanpenanggungjawab'),
				'agama_idpenanggungjawab' => $this->input->post('agama_idpenanggungjawab'),
				'alamatpenanggungjawab' => $this->input->post('alamatpenanggungjawab'),
				'provinsi_idpenanggungjawab' => $this->input->post('provinsi_idpenanggungjawab'),
				'kabupaten_idpenanggungjawab' => $this->input->post('kabupaten_idpenanggungjawab'),
				'kecamatan_idpenanggungjawab' => $this->input->post('kecamatan_idpenanggungjawab'),
				'kelurahan_idpenanggungjawab' => $this->input->post('kelurahan_idpenanggungjawab'),
				'kodepospenanggungjawab' => $this->input->post('kodepospenanggungjawab'),
				'rwpenanggungjawab' => $this->input->post('rwpenanggungjawab'),
				'rtpenanggungjawab' => $this->input->post('rtpenanggungjawab'),
				'teleponpenanggungjawab' => $this->input->post('teleponpenanggungjawab'),
				'noidentitaspenanggungjawab' => $this->input->post('noidentitaspenanggungjawab'),
				'chk_st_pengantar' => $this->input->post('chk_st_pengantar'),
				'namapengantar' => $this->input->post('namapengantar'),
				'hubunganpengantar' => $this->input->post('hubunganpengantar'),
				'tanggal_lahirpengantar' => $tanggal_lahirpengantar,
				'umur_tahunpengantar' => $this->input->post('umur_tahunpengantar'),
				'umur_bulanpengantar' => $this->input->post('umur_bulanpengantar'),
				'umur_haripengantar' => $this->input->post('umur_haripengantar'),
				'pekerjaannpengantar' => $this->input->post('pekerjaannpengantar'),
				'pendidikanpengantar' => $this->input->post('pendidikanpengantar'),
				'agama_idpengantar' => $this->input->post('agama_idpengantar'),
				'alamatpengantar' => $this->input->post('alamatpengantar'),
				'provinsi_idpengantar' => $this->input->post('provinsi_idpengantar'),
				'kabupaten_idpengantar' => $this->input->post('kabupaten_idpengantar'),
				'kecamatan_idpengantar' => $this->input->post('kecamatan_idpengantar'),
				'kelurahan_idpengantar' => $this->input->post('kelurahan_idpengantar'),
				'kodepospengantar' => $this->input->post('kodepospengantar'),
				'rwpengantar' => $this->input->post('rwpengantar'),
				'rtpengantar' => $this->input->post('rtpengantar'),
				'teleponpengantar' => $this->input->post('teleponpengantar'),
				'noidentitaspengantar' => $this->input->post('noidentitaspengantar'),
				'nama_keluarga_td' => $this->input->post('nama_keluarga_td'),
				'hubungan_td' => $this->input->post('hubungan_td'),
				'alamat_td' => $this->input->post('alamat_td'),
				'catatan_lain' => $this->input->post('catatan_lain'),
				'kasus_kepolisian' => $this->input->post('kasus_kepolisian'),
				'idasalpasien' => $this->input->post('idasalpasien'),
				'dirawat_ke' => $this->input->post('dirawat_ke'),
				'nokartu' => $this->input->post('nokartu'),
				'nama_kartu' => $this->input->post('nama_kartu'),
				'noreference_kartu' => $this->input->post('noreference_kartu'),
				'kartu_id' => $this->input->post('kartu_id'),
				'st_dpjp_pendamping' => $this->input->post('st_dpjp_pendamping'),
				'idruangan_permintaan' => $this->input->post('idruangan_permintaan'),
				'idkelas_permintaan' => $this->input->post('idkelas_permintaan'),
				'idbed_permintaan' => $this->input->post('idbed_permintaan'),
				'catatan_registrasi' => $this->input->post('catatan_registrasi'),
				'st_lm' => $this->input->post('st_lm'),
				'st_sp' => $this->input->post('st_sp'),
				'st_gc' => $this->input->post('st_gc'),
				'st_hk' => $this->input->post('st_hk'),
				'st_lembar' => ($this->input->post('st_lm')=='0'?1:'0'),
				'st_pernyataan' => ($this->input->post('st_sp')=='0'?1:'0'),
				'st_general' => ($this->input->post('st_gc')=='0'?1:'0'),
				'st_hak' => ($this->input->post('st_hk')=='0'?1:'0'),
				'treservasi_bed_id' => $this->input->post('treservasi_bed_id'),
				'st_notif_sound' => 0,
				'st_play' => $st_play,
				'sound_play' => $sound_play,
				'st_terima_ranap' => $st_terima_ranap,

			);
			if ($st_terima_ranap=='1'){
				$data['tanggal_terima']=date('Y-m-d H:i:s');
				$data['user_terima']=$this->session->userdata('user_id');
				$data['user_nama_terima']=$this->session->userdata('user_name');
			}
			if ($this->db->insert('trawatinap_pendaftaran', $data)) {
				$id=$this->db->insert_id();
				$st_dpjp_pendamping = $this->input->post('st_dpjp_pendamping');
				$dpjp_id = $this->input->post('dpjp_id');
				$no_dpjp = $this->input->post('no_dpjp');
				if ($st_dpjp_pendamping=='1'){
					if ($dpjp_id){
						
						foreach($dpjp_id as $index=>$val){
							$data_dokter=array(
								'pendaftaran_id' =>$id,
								'iddokter' =>$val,
								'nourut' =>$no_dpjp[$index],
							);
							$this->db->insert('trawatinap_pendaftaran_dpjp',$data_dokter);
						}
					}
				}
				$this->insert_lm($id);
				
				
				return $id;
			}
		// } else {
			// $this->error_message = 'Penyimpanan Gagal';
			// return false;
		// }
	}
	function insert_lm($pendaftaran_id){
		$q="SELECT *FROM trawatinap_pendaftaran_lm WHERE pendaftaran_id='$pendaftaran_id'";
		$hasil=$this->db->query($q)->row('id');
		if ($hasil){
		}else{
			$q="INSERT INTO trawatinap_pendaftaran_lm (pendaftaran_id,nourut,setting_id,header_id,no,jenis_isi,pertanyaan,group_jawaban_nama,group_jawaban_id)
					SELECT '$pendaftaran_id',T.* FROM (
						SELECT 
						LPAD(H.`no`,3,'0') as  nourut
						,H.id as setting_id,H.header_id,H.`no`,H.jenis_isi as pertanyaan,H.isi,GROUP_CONCAT(R.ref SEPARATOR ' / ') as group_jawaban_nama,GROUP_CONCAT(R.id SEPARATOR ', ') as group_jawaban_id FROM setting_ranap_lembar_masuk_isi H
									LEFT JOIN musers C ON C.id=H.created_by
									LEFT JOIN musers E ON E.id=H.edited_by
									LEFT JOIN setting_ranap_lembar_masuk_isi_jawaban M ON M.general_isi_id=H.id
									LEFT JOIN merm_referensi R ON R.id=M.ref_id
									WHERE H.`status`='1' AND H.header_id=0
									
									GROUP BY H.id
									
									UNION ALL
									
									SELECT 
						CONCAT(LPAD(HH.`no`,3,'0'),'-',LPAD(H.`no`,3,'0')) as  nourut
						,H.id as setting_id,H.header_id,H.`no`,H.jenis_isi,H.isi as pertanyaan,GROUP_CONCAT(R.ref SEPARATOR ' / ') as group_jawaban_nama,GROUP_CONCAT(R.id SEPARATOR ', ') as group_jawaban_id FROM setting_ranap_lembar_masuk_isi H
									LEFT JOIN musers C ON C.id=H.created_by
									LEFT JOIN musers E ON E.id=H.edited_by
									LEFT JOIN setting_ranap_lembar_masuk_isi_jawaban M ON M.general_isi_id=H.id
									LEFT JOIN merm_referensi R ON R.id=M.ref_id
									LEFT JOIN setting_ranap_lembar_masuk_isi HH ON HH.id=H.header_id
									WHERE H.`status`='1' AND H.header_id !=0
									
									GROUP BY H.id
									
					) T ORDER BY T.nourut ASC";
			$hasil = $this->db->query($q);
		return $hasil;
		}
	}
	function insert_hk($pendaftaran_id){
		$q="SELECT *FROM trawatinap_pendaftaran_hk WHERE pendaftaran_id='$pendaftaran_id'";
		$hasil=$this->db->query($q)->row('id');
		if ($hasil){
		}else{
			$q="INSERT INTO trawatinap_pendaftaran_hk (pendaftaran_id,nourut,setting_id,header_id,no,jenis_isi,pertanyaan,group_jawaban_nama,group_jawaban_id)
					SELECT '$pendaftaran_id',T.* FROM (
						SELECT 
						LPAD(H.`no`,3,'0') as  nourut
						,H.id as setting_id,H.header_id,H.`no`,H.jenis_isi as pertanyaan,H.isi,GROUP_CONCAT(R.ref SEPARATOR ' / ') as group_jawaban_nama,GROUP_CONCAT(R.id SEPARATOR ', ') as group_jawaban_id FROM merm_general_ranap_hak_isi H
									LEFT JOIN musers C ON C.id=H.created_by
									LEFT JOIN musers E ON E.id=H.edited_by
									LEFT JOIN merm_general_ranap_hak_isi_jawaban M ON M.general_isi_id=H.id
									LEFT JOIN merm_referensi R ON R.id=M.ref_id
									WHERE H.`status`='1' AND H.header_id=0
									
									GROUP BY H.id
									
									UNION ALL
									
									SELECT 
						CONCAT(LPAD(HH.`no`,3,'0'),'-',LPAD(H.`no`,3,'0')) as  nourut
						,H.id as setting_id,H.header_id,H.`no`,H.jenis_isi,H.isi as pertanyaan,GROUP_CONCAT(R.ref SEPARATOR ' / ') as group_jawaban_nama,GROUP_CONCAT(R.id SEPARATOR ', ') as group_jawaban_id FROM merm_general_ranap_hak_isi H
									LEFT JOIN musers C ON C.id=H.created_by
									LEFT JOIN musers E ON E.id=H.edited_by
									LEFT JOIN merm_general_ranap_hak_isi_jawaban M ON M.general_isi_id=H.id
									LEFT JOIN merm_referensi R ON R.id=M.ref_id
									LEFT JOIN merm_general_ranap_hak_isi HH ON HH.id=H.header_id
									WHERE H.`status`='1' AND H.header_id !=0
									
									GROUP BY H.id
					) T ORDER BY T.nourut ASC";
			$hasil = $this->db->query($q);
		return $hasil;
		}
	}
	function insert_gc($pendaftaran_id){
		$q="SELECT *FROM trawatinap_pendaftaran_gc_head WHERE pendaftaran_id='$pendaftaran_id'";
		$hasil=$this->db->query($q)->row('id');
		if ($hasil){
		}else{
			$q="INSERT INTO trawatinap_pendaftaran_gc_head (pendaftaran_id,logo,judul,sub_header,sub_header_side,footer_1,footer_2,ttd_1,ttd_2,footer_form_1,footer_form_2)
			SELECT '$pendaftaran_id' as pendaftaran_id,logo,judul,sub_header,sub_header_side,footer_1,footer_2,ttd_1,ttd_2,footer_form_1,footer_form_2
			FROM merm_general_ranap WHERE id='1'";
			$hasil = $this->db->query($q);
			$q="INSERT INTO trawatinap_pendaftaran_gc (pendaftaran_id,no,pertanyaan,group_jawaban_id,group_jawaban_nama,jawaban_id,jawaban_nama,jenis_isi)
				SELECT '$pendaftaran_id',H.`no`,H.isi,GROUP_CONCAT(J.ref_id) as group_jawaban_id,GROUP_CONCAT(R.ref) as group_jawaban_nama,0 as jawaban_id,null as jawaban_nama,jenis_isi FROM merm_general_isi H
				LEFT JOIN merm_general_ranap_isi_jawaban J ON J.general_isi_id=H.id
				LEFT JOIN merm_referensi R ON R.id=J.ref_id
				WHERE H.`status`='1'

				GROUP BY H.id
				ORDER BY H.`no`";
			$hasil = $this->db->query($q);
		return $hasil;
		}
	}
	function get_edit_pendaftaran($id){
		$q="SELECT K.foto_kartu as foto_kartu_master,MA.nama as cara_masuk,MP.nama as naam_poli_asal,H.*
,MK.nama as nama_kelompok_pasien,MD.nama as nama_dokter_dpjp,MR.nama as nama_ruangan,mkelas.nama as nama_kelas,mbed.nama as nama_bed,mppa.id as mppa_id,mppa.nama as nama_ppa
			FROM trawatinap_pendaftaran H 
			LEFT JOIN mfpasien_kartu K ON K.id=H.kartu_id
			LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik_asal
			LEFT JOIN mpasien_asal MA ON MA.id=H.idasalpasien
			INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
			INNER JOIN mdokter MD ON MD.id=H.iddokterpenanggungjawab
			LEFT JOIN mruangan MR ON MR.id=H.idruangan
			LEFT JOIN mkelas ON mkelas.id=H.idkelas
			LEFT JOIN mbed ON mbed.id=H.idbed
			LEFT JOIN mppa ON mppa.user_id=H.created_by
			WHERE H.id='$id'";
		// print_r($q);exit;
		return $this->db->query($q)->row_array();
	}
	public function get_pasien($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('mfpasien');
		$info = $query->row('inf_gabungmedrec');
		if ($info) {
			$arr = json_decode($info);
			$row = $arr->ke_pasien_id;

			$q = "SELECT mfpasien.*,(SELECT no_medrec from mfpasien where id='$id') as noid_lama from mfpasien
				where id='$row'";
		} else {
			$q = "SELECT mfpasien.*,'' as noid_lama from mfpasien
				where id='$id'";
		}
		// print_r($q);exit;
		$query = $this->db->query($q);
		return $query->result();
	}
	public function updateData($idpendaftaran)
	{
		
		$idpasien=$this->input->post('idpasien');
			$kontak_tambahan=[];
			$id_kontak_tambahan = $this->input->post('id_kontak_tambahan');
			$nama_kontak = $this->input->post('nama_kontak');
			$jenis_kontak = $this->input->post('jenis_kontak');
			$level_kontak = $this->input->post('level_kontak');
			if ($nama_kontak){
				foreach($id_kontak_tambahan as $index=>$val){
					if ($nama_kontak[$index]!=''){
						$data_kontak=array(
							'nama_kontak' =>$nama_kontak[$index],
							'jenis_kontak' =>$jenis_kontak[$index],
							'level_kontak' =>$level_kontak[$index],
							'idpasien' =>$idpasien,
						);
						if ($id_kontak_tambahan[$index]==''){//INSERT
							$data_kontak['created_by'] = $this->session->userdata('user_id');
							$data_kontak['created_date'] = date('Y-m-d H:i:s');
							$this->db->insert('mfpasien_kontak',$data_kontak);
						}else{//UPDATE
							$data_kontak['edited_by'] = $this->session->userdata('user_id');
							$data_kontak['edited_date'] = date('Y-m-d H:i:s');
							$this->db->where('id',$id_kontak_tambahan[$index]);
							$this->db->update('mfpasien_kontak',$data_kontak);
						}
					}
				}
			}
			$bahasa = $this->input->post('bahasa');
			$bahasa_array='';
			if ($bahasa){
				
			$bahasa_array=implode(", ", $bahasa);
			}
			// print_r($bahasa);exit;
			$this->db->where('idpasien',$idpasien);
			$this->db->delete('mfpasien_bahasa');
			if ($bahasa){
				
				foreach($bahasa as $index=>$val){
					$data_bahasa=array(
						'idpasien' =>$idpasien,
						'bahasa_id' =>$val,
					);
					$this->db->insert('mfpasien_bahasa',$data_bahasa);
				}
			}
			$st_dpjp_pendamping = $this->input->post('st_dpjp_pendamping');
			$dpjp_id = $this->input->post('dpjp_id');
			$no_dpjp = $this->input->post('no_dpjp');
			$this->db->where('pendaftaran_id',$idpendaftaran);
			$this->db->delete('trawatinap_pendaftaran_dpjp');
			if ($st_dpjp_pendamping=='1'){
				if ($dpjp_id){
					
					foreach($dpjp_id as $index=>$val){
						$data_dokter=array(
							'pendaftaran_id' =>$idpendaftaran,
							'iddokter' =>$val,
							'nourut' =>$no_dpjp[$index],
						);
						$this->db->insert('trawatinap_pendaftaran_dpjp',$data_dokter);
					}
				}
			}
			$tanggaldaftar= YMDFormat($this->input->post('tanggaldaftar')) . ' ' . $this->input->post('waktudaftar');
			$tahun = $this->input->post('tahun_lahir');$bulan = $this->input->post('bulan_lahir');$hari = $this->input->post('tgl_lahir');
			$tanggallahir = date("$tahun-$bulan-$hari");
			$tahun = $this->input->post('tahun_lahirpenanggungjawab');$bulan = $this->input->post('bulan_lahirpenanggungjawab');$hari = $this->input->post('tgl_lahirpenanggungjawab');
			$tanggal_lahirpenanggungjawab = date("$tahun-$bulan-$hari");
			$tahun = $this->input->post('tahun_lahirpengantar');$bulan = $this->input->post('bulan_lahirpengantar');$hari = $this->input->post('tgl_lahirpengantar');
			$tanggal_lahirpengantar = date("$tahun-$bulan-$hari");
			
			$data=array(
				'idtipe' => $this->input->post('idtipe'),
				'idpoliklinik' => $this->input->post('pendaftaran_poli_id'),
				'tanggaldaftar' => $tanggaldaftar,
				'noantrian' => $this->input->post('noantrian'),
				'idtipepasien' => $this->input->post('idtipepasien'),
				'idjenispasien' => $this->input->post('idjenispasien'),
				'idkelompokpasien' => $this->input->post('idkelompokpasien'),
				'idrekanan' => $this->input->post('idrekanan'),
				'idtarifbpjskesehatan' => $this->input->post('idtarifbpjskesehatan'),
				'idtarifbpjstenagakerja' => $this->input->post('idtarifbpjstenagakerja'),
				'idkelompokpasien2' => $this->input->post('idkelompokpasien2'),
				'idrekanan2' => $this->input->post('idrekanan2'),
				'idtarifbpjskesehatan2' => $this->input->post('idtarifbpjskesehatan2'),
				'idtarifbpjstenagakerja2' => $this->input->post('idtarifbpjstenagakerja2'),
				'iddokterpenanggungjawab' => $this->input->post('iddokterpenanggungjawab'),
				'idruangan' => $this->input->post('idruangan'),
				'idkelas' => $this->input->post('idkelas'),
				'idbed' => $this->input->post('idbed'),
				'idpasien' => $this->input->post('idpasien'),
				'title' => $this->input->post('title'),
				'bahasa' => $bahasa_array,
				'namapasien' => $this->input->post('namapasien'),
				'no_medrec' => $this->input->post('no_medrec'),
				'nohp' => $this->input->post('nohp'),
				'telepon' => $this->input->post('telepon'),
				'tempat_lahir' => $this->input->post('tempat_lahir'),
				'tanggal_lahir' => $tanggallahir,
				'alamatpasien' => $this->input->post('alamatpasien'),
				'provinsi_id' => $this->input->post('provinsi_id'),
				'kabupaten_id' => $this->input->post('kabupaten_id'),
				'kecamatan_id' => $this->input->post('kecamatan_id'),
				'kelurahan_id' => $this->input->post('kelurahan_id'),
				'kodepos' => $this->input->post('kodepos'),
				'namapenanggungjawab' => $this->input->post('namapenanggungjawab'),
				'hubungan' => $this->input->post('hubungan'),
				'umurhari' => $this->input->post('umurhari'),
				'umurbulan' => $this->input->post('umurbulan'),
				'umurtahun' => $this->input->post('umurtahun'),
				'idalasan' => $this->input->post('idalasan'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'catatan' => $this->input->post('catatan'),
				'antrian_id' => $this->input->post('antrian_id'),
				'pendaftaran_poli_id' => $this->input->post('pendaftaran_poli_id'),
				'nopendaftaran_poli' => $this->input->post('nopendaftaran_poli'),
				'perencanaan_id' => $this->input->post('perencanaan_id'),
				'nopermintaan' => $this->input->post('nopermintaan'),
				'idtipe_asal' => $this->input->post('idtipe_asal'),
				'idpoliklinik_asal' => $this->input->post('idpoliklinik_asal'),
				'diagnosa' => $this->input->post('diagnosa'),
				'iddokter_perujuk' => $this->input->post('iddokter_perujuk'),
				'nik' => $this->input->post('nik'),
				'nama_panggilan' => $this->input->post('nama_panggilan'),
				'email' => $this->input->post('email'),
				'rw' => $this->input->post('rw'),
				'rt' => $this->input->post('rt'),
				'nama_ibu_kandung' => $this->input->post('nama_ibu_kandung'),
				'golongan_darah' => $this->input->post('golongan_darah'),
				'agama_id' => $this->input->post('agama_id'),
				'warganegara' => $this->input->post('warganegara'),
				'suku_id' => $this->input->post('suku_id'),
				'pendidikan' => $this->input->post('pendidikan'),
				'pekerjaan' => $this->input->post('pekerjaan'),
				'statuskawin' => $this->input->post('statuskawin'),
				'jenis_id' => $this->input->post('jenis_id'),
				'noidentitas' => $this->input->post('noidentitas'),
				'chk_st_domisili' => $this->input->post('chk_st_domisili'),
				'alamat_jalan_ktp' => $this->input->post('alamat_jalan_ktp'),
				'rw_ktp' => $this->input->post('rw_ktp'),
				'rt_ktp' => $this->input->post('rt_ktp'),
				'provinsi_id_ktp' => $this->input->post('provinsi_id_ktp'),
				'kabupaten_id_ktp' => $this->input->post('kabupaten_id_ktp'),
				'kecamatan_id_ktp' => $this->input->post('kecamatan_id_ktp'),
				'kelurahan_id_ktp' => $this->input->post('kelurahan_id_ktp'),
				'kodepos_ktp' => $this->input->post('kodepos_ktp'),
				'hubunganpenanggungjawab' => $this->input->post('hubunganpenanggungjawab'),
				'tanggal_lahirpenanggungjawab' => $tanggal_lahirpenanggungjawab,
				'umur_tahunpenanggungjawab' => $this->input->post('umur_tahunpenanggungjawab'),
				'umur_bulanpenanggungjawab' => $this->input->post('umur_bulanpenanggungjawab'),
				'umur_haripenanggungjawab' => $this->input->post('umur_haripenanggungjawab'),
				'pekerjaannpenanggungjawab' => $this->input->post('pekerjaannpenanggungjawab'),
				'pendidikanpenanggungjawab' => $this->input->post('pendidikanpenanggungjawab'),
				'agama_idpenanggungjawab' => $this->input->post('agama_idpenanggungjawab'),
				'alamatpenanggungjawab' => $this->input->post('alamatpenanggungjawab'),
				'provinsi_idpenanggungjawab' => $this->input->post('provinsi_idpenanggungjawab'),
				'kabupaten_idpenanggungjawab' => $this->input->post('kabupaten_idpenanggungjawab'),
				'kecamatan_idpenanggungjawab' => $this->input->post('kecamatan_idpenanggungjawab'),
				'kelurahan_idpenanggungjawab' => $this->input->post('kelurahan_idpenanggungjawab'),
				'kodepospenanggungjawab' => $this->input->post('kodepospenanggungjawab'),
				'rwpenanggungjawab' => $this->input->post('rwpenanggungjawab'),
				'rtpenanggungjawab' => $this->input->post('rtpenanggungjawab'),
				'teleponpenanggungjawab' => $this->input->post('teleponpenanggungjawab'),
				'noidentitaspenanggungjawab' => $this->input->post('noidentitaspenanggungjawab'),
				'chk_st_pengantar' => $this->input->post('chk_st_pengantar'),
				'namapengantar' => $this->input->post('namapengantar'),
				'hubunganpengantar' => $this->input->post('hubunganpengantar'),
				'tanggal_lahirpengantar' => $tanggal_lahirpengantar,
				'umur_tahunpengantar' => $this->input->post('umur_tahunpengantar'),
				'umur_bulanpengantar' => $this->input->post('umur_bulanpengantar'),
				'umur_haripengantar' => $this->input->post('umur_haripengantar'),
				'pekerjaannpengantar' => $this->input->post('pekerjaannpengantar'),
				'pendidikanpengantar' => $this->input->post('pendidikanpengantar'),
				'agama_idpengantar' => $this->input->post('agama_idpengantar'),
				'alamatpengantar' => $this->input->post('alamatpengantar'),
				'provinsi_idpengantar' => $this->input->post('provinsi_idpengantar'),
				'kabupaten_idpengantar' => $this->input->post('kabupaten_idpengantar'),
				'kecamatan_idpengantar' => $this->input->post('kecamatan_idpengantar'),
				'kelurahan_idpengantar' => $this->input->post('kelurahan_idpengantar'),
				'kodepospengantar' => $this->input->post('kodepospengantar'),
				'rwpengantar' => $this->input->post('rwpengantar'),
				'rtpengantar' => $this->input->post('rtpengantar'),
				'teleponpengantar' => $this->input->post('teleponpengantar'),
				'noidentitaspengantar' => $this->input->post('noidentitaspengantar'),
				'nama_keluarga_td' => $this->input->post('nama_keluarga_td'),
				'hubungan_td' => $this->input->post('hubungan_td'),
				'alamat_td' => $this->input->post('alamat_td'),
				'catatan_lain' => $this->input->post('catatan_lain'),
				'kasus_kepolisian' => $this->input->post('kasus_kepolisian'),
				'idasalpasien' => $this->input->post('idasalpasien'),
				'dirawat_ke' => $this->input->post('dirawat_ke'),
				'nokartu' => $this->input->post('nokartu'),
				'nama_kartu' => $this->input->post('nama_kartu'),
				'noreference_kartu' => $this->input->post('noreference_kartu'),
				'kartu_id' => $this->input->post('kartu_id'),
				'st_dpjp_pendamping' => $this->input->post('st_dpjp_pendamping'),
				'idruangan_permintaan' => $this->input->post('idruangan_permintaan'),
				'idkelas_permintaan' => $this->input->post('idkelas_permintaan'),
				'idbed_permintaan' => $this->input->post('idbed_permintaan'),
				'catatan_registrasi' => $this->input->post('catatan_registrasi'),
				'treservasi_bed_id' => $this->input->post('treservasi_bed_id'),

				// 'st_lm' => $this->input->post('st_lm'),
				// 'st_sp' => $this->input->post('st_sp'),
				// 'st_gc' => $this->input->post('st_gc'),
				// 'st_hk' => $this->input->post('st_hk'),
				// 'st_lembar' => ($this->input->post('st_lm')=='0'?1:'0'),
				// 'st_pernyataan' => ($this->input->post('st_sp')=='0'?1:'0'),
				// 'st_general' => ($this->input->post('st_gc')=='0'?1:'0'),
				// 'st_hak' => ($this->input->post('st_hk')=='0'?1:'0'),
			);
			// print_r($data);exit;
			$data['edited_by'] = $this->session->userdata('user_id');
			$data['edited_date'] = date('Y-m-d H:i:s');
			// print_r($pendaftaran);exit();
			$this->db->where('id', $idpendaftaran);
			if ($this->db->update('trawatinap_pendaftaran', $data)) {
				return true;
			}
		
	}
	function get_jumlah_rawat($idpasien){
		$q="SELECT COUNT(H.id) as dirawat_ke FROM `trawatinap_pendaftaran` H WHERE H.idpasien='$idpasien' AND H.`status` != '0'";
		$dirawat_ke=$this->db->query($q)->row('dirawat_ke');
		return $dirawat_ke;
	}
	function list_kontak($idpasien){
		$q="SELECT *FROM mfpasien_kontak WHERE idpasien='$idpasien' AND staktif='1'";
		return $this->db->query($q)->result();
	}
	function list_antrian($antrian_id=''){
		if ($antrian_id==''){
		$q="SELECT H.id,H.kodeantrian,H.tanggal FROM `antrian_harian` H WHERE H.tanggal >= CURRENT_DATE()";
			
		}else{
			
		$q="SELECT H.id,H.kodeantrian,H.tanggal FROM `antrian_harian` H WHERE H.id = '$id'";
		}
		return $this->db->query($q)->result();
	}
	function list_poli($idtipe=''){
		$q="SELECT *FROM mpoliklinik H WHERE H.`status`='1' AND H.idtipe='$idtipe'";
		return $this->db->query($q)->result();
	}
	function list_tanggal($idpoli,$iddokter){
		$q="SELECT H.tanggal,MC.sebab,MC.id as cuti_id,CASE WHEN MC.id IS NOT NULL THEN 'disabled' ELSE '' END st_disabel 
				,CONCAT(MR.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y') )as tanggal_nama,H.kodehari
				FROM `app_reservasi_tanggal` H
				LEFT JOIN mcuti_dokter MC ON MC.iddokter=H.iddokter AND H.tanggal BETWEEN MC.tanggal_dari AND MC.tanggal_sampai AND MC.`status`='1'
				LEFT JOIN merm_hari MR ON MR.kodehari=H.kodehari
				WHERE H.reservasi_tipe_id='1' AND H.iddokter='$iddokter' AND H.idpoli='$idpoli' AND H.tanggal >= CURRENT_DATE AND H.tanggal<DATE_ADD(CURRENT_DATE, INTERVAL 7 DAY) 
				GROUP BY H.tanggal";
		$data = $this->db->query($q)->result();
		return $data;
	}
	function list_jadwal_igd($tanggal){
		$q="SELECT H.tanggal, CONCAT(MH.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y') )as tanggal_nama,H.hari as kodehari FROM date_row H 
			LEFT JOIN merm_hari MH ON MH.kodehari=H.hari
			WHERE H.tanggal='$tanggal'";
		return $this->db->query($q)->result();
	}
	function list_jadwal($idpoli,$iddokter,$tanggal){
		$q="SELECT H.jadwal_id,CONCAT(DATE_FORMAT(JD.jam_dari,'%H:%i') ,' - ',DATE_FORMAT(JD.jam_sampai,'%H:%i') ) jam_nama
			,H.saldo_kuota,CASE WHEN H.saldo_kuota > 0 THEN '' ELSE 'disabled' END st_disabel
			FROM `app_reservasi_tanggal` H
			LEFT JOIN mjadwal_dokter JD ON JD.id=H.jadwal_id
			WHERE H.reservasi_tipe_id='1' AND H.iddokter='$iddokter' AND H.idpoli='$idpoli' AND H.tanggal = '$tanggal'
			GROUP BY H.tanggal,H.jadwal_id";
		$data = $this->db->query($q)->result();
		return $data;
	}
	function list_dokter(){
		$q="SELECT MP.id,MP.nama FROM `mjadwal_dokter` H
			LEFT JOIN mdokter MP ON MP.id=H.iddokter
			WHERE MP.`status`='1'
			GROUP BY H.iddokter
";
		return $this->db->query($q)->result();
	}
	function list_dokter_poli($idpoli){
		$q="SELECT MD.id,MD.nama FROM `mpoliklinik_dokter` H
			LEFT JOIN mdokter MD ON MD.id=H.iddokter
			WHERE H.idpoliklinik='$idpoli' AND H.`status`='1'";
		return $this->db->query($q)->result();
	}
	public function list_jadwal_dokter_poli($reservasi_tipe_id,$idpoli,$iddokter,$tanggal='')
	{
		
		if ($reservasi_tipe_id=='1'){
			
		
				if ($tanggal!=''){
					
					$q="SELECT 
							CONCAT(MR.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y'))as tanggal_nama
							,H.tanggal,D.saldo_kuota,D.jadwal_id,MJ.id,MJ.kuota
							,CASE 
									WHEN MC.id IS NOT NULL THEN CONCAT('CUTI' ,' ',MC.alasan)
									WHEN ML.id IS NOT NULL THEN CONCAT('Libur' ,' ',ML.nama_libur)
									WHEN H.hari='1' THEN 'Libur' 
								
									WHEN (D.saldo_kuota IS NULL OR D.saldo_kuota<=0) AND MJ.id IS NOT NULL THEN 'TIDAK ADA KUOTA' 
									WHEN MJ.id IS NULL THEN 'TIDAK ADA JADWAL' 
									WHEN MJ.id IS NOT NULL AND MJ.kuota<=0 THEN 'TIDAK ADA KUOTA'
									WHEN MJ.id IS NULL AND jadwal_antara(H.tanggal,11,1)=1 THEN 'TIDAK ANTARA JADWAL'
									END AS ket
							,CASE 
									WHEN MC.id IS NOT NULL THEN 'disabled' 
									WHEN ML.id IS NOT NULL THEN 'disabled' 
									WHEN H.hari='1' THEN 'disabled' 
									WHEN (D.saldo_kuota IS NULL OR D.saldo_kuota<=0) AND MJ.id IS NOT NULL AND  H.tanggal!=CURRENT_DATE() THEN 'disabled' 
									WHEN MJ.id IS NULL AND jadwal_antara(H.tanggal,11,1)=1 THEN ''
									WHEN MJ.id IS NOT NULL AND H.tanggal!=CURRENT_DATE() AND MJ.kuota<=0 THEN 'disabled' 
									WHEN MJ.id IS NULL AND H.tanggal!=CURRENT_DATE() THEN 'disabled' 
									
											
									ELSE '' END st_disabel
							FROM date_row H
						LEFT JOIN merm_hari MR ON MR.kodehari=H.hari
						LEFT JOIN app_reservasi_tanggal D ON D.iddokter='$iddokter' AND D.idpoli='$idpoli' AND D.tanggal=H.tanggal
						LEFT JOIN mjadwal_dokter  MJ ON MJ.iddokter='$iddokter' AND MJ.idpoliklinik='$idpoli' AND MJ.kodehari=H.hari
						LEFT JOIN mholiday ML ON H.tanggal  BETWEEN ML.tgl_libur_awal AND ML.tgl_libur_akhir
						LEFT JOIN mcuti_dokter MC ON MC.iddokter='$iddokter' AND H.tanggal BETWEEN MC.tanggal_dari AND MC.tanggal_sampai AND MC.`status`='1'
						WHERE H.tanggal ='$tanggal'
						GROUP BY H.tanggal";
				}else{
					$q="SELECT H.tanggal,CASE WHEN ML.id IS NOT NULL THEN 'LIBUR NASIONAL' ELSE MC.sebab END sebab,CASE WHEN ML.id IS NOT NULL THEN ML.id ELSE MC.id END as cuti_id
						,CASE 
						WHEN MC.id IS NOT NULL THEN 'disabled' 
						WHEN ML.id IS NOT NULL THEN 'disabled' 

						ELSE '' END st_disabel 
						,CONCAT(MR.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y') )as tanggal_nama,H.kodehari,'' as ket
						FROM `app_reservasi_tanggal` H
						LEFT JOIN mcuti_dokter MC ON MC.iddokter=H.iddokter AND H.tanggal BETWEEN MC.tanggal_dari AND MC.tanggal_sampai AND MC.`status`='1'
						LEFT JOIN mholiday ML ON H.tanggal  BETWEEN ML.tgl_libur_awal AND ML.tgl_libur_akhir
						LEFT JOIN merm_hari MR ON MR.kodehari=H.kodehari
						WHERE H.reservasi_tipe_id='1' AND H.iddokter='$iddokter' AND H.idpoli='$idpoli' AND H.tanggal >= CURRENT_DATE AND H.tanggal<DATE_ADD(CURRENT_DATE, INTERVAL 7 DAY) 
						GROUP BY H.tanggal";
				}
		}else{
			$q="SELECT H.tanggal
				,CONCAT(MR.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y') )as tanggal_nama,H.kodehari
				,CASE WHEN ML.id IS NOT NULL THEN 'disabled' ELSE '' END as st_disabel,'' as ket
				FROM `app_reservasi_tanggal` H
				LEFT JOIN mholiday ML ON H.tanggal  BETWEEN ML.tgl_libur_awal AND ML.tgl_libur_akhir
				LEFT JOIN merm_hari MR ON MR.kodehari=H.kodehari
				WHERE H.reservasi_tipe_id='2' AND H.idpoli='$idpoli' AND H.tanggal >= CURRENT_DATE AND H.tanggal<DATE_ADD(CURRENT_DATE, INTERVAL 7 DAY) 
				GROUP BY H.tanggal";
		}
		// print_r($q);exit;
		$data = $this->db->query($q)->result();
		return $data;
	}
	public function list_jam_dokter_poli($reservasi_tipe_id,$idpoli,$iddokter,$tanggal)
	{
		
		if ($reservasi_tipe_id=='1'){
			
			$q="SELECT H.jadwal_id,CONCAT(DATE_FORMAT(JD.jam_dari,'%H:%i') ,' - ',DATE_FORMAT(JD.jam_sampai,'%H:%i') ) jam_nama
			,H.saldo_kuota,CASE WHEN H.saldo_kuota > 0 THEN '' ELSE 'disabled' END st_disabel
			FROM `app_reservasi_tanggal` H
			LEFT JOIN mjadwal_dokter JD ON JD.id=H.jadwal_id
			WHERE H.reservasi_tipe_id='1' AND H.iddokter='$iddokter' AND H.idpoli='$idpoli' AND H.tanggal = '$tanggal'
			GROUP BY H.tanggal,H.jadwal_id";
		}else{
			$q="SELECT H.jam_id as jadwal_id,CONCAT(JD.jam ,'.00 - ',JD.jam_akhir,'.00' ) jam_nama
			,H.saldo_kuota,CASE WHEN COALESCE(H.saldo_kuota)  > 0 THEN '' ELSE 'disabled' END st_disabel
			FROM `app_reservasi_tanggal` H
			LEFT JOIN merm_jam JD ON JD.jam_id=H.jam_id
			WHERE H.reservasi_tipe_id='2' AND H.idpoli='$idpoli' AND H.tanggal = '$tanggal' AND H.saldo_kuota IS NOT NULL
			GROUP BY H.tanggal,H.jam_id";
		}
		// print_r($q);exit;
		$data = $this->db->query($q)->result();
		return $data;
	}
	function load_edit_poli($id){
		$q="SELECT MD.foto as foto_dokter,MD.jeniskelamin as jk_dokter,MD.nama as nama_dokter,P.nama as nama_poli,
			H.pendidikan_id as pendidikan,H.pekerjaan_id as pekerjaan
			,
			CASE 
			
			WHEN H.reservasi_tipe_id=1 THEN 
				CONCAT(merm_hari.namahari,', ',DATE_FORMAT(H.tanggal,'%d-%m-%Y'),' Pukul : ',DATE_FORMAT(MJ.jam_dari,'%H:%i'),' - ',DATE_FORMAT(MJ.jam_sampai,'%H:%i'))
			WHEN H.reservasi_tipe_id=2 THEN 
				CONCAT(merm_hari.namahari,', ',DATE_FORMAT(H.tanggal,'%d-%m-%Y'),' Pukul : ',CONCAT(MJ2.jam,'.00'),' - ',CONCAT(MJ2.jam_akhir,'.00')) END
			
			 as janji_temu
			,H.* 
			
			FROM `app_reservasi_tanggal_pasien` H
			LEFT JOIN mfpasien MP ON MP.id=H.idpasien
			LEFT JOIN merm_hari ON merm_hari.kodehari=H.kodehari
			LEFT JOIN mjadwal_dokter MJ ON MJ.id=H.jadwal_id
			LEFT JOIN mdokter MD ON MD.id=H.iddokter
			LEFT JOIN mpoliklinik P ON P.id=H.idpoli
			LEFT JOIN merm_jam MJ2 ON MJ2.jam_id=H.jam_id
			WHERE H.id='$id'";
			// print_r($q);exit;
		return $this->db->query($q)->row_array();
	}
	
	function get_gc($id){
		$q="SELECT pendaftaran_id,jawaban_ttd,jawaban_perssetujuan,logo as logo_gc,judul as judul_gc,sub_header as sub_header_gc
		,sub_header_side as sub_header_side_gc,footer_1 as footer_1_gc,footer_2 as footer_2_gc,ttd_1 as ttd_1_gc,ttd_2,footer_form_1,footer_form_2 
		FROM trawatinap_pendaftaran_gc_head WHERE pendaftaran_id='$id'";
		return $this->db->query($q)->row_array();
	}
	function get_sp($id){
		$q="SELECT logo as logo_sp,judul as judul_sp FROM `merm_skrining_pasien`";
		return $this->db->query($q)->row_array();
	}
	function get_sc($id){
		$q="SELECT logo as logo_sc,judul as judul_sc FROM `merm_skrining_covid`";
		return $this->db->query($q)->row_array();
	}
	function list_content_gc($id){
		$q="SELECT  * FROM trawatinap_pendaftaran_gc WHERE pendaftaran_id='$id'";
		return $this->db->query($q)->result();
	}
	function list_cara_bayar(){
		$q="SELECT *FROM mpasien_kelompok H WHERE H.`status`='1'";
		return $this->db->query($q)->result();
	}
	function list_asuransi(){
		$q="SELECT M.* FROM mrekanan M WHERE M.status='1'";
		return $this->db->query($q)->result();
	}
}
