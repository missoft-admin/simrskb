<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mlogic_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mlogic');
        return $query->row();
    }
	
	public function list_unit($id)
    {
        $q="SELECT S.idunit as id,U.nama FROM munitpelayanan_user_setting S
			LEFT JOIN munitpelayanan U ON U.id=S.idunit
			WHERE S.idunit NOT IN (SELECT idunit from mlogic_unit)
			GROUP BY
			S.idunit";
        return $this->db->query($q)->result();
    }
	public function list_user_bendahara($id,$tipe_rka,$idjenis)
    {
        $q="SELECT *FROM musers M
				WHERE M.id NOT IN (SELECT iduser from mlogic_user_bendahara 
									WHERE mlogic_user_bendahara.idlogic='$id' 
									AND mlogic_user_bendahara.tipe_rka='$tipe_rka' 
										AND mlogic_user_bendahara.idjenis='$idjenis' 
									) AND M.status='1'";
        return $this->db->query($q)->result();
    }
	public function list_user($id,$step,$tipe_rka,$idjenis)
    {
        $q="SELECT *FROM musers M
				WHERE M.id NOT IN (SELECT iduser from mlogic_detail WHERE mlogic_detail.idlogic='$id' 
										AND mlogic_detail.step='$step' 
										AND mlogic_detail.tipe_rka='$tipe_rka' 
										AND mlogic_detail.idjenis='$idjenis' 
										AND mlogic_detail.status='1') AND M.status='1'";
		// print_r($q);
        return $this->db->query($q)->result();
    }
	public function js_icd(){
		$search=$this->input->post('search');
		$q="SELECT *from icd_9
			WHERE  (deskripsi LIKE '%".$search."%' OR kode LIKE '%".$search."%')";
		$query=$this->db->query($q);
		$result= $query->result();		
		return $result;			
	}
	public function jd_pegawai_dokter(){
		$tipe=$this->input->post('tipe');
		$search=$this->input->post('search');
		if ($tipe=='1'){//PEgawai
			$q="SELECT M.id,M.nama from mpegawai M
			WHERE M.id NOT IN (SELECT idpegawai from mlogic_pegawai) AND nama LIKE '%".$search."%' AND M.status='1'";
		}else{
			$q="SELECT M.id,M.nama from mdokter M
			WHERE M.id NOT IN (SELECT iddokter from mlogic_dokter) AND nama LIKE '%".$search."%' AND M.status='1'";
			
		}
		// print_r($q);exit();
		$query=$this->db->query($q);
		$result= $query->result();		
		return $result;			
	}
    public function saveData()
    {
        $this->nama 		= $_POST['nama'];
        
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mlogic', $this)) {
            return  $this->db->insert_id();
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama 		= $_POST['nama'];
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mlogic', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mlogic', $this, array('id' => $id))) {
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;

        if ($this->db->update('mlogic', $this, array('id' => $id))) {
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function load_all()
    {
        $query = $this->db->get('mlogic');
        return $query->result();
    }
}
