<?php
class Widgets_laboratorium_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
    
	public function getTotalPermintaanLaboratorium($tanggalDari, $tanggalSampai)
	{
		$this->db->where('DATE(rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(rencana_pemeriksaan) <= ', $tanggalSampai);
		return $this->db->count_all_results('term_laboratorium_umum');
	}

	public function getItemPermintaanLaboratorium($tanggalDari, $tanggalSampai)
	{
		$this->db->select('term_laboratorium_umum.id,
            term_laboratorium_umum.pendaftaran_id,
            term_laboratorium_umum.pasien_id,
            term_laboratorium_umum.jumlah_edit,
            tpoliklinik_pendaftaran.nopendaftaran AS nomor_pendaftaran,
			term_laboratorium_umum.rencana_pemeriksaan AS tanggal_permintaan,
			term_laboratorium_umum.nomor_permintaan AS nomor_order,
			term_laboratorium_umum.asal_rujukan AS asal_pasien,
            mfpasien.no_medrec AS nomor_medrec,
            mfpasien.nama AS nama_pasien,
            mpoliklinik.nama AS poliklinik,
            mdokter_perujuk.nama AS dokter_perujuk,
            merm_pengaturan_tujuan_laboratorium.nama AS tujuan_laboratorium', false);
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_umum.pendaftaran_id', 'LEFT');
        $this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_laboratorium_umum.dokter_perujuk_id', 'LEFT');
        $this->db->join('merm_pengaturan_tujuan_laboratorium', 'merm_pengaturan_tujuan_laboratorium.id = term_laboratorium_umum.tujuan_laboratorium', 'LEFT');

		$this->db->where('DATE(term_laboratorium_umum.rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(term_laboratorium_umum.rencana_pemeriksaan) <= ', $tanggalSampai);

		$query = $this->db->get('term_laboratorium_umum');
		return $query->result();
	}

	public function getTotalPermintaanLaboratoriumPA($tanggalDari, $tanggalSampai)
	{
		$this->db->where('DATE(rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(rencana_pemeriksaan) <= ', $tanggalSampai);
		return $this->db->count_all_results('term_laboratorium_patologi_anatomi');
	}

	public function getItemPermintaanLaboratoriumPA($tanggalDari, $tanggalSampai)
	{
		$this->db->select('term_laboratorium_patologi_anatomi.id,
            term_laboratorium_patologi_anatomi.pendaftaran_id,
            term_laboratorium_patologi_anatomi.pasien_id,
            term_laboratorium_patologi_anatomi.jumlah_edit,
            tpoliklinik_pendaftaran.nopendaftaran AS nomor_pendaftaran,
						term_laboratorium_patologi_anatomi.rencana_pemeriksaan AS tanggal_permintaan,
						term_laboratorium_patologi_anatomi.nomor_permintaan AS nomor_order,
						term_laboratorium_patologi_anatomi.asal_rujukan AS asal_pasien,
            mfpasien.no_medrec AS nomor_medrec,
            mfpasien.nama AS nama_pasien,
            mpoliklinik.nama AS poliklinik,
            mdokter_perujuk.nama AS dokter_perujuk,
            merm_pengaturan_tujuan_laboratorium.nama AS tujuan_laboratorium', false);
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_patologi_anatomi.pendaftaran_id', 'LEFT');
        $this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_laboratorium_patologi_anatomi.dokter_perujuk_id', 'LEFT');
        $this->db->join('merm_pengaturan_tujuan_laboratorium', 'merm_pengaturan_tujuan_laboratorium.id = term_laboratorium_patologi_anatomi.tujuan_laboratorium', 'LEFT');

		$this->db->where('DATE(term_laboratorium_patologi_anatomi.rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(term_laboratorium_patologi_anatomi.rencana_pemeriksaan) <= ', $tanggalSampai);

		$query = $this->db->get('term_laboratorium_patologi_anatomi');
		return $query->result();
	}

	public function getTotalPermintaanLaboratoriumBD($tanggalDari, $tanggalSampai)
	{
		$this->db->where('DATE(rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(rencana_pemeriksaan) <= ', $tanggalSampai);
		return $this->db->count_all_results('term_laboratorium_bank_darah');
	}

	public function getItemPermintaanLaboratoriumBD($tanggalDari, $tanggalSampai)
	{
		$this->db->select('term_laboratorium_bank_darah.id,
            term_laboratorium_bank_darah.pendaftaran_id,
            term_laboratorium_bank_darah.pasien_id,
            term_laboratorium_bank_darah.jumlah_edit,
            tpoliklinik_pendaftaran.nopendaftaran AS nomor_pendaftaran,
						term_laboratorium_bank_darah.rencana_pemeriksaan AS tanggal_permintaan,
						term_laboratorium_bank_darah.nomor_permintaan AS nomor_order,
						term_laboratorium_bank_darah.asal_rujukan AS asal_pasien,
            mfpasien.no_medrec AS nomor_medrec,
            mfpasien.nama AS nama_pasien,
            mpoliklinik.nama AS poliklinik,
            mdokter_perujuk.nama AS dokter_perujuk,
            merm_pengaturan_tujuan_laboratorium.nama AS tujuan_laboratorium', false);
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_bank_darah.pendaftaran_id', 'LEFT');
        $this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_laboratorium_bank_darah.dokter_perujuk_id', 'LEFT');
        $this->db->join('merm_pengaturan_tujuan_laboratorium', 'merm_pengaturan_tujuan_laboratorium.id = term_laboratorium_bank_darah.tujuan_laboratorium', 'LEFT');

		$this->db->where('DATE(term_laboratorium_bank_darah.rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(term_laboratorium_bank_darah.rencana_pemeriksaan) <= ', $tanggalSampai);

		$query = $this->db->get('term_laboratorium_bank_darah');
		return $query->result();
	}

	public function getTotalPermintaanRadiologiXray($tanggalDari, $tanggalSampai)
	{
		$this->db->where('DATE(rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(rencana_pemeriksaan) <= ', $tanggalSampai);
		return $this->db->count_all_results('term_radiologi_xray');
	}

	public function getItemPermintaanRadiologiXray($tanggalDari, $tanggalSampai)
	{
		$this->db->select('term_radiologi_xray.id,
            term_radiologi_xray.pendaftaran_id,
            term_radiologi_xray.pasien_id,
            term_radiologi_xray.jumlah_edit,
            tpoliklinik_pendaftaran.nopendaftaran AS nomor_pendaftaran,
						term_radiologi_xray.rencana_pemeriksaan AS tanggal_permintaan,
						term_radiologi_xray.nomor_permintaan AS nomor_order,
						term_radiologi_xray.asal_rujukan AS asal_pasien,
            mfpasien.no_medrec AS nomor_medrec,
            mfpasien.nama AS nama_pasien,
            mpoliklinik.nama AS poliklinik,
            mdokter_perujuk.nama AS dokter_perujuk,
            merm_pengaturan_tujuan_radiologi.nama AS tujuan_radiologi', false);
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_radiologi_xray.pendaftaran_id', 'LEFT');
        $this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_radiologi_xray.dokter_perujuk_id', 'LEFT');
        $this->db->join('merm_pengaturan_tujuan_radiologi', 'merm_pengaturan_tujuan_radiologi.id = term_radiologi_xray.tujuan_radiologi', 'LEFT');

		$this->db->where('DATE(term_radiologi_xray.rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(term_radiologi_xray.rencana_pemeriksaan) <= ', $tanggalSampai);

		$query = $this->db->get('term_radiologi_xray');
		return $query->result();
	}

	public function getTotalPermintaanRadiologiUsg($tanggalDari, $tanggalSampai)
	{
		$this->db->where('DATE(rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(rencana_pemeriksaan) <= ', $tanggalSampai);
		return $this->db->count_all_results('term_radiologi_usg');
	}

	public function getItemPermintaanRadiologiUsg($tanggalDari, $tanggalSampai)
	{
		$this->db->select('term_radiologi_usg.id,
            term_radiologi_usg.pendaftaran_id,
            term_radiologi_usg.pasien_id,
            term_radiologi_usg.jumlah_edit,
            tpoliklinik_pendaftaran.nopendaftaran AS nomor_pendaftaran,
						term_radiologi_usg.rencana_pemeriksaan AS tanggal_permintaan,
						term_radiologi_usg.nomor_permintaan AS nomor_order,
						term_radiologi_usg.asal_rujukan AS asal_pasien,
            mfpasien.no_medrec AS nomor_medrec,
            mfpasien.nama AS nama_pasien,
            mpoliklinik.nama AS poliklinik,
            mdokter_perujuk.nama AS dokter_perujuk,
            merm_pengaturan_tujuan_radiologi.nama AS tujuan_radiologi', false);
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_radiologi_usg.pendaftaran_id', 'LEFT');
        $this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_radiologi_usg.dokter_perujuk_id', 'LEFT');
        $this->db->join('merm_pengaturan_tujuan_radiologi', 'merm_pengaturan_tujuan_radiologi.id = term_radiologi_usg.tujuan_radiologi', 'LEFT');

		$this->db->where('DATE(term_radiologi_usg.rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(term_radiologi_usg.rencana_pemeriksaan) <= ', $tanggalSampai);

		$query = $this->db->get('term_radiologi_usg');
		return $query->result();
	}

	public function getTotalPermintaanRadiologiCtscan($tanggalDari, $tanggalSampai)
	{
		$this->db->where('DATE(rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(rencana_pemeriksaan) <= ', $tanggalSampai);
		return $this->db->count_all_results('term_radiologi_ctscan');
	}

	public function getItemPermintaanRadiologiCtscan($tanggalDari, $tanggalSampai)
	{
		$this->db->select('term_radiologi_ctscan.id,
            term_radiologi_ctscan.pendaftaran_id,
            term_radiologi_ctscan.pasien_id,
            term_radiologi_ctscan.jumlah_edit,
            tpoliklinik_pendaftaran.nopendaftaran AS nomor_pendaftaran,
						term_radiologi_ctscan.rencana_pemeriksaan AS tanggal_permintaan,
						term_radiologi_ctscan.nomor_permintaan AS nomor_order,
						term_radiologi_ctscan.asal_rujukan AS asal_pasien,
            mfpasien.no_medrec AS nomor_medrec,
            mfpasien.nama AS nama_pasien,
            mpoliklinik.nama AS poliklinik,
            mdokter_perujuk.nama AS dokter_perujuk,
            merm_pengaturan_tujuan_radiologi.nama AS tujuan_radiologi', false);
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_radiologi_ctscan.pendaftaran_id', 'LEFT');
        $this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_radiologi_ctscan.dokter_perujuk_id', 'LEFT');
        $this->db->join('merm_pengaturan_tujuan_radiologi', 'merm_pengaturan_tujuan_radiologi.id = term_radiologi_ctscan.tujuan_radiologi', 'LEFT');

		$this->db->where('DATE(term_radiologi_ctscan.rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(term_radiologi_ctscan.rencana_pemeriksaan) <= ', $tanggalSampai);

		$query = $this->db->get('term_radiologi_ctscan');
		return $query->result();
	}

	public function getTotalPermintaanRadiologiMri($tanggalDari, $tanggalSampai)
	{
		$this->db->where('DATE(rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(rencana_pemeriksaan) <= ', $tanggalSampai);
		return $this->db->count_all_results('term_radiologi_mri');
	}

	public function getItemPermintaanRadiologiMri($tanggalDari, $tanggalSampai)
	{
		$this->db->select('term_radiologi_mri.id,
            term_radiologi_mri.pendaftaran_id,
            term_radiologi_mri.pasien_id,
            term_radiologi_mri.jumlah_edit,
            tpoliklinik_pendaftaran.nopendaftaran AS nomor_pendaftaran,
						term_radiologi_mri.rencana_pemeriksaan AS tanggal_permintaan,
						term_radiologi_mri.nomor_permintaan AS nomor_order,
						term_radiologi_mri.asal_rujukan AS asal_pasien,
            mfpasien.no_medrec AS nomor_medrec,
            mfpasien.nama AS nama_pasien,
            mpoliklinik.nama AS poliklinik,
            mdokter_perujuk.nama AS dokter_perujuk,
            merm_pengaturan_tujuan_radiologi.nama AS tujuan_radiologi', false);
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_radiologi_mri.pendaftaran_id', 'LEFT');
        $this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_radiologi_mri.dokter_perujuk_id', 'LEFT');
        $this->db->join('merm_pengaturan_tujuan_radiologi', 'merm_pengaturan_tujuan_radiologi.id = term_radiologi_mri.tujuan_radiologi', 'LEFT');

		$this->db->where('DATE(term_radiologi_mri.rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(term_radiologi_mri.rencana_pemeriksaan) <= ', $tanggalSampai);

		$query = $this->db->get('term_radiologi_mri');
		return $query->result();
	}

	public function getTotalPermintaanRadiologiLainnya($tanggalDari, $tanggalSampai)
	{
		$this->db->where('DATE(rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(rencana_pemeriksaan) <= ', $tanggalSampai);
		return $this->db->count_all_results('term_radiologi_lainnya');
	}

	public function getItemPermintaanRadiologiLainnya($tanggalDari, $tanggalSampai)
	{
		$this->db->select('term_radiologi_lainnya.id,
            term_radiologi_lainnya.pendaftaran_id,
            term_radiologi_lainnya.pasien_id,
            term_radiologi_lainnya.jumlah_edit,
            tpoliklinik_pendaftaran.nopendaftaran AS nomor_pendaftaran,
						term_radiologi_lainnya.rencana_pemeriksaan AS tanggal_permintaan,
						term_radiologi_lainnya.nomor_permintaan AS nomor_order,
						term_radiologi_lainnya.asal_rujukan AS asal_pasien,
            mfpasien.no_medrec AS nomor_medrec,
            mfpasien.nama AS nama_pasien,
            mpoliklinik.nama AS poliklinik,
            mdokter_perujuk.nama AS dokter_perujuk,
            merm_pengaturan_tujuan_radiologi.nama AS tujuan_radiologi', false);
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_radiologi_lainnya.pendaftaran_id', 'LEFT');
        $this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_radiologi_lainnya.dokter_perujuk_id', 'LEFT');
        $this->db->join('merm_pengaturan_tujuan_radiologi', 'merm_pengaturan_tujuan_radiologi.id = term_radiologi_lainnya.tujuan_radiologi', 'LEFT');

		$this->db->where('DATE(term_radiologi_lainnya.rencana_pemeriksaan) >= ', $tanggalDari);
		$this->db->where('DATE(term_radiologi_lainnya.rencana_pemeriksaan) <= ', $tanggalSampai);

		$query = $this->db->get('term_radiologi_lainnya');
		return $query->result();
	}
}
/* End Of File */
