<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tunit_penerimaan_model extends CI_Model
{
    public function datapermintaan()
    {
        $this->db->select('a.id, b.nama as text');
        $this->db->join('munitpelayanan b', 'b.id = a.idunitpeminta', 'left');
        $this->db->where('a.status', 2);
        $this->db->or_where('a.status', 3);
        $res = $this->db->get('tunit_permintaan a')->result();
        return $this->output->set_output(json_encode($res));
    }

    public function databarang_permintaan()
    {
        $this->load->library('datatables');
        $this->datatables->select('a.id,a.nopenerimaan,a.idpermintaan,a.totalbarang,a.status,b.nopermintaan, c.nama as peminta');
        $this->datatables->from('tunit_penerimaan a');
        $this->datatables->join('tunit_permintaan b', 'a.idpermintaan = b.id');
        $this->datatables->join('munitpelayanan c', 'b.idunitpeminta = c.id');
        return print_r($this->datatables->generate());
    }

    public function getPenerimaan($id)
    {
        $this->db->select('a.id, a.nopenerimaan, a.totalbarang, c.nama as unitpeminta, d.nama as unitpenerima, b.id as idpermintaan');
        $this->db->where('a.id', $id);
        $this->db->join('tunit_permintaan b', 'b.id = a.idpermintaan', 'left');
        $this->db->join('munitpelayanan c', 'b.idunitpeminta = c.id', 'left');
        $this->db->join('munitpelayanan d', 'b.idunitpenerima = d.id', 'left');
        return $this->db->get('tunit_penerimaan a')->row_array();
    }

    public function getPenerimaanDetail($idpenerimaan)
    {
        $this->db->where('idpenerimaan', $idpenerimaan);
        return $this->db->get('tunit_penerimaan_detail')->result();
    }

    public function getNoPermintaan()
    {
        $this->db->select('a.nopermintaan, b.nama as peminta, a.id');
        $this->db->where('a.status', 2);
        $this->db->or_where('a.status', 3);
        $this->db->join('munitpelayanan b', 'b.id = a.idunitpeminta', 'left');
        return $this->db->get('tunit_permintaan a')->result();
    }

    public function getPermintaanDetail($id)
    {
        $this->db->where('a.idpermintaan', $id);
        return $this->db->get('tunit_permintaan_detail a')->result();
    }

    public function getKuantitasSisa($idpermintaan, $idbarang, $idtipe)
    {
        $str1 = '(SELECT kuantitas FROM tunit_permintaan_detail WHERE idpermintaan = '.$idpermintaan.' AND idbarang = '.$idbarang.' AND idtipe = '.$idtipe.')';
        return $this->db->query(
            'SELECT
			IF( '.$str1.'-SUM(a.kuantitas) IS NULL, '.$str1.', '.$str1.'-SUM(a.kuantitas) ) AS kuantitasSisa
			FROM tunit_penerimaan_detail a
			LEFT JOIN tunit_penerimaan b ON b.id = a.idpenerimaan
			LEFT JOIN tunit_permintaan c ON c.id = b.idpermintaan
			WHERE a.idbarang = '.$idbarang.' AND a.idtipe = '.$idtipe.' AND c.id = '.$idpermintaan
        )->row()->kuantitasSisa;
    }

    private function _getCekKuantitasSisa($idpermintaan, $idbarang, $idtipe)
    {
        $str1 = '(SELECT kuantitas FROM tunit_permintaan_detail WHERE idpermintaan = '.$idpermintaan.' AND idbarang = '.$idbarang.' AND idtipe = '.$idtipe.')';
        return $this->db->query(
            'SELECT
			IF( '.$str1.'-SUM(a.kuantitas) IS NULL, '.$str1.', '.$str1.'-SUM(a.kuantitas) ) AS kuantitas_pemesanan,
			IF( SUM(a.kuantitas) IS NULL, 0, SUM(a.kuantitas) ) AS kuantitas_penerimaan
			FROM tunit_penerimaan_detail a
			LEFT JOIN tunit_penerimaan b ON b.id = a.idpenerimaan
			LEFT JOIN tunit_permintaan c ON c.id = b.idpermintaan
			WHERE a.idbarang = '.$idbarang.' AND a.idtipe = '.$idtipe.' AND c.id = '.$idpermintaan
        )->row();
    }

    public function save()
    {
        if ($this->input->post('totalbarang') > 0 &&  $this->input->post('totalbarang') !== '') {
            $this->db->set('nopenerimaan', get_kode('PU', 'nopenerimaan', 'tunit_penerimaan', 3));
            $this->db->set('idpermintaan', $this->input->post('idpermintaan'));
            $this->db->set('totalbarang', $this->input->post('totalbarang'));
            $this->db->set('status', 1);
            $insert = $this->db->insert('tunit_penerimaan');
            if ($insert) {
                $idpenerimaan = $this->db->insert_id();
                $detail_value = $this->input->post('detailValue_1');
                foreach (json_decode($detail_value) as $r) {
                    if ($r[2] > 0) {
                        $detail = array();
                        $detail['idpenerimaan'] = $idpenerimaan;
                        $detail['idtipe'] = $r[4];
                        $detail['idbarang'] = $r[5];
                        $detail['kuantitas'] = $r[2];

                        $qty = $this->_getCekKuantitasSisa($this->input->post('idpermintaan'), $r[5], $r[4]);
                        $kuantitas_penerimaan = $qty->kuantitas_penerimaan + $r[2];
                        if ($kuantitas_penerimaan == $qty->kuantitas_pemesanan) {
                            $detail['status'] = 2;
                            $this->_updateStatusPermintaanDetail($this->input->post('idpermintaan'), $r[5], $r[4]);
                        } else {
                            $detail['status'] = 1;
                        }
                        $insertDetail = $this->db->insert('tunit_penerimaan_detail', $detail);
                        if ($insertDetail) {
                            $this->_updateStatusPermintaan($this->input->post('idpermintaan'));
                        }
                    }
                }
                return true;
            } else {
                return false;
            }
        }
    }

    private function _updateStatusPermintaanDetail($idpermintaan, $idbarang, $idtipe)
    {
        return $this->db->update('tunit_permintaan_detail', array('status' => 2), array('idpermintaan' => $idpermintaan, 'idbarang' => $idbarang, 'idtipe' => $idtipe ));
    }


    private function _updateStatusPermintaan($idpermintaan)
    {
        $query_totalbarang_penerimaan 	= "SELECT IF( SUM(totalbarang) IS NULL, 0, SUM(totalbarang) ) totalbarang FROM tunit_penerimaan WHERE idpermintaan = ".$idpermintaan;
        $totalbarang_penerimaan 		= $this->db->query($query_totalbarang_penerimaan)->row()->totalbarang;
        $totalbarang_pemesanan 			= $this->db->get_where('tunit_permintaan', array('id' => $idpermintaan))->row()->totalbarang;

        if ($totalbarang_penerimaan !== $totalbarang_pemesanan) {
            if ($this->db->update('tunit_permintaan', array('status' => 3), array('id' => $idpermintaan))) {
                return true;
            } else {
                return false;
            }
        } else {
            if ($this->db->update('tunit_permintaan', array('status' => 4), array('id' => $idpermintaan))) {
                return true;
            } else {
                return false;
            }
        }
    }


    public function getNamaBarang($idtipe, $idbarang)
    {
        $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        $this->db->from($table[$idtipe]);
        $this->db->where('id', $idbarang);
        return $this->db->get()->row()->nama;
    }
}

/* End of file Tunit_penerimaan_model.php */
/* Location: ./application/models/Tunit_penerimaan_model.php */
