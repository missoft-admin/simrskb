<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Msetting_approval_feerujukan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSettingApproval()
    {
        $this->db->select('msetting_feerujukan_rumahsakit.*, musers.name AS namauser');
        $this->db->join('musers', 'musers.id = msetting_feerujukan_rumahsakit.userapproval');
        $query = $this->db->get('msetting_feerujukan_rumahsakit');
        return $query->result();
    }

    public function insertData()
    {
        $data = array();
        $data['step'] = $_POST['step'];
        $data['tiperujukan'] = $_POST['tiperujukan'];
        $data['nominal'] = RemoveComma($_POST['nominal']);
        $data['userapproval'] = $_POST['userapproval'];
        $data['jikasetuju'] = $_POST['jikasetuju'];
        $data['jikamenolak'] = $_POST['jikamenolak'];

        $this->db->insert('msetting_feerujukan_rumahsakit', $data);

        return true;
    }

    public function deleteData($id)
    {
      $this->db->where('id', $id);
      $this->db->delete('msetting_feerujukan_rumahsakit');
      return true;
    }
}
