<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mdata_alkes_new_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getKode()
    {
        $this->db->like('kode', '6-', 'after');
        $this->db->from('mdata_alkes');
        $query = $this->db->count_all_results();

        if ($query > 0) {
            $autono = $query + 1;
            $autono = '6-'.str_pad($autono, 4, '0', STR_PAD_LEFT);
        } else {
            $autono = '6-'.'0001';
        }

        return $autono;
    }
	function list_data_history($id){
		$q="SELECT H.*
			,MC.`name` as user_created 
			,MC.`name` as user_edited
			FROM mdata_alkes_his H
			LEFT JOIN musers MC ON MC.id=H.created_by
			LEFT JOIN musers ME ON ME.id=H.edited_by
			WHERE H.id='$id' ORDER BY H.versi_edit ASC";
		return $this->db->query($q)->result();
	}
	public function aktifkan($id)
    {
        $this->status = 1;
		
        if ($this->db->update('mdata_alkes', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mdata_alkes');
        return $query->row();
    }

    public function getKategori()
    {
        // $this->db->where('idtipe', '1');
        // $this->db->where('status', '1');
        // $query = $this->db->get('mdata_kategori');
        // return $query->result();
		$q="SELECT id,`level`,path,
				CASE 
					WHEN nama_L1 IS NOT NULL THEN nama_L1
					WHEN nama_L2 IS NOT NULL THEN nama_L2
					WHEN nama_L3 IS NOT NULL THEN nama_L3
					WHEN nama_L4 IS NOT NULL THEN nama_L4
					WHEN nama_L5 IS NOT NULL THEN nama_L5
					WHEN nama_L6 IS NOT NULL THEN nama_L6
					WHEN nama_L7 IS NOT NULL THEN nama_L7
				  ELSE NULL
				  
				END as nama
				 FROM view_kategori_alkes";
			$query=$this->db->query($q);
			return $query->result();
    }
	public function get_array_kategori($path){
		$q="SELECT id FROM view_kategori_alkes T1
			where T1.path like '$path%'";
		$query=$this->db->query($q);
		$query=$query->result();
		$array=array();
		foreach($query as $row){
			$array[]=$row->id;
		}
		
		return array_values($array);;
	}
    public function getSatuan()
    {
        $this->db->where('idkategori', '3');
        $this->db->where('status', '1');
        $query = $this->db->get('msatuan');
        return $query->result();
    }

    public function getRak($tipe)
    {
        $this->db->where('idtipe', $tipe);
        $this->db->where('status', '1');
        $query = $this->db->get('mrak');
        return $query->result();
    }

    public function saveData()
    {
        $this->kode											= $this->getKode();
        $this->idkategori								= $_POST['idkategori'];
        $this->nama											= $_POST['nama'];
        $this->komposisi								= $_POST['komposisi'];
        $this->indikasi									= $_POST['indikasi'];
        $this->idsatuanbesar						= $_POST['idsatuanbesar'];
        $this->hargasatuanbesar					= RemoveComma($_POST['hargasatuanbesar']);
        $this->jumlahsatuanbesar				= RemoveComma($_POST['jumlahsatuanbesar']);
        $this->idsatuankecil						= $_POST['idsatuankecil'];
        $this->ppn								      = RemoveComma($_POST['ppn']);
        $this->hargabeli								= RemoveComma($_POST['hargabeli']);
        $this->hargadasar								= RemoveComma($_POST['hargadasar']);
        $this->idrakgudang 					    = $_POST['idrakgudang'];
        $this->idrakfarmasi 				    = $_POST['idrakfarmasi'];
        $this->alokasiumum 							= (isset($_POST['alokasiumum']) ? 1:0);
        $this->alokasiasuransi 					= (isset($_POST['alokasiasuransi']) ? 1:0);
        $this->alokasijasaraharja 			= (isset($_POST['alokasijasaraharja']) ? 1:0);
        $this->alokasibpjskesehatan 		= (isset($_POST['alokasibpjskesehatan']) ? 1:0);
        $this->alokasibpjstenagakerja 	= (isset($_POST['alokasibpjstenagakerja']) ? 1:0);
        $this->marginumum 							= RemoveComma($_POST['marginumum']);
        $this->marginasuransi 					= RemoveComma($_POST['marginasuransi']);
        $this->marginjasaraharja 				= RemoveComma($_POST['marginjasaraharja']);
        $this->marginbpjskesehatan 			= RemoveComma($_POST['marginbpjskesehatan']);
        $this->marginbpjstenagakerja 		= RemoveComma($_POST['marginbpjstenagakerja']);
        $this->stokreorder							= RemoveComma($_POST['stokreorder']);
        $this->stokminimum							= RemoveComma($_POST['stokminimum']);
        $this->catatan									= $_POST['catatan'];
        $this->idsatuanlabel									= $_POST['idsatuanlabel'];
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		
		$this->nama_generik = $this->input->post('nama_generik');
		$this->dosis = RemoveComma($this->input->post('dosis'));
		$this->merk = $this->input->post('merk');
		$this->high_alert = $this->input->post('high_alert');
		$this->nama_industri = $this->input->post('nama_industri');
		$this->bentuk_sediaan = $this->input->post('bentuk_sediaan');
		$this->gol_obat = $this->input->post('gol_obat');
		$this->jenis_generik = $this->input->post('jenis_generik');
		$this->formularium = $this->input->post('formularium');
		$this->idsatuandosis = $this->input->post('idsatuandosis');
		
        if ($this->db->insert('mdata_alkes', $this)) {
			$this->versi_edit=0;
			$this->id=$this->db->insert_id();
			$this->db->insert('mdata_alkes_his', $this);
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->kode											= $_POST['kode'];
        $this->idkategori								= $_POST['idkategori'];
        $this->nama											= $_POST['nama'];
        $this->komposisi								= $_POST['komposisi'];
        $this->indikasi									= $_POST['indikasi'];
        $this->idsatuanbesar						= $_POST['idsatuanbesar'];
        $this->hargasatuanbesar					= RemoveComma($_POST['hargasatuanbesar']);
        $this->jumlahsatuanbesar				= RemoveComma($_POST['jumlahsatuanbesar']);
        $this->idsatuankecil						= $_POST['idsatuankecil'];
        $this->ppn								      = RemoveComma($_POST['ppn']);
        $this->hargabeli								= RemoveComma($_POST['hargabeli']);
        $this->hargadasar								= RemoveComma($_POST['hargadasar']);
        $this->idrakgudang 					    = $_POST['idrakgudang'];
        $this->idrakfarmasi 				    = $_POST['idrakfarmasi'];
        $this->alokasiumum 							= (isset($_POST['alokasiumum']) ? 1:0);
        $this->alokasiasuransi 					= (isset($_POST['alokasiasuransi']) ? 1:0);
        $this->alokasijasaraharja 			= (isset($_POST['alokasijasaraharja']) ? 1:0);
        $this->alokasibpjskesehatan 		= (isset($_POST['alokasibpjskesehatan']) ? 1:0);
        $this->alokasibpjstenagakerja 	= (isset($_POST['alokasibpjstenagakerja']) ? 1:0);
        $this->marginumum 							= RemoveComma($_POST['marginumum']);
        $this->marginasuransi 					= RemoveComma($_POST['marginasuransi']);
        $this->marginjasaraharja 				= RemoveComma($_POST['marginjasaraharja']);
        $this->marginbpjskesehatan 			= RemoveComma($_POST['marginbpjskesehatan']);
        $this->marginbpjstenagakerja 		= RemoveComma($_POST['marginbpjstenagakerja']);
        $this->stokreorder							= RemoveComma($_POST['stokreorder']);
        $this->stokminimum							= RemoveComma($_POST['stokminimum']);
        $this->catatan									= $_POST['catatan'];
        $this->idsatuandosis									= $_POST['idsatuandosis'];
        $this->idsatuanlabel									= $_POST['idsatuanlabel'];
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		
		$this->nama_generik = $this->input->post('nama_generik');
		$this->dosis = RemoveComma($this->input->post('dosis'));
		$this->merk = $this->input->post('merk');
		$this->high_alert = $this->input->post('high_alert');
		$this->nama_industri = $this->input->post('nama_industri');
		$this->bentuk_sediaan = $this->input->post('bentuk_sediaan');
		$this->gol_obat = $this->input->post('gol_obat');
		$this->jenis_generik = $this->input->post('jenis_generik');
		$this->formularium = $this->input->post('formularium');
		
        if ($this->db->update('mdata_alkes', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
        if ($this->db->update('mdata_alkes', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
