<?php

declare(strict_types=1);

class Mpengaturan_direct_poliklinik_radiologi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getListSetting()
    {
        $this->db->select('merm_pengaturan_direct_poliklinik_radiologi.*,
            mpoliklinik.nama AS nama_poliklinik');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = merm_pengaturan_direct_poliklinik_radiologi.idpoliklinik');
        $this->db->where('merm_pengaturan_direct_poliklinik_radiologi.status', '1');
        $query = $this->db->get('merm_pengaturan_direct_poliklinik_radiologi');

        return $query->result();
    }

    public function saveData()
    {
        $this->idpoliklinik = $this->input->post('idpoliklinik');
        $this->idtipe = $this->input->post('idtipe');
        $this->created_by = $this->session->userdata('user_id');
        $this->created_date = date('Y-m-d H:i:s');

        if ($this->db->insert('merm_pengaturan_direct_poliklinik_radiologi', $this)) {
            return true;
        } else {
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
        $this->deleted_by = $this->session->userdata('user_id');
        $this->deleted_date = date('Y-m-d H:i:s');

        if ($this->db->update('merm_pengaturan_direct_poliklinik_radiologi', $this, ['id' => $id])) {
            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;
    }
}
