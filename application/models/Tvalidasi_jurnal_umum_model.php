<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tvalidasi_jurnal_umum_model extends CI_Model
{
  
   public function list_tipe(){
	   $q="SELECT id,nama from ref_trx_jurnal_umum M
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_jenis(){
	   $q="SELECT id,nama from msetting_jurnal_umum_rekon M
				WHERE M.status='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_akun(){
	   $q="SELECT * from makun_nomor M
				WHERE M.status='1' 
				ORDER BY noakun ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
    public function saveData() {
        $this->tanggal_transaksi        = YMDFormat($_POST['tanggal_transaksi']);
        $this->tipe         = $this->input->post('tipe');
        $this->jenis         = ($this->input->post('tipe')!='3'?'0':$this->input->post('jenis'));
        $this->keterangan         = $this->input->post('keterangan');      
        $this->status         = 1;
        // $this->statusjurnal   = 0;
        $this->created_date  = date("Y-m-d H:i:s");
        $this->created_by  = $this->session->userdata('user_id');
        $this->created_nama  = $this->session->userdata('user_name');
		$this->db->insert('tvalidasi_jurnal_umum', $this);
		return $this->db->insert_id();
    }
	public function updateData() {
		$btn_simpan= $this->input->post('btn_simpan');
		$id= $this->input->post('id');
        $this->debet        = RemoveComma($_POST['total_debet']);
        $this->kredit        = RemoveComma($_POST['total_kredit']);
        $this->tanggal_transaksi        = YMDFormat($_POST['tanggal_transaksi']);
        $this->tipe         = $this->input->post('tipe');
        $this->jenis         = ($this->input->post('tipe')!='3'?'0':$this->input->post('jenis'));
        $this->keterangan         = $this->input->post('keterangan');      
        $this->status         = 1;
        // $this->statusjurnal   = 0;
        $this->created_date  = date("Y-m-d H:i:s");
        $this->created_by  = $this->session->userdata('user_id');
        $this->created_nama  = $this->session->userdata('user_name');
		if ($btn_simpan=='2'){
			$this->st_posting         = 1;
			$this->posting_date  = date("Y-m-d H:i:s");
			$this->posting_by  = $this->session->userdata('user_id');
			$this->posting_nama  = $this->session->userdata('user_name');
		}
		// print_r($this);exit();
		$this->db->where('id',$id);
		$this->db->update('tvalidasi_jurnal_umum', $this);
		return true;
    }
	function refresh_image($id){
		$q="SELECT  H.* from tvalidasi_jurnal_umum_dokumen H

			WHERE H.idvalidasi='$id'";
		// print_r($q);exit();
		$row= $this->db->query($q)->result();
		$tabel='';
		$no=1;
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/tvalidasi_jurnal_umum/'.$r->filename.'" target="_blank">'.substr($r->filename, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->upload_by_nama.'-'.HumanDateLong($r->upload_date).'</td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/tvalidasi_jurnal_umum/'.$r->filename.'" target="_blank" data-toggle="tooltip" title="Preview" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                    <button data-toggle="tooltip" type="button" title="Hapus" class="btn btn-danger btn-sm " onclick="removeFile('.$r->id.')"><i class="fa fa-trash-o"></i></button></td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		return $tabel;
	}
	public function list_user(){
	   $q="SELECT id,name as nama from musers 
				WHERE musers.`status`='1'
				ORDER BY name ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
	//HAPUS
	
   
   public function list_kp(){
	   $q="SELECT id,nama from mpasien_kelompok 
				WHERE mpasien_kelompok.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getHeader($id){
	   $q="SELECT tvalidasi_jurnal_umum.* from tvalidasi_jurnal_umum 
				WHERE tvalidasi_jurnal_umum.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   
   function get_nama_akun($id){
	   $q="SELECT *from makun_nomor A WHERE A.id='$id'";
	   return $this->db->query($q)->row();
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
