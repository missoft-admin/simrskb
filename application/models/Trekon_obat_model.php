<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trekon_obat_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function get_data_rekon_obat($pendaftaran_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT * FROM tpoliklinik_rekon_obat H WHERE H.status_rekon_obat='1' AND H.pendaftaran_id='$pendaftaran_id' AND H.created_ppa='$login_ppa_id'";
		return $this->db->query($q)->row_array();
	}
	function list_dokter($idpoli){
		$q="SELECT M.id,M.nama FROM `mpoliklinik_rekon_obat_dokter` H
				INNER JOIN mdokter M ON M.id=H.iddokter
				WHERE H.idpoliklinik='$idpoli'";
		return $this->db->query($q)->result();
	}
	function list_dokter_all(){
		$q="SELECT M.id,M.nama FROM `mpoliklinik_rekon_obat_dokter` H
				INNER JOIN mdokter M ON M.id=H.iddokter
				";
		return $this->db->query($q)->result();
	}
	function list_poliklinik(){
		$q="SELECT M.id,M.nama FROM mpoliklinik_rekon_obat H INNER JOIN mpoliklinik M ON M.id=H.idpoliklinik";
		return $this->db->query($q)->result();
	}
}
