<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Msumber_kas_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getSpecified($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('msumber_kas');
        return $query->row();
    }
	public function list_user($id){
		$q="SELECT M.id,M.`name`,CASE WHEN U.userid IS NOT NULL THEN 'selected' ELSE '' END as selected  
			FROM musers M 
			LEFT JOIN msumber_kas_user U ON U.userid=M.id AND U.sumber_kas_id='$id'";
		return $this->db->query($q)->result();
	}
	public function list_kas($id){
		$q="SELECT H.id,H.nama,CASE WHEN H2.sumber_kas_koneksi IS NOT NULL THEN 'selected' ELSE '' END as selected FROM msumber_kas H
			LEFT JOIN msumber_kas_koneksi H2 ON H2.sumber_kas_koneksi=H.id AND H2.sumber_kas_id='$id'
			WHERE H.id !='$id'
			";
		return $this->db->query($q)->result();
	}
    public function saveData() {
        $this->nama       = $_POST['nama'];
        $this->jenis_kas_id  = $_POST['jenis_kas_id'];
        $this->bank_id  = $_POST['bank_id'];
        $this->idakun  = $_POST['idakun'];
        $this->noakun  = $_POST['noakun'];
        $this->created_by  = $this->session->userdata('user_id');
        $this->created_date  = date('Y-m-d H:i:s');
		$this->db->insert('msumber_kas', $this);
		$id=$this->db->insert_id();
        if ($id) {
			$userid=$_POST['userid'];
			foreach($userid as $r) {
				$this->db->set('sumber_kas_id',$id);
				$this->db->set('userid',$r);
				$this->db->insert('msumber_kas_user');
			}
			$kasid=$_POST['kasid'];
			foreach($kasid as $r) {
				$this->db->set('sumber_kas_id',$id);
				$this->db->set('sumber_kas_koneksi',$r);
				$this->db->insert('msumber_kas_koneksi');
			}
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	
    public function updateData() {
        $this->nama       = $_POST['nama'];
        $this->jenis_kas_id  = $_POST['jenis_kas_id'];
        $this->bank_id  = $_POST['bank_id'];
        $this->noakun  = $_POST['noakun'];
        $this->idakun  = $_POST['idakun'];

        $this->edited_by  = $this->session->userdata('user_id');
        $this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('msumber_kas', $this, ['id' => $_POST['id']])) {
			$this->db->where('sumber_kas_id', $_POST['id']);
            $this->db->delete('msumber_kas_user');
            $userid=$_POST['userid'];
			foreach($userid as $r) {
				$this->db->set('sumber_kas_id',$_POST['id']);
				$this->db->set('userid',$r);
				$this->db->insert('msumber_kas_user');
			}
			$this->db->where('sumber_kas_id', $_POST['id']);
            $this->db->delete('msumber_kas_koneksi');
			$kasid=$_POST['kasid'];
			foreach($kasid as $r) {
				$this->db->set('sumber_kas_id',$_POST['id']);
				$this->db->set('sumber_kas_koneksi',$r);
				$this->db->insert('msumber_kas_koneksi');
			}
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id) {
        $this->status = 0;

        $this->deleted_by  = $this->session->userdata('user_id');
        $this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('msumber_kas', $this, ['id' => $id])) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    
}
