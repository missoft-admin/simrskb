<?php

declare(strict_types=1);

class Mpengaturan_laporan_gudang_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $this->db->where('status', '1');
        $query = $this->db->get('merm_pengaturan_laporan_gudang');

        return $query->row();
    }

    public function getListJenisGudang()
    {
        $query = $this->db->get('merm_pengaturan_laporan_gudang');

        return $query->result();
    }

    public function getEmail()
    {
        $query = $this->db->get('merm_pengaturan_laporan_gudang_email');
        $result_array = $query->result_array();

        return implode(',', array_column($result_array, 'email'));
    }

    public function updateData()
    {
        $this->savePengaturanLaporanGudang(json_decode($_POST['jenis_gudang_data'], true));
        $this->savePengaturanLaporanGudangEmail($_POST['email']);
        
        return true;
    }

    private function savePengaturanLaporanGudang($data)
    {
        $this->db->where('status', '1');
        if ($this->db->delete('merm_pengaturan_laporan_gudang')) {
            foreach ($data as $row) {
                $detail = [
                    'jenis_gudang' => $row['jenis_gudang'],
                    'label_name' => $row['label_name'],
                ];
    
                $this->db->insert('merm_pengaturan_laporan_gudang', $detail);
            }
        }
        
        return true;
    }

    private function savePengaturanLaporanGudangEmail($params)
    {
        $this->db->where('status', '1');
        if ($this->db->delete('merm_pengaturan_laporan_gudang_email')) {
            $email_params = explode(',', $params);
            foreach ($email_params as $email) {
                $data['email'] = $email;
                $this->db->insert('merm_pengaturan_laporan_gudang_email', $data);
            }
        }

        return true;
    }
}
