<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mppa_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	public function saveData()
	{
	
		// Tanggal Lahir
		$tahun = $this->input->post('tahun_lahir');
		$bulan = $this->input->post('bulan_lahir');
		$hari = $this->input->post('tgl_lahir');
		$tanggallahir = date("$tahun-$bulan-$hari");
		$id=$this->input->post('id');
		$data = [];
		$data['tipepegawai'] = $this->input->post('tipepegawai');
		$data['pegawai_id'] = $this->input->post('pegawai_id');
		$data['jenis_profesi_id'] = $this->input->post('jenis_profesi_id');
		$data['spesialisasi_id'] = $this->input->post('spesialisasi_id');
		$data['nip'] = $this->input->post('nip');
		$data['nama'] = $this->input->post('nama');
		$data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
		$data['alamat'] = $this->input->post('alamat');
		$data['provinsi_id'] = $this->input->post('provinsi_id');
		$data['kabupaten_id'] = $this->input->post('kabupaten_id');
		$data['kecamatan_id'] = $this->input->post('kecamatan_id');
		$data['kelurahan_id'] = $this->input->post('kelurahan_id');
		$data['kodepos'] = $this->input->post('kodepos');
		$data['rw'] = $this->input->post('rw');
		$data['rt'] = $this->input->post('rt');
		$data['warganegara_id'] = $this->input->post('warganegara_id');
		$data['tempat_lahir'] = $this->input->post('tempat_lahir');
		$data['tanggal_lahir'] = $tanggallahir;
		$data['umur_tahun'] = $this->input->post('umur_tahun');
		$data['umur_bulan'] = $this->input->post('umur_bulan');
		$data['umur_hari'] = $this->input->post('umur_hari');
		$data['agama_id'] = $this->input->post('agama_id');
		$data['pendidikan_id'] = $this->input->post('pendidikan_id');
		$data['nik'] = $this->input->post('nik');
		$data['provinsi_id_ktp'] = $this->input->post('provinsi_id_ktp');
		$data['kabupaten_id_ktp'] = $this->input->post('kabupaten_id_ktp');
		$data['kecamatan_id_ktp'] = $this->input->post('kecamatan_id_ktp');
		$data['kelurahan_id_ktp'] = $this->input->post('kelurahan_id_ktp');
		$data['kodepos_ktp'] = $this->input->post('kodepos_ktp');
		$data['rw_ktp'] = $this->input->post('rw_ktp');
		$data['rt_ktp'] = $this->input->post('rt_ktp');
		$data['alamat_ktp'] = $this->input->post('alamat_ktp');
		$data['warganegara_id'] = $this->input->post('warganegara_id');
		$data['warganegara_id_ktp'] = $this->input->post('warganegara_id_ktp');
		$data['gelar_depan'] = $this->input->post('gelar_depan');
		$data['gelar_belakang'] = $this->input->post('gelar_belakang');
		$data['chk_st_domisili'] = ($this->input->post('chk_st_domisili')?1:0);
		if ($id == '') {
			$data['created_by'] = $this->session->userdata('user_id');
			$data['created_date'] = date('Y-m-d H:i:s');
		}
		// print_r($data);exit;
		if ($this->db->replace('mppa', $data)) {
			$mppa_id = $this->db->insert_id();
			$kontak_tambahan=[];
			$id_kontak_tambahan = $this->input->post('id_kontak_tambahan');
			$nama_kontak = $this->input->post('nama_kontak');
			$jenis_kontak = $this->input->post('jenis_kontak');
			$level_kontak = $this->input->post('level_kontak');
			$bahasa = $this->input->post('bahasa');
			if ($nama_kontak){
				foreach($id_kontak_tambahan as $index=>$val){
					if ($nama_kontak[$index]!=''){
						$data_kontak=array(
							'nama_kontak' =>$nama_kontak[$index],
							'jenis_kontak' =>$jenis_kontak[$index],
							'level_kontak' =>$level_kontak[$index],
							'mppa_id' =>$mppa_id,
						);
						if ($id_kontak_tambahan[$index]==''){//INSERT
							$data_kontak['created_by'] = $this->session->userdata('user_id');
							$data_kontak['created_date'] = date('Y-m-d H:i:s');
							$this->db->insert('mppa_kontak',$data_kontak);
						}else{//UPDATE
							$data_kontak['edited_by'] = $this->session->userdata('user_id');
							$data_kontak['edited_date'] = date('Y-m-d H:i:s');
							$this->db->where('id',$id_kontak_tambahan[$index]);
							$this->db->update('mppa_kontak',$data_kontak);
						}
					}
				}
			}
			$data_identitas=[];
			$id_identitas_tambahan = $this->input->post('id_identitas_tambahan');
			$nama_identitas = $this->input->post('nama_identitas');
			$jenis_id = $this->input->post('jenis_id');
						// print_r($nama_identitas);exit;
			if ($nama_identitas){
				foreach($id_identitas_tambahan as $index=>$val){
					if ($nama_identitas[$index]!=''){
						$data_identitas=array(
							'nama_identitas' =>$nama_identitas[$index],
							'jenis_id' =>$jenis_id[$index],
							'mppa_id' =>$mppa_id,
						);
						// print_r($data_identitas);exit;
						if ($id_identitas_tambahan[$index]==''){//INSERT
							$data_identitas['created_by'] = $this->session->userdata('user_id');
							$data_identitas['created_date'] = date('Y-m-d H:i:s');
							$this->db->insert('mppa_identitas',$data_identitas);
						}else{//UPDATE
							$data_identitas['edited_by'] = $this->session->userdata('user_id');
							$data_identitas['edited_date'] = date('Y-m-d H:i:s');
							$this->db->where('id',$id_identitas_tambahan[$index]);
							$this->db->update('mppa_identitas',$data_identitas);
						}
					}
				}
			}
			return $mppa_id;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}
	function get_edit($id){
		$q="SELECT *FROM mppa H WHERE H.id='$id'";
		return $this->db->query($q)->row_array();
	}
	public function get_pasien($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('mfpasien');
		$info = $query->row('inf_gabungmedrec');
		if ($info) {
			$arr = json_decode($info);
			$row = $arr->ke_pasien_id;

			$q = "SELECT mfpasien.*,(SELECT no_medrec from mfpasien where id='$id') as noid_lama from mfpasien
				where id='$row'";
		} else {
			$q = "SELECT mfpasien.*,'' as noid_lama from mfpasien
				where id='$id'";
		}
		// print_r($q);exit;
		$query = $this->db->query($q);
		return $query->result();
	}
	public function updateData($idpendaftaran)
	{
		$id = $this->input->post('id');
		$tahun = $this->input->post('tahun_lahir');
		$bulan = $this->input->post('bulan_lahir');
		$hari = $this->input->post('tgl_lahir');
		$tanggallahir = date("$tahun-$bulan-$hari");
		// $data['tipepegawai'] = $this->input->post('tipepegawai');
		$data['pegawai_id'] = $this->input->post('pegawai_id');
		$data['jenis_profesi_id'] = $this->input->post('jenis_profesi_id');
		$data['spesialisasi_id'] = $this->input->post('spesialisasi_id');
		$data['nip'] = $this->input->post('nip');
		$data['nama'] = $this->input->post('nama');
		$data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
		$data['alamat'] = $this->input->post('alamat');
		$data['provinsi_id'] = $this->input->post('provinsi_id');
		$data['kabupaten_id'] = $this->input->post('kabupaten_id');
		$data['kecamatan_id'] = $this->input->post('kecamatan_id');
		$data['kelurahan_id'] = $this->input->post('kelurahan_id');
		$data['kodepos'] = $this->input->post('kodepos');
		$data['rw'] = $this->input->post('rw');
		$data['rt'] = $this->input->post('rt');
		$data['warganegara_id'] = $this->input->post('warganegara_id');
		$data['tempat_lahir'] = $this->input->post('tempat_lahir');
		$data['tanggal_lahir'] = $tanggallahir;
		$data['umur_tahun'] = $this->input->post('umur_tahun');
		$data['umur_bulan'] = $this->input->post('umur_bulan');
		$data['umur_hari'] = $this->input->post('umur_hari');
		$data['agama_id'] = $this->input->post('agama_id');
		$data['pendidikan_id'] = $this->input->post('pendidikan_id');
		$data['nik'] = $this->input->post('nik');
		$data['provinsi_id_ktp'] = $this->input->post('provinsi_id_ktp');
		$data['kabupaten_id_ktp'] = $this->input->post('kabupaten_id_ktp');
		$data['kecamatan_id_ktp'] = $this->input->post('kecamatan_id_ktp');
		$data['kelurahan_id_ktp'] = $this->input->post('kelurahan_id_ktp');
		$data['kodepos_ktp'] = $this->input->post('kodepos_ktp');
		$data['rw_ktp'] = $this->input->post('rw_ktp');
		$data['rt_ktp'] = $this->input->post('rt_ktp');
		$data['alamat_ktp'] = $this->input->post('alamat_ktp');
		$data['warganegara_id'] = $this->input->post('warganegara_id');
		$data['warganegara_id_ktp'] = $this->input->post('warganegara_id_ktp');
		$data['gelar_depan'] = $this->input->post('gelar_depan');
		$data['gelar_belakang'] = $this->input->post('gelar_belakang');
		$data['chk_st_domisili'] = ($this->input->post('chk_st_domisili')?1:0);
			$data['edited_by'] = $this->session->userdata('user_id');
			$data['edited_date'] = date('Y-m-d H:i:s');
		// print_r($data);exit;
		
		$this->db->where('id', $id);
		if ($this->db->update('mppa', $data)) {
			$kontak_tambahan=[];
			$id_kontak_tambahan = $this->input->post('id_kontak_tambahan');
			$nama_kontak = $this->input->post('nama_kontak');
			$jenis_kontak = $this->input->post('jenis_kontak');
			$level_kontak = $this->input->post('level_kontak');
			if ($nama_kontak){
				foreach($id_kontak_tambahan as $index=>$val){
					if ($nama_kontak[$index]!=''){
						$data_kontak=array(
							'nama_kontak' =>$nama_kontak[$index],
							'jenis_kontak' =>$jenis_kontak[$index],
							'level_kontak' =>$level_kontak[$index],
							'mppa_id' =>$id,
						);
						if ($id_kontak_tambahan[$index]==''){//INSERT
							$data_kontak['created_by'] = $this->session->userdata('user_id');
							$data_kontak['created_date'] = date('Y-m-d H:i:s');
							$this->db->insert('mppa_kontak',$data_kontak);
						}else{//UPDATE
							$data_kontak['edited_by'] = $this->session->userdata('user_id');
							$data_kontak['edited_date'] = date('Y-m-d H:i:s');
							$this->db->where('id',$id_kontak_tambahan[$index]);
							$this->db->update('mppa_kontak',$data_kontak);
						}
					}
				}
			}
			$data_identitas=[];
			$id_identitas_tambahan = $this->input->post('id_identitas_tambahan');
			$nama_identitas = $this->input->post('nama_identitas');
			$jenis_id = $this->input->post('jenis_id');
						// print_r($nama_identitas);exit;
			if ($nama_identitas){
				foreach($id_identitas_tambahan as $index=>$val){
					if ($nama_identitas[$index]!=''){
						$data_identitas=array(
							'nama_identitas' =>$nama_identitas[$index],
							'jenis_id' =>$jenis_id[$index],
							'mppa_id' =>$id,
						);
						// print_r($data_identitas);exit;
						if ($id_identitas_tambahan[$index]==''){//INSERT
							$data_identitas['created_by'] = $this->session->userdata('user_id');
							$data_identitas['created_date'] = date('Y-m-d H:i:s');
							$this->db->insert('mppa_identitas',$data_identitas);
						}else{//UPDATE
							$data_identitas['edited_by'] = $this->session->userdata('user_id');
							$data_identitas['edited_date'] = date('Y-m-d H:i:s');
							$this->db->where('id',$id_identitas_tambahan[$index]);
							$this->db->update('mppa_identitas',$data_identitas);
						}
					}
				}
			}
			
			return true;
			
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}
	function list_kontak($idpasien){
		$q="SELECT *FROM mppa_kontak WHERE mppa_id='$idpasien' AND staktif='1'";
		return $this->db->query($q)->result();
	}
	function list_identitas($idpasien){
		$q="SELECT *FROM mppa_identitas WHERE mppa_id='$idpasien' AND staktif='1'";
		return $this->db->query($q)->result();
	}
	
}
