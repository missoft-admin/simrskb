<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_pengelolaan_hold_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDokter($id)
    {
        $this->db->select('id, nama');
        $this->db->where('id', $id);
        $query = $this->db->get('mdokter');
        return $query->row();
    }

    public function getRuangan($id)
    {
        $this->db->select('id, nama');
        $this->db->where('id', $id);
        $query = $this->db->get('mruangan');
        return $query->row();
    }
}
