<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Minterval_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('minterval');
        return $query->row();
    }

    public function saveData()
    {
        $this->jumlah_setiap_pemberian 		= $_POST['jumlah_setiap_pemberian'];
        $this->nama 		= $_POST['nama'];
        $this->label_interval 		= $_POST['label_interval'];
        $this->jumlah_pemberian 	= $_POST['jumlah_pemberian'];
        $this->interval_waktu 	= $_POST['interval_waktu'];
        $this->st_default 	= $_POST['st_default'];
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('minterval', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->jumlah_setiap_pemberian 		= $_POST['jumlah_setiap_pemberian'];
        $this->nama 		= $_POST['nama'];
        $this->label_interval 		= $_POST['label_interval'];
        $this->jumlah_pemberian 	= $_POST['jumlah_pemberian'];
        $this->interval_waktu 	= $_POST['interval_waktu'];
        $this->st_default 	= $_POST['st_default'];
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('minterval', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('minterval', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function load_all()
    {
        $query = $this->db->get('minterval');
        return $query->result();
    }
}
