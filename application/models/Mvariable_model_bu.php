<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mvariable_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getSpecified($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('mvariable');
        return $query->row();
    }
	public function list_user($id){
		$q="SELECT M.id,M.`name`,CASE WHEN U.userid IS NOT NULL THEN 'selected' ELSE '' END as selected  
			FROM musers M 
			LEFT JOIN mvariable_user U ON U.userid=M.id AND U.sumber_kas_id='$id'";
		return $this->db->query($q)->result();
	}
	public function list_kas($id){
		$q="SELECT H.id,H.nama,CASE WHEN H2.sumber_kas_koneksi IS NOT NULL THEN 'selected' ELSE '' END as selected FROM mvariable H
			LEFT JOIN mvariable_koneksi H2 ON H2.sumber_kas_koneksi=H.id AND H2.sumber_kas_id='$id'
			WHERE H.id !='$id'
			";
		return $this->db->query($q)->result();
	}
    public function saveData() {
        $this->nama       = $_POST['nama'];
        $this->jenis_id  = $_POST['jenis_id'];
        $this->tipe_id  = $_POST['tipe_id'];
        $this->created_by  = $this->session->userdata('user_id');
        $this->created_date  = date('Y-m-d H:i:s');
		$this->db->insert('mvariable', $this);
		return true;
    }
	
    public function updateData() {
        $this->nama       = $_POST['nama'];
        $this->jenis_id  = $_POST['jenis_id'];
        $this->tipe_id  = $_POST['tipe_id'];

        $this->edited_by  = $this->session->userdata('user_id');
        $this->edited_date  = date('Y-m-d H:i:s');
		$this->db->update('mvariable', $this, ['id' => $_POST['id']]);
       
        return true;
       
    }
	public function aktifkan($id) {
        $this->status = 1;

      

        if ($this->db->update('mvariable', $this, ['id' => $id])) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    
    public function softDelete($id) {
        $this->status = 0;

        $this->deleted_by  = $this->session->userdata('user_id');
        $this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mvariable', $this, ['id' => $id])) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    
}
