<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tbuku_besar_hutang_detail_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function getDistributor($iddistributor){
		$q="SELECT GROUP_CONCAT(M.nama) as distributor FROM (
			SELECT CONCAT('1-',H.id) as id,H.nama from mdistributor H 
			UNION ALL
			SELECT CONCAT('3-',H.id) as id,H.nama from mvendor H 
			) M 
			WHERE M.id IN (".$iddistributor.")
			ORDER BY M.nama";
		return $this->db->query($q)->row('distributor');
			
	}
	function tipe_pemesanan_biasa($tipe){
		if ($tipe=='1'){
			$tipe='PEMBELIAN';
		}
		if ($tipe=='2'){
			$tipe='RETUR';
			
		}
		if ($tipe=='3'){
			$tipe='PENGAJUAN';
		}
		if ($tipe=='4'){
			$tipe='PEMBAYARAN';
		}
		return $tipe;
	}
}
