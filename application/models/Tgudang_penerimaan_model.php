<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tgudang_penerimaan_model extends CI_Model {
    public function get_satuan_barang() {
        $idtipe = 1;
        $idbarang = 1;
        $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        $this->db->from($table[$idtipe]);
        $this->db->where('id', $idbarang);
        $row = $this->db->get()->row();
        $res = array();
        if ($row->idsatuanbesar == $row->idsatuankecil) {
            $res[]['id'] = $row->idsatuanbesar;
            $res[]['text'] = $this->get_satuan_detail($row->idsatuanbesar)->nama;
        } else {
            $res[]['id'] = $row->idsatuanbesar;
            $res[]['text'] = $this->get_satuan_detail($row->idsatuanbesar)->nama;
            $res[]['id'] = $row->idsatuankecil;
            $res[]['text'] = $this->get_satuan_detail($row->idsatuankecil)->nama;
        }
        $this->output->set_output(json_encode($res));
    }
	public function list_pembayaran($id,$head_bayar_id) {
        // $this->db->where('id', $id);
        // $query = $this->db->get('tsetoran_kas_detail');
		$q="SELECT JK.nama as jenis_kas,SK.nama as sumber_kas,D.* FROM tgudang_penerimaan_pembayaran D
		LEFT JOIN mjenis_kas JK ON JK.id=D.jenis_kas_id
		LEFT JOIN msumber_kas SK ON SK.id=D.sumber_kas_id
		WHERE D.penerimaan_id='$id' AND D.status='1' AND D.head_bayar_id='$head_bayar_id'
		";
        return $this->db->query($q)->result();
    }

	public function list_pembayaran_head($id) {
        // $this->db->where('id', $id);
        // $query = $this->db->get('tsetoran_kas_detail');
		$q="SELECT H.*,GROUP_CONCAT(M.nama) as kas FROM `tgudang_penerimaan_pembayaran_head` H
			LEFT JOIN tgudang_penerimaan_pembayaran D ON D.head_bayar_id=H.id 
			LEFT JOIN msumber_kas M ON M.id=D.sumber_kas_id
			WHERE H.penerimaan_id='$id' AND D.status='1' AND H.status='1'
			GROUP BY H.id
		";
        return $this->db->query($q)->result();
    }
	public function list_barang($id) {
         $userid = $this->session->userdata('user_id');
		$q="SELECT view_barang_all.idkategori,M.nama as nama_kategori,view_barang_all.nama as nama_barang
			,view_barang_all.namatipe
			,MAX(
			IF(calculate_logic(mlogic_gudang_kas.operand, tgudang_penerimaan_detail.totalharga, mlogic_gudang_kas.nominal) = 1,
				IF(mlogic_gudang_kas.idbarang = 0 AND mlogic_gudang_kas.idkategori = 0,1
					,IF(mlogic_gudang_kas.idbarang !=0 AND mlogic_gudang_kas.idkategori = 0 AND tgudang_penerimaan_detail.idbarang =  mlogic_gudang_kas.idbarang,1
				,IF(mlogic_gudang_kas.idbarang = 0 AND mlogic_gudang_kas.idkategori != 0 AND view_barang_all.idkategori =  mlogic_gudang_kas.idkategori,1,0)))
			,0)) as st_acces
			,tgudang_penerimaan_detail.*
			FROM tgudang_penerimaan_detail
			INNER JOIN view_barang_all ON view_barang_all.id = tgudang_penerimaan_detail.idbarang AND view_barang_all.idtipe = tgudang_penerimaan_detail.idtipe
			INNER JOIN mlogic_gudang_kas ON tgudang_penerimaan_detail.idtipe = mlogic_gudang_kas.idtipe 
			INNER JOIN mdata_kategori M ON M.id=view_barang_all.idkategori
			WHERE idpenerimaan = '$id' AND mlogic_gudang_kas.iduser = '$userid'
			GROUP BY  tgudang_penerimaan_detail.id
			ORDER BY tgudang_penerimaan_detail.id ASC
		";
        return $this->db->query($q)->result();
    }
	public function list_barang_all($id) {
         $userid = $this->session->userdata('user_id');
		$q="SELECT view_barang_all.idkategori,M.nama as nama_kategori,view_barang_all.nama as nama_barang
			,view_barang_all.namatipe
			,MAX(
			IF(calculate_logic(mlogic_gudang_kas.operand, tgudang_penerimaan_detail.totalharga, mlogic_gudang_kas.nominal) = 1,
				IF(mlogic_gudang_kas.idbarang = 0 AND mlogic_gudang_kas.idkategori = 0 AND mlogic_gudang_kas.iduser IN (".$userid."),1
					,IF(mlogic_gudang_kas.idbarang !=0 AND mlogic_gudang_kas.idkategori = 0 AND tgudang_penerimaan_detail.idbarang =  mlogic_gudang_kas.idbarang  AND mlogic_gudang_kas.iduser IN (".$userid."),1
				,IF(mlogic_gudang_kas.idbarang = 0 AND mlogic_gudang_kas.idkategori != 0 AND view_barang_all.idkategori =  mlogic_gudang_kas.idkategori  AND mlogic_gudang_kas.iduser IN (".$userid."),1,0)))
			,0)) as st_acces
			,tgudang_penerimaan_detail.*
			FROM tgudang_penerimaan_detail
			INNER JOIN view_barang_all ON view_barang_all.id = tgudang_penerimaan_detail.idbarang AND view_barang_all.idtipe = tgudang_penerimaan_detail.idtipe
			INNER JOIN mlogic_gudang_kas ON tgudang_penerimaan_detail.idtipe = mlogic_gudang_kas.idtipe 
			INNER JOIN mdata_kategori M ON M.id=view_barang_all.idkategori
			WHERE idpenerimaan = '$id'
			GROUP BY  tgudang_penerimaan_detail.id
			ORDER BY tgudang_penerimaan_detail.id ASC
		";
        return $this->db->query($q)->result();
    }
	public function list_barang_detail($id,$head_bayar_id='') {
		
		 $where='';
         $userid = $this->session->userdata('user_id');
		 if ($head_bayar_id==''){
			 $where=" AND tgudang_penerimaan_detail.head_bayar_id is null ";
		 }else{
			 $where=" AND tgudang_penerimaan_detail.head_bayar_id='".$head_bayar_id."'";
			 
		 }
		$q="SELECT view_barang_all.idkategori,M.nama as nama_kategori,view_barang_all.nama as nama_barang
			,view_barang_all.namatipe
			,MAX(
			IF(calculate_logic(mlogic_gudang_kas.operand, tgudang_penerimaan_detail.totalharga, mlogic_gudang_kas.nominal) = 1,
				IF(mlogic_gudang_kas.idbarang = 0 AND mlogic_gudang_kas.idkategori = 0,1
					,IF(mlogic_gudang_kas.idbarang !=0 AND mlogic_gudang_kas.idkategori = 0 AND tgudang_penerimaan_detail.idbarang =  mlogic_gudang_kas.idbarang,1
				,IF(mlogic_gudang_kas.idbarang = 0 AND mlogic_gudang_kas.idkategori != 0 AND view_barang_all.idkategori =  mlogic_gudang_kas.idkategori,1,0)))
			,0)) as st_acces
			,tgudang_penerimaan_detail.*
			FROM tgudang_penerimaan_detail
			INNER JOIN view_barang_all ON view_barang_all.id = tgudang_penerimaan_detail.idbarang AND view_barang_all.idtipe = tgudang_penerimaan_detail.idtipe
			INNER JOIN mlogic_gudang_kas ON tgudang_penerimaan_detail.idtipe = mlogic_gudang_kas.idtipe 
			INNER JOIN mdata_kategori M ON M.id=view_barang_all.idkategori
			WHERE idpenerimaan = '$id' AND mlogic_gudang_kas.iduser = '$userid' ".$where."
			GROUP BY  tgudang_penerimaan_detail.id
			ORDER BY tgudang_penerimaan_detail.id ASC
		";
        return $this->db->query($q)->result();
    }
    public function get_satuan_detail($id) {
        $this->db->where('id', $id);
        return $this->db->get('msatuan', 1)->row();
    }
	public function get_file_name($id) {
        $this->db->where('id', $id);
        return $this->db->get('tgudang_penerimaan_lampiran', 1)->row();
    }

    public function get_pemesanan_detail($idpemesanan) {
        $qry="SELECT a.*,";
        $qry.="b.nama,";
        $qry.="c.nopemesanan,";
        $qry.="c.iddistributor";
        $qry.=" FROM tgudang_pemesanan_detail a";
        $qry.=" JOIN tgudang_pemesanan c ON a.idpemesanan = c.id";
        $qry.=" JOIN mdistributor b ON c.iddistributor = b.id";
        $qry.=" WHERE a.idpemesanan = ".$idpemesanan;
        $qry.=" AND a.status=1";
        return $this->db->query($qry)->result();
    }

    public function get_nopemesanan() {
        $userid = $this->session->userdata('user_id');
        $from = "(SELECT
        tgudang_pemesanan.*,
        mdistributor.nama
        FROM tgudang_pemesanan
        LEFT JOIN mdistributor ON tgudang_pemesanan.iddistributor = mdistributor.id
        WHERE (tgudang_pemesanan.status = 4) AND CEILING(DATEDIFF(curdate(),tgudang_pemesanan.tanggal))<=30
        AND tipepemesanan IN (SELECT gudangtipe FROM musers_tipegudang WHERE iduser = ".$userid.")
        ) as tbl";
        $this->db->from($from);
        return $this->db->get()->result();
    }
	public function get_nopemesanan2($id) {
        $userid = $this->session->userdata('user_id');
        $from = "(SELECT
        tgudang_pemesanan.*,
        mdistributor.nama
        FROM tgudang_pemesanan
        LEFT JOIN mdistributor ON tgudang_pemesanan.iddistributor = mdistributor.id
        WHERE (tgudang_pemesanan.status = 4) AND tgudang_pemesanan.id !='$id' AND CEILING(DATEDIFF(curdate(),tgudang_pemesanan.tanggal)/7)<=2
        AND tipepemesanan IN (SELECT gudangtipe FROM musers_tipegudang WHERE iduser = ".$userid.")
        ) as tbl";
        $this->db->from($from);
        return $this->db->get()->result();
    }

    public function save() {
		// print_r($this->input->post());exit();
			$idpemesanan=$this->input->post('nopemesanan');
			
        if ($this->input->post('totalbarang') > 0 &&  $this->input->post('totalbarang') !== '') {
            $pemesanan = get_by_field('id', $this->input->post('nopemesanan'), 'tgudang_pemesanan');
			// print_r($pemesanan);exit();
            $head 						= array();
            $head['nopenerimaan']		= '-';
            $head['iddistributor']		= $pemesanan->iddistributor;
            $head['tipepenerimaan']		= $pemesanan->tipepemesanan;
            $head['nofakturexternal']	= $this->input->post('nofakturexternal');
            $head['tipe_bayar']			= $this->input->post('pembayaran');
            $head['keterangan']			= $this->input->post('keterangan');
            $head['idpemesanan']		= $this->input->post('nopemesanan');
            $head['tgl_terima']			= YMDFormat($this->input->post('tanggalpenerimaan'));
            if ($this->input->post('pembayaran')=='2'){
				$head['tanggaljatuhtempo']	= YMDFormat($this->input->post('tanggaljatuhtempo'));
			}
            $head['tanggalpenerimaan']		= date('Y-m-d H:i:s');
			
            $head['totalbarang']		= $this->input->post('totalbarang');
            $head['userpenerimaan']		= $this->session->userdata('user_name');
            $head['totalharga']			= 0;
            $head['status']				= 1;
			
			
			$idtipe=$this->input->post('e_idtipe');
			$idbarang=$this->input->post('e_idbarang');
			$iddet=$this->input->post('e_iddet');
			$harga=$this->input->post('e_harga');
			$harga_plus_ppn=$this->input->post('e_harga_plus_ppn');
			$harga_total_fix=$this->input->post('e_harga_total_fix');
			$no_batch=$this->input->post('e_no_batch');
			$st_edit=$this->input->post('e_st_edit');
			$st_hapus=$this->input->post('e_st_hapus');
			$st_alih=$this->input->post('e_st_alih');
			$kuantitas=$this->input->post('e_kuantitas');
			$kuantitas_sisa=$this->input->post('e_kuantitas_sisa');
			$harga_master=$this->input->post('e_harga_master');
			$tgl_kadaluarsa=$this->input->post('e_tgl_kadaluarsa');
			$jml_Terima=$this->input->post('e_jml_Terima');
			$satuan=$this->input->post('e_satuan');
			$opsisatuan=$this->input->post('e_opsisatuan');
			$diskon=$this->input->post('e_harga_diskon');
			$ppn=$this->input->post('e_ppn');
			// print_r($this->input->post('tanggalpenerimaan'));exit();
			
			// print_r($head);exit();
            if ($this->db->insert('tgudang_penerimaan', $head)) {
                $idpenerimaan = $this->db->insert_id();

                foreach ($st_edit as $key => $val) {
				if ($jml_Terima[$key] > 0){
                        
                        $detail 						= array();

                        $detail['opsisatuan'] 			= $opsisatuan[$key];
                        $detail['idpenerimaan']			= $idpenerimaan;
                        $detail['nobatch']				= $no_batch[$key];
                        $detail['harga']				= $harga_plus_ppn[$key];
                        $detail['nominaldiskon']		= $diskon[$key];
                        $detail['ppn']					= $ppn[$key];
                        $detail['nominalppn']			= $harga_plus_ppn[$key] - $harga[$key];
                        $detail['kuantitas']			= $kuantitas[$key];
                        $detail['totalharga']			= $harga_total_fix[$key];
                        $detail['idtipe']				= $idtipe[$key];
                        $detail['idbarang']				= $idbarang[$key];
                        $detail['tanggalkadaluarsa']	= YMDFormat($tgl_kadaluarsa[$key]);
                        $detail['namasatuan'] 			= $satuan[$key];
                        $detail['idpemesanandetail']    = $iddet[$key];
                        $detail['harga_sebelumnya']     = $harga_master[$key];
						

                        $detail['status']				= 1;
                        if ($this->db->insert('tgudang_penerimaan_detail', $detail)) {
                            $this->db->where('id', $idpenerimaan);
                            $this->db->set('totalharga', ' totalharga + ' . $harga_total_fix[$key], false);
                            $this->db->update('tgudang_penerimaan');
                        }
                    }
                }
				
           
            }
        }
		if ($this->input->post('st_hapus_alih')=='1'){
			$d_iddistributor=$this->input->post('d_iddistributor');
			$d_alasan=$this->input->post('d_alasan');
			$d_iddet=$this->input->post('d_iddet');
			$d_status=$this->input->post('d_status');
			$d_kuantitas=$this->input->post('d_kuantitas');
			asort($d_iddistributor);
			$dist_asal='';
			$no_head='';
			$no_detail='';
			foreach ($d_iddistributor as $key => $val) {
				$id_detail=$d_iddet[$key];
				$q="SELECT D.kuantitas from tgudang_pemesanan_detail D WHERE D.id='$id_detail'";
				
				$r=$this->db->query($q);
				$qty=$r->row('kuantitas');
				
				$t_qty=$d_kuantitas[$key];
				$kuantitas=$d_kuantitas[$key];
				$kuantitas_kecil=$this->get_data_satuan($d_iddet[$key]) * $kuantitas;
					
				if ($d_status[$key]=='1'){//HAPUS 
					if ($qty != $d_kuantitas[$key]){//HAPUS SEBAGIAN
						$alasan='BATALKAN SEBAGIAN PENERIMAAN : '.$d_alasan[$key];
						$this->db->set('alasan', $alasan);
						$this->db->set('kuantitas', ' kuantitas - ' . $kuantitas, false);
						$this->db->set('kuantitas_kecil', ' kuantitas_kecil - ' . $kuantitas_kecil, false);
						$this->db->where('id', $d_iddet[$key]);
						$this->db->update('tgudang_pemesanan_detail'); 
					}else{//HAPUS ALL
						$data_update=array();
						$data_update['alasan']='BATALKAN PENERIMAAN :'.$d_alasan[$key];
						$data_update['status']='0';
						$data_update['hapus_by']=$this->session->userdata('user_id');
						$data_update['hapus_date']=date('Y-m-d H:i:s');
						// print_r($d_iddet[$key]);exit();
						$this->db->where('id', $d_iddet[$key]);
						$this->db->update('tgudang_pemesanan_detail',$data_update); 
				
					}
				
				}
				
				if ($d_status[$key]=='3'){//GANTI DISRIBUTOR
					
					// print_r('SINI GA');
					if ($dist_asal != $d_iddistributor[$key]){//Membuat No HEAD
						$dist_asal=$d_iddistributor[$key];
						
						$alasan='ALIHKAN DISRIBUTOR PENERIMAAN : '.$d_alasan[$key];
						$query="INSERT INTO tgudang_pemesanan (tipepemesanan,tanggal,iddistributor,totalbarang,totalharga,statusretur,stdraft,`status`,userpemesanan,id_user_pemesanan,id_user_konfirmasi,nama_user_konfirmasi,tanggal_konfirmasi,id_user_approval,nama_user_approval,tanggal_approval,edit_user,edit_date,id_asal,alasan_ganti)
								SELECT
									tipepemesanan,now() as	tanggal,	'$dist_asal' AS iddistributor,	'0' AS totalbarang,	'0' AS totalharga,	'0' AS statusretur,	'0' AS stdraft,'3' AS `status`,
									userpemesanan,id_user_pemesanan,id_user_konfirmasi,nama_user_konfirmasi,tanggal_konfirmasi,id_user_approval,nama_user_approval,tanggal_approval,edit_user,edit_date,'$idpemesanan' as id_asal,'$alasan' as alasan_ganti
								FROM
									tgudang_pemesanan 
								WHERE
									id = '$idpemesanan'";
						$this->db->query($query);
						$no_head=$this->db->insert_id();
						// print_r($no_head);exit();
					}
					
					$q="INSERT INTO tgudang_pemesanan_detail (idpemesanan,idtipe,idbarang,opsisatuan,kuantitas,kuantitas_kecil,
							harga,harga_total,status,harga_terakhir,stok_terakhir,id_asal,harga_sebelumnya,
							stok_sebelumnya,st_konversi,edit_by,date_edit,hapus_by,hapus_date,alasan)
							SELECT '$no_head' as idpemesanan,idtipe,idbarang,opsisatuan,'$t_qty' as kuantitas,kuantitas_kecil,
							harga,harga_total,'1' as status,harga_terakhir,stok_terakhir,'$id_detail' as id_asal,harga_sebelumnya,
							stok_sebelumnya,st_konversi,edit_by,date_edit,hapus_by,hapus_date,alasan  
							from tgudang_pemesanan_detail WHERE id='$id_detail'";
							
					
					
					$this->db->query($q);
					$no_detail=$this->db->insert_id();
					
					// print_r($qty.'-'.$d_kuantitas[$key]);exit();
					if ($qty != $d_kuantitas[$key]){
						$alasan='ALIHKAN DI PENERIMAAN :'.$d_alasan[$key];
						$this->db->set('alasan', $alasan);
						$this->db->set('kuantitas', ' kuantitas - ' . $kuantitas, false);
						$this->db->set('kuantitas_kecil', ' kuantitas_kecil - ' . $kuantitas_kecil, false);
						$this->db->where('id', $d_iddet[$key]);
						$this->db->update('tgudang_pemesanan_detail'); 
						// $this->db->set('kuantitas', ' kuantitas - ' . $kuantitas, false);
					}else{
						$q="UPDATE tgudang_pemesanan_history SET iddet_gudang='$no_detail' WHERE iddet_gudang='$id_detail'";
						$this->db->query($q);
						
						$data_update=array();
						$data_update['alasan']='ALIHKAN DI PENERIMAAN :'.$d_alasan[$key];
						$data_update['status']='2';
						$this->db->where('id', $d_iddet[$key]);
						$this->db->update('tgudang_pemesanan_detail',$data_update);
					
					}
									
				}
				
			}
			
		}
		
        return true;
    }
	public function save2() {
		// print_r($this->input->post());exit();
			$idpemesanan=$this->input->post('nopemesanan');
			
        if ($this->input->post('totalbarang') > 0 &&  $this->input->post('totalbarang') !== '') {
            $pemesanan = get_by_field('id', $this->input->post('nopemesanan'), 'tgudang_pemesanan');
			// print_r($pemesanan);exit();
            $head 						= array();
            $head['nopenerimaan']		= '-';
            $head['iddistributor']		= $pemesanan->iddistributor;
            $head['tipepenerimaan']		= $pemesanan->tipepemesanan;
            $head['nofakturexternal']	= $this->input->post('nofakturexternal');
            $head['tipe_bayar']			= $this->input->post('pembayaran');
            $head['keterangan']			= $this->input->post('keterangan');
            $head['idpemesanan']		= $this->input->post('nopemesanan');
            $head['tgl_terima']			= YMDFormat($this->input->post('tanggalpenerimaan'));
            if ($this->input->post('pembayaran')=='2'){
				$head['tanggaljatuhtempo']	= YMDFormat($this->input->post('tanggaljatuhtempo'));
			}
            $head['tanggalpenerimaan']		= date('Y-m-d H:i:s');
			
            $head['totalbarang']		= $this->input->post('totalbarang');
            $head['userpenerimaan']		= $this->session->userdata('user_name');
            $head['totalharga']			= 0;
            $head['status']				= 1;
			
			
			$idtipe=$this->input->post('e_idtipe');
			$idbarang=$this->input->post('e_idbarang');
			$iddet=$this->input->post('e_iddet');
			// $harga=$this->input->post('e_harga');
			// $harga_plus_ppn=$this->input->post('e_harga_plus_ppn');
			// $harga_total_fix=$this->input->post('e_harga_total_fix');
			$no_batch=$this->input->post('e_no_batch');
			$st_edit=$this->input->post('e_st_edit');
			$st_hapus=$this->input->post('e_st_hapus');
			$st_alih=$this->input->post('e_st_alih');
			$kuantitas=$this->input->post('e_kuantitas');
			$kuantitas_sisa=$this->input->post('e_kuantitas_sisa');
			$harga_master=$this->input->post('e_harga_master');
			$tgl_kadaluarsa=$this->input->post('e_tgl_kadaluarsa');
			$jml_Terima=$this->input->post('e_jml_Terima');
			$satuan=$this->input->post('e_satuan');
			$opsisatuan=$this->input->post('e_opsisatuan');
			// $diskon=$this->input->post('e_harga_diskon');
			// $ppn=$this->input->post('e_ppn');
			// print_r($this->input->post('tanggalpenerimaan'));exit();
			
			// print_r($head);exit();
			$harga_asli=$this->input->post('e_harga_asli');
			$diskon=$this->input->post('e_diskon');
			$diskon_rp=$this->input->post('e_diskon_rp');
			$harga_after_diskon=$this->input->post('e_harga_after_diskon');
			$pajak=$this->input->post('e_pajak');
			$pajak_rp=$this->input->post('e_pajak_rp');
			$harga_after_ppn=$this->input->post('e_harga_after_ppn');
			$harga_total_final=$this->input->post('e_harga_total_final');
			
            if ($this->db->insert('tgudang_penerimaan', $head)) {
                $idpenerimaan = $this->db->insert_id();

                foreach ($st_edit as $key => $val) {
				if ($jml_Terima[$key] > 0){
                        
                        $detail 						= array();

                        $detail['opsisatuan'] 			= $opsisatuan[$key];
                        $detail['idpenerimaan']			= $idpenerimaan;
                        $detail['nobatch']				= $no_batch[$key];
                        $detail['harga']				= $harga_after_ppn[$key];
                        $detail['diskon']				= $diskon[$key];
                        $detail['nominaldiskon']		= $diskon_rp[$key];
                        $detail['ppn']					= $pajak[$key];
                        $detail['nominalppn']			= $pajak_rp[$key];
                        $detail['kuantitas']			= $kuantitas[$key];
                        $detail['totalharga']			= $harga_total_final[$key];
                        $detail['idtipe']				= $idtipe[$key];
                        $detail['idbarang']				= $idbarang[$key];
                        $detail['tanggalkadaluarsa']	= YMDFormat($tgl_kadaluarsa[$key]);
                        $detail['namasatuan'] 			= $satuan[$key];
                        $detail['idpemesanandetail']    = $iddet[$key];
                        $detail['harga_sebelumnya']     = $harga_master[$key];
                        $detail['harga_asli']     = $harga_asli[$key];
                        $detail['harga_after_diskon']     = $harga_after_diskon[$key];
                        $detail['harga_after_ppn']     = $harga_after_ppn[$key];
						

                        $detail['status']				= 1;
                        if ($this->db->insert('tgudang_penerimaan_detail', $detail)) {
                            $this->db->where('id', $idpenerimaan);
                            $this->db->set('totalharga', ' totalharga + ' . $harga_total_final[$key], false);
                            $this->db->update('tgudang_penerimaan');
                        }
                    }
                }
				
           
            }
        }
		if ($this->input->post('st_hapus_alih')=='1'){
			$d_iddistributor=$this->input->post('d_iddistributor');
			$d_alasan=$this->input->post('d_alasan');
			$d_iddet=$this->input->post('d_iddet');
			$d_status=$this->input->post('d_status');
			$d_kuantitas=$this->input->post('d_kuantitas');
			asort($d_iddistributor);
			$dist_asal='';
			$no_head='';
			$no_detail='';
			foreach ($d_iddistributor as $key => $val) {
				$id_detail=$d_iddet[$key];
				$q="SELECT D.kuantitas from tgudang_pemesanan_detail D WHERE D.id='$id_detail'";
				
				$r=$this->db->query($q);
				$qty=$r->row('kuantitas');
				
				$t_qty=$d_kuantitas[$key];
				$kuantitas=$d_kuantitas[$key];
				$kuantitas_kecil=$this->get_data_satuan($d_iddet[$key]) * $kuantitas;
					
				if ($d_status[$key]=='1'){//HAPUS 
					if ($qty != $d_kuantitas[$key]){//HAPUS SEBAGIAN
						$alasan='BATALKAN SEBAGIAN PENERIMAAN : '.$d_alasan[$key];
						$this->db->set('alasan', $alasan);
						$this->db->set('kuantitas', ' kuantitas - ' . $kuantitas, false);
						$this->db->set('kuantitas_kecil', ' kuantitas_kecil - ' . $kuantitas_kecil, false);
						$this->db->where('id', $d_iddet[$key]);
						$this->db->update('tgudang_pemesanan_detail'); 
					}else{//HAPUS ALL
						$data_update=array();
						$data_update['alasan']='BATALKAN PENERIMAAN :'.$d_alasan[$key];
						$data_update['status']='0';
						$data_update['hapus_by']=$this->session->userdata('user_id');
						$data_update['hapus_date']=date('Y-m-d H:i:s');
						// print_r($d_iddet[$key]);exit();
						$this->db->where('id', $d_iddet[$key]);
						$this->db->update('tgudang_pemesanan_detail',$data_update); 
				
					}
				
				}
				
				if ($d_status[$key]=='3'){//GANTI DISRIBUTOR
					
					// print_r('SINI GA');
					if ($dist_asal != $d_iddistributor[$key]){//Membuat No HEAD
						$dist_asal=$d_iddistributor[$key];
						
						$alasan='ALIHKAN DISRIBUTOR PENERIMAAN : '.$d_alasan[$key];
						$query="INSERT INTO tgudang_pemesanan (tipepemesanan,tanggal,iddistributor,totalbarang,totalharga,statusretur,stdraft,`status`,userpemesanan,id_user_pemesanan,id_user_konfirmasi,nama_user_konfirmasi,tanggal_konfirmasi,id_user_approval,nama_user_approval,tanggal_approval,edit_user,edit_date,id_asal,alasan_ganti)
								SELECT
									tipepemesanan,now() as	tanggal,	'$dist_asal' AS iddistributor,	'0' AS totalbarang,	'0' AS totalharga,	'0' AS statusretur,	'0' AS stdraft,'3' AS `status`,
									userpemesanan,id_user_pemesanan,id_user_konfirmasi,nama_user_konfirmasi,tanggal_konfirmasi,id_user_approval,nama_user_approval,tanggal_approval,edit_user,edit_date,'$idpemesanan' as id_asal,'$alasan' as alasan_ganti
								FROM
									tgudang_pemesanan 
								WHERE
									id = '$idpemesanan'";
						$this->db->query($query);
						$no_head=$this->db->insert_id();
						// print_r($no_head);exit();
					}
					
					$q="INSERT INTO tgudang_pemesanan_detail (idpemesanan,idtipe,idbarang,opsisatuan,kuantitas,kuantitas_kecil,
							harga,harga_total,status,harga_terakhir,stok_terakhir,id_asal,harga_sebelumnya,
							stok_sebelumnya,st_konversi,edit_by,date_edit,hapus_by,hapus_date,alasan)
							SELECT '$no_head' as idpemesanan,idtipe,idbarang,opsisatuan,'$t_qty' as kuantitas,kuantitas_kecil,
							harga,harga_total,'1' as status,harga_terakhir,stok_terakhir,'$id_detail' as id_asal,harga_sebelumnya,
							stok_sebelumnya,st_konversi,edit_by,date_edit,hapus_by,hapus_date,alasan  
							from tgudang_pemesanan_detail WHERE id='$id_detail'";
							
					
					
					$this->db->query($q);
					$no_detail=$this->db->insert_id();
					
					// print_r($qty.'-'.$d_kuantitas[$key]);exit();
					if ($qty != $d_kuantitas[$key]){
						$alasan='ALIHKAN DI PENERIMAAN :'.$d_alasan[$key];
						$this->db->set('alasan', $alasan);
						$this->db->set('kuantitas', ' kuantitas - ' . $kuantitas, false);
						$this->db->set('kuantitas_kecil', ' kuantitas_kecil - ' . $kuantitas_kecil, false);
						$this->db->where('id', $d_iddet[$key]);
						$this->db->update('tgudang_pemesanan_detail'); 
						// $this->db->set('kuantitas', ' kuantitas - ' . $kuantitas, false);
					}else{
						$q="UPDATE tgudang_pemesanan_history SET iddet_gudang='$no_detail' WHERE iddet_gudang='$id_detail'";
						$this->db->query($q);
						
						$data_update=array();
						$data_update['alasan']='ALIHKAN DI PENERIMAAN :'.$d_alasan[$key];
						$data_update['status']='2';
						$this->db->where('id', $d_iddet[$key]);
						$this->db->update('tgudang_pemesanan_detail',$data_update);
					
					}
									
				}
				
			}
			
		}
		
        return true;
    }
	public function update_header_selesai($idpemesanan){
		
	}
	public function save_edit() {
		// print_r($this->input->post());exit();
		$id=$this->input->post('id');
			
        if ($this->input->post('totalbarang') > 0 &&  $this->input->post('totalbarang') !== '') {
            $head 						= array();
            $head['nofakturexternal']	= $this->input->post('nofakturexternal');
            $head['tipe_bayar']			= $this->input->post('pembayaran');
            $head['keterangan']			= $this->input->post('keterangan');
            $head['tgl_terima']			= YMDFormat($this->input->post('tanggalpenerimaan'));
			if ($this->input->post('pembayaran')=='2'){
				$head['tanggaljatuhtempo']	= YMDFormat($this->input->post('tanggaljatuhtempo'));
			}
            $head['totalbarang']		= RemoveComma($this->input->post('totalbarang'));
            $head['id_user_updated']		= $this->session->userdata('user_name');
            $head['name_user_updated']		= $this->session->userdata('user_id');
            $head['totalharga']			= RemoveComma($this->input->post('totalharga'));
			// $head['tanggalpenerimaan']		= date('Y-m-d H:i:s');
            // $head['status']				= 1;
			if ($this->input->post('btn_simpan')=='3'){
				$head['stverif_kbo']		= '2';			
			}
			if ($this->input->post('btn_simpan')=='4'){
				$head['stverif_kbo']		= '1';			
			}
			
			$idtipe=$this->input->post('e_idtipe');
			$idbarang=$this->input->post('e_idbarang');
			$iddet=$this->input->post('e_iddet');
			$harga=$this->input->post('e_harga');
			$harga_plus_ppn=$this->input->post('e_harga_plus_ppn');
			$harga_total_fix=$this->input->post('e_harga_total_fix');
			$no_batch=$this->input->post('e_no_batch');
			$st_edit=$this->input->post('e_st_edit');
			$st_hapus=$this->input->post('e_st_hapus');
			$st_alih=$this->input->post('e_st_alih');
			$kuantitas=$this->input->post('e_kuantitas');
			$kuantitas_sisa=$this->input->post('e_kuantitas_sisa');
			$harga_master=$this->input->post('e_harga_master');
			$tgl_kadaluarsa=$this->input->post('e_tgl_kadaluarsa');
			$jml_Terima=$this->input->post('e_jml_Terima');
			$satuan=$this->input->post('e_satuan');
			$opsisatuan=$this->input->post('e_opsisatuan');
			$diskon=$this->input->post('e_harga_diskon');
			$ppn=$this->input->post('e_ppn');
			
			$harga_asli=$this->input->post('e_harga_asli');
			$diskon=$this->input->post('e_diskon');
			$diskon_rp=$this->input->post('e_diskon_rp');
			$harga_after_diskon=$this->input->post('e_harga_after_diskon');
			$pajak=$this->input->post('e_pajak');
			$pajak_rp=$this->input->post('e_pajak_rp');
			$harga_after_ppn=$this->input->post('e_harga_after_ppn');
			$harga_total_final=$this->input->post('e_harga_total_final');
			
			// print_r($this->input->post('tanggalpenerimaan'));exit();
			$this->db->where('id',$id);
			// print_r($head);exit();
            if ($this->db->update('tgudang_penerimaan', $head)) {

                foreach ($st_edit as $key => $val) {
				if ($st_edit[$key] == '1'){
                        
                        $detail 						= array();

                        // $detail['opsisatuan'] 			= $opsisatuan[$key];
                        // $detail['nobatch']				= $no_batch[$key];
                        // $detail['harga']				= $harga_plus_ppn[$key];
                        // $detail['nominaldiskon']		= $diskon[$key];
                        // $detail['ppn']					= $ppn[$key];
                        // $detail['nominalppn']			= $harga_plus_ppn[$key] - $harga[$key];
                        // $detail['kuantitas']			= $kuantitas[$key];
                        // $detail['totalharga']			= $harga_total_fix[$key];
                        // $detail['tanggalkadaluarsa']	= YMDFormat($tgl_kadaluarsa[$key]);
                        // $detail['namasatuan'] 			= $satuan[$key];
                        // $detail['harga_sebelumnya']     = $harga_master[$key];
						
						$detail['opsisatuan'] 			= $opsisatuan[$key];
                        // $detail['idpenerimaan']			= $idpenerimaan;
                        $detail['nobatch']				= $no_batch[$key];
                        $detail['harga']				= $harga_after_ppn[$key];
                        $detail['diskon']				= $diskon[$key];
                        $detail['nominaldiskon']		= $diskon_rp[$key];
                        $detail['ppn']					= $pajak[$key];
                        $detail['nominalppn']			= $pajak_rp[$key];
                        $detail['kuantitas']			= $kuantitas[$key];
                        $detail['totalharga']			= $harga_total_final[$key];
                        $detail['tanggalkadaluarsa']	= YMDFormat($tgl_kadaluarsa[$key]);
                        $detail['namasatuan'] 			= $satuan[$key];
                        $detail['harga_sebelumnya']     = $harga_master[$key];
                        $detail['harga_asli']     = $harga_asli[$key];
                        $detail['harga_after_diskon']     = $harga_after_diskon[$key];
                        $detail['harga_after_ppn']     = $harga_after_ppn[$key];
						
						$this->db->where('id', $iddet[$key]);
						$this->db->update('tgudang_penerimaan_detail',$detail);
                    }
                }
				
           
            }
        }
		
        return true;
    }
	public function save_batal() {
		// print_r($this->input->post());exit();
		$tanggal=date('Y-m-d H:i:s');
		$id=$this->input->post('id');
		$st_batal_all=$this->input->post('st_batal_all');
		$d_iddet=$this->input->post('d_iddet');
		$d_alasan=$this->input->post('d_alasan');
		if ($st_batal_all){//All Batal
			$head 						= array();
			$head['id_user_batal'] 			= $this->session->userdata('user_id');
			$head['nama_user_batal'] 			= $this->session->userdata('user_name');
			$head['datetime_batal'] 			= $tanggal;
			$head['alasanbatal'] 			=$this->input->post('alasan_all');
			$head['status'] 			= '0';
			$head['totalbarang']		= RemoveComma($this->input->post('totalbarang'));
            $head['id_user_updated']		= $this->session->userdata('user_name');
            $head['name_user_updated']		= $this->session->userdata('user_id');
            $head['totalharga']			= RemoveComma($this->input->post('totalharga'));
			$this->db->where('id',$id);
			$this->db->update('tgudang_penerimaan', $head);
		}else{
			$head 						= array();
			$head['totalbarang']		= RemoveComma($this->input->post('totalbarang'));
            $head['id_user_updated']		= $this->session->userdata('user_name');
            $head['name_user_updated']		= $this->session->userdata('user_id');
            $head['totalharga']			= RemoveComma($this->input->post('totalharga'));
			$this->db->where('id',$id);
			$this->db->update('tgudang_penerimaan', $head);
			
		}
        foreach ($d_iddet as $key => $val) 
		{
			$detail 						= array();
			$detail['status'] 			= '0';
			$detail['id_user_batal'] 			= $this->session->userdata('user_id');
			$detail['nama_user_batal'] 			= $this->session->userdata('user_name');
			$detail['datetime_batal'] 			= $tanggal;
			$detail['alasanbatal'] 			= $d_alasan[$key];
			$this->db->where('id', $d_iddet[$key]);
			$this->db->update('tgudang_penerimaan_detail',$detail);
		}
		
        return true;
    }
	public function get_data_satuan($id){
		$q="SELECT 
			CASE WHEN D.opsisatuan='2' THEN B.jumlahsatuanbesar ELSE '1' END as kali
			 from tgudang_pemesanan_detail D
			LEFT JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe
			WHERE D.id='$id'";
			$query=$this->db->query($q);
		return $query->row('kali');
	}
    public function update() {
        if ($this->input->post('totalbarang') > 0 &&  $this->input->post('totalbarang') !== '') {
            $idpenerimaan               = $this->input->post("id");
            $penerimaan                 = get_by_field('id', $this->input->post('id'), 'tgudang_penerimaan');
            $pemesanan                  = get_by_field('id', $penerimaan->idpemesanan, 'tgudang_pemesanan');
            $head 						= array();
            $head['tipepenerimaan']		= $pemesanan->tipepemesanan;
            $head['nofakturexternal']	= $this->input->post('nofakturexternal');
            $head['keterangan']			= $this->input->post('keterangan');
            $head['idpemesanan']		= $this->input->post('nopemesanan');
            // $head['tanggalpenerimaan']	= $this->input->post('tanggalpenerimaan');
            $head['tanggaljatuhtempo']	= $this->input->post('tanggaljatuhtempo');
            $head['totalbarang']		= $this->input->post('totalbarang');
            $head['userpenerimaan']		= $this->session->userdata('user_name');
            $head['totalharga']			= 0;
            $head['status']				= 1;
            $head['datetime_updated']   = date("Y-m-d H:i:s");
            $head['id_user_updated']    = $this->session->userdata('user_id');
            $head['name_user_updated']  = $this->session->userdata('user_name');
            $this->db->where('id',$idpenerimaan);
            $this->db->update('tgudang_penerimaan', $head);

            $detail_value = $this->input->post('detailValue');
            foreach ($detail_value as $r) {
                if ($r[6] > 0) {
                    $this->db->select('opsisatuan');
                    $this->db->where('idtipe', $r[10]);
                    $this->db->where('idbarang', $r[11]);
                    $this->db->where('idpemesanan', $head['idpemesanan']);
                    $opsisatuan = $this->db->get('tgudang_pemesanan_detail', 1)->row()->opsisatuan;

                    $detail 						= array();
                    $detail['opsisatuan'] 			= $opsisatuan;
                    $detail['idpenerimaan']			= $idpenerimaan;
                    $detail['nobatch']				= $r[2];
                    $detail['harga']				= $r[3];
                    $detail['diskon']				= $r[5];
                    $detail['nominaldiskon']		= $r[15];
                    $detail['ppn']					= $r[4];
                    $detail['nominalppn']			= $r[16];
                    $detail['kuantitas']			= $r[6];
                    $detail['totalharga']			= $r[7];
                    $detail['idtipe']				= $r[10];
                    $detail['idbarang']				= $r[11];
                    $detail['tanggalkadaluarsa']	= $r[14];
                    $detail['namasatuan'] 			= $r[17];
                    $detail['idpemesanandetail']    = $r[20];
                    
                    $sisa = $this->get_kuantitas_sisa($head['idpemesanan'], $r[11], $r[10]);
                    $sisa = $sisa - $r[6];
                    if ($sisa<=0){
                        $detail['status']				= 2;
                        $this->db->update('tgudang_pemesanan_detail', 
                            array('status' => 2), 
                            array('idpemesanan' => $head['idpemesanan'], 'idbarang' => $r [11], 'idtipe' => $r [10] )
                        );
                    } else {
                        $detail['status']				= 1;
                    }
                    $this->db->where('id',$r[21]);
                    $this->db->update('tgudang_penerimaan_detail', $detail);
                    $this->db->set('totalharga', ' totalharga + ' . $r[7], false);
                    $this->db->update('tgudang_penerimaan');
                }
            }
        }
    }

    public function get_kuantitas_sisa($idpemesanan, $idbarang, $idtipe) {
        $qry='(SELECT kuantitas FROM tgudang_pemesanan_detail WHERE idpemesanan = '.$idpemesanan.' AND idbarang = '.$idbarang.' AND idtipe = '.$idtipe.')';
        $qry='SELECT
			IF( '.$qry.'-SUM(a.kuantitas) IS NULL, '.$qry.', '.$qry.'-SUM(a.kuantitas) ) AS kuantitas_sisa
			FROM tgudang_penerimaan_detail a
			LEFT JOIN tgudang_penerimaan b ON b.id = a.idpenerimaan
			LEFT JOIN tgudang_pemesanan c ON c.id = b.idpemesanan
			WHERE a.idbarang = '.$idbarang.' AND a.idtipe = '.$idtipe.'
            AND c.id = '.$idpemesanan;
        return $this->db->query($qry)->row()->kuantitas_sisa;
    }

    private function _updateHargaBarang($idtipe, $idbarang, $harga) {
        $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        $this->db->set('hargadasar', $harga);
        $this->db->where('id', $idbarang);
        return $this->db->update($table[$idtipe]);
    }

    private function _updateStokGudang($idtipe, $idbarang, $kuantitas) {
        $this->db->where('idunitpelayanan', 0);
        $this->db->where('idtipe', $idtipe);
        $this->db->where('idbarang', $idbarang);
        $cekGudang = $this->db->get('mgudang_stok');
        if ($cekGudang->num_rows() > 0) {
            $g = $cekGudang->row();
            $this->db->set('stok', $kuantitas+$g->stok);
            $this->db->where('idtipe', $idtipe);
            $this->db->where('idbarang', $idbarang);
            $this->db->where('idunitpelayanan', 0);
            return $this->db->update('mgudang_stok');
        } else {
            $this->db->set('stok', $kuantitas);
            $this->db->set('idtipe', $idtipe);
            $this->db->set('idbarang', $idbarang);
            $this->db->set('idunitpelayanan', 0);
            $this->db->set('stokkedaluwarsa', 0);
            $this->db->set('stokrusak', 0);
            $this->db->set('status', 1);
            return $this->db->insert('mgudang_stok');
        }
    }

    public function _update_status_head_pemesanan($idpemesanan) {
        $query_totalbarang_penerimaan 	= "SELECT IF( SUM(totalbarang) IS NULL, 0, SUM(totalbarang) ) totalbarang FROM tgudang_penerimaan WHERE idpemesanan = ".$idpemesanan;
        $totalbarang_penerimaan 		= $this->db->query($query_totalbarang_penerimaan)->row()->totalbarang;
        $totalbarang_pemesanan 			= $this->db->get_where('tgudang_pemesanan', array('id' => $idpemesanan))->row()->totalbarang;

        if ($totalbarang_penerimaan !== $totalbarang_pemesanan) {
            if ($this->db->update('tgudang_pemesanan', array('status' => 3), array('id' => $idpemesanan))) {
                return true;
            } else {
                return false;
            }
        } else {
            if ($this->db->update('tgudang_pemesanan', array('status' => 4), array('id' => $idpemesanan))) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function _status_detail() {
    }
	public function list_pesanan_edit($id,$idterima){
	   $q="SELECT
	D.id,
	D.idpemesanan,
	D.idtipe,
	D.idbarang,
	B.namatipe,
	B.kode,
	B.nama AS namabarang,
	B.jumlahsatuanbesar,
	D.kuantitas_kecil,
	CEIL( D.kuantitas_kecil / B.jumlahsatuanbesar ) AS kuantitas_besar,
	( D.kuantitas_kecil * B.hargabeli ) AS harga,
	sk.nama AS satuan_kecil,
	sb.nama AS satuan_besar,
	D.opsisatuan,
	B.hargabeli,
	B.hargabeli_besar,COALESCE(TD.kuantitas,0)  as kuantitas_terima
FROM
	tgudang_pemesanan_detail D
	LEFT JOIN view_barang B ON B.id = D.idbarang 
	AND B.idtipe = D.idtipe
	LEFT JOIN msatuan sk ON sk.id = B.idsatuan
	LEFT JOIN msatuan sb ON sb.id = B.idsatuanbesar 
	LEFT JOIN tgudang_penerimaan_detail TD ON TD.idpenerimaan='$idterima' AND TD.idpemesanandetail=D.id
WHERE
	D.idpemesanan = '$id' 
	AND D.STATUS = '1'";
		// print_r($q);exit;
		$query=$this->db->query($q);
		return $query->result();
   }
    public function viewHead($id) {
        $this->db->select('
			a.*,
            c.nama as distributor,
            c.id as iddistributor,
            b.nopemesanan,m.id as id_user_penerimaan,mppa.id as mppa_id
		');
        $this->db->where('a.id', $id);
        $this->db->join('tgudang_pemesanan b', 'b.id = a.idpemesanan', 'left');
        $this->db->join('mdistributor c', 'c.id = b.iddistributor', 'left');
        $this->db->join('musers m', 'm.name = a.userpenerimaan', 'left');
        $this->db->join('mppa', 'mppa.user_id = m.id', 'left');
        return $this->db->get('tgudang_penerimaan a')->row_array();
    }

    public function viewDetail($idpenerimaan) {
        $this->db->select('p.*,d.kuantitas as kuantitaspemesanan,(p.totalharga-p.nominaldiskon) totalsebelumdiskon');
        $this->db->where('idpenerimaan', $idpenerimaan);
        $this->db->join('tgudang_pemesanan_detail d','d.id = p.idpemesanandetail','left');
        return $this->db->get('tgudang_penerimaan_detail p')->result();
    }

    public function getNamaBarang($idtipe, $idbarang) {
        $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        $this->db->from($table[$idtipe]);
        $this->db->where('id', $idbarang);
        return $this->db->get()->row()->nama;
    }

    public function ajaxDetail($idpenerimaan, $idtipe, $idbarang) {
        $tipeBarang  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        $this->db->select('nama as namabarang');
        $this->db->from($tipeBarang[$idtipe]);
        $this->db->where('id', $idbarang);
        $data = $this->db->get()->row_array();
        $ajaxDetailJml = $this->ajaxDetailJml($idpenerimaan, $idtipe, $idbarang);
        $data = array_merge($data, $ajaxDetailJml);
        return $data;
    }

    public function ajaxDetailJml($idpenerimaan, $idtipe, $idbarang) {
        $query = "SELECT
		(SELECT kuantitas FROM tgudang_pemesanan_detail WHERE idpemesanan = trm.idpemesanan AND idbarang = ".$idbarang." AND idtipe = ".$idtipe.") jmlpesan, (SELECT SUM(kuantitas) FROM tgudang_penerimaan_detail WHERE idbarang = ".$idbarang." AND idtipe = ".$idtipe."
		AND idpenerimaan IN (SELECT id FROM tgudang_penerimaan WHERE idpemesanan = trm.idpemesanan) ) as jmltrm
		FROM tgudang_penerimaan trm
		INNER JOIN tgudang_penerimaan_detail trmdet ON trmdet.idpenerimaan = trm.id
		WHERE trmdet.idpenerimaan = ".$idpenerimaan;
        return $this->db->query($query)->row_array();
    }

    public function detailPenerimaan($idpenerimaan) {
        $this->db->select('trmdet.idbarang, trmdet.idtipe, trmdet.tanggalkadaluarsa');
        $this->db->where('trmdet.id', $idpenerimaan);
        // $this->db->join('tgudang_pemesanan psn', 'psn.id = trm.idpemesanan', 'left');
        // $this->db->join('tgudang_penerimaan_detail trmdet', 'trmdet.idpenerimaan = trm.id', 'left');
        return $this->db->get('tgudang_penerimaan_detail trmdet')->result();
    }

    public function edit_no_faktur() {
        $idpenerimaan = $this->input->post('idpenerimaan');
        $nofakturexternal = $this->input->post('nofakturexternal');
        $this->db->set('nofakturexternal', $nofakturexternal);
        $this->db->where('id', $idpenerimaan);
        $update = $this->db->update('tgudang_penerimaan');
        if ($update) {
            $data['status'] = 200;
            $data['msg'] = 'Nomor faktur berhasil diperbaharui';
        } else {
            $data['status'] = 401;
            $data['msg'] = 'Nomor faktur gagal diperbaharui';
        }

        $this->output->set_output(json_encode($data));
    }
	function find_detail($id){
		$q="SELECT T.*,(T.kuantitas - T.sudah_terima) as sisa,FORMAT(COALESCE((
			SELECT COALESCE(D1.diskon,0) as diskon from tgudang_penerimaan H1
			LEFT JOIN tgudang_penerimaan_detail D1 ON  H1.id=D1.idpenerimaan
			WHERE H1.iddistributor=T.iddistributor AND H1.status='1' AND D1.idbarang=T.idbarang AND D1.idtipe=T.idtipe
			ORDER BY H1.id DESC
			LIMIT 1
			),0),0) as diskon,((SELECT diskon) * T.harga_master /100) as nominal_dikson,T.harga_master-(SELECT nominal_dikson) as harga_after_diskon 
			,FORMAT((SELECT harga_after_diskon) * (SELECT sisa),0) as total_harga
			FROM (
			SELECT D.id,H.iddistributor,D.idpemesanan,B.namatipe,D.idtipe,D.idbarang,B.nama as namabarang,D.opsisatuan,(CASE WHEN D.opsisatuan='1' THEN 'KECIL' ELSE 'BESAR' END) as jenissatuan,D.kuantitas,
			(CASE WHEN D.opsisatuan='1' THEN sk.nama ELSE sb.nama END) as satuan,
			D.kuantitas_kecil,D.harga,D.harga_total,B.jumlahsatuanbesar,COALESCE((SELECT SUM(P.kuantitas) FROM tgudang_penerimaan_detail P WHERE P.idpemesanandetail=D.id AND P.`status`='1'),0) as sudah_terima
			,(CASE WHEN D.opsisatuan='1' THEN B.hargabeli ELSE B.hargabeli_besar END) as harga_master
			from tgudang_pemesanan_detail D
			LEFT JOIN tgudang_pemesanan H ON H.id=D.idpemesanan
			LEFT JOIN view_barang B ON B.idtipe=D.idtipe AND B.id=D.idbarang 
			LEFT JOIN msatuan sk ON sk.id=B.idsatuan
			LEFT JOIN msatuan sb on sb.id=B.idsatuanbesar
			
			WHERE D.idpemesanan='$id' AND D.`status`='1') T ";
		$query=$this->db->query($q);
		return $query->result_array();
	}
	function find_detail_user($id){
		$q="SELECT H.userpemesanan,H.tgl_pemesanan,nama_user_konfirmasi,tanggal_konfirmasi,nama_user_approval,tanggal_approval,nama_user_finalisasi,tgl_finalisasi from tgudang_pemesanan H 
			
			WHERE H.id='$id'";
		$query=$this->db->query($q);
		return $query->row_array();
	}
	function find_detail_user_batal($id){
		$q="SELECT H.nama_user_batal,H.datetime_batal from tgudang_penerimaan H 
			
			WHERE H.id='$id'";
		$query=$this->db->query($q);
		return $query->row_array();
	}
	function get_distributor(){
		$term = $this->input->post('search');
		
		$q="SELECT *from mdistributor WHERE nama LIKE '%".$term."%' ORDER BY nama";
			// print
		$query=$this->db->query($q);
		$res=$query->result();
        $this->output->set_output(json_encode($res));
	}
	function list_distributor(){
		// $term = $this->input->post('search');
		
		$q="SELECT mdistributor.id,mdistributor.nama
			FROM tgudang_penerimaan 
			LEFT JOIN mdistributor ON mdistributor.id=tgudang_penerimaan.iddistributor
			GROUP BY iddistributor ORDER BY mdistributor.nama ";
			// print
		$query=$this->db->query($q);
		$res=$query->result();
        return $res;
	}
	function list_lampiran($id){
		// $term = $this->input->post('search');
		
		$q="SELECT *from tgudang_penerimaan_lampiran 
			WHERE idpenerimaan='$id' ORDER BY id ASC";
			// print
		$query=$this->db->query($q);
		$res=$query->result();
        return $res;
	}
	function get_file_lampiran($id){
		$q="SELECT *from tgudang_penerimaan_lampiran 
			WHERE idpenerimaan='$id' ORDER BY id ASC";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/penerimaan/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">'.$r->user_upload.'-'.HumanDateLong($r->tanggal_upload).'</td>';
			$tabel .='<td class="text-left"><button class="btn btn-xs btn-danger hapus_file" title="Hapus"><li class="fa fa-trash-o"></li></button></td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
		}
		return $tabel;
	}
	function list_data($id){
		// $term = $this->input->post('search');
		
		$q="SELECT D.idpenerimaan,D.idbarang,D.idtipe,B.nama as namabarang,D.nobatch,H.tgl_terima,D.tanggalkadaluarsa,D.id,B.kode,D.opsisatuan,B.jumlahsatuanbesar,
		CASE WHEN D.opsisatuan ='2' THEN D.kuantitas * B.jumlahsatuanbesar ELSE D.kuantitas END kuantitas
			from tgudang_penerimaan_detail D 
			INNER JOIN tgudang_penerimaan H ON H.id=D.idpenerimaan
			LEFT JOIN view_barang B ON B.idtipe=D.idtipe AND B.id=D.idbarang
			WHERE D.idpenerimaan='$id' AND D.status='1'";
			// print
		$query=$this->db->query($q);
		$res=$query->result();
        return $res;
	}
	function detail_penerimaan($id){
		$q="SELECT B.namatipe,B.nama as nama_barang,D.harga,COALESCE(D.diskon,0) as diskon,totalharga,D.kuantitas,PD.kuantitas as pesan,
			(SELECT SUM(kuantitas) from tgudang_penerimaan_detail DD WHERE DD.idpemesanandetail=D.idpemesanandetail AND DD.status='1') as sudah_terima,D.nobatch,D.tanggalkadaluarsa,
			D.idtipe,D.idbarang,D.id,D.idpemesanandetail,D.ppn,D.nominalppn,D.nominaldiskon,D.opsisatuan,D.namasatuan,(CASE WHEN D.opsisatuan='1' THEN 'KECIL' ELSE 'BESAR' END) as jenissatuan,
			(CASE WHEN D.opsisatuan='1' THEN B.hargabeli ELSE B.hargabeli_besar END) as harga_master,D.namasatuan,D.`status`
			,D.harga_asli,D.harga_after_diskon,D.harga_after_ppn
			from tgudang_penerimaan_detail D
			LEFT JOIN tgudang_pemesanan_detail PD ON PD.id=D.idpemesanandetail
			LEFT JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe
			LEFT JOIN msatuan sk ON sk.id=B.idsatuan
			LEFT JOIN msatuan sb on sb.id=B.idsatuanbesar
			WHERE D.idpenerimaan='$id'";
		$query=$this->db->query($q);
		return $query->result();
	}
	public function save_pembayaran(){
		// print_r($this->input->post());exit();
		$e_iddet=$this->input->post('e_iddet');
		$head_bayar_id=$this->input->post('head_bayar_id');
		$idtransaksi = $this->input->post('id');
		if ($head_bayar_id==''){
			$head=array(
				'penerimaan_id'=>$idtransaksi,
				'tanggal_trx'=>date('Y-m-d H:i:s'),
				'iduser'=>$this->session->userdata('user_id'),
				'nama_user'=>$this->session->userdata('user_name'),
				'total_bayar'=>0,
			);
			$this->db->insert('tgudang_penerimaan_pembayaran_head',$head);
			$head_bayar_id= $this->db->insert_id();
		}
		
		$xjenis_kas_id= $_POST['xjenis_kas_id'];
		$xsumber_kas_id= $_POST['xsumber_kas_id'];
		$xidmetode= $_POST['xidmetode'];
		$ket= $_POST['ket'];
		$xnominal_bayar= $_POST['xnominal_bayar'];
		$xiddet= $_POST['xiddet'];
		$xstatus= $_POST['xstatus'];
		$xtanggal_pencairan= $_POST['xtanggal_pencairan'];
		
		foreach ($xjenis_kas_id as $index => $val){
			if ($xiddet[$index]!=''){
				$data_detail=array(
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'idmetode'=>$xidmetode[$index],
					'ket_pembayaran'=>$ket[$index],
					'tanggal_pencairan'=>YMDFormat($xtanggal_pencairan[$index]),
					'status'=>$xstatus[$index],
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);

				$this->db->where('id',$xiddet[$index]);
				$this->db->update('tgudang_penerimaan_pembayaran', $data_detail);
			}else{
				
				$data_detail=array(
					'head_bayar_id'=>$head_bayar_id,
					'penerimaan_id'=>$idtransaksi,
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'idmetode'=>$xidmetode[$index],
					'ket_pembayaran'=>$ket[$index],
					'tanggal_pencairan'=>YMDFormat($xtanggal_pencairan[$index]),
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->insert('tgudang_penerimaan_pembayaran', $data_detail);
				// print_r($data_detail);exit();
			}
		
			// $this->db->where(id,$idtransaksi);
			// $this->db->update('tgudang_penerimaan',array('st_trx'=>1));
		}
		foreach ($e_iddet as $index=>$val){
			if ($val!='0'){
				$this->db->where('id',$val);
				$this->db->update('tgudang_penerimaan_detail',array('st_trx'=>1,'head_bayar_id'=>$head_bayar_id));
			}
		}
		return true;
	}

	
}

/* End of file Tgudang_penerimaan_model.php */
/* Location: ./application/models/Tgudang_penerimaan_model.php */
