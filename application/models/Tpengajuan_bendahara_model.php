<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpengajuan_bendahara_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /* @function (new getindex pengajuan bendahara) &start */
    public function getIndexPengajuanBendahara($id)
    {
        $this->db->where(array(
            'status' => 1,
            'idbendahara' => $id
        ));

        // don't forget change name of view
        $this->db->group_by('id');
        $query = $this->db->get('zzpercobaan_bendahara');
        return $query->result();
    }
    /* @function (new getindex pengajuan bendahara) &end */

    public function getSpecified($id)
    {
        $this->db->select('tpengajuan.*,
        musers.name AS namapemohon,
        tpengajuan_persetujuan.memo,
        mvendor.id AS idvendor,
        mvendor.nama AS namavendor');
        $this->db->join('musers', 'musers.id = tpengajuan.idpemohon');
        $this->db->join('tpengajuan_persetujuan', 'tpengajuan_persetujuan.idpengajuan = tpengajuan.id', 'LEFT');
        $this->db->join('mvendor', 'mvendor.id = tpengajuan.idvendor');
        $this->db->where('tpengajuan.id', $id);
        $this->db->order_by('tpengajuan_persetujuan.id', 'DESC');
        $query = $this->db->get('tpengajuan');
        return $query->row();
    }

    public function getSatuan()
    {
        $this->db->where('status', 1);
        $query = $this->db->get('msatuan');
        return $query->result();
    }

    public function getDetailBarang($idpengajuan)
    {
        $this->db->select('tpengajuan_barang.*, msatuan.nama AS namasatuan');
        $this->db->join('msatuan', 'msatuan.id = tpengajuan_barang.idsatuan');
        $this->db->where('tpengajuan_barang.idpengajuan', $idpengajuan);
        $this->db->where('tpengajuan_barang.status', 1);
        $query = $this->db->get('tpengajuan_barang');
        return $query->result();
    }

    public function getDetailTerminProgress($idpengajuan)
    {
        $this->db->where('idpengajuan', $idpengajuan);
        $this->db->where('statusaktivasi1 <>', 0);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('tpengajuan_termin_progress');
        return $query->result();
    }

    public function getDetailTerminFix($idpengajuan)
    {
        $this->db->where('idpengajuan', $idpengajuan);
        $this->db->where('statusaktivasi1 <>', 0);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('tpengajuan_termin_fix');
        return $query->result();
    }

    public function getDetailLampiran($idpengajuan)
    {
        $this->db->where('tpengajuan_lampiran.idpengajuan', $idpengajuan);
        $this->db->where('tpengajuan_lampiran.status', 1);
        $query = $this->db->get('tpengajuan_lampiran');
        return $query->result();
    }

    public function checkTotalCicilan($id, $status = null)
    {
        # empty: , 1: , 2:
        if ($status !== null) {
            $this->db->where('statusaktivasi2', $status);
        }

        $this->db->where('idpengajuan', $id);
        $query = $this->db->get('tpengajuan_termin_fix');
        return $query->result();
    }

    public function checkTotalProgress($id, $status = null)
    {
        # empty: , 1: , 2:
        if ($status !== null) {
            $this->db->where('statusaktivasi2', $status);
        }

        $this->db->where('idpengajuan', $id);
        $query = $this->db->get('tpengajuan_termin_progress');
        return $query->result();
    }

    public function saveProses()
    {
        $sessionUserId = $this->session->userdata('user_id');
        $idpengajuan = $this->input->post('id');

        /* - KODE NYA NANTI DISINI YA ! - */

        if ($this->input->post('jenispembayaran') == 5) {
            $this->db->set('stproses', 7);
        } else {
            # status proses 5 artinya 'selesai'
            $this->db->set('stproses', 5);
        }
        /* ------------------------------ */

        // Update Status (Aktivasi Bendahara)
        $this->db->where('id', $idpengajuan);
        if ($this->db->update('tpengajuan')) {

            // Update Data Barang
            $detail_list = json_decode($this->input->post('detailValue'));
            foreach ($detail_list as $row) {
                $data = array();
                $data['id']          = $row[0];
                $data['idpengajuan'] = $idpengajuan;
                $data['namabarang']  = $row[1];
                $data['merk']        = $row[2];
                $data['kuantitas']   = $row[3];
                $data['idsatuan']    = $row[4];
                $data['harga']       = RemoveComma($row[6]);
                $data['jumlah']      = RemoveComma($row[7]);
                $data['keterangan']  = $row[8];

                $this->db->where('idpengajuan', $idpengajuan);
                $this->db->update('tpengajuan_barang', $data);
            }

            // ----------- Update Bendahara (Tpengajuan_aktivasi) ------------
            $this->db->where('idpengajuan', $idpengajuan);
            $this->db->set('status', 2);
            $this->db->update('tpengajuan_aktivasi');
            // ---------------------------------------------------------------

            // Update Data Termin Progress
            if ($this->input->post('jenispembayaran') == 4) {
                $termin_list = json_decode($this->input->post('terminValue'));
                foreach ($termin_list as $row) {
                    $data = array();
                    $data['id']          = $row[0];
                    $data['idpengajuan'] = $idpengajuan;
                    $data['tanggal']     = YMDFormat($row[1]);
                    $data['subjek']      = $row[2];
                    $data['deskripsi']   = $row[3];
                    $data['nominal']     = RemoveComma($row[4]);
                    $data['statusaktivasi1'] = $row[4];
                    $data['statusaktivasi2'] = $row[4];
                    $data['statusaktivasi3'] = 1;

                    $this->db->replace('tpengajuan_termin_progress', $data);
                }

                #status proses 7 artinya 'aktivasi lanjutan'
                $jumlah_cicilan = $this->checkTotalProgress($idpengajuan);
                $cicilan_terbayar = $this->checkTotalProgress($idpengajuan, 2);

                if ($jumlah_cicilan == $cicilan_terbayar) {
                    $this->db->set('stproses', 5);
                    $this->db->where('id', $idpengajuan);
                    $this->db->update('tpengajuan');
                }
            }

            // Update Data Termin Fix # change
            if ($this->input->post('jenispembayaran') == 5) {
                $terminfix_list = json_decode($this->input->post('terminFixValue'));
                foreach ($terminfix_list as $row) {
                    $data = array();
                    $data['id'] = $row[0];
                    $data['idpengajuan'] = $idpengajuan;
                    $data['deskripsi'] = $row[1];
                    $data['nominal'] = RemoveComma($row[2]);
                    $data['tanggal'] = YMDFormat($row[3]);
                    $data['statusaktivasi1'] = $row[4];
                    $data['statusaktivasi2'] = $row[4];
                    $data['statusaktivasi3'] = 1;

                    $this->db->replace('tpengajuan_termin_fix', $data);
                }

                #status proses 7 artinya 'aktivasi lanjutan'
                $jumlah_cicilan = $this->checkTotalCicilan($idpengajuan);
                $cicilan_terbayar = $this->checkTotalCicilan($idpengajuan, 2);

                if ($jumlah_cicilan == $cicilan_terbayar) {
                    $this->db->set('stproses', 5);
                    $this->db->where('id', $idpengajuan);
                    $this->db->update('tpengajuan');
                }
            }

            return true;
        }
    }

    /* for user_perintah */
    public function getUserPerintah($id)
    {
        $this->db->select('musers.name as nama');
        $this->db->join('musers', 'musers.id = tpengajuan_unitpemroses_detail.iduser', 'left');
        $this->db->where('tpengajuan_unitpemroses_detail.idunitpemroses', $id);
        $query = $this->db->get('tpengajuan_unitpemroses_detail');
        return $query->result();
    }
}
