<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mbagi_hasil_setting_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $q="SELECT 
			*
			from mbagi_hasil M
					
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	public function list_user($id,$step)
    {
        $q="SELECT *FROM musers M
				WHERE M.id NOT IN (SELECT iduser from mbagi_hasil_logic WHERE mbagi_hasil_logic.mbagi_hasil_id='$id' 
										AND mbagi_hasil_logic.step='$step' 
										AND mbagi_hasil_logic.status='1') AND M.status='1'";
		// print_r($q);
        return $this->db->query($q)->result();
    }
    public function saveData()
    {
        $this->nama 	= $_POST['nama'];		
        $this->bagian_rs = RemoveComma($_POST['bagian_rs']);
        $this->bagian_ps = RemoveComma($_POST['bagian_ps']);
        $this->tanggal_mulai = YMDFormat($_POST['tanggal_mulai']);
        $this->keterangan = ($_POST['keterangan']);
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		$this->status  = 1;
		$hasil=$this->db->insert('mbagi_hasil', $this);
        if ($hasil) {
			$id= $this->db->insert_id();
            return $id;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
		$this->nama 	= $_POST['nama'];		
        $this->bagian_rs = RemoveComma($_POST['bagian_rs']);
        $this->bagian_ps = RemoveComma($_POST['bagian_ps']);
        $this->tanggal_mulai = YMDFormat($_POST['tanggal_mulai']);
        $this->keterangan = ($_POST['keterangan']);
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		$this->status  = 1;

        if ($this->db->update('mbagi_hasil', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mbagi_hasil', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;		
		
        if ($this->db->update('mbagi_hasil', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	function refresh_image($id){
		$q="SELECT  H.* from mbagi_hasil_dokumen H

			WHERE H.idtransaksi='$id'";
		// print_r($q);exit();
		$row= $this->db->query($q)->result();
		$tabel='';
		$no=1;
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/mbagi_hasil/'.$r->filename.'" target="_blank">'.substr($r->filename, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->upload_by_nama.'-'.HumanDateLong($r->upload_date).'</td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/mbagi_hasil/'.$r->filename.'" target="_blank" data-toggle="tooltip" title="Preview" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                    <button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm " onclick="removeFile('.$r->id.')"><i class="fa fa-trash-o"></i></button></td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		return $tabel;
	}
}
