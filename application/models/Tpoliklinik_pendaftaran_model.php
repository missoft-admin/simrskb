<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpoliklinik_pendaftaran_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getPrintInfo($idpendaftaran)
	{
		$this->db->select('mfpasien.no_medrec AS nomedrec, COALESCE(mrekanan.nama, "-") AS namarekanan, mfpasien.nama AS namapasien, mfpasien.jenis_kelamin AS jeniskelamin,
				mfpasien.alamat_jalan AS alamat, mfpasien.tanggal_lahir AS tanggallahir,mfpasien.hp as telepon, mfpasien.pekerjaan_id, tpoliklinik_pendaftaran.umurtahun, tpoliklinik_pendaftaran.umurbulan, tpoliklinik_pendaftaran.umurhari,
				mfpasien.agama_id, mfpasien.golongan_darah AS golongandarah, mfpasien.status_kawin AS statuskawin, mpasien_kelompok.nama AS namakelompok, mpoliklinik.nama AS namapoliklinik, mdokter.nama AS namadokter,
				mpasien_asal.nama AS asalpasien, tpoliklinik_pendaftaran.tanggaldaftar,
				tpoliklinik_pendaftaran.noantrian, musers.name AS namauserinput,tpoliklinik_pendaftaran.tanggal,
				magama.nama AS namaagama, mpekerjaan.nama AS namapekerjaan,
				kab.nama as kab,kec.nama as kec,desa.nama as desa,mfpasien.title,tpoliklinik_pendaftaran.idpasien
				,tpoliklinik_pendaftaran.nopendaftaran,tpoliklinik_pendaftaran.namapenanggungjawab,tpoliklinik_pendaftaran.idtipe,tpoliklinik_pendaftaran.kode_antrian');
		$this->db->from('tpoliklinik_pendaftaran');
		$this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
		$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
		$this->db->join('mpasien_asal', 'mpasien_asal.id = tpoliklinik_pendaftaran.idasalpasien', 'LEFT');
		$this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
		$this->db->join('mdokter', 'mdokter.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
		$this->db->join('musers', 'musers.id = tpoliklinik_pendaftaran.iduserinput', 'LEFT');
		$this->db->join('magama', 'magama.id = mfpasien.agama_id', 'LEFT');
		$this->db->join('mpekerjaan', 'mpekerjaan.id = mfpasien.pekerjaan_id', 'LEFT');
		$this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
		$this->db->join('mfwilayah kab', 'kab.id = mfpasien.kabupaten_id', 'LEFT');
		$this->db->join('mfwilayah kec', 'kec.id = mfpasien.kecamatan_id', 'LEFT');
		$this->db->join('mfwilayah desa', 'desa.id = mfpasien.kecamatan_id', 'LEFT');
		$this->db->where('tpoliklinik_pendaftaran.id', $idpendaftaran);
		$query = $this->db->get();
		// print_r($this->db->last_query());exit();
		return $query->row();
	}

	public function getHistoryKunjungan($idpasien)
	{
		$this->db->select('mpoliklinik.nama AS poli,
				mpoliklinik.idtipe, mdokter.nama, tanggaldaftar, idpasien');
		$this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
		$this->db->join('mdokter', 'mdokter.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
		$this->db->where('tpoliklinik_pendaftaran.idpasien', $idpasien);
		$query = $this->db->get('tpoliklinik_pendaftaran');
		return $query->result();
	}

	public function list_riwayat($idpasien)
	{
		// $this->db->select('mpoliklinik.nama AS poli,
		// mpoliklinik.idtipe, mdokter.nama, tanggaldaftar, idpasien');
		// $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
		// $this->db->join('mdokter', 'mdokter.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
		// $this->db->where('tpoliklinik_pendaftaran.idpasien', $idpasien);
		// $query = $this->db->get('tpoliklinik_pendaftaran');
		// return $query->result();
		$q = "SELECT *FROM (
				SELECT
					P.tanggaldaftar,
					P.idpoliklinik,
					Pol.nama AS nama_poli,
					Dok.nama AS nama_dokter,
					T.diagnosa,
					T.keluhan,
					GROUP_CONCAT( MTL.nama SEPARATOR ', ' ) AS layanan,
					GROUP_CONCAT( O.nama SEPARATOR ', ' ) AS obat,
					GROUP_CONCAT( MA.nama SEPARATOR ', ' ) AS alkes,
					GROUP_CONCAT( TRF.nama SEPARATOR ', ' ) AS fisio,
					GROUP_CONCAT( TarifLab.nama SEPARATOR ', ' ) AS lab,
					GROUP_CONCAT( FarObat.nama SEPARATOR ', ' ) AS far,
					GROUP_CONCAT( TRad.nama SEPARATOR ', ' ) AS Rad
				FROM
					tpoliklinik_pendaftaran P

					LEFT JOIN mpoliklinik Pol ON Pol.id = P.idpoliklinik
					LEFT JOIN mdokter Dok ON Dok.id = P.iddokter
					LEFT JOIN tpoliklinik_tindakan T ON T.idpendaftaran = P.id
					LEFT JOIN tpoliklinik_pelayanan Pel ON Pel.idtindakan = T.id
					LEFT JOIN mtarif_rawatjalan MTL ON MTL.id = Pel.idpelayanan
					LEFT JOIN tpoliklinik_obat OT ON OT.idtindakan = T.id
					LEFT JOIN mdata_obat O ON O.id = OT.idobat
					LEFT JOIN tpoliklinik_alkes Alk ON Alk.idtindakan = T.id
					LEFT JOIN mdata_alkes MA ON MA.id = Alk.idalkes
					LEFT JOIN trujukan_fisioterapi RF ON RF.idtindakan = T.id AND RF.asalrujukan <> '3'
					LEFT JOIN trujukan_fisioterapi_detail RFD ON RFD.idrujukan = RF.id
					LEFT JOIN mtarif_fisioterapi TRF ON TRF.id = RFD.idfisioterapi
					LEFT JOIN trujukan_laboratorium TLab ON TLab.idtindakan = T.id AND TLab.asalrujukan <> '3'
					LEFT JOIN trujukan_laboratorium_detail TLabD ON TLabD.idrujukan = TLab.id
					LEFT JOIN mtarif_laboratorium TarifLab ON TarifLab.id = TLabD.idlaboratorium
					LEFT JOIN tpasien_penjualan Far ON Far.idtindakan = T.id AND Far.asalrujukan <> '3'
					LEFT JOIN tpasien_penjualan_nonracikan NonObat ON NonObat.idpenjualan = Far.id
					LEFT JOIN mdata_obat FarObat ON FarObat.id = NonObat.idbarang
					LEFT JOIN trujukan_radiologi Rad ON Rad.idtindakan=T.id AND Rad.asalrujukan <> '3'
					LEFT JOIN trujukan_radiologi_detail RadDet On RadDet.idrujukan=Rad.id
					LEFT JOIN mtarif_radiologi TRad ON TRad.id=RadDet.idradiologi

				WHERE
					P.idpasien = '$idpasien'
				GROUP BY
					P.id

					UNION

					SELECT
				R.tanggaldaftar,
				P.idpoliklinik,
				'Rawat Inap' as nama_poli,
				Dok.nama AS nama_dokter,
				'' as diagnosa,'' as keluhan,
				GROUP_CONCAT( MTL.nama SEPARATOR ', ' ) AS layanan,
				'' as obat,
				GROUP_CONCAT( MA.nama SEPARATOR ', ' ) AS alkes,
				GROUP_CONCAT( TRF.nama SEPARATOR ', ' ) AS fisio,
				GROUP_CONCAT( TarifLab.nama SEPARATOR ', ' ) AS lab,
				GROUP_CONCAT( FarObat.nama SEPARATOR ', ' ) AS far,
				GROUP_CONCAT( TRad.nama SEPARATOR ', ' ) AS Rad
				FROM trawatinap_pendaftaran R
					LEFT JOIN mdokter Dok ON Dok.id = R.iddokterpenanggungjawab
				LEFT JOIN tpoliklinik_pendaftaran P ON P.id=R.idpoliklinik
				LEFT JOIN mpoliklinik Pol ON Pol.id = P.idpoliklinik
				LEFT JOIN trawatinap_tindakan T ON T.idrawatinap=R.id
				LEFT JOIN mtarif_rawatinap MTL ON MTL.id=T.idpelayanan
				LEFT JOIN tpasien_penjualan Far ON Far.idtindakan=R.id AND Far.asalrujukan='3'
				LEFT JOIN tpasien_penjualan_nonracikan NonObat ON NonObat.idpenjualan = Far.id
				LEFT JOIN mdata_obat FarObat ON FarObat.id = NonObat.idbarang
				LEFT JOIN trawatinap_alkes Al on Al.idrawatinap=R.id
				LEFT JOIN mdata_alkes MA ON MA.id = Al.idalkes
				LEFT JOIN trujukan_fisioterapi RF ON RF.idtindakan = R.id AND RF.asalrujukan='3'
				LEFT JOIN trujukan_fisioterapi_detail RFD ON RFD.idrujukan = RF.id
				LEFT JOIN mtarif_fisioterapi TRF ON TRF.id = RFD.idfisioterapi
				LEFT JOIN trujukan_laboratorium TLab ON TLab.idtindakan = R.id AND TLab.asalrujukan='3'
				LEFT JOIN trujukan_laboratorium_detail TLabD ON TLabD.idrujukan = TLab.id
				LEFT JOIN mtarif_laboratorium TarifLab ON TarifLab.id = TLabD.idlaboratorium
				LEFT JOIN trujukan_radiologi Rad ON Rad.idtindakan=R.id AND Rad.asalrujukan='3'
				LEFT JOIN trujukan_radiologi_detail RadDet On RadDet.idrujukan=Rad.id
				LEFT JOIN mtarif_radiologi TRad ON TRad.id=RadDet.idradiologi
				where R.idpasien='$idpasien'
				GROUP BY R.id
				) D ORDER BY D.tanggaldaftar DESC
";
		// print_r($q);exit();
		$query = $this->db->query($q);
		return $query->result();
	}

	public function getSpecified($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('tpoliklinik_pendaftaran');
		return $query->row();
	}

	public function getKelompokPasien()
	{
		$this->db->where('status', 1);
		$query = $this->db->get('mpasien_kelompok');
		return $query->result();
	}

	public function getDokter()
	{
		$this->db->where('status', 1);
		$query = $this->db->get('mdokter');
		return $query->result();
	}

	public function get_pasien($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('mfpasien');
		$info = $query->row('inf_gabungmedrec');
		if ($info) {
			$arr = json_decode($info);
			$row = $arr->ke_pasien_id;

			$q = "SELECT mfpasien.*,(SELECT no_medrec from mfpasien where id='$id') as noid_lama from mfpasien
				where id='$row'";
		} else {
			$q = "SELECT mfpasien.*,'' as noid_lama from mfpasien
				where id='$id'";
		}
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_record_pasien($id, $row)
	{
		if ($row) {
			$q = "SELECT mfpasien.*,(SELECT no_medrec from mfpasien where id='$id') as noid_lama from mfpasien
				where id='$row'";
		} else {
			$q = "SELECT mfpasien.*,'' as noid_lama from mfpasien
				where id='$id'";
		}
		$query = $this->db->query($q);
		return $query->result();
	}

	public function getAlasan()
	{
		$this->db->where('status', 1);
		$query = $this->db->get('malasan_batal');
		return $query->result();
	}

	public function getLastKelompokPasien()
	{
		$this->db->select('LAST_INSERT_ID(idkelompokpasien) AS kelompokpasien');
		$this->db->limit('1');
		$query = $this->db->get('tpoliklinik_pendaftaran');

		$row = $query->row();
		if ($query->num_rows() > 0) {
			return $row->kelompokpasien;
		} else {
			return '';
		}
	}

	public function get_nomedrec()
	{
		$nomedrec = '';
		$init = 'H' . date('y');
		$query = $this->db->query('SELECT no_medrec FROM mfpasien WHERE no_medrec IS NOT NULL
									ORDER BY id DESC LIMIT 1');
		if ($row = $query->row()) {
			$hasil = $row->no_medrec;
			$panjang = strlen($hasil);
			$hasil = substr($hasil, 3, ($panjang - 3));
			$panjang = strlen($hasil);
			$hasil = $hasil + 1;
			$nomedrec = str_repeat('0', $panjang);
			$hasil = $init . substr($nomedrec, 0, $panjang - strlen($hasil)) . $hasil;
			// print_r($hasil);exit();
		} else {
			$hasil = $init . '000001';
		}
		// return $hasil;
		// print_r($hasil);exit();
		return $hasil;
	}

	public function saveData()
	{
		// print_r($this->input->post());exit();
		$nomedrec = ($this->input->post('no_medrec') == '' ? null : $this->input->post('no_medrec'));

		if ($this->input->post('idpasien')) {
			$idpasien = $this->input->post('idpasien');
			$statuspasienbaru = 0;
		} else {
			$idpasien = null;
			$statuspasienbaru = 1;
			$nomedrec = $this->get_nomedrec();
		}

		// Tanggal Lahir
		$tahun = $this->input->post('tahun_lahir');
		$bulan = $this->input->post('bulan_lahir');
		$hari = $this->input->post('tgl_lahir');
		$tanggallahir = date("$tahun-$bulan-$hari");

		$pasien = [];
		$pasien['id'] = $idpasien;
		$pasien['no_medrec'] = $nomedrec;
		$pasien['title'] = $this->input->post('title');
		$pasien['nama'] = $this->input->post('nama');
		$pasien['jenis_kelamin'] = $this->input->post('jeniskelamin');
		$pasien['alamat_jalan'] = $this->input->post('alamat');
		$pasien['provinsi_id'] = $this->input->post('provinsi');
		$pasien['kabupaten_id'] = $this->input->post('kabupaten');
		$pasien['kecamatan_id'] = $this->input->post('kecamatan');
		$pasien['kelurahan_id'] = $this->input->post('kelurahan');
		$pasien['kodepos'] = $this->input->post('kodepos');
		$pasien['jenis_id'] = $this->input->post('jenisidentitas');
		$pasien['ktp'] = $this->input->post('noidentitas');
		$pasien['hp'] = $this->input->post('nohp');
		$pasien['telepon'] = $this->input->post('telprumah');
		$pasien['email'] = $this->input->post('email');
		$pasien['tempat_lahir'] = $this->input->post('tempatlahir');
		$pasien['tanggal_lahir'] = $tanggallahir;
		$pasien['umur_tahun'] = $this->input->post('umurtahun');
		$pasien['umur_bulan'] = $this->input->post('umurbulan');
		$pasien['umur_hari'] = $this->input->post('umurhari');
		$pasien['golongan_darah'] = $this->input->post('golongandarah');
		$pasien['agama_id'] = $this->input->post('agama');
		$pasien['warganegara'] = $this->input->post('kewarganegaraan');
		$pasien['suku'] = $this->input->post('suku');
		$pasien['status_kawin'] = $this->input->post('statuskawin');
		$pasien['pendidikan_id'] = $this->input->post('pendidikan');
		$pasien['pekerjaan_id'] = $this->input->post('pekerjaan');
		$pasien['catatan'] = $this->input->post('catatan');
		$pasien['nama_keluarga'] = $this->input->post('namapenanggungjawab');
		$pasien['hubungan_dengan_pasien'] = $this->input->post('hubungan');
		$pasien['alamat_keluarga'] = $this->input->post('alamatpenanggungjawab');
		$pasien['telepon_keluarga'] = $this->input->post('telepon');
		$pasien['ktp_keluarga'] = $this->input->post('noidentitaspenanggung');

		if ($statuspasienbaru == 1) {
			$pasien['created_by'] = $this->session->userdata('user_id');
			$pasien['created'] = date('Y-m-d H:i:s');
		}

		if ($this->db->replace('mfpasien', $pasien)) {
			$idpasien = $this->db->insert_id();

			$pendaftaran = [];
			$pendaftaran['idtipe'] = $this->input->post('idtipe');
			$pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
			$pendaftaran['idpasien'] = $idpasien;
			$pendaftaran['idasalpasien'] = $this->input->post('asalpasien');
			$pendaftaran['idtipepasien'] = $this->input->post('idtipepasien');

			$pendaftaran['title'] = $this->input->post('title');
			$pendaftaran['no_medrec'] = $nomedrec;
			$pendaftaran['nohp'] = $this->input->post('nohp');
			$pendaftaran['telepon'] = $this->input->post('telprumah');
			$pendaftaran['namapasien'] = $this->input->post('nama');
			$pendaftaran['alamatpasien'] = $this->input->post('alamat');
			$pendaftaran['provinsi_id'] = $this->input->post('provinsi');
			$pendaftaran['kabupaten_id'] = $this->input->post('kabupaten');
			$pendaftaran['kecamatan_id'] = $this->input->post('kecamatan');
			$pendaftaran['kelurahan_id'] = $this->input->post('kelurahan');
			$pendaftaran['kodepos'] = $this->input->post('kodepos');
			$pendaftaran['tempat_lahir'] = $this->input->post('tempatlahir');
			$pendaftaran['tanggal_lahir'] = $tanggallahir;

			$pendaftaran['idrujukan'] = $this->input->post('rujukan');
			$pendaftaran['idjenispasien'] = $this->input->post('jenispasien');
			$pendaftaran['idkelompokpasien'] = $this->input->post('kelompokpasien');
			$pendaftaran['idrekanan'] = $this->input->post('rekanan');
			$pendaftaran['idtarifbpjskesehatan'] = $this->input->post('bpjskesehatan');
			$pendaftaran['idtarifbpjstenagakerja'] = $this->input->post('bpjstenagakerja');
			$pendaftaran['idkelompokpasien2'] = $this->input->post('kelompokpasien2');
			$pendaftaran['idrekanan2'] = $this->input->post('rekanan2');
			$pendaftaran['idtarifbpjskesehatan2'] = $this->input->post('bpjskesehatan2');
			$pendaftaran['idtarifbpjstenagakerja2'] = $this->input->post('bpjstenagakerja2');
			$pendaftaran['idpoliklinik'] = $this->input->post('poliklinik');
			$pendaftaran['idjenispertemuan'] = $this->input->post('pertemuan');
			$pendaftaran['iddokter'] = $this->input->post('iddokter');
			$pendaftaran['catatan'] = $this->input->post('catatan');
			$pendaftaran['namapenanggungjawab'] = $this->input->post('namapenanggungjawab');
			$pendaftaran['teleponpenanggungjawab'] = $this->input->post('telepon');
			$pendaftaran['noidentitaspenanggungjawab'] = $this->input->post('noidentitaspenanggung');
			$pendaftaran['umurhari'] = $this->input->post('umurhari');
			$pendaftaran['umurbulan'] = $this->input->post('umurbulan');
			$pendaftaran['umurtahun'] = $this->input->post('umurtahun');
			$pendaftaran['iduserinput'] = $this->session->userdata('user_id');
			$pendaftaran['statuspasienbaru'] = $statuspasienbaru;
			$pendaftaran['hubungan'] = $this->input->post('hubungan');
			$pendaftaran['created_by'] = $this->session->userdata('user_id');
			$pendaftaran['created_date'] = date('Y-m-d H:i:s');
			if ($this->db->insert('tpoliklinik_pendaftaran', $pendaftaran)) {
				return $this->db->insert_id();
			}
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function updateData($idpendaftaran)
	{
		$idpasien = ($this->input->post('idpasien') == '' ? null : $this->input->post('idpasien'));
		$nomedrec = ($this->input->post('no_medrec') == '' ? null : $this->input->post('no_medrec'));

		// Tanggal Lahir
		$tahun = $this->input->post('tahun_lahir');
		$bulan = $this->input->post('bulan_lahir');
		$hari = $this->input->post('tgl_lahir');
		$tanggallahir = date("$tahun-$bulan-$hari");

		$pasien = [];
		$pasien['no_medrec'] = $nomedrec;
		$pasien['title'] = $this->input->post('title');
		$pasien['nama'] = $this->input->post('nama');
		$pasien['jenis_kelamin'] = $this->input->post('jeniskelamin');
		$pasien['alamat_jalan'] = $this->input->post('alamat');
		$pasien['provinsi_id'] = $this->input->post('provinsi');
		$pasien['kabupaten_id'] = $this->input->post('kabupaten');
		$pasien['kecamatan_id'] = $this->input->post('kecamatan');
		$pasien['kelurahan_id'] = $this->input->post('kelurahan');
		$pasien['kodepos'] = $this->input->post('kodepos');
		$pasien['jenis_id'] = $this->input->post('jenisidentitas');
		$pasien['ktp'] = $this->input->post('noidentitas');
		$pasien['hp'] = $this->input->post('nohp');
		$pasien['telepon'] = $this->input->post('telprumah');
		$pasien['email'] = $this->input->post('email');
		$pasien['tempat_lahir'] = $this->input->post('tempatlahir');
		$pasien['tanggal_lahir'] = $tanggallahir;
		$pasien['umur_tahun'] = $this->input->post('umurtahun');
		$pasien['umur_bulan'] = $this->input->post('umurbulan');
		$pasien['umur_hari'] = $this->input->post('umurhari');
		$pasien['golongan_darah'] = $this->input->post('golongandarah');
		$pasien['agama_id'] = $this->input->post('agama');
		$pasien['warganegara'] = $this->input->post('kewarganegaraan');
		$pasien['suku'] = $this->input->post('suku');
		$pasien['status_kawin'] = $this->input->post('statuskawin');
		$pasien['pendidikan_id'] = $this->input->post('pendidikan');
		$pasien['pekerjaan_id'] = $this->input->post('pekerjaan');
		$pasien['catatan'] = $this->input->post('catatan');
		$pasien['nama_keluarga'] = $this->input->post('namapenanggungjawab');
		$pasien['hubungan_dengan_pasien'] = $this->input->post('hubungan');
		// $pasien['idhubungan'] 	= $this->input->post('hubungan');
		$pasien['alamat_keluarga'] = $this->input->post('alamatpenanggungjawab');
		$pasien['telepon_keluarga'] = $this->input->post('telepon');
		$pasien['ktp_keluarga'] = $this->input->post('noidentitaspenanggung');

		$pasien['modified'] = date('Y-m-d H:i:s');
		$pasien['modified_by'] = $this->session->userdata('user_id');

		$this->db->where('id', $idpasien);
		if ($this->db->update('mfpasien', $pasien)) {
			$pendaftaran = [];
			$pendaftaran['idtipe'] = $this->input->post('idtipe');
			$pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
			$pendaftaran['idpasien'] = $idpasien;
			$pendaftaran['idasalpasien'] = $this->input->post('asalpasien');
			$pendaftaran['idtipepasien'] = $this->input->post('idtipepasien');
			$pendaftaran['idrujukan'] = $this->input->post('rujukan');
			$pendaftaran['idjenispasien'] = $this->input->post('jenispasien');
			$pendaftaran['idkelompokpasien'] = $this->input->post('kelompokpasien');
			$pendaftaran['idrekanan'] = $this->input->post('rekanan');
			$pendaftaran['idtarifbpjskesehatan'] = $this->input->post('bpjskesehatan');
			$pendaftaran['idtarifbpjstenagakerja'] = $this->input->post('bpjstenagakerja');
			$pendaftaran['idkelompokpasien2'] = $this->input->post('kelompokpasien2');
			$pendaftaran['idrekanan2'] = $this->input->post('rekanan2');
			$pendaftaran['idtarifbpjskesehatan2'] = $this->input->post('bpjskesehatan2');
			$pendaftaran['idtarifbpjstenagakerja2'] = $this->input->post('bpjstenagakerja2');
			$pendaftaran['idpoliklinik'] = $this->input->post('poliklinik');
			$pendaftaran['idjenispertemuan'] = $this->input->post('pertemuan');
			$pendaftaran['iddokter'] = $this->input->post('iddokter');
			$pendaftaran['catatan'] = $this->input->post('catatan');
			$pendaftaran['namapenanggungjawab'] = $this->input->post('namapenanggungjawab');
			$pendaftaran['teleponpenanggungjawab'] = $this->input->post('telepon');
			$pendaftaran['noidentitaspenanggungjawab'] = $this->input->post('noidentitaspenanggung');
			$pendaftaran['umurhari'] = $this->input->post('umurhari');
			$pendaftaran['umurbulan'] = $this->input->post('umurbulan');
			$pendaftaran['umurtahun'] = $this->input->post('umurtahun');
			$pendaftaran['hubungan'] = $this->input->post('hubungan');

			$pendaftaran['no_medrec'] = $nomedrec;
			$pendaftaran['title'] = $this->input->post('title');
			$pendaftaran['nohp'] = $this->input->post('nohp');
			$pendaftaran['telepon'] = $this->input->post('telprumah');
			$pendaftaran['namapasien'] = $this->input->post('nama');
			$pendaftaran['alamatpasien'] = $this->input->post('alamat');
			$pendaftaran['provinsi_id'] = $this->input->post('provinsi');
			$pendaftaran['kabupaten_id'] = $this->input->post('kabupaten');
			$pendaftaran['kecamatan_id'] = $this->input->post('kecamatan');
			$pendaftaran['kelurahan_id'] = $this->input->post('kelurahan');
			$pendaftaran['kodepos'] = $this->input->post('kodepos');
			$pendaftaran['tempat_lahir'] = $this->input->post('tempatlahir');
			$pendaftaran['tanggal_lahir'] = $tanggallahir;

			// $pendaftaran['iduserinput'] 								= $this->session->userdata('user_id');

			$pendaftaran['edited_by'] = $this->session->userdata('user_id');
			$pendaftaran['edited_date'] = date('Y-m-d H:i:s');
			// print_r($pendaftaran);exit();
			$this->db->where('id', $idpendaftaran);
			if ($this->db->update('tpoliklinik_pendaftaran', $pendaftaran)) {
				return true;
			}
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function softDelete($keterangan, $id)
	{
		$data = [];
		$data['status'] = 0;
		if ($this->db->update('tpoliklinik_pendaftaran', $this, ['tanggaldaftar' => $id])) {
			$detail = [];
			$detail['keterangan'] = $keterangan;
			$detail['status'] = 0;
			if ($this->db->insert('malasan_batal', $detail)) {
				return true;
			}
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function get_duplicate_hariini($idpasien, $idpoliklinik, $iddokter, $hari, $bulan, $tahun)
	{
		$tanggal = "$tahun-$bulan-$hari";
		$q = "SELECT tpoliklinik_pendaftaran.id,tkasir.idtindakan FROM tpoliklinik_pendaftaran LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran=tpoliklinik_pendaftaran.id LEFT JOIN tkasir ON tkasir.idtipe IN (1,2) AND tkasir.idtindakan=tpoliklinik_tindakan.id WHERE tpoliklinik_pendaftaran.idpasien='$idpasien' AND tpoliklinik_pendaftaran.idpoliklinik='$idpoliklinik' AND tpoliklinik_pendaftaran.iddokter='$iddokter' AND DATE_FORMAT(tpoliklinik_pendaftaran.tanggaldaftar,'%Y-%m-%d')='$tanggal' AND tpoliklinik_pendaftaran.STATUS='1' AND (tkasir.STATUS IS NULL OR tkasir.STATUS !='2')";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getPoliklinik()
	{
		$this->db->where('status', 1);
		$query = $this->db->get('mpoliklinik');
		return $query->result();
	}
}
