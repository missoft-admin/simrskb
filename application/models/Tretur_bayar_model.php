<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tretur_bayar_model extends CI_Model
{
   public function list_distributor(){
	   $q="SELECT id,nama from mdistributor 
				WHERE mdistributor.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getHeader($id){
	   $q="SELECT * from tretur_penerimaan 
				WHERE tretur_penerimaan.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
	public function list_pembayaran($id) {
        // $this->db->where('id', $id);
        // $query = $this->db->get('tsetoran_kas_detail');
		$q="SELECT JK.nama as jenis_kas,SK.nama as sumber_kas,D.* FROM tretur_penerimaan_pembayaran D
		LEFT JOIN mjenis_kas JK ON JK.id=D.jenis_kas_id
		LEFT JOIN msumber_kas SK ON SK.id=D.sumber_kas_id
		WHERE D.idretur='$id' AND D.status='1'
		";
        return $this->db->query($q)->result();
    }
	public function saveData(){
		// print_r();exit();
		$id= $_POST['id'];
		$xjenis_kas_id= $_POST['xjenis_kas_id'];
		$xsumber_kas_id= $_POST['xsumber_kas_id'];
		$xidmetode= $_POST['xidmetode'];
		$ket= $_POST['ket'];
		$xnominal_bayar= $_POST['xnominal_bayar'];
		$xiddet= $_POST['xiddet'];
		$xstatus= $_POST['xstatus'];
		$xtanggal_pencairan= $_POST['xtanggal_pencairan'];
		foreach ($xjenis_kas_id as $index => $val){
			if ($xiddet[$index]!=''){
				$data_detail=array(
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'idmetode'=>$xidmetode[$index],
					'ket_pembayaran'=>$ket[$index],
					'tanggal_pencairan'=>YMDFormat($xtanggal_pencairan[$index]),
					'status'=>$xstatus[$index],
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);

				$this->db->where('id',$xiddet[$index]);
				$this->db->update('tretur_penerimaan_pembayaran', $data_detail);
			}else{
				$idtransaksi = $this->input->post('id');
				$data_detail=array(
					'idretur'=>$idtransaksi,
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'idmetode'=>$xidmetode[$index],
					'ket_pembayaran'=>$ket[$index],
					'tanggal_pencairan'=>YMDFormat($xtanggal_pencairan[$index]),
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->insert('tretur_penerimaan_pembayaran', $data_detail);
				// print_r($data_detail);exit();
			}
		}
		if ($this->input->post('btn_simpan') == '2') {
			$data =array(
				'st_trx'=>'1',
				'status'=>'2',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $id);
			$result=$this->db->update('tretur_penerimaan', $data);
		}else{
			$this->db->where('id',$id);
			$result=$this->db->update('tretur_penerimaan',array('st_trx'=>1));
		}
		
		return $result;
	}

		
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
