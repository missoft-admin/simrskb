<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_aps_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_assesmen_setting(){
		$q="SELECT * FROM setting_aps H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_label(){
		$q="SELECT * FROM setting_aps_keterangan";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_user(){
		$q="SELECT * FROM setting_aps_user H";
		return $this->db->query($q)->row_array();
	}

	function save_assesmen(){
		$id =1;
		$this->judul_header = $this->input->post('judul_header');
		$this->judul_header_eng = $this->input->post('judul_header_eng');
		$this->footer_eng = $this->input->post('footer_eng');
		$this->footer_ina = $this->input->post('footer_ina');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->lama_edit = $this->input->post('lama_edit');
		$this->orang_edit = $this->input->post('orang_edit');
		$this->st_hapus_catatan = $this->input->post('st_hapus_catatan');
		$this->lama_hapus = $this->input->post('lama_hapus');
		$this->orang_hapus = $this->input->post('orang_hapus');
		$this->st_duplikasi_catatan = $this->input->post('st_duplikasi_catatan');
		$this->lama_duplikasi = $this->input->post('lama_duplikasi');
		$this->orang_duplikasi = $this->input->post('orang_duplikasi');
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_aps', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_label(){
		$id =1;
		// print_r($this->input->post());exit;
		$this->paragraf_1_ina =$this->input->post('paragraf_1_ina');
		$this->paragraf_1_eng =$this->input->post('paragraf_1_eng');
		$this->pilih_ttd_ina =$this->input->post('pilih_ttd_ina');
		$this->pilih_ttd_eng =$this->input->post('pilih_ttd_eng');
		$this->nama_ttd_ina =$this->input->post('nama_ttd_ina');
		$this->nama_ttd_eng =$this->input->post('nama_ttd_eng');
		$this->ttl_ttd_ina =$this->input->post('ttl_ttd_ina');
		$this->ttl_ttd_eng =$this->input->post('ttl_ttd_eng');
		$this->umur_ttd_ina =$this->input->post('umur_ttd_ina');
		$this->umur_ttd_eng =$this->input->post('umur_ttd_eng');
		$this->alamat_ttd_ina =$this->input->post('alamat_ttd_ina');
		$this->alamat_ttd_eng =$this->input->post('alamat_ttd_eng');
		$this->hubungan_ina =$this->input->post('hubungan_ina');
		$this->hubungan_eng =$this->input->post('hubungan_eng');
		$this->atas_permintaan_ina =$this->input->post('atas_permintaan_ina');
		$this->atas_permintaan_eng =$this->input->post('atas_permintaan_eng');
		$this->terhadap_ina =$this->input->post('terhadap_ina');
		$this->terhadap_eng =$this->input->post('terhadap_eng');
		$this->noreg_ina =$this->input->post('noreg_ina');
		$this->noreg_eng =$this->input->post('noreg_eng');
		$this->nama_pasien_ina =$this->input->post('nama_pasien_ina');
		$this->nama_pasien_eng =$this->input->post('nama_pasien_eng');
		$this->nomedrec_ina =$this->input->post('nomedrec_ina');
		$this->nomedrec_eng =$this->input->post('nomedrec_eng');
		$this->ttl_pasien_ina =$this->input->post('ttl_pasien_ina');
		$this->ttl_pasien_eng =$this->input->post('ttl_pasien_eng');
		$this->umur_pasien_ina =$this->input->post('umur_pasien_ina');
		$this->umur_pasien_eng =$this->input->post('umur_pasien_eng');
		$this->jk_ina =$this->input->post('jk_ina');
		$this->jk_eng =$this->input->post('jk_eng');
		$this->paragraf_3_ina =$this->input->post('paragraf_3_ina');
		$this->paragraf_3_eng =$this->input->post('paragraf_3_eng');
		$this->paragraf_4_ina =$this->input->post('paragraf_4_ina');
		$this->paragraf_4_eng =$this->input->post('paragraf_4_eng');
		$this->yg_pernyataan_ina =$this->input->post('yg_pernyataan_ina');
		$this->yg_pernyataan_eng =$this->input->post('yg_pernyataan_eng');
		$this->saksi_rs_ina =$this->input->post('saksi_rs_ina');
		$this->saksi_rs_eng =$this->input->post('saksi_rs_eng');
		$this->saksi_kel_ina =$this->input->post('saksi_kel_ina');
		$this->saksi_kel_eng =$this->input->post('saksi_kel_eng');

		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		// $this->db->where('id', $id);
			
		if ($this->db->update('setting_aps_keterangan', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


