<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpegawai_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mpegawai');
        return $query->row();
    }

    public function getStatusAvailableApoteker()
    {
        $this->db->where('idkategori', 6);
        $query = $this->db->get('mpegawai');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function saveData()
    {
        $this->nip 				  	= $_POST['nip'];
		if ($_POST['nip_hrms']){
        $this->nip_hrms 				  	= $_POST['nip_hrms'];
			$this->nama_hrms 				  	= $_POST['nama'];			
		}
        $this->nama 					= $_POST['nama'];
        $this->jeniskelamin 	= $_POST['jeniskelamin'];
        $this->alamat 				= $_POST['alamat'];
        $this->tempatlahir 		= $_POST['tempatlahir'];
        $this->tanggallahir 	= YMDFormat($_POST['tanggallahir']);
        $this->telepon 				= $_POST['telepon'];
        $this->idkategori 		= $_POST['idkategori'];
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mpegawai', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nip 				  	= $_POST['nip'];
		if ($_POST['nip_hrms']){
        $this->nip_hrms 				  	= $_POST['nip_hrms'];
			$this->nama_hrms 				  	= $_POST['nama'];			
		}
        $this->nama 					= $_POST['nama'];
        $this->jeniskelamin 	= $_POST['jeniskelamin'];
        $this->alamat 				= $_POST['alamat'];
        $this->tempatlahir 		= $_POST['tempatlahir'];
        $this->tanggallahir 	= YMDFormat($_POST['tanggallahir']);
        $this->telepon 				= $_POST['telepon'];
        $this->idkategori 		= $_POST['idkategori'];
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mpegawai', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mpegawai', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
