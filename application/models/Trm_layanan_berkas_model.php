<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Trm_layanan_berkas_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

	public function poli(){
		$q="SELECT id,nama from mpoliklinik WHERE `STATUS`='1'";
		$query=$this->db->query($q);
		return $query->result();
	}
	public function list_dokter(){
		$q="SELECT id,nama from mdokter WHERE `STATUS`='1'";
		$query=$this->db->query($q);
		return $query->result();
	}
	public function getPrintInfo($id, $tujuan,$id_trx){
		// print_r($tujuan);exit();
		if ($tujuan=='1' || $tujuan=='2'){//POLI
			$q="SELECT CASE WHEN P.idtipe='1' THEN 'Rawat Jalan' ELSE 'IGD' END tipe_nama,P.no_medrec,
			P.namapasien,P.title,P.tanggal_lahir,P.idpoliklinik,M.nama as namapoliklinik, M.nama as asal_poli, 
				P.nopendaftaran,D.nama as namadokter,COALESCE(R.nama,'-')  as namarekanan,
				CASE WHEN P.idjenispertemuan='1' THEN 'Berdasarkan Janji' ELSE 'Tidak Berdasarkan Janji' END as janjian,
				K.nama as namakelompok,U.`name` as namauserinput,P.noantrian
				from tpoliklinik_pendaftaran P 
				LEFT JOIN mpoliklinik M ON M.id=P.idpoliklinik
				LEFT JOIN mdokter D ON D.id=P.iddokter
				LEFT JOIN mrekanan R ON R.id=P.idrekanan
				LEFT JOIN mpasien_kelompok K ON K.id=P.idkelompokpasien
				LEFT JOIN musers U ON U.id=P.iduserinput
				WHERE P.id='$id_trx'";
		}elseif($tujuan=='3' || $tujuan=='4'){//RI
			$q="SELECT CASE WHEN P.idtipe='1' THEN 'Rawat Inap' ELSE 'ODS' END tipe_nama,P.no_medrec,M.nama as asal_poli,
			P.namapasien,P.title,P.tanggal_lahir,P.idpoliklinik,CONCAT(kelas.nama,' - ',ruang.nama) as namapoliklinik, 
				P.nopendaftaran,D.nama as namadokter,COALESCE(R.nama,'-')  as namarekanan,
				'-' as janjian,
				K.nama as namakelompok,U.`name` as namauserinput,'-' as noantrian
				from trawatinap_pendaftaran P
				LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=P.idpoliklinik				
				LEFT JOIN mpoliklinik M ON M.id=TP.idpoliklinik
				LEFT JOIN mdokter D ON D.id=P.iddokterpenanggungjawab
				LEFT JOIN mrekanan R ON R.id=P.idrekanan
				LEFT JOIN mpasien_kelompok K ON K.id=P.idkelompokpasien
				LEFT JOIN musers U ON U.id=P.iduserinput
				LEFT JOIN mkelas kelas ON kelas.id=P.idkelas
				LEFT JOIN mbed ruang ON ruang.id=P.idbed
				WHERE P.id='$id_trx'";
		}else{
			$q="SELECT H.no_medrec as nomedrec,H.title,H.namapasien,H.tanggal_lahir as tanggallahir,H.catatan,
					CASE WHEN tipe_user_peminjam='1' THEN 'DOKTER' ELSE 'PEGAWAI' END as tipe_peminjam,
					CASE WHEN tipe_user_peminjam='1' THEN D.nama ELSE P.nama END as nama_peminjam,U.`name` as user_nama_trx
					FROM trm_layanan_berkas H
					LEFT JOIN mpegawai P ON P.id=H.user_peminjam_id
					LEFT JOIN mdokter D ON D.id=H.user_peminjam_id
					LEFT JOIN musers U ON U.id=H.user_trx
					WHERE H.id='$id'";
		}
		// print_r($tujuan);exit();
		$query=$this->db->query($q)->row_array();
		return $query;
	}
	
	//BATAS DELETE
	
}
