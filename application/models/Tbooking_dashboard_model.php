<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tbooking_dashboard_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function list_poli(){
		$q="SELECT MP.id,MP.nama FROM `mjadwal_dokter` H
			LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
			WHERE MP.`status`='1' AND H.kuota > 0
			GROUP BY H.idpoliklinik";
		return $this->db->query($q)->result();
	}
	function list_dokter(){
		$q="SELECT MP.id,MP.nama FROM `mjadwal_dokter` H
			LEFT JOIN mdokter MP ON MP.id=H.iddokter
			WHERE MP.`status`='1'
			GROUP BY H.iddokter
";
		return $this->db->query($q)->result();
	}
	function load_edit_poli($id){
		$q="SELECT 
			H.pendidikan_id as pendidikan,H.pekerjaan_id as pekerjaan
			,CONCAT(merm_hari.namahari,', ',DATE_FORMAT(H.tanggal,'%d-%m-%Y'),' Pukul : ',DATE_FORMAT(MJ.jam_dari,'%H:%i'),' - ',DATE_FORMAT(MJ.jam_sampai,'%H:%i')) as janji_temu
			,H.* 
			
			FROM `app_reservasi_tanggal_pasien` H
			LEFT JOIN mfpasien MP ON MP.id=H.idpasien
			LEFT JOIN merm_hari ON merm_hari.kodehari=H.kodehari
			LEFT JOIN mjadwal_dokter MJ ON MJ.id=H.jadwal_id
			WHERE H.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function get_gc($id){
		$q="SELECT app_id,logo as logo_gc,judul as judul_gc,sub_header as sub_header_gc,sub_header_side as sub_header_side_gc,footer_1 as footer_1_gc,footer_2 as footer_2_gc,ttd_1,ttd_2,footer_form_1,footer_form_2 FROM app_reservasi_tanggal_pasien_gc_head WHERE app_id='$id'";
		return $this->db->query($q)->row_array();
	}
	function get_sp($id){
		$q="SELECT logo as logo_sp,judul as judul_sp FROM `merm_skrining_pasien`";
		return $this->db->query($q)->row_array();
	}
	function get_sc($id){
		$q="SELECT logo as logo_sc,judul as judul_sc FROM `merm_skrining_covid`";
		return $this->db->query($q)->row_array();
	}
	function list_content_gc($id){
		$q="SELECT  * FROM app_reservasi_tanggal_pasien_gc WHERE app_id='$id'";
		return $this->db->query($q)->result();
	}
}
