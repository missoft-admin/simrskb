<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpengaturan_form_order_laboratorium_umum_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('merm_pengaturan_form_order_laboratorium_umum');
        return $query->row();
    }

    public function saveData()
    {
        $this->pesan_informasi_berhasil = $_POST['pesan_informasi_berhasil'];
        
        $this->label_judul = $_POST['label_judul'];
        $this->label_judul_eng = $_POST['label_judul_eng'];

        $this->label_tujuan_laboratorium = $_POST['label_tujuan_laboratorium'];
        $this->label_tujuan_laboratorium_eng = $_POST['label_tujuan_laboratorium_eng'];
        
        if (isset($_POST['required_tujuan_laboratorium'])) {
            $this->required_tujuan_laboratorium = $_POST['required_tujuan_laboratorium'];
        }
        
        $this->label_dokter_peminta_pemeriksaan = $_POST['label_dokter_peminta_pemeriksaan'];
        $this->label_dokter_peminta_pemeriksaan_eng = $_POST['label_dokter_peminta_pemeriksaan_eng'];
        if (isset($_POST['required_dokter_peminta_pemeriksaan'])) {
            $this->required_dokter_peminta_pemeriksaan = $_POST['required_dokter_peminta_pemeriksaan'];
        }
        
        $this->label_diagnosa = $_POST['label_diagnosa'];
        $this->label_diagnosa_eng = $_POST['label_diagnosa_eng'];
        if (isset($_POST['required_diagnosa'])) {
            $this->required_diagnosa = $_POST['required_diagnosa'];
        }
        
        $this->label_catatan_pemeriksaan = $_POST['label_catatan_pemeriksaan'];
        $this->label_catatan_pemeriksaan_eng = $_POST['label_catatan_pemeriksaan_eng'];
        if (isset($_POST['required_catatan_pemeriksaan'])) {
            $this->required_catatan_pemeriksaan = $_POST['required_catatan_pemeriksaan'];
        }
        
        $this->label_waktu_pemeriksaan = $_POST['label_waktu_pemeriksaan'];
        $this->label_waktu_pemeriksaan_eng = $_POST['label_waktu_pemeriksaan_eng'];
        if (isset($_POST['required_waktu_pemeriksaan'])) {
            $this->required_waktu_pemeriksaan = $_POST['required_waktu_pemeriksaan'];
        }
        
        $this->label_prioritas = $_POST['label_prioritas'];
        $this->label_prioritas_eng = $_POST['label_prioritas_eng'];
        if (isset($_POST['required_prioritas'])) {
            $this->required_prioritas = $_POST['required_prioritas'];
        }
        
        $this->label_pasien_puasa = $_POST['label_pasien_puasa'];
        $this->label_pasien_puasa_eng = $_POST['label_pasien_puasa_eng'];
        if (isset($_POST['required_pasien_puasa'])) {
            $this->required_pasien_puasa = $_POST['required_pasien_puasa'];
        }
        
        $this->label_pengiriman_hasil = $_POST['label_pengiriman_hasil'];
        $this->label_pengiriman_hasil_eng = $_POST['label_pengiriman_hasil_eng'];
        if (isset($_POST['required_pengiriman_hasil'])) {
            $this->required_pengiriman_hasil = $_POST['required_pengiriman_hasil'];
        }
        
        $this->label_catatan = $_POST['label_catatan'];
        $this->label_catatan_eng = $_POST['label_catatan_eng'];
        if (isset($_POST['required_catatan'])) {
            $this->required_catatan = $_POST['required_catatan}'];
        }

        $this->db->where('id', 1);
        if ($this->db->update('merm_pengaturan_form_order_laboratorium_umum', $this)) {
            return true;
        }

        return false;
    }
}
