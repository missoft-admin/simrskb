<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mrka_pengajuan_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
	public function get_default(){
		$user=$this->session->userdata('user_id');
		$q="SELECT unitpelayananiddefault from musers where id='$user'";
		$query=$this->db->query($q);
		$result= $query->row('unitpelayananiddefault');
		if ($result){
			return $result;
		}else{
			if ($result=='0'){
				return $result;
			}else{
			return '#';
				
			}
		}
	}
	public function get_default_limit(){
		$user=$this->session->userdata('user_id');
		$q="SELECT H.idunit FROM `munitpelayanan_user_setting` H WHERE H.userid='$user'";
		$query=$this->db->query($q);
		$result= $query->row('idunit');
		if ($result){
			return $result;
		}else{
			if ($result=='0'){
				return $result;
			}else{
				return '#';
				
			}
		}
	}
    public function getSpecified($id) {
		$q="SELECT H.*,M.st_berjenjang as status_berjenjang FROM rka_pengajuan H
			LEFT JOIN mjenis_pengajuan_rka M ON M.id=H.idjenis
			WHERE H.id='$id'";
        // $this->db->where('id', $id);
        // $query = $this->db->get('rka_pengajuan');
		$query=$this->db->query($q);
        return $query->row_array();
    }
	public function getSpecified_idrka_kegiatan($id) {
        $q="SELECT H.*,M.nama as nama_kegiatan 
			FROM mrka_kegiatan H
			LEFT JOIN mkegiatan_rka M ON M.id=H.idkegiatan
			WHERE H.id='$id'";
        return $this->db->query($q)->row();
    }
	public function load_detail_setting($idrka_kegiatan,$bulan,$tanggal_bayar='') {
		$where='';
		if ($tanggal_bayar){
			$where=" AND D.tanggal_bayar='$tanggal_bayar'";
		}
        $q="SELECT D.idrka_kegiatan,D.idrka,D.tanggal_bayar,D.idvendor
			,H.idunit,MK.nama
			FROM mrka_kegiatan_detail D
			LEFT JOIN mrka_kegiatan H ON H.id=D.idrka_kegiatan
			LEFT JOIN mkegiatan_rka MK ON MK.id=H.idkegiatan
			WHERE D.idrka_kegiatan='$idrka_kegiatan' AND D.bulan='$bulan' ".$where."
			GROUP BY D.idrka_kegiatan,D.bulan";
        return $this->db->query($q)->row();
    }
	public function load_detail($idrka_kegiatan,$bulan,$tanggal_bayar='') {
		$where='';
		if ($tanggal_bayar){
			$where=" AND D.tanggal_bayar='$tanggal_bayar'";
		}
        $q="SELECT D.*,D.harga_satuan as harga_pokok,0 as harga_diskon,0 as harga_ppn
			FROM mrka_kegiatan_detail D
			WHERE D.idrka_kegiatan='$idrka_kegiatan' AND D.bulan='$bulan' ".$where."
			";
        return $this->db->query($q)->result();
    }
	public function list_detail($id){
		$q="SELECT 
			A.harga_pokok as harga_pokok_asal,A.harga_ppn as harga_ppn_asal,A.harga_diskon as harga_diskon_asal,A.harga_satuan as harga_satuan_asal,A.total_harga as total_harga_asal
			,M.*
			FROM rka_pengajuan_detail M
			LEFT JOIN rka_pengajuan_detail_asal A ON A.id=M.id
			WHERE M.`status`='1' AND M.idpengajuan='$id'
			ORDER BY M.id ASC
			";
		return $this->db->query($q)->result();
	}
	public function list_rka(){
		$q="SELECT *from mrka
			WHERE `status`='3'";
		return $this->db->query($q)->result();
	}
	public function list_klasifikasi(){
		$q="SELECT *from mklasifikasi
			WHERE `status`='1'";
		return $this->db->query($q)->result();
	}
	public function list_prespektif(){
		$q="SELECT * from mprespektif_rka WHERE `status`='1'";
		return $this->db->query($q)->result();
	}
	public function list_cicilan($id,$jenis_cicilan){
		$q="SELECT *FROM rka_pengajuan_cicilan M
			WHERE M.`idpengajuan`='$id' AND M.jenis_cicilan='$jenis_cicilan'";
		return $this->db->query($q)->result();
	}
	public function list_dp_termin($id){
		$q="SELECT *FROM rka_pengajuan_termin M
			WHERE M.`idpengajuan`='$id'";
		return $this->db->query($q)->result();
	}
	public function list_jenis($tipe_rka='1'){
		$q="SELECT M.id,M.nama 
			FROM mlogic_detail L 
			LEFT JOIN mjenis_pengajuan_rka M ON M.id=L.idjenis
			WHERE L.tipe_rka='$tipe_rka' AND L.`status`='1' AND M.status='1'
			GROUP BY L.idjenis";
		return $this->db->query($q)->result();
	}
	public function list_jenis_all(){
		$q="SELECT M.id,M.nama 
			FROM mlogic_detail L 
			LEFT JOIN mjenis_pengajuan_rka M ON M.id=L.idjenis
			WHERE L.`status`='1'
			GROUP BY L.idjenis";
		return $this->db->query($q)->result();
	}
	public function list_vendor(){
		$q="SELECT *FROM mvendor M
			WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
    public function saveData() {
		$cara_pembayaran=$this->input->post('cara_pembayaran');
		$xnama_barang=$this->input->post('xnama_barang');
		$xmerk_barang=$this->input->post('xmerk_barang');
		$xkuantitas=$this->input->post('xkuantitas');
		$xsatuan=$this->input->post('xsatuan');
		$xharga_satuan=$this->input->post('xharga_satuan');
		$xtotal_harga=$this->input->post('xtotal_harga');
		$xketerangan=$this->input->post('xketerangan');
		$xstatus=$this->input->post('xstatus');
		
		$xharga_pokok=$this->input->post('xharga_pokok');
		$xharga_diskon=$this->input->post('xharga_diskon');
		$xharga_ppn=$this->input->post('xharga_ppn');
		$xidklasifikasi_det=$this->input->post('xidklasifikasi_det');
		
		$data_header=array(
			'idklasifikasi'=>$this->input->post('idklasifikasi'),
			'nama_pemohon'=>$this->input->post('nama_pemohon'),
			'tanggal_pengajuan'=>YMDFormat($this->input->post('tanggal_pengajuan')),
			'tanggal_dibutuhkan'=>YMDFormat($this->input->post('tanggal_dibutuhkan')),
			'bulan'=>$this->input->post('bulan'),
			'tipe_rka'=>$this->input->post('tipe_rka'),
			'idunit'=>$this->input->post('idunit'),
			'idunit_pengaju'=>$this->input->post('idunit_pengaju'),
			'catatan'=>$this->input->post('catatan'),
			'idvendor'=>$this->input->post('idvendor'),
			'idrka_kegiatan'=>$this->input->post('idrka_kegiatan'),
			'nama_kegiatan'=>$this->input->post('nama_kegiatan'),
			'grand_total'=> RemoveComma($this->input->post('grand_total')),
			'total_jenis_barang'=> RemoveComma($this->input->post('total_jenis_barang')),
			'total_item'=> RemoveComma($this->input->post('total_item')),
			'cara_pembayaran'=>$this->input->post('cara_pembayaran'),
			'jenis_pembayaran'=>$this->input->post('jenis_pembayaran'),			
			'total_dp_cicilan'=> RemoveComma($this->input->post('total_dp_cicilan')),
			'sisa_cicilan'=> RemoveComma($this->input->post('sisa_cicilan')),
			'jml_kali_cicilan'=> RemoveComma($this->input->post('jml_kali_cicilan')),
			'per_bulan_cicilan'=> RemoveComma($this->input->post('per_bulan_cicilan')),
			'sisa_termin'=> RemoveComma($this->input->post('sisa_termin')),
			'tanggal_kontrabon'=>($this->input->post('tanggal_kontrabon')?YMDFormat($this->input->post('tanggal_kontrabon')):null),
			'xtanggal_cicilan_pertama'=>($this->input->post('xtanggal_cicilan_pertama')?YMDFormat($this->input->post('xtanggal_cicilan_pertama')):null),
			'total_dp_termin'=> RemoveComma($this->input->post('total_dp_termin')),
			'idjenis'=>$this->input->post('idjenis'),
			'norek'=>$this->input->post('norek'),
			'bank'=>$this->input->post('bank'),
			'atasnama'=>$this->input->post('atasnama'),
			'keterangan_bank'=>$this->input->post('keterangan_bank'),
			'rekening_id'=>$this->input->post('rekening_id'),
			
			'created_by'=> $this->session->userdata('user_id'),
			'created_nama'=> $this->session->userdata('user_name'),
			'created_date'=>  date('Y-m-d H:i:s'),
		);
		
			if ($this->db->insert('rka_pengajuan', $data_header)) {
			   $iddet=$this->db->insert_id();
			   foreach($xnama_barang as $index => $val){
				  $data_detail=array(
					'idpengajuan'=>$iddet,
					'nama_barang'=>$xnama_barang[$index],
					'merk_barang'=>$xmerk_barang[$index],
					'kuantitas'=>$xkuantitas[$index],
					'satuan'=>$xsatuan[$index],
					'harga_satuan'=>$xharga_satuan[$index],
					'total_harga'=>$xtotal_harga[$index],
					'keterangan'=>$xketerangan[$index],
					'status'=>$xstatus[$index],
					
					'idklasifikasi_det'=>$xidklasifikasi_det[$index],
					'harga_pokok'=>$xharga_pokok[$index],
					'harga_diskon'=>$xharga_diskon[$index],
					'harga_ppn'=>$xharga_ppn[$index],
					
					'created_by'=> $this->session->userdata('user_id'),
					'created_nama'=> $this->session->userdata('user_name'),
					'created_date'=>  date('Y-m-d H:i:s'),
				  ); 
				  $this->db->insert('rka_pengajuan_detail', $data_detail);
			   }
			   
				// //CIICLAN
				// $nominal_cicilan=$this->input->post('nominal_cicilan');
				// $tanggal_cicilan=$this->input->post('tanggal_cicilan');
				// $keterangan_cicilan=$this->input->post('keterangan_cicilan');
				// $jenis_cicilan=$this->input->post('jenis_cicilan');
				// $this->db->where('idpengajuan',$_POST['id']);
				// $this->db->delete('rka_pengajuan_cicilan');
				// if ($nominal_cicilan){
					// foreach($nominal_cicilan as $index => $val){
						// $det_cicilan=array(
							// 'idpengajuan'=> $iddet,
							// 'jenis_cicilan'=> $jenis_cicilan[$index],
							// 'tanggal_cicilan'=> YMDFormat($tanggal_cicilan[$index]),
							// 'keterangan_cicilan'=> $keterangan_cicilan[$index],
							// 'nominal_cicilan'=>RemoveComma($nominal_cicilan[$index]),
						// );
						// $this->db->insert('rka_pengajuan_cicilan',$det_cicilan);
					// }
				// }
				
				//TERMIN			
				$jenis_termin=$this->input->post('jenis_termin');
				$tanggal_termin=$this->input->post('tanggal_termin');
				$judul_termin=$this->input->post('judul_termin');
				$deskripsi_termin=$this->input->post('deskripsi_termin');
				$nominal_termin=$this->input->post('nominal_termin');
				$status_dibayar_termin=$this->input->post('status_dibayar_termin');
				$this->db->where('idpengajuan',$_POST['id']);
				$this->db->delete('rka_pengajuan_termin');
				if ($jenis_termin){
					foreach($jenis_termin as $index => $val){
						$det_cicilan=array(
							'idpengajuan'=> $iddet,
							'jenis_termin'=> $jenis_termin[$index],
							'judul_termin'=> $judul_termin[$index],
							'deskripsi_termin'=> $deskripsi_termin[$index],
							'tanggal_termin'=> YMDFormat($tanggal_termin[$index]),
							'nominal_termin'=> RemoveComma($nominal_termin[$index]),
							'status_dibayar_termin'=> $status_dibayar_termin[$index],
						);
						$this->db->insert('rka_pengajuan_termin',$det_cicilan);
					}
				}
				return $iddet;
			} else {
				$this->error_message = "Penyimpanan Gagal";
				return false;
			}
		
    }
	
    public function updateData() {
       $data_header=array(
			'idklasifikasi'=>$this->input->post('idklasifikasi'),
			'nama_pemohon'=>$this->input->post('nama_pemohon'),
			'tanggal_pengajuan'=>YMDFormat($this->input->post('tanggal_pengajuan')),
			'tanggal_dibutuhkan'=>YMDFormat($this->input->post('tanggal_dibutuhkan')),
			'tipe_rka'=>$this->input->post('tipe_rka'),
			'idunit'=>$this->input->post('idunit'),
			'idunit_pengaju'=>$this->input->post('idunit_pengaju'),
			'catatan'=>$this->input->post('catatan'),
			'idvendor'=>$this->input->post('idvendor'),
			'idrka_kegiatan'=>$this->input->post('idrka_kegiatan'),
			'nama_kegiatan'=>$this->input->post('nama_kegiatan'),
			'grand_total'=> RemoveComma($this->input->post('grand_total')),
			'total_jenis_barang'=> RemoveComma($this->input->post('total_jenis_barang')),
			'total_item'=> RemoveComma($this->input->post('total_item')),
			'cara_pembayaran'=>$this->input->post('cara_pembayaran'),
			'jenis_pembayaran'=>$this->input->post('jenis_pembayaran'),			
			'total_dp_cicilan'=> RemoveComma($this->input->post('total_dp_cicilan')),
			'sisa_cicilan'=> RemoveComma($this->input->post('sisa_cicilan')),
			'jml_kali_cicilan'=> RemoveComma($this->input->post('jml_kali_cicilan')),
			'per_bulan_cicilan'=> RemoveComma($this->input->post('per_bulan_cicilan')),
			'sisa_termin'=> RemoveComma($this->input->post('sisa_termin')),
			'tanggal_kontrabon'=>($this->input->post('tanggal_kontrabon')?YMDFormat($this->input->post('tanggal_kontrabon')):null),
			'xtanggal_cicilan_pertama'=>($this->input->post('xtanggal_cicilan_pertama')?YMDFormat($this->input->post('xtanggal_cicilan_pertama')):null),
			'total_dp_termin'=> RemoveComma($this->input->post('total_dp_termin')),
			'idjenis'=>$this->input->post('idjenis'),
			'norek'=>$this->input->post('norek'),
			'bank'=>$this->input->post('bank'),
			'rekening_id'=>$this->input->post('rekening_id'),
			'atasnama'=>$this->input->post('atasnama'),
			'keterangan_bank'=>$this->input->post('keterangan_bank'),
			
			'edited_by'=> $this->session->userdata('user_id'),
			'edited_nama'=> $this->session->userdata('user_name'),
			'edited_date'=>  date('Y-m-d H:i:s'),
		);
		$xnama_barang=$this->input->post('xnama_barang');
		$xmerk_barang=$this->input->post('xmerk_barang');
		$xkuantitas=$this->input->post('xkuantitas');
		$xsatuan=$this->input->post('xsatuan');
		$xharga_satuan=$this->input->post('xharga_satuan');
		$xtotal_harga=$this->input->post('xtotal_harga');
		$xketerangan=$this->input->post('xketerangan');
		$xstatus=$this->input->post('xstatus');
		$xiddet=$this->input->post('xiddet');
		
		$xharga_pokok=$this->input->post('xharga_pokok');
		$xharga_diskon=$this->input->post('xharga_diskon');
		$xharga_ppn=$this->input->post('xharga_ppn');
		$xidklasifikasi_det=$this->input->post('xidklasifikasi_det');
		
		// //CIICLAN
		// $nominal_cicilan=$this->input->post('nominal_cicilan');
		// $tanggal_cicilan=$this->input->post('tanggal_cicilan');
		// $keterangan_cicilan=$this->input->post('keterangan_cicilan');
		// $jenis_cicilan=$this->input->post('jenis_cicilan');
		// $this->db->where('idpengajuan',$_POST['id']);
		// $this->db->delete('rka_pengajuan_cicilan');
		// if ($nominal_cicilan){
			// foreach($nominal_cicilan as $index => $val){
				// $det_cicilan=array(
					// 'idpengajuan'=> $_POST['id'],
					// 'jenis_cicilan'=> $jenis_cicilan[$index],
					// 'tanggal_cicilan'=> YMDFormat($tanggal_cicilan[$index]),
					// 'keterangan_cicilan'=> $keterangan_cicilan[$index],
					// 'nominal_cicilan'=> $nominal_cicilan[$index],
				// );
				// $this->db->insert('rka_pengajuan_cicilan',$det_cicilan);
			// }
		// }
		//TERMIN
		
		// $jenis_termin=$this->input->post('jenis_termin');
		// $tanggal_termin=$this->input->post('tanggal_termin');
		// $judul_termin=$this->input->post('judul_termin');
		// $deskripsi_termin=$this->input->post('deskripsi_termin');
		// $nominal_termin=$this->input->post('nominal_termin');
		// $status_dibayar_termin=$this->input->post('status_dibayar_termin');
		// $this->db->where('idpengajuan',$_POST['id']);
		// $this->db->delete('rka_pengajuan_termin');
		// foreach($jenis_termin as $index => $val){
			// $det_cicilan=array(
				// 'idpengajuan'=> $_POST['id'],
				// 'jenis_termin'=> $jenis_termin[$index],
				// 'judul_termin'=> $judul_termin[$index],
				// 'deskripsi_termin'=> $deskripsi_termin[$index],
				// 'tanggal_termin'=> YMDFormat($tanggal_termin[$index]),
				// 'nominal_termin'=> $nominal_termin[$index],
				// 'status_dibayar_termin'=> $status_dibayar_termin[$index],
			// );
			// $this->db->insert('rka_pengajuan_termin',$det_cicilan);
		// }
		
		// print_r($this->input->post());exit();
        if ($this->db->update('rka_pengajuan', $data_header, ['id' => $_POST['id']])) {
            foreach($xnama_barang as $index => $val){
			  $data_detail=array(
				'nama_barang'=>$xnama_barang[$index],
				'merk_barang'=>$xmerk_barang[$index],
				'kuantitas'=>$xkuantitas[$index],
				'satuan'=>$xsatuan[$index],
				'harga_satuan'=>$xharga_satuan[$index],
				'total_harga'=>$xtotal_harga[$index],
				'keterangan'=>$xketerangan[$index],
				'status'=>$xstatus[$index],
				
				'idklasifikasi_det'=>$xidklasifikasi_det[$index],
				'harga_pokok'=>$xharga_pokok[$index],
				'harga_diskon'=>$xharga_diskon[$index],
				'harga_ppn'=>$xharga_ppn[$index],
				
			  ); 
			  if ($xstatus[$index]=='0'){
				  $data_detail['deleted_by']=$this->session->userdata('user_id');
				  $data_detail['deleted_nama']=$this->session->userdata('user_name');
				  $data_detail['deleted_date']= date('Y-m-d H:i:s');
				  $data_detail['status']= 0;
			  }else{
				  $data_detail['edited_by']=$this->session->userdata('user_id');
				  $data_detail['edited_nama']=$this->session->userdata('user_name');
				  $data_detail['edited_date']= date('Y-m-d H:i:s');
			  }
			  if ($xiddet[$index]==''){
				  $data_detail['idpengajuan']= $_POST['id'];
				  // print_r($data_detail);exit();
				  $this->db->insert('rka_pengajuan_detail', $data_detail);
			  }else{
				 $this->db->where('id',$xiddet[$index]);
				$this->db->update('rka_pengajuan_detail', $data_detail);
			  }
			  
		   }
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function update_status($id,$status) {
		
		if ($status=='0'){
			$this->status  = $status;
			$this->deleted_by  = $this->session->userdata('user_id');
			$this->deleted_nama  = $this->session->userdata('user_name');
			$this->deleted_date  = date('Y-m-d H:i:s');
		}
		if ($status=='2'){
			$this->status  = $status;
			$this->publish_by  = $this->session->userdata('user_id');
			$this->publish_nama  = $this->session->userdata('user_name');
			$this->publish_date  = date('Y-m-d H:i:s');
		}
		if ($status=='3'){
			$this->status  = $status;
			$this->actived_by  = $this->session->userdata('user_id');
			$this->actived_nama  = $this->session->userdata('user_name');
			$this->actived_date  = date('Y-m-d H:i:s');
		}
		if ($status=='4'){
			$this->status  = $status;
			$this->selesai_by  = $this->session->userdata('user_id');
			$this->selesai_nama  = $this->session->userdata('user_name');
			$this->selesai_date  = date('Y-m-d H:i:s');
		}
       
        if ($this->db->update('rka_pengajuan', $this, ['id' => $id])) {
            
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function simpan_proses_peretujuan($id) {
		$q="SELECT rka_pengajuan.grand_total,U.`name` as user_nama, mlogic_detail.*  FROM rka_pengajuan 
			LEFT JOIN mlogic_unit ON  rka_pengajuan.idunit_pengaju = mlogic_unit.idunit
			LEFT JOIN mlogic_detail ON  mlogic_unit.idlogic = mlogic_detail.idlogic AND rka_pengajuan.tipe_rka = mlogic_detail.tipe_rka AND rka_pengajuan.idjenis = mlogic_detail.idjenis AND mlogic_detail.`status` = 1
			LEFT JOIN  musers U ON U.id=mlogic_detail.iduser
			WHERE rka_pengajuan.id = '$id' AND  calculate_logic(mlogic_detail.operand,rka_pengajuan.grand_total,mlogic_detail.nominal) = 1
			ORDER BY mlogic_detail.step,mlogic_detail.id";
		$list_user=$this->db->query($q)->result();
		$step=999;
		$idlogic='';
		foreach ($list_user as $row){
			if ($row->step < $step){
				$step=$row->step;
			}
			$data=array(
				'idrka' => $id,
				'step' => $row->step,
				'idlogic' => $row->idlogic,
				'iduser' => $row->iduser,
				'user_nama' => $row->user_nama,
				'proses_setuju' => $row->proses_setuju,
				'proses_tolak' => $row->proses_tolak,
				'approve' => 0,
			);
			$idlogic=$row->idlogic;
			$this->db->insert('rka_pengajuan_approval', $data);
		}
		
		$this->db->update('rka_pengajuan_approval',array('st_aktif'=>1),array('idrka'=>$id,'step'=>$step));
		
		$data=array(
			'idlogic' => $idlogic,
			'status' => '2',
			'proses_by' => $this->session->userdata('user_id'),
			'proses_nama' => $this->session->userdata('user_name'),
			'proses_date' => date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		return $this->db->update('rka_pengajuan', $data);
		
		
    }

    public function softDelete($id) {
        $this->status = 0;

        $this->deleted_by  = $this->session->userdata('user_id');
        $this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mrka', $this, ['id' => $id])) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    
}
