<?php

class Mcuti_dokter_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->select('mcuti_dokter.*, mdokter.nama AS namadokter');
        $this->db->join('mdokter', 'mdokter.id = mcuti_dokter.iddokter');
        $this->db->where('mcuti_dokter.id', $id);
        $query = $this->db->get('mcuti_dokter');
        return $query->row();
    }

    public function saveData()
    {
        $this->id = $_POST['id'];
        $this->iddokter = $_POST['iddokter'];
        $this->sebab = $_POST['sebab'];
        $this->tanggal_dari = YMDFormat($_POST['tanggal_dari']);
        $this->tanggal_sampai = YMDFormat($_POST['tanggal_sampai']);
        $this->alasan = $_POST['alasan'];

        if ($this->db->insert('mcuti_dokter', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->id = $_POST['id'];
        $this->iddokter = $_POST['iddokter'];
        $this->sebab = $_POST['sebab'];
        $this->tanggal_dari = YMDFormat($_POST['tanggal_dari']);
        $this->tanggal_sampai = YMDFormat($_POST['tanggal_sampai']);
        $this->alasan = $_POST['alasan'];

        if ($this->db->update('mcuti_dokter', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('mcuti_dokter', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
