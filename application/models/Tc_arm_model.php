<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tc_arm_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
    }

    public function changeStatusCarm($id, $status)
    {
        $this->db->set('status', $status);

        $this->db->where('id', $id);
        $this->db->update('mcarm');

        return true;
    }

    // @here
    public function changeTransaction($id)
    {
        $get = $this->getCarm($id);
        $tarif = explode(",", $get->tarif_carm);

        for ($iter = 0; $iter < count($tarif); $iter++) {
            $this->db->or_where('id', $tarif[$iter]);
        }



        $this->db->set('statuscarm', 1);
        $this->db->update('tkamaroperasi_sewaalat');

        return true;
    }
    // @here

    public function deleteCarm($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('mcarm');

        $this->deleteCarmDetail($id);
        return true;
    }

    public function deleteCarmDetail($id)
    {
        $this->db->where('id_carm', $id);
        $this->db->delete('mcarm_detail');
    }

    public function deleteTransactionDetail($id)
    {
        $this->db->where('id_transaksi', $id);
        $this->db->delete('tcarm_detail');
    }

    public function getCarm($id = null)
    {
        if ($id != null) {
            $this->db->where('id', $id);
        }

        $query = $this->db->get('mcarm');
        return $query->row();
    }

    public function getCarmView($id = null)
    {
        if ($id != null) {
            $this->db->where('id', $id);
        }

        $query = $this->db->get('view_index_mcarm');
        return $query->result();
    }

    public function getCarmDetail($id)
    {
        $this->db->where('id_carm', $id);
        $query = $this->db->get('mcarm_detail');
        return $query->result();
    }

    public function getCarmId()
    {
        $this->db->select('id, nama, idpemilik')
             ->where('status', 1);
        $query = $this->db->get('mtarif_operasi_sewaalat');
        return $query->result();
    }







    // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    // @checklist here
    public function getDetailInMonths($id, $year, $month, $idt)
    {
        $get = $this->getCarm($id);

        $tarif = explode(",", $get->tarif_carm);

        if ($idt == "") {
            for ($iter = 0; $iter < count($tarif); $iter++) {
                $this->db->or_where('id', $tarif[$iter]);
                $this->db->where('kondisi >=', $get->tanggal_berlaku);
                $this->db->where('tanggal_tutup', null);
                // $this->db->where('statusverifikasi', 1);
            }
        } else {
            $get_trans = $this->getTransactionInMonths($idt);

            for ($iter = 0; $iter < count($tarif); $iter++) {
                $this->db->or_where('id', $tarif[$iter]);
                // $this->db->where('statuscarm', 2);
                // $this->db->where('statuscarm', 1);
                $this->db->where('tanggal_tutup', $get_trans[$iter]->tanggal_tutup);
                // $this->db->where('statusverifikasi', 1);
            }
        }

        $query = $this->db->get('view_carm_periodday_inamonths');
        return $query->result();
    }

    // @ ----------------------------------------------------
    public function getPaymentsMonths($id, $year, $status)
    {
        $this->db->select('periodebulan, periodetahun, statuspayment')
             ->where(['id_carm' => $id, 'periodetahun' => $year]);

        // if ($status == null) {
        //   // @change to null
        //   $this->db->where('statuspayment', 0);
        // }

        $query = $this->db->get('tcarm');
        return $query->result();
    }
    // @ ----------------------------------------------------

    // @addition &start
    public function getTransactionInMonths($id = null, $periode = null)
    {
        $this->db->where('id', $id);

        $query = $this->db->get('tcarm');
        return $query->result();
    }
    // @addition &end


    // (DONE) 100%
    // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@










    public function getJenis($id)
    {
        $this->db->where('id', $id);

        $query = $this->db->get('mjenis_operasi');
        return $query->result();
    }

    public function getPayments($id = null)
    {
        if ($id != null) {
            $this->db->where('id', $id);
        }

        $query = $this->db->get('tcarm_payments');
        return $query->result();
    }

    public function getPaymentsB($id)
    {
        $this->db->where('id', $id);

        $query = $this->db->get('view_payments');
        return $query->result();
    }

    public function getPaymentsDetail($year, $months)
    {
        $month = explode('%20', $months);

        $this->db->where('periodetahun', $year);

        $condition = "";
        for ($i = 0; $i < count($month); $i++) {
            if ((count($month) - 1) == $i) {
                $condition .= 'periodebulan = '."'".$month[$i]."'";
            } else {
                $condition .= 'periodebulan = '."'".$month[$i]."'".' OR ';
            }
        }

        $this->db->where($condition);

        $query = $this->db->get('view_paymentsdetail');
        return $query->result();

        // return count($month);
    }

    public function getPaymentsPS($id)
    {
        $get = $this->getPayments($id);

        $this->db->select('mcarm_detail.id, mcarm_detail.lembaran, tcarm_payments_detail.statuskirim, tcarm_payments_detail.tanggal, mpemilik_saham.id, mpemilik_saham.nama_pemilik, mpemilik_saham.bank_pemilik as namabank, mpemilik_saham.norek_pemilik as norekening');
        $this->db->join('mcarm_detail', 'mcarm_detail.id = mpemilik_saham.idkelompok', 'left');
        $this->db->join('tcarm_payments_detail', 'tcarm_payments_detail.id_pemilik = mpemilik_saham.idkelompok', 'left');
        $this->db->where('id_carm', $get[0]->id_carm);

        $query = $this->db->get('mpemilik_saham');
        return $query->result();
    }

    public function getPemilikSaham()
    {
        $this->db->where('status', 1);

        $query = $this->db->get('mpemilik_saham');
        return $query->result();
    }

    public function getPeriodInYears($id)
    {
        $get = $this->getCarm($id);
        $tarif = explode(",", $get->tarif_carm);

        $this->db->select('id, periode, SUM(totalpendapatan) AS totalpendapatan, status, id_carm, idpemilik');

        // $this->db->where('statusverifikasi', 1);
        $this->db->where_in('id', $tarif);
        // $this->db->group_start();
        // for ($iter = 0; $iter < count($tarif); $iter++) {
        //     $this->db->or_where('id', $tarif[$iter]);
        // }
        // $this->db->group_end();

        $this->db->group_by('periode, idpemilik');
        $query = $this->db->get('view_carm_periodyears');
        // print_r($this->db->last_query());exit();
        return $query->result();
    }







    // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    public function getPeriodInMonths($id)
    {
        $get = $this->getCarm($id);
        $tarif = explode(",", $get->tarif_carm);

        $this->db->select('id, periode, tahun, SUM(totalpendapatan) as totalpendapatan, status, tanggal, tanggal_tutup, periodebulan, periodetahun, id_carm, idpemilik');

        $this->db->where_in('id', $tarif);
        $this->db->where('urutan >=', $get->tanggal_berlaku);
        // $this->db->where('statusverifikasi', 1);
        $this->db->order_by('id', 'DESC');

        $this->db->group_by('status');
        $this->db->group_by('tanggal_tutup');

        $query = $this->db->get('view_carm_periodmonths');

        // print_r($this->db->last_query());exit();
        return $query->result();
    }

    // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@










    public function getTransaction($id, $periode = null)
    {
        $this->db->where('id_carm', $id);
        $this->db->where('periode', $periode);

        $query = $this->db->get('tcarm');
        return $query->result();
    }

    public function getTransactionDetail($id)
    {
        $this->db->where('id_transaksi', $id);

        $query = $this->db->get('tcarm_detail');
        return $query->result();
    }

    public function getTransactionId($id)
    {
        $this->db->where('id', $id);

        $query = $this->db->get('tcarm');
        return $query->row();
    }

    public function giveIdBeforePs($id)
    {
        $this->db->where('idpemilik', $id);

        $this->db->set('idpemilik', null);
        $this->db->update('mtarif_operasi_sewaalat');
    }

    public function giveIdPemilik($tarif, $id)
    {
        $arr_tarif = explode(",", $tarif);

        $this->giveIdBeforePs($id);

        for ($iter = 0; $iter < count($arr_tarif); $iter++) {
            $this->db->where('id', $arr_tarif[$iter]);

            $this->db->set('idpemilik', $id);
            $this->db->update('mtarif_operasi_sewaalat');
        }
    }


    public function givePaymentsStatus($id, $months, $year)
    {
        $this->db->where('id_carm', $id);
        $this->db->where('periodetahun', $year);

        for ($iter = 0; $iter < count($months); $iter++) {
            $this->db->where('periodebulan', $months[$iter]);

            $this->db->set('statuspayment', 1);
            $this->db->update('tcarm');
        }

        return true;
    }

    public function delPaymentsMonths($id, $year)
    {
        $this->db->where('id_carm', $id);
        $this->db->where('periodetahun', $year);
        $this->db->where('statuspayment', 1);

        $this->db->set('statuspayment', 3);
        $this->db->update('tcarm');

        return true;
    }

    public function savePaymentsDetail($data, $id)
    {
        $this->deletePaymentsDetail($id);
        $detail = json_decode($data);

        foreach ($detail as $rows) {
            $this->db->set(
        array(
          'id_transaksi' => $id,
          'id_pemilik'   => $rows[11],
          'nama_pemilik' => $rows[0],
          'lembaran'     => $rows[1],
          'total'        => RemoveComma($rows[2]),
          'statuskirim'  => $rows[8],
          'norekening'   => $rows[10],
          'namabank'     => $rows[9],
          'tanggal'      => $rows[16],
        )
      );

            $this->db->insert('tcarm_payments_detail');
        }

        $this->giveStatusPayments($id);
        return true;
    }

    public function giveStatusPayments($id)
    {
        $this->db->where('id', $id);
        $this->db->set('status', 1);

        $this->db->update('tcarm_payments');
    }

    public function deletePaymentsDetail($id)
    {
        $this->db->where('id_transaksi', $id);
        $this->db->delete('tcarm_payments_detail');
    }

    // @action &start
    public function giveTransactionStatus($id, $detail_s)
    {
        $data   = explode(",", $detail_s);
        $result = $this->getTransactionId($id);

        for ($iter = 0; $iter < count($data); $iter++) {
            $this->db->where('idalat', $data[$iter]);
            $this->db->where('statuscarm', 0);

            $this->db->where('tanggal <=', date("Y-m-d H:i"));

            $this->db->set('statuscarm', 2);
            date_default_timezone_set('Asia/Jakarta');
            $this->db->set('tanggal_tutup', date("Y-m-d H:i"));
            $this->db->set('id_carm', $this->input->post('id-tarif'));
            $this->db->update('tkamaroperasi_sewaalat');
        }
    }
    // @action &end

    public function saveCarm($data, $detail, $id, $tarif)
    {
        if ($id == null) {
            // print_r('no');exit();
            $this->db->insert('mcarm', $data);
            $id = $this->db->insert_id();

            $this->giveIdPemilik($tarif, $id);
        } else {
            // print_r('yes');exit();
            $this->giveIdPemilik($tarif, $id);

            $this->db->set($data);

            $this->db->where('id', $id);
            $this->db->update('mcarm');

            $this->deleteCarmDetail($id);
        }

        $data = json_decode($detail);
        if (count($data) != 0) {
            if ($this->saveCarmDetail($data, $id)) {
                return true;
            }
        }

        return true;
    }

    public function saveCarmDetail($data, $id)
    {
        foreach ($data as $rows) {
            $this->db->set([
        'id_carm'  => $id,
        'nama'     => $rows[0],
        'lembaran' => $rows[1]
      ]);

            $this->db->insert('mcarm_detail');
        }

        return true;
    }

    public function savePayments($data, $id, $year, $months, $state)
    {
        if ($state == 0) {
            $this->db->insert('tcarm_payments', $data);
        } else {
            $this->delPaymentsMonths($id, $year);
            $this->db->where('id', $state);
            $this->db->update('tcarm_payments', $data);
        }

        $this->givePaymentsStatus($id, $months, $year);

        return true;
    }

    public function saveTransaction($data, $detail, $id, $detail_s, $action)
    {
        // ---------------------------------------------- @
        if ($id == null) {
            $this->db->insert('tcarm', $data);
            $id = $this->db->insert_id();

            // ---------------------------------------------- @
            if ($action == 1) {
                $this->giveTransactionStatus($id, $detail_s);
            }
            // ---------------------------------------------- @
        } else {

      // ---------------------------------------------- @
            if ($action == 1) {
                $this->giveTransactionStatus($id, $detail_s);
            }
            // ---------------------------------------------- @
            $this->db->set($data);

            $this->db->where('id', $id);
            $this->db->update('tcarm');
        }
        // ---------------------------------------------- @

        $data = json_decode($detail);
        if (count($data) != 0) {
            if ($this->saveTransactionDetail($data, $id)) {
                return true;
            }
        }

        return true;
    }

    public function saveTransactionDetail($data, $id)
    {
        $this->deleteTransactionDetail($id);

        foreach ($data as $rows) {
          $this->db->set([
            'id_transaksi'  => $id,
            'tanggal'       => $rows[0],
            'deskripsi'     => $rows[1],
            'total'         => RemoveComma($rows[2])
          ]);

          $this->db->insert('tcarm_detail');
        }

        return true;
    }
}
