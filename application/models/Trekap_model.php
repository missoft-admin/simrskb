<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trekap_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function insert_validasi_gaji($id){
		$q="SELECT '4' as id_reff,H.id as idtransaksi,H.periode as tanggal_transaksi,H.notransaksi
			,CONCAT('REKAP GAJI ',DATE_FORMAT(H.periode,'%d-%m-%Y'),' (',H.notransaksi,')') as keterangan,S.st_auto_posting
			,SUM(TV.nominal) as nominal,null as idakun,null as posisi_akun			
			 FROM trekap H
			 LEFT JOIN trekap_master TM ON TM.idrekap=H.id
			 LEFT JOIN trekap_master_variable TV ON TV.trekap_master_id=TM.id
			 LEFT JOIN msetting_jurnal_gaji S ON S.id='1'
			WHERE H.id='$id'
			GROUP BY H.id";
		$row=$this->db->query($q)->row();
		$data_header=array(
			'id_reff' =>4,
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'keterangan' => $row->keterangan,
			'nominal' => $row->nominal,
			'idakun' => $row->idakun,
			'posisi_akun' => $row->posisi_akun,
			'status' => 1,
			'st_auto_posting' => $row->st_auto_posting,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
			'created_date'=>date('Y-m-d H:i:s')
		);
		// print_r($data_header);exit();
		$st_auto_posting=$row->st_auto_posting;
		$this->db->insert('tvalidasi_kas',$data_header);
		$idvalidasi=$this->db->insert_id();
		
		$data_header=array(
			'idvalidasi' =>$idvalidasi,
			'idtransaksi' => $row->idtransaksi,
			'idrekap' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'periode' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'nominal' => $row->nominal,
			'idakun' => $row->idakun,
			'posisi_akun' => $row->posisi_akun,
			'status' =>'1',
			'st_posting' =>0,
		);
		$this->db->insert('tvalidasi_kas_04_gaji',$data_header);
		//VARIBALE TIPE 1 yang NULL ID AKUN
		$q="SELECT MV.tgaji_detail_id as iddet,H.id as idrekap,H.periode,MV.idjenis
			,MJ.nama as nama_jenis,H.notransaksi,MV.nama_variable,S.idakun,CONCAT(A.noakun,'-',A.namaakun) as akun
			,CASE WHEN MV.idtipe='1' THEN 'D' ELSE 'K' END as posisi_akun,MV.nominal
			FROM trekap H
			LEFT JOIN trekap_master M ON H.id=M.idrekap
			LEFT JOIN trekap_master_variable MV ON MV.trekap_master_id=M.id
			LEFT JOIN mjenis_gaji MJ ON MJ.id=MV.idjenis
			LEFT JOIN mjenis_gaji_setting S ON S.idjenis=MV.idjenis AND S.idvariable=MV.idvariable
			LEFT JOIN makun_nomor A ON A.id=S.idakun
			WHERE H.id='$id' AND M.tipe_bayar IN (1) AND MV.nominal > 0 AND S.idakun IS NOT NULL
			";
		$rows=$this->db->query($q)->result();
		foreach($rows as $r){
			$data_bayar=array(
				'idvalidasi' =>$idvalidasi,
				'iddet' =>$r->iddet,
				'idrekap' =>$r->idrekap,
				'periode' =>$r->periode,
				'idjenis' =>$r->idjenis,
				'nama_jenis' =>$r->nama_jenis,
				'notransaksi' =>$r->notransaksi,
				'nama_variable' =>$r->nama_variable,
				'nominal' =>$r->nominal,
				'idakun' =>$r->idakun,
				'posisi_akun' =>$r->posisi_akun
			);
			$this->db->insert('tvalidasi_kas_04_gaji_detail',$data_bayar);
		}
		//VARIBALE YANG TIDAK ADA DALAM REKAPAN
		$q="SELECT *FROM (
				SELECT DG.id as iddet,H.id as idrekap,H.periode,G.idjenis,MJ.nama as nama_jenis,H.notransaksi,S.idakun,CONCAT(A.noakun,'-',A.namaakun) as akun,DG.nama_variable
				,DG.nominal
				,CASE WHEN DG.idtipe='1' THEN 'D' ELSE 'K' END as posisi_akun
				,RM.id as rm_id
				FROM trekap H
				LEFT JOIN trekap_detail D ON D.idrekap=H.id
				LEFT JOIN tgaji G ON G.id=D.idgaji
				LEFT JOIN mjenis_gaji MJ ON MJ.id=G.idjenis
				LEFT JOIN tgaji_detail DG ON DG.idgaji=D.idgaji
				LEFT JOIN mjenis_gaji_setting S ON S.idjenis=G.idjenis AND S.idvariable=DG.idvariable
				LEFT JOIN makun_nomor A ON A.id=S.idakun
				LEFT JOIN trekap_master_variable RM ON RM.tgaji_detail_id=DG.id
				WHERE H.id='$id' AND RM.id IS NULL
				) T 

				ORDER BY T.idjenis,T.posisi_akun";
		$rows=$this->db->query($q)->result();
		foreach($rows as $r){
			$data_bayar=array(
				'idvalidasi' =>$idvalidasi,
				'iddet' =>$r->iddet,
				'idrekap' =>$r->idrekap,
				'periode' =>$r->periode,
				'idjenis' =>$r->idjenis,
				'nama_jenis' =>$r->nama_jenis,
				'notransaksi' =>$r->notransaksi,
				'nama_variable' =>$r->nama_variable,
				'nominal' =>$r->nominal,
				'idakun' =>$r->idakun,
				'posisi_akun' =>$r->posisi_akun
			);
			$this->db->insert('tvalidasi_kas_04_gaji_detail',$data_bayar);
		}
		
		//VARIBALE TIPE 2 DAN 3
		$q="SELECT MV.tgaji_detail_id as iddet,H.id as idrekap,H.periode,MV.idjenis
			,MJ.nama as nama_jenis,H.notransaksi,MV.nama_variable,S.idakun,CONCAT(A.noakun,'-',A.namaakun) as akun
			,CASE WHEN MV.idtipe='1' THEN 'D' ELSE 'K' END as posisi_akun,MV.nominal
			FROM trekap H
			LEFT JOIN trekap_master M ON H.id=M.idrekap
			LEFT JOIN trekap_master_variable MV ON MV.trekap_master_id=M.id
			LEFT JOIN mjenis_gaji MJ ON MJ.id=MV.idjenis
			LEFT JOIN mjenis_gaji_setting S ON S.idjenis=MV.idjenis AND S.idvariable=MV.idvariable
			LEFT JOIN makun_nomor A ON A.id=S.idakun
			WHERE H.id='$id' AND M.tipe_bayar IN (2,3) AND MV.nominal > 0
			";
		$rows=$this->db->query($q)->result();
		foreach($rows as $r){
			$data_bayar=array(
				'idvalidasi' =>$idvalidasi,
				'iddet' =>$r->iddet,
				'idrekap' =>$r->idrekap,
				'periode' =>$r->periode,
				'idjenis' =>$r->idjenis,
				'nama_jenis' =>$r->nama_jenis,
				'notransaksi' =>$r->notransaksi,
				'nama_variable' =>$r->nama_variable,
				'nominal' =>$r->nominal,
				'idakun' =>$r->idakun,
				'posisi_akun' =>$r->posisi_akun
			);
			$this->db->insert('tvalidasi_kas_04_gaji_detail',$data_bayar);
		}
		
		
		//PEMBAYARAN
		$q="SELECT B.id as idbayar_id,B.jenis_kas_id,J.nama jenis_kas_nama
			,B.sumber_kas_id,S.nama as sumber_nama
			,S.bank_id as bankid,mbank.nama as bank
			,B.idmetode,RM.metode_bayar as metode_nama,B.nominal_bayar,S.idakun,'K' as posisi_akun
			from trekap_master H
			INNER JOIN trekap_master_pembayaran B ON B.trekap_master_id=H.id
			LEFT JOIN ref_metode RM ON RM.id=B.idmetode
			LEFT JOIN mjenis_kas J ON J.id=B.jenis_kas_id
			LEFT JOIN msumber_kas S ON S.id=B.sumber_kas_id
			LEFT JOIN mbank ON mbank.id=S.bank_id
			WHERE H.idrekap='$id'";
		$rows=$this->db->query($q)->result();
		foreach($rows as $r){
			$data_bayar=array(
				'idvalidasi' =>$idvalidasi,
				'idbayar_id' =>$r->idbayar_id,
				'jenis_kas_id' =>$r->jenis_kas_id,
				'jenis_kas_nama' =>$r->jenis_kas_nama,
				'sumber_kas_id' =>$r->sumber_kas_id,
				'sumber_nama' =>$r->sumber_nama,
				'bank' =>$r->bank,
				'bankid' =>$r->bankid,
				'idmetode' =>$r->idmetode,
				'metode_nama' =>$r->metode_nama,
				'nominal_bayar' =>$r->nominal_bayar,
				'idakun' =>$r->idakun,
				'posisi_akun' =>$r->posisi_akun,

			);
			$this->db->insert('tvalidasi_kas_04_gaji_bayar',$data_bayar);
		}
		if ($st_auto_posting=='1'){
			$data_header=array(
				
				'st_posting' => 1,
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
				

			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_kas',$data_header);
		}
		
		//INSERT JURNAL PEMBALIK DENGAN NO JURNAL BARU
		$q="SELECT '4' as id_reff,H.id as idtransaksi,H.periode as tanggal_transaksi,H.notransaksi
			,CONCAT('REKAP GAJI PEMBALIK ',DATE_FORMAT(H.periode,'%d-%m-%Y'),' (',H.notransaksi,')') as keterangan,S.st_auto_posting
			,SUM(TM.total) as nominal,null as idakun,null as posisi_akun			
			 FROM trekap H
			 LEFT JOIN trekap_master TM ON TM.idrekap=H.id AND TM.tipe_bayar='3'
			
			 LEFT JOIN msetting_jurnal_gaji S ON S.id='1'
			WHERE H.id='$id'
			GROUP BY H.id";
		$row=$this->db->query($q)->row();
		$data_header=array(
			'id_reff' =>4,
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'keterangan' => $row->keterangan,
			'nominal' => $row->nominal,
			'idakun' => $row->idakun,
			'posisi_akun' => $row->posisi_akun,
			'status' => 1,
			'st_auto_posting' => $row->st_auto_posting,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
			'created_date'=>date('Y-m-d H:i:s')
		);
		// print_r($data_header);exit();
		$st_auto_posting=$row->st_auto_posting;
		$this->db->insert('tvalidasi_kas',$data_header);
		$idvalidasi=$this->db->insert_id();
		
		$data_header=array(
			'idvalidasi' =>$idvalidasi,
			'idtransaksi' => $row->idtransaksi,
			'idrekap' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'periode' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'nominal' => $row->nominal,
			'idakun' => $row->idakun,
			'posisi_akun' => $row->posisi_akun,
			'status' =>'1',
			'st_posting' =>0,
		);
		$this->db->insert('tvalidasi_kas_04_gaji',$data_header);
		
		//PEMBAYARAN PENDAPATAN
		$q="SELECT B.id as idbayar_id,B.jenis_kas_id,J.nama jenis_kas_nama
			,B.sumber_kas_id,S.nama as sumber_nama
			,S.bank_id as bankid,mbank.nama as bank
			,B.idmetode,RM.metode_bayar as metode_nama,B.nominal_bayar,S.idakun,'D' as posisi_akun
			from trekap_master H
			INNER JOIN trekap_master_pendapatan B ON B.trekap_master_id=H.id
			LEFT JOIN ref_metode RM ON RM.id=B.idmetode
			LEFT JOIN mjenis_kas J ON J.id=B.jenis_kas_id
			LEFT JOIN msumber_kas S ON S.id=B.sumber_kas_id
			LEFT JOIN mbank ON mbank.id=S.bank_id
			WHERE H.idrekap='$id'";
		$rows=$this->db->query($q)->result();
		foreach($rows as $r){
			$data_bayar=array(
				'idvalidasi' =>$idvalidasi,
				'idbayar_id' =>$r->idbayar_id,
				'jenis_kas_id' =>$r->jenis_kas_id,
				'jenis_kas_nama' =>$r->jenis_kas_nama,
				'sumber_kas_id' =>$r->sumber_kas_id,
				'sumber_nama' =>$r->sumber_nama,
				'bank' =>$r->bank,
				'bankid' =>$r->bankid,
				'idmetode' =>$r->idmetode,
				'metode_nama' =>$r->metode_nama,
				'nominal_bayar' =>$r->nominal_bayar,
				'idakun' =>$r->idakun,
				'posisi_akun' =>$r->posisi_akun,

			);
			$this->db->insert('tvalidasi_kas_04_gaji_bayar',$data_bayar);
		}
		//VARIBALE AKUN HEADER TIEP 3
		$q="SELECT MV.tgaji_detail_id as iddet,H.id as idrekap,H.periode,MV.idjenis
			,MJ.nama as nama_jenis,H.notransaksi,MV.nama_variable,M.idakun,CONCAT(A.noakun,'-',A.namaakun) as akun
			,'K' as posisi_akun,MV.nominal
			FROM trekap H
			LEFT JOIN trekap_master M ON H.id=M.idrekap
			LEFT JOIN trekap_master_variable MV ON MV.trekap_master_id=M.id
			LEFT JOIN mjenis_gaji MJ ON MJ.id=MV.idjenis
			LEFT JOIN makun_nomor A ON A.id=M.idakun
			WHERE H.id='$id' AND M.tipe_bayar IN (3) AND MV.nominal > 0";
		$rows=$this->db->query($q)->result();
		foreach($rows as $r){
			$data_bayar=array(
				'idvalidasi' =>$idvalidasi,
				'iddet' =>$r->iddet,
				'idrekap' =>$r->idrekap,
				'periode' =>$r->periode,
				'idjenis' =>$r->idjenis,
				'nama_jenis' =>$r->nama_jenis,
				'notransaksi' =>$r->notransaksi,
				'nama_variable' =>$r->nama_variable,
				'nominal' =>$r->nominal,
				'idakun' =>$r->idakun,
				'posisi_akun' =>$r->posisi_akun
			);
			$this->db->insert('tvalidasi_kas_04_gaji_detail',$data_bayar);
		}
		if ($st_auto_posting=='1'){
			$data_header=array(
				
				'st_posting' => 1,
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
				

			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_kas',$data_header);
		}
		return true;
		// print_r('BERAHSIL');
	}
    public function getSpecified($id)
    {
        $this->db->where('trekap.id', $id);
        $query = $this->db->get('trekap');
        return $query->row();
    }
	public function getSpecifiedHeader($id)
    {
        $this->db->where('trekap.id', $id);
        $query = $this->db->get('trekap');
        return $query->row_array();
    }
	public function getSpecifiedHeader2($id)
    {
        $q="SELECT H.*,SUM(RM.total) as total_rekap,COUNT(RM.id) as jml_detail,SUM(RM.st_verifikasi) as jml_verif,COUNT(DISTINCT Doc.id) as doc 
			FROM trekap H
			LEFT JOIN trekap_master RM ON RM.idrekap=H.id AND RM.`status`='1'
			LEFT JOIN trekap_dokumen Doc ON Doc.idtransaksi=H.id
			WHERE H.id='$id'
			GROUP BY H.id";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	public function getSpecified_master($id)
    {
        $this->db->where('trekap_master.id', $id);
        $query = $this->db->get('trekap_master');
        return $query->row();
    }
	public function list_rekening($id)
    {
       $q="SELECT R.id,R.norek,R.atas_nama,R.idbank,M.bank FROM  mrekap_rekening R
			LEFT JOIN ref_bank M ON M.id=R.idbank
			WHERE R.idrekap='$id'";
        return $this->db->query($q)->result();
    }
	function saveData(){
		$tipe_bayar=$this->input->post('tipe_bayar');
		// print_r($this->input->post());exit();
		if($this->input->post('btn_simpan')=='2'){
			$this->db->where('id',$this->input->post('id'));
			$this->db->update('trekap_master',array('st_verifikasi'=>'1','tipe_bayar'=>$tipe_bayar));
		}
		return true;
	}
	function list_jenis(){
		$this->db->select('mjenis_gaji.*, ');
        $this->db->where('mjenis_gaji.status', 1);
        $query = $this->db->get('mjenis_gaji');
        return $query->result();
	}
	function list_detail($id,$jenis){
		if ($jenis=='1'){
			$q="SELECT H.nama,H.total from trekap_master H
			WHERE H.idrekap='$id'
			ORDER BY H.id ASC";
		}else{
			$q="SELECT *FROM (
			SELECT LPAD(H.id,8,'0') as id,H.nama,H.total,'1' as st_header from trekap_master H
						WHERE H.idrekap='$id'
						

			UNION ALL

			SELECT CONCAT(LPAD(H.id,8,'0'),'-',LPAD(D.id,8,'0')) as id,D.nama_variable as nama,D.nominal as total,'0' as st_header
			from trekap_master H
			INNER JOIN trekap_master_variable D ON D.trekap_master_id=H.id
			WHERE H.idrekap='$id'
			) T ORDER BY T.id ASC
";
		}
        $query = $this->db->query($q);
        return $query->result();
	}
    public function getListUploadedDocument($idtransaksi)
    {
        $this->db->where('idtransaksi', $idtransaksi);
        $query = $this->db->get('trekap_dokumen');
        return $query->result();
    }
	function refresh_image($id){
		$q="SELECT  H.* from trekap_dokumen H

			WHERE H.idtransaksi='$id'";
		// print_r($q);exit();
		$row= $this->db->query($q)->result();
		$tabel='';
		$no=1;
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/rekap_gaji/'.$r->filename.'" target="_blank">'.$r->filename.'</a></td>';
			$tabel .='<td class="text-left">'.$r->keterangan.'</td>';
			$tabel .='<td class="text-left">'.$r->upload_by_nama.'-'.HumanDateLong($r->upload_date).'</td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/rekap_gaji/'.$r->filename.'" target="_blank" data-toggle="tooltip" title="Preview" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                    <a href="#" data-urlindex="'.base_url().'trekap/upload_document/'.$r->idtransaksi.'" data-urlremove="'.base_url().'trekap/delete_file/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a></td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		return $tabel;
	}
}
