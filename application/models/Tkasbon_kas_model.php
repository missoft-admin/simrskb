<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tkasbon_kas_model extends CI_Model
{
    public function simpan_proses_peretujuan($id) {

		$this->db->where('idkasbon',$id);
		$this->db->delete('tkasbon_approval');
		$q="SELECT S.iduser,S.step,U.`name` as user_nama,S.proses_setuju,S.proses_tolak
		FROM tkasbon TR
		LEFT JOIN mlogic_kasbon S ON TR.idtipe=S.idtipe
		LEFT JOIN musers U ON U.id=S.iduser
		WHERE TR.id='$id' AND calculate_logic(S.operand, TR.nominal, S.nominal)='1' AND S.status='1'
		ORDER BY S.step,S.id";
		$list_user=$this->db->query($q)->result();
		$step=999;
		$idlogic='';
		foreach ($list_user as $row){
			if ($row->step < $step){
				$step=$row->step;
			}
			$data=array(
				'idkasbon' => $id,
				'step' => $row->step,
				// 'idlogic' => $row->idlogic,
				'iduser' => $row->iduser,
				'user_nama' => $row->user_nama,
				'proses_setuju' => $row->proses_setuju,
				'proses_tolak' => $row->proses_tolak,
				'approve' => 0,
			);
			// $idlogic=$row->idlogic;
			$this->db->insert('tkasbon_approval', $data);
		}

		$this->db->update('tkasbon_approval',array('st_aktif'=>1),array('idkasbon'=>$id,'step'=>$step));

		$data=array(
			'st_transaksi' => '1',
			'st_approval' => '0',
			'stpencairan' => '0',
			'transaksi_by' => $this->session->userdata('user_id'),
			'transaksi_nama' => $this->session->userdata('user_name'),
			'transaksi_date' => date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		return $this->db->update('tkasbon', $data);


    }
	public function getListUploadedDocument($idkasbon)
    {
        $this->db->where('idkasbon', $idkasbon);
        $query = $this->db->get('tkasbon_dokumen');
        return $query->result();
    }
	function refresh_image($id){
		$q="SELECT  H.* from tkasbon_dokumen H

			WHERE H.idkasbon='$id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		$no=1;
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/kasbon/'.$r->filename.'" target="_blank">'.$r->filename.'</a></td>';
			$tabel .='<td class="text-left">'.$r->keterangan.'</td>';
			$tabel .='<td class="text-left">'.$r->upload_by_nama.'-'.HumanDateLong($r->upload_date).'</td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/kasbon/'.$r->filename.'" target="_blank" data-toggle="tooltip" title="Preview" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                    <a href="#" data-urlindex="'.base_url().'tkasbon_kas/upload_document/'.$r->idkasbon.'" data-urlremove="'.base_url().'tkasbon_kas/delete_file/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a></td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		return $tabel;
	}
	public function list_pembayaran($id) {
        // $this->db->where('id', $id);
        // $query = $this->db->get('tsetoran_kas_detail');
		$q="SELECT JK.nama as jenis_kas,SK.nama as sumber_kas,D.*,R.bank,M.norek,M.atas_nama FROM tkasbon_pembayaran D
		LEFT JOIN mjenis_kas JK ON JK.id=D.jenis_kas_id
		LEFT JOIN msumber_kas SK ON SK.id=D.sumber_kas_id
		LEFT JOIN mdokter_bank M ON M.id=D.rekening_id
		LEFT JOIN ref_bank R ON R.id=M.idbank
		WHERE D.idkasbon='$id' AND D.status='1'
		";
        return $this->db->query($q)->result();
    }
	public function saveData(){

		$xjenis_kas_id= $_POST['xjenis_kas_id'];
		$xsumber_kas_id= $_POST['xsumber_kas_id'];
		$xidmetode= $_POST['xidmetode'];
		$ket= $_POST['ket'];
		$xnominal_bayar= $_POST['xnominal_bayar'];
		$xiddet= $_POST['xiddet'];
		$xstatus= $_POST['xstatus'];
		$xtanggal_pencairan= $_POST['xtanggal_pencairan'];
		$xrekening_id= $_POST['xrekening_id'];
		foreach ($xjenis_kas_id as $index => $val){
			if ($xiddet[$index]!=''){
				$data_detail=array(
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'idmetode'=>$xidmetode[$index],
					'rekening_id'=>$xrekening_id[$index],
					'ket_pembayaran'=>$ket[$index],
					'tanggal_pencairan'=>YMDFormat($xtanggal_pencairan[$index]),
					'status'=>$xstatus[$index],
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);

				$this->db->where('id',$xiddet[$index]);
				$this->db->update('tkasbon_pembayaran', $data_detail);
			}else{
				$idtransaksi = $this->input->post('id');
				$data_detail=array(
					'idkasbon'=>$idtransaksi,
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'idmetode'=>$xidmetode[$index],
					'rekening_id'=>$xrekening_id[$index],
					'ket_pembayaran'=>$ket[$index],
					'tanggal_pencairan'=>YMDFormat($xtanggal_pencairan[$index]),
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->insert('tkasbon_pembayaran', $data_detail);
				// print_r($data_detail);exit();
			}
		}

		$this->stpencairan  = 1;
		// $this->metode  = $_POST['metode'];
		$this->bayar_by  = $this->session->userdata('user_id');
		$this->bayar_date  = date('Y-m-d H:i:s');
		if ($this->input->post('btn_simpan')=='2') {
			$this->st_verifikasi  = 1;
		}
		$this->db->where('id', $_POST['id']);
		if ($this->db->update('tkasbon', $this)) {
        // Status Verifikasi TRUE
        if ($this->input->post('btn_simpan') == '2') {
			$this->model->insert_validasi_kasbon($_POST['id']);
            $row = $this->db->query('SELECT * FROM tkasbon WHERE id = '.$_POST['id'])->row();

            // Proses Honor Dokter
            $idkasbon = $row->id;
            $tanggal = $row->tanggal;
            $idtipe = $row->idtipe;
            $iddokter = $row->idpegawai;
            $catatan = $row->catatan;
            $nominal = $row->nominal;

            if ($idtipe == '1') {
              $tanggal_pembayaran = YMDFormat(getPembayaranHonorDokter($iddokter));
              $tanggal_jatuhtempo = YMDFormat(getJatuhTempoHonorDokter($iddokter));

              $this->db->set('tanggal_pembayaran', $tanggal_pembayaran);
              $this->db->set('tanggal_jatuhtempo', $tanggal_jatuhtempo);
              $this->db->where('id', $idkasbon);
              $result = $this->db->update('tkasbon');

              if ($result) {

                // Proses Honor Dokter
                $isStopPeriode = $this->Thonor_dokter_model->checkStatusHonorDokterStopped($iddokter, $tanggal_pembayaran);
                if ($isStopPeriode == null) {
                  $idhonor = $this->Thonor_dokter_model->checkStatusHonorDokter($iddokter, $tanggal_pembayaran);

                  $dokter = $this->db->query("SELECT mdokter.id,
                      mdokter.nama,
                      mdokter_kategori.id AS idkategori,
                      mdokter_kategori.nama AS namakategori
                    FROM
                      mdokter
                    JOIN mdokter_kategori ON mdokter_kategori.id=mdokter.idkategori
                    WHERE
                      mdokter.id = $iddokter AND
                      mdokter.status = 1")->row();

                  if ($idhonor == null) {
                      $dataHonorDokter = array(
                        'iddokter' => $iddokter,
                        'namadokter' => $dokter->nama,
                        'tanggal_pembayaran' => $tanggal_pembayaran,
                        'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
                        'nominal' => 0,
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => $this->session->userdata('user_id')
                      );

                      if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
                          $idhonor = $this->db->insert_id();
                      }
                  }
                }

                $dataDetailHonorDokter = array(
                  'idhonor' => $idhonor,
                  'idtransaksi' => $idkasbon,
                  'jenis_transaksi' => 'pengeluaran',
                  'jenis_tindakan' => 'POTONGAN KASBON',
                  'reference_table' => 'tkasbon',
                  'namatarif' => $catatan,
                  'idkategori' => $dokter->idkategori,
                  'namakategori' => $dokter->namakategori,
                  'iddokter' => $iddokter,
                  'namadokter' => $dokter->nama,
                  'jasamedis' => $nominal,
                  'potongan_rs' => 0,
                  'nominal_potongan_rs' => 0,
                  'pajak_dokter' => 0,
                  'nominal_pajak_dokter' => 0,
                  'jasamedis_netto' => $nominal,
                  'tanggal_pemeriksaan' => $tanggal,
                  'tanggal_pembayaran' => $tanggal_pembayaran,
                  'tanggal_jatuhtempo' => $tanggal_jatuhtempo
                );

                $this->db->insert('thonor_dokter_detail', $dataDetailHonorDokter);

            		$this->output->set_output(json_encode($result));
              }
            }
        }

        return true;
    }
	}
    public function get_kasbon($id)
    {
        $this->db->select('tkasbon.*, (CASE WHEN tkasbon.idtipe = 1 THEN mdokter.nama ELSE mpegawai.nama END) AS namapegawai');
        $this->db->join('mdokter', 'mdokter.id = tkasbon.idpegawai AND tkasbon.idtipe = 1', 'LEFT');
        $this->db->join('mpegawai', 'mpegawai.id = tkasbon.idpegawai AND tkasbon.idtipe = 2', 'LEFT');
        $this->db->where('tkasbon.id', $id);
        $query = $this->db->get('tkasbon');
        return $query->row_array();
		// return $this->db->query($q)->row_array();
    }
	public function removeRefund($id, $alasan)
    {
        $this->db->where('id', $id);
        $this->db->update('tkasbon', ['alasan' => $alasan, 'status' => 0]);
    }

    public function saveRefundTransaksi($data, $detail)
    {
        $this->db->insert('tkasbon', $data);
        $insert_id = $this->db->insert_id();

        if ($this->saveRefundTransaksiDetail($detail, $insert_id)) {
            return true;
        }
    }

    public function saveRefundTransaksiDetail($detail, $id)
    {
        $data = json_decode($detail);

        foreach ($data as $rows) {
            $transaksi = explode('.', $rows[0]);

            $this->db->set(array(
                'idtransaksi'       => $transaksi[0],
                'idkasbon'          => $id,
                'tipetransaksi'     => $transaksi[1],
                'idpendaftaran'     => $transaksi[3],
                'idpelayanan'       => $transaksi[4],
                'idkelas'           => $transaksi[5],
                'idtipe'            => $transaksi[2],
                'tarif'             => $transaksi[6],
                'kuantitas'         => $transaksi[7],
                'totalkeseluruhan'  => $transaksi[8],
                'status'            => 1
            ));

            $this->db->insert('tkasbon_detail');
        }

        return true;
    }

    public function getInfoRefund($id)
    {
        $this->db->select('tkasbon.id,
          tkasbon.notransaksi,
          tkasbon.tanggal AS tanggaltransaksi,
          tkasbon.norefund,
          tkasbon.tanggal AS tanggalrefund,
          (CASE
            WHEN tkasbon.tipe = 0 THEN "Deposit"
            WHEN tkasbon.tipe = 1 THEN "Retur Obat"
            WHEN tkasbon.tipe = 2 THEN "Transaksi"
          END) AS tiperefund,
          mfpasien.no_medrec AS nomedrec,
          mfpasien.nama AS namapasien,
          mpasien_kelompok.nama AS kelompokpasien,
          tkasbon.totaltransaksi,
          tkasbon.totaldeposit,
          tkasbon.totalrefund,
          tkasbon.alasan,
          tkasbon.metode AS idmetode,
          (CASE
            WHEN tkasbon.metode = 1 THEN "Tunai"
            WHEN tkasbon.metode = 2 THEN "Debit"
            WHEN tkasbon.metode = 3 THEN "Kredit"
            WHEN tkasbon.metode = 4 THEN "Transfer"
          END) AS metode,
          tkasbon.bank,
          tkasbon.norekening');

        $this->db->join('mfpasien', 'mfpasien.id = tkasbon.idpasien', 'LEFT');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tkasbon.idkelompokpasien', 'LEFT');

        $this->db->where('tkasbon.id', $id);
        $query = $this->db->get('tkasbon');

        return $query->row();
    }

    public function getFakturRefundDeposit($id)
    {
        $this->db->select('trawatinap_deposit.id,
        DATE_FORMAT(trawatinap_deposit.tanggal, "%d/%m/%Y") AS tanggal,
        (CASE
          WHEN trawatinap_deposit.idmetodepembayaran = 1 THEN "Tunai"
          WHEN trawatinap_deposit.idmetodepembayaran = 2 THEN "Debit"
          WHEN trawatinap_deposit.idmetodepembayaran = 3 THEN "Kredit"
          WHEN trawatinap_deposit.idmetodepembayaran = 4 THEN "Transfer"
        END) AS metodepembayaran,
        trawatinap_deposit.nominal');
        $this->db->join('tkasbon', 'tkasbon.idtransaksi = trawatinap_deposit.idrawatinap');
        $this->db->where('tkasbon.id', $id);
        $this->db->where('trawatinap_deposit.status', 1);
        $query = $this->db->get('trawatinap_deposit');
        return $query->result();
    }

    public function getFakturRefundObat($id)
    {
        $query = $this->db->query("SELECT
          tpasien_pengembalian.tanggal,
          (
            CASE
              WHEN tpasien_pengembalian_detail.idtipe = 1 THEN
                mdata_alkes.nama
              WHEN tpasien_pengembalian_detail.idtipe = 3 THEN
                mdata_obat.nama
            END
          ) AS namatarif,
          tpasien_pengembalian_detail.kuantitas,
          tpasien_pengembalian_detail.harga,
          tpasien_pengembalian_detail.totalharga
        FROM
          tpasien_pengembalian_detail
        JOIN tpasien_pengembalian ON tpasien_pengembalian.id = tpasien_pengembalian_detail.idpengembalian
				JOIN tkasbon ON tkasbon.idtransaksi = tpasien_pengembalian.id
        LEFT JOIN mdata_alkes ON mdata_alkes.id = tpasien_pengembalian_detail.idbarang AND tpasien_pengembalian_detail.idtipe = 1
        LEFT JOIN mdata_obat ON mdata_obat.id = tpasien_pengembalian_detail.idbarang AND tpasien_pengembalian_detail.idtipe = 3
        WHERE
          tkasbon.id = $id");

        return $query->result();
    }

    public function getFakturRefundTransaksi($id)
    {
      $this->db->where('tkasbon_detail.idkasbon', $id);
      $query = $this->db->get('tkasbon_detail');

      return $query->result();
    }

    public function getRefundDeposit($tanggalawal='', $tanggalakhir='', $idtipetransaksi='')
    {
        if ($tanggalawal != '' && $tanggalakhir != '') {
          if ($idtipetransaksi != '0') {
            $query = $this->db->query("SELECT * FROM
            (
            	SELECT
            		trawatinap_pendaftaran.id AS id,
            		trawatinap_pendaftaran.nopendaftaran AS notransaksi,
                trawatinap_pendaftaran.tanggaldaftar AS tanggal,
            		mfpasien.id AS idpasien,
            		mfpasien.no_medrec AS nomedrec,
            		mfpasien.nama AS namapasien,
            		CONCAT(trawatinap_pendaftaran.nopendaftaran, ' ', '(', mfpasien.nama, ')') AS text,
                mpasien_kelompok.id AS idkelompokpasien,
            		mpasien_kelompok.nama AS namakelompokpasien,
            		mdokter.nama AS namadokter,
                ( CASE WHEN trawatinap_pendaftaran.idtipe = 1 THEN 2 WHEN trawatinap_pendaftaran.idtipe = 2 THEN 3 END ) AS idtipetransaksi,
            		( CASE WHEN trawatinap_pendaftaran.idtipe = 1 THEN 'Rawat Inap' WHEN trawatinap_pendaftaran.idtipe = 2 THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi,
            		tkasbon.id AS stkasbon
            	FROM
            		trawatinap_deposit
            		JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_deposit.idrawatinap AND trawatinap_deposit.status = 1
            		JOIN mfpasien ON mfpasien.id = trawatinap_pendaftaran.idpasien
            		JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
            		LEFT JOIN mrekanan ON mrekanan.id = trawatinap_pendaftaran.idrekanan
            		LEFT JOIN mkelas ON mkelas.id = trawatinap_pendaftaran.idkelas
            		LEFT JOIN mbed ON mbed.id = trawatinap_pendaftaran.idbed
            		JOIN mdokter ON mdokter.id = trawatinap_pendaftaran.iddokterpenanggungjawab
            		JOIN trawatinap_tindakan_pembayaran ON trawatinap_tindakan_pembayaran.idtindakan = trawatinap_pendaftaran.id AND trawatinap_tindakan_pembayaran.statusbatal = 0
            		LEFT JOIN tkasbon ON tkasbon.tipe = 0 AND tkasbon.idtransaksi = trawatinap_pendaftaran.id AND tkasbon.status = 1
            	WHERE
            		trawatinap_pendaftaran.statuspembayaran = 1 AND
            		trawatinap_tindakan_pembayaran.deposit > (trawatinap_tindakan_pembayaran.total - trawatinap_tindakan_pembayaran.pembayaran)
            	GROUP BY trawatinap_pendaftaran.id
            ) AS result
            WHERE result.stkasbon IS NULL AND result.idtipetransaksi = $idtipetransaksi AND (DATE(result.tanggal) >= '$tanggalawal' AND DATE(result.tanggal) <= '$tanggalakhir')");
          } else {
            $query = $this->db->query("SELECT * FROM
            (
            	SELECT
            		trawatinap_pendaftaran.id AS id,
            		trawatinap_pendaftaran.nopendaftaran AS notransaksi,
                trawatinap_pendaftaran.tanggaldaftar AS tanggal,
            		mfpasien.id AS idpasien,
            		mfpasien.no_medrec AS nomedrec,
            		mfpasien.nama AS namapasien,
            		CONCAT(trawatinap_pendaftaran.nopendaftaran, ' ', '(', mfpasien.nama, ')') AS text,
                mpasien_kelompok.id AS idkelompokpasien,
            		mpasien_kelompok.nama AS namakelompokpasien,
            		mdokter.nama AS namadokter,
            		( CASE WHEN ( trawatinap_pendaftaran.idtipe = 1 ) THEN 'Rawat Inap' WHEN ( trawatinap_pendaftaran.idtipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi,
            		tkasbon.id AS stkasbon
            	FROM
            		trawatinap_deposit
            		JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_deposit.idrawatinap AND trawatinap_deposit.status = 1
            		JOIN mfpasien ON mfpasien.id = trawatinap_pendaftaran.idpasien
            		JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
            		LEFT JOIN mrekanan ON mrekanan.id = trawatinap_pendaftaran.idrekanan
            		LEFT JOIN mkelas ON mkelas.id = trawatinap_pendaftaran.idkelas
            		LEFT JOIN mbed ON mbed.id = trawatinap_pendaftaran.idbed
            		JOIN mdokter ON mdokter.id = trawatinap_pendaftaran.iddokterpenanggungjawab
            		JOIN trawatinap_tindakan_pembayaran ON trawatinap_tindakan_pembayaran.idtindakan = trawatinap_pendaftaran.id AND trawatinap_tindakan_pembayaran.statusbatal = 0
            		LEFT JOIN tkasbon ON tkasbon.tipe = 0 AND tkasbon.idtransaksi = trawatinap_pendaftaran.id AND tkasbon.status = 1
            	WHERE
            		trawatinap_pendaftaran.statuspembayaran = 1 AND
            		trawatinap_tindakan_pembayaran.deposit > (trawatinap_tindakan_pembayaran.total - trawatinap_tindakan_pembayaran.pembayaran)
            	GROUP BY trawatinap_pendaftaran.id
            ) AS result
            WHERE result.stkasbon IS NULL AND (DATE(result.tanggal) >= '$tanggalawal' AND DATE(result.tanggal) <= '$tanggalakhir')");
          }
        } else {
          $query = $this->db->query("SELECT * FROM
          (
            SELECT
              trawatinap_pendaftaran.id AS id,
              trawatinap_pendaftaran.nopendaftaran AS notransaksi,
              trawatinap_pendaftaran.tanggaldaftar AS tanggal,
              mfpasien.id AS idpasien,
              mfpasien.no_medrec AS nomedrec,
              mfpasien.nama AS namapasien,
              CONCAT(trawatinap_pendaftaran.nopendaftaran, ' ', '(', mfpasien.nama, ')') AS text,
              mpasien_kelompok.id AS idkelompokpasien,
              mpasien_kelompok.nama AS namakelompokpasien,
              mdokter.nama AS namadokter,
              ( CASE WHEN ( trawatinap_pendaftaran.idtipe = 1 ) THEN 'Rawat Inap' WHEN ( trawatinap_pendaftaran.idtipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi,
              tkasbon.id AS stkasbon
            FROM
              trawatinap_deposit
              JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_deposit.idrawatinap AND trawatinap_deposit.status = 1
              JOIN mfpasien ON mfpasien.id = trawatinap_pendaftaran.idpasien
              JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
              LEFT JOIN mrekanan ON mrekanan.id = trawatinap_pendaftaran.idrekanan
              LEFT JOIN mkelas ON mkelas.id = trawatinap_pendaftaran.idkelas
              LEFT JOIN mbed ON mbed.id = trawatinap_pendaftaran.idbed
              JOIN mdokter ON mdokter.id = trawatinap_pendaftaran.iddokterpenanggungjawab
              JOIN trawatinap_tindakan_pembayaran ON trawatinap_tindakan_pembayaran.idtindakan = trawatinap_pendaftaran.id AND trawatinap_tindakan_pembayaran.statusbatal = 0
              LEFT JOIN tkasbon ON tkasbon.tipe = 0 AND tkasbon.idtransaksi = trawatinap_pendaftaran.id AND tkasbon.status = 1
            WHERE
              trawatinap_pendaftaran.statuspembayaran = 1 AND
              trawatinap_tindakan_pembayaran.deposit > (trawatinap_tindakan_pembayaran.total - trawatinap_tindakan_pembayaran.pembayaran)
            GROUP BY trawatinap_pendaftaran.id
          ) AS result
          WHERE result.stkasbon IS NULL");
        }

        return $query->result();
    }

    public function getInfoRefundDeposit($id)
    {
        $query = $this->db->query("SELECT
        	'rawatinap' AS reference,
        	trawatinap_pendaftaran.id AS id,
        	mfpasien.no_medrec AS nomedrec,
        	mfpasien.nama AS namapasien,
        	mfpasien.alamat_jalan AS alamatpasien,
        	trawatinap_pendaftaran.tanggaldaftar AS tanggaltransaksi,
        	( CASE WHEN ( trawatinap_pendaftaran.idtipe = 1 ) THEN 'Rawat Inap' WHEN ( trawatinap_pendaftaran.idtipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi,
        	mpasien_kelompok.nama AS namakelompokpasien,
        	COALESCE(mrekanan.nama, '-') AS namaperusahaan,
        	mkelas.nama AS namakelas,
        	mbed.nama AS namabed,
        	mdokter.nama AS namadokter,
        	trawatinap_tindakan_pembayaran.total AS totaltransaksi,
        	trawatinap_tindakan_pembayaran.pembayaran AS totalpembayaran,
        	trawatinap_tindakan_pembayaran.deposit AS totaldeposit,
        	((trawatinap_tindakan_pembayaran.total - trawatinap_tindakan_pembayaran.pembayaran) - trawatinap_tindakan_pembayaran.deposit) * -1 AS totalrefund
        FROM
        	trawatinap_deposit
        	JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_deposit.idrawatinap AND trawatinap_deposit.status = 1
        	JOIN mfpasien ON mfpasien.id = trawatinap_pendaftaran.idpasien
        	JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
        	LEFT JOIN mrekanan ON mrekanan.id = trawatinap_pendaftaran.idrekanan
        	LEFT JOIN mkelas ON mkelas.id = trawatinap_pendaftaran.idkelas
        	LEFT JOIN mbed ON mbed.id = trawatinap_pendaftaran.idbed
        	JOIN mdokter ON mdokter.id = trawatinap_pendaftaran.iddokterpenanggungjawab
        	JOIN trawatinap_tindakan_pembayaran ON trawatinap_tindakan_pembayaran.idtindakan = trawatinap_pendaftaran.id AND trawatinap_tindakan_pembayaran.statusbatal = 0
        WHERE
        	trawatinap_pendaftaran.id = $id AND
        	trawatinap_pendaftaran.statuspembayaran = 1 AND
        	trawatinap_tindakan_pembayaran.deposit > (trawatinap_tindakan_pembayaran.total - trawatinap_tindakan_pembayaran.pembayaran)");

        return $query->row();
    }

    public function getRefundObat($tanggalawal='', $tanggalakhir='', $idtipetransaksi='')
    {
        if ($tanggalawal != '' && $tanggalakhir != '') {
          if ($idtipetransaksi != '0') {
            $query = $this->db->query("SELECT * FROM
            (
          	SELECT
          		tpasien_pengembalian.id AS id,
          		tpasien_pengembalian.nopengembalian AS notransaksi,
          		tpasien_pengembalian.tanggal,
          		mfpasien.id AS idpasien,
          		mfpasien.no_medrec AS nomedrec,
          		mfpasien.nama AS namapasien,
          		CONCAT(tpasien_pengembalian.nopengembalian, ' ', '(', mfpasien.nama, ')') AS text,
          		mpasien_kelompok.id AS idkelompokpasien,
          		mpasien_kelompok.nama AS namakelompokpasien,
          		mdokter.nama AS namadokter,
          		( CASE WHEN tpasien_penjualan.asalrujukan = 1 THEN 1 WHEN tpasien_penjualan.asalrujukan = 2 THEN 1 END ) AS idtipetransaksi,
          		( CASE WHEN tpasien_penjualan.asalrujukan = 1 THEN 'Poliklinik' WHEN tpasien_penjualan.asalrujukan = 2 THEN 'Instalasi Gawat Darurat (IGD)' END ) AS tipetransaksi,
          		tkasbon.id AS stkasbon
          	FROM
          		tpasien_pengembalian
          		JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan AND tpasien_pengembalian.status = 1
          		JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpasien_penjualan.idtindakan
          		JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
          		JOIN mfpasien ON mfpasien.id = tpasien_pengembalian.idpasien
          		JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
          		JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
          		LEFT JOIN mrekanan ON mrekanan.id = tpoliklinik_pendaftaran.idrekanan
          		JOIN mdokter ON mdokter.id = tpoliklinik_pendaftaran.iddokter
          		LEFT JOIN tkasbon ON tkasbon.tipe = 1 AND tkasbon.idtransaksi = tpasien_pengembalian.id AND tkasbon.status = 1
          	WHERE
          		tpasien_penjualan.asalrujukan IN (1,2)
          	) AS result
          	WHERE result.stkasbon IS NULL AND result.idtipetransaksi = $idtipetransaksi AND (DATE(result.tanggal) >= '$tanggalawal' AND DATE(result.tanggal) <= '$tanggalakhir')");
          } else {
            $query = $this->db->query("SELECT * FROM
            (
          	SELECT
          		tpasien_pengembalian.id AS id,
          		tpasien_pengembalian.nopengembalian AS notransaksi,
          		tpasien_pengembalian.tanggal,
          		mfpasien.id AS idpasien,
          		mfpasien.no_medrec AS nomedrec,
          		mfpasien.nama AS namapasien,
          		CONCAT(tpasien_pengembalian.nopengembalian, ' ', '(', mfpasien.nama, ')') AS text,
              mpasien_kelompok.id AS idkelompokpasien,
          		mpasien_kelompok.nama AS namakelompokpasien,
          		mdokter.nama AS namadokter,
          		( CASE WHEN ( tpasien_penjualan.asalrujukan = 1 ) THEN 'Poliklinik' WHEN ( tpasien_penjualan.asalrujukan = 2 ) THEN 'Instalasi Gawat Darurat (IGD)' END ) AS tipetransaksi,
          		tkasbon.id AS stkasbon
          	FROM
          		tpasien_pengembalian
          		JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan AND tpasien_pengembalian.status = 1
          		JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpasien_penjualan.idtindakan
          		JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
          		JOIN mfpasien ON mfpasien.id = tpasien_pengembalian.idpasien
          		JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
          		JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
          		LEFT JOIN mrekanan ON mrekanan.id = tpoliklinik_pendaftaran.idrekanan
          		JOIN mdokter ON mdokter.id = tpoliklinik_pendaftaran.iddokter
          		LEFT JOIN tkasbon ON tkasbon.tipe = 1 AND tkasbon.idtransaksi = tpasien_pengembalian.id AND tkasbon.status = 1
          	WHERE
          		tpasien_penjualan.asalrujukan IN (1,2)
          	) AS result
          	WHERE result.stkasbon IS NULL AND (DATE(result.tanggal) >= '$tanggalawal' AND DATE(result.tanggal) <= '$tanggalakhir')");
          }
        } else {
          $query = $this->db->query("SELECT * FROM
          (
          SELECT
            tpasien_pengembalian.id AS id,
            tpasien_pengembalian.nopengembalian AS notransaksi,
            tpasien_pengembalian.tanggal,
            mfpasien.id AS idpasien,
            mfpasien.no_medrec AS nomedrec,
            mfpasien.nama AS namapasien,
            CONCAT(tpasien_pengembalian.nopengembalian, ' ', '(', mfpasien.nama, ')') AS text,
            mpasien_kelompok.id AS idkelompokpasien,
            mpasien_kelompok.nama AS namakelompokpasien,
            mdokter.nama AS namadokter,
            ( CASE WHEN ( tpasien_penjualan.asalrujukan = 1 ) THEN 'Poliklinik' WHEN ( tpasien_penjualan.asalrujukan = 2 ) THEN 'Instalasi Gawat Darurat (IGD)' END ) AS tipetransaksi,
            tkasbon.id AS stkasbon
          FROM
            tpasien_pengembalian
            JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan AND tpasien_pengembalian.status = 1
            JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpasien_penjualan.idtindakan
            JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
            JOIN mfpasien ON mfpasien.id = tpasien_pengembalian.idpasien
            JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
            JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
            LEFT JOIN mrekanan ON mrekanan.id = tpoliklinik_pendaftaran.idrekanan
            JOIN mdokter ON mdokter.id = tpoliklinik_pendaftaran.iddokter
            LEFT JOIN tkasbon ON tkasbon.tipe = 1 AND tkasbon.idtransaksi = tpasien_pengembalian.id AND tkasbon.status = 1
          WHERE
            tpasien_penjualan.asalrujukan IN (1,2)
          ) AS result
          WHERE result.stkasbon IS NULL");
        }

        return $query->result();
    }

    public function getInfoRefundObat($id)
    {
        $query = $this->db->query("SELECT
        	'rawatjalan' AS reference,
        	tpasien_pengembalian.id AS id,
        	mfpasien.no_medrec AS nomedrec,
        	mfpasien.nama AS namapasien,
        	mfpasien.alamat_jalan AS alamatpasien,
        	tpasien_pengembalian.tanggal AS tanggaltransaksi,
        	( CASE WHEN ( tpasien_penjualan.asalrujukan = 1 ) THEN 'Poliklinik' WHEN ( tpasien_penjualan.asalrujukan = 2 ) THEN 'Instalasi Gawat Darurat (IGD)' END ) AS tipetransaksi,
        	mpoliklinik.nama AS namapoliklinik,
        	mpasien_kelompok.nama AS namakelompokpasien,
        	COALESCE(mrekanan.nama, '-') AS namaperusahaan,
          mdokter.nama AS namadokter,
        	SUM(tpasien_pengembalian.totalharga) AS totalretur
        FROM
        	tpasien_pengembalian
        	JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_pengembalian.idpenjualan AND tpasien_pengembalian.status = 1
        	JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpasien_penjualan.idtindakan
        	JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
        	JOIN mfpasien ON mfpasien.id = tpasien_pengembalian.idpasien
        	JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
        	JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
        	LEFT JOIN mrekanan ON mrekanan.id = tpoliklinik_pendaftaran.idrekanan
        	JOIN mdokter ON mdokter.id = tpoliklinik_pendaftaran.iddokter
        WHERE
          tpasien_pengembalian.id = $id AND
        	tpasien_penjualan.asalrujukan IN (1,2)");

        return $query->row();
    }

    public function getDetailRefundObat($id)
    {
        $query = $this->db->query("SELECT
        	tpasien_pengembalian.id,
        	tpasien_pengembalian.nopengembalian,
        	tpasien_pengembalian.tanggal,
        	(
        		CASE
        			WHEN tpasien_pengembalian_detail.idtipe = 1 THEN
        				mdata_alkes.nama
        			WHEN tpasien_pengembalian_detail.idtipe = 3 THEN
        				mdata_obat.nama
        		END
        	) AS namatarif,
        	tpasien_pengembalian_detail.harga,
        	tpasien_pengembalian_detail.kuantitas,
        	tpasien_pengembalian_detail.totalharga,
        	munitpelayanan.nama AS namaunitpelayanan
        FROM
        	tpasien_pengembalian_detail
        JOIN tpasien_pengembalian ON tpasien_pengembalian.id = tpasien_pengembalian_detail.idpengembalian AND tpasien_pengembalian.status = 1
        JOIN munitpelayanan ON munitpelayanan.id = tpasien_pengembalian_detail.idunit
        LEFT JOIN mdata_alkes ON mdata_alkes.id = tpasien_pengembalian_detail.idbarang AND tpasien_pengembalian_detail.idtipe = 1
        LEFT JOIN mdata_obat ON mdata_obat.id = tpasien_pengembalian_detail.idbarang AND tpasien_pengembalian_detail.idtipe = 3
        WHERE
          tpasien_pengembalian_detail.idpengembalian = $id");

        return $query->result();
    }

    public function getRefundTransaksi($tanggalawal='', $tanggalakhir='', $idtipetransaksi='')
    {
        if ($tanggalawal != '' && $tanggalakhir != '') {
          if ($idtipetransaksi == '0') {
            $query = $this->db->query("SELECT
            	'rawatinap' AS reference,
            	view_refund_ranap.id AS id,
              view_refund_ranap.noregis AS notransaksi,
              view_refund_ranap.idpasien,
              view_refund_ranap.nomedrec,
              view_refund_ranap.nama AS namapasien,
            	CONCAT( view_refund_ranap.noregis, ' ', '(', view_refund_ranap.nama, ')' ) AS text,
              view_refund_ranap.idkelompokpasien,
              view_refund_ranap.kelompokpasien AS namakelompokpasien,
            	view_refund_ranap.namadokter AS namadokter,
              ( CASE WHEN ( view_refund_ranap.tipe = 1 ) THEN 'Rawat Inap' WHEN ( view_refund_ranap.tipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi
            FROM
            	view_refund_ranap
            WHERE view_refund_ranap.status IS NULL AND DATE(view_refund_ranap.tanggal) >= '$tanggalawal' AND DATE(view_refund_ranap.tanggal) <= '$tanggalakhir'

            UNION ALL

            SELECT
            	'rawatjalan' AS reference,
            	view_refund_rajal.id AS id,
              view_refund_rajal.noregis AS notransaksi,
              view_refund_rajal.idpasien,
              view_refund_rajal.nomedrec,
              view_refund_rajal.nama AS namapasien,
            	CONCAT( view_refund_rajal.noregis, ' ', '(', view_refund_rajal.nama, ')' ) AS text,
              view_refund_rajal.idkelompokpasien,
              view_refund_rajal.kelompokpasien AS namakelompokpasien,
            	view_refund_rajal.namadokter AS namadokter,
              'Rawat Jalan' AS tipetransaksi
            FROM
            	view_refund_rajal
            WHERE view_refund_rajal.status IS NULL AND DATE(view_refund_rajal.tanggal) >= '$tanggalawal' AND DATE(view_refund_rajal.tanggal) <= '$tanggalakhir'");
          } else if ($idtipetransaksi == '1') {
            $query = $this->db->query("SELECT
              'rawatjalan' AS reference,
              view_refund_rajal.id AS id,
              view_refund_rajal.noregis AS notransaksi,
              view_refund_rajal.idpasien,
              view_refund_rajal.nomedrec,
              view_refund_rajal.nama AS namapasien,
              CONCAT( view_refund_rajal.noregis, ' ', '(', view_refund_rajal.nama, ')' ) AS text,
              view_refund_rajal.idkelompokpasien,
              view_refund_rajal.kelompokpasien AS namakelompokpasien,
              view_refund_rajal.namadokter AS namadokter,
              'Rawat Jalan' AS tipetransaksi
            FROM
              view_refund_rajal
            WHERE view_refund_rajal.status IS NULL AND DATE(view_refund_rajal.tanggal) >= '$tanggalawal' AND DATE(view_refund_rajal.tanggal) <= '$tanggalakhir'");
          } else if ($idtipetransaksi == '2') {
            $query = $this->db->query("SELECT
            	'rawatinap' AS reference,
            	view_refund_ranap.id AS id,
              view_refund_ranap.noregis AS notransaksi,
              view_refund_ranap.idpasien,
              view_refund_ranap.nomedrec,
              view_refund_ranap.nama AS namapasien,
            	CONCAT( view_refund_ranap.noregis, ' ', '(', view_refund_ranap.nama, ')' ) AS text,
              view_refund_ranap.idkelompokpasien,
              view_refund_ranap.kelompokpasien AS namakelompokpasien,
            	view_refund_ranap.namadokter AS namadokter,
              ( CASE WHEN ( view_refund_ranap.tipe = 1 ) THEN 'Rawat Inap' WHEN ( view_refund_ranap.tipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi
            FROM
            	view_refund_ranap
            WHERE view_refund_ranap.status IS NULL AND view_refund_ranap.tipe = 1 AND DATE(view_refund_ranap.tanggal) >= '$tanggalawal' AND DATE(view_refund_ranap.tanggal) <= '$tanggalakhir'");
          } else if ($idtipetransaksi == '3') {
            $query = $this->db->query("SELECT
            	'rawatinap' AS reference,
            	view_refund_ranap.id AS id,
              view_refund_ranap.noregis AS notransaksi,
              view_refund_ranap.idpasien,
              view_refund_ranap.nomedrec,
              view_refund_ranap.nama AS namapasien,
            	CONCAT( view_refund_ranap.noregis, ' ', '(', view_refund_ranap.nama, ')' ) AS text,
              view_refund_ranap.idkelompokpasien,
              view_refund_ranap.kelompokpasien AS namakelompokpasien,
            	view_refund_ranap.namadokter AS namadokter,
              ( CASE WHEN ( view_refund_ranap.tipe = 1 ) THEN 'Rawat Inap' WHEN ( view_refund_ranap.tipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi
            FROM
            	view_refund_ranap
            WHERE view_refund_ranap.status IS NULL AND view_refund_ranap.tipe = 2 AND DATE(view_refund_ranap.tanggal) >= '$tanggalawal' AND DATE(view_refund_ranap.tanggal) <= '$tanggalakhir'");
          }
        } else {
          $query = $this->db->query("SELECT
            'rawatinap' AS reference,
            view_refund_ranap.id AS id,
            view_refund_ranap.noregis AS notransaksi,
            view_refund_ranap.idpasien,
            view_refund_ranap.nomedrec,
            view_refund_ranap.nama AS namapasien,
            CONCAT( view_refund_ranap.noregis, ' ', '(', view_refund_ranap.nama, ')' ) AS text,
            view_refund_ranap.idkelompokpasien,
            view_refund_ranap.kelompokpasien AS namakelompokpasien,
            view_refund_ranap.namadokter AS namadokter,
            ( CASE WHEN ( view_refund_ranap.tipe = 1 ) THEN 'Rawat Inap' WHEN ( view_refund_ranap.tipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi
          FROM
            view_refund_ranap
          WHERE view_refund_ranap.status IS NULL

          UNION ALL

          SELECT
            'rawatjalan' AS reference,
            view_refund_rajal.id AS id,
            view_refund_rajal.noregis AS notransaksi,
            view_refund_rajal.idpasien,
            view_refund_rajal.nomedrec,
            view_refund_rajal.nama AS namapasien,
            CONCAT( view_refund_rajal.noregis, ' ', '(', view_refund_rajal.nama, ')' ) AS text,
            view_refund_rajal.idkelompokpasien,
            view_refund_rajal.kelompokpasien AS namakelompokpasien,
            view_refund_rajal.namadokter AS namadokter,
            'Rawat Jalan' AS tipetransaksi
          FROM
            view_refund_rajal
          WHERE view_refund_rajal.status IS NULL");
        }

        return $query->result();
    }

    public function getInfoRefundTransaksi($reference, $id)
    {
        $query = $this->db->query("SELECT * FROM
        (
        	SELECT
        		'rawatinap' AS reference,
        		view_refund_ranap.id AS id,
        		view_refund_ranap.nomedrec,
        		view_refund_ranap.nama AS namapasien,
        		view_refund_ranap.alamat AS alamatpasien,
        		view_refund_ranap.tanggal AS tanggaltransaksi,
        		( CASE WHEN ( view_refund_ranap.tipe = 1 ) THEN 'Rawat Inap' WHEN ( view_refund_ranap.tipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi,
            '' AS namapoliklinik,
        		view_refund_ranap.kelompokpasien AS namakelompokpasien,
        		view_refund_ranap.asuransi AS namaperusahaan,
        		view_refund_ranap.kelas AS namakelas,
        		view_refund_ranap.bed AS namabed,
        		view_refund_ranap.namadokter
        	FROM
        		view_refund_ranap UNION ALL
        	SELECT
        		'rawatjalan' AS reference,
        		view_refund_rajal.id AS id,
        		view_refund_rajal.nomedrec,
        		view_refund_rajal.nama AS namapasien,
        		view_refund_rajal.alamat AS alamatpasien,
        		view_refund_rajal.tanggal AS tanggaltransaksi,
        		'Rawat Jalan' AS tipetransaksi,
        		view_refund_rajal.namapoliklinik AS namapoliklinik,
        		view_refund_rajal.kelompokpasien AS namakelompokpasien,
        		view_refund_rajal.asuransi AS namaperusahaan,
        		'' AS namakelas,
        		'' AS namabed,
        		view_refund_rajal.namadokter
        	FROM
        		view_refund_rajal
        	) AS result
        	WHERE result.reference = '$reference' AND result.id = $id");

        return $query->row();
    }

    function getInfoPendaftaran($idtindakan) {
      $this->db->select('tpoliklinik_pendaftaran.nopendaftaran, tpoliklinik_pendaftaran.tanggaldaftar AS tanggal');
      $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran');
      $this->db->where('tpoliklinik_tindakan.id', $idtindakan);
      $query = $this->db->get('tpoliklinik_tindakan');

      return $query->row();
    }

    function getInfoPendaftaranById($idpendaftaran) {
      $this->db->select('nopendaftaran, tanggaldaftar AS tanggal');
      $this->db->where('id', $idpendaftaran);
      $query = $this->db->get('tpoliklinik_pendaftaran');

      return $query->row();
    }

    public function getListLaboratorium($id)
    {
        $this->db->select('*');
        $this->db->where('idpoliklinik', $id);
        $this->db->where('statusrincianpaket', '0');
        $query = $this->db->get('view_rincian_laboratorium_rajal');
        return $query->result();
    }

    public function getListRadiologi($id)
    {
        $this->db->select('*');
        $this->db->where('idpoliklinik', $id);
        $query = $this->db->get('view_rincian_radiologi_rajal');
        return $query->result();
    }

    public function getListFisioterapi($id)
    {
        $this->db->select('*');
        $this->db->where('idpoliklinik', $id);
        $query = $this->db->get('view_rincian_fisioterapi_rajal');
        return $query->result();
    }

    public function getListFarmasi($id)
    {
        $this->db->select('*');
        $this->db->where('idpoliklinik', $id);
        $query = $this->db->get('view_rincian_farmasi_rajal');
        return $query->result();
    }

    public function getListAdministrasi($id)
    {
        $this->db->select('*');
        $this->db->where('idpoliklinik', $id);
        $query = $this->db->get('view_rincian_administrasi_rajal');
        return $query->result();
    }
	public function insert_validasi_kasbon($id){
		$q="SELECT T.*
			,compare_value(MAX(IF(SB.idpegawai = T.idpegawai,SB.idakun,NULL)),
										MAX(IF(SB.idpegawai = 0 AND SB.idkategori = T.idkategori,SB.idakun,NULL)),
									  MAX(IF(SB.idpegawai = 0 AND SB.idkategori = 0 AND SB.idtipe = T.idtipe,SB.idakun,NULL))) idakun
				,'D' as posisi_akun,S.st_auto_posting
				FROM (
				SELECT '1' as id_reff
				,H.id as idtransaksi,H.tanggal as tanggal_transaksi,H.notransaksi,H.nominal,H.idtipe
				,CASE WHEN H.idtipe='1' THEN MD.idkategori ELSE MP.idkategori END as idkategori,H.idpegawai
				,CASE WHEN H.idtipe='1' THEN MD.nama ELSE MP.nama END as nama
				,CASE WHEN H.idtipe='1' THEN 'DOKTER' ELSE 'PEGAWAI' END as tipe_nama
				,H.catatan

				From tkasbon H
				LEFT JOIN mdokter MD ON MD.id=H.idpegawai AND H.idtipe='1'
				LEFT JOIN mpegawai MP ON MP.id=H.idpegawai AND H.idtipe='2'

				WHERE H.id='$id'
				) T 
				LEFT JOIN msetting_jurnal_kasbon_detail SB ON SB.setting_id='1'
				LEFT JOIN msetting_jurnal_kasbon S ON S.id='1'";
		$row=$this->db->query($q)->row();
		$data_header=array(
			'id_reff' => $row->id_reff,
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'keterangan' => 'KASBON '.$row->tipe_nama.' : '.$row->nama.'('.$row->catatan.')',
			'nominal' => $row->nominal,
			'idakun' => $row->idakun,
			'posisi_akun' => $row->posisi_akun,
			'status' => 1,
			'st_auto_posting' => $row->st_auto_posting,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
            'created_date'=>date('Y-m-d H:i:s')
		);
		// print_r($data_header);exit();
		$st_auto_posting=$row->st_auto_posting;
		$this->db->insert('tvalidasi_kas',$data_header);
		$idvalidasi=$this->db->insert_id();
		$data_kasbon=array(
			'idvalidasi' =>$idvalidasi,
			'idtransaksi' =>$row->idtransaksi,
			'tanggal_transaksi' =>$row->tanggal_transaksi,
			'notransaksi' =>$row->notransaksi,
			'idtipe' =>$row->idtipe,
			'tipe_nama' =>$row->tipe_nama,
			'idpegawai' =>$row->idpegawai,
			'nama' =>$row->nama,
			'catatan' =>$row->catatan,
			'nominal' =>$row->nominal,
			'idakun' =>$row->idakun,
			'posisi_akun' =>$row->posisi_akun,
			'st_posting' =>0,
		);
		$this->db->insert('tvalidasi_kas_01_kasbon',$data_kasbon);
		
		$q="SELECT 
			H.id as idbayar_id,H.jenis_kas_id,J.nama as jenis_kas_nama
			,H.sumber_kas_id,S.nama as sumber_nama
			,S.bank_id as bankid,mbank.nama as bank
			,H.idmetode,RM.metode_bayar as metode_nama,H.nominal_bayar, S.idakun,'K' as posisi_akun
			FROM `tkasbon_pembayaran` H
			LEFT JOIN mjenis_kas J ON J.id=H.jenis_kas_id
			LEFT JOIN msumber_kas S ON S.id=H.sumber_kas_id
			LEFT JOIN ref_metode RM ON RM.id=H.idmetode
			LEFT JOIN mbank ON mbank.id=S.bank_id
			WHERE H.idkasbon='$id'";
		$rows=$this->db->query($q)->result();
		foreach($rows as $r){
			$data_bayar=array(
				'idvalidasi' =>$idvalidasi,
				'idbayar_id' =>$r->idbayar_id,
				'jenis_kas_id' =>$r->jenis_kas_id,
				'jenis_kas_nama' =>$r->jenis_kas_nama,
				'sumber_kas_id' =>$r->sumber_kas_id,
				'sumber_nama' =>$r->sumber_nama,
				'bank' =>$r->bank,
				'bankid' =>$r->bankid,
				'idmetode' =>$r->idmetode,
				'metode_nama' =>$r->metode_nama,
				'nominal_bayar' =>$r->nominal_bayar,
				'idakun' =>$r->idakun,
				'posisi_akun' =>$r->posisi_akun,

			);
			$this->db->insert('tvalidasi_kas_01_kasbon_bayar',$data_bayar);
		}
		if ($st_auto_posting=='1'){
			$data_header=array(
				
				'st_posting' => 1,
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
				

			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_kas',$data_header);
		}
		return true;
		// print_r('BERAHSIL');
	}
}
