<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpengaturan_pengiriman_email_radiologi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('merm_pengaturan_pengiriman_email_radiologi');
        return $query->row();
    }

    public function getPengaturanFileDikirim($id)
    {
        $this->db->select('file_hasil_id');
        $this->db->where('pengaturan_id', $id);
        $query = $this->db->get('merm_pengaturan_pengiriman_email_radiologi_file');
        $result_array = $query->result_array();

        return array_column($result_array, 'file_hasil_id');
    }

    public function saveData()
    {
        $pengaturan_id = 1;

        $this->judul = $_POST['judul'];
        $this->body = $_POST['body'];
        $this->footer_pengirim = $_POST['footer_pengirim'];
        $this->footer_email = $_POST['footer_email'];

        $this->db->where('id', $pengaturan_id);
        if ($this->db->update('merm_pengaturan_pengiriman_email_radiologi', $this)) {
            
            $this->db->where('pengaturan_id', $pengaturan_id);
            $this->db->delete('merm_pengaturan_pengiriman_email_radiologi_file');

            foreach ($_POST['file_dikirim'] as $id) {
                $data['pengaturan_id'] = $pengaturan_id;
                $data['file_hasil_id'] = $id;
                $this->db->insert('merm_pengaturan_pengiriman_email_radiologi_file', $data);
            }
            
            return true;
        }

        return false;
    }
}
