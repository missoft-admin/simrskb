<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tgudang_verifikasi_model extends CI_Model
{
   public function list_pesanan_draft($id){
	   $q="SELECT D.id,D.idpemesanan,D.idtipe,D.idbarang,B.namatipe,B.kode,B.nama as namabarang,B.jumlahsatuanbesar,
		D.kuantitas_kecil,CEIL(D.kuantitas_kecil/B.jumlahsatuanbesar) as kuantitas_besar,(D.kuantitas_kecil * B.hargabeli) as harga
		,sk.nama as satuan_kecil,sb.nama as satuan_besar,D.opsisatuan
		from tgudang_pemesanan_detail D 
		LEFT JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe
		LEFT JOIN msatuan sk ON sk.id=B.idsatuan
		LEFT JOIN msatuan sb on sb.id=B.idsatuanbesar
		WHERE D.idpemesanan='$id'";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function find_beli($idtipe,$idbarang){
	   $q="SELECT D.idbarang,D.idtipe,B.nama,SUM(CASE WHEN D.opsisatuan='1' THEN D.kuantitas ELSE D.kuantitas * B.jumlahsatuanbesar END) as kuantitas,SUM(D.totalharga) as total_beli,B.idsatuan,kecil.nama as satuan_kecil,B.idsatuanbesar,besar.nama as satuan_besar,B.jumlahsatuanbesar FROM tgudang_penerimaan_detail D
			INNER JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe
			INNER JOIN msatuan besar ON besar.id=B.idsatuanbesar
			INNER JOIN msatuan kecil ON kecil.id=B.idsatuan
			WHERE D.idbarang='$idbarang' AND D.idtipe='$idtipe'
			GROUP BY D.idtipe,D.idbarang";
		$query=$this->db->query($q);
		return $query->row_array();
   }
    public function info_barang($idtipe,$idbarang){
	   $q="SELECT B.idtipe,B.id as idbarang,B.namatipe,B.nama,B.kode,B.idkategori,M.nama as nama_kategori from view_barang B
LEFT JOIN mdata_kategori M ON M.id=B.idkategori
			WHERE B.idtipe='$idtipe' AND B.id='$idbarang'";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function find_jual($idtipe,$idbarang){
	   $q="SELECT SUM(D.kuantitas) as kuantitas_jual,SUM(D.totalharga) as total_jual,
			kecil.nama as satuan_kecil_jual,besar.nama as satuan_besar_jual,B.jumlahsatuanbesar as jmlsatuanbesar_jual
			FROM `tpasien_penjualan_nonracikan` D
			INNER JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe
			INNER JOIN msatuan besar ON besar.id=B.idsatuanbesar
			INNER JOIN msatuan kecil ON kecil.id=B.idsatuan
			WHERE D.idbarang='$idbarang' AND D.idtipe='$idtipe'
			GROUP BY D.idtipe,D.idbarang";
		$query=$this->db->query($q);
		return $query->row();
   }
   public function list_distributor(){
	   $q="SELECT *FROM mdistributor
			WHERE `status`='1'
			ORDER BY nama";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_pesanan_edit($id){
	   $q="SELECT D.id,D.idpemesanan,D.idtipe,D.idbarang,B.namatipe,B.kode,B.nama as namabarang,B.jumlahsatuanbesar,
		D.kuantitas_kecil,D.kuantitas as kuantitas_besar,(D.kuantitas_kecil * B.hargabeli) as harga
		,sk.nama as satuan_kecil,sb.nama as satuan_besar,D.opsisatuan,B.hargabeli,B.hargabeli_besar,
		COALESCE((SELECT GS.stok FROM mgudang_stok GS where GS.idunitpelayanan='0' AND GS.idtipe=D.idtipe AND GS.idbarang=D.idbarang),0) as stok
		from tgudang_pemesanan_detail D 
		LEFT JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe
		LEFT JOIN msatuan sk ON sk.id=B.idsatuan
		LEFT JOIN msatuan sb on sb.id=B.idsatuanbesar
		WHERE D.idpemesanan='$id' AND D.status = '1'";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function selectbarang_all() {
        $idtipe             = $this->input->post('idtipe');
        $idunitpelayanan    = $this->input->post('idunitpelayanan');
        $term = $this->input->post('search');
		$panjang=strlen($term);
		 if ($idtipe) {
			
			$q="SELECT B.id,B.nama
				FROM view_barang B				
				WHERE  B.idtipe='$idtipe' AND B.nama like '%".$term."%' LIMIT 100";
			$query=$this->db->query($q);
			$res=$query->result();
			$this->output->set_output(json_encode($res));
		 }
    }
   public function selected_distributor() {
        $id             = $this->input->post('id');
		 if ($id) {
			
			$q="SELECT *from mdistributor where `status`='1' AND id='$id'";
			$query=$this->db->query($q);
			$res=$query->row();
			$this->output->set_output(json_encode($res));
		 }
    }
   public function get_satuan_barang()
    {
        $idtipe = $this->input->post('idtipe');
        $idbarang = $this->input->post('idbarang');
        $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        $this->db->from($table[$idtipe]);
        $this->db->where('id', $idbarang);
        $row = $this->db->get()->row();
        $res = array();
        if ($row->idsatuanbesar == $row->idsatuankecil) {
            $res[]['id'] = $row->idsatuanbesar;
            $res[]['text'] = $this->get_satuan_detail($row->idsatuanbesar)->nama;
        } else {
            $res[]['id'] = $row->idsatuanbesar;
            $res[]['text'] = $this->get_satuan_detail($row->idsatuanbesar)->nama;
            $res[]['id'] = $row->idsatuankecil;
            $res[]['text'] = $this->get_satuan_detail($row->idsatuankecil)->nama;
        }
        $this->output->set_output(json_encode($res));
    }

    public function get_satuan_detail($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('msatuan', 1)->row();
    }
	public function getsorted_distributor_2(){
		
        $idbarang = $this->input->post('idbarang');
        $idtipe = $this->input->post('idtipe');
		$term = $this->input->post('search');
		
		$q="SELECT T.id,CONCAT(T.nama,' (',T.jml,' Kali)') as text 
				FROM (
					SELECT D.id,D.nama,(SELECT COUNT(P.id)  From tgudang_pemesanan P 
					LEFT JOIN tgudang_pemesanan_detail Det ON Det.idpemesanan=P.id 
					Where P.iddistributor=D.id AND Det.idtipe='$idtipe' AND Det.idbarang='$idbarang') as jml 
					FROM mdistributor D
					WHERE D.`status`='1'  AND D.nama LIKE '%".$term."%' ) T 
					
					ORDER BY T.jml DESC,T.nama";
			// print
		$query=$this->db->query($q);
		$res=$query->result();
        $this->output->set_output(json_encode($res));
	}
    public function get_detailbarang($idtipe, $idbarang)
    {
        $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        $this->db->from($table[$idtipe]);
        $this->db->where('id', $idbarang);
        return $this->db->get()->row();
    }

    public function update()
    {
        $detailValue    = $this->input->post('detailValue');
        $idPemesanan    = $this->input->post('idpemesanan');
        if (count($detailValue) > 0) {
            $this->db->where('idpemesanan', $idPemesanan);
            $delDet = $this->db->delete('tgudang_pemesanan_detail');
            if ($delDet) {
                $this->db->set('totalbarang', 0);
                $this->db->set('totalharga', 0);
                $this->db->where('id', $idPemesanan);
                $updateHead0 = $this->db->update('tgudang_pemesanan');
                if ($updateHead0) {
                    foreach ($detailValue as $r) {
                        $tipegudang = $r[6];
                        if ($tipegudang == 4) {
                            // gudang logistik

                            $distributor = $r[7];
                            $this->db->where('iddistributor', $r[7]);
                            $this->db->where('tipepemesanan', 2);
                            $this->db->where('status', 1);
                            $cekDist = $this->db->get('tgudang_pemesanan');
                            if ($cekDist->num_rows() > 0) {
                                $rDist          = $cekDist->row();

                                $this->db->where('idpemesanan', $rDist->id);
                                $this->db->where('idtipe', $r[6]);
                                $this->db->where('idbarang', $r[8]);
                                $cekBarang = $this->db->get('tgudang_pemesanan_detail');

                                $totalHarga     = $r[4]*$r[3];
                                $this->db->set('totalbarang', '`totalbarang` + '.$r[4], false);
                                $this->db->set('totalharga', '`totalharga` + '.$totalHarga, false);
                                $this->db->where('id', $rDist->id);
                                if ($this->db->update('tgudang_pemesanan')) {
                                    $this->db->where('idpemesanan', $rDist->id);
                                    $this->db->where('idtipe', $r[6]);
                                    $this->db->where('idbarang', $r[8]);
                                    $cekBarang = $this->db->get('tgudang_pemesanan_detail');
                                    if ($cekBarang->num_rows() > 0) {
                                        $rBarang        = $cekBarang->row();
                                        $totalHarga     = $r[4]*$r[3];
                                        $this->db->set('kuantitas', '`kuantitas` + '.$r[4], false);
                                        $this->db->set('harga', $r[3]);
                                        $this->db->set('opsisatuan', $r[10]);
                                        $this->db->where('idpemesanan', $rDist->id);
                                        $this->db->where('idtipe', $r[6]);
                                        $this->db->where('idbarang', $r[8]);
                                        if ($this->db->update('tgudang_pemesanan_detail')) {
                                            $status = true;
                                        }
                                    } else {
                                        $this->db->set('idpemesanan', $rDist->id);
                                        $this->db->set('idtipe', $r[6]);
                                        $this->db->set('idbarang', $r[8]);
                                        $this->db->set('kuantitas', $r[4]);
                                        $this->db->set('harga', $r[3]);
                                        $this->db->set('opsisatuan', $r[10]);
                                        $this->db->set('status', 1);
                                        if ($this->db->insert('tgudang_pemesanan_detail')) {
                                            $status = true;
                                        }
                                    }
                                }
                            } else {
                                $tipepemesanan = ($r[6] == 4) ? 2 : 1;
                                $this->db->set('nopemesanan', '-');
                                $this->db->set('tipepemesanan', $tipepemesanan);
                                $this->db->set('tanggal', date('Y-m-d H:i:s'));
                                $this->db->set('iddistributor', $distributor);
                                $this->db->set('totalbarang', $r[4]);
                                $this->db->set('totalharga', $r[3]);
                                $this->db->set('status', 1);
                                if ($this->db->insert('tgudang_pemesanan')) {
                                    $idpemesanan = $this->db->insert_id();
                                    $this->db->set('idpemesanan', $idpemesanan);
                                    $this->db->set('opsisatuan', $r[10]);
                                    $this->db->set('idtipe', $r[6]);
                                    $this->db->set('idbarang', $r[8]);
                                    $this->db->set('kuantitas', $r[4]);
                                    $this->db->set('harga', $r[3]);
                                    $this->db->set('status', 1);
                                    if ($this->db->insert('tgudang_pemesanan_detail')) {
                                        $status = true;
                                    }
                                }
                            }
                        } else {
                            // gudang non logistik
                            $distributor = $r[7];
                            $this->db->where('iddistributor', $r[7]);
                            $this->db->where('tipepemesanan', 1);
                            $this->db->where('status', 1);
                            $cekDist = $this->db->get('tgudang_pemesanan');
                            if ($cekDist->num_rows() > 0) {
                                $rDist          = $cekDist->row();

                                $this->db->where('idpemesanan', $rDist->id);
                                $this->db->where('idtipe', $r[6]);
                                $this->db->where('idbarang', $r[8]);
                                $cekBarang = $this->db->get('tgudang_pemesanan_detail');

                                $totalHarga     = $r[4]*$r[3];
                                $this->db->set('totalbarang', '`totalbarang` + '.$r[4], false);
                                $this->db->set('totalharga', '`totalharga` + '.$totalHarga, false);
                                $this->db->where('id', $rDist->id);
                                if ($this->db->update('tgudang_pemesanan')) {
                                    $this->db->where('idpemesanan', $rDist->id);
                                    $this->db->where('idtipe', $r[6]);
                                    $this->db->where('idbarang', $r[8]);
                                    $cekBarang = $this->db->get('tgudang_pemesanan_detail');
                                    if ($cekBarang->num_rows() > 0) {
                                        $rBarang        = $cekBarang->row();
                                        $totalHarga     = $r[4]*$r[3];
                                        $this->db->set('kuantitas', '`kuantitas` + '.$r[4], false);
                                        $this->db->set('harga', $r[3]);
                                        $this->db->set('opsisatuan', $r[10]);
                                        $this->db->where('idpemesanan', $rDist->id);
                                        $this->db->where('idtipe', $r[6]);
                                        $this->db->where('idbarang', $r[8]);
                                        if ($this->db->update('tgudang_pemesanan_detail')) {
                                            $status = true;
                                        }
                                    } else {
                                        $this->db->set('idpemesanan', $rDist->id);
                                        $this->db->set('idtipe', $r[6]);
                                        $this->db->set('idbarang', $r[8]);
                                        $this->db->set('kuantitas', $r[4]);
                                        $this->db->set('harga', $r[3]);
                                        $this->db->set('opsisatuan', $r[10]);
                                        $this->db->set('status', 1);
                                        if ($this->db->insert('tgudang_pemesanan_detail')) {
                                            $status = true;
                                        }
                                    }
                                }
                            } else {
                                $tipepemesanan = ($r[6] == 4) ? 2 : 1;
                                $this->db->set('nopemesanan', '-');
                                $this->db->set('tipepemesanan', $tipepemesanan);
                                $this->db->set('tanggal', date('Y-m-d H:i:s'));
                                $this->db->set('iddistributor', $distributor);
                                $this->db->set('totalbarang', $r[4]);
                                $this->db->set('totalharga', $r[3]);
                                $this->db->set('status', 1);
                                if ($this->db->insert('tgudang_pemesanan')) {
                                    $idpemesanan = $this->db->insert_id();
                                    $this->db->set('idpemesanan', $idpemesanan);
                                    $this->db->set('opsisatuan', $r[10]);
                                    $this->db->set('idtipe', $r[6]);
                                    $this->db->set('idbarang', $r[8]);
                                    $this->db->set('kuantitas', $r[4]);
                                    $this->db->set('harga', $r[3]);
                                    $this->db->set('status', 1);
                                    if ($this->db->insert('tgudang_pemesanan_detail')) {
                                        $status = true;
                                    }
                                }
                            }
                        }
                    }
                }

                $this->db->where('idpemesanan', $idPemesanan);
                $cekDetail = $this->db->get('tgudang_pemesanan_detail')->num_rows();
                if ($cekDetail < 1) {
                    $this->db->set('status', 5);
                    $this->db->where('id', $idPemesanan);
                    if ($this->db->update('tgudang_pemesanan')) {
                        $status = true;
                    }
                }
            }

            $status = true;
        }
        return $status;
    }
	public function save_finalisasi(){
		
		$idpemesanan=$this->input->post('idpemesanan');		
		$id_det=$this->input->post('e_id_det');		
		$harga=$this->input->post('harga');
		$harga_total=$this->input->post('harga_total');
		// print_r($harga_total);exit();
		foreach ($id_det as $key => $val) {
			
				$data_update=array();
				$data_update['harga']=$harga[$key];
				$data_update['harga_total']=$harga_total[$key];				
				$this->db->where('id', $id_det[$key]);
				$this->db->update('tgudang_pemesanan_detail',$data_update); 
			
		}
		
		$head_update=array();
		$head_update['status']=4;
		$head_update['id_user_finalisasi']=$this->session->userdata('user_id');
		$head_update['nama_user_finalisasi']=$this->session->userdata('user_name');
		$head_update['tgl_finalisasi']=date('Y-m-d H:i:s');
		$this->db->where('id', $idpemesanan);
		$this->db->update('tgudang_pemesanan',$head_update); 
		
		return true;
	}
	public function tolak($id) {
        return $this->db->update('tgudang_pemesanan', array('status' => 0,'st_batal_by' => 3,'iduser_batal' => $this->session->userdata("user_id"),'tgl_batal' => date('Y-m-d H:i:s')), array('id' => $id) );
    }   
	public function save_edit(){
		$btn_sumbit=$this->input->post('btn_submit');
		// print_r();exit();
		$jml_edit='0';
		// $xidistributor=$this->input->post('xidistributor');
		// $iddistributor=$this->input->post('iddistributor');
		
		$approval_id=$this->input->post('approval_id');
		$idpemesanan=$this->input->post('idpemesanan');
		$idtipe=$this->input->post('e_idtipe');
		$idbarang=$this->input->post('e_idbarang');
		$st_edit=$this->input->post('e_st_edit');
		$st_hapus=$this->input->post('e_st_hapus');
		$qty=$this->input->post('e_kuantitas_besar');
		$qty_kecil=$this->input->post('e_kuantitas_kecil');
		$id_det=$this->input->post('e_id_det');
		$namabarang=$this->input->post('nama_barang');
		$opsisatuan=$this->input->post('opsisatuan');
		$harga=$this->input->post('e_hargabeli');
		$harga_besar=$this->input->post('e_hargabeli_besar');
		foreach ($st_edit as $key => $val) {
			if ($st_edit[$key]=='1'){
				$jml_edit=$jml_edit +1 ;
				$data_update=array();
				$data_update['idtipe']=$idtipe[$key];
				$data_update['idbarang']=$idbarang[$key];
				$data_update['opsisatuan']=$opsisatuan[$key];
				$data_update['kuantitas']=$qty[$key];
				$data_update['kuantitas_kecil']=$qty_kecil[$key];
				$data_update['kuantitas_kecil']=$qty_kecil[$key];
				$data_update['harga']=$qty_kecil[$key];
				$data_update['edit_by']=$this->session->userdata('user_id');
				$data_update['date_edit']=date('Y-m-d H:i:s');
				// $data_update['namabarang']=RemoveBintang($namabarang[$key]);
				if ($opsisatuan[$key]=='1'){
					$data_update['harga']=RemoveComma($harga[$key]);
				}else{
					$data_update['harga']=RemoveComma($harga_besar[$key]);
				}
				if ($st_hapus[$key]=='1'){					
					$data_update['status']='0';
					$data_update['hapus_by']=$this->session->userdata('user_id');
					$data_update['hapus_date']=date('Y-m-d H:i:s');
				}
				// print_r($data_update);exit();
				// $data_update['kuantitas']=$qty[$key];
				if ($id_det[$key]=='0'){
					$data_update['idpemesanan']=$idpemesanan;
					$data_update['status']='1';
					
					$this->db->insert('tgudang_pemesanan_detail',$data_update); 
				}else{
					$this->db->where('id', $id_det[$key]);
					$this->db->update('tgudang_pemesanan_detail',$data_update); 
				}
			}
		}
		if ($jml_edit > 0){
			$head_update=array();
			// $head_update['iddistributor']=$iddistributor;
			$head_update['edit_user']=$this->session->userdata('user_id');
			$head_update['edit_date']=date('Y-m-d H:i:s');
			$this->db->where('id', $idpemesanan);
			$this->db->update('tgudang_pemesanan',$head_update); 
		}
		if ($btn_sumbit=='2'){//ACC
			$data_acc['id_user_approval']=$this->session->userdata('user_id');
			$data_acc['nama_user_approval']=$this->session->userdata('user_name');
			$data_acc['tanggal_approval']=date('Y-m-d H:i:s');
			// $data_acc['status']='3';		
				
			$this->db->where('id', $idpemesanan);
			$this->db->update('tgudang_pemesanan',$data_acc); 
			$q="call update_gudang_approval('$approval_id', 1) ";
			$result=$this->db->query($q);
			
		}
		if ($btn_sumbit=='3'){//TOLAK
			$data_acc['iduser_batal']=$this->session->userdata('user_id');
			$data_acc['tgl_batal']=date('Y-m-d H:i:s');
			$data_acc['st_batal_by']='3';			
			// $data_acc['status']='0';			
			$this->db->where('id', $idpemesanan);
			$this->db->update('tgudang_pemesanan',$data_acc); 	
			$q="call update_gudang_approval('$approval_id', 2) ";
			$result=$this->db->query($q);
		}
		return true;
	}
    public function save_edit_pesanan(){
		// print_r($this->input->post());exit();
		$jml_edit='0';
		$jml_rubah='0';
		$xidistributor=$this->input->post('xidistributor');
		$iddistributor=$this->input->post('iddistributor');
		$xtipe_bayar=$this->input->post('xtipe_bayar');
		$tipe_bayar=$this->input->post('tipe_bayar');
		
		$alasan=$this->input->post('alasan');
		$idpemesanan=$this->input->post('idpemesanan');
		$idtipe=$this->input->post('e_idtipe');
		$idbarang=$this->input->post('e_idbarang');
		$st_edit=$this->input->post('e_st_edit');
		$st_hapus=$this->input->post('e_st_hapus');
		$qty=$this->input->post('e_kuantitas_besar');
		$qty_kecil=$this->input->post('e_kuantitas_kecil');
		$id_det=$this->input->post('e_id_det');
		$namabarang=$this->input->post('nama_barang');
		$opsisatuan=$this->input->post('opsisatuan');
		$harga=$this->input->post('e_hargabeli');
		$harga_besar=$this->input->post('e_hargabeli_besar');
		$e_iddistributor=$this->input->post('e_iddistributor');
		$e_alasan=$this->input->post('e_alasan');
		$sort_iddistributor=$this->input->post('e_iddistributor');
		foreach ($st_edit as $key => $val) {
			if ($st_edit[$key]=='1' ){
				$jml_edit=$jml_edit +1 ;
				$data_update=array();
				$data_update['idtipe']=$idtipe[$key];
				$data_update['idbarang']=$idbarang[$key];
				$data_update['opsisatuan']=$opsisatuan[$key];
				$data_update['kuantitas']=$qty[$key];
				$data_update['kuantitas_kecil']=$qty_kecil[$key];
				$data_update['kuantitas_kecil']=$qty_kecil[$key];
				$data_update['harga']=$qty_kecil[$key];
				$data_update['edit_by']=$this->session->userdata('user_id');
				$data_update['date_edit']=date('Y-m-d H:i:s');
				// $data_update['namabarang']=RemoveBintang($namabarang[$key]);
				if ($opsisatuan[$key]=='1'){
					$data_update['harga']=RemoveComma($harga[$key]);
				}else{
					$data_update['harga']=RemoveComma($harga_besar[$key]);
				}
				if ($st_hapus[$key]=='1'){					
					$data_update['alasan']=$e_alasan[$key];
					$data_update['status']='0';
					$data_update['hapus_by']=$this->session->userdata('user_id');
					$data_update['hapus_date']=date('Y-m-d H:i:s');
				}
				if ($e_iddistributor[$key]!=$iddistributor){	
					$data_update['alasan']=$e_alasan[$key];
					$data_update['status']='2';
					$jml_rubah=$jml_rubah +1 ;
					
				}
				
				// $data_update['kuantitas']=$qty[$key];
				if ($id_det[$key]=='0'){
					$data_update['idpemesanan']=$idpemesanan;
					$data_update['status']='1';
					
					$this->db->insert('tgudang_pemesanan_detail',$data_update); 
				}else{
					$this->db->where('id', $id_det[$key]);
					$this->db->update('tgudang_pemesanan_detail',$data_update); 
				}
			}
		}
		$dist_asal='';
		$no_head='';
		$no_detail='';
		if ($jml_rubah > 0){
			asort($sort_iddistributor);
			foreach ($sort_iddistributor as $key => $val) {
				if ($e_iddistributor[$key]!=$iddistributor){	
					if ($dist_asal != $e_iddistributor[$key]){//Membuat No HEAD
						$dist_asal=$e_iddistributor[$key];
						
						$query="INSERT INTO tgudang_pemesanan (tipepemesanan,tanggal,iddistributor,totalbarang,totalharga,statusretur,stdraft,`status`,userpemesanan,id_user_pemesanan,id_user_konfirmasi,nama_user_konfirmasi,tanggal_konfirmasi,id_user_approval,nama_user_approval,tanggal_approval,edit_user,edit_date,id_asal,alasan_ganti)
								SELECT
									tipepemesanan,	tanggal,	'$dist_asal' AS iddistributor,	'0' AS totalbarang,	'0' AS totalharga,	'0' AS statusretur,	'0' AS stdraft,	`status`,
									userpemesanan,id_user_pemesanan,id_user_konfirmasi,nama_user_konfirmasi,tanggal_konfirmasi,id_user_approval,nama_user_approval,tanggal_approval,edit_user,edit_date,'$idpemesanan' as id_asal,'$alasan' as alasan_ganti
								FROM
									tgudang_pemesanan 
								WHERE
									id = '$idpemesanan'";
						$this->db->query($query);
						$no_head=$this->db->insert_id();
						// print_r($no_head);exit();
					}
					$id_detail=$id_det[$key];
					$q="INSERT INTO tgudang_pemesanan_detail (idpemesanan,idtipe,idbarang,opsisatuan,kuantitas,kuantitas_kecil,
							harga,harga_total,status,harga_terakhir,stok_terakhir,id_asal,harga_sebelumnya,
							stok_sebelumnya,st_konversi,edit_by,date_edit,hapus_by,hapus_date,alasan)
							SELECT '$no_head' as idpemesanan,idtipe,idbarang,opsisatuan,kuantitas,kuantitas_kecil,
							harga,harga_total,'1' as status,harga_terakhir,stok_terakhir,'$id_detail' as id_asal,harga_sebelumnya,
							stok_sebelumnya,st_konversi,edit_by,date_edit,hapus_by,hapus_date,alasan  
							from tgudang_pemesanan_detail WHERE id='$id_detail'";
					$this->db->query($q);
					$no_detail=$this->db->insert_id();
					$q="UPDATE tgudang_pemesanan_history SET iddet_gudang='$no_detail' WHERE iddet_gudang='$id_detail'";
					$this->db->query($q);
				}
			}				
		}
		if ($jml_edit > 0 || ($xidistributor != $iddistributor || $xtipe_bayar != $tipe_bayar)){
			$head_update=array();
			$head_update['alasan_ganti']=$alasan;
			$head_update['tipe_bayar']=$tipe_bayar;
			$head_update['iddistributor']=$iddistributor;
			$head_update['edit_user']=$this->session->userdata('user_id');
			$head_update['edit_date']=date('Y-m-d H:i:s');
			$this->db->where('id', $idpemesanan);
			$this->db->update('tgudang_pemesanan',$head_update); 
		}
		return true;
	}
    public function setujui($id){
		
		$head_update=array();
		$head_update['id_user_approval']=$this->session->userdata('user_id');
		$head_update['nama_user_approval']=$this->session->userdata('user_name');
		$head_update['tanggal_approval']=date('Y-m-d H:i:s');
		$head_update['status']='3';
		$this->db->where('id', $id);
		$this->db->update('tgudang_pemesanan',$head_update); 
		
		return true;
	}
    public function save()
    {
        $detailValue = $this->input->post('detailValue');
		
        if (count($detailValue) > 0) {
            foreach ($detailValue as $r) {
                $tipegudang = $r[6];

                if ($tipegudang == 4) {
                    // gudang logistik

                    $distributor = $r[7];
                    $this->db->where('iddistributor', $r[7]);
                    $this->db->where('tipepemesanan', 2);
                    $this->db->where('status', 1);
                    $cekDist = $this->db->get('tgudang_pemesanan');
                    if ($cekDist->num_rows() > 0) {
                        $rDist          = $cekDist->row();
                        $totalHarga     = $r[4]*$r[3];
                        $this->db->set('totalbarang', '`totalbarang` + '.$r[4], false);
                        $this->db->set('totalharga', '`totalharga` + '.$totalHarga, false);
                        $this->db->where('id', $rDist->id);
                        if ($this->db->update('tgudang_pemesanan')) {
                            $this->db->where('idpemesanan', $rDist->id);
                            $this->db->where('idtipe', $r[6]);
                            $this->db->where('idbarang', $r[8]);
                            $cekBarang = $this->db->get('tgudang_pemesanan_detail');
                            if ($cekBarang->num_rows() > 0) {
                                $rBarang        = $cekBarang->row();
                                
                                $totalHarga     = $r[4]*$r[3];
                                $this->db->set('kuantitas', '`kuantitas` + '.$r[4], false);
								$this->db->set('kuantitas_kecil', '`kuantitas_kecil` + '.$r[13], false);
                                $this->db->set('harga', $r[3]);
                                $this->db->set('opsisatuan', $r[10]);
                                $this->db->where('idpemesanan', $rDist->id);
                                $this->db->where('idtipe', $r[6]);
                                $this->db->where('idbarang', $r[8]);
                                if ($this->db->update('tgudang_pemesanan_detail')) {
									$data_his['iddet_gudang']=$cekBarang->row()->id;
									$data_his['iddet_permintaan']=0;
									$data_his['unit_peminta']='0';
									$data_his['no_up_permintaan']='-';
									$data_his['tipepemesanan']='2';
									$data_his['idtipe']=$r[6];
									$data_his['idbarang']=$r[8];
									$data_his['opsisatuan']=$r[10];
									$data_his['kuantitas']=$r[4];
									$data_his['kuantitas_kecil']=$r[13];
									$data_his['iduser_peminta']=$this->session->userdata('user_id');
									$data_his['tanggal']=date('Y-m-d H:i:s');
									$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                    $status = true;
                                }
                            } else {
                                $this->db->set('idpemesanan', $rDist->id);
                                $this->db->set('opsisatuan', $r[10]);
                                $this->db->set('idtipe', $r[6]);
                                $this->db->set('idbarang', $r[8]);
                                $this->db->set('kuantitas', $r[4]);
								$this->db->set('kuantitas_kecil', $r[13]);
                                $this->db->set('harga', $r[3]);
                                $this->db->set('status', 1);
                                $this->db->set('harga_terakhir', $r[11]);
                                $this->db->set('stok_terakhir', $r[12]);
                                if ($this->db->insert('tgudang_pemesanan_detail')) {
									$data_his['iddet_gudang']=$this->db->insert_id();
									$data_his['iddet_permintaan']=0;
									$data_his['unit_peminta']='0';
									$data_his['no_up_permintaan']='-';
									$data_his['tipepemesanan']='2';
									$data_his['idtipe']=$r[6];
									$data_his['idbarang']=$r[8];
									$data_his['opsisatuan']=$r[10];
									$data_his['kuantitas']=$r[4];
									$data_his['kuantitas_kecil']=$r[13];
									$data_his['iduser_peminta']=$this->session->userdata('user_id');
									$data_his['tanggal']=date('Y-m-d H:i:s');
									$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                    $status = true;
                                }
                            }
                        }
                    } else {
                        $tipepemesanan = ($r[6] == 4) ? 2 : 1;
                        $totalHarga = $r[4]*$r[3];
                        $this->db->set('nopemesanan', '-');
                        $this->db->set('stdraft', '1');
                        $this->db->set('tipepemesanan', $tipepemesanan);
                        $this->db->set('tanggal', date('Y-m-d H:i:s'));
                        $this->db->set('iddistributor', $distributor);
                        $this->db->set('totalbarang', $r[4]);
                        $this->db->set('totalharga', $totalHarga);
                        $this->db->set('userpemesanan', $this->session->userdata('user_name'));
                        $this->db->set('status', 1);
                        if ($this->db->insert('tgudang_pemesanan')) {
                            $idpemesanan = $this->db->insert_id();

                            $this->db->set('idpemesanan', $idpemesanan);
                            $this->db->set('opsisatuan', $r[10]);
                            $this->db->set('idtipe', $r[6]);
                            $this->db->set('idbarang', $r[8]);
                            $this->db->set('kuantitas', $r[4]);
							$this->db->set('kuantitas_kecil', $r[13]);
                            $this->db->set('harga', $r[3]);
                            $this->db->set('status', 1);
                            $this->db->set('harga_terakhir', $r[11]);
                            $this->db->set('stok_terakhir', $r[12]);
                            if ($this->db->insert('tgudang_pemesanan_detail')) {
								$data_his['iddet_gudang']=$this->db->insert_id();
								$data_his['iddet_permintaan']=0;
								$data_his['unit_peminta']='0';
								$data_his['no_up_permintaan']='-';
								$data_his['tipepemesanan']='2';
								$data_his['idtipe']=$r[6];
								$data_his['idbarang']=$r[8];
								$data_his['opsisatuan']=$r[10];
								$data_his['kuantitas']=$r[4];
								$data_his['kuantitas_kecil']=$r[13];
								$data_his['iduser_peminta']=$this->session->userdata('user_id');
								$data_his['tanggal']=date('Y-m-d H:i:s');
								$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                $status = true;
                            }
                        }
                    }
                } else {

                    // gudang non logistik
                    $distributor = $r[7];
                    $this->db->where('iddistributor', $r[7]);
                    $this->db->where('tipepemesanan', 1);
                    $this->db->where('status', 1);
                    $cekDist = $this->db->get('tgudang_pemesanan');
                    if ($cekDist->num_rows() > 0) {
                        $rDist          = $cekDist->row();
                        $totalHarga     = $r[4]*$r[3];
                        $this->db->set('totalbarang', '`totalbarang` + '.$r[4], false);
                        $this->db->set('totalharga', '`totalharga` + '.$totalHarga, false);
                        $this->db->where('id', $rDist->id);
                        if ($this->db->update('tgudang_pemesanan')) {
                            $this->db->where('idpemesanan', $rDist->id);
                            $this->db->where('idtipe', $r[6]);
                            $this->db->where('idbarang', $r[8]);
                            $cekBarang = $this->db->get('tgudang_pemesanan_detail');
                            if ($cekBarang->num_rows() > 0) {
                                $rBarang        = $cekBarang->row();
                                
                                $totalHarga     = $r[4]*$r[3];
                                $this->db->set('kuantitas', '`kuantitas` + '.$r[4], false);
								$this->db->set('kuantitas_kecil', '`kuantitas_kecil` + '.$r[13], false);
                                $this->db->set('harga', $r[3]);
                                $this->db->set('opsisatuan', $r[10]);
                                $this->db->where('idpemesanan', $rDist->id);
                                $this->db->where('idtipe', $r[6]);
                                $this->db->where('idbarang', $r[8]);
                                if ($this->db->update('tgudang_pemesanan_detail')) {
									$data_his=array();
									$data_his['iddet_gudang']=$cekBarang->row()->id;
									$data_his['iddet_permintaan']=0;
									$data_his['unit_peminta']='0';
									$data_his['no_up_permintaan']='-';
									$data_his['tipepemesanan']='1';
									$data_his['idtipe']=$r[6];
									$data_his['idbarang']=$r[8];
									$data_his['opsisatuan']=$r[10];
									$data_his['kuantitas']=$r[4];
									$data_his['kuantitas_kecil']=$r[13];
									$data_his['iduser_peminta']=$this->session->userdata('user_id');
									$data_his['tanggal']=date('Y-m-d H:i:s');
									$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                    $status = true;
                                }
                            } else {
                                $this->db->set('idpemesanan', $rDist->id);
                                $this->db->set('opsisatuan', $r[10]);
                                $this->db->set('idtipe', $r[6]);
                                $this->db->set('idbarang', $r[8]);
                                $this->db->set('kuantitas', $r[4]);
                                $this->db->set('kuantitas_kecil', $r[13]);
                                $this->db->set('harga', $r[3]);
                                $this->db->set('status', 1);
                                $this->db->set('harga_terakhir', $r[11]);
                                $this->db->set('stok_terakhir', $r[12]);
                                if ($this->db->insert('tgudang_pemesanan_detail')) {
									$data_his=array();
									$data_his['iddet_gudang']=$this->db->insert_id();
									$data_his['iddet_permintaan']=0;
									$data_his['unit_peminta']='0';
									$data_his['no_up_permintaan']='-';
									$data_his['tipepemesanan']='1';
									$data_his['idtipe']=$r[6];
									$data_his['idbarang']=$r[8];
									$data_his['opsisatuan']=$r[10];
									$data_his['kuantitas']=$r[4];
									$data_his['kuantitas_kecil']=$r[13];
									$data_his['iduser_peminta']=$this->session->userdata('user_id');
									$data_his['tanggal']=date('Y-m-d H:i:s');
									$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                    $status = true;
                                }
                            }
                        }
                    } else {
                        $tipepemesanan = ($r[6] == 4) ? 2 : 1;
                        $totalHarga = $r[4]*$r[3];
                        $this->db->set('nopemesanan', '-');
                        $this->db->set('stdraft', 1);
                        $this->db->set('tipepemesanan', $tipepemesanan);
                        $this->db->set('tanggal', date('Y-m-d H:i:s'));
                        $this->db->set('iddistributor', $distributor);
                        $this->db->set('totalbarang', $r[4]);
                        $this->db->set('totalharga', $totalHarga);
                        $this->db->set('userpemesanan', $this->session->userdata('user_name'));
                        $this->db->set('status', 1);
                        if ($this->db->insert('tgudang_pemesanan')) {
                            $idpemesanan = $this->db->insert_id();

                            $this->db->set('idpemesanan', $idpemesanan);
                            $this->db->set('opsisatuan', $r[10]);
                            $this->db->set('idtipe', $r[6]);
                            $this->db->set('idbarang', $r[8]);
                            $this->db->set('kuantitas', $r[4]);
                            $this->db->set('kuantitas_kecil', $r[13]);
                            $this->db->set('harga', $r[3]);
                            $this->db->set('status', 1);
                            $this->db->set('harga_terakhir', $r[11]);
                            $this->db->set('stok_terakhir', $r[12]);
                            if ($this->db->insert('tgudang_pemesanan_detail')) {
								$data_his['iddet_gudang']=$this->db->insert_id();
								$data_his['iddet_permintaan']=0;
								$data_his['unit_peminta']='0';
								$data_his['no_up_permintaan']='-';
								$data_his['tipepemesanan']='1';
								$data_his['idtipe']=$r[6];
								$data_his['idbarang']=$r[8];
								$data_his['opsisatuan']=$r[10];
								$data_his['kuantitas']=$r[4];
								$data_his['kuantitas_kecil']=$r[13];
								$data_his['iduser_peminta']=$this->session->userdata('user_id');
								$data_his['tanggal']=date('Y-m-d H:i:s');
								$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                $status = true;
                            }
                        }
                    }
                }
            }
        }
        return $status;
    }

    public function editPemesanan($id)
    {
        $this->db->select(
            'a.id, 
            a.idtipe,
            a.idbarang,
            b.iddistributor,
            a.harga,
            a.kuantitas,
            a.opsisatuan'
        );
        $this->db->where('a.idpemesanan', $id);
        $this->db->join('tgudang_pemesanan b', 'b.id = a.idpemesanan', 'left');
        return $this->db->get('tgudang_pemesanan_detail a')->result();
    }

    public function viewHead($id)
    {
        $this->db->select('a.id, a.nopemesanan, a.totalbarang, a.totalharga, b.nama as distributor');
        $this->db->from('tgudang_pemesanan a');
        $this->db->join('mdistributor b', 'b.id = a.iddistributor', 'left');
        $this->db->where('a.id', $id);
        return $this->db->get()->row_array();
    }

    public function viewDetail($idpemesanan)
    {
        $this->db->where('idpemesanan', $idpemesanan);
        return $this->db->get('tgudang_pemesanan_detail')->result();
    }

    public function getNamaBarang($idtipe, $idbarang)
    {
        $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        $this->db->from($table[$idtipe]);
        $this->db->where('id', $idbarang);
        return $this->db->get()->row()->nama;
    }

    public function getSatuanBarang($idtipe, $idbarang)
    {
        $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        if ($idtipe == 2 || $idtipe == 4) {
            return '-';
        } else {
            $this->db->select('b.nama');
            $this->db->join('msatuan b', 'b.id = a.idsatuanbesar', 'left');
            $this->db->where('a.id', $idbarang);
            return $this->db->get($table[$idtipe].' a', 1)->row()->nama;
        }
    }

    public function getAnyQty($idtipe, $idbarang, $idpemesanan)
    {
        $this->db->where('idpemesanan', $idpemesanan);
        $this->db->where('idtipe', $idtipe);
        $this->db->where('idbarang', $idbarang);
        $r = $this->db->get('tgudang_pemesanan_detail', 1)->row();
        if ($r->status == 1) {
            $jmlterima = 0;
            $this->db->select_sum('kuantitas', 'kuantitas');
            $this->db->where('b.idpemesanan', $idpemesanan);
            $this->db->where('a.idtipe', $idtipe);
            $this->db->where('a.idbarang', $idbarang);
            $this->db->join('tgudang_penerimaan b', 'b.id = a.idpenerimaan', 'left');
            $rk = $this->db->get('tgudang_penerimaan_detail a')->row();
            if ($rk) {
                $jmlterima = $rk->kuantitas;
            }

            $data['jmlpesan']   = $r->kuantitas;
            $data['jmlterima']  = $jmlterima;
            $data['selisih']    = $r->kuantitas-$jmlterima;
        } else {
            $data['jmlpesan']   = $r->kuantitas;
            $data['jmlterima']  = $r->kuantitas;
            $data['selisih']    = 0;
        }
        return $data;
    }

    public function getJmlPesanTerima($idpemesanan)
    {
        $this->db->select_sum('totalbarang', 'totalbarang');
        $this->db->where('idpemesanan', $idpemesanan);
        $rt = $this->db->get('tgudang_penerimaan', 1)->row();
        if ($rt->totalbarang == null) {
            $data['jmlterima'] = 0;
        } else {
            $data['jmlterima'] = $rt->totalbarang;
        }
        return $data;
    }


    public function getMaxDistributor($idtipe, $idbarang)
    {
        $table = "(SELECT
        iddistributor, total FROM (
        SELECT
        g.iddistributor,
        COUNT(g.iddistributor) total
        FROM tgudang_pemesanan_detail gdet
        INNER JOIN tgudang_pemesanan g ON g.id = gdet.idpemesanan
        WHERE idbarang = ".$idbarang." AND idtipe = ".$idtipe."
        AND g.status != 1 AND g.status != 0
        GROUP BY g.iddistributor ) t
        ORDER BY total DESC LIMIT 1) tbl";
        $this->db->select('iddistributor');
        return $this->db->get($table)->row();
    }

    public function getStokGudang($idtipe, $idbarang)
    {
        $this->db->where('idtipe', $idtipe);
        $this->db->where('idbarang', $idbarang);
        $this->db->where('idunitpelayanan', 0);
        $query = $this->db->get('mgudang_stok', 1)->row();
        if ($query) {
            return $query->stok;
        } else {
            return 0;
        }
    }

    public function print_po_head($id)
    {
        if ($id) {
            $this->db->where('gp.id', $id);
            $this->db->select('gp.*, dist.nama, dist.alamat, dist.telepon');
            $this->db->join('mdistributor dist', 'dist.id = gp.iddistributor', 'left');
            return $this->db->get('tgudang_pemesanan gp')->row_array();
        }
    }
    public function print_po_detail($id)
    {
        $this->db->where('det.idpemesanan', $id);
        return $this->db->get('tgudang_pemesanan_detail det')->result();
    }
    public function detailbarangdraft()
    {
        $idpemesanan = $this->uri->segment(3);
        if ($idpemesanan) {
            $tabletipe  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
            $namatipe  = array(null,'Alkes','Implan','Obat','Logistik');

            $this->db->select('det.idtipe,det.idbarang,det.kuantitas,det.harga');
            $this->db->where('det.idpemesanan', $idpemesanan);
            $result = $this->db->get('tgudang_pemesanan_detail det')->result_array();
            foreach ($result as $r) {
                $item = array();

                $aksi = '';
                if($r['idtipe'] == 4) {
                    $aksi .= '<select name="konversisatuan" class="form-control">';
                    $aksi .= '<option value="0" selected>TIDAK</option>';
                    $aksi .= '</select>';
                } else {
                    $aksi .= '<select name="konversisatuan" class="form-control">';
                    $aksi .= '<option value="1">YA</option>';
                    $aksi .= '<option value="0" selected>TIDAK</option>';
                    $aksi .= '</select>';
                }

                $item[] = $namatipe[$r['idtipe']];
                $item[] = $this->getNamaBarang($r['idtipe'], $r['idbarang']);
                $item[] = $r['kuantitas'];
                $item[] = $aksi;
                $item[] = $r['idtipe'];
                $item[] = $r['idbarang'];
                $item[] = 0;
                $items[] = $item;
            }

            $data = array('data' => $items);
            $this->output->set_output(json_encode($data));
        }
    }
    public function getdistributor()
    {
        $this->db->select('id, nama as text');
        $this->db->where('status', 1);
        if ($this->input->get('q')) {
            $this->db->like('nama', $this->input->get('q'), 'BOTH');
        }
        $res = $this->db->get('mdistributor')->result();
        $this->output->set_output(json_encode($res));
    }

    public function save_konfirmasi_pemesanan_draft()
    {
        $tablebarang = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        $detailvalue = json_decode($this->input->post('detailvalue'));
        $iddistributor = $this->input->post('iddistributor');

        foreach ($detailvalue as $r) {
            $kuantitas          = $r[2];
            $idtipe             = $r[4];
            $idbarang           = $r[5];
            $konversisatuan     = $r[6];
            $harga              = 0;
            $opsisatuan         = 1;
            $tipepemesanan      = 1;

            if ($idtipe == 4) {
                $tipepemesanan = 2;
            }
            if ($konversisatuan == 1) {
                $this->db->where('id', $idbarang);
                $detailbarang = $this->db->get($tablebarang[$idtipe], 1)->row();
                $harga = $detailbarang->hargasatuanbesar;
                $jumlahsatuanbesar = $detailbarang->jumlahsatuanbesar;
                $var1 = $kuantitas/$jumlahsatuanbesar;

                $qty = 0;
                if ($var1 < 1) {
                    $qty = 1;
                } else {
                    $qty = ceil($var1);
                }
                
                $opsisatuan = 2;
                $kuantitas = $qty;
            } else {
                $this->db->where('id', $idbarang);
                $detailbarang = $this->db->get($tablebarang[$idtipe], 1)->row();
                $harga = $detailbarang->hargabeli;
                $opsisatuan = 1;
            }
            

            $this->db->where('iddistributor', $iddistributor);
            $this->db->where('tipepemesanan', $tipepemesanan);
            $this->db->where('stdraft !=', 1);
            $this->db->where('stdraft !=', 2);
            $this->db->where('status', 1);
            $pemesanan = $this->db->get('tgudang_pemesanan');
            if ($pemesanan->num_rows() > 0) {
                $rowpemesanan = $pemesanan->row();
                $totalharga = $kuantitas*$harga;
                $this->db->set('totalbarang', '`totalbarang` + '.$kuantitas, false);
                $this->db->set('totalharga', '`totalharga` + '.$totalharga, false);
                $this->db->where('id', $rowpemesanan->id);
                $updatehead = $this->db->update('tgudang_pemesanan');
                if ($updatehead) {
                    $this->db->where('idpemesanan', $rowpemesanan->id);
                    $this->db->where('idtipe', $idtipe);
                    $this->db->where('idbarang', $idbarang);
                    $this->db->where('opsisatuan', $opsisatuan);
                    $pemesanandetail = $this->db->get('tgudang_pemesanan_detail');
                    if ($pemesanandetail->num_rows() > 0) {
                        $rowpemesanandetail = $pemesanandetail->row();
                        $this->db->set('kuantitas', '`kuantitas` + '.$kuantitas, false);
                        $this->db->set('harga', $harga);
                        $this->db->set('opsisatuan', $opsisatuan);
                        $this->db->set('idtipe', $idtipe);
                        $this->db->set('idbarang', $idbarang);
                        $this->db->where('idtipe', $idtipe);
                        $this->db->where('idbarang', $idbarang);
                        $this->db->where('idpemesanan', $rowpemesanan->id);
                        $updatedetail = $this->db->update('tgudang_pemesanan_detail');
                        ($updatedetail) ? $status = true : $status = false;
                    } else {
                        $rowpemesanandetail = $pemesanandetail->row();
                        $this->db->set('kuantitas', $kuantitas);
                        $this->db->set('harga', $harga);
                        $this->db->set('opsisatuan', $opsisatuan);
                        $this->db->set('idpemesanan', $rowpemesanan->id);
                        $this->db->set('idtipe', $idtipe);
                        $this->db->set('idbarang', $idbarang);
                        $this->db->set('status', 1);
                        $insertdetail = $this->db->insert('tgudang_pemesanan_detail');
                        ($insertdetail) ? $status = true : $status = false;
                    }
                }
            } else {
                $totalharga = $kuantitas*$harga;
                $this->db->set('nopemesanan', '-');
                $this->db->set('tipepemesanan', $tipepemesanan);
                $this->db->set('tanggal', date('Y-m-d H:i:s'));
                $this->db->set('iddistributor', $iddistributor);
                $this->db->set('totalbarang', $kuantitas);
                $this->db->set('totalharga', $totalharga);
                $this->db->set('status', 1);
                $inserthead = $this->db->insert('tgudang_pemesanan');
                if ($inserthead) {
                    $insertid = $this->db->insert_id();
                    $this->db->set('idpemesanan', $insertid);
                    $this->db->set('opsisatuan', $opsisatuan);
                    $this->db->set('idtipe', $idtipe);
                    $this->db->set('idbarang', $idbarang);
                    $this->db->set('kuantitas', $kuantitas);
                    $this->db->set('harga', $harga);
                    $this->db->set('status', 1);
                    $insertdetail = $this->db->insert('tgudang_pemesanan_detail');
                    ($insertdetail) ? $status = true : $status = false;
                }
            }
        }

        $this->db->set('stdraft', 2);
        $this->db->where('id', $this->input->post('id'));
        $updatestatusdraft = $this->db->update('tgudang_pemesanan');
        ($updatestatusdraft) ? $status = true : $status = false;

        return $status;
    }
	public function save_konfirmasi_pemesanan_draft2()
    {
		// print_r($this->input->post());exit();
		$kuantitas_pesan=$this->input->post('kuantitas_pesan');
		$konversi=$this->input->post('konversi');
		$id_det=$this->input->post('id_det');
		foreach ($id_det as $key => $val) {			
			$data_update=array();
			$data_update['opsisatuan']=$konversi[$key];
			$data_update['kuantitas']=$kuantitas_pesan[$key];
			$data_update['st_konversi']='1';
			$this->db->where('id', $id_det[$key]);
			$this->db->update('tgudang_pemesanan_detail',$data_update); 
			
		}
		$this->db->set('stdraft', 0);
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('tgudang_pemesanan');
        return true;
    }

    public function getsorted_distributor($mode){
        $idbarang=$this->input->post("idbarang");
        $idtipe=$this->input->post("idtipe");

        $qry="(SELECT m.id,CONCAT(m.nama,' (',IFNULL(r.total,0),')') as 'text'
            FROM mdistributor m 
            LEFT JOIN
            (
                SELECT p.iddistributor,COUNT(p.iddistributor) as 'total'
                FROM tgudang_pemesanan p 
                JOIN tgudang_pemesanan_detail d ON p.id=d.idpemesanan
                WHERE p.id IS NOT NULL
                AND d.idbarang ='".$idbarang."'  
                AND d.idtipe = '".$idtipe."'
                AND p.`status` NOT IN (0,1)
                GROUP BY p.iddistributor
            ) r ON r.iddistributor=m.id
            ORDER BY r.total DESC) tbl";
        $res=$this->db->get($qry)->result();
        if($mode=='json')
            $this->output->set_output(json_encode($res));
        else{
            $data = "<option value=''>Pilih Opsi</option>";
            foreach ($res as $r)
                $data .= "<option value='".$r->id."'>".$r->text."</option>";
            echo $data;
        }
    }
	public function view_unit() {
        $iddet             = $this->input->post('iddet');
        // $iddet             = '10';
		 if ($iddet) {
			
			$q="(SELECT H.id,H.no_up_permintaan,H.unit_peminta,U.nama as unit,H.kuantitas,H.opsisatuan,B.nama as namabarang,
				CASE WHEN H.opsisatuan='1' THEN sk.nama ELSE sb.nama END as satuan,CASE WHEN H.opsisatuan='1' THEN 'KECIL' ELSE 'BESAR' END as jenis_satuan
				from tgudang_pemesanan_history H
				LEFT JOIN munitpelayanan U ON U.id=H.unit_peminta
				LEFT JOIN view_barang B ON B.id=H.idbarang AND B.idtipe=H.idtipe
				LEFT JOIN msatuan sk ON sk.id=B.idsatuan
				LEFT JOIN msatuan sb ON sb.id=B.idsatuanbesar
				WHERE H.iddet_gudang='$iddet')T ";
			$this->load->library('datatables'); 
			$this->datatables->select('no_up_permintaan,unit,kuantitas,opsisatuan,satuan,jenis_satuan'); 
			$this->datatables->from($q); 
			$this->datatables->add_column("Actions", "<center></center>", "id");
			$this->datatables->unset_column('id');
			// echo $this->datatables->generate();
			
			 return print($this->datatables->generate());
			// $query=$this->db->query($q);
			// $res=$query->result();
			// $this->output->set_output(json_encode($res));
		 }
    }
}

/* End of file Tgudang_pemesanan_model.php */
/* Location: ./application/models/Tgudang_pemesanan_model.php */
