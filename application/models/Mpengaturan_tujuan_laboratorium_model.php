<?php

declare(strict_types=1);

class Mpengaturan_tujuan_laboratorium_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $this->db->where('status', '1');
        $query = $this->db->get('merm_pengaturan_tujuan_laboratorium');

        return $query->row();
    }

    public function getListAudio($id)
    {
        $this->db->select('merm_pengaturan_tujuan_laboratorium_audio.*, antrian_asset_sound.nama_asset AS file_audio_name');
        $this->db->join('antrian_asset_sound', 'antrian_asset_sound.id = merm_pengaturan_tujuan_laboratorium_audio.file_audio');
        $this->db->where('merm_pengaturan_tujuan_laboratorium_audio.idpengaturan', $id);
        $this->db->where('merm_pengaturan_tujuan_laboratorium_audio.status', 1);
        $query = $this->db->get('merm_pengaturan_tujuan_laboratorium_audio');

        return $query->result();
    }

    public function getListDokterLaboratorium($id)
    {
        $this->db->select('merm_pengaturan_tujuan_laboratorium_dokter.*, mdokter.nama AS nama_dokter');
        $this->db->join('mdokter', 'mdokter.id = merm_pengaturan_tujuan_laboratorium_dokter.iddokter');
        $this->db->where('merm_pengaturan_tujuan_laboratorium_dokter.idpengaturan', $id);
        $this->db->where('merm_pengaturan_tujuan_laboratorium_dokter.status', 1);
        $query = $this->db->get('merm_pengaturan_tujuan_laboratorium_dokter');

        return $query->result();
    }

    public function getListAksesUnit($id)
    {
        $this->db->select('merm_pengaturan_tujuan_laboratorium_unit_akses.*,
            (CASE
                WHEN merm_pengaturan_tujuan_laboratorium_unit_akses.asal_pasien IN (1, 2) THEN
                    mpoliklinik.nama
                ELSE
                    mkelas.nama
            END) AS nama_unit,
            mdokter.nama AS nama_dokter');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = merm_pengaturan_tujuan_laboratorium_unit_akses.idpoliklinik AND merm_pengaturan_tujuan_laboratorium_unit_akses.asal_pasien IN (1, 2)', 'LEFT');
        $this->db->join('mkelas', 'mkelas.id = merm_pengaturan_tujuan_laboratorium_unit_akses.idpoliklinik AND merm_pengaturan_tujuan_laboratorium_unit_akses.asal_pasien IN (3, 4)', 'LEFT');
        $this->db->join('mdokter', 'mdokter.id = merm_pengaturan_tujuan_laboratorium_unit_akses.iddokter', 'LEFT');
        $this->db->where('merm_pengaturan_tujuan_laboratorium_unit_akses.idpengaturan', $id);
        $this->db->where('merm_pengaturan_tujuan_laboratorium_unit_akses.status', 1);
        $query = $this->db->get('merm_pengaturan_tujuan_laboratorium_unit_akses');

        return $query->result();
    }

    public function getUserLaboratoriumAkses($id)
    {
        $this->db->select('merm_pengaturan_tujuan_laboratorium_user_akses.iduser AS id');
        $this->db->where('merm_pengaturan_tujuan_laboratorium_user_akses.idpengaturan', $id);
        $this->db->where('merm_pengaturan_tujuan_laboratorium_user_akses.status', 1);
        $query = $this->db->get('merm_pengaturan_tujuan_laboratorium_user_akses');
        $result_array = $query->result_array();

        return array_column($result_array, 'id');
    }

    public function getTipeLayananAkses($id)
    {
        $this->db->select('merm_pengaturan_tujuan_laboratorium_tipe_layanan.idtipe AS id');
        $this->db->where('merm_pengaturan_tujuan_laboratorium_tipe_layanan.idpengaturan', $id);
        $this->db->where('merm_pengaturan_tujuan_laboratorium_tipe_layanan.status', 1);
        $query = $this->db->get('merm_pengaturan_tujuan_laboratorium_tipe_layanan');
        $result_array = $query->result_array();

        return array_column($result_array, 'id');
    }

    public function saveData()
    {
        $this->nama = $this->input->post('nama');
        $this->tipe_layanan = $this->input->post('tipe_layanan');
        $this->tujuan = $this->input->post('tujuan');
        $this->status_file_audio = $this->input->post('status_file_audio');
        $this->dokter_penanggung_jawab = $this->input->post('dokter_penanggung_jawab');
        $this->created_by = $this->session->userdata('user_id');
        $this->created_date = date('Y-m-d H:i:s');

        if ($this->db->insert('merm_pengaturan_tujuan_laboratorium', $this)) {
            $idpengaturan = $this->db->insert_id();

            $this->saveAudioData($idpengaturan, json_decode($_POST['audio_data'], true));
            $this->saveDokterLaboratoriumData($idpengaturan, json_decode($_POST['dokter_laboratorium_data'], true));
            $this->saveAksesUnitData($idpengaturan, json_decode($_POST['akses_unit_data'], true));
            $this->saveUserLaboratoriumAkses($idpengaturan, $_POST['user_laboratorium_akses']);
            $this->saveTipeLayananAkses($idpengaturan, $_POST['tipe_layanan_akses']);
        }

        return true;
    }

    public function updateData()
    {
        $idpengaturan = $this->input->post('id');

        $data = array();
        $data['nama'] = $this->input->post('nama');
        $data['tipe_layanan'] = $this->input->post('tipe_layanan');
        $data['tujuan'] = $this->input->post('tujuan');
        $data['status_file_audio'] = $this->input->post('status_file_audio');
        $data['dokter_penanggung_jawab'] = $this->input->post('dokter_penanggung_jawab');
        $data['edited_by'] = $this->session->userdata('user_id');
        $data['edited_date'] = date('Y-m-d H:i:s');

        $this->db->where('id', $idpengaturan);
        if ($this->db->update('merm_pengaturan_tujuan_laboratorium', $data)) {
            
            $this->saveAudioData($idpengaturan, json_decode($_POST['audio_data'], true));
            $this->saveDokterLaboratoriumData($idpengaturan, json_decode($_POST['dokter_laboratorium_data'], true));
            $this->saveAksesUnitData($idpengaturan, json_decode($_POST['akses_unit_data'], true));
            $this->saveUserLaboratoriumAkses($idpengaturan, $_POST['user_laboratorium_akses']);
            $this->saveTipeLayananAkses($idpengaturan, $_POST['tipe_layanan_akses']);
        }
        
        return true;
    }

    public function softDelete($id)
    {
        $this->status = 0;
        $this->deleted_by = $this->session->userdata('user_id');
        $this->deleted_date = date('Y-m-d H:i:s');

        if ($this->db->update('merm_pengaturan_tujuan_laboratorium', $this, ['id' => $id])) {
            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;
    }

    private function saveAudioData($idpengaturan, $data)
    {
        $this->db->where('idpengaturan', $idpengaturan);
        $this->db->delete('merm_pengaturan_tujuan_laboratorium_audio');
        
        foreach ($data as $row) {
            $detail = [
                'idpengaturan' => $idpengaturan,
                'nourut' => $row['urutan'],
                'file_audio' => $row['file_audio'],
                'status' => 1,
            ];

            $this->db->insert('merm_pengaturan_tujuan_laboratorium_audio', $detail);
        }
    }

    private function saveDokterLaboratoriumData($idpengaturan, $data)
    {
        $this->db->where('idpengaturan', $idpengaturan);
        $this->db->delete('merm_pengaturan_tujuan_laboratorium_dokter');
        
        foreach ($data as $row) {
            $detail = [
                'idpengaturan' => $idpengaturan,
                'iddokter' => $row['iddokter'],
                'status_default' => $row['status_default'],
                'status' => 1,
            ];

            $this->db->insert('merm_pengaturan_tujuan_laboratorium_dokter', $detail);
        }
    }

    private function saveAksesUnitData($idpengaturan, $data)
    {
        $this->db->where('idpengaturan', $idpengaturan);
        $this->db->delete('merm_pengaturan_tujuan_laboratorium_unit_akses');

        foreach ($data as $row) {
            $detail = [
                'idpengaturan' => $idpengaturan,
                'tipe_layanan' => $row['tipe_layanan'],
                'asal_pasien' => $row['asal_pasien'],
                'idpoliklinik' => $row['poliklinik'],
                'iddokter' => $row['dokter'],
                'status' => 1,
            ];

            $this->db->insert('merm_pengaturan_tujuan_laboratorium_unit_akses', $detail);
        }
    }

    private function saveUserLaboratoriumAkses($idpengaturan, $data)
    {
        $this->db->where('idpengaturan', $idpengaturan);
        $this->db->delete('merm_pengaturan_tujuan_laboratorium_user_akses');
        
        foreach ($data as $row) {
            $detail = [
                'idpengaturan' => $idpengaturan,
                'iduser' => $row,
                'status' => 1,
            ];

            $this->db->insert('merm_pengaturan_tujuan_laboratorium_user_akses', $detail);
        }
    }

    private function saveTipeLayananAkses($idpengaturan, $data)
    {
        $this->db->where('idpengaturan', $idpengaturan);
        $this->db->delete('merm_pengaturan_tujuan_laboratorium_tipe_layanan');
        
        foreach ($data as $row) {
            $detail = [
                'idpengaturan' => $idpengaturan,
                'idtipe' => $row,
                'status' => 1,
            ];

            $this->db->insert('merm_pengaturan_tujuan_laboratorium_tipe_layanan', $detail);
        }
    }
}
