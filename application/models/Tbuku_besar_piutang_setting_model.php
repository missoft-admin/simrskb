<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tbuku_besar_piutang_setting_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($idkelompok,$idrekanan)
    {
		// $where="AND M.id='".$tipe."-".$id."'";
        $q="SELECT CONCAT(T.idkelompok,'-',T.idrekanan) as id, T.*,K.nama as nama_kelompok,K.st_multiple FROM (
				SELECT '1' as urutan,H.id as idkelompok,H.id as idrekanan,H.nama  FROM mpasien_kelompok H
				WHERE H.`status`='1' AND H.id NOT IN (1,5)

				UNION ALL

				SELECT '2' as urutan,1 as idkelompok,H.id as idrekanan,H.nama FROM mrekanan H
				WHERE H.`status`='1' 
				) T
				LEFT JOIN mpasien_kelompok K ON K.id=T.idkelompok
				WHERE T.idkelompok='$idkelompok' AND T.idrekanan='$idrekanan'
				ORDER BY T.urutan,T.idkelompok,T.nama";
			// print_r($q);exit();
        return $this->db->query($q)->row_array();
    }
	function list_KP(){
		$q="SELECT H.id,H.nama  FROM mpasien_kelompok H
WHERE H.`status`='1' AND H.id NOT IN (5)";
		return $this->db->query($q)->result();
			
	}
	function list_asuransi(){
		$q="SELECT CONCAT(T.idkelompok,'-',T.idrekanan) as id, T.*,0 as debet,0 as kredit,K.nama as nama_kelompok,1 as st_verifikasi FROM (
				SELECT '1' as urutan,H.id as idkelompok,H.id as idrekanan,H.nama  FROM mpasien_kelompok H
				WHERE H.`status`='1' AND H.id NOT IN (1,5)

				UNION ALL

				SELECT '2' as urutan,1 as idkelompok,H.id as idrekanan,H.nama FROM mrekanan H
				WHERE H.`status`='1' 
				) T
				LEFT JOIN mpasien_kelompok K ON K.id=T.idkelompok
				ORDER BY T.urutan,T.idkelompok,T.nama
";
		return $this->db->query($q)->result();
			
	}
	function list_rekanan(){
		$q="

				SELECT H.id,H.nama FROM mrekanan H
				WHERE H.`status`='1' ORDER BY H.nama 
				
";
		return $this->db->query($q)->result();
			
	}
	function list_pasien($cari){
		$q="SELECT M.id,CONCAT(M.no_medrec,' - ',M.nama) as nama FROM mfpasien M 
		WHERE M.`status`='1' AND (M.no_medrec LIKE '%".$cari."%' OR M.nama LIKE '%".$cari."%' )
		LIMIT 100";
		return $this->db->query($q)->result();
	}
    public function saveData()
    {
        $this->kode 		= $_POST['kode'];
        $this->nama 		= $_POST['nama'];
        $this->alamat 	= $_POST['alamat'];
        $this->telepon 	= $_POST['telepon'];
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mdistributor', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->kode 		= $_POST['kode'];
        $this->nama 		= $_POST['nama'];
        $this->alamat 	= $_POST['alamat'];
        $this->telepon 	= $_POST['telepon'];
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mdistributor', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mdistributor', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function load_all()
    {
        $query = $this->db->get('mdistributor');
        return $query->result();
    }
}
