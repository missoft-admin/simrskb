<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Antrian_poli_kode_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function list_dokter($idpoli){
		$q="SELECT H.*,MD.nama FROM antrian_poli_kode_dokter H
			LEFT JOIN mdokter MD ON MD.id=H.iddokter
			WHERE H.idpoli='$idpoli'";
		return $this->db->query($q)->result();
	}
	function list_sound(){
		$q="SELECT H.* FROM antrian_asset_sound H
			WHERE H.status='1' ";
		return $this->db->query($q)->result();
	}
	function get_poli($idpoli){
		$q="SELECT MP.id as idpoli,MP.nama as nama_poli,MP.idtipe,H.kodeantrian_poli,H.sound_poli_id 
		FROM antrian_poli_kode H LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli 
		WHERE MP.id='$idpoli'
		";
		return $this->db->query($q)->row_array();
	}
	function updateData(){
		$idpoli = $this->input->post('idpoliklinik');
		$this->kodeantrian_poli = $this->input->post('kodeantrian_poli');
		// $this->sound_poli_id = $this->input->post('sound_poli_id');
		$this->db->where('idpoli', $idpoli);
			
		if ($this->db->update('antrian_poli_kode', $this)) {
			$iddokter = $this->input->post('iddokter');
			$idpoli = $this->input->post('idpoli');
			$sound_dokter_id = $this->input->post('sound_dokter_id');
			$kodeantrian_dokter = $this->input->post('kodeantrian_dokter');
			foreach($iddokter as $index=>$val){
				$data_edit=array(
					'kodeantrian_dokter'=>$kodeantrian_dokter[$index],
					'sound_dokter_id'=>$sound_dokter_id[$index],
				);
				$this->db->where('iddokter',$val);
				$this->db->where('idpoli',$idpoli[$index]);
				$this->db->update('antrian_poli_kode_dokter',$data_edit);
			}
			
			return true;
		} else {
			return false;
		}
			
	}
	// public function get_login_setting(){
		// $q="SELECT H.id, H.login_logo,H.login_judul,H.login_gambar_side,H.login_salah FROM app_setting H WHERE H.id='1'";
		// return $this->db->query($q)->row_array();
	// }
	// public function get_about_setting(){
		// $q="SELECT H.id, H.about_us_judul,H.about_us_isi FROM app_setting H WHERE H.id='1'";
		// return $this->db->query($q)->row_array();
	// }
	// public function get_index_setting(){
		// $q="SELECT id,index_logo,index_pesan,index_icon_1,index_icon_2,index_icon_3,index_icon_4,index_icon_5,index_icon_6,index_icon_7,index_icon_8
			 // FROM app_setting
			// WHERE id='1'";
		// return $this->db->query($q)->row_array();
	// }
	
	
	// function save_login(){
		// $id = $this->input->post('id');
		// $this->login_judul = $this->input->post('login_judul');
		// $this->login_salah = $this->input->post('login_salah');
		// $this->upload_login_logo(true);
		// $this->upload_login_bg(true);
		// $this->db->where('id', $id);
			
		// if ($this->db->update('app_setting', $this)) {
			// return true;
		// } else {
			// return false;
		// }
			
	// }
	
	// public function upload_login_logo($update = false)
    // {
        // if (!file_exists('assets/upload/app_setting')) {
            // mkdir('assets/upload/app_setting', 0755, true);
        // }

        // if (isset($_FILES['login_logo'])) {
            // if ($_FILES['login_logo']['name'] != '') {
                // $config['upload_path'] = './assets/upload/app_setting/';
				// // $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	// $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				// $config['encrypt_name']  = TRUE;
				// $config['overwrite']  = FALSE;
				// $this->upload->initialize($config);
				// // $this->load->library('upload', $config);
				// // $this->load->library('myimage');	
				

                // $this->load->library('upload', $config);
				// // print_r	($config['upload_path']);exit;
                // if ($this->upload->do_upload('login_logo')) {
                    // $image_upload = $this->upload->data();
                    // $this->login_logo = $image_upload['file_name'];

                    // if ($update == true) {
                        // $this->remove_image_login_logo($this->input->post('id'));
                    // }
                    // return true;
                // } else {
					// print_r	($this->upload->display_errors());exit;
                    // $this->error_message = $this->upload->display_errors();
                    // return false;
                // }
            // } else {
                // return true;
            // }
					// // print_r($this->foto);exit;
        // } else {
            // return true;
        // }
		
    // }
	// public function remove_image_login_logo($id)
    // {
		// $q="select login_logo From app_setting H WHERE H.id='$id'";
        // $row = $this->db->query($q)->row();
        // if (file_exists('./assets/upload/app_setting/'.$row->login_logo) && $row->login_logo !='') {
            // unlink('./assets/upload/app_setting/'.$row->login_logo);
        // }
    // }
	// public function upload_login_bg($update = false)
    // {
        // if (!file_exists('assets/upload/app_setting')) {
            // mkdir('assets/upload/app_setting', 0755, true);
        // }

        // if (isset($_FILES['login_gambar_side'])) {
            // if ($_FILES['login_gambar_side']['name'] != '') {
                // $config['upload_path'] = './assets/upload/app_setting/';
				// // $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	// $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				// $config['encrypt_name']  = TRUE;
				// $config['overwrite']  = FALSE;
				// $this->upload->initialize($config);
				// // $this->load->library('upload', $config);
				// // $this->load->library('myimage');	
				

                // $this->load->library('upload', $config);
				// // print_r	($config['upload_path']);exit;
                // if ($this->upload->do_upload('login_gambar_side')) {
                    // $image_upload = $this->upload->data();
                    // $this->login_gambar_side = $image_upload['file_name'];

                    // if ($update == true) {
                        // $this->remove_image_login_bg($this->input->post('id'));
                    // }
                    // return true;
                // } else {
					// print_r	($this->upload->display_errors());exit;
                    // $this->error_message = $this->upload->display_errors();
                    // return false;
                // }
            // } else {
                // return true;
            // }
					// // print_r($this->foto);exit;
        // } else {
            // return true;
        // }
		
    // }
	// public function remove_image_login_bg($id)
    // {
		// $q="select login_gambar_side From app_setting H WHERE H.id='$id'";
        // $row = $this->db->query($q)->row();
        // if (file_exists('./assets/upload/app_setting/'.$row->login_gambar_side) && $row->login_gambar_side !='') {
            // unlink('./assets/upload/app_setting/'.$row->login_gambar_side);
        // }
    // }
	// function save_index(){
		// $id = $this->input->post('id');
		// $this->index_pesan = $this->input->post('index_pesan');
		
		// $this->upload_index_logo(true);
		// $this->upload_index_icon_1(true);
		// $this->upload_index_icon_2(true);
		// $this->upload_index_icon_3(true);
		// $this->upload_index_icon_4(true);
		// $this->upload_index_icon_5(true);
		// $this->upload_index_icon_6(true);
		// $this->upload_index_icon_7(true);
		// $this->upload_index_icon_8(true);
		
		// $this->db->where('id', $id);
			
		// if ($this->db->update('app_setting', $this)) {
			// return true;
		// } else {
			// return false;
		// }
			
	// }
    // public function upload_index_logo($update = false)
    // {
        // if (!file_exists('assets/upload/app_setting')) {
            // mkdir('assets/upload/app_setting', 0755, true);
        // }

        // if (isset($_FILES['index_logo'])) {
            // if ($_FILES['index_logo']['name'] != '') {
                // $config['upload_path'] = './assets/upload/app_setting/';
				// // $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	// $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				// $config['encrypt_name']  = TRUE;
				// $config['overwrite']  = FALSE;
				// $this->upload->initialize($config);
                // $this->load->library('upload', $config);
				// // print_r	($config['upload_path']);exit;
                // if ($this->upload->do_upload('index_logo')) {
                    // $image_upload = $this->upload->data();
                    // $this->index_logo = $image_upload['file_name'];

                    // return true;
                // } else {
					// print_r	($this->upload->display_errors());exit;
                    // $this->error_message = $this->upload->display_errors();
                    // return false;
                // }
            // } else {
                // return true;
            // }
					// // print_r($this->foto);exit;
        // } else {
            // return true;
        // }
		
    // }
	// public function upload_index_icon_1($update = false)
    // {
        // if (!file_exists('assets/upload/app_setting')) {
            // mkdir('assets/upload/app_setting', 0755, true);
        // }

        // if (isset($_FILES['index_icon_1'])) {
            // if ($_FILES['index_icon_1']['name'] != '') {
                // $config['upload_path'] = './assets/upload/app_setting/';
				// // $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	// $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				// $config['encrypt_name']  = TRUE;
				// $config['overwrite']  = FALSE;
				// $this->upload->initialize($config);
                // $this->load->library('upload', $config);
				// // print_r	($config['upload_path']);exit;
                // if ($this->upload->do_upload('index_icon_1')) {
                    // $image_upload = $this->upload->data();
                    // $this->index_icon_1 = $image_upload['file_name'];

                    // return true;
                // } else {
					// print_r	($this->upload->display_errors());exit;
                    // $this->error_message = $this->upload->display_errors();
                    // return false;
                // }
            // } else {
                // return true;
            // }
					// // print_r($this->foto);exit;
        // } else {
            // return true;
        // }
		
    // }
	// public function upload_index_icon_2($update = false)
    // {
        // if (!file_exists('assets/upload/app_setting')) {
            // mkdir('assets/upload/app_setting', 0755, true);
        // }

        // if (isset($_FILES['index_icon_2'])) {
            // if ($_FILES['index_icon_2']['name'] != '') {
                // $config['upload_path'] = './assets/upload/app_setting/';
				// // $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	// $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				// $config['encrypt_name']  = TRUE;
				// $config['overwrite']  = FALSE;
				// $this->upload->initialize($config);
                // $this->load->library('upload', $config);
				// // print_r	($config['upload_path']);exit;
                // if ($this->upload->do_upload('index_icon_2')) {
                    // $image_upload = $this->upload->data();
                    // $this->index_icon_2 = $image_upload['file_name'];

                    // return true;
                // } else {
					// print_r	($this->upload->display_errors());exit;
                    // $this->error_message = $this->upload->display_errors();
                    // return false;
                // }
            // } else {
                // return true;
            // }
					// // print_r($this->foto);exit;
        // } else {
            // return true;
        // }
		
    // }
	// public function upload_index_icon_3($update = false)
    // {
        // if (!file_exists('assets/upload/app_setting')) {
            // mkdir('assets/upload/app_setting', 0755, true);
        // }

        // if (isset($_FILES['index_icon_3'])) {
            // if ($_FILES['index_icon_3']['name'] != '') {
                // $config['upload_path'] = './assets/upload/app_setting/';
				// // $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	// $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				// $config['encrypt_name']  = TRUE;
				// $config['overwrite']  = FALSE;
				// $this->upload->initialize($config);
                // $this->load->library('upload', $config);
				// // print_r	($config['upload_path']);exit;
                // if ($this->upload->do_upload('index_icon_3')) {
                    // $image_upload = $this->upload->data();
                    // $this->index_icon_3 = $image_upload['file_name'];

                    // return true;
                // } else {
					// print_r	($this->upload->display_errors());exit;
                    // $this->error_message = $this->upload->display_errors();
                    // return false;
                // }
            // } else {
                // return true;
            // }
					// // print_r($this->foto);exit;
        // } else {
            // return true;
        // }
		
    // }
	// public function upload_index_icon_4($update = false)
    // {
        // if (!file_exists('assets/upload/app_setting')) {
            // mkdir('assets/upload/app_setting', 0755, true);
        // }

        // if (isset($_FILES['index_icon_4'])) {
            // if ($_FILES['index_icon_4']['name'] != '') {
                // $config['upload_path'] = './assets/upload/app_setting/';
				// // $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	// $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				// $config['encrypt_name']  = TRUE;
				// $config['overwrite']  = FALSE;
				// $this->upload->initialize($config);
                // $this->load->library('upload', $config);
				// // print_r	($config['upload_path']);exit;
                // if ($this->upload->do_upload('index_icon_4')) {
                    // $image_upload = $this->upload->data();
                    // $this->index_icon_4 = $image_upload['file_name'];

                    // return true;
                // } else {
					// print_r	($this->upload->display_errors());exit;
                    // $this->error_message = $this->upload->display_errors();
                    // return false;
                // }
            // } else {
                // return true;
            // }
					// // print_r($this->foto);exit;
        // } else {
            // return true;
        // }
		
    // }
	// public function upload_index_icon_5($update = false)
    // {
        // if (!file_exists('assets/upload/app_setting')) {
            // mkdir('assets/upload/app_setting', 0755, true);
        // }

        // if (isset($_FILES['index_icon_5'])) {
            // if ($_FILES['index_icon_5']['name'] != '') {
                // $config['upload_path'] = './assets/upload/app_setting/';
				// // $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	// $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				// $config['encrypt_name']  = TRUE;
				// $config['overwrite']  = FALSE;
				// $this->upload->initialize($config);
                // $this->load->library('upload', $config);
				// // print_r	($config['upload_path']);exit;
                // if ($this->upload->do_upload('index_icon_5')) {
                    // $image_upload = $this->upload->data();
                    // $this->index_icon_5 = $image_upload['file_name'];

                    // return true;
                // } else {
					// print_r	($this->upload->display_errors());exit;
                    // $this->error_message = $this->upload->display_errors();
                    // return false;
                // }
            // } else {
                // return true;
            // }
					// // print_r($this->foto);exit;
        // } else {
            // return true;
        // }
		
    // }
	// public function upload_index_icon_6($update = false)
    // {
        // if (!file_exists('assets/upload/app_setting')) {
            // mkdir('assets/upload/app_setting', 0755, true);
        // }

        // if (isset($_FILES['index_icon_6'])) {
            // if ($_FILES['index_icon_6']['name'] != '') {
                // $config['upload_path'] = './assets/upload/app_setting/';
				// // $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	// $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				// $config['encrypt_name']  = TRUE;
				// $config['overwrite']  = FALSE;
				// $this->upload->initialize($config);
                // $this->load->library('upload', $config);
				// // print_r	($config['upload_path']);exit;
                // if ($this->upload->do_upload('index_icon_6')) {
                    // $image_upload = $this->upload->data();
                    // $this->index_icon_6 = $image_upload['file_name'];

                    // return true;
                // } else {
					// print_r	($this->upload->display_errors());exit;
                    // $this->error_message = $this->upload->display_errors();
                    // return false;
                // }
            // } else {
                // return true;
            // }
					// // print_r($this->foto);exit;
        // } else {
            // return true;
        // }
		
    // }
	// public function upload_index_icon_7($update = false)
    // {
        // if (!file_exists('assets/upload/app_setting')) {
            // mkdir('assets/upload/app_setting', 0755, true);
        // }

        // if (isset($_FILES['index_icon_7'])) {
            // if ($_FILES['index_icon_7']['name'] != '') {
                // $config['upload_path'] = './assets/upload/app_setting/';
				// // $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	// $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				// $config['encrypt_name']  = TRUE;
				// $config['overwrite']  = FALSE;
				// $this->upload->initialize($config);
                // $this->load->library('upload', $config);
				// // print_r	($config['upload_path']);exit;
                // if ($this->upload->do_upload('index_icon_7')) {
                    // $image_upload = $this->upload->data();
                    // $this->index_icon_7 = $image_upload['file_name'];

                    // return true;
                // } else {
					// print_r	($this->upload->display_errors());exit;
                    // $this->error_message = $this->upload->display_errors();
                    // return false;
                // }
            // } else {
                // return true;
            // }
					// // print_r($this->foto);exit;
        // } else {
            // return true;
        // }
		
    // }
	// public function upload_index_icon_8($update = false)
    // {
        // if (!file_exists('assets/upload/app_setting')) {
            // mkdir('assets/upload/app_setting', 0755, true);
        // }

        // if (isset($_FILES['index_icon_8'])) {
            // if ($_FILES['index_icon_8']['name'] != '') {
                // $config['upload_path'] = './assets/upload/app_setting/';
				// // $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	// $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				// $config['encrypt_name']  = TRUE;
				// $config['overwrite']  = FALSE;
				// $this->upload->initialize($config);
                // $this->load->library('upload', $config);
				// // print_r	($config['upload_path']);exit;
                // if ($this->upload->do_upload('index_icon_8')) {
                    // $image_upload = $this->upload->data();
                    // $this->index_icon_8 = $image_upload['file_name'];

                    // return true;
                // } else {
					// print_r	($this->upload->display_errors());exit;
                    // $this->error_message = $this->upload->display_errors();
                    // return false;
                // }
            // } else {
                // return true;
            // }
					// // print_r($this->foto);exit;
        // } else {
            // return true;
        // }
		
    // }
	// function refresh_image(){
		// $q="SELECT  H.* from app_setting_file H

			// WHERE H.status='1'";
		// // print_r($q);exit();
		// $row= $this->db->query($q)->result();
		// $tabel='';
		// $no=1;
		// foreach ($row as $r){
			
			// $tabel .='<tr>';
			// $tabel .='<td class="text-right">'.$no.'</td>';
			// $tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/app_setting/'.$r->filename.'" target="_blank">'.substr($r->filename, 11).'</a></td>';
			// $tabel .='<td class="text-left">'.$r->upload_by_nama.'-'.HumanDateLong($r->upload_date).'</td>';
			// $tabel .='<td class="text-left">'.$r->size.'</td>';
			// $tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/app_setting/'.$r->filename.'" target="_blank" data-toggle="tooltip" title="Preview" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                    // <button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm " type="button" onclick="removeFile('.$r->id.')"><i class="fa fa-trash-o"></i></button></td>';
			// $tabel .='</tr>';
			// $no=$no+1;
		// }
		// return $tabel;
	// }
	// function saveDataInfo(){
		// // $id = $this->input->post('id');
		// if ($this->input->post('btn_simpan')=='2'){
			// $this->status = 2;
			// $this->published_by = $this->session->userdata('user_id');
			// $this->published_date = date('Y-m-d H:i:s');			
		// }else{
			// $this->status = 1;			
		// }
		// $this->judul = $this->input->post('judul');
		// $this->isi = $this->input->post('isi');
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		// $this->date_start = YMDFormat($this->input->post('date_start'));
		// $this->date_end = YMDFormat($this->input->post('date_end'));
		// $this->upload_gambar_info(false);
		// // $this->upload_login_logo(true);
		// // $this->upload_login_bg(true);
		// // $this->db->where('id', $id);
		
		// if ($this->db->insert('app_informasi', $this)) {
			// $id=$this->db->insert_id();
			// return $id;
		// } else {
			// return false;
		// }
			
	// }
	// function updateDataInfo(){
		// $id = $this->input->post('id');
		// $this->judul = $this->input->post('judul');
		// $this->status = $this->input->post('btn_simpan');
		// $this->isi = $this->input->post('isi');
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		// $this->date_start = YMDFormat($this->input->post('date_start'));
		// $this->date_end = YMDFormat($this->input->post('date_end'));
		// $this->upload_gambar_info(false);
		// // $this->upload_login_logo(true);
		// // $this->upload_login_bg(true);
		// $this->db->where('id', $id);
		
		// if ($this->db->update('app_informasi', $this)) {
			// // $id=$this->db->insert_id();
			// return true;
		// } else {
			// return false;
		// }
			
	// }
	// public function upload_gambar_info($update = false)
    // {
        // if (!file_exists('assets/upload/app_setting')) {
            // mkdir('assets/upload/app_setting', 0755, true);
        // }

        // if (isset($_FILES['gambar'])) {
            // if ($_FILES['gambar']['name'] != '') {
                // $config['upload_path'] = './assets/upload/app_setting/';
				// // $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	// $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				// $config['encrypt_name']  = TRUE;
				// $config['overwrite']  = FALSE;
				// $this->upload->initialize($config);
                // $this->load->library('upload', $config);
				// // print_r	($config['upload_path']);exit;
                // if ($this->upload->do_upload('gambar')) {
                    // $image_upload = $this->upload->data();
                    // $this->gambar = $image_upload['file_name'];

                    // return true;
                // } else {
					// print_r	($this->upload->display_errors());exit;
                    // $this->error_message = $this->upload->display_errors();
                    // return false;
                // }
            // } else {
                // return true;
            // }
					// // print_r($this->foto);exit;
        // } else {
            // return true;
        // }
		
    // }
	// function get_informasi($id){
		// $q="select *from app_informasi where id='$id'";
		// return $this->db->query($q)->row_array();
	// }
	
	// //EVENT
	
	// function saveDataEvent(){
		// // $id = $this->input->post('id');
		// if ($this->input->post('btn_simpan')=='2'){
			// $this->status = 2;
			// $this->published_by = $this->session->userdata('user_id');
			// $this->published_date = date('Y-m-d H:i:s');			
		// }else{
			// $this->status = 1;			
		// }
		// $this->judul = $this->input->post('judul');
		// $this->isi = $this->input->post('isi');
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		// $this->date_start = YMDFormat($this->input->post('date_start'));
		// $this->date_end = YMDFormat($this->input->post('date_end'));
		// $this->upload_gambar_event(false);
		// // $this->upload_login_logo(true);
		// // $this->upload_login_bg(true);
		// // $this->db->where('id', $id);
		
		// if ($this->db->insert('app_event', $this)) {
			// $id=$this->db->insert_id();
			// return $id;
		// } else {
			// return false;
		// }
			
	// }
	// function updateDataEvent(){
		// $id = $this->input->post('id');
		// $this->judul = $this->input->post('judul');
		// $this->status = $this->input->post('btn_simpan');
		// $this->isi = $this->input->post('isi');
		
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		// $this->date_start = YMDFormat($this->input->post('date_start'));
		// $this->date_end = YMDFormat($this->input->post('date_end'));
		// $this->upload_gambar_event(false);
		// // $this->upload_login_logo(true);
		// // $this->upload_login_bg(true);
		// $this->db->where('id', $id);
		
		// if ($this->db->update('app_event', $this)) {
			// // $id=$this->db->insert_id();
			// return true;
		// } else {
			// return false;
		// }
			
	// }
	// public function upload_gambar_event($update = false)
    // {
        // if (!file_exists('assets/upload/app_setting')) {
            // mkdir('assets/upload/app_setting', 0755, true);
        // }

        // if (isset($_FILES['gambar'])) {
            // if ($_FILES['gambar']['name'] != '') {
                // $config['upload_path'] = './assets/upload/app_setting/';
				// // $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	// $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				// $config['encrypt_name']  = TRUE;
				// $config['overwrite']  = FALSE;
				// $this->upload->initialize($config);
                // $this->load->library('upload', $config);
				// // print_r	($config['upload_path']);exit;
                // if ($this->upload->do_upload('gambar')) {
                    // $image_upload = $this->upload->data();
                    // $this->gambar = $image_upload['file_name'];

                    // return true;
                // } else {
					// print_r	($this->upload->display_errors());exit;
                    // $this->error_message = $this->upload->display_errors();
                    // return false;
                // }
            // } else {
                // return true;
            // }
					// // print_r($this->foto);exit;
        // } else {
            // return true;
        // }
		
    // }
	// function get_event($id){
		// $q="select *from app_event where id='$id'";
		// return $this->db->query($q)->row_array();
	// }
	
	// //PENDAFTARAN
	// public function get_pendaftaran_setting(){
		// $q="SELECT * FROM app_pendaftaran H WHERE H.id='1'";
		// return $this->db->query($q)->row_array();
	// }
	// function save_pendaftaran(){
		// $id =1;
		// $this->peraturan = $this->input->post('peraturan');
		// $this->pesan_berhasil = $this->input->post('pesan_berhasil');
		// $this->st_auto_validasi = $this->input->post('st_auto_validasi');
		// $this->st_antrian_tampil = $this->input->post('st_antrian_tampil');
		// $this->edited_by = $this->session->userdata('user_id');
		// $this->edited_date = date('Y-m-d H:i:s');
		// $this->db->where('id', $id);
			
		// if ($this->db->update('app_pendaftaran', $this)) {
			// return true;
		// } else {
			// return false;
		// }
			
	// }
	// public function get_pendaftaran_rm_setting(){
		// $q="SELECT * FROM app_pendaftaran_rm H WHERE H.id='1'";
		// return $this->db->query($q)->row_array();
	// }
	// function save_pendaftaran_rm(){
		// $id =1;
		// $this->peraturan = $this->input->post('peraturan');
		// $this->pesan_berhasil = $this->input->post('pesan_berhasil');
		// $this->st_auto_validasi = $this->input->post('st_auto_validasi');
		// $this->st_antrian_tampil = $this->input->post('st_antrian_tampil');
		// $this->edited_by = $this->session->userdata('user_id');
		// $this->edited_date = date('Y-m-d H:i:s');
		// $this->db->where('id', $id);
		// // print_r($this);exit;
		// if ($this->db->update('app_pendaftaran_rm', $this)) {
			// return true;
		// } else {
			// return false;
		// }
			
	// }
	
	
	// function get_mpoli($id){
		// $q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		// return $this->db->query($q)->row_array();
	// }
	// function list_poli(){
		// $q="SELECT A.idpoli,M.nama FROM antrian_poli_kode A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		// return $this->db->query($q)->result();
	// }
}
