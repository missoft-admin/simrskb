<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tmutasi_kas_approval_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
	public function simpan_proses_peretujuan($id) {

		$this->db->where('idtransaksi',$id);
		$this->db->delete('tmutasi_kas_approval');
		$q="SELECT S.iduser,S.step,U.`name` as user_nama,S.proses_setuju,S.proses_tolak
		FROM tmutasi_kas TR
		LEFT JOIN mlogic_mutasi S ON TR.dari=S.dari AND TR.ke=S.ke
		LEFT JOIN musers U ON U.id=S.iduser
		WHERE TR.id='$id' AND calculate_logic(S.operand, TR.nominal, S.nominal)='1' AND S.status='1'
		ORDER BY S.step,S.id";
		$list_user=$this->db->query($q)->result();
		$step=999;
		$idlogic='';
		foreach ($list_user as $row){
			if ($row->step < $step){
				$step=$row->step;
			}
			$data=array(
				'idtransaksi' => $id,
				'step' => $row->step,
				// 'idlogic' => $row->idlogic,
				'iduser' => $row->iduser,
				'user_nama' => $row->user_nama,
				'proses_setuju' => $row->proses_setuju,
				'proses_tolak' => $row->proses_tolak,
				'approve' => 0,
			);
			// $idlogic=$row->idlogic;
			$this->db->insert('tmutasi_kas_approval', $data);
		}

		$this->db->update('tmutasi_kas_approval',array('st_aktif'=>1),array('idtransaksi'=>$id,'step'=>$step));

		$data=array(
			'st_approval' => '1',
			'status_approval' => '0',
		);
		$this->db->where('id',$id);
		return $this->db->update('tmutasi_kas', $data);


    }
	public function insert_validasi_mutasi($id){
		$q="SELECT '5' as id_reff,
			H.id as idtransaksi,H.tanggal_trx as tanggal_transaksi,H.notransaksi as notransaksi
			,H.dari,H.ke,H.saldoawal_dari,saldoawal_ke
			,H.saldoakhir_dari,saldoakhir_ke
			,MD.nama as nama_dari,MK.nama as nama_ke
			,MD.idakun as idakun_dari,'K' as  posisi_akun_dari
			,MK.idakun as idakun_ke,'D' as  posisi_akun_ke,H.nominal
			,CONCAT('MUTASI KAS DARI ',MD.nama,' KE ',MK.nama,' (',H.notransaksi,')') as keterangan
			,S.st_auto_posting_mutasi as st_auto_posting
			From tmutasi_kas H
			LEFT JOIN msumber_kas MD ON MD.id=H.dari
			LEFT JOIN msumber_kas MK ON MK.id=H.ke
			LEFT JOIN msetting_jurnal_kas_lain S ON S.id='1'
			WHERE H.id='$id'";
		$row=$this->db->query($q)->row();
		$data_header=array(
			'id_reff' => $row->id_reff,
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'keterangan' => $row->keterangan,
			'nominal' => $row->nominal,
			// 'idakun' => $row->idakun,
			// 'posisi_akun' => $row->posisi_akun,
			'status' => 1,
			'st_auto_posting' => $row->st_auto_posting,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
            'created_date'=>date('Y-m-d H:i:s')
		);
		// print_r($data_header);exit();
		$st_auto_posting=$row->st_auto_posting;
		$this->db->insert('tvalidasi_kas',$data_header);
		$idvalidasi=$this->db->insert_id();
		$data_detail=array(
			'idvalidasi' =>$idvalidasi,
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'dari' => $row->dari,
			'ke' => $row->ke,
			'nama_dari' => $row->nama_dari,
			'nama_ke' => $row->nama_ke,
			'nominal' => $row->nominal,
			'saldoawal_dari' => $row->saldoawal_dari,
			'saldoawal_ke' => $row->saldoawal_ke,
			'saldoakhir_dari' => $row->saldoakhir_dari,
			'saldoakhir_ke' => $row->saldoakhir_ke,
			'idakun_dari' => $row->idakun_dari,
			'posisi_akun_dari' => $row->posisi_akun_dari,
			'idakun_ke' => $row->idakun_ke,
			'posisi_akun_ke' => $row->posisi_akun_ke,
			'status' =>1,
			'st_posting' =>0,
		);
		$this->db->insert('tvalidasi_kas_05_mutasi',$data_detail);
		
		
		if ($st_auto_posting=='1'){
			$data_header=array(
				
				'st_posting' => 1,
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
				

			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_kas',$data_header);
		}
		return true;
		// print_r('BERAHSIL');
	}

	public function getNoTransaksi()
    {
        $this->db->like('notransaksi', 'MTS'.date('Y').date('m'), 'after');
        $this->db->from('tmutasi_kas');
        $query = $this->db->count_all_results();

        if ($query > 0) {
            $autono = $query + 1;
            $autono = "MTS".date('Y').date('m').str_pad($autono, 4, '0', STR_PAD_LEFT);
        } else {
            $autono = "MTS".date('Y').date('m')."0001";
        }

        return $autono;
    }
    public function getSpecified($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('tmutasi_kas');
        return $query->row_array();
    }
	public function getSpecifiedHeader($id) {
        $q="SELECT D.nama as dari_nama,K.nama as ke_nama,U.`name` as user_buat,H.* FROM `tmutasi_kas` H
		LEFT JOIN msumber_kas D ON D.id=H.dari
		LEFT JOIN msumber_kas K ON K.id=H.ke
		LEFT JOIN musers U ON U.id=H.user_created
		WHERE H.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	public function list_sumber_kas(){
		$this->db->where('status', '1');
        $query = $this->db->get('msumber_kas');
        return $query->result();
	}
	public function list_pembayaran($id) {
        // $this->db->where('id', $id);
        // $query = $this->db->get('tmutasi_kas_detail');
		$q="SELECT JK.nama as jenis_kas,SK.nama as sumber_kas,D.* FROM tmutasi_kas_detail D
		LEFT JOIN mjenis_kas JK ON JK.id=D.jenis_kas_id
		LEFT JOIN msumber_kas SK ON SK.id=D.sumber_kas_id
		WHERE D.idtransaksi='$id' AND D.status='1'";
        return $this->db->query($q)->result();
    }
	
	
    public function saveData() {
		
		$this->notransaksi    = $this->getNoTransaksi();
        $this->tanggal_trx        = YMDFormat($_POST['tanggal_trx']);
        $this->dari        = $_POST['dari'];
        $this->ke        = $_POST['ke'];
        $this->idmetode        = $_POST['idmetode'];
		$saldoawal_dari=$this->db->query("SELECT H.saldo FROM `msumber_kas` H WHERE H.id='".$_POST['dari']."'")->row('saldo');
		$saldoawal_ke=$this->db->query("SELECT H.saldo FROM `msumber_kas` H WHERE H.id='".$_POST['ke']."'")->row('saldo');
        $this->saldoawal_dari        = $saldoawal_dari;
        $this->saldoawal_ke        = $saldoawal_ke;
        $this->saldoakhir_dari        = $saldoawal_dari - RemoveComma($_POST['nominal_bayar']);
        $this->saldoakhir_ke        = $saldoawal_ke + RemoveComma($_POST['nominal_bayar']);
       
        $this->nominal         = RemoveComma($_POST['nominal_bayar']);
        $this->deskripsi         = $_POST['deskripsi'];
        $this->idpegawai         = $_POST['idpegawai'];
        $this->nama_pegawai         = $_POST['nama_pegawai'];
        $this->status         = 1;
		// if ($_POST['btn_simpan']=='2'){
			// $this->st_verifikasi         = 1;
		// }
        // $this->statusjurnal   = 0;
        $this->created_at  = date("Y-m-d H:i:s");
        $this->user_created  = $this->session->userdata('user_id');

        if ($this->db->insert('tmutasi_kas', $this)) {
			$idtransaksi = $this->db->insert_id();
			if ($_POST['btn_simpan']=='2'){
				$result=$this->db->query("UPDATE tmutasi_kas set st_verifikasi='1' WHERE id='$idtransaksi'");
				 $this->Tmutasi_kas_model->insert_validasi_mutasi($idtransaksi);		
			}else{
				$result=true;
			}
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
		
    }
	
    public function updateData() {
       // $this->notransaksi    = $this->getNoTransaksi();
        $this->tanggal_trx        = YMDFormat($_POST['tanggal_trx']);
        $this->dari        = $_POST['dari'];
        $this->ke        = $_POST['ke'];
        $this->idmetode        = $_POST['idmetode'];
       $this->idpegawai         = $_POST['idpegawai'];
        $this->nama_pegawai         = $_POST['nama_pegawai'];
        $this->nominal         = RemoveComma($_POST['nominal_bayar']);
        $this->deskripsi         = $_POST['deskripsi'];
        $saldoawal_dari=$this->db->query("SELECT H.saldo FROM `msumber_kas` H WHERE H.id='".$_POST['dari']."'")->row('saldo');
		$saldoawal_ke=$this->db->query("SELECT H.saldo FROM `msumber_kas` H WHERE H.id='".$_POST['ke']."'")->row('saldo');
        $this->saldoawal_dari        = $saldoawal_dari;
        $this->saldoawal_ke        = $saldoawal_ke;
        $this->saldoakhir_dari        = $saldoawal_dari - RemoveComma($_POST['nominal_bayar']);
        $this->saldoakhir_ke        = $saldoawal_ke + RemoveComma($_POST['nominal_bayar']);
        $this->edited_date  = date("Y-m-d H:i:s");
        $this->edited_by  = $this->session->userdata('user_id');
		$this->db->where('id',$_POST['id']);
        if ($this->db->update('tmutasi_kas', $this)) {
			$idtransaksi = $_POST['id'];
			if ($_POST['btn_simpan']=='2'){
				$result=$this->db->query("UPDATE tmutasi_kas set st_verifikasi='1' WHERE id='$idtransaksi'");
				$this->Tmutasi_kas_model->insert_validasi_mutasi($idtransaksi);	
			}else{
				$result=true;
			}
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
	}
	public function getListUploadedDocument($idtransaksi)
    {
        $this->db->where('idtransaksi', $idtransaksi);
        $query = $this->db->get('tmutasi_kas_dokumen');
        return $query->result();
    }
	function refresh_image($id){
		$q="SELECT  H.* from tmutasi_kas_dokumen H

			WHERE H.idtransaksi='$id'";
		// print_r($q);exit();
		$row= $this->db->query($q)->result();
		$tabel='';
		$no=1;
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/mutasi_kas/'.$r->filename.'" target="_blank">'.$r->filename.'</a></td>';
			$tabel .='<td class="text-left">'.$r->keterangan.'</td>';
			$tabel .='<td class="text-left">'.$r->upload_by_nama.'-'.HumanDateLong($r->upload_date).'</td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/mutasi_kas/'.$r->filename.'" target="_blank" data-toggle="tooltip" title="Preview" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                    <a href="#" data-urlindex="'.base_url().'tmutasi_kas/upload_document/'.$r->idtransaksi.'" data-urlremove="'.base_url().'tmutasi_kas/delete_file/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a></td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		return $tabel;
	}
}
