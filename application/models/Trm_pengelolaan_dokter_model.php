<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_pengelolaan_dokter_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $row = $this->db->select("*")
            ->join("trm_pengelolaan_dokter", "trm_pengelolaan_dokter.id = trm_pengelolaan_dokter_detail.idtransaksi")
            ->where("trm_pengelolaan_dokter_detail.idpengajuan_detail", $id)
            ->where("trm_pengelolaan_dokter_detail.status", '1')
            ->get("trm_pengelolaan_dokter_detail")
            ->row();

        return $row;
    }

    public function getDokter($id)
    {
        $row = $this->db->select("nama")
            ->where("status", '1')
            ->get("mdokter")
            ->row();

        return $row;
    }
}
