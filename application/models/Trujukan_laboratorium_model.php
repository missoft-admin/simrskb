<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trujukan_laboratorium_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getSpecified($id)
	{
		$this->db->select('trujukan_laboratorium.nolab, trujukan_laboratorium.tanggal AS tanggalrujukan, trujukan_laboratorium.norujukan,
        trujukan_laboratorium.noantrian, trujukan_laboratorium.catatan, trujukan_laboratorium.asalrujukan, mfpasien.no_medrec AS nomedrec,
        mfpasien.nama AS namapasien, mfpasien.jenis_kelamin AS jeniskelamin, mfpasien.alamat_jalan AS alamat,
        mfpasien.tanggal_lahir AS tanggallahir, mfpasien.umur_tahun, mfpasien.umur_bulan, mfpasien.umur_hari,
        tpoliklinik_pendaftaran.id AS idpendaftaran, tpoliklinik_pendaftaran.tanggaldaftar, tpoliklinik_pendaftaran.idtipe, tpoliklinik_pendaftaran.nopendaftaran, tpoliklinik_pendaftaran.idkelompokpasien,
        mpasien_kelompok.nama AS namakelompok, mrekanan.id AS idrekanan, mrekanan.nama AS namarekanan, trujukan_laboratorium.iddokterlaboratorium,
        mdokter_laboratorium.nama AS namadokterlaboratorium, trujukan_laboratorium.iddokterperujuk, mdokter_perujuk.nama AS namadokterperujuk, COALESCE(trawatinap_pendaftaran.idkelas, 0) AS idkelas,
        mpoliklinik.nama AS namapoliklinik, mdokter_poliklinik.nama AS namadokterpoliklinik,
        mkelas.nama AS kelas, mbed.nama AS bed, mdokter_dpjp.nama AS namadokterdpjp,
        mtarif_bpjstenagakerja.id AS idtarif_bpjstk, mtarif_bpjstenagakerja.nama AS tarif_bpjstk, mtarif_bpjskesehatan.kode AS kodebpjskesehatan, mtarif_bpjskesehatan.nama AS tarif_bpjs_kesehatan,
        trujukan_laboratorium.idpengambilsample, trujukan_laboratorium.idpengujisample, trujukan_laboratorium.idpengirimhasil, trujukan_laboratorium.idpenerimahasil,
        trujukan_laboratorium.tanggal_input_pemeriksaan, trujukan_laboratorium.tanggal_pengambilan_sample, trujukan_laboratorium.tanggal_input_hasil, trujukan_laboratorium.tanggal_pengujian_sample, trujukan_laboratorium.tanggal_pengiriman_hasil, trujukan_laboratorium.tanggal_penerimaan_hasil,
        trujukan_laboratorium.cetakanke,trujukan_laboratorium.status,
        muser_pemeriksa.nama AS user_input_pemeriksaan,
        muser_sampling.nama AS user_pengambilan_sample,
        muser_hasil.nama AS user_input_hasil,
        ');
		$this->db->join('trawatinap_pendaftaran', 'trujukan_laboratorium.idtindakan = trawatinap_pendaftaran.id AND trujukan_laboratorium.asalrujukan = 3', 'LEFT');
		$this->db->join('mkelas', 'mkelas.id = trawatinap_pendaftaran.idkelas', 'LEFT');
		$this->db->join('mbed', 'mbed.id = trawatinap_pendaftaran.idbed', 'LEFT');
		$this->db->join('tpoliklinik_tindakan', 'tpoliklinik_tindakan.id = trujukan_laboratorium.idtindakan AND trujukan_laboratorium.asalrujukan IN (1, 2)', 'LEFT');
		$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran OR tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT');
		$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
		$this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
		$this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
		$this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
		$this->db->join('mdokter mdokter_poliklinik', 'mdokter_poliklinik.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
		$this->db->join('mdokter mdokter_dpjp', 'mdokter_dpjp.id = trawatinap_pendaftaran.iddokterpenanggungjawab', 'LEFT');
		$this->db->join('mdokter mdokter_perujuk', 'mdokter_perujuk.id = trujukan_laboratorium.iddokterperujuk', 'LEFT');
		$this->db->join('mdokter mdokter_laboratorium', 'mdokter_laboratorium.id = trujukan_laboratorium.iddokterlaboratorium', 'LEFT');
		$this->db->join('mtarif_bpjstenagakerja', 'mtarif_bpjstenagakerja.id = tpoliklinik_pendaftaran.idtarifbpjstenagakerja', 'LEFT');
		$this->db->join('mtarif_bpjskesehatan', 'mtarif_bpjskesehatan.id = tpoliklinik_pendaftaran.idtarifbpjskesehatan', 'LEFT');
		$this->db->join('mppa muser_pemeriksa', 'muser_pemeriksa.id = trujukan_laboratorium.iduser_input_pemeriksaan', 'LEFT');
		$this->db->join('mppa muser_sampling', 'muser_sampling.id = trujukan_laboratorium.idpengambilsample', 'LEFT');
		$this->db->join('mppa muser_hasil', 'muser_hasil.id = trujukan_laboratorium.iduser_input_hasil', 'LEFT');
		$this->db->where('trujukan_laboratorium.id', $id);
		$query = $this->db->get('trujukan_laboratorium');
		return $query->row();
	}

	public function getPegawai()
	{
		$this->db->where('status', 1);
		$query = $this->db->get('mpegawai');
		return $query->result();
	}

	public function getListDokterPerujuk()
	{
		$this->db->where('status', 1);
		$query = $this->db->get('mdokter');
		return $query->result();
	}

	public function getListDokterLaboratorium()
	{
		$this->db->where('idkategori', 3);
		$this->db->where('status', 1);
		$query = $this->db->get('mdokter');
		return $query->result();
	}

	public function getListDetail($id)
	{
		$this->db->select('mtarif_laboratorium.idkelompok, mtarif_laboratorium.idpaket, mtarif_laboratorium.path,
        mtarif_laboratorium.level, mtarif_laboratorium.nama, trujukan_laboratorium_detail.*');
		$this->db->join('mtarif_laboratorium', 'trujukan_laboratorium_detail.idlaboratorium = mtarif_laboratorium.id', 'left');
		$this->db->where('trujukan_laboratorium_detail.statusrincianpaket', 0);
		$this->db->where('trujukan_laboratorium_detail.idrujukan', $id);
		$this->db->order_by('mtarif_laboratorium.path', 'ASC');
		$this->db->from('trujukan_laboratorium_detail');
		$query = $this->db->get();
		return $query->result();
	}

	public function getListReview($id)
	{
		// $query = $this->db->query('SELECT
		// 	trujukan_laboratorium_detail.*, mtarif_laboratorium.idkelompok,
		// 	mtarif_laboratorium.idpaket, mtarif_laboratorium.level, mtarif_laboratorium.nama, mtarif_laboratorium.path,
		// 	CAST(REPLACE(mtarif_laboratorium.path, ".", "") AS SIGNED) pathile,
		// 	IF(st_paket = 0 AND mtarif_laboratorium.idpaket = 1, mtarif_laboratorium.idkelompok, trujukan_laboratorium_detail.st_paket) AS kelompok
		// FROM
		// 	trujukan_laboratorium_detail
		// 	LEFT JOIN mtarif_laboratorium ON trujukan_laboratorium_detail.idlaboratorium = mtarif_laboratorium.id
		// WHERE
		// 	trujukan_laboratorium_detail.idrujukan = '.$id.'
		// ORDER BY
		// 	kelompok,
		//   IF (kelompok = 1, trujukan_laboratorium_detail.INDEX, pathile),
		//   IF (kelompok = 1, pathile, trujukan_laboratorium_detail.INDEX)');
		$query = $this->db->query('SELECT
          trujukan_laboratorium_detail.*,
        	mtarif_laboratorium.idkelompok,
        	mtarif_laboratorium.idpaket,
        	mtarif_laboratorium.LEVEL,
        	mtarif_laboratorium.nama,
        	mtarif_laboratorium.path,
        	CAST(REPLACE(mtarif_laboratorium.path, ".", "") AS SIGNED) pathile,
        	IF(st_paket = 0 AND mtarif_laboratorium.idpaket = 1, mtarif_laboratorium.idkelompok, trujukan_laboratorium_detail.st_paket) AS kelompok
        FROM
        	trujukan_laboratorium_detail
        	LEFT JOIN mtarif_laboratorium ON trujukan_laboratorium_detail.idlaboratorium = mtarif_laboratorium.id
        WHERE
        	trujukan_laboratorium_detail.idrujukan = ' . $id . '
        ORDER BY
        	kelompok,
          trujukan_laboratorium_detail.INDEX,
        	SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(mtarif_laboratorium.path, "."), ".", 1), ".", -1) + 0,
        	SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(mtarif_laboratorium.path, "."), ".", 2), ".", -1) + 0,
        	SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(mtarif_laboratorium.path, "."), ".", 3), ".", -1) + 0,
        	SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(mtarif_laboratorium.path, "."), ".", 4), ".", -1) + 0');
		return $query->result();
	}

	public function getListTarifLaboratorium()
	{
		$this->db->where('status', 1);
		$this->db->order_by('path', 'ASC');
		$query = $this->db->get('mtarif_laboratorium');
		return $query->result();
	}

	// public function getListRincianPaketX($path)
	// {
	//   $this->db->select('trujukan_laboratorium_detail.*, mtarif_laboratorium.id AS idlaboratorium, mtarif_laboratorium.idkelompok,
	//   mtarif_laboratorium.idpaket, mtarif_laboratorium.level, mtarif_laboratorium.nama,
	//   mtarif_laboratorium.path, msatuan.nama AS namasatuan,
	//   GROUP_CONCAT(mnilainormal_kriteria.kriteria, " : ", mnilainormal_kriteria.nilai SEPARATOR "<br>") AS nilainormal');
	//   $this->db->join('trujukan_laboratorium_detail','trujukan_laboratorium_detail.idlaboratorium = mtarif_laboratorium.id', 'left');
	//   $this->db->join('mnilainormal_pelayanan','mnilainormal_pelayanan.idtariflaboratorium = mtarif_laboratorium.id', 'left');
	//   $this->db->join('mnilainormal','mnilainormal.id = mnilainormal_pelayanan.idnilainormal', 'left');
	//   $this->db->join('mnilainormal_kriteria','mnilainormal_kriteria.idnilainormal = mnilainormal.id', 'left');
	//   $this->db->join('msatuan','msatuan.id = mnilainormal.idsatuan', 'left');
	//   $this->db->where('mtarif_laboratorium.idpaket', 0);
	//   $this->db->where('mtarif_laboratorium.path LIKE', $path.'%');
	//   $this->db->group_by('mtarif_laboratorium.id');
	//   $query = $this->db->get('mtarif_laboratorium');
	//   return $query->result();
	// }

	// public function getListRincianPaket($path)
	// {
	//   $this->db->select('mtarif_laboratorium.*');
	//   $this->db->join('trujukan_laboratorium_detail','trujukan_laboratorium_detail.idlaboratorium = mtarif_laboratorium.id', 'left');
	//   $this->db->join('mnilainormal_pelayanan','mnilainormal_pelayanan.idtariflaboratorium = mtarif_laboratorium.id', 'left');
	//   $this->db->join('mnilainormal','mnilainormal.id = mnilainormal_pelayanan.idnilainormal', 'left');
	//   $this->db->join('mnilainormal_kriteria','mnilainormal_kriteria.idnilainormal = mnilainormal.id', 'left');
	//   $this->db->join('msatuan','msatuan.id = mnilainormal.idsatuan', 'left');
	//   $this->db->where('mtarif_laboratorium.idpaket', 0);
	//   $this->db->where('mtarif_laboratorium.path LIKE', $path.'%');
	//   $this->db->group_by('mtarif_laboratorium.id');
	//   $query = $this->db->get('mtarif_laboratorium');
	//   return $query->result();
	// }

	public function getListRincianPaket($path)
	{
		$this->db->select('mtarif_laboratorium.*, mtarif_laboratorium_detail.*');
		$this->db->join('mtarif_laboratorium_detail', 'mtarif_laboratorium_detail.idtarif = mtarif_laboratorium.id', 'LEFT');
		// $this->db->where('mtarif_laboratorium_detail.kelas', 1);
		$this->db->where('mtarif_laboratorium.idpaket', 0);
		$this->db->where('mtarif_laboratorium.path LIKE', $path . '.%');
		$this->db->group_by('mtarif_laboratorium.id');
		$query = $this->db->get('mtarif_laboratorium');
		// print_r($this->db->last_query());exit();
		return $query->result();
	}

	public function getListSatuanNilaiNormal($idtarif)
	{
		$this->db->select('mtarif_laboratorium.nama, COALESCE(msatuan.singkatan, " -") AS singkatansatuan,
        GROUP_CONCAT(COALESCE(mnilainormal_kriteria.kriteria, ""), " : ", mnilainormal_kriteria.nilai SEPARATOR "<br>") AS nilainormal');
		$this->db->join('mnilainormal_pelayanan', 'mnilainormal_pelayanan.idtariflaboratorium = mtarif_laboratorium.id', 'LEFT');
		$this->db->join('mnilainormal', 'mnilainormal.id = mnilainormal_pelayanan.idnilainormal', 'LEFT');
		$this->db->join('mnilainormal_kriteria', 'mnilainormal_kriteria.idnilainormal = mnilainormal.id', 'LEFT');
		$this->db->join('msatuan', 'msatuan.id = mnilainormal.idsatuan', 'LEFT');
		$this->db->where('mtarif_laboratorium.id', $idtarif);
		$this->db->where('mnilainormal.status', 1);
		$this->db->group_by('mnilainormal_pelayanan.id');
		$this->db->group_by('mnilainormal_pelayanan.idnilainormal');
		$this->db->group_by('mnilainormal_pelayanan.idtariflaboratorium');
		$query = $this->db->get('mtarif_laboratorium');
		return $query->row();
	}

	public function save()
	{
		$this->tanggal = YMDFormat($_POST['tanggalrujukan']) . ' ' . $_POST['wakturujukan'];
		$this->iddokterperujuk = $_POST['iddokterperujuk'];
		$this->iddokterlaboratorium = $_POST['iddokterlaboratorium'];
		$this->status = $_POST['status'];

		if ($_POST['stedit'] == 1) {
			$this->iduser_edit_pemeriksaan = $this->session->userdata('user_id');
			$this->tanggal_edit_pemeriksaan = date('Y-m-d H:i:s');
		} else {
			$this->iduser_input_pemeriksaan = $this->session->userdata('user_id');
			$this->tanggal_input_pemeriksaan = date('Y-m-d H:i:s');
		}

		$this->idpengambilsample = ($_POST['idpengambilsample'] != '#' ? $_POST['idpengambilsample'] : null);
		$this->idpengujisample = ($_POST['idpengujisample'] != '#' ? $_POST['idpengujisample'] : null);
		$this->tanggal_pengambilan_sample = ($_POST['tanggal_pengambilan_sample'] != '' ? YMDTimeFormat($_POST['tanggal_pengambilan_sample']) : null);
		$this->tanggal_pengujian_sample = ($_POST['tanggal_pengujian_sample'] != '' ? YMDTimeFormat($_POST['tanggal_pengujian_sample']) : null);

		if ($this->db->update('trujukan_laboratorium', $this, ['id' => $_POST['id']])) {
			$query = $this->db->query("SELECT
              mdokter.id,
              (CASE
                WHEN DATE_FORMAT(trujukan_laboratorium.tanggal_input_pemeriksaan, '%H:%i') < '12:00' THEN
                  mdokter.potonganrspagi
                ELSE mdokter.potonganrssiang
              END) AS potongan_rs,
              mdokter.pajak AS pajak_dokter
            FROM trujukan_laboratorium
            JOIN mdokter ON mdokter.id = trujukan_laboratorium.iddokterperujuk
            WHERE trujukan_laboratorium.id = " . $_POST['id']);
			$dokter = $query->row();

			$tindakan_list = json_decode($_POST['tindakan-value']);
			foreach ($tindakan_list as $row) {
				$sthapus = $row[13];
				$index = $row[14];
				if ($sthapus == 0) {
					// CASE : CHILD GET PARENT HEADER
					$query = $this->db->query("call spGetParentFromChild('" . $row[12] . "')");
					$result = $query->result();

					$query->next_result();
					$query->free_result();

					foreach ($result as $xrow) {
						$tindakan = [];
						$tindakan['idrujukan'] = $_POST['id'];
						$tindakan['idlaboratorium'] = $xrow->id;
						$tindakan['namatarif'] = $xrow->nama;
						$tindakan['jasasarana'] = 0;
						$tindakan['jasapelayanan'] = 0;
						$tindakan['bhp'] = 0;
						$tindakan['biayaperawatan'] = 0;
						$tindakan['total'] = 0;
						$tindakan['kuantitas'] = 0;
						$tindakan['diskon'] = 0;
						$tindakan['totalkeseluruhan'] = 0;
						$tindakan['statusrincianpaket'] = 2;
						$tindakan['index'] = 1;
						$tindakan['st_paket'] = 0;

						$tindakan['iddokter'] = $_POST['iddokterperujuk'];
						$tindakan['potongan_rs'] = $dokter->potongan_rs;
						$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

						$this->db->on_duplicate('trujukan_laboratorium_detail', $tindakan);
					}

					// NORMAL CASE
					$tindakan = [];
					$tindakan['idrujukan'] = $_POST['id'];
					$tindakan['idlaboratorium'] = $row[10];
					$tindakan['namatarif'] = $row[0];
					$tindakan['jasasarana'] = RemoveComma($row[1]);
					$tindakan['jasapelayanan'] = RemoveComma($row[2]);
					$tindakan['bhp'] = RemoveComma($row[3]);
					$tindakan['biayaperawatan'] = RemoveComma($row[4]);
					$tindakan['total'] = RemoveComma($row[5]);
					$tindakan['kuantitas'] = RemoveComma($row[6]);
					$tindakan['diskon'] = RemoveComma($row[8]);
					$tindakan['totalkeseluruhan'] = RemoveComma($row[9]);
					$tindakan['statusverifikasi'] = $row[11];
					$tindakan['index'] = $index;
					$tindakan['st_paket'] = 0;

					$tindakan['iddokter'] = $_POST['iddokterperujuk'];
					$tindakan['potongan_rs'] = $dokter->potongan_rs;
					$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

					if ($this->db->on_duplicate('trujukan_laboratorium_detail', $tindakan)) {
					  // GET KELOMPOK, PAKET, PATH ID LAB
						$tarif = get_by_field('id', $row[10], 'mtarif_laboratorium');
						$idkelompok = $tarif->idkelompok;
						$idpaket = $tarif->idpaket;
						$path = $tarif->path;

						// CASE : CHILD KELOMPOK & NON PAKET
						if ($idkelompok == 1 && $idpaket == 0) {
							$rincianPaket = $this->getListRincianPaket($path);
							foreach ($rincianPaket as $row) {
								$rincian = [];
								$rincian['idrujukan'] = $_POST['id'];
								$rincian['namatarif'] = $row->nama;
								$rincian['idlaboratorium'] = $row->id;
								$rincian['jasasarana'] = $row->jasasarana;
								$rincian['jasapelayanan'] = $row->jasapelayanan;
								$rincian['bhp'] = $row->bhp;
								$rincian['biayaperawatan'] = $row->biayaperawatan;
								$rincian['total'] = $row->total;
								$rincian['kuantitas'] = 1;
								$rincian['diskon'] = 0;
								$rincian['totalkeseluruhan'] = $row->total;
								$rincian['statusrincianpaket'] = 1;
								$rincian['index'] = $index;
								$rincian['st_paket'] = 1;

								$tindakan['iddokter'] = $_POST['iddokterperujuk'];
								$tindakan['potongan_rs'] = $dokter->potongan_rs;
								$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

								$this->db->on_duplicate('trujukan_laboratorium_detail', $rincian);
							}
						}

						// CASE : CHILD KELOMPOK & PAKET
						if ($idkelompok == 1 && $idpaket == 1) {
							$rincianPaket = $this->getListRincianPaket($path);
							foreach ($rincianPaket as $row) {
								$rincian = [];
								$rincian['idrujukan'] = $_POST['id'];
								$rincian['idlaboratorium'] = $row->id;
								$rincian['namatarif'] = $row->nama;
								$rincian['jasasarana'] = 0;
								$rincian['jasapelayanan'] = 0;
								$rincian['bhp'] = 0;
								$rincian['biayaperawatan'] = 0;
								$rincian['total'] = 0;
								$rincian['kuantitas'] = 0;
								$rincian['diskon'] = 0;
								$rincian['totalkeseluruhan'] = 0;
								$rincian['statusrincianpaket'] = 1;
								$rincian['index'] = $index;
								$rincian['st_paket'] = 1;

								$tindakan['iddokter'] = $_POST['iddokterperujuk'];
								$tindakan['potongan_rs'] = $dokter->potongan_rs;
								$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

								$this->db->on_duplicate('trujukan_laboratorium_detail', $rincian);
							}
						}
					}
				} else {
					$this->db->where('idrujukan', $_POST['id']);
					$this->db->where('idlaboratorium', $row[10]);
					if ($this->db->delete('trujukan_laboratorium_detail')) {
						$this->db->query("call spRemoveParentChildTLD('" . $row[12] . "','" . $_POST['id'] . "')");
					}
				}
			}
		}

		$this->db->update(
			'tpoliklinik_pendaftaran',
			['statustindakan' => '1'],
			['id' => $_POST['idpendaftaran']]
		);

		return true;
	}

	public function saveReview()
	{
		$this->db->set('tanggal', YMDFormat($_POST['tanggalrujukan']) . ' ' . $_POST['wakturujukan']);

		$this->db->set('idpengambilsample', $_POST['idpengambilsample']);
		$this->db->set('idpengujisample', $_POST['idpengujisample']);
		$this->db->set('tanggal_pengambilan_sample', ($_POST['tanggal_pengambilan_sample'] != '' ? YMDTimeFormat($_POST['tanggal_pengambilan_sample']) : null));
		$this->db->set('tanggal_pengujian_sample', ($_POST['tanggal_pengujian_sample'] != '' ? YMDTimeFormat($_POST['tanggal_pengujian_sample']) : null));

		$this->db->set('idpengirimhasil', $_POST['idpengirimhasil']);
		$this->db->set('idpenerimahasil', $_POST['idpenerimahasil']);
		$this->db->set('tanggal_pengiriman_hasil', YMDTimeFormat($_POST['tanggal_pengiriman_hasil']));
		$this->db->set('tanggal_penerimaan_hasil', YMDTimeFormat($_POST['tanggal_penerimaan_hasil']));

		$this->db->set('catatan', $_POST['catatan']);
		$this->db->set('status', 3);

		if ($_POST['stedit'] == 1) {
			$this->db->set('iduser_edit_hasil', $this->session->userdata('user_id'));
			$this->db->set('tanggal_edit_hasil', date('Y-m-d H:i:s'));
		} else {
			$this->db->set('iduser_input_hasil', $this->session->userdata('user_id'));
			$this->db->set('tanggal_input_hasil', date('Y-m-d H:i:s'));
		}

		$this->db->where('id', $_POST['id']);
		if ($this->db->update('trujukan_laboratorium')) {
			$review = json_decode($_POST['review-value']);
			foreach ($review as $row) {
				// if (isset($row[1])) {
				$data = [
					'hasil' => $row[1],
					'satuan' => ($row[2] == '0' ? '' : $row[2]),
					'nilainormal' => $row[3],
					'keterangan' => $row[4]
				];

				$this->db->update('trujukan_laboratorium_detail', $data, ['id' => $row[5]]);
				// }
			}
		}

		return true;
	}

	public function deleteTrx($id)
	{
		$this->status = 0;

		if ($this->db->update('trujukan_laboratorium', $this, ['id' => $id])) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function updateLogCetak($id)
	{
		$this->db->set('iduser_cetak', $this->session->userdata('user_id'));
		$this->db->set('tanggal_cetak', date('Y-m-d H:i:s'));
		$this->db->set('cetakanke', 'cetakanke+1', false);
		$this->db->where('id', $id);

		if ($this->db->update('trujukan_laboratorium')) {
			$dataHistory = [];
			$dataHistory['idrujukan'] = $id;
			$dataHistory['iduser'] = $this->session->userdata('user_id');
			$dataHistory['tanggal'] = date('Y-m-d H:i:s');

			if ($this->db->insert('trujukan_laboratorium_cetak', $dataHistory)) {
				return true;
			}
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function getHeadParentTarifLaboratorium($idtipe, $idkelompokpasien, $idrekanan)
	{
		$this->db->select('mtarif_laboratorium.path, mtarif_laboratorium.level, mtarif_laboratorium.nama');
		if ($idkelompokpasien == 1) {
			$this->db->select('mtarif_laboratorium.path');
			$this->db->join('mrekanan', 'mrekanan.tlaboratorium_umum = mtarif_laboratorium.id');
			$this->db->where('mrekanan.id', $idrekanan);
			$query = $this->db->get('mtarif_laboratorium');

			if ($query->num_rows() > 0) {
				if ($idtipe == 1) {
					$this->db->join('mrekanan', 'mrekanan.tlaboratorium_umum = mtarif_laboratorium.id');
				} elseif ($idtipe == 2) {
					$this->db->join('mrekanan', 'mrekanan.tlaboratorium_pa = mtarif_laboratorium.id');
				} else {
					$this->db->join('mrekanan', 'mrekanan.tlaboratorium_pmi = mtarif_laboratorium.id');
				}
				$this->db->where('mrekanan.id', $idrekanan);
			} else {
				if ($idtipe == 1) {
					$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_umum = mtarif_laboratorium.id');
				} elseif ($idtipe == 2) {
					$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_pa = mtarif_laboratorium.id');
				} else {
					$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_pmi = mtarif_laboratorium.id');
				}
				$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
			}
		} else {
			if ($idtipe == 1) {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_umum = mtarif_laboratorium.id');
			} elseif ($idtipe == 2) {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_pa = mtarif_laboratorium.id');
			} else {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_pmi = mtarif_laboratorium.id');
			}
			$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
		}

		$this->db->where('mtarif_laboratorium.idkelompok', '1');
		$this->db->where('mtarif_laboratorium.status', '1');
		$this->db->order_by('mtarif_laboratorium.path', 'ASC');
		$query = $this->db->get('mtarif_laboratorium');
		return $query->result();
	}
}

/* End of file Trujukan_laboratorium_model.php */
/* Location: ./application/models/Trujukan_laboratorium_model.php */
