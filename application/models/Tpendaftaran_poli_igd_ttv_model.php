<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpendaftaran_poli_igd_ttv_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function list_poli(){
		$q="SELECT MP.id,MP.nama FROM `mppa_poli` H
			INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
			WHERE H.setting_lainnya='1' AND H.idpoliklinik IN (SELECT idpoliklinik FROM mpoliklinik_emergency)
			GROUP BY H.idpoliklinik";
		return $this->db->query($q)->result();
	}
	function list_dokter(){
		$q="SELECT *FROM mdokter M WHERE M.`status`='1' ";
		return $this->db->query($q)->result();
	}
	function list_ruang(){
		$q="SELECT * FROM `mtujuan` M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	
}
