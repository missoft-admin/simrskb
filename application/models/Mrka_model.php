<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mrka_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getSpecified($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('mrka');
        return $query->row();
    }

    public function saveData() {
        $this->nama       = $_POST['nama'];
        $this->periode  = $_POST['periode'];
        $this->created_by  = $this->session->userdata('user_id');
        $this->created_nama  = $this->session->userdata('user_name');
        $this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mrka', $this)) {
           $id=$this->db->insert_id();
            return $id;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	
    public function updateData() {
        $this->nama       = $_POST['nama'];
		$this->periode  = $_POST['periode'];

        $this->edited_by  = $this->session->userdata('user_id');
        $this->edited_nama  = $this->session->userdata('user_name');
        $this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mrka', $this, ['id' => $_POST['id']])) {
            
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function update_status($id,$status) {
		
		if ($status=='0'){
			$this->status  = $status;
			$this->deleted_by  = $this->session->userdata('user_id');
			$this->deleted_nama  = $this->session->userdata('user_name');
			$this->deleted_date  = date('Y-m-d H:i:s');
		}
		if ($status=='2'){
			$this->status  = $status;
			$this->publish_by  = $this->session->userdata('user_id');
			$this->publish_nama  = $this->session->userdata('user_name');
			$this->publish_date  = date('Y-m-d H:i:s');
		}
		if ($status=='3'){
			$this->status  = $status;
			$this->actived_by  = $this->session->userdata('user_id');
			$this->actived_nama  = $this->session->userdata('user_name');
			$this->actived_date  = date('Y-m-d H:i:s');
		}
		if ($status=='4'){
			$this->status  = $status;
			$this->selesai_by  = $this->session->userdata('user_id');
			$this->selesai_nama  = $this->session->userdata('user_name');
			$this->selesai_date  = date('Y-m-d H:i:s');
		}
       
        if ($this->db->update('mrka', $this, ['id' => $id])) {
            
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id) {
        $this->status = 0;

        $this->deleted_by  = $this->session->userdata('user_id');
        $this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mrka', $this, ['id' => $id])) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    
}
