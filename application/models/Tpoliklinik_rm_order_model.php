<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
class Tpoliklinik_rm_order_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function cetak_rencana_rehab($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT 
			kode,logo,judul_per_ina,judul_per_eng,paragraph_1_ina,paragraph_1_eng,paragraph_2_ina,paragraph_2_eng,nama_pasien_ina,nama_pasien_eng,ttl_ina
			,ttl_eng,diagnosa_per_ina,diagnosa_per_eng,fisio_ina,fisio_eng,sebanyak_per_ina,sebanyak_per_eng,selama_per_ina
			,selama_per_eng,kontrol_per_ina,kontrol_per_eng,paragraph_3_ina,paragraph_3_eng,footer_ina,footer_eng

			FROM setting_rm_label_perencanaan H ";
		$data=$this->db->query($q)->row_array();
		$q="
			SELECT S.ref as sebanyak_nama,L.ref as lama_nama
			,JK.ref as jenis_kelamin
			,H.* 
			FROM tpoliklinik_rm_perencanaan H 
			LEFT JOIN merm_referensi S ON S.nilai=H.sebanyak AND S.ref_head_id='112'
			LEFT JOIN merm_referensi L ON L.nilai=H.selama AND L.ref_head_id='111'
			INNER JOIN mfpasien P ON P.id=H.idpasien
			LEFT JOIN merm_referensi JK ON JK.nilai=P.jenis_kelamin AND JK.ref_head_id='1'
			WHERE H.assesmen_id='$assesmen_id'
		";
		
		$data_assemen=$this->db->query($q)->row_array();
		// print_r($data);exit;
		$data=array_merge($data,$data_assemen);
        $data['title']=$data['judul_per_ina'] .' '.$data['nopermintaan'];
		$data = array_merge($data, backend_info());
		
        $html = $this->load->view('Tpoliklinik_rm_order/pdf_rencana_rehab', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        if ($st_create=='1'){
			// $nama_file=$data['judul_per_ina'].' '.$data_transaksi->nopermintaan.'.pdf';
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	}
	function list_header_path($idtipe,$idkelompokpasien,$idrekanan=0){
		$idrekanan=($idkelompokpasien==1?$idrekanan:0);
		$q="SELECT *FROM (
			SELECT  M.id as kp,0 as idrekanan,M.tfisioterapi FROM mpasien_kelompok M
			UNION ALL
			SELECT 1 as kp,M.id as idrekanan,M.tfisioterapi FROM mrekanan M WHERE (M.tfisioterapi > 0) 
			) T WHERE T.kp='$idkelompokpasien' AND (T.idrekanan='0' OR T.idrekanan='$idrekanan')

			ORDER BY T.idrekanan DESC";
		// print_r($q);exit;
		$row=$this->db->query($q)->row();
		$idlayanan='';
		$idlayanan=$row->tfisioterapi;
		
		return $idlayanan;
	}
	function get_data_konsul($pendaftaran_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT * FROM tpoliklinik_konsul H WHERE H.status_konsul='1' AND H.pendaftaran_id='$pendaftaran_id' AND H.created_ppa='$login_ppa_id'";
		return $this->db->query($q)->row_array();
	}
	function list_dokter($idpoli){
		$q="SELECT M.id,M.nama FROM `mpoliklinik_konsul_dokter` H
				INNER JOIN mdokter M ON M.id=H.iddokter
				WHERE H.idpoliklinik='$idpoli'";
		return $this->db->query($q)->result();
	}
	function list_dokter_all(){
		$q="SELECT M.id,M.nama FROM `mdokter` M WHERE M.status='1'
				";
		return $this->db->query($q)->result();
	}
	function list_poliklinik(){
		$q="SELECT M.id,M.nama FROM mpoliklinik_konsul H INNER JOIN mpoliklinik M ON M.id=H.idpoliklinik";
		return $this->db->query($q)->result();
	}
	function list_ppa_dokter(){
		$q="SELECT H.id,H.nama FROM mppa H WHERE H.tipepegawai='2' AND H.jenis_profesi_id='1'";
		return $this->db->query($q)->result();
	}
	function list_tujuan(){
		$q="SELECT M.id,M.nama FROM app_reservasi_poli H
INNER JOIN mpoliklinik M ON M.id=H.idpoli";
		return $this->db->query($q)->result();
	}
	function list_tujuan_fisio(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT MP.id,MP.nama FROM `app_reservasi_poli` H
			INNER JOIN app_reservasi_poli_user U ON U.idpoli=H.idpoli AND U.iduser='$user_id'
			INNER JOIN mpoliklinik MP ON MP.id=H.idpoli";
		return $this->db->query($q)->result();
	}
	function list_tujuan_fisio_array(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT GROUP_CONCAT(MP.id) as id FROM `app_reservasi_poli` H
			INNER JOIN app_reservasi_poli_user U ON U.idpoli=H.idpoli AND U.iduser='$user_id'
			INNER JOIN mpoliklinik MP ON MP.id=H.idpoli";
		return $this->db->query($q)->row('id');
	}
	function get_data_assesmen($pendaftaran_id,$st_ranap='0'){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		if ($st_ranap=='1'){
		$q="SELECT * FROM tpoliklinik_rm_order H WHERE H.pendaftaran_id_ranap='$pendaftaran_id' AND H.status_assemen='1' AND H.created_ppa='$login_ppa_id'";
			
		}else{
		$q="SELECT * FROM tpoliklinik_rm_order H WHERE H.pendaftaran_id='$pendaftaran_id' AND H.status_assemen='1' AND H.created_ppa='$login_ppa_id'";
			
		}
		return $this->db->query($q)->row_array();
	}
	function get_data_assesmen_per($pendaftaran_id,$st_ranap='0'){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		if ($st_ranap=='1'){
		$q="SELECT * FROM tpoliklinik_rm_perencanaan H WHERE H.pendaftaran_id_ranap='$pendaftaran_id' AND H.status_assemen='1' AND H.created_ppa='$login_ppa_id'";
			
		}else{
		$q="SELECT * FROM tpoliklinik_rm_perencanaan H WHERE H.pendaftaran_id='$pendaftaran_id' AND H.status_assemen='1' AND H.created_ppa='$login_ppa_id'";
			
		}
		return $this->db->query($q)->row_array();
	}
	function get_data_assesmen_trx($assesmen_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT * FROM tpoliklinik_rm_order H WHERE H.assesmen_id='$assesmen_id'";
		return $this->db->query($q)->row_array();
	}
	function get_data_assesmen_trx_per($assesmen_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT * FROM tpoliklinik_rm_perencanaan H WHERE H.assesmen_id='$assesmen_id'";
		return $this->db->query($q)->row_array();
	}
	function setting_label_rm(){
		$q="SELECT 
			pesan_informasi,judul_ina,judul_eng,tujuan_ina,tujuan_eng,tujuan_req,dokter_ina,dokter_eng,dokter_req,waktu_ina,waktu_eng,waktu_req,prioritas_ina,prioritas_eng,prioritas_req,catatan_ina,catatan_eng,catatan_req,ket_ina,ket_eng,ket_req,sebanyak_ina,sebanyak_eng,sebanyak_req
			,selama_ina,selama_eng,selama_req,kontrol_ina,kontrol_eng,kontrol_req,detail_ina,detail_eng,detail_req,diagnosa_ina,diagnosa_eng,diagnosa_req

		FROM setting_rm_label H ";
		return $this->db->query($q)->row_array();
	}
	function setting_label_rm_perencanaan(){
		$q="SELECT 
			kode,logo,judul_per_ina,judul_per_eng,paragraph_1_ina,paragraph_1_eng,paragraph_2_ina,paragraph_2_eng,nama_pasien_ina,nama_pasien_eng,ttl_ina,ttl_eng,diagnosa_per_ina,diagnosa_per_eng,fisio_ina,fisio_eng,sebanyak_per_ina,sebanyak_per_eng,selama_per_ina,selama_per_eng,kontrol_per_ina,kontrol_per_eng,paragraph_3_ina,paragraph_3_eng,footer_ina,footer_eng

			FROM setting_rm_label_perencanaan H ";
		return $this->db->query($q)->row_array();
	}
}
