<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tfarmasi_tindakan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function list_tujuan(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT H.* FROM `mtujuan_farmasi` H 
			INNER JOIN mtujuan_farmasi_user U ON U.tujuan_id=H.id
			WHERE H.`status`='1' AND U.userid='$user_id'";
		return $this->db->query($q)->result();
	}
	function get_data_login(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT H.id as ruangan_id,H.tanggal_login,H.st_login,H.nama_tujuan,H.iddokter,H.idpoli FROM mtujuan_farmasi H WHERE H.user_login='$user_id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT id,nama FROM `mpoliklinik` H
			
			WHERE H.`status`='1'";
		return $this->db->query($q)->result();
	}
	function list_dokter(){
		$q="SELECT *FROM mppa M
WHERE M.`staktif`='1' AND M.tipepegawai='2'
";
		return $this->db->query($q)->result();
	}
	function list_obat_perubahan($assesmen_id){
		$q="SELECT *FROM tpoliklinik_e_resep_obat H WHERE H.assesmen_id='$assesmen_id' AND H.user_perubahan > 0";
		return $this->db->query($q)->result();
	}
	//HAPUS
	public function update_cetak($assesmen_id, $st_original)
	{
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data = [
			'assesmen_id' => $assesmen_id,
			'printed_by' => $login_ppa_id,
			'printed_date' => date('Y-m-d H:i:s'),
			'st_original' => $st_original,
		];
		// print_r($data);exit();
		if ($this->db->insert('tpoliklinik_e_resep_print_his', $data)) {
			$id=$this->db->insert_id();
			$q="SELECT M.nama as nama_cetak,M.nip as nip_cetak 
			,H.printed_date,H.printed_number
			FROM `tpoliklinik_e_resep_print_his` H 
			INNER JOIN mppa M ON M.id=H.printed_by
			WHERE H.id='$id'";
			$data=$this->db->query($q)->row_array();
			return $data;
		} else {
			$data=array();
			$this->error_message = 'Penyimpanan Gagal';
			return $data;
		}
	}
	public function update_cetak_copy($assesmen_id, $st_original)
	{
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data = [
			'assesmen_id' => $assesmen_id,
			'printed_by' => $login_ppa_id,
			'printed_date' => date('Y-m-d H:i:s'),
			'st_original' => $st_original,
		];
		// print_r($data);exit();
		if ($this->db->insert('tpoliklinik_e_resep_print_copy_his', $data)) {
			$id=$this->db->insert_id();
			$q="SELECT M.nama as nama_cetak,M.nip as nip_cetak 
			,H.printed_date,H.printed_number
			FROM `tpoliklinik_e_resep_print_his` H 
			INNER JOIN mppa M ON M.id=H.printed_by
			WHERE H.id='$id'";
			$data=$this->db->query($q)->row_array();
			return $data;
		} else {
			$data=array();
			$this->error_message = 'Penyimpanan Gagal';
			return $data;
		}
	}
	public function saveData()
	{
		// print_r($this->input->post());exit();
		$nomedrec = ($this->input->post('no_medrec') == '' ? null : $this->input->post('no_medrec'));

		if ($this->input->post('idpasien')) {
			$idpasien = $this->input->post('idpasien');
			$statuspasienbaru = 0;
		} else {
			$idpasien = null;
			$statuspasienbaru = 1;
			$nomedrec = $this->Tpoliklinik_pendaftaran_model->get_nomedrec();
		}

		// Tanggal Lahir
		$tahun = $this->input->post('tahun_lahir');
		$bulan = $this->input->post('bulan_lahir');
		$hari = $this->input->post('tgl_lahir');
		$tanggallahir = date("$tahun-$bulan-$hari");

		$pasien = [];
		$pasien['id'] = $idpasien;
		$pasien['no_medrec'] = $nomedrec;
		$pasien['title'] = $this->input->post('title');
		$pasien['nama'] = $this->input->post('nama');
		$pasien['jenis_kelamin'] = $this->input->post('jenis_kelamin');
		$pasien['alamat_jalan'] = $this->input->post('alamat_jalan');
		$pasien['provinsi_id'] = $this->input->post('provinsi_id');
		$pasien['kabupaten_id'] = $this->input->post('kabupaten_id');
		$pasien['kecamatan_id'] = $this->input->post('kecamatan_id');
		$pasien['kelurahan_id'] = $this->input->post('kelurahan_id');
		$pasien['kodepos'] = $this->input->post('kodepos');
		$pasien['rw'] = $this->input->post('rw');
		$pasien['rt'] = $this->input->post('rt');
		$pasien['jenis_id'] = $this->input->post('jenis_id');
		$pasien['ktp'] = $this->input->post('noidentitas');
		$pasien['hp'] = $this->input->post('nohp');
		$pasien['telepon'] = $this->input->post('telprumah');
		$pasien['email'] = $this->input->post('email');
		$pasien['tempat_lahir'] = $this->input->post('tempat_lahir');
		$pasien['tanggal_lahir'] = $tanggallahir;
		$pasien['umur_tahun'] = $this->input->post('umur_tahun');
		$pasien['umur_bulan'] = $this->input->post('umur_bulan');
		$pasien['umur_hari'] = $this->input->post('umur_hari');
		$pasien['golongan_darah'] = $this->input->post('golongan_darah');
		$pasien['agama_id'] = $this->input->post('agama_id');
		$pasien['warganegara'] = $this->input->post('warganegara');
		$pasien['suku'] = $this->input->post('suku');
		$pasien['suku_id'] = $this->input->post('suku_id');
		$pasien['status_kawin'] = $this->input->post('statuskawin');
		$pasien['pendidikan_id'] = $this->input->post('pendidikan');
		$pasien['pekerjaan_id'] = $this->input->post('pekerjaan');
		$pasien['catatan'] = $this->input->post('catatan');
		$pasien['nama_keluarga'] = $this->input->post('namapenanggungjawab');
		$pasien['hubungan_dengan_pasien'] = $this->input->post('hubungan');
		$pasien['alamat_keluarga'] = $this->input->post('alamatpenanggungjawab');
		$pasien['telepon_keluarga'] = $this->input->post('teleponpenanggungjawab');
		$pasien['ktp_keluarga'] = $this->input->post('noidentitaspenanggung');

		if ($statuspasienbaru == 1) {
			$pasien['created_by'] = $this->session->userdata('user_id');
			$pasien['created'] = date('Y-m-d H:i:s');
		}

		if ($this->db->replace('mfpasien', $pasien)) {
			$idpasien = $this->db->insert_id();

			$pendaftaran = [];
			$pendaftaran['idtipe'] = $this->input->post('idtipe');
			$pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
			$pendaftaran['idpasien'] = $idpasien;
			$pendaftaran['idasalpasien'] = $this->input->post('idasalpasien');
			$pendaftaran['idtipepasien'] = $this->input->post('idtipepasien');

			$pendaftaran['antrian_id'] = $this->input->post('antrian_id');
			$pendaftaran['title'] = $this->input->post('title');
			$pendaftaran['no_medrec'] = $nomedrec;
			$pendaftaran['nohp'] = $this->input->post('nohp');
			$pendaftaran['telepon'] = $this->input->post('telprumah');
			$pendaftaran['namapasien'] = $this->input->post('nama');
			// $pendaftaran['alamatpasien'] = $this->input->post('alamat');
			$pendaftaran['alamatpasien'] = $this->input->post('alamat_jalan');
			$pendaftaran['provinsi_id'] = $this->input->post('provinsi_id');
			$pendaftaran['kabupaten_id'] = $this->input->post('kabupaten_id');
			$pendaftaran['kecamatan_id'] = $this->input->post('kecamatan_id');
			$pendaftaran['kelurahan_id'] = $this->input->post('kelurahan_id');
			$pendaftaran['kodepos'] = $this->input->post('kodepos');
			$pendaftaran['tempat_lahir'] = $this->input->post('tempat_lahir');
			$pendaftaran['tanggal_lahir'] = $tanggallahir;
			$pendaftaran['jenis_id'] = $this->input->post('jenis_id');
			$pendaftaran['noidentitas'] = $this->input->post('noidentitas');

			$pendaftaran['idrujukan'] = $this->input->post('idrujukan');
			$pendaftaran['idjenispasien'] = $this->input->post('jenispasien');
			$pendaftaran['idkelompokpasien'] = $this->input->post('idkelompokpasien');
			$pendaftaran['idtarifbpjskesehatan'] = $this->input->post('bpjskesehatan');
			$pendaftaran['idtarifbpjstenagakerja'] = $this->input->post('bpjstenagakerja');
			$pendaftaran['idkelompokpasien2'] = $this->input->post('kelompokpasien2');
			$pendaftaran['idrekanan2'] = $this->input->post('rekanan2');
			$pendaftaran['idtarifbpjskesehatan2'] = $this->input->post('bpjskesehatan2');
			$pendaftaran['idtarifbpjstenagakerja2'] = $this->input->post('bpjstenagakerja2');
			$pendaftaran['idpoliklinik'] = $this->input->post('idpoliklinik');
			$pendaftaran['idrekanan'] = $this->input->post('idrekanan');
			$pendaftaran['idjenispertemuan'] = $this->input->post('pertemuan_id');
			$pendaftaran['iddokter'] = $this->input->post('iddokter');
			$pendaftaran['catatan'] = $this->input->post('catatan');
			$pendaftaran['namapenanggungjawab'] = $this->input->post('namapenanggungjawab');
			$pendaftaran['teleponpenanggungjawab'] = $this->input->post('teleponpenanggungjawab');
			$pendaftaran['noidentitaspenanggungjawab'] = $this->input->post('noidentitaspenanggung');
			$pendaftaran['umurhari'] = $this->input->post('umur_hari');
			$pendaftaran['umurbulan'] = $this->input->post('umur_bulan');
			$pendaftaran['umurtahun'] = $this->input->post('umur_tahun');
			$pendaftaran['iduserinput'] = $this->session->userdata('user_id');
			$pendaftaran['statuspasienbaru'] = $statuspasienbaru;
			$pendaftaran['hubungan'] = $this->input->post('hubungan');
			$pendaftaran['created_by'] = $this->session->userdata('user_id');
			$pendaftaran['created_date'] = date('Y-m-d H:i:s');
			$pendaftaran['reservasi_id'] = 0;
			$pendaftaran['st_logic_final'] = 1;
			$pendaftaran['tanggal'] = YMDFormat($this->input->post('tanggal'));
			$pendaftaran['st_gc'] = $this->input->post('st_gc');
			$pendaftaran['st_general'] = ($this->input->post('st_gc')=='1'?0:1);
			$pendaftaran['st_sp'] = $this->input->post('st_sp');
			$pendaftaran['st_skrining'] = ($this->input->post('st_sp')=='1'?0:1);
			$pendaftaran['st_sc'] = $this->input->post('st_sc');
			$pendaftaran['st_covid'] = ($this->input->post('st_sc')=='1'?0:1);
			$pendaftaran['provinsi_id_ktp'] = $this->input->post('provinsi_id_ktp');
			$pendaftaran['kabupaten_id_ktp'] = $this->input->post('kabupaten_id_ktp');
			$pendaftaran['kecamatan_id_ktp'] = $this->input->post('kecamatan_id_ktp');
			$pendaftaran['kelurahan_id_ktp'] = $this->input->post('kelurahan_id_ktp');
			$pendaftaran['kodepos_ktp'] = $this->input->post('kodepos_ktp');
			$pendaftaran['rw_ktp'] = $this->input->post('rw_ktp');
			$pendaftaran['rt_ktp'] = $this->input->post('rt_ktp');
			$pendaftaran['alamat_jalan_ktp'] = $this->input->post('alamat_jalan_ktp');
			$pendaftaran['chk_st_domisili'] = ($this->input->post('chk_st_domisili')?1:0);
			$pendaftaran['chk_st_pengantar'] = $this->input->post('chk_st_pengantar');
			$pendaftaran['namapengantar'] = $this->input->post('namapengantar');
			$pendaftaran['hubungan_pengantar'] = $this->input->post('hubungan_pengantar');
			$pendaftaran['alamatpengantar'] = $this->input->post('alamatpengantar');
			$pendaftaran['teleponpengantar'] = $this->input->post('teleponpengantar');
			$pendaftaran['noidentitaspengantar'] = $this->input->post('noidentitaspengantar');
			$pendaftaran['st_kecelakaan'] = $this->input->post('st_kecelakaan');
			$pendaftaran['jenis_kecelakaan'] = $this->input->post('jenis_kecelakaan');
			// $pendaftaran['tgl_kecelakaan'] = YMDFormat($this->input->post('tgl_kecelakaan'));
			$pendaftaran['tgl_kecelakaan'] = ($this->input->post('tgl_kecelakaan') ? YMDFormat($this->input->post('tgl_kecelakaan')) : null);
			// $pendaftaran['tgl_kecelakaan'] = $this->input->post('tgl_kecelakaan');
			$pendaftaran['ket_kecelakaan'] = $this->input->post('ket_kecelakaan');
			$pendaftaran['kartu_id'] = $this->input->post('kartu_id');
			$pendaftaran['nokartu'] = $this->input->post('nokartu');
			$pendaftaran['nama_kartu'] = $this->input->post('nama_kartu');
			$pendaftaran['noreference_kartu'] = $this->input->post('noreference_kartu');
			$pendaftaran['chk_st_tidakdikenal'] = $this->input->post('chk_st_tidakdikenal');
			$pendaftaran['lokasi_ditemukan'] = $this->input->post('lokasi_ditemukan');
			$pendaftaran['perkiraan_umur'] = $this->input->post('perkiraan_umur');
			$pendaftaran['tgl_ditemukan'] = ($this->input->post('tgl_ditemukan')?YMDFormat($this->input->post('tgl_ditemukan')):null);
			$pendaftaran['jenis_kelamin'] = $this->input->post('jenis_kelamin');
			$pendaftaran['alamat_jalan_ktp'] = $this->input->post('alamat_jalan_ktp');
			$pendaftaran['golongan_darah'] = $this->input->post('golongan_darah');
			$pendaftaran['agama_id'] = $this->input->post('agama_id');
			$pendaftaran['warganegara'] = $this->input->post('warganegara');
			$pendaftaran['suku_id'] = $this->input->post('suku_id');
			$pendaftaran['pendidikan'] = $this->input->post('pendidikan');
			$pendaftaran['pekerjaan'] = $this->input->post('pekerjaan');
			$pendaftaran['statuskawin'] = $this->input->post('statuskawin');
			$pendaftaran['rw'] = $this->input->post('rw');
			$pendaftaran['rt'] = $this->input->post('rt');
			$pendaftaran['alamatpenanggungjawab'] = $this->input->post('alamatpenanggungjawab');
			$pendaftaran['email'] = $this->input->post('email');
			$pendaftaran['reservasi_tipe_id'] = $this->input->post('reservasi_tipe_id');
			$pendaftaran['reservasi_cara'] = 0;
			$pendaftaran['jadwal_id'] = $this->input->post('jadwal_id');
			$pendaftaran['pertemuan_id'] = $this->input->post('pertemuan_id');
			// $pendaftaran['agama_id'] = $this->input->post('agama_id');
			// $pendaftaran['noreference_kartu'] = $this->input->post('noreference_kartu');
			// print_r($pendaftaran);;exit;
			if ($this->db->insert('tpoliklinik_pendaftaran', $pendaftaran)) {
				$id=$this->db->insert_id();
				// print_r($id);exit;
				return $id;
			}
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}
	function get_edit_pendaftaran($id){
		$q="SELECT *FROM tpoliklinik_pendaftaran H WHERE H.id='$id'";
		return $this->db->query($q)->row_array();
	}
	public function get_pasien($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('mfpasien');
		$info = $query->row('inf_gabungmedrec');
		if ($info) {
			$arr = json_decode($info);
			$row = $arr->ke_pasien_id;

			$q = "SELECT mfpasien.*,(SELECT no_medrec from mfpasien where id='$id') as noid_lama from mfpasien
				where id='$row'";
		} else {
			$q = "SELECT mfpasien.*,'' as noid_lama from mfpasien
				where id='$id'";
		}
		// print_r($q);exit;
		$query = $this->db->query($q);
		return $query->result();
	}
	public function updateData($idpendaftaran)
	{
		$idpasien = ($this->input->post('idpasien') == '' ? null : $this->input->post('idpasien'));
		$nomedrec = ($this->input->post('no_medrec') == '' ? null : $this->input->post('no_medrec'));

		// Tanggal Lahir
		$tahun = $this->input->post('tahun_lahir');
		$bulan = $this->input->post('bulan_lahir');
		$hari = $this->input->post('tgl_lahir');
		$tanggallahir = date("$tahun-$bulan-$hari");

		$pasien = [];
		$pasien['title'] = $this->input->post('title');
		$pasien['nama'] = $this->input->post('nama');
		$pasien['jenis_kelamin'] = $this->input->post('jenis_kelamin');
		$pasien['alamat_jalan'] = $this->input->post('alamat_jalan');
		$pasien['provinsi_id'] = $this->input->post('provinsi_id');
		$pasien['kabupaten_id'] = $this->input->post('kabupaten_id');
		$pasien['kecamatan_id'] = $this->input->post('kecamatan_id');
		$pasien['kelurahan_id'] = $this->input->post('kelurahan_id');
		$pasien['kodepos'] = $this->input->post('kodepos');
		$pasien['rw'] = $this->input->post('rw');
		$pasien['rt'] = $this->input->post('rt');
		$pasien['jenis_id'] = $this->input->post('jenis_id');
		$pasien['ktp'] = $this->input->post('noidentitas');
		$pasien['hp'] = $this->input->post('nohp');
		$pasien['telepon'] = $this->input->post('telprumah');
		$pasien['email'] = $this->input->post('email');
		$pasien['tempat_lahir'] = $this->input->post('tempat_lahir');
		$pasien['tanggal_lahir'] = $tanggallahir;
		$pasien['umur_tahun'] = $this->input->post('umur_tahun');
		$pasien['umur_bulan'] = $this->input->post('umur_bulan');
		$pasien['umur_hari'] = $this->input->post('umur_hari');
		$pasien['golongan_darah'] = $this->input->post('golongan_darah');
		$pasien['agama_id'] = $this->input->post('agama_id');
		$pasien['warganegara'] = $this->input->post('warganegara');
		$pasien['suku'] = $this->input->post('suku');
		$pasien['suku_id'] = $this->input->post('suku_id');
		$pasien['status_kawin'] = $this->input->post('statuskawin');
		$pasien['pendidikan_id'] = $this->input->post('pendidikan');
		$pasien['pekerjaan_id'] = $this->input->post('pekerjaan');
		$pasien['catatan'] = $this->input->post('catatan');
		$pasien['nama_keluarga'] = $this->input->post('namapenanggungjawab');
		$pasien['hubungan_dengan_pasien'] = $this->input->post('hubungan');
		$pasien['alamat_keluarga'] = $this->input->post('alamatpenanggungjawab');
		$pasien['telepon_keluarga'] = $this->input->post('teleponpenanggungjawab');
		$pasien['ktp_keluarga'] = $this->input->post('noidentitaspenanggung');

		$pasien['modified'] = date('Y-m-d H:i:s');
		$pasien['modified_by'] = $this->session->userdata('user_id');

		$this->db->where('id', $idpasien);
		if ($this->db->update('mfpasien', $pasien)) {
			$pendaftaran = [];
			$pendaftaran['idtipe'] = $this->input->post('idtipe');
			// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
			$pendaftaran['idpasien'] = $idpasien;
			$pendaftaran['idasalpasien'] = $this->input->post('idasalpasien');
			$pendaftaran['idtipepasien'] = $this->input->post('idtipepasien');

			$pendaftaran['antrian_id'] = $this->input->post('antrian_id');
			$pendaftaran['title'] = $this->input->post('title');
			$pendaftaran['no_medrec'] = $nomedrec;
			$pendaftaran['nohp'] = $this->input->post('nohp');
			$pendaftaran['telepon'] = $this->input->post('telprumah');
			$pendaftaran['namapasien'] = $this->input->post('nama');
			// $pendaftaran['alamatpasien'] = $this->input->post('alamat');
			$pendaftaran['alamatpasien'] = $this->input->post('alamat_jalan');
			$pendaftaran['provinsi_id'] = $this->input->post('provinsi_id');
			$pendaftaran['kabupaten_id'] = $this->input->post('kabupaten_id');
			$pendaftaran['kecamatan_id'] = $this->input->post('kecamatan_id');
			$pendaftaran['kelurahan_id'] = $this->input->post('kelurahan_id');
			$pendaftaran['kodepos'] = $this->input->post('kodepos');
			$pendaftaran['tempat_lahir'] = $this->input->post('tempat_lahir');
			$pendaftaran['tanggal_lahir'] = $tanggallahir;
			$pendaftaran['jenis_id'] = $this->input->post('jenis_id');
			$pendaftaran['noidentitas'] = $this->input->post('noidentitas');

			$pendaftaran['idrujukan'] = $this->input->post('idrujukan');
			$pendaftaran['idjenispasien'] = $this->input->post('jenispasien');
			$pendaftaran['idkelompokpasien'] = $this->input->post('idkelompokpasien');
			$pendaftaran['idtarifbpjskesehatan'] = $this->input->post('bpjskesehatan');
			$pendaftaran['idtarifbpjstenagakerja'] = $this->input->post('bpjstenagakerja');
			$pendaftaran['idkelompokpasien2'] = $this->input->post('kelompokpasien2');
			$pendaftaran['idrekanan2'] = $this->input->post('rekanan2');
			$pendaftaran['idtarifbpjskesehatan2'] = $this->input->post('bpjskesehatan2');
			$pendaftaran['idtarifbpjstenagakerja2'] = $this->input->post('bpjstenagakerja2');
			$pendaftaran['idpoliklinik'] = $this->input->post('idpoliklinik');
			$pendaftaran['idrekanan'] = $this->input->post('idrekanan');
			$pendaftaran['idjenispertemuan'] = $this->input->post('pertemuan_id');
			$pendaftaran['iddokter'] = $this->input->post('iddokter');
			$pendaftaran['catatan'] = $this->input->post('catatan');
			$pendaftaran['namapenanggungjawab'] = $this->input->post('namapenanggungjawab');
			$pendaftaran['teleponpenanggungjawab'] = $this->input->post('teleponpenanggungjawab');
			$pendaftaran['noidentitaspenanggungjawab'] = $this->input->post('noidentitaspenanggung');
			$pendaftaran['umurhari'] = $this->input->post('umur_hari');
			$pendaftaran['umurbulan'] = $this->input->post('umur_bulan');
			$pendaftaran['umurtahun'] = $this->input->post('umur_tahun');
			$pendaftaran['iduserinput'] = $this->session->userdata('user_id');
			$pendaftaran['statuspasienbaru'] = $statuspasienbaru;
			$pendaftaran['hubungan'] = $this->input->post('hubungan');
			// $pendaftaran['created_by'] = $this->session->userdata('user_id');
			// $pendaftaran['created_date'] = date('Y-m-d H:i:s');
			// $pendaftaran['reservasi_id'] = 0;
			// $pendaftaran['st_logic_final'] = 1;
			$pendaftaran['tanggal'] = YMDFormat($this->input->post('tanggal'));
			// $pendaftaran['st_gc'] = $this->input->post('st_gc');
			// $pendaftaran['st_general'] = ($this->input->post('st_gc')=='1'?0:1);
			// $pendaftaran['st_sp'] = $this->input->post('st_sp');
			// $pendaftaran['st_skrining'] = ($this->input->post('st_sp')=='1'?0:1);
			// $pendaftaran['st_sc'] = $this->input->post('st_sc');
			// $pendaftaran['st_covid'] = ($this->input->post('st_sc')=='1'?0:1);
			$pendaftaran['provinsi_id_ktp'] = $this->input->post('provinsi_id_ktp');
			$pendaftaran['kabupaten_id_ktp'] = $this->input->post('kabupaten_id_ktp');
			$pendaftaran['kecamatan_id_ktp'] = $this->input->post('kecamatan_id_ktp');
			$pendaftaran['kelurahan_id_ktp'] = $this->input->post('kelurahan_id_ktp');
			$pendaftaran['kodepos_ktp'] = $this->input->post('kodepos_ktp');
			$pendaftaran['rw_ktp'] = $this->input->post('rw_ktp');
			$pendaftaran['rt_ktp'] = $this->input->post('rt_ktp');
			$pendaftaran['alamat_jalan_ktp'] = $this->input->post('alamat_jalan_ktp');
			$pendaftaran['chk_st_domisili'] = ($this->input->post('chk_st_domisili')?1:0);
			$pendaftaran['chk_st_pengantar'] = $this->input->post('chk_st_pengantar');
			$pendaftaran['namapengantar'] = $this->input->post('namapengantar');
			$pendaftaran['hubungan_pengantar'] = $this->input->post('hubungan_pengantar');
			$pendaftaran['alamatpengantar'] = $this->input->post('alamatpengantar');
			$pendaftaran['teleponpengantar'] = $this->input->post('teleponpengantar');
			$pendaftaran['noidentitaspengantar'] = $this->input->post('noidentitaspengantar');
			$pendaftaran['st_kecelakaan'] = $this->input->post('st_kecelakaan');
			$pendaftaran['jenis_kecelakaan'] = $this->input->post('jenis_kecelakaan');
			// $pendaftaran['tgl_kecelakaan'] = YMDFormat($this->input->post('tgl_kecelakaan'));
			$pendaftaran['tgl_kecelakaan'] = ($this->input->post('tgl_kecelakaan') ? YMDFormat($this->input->post('tgl_kecelakaan')) : null);
			// $pendaftaran['tgl_kecelakaan'] = $this->input->post('tgl_kecelakaan');
			$pendaftaran['ket_kecelakaan'] = $this->input->post('ket_kecelakaan');
			$pendaftaran['kartu_id'] = $this->input->post('kartu_id');
			$pendaftaran['nokartu'] = $this->input->post('nokartu');
			$pendaftaran['nama_kartu'] = $this->input->post('nama_kartu');
			$pendaftaran['noreference_kartu'] = $this->input->post('noreference_kartu');
			$pendaftaran['chk_st_tidakdikenal'] = $this->input->post('chk_st_tidakdikenal');
			$pendaftaran['lokasi_ditemukan'] = $this->input->post('lokasi_ditemukan');
			$pendaftaran['perkiraan_umur'] = $this->input->post('perkiraan_umur');
			$pendaftaran['tgl_ditemukan'] = ($this->input->post('tgl_ditemukan')?YMDFormat($this->input->post('tgl_ditemukan')):null);
			$pendaftaran['jenis_kelamin'] = $this->input->post('jenis_kelamin');
			$pendaftaran['alamat_jalan_ktp'] = $this->input->post('alamat_jalan_ktp');
			$pendaftaran['golongan_darah'] = $this->input->post('golongan_darah');
			$pendaftaran['agama_id'] = $this->input->post('agama_id');
			$pendaftaran['warganegara'] = $this->input->post('warganegara');
			$pendaftaran['suku_id'] = $this->input->post('suku_id');
			$pendaftaran['pendidikan'] = $this->input->post('pendidikan');
			$pendaftaran['pekerjaan'] = $this->input->post('pekerjaan');
			$pendaftaran['statuskawin'] = $this->input->post('statuskawin');
			$pendaftaran['rw'] = $this->input->post('rw');
			$pendaftaran['rt'] = $this->input->post('rt');
			$pendaftaran['alamatpenanggungjawab'] = $this->input->post('alamatpenanggungjawab');
			$pendaftaran['email'] = $this->input->post('email');
			$pendaftaran['reservasi_tipe_id'] = $this->input->post('reservasi_tipe_id');
			// $pendaftaran['reservasi_cara'] = 0;
			$pendaftaran['jadwal_id'] = $this->input->post('jadwal_id');
			$pendaftaran['pertemuan_id'] = $this->input->post('pertemuan_id');

			$pendaftaran['edited_by'] = $this->session->userdata('user_id');
			$pendaftaran['edited_date'] = date('Y-m-d H:i:s');
			// print_r($pendaftaran);exit();
			$this->db->where('id', $idpendaftaran);
			if ($this->db->update('tpoliklinik_pendaftaran', $pendaftaran)) {
				return true;
			}
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	function list_antrian($antrian_id=''){
		if ($antrian_id==''){
		$q="SELECT H.id,H.kodeantrian,H.tanggal FROM `antrian_harian` H WHERE H.tanggal >= CURRENT_DATE()";
			
		}else{
			
		$q="SELECT H.id,H.kodeantrian,H.tanggal FROM `antrian_harian` H WHERE H.id = '$id'";
		}
		return $this->db->query($q)->result();
	}
	
	function list_tanggal($idpoli,$iddokter){
		$q="SELECT H.tanggal,MC.sebab,MC.id as cuti_id,CASE WHEN MC.id IS NOT NULL THEN 'disabled' ELSE '' END st_disabel 
				,CONCAT(MR.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y') )as tanggal_nama,H.kodehari
				FROM `app_reservasi_tanggal` H
				LEFT JOIN mcuti_dokter MC ON MC.iddokter=H.iddokter AND H.tanggal BETWEEN MC.tanggal_dari AND MC.tanggal_sampai AND MC.`status`='1'
				LEFT JOIN merm_hari MR ON MR.kodehari=H.kodehari
				WHERE H.reservasi_tipe_id='1' AND H.iddokter='$iddokter' AND H.idpoli='$idpoli' AND H.tanggal >= CURRENT_DATE AND H.tanggal<DATE_ADD(CURRENT_DATE, INTERVAL 7 DAY) 
				GROUP BY H.tanggal";
		$data = $this->db->query($q)->result();
		return $data;
	}
	function list_jadwal($idpoli,$iddokter,$tanggal){
		$q="SELECT H.jadwal_id,CONCAT(DATE_FORMAT(JD.jam_dari,'%H:%i') ,' - ',DATE_FORMAT(JD.jam_sampai,'%H:%i') ) jam_nama
			,H.saldo_kuota,CASE WHEN H.saldo_kuota > 0 THEN '' ELSE 'disabled' END st_disabel
			FROM `app_reservasi_tanggal` H
			LEFT JOIN mjadwal_dokter JD ON JD.id=H.jadwal_id
			WHERE H.reservasi_tipe_id='1' AND H.iddokter='$iddokter' AND H.idpoli='$idpoli' AND H.tanggal = '$tanggal'
			GROUP BY H.tanggal,H.jadwal_id";
		$data = $this->db->query($q)->result();
		return $data;
	}
	
	function list_dokter_poli($idpoli){
		$q="SELECT MD.id,MD.nama FROM `mpoliklinik_dokter` H
			LEFT JOIN mdokter MD ON MD.id=H.iddokter
			WHERE H.idpoliklinik='$idpoli' AND H.`status`='1'";
		return $this->db->query($q)->result();
	}
	public function list_jadwal_dokter_poli($reservasi_tipe_id,$idpoli,$iddokter)
	{
		
		if ($reservasi_tipe_id=='1'){
			
		$q="SELECT H.tanggal,CASE WHEN ML.id IS NOT NULL THEN 'LIBUR NASIONAL' ELSE MC.sebab END sebab,CASE WHEN ML.id IS NOT NULL THEN ML.id ELSE MC.id END as cuti_id
				,CASE 
				WHEN MC.id IS NOT NULL THEN 'disabled' 
				WHEN ML.id IS NOT NULL THEN 'disabled' 

				ELSE '' END st_disabel 
				,CONCAT(MR.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y') )as tanggal_nama,H.kodehari
				FROM `app_reservasi_tanggal` H
				LEFT JOIN mcuti_dokter MC ON MC.iddokter=H.iddokter AND H.tanggal BETWEEN MC.tanggal_dari AND MC.tanggal_sampai AND MC.`status`='1'
				LEFT JOIN mholiday ML ON H.tanggal  BETWEEN ML.tgl_libur_awal AND ML.tgl_libur_akhir
				LEFT JOIN merm_hari MR ON MR.kodehari=H.kodehari
				WHERE H.reservasi_tipe_id='1' AND H.iddokter='$iddokter' AND H.idpoli='$idpoli' AND H.tanggal >= CURRENT_DATE AND H.tanggal<DATE_ADD(CURRENT_DATE, INTERVAL 7 DAY) 
				GROUP BY H.tanggal";
		}else{
			$q="SELECT H.tanggal
				,CONCAT(MR.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y') )as tanggal_nama,H.kodehari
				,CASE WHEN ML.id IS NOT NULL THEN 'disabled' ELSE '' END as st_disabel
				FROM `app_reservasi_tanggal` H
				LEFT JOIN mholiday ML ON H.tanggal  BETWEEN ML.tgl_libur_awal AND ML.tgl_libur_akhir
				LEFT JOIN merm_hari MR ON MR.kodehari=H.kodehari
				WHERE H.reservasi_tipe_id='2' AND H.idpoli='$idpoli' AND H.tanggal >= CURRENT_DATE AND H.tanggal<DATE_ADD(CURRENT_DATE, INTERVAL 7 DAY) 
				GROUP BY H.tanggal";
		}
		// print_r($q);exit;
		$data = $this->db->query($q)->result();
		return $data;
	}
	public function list_jam_dokter_poli($reservasi_tipe_id,$idpoli,$iddokter,$tanggal)
	{
		
		if ($reservasi_tipe_id=='1'){
			
			$q="SELECT H.jadwal_id,CONCAT(DATE_FORMAT(JD.jam_dari,'%H:%i') ,' - ',DATE_FORMAT(JD.jam_sampai,'%H:%i') ) jam_nama
			,H.saldo_kuota,CASE WHEN H.saldo_kuota > 0 THEN '' ELSE 'disabled' END st_disabel
			FROM `app_reservasi_tanggal` H
			LEFT JOIN mjadwal_dokter JD ON JD.id=H.jadwal_id
			WHERE H.reservasi_tipe_id='1' AND H.iddokter='$iddokter' AND H.idpoli='$idpoli' AND H.tanggal = '$tanggal'
			GROUP BY H.tanggal,H.jadwal_id";
		}else{
			$q="SELECT H.jam_id as jadwal_id,CONCAT(JD.jam ,'.00 - ',JD.jam_akhir,'.00' ) jam_nama
			,H.saldo_kuota,CASE WHEN COALESCE(H.saldo_kuota)  > 0 THEN '' ELSE 'disabled' END st_disabel
			FROM `app_reservasi_tanggal` H
			LEFT JOIN merm_jam JD ON JD.jam_id=H.jam_id
			WHERE H.reservasi_tipe_id='2' AND H.idpoli='$idpoli' AND H.tanggal = '$tanggal' AND H.saldo_kuota IS NOT NULL
			GROUP BY H.tanggal,H.jam_id";
		}
		// print_r($q);exit;
		$data = $this->db->query($q)->result();
		return $data;
	}
	function load_edit_poli($id){
		$q="SELECT MD.foto as foto_dokter,MD.jeniskelamin as jk_dokter,MD.nama as nama_dokter,P.nama as nama_poli,
			H.pendidikan_id as pendidikan,H.pekerjaan_id as pekerjaan
			,
			CASE 
			
			WHEN H.reservasi_tipe_id=1 THEN 
				CONCAT(merm_hari.namahari,', ',DATE_FORMAT(H.tanggal,'%d-%m-%Y'),' Pukul : ',DATE_FORMAT(MJ.jam_dari,'%H:%i'),' - ',DATE_FORMAT(MJ.jam_sampai,'%H:%i'))
			WHEN H.reservasi_tipe_id=2 THEN 
				CONCAT(merm_hari.namahari,', ',DATE_FORMAT(H.tanggal,'%d-%m-%Y'),' Pukul : ',CONCAT(MJ2.jam,'.00'),' - ',CONCAT(MJ2.jam_akhir,'.00')) END
			
			 as janji_temu
			,H.* 
			
			FROM `app_reservasi_tanggal_pasien` H
			LEFT JOIN mfpasien MP ON MP.id=H.idpasien
			LEFT JOIN merm_hari ON merm_hari.kodehari=H.kodehari
			LEFT JOIN mjadwal_dokter MJ ON MJ.id=H.jadwal_id
			LEFT JOIN mdokter MD ON MD.id=H.iddokter
			LEFT JOIN mpoliklinik P ON P.id=H.idpoli
			LEFT JOIN merm_jam MJ2 ON MJ2.jam_id=H.jam_id
			WHERE H.id='$id'";
			// print_r($q);exit;
		return $this->db->query($q)->row_array();
	}
	
	function get_gc($id){
		$q="SELECT pendaftaran_id,jawaban_ttd,jawaban_perssetujuan,logo as logo_gc,judul as judul_gc,sub_header as sub_header_gc
		,sub_header_side as sub_header_side_gc,footer_1 as footer_1_gc,footer_2 as footer_2_gc,ttd_1,ttd_2,footer_form_1,footer_form_2 FROM tpoliklinik_pendaftaran_gc_head WHERE pendaftaran_id='$id'";
		return $this->db->query($q)->row_array();
	}
	function get_sp($id){
		$q="SELECT logo as logo_sp,judul as judul_sp FROM `merm_skrining_pasien`";
		return $this->db->query($q)->row_array();
	}
	function get_sc($id){
		$q="SELECT logo as logo_sc,judul as judul_sc FROM `merm_skrining_covid`";
		return $this->db->query($q)->row_array();
	}
	function list_content_gc($id){
		$q="SELECT  * FROM tpoliklinik_pendaftaran_gc WHERE pendaftaran_id='$id'";
		return $this->db->query($q)->result();
	}
	function list_cara_bayar(){
		$q="SELECT *FROM mpasien_kelompok H WHERE H.`status`='1'";
		return $this->db->query($q)->result();
	}
	function list_asuransi(){
		$q="SELECT M.* FROM mrekanan M WHERE M.status='1'";
		return $this->db->query($q)->result();
	}
}
