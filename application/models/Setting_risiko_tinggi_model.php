<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_risiko_tinggi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_assesmen_setting(){
		$q="SELECT * FROM setting_risiko_tinggi H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_label(){
		$q="SELECT * FROM setting_risiko_tinggi_keterangan";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_user(){
		$q="SELECT * FROM setting_risiko_tinggi_user H";
		return $this->db->query($q)->row_array();
	}

	function save_assesmen(){
		$id =1;
		$this->judul_header = $this->input->post('judul_header');
		$this->judul_footer = $this->input->post('judul_footer');
		$this->judul_header_eng = $this->input->post('judul_header_eng');
		$this->footer_eng = $this->input->post('footer_eng');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->lama_edit = $this->input->post('lama_edit');
		$this->orang_edit = $this->input->post('orang_edit');
		$this->st_hapus_catatan = $this->input->post('st_hapus_catatan');
		$this->lama_hapus = $this->input->post('lama_hapus');
		$this->orang_hapus = $this->input->post('orang_hapus');
		$this->st_duplikasi_catatan = $this->input->post('st_duplikasi_catatan');
		$this->lama_duplikasi = $this->input->post('lama_duplikasi');
		$this->orang_duplikasi = $this->input->post('orang_duplikasi');
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_risiko_tinggi', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_label(){
		$id =1;
		// print_r($this->input->post());exit;
		$this->dokter_pelaksan_ina = $this->input->post('dokter_pelaksan_ina');
		$this->dokter_pelaksan_eng = $this->input->post('dokter_pelaksan_eng');
		$this->waktu_tindakan_ina = $this->input->post('waktu_tindakan_ina');
		$this->waktu_tindakan_eng = $this->input->post('waktu_tindakan_eng');
		$this->tanggal_tindakan_ina = $this->input->post('tanggal_tindakan_ina');
		$this->tanggal_tindakan_eng = $this->input->post('tanggal_tindakan_eng');
		$this->nama_pemberi_info_ina = $this->input->post('nama_pemberi_info_ina');
		$this->nama_pemberi_info_eng = $this->input->post('nama_pemberi_info_eng');
		$this->nama_petugas_pendamping_ina = $this->input->post('nama_petugas_pendamping_ina');
		$this->nama_petugas_pendamping_eng = $this->input->post('nama_petugas_pendamping_eng');
		$this->nama_penerima_info_ina = $this->input->post('nama_penerima_info_ina');
		$this->nama_penerima_info_eng = $this->input->post('nama_penerima_info_eng');
		$this->hubungan_ina = $this->input->post('hubungan_ina');
		$this->hubungan_eng = $this->input->post('hubungan_eng');
		$this->jenis_info_ina = $this->input->post('jenis_info_ina');
		$this->jenis_info_eng = $this->input->post('jenis_info_eng');
		$this->isi_info_ina = $this->input->post('isi_info_ina');
		$this->isi_info_eng = $this->input->post('isi_info_eng');
		$this->paraf_ina = $this->input->post('paraf_ina');
		$this->paraf_eng = $this->input->post('paraf_eng');
		$this->ttd_dokter_ina = $this->input->post('ttd_dokter_ina');
		$this->ttd_dokter_eng = $this->input->post('ttd_dokter_eng');
		$this->ket_ttd_dokter_ina = $this->input->post('ket_ttd_dokter_ina');
		$this->ket_ttd_dokter_eng = $this->input->post('ket_ttd_dokter_eng');
		$this->ttd_pasien_ina = $this->input->post('ttd_pasien_ina');
		$this->ttd_pasien_eng = $this->input->post('ttd_pasien_eng');
		$this->ket_ttd_pasien_ina = $this->input->post('ket_ttd_pasien_ina');
		$this->ket_ttd_pasien_eng = $this->input->post('ket_ttd_pasien_eng');
		$this->info_pernyataan_ina = $this->input->post('info_pernyataan_ina');
		$this->info_pernyataan_eng = $this->input->post('info_pernyataan_eng');
		$this->nama_pembuat_pernyataan_ina = $this->input->post('nama_pembuat_pernyataan_ina');
		$this->nama_pembuat_pernyataan_eng = $this->input->post('nama_pembuat_pernyataan_eng');
		$this->umur_pembuat_pernyataan_ina = $this->input->post('umur_pembuat_pernyataan_ina');
		$this->umur_pembuat_pernyataan_eng = $this->input->post('umur_pembuat_pernyataan_eng');
		$this->jk_pembuat_pernyataan_ina = $this->input->post('jk_pembuat_pernyataan_ina');
		$this->jk_pembuat_pernyataan_eng = $this->input->post('jk_pembuat_pernyataan_eng');
		$this->alamat_pembuat_pernyataan_ina = $this->input->post('alamat_pembuat_pernyataan_ina');
		$this->alamat_pembuat_pernyataan_eng = $this->input->post('alamat_pembuat_pernyataan_eng');
		$this->nik_pembuat_pernyataan_ina = $this->input->post('nik_pembuat_pernyataan_ina');
		$this->nik_pembuat_pernyataan_eng = $this->input->post('nik_pembuat_pernyataan_eng');
		$this->dengan_ini_memberikan_ina = $this->input->post('dengan_ini_memberikan_ina');
		$this->dengan_ini_memberikan_eng = $this->input->post('dengan_ini_memberikan_eng');
		$this->untuk_tindakan_ina = $this->input->post('untuk_tindakan_ina');
		$this->untuk_tindakan_eng = $this->input->post('untuk_tindakan_eng');
		$this->terhadap_ina = $this->input->post('terhadap_ina');
		$this->terhadap_eng = $this->input->post('terhadap_eng');
		$this->nama_pasien_ina = $this->input->post('nama_pasien_ina');
		$this->nama_pasien_eng = $this->input->post('nama_pasien_eng');
		$this->tanggal_lahir_ina = $this->input->post('tanggal_lahir_ina');
		$this->tanggal_lahir_eng = $this->input->post('tanggal_lahir_eng');
		$this->usia_ina = $this->input->post('usia_ina');
		$this->usia_eng = $this->input->post('usia_eng');
		$this->jenis_kelamin_ina = $this->input->post('jenis_kelamin_ina');
		$this->jenis_kelamin_eng = $this->input->post('jenis_kelamin_eng');
		$this->norek_ina = $this->input->post('norek_ina');
		$this->norek_eng = $this->input->post('norek_eng');
		$this->waktu_pernyataan_ina = $this->input->post('waktu_pernyataan_ina');
		$this->waktu_pernyataan_eng = $this->input->post('waktu_pernyataan_eng');
		$this->ket_pernyataan_ina = $this->input->post('ket_pernyataan_ina');
		$this->ket_pernyataan_eng = $this->input->post('ket_pernyataan_eng');
		$this->lokasi_pernyataan_ina = $this->input->post('lokasi_pernyataan_ina');
		$this->lokasi_pernyataan_eng = $this->input->post('lokasi_pernyataan_eng');
		$this->st_auto_ttd_user = $this->input->post('st_auto_ttd_user');
		$this->nama_ttd_dokter_ina = $this->input->post('nama_ttd_dokter_ina');
		$this->nama_ttd_dokter_eng = $this->input->post('nama_ttd_dokter_eng');
		$this->ket_gambar_ttd_dokter_ina = $this->input->post('ket_gambar_ttd_dokter_ina');
		$this->ket_gambar_ttd_dokter_eng = $this->input->post('ket_gambar_ttd_dokter_eng');
		$this->ket_gambar_ttd_pasien_ina = $this->input->post('ket_gambar_ttd_pasien_ina');
		$this->ket_gambar_ttd_pasien_eng = $this->input->post('ket_gambar_ttd_pasien_eng');
		$this->nama_ttd_kel_ina = $this->input->post('nama_ttd_kel_ina');
		$this->nama_ttd_kel_eng = $this->input->post('nama_ttd_kel_eng');

		$this->label_pemberi_info_ina = $this->input->post('label_pemberi_info_ina');
		$this->label_pemberi_info_eng = $this->input->post('label_pemberi_info_eng');
		$this->label_identitas_ina = $this->input->post('label_identitas_ina');
		$this->label_identitas_eng = $this->input->post('label_identitas_eng');
		$this->no_reg_ina = $this->input->post('no_reg_ina');
		$this->no_reg_eng = $this->input->post('no_reg_eng');
		$this->label_pernyataan_ina = $this->input->post('label_pernyataan_ina');
		$this->label_pernyataan_eng = $this->input->post('label_pernyataan_eng');

		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		// $this->db->where('id', $id);
			
		if ($this->db->update('setting_risiko_tinggi_keterangan', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_paraf(){
		$id =1;
		$this->ket_gambar_paraf_penerima_ina = $this->input->post('ket_gambar_paraf_penerima_ina');
		$this->ket_gambar_paraf_penerima_eng = $this->input->post('ket_gambar_paraf_penerima_eng');
		$this->ket_nama_paraf_penerima_ina = $this->input->post('ket_nama_paraf_penerima_ina');
		$this->ket_nama_paraf_penerima_eng = $this->input->post('ket_nama_paraf_penerima_eng');
		$this->ket_gambar_paraf_menyatakan_ina = $this->input->post('ket_gambar_paraf_menyatakan_ina');
		$this->ket_gambar_paraf_menyatakan_eng = $this->input->post('ket_gambar_paraf_menyatakan_eng');
		$this->ket_nama_paraf_menyatakan_ina = $this->input->post('ket_nama_paraf_menyatakan_ina');
		$this->ket_nama_paraf_menyatakan_eng = $this->input->post('ket_nama_paraf_menyatakan_eng');
		$this->ket_gambar_paraf_saksi_ina = $this->input->post('ket_gambar_paraf_saksi_ina');
		$this->ket_gambar_paraf_saksi_eng = $this->input->post('ket_gambar_paraf_saksi_eng');
		$this->ket_nama_paraf_saksi_ina = $this->input->post('ket_nama_paraf_saksi_ina');
		$this->ket_nama_paraf_saksi_eng = $this->input->post('ket_nama_paraf_saksi_eng');
		$this->ket_gambar_paraf_saksi_rs_ina = $this->input->post('ket_gambar_paraf_saksi_rs_ina');
		$this->ket_gambar_paraf_saksi_rs_eng = $this->input->post('ket_gambar_paraf_saksi_rs_eng');
		$this->ket_nama_paraf_saksi_rs_ina = $this->input->post('ket_nama_paraf_saksi_rs_ina');
		$this->ket_nama_paraf_saksi_rs_eng = $this->input->post('ket_nama_paraf_saksi_rs_eng');

		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		// $this->db->where('id', $id);
			
		if ($this->db->update('setting_risiko_tinggi_keterangan', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_defaultsave_default(){
		$id =1;
		$this->st_spesifik_risiko_tinggi = $this->input->post('st_spesifik_risiko_tinggi');
		if (!empty($this->input->post('st_kunci_default_risiko_tinggi'))){
		$this->st_kunci_default_risiko_tinggi =($this->input->post('st_spesifik_risiko_tinggi')=='0'?'0':$this->input->post('st_kunci_default_risiko_tinggi'));
			
		}else{
			$this->st_kunci_default_risiko_tinggi =0;
		}
		
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('msetting_risiko_tinggi_default', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


