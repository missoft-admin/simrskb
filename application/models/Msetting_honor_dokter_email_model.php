<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Msetting_honor_dokter_email_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSettingHeader()
    {
        $this->db->where('id', '1');
        $this->db->where('status', '1');
        $query = $this->db->get('msetting_printout_honor_dokter');
        return $query->row();
    }

    public function getSettingCatatanFooter()
    {
        $this->db->where('idsetting', '1');
        $this->db->where('status', '1');
        $query = $this->db->get('msetting_printout_honor_dokter_footer');
        return $query->result();
    }

    public function getSettingKirimEmail()
    {
        $this->db->select('msetting_printout_honor_dokter_email.*, mdokter_kategori.nama AS namakategori');
        $this->db->join('mdokter_kategori', 'mdokter_kategori.id = msetting_printout_honor_dokter_email.kategori_dokter');
        $this->db->join('mdokter', 'mdokter.id = msetting_printout_honor_dokter_email.iddokter');
        $this->db->where('msetting_printout_honor_dokter_email.idsetting', '1');
        $this->db->where('msetting_printout_honor_dokter_email.status', '1');
        $query = $this->db->get('msetting_printout_honor_dokter_email');
        return $query->result();
    }

    public function updateData()
    {
        // print_r(['post' => $this->input->post(), 'files' => $_FILES]);exit();

        $data = array();
        $data['nama_ttd'] = $this->input->post('nama_ttd');
        $data['nama_jabatan'] = $this->input->post('nama_jabatan');
        $data['img_logo'] = $this->upload_logo();
        $data['img_ttd'] = $this->upload_ttd();
        
        $this->db->where('id', '1');
        if ($this->db->update('msetting_printout_honor_dokter', $data)) {
            $this->db->where('idsetting', '1');
            if ($this->db->delete('msetting_printout_honor_dokter_footer')) {
                $setting_catatan_footer = json_decode($_POST['catatan_footer']);
                if (COUNT($setting_catatan_footer) > 0) {
                    foreach ($setting_catatan_footer as $row) {
                    $data = array();
                    $data['idsetting'] = '1';
                    $data['keterangan'] = $row[0];
                    $data['urutan'] = $row[1];
                    $data['rekap_pendapatan'] = $row[2];
                    $data['rekap_pendapatan_tunda'] = $row[4];
                    $data['detail_rajal'] = $row[6];
                    $data['detail_radiologi'] = $row[8];
    
                    $this->db->insert('msetting_printout_honor_dokter_footer', $data);
                    }
                }
            }
    
            $this->db->where('idsetting', '1');
            if ($this->db->delete('msetting_printout_honor_dokter_email')) {
                $kirim_email = json_decode($_POST['kirim_email']);
                if (COUNT($kirim_email) > 0) {
                    foreach ($kirim_email as $row) {
                    $data = array();
                    $data['idsetting'] = '1';
                    $data['kategori_dokter'] = $row[0];
                    $data['iddokter'] = $row[2];
                    $data['rekap_pendapatan'] = $row[4];
                    $data['rekap_pendapatan_tunda'] = $row[6];
                    $data['detail_rajal'] = $row[8];
                    $data['detail_radiologi'] = $row[10];
        
                    $this->db->insert('msetting_printout_honor_dokter_email', $data);
                    }
                }
            }
        }

        return true;
    }

    public function upload_logo()
    {
        if (!file_exists('assets/upload/setting_printout_honor_dokter')) {
            mkdir('assets/upload/setting_printout_honor_dokter', 0755, true);
        }

        if (isset($_FILES['img_logo'])) {
            if ($_FILES['img_logo']['name'] != '') {
                $config['upload_path'] = './assets/upload/setting_printout_honor_dokter';
                $config['allowed_types'] = 'png|jpg|jpeg|bmp';
                $config['encrypt_name']  = true;

				$this->upload->initialize($config);
                $this->load->library('upload', $config);

                if ($this->upload->do_upload('img_logo')) {
                    $image_upload = $this->upload->data();
                    return $image_upload['file_name'];
                } else {
                    $this->error_message = $this->upload->display_errors();
                    print_r($this->error_message);exit();
                    return false;
                }
            } else {
                print_r('file_name_logo_empty');exit();
                return true;
            }
        } else {
            print_r('file_logo_empty');exit();
            return true;
        }
    }

    public function upload_ttd()
    {
        if (!file_exists('assets/upload/setting_printout_honor_dokter')) {
            mkdir('assets/upload/setting_printout_honor_dokter', 0755, true);
        }

        if (isset($_FILES['img_ttd'])) {
            if ($_FILES['img_ttd']['name'] != '') {
                $config['upload_path'] = './assets/upload/setting_printout_honor_dokter';
                $config['allowed_types'] = 'png|jpg|jpeg|bmp';
                $config['encrypt_name']  = true;

				$this->upload->initialize($config);
                $this->load->library('upload', $config);

                if ($this->upload->do_upload('img_ttd')) {
                    $image_upload = $this->upload->data();
                    return $image_upload['file_name'];
                } else {
                    $this->error_message = $this->upload->display_errors();
                    print_r($this->error_message);exit();
                    return false;
                }
            } else {
                print_r('file_name_ttd_empty');exit();
                return true;
            }
        } else {
            print_r('file_ttd_empty');exit();
            return true;
        }
    }
}
