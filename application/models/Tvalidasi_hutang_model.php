<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tvalidasi_hutang_model extends CI_Model
{
   public function list_distributor(){
	   $q="SELECT id,nama from mdistributor 
				WHERE mdistributor.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getHeader($id){
	   $q="SELECT * from tvalidasi_hutang 
				WHERE tvalidasi_hutang.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function saveData(){
	   
		$id=($this->input->post('id'));
		
		$row_hutang=$this->get_nama_akun($this->input->post('idakun_hutang'));
		$row_cheq=$this->get_nama_akun($this->input->post('idakun_cheq'));
		$row_materai=$this->get_nama_akun($this->input->post('idakun_materai'));
		$data_header=array(
			'idakun_hutang'=>$this->input->post('idakun_hutang'),
			'noakun_hutang'=>$row_hutang->noakun,
			'namaakun_hutang'=>$row_hutang->namaakun,
			
			'idakun_cheq'=>$this->input->post('idakun_cheq'),
			'noakun_cheq'=>$row_cheq->noakun,
			'namaakun_cheq'=>$row_cheq->namaakun,
			
			'idakun_materai'=>$this->input->post('idakun_materai'),
			'noakun_materai'=>$row_materai->noakun,
			'namaakun_materai'=>$row_materai->namaakun,
		);
		$this->db->where('id',$id);
		$this->db->update('tvalidasi_hutang',$data_header);
		
		$st_posting=($this->input->post('st_posting'));
		$btn_simpan=($this->input->post('btn_simpan'));
		$iddet=($this->input->post('iddet'));
		$idakun_tf=($this->input->post('idakun_tf'));
		
		foreach($iddet as $index => $val){
			$row_tf=$this->get_nama_akun($idakun_tf[$index]);
			
			$detail=array(
				'idakun'=>$idakun_tf[$index],
				'noakun'=>$row_tf->noakun,
				'namaakun'=>$row_tf->namaakun,
				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_hutang_bayar',$detail);
		}
		if ($st_posting=='1'){
			$this->db->where('id',$id);
			$this->db->update('tvalidasi_hutang',array('st_posting'=>0));
		}
		if ($btn_simpan=='2'){
			$data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $id);
			$result=$this->db->update('tvalidasi_hutang', $data);
		}
		
		return true;
   }
   function get_nama_akun($id){
	   $q="SELECT *from makun_nomor A WHERE A.id='$id'";
	   return $this->db->query($q)->row();
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
