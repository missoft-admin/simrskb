<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_eresep_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_eresep_setting_label(){
		$q="SELECT * FROM setting_eresep_label H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	public function get_eresep_setting(){
		$q="SELECT * FROM setting_eresep H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function save_eresep(){
		$id =1;
		// $this->judul_header = $this->input->post('judul_header');
		// $this->judul_footer = $this->input->post('judul_footer');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->lama_edit = $this->input->post('lama_edit');
		$this->orang_edit = $this->input->post('orang_edit');
		$this->st_hapus_catatan = $this->input->post('st_hapus_catatan');
		$this->lama_hapus = $this->input->post('lama_hapus');
		$this->orang_hapus = $this->input->post('orang_hapus');
		$this->st_duplikasi_catatan = $this->input->post('st_duplikasi_catatan');
		$this->lama_duplikasi = $this->input->post('lama_duplikasi');
		$this->orang_duplikasi = $this->input->post('orang_duplikasi');
		$this->st_iter_default = $this->input->post('st_iter_default');
		$this->max_iter = $this->input->post('max_iter');
		$this->jml_iter = $this->input->post('jml_iter');
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_eresep', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_eresep_label(){
		// print_r($this->input->post());exit;
		$id =1;
		$this->pesan_informasi = $this->input->post('pesan_informasi');
		$this->judul_ina = $this->input->post('judul_ina');
		$this->judul_eng = $this->input->post('judul_eng');
		$this->waktu_ina = $this->input->post('waktu_ina');
		$this->waktu_eng = $this->input->post('waktu_eng');
		$this->waktu_req = ($this->input->post('waktu_req')=='on'?1:0);
		$this->tujuan_ina = $this->input->post('tujuan_ina');
		$this->tujuan_eng = $this->input->post('tujuan_eng');
		$this->tujuan_req = ($this->input->post('tujuan_req')=='on'?1:0);
		$this->dokter_ina = $this->input->post('dokter_ina');
		$this->dokter_eng = $this->input->post('dokter_eng');
		$this->dokter_req = ($this->input->post('dokter_req')=='on'?1:0);
		$this->bb_ina = $this->input->post('bb_ina');
		$this->bb_eng = $this->input->post('bb_eng');
		$this->bb_req = ($this->input->post('bb_req')=='on'?1:0);
		$this->tb_ina = $this->input->post('tb_ina');
		$this->tb_eng = $this->input->post('tb_eng');
		$this->tb_req = ($this->input->post('tb_req')=='on'?1:0);
		$this->luas_ina = $this->input->post('luas_ina');
		$this->luas_eng = $this->input->post('luas_eng');
		$this->luas_req = ($this->input->post('luas_req')=='on'?1:0);
		$this->diagnosa_ina = $this->input->post('diagnosa_ina');
		$this->diagnosa_eng = $this->input->post('diagnosa_eng');
		$this->diagnosa_req = ($this->input->post('diagnosa_req')=='on'?1:0);
		$this->menyusui_ina = $this->input->post('menyusui_ina');
		$this->menyusui_eng = $this->input->post('menyusui_eng');
		$this->menyusui_req = ($this->input->post('menyusui_req')=='on'?1:0);
		$this->gangguan_ina = $this->input->post('gangguan_ina');
		$this->gangguan_eng = $this->input->post('gangguan_eng');
		$this->gangguan_req = ($this->input->post('gangguan_req')=='on'?1:0);
		$this->puasa_ina = $this->input->post('puasa_ina');
		$this->puasa_eng = $this->input->post('puasa_eng');
		$this->puasa_req = ($this->input->post('puasa_req')=='on'?1:0);
		$this->alergi_ina = $this->input->post('alergi_ina');
		$this->alergi_eng = $this->input->post('alergi_eng');
		$this->alergi_req = ($this->input->post('alergi_req')=='on'?1:0);
		$this->alergi_detail_ina = $this->input->post('alergi_detail_ina');
		$this->alergi_detail_eng = $this->input->post('alergi_detail_eng');
		$this->alergi_detail_req = ($this->input->post('alergi_detail_req')=='on'?1:0);
		$this->prioritas_ina = $this->input->post('prioritas_ina');
		$this->prioritas_eng = $this->input->post('prioritas_eng');
		$this->prioritas_req = ($this->input->post('prioritas_req')=='on'?1:0);
		$this->pulang_ina = $this->input->post('pulang_ina');
		$this->pulang_eng = $this->input->post('pulang_eng');
		$this->pulang_req = ($this->input->post('pulang_req')=='on'?1:0);
		$this->catatan_ina = $this->input->post('catatan_ina');
		$this->catatan_eng = $this->input->post('catatan_eng');
		$this->catatan_req = ($this->input->post('catatan_req')=='on'?1:0);
		$this->ket_ina = $this->input->post('ket_ina');
		$this->ket_eng = $this->input->post('ket_eng');
		$this->ket_req = ($this->input->post('ket_req')=='on'?1:0);

		$this->db->where('id', $id);
			
		if ($this->db->update('setting_eresep_label', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


