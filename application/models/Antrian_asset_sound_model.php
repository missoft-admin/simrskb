<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Antrian_asset_sound_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function list_layanan($counter_id=''){
		$q="SELECT H.id,CONCAT(H.kode,' - ',H.nama_pelayanan) as pelayanan 
			,CASE WHEN M.pelayanan_id IS NOT NULL THEN 'selected' ELSE '' END as pilih
			FROM antrian_pelayanan H
			LEFT JOIN antrian_asset_sound M ON M.pelayanan_id=H.id AND M.counter_id='$counter_id'
			WHERE H.`status`='1' ORDER BY H.urutan";
		return $this->db->query($q)->result();
	}
	public function list_user($counter_id=''){
		$q="SELECT H.id,H.`name` as nama_user
			,CASE WHEN M.userid IS NOT NULL THEN 'selected' ELSE '' END as pilih
			FROM musers H
			LEFT JOIN antrian_asset_sound_user M ON M.userid=H.id AND M.counter_id='$counter_id'
			WHERE H.`status`='1' ORDER BY H.id";
		return $this->db->query($q)->result();
	}
	
	public function getSpecified($id)
    {
        $q="SELECT 
			*
			from antrian_asset_sound M
					
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	
    public function saveData()
    {
		// print_r($this->input->post());exit;
        $this->nama_asset 	= $_POST['nama_asset'];		
        $this->upload_sound(false);
		// $this->created_by  = $this->session->userdata('user_id');
		// $this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('antrian_asset_sound', $this)) {
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
		$counter_id=$_POST['id'];
        $this->nama_asset 	= $_POST['nama_asset'];		
		$this->upload_sound(true);
		// $this->edited_by  = $this->session->userdata('user_id');
		// $this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('antrian_asset_sound', $this, array('id' => $_POST['id']))) {
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function upload_sound($update = false)
    {
        if (!file_exists('assets/upload/sound_antrian')) {
            mkdir('assets/upload/sound_antrian', 0755, true);
        }

        if (isset($_FILES['file_sound'])) {
            if ($_FILES['file_sound']['name'] != '') {
                $config['upload_path'] = './assets/upload/sound_antrian/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'wav|mp3';
				// $config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('file_sound')) {
                    $image_upload = $this->upload->data();
                    $this->file_sound = $image_upload['file_name'];

                    if ($update == true) {
                        $this->remove_sound($this->input->post('id'));
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
			// print_r('TIDAK ADA');exit;
                return true;
            }
					// print_r($this->foto);exit;
        } else {
			// print_r('TIDAK ADA');exit;
            return true;
        }
		
    }
	public function remove_sound($id)
    {
		$q="select file_sound From antrian_asset_sound H WHERE H.id='$id'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/sound_antrian/'.$row->file_sound) && $row->file_sound !='') {
            unlink('./assets/upload/sound_antrian/'.$row->file_sound);
        }
    }
    public function softDelete($id)
    {
        $this->status = 0;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('antrian_asset_sound', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('antrian_asset_sound', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
