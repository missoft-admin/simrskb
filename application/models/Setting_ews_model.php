<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_ews_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	
	public function get_ews_setting(){
		$q="SELECT * FROM setting_ews H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function save_ews(){
		$id =1;
		// $this->judul_header = $this->input->post('judul_header');
		// $this->judul_footer = $this->input->post('judul_footer');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->lama_edit = $this->input->post('lama_edit');
		$this->orang_edit = $this->input->post('orang_edit');
		$this->st_hapus_catatan = $this->input->post('st_hapus_catatan');
		$this->lama_hapus = $this->input->post('lama_hapus');
		$this->orang_hapus = $this->input->post('orang_hapus');
		$this->st_duplikasi_catatan = $this->input->post('st_duplikasi_catatan');
		$this->lama_duplikasi = $this->input->post('lama_duplikasi');
		$this->orang_duplikasi = $this->input->post('orang_duplikasi');
		$this->judul_header_ina = $this->input->post('judul_header_ina');
		$this->judul_footer_ina = $this->input->post('judul_footer_ina');
		$this->judul_header_eng = $this->input->post('judul_header_eng');
		$this->judul_footer_eng = $this->input->post('judul_footer_eng');
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_ews', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


