<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tbagi_hasil_bayar_model extends CI_Model
{
  
   public function get_header($id){
	   $q="SELECT H.id,H.nama_bagi_hasil,H.tanggal_trx,H.notransaksi,H.mbagi_hasil_id,H.list_tbagi_hasil_id as list_trx
			,H.pendapatan_bersih_rs,H.pendapatan_bersih_ps,H.`status`
			FROM `tbagi_hasil_bayar_head` H
			WHERE H.id='$id'
			";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function get_header_bayar($id){
	   $q="SELECT H.notransaksi,H.nama_bagi_hasil,D.id,D.nama_pemilik,D.tipe_pemilik_nama,D.total_pendapatan,D.tanggal_bayar,D.idmetode 
		,D.tbagi_hasil_bayar_id
	   FROM `tbagi_hasil_bayar_detail` D
			LEFT JOIN tbagi_hasil_bayar_head H ON H.id=D.tbagi_hasil_bayar_id
			WHERE D.id='$id'
			";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function list_pembayaran($id) {
        // $this->db->where('id', $id);
        // $query = $this->db->get('tsetoran_kas_detail');
		$q="SELECT JK.nama as jenis_kas,SK.nama as sumber_kas,D.* 
		FROM tbagi_hasil_pembayaran D
		LEFT JOIN mjenis_kas JK ON JK.id=D.jenis_kas_id
		LEFT JOIN msumber_kas SK ON SK.id=D.sumber_kas_id
		WHERE D.iddet='$id' AND D.status='1'
		";
        return $this->db->query($q)->result();
    }
   public function list_mbagi_hasil(){
	   $q="SELECT id,nama from mbagi_hasil 
				WHERE mbagi_hasil.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_bagi_hasil(){
	   $q="SELECT id,nama from mbagi_hasil 
				WHERE mbagi_hasil.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function saveData(){
	   $list_trx=$this->input->post('list_trx');
	   $q="SELECT H.mbagi_hasil_id,H.nama_bagi_hasil,SUM(H.pendapatan_bersih_rs) as pendapatan_bersih_rs,SUM(pendapatan_bersih_ps) as pendapatan_bersih_ps,SUM(jumlah_lembar) as jumlah_lembar FROM tbagi_hasil H
			WHERE H.id IN (".$list_trx.")";
		$head=$this->db->query($q)->row_array();
		$head['list_tbagi_hasil_id']=$list_trx;
		$head['tanggal_trx']=date('Y-m-d H:i:s');
		$head['created_date']=date('Y-m-d H:i:s');
		$head['created_by']=$this->session->userdata('user_id');
		
		$this->db->insert('tbagi_hasil_bayar_head',$head);
		
		$tbagi_hasil_bayar_id= $this->db->insert_id();
		$id_pemilik_saham=$this->input->post('id_pemilik_saham');
		$nama_pemilik=$this->input->post('nama_pemilik');
		$tipe_pemilik=$this->input->post('tipe_pemilik');
		$total_pendapatan=($this->input->post('total_pendapatan'));
		$idmetode=($this->input->post('idmetode'));
		$pemilik_rek_id=($this->input->post('pemilik_rek_id'));
		$tanggal_bayar=($this->input->post('tanggal_bayar'));
		$jumlah_lembar=($this->input->post('jumlah_lembar'));
		foreach($id_pemilik_saham as $index => $val){
			$rek=getRekeningPS_ID($pemilik_rek_id[$index]);
			// if ($rek){}else{$rek[]=(object) array('idbank'=>null,'bank'=>null,'norek'=>null,'atas_nama'=>null);}
			$detail=array(
				'tbagi_hasil_bayar_id'=>$tbagi_hasil_bayar_id,
				'id_pemilik_saham'=>$id_pemilik_saham[$index],
				'nama_pemilik'=>$nama_pemilik[$index],
				'tipe_pemilik'=>$tipe_pemilik[$index],
				'tipe_pemilik_nama'=>GetTipePemilikSaham($tipe_pemilik[$index]),
				'total_pendapatan'=>RemoveComma($total_pendapatan[$index]),
				'idmetode'=>$idmetode[$index],
				'jumlah_lembar'=>$jumlah_lembar[$index],
				'pemilik_rek_id'=>$pemilik_rek_id[$index],
				'idbank'=>($rek)?$rek->idbank:null,
				'bank'=>($rek)?$rek->bank:null,
				'norek'=>($rek)?$rek->norek:null,
				'atas_nama'=>($rek)?$rek->atas_nama:null,
				'tanggal_bayar'=>YMDFormat($tanggal_bayar[$index]),
			);
			$result=$this->db->insert('tbagi_hasil_bayar_detail',$detail);
		}
		$arr=explode(',',$list_trx);
		foreach($arr as $index => $val){
			$data=array(
				'tbagi_hasil_bayar_id'=>$tbagi_hasil_bayar_id,
				'tbagi_hasil_id'=>$val,
			);
			$result=$this->db->insert('tbagi_hasil_bayar_head_trx',$data);
		}
		
		return $result;
   }
   public function updateData(){
	   $id=$this->input->post('id');
	   $list_trx=$this->input->post('list_trx');
	   $q="SELECT H.mbagi_hasil_id,H.nama_bagi_hasil,SUM(H.pendapatan_bersih_rs) as pendapatan_bersih_rs,SUM(pendapatan_bersih_ps) as pendapatan_bersih_ps,SUM(jumlah_lembar) as jumlah_lembar FROM tbagi_hasil H
			WHERE H.id IN (".$list_trx.")";
		$head=$this->db->query($q)->row_array();
		$head['list_tbagi_hasil_id']=$list_trx;
		// $head['tanggal_trx']=date('Y-m-d H:i:s');
		$head['edited_date']=date('Y-m-d H:i:s');
		$head['edited_by']=$this->session->userdata('user_id');
		
		$this->db->where('id',$id);
		$this->db->update('tbagi_hasil_bayar_head',$head);
		
		$tbagi_hasil_bayar_id= $id;
		
		// $this->db->where('tbagi_hasil_bayar_id',$tbagi_hasil_bayar_id);
		// $this->db->delete('tbagi_hasil_bayar_detail');
		$this->db->where('tbagi_hasil_bayar_id',$tbagi_hasil_bayar_id);
		$this->db->delete('tbagi_hasil_bayar_head_trx');
		
		$id_pemilik_saham=$this->input->post('id_pemilik_saham');
		$nama_pemilik=$this->input->post('nama_pemilik');
		$tipe_pemilik=$this->input->post('tipe_pemilik');
		$total_pendapatan=($this->input->post('total_pendapatan'));
		$idmetode=($this->input->post('idmetode'));
		$pemilik_rek_id=($this->input->post('pemilik_rek_id'));
		$tanggal_bayar=($this->input->post('tanggal_bayar'));
		$jumlah_lembar=($this->input->post('jumlah_lembar'));
		foreach($id_pemilik_saham as $index => $val){
			$rek=getRekeningPS_ID($pemilik_rek_id[$index]);
			// if ($rek){}else{$rek[]=(object) array('idbank'=>null,'bank'=>null,'norek'=>null,'atas_nama'=>null);}
			$detail=array(
				'tbagi_hasil_bayar_id'=>$tbagi_hasil_bayar_id,
				'id_pemilik_saham'=>$id_pemilik_saham[$index],
				'nama_pemilik'=>$nama_pemilik[$index],
				'tipe_pemilik'=>$tipe_pemilik[$index],
				'tipe_pemilik_nama'=>GetTipePemilikSaham($tipe_pemilik[$index]),
				'total_pendapatan'=>RemoveComma($total_pendapatan[$index]),
				'idmetode'=>$idmetode[$index],
				'jumlah_lembar'=>$jumlah_lembar[$index],
				'pemilik_rek_id'=>$pemilik_rek_id[$index],
				'idbank'=>($rek)?$rek->idbank:null,
				'bank'=>($rek)?$rek->bank:null,
				'norek'=>($rek)?$rek->norek:null,
				'atas_nama'=>($rek)?$rek->atas_nama:null,
				'tanggal_bayar'=>YMDFormat($tanggal_bayar[$index]),
			);
			$result=$this->db->insert('tbagi_hasil_bayar_detail',$detail);
		}
			// print_r($detail);exit();
		
		$arr=explode(',',$list_trx);
		foreach($arr as $index => $val){
			$data=array(
				'tbagi_hasil_bayar_id'=>$tbagi_hasil_bayar_id,
				'tbagi_hasil_id'=>$val,
			);
			$result=$this->db->insert('tbagi_hasil_bayar_head_trx',$data);
		}
		
		return $result;
   }
   public function detail($id){
	   $q="SELECT * from tbagi_hasil 
				WHERE tbagi_hasil.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function simpan_proses_peretujuan($id) {
	   $this->db->where('tbagi_hasil_bayar_id',$id);
	   $this->db->delete('tbagi_hasil_bayar_approval');
		$q="SELECT S.id,S.step,S.proses_setuju,S.proses_tolak,U.`name` as user_nama,S.iduser FROM tbagi_hasil_bayar_head H
			LEFT JOIN mbagi_hasil_logic S ON S.mbagi_hasil_id=H.mbagi_hasil_id AND S.`status`='1'
			LEFT JOIN musers U ON U.id=S.iduser
			WHERE H.id='$id'  AND calculate_logic(S.operand,H.nominal_dibagikan,S.nominal) = 1
			ORDER BY S.step ASC,S.id ASC";
		$list_user=$this->db->query($q)->result();
		$step=999;
		$idlogic='';
		foreach ($list_user as $row){
			if ($row->step < $step){
				$step=$row->step;
			}
			$data=array(
				'tbagi_hasil_bayar_id' => $id,
				'step' => $row->step,
				// 'idlogic' => $row->idlogic,
				'iduser' => $row->iduser,
				'user_nama' => $row->user_nama,
				'proses_setuju' => $row->proses_setuju,
				'proses_tolak' => $row->proses_tolak,
				'approve' => 0,
			);
			// $idlogic=$row->idlogic;
			$this->db->insert('tbagi_hasil_bayar_approval', $data);
		}
		
		$this->db->update('tbagi_hasil_bayar_head',array('status'=>2),array('id'=>$id));
		return $this->db->update('tbagi_hasil_bayar_approval',array('st_aktif'=>1),array('tbagi_hasil_bayar_id'=>$id,'step'=>$step));
		
		// $data=array(
			// 'idlogic' => $idlogic,
			// 'status' => '2',
			// 'proses_by' => $this->session->userdata('user_id'),
			// 'proses_nama' => $this->session->userdata('user_name'),
			// 'proses_date' => date('Y-m-d H:i:s'),
		// );
		// $this->db->where('id',$id);
		return $this->db->update('rka_pengajuan', $data);
		
		
    }
	public function save_pembayaran(){

		$xjenis_kas_id= $_POST['xjenis_kas_id'];
		$xsumber_kas_id= $_POST['xsumber_kas_id'];
		$xidmetode= $_POST['xidmetode'];
		$ket= $_POST['ket'];
		$xnominal_bayar= $_POST['xnominal_bayar'];
		$xiddet= $_POST['xiddet'];
		$xstatus= $_POST['xstatus'];
		$xtanggal_pencairan= $_POST['xtanggal_pencairan'];
		$idtransaksi = $this->input->post('id');
		foreach ($xjenis_kas_id as $index => $val){
			if ($xiddet[$index]!=''){
				$data_detail=array(
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'idmetode'=>$xidmetode[$index],
					'ket_pembayaran'=>$ket[$index],
					'tanggal_pencairan'=>YMDFormat($xtanggal_pencairan[$index]),
					'status'=>$xstatus[$index],
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);

				$this->db->where('id',$xiddet[$index]);
				$this->db->update('tbagi_hasil_pembayaran', $data_detail);
			}else{
				
				$data_detail=array(
					'iddet'=>$idtransaksi,
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'idmetode'=>$xidmetode[$index],
					'ket_pembayaran'=>$ket[$index],
					'tanggal_pencairan'=>YMDFormat($xtanggal_pencairan[$index]),
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->insert('tbagi_hasil_pembayaran', $data_detail);
				// print_r($data_detail);exit();
			}
			$this->db->where(id,$idtransaksi);
			$this->db->update('tbagi_hasil_bayar_detail',array('st_trx'=>1));
		}
		return true;
	}
	public function insert_validasi_bagi_hasil($id){
		$q="SELECT '3' as id_reff,H.mbagi_hasil_id,H.nama_bagi_hasil
			,H.id as idtransaksi,H.tanggal_trx as tanggal_transaksi,H.notransaksi as notransaksi
			,CONCAT('BAGI HASIL ',H.nama_bagi_hasil,' (',H.notransaksi,')') as keterangan
			,H.nominal_dibagikan as nominal,null as idakun,null as posisi_akun,S.st_auto_posting,1 as status
			,M.bagian_rs,M.bagian_ps,M.tanggal_mulai,M.keterangan as ket,CASE WHEN M.status_langsung='1' THEN 'Dibayar Langsung' ELSE 'Menunggu Pembayaran Asuransi' END as status_langsung
			,H.list_tbagi_hasil_id
			FROM tbagi_hasil_bayar_head H
			LEFT JOIN mbagi_hasil M ON M.id=H.mbagi_hasil_id
			LEFT JOIN msetting_jurnal_bagi_hasil S ON S.id='1'
			WHERE H.id='$id'";
		$row=$this->db->query($q)->row();
		$data_header=array(
			'id_reff' => $row->id_reff,
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'keterangan' => $row->keterangan,
			'nominal' => $row->nominal,
			'idakun' => $row->idakun,
			'posisi_akun' => $row->posisi_akun,
			'status' => 1,
			'st_auto_posting' => $row->st_auto_posting,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
            'created_date'=>date('Y-m-d H:i:s')
		);
		// print_r($data_header);exit();
		$st_auto_posting=$row->st_auto_posting;
		$this->db->insert('tvalidasi_kas',$data_header);
		$idvalidasi=$this->db->insert_id();
		$data_kasbon=array(
			'idvalidasi' =>$idvalidasi,
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'mbagi_hasil_id' => $row->mbagi_hasil_id,
			'nama_bagi_hasil' => $row->nama_bagi_hasil,
			'list_tbagi_hasil_id' => $row->list_tbagi_hasil_id,
			'bagian_rs' => $row->bagian_rs,
			'bagian_ps' => $row->bagian_ps,
			'tanggal_mulai' => $row->tanggal_mulai,
			'status_langsung' => $row->status_langsung,
			'keterangan' => $row->ket,
			'nominal' => $row->nominal,
			'idakun' => $row->idakun,
			'posisi_akun' => $row->posisi_akun,
			'st_posting' =>0,
		);
		$this->db->insert('tvalidasi_kas_03_bagi_hasil',$data_kasbon);
		
		$q="SELECT T.*,S.idakun,'D' as posisi_akun FROM (
			SELECT H.id as iddet,H.id_pemilik_saham,H.nama_pemilik,H.tipe_pemilik,H.jumlah_lembar as jumlah_lembar,H.total_pendapatan as nominal,H.idmetode,H.pemilik_rek_id,H.tanggal_bayar 
			,RM.metode_bayar as metode,REK.atas_nama,RB.bank,REK.idbank,BH.mbagi_hasil_id,M.nama as nama_bagi_hasil
			from tbagi_hasil_bayar_detail H
			LEFT JOIN tbagi_hasil_bayar_head BH ON BH.id=H.tbagi_hasil_bayar_id
			LEFT JOIN mbagi_hasil M ON M.id=BH.mbagi_hasil_id
			LEFT JOIN ref_metode RM ON RM.id=H.idmetode
			LEFT JOIN mpemilik_saham_rek REK ON REK.id=H.pemilik_rek_id
			LEFT JOIN ref_bank RB ON RB.id=REK.idbank
			WHERE H.tbagi_hasil_bayar_id='$id'
			) T 
			LEFT JOIN msetting_jurnal_bagi_hasil_detail S ON S.mbagi_hasil_id=T.mbagi_hasil_id";
		$rows=$this->db->query($q)->result();
		foreach($rows as $row){
			$data_bayar=array(
				'idvalidasi' =>$idvalidasi,
				'iddet' => $row->iddet,
				'id_pemilik_saham' => $row->id_pemilik_saham,
				'nama_pemilik' => $row->nama_pemilik,
				'tipe_pemilik' => $row->tipe_pemilik,
				'nama_tipe' => GetTipePemilikSaham($row->tipe_pemilik),
				'jumlah_lembar' => $row->jumlah_lembar,
				'nominal' => $row->nominal,
				'idmetode' => $row->idmetode,
				'metode' => $row->metode,
				'pemilik_rek_id' => $row->pemilik_rek_id,
				'tanggal_bayar' => $row->tanggal_bayar,
				'atas_nama' => $row->atas_nama,
				'idbank' => $row->idbank,
				'bank' => $row->bank,
				'mbagi_hasil_id' => $row->mbagi_hasil_id,
				'nama_bagi_hasil' => $row->nama_bagi_hasil,
				'idakun' => $row->idakun,
				'posisi_akun' => $row->posisi_akun,

			);
			$this->db->insert('tvalidasi_kas_03_bagi_hasil_detail',$data_bayar);
		}
		$q="
			SELECT H.id,D.id as idbayar_id,D.jenis_kas_id,JK.nama as jenis_kas_nama,D.sumber_kas_id
			,SK.nama as sumber_nama,SK.bank_id as bankid,mbank.nama as bank
			,D.idmetode,RM.metode_bayar as metode_nama,D.nominal_bayar,SK.idakun,'K' as posisi_akun
			from tbagi_hasil_bayar_detail H
			LEFT JOIN tbagi_hasil_pembayaran D ON D.iddet=H.id
			LEFT JOIN mjenis_kas JK ON JK.id=D.jenis_kas_id
			LEFT JOIN msumber_kas SK ON SK.id=D.sumber_kas_id
			LEFT JOIN mbank ON mbank.id=SK.bank_id
			LEFT JOIN ref_metode RM ON RM.id=D.idmetode
			WHERE H.tbagi_hasil_bayar_id='$id'";
		$rows=$this->db->query($q)->result();
		foreach($rows as $r){
			$data_bayar=array(
				'idvalidasi' =>$idvalidasi,
				'idbayar_id' =>$r->idbayar_id,
				'jenis_kas_id' =>$r->jenis_kas_id,
				'jenis_kas_nama' =>$r->jenis_kas_nama,
				'sumber_kas_id' =>$r->sumber_kas_id,
				'sumber_nama' =>$r->sumber_nama,
				'bank' =>$r->bank,
				'bankid' =>$r->bankid,
				'idmetode' =>$r->idmetode,
				'metode_nama' =>$r->metode_nama,
				'nominal_bayar' =>$r->nominal_bayar,
				'idakun' =>$r->idakun,
				'posisi_akun' =>$r->posisi_akun,

			);
			$this->db->insert('tvalidasi_kas_03_bagi_hasil_bayar',$data_bayar);
		}
		if ($st_auto_posting=='1'){
			$data_header=array(
				
				'st_posting' => 1,
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
				

			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_kas',$data_header);
		}
		return true;
		// print_r('BERAHSIL');
	}

}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
