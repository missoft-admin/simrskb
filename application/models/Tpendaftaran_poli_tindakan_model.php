<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpendaftaran_poli_tindakan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function list_ruangan(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT H.* FROM `mtujuan` H 
			INNER JOIN mtujuan_user U ON U.tujuan_id=H.id
			WHERE H.`status`='1' AND U.userid='$user_id'";
		return $this->db->query($q)->result();
	}
	function get_data_login(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT H.id as ruangan_id,H.tanggal_login,H.st_login,H.nama_tujuan,H.iddokter,H.idpoli FROM mtujuan H WHERE H.user_login='$user_id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT id,nama FROM `mpoliklinik` H
			
			WHERE H.`status`='1'";
		return $this->db->query($q)->result();
	}
	function list_dokter(){
		$q="SELECT *FROM mdokter M
WHERE M.`status`='1'
";
		return $this->db->query($q)->result();
	}
	
}
