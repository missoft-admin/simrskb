<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tbuku_besar_hutang_setting_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($tipe,$id)
    {
		$where="AND M.id='".$tipe."-".$id."'";
        $q="SELECT  *FROM (
			SELECT CONCAT('1-',H.id) as id,H.nama,'1' as tipe_distributor,H.id as iddistributor,H.st_verifikasi from mdistributor H 
			UNION ALL
			SELECT CONCAT('3-',H.id) as id,H.nama,'3' as tipe_distributor,H.id as iddistributor,H.st_verifikasi from mvendor H 
			) M 
			WHERE M.id is not null ".$where."";
			// print_r($q);exit();
        return $this->db->query($q)->row_array();
    }
	function list_distributor(){
		$q="SELECT *FROM (
			SELECT CONCAT('1-',H.id) as id,H.nama from mdistributor H 
			UNION ALL
			SELECT CONCAT('3-',H.id) as id,H.nama from mvendor H 
			) M ORDER BY M.nama";
		return $this->db->query($q)->result();
			
	}
    public function saveData()
    {
        $this->kode 		= $_POST['kode'];
        $this->nama 		= $_POST['nama'];
        $this->alamat 	= $_POST['alamat'];
        $this->telepon 	= $_POST['telepon'];
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mdistributor', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->kode 		= $_POST['kode'];
        $this->nama 		= $_POST['nama'];
        $this->alamat 	= $_POST['alamat'];
        $this->telepon 	= $_POST['telepon'];
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mdistributor', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mdistributor', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function load_all()
    {
        $query = $this->db->get('mdistributor');
        return $query->result();
    }
}
