<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lkasir_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function list_user()
    {

        $this->db->where('status', '1');
        $this->db->where('st_kasir', '1');
        $query = $this->db->get('musers');

        return $query->result();
    }

	 public function list_dokter()
    {

        $this->db->where('status', '1');
        $query = $this->db->get('mdokter');

        return $query->result();
    }
	public function list_ob(){
		$q="SELECT T1.id,T1.tanggal,T1.no_bukti,T1.no_kasir,T1.nomedrec,T1.nama,GROUP_CONCAT(T1.tag separator '+') as tag
			,SUM(T1.tunai) as tunai,SUM(T1.non_tunai) as non_tunai,SUM(T1.kontraktor) as kontraktor,SUM(T1.t_t) as t_t
			,GROUP_CONCAT(T1.ket_bank) as ket_bank,GROUP_CONCAT(T1.nama_rekanan) as nama_rekanan, GROUP_CONCAT(T1.jaminan) as jaminan
			,GROUP_CONCAT(T1.nama_dokter) as nama_dokter,GROUP_CONCAT(T1.nama_pegawai) as nama_pegawai
			FROM (SELECT H.id,H.tanggal,P.nopenjualan as no_bukti,H.nokasir as no_kasir,P.nomedrec,P.nama,
			CASE
			WHEN B.idmetode='1' THEN 'T'
			WHEN B.idmetode='2' THEN 'DB'
			WHEN B.idmetode='3' THEN 'KR'
			WHEN B.idmetode='4' THEN 'TF'
			WHEN B.idmetode='5' AND B.idpegawai IS NOT NULL THEN 'TP'
			WHEN B.idmetode='5' AND B.iddokter IS NOT NULL THEN 'TD'
			WHEN B.idmetode='6' THEN 'TT'
			WHEN B.idmetode='7' THEN 'PA'
			END as tag
			,CASE WHEN B.idmetode='1' THEN B.nominal ELSE 0 END as tunai
			,CASE WHEN B.idmetode IN('2','3','4') THEN B.nominal ELSE 0 END as non_tunai
			,CASE WHEN B.idmetode IN('5','7') THEN B.nominal ELSE 0 END as kontraktor
			,CASE WHEN B.idmetode IN('6') THEN B.nominal ELSE 0 END as t_t,'' as porsi_dokter,
			CONCAT(mbank.noakun,'-',mbank.nama) as ket_bank,mrekanan.nama as nama_rekanan,B.jaminan,mdokter.nama as nama_dokter,mpegawai.nama as nama_pegawai
			from tkasir H
			INNER JOIN tkasir_pembayaran B ON H.id=B.idkasir
			INNER JOIN tpasien_penjualan P ON P.id=H.idtindakan
			LEFT JOIN mbank ON mbank.id=B.idbank
			LEFT JOIN mrekanan ON mrekanan.id=B.idkontraktor AND B.tipekontraktor='1'
			LEFT JOIN mdokter ON mdokter.id = B.iddokter AND B.idmetode='5' AND B.tipepegawai='2'
			LEFT JOIN mpegawai ON mpegawai.id=B.idpegawai AND B.idmetode='5' AND B.tipepegawai='1'
			WHERE H.idtipe='3' AND H.`status`='2') T1
			GROUP BY T1.id
			ORDER BY T1.tanggal ASC";
		return $this->db->query($q)->result();

	}
	public function list_rajal(){
		$q="SELECT T1.id,T1.tanggal,T1.no_bukti,T1.no_kasir,T1.nomedrec,T1.nama,GROUP_CONCAT(T1.tag separator '+') as tag
			,SUM(T1.tunai) as tunai,SUM(T1.non_tunai) as non_tunai,SUM(T1.kontraktor) as kontraktor,SUM(T1.t_t) as t_t
			,GROUP_CONCAT(T1.ket_bank) as ket_bank,GROUP_CONCAT(T1.nama_rekanan) as nama_rekanan, GROUP_CONCAT(T1.jaminan) as jaminan
			,GROUP_CONCAT(T1.nama_dokter) as nama_dokter,GROUP_CONCAT(T1.nama_pegawai) as nama_pegawai
			FROM (SELECT H.id,H.tanggal,P.nopenjualan as no_bukti,H.nokasir as no_kasir,P.nomedrec,P.nama,
			CASE
			WHEN B.idmetode='1' THEN 'T'
			WHEN B.idmetode='2' THEN 'DB'
			WHEN B.idmetode='3' THEN 'KR'
			WHEN B.idmetode='4' THEN 'TF'
			WHEN B.idmetode='5' AND B.idpegawai IS NOT NULL THEN 'TP'
			WHEN B.idmetode='5' AND B.iddokter IS NOT NULL THEN 'TD'
			WHEN B.idmetode='6' THEN 'TT'
			WHEN B.idmetode='7' THEN 'PA'
			END as tag
			,CASE WHEN B.idmetode='1' THEN B.nominal ELSE 0 END as tunai
			,CASE WHEN B.idmetode IN('2','3','4') THEN B.nominal ELSE 0 END as non_tunai
			,CASE WHEN B.idmetode IN('5','7') THEN B.nominal ELSE 0 END as kontraktor
			,CASE WHEN B.idmetode IN('6') THEN B.nominal ELSE 0 END as t_t,'' as porsi_dokter,
			CONCAT(mbank.noakun,'-',mbank.nama) as ket_bank,mrekanan.nama as nama_rekanan,B.jaminan,mdokter.nama as nama_dokter,mpegawai.nama as nama_pegawai
			from tkasir H
			INNER JOIN tkasir_pembayaran B ON H.id=B.idkasir
			INNER JOIN tpasien_penjualan P ON P.id=H.idtindakan
			LEFT JOIN mbank ON mbank.id=B.idbank
			LEFT JOIN mrekanan ON mrekanan.id=B.idkontraktor AND B.tipekontraktor='1'
			LEFT JOIN mdokter ON mdokter.id = B.iddokter AND B.idmetode='5' AND B.tipepegawai='2'
			LEFT JOIN mpegawai ON mpegawai.id=B.idpegawai AND B.idmetode='5' AND B.tipepegawai='1'
			WHERE H.idtipe='1' AND H.`status`='2') T1
			GROUP BY T1.id
			ORDER BY T1.tanggal ASC";
		return $this->db->query($q)->result();

	}
	public function list_penerimaan_dokter(){
		$q="SELECT iddokter,idtindakan,idpend,nopendaftaran,nokasir,no_medrec,namapasien,tag,nama_dokter,
			SUM(jasapelayanan) jasapelayanan, MAX(jasapelayanan_disc) jasapelayanan_disc, SUM(nominal_disc) nominal_disc,
			SUM(after_disc) after_disc, MAX(pot_rs) pot_rs, SUM(tot_potongan_rs) tot_potongan_rs,
			MAX(pajak_dokter) pajak_dokter, SUM(tot_pajak_dokter) tot_pajak_dokter, SUM(total_bersih), nama_user

			FROM
			(
			SELECT tpl.iddokter,tpl.idtindakan,tpend.id idpend,tpend.nopendaftaran,tkasir.nokasir,tpend.no_medrec,tpend.namapasien,IF(tkasir.idtipe = 1,'RJ',IF(tkasir.idtipe = 2,'IGD','')) tag,
			mdokter.nama nama_dokter, tpl.jasapelayanan,
			tpl.jasapelayanan_disc,
			(jasapelayanan_disc/ 100) * jasapelayanan nominal_disc,
			tpl.jasapelayanan - (SELECT nominal_disc)after_disc,
			(tpl.pot_rs) pot_rs , (tpl.pot_rs / 100) * (SELECT after_disc) tot_potongan_rs,
			tpl.pajak_dokter, (tpl.pajak_dokter / 100) * (SELECT after_disc) tot_pajak_dokter,

			(SELECT after_disc) - (SELECT tot_potongan_rs) - (SELECT tot_pajak_dokter) total_bersih,
			musers.name nama_user

			FROM tkasir
			INNER JOIN tpoliklinik_pelayanan tpl ON tkasir.idtindakan = tpl.idtindakan
			INNER JOIN tpoliklinik_tindakan tpd ON tpl.idtindakan = tpd.id
			INNER JOIN mdokter ON tpl.iddokter = mdokter.id
			INNER JOIN tpoliklinik_pendaftaran tpend ON tpend.id = tpd.idpendaftaran
			INNER JOIN musers ON  tkasir.edited_by = musers.id
			WHERE tkasir.`status` = 2 AND DATE_FORMAT( tkasir.tanggal, '%Y-%m-%d' ) BETWEEN '2020-11-16' AND '2020-11-18'
			AND tkasir.idtipe IN ( 1, 2 )

			) xxx
			GROUP BY iddokter,idtindakan,idpend WITH ROLLUP";
		return $this->db->query($q)->result();

	}

  public function getTotalKwitansiManual($tanggaldari, $tanggalsampai)
  {
      $this->db->select('SUM(nominal) AS total');
      $this->db->where('status', '1');
      $this->db->where('pasien_id !=', 0);
      $this->db->where('DATE(tanggal) >=', $tanggaldari);
      $this->db->where('DATE(tanggal) <=', $tanggalsampai);
      $query = $this->db->get('tkwitansi_manual');

      return $query->row()->total;
  }

  public function getTotalKasbon($tanggaldari, $tanggalsampai, $iduser)
  {
      $this->db->select('SUM(nominal) AS total');
      $this->db->where('DATE(tanggal) >=', $tanggaldari);
      $this->db->where('DATE(tanggal) <=', $tanggalsampai);
      $this->db->where('user_created', $iduser);
      $this->db->where('alokasidana', '2');
      $this->db->where('status', '1');
      $query = $this->db->get('tkasbon');

      return $query->row()->total;
  }

  public function getPendapatanNonTunai($tanggaldari, $tanggalsampai, $iduserinput)
  {
      // view_laporan_kasir_rawat_inap_non_tunai
      $query = $this->db->query("
      SELECT tanggal, bank, SUM(total_debit) AS total_debit, SUM(total_kredit) AS total_kredit, SUM(total_transfer) AS total_transfer
      FROM
      (
      	SELECT
      		trawatinap_tindakan_pembayaran.tanggal AS tanggal,
          mbank.id AS idbank,
      		mbank.nama AS bank,
      		SUM(
      			CASE
      			WHEN trawatinap_tindakan_pembayaran_detail.idmetode = 2 THEN
      				trawatinap_tindakan_pembayaran_detail.nominal
      			END
      		) AS total_debit,
      		SUM(
      			CASE
      			WHEN trawatinap_tindakan_pembayaran_detail.idmetode = 3 THEN
      				trawatinap_tindakan_pembayaran_detail.nominal
      			END
      		) AS total_kredit,
      		SUM(
      			CASE
      			WHEN trawatinap_tindakan_pembayaran_detail.idmetode = 4 THEN
      				trawatinap_tindakan_pembayaran_detail.nominal
      			END
      		) AS total_transfer
      	FROM
      		trawatinap_tindakan_pembayaran
      	JOIN trawatinap_tindakan_pembayaran_detail ON trawatinap_tindakan_pembayaran_detail.idtindakan = trawatinap_tindakan_pembayaran.id
      	JOIN mbank ON mbank.id = trawatinap_tindakan_pembayaran_detail.idbank
      	WHERE
      		trawatinap_tindakan_pembayaran.statusbatal = 0 AND trawatinap_tindakan_pembayaran.iduser_input = '$iduserinput' AND
			DATE(trawatinap_tindakan_pembayaran.tanggal) >= '$tanggaldari' AND
			DATE(trawatinap_tindakan_pembayaran.tanggal) <= '$tanggalsampai'
        GROUP BY mbank.id

      	UNION ALL

      	SELECT
      		trawatinap_deposit.tanggal AS tanggal,
          mbank.id AS idbank,
      		mbank.nama AS bank,
      		SUM(
      			CASE
      				WHEN trawatinap_deposit.idmetodepembayaran = 2 THEN
      					trawatinap_deposit.nominal
      			END
      		) AS total_debit,
      		SUM(
      			CASE
      				WHEN trawatinap_deposit.idmetodepembayaran = 3 THEN
      				trawatinap_deposit.nominal
      			END
      		) AS total_kredit,
      		SUM(
      			CASE
      				WHEN trawatinap_deposit.idmetodepembayaran = 4 THEN
      				trawatinap_deposit.nominal
      			END
      		) AS total_transfer
      	FROM
      		trawatinap_deposit
      	JOIN mbank ON mbank.id = trawatinap_deposit.idbank
      	WHERE
      		trawatinap_deposit.status = 1 AND trawatinap_deposit.iduserinput = '$iduserinput' AND
			DATE(trawatinap_deposit.tanggal) >= '$tanggaldari' AND
			DATE(trawatinap_deposit.tanggal) <= '$tanggalsampai'
        GROUP BY mbank.id
      ) AS result
      GROUP BY result.idbank");

      return $query->result();
  }

  public function getSetoranDepositRanap($tanggaldari, $tanggalsampai, $iduserinput)
  {
      // view_laporan_kasir_rawat_inap_setor_deposit
      $query = $this->db->query("SELECT
      	DATE(trawatinap_deposit.tanggal) AS tanggal,
      	trawatinap_deposit.nodeposit AS notransaksi,
      	trawatinap_pendaftaran.no_medrec AS nomedrec,
      	trawatinap_pendaftaran.namapasien AS namapasien,
        IF(trawatinap_pendaftaran.idtipe = 1, 'RI', 'ODS' ) AS tag_kunjungan,
        GROUP_CONCAT( GetMetodePembayaran(trawatinap_deposit.idmetodepembayaran, 0) SEPARATOR ' + ' ) AS tag_bayar,
      	'Deposit' AS tipe_bayar,
        SUM(
        CASE
        WHEN trawatinap_deposit.idmetodepembayaran = 1 THEN
          trawatinap_deposit.nominal
        END) AS pelunasan_tunai,
        SUM(CASE
        WHEN trawatinap_deposit.idmetodepembayaran = 2 OR trawatinap_deposit.idmetodepembayaran = 3 OR trawatinap_deposit.idmetodepembayaran = 4 THEN
          trawatinap_deposit.nominal
        END) AS pelunasan_nontunai,
        SUM(
        CASE
        WHEN trawatinap_deposit.idmetodepembayaran = 5 OR trawatinap_deposit.idmetodepembayaran = 6 OR trawatinap_deposit.idmetodepembayaran = 8 THEN
          trawatinap_deposit.nominal
        END) AS pelunasan_kontraktor,
        SUM(CASE
        WHEN trawatinap_deposit.idmetodepembayaran = 7 THEN
          trawatinap_deposit.nominal
        END) AS pelunasan_tidaktertagih,
        (CASE
          WHEN trawatinap_deposit.idmetodepembayaran = 2 OR trawatinap_deposit.idmetodepembayaran = 4 THEN
          mbank.nama ELSE ''
        END) AS keterangan
      FROM
        trawatinap_pendaftaran
      JOIN trawatinap_deposit ON trawatinap_deposit.idrawatinap = trawatinap_pendaftaran.id AND trawatinap_deposit.status = 1
      LEFT JOIN mbank ON mbank.id = trawatinap_deposit.idbank
      WHERE
        DATE(trawatinap_deposit.tanggal) >= '$tanggaldari' AND
        DATE(trawatinap_deposit.tanggal) <= '$tanggalsampai' AND
        trawatinap_deposit.iduserinput = '$iduserinput'
      GROUP BY
      	trawatinap_deposit.id");

      return $query->result();
  }

  public function getSetoranPelunasanRanap($tanggaldari, $tanggalsampai, $iduserinput)
  {
      // view_laporan_kasir_rawat_inap_setor_pelunasan
      $query = $this->db->query("
      SELECT
      	DATE(trawatinap_tindakan_pembayaran.tanggal) AS tanggal,
      	trawatinap_pendaftaran.nopendaftaran AS notransaksi,
      	trawatinap_pendaftaran.no_medrec AS nomedrec,
      	trawatinap_pendaftaran.namapasien AS namapasien,
      	IF(trawatinap_pendaftaran.idtipe = 1 , 'RI', 'ODS') AS tag_kunjungan,
      	GROUP_CONCAT(GetMetodePembayaran(trawatinap_tindakan_pembayaran_detail.idmetode, trawatinap_tindakan_pembayaran_detail.tipekontraktor) SEPARATOR ' + ') AS tag_bayar,
      	'PELUNASAN' AS tipe_bayar,
      	SUM(
      		CASE
      		WHEN trawatinap_tindakan_pembayaran_detail.idmetode = 1 THEN
      			trawatinap_tindakan_pembayaran_detail.nominal
      		END
      	) AS pelunasan_tunai,
      	SUM(
      		CASE
      		WHEN trawatinap_tindakan_pembayaran_detail.idmetode = 2 OR trawatinap_tindakan_pembayaran_detail.idmetode = 3 OR trawatinap_tindakan_pembayaran_detail.idmetode = 4 THEN
      			trawatinap_tindakan_pembayaran_detail.nominal
      		END
      	) AS pelunasan_nontunai,
      	SUM(
      		CASE
      		WHEN trawatinap_tindakan_pembayaran_detail.idmetode = 5 OR trawatinap_tindakan_pembayaran_detail.idmetode = 6 OR trawatinap_tindakan_pembayaran_detail.idmetode = 8 THEN
      			trawatinap_tindakan_pembayaran_detail.nominal
      		END
      	) AS pelunasan_kontraktor,
      	SUM(
      		CASE
      			WHEN trawatinap_tindakan_pembayaran_detail.idmetode = 7 THEN
      			trawatinap_tindakan_pembayaran_detail.nominal
      		END
      	) AS pelunasan_tidaktertagih,
      	trawatinap_tindakan_pembayaran.total AS total_tagihan,
      	(
      		CASE
      			WHEN trawatinap_tindakan_pembayaran_detail.idmetode = 2 OR trawatinap_tindakan_pembayaran_detail.idmetode = 4 THEN
      				CONCAT(
      					CONVERT(trawatinap_tindakan_pembayaran_detail.tracenumber USING utf8 ),
      					' - ',
      					mbank.nama
      				) ELSE ''
      		END
      	) AS keterangan,
      	trawatinap_tindakan_pembayaran_detail.idmetode AS idmetode,
      	trawatinap_tindakan_pembayaran_detail.tipekontraktor AS tipekontraktor
      FROM
      	trawatinap_pendaftaran
      LEFT JOIN trawatinap_tindakan_pembayaran ON trawatinap_tindakan_pembayaran.idtindakan = trawatinap_pendaftaran.id AND trawatinap_tindakan_pembayaran.statusbatal = 0
      LEFT JOIN trawatinap_tindakan_pembayaran_detail ON trawatinap_tindakan_pembayaran_detail.idtindakan = trawatinap_tindakan_pembayaran.id
      LEFT JOIN mbank ON mbank.id = trawatinap_tindakan_pembayaran_detail.idbank
      WHERE
      	trawatinap_pendaftaran.statuspembayaran = 1 AND
        DATE(trawatinap_tindakan_pembayaran.tanggal) >= '$tanggaldari' AND
        DATE(trawatinap_tindakan_pembayaran.tanggal) <= '$tanggalsampai' AND
        trawatinap_tindakan_pembayaran.iduser_input = '$iduserinput'
      GROUP BY
      	trawatinap_pendaftaran.id");

      return $query->result();
  }

  public function getSetoranRefundDepositRanap($tanggaldari, $tanggalsampai, $iduserinput)
  {
      // view_laporan_kasir_rawat_inap_setor_refund_deposit
      $query = $this->db->query("
      SELECT
      	DATE(trefund.tanggal) AS tanggal,
      	trefund.norefund AS notransaksi,
      	trawatinap_pendaftaran.no_medrec AS nomedrec,
      	trawatinap_pendaftaran.namapasien AS namapasien,
      	IF(trawatinap_pendaftaran.idtipe = 1, 'RI', 'ODS') AS tag_kunjungan,
      	GROUP_CONCAT(GetMetodePembayaran(trefund.metode, 0) SEPARATOR ' + ') AS tag_bayar,
      	'Refund' AS tipe_bayar,
      	SUM(
      		CASE
      			WHEN trefund.metode = 1 THEN
      			trefund.nominal
      		END
      	) AS pelunasan_tunai,
      	SUM(
      		CASE
      				WHEN trefund.metode = 2 OR trefund.metode = 3 OR trefund.metode = 4 THEN
      					trefund.nominal
      				END
      	) AS pelunasan_nontunai,
      	SUM(
      		CASE
      			WHEN trefund.metode = 5 OR trefund.metode = 6 OR trefund.metode = 8 THEN
      				trefund.nominal
      			END
      	) AS pelunasan_kontraktor,
      	SUM(
      		CASE
      			WHEN trefund.metode = 7 THEN
      				trefund.nominal
      			END
      	) AS pelunasan_tidaktertagih,
      	(CASE
      		WHEN
      			trefund.metode = 2 OR trefund.metode = 4 THEN
      			trefund.bank ELSE ''
      		END
      	) AS keterangan
      FROM
      trawatinap_pendaftaran
      JOIN trefund ON trefund.status = 1 AND trefund.tipe = 0 AND trefund.idtransaksi = trawatinap_pendaftaran.id
      WHERE
      DATE(trefund.tanggal) >= '$tanggaldari' AND
      DATE(trefund.tanggal) <= '$tanggalsampai' AND
      trefund.created_by = '$iduserinput'
      GROUP BY
      	trawatinap_pendaftaran.id");

      return $query->result();
  }

  public function getSetoranRefundTransaksiRanap($tanggaldari, $tanggalsampai, $iduserinput)
  {
      // view_laporan_kasir_rawat_inap_setor_refund_ranap
      $query = $this->db->query("
      SELECT
      	DATE(trefund.tanggal) AS tanggal,
      	trefund.norefund AS notransaksi,
      	trawatinap_pendaftaran.no_medrec AS nomedrec,
      	trawatinap_pendaftaran.namapasien AS namapasien,
      	IF(trawatinap_pendaftaran.idtipe = 1, 'RI', 'ODS') AS tag_kunjungan,
      	GROUP_CONCAT(GetMetodePembayaran(trefund.metode, 0) SEPARATOR ' + ') AS tag_bayar,
      	'Refund' AS tipe_bayar,
      	SUM(
      		CASE
      			WHEN trefund.metode = 1 THEN
      			trefund.nominal
      		END
      	) AS pelunasan_tunai,
      	SUM(
      		CASE
      				WHEN trefund.metode = 2 OR trefund.metode = 3 OR trefund.metode = 4 THEN
      					trefund.nominal
      				END
      	) AS pelunasan_nontunai,
      	SUM(
      		CASE
      			WHEN trefund.metode = 5 OR trefund.metode = 6 OR trefund.metode = 8 THEN
      				trefund.nominal
      			END
      	) AS pelunasan_kontraktor,
      	SUM(
      		CASE
      			WHEN trefund.metode = 7 THEN
      				trefund.nominal
      			END
      	) AS pelunasan_tidaktertagih,
      	(CASE
      		WHEN
      			trefund.metode = 2 OR trefund.metode = 4 THEN
      			trefund.bank ELSE ''
      		END
      	) AS keterangan
      FROM
      trawatinap_pendaftaran
      JOIN trefund ON trefund.status = 1 AND trefund.tipe = 2 AND trefund.tipetransaksi = 'rawatinap' AND trefund.idtransaksi = trawatinap_pendaftaran.id
      WHERE
      trawatinap_pendaftaran.status = 1 AND
      DATE(trefund.tanggal) >= '$tanggaldari' AND
      DATE(trefund.tanggal) <= '$tanggalsampai' AND
      trefund.created_by = '$iduserinput'
      GROUP BY
      	trawatinap_pendaftaran.id");

      return $query->result();
  }
}
