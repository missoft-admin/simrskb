<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_pembayaran_informasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDetailTransaction($id)
    {
        $row = $this->db->select("trm_pengajuan_informasi.id,
                trm_pembayaran_informasi.tanggal AS tanggal_pembayaran,
                trm_pengajuan_informasi.notransaksi AS no_pengajuan,
                trm_pengajuan_informasi.tanggal AS tanggal_pengajuan,
                trm_pengajuan_informasi.nama_pemohon,
                trm_pengajuan_informasi.keterangan,
                COUNT(trm_pengajuan_informasi_detail.id) AS jumlah_pengajuan,
                SUM(trm_pengajuan_informasi_detail.nominal - trm_pengajuan_informasi_detail.diskon_rp) AS sub_total,
                COALESCE(trm_pembayaran_informasi.diskon_rp, 0) AS diskon_rp,
                COALESCE(trm_pembayaran_informasi.grand_total, 0) AS grand_total")
            ->join("trm_pengajuan_informasi_detail", "trm_pengajuan_informasi_detail.idtransaksi = trm_pengajuan_informasi.id")
            ->join("trm_pembayaran_informasi", "trm_pembayaran_informasi.idpengajuan = trm_pengajuan_informasi.id AND trm_pengajuan_informasi.status = 1", "LEFT")
            ->where("trm_pengajuan_informasi.id", $id)
            ->where("trm_pengajuan_informasi_detail.status", "1")
            ->get("trm_pengajuan_informasi")
            ->row();

        return $row;
    }

    public function getPembayaran($id)
    {
        $this->db->where('idpengajuan', $id);
        $query = $this->db->get('trm_pembayaran_informasi');
        return $query->row();
    }

    public function getDetailPembayaran($id)
    {
        $this->db->select('trm_pembayaran_informasi_detail.*');
        $this->db->join('trm_pembayaran_informasi', 'trm_pembayaran_informasi.id = trm_pembayaran_informasi_detail.idtransaksi');
        $this->db->where('trm_pembayaran_informasi_detail.idpengajuan', $id);
        $query = $this->db->get('trm_pembayaran_informasi_detail');
        return $query->result();
    }

    public function getPengajuan($id)
    {
        $this->db->select('trm_pengajuan_informasi.id,
            trm_pengajuan_informasi.notransaksi AS no_pengajuan,
            trm_pengajuan_informasi.tanggal AS tanggal_pengajuan,
            trm_pengajuan_informasi.nama_pemohon,
            trm_pembayaran_informasi.sub_total,
            trm_pembayaran_informasi.diskon_rp,
            trm_pembayaran_informasi.grand_total,
            trm_pembayaran_informasi.selisih_pembayaran,
            musers.name AS user_created
        ');
        $this->db->join('trm_pengajuan_informasi_detail', 'trm_pengajuan_informasi_detail.idtransaksi = trm_pengajuan_informasi.id');
        $this->db->join('trm_pembayaran_informasi', 'trm_pembayaran_informasi.idpengajuan = trm_pengajuan_informasi.id');
        $this->db->join('musers', 'musers.id = trm_pembayaran_informasi.created_by');
        $this->db->where('trm_pengajuan_informasi.id', $id);
        $query = $this->db->get('trm_pengajuan_informasi');
        return $query->row();
    }

    public function getDetailPengajuan($id)
    {
        $this->db->select("trm_pengajuan_informasi_detail.id AS iddetail,
            trm_pengajuan_informasi_detail.tanggal_kunjungan,
            trm_pengajuan_informasi_detail.tanggal_pengajuan,
            trm_pengajuan_informasi_detail.pengajuan_ke,
            trm_layanan_berkas.iddokter,
            trm_layanan_berkas.no_medrec,
            trm_layanan_berkas.namapasien AS nama_pasien,
            mpengajuan_skd.nama AS jenis_pengajuan,
            (CASE WHEN trawatinap_pendaftaran.id
                THEN
                    IF(trawatinap_pendaftaran.idtipepasien = 1, 'Pasien RS', 'Pasien Pribadi')
                ELSE 
                    IF(tpoliklinik_pendaftaran.idtipepasien = 1, 'Pasien RS', 'Pasien Pribadi')
            END) AS jenis_pasien,

            (CASE WHEN trawatinap_pendaftaran.id
                THEN
                    trawatinap_pendaftaran.idkelompokpasien
                ELSE 
                    tpoliklinik_pendaftaran.idkelompokpasien
            END) AS idkelompok,

            (CASE WHEN trawatinap_pendaftaran.id
                THEN
                    mkelompok_ranap.nama
                ELSE 
                    mkelompok_poli.nama
            END) AS namakelompok,

            (CASE WHEN trawatinap_pendaftaran.id
                THEN
                    trawatinap_pendaftaran.idrekanan
                ELSE 
                    tpoliklinik_pendaftaran.idrekanan
            END) AS idrekanan,

            (CASE WHEN trawatinap_pendaftaran.id
                THEN
                    mrekanan_ranap.nama
                ELSE 
                    mrekanan_poli.nama
            END) AS namarekanan,
            trm_pengajuan_informasi_detail.nominal,
            trm_pengajuan_informasi_detail.tarif_dokter,
            trm_pengajuan_informasi_detail.diskon_rp AS diskon,
            trm_pengajuan_informasi_detail.nominal_akhir");
        $this->db->join('trm_pengajuan_informasi', 'trm_pengajuan_informasi.id = trm_pengajuan_informasi_detail.idtransaksi');
        $this->db->join('mpengajuan_skd', 'mpengajuan_skd.id = trm_pengajuan_informasi_detail.jenis_pengajuan');
        $this->db->join('trm_layanan_berkas', 'trm_layanan_berkas.id = trm_pengajuan_informasi_detail.idberkas');
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = trm_layanan_berkas.id_trx AND trm_layanan_berkas.tujuan IN (1, 2)', 'LEFT');
        $this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = trm_layanan_berkas.id_trx AND trm_layanan_berkas.tujuan IN (3, 4)', 'LEFT');
        $this->db->join('mpasien_kelompok mkelompok_poli', 'mkelompok_poli.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
        $this->db->join('mpasien_kelompok mkelompok_ranap', 'mkelompok_ranap.id = trawatinap_pendaftaran.idkelompokpasien', 'LEFT');
        $this->db->join('mrekanan mrekanan_poli', 'mrekanan_poli.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
        $this->db->join('mrekanan mrekanan_ranap', 'mrekanan_ranap.id = trawatinap_pendaftaran.idrekanan', 'LEFT');
        $this->db->where('trm_pengajuan_informasi_detail.idtransaksi', $id);
        $query = $this->db->get('trm_pengajuan_informasi_detail');
        return $query->result();
    }

    public function rejectTransaction($id)
    {
        $this->db->set("status_pembayaran", '0');
        $this->db->where("id", $id);
        if ($this->db->update("trm_pengajuan_informasi")) {

            $this->db->set("status", '0');
            $this->db->where("idpengajuan", $id);
            $this->db->update("trm_pembayaran_informasi");

            return true;
        } else {
            return false;
        }
    }

    public function approveTransaction($id)
    {
        $this->db->set("status_pembayaran", '2');
        $this->db->where("id", $id);
        if ($this->db->update("trm_pengajuan_informasi")) {
            return true;
        } else {
            return false;
        }
    }

    public function saveTransaction()
    {
        $idpengajuan = $this->input->post('idpengajuan');
        $tanggal_pembayaran = $this->input->post('tanggal_pembayaran');

        if ($this->deleteTransaction($idpengajuan)) {
            $this->tanggal = YMDFormat($tanggal_pembayaran);
            $this->idpengajuan = $idpengajuan;
            $this->sub_total = RemoveComma($this->input->post('sub_total'));
            $this->diskon_rp = RemoveComma($this->input->post('diskon_rp'));
            $this->grand_total = RemoveComma($this->input->post('grand_total'));
            $this->nominal_round = $this->diskon_rp - ($this->sub_total - $this->grand_total);
            $this->nominal_pembayaran = RemoveComma($this->input->post('nominal_pembayaran'));
            $this->selisih_pembayaran = RemoveComma($this->input->post('sisa_pembayaran'));
            
            $this->created_at = date("Y-m-d H:i:s");
            $this->created_by = $this->session->userdata('user_id');

            if ($this->db->insert('trm_pembayaran_informasi', $this)) {
                $idtransaksi = $this->db->insert_id();

                $detailPembayaran = json_decode($this->input->post('detail_pembayaran'));
                foreach ($detailPembayaran as $row) {
                    $dataPembayaranDetail = array(
                        'idtransaksi' => $idtransaksi,
                        'idpengajuan' => $idpengajuan,
                        'idmetode' => $row[0],
                        'nominal' => RemoveComma($row[4]),
                        'idpegawai' => $row[8],
                        'idbank' => $row[9],
                        'ket_cc' => $row[10],
                        'idkontraktor' => $row[11],
                        'jaminan' => $row[14],
                        'tracenumber' => $row[15],
                        'tipekontraktor' => $row[16],
                        'keterangan' => $row[3],
                    );

                    $this->db->insert('trm_pembayaran_informasi_detail', $dataPembayaranDetail);
                }

                $this->updateStatusPembayaranPengajuan($idpengajuan);

                $diskonPersen = ($this->diskon_rp / $this->sub_total * 100);
                $this->updateDiskonAllPengajuanDetail($idpengajuan, $diskonPersen);
            }

            return true;
        }
    }

    function deleteTransaction($idpengajuan)
    {
        $this->db->where("id", $idpengajuan);
        if ($this->db->delete("trm_pembayaran_informasi")) {
            $this->db->where("id", $idpengajuan);
            $this->db->delete("trm_pembayaran_informasi_detail");

            return true;
        } else {
            return false;
        }
    }

    function updateStatusPembayaranPengajuan($idpengajuan)
    {
        $this->db->set("status_pembayaran", '1');
        $this->db->where("id", $idpengajuan);
        $this->db->update("trm_pengajuan_informasi");
    }

    function updateDiskonAllPengajuanDetail($idpengajuan, $diskonPersen)
    {
        $this->db->set("status", '1');
        $this->db->where("idtransaksi", $idpengajuan);
        $query = $this->db->get("trm_pengajuan_informasi_detail");

        foreach ($query->result() as $row) {
            $iddetail = $row->id;
            $diskonAll = round($row->nominal_akhir * ($diskonPersen / 100));

            $this->db->set("diskon_all", $diskonAll);
            $this->db->where("id", $iddetail);
            $this->db->update("trm_pengajuan_informasi_detail");
        }
    }
}
