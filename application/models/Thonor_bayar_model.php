<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Thonor_bayar_model extends CI_Model
{
	public function list_kategori()
	{
		$q = 'SELECT id,nama from mdokter_kategori 
				
				ORDER BY nama ASC';
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_header($id)
	{
		$q = "SELECT MB.id as idrekening,H.*,MD.nama as kategori FROM `thonor_dokter` H
			LEFT JOIN mdokter M ON M.id=H.iddokter
			LEFT JOIN mdokter_kategori MD ON MD.id=M.idkategori
			LEFT JOIN mdokter_bank MB ON MB.iddokter=H.iddokter AND MB.pilih_default='1'
			WHERE H.id='$id'
			";
		$query = $this->db->query($q);
		return $query->row_array();
	}

	public function list_pembayaran($id)
	{
		$q = "SELECT JK.nama as jenis_kas,SK.nama as sumber_kas,D.*,MB.atas_nama,RB.bank,MB.norek
		FROM thonor_pembayaran D
		LEFT JOIN mjenis_kas JK ON JK.id=D.jenis_kas_id
		LEFT JOIN msumber_kas SK ON SK.id=D.sumber_kas_id
		LEFT JOIN mdokter_bank MB ON MB.id=D.idrekening
		LEFT JOIN ref_bank RB ON RB.id=MB.idbank
		WHERE D.idhonor='$id' AND D.status='1'
		";
		return $this->db->query($q)->result();
	}

	public function list_rekening($id)
	{
		$q = "SELECT M.id,M.norek,M.atas_nama,R.bank FROM mdokter_bank M
			LEFT JOIN ref_bank R ON R.id=M.idbank
			WHERE M.iddokter='$id'
		";
		return $this->db->query($q)->result();
	}

	public function save_pembayaran()
	{
		// print_r($this->input->post());exit();
		$xjenis_kas_id = $_POST['xjenis_kas_id'];
		$xsumber_kas_id = $_POST['xsumber_kas_id'];
		$xidmetode = $_POST['xidmetode'];
		$ket = $_POST['ket'];
		$xnominal_bayar = $_POST['xnominal_bayar'];
		$xiddet = $_POST['xiddet'];
		$xstatus = $_POST['xstatus'];
		$xtanggal_pencairan = $_POST['xtanggal_pencairan'];
		$idhonor = $this->input->post('idhonor');
		$idrekening = $this->input->post('xidrekening');
		foreach ($xjenis_kas_id as $index => $val) {
			if ($xiddet[$index] != '') {
				$data_detail = [
					'jenis_kas_id' => $xjenis_kas_id[$index],
					'sumber_kas_id' => $xsumber_kas_id[$index],
					'nominal_bayar' => RemoveComma($xnominal_bayar[$index]),
					'idmetode' => $xidmetode[$index],
					'ket_pembayaran' => $ket[$index],
					'tanggal_pencairan' => YMDFormat($xtanggal_pencairan[$index]),
					'status' => $xstatus[$index],
					'idrekening' => $idrekening[$index],
					'edited_by' => $this->session->userdata('user_id'),
					'edited_date' => date('Y-m-d H:i:s'),
				];

				$this->db->where('id', $xiddet[$index]);
				$this->db->update('thonor_pembayaran', $data_detail);
			} else {
				$data_detail = [
					'idhonor' => $idhonor,
					'jenis_kas_id' => $xjenis_kas_id[$index],
					'sumber_kas_id' => $xsumber_kas_id[$index],
					'nominal_bayar' => RemoveComma($xnominal_bayar[$index]),
					'idmetode' => $xidmetode[$index],
					'ket_pembayaran' => $ket[$index],
					'idrekening' => $idrekening[$index],
					'tanggal_pencairan' => YMDFormat($xtanggal_pencairan[$index]),
					'created_by' => $this->session->userdata('user_id'),
					'created_date' => date('Y-m-d H:i:s'),
				];
				$this->db->insert('thonor_pembayaran', $data_detail);
				// print_r($data_detail);exit();
			}
			if ($this->input->post('btn_simpan') == '2') {
				$data_detail = [
					'verifikasi_at' => date('Y-m-d H:i:s'),
					'verifikasi_by' => $this->session->userdata('user_id'),
					'varifikasi_nama' => $this->session->userdata('user_name'),
					'st_verifikasi' => 1,
					'status_pembayaran' => 1,
				];
				$this->db->where('id', $idhonor);
				$result = $this->db->update('thonor_dokter', $data_detail);
				$this->Tvalidasi_model->InsertJurnalKas_HonorDokter($idhonor);
			} else {
				$this->db->where(id, $idhonor);
				$this->db->update('thonor_dokter', ['status_pembayaran' => 1]);
			}
		}
		return true;
	}

	public function getHeadDokumenUpload($idhonor)
	{
		$this->db->where('id', $idhonor);
		$query = $this->db->get('thonor_dokter');
		return $query->row();
	}

	public function getListUploadedDocument($idhonor)
	{
		$this->db->where('idhonor', $idhonor);
		$query = $this->db->get('thonor_pembayaran_dokumen');
		return $query->result();
	}
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
