<?php

class Tkwitansi_manual_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

	function getAll(){
    $this->db->select('tkwitansi_manual.*, mfpasien.nama AS nama_pasien');
  	$this->db->join('mfpasien','mfpasien.id = tkwitansi_manual.pasien_id');
    $this->db->where('tkwitansi_manual.status','1');
		$query = $this->db->get('tkwitansi_manual');
		return $query->result();
  }

	function getAllPasien(){
		$this->db->order_by('no_medrec', 'DESC');
		$this->db->order_by('nama', 'ASC');
		$this->db->limit('100');
		$query = $this->db->get('mfpasien');
		return $query->result();
  }

  function getSpecified($id){
  	$this->db->select('tkwitansi_manual.*, mfpasien.nama AS nama_pasien');
  	$this->db->join('mfpasien','mfpasien.id = tkwitansi_manual.pasien_id');
  	$this->db->where('tkwitansi_manual.id',$id);
  	$query = $this->db->get('tkwitansi_manual');
    return $query->row();
  }

  function save() {
		$this->tanggal 			= YMDFormat($_POST['tanggal']).' '.$_POST['waktu'];
		$this->pasien_id 	  = $_POST['pasien_id'];
		$this->idmetode 	  = $_POST['idmetode'];
		$this->idbank 	    = $_POST['idbank'];
		$this->ket_cc 	    = $_POST['ket_cc'];
		$this->trace_number = $_POST['trace_number'];
		$this->nominal 	    = RemoveComma($_POST['nominal']);
		$this->jaminan 	    = $_POST['jaminan'];
    $this->keterangan 	= $_POST['keterangan'];
		$this->tipe 	      = $_POST['tipe'];
		$this->status 			= 1;

		if($this->db->insert('tkwitansi_manual', $this)){
      redirect('Tkwitansi_manual/redirect/'.$this->db->insert_id(),'location');
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
  }

  function update() {
		$this->tanggal 			= YMDFormat($_POST['tanggal']).' '.$_POST['waktu'];
		$this->pasien_id 	  = $_POST['pasien_id'];
		$this->idmetode 	  = $_POST['idmetode'];
		$this->idbank 	    = $_POST['idbank'];
		$this->ket_cc 	    = $_POST['ket_cc'];
		$this->trace_number = $_POST['trace_number'];
		$this->nominal 	    = RemoveComma($_POST['nominal']);
    $this->jaminan 	    = $_POST['jaminan'];
		$this->keterangan 	= $_POST['keterangan'];
		$this->tipe 	      = $_POST['tipe'];
		$this->status 			= 1;

		if($this->db->update('tkwitansi_manual', $this, array('id' => $_POST['id']))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
  }

	function remove($id){
		$this->status = 0;

		if($this->db->update('tkwitansi_manual', $this, array('id' => $id))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
  }

}

?>
