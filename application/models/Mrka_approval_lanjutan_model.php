<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mrka_approval_lanjutan_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
	public function list_jenis_all(){
		$q="SELECT M.id,M.nama 
			FROM mlogic_detail L 
			LEFT JOIN mjenis_pengajuan_rka M ON M.id=L.idjenis
			WHERE L.`status`='1'
			GROUP BY L.idjenis";
		return $this->db->query($q)->result();
	}
    public function saveData() {
		$id=$this->input->post('id');
		// print_r($this->input->post());exit();
		$tipe_rka=$this->input->post('tipe_rka');
		$idjenis=$this->input->post('idjenis');
		$unit_lain=$this->input->post('unit_lain');
		$idunit_pengaju=$this->input->post('idunit_pengaju');
		$st_user_unit=$this->input->post('st_user_unit');
		$st_user_unit_lain=$this->input->post('st_user_unit_lain');
		$st_user_bendahara=$this->input->post('st_user_bendahara');
		$idlogic=$this->input->post('idlogic');
		$data=array(
			'idvendor' => $this->input->post('idvendor'),
			'cara_pembayaran'=>$this->input->post('cara_pembayaran'),
			'jenis_pembayaran'=>$this->input->post('jenis_pembayaran'),			
			'memo_bendahara' => $this->input->post('memo_bendahara'),
			'unit_lain' => $this->input->post('unit_lain'),
			'st_user_unit' => $this->input->post('st_user_unit'),
			'st_user_unit_lain' => $this->input->post('st_user_unit_lain'),
			'st_user_bendahara' => $this->input->post('st_user_bendahara'),
			'st_berjenjang' => $this->input->post('status_berjenjang'),
		);
		if ($this->input->post('btn_simpan')=='2'){
			$data['status']='3';
		}
		$this->db->where('id',$this->input->post('id'));
		$this->db->update('rka_pengajuan',$data);
		
		
		if ($this->input->post('btn_simpan')=='2'){
			// print_r($id);exit();
			$this->db->where('idrka',$id);
			$this->db->delete('rka_pengajuan_bendahara');
			
			$q="INSERT INTO rka_pengajuan_bendahara (idrka,tipe,iduser,nama_user,status)  
				SELECT '$id' as idrka,H.tipe_rka as tipe, H.iduser,M.`name` as nama_user,'1' as status FROM mlogic_user_bendahara H
											LEFT JOIN musers M  ON M.id=H.iduser
											WHERE H.idlogic='$idlogic' AND H.tipe_rka='$tipe_rka' AND H.idjenis='$idjenis'";
				$this->db->query($q);
				
			$this->db->where('idrka',$id);
			$this->db->delete('rka_pengajuan_user_proses');
			
			if ($st_user_unit=='1'){
				$q="INSERT INTO rka_pengajuan_user_proses (id,idrka,tipe,iduser,nama_user,status,idunit)  
					SELECT null,'$id' as idrka,'1' as tipe,S.userid as iduser, M.`name` as nama_user,'1' as status,'$idunit_pengaju' from munitpelayanan_user_setting S
					LEFT JOIN musers M ON S.userid=M.id
					WHERE S.idunit='$idunit_pengaju'";
				$this->db->query($q);
			}
			if ($st_user_unit_lain=='1'){
				$q="INSERT INTO rka_pengajuan_user_proses (id,idrka,tipe,iduser,nama_user,status,idunit)  
					SELECT null,'$id' as idrka,'2' as tipe,S.userid as iduser, M.`name` as nama_user,'1' as status,'$unit_lain' from munitpelayanan_user_setting S
					LEFT JOIN musers M ON S.userid=M.id
					WHERE S.idunit='$unit_lain'";
				$this->db->query($q);
			}
			if ($st_user_bendahara=='1'){
				$q="INSERT INTO rka_pengajuan_user_proses (id,idrka,tipe,iduser,nama_user,status)  
				SELECT null,'$id' as idrka,'3' as tipe,S.iduser as userid, M.`name` as nama_user,'1' as status FROM mlogic_user_bendahara S
				LEFT JOIN musers M ON M.id=S.iduser
				WHERE S.idlogic='$idlogic'";
				// print_r($q);exit();
				$this->db->query($q);
			}
		}
		return true;
    }
    function list_unit_lain(){
		$q="SELECT L.idunit,M.nama FROM mlogic_unit L
			LEFT JOIN munitpelayanan M ON M.id=L.idunit";
		return $this->db->query($q)->result();
	}
}
