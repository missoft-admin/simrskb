<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpendaftaran_poli_igd_pasien_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function list_poli($mppa_id){
		$q="SELECT M.id,M.nama FROM mppa_poli H
				INNER JOIN mpoliklinik M ON M.id=H.idpoliklinik
				WHERE H.mppa_id='$mppa_id' AND H.setting_dokter='1' AND M.`status`='1'
				AND H.idpoliklinik IN (SELECT idpoliklinik FROM mpoliklinik_emergency)
				";
		return $this->db->query($q)->result();
	}
		function list_dokter(){
		$q="SELECT *FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	
}
