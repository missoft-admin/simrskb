<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tvalidasi_pendapatan_model extends CI_Model
{
   public function list_pendapatan(){
	   $q="SELECT id,keterangan as nama from mpendapatan 
				WHERE mpendapatan.`status`='1'
				ORDER BY keterangan ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_dari(){
	   $q="SELECT id,nama from mdari 
				WHERE mdari.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getHeader($id){
	   $q="SELECT tvalidasi_pendapatan.* from tvalidasi_pendapatan 
				WHERE tvalidasi_pendapatan.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function saveData(){
	   $id=($this->input->post('id'));
		// print_r($this->input->post());exit();
		$iddet=($this->input->post('iddet'));
		$idakun_tf=($this->input->post('idakun_tf'));
		$row_pendapatan=$this->get_nama_akun($this->input->post('idakun_pendapatan'));
		// print_r($row_pendapatan);exit();
		$data_header=array(
			'idakun_pendapatan'=>$this->input->post('idakun_pendapatan'),
			'noakun_pendapatan'=>$row_pendapatan->noakun,
			'namaakun_pendapatan'=>$row_pendapatan->namaakun,
		);
		$this->db->where('id',$id);
		$this->db->update('tvalidasi_pendapatan',$data_header);
		
		$st_posting=($this->input->post('st_posting'));
		$btn_simpan=($this->input->post('btn_simpan'));
		foreach($iddet as $index => $val){
			$row_tf=$this->get_nama_akun($idakun_tf[$index]);
			
			$detail=array(
				'idakun'=>$idakun_tf[$index],
				'noakun'=>$row_tf->noakun,
				'namaakun'=>$row_tf->namaakun,
				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_bayar',$detail);
		}
		if ($st_posting=='1'){
			$this->db->where('id',$id);
			$this->db->update('tvalidasi_pendapatan',array('st_posting'=>0));
		}
		if ($btn_simpan=='2'){
			$data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $id);
			$result=$this->db->update('tvalidasi_pendapatan', $data);
		}
		
		return true;
   }
   function get_nama_akun($id){
	   $q="SELECT *from makun_nomor A WHERE A.id='$id'";
	   return $this->db->query($q)->row();
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
