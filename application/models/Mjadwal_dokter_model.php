<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mjadwal_dokter_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($iddokter, $idpoliklinik, $idjadwal)
    {
        $this->db->select('
            mdokter.id,
            mdokter.nama,
            mjadwal_dokter.id AS idjadwal,
            mpoliklinik.id AS idpoliklinik,
            mpoliklinik.nama AS poliklinik,
            mjadwal_dokter.iddokter,
            mjadwal_dokter.hari,
            mjadwal_dokter.jam_dari,
            mjadwal_dokter.jam_sampai,
            mjadwal_dokter.idruangan,
            mjadwal_dokter.keterangan');
        $this->db->where('mdokter.id', $iddokter);
        $this->db->where('mpoliklinik.id', $idpoliklinik);
        if ($idjadwal) $this->db->where('mjadwal_dokter.id', $idjadwal);

        $this->db->join('mpoliklinik_dokter', 'mpoliklinik_dokter.iddokter = mdokter.id');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = mpoliklinik_dokter.idpoliklinik');
        $this->db->join('mjadwal_dokter', 'mjadwal_dokter.iddokter = mdokter.id', 'LEFT');
        $query = $this->db->get('mdokter');
        return $query->row();
    }
	public function list_poli(){
		$q="SELECT H.idpoliklinik as idpoli,MP.nama FROM mjadwal_dokter H
			LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
			WHERE H.kuota > 0
			GROUP BY H.idpoliklinik";
		return $this->db->query($q)->result();
	}
	public function list_dokter(){
		$q="SELECT H.iddokter,MD.nama FROM mjadwal_dokter H
				LEFT JOIN mdokter MD ON MD.id=H.iddokter
				
				GROUP BY H.iddokter";
		return $this->db->query($q)->result();
	}
    public function getJadwalDokter($iddokter, $idpoliklinik)
    {
        $this->db->select('
            mjadwal_dokter.id AS idjadwal,
            mjadwal_dokter.idpoliklinik,
            mjadwal_dokter.iddokter,
            mdokter.nama AS dokter,
            mjadwal_dokter.hari,
            mjadwal_dokter.jam_dari,
            mjadwal_dokter.jam_sampai,
            mjadwal_dokter.idruangan,
            mjadwal_dokter.kuota,
            mjadwal_dokter.notes,
            mjadwal_dokter.st_reservasi_online,
            mruangan.nama AS ruangan,
            mjadwal_dokter.keterangan');
        $this->db->where('mjadwal_dokter.iddokter', $iddokter);
        $this->db->where('mjadwal_dokter.idpoliklinik', $idpoliklinik);
        $this->db->where('mjadwal_dokter.status', '1');
        $this->db->join('mdokter', 'mdokter.id = mjadwal_dokter.iddokter', 'LEFT');
        $this->db->join('mruangan', 'mruangan.id = mjadwal_dokter.idruangan', 'LEFT');
        $this->db->order_by('mjadwal_dokter.kodehari,mjadwal_dokter.jam_dari', 'ASC');
		// $this->db->where('status',1);
        $query = $this->db->get('mjadwal_dokter');
        return $query->result();
    }

    public function getScheduleDokter($iddokter, $idpoliklinik)
    {
        $this->db->select('mdokter.id, mdokter.foto, mdokter.nama, mdokter.nip, mdokter.jeniskelamin, COUNT(DISTINCT mjadwal_dokter.idpoliklinik) + 1 AS rowspan');
        $this->db->join('mdokter', 'mdokter.id = mjadwal_dokter.iddokter', 'LEFT');
        if ($iddokter) $this->db->where('mjadwal_dokter.iddokter', $iddokter);
        if ($idpoliklinik) $this->db->where('mjadwal_dokter.idpoliklinik', $idpoliklinik);
        $this->db->where('mjadwal_dokter.status', '1');
        $this->db->group_by('mjadwal_dokter.iddokter');
        $query = $this->db->get('mjadwal_dokter');
        return $query->result();
    }

    public function getScheduleDokterPoliklinik($iddokter, $idpoliklinik)
    {
        $this->db->select('mpoliklinik.id, mpoliklinik.nama');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = mjadwal_dokter.idpoliklinik', 'LEFT');
        $this->db->where('mjadwal_dokter.iddokter', $iddokter);
        if ($idpoliklinik) $this->db->where('mjadwal_dokter.idpoliklinik', $idpoliklinik);
        $this->db->where('mjadwal_dokter.status', '1');
        $this->db->group_by('mjadwal_dokter.idpoliklinik');
        $query = $this->db->get('mjadwal_dokter');
        return $query->result();
    }

    public function getScheduleDokterPoliklinikDay($iddokter, $idpoliklinik, $hari)
    {
        $this->db->select('mpoliklinik.nama AS poliklinik, 
        TIME_FORMAT(mjadwal_dokter.jam_dari, "%H:%i") AS jam_dari,
        TIME_FORMAT(mjadwal_dokter.jam_sampai, "%H:%i") AS jam_sampai,
        mruangan.nama AS ruangan');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = mjadwal_dokter.idpoliklinik', 'LEFT');
        $this->db->join('mruangan', 'mruangan.id = mjadwal_dokter.idruangan', 'LEFT');
        $this->db->where('mjadwal_dokter.iddokter', $iddokter);
        $this->db->where('mjadwal_dokter.idpoliklinik', $idpoliklinik);
        $this->db->where('mjadwal_dokter.hari', $hari);
        $this->db->where('mjadwal_dokter.status', '1');
        $query = $this->db->get('mjadwal_dokter');
        return $query->result();
    }

    public function getLeaveDokter($iddokter, $tanggal)
    {
        $query = $this->db->query("SELECT
            mcuti_dokter.sebab,
            mcuti_dokter.alasan
        FROM
            mcuti_dokter
        JOIN mdokter ON mdokter.id = mcuti_dokter.iddokter
        WHERE
            mcuti_dokter.iddokter = $iddokter AND
            DATE('$tanggal') >= DATE(mcuti_dokter.tanggal_dari) AND
	        DATE('$tanggal') <= DATE(mcuti_dokter.tanggal_sampai)
        ");

        return $query->result();
    }

    public function saveData()
    {
        // $this->id = $_POST['idjadwal'];
        $this->iddokter = $_POST['iddokter'];
        $this->idpoliklinik = $_POST['idpoliklinik'];
		if ($_POST['idjadwal']==''){
			$this->hari = $_POST['hari'];
		}else{
			
			$this->id = $_POST['idjadwal'];
		}
        $this->jam_dari = $_POST['jam_dari'];
			
        $this->jam_sampai = $_POST['jam_sampai'];
        $this->idruangan = $_POST['idruangan'];
        $this->keterangan = $_POST['keterangan'];
		// print_r($this);exit;
		if ($_POST['idjadwal']==''){
			$hasil=$this->db->insert('mjadwal_dokter', $this);
		}else{
			$this->db->where('id',$_POST['idjadwal']);
			$hasil=$this->db->update('mjadwal_dokter', $this);
		}
		// $hasil='';
        if ($hasil) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function save_slot()
    {
        $id = $_POST['id'];
        $kuota = $_POST['kuota'];
        $notes = $_POST['notes'];
        $st_reservasi_online = $_POST['st_reservasi_online'];
        foreach($id as $index => $val){
			$data_detail=array(
				'kuota' => $kuota[$index],
				'notes' => $notes[$index],
				'st_reservasi_online' => $st_reservasi_online[$index],
			);
			// print_r($data_detail);exit;
			$this->db->where('id',$val);
			$this->db->update('mjadwal_dokter',$data_detail);
		}
		// print_r($this->input->post());exit;
		return true;
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('mjadwal_dokter', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function hardDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('mjadwal_dokter', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
