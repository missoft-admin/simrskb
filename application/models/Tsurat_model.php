<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tsurat_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function update_cetak($assesmen_id, $jenis_surat)
	{
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data = [
			'assesmen_id' => $assesmen_id,
			'printed_by' => $login_ppa_id,
			'printed_date' => date('Y-m-d H:i:s'),
			'jenis_surat' => $jenis_surat,
		];
		// $nama_tabel='tsurat_cetak';
		if ($jenis_surat=='1'){
			$nama_tabel='tsurat_sakit';
		}
		if ($jenis_surat=='2'){
			$nama_tabel='tsurat_sehat';
		}
		if ($jenis_surat=='3'){
			$nama_tabel='tsurat_kontrol';
		}
		if ($jenis_surat=='4'){
			$nama_tabel='tsurat_lanjut';
		}
		if ($jenis_surat=='5'){
			$nama_tabel='tsurat_rawat';
		}
		if ($jenis_surat=='6'){
			$nama_tabel='tsurat_external';
		}
		if ($jenis_surat=='7'){
			$nama_tabel='tsurat_amputasi';
		}
		
		if ($jenis_surat=='8'){
			$nama_tabel='tsurat_diagnosa';
		}
		if ($jenis_surat=='9'){
			$nama_tabel='tsurat_skdp';
		}
		
		if ($jenis_surat=='99'){
			$nama_tabel='tranap_permintaan_dpjp';
		}
		// // print_r($data);exit();
		if ($this->db->insert('tsurat_cetak', $data)) {
			$id=$this->db->insert_id();
			$q="SELECT M.nama as nama_cetak,M.nip as nip_cetak 
			,H.printed_date,H.printed_number
			FROM tsurat_cetak H 
			INNER JOIN mppa M ON M.id=H.printed_by
			WHERE H.id='$id'";
			$data_his=$this->db->query($q)->row_array();
			$data_update = [
				'printed_by' => $login_ppa_id,
				'printed_date' => date('Y-m-d H:i:s'),
				'jumlah_cetak' => $data_his['printed_number'],
			];
			$this->db->where('assesmen_id',$assesmen_id);
			$this->db->update($nama_tabel,$data_update);
			return $data;
		} else {
			$data=array();
			$this->error_message = 'Penyimpanan Gagal';
			return $data;
		}
	}
	function setting_surat($id){
		$q="SELECT jenis_surat,kode,judul_ina,judul_eng,footer_ina,footer_eng,isi_surat FROM msurat_template H WHERE H.jenis_surat='$id' limit 1";
		return $this->db->query($q)->row_array();
	}
	function setting_surat_detail($id){
		$q="SELECT H.* FROM msurat_template_var_".$id." H limit 1";
		return $this->db->query($q)->row_array();
	}
	
	function list_dokter($idpoli){
		$q="SELECT M.id,M.nama FROM `mpoliklinik_konsul_dokter` H
				INNER JOIN mdokter M ON M.id=H.iddokter
				WHERE H.idpoliklinik='$idpoli'";
		return $this->db->query($q)->result();
	}
	function list_dokter_all(){
		$q="SELECT M.id,M.nama FROM `mdokter` M WHERE M.status='1'
				";
		return $this->db->query($q)->result();
	}
	function list_poliklinik(){
		$q="SELECT H.id,H.nama FROM mpoliklinik H WHERE H.status='1' AND H.idtipe='1'";
		return $this->db->query($q)->result();
	}
	function list_ppa_dokter(){
		$q="SELECT H.id,H.nama FROM surat_sakita H WHERE H.tipepegawai='2' AND H.jenis_profesi_id='1'";
		return $this->db->query($q)->result();
	}
	
	
	//SURAT SAKIT
	function get_data_surat_sakit($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='1'){
		$q="SELECT * FROM tsurat_sakit WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			
		}else{
			
		$q="SELECT * FROM tsurat_sakit WHERE pendaftaran_id='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		// print_r($q);exit;
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_surat_sakit_trx($assesmen_id){
		$q="SELECT *
			 FROM tsurat_sakit WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	//SURAT SEHAT
	function get_data_surat_sehat($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='1'){
		$q="SELECT * FROM tsurat_sehat WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			
		}else{
			
		$q="SELECT * FROM tsurat_sehat WHERE pendaftaran_id='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		// print_r($q);exit;
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_surat_sehat_trx($assesmen_id){
		$q="SELECT *
			 FROM tsurat_sehat WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	//SURAT KONTROL
	function get_data_surat_kontrol($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='1'){
		$q="SELECT * FROM tsurat_kontrol WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			
		}else{
			
		$q="SELECT * FROM tsurat_kontrol WHERE pendaftaran_id='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	
	function get_data_surat_kontrol_trx($assesmen_id){
		$q="SELECT *
		 FROM tsurat_kontrol WHERE assesmen_id='$assesmen_id'";
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	//SURAT LANJUT
	function get_data_surat_lanjut($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='1'){
		$q="SELECT * FROM tsurat_lanjut WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			
		}else{
			
		$q="SELECT * FROM tsurat_lanjut WHERE pendaftaran_id='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	
	function get_data_surat_lanjut_trx($assesmen_id){
		$q="SELECT *
		 FROM tsurat_lanjut WHERE assesmen_id='$assesmen_id'";
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	//SKDP
	function get_data_surat_skdp($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='1'){
		$q="SELECT * FROM tsurat_skdp WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			
		}else{
			
		$q="SELECT * FROM tsurat_skdp WHERE pendaftaran_id='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	
	function get_data_surat_skdp_trx($assesmen_id){
		$q="SELECT *
		 FROM tsurat_skdp WHERE assesmen_id='$assesmen_id'";
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	function get_data_surat_skdp_cetak($assesmen_id,$st_ranap='0'){
		if ($st_ranap=='1'){
		$q="SELECT 
			MP.no_medrec,MP.namapasien,MP.tanggal_lahir,MP.umurtahun,MP.umurbulan,MP.umurhari
			,DATE(MP.tanggaldaftar) as tanggal_daftar 
			,CASE WHEN MP.statuscheckout='1' THEN MP.tanggalcheckout ELSE null END as tanggal_checkout
			,MP.alamatpasien
			,desa.nama as nama_desa,kec.nama as nama_kecamatan,kota.nama as nama_kota,prov.nama as nama_provinsi
			,H.*,MD.nama as nama_dokter,P.ref as nama_pekerjaan
			FROM tsurat_skdp H 
			LEFT JOIN merm_referensi P ON P.nilai=H.pekerjaan_pasien AND P.ref_head_id='6'
			LEFT JOIN mdokter MD ON MD.id=H.dpjp
			LEFT JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id_ranap
			LEFT JOIN mfwilayah desa ON desa.id=MP.kelurahan_id
			LEFT JOIN mfwilayah kec ON kec.id=MP.kecamatan_id
			LEFT JOIN mfwilayah kota ON kota.id=MP.kabupaten_id
			LEFT JOIN mfwilayah prov ON prov.id=MP.provinsi_id
			WHERE H.assesmen_id='$assesmen_id'";
			
		}else{
		$q="SELECT 
			MP.no_medrec,MP.namapasien,MP.tanggal_lahir,MP.umurtahun,MP.umurbulan,MP.umurhari
			,MP.tanggal as tanggal_daftar,null as tanggal_checkout
			,MP.alamatpasien
			,desa.nama as nama_desa,kec.nama as nama_kecamatan,kota.nama as nama_kota,prov.nama as nama_provinsi
			,H.*,MD.nama as nama_dokter,P.ref as nama_pekerjaan
			FROM tsurat_skdp H 
			LEFT JOIN merm_referensi P ON P.nilai=H.pekerjaan_pasien AND P.ref_head_id='6'
			LEFT JOIN mdokter MD ON MD.id=H.dpjp
			LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
			LEFT JOIN mfwilayah desa ON desa.id=MP.kelurahan_id
			LEFT JOIN mfwilayah kec ON kec.id=MP.kecamatan_id
			LEFT JOIN mfwilayah kota ON kota.id=MP.kabupaten_id
			LEFT JOIN mfwilayah prov ON prov.id=MP.provinsi_id
			WHERE H.assesmen_id='$assesmen_id'";
		}
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	//SURAT DIAGNOSA
	function get_data_surat_diagnosa($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='1'){
		$q="SELECT * FROM tsurat_diagnosa WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			
		}else{
			
		$q="SELECT * FROM tsurat_diagnosa WHERE pendaftaran_id='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	
	function get_data_surat_diagnosa_trx($assesmen_id){
		$q="SELECT *
		 FROM tsurat_diagnosa WHERE assesmen_id='$assesmen_id'";
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	
	//SURAT RAWAT
	function get_data_surat_rawat($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='1'){
		$q="SELECT * FROM tsurat_rawat WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			
		}else{
			
		$q="SELECT * FROM tsurat_rawat WHERE pendaftaran_id='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	
	function get_data_surat_rawat_trx($assesmen_id){
		$q="SELECT *
		 FROM tsurat_rawat WHERE assesmen_id='$assesmen_id'";
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	
	//SURAT EXTERNAL
	function get_data_surat_external($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='1'){
		$q="SELECT * FROM tsurat_external WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			
		}else{
			
		$q="SELECT * FROM tsurat_external WHERE pendaftaran_id='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	
	function get_data_surat_external_trx($assesmen_id){
		$q="SELECT *
		 FROM tsurat_external WHERE assesmen_id='$assesmen_id'";
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	
	//SURAT AMPUTASI
	function get_data_surat_amputasi($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='1'){
		$q="SELECT 
			MP.no_medrec,MP.namapasien,MP.tanggal_lahir,MP.umurtahun,MP.umurbulan,MP.umurhari
			,DATE(MP.tanggaldaftar) as tanggal_daftar 
			,CASE WHEN MP.statuscheckout='1' THEN MP.tanggalcheckout ELSE null END as tanggal_checkout
			,MP.alamatpasien
			,desa.nama as nama_desa,kec.nama as nama_kecamatan,kota.nama as nama_kota,prov.nama as nama_provinsi
			,H.*
			FROM tsurat_amputasi H 
			LEFT JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id_ranap AND H.st_ranap='1'
			LEFT JOIN mfwilayah desa ON desa.id=MP.kelurahan_id
			LEFT JOIN mfwilayah kec ON kec.id=MP.kecamatan_id
			LEFT JOIN mfwilayah kota ON kota.id=MP.kabupaten_id
			LEFT JOIN mfwilayah prov ON prov.id=MP.provinsi_id
			WHERE H.pendaftaran_id_ranap='$pendaftaran_id_ranap' AND H.status_assemen IN (1,3) ORDER BY H.assesmen_id DESC LIMIT 1";
			
		}else{
			
		$q="SELECT 
			MP.no_medrec,MP.namapasien,MP.tanggal_lahir,MP.umurtahun,MP.umurbulan,MP.umurhari
			,MP.tanggal as tanggal_daftar,null as tanggal_checkout
			,MP.alamatpasien
			,desa.nama as nama_desa,kec.nama as nama_kecamatan,kota.nama as nama_kota,prov.nama as nama_provinsi
			,H.*
			FROM tsurat_amputasi H 
			LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id  AND H.st_ranap='0'
			LEFT JOIN mfwilayah desa ON desa.id=MP.kelurahan_id
			LEFT JOIN mfwilayah kec ON kec.id=MP.kecamatan_id
			LEFT JOIN mfwilayah kota ON kota.id=MP.kabupaten_id
			LEFT JOIN mfwilayah prov ON prov.id=MP.provinsi_id
			WHERE H.pendaftaran_id='$pendaftaran_id_ranap' AND H.status_assemen IN (1,3) ORDER BY H.assesmen_id DESC LIMIT 1";
		}
		// print_r($q);exit;
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	function get_data_surat_diagnosa_cetak($assesmen_id,$st_ranap='0'){
		if ($st_ranap=='1'){
		$q="SELECT 
			MP.no_medrec,MP.namapasien,MP.tanggal_lahir,MP.umurtahun,MP.umurbulan,MP.umurhari
			,DATE(MP.tanggaldaftar) as tanggal_daftar 
			,CASE WHEN MP.statuscheckout='1' THEN MP.tanggalcheckout ELSE null END as tanggal_checkout
			,MP.alamatpasien
			,desa.nama as nama_desa,kec.nama as nama_kecamatan,kota.nama as nama_kota,prov.nama as nama_provinsi
			,H.*,MD.nama as nama_dokter,P.ref as nama_pekerjaan
			FROM tsurat_diagnosa H 
			LEFT JOIN merm_referensi P ON P.nilai=H.pekerjaan_pasien AND P.ref_head_id='6'
			LEFT JOIN mdokter MD ON MD.id=H.dpjp
			LEFT JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id_ranap
			LEFT JOIN mfwilayah desa ON desa.id=MP.kelurahan_id
			LEFT JOIN mfwilayah kec ON kec.id=MP.kecamatan_id
			LEFT JOIN mfwilayah kota ON kota.id=MP.kabupaten_id
			LEFT JOIN mfwilayah prov ON prov.id=MP.provinsi_id
			WHERE H.assesmen_id='$assesmen_id'";
			
		}else{
		$q="SELECT 
			MP.no_medrec,MP.namapasien,MP.tanggal_lahir,MP.umurtahun,MP.umurbulan,MP.umurhari
			,MP.tanggal as tanggal_daftar,null as tanggal_checkout
			,MP.alamatpasien
			,desa.nama as nama_desa,kec.nama as nama_kecamatan,kota.nama as nama_kota,prov.nama as nama_provinsi
			,H.*,MD.nama as nama_dokter,P.ref as nama_pekerjaan
			FROM tsurat_diagnosa H 
			LEFT JOIN merm_referensi P ON P.nilai=H.pekerjaan_pasien AND P.ref_head_id='6'
			LEFT JOIN mdokter MD ON MD.id=H.dpjp
			LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
			LEFT JOIN mfwilayah desa ON desa.id=MP.kelurahan_id
			LEFT JOIN mfwilayah kec ON kec.id=MP.kecamatan_id
			LEFT JOIN mfwilayah kota ON kota.id=MP.kabupaten_id
			LEFT JOIN mfwilayah prov ON prov.id=MP.provinsi_id
			WHERE H.assesmen_id='$assesmen_id'";
		}
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
	function get_data_surat_amputasi_trx($assesmen_id,$st_ranap){
		if ($st_ranap=='1'){
		$q="SELECT 
			MP.no_medrec,MP.namapasien,MP.tanggal_lahir,MP.umurtahun,MP.umurbulan,MP.umurhari
			,DATE(MP.tanggaldaftar) as tanggal_daftar 
			,CASE WHEN MP.statuscheckout='1' THEN MP.tanggalcheckout ELSE null END as tanggal_checkout
			,MP.alamatpasien
			,desa.nama as nama_desa,kec.nama as nama_kecamatan,kota.nama as nama_kota,prov.nama as nama_provinsi
			,H.*
			FROM tsurat_amputasi H 
			LEFT JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id_ranap
			LEFT JOIN mfwilayah desa ON desa.id=MP.kelurahan_id
			LEFT JOIN mfwilayah kec ON kec.id=MP.kecamatan_id
			LEFT JOIN mfwilayah kota ON kota.id=MP.kabupaten_id
			LEFT JOIN mfwilayah prov ON prov.id=MP.provinsi_id
			WHERE H.assesmen_id='$assesmen_id'";
			
		}else{
		$q="SELECT 
			MP.no_medrec,MP.namapasien,MP.tanggal_lahir,MP.umurtahun,MP.umurbulan,MP.umurhari
			,MP.tanggal as tanggal_daftar,null as tanggal_checkout
			,MP.alamatpasien
			,desa.nama as nama_desa,kec.nama as nama_kecamatan,kota.nama as nama_kota,prov.nama as nama_provinsi
			,H.*
			FROM tsurat_amputasi H 
			LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
			LEFT JOIN mfwilayah desa ON desa.id=MP.kelurahan_id
			LEFT JOIN mfwilayah kec ON kec.id=MP.kecamatan_id
			LEFT JOIN mfwilayah kota ON kota.id=MP.kabupaten_id
			LEFT JOIN mfwilayah prov ON prov.id=MP.provinsi_id
			WHERE H.assesmen_id='$assesmen_id'";
		}
		 $hasil=$this->db->query($q)->row_array();
		
		return $hasil;
	}
}
