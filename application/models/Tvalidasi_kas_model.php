<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tvalidasi_kas_model extends CI_Model
{
   public function list_rekanan(){
	   $q="SELECT id,nama from mrekanan 
				WHERE mrekanan.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_kp(){
	   $q="SELECT id,nama from tvalidasi_kas_00_ref 
				ORDER BY id ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getHeader($id,$id_reff){
	   $nama_tabel=$this->db->query("SELECT M.nama_tabel_validasi FROM tvalidasi_kas H 
									LEFT JOIN tvalidasi_kas_00_ref M ON M.id=H.id_reff
									WHERE H.id='$id'")->row('nama_tabel_validasi');
		$q="SELECT H.* from ".$nama_tabel." H WHERE H.`idvalidasi`='$id'";
		// print_r("SELECT M.nama_tabel_validasi FROM tvalidasi_kas H 
									// LEFT JOIN tvalidasi_kas_00_ref M ON M.id=H.id_reff
									// WHERE H.id='$id'");exit();
	   // if ($id_reff=='1'){		   
	   // }
	   // if ($id_reff=='2'){		   
		// $q="SELECT H.* from tvalidasi_kas_02_refund H WHERE H.`idvalidasi`='$id'";
	   // }
	   // if ($id_reff=='2'){		   
		// $q="SELECT H.* from tvalidasi_kas_02_refund H WHERE H.`idvalidasi`='$id'";
	   // }
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function saveData(){
	   $idvalidasi=($this->input->post('idvalidasi'));
	   $iddet_ps=($this->input->post('iddet_ps'));
	   
	   $idakun_ps=($this->input->post('idakun_ps'));
	   $id_reff=($this->input->post('id_reff'));
	   $id=($this->input->post('id'));
		
		$iddet_bayar=($this->input->post('iddet_bayar'));
		$idakun=($this->input->post('idakun'));
		$idakun_bayar=($this->input->post('idakun_bayar'));
		$idakun_gaji=($this->input->post('idakun_gaji'));
		$iddet_gaji=($this->input->post('iddet_gaji'));
		
		$st_posting=($this->input->post('st_posting'));
		$btn_simpan=($this->input->post('btn_simpan'));
		$detail=array(
			'idakun'=>$idakun,			
		);
		$this->db->where('idvalidasi',$idvalidasi);
		if ($id_reff=='1'){
			$this->db->update('tvalidasi_kas_01_kasbon',$detail);			
		}
		if ($id_reff=='2'){
			if ($idtipe=='0'){
				$this->db->update('tvalidasi_kas_02_refund',$detail);					
			}
			if ($idtipe='1'){//Refund Obat
				$iddet_refund_obat=($this->input->post('iddet_refund_obat'));
				$idakun_beli=($this->input->post('idakun_beli'));
				$idakun_diskon=($this->input->post('idakun_diskon'));
				foreach($iddet_refund_obat as $index => $val){	
					$detail_refund=array(
						'idakun_beli'=>$idakun_beli[$index],
						'idakun_diskon'=>$idakun_diskon[$index],
						
					);
					$this->db->where('id',$val);				
					$this->db->update('tvalidasi_kas_02_refund_obat',$detail_refund);
				}
			}
		}
		
		
		
		if ($id_reff=='5'){
			$detail=array(
				'idakun_dari'=>$this->input->post('idakun_dari'),			
				'idakun_ke'=>$this->input->post('idakun_ke'),			
			);
			
			$this->db->update('tvalidasi_kas_05_mutasi',$detail);			
		}
		
		if ($id_reff=='7'){
			
			$detail7=array(
				'idakun1'=>$this->input->post('idakun1'),			
				'idakun2'=>$this->input->post('idakun2'),			
			);
			
			$this->db->update('tvalidasi_kas_07_penyesuaian',$detail7);			
		}
		if ($id_reff=='6'){
			// // print_r($detail);exit();
			// $this->db->update('tvalidasi_kas_06_pengajuan',$detail);		
				$iddet_pengajuan=($this->input->post('iddet_pengajuan'));
				if ($iddet_pengajuan){	
					$idakun_beli=($this->input->post('idakun_beli'));
					$idakun_ppn=($this->input->post('idakun_ppn'));
					$idakun_diskon=($this->input->post('idakun_diskon'));
					foreach($iddet_pengajuan as $index => $val){				
						$detail_gudang=array(
							'idakun_beli'=>$idakun_beli[$index],
							'idakun_ppn'=>$idakun_ppn[$index],
							'idakun_diskon'=>$idakun_diskon[$index],
							
						);
					// print_r($iddet_gudang);exit();
						$this->db->where('id',$val);				
						$this->db->update('tvalidasi_kas_06_pengajuan_detail',$detail_gudang);
					}
				}
				$iddet_piutang=($this->input->post('iddet_piutang'));
				if ($iddet_piutang){	
					
					$idakun_piutang=($this->input->post('idakun_piutang'));
					foreach($iddet_piutang as $index => $val){				
						$detail_piutang=array(
							'idakun'=>$idakun_piutang[$index],
							
						);
						$this->db->where('id',$val);				
						$this->db->update('tvalidasi_kas_06_pengajuan_piutang',$detail_piutang);
					}
				}
				$iddet_pengembalian=($this->input->post('iddet_pengembalian'));
				if ($iddet_pengembalian){	
					
					$idakun_pengembalian=($this->input->post('idakun_pengembalian'));
					foreach($iddet_pengembalian as $index => $val){				
						$detail_pengembalian=array(
							'idakun'=>$idakun_pengembalian[$index],
							
						);
						$this->db->where('id',$val);				
						$this->db->update('tvalidasi_kas_06_pengajuan_pengembalian',$detail_pengembalian);
					}
				}
		}
		if ($id_reff=='9'){
			$detail=array(
				'idakun_beban'=>$this->input->post('idakun_beban'),			
				'idakun_pajak'=>$this->input->post('idakun_pajak'),			
				'idakun_pembelian'=>$this->input->post('idakun_pembelian'),			
				'idakun_berobat'=>$this->input->post('idakun_berobat'),			
				'idakun_kasbon'=>$this->input->post('idakun_kasbon'),			
				'idakun_potongan'=>$this->input->post('idakun_potongan'),			
			);
			
			$this->db->update('tvalidasi_kas_09_honor',$detail);			
		}
		foreach($iddet_bayar as $index => $val){
			
			$detail=array(
				'idakun'=>$idakun_bayar[$index],
				
			);
			$this->db->where('id',$val);
			if ($id_reff=='1'){
				$this->db->update('tvalidasi_kas_01_kasbon_bayar',$detail);
			}
			if ($id_reff=='2'){
				$this->db->update('tvalidasi_kas_02_refund_bayar',$detail);
			}
			if ($id_reff=='3'){
				$this->db->update('tvalidasi_kas_03_bagi_hasil_bayar',$detail);
			}
			if ($id_reff=='4'){
				$this->db->update('tvalidasi_kas_04_gaji_bayar',$detail);
			}
			if ($id_reff=='6'){
				// print_r($detail);exit();
				$this->db->update('tvalidasi_kas_06_pengajuan_bayar',$detail);
			}
			if ($id_reff=='8'){
				// print_r($detail);exit();
				$this->db->update('tvalidasi_kas_08_gudang_bayar',$detail);
			}
			if ($id_reff=='9'){
				// print_r($detail);exit();
				$this->db->update('tvalidasi_kas_09_honor_bayar',$detail);
			}
		}
		if ($iddet_gaji){	
			foreach($iddet_gaji as $index => $val){				
				$detail=array(
					'idakun'=>$idakun_gaji[$index],
					
				);
			// print_r($val);exit();
				$this->db->where('id',$val);				
				$this->db->update('tvalidasi_kas_04_gaji_detail',$detail);
			}
		}
		
		if ($iddet_ps){	
			// print_r($idakun_ps);exit();
			foreach($iddet_ps as $index => $val){				
				$detail=array(
					'idakun'=>$idakun_ps[$index],
					
				);
				$this->db->where('id',$val);				
				$this->db->update('tvalidasi_kas_03_bagi_hasil_detail',$detail);
			}
		}
		$iddet_gudang=($this->input->post('iddet_gudang'));
		if ($iddet_gudang){	
			$idakun_beli=($this->input->post('idakun_beli'));
			$idakun_ppn=($this->input->post('idakun_ppn'));
			$idakun_diskon=($this->input->post('idakun_diskon'));
			foreach($iddet_gudang as $index => $val){				
				$detail_gudang=array(
					'idakun_beli'=>$idakun_beli[$index],
					'idakun_ppn'=>$idakun_ppn[$index],
					'idakun_diskon'=>$idakun_diskon[$index],
					
				);
			// print_r($iddet_gudang);exit();
				$this->db->where('id',$val);				
				$this->db->update('tvalidasi_kas_08_gudang_detail',$detail_gudang);
			}
		}
		if ($st_posting=='1'){
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_kas',array('st_posting'=>0));
		}
		if ($btn_simpan=='2'){
			$data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $idvalidasi);
			$result=$this->db->update('tvalidasi_kas', $data);
		}
		
		return true;
   }
   function get_nama_akun($id){
	   $q="SELECT *from makun_nomor A WHERE A.id='$id'";
	   return $this->db->query($q)->row();
   }
   function list_akun(){
	   $q="SELECT *from makun_nomor A ";
	   return $this->db->query($q)->result();
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
