<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Trm_berkas_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function poli()
    {
        $q="SELECT id,nama from mpoliklinik WHERE `STATUS`='1'";
        $query=$this->db->query($q);
        return $query->result();
    }

    public function list_dokter2()
    {
        $q="SELECT id,nama from mdokter WHERE `STATUS`='1'";
        $query=$this->db->query($q);
        return $query->result();
    }

    public function get_dokter($id)
    {
        $q="SELECT H.iddokter,H.iddokter_perujuk from trm_layanan_berkas H
			WHERE H.id='$id'";
        $query=$this->db->query($q);
        return $query->row_array();
    }

    public function get_data_pribadi($id)
    {
        $q="SELECT PK.nama as pekerjaan,P.nama as pendidikan,prov.nama as provinsi,kota.nama as kota,kec.nama as kecamatan,kel.nama as desa,
			M.*,M.title AS title_pasien, D.nama as last_dokter,M.lokasi_berkas
			from mfpasien M
			LEFT JOIN mdokter D ON D.id=M.last_visit_iddokter
			LEFT JOIN mfwilayah prov ON prov.id=M.provinsi_id
			LEFT JOIN mfwilayah kota ON kota.id=M.kabupaten_id
			LEFT JOIN mfwilayah kec ON kec.id=M.kecamatan_id
			LEFT JOIN mfwilayah kel ON kel.id=M.kelurahan_id
			LEFT JOIN mpendidikan P ON P.id=M.pendidikan_id
			LEFT JOIN mpekerjaan PK ON PK.id=M.pekerjaan_id

			WHERE M.id='$id'";
        $query=$this->db->query($q);
        return $query->row_array();
    }

    public function get_header_kodetifikasi($id)
    {
        $q="SELECT H.id_trx,H.idpoliklinik,H.statuspasienbaru,CASE WHEN H.statuspasienbaru='0' THEN 'PASIEN LAMA' ELSE 'PASIEN BARU' END as status_pasien,
			TP.statuskasus,CASE WHEN TP.statuskasus='1' THEN 'KASUS BARU' WHEN TP.statuskasus='2' THEN 'KASUS LAMA' ELSE 'TIDAK DIISI' END as statuskasus_nama,
			H.tanggal_trx,P.nama as namapoliklinik,H.tujuan,D.nama as namadokter,
			U.`name` as user_daftar,H.tanggal_kirim,H.user_kirim_nama,H.tanggal_kembali,H.iddokter_perujuk
			from trm_layanan_berkas H
			LEFT JOIN tpoliklinik_tindakan TP ON TP.idpendaftaran=H.id_trx
			LEFT JOIN mpoliklinik P ON P.id=H.idpoliklinik
			LEFT JOIN mdokter D ON D.id=H.iddokter
			LEFT JOIN musers U ON U.id=H.user_trx
			WHERE H.id='$id'";
        $query=$this->db->query($q);
        return $query->row_array();
    }

    public function auto_insert_tindakan_KO($idberkas, $id_trx, $tipe)
    {
        if ($tipe=='3') {
            $tipe='1';//RANAP
        } else {
            $tipe='2';//ODS
        }

        $iduser=$this->session->userdata('user_id');
        $nama_user=$this->session->userdata('user_name');
        $q="INSERT INTO trm_berkas_icd9 (idberkas,icd_id,kelompok_tindakan_id,jenis_id,cara_input,tipe_tabel,tanggal_created,user_created,user_name_created,auto_asal)
			SELECT *FROM (SELECT
			'$idberkas' as idberkas,KTI.icd9_id as icd_id,D.kelompok_tindakan_id as kelompok_tindakan_id,'2' as jenis_id,'3' as cara_input,'1' as tipe_tabel,NOW() as tanggal_created,'$iduser' as user_created,'$nama_user' as user_name_created,'KO' as auto_asal

			from tkamaroperasi_pendaftaran H
			INNER JOIN tkamaroperasi_tindakan D ON D.idpendaftaranoperasi=H.id
			INNER JOIN mkelompok_tindakan KT ON KT.id=D.kelompok_tindakan_id
			INNER JOIN mkelompok_tindakan_icd KTI ON KTI.kelompok_tindakan_id=KT.id AND KTI.jenis_id='1'
			WHERE H.idpendaftaran='$id_trx' AND H.tipe='$tipe')  TBL
			WHERE TBL.icd_id NOT IN (SELECT Ref.icd_id from trm_berkas_icd9 Ref WHERE Ref.idberkas='$idberkas' AND Ref.cara_input='3' AND Ref.auto_asal='KO')";
        $query=$this->db->query($q);
    }

    public function auto_insert_layanan_KO($idberkas, $id_trx, $tipe)
    {
        if ($tipe=='3') {
            $tipe='1';//RANAP
        } else {
            $tipe='2';//ODS
        }

        $iduser=$this->session->userdata('user_id');
        $nama_user=$this->session->userdata('user_name');
        $q="INSERT INTO trm_berkas_icd9 (idberkas,icd_id,tindakan_id,jenis_id,cara_input,tipe_tabel,tanggal_created,user_created,user_name_created,auto_asal)
			SELECT *FROM (SELECT
			'$idberkas' as idberkas,icd.icd9_id as icd_id,L.tindakan_id as tindakan_id,'2' as jenis_id,'3' as cara_input,'2' as tipe_tabel,NOW() as tanggal_created,'$iduser' as user_created,'$nama_user' as user_name_created,'KO' as auto_asal
			from tkamaroperasi_pendaftaran H
			INNER JOIN tkamaroperasi_jasado D ON D.idpendaftaranoperasi=H.id
			INNER JOIN mtindakan2icd_layanan L ON L.tarif_id=D.id_tarif AND L.tipe_id='6' AND L.`status`='1'
			INNER JOIN mtindakan2icd_icd icd ON icd.tindakan_id=L.tindakan_id AND icd.jenis_id='1'

			WHERE H.idpendaftaran='$id_trx' AND H.tipe='$tipe') AS TBL
			WHERE TBL.tindakan_id NOT IN (SELECT Ref.tindakan_id from trm_berkas_icd9 Ref WHERE Ref.idberkas='$idberkas' AND Ref.tindakan_id=TBL.tindakan_id AND Ref.cara_input='3' AND Ref.auto_asal='KO')";
        $query=$this->db->query($q);
    }

    public function auto_insert_layanan_sewa_alat_KO($idberkas, $id_trx, $tipe)
    {
        if ($tipe=='3') {
            $tipe='1';//RANAP
        } else {
            $tipe='2';//ODS
        }

        $iduser=$this->session->userdata('user_id');
        $nama_user=$this->session->userdata('user_name');
        $q="INSERT INTO trm_berkas_icd9 (idberkas,icd_id,tindakan_id,jenis_id,cara_input,tipe_tabel,tanggal_created,user_created,user_name_created,auto_asal)
			SELECT *FROM (
				SELECT
			'$idberkas' as idberkas,icd.icd9_id as icd_id,L.tindakan_id as tindakan_id,'2' as jenis_id,'3' as cara_input,'2' as tipe_tabel,NOW() as tanggal_created,'$iduser' as user_created,'$nama_user' as user_name_created,'KO' as auto_asal
			from tkamaroperasi_pendaftaran H
			INNER JOIN tkamaroperasi_sewaalat D ON D.idpendaftaranoperasi=H.id
			INNER JOIN mtindakan2icd_layanan L ON L.tarif_id=D.idalat AND L.tipe_id='7' AND L.`status`='1'
			INNER JOIN mtindakan2icd_icd icd ON icd.tindakan_id=L.tindakan_id AND icd.jenis_id='1'

			WHERE H.idpendaftaran='$id_trx' AND H.tipe='$tipe'
			) AS TBL
			WHERE TBL.tindakan_id NOT IN (SELECT Ref.tindakan_id from trm_berkas_icd9 Ref WHERE Ref.idberkas='$idberkas' AND Ref.tindakan_id=TBL.tindakan_id AND Ref.cara_input='3' AND Ref.auto_asal='KO')
			";
        $query=$this->db->query($q);
    }

    public function auto_insert_tindakan_Ranap($idberkas, $id_trx)
    {
        $iduser=$this->session->userdata('user_id');
        $nama_user=$this->session->userdata('user_name');
        $q="INSERT INTO trm_berkas_icd9 (idberkas,icd_id,tindakan_id,jenis_id,cara_input,tipe_tabel,tanggal_created,user_created,user_name_created,auto_asal)
				SELECT *FROM (
				SELECT '$idberkas' as idberkas,icd.icd9_id as icd_id,L.tindakan_id as tindakan_id,'2' as jenis_id,'3' as cara_input,'2' as tipe_tabel,NOW() as tanggal_created,'$iduser' as user_created,'$nama_user' as user_name_created,'RI' as auto_asal
				FROM trawatinap_tindakan H
				INNER JOIN mtindakan2icd_layanan L ON L.tarif_id=H.idpelayanan AND L.tipe_id='1'
				INNER JOIN mtindakan2icd_icd icd ON icd.tindakan_id=L.tindakan_id AND icd.jenis_id='1'
				WHERE H.idrawatinap='$id_trx'
				GROUP BY L.tindakan_id )  TBL
				WHERE TBL.icd_id NOT IN (SELECT Ref.icd_id from trm_berkas_icd9 Ref WHERE Ref.idberkas='$idberkas' AND Ref.cara_input='3' AND Ref.auto_asal='RI')";
        $query=$this->db->query($q);
        $q="UPDATE trm_berkas_icd9 INNER JOIN
			(SELECT *from trm_berkas_icd9
			H WHERE H.idberkas='$idberkas' AND H.status='1'
			ORDER BY H.id ASC LIMIT 1) REF ON REF.id=trm_berkas_icd9.id
			SET trm_berkas_icd9.jenis_id='1' WHERE trm_berkas_icd9.id=REF.id";
        $this->db->query($q);
    }

    public function auto_insert_tindakan_lab($idberkas)
    {
        $iduser=$this->session->userdata('user_id');
        $nama_user=$this->session->userdata('user_name');
        $q="INSERT INTO trm_berkas_icd9 (idberkas,icd_id,tindakan_id,jenis_id,cara_input,tipe_tabel,tanggal_created,user_created,user_name_created,auto_asal)
			SELECT *FROM (
			SELECT
			'$idberkas' as idberkas,icd.icd9_id as icd_id,L.tindakan_id as tindakan_id,'2' as jenis_id,'3' as cara_input,'2' as tipe_tabel,NOW() as tanggal_created,'$iduser' as user_created,'$nama_user' as user_name_created,'LAB' as auto_asal
			FROM (SELECT H.id,H.id_trx,H.tujuan,HP.id as poli_id,HRI.id as ri_id,
			CASE WHEN H.tujuan <= '2' THEN TP.id ELSE H.id_trx  END as idtindakan from trm_layanan_berkas H
			LEFT JOIN tpoliklinik_pendaftaran HP ON HP.id=H.id_trx AND (H.tujuan='1' OR H.tujuan='2')
			LEFT JOIN tpoliklinik_tindakan TP ON TP.idpendaftaran=HP.id
			LEFT JOIN trawatinap_pendaftaran HRI ON HRI.id=H.id_trx AND (H.tujuan='3' OR H.tujuan='4')
			WHERE H.id='$idberkas') Tberkas
			INNER JOIN trujukan_laboratorium Lab ON Lab.idtindakan=Tberkas.idtindakan AND Lab.asalrujukan=Tberkas.tujuan
			INNER JOIN trujukan_laboratorium_detail Det ON Det.idrujukan=Lab.id
			INNER JOIN mtindakan2icd_layanan L ON L.tarif_id=Det.idlaboratorium AND L.tipe_id='4'
			INNER JOIN mtindakan2icd_icd icd ON icd.tindakan_id=L.tindakan_id AND icd.jenis_id='1'
			WHERE Det.statusrincianpaket='0'
			ORDER BY Det.id) TBL WHERE TBL.tindakan_id NOT IN (SELECT Ref.tindakan_id from trm_berkas_icd9 Ref WHERE Ref.idberkas='$idberkas' AND Ref.cara_input='3' AND Ref.auto_asal='LAB') ";
        $query=$this->db->query($q);
    }

    public function auto_insert_tindakan_rad($idberkas)
    {
        $iduser=$this->session->userdata('user_id');
        $nama_user=$this->session->userdata('user_name');
        $q="INSERT INTO trm_berkas_icd9 (idberkas,icd_id,tindakan_id,jenis_id,cara_input,tipe_tabel,tanggal_created,user_created,user_name_created,auto_asal)
			SELECT *FROM (
			SELECT '$idberkas' as idberkas,icd.icd9_id as icd_id,L.tindakan_id as tindakan_id,'2' as jenis_id,'3' as cara_input,'2' as tipe_tabel,NOW() as tanggal_created,'$iduser' as user_created,'$nama_user' as user_name_created,'RAD' as auto_asal FROM (SELECT H.id,H.id_trx,H.tujuan,HP.id as poli_id,HRI.id as ri_id,
			CASE WHEN H.tujuan <= '2' THEN TP.id ELSE H.id_trx  END as idtindakan from trm_layanan_berkas H
			LEFT JOIN tpoliklinik_pendaftaran HP ON HP.id=H.id_trx AND (H.tujuan='1' OR H.tujuan='2')
			LEFT JOIN tpoliklinik_tindakan TP ON TP.idpendaftaran=HP.id
			LEFT JOIN trawatinap_pendaftaran HRI ON HRI.id=H.id_trx AND (H.tujuan='3' OR H.tujuan='4')
			WHERE H.id='$idberkas') Tberkas
			INNER JOIN trujukan_radiologi Lab ON Lab.idtindakan=Tberkas.idtindakan AND Lab.asalrujukan=Tberkas.tujuan
			INNER JOIN trujukan_radiologi_detail Det ON Det.idrujukan=Lab.id
			INNER JOIN mtindakan2icd_layanan L ON L.tarif_id=Det.idradiologi AND L.tipe_id='3'
			INNER JOIN mtindakan2icd_icd icd ON icd.tindakan_id=L.tindakan_id AND icd.jenis_id='1'
			) TBL WHERE TBL.tindakan_id NOT IN
			(SELECT Ref.tindakan_id from trm_berkas_icd9 Ref WHERE Ref.idberkas='$idberkas' AND Ref.cara_input='3' AND Ref.auto_asal='RAD') ";
        $query=$this->db->query($q);
    }

    public function auto_insert_tindakan_fisio($idberkas)
    {
        $iduser=$this->session->userdata('user_id');
        $nama_user=$this->session->userdata('user_name');
        $q="INSERT INTO trm_berkas_icd9 (idberkas,icd_id,tindakan_id,jenis_id,cara_input,tipe_tabel,tanggal_created,user_created,user_name_created,auto_asal)
			SELECT *FROM (
			SELECT '$idberkas' as idberkas,icd.icd9_id as icd_id,L.tindakan_id as tindakan_id,'2' as jenis_id,'3' as cara_input,'2' as tipe_tabel,NOW() as tanggal_created,'$iduser' as user_created,'$nama_user' as user_name_created,'FISIO' as auto_asal FROM (SELECT H.id,H.id_trx,H.tujuan,HP.id as poli_id,HRI.id as ri_id,
			CASE WHEN H.tujuan <= '2' THEN TP.id ELSE H.id_trx  END as idtindakan from trm_layanan_berkas H
			LEFT JOIN tpoliklinik_pendaftaran HP ON HP.id=H.id_trx AND (H.tujuan='1' OR H.tujuan='2')
			LEFT JOIN tpoliklinik_tindakan TP ON TP.idpendaftaran=HP.id
			LEFT JOIN trawatinap_pendaftaran HRI ON HRI.id=H.id_trx AND (H.tujuan='3' OR H.tujuan='4')
			WHERE H.id='$idberkas') Tberkas
			INNER JOIN trujukan_fisioterapi Lab ON Lab.idtindakan=Tberkas.idtindakan AND Lab.asalrujukan=Tberkas.tujuan
			INNER JOIN trujukan_fisioterapi_detail Det ON Det.idrujukan=Lab.id
			INNER JOIN mtindakan2icd_layanan L ON L.tarif_id=Det.idfisioterapi AND L.tipe_id='5'
			INNER JOIN mtindakan2icd_icd icd ON icd.tindakan_id=L.tindakan_id AND icd.jenis_id='1'
			) TBL WHERE TBL.tindakan_id NOT IN (SELECT Ref.tindakan_id from trm_berkas_icd9 Ref WHERE Ref.idberkas='$idberkas' AND Ref.cara_input='3' AND Ref.auto_asal='FISIO') ";
        $query=$this->db->query($q);
    }

    public function auto_insert_tindakan_poli($idberkas, $id_trx)
    {
        $iduser=$this->session->userdata('user_id');
        $nama_user=$this->session->userdata('user_name');
        $q="INSERT INTO trm_berkas_icd9 (idberkas,icd_id,tindakan_id,jenis_id,cara_input,tipe_tabel,tanggal_created,user_created,user_name_created,auto_asal)
			SELECT *FROM (
			SELECT '$idberkas' as idberkas,icd.icd9_id as icd_id,L.tindakan_id as tindakan_id,'2' as jenis_id,'3' as cara_input,'2' as tipe_tabel,NOW() as tanggal_created,'$iduser' as user_created,'$nama_user' as user_name_created,'PO' as auto_asal  from tpoliklinik_pendaftaran H
			INNER JOIN tpoliklinik_tindakan T ON T.idpendaftaran=H.id
			INNER JOIN tpoliklinik_pelayanan P ON P.idtindakan=T.id
			INNER JOIN mtindakan2icd_layanan L ON L.tarif_id=P.idpelayanan AND L.tipe_id='2'
			INNER JOIN mtindakan2icd_icd icd ON icd.tindakan_id=L.tindakan_id AND icd.jenis_id='1'
			WHERE H.id='$id_trx'
			GROUP BY P.idpelayanan
			ORDER BY P.id) TBL WHERE TBL.icd_id NOT IN (SELECT Ref.icd_id from trm_berkas_icd9 Ref WHERE Ref.idberkas='$idberkas' AND Ref.cara_input='3' AND Ref.auto_asal='PO')";
        $this->db->query($q);
        $q="UPDATE trm_berkas_icd9 INNER JOIN
			(SELECT *from trm_berkas_icd9
			H WHERE H.idberkas='$idberkas' AND H.status='1'
			ORDER BY H.id ASC LIMIT 1) REF ON REF.id=trm_berkas_icd9.id
			SET trm_berkas_icd9.jenis_id='1' WHERE trm_berkas_icd9.id=REF.id";
        $this->db->query($q);
    }

    public function auto_insert_perujuk($idberkas, $id_trx)
    {
        $q="UPDATE trm_layanan_berkas B INNER JOIN
			(
			SELECT H.id, TP.iddokter AS iddokter_perujuk from trawatinap_pendaftaran H
			INNER JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpoliklinik
			WHERE H.id='$id_trx'
			) D ON D.id=B.id_trx
			SET B.iddokter_perujuk=D.iddokter_perujuk
			WHERE B.id='$idberkas'";
        $this->db->query($q);
    }

    public function auto_insert_dokter($idberkas, $id_trx)
    {
        $iduser=$this->session->userdata('user_id');
        $nama_user=$this->session->userdata('user_name');

        $q="INSERT INTO trm_berkas_dokter (idberkas,iddokter,detail,tabel_asal,tanggal_created,user_created,user_name_created)
			SELECT *FROM (
			SELECT '$idberkas' as idberkas, iddokter,'Visite' as detail,'trawatinap_visite' as tabel_asal,
			NOW() as tanggal_created,'$iduser' as user_created,'$nama_user' as user_name_created
			from trawatinap_visite H
			INNER JOIN mdokter D ON D.id=H.iddokter
			WHERE H.idrawatinap='$id_trx' AND H.`status`='1') T WHERE T.iddokter NOT IN (SELECT B.iddokter FROM trm_berkas_dokter B WHERE B.idberkas='$idberkas' AND B.iddokter=T.iddokter)";
        $this->db->query($q);
    }

    public function auto_insert_diagnosa_ko($idberkas, $id_trx, $tipe)
    {
        if ($tipe=='3') {
            $tipe='1';//RANAP
        } else {
            $tipe='2';//ODS
        }
        $iduser=$this->session->userdata('user_id');
        $nama_user=$this->session->userdata('user_name');

        //INSERT KE ICD 10 Dari Kamar Operasi
        $q="INSERT INTO trm_berkas_icd10 (idberkas,icd_id,kelompok_diagnosa_id,jenis_id,cara_input,auto_asal,user_created,user_name_created,tanggal_created,tipe_tabel)
			SELECT *FROM(
			SELECT '$idberkas' as idberkas,KD.icd10_id as icd_id,D.kelompok_diagnosa_id,'2' as jenis_id,'3' as cara_input,'KO' as auto_asal,'$iduser' as user_created,'$nama_user' as user_name_created,NOW() as tanggal_created,'1' as tipe_tabel
			from tkamaroperasi_pendaftaran H
			INNER JOIN tkamaroperasi_diagnosa D ON D.idpendaftaranoperasi=H.id
			INNER JOIN mkelompok_diagnosa_icd KD ON KD.kelompok_diagnosa_id=D.kelompok_diagnosa_id AND KD.jenis_id='1'
			INNER JOIN icd_10 icd ON icd.id=KD.icd10_id
			WHERE H.idpendaftaran='$id_trx' AND H.tipe='$tipe'
			ORDER BY D.id ASC
			) T WHERE T.icd_id NOT IN (SELECT B.icd_id FROM trm_berkas_icd10 B WHERE B.idberkas='$idberkas' AND B.auto_asal='KO')";
        $this->db->query($q);
    }

    public function auto_insert_diagnosa_utama_ranap($idberkas, $id_trx)
    {
        $iduser=$this->session->userdata('user_id');
        $nama_user=$this->session->userdata('user_name');

        $q="INSERT INTO trm_berkas_icd10 (idberkas,icd_id,kelompok_diagnosa_id,jenis_id,cara_input,auto_asal,user_created,user_name_created,tanggal_created,tipe_tabel)
			SELECT *FROM (SELECT
			'$idberkas' as idberkas,icd.icd10_id as icd_id,K.id as kelompok_diagnosa_id,'2' as jenis_id,'3' as cara_input,'RI' as auto_asal,'$iduser' as user_created,'$nama_user' as user_name_created,NOW() as tanggal_created,'1' as tipe_tabel
			 from trawatinap_diagnosa H
			 INNER JOIN mkelompok_diagnosa K ON K.id=H.idkelompok_diagnosa
			 INNER JOIN mkelompok_diagnosa_icd icd ON icd.kelompok_diagnosa_id=K.id AND icd.jenis_id='1'

			WHERE H.idpendaftaran='$id_trx') T WHERE T.icd_id NOT IN (SELECT B.icd_id FROM trm_berkas_icd10 B WHERE B.idberkas='$idberkas' AND B.kelompok_diagnosa_id=T.kelompok_diagnosa_id AND T.tipe_tabel='1')";
        $this->db->query($q);
        //update jadi Utama Paling Atas
        $q="UPDATE trm_berkas_icd10 INNER JOIN
			(SELECT *from trm_berkas_icd10
			H WHERE H.idberkas='$idberkas' AND H.tipe_tabel='1' AND H.status='1'
			ORDER BY H.id ASC LIMIT 1) REF ON REF.id=trm_berkas_icd10.id
			SET trm_berkas_icd10.jenis_id='1' WHERE trm_berkas_icd10.id=REF.id";
        $this->db->query($q);
    }

    public function auto_insert_diagnosa_sebab_luar_ranap($idberkas, $id_trx)
    {
        $iduser=$this->session->userdata('user_id');
        $nama_user=$this->session->userdata('user_name');

        $q="INSERT INTO trm_berkas_icd10 (idberkas,icd_id,sebab_luar_id,jenis_id,cara_input,auto_asal,user_created,user_name_created,tanggal_created,tipe_tabel)
		SELECT *FROM (SELECT
		'$idberkas' as idberkas,icd.icd_id,H.sebabluar as msebab_luar,'4' as jenis_id,'3' as cara_input,'RI' as auto_asal,'$iduser' as user_created,'$nama_user' as user_name_created,NOW() as tanggal_created,'3' as tipe_tabel
		 from trawatinap_checkout H
		 INNER JOIN msebab_luar sebab ON sebab.id=H.sebabluar
		 INNER JOIN msebab_luar_icd icd ON icd.msebab_luar_id=sebab.id AND icd.jenis_id='1'

		WHERE H.idrawatinap='$id_trx') T WHERE T.jenis_id NOT IN (SELECT B.jenis_id FROM trm_berkas_icd10 B WHERE B.idberkas='$idberkas' AND B.jenis_id='4' AND B.auto_asal='RI' )";
        $this->db->query($q);
    }

    public function auto_insert_diagnosa_utama_poli($idberkas, $id_trx)
    {
        $iduser=$this->session->userdata('user_id');
        $nama_user=$this->session->userdata('user_name');

        $q="INSERT INTO trm_berkas_icd10 (idberkas,icd_id,kelompok_diagnosa_id,jenis_id,cara_input,auto_asal,user_created,user_name_created,tanggal_created,tipe_tabel)
			SELECT *FROM (
			SELECT '$idberkas' as idberkas, icd.icd10_id as icd_id,H.idkelompok_diagnosa as kelompok_diagnosa_id,'2' as jenis_id,'3' as cara_input,'PO' as auto_asal,
			'$iduser' as user_created,'$nama_user' as user_name_created,NOW() as tanggal_created,'1' as tipe_tabel
			FROM tpoliklinik_diagnosa H
			INNER  JOIN mkelompok_diagnosa K ON K.id=H.idkelompok_diagnosa
			INNER JOIN mkelompok_diagnosa_icd icd ON icd.kelompok_diagnosa_id=K.id AND icd.jenis_id='1'
			WHERE H.idpendaftaran='$id_trx'
			ORDER BY H.id ASC ) T WHERE T.icd_id NOT IN (SELECT B.icd_id FROM trm_berkas_icd10 B WHERE B.idberkas='$idberkas' AND B.kelompok_diagnosa_id=T.kelompok_diagnosa_id AND B.tipe_tabel='1')";
        $this->db->query($q);

        //update jadi Utama Paling Atas
        $q="UPDATE trm_berkas_icd10 INNER JOIN
			(SELECT *from trm_berkas_icd10
			H WHERE H.idberkas='$idberkas' AND H.tipe_tabel='1' AND H.status='1'
			ORDER BY H.id ASC LIMIT 1) REF ON REF.id=trm_berkas_icd10.id
			SET trm_berkas_icd10.jenis_id='1' WHERE trm_berkas_icd10.id=REF.id";
        $this->db->query($q);
    }

    public function auto_insert_diagnosa_kontrol_poli($idberkas, $id_trx, $idpoliklinik)
    {
        $iduser=$this->session->userdata('user_id');
        $nama_user=$this->session->userdata('user_name');
        $q="INSERT INTO trm_berkas_icd10 (idberkas,icd_id,kontrol_id,jenis_id,cara_input,auto_asal,user_created,user_name_created,tanggal_created,tipe_tabel)
				SELECT *FROM(SELECT
				'$idberkas' as idberkas,icd.icd_id,H.id as mkontrol_id,'3' as jenis_id,'3' as cara_input,'PO' as auto_asal,
				'$iduser' as user_created,'$nama_user' as user_name_created,NOW() as tanggal_created,'2' as tipe_tabel
				 from mkontrol H
				INNER JOIN mkontrol_icd icd ON icd.mkontrol_id=H.id AND icd.jenis_id='1'
				WHERE H.idpoliklinik='$idpoliklinik') T WHERE T.icd_id
				NOT IN (SELECT B.icd_id FROM trm_berkas_icd10 B WHERE B.jenis_id='3' AND B.kontrol_id=T.mkontrol_id AND B.idberkas='$idberkas')";
        $this->db->query($q);
    }

    public function auto_insert_diagnosa_sebab_luar_poli($idberkas, $id_trx)
    {
        $iduser=$this->session->userdata('user_id');
        $nama_user=$this->session->userdata('user_name');

        $q="INSERT INTO trm_berkas_icd10 (idberkas,icd_id,sebab_luar_id,jenis_id,cara_input,auto_asal,user_created,user_name_created,tanggal_created,tipe_tabel)
			SELECT *FROM (
			SELECT
			'$idberkas' as idberkas,icd.icd_id as icd_id,H.sebabluar as sebab_luar_id,'4' as jenis_id,'3' as cara_input,'PO' as auto_asal,'$iduser' as user_created,'$nama_user' as user_name_created,NOW() as tanggal_created,'3' as tipe_tabel
			from tpoliklinik_tindakan H
			LEFT JOIN msebab_luar M ON M.id=H.sebabluar
			INNER JOIN msebab_luar_icd icd ON icd.msebab_luar_id=M.id AND icd.jenis_id='1'

			WHERE H.idpendaftaran='$id_trx'
			) T WHERE T.jenis_id NOT IN (SELECT B.jenis_id FROM trm_berkas_icd10 B WHERE B.idberkas='$idberkas' AND B.jenis_id='4' AND B.auto_asal='PO')";
        $this->db->query($q);
    }

    public function list_dokter()
    {
        $q="SELECT id,nama from mdokter WHERE `STATUS`='1'";
        $query=$this->db->query($q);
        return $query->row();
    }

    public function js_kelompok_diagnosa()
    {
        $search=$this->input->post('search');
        $q="SELECT *from mkelompok_diagnosa
			WHERE  (nama LIKE '%".$search."%') AND status='1'";
        $query=$this->db->query($q);
        $result= $query->result();
        return $result;
    }

    public function js_kelompok_tindakan()
    {
        $search=$this->input->post('search');
        $q="SELECT *FROM (
				SELECT H.id,CONCAT(H.nama, ' [KT]') as nama ,'1' as tipe from mkelompok_tindakan H
				WHERE H.`status`='1'
				UNION
				SELECT H.id,CONCAT(H.nama, ' [KT TO ICD]') as nama,'2' as tipe from mtindakan2icd H
				WHERE H.`status`='1') TBL WHERE TBL.nama LIKE '%".$search."%'";
        $query=$this->db->query($q);
        $result= $query->result();
        return $result;
    }

    public function js_ket_kontrol()
    {
        $search=$this->input->post('search');
        $q="SELECT *from mkontrol
			WHERE  (nama LIKE '%".$search."%') AND status='1'";
        $query=$this->db->query($q);
        $result= $query->result();
        return $result;
    }

    public function js_sebab_luar()
    {
        $search=$this->input->post('search');
        $q="SELECT *from msebab_luar
			WHERE  (nama LIKE '%".$search."%') AND status='1'";
        $query=$this->db->query($q);
        $result= $query->result();
        return $result;
    }

    public function get_diagnosa($id)
    {
        $search=$this->input->post('search');
        $q="SELECT D.icd10_id as id,CONCAT(M.kode,' - ',M.deskripsi) as nama from mkelompok_diagnosa_icd D
			INNER JOIN icd_10 M ON M.id=D.icd10_id
			WHERE D.kelompok_diagnosa_id='$id' AND D.jenis_id='1'";
        $query=$this->db->query($q);
        $result= $query->row();
        return $result;
    }

    public function get_kt($id, $tipe)
    {
        if ($tipe=='1') {
            $q="SELECT D.icd9_id as id,CONCAT(icd_9.kode,' - ',icd_9.deskripsi) as nama from mkelompok_tindakan H
				LEFT JOIN mkelompok_tindakan_icd D ON H.id=D.kelompok_tindakan_id AND D.jenis_id='1'
				LEFT JOIN icd_9 ON icd_9.id=D.icd9_id
				WHERE H.id='$id'";
        } else {
            $q="SELECT D.icd9_id as id,CONCAT(icd_9.kode,' - ',icd_9.deskripsi) as nama  from mtindakan2icd H
				INNER JOIN mtindakan2icd_icd D ON D.tindakan_id=H.id AND D.jenis_id='1'
				LEFT JOIN icd_9 ON icd_9.id=D.icd9_id
				WHERE H.id='$id'";
        }

        $query=$this->db->query($q);
        $result= $query->row();
        return $result;
    }

    public function get_kontrol($id)
    {
        $search=$this->input->post('search');
        $q="SELECT D.icd_id as id,CONCAT(M.kode,' - ',M.deskripsi) as nama
			from mkontrol_icd D
			INNER JOIN icd_10 M ON M.id=D.icd_id
			WHERE D.mkontrol_id='$id' AND D.jenis_id='1'";
        $query=$this->db->query($q);
        $result= $query->row();
        return $result;
    }

    public function get_sebab($id)
    {
        $search=$this->input->post('search');
        $q="SELECT D.icd_id as id,CONCAT(M.kode,' - ',M.deskripsi) as nama
			from msebab_luar_icd D
			INNER JOIN icd_10 M ON M.id=D.icd_id
			WHERE D.msebab_luar_id='$id' AND D.jenis_id='1'";
        $query=$this->db->query($q);
        $result= $query->row();
        return $result;
    }
}
