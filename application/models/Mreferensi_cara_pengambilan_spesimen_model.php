<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mreferensi_cara_pengambilan_spesimen_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('merm_referensi_cara_pengambilan_spesimen');
        return $query->row();
    }

    public function saveData()
    {
        $this->noid = $_POST['noid'];
        $this->nama = $_POST['nama'];
        $this->status_default = $_POST['status_default'];
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('merm_referensi_cara_pengambilan_spesimen', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->noid = $_POST['noid'];
        $this->nama = $_POST['nama'];
        $this->status_default = $_POST['status_default'];
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('merm_referensi_cara_pengambilan_spesimen', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('merm_referensi_cara_pengambilan_spesimen', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function load_all()
    {
        $query = $this->db->get('merm_referensi_cara_pengambilan_spesimen');
        return $query->result();
    }
}
