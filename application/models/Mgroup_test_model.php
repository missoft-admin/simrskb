<?php

declare(strict_types=1);

class Mgroup_test_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $this->db->where('status', '1');
        $query = $this->db->get('merm_group_test');

        return $query->row();
    }

    public function getUser()
    {
        $this->db->select('id, name AS nama');
        $this->db->where('status', '1');
        $query = $this->db->get('musers');
        return $query->result();
    }

    public function getTarifLab()
    {
        $query = $this->db->query('SELECT
        	mtarif_laboratorium.id,
        	mtarif_laboratorium.nama,
        	mtarif_laboratorium.level
        FROM
        	mtarif_laboratorium
        	LEFT JOIN merm_group_test_pemeriksaan ON merm_group_test_pemeriksaan.idtariflaboratorium = mtarif_laboratorium.id
        	LEFT JOIN merm_group_test ON merm_group_test.id = merm_group_test_pemeriksaan.idgroup_test
        WHERE
        	mtarif_laboratorium.idkelompok = 0
        	AND mtarif_laboratorium.STATUS = 1
        	AND (merm_group_test.STATUS = 0 OR merm_group_test.STATUS IS NULL)
        GROUP BY
        	mtarif_laboratorium.id');

        return $query->result();
    }

    public function getListPemeriksaan($id)
    {
        $this->db->select('mtarif_laboratorium.id, mtarif_laboratorium.nama');
        $this->db->join('mtarif_laboratorium', 'mtarif_laboratorium.id = merm_group_test_pemeriksaan.idtariflaboratorium');
        $this->db->where('merm_group_test_pemeriksaan.idgroup_test', $id);
        $this->db->where('merm_group_test_pemeriksaan.status', 1);
        $query = $this->db->get('merm_group_test_pemeriksaan');

        return $query->result();
    }

    public function getUserSelected($id)
    {
        $this->db->select('musers.id');
        $this->db->join('musers', 'musers.id = merm_group_test_user_akses.iduser');
        $this->db->where('merm_group_test_user_akses.idgroup_test', $id);
        $query = $this->db->get('merm_group_test_user_akses');

        $result = $query->result();

        $array = array();
		foreach ($result as $row){
			$array[] = $row->id;
		}

		return $array;
    }

    public function saveData()
    {
        $this->nama = $_POST['nama'];
        $this->status = 1;
        $this->created_by = $this->session->userdata('user_id');
        $this->created_date = date('Y-m-d H:i:s');

        if ($this->db->insert('merm_group_test', $this)) {
            $idgroup_test = $this->db->insert_id();

            $pemeriksaan = json_decode($_POST['detail_value']);
            foreach ($pemeriksaan as $row) {
                $detail = [];
                $detail['idgroup_test'] = $idgroup_test;
                $detail['idtariflaboratorium'] = $row[1];
                $detail['status'] = 1;

                $this->db->insert('merm_group_test_pemeriksaan', $detail);
            }

            if (isset($_POST['iduser'])) {
                $users = $_POST['iduser'];
                foreach ($users as $row) {
                    $detail = [];
                    $detail['idgroup_test'] = $idgroup_test;
                    $detail['iduser'] = $row;
                    $detail['status'] = 1;

                    $this->db->insert('merm_group_test_user_akses', $detail);
                }
            }
        }

        return true;
    }

    public function updateData()
    {
        $this->nama = $_POST['nama'];
        $this->status = 1;
        $this->edited_by = $this->session->userdata('user_id');
        $this->edited_date = date('Y-m-d H:i:s');

        if ($this->db->update('merm_group_test', $this, ['id' => $_POST['id']])) {
            $idgroup_test = $_POST['id'];

            $this->db->where('idgroup_test', $idgroup_test);
            if ($this->db->delete('merm_group_test_pemeriksaan')) {
                $pemeriksaan = json_decode($_POST['detail_value']);
                foreach ($pemeriksaan as $row) {
                    $detail = [];
                    $detail['idgroup_test'] = $idgroup_test;
                    $detail['idtariflaboratorium'] = $row[1];
                    $detail['status'] = 1;

                    $this->db->insert('merm_group_test_pemeriksaan', $detail);
                }
            }

            $this->db->where('idgroup_test', $idgroup_test);
            if ($this->db->delete('merm_group_test_user_akses')) {
                $users = $_POST['iduser'];
                foreach ($users as $row) {
                    $detail = [];
                    $detail['idgroup_test'] = $idgroup_test;
                    $detail['iduser'] = $row;
                    $detail['status'] = 1;

                    $this->db->insert('merm_group_test_user_akses', $detail);
                }
            }

            return true;
        }

        $this->error_message = 'Penyimpanan Gagal';
        return false;
    }

    public function softDelete($id)
    {
        $this->status = 0;
        $this->deleted_by = $this->session->userdata('user_id');
        $this->deleted_date = date('Y-m-d H:i:s');

        if ($this->db->update('merm_group_test', $this, ['id' => $id])) {
            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;
    }
}
