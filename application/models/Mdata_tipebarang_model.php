<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mdata_tipebarang_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function load_by_jenis($val){
        $this->db->from("mdata_tipebarang");
        $this->db->where("jenis_tipe",$val);
        return $this->db->get()->result();
    }
    
    public function load_by_id($val){
        $this->db->from("mdata_tipebarang");
        $this->db->where("id",$val);
        return $this->db->get()->row();
    }
}