<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mrumahsakit_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getSpecified($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('mrumahsakit');
		return $query->row();
	}

	public function saveData()
	{
		$this->idtipe = $_POST['idtipe'];
		$this->nama = $_POST['nama'];
		$this->idkategori = $_POST['idkategori'];
		$this->jenis_berekanan = $_POST['jenis_berekanan'];

		if ($_POST['jenis_berekanan'] == '1') {
			$this->persentase = $_POST['persentase'];
		} else {
			$this->ratetarif = $_POST['ratetarif'];
		}

		$this->berdasarkan = $_POST['berdasarkan'];
		$this->tentukantarif = $_POST['tentukantarif'];

		if ($_POST['tentukantarif'] == '1') {
			$this->alkes_poli = $_POST['alkes_poli'];
			$this->obat_poli = $_POST['obat_poli'];
			$this->alkes_farmasi = $_POST['alkes_farmasi'];
			$this->obat_farmasi = $_POST['obat_farmasi'];
		}

		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');

		if ($this->db->insert('mrumahsakit', $this)) {

			$idrumahsakit = $this->db->insert_id();
			$this->db->set('idrumahsakit', $idrumahsakit);
			$this->db->where('idrumahsakit', $_POST['idtemp']);
			$this->db->update('mrumahsakit_tarif');

			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function updateData()
	{
		$this->idtipe = $_POST['idtipe'];
		$this->nama = $_POST['nama'];
		$this->idkategori = $_POST['idkategori'];
		$this->jenis_berekanan = $_POST['jenis_berekanan'];

		if ($_POST['jenis_berekanan'] == '1') {
			$this->persentase = RemoveComma($_POST['persentase']);
			$this->ratetarif = '0';
		} else {
			$this->persentase = '0';
			$this->ratetarif = RemoveComma($_POST['ratetarif']);
		}

		$this->berdasarkan = $_POST['berdasarkan'];
		$this->tentukantarif = $_POST['tentukantarif'];

		if ($_POST['tentukantarif'] == '1') {
			$this->alkes_poli = $_POST['alkes_poli'];
			$this->obat_poli = $_POST['obat_poli'];
			$this->alkes_farmasi = $_POST['alkes_farmasi'];
			$this->obat_farmasi = $_POST['obat_farmasi'];
		} else {
			$this->alkes_poli = '';
			$this->obat_poli = '';
			$this->alkes_farmasi = '';
			$this->obat_farmasi = '';
		}

		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');

		if ($this->db->update('mrumahsakit', $this, ['id' => $_POST['id']])) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function softDelete($id)
	{
		$this->status = 0;
		$this->deleted_by = $this->session->userdata('user_id');
		$this->deleted_date = date('Y-m-d H:i:s');

		if ($this->db->update('mrumahsakit', $this, ['id' => $id])) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}
}
