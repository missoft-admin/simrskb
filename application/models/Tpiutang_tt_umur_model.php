<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tpiutang_tt_umur_model extends CI_Model
{
  
   public function list_kelompok_pasien(){
	   $q="SELECT id,nama from mpasien_kelompok 
				WHERE mpasien_kelompok.`status`='1' AND id !='5'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function get_nama_asuransi($idkelompokpasien,$idrekanan){
	   if ($idkelompokpasien=='1'){
		   $q="SELECT M.nama FROM `mrekanan` M WHERE M.id='$idrekanan'";
		   
	   }else{
			$q="SELECT M.nama FROM `mpasien_kelompok` M WHERE M.id='$idkelompokpasien'";
	   
	   }
	  
		$query=$this->db->query($q);
		return $query->row('nama');
   }
   public function list_rekanan(){
	   $q="SELECT id,nama from mrekanan 
				WHERE mrekanan.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   function get_total_piutang($idtipe,$bulan){
	   $where1='';
			$where1 .="AND H.idtipe='$idtipe'";			
		
		if ($bulan ==1){
			$where1 .=" AND (ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30)<=".$bulan.")";
		
		}elseif($bulan<12){
			$where1 .=" AND (ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30)=".$bulan.")";
			
		}elseif($bulan==12){
			$where1 .="AND (ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30)=".$bulan.")";
		}elseif($bulan==13){
			$where1 .="AND ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) >12 AND ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) <24";
		}elseif($bulan==24){
			$where1 .="AND (ceil(DATEDIFF(CURRENT_DATE, H.tanggal_jatuh_tempo) / 30) > 24)";
		}
		$q="SELECT SUM(H.nominal_tagihan-H.nominal_bayar-H.nominal_dihapus) as sisa from tpiutang_tt_verifikasi H WHERE H.st_lunas='0' AND H.status='1' ".$where1;
		// print_r($q);exit();
		return $this->db->query($q)->row('sisa');
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
