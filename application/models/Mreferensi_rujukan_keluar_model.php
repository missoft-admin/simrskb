<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mreferensi_rujukan_keluar_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('merm_referensi_rujukan_keluar');
        return $query->row();
    }

    public function getReferensiJenisLayanan($id)
    {
        $this->db->select('jenis_layanan');
        $this->db->where('referensi_id', $id);
        $query = $this->db->get('merm_referensi_rujukan_keluar_layanan');
        $result_array = $query->result_array();

        return array_column($result_array, 'jenis_layanan');
    }

    public function saveData()
    {
        $this->nama_tujuan = $_POST['nama_tujuan'];
        $this->nama_lengkap = $_POST['nama_lengkap'];
        $this->alamat = $_POST['alamat'];
        $this->nomor_telepon = $_POST['nomor_telepon'];
        $this->email = $_POST['email'];
        $this->nomor_perjanjian = $_POST['nomor_perjanjian'];
        $this->tanggal_perjanjian_dari = YMDFormat($_POST['tanggal_perjanjian_dari']);
        $this->tanggal_perjanjian_sampai = YMDFormat($_POST['tanggal_perjanjian_sampai']);
        $this->pic = $_POST['pic'];
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('merm_referensi_rujukan_keluar', $this)) {
            $referensi_id = $this->db->insert_id();

            $this->db->where('referensi_id', $referensi_id);
            $this->db->delete('merm_referensi_rujukan_keluar_layanan');

            foreach ($_POST['jenis_layanan'] as $id) {
                $data['referensi_id'] = $referensi_id;
                $data['jenis_layanan'] = $id;
                $this->db->insert('merm_referensi_rujukan_keluar_layanan', $data);
            }
            
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama_tujuan = $_POST['nama_tujuan'];
        $this->nama_lengkap = $_POST['nama_lengkap'];
        $this->alamat = $_POST['alamat'];
        $this->nomor_telepon = $_POST['nomor_telepon'];
        $this->email = $_POST['email'];
        $this->nomor_perjanjian = $_POST['nomor_perjanjian'];
        $this->tanggal_perjanjian_dari = YMDFormat($_POST['tanggal_perjanjian_dari']);
        $this->tanggal_perjanjian_sampai = YMDFormat($_POST['tanggal_perjanjian_sampai']);
        $this->pic = $_POST['pic'];
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('merm_referensi_rujukan_keluar', $this, array('id' => $_POST['id']))) {
            $referensi_id = $_POST['id'];

            $this->db->where('referensi_id', $referensi_id);
            $this->db->delete('merm_referensi_rujukan_keluar_layanan');

            foreach ($_POST['jenis_layanan'] as $id) {
                $data['referensi_id'] = $referensi_id;
                $data['jenis_layanan'] = $id;
                $this->db->insert('merm_referensi_rujukan_keluar_layanan', $data);
            }
            
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('merm_referensi_rujukan_keluar', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function load_all()
    {
        $query = $this->db->get('merm_referensi_rujukan_keluar');
        return $query->result();
    }
}
