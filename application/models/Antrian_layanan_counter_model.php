<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Antrian_layanan_counter_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function list_layanan($counter_id=''){
		$q="SELECT H.id,CONCAT(H.kode,' - ',H.nama_pelayanan) as pelayanan 
			,CASE WHEN M.pelayanan_id IS NOT NULL THEN 'selected' ELSE '' END as pilih
			FROM antrian_pelayanan H
			LEFT JOIN antrian_pelayanan_counter_pelayanan M ON M.pelayanan_id=H.id AND M.counter_id='$counter_id'
			WHERE H.`status`='1' ORDER BY H.urutan";
		return $this->db->query($q)->result();
	}
	public function list_user($counter_id=''){
		$q="SELECT H.id,H.`name` as nama_user
			,CASE WHEN M.userid IS NOT NULL THEN 'selected' ELSE '' END as pilih
			FROM musers H
			LEFT JOIN antrian_pelayanan_counter_user M ON M.userid=H.id AND M.counter_id='$counter_id'
			WHERE H.`status`='1' ORDER BY H.id";
		return $this->db->query($q)->result();
	}
	
	public function getSpecified($id)
    {
        $q="SELECT 
			*
			from antrian_pelayanan_counter M
					
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	
    public function saveData()
    {
		// print_r($this->input->post());exit;
        $this->nama_counter 	= $_POST['nama_counter'];		
        $this->upload_sound(false);
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('antrian_pelayanan_counter', $this)) {
			$counter_id=$this->db->insert_id();
			$pelayanan_id=$this->input->post('pelayanan_id');
			$userid=$this->input->post('userid');
			
			foreach($pelayanan_id as $index=>$val){
				$data['counter_id']=$counter_id;
				$data['pelayanan_id']=$val;
				
				$this->db->insert('antrian_pelayanan_counter_pelayanan',$data);
			}
			foreach($userid as $index=>$val){
				$data_user['counter_id']=$counter_id;
				$data_user['userid']=$val;
				$this->db->insert('antrian_pelayanan_counter_user',$data_user);
			}
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
		$counter_id=$_POST['id'];
        $this->nama_counter 	= $_POST['nama_counter'];		
		$this->upload_sound(true);
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('antrian_pelayanan_counter', $this, array('id' => $_POST['id']))) {
			$pelayanan_id=$this->input->post('pelayanan_id');
			$userid=$this->input->post('userid');
			$this->db->where('counter_id',$counter_id);
			$this->db->delete('antrian_pelayanan_counter_pelayanan');
			foreach($pelayanan_id as $index=>$val){
				$data['counter_id']=$counter_id;
				$data['pelayanan_id']=$val;
				
				$this->db->insert('antrian_pelayanan_counter_pelayanan',$data);
			}
			$this->db->where('counter_id',$counter_id);
			$this->db->delete('antrian_pelayanan_counter_user');
			foreach($userid as $index=>$val){
				$data_user['counter_id']=$counter_id;
				$data_user['userid']=$val;
				$this->db->insert('antrian_pelayanan_counter_user',$data_user);
			}
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function upload_sound($update = false)
    {
        if (!file_exists('assets/upload/sound_antrian')) {
            mkdir('assets/upload/sound_antrian', 0755, true);
        }

        if (isset($_FILES['file_counter'])) {
            if ($_FILES['file_counter']['name'] != '') {
                $config['upload_path'] = './assets/upload/sound_antrian/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'wav|mp3';
				// $config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('file_counter')) {
                    $image_upload = $this->upload->data();
                    $this->file_counter = $image_upload['file_name'];

                    if ($update == true) {
                        $this->remove_sound($this->input->post('id'));
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
			// print_r('TIDAK ADA');exit;
                return true;
            }
					// print_r($this->foto);exit;
        } else {
			// print_r('TIDAK ADA');exit;
            return true;
        }
		
    }
	public function remove_sound($id)
    {
		$q="select file_counter From antrian_pelayanan_counter H WHERE H.id='$id'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/sound_antrian/'.$row->file_counter) && $row->file_counter !='') {
            unlink('./assets/upload/sound_antrian/'.$row->file_counter);
        }
    }
    public function softDelete($id)
    {
        $this->status = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('antrian_pelayanan_counter', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('antrian_pelayanan_counter', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
