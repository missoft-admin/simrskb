<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tsetoran_rj_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
	public function getNoTransaksi()
    {
        $this->db->like('notransaksi', 'SRJ'.date('Y').date('m'), 'after');
        $this->db->from('tsetoran_rj');
        $query = $this->db->count_all_results();

        if ($query > 0) {
            $autono = $query + 1;
            $autono = "TSK".date('Y').date('m').str_pad($autono, 4, '0', STR_PAD_LEFT);
        } else {
            $autono = "TSK".date('Y').date('m')."0001";
        }

        return $autono;
    }
    public function getSpecified($id) {
      $q="
		SELECT MC.`name` as user_buat,MU.`name` as user_update,H.* FROM tsetoran_rj H
LEFT JOIN musers MC ON MC.id=H.user_created
LEFT JOIN musers MU ON MU.id=H.edited_by
WHERE H.id='$id'
	  ";
        return $this->db->query($q)->row_array();
    }
	public function list_pembayaran($id) {
        // $this->db->where('id', $id);
        // $query = $this->db->get('tsetoran_rj_detail');
		$q="SELECT JK.nama as jenis_kas,SK.nama as sumber_kas,D.* FROM tsetoran_rj_bayar D
		LEFT JOIN mjenis_kas JK ON JK.id=D.jenis_kas_id
		LEFT JOIN msumber_kas SK ON SK.id=D.sumber_kas_id
		WHERE D.idtransaksi='$id' AND D.status='1'";
        return $this->db->query($q)->result();
    }
	
	
    public function saveData() {
        $this->tanggal_trx        = YMDFormat($_POST['tanggal_trx']);
        $this->tanggal_setoran        = YMDFormat($_POST['tanggal_setoran']);
		$this->notransaksi    = $this->getNoTransaksi();
       
        $this->cash_rajal         = RemoveComma($_POST['cash_rajal']);
        $this->cash_ranap         = RemoveComma($_POST['cash_ranap']);
        $this->cash_pendapatan         = RemoveComma($_POST['cash_pendapatan']);
        $this->cash_retur         = RemoveComma($_POST['cash_retur']);
        $this->cash_piutang         = RemoveComma($_POST['cash_piutang']);
        $this->nominal         = RemoveComma($_POST['nominal']);
        $this->st_bayar         = 1;
        // $this->statusjurnal   = 0;
        $this->created_at  = date("Y-m-d H:i:s");
        $this->user_created  = $this->session->userdata('user_id');

        if ($this->db->insert('tsetoran_rj', $this)) {
            $idtransaksi = $this->db->insert_id();
			$xjenis_kas_id= $_POST['xjenis_kas_id'];
			$xsumber_kas_id= $_POST['xsumber_kas_id'];
			$xidmetode= $_POST['xidmetode'];
			$ket= $_POST['ket'];
			$xnominal_bayar= $_POST['xnominal_bayar'];
            foreach ($xjenis_kas_id as $index => $val){
				$data_detail=array(
					'idtransaksi'=>$idtransaksi,
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'idmetode'=>$xidmetode[$index],
					'ket_pembayaran'=>$ket[$index],
					// 'tanggal_pencairan'=>YMDFormat($tanggal_pencairan),
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->insert('tsetoran_rj_detail', $data_detail);
			}

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
		
    }
	
    public function updateData() {
        // $this->tanggal_trx        = YMDFormat($_POST['tanggal_trx']);
        $this->tanggal_setoran        = YMDFormat($_POST['tanggal_setoran']);
		$this->notransaksi    = $this->getNoTransaksi();
        $jenis_id=($_POST['jenis_id']);
        $this->jenis_id         = ($_POST['jenis_id']);
        $this->keterangan         = ($_POST['keterangan']);
		$this->posisi_akun         = ($_POST['jenis_id']=='1'?'K':'D');
        $this->nominal         = RemoveComma($_POST['nominal']);
		$this->st_bayar         = 1;
        if ($jenis_id=='1'){//Pendapatan
			$q="SELECT H.idakun,A.noakun FROM `mjenis_pendapatan` H 
			LEFT JOIN makun_nomor A ON A.id=H.idakun
			WHERE H.id='1'";
		}else{
			$q="SELECT H.idakun,A.noakun FROM `mjenis_loss` H 
			LEFT JOIN makun_nomor A ON A.id=H.idakun
			WHERE H.id='1'";
		}
		$data_akun=$this->db->query($q)->row();
		$this->idakun=$data_akun->idakun;
		$this->noakun=$data_akun->noakun;
		$idtransaksi=$_POST['idtransaksi'];
		// print_r($idtransaksi);exit;
		$this->db->where('id',$_POST['idtransaksi']);
        if ($this->db->update('tsetoran_rj', $this)) {		
			$xjenis_kas_id= $_POST['xjenis_kas_id'];
			$xsumber_kas_id= $_POST['xsumber_kas_id'];
			$xidmetode= $_POST['xidmetode'];
			$ket= $_POST['ket'];
			$xnominal_bayar= $_POST['xnominal_bayar'];
			$xiddet= $_POST['xiddet'];
			$xstatus= $_POST['xstatus'];
		   foreach ($xjenis_kas_id as $index => $val){
				if ($xiddet[$index]!=''){
					$data_detail=array(
						'jenis_kas_id'=>$xjenis_kas_id[$index],
						'sumber_kas_id'=>$xsumber_kas_id[$index],
						'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
						'idmetode'=>$xidmetode[$index],
						'ket_pembayaran'=>$ket[$index],
						'status'=>$xstatus[$index],
						'edited_by'=>$this->session->userdata('user_id'),
						'edited_date'=>date('Y-m-d H:i:s'),
					);
					$this->db->where('id',$xiddet[$index]);
					$this->db->update('tsetoran_rj_bayar', $data_detail);
				}else{
					$data_detail=array(
						'idtransaksi'=>$idtransaksi,
						'jenis_kas_id'=>$xjenis_kas_id[$index],
						'sumber_kas_id'=>$xsumber_kas_id[$index],
						'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
						'idmetode'=>$xidmetode[$index],
						'ket_pembayaran'=>$ket[$index],
						// 'tanggal_pencairan'=>YMDFormat($tanggal_pencairan),
						'created_by'=>$this->session->userdata('user_id'),
						'created_date'=>date('Y-m-d H:i:s'),
					);
					// print_r($data_detail);exit;
					$this->db->insert('tsetoran_rj_bayar', $data_detail);
				}				
			}
		}
		return true;
	}
	public function insert_jurnal_setoran($id){
		$q="INSERT INTO tvalidasi_pendapatan_rajal_other (
				idvalidasi,idtransaksi,hasil_pemeriksaan,nominal_bayar,idakun_debet,idakun_kredit
			)
			SELECT T.id as idvalidasi,H.id as idtransaksi,H.hasil_pemeriksaan,H.nominal as nominal_bayar,H.idakun as idakun_debet,H.idakun_kredit as idakun_kredit
			FROM tsetoran_rj H
			LEFT JOIN tvalidasi_pendapatan_rajal T ON DATE(T.tanggal_transaksi)=H.tanggal_trx AND T.st_posting='0' AND T.tipe_trx='1'
			WHERE H.id='$id' AND H.hasil_pemeriksaan!='0'";
		$this->db->query($q);
		
		return true;
	}
}
