<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tmonitoring_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
	public function getNoTransaksi()
    {
        $this->db->like('notransaksi', 'TSK'.date('Y').date('m'), 'after');
        $this->db->from('tsetoran_kas');
        $query = $this->db->count_all_results();

        if ($query > 0) {
            $autono = $query + 1;
            $autono = "TSK".date('Y').date('m').str_pad($autono, 4, '0', STR_PAD_LEFT);
        } else {
            $autono = "TSK".date('Y').date('m')."0001";
        }

        return $autono;
    }
    public function getSpecified($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('tsetoran_kas');
        return $query->row_array();
    }
	public function list_user() {
        $q="SELECT U.id,U.`name` from musers U 
WHERE U.`status`='1'";
        return $this->db->query($q)->result();
    }
	
}
