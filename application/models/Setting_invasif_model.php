<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_invasif_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_assesmen_setting(){
		$q="SELECT * FROM setting_invasif H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_label(){
		$q="SELECT * FROM setting_invasif_keterangan";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_user(){
		$q="SELECT * FROM setting_invasif_user H";
		return $this->db->query($q)->row_array();
	}

	function save_assesmen(){
		$id =1;
		$this->judul_header = $this->input->post('judul_header');
		$this->judul_footer = $this->input->post('judul_footer');
		$this->judul_header_eng = $this->input->post('judul_header_eng');
		$this->footer_eng = $this->input->post('footer_eng');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->lama_edit = $this->input->post('lama_edit');
		$this->orang_edit = $this->input->post('orang_edit');
		$this->st_hapus_catatan = $this->input->post('st_hapus_catatan');
		$this->lama_hapus = $this->input->post('lama_hapus');
		$this->orang_hapus = $this->input->post('orang_hapus');
		$this->st_duplikasi_catatan = $this->input->post('st_duplikasi_catatan');
		$this->lama_duplikasi = $this->input->post('lama_duplikasi');
		$this->orang_duplikasi = $this->input->post('orang_duplikasi');
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_invasif', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_label(){
		$id =1;
		// print_r($this->input->post());exit;
		$this->identitas_pasien_ina = $this->input->post('identitas_pasien_ina');
		$this->identitas_pasien_eng = $this->input->post('identitas_pasien_eng');
		$this->no_reg_ina = $this->input->post('no_reg_ina');
		$this->no_reg_eng = $this->input->post('no_reg_eng');
		$this->norek_ina = $this->input->post('norek_ina');
		$this->norek_eng = $this->input->post('norek_eng');
		$this->nama_pasien_ina = $this->input->post('nama_pasien_ina');
		$this->nama_pasien_eng = $this->input->post('nama_pasien_eng');
		$this->tanggal_lahir_ina = $this->input->post('tanggal_lahir_ina');
		$this->tanggal_lahir_eng = $this->input->post('tanggal_lahir_eng');
		$this->umur_ina = $this->input->post('umur_ina');
		$this->umur_eng = $this->input->post('umur_eng');
		$this->jenis_kelamin_ina = $this->input->post('jenis_kelamin_ina');
		$this->jenis_kelamin_eng = $this->input->post('jenis_kelamin_eng');
		$this->tanggal_jam_ina = $this->input->post('tanggal_jam_ina');
		$this->tanggal_jam_eng = $this->input->post('tanggal_jam_eng');
		$this->nama_tindakan_ina = $this->input->post('nama_tindakan_ina');
		$this->nama_tindakan_eng = $this->input->post('nama_tindakan_eng');
		$this->ket_ina = $this->input->post('ket_ina');
		$this->ket_eng = $this->input->post('ket_eng');
		$this->ttd_petugas_ina = $this->input->post('ttd_petugas_ina');
		$this->ttd_petugas_eng = $this->input->post('ttd_petugas_eng');
		$this->daftar_persetujuan_ina = $this->input->post('daftar_persetujuan_ina');
		$this->daftar_persetujuan_eng = $this->input->post('daftar_persetujuan_eng');
		$this->ttd_pasien_ina = $this->input->post('ttd_pasien_ina');
		$this->ttd_pasien_eng = $this->input->post('ttd_pasien_eng');
		$this->persetujuan_ina = $this->input->post('persetujuan_ina');
		$this->persetujuan_eng = $this->input->post('persetujuan_eng');
		// $this->db->where('id', $id);
			
		if ($this->db->update('setting_invasif_keterangan', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_paraf(){
		$this->paragraf_1_ina = $this->input->post('paragraf_1_ina');
		$this->paragraf_1_eng = $this->input->post('paragraf_1_eng');
		$this->yang_ttd_ina = $this->input->post('yang_ttd_ina');
		$this->yang_ttd_eng = $this->input->post('yang_ttd_eng');
		$this->nama_ina = $this->input->post('nama_ina');
		$this->nama_eng = $this->input->post('nama_eng');
		$this->memberikan_ina = $this->input->post('memberikan_ina');
		$this->memberikan_eng = $this->input->post('memberikan_eng');
		$this->untuk_ina = $this->input->post('untuk_ina');
		$this->untuk_eng = $this->input->post('untuk_eng');
		$this->terhadap_ina = $this->input->post('terhadap_ina');
		$this->terhadap_eng = $this->input->post('terhadap_eng');
		$this->sign_ina = $this->input->post('sign_ina');
		$this->sign_eng = $this->input->post('sign_eng');
		$this->ttd_petugas_st_auto = $this->input->post('ttd_petugas_st_auto');

			
		if ($this->db->update('setting_invasif_keterangan', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_defaultsave_default(){
		$id =1;
		$this->st_spesifik_invasif = $this->input->post('st_spesifik_invasif');
		if (!empty($this->input->post('st_kunci_default_invasif'))){
		$this->st_kunci_default_invasif =($this->input->post('st_spesifik_invasif')=='0'?'0':$this->input->post('st_kunci_default_invasif'));
			
		}else{
			$this->st_kunci_default_invasif =0;
		}
		
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('msetting_invasif_default', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


