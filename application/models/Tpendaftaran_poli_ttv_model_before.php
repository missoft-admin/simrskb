<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpendaftaran_poli_ttv_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function get_data_header($pendaftaran_id){
		$q="SELECT MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
			,MK.nama as nama_kelompok,MR.nama as nama_rekanan
			,H.*,H.title as title_nama,H.id as pendaftaran_id,COALESCE(TPA.risiko_jatuh,TPA2.risiko_jatuh) as var_header_risiko_jatuh
			,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header 
			FROM tpoliklinik_pendaftaran H
			INNER JOIN mdokter MD ON MD.id=H.iddokter
			INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
			INNER JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
			INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
			LEFT JOIN tpoliklinik_assesmen TPA ON TPA.assesmen_id=H.assesmen_id AND H.idtipe='1'
			LEFT JOIN tpoliklinik_assesmen_igd TPA2 ON TPA2.assesmen_id=H.assesmen_id AND H.idtipe='2'
			LEFT JOIN (
				SELECT * FROM `tpoliklinik_assesmen_igd_alergi`
				WHERE  status_assemen='2'
				UNION ALL
				SELECT * FROM `tpoliklinik_assesmen_alergi`
				WHERE status_assemen='2'
			) A ON A.idpasien=H.idpasien AND A.status_assemen='2'
			WHERE H.id='$pendaftaran_id'
			GROUP BY H.id";
		return $this->db->query($q)->row_array();
	}
	function get_default_warna_asmed(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT jenis_drawing,warna FROM tpoliklinik_asmed_default WHERE mppa_id='$login_ppa_id'";
		$data=$this->db->query($q)->row_array();
		if ($data){
			
		}else{
			$data=array(
				'warna'=>'#ff2525',
				'jenis_drawing'=>'bulat',
			);
		}
		return $data;
	}
	function get_lokalis($assesmen_id,$gambar_id,$versi_edit='#'){
		if ($versi_edit=='#'){
		$q="SELECT *FROM tpoliklinik_asmed_lokalis WHERE assesmen_id='$assesmen_id' AND gambar_id='$gambar_id'";
			
		}else{
			$q="
				SELECT *FROM(
					SELECT D.jml_edit as versi_edit,H.* 
					FROM tpoliklinik_asmed_lokalis H
					LEFT JOIN tpoliklinik_asmed D ON D.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'

					UNION ALL

					SELECT H.* 
					FROM tpoliklinik_asmed_his_lokalis H
					WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'
					) T 

					WHERE T.versi_edit='$versi_edit'
					ORDER BY T.`index` ASC
			";
		}
		// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	function list_ruang(){
		$q="SELECT * FROM `mtujuan` M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function list_ppa(){
		$q="SELECT * FROM `mppa` M WHERE M.`staktif`='1'";
		return $this->db->query($q)->result();
	}
	function list_poli(){
		$q="SELECT MP.id,MP.nama FROM `mppa_poli` H
			INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
			WHERE H.setting_lainnya='1'
			GROUP BY H.idpoliklinik";
		return $this->db->query($q)->result();
	}
		function list_dokter(){
		$q="SELECT *FROM mdokter M
WHERE M.`status`='1'
";
		return $this->db->query($q)->result();
	}
	function list_menu_kiri($menu_atas){
		$q="SELECT H.menu_kiri FROM tindakan_menu H WHERE H.menu_atas='$menu_atas'";
		return json_encode(array_column($this->db->query($q)->result_array(),'menu_kiri'));
	}
	function logic_akses_ttv($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_ttv,H.st_lihat as st_lihat_ttv,H.st_edit as st_edit_ttv,H.st_hapus as st_hapus_ttv,H.st_cetak as st_cetak_ttv
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_ttv_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_ttv_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_ttv_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat'=>'0',
				'st_input'=>'0',
				'st_edit'=>'0',
				'st_hapus'=>'0',
			);
		}
		return $hasil;
		
	}
	function logic_akses_assesmen($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_ttv,H.st_lihat as st_lihat_ttv,H.st_edit as st_edit_ttv,H.st_hapus as st_hapus_ttv,H.st_cetak as st_cetak_ttv
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_assesmen_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_assesmen_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_assesmen_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_ttv'=>'0',
				'st_input_ttv'=>'0',
				'st_edit_ttv'=>'0',
				'st_hapus_ttv'=>'0',
				'st_cetak_ttv'=>'0',
			);
		}
		return $hasil;
		
	}
	
	function list_template_assement(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_assesmen H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function list_template_asmed(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_asmed H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	function list_template_triage(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_triage H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function setting_ttv(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			FROM `setting_ttv` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function setting_assesmen(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			FROM `setting_assesmen` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	
	function get_data_ttv($pendaftaran_id){
		$q="SELECT id as ttv_id,pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv
			 FROM tpoliklinik_ttv WHERE pendaftaran_id='$pendaftaran_id' AND status_ttv!=0 ORDER BY id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$hasil=array(
					'ttv_id' => '',
					'pendaftaran_id' => '',
					'tanggal_input' => '',
					'tingkat_kesadaran' => '',
					'nadi' => '',
					'nafas' => '',
					'td_sistole' => '',
					'td_diastole' => '',
					'suhu' => '',
					'tinggi_badan' => '',
					'berat_badan' => '',
					'status_ttv' => '0',
				);
			}else{
				if ($hasil['status_ttv']>1){
					$hasil=array(
					'ttv_id' => '',
					'pendaftaran_id' => '',
					'tanggal_input' => '',
					'tingkat_kesadaran' => '',
					'nadi' => '',
					'nafas' => '',
					'td_sistole' => '',
					'td_diastole' => '',
					'suhu' => '',
					'tinggi_badan' => '',
					'berat_badan' => '',
					'status_ttv' => '0',
				);
				}
			}
			return $hasil;
	}
	function get_data_ttv_all($pendaftaran_id){
		$q="SELECT id as ttv_id,pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv
			 FROM tpoliklinik_ttv WHERE pendaftaran_id='$pendaftaran_id' AND status_ttv!=0 ORDER BY id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$hasil=array(
					'ttv_id' => '',
					'pendaftaran_id' => '',
					'tanggal_input' => '',
					'tingkat_kesadaran' => '',
					'nadi' => '',
					'nafas' => '',
					'td_sistole' => '',
					'td_diastole' => '',
					'suhu' => '',
					'tinggi_badan' => '',
					'berat_badan' => '',
					'status_ttv' => '0',
				);
			}
			return $hasil;
	}
	function get_data_ttv_assesmen($assesmen_id){
		$q="SELECT assesmen_id as assesmen_id,pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv
			 FROM tpoliklinik_assesmen_ttv WHERE assesmen_id='$assesmen_id' AND status_ttv!=0 ORDER BY id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$hasil=array(
					'ttv_id' => '',
					'pendaftaran_id' => '',
					'tanggal_input' => '',
					'tingkat_kesadaran' => '',
					'nadi' => '',
					'nafas' => '',
					'td_sistole' => '',
					'td_diastole' => '',
					'suhu' => '',
					'tinggi_badan' => '',
					'berat_badan' => '',
					'status_ttv' => '0',
				);
			}
			return $hasil;
	}
	function get_data_ttv_assesmen_trx($assesmen_id,$versi_edit){
		$q=" 
			 SELECT assesmen_id as assesmen_id,pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv
			 FROM tpoliklinik_assesmen_his_ttv WHERE assesmen_id='$assesmen_id' AND status_ttv!=0 AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT D.assesmen_id as assesmen_id,D.pendaftaran_id,D.tanggal_input,D.tingkat_kesadaran,D.nadi,D.nafas,D.td_sistole,D.td_diastole,D.suhu
,D.tinggi_badan,D.berat_badan,D.status_ttv
			 FROM tpoliklinik_assesmen_ttv D 
			 INNER JOIN tpoliklinik_assesmen H ON H.assesmen_id=D.assesmen_id
			 WHERE D.assesmen_id='$assesmen_id' AND status_ttv!=0 AND H.jml_edit='$versi_edit'
			 
			 ";
			 $hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$hasil=array(
					'ttv_id' => '',
					'pendaftaran_id' => '',
					'tanggal_input' => '',
					'tingkat_kesadaran' => '',
					'nadi' => '',
					'nafas' => '',
					'td_sistole' => '',
					'td_diastole' => '',
					'suhu' => '',
					'tinggi_badan' => '',
					'berat_badan' => '',
					'status_ttv' => '0',
				);
			}
			return $hasil;
	}
	
	function get_data_ttv_assesmen_trx_last($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tingkat_kesadaran as tingkat_kesadaran_asal,nadi as nadi_asal,nafas as nafas_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal
			,suhu as suhu_asal,tinggi_badan as tinggi_badan_asal,berat_badan as  berat_badan_asal,status_ttv as status_ttv_asal
			 FROM tpoliklinik_assesmen_his_ttv WHERE assesmen_id='$assesmen_id' AND status_ttv!=0 AND versi_edit='$versi_edit' ORDER BY id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$hasil=array(
					'ttv_id' => '',
					'pendaftaran_id' => '',
					'tanggal_input' => '',
					'tingkat_kesadaran_asal' => '',
					'nadi_asal' => '',
					'nafas_asal' => '',
					'td_sistole_asal' => '',
					'td_diastole_asal' => '',
					'suhu_asal' => '',
					'tinggi_badan_asal' => '',
					'berat_badan_asal' => '',
					'status_ttv_asal' => '0',
				);
			}
			return $hasil;
	}
	function get_data_assesmen($pendaftaran_id){
		$q="SELECT *
			 FROM tpoliklinik_assesmen WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_assesmen_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_assesmen WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_assesmen_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_assesmen_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_assesmen H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_assesmen_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT st_anamnesa as st_anamnesa_asal,keluhan_utama as keluhan_utama_asal,nama_anamnesa as nama_anamnesa_asal,hubungan_anamnesa as hubungan_anamnesa_asal,st_riwayat_penyakit as st_riwayat_penyakit_asal,riwayat_penyakit_lainnya as riwayat_penyakit_lainnya_asal,riwayat_alergi as riwayat_alergi_asal,riwayat_pengobatan as riwayat_pengobatan_asal,hubungan_anggota_keluarga as hubungan_anggota_keluarga_asal,st_psikologis as st_psikologis_asal,st_sosial_ekonomi as st_sosial_ekonomi_asal,st_spiritual as st_spiritual_asal,pendidikan as pendidikan_asal,pekerjaan as pekerjaan_asal,st_nafsu_makan as st_nafsu_makan_asal,st_turun_bb as st_turun_bb_asal,st_mual as st_mual_asal,st_muntah as st_muntah_asal,st_fungsional as st_fungsional_asal,st_alat_bantu as st_alat_bantu_asal,st_cacat as st_cacat_asal,cacat_lain as cacat_lain_asal,header_risiko_jatuh as header_risiko_jatuh_asal,footer_risiko_jatuh as footer_risiko_jatuh_asal,skor_pengkajian as skor_pengkajian_asal,risiko_jatuh as risiko_jatuh_asal,nama_tindakan as nama_tindakan_asal,st_tindakan as st_tindakan_asal,st_tindakan_action as st_tindakan_action_asal,st_edukasi as st_edukasi_asal,st_menerima_info as st_menerima_info_asal,st_hambatan_edukasi as st_hambatan_edukasi_asal,st_penerjemaah as st_penerjemaah_asal,penerjemaah as penerjemaah_asal,catatan_edukasi as catatan_edukasi_asal 
				FROM tpoliklinik_assesmen_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function list_edukasi($assesmen_id){
		$q="SELECT M.id,M.judul,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM `medukasi` M
			LEFT JOIN tpoliklinik_assesmen_edukasi H ON H.assesmen_id='$assesmen_id' AND H.medukasi_id=M.id
			WHERE M.staktif='1'";
		return $this->db->query($q)->result();
	}
	
	function list_edukasi_asmed($assesmen_id){
		$q="SELECT M.id,M.judul,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM `medukasi` M
			LEFT JOIN tpoliklinik_asmed_edukasi H ON H.assesmen_id='$assesmen_id' AND H.medukasi_id=M.id
			WHERE M.staktif='1'";
		return $this->db->query($q)->result();
	}
	function list_edukasi_his($assesmen_id,$versi_edit){
		$q="SELECT H.medukasi_id as id,M.judul,'selected' as pilih FROM (
			SELECT H.medukasi_id,H.versi_edit FROM tpoliklinik_assesmen_his_edukasi H WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT D.medukasi_id,H.jml_edit as versi_edit FROM tpoliklinik_assesmen_edukasi D 
			LEFT JOIN tpoliklinik_assesmen H ON H.assesmen_id=D.assesmen_id
			WHERE D.assesmen_id='$assesmen_id'
			) H 
			INNER JOIN medukasi M ON M.id=H.medukasi_id
			WHERE H.versi_edit='$versi_edit'";
		// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	function list_edukasi_asmed_his($assesmen_id,$versi_edit){
		$q="SELECT H.medukasi_id as id,M.judul,'selected' as pilih FROM (
			SELECT H.medukasi_id,H.versi_edit FROM tpoliklinik_asmed_his_edukasi H WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT D.medukasi_id,H.jml_edit as versi_edit FROM tpoliklinik_asmed_edukasi D 
			LEFT JOIN tpoliklinik_asmed H ON H.assesmen_id=D.assesmen_id
			WHERE D.assesmen_id='$assesmen_id'
			) H 
			INNER JOIN medukasi M ON M.id=H.medukasi_id
			WHERE H.versi_edit='$versi_edit'";
		// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	function perubahan_edukasi($assesmen_id,$versi_edit){
		if ($versi_edit!=0){
			$versi_edit_last=$versi_edit -1;
		}
		$hasil_1='';
		$hasil_2='';
		if ($versi_edit!=0){
			
			$q="SELECT H.versi_edit, GROUP_CONCAT(M.judul) as judul FROM (
				SELECT H.medukasi_id,H.versi_edit FROM tpoliklinik_assesmen_his_edukasi H WHERE H.assesmen_id='$assesmen_id'
				UNION ALL
				SELECT D.medukasi_id,H.jml_edit as versi_edit FROM tpoliklinik_assesmen_edukasi D 
				LEFT JOIN tpoliklinik_assesmen H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'
				) H 
				INNER JOIN medukasi M ON M.id=H.medukasi_id
				WHERE H.versi_edit='$versi_edit'
				GROUP BY H.versi_edit
			";
			$hasil_1=$this->db->query($q)->row('judul');
			$q="SELECT H.versi_edit, GROUP_CONCAT(M.judul) as judul FROM (
				SELECT H.medukasi_id,H.versi_edit FROM tpoliklinik_assesmen_his_edukasi H WHERE H.assesmen_id='$assesmen_id'
				UNION ALL
				SELECT D.medukasi_id,H.jml_edit as versi_edit FROM tpoliklinik_assesmen_edukasi D 
				LEFT JOIN tpoliklinik_assesmen H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'
				) H 
				INNER JOIN medukasi M ON M.id=H.medukasi_id
				WHERE H.versi_edit='$versi_edit_last'
				GROUP BY H.versi_edit
			";
			$hasil_2=$this->db->query($q)->row('judul');
			
		}
		$data_edukasi=array(
			'edukasi_current'=>$hasil_1,
			'edukasi_last'=>$hasil_2,
		);
		return $data_edukasi;
	}
	function list_riwayat_penyakit($assesmen_id){
		$q="SELECT M.nilai as id,M.ref as nama,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM merm_referensi M
			LEFT JOIN tpoliklinik_assesmen_riwayat_penyakit H ON H.assesmen_id='$assesmen_id' AND H.penyakit_id=M.nilai
			WHERE M.ref_head_id='25' AND M.nilai<>0";
		return $this->db->query($q)->result();
	}
	
	function list_riwayat_penyakit_asmed($assesmen_id,$jenis='1',$versi_edit='#'){
		if ($versi_edit=='#'){
			
		$nama_tabel='';
			if ($jenis=='1'){
				$nama_tabel='tpoliklinik_asmed_riwayat_penyakit';
			}
			if ($jenis=='2'){
				$nama_tabel='tpoliklinik_asmed_riwayat_penyakit_dahulu';
			}
			if ($jenis=='3'){
				$nama_tabel='tpoliklinik_asmed_riwayat_penyakit_keluarga';
			}
			$q="SELECT M.nilai as id,M.ref as nama,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM merm_referensi M
				LEFT JOIN ".$nama_tabel." H ON H.assesmen_id='$assesmen_id' AND H.penyakit_id=M.nilai
				WHERE M.ref_head_id='25' AND M.nilai<>0";
		}else{
			$q="
				SELECT M.nilai as id,H.jenis,M.ref as nama,CASE WHEN H.penyakit_id IS NOT NULL THEN 'selected' ELSE '' END as pilih 
				FROM merm_referensi M
				LEFT JOIN (
				SELECT 1 as jenis,H.jml_edit as versi_edit,D.penyakit_id FROM tpoliklinik_asmed H
				LEFT JOIN tpoliklinik_asmed_riwayat_penyakit D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id'

				UNION ALL
				SELECT 1 as jenis, H.versi_edit,H.penyakit_id FROM tpoliklinik_asmed_his_riwayat_penyakit H WHERE H.assesmen_id='$assesmen_id'

				UNION ALL

				SELECT 2 as jenis,H.jml_edit as versi_edit,D.penyakit_id FROM tpoliklinik_asmed H
				LEFT JOIN tpoliklinik_asmed_riwayat_penyakit_dahulu D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id'

				UNION ALL
				SELECT 2 as jenis, H.versi_edit,H.penyakit_id FROM tpoliklinik_asmed_his_riwayat_penyakit_dahulu H WHERE H.assesmen_id='$assesmen_id'

				UNION ALL

				SELECT 3 as jenis,H.jml_edit as versi_edit,D.penyakit_id FROM tpoliklinik_asmed H
				LEFT JOIN tpoliklinik_asmed_riwayat_penyakit_keluarga D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id'

				UNION ALL
				SELECT 3 as jenis, H.versi_edit,H.penyakit_id FROM tpoliklinik_asmed_his_riwayat_penyakit_keluarga H WHERE H.assesmen_id='$assesmen_id'
				) H ON H.penyakit_id=M.nilai
				WHERE M.ref_head_id='25' AND H.jenis='$jenis' AND H.versi_edit='$versi_edit'
			";
		}
		return $this->db->query($q)->result();
	}
	function get_header_ttv($pendaftaran_id){
		$q="SELECT 
				H.nadi as header_nadi
				,mnadi.warna as warna_nadi
				,H.nafas as  header_nafas,mnafas.warna as warna_nafas
				,H.td_sistole as header_td_sistole,mtd_sistole.warna as warna_sistole
				,H.td_diastole as header_td_diastole,mtd_diastole.warna as warna_diastole
				,H.suhu as header_suhu,msuhu.warna as warna_suhu
				,H.tinggi_badan as header_tinggi_badan
				,H.berat_badan as header_berat_badan
				,H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) as masa_tubuh,mberat.warna as warna_berat
				FROM tpoliklinik_ttv H
				LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
				LEFT JOIN mnafas ON 12 BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
				LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
				LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
				LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
				LEFT JOIN mberat ON H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
				
				WHERE pendaftaran_id='$pendaftaran_id' AND status_ttv > 1 
				ORDER BY H.id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$data_header=array(
					'header_nadi'=>'',
					'warna_nadi'=>'',
					'header_nafas'=>'',
					'warna_nafas'=>'',
					'header_td_sistole'=>'',
					'warna_sistole'=>'',
					'header_td_diastole'=>'',
					'warna_diastole'=>'',
					'header_suhu'=>'',
					'warna_suhu'=>'',
					'header_tinggi_badan'=>'',
					'warna_berat'=>'',
					'header_berat_badan'=>'',
				);
			}else{$data_header=$hasil;}
			return $data_header;
	}
	function get_header_ttv_igd($assesmen_id){
		$q="SELECT 
				H.nadi as header_nadi
				,mnadi.warna as warna_nadi
				,H.nafas as  header_nafas,mnafas.warna as warna_nafas
				,H.td_sistole as header_td_sistole,mtd_sistole.warna as warna_sistole
				,H.td_diastole as header_td_diastole,mtd_diastole.warna as warna_diastole
				,H.suhu as header_suhu,msuhu.warna as warna_suhu
				,H.tinggi_badan as header_tinggi_badan
				,H.berat_badan as header_berat_badan
				,H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) as masa_tubuh,mberat.warna as warna_berat
				FROM tpoliklinik_assesmen_igd H
				LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
				LEFT JOIN mnafas ON 12 BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
				LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
				LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
				LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
				LEFT JOIN mberat ON H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
				
				WHERE H.assesmen_id='$assesmen_id' ";
			 $hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$data_header=array(
					'header_nadi'=>'',
					'warna_nadi'=>'',
					'header_nafas'=>'',
					'warna_nafas'=>'',
					'header_td_sistole'=>'',
					'warna_sistole'=>'',
					'header_td_diastole'=>'',
					'warna_diastole'=>'',
					'header_suhu'=>'',
					'warna_suhu'=>'',
					'header_tinggi_badan'=>'',
					'warna_berat'=>'',
					'header_berat_badan'=>'',
				);
			}else{$data_header=$hasil;}
			return $data_header;
	}
	
	function logic_akses_assemen_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_assesmen,H.st_lihat as st_lihat_assesmen,H.st_edit as st_edit_assesmen,H.st_hapus as st_hapus_assesmen,H.st_cetak as st_cetak_assesmen
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_assesmen_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_assesmen_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_assesmen_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_assesmen'=>'0',
				'st_input_assesmen'=>'0',
				'st_edit_assesmen'=>'0',
				'st_hapus_assesmen'=>'0',
				'st_cetak_assesmen'=>'0',
			);
		}
		return $hasil;
		
	}
	
	//ASMED
	function get_data_asmed($pendaftaran_id){
		$q="SELECT *
			 FROM tpoliklinik_asmed WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function logic_akses_asmed_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_asmed,H.st_lihat as st_lihat_asmed,H.st_edit as st_edit_asmed,H.st_hapus as st_hapus_asmed,H.st_cetak as st_cetak_asmed
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_asmed_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_asmed_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_asmed_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_asmed'=>'0',
				'st_input_asmed'=>'0',
				'st_edit_asmed'=>'0',
				'st_hapus_asmed'=>'0',
				'st_cetak_asmed'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	function setting_asmed(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			,H.st_radiologi,H.st_lab,H.st_eresep

			FROM `setting_asmed` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	
	function get_data_asmed_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_asmed WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function logic_akses_asmed($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_ttv,H.st_lihat as st_lihat_ttv,H.st_edit as st_edit_ttv,H.st_hapus as st_hapus_ttv,H.st_cetak as st_cetak_ttv
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_asmed_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_asmed_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_asmed_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_ttv'=>'0',
				'st_input_ttv'=>'0',
				'st_edit_ttv'=>'0',
				'st_hapus_ttv'=>'0',
				'st_cetak_ttv'=>'0',
			);
		}
		return $hasil;
		
	}
	//NYERI
	function setting_nyeri(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_nyeri` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function get_data_nyeri($pendaftaran_id){
		$q="SELECT *
			 FROM tpoliklinik_nyeri WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function logic_akses_nyeri_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_nyeri,H.st_lihat as st_lihat_nyeri,H.st_edit as st_edit_nyeri,H.st_hapus as st_hapus_nyeri,H.st_cetak as st_cetak_nyeri
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_nyeri_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_nyeri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_nyeri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_nyeri'=>'0',
				'st_input_nyeri'=>'0',
				'st_edit_nyeri'=>'0',
				'st_hapus_nyeri'=>'0',
				'st_cetak_nyeri'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	function default_logic_nyeri($data){
		$idtipe=$data['idtipe'];
		$idpoli=$data['idpoliklinik'];
		$statuspasienbaru=$data['statuspasienbaru'];
		$pertemuan_id=$data['pertemuan_id'];
		$umur_tahun=$data['umurtahun'];
		$umur_bulan=$data['umurbulan'];
		$umur_hari=$data['umurhari'];
		$q="SELECT *FROM (
				SELECT '7' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe'
				UNION ALL
				SELECT '6.0' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli'
				UNION ALL
				SELECT '6.1' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='0'
				UNION ALL
				SELECT '5.0' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru'
				UNION ALL
				SELECT '5.1' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='0' AND H.statuspasienbaru='$statuspasienbaru'
				UNION ALL
				SELECT '4.0' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru' AND H.pertemuan_id='$pertemuan_id'
				UNION ALL
				SELECT '4.1' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='2' AND H.pertemuan_id='$pertemuan_id'
				UNION ALL
				SELECT '4.2' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='0' AND H.statuspasienbaru='2' AND H.pertemuan_id='$pertemuan_id'
				UNION ALL
				SELECT '4.3' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='0' AND H.statuspasienbaru='2' AND H.pertemuan_id='0'

				UNION ALL
				SELECT '3.0' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru' AND H.pertemuan_id='$pertemuan_id' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				UNION ALL
				SELECT '3.1' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				UNION ALL
				SELECT '3.2' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='2' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				UNION ALL
				SELECT '3.3' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='0' AND H.statuspasienbaru='2' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				UNION ALL
				SELECT '2.0' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru' AND H.pertemuan_id='$pertemuan_id' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan))
				UNION ALL
				SELECT '2.1' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan))
				UNION ALL
				SELECT '2.2' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='2' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan))
				UNION ALL
				SELECT '2.3' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='0' AND H.statuspasienbaru='2' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan))

				UNION ALL
				SELECT '1.0' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru' AND H.pertemuan_id='$pertemuan_id' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan)) AND (H.operand_hari != '0' AND calculate_logic(H.operand_hari,'$umur_hari',H.umur_hari))
				UNION ALL
				SELECT '1.1' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan)) AND (H.operand_hari != '0' AND calculate_logic(H.operand_hari,'$umur_hari',H.umur_hari))
				UNION ALL
				SELECT '1.2' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='2' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan)) AND (H.operand_hari != '0' AND calculate_logic(H.operand_hari,'$umur_hari',H.umur_hari))
				UNION ALL
				SELECT '1.3' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='0' AND H.statuspasienbaru='2' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan)) AND (H.operand_hari != '0' AND calculate_logic(H.operand_hari,'$umur_hari',H.umur_hari))
				) H ORDER BY H.lev asc
		LIMIT 1";
		$row=$this->db->query($q)->row('mnyeri_id');
		if ($row){
			return $row;
		}else{
			return 0;
		}
	}
	function setting_default_nyeri(){
		$q="SELECT st_spesifik_nyeri,st_kunci_default_nyeri FROM msetting_nyeri_default";
		return $this->db->query($q)->row_array();
	}
	function logic_akses_nyeri($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_nyeri,H.st_lihat as st_lihat_nyeri,H.st_edit as st_edit_nyeri,H.st_hapus as st_hapus_nyeri,H.st_cetak as st_cetak_nyeri
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_nyeri_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_nyeri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_nyeri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_nyeri'=>'0',
				'st_input_nyeri'=>'0',
				'st_edit_nyeri'=>'0',
				'st_hapus_nyeri'=>'0',
				'st_cetak_nyeri'=>'0',
			);
		}
		return $hasil;
		
	}
	function get_data_nyeri_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_nyeri WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_nyeri_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_nyeri_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_nyeri H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_nyeri_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT lokasi_nyeri as lokasi_nyeri_asal,penyebab_nyeri as penyebab_nyeri_asal,durasi_nyeri as durasi_nyeri_asal,frek_nyeri as frek_nyeri_asal,jenis_nyeri as jenis_nyeri_asal,skala_nyeri as skala_nyeri_asal,karakteristik_nyeri as karakteristik_nyeri_asal,nyeri_hilang as nyeri_hilang_asal,total_skor_nyeri as total_skor_nyeri_asal
				FROM tpoliklinik_nyeri_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_asmed_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT st_anamnesa as st_anamnesa_asal,keluhan_utama as keluhan_utama_asal,nama_anamnesa as nama_anamnesa_asal,hubungan_anamnesa as hubungan_anamnesa_asal
			,st_riwayat_penyakit as st_riwayat_penyakit_asal,riwayat_penyakit_lainnya as riwayat_penyakit_lainnya_asal
			,st_riwayat_penyakit_dahulu as st_riwayat_penyakit_dahulu_asal,riwayat_penyakit_dahulu as riwayat_penyakit_dahulu_asal
			,st_riwayat_penyakit_keluarga as st_riwayat_penyakit_keluarga_asal,riwayat_penyakit_keluarga as riwayat_penyakit_keluarga_asal
			,template_gambar as template_gambar_asal,gambar_tubuh as gambar_tubuh_asal,pemeriksaan_penunjang as pemeriksaan_penunjang_asal
			,rencana_asuhan as rencana_asuhan_asal,intruksi as intruksi_asal
			,rencana_kontrol as rencana_kontrol_asal,tanggal_kontrol as tanggal_kontrol_asal,idpoli_kontrol as idpoli_kontrol_asal
			,iddokter_kontrol as iddokter_kontrol_asal,st_edukasi as st_edukasi_asal,catatan_edukasi as catatan_edukasi_asal
			,diagnosa_utama as diagnosa_utama_asal,txt_keterangan as txt_keterangan_asal
				FROM tpoliklinik_asmed_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_asmed_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_asmed_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_asmed H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	//TRIAGE
	function get_data_triage($pendaftaran_id){
		$q="SELECT *
			 FROM tpoliklinik_triage WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_triage(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_triage` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function logic_akses_triage_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_triage,H.st_lihat as st_lihat_triage,H.st_edit as st_edit_triage,H.st_hapus as st_hapus_triage,H.st_cetak as st_cetak_triage
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_triage_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_triage_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_triage_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_triage'=>'0',
				'st_input_triage'=>'0',
				'st_edit_triage'=>'0',
				'st_hapus_triage'=>'0',
				'st_cetak_triage'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function logic_akses_triage($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_triage,H.st_lihat as st_lihat_triage,H.st_edit as st_edit_triage,H.st_hapus as st_hapus_triage,H.st_cetak as st_cetak_triage
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_triage_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_triage_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_triage_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_triage'=>'0',
				'st_input_triage'=>'0',
				'st_edit_triage'=>'0',
				'st_hapus_triage'=>'0',
				'st_cetak_triage'=>'0',
			);
		}
		return $hasil;
		
	}
	function get_data_triage_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_triage WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_triage_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_triage_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_triage H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_triage_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_datang as tanggal_datang_asal,sarana_transport as sarana_transport_asal,surat_pengantar as surat_pengantar_asal,asal_rujukan as asal_rujukan_asal,cara_masuk as cara_masuk_asal,macam_kasus as macam_kasus_asal,gcs_eye as gcs_eye_asal,gcs_voice as gcs_voice_asal,gcs_motion as gcs_motion_asal,pupil_1 as pupil_1_asal,pupil_2 as pupil_2_asal,reflex_cahaya_1 as reflex_cahaya_1_asal,reflex_cahaya_2 as reflex_cahaya_2_asal,spo2 as spo2_asal,akral as akral_asal,nadi as nadi_asal,nafas as nafas_asal,suhu as suhu_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,tinggi_badan as tinggi_badan_asal,berat_badan as berat_badan_asal,kasus_alergi as kasus_alergi_asal,alergi as alergi_asal,st_psikologi as st_psikologi_asal,risiko_jatuh_morse as risiko_jatuh_morse_asal,risiko_pasien_anak as risiko_pasien_anak_asal,kategori_id as kategori_id_asal,pemeriksaan as pemeriksaan_asal,respon_time as respon_time_asal,ket_nadi as ket_nadi_asal,idasalpasien as idasalpasien_asal,idrujukan as idrujukan_asal,namapengantar as namapengantar_asal,teleponpengantar as teleponpengantar_asal
				FROM tpoliklinik_triage_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	//ASMED IGD
	function get_data_asmed_igd($pendaftaran_id){
		$q="SELECT *
			 FROM tpoliklinik_asmed_igd WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function logic_akses_asmed_igd($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_asmed_igd,H.st_lihat as st_lihat_asmed_igd,H.st_edit as st_edit_asmed_igd,H.st_hapus as st_hapus_asmed_igd,H.st_cetak as st_cetak_asmed_igd
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_asmed_igd_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_asmed_igd_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_asmed_igd_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_asmed_igd'=>'0',
				'st_input_asmed_igd'=>'0',
				'st_edit_asmed_igd'=>'0',
				'st_hapus_asmed_igd'=>'0',
				'st_cetak_asmed_igd'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	function setting_asmed_igd(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			,H.st_radiologi,H.st_lab,H.st_eresep

			FROM `setting_asmed_igd` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function get_data_asmed_igd_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_asmed_igd WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	// function logic_akses_asmed_igd($mppa_id,$spesialisasi_id,$profesi_id){
		// $q="SELECT H.st_input as st_input_ttv,H.st_lihat as st_lihat_ttv,H.st_edit as st_edit_ttv,H.st_hapus as st_hapus_ttv,H.st_cetak as st_cetak_ttv
			// FROM(
			// SELECT 1 as level_akses,H.* FROM setting_asmed_igd_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			// UNION ALL
			// SELECT 2 as level_akses,H.* FROM setting_asmed_igd_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			// UNION ALL
			// SELECT 3 as level_akses,H.* FROM setting_asmed_igd_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			// ) H ORDER BY H.level_akses ASC
			// LIMIT 1";
		// $hasil=$this->db->query($q)->row_array();
		// if ($hasil){
			
		// }else{
			// $hasil=array(
				// 'st_lihat_ttv'=>'0',
				// 'st_input_ttv'=>'0',
				// 'st_edit_ttv'=>'0',
				// 'st_hapus_ttv'=>'0',
				// 'st_cetak_ttv'=>'0',
			// );
		// }
		// return $hasil;
		
	// }
	function list_edukasi_asmed_igd($assesmen_id){
		$q="SELECT M.id,M.judul,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM `medukasi` M
			LEFT JOIN tpoliklinik_asmed_igd_edukasi H ON H.assesmen_id='$assesmen_id' AND H.medukasi_id=M.id
			WHERE M.staktif='1'";
		return $this->db->query($q)->result();
	}
	function list_riwayat_penyakit_asmed_igd($assesmen_id,$jenis='1',$versi_edit='#'){
		if ($versi_edit=='#'){
			
		$nama_tabel='';
			if ($jenis=='1'){
				$nama_tabel='tpoliklinik_asmed_igd_riwayat_penyakit';
			}
			if ($jenis=='2'){
				$nama_tabel='tpoliklinik_asmed_igd_riwayat_penyakit_dahulu';
			}
			if ($jenis=='3'){
				$nama_tabel='tpoliklinik_asmed_igd_riwayat_penyakit_keluarga';
			}
			$q="SELECT M.nilai as id,M.ref as nama,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM merm_referensi M
				LEFT JOIN ".$nama_tabel." H ON H.assesmen_id='$assesmen_id' AND H.penyakit_id=M.nilai
				WHERE M.ref_head_id='25' AND M.nilai<>0";
		}else{
			$q="
				SELECT M.nilai as id,H.jenis,M.ref as nama,CASE WHEN H.penyakit_id IS NOT NULL THEN 'selected' ELSE '' END as pilih 
				FROM merm_referensi M
				LEFT JOIN (
				SELECT 1 as jenis,H.jml_edit as versi_edit,D.penyakit_id FROM tpoliklinik_asmed_igd H
				LEFT JOIN tpoliklinik_asmed_igd_riwayat_penyakit D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id'

				UNION ALL
				SELECT 1 as jenis, H.versi_edit,H.penyakit_id FROM tpoliklinik_asmed_igd_his_riwayat_penyakit H WHERE H.assesmen_id='$assesmen_id'

				UNION ALL

				SELECT 2 as jenis,H.jml_edit as versi_edit,D.penyakit_id FROM tpoliklinik_asmed_igd H
				LEFT JOIN tpoliklinik_asmed_igd_riwayat_penyakit_dahulu D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id'

				UNION ALL
				SELECT 2 as jenis, H.versi_edit,H.penyakit_id FROM tpoliklinik_asmed_igd_his_riwayat_penyakit_dahulu H WHERE H.assesmen_id='$assesmen_id'

				UNION ALL

				SELECT 3 as jenis,H.jml_edit as versi_edit,D.penyakit_id FROM tpoliklinik_asmed_igd H
				LEFT JOIN tpoliklinik_asmed_igd_riwayat_penyakit_keluarga D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id'

				UNION ALL
				SELECT 3 as jenis, H.versi_edit,H.penyakit_id FROM tpoliklinik_asmed_igd_his_riwayat_penyakit_keluarga H WHERE H.assesmen_id='$assesmen_id'
				) H ON H.penyakit_id=M.nilai
				WHERE M.ref_head_id='25' AND H.jenis='$jenis' AND H.versi_edit='$versi_edit'
			";
		}
		return $this->db->query($q)->result();
	}
	function get_default_warna_asmed_igd(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT jenis_drawing,warna FROM tpoliklinik_asmed_igd_default WHERE mppa_id='$login_ppa_id'";
		$data=$this->db->query($q)->row_array();
		if ($data){
			
		}else{
			$data=array(
				'warna'=>'#ff2525',
				'jenis_drawing'=>'bulat',
			);
		}
		return $data;
	}
	function get_lokalis_igd($assesmen_id,$gambar_id,$versi_edit='#'){
		if ($versi_edit=='#'){
		$q="SELECT *FROM tpoliklinik_asmed_igd_lokalis WHERE assesmen_id='$assesmen_id' AND gambar_id='$gambar_id'";
			
		}else{
			$q="
				SELECT *FROM(
					SELECT D.jml_edit as versi_edit,H.* 
					FROM tpoliklinik_asmed_lokalis H
					LEFT JOIN tpoliklinik_asmed_igd D ON D.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'

					UNION ALL

					SELECT H.* 
					FROM tpoliklinik_asmed_igd_his_lokalis H
					WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'
					) T 

					WHERE T.versi_edit='$versi_edit'
					ORDER BY T.`index` ASC
			";
		}
		// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	function list_template_asmed_igd(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_asmed_igd H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	function get_data_asmed_igd_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_asmed_igd_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_asmed_igd H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_asmed_igd_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT st_anamnesa as st_anamnesa_asal,keluhan_utama as keluhan_utama_asal,nama_anamnesa as nama_anamnesa_asal,hubungan_anamnesa as hubungan_anamnesa_asal
			,st_riwayat_penyakit as st_riwayat_penyakit_asal,riwayat_penyakit_lainnya as riwayat_penyakit_lainnya_asal
			,st_riwayat_penyakit_dahulu as st_riwayat_penyakit_dahulu_asal,riwayat_penyakit_dahulu as riwayat_penyakit_dahulu_asal
			,st_riwayat_penyakit_keluarga as st_riwayat_penyakit_keluarga_asal,riwayat_penyakit_keluarga as riwayat_penyakit_keluarga_asal
			,template_gambar as template_gambar_asal,gambar_tubuh as gambar_tubuh_asal,pemeriksaan_penunjang as pemeriksaan_penunjang_asal
			,rencana_asuhan as rencana_asuhan_asal,intruksi as intruksi_asal
			,rencana_kontrol as rencana_kontrol_asal,tanggal_kontrol as tanggal_kontrol_asal,idpoli_kontrol as idpoli_kontrol_asal
			,iddokter_kontrol as iddokter_kontrol_asal,st_edukasi as st_edukasi_asal,catatan_edukasi as catatan_edukasi_asal
			,diagnosa_utama as diagnosa_utama_asal,txt_keterangan as txt_keterangan_asal
			,txt_keterangan_diagnosa as txt_keterangan_diagnosa_asal,keadaan_umum as keadaan_umum_asal,tingkat_kesadaran as tingkat_kesadaran_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,nafas as nafas_asal,gds as gds_asal,st_pulang as st_pulang_asal,tanggal_keluar as tanggal_keluar_asal
				FROM tpoliklinik_asmed_igd_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	// function list_edukasi_asmed_igd_his($assesmen_id){
		// $q="SELECT M.id,M.judul,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM `medukasi` M
			// LEFT JOIN tpoliklinik_asmed_igd_edukasi H ON H.assesmen_id='$assesmen_id' AND H.medukasi_id=M.id
			// WHERE M.staktif='1'";
		// return $this->db->query($q)->result();
	// }
	function list_edukasi_asmed_igd_his($assesmen_id,$versi_edit){
		$q="SELECT H.medukasi_id as id,M.judul,'selected' as pilih FROM (
			SELECT H.medukasi_id,H.versi_edit FROM tpoliklinik_asmed_igd_his_edukasi H WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT D.medukasi_id,H.jml_edit as versi_edit FROM tpoliklinik_asmed_igd_edukasi D 
			LEFT JOIN tpoliklinik_asmed_igd H ON H.assesmen_id=D.assesmen_id
			WHERE D.assesmen_id='$assesmen_id'
			) H 
			INNER JOIN medukasi M ON M.id=H.medukasi_id
			WHERE H.versi_edit='$versi_edit'";
		// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	//ASSESMEN IGD
	function get_data_assesmen_igd($pendaftaran_id){
		$q="SELECT *
			 FROM tpoliklinik_assesmen_igd WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	function get_data_assesmen_igd_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_assesmen_igd WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_assesmen_igd_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_assesmen_igd_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_assesmen_igd H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_assesmen_igd_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT template_id as template_id_asal,
				tanggal_datang as tanggal_datang_asal,sarana_transport as sarana_transport_asal,surat_pengantar as surat_pengantar_asal,asal_rujukan as asal_rujukan_asal,cara_masuk as cara_masuk_asal,macam_kasus as macam_kasus_asal,idasalpasien as idasalpasien_asal,idrujukan as idrujukan_asal,namapengantar as namapengantar_asal,teleponpengantar as teleponpengantar_asal,st_kecelakaan as st_kecelakaan_asal,jenis_kecelakaan as jenis_kecelakaan_asal,jenis_kendaraan_pasien as jenis_kendaraan_pasien_asal,jenis_kendaraan_lawan as jenis_kendaraan_lawan_asal,lokasi_kecelakaan as lokasi_kecelakaan_asal,lokasi_lainnya as lokasi_lainnya_asal,st_hamil as st_hamil_asal,hamil_g as hamil_g_asal,hamil_p as hamil_p_asal,hamil_a as hamil_a_asal,st_menyusui as st_menyusui_asal,info_menyusui as info_menyusui_asal,keadaan_umum as keadaan_umum_asal,tingkat_kesadaran as tingkat_kesadaran_asal,nadi as nadi_asal,nafas as nafas_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,suhu as suhu_asal,tinggi_badan as tinggi_badan_asal,berat_badan as berat_badan_asal,agama as agama_asal,faktor_komorbid as faktor_komorbid_asal,penurunan_fungsi as penurunan_fungsi_asal,st_decubitus as st_decubitus_asal,st_p3 as st_p3_asal,st_riwayat_demam as st_riwayat_demam_asal,st_berkeringat_malam as st_berkeringat_malam_asal,st_pergi_area_wabah as st_pergi_area_wabah_asal,st_pemakaian_obat_panjang as st_pemakaian_obat_panjang_asal,st_turun_bb_tanpa_sebab as st_turun_bb_tanpa_sebab_asal,intruksi_keperawatan as intruksi_keperawatan_asal,keadaan_umum_keluar as keadaan_umum_keluar_asal,tingkat_kesadaran_keluar as tingkat_kesadaran_keluar_asal,td_sistole_keluar as td_sistole_keluar_asal,td_diastole_keluar as td_diastole_keluar_asal,nafas_keluar as nafas_keluar_asal,gds_keluar as gds_keluar_asal,st_pulang as st_pulang_asal,tanggal_keluar as tanggal_keluar_asal,rencana_kontrol as rencana_kontrol_asal,tanggal_kontrol as tanggal_kontrol_asal,idpoli_kontrol as idpoli_kontrol_asal,iddokter_kontrol as iddokter_kontrol_asal,st_anamnesa as st_anamnesa_asal,keluhan_utama as keluhan_utama_asal,nama_anamnesa as nama_anamnesa_asal,hubungan_anamnesa as hubungan_anamnesa_asal,st_riwayat_penyakit as st_riwayat_penyakit_asal,riwayat_penyakit_lainnya as riwayat_penyakit_lainnya_asal,riwayat_alergi as riwayat_alergi_asal,riwayat_pengobatan as riwayat_pengobatan_asal,hubungan_anggota_keluarga as hubungan_anggota_keluarga_asal,st_psikologis as st_psikologis_asal,st_sosial_ekonomi as st_sosial_ekonomi_asal,st_spiritual as st_spiritual_asal,pendidikan as pendidikan_asal,pekerjaan as pekerjaan_asal,st_nafsu_makan as st_nafsu_makan_asal,st_turun_bb as st_turun_bb_asal,st_mual as st_mual_asal,st_muntah as st_muntah_asal,st_fungsional as st_fungsional_asal,st_alat_bantu as st_alat_bantu_asal,st_cacat as st_cacat_asal,cacat_lain as cacat_lain_asal,header_risiko_jatuh as header_risiko_jatuh_asal,footer_risiko_jatuh as footer_risiko_jatuh_asal,skor_pengkajian as skor_pengkajian_asal,risiko_jatuh as risiko_jatuh_asal,nama_tindakan as nama_tindakan_asal,st_tindakan as st_tindakan_asal,st_tindakan_action as st_tindakan_action_asal,st_edukasi as st_edukasi_asal,st_menerima_info as st_menerima_info_asal,st_hambatan_edukasi as st_hambatan_edukasi_asal,st_penerjemaah as st_penerjemaah_asal,penerjemaah as penerjemaah_asal,catatan_edukasi as catatan_edukasi_asal
				FROM tpoliklinik_assesmen_igd_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_assesmen_igd(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			FROM `setting_assesmen_igd` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function list_template_assement_igd(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_assesmen_igd H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function logic_akses_assesmen_igd($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_assesmen_igd,H.st_lihat as st_lihat_assesmen_igd,H.st_edit as st_edit_assesmen_igd,H.st_hapus as st_hapus_assesmen_igd,H.st_cetak as st_cetak_assesmen_igd
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_assesmen_igd_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_assesmen_igd_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_assesmen_igd_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_assesmen_igd'=>'0',
				'st_input_assesmen_igd'=>'0',
				'st_edit_assesmen_igd'=>'0',
				'st_hapus_assesmen_igd'=>'0',
				'st_cetak_assesmen_igd'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function list_edukasi_assesmen_igd($assesmen_id){
		$q="SELECT M.id,M.judul,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM `medukasi` M
			LEFT JOIN tpoliklinik_assesmen_igd_edukasi H ON H.assesmen_id='$assesmen_id' AND H.medukasi_id=M.id
			WHERE M.staktif='1'";
		return $this->db->query($q)->result();
	}
	function list_riwayat_penyakit_assesmen_igd($assesmen_id){
		$q="SELECT M.nilai as id,M.ref as nama,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM merm_referensi M
			LEFT JOIN tpoliklinik_assesmen_igd_riwayat_penyakit H ON H.assesmen_id='$assesmen_id' AND H.penyakit_id=M.nilai
			WHERE M.ref_head_id='25' AND M.nilai<>0";
		return $this->db->query($q)->result();
	}
	function perubahan_edukasi_assesmen_igd($assesmen_id,$versi_edit){
		if ($versi_edit!=0){
			$versi_edit_last=$versi_edit -1;
		}
		$hasil_1='';
		$hasil_2='';
		if ($versi_edit!=0){
			
			$q="SELECT H.versi_edit, GROUP_CONCAT(M.judul) as judul FROM (
				SELECT H.medukasi_id,H.versi_edit FROM tpoliklinik_assesmen_igd_his_edukasi H WHERE H.assesmen_id='$assesmen_id'
				UNION ALL
				SELECT D.medukasi_id,H.jml_edit as versi_edit FROM tpoliklinik_assesmen_igd_edukasi D 
				LEFT JOIN tpoliklinik_assesmen_igd H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'
				) H 
				INNER JOIN medukasi M ON M.id=H.medukasi_id
				WHERE H.versi_edit='$versi_edit'
				GROUP BY H.versi_edit
			";
			$hasil_1=$this->db->query($q)->row('judul');
			$q="SELECT H.versi_edit, GROUP_CONCAT(M.judul) as judul FROM (
				SELECT H.medukasi_id,H.versi_edit FROM tpoliklinik_assesmen_igd_his_edukasi H WHERE H.assesmen_id='$assesmen_id'
				UNION ALL
				SELECT D.medukasi_id,H.jml_edit as versi_edit FROM tpoliklinik_assesmen_igd_edukasi D 
				LEFT JOIN tpoliklinik_assesmen_igd H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'
				) H 
				INNER JOIN medukasi M ON M.id=H.medukasi_id
				WHERE H.versi_edit='$versi_edit_last'
				GROUP BY H.versi_edit
			";
			$hasil_2=$this->db->query($q)->row('judul');
			
		}
		$data_edukasi=array(
			'edukasi_current'=>$hasil_1,
			'edukasi_last'=>$hasil_2,
		);
		return $data_edukasi;
	}
	//CPPT
	function list_template_cppt(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_cppt H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function setting_cppt(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_cppt` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function setting_cppt_field($profesi_id=''){
		if ($profesi_id==''){
			$login_profesi_id=$this->session->userdata('login_profesi_id');
			
		}else{
			$login_profesi_id=$profesi_id;
			
		}
		$q="SELECT field_subjectif,field_objectif,field_assesmen,field_planing,field_intruction,field_intervensi,field_evaluasi,field_reasessmen FROM setting_cppt_filed 
			WHERE profesi_id='$login_profesi_id'";
		$row=$this->db->query($q)->row_array();
		if ($row){
			$data=$row;
		}else{
			$data=array(
				'field_subjectif' =>'0',
				'field_objectif' =>'0',
				'field_assesmen' =>'0',
				'field_planing' =>'0',
				'field_intruction' =>'0',
				'field_intervensi' =>'0',
				'field_evaluasi' =>'0',
				'field_reasessmen' =>'0',
			);
		}
		return $data;
	}
	function get_data_cppt($pendaftaran_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *
			 FROM tpoliklinik_cppt WHERE pendaftaran_id='$pendaftaran_id' AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	
	function logic_akses_cppt_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_cppt,H.st_lihat as st_lihat_cppt,H.st_edit as st_edit_cppt,H.st_hapus as st_hapus_cppt,H.st_cetak as st_cetak_cppt
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_cppt_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_cppt_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_cppt_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_cppt'=>'0',
				'st_input_cppt'=>'0',
				'st_edit_cppt'=>'0',
				'st_hapus_cppt'=>'0',
				'st_cetak_cppt'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	
	function logic_akses_cppt($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_cppt,H.st_lihat as st_lihat_cppt,H.st_edit as st_edit_cppt,H.st_hapus as st_hapus_cppt,H.st_cetak as st_cetak_cppt
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_cppt_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_cppt_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_cppt_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_cppt'=>'0',
				'st_input_cppt'=>'0',
				'st_edit_cppt'=>'0',
				'st_hapus_cppt'=>'0',
				'st_cetak_cppt'=>'0',
			);
		}
		return $hasil;
		
	}
	function get_data_cppt_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_cppt WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_cppt_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_cppt_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_cppt H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_cppt_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT subjectif as subjectif_asal,objectif as objectif_asal,assemen as assemen_asal,planing as planing_asal,intruksi as intruksi_asal,intervensi as intervensi_asal,evaluasi as evaluasi_asal,reassesmen as reassesmen_asal
				FROM tpoliklinik_cppt_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	//CPPT
	function list_template_ic(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_ic H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function setting_ic(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_ic` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function get_data_ic($pendaftaran_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *
			 FROM tpoliklinik_ic WHERE pendaftaran_id='$pendaftaran_id' AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	
	function logic_akses_ic_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_ic,H.st_lihat as st_lihat_ic,H.st_edit as st_edit_ic,H.st_hapus as st_hapus_ic,H.st_cetak as st_cetak_ic
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_ic_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_ic_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_ic_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_ic'=>'0',
				'st_input_ic'=>'0',
				'st_edit_ic'=>'0',
				'st_hapus_ic'=>'0',
				'st_cetak_ic'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	
	function logic_akses_ic($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_ic,H.st_lihat as st_lihat_ic,H.st_edit as st_edit_ic,H.st_hapus as st_hapus_ic,H.st_cetak as st_cetak_ic
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_ic_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_ic_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_ic_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_ic'=>'0',
				'st_input_ic'=>'0',
				'st_edit_ic'=>'0',
				'st_hapus_ic'=>'0',
				'st_cetak_ic'=>'0',
			);
		}
		return $hasil;
		
	}
	function get_data_ic_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_ic WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_ic_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_ic_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_ic H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_ic_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT subjectif as subjectif_asal,objectif as objectif_asal,assemen as assemen_asal,planing as planing_asal,intruksi as intruksi_asal,intervensi as intervensi_asal,evaluasi as evaluasi_asal,reassesmen as reassesmen_asal
				FROM tpoliklinik_ic_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_user_pelaksana_logic_ic(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *FROM setting_ic_user,setting_ic_keterangan";
		$data=$this->db->query($q)->row_array();
		if ($data['st_setting_dokter']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}else{
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_ic_user_list` H
					WHERE H.jenis_akses='1' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_ic_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_ic_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}
		if ($data['st_setting_pemberi_info']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pemberi']=$this->db->query($q)->result();
		}else{
			$data['list_pemberi']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_ic_user_list` H
					WHERE H.jenis_akses='2' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_ic_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_ic_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pemberi']=$this->db->query($q)->result();
		}
		if ($data['st_setting_petugas_pendamping']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pendamping']=$this->db->query($q)->result();
		}else{
			$data['list_pendamping']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_ic_user_list` H
					WHERE H.jenis_akses='3' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_ic_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_ic_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pendamping']=$this->db->query($q)->result();
		}
		
		// print_r($data);exit;
		return $data;
	}
}
