<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mdiagnosa_ranap_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	public function getSpecified($id)
    {
        $q="SELECT 
			*
			from mdiagnosa_ranap M
					
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	
    public function saveData()
    {
		// print_r($this->input->post());exit;
        $this->nama 	= $_POST['nama'];		
        $this->kode_diagnosa 	= $_POST['kode_diagnosa'];		
        $this->template_id 	= $_POST['template_id'];		
        $this->deskripsi 	= $_POST['deskripsi'];		
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mdiagnosa_ranap', $this)) {
			$id=$this->db->insert_id();
            return $id;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama 	= $_POST['nama'];		
        $this->kode_diagnosa 	= $_POST['kode_diagnosa'];		
        $this->template_id 	= $_POST['template_id'];			
        $this->deskripsi 	= $_POST['deskripsi'];			
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mdiagnosa_ranap', $this, array('id' => $_POST['id']))) {
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->staktif = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mdiagnosa_ranap', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->staktif = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mdiagnosa_ranap', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
