<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tklaim_rincian_model extends CI_Model
{
  
   public function detail($id){
	   $q="SELECT TK.id,TK.no_klaim,TK.tipe,CASE WHEN TK.idkelompokpasien !='1' THEN kel.nama ELSE R.nama END as rekanan_nama,TK.idkelompokpasien
					,TK.idrekanan,TK.jml_faktur,TK.total_tagihan,TK.tanggal_tagihan
					,DATEDIFF(TK.tanggal_kirim, TK.batas_kirim) as selisih,TK.jatuh_tempo_bayar,TK.kirim_date,TK.noresi
					,TK.`status`,TK.status_kirim,TK.idpasien
					FROM tklaim TK
					LEFT JOIN mpasien_kelompok kel ON kel.id=TK.idkelompokpasien
					LEFT JOIN mrekanan R ON R.id=TK.idrekanan

					WHERE TK.id='$id'";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function list_kelompok_pasien(){
	   $q="SELECT id,nama from mpasien_kelompok 
				WHERE mpasien_kelompok.`status`='1' AND mpasien_kelompok.id !='5'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_rekanan(){
	   $q="SELECT id,nama from mrekanan 
				WHERE mrekanan.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function Get_Kwitansi($id){
	   // $q="SELECT H.id,H.no_klaim,CASE WHEN H.idkelompokpasien='1' THEN MR.namalengkap ELSE MK.nama END nama 
			// ,H.total_tagihan,H.tipe,H.idkelompokpasien,H.idrekanan
			// from tklaim H 
			// LEFT JOIN mrekanan MR ON MR.id=H.idrekanan AND H.idkelompokpasien='1'
			// LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien AND H.idkelompokpasien!='1'
			// WHERE H.id='$id'";
		$q="SELECT H.id,H.no_klaim,CONCAT(MP.title,' ',MP.nama) as namapasien,H.idpasien,CASE WHEN H.idkelompokpasien='1' THEN MR.namalengkap ELSE MK.nama END nama 
			,H.total_tagihan,H.tipe,H.idkelompokpasien,H.idrekanan
			from tklaim H 
			LEFT JOIN mfpasien MP ON MP.id=H.idpasien
			LEFT JOIN mpasien_title MPT ON MPT.id=MP.title
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan AND H.idkelompokpasien='1'
			LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien AND H.idkelompokpasien!='1'
			WHERE H.id='$id'";
		$query=$this->db->query($q);
		return $query->row();
   }
   public function Get_KwitansiHistory($id){
	   $q="SELECT H.id,H.no_klaim,CASE WHEN H.idkelompokpasien='1' THEN MR.namalengkap ELSE MK.nama END namakontraktor 
			,H.total_tagihan,H.tipe,H.idkelompokpasien,H.idrekanan
			from tklaim H 
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan AND H.idkelompokpasien='1'
			LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien AND H.idkelompokpasien!='1'
			WHERE H.id='$id'";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function Get_KwitansiDetail($id){
	   $q="SELECT H.no_klaim
			,CASE WHEN H.tipe='1' THEN P.nopendaftaran ELSE RI.nopendaftaran END as nopendaftaran
			,CASE WHEN H.tipe='1' THEN CONCAT(P.title,'. ',P.namapasien) ELSE CONCAT(RI.title,'. ',RI.namapasien) END as namapasien
			,CASE WHEN H.idkelompokpasien='1' THEN R.namalengkap ELSE K.nama END as nama_perusahaan
			,H.tipe,D.nominal
			FROM `tklaim_detail` D
			LEFT JOIN tklaim H ON H.id=D.klaim_id
			LEFT JOIN tpoliklinik_pendaftaran P ON P.id=D.pendaftaran_id AND H.tipe='1'
			LEFT JOIN trawatinap_pendaftaran RI ON RI.id=D.pendaftaran_id AND H.tipe='2'
			LEFT JOIN mpasien_kelompok K ON K.id=H.idkelompokpasien AND H.idkelompokpasien !='1'
			LEFT JOIN mrekanan R ON R.id=H.idrekanan AND H.idkelompokpasien='1'
			WHERE D.id='$id'";
		$query=$this->db->query($q);
		return $query->row();
   }
   public function detail_rajal($id){
	   $q="
			SELECT TP.id as idpendaftaran,D.kasir_id, TP.tanggaldaftar,TP.nopendaftaran,TP.no_medrec,CONCAT(TP.title,'. ',TP.namapasien) as namapasien,TP.iddokter,MD.nama as namadokter 
			,0 as adm
			,0 as tindakan
			,0 as lab
			,0 as rad
			,0 as fis
			,0 as p_alkes
			,0 as p_obat
			,0 as f_alkes
			,0 as f_obat
			,SUM(K.total) as total,SUM(D.nominal) as nominal
			FROM tklaim H
			LEFT JOIN tklaim_detail D ON D.klaim_id=H.id
			LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=D.pendaftaran_id
			LEFT JOIN mdokter MD ON MD.id=TP.iddokter
			LEFT JOIN tkasir K ON K.id=D.kasir_id
			WHERE H.id='$id'
			GROUP BY H.id,TP.id
		 ";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function detail_history($id,$idkelompokpasien,$idrekanan){
	   $where='';
	   if ($idkelompokpasien=='1'){
		   $where =" AND H.idrekanan='$idrekanan'";	   
	   }else{
		    $where =" AND H.idkelompokpasien='$idkelompokpasien'";
	   }
	   $q="SELECT H.id,H.idkelompokpasien,H.idrekanan, H.no_klaim,H.tipe,H.tanggal_tagihan,H.tanggal_kirim,H.jatuh_tempo_bayar,SUM(D.tidak_terbayar) as sisa_tagihan,H.total_tagihan
			,ceil(DATEDIFF(CURRENT_DATE, H.tanggal_kirim) / 30) as umur
			FROM tklaim H
			INNER JOIN tklaim_detail D ON D.klaim_id=H.id AND D.status_lunas=0
			WHERE H.id !='$id' ".$where." AND H.tidak_terbayar > 0 AND H.status_kirim='1'
			GROUP BY H.id
		 ";
		$query=$this->db->query($q);
		return $query->result();
   }
   function refresh_image($id){
		$q="SELECT  H.* from tklaim_resi H

			WHERE H.klaim_id='$id'";
		// print_r($q);exit();
		$row= $this->db->query($q)->result();
		$tabel='';
		$no=1;
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/klaim_resi/'.$r->file_name.'" target="_blank">'.$r->file_name.'</a></td>';
			$tabel .='<td class="text-left">'.$r->user_upload.'-'.HumanDateLong($r->tanggal_upload).'</td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">
					<button class="btn btn-danger btn-xs removeData" onclick="hapus_file('.$r->id.')"><i class="fa fa-trash-o"></i></button></td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		return $tabel;
	}
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
