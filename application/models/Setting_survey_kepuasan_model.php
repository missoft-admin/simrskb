<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_survey_kepuasan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_survey_kepuasan_setting_label(){
		$q="SELECT * FROM setting_survey_kepuasan_label H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	public function get_survey_kepuasan_setting(){
		$q="SELECT * FROM setting_survey_kepuasan H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function save_survey_kepuasan(){
		$id =1;
		// $this->judul_header = $this->input->post('judul_header');
		// $this->judul_footer = $this->input->post('judul_footer');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->lama_edit = $this->input->post('lama_edit');
		$this->orang_edit = $this->input->post('orang_edit');
		$this->st_hapus_catatan = $this->input->post('st_hapus_catatan');
		$this->lama_hapus = $this->input->post('lama_hapus');
		$this->orang_hapus = $this->input->post('orang_hapus');
		$this->st_duplikasi_catatan = $this->input->post('st_duplikasi_catatan');
		$this->lama_duplikasi = $this->input->post('lama_duplikasi');
		$this->orang_duplikasi = $this->input->post('orang_duplikasi');
		$this->judul_header_ina = $this->input->post('judul_header_ina');
		$this->judul_footer_ina = $this->input->post('judul_footer_ina');
		$this->judul_header_eng = $this->input->post('judul_header_eng');
		$this->judul_footer_eng = $this->input->post('judul_footer_eng');
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_survey_kepuasan', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_survey_kepuasan_label(){
		// print_r($this->input->post());exit;
		$id =1;
		$this->alamat_rs = $this->input->post('alamat_rs');
		$this->phone_rs = $this->input->post('phone_rs');
		$this->web_rs = $this->input->post('web_rs');
		$this->label_identitas_ina= $this->input->post('label_identitas_ina');
		$this->label_identitas_eng= $this->input->post('label_identitas_eng');
		$this->no_reg_ina= $this->input->post('no_reg_ina');
		$this->no_reg_eng= $this->input->post('no_reg_eng');
		$this->no_rm_ina= $this->input->post('no_rm_ina');
		$this->no_rm_eng= $this->input->post('no_rm_eng');
		$this->nama_pasien_ina= $this->input->post('nama_pasien_ina');
		$this->nama_pasien_eng= $this->input->post('nama_pasien_eng');
		$this->ttl_ina= $this->input->post('ttl_ina');
		$this->ttl_eng= $this->input->post('ttl_eng');
		$this->umur_ina= $this->input->post('umur_ina');
		$this->umur_eng= $this->input->post('umur_eng');
		$this->jk_ina= $this->input->post('jk_ina');
		$this->jk_eng= $this->input->post('jk_eng');
		$this->asal_pasien_ina= $this->input->post('asal_pasien_ina');
		$this->asal_pasien_eng= $this->input->post('asal_pasien_eng');
		$this->detail_ina= $this->input->post('detail_ina');
		$this->detail_eng= $this->input->post('detail_eng');
		$this->dokter_ina= $this->input->post('dokter_ina');
		$this->dokter_eng= $this->input->post('dokter_eng');
		$this->profile_ina= $this->input->post('profile_ina');
		$this->profile_eng= $this->input->post('profile_eng');
		$this->profile_nama_ina= $this->input->post('profile_nama_ina');
		$this->profile_nama_eng= $this->input->post('profile_nama_eng');
		$this->profile_nama_ina_ina= $this->input->post('profile_nama_ina_ina');
		$this->profile_nama_ina_eng= $this->input->post('profile_nama_ina_eng');
		$this->ttl_profile_ina= $this->input->post('ttl_profile_ina');
		$this->ttl_profile_eng= $this->input->post('ttl_profile_eng');
		$this->umur_profile_ina= $this->input->post('umur_profile_ina');
		$this->umur_profile_eng= $this->input->post('umur_profile_eng');
		$this->jk_profile_ina= $this->input->post('jk_profile_ina');
		$this->jk_profile_eng= $this->input->post('jk_profile_eng');
		$this->pekerjaan_profile_ina= $this->input->post('pekerjaan_profile_ina');
		$this->pekerjaan_profile_eng= $this->input->post('pekerjaan_profile_eng');
		
		$this->pendidikan_profile_ina= $this->input->post('pendidikan_profile_ina');
		$this->pendidikan_profile_eng= $this->input->post('pendidikan_profile_eng');
		$this->mengetahui_rs_ina= $this->input->post('mengetahui_rs_ina');
		$this->mengetahui_rs_eng= $this->input->post('mengetahui_rs_eng');
		
		$this->mengetahui_lain_ina= $this->input->post('mengetahui_lain_ina');
		$this->mengetahui_lain_eng= $this->input->post('mengetahui_lain_eng');
		$this->waktu_survey_ina= $this->input->post('waktu_survey_ina');
		$this->waktu_survey_eng= $this->input->post('waktu_survey_eng');
		$this->nama_survey_ina= $this->input->post('nama_survey_ina');
		$this->nama_survey_eng= $this->input->post('nama_survey_eng');
		
		$this->total_nilai_ina= $this->input->post('total_nilai_ina');
		$this->total_nilai_eng= $this->input->post('total_nilai_eng');
		
		$this->nilai_perunsur_ina= $this->input->post('nilai_perunsur_ina');
		$this->nilai_perunsur_eng= $this->input->post('nilai_perunsur_eng');
		
		$this->nrr_perunsur_ina= $this->input->post('nrr_perunsur_ina');
		$this->nrr_perunsur_eng= $this->input->post('nrr_perunsur_eng');
		
		$this->nrr_tertimbang_ina= $this->input->post('nrr_tertimbang_ina');
		$this->nrr_tertimbang_eng= $this->input->post('nrr_tertimbang_eng');
		$this->satuan_kepuasan_ina= $this->input->post('satuan_kepuasan_ina');
		$this->satuan_kepuasan_eng= $this->input->post('satuan_kepuasan_eng');
		$this->hasil_survey_ina= $this->input->post('hasil_survey_ina');
		$this->hasil_survey_eng= $this->input->post('hasil_survey_eng');
		
		$this->pertanyaan_ina= $this->input->post('pertanyaan_ina');
		$this->pertanyaan_eng= $this->input->post('pertanyaan_eng');
		$this->jawaban_ina= $this->input->post('jawaban_ina');
		$this->jawaban_eng= $this->input->post('jawaban_eng');
		
		$this->total_penilaian_ina= $this->input->post('total_penilaian_ina');
		$this->total_penilaian_eng= $this->input->post('total_penilaian_eng');
		$this->kritik_ina= $this->input->post('kritik_ina');
		$this->kritik_eng= $this->input->post('kritik_eng');
		$this->responden_ina= $this->input->post('responden_ina');
		$this->responden_eng= $this->input->post('responden_eng');
		$this->notes_footer_ina= $this->input->post('notes_footer_ina');
		$this->notes_footer_eng= $this->input->post('notes_footer_eng');
		
		
		$this->upload_login_logo(true);
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_survey_kepuasan_label', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	public function upload_login_logo($update = false)
    {
        if (!file_exists('assets/upload/app_setting')) {
            mkdir('assets/upload/app_setting', 0755, true);
        }
        if (isset($_FILES['logo'])) {
		// print_r('sini');exit;
            if ($_FILES['logo']['name'] != '') {
                $config['upload_path'] = './assets/upload/app_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('logo')) {
                    $image_upload = $this->upload->data();
                    $this->logo = $image_upload['file_name'];
					
                    if ($update == true) {
                        // $this->remove_image_logo(1);
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
                return true;
            }
					// print_r($this->foto);exit;
        } else {
            return true;
        }
		
    }
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


