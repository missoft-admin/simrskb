<?php

declare(strict_types=1);

class Mnilainormal_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $this->db->where('status', '1');
        $query = $this->db->get('mnilainormal');

        return $query->row();
    }

    public function getSatuanLab()
    {
        $this->db->where('idkategori', '2');
        $this->db->where('status', '1');
        $query = $this->db->get('msatuan');

        return $query->result();
    }

    public function getTarifLab()
    {
        $query = $this->db->query('SELECT
        	mtarif_laboratorium.id,
        	mtarif_laboratorium.nama AS name
        FROM
        	mtarif_laboratorium
        	LEFT JOIN mnilainormal_pelayanan ON mnilainormal_pelayanan.idtariflaboratorium = mtarif_laboratorium.id
        	LEFT JOIN mnilainormal ON mnilainormal.id = mnilainormal_pelayanan.idnilainormal
        WHERE
        	mtarif_laboratorium.idkelompok = 0
        	AND mtarif_laboratorium.STATUS = 1
        	AND (mnilainormal.STATUS = 0 OR mnilainormal.STATUS IS NULL)
        GROUP BY
        	mtarif_laboratorium.id');
        $result = $query->result();

        return json_encode($result);
    }

    public function getTarifLabSelected($id)
    {
        $this->db->select('mtarif_laboratorium.id, mtarif_laboratorium.nama');
        $this->db->join('mtarif_laboratorium', 'mtarif_laboratorium.id = mnilainormal_pelayanan.idtariflaboratorium');
        $this->db->where('mnilainormal_pelayanan.idnilainormal', $id);
        $this->db->where('mnilainormal_pelayanan.status', 1);
        $query = $this->db->get('mnilainormal_pelayanan');

        return $query->result();
    }

    public function getListKriteria($id)
    {
        $this->db->where('idnilainormal', $id);
        $query = $this->db->get('mnilainormal_kriteria');

        return $query->result();
    }

    public function getListKritis($id)
    {
        $this->db->where('idnilainormal', $id);
        $query = $this->db->get('mnilainormal_kritis');

        return $query->result();
    }

    public function getIdTarifLab($namatarif)
    {
        $this->db->where('nama', $namatarif);
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_laboratorium');

        return $query->row()->id;
    }

    public function saveData()
    {
        $this->nama = $_POST['nama'];
        $this->idsatuan = $_POST['idsatuan'];
        $this->status = 1;
        $this->created_by = $this->session->userdata('user_id');
        $this->created_date = date('Y-m-d H:i:s');

        if ($this->db->insert('mnilainormal', $this)) {
            $idnilainormal = $this->db->insert_id();

            $kriteria = json_decode($_POST['kriteria_value']);
            foreach ($kriteria as $row) {
                $detail = [];
                $detail['idnilainormal'] = $idnilainormal;
                $detail['kriteria'] = $row[0];
                $detail['nilai'] = $row[1];
                $detail['status'] = 1;

                $this->db->insert('mnilainormal_kriteria', $detail);
            }

            $kritis = json_decode($_POST['kritis_value']);
            foreach ($kritis as $row) {
                $detail = [];
                $detail['idnilainormal'] = $idnilainormal;
                $detail['kriteria'] = $row[0];
                $detail['nilai'] = $row[1];
                $detail['status'] = 1;

                $this->db->insert('mnilainormal_kritis', $detail);
            }

            if (isset($_POST['idtariflaboratorium'])) {
                $pelayanan = $_POST['idtariflaboratorium'];
                foreach ($pelayanan as $row) {
                    $detail = [];
                    $detail['idnilainormal'] = $idnilainormal;
                    $detail['idtariflaboratorium'] = $this->getIdTarifLab($row);
                    $detail['status'] = 1;

                    $this->db->insert('mnilainormal_pelayanan', $detail);
                }
            }
        }

        return true;
    }

    public function updateData()
    {
        $this->nama = $_POST['nama'];
        $this->idsatuan = $_POST['idsatuan'];
        $this->status = 1;
        $this->edited_by = $this->session->userdata('user_id');
        $this->edited_date = date('Y-m-d H:i:s');

        if ($this->db->update('mnilainormal', $this, ['id' => $_POST['id']])) {
            $idnilainormal = $_POST['id'];

            $this->db->where('idnilainormal', $idnilainormal);
            if ($this->db->delete('mnilainormal_kriteria')) {
                $kriteria = json_decode($_POST['kriteria_value']);
                foreach ($kriteria as $row) {
                    $detail = [];
                    $detail['idnilainormal'] = $idnilainormal;
                    $detail['kriteria'] = $row[0];
                    $detail['nilai'] = $row[1];
                    $detail['status'] = 1;

                    $this->db->insert('mnilainormal_kriteria', $detail);
                }
            }

            $this->db->where('idnilainormal', $idnilainormal);
            if ($this->db->delete('mnilainormal_kritis')) {
                $kritis = json_decode($_POST['kritis_value']);
                foreach ($kritis as $row) {
                    $detail = [];
                    $detail['idnilainormal'] = $idnilainormal;
                    $detail['kriteria'] = $row[0];
                    $detail['nilai'] = $row[1];
                    $detail['status'] = 1;

                    $this->db->insert('mnilainormal_kritis', $detail);
                }
            }

            $this->db->where('idnilainormal', $idnilainormal);
            if ($this->db->delete('mnilainormal_pelayanan')) {
                $pelayanan = $_POST['idtariflaboratorium'];
                foreach ($pelayanan as $row) {
                    $detail = [];
                    $detail['idnilainormal'] = $idnilainormal;
                    $detail['idtariflaboratorium'] = $this->getIdTarifLab($row);
                    $detail['status'] = 1;

                    $this->db->insert('mnilainormal_pelayanan', $detail);
                }
            }

            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;

        return true;
    }

    public function softDelete($id)
    {
        $this->status = 0;
        $this->deleted_by = $this->session->userdata('user_id');
        $this->deleted_date = date('Y-m-d H:i:s');

        if ($this->db->update('mnilainormal', $this, ['id' => $id])) {
            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;
    }
}
