<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mrekapan_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getSpecified($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('mrekap');
        return $query->row();
    }
	public function list_akun() {
        $this->db->where('status', '1');
        // $this->db->where('stheader', '0');
        $query = $this->db->get('makun_nomor');
        return $query->result();
    }
	public function list_rekening() {
        // $this->db->where('status', '1');
        // $this->db->where('stheader', '0');
        $query = $this->db->get('ref_bank');
        return $query->result();
    }
	public function list_jenis() {
        $this->db->where('status', '1');
        // $this->db->where('stheader', '0');
        $query = $this->db->get('mjenis_gaji');
        return $query->result();
    }

    public function saveData() {
        $this->nama       = $_POST['nama'];
        $this->st_pendapatan  = $_POST['st_pendapatan'];
		if ($_POST['st_pendapatan']=='1'){
			$this->idakun  = $_POST['idakun'];			
		}else{
			$this->idakun  = null;			
		}
        $this->created_by  = $this->session->userdata('user_id');
        $this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mrekap', $this)) {
           $id=$this->db->insert_id();
            return $id;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	
    public function updateData() {
        $this->nama       = $_POST['nama'];
        $this->st_pendapatan  = $_POST['st_pendapatan'];
		if ($_POST['st_pendapatan']=='1'){
			$this->idakun  = $_POST['idakun'];			
		}else{
			$this->idakun  = null;			
		}
        $this->edited_by  = $this->session->userdata('user_id');
        $this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mrekap', $this, ['id' => $_POST['id']])) {
            
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id) {
        $this->status = 0;

        $this->deleted_by  = $this->session->userdata('user_id');
        $this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mrekap', $this, ['id' => $id])) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    
}
