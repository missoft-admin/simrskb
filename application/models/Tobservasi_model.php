<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
class Tobservasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	function get_data_observasi($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='1'){
			$q="SELECT *
			 FROM tranap_observasi 
			 WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND st_ranap='1' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}else{
			$q="SELECT *
			 FROM tranap_observasi 
			 WHERE pendaftaran_id='$pendaftaran_id_ranap' AND st_ranap='0' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function list_template_observasi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_observasi H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function get_data_observasi_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_observasi WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_observasi_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_observasi_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_observasi H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_observasi_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_input as tanggal_input_asal,tanggal_observasi as tanggal_observasi_asal
				,ttd_hubungan as ttd_hubungan_asal,jenis_ttd as jenis_ttd_asal,total_skor_observasi as total_skor_observasi_asal,nama_kajian as nama_kajian_asal,isi_header as isi_header_asal,isi_footer as isi_footer_asal,nilai_tertimbang as nilai_tertimbang_asal,nilai_satuan as nilai_satuan_asal,nama_profile as nama_profile_asal,jk_profile as jk_profile_asal,mengetahui_rs as mengetahui_rs_asal,ttl_profile as ttl_profile_asal,pendidikan_profile as pendidikan_profile_asal,lainnya_profile as lainnya_profile_asal,umur_profile as umur_profile_asal,pekerjaan_profile as pekerjaan_profile_asal,tanggal_observasi as tanggal_observasi_asal,kritik as kritik_asal,hasil_penilaian as hasil_penilaian_asal,nilai_per_unsur as nilai_per_unsur_asal,jumlah_jawaban as jumlah_jawaban_asal,nrr_per_unsur as nrr_per_unsur_asal,nrr_tertimbang as nrr_tertimbang_asal,kepuasan_mas as kepuasan_mas_asal,flag_nilai_mas as flag_nilai_mas_asal,hasil_flag as hasil_flag_asal
				FROM tranap_observasi_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function logic_akses_observasi($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_observasi,H.st_lihat as st_lihat_observasi,H.st_edit as st_edit_observasi,H.st_hapus as st_hapus_observasi,H.st_cetak as st_cetak_observasi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_observasi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_observasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_observasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_observasi'=>'0',
				'st_input_observasi'=>'0',
				'st_edit_observasi'=>'0',
				'st_hapus_observasi'=>'0',
				'st_cetak_observasi'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_observasi(){
		$q="SELECT H.judul_header,H.judul_footer,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date
		
		FROM setting_observasi H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	
}
