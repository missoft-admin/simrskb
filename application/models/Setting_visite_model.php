<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_visite_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_assesmen_setting(){
		$q="SELECT * FROM setting_visite H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function save_assesmen(){
		$id =1;
		$this->st_duplikate = $this->input->post('st_duplikate');
		$this->st_ruangan = $this->input->post('st_ruangan');
		$this->st_kelas = $this->input->post('st_kelas');
		$this->st_diskon = $this->input->post('st_diskon');
		$this->st_kuantitas = $this->input->post('st_kuantitas');
		
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_visite', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


