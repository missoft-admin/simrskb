<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mdata_implan_new_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getKode()
    {
        $this->db->like('kode', '7-', 'after');
        $this->db->from('mdata_implan');
        $query = $this->db->count_all_results();

        if ($query > 0) {
            $autono = $query + 1;
            $autono = '7-'.str_pad($autono, 4, '0', STR_PAD_LEFT);
        } else {
            $autono = '7-'.'0001';
        }

        return $autono;
    }
	public function aktifkan($id)
    {
        $this->status = 1;
		
        if ($this->db->update('mdata_implan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function getKategori()
    {
        // $this->db->where('idtipe', '2');
        // $this->db->where('status', '1');
        // $query = $this->db->get('mdata_kategori');
        // return $query->result();
		$q="SELECT id,`level`,path,
				CASE 
					WHEN nama_L1 IS NOT NULL THEN nama_L1
					WHEN nama_L2 IS NOT NULL THEN nama_L2
					WHEN nama_L3 IS NOT NULL THEN nama_L3
					WHEN nama_L4 IS NOT NULL THEN nama_L4
					WHEN nama_L5 IS NOT NULL THEN nama_L5
					WHEN nama_L6 IS NOT NULL THEN nama_L6
					WHEN nama_L7 IS NOT NULL THEN nama_L7
				  ELSE NULL
				  
				END as nama
				 FROM view_kategori_implan";
			$query=$this->db->query($q);
			return $query->result();
    }
	public function get_array_kategori($path){
		$q="SELECT id FROM view_kategori_implan T1
			where T1.path like '$path%'";
		$query=$this->db->query($q);
		$query=$query->result();
		// print_r($query);exit;
		$array=array();
		foreach($query as $row){
			$array[]=$row->id;
		}
		
		return array_values($array);;
	}
    public function getSatuan()
    {
        $this->db->where('idkategori', '3');
        $this->db->where('status', '1');
        $query = $this->db->get('msatuan');
        return $query->result();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mdata_implan');
        return $query->row();
    }

    public function saveData()
    {
		// print_r($this->input->post());exit();
        $this->kode						= $this->getKode();
        $this->idkategori			= $_POST['idkategori'];
        $this->nama						= $_POST['nama'];
        $this->idsatuan				= $_POST['idsatuan'];
        $this->ppn						= RemoveComma($_POST['ppn']);
        $this->hargabeli			= RemoveComma($_POST['hargabeli']);
        $this->hargadasar			= RemoveComma($_POST['hargadasar']);
        $this->catatan				= $_POST['catatan'];
		$this->idsatuanbesar		= $_POST['idsatuanbesar'];
        $this->hargasatuanbesar		= RemoveComma($_POST['hargasatuanbesar']);
        $this->jumlahsatuanbesar	= RemoveComma($_POST['jumlahsatuanbesar']);
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		
		$this->nama_generik		= $_POST['nama_generik'];
		$this->merk		= $_POST['merk'];
		$this->nama_industri		= $_POST['nama_industri'];
		$this->formularium		= $_POST['formularium'];
		
        if ($this->db->insert('mdata_implan', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	
	function list_data_history($id){
		$q="SELECT H.*
			,MC.`name` as user_created 
			,MC.`name` as user_edited
			FROM mdata_implan_his H
			LEFT JOIN musers MC ON MC.id=H.created_by
			LEFT JOIN musers ME ON ME.id=H.edited_by
			WHERE H.id='$id' ORDER BY H.versi_edit ASC";
		return $this->db->query($q)->result();
	}
    public function updateData()
    {
        $this->kode					  = $_POST['kode'];
        $this->idkategori		  = $_POST['idkategori'];
        $this->nama					  = $_POST['nama'];
        $this->idsatuan				= $_POST['idsatuan'];
        $this->ppn					  = RemoveComma($_POST['ppn']);
        $this->hargabeli		  = RemoveComma($_POST['hargabeli']);
        $this->hargadasar		  = RemoveComma($_POST['hargadasar']);
        $this->catatan			  = $_POST['catatan'];
		$this->idsatuanbesar		= $_POST['idsatuanbesar'];
        $this->hargasatuanbesar		= RemoveComma($_POST['hargasatuanbesar']);
        $this->jumlahsatuanbesar	= RemoveComma($_POST['jumlahsatuanbesar']);
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		
		$this->nama_generik		= $_POST['nama_generik'];
		$this->merk		= $_POST['merk'];
		$this->nama_industri		= $_POST['nama_industri'];
		$this->formularium		= $_POST['formularium'];
		
        if ($this->db->update('mdata_implan', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
		$this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        if ($this->db->update('mdata_implan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
