<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Thonor_approval_model extends CI_Model
{
	public function list_kategori()
	{
		$q = 'SELECT id,nama from mdokter_kategori 
				
				ORDER BY nama ASC';
		$query = $this->db->query($q);
		return $query->result();
	}

	public function saveData($list_trx)
	{
		$userid = $this->session->userdata('user_id');
		// $list_trx=$this->input->post('list_trx');
		$list_logic = '';
		$q = "SELECT *FROM (
			SELECT H.id,H.notransaksi,H.iddokter,H.namadokter,MD.idkategori,MK.nama as kategori 
			,H.nominal,H.tanggal_pembayaran,H.tanggal_jatuhtempo,H.`status`,H.idapproval
			,GROUP_CONCAT(S.id ORDER BY S.id asc) as list_logic
			FROM thonor_dokter H
			LEFT JOIN mdokter MD ON MD.id=H.iddokter
			LEFT JOIN mdokter_kategori MK ON MK.id=MD.idkategori
			LEFT JOIN mlogic_honor S ON MD.idkategori=S.idtipe AND calculate_logic(S.operand, H.nominal, S.nominal)='1'
			WHERE H.id IN (" . $list_trx . ')
			GROUP BY H.id
			) T ORDER BY T.list_logic';
		$row = $this->db->query($q)->result();
		foreach ($row as $r) {
			if ($list_logic != $r->list_logic) {
				//Create Header
				$data_header = [
					'tanggal_pengajuan' => date('Y-m-d'),
					'nominal' => 0,
					'status_approval' => 1,
					'list_logic' => $r->list_logic,
					'created_by' => $userid,
					'created_at' => date('Y-m-d H:i:s'),
				];
				$this->db->insert('thonor_approval_head', $data_header);
				$idhead = $this->db->insert_id();
				$list_logic = $r->list_logic;
				$step = 999;
				$qur = 'SELECT H.step,H.iduser,U.`name` as user_nama,H.proses_setuju,H.proses_tolak,0 as approve
					FROM mlogic_honor H
					LEFT JOIN musers U ON U.id=H.iduser
					WHERE H.id IN (' . $list_logic . ") AND H.`status`='1'
					ORDER BY H.step,H.id";
				$hasil = $this->db->query($qur)->result();
				foreach ($hasil as $app) {
					if ($app->step < $step) {
						$step = $app->step;
					}
					$data_approval = [
						'idhead' => $idhead,
						'step' => $app->step,
						'iduser' => $app->iduser,
						'user_nama' => $app->user_nama,
						'proses_setuju' => $app->proses_setuju,
						'proses_tolak' => $app->proses_tolak,
						'approve' => $app->approve,
					];
					$this->db->insert('thonor_approval', $data_approval);
				}
				$this->db->update('thonor_approval', ['st_aktif' => 1], ['idhead' => $idhead, 'step' => $step]);
			}
			//CREATE DETAIL
			$data_detail = [
				'idhead' => $idhead,
				'idhonor' => $r->id,
				'iddokter' => $r->iddokter,
				'idkategori' => $r->idkategori,
				'namadokter' => $r->namadokter,
				'kategori' => $r->kategori,
				'nominal' => $r->nominal,

			];
			$this->db->insert('thonor_approval_detail', $data_detail);

			// Next Periode Honor Dokter HOLD
			$xidhonor = $r->id;
			// $this->db->query("CALL setNextPeriodeHonorHold($xidhonor)");
		}
		return true;
	}

	public function get_header($id)
	{
		$q = "SELECT *
			FROM `thonor_approval_head` H
			WHERE H.id='$id'
			";
		$query = $this->db->query($q);
		return $query->row_array();
	}

	public function list_bagi_hasil()
	{
		$q = "SELECT id,nama from mbagi_hasil 
				WHERE mbagi_hasil.`status`='1'
				ORDER BY nama ASC";
		$query = $this->db->query($q);
		return $query->result();
	}

	// public function saveData(){
	// $list_trx=$this->input->post('list_trx');
	// $q="SELECT H.mbagi_hasil_id,H.nama_bagi_hasil,SUM(H.pendapatan_bersih_rs) as pendapatan_bersih_rs,SUM(pendapatan_bersih_ps) as pendapatan_bersih_ps,SUM(jumlah_lembar) as jumlah_lembar FROM tbagi_hasil H
	// WHERE H.id IN (".$list_trx.")";
	// $head=$this->db->query($q)->row_array();
	// $head['list_tbagi_hasil_id']=$list_trx;
	// $head['tanggal_trx']=date('Y-m-d H:i:s');
	// $head['created_date']=date('Y-m-d H:i:s');
	// $head['created_by']=$this->session->userdata('user_id');

	// $this->db->insert('tbagi_hasil_bayar_head',$head);

	// $tbagi_hasil_bayar_id= $this->db->insert_id();
	// $id_pemilik_saham=$this->input->post('id_pemilik_saham');
	// $nama_pemilik=$this->input->post('nama_pemilik');
	// $tipe_pemilik=$this->input->post('tipe_pemilik');
	// $total_pendapatan=($this->input->post('total_pendapatan'));
	// $idmetode=($this->input->post('idmetode'));
	// $pemilik_rek_id=($this->input->post('pemilik_rek_id'));
	// $tanggal_bayar=($this->input->post('tanggal_bayar'));
	// $jumlah_lembar=($this->input->post('jumlah_lembar'));
	// foreach($id_pemilik_saham as $index => $val){
	// $rek=getRekeningPS_ID($pemilik_rek_id[$index]);
	// // if ($rek){}else{$rek[]=(object) array('idbank'=>null,'bank'=>null,'norek'=>null,'atas_nama'=>null);}
	// $detail=array(
	// 'tbagi_hasil_bayar_id'=>$tbagi_hasil_bayar_id,
	// 'id_pemilik_saham'=>$id_pemilik_saham[$index],
	// 'nama_pemilik'=>$nama_pemilik[$index],
	// 'tipe_pemilik'=>$tipe_pemilik[$index],
	// 'tipe_pemilik_nama'=>GetTipePemilikSaham($tipe_pemilik[$index]),
	// 'total_pendapatan'=>RemoveComma($total_pendapatan[$index]),
	// 'idmetode'=>$idmetode[$index],
	// 'jumlah_lembar'=>$jumlah_lembar[$index],
	// 'pemilik_rek_id'=>$pemilik_rek_id[$index],
	// 'idbank'=>($rek)?$rek->idbank:null,
	// 'bank'=>($rek)?$rek->bank:null,
	// 'norek'=>($rek)?$rek->norek:null,
	// 'atas_nama'=>($rek)?$rek->atas_nama:null,
	// 'tanggal_bayar'=>YMDFormat($tanggal_bayar[$index]),
	// );
	// $result=$this->db->insert('tbagi_hasil_bayar_detail',$detail);
	// }
	// $arr=explode(',',$list_trx);
	// foreach($arr as $index => $val){
	// $data=array(
	// 'tbagi_hasil_bayar_id'=>$tbagi_hasil_bayar_id,
	// 'tbagi_hasil_id'=>$val,
	// );
	// $result=$this->db->insert('tbagi_hasil_bayar_head_trx',$data);
	// }

	// return $result;
	// }
	public function updateData()
	{
		$id = $this->input->post('id');
		$list_trx = $this->input->post('list_trx');
		$q = 'SELECT H.mbagi_hasil_id,H.nama_bagi_hasil,SUM(H.pendapatan_bersih_rs) as pendapatan_bersih_rs,SUM(pendapatan_bersih_ps) as pendapatan_bersih_ps,SUM(jumlah_lembar) as jumlah_lembar FROM tbagi_hasil H
			WHERE H.id IN (' . $list_trx . ')';
		$head = $this->db->query($q)->row_array();
		$head['list_tbagi_hasil_id'] = $list_trx;
		// $head['tanggal_trx']=date('Y-m-d H:i:s');
		$head['edited_date'] = date('Y-m-d H:i:s');
		$head['edited_by'] = $this->session->userdata('user_id');

		$this->db->where('id', $id);
		$this->db->update('tbagi_hasil_bayar_head', $head);

		$tbagi_hasil_bayar_id = $id;

		// $this->db->where('tbagi_hasil_bayar_id',$tbagi_hasil_bayar_id);
		// $this->db->delete('tbagi_hasil_bayar_detail');
		$this->db->where('tbagi_hasil_bayar_id', $tbagi_hasil_bayar_id);
		$this->db->delete('tbagi_hasil_bayar_head_trx');

		$id_pemilik_saham = $this->input->post('id_pemilik_saham');
		$nama_pemilik = $this->input->post('nama_pemilik');
		$tipe_pemilik = $this->input->post('tipe_pemilik');
		$total_pendapatan = ($this->input->post('total_pendapatan'));
		$idmetode = ($this->input->post('idmetode'));
		$pemilik_rek_id = ($this->input->post('pemilik_rek_id'));
		$tanggal_bayar = ($this->input->post('tanggal_bayar'));
		$jumlah_lembar = ($this->input->post('jumlah_lembar'));
		foreach ($id_pemilik_saham as $index => $val) {
			$rek = getRekeningPS_ID($pemilik_rek_id[$index]);
			// if ($rek){}else{$rek[]=(object) array('idbank'=>null,'bank'=>null,'norek'=>null,'atas_nama'=>null);}
			$detail = [
				'tbagi_hasil_bayar_id' => $tbagi_hasil_bayar_id,
				'id_pemilik_saham' => $id_pemilik_saham[$index],
				'nama_pemilik' => $nama_pemilik[$index],
				'tipe_pemilik' => $tipe_pemilik[$index],
				'tipe_pemilik_nama' => GetTipePemilikSaham($tipe_pemilik[$index]),
				'total_pendapatan' => RemoveComma($total_pendapatan[$index]),
				'idmetode' => $idmetode[$index],
				'jumlah_lembar' => $jumlah_lembar[$index],
				'pemilik_rek_id' => $pemilik_rek_id[$index],
				'idbank' => ($rek) ? $rek->idbank : null,
				'bank' => ($rek) ? $rek->bank : null,
				'norek' => ($rek) ? $rek->norek : null,
				'atas_nama' => ($rek) ? $rek->atas_nama : null,
				'tanggal_bayar' => YMDFormat($tanggal_bayar[$index]),
			];
			$result = $this->db->insert('tbagi_hasil_bayar_detail', $detail);
		}
		// print_r($detail);exit();

		$arr = explode(',', $list_trx);
		foreach ($arr as $index => $val) {
			$data = [
				'tbagi_hasil_bayar_id' => $tbagi_hasil_bayar_id,
				'tbagi_hasil_id' => $val,
			];
			$result = $this->db->insert('tbagi_hasil_bayar_head_trx', $data);
		}

		return $result;
	}

	public function detail($id)
	{
		$q = "SELECT * from tbagi_hasil 
				WHERE tbagi_hasil.`id`='$id'
				";
		$query = $this->db->query($q);
		return $query->row_array();
	}

	public function simpan_proses_peretujuan($id)
	{
		$q = "SELECT S.id,S.step,S.proses_setuju,S.proses_tolak,U.`name` as user_nama,S.iduser FROM tbagi_hasil_bayar_head H
			LEFT JOIN mbagi_hasil_logic S ON S.mbagi_hasil_id=H.mbagi_hasil_id AND S.`status`='1'
			LEFT JOIN musers U ON U.id=S.iduser
			WHERE H.id='$id'  AND calculate_logic(S.operand,H.nominal_dibagikan,S.nominal) = 1
			ORDER BY S.step ASC,S.id ASC";
		$list_user = $this->db->query($q)->result();
		$step = 999;
		$idlogic = '';
		foreach ($list_user as $row) {
			if ($row->step < $step) {
				$step = $row->step;
			}
			$data = [
				'tbagi_hasil_bayar_id' => $id,
				'step' => $row->step,
				// 'idlogic' => $row->idlogic,
				'iduser' => $row->iduser,
				'user_nama' => $row->user_nama,
				'proses_setuju' => $row->proses_setuju,
				'proses_tolak' => $row->proses_tolak,
				'approve' => 0,
			];
			// $idlogic=$row->idlogic;
			$this->db->insert('tbagi_hasil_bayar_approval', $data);
		}

		$this->db->update('tbagi_hasil_bayar_head', ['status' => 2], ['id' => $id]);
		return $this->db->update('tbagi_hasil_bayar_approval', ['st_aktif' => 1], ['tbagi_hasil_bayar_id' => $id, 'step' => $step]);

		// $data=array(
		// 'idlogic' => $idlogic,
		// 'status' => '2',
		// 'proses_by' => $this->session->userdata('user_id'),
		// 'proses_nama' => $this->session->userdata('user_name'),
		// 'proses_date' => date('Y-m-d H:i:s'),
		// );
		// $this->db->where('id',$id);
		return $this->db->update('rka_pengajuan', $data);
	}

	public function getHeadDokumenUpload($idapproval)
	{
		$this->db->select('id, noreg AS notransaksi, tanggal_pengajuan');
		$this->db->where('id', $idapproval);
		$query = $this->db->get('thonor_approval_head');
		return $query->row();
	}

	public function getListUploadedDocument($idapproval)
	{
		$this->db->where('idapproval', $idapproval);
		$query = $this->db->get('thonor_approval_dokumen');
		return $query->result();
	}
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
