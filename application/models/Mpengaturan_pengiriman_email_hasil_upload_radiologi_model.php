<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpengaturan_pengiriman_email_hasil_upload_radiologi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('merm_pengaturan_pengiriman_email_hasil_upload_radiologi');
        return $query->row();
    }

    public function getPengaturanBccEmail($id)
    {
        $this->db->select('email');
        $this->db->where('pengaturan_id', $id);
        $query = $this->db->get('merm_pengaturan_pengiriman_email_hasil_upload_radiologi_bcc');
        $result_array = $query->result_array();

        return array_column($result_array, 'email');
    }

    public function saveData()
    {
        $pengaturan_id = 1;

        $this->judul = $_POST['judul'];
        $this->body = $_POST['body'];
        $this->footer_pengirim = $_POST['footer_pengirim'];
        $this->footer_email = $_POST['footer_email'];

        $this->db->where('id', $pengaturan_id);
        if ($this->db->update('merm_pengaturan_pengiriman_email_hasil_upload_radiologi', $this)) {
            
            $this->db->where('pengaturan_id', $pengaturan_id);
            $this->db->delete('merm_pengaturan_pengiriman_email_hasil_upload_radiologi_bcc');

            $bccEmail = explode(',', $_POST['bcc_email']);
            foreach ($bccEmail as $email) {
                $data['pengaturan_id'] = $pengaturan_id;
                $data['email'] = $email;
                $this->db->insert('merm_pengaturan_pengiriman_email_hasil_upload_radiologi_bcc', $data);
            }
            
            return true;
        }

        return false;
    }
}
