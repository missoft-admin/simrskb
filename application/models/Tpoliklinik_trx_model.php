<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpoliklinik_trx_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function get_logic_diskon_pelayanan($idkelompokpasien='5',$idrekanan='0',$idtipe='1',$idpoliklinik='1',$statuspasienbaru='0',$pertemuan_id='1',$head_parent='2783',$pelayanan_id='2795'){
		$statuspasienbaru=($statuspasienbaru=='0'?'2':$statuspasienbaru);
		$idrekanan=($idkelompokpasien!='1'?'0':$idrekanan);
		$q="
			SELECT H.id,H.tipe_diskon_jasapelayanan,H.status_diskon_jasapelayanan,H.diskon_jasapelayanan FROM merm_pengaturan_diskon_rajal_pelayanan H
			INNER JOIN 
			(
			SELECT 
			compare_value_9(
			MAX(IF(H.tarif_pelayanan = '$pelayanan_id' AND H.head_parent_tarif='$head_parent' AND H.kasus='$pertemuan_id' AND H.status_pasien='$statuspasienbaru' AND (H.status_tujuan_poliklinik=1 AND H.tujuan_poliklinik='$idpoliklinik') AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND H.idrekanan='$idrekanan',H.id,NULL))
			,MAX(IF(H.tarif_pelayanan = '$pelayanan_id' AND (H.kasus='$pertemuan_id' OR H.kasus='0') AND (H.status_pasien='$statuspasienbaru' OR H.status_pasien='0') AND (H.status_tujuan_poliklinik IN (0,1) AND H.tujuan_poliklinik IN ('$idpoliklinik',0)) AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND (H.idrekanan='$idrekanan' OR H.idrekanan='0'),H.id,NULL))
			,MAX(IF(H.tarif_pelayanan = '0' AND H.head_parent_tarif='$head_parent' AND H.kasus='$pertemuan_id' AND H.status_pasien='$statuspasienbaru' AND (H.status_tujuan_poliklinik=1 AND H.tujuan_poliklinik='$idpoliklinik') AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0' AND H.head_parent_tarif='$head_parent' AND (H.kasus='$pertemuan_id' OR H.kasus='0') AND (H.status_pasien='$statuspasienbaru' OR H.status_pasien='0') AND (H.status_tujuan_poliklinik IN (0,1) AND H.tujuan_poliklinik IN ('$idpoliklinik',0)) AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND (H.idrekanan='$idrekanan' OR H.idrekanan='0'),H.id,NULL))
			,MAX(IF(H.tarif_pelayanan = '0' AND H.head_parent_tarif='0' AND H.kasus='$pertemuan_id' AND H.status_pasien='$statuspasienbaru' AND (H.status_tujuan_poliklinik=1 AND H.tujuan_poliklinik='$idpoliklinik') AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0' AND H.head_parent_tarif='0' AND H.kasus='0' AND H.status_pasien='$statuspasienbaru' AND (H.status_tujuan_poliklinik=1 AND H.tujuan_poliklinik='$idpoliklinik') AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0' AND H.head_parent_tarif='0' AND H.kasus='0' AND H.status_pasien='0' AND (H.status_tujuan_poliklinik=1 AND H.tujuan_poliklinik='$idpoliklinik') AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0' AND H.head_parent_tarif='0' AND H.kasus='0' AND H.status_pasien='0' AND (H.status_tujuan_poliklinik=0) AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0' AND H.head_parent_tarif='0' AND H.kasus='0' AND H.status_pasien='0' AND (H.status_tujuan_poliklinik=0) AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND H.idrekanan='0',H.id,NULL)) 

			) as id
			FROM merm_pengaturan_diskon_rajal_pelayanan H			
			WHERE H.idkelompokpasien='$idkelompokpasien'
			) T ON T.id=H.id
		";
		// print_r();exi
		$hasil=$this->db->query($q)->row();
		return $hasil;
	}
	function get_logic_diskon_pelayanan_ri($idkelompokpasien='5',$idrekanan='0',$idtipe='1',$idruangan='1',$idkelas='0',$headparent,$pelayanan_id='2795'){
		$q="
			SELECT H.id,H.tipe_diskon_jasapelayanan,H.status_diskon_jasapelayanan,H.diskon_jasapelayanan FROM merm_pengaturan_diskon_ranap_pelayanan H
			INNER JOIN 
			(
			SELECT 
			compare_value_9(
			MAX(IF(H.tarif_pelayanan = '$pelayanan_id'  AND H.idkelas='$idkelas' AND ( H.idruangan='$idruangan')   AND H.idrekanan='$idrekanan',H.id,NULL))
			,MAX(IF(H.tarif_pelayanan = '$pelayanan_id' AND (H.idkelas='$idkelas' OR H.idkelas='0') AND (H.idruangan IN ('$idruangan',0))   AND (H.idrekanan='$idrekanan' OR H.idrekanan='0'),H.id,NULL))
			,MAX(IF(H.tarif_pelayanan = '0'  AND H.idkelas='$idkelas' AND ( H.idruangan='$idruangan')   AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0'  AND (H.idkelas='$idkelas' OR H.idkelas='0') AND (H.idruangan IN ('$idruangan',0))   AND (H.idrekanan='$idrekanan' OR H.idrekanan='0'),H.id,NULL))
			,MAX(IF(H.tarif_pelayanan = '0'  AND H.idkelas='$idkelas' AND ( H.idruangan='$idruangan')   AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0'  AND H.idkelas='$idkelas' AND ( H.idruangan='$idruangan')   AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0'  AND H.idkelas='0' AND ( H.idruangan='$idruangan')   AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0'  AND H.idkelas='0'  AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0'  AND H.idkelas='0'  AND H.idrekanan='0',H.id,NULL)) 

			) as id
			FROM merm_pengaturan_diskon_ranap_pelayanan H			
			WHERE H.idkelompokpasien='$idkelompokpasien'
			) T ON T.id=H.id
		";
		// print_r();exi
		$hasil=$this->db->query($q)->row();
		return $hasil;
	}
	function get_logic_diskon_visite($idkelompokpasien='5',$idrekanan='0',$idtipe='1',$idruangan='1',$idkelas='0',$pelayanan_id='2795'){
		$idkelas=($idkelas=='0'?'2':$idkelas);
		$idrekanan=($idkelompokpasien!='1'?'0':$idrekanan);
		$q="
			SELECT H.id,H.tipe_diskon_jasapelayanan,H.status_diskon_jasapelayanan,H.diskon_jasapelayanan FROM merm_pengaturan_diskon_ranap_visite H
			INNER JOIN 
			(
			SELECT 
			compare_value_9(
			MAX(IF(H.tarif_pelayanan = '$pelayanan_id'  AND H.idkelas='$idkelas' AND ( H.idruangan='$idruangan')   AND H.idrekanan='$idrekanan',H.id,NULL))
			,MAX(IF(H.tarif_pelayanan = '$pelayanan_id' AND (H.idkelas='$idkelas' OR H.idkelas='0') AND (H.idruangan IN ('$idruangan',0))   AND (H.idrekanan='$idrekanan' OR H.idrekanan='0'),H.id,NULL))
			,MAX(IF(H.tarif_pelayanan = '0'  AND H.idkelas='$idkelas' AND ( H.idruangan='$idruangan')   AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0'  AND (H.idkelas='$idkelas' OR H.idkelas='0') AND (H.idruangan IN ('$idruangan',0))   AND (H.idrekanan='$idrekanan' OR H.idrekanan='0'),H.id,NULL))
			,MAX(IF(H.tarif_pelayanan = '0'  AND H.idkelas='$idkelas' AND ( H.idruangan='$idruangan')   AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0'  AND H.idkelas='$idkelas' AND ( H.idruangan='$idruangan')   AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0'  AND H.idkelas='0' AND ( H.idruangan='$idruangan')   AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0'  AND H.idkelas='0'  AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0'  AND H.idkelas='0'  AND H.idrekanan='0',H.id,NULL)) 

			) as id
			FROM merm_pengaturan_diskon_ranap_visite H			
			WHERE H.idkelompokpasien='$idkelompokpasien'
			) T ON T.id=H.id
		";
		// print_r($q);exit;
		$hasil=$this->db->query($q)->row();
		return $hasil;
	}
	function get_logic_diskon_pelayanan_fisio($idkelompokpasien='5',$idrekanan='0',$idtipe='1',$idpoliklinik='1',$statuspasienbaru='0',$pertemuan_id='1',$head_parent='2783',$pelayanan_id='2795'){
		$statuspasienbaru=($statuspasienbaru=='0'?'2':$statuspasienbaru);
		$idrekanan=($idkelompokpasien!='1'?'0':$idrekanan);
		$q="
			SELECT H.id,H.tipe_diskon_jasapelayanan,H.status_diskon_jasapelayanan,H.diskon_jasapelayanan FROM merm_pengaturan_diskon_rajal_fisioterapi H
			INNER JOIN 
			(
			SELECT 
			compare_value_10(
			MAX(IF(H.tarif_pelayanan = '$pelayanan_id' AND H.head_parent_tarif='$head_parent' AND H.kasus='$pertemuan_id' AND H.status_pasien='$statuspasienbaru' AND (H.status_tujuan_poliklinik=1 AND H.tujuan_poliklinik='$idpoliklinik') AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND H.idrekanan='$idrekanan',H.id,NULL))
			,MAX(IF(H.tarif_pelayanan = '$pelayanan_id' AND (H.kasus='$pertemuan_id' OR H.kasus='0') AND (H.status_pasien='$statuspasienbaru' OR H.status_pasien='0') AND (H.status_tujuan_poliklinik IN (0,1) AND H.tujuan_poliklinik IN ('$idpoliklinik',0)) AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND (H.idrekanan='$idrekanan' OR H.idrekanan='0'),H.id,NULL))
			,MAX(IF(H.tarif_pelayanan = '0' AND H.head_parent_tarif='$head_parent' AND H.kasus='$pertemuan_id' AND H.status_pasien='$statuspasienbaru' AND (H.status_tujuan_poliklinik=1 AND H.tujuan_poliklinik='$idpoliklinik') AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0' AND H.head_parent_tarif='$head_parent' AND (H.kasus='$pertemuan_id' OR H.kasus='0') AND (H.status_pasien='$statuspasienbaru' OR H.status_pasien='0') AND (H.status_tujuan_poliklinik IN (0,1) AND H.tujuan_poliklinik IN ('$idpoliklinik',0)) AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND (H.idrekanan='$idrekanan' OR H.idrekanan='0'),H.id,NULL))
			,MAX(IF(H.tarif_pelayanan = '0' AND H.head_parent_tarif='0' AND H.kasus='$pertemuan_id' AND H.status_pasien='$statuspasienbaru' AND (H.status_tujuan_poliklinik=1 AND H.tujuan_poliklinik='$idpoliklinik') AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0' AND H.head_parent_tarif='0' AND H.kasus='0' AND H.status_pasien='$statuspasienbaru' AND (H.status_tujuan_poliklinik=1 AND H.tujuan_poliklinik='$idpoliklinik') AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0' AND H.head_parent_tarif='0' AND H.kasus='0' AND H.status_pasien='0' AND (H.status_tujuan_poliklinik=1 AND H.tujuan_poliklinik='$idpoliklinik') AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0' AND H.head_parent_tarif='0' AND H.kasus='0' AND H.status_pasien='0' AND (H.status_tujuan_poliklinik=0) AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND H.idrekanan='$idrekanan',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '0' AND H.head_parent_tarif='0' AND H.kasus='0' AND H.status_pasien='0' AND (H.status_tujuan_poliklinik=0) AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND H.idrekanan='0',H.id,NULL)) 
			,MAX(IF(H.tarif_pelayanan = '$pelayanan_id' AND H.head_parent_tarif='0' AND H.kasus='0' AND H.status_pasien='0' AND (H.status_tujuan_poliklinik=0) AND (H.tujuan_pasien='$idtipe' OR H.tujuan_pasien='0')  AND H.idrekanan='0',H.id,NULL)) 

			) as id
			FROM merm_pengaturan_diskon_rajal_fisioterapi H			
			WHERE H.idkelompokpasien='$idkelompokpasien'
			) T ON T.id=H.id
		";
		// print_r($q);exit;
		$hasil=$this->db->query($q)->row();
		return $hasil;
	}
	public function getListUnitPelayanan()
	{
		$this->db->select('munitpelayanan.id, munitpelayanan.nama');
		$this->db->where('munitpelayanan_user.userid', $this->session->userdata('user_id'));
		$this->db->join('munitpelayanan', 'munitpelayanan_user.idunitpelayanan = munitpelayanan.id', 'left');
		$this->db->from('munitpelayanan_user');
		$query = $this->db->get();
		return $query->result();
	}
	public function getListTipe()
	{
		$iduser=$this->session->userdata('user_id');
		$q="SELECT H.idtipe as id,M.nama_tipe  as nama
		FROM musers_tipebarang H 
		INNER JOIN mdata_tipebarang M ON M.id=H.idtipe
		WHERE H.iduser='$iduser' AND M.id NOT IN (4)";
		$query=$this->db->query($q);
		return $query->result();
	}
	public function getListTipeAll()
	{
		$iduser=$this->session->userdata('user_id');
		$q="SELECT H.idtipe as id,M.nama_tipe  as nama
		FROM musers_tipebarang H 
		INNER JOIN mdata_tipebarang M ON M.id=H.idtipe
		WHERE H.iduser='$iduser' ";
		$query=$this->db->query($q);
		return $query->result();
	}
	public function list_ppa($login_profesi_id)
	{
		$iduser=$this->session->userdata('user_id');
		$q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='$login_profesi_id' AND H.staktif='1'";
		$query=$this->db->query($q);
		return $query->result();
	}
	public function list_ppa_all()
	{
		$iduser=$this->session->userdata('user_id');
		$q="SELECT *FROM mppa H WHERE H.staktif='1'";
		$query=$this->db->query($q);
		return $query->result();
	}
	public function get_obat($searchText)
    {
        $this->db->select("view_barang_farmasi.*,mgudang_stok.stok");
        $this->db->from('view_barang_farmasi');
        $this->db->join('mgudang_stok', 'mgudang_stok.idbarang=view_barang_farmasi.id AND mgudang_stok.idunitpelayanan=1 AND mgudang_stok.idtipe=view_barang_farmasi.idtipe');
        $this->db->like('view_barang_farmasi.nama', $searchText);
        $this->db->or_like('view_barang_farmasi.kode', $searchText);
        $query = $this->db->get();
        // print_r($this->db->last_query());exit();
        return $query->result_array();
    }
	public function getDefaultUnitPelayananUser()
	{
		$this->db->select('unitpelayananiddefault as idunitpelayanan');
		$this->db->from('musers');
		$this->db->where('id', $this->session->userdata('user_id'));
		$query = $this->db->get();
		return $query->row('idunitpelayanan');
	}
	public function get_obat_detail($id, $kelompok_pasien=0, $idtipe,$idunit)
    {

        $q="SELECT view_barang_farmasi.*, msatuan.singkatan, msatuan.nama as nama_satuan,(CASE
				WHEN $kelompok_pasien = 5 THEN
					marginumum
				WHEN $kelompok_pasien = 0 THEN
					marginumum
				WHEN $kelompok_pasien = 1 THEN
					marginasuransi
				WHEN $kelompok_pasien = 2 THEN
					marginjasaraharja
				WHEN $kelompok_pasien = 3 THEN
					marginbpjskesehatan
				WHEN $kelompok_pasien = 4 THEN
					marginbpjstenagakerja
			END) AS margin,mgudang_stok.stok
			 FROM view_barang_farmasi
			 LEFT JOIN msatuan ON msatuan.id=view_barang_farmasi.idsatuankecil
			 LEFT JOIN mgudang_stok ON mgudang_stok.idbarang = view_barang_farmasi.id AND mgudang_stok.idtipe=view_barang_farmasi.idtipe AND idunitpelayanan='$idunit'
			 WHERE view_barang_farmasi.id = '$id' AND view_barang_farmasi.idtipe='$idtipe'";
        $query=$this->db->query($q);
		// print_r($q);exit;
        return $query->row();
    }
	function data_paket(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT H.id as paket_id,H.nama_paket,H.status_paket,H.st_edited FROM mpaket H WHERE H.created_by='$user_id' AND H.status_paket='1' AND H.staktif='1' LIMIT 1";
		return $this->db->query($q)->row_array();
	}
	function data_paket_layanan(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT H.id as paket_id,H.nama_paket,H.status_paket,H.st_edited FROM mpaket_layanan H WHERE H.created_by='$user_id' AND H.status_paket='1' AND H.staktif='1' LIMIT 1";
		return $this->db->query($q)->row_array();
	}
	function data_paket_layanan_fisio(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT H.id as paket_id,H.nama_paket,H.status_paket,H.st_edited FROM mpaket_fisio H WHERE H.created_by='$user_id' AND H.status_paket='1' LIMIT 1";
		return $this->db->query($q)->row_array();
	}
	function list_header_path($idtipe,$idkelompokpasien,$idrekanan=0){
		$idrekanan=($idkelompokpasien==1?$idrekanan:0);
		$q="SELECT *FROM (
			SELECT  M.id as kp,0 as idrekanan,M.trawatjalan,M.tigd FROM mpasien_kelompok M
			UNION ALL
			SELECT 1 as kp,M.id as idrekanan,M.trawatjalan,M.tigd FROM mrekanan M WHERE (M.trawatjalan > 0 AND M.tigd > 0) 
			) T WHERE T.kp='$idkelompokpasien' AND (T.idrekanan='0' OR T.idrekanan='$idrekanan')

			ORDER BY T.idrekanan DESC";
		$row=$this->db->query($q)->row();
		$idlayanan='';
		if ($idtipe=='1'){
			$idlayanan=$row->trawatjalan;
		}else{
			$idlayanan=$row->tigd;
		}
		// print_r($row);exit;
		$q="SELECT *FROM mtarif_rawatjalan WHERE id='$idlayanan'";
		return $this->db->query($q)->result();
	}
	function list_header_path_fisio($idtipe,$idkelompokpasien,$idrekanan=0){
		$idrekanan=($idkelompokpasien==1?$idrekanan:0);
		$q="SELECT *FROM (
			SELECT  M.id as kp,0 as idrekanan,M.tfisioterapi FROM mpasien_kelompok M
			UNION ALL
			SELECT 1 as kp,M.id as idrekanan,M.tfisioterapi FROM mrekanan M WHERE (M.tfisioterapi > 0 ) 
			) T WHERE T.kp='$idkelompokpasien' AND (T.idrekanan='0' OR T.idrekanan='$idrekanan')

			ORDER BY T.idrekanan DESC";
		$row=$this->db->query($q)->row();
		$idlayanan='';
			$idlayanan=$row->tfisioterapi;
		// print_r($row);exit;
		$q="SELECT *FROM mtarif_fisioterapi WHERE id='$idlayanan'";
		return $this->db->query($q)->result();
	}
}
