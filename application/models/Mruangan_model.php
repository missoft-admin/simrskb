<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mruangan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mruangan');
        return $query->row();
    }

    public function saveData()
    {
        $this->nama 					= $_POST['nama'];
        $this->idtipe 			  = $_POST['idtipe'];
        $this->status 			  = 1;
        $this->st_tampil 			  = 1;
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
        if ($this->db->insert('mruangan', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama 					= $_POST['nama'];
        $this->idtipe 				= $_POST['idtipe'];
        $this->st_tampil 				= $_POST['st_tampil'];
        $this->status 			  = 1;
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mruangan', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mruangan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
