<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mrka_kegiatan_setting_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getSpecified_idrka_kegiatan($id,$bulan) {
        $q="SELECT  rka.periode,N.nominal,N.bulan,H.*,M.nama as nama_kegiatan 
			FROM mrka_kegiatan H
			LEFT JOIN mkegiatan_rka M ON M.id=H.idkegiatan
			LEFT JOIN mrka_kegiatan_nominal N ON N.idrka_kegiatan=H.id AND N.bulan='$bulan'
			LEFT JOIN mrka rka ON rka.id=N.idrka
			WHERE H.id='$id'";
        return $this->db->query($q)->row();
    }
	public function save(){
		$idrka_kegiatan=$this->input->post('idrka_kegiatan');
		$idrka=$this->input->post('idrka');
		$idkegiatan=$this->input->post('idkegiatan');
		$bulan=$this->input->post('bulan');
		$idvendor=$this->input->post('idvendor');
		$tanggal_bayar=$this->input->post('tanggal_bayar');
		// $tanggal_bayar=''
		
		$xnama_barang=$this->input->post('xnama_barang');
		$xmerk_barang=$this->input->post('xmerk_barang');
		$xkuantitas=$this->input->post('xkuantitas');
		$xsatuan=$this->input->post('xsatuan');
		$xharga_satuan=$this->input->post('xharga_satuan');
		$xtotal_harga=$this->input->post('xtotal_harga');
		$xketerangan=$this->input->post('xketerangan');
		$xstatus=$this->input->post('xstatus');
		$this->db->delete('mrka_kegiatan_detail',array('idrka_kegiatan'=>$idrka_kegiatan,'bulan'=>$bulan,'jenis_setting'=>'2'));
		foreach($xnama_barang as $index => $value){
			$data_detail=array(
				'idrka_kegiatan' =>$idrka_kegiatan,
				'idrka' =>$idrka,
				'idkegiatan' =>$idkegiatan,
				'idvendor' =>$idvendor,
				'bulan' =>$bulan,
				'tanggal_bayar' =>YMDFormat($tanggal_bayar),
				'jenis_setting' =>'2',
				'nama_barang' =>$xnama_barang[$index],
				'merk_barang' =>$xmerk_barang[$index],
				'kuantitas' =>$xkuantitas[$index],
				'satuan' => RemoveComma($xsatuan[$index]),
				'harga_satuan' => RemoveComma($xharga_satuan[$index]),
				'total_harga' => RemoveComma($xtotal_harga[$index]),
				'keterangan' =>$xketerangan[$index],
			);
			$this->db->insert('mrka_kegiatan_detail',$data_detail);
		}
		$this->db->update('mrka_kegiatan_nominal',array('st_setting'=>'2'),array('idrka_kegiatan'=>$idrka_kegiatan,'bulan'=>$bulan));
		return true;
	}
}
