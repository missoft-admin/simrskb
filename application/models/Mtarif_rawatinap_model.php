<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mtarif_rawatinap_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllRuangan()
    {
        $this->db->where('status', '1');
        $query = $this->db->get('mruangan');
        return $query->result();
    }

    public function getAllParent()
    {
        $this->db->where('status', '1');
        $this->db->order_by('path', 'ASC');
        $query = $this->db->get('mtarif_rawatinap');
        return $query->result();
    }

    public function getAllTarif($id)
    {
        $this->db->where('idtarif', $id);
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_rawatinap_detail');
        return $query->result();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mtarif_rawatinap');
        return $query->row();
    }

    public function saveData()
    {
        $this->idruangan       = $_POST['idruangan'];
        $this->idtipe          = $_POST['idtipe'];
        $this->nama            = $_POST['nama'];
        $this->idkelompok      = $_POST['idkelompok'];
        $this->headerpath      = $_POST['headerpath'];
        $this->path            = $_POST['path'];
        $this->level           = $_POST['level'];
        $this->status          = 1;
    		$this->created_by  = $this->session->userdata('user_id');
    		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mtarif_rawatinap', $this)) {
            $idtarif = $this->db->insert_id();
            $tariflist = json_decode($_POST['totaltarif_value']);
            foreach ($tariflist as $row) {
                $detail = array();
                $detail['idtarif']              = $idtarif;
                $detail['kelas']                = $row[1];
                $detail['jasasarana']           = RemoveComma($row[2]);
                $detail['group_jasasarana']     = $row[3];
                $detail['jasapelayanan']        = RemoveComma($row[4]);
                $detail['group_jasapelayanan']  = $row[5];
                $detail['bhp']                  = RemoveComma($row[6]);
                $detail['group_bhp']            = $row[7];
                $detail['biayaperawatan']       = RemoveComma($row[8]);
                $detail['group_biayaperawatan'] = $row[9];
                $detail['total']                = RemoveComma($row[10]);
                $detail['status']               = '1';

                $this->db->insert('mtarif_rawatinap_detail', $detail);
            }
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->idruangan       = $_POST['idruangan'];
        $this->idtipe          = $_POST['idtipe'];
        $this->nama            = $_POST['nama'];
        $this->idkelompok      = $_POST['idkelompok'];
        $this->status          = 1;
    		$this->edited_by  = $this->session->userdata('user_id');
    		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mtarif_rawatinap', $this, array('id' => $_POST['id']))) {
            $idtarif = $_POST['id'];
            $tariflist = json_decode($_POST['totaltarif_value']);

            $this->db->where('idtarif', $idtarif);
            if ($this->db->delete('mtarif_rawatinap_detail')) {
                foreach ($tariflist as $row) {
                    $detail = array();
                    $detail['idtarif']              = $idtarif;
                    $detail['kelas']                = $row[1];
                    $detail['jasasarana']           = RemoveComma($row[2]);
                    $detail['group_jasasarana']     = $row[3];
                    $detail['jasapelayanan']        = RemoveComma($row[4]);
                    $detail['group_jasapelayanan']  = $row[5];
                    $detail['bhp']                  = RemoveComma($row[6]);
                    $detail['group_bhp']            = $row[7];
                    $detail['biayaperawatan']       = RemoveComma($row[8]);
                    $detail['group_biayaperawatan'] = $row[9];
                    $detail['total']                = RemoveComma($row[10]);
                    $detail['status']               = '1';

                    $this->db->insert('mtarif_rawatinap_detail', $detail);
                }
            }

            // IF CHANGE PARENT
            if ($_POST['headerpath'] != $_POST['old_headerpath']) {
                $this->db->query("call updateTarifRawatInap('".$_POST['id']."','".$_POST['headerpath']."')");
            }
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
        return true;
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
        if ($this->db->update('mtarif_rawatinap', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function getParent($idtipe, $idruangan)
    {
        # Get ID Root
        $this->db->select('MAX(CAST(headerpath AS SIGNED)) + 1 AS idroot');
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_rawatinap');
        $idroot = $query->row()->idroot;

        # Get Option
        $this->db->where('idtipe', $idtipe);
        $this->db->where('idruangan', $idruangan);
        $this->db->where('status', '1');
        $this->db->order_by('path', 'ASC');
        $query = $this->db->get('mtarif_rawatinap');
        $result = $query->result();

        $data = '<option value="'.($idroot ? $idroot:1).'">Root</option>';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                $data .= '<option value="'.$row->path.'">'.TreeView($row->level, $row->nama).'</option>';
            }
        }
        return $data;
    }

    public function getPathLevel($headerpath)
    {
        $strquery = "SELECT LEVEL + 1 AS level, CASE WHEN ( SELECT COUNT(headerpath) FROM mtarif_rawatinap WHERE headerpath = t1.headerpath ) = 0 THEN CONCAT(t1.path, '.1') ELSE  CONCAT(t1.path,'.',  IF ( ( SELECT COUNT(headerpath) FROM mtarif_rawatinap WHERE headerpath = t1.path ) = 0, ( SELECT COUNT(headerpath) FROM mtarif_rawatinap WHERE headerpath = t1.path ) + 1, IF ( LENGTH(t1.path) = '1',( SELECT COUNT(headerpath) FROM mtarif_rawatinap WHERE headerpath = t1.path ), ( SELECT COUNT(headerpath) FROM mtarif_rawatinap WHERE headerpath = t1.path ) + 1 ) ) ) END AS path FROM mtarif_rawatinap t1 WHERE t1.path = '".$headerpath."'";
        $query = $this->db->query($strquery);
        $row = $query->row();

        if ($query->num_rows() > 0) {
            $data['path']  = $row->path;
            $data['level'] = $row->level;
        } else {
            $data['path']  = 1;
            $data['level'] = 0;
        }
        return $data;
    }

    public function updateSetting()
    {
        $settingList = json_decode($_POST['setting_value']);
        foreach ($settingList as $row) {
            $data = array();
            $data['group_jasasarana']     = $row[3];
            $data['group_jasapelayanan']  = $row[4];
            $data['group_bhp']            = $row[5];
            $data['group_biayaperawatan'] = $row[6];

            $this->db->update('mtarif_rawatinap_detail', $data,
              array(
                'idtarif' => $row[0],
                'kelas' => $row[1]
              )
            );
        }
		$settingDiskonList = json_decode($_POST['setting_diskon_value']);
		foreach ($settingDiskonList as $row) {
            $data = array();
            $data['group_jasasarana_diskon']     = $row[3];
            $data['group_jasapelayanan_diskon']  = $row[4];
            $data['group_bhp_diskon']            = $row[5];
            $data['group_biayaperawatan_diskon'] = $row[6];

            $this->db->update('mtarif_rawatinap_detail', $data,
              array(
                'idtarif' => $row[0],
                'kelas' => $row[1]
              )
            );
        }
		$data_edit=array('group_diskon_all' => $this->input->post('group_diskon_all'));
		$this->db->where('id',$this->input->post('id'));
		$this->db->update('mtarif_rawatinap',$data_edit);
		
        return true;
    }
}
