<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tpiutang_verifikasi_model extends CI_Model
{
  
   public function list_kelompok_pasien(){
	   $q="SELECT id,nama from mpasien_kelompok 
				WHERE mpasien_kelompok.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_rekanan(){
	   $q="SELECT id,nama from mrekanan 
				WHERE mrekanan.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_poli(){
	   $q="SELECT id,nama from mpoliklinik 
				WHERE mpoliklinik.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_dokter(){
	   $q="SELECT id,nama from mdokter 
				WHERE mdokter.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function js_pegawai_dokter(){
		$tipe=$this->input->post('tipe');
		$search=$this->input->post('search');
		if ($tipe=='1'){//PEgawai
			$q="SELECT M.id,M.nama from mpegawai M
			WHERE nama LIKE '%".$search."%' AND M.status='1'";
		}else{
			$q="SELECT M.id,M.nama from mdokter M
			WHERE nama LIKE '%".$search."%' AND M.status='1'";
			
		}
		// print_r($q);exit();
		$query=$this->db->query($q);
		$result= $query->result();		
		return $result;			
	}
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
