<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_info_ods_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_assesmen_setting(){
		$q="SELECT * FROM setting_info_ods H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_label(){
		$q="SELECT * FROM setting_info_ods_label";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_user(){
		$q="SELECT * FROM setting_info_ods_user H";
		return $this->db->query($q)->row_array();
	}

	function save_assesmen(){
		$id =1;
		$this->judul_header = $this->input->post('judul_header');
		$this->judul_header_eng = $this->input->post('judul_header_eng');
		$this->footer_eng = $this->input->post('footer_eng');
		$this->footer_ina = $this->input->post('footer_ina');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->lama_edit = $this->input->post('lama_edit');
		$this->orang_edit = $this->input->post('orang_edit');
		$this->st_hapus_catatan = $this->input->post('st_hapus_catatan');
		$this->lama_hapus = $this->input->post('lama_hapus');
		$this->orang_hapus = $this->input->post('orang_hapus');
		$this->st_duplikasi_catatan = $this->input->post('st_duplikasi_catatan');
		$this->lama_duplikasi = $this->input->post('lama_duplikasi');
		$this->orang_duplikasi = $this->input->post('orang_duplikasi');
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_info_ods', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_label(){
		$id =1;
		// print_r($this->input->post());exit;
		$this->alamat_rs = $this->input->post('alamat_rs');
		$this->phone_rs = $this->input->post('phone_rs');
		$this->web_rs = $this->input->post('web_rs');
		$this->label_identitas_ina = $this->input->post('label_identitas_ina');
		$this->label_identitas_eng = $this->input->post('label_identitas_eng');
		$this->no_reg_ina = $this->input->post('no_reg_ina');
		$this->no_reg_eng = $this->input->post('no_reg_eng');
		$this->no_rm_ina = $this->input->post('no_rm_ina');
		$this->no_rm_eng = $this->input->post('no_rm_eng');
		$this->nama_pasien_ina = $this->input->post('nama_pasien_ina');
		$this->nama_pasien_eng = $this->input->post('nama_pasien_eng');
		$this->ttl_ina = $this->input->post('ttl_ina');
		$this->ttl_eng = $this->input->post('ttl_eng');
		$this->umur_ina = $this->input->post('umur_ina');
		$this->umur_eng = $this->input->post('umur_eng');
		$this->jk_ina = $this->input->post('jk_ina');
		$this->jk_eng = $this->input->post('jk_eng');
		$this->paragraf_1_ina = $this->input->post('paragraf_1_ina');
		$this->paragraf_1_eng = $this->input->post('paragraf_1_eng');
		$this->dpjp_ina = $this->input->post('dpjp_ina');
		$this->dpjp_eng = $this->input->post('dpjp_eng');
		$this->rencana_ina = $this->input->post('rencana_ina');
		$this->rencana_eng = $this->input->post('rencana_eng');
		$this->tanggal_tindakan_ina = $this->input->post('tanggal_tindakan_ina');
		$this->tanggal_tindakan_eng = $this->input->post('tanggal_tindakan_eng');
		$this->waktu_tindakan_ina = $this->input->post('waktu_tindakan_ina');
		$this->waktu_tindakan_eng = $this->input->post('waktu_tindakan_eng');
		$this->jenis_bius_ina = $this->input->post('jenis_bius_ina');
		$this->jenis_bius_eng = $this->input->post('jenis_bius_eng');
		$this->minum_ina = $this->input->post('minum_ina');
		$this->minum_eng = $this->input->post('minum_eng');
		$this->jika_ya_ina = $this->input->post('jika_ya_ina');
		$this->jika_ya_eng = $this->input->post('jika_ya_eng');
		$this->info_ina = $this->input->post('info_ina');
		$this->info_eng = $this->input->post('info_eng');
		$this->paragraf_2_ina = $this->input->post('paragraf_2_ina');
		$this->paragraf_2_eng = $this->input->post('paragraf_2_eng');
		$this->paragraf_3_ina = $this->input->post('paragraf_3_ina');
		$this->paragraf_3_eng = $this->input->post('paragraf_3_eng');
		$this->paragraf_4_ina = $this->input->post('paragraf_4_ina');
		$this->paragraf_4_eng = $this->input->post('paragraf_4_eng');
		$this->label_pasien_ina = $this->input->post('label_pasien_ina');
		$this->label_pasien_eng = $this->input->post('label_pasien_eng');
		$this->label_info_ina = $this->input->post('label_info_ina');
		$this->label_info_eng = $this->input->post('label_info_eng');
		$this->label_puasa_ina = $this->input->post('label_puasa_ina');
		$this->label_puasa_eng = $this->input->post('label_puasa_eng');
		$this->paragraf_5_ina = $this->input->post('paragraf_5_ina');
		$this->paragraf_5_eng = $this->input->post('paragraf_5_eng');
		$this->paragraf_dok_ina = $this->input->post('paragraf_dok_ina');
		$this->paragraf_dok_eng = $this->input->post('paragraf_dok_eng');
		$this->label_status_ina = $this->input->post('label_status_ina');
		$this->label_status_eng = $this->input->post('label_status_eng');
		$this->label_ket_ina = $this->input->post('label_ket_ina');
		$this->label_ket_eng = $this->input->post('label_ket_eng');
		$this->paragraf_6_ina = $this->input->post('paragraf_6_ina');
		$this->paragraf_6_eng = $this->input->post('paragraf_6_eng');
		$this->paragraf_7_ina = $this->input->post('paragraf_7_ina');
		$this->paragraf_7_eng = $this->input->post('paragraf_7_eng');
		$this->label_catatan_ina = $this->input->post('label_catatan_ina');
		$this->label_catatan_eng = $this->input->post('label_catatan_eng');
		$this->content_catatan_ina = $this->input->post('content_catatan_ina');
		$this->content_catatan_eng = $this->input->post('content_catatan_eng');
		$this->label_keluarga_ina = $this->input->post('label_keluarga_ina');
		$this->label_keluarga_eng = $this->input->post('label_keluarga_eng');
		$this->label_petugas_ina = $this->input->post('label_petugas_ina');
		$this->label_petugas_eng = $this->input->post('label_petugas_eng');
		$this->upload_login_logo(true);
		// $this->db->where('id', $id);
			
		if ($this->db->update('setting_info_ods_label', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	public function upload_login_logo($update = false)
    {
        if (!file_exists('assets/upload/app_setting')) {
            mkdir('assets/upload/app_setting', 0755, true);
        }
        if (isset($_FILES['logo'])) {
		// print_r('sini');exit;
            if ($_FILES['logo']['name'] != '') {
                $config['upload_path'] = './assets/upload/app_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('logo')) {
                    $image_upload = $this->upload->data();
                    $this->logo = $image_upload['file_name'];
					
                    if ($update == true) {
                        // $this->remove_image_logo(1);
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
                return true;
            }
					// print_r($this->foto);exit;
        } else {
            return true;
        }
		
    }
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


