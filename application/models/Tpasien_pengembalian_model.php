<?php
defined('BASEPATH') or exit('Hacking Attemp. Force Close!');

class Tpasien_pengembalian_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function getAll()
    {
        $this->db->where('status', '1');
        $query = $this->db->get('tpasien_pengembalian');
        return $query->result();
    }



    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('tpoliklinik_tindakan');
        return $query->row();
    }


    public function getObat($idobat=null)
    {
        return $this->db->select('t1.id as idstok,
                                t1.*,
                                t2.id as idobat,
                                t2.nama as namaObat,
                                t2.*,
                                t3.nama as namasatuan,
                                t3.singkatan')
                      ->join('mdata_obat t2', 't1.idbarang = t2.id')
                      ->join('msatuan t3', 't2.idsatuankecil = t3.id')
                      ->where('t1.idtipe', 3)
                      ->where('t1.id', $idobat)
                      ->group_by("t2.id")
                      ->get('mgudang_stok t1');
    }


    public function list_data($id)
    {
        $this->db->select("tpasien_pengembalian_detail.*,view_barang_farmasi.nama");
        $this->db->from('tpasien_pengembalian_detail');
        $this->db->join('view_barang_farmasi', 'view_barang_farmasi.id=tpasien_pengembalian_detail.idbarang AND view_barang_farmasi.idtipe=tpasien_pengembalian_detail.idtipe');
        $this->db->where('tpasien_pengembalian_detail.idpengembalian', $id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_header($id)
    {
        $this->db->select('tpasien_pengembalian.*,
					tpasien_penjualan.nopenjualan,tpasien_penjualan.nomedrec
					,tpasien_penjualan.nama
					,tpasien_penjualan.umurtahun
					,tpasien_penjualan.umurbulan
					,tpasien_penjualan.umurhari
					,tpasien_penjualan.asalrujukan,musers.name as created_nama
					');
        $this->db->from('tpasien_pengembalian');
        $this->db->join('tpasien_penjualan', 'tpasien_penjualan.id=tpasien_pengembalian.idpenjualan');
        $this->db->join('musers', 'tpasien_pengembalian.created_by=musers.id','LEFT');
        $this->db->where('tpasien_pengembalian.id', $id);
        $query=$this->db->get();
        return $query->row();
    }
    public function get_penjualan_detail($id)
    {
        $q="SELECT tpasien_penjualan.*,(CASE
				WHEN asalrujukan = 0 THEN 'Non Rujukan'
				WHEN asalrujukan = 1 THEN 'Poliklinik'
				WHEN asalrujukan = 2 THEN 'Instalasi Gawat Darurat'
				WHEN asalrujukan = 3 THEN 'Rawat Inap'	END) AS asal_pasien,
mpasien_kelompok.nama as nama_kelompok,
(CASE WHEN P.idjenispasien='1' THEN 'NON COB' WHEN P.idjenispasien='2' THEN 'COB' END ) as jenis_pasien,
RK.nama as nama_rekanan,Kes.kode as kode_kes,Ket.id as kode_ket
			 FROM tpasien_penjualan
			 INNER JOIN mpasien_kelompok ON mpasien_kelompok.id=tpasien_penjualan.idkelompokpasien
LEFT JOIN tpoliklinik_tindakan T ON T.id=tpasien_penjualan.idtindakan
LEFT JOIN tpoliklinik_pendaftaran P ON P.id=T.idpendaftaran
LEFT JOIN mrekanan RK ON RK.id=P.idrekanan
LEFT JOIN mtarif_bpjskesehatan Kes ON Kes.id=P.idtarifbpjskesehatan
LEFT JOIN mtarif_bpjstenagakerja Ket ON Ket.id=P.idtarifbpjstenagakerja
			 WHERE tpasien_penjualan.id = '$id'";
        $query=$this->db->query($q);
        return $query->row();
    }
    public function get_pengembalian_detail($id)
    {
        $q="SELECT tpasien_penjualan.*,(CASE
				WHEN tpasien_penjualan.asalrujukan = 0 THEN
					'Non Rujukan'
				WHEN tpasien_penjualan.asalrujukan = 1 THEN
					'Rawat Jalan'
				WHEN tpasien_penjualan.asalrujukan = 2 THEN
					'Instalasi Gawat Darurat'
				WHEN tpasien_penjualan.asalrujukan = 3 THEN
					'Rawat Inap'
			END) AS asal_pasien,mpasien_kelompok.nama as nama_kelompok
			,tpasien_pengembalian.idpenjualan,tpasien_pengembalian.alasan
			,tpasien_pengembalian.totalharga as total_harga
			 FROM tpasien_pengembalian
			 INNER JOIN tpasien_penjualan ON tpasien_pengembalian.idpenjualan=tpasien_penjualan.id
			 INNER JOIN mpasien_kelompok ON mpasien_kelompok.id=tpasien_penjualan.idkelompokpasien
			 WHERE tpasien_pengembalian.id = '$id'";
        $query=$this->db->query($q);
        // print_r($this->db->last_query());exit();
        return $query->row();
    }
    public function get_penjualan_obat_detail($id)
    {
        // $this->db->select("tpasien_penjualan_nonracikan.*,view_barang_farmasi.nama,msatuan.singkatan");
        // $this->db->from('tpasien_penjualan_nonracikan');
        // $this->db->join('view_barang_farmasi','view_barang_farmasi.id=tpasien_penjualan_nonracikan.idbarang AND view_barang_farmasi.idtipe=tpasien_penjualan_nonracikan.idtipe ');
        // $this->db->join('msatuan','msatuan.id=view_barang_farmasi.idsatuankecil');
        // $this->db->where('tpasien_penjualan_nonracikan.idpenjualan',$id);
        // $query = $this->db->get();
        // print_r($this->db->last_query());exit();
        $q="SELECT
			`tpasien_penjualan_nonracikan`.*, `view_barang_farmasi`.`nama`,
				`msatuan`.`singkatan`
			,SUM(CASE WHEN tpasien_penjualan_nonracikan.id=tpasien_pengembalian_detail.idpenjualan_detail THEN tpasien_pengembalian_detail.kuantitas ELSE 0 END) as qty_retur
			FROM
				`tpasien_penjualan_nonracikan`
			LEFT JOIN tpasien_pengembalian_detail ON tpasien_pengembalian_detail.idpenjualan_detail = tpasien_penjualan_nonracikan.id
			LEFT JOIN `view_barang_farmasi` ON `view_barang_farmasi`.`id` = `tpasien_penjualan_nonracikan`.`idbarang`
			AND `view_barang_farmasi`.`idtipe` = `tpasien_penjualan_nonracikan`.`idtipe`
			INNER JOIN `msatuan` ON `msatuan`.`id` = `view_barang_farmasi`.`idsatuankecil`
			WHERE
				`tpasien_penjualan_nonracikan`.`idpenjualan` = '$id'
			GROUP BY tpasien_penjualan_nonracikan.id";
        $query=$this->db->query($q);
        return $query->result_array();
    }
    public function get_retur_obat_detail($idpengembalian, $idpenjualan)
    {
        $q="SELECT PJ.id,PB.idpenjualan_detail,
				PJ.idpenjualan, PJ.idbarang,PJ.idtipe
				,PJ.harga_dasar AS hargadasar, PJ.margin, PJ.diskon
				,PJ.kuantitas,barang.nama
				,SUM(CASE WHEN PJ.id=PB.idpenjualan_detail AND PB.idpengembalian='$idpengembalian' THEN PB.kuantitas ELSE 0 END) as qty_retur
				,SUM(CASE WHEN PJ.id=PB.idpenjualan_detail  THEN PB.kuantitas ELSE 0 END) as total_retur
				,PJ.carapakai,PJ.harga,PB.totalharga,msatuan.singkatan,PJ.diskon_rp
			FROM
				tpasien_penjualan_nonracikan PJ
			LEFT JOIN tpasien_pengembalian_detail PB ON PB.idpenjualan_detail=PJ.id
			LEFT JOIN view_barang_farmasi barang ON barang.id=PJ.idbarang AND barang.idtipe=PJ.idtipe
			INNER JOIN msatuan ON msatuan.id=barang.idsatuankecil
			WHERE PJ.idpenjualan='$idpenjualan'
			GROUP BY PJ.id";
			// print_r($q);exit();
        $query = $this->db->query($q);
        return $query->result();
    }
    // fungsi save retur
    public function saveData($retur)
    {
        if ($this->db->insert('tpasien_pengembalian', $retur)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function delete_non_racikan($id)
    {
        return $this->db->delete('tpasien_pengembalian_detail', array('idpengembalian' => $id));
        // print_r($id);exit();
    }
    public function saveDataNonracikan($nonrac)
    {
        if ($this->db->insert('tpasien_pengembalian_detail', $nonrac)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function update_total_barang($id, $total_barang)
    {
        if ($this->db->update('tpasien_pengembalian', array('totalbarang'=>$total_barang), array('id' => $id))) {
            return true;
        } else {
            return false;
        }
    }
}
