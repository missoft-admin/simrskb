<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Merm_skrining_covid_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function get_data(){
		$q="select *from merm_skrining_covid where id='1'";
		return $this->db->query($q)->row_array();
	}
	function list_jawaban(){
		$q="select *from merm_referensi where ref_head_id='17'";
		return $this->db->query($q)->result();
	}
	function list_content(){
		$q="SELECT H.*,C.`name` as user_created,E.`name` as user_edited,GROUP_CONCAT(R.ref SEPARATOR '#') as ref FROM merm_skrining_covid_isi H
			LEFT JOIN musers C ON C.id=H.created_by
			LEFT JOIN musers E ON E.id=H.edited_by
			LEFT JOIN merm_skrining_covid_isi_jawaban M ON M.general_isi_id=H.id
			LEFT JOIN merm_referensi R ON R.id=M.ref_id
			WHERE H.`status`='1'
			
			GROUP BY H.id
			ORDER BY H.`no` ASC";
		return $this->db->query($q)->result();
	}
	function save(){
		$id = $this->input->post('id');
		$this->judul = $this->input->post('judul');
		// $this->sub_header = $this->input->post('sub_header');
		// $this->sub_header_side = $this->input->post('sub_header_side');
		// $this->footer_1 = $this->input->post('footer_1');
		// $this->footer_2 = $this->input->post('footer_2');
		// $this->ttd_1 = $this->input->post('ttd_1');
		// $this->ttd_2 = $this->input->post('ttd_2');
		$this->footer_form_1 = $this->input->post('footer_form_1');
		$this->footer_form_2 = $this->input->post('footer_form_2');
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
	  
		$this->upload_logo(true);
		$this->db->where('id', $id);
			
		if ($this->db->update('merm_skrining_covid', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	public function upload_logo($update = false)
    {
        if (!file_exists('assets/upload/app_setting')) {
            mkdir('assets/upload/app_setting', 0755, true);
        }

        if (isset($_FILES['logo'])) {
            if ($_FILES['logo']['name'] != '') {
                $config['upload_path'] = './assets/upload/app_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('logo')) {
                    $image_upload = $this->upload->data();
                    $this->logo = $image_upload['file_name'];

                    if ($update == true) {
                        $this->remove_image_logo($this->input->post('id'));
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
                return true;
            }
					// print_r($this->foto);exit;
        } else {
            return true;
        }
		
    }
	public function remove_image_logo($id)
    {
		$q="select logo From merm_skrining_covid H WHERE H.id='$id'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/app_setting/'.$row->logo) && $row->logo !='') {
            unlink('./assets/upload/app_setting/'.$row->logo);
        }
    }
}
