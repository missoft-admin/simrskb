<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Trujukan_rumahsakit_approval_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function approval($id)
	{
		$this->db->set('status_approval', 3);
		$this->db->where('id', $id);
		if ($this->db->update('tfeerujukan_rumahsakit')) {
			return true;
		}
	}

	public function reject($id)
	{
		$this->db->set('status_approval', 4);
		$this->db->where('id', $id);
		if ($this->db->update('tfeerujukan_rumahsakit')) {
			return true;
		}
	}
}
