<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Msetting_analisa_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('msetting_analisa');
        return $query->row();
    }

    public function saveData()
    {
        $this->nama                       = $_POST['nama'];
        $this->jenis                      = jsonEncode($_POST['jenis']);
        $this->tipe                       = jsonEncode($_POST['tipe']);
        $this->poliklinik                 = $_POST['poliklinik'];
        $this->dokter                     = jsonEncode($_POST['dokter']);
        $this->kelompok_diagnosa          = jsonEncode($_POST['kelompok_diagnosa']);
        $this->kelompok_tindakan          = jsonEncode($_POST['kelompok_tindakan']);
        $this->kelompok_tindakan_operasi  = jsonEncode($_POST['kelompok_tindakan_operasi']);
        $this->lembaran                   = $_POST['lembaran'];
        $this->keterangan                 = $_POST['keterangan'];

        if ($this->db->insert('msetting_analisa', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama                       = $_POST['nama'];
        $this->jenis                      = jsonEncode($_POST['jenis']);
        $this->tipe                       = jsonEncode($_POST['tipe']);
        $this->poliklinik                 = $_POST['poliklinik'];
        $this->dokter                     = jsonEncode($_POST['dokter']);
        $this->kelompok_diagnosa          = jsonEncode($_POST['kelompok_diagnosa']);
        $this->kelompok_tindakan          = jsonEncode($_POST['kelompok_tindakan']);
        $this->kelompok_tindakan_operasi  = jsonEncode($_POST['kelompok_tindakan_operasi']);
        $this->lembaran                   = $_POST['lembaran'];
        $this->keterangan                 = $_POST['keterangan'];

        if ($this->db->update('msetting_analisa', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
        return $status;
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('msetting_analisa', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

  	public function formData() {
  		$mpoliklinik = get_all('mpoliklinik', array('status' => '1'));
  		$mkelas = get_all('mkelas', array('status' => '1'));
  		$mdokter = get_all('mdokter', array('status' => '1'));

  		$poliklinik = array_map(
  				function ($item) {
  					return array(
              'id' => $item->id,
  						'nama' => $item->nama,
  						'jenis' => 'rawat_jalan',
  						'tipe' => $item->idtipe == 1 ? 'poliklinik' : 'igd',
  					);
  				}, $mpoliklinik
  		);

  		$kelas = array_map(
  				function ($item) {
  					return array(
              'id' => $item->id,
  						'nama' => 'Kelas ' . $item->nama,
  						'jenis' => 'rawat_inap',
  						'tipe' => 'rawat_inap',
  					);
  				}, $mkelas
  		);

  		$data = array(
  			'id' => '',
  			'nama' => '',
  			'jenis' => array(
  				array(
            'id' => 'rawat_jalan',
  					'nama' => 'Rawat Jalan',
  				),
  				array(
            'id' => 'rawat_inap',
  					'nama' => 'Rawat Inap',
  				),
  			),
  			'tipe' => array(
  				array(
            'id' => 'poliklinik',
  					'nama' => 'Poliklinik',
  					'jenis' => 'rawat_jalan',
  				),
  				array(
            'id' => 'igd',
  					'nama' => 'Instalasi Gawat Darurat (IGD)',
  					'jenis' => 'rawat_jalan',
  				),
  				array(
            'id' => 'rawat_inap',
  					'nama' => 'Rawat Inap',
  					'jenis' => 'rawat_inap',
  				),
  				array(
            'id' => 'ods',
  					'nama' => 'One Day Surgery (ODS)',
  					'jenis' => 'rawat_inap',
  				),
  			),
  			'poliklinik' => array_merge(
  				$poliklinik, $kelas
  			),
  			'dokter' => $mdokter,
  			'kelompok_diagnosa' => array(),
  			'kelompok_tindakan' => array(),
  			'kelompok_tindakan_operasi' => array(),
  			'lembaran_rm' => array(),
  			'keterangan' => '',
  			'status' => '',
  		);

  		return $data;
  	}
}
