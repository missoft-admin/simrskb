<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tunit_pengembalian_model extends CI_Model
{
    public function getIdPermintaan($idbarang, $idtipe)
    {
        $this->db->select('a.nopermintaan, a.id, c.nama as unitpeminta');
        $this->db->where('b.idtipe', $idtipe);
        $this->db->where('b.idbarang', $idbarang);
        $this->db->join('tunit_permintaan_detail b', 'b.idpermintaan = a.id', 'left');
        $this->db->join('munitpelayanan c', 'c.id = a.idunitpeminta', 'left');
        return $this->db->get('tunit_permintaan a')->result();
    }

    public function save()
    {
        $detail_value = json_decode($this->input->post('detailValue_1'));
        $get_unique = array_map('unserialize', array_unique(array_map(function ($arr) {
            return serialize(
                array(
                    0 => $arr[8],
                    1 => $arr[11],
                    2 => $arr[9]
                )
            );
        }, $detail_value)));
		// print_r($get_unique);exit();

        foreach ($get_unique as $row_reff) {
            // var default
            $totalbarang           = 0;
            $alasan            	   = '-';

            // this input
            // $this->nopengembalian   = get_kode('UP', 'nopengembalian', 'tunit_pengembalian', 3);
            $this->nopengembalian   = $this->generate_no();
            $this->idunitpelayanan  = $row_reff[2];
            $this->tanggal          = $row_reff[1];
            $this->status           = 1;
            $this->idpermintaan     = $row_reff[0];
            $this->iduser_created   = $this->session->userdata("user_id");
            $this->namauser_created = $this->session->userdata("user_name");

            if ($this->db->insert('tunit_pengembalian', $this)) {
                $idpengembalian = $this->db->insert_id();

                foreach ($detail_value as $r) {
                    if ($row_reff[0] == $r[8]) {
                        $detail 					= array();
                        $detail['idpengembalian'] 	= $idpengembalian;
                        $detail['idtipe'] 			= $r[6];
                        $detail['idbarang'] 		= $r[7];
                        $detail['kuantitas'] 		= $r[3];
                        $detail['nobatch'] 		= $r[4];
                        $detail['status'] 			= 1;
                        $detail['namabarang']       = $r[1];
                        $detail['hargabarang']       = $r[12];
                        $detail['marginbarang']       = $r[13];
                        $detail['total']       = $r[3]*$r[12];
                        if ($this->db->insert('tunit_pengembalian_detail', $detail)) {
                            $totalbarang     = $totalbarang + RemoveComma($r[3]);
                            $alasan     	 = $r[10];
                        }
                    }
                }
            }

            // update total transaksi
            $this->db->set('totalbarang', $totalbarang);
            $this->db->set('alasan', $alasan);
            $this->db->where('id', $idpengembalian);
            if ($this->db->update('tunit_pengembalian')) {
                $status = true;
            } else {
                $status = false;
            }
        }
        return $status;
    }
	public function save_edit(){
		$idbarang=$this->input->post('e_idbarang');
		$st_edit=$this->input->post('e_st_edit');
		$st_hapus=$this->input->post('e_st_hapus');
		$qty=$this->input->post('e_kuantitas');
		$id_det=$this->input->post('e_id_det');
		$idtipe=$this->input->post('e_idtipe');
		$nobatch=$this->input->post('e_nobatch');
		$namabarang=$this->input->post('e_namabarang');
		$data_edit=array();
		$data_edit['tgl_edit']=date('Y-m-d H:i:s');
		$data_edit['alasan']=$this->input->post('alasan');
		$data_edit['iduser_updated']=$this->session->userdata("user_id");
		$data_edit['namauser_updated']=$this->session->userdata("user_name");
		$this->db->where('id', $this->input->post('id'));
		// print_r($this->input->post('id'));exit();
		$this->db->update('tunit_pengembalian',$data_edit); 
		foreach ($st_edit as $key => $val) {
			if ($st_edit[$key]=='1'){
				if ($id_det[$key]=='0'){
					$data_insert=array();
					$data_insert['idpengembalian']=$this->input->post('id');
					$data_insert['idtipe']=$idtipe[$key];
					$data_insert['idbarang']=$idbarang[$key];
					$data_insert['kuantitas']=$qty[$key];
					$data_insert['nobatch']=$nobatch[$key];
					$data_insert['kuantitas_terima']='0';
					$data_insert['status']='1';
					$data_insert['namabarang']=RemoveBintang($namabarang[$key]);
					$data_insert['hargabarang']='0';
					$data_insert['marginbarang']='0';
					$data_insert['total']='0';
					// print_r($data_insert);exit();
					$this->db->insert('tunit_pengembalian_detail', $data_insert);
					// $data_update['kuantitas']=$qty[$key];
				}else{					
					$data_update=array();
					$data_update['idbarang']=$idbarang[$key];
					$data_update['kuantitas']=$qty[$key];
					$data_update['nobatch']=$nobatch[$key];
					$data_update['namabarang']=RemoveBintang($namabarang[$key]);
					if ($st_hapus[$key]=='1'){
						$data_update['status']=0;
					}
					
					$this->db->where('id', $id_det[$key]);
					$this->db->update('tunit_pengembalian_detail',$data_update); 
				}
			}
		}
		return true;
	}
	public function save_terima(){
		$idbarang=$this->input->post('e_idbarang');
		$st_edit=$this->input->post('e_st_edit');
		$st_hapus=$this->input->post('e_st_hapus');
		$qty=$this->input->post('e_kuantitas');
		$id_det=$this->input->post('e_id_det');
		$namabarang=$this->input->post('e_namabarang');
		$data_edit=array();
		$data_edit['iduser_terima']=$this->session->userdata("user_id");
		$data_edit['namauser_terima']=$this->session->userdata("user_name");
		$data_edit['tgl_terima']=date('Y-m-d H:i:s');
		$data_edit['status']=2;
		$this->db->where('id', $this->input->post('id'));
		// print_r($this->input->post('id'));exit();
		$this->db->update('tunit_pengembalian',$data_edit); 
		foreach ($st_edit as $key => $val) {
			
				$data_update=array();
				$data_update['kuantitas_terima']=$qty[$key];	
				// print_r($data_update);exit();
				$this->db->where('id', $id_det[$key]);
				$this->db->update('tunit_pengembalian_detail',$data_update); 
			
		}
		return true;
	}
	function generate_no(){
		$kode='UP';
		$this->db->like('nopengembalian',$kode.date('y').date('m'), 'after');
		$this->db->from('tunit_pengembalian');
		$this->db->select_max('nopengembalian');
		$query=$this->db->get();
		
		$query=$query->row('nopengembalian');
		// $query = $this->db->count_all_results();

		if ($query) {
			$query=(int)substr($query,-3);
			// print_r($query);exit();
			
			$autono = $query + 1;
			$autono = $kode.date('y').date('m').str_pad($autono, 3, '0', STR_PAD_LEFT);
		} else {
			$autono = $kode.date('y').date('m').'001';
		}

		return $autono;
  }
    public function edit($id){
       
		$q="SELECT H.id,H.tanggal,H.nopengembalian,H.alasan,H.idunitpelayanan as id_unit_dari,UD.nama as unit_dari,H.idpermintaan as id_unit_ke,UK.nama as unit_ke,SUM(D.kuantitas) as tot_kembali,
				SUM(D.kuantitas_terima) as tot_terima,iduser_created,musers.`name` as nama_user,H.`status`,ut.`name` as user_terima
				FROM tunit_pengembalian H
				LEFT JOIN tunit_pengembalian_detail D ON H.id=D.idpengembalian
				LEFT JOIN munitpelayanan UD ON UD.id=H.idunitpelayanan
				LEFT JOIN munitpelayanan UK ON UK.id=H.idpermintaan
				LEFT JOIN musers ON musers.id=H.iduser_created
				LEFT JOIN musers ut ON ut.id=H.iduser_terima
				WHERE H.id='$id'
				GROUP BY H.id";
		$query=$this->db->query($q);
		return $query->row_array();
    }
    public function List_barang_edit($id){
       
		$q="SELECT D.id,D.idpengembalian,D.idtipe,D.idbarang,B.nama as namabarang,COALESCE(G.stok,0) as stok,B.namatipe,B.kode,D.kuantitas,
			mdata_kategori.nama as nama_kategori,msatuan.singkatan as satuan,D.nobatch
			FROM tunit_pengembalian H
			LEFT JOIN tunit_pengembalian_detail D ON H.id=D.idpengembalian
			LEFT JOIN view_barang B ON B.idtipe=D.idtipe AND B.id=D.idbarang
			LEFT JOIN mgudang_stok G ON G.idunitpelayanan=H.idunitpelayanan AND G.idbarang=D.idbarang AND G.idtipe=D.idtipe
			LEFT JOIN mdata_kategori ON mdata_kategori.id=B.idkategori
			LEFT JOIN msatuan ON msatuan.id=B.idsatuan
			WHERE D.idpengembalian='$id' AND D.status='1'";
		$query=$this->db->query($q);
		return $query->result();
    }
    public function update($id){
        $detail_value = json_decode($this->input->post('detailValue_1'));
        $get_unique = array_map('unserialize', array_unique(array_map(function ($arr) {
            return serialize(
                array(
                    0 => $arr[8],
                    1 => $arr[10],
                    2 => $arr[7]
                )
            );
        }, $detail_value)));

        foreach ($get_unique as $row_reff) {
            $totalbarang           = 0;

            foreach ($detail_value as $r) {
                echo json_encode($r);
                if ($row_reff[0] == $r[8]) {
                    $detail 					= array();
                    $detail['idpengembalian'] 	= $id;
                    $detail['idtipe'] 			= $r[5];
                    $detail['idbarang'] 		= $r[6];
                    $detail['kuantitas'] 		= $r[3];
                    $detail['status'] 			= 1;
                    $this->db->where("id",$r[11]);
                    if ($this->db->update('tunit_pengembalian_detail', $detail)) {
                        $totalbarang     = $totalbarang + RemoveComma($r[3]);
                    }
                }
            }

            // update total transaksi
            $this->db->set('iduser_updated',$this->session->userdata("user_id"));
            $this->db->set('namauser_updated',$this->session->userdata("user_name"));
            $this->db->set('totalbarang', $totalbarang);
            $this->db->set('alasan', $this->input->post("alasan"));
            $this->db->where('id', $id);
            if ($this->db->update('tunit_pengembalian')) {
                $status = true;
            } else {
                $status = false;
            } 
        }
        return $status;
    }
    public function delete($id) {
        $this->db->delete("tunit_pengembalian",array("id"=>$id));
        $this->db->delete("tunit_pengembalian_detail",array("idpengembalian"=>$id));
    }
}


/* End of file Tunit_pengembalian_model.php */
/* Location: ./application/models/Tunit_pengembalian_model.php */
