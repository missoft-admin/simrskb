<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tsetoran_kas_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
	public function getNoTransaksi()
    {
        $this->db->like('notransaksi', 'TSK'.date('Y').date('m'), 'after');
        $this->db->from('tsetoran_kas');
        $query = $this->db->count_all_results();

        if ($query > 0) {
            $autono = $query + 1;
            $autono = "TSK".date('Y').date('m').str_pad($autono, 4, '0', STR_PAD_LEFT);
        } else {
            $autono = "TSK".date('Y').date('m')."0001";
        }

        return $autono;
    }
    public function getSpecified($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('tsetoran_kas');
        return $query->row_array();
    }
	public function list_pembayaran($id) {
        // $this->db->where('id', $id);
        // $query = $this->db->get('tsetoran_kas_detail');
		$q="SELECT JK.nama as jenis_kas,SK.nama as sumber_kas,D.* FROM tsetoran_kas_detail D
		LEFT JOIN mjenis_kas JK ON JK.id=D.jenis_kas_id
		LEFT JOIN msumber_kas SK ON SK.id=D.sumber_kas_id
		WHERE D.idtransaksi='$id' AND D.status='1'";
        return $this->db->query($q)->result();
    }
	
	
    public function saveData() {
        $this->tanggal_trx        = YMDFormat($_POST['tanggal_trx']);
        $this->tanggal_setoran        = YMDFormat($_POST['tanggal_setoran']);
		$this->notransaksi    = $this->getNoTransaksi();
       
        $this->cash_rajal         = RemoveComma($_POST['cash_rajal']);
        $this->cash_ranap         = RemoveComma($_POST['cash_ranap']);
        $this->cash_pendapatan         = RemoveComma($_POST['cash_pendapatan']);
        $this->cash_retur         = RemoveComma($_POST['cash_retur']);
        $this->cash_piutang         = RemoveComma($_POST['cash_piutang']);
        $this->nominal         = RemoveComma($_POST['nominal']);
        $this->status         = 1;
        // $this->statusjurnal   = 0;
        $this->created_at  = date("Y-m-d H:i:s");
        $this->user_created  = $this->session->userdata('user_id');

        if ($this->db->insert('tsetoran_kas', $this)) {
            $idtransaksi = $this->db->insert_id();
			$xjenis_kas_id= $_POST['xjenis_kas_id'];
			$xsumber_kas_id= $_POST['xsumber_kas_id'];
			$xidmetode= $_POST['xidmetode'];
			$ket= $_POST['ket'];
			$xnominal_bayar= $_POST['xnominal_bayar'];
            foreach ($xjenis_kas_id as $index => $val){
				$data_detail=array(
					'idtransaksi'=>$idtransaksi,
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'idmetode'=>$xidmetode[$index],
					'ket_pembayaran'=>$ket[$index],
					// 'tanggal_pencairan'=>YMDFormat($tanggal_pencairan),
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->insert('tsetoran_kas_detail', $data_detail);
			}

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
		
    }
	
    public function updateData() {
        // $this->tanggal_trx        = YMDFormat($_POST['tanggal_trx']);
        $this->tanggal_setoran        = YMDFormat($_POST['tanggal_setoran']);
		$this->notransaksi    = $this->getNoTransaksi();
       
        $this->cash_rajal         = RemoveComma($_POST['cash_rajal']);
        $this->cash_ranap         = RemoveComma($_POST['cash_ranap']);
        $this->cash_pendapatan         = RemoveComma($_POST['cash_pendapatan']);
		$this->cash_retur         = RemoveComma($_POST['cash_retur']);
		$this->cash_piutang         = RemoveComma($_POST['cash_piutang']);
        $this->nominal         = RemoveComma($_POST['nominal']);
        $this->status         = 1;
        // $this->statusjurnal   = 0;
        $this->created_at  = date("Y-m-d H:i:s");
        $this->user_created  = $this->session->userdata('user_id');
		
		$this->db->where('id',$_POST['id']);
        if ($this->db->update('tsetoran_kas', $this)) {		
			$xjenis_kas_id= $_POST['xjenis_kas_id'];
			$xsumber_kas_id= $_POST['xsumber_kas_id'];
			$xidmetode= $_POST['xidmetode'];
			$ket= $_POST['ket'];
			$xnominal_bayar= $_POST['xnominal_bayar'];
			$xiddet= $_POST['xiddet'];
			$xstatus= $_POST['xstatus'];
		   foreach ($xjenis_kas_id as $index => $val){
				if ($xiddet[$index]!=''){
					$data_detail=array(
						'jenis_kas_id'=>$xjenis_kas_id[$index],
						'sumber_kas_id'=>$xsumber_kas_id[$index],
						'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
						'idmetode'=>$xidmetode[$index],
						'ket_pembayaran'=>$ket[$index],
						'status'=>$xstatus[$index],
						'edited_by'=>$this->session->userdata('user_id'),
						'edited_date'=>date('Y-m-d H:i:s'),
					);
					$this->db->where('id',$xiddet[$index]);
					$this->db->update('tsetoran_kas_detail', $data_detail);
				}else{
					$idtransaksi=$this->input->post('id');
					$data_detail=array(
						'idtransaksi'=>$idtransaksi,
						'jenis_kas_id'=>$xjenis_kas_id[$index],
						'sumber_kas_id'=>$xsumber_kas_id[$index],
						'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
						'idmetode'=>$xidmetode[$index],
						'ket_pembayaran'=>$ket[$index],
						// 'tanggal_pencairan'=>YMDFormat($tanggal_pencairan),
						'created_by'=>$this->session->userdata('user_id'),
						'created_date'=>date('Y-m-d H:i:s'),
					);
					$this->db->insert('tsetoran_kas_detail', $data_detail);
				}				
			}
		}
		return true;
	}
	public function insert_jurnal_setoran($id){
		// $id='60';
		// $tanggal='2021-08-08';
		$q="SELECT 
			H.id as idtransaksi,H.tanggal_trx as tanggal_transaksi,H.tanggal_setoran as periode_transaksi
			,H.notransaksi as notransaksi,U.`name` as user_setor,H.nominal as nominal,S.idakun_tunai as idakun_tunai,'K' as posisi_akun
			,S.st_auto_posting
			FROM tsetoran_kas H
			LEFT JOIN musers U ON U.id=H.user_created
			LEFT JOIN msetting_jurnal_setoran S ON S.id='1'
			WHERE H.id='$id'";
		$row=$this->db->query($q)->row();
		// print_r($row);exit();
		$st_auto_posting=$row->st_auto_posting;
		$data_header=array(
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'periode_transaksi' => $row->periode_transaksi,
			'notransaksi' => $row->notransaksi,
			'user_setor' => $row->user_setor,
			'nominal' => $row->nominal,
			'idakun_tunai' => $row->idakun_tunai,
			'posisi_akun' => $row->posisi_akun,
			'st_auto_posting' => $row->st_auto_posting,
			'status' => '1',
			'st_posting' => 0,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
            'created_date'=>date('Y-m-d H:i:s')
		);
		$this->db->insert('tvalidasi_setoran',$data_header);
		$idvalidasi=$this->db->insert_id();
		
		$q="SELECT 
			D.id as idbayar_id,D.jenis_kas_id,JK.nama as jenis_kas_nama
			,D.sumber_kas_id,SK.nama as sumber_nama,MB.nama as bank
			,SK.bank_id as bankid,D.idmetode as metode_bayar
			,M.metode_bayar as metode_nama,D.nominal_bayar
			,SK.idakun,'D' as posisi_akun
			FROM tsetoran_kas_detail D
			LEFT JOIN mjenis_kas JK ON JK.id=D.jenis_kas_id
			LEFT JOIN msumber_kas SK ON SK.id=D.sumber_kas_id
			LEFT JOIN mbank MB ON MB.id=SK.bank_id
			LEFT JOIN ref_metode M ON M.id=D.idmetode
			WHERE D.idtransaksi='$id' AND D.status='1'

			
			";
		$rows=$this->db->query($q)->result();
		foreach($rows as $r){
			$data_detail=array(
				'idvalidasi' => $idvalidasi ,
				'idbayar_id' =>$r->idbayar_id,
				'jenis_kas_id' =>$r->jenis_kas_id,
				'jenis_kas_nama' =>$r->jenis_kas_nama,
				'sumber_kas_id' =>$r->sumber_kas_id,
				'sumber_nama' =>$r->sumber_nama,
				'bank' =>$r->bank,
				'bankid' =>$r->bankid,
				'metode_bayar' =>$r->metode_bayar,
				'metode_nama' =>$r->metode_nama,
				'nominal_bayar' =>$r->nominal_bayar,
				'idakun' =>$r->idakun,
				'posisi_akun' =>$r->posisi_akun,

			);
			$this->db->insert('tvalidasi_setoran_bayar',$data_detail);
		}
		if ($st_auto_posting=='1'){
			$data_header=array(
				
				'st_posting' => 1,
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
				

			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_setoran',$data_header);
		}
		return true;
	}
}
