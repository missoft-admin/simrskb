<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mdata_pengelolaan_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getSpecified($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('mdata_pengelolaan');
        return $query->row();
    }
	public function list_akun(){
	   $q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		return $list;
   }
   public function list_bank(){
	   $q="SELECT *from ref_bank";
		$list=$this->db->query($q)->result();
		return $list;
   }
   public function list_hutang(){
	   $q="SELECT *from mdata_hutang WHERE mdata_hutang.status='1'";
		$list=$this->db->query($q)->result();
		return $list;
   }
   public function list_piutang(){
	   $q="SELECT *from mdata_piutang WHERE mdata_piutang.status='1'";
		$list=$this->db->query($q)->result();
		return $list;
   }
   public function aktifkan($id)
    {
        $this->status = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mdata_pengelolaan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function saveData() {
        $this->nama       = $_POST['nama'];
        $this->st_harga_jual  = $_POST['st_harga_jual'];
		
        $this->created_by  = $this->session->userdata('user_id');
        $this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mdata_pengelolaan', $this)) {
           $id=$this->db->insert_id();
            return $id;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function list_user_approval($step,$idpengelolaan)
    {
        $q="SELECT *FROM musers M
				WHERE M.id NOT IN (SELECT iduser from mdata_pengelolaan_approval WHERE mdata_pengelolaan_approval.step='$step' AND M.status='1' 
				AND mdata_pengelolaan_approval.idpengelolaan='$idpengelolaan')";
		// print_r($q);
        return $this->db->query($q)->result();
    }
    public function updateData() {
		// print_r($_POST['st_harga_jual']);exit();
        $this->nama       = $_POST['nama'];
        $this->st_harga_jual  = $_POST['st_harga_jual'];
		
        $this->edited_by  = $this->session->userdata('user_id');
        $this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mdata_pengelolaan', $this, ['id' => $_POST['id']])) {
            
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id) {
        $this->status = 0;

        $this->deleted_by  = $this->session->userdata('user_id');
        $this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mdata_pengelolaan', $this, ['id' => $id])) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    
}
