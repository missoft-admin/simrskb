<?php

class Tpendapatan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getNoTransaksi()
    {
        $this->db->like('notransaksi', 'TPD'.date('Y').date('m'), 'after');
        $this->db->from('tbendahara_pendapatan');
        $query = $this->db->count_all_results();

        if ($query > 0) {
            $autono = $query + 1;
            $autono = "TPD".date('Y').date('m').str_pad($autono, 4, '0', STR_PAD_LEFT);
        } else {
            $autono = "TPD".date('Y').date('m')."0001";
        }

        return $autono;
    }

    public function getNoAkun($id)
    {
        $this->db->select('mpendapatan.*, makun_nomor.noakun AS kredit_noakun, makun_nomor.namaakun AS kredit_namaakun');
        $this->db->join('makun_nomor', 'makun_nomor.noakun = mpendapatan.noakun');
        $this->db->where('mpendapatan.id', $id);
        $this->db->where('mpendapatan.noakun != ', null);
        $this->db->where('mpendapatan.status', '1');
        $query = $this->db->get('mpendapatan');
        $row = $query->row();

        if ($query->num_rows() > 0) {
            $data['kredit_noakun'] = $row->kredit_noakun;
            $data['kredit_namaakun'] = $row->kredit_namaakun;
        } else {
            $data['kredit_noakun'] = '';
            $data['kredit_namaakun'] = '';
        }
        return $data;
    }

    public function getListBiaya()
    {
        $this->db->order_by('mpendapatan.keterangan', 'ASC');
        $this->db->where('mpendapatan.noakun != ', null);
        $this->db->where('mpendapatan.status', 1);
        $query = $this->db->get('mpendapatan');
        return $query->result();
    }
	public function list_dari()
    {
        $this->db->order_by('mdari.nama', 'ASC');
        $this->db->where('mdari.status', 1);
        $query = $this->db->get('mdari');
        return $query->result();
    }

    public function getListUser()
    {
        $this->db->where('status', 1);
        $query = $this->db->get('musers');
        return $query->result();
    }

    public function getListKasBesar()
    {
        $this->db->order_by('noakun', 'ASC');
        $this->db->where('noakun LIKE ', '%11.10.%');
        $this->db->or_where('noakun LIKE ', '%11.20.%');
        $this->db->or_where('noakun ', '21.10.04');
        $this->db->where('status', '1');
        $query = $this->db->get('makun_nomor');
        return $query->result();
    }

    public function getListPembayaran($id)
    {
       $q="SELECT JK.nama as jenis_kas,SK.nama as sumber_kas,D.* FROM tbendahara_pendapatan_detail D
		LEFT JOIN mjenis_kas JK ON JK.id=D.jenis_kas_id
		LEFT JOIN msumber_kas SK ON SK.id=D.sumber_kas_id
		WHERE D.idtransaksi='$id' AND D.status='1'";
		return $this->db->query($q)->result();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        // $this->db->where('s', $id);
        $query = $this->db->get('tbendahara_pendapatan');
        return $query->row_array();
    }
	public function getSpecifiedHeader($id)
    {
        $q="SELECT H.*,M.nama as dari_nama from tbendahara_pendapatan H
LEFT JOIN mdari M ON M.id=H.terimadari
WHERE H.id='$id'";
        // $this->db->where('s', $id);
        $query = $this->db->query($q);
        return $query->row_array();
    }

    public function saveData()
    {
		// print_r($this->input->post());exit();
        $this->id             = $this->input->post('idtransaksi');
        $this->notransaksi    = $this->getNoTransaksi();
        $this->tanggal        = YMDFormat($_POST['tanggal']);
        $this->idpendapatan   = $_POST['idpendapatan'];
        $this->keterangan     = $_POST['keterangan'];
        $this->terimadari     = $_POST['terimadari'];
        $this->noakun         = $_POST['noakun'];
        $this->nominal         = RemoveComma($_POST['nominal']);
        $this->status         = 1;
        $this->statusjurnal   = 0;
        $this->created_at  = date("Y-m-d H:i:s");
        $this->user_created  = $this->session->userdata('user_id');

        if ($this->db->insert('tbendahara_pendapatan', $this)) {
            $idtransaksi = $this->db->insert_id();
			$xjenis_kas_id= $_POST['xjenis_kas_id'];
			$xsumber_kas_id= $_POST['xsumber_kas_id'];
			$xidmetode= $_POST['xidmetode'];
			$ket= $_POST['ket'];
			$xnominal_bayar= $_POST['xnominal_bayar'];
            foreach ($xjenis_kas_id as $index => $val){
				$data_detail=array(
					'idtransaksi'=>$idtransaksi,
					'jenis_kas_id'=>$xjenis_kas_id[$index],
					'sumber_kas_id'=>$xsumber_kas_id[$index],
					'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
					'idmetode'=>$xidmetode[$index],
					'ket_pembayaran'=>$ket[$index],
					// 'tanggal_pencairan'=>YMDFormat($tanggal_pencairan),
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->insert('tbendahara_pendapatan_detail', $data_detail);
			}
			if ($_POST['btn_simpan']=='2'){
				$this->Tpendapatan_model->insert_jurnal_pendapatan($idtransaksi);
				$result=$this->db->query("UPDATE tbendahara_pendapatan set st_verifikasi='1' WHERE id='$idtransaksi'");
			}else{
				$result=true;
			}
            return $result;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function updateData()
    {
		
        $this->idpendapatan   = $_POST['idpendapatan'];
        $this->keterangan     = $_POST['keterangan'];
        $this->terimadari     = $_POST['terimadari'];
        $this->noakun         = $_POST['noakun'];
        $this->nominal         = RemoveComma($_POST['nominal']);
      
        $this->edited_date  = date("Y-m-d H:i:s");
        $this->edited_by  = $this->session->userdata('user_id');
		$this->db->where('id',$this->input->post('idtransaksi'));
        if ($this->db->update('tbendahara_pendapatan', $this)) {
			$xjenis_kas_id= $_POST['xjenis_kas_id'];
			$xsumber_kas_id= $_POST['xsumber_kas_id'];
			$xidmetode= $_POST['xidmetode'];
			$ket= $_POST['ket'];
			$xnominal_bayar= $_POST['xnominal_bayar'];
			$xiddet= $_POST['xiddet'];
			$xstatus= $_POST['xstatus'];
				// print_r($xiddet);exit();
            foreach ($xjenis_kas_id as $index => $val){
				if ($xiddet[$index]!=''){
					$data_detail=array(
						'jenis_kas_id'=>$xjenis_kas_id[$index],
						'sumber_kas_id'=>$xsumber_kas_id[$index],
						'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
						'idmetode'=>$xidmetode[$index],
						'ket_pembayaran'=>$ket[$index],
						'status'=>$xstatus[$index],
						'edited_by'=>$this->session->userdata('user_id'),
						'edited_date'=>date('Y-m-d H:i:s'),
					);
					$this->db->where('id',$xiddet[$index]);
					$this->db->update('tbendahara_pendapatan_detail', $data_detail);
				}else{
					$idtransaksi=$this->input->post('idtransaksi');
					$data_detail=array(
						'idtransaksi'=>$idtransaksi,
						'jenis_kas_id'=>$xjenis_kas_id[$index],
						'sumber_kas_id'=>$xsumber_kas_id[$index],
						'nominal_bayar'=>RemoveComma($xnominal_bayar[$index]),
						'idmetode'=>$xidmetode[$index],
						'ket_pembayaran'=>$ket[$index],
						// 'tanggal_pencairan'=>YMDFormat($tanggal_pencairan),
						'created_by'=>$this->session->userdata('user_id'),
						'created_date'=>date('Y-m-d H:i:s'),
					);
					$this->db->insert('tbendahara_pendapatan_detail', $data_detail);
				}
				
			}

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function getListUploadedDocument($idtransaksi)
    {
        $this->db->where('idtransaksi', $idtransaksi);
        $query = $this->db->get('tbendahara_pendapatan_dokumen');
        return $query->result();
    }
	function refresh_image($id){
		$q="SELECT  H.* from tbendahara_pendapatan_dokumen H

			WHERE H.idtransaksi='$id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		$no=1;
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/pendapatan/'.$r->filename.'" target="_blank">'.$r->filename.'</a></td>';
			$tabel .='<td class="text-left">'.$r->keterangan.'</td>';
			$tabel .='<td class="text-left">'.$r->upload_by_nama.'-'.HumanDateLong($r->upload_date).'</td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/pendapatan/'.$r->filename.'" target="_blank" data-toggle="tooltip" title="Preview" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                    <a href="#" data-urlindex="'.base_url().'tpendapatan/upload_document/'.$r->idtransaksi.'" data-urlremove="'.base_url().'tpendapatan/delete_file/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a></td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		return $tabel;
	}
	public function insert_jurnal_pendapatan($id){
	   
	   $q="SELECT H.id as idtransaksi
		,H.tanggal as tanggal_transaksi
		,H.notransaksi as notransaksi
		,H.idpendapatan,M.keterangan as nama_pendapatan,H.keterangan as keterangan_pendapatan
		,H.terimadari,mdari.nama as terimadar_nama,H.nominal as nominal_pendapatan
		,H.nominal as nominal_bayar
		,M.idakun as idakun_pendapatan
		,A.noakun as noakun_pendapatan
		,A.namaakun as namaakun_pendapatan,'K' as posisi_pendapatan,S.st_auto_posting
		FROM tbendahara_pendapatan H
		LEFT JOIN mpendapatan M ON M.id=H.idpendapatan
		LEFT JOIN makun_nomor A ON A.id=M.idakun
		LEFT JOIN mdari ON mdari.id=H.terimadari
		LEFT JOIN msetting_jurnal_pendapatan S ON S.id='1'
		WHERE H.id='$id'
		GROUP BY H.id";
		$row=$this->db->query($q)->row();
		$data_header=array(
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'idpendapatan' => $row->idpendapatan,
			'nama_pendapatan' => $row->nama_pendapatan,
			'keterangan_pendapatan' => $row->keterangan_pendapatan,
			'terimadari' => $row->terimadari,
			'terimadar_nama' => $row->terimadar_nama,
			'nominal_pendapatan' => $row->nominal_pendapatan,
			'nominal_bayar' => $row->nominal_bayar,
			'idakun_pendapatan' => $row->idakun_pendapatan,
			'noakun_pendapatan' => $row->noakun_pendapatan,
			'namaakun_pendapatan' => $row->namaakun_pendapatan,
			'posisi_pendapatan' => $row->posisi_pendapatan,
			'status' => 1,
			'st_posting' => 0,
			'st_auto_posting' => $row->st_auto_posting,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
            'created_date'=>date('Y-m-d H:i:s')
			

		);
		$st_auto_posting=$row->st_auto_posting;
		$this->db->insert('tvalidasi_pendapatan',$data_header);
		$idvalidasi=$this->db->insert_id();
		
		
		// //INSERT PEMBAYARAN
			$q="SELECT 
				D.idtransaksi as idtransaksi,D.id as iddet,M.idakun
				,A.noakun,A.namaakun,D.nominal_bayar as nominal,'D' as posisi_akun
				,CONCAT('PENDAPATAN NO.REG : ',H.notransaksi,' | ',MP.keterangan)  as keterangan


				FROM tbendahara_pendapatan_detail D
				LEFT JOIN tbendahara_pendapatan H ON H.id=D.idtransaksi
				LEFT JOIN mpendapatan MP ON MP.id=H.idpendapatan
				LEFT JOIN msumber_kas M ON M.id=D.sumber_kas_id
				LEFT JOIN makun_nomor A ON A.id=M.idakun
				WHERE D.idtransaksi='$id' AND D.`status`='1'
			";			
		
		// print_r($q);exit();
		$rows=$this->db->query($q)->result();
		foreach($rows as $r){
			$data_detail=array(
				'idvalidasi' => $idvalidasi ,
				'idtransaksi' => $id,
				'iddet' => $r->iddet,
				'idakun' => $r->idakun,
				'noakun' => $r->noakun,
				'namaakun' => $r->namaakun,
				'nominal' => $r->nominal,
				'posisi_akun' => $r->posisi_akun,
				'keterangan' => $r->keterangan,
				'st_biaya_tf' => 0,
				
			);
			$this->db->insert('tvalidasi_pendapatan_bayar',$data_detail);
		}
		if ($st_auto_posting=='1'){
			$data_header=array(
				
				'st_posting' => 1,
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
				

			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_pendapatan',$data_header);
		}
		return true;
   }
    // public function updateData($idtransaksi)
    // {
        // $this->db->select('tbendahara_pendapatan.*, makun_nomor.namaakun');
        // $this->db->join('makun_nomor', 'makun_nomor.noakun = tbendahara_pendapatan.noakun');
        // $this->db->where('tbendahara_pendapatan.id', $idtransaksi);
        // $query = $this->db->get('tbendahara_pendapatan');
        // $resultHead = $query->row();

        // if ($resultHead) {
            // $nourut = 1;

            // $data = array(
            // 'idtransaksi' => $idtransaksi,
            // 'tanggal' => $resultHead->tanggal,
            // 'bukti' => '',
            // 'namaakun' => $resultHead->namaakun,
            // 'noakun' => $resultHead->noakun,
            // 'kodebantu' => '',
            // 'debit' => '0',
            // 'kredit' => RemoveComma($resultHead->kredit),
            // 'keterangan' => $resultHead->keterangan.' Dari '.$resultHead->terimadari,
            // 'nourut' => $nourut,
          // );

            // if ($this->db->insert('tjurnal_pendapatan_lainlain', $data)) {
                // $this->db->select('tbendahara_pendapatan_detail.*, makun_nomor.namaakun');
                // $this->db->join('makun_nomor', 'makun_nomor.noakun = tbendahara_pendapatan_detail.noakun');
                // $this->db->where('tbendahara_pendapatan_detail.idtransaksi', $idtransaksi);
                // $query = $this->db->get('tbendahara_pendapatan_detail');
                // $resultDetail = $query->result();

                // foreach ($resultDetail as $key => $row) {
                    // $nourut = ($nourut + 1);
                    // $data = array(
                // 'idtransaksi' => $idtransaksi,
                // 'tanggal' => $resultHead->tanggal,
                // 'bukti' => '',
                // 'namaakun' => $row->namaakun,
                // 'noakun' => $row->noakun,
                // 'kodebantu' => '',
                // 'debit' => RemoveComma($row->debit),
                // 'kredit' => '0',
                // 'keterangan' => $resultHead->keterangan.' Dari '.$resultHead->terimadari,
                // 'nourut' => ($nourut + 1),
              // );

                    // $this->db->insert('tjurnal_pendapatan_lainlain', $data);
                // }
            // }

            // $this->db->set('statusjurnal', 1);
            // $this->db->where('id', $idtransaksi);
            // if ($this->db->update('tbendahara_pendapatan')) {
                // return true;
            // } else {
                // return false;
            // }
        // }
    // }
}
