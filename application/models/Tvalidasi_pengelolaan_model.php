<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tvalidasi_pengelolaan_model extends CI_Model
{
   
   public function getHeader($id){
	   $q="SELECT * from tvalidasi_pengelolaan 
				WHERE tvalidasi_pengelolaan.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function saveData(){
	   // print_r($this->input->post());exit();
		$id=($this->input->post('id'));
		$st_posting=($this->input->post('st_posting'));
		$btn_simpan=($this->input->post('btn_simpan'));
		
		$iddet=($this->input->post('iddet'));
		$idakun=($this->input->post('idakun'));
		$idtabel=($this->input->post('idtabel'));
		foreach($iddet as $index => $val){
			$nama_tabel=$idtabel[$index];
			$detail=array(
				'idakun'=>$idakun[$index],				
			);
			$this->db->where('id',$val);
			$this->db->update($nama_tabel,$detail);
		}
		
		if ($st_posting=='1'){
			$this->db->where('id',$id);
			$this->db->update('tvalidasi_pengelolaan',array('st_posting'=>0));
		}
		if ($btn_simpan=='2'){
			$data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $id);
			$result=$this->db->update('tvalidasi_pengelolaan', $data);
		}
		
		return true;
   }
   function get_nama_akun($id){
	   $q="SELECT *from makun_nomor A WHERE A.id='$id'";
	   return $this->db->query($q)->row();
   }
   function query_ringkasan($idvalidasi=''){
	   $q="
			SELECT H.idvalidasi,H.jenis_id,H.jenis_nama,H.metode_nama as keterangan,H.nominal_bayar as nominal,H.idakun,H.posisi_akun,'tvalidasi_pengelolaan_bayar' as tabel,H.id  FROM `tvalidasi_pengelolaan_bayar` H WHERE H.idvalidasi='$idvalidasi'
			UNION ALL
			SELECT H.idvalidasi,H.jenis_id,H.jenis_nama,H.nama_transaksi as keterangan,H.nominal as nominal,H.idakun,H.posisi_akun,'tvalidasi_pengelolaan_hutang' as tabel,H.id  FROM `tvalidasi_pengelolaan_hutang` H WHERE H.idvalidasi='$idvalidasi'
			UNION ALL
			SELECT H.idvalidasi,H.jenis_id,H.jenis_nama,H.nama_transaksi as keterangan,H.nominal as nominal,H.idakun,H.posisi_akun,'tvalidasi_pengelolaan_piutang' as tabel,H.id  FROM `tvalidasi_pengelolaan_piutang` H WHERE H.idvalidasi='$idvalidasi'
			UNION ALL
			SELECT H.idvalidasi,H.jenis_id,H.jenis_nama,H.nama_transaksi as keterangan,H.nominal as nominal,H.idakun,H.posisi_akun,'tvalidasi_pengelolaan_pendapatan' as tabel,H.id  FROM `tvalidasi_pengelolaan_pendapatan` H WHERE H.idvalidasi='$idvalidasi'
			UNION ALL
			SELECT H.idvalidasi,H.jenis_id,H.jenis_nama,CONCAT(H.nopermintaan,'-',H.nama_barang) as keterangan,H.totalkeseluruhan as nominal,H.idakun,H.posisi_akun,'tvalidasi_pengelolaan_permintaan' as tabel,H.id  FROM `tvalidasi_pengelolaan_permintaan` H WHERE H.idvalidasi='$idvalidasi'
	   ";
	   return $q;
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
