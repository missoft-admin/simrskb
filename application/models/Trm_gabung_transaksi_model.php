<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_gabung_transaksi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getTransaksi($tanggalawal='', $tanggalakhir='', $idtipetransaksi='')
    {
        $query = $this->db->query("SELECT
        	'rawatinap' AS reference,
        	view_gabung_transaksi_ranap.id AS idtransaksi,
          view_gabung_transaksi_ranap.noregis AS notransaksi,
          view_gabung_transaksi_ranap.idpasien,
          view_gabung_transaksi_ranap.nama AS namapasien,
          view_gabung_transaksi_ranap.nomedrec,
          view_gabung_transaksi_ranap.tanggallahir,
          view_gabung_transaksi_ranap.jeniskelamin,
          view_gabung_transaksi_ranap.alamat AS alamatpasien,
          view_gabung_transaksi_ranap.tanggal AS tanggaltransaksi,
          ( CASE WHEN ( view_gabung_transaksi_ranap.tipe = 1 ) THEN 'Rawat Inap' WHEN ( view_gabung_transaksi_ranap.tipe = 2 ) THEN 'One Day Surgery (ODS)' END ) AS tipetransaksi,
          view_gabung_transaksi_ranap.kelompokpasien AS namakelompokpasien,
          view_gabung_transaksi_ranap.namadokter AS namadokter
        FROM
        	view_gabung_transaksi_ranap UNION ALL
        SELECT
        	'rawatjalan' AS reference,
        	view_gabung_transaksi_rajal.id AS idtransaksi,
          view_gabung_transaksi_rajal.noregis AS notransaksi,
          view_gabung_transaksi_rajal.idpasien,
          view_gabung_transaksi_rajal.nama AS namapasien,
          view_gabung_transaksi_rajal.nomedrec,
          view_gabung_transaksi_rajal.tanggallahir,
          view_gabung_transaksi_rajal.jeniskelamin,
          view_gabung_transaksi_rajal.alamat AS alamatpasien,
          view_gabung_transaksi_rajal.tanggal AS tanggaltransaksi,
          'Rawat Jalan' AS tipetransaksi,
          view_gabung_transaksi_rajal.kelompokpasien AS namakelompokpasien,
          view_gabung_transaksi_rajal.namadokter AS namadokter
        FROM
        	view_gabung_transaksi_rajal");

        return $query->result();
    }

    public function saveData() {
        $this->jenistransaksi_lama  = $_POST['jenistransaksi'];
        $this->idtransaksi_lama     = $_POST['idtransaksi'];
        $this->jenistransaksi_baru  = $_POST['jenistransaksi_new'];
        $this->idtransaksi_baru     = $_POST['idtransaksi_new'];
        $this->alasan               = $_POST['alasan'];
        $this->created_at           = date("Y-m-d H:i:s");
        $this->created_by           = $this->session->userdata('user_id');

        if ($this->db->insert('trm_gabung_transaksi', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
