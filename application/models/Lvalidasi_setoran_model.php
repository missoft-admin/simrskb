<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Lvalidasi_setoran_model extends CI_Model
{
   public function list_akun(){
	   $q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		return $list;
   }
   public function getHeader($id){
	   $q="SELECT * from tvalidasi_setoran 
				WHERE tvalidasi_setoran.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
  
   function get_nama_akun($id){
	   $q="SELECT *from makun_nomor A WHERE A.id='$id'";
	   return $this->db->query($q)->row();
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
