<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpengaturan_jatuh_tempo_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('msetting_pembayaran_rumahsakit');
        return $query->row();
    }

    public function getDetailJatuhTempo($idsetting)
    {
        $this->db->select("setting.*, CONCAT(musers.name, ' - ', setting.created_at) AS userinfo");
        $this->db->join('musers', 'musers.id = setting.created_by');
        $this->db->where('setting.idsetting', $idsetting);
        $query = $this->db->get('msetting_pembayaran_rumahsakit_jatuhtempo setting');
        return $query->result();
    }

    public function getDetailRujukan($idsetting)
    {
        $this->db->where('idsetting', $idsetting);
        $query = $this->db->get('msetting_pembayaran_rumahsakit_rujukan');
        return $query->result();
    }

    public function saveData()
    {
        $this->nama = $_POST['nama'];

        if ($this->db->insert('msetting_pembayaran_rumahsakit', $this)) {
            $idsetting = $this->db->insert_id();
            $idtemporary = $this->input->post('idtemp');

            $this->updateIdTempJatuhTempo($idsetting, $idtemporary);
            $this->updateIdTempRujukan($idsetting, $idtemporary);

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama = $_POST['nama'];

        if ($this->db->update('msetting_pembayaran_rumahsakit', $this, array('id' => $_POST['id']))) {
            $idsetting = $this->input->post('id');
            $idtemporary = $this->input->post('idtemp');

            $this->updateIdTempJatuhTempo($idsetting, $idtemporary);
            $this->updateIdTempRujukan($idsetting, $idtemporary);

            return true;

        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('msetting_pembayaran_rumahsakit', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateIdTempJatuhTempo($idsetting, $idtemporary)
    {
        $this->db->set('idsetting', $idsetting);
        $this->db->where('idsetting', $idtemporary);
        $this->db->update('msetting_pembayaran_rumahsakit_jatuhtempo');
        return true;
    }

    public function updateIdTempRujukan($idsetting, $idtemporary)
    {
        $this->db->set('idsetting', $idsetting);
        $this->db->where('idsetting', $idtemporary);
        $this->db->update('msetting_pembayaran_rumahsakit_rujukan');
        return true;
    }
}
