<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Antrian_caller_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function list_counter(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT H.id,H.nama_counter,H.st_login,H.user_login,H.st_istirahat 
		FROM `antrian_pelayanan_counter` H
		INNER JOIN antrian_pelayanan_counter_user U ON U.counter_id=H.id AND U.userid='$user_id'
		WHERE H.`status`='1' AND (H.user_login='$user_id' OR H.user_login='' OR H.user_login IS NULL)";
		return $this->db->query($q)->result();
	}
	public function list_counter2(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT *FROM antrian_pelayanan_counter H WHERE H.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_data_login(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT H.tanggal_login,H.st_login,H.nama_counter,H.st_istirahat  FROM antrian_pelayanan_counter H WHERE H.user_login='$user_id'";
		return $this->db->query($q)->row_array();
	}
	
}
