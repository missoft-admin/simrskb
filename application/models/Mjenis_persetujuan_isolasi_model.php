<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mjenis_persetujuan_isolasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	public function getSpecified($id)
    {
        $q="SELECT 
			*
			from mjenis_persetujuan_isolasi M
					
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	
    public function saveData()
    {
		
        $this->label_eng 	= $_POST['label_eng'];		
        $this->label_ina 	= $_POST['label_ina'];		
        $this->nama 	= $_POST['nama'];		
        $this->staktif 	= 1;		
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
        if ($this->db->insert('mjenis_persetujuan_isolasi', $this)) {
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
		
		$template_id=$_POST['id'];
        $this->nama 	= $_POST['nama'];	
		$this->label_eng 	= $_POST['label_eng'];		
        $this->label_ina 	= $_POST['label_ina'];		
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
        if ($this->db->update('mjenis_persetujuan_isolasi', $this, array('id' => $_POST['id']))) {
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	
	public function remove_image($id)
    {
		$q="select gambar_tubuh From mjenis_persetujuan_isolasi H WHERE H.id='$id'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/jenis_persetujuan_isolasi/'.$row->gambar_tubuh) && $row->gambar_tubuh !='') {
            unlink('./assets/upload/jenis_persetujuan_isolasi/'.$row->gambar_tubuh);
        }
    }
    public function softDelete($id)
    {
        $this->staktif = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mjenis_persetujuan_isolasi', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function pilih($id)
    {
		$q="UPDATE mjenis_persetujuan_isolasi SET st_default='0' WHERE st_default='1'";
		$this->db->query($q);
        $this->st_default = 1;		
		
        if ($this->db->update('mjenis_persetujuan_isolasi', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->staktif = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mjenis_persetujuan_isolasi', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
