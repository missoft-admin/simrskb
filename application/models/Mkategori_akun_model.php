<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mkategori_akun_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mkategori_akun');
        return $query->row();
    }
	public function list_kategori($idkategori='')
	{
		$q="SELECT * FROM `mkategori_akun` H WHERE H.`status`='1'";
		return $this->db->query($q)->result();
	}
	public function list_header($idkategori='')
	{
		$where='';
		if ($idkategori !=''){
			$where=" AND H.idkategori='$idkategori' ";
		}
		$q="SELECT * FROM `makun_nomor` H WHERE H.`status`='1'  ".$where;
		return $this->db->query($q)->result();
	}
	public function list_akun($idkategori='')
	{
		$where='';
		if ($idkategori !=''){
			$where=" AND H.idkategori='$idkategori' ";
		}
		$q="SELECT * FROM `makun_nomor` H WHERE H.`status`='1'  ".$where;
		return $this->db->query($q)->result();
	}
    public function saveData()
    {
        $this->nama = $_POST['nama'];
        $this->jikabertambah = $_POST['jikabertambah'];
        $this->jikaberkurang = $_POST['jikaberkurang'];
        $this->possaldo = $_POST['possaldo'];
        $this->poslaporan = $_POST['poslaporan'];
        
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_at  = date('Y-m-d H:i:s');

        if ($this->db->insert('mkategori_akun', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama = $_POST['nama'];
		$this->jikabertambah = $_POST['jikabertambah'];
		$this->jikaberkurang = $_POST['jikaberkurang'];
		$this->possaldo = $_POST['possaldo'];
		$this->poslaporan = $_POST['poslaporan'];

		$this->updated_by  = $this->session->userdata('user_id');
		$this->updated_at  = date('Y-m-d H:i:s');

        if ($this->db->update('mkategori_akun', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_at  = date('Y-m-d H:i:s');

        if ($this->db->update('mkategori_akun', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
