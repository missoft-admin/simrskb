<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mheader_surat_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function get_data(){
		$q="select *from mheader_surat where id='1'";
		return $this->db->query($q)->row_array();
	}
	function list_jawaban(){
		$q="select *from merm_referensi where ref_head_id='16'";
		return $this->db->query($q)->result();
	}
	function list_content(){
		$q="SELECT H.*,C.`name` as user_created,E.`name` as user_edited,GROUP_CONCAT(R.ref SEPARATOR ' / ') as ref FROM mheader_surat_isi H
			LEFT JOIN musers C ON C.id=H.created_by
			LEFT JOIN musers E ON E.id=H.edited_by
			LEFT JOIN mheader_surat_isi_jawaban M ON M.general_isi_id=H.id
			LEFT JOIN merm_referensi R ON R.id=M.ref_id
			WHERE H.`status`='1'
			
			GROUP BY H.id
			ORDER BY H.`no` ASC";
		return $this->db->query($q)->result();
	}
	function save(){
		$id = $this->input->post('id');
		$this->alamat_form = $this->input->post('alamat_form');
		$this->telepone_form = $this->input->post('telepone_form');
		$this->email_form = $this->input->post('email_form');
		$this->nama_form = $this->input->post('nama_form');
		$this->kota_form = $this->input->post('kota_form');
		$this->header_color = $this->input->post('header_color');
		$this->footer_color = $this->input->post('footer_color');
	  
		$this->upload_logo_form(true);
		$this->db->where('id', $id);
			
		if ($this->db->update('mheader_surat', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	public function upload_logo_form($update = false)
    {
        if (!file_exists('assets/upload/app_setting')) {
            mkdir('assets/upload/app_setting', 0755, true);
        }

        if (isset($_FILES['logo_form'])) {
            if ($_FILES['logo_form']['name'] != '') {
                $config['upload_path'] = './assets/upload/app_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('logo_form')) {
                    $image_upload = $this->upload->data();
                    $this->logo_form = $image_upload['file_name'];

                    if ($update == true) {
                        $this->remove_image_logo_form($this->input->post('id'));
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
                return true;
            }
					// print_r($this->foto);exit;
        } else {
            return true;
        }
		
    }
	public function remove_image_logo_form($id)
    {
		$q="select logo_form From mheader_surat H WHERE H.id='$id'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/app_setting/'.$row->logo_form) && $row->logo_form !='') {
            unlink('./assets/upload/app_setting/'.$row->logo_form);
        }
    }
}
