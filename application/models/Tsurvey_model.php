<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
class Tsurvey_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function cetak_survey_kepuasan($assesmen_id,$tipe,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT *FROM setting_survey_kepuasan_label";
		$data=$this->db->query($q)->row_array();
			// print_r($data);exit;
		$q="
			SELECT 
			CASE WHEN H.st_ranap='1' THEN RI.no_medrec ELSE RJ.no_medrec END no_medrec
			,CASE WHEN H.st_ranap='1' THEN RI.namapasien ELSE RJ.namapasien END namapasien
			,CASE WHEN H.st_ranap='1' THEN CONCAT(RI.umurtahun,' Tahun ',RI.umurbulan,' Bulan ',RI.umurhari,' Hari') ELSE CONCAT(RJ.umurtahun,' Tahun ',RJ.umurbulan,' Bulan ',RJ.umurhari,' Hari') END umur
			,CASE WHEN H.st_ranap='1' THEN RI.tanggal_lahir ELSE RJ.tanggal_lahir END tanggal_lahir
			,CASE WHEN H.st_ranap='1' THEN RI.jenis_kelamin ELSE RJ.jenis_kelamin END jenis_kelamin
			,CASE WHEN H.st_ranap='1' THEN MDI.nama ELSE MDJ.nama END nama_dokter
			,H.*
			,CASE WHEN H.st_ranap='0' THEN MPOL.nama ELSE 
						CONCAT(COALESCE(mruangan.nama,''),' - ',COALESCE(mkelas.nama,''),' - ',COALESCE(mbed.nama,''))
					
					END as poli
					
			
			FROM `tpoliklinik_survey_kepuasan` H
			LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.pendaftaran_id_ranap AND H.st_ranap='1'
			LEFT JOIN tpoliklinik_pendaftaran RJ ON RJ.id=H.pendaftaran_id AND H.st_ranap='0'
			LEFT JOIN mpoliklinik MPOL ON MPOL.id=RJ.idpoliklinik
			LEFT JOIN mdokter MD ON MD.id=RJ.iddokter
			LEFT JOIN mruangan ON mruangan.id=RI.idruangan
			LEFT JOIN mbed ON mbed.id=RI.idbed
			LEFT JOIN mkelas ON mkelas.id=RI.idkelas
			LEFT JOIN mdokter MDJ ON MDJ.id=RJ.iddokter
			LEFT JOIN mdokter MDI ON MDI.id=RI.iddokterpenanggungjawab
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header_ina,judul_header_eng,judul_footer_ina,judul_footer_eng
			FROM setting_survey_kepuasan
			";
		$data_detail=$this->db->query($q)->row_array();
		if ($data_detail){
			
		}else{
			$data_detail=array();
		}
		$q="
			SELECT H.id,H.assesmen_id,H.parameter_nama,H.jawaban_id,M.deskripsi_nama,H.jawaban_skor,H.st_nilai FROM tpoliklinik_survey_kepuasan_param H
			LEFT JOIN msurvey_kepuasan_param_skor M ON M.id=jawaban_id
			WHERE H.assesmen_id='$assesmen_id' ORDER BY H.id
		";
		$data_list=$this->db->query($q)->result();
		$data['tipe']=$tipe;
		$data['data_list']=$data_list;
		$data=array_merge($data,$data_header,$data_detail);
		// print_r($data);exit;
        $data['title']=$data['judul_header_ina'] .' '.$data['no_medrec'];
        $nama_file=$data['judul_header_ina'] .' '.$data['no_medrec'];
		$data = array_merge($data, backend_info());
        $html = $this->load->view('Tsurvey/pdf_survey_kepuasan', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);exit;
		
        // (Optional) Setup the paper size and orientation
        // $dompdf->setPaper(array(0,0,609.4488,935.433), 'portrait');
		$dompdf->setPaper('A4', 'portrait');
        // Render the HTML as PDF
        $dompdf->render();
		if ($st_create=='1'){
			// $nama_file=$data['judul_per_ina'].' '.$data_transaksi->nopermintaan.'.pdf';
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
		}
        // // Output the generated PDF to Browser
	}
	function get_data_survey($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='1'){
			$q="SELECT *
			 FROM tpoliklinik_survey_kepuasan 
			 WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND st_ranap='1' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}else{
			$q="SELECT *
			 FROM tpoliklinik_survey_kepuasan 
			 WHERE pendaftaran_id='$pendaftaran_id_ranap' AND st_ranap='0' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function list_template_survey(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_survey_kepuasan H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function get_data_survey_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_survey_kepuasan WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_survey_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_survey_kepuasan_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_survey_kepuasan H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_survey_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_input as tanggal_input_asal,tanggal_survey as tanggal_survey_asal
				,ttd_hubungan as ttd_hubungan_asal,jenis_ttd as jenis_ttd_asal,total_skor_survey_kepuasan as total_skor_survey_kepuasan_asal,nama_kajian as nama_kajian_asal,isi_header as isi_header_asal,isi_footer as isi_footer_asal,nilai_tertimbang as nilai_tertimbang_asal,nilai_satuan as nilai_satuan_asal,nama_profile as nama_profile_asal,jk_profile as jk_profile_asal,mengetahui_rs as mengetahui_rs_asal,ttl_profile as ttl_profile_asal,pendidikan_profile as pendidikan_profile_asal,lainnya_profile as lainnya_profile_asal,umur_profile as umur_profile_asal,pekerjaan_profile as pekerjaan_profile_asal,tanggal_survey as tanggal_survey_asal,kritik as kritik_asal,hasil_penilaian as hasil_penilaian_asal,nilai_per_unsur as nilai_per_unsur_asal,jumlah_jawaban as jumlah_jawaban_asal,nrr_per_unsur as nrr_per_unsur_asal,nrr_tertimbang as nrr_tertimbang_asal,kepuasan_mas as kepuasan_mas_asal,flag_nilai_mas as flag_nilai_mas_asal,hasil_flag as hasil_flag_asal
				FROM tpoliklinik_survey_kepuasan_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function logic_akses_survey($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_survey,H.st_lihat as st_lihat_survey,H.st_edit as st_edit_survey,H.st_hapus as st_hapus_survey,H.st_cetak as st_cetak_survey
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_survey_kepuasan_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_survey_kepuasan_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_survey_kepuasan_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_survey'=>'0',
				'st_input_survey'=>'0',
				'st_edit_survey'=>'0',
				'st_hapus_survey'=>'0',
				'st_cetak_survey'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_survey(){
		$q="SELECT H.judul_header_ina,H.judul_footer_ina,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.judul_header_eng,H.judul_footer_eng
		FROM setting_survey H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function setting_survey_label(){
		$q="SELECT 
		logo,alamat_rs,phone_rs,web_rs,label_identitas_ina,pendidikan_profile_ina
		,mengetahui_lain_ina,mengetahui_lain_eng
		,pendidikan_profile_eng,label_identitas_eng,no_reg_ina,no_reg_eng,no_rm_ina,no_rm_eng,nama_pasien_ina,nama_pasien_eng,ttl_ina,ttl_eng,umur_ina,umur_eng,jk_ina,jk_eng,asal_pasien_ina,asal_pasien_eng,detail_ina,detail_eng,dokter_ina,dokter_eng,profile_ina,profile_eng,profile_nama_ina,profile_nama_eng,profile_nama_ina_ina,profile_nama_ina_eng,ttl_profile_ina,ttl_profile_eng,umur_profile_ina,umur_profile_eng,jk_profile_ina,jk_profile_eng,pekerjaan_profile_ina,pekerjaan_profile_eng,mengetahui_rs_ina,mengetahui_rs_eng,waktu_survey_ina,waktu_survey_eng,nama_survey_ina,nama_survey_eng,total_nilai_ina,total_nilai_eng,nilai_perunsur_ina,nilai_perunsur_eng,nrr_perunsur_ina,nrr_perunsur_eng,nrr_tertimbang_ina,nrr_tertimbang_eng,satuan_kepuasan_ina,satuan_kepuasan_eng,hasil_survey_ina,hasil_survey_eng,pertanyaan_ina,pertanyaan_eng,jawaban_ina,jawaban_eng,total_penilaian_ina,total_penilaian_eng,kritik_ina,kritik_eng,responden_ina,responden_eng,notes_footer_ina,notes_footer_eng
		FROM setting_survey_kepuasan_label H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
}
