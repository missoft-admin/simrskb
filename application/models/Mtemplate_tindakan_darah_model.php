<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mtemplate_tindakan_darah_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // public function getAll()
    // {
    //     $this->db->where('staktif', '1');
    //     $this->db->order_by('nama', 'ASC');
    //     $this->db->get('mtemplate_tindakan_darah');
    //     return $this->db->last_query();
    // }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mtemplate_tindakan_darah');
        return $query->row();
    }

    public function saveData()
    {
        $this->nama 					= $_POST['nama'];
        
        $this->staktif 			  = 1;
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mtemplate_tindakan_darah', $this)) {
            $tindakan_id=$this->db->insert_id();
			$q="
				INSERT  INTO mtemplate_tindakan_darah_informasi (template_id,tindakan_id,isi_informasi)
				SELECT '$tindakan_id' as template_id,H.id as tindakan_id,'' as isi_informasi FROM mjenis_info_darah H
				WHERE H.staktif='1'
			";
			$this->db->query($q);
			return $tindakan_id;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama 					= $_POST['nama'];
       
        $this->staktif 			  = 1;
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mtemplate_tindakan_darah', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->staktif = 0;
		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mtemplate_tindakan_darah', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
