<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trujukan_rumahsakit_verifikasi_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getTindakan($idpendaftaran)
	{
		$query = $this->db->query("SELECT * FROM view_index_verifikasi_transaksi_rujukan WHERE idpendaftaran = $idpendaftaran");
		return $query->row();
	}

	public function GetNominalRujukanRS($row)
	{
		$nominalRujukan = 0;
		if ($row->idkategori == 1) {
			if ($row->tentukantarif == 0) {
				if ($row->jenis_berekanan == 1) {
					$nominalRujukan = $row->jumlah * ($row->persentase / 100);
				} else if ($row->jenis_berekanan == 2) {
					$nominalRujukan = $row->ratetarif;
				}
			} else if ($row->tentukantarif == 1) {

				// Alkes Poli
				$nominalAlkesPoli = 0;
				if ($row->alkes_poli) {
					$nominalAlkesPoli = $this->model->totalRujukanRajalAlkes($row->idpendaftaran);
				}

				// Obat Poli
				$nominalObatPoli = 0;
				if ($row->obat_poli) {
					$nominalObatPoli = $this->model->totalRujukanRajalObat($row->idpendaftaran);
				}

				// Alkes Farmasi
				$nominalAlkesFarmasi = 0;
				if ($row->alkes_farmasi) {
					$nominalAlkesFarmasi = $this->model->totalRujukanFarmasi($row->idpendaftaran, 1);
				}

				// Obat Farmasi
				$nominalObatFarmasi = 0;
				if ($row->obat_farmasi) {
					$nominalObatFarmasi = $this->model->totalRujukanFarmasi($row->idpendaftaran, 3);
				}

				// Nominal Rujukan
				$nominalRujukanLab = $this->model->totalRujukanLaboratorium($row->idpendaftaran);
				$nominalRujukanRad = $this->model->totalRujukanRadiologi($row->idpendaftaran);
				$nominalRujukanFisio = $this->model->totalRujukanFisioterapi($row->idpendaftaran);
				$nominalTindakanPoli = $this->model->totalRujukanRajalTindakan($row->idpendaftaran);

				// Nominal Total Tindakan
				$nominalTotalTindakan = $nominalAlkesPoli + $nominalObatPoli + $nominalAlkesFarmasi + $nominalObatFarmasi + $nominalRujukanLab + $nominalRujukanRad + $nominalRujukanFisio + $nominalTindakanPoli;

				if ($row->jenis_berekanan == 1) {
					$nominalRujukan = $nominalTotalTindakan * ($row->persentase / 100);
				} else if ($row->jenis_berekanan == 2) {
					if ($nominalTotalTindakan > 0) {
						$nominalRujukan = $row->ratetarif;
					}
				}
			}
		}

		return $nominalRujukan;
	}

	public function totalRujukanRajalObat($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			SUM(tpoliklinik_obat.totalkeseluruhan) AS totalkeseluruhan
		FROM
			tpoliklinik_pendaftaran
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			INNER JOIN tpoliklinik_obat ON tpoliklinik_obat.idtindakan = tpoliklinik_tindakan.id
			INNER JOIN munitpelayanan ON munitpelayanan.id = tpoliklinik_obat.idunit
			LEFT JOIN mdata_obat ON mdata_obat.id = tpoliklinik_obat.idobat
		WHERE
			tpoliklinik_pendaftaran.id = $idpendaftaran
		");
		return $query->row() ? $query->row()->totalkeseluruhan : 0;
	}

	public function totalRujukanRajalAlkes($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			SUM(tpoliklinik_alkes.totalkeseluruhan) AS totalkeseluruhan
		FROM
			tpoliklinik_pendaftaran
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			INNER JOIN tpoliklinik_alkes ON tpoliklinik_alkes.idtindakan = tpoliklinik_tindakan.id
			INNER JOIN munitpelayanan ON munitpelayanan.id = tpoliklinik_alkes.idunit
			LEFT JOIN mdata_alkes ON mdata_alkes.id = tpoliklinik_alkes.idalkes
		WHERE
			tpoliklinik_pendaftaran.id = $idpendaftaran
		");
		return $query->row() ? $query->row()->totalkeseluruhan : 0;
	}

	public function totalRujukanRajalTindakan($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			(jasasarana + jasapelayanan + bhp + biayaperawatan) AS totalkeseluruhan
		FROM
		(
			SELECT
				SUM(CASE WHEN mrumahsakit_tarif.jasasarana = 1 THEN tpoliklinik_pelayanan.jasasarana ELSE 0 END) AS jasasarana,
				SUM(CASE WHEN mrumahsakit_tarif.jasapelayanan = 1 THEN tpoliklinik_pelayanan.jasapelayanan ELSE 0 END) AS jasapelayanan,
				SUM(CASE WHEN mrumahsakit_tarif.bhp = 1 THEN tpoliklinik_pelayanan.bhp ELSE 0 END) AS bhp,
				SUM(CASE WHEN mrumahsakit_tarif.biayaperawatan = 1 THEN tpoliklinik_pelayanan.biayaperawatan ELSE 0 END) AS biayaperawatan
			FROM
				tpoliklinik_pendaftaran
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			LEFT JOIN tpoliklinik_pelayanan ON tpoliklinik_pelayanan.idtindakan = tpoliklinik_tindakan.id
			LEFT JOIN mtarif_rawatjalan ON mtarif_rawatjalan.id = tpoliklinik_pelayanan.idpelayanan
			LEFT JOIN mdokter ON mdokter.id = tpoliklinik_pelayanan.iddokter
			LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
			JOIN mrumahsakit_tarif ON mrumahsakit_tarif.idrumahsakit = tpoliklinik_pendaftaran.idrujukan AND mrumahsakit_tarif.idtarif = mtarif_rawatjalan.id AND mrumahsakit_tarif.reference_table = 'mtarif_rawatjalan'
			WHERE
				tpoliklinik_pendaftaran.id = $idpendaftaran
				AND tpoliklinik_pelayanan.status != 0
		) AS result
		");
		return $query->row() ? $query->row()->totalkeseluruhan : 0;
	}

	public function totalRujukanLaboratorium($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			(jasasarana + jasapelayanan + bhp + biayaperawatan) AS totalkeseluruhan
		FROM
		(
			SELECT
				SUM(CASE WHEN mrumahsakit_tarif.jasasarana = 1 THEN trujukan_laboratorium_detail.jasasarana ELSE 0 END) AS jasasarana,
				SUM(CASE WHEN mrumahsakit_tarif.jasapelayanan = 1 THEN trujukan_laboratorium_detail.jasapelayanan ELSE 0 END) AS jasapelayanan,
				SUM(CASE WHEN mrumahsakit_tarif.bhp = 1 THEN trujukan_laboratorium_detail.bhp ELSE 0 END) AS bhp,
				SUM(CASE WHEN mrumahsakit_tarif.biayaperawatan = 1 THEN trujukan_laboratorium_detail.biayaperawatan ELSE 0 END) AS biayaperawatan
			FROM
				tpoliklinik_pendaftaran
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			LEFT JOIN trujukan_laboratorium ON trujukan_laboratorium.idtindakan = tpoliklinik_tindakan.id AND trujukan_laboratorium.asalrujukan IN ( 1, 2 )
			LEFT JOIN trujukan_laboratorium_detail ON trujukan_laboratorium_detail.idrujukan = trujukan_laboratorium.id
			LEFT JOIN mtarif_laboratorium ON mtarif_laboratorium.id = trujukan_laboratorium_detail.idlaboratorium
			LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_laboratorium_detail.iddokter, trujukan_laboratorium.iddokterperujuk)
			LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
			JOIN mrumahsakit_tarif ON mrumahsakit_tarif.idrumahsakit = tpoliklinik_pendaftaran.idrujukan AND mrumahsakit_tarif.idtarif = mtarif_laboratorium.id AND mrumahsakit_tarif.reference_table = 'mtarif_laboratorium'
			WHERE
				tpoliklinik_pendaftaran.id = $idpendaftaran
				AND trujukan_laboratorium_detail.statusrincianpaket = 0
				AND trujukan_laboratorium.status != 0
		) AS result
		");
		return $query->row() ? $query->row()->totalkeseluruhan : 0;
	}

	public function totalRujukanRadiologi($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			(jasasarana + jasapelayanan + bhp + biayaperawatan) AS totalkeseluruhan
		FROM
		(
			SELECT
				SUM(CASE WHEN mrumahsakit_tarif.jasasarana = 1 THEN trujukan_radiologi_detail.jasasarana ELSE 0 END) AS jasasarana,
				SUM(CASE WHEN mrumahsakit_tarif.jasapelayanan = 1 THEN trujukan_radiologi_detail.jasapelayanan ELSE 0 END) AS jasapelayanan,
				SUM(CASE WHEN mrumahsakit_tarif.bhp = 1 THEN trujukan_radiologi_detail.bhp ELSE 0 END) AS bhp,
				SUM(CASE WHEN mrumahsakit_tarif.biayaperawatan = 1 THEN trujukan_radiologi_detail.biayaperawatan ELSE 0 END) AS biayaperawatan
			FROM
				tpoliklinik_pendaftaran
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			LEFT JOIN trujukan_radiologi ON trujukan_radiologi.idtindakan = tpoliklinik_tindakan.id AND trujukan_radiologi.asalrujukan IN ( 1, 2 )
			LEFT JOIN trujukan_radiologi_detail ON trujukan_radiologi_detail.idrujukan = trujukan_radiologi.id
			LEFT JOIN mtarif_radiologi ON mtarif_radiologi.id = trujukan_radiologi_detail.idradiologi
			LEFT JOIN mtarif_radiologi_expose ON mtarif_radiologi.idexpose = mtarif_radiologi_expose.id
			LEFT JOIN mtarif_radiologi_film ON mtarif_radiologi.idfilm = mtarif_radiologi_film.id
			LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_radiologi_detail.iddokter, trujukan_radiologi.iddokterradiologi)
			LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
			JOIN mrumahsakit_tarif ON mrumahsakit_tarif.idrumahsakit = tpoliklinik_pendaftaran.idrujukan AND mrumahsakit_tarif.idtarif = mtarif_radiologi.id AND mrumahsakit_tarif.reference_table = 'mtarif_radiologi'
			WHERE
				tpoliklinik_pendaftaran.id = $idpendaftaran
				AND trujukan_radiologi.status != 0
		) AS result
		");
		return $query->row() ? $query->row()->totalkeseluruhan : 0;
	}

	public function totalRujukanFisioterapi($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			(jasasarana + jasapelayanan + bhp + biayaperawatan) AS totalkeseluruhan
		FROM
		(
			SELECT
				SUM(CASE WHEN mrumahsakit_tarif.jasasarana = 1 THEN trujukan_fisioterapi_detail.jasasarana ELSE 0 END) AS jasasarana,
				SUM(CASE WHEN mrumahsakit_tarif.jasapelayanan = 1 THEN trujukan_fisioterapi_detail.jasapelayanan ELSE 0 END) AS jasapelayanan,
				SUM(CASE WHEN mrumahsakit_tarif.bhp = 1 THEN trujukan_fisioterapi_detail.bhp ELSE 0 END) AS bhp,
				SUM(CASE WHEN mrumahsakit_tarif.biayaperawatan = 1 THEN trujukan_fisioterapi_detail.biayaperawatan ELSE 0 END) AS biayaperawatan
			FROM
				tpoliklinik_pendaftaran
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			LEFT JOIN trujukan_fisioterapi ON trujukan_fisioterapi.idtindakan = tpoliklinik_tindakan.id AND trujukan_fisioterapi.asalrujukan IN ( 1, 2 )
			JOIN trujukan_fisioterapi_detail ON trujukan_fisioterapi_detail.idrujukan = trujukan_fisioterapi.id
			LEFT JOIN mtarif_fisioterapi ON mtarif_fisioterapi.id = trujukan_fisioterapi_detail.idfisioterapi
			LEFT JOIN mdokter ON mdokter.id = COALESCE(trujukan_fisioterapi_detail.iddokter, trujukan_fisioterapi.iddokterperujuk)
			LEFT JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
			JOIN mrumahsakit_tarif ON mrumahsakit_tarif.idrumahsakit = tpoliklinik_pendaftaran.idrujukan AND mrumahsakit_tarif.idtarif = mtarif_fisioterapi.id AND mrumahsakit_tarif.reference_table = 'mtarif_fisioterapi'
			WHERE
				tpoliklinik_pendaftaran.id = $idpendaftaran
				AND trujukan_fisioterapi.STATUS != 0
		) AS result
		");
		return $query->row() ? $query->row()->totalkeseluruhan : 0;
	}

	public function totalRujukanFarmasi($idpendaftaran, $idtipe)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$query = $this->db->query("SELECT
			SUM(tpasien_penjualan_nonracikan.totalharga) AS totalkeseluruhan
		FROM
			tpoliklinik_pendaftaran
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			LEFT JOIN tpasien_penjualan ON tpasien_penjualan.idtindakan = tpoliklinik_tindakan.id AND tpasien_penjualan.asalrujukan IN ( 1, 2 )
			LEFT JOIN tpasien_penjualan_nonracikan ON tpasien_penjualan_nonracikan.idpenjualan = tpasien_penjualan.id AND tpasien_penjualan_nonracikan.status = 1
			LEFT JOIN view_barang ON view_barang.id = tpasien_penjualan_nonracikan.idbarang AND view_barang.idtipe = tpasien_penjualan_nonracikan.idtipe
			LEFT JOIN munitpelayanan ON munitpelayanan.id = tpasien_penjualan_nonracikan.idunit
		WHERE
			tpoliklinik_pendaftaran.id = $idpendaftaran
			AND tpasien_penjualan_nonracikan.idtipe = $idtipe
			AND tpasien_penjualan.status != 0
		");
		return $query->row() ? $query->row()->totalkeseluruhan : 0;
	}

	public function updateData()
    {
		$this->idasalpasien = $_POST['idasalpasien'];
		$this->idrujukan = $_POST['idrujukan'];
		$this->periode_pembayaran_feers = YMDFormat($_POST['periode_pembayaran']);
		$this->periode_jatuhtempo_feers = YMDFormat($_POST['periode_jatuhtempo']);

        if ($this->db->update('tpoliklinik_pendaftaran', $this, array('id' => $_POST['idtransaksi']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

	public function updateStatusVerifikasi($idpendaftaran)
    {
		$this->status_verifikasi_feers = 1;

        if ($this->db->update('tpoliklinik_pendaftaran', $this, array('id' => $idpendaftaran))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

	public function generateTransaksiRujukanRS($dataPendaftaran)
	{
		$idpendaftaran = $dataPendaftaran['idtransaksi'];
		$nomedrec = $dataPendaftaran['nomedrec'];
		$namapasien = $dataPendaftaran['namapasien'];
		$idlayanan = $dataPendaftaran['idlayanan'];
		$layanan = $dataPendaftaran['layanan'];
		$idasalpasien = $dataPendaftaran['idasalpasien'];
		$asalpasien = $dataPendaftaran['asalpasien'];
		$idrujukan = $dataPendaftaran['idrujukan'];
		$idtiperujukan = $dataPendaftaran['idtiperujukan'];
		$namarujukan = $dataPendaftaran['namarujukan'];
		$totaltransaksi = $dataPendaftaran['totaltransaksi'];
		$nominalrujukan = $dataPendaftaran['nominalrujukan'];
		$tanggal_pemeriksaan = $dataPendaftaran['tanggal_pemeriksaan'];
		$periode_pembayaran = $dataPendaftaran['periode_pembayaran'];
		$periode_jatuhtempo = $dataPendaftaran['periode_jatuhtempo'];

		$tanggal_pembayaran = ($periode_pembayaran != '' ? $periode_pembayaran : getPembayaranFeeRujukanRS($idtiperujukan, $idrujukan));
		$tanggal_jatuhtempo = ($periode_jatuhtempo != '' ? $periode_jatuhtempo : getJatuhTempoFeeRujukanRS($idtiperujukan, $idrujukan));

		$this->db->set('periode_pembayaran_feers', $tanggal_pembayaran);
		$this->db->set('periode_jatuhtempo_feers', $tanggal_jatuhtempo);
		$this->db->where('id', $idpendaftaran);
		$this->db->update('tpoliklinik_pendaftaran');

		$isStopPeriode = $this->checkStatusFeeRujukanRSStopped($idasalpasien,$idrujukan, $tanggal_pembayaran);
		if ($isStopPeriode == null) {
			$idfeerujukan = $this->checkStatusFeeRujukanRS($idasalpasien, $idrujukan, $tanggal_pembayaran);

			if ($idfeerujukan == null) {
				$dataFeeRujukanRS = [
					'idasalpasien' => $idasalpasien,
					'idrujukan' => $idrujukan,
					'namarujukan' => $namarujukan,
					'idtipe' => $idtiperujukan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'nominal' => 0,
					'created_at' => date('Y-m-d H:i:s'),
					'created_by' => $this->session->userdata('user_id')
				];

				if ($this->db->insert('tfeerujukan_rumahsakit', $dataFeeRujukanRS)) {
					$idfeerujukan = $this->db->insert_id();
				}
			}
		}

		$dataDetailFeeRujukanRS = [
			'idfeerujukan' => $idfeerujukan,
			'idtransaksi' => $idpendaftaran,
			'nomedrec' => $nomedrec,
			'namapasien' => $namapasien,
			'idlayanan' => $idlayanan,
			'layanan' => $layanan,
			'idasalpasien' => $idasalpasien,
			'asalpasien' => $asalpasien,
			'idrujukan' => $idrujukan,
			'namarujukan' => $namarujukan,
			'totaltransaksi' => $totaltransaksi,
			'nominalrujukan' => $nominalrujukan,
			'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
			'tanggal_pembayaran' => $tanggal_pembayaran,
			'tanggal_jatuhtempo' => $tanggal_jatuhtempo
		];

		if ($this->checkFeeRujukanRSByDetail($dataDetailFeeRujukanRS) == '0') {
			$this->db->replace('tfeerujukan_rumahsakit_detail', $dataDetailFeeRujukanRS);
		}

		return true;
	}

	public function checkStatusFeeRujukanRS($idasalpasien, $idrujukan, $tanggal_pembayaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$this->db->where('idasalpasien', $idasalpasien);
		$this->db->where('idrujukan', $idrujukan);
		$this->db->where('tanggal_pembayaran', $tanggal_pembayaran);
		$this->db->where('status_stop', '0');
		$this->db->where('status', '1');
		$query = $this->db->get('tfeerujukan_rumahsakit');
		$row = $query->row();

		if ($query->num_rows() > 0) {
			return $row->id;
		} else {
			return null;
		}
	}

	public function checkStatusFeeRujukanRSStopped($idasalpasien, $idrujukan, $tanggal_pembayaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$this->db->where('idasalpasien', $idasalpasien);
		$this->db->where('idrujukan', $idrujukan);
		$this->db->where('tanggal_pembayaran', $tanggal_pembayaran);
		$this->db->where('status_stop', '1');
		$this->db->where('status', '1');
		$query = $this->db->get('tfeerujukan_rumahsakit');
		$row = $query->row();

		if ($query->num_rows() > 0) {
			return $row->id;
		} else {
			return null;
		}
	}

	public function checkFeeRujukanRSByDetail($data)
	{
		$this->db->where('idtransaksi', $data['idtransaksi']);
		$query = $this->db->get('tfeerujukan_rumahsakit_detail');
		$idfeerujukan = $query->row()->idfeerujukan;

		if ($query->num_rows() > 0) {
			$this->checkStatusStopFeeRujukanRSById($idfeerujukan);
		} else {
			return '0';
		}
	}

	public function checkStatusStopFeeRujukanRSById($idfeerujukan)
	{
		$this->db->where('id', $idfeerujukan);
		$this->db->where('status_stop', '1');
		$query = $this->db->get('tfeerujukan_rumahsakit');

		if ($query->num_rows() > 0) {
			return '1';
		} else {
			return '0';
		}
	}
}
