<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_pengelolaan_informasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $row = $this->db->select("*")
            ->join("trm_pengelolaan_informasi", "trm_pengelolaan_informasi.id = trm_pengelolaan_informasi_detail.idtransaksi")
            ->where("trm_pengelolaan_informasi_detail.idpengajuan_detail", $id)
            ->where("trm_pengelolaan_informasi_detail.status", '1')
            ->get("trm_pengelolaan_informasi_detail")
            ->row();

        return $row;
    }

    public function getDataKunjungan($idDetailKunjugan)
    {
        $result = $this->db->select("trm_pengajuan_informasi_detail.id,
                trm_layanan_berkas.no_medrec, 
                trm_layanan_berkas.namapasien AS nama_pasien, 
                trm_layanan_berkas.tanggal_lahir, 
                trm_layanan_berkas.tanggal_trx AS tanggal_kunjungan,
                (CASE 
                    WHEN trm_layanan_berkas.tujuan = 1 THEN 'Rawat Jalan'
                    WHEN trm_layanan_berkas.tujuan = 2 THEN 'Instalasi Gawat Darurat (IGD)'
                    WHEN trm_layanan_berkas.tujuan = 3 THEN 'Rawat Inap'
                    WHEN trm_layanan_berkas.tujuan = 4 THEN 'One Day Surgery (ODS)'
                END) AS jenis_kunjungan,
                mdokter.nama AS nama_dokter,
                mpengajuan_skd.nama AS jenis_pengajuan,
                trm_pengajuan_informasi.nama_pemohon,
                trm_pengajuan_informasi.keterangan,
                trm_pengajuan_informasi_detail.estimasi_selesai,
                trm_pengajuan_informasi_detail.nominal,
                musers.name AS verified_by,
                trm_pengajuan_informasi_detail.verified_at")
            ->join("trm_pengajuan_informasi", "trm_pengajuan_informasi.id = trm_pengajuan_informasi_detail.idtransaksi")
            ->join("mpengajuan_skd", "mpengajuan_skd.id = trm_pengajuan_informasi_detail.jenis_pengajuan")
            ->join("trm_layanan_berkas", "trm_layanan_berkas.id = trm_pengajuan_informasi_detail.idberkas")
            ->join("mdokter", "mdokter.id = trm_layanan_berkas.iddokter")
            ->join("musers", "musers.id = trm_pengajuan_informasi_detail.verified_by")
            ->where_in("trm_pengajuan_informasi_detail.id", $idDetailKunjugan)
            ->where("trm_pengajuan_informasi_detail.status", '1')
            ->get("trm_pengajuan_informasi_detail")
            ->result();

        return $result;
    }

    public function removeDataKunjungan($id)
    {
        $key = array_search($id, $_SESSION['data_pengajuan_informasi']);
        if($key !== false) {
            unset($_SESSION['data_pengajuan_informasi'][$key]);
            $_SESSION["data_pengajuan_informasi"] = array_values($_SESSION["data_pengajuan_informasi"]);
        }

        return true;
    }

    public function saveData()
    {
        $this->tanggal = YMDFormat($this->input->post('tanggal_penyerahan'));
        $this->nama_penerima = $this->input->post('nama_penerima');
        $this->keterangan = $this->input->post('keterangan');
        $this->created_at = date("Y-m-d H:i:s");
        $this->created_by = $this->session->userdata('user_id');

        if ($this->uploadFile()) {
            if ($this->db->insert('trm_pengelolaan_informasi', $this)) {
                $idtransaksi = $this->db->insert_id();
                $this->saveDataDetail($idtransaksi);
            }
        }

        // Reset Data Session
        $_SESSION['data_pengajuan_informasi'] = array();

        return true;
    }

    public function saveDataDetail($idtransaksi)
    {
        $dataPengajuan = $_SESSION['data_pengajuan_informasi'];
        foreach ($dataPengajuan as $idPengajuan) {
            $dataDetail = array();
            $dataDetail['idtransaksi'] = $idtransaksi;
            $dataDetail['idpengajuan_detail'] = $idPengajuan;
            if ($this->db->insert('trm_pengelolaan_informasi_detail', $dataDetail)) {
                $this->updateStatusPengajuan($idPengajuan);
            }
        }
    }

    public function updateStatusPengajuan($idPengajuan)
    {
        $this->db->set('status_pengajuan', '1');
        $this->db->where('id', $idPengajuan);
        $this->db->update('trm_pengajuan_informasi_detail');
    }

    public function uploadFile()
    {
        if (!file_exists('assets/upload/pengelolaan_informasi_medis')) {
            mkdir('assets/upload/pengelolaan_informasi_medis', 0755, true);
        }

        if (isset($_FILES['file'])) {
            if ($_FILES['file']['name'] != '') {
                $config['upload_path'] = './assets/upload/pengelolaan_informasi_medis';
                $config['allowed_types'] = 'jpg|jpeg|bmp|doc|docx|pdf|txt';
                $config['encrypt_name']  = true;

                $this->upload->initialize($config);

                if ($this->upload->do_upload('file')) {
                    $image_upload = $this->upload->data();
                    $this->file = $image_upload['file_name'];

                    return true;
                } else {
                    $this->error_message = $this->upload->display_errors();


                print_r($this->error_message);exit();
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}
