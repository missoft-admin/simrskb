<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mvariable_rekapan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('mvariable_rekapan.id', $id);
        $query = $this->db->get('mvariable_rekapan');
        return $query->row();
    }

    public function getSubData($id)
    {
        $this->db->where('idvariable', $id);
        $query = $this->db->get('mvariable_rekapan_sub');
        return $query->result();
    }

    public function getAkunData($id, $idtipeakun, $idtipevariable)
    {
        $this->db->select('mvariable_rekapan_akun.*, makun_nomor.namaakun');
        $this->db->join('makun_nomor', 'makun_nomor.noakun = mvariable_rekapan_akun.noakun');
        $this->db->where('mvariable_rekapan_akun.idvariable', $id);
        $this->db->where('mvariable_rekapan_akun.tipeakun', $idtipeakun);
        $this->db->where('mvariable_rekapan_akun.tipevariable', $idtipevariable);
        $query = $this->db->get('mvariable_rekapan_akun');
        return $query->result();
    }

    public function saveData()
    {
        $this->nama   = $_POST['nama'];
        $this->idtipe = $_POST['idtipe'];
        $this->idsub  = $_POST['idsub'];

        if ($this->db->insert('mvariable_rekapan', $this)) {
            $idvariable = $this->db->insert_id();

            $subdatalist = json_decode($_POST['subdatalist_value']);
            foreach ($subdatalist as $row) {
                $data = array();
                $data['idvariable'] = $idvariable;
                $data['nama']       = $row[0];

                $this->db->insert('mvariable_rekapan_sub', $data);
            }

            $pengeluarankredit_list = json_decode($_POST['pengeluarankredit_list_value']);
            foreach ($pengeluarankredit_list as $row) {
                $data = array();
                $data['idvariable']   = $idvariable;
                $data['noakun']       = $row[0];
                $data['tipeakun']     = 'K';
                $data['tipevariable'] = '1';

                $this->db->insert('mvariable_rekapan_akun', $data);
            }

            $pengeluarandebit_list = json_decode($_POST['pengeluarandebit_list_value']);
            foreach ($pengeluarandebit_list as $row) {
                $data = array();
                $data['idvariable']   = $idvariable;
                $data['noakun']       = $row[0];
                $data['tipeakun']     = 'D';
                $data['tipevariable'] = '1';

                $this->db->insert('mvariable_rekapan_akun', $data);
            }

            $pendapatankredit_list = json_decode($_POST['pendapatankredit_list_value']);
            foreach ($pendapatankredit_list as $row) {
                $data = array();
                $data['idvariable']   = $idvariable;
                $data['noakun']       = $row[0];
                $data['tipeakun']     = 'K';
                $data['tipevariable'] = '2';

                $this->db->insert('mvariable_rekapan_akun', $data);
            }

            $pendapatandebit_list = json_decode($_POST['pendapatandebit_list_value']);
            foreach ($pendapatandebit_list as $row) {
                $data = array();
                $data['idvariable']   = $idvariable;
                $data['noakun']       = $row[0];
                $data['tipeakun']     = 'D';
                $data['tipevariable'] = '2';

                $this->db->insert('mvariable_rekapan_akun', $data);
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama   = $_POST['nama'];
        $this->idtipe = $_POST['idtipe'];
        $this->idsub  = $_POST['idsub'];

        if ($this->db->update('mvariable_rekapan', $this, array('id' => $_POST['id']))) {
            $idvariable = $_POST['id'];

            $this->db->where('idvariable', $idvariable);
            if ($this->db->delete('mvariable_rekapan_sub')) {
                $subdatalist = json_decode($_POST['subdatalist_value']);
                foreach ($subdatalist as $row) {
                    $data = array();
                    $data['idvariable'] = $idvariable;
                    $data['nama']       = $row[0];

                    $this->db->insert('mvariable_rekapan_sub', $data);
                }
            }

            $this->db->where('tipeakun', 'K');
            $this->db->where('tipevariable', '1');
            $this->db->where('idvariable', $idvariable);
            if ($this->db->delete('mvariable_rekapan_akun')) {
                $pengeluarankredit_list = json_decode($_POST['pengeluarankredit_list_value']);
                foreach ($pengeluarankredit_list as $row) {
                    $data = array();
                    $data['idvariable']   = $idvariable;
                    $data['noakun']       = $row[0];
                    $data['tipeakun']     = 'K';
                    $data['tipevariable'] = '1';

                    $this->db->insert('mvariable_rekapan_akun', $data);
                }
            }

            $this->db->where('tipeakun', 'D');
            $this->db->where('tipevariable', '1');
            $this->db->where('idvariable', $idvariable);
            if ($this->db->delete('mvariable_rekapan_akun')) {
                $pengeluarandebit_list = json_decode($_POST['pengeluarandebit_list_value']);
                foreach ($pengeluarandebit_list as $row) {
                    $data = array();
                    $data['idvariable']   = $idvariable;
                    $data['noakun']       = $row[0];
                    $data['tipeakun']     = 'D';
                    $data['tipevariable'] = '1';

                    $this->db->insert('mvariable_rekapan_akun', $data);
                }
            }

            $this->db->where('tipeakun', 'K');
            $this->db->where('tipevariable', '2');
            $this->db->where('idvariable', $idvariable);
            if ($this->db->delete('mvariable_rekapan_akun')) {
                $pendapatankredit_list = json_decode($_POST['pendapatankredit_list_value']);
                foreach ($pendapatankredit_list as $row) {
                    $data = array();
                    $data['idvariable']   = $idvariable;
                    $data['noakun']       = $row[0];
                    $data['tipeakun']     = 'K';
                    $data['tipevariable'] = '2';

                    $this->db->insert('mvariable_rekapan_akun', $data);
                }
            }

            $this->db->where('tipeakun', 'D');
            $this->db->where('tipevariable', '2');
            $this->db->where('idvariable', $idvariable);
            if ($this->db->delete('mvariable_rekapan_akun')) {
                $pendapatandebit_list = json_decode($_POST['pendapatandebit_list_value']);
                foreach ($pendapatandebit_list as $row) {
                    $data = array();
                    $data['idvariable']   = $idvariable;
                    $data['noakun']       = $row[0];
                    $data['tipeakun']     = 'D';
                    $data['tipevariable'] = '2';

                    $this->db->insert('mvariable_rekapan_akun', $data);
                }
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
        return $status;
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('mvariable_rekapan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
