<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tbuku_besar_persediaan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function getTipe($idtipe){
		$q="SELECT GROUP_CONCAT(H.nama_tipe) as nama_tipe FROM mdata_tipebarang H WHERE H.id IN (".$idtipe.")";
		return $this->db->query($q)->row('nama_tipe');
			
	}
	function getKategori($idkategori){
		$q="SELECT GROUP_CONCAT(H.nama) as nama FROM mdata_kategori H WHERE H.id IN (".$idkategori.")";
		return $this->db->query($q)->row('nama');
			
	}
	function get_spesifik($id){
		$q="SELECT H.id,H.tanggal,H.nilai_persediaan FROM tbuku_besar_persediaan H
		WHERE H.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function tipe_pemesanan_biasa($tipe){
		if ($tipe=='1'){
			$tipe='PEMBELIAN';
		}
		if ($tipe=='2'){
			$tipe='RETUR';
			
		}
		if ($tipe=='3'){
			$tipe='PENGAJUAN';
		}
		if ($tipe=='4'){
			$tipe='PEMBAYARAN';
		}
		return $tipe;
	}
}
