<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpoliklinik_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllDokter()
    {
        $this->db->where('status', '1');
        $query = $this->db->get('mdokter');
        return $query->result();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mpoliklinik');
        return $query->row();
    }

    public function getSpecifiedDetail($id)
    {
        $this->db->select('mpoliklinik_dokter.iddokter, mdokter.nama AS namadokter');
        $this->db->where('idpoliklinik', $id);
        $this->db->join('mdokter', 'mdokter.id = mpoliklinik_dokter.iddokter');
        $query = $this->db->get('mpoliklinik_dokter');
        return $query->result();
    }

    public function saveData()
    {
        $this->id          = $_POST['id'];
        $this->nama        = $_POST['nama'];
        $this->idtipe      = $_POST['idtipe'];
        $this->statuslock  = $_POST['statuslock'];
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
        if ($this->db->replace('mpoliklinik', $this)) {
            $idpoliklinik = $this->db->insert_id();
            $this->db->where('idpoliklinik', $idpoliklinik);
            if ($this->db->delete('mpoliklinik_dokter')) {
                $detail_list = json_decode($_POST['detail_value']);
                foreach ($detail_list as $row) {
                    $detail = array();
                    $detail['idpoliklinik'] 	= $idpoliklinik;
                    $detail['iddokter']       = $row[1];

                    $this->db->insert('mpoliklinik_dokter', $detail);
                }
            }
        }

        return true;
    }

    public function softDelete($id)
    {
        $this->status = 0;
		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mpoliklinik', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
