<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mbank_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mbank');
        return $query->row();
    }

    public function saveData()
    {
        $this->nama 					= $_POST['nama'];
        $this->atasnama 			= $_POST['atasnama'];
        $this->norekening 		= $_POST['norekening'];
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');


        if ($this->db->insert('mbank', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama 					= $_POST['nama'];
        $this->atasnama 			= $_POST['atasnama'];
        $this->norekening 		= $_POST['norekening'];
		
		
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mbank', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mbank', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
