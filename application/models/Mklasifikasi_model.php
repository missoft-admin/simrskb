<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mklasifikasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('mklasifikasi.id', $id);
        $query = $this->db->get('mklasifikasi');
        return $query->row();
    }

    public function getAkunData($id)
    {
        $this->db->select('mklasifikasi_detail.*, makun_nomor.namaakun');
        $this->db->join('makun_nomor', 'makun_nomor.id = mklasifikasi_detail.idakun');
        $this->db->where('mklasifikasi_detail.idklasifikasi', $id);
        $query = $this->db->get('mklasifikasi_detail');
        return $query->result();
    }
	public function list_akun()
    {
        $this->db->select('makun_nomor.*');
        // $this->db->where('stheader', '0');
        $this->db->where('status', '1');
        $query = $this->db->get('makun_nomor');
        return $query->result();
    }

    public function saveData()
    {
        $this->nama   = $_POST['nama'];
        $this->idakun   = $_POST['idakun'];
		$this->idakun_diskon   = $_POST['idakun_diskon'];
		$this->idakun_ppn   = $_POST['idakun_ppn'];
    		$this->created_at  = date('Y-m-d H:i:s');
    		$this->created_by  = $this->session->userdata('user_id');

        if ($this->db->insert('mklasifikasi', $this)) {
            // $idklasifikasi = $this->db->insert_id();

            // $akunList = json_decode($_POST['akun_list_value']);
            // foreach ($akunList as $row) {
                // $data = array();
                // $data['idklasifikasi'] = $idklasifikasi;
                // $data['idakun']  = $row[0];
                // $data['created_at'] = date('Y-m-d H:i:s');
                // $data['created_by'] = $this->session->userdata('user_id');

                // $this->db->insert('mklasifikasi_detail', $data);
            // }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama   = $_POST['nama'];
		$this->idakun   = $_POST['idakun'];
		$this->idakun_diskon   = $_POST['idakun_diskon'];
		$this->idakun_ppn   = $_POST['idakun_ppn'];
    		$this->updated_at  = date('Y-m-d H:i:s');
    		$this->updated_by  = $this->session->userdata('user_id');

        if ($this->db->update('mklasifikasi', $this, array('id' => $_POST['id']))) {
            // $idklasifikasi = $_POST['id'];

            // $this->db->where('idklasifikasi', $idklasifikasi);
            // if ($this->db->delete('mklasifikasi_detail')) {
                // $akunList = json_decode($_POST['akun_list_value']);
                // foreach ($akunList as $row) {
                    // $data = array();
                    // $data['idklasifikasi'] = $idklasifikasi;
                    // $data['idakun']  = $row[0];
                    // $data['updated_at'] = date('Y-m-d H:i:s');
                    // $data['updated_by'] = $this->session->userdata('user_id');

                    // $this->db->insert('mklasifikasi_detail', $data);
                // }
            // }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
        return $status;
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
        $this->deleted_date  = date('Y-m-d H:i:s');
        if ($this->db->update('mklasifikasi', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
