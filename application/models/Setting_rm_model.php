<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_rm_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_rm_setting_label(){
		$q="SELECT * FROM setting_rm_label H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	public function get_rm_setting_label_per(){
		$q="SELECT * FROM setting_rm_label_perencanaan H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	public function get_rm_setting(){
		$q="SELECT * FROM setting_rm H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function save_rm_label(){
		// print_r($this->input->post());exit;
		$id =1;
		$this->pesan_informasi = $this->input->post('pesan_informasi');
		$this->judul_ina = $this->input->post('judul_ina');
		$this->judul_eng = $this->input->post('judul_eng');
		$this->waktu_ina = $this->input->post('waktu_ina');
		$this->waktu_eng = $this->input->post('waktu_eng');
		$this->waktu_req = ($this->input->post('waktu_req')=='on'?1:0);
		$this->tujuan_ina = $this->input->post('tujuan_ina');
		$this->tujuan_eng = $this->input->post('tujuan_eng');
		$this->tujuan_req = ($this->input->post('tujuan_req')=='on'?1:0);
		$this->dokter_ina = $this->input->post('dokter_ina');
		$this->dokter_eng = $this->input->post('dokter_eng');
		$this->dokter_req = ($this->input->post('dokter_req')=='on'?1:0);
		$this->sebanyak_ina = $this->input->post('sebanyak_ina');
		$this->sebanyak_eng = $this->input->post('sebanyak_eng');
		$this->sebanyak_req = ($this->input->post('sebanyak_req')=='on'?1:0);
		$this->diagnosa_ina = $this->input->post('diagnosa_ina');
		$this->diagnosa_eng = $this->input->post('diagnosa_eng');
		$this->diagnosa_req = ($this->input->post('diagnosa_req')=='on'?1:0);
		$this->selama_ina = $this->input->post('selama_ina');
		$this->selama_eng = $this->input->post('selama_eng');
		$this->selama_req = ($this->input->post('selama_req')=='on'?1:0);
		$this->kontrol_ina = $this->input->post('kontrol_ina');
		$this->kontrol_eng = $this->input->post('kontrol_eng');
		$this->kontrol_req = ($this->input->post('kontrol_req')=='on'?1:0);
		$this->detail_ina = $this->input->post('detail_ina');
		$this->detail_eng = $this->input->post('detail_eng');
		$this->detail_req = ($this->input->post('detail_req')=='on'?1:0);
		$this->prioritas_ina = $this->input->post('prioritas_ina');
		$this->prioritas_eng = $this->input->post('prioritas_eng');
		$this->prioritas_req = ($this->input->post('prioritas_req')=='on'?1:0);
		$this->catatan_ina = $this->input->post('catatan_ina');
		$this->catatan_eng = $this->input->post('catatan_eng');
		$this->catatan_req = ($this->input->post('catatan_req')=='on'?1:0);
		$this->ket_ina = $this->input->post('ket_ina');
		$this->ket_eng = $this->input->post('ket_eng');
		$this->ket_req = ($this->input->post('ket_req')=='on'?1:0);

		$this->db->where('id', $id);
			
		if ($this->db->update('setting_rm_label', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_rm_label_per(){
		// print_r($this->input->post());exit;
		$id =1;
		$this->kode = $this->input->post('kode');
		$this->judul_per_ina = $this->input->post('judul_per_ina');
		$this->judul_per_eng = $this->input->post('judul_per_eng');
		$this->paragraph_1_ina = $this->input->post('paragraph_1_ina');
		$this->paragraph_1_eng = $this->input->post('paragraph_1_eng');
		$this->paragraph_2_ina = $this->input->post('paragraph_2_ina');
		$this->paragraph_2_eng = $this->input->post('paragraph_2_eng');
		$this->nama_pasien_ina = $this->input->post('nama_pasien_ina');
		$this->nama_pasien_eng = $this->input->post('nama_pasien_eng');
		$this->ttl_ina = $this->input->post('ttl_ina');
		$this->ttl_eng = $this->input->post('ttl_eng');
		$this->diagnosa_per_ina = $this->input->post('diagnosa_per_ina');
		$this->diagnosa_per_eng = $this->input->post('diagnosa_per_eng');
		$this->fisio_ina = $this->input->post('fisio_ina');
		$this->fisio_eng = $this->input->post('fisio_eng');
		$this->sebanyak_per_ina = $this->input->post('sebanyak_per_ina');
		$this->sebanyak_per_eng = $this->input->post('sebanyak_per_eng');
		$this->selama_per_ina = $this->input->post('selama_per_ina');
		$this->selama_per_eng = $this->input->post('selama_per_eng');
		$this->kontrol_per_ina = $this->input->post('kontrol_per_ina');
		$this->kontrol_per_eng = $this->input->post('kontrol_per_eng');
		$this->paragraph_3_ina = $this->input->post('paragraph_3_ina');
		$this->paragraph_3_eng = $this->input->post('paragraph_3_eng');
		$this->footer_ina = $this->input->post('footer_ina');
		$this->footer_eng = $this->input->post('footer_eng');
		$this->upload_login_logo(true);

		$this->db->where('id', $id);
			
		if ($this->db->update('setting_rm_label_perencanaan', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	public function upload_login_logo($update = false)
    {
        if (!file_exists('assets/upload/app_setting')) {
            mkdir('assets/upload/app_setting', 0755, true);
        }

        if (isset($_FILES['logo'])) {
            if ($_FILES['logo']['name'] != '') {
                $config['upload_path'] = './assets/upload/app_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('logo')) {
                    $image_upload = $this->upload->data();
                    $this->logo = $image_upload['file_name'];

                    if ($update == true) {
                        $this->remove_image_logo(1);
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
                return true;
            }
					// print_r($this->foto);exit;
        } else {
            return true;
        }
		
    }
	public function remove_image_logo($id)
    {
		$q="select logo From setting_rm_label_perencanaan H WHERE H.id='1'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/app_setting/'.$row->login_logo) && $row->login_logo !='') {
            unlink('./assets/upload/app_setting/'.$row->login_logo);
        }
    }
	
}


