<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpengajuan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getNoPengajuan()
    {
        $this->db->like('nopengajuan', 'PG.' . date('y') . date('m'), 'after');
        $this->db->from('tpengajuan');
        $query = $this->db->count_all_results();

        if ($query > 0) {
            $autono = $query + 1;
            $autono = 'PG.' . date('y') . date('m') . str_pad($autono, 4, '0', STR_PAD_LEFT);
        } else {
            $autono = 'PG.' . date('y') . date('m') . '0001';
        }

        return $autono;
    }

//    public function getSpecified($id)
//    {
//        $this->db->select('tpengajuan.*,
//        musers.name AS namapemohon,
//        tpengajuan_persetujuan.memo,
//        mvendor.id AS idvendor,
//        mvendor.nama AS namavendor');
//        $this->db->join('musers', 'musers.id = tpengajuan.idpemohon');
//        $this->db->join('tpengajuan_persetujuan', 'tpengajuan_persetujuan.idpengajuan = tpengajuan.id', 'LEFT');
//        $this->db->join('mvendor', 'mvendor.id = tpengajuan.idvendor');
//        $this->db->where('tpengajuan.id', $id);
//        $this->db->order_by('tpengajuan_persetujuan.id', 'DESC');
//        $query = $this->db->get('tpengajuan');
//        return $query->row();
//    }

    public function getLastIdUlasan($id)
    {
        $this->db->select('MAX(tpengajuan_persetujuan.id) AS idpersetujuan');
        $this->db->where('tpengajuan_persetujuan.idpengajuan', $id);
        $query = $this->db->get('tpengajuan_persetujuan');
        return $query->row()->idpersetujuan;
    }

//    public function getPegawai()
//    {
//        $this->db->where('status', 1);
//        $query = $this->db->get('mpegawai');
//        return $query->result();
//    }

    public function getUsers($idpengajuan)
    {
        $sessionUserId = $this->session->userdata('user_id');

        $this->db->select('musers.id, musers.name');
        $this->db->join('tpengajuan_persetujuan_detail', 'tpengajuan_persetujuan_detail.idpenyetuju = musers.id', 'LEFT');
        $this->db->join('tpengajuan_persetujuan', 'tpengajuan_persetujuan.id = tpengajuan_persetujuan_detail.idpersetujuan', 'LEFT');
        $this->db->join('tpengajuan', 'tpengajuan.id = tpengajuan_persetujuan.idpengajuan AND tpengajuan.id = ' . $idpengajuan, 'LEFT');
        $this->db->where('musers.id <> (SELECT idpemohon FROM tpengajuan WHERE id=' . $idpengajuan . ')');
        $this->db->where('musers.id <> ', $sessionUserId);
        $this->db->where('tpengajuan.id IS NULL');
        $this->db->where('musers.status', 1);
        $this->db->group_by('musers.id');
        $query = $this->db->get('musers');
        return $query->result();
    }

    public function getUsersBendahara()
    {
        $this->db->select('musers.id, musers.name');
        $this->db->where('musers.idpermission', 4);
        $this->db->where('musers.status', 1);
        $query = $this->db->get('musers');
        return $query->result();
    }

    public function getUnitPelayanan()
    {
        $this->db->where('status', 1);
        $query = $this->db->get('munitpelayanan');
        return $query->result();
    }

    public function getJenisKeperluan()
    {
        $this->db->where('status', 1);
        $query = $this->db->get('mjenis_keperluan');
        return $query->result();
    }

    public function getVendor()
    {
        $this->db->where('status', 1);
        $query = $this->db->get('mvendor');
        return $query->result();
    }

    public function getSatuan()
    {
        $this->db->where('status', 1);
        $query = $this->db->get('msatuan');
        return $query->result();
    }

    /* it's right &start */
    public function getDetailPengajuan($id)
    {
        $this->db->select('tpengajuan.*, tpengajuan_persetujuan.id as idpersetujuan, tpengajuan_persetujuan.memo, musers.name as namapemohon');
        $this->db->join('musers', 'musers.id = tpengajuan.idpemohon');
        $this->db->join('tpengajuan_persetujuan', 'tpengajuan_persetujuan.idpengajuan = tpengajuan.id', 'LEFT');

        $this->db->where('tpengajuan.id', $id);
        $this->db->where('tpengajuan.status', 1);

        $this->db->order_by('tpengajuan_persetujuan.id', 'desc');
        $query = $this->db->get('tpengajuan');
        return $query->row();
    }

    public function getDetailBarang($id)
    {
        $this->db->select('tpengajuan_barang.*, msatuan.nama AS namasatuan');
        $this->db->join('msatuan', 'msatuan.id = tpengajuan_barang.idsatuan');

        $this->db->where(
            array(
                'tpengajuan_barang.idpengajuan' => $id,
                'tpengajuan_barang.status' => 1
            )
        );

        $query = $this->db->get('tpengajuan_barang');
        return $query->result();
    }

    /* it's right &send */

    public function getDetailTerminProgress($idpengajuan)
    {
        $this->db->where('idpengajuan', $idpengajuan);
        $this->db->where('statusaktivasi1 <>', 0);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('tpengajuan_termin_progress');
        return $query->result();
    }

    public function getDetailTerminFix($idpengajuan)
    {
        $this->db->where('idpengajuan', $idpengajuan);
        $this->db->where('statusaktivasi1 <>', 0);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('tpengajuan_termin_fix');
        return $query->result();
    }

    public function getDetailLampiran($idpengajuan)
    {
        $this->db->where('tpengajuan_lampiran.idpengajuan', $idpengajuan);
        $this->db->where('tpengajuan_lampiran.status', 1);
        $query = $this->db->get('tpengajuan_lampiran');
        return $query->result();
    }

    /* it's right &start */
    public function saveData()
    {
        $this->nopengajuan = $this->getNoPengajuan();
        $this->tanggal = YMDFormat($_POST['tanggal']);
        $this->idpemohon = $_POST['idpemohon'];
        $this->subjek = $_POST['subjek'];
        $this->untukbagian = $_POST['untukbagian'];
        $this->diperlukanuntuk = $_POST['diperlukanuntuk'];
        $this->tanggaldibutuhkan = YMDFormat($_POST['tanggaldibutuhkan']);
        $this->jenispembayaran = $_POST['jenispembayaran'];
        $this->idvendor = $_POST['idvendor'];

        if ($this->jenispembayaran == 2) {
            $this->tanggalkontrabon = YMDFormat($_POST['tanggalkontrabon']);
        } elseif ($this->jenispembayaran == 3) {
            $this->norekening = $_POST['norekening'];
            $this->bank = $_POST['bank'];
        } elseif ($this->jenispembayaran == 5) {
            $this->tanggaldownpayment = YMDFormat($_POST['tanggaldownpayment']);
            $this->downpayment = RemoveComma($_POST['downpayment']);
            $this->cicilan = RemoveComma($_POST['cicilan']);
            $this->nominalcicilan = RemoveComma($_POST['nominalcicilan']);
        }

        $this->fakturpajak = (isset($_POST['fakturpajak']) ? 1 : 0);
        $this->totalnominal = RemoveComma($_POST['totalnominal']);

        if ($this->db->insert('tpengajuan', $this)) {
            $idpengajuan = $this->db->insert_id();
            $detail_list = json_decode($_POST['detailValue']);
            foreach ($detail_list as $row) {
                $data = array();
                $data['idpengajuan'] = $idpengajuan;
                $data['namabarang'] = $row[0];
                $data['merk'] = $row[1];
                $data['kuantitas'] = $row[2];
                $data['idsatuan'] = $row[3];
                $data['harga'] = RemoveComma($row[5]);
                $data['jumlah'] = RemoveComma($row[6]);
                $data['keterangan'] = $row[7];

                $this->db->insert('tpengajuan_barang', $data);
            }

            $termin_list = json_decode($_POST['terminValue']);
            foreach ($termin_list as $row) {
                $data = array();
                $data['idpengajuan'] = $idpengajuan;
                $data['subjek'] = $row[0];
                $data['deskripsi'] = $row[1];
                $data['nominal'] = RemoveComma($row[2]);
                $data['tanggal'] = YMDFormat($row[3]);

                $this->db->insert('tpengajuan_termin_progress', $data);
            }

            // Termin Fix
            if ($this->jenispembayaran == 5) {
                $data = array();
                $data['idpengajuan'] = $idpengajuan;
                $data['deskripsi'] = 'Down Payment';
                $data['nominal'] = RemoveComma($_POST['downpayment']);
                $data['tanggal'] = YMDFormat($_POST['tanggal']);

                if ($this->db->insert('tpengajuan_termin_fix', $data)) {
                    for ($i = 1; $i <= $this->cicilan; $i++) {
                        $tanggal = strtotime(YMDFormat($_POST['tanggal']));
                        $tanggalTempo = date("Y-m-d", strtotime("+$i month", $tanggal));

                        $data = array();
                        $data['idpengajuan'] = $idpengajuan;
                        $data['deskripsi'] = 'Cicilan ' . $i;
                        $data['nominal'] = $this->nominalcicilan;
                        $data['tanggal'] = $tanggalTempo;

                        $this->db->insert('tpengajuan_termin_fix', $data);
                    }
                }
            }

            if (isset($_FILES['attachment'])) {
                // Attachment File
                // Configure upload.
                $this->upload->initialize(array(
                    "upload_path" => "./assets/upload/pengajuan",
                    "allowed_types" => '*',
                    "encrypt_name" => true
                ));

                //Perform upload.
                if ($this->upload->do_multi_upload("attachment")) {
                    $result = $this->upload->get_multi_data();
                    foreach ($result as $row) {
                        $data = array();
                        $data['idpengajuan'] = $idpengajuan;
                        $data['lampiran'] = $row['file_name'];

                        $this->db->insert('tpengajuan_lampiran', $data);
                    }
                }
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    /* it's right &end */

    public function updateData()
    {
        $this->tanggal = YMDFormat($_POST['tanggal']);
        $this->idpemohon = $_POST['idpemohon'];
        $this->subjek = $_POST['subjek'];
        $this->untukbagian = $_POST['untukbagian'];
        $this->diperlukanuntuk = $_POST['diperlukanuntuk'];
        $this->tanggaldibutuhkan = YMDFormat($_POST['tanggaldibutuhkan']);
        $this->jenispembayaran = $_POST['jenispembayaran'];
        $this->idvendor = $_POST['idvendor'];

        if ($this->jenispembayaran == 2) {
            $this->tanggalkontrabon = YMDFormat($_POST['tanggalkontrabon']);

        } elseif ($this->jenispembayaran == 3) {
            $this->norekening = $_POST['norekening'];
            $this->bank = $_POST['bank'];

        } elseif ($this->jenispembayaran == 5) {
            $this->tanggaldownpayment = YMDFormat($_POST['tanggaldownpayment']);
            $this->downpayment = RemoveComma($_POST['downpayment']);
            $this->cicilan = RemoveComma($_POST['cicilan']);
            $this->nominalcicilan = RemoveComma($_POST['nominalcicilan']);

        }

        $this->fakturpajak = (isset($_POST['fakturpajak']) ? 1 : 0);
        $this->totalnominal = RemoveComma(['totalnominal']);

        if ($this->db->update('tpengajuan', $this, array('id' => $_POST['id']))) {
            $total = 0;

            $this->db->where("idpengajuan", $_POST['id']);
            if ($this->db->delete("tpengajuan_barang")) {
                $detail_list = json_decode($_POST['detailValue']);
                foreach ($detail_list as $row) {
                    $data = array();
                    $data['idpengajuan'] = $_POST['id'];
                    $data['namabarang'] = $row[0];
                    $data['merk'] = $row[1];
                    $data['kuantitas'] = $row[2];
                    $data['idsatuan'] = (int)str_replace("\n", "", str_replace("\t", "", $row[3]));
                    $data['harga'] = RemoveComma($row[5]);
                    $data['jumlah'] = RemoveComma($row[6]);
                    $data['keterangan'] = $row[7];

                    $total += RemoveComma($row[6]);
                    $this->db->insert('tpengajuan_barang', $data);
                }
            }

            $this->db->where('id', $_POST['id']);
            $this->db->update('tpengajuan', ['totalnominal' => $total]);

            $this->db->where("idpengajuan", $_POST['id']);
            if ($this->db->delete("tpengajuan_termin_progress")) {
                $termin_list = json_decode($_POST['terminValue']);
                foreach ($termin_list as $row) {
                    $data = array();
                    $data['idpengajuan'] = $_POST['id'];
                    $data['subjek'] = $row[0];
                    $data['deskripsi'] = $row[1];
                    $data['nominal'] = RemoveComma($row[2]);
                    $data['tanggal'] = YMDFormat($row[3]);

                    $this->db->insert('tpengajuan_termin_progress', $data);
                }
            }

            // Termin Fix
            if ($this->jenispembayaran == 5) {
                $this->db->where("idpengajuan", $_POST['id']);
                if ($this->db->delete("tpengajuan_termin_fix")) {
                    $data = array();
                    $data['idpengajuan'] = $_POST['id'];
                    $data['deskripsi'] = 'Down Payment';
                    $data['nominal'] = RemoveComma($_POST['downpayment']);
                    $data['tanggal'] = YMDFormat($_POST['tanggal']);

                    if ($this->db->insert('tpengajuan_termin_fix', $data)) {
                        for ($i = 1; $i <= $this->cicilan; $i++) {
                            $tanggal = strtotime(YMDFormat($_POST['tanggal']));;
                            $tanggalTempo = date("Y-m-d", strtotime("+$i month", $tanggal));

                            $data = array();
                            $data['idpengajuan'] = $_POST['id'];
                            $data['deskripsi'] = 'Cicilan ' . $i;
                            $data['nominal'] = $this->nominalcicilan;
                            $data['tanggal'] = $tanggalTempo;

                            $this->db->insert('tpengajuan_termin_fix', $data);
                        }
                    }
                }
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('tpengajuan', $this, array('id' => $id))) {
            if ($this->db->update('tpengajuan_persetujuan', $this, array('idpengajuan' => $id))) {
                if ($this->db->update('tpengajuan_persetujuan_detail', $this, array('idpengajuan' => $id))) {
                    return true;
                }
            }
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function checkIdPermission($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('musers');
        return $query->row();
    }

    /* it's right function &start */
    public function savePersetujuan()
    {
        /* buat status dimana jika user tidak ada yg mempunyai idpermission 5 maka proses akan berulang menjadi ke awal */

        date_default_timezone_set("Asia/Jakarta");
        $kategori = $this->input->post('kategori');
        $assigment_list = json_decode($this->input->post('assignmentValue'));

        foreach ($assigment_list as $row) {
            $permission = $this->model->checkIdPermission($row[0]);
            if ($permission->idpermission == 5) {
                $result = $permission->idpermission;
                break;
            }
        }

        if($result <> 5 && $kategori == 2) {
            $kategori = 1;

            $this->db->set('stproses', 1);
            $this->db->where('id', $this->input->post('idpengajuan'));
            $this->db->update('tpengajuan');

            $this->db->set('status', 0);
            $this->db->where('idpengajuan', $this->input->post('idpengajuan'));
            $this->db->update('tpengajuan_persetujuan');

            $this->db->set('status', 0);
            $this->db->where('idpengajuan', $this->input->post('idpengajuan'));
            $this->db->update('tpengajuan_persetujuan_detail');
        }

        if ($kategori == 2) {
            $tahapan = 2;
            $kategori = 2;
        } else {
            $tahapan = 1;
            $kategori = 1;
        }

        $data = array(
            'tanggal' => date("Y-m-d H:i:s"),
            'idpengajuan' => $this->input->post('idpengajuan'),
            'memo' => $this->input->post('memo'),
            'tahapan' => $tahapan,
            'kategori' => $kategori,
            'status' => 1
        );

        if ($this->db->insert('tpengajuan_persetujuan', $data)) {
            $idpersetujuan = $this->db->insert_id();

            foreach ($assigment_list as $row) {
                $data = array();
                $data['tanggal'] = date('Y-m-d');
                $data['idpengajuan'] = $this->input->post('idpengajuan');
                $data['idpersetujuan'] = $idpersetujuan;
                $data['idpenyetuju'] = $row[0];
                $data['kategori'] = $kategori;
                $data['status_persetujuan'] = 0;
                $data['status_persetujuan_'] = 0;
                $data['tanggal_update'] = date('Y-m-d H:i:s');
                $data['tanggal_update_'] = date('Y-m-d H:i:s');
                $data['status'] = 1;

                $this->db->insert('tpengajuan_persetujuan_detail', $data);
            }

            # @function update &start
            $this->db->set('stproses', 1);
            $this->db->where('id', $this->input->post('idpengajuan'));
            $this->db->update('tpengajuan');
            # @function update &end

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    /* it's right function &end */

    public function saveUlasan()
    {
        date_default_timezone_set("Asia/Jakarta");

        $id = $this->session->userdata('user_id');
        $idpermission = $this->session->userdata('user_idpermission');
        $idpengajuan = $this->input->post('idpengajuan');
        $idpersetujuan = $this->input->post('idpersetujuan');
        $status = $this->input->post('status');

        $stproses_before = $this->getPengajuanDetail($idpengajuan);

        if ($stproses_before->stproses == 6) {
            $this->db->set('status_persetujuan_', $status);
            $this->db->set('tanggal_update_', date('Y-m-d H:i:s'));
        } else {
            $this->db->set('status_persetujuan', $status);
            $this->db->set('tanggal_update', date('Y-m-d H:i:s'));
        }

        $this->db->where(array('idpengajuan' => $idpengajuan, 'idpersetujuan' => $idpersetujuan, 'idpenyetuju' => $id));
        $status_update = $this->db->update('tpengajuan_persetujuan_detail');

        if ($status_update) {

            /* -------------------------------------------------------------------------------------------------------------------- */
            # ------- tambahkan status permission wadir disini untuk membedakan ketika memproses mana wadir & mana karyawan ------- #
            /* -------------------------------------------------------------------------------------------------------------------- */

            if ($idpermission == 5) {
                /* jika disetujui */
                if ($status == 1) {
                    /* @update (pengajuan) &start */
                    $this->updateStatusPengajuan(2, $idpengajuan);
                    /* @update (pengajuan) &end */

                    /* ----------------------------------------------------------------------- */
                    /* @update (persetujuan) &start */
                    $this->updateStatusPersetujuan(2, 2, $idpengajuan);
                    /* @update (persetujuan) &end */

                    /* ----------------------------------------------------------------------- */
                }

                if ($status == 2) {
                    $this->updateStatusPengajuan(3, $idpengajuan);

                    /* ----------------------------------------------------------------------- */
                    /* @update (persetujuan) &start */
                    $this->updateStatusPersetujuan(2, 2, $idpengajuan);
                    /* @update (persetujuan) &end */
                    /* ----------------------------------------------------------------------- */
                }
            } else {
                $total = $this->getTotalPersetujuan($idpengajuan);
                $disetujui = $this->getTotalPersetujuan($idpengajuan, 1);
                $ditolak = $this->getTotalPersetujuan($idpengajuan, 2);

                /* jika disetujui */
                if ($total == ($disetujui + $ditolak) && $status == 1) {
                    /* @update (pengajuan) &start */
                    if ($stproses_before->stproses == 6) {
                        $this->updateStatusPengajuan(2, $idpengajuan);
                    } else {
                        $this->updateStatusPengajuan(2, $idpengajuan);
                    }
                    /* @update (pengajuan) &end */

                    /* ----------------------------------------------------------------------- */
                    /* @update (persetujuan) &start */
                    $this->updateStatusPersetujuan(2, 1, $idpengajuan);
                    /* @update (persetujuan) &end */

                    /* ----------------------------------------------------------------------- */
                }

                if ($total == ($disetujui + $ditolak) && $status == 2) {
                    $this->updateStatusPengajuan(6, $idpengajuan);

                    /* ----------------------------------------------------------------------- */
                    /* @update (persetujuan) &start */
                    $this->updateStatusPersetujuan(1, 1, $idpengajuan);
                    /* @update (persetujuan) &end */
                    /* ----------------------------------------------------------------------- */
                }
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    /* newFunction (update) &start ---------------------------------------------------------- */
    public function updateStatusPengajuan($stproses, $idpengajuan)
    {
        $this->db->set('stproses', $stproses);
        $this->db->where('id', $idpengajuan);
        $this->db->update('tpengajuan');
    }

    public function updateStatusPersetujuan($tahapan, $kategori, $idpengajuan)
    {
        $this->db->set(array('tahapan' => $tahapan, 'kategori' => $kategori));
        $this->db->where('idpengajuan', $idpengajuan);
        $this->db->update('tpengajuan_persetujuan');
    }

    public function checkStatusPersetujuan($id, $idpengajuan)
    {
        $this->db->where(array('idpenyetuju' => $id, 'idpengajuan' => $idpengajuan));
        $this->db->where('status', 1);

        $query = $this->db->get('tpengajuan_persetujuan_detail');
        return $query->row();
    }

    /* newFunction (update) &end ---------------------------------------------------------- */

    public function saveAktivasiBendahara()
    {
        $sessionUserId = $this->session->userdata('user_id');
        $id = $this->input->post('id');
        $statusdeskripsi_termin = "";

        # Update Status (Aktivasi Bendahara)
        $this->db->set('stproses', 4);
        $this->db->where('id', $id);
        if ($this->db->update('tpengajuan')) {

            # Update Data Barang
            $detail_list = json_decode($this->input->post('detailValue'));
            foreach ($detail_list as $row) {
                $data = array();
                $data['id'] = $row[0];
                $data['idpengajuan'] = $id;
                $data['namabarang'] = $row[1];
                $data['merk'] = $row[2];
                $data['kuantitas'] = $row[3];
                $data['idsatuan'] = $row[4];
                $data['harga'] = RemoveComma($row[6]);
                $data['jumlah'] = RemoveComma($row[7]);
                $data['keterangan'] = $row[8];

                $this->db->where('idpengajuan', $id);
                $this->db->update('tpengajuan_barang', $data);
            }

            // Update Data Termin Progress & jenispembayaran = 4
            if ($this->input->post('jenispembayaran') == 4) {
                $termin_list = json_decode($_POST['terminValue']);
                foreach ($termin_list as $row) {
                    $result = $this->getStatusTerminProgress($row[0]);

                    $data = array();
                    $data['id'] = $row[0];
                    $data['idpengajuan'] = $id;
                    $data['tanggal'] = YMDFormat($row[1]);
                    $data['subjek'] = $row[2];
                    $data['deskripsi'] = $row[3];
                    $data['nominal'] = RemoveComma($row[4]);
                    $data['statusaktivasi1'] = $row[4];

                    if ($result->statusaktivasi2 == 2) {
                        $data['statusaktivasi2'] = 2;
                    } else {
                        $data['statusaktivasi2'] = 1;
                    }

                    $data['statusaktivasi3'] = 1;

                    $this->db->replace('tpengajuan_termin_progress', $data);

                    if ($row[4] == 2 && $result->statusaktivasi2 == 1) {
                        $statusdeskripsi_termin = $row[3].", ";
                    }
                }
            }

            // Update Data Termin Fix & jenispembayaran = 5
            if ($this->input->post('jenispembayaran') == 5) {
                $terminfix_list = json_decode($this->input->post('terminFixValue'));
                foreach ($terminfix_list as $row) {
                    $result = $this->getStatusTerminFix($row[0]);

                    $data = array();
                    $data['id'] = $row[0];
                    $data['idpengajuan'] = $id;
                    $data['deskripsi'] = $row[1];
                    $data['nominal'] = RemoveComma($row[2]);
                    $data['tanggal'] = YMDFormat($row[3]);
                    $data['statusaktivasi1'] = $row[4];

                    if ($result->statusaktivasi2 == 2) {
                        $data['statusaktivasi2'] = 2;
                    } else {
                        $data['statusaktivasi2'] = 1;
                    }

                    $data['statusaktivasi3'] = 1;

                    $this->db->replace('tpengajuan_termin_fix', $data);

                    if ($row[4] == 2 && $result->statusaktivasi2 == 1) {
                        $statusdeskripsi_termin .= $result->deskripsi.", ";
                    }
                }

            }

            # Insert Data Aktivasi
            $data = array();
            $data['idpengajuan'] = $id;
            $data['tanggal'] = date("Y-m-d");
            $data['iduser'] = $this->input->post('idbendahara');
            $data['memo'] = $this->input->post('memobendahara');
            $this->db->insert('tpengajuan_aktivasi', $data);
            # ------------------------------------------------------------

            # Insert Data Unit Pemroses
            $data = array();
            $data['tanggal'] = date("Y-m-d");
            $data['idpengajuan'] = $id;
            $data['memo'] = $this->input->post('memo');

            if ($statusdeskripsi_termin !== "") {
                $statusdeskripsi_termin = substr($statusdeskripsi_termin, 0, -2);
                $data['statusdeskripsi'] = $statusdeskripsi_termin;
            } else {
                $data['statusdeskripsi'] = "Tunai/Kontrabon/Transfer";
            }

            // iduser wakil direktur &start
            $data['iduser'] = $sessionUserId;
            // iduser wakil direktur &end

            if ($this->db->insert('tpengajuan_unitpemroses', $data)) {
                $idunitpemroses = $this->db->insert_id();

                // User Assigment
                $assigment_list = json_decode($_POST['assignmentValue']);
                foreach ($assigment_list as $row) {
                    $data = array();
                    $data['idunitpemroses'] = $idunitpemroses;

                    // iduser pemrosesan &start
                    $data['iduser'] = $row[0];
                    // iduser pemrosesan &end

                    $this->db->insert('tpengajuan_unitpemroses_detail', $data);
                }
            }
            # ------------------------------------------------------------

            return true;
        }
    }

    public function getStatusTerminFix($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('tpengajuan_termin_fix');
        return $query->row();
    }

    public function getStatusTerminProgress($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('tpengajuan_termin_progress');
        return $query->row();
    }

    /* -------------------------------------------------------------------------- */
    public function savePenolakan()
    {
        date_default_timezone_set("Asia/Jakarta");
        $id = $this->session->userdata('user_id');

        $idpengajuan = $this->input->post('id');
        $memo = $this->input->post('memo');

        $check = $this->model->checkStatusPersetujuan($id, $idpengajuan);

        $total = $this->getTotalPersetujuan($idpengajuan);
        $disetujui = $this->getTotalPersetujuan($idpengajuan, 1);
        $ditolak = $this->getTotalPersetujuan($idpengajuan, 2);

        $stproses_before = $this->getPengajuanDetail($idpengajuan);

        if ($check->status_persetujuan_ == 0) {
            $this->db->set('alasan', $memo);
        } else {
            if ($check->alasan == "") {
                $this->db->set('alasan', "-");
            }
            $this->db->set('alasan_', $memo);
        }

        $this->db->where(array('idpengajuan' => $idpengajuan, 'idpenyetuju' => $id));
        if ($this->db->update('tpengajuan_persetujuan_detail')) {
            /* jika ditolak */
            if ($total == ($disetujui + $ditolak)) {
                /* @update (pengajuan) &start */
                if ($stproses_before->stproses == 6) {
                    $disetujui2 = $this->getTotalPersetujuan2($idpengajuan, 1);
                    $ditolak2 = $this->getTotalPersetujuan2($idpengajuan, 2);

                    if ($total == ($disetujui2 + $ditolak2)) {
                        $this->updateStatusPengajuan(3, $idpengajuan);
                    }
                }
                /* @update (pengajuan) &end */
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    /* -------------------------------------------------------------------------- */

    /* @function (new) &start */
    public function getVendorRekening($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mvendor');
        return $query->row();
    }
    /* @function (new) &end */

    /* @function (new getindex) &start */
    public function getIndexPengajuan($id)
    {
        $this->db->where(array(
            'status' => 1,
            'idpemohon' => $id
        ));


        $this->db->or_where('idpenyetuju', $id);

        $this->db->where('sttampil1', 1);
        $this->db->where('sttampil2', 1);
        $this->db->group_by('id');

        // don't forget change name of view
        $query = $this->db->get('zzpercobaan');
        return $query->result();
    }
    /* @function (new getindex) &end */

    /* @function (getpengajuandetail, getpegawai) &start */
    public function getPengajuanDetail($id)
    {

        $this->db->select('tpengajuan.*, mvendor.nama as namavendor');
        $this->db->join('mvendor', 'tpengajuan.idvendor = mvendor.id', 'left');

        $this->db->where('tpengajuan.id', $id);
        $query = $this->db->get('tpengajuan');
        return $query->row();
    }

    /* @function new (getpengajuandetail, getpegawai) &start */
    public function getPegawai($idpemohon = null)
    {
        $iduser = $this->session->userdata('user_id');
//        $idpermission = $this->session->userdata('user_idpermission');

        # 4 : bendahara & 5 : wadir
        $this->db->where(array(
//            'idpermission !=' => 5,
            'id !=' => $iduser,
            'status' => 1
        ));

        $this->db->where('id !=', $idpemohon);

        $query = $this->db->get('musers');
        return $query->result();
    }

    /* @function new (getpengajuandetail, getpegawai) &end */
    public function getTotalPersetujuan($id, $status = null)
    {
        $this->db->where('idpengajuan', $id);
        if ($status !== null) {
            $this->db->where('status_persetujuan', $status);
        }

        $this->db->where('status', 1);
        $query = $this->db->get('tpengajuan_persetujuan_detail');
        return count($query->result());
    }

    public function getTotalPersetujuan2($id, $status = null)
    {
        $this->db->where('idpengajuan', $id);
        if ($status !== null) {
            $this->db->where('status_persetujuan_', $status);
        }

        $this->db->where('status', 1);
        $query = $this->db->get('tpengajuan_persetujuan_detail');
        return count($query->result());
    }

    /* @function new (getpersetujuandetail) &start */
    public function getPersetujuanDetail($id, $idpersetujuan_detail = null)
    {
        $this->db->select('tpengajuan_persetujuan_detail.id, tpengajuan_persetujuan_detail.idpengajuan, musers.name as nama, tpengajuan_persetujuan_detail.status_persetujuan, tpengajuan_persetujuan_detail.status_persetujuan_, tpengajuan_persetujuan_detail.alasan, tpengajuan_persetujuan_detail.alasan_');
        $this->db->join('musers', 'tpengajuan_persetujuan_detail.idpenyetuju = musers.id', 'LEFT');
        if ($idpersetujuan_detail !== null) {
            $this->db->where('tpengajuan_persetujuan_detail.id', $idpersetujuan_detail);
        }

        $this->db->where('tpengajuan_persetujuan_detail.idpengajuan', $id);
        $this->db->where(array('tpengajuan_persetujuan_detail.status' => 1));

        $query = $this->db->get('tpengajuan_persetujuan_detail');

        return $query->result();
    }
    /* @function new (getpersetujuandetail) &end */

    public function checkTotalCicilan($id, $status = null)
    {
        # empty: , 1: , 2:
        if ($status !== null) {
            $this->db->where('statusaktivasi1', $status);
        }

        $this->db->where('idpengajuan', $id);
        $query = $this->db->get('tpengajuan_termin_fix');
        return $query->result();
    }
}
