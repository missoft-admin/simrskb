<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Muji_otot_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // public function getAll()
    // {
    //     $this->db->where('staktif', '1');
    //     $this->db->order_by('nama', 'ASC');
    //     $this->db->get('muji_otot');
    //     return $this->db->last_query();
    // }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('muji_otot');
        return $query->row();
    }

    public function saveData()
    {
        $this->nourut 					= $_POST['nourut'];
        $this->nama 					= $_POST['nama'];
        $this->isi_header 			= $_POST['isi_header'];
        $this->isi_footer 			= $_POST['isi_footer'];
        $this->staktif 			  = 1;
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('muji_otot', $this)) {
            return $this->db->insert_id();
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
		$this->nourut 					= $_POST['nourut'];
        $this->nama 					= $_POST['nama'];
        $this->isi_header 			= $_POST['isi_header'];
        $this->isi_footer 			= $_POST['isi_footer'];
        $this->staktif 			  = 1;
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('muji_otot', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->staktif = 0;
		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('muji_otot', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
