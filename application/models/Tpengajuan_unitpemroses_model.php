<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpengajuan_unitpemroses_model extends CI_Model
{
    /*
     * Pengajuan Unit Proses
     * Developer muhamadali1504@gmail.com
     * */

    public function __construct()
    {
        parent::__construct();
    }

    public function getIndex($id)
    {
        $this->db->where('unitaccess', $id);
        $this->db->where('status', 1);
        // don't forget change the name of view in database
        $query = $this->db->get('zzpercobaan_unitpemroses');
        return $query->result();
    }

    public function getDetailUnitPengajuan($id)
    {
        $this->db->where('idunit', $id);
        $this->db->where('status', 1);
        // don't forget change the name of view in database
        $query = $this->db->get('zzpercobaan_unitpemroses');
        return $query->result();
    }


}

?>