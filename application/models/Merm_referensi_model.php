<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Merm_referensi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function list_head()
    {
        $q="SELECT 
			*
			from merm_referensi_head M
					
			WHERE M.status='1'";
        $query = $this->db->query($q);
        return $query->result();
    }
	public function getSpecified($id)
    {
        $q="SELECT 
			*
			from merm_referensi M
					
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	public function list_akun(){
	   $q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		return $list;
   }
    public function saveData()
    {
        $this->ref 	= $_POST['ref'];		
        $this->ref_head_id 	= $_POST['ref_head_id'];		
        $this->nilai 			= ($_POST['nilai']);
        $this->st_default 			= ($_POST['st_default']);
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('merm_referensi', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->ref 	= $_POST['ref'];
		$this->ref_head_id 	= $_POST['ref_head_id'];	
        $this->nilai 			= ($_POST['nilai']);
        $this->st_default 			= ($_POST['st_default']);
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('merm_referensi', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('merm_referensi', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('merm_referensi', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
