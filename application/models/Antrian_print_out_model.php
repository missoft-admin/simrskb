<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Antrian_print_out_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function get_index_setting(){
		$q="SELECT *
			 FROM antrian_print_out
			WHERE id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function save_general(){
		$id = $this->input->post('id');
		$this->judul_header = $this->input->post('judul_header');
		$this->judul_sub_header = $this->input->post('judul_sub_header');
		$this->footer = $this->input->post('footer');
		$this->footer_sub = $this->input->post('footer_sub');
		$this->st_tampil_tanggal = $this->input->post('st_tampil_tanggal');
		$this->db->where('id', $id);
		// print_r($this);exit;	
		if ($this->db->update('antrian_print_out', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
}
