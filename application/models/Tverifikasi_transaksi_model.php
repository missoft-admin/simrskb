<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tverifikasi_transaksi_model extends CI_Model
{
	public function getDataKasirRawatJalan()
	{
		$query = $this->db->query("
      SELECT
      	tkasir.id,
      	tkasir.tanggal,
      	COALESCE(tpoliklinik_pendaftaran.nopendaftaran, '-') AS nopendaftaran,
      	tkasir.idtipe,
      	(CASE
      		WHEN tkasir.idtipe = 1 THEN 'Poliklinik'
      		WHEN tkasir.idtipe = 2 THEN 'IGD'
      		WHEN tkasir.idtipe = 3 THEN 'Obat Bebas'
      	END) AS tipe,
      	COALESCE((CASE WHEN tkasir.idtipe = 3 THEN tpasien_penjualan.nomedrec ELSE tpoliklinik_pendaftaran.no_medrec END), '-') AS nomedrec,
      	COALESCE((CASE WHEN tkasir.idtipe = 3 THEN tpasien_penjualan.nama ELSE tpoliklinik_pendaftaran.namapasien END), '-') AS namapasien,
      	tkasir.bayar AS jumlah,
      	tkasir.status_edit,
      	tkasir.status_verifikasi
      FROM
      	tkasir
      LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tkasir.idtindakan AND tkasir.idtipe IN (1,2)
      LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
      LEFT JOIN tpasien_penjualan ON tpasien_penjualan.id = tkasir.idtindakan AND tkasir.idtipe = 3
      WHERE DATE(tkasir.tanggal) >= '2021-02-28' AND DATE(tkasir.tanggal) <= '2021-03-02'
      ORDER BY tkasir.tanggal
    	");
		return $query->result();
	}

	public function getDataKasirRawatInap()
	{
		$query = $this->db->query("
      SELECT
      	trawatinap_tindakan_pembayaran.id,
      	trawatinap_tindakan_pembayaran.tanggal,
        trawatinap_pendaftaran.id AS idrawatinap,
      	COALESCE(trawatinap_pendaftaran.nopendaftaran, '-') AS nopendaftaran,
      	trawatinap_pendaftaran.idtipe,
      	(CASE
      		WHEN trawatinap_pendaftaran.idtipe = 1 THEN 'Rawat Inap'
      		WHEN trawatinap_pendaftaran.idtipe = 2 THEN 'ODS'
      	END) AS tipe,
      	(CASE
      		WHEN trawatinap_pendaftaran.idtipe = 1 THEN 'rawatinap'
      		WHEN trawatinap_pendaftaran.idtipe = 2 THEN 'ods'
      	END) AS page,
      	trawatinap_pendaftaran.no_medrec AS nomedrec,
      	trawatinap_pendaftaran.namapasien AS namapasien,
      	trawatinap_tindakan_pembayaran.pembayaran AS jumlah,
      	trawatinap_tindakan_pembayaran.status_edit,
      	trawatinap_tindakan_pembayaran.status_verifikasi
      FROM
      	trawatinap_tindakan_pembayaran
      LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_tindakan_pembayaran.idtindakan
      WHERE trawatinap_tindakan_pembayaran.statusbatal = 0 AND DATE(trawatinap_tindakan_pembayaran.tanggal) >= '2021-02-20' AND DATE(trawatinap_tindakan_pembayaran.tanggal) <= '2021-03-02'
      ORDER BY trawatinap_tindakan_pembayaran.tanggal
    	");
		return $query->result();
	}

	public function getHeadDokumenRawatJalan($idkasir)
	{
		$this->db->select("tkasir.id AS idkasir,
        (CASE
          WHEN tkasir.idtipe = 3 THEN tpasien_penjualan.nopenjualan
          ELSE tpoliklinik_pendaftaran.nopendaftaran
        END) AS nopendaftaran,
        (CASE
          WHEN tkasir.idtipe = 1 THEN 'Poliklinik'
          WHEN tkasir.idtipe = 2 THEN 'IGD'
          WHEN tkasir.idtipe = 3 THEN 'Obat Bebas'
        END) AS tipe,
        COALESCE((CASE WHEN tkasir.idtipe = 3 THEN COALESCE(tpasien_penjualan.nomedrec, '-') ELSE tpoliklinik_pendaftaran.no_medrec END), '-') AS nomedrec,
      	COALESCE((CASE WHEN tkasir.idtipe = 3 THEN tpasien_penjualan.nama ELSE tpoliklinik_pendaftaran.namapasien END), '-') AS namapasien");
		$this->db->join('tpoliklinik_tindakan', 'tpoliklinik_tindakan.id = tkasir.idtindakan AND tkasir.idtipe IN (1,2)', 'LEFT');
		$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran', 'LEFT');
		$this->db->join('tpasien_penjualan', 'tpasien_penjualan.id = tkasir.idtindakan AND tkasir.idtipe = 3', 'LEFT');
		$this->db->where('tkasir.id', $idkasir);
		$query = $this->db->get('tkasir');
		return $query->row();
	}

	public function getHeadDokumenRawatInap($idkasir)
	{
		$this->db->select("trawatinap_tindakan_pembayaran.id AS idkasir,
        COALESCE(trawatinap_pendaftaran.nopendaftaran, '-') AS nopendaftaran,
        (CASE
      		WHEN trawatinap_pendaftaran.idtipe = 1 THEN 'Rawat Inap'
      		WHEN trawatinap_pendaftaran.idtipe = 2 THEN 'ODS'
      	END) AS tipe,
        trawatinap_pendaftaran.no_medrec AS nomedrec,
      	trawatinap_pendaftaran.namapasien AS namapasien");
		$this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = trawatinap_tindakan_pembayaran.idtindakan');
		$this->db->where('trawatinap_tindakan_pembayaran.id', $idkasir);
		$query = $this->db->get('trawatinap_tindakan_pembayaran');
		return $query->row();
	}

	public function updateTipePasien()
	{
		$this->idtipepasien = $_POST['idtipepasien'];
		if ($this->db->update($_POST['table'], $this, ['id' => $_POST['idpendaftaran']])) {
			return true;
		} else {
			return false;
		}
	}

	public function updateStatus($id, $tipe, $status)
	{
		// print_r($id);exit();
		if ($tipe == 'poliklinik' || $tipe == 'obatbebas') {
			$table = 'tkasir';
			$this->status_verifikasi = $status;
		} elseif ($tipe == 'rawatinap') {
			$table = 'trawatinap_tindakan_pembayaran';
			// $status=0;
			$this->status_verifikasi = $status;
		} elseif ($tipe == 'deposit') {
			$table = 'trawatinap_deposit';
			$this->statusverifikasi = $status;
		}

		if ($this->db->update($table, $this, ['id' => $id])) {
			if ($tipe == 'poliklinik') {
				if ($status == '0') {//JURNAL DEPOSIT
					$this->remove_jurnal_pendapatan($id, 1);
				}
			}
			if ($tipe == 'deposit') {
				if ($status == '1') {//JURNAL DEPOSIT
					$this->insert_jurnal_deposit($id);
				} else {//BATALKAN STATUS
					$this->remove_jurnal_deposit($id);
				}
			}
			if ($tipe == 'obatbebas') {//INSERT PENDAPTAN OBAT BEBAS
			  if ($status == '1') {//JURNAL DEPOSIT
				 $this->Tvalidasi_model->insert_tarif_obat_bebas($id);
			  } else {
			  	$this->remove_jurnal_pendapatan($id, 1);
			  }
			}
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function remove_jurnal_pendapatan($id, $tipe)
	{
		if ($tipe = '1') {
			$q = "SELECT D.kasir_id,H.tipe_trx,H.st_posting,D.id as iddetail FROM `tvalidasi_pendapatan_rajal_detail` D
			LEFT JOIN tvalidasi_pendapatan_rajal H ON H.id=D.idvalidasi
			WHERE D.kasir_id='$id' AND H.tipe_trx='$tipe'";
			$hasil = $this->db->query($q)->row();
			if ($hasil) {
				if ($hasil->st_posting == '0') {//Jika Belum di posting
					$this->db->where('id', $hasil->iddetail);
					$this->db->delete('tvalidasi_pendapatan_rajal_detail');
				}
			}
		}
		if ($tipe = '4') {
			$q = "SELECT D.pendaftaran_id,H.tipe_trx,H.st_posting,D.id as iddetail FROM `tvalidasi_pendapatan_rajal_detail` D
			LEFT JOIN tvalidasi_pendapatan_rajal H ON H.id=D.idvalidasi
			WHERE D.pendaftaran_id='$id' AND H.tipe_trx='1' AND D.idtipe='$tipe'";
			$hasil = $this->db->query($q)->row();
			if ($hasil) {
				if ($hasil->st_posting == '0') {//Jika Belum di posting
					$this->db->where('id', $hasil->iddetail);
					$this->db->delete('tvalidasi_pendapatan_rajal_detail');
				}
			}
		}
	}

	public function get_status_posting_rajal($id, $tipe)
	{
		$posting = 0;
		if ($tipe == '1' || $tipe == '2' || $tipe == '3') {
			$q = "SELECT D.kasir_id,H.tipe_trx,H.st_posting,D.id as iddetail FROM `tvalidasi_pendapatan_rajal_detail` D
			LEFT JOIN tvalidasi_pendapatan_rajal H ON H.id=D.idvalidasi
			WHERE D.kasir_id='$id' AND H.tipe_trx='$tipe'";
			$hasil = $this->db->query($q)->row('st_posting');
			if ($hasil) {
				$posting = $hasil;
			}
		}
		if ($tipe == '4') {
			$q = "SELECT D.pendaftaran_id,H.tipe_trx,H.st_posting,D.id as iddetail FROM `tvalidasi_pendapatan_rajal_detail` D
			LEFT JOIN tvalidasi_pendapatan_rajal H ON H.id=D.idvalidasi
			WHERE D.pendaftaran_id='$id' AND H.tipe_trx='1' AND D.idtipe='$tipe'";
			// print_r($q);exit();
			$hasil = $this->db->query($q)->row('st_posting');
			if ($hasil) {
				$posting = $hasil;
			}
		}
		return $posting;
	}

	public function get_status_posting_ranap($id, $tipe = '')
	{
		$posting = 0;
		if ($tipe == '') {
			$q = "SELECT H.st_posting FROM `tvalidasi_pendapatan_ranap` H WHERE H.kasir_id='$id' AND H.tipe_trx IN (1,2,3,4) LIMIT 1";
			$hasil = $this->db->query($q)->row('st_posting');
			if ($hasil) {
				$posting = $hasil;
			}
		} else {
			$q = "SELECT H.st_posting FROM `tvalidasi_pendapatan_ranap` H WHERE H.pendaftaran_id='$id' LIMIT 1";
			$hasil = $this->db->query($q)->row('st_posting');
			if ($hasil) {
				$posting = $hasil;
			}
		}

		return $posting;
	}

	public function verif_refund($id, $tipe, $status)
	{
		$table = 'trefund';
		$this->st_verifikasi = $status;

		if ($this->db->update($table, $this, ['id' => $id])) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function insert_jurnal_deposit($id)
	{
		$q = "SELECT T.*,A.noakun as noakun_bayar,A.namaakun as namaakun_bayar,S.st_auto_posting FROM (
			SELECT H.id as idtransaksi,H.tanggal as tanggal_transaksi,H.nodeposit as notransaksi
			,P.idtipe,CASE WHEN P.idtipe='1' THEN 'RAWAT INAP' ELSE 'ODS' END as nama_tipe,H.idmetodepembayaran as st_cara_bayar,P.idpasien,P.no_medrec,P.namapasien,P.nopendaftaran
			,H.nominal as nominal_pendapatan,H.nominal as nominal_bayar
			,SP.idakun as idakun_pendapatan,AP.noakun as noakun_pendapatan,AP.namaakun as namaakun_pendapatan
			,CASE WHEN H.idmetodepembayaran='1' THEN SB.idakun_tunai ELSE SK.idakun END as idakun_bayar,H.idbank,H.idrawatinap,P.idkelompokpasien
			FROM trawatinap_deposit H
			LEFT JOIN trawatinap_pendaftaran P ON P.id=H.idrawatinap
			LEFT JOIN msetting_jurnal_deposit_pendapatan SP ON SP.idtipe=H.idmetodepembayaran 
			LEFT JOIN makun_nomor AP ON AP.id=SP.idakun
			LEFT JOIN msetting_jurnal_deposit SB ON H.idmetodepembayaran='1'
			LEFT JOIN msumber_kas SK ON SK.bank_id=H.idbank
			WHERE H.id='$id') T
			LEFT JOIN makun_nomor A ON A.id=T.idakun_bayar
			LEFt JOIN msetting_jurnal_deposit S ON S.id='1'

		";
		$row = $this->db->query($q)->row();
		$st_auto_posting = $row->st_auto_posting;
		// $tipe_bayar=$row->tipe_bayar;
		$data_header = [
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'idtipe' => $row->idtipe,
			'st_cara_bayar' => $row->st_cara_bayar,
			'idbank' => $row->idbank,
			'idpasien' => $row->idpasien,
			'idkelompokpasien' => $row->idkelompokpasien,
			'idrawatinap' => $row->idrawatinap,
			'no_medrec' => $row->no_medrec,
			'namapasien' => $row->namapasien,
			'nopendaftaran' => $row->nopendaftaran,
			'nominal_pendapatan' => $row->nominal_pendapatan,
			'nominal_bayar' => $row->nominal_bayar,
			'idakun_pendapatan' => $row->idakun_pendapatan,
			'idakun_bayar' => $row->idakun_bayar,
			'noakun_pendapatan' => $row->noakun_pendapatan,
			'noakun_bayar' => $row->noakun_bayar,
			'namaakun_pendapatan' => $row->namaakun_pendapatan,
			'namaakun_bayar' => $row->namaakun_bayar,
			'posisi_pendapatan' => 'K',
			'posisi_bayar' => 'D',
			'status' => 1,
			'st_posting' => 0,
			'st_auto_posting' => $row->st_auto_posting,
			'created_by' => $this->session->userdata('user_id'),
			'created_nama' => $this->session->userdata('user_name'),
			'created_date' => date('Y-m-d H:i:s')

		];
		$st_auto_posting = $row->st_auto_posting;
		$this->db->insert('tvalidasi_deposit', $data_header);
		$idvalidasi = $this->db->insert_id();

		if ($row->idakun_bayar && $row->idakun_pendapatan) {
			if ($st_auto_posting == '1') {
				$data_header = [

					'st_posting' => 1,
					'posting_by' => $this->session->userdata('user_id'),
					'posting_nama' => $this->session->userdata('user_name'),
					'posting_date' => date('Y-m-d H:i:s')

				];
				$this->db->where('id', $idvalidasi);
				$this->db->update('tvalidasi_deposit', $data_header);
			}
		}
		return true;
	}

	public function remove_jurnal_deposit($id)
	{
		$data_header = [

			'status' => 0,
			'deleted_by' => $this->session->userdata('user_id'),
			'deleted_nama' => $this->session->userdata('user_name'),
			'deleted_date' => date('Y-m-d H:i:s')

		];
		$this->db->where('idtransaksi', $id);
		$this->db->where('status', 1);
		$this->db->update('tvalidasi_deposit', $data_header);

		return true;
	}

	public function updateStatusAll()
	{
		if ($_POST['tipetransaksi'] == 'poliklinik') {
			$table = 'tkasir';
		} elseif ($_POST['tipetransaksi'] == 'rawatinap') {
			$table = 'trawatinap_tindakan_pembayaran';
		}

		foreach ($_POST['check_status'] as $id) {
			$this->status_verifikasi = 1;
			$this->db->update($table, $this, ['id' => $id]);
		}
	}

	public function getStatusVerifikasiKasir($idtipe, $idkasir)
	{
		if ($idtipe == 'poliklinik') {
			$this->db->where('id', $idkasir);
			$query = $this->db->get('tkasir');
			return $query->row()->status_verifikasi;
		} elseif ($idtipe == 'rawatinap') {
			$this->db->where('idtindakan', $idkasir);
			$this->db->where('statusbatal', 0);
			$query = $this->db->get('trawatinap_tindakan_pembayaran');
			return $query->row()->status_verifikasi;
		}
	}

	public function getStatusEditRanapRuangan($idtarif, $kelas)
	{
		$this->db->where('idtarif', $idtarif);
		$this->db->where('kelas', $kelas);
		$query = $this->db->get('mtarif_ruangperawatan_detail');
		return $query->row();
	}

	public function getStatusEditRanapTindakan($idtarif, $kelas)
	{
		$this->db->where('idtarif', $idtarif);
		$this->db->where('kelas', $kelas);
		$query = $this->db->get('mtarif_rawatinap_detail');
		return $query->row();
	}

	public function getStatusEditRanapVisite($idtarif, $kelas)
	{
		$this->db->where('idtarif', $idtarif);
		$this->db->where('kelas', $kelas);
		$query = $this->db->get('mtarif_visitedokter_detail');
		return $query->row();
	}

	public function getStatusEditRanapObat($idtarif)
	{
		$this->db->where('id', $idtarif);
		$query = $this->db->get('mdata_obat');
		return $query->row();
	}

	public function getStatusEditRanapAlkes($idtarif)
	{
		$this->db->where('id', $idtarif);
		$query = $this->db->get('mdata_alkes');
		return $query->row();
	}

	public function getStatusEditRanaImplan($idtarif)
	{
		$this->db->where('id', $idtarif);
		$query = $this->db->get('mdata_implan');
		return $query->row();
	}

	public function getStatusEditRajalTindakan($idtarif)
	{
		$this->db->where('id', $idtarif);
		$query = $this->db->get('mtarif_rawatjalan');
		return $query->row();
	}

	public function getStatusEditLaboratorium($idtarif, $kelas)
	{
		$this->db->where('idtarif', $idtarif);
		$this->db->where('kelas', $kelas);
		$query = $this->db->get('mtarif_laboratorium_detail');
		return $query->row();
	}

	public function getStatusEditRadiologi($idtarif, $kelas)
	{
		$this->db->where('idtarif', $idtarif);
		$this->db->where('kelas', $kelas);
		$query = $this->db->get('mtarif_radiologi_detail');
		return $query->row();
	}

	public function getStatusEditFisioterapi($idtarif, $kelas)
	{
		$this->db->where('idtarif', $idtarif);
		$this->db->where('kelas', $kelas);
		$query = $this->db->get('mtarif_fisioterapi_detail');
		return $query->row();
	}

	public function getStatusEditSewaAlatOperasi($idtarif, $kelas)
	{
		$this->db->where('idtarif', $idtarif);
		$this->db->where('kelas', $kelas);
		$query = $this->db->get('mtarif_operasi_sewaalat_detail');
		return $query->row();
	}

	public function getStatusEditOperasi($idtarif, $kelas)
	{
		$this->db->where('idtarif', $idtarif);
		$this->db->where('kelas', $kelas);
		$query = $this->db->get('mtarif_operasi_detail');
		return $query->row();
	}

	public function getGroupPembayaran()
	{
		$this->db->where('status', 1);
		$query = $this->db->get('mgroup_pembayaran');
		return $query->result();
	}

	public function getDetailTindakanRanap($table, $iddetail)
	{
		switch ($table) {
		case 'trawatinap_ruangan':
		  $query = $this->db->query("SELECT
          	'mtarif_ruangperawatan' AS table_tarif,
          	trawatinap_ruangan.id,
          	trawatinap_ruangan.idtarif,
          	trawatinap_ruangan.namatarif,
          	0 AS iduserinput,
          	'-' AS namauserinput,
          	0 AS iddokter,
          	'-' AS namadokter,

          	mtarif_ruangperawatan_detail.jasasarana AS jasasarana_master,
          	mtarif_ruangperawatan_detail.jasapelayanan AS jasapelayanan_master,
          	mtarif_ruangperawatan_detail.bhp AS bhp_master,
          	mtarif_ruangperawatan_detail.biayaperawatan AS biayaperawatan_master,

          	IF(trawatinap_ruangan_verifikator.id, trawatinap_ruangan_verifikator.jasasarana, trawatinap_ruangan.jasasarana) AS jasasarana_tarif,
          	IF(trawatinap_ruangan_verifikator.id, trawatinap_ruangan_verifikator.jasapelayanan, trawatinap_ruangan.jasapelayanan) AS jasapelayanan_tarif,
          	IF(trawatinap_ruangan_verifikator.id, trawatinap_ruangan_verifikator.bhp, trawatinap_ruangan.bhp) AS bhp_tarif,
          	IF(trawatinap_ruangan_verifikator.id, trawatinap_ruangan_verifikator.biayaperawatan, trawatinap_ruangan.biayaperawatan) AS biayaperawatan_tarif,

          	IF(trawatinap_ruangan_verifikator.id, trawatinap_ruangan_verifikator.jasasarana_disc, trawatinap_ruangan.jasasarana_disc) AS jasasarana_disc,
          	IF(trawatinap_ruangan_verifikator.id, trawatinap_ruangan_verifikator.jasapelayanan_disc, trawatinap_ruangan.jasapelayanan_disc) AS jasapelayanan_disc,
          	IF(trawatinap_ruangan_verifikator.id, trawatinap_ruangan_verifikator.bhp_disc, trawatinap_ruangan.bhp_disc) AS bhp_disc,
          	IF(trawatinap_ruangan_verifikator.id, trawatinap_ruangan_verifikator.biayaperawatan_disc, trawatinap_ruangan.biayaperawatan_disc) AS biayaperawatan_disc,

          	IF(trawatinap_ruangan_verifikator.id, trawatinap_ruangan_verifikator.jasasarana_group, mtarif_ruangperawatan_detail.group_jasasarana) AS jasasarana_group,
          	IF(trawatinap_ruangan_verifikator.id, trawatinap_ruangan_verifikator.jasapelayanan_group, mtarif_ruangperawatan_detail.group_jasapelayanan) AS jasapelayanan_group,
          	IF(trawatinap_ruangan_verifikator.id, trawatinap_ruangan_verifikator.bhp_group, mtarif_ruangperawatan_detail.group_bhp) AS bhp_group,
          	IF(trawatinap_ruangan_verifikator.id, trawatinap_ruangan_verifikator.biayaperawatan_group, mtarif_ruangperawatan_detail.group_biayaperawatan) AS biayaperawatan_group,

          	trawatinap_ruangan_verifikator.jasasarana_keputusan AS jasasarana_keputusan,
          	trawatinap_ruangan_verifikator.jasapelayanan_keputusan AS jasapelayanan_keputusan,
          	trawatinap_ruangan_verifikator.bhp_keputusan AS bhp_keputusan,
          	trawatinap_ruangan_verifikator.biayaperawatan_keputusan AS biayaperawatan_keputusan,

          	IF(trawatinap_ruangan_verifikator.id,
          		(CASE WHEN trawatinap_ruangan_verifikator.jasasarana != mtarif_ruangperawatan_detail.jasasarana THEN 1 ELSE 0 END),
          		(CASE WHEN trawatinap_ruangan.jasasarana != mtarif_ruangperawatan_detail.jasasarana THEN 1 ELSE 0 END)
          	) AS jasasarana_status,

          	IF(trawatinap_ruangan_verifikator.id,
          		(CASE WHEN trawatinap_ruangan_verifikator.jasapelayanan != mtarif_ruangperawatan_detail.jasapelayanan THEN 1 ELSE 0 END),
          		(CASE WHEN trawatinap_ruangan.jasapelayanan != mtarif_ruangperawatan_detail.jasapelayanan THEN 1 ELSE 0 END)
          	) AS jasapelayanan_status,

          	IF(trawatinap_ruangan_verifikator.id,
          		(CASE WHEN trawatinap_ruangan_verifikator.bhp != mtarif_ruangperawatan_detail.bhp THEN 1 ELSE 0 END),
          		(CASE WHEN trawatinap_ruangan.bhp != mtarif_ruangperawatan_detail.bhp THEN 1 ELSE 0 END)
          	) AS bhp_status,

          	IF(trawatinap_ruangan_verifikator.id,
          		(CASE WHEN trawatinap_ruangan_verifikator.biayaperawatan != mtarif_ruangperawatan_detail.biayaperawatan THEN 1 ELSE 0 END),
          		(CASE WHEN trawatinap_ruangan.biayaperawatan != mtarif_ruangperawatan_detail.biayaperawatan THEN 1 ELSE 0 END)
          	) AS biayaperawatan_status

          FROM
          	trawatinap_ruangan
          LEFT JOIN mtarif_ruangperawatan_detail ON mtarif_ruangperawatan_detail.idtarif = trawatinap_ruangan.idtarif AND (mtarif_ruangperawatan_detail.kelas = trawatinap_ruangan.idkelas OR mtarif_ruangperawatan_detail.kelas = 1)
        LEFT JOIN trawatinap_ruangan_verifikator ON trawatinap_ruangan_verifikator.id = trawatinap_ruangan.id
          WHERE trawatinap_ruangan.id = $iddetail
        	");
		  return $query->row();

		  break;

		case 'trawatinap_tindakan':
		  $query = $this->db->query("SELECT
          	'mtarif_rawatinap' AS table_tarif,
          	trawatinap_tindakan.id,
          	trawatinap_tindakan.idpelayanan AS idtarif,
          	mtarif_rawatinap.nama AS namatarif,
          	musers.id AS iduserinput,
          	musers.name AS namauserinput,
          	mdokter.id AS iddokter,
          	mdokter.nama AS namadokter,

          	mtarif_rawatinap_detail.jasasarana AS jasasarana_master,
          	mtarif_rawatinap_detail.jasapelayanan AS jasapelayanan_master,
          	mtarif_rawatinap_detail.bhp AS bhp_master,
          	mtarif_rawatinap_detail.biayaperawatan AS biayaperawatan_master,

          	IF(trawatinap_tindakan_verifikator.id, trawatinap_tindakan_verifikator.jasasarana, trawatinap_tindakan.jasasarana) AS jasasarana_tarif,
          	IF(trawatinap_tindakan_verifikator.id, trawatinap_tindakan_verifikator.jasapelayanan, trawatinap_tindakan.jasapelayanan) AS jasapelayanan_tarif,
          	IF(trawatinap_tindakan_verifikator.id, trawatinap_tindakan_verifikator.bhp, trawatinap_tindakan.bhp) AS bhp_tarif,
          	IF(trawatinap_tindakan_verifikator.id, trawatinap_tindakan_verifikator.biayaperawatan, trawatinap_tindakan.biayaperawatan) AS biayaperawatan_tarif,

          	IF(trawatinap_tindakan_verifikator.id, trawatinap_tindakan_verifikator.jasasarana_disc, trawatinap_tindakan.jasasarana_disc) AS jasasarana_disc,
          	IF(trawatinap_tindakan_verifikator.id, trawatinap_tindakan_verifikator.jasapelayanan_disc, trawatinap_tindakan.jasapelayanan_disc) AS jasapelayanan_disc,
          	IF(trawatinap_tindakan_verifikator.id, trawatinap_tindakan_verifikator.bhp_disc, trawatinap_tindakan.bhp_disc) AS bhp_disc,
          	IF(trawatinap_tindakan_verifikator.id, trawatinap_tindakan_verifikator.biayaperawatan_disc, trawatinap_tindakan.biayaperawatan_disc) AS biayaperawatan_disc,

          	IF(trawatinap_tindakan_verifikator.id, trawatinap_tindakan_verifikator.jasasarana_group, mtarif_rawatinap_detail.group_jasasarana) AS jasasarana_group,
          	IF(trawatinap_tindakan_verifikator.id, trawatinap_tindakan_verifikator.jasapelayanan_group, mtarif_rawatinap_detail.group_jasapelayanan) AS jasapelayanan_group,
          	IF(trawatinap_tindakan_verifikator.id, trawatinap_tindakan_verifikator.bhp_group, mtarif_rawatinap_detail.group_bhp) AS bhp_group,
          	IF(trawatinap_tindakan_verifikator.id, trawatinap_tindakan_verifikator.biayaperawatan_group, mtarif_rawatinap_detail.group_biayaperawatan) AS biayaperawatan_group,

          	trawatinap_tindakan_verifikator.jasasarana_keputusan AS jasasarana_keputusan,
          	trawatinap_tindakan_verifikator.jasapelayanan_keputusan AS jasapelayanan_keputusan,
          	trawatinap_tindakan_verifikator.bhp_keputusan AS bhp_keputusan,
          	trawatinap_tindakan_verifikator.biayaperawatan_keputusan AS biayaperawatan_keputusan,

          	IF(trawatinap_tindakan_verifikator.id,
          		(CASE WHEN trawatinap_tindakan_verifikator.jasasarana != mtarif_rawatinap_detail.jasasarana THEN 1 ELSE 0 END),
          		(CASE WHEN trawatinap_tindakan.jasasarana != mtarif_rawatinap_detail.jasasarana THEN 1 ELSE 0 END)
          	) AS jasasarana_status,

          	IF(trawatinap_tindakan_verifikator.id,
          		(CASE WHEN trawatinap_tindakan_verifikator.jasapelayanan != mtarif_rawatinap_detail.jasapelayanan THEN 1 ELSE 0 END),
          		(CASE WHEN trawatinap_tindakan.jasapelayanan != mtarif_rawatinap_detail.jasapelayanan THEN 1 ELSE 0 END)
          	) AS jasapelayanan_status,

          	IF(trawatinap_tindakan_verifikator.id,
          		(CASE WHEN trawatinap_tindakan_verifikator.bhp != mtarif_rawatinap_detail.bhp THEN 1 ELSE 0 END),
          		(CASE WHEN trawatinap_tindakan.bhp != mtarif_rawatinap_detail.bhp THEN 1 ELSE 0 END)
          	) AS bhp_status,

          	IF(trawatinap_tindakan_verifikator.id,
          		(CASE WHEN trawatinap_tindakan_verifikator.biayaperawatan != mtarif_rawatinap_detail.biayaperawatan THEN 1 ELSE 0 END),
          		(CASE WHEN trawatinap_tindakan.biayaperawatan != mtarif_rawatinap_detail.biayaperawatan THEN 1 ELSE 0 END)
          	) AS biayaperawatan_status

          FROM
          	trawatinap_tindakan
          LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_tindakan.idrawatinap
          LEFT JOIN mtarif_rawatinap_detail ON mtarif_rawatinap_detail.idtarif = trawatinap_tindakan.idpelayanan AND (mtarif_rawatinap_detail.kelas = trawatinap_pendaftaran.idkelas OR mtarif_rawatinap_detail.kelas = 1)
          LEFT JOIN mtarif_rawatinap ON mtarif_rawatinap.id = mtarif_rawatinap_detail.idtarif
          LEFT JOIN mdokter ON mdokter.id = trawatinap_tindakan.iddokter
          LEFT JOIN musers ON musers.id = trawatinap_tindakan.created_by
          LEFT JOIN trawatinap_tindakan_verifikator ON trawatinap_tindakan_verifikator.id = trawatinap_tindakan.id
          WHERE trawatinap_tindakan.id = $iddetail
          ");
		  return $query->row();

		  break;

		case 'trawatinap_visite':
		  $query = $this->db->query("SELECT
          	'mtarif_visitedokter' AS table_tarif,
          	trawatinap_visite.id,
          	trawatinap_visite.idpelayanan AS idtarif,
          	mtarif_visitedokter.nama AS namatarif,
          	musers.id AS iduserinput,
          	musers.name AS namauserinput,
          	mdokter.id AS iddokter,
          	mdokter.nama AS namadokter,

          	mtarif_visitedokter_detail.jasasarana AS jasasarana_master,
          	mtarif_visitedokter_detail.jasapelayanan AS jasapelayanan_master,
          	mtarif_visitedokter_detail.bhp AS bhp_master,
          	mtarif_visitedokter_detail.biayaperawatan AS biayaperawatan_master,

          	IF(trawatinap_visite_verifikator.id, trawatinap_visite_verifikator.jasasarana, trawatinap_visite.jasasarana) AS jasasarana_tarif,
          	IF(trawatinap_visite_verifikator.id, trawatinap_visite_verifikator.jasapelayanan, trawatinap_visite.jasapelayanan) AS jasapelayanan_tarif,
          	IF(trawatinap_visite_verifikator.id, trawatinap_visite_verifikator.bhp, trawatinap_visite.bhp) AS bhp_tarif,
          	IF(trawatinap_visite_verifikator.id, trawatinap_visite_verifikator.biayaperawatan, trawatinap_visite.biayaperawatan) AS biayaperawatan_tarif,

          	IF(trawatinap_visite_verifikator.id, trawatinap_visite_verifikator.jasasarana_disc, trawatinap_visite.jasasarana_disc) AS jasasarana_disc,
          	IF(trawatinap_visite_verifikator.id, trawatinap_visite_verifikator.jasapelayanan_disc, trawatinap_visite.jasapelayanan_disc) AS jasapelayanan_disc,
          	IF(trawatinap_visite_verifikator.id, trawatinap_visite_verifikator.bhp_disc, trawatinap_visite.bhp_disc) AS bhp_disc,
          	IF(trawatinap_visite_verifikator.id, trawatinap_visite_verifikator.biayaperawatan_disc, trawatinap_visite.biayaperawatan_disc) AS biayaperawatan_disc,

          	IF(trawatinap_visite_verifikator.id, trawatinap_visite_verifikator.jasasarana_group, mtarif_visitedokter_detail.group_jasasarana) AS jasasarana_group,
          	IF(trawatinap_visite_verifikator.id, trawatinap_visite_verifikator.jasapelayanan_group, mtarif_visitedokter_detail.group_jasapelayanan) AS jasapelayanan_group,
          	IF(trawatinap_visite_verifikator.id, trawatinap_visite_verifikator.bhp_group, mtarif_visitedokter_detail.group_bhp) AS bhp_group,
          	IF(trawatinap_visite_verifikator.id, trawatinap_visite_verifikator.biayaperawatan_group, mtarif_visitedokter_detail.group_biayaperawatan) AS biayaperawatan_group,

          	trawatinap_visite_verifikator.jasasarana_keputusan AS jasasarana_keputusan,
          	trawatinap_visite_verifikator.jasapelayanan_keputusan AS jasapelayanan_keputusan,
          	trawatinap_visite_verifikator.bhp_keputusan AS bhp_keputusan,
          	trawatinap_visite_verifikator.biayaperawatan_keputusan AS biayaperawatan_keputusan,

          	IF(trawatinap_visite_verifikator.id,
          		(CASE WHEN trawatinap_visite_verifikator.jasasarana != mtarif_visitedokter_detail.jasasarana THEN 1 ELSE 0 END),
          		(CASE WHEN trawatinap_visite.jasasarana != mtarif_visitedokter_detail.jasasarana THEN 1 ELSE 0 END)
          	) AS jasasarana_status,

          	IF(trawatinap_visite_verifikator.id,
          		(CASE WHEN trawatinap_visite_verifikator.jasapelayanan != mtarif_visitedokter_detail.jasapelayanan THEN 1 ELSE 0 END),
          		(CASE WHEN trawatinap_visite.jasapelayanan != mtarif_visitedokter_detail.jasapelayanan THEN 1 ELSE 0 END)
          	) AS jasapelayanan_status,

          	IF(trawatinap_visite_verifikator.id,
          		(CASE WHEN trawatinap_visite_verifikator.bhp != mtarif_visitedokter_detail.bhp THEN 1 ELSE 0 END),
          		(CASE WHEN trawatinap_visite.bhp != mtarif_visitedokter_detail.bhp THEN 1 ELSE 0 END)
          	) AS bhp_status,

          	IF(trawatinap_visite_verifikator.id,
          		(CASE WHEN trawatinap_visite_verifikator.biayaperawatan != mtarif_visitedokter_detail.biayaperawatan THEN 1 ELSE 0 END),
          		(CASE WHEN trawatinap_visite.biayaperawatan != mtarif_visitedokter_detail.biayaperawatan THEN 1 ELSE 0 END)
          	) AS biayaperawatan_status

          FROM
          	trawatinap_visite
          LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trawatinap_visite.idrawatinap
          LEFT JOIN mtarif_visitedokter_detail ON mtarif_visitedokter_detail.idtarif = trawatinap_visite.idpelayanan AND (mtarif_visitedokter_detail.kelas = trawatinap_pendaftaran.idkelas OR mtarif_visitedokter_detail.kelas = 1)
          LEFT JOIN mtarif_visitedokter ON mtarif_visitedokter.id = mtarif_visitedokter_detail.idtarif
          LEFT JOIN mdokter ON mdokter.id = trawatinap_visite.iddokter
          LEFT JOIN musers ON musers.id = trawatinap_visite.created_by
          LEFT JOIN trawatinap_visite_verifikator ON trawatinap_visite_verifikator.id = trawatinap_visite.id
          WHERE trawatinap_visite.id = $iddetail
          ");
		  return $query->row();

		  break;

		case 'tpoliklinik_pelayanan':
		  $query = $this->db->query("SELECT
          	'mtarif_rawatjalan' AS table_tarif,
          	tpoliklinik_pelayanan.id,
          	tpoliklinik_pelayanan.idpelayanan AS idtarif,
          	mtarif_rawatjalan.nama AS namatarif,
          	musers.id AS iduserinput,
          	musers.name AS namauserinput,
          	mdokter.id AS iddokter,
          	mdokter.nama AS namadokter,

          	mtarif_rawatjalan.jasasarana AS jasasarana_master,
          	mtarif_rawatjalan.jasapelayanan AS jasapelayanan_master,
          	mtarif_rawatjalan.bhp AS bhp_master,
          	mtarif_rawatjalan.biayaperawatan AS biayaperawatan_master,

          	IF(tpoliklinik_pelayanan_verifikator.id, tpoliklinik_pelayanan_verifikator.jasasarana, tpoliklinik_pelayanan.jasasarana) AS jasasarana_tarif,
          	IF(tpoliklinik_pelayanan_verifikator.id, tpoliklinik_pelayanan_verifikator.jasapelayanan, tpoliklinik_pelayanan.jasapelayanan) AS jasapelayanan_tarif,
          	IF(tpoliklinik_pelayanan_verifikator.id, tpoliklinik_pelayanan_verifikator.bhp, tpoliklinik_pelayanan.bhp) AS bhp_tarif,
          	IF(tpoliklinik_pelayanan_verifikator.id, tpoliklinik_pelayanan_verifikator.biayaperawatan, tpoliklinik_pelayanan.biayaperawatan) AS biayaperawatan_tarif,

          	IF(tpoliklinik_pelayanan_verifikator.id, tpoliklinik_pelayanan_verifikator.jasasarana_disc, tpoliklinik_pelayanan.jasasarana_disc) AS jasasarana_disc,
          	IF(tpoliklinik_pelayanan_verifikator.id, tpoliklinik_pelayanan_verifikator.jasapelayanan_disc, tpoliklinik_pelayanan.jasapelayanan_disc) AS jasapelayanan_disc,
          	IF(tpoliklinik_pelayanan_verifikator.id, tpoliklinik_pelayanan_verifikator.bhp_disc, tpoliklinik_pelayanan.bhp_disc) AS bhp_disc,
          	IF(tpoliklinik_pelayanan_verifikator.id, tpoliklinik_pelayanan_verifikator.biayaperawatan_disc, tpoliklinik_pelayanan.biayaperawatan_disc) AS biayaperawatan_disc,

          	IF(tpoliklinik_pelayanan_verifikator.id, tpoliklinik_pelayanan_verifikator.jasasarana_group, mtarif_rawatjalan.group_jasasarana) AS jasasarana_group,
          	IF(tpoliklinik_pelayanan_verifikator.id, tpoliklinik_pelayanan_verifikator.jasapelayanan_group, mtarif_rawatjalan.group_jasapelayanan) AS jasapelayanan_group,
          	IF(tpoliklinik_pelayanan_verifikator.id, tpoliklinik_pelayanan_verifikator.bhp_group, mtarif_rawatjalan.group_bhp) AS bhp_group,
          	IF(tpoliklinik_pelayanan_verifikator.id, tpoliklinik_pelayanan_verifikator.biayaperawatan_group, mtarif_rawatjalan.group_biayaperawatan) AS biayaperawatan_group,

          	tpoliklinik_pelayanan_verifikator.jasasarana_keputusan AS jasasarana_keputusan,
          	tpoliklinik_pelayanan_verifikator.jasapelayanan_keputusan AS jasapelayanan_keputusan,
          	tpoliklinik_pelayanan_verifikator.bhp_keputusan AS bhp_keputusan,
          	tpoliklinik_pelayanan_verifikator.biayaperawatan_keputusan AS biayaperawatan_keputusan,

          	IF(tpoliklinik_pelayanan_verifikator.id,
          		(CASE WHEN tpoliklinik_pelayanan_verifikator.jasasarana != mtarif_rawatjalan.jasasarana THEN 1 ELSE 0 END),
          		(CASE WHEN tpoliklinik_pelayanan.jasasarana != mtarif_rawatjalan.jasasarana THEN 1 ELSE 0 END)
          	) AS jasasarana_status,

          	IF(tpoliklinik_pelayanan_verifikator.id,
          		(CASE WHEN tpoliklinik_pelayanan_verifikator.jasapelayanan != mtarif_rawatjalan.jasapelayanan THEN 1 ELSE 0 END),
          		(CASE WHEN tpoliklinik_pelayanan.jasapelayanan != mtarif_rawatjalan.jasapelayanan THEN 1 ELSE 0 END)
          	) AS jasapelayanan_status,

          	IF(tpoliklinik_pelayanan_verifikator.id,
          		(CASE WHEN tpoliklinik_pelayanan_verifikator.bhp != mtarif_rawatjalan.bhp THEN 1 ELSE 0 END),
          		(CASE WHEN tpoliklinik_pelayanan.bhp != mtarif_rawatjalan.bhp THEN 1 ELSE 0 END)
          	) AS bhp_status,

          	IF(tpoliklinik_pelayanan_verifikator.id,
          		(CASE WHEN tpoliklinik_pelayanan_verifikator.biayaperawatan != mtarif_rawatjalan.biayaperawatan THEN 1 ELSE 0 END),
          		(CASE WHEN tpoliklinik_pelayanan.biayaperawatan != mtarif_rawatjalan.biayaperawatan THEN 1 ELSE 0 END)
          	) AS biayaperawatan_status

          FROM
          	tpoliklinik_pelayanan
          LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpoliklinik_pelayanan.idtindakan
          LEFT JOIN mtarif_rawatjalan ON mtarif_rawatjalan.id = tpoliklinik_pelayanan.idpelayanan
          LEFT JOIN mdokter ON mdokter.id = tpoliklinik_pelayanan.iddokter
          LEFT JOIN musers ON musers.id = tpoliklinik_tindakan.iduser_tindakan
          LEFT JOIN tpoliklinik_pelayanan_verifikator ON tpoliklinik_pelayanan_verifikator.id = tpoliklinik_pelayanan.id
          WHERE tpoliklinik_pelayanan.id = $iddetail");
		  return $query->row();

		  break;

		case 'trujukan_laboratorium_detail':
		  $query = $this->db->query("SELECT
          	'mtarif_laboratorium' AS table_tarif,
        		trujukan_laboratorium_detail.id,
        		trujukan_laboratorium_detail.idlaboratorium AS idtarif,
        		mtarif_laboratorium.nama AS namatarif,
        		musers.id AS iduserinput,
        		musers.name AS namauserinput,
        		0 AS iddokter,
        		'-' AS namadokter,

        		mtarif_laboratorium_detail.jasasarana AS jasasarana_master,
        		mtarif_laboratorium_detail.jasapelayanan AS jasapelayanan_master,
        		mtarif_laboratorium_detail.bhp AS bhp_master,
        		mtarif_laboratorium_detail.biayaperawatan AS biayaperawatan_master,

        		IF(trujukan_laboratorium_detail_verifikator.id, trujukan_laboratorium_detail_verifikator.jasasarana, trujukan_laboratorium_detail.jasasarana) AS jasasarana_tarif,
        		IF(trujukan_laboratorium_detail_verifikator.id, trujukan_laboratorium_detail_verifikator.jasapelayanan, trujukan_laboratorium_detail.jasapelayanan) AS jasapelayanan_tarif,
        		IF(trujukan_laboratorium_detail_verifikator.id, trujukan_laboratorium_detail_verifikator.bhp, trujukan_laboratorium_detail.bhp) AS bhp_tarif,
        		IF(trujukan_laboratorium_detail_verifikator.id, trujukan_laboratorium_detail_verifikator.biayaperawatan, trujukan_laboratorium_detail.biayaperawatan) AS biayaperawatan_tarif,

        		IF(trujukan_laboratorium_detail_verifikator.id, trujukan_laboratorium_detail_verifikator.jasasarana_disc, trujukan_laboratorium_detail.jasasarana_disc) AS jasasarana_disc,
        		IF(trujukan_laboratorium_detail_verifikator.id, trujukan_laboratorium_detail_verifikator.jasapelayanan_disc, trujukan_laboratorium_detail.jasapelayanan_disc) AS jasapelayanan_disc,
        		IF(trujukan_laboratorium_detail_verifikator.id, trujukan_laboratorium_detail_verifikator.bhp_disc, trujukan_laboratorium_detail.bhp_disc) AS bhp_disc,
        		IF(trujukan_laboratorium_detail_verifikator.id, trujukan_laboratorium_detail_verifikator.biayaperawatan_disc, trujukan_laboratorium_detail.biayaperawatan_disc) AS biayaperawatan_disc,

        		IF(trujukan_laboratorium_detail_verifikator.id, trujukan_laboratorium_detail_verifikator.jasasarana_group, mtarif_laboratorium_detail.group_jasasarana) AS jasasarana_group,
        		IF(trujukan_laboratorium_detail_verifikator.id, trujukan_laboratorium_detail_verifikator.jasapelayanan_group, mtarif_laboratorium_detail.group_jasapelayanan) AS jasapelayanan_group,
        		IF(trujukan_laboratorium_detail_verifikator.id, trujukan_laboratorium_detail_verifikator.bhp_group, mtarif_laboratorium_detail.group_bhp) AS bhp_group,
        		IF(trujukan_laboratorium_detail_verifikator.id, trujukan_laboratorium_detail_verifikator.biayaperawatan_group, mtarif_laboratorium_detail.group_biayaperawatan) AS biayaperawatan_group,

        		trujukan_laboratorium_detail_verifikator.jasasarana_keputusan AS jasasarana_keputusan,
        		trujukan_laboratorium_detail_verifikator.jasapelayanan_keputusan AS jasapelayanan_keputusan,
        		trujukan_laboratorium_detail_verifikator.bhp_keputusan AS bhp_keputusan,
        		trujukan_laboratorium_detail_verifikator.biayaperawatan_keputusan AS biayaperawatan_keputusan,

        		IF(trujukan_laboratorium_detail_verifikator.id,
        			(CASE WHEN trujukan_laboratorium_detail_verifikator.jasasarana != mtarif_laboratorium_detail.jasasarana THEN 1 ELSE 0 END),
        			(CASE WHEN trujukan_laboratorium_detail.jasasarana != mtarif_laboratorium_detail.jasasarana THEN 1 ELSE 0 END)
        		) AS jasasarana_status,

        		IF(trujukan_laboratorium_detail_verifikator.id,
        			(CASE WHEN trujukan_laboratorium_detail_verifikator.jasapelayanan != mtarif_laboratorium_detail.jasapelayanan THEN 1 ELSE 0 END),
        			(CASE WHEN trujukan_laboratorium_detail.jasapelayanan != mtarif_laboratorium_detail.jasapelayanan THEN 1 ELSE 0 END)
        		) AS jasapelayanan_status,

        		IF(trujukan_laboratorium_detail_verifikator.id,
        			(CASE WHEN trujukan_laboratorium_detail_verifikator.bhp != mtarif_laboratorium_detail.bhp THEN 1 ELSE 0 END),
        			(CASE WHEN trujukan_laboratorium_detail.bhp != mtarif_laboratorium_detail.bhp THEN 1 ELSE 0 END)
        		) AS bhp_status,

        		IF(trujukan_laboratorium_detail_verifikator.id,
        			(CASE WHEN trujukan_laboratorium_detail_verifikator.biayaperawatan != mtarif_laboratorium_detail.biayaperawatan THEN 1 ELSE 0 END),
        			(CASE WHEN trujukan_laboratorium_detail.biayaperawatan != mtarif_laboratorium_detail.biayaperawatan THEN 1 ELSE 0 END)
        		) AS biayaperawatan_status

        	FROM
        		trujukan_laboratorium_detail
        	LEFT JOIN trujukan_laboratorium ON trujukan_laboratorium.id = trujukan_laboratorium_detail.idrujukan
        	LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = trujukan_laboratorium.idtindakan AND trujukan_laboratorium.asalrujukan IN (1,2)
        	LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
        	LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id OR (trawatinap_pendaftaran.id = trujukan_laboratorium.idtindakan AND trujukan_laboratorium.asalrujukan = 3)
        	LEFT JOIN mtarif_laboratorium_detail ON mtarif_laboratorium_detail.idtarif = trujukan_laboratorium_detail.idlaboratorium AND (mtarif_laboratorium_detail.kelas = trawatinap_pendaftaran.idkelas OR mtarif_laboratorium_detail.kelas = 1)
        	LEFT JOIN mtarif_laboratorium ON mtarif_laboratorium.id = mtarif_laboratorium_detail.idtarif
        	LEFT JOIN musers ON musers.id = trujukan_laboratorium.iduser_input_pemeriksaan
        	LEFT JOIN trujukan_laboratorium_detail_verifikator ON trujukan_laboratorium_detail_verifikator.id = trujukan_laboratorium_detail.id
        	WHERE trujukan_laboratorium_detail.id = $iddetail");
		  return $query->row();

		  break;

		case 'trujukan_radiologi_detail':
		  $query = $this->db->query("SELECT
          	'mtarif_radiologi' AS table_tarif,
        		trujukan_radiologi_detail.id,
        		trujukan_radiologi_detail.idradiologi AS idtarif,
            (CASE
      				WHEN ( mtarif_radiologi.idtipe = 1 ) THEN
      					concat(
      						mtarif_radiologi.nama,
      						' (',
      						mtarif_radiologi_expose.nama,
      						' ',
      						mtarif_radiologi_film.nama,
      						')'
      					) ELSE mtarif_radiologi.nama
      				END
      			) AS namatarif,
        		musers.id AS iduserinput,
        		musers.name AS namauserinput,
        		0 AS iddokter,
        		'-' AS namadokter,

        		mtarif_radiologi_detail.jasasarana AS jasasarana_master,
        		mtarif_radiologi_detail.jasapelayanan AS jasapelayanan_master,
        		mtarif_radiologi_detail.bhp AS bhp_master,
        		mtarif_radiologi_detail.biayaperawatan AS biayaperawatan_master,

        		IF(trujukan_radiologi_detail_verifikator.id, trujukan_radiologi_detail_verifikator.jasasarana, trujukan_radiologi_detail.jasasarana) AS jasasarana_tarif,
        		IF(trujukan_radiologi_detail_verifikator.id, trujukan_radiologi_detail_verifikator.jasapelayanan, trujukan_radiologi_detail.jasapelayanan) AS jasapelayanan_tarif,
        		IF(trujukan_radiologi_detail_verifikator.id, trujukan_radiologi_detail_verifikator.bhp, trujukan_radiologi_detail.bhp) AS bhp_tarif,
        		IF(trujukan_radiologi_detail_verifikator.id, trujukan_radiologi_detail_verifikator.biayaperawatan, trujukan_radiologi_detail.biayaperawatan) AS biayaperawatan_tarif,

        		IF(trujukan_radiologi_detail_verifikator.id, trujukan_radiologi_detail_verifikator.jasasarana_disc, trujukan_radiologi_detail.jasasarana_disc) AS jasasarana_disc,
        		IF(trujukan_radiologi_detail_verifikator.id, trujukan_radiologi_detail_verifikator.jasapelayanan_disc, trujukan_radiologi_detail.jasapelayanan_disc) AS jasapelayanan_disc,
        		IF(trujukan_radiologi_detail_verifikator.id, trujukan_radiologi_detail_verifikator.bhp_disc, trujukan_radiologi_detail.bhp_disc) AS bhp_disc,
        		IF(trujukan_radiologi_detail_verifikator.id, trujukan_radiologi_detail_verifikator.biayaperawatan_disc, trujukan_radiologi_detail.biayaperawatan_disc) AS biayaperawatan_disc,

        		IF(trujukan_radiologi_detail_verifikator.id, trujukan_radiologi_detail_verifikator.jasasarana_group, mtarif_radiologi_detail.group_jasasarana) AS jasasarana_group,
        		IF(trujukan_radiologi_detail_verifikator.id, trujukan_radiologi_detail_verifikator.jasapelayanan_group, mtarif_radiologi_detail.group_jasapelayanan) AS jasapelayanan_group,
        		IF(trujukan_radiologi_detail_verifikator.id, trujukan_radiologi_detail_verifikator.bhp_group, mtarif_radiologi_detail.group_bhp) AS bhp_group,
        		IF(trujukan_radiologi_detail_verifikator.id, trujukan_radiologi_detail_verifikator.biayaperawatan_group, mtarif_radiologi_detail.group_biayaperawatan) AS biayaperawatan_group,

        		trujukan_radiologi_detail_verifikator.jasasarana_keputusan AS jasasarana_keputusan,
        		trujukan_radiologi_detail_verifikator.jasapelayanan_keputusan AS jasapelayanan_keputusan,
        		trujukan_radiologi_detail_verifikator.bhp_keputusan AS bhp_keputusan,
        		trujukan_radiologi_detail_verifikator.biayaperawatan_keputusan AS biayaperawatan_keputusan,

        		IF(trujukan_radiologi_detail_verifikator.id,
        			(CASE WHEN trujukan_radiologi_detail_verifikator.jasasarana != mtarif_radiologi_detail.jasasarana THEN 1 ELSE 0 END),
        			(CASE WHEN trujukan_radiologi_detail.jasasarana != mtarif_radiologi_detail.jasasarana THEN 1 ELSE 0 END)
        		) AS jasasarana_status,

        		IF(trujukan_radiologi_detail_verifikator.id,
        			(CASE WHEN trujukan_radiologi_detail_verifikator.jasapelayanan != mtarif_radiologi_detail.jasapelayanan THEN 1 ELSE 0 END),
        			(CASE WHEN trujukan_radiologi_detail.jasapelayanan != mtarif_radiologi_detail.jasapelayanan THEN 1 ELSE 0 END)
        		) AS jasapelayanan_status,

        		IF(trujukan_radiologi_detail_verifikator.id,
        			(CASE WHEN trujukan_radiologi_detail_verifikator.bhp != mtarif_radiologi_detail.bhp THEN 1 ELSE 0 END),
        			(CASE WHEN trujukan_radiologi_detail.bhp != mtarif_radiologi_detail.bhp THEN 1 ELSE 0 END)
        		) AS bhp_status,

        		IF(trujukan_radiologi_detail_verifikator.id,
        			(CASE WHEN trujukan_radiologi_detail_verifikator.biayaperawatan != mtarif_radiologi_detail.biayaperawatan THEN 1 ELSE 0 END),
        			(CASE WHEN trujukan_radiologi_detail.biayaperawatan != mtarif_radiologi_detail.biayaperawatan THEN 1 ELSE 0 END)
        		) AS biayaperawatan_status

        	FROM
        		trujukan_radiologi_detail
        	LEFT JOIN trujukan_radiologi ON trujukan_radiologi.id = trujukan_radiologi_detail.idrujukan
        	LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = trujukan_radiologi.idtindakan AND trujukan_radiologi.asalrujukan IN (1,2)
        	LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
        	LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id OR (trawatinap_pendaftaran.id = trujukan_radiologi.idtindakan AND trujukan_radiologi.asalrujukan = 3)
        	LEFT JOIN mtarif_radiologi_detail ON mtarif_radiologi_detail.idtarif = trujukan_radiologi_detail.idradiologi AND (mtarif_radiologi_detail.kelas = trawatinap_pendaftaran.idkelas OR mtarif_radiologi_detail.kelas = 1)
        	LEFT JOIN mtarif_radiologi ON mtarif_radiologi.id = mtarif_radiologi_detail.idtarif
          LEFT JOIN mtarif_radiologi_expose ON mtarif_radiologi.idexpose = mtarif_radiologi_expose.id
      		LEFT JOIN mtarif_radiologi_film ON mtarif_radiologi.idfilm = mtarif_radiologi_film.id
        	LEFT JOIN musers ON musers.id = trujukan_radiologi.iduser_input_pemeriksaan
        	LEFT JOIN trujukan_radiologi_detail_verifikator ON trujukan_radiologi_detail_verifikator.id = trujukan_radiologi_detail.id
        	WHERE trujukan_radiologi_detail.id = $iddetail");
		  return $query->row();

		  break;

		case 'trujukan_fisioterapi_detail':
		  $query = $this->db->query("SELECT
          	'mtarif_fisioterapi' AS table_tarif,
        		trujukan_fisioterapi_detail.id,
        		trujukan_fisioterapi_detail.idfisioterapi AS idtarif,
        		mtarif_fisioterapi.nama AS namatarif,
        		musers.id AS iduserinput,
        		musers.name AS namauserinput,
        		0 AS iddokter,
        		'-' AS namadokter,

        		mtarif_fisioterapi_detail.jasasarana AS jasasarana_master,
        		mtarif_fisioterapi_detail.jasapelayanan AS jasapelayanan_master,
        		mtarif_fisioterapi_detail.bhp AS bhp_master,
        		mtarif_fisioterapi_detail.biayaperawatan AS biayaperawatan_master,

        		IF(trujukan_fisioterapi_detail_verifikator.id, trujukan_fisioterapi_detail_verifikator.jasasarana, trujukan_fisioterapi_detail.jasasarana) AS jasasarana_tarif,
        		IF(trujukan_fisioterapi_detail_verifikator.id, trujukan_fisioterapi_detail_verifikator.jasapelayanan, trujukan_fisioterapi_detail.jasapelayanan) AS jasapelayanan_tarif,
        		IF(trujukan_fisioterapi_detail_verifikator.id, trujukan_fisioterapi_detail_verifikator.bhp, trujukan_fisioterapi_detail.bhp) AS bhp_tarif,
        		IF(trujukan_fisioterapi_detail_verifikator.id, trujukan_fisioterapi_detail_verifikator.biayaperawatan, trujukan_fisioterapi_detail.biayaperawatan) AS biayaperawatan_tarif,

        		IF(trujukan_fisioterapi_detail_verifikator.id, trujukan_fisioterapi_detail_verifikator.jasasarana_disc, trujukan_fisioterapi_detail.jasasarana_disc) AS jasasarana_disc,
        		IF(trujukan_fisioterapi_detail_verifikator.id, trujukan_fisioterapi_detail_verifikator.jasapelayanan_disc, trujukan_fisioterapi_detail.jasapelayanan_disc) AS jasapelayanan_disc,
        		IF(trujukan_fisioterapi_detail_verifikator.id, trujukan_fisioterapi_detail_verifikator.bhp_disc, trujukan_fisioterapi_detail.bhp_disc) AS bhp_disc,
        		IF(trujukan_fisioterapi_detail_verifikator.id, trujukan_fisioterapi_detail_verifikator.biayaperawatan_disc, trujukan_fisioterapi_detail.biayaperawatan_disc) AS biayaperawatan_disc,

        		IF(trujukan_fisioterapi_detail_verifikator.id, trujukan_fisioterapi_detail_verifikator.jasasarana_group, mtarif_fisioterapi_detail.group_jasasarana) AS jasasarana_group,
        		IF(trujukan_fisioterapi_detail_verifikator.id, trujukan_fisioterapi_detail_verifikator.jasapelayanan_group, mtarif_fisioterapi_detail.group_jasapelayanan) AS jasapelayanan_group,
        		IF(trujukan_fisioterapi_detail_verifikator.id, trujukan_fisioterapi_detail_verifikator.bhp_group, mtarif_fisioterapi_detail.group_bhp) AS bhp_group,
        		IF(trujukan_fisioterapi_detail_verifikator.id, trujukan_fisioterapi_detail_verifikator.biayaperawatan_group, mtarif_fisioterapi_detail.group_biayaperawatan) AS biayaperawatan_group,

        		trujukan_fisioterapi_detail_verifikator.jasasarana_keputusan AS jasasarana_keputusan,
        		trujukan_fisioterapi_detail_verifikator.jasapelayanan_keputusan AS jasapelayanan_keputusan,
        		trujukan_fisioterapi_detail_verifikator.bhp_keputusan AS bhp_keputusan,
        		trujukan_fisioterapi_detail_verifikator.biayaperawatan_keputusan AS biayaperawatan_keputusan,

        		IF(trujukan_fisioterapi_detail_verifikator.id,
        			(CASE WHEN trujukan_fisioterapi_detail_verifikator.jasasarana != mtarif_fisioterapi_detail.jasasarana THEN 1 ELSE 0 END),
        			(CASE WHEN trujukan_fisioterapi_detail.jasasarana != mtarif_fisioterapi_detail.jasasarana THEN 1 ELSE 0 END)
        		) AS jasasarana_status,

        		IF(trujukan_fisioterapi_detail_verifikator.id,
        			(CASE WHEN trujukan_fisioterapi_detail_verifikator.jasapelayanan != mtarif_fisioterapi_detail.jasapelayanan THEN 1 ELSE 0 END),
        			(CASE WHEN trujukan_fisioterapi_detail.jasapelayanan != mtarif_fisioterapi_detail.jasapelayanan THEN 1 ELSE 0 END)
        		) AS jasapelayanan_status,

        		IF(trujukan_fisioterapi_detail_verifikator.id,
        			(CASE WHEN trujukan_fisioterapi_detail_verifikator.bhp != mtarif_fisioterapi_detail.bhp THEN 1 ELSE 0 END),
        			(CASE WHEN trujukan_fisioterapi_detail.bhp != mtarif_fisioterapi_detail.bhp THEN 1 ELSE 0 END)
        		) AS bhp_status,

        		IF(trujukan_fisioterapi_detail_verifikator.id,
        			(CASE WHEN trujukan_fisioterapi_detail_verifikator.biayaperawatan != mtarif_fisioterapi_detail.biayaperawatan THEN 1 ELSE 0 END),
        			(CASE WHEN trujukan_fisioterapi_detail.biayaperawatan != mtarif_fisioterapi_detail.biayaperawatan THEN 1 ELSE 0 END)
        		) AS biayaperawatan_status

        	FROM
        		trujukan_fisioterapi_detail
        	LEFT JOIN trujukan_fisioterapi ON trujukan_fisioterapi.id = trujukan_fisioterapi_detail.idrujukan
        	LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = trujukan_fisioterapi.idtindakan AND trujukan_fisioterapi.asalrujukan IN (1,2)
        	LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
        	LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id OR (trawatinap_pendaftaran.id = trujukan_fisioterapi.idtindakan AND trujukan_fisioterapi.asalrujukan = 3)
        	LEFT JOIN mtarif_fisioterapi_detail ON mtarif_fisioterapi_detail.idtarif = trujukan_fisioterapi_detail.idfisioterapi AND (mtarif_fisioterapi_detail.kelas = trawatinap_pendaftaran.idkelas OR mtarif_fisioterapi_detail.kelas = 1)
        	LEFT JOIN mtarif_fisioterapi ON mtarif_fisioterapi.id = mtarif_fisioterapi_detail.idtarif
        	LEFT JOIN musers ON musers.id = trujukan_fisioterapi.iduser_tindakan
        	LEFT JOIN trujukan_fisioterapi_detail_verifikator ON trujukan_fisioterapi_detail_verifikator.id = trujukan_fisioterapi_detail.id
        	WHERE trujukan_fisioterapi_detail.id = $iddetail");
		  return $query->row();

		  break;

		case 'tkamaroperasi_sewaalat':
		  $query = $this->db->query("SELECT
          	'mtarif_operasi_sewaalat' AS table_tarif,
          	tkamaroperasi_sewaalat.id,
          	tkamaroperasi_sewaalat.idalat AS idtarif,
          	mtarif_operasi_sewaalat.nama AS namatarif,
          	musers.id AS iduserinput,
          	musers.name AS namauserinput,
          	0 AS iddokter,
          	'-' AS namadokter,

          	mtarif_operasi_sewaalat_detail.jasasarana AS jasasarana_master,
          	mtarif_operasi_sewaalat_detail.jasapelayanan AS jasapelayanan_master,
          	mtarif_operasi_sewaalat_detail.bhp AS bhp_master,
          	mtarif_operasi_sewaalat_detail.biayaperawatan AS biayaperawatan_master,

          	IF(tkamaroperasi_sewaalat_verifikator.id, tkamaroperasi_sewaalat_verifikator.jasasarana, tkamaroperasi_sewaalat.jasasarana) AS jasasarana_tarif,
          	IF(tkamaroperasi_sewaalat_verifikator.id, tkamaroperasi_sewaalat_verifikator.jasapelayanan, tkamaroperasi_sewaalat.jasapelayanan) AS jasapelayanan_tarif,
          	IF(tkamaroperasi_sewaalat_verifikator.id, tkamaroperasi_sewaalat_verifikator.bhp, tkamaroperasi_sewaalat.bhp) AS bhp_tarif,
          	IF(tkamaroperasi_sewaalat_verifikator.id, tkamaroperasi_sewaalat_verifikator.biayaperawatan, tkamaroperasi_sewaalat.biayaperawatan) AS biayaperawatan_tarif,

          	IF(tkamaroperasi_sewaalat_verifikator.id, tkamaroperasi_sewaalat_verifikator.jasasarana_disc, tkamaroperasi_sewaalat.jasasarana_disc) AS jasasarana_disc,
          	IF(tkamaroperasi_sewaalat_verifikator.id, tkamaroperasi_sewaalat_verifikator.jasapelayanan_disc, tkamaroperasi_sewaalat.jasapelayanan_disc) AS jasapelayanan_disc,
          	IF(tkamaroperasi_sewaalat_verifikator.id, tkamaroperasi_sewaalat_verifikator.bhp_disc, tkamaroperasi_sewaalat.bhp_disc) AS bhp_disc,
          	IF(tkamaroperasi_sewaalat_verifikator.id, tkamaroperasi_sewaalat_verifikator.biayaperawatan_disc, tkamaroperasi_sewaalat.biayaperawatan_disc) AS biayaperawatan_disc,

          	IF(tkamaroperasi_sewaalat_verifikator.id, tkamaroperasi_sewaalat_verifikator.jasasarana_group, mtarif_operasi_sewaalat_detail.group_jasasarana) AS jasasarana_group,
          	IF(tkamaroperasi_sewaalat_verifikator.id, tkamaroperasi_sewaalat_verifikator.jasapelayanan_group, mtarif_operasi_sewaalat_detail.group_jasapelayanan) AS jasapelayanan_group,
          	IF(tkamaroperasi_sewaalat_verifikator.id, tkamaroperasi_sewaalat_verifikator.bhp_group, mtarif_operasi_sewaalat_detail.group_bhp) AS bhp_group,
          	IF(tkamaroperasi_sewaalat_verifikator.id, tkamaroperasi_sewaalat_verifikator.biayaperawatan_group, mtarif_operasi_sewaalat_detail.group_biayaperawatan) AS biayaperawatan_group,

          	tkamaroperasi_sewaalat_verifikator.jasasarana_keputusan AS jasasarana_keputusan,
          	tkamaroperasi_sewaalat_verifikator.jasapelayanan_keputusan AS jasapelayanan_keputusan,
          	tkamaroperasi_sewaalat_verifikator.bhp_keputusan AS bhp_keputusan,
          	tkamaroperasi_sewaalat_verifikator.biayaperawatan_keputusan AS biayaperawatan_keputusan,

          	IF(tkamaroperasi_sewaalat_verifikator.id,
          		(CASE WHEN tkamaroperasi_sewaalat_verifikator.jasasarana != mtarif_operasi_sewaalat_detail.jasasarana THEN 1 ELSE 0 END),
          		(CASE WHEN tkamaroperasi_sewaalat.jasasarana != mtarif_operasi_sewaalat_detail.jasasarana THEN 1 ELSE 0 END)
          	) AS jasasarana_status,

          	IF(tkamaroperasi_sewaalat_verifikator.id,
          		(CASE WHEN tkamaroperasi_sewaalat_verifikator.jasapelayanan != mtarif_operasi_sewaalat_detail.jasapelayanan THEN 1 ELSE 0 END),
          		(CASE WHEN tkamaroperasi_sewaalat.jasapelayanan != mtarif_operasi_sewaalat_detail.jasapelayanan THEN 1 ELSE 0 END)
          	) AS jasapelayanan_status,

          	IF(tkamaroperasi_sewaalat_verifikator.id,
          		(CASE WHEN tkamaroperasi_sewaalat_verifikator.bhp != mtarif_operasi_sewaalat_detail.bhp THEN 1 ELSE 0 END),
          		(CASE WHEN tkamaroperasi_sewaalat.bhp != mtarif_operasi_sewaalat_detail.bhp THEN 1 ELSE 0 END)
          	) AS bhp_status,

          	IF(tkamaroperasi_sewaalat_verifikator.id,
          		(CASE WHEN tkamaroperasi_sewaalat_verifikator.biayaperawatan != mtarif_operasi_sewaalat_detail.biayaperawatan THEN 1 ELSE 0 END),
          		(CASE WHEN tkamaroperasi_sewaalat.biayaperawatan != mtarif_operasi_sewaalat_detail.biayaperawatan THEN 1 ELSE 0 END)
          	) AS biayaperawatan_status

          FROM
          	tkamaroperasi_sewaalat
          LEFT JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_sewaalat.idpendaftaranoperasi
          LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran
          LEFT JOIN mtarif_operasi_sewaalat_detail ON mtarif_operasi_sewaalat_detail.idtarif = tkamaroperasi_sewaalat.idalat AND mtarif_operasi_sewaalat_detail.idjenis = tkamaroperasi_pendaftaran.jenis_operasi_id AND (mtarif_operasi_sewaalat_detail.kelas = trawatinap_pendaftaran.idkelas OR mtarif_operasi_sewaalat_detail.kelas = 1)
          LEFT JOIN mtarif_operasi_sewaalat ON mtarif_operasi_sewaalat.id = mtarif_operasi_sewaalat_detail.idtarif
          LEFT JOIN musers ON musers.id = tkamaroperasi_sewaalat.iduser_transaksi
          LEFT JOIN tkamaroperasi_sewaalat_verifikator ON tkamaroperasi_sewaalat_verifikator.id = tkamaroperasi_sewaalat.id
          WHERE tkamaroperasi_sewaalat.id = $iddetail");
		  return $query->row();

		  break;

		case 'tkamaroperasi_ruangan':
		  $query = $this->db->query("SELECT
          	'mtarif_operasi' AS table_tarif,
          	tkamaroperasi_ruangan.id,
          	tkamaroperasi_ruangan.id_tarif AS idtarif,
          	mtarif_operasi.nama AS namatarif,
          	musers.id AS iduserinput,
          	musers.name AS namauserinput,
          	0 AS iddokter,
          	'-' AS namadokter,

          	mtarif_operasi_detail.jasasarana AS jasasarana_master,
          	mtarif_operasi_detail.jasapelayanan AS jasapelayanan_master,
          	mtarif_operasi_detail.bhp AS bhp_master,
          	mtarif_operasi_detail.biayaperawatan AS biayaperawatan_master,

          	IF(tkamaroperasi_ruangan_verifikator.id, tkamaroperasi_ruangan_verifikator.jasasarana, tkamaroperasi_ruangan.jasasarana) AS jasasarana_tarif,
          	IF(tkamaroperasi_ruangan_verifikator.id, tkamaroperasi_ruangan_verifikator.jasapelayanan, tkamaroperasi_ruangan.jasapelayanan) AS jasapelayanan_tarif,
          	IF(tkamaroperasi_ruangan_verifikator.id, tkamaroperasi_ruangan_verifikator.bhp, tkamaroperasi_ruangan.bhp) AS bhp_tarif,
          	IF(tkamaroperasi_ruangan_verifikator.id, tkamaroperasi_ruangan_verifikator.biayaperawatan, tkamaroperasi_ruangan.biayaperawatan) AS biayaperawatan_tarif,

          	IF(tkamaroperasi_ruangan_verifikator.id, tkamaroperasi_ruangan_verifikator.jasasarana_disc, tkamaroperasi_ruangan.jasasarana_disc) AS jasasarana_disc,
          	IF(tkamaroperasi_ruangan_verifikator.id, tkamaroperasi_ruangan_verifikator.jasapelayanan_disc, tkamaroperasi_ruangan.jasapelayanan_disc) AS jasapelayanan_disc,
          	IF(tkamaroperasi_ruangan_verifikator.id, tkamaroperasi_ruangan_verifikator.bhp_disc, tkamaroperasi_ruangan.bhp_disc) AS bhp_disc,
          	IF(tkamaroperasi_ruangan_verifikator.id, tkamaroperasi_ruangan_verifikator.biayaperawatan_disc, tkamaroperasi_ruangan.biayaperawatan_disc) AS biayaperawatan_disc,

          	IF(tkamaroperasi_ruangan_verifikator.id, tkamaroperasi_ruangan_verifikator.jasasarana_group, mtarif_operasi_detail.group_jasasarana) AS jasasarana_group,
          	IF(tkamaroperasi_ruangan_verifikator.id, tkamaroperasi_ruangan_verifikator.jasapelayanan_group, mtarif_operasi_detail.group_jasapelayanan) AS jasapelayanan_group,
          	IF(tkamaroperasi_ruangan_verifikator.id, tkamaroperasi_ruangan_verifikator.bhp_group, mtarif_operasi_detail.group_bhp) AS bhp_group,
          	IF(tkamaroperasi_ruangan_verifikator.id, tkamaroperasi_ruangan_verifikator.biayaperawatan_group, mtarif_operasi_detail.group_biayaperawatan) AS biayaperawatan_group,

          	tkamaroperasi_ruangan_verifikator.jasasarana_keputusan AS jasasarana_keputusan,
          	tkamaroperasi_ruangan_verifikator.jasapelayanan_keputusan AS jasapelayanan_keputusan,
          	tkamaroperasi_ruangan_verifikator.bhp_keputusan AS bhp_keputusan,
          	tkamaroperasi_ruangan_verifikator.biayaperawatan_keputusan AS biayaperawatan_keputusan,

          	IF(tkamaroperasi_ruangan_verifikator.id,
          		(CASE WHEN tkamaroperasi_ruangan_verifikator.jasasarana != mtarif_operasi_detail.jasasarana THEN 1 ELSE 0 END),
          		(CASE WHEN tkamaroperasi_ruangan.jasasarana != mtarif_operasi_detail.jasasarana THEN 1 ELSE 0 END)
          	) AS jasasarana_status,

          	IF(tkamaroperasi_ruangan_verifikator.id,
          		(CASE WHEN tkamaroperasi_ruangan_verifikator.jasapelayanan != mtarif_operasi_detail.jasapelayanan THEN 1 ELSE 0 END),
          		(CASE WHEN tkamaroperasi_ruangan.jasapelayanan != mtarif_operasi_detail.jasapelayanan THEN 1 ELSE 0 END)
          	) AS jasapelayanan_status,

          	IF(tkamaroperasi_ruangan_verifikator.id,
          		(CASE WHEN tkamaroperasi_ruangan_verifikator.bhp != mtarif_operasi_detail.bhp THEN 1 ELSE 0 END),
          		(CASE WHEN tkamaroperasi_ruangan.bhp != mtarif_operasi_detail.bhp THEN 1 ELSE 0 END)
          	) AS bhp_status,

          	IF(tkamaroperasi_ruangan_verifikator.id,
          		(CASE WHEN tkamaroperasi_ruangan_verifikator.biayaperawatan != mtarif_operasi_detail.biayaperawatan THEN 1 ELSE 0 END),
          		(CASE WHEN tkamaroperasi_ruangan.biayaperawatan != mtarif_operasi_detail.biayaperawatan THEN 1 ELSE 0 END)
          	) AS biayaperawatan_status

          FROM
          	tkamaroperasi_ruangan
          LEFT JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_ruangan.idpendaftaranoperasi
          LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran
          LEFT JOIN mtarif_operasi_detail ON mtarif_operasi_detail.idtarif = tkamaroperasi_ruangan.id_tarif AND (mtarif_operasi_detail.kelas = trawatinap_pendaftaran.idkelas OR mtarif_operasi_detail.kelas = 1)
          LEFT JOIN mtarif_operasi ON mtarif_operasi.id = mtarif_operasi_detail.idtarif
          LEFT JOIN musers ON musers.id = tkamaroperasi_ruangan.iduser_transaksi
          LEFT JOIN tkamaroperasi_ruangan_verifikator ON tkamaroperasi_ruangan_verifikator.id = tkamaroperasi_ruangan.id
          WHERE tkamaroperasi_ruangan.id = $iddetail");
		  return $query->row();

		  break;

		case 'tkamaroperasi_jasado':
		  $query = $this->db->query("SELECT
          	'mtarif_operasi' AS table_tarif,
          	tkamaroperasi_jasado.id,
          	tkamaroperasi_jasado.id_tarif AS idtarif,
          	mtarif_operasi.nama AS namatarif,
          	musers.id AS iduserinput,
          	musers.name AS namauserinput,
          	mdokter.id AS iddokter,
          	mdokter.nama AS namadokter,

          	mtarif_operasi_detail.jasasarana AS jasasarana_master,
          	mtarif_operasi_detail.jasapelayanan AS jasapelayanan_master,
          	mtarif_operasi_detail.bhp AS bhp_master,
          	mtarif_operasi_detail.biayaperawatan AS biayaperawatan_master,

          	IF(tkamaroperasi_jasado_verifikator.id, tkamaroperasi_jasado_verifikator.jasasarana, tkamaroperasi_jasado.jasasarana) AS jasasarana_tarif,
          	IF(tkamaroperasi_jasado_verifikator.id, tkamaroperasi_jasado_verifikator.jasapelayanan, tkamaroperasi_jasado.jasapelayanan) AS jasapelayanan_tarif,
          	IF(tkamaroperasi_jasado_verifikator.id, tkamaroperasi_jasado_verifikator.bhp, tkamaroperasi_jasado.bhp) AS bhp_tarif,
          	IF(tkamaroperasi_jasado_verifikator.id, tkamaroperasi_jasado_verifikator.biayaperawatan, tkamaroperasi_jasado.biayaperawatan) AS biayaperawatan_tarif,

          	IF(tkamaroperasi_jasado_verifikator.id, tkamaroperasi_jasado_verifikator.jasasarana_disc, tkamaroperasi_jasado.jasasarana_disc) AS jasasarana_disc,
          	IF(tkamaroperasi_jasado_verifikator.id, tkamaroperasi_jasado_verifikator.jasapelayanan_disc, tkamaroperasi_jasado.jasapelayanan_disc) AS jasapelayanan_disc,
          	IF(tkamaroperasi_jasado_verifikator.id, tkamaroperasi_jasado_verifikator.bhp_disc, tkamaroperasi_jasado.bhp_disc) AS bhp_disc,
          	IF(tkamaroperasi_jasado_verifikator.id, tkamaroperasi_jasado_verifikator.biayaperawatan_disc, tkamaroperasi_jasado.biayaperawatan_disc) AS biayaperawatan_disc,

          	IF(tkamaroperasi_jasado_verifikator.id, tkamaroperasi_jasado_verifikator.jasasarana_group, mtarif_operasi_detail.group_jasasarana) AS jasasarana_group,
          	IF(tkamaroperasi_jasado_verifikator.id, tkamaroperasi_jasado_verifikator.jasapelayanan_group, mtarif_operasi_detail.group_jasapelayanan) AS jasapelayanan_group,
          	IF(tkamaroperasi_jasado_verifikator.id, tkamaroperasi_jasado_verifikator.bhp_group, mtarif_operasi_detail.group_bhp) AS bhp_group,
          	IF(tkamaroperasi_jasado_verifikator.id, tkamaroperasi_jasado_verifikator.biayaperawatan_group, mtarif_operasi_detail.group_biayaperawatan) AS biayaperawatan_group,

          	tkamaroperasi_jasado_verifikator.jasasarana_keputusan AS jasasarana_keputusan,
          	tkamaroperasi_jasado_verifikator.jasapelayanan_keputusan AS jasapelayanan_keputusan,
          	tkamaroperasi_jasado_verifikator.bhp_keputusan AS bhp_keputusan,
          	tkamaroperasi_jasado_verifikator.biayaperawatan_keputusan AS biayaperawatan_keputusan,

          	IF(tkamaroperasi_jasado_verifikator.id,
          		(CASE WHEN tkamaroperasi_jasado_verifikator.jasasarana != mtarif_operasi_detail.jasasarana THEN 1 ELSE 0 END),
          		(CASE WHEN tkamaroperasi_jasado.jasasarana != mtarif_operasi_detail.jasasarana THEN 1 ELSE 0 END)
          	) AS jasasarana_status,

          	IF(tkamaroperasi_jasado_verifikator.id,
          		(CASE WHEN tkamaroperasi_jasado_verifikator.jasapelayanan != mtarif_operasi_detail.jasapelayanan THEN 1 ELSE 0 END),
          		(CASE WHEN tkamaroperasi_jasado.jasapelayanan != mtarif_operasi_detail.jasapelayanan THEN 1 ELSE 0 END)
          	) AS jasapelayanan_status,

          	IF(tkamaroperasi_jasado_verifikator.id,
          		(CASE WHEN tkamaroperasi_jasado_verifikator.bhp != mtarif_operasi_detail.bhp THEN 1 ELSE 0 END),
          		(CASE WHEN tkamaroperasi_jasado.bhp != mtarif_operasi_detail.bhp THEN 1 ELSE 0 END)
          	) AS bhp_status,

          	IF(tkamaroperasi_jasado_verifikator.id,
          		(CASE WHEN tkamaroperasi_jasado_verifikator.biayaperawatan != mtarif_operasi_detail.biayaperawatan THEN 1 ELSE 0 END),
          		(CASE WHEN tkamaroperasi_jasado.biayaperawatan != mtarif_operasi_detail.biayaperawatan THEN 1 ELSE 0 END)
          	) AS biayaperawatan_status

          FROM
          	tkamaroperasi_jasado
          LEFT JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_jasado.idpendaftaranoperasi
          LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran
          LEFT JOIN mtarif_operasi_detail ON mtarif_operasi_detail.idtarif = tkamaroperasi_jasado.id_tarif AND (mtarif_operasi_detail.kelas = tkamaroperasi_jasado.kelas_tarif OR mtarif_operasi_detail.kelas = 1)
          LEFT JOIN mtarif_operasi ON mtarif_operasi.id = mtarif_operasi_detail.idtarif
          LEFT JOIN mdokter ON mdokter.id = tkamaroperasi_jasado.iddokteroperator
          LEFT JOIN musers ON musers.id = tkamaroperasi_jasado.iduser_transaksi
          LEFT JOIN tkamaroperasi_jasado_verifikator ON tkamaroperasi_jasado_verifikator.id = tkamaroperasi_jasado.id
          WHERE tkamaroperasi_jasado.id = $iddetail");
		  return $query->row();

		  break;

		case 'tpoliklinik_administrasi':
		  $query = $this->db->query("SELECT
          	'mtarif_administrasi' AS table_tarif,
          	tpoliklinik_administrasi.id,
          	tpoliklinik_administrasi.idadministrasi AS idtarif,
          	mtarif_administrasi.nama AS namatarif,
          	musers.id AS iduserinput,
          	musers.name AS namauserinput,
          	0 AS iddokter,
          	'-' AS namadokter,

          	mtarif_administrasi.jasasarana AS jasasarana_master,
          	mtarif_administrasi.jasapelayanan AS jasapelayanan_master,
          	mtarif_administrasi.bhp AS bhp_master,
          	mtarif_administrasi.biayaperawatan AS biayaperawatan_master,

          	IF(tpoliklinik_administrasi_verifikator.id, tpoliklinik_administrasi_verifikator.jasasarana, tpoliklinik_administrasi.jasasarana) AS jasasarana_tarif,
          	IF(tpoliklinik_administrasi_verifikator.id, tpoliklinik_administrasi_verifikator.jasapelayanan, tpoliklinik_administrasi.jasapelayanan) AS jasapelayanan_tarif,
          	IF(tpoliklinik_administrasi_verifikator.id, tpoliklinik_administrasi_verifikator.bhp, tpoliklinik_administrasi.bhp) AS bhp_tarif,
          	IF(tpoliklinik_administrasi_verifikator.id, tpoliklinik_administrasi_verifikator.biayaperawatan, tpoliklinik_administrasi.biayaperawatan) AS biayaperawatan_tarif,

          	IF(tpoliklinik_administrasi_verifikator.id, tpoliklinik_administrasi_verifikator.jasasarana_disc, tpoliklinik_administrasi.jasasarana_disc) AS jasasarana_disc,
          	IF(tpoliklinik_administrasi_verifikator.id, tpoliklinik_administrasi_verifikator.jasapelayanan_disc, tpoliklinik_administrasi.jasapelayanan_disc) AS jasapelayanan_disc,
          	IF(tpoliklinik_administrasi_verifikator.id, tpoliklinik_administrasi_verifikator.bhp_disc, tpoliklinik_administrasi.bhp_disc) AS bhp_disc,
          	IF(tpoliklinik_administrasi_verifikator.id, tpoliklinik_administrasi_verifikator.biayaperawatan_disc, tpoliklinik_administrasi.biayaperawatan_disc) AS biayaperawatan_disc,

          	IF(tpoliklinik_administrasi_verifikator.id, tpoliklinik_administrasi_verifikator.jasasarana_group, mtarif_administrasi.group_jasasarana) AS jasasarana_group,
          	IF(tpoliklinik_administrasi_verifikator.id, tpoliklinik_administrasi_verifikator.jasapelayanan_group, mtarif_administrasi.group_jasapelayanan) AS jasapelayanan_group,
          	IF(tpoliklinik_administrasi_verifikator.id, tpoliklinik_administrasi_verifikator.bhp_group, mtarif_administrasi.group_bhp) AS bhp_group,
          	IF(tpoliklinik_administrasi_verifikator.id, tpoliklinik_administrasi_verifikator.biayaperawatan_group, mtarif_administrasi.group_biayaperawatan) AS biayaperawatan_group,

          	tpoliklinik_administrasi_verifikator.jasasarana_keputusan AS jasasarana_keputusan,
          	tpoliklinik_administrasi_verifikator.jasapelayanan_keputusan AS jasapelayanan_keputusan,
          	tpoliklinik_administrasi_verifikator.bhp_keputusan AS bhp_keputusan,
          	tpoliklinik_administrasi_verifikator.biayaperawatan_keputusan AS biayaperawatan_keputusan,

          	IF(tpoliklinik_administrasi_verifikator.id,
          		(CASE WHEN tpoliklinik_administrasi_verifikator.jasasarana != mtarif_administrasi.jasasarana THEN 1 ELSE 0 END),
          		(CASE WHEN tpoliklinik_administrasi.jasasarana != mtarif_administrasi.jasasarana THEN 1 ELSE 0 END)
          	) AS jasasarana_status,

          	IF(tpoliklinik_administrasi_verifikator.id,
          		(CASE WHEN tpoliklinik_administrasi_verifikator.jasapelayanan != mtarif_administrasi.jasapelayanan THEN 1 ELSE 0 END),
          		(CASE WHEN tpoliklinik_administrasi.jasapelayanan != mtarif_administrasi.jasapelayanan THEN 1 ELSE 0 END)
          	) AS jasapelayanan_status,

          	IF(tpoliklinik_administrasi_verifikator.id,
          		(CASE WHEN tpoliklinik_administrasi_verifikator.bhp != mtarif_administrasi.bhp THEN 1 ELSE 0 END),
          		(CASE WHEN tpoliklinik_administrasi.bhp != mtarif_administrasi.bhp THEN 1 ELSE 0 END)
          	) AS bhp_status,

          	IF(tpoliklinik_administrasi_verifikator.id,
          		(CASE WHEN tpoliklinik_administrasi_verifikator.biayaperawatan != mtarif_administrasi.biayaperawatan THEN 1 ELSE 0 END),
          		(CASE WHEN tpoliklinik_administrasi.biayaperawatan != mtarif_administrasi.biayaperawatan THEN 1 ELSE 0 END)
          	) AS biayaperawatan_status

          FROM
          	tpoliklinik_administrasi
          LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_administrasi.idpendaftaran
          LEFT JOIN mtarif_administrasi ON mtarif_administrasi.id = tpoliklinik_administrasi.idadministrasi
          LEFT JOIN musers ON musers.id = tpoliklinik_pendaftaran.created_by
          LEFT JOIN tpoliklinik_administrasi_verifikator ON tpoliklinik_administrasi_verifikator.id = tpoliklinik_administrasi.id
          WHERE tpoliklinik_administrasi.id = $iddetail");
		  return $query->row();

		  break;

		default:
		  // code...
		  break;
	  }
	}

	public function getMasterTarifTindakan($table, $idtarif)
	{
		switch ($table) {
		case 'trawatinap_ruangan':
		  $query = $this->db->query('
          SELECT * FROM mtarif_ruangperawatan WHERE status = 1
          ');
		  return $query->result();

		  break;

		case 'trawatinap_tindakan':
		  $query = $this->db->query('
          SELECT * FROM mtarif_rawatinap WHERE status = 1
          ');
		  return $query->result();

		  break;

		case 'trawatinap_visite':
		  $query = $this->db->query('
          SELECT * FROM mtarif_visitedokter WHERE status = 1
          ');
		  return $query->result();

		  break;

		case 'tpoliklinik_pelayanan':
		  $query = $this->db->query('
          SELECT * FROM mtarif_rawatjalan WHERE status = 1
          ');
		  return $query->result();

		  break;

		case 'trujukan_laboratorium_detail':
		  $query = $this->db->query('
          SELECT * FROM mtarif_laboratorium WHERE status = 1
          ');
		  return $query->result();

		  break;

		case 'trujukan_radiologi_detail':
		  $query = $this->db->query('
          SELECT * FROM mtarif_radiologi WHERE status = 1
          ');
		  return $query->result();

		  break;

		case 'trujukan_fisioterapi_detail':
		  $query = $this->db->query('
          SELECT * FROM mtarif_fisioterapi WHERE status = 1
          ');
		  return $query->result();

		  break;

		case 'tkamaroperasi_sewaalat':
		  $query = $this->db->query('
          SELECT * FROM mtarif_operasi_sewaalat WHERE status = 1
          ');
		  return $query->result();

		  break;

		case 'tkamaroperasi_ruangan':
		  $query = $this->db->query('
          SELECT * FROM mtarif_operasi WHERE status = 1
          ');
		  return $query->result();

		  break;

		case 'tkamaroperasi_jasado':
		  $query = $this->db->query('
          SELECT * FROM mtarif_operasi WHERE status = 1
          ');
		  return $query->result();

		  break;

		case 'tpoliklinik_administrasi':
		  $query = $this->db->query('
          SELECT * FROM mtarif_administrasi WHERE status = 1
          ');
		  return $query->result();

		  break;

		default:
		  // code...
		  break;
	  }
	}

	public function noticeTindakan($table, $iddetail, $idtarif, $iddokter, $data)
	{
		$this->id = $iddetail;
		$this->idtarif = $idtarif;
		$this->iddokter = $iddokter;
		$this->jasasarana = RemoveComma($data[0][2]);
		$this->jasasarana_disc = RemoveComma($data[0][4]);
		$this->jasasarana_group = $data[0][7];
		$this->jasasarana_keputusan = $data[0][8];
		$this->jasapelayanan = RemoveComma($data[1][2]);
		$this->jasapelayanan_disc = RemoveComma($data[1][4]);
		$this->jasapelayanan_group = $data[1][7];
		$this->jasapelayanan_keputusan = $data[1][8];
		$this->bhp = RemoveComma($data[2][2]);
		$this->bhp_disc = RemoveComma($data[2][4]);
		$this->bhp_group = $data[2][7];
		$this->bhp_keputusan = $data[2][8];
		$this->biayaperawatan = RemoveComma($data[3][2]);
		$this->biayaperawatan_disc = RemoveComma($data[3][4]);
		$this->biayaperawatan_group = $data[3][7];
		$this->biayaperawatan_keputusan = $data[3][8];
		$this->total = ($this->jasasarana - $this->jasasarana_disc) + ($this->jasapelayanan - $this->jasapelayanan_disc) + ($this->bhp - $this->bhp_disc) + ($this->biayaperawatan - $this->biayaperawatan_disc);

		if ($this->db->replace($table . '_verifikator', $this, ['id' => $iddetail])) {
			return true;
		} else {
			return false;
		}
	}

	public function noticeAllTindakan($table, $iddetail, $idtarif, $idkeputusan, $data)
	{
		$this->id = $iddetail;
		$this->idtarif = $idtarif;
		$this->iddokter = $iddokter;
		$this->jasasarana = RemoveComma($data[0][2]);
		$this->jasasarana_disc = 0;
		$this->jasasarana_group = '';
		$this->jasasarana_keputusan = $idkeputusan;
		$this->jasapelayanan = RemoveComma($data[1][2]);
		$this->jasapelayanan_disc = 0;
		$this->jasapelayanan_group = '';
		$this->jasapelayanan_keputusan = $idkeputusan;
		$this->bhp = RemoveComma($data[2][2]);
		$this->bhp_disc = 0;
		$this->bhp_group = '';
		$this->bhp_keputusan = $idkeputusan;
		$this->biayaperawatan = RemoveComma($data[3][2]);
		$this->biayaperawatan_disc = 0;
		$this->biayaperawatan_group = '';
		$this->biayaperawatan_keputusan = $idkeputusan;
		$this->total = ($this->jasasarana - $this->jasasarana_disc) + ($this->jasapelayanan - $this->jasapelayanan_disc) + ($this->bhp - $this->bhp_disc) + ($this->biayaperawatan - $this->biayaperawatan_disc);

		if ($this->db->replace($table . '_verifikator', $this, ['id' => $iddetail])) {
			return true;
		} else {
			return false;
		}
	}

	public function getListUploadedDocument($reference, $idkasir)
	{
		$this->db->where('idkasir', $idkasir);
		$query = $this->db->get($reference . '_dokumen');
		return $query->result();
	}
}
