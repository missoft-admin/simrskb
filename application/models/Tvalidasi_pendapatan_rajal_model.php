<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tvalidasi_pendapatan_rajal_model extends CI_Model
{
   public function get_edit_detail_obat($idhead){
	   $q="SELECT H.tanggal_pendaftaran as tanggaldaftar,H.notransaksi,H.pendaftaran_id,H.idtipe,TP.iddokter,MD.nama as dokter,MU.`name` as user_trx,H.kasir_id,H.idvalidasi
			,H.no_medrec as no_medrec
			,H.namapasien as namapasien
			,TP.idpoliklinik,TP.idkelompokpasien,TP.idrekanan
			,poli.nama as namapoliklinik,Kel.nama as namakelompok,R.nama as rekanan
			,TF.statusresep,TF.statuspasien,TF.idkategori
			FROM tvalidasi_pendapatan_rajal_detail H
			LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.pendaftaran_id AND H.idtipe !='3'
			LEFT JOIN tpasien_penjualan TF ON TF.id=H.pendaftaran_id
			LEFT JOIN mdokter MD ON MD.id=TP.iddokter
			LEFT JOIN musers MU ON MU.id=H.userid_trx
			LEFT JOIN mpoliklinik poli ON poli.id=TP.idpoliklinik
			LEFT JOIN mpasien_kelompok Kel ON Kel.id=TP.idkelompokpasien
			LEFT JOIN mrekanan R ON R.id=TP.idrekanan AND TP.idkelompokpasien='1'
			WHERE H.id='$idhead'";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function get_edit_detail($idhead){
	   $q="SELECT H.pendaftaran_id,H.idtipe,TP.iddokter,MD.nama as dokter,MU.`name` as user_trx,H.kasir_id,H.idvalidasi
			FROM tvalidasi_pendapatan_rajal_detail H
			LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.pendaftaran_id
			LEFT JOIN mdokter MD ON MD.id=TP.iddokter
			LEFT JOIN musers MU ON MU.id=H.userid_trx
			WHERE H.id='$idhead'";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function get_edit_detail2($iddet){
	   $q="SELECT * FROM tvalidasi_pendapatan_rajal_detail_tindakan H
			WHERE H.id='$iddet'";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function get_head_racikan($idhead,$iddet){
	   $q="SELECT H.id as idracikan,H.namabarang,H.tuslah,H.idgroup_tuslah,H.totalkeseluruhan 
	   FROM tvalidasi_pendapatan_rajal_detail_farmasi_racikan H
			WHERE H.idhead='$idhead' AND H.iddetail='$iddet'";
		$query=$this->db->query($q);
		return $query->row();
   }
   public function get_head_refund($idhead){
	   $q="SELECT U.`name` as nama_user, H.* FROM `tvalidasi_pendapatan_rajal_detail` H 
LEFT JOIN musers U ON U.id=H.userid_trx
WHERE H.id='$idhead'";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   
   public function get_head($idhead){
	   $q="SELECT H.idgroup_diskon,H.idgroup_gabungan,H.st_gabungan,H.nominal_gabungan as nominal_gabungan,H.nominal_diskon,H.userid_trx,U.`name` as nama_user 
			,H.pendaftaran_id,H.kasir_id,H.idvalidasi,H.idgroup_round,H.nominal_round
			FROM tvalidasi_pendapatan_rajal_detail H
			LEFT JOIN musers U ON U.id=H.userid_trx
			WHERE H.id='$idhead'";
		$query=$this->db->query($q);
		return $query->row();
   }
    public function list_pembayaran($idhead)
    {
        $q="SELECT H.id,H.idvalidasi,H.idhead,H.idbayar_id,H.metode_nama as jenis,H.idmetode, CONCAT(H.metode_nama,'',IFNULL(H.bank,'')) metode_nama,H.nominal_bayar as nominal 
			,'tvalidasi_pendapatan_rajal_bayar' as reff,'1' as ref_id,H.idhead
			FROM tvalidasi_pendapatan_rajal_bayar H
			WHERE H.idhead='$idhead'
			UNION ALL
			SELECT H.id,H.idvalidasi,H.idhead,H.idbayar_id,'Non Tunai ' as jenis,H.idmetode,H.metode_nama,H.nominal_bayar as nominal 
			,'tvalidasi_pendapatan_rajal_bayar_non_tunai' as reff,2 as ref_id,H.idhead
			FROM tvalidasi_pendapatan_rajal_bayar_non_tunai H 
			WHERE H.idhead='$idhead'";
			// print_r($q);exit();
        $query = $this->db->query($q);
        return $query->result();
    }
	public function list_tunai($idhead)
    {
        $q="SELECT H.id,H.idvalidasi,H.idhead,H.idbayar_id,H.metode_nama as jenis,H.idmetode, CONCAT(H.metode_nama,' ',IFNULL(H.bank,'')) metode_nama,H.nominal_bayar as nominal 
			,'tvalidasi_pendapatan_rajal_bayar' as reff,'1' as ref_id,H.idhead,H.idakun
			FROM tvalidasi_pendapatan_rajal_bayar H
			WHERE H.idhead='$idhead'
			";
			// print_r($q);exit();
        $query = $this->db->query($q);
        return $query->result();
    }
	public function list_non_tunai($idhead)
    {
        $q="
			SELECT H.id,H.idvalidasi,H.idhead,H.idbayar_id,'Non Tunai ' as jenis,H.idmetode,H.metode_nama,H.nominal_bayar as nominal 
			,'tvalidasi_pendapatan_rajal_bayar_non_tunai' as reff,2 as ref_id,H.idhead,H.group_akun
			FROM tvalidasi_pendapatan_rajal_bayar_non_tunai H 
			WHERE H.idhead='$idhead'";
			// print_r($q);exit();
        $query = $this->db->query($q);
        return $query->result();
    }
   public function list_group(){
	   $q="SELECT H.id,H.nama,A.noakun,A.namaakun FROM mgroup_pembayaran H
			LEFT JOIN mgroup_pembayaran_detail D ON D.idgroup=H.id
			LEFT JOIN makun_nomor A ON A.id=D.idakun";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_akun(){
	   $q="SELECT * FROM makun_nomor";
		$query=$this->db->query($q);
		return $query->result();
   }
    public function jml_sisa_verifikasi($tanggal){
	   $q="SELECT COUNT(H.id) as jml FROM tkasir H
			WHERE H.`status`='2' AND DATE(H.tanggal)='$tanggal' AND H.status_verifikasi='0'";
		$query=$this->db->query($q);
		return $query->row('jml');
   }
   
   public function list_group_null($id,$idhead=''){
	   $where='';
	   if ($idhead!=''){
		   $where=" AND T.idhead='$idhead'";
	   }
	   $q="SELECT T.idvalidasi,T.idhead,T.idakun,A.noakun,A.namaakun
				,(CASE WHEN T.posisi_akun='D' THEN IFNULL(T.nominal,0) ELSE 0 END) as debet 
				,(CASE WHEN T.posisi_akun='K' THEN IFNULL(T.nominal,0) ELSE 0 END) as kredit
				,T.ket as keterangan
				FROM (
						SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,H.nominal_diskon as nominal,H.id as idhead,CONCAT('DISKON RAWAT JALAN') as ket
						FROM tvalidasi_pendapatan_rajal_detail H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_gabungan as idakun,H.posisi_gabungan as posisi_akun,H.nominal_gabungan as nominal,H.id as idhead,CONCAT('GABUNGAN RAWAT JALAN') as ket
						FROM tvalidasi_pendapatan_rajal_detail H WHERE H.idvalidasi='$id' AND H.st_gabungan='1'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_jasasarana as idakun,'K' as posisi_akun,H.jasasarana*H.kuantitas as nominal,H.idhead,CONCAT('JASA SARANA ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_jasapelayanan as idakun,'K' as posisi_akun,H.jasapelayanan*H.kuantitas as nominal,H.idhead,CONCAT('JASA PELAYANAN ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_bhp as idakun,'K' as posisi_akun,H.bhp*H.kuantitas as nominal,H.idhead,CONCAT('BHP ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_biayaperawatan as idakun,'K' as posisi_akun,H.biayaperawatan*H.kuantitas as nominal,H.idhead,CONCAT('BIAYA PERAWATAN ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_jasasarana_diskon as idakun,'D' as posisi_akun,H.jasasarana_diskon*H.kuantitas as nominal,H.idhead,CONCAT('DISKON JASA SARANA ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_jasapelayanan_diskon as idakun,'D' as posisi_akun,H.jasapelayanan_diskon*H.kuantitas as nominal,H.idhead,CONCAT('DISKON JASA PELAYANAN  ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_bhp_diskon as idakun,'D' as posisi_akun,H.bhp_diskon*H.kuantitas as nominal,H.idhead,CONCAT('DISKON BHP ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_biayaperawatan_diskon as idakun,'D' as posisi_akun,H.biayaperawatan_diskon*H.kuantitas as nominal,H.idhead,CONCAT('DISKON BIAYA PERAWATAN ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_diskon_all as idakun,'D' as posisi_akun,H.diskon as nominal,H.idhead,CONCAT('DISKON SUB TINDAKAN ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H WHERE H.idvalidasi='$id' 


						UNION ALL
						SELECT H.idvalidasi,H.idakun_penjualan as idakun,H.posisi_penjualan as posisi_akun,H.hargajual * H.kuantitas as nominal,H.idhead,CONCAT('PENJUALAN ',CASE WHEN H.idtipe='1' THEN 'ALKES' ELSE 'OBAT' END) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan_obat H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,H.diskon as nominal,H.idhead,CONCAT('DISKON ',CASE WHEN H.idtipe='1' THEN 'ALKES' ELSE 'OBAT' END) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan_obat H WHERE H.idvalidasi='$id'

						UNION ALL

						SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,H.total_diskon as nominal,H.idhead,CONCAT('DISKON ',CASE WHEN H.idtipe='1' THEN 'ALKES' ELSE 'OBAT' END) as ket
						FROM tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_penjualan as idakun,H.posisi_penjualan as posisi_akun,H.total_jual as nominal,H.idhead,CONCAT('FARMASI ',CASE WHEN H.idtipe='1' THEN 'ALKES' ELSE 'OBAT' END) as ket
						FROM tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_tuslah as idakun,H.posisi_tuslah as posisi_akun,H.tuslah as nominal,H.idhead,CONCAT('TUSLAH ',CASE WHEN H.idtipe='1' THEN 'ALKES' ELSE 'OBAT' END) as ket
						FROM tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan H WHERE H.idvalidasi='$id'
						
						UNION ALL
						SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,H.total_diskon as nominal,H.idhead,CONCAT('DISKON RACIKAN ',CASE WHEN H.idtipe='1' THEN 'ALKES' ELSE 'OBAT' END) as ket
						FROM tvalidasi_pendapatan_rajal_detail_farmasi_racikan_detail H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_penjualan as idakun,H.posisi_penjualan as posisi_akun,H.total_jual as nominal,H.idhead,CONCAT('FARMASI RACIKAN ',CASE WHEN H.idtipe='1' THEN 'ALKES' ELSE 'OBAT' END) as ket
						FROM tvalidasi_pendapatan_rajal_detail_farmasi_racikan_detail H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_tuslah as idakun,H.posisi_tuslah as posisi_akun,H.tuslah as nominal,H.idhead,CONCAT('TUSLAH RACIKAN') as ket
						FROM tvalidasi_pendapatan_rajal_detail_farmasi_racikan H WHERE H.idvalidasi='$id'

						UNION ALL
						SELECT H.idvalidasi,H.idakun as idakun,H.posisi_akun as posisi_akun,H.nominal_bayar as nominal,H.idhead,CONCAT('PEMBAYARAN ',H.metode_nama) as ket
						FROM tvalidasi_pendapatan_rajal_bayar H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun as idakun,H.posisi_akun as posisi_akun,H.nominal_bayar as nominal,H.idhead,CONCAT('PEMBAYARAN NON TUNAI',H.metode_nama) as ket
						FROM tvalidasi_pendapatan_rajal_bayar_non_tunai H WHERE H.idvalidasi='$id'
				) T 
				LEFT JOIN makun_nomor A ON A.id=T.idakun
				WHERE T.nominal > 0 AND T.idakun IS NULL ".$where;
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_rekanan(){
	   $q="SELECT id,nama from mrekanan 
				WHERE mrekanan.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
    public function list_kp(){
	   $q="SELECT id,nama from mpasien_kelompok 
				WHERE mpasien_kelompok.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getHeader($id){
	   $q="SELECT tvalidasi_pendapatan_rajal.* from tvalidasi_pendapatan_rajal 
				WHERE tvalidasi_pendapatan_rajal.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function getHeaderDetail($id){
	   $q="SELECT tvalidasi_pendapatan_rajal_detail.* from tvalidasi_pendapatan_rajal_detail 
				WHERE tvalidasi_pendapatan_rajal_detail.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function list_obat($id,$tipe){
	   $q="SELECT *FROM tvalidasi_pendapatan_rajal_detail_tindakan_obat H
			WHERE H.idhead='$id' AND H.idtipe='$tipe'
				";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_obat_farmasi($id,$tipe){
	   $q="SELECT *FROM tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan H
			WHERE H.idhead='$id' AND H.idtipe='$tipe'
				";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_obat_farmasi_racikan($id,$idracikan){
	   $q="SELECT *FROM tvalidasi_pendapatan_rajal_detail_farmasi_racikan_detail H
			WHERE H.idhead='$id' AND H.idracikan='$idracikan'
				";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function saveData(){
	   $id=($this->input->post('id'));
		$iddet_bayar=($this->input->post('iddet_bayar'));
		$idakun=($this->input->post('idakun'));
		$iddet=($this->input->post('iddet'));
		$idakun_piutang=($this->input->post('idakun_piutang'));
		$idakun_income=($this->input->post('idakun_income'));
		$idakun_other_loss=($this->input->post('idakun_other_loss'));

		$st_posting=($this->input->post('st_posting'));
		$btn_simpan=($this->input->post('btn_simpan'));
		foreach($iddet as $index => $val){
			
			$detail=array(
				'idakun_piutang'=>$idakun_piutang[$index],
				'idakun_income'=>$idakun_income[$index],
				'idakun_other_loss'=>$idakun_other_loss[$index],
				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_rajal_detail',$detail);
		}
		foreach($iddet_bayar as $index => $val){
			
			$detail=array(
				'idakun'=>$idakun[$index],
				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_rajal_bayar',$detail);
		}
		if ($st_posting=='1'){
			$this->db->where('id',$id);
			$this->db->update('tvalidasi_pendapatan_rajal',array('st_posting'=>0));
		}
		if ($btn_simpan=='2'){
			$data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $id);
			$result=$this->db->update('tvalidasi_pendapatan_rajal', $data);
		}
		
		return true;
   }
   function get_nama_akun($id){
	   $q="SELECT *from makun_nomor A WHERE A.id='$id'";
	   return $this->db->query($q)->row();
   }
   function ViewRincianTindakan($idhead,$idjenis){
	   $q="SELECT D.*,H.notransaksi as nopendaftaran,H.tanggal_pendaftaran as tanggal
			FROM `tvalidasi_pendapatan_rajal_detail_tindakan` D
			LEFT JOIN tvalidasi_pendapatan_rajal_detail H ON H.id=D.idhead
			WHERE D.idhead='$idhead' AND D.idjenis='$idjenis'";
	   return $this->db->query($q)->result();
   }
   function ViewRincianObatIGD($idhead,$idtipe){
	   $q="SELECT D.*,H.notransaksi as nopendaftaran,H.tanggal_pendaftaran as tanggal
			FROM `tvalidasi_pendapatan_rajal_detail_tindakan_obat` D
			LEFT JOIN tvalidasi_pendapatan_rajal_detail H ON H.id=D.idhead
			WHERE D.idhead='$idhead' AND D.idtipe='$idtipe'";
	   return $this->db->query($q)->result();
   }
   function ViewRincianFarmasiNonRacikan($idhead,$idtipe){
	   $q="SELECT D.*,H.notransaksi as nopendaftaran,H.tanggal_pendaftaran as tanggal
			FROM `tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan` D
			LEFT JOIN tvalidasi_pendapatan_rajal_detail H ON H.id=D.idhead
			WHERE D.idhead='$idhead' AND D.idtipe='$idtipe'";
			// print_r($q);exit();
	   return $this->db->query($q)->result();
   }
   function ViewRincianFarmasiRacikan($idhead){
	   $q="SELECT D.*,H.notransaksi as nopendaftaran,H.tanggal_pendaftaran as tanggal
			FROM `tvalidasi_pendapatan_rajal_detail_farmasi_racikan` D
			LEFT JOIN tvalidasi_pendapatan_rajal_detail H ON H.id=D.idhead
			WHERE D.idhead='$idhead'";
	   return $this->db->query($q)->result();
   }
   function GetStatusPosting($idvalidasi){
	   $q="SELECT H.st_posting FROM `tvalidasi_pendapatan_rajal` H WHERE H.id='$idvalidasi'";
	   $st=$this->db->query($q)->row('st_posting');
	   if ($st=='1'){
		   return 'disabled';
	   }else{
		   return '';
		   
	   }
   }
   function query_ringkasan($idvalidasi){
	   
	   $q="(	
			SELECT H.idvalidasi,H.idakun_round as idakun,H.posisi_round as posisi_akun,H.nominal_round as nominal,H.id as idhead,CONCAT('ROUND UP KASIR RAWAT JALAN') as ket,H.idgroup_round as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail H WHERE H.idvalidasi='$idvalidasi'
			UNION ALL
			SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,H.nominal_diskon as nominal,H.id as idhead,CONCAT('DISKON RAWAT JALAN') as ket,H.idgroup_diskon as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail H WHERE H.idvalidasi='$idvalidasi'
			UNION ALL
			SELECT H.idvalidasi,H.idakun_gabungan as idakun,H.posisi_gabungan as posisi_akun,H.nominal_gabungan as nominal,H.id as idhead,CONCAT('GABUNGAN RAWAT JALAN') as ket,H.idgroup_gabungan as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail H WHERE H.idvalidasi='$idvalidasi' AND H.st_gabungan='1'

			UNION ALL
			SELECT H.idvalidasi,H.idakun_jasasarana as idakun,CASE WHEN D.idtipe='4' THEN 'D' ELSE 'K' END as posisi_akun,H.jasasarana*H.kuantitas as nominal,H.idhead,CONCAT('JASA SARANA ',H.namatarif) as ket,group_jasasarana as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
			LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
			WHERE H.idvalidasi='$idvalidasi' 

			UNION ALL
			SELECT H.idvalidasi,H.idakun_jasapelayanan as idakun,CASE WHEN D.idtipe='4' THEN 'D' ELSE 'K' END  as posisi_akun,H.jasapelayanan*H.kuantitas as nominal,H.idhead,CONCAT('JASA PELAYANAN ',H.namatarif) as ket,H.group_jasapelayanan as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
			LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
			WHERE H.idvalidasi='$idvalidasi'

			UNION ALL
			SELECT H.idvalidasi,H.idakun_bhp as idakun,CASE WHEN D.idtipe='4' THEN 'D' ELSE 'K' END as posisi_akun,H.bhp*H.kuantitas as nominal,H.idhead,CONCAT('BHP ',H.namatarif) as ket,H.group_bhp as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
			LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
			WHERE H.idvalidasi='$idvalidasi' 


			UNION ALL
			SELECT H.idvalidasi,H.idakun_biayaperawatan as idakun,CASE WHEN D.idtipe='4' THEN 'D' ELSE 'K' END  as posisi_akun,H.biayaperawatan*H.kuantitas as nominal,H.idhead,CONCAT('BIAYA PERAWATAN ',H.namatarif) as ket,H.group_biayaperawatan as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
			LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
			WHERE H.idvalidasi='$idvalidasi' 

			UNION ALL
			SELECT H.idvalidasi,H.idakun_jasasarana_diskon as idakun,CASE WHEN D.idtipe='4' THEN 'K' ELSE 'D' END as posisi_akun,H.jasasarana_diskon*H.kuantitas as nominal,H.idhead,CONCAT('DISKON JASA SARANA ',H.namatarif) as ket,H.group_jasasarana_diskon as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
			LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
			WHERE H.idvalidasi='$idvalidasi' 

			UNION ALL
			SELECT H.idvalidasi,H.idakun_jasapelayanan_diskon as idakun,CASE WHEN D.idtipe='4' THEN 'K' ELSE 'D' END as posisi_akun,H.jasapelayanan_diskon*H.kuantitas as nominal,H.idhead,CONCAT('DISKON JASA PELAYANAN  ',H.namatarif) as ket,H.group_jasapelayanan_diskon as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
			LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
			WHERE H.idvalidasi='$idvalidasi' 

			UNION ALL
			SELECT H.idvalidasi,H.idakun_bhp_diskon as idakun,CASE WHEN D.idtipe='4' THEN 'K' ELSE 'D' END as posisi_akun,H.bhp_diskon*H.kuantitas as nominal,H.idhead,CONCAT('DISKON BHP ',H.namatarif) as ket,H.group_bhp_diskon as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
			LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
			WHERE H.idvalidasi='$idvalidasi' 

			UNION ALL
			SELECT H.idvalidasi,H.idakun_biayaperawatan_diskon as idakun,CASE WHEN D.idtipe='4' THEN 'K' ELSE 'D' END as posisi_akun,H.biayaperawatan_diskon*H.kuantitas as nominal,H.idhead,CONCAT('DISKON BIAYA PERAWATAN ',H.namatarif) as ket,H.group_biayaperawatan_diskon as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
			LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
			WHERE H.idvalidasi='$idvalidasi'

			UNION ALL
			SELECT H.idvalidasi,H.idakun_diskon_all as idakun,CASE WHEN D.idtipe='4' THEN 'K' ELSE 'D' END as posisi_akun,H.diskon as nominal,H.idhead,CONCAT('DISKON LAYANAN ',H.namatarif) as ket,H.group_diskon_all as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
			LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
			WHERE H.idvalidasi='$idvalidasi' 

			UNION ALL
			SELECT H.idvalidasi,H.idakun_penjualan as idakun,H.posisi_penjualan as posisi_akun,H.hargajual * H.kuantitas as nominal,H.idhead,CONCAT('PENJUALAN ',CASE WHEN H.idtipe='$idvalidasi' THEN 'ALKES' ELSE 'OBAT' END) as ket,H.idgroup_penjualan as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_tindakan_obat H WHERE H.idvalidasi='$idvalidasi'
			UNION ALL
			SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,H.diskon as nominal,H.idhead,CONCAT('DISKON ',CASE WHEN H.idtipe='$idvalidasi' THEN 'ALKES' ELSE 'OBAT' END) as ket,H.idgroup_diskon as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_tindakan_obat H WHERE H.idvalidasi='$idvalidasi'

			UNION ALL

			SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,H.total_diskon as nominal,H.idhead,CONCAT('DISKON ',CASE WHEN H.idtipe='$idvalidasi' THEN 'ALKES' ELSE 'OBAT' END) as ket
			,H.idgroup_diskon as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan H WHERE H.idvalidasi='$idvalidasi'

			UNION ALL
			SELECT H.idvalidasi,H.idakun_penjualan as idakun,H.posisi_penjualan as posisi_akun,H.total_jual as nominal,H.idhead,CONCAT('FARMASI ',CASE WHEN H.idtipe='$idvalidasi' THEN 'ALKES' ELSE 'OBAT' END) as ket
			,H.idgroup_penjualan as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan H WHERE H.idvalidasi='$idvalidasi'

			UNION ALL
			SELECT H.idvalidasi,H.idakun_tuslah as idakun,H.posisi_tuslah as posisi_akun,H.tuslah as nominal,H.idhead,CONCAT('TUSLAH ',CASE WHEN H.idtipe='$idvalidasi' THEN 'ALKES' ELSE 'OBAT' END) as ket
			,H.idgroup_tuslah as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan H WHERE H.idvalidasi='$idvalidasi'						

			UNION ALL
			SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,H.total_diskon as nominal,H.idhead,CONCAT('DISKON RACIKAN ',CASE WHEN H.idtipe='$idvalidasi' THEN 'ALKES' ELSE 'OBAT' END) as ket
			,H.idgroup_diskon as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_farmasi_racikan_detail H WHERE H.idvalidasi='$idvalidasi'


			UNION ALL
			SELECT H.idvalidasi,H.idakun_penjualan as idakun,H.posisi_penjualan as posisi_akun,H.total_jual as nominal,H.idhead,CONCAT('FARMASI RACIKAN ',CASE WHEN H.idtipe='$idvalidasi' THEN 'ALKES' ELSE 'OBAT' END) as ket
			,H.idgroup_penjualan as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_farmasi_racikan_detail H WHERE H.idvalidasi='$idvalidasi'

			UNION ALL
			SELECT H.idvalidasi,H.idakun_tuslah as idakun,H.posisi_tuslah as posisi_akun,H.tuslah as nominal,H.idhead,CONCAT('TUSLAH RACIKAN') as ket
			,H.idgroup_tuslah as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_detail_farmasi_racikan H WHERE H.idvalidasi='$idvalidasi'

			UNION ALL
			SELECT H.idvalidasi,H.idakun as idakun,H.posisi_akun as posisi_akun,H.nominal_bayar as nominal,H.idhead,CONCAT('PEMBAYARAN ',H.metode_nama) as ket
			,null as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_bayar H WHERE H.idvalidasi='$idvalidasi'

			UNION ALL
			SELECT H.idvalidasi,H.idakun as idakun,H.posisi_akun as posisi_akun,H.nominal_bayar as nominal,H.idhead,CONCAT('PEMBAYARAN NON TUNAI',H.metode_nama) as ket
			,H.group_akun as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_bayar_non_tunai H WHERE H.idvalidasi='$idvalidasi'
			
			UNION ALL
			SELECT H.idvalidasi,H.idakun as idakun,H.posisi_akun as posisi_akun,H.nominal_bayar as nominal,H.idhead,CONCAT('PEMBAYARAN INFORMASI MEDIS ',H.metode_nama,' ',IFNULL(H.bank,'')) as ket,null as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_info_medis_bayar H WHERE H.idvalidasi='$idvalidasi'

			UNION ALL
			SELECT H.idvalidasi,H.idakun_diskon_all as idakun,H.posisi_diskon_all as posisi_akun,H.nominal_diskon_all as nominal,H.id as  idhead,CONCAT('INFORMASI MEDIS DISKON ALL - ',H.notransaksi) as ket,H.idgroup_diskon_all as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_info_medis H WHERE H.idvalidasi='$idvalidasi'
			UNION ALL
			SELECT H.idvalidasi,H.idakun_round as idakun,H.posisi_round as posisi_akun,H.nominal_round as nominal,H.id as  idhead,CONCAT('INFORMASI MEDIS ROUND - ',H.notransaksi) as ket,H.idgroup_round as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_info_medis H WHERE H.idvalidasi='$idvalidasi'

			UNION ALL
			SELECT H.idvalidasi,H.idakun_dokter as idakun,H.posisi_dokter as posisi_akun,H.nominal_dokter as nominal,H.idhead,CONCAT('TARIF DOKTER ',H.namapasien,' - ',IFNULL(H.no_medrec,'')) as ket,H.idgroup_dokter as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_info_medis_detail H WHERE H.idvalidasi='$idvalidasi'
			UNION ALL
			SELECT H.idvalidasi,H.idakun_rs as idakun,H.posisi_rs as posisi_akun,H.nominal_rs as nominal,H.idhead,CONCAT('TARIF RS ',H.namapasien,' - ',IFNULL(H.no_medrec,'')) as ket,H.idgroup_rs as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_info_medis_detail H WHERE H.idvalidasi='$idvalidasi'
			UNION ALL
			SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,H.diskon_rp as nominal,H.idhead,CONCAT('DISKON ',H.namapasien,' - ',IFNULL(H.no_medrec,'')) as ket,H.idgroup_diskon as idgroup_akun
			FROM tvalidasi_pendapatan_rajal_info_medis_detail H WHERE H.idvalidasi='$idvalidasi'


			)";
		return $q;
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
