<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Antrian_display_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function get_index_setting($id){
		$q="SELECT *
			 FROM antrian_display
			WHERE id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_sound($display_id){
		$q="SELECT *FROM antrian_display_sound H WHERE H.display_id='$display_id' AND H.status='1' ORDER BY H.nourut ASC";
		return $this->db->query($q)->result();
	}
	function list_counter(){
		$q="SELECT *FROM antrian_pelayanan_counter H WHERE H.status=1 ORDER BY H.nama_counter ASC";
		return $this->db->query($q)->result();
	}
	function save_general(){
		$id = $this->input->post('id');
		
		$this->nama_display = $this->input->post('nama_display');
		$this->bg_color = $this->input->post('bg_color');
		$this->judul_header = $this->input->post('judul_header');
		$this->judul_sub_header = $this->input->post('judul_sub_header');
		$this->alamat = $this->input->post('alamat');
		$this->telepone = $this->input->post('telepone');
		$this->email = $this->input->post('email');
		$this->website = $this->input->post('website');
		$this->upload_header_logo(true);
		// print_r($this);exit;	
		if ($id){
			$this->db->where('id', $id);
			if ($this->db->update('antrian_display', $this)) {
				return true;
			} else {
				return false;
			}
		}else{
			if ($this->db->insert('antrian_display', $this)) {
				return true;
			} else {
				return false;
			}
		}
		
			
	}
	 public function softDelete($id)
    {
        $this->status = 0;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('antrian_display', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('antrian_display', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function upload_header_logo($update = false)
    {
        if (!file_exists('assets/upload/antrian')) {
            mkdir('assets/upload/antrian', 0755, true);
        }

        if (isset($_FILES['header_logo'])) {
            if ($_FILES['header_logo']['name'] != '') {
                $config['upload_path'] = './assets/upload/antrian/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('header_logo')) {
                    $image_upload = $this->upload->data();
                    $this->header_logo = $image_upload['file_name'];

                    if ($update == true) {
                        $this->remove_image_header_logo($this->input->post('id'));
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
			// print_r('TIDAK ADA');exit;
                return true;
            }
					// print_r($this->foto);exit;
        } else {
			// print_r('TIDAK ADA');exit;
            return true;
        }
		
    }
	
	public function remove_image_header_logo($id)
    {
		$q="select header_logo From antrian_display H WHERE H.id='$id'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/antrian/'.$row->header_logo) && $row->header_logo !='') {
            unlink('./assets/upload/antrian/'.$row->header_logo);
        }
    }
	public function hapus_sound($id){
		$this->remove_sound($id);
		$this->status=0;
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		$this->db->where('id',$id);
		return $this->db->update('antrian_display_sound',$this);
		
	}
	public function upload_sound($update = false)
    {
        if (!file_exists('assets/upload/sound_antrian')) {
            mkdir('assets/upload/sound_antrian', 0755, true);
        }

        if (isset($_FILES['file_sound'])) {
            if ($_FILES['file_sound']['name'] != '') {
                $config['upload_path'] = './assets/upload/sound_antrian/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'wav|mp3';
				// $config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('file_sound')) {
                    $image_upload = $this->upload->data();
                    $this->file_sound = $image_upload['file_name'];

                    if ($update == true) {
                        $this->remove_sound($this->input->post('id'));
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
			// print_r('TIDAK ADA');exit;
                return true;
            }
					// print_r($this->foto);exit;
        } else {
			// print_r('TIDAK ADA');exit;
            return true;
        }
		
    }
	public function remove_sound($id)
    {
		$q="select file_sound From antrian_display_sound H WHERE H.id='$id'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/sound_antrian/'.$row->file_sound) && $row->file_sound !='') {
            unlink('./assets/upload/sound_antrian/'.$row->file_sound);
			
        }
    }
	public function save_sound()
    {
		$display_id=$_POST['display_id'];
        $this->display_id 	= $_POST['display_id'];		
        $this->nourut 	= $_POST['nourut'];		
		$this->upload_sound(false);
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('antrian_display_sound',$this)) {
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	function refresh_video($id){
		$q="SELECT  H.* from antrian_display_video H

			WHERE H.display_id='$id'
			ORDER BY H.id ASC
			";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/video/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">'.$r->user_upload.'-'.HumanDateLong($r->tanggal_upload).'</td>';
			$tabel .='<td class="text-left"><button class="btn btn-xs btn-danger hapus_file" title="Hapus"><li class="fa fa-trash-o"></li></button></td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
		}
		return $tabel;
	}
	function get_file_name($id){
		$q="select file_name from antrian_display_video where id='$id'
			";
		return $this->db->query($q)->row();
	}
}
