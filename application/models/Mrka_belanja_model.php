<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mrka_belanja_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
	// public function insert_validasi_rka($id){
		// $q="SELECT 
			// H.id as idtransaksi,CURRENT_DATE as tanggal_transaksi
			// ,H.no_pengajuan as notransaksi,H.tipe_rka,CASE WHEN H.tipe_rka='1' THEN 'RKA' ELSE 'NON RKA' END as tipe_nama
			// ,H.idunit,UD.nama as nama_unit
			// ,H.idunit_pengaju,UP.nama as nama_pengaju
			// ,H.idrka_kegiatan,H.nama_kegiatan,H.bulan
			// ,H.idjenis,MJ.nama	as nama_jenis,H.idklasifikasi,MK.nama as klasifikasi,MK.idakun
			// ,H.idvendor,MV.nama as nama_vendor,H.cara_pembayaran,H.jenis_pembayaran,H.grand_total as nominal
			// ,H.catatan,'D' as posisi_akun,S.st_auto_posting_pengajuan as st_auto_posting
			// FROM rka_pengajuan H
			// LEFT JOIN mklasifikasi MK ON MK.id=H.idklasifikasi
			// LEFT JOIN mvendor MV ON MV.id=H.idvendor
			// LEFT JOIN mjenis_pengajuan_rka MJ ON MJ.id=H.idjenis
			// LEFT JOIN munitpelayanan UD ON UD.id=H.idunit
			// LEFT JOIN munitpelayanan UP ON UP.id=H.idunit_pengaju
			// LEFT JOIN msetting_jurnal_kas_lain S ON S.id='1'
			// WHERE H.cara_pembayaran='1' AND H.jenis_pembayaran !='4' AND H.id='$id' ";
		// $row=$this->db->query($q)->row();
		// if ($row){
			
		
			// $data_header=array(
				// 'id_reff' => '6',
				// 'idtransaksi' => $row->idtransaksi,
				// 'tanggal_transaksi' => $row->tanggal_transaksi,
				// 'notransaksi' => $row->notransaksi,
				// 'keterangan' => $row->tipe_nama.' : '.$row->nama_kegiatan.' ('.$row->notransaksi.')',
				// 'nominal' => $row->nominal,
				// 'idakun' => $row->idakun,
				// 'posisi_akun' => $row->posisi_akun,
				// 'status' => 1,
				// 'st_auto_posting' => $row->st_auto_posting,
				// 'created_by'=>$this->session->userdata('user_id'),
				// 'created_nama'=>$this->session->userdata('user_name'),
				// 'created_date'=>date('Y-m-d H:i:s')
			// );
			// // print_r($data_header);exit();
			// $st_auto_posting=$row->st_auto_posting;
			// $this->db->insert('tvalidasi_kas',$data_header);
			// $idvalidasi=$this->db->insert_id();
			// $data_kasbon=array(
				// 'idvalidasi' =>$idvalidasi,
				// 'idtransaksi' =>$row->idtransaksi,
				// 'tanggal_transaksi' =>$row->tanggal_transaksi,
				// 'notransaksi' =>$row->notransaksi,
				// 'tipe_rka' =>$row->tipe_rka,
				// 'tipe_nama' =>$row->tipe_nama,
				// 'idunit' =>$row->idunit,
				// 'nama_unit' =>$row->nama_unit,
				// 'idunit_pengaju' =>$row->idunit_pengaju,
				// 'nama_pengaju' =>$row->nama_pengaju,
				// 'idrka_kegiatan' =>$row->idrka_kegiatan,
				// 'nama_kegiatan' =>$row->nama_kegiatan,
				// 'bulan' =>$row->bulan,
				// 'idjenis' =>$row->idjenis,
				// 'nama_jenis' =>$row->nama_jenis,
				// 'idklasifikasi' =>$row->idklasifikasi,
				// 'klasifikasi' =>$row->klasifikasi,
				// 'idvendor' =>$row->idvendor,
				// 'nama_vendor' =>$row->nama_vendor,
				// 'cara_pembayaran' =>$row->cara_pembayaran,
				// 'jenis_pembayaran' =>$row->jenis_pembayaran,
				// 'catatan' =>$row->catatan,
				// 'nominal' =>$row->nominal,
				// 'idakun' =>$row->idakun,
				// 'posisi_akun' =>$row->posisi_akun,
				// 'st_posting' =>0,
			// );
			// $this->db->insert('tvalidasi_kas_06_pengajuan',$data_kasbon);
			
			// $q="SELECT H.id as iddet
				// ,(H.harga_pokok*H.kuantitas) as nominal_beli,M.idakun as idakun_beli,'D' as posisi_beli
				// ,(H.harga_diskon*H.kuantitas) as nominal_diskon,M.idakun_diskon as idakun_diskon,'K' as posisi_diskon
				// ,(H.harga_ppn*H.kuantitas) as nominal_ppn,M.idakun_ppn as idakun_ppn,'D' as posisi_ppn

				// ,H.idpengajuan,H.idklasifikasi_det,H.nama_barang,H.merk_barang,H.kuantitas,H.satuan,H.harga_pokok,H.harga_diskon,H.harga_ppn,H.harga_satuan,H.total_harga,H.keterangan

				// FROM rka_pengajuan_detail H
				// LEFT JOIN mklasifikasi M ON M.id=H.idklasifikasi_det
				// WHERE H.idpengajuan='$id' AND H.`status`='1'
// ";
			// $rows=$this->db->query($q)->result();
			// foreach($rows as $r){
				// $data_detail=array(
					// 'idvalidasi' =>$idvalidasi,
					// 'iddet' => $r->iddet,
					// 'nominal_beli' => $r->nominal_beli,
					// 'nominal_ppn' => $r->nominal_ppn,
					// 'nominal_diskon' => $r->nominal_diskon,
					// 'idakun_beli' => $r->idakun_beli,
					// 'idakun_ppn' => $r->idakun_ppn,
					// 'idakun_diskon' => $r->idakun_diskon,
					// 'posisi_beli' => $r->posisi_beli,
					// 'posisi_ppn' => $r->posisi_ppn,
					// 'posisi_diskon' => $r->posisi_diskon,
					// 'idpengajuan' => $r->idpengajuan,
					// 'idklasifikasi_det' => $r->idklasifikasi_det,
					// 'nama_barang' => $r->nama_barang,
					// 'merk_barang' => $r->merk_barang,
					// 'kuantitas' => $r->kuantitas,
					// 'satuan' => $r->satuan,
					// 'harga_pokok' => $r->harga_pokok,
					// 'harga_diskon' => $r->harga_diskon,
					// 'harga_ppn' => $r->harga_ppn,
					// 'harga_satuan' => $r->harga_satuan,
					// 'total_harga' => $r->total_harga,
					// 'keterangan' => $r->keterangan,
				// );
				// $this->db->insert('tvalidasi_kas_06_pengajuan_detail',$data_detail);
			// }
			// $q="SELECT 
				// H.id as idbayar_id,H.jenis_kas_id,J.nama as jenis_kas_nama
				// ,H.sumber_kas_id,S.nama as sumber_nama
				// ,S.bank_id as bankid,mbank.nama as bank
				// ,H.idmetode,RM.metode_bayar as metode_nama,H.nominal_bayar, S.idakun,'K' as posisi_akun
				// FROM rka_pengajuan_pembayaran H
				// LEFT JOIN mjenis_kas J ON J.id=H.jenis_kas_id
				// LEFT JOIN msumber_kas S ON S.id=H.sumber_kas_id
				// LEFT JOIN ref_metode RM ON RM.id=H.idmetode
				// LEFT JOIN mbank ON mbank.id=S.bank_id
				// WHERE H.idrka='$id'";
			// $rows=$this->db->query($q)->result();
			// foreach($rows as $r){
				// $data_bayar=array(
					// 'idvalidasi' =>$idvalidasi,
					// 'idbayar_id' =>$r->idbayar_id,
					// 'jenis_kas_id' =>$r->jenis_kas_id,
					// 'jenis_kas_nama' =>$r->jenis_kas_nama,
					// 'sumber_kas_id' =>$r->sumber_kas_id,
					// 'sumber_nama' =>$r->sumber_nama,
					// 'bank' =>$r->bank,
					// 'bankid' =>$r->bankid,
					// 'idmetode' =>$r->idmetode,
					// 'metode_nama' =>$r->metode_nama,
					// 'nominal_bayar' =>$r->nominal_bayar,
					// 'idakun' =>$r->idakun,
					// 'posisi_akun' =>$r->posisi_akun,

				// );
				// $this->db->insert('tvalidasi_kas_06_pengajuan_bayar',$data_bayar);
			// }
			// if ($st_auto_posting=='1'){
				// $data_header=array(
					
					// 'st_posting' => 1,
					// 'posting_by'=>$this->session->userdata('user_id'),
					// 'posting_nama'=>$this->session->userdata('user_name'),
					// 'posting_date'=>date('Y-m-d H:i:s')
					

				// );
				// $this->db->where('id',$idvalidasi);
				// $this->db->update('tvalidasi_kas',$data_header);
			// }
		// }
		// return true;
		// // print_r('BERAHSIL');
	// }
	public function list_jenis_all(){
		$q="SELECT M.id,M.nama 
			FROM mlogic_detail L 
			LEFT JOIN mjenis_pengajuan_rka M ON M.id=L.idjenis
			WHERE L.`status`='1'
			GROUP BY L.idjenis";
		return $this->db->query($q)->result();
	}
    public function saveData() {
		$id=$this->input->post('id');
		// print_r($id);exit();
		$unit_lain=$this->input->post('unit_lain');
		$idunit_pengaju=$this->input->post('idunit_pengaju');
		$st_user_unit=$this->input->post('st_user_unit');
		$st_user_unit_lain=$this->input->post('st_user_unit_lain');
		$st_user_bendahara=$this->input->post('st_user_bendahara');
		$idlogic=$this->input->post('idlogic');
		$data=array(
			'memo_bendahara' => $this->input->post('memo_bendahara'),
			'unit_lain' => $this->input->post('unit_lain'),
			'st_user_unit' => $this->input->post('st_user_unit'),
			'st_user_unit_lain' => $this->input->post('st_user_unit_lain'),
			'st_user_bendahara' => $this->input->post('st_user_bendahara'),
		);
		if ($this->input->post('btn_simpan')=='2'){
			$data['status']='3';
		}
		$this->db->where('id',$this->input->post('id'));
		$this->db->update('rka_pengajuan',$data);
		
		
		if ($this->input->post('btn_simpan')=='2'){
			// print_r($id);exit();
			$this->db->where('idrka',$id);
			$this->db->delete('rka_pengajuan_bendahara');
			
			if ($st_user_unit=='1'){
				$q="INSERT INTO rka_pengajuan_bendahara (id,idrka,tipe,iduser,nama_user,status)  
					SELECT null,'$id' as idrka,'1' as tipe,S.userid as iduser, M.`name` as nama_user,'1' as status from munitpelayanan_user_setting S
					LEFT JOIN musers M ON S.userid=M.id
					WHERE S.idunit='$idunit_pengaju'";
				$this->db->query($q);
			}
			if ($st_user_unit_lain=='1'){
				$q="INSERT INTO rka_pengajuan_bendahara (id,idrka,tipe,iduser,nama_user,status)  
					SELECT null,'$id' as idrka,'2' as tipe,S.userid as iduser, M.`name` as nama_user,'1' as status from munitpelayanan_user_setting S
					LEFT JOIN musers M ON S.userid=M.id
					WHERE S.idunit='$unit_lain'";
				$this->db->query($q);
			}
			if ($st_user_bendahara=='1'){
				$q="INSERT INTO rka_pengajuan_bendahara (id,idrka,tipe,iduser,nama_user,status)  
				SELECT null,'$id' as idrka,'3' as tipe,S.iduser as userid, M.`name` as nama_user,'1' as status FROM mlogic_user_bendahara S
				LEFT JOIN musers M ON M.id=S.iduser
				WHERE S.idlogic='$idlogic'";
				// print_r($q);exit();
				$this->db->query($q);
			}
		}
		return true;
    }
    function list_unit_lain(){
		$q="SELECT L.idunit,M.nama FROM mlogic_unit L
			LEFT JOIN munitpelayanan M ON M.id=L.idunit";
		return $this->db->query($q)->result();
	}
	function list_unit_ganti(){
		$q="SELECT U.id as idunit,U.nama from munitpelayanan_user_setting S
			LEFT JOIN munitpelayanan U ON U.id=S.idunit
			GROUP BY S.idunit";
		return $this->db->query($q)->result();
	}
	public function list_unit(){
		$userid=$this->session->userdata('user_id');
		$q="SELECT S.idunit,M.nama from munitpelayanan_user_setting S
			LEFT JOIN munitpelayanan M ON M.id=S.idunit
			WHERE S.userid='$userid'";
		return $this->db->query($q)->result();
	}
	function refresh_image($id){
		$q="SELECT  H.* from rka_pengajuan_upload H

			WHERE H.idrka='$id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/bukti_penyerahan/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">'.$r->user_upload.'-'.HumanDateLong($r->tanggal_upload).'</td>';
			$tabel .='<td class="text-left"><button class="btn btn-xs btn-danger hapus_file" title="Hapus"><li class="fa fa-trash-o"></li></button></td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
		}
		return $tabel;
	}
	function refresh_pembayaran($id){
		$q="SELECT  H.* from rka_pengajuan_upload H
			WHERE H.idrka='$id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/bukti_penyerahan/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">'.$r->user_upload.'-'.HumanDateLong($r->tanggal_upload).'</td>';
			$tabel .='<td class="text-left"><button class="btn btn-xs btn-danger hapus_file" title="Hapus"><li class="fa fa-trash-o"></li></button></td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
		}
		return $tabel;
	}
	function get_file_name($id){
		$q="select file_name from rka_pengajuan_upload where id='$id'
			";
		return $this->db->query($q)->row();
	}
	function get_pembayaran($id){
		$q="select * from rka_pengajuan_pembayaran where id='$id'
			";
		return $this->db->query($q)->row_array();
	}
	function list_jenis_kas(){
		$q="SELECT H.id,H.nama FROM `mjenis_kas` H
			WHERE H.`status`='1'";
		return $this->db->query($q)->result();
	}
}
