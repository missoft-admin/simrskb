<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mkelompok_tindakan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mkelompok_tindakan');
        return $query->row();
    }
	public function lokasi_tubuh_list()
    {
        $this->db->where('status','1');
        $query = $this->db->get('mlokasi_tubuh');
        return $query->result();
    }
	public function kelompok_operasi_list()
    {
        $this->db->where('status','1');
        $query = $this->db->get('mkelompok_operasi');
        return $query->result();
    }
	public function js_icd(){
		$search=$this->input->post('search');
		$q="SELECT *from icd_9
			WHERE  (deskripsi LIKE '%".$search."%' OR kode LIKE '%".$search."%')";
		$query=$this->db->query($q);
		$result= $query->result();		
		return $result;			
	}
    public function saveData()
    {
        $this->nama 		= $_POST['nama'];
        $this->kelompok_operasi_id 		= $_POST['kelompok_operasi_id'];
        
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mkelompok_tindakan', $this)) {
            return  $this->db->insert_id();
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama 		= $_POST['nama'];
        $this->kelompok_operasi_id 		= $_POST['kelompok_operasi_id'];
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mkelompok_tindakan', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mkelompok_tindakan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function load_all()
    {
        $query = $this->db->get('mkelompok_tindakan');
        return $query->result();
    }
}
