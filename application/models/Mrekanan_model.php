<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mrekanan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mrekanan');
        return $query->row();
    }

    public function getJenisOperasi($id)
    {
        $this->db->select('idjenisoperasi');
        $this->db->where('idrekanan', $id);
        $query = $this->db->get('mrekanan_operasi');

        $result = $query->result();

        $data = "";
        $numItems = count($result);
        $i = 0;
        foreach ($query->result() as $row) {
            $data .= $row->idjenisoperasi;
            if (++$i != $numItems) {
                $data .= ',';
            }
        }

        return explode(',', $data);;
    }

    public function saveData()
    {
        $this->nama 					      = $_POST['nama'];
        $this->tipe_rekanan 					      = $_POST['tipe_rekanan'];
        $this->namalengkap 					= $_POST['namalengkap'];
        $this->alamat 				      = $_POST['alamat'];
        $this->telepon 				      = $_POST['telepon'];
        $this->fax 						      = $_POST['fax'];
        $this->email 					      = $_POST['email'];
        $this->jenispks 			      = $_POST['jenispks'];
        $this->prosedur 			      = $_POST['prosedur'];
        $this->penggunaanformulir 	= $_POST['penggunaanformulir'];
        $this->alokasi 			        = $_POST['alokasi'];

        // Tarif Pelayanan
        $this->tadm_rawatjalan		        = $_POST['tadm_rawatjalan'];
        $this->tkartu_rawatjalan		      = $_POST['tkartu_rawatjalan'];
        $this->tadm_rawatinap		          = $_POST['tadm_rawatinap'];
        $this->tkartu_rawatinap		        = $_POST['tkartu_rawatinap'];
        $this->toperasi_sewaalat					= $_POST['toperasi_sewaalat'];
        $this->toperasi_tindakan					= $_POST['toperasi_tindakan'];
        $this->tradiologi_xray						= $_POST['tradiologi_xray'];
        $this->tradiologi_usg							= $_POST['tradiologi_usg'];
        $this->tradiologi_ctscan					= $_POST['tradiologi_ctscan'];
        $this->tradiologi_mri							= $_POST['tradiologi_mri'];
        $this->tradiologi_bmd							= $_POST['tradiologi_bmd'];
        $this->tlaboratorium_umum					= $_POST['tlaboratorium_umum'];
        $this->tlaboratorium_pa						= $_POST['tlaboratorium_pa'];
        $this->tlaboratorium_pmi				  = $_POST['tlaboratorium_pmi'];
        $this->tfisioterapi				        = $_POST['tfisioterapi'];
        $this->trawatjalan						    = $_POST['trawatjalan'];
        $this->tigd						            = $_POST['tigd'];
        $this->trawatinap_fullcare1				= $_POST['trawatinap_fullcare1'];
        $this->trawatinap_ecg1						= $_POST['trawatinap_ecg1'];
        $this->trawatinap_visite1					= $_POST['trawatinap_visite1'];
        $this->trawatinap_sewaalat1				= $_POST['trawatinap_sewaalat1'];
        $this->trawatinap_ambulance1			= $_POST['trawatinap_ambulance1'];
        $this->trawatinap_fullcare2				= $_POST['trawatinap_fullcare2'];
        $this->trawatinap_ecg2						= $_POST['trawatinap_ecg2'];
        $this->trawatinap_visite2					= $_POST['trawatinap_visite2'];
        $this->trawatinap_sewaalat2				= $_POST['trawatinap_sewaalat2'];
        $this->trawatinap_ambulance2			= $_POST['trawatinap_ambulance2'];
        $this->trawatinap_fullcare3				= $_POST['trawatinap_fullcare3'];
        $this->trawatinap_ecg3						= $_POST['trawatinap_ecg3'];
        $this->trawatinap_visite3					= $_POST['trawatinap_visite3'];
        $this->trawatinap_sewaalat3				= $_POST['trawatinap_sewaalat3'];
        $this->trawatinap_ambulance3			= $_POST['trawatinap_ambulance3'];
        $this->trawatinap_fullcare4				= $_POST['trawatinap_fullcare4'];
        $this->trawatinap_ecg4						= $_POST['trawatinap_ecg4'];
        $this->trawatinap_visite4					= $_POST['trawatinap_visite4'];
        $this->trawatinap_sewaalat4				= $_POST['trawatinap_sewaalat4'];
        $this->trawatinap_ambulance4			= $_POST['trawatinap_ambulance4'];
        $this->truang_perawatan			      = $_POST['truang_perawatan'];
        $this->truang_hcu			            = $_POST['truang_hcu'];
        $this->truang_icu			            = $_POST['truang_icu'];
        $this->truang_isolasi			        = $_POST['truang_isolasi'];
        $this->masa_berlaku			        = ($_POST['masa_berlaku']?YMDFormat($_POST['masa_berlaku']):null);
        $this->besaran_diskon			        = $_POST['besaran_diskon'];
        $this->pic			        = $_POST['pic'];
    		$this->created_by  = $this->session->userdata('user_id');
    		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mrekanan', $this)) {

            $idrekanan = $this->db->insert_id();
            $this->db->where('idrekanan', $idrekanan);
            if ($this->db->delete('mrekanan_operasi')) {
              foreach ($_POST['toperasi_jenis'] as $idjenisoperasi) {
                $data = array();
                $data['idrekanan'] = $idrekanan;
                $data['idjenisoperasi'] = $idjenisoperasi;
                $this->db->insert('mrekanan_operasi', $data);
              }
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama 					      = $_POST['nama'];
        $this->tipe_rekanan 					= $_POST['tipe_rekanan'];
        $this->namalengkap 					= $_POST['namalengkap'];
        $this->alamat 				      = $_POST['alamat'];
        $this->telepon 				      = $_POST['telepon'];
        $this->fax 						      = $_POST['fax'];
        $this->email 					      = $_POST['email'];
        $this->jenispks 			      = $_POST['jenispks'];
        $this->prosedur 			      = $_POST['prosedur'];
        $this->penggunaanformulir 	= $_POST['penggunaanformulir'];
        $this->alokasi 			        = $_POST['alokasi'];

        // Tarif Pelayanan
        $this->tadm_rawatjalan		        = $_POST['tadm_rawatjalan'];
        $this->tkartu_rawatjalan		      = $_POST['tkartu_rawatjalan'];
        $this->tadm_rawatinap		          = $_POST['tadm_rawatinap'];
        $this->tkartu_rawatinap		        = $_POST['tkartu_rawatinap'];
        $this->toperasi_sewaalat					= $_POST['toperasi_sewaalat'];
        $this->toperasi_tindakan					= $_POST['toperasi_tindakan'];
        $this->tradiologi_xray						= $_POST['tradiologi_xray'];
        $this->tradiologi_usg							= $_POST['tradiologi_usg'];
        $this->tradiologi_ctscan					= $_POST['tradiologi_ctscan'];
        $this->tradiologi_mri							= $_POST['tradiologi_mri'];
        $this->tradiologi_bmd							= $_POST['tradiologi_bmd'];
        $this->tlaboratorium_umum					= $_POST['tlaboratorium_umum'];
        $this->tlaboratorium_pa						= $_POST['tlaboratorium_pa'];
        $this->tlaboratorium_pmi				  = $_POST['tlaboratorium_pmi'];
        $this->tfisioterapi				        = $_POST['tfisioterapi'];
        $this->trawatjalan						    = $_POST['trawatjalan'];
        $this->tigd						            = $_POST['tigd'];
        $this->trawatinap_fullcare1				= $_POST['trawatinap_fullcare1'];
        $this->trawatinap_ecg1						= $_POST['trawatinap_ecg1'];
        $this->trawatinap_visite1					= $_POST['trawatinap_visite1'];
        $this->trawatinap_sewaalat1				= $_POST['trawatinap_sewaalat1'];
        $this->trawatinap_ambulance1			= $_POST['trawatinap_ambulance1'];
        $this->trawatinap_fullcare2				= $_POST['trawatinap_fullcare2'];
        $this->trawatinap_ecg2						= $_POST['trawatinap_ecg2'];
        $this->trawatinap_visite2					= $_POST['trawatinap_visite2'];
        $this->trawatinap_sewaalat2				= $_POST['trawatinap_sewaalat2'];
        $this->trawatinap_ambulance2			= $_POST['trawatinap_ambulance2'];
        $this->trawatinap_fullcare3				= $_POST['trawatinap_fullcare3'];
        $this->trawatinap_ecg3						= $_POST['trawatinap_ecg3'];
        $this->trawatinap_visite3					= $_POST['trawatinap_visite3'];
        $this->trawatinap_sewaalat3				= $_POST['trawatinap_sewaalat3'];
        $this->trawatinap_ambulance3			= $_POST['trawatinap_ambulance3'];
        $this->trawatinap_fullcare4				= $_POST['trawatinap_fullcare4'];
        $this->trawatinap_ecg4						= $_POST['trawatinap_ecg4'];
        $this->trawatinap_visite4					= $_POST['trawatinap_visite4'];
        $this->trawatinap_sewaalat4				= $_POST['trawatinap_sewaalat4'];
        $this->trawatinap_ambulance4			= $_POST['trawatinap_ambulance4'];
        $this->truang_perawatan			      = $_POST['truang_perawatan'];
        $this->truang_hcu			            = $_POST['truang_hcu'];
        $this->truang_icu			            = $_POST['truang_icu'];
        $this->truang_isolasi			        = $_POST['truang_isolasi'];
		 $this->masa_berlaku			        = ($_POST['masa_berlaku']?YMDFormat($_POST['masa_berlaku']):null);
        $this->besaran_diskon			        = $_POST['besaran_diskon'];
        $this->pic			        = $_POST['pic'];
    		$this->edited_by  = $this->session->userdata('user_id');
    		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mrekanan', $this, array('id' => $_POST['id']))) {

            $idrekanan = $_POST['id'];
            $this->db->where('idrekanan', $idrekanan);
            if ($this->db->delete('mrekanan_operasi')) {
              foreach ($_POST['toperasi_jenis'] as $idjenisoperasi) {
                $data = array();
                $data['idrekanan'] = $idrekanan;
                $data['idjenisoperasi'] = $idjenisoperasi;
                $this->db->insert('mrekanan_operasi', $data);
              }
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;

    		$this->deleted_by  = $this->session->userdata('user_id');
    		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mrekanan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	function get_file_name($id){
		$q="select file_name from mrekanan_upload where id='$id'
			";
		return $this->db->query($q)->row();
	}
	function refresh_image($id){
		$q="SELECT  H.* from mrekanan_upload H

			WHERE H.idrekanan='$id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/mrekanan/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">'.$r->user_upload.'-'.HumanDateLong($r->tanggal_upload).'</td>';
			$tabel .='<td class="text-left"><button class="btn btn-xs btn-danger hapus_file" type="button" title="Hapus"><li class="fa fa-trash-o"></li></button></td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
		}
		return $tabel;
	}
}
