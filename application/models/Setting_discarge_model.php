<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_discarge_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_assesmen_setting(){
		$q="SELECT * FROM setting_discarge ";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_label(){
		$q="SELECT * FROM setting_discarge_keterangan";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_user(){
		$q="SELECT * FROM setting_discarge_user H";
		return $this->db->query($q)->row_array();
	}

	function save_assesmen(){
		// $id =1;
		$this->judul_header_ina = $this->input->post('judul_header_ina');
		$this->judul_header_eng = $this->input->post('judul_header_eng');
		$this->judul_footer_ina = $this->input->post('judul_footer_ina');
		$this->judul_footer_eng = $this->input->post('judul_footer_eng');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->lama_edit = $this->input->post('lama_edit');
		$this->orang_edit = $this->input->post('orang_edit');
		$this->st_hapus_catatan = $this->input->post('st_hapus_catatan');
		$this->lama_hapus = $this->input->post('lama_hapus');
		$this->orang_hapus = $this->input->post('orang_hapus');
		$this->st_duplikasi_catatan = $this->input->post('st_duplikasi_catatan');
		$this->lama_duplikasi = $this->input->post('lama_duplikasi');
		$this->orang_duplikasi = $this->input->post('orang_duplikasi');
		
			
		if ($this->db->update('setting_discarge', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_label(){
		$this->header_pengkajian = $this->input->post('header_pengkajian');
		$this->footer_pengkajian = $this->input->post('footer_pengkajian');
		
		if ($this->db->update('setting_discarge', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_paraf(){
		$id =1;
		$this->ket_gambar_paraf_penerima_ina = $this->input->post('ket_gambar_paraf_penerima_ina');
		$this->ket_gambar_paraf_penerima_eng = $this->input->post('ket_gambar_paraf_penerima_eng');
		$this->ket_nama_paraf_penerima_ina = $this->input->post('ket_nama_paraf_penerima_ina');
		$this->ket_nama_paraf_penerima_eng = $this->input->post('ket_nama_paraf_penerima_eng');
		$this->ket_gambar_paraf_menyatakan_ina = $this->input->post('ket_gambar_paraf_menyatakan_ina');
		$this->ket_gambar_paraf_menyatakan_eng = $this->input->post('ket_gambar_paraf_menyatakan_eng');
		$this->ket_nama_paraf_menyatakan_ina = $this->input->post('ket_nama_paraf_menyatakan_ina');
		$this->ket_nama_paraf_menyatakan_eng = $this->input->post('ket_nama_paraf_menyatakan_eng');
		$this->ket_gambar_paraf_saksi_ina = $this->input->post('ket_gambar_paraf_saksi_ina');
		$this->ket_gambar_paraf_saksi_eng = $this->input->post('ket_gambar_paraf_saksi_eng');
		$this->ket_nama_paraf_saksi_ina = $this->input->post('ket_nama_paraf_saksi_ina');
		$this->ket_nama_paraf_saksi_eng = $this->input->post('ket_nama_paraf_saksi_eng');
		$this->ket_gambar_paraf_saksi_rs_ina = $this->input->post('ket_gambar_paraf_saksi_rs_ina');
		$this->ket_gambar_paraf_saksi_rs_eng = $this->input->post('ket_gambar_paraf_saksi_rs_eng');
		$this->ket_nama_paraf_saksi_rs_ina = $this->input->post('ket_nama_paraf_saksi_rs_ina');
		$this->ket_nama_paraf_saksi_rs_eng = $this->input->post('ket_nama_paraf_saksi_rs_eng');

		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		// $this->db->where('id', $id);
			
		if ($this->db->update('setting_discarge_keterangan', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_defaultsave_default(){
		$id =1;
		$this->st_spesifik_discarge = $this->input->post('st_spesifik_discarge');
		if (!empty($this->input->post('st_kunci_default_discarge'))){
		$this->st_kunci_default_discarge =($this->input->post('st_spesifik_discarge')=='0'?'0':$this->input->post('st_kunci_default_discarge'));
			
		}else{
			$this->st_kunci_default_discarge =0;
		}
		
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('msetting_discarge_default', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


