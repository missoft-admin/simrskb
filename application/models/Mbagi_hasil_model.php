<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mbagi_hasil_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $q="SELECT 
			*
			from mbagi_hasil M
					
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	public function list_ps()
    {
        $q="SELECT 
			M.id,M.tipe_pemilik
			,CASE WHEN M.tipe_pemilik='1' THEN MP.nama WHEN M.tipe_pemilik='2' THEN MD.nama ELSE M.nama END as nama
			,CASE WHEN M.tipe_pemilik='1' THEN 'PEGAWAI' WHEN M.tipe_pemilik='2' THEN 'DOKTER' ELSE 'NON PEGAWAI' END as tipe_nama
			,M.npwp,M.keterangan,M.`status`

			from mpemilik_saham M

			LEFT JOIN mpegawai MP ON MP.id=M.idpeg_dok AND M.tipe_pemilik='1'
			LEFT JOIN mdokter MD ON MD.id=M.idpeg_dok AND M.tipe_pemilik='2'
			WHERE M.status='1'
			ORDER BY M.id ASC
			";
        $query = $this->db->query($q);
        return $query->result();
    }
	public function list_jenis_ko()
    {
        $q="SELECT M.id,M.nama from mjenis_operasi M
WHERE M.`status`='1'
ORDER BY M.id ASC
			";
        $query = $this->db->query($q);
        return $query->result();
    }
    public function saveData()
    {
        $this->nama 	= $_POST['nama'];		
        $this->bagian_rs = RemoveComma($_POST['bagian_rs']);
        $this->bagian_ps = RemoveComma($_POST['bagian_ps']);
        $this->tanggal_mulai = YMDFormat($_POST['tanggal_mulai']);
        $this->status_langsung = ($_POST['status_langsung']);
        $this->keterangan = ($_POST['keterangan']);
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		$this->status  = 1;
		$hasil=$this->db->insert('mbagi_hasil', $this);
        if ($hasil) {
			$id= $this->db->insert_id();
            return $id;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
		$this->nama 	= $_POST['nama'];		
		$this->status_langsung 	= $_POST['status_langsung'];		
        $this->bagian_rs = RemoveComma($_POST['bagian_rs']);
        $this->bagian_ps = RemoveComma($_POST['bagian_ps']);
        $this->tanggal_mulai = YMDFormat($_POST['tanggal_mulai']);
        $this->keterangan = ($_POST['keterangan']);
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		$this->status  = 1;

        if ($this->db->update('mbagi_hasil', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mbagi_hasil', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;		
		
        if ($this->db->update('mbagi_hasil', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	function refresh_image($id){
		$q="SELECT  H.* from mbagi_hasil_dokumen H

			WHERE H.idtransaksi='$id'";
		// print_r($q);exit();
		$row= $this->db->query($q)->result();
		$tabel='';
		$no=1;
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/mbagi_hasil/'.$r->filename.'" target="_blank">'.substr($r->filename, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->upload_by_nama.'-'.HumanDateLong($r->upload_date).'</td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/mbagi_hasil/'.$r->filename.'" target="_blank" data-toggle="tooltip" title="Preview" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                    <button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm " onclick="removeFile('.$r->id.')"><i class="fa fa-trash-o"></i></button></td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		return $tabel;
	}
	public function getLevel0($idtipe='0'){
		$q="select * from mtarif_laboratorium where status=1 AND idkelompok = 1 AND idtipe=$idtipe ORDER BY path ASC";
		$query=$this->db->query($q);
		return $query->result();
	}
	//
	public function generateTransaksiHonorDokterRawatJalan($dataPendaftaran)
    {
        $idpendaftaran = $dataPendaftaran['idpendaftaran'];
        $jenis_pasien = $dataPendaftaran['jenis_pasien'];
        $idkelompok = $dataPendaftaran['idkelompok'];
        $namakelompok = $dataPendaftaran['namakelompok'];
        $idrekanan = $dataPendaftaran['idrekanan'];
        $namarekanan = $dataPendaftaran['namarekanan'];
        $tanggal_pemeriksaan = $dataPendaftaran['tanggal_pemeriksaan'];
        $status_asuransi = $dataPendaftaran['status_asuransi'];
		$jenis_transaksi_id='1';
		
		// //ADMINISTRASI
        foreach ($this->Tpoliklinik_verifikasi_model->viewRincianAdministrasi($idpendaftaran) as $row) {
			
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'ADMINISTRASI RAWAT JALAN','tpoliklinik_administrasi',1);
		  
        }
        // JASA DOKTER RAWAT JALAN
		// print_r($idpendaftaran);exit();
		foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRajalTindakan($idpendaftaran) as $row) {
           // print_r($row);exit();
		    
		   $this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF RAWAT JALAN','tpoliklinik_pelayanan',2);
        }
		//JASA DOKTER LABORATORIUM
			 // print_r($idpendaftaran);exit();
        foreach ($this->Tpoliklinik_verifikasi_model->viewRincianLaboratorium($idpendaftaran) as $row) {
			// print_r($row);exit();
             $this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF LABORATORIUM RAWAT JALAN','trujukan_laboratorium_detail',6);
        }

        // JASA DOKTER RADIOLOGI
        foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRadiologi($idpendaftaran) as $index => $row) {
            // print_r($row);exit();
             $this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF RADIOLOGI RAWAT JALAN','trujukan_radiologi_detail',5);
        }

        // JASA DOKTER FISIOTERAPI
        foreach ($this->Tpoliklinik_verifikasi_model->viewRincianFisioterapi($idpendaftaran) as $row) {
            $this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF FISIOTERAPI RAWAT JALAN','trujukan_fisioterapi_detail',7);
        }
		print_r('BERHASIL INSERT');exit();

        return true;
    }
	function insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,$jenis_tindakan,$reference_table,$table_id){
			$idpendaftaran = $dataPendaftaran['idpendaftaran'];
			$jenis_pasien = $dataPendaftaran['jenis_pasien'];
			$idkelompok = $dataPendaftaran['idkelompok'];
			$namakelompok = $dataPendaftaran['namakelompok'];
			$idrekanan = $dataPendaftaran['idrekanan'];
			$namarekanan = $dataPendaftaran['namarekanan'];
			$tanggal_pemeriksaan = $dataPendaftaran['tanggal_pemeriksaan'];
			$status_asuransi = $dataPendaftaran['status_asuransi'];
			$jenis_transaksi_id=$jenis_transaksi_id;
			$jenis_transaksi=($jenis_transaksi_id=='1'?'poliklinik':'rawatinap');
			
			// print_r($row);exit();
		 //Insert tbagi_hasil
		   if ($this->Mbagi_hasil_model->cek_duplicate($idpendaftaran,$row->iddetail,$jenis_transaksi_id)==false){
			   // print_r($row->idtarif);exit();
			   $result_setting=$this->Mbagi_hasil_model->get_setting_bagi_hasil($row->tanggal,$row->idtarif,$table_id);
				if ($result_setting){
					$tbagi_hasil_id='';
					$status_langsung=$result_setting->status_langsung;
					if ($result_setting->tbagi_hasil_id == null){//Jika Belum Ada di tbagi_hasil
						$data_header=array(
							'mbagi_hasil_id'=>$result_setting->mbagi_hasil_id,
							'nama_bagi_hasil'=>$result_setting->nama_bagi_hasil,
							'deskripsi'=>$result_setting->deskripsi,
							'tanggal_tagihan'=>$result_setting->next_date,
							'bagian_rs_persen'=>$result_setting->bagian_rs,
							'bagian_ps_persen'=>$result_setting->bagian_ps,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id'),
							'status' => 1
						);
						$this->db->insert('tbagi_hasil',$data_header);
						$tbagi_hasil_id= $this->db->insert_id();
						
					}else{
						$tbagi_hasil_id=$result_setting->tbagi_hasil_id;				
					}
					if ($idkelompok=='5'){
						$status_langsung=1;				
					}
					$dataDetailBagiHasil = array(
					  'tbagi_hasil_id' => $tbagi_hasil_id,
					  'mbagi_hasil_pelayanan_id' => $result_setting->mbagi_hasil_pelayanan_id,
					  'idtransaksi' => $idpendaftaran,
					  'jenis_transaksi' => $jenis_transaksi,
					  'jenis_transaksi_id' =>$jenis_transaksi_id,
					  'jenis_tindakan' => $jenis_tindakan,
					  'reference_table' => $reference_table,
					  'tanggal' => YMDFormat($row->tanggal),
					  'jenis_pasien' => $jenis_pasien,
					  'idkelompok' => $idkelompok,
					  'namakelompok' => $namakelompok,
					  'idrekanan' => $idrekanan,
					  'namarekanan' => $namarekanan,
					  'iddetail' => $row->iddetail,
					  'idtarif' => $row->idtarif,
					  'namatarif' => $row->namatarif,
					  'kuantitas' => $row->kuantitas,
					  'status_asuransi' => $status_asuransi,
					  'jasasarana' => $result_setting->jasasarana,
					  'jasapelayanan' => $result_setting->jasapelayanan,
					  'bhp' => $result_setting->bhp,
					  'biayaperawatan' => $result_setting->biayaperawatan,
					  'biayaperawatan' => $result_setting->biayaperawatan,
					  'nominal_jasasarana' => $row->jasasarana - $row->jasasarana_disc,
					  'nominal_jasapelayanan' => $row->jasapelayanan - $row->jasapelayanan_disc,
					  'nominal_bhp' => $row->bhp - $row->bhp_disc,
					  'nominal_biayaperawatan' => $row->biayaperawatan - $row->biayaperawatan_disc,				  
					  'status' => $status_langsung,//Jika 1=Langsung;2:Hold Asuransi
					);
					$total_pendapatan=($dataDetailBagiHasil['jasasarana'] * $dataDetailBagiHasil['nominal_jasasarana']) + ($dataDetailBagiHasil['jasapelayanan'] * $dataDetailBagiHasil['nominal_jasapelayanan']) + ($dataDetailBagiHasil['bhp'] * $dataDetailBagiHasil['nominal_bhp']) + ($dataDetailBagiHasil['biayaperawatan'] * $dataDetailBagiHasil['nominal_biayaperawatan']);
					$dataDetailBagiHasil['total_pendapatan']=$total_pendapatan * $row->kuantitas;
					$this->db->insert('tbagi_hasil_detail', $dataDetailBagiHasil);
				}
		   }
		   
	}
	public function get_setting_bagi_hasil($tanggal_daftar,$idlayanan,$table_id='1'){
		$q="SELECT L.mbagi_hasil_id,L.id as mbagi_hasil_pelayanan_id,M.nama	 as nama_bagi_hasil,M.keterangan as deskripsi,L.jasasarana,L.jasapelayanan,L.bhp,L.biayaperawatan,T.id as tbagi_hasil_id 
			,cek_next_date_bagi_hasil(L.mbagi_hasil_id) as next_date,M.bagian_rs,M.bagian_ps,M.status_langsung
			FROM mbagi_hasil_layanan L
			LEFT JOIN mbagi_hasil M ON M.id=L.mbagi_hasil_id
			LEFT JOIN tbagi_hasil T ON T.mbagi_hasil_id=L.mbagi_hasil_id AND T.status_stop='0' AND T.`status` !='0'
			WHERE M.`status`='1' AND L.`status`='1' AND M.tanggal_mulai <= '".YMDFormat($tanggal_daftar)."' AND L.idlayanan='$idlayanan' AND L.table_id='$table_id'
			LIMIT 1";
			
			$result=$this->db->query($q)->row();
			// print_r($q);exit();
			return $result;
	}
	public function cek_duplicate($idtransaksi,$iddetail,$jenis_transaksi_id='1'){
		$q="SELECT T.id FROM `tbagi_hasil_detail` T
			WHERE T.idtransaksi='$idtransaksi' AND T.iddetail='$iddetail' AND T.`status` !='0' AND T.jenis_transaksi_id='$jenis_transaksi_id'
			LIMIT 1";

			$result=$this->db->query($q)->row();
			// print_r($result);exit();
			// if ($result){
				// return true;
			// }else{
				// return false;
			// }
			return false;
			// return $result;
	}
    public function generateTransaksiHonorDokterRawatInap($dataPendaftaran)
    {
        $tanggal_pemeriksaan = $dataPendaftaran['tanggal_pemeriksaan'];
        $idpendaftaran = $dataPendaftaran['idpendaftaran'];
        $idtipepasien = $dataPendaftaran['idtipepasien'];
        $idasalpasien = $dataPendaftaran['idasalpasien'];
        $idpoliklinik = $dataPendaftaran['idpoliklinik'];
        $jenis_pasien = $dataPendaftaran['jenis_pasien'];
        $idkelompok = $dataPendaftaran['idkelompok'];
        $namakelompok = $dataPendaftaran['namakelompok'];
        $idrekanan = $dataPendaftaran['idrekanan'];
        $namarekanan = $dataPendaftaran['namarekanan'];
        $idkategori_perujuk = $dataPendaftaran['idkategori_perujuk'];
        $namakategori_perujuk = $dataPendaftaran['namakategori_perujuk'];
        $iddokter_perujuk = $dataPendaftaran['iddokter_perujuk'];
        $namadokter_perujuk = $dataPendaftaran['namadokter_perujuk'];
        $potonganrs_perujuk = $dataPendaftaran['potonganrs_perujuk'];
        $pajak_perujuk = $dataPendaftaran['pajak_perujuk'];
        $status_asuransi = $dataPendaftaran['status_asuransi'];
        $status_kasir_rajal = $dataPendaftaran['status_kasir_rajal'];
		$jenis_transaksi_id='2';
        // $idDokterOperator = $this->getDokterOperator($idpendaftaran);
		
		// //ADMINISTRASI
        foreach ($this->Trawatinap_verifikasi_model->viewRincianAdministrasiRawatJalan($idpendaftaran) as $row) {
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'ADMINISTRASI RAWAT INAP','trawatinap_administrasi',1);
		  
        }
		// //RUANG KAMAR OPERASI
        foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiKamar($idpendaftaran) as $row) {
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'RUANG KAMAR OPERASI','tkamaroperasi_ruangan',9);
		  
        }
		// //SEWA ALAT KAMAR OPERASI
        foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiSewaAlat($idpendaftaran) as $row) {
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'SEWA ALAT KAMAR OPERASI','tkamaroperasi_sewaalat',8);
		  
        }
		// print_r($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 1));
        // JASA DOKTER RAWAT INAP - FULL CARE
        foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 1) as $row) {
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF TINDAKAN RAWAT INAP','trawatinap_tindakan',3);
			// print_r($row);exit();
        }

        // JASA DOKTER RAWAT INAP - ECG
        foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 2) as $row) {
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF TINDAKAN RAWAT INAP','trawatinap_tindakan',3);
        }

        // JASA DOKTER RAWAT INAP - AMBULANCE
        foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 5) as $row) {
          $this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF TINDAKAN RAWAT INAP','trawatinap_tindakan',3);
        }

        // JASA DOKTER RAWAT INAP - SEWA ALAT
        foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 4) as $row) {
          $this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF TINDAKAN RAWAT INAP','trawatinap_tindakan',3);
        }

        // JASA DOKTER RAWAT INAP - LAIN LAIN
        foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 6) as $row) {
          $this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF TINDAKAN RAWAT INAP','trawatinap_tindakan',3);
        }

        // JASA DOKTER LABORATORIUM - UMUM
        foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 1) as $row) {
			// print_r($row);exit();
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF LABORATORIUM RAWAT INAP','trujukan_laboratorium_detail',6);
        }

        // JASA DOKTER LABORATORIUM - PATHOLOGI ANATOMI
        foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 2) as $row) {
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF LABORATORIUM RAWAT INAP','trujukan_laboratorium_detail',6);
        }

        // JASA DOKTER LABORATORIUM - PMI
        foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 3) as $row) {
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF LABORATORIUM RAWAT INAP','trujukan_laboratorium_detail',6);
        }

        // JASA DOKTER RADIOLOGI - X-RAY
        foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 1) as $row) {
			 $this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF RADIOLOGI RAWAT INAP','trujukan_radiologi_detail',5);
        }

        // JASA DOKTER RADIOLOGI - USG
        foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 2) as $row) {
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF RADIOLOGI RAWAT INAP','trujukan_radiologi_detail',5);
        }

        // JASA DOKTER RADIOLOGI - CT SCAN
        foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 3) as $row) {
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF RADIOLOGI RAWAT INAP','trujukan_radiologi_detail',5);
        }

        // JASA DOKTER RADIOLOGI - MRI
        foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 4) as $row) {
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF RADIOLOGI RAWAT INAP','trujukan_radiologi_detail',5);
        }

        // JASA DOKTER RADIOLOGI - BMD
        foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 5) as $row) {
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF RADIOLOGI RAWAT INAP','trujukan_radiologi_detail',5);
        }

        // JASA DOKTER FISIOTERAPI
        foreach ($this->Trawatinap_verifikasi_model->viewRincianFisioterapi($idpendaftaran) as $row) {
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF FISIOTERAPI RAWAT INAP','trujukan_fisioterapi_detail',7);
        }

        // JASA DOKTER VISITE
        $FeeJasaDokterVisite = array();
        $dataVisite = $this->Trawatinap_verifikasi_model->viewRincianRanapVisite($idpendaftaran);
        foreach ($dataVisite as $index => $row) {
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran,$row,$jenis_transaksi_id,'TARIF VISITE RAWAT INAP','trawatinap_visite',4);
        }
        
		print_r('INSERT RANAP BERHASIL');exit();
        return true;
    }

}
