<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Term_radiologi_model extends CI_Model
{
    public function __construct()
	{
		parent::__construct();
		$this->load->model('Tpoliklinik_verifikasi_model');
		$this->load->model('Trawatinap_verifikasi_model');
		$this->load->model('Thonor_dokter_model');
	}
	
	function get_dianosa_utama($pendaftaran_id, $asal_rujukan){
		$query = "SELECT * FROM (
			SELECT 
				1 AS asal_rujukan, 
				tpoliklinik_asmed.diagnosa_utama
			FROM
				tpoliklinik_asmed
			WHERE
				tpoliklinik_asmed.status_assemen = '2' AND
				tpoliklinik_asmed.pendaftaran_id = '$pendaftaran_id'
			
			UNION ALL
			
			SELECT
				2 AS asal_rujukan,
				tpoliklinik_asmed_igd.diagnosa_utama
			FROM
				tpoliklinik_asmed_igd
			WHERE
				tpoliklinik_asmed_igd.status_assemen = '2' AND
				tpoliklinik_asmed_igd.pendaftaran_id='$pendaftaran_id'
		) result
		WHERE
			result.asal_rujukan = '$asal_rujukan'
		LIMIT 1";

		$data = $this->db->query($query)->row('diagnosa_utama');
		if ($data){
			return $data;
		}else{
			return '';
		}
	}

	public function check_setting_expertise_pemeriksaan($tipe_layanan, $pemeriksaan_id, $asal_pasien, $poliklinik_kelas, $dokter_radiologi) {
		$this->db->select('id, klinis, kesan, usul, hasil');
		$this->db->where('tipe_layanan', $tipe_layanan);
		$this->db->where("(pemeriksaan_id = '0' OR pemeriksaan_id = '$pemeriksaan_id')");
		$this->db->where("(asal_pasien = '0' OR asal_pasien = '$asal_pasien')");
		$this->db->where("(poliklinik_kelas = '0' OR poliklinik_kelas = '$poliklinik_kelas')");
		$this->db->where("(dokter_radiologi = '0' OR dokter_radiologi = '$dokter_radiologi')");
		$this->db->order_by('id', 'ASC');

		$query = $this->db->get('merm_pengaturan_penginputan_ekpertise_radiologi');
		return $query->row();
	}

	// [RUJUKAN RADIOLOGI]
    public function updateNominalJasaMedis($rujukanId, $statusExpertise, $kelasId)
	{
		$queryDokter = $this->db->query("SELECT
					mdokter.id AS iddokter,
					(CASE
						WHEN DATE_FORMAT(trujukan_radiologi.tanggal_input_pemeriksaan, '%H:%i') < '12:00' THEN
							mdokter.potonganrspagi
						ELSE mdokter.potonganrssiang
					END) AS potongan_rs,
					mdokter.pajak AS pajak_dokter
				FROM trujukan_radiologi
				JOIN mdokter ON mdokter.id = trujukan_radiologi.iddokterradiologi
				WHERE trujukan_radiologi.id = " . $rujukanId);
		$dataDokter = $queryDokter->row();

		$queryNominalJasaMedis = $this->db->query("SELECT
				trujukan_radiologi_detail.id,
				trujukan_radiologi_detail.idradiologi,
				trujukan_radiologi.statuspasien,
				(CASE
					WHEN trujukan_radiologi.statuspasien = 1 THEN
						mtarif_radiologi_detail.nominallangsung
					WHEN trujukan_radiologi.statuspasien = 2 THEN
						mtarif_radiologi_detail.nominalmenolak
					WHEN trujukan_radiologi.statuspasien = 3 THEN
						mtarif_radiologi_detail.nominalkembali
					END) AS nominal_jasamedis
			FROM
				trujukan_radiologi_detail
			JOIN trujukan_radiologi ON trujukan_radiologi.id = trujukan_radiologi_detail.idrujukan
			JOIN mtarif_radiologi ON mtarif_radiologi.id = trujukan_radiologi_detail.idradiologi
			JOIN mtarif_radiologi_detail ON mtarif_radiologi_detail.idtarif = mtarif_radiologi.id
			WHERE
				trujukan_radiologi.id = $rujukanId AND
				mtarif_radiologi_detail.kelas = $kelasId");

		$queryStatusKasir = $this->db->query("SELECT
				(CASE
					WHEN trujukan_radiologi.asalrujukan IN (1, 2) THEN
						tpoliklinik_tindakan.idpendaftaran
					ELSE
						trawatinap_tindakan_pembayaran.idtindakan
				END) AS idpendaftaran,
				(CASE
					WHEN trujukan_radiologi.asalrujukan IN (1, 2) THEN
						tkasir.id
					ELSE
						trawatinap_tindakan_pembayaran.id
				END) AS idkasir,
				(CASE
					WHEN trujukan_radiologi.asalrujukan IN (1, 2) THEN
						'poliklinik'
					ELSE
						'rawatinap'
				END) AS tipe_pendaftaran,
				(CASE
					WHEN trujukan_radiologi.asalrujukan IN (1, 2) THEN
						COALESCE(tkasir.status_verifikasi, 0)
					ELSE
						COALESCE(trawatinap_tindakan_pembayaran.status_verifikasi, 0)
				END) AS status_verifikasi
			FROM
				trujukan_radiologi
			LEFT JOIN trawatinap_tindakan_pembayaran ON trawatinap_tindakan_pembayaran.idtindakan = trujukan_radiologi.idtindakan AND trujukan_radiologi.asalrujukan = 3
			LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = trujukan_radiologi.idtindakan AND trujukan_radiologi.asalrujukan IN (1, 2)
			LEFT JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN (1, 2)
			WHERE trujukan_radiologi.id = $rujukanId");

		// Update Nominal Jasa Medis By Expertise
		foreach ($queryNominalJasaMedis->result() as $row) {
			$this->db->set('iddokter', $dataDokter->iddokter);
			$this->db->set('potongan_rs', $dataDokter->potongan_rs);
			$this->db->set('pajak_dokter', $dataDokter->pajak_dokter);

			$this->db->set('status_tindakan', $statusExpertise);
			$this->db->set('nominal_jasamedis', $row->nominal_jasamedis);
			$this->db->where('id', $row->id);

			if ($this->db->update('trujukan_radiologi_detail')) {
				// Reset Honor Dokter
				$this->db->where('reference_table', 'trujukan_radiologi_detail');
				$this->db->where('iddetail', $row->id);
				$this->db->delete('thonor_dokter_detail');
			}
		}

		// Check Status Kasir
		$rowStatus = $queryStatusKasir->row();
		if ($rowStatus) {
				$idPendaftaran = $rowStatus->idpendaftaran;
				$idKasir = $rowStatus->idkasir;
				$tipePendaftaran = $rowStatus->tipe_pendaftaran;
				$statusVerifikasiKasir = $rowStatus->status_verifikasi;
		
				if ($statusVerifikasiKasir) {
						// Create Honor Dokter
						$this->createHonorDokter($tipePendaftaran, $idPendaftaran, $idKasir);
				}
		}

		return true;
	}

	public function createHonorDokter($tipe_pendaftaran, $idpendaftaran, $idKasir)
	{
		if ($tipe_pendaftaran == 'poliklinik') {
			// Get Detail Info Poliklinik
			$this->db->select("tpoliklinik_pendaftaran.id AS idpendaftaran,
						tpoliklinik_tindakan.id AS idtindakan, tkasir.id AS idkasir,
						IF(tpoliklinik_pendaftaran.idtipepasien = 1, 'Pasien RS', 'Pasien Pribadi') AS jenispasien,
						tpoliklinik_pendaftaran.idkelompokpasien AS idkelompok,
						mpasien_kelompok.nama AS namakelompok,
						tpoliklinik_pendaftaran.idrekanan,
						mrekanan.nama AS namarekanan,
						tpoliklinik_pendaftaran.tanggaldaftar AS tanggal_pemeriksaan");
			$this->db->join('tpoliklinik_tindakan', 'tpoliklinik_tindakan.id = tkasir.idtindakan AND idtipe IN (1,2)');
			$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran');
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
			$this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
			$this->db->where('tkasir.id', $idKasir);
			$this->db->where('tkasir.status', 2);
			$this->db->limit(1);
			$query = $this->db->get('tkasir');
			$dataPoliklinik = $query->row();

			$tanggal_pemeriksaan = $dataPoliklinik->tanggal_pemeriksaan;
			$idpendaftaran = $dataPoliklinik->idpendaftaran;
			$jenis_pasien = $dataPoliklinik->jenispasien;
			$idkelompok = $dataPoliklinik->idkelompok;
			$namakelompok = $dataPoliklinik->namakelompok;
			$idrekanan = $dataPoliklinik->idrekanan;
			$namarekanan = $dataPoliklinik->namarekanan;
			$status_asuransi = ($dataPoliklinik->idkelompok != 5 ? 1 : 0);

			// JASA DOKTER RADIOLOGI [X-RAY] - RAWAT JALAN
			foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRadiologi($idpendaftaran) as $index => $row) {
				if ($row->idtipe == 1 && $row->jasamedis > 0) {
					$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
					$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
					$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

					$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
					$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

					$this->db->set('periode_pembayaran', $tanggal_pembayaran);
					$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
					$this->db->where('id', $row->iddetail);
					$this->db->update('trujukan_radiologi_detail');

					$isStopPeriode = $this->Thonor_dokter_model->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
					if ($isStopPeriode == null) {
						$idhonor = $this->Thonor_dokter_model->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

						if ($idhonor == null) {
							$dataHonorDokter = [
								'iddokter' => $row->iddokter,
								'namadokter' => $row->namadokter,
								'tanggal_pembayaran' => $tanggal_pembayaran,
								'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
								'nominal' => 0,
								'created_at' => date('Y-m-d H:i:s'),
								'created_by' => $this->session->userdata('user_id')
							];

							if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
								$idhonor = $this->db->insert_id();
							}
						}
					}

					$dataDetailHonorDokter = [
						'idhonor' => $idhonor,
						'idtransaksi' => $idpendaftaran,
						'jenis_transaksi' => 'poliklinik',
						'jenis_tindakan' => 'JASA DOKTER RADIOLOGI',
						'reference_table' => 'trujukan_radiologi_detail',
						'jenis_pasien' => $jenis_pasien,
						'idkelompok' => $idkelompok,
						'namakelompok' => $namakelompok,
						'idrekanan' => $idrekanan,
						'namarekanan' => $namarekanan,
						'iddetail' => $row->iddetail,
						'idtarif' => $row->idtarif,
						'namatarif' => $row->namatarif,
						'idkategori' => $row->idkategori,
						'namakategori' => $row->namakategori,
						'iddokter' => $row->iddokter,
						'namadokter' => $row->namadokter,
						'jasamedis' => $row->jasamedis,
						'potongan_rs' => $row->potongan_rs,
						'nominal_potongan_rs' => $nominal_potongan_rs,
						'pajak_dokter' => $row->pajak_dokter,
						'nominal_pajak_dokter' => $nominal_pajak_dokter,
						'jasamedis_netto' => $jasamedis_netto,
						'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
						'tanggal_pembayaran' => $tanggal_pembayaran,
						'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
						'status_expertise' => $row->status_expertise,
						'status_asuransi' => $status_asuransi
					];

					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}
		} elseif ($tipe_pendaftaran == 'rawatinap') {
			// Get Detail Info Rawat Inap
			$this->db->select("trawatinap_pendaftaran.id AS idpendaftaran,
						IF(trawatinap_pendaftaran.idtipepasien = 1, 'Pasien RS', 'Pasien Pribadi') AS jenispasien,
						trawatinap_pendaftaran.idkelompokpasien AS idkelompok,
						mpasien_kelompok.nama AS namakelompok,
						trawatinap_pendaftaran.idrekanan,
						mrekanan.nama AS namarekanan,
						trawatinap_pendaftaran.tanggaldaftar AS tanggal_pemeriksaan");
			$this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = trawatinap_tindakan_pembayaran.idtindakan');
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien', 'LEFT');
			$this->db->join('mrekanan', 'mrekanan.id = trawatinap_pendaftaran.idrekanan', 'LEFT');
			$this->db->where('trawatinap_tindakan_pembayaran.id', $idKasir);
			$this->db->where('trawatinap_tindakan_pembayaran.statusbatal', 0);
			$this->db->limit(1);
			$query = $this->db->get('trawatinap_tindakan_pembayaran');
			$dataRanap = $query->row();

			$tanggal_pemeriksaan = $dataRanap->tanggal_pemeriksaan;
			$idpendaftaran = $dataRanap->idpendaftaran;
			$jenis_pasien = $dataRanap->jenispasien;
			$idkelompok = $dataRanap->idkelompok;
			$namakelompok = $dataRanap->namakelompok;
			$idrekanan = $dataRanap->idrekanan;
			$namarekanan = $dataRanap->namarekanan;
			$status_asuransi = ($dataRanap->idkelompok != 5 ? 1 : 0);

			// JASA DOKTER RADIOLOGI [X-RAY] - RAWAT INAP
			foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 1) as $row) {
				if ($row->jasamedis > 0) {
					$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
					$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
					$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

					$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
					$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

					$this->db->set('periode_pembayaran', $tanggal_pembayaran);
					$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
					$this->db->where('id', $row->iddetail);
					$this->db->update('trujukan_radiologi_detail');

					$isStopPeriode = $this->Thonor_dokter_model->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
					if ($isStopPeriode == null) {
						$idhonor = $this->Thonor_dokter_model->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

						if ($idhonor == null) {
							$dataHonorDokter = [
								'iddokter' => $row->iddokter,
								'namadokter' => $row->namadokter,
								'tanggal_pembayaran' => $tanggal_pembayaran,
								'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
								'nominal' => 0,
								'created_at' => date('Y-m-d H:i:s'),
								'created_by' => $this->session->userdata('user_id')
							];

							if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
								$idhonor = $this->db->insert_id();
							}
						}
					}

					$dataDetailHonorDokter = [
						'idhonor' => $idhonor,
						'idtransaksi' => $idpendaftaran,
						'jenis_transaksi' => 'rawatinap',
						'jenis_tindakan' => 'JASA DOKTER RADIOLOGI (X-RAY)',
						'reference_table' => 'trujukan_radiologi_detail',
						'jenis_pasien' => $jenis_pasien,
						'idkelompok' => $idkelompok,
						'namakelompok' => $namakelompok,
						'idrekanan' => $idrekanan,
						'namarekanan' => $namarekanan,
						'iddetail' => $row->iddetail,
						'idtarif' => $row->idradiologi,
						'namatarif' => $row->namatarif,
						'idkategori' => $row->idkategori,
						'namakategori' => $row->namakategori,
						'iddokter' => $row->iddokter,
						'namadokter' => $row->namadokter,
						'jasamedis' => $row->jasamedis,
						'potongan_rs' => $row->potongan_rs,
						'nominal_potongan_rs' => $nominal_potongan_rs,
						'pajak_dokter' => $row->pajak_dokter,
						'nominal_pajak_dokter' => $nominal_pajak_dokter,
						'jasamedis_netto' => $jasamedis_netto,
						'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
						'tanggal_pembayaran' => $tanggal_pembayaran,
						'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
						'status_expertise' => $row->status_expertise,
						'status_asuransi' => $status_asuransi
					];

					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}
		}
	}
}
