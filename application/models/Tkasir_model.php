<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tkasir_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getAlasan()
	{
		$this->db->where('status', 1);
		$query = $this->db->get('malasan_batal');
		return $query->result();
	}

	public function batalkan($id)
	{
		if ($this->db->update('tkasir', ['status' => 0, 'idalasan' => $this->input->post('idalasan'), 'deleted_by' => $this->session->userdata('user_id'), 'deleted_nama' => $this->session->userdata('user_name'), 'deleted_date' => date('Y-m-d H:i:s')], ['id' => $id])) {
			// $this->db->delete('tkasir_pembayaran', array('idkasir'=>$id));
			$now = date('Y-m-d H:i:s', strtotime(date('H:i:s')));
			$data = [
				'idkasir' => $id,
				'tanggal' => $now,
				'idalasan' => $this->input->post('idalasan'),
				'deleted_by' => $this->session->userdata('user_id'),
				'deleted_nama' => $this->session->userdata('user_name'),
				// 'alasan' =>$this->input->post('alasan'),
				'status' => '1'
			];

			$this->db->insert('tkasir_pembatalan', $data);

			return true;
		} else {
			return false;
		}
	}

	public function get_tgl_trx($id)
	{
		$q = "SELECT CASE WHEN H.idtipe='3' THEN OB.tanggal ELSE H.tanggal END as tgl_trx from 
			tkasir H
			LEFT JOIN tpoliklinik_tindakan TT ON TT.id=H.idtindakan AND H.idtipe !='3'
			LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TT.idpendaftaran
			LEFT JOIN tpasien_penjualan OB ON OB.id=H.idtindakan AND H.idtipe='3'
			WHERE H.id='$id'";
		return $this->db->query($q)->row('tgl_trx');
	}

	public function get_nama_kontraktor($idpoli, $jenis)
	{
		$q = "SELECT *FROM (SELECT idrekanan as idkontraktor,M1.nama as nama_kontraktor,'1' as jenis
				FROM tpoliklinik_pendaftaran T1
				LEFT JOIN mrekanan M1 ON T1.idrekanan=M1.id
				WHERE T1.id='$idpoli'  AND T1.idrekanan IS NOT NULL
				UNION ALL
				SELECT idrekanan2 as idkontraktor,M2.nama as nama_kontraktor,'1' as jenis
				FROM tpoliklinik_pendaftaran T2
				LEFT JOIN mrekanan M2 ON T2.idrekanan2=M2.id
				WHERE T2.id='$idpoli' AND T2.idrekanan2 IS NOT NULL
				UNION ALL
				SELECT '2' as idkontraktor,'Jasa Raharja' as nama_kontraktor,'2' as jenis
				FROM tpoliklinik_pendaftaran T2
				LEFT JOIN mrekanan M2 ON T2.idrekanan2=M2.id
				WHERE T2.id='$idpoli' AND (T2.idkelompokpasien='2' OR T2.idkelompokpasien2='2')
				UNION ALL
				SELECT idtarifbpjskesehatan as idkontraktor,M3.kode as nama_kontraktor,'3' as jenis
				FROM tpoliklinik_pendaftaran T3
				LEFT JOIN mtarif_bpjskesehatan M3 ON T3.idtarifbpjskesehatan=M3.id
				WHERE T3.id='$idpoli' AND T3.idtarifbpjskesehatan IS NOT NULL
				UNION ALL
				SELECT idtarifbpjskesehatan2 as idkontraktor,M3.kode as nama_kontraktor,'3' as jenis
				FROM tpoliklinik_pendaftaran T4
				LEFT JOIN mtarif_bpjskesehatan M3 ON T4.idtarifbpjskesehatan2=M3.id
				WHERE T4.id='$idpoli' AND T4.idtarifbpjskesehatan2 IS NOT NULL
				UNION ALL
				SELECT idtarifbpjstenagakerja as idkontraktor,M3.id as nama_kontraktor,'4' as jenis
				FROM tpoliklinik_pendaftaran T5
				LEFT JOIN mtarif_bpjstenagakerja M3 ON T5.idtarifbpjstenagakerja=M3.id
				WHERE T5.id='$idpoli' AND T5.idtarifbpjstenagakerja IS NOT NULL
				UNION ALL
				SELECT idtarifbpjstenagakerja2 as idkontraktor,M3.id as nama_kontraktor,'4' as jenis
				FROM tpoliklinik_pendaftaran T5
				LEFT JOIN mtarif_bpjstenagakerja M3 ON T5.idtarifbpjstenagakerja2=M3.id
				WHERE T5.id='$idpoli' AND T5.idtarifbpjstenagakerja2 IS NOT NULL
				)G where jenis='$jenis'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_header_poli($id)
	{
		$this->db->select('tpoliklinik_pendaftaran.*,
        mfpasien.no_medrec, mfpasien.title,mfpasien.nama,mfpasien.alamat_jalan AS alamat,
        mpasien_kelompok.id AS idkelompokpasien, mpasien_kelompok.nama as kelompok,kel2.nama as kelompok2,mdokter.nama as dokter,mpoliklinik.nama as poli,
    		mtarif_bpjskesehatan.kode as tarif_bpjs_kes,
    		kes2.kode as tarif_bpjs_kes2,mtarif_bpjstenagakerja.nama as tarif_bpjs_ket,
        ket2.nama as tarif_bpjs_ket2,mrekanan.nama as rekanan,rek2.nama as rekanan2');
		$this->db->from('tpoliklinik_pendaftaran');
		$this->db->join('tpoliklinik_tindakan', 'tpoliklinik_tindakan.idpendaftaran=tpoliklinik_pendaftaran.id', 'LEFT');
		$this->db->join('mtarif_bpjskesehatan', 'tpoliklinik_pendaftaran.idtarifbpjskesehatan=mtarif_bpjskesehatan.id', 'LEFT');
		$this->db->join('mtarif_bpjskesehatan kes2', 'tpoliklinik_pendaftaran.idtarifbpjskesehatan2=kes2.id', 'LEFT');
		$this->db->join('mtarif_bpjstenagakerja', 'tpoliklinik_pendaftaran.idtarifbpjstenagakerja=mtarif_bpjstenagakerja.id', 'LEFT');
		$this->db->join('mtarif_bpjstenagakerja ket2', 'tpoliklinik_pendaftaran.idtarifbpjstenagakerja2=ket2.id', 'LEFT');
		$this->db->join('mrekanan', 'tpoliklinik_pendaftaran.idrekanan=mrekanan.id', 'LEFT');
		$this->db->join('mrekanan rek2', 'tpoliklinik_pendaftaran.idrekanan2=rek2.id', 'LEFT');
		$this->db->join('mfpasien', 'mfpasien.id=tpoliklinik_pendaftaran.idpasien', 'LEFT');
		$this->db->join('mdokter', 'mdokter.id=tpoliklinik_pendaftaran.iddokter', 'LEFT');
		$this->db->join('mpoliklinik', 'mpoliklinik.id=tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
		$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id=tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
		$this->db->join('mpasien_kelompok kel2', 'kel2.id=tpoliklinik_pendaftaran.idkelompokpasien2', 'LEFT');
		$this->db->where('tpoliklinik_tindakan.idpendaftaran', $id);
		$query = $this->db->get();
		return $query->row();
	}

	public function getBank()
	{
		$this->db->select('*');
		$query = $this->db->get('mbank');
		return $query->result();
	}

	public function list_admin($id)
	{
		// $this->db->select('*');
		// $this->db->where('idpoliklinik', $id);
		// $query = $this->db->get('view_rincian_administrasi_rajal');
		// return $query->result();
		$q = "SELECT H.id as iddetail,H.namatarif,H.total as subtotal,H.kuantitas,H.diskon,H.totalkeseluruhan as total
			,H.jasasarana,H.jasapelayanan,H.bhp,H.biayaperawatan
			,H.jasasarana_disc,H.jasapelayanan_disc,H.bhp_disc,H.biayaperawatan_disc
			,H.totalkeseluruhan
			from tpoliklinik_administrasi H
			WHERE H.idpendaftaran='$id' AND H.`status`='1'";
		return $this->db->query($q)->result();
	}

	public function list_tindakan($id)
	{
		// $this->db->select('*');
		// $this->db->where('idpoliklinik', $id);
		// $query = $this->db->get('view_rincian_rajal_tindakan');
		$q = "SELECT R.id as iddetail,R.idtindakan, RI.id as idrawatinap, P.id as idpoliklinik
			,DATE(P.tanggaldaftar) as tanggal,R.idpelayanan,M.nama as namatarif,R.iddokter as iddokter
			,mdokter.nama as namadokter,R.jasasarana,R.jasasarana_disc,R.jasapelayanan,R.jasapelayanan_disc,R.bhp,R.bhp_disc
			,R.biayaperawatan,R.biayaperawatan_disc,R.total,R.kuantitas,R.diskon,R.totalkeseluruhan,R.`status`,R.statusverifikasi
			,9 as kelompok,V.harga_revisi,V.id as idverifdetail
			FROM tpoliklinik_pendaftaran P
			LEFT JOIN tpoliklinik_tindakan T ON T.idpendaftaran=P.id
			LEFT JOIN trawatinap_pendaftaran RI ON RI.idpoliklinik=P.id AND RI.STATUS <> 0
			LEFT JOIN tpoliklinik_pelayanan R ON R.idtindakan=T.id
			LEFT JOIN mtarif_rawatjalan M ON M.id=R.idpelayanan
			LEFT JOIN mdokter ON mdokter.id=R.iddokter
			LEFT JOIN tverifikasi_transaksi_detail V ON V.id_detail=R.id AND V.id_kelompok=9
			WHERE P.id='$id' AND R.`status` != 0";
		// print_r($q);exit();
		$query = $this->db->query($q);
		return $query->result();
	}

	public function list_lab($id)
	{
		// $this->db->select('*');
		// $this->db->where('idpoliklinik', $id);
		// $this->db->where('statusrincianpaket', '0');
		// $query = $this->db->get('view_rincian_laboratorium_rajal');
		// print_r($this->db->last_query());exit();
		$q = "SELECT D.id as iddetail,RI.id as idrawatinap, P.id as idpoliklinik,D.idrujukan,R.norujukan
			,DATE(R.tanggal) as tanggal,M.idtipe,D.idlaboratorium,M.nama as namatarif,R.iddokterperujuk as iddokter
			,mdokter.nama as namadokter,D.jasasarana,D.jasasarana_disc,D.jasapelayanan,D.jasapelayanan_disc,D.bhp,D.bhp_disc
			,D.biayaperawatan,D.biayaperawatan_disc,D.total,D.kuantitas,D.diskon,D.totalkeseluruhan,D.`status`,D.statusrincianpaket,D.statusverifikasi
			,6 as kelompok,V.harga_revisi,V.id as idverifdetail
			FROM tpoliklinik_pendaftaran P
			LEFT JOIN tpoliklinik_tindakan T ON T.idpendaftaran=P.id
			LEFT JOIN trawatinap_pendaftaran RI ON RI.idpoliklinik=P.id AND RI.STATUS <> 0
			LEFT JOIN trujukan_laboratorium R ON R.idtindakan=T.id AND R.asalrujukan IN (1,2)
			LEFT JOIN trujukan_laboratorium_detail D ON D.idrujukan=R.id
			LEFT JOIN mtarif_laboratorium M ON M.id=D.idlaboratorium
			LEFT JOIN mdokter ON mdokter.id=R.iddokterperujuk
			LEFT JOIN tverifikasi_transaksi_detail V ON V.id_detail=D.id AND V.id_kelompok=6
			WHERE P.id='$id' AND D.statusrincianpaket='0' AND R.`status` != 0";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function list_radiologi($id)
	{
		// $this->db->select('*');
		// $this->db->where('idpoliklinik', $id);
		// $query = $this->db->get('view_rincian_radiologi_rajal');
		$q = "SELECT D.id as iddetail,RI.id as idrawatinap, P.id as idpoliklinik,D.idrujukan,R.norujukan
			,DATE(R.tanggal) as tanggal,M.idtipe,D.idradiologi,
			(CASE WHEN M.idtipe='1' THEN
			concat( `M`.`nama`, ' (', `E`.`nama`, ' ', `F`.`nama`, ')' ) ELSE `M`.`nama`
					END ) as namatarif
			,R.iddokterperujuk as iddokter
			,mdokter.nama as namadokter,D.jasasarana,D.jasasarana_disc,D.jasapelayanan,D.jasapelayanan_disc,D.bhp,D.bhp_disc
			,D.biayaperawatan,D.biayaperawatan_disc,D.total,D.kuantitas,D.diskon,D.totalkeseluruhan,D.`status`,D.statusverifikasi
			,8 as kelompok,V.harga_revisi,V.id as idverifdetail
			FROM tpoliklinik_pendaftaran P
			LEFT JOIN tpoliklinik_tindakan T ON T.idpendaftaran=P.id
			LEFT JOIN trawatinap_pendaftaran RI ON RI.idpoliklinik=P.id AND RI.STATUS <> 0
			LEFT JOIN trujukan_radiologi R ON R.idtindakan=T.id AND R.asalrujukan IN (1,2)
			LEFT JOIN trujukan_radiologi_detail D ON D.idrujukan=R.id
			LEFT JOIN mtarif_radiologi M ON M.id=D.idradiologi
			LEFT JOIN mtarif_radiologi_expose E ON M.idexpose=E.id
			LEFT JOIN mtarif_radiologi_film F ON M.idfilm=F.id
			LEFT JOIN mdokter ON mdokter.id=R.iddokterperujuk
			LEFT JOIN tverifikasi_transaksi_detail V ON V.id_detail=D.id AND V.id_kelompok=8
			WHERE P.id='$id' AND R.`status`!='0'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function list_fisio($id)
	{
		// $this->db->select('*');
		// $this->db->where('idpoliklinik', $id);
		// $query = $this->db->get('view_rincian_fisioterapi_rajal');
		$q = "SELECT D.id as iddetail,RI.id as idrawatinap, P.id as idpoliklinik,D.idrujukan,R.norujukan
			,DATE(R.tanggal) as tanggal,D.idfisioterapi,M.nama as namatarif,R.iddokterperujuk as iddokter
			,mdokter.nama as namadokter,D.jasasarana,D.jasasarana_disc,D.jasapelayanan,D.jasapelayanan_disc,D.bhp,D.bhp_disc
			,D.biayaperawatan,D.biayaperawatan_disc,D.total,D.kuantitas,D.diskon,D.totalkeseluruhan,D.`status`,D.statusverifikasi
			,9 as kelompok,V.harga_revisi,V.id as idverifdetail
			FROM tpoliklinik_pendaftaran P
			LEFT JOIN tpoliklinik_tindakan T ON T.idpendaftaran=P.id
			LEFT JOIN trawatinap_pendaftaran RI ON RI.idpoliklinik=P.id AND RI.STATUS <> 0
			LEFT JOIN trujukan_fisioterapi R ON R.idtindakan=T.id AND R.asalrujukan IN (1,2)
			LEFT JOIN trujukan_fisioterapi_detail D ON D.idrujukan=R.id AND D.status='1'
			LEFT JOIN mtarif_fisioterapi M ON M.id=D.idfisioterapi
			LEFT JOIN mdokter ON mdokter.id=R.iddokterperujuk
			LEFT JOIN tverifikasi_transaksi_detail V ON V.id_detail=D.id AND V.id_kelompok=9
			WHERE P.id='$id' AND R.`status` != 0";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function list_farmasi($id, $idtipe)
	{
		// $this->db->select('*');
		// $this->db->where('idpoliklinik', $id);
		// $this->db->where('idtipe', $idtipe);
		// $query = $this->db->get('view_rincian_farmasi_rajal');
		$q = "SELECT D.id,R.asalrujukan, R.idtindakan,RI.id as idrawatinap, P.id as idpoliklinik,R.nopenjualan,DATE(R.tanggal) tanggal,B.nama as namatarif,B.kode,B.idkategori
			,D.idbarang,D.idtipe,D.kuantitas,D.harga_dasar,D.margin,D.harga as hargajual,D.diskon,D.tuslah,D.totalharga as totalkeseluruhan,D.statusverifikasi,3 as kelompok
			,V.harga_revisi,V.id as idverifdetail
			FROM tpoliklinik_pendaftaran P
			LEFT JOIN tpoliklinik_tindakan T ON T.idpendaftaran=P.id
			LEFT JOIN trawatinap_pendaftaran RI ON RI.idpoliklinik=P.id AND RI.STATUS <> 0
			LEFT JOIN tpasien_penjualan R ON R.idtindakan=T.id AND R.asalrujukan IN (1,2)
			LEFT JOIN tpasien_penjualan_nonracikan D ON D.idpenjualan=R.id AND D.`status`='1'
			LEFT JOIN view_barang_all B ON B.id=D.idbarang AND B.idtipe=D.idtipe
			LEFT JOIN tverifikasi_transaksi_detail V ON V.id_detail=D.id AND V.id_kelompok='3'
			WHERE P.id='$id' AND D.idtipe='$idtipe' AND  R.`status` != '0'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function list_farmasi_racikan($id)
	{
		// $this->db->select('*');
		// $this->db->where('idpoliklinik', $id);
		// $this->db->where('statusracikan', '1');
		// $query = $this->db->get('view_rincian_farmasi_rajal');
		$q = "SELECT D.id,R.asalrujukan, R.idtindakan,RI.id as idrawatinap, P.id as idpoliklinik,R.nopenjualan,DATE(R.tanggal) tanggal,D.namaracikan as namatarif,'' as kode,'0' as idkategori
			,D.kuantitas,D.totalharga as  harga_dasar,'0' as margin,D.totalharga as hargajual,'0' as diskon,D.tuslah,D.totalharga as totalkeseluruhan,D.statusverifikasi,3 as kelompok
			,V.harga_revisi,V.id as idverifdetail
			FROM tpoliklinik_pendaftaran P
			LEFT JOIN tpoliklinik_tindakan T ON T.idpendaftaran=P.id
			LEFT JOIN trawatinap_pendaftaran RI ON RI.idpoliklinik=P.id AND RI.STATUS <> 0
			LEFT JOIN tpasien_penjualan R ON R.idtindakan=T.id AND R.asalrujukan IN (1,2)
			LEFT JOIN tpasien_penjualan_racikan D ON D.idpenjualan=R.id AND D.`status`='1'
			LEFT JOIN tverifikasi_transaksi_detail V ON V.id_detail=D.id AND V.id_kelompok='3'
			WHERE P.id='$id' AND  R.`status` != '0' AND D.id IS NOT NULL";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function list_bayar($id)
	{
		$this->db->select('*');
		$this->db->where('tkasir_pembayaran.idkasir', $id);
		$query = $this->db->get('tkasir_pembayaran');
		return $query->result();
	}

	public function get_tipe_kontraktor()
	{
		$q = 'SELECT id,nama from mpasien_kelompok WHERE id IN (1,2,3,4)';
		return $this->db->query($q)->result();
	}

	public function list_igd_obat($id)
	{
		// $this->db->select('*');
		// $this->db->where('idpoliklinik', $id);
		// $query = $this->db->get('view_rincian_rajal_obat');
		$q = "SELECT R.id as iddetail,RI.id as idrawatinap, P.id as idpoliklinik
			,DATE(P.tanggaldaftar) as tanggal,
			R.idobat,B.nama as namatarif,B.kode
			,R.hargadasar,R.margin,R.hargajual,R.kuantitas,R.diskon,R.totalkeseluruhan
			,R.`status`,R.statusverifikasi
			,7 as kelompok,V.harga_revisi,V.id as idverifdetail
			FROM tpoliklinik_pendaftaran P
			LEFT JOIN tpoliklinik_tindakan T ON T.idpendaftaran=P.id
			LEFT JOIN trawatinap_pendaftaran RI ON RI.idpoliklinik=P.id AND RI.STATUS <> 0
			INNER JOIN tpoliklinik_obat R ON R.idtindakan=T.id
			LEFT JOIN view_barang_all B ON B.id=R.idobat AND B.idtipe=R.idtipe
			LEFT JOIN tverifikasi_transaksi_detail V ON V.id_detail=R.id AND V.id_kelompok=7
			WHERE P.id='$id' ";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function list_igd_alkes($id)
	{
		// $this->db->select('*');
		// $this->db->where('idpoliklinik', $id);
		// $query = $this->db->get('view_rincian_rajal_alkes');
		$q = "SELECT R.id as iddetail,RI.id as idrawatinap, P.id as idpoliklinik
			,DATE(P.tanggaldaftar) as tanggal,
			R.idalkes,B.nama as namatarif,B.kode
			,R.hargadasar,R.margin,R.hargajual,R.kuantitas,R.diskon,R.totalkeseluruhan
			,R.`status`,R.statusverifikasi
			,7 as kelompok,V.harga_revisi,V.id as idverifdetail
			FROM tpoliklinik_pendaftaran P
			LEFT JOIN tpoliklinik_tindakan T ON T.idpendaftaran=P.id
			LEFT JOIN trawatinap_pendaftaran RI ON RI.idpoliklinik=P.id AND RI.STATUS <> 0
			INNER JOIN tpoliklinik_alkes R ON R.idtindakan=T.id
			LEFT JOIN view_barang_all B ON B.id=R.idalkes AND B.idtipe=R.idtipe
			LEFT JOIN tverifikasi_transaksi_detail V ON V.id_detail=R.id AND V.id_kelompok=7
			WHERE P.id='$id' ";
		$query = $this->db->query($q);
		// print_r($q);exit();
		return $query->result();
	}

	public function list_pembayaran($idkasir)
	{
		$this->db->select('*');
		$this->db->where('idkasir', $idkasir);
		$query = $this->db->get('tkasir_pembayaran');
		return $query->result();
	}

	public function get_kontraktor($idtipe, $searchText)
	{
		if ($idtipe == '1') {
			$this->db->select('mrekanan.*');
			$this->db->from('mrekanan');
			$this->db->like('mrekanan.nama', $searchText);
			$query = $this->db->get();
			// print_r($this->db->last_query());exit();
			return $query->result_array();
		} elseif ($idtipe == '2') {
			$data[] = ['id' => '2', 'nama' => 'Jasa Raharja'];
			return $data;
		} elseif ($idtipe == '3') {
			$q = "SELECT H.id,H.kode as nama from mtarif_bpjskesehatan H WHERE H.kode like '%" . $searchText . "%' OR H.nama like '%" . $searchText . "%'";
			$query = $this->db->query($q);
			return $query->result_array();
		} elseif ($idtipe == '4') {
			$q = "SELECT H.id,H.id as nama from mtarif_bpjstenagakerja H WHERE H.id like '%" . $searchText . "%' OR H.nama like '%" . $searchText . "%'";
			$query = $this->db->query($q);
			return $query->result_array();
		}
	}

	public function get_pegawai($searchText)
	{
		$this->db->select('mpegawai.*');
		$this->db->from('mpegawai');
		$this->db->where('status', 1);
		$this->db->like('mpegawai.nama', $searchText);
		$query = $this->db->get();
		// print_r($this->db->last_query());exit();
		return $query->result_array();
	}

	public function get_dokter($searchText)
	{
		$this->db->select('mdokter.*');
		$this->db->from('mdokter');
		$this->db->like('mdokter.nama', $searchText);
		$query = $this->db->get();
		// print_r($this->db->last_query());exit();
		return $query->result_array();
	}

	public function saveDataPembayaran($bayar)
	{
		if ($this->db->insert('tkasir_pembayaran', $bayar)) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function update_cetak($idkasir, $jenis, $idbayar = '0')
	{
		$data = [
			'idkasir' => $idkasir,
			'idbayar' => $idbayar,
			'jenis' => $jenis,
			'tgl_cetak' => date('Y-m-d H:i:s'),
			'user_cetak' => $this->session->userdata('user_id')
		];
		// print_r($data);exit();
		if ($this->db->insert('tkasir_cetak', $data)) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function DeletePembayaran($idkasir)
	{
		$this->db->delete('tkasir_pembayaran', ['idkasir' => $idkasir]);
		return true;
	}

	public function UpdateTkasir($idkasir, $jenis = 'rajal')
	{
		if ($jenis == 'rajal') {
			$tanggal = YMDTimeFormat($this->input->post('tanggal'));
		} else {
			$tanggal = YMDTimeFormat($this->input->post('tanggal'));
		}

		$subTotal = RemoveComma($this->input->post('gt_all'));
		$diskonRp = RemoveComma($this->input->post('diskon_rp'));
		$totalTransaksi = RemoveComma($this->input->post('gt_rp'));
		$nominalPembayaran = RemoveComma($this->input->post('bayar_rp'));
		$nominalRound = ($totalTransaksi + $diskonRp) - $subTotal;

		$nominalKembalian = 0;
		if ($nominalPembayaran > $totalTransaksi) {
			$nominalKembalian = $nominalPembayaran - $totalTransaksi;
		}

		$data = [
			'tanggal' => $tanggal,
			'edited_by' => $this->session->userdata('user_id'),
			'edited_nama' => $this->session->userdata('user_name'),
			'edited_date' => date('Y-m-d H:i:s'),
			'status' => 2,
			'subtotal' => $subTotal,
			'diskon' => $diskonRp,
			'total' => $totalTransaksi,
			'bayar' => $nominalPembayaran,
			'kembalian' => $nominalKembalian,
			'nominal_round' => $nominalRound,
		];

		$this->db->where('id', $idkasir);
		$this->db->update('tkasir', $data);
		return true;
	}

	public function hapus_tpoliklinik_pelayanan($iddetail)
	{
		$data = [
			'status' => 0,
		];

		$this->db->where('id', $iddetail);
		$this->db->update('tpoliklinik_pelayanan', $data);
		return true;
	}

	public function hapus_tpoliklinik_admin($iddetail)
	{
		$data = [
			'status' => 0,
		];

		$this->db->where('id', $iddetail);
		$this->db->update('tpoliklinik_administrasi', $data);
		return true;
	}

	public function update_tpoliklinik_pelayanan($data, $iddetail)
	{
		// print_r($data);exit();
		$this->db->where('id', $iddetail);
		$this->db->update('tpoliklinik_pelayanan', $data);
		return true;
	}

	public function update_tpoliklinik_admin($data, $iddetail)
	{
		// print_r($data);exit();
		$this->db->where('id', $iddetail);
		$this->db->update('tpoliklinik_administrasi', $data);
		return true;
	}

	public function update_tpoliklinik_lab($data, $iddetail)
	{
		// print_r($data);exit();
		$this->db->where('id', $iddetail);
		$this->db->update('trujukan_laboratorium_detail', $data);
		return true;
	}

	public function update_tpoliklinik_radiologi($data, $iddetail)
	{
		// print_r($data);exit();
		$this->db->where('id', $iddetail);
		$this->db->update('trujukan_radiologi_detail', $data);
		return true;
	}

	public function update_tpoliklinik_fisio($data, $iddetail)
	{
		// print_r($data);exit();
		$this->db->where('id', $iddetail);
		$this->db->update('trujukan_fisioterapi_detail', $data);
		return true;
	}

	public function get_kasir($idkasir)
	{
		$q = "select diskon,SUM(tkasir_pembayaran.nominal) as total_bayar From tkasir INNER JOIN tkasir_pembayaran ON tkasir.id=tkasir_pembayaran.idkasir
		WHERE tkasir.id='$idkasir'
		GROUP BY tkasir.id ";
		$query = $this->db->query($q);
		// print_r($query->row());exit();
		return $query->row();
	}

	// EQI
	public function getHeaderInfoFaktur($id)
	{
		$this->db->select('tpoliklinik_pendaftaran.*,
          mfpasien.no_medrec AS nomedrec,
          mfpasien.nama AS namapasien,

          mdokter.nama AS namadokter,mpoliklinik.nama as namapoli,mrekanan.nama as namarekanan,tkasir.tanggal as tanggal_kasir');
		$this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
		$this->db->join('mdokter', 'mdokter.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
		$this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
		$this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
		$this->db->join('tpoliklinik_tindakan', 'tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id', 'LEFT');
		$this->db->join('tkasir', 'tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe !="3"', 'LEFT');
		$this->db->where('tpoliklinik_pendaftaran.id', $id);
		$query = $this->db->get('tpoliklinik_pendaftaran');
		// print_r($this->db->last_query());exit();
		return $query->row();
	}

	public function getHeaderInfoFaktur_NR($id)
	{
		$q = "SELECT H.nama,H.nomedrec,H.alamat,K.tanggal,K.nokasir from tkasir K INNER JOIN tpasien_penjualan H ON H.id=K.idtindakan
			WHERE K.id='$id'";
		$query = $this->db->query($q);
		return $query->row();
	}

	public function jenis_rujukan($id)
	{
		$q = "SELECT idtipe from tkasir where id='$id' ";
		$query = $this->db->query($q);
		return $query->row('idtipe');
	}

	public function list_non_racikan($id)
	{
		$q = "SELECT B.kode,B.nama as namatarif,D.harga,D.diskon,
	D.kuantitas,D.totalharga  from tkasir H
INNER JOIN tpasien_penjualan_nonracikan D ON D.idpenjualan=H.idtindakan
INNER JOIN view_barang_all B ON B.id=D.idbarang AND B.idtipe=D.idtipe
WHERE H.id='$id'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function list_racikan($id)
	{
		// $q="SELECT R.namaracikan as namatarif,R.harga,R.tuslah,R.kuantitas,R.totalharga from tkasir K INNER JOIN tpasien_penjualan H ON H.id=K.idtindakan
		// LEFT JOIN tpasien_penjualan_racikan R ON R.idpenjualan=H.id
		// WHERE K.id='$id'";
		$q = "SELECT D.namaracikan as namatarif,D.harga,D.tuslah,D.kuantitas,D.totalharga  from tkasir H
INNER JOIN tpasien_penjualan_racikan D ON D.idpenjualan=H.idtindakan
WHERE H.id='$id'";
		$query = $this->db->query($q);
		// print_r($q);exit();
		return $query->result();
	}

	public function getHeaderKwitansi($id)
	{
		$q = "SELECT PP.no_medrec,PP.namapasien,PP.nopendaftaran,PP.tanggaldaftar,
			PP.idtipe,PP.alamatpasien,PT.iddokter1,D.nama as namadokter,P.nama as namapoli
			from tkasir K
			INNER JOIN tpoliklinik_tindakan PT On PT.id=K.idtindakan
			INNER JOIN tpoliklinik_pendaftaran PP ON PP.id=PT.idpendaftaran
			LEFT JOIN `mpoliklinik` ON `mpoliklinik`.`id` = `PP`.`idpoliklinik`
			LEFT JOIN mdokter D on D.id=PT.iddokter1
			LEFT JOIN mpoliklinik P ON P.id=PP.idpoliklinik
			where K.id='$id'";
		$query = $this->db->query($q);
		// print_r($q);exit();
		return $query->row();
	}

	public function get_data_kasir($id, $idprint = '0')
	{
		// $this->db->select("tkasir.*");
		// $this->db->where('tkasir.id', $id);
		// $query = $this->db->get('tkasir');
		if ($idprint == '0') {
			$q = "SELECT K.id,K.total,K.bayar,K.kembalian,K.edited_by,U.`name` as namakasir,K.penyetor,K.deskripsi_kwitansi,
			SUM(CASE WHEN C.jenis='1' THEN 1 ELSE 0 END) as totalcetakfaktur,
			SUM(CASE WHEN C.jenis='2' THEN 1 ELSE 0 END) as totalcetakkwitansi,TC.namacetak,TC.tgl_cetak
			FROM tkasir K
			LEFT JOIN musers U ON U.id=K.edited_by
			LEFT JOIN tkasir_cetak C ON C.idkasir=K.id
			LEFT JOIN (
				SELECT tkasir_cetak.user_cetak,tkasir_cetak.tgl_cetak,musers.`name` as namacetak,tkasir_cetak.idkasir FROM tkasir_cetak LEFT JOIN musers ON musers.id=tkasir_cetak.user_cetak WHERE tkasir_cetak.idkasir='$id' AND tkasir_cetak.jenis='2' ORDER BY tkasir_cetak.id DESC LIMIT 1
			) as TC ON TC.idkasir=K.id

			where K.id='$id'
			GROUP BY K.id";
		// print_r($idprint);exit();
		} else {
			$q = "SELECT K.id,K.nominal as bayar,'0' as kembalian,TK.edited_by,U.`name` as namakasir,TK.penyetor,TK.deskripsi_kwitansi,
			SUM(CASE WHEN C.jenis='1' THEN 1 ELSE 0 END) as totalcetakfaktur,
			SUM(CASE WHEN C.jenis='2' AND C.idbayar='$idprint' THEN 1 ELSE 0 END) as totalcetakkwitansi,TC.namacetak,TC.tgl_cetak
			FROM tkasir_pembayaran K
			LEFT JOIN tkasir TK ON TK.id=K.idkasir
			LEFT JOIN musers U ON U.id=TK.edited_by
			LEFT JOIN tkasir_cetak C ON C.idkasir=TK.id
			LEFT JOIN (
				SELECT tkasir_cetak.user_cetak,tkasir_cetak.tgl_cetak,musers.`name` as namacetak,tkasir_cetak.idkasir FROM tkasir_cetak LEFT JOIN musers ON musers.id=tkasir_cetak.user_cetak WHERE tkasir_cetak.idkasir='$id' AND tkasir_cetak.jenis='2' AND tkasir_cetak.idbayar='$idprint' ORDER BY tkasir_cetak.id DESC LIMIT 1
			) as TC ON TC.idkasir=K.id

			where K.idkasir='$id' AND K.id='$idprint'
			GROUP BY K.id";
		}

		$query = $this->db->query($q);
		// print_r($q);exit;
		return $query->row();
	}

	public function find_old($id)
	{
		$q = "SELECT penyetor,deskripsi_kwitansi from tkasir where id='$id'";
		$query = $this->db->query($q);
		return $query->row();
	}

	public function list_tarif_admin()
	{
		$q = "SELECT H.id,H.nama from mtarif_administrasi H
				WHERE H.idtipe='1' AND H.`status`='1'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_tarif_admin($id)
	{
		$q = "SELECT * from mtarif_administrasi H
				WHERE H.id='$id'";
		$query = $this->db->query($q);
		return $query->row();
	}
}
