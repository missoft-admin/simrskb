<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Msurat_template_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // public function getAll()
    // {
    //     $this->db->where('staktif', '1');
    //     $this->db->order_by('kode', 'ASC');
    //     $this->db->get('msurat_template');
    //     return $this->db->last_query();
    // }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('msurat_template');
        return $query->row();
    }
	function load_var($id){
		if ($id=='1'){
			$nama_tabel='msurat_template_var_1';
		}
		if ($id=='2'){
			$nama_tabel='msurat_template_var_2';
		}
		if ($id=='3'){
			$nama_tabel='msurat_template_var_3';
		}
		if ($id=='4'){
			$nama_tabel='msurat_template_var_4';
		}
		if ($id=='5'){
			$nama_tabel='msurat_template_var_5';
		}
		if ($id=='6'){
			$nama_tabel='msurat_template_var_6';
		}
		if ($id=='7'){
			$nama_tabel='msurat_template_var_7';
		}
		if ($id=='8'){
			$nama_tabel='msurat_template_var_8';
		}
		if ($id=='9'){
			$nama_tabel='msurat_template_var_9';
		}
		$q="SELECT *FROM ".$nama_tabel." LIMIT 1";
		// print_r($q);exit;
		return $this->db->query($q)->row_array();
	}
    public function saveData()
    {
        $this->kode 					= $_POST['kode'];
        $this->jenis_surat 					= $_POST['jenis_surat'];
        $this->isi_surat 			= $_POST['isi_surat'];
		$this->judul_ina 			= $_POST['judul_ina'];
        $this->judul_eng 			= $_POST['judul_eng'];
        $this->footer_ina 			= $_POST['footer_ina'];
        $this->footer_eng 			= $_POST['footer_eng'];
        $this->staktif 			  = 1;
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('msurat_template', $this)) {
            return $this->db->insert_id();
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->jenis_surat 					= $_POST['jenis_surat'];
        $this->kode 					= $_POST['kode'];
        $this->isi_surat 			= $_POST['isi_surat'];
        $this->judul_ina 			= $_POST['judul_ina'];
        $this->judul_eng 			= $_POST['judul_eng'];
        $this->footer_ina 			= $_POST['footer_ina'];
        $this->footer_eng 			= $_POST['footer_eng'];
        $this->staktif 			  = 1;
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		if ($_POST['id']=='1'){
			$data_var=array(
				'paragraf_1_ina' => $this->input->post('paragraf_1_ina'),
				'paragraf_1_eng' => $this->input->post('paragraf_1_eng'),
				'nama_pasien_ina' => $this->input->post('nama_pasien_ina'),
				'nama_pasien_eng' => $this->input->post('nama_pasien_eng'),
				'alamat_ina' => $this->input->post('alamat_ina'),
				'alamat_eng' => $this->input->post('alamat_eng'),
				'pekerjaan_ina' => $this->input->post('pekerjaan_ina'),
				'pekerjaan_eng' => $this->input->post('pekerjaan_eng'),
				'opsi_ina' => $this->input->post('opsi_ina'),
				'opsi_eng' => $this->input->post('opsi_eng'),
				'lama_ina' => $this->input->post('lama_ina'),
				'lama_eng' => $this->input->post('lama_eng'),
				'mulai_ina' => $this->input->post('mulai_ina'),
				'mulai_eng' => $this->input->post('mulai_eng'),
				'sampai_ina' => $this->input->post('sampai_ina'),
				'sampai_eng' => $this->input->post('sampai_eng'),
				'paragraf_2_ina' => $this->input->post('paragraf_2_ina'),
				'paragraf_2_eng' => $this->input->post('paragraf_2_eng'),

			);
			$this->db->update('msurat_template_var_1', $data_var);
		}
		if ($_POST['id']=='2'){
			$data_var=array(
				'paragraf_1_ina' => $this->input->post('paragraf_1_ina'),
				'paragraf_1_eng' => $this->input->post('paragraf_1_eng'),
				'nama_pasien_ina' => $this->input->post('nama_pasien_ina'),
				'nama_pasien_eng' => $this->input->post('nama_pasien_eng'),
				'alamat_ina' => $this->input->post('alamat_ina'),
				'alamat_eng' => $this->input->post('alamat_eng'),
				'pekerjaan_ina' => $this->input->post('pekerjaan_ina'),
				'pekerjaan_eng' => $this->input->post('pekerjaan_eng'),
				'tb_ina' => $this->input->post('tb_ina'),
				'tb_eng' => $this->input->post('tb_eng'),
				'satuan_tb_ina' => $this->input->post('satuan_tb_ina'),
				'satuan_tb_eng' => $this->input->post('satuan_tb_eng'),
				'bb_ina' => $this->input->post('bb_ina'),
				'bb_eng' => $this->input->post('bb_eng'),
				'satuan_bb_ina' => $this->input->post('satuan_bb_ina'),
				'satuan_bb_eng' => $this->input->post('satuan_bb_eng'),
				'tekanan_darah_ina' => $this->input->post('tekanan_darah_ina'),
				'tekanan_darah_eng' => $this->input->post('tekanan_darah_eng'),
				'satuan_tekanan_darah_ina' => $this->input->post('satuan_tekanan_darah_ina'),
				'satuan_tekanan_darah_eng' => $this->input->post('satuan_tekanan_darah_eng'),
				'buta_ina' => $this->input->post('buta_ina'),
				'buta_eng' => $this->input->post('buta_eng'),
				'informasi_lain_ina' => $this->input->post('informasi_lain_ina'),
				'informasi_lain_eng' => $this->input->post('informasi_lain_eng'),
				'paragraf_2_ina' => $this->input->post('paragraf_2_ina'),
				'paragraf_2_eng' => $this->input->post('paragraf_2_eng'),
				'keadaan_ina' => $this->input->post('keadaan_ina'),
				'keadaan_eng' => $this->input->post('keadaan_eng'),
			);
			$this->db->update('msurat_template_var_2', $data_var);
		}
		if ($_POST['id']=='3'){
			$data_var=array(
				'paragraf_1_ina' => $this->input->post('paragraf_1_ina'),
				'paragraf_1_eng' => $this->input->post('paragraf_1_eng'),
				'nama_pasien_ina' => $this->input->post('nama_pasien_ina'),
				'nama_pasien_eng' => $this->input->post('nama_pasien_eng'),
				'pekerjaan_ina' => $this->input->post('pekerjaan_ina'),
				'pekerjaan_eng' => $this->input->post('pekerjaan_eng'),
				'alamat_ina' => $this->input->post('alamat_ina'),
				'alamat_eng' => $this->input->post('alamat_eng'),
				'paragraf_2_ina' => $this->input->post('paragraf_2_ina'),
				'paragraf_2_eng' => $this->input->post('paragraf_2_eng'),
				'tl_ina' => $this->input->post('tl_ina'),
				'tl_eng' => $this->input->post('tl_eng'),
				'jk_ina' => $this->input->post('jk_ina'),
				'jk_eng' => $this->input->post('jk_eng'),
				'telah_ina' => $this->input->post('telah_ina'),
				'telah_eng' => $this->input->post('telah_eng'),
				'diagnosa_ina' => $this->input->post('diagnosa_ina'),
				'diagnosa_eng' => $this->input->post('diagnosa_eng'),
				'diharuskan_ina' => $this->input->post('diharuskan_ina'),
				'diharuskan_eng' => $this->input->post('diharuskan_eng'),
			);
			$this->db->update('msurat_template_var_3', $data_var);
		}
		if ($_POST['id']=='4'){
			$data_var=array(
				'kepada_ina' => $this->input->post('kepada_ina'),
				'kepada_eng' => $this->input->post('kepada_eng'),
				'up_ina' => $this->input->post('up_ina'),
				'up_eng' => $this->input->post('up_eng'),
				'di_ina' => $this->input->post('di_ina'),
				'di_eng' => $this->input->post('di_eng'),
				'paragraf_1_ina' => $this->input->post('paragraf_1_ina'),
				'paragraf_1_eng' => $this->input->post('paragraf_1_eng'),
				'paragraf_2_ina' => $this->input->post('paragraf_2_ina'),
				'paragraf_2_eng' => $this->input->post('paragraf_2_eng'),
				'nama_pasien_ina' => $this->input->post('nama_pasien_ina'),
				'nama_pasien_eng' => $this->input->post('nama_pasien_eng'),
				'tl_ina' => $this->input->post('tl_ina'),
				'tl_eng' => $this->input->post('tl_eng'),
				'diagnosa_ina' => $this->input->post('diagnosa_ina'),
				'diagnosa_eng' => $this->input->post('diagnosa_eng'),
				'tindakan_ina' => $this->input->post('tindakan_ina'),
				'tindakan_eng' => $this->input->post('tindakan_eng'),
				'paragraf_3_ina' => $this->input->post('paragraf_3_ina'),
				'paragraf_3_eng' => $this->input->post('paragraf_3_eng'),

			);
			$this->db->update('msurat_template_var_4', $data_var);
		}
		if ($_POST['id']=='5'){
			$data_var=array(
				'paragraf_1_ina' => $this->input->post('paragraf_1_ina'),
				'paragraf_1_eng' => $this->input->post('paragraf_1_eng'),
				'nama_pasien_ina' => $this->input->post('nama_pasien_ina'),
				'nama_pasien_eng' => $this->input->post('nama_pasien_eng'),
				'alamat_ina' => $this->input->post('alamat_ina'),
				'alamat_eng' => $this->input->post('alamat_eng'),
				'pekerjaan_ina' => $this->input->post('pekerjaan_ina'),
				'pekerjaan_eng' => $this->input->post('pekerjaan_eng'),
				'lama_ina' => $this->input->post('lama_ina'),
				'lama_eng' => $this->input->post('lama_eng'),
				'mulai_ina' => $this->input->post('mulai_ina'),
				'mulai_eng' => $this->input->post('mulai_eng'),
				'sampai_ina' => $this->input->post('sampai_ina'),
				'sampai_eng' => $this->input->post('sampai_eng'),
				'paragraf_2_ina' => $this->input->post('paragraf_2_ina'),
				'paragraf_2_eng' => $this->input->post('paragraf_2_eng'),
				'paragraf_3_ina' => $this->input->post('paragraf_3_ina'),
				'paragraf_3_eng' => $this->input->post('paragraf_3_eng'),


			);
			$this->db->update('msurat_template_var_5', $data_var);
		}
		if ($_POST['id']=='6'){
			$data_var=array(
				'kepada_ina' => $this->input->post('kepada_ina'),
				'kepada_eng' => $this->input->post('kepada_eng'),
				'paragraf_1_ina' => $this->input->post('paragraf_1_ina'),
				'paragraf_1_eng' => $this->input->post('paragraf_1_eng'),
				'paragraf_2_ina' => $this->input->post('paragraf_2_ina'),
				'paragraf_2_eng' => $this->input->post('paragraf_2_eng'),
				'nama_pasien_ina' => $this->input->post('nama_pasien_ina'),
				'nama_pasien_eng' => $this->input->post('nama_pasien_eng'),
				'mohon_ina' => $this->input->post('mohon_ina'),
				'mohon_eng' => $this->input->post('mohon_eng'),
				'diagnosa_ina' => $this->input->post('diagnosa_ina'),
				'diagnosa_eng' => $this->input->post('diagnosa_eng'),
				'detail_ina' => $this->input->post('detail_ina'),
				'detail_eng' => $this->input->post('detail_eng'),
				'dokter_ina' => $this->input->post('dokter_ina'),
				'dokter_eng' => $this->input->post('dokter_eng'),



			);
			$this->db->update('msurat_template_var_6', $data_var);
		}
		if ($_POST['id']=='7'){
			$data_var=array(
				'dari_ina' => $this->input->post('dari_ina'),
				'dari_eng' => $this->input->post('dari_eng'),
				'paragraf_1_ina' => $this->input->post('paragraf_1_ina'),
				'paragraf_1_eng' => $this->input->post('paragraf_1_eng'),
				'paragraf_2_ina' => $this->input->post('paragraf_2_ina'),
				'paragraf_2_eng' => $this->input->post('paragraf_2_eng'),
				'nama_pasien_ina' => $this->input->post('nama_pasien_ina'),
				'nama_pasien_eng' => $this->input->post('nama_pasien_eng'),
				'alamat_pasien_ina' => $this->input->post('alamat_pasien_ina'),
				'alamat_pasien_eng' => $this->input->post('alamat_pasien_eng'),
				'diagnosa_ina' => $this->input->post('diagnosa_ina'),
				'diagnosa_eng' => $this->input->post('diagnosa_eng'),
				'tindakan_ina' => $this->input->post('tindakan_ina'),
				'tindakan_eng' => $this->input->post('tindakan_eng'),
				'tanggal_tindakan_ina' => $this->input->post('tanggal_tindakan_ina'),
				'tanggal_tindakan_eng' => $this->input->post('tanggal_tindakan_eng'),
				'paragraf_3_ina' => $this->input->post('paragraf_3_ina'),
				'paragraf_3_eng' => $this->input->post('paragraf_3_eng'),
				'bagian_ina' => $this->input->post('bagian_ina'),
				'bagian_eng' => $this->input->post('bagian_eng'),
				'serta_ina' => $this->input->post('serta_ina'),
				'serta_eng' => $this->input->post('serta_eng'),
				'dibawa_ina' => $this->input->post('dibawa_ina'),
				'dibawa_eng' => $this->input->post('dibawa_eng'),
				'nama_ina' => $this->input->post('nama_ina'),
				'nama_eng' => $this->input->post('nama_eng'),
				'alamat_ina' => $this->input->post('alamat_ina'),
				'alamat_eng' => $this->input->post('alamat_eng'),
				'nik_ina' => $this->input->post('nik_ina'),
				'nik_eng' => $this->input->post('nik_eng'),
				'hubungan_ina' => $this->input->post('hubungan_ina'),
				'hubungan_eng' => $this->input->post('hubungan_eng'),
				'paragraf_4_ina' => $this->input->post('paragraf_4_ina'),
				'paragraf_4_eng' => $this->input->post('paragraf_4_eng'),
				'note_ina' => $this->input->post('note_ina'),
				'note_eng' => $this->input->post('note_eng'),
				'mengetahui_ina' => $this->input->post('mengetahui_ina'),
				'mengetahui_eng' => $this->input->post('mengetahui_eng'),
				'pemohon_ina' => $this->input->post('pemohon_ina'),
				'pemohon_eng' => $this->input->post('pemohon_eng'),
				'saksi_kel_ina' => $this->input->post('saksi_kel_ina'),
				'saksi_kel_eng' => $this->input->post('saksi_kel_eng'),
				'saksi_rs_ina' => $this->input->post('saksi_rs_ina'),
				'saksi_rs_eng' => $this->input->post('saksi_rs_eng'),
			);
			$this->db->update('msurat_template_var_7', $data_var);
		}
		if ($_POST['id']=='8'){
			$data_var=array(
				'paragraf_1_ina' => $this->input->post('paragraf_1_ina'),
				'paragraf_1_eng' => $this->input->post('paragraf_1_eng'),
				'paragraf_2_ina' => $this->input->post('paragraf_2_ina'),
				'paragraf_2_eng' => $this->input->post('paragraf_2_eng'),
				'nama_pasien_ina' => $this->input->post('nama_pasien_ina'),
				'nama_pasien_eng' => $this->input->post('nama_pasien_eng'),
				'alamat_pasien_ina' => $this->input->post('alamat_pasien_ina'),
				'alamat_pasien_eng' => $this->input->post('alamat_pasien_eng'),
				'pelayanan_ina' => $this->input->post('pelayanan_ina'),
				'pelayanan_eng' => $this->input->post('pelayanan_eng'),
				'tanggal_tindakan_ina' => $this->input->post('tanggal_tindakan_ina'),
				'tanggal_tindakan_eng' => $this->input->post('tanggal_tindakan_eng'),
				'paragraf_3_ina' => $this->input->post('paragraf_3_ina'),
				'paragraf_3_eng' => $this->input->post('paragraf_3_eng'),
				'riwayat_ina' => $this->input->post('riwayat_ina'),
				'riwayat_eng' => $this->input->post('riwayat_eng'),
				'penunjang_ina' => $this->input->post('penunjang_ina'),
				'penunjang_eng' => $this->input->post('penunjang_eng'),
				'diagnosa_akhir_ina' => $this->input->post('diagnosa_akhir_ina'),
				'diagnosa_akhir_eng' => $this->input->post('diagnosa_akhir_eng'),
				'terapi_ina' => $this->input->post('terapi_ina'),
				'terapi_eng' => $this->input->post('terapi_eng'),
				'tindakan_ina' => $this->input->post('tindakan_ina'),
				'tindakan_eng' => $this->input->post('tindakan_eng'),
				'keadaan_ina' => $this->input->post('keadaan_ina'),
				'keadaan_eng' => $this->input->post('keadaan_eng'),
				'dokter_ina' => $this->input->post('dokter_ina'),
				'dokter_eng' => $this->input->post('dokter_eng'),
				'paragraf_4_ina' => $this->input->post('paragraf_4_ina'),
				'paragraf_4_eng' => $this->input->post('paragraf_4_eng'),

			);
			$this->db->update('msurat_template_var_8', $data_var);
		}
		if ($_POST['id']=='9'){
			$data_var=array(
				'paragraf_1_ina' => $this->input->post('paragraf_1_ina'),
				'paragraf_1_eng' => $this->input->post('paragraf_1_eng'),
				'paragraf_2_ina' => $this->input->post('paragraf_2_ina'),
				'paragraf_2_eng' => $this->input->post('paragraf_2_eng'),
				'nama_pasien_ina' => $this->input->post('nama_pasien_ina'),
				'nama_pasien_eng' => $this->input->post('nama_pasien_eng'),
				'alamat_pasien_ina' => $this->input->post('alamat_pasien_ina'),
				'alamat_pasien_eng' => $this->input->post('alamat_pasien_eng'),
				'pekerjaan_pasien_ina' => $this->input->post('pekerjaan_pasien_ina'),
				'pekerjaan_pasien_eng' => $this->input->post('pekerjaan_pasien_eng'),
				'oleh_ina' => $this->input->post('oleh_ina'),
				'oleh_eng' => $this->input->post('oleh_eng'),
				'tanggal_tindakan_ina' => $this->input->post('tanggal_tindakan_ina'),
				'tanggal_tindakan_eng' => $this->input->post('tanggal_tindakan_eng'),
				'paragraf_3_ina' => $this->input->post('paragraf_3_ina'),
				'paragraf_3_eng' => $this->input->post('paragraf_3_eng'),
				'paragraf_4_ina' => $this->input->post('paragraf_4_ina'),
				'paragraf_4_eng' => $this->input->post('paragraf_4_eng'),
				'dokter_ina' => $this->input->post('dokter_ina'),
				'dokter_eng' => $this->input->post('dokter_eng'),
			);
			$this->db->update('msurat_template_var_9', $data_var);
		}
		
			
        if ($this->db->update('msurat_template', $this, array('id' => $_POST['id']))) {
			
			
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->staktif = 0;
		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('msurat_template', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	function refresh_image($id){
		$q="SELECT  H.* from msurat_template_dokumen H

			WHERE H.idtransaksi='$id'";
		// print_r($q);exit();
		$row= $this->db->query($q)->result();
		$tabel='';
		$no=1;
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/msurat_template_dokumen/'.$r->filename.'" target="_blank">'.$r->filename.'</a></td>';
			$tabel .='<td class="text-left">'.$r->keterangan.'</td>';
			$tabel .='<td class="text-left">'.$r->upload_by_nama.'-'.HumanDateLong($r->upload_date).'</td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/msurat_template_dokumen/'.$r->filename.'" target="_blank" data-toggle="tooltip" title="Preview" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                    <button title="Hapus" class="btn btn-danger btn-sm" onclick="hapus_file('.$r->id.')"><i class="fa fa-trash-o"></i></a></td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		return $tabel;
	}
}
