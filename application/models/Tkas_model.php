<?php

class Tkas_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getNoTransaksi()
    {
        $this->db->like('notransaksi', 'TKS'.date('Y').date('m'), 'after');
        $this->db->from('tbendahara_kas');
        $query = $this->db->count_all_results();

        if ($query > 0) {
            $autono = $query + 1;
            $autono = "TKS".date('Y').date('m').str_pad($autono, 4, '0', STR_PAD_LEFT);
        } else {
            $autono = "TKS".date('Y').date('m')."0001";
        }

        return $autono;
    }

    public function getListAkunDebit()
    {
        $this->db->order_by('noakun', 'ASC');
        $this->db->where('noakun NOT LIKE ', '%11.10.%');
        $this->db->or_where('noakun NOT LIKE ', '%11.20.%');
        $this->db->or_where('noakun <>', '21.10.04');
        $this->db->where('status', '1');
        $query = $this->db->get('makun_nomor');
        return $query->result();
    }

    public function getListAkunKredit()
    {
        $this->db->order_by('noakun', 'ASC');
        $this->db->where('noakun LIKE ', '%11.10.%');
        $this->db->or_where('noakun LIKE ', '%11.20.%');
        $this->db->or_where('noakun ', '21.10.04');
        $this->db->where('status', '1');
        $query = $this->db->get('makun_nomor');
        return $query->result();
    }

    public function getDataPencairan($idtipe, $idtransaksi)
    {
        if ($idtipe == 'pengajuan') {
            $this->db->select('tpencairan_detail.id,
              tpengajuan.nopengajuan AS notransaksi,
              tpengajuan.subjek AS namatransaksi,
              tpengajuan.totalnominal AS nominaltransaksi');
            $this->db->join('tpengajuan', 'tpengajuan.id = tpencairan_detail.idtransaksi');
        } elseif ($idtipe == 'kontrabon') {
            $this->db->select('tpencairan_detail.id,
              tkontrabon.nokontrabon AS notransaksi,
              "-" AS namatransaksi,
              tkontrabon.grandtotalnominal AS nominaltransaksi');
            $this->db->join('tkontrabon', 'tkontrabon.id = tpencairan_detail.idtransaksi');
        } elseif ($idtipe == 'carm') {
            $this->db->select('tpencairan_detail.id,
              tcarm_payments.no_transaksi AS notransaksi,
              tcarm_payments.nama_transaksi AS namatransaksi,
              SUM(tcarm_payments_detail.total) AS nominaltransaksi');
            $this->db->join('tcarm_payments_detail', 'tcarm_payments_detail.id = tpencairan_detail.idtransaksi');
            $this->db->join('tcarm_payments', 'tcarm_payments.id = tcarm_payments_detail.id_transaksi');
        } elseif ($idtipe == 'honordokter') {
            $this->db->select('tpencairan_detail.id,
              "-" AS notransaksi,
              mdokter.nama AS namatransaksi,
              thonor_dokter.nominal AS nominaltransaksi');
            $this->db->join('thonor_dokter', 'thonor_dokter.id = tpencairan_detail.idtransaksi');
            $this->db->join('mdokter', 'mdokter.id = thonor_dokter.iddokter');
        } elseif ($idtipe == 'kasbon') {
            $this->db->select('tpencairan_detail.id,
              "-" AS notransaksi,
              tkasbon.catatan AS namatransaksi,
              tkasbon.nominal AS nominaltransaksi');
            $this->db->join('tkasbon', 'tkasbon.id = tpencairan_detail.idtransaksi');
        } elseif ($idtipe == 'gajikaryawan') {
            $this->db->select('tpencairan_detail.id,
              trekap_penggajian.notransaksi AS notransaksi,
              (
                CASE WHEN trekap_penggajian_detail.idsub != 0 THEN
                  mvariable_rekapan_sub.nama
                ELSE
                  mvariable_rekapan.nama
                END
              ) AS namatransaksi,
              trekap_penggajian_detail.nominal AS nominaltransaksi');
            $this->db->join('trekap_penggajian_detail', 'trekap_penggajian_detail.id = tpencairan_detail.idtransaksi');
            $this->db->join('trekap_penggajian', 'trekap_penggajian.id = trekap_penggajian_detail.idrekap');
            $this->db->join('mvariable_rekapan', 'mvariable_rekapan.id = trekap_penggajian_detail.idvariable', 'LEFT');
            $this->db->join('mvariable_rekapan_sub', 'mvariable_rekapan_sub.id = trekap_penggajian_detail.idsub', 'LEFT');
        } elseif ($idtipe == 'feeklinik') {
            $this->db->select('tpencairan_detail.id,
              "-" AS notransaksi,
              mrumahsakit.nama AS namatransaksi,
              tverifikasi_rujukan_klinik_fee.total AS nominaltransaksi');
            $this->db->join('tverifikasi_rujukan_klinik_fee', 'tverifikasi_rujukan_klinik_fee.id = tpencairan_detail.idtransaksi');
            $this->db->join('mrumahsakit', 'mrumahsakit.id = tverifikasi_rujukan_klinik_fee.idrumahsakit');
        }

        $this->db->where('tpencairan_detail.idtipe', $idtipe);
        $this->db->where('tpencairan_detail.idtransaksi', $idtransaksi);
        $query = $this->db->get('tpencairan_detail');
        return $query->row();
    }

    public function saveData()
    {
        $this->notransaksi       = $this->getNoTransaksi();
        $this->iddetailpencairan = $this->input->post('iddetailpencairan');
        $this->tanggal           = YMDFormat($_POST['tanggal']);

        if ($this->db->insert('tbendahara_kas', $this)) {
            $idtransaksi = $this->db->insert_id();
            $debitList = json_decode($_POST['debitvalue']);
            $kreditList = json_decode($_POST['kreditvalue']);

            $nourut = 0;
            $totaldebit = 0;
            $totalkredit = 0;

            foreach ($debitList as $row) {
                $nourut = $nourut + 1;

                $detail = array();
                $detail['idtransaksi']      = $idtransaksi;
                $detail['biaya']            = $row[1];
                $detail['noakun']           = $row[2];
                $detail['keterangan']       = $row[3];
                $detail['nominal']          = RemoveComma($row[4]);
                $detail['nourut']           = $nourut;

                $this->db->replace('tbendahara_kas_debit', $detail);

                $totaldebit = $totaldebit + RemoveComma($row[4]);
            }

            foreach ($kreditList as $row) {
                $nourut = $nourut + 1;

                $detail = array();
                $detail['idtransaksi']      = $idtransaksi;
                $detail['idtipe']           = $row[1];
                $detail['noakun']           = $row[5];
                $detail['idmetode']         = $row[6];
                $detail['keterangan']       = $row[8];
                $detail['nominal']          = RemoveComma($row[9]);
                $detail['nourut']           = $nourut;

                $this->db->replace('tbendahara_kas_kredit', $detail);

                $totalkredit = $totalkredit + RemoveComma($row[9]);
            }

            $this->db->set('debit', $totaldebit);
            $this->db->set('kredit', $totalkredit);
            $this->db->where('id', $idtransaksi);
            $this->db->update('tbendahara_kas');

            return $idtransaksi;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
