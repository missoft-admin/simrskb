<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mvariable_hold_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mvariable_hold');
        return $query->row();
    }

    public function saveData()
    {
        $this->nama        = $_POST['nama'];
        $this->deskripsi   = $_POST['deskripsi'];

        if ($this->db->insert('mvariable_hold', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama        = $_POST['nama'];
        $this->deskripsi   = $_POST['deskripsi'];

        if ($this->db->update('mvariable_hold', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('mvariable_hold', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
