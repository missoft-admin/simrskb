<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tbuku_besar_piutang_detail_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function getKP($idkelompokpasien){
		$q="SELECT GROUP_CONCAT(H.nama) as kp FROM mpasien_kelompok H
			WHERE H.id IN (".$idkelompokpasien.")";
		return $this->db->query($q)->row('kp');
			
	}
	function getRekanan($idrekanan){
		$q="SELECT GROUP_CONCAT(H.nama) as kp FROM mrekanan H
			WHERE H.id IN (".$idrekanan.")";
		return $this->db->query($q)->row('kp');
			
	}
	function tipe_pemesanan_biasa($tipe){
		if ($tipe=='1'){
			$tipe='PIUTANG';
		}
		if ($tipe=='2'){
			$tipe='PEMBAYARAN';
			
		}
		if ($tipe=='3'){
			$tipe='OTHER LOSS';
		}
		if ($tipe=='4'){
			$tipe='PEMBAYARAN';
		}
		return $tipe;
	}
}
