<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mtarif_administrasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllParent($idtipe)
    {
        $this->db->where('idtipe', $idtipe);
        $this->db->where('status', '1');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('mtarif_administrasi');
        return $query->result();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mtarif_administrasi');
        return $query->row();
    }

    public function saveData()
    {
        $this->idtipe          = $_POST['idtipe'];
        $this->idjenis         = $_POST['idjenis'];
        $this->nama            = $_POST['nama'];
        $this->jasasarana      = RemoveComma($_POST['jasasarana']);
        $this->jasapelayanan   = RemoveComma($_POST['jasapelayanan']);
        $this->bhp             = RemoveComma($_POST['bhp']);
        $this->biayaperawatan  = RemoveComma($_POST['biayaperawatan']);
        $this->total           = RemoveComma($_POST['total']);
        $this->mintarif        = RemoveComma($_POST['mintarif']);
        $this->maxtarif        = RemoveComma($_POST['maxtarif']);
        $this->persentasetarif = RemoveComma($_POST['persentasetarif']);
        $this->status          = 1;
    		$this->created_by  = $this->session->userdata('user_id');
    		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mtarif_administrasi', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->idtipe          = $_POST['idtipe'];
        $this->idjenis         = $_POST['idjenis'];
        $this->nama            = $_POST['nama'];
        $this->jasasarana      = RemoveComma($_POST['jasasarana']);
        $this->jasapelayanan   = RemoveComma($_POST['jasapelayanan']);
        $this->bhp             = RemoveComma($_POST['bhp']);
        $this->biayaperawatan  = RemoveComma($_POST['biayaperawatan']);
        $this->total           = RemoveComma($_POST['total']);
        $this->mintarif        = RemoveComma($_POST['mintarif']);
        $this->maxtarif        = RemoveComma($_POST['maxtarif']);
        $this->persentasetarif = RemoveComma($_POST['persentasetarif']);
        $this->status          = 1;
    		$this->edited_by  = $this->session->userdata('user_id');
    		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mtarif_administrasi', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
        return $status;
    }

    public function softDelete($id)
    {
        $this->status = 0;
    		$this->deleted_by  = $this->session->userdata('user_id');
    		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mtarif_administrasi', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function getGroupPembayaran()
    {
        $this->db->where('status', 1);
        $query = $this->db->get('mgroup_pembayaran');
        return $query->result();
    }

    public function updateSetting()
    {
        $this->group_jasasarana      = RemoveComma($_POST['group_jasasarana']);
        $this->group_jasapelayanan   = RemoveComma($_POST['group_jasapelayanan']);
        $this->group_bhp             = RemoveComma($_POST['group_bhp']);
        $this->group_biayaperawatan  = RemoveComma($_POST['group_biayaperawatan']);
		
		$this->group_jasasarana_diskon      = RemoveComma($_POST['group_jasasarana_diskon']);
        $this->group_jasapelayanan_diskon   = RemoveComma($_POST['group_jasapelayanan_diskon']);
        $this->group_bhp_diskon             = RemoveComma($_POST['group_bhp_diskon']);
        $this->group_biayaperawatan_diskon  = RemoveComma($_POST['group_biayaperawatan_diskon']);
        $this->group_diskon_all  = RemoveComma($_POST['group_diskon_all']);

        if ($this->db->update('mtarif_administrasi', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
        return $status;
    }
}
