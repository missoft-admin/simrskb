<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Medc_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getSpecified($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('medc');
        return $query->row();
    }
	public function list_user($id){
		$q="SELECT M.id,M.`name`,CASE WHEN U.userid IS NOT NULL THEN 'selected' ELSE '' END as selected  
			FROM musers M 
			LEFT JOIN medc_user U ON U.userid=M.id AND U.idedc='$id'";
		return $this->db->query($q)->result();
	}
	
    public function saveData() {
        $this->nama       = $_POST['nama'];
        $this->lokasi  = $_POST['lokasi'];
        $this->bank_id  = $_POST['bank_id'];
        $this->deskripsi  = $_POST['deskripsi'];
        $this->created_by  = $this->session->userdata('user_id');
        $this->created_date  = date('Y-m-d H:i:s');
		$this->db->insert('medc', $this);
		$id=$this->db->insert_id();
        if ($id) {
			$userid=$_POST['userid'];
			foreach($userid as $r) {
				$this->db->set('idedc',$id);
				$this->db->set('userid',$r);
				$this->db->insert('medc_user');
			}
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	
    public function updateData() {
        $this->nama       = $_POST['nama'];
        $this->lokasi  = $_POST['lokasi'];
        $this->bank_id  = $_POST['bank_id'];
        $this->deskripsi  = $_POST['deskripsi'];

        $this->edited_by  = $this->session->userdata('user_id');
        $this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('medc', $this, ['id' => $_POST['id']])) {
			$this->db->where('idedc', $_POST['id']);
            $this->db->delete('medc_user');
            $userid=$_POST['userid'];
			foreach($userid as $r) {
				$this->db->set('idedc',$_POST['id']);
				$this->db->set('userid',$r);
				$this->db->insert('medc_user');
			}
            
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id) {
        $this->status = 0;

        $this->deleted_by  = $this->session->userdata('user_id');
        $this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('medc', $this, ['id' => $id])) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id) {
        $this->status = 1;      

        if ($this->db->update('medc', $this, ['id' => $id])) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    
}
