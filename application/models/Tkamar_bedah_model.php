<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tkamar_bedah_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function find_pasien($id){
		$q="SELECT H.id,H.idkelompokpasien,H.idrekanan,H.diagnosa,
			CASE WHEN H.idtipe='1' THEN '1' ELSE '2' END as tipe,H.idtipe,
			CASE WHEN H.idtipe='1' THEN H.id ELSE P.id END as idpendaftaran,
			CASE WHEN H.idtipe='1' THEN 1 ELSE 2 END as idasalpendaftaran,H.idkelas,
			H.no_medrec,H.namapasien,DATE_FORMAT(H.tanggal_lahir,'%d-%m-%Y') as tanggal_lahir,H.idpasien,H.umurhari,H.umurbulan,H.umurtahun
			,R.nama as ruang,K.nama as kelas,B.nama as bed,D.nama as dpjp,P.iddokter,rujuk.nama as perujuk,Kp.nama as kelompokpasien,Rek.nama as asuransi,H.iddokterpenanggungjawab
			from trawatinap_pendaftaran H
			LEFT JOIN mruangan R ON R.id=H.idruangan
			LEFT JOIN mkelas K ON K.id=H.idkelas
			LEFT JOIN mbed B ON B.id=H.idbed
			LEFT JOIN mdokter D ON D.id=H.iddokterpenanggungjawab
			LEFT JOIN tpoliklinik_pendaftaran P ON P.id=H.idpoliklinik
			LEFT JOIN mpasien_kelompok Kp ON Kp.id=H.idkelompokpasien
			LEFT JOIN mdokter rujuk ON rujuk.id=P.iddokter
			LEFT JOIN mrekanan Rek On Rek.id=H.idrekanan
			WHERE H.id='$id'";
		// print_r($q);exit;
		$query=$this->db->query($q);
		return $query->row_array();
	}
		function list_dokter(){
		$q="SELECT MP.id,MP.nama FROM `mjadwal_dokter` H
			LEFT JOIN mdokter MP ON MP.id=H.iddokter
			WHERE MP.`status`='1'
			GROUP BY H.iddokter";
		return $this->db->query($q)->result();
	}
	public function get_edit($id){
		$q="SELECT H.id, H.idpendaftaran,H.idasalpendaftaran,H.idpasien
			,CASE WHEN RI.idtipe IS null THEN 'ODS' WHEN RI.idtipe='1' THEN 'RAWAT INAP' WHEN RI.idtipe='2' THEN 'ODS' END as tipe_nama
			,H.jenis_bedah,H.tarif_jenis_opr,H.statuskasir,H.statussetuju,H.statusdatang,H.tanggal_trx,RB.created_ppa,RB.created_date,H.st_ranap,
			H.waktumulaioperasi,H.waktuselesaioperasi,
			H.tipe as tipe,
			CASE WHEN H.st_ranap='0' THEN TP.namapasien ELSE RI.namapasien END as namapasien,
			CASE WHEN H.st_ranap='0' THEN TP.no_medrec ELSE RI.no_medrec END as no_medrec,
			CASE WHEN H.st_ranap='0' THEN DATE_FORMAT(TP.tanggal_lahir,'%d-%m-%Y') ELSE DATE_FORMAT(RI.tanggal_lahir,'%d-%m-%Y') END as tanggal_lahir,
			CASE WHEN H.st_ranap='0' THEN TP.umurtahun ELSE RI.umurtahun END as umurtahun,
			CASE WHEN H.st_ranap='0' THEN TP.umurbulan ELSE RI.umurbulan END as umurbulan,
			CASE WHEN H.st_ranap='0' THEN TP.umurhari ELSE RI.umurhari END as umurhari,
			CASE WHEN H.st_ranap='0' THEN RP.nama ELSE RR.nama END as asuransi,H.catatan,RB.nopermintaan,
			CASE WHEN H.st_ranap='0' THEN TP.idkelompokpasien ELSE RI.idkelompokpasien END as idkelompokpasien,
			CASE WHEN H.st_ranap='0' THEN P.nama ELSE PR.nama END as kelompokpasien,
			K.nama as kelas,B.nama as bed,
			RI.id as ri_id,TP.id as poli_id,
			COALESCE(H.dpjp_id,'0') as dpjp,DATE_FORMAT(H.tanggaloperasi,'%d-%m-%Y') as tanggaloperasi,H.diagnosa,H.operasi,
			COALESCE(H.jenis_operasi_id,'0') as jenis_operasi_id,
			COALESCE(H.jenis_anestesi_id,'0') as jenis_anestesi_id,
			COALESCE(kelompok_operasi_id,'0') as kelompok_operasi_id,
			COALESCE(kelompok_tindakan_id,'0') as kelompok_tindakan_id,
			COALESCE(kelas_tarif_id,'0') as kelas_tarif_id,
			COALESCE(ruang_id,'0') as ruang_id,
			COALESCE(kelompk_diagnosa_id,'0') as kelompk_diagnosa_id,
			H.namapetugas,
			H.tanggal_diajukan,H.idapprove,U.`name` as user_setuju,H.tanggal_disetujui,H.status
			,MD.nama as dokter_perujuk
			from tkamaroperasi_pendaftaran H
			LEFT JOIN tpoliklinik_ranap_perencanaan_bedah RB ON RB.pendaftaran_bedah_id=H.id
			LEFT JOIN musers U ON U.id=H.idapprove
			LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.idpendaftaran
			LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpendaftaran
			LEFT JOIN mpasien_kelompok P ON P.id=TP.idkelompokpasien
			LEFT JOIN mpasien_kelompok PR ON PR.id=RI.idkelompokpasien
			LEFT JOIN mrekanan RP ON RP.id=TP.idrekanan
			LEFT JOIN mrekanan RR ON RR.id=RI.idrekanan
			LEFT JOIN mkelas K ON K.id=RI.idkelas
			LEFT JOIN mbed B ON B.id=RI.idbed
			LEFT JOIN mppa MD ON MD.id=RB.iddokter_peminta
			WHERE H.id='$id'";
			// print_r($q);exit;
		$query=$this->db->query($q);
		return $query->row_array();
	}
	function get_data_bedah($id){
		$q="SELECT H.id as pendaftaran_bedah_id
			,RB.iddokter_peminta,MD.nama as nama_pengaju,RB.tanggal_input as tanggal_pengajuan 
			,H.*
			FROM tkamaroperasi_pendaftaran H
			LEFT JOIN tpoliklinik_ranap_perencanaan_bedah RB ON RB.pendaftaran_bedah_id=H.id
			LEFT JOIN mppa MD ON MD.id=RB.iddokter_peminta
			WHERE H.id='$id'";
		return $this->db->query($q)->row_array();
	}
	public function kelompok_tindakan_list($id){
		$q="SELECT T.id,T.nama as nama, CASE WHEN  H.id IS NULL THEN '' ELSE 'selected' END as selected from mkelompok_tindakan T
			LEFT JOIN tkamaroperasi_tindakan H ON H.kelompok_tindakan_id=T.id AND H.idpendaftaranoperasi='$id'
			WHERE T.`status`='1' ORDER BY H.id";
		$query=$this->db->query($q);
		$result= $query->result();
		if ($result){
			return $result;
		}else{
			return '';
		}
	}
	public function kelompok_diagnosa_list($id){
		$q="SELECT T.id,T.nama as nama, CASE WHEN  H.id IS NULL THEN '' ELSE 'selected' END as selected from mkelompok_diagnosa T
			LEFT JOIN tkamaroperasi_diagnosa H ON H.kelompok_diagnosa_id=T.id AND H.idpendaftaranoperasi='$id'
			WHERE T.`status`='1' ORDER BY H.id";
		$query=$this->db->query($q);
		$result= $query->result();
		if ($result){
			return $result;
		}else{
			return '';
		}
	}
	public function tind_konsulen($id){
		$q="SELECT iddokter from tkamaroperasi_konsulen H
			WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result= $query->result();
		$array=array();
		foreach ($result as $row){
			$array[]=$row->iddokter;
		}

		return $array;

	}
	public function tind_instrumen($id){
		$q="SELECT idpegawai from tkamaroperasi_instrumen H
			WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result= $query->result();
		$array=array();
		foreach ($result as $row){
			$array[]=$row->idpegawai;
		}

		return $array;

	}
	public function tind_sirkuler($id){
		$q="SELECT idpegawai from tkamaroperasi_sirkuler H
			WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result= $query->result();
		$array=array();
		foreach ($result as $row){
			$array[]=$row->idpegawai;
		}

		return $array;

	}
	public function list_instrumen(){
		$q="SELECT *from (SELECT P.id,P.nama From mpegawai P
WHERE P.idkategori IN (2,4)) TBL";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function load_tarif_ruangan($jenis_operasi_id,$kelas_tarif_id){
		$q="SELECT H.id,H.nama as nama_tarif,COALESCE(D.jasasarana,0) as jasasarana,COALESCE(D.jasapelayanan,0) as jasapelayanan,COALESCE(D.bhp,0) as bhp,
		COALESCE(D.biayaperawatan,0) as biayaperawatan,D.total,J.nama as jenis_operasi
		from mtarif_operasi H
			LEFT JOIN mtarif_operasi_detail D ON D.idtarif=H.id AND D.kelas='$kelas_tarif_id'
			LEFT JOIN mjenis_operasi J ON J.id=H.idjenis
			WHERE H.idtipe='1' AND H.idjenis='$jenis_operasi_id' AND D.`status`='1' AND H.`status`='1'";
		// print_r($q);exit;
		$query=$this->db->query($q);
		$result= $query->row();
		if ($result){
			return $result;

		}else{
			return 'error';
		}
	}
	public function auto_insert_sewa_ruangan($id){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT COUNT(H.id) as jml_trx FROM tkamaroperasi_ruangan H WHERE H.idpendaftaranoperasi='$id' AND H.status='1'";
		$data=$this->db->query($q)->row('jml_trx');
		
		if ($data<1){
			$q="
				INSERT INTO tkamaroperasi_ruangan 
				(idpendaftaranoperasi,id_tarif,kelas_tarif,nama_tarif,statusverif,status,tanggal_transaksi,iduser_transaksi,jasasarana,jasasarana_disc,jasapelayanan,jasapelayanan_disc,bhp,bhp_disc,biayaperawatan,biayaperawatan_disc,total,diskon,totalkeseluruhan,jenis_operasi_id)

				SELECT H.id as idpendaftaranoperasi
				,T.id as id_tarif,TD.kelas as kelas_tarif,T.nama,0 as statusverif,1 as `status`
				,NOW() as tanggal_transaksi,'$user_id' as iduser_transaksi,TD.jasasarana,0 as jasasarana_disc
				,COALESCE(TD.jasapelayanan,0) as jasapelayanan
				,0 as jasapelayanan_disc
				,COALESCE(TD.bhp,0) as bhp
				,0 as bhp_disc
				,COALESCE(TD.biayaperawatan,0) as biayaperawatan
				,0 as biayaperawatan_disc
				,COALESCE(TD.total,0) as total
				,0 as diskon
				,COALESCE(TD.total,0) as totalkeseluruhan,H.jenis_operasi_id
				FROM tkamaroperasi_pendaftaran H
				INNER JOIN mtarif_operasi T ON T.idjenis=H.tarif_jenis_opr
				INNER JOIN mtarif_operasi_detail TD ON TD.idtarif=T.id AND TD.kelas=H.kelas_tarif_id
				WHERE H.id='$id' AND T.idtipe='1' 
			";
			$this->db->query($q);
		}
		return true;
	}
	public function auto_insert_sewa_ruangan_fc($id,$data){
		// print_r($data['idkelompokpasien']);exit;
		$user_id=$this->session->userdata('user_id');
		$q="SELECT COUNT(H.id) as jml_trx FROM tkamaroperasi_fullcare H WHERE H.idpendaftaran='$id' AND H.status='1'";
		$data_trx=$this->db->query($q)->row('jml_trx');
		
		if ($data_trx<1){
			$idkelompokpasien=$data['idkelompokpasien'];
			// print_r($idkelompokpasien);exit;
			$idrekanan=($idkelompokpasien!='1'?'0':$data['idrekanan']);
			$kelompok_opr=$data['kelompok_operasi_id'];
			$jenis_opr=$data['jenis_operasi_id'];
			$jenis_bedah=$data['jenis_bedah'];
			$jenis_anestesi=$data['jenis_anestesi_id'];
			$kelas_tarif_id=$data['kelas_tarif_id'];
		$q="
			SELECT H.tarif_jenis_opr,H.taif_full_care,H.tarif_sewa_alat  FROM setting_tko_tarif_bedah H INNER JOIN (
			SELECT 
			compare_value_11(
				MAX(IF(H.kelompok_opr = '$kelompok_opr' AND H.jenis_opr='$jenis_opr' AND H.jenis_bedah='$jenis_bedah' AND H.jenis_anestesi='$jenis_anestesi',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '$kelompok_opr' AND H.jenis_opr='$jenis_opr' AND H.jenis_bedah='$jenis_bedah' AND H.jenis_anestesi='0',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '$kelompok_opr' AND H.jenis_opr='$jenis_opr' AND H.jenis_bedah='0' AND H.jenis_anestesi='0',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '$kelompok_opr' AND H.jenis_opr='0' AND H.jenis_bedah='0' AND H.jenis_anestesi='0',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '0' AND H.jenis_opr='$jenis_opr' AND H.jenis_bedah='$jenis_bedah' AND H.jenis_anestesi='0',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '0' AND H.jenis_opr='$jenis_opr' AND H.jenis_bedah='0' AND H.jenis_anestesi='0',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '0' AND H.jenis_opr='0' AND H.jenis_bedah='$jenis_bedah' AND H.jenis_anestesi='$jenis_anestesi',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '0' AND H.jenis_opr='0' AND H.jenis_bedah='$jenis_bedah' AND H.jenis_anestesi='$jenis_anestesi',H.id,NULL))
				,MAX(IF(H.kelompok_opr = '0' AND H.jenis_opr='0' AND H.jenis_bedah='0' AND H.jenis_anestesi='$jenis_anestesi',H.id,NULL))
				,MAX(IF((H.kelompok_opr = '$kelompok_opr' OR H.kelompok_opr = '0') AND (H.jenis_opr='$jenis_opr' OR H.jenis_opr='0') AND (H.jenis_bedah='$jenis_bedah' OR H.jenis_bedah='0') AND (H.jenis_anestesi='$jenis_anestesi' OR H.jenis_anestesi='0'),H.id,NULL))
				,MAX(IF(H.kelompok_opr = '0' AND H.jenis_opr='0' AND H.jenis_bedah='0' AND H.jenis_anestesi='0',H.id,NULL))
			) id
			FROM setting_tko_tarif_bedah H

			WHERE H.idkelompokpasien='$idkelompokpasien' AND (H.idrekanan='$idrekanan' OR H.idrekanan='0')
			) S ON S.id=H.id
			LIMIT 1
		   ";
		   // print_r($q);exit;
		
		$idtarif=$this->db->query($q)->row('taif_full_care');
		
			$q="
				INSERT INTO tkamaroperasi_fullcare 
				(idpendaftaran,idtarif,kelas_tarif,namatarif,statusverifikasi,status
				,jasasarana,jasasarana_disc,jasapelayanan,jasapelayanan_disc,bhp,bhp_disc,biayaperawatan,biayaperawatan_disc,total,diskon,totalkeseluruhan,jenis_operasi_id
				,kuantitas
				)

				SELECT H.id as idpendaftaran
				,T.id as idtarif,TD.kelas as kelas_tarif,T.nama,0 as statusverif,1 as `status`
				,TD.jasasarana,0 as jasasarana_disc
				,COALESCE(TD.jasapelayanan,0) as jasapelayanan
				,0 as jasapelayanan_disc
				,COALESCE(TD.bhp,0) as bhp
				,0 as bhp_disc
				,COALESCE(TD.biayaperawatan,0) as biayaperawatan
				,0 as biayaperawatan_disc
				,COALESCE(TD.total,0) as total
				,0 as diskon
				,COALESCE(TD.total,0) as totalkeseluruhan,H.jenis_operasi_id,1 as kuantitas
				FROM tkamaroperasi_pendaftaran H
				INNER JOIN mtarif_rawatinap T ON T.id='$idtarif'
				INNER JOIN mtarif_rawatinap_detail TD ON TD.idtarif=T.id AND TD.kelas=H.kelas_tarif_id
				WHERE H.id='$id' AND T.id='$idtarif' 
			";
			$this->db->query($q);
		}
		return true;
	}
	public function load_tarif_do($jenis_operasi_id,$kelas_tarif_id){
		$q="SELECT H.id,H.nama as nama_tarif,D.jasasarana,D.jasapelayanan,D.bhp,
		D.biayaperawatan,D.total,J.nama as jenis_operasi
		from mtarif_operasi H
			LEFT JOIN mtarif_operasi_detail D ON D.idtarif=H.id AND D.kelas='$kelas_tarif_id'
			LEFT JOIN mjenis_operasi J ON J.id=H.idjenis
			WHERE H.idtipe='3' AND H.idjenis='$jenis_operasi_id' AND D.`status`='1' AND H.`status`='1'";
		$query=$this->db->query($q);
		$result= $query->row();
		if ($result){
			return $result;

		}else{
			return 'error';
		}
	}
	public function getListUnitPelayanan()
	{
		$this->db->select('munitpelayanan.id, munitpelayanan.nama');
		$this->db->where('munitpelayanan_user.userid', $this->session->userdata('user_id'));
		$this->db->join('munitpelayanan', 'munitpelayanan_user.idunitpelayanan = munitpelayanan.id', 'left');
		$this->db->from('munitpelayanan_user');
		$query = $this->db->get();
		return $query->result();
	}
	public function getDefaultUnitPelayananUser()
	{
		$this->db->select('unitpelayananiddefault as idunitpelayanan');
		$this->db->from('musers');
		$this->db->where('id', $this->session->userdata('user_id'));
		$query = $this->db->get();
		return $query->row('idunitpelayanan');
	}
	function get_data_paket($tipe_form){
		$iduser=$this->session->userdata('user_id');
		$q="SELECT H.id as paket_id,H.nama_paket,H.status_paket,H.st_edited FROM mpaket_bedah H WHERE H.tipe_form='$tipe_form' AND created_by='$iduser' AND status_paket='1'";
		$data=$this->db->query($q)->row_array();
		// print_r();exit;
		return $data;
	}
	public function getListTipe()
	{
		$iduser=$this->session->userdata('user_id');
		$q="SELECT H.idtipe as id,M.nama_tipe  as nama
		FROM musers_tipebarang H 
		INNER JOIN mdata_tipebarang M ON M.id=H.idtipe
		WHERE H.iduser='$iduser'";
		$query=$this->db->query($q);
		return $query->result();
	}
	function update_persen_da($idpendaftaranoperasi,$total_do,$persen_da){
		$total_acuan_da=$total_do * $persen_da/100;
		$q="SELECT *FROM tkamaroperasi_jasada WHERE idpendaftaranoperasi='$idpendaftaranoperasi' AND status='1'";
		$list=$this->db->query($q)->result();
		$total_all=0;
		foreach($list as $row){
			$total_before_diskon=$total_acuan_da * $row->persen / 100;
			$diskon_persen=0;
			if ($row->diskon>0){
				$diskon_persen=$row->diskon/$row->total_before_diskon*100;
			}
			$diskon=$total_before_diskon * $diskon_persen/100;
			$totalkeseluruhan=$total_before_diskon-$diskon;
			$total_all=$total_all + $totalkeseluruhan;
			$data=array(
				'total_acuan' => $total_acuan_da,
				'total_before_diskon' => $total_before_diskon,
				'diskon' => $diskon,
				'totalkeseluruhan' => $total_before_diskon-$diskon,
			);
			$this->db->where('id',$row->id);
			$this->db->update('tkamaroperasi_jasada',$data);
		}
		
		$q="UPDATE tkamaroperasi_pendaftaran set persen_da='$persen_da',total_acuan_da='$total_acuan_da',total_da='$total_all' WHERE id='$idpendaftaranoperasi'";
		$hasil=$this->db->query($q);
		
		$q="SELECT *FROM tkamaroperasi_pendaftaran WHERE id='$idpendaftaranoperasi'";
		$row=$this->db->query($q)->row();
		$this->update_persen_daa($idpendaftaranoperasi,$total_all,$row->persen_daa);
		
		$this->update_persen_dao($idpendaftaranoperasi,$total_all,$row->persen_dao);
		
		return $hasil;
	}
	function update_persen_daa($idpendaftaranoperasi,$total_do,$persen_da){
		$total_acuan_da=$total_do * $persen_da/100;
		$q="SELECT *FROM tkamaroperasi_jasadaa WHERE idpendaftaranoperasi='$idpendaftaranoperasi' AND status='1'";
		$list=$this->db->query($q)->result();
		$total_all=0;
		foreach($list as $row){
			$total_before_diskon=$total_acuan_da * $row->persen / 100;
			$diskon_persen=0;
			if ($row->diskon>0){
				$diskon_persen=$row->diskon/$row->total_before_diskon*100;
			}
			$diskon=$total_before_diskon * $diskon_persen/100;
			$totalkeseluruhan=$total_before_diskon-$diskon;
			$total_all=$total_all + $totalkeseluruhan;
			$data=array(
				'total_acuan' => $total_acuan_da,
				'total_before_diskon' => $total_before_diskon,
				'diskon' => $diskon,
				'totalkeseluruhan' => $total_before_diskon-$diskon,
			);
			$this->db->where('id',$row->id);
			$this->db->update('tkamaroperasi_jasadaa',$data);
		}
		
		$q="UPDATE tkamaroperasi_pendaftaran set persen_daa='$persen_da',total_acuan_daa='$total_acuan_da',total_daa='$total_all' WHERE id='$idpendaftaranoperasi'";
		$hasil=$this->db->query($q);
		return $hasil;
	}
	function update_persen_dao($idpendaftaranoperasi,$total_do,$persen_da){
		$total_acuan_da=$total_do * $persen_da/100;
		$q="SELECT *FROM tkamaroperasi_jasaao WHERE idpendaftaranoperasi='$idpendaftaranoperasi' AND status='1'";
		$list=$this->db->query($q)->result();
		$total_all=0;
		foreach($list as $row){
			$total_before_diskon=$total_acuan_da * $row->persen / 100;
			$diskon_persen=0;
			if ($row->diskon>0){
				$diskon_persen=$row->diskon/$row->total_before_diskon*100;
			}
			$diskon=$total_before_diskon * $diskon_persen/100;
			$totalkeseluruhan=$total_before_diskon-$diskon;
			$total_all=$total_all + $totalkeseluruhan;
			$data=array(
				'total_acuan' => $total_acuan_da,
				'total_before_diskon' => $total_before_diskon,
				'diskon' => $diskon,
				'totalkeseluruhan' => $total_before_diskon-$diskon,
			);
			$this->db->where('id',$row->id);
			$this->db->update('tkamaroperasi_jasaao',$data);
		}
		
		$q="UPDATE tkamaroperasi_pendaftaran set persen_dao='$persen_da',total_acuan_dao='$total_acuan_da',total_dao='$total_all' WHERE id='$idpendaftaranoperasi'";
		$hasil=$this->db->query($q);
		
		return $hasil;
	}
}
