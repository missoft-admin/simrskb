<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Apm_setting_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_index_setting()
    {
        $q = "SELECT *
			 FROM apm_setting
			WHERE id='1'";

        return $this->db->query($q)->row_array();
    }

    public function get_index_setting_self()
    {
        $q = "SELECT *
			 FROM apm_setting_self
			WHERE id='1'";

        return $this->db->query($q)->row_array();
    }

    public function get_index_setting_mandiri()
    {
        $q = "SELECT *
			 FROM apm_setting_mandiri
			WHERE id='1'";

        return $this->db->query($q)->row_array();
    }

    public function save_general()
    {
        $id = $this->input->post('id');
        $this->bg_color = $this->input->post('bg_color');
        $this->judul_header = $this->input->post('judul_header');
        $this->judul_sub_header = $this->input->post('judul_sub_header');
        $this->upload_header_logo(true);
        $this->db->where('id', $id);
        // print_r($this);exit;
        if ($this->db->update('apm_setting', $this)) {
            return true;
        }

        return false;
    }

    public function save_self()
    {
        $id = $this->input->post('id');
        $this->bg_color_self = $this->input->post('bg_color_self');
        $this->judul_header_self = $this->input->post('judul_header_self');
        $this->judul_sub_header_self = $this->input->post('judul_sub_header_self');
        $this->content_input_kode_self = $this->input->post('content_input_kode_self');
        $this->content_rincian_header_self = $this->input->post('content_rincian_header_self');
        $this->content_rincian_footer_self = $this->input->post('content_rincian_footer_self');
        $this->upload_header_logo_self(true);
        $this->db->where('id', $id);
        // print_r($this);exit;
        if ($this->db->update('apm_setting_self', $this)) {
            return true;
        }

        return false;
    }

    public function save_mandiri()
    {
        $id = $this->input->post('id');
        $this->judul_header_mandiri = $this->input->post('judul_header_mandiri');
        $this->judul_sub_header_mandiri = $this->input->post('judul_sub_header_mandiri');
        $this->content_pencarian_header_mandiri = $this->input->post('content_pencarian_header_mandiri');
        $this->content_pencarian_footer_mandiri = $this->input->post('content_pencarian_footer_mandiri');
        $this->judul_pasien_baru_mandiri = $this->input->post('judul_pasien_baru_mandiri');
        $this->judul_pasien_lama_mandiri = $this->input->post('judul_pasien_lama_mandiri');
        $this->upload_header_logo_mandiri(true);
        $this->upload_icon_baru(true);
        $this->upload_icon_lama(true);
        $this->db->where('id', $id);
        // print_r($this);exit;
        if ($this->db->update('apm_setting_mandiri', $this)) {
            return true;
        }

        return false;
    }

    public function list_dokter()
    {
        $q = "SELECT * FROM mdokter M WHERE M.`status`='1'";

        return $this->db->query($q)->result();
    }

    public function upload_header_logo($update = false)
    {
        if (!file_exists('assets/upload/apm_setting')) {
            mkdir('assets/upload/apm_setting', 0755, true);
        }

        if (isset($_FILES['header_logo'])) {
            if ('' !== $_FILES['header_logo']['name']) {
                $config['upload_path'] = './assets/upload/apm_setting/';
                // $config['upload_path'] = './assets/upload/asset_pemindahan/';
                $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
                $config['encrypt_name'] = true;
                $config['overwrite'] = false;
                $this->upload->initialize($config);
                // $this->load->library('upload', $config);
                // $this->load->library('myimage');

                $this->load->library('upload', $config);
                // print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('header_logo')) {
                    $image_upload = $this->upload->data();
                    $this->header_logo = $image_upload['file_name'];

                    if (true === $update) {
                        $this->remove_image_header_logo($this->input->post('id'));
                    }

                    return true;
                }
                print_r($this->upload->display_errors());

                exit;
                $this->error_message = $this->upload->display_errors();

                return false;
            }
            // print_r('TIDAK ADA');exit;
            return true;
            // print_r($this->foto);exit;
        }
        // print_r('TIDAK ADA');exit;
        return true;
    }

    public function upload_header_logo_self($update = false)
    {
        if (!file_exists('assets/upload/apm_setting')) {
            mkdir('assets/upload/apm_setting', 0755, true);
        }

        if (isset($_FILES['header_logo_self'])) {
            if ('' !== $_FILES['header_logo_self']['name']) {
                $config['upload_path'] = './assets/upload/apm_setting/';
                // $config['upload_path'] = './assets/upload/asset_pemindahan/';
                $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
                $config['encrypt_name'] = true;
                $config['overwrite'] = false;
                $this->upload->initialize($config);
                // $this->load->library('upload', $config);
                // $this->load->library('myimage');

                $this->load->library('upload', $config);
                // print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('header_logo_self')) {
                    $image_upload = $this->upload->data();
                    $this->header_logo_self = $image_upload['file_name'];

                    if (true === $update) {
                        $this->remove_image_header_logo_self($this->input->post('id'));
                    }

                    return true;
                }
                print_r($this->upload->display_errors());

                exit;
                $this->error_message = $this->upload->display_errors();

                return false;
            }
            // print_r('TIDAK ADA');exit;
            return true;
            // print_r($this->foto);exit;
        }
        // print_r('TIDAK ADA');exit;
        return true;
    }

    public function upload_header_logo_mandiri($update = false)
    {
        if (!file_exists('assets/upload/apm_setting')) {
            mkdir('assets/upload/apm_setting', 0755, true);
        }

        if (isset($_FILES['header_logo_mandiri'])) {
            if ('' !== $_FILES['header_logo_mandiri']['name']) {
                $config['upload_path'] = './assets/upload/apm_setting/';
                // $config['upload_path'] = './assets/upload/asset_pemindahan/';
                $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
                $config['encrypt_name'] = true;
                $config['overwrite'] = false;
                $this->upload->initialize($config);
                // $this->load->library('upload', $config);
                // $this->load->library('myimage');

                $this->load->library('upload', $config);
                // print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('header_logo_mandiri')) {
                    $image_upload = $this->upload->data();
                    $this->header_logo_mandiri = $image_upload['file_name'];

                    if (true === $update) {
                        $this->remove_image_header_logo_mandiri($this->input->post('id'));
                    }

                    return true;
                }
                print_r($this->upload->display_errors());

                exit;
                $this->error_message = $this->upload->display_errors();

                return false;
            }
            // print_r('TIDAK ADA');exit;
            return true;
            // print_r($this->foto);exit;
        }
        // print_r('TIDAK ADA');exit;
        return true;
    }

    public function remove_image_header_logo($id): void
    {
        $q = "select header_logo From apm_setting H WHERE H.id='{$id}'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/apm_setting/'.$row->header_logo) && '' !== $row->header_logo) {
            unlink('./assets/upload/apm_setting/'.$row->header_logo);
        }
    }

    public function remove_image_header_logo_self($id): void
    {
        $q = "select header_logo_self From apm_setting_self H WHERE H.id='{$id}'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/apm_setting/'.$row->header_logo_self) && '' !== $row->header_logo_self) {
            unlink('./assets/upload/apm_setting/'.$row->header_logo_self);
        }
    }

    public function remove_image_header_logo_mandiri($id): void
    {
        $q = "select header_logo_mandiri From apm_setting_mandiri H WHERE H.id='{$id}'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/apm_setting/'.$row->header_logo_mandiri) && '' !== $row->header_logo_mandiri) {
            unlink('./assets/upload/apm_setting/'.$row->header_logo_mandiri);
        }
    }

    public function upload_icon_baru($update = false)
    {
        if (!file_exists('assets/upload/apm_setting')) {
            mkdir('assets/upload/apm_setting', 0755, true);
        }

        if (isset($_FILES['icon_pasien_baru_mandiri'])) {
            if ('' !== $_FILES['icon_pasien_baru_mandiri']['name']) {
                $config['upload_path'] = './assets/upload/apm_setting/';
                // $config['upload_path'] = './assets/upload/asset_pemindahan/';
                $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
                $config['encrypt_name'] = true;
                $config['overwrite'] = false;
                $this->upload->initialize($config);
                // $this->load->library('upload', $config);
                // $this->load->library('myimage');

                $this->load->library('upload', $config);
                // print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('icon_pasien_baru_mandiri')) {
                    $image_upload = $this->upload->data();
                    $this->icon_pasien_baru_mandiri = $image_upload['file_name'];

                    if (true === $update) {
                        $this->remove_icon_baru($this->input->post('id'));
                    }

                    return true;
                }
                print_r($this->upload->display_errors());

                exit;
                $this->error_message = $this->upload->display_errors();

                return false;
            }
            // print_r('TIDAK ADA');exit;
            return true;
            // print_r($this->foto);exit;
        }
        // print_r('TIDAK ADA');exit;
        return true;
    }

    public function remove_icon_baru($id): void
    {
        $q = "select icon_pasien_baru_mandiri From apm_setting_mandiri H WHERE H.id='{$id}'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/apm_setting/'.$row->icon_pasien_baru_mandiri) && '' !== $row->icon_pasien_baru_mandiri) {
            unlink('./assets/upload/apm_setting/'.$row->icon_pasien_baru_mandiri);
        }
    }

    public function upload_icon_lama($update = false)
    {
        if (!file_exists('assets/upload/apm_setting')) {
            mkdir('assets/upload/apm_setting', 0755, true);
        }

        if (isset($_FILES['icon_pasien_lama_mandiri'])) {
            if ('' !== $_FILES['icon_pasien_lama_mandiri']['name']) {
                $config['upload_path'] = './assets/upload/apm_setting/';
                // $config['upload_path'] = './assets/upload/asset_pemindahan/';
                $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
                $config['encrypt_name'] = true;
                $config['overwrite'] = false;
                $this->upload->initialize($config);
                // $this->load->library('upload', $config);
                // $this->load->library('myimage');

                $this->load->library('upload', $config);
                // print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('icon_pasien_lama_mandiri')) {
                    $image_upload = $this->upload->data();
                    $this->icon_pasien_lama_mandiri = $image_upload['file_name'];

                    if (true === $update) {
                        $this->remove_icon_lama($this->input->post('id'));
                    }

                    return true;
                }
                print_r($this->upload->display_errors());

                exit;
                $this->error_message = $this->upload->display_errors();

                return false;
            }
            // print_r('TIDAK ADA');exit;
            return true;
            // print_r($this->foto);exit;
        }
        // print_r('TIDAK ADA');exit;
        return true;
    }

    public function remove_icon_lama($id): void
    {
        $q = "select icon_pasien_lama_mandiri From apm_setting_mandiri H WHERE H.id='{$id}'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/apm_setting/'.$row->icon_pasien_lama_mandiri) && '' !== $row->icon_pasien_lama_mandiri) {
            unlink('./assets/upload/apm_setting/'.$row->icon_pasien_lama_mandiri);
        }
    }

    public function refresh_image()
    {
        $data_user = get_acces();
        $user_acces_form = $data_user['user_acces_form'];
        $q = "SELECT  H.* from apm_setting_file H

			WHERE H.status='1'";
        // print_r($q);exit();
        $row = $this->db->query($q)->result();
        $tabel = '';
        $no = 1;
        foreach ($row as $r) {
            $tabel .= '<tr>';
            $tabel .= '<td class="text-right">'.$no.'</td>';
            $tabel .= '<td class="text-left"><a href="'.base_url().'assets/upload/apm_setting/'.$r->filename.'" target="_blank">'.substr($r->filename, 11).'</a></td>';
            $tabel .= '<td class="text-left">'.$r->upload_by_nama.'-'.HumanDateLong($r->upload_date).'</td>';
            $tabel .= '<td class="text-left">'.$r->size.'</td>';
            $tabel .= '<td class="text-left">';
            $tabel .= '<a href="'.base_url().'assets/upload/apm_setting/'.$r->filename.'" target="_blank" data-toggle="tooltip" title="Preview" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
            if (UserAccesForm($user_acces_form, ['1524'])) {
                $tabel .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm " type="button" onclick="removeFile('.$r->id.')"><i class="fa fa-trash-o"></i></button>';
            }
            $tabel .= '</td></tr>';
            $no = $no + 1;
        }

        return $tabel;
    }
    // HAPUS
}
