<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Antrian_tiket_setting_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function get_index_setting(){
		$q="SELECT *
			 FROM antrian_tiket_setting
			WHERE id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function save_general(){
		$id = $this->input->post('id');
		$this->bg_color = $this->input->post('bg_color');
		$this->judul_header = $this->input->post('judul_header');
		$this->judul_sub_header = $this->input->post('judul_sub_header');
		$this->alamat = $this->input->post('alamat');
		$this->telepone = $this->input->post('telepone');
		$this->email = $this->input->post('email');
		$this->website = $this->input->post('website');
		$this->upload_header_logo(true);
		$this->db->where('id', $id);
		// print_r($this);exit;	
		if ($this->db->update('antrian_tiket_setting', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	public function upload_header_logo($update = false)
    {
        if (!file_exists('assets/upload/antrian')) {
            mkdir('assets/upload/antrian', 0755, true);
        }

        if (isset($_FILES['header_logo'])) {
            if ($_FILES['header_logo']['name'] != '') {
                $config['upload_path'] = './assets/upload/antrian/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('header_logo')) {
                    $image_upload = $this->upload->data();
                    $this->header_logo = $image_upload['file_name'];

                    if ($update == true) {
                        $this->remove_image_header_logo($this->input->post('id'));
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
			// print_r('TIDAK ADA');exit;
                return true;
            }
					// print_r($this->foto);exit;
        } else {
			// print_r('TIDAK ADA');exit;
            return true;
        }
		
    }
	
	public function remove_image_header_logo($id)
    {
		$q="select header_logo From antrian_tiket_setting H WHERE H.id='$id'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/antrian/'.$row->header_logo) && $row->header_logo !='') {
            unlink('./assets/upload/antrian/'.$row->header_logo);
        }
    }
	
	
}
