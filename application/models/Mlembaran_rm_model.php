<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mlembaran_rm_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mlembaran_rm');
        return $query->row();
    }

    public function saveData()
    {
        $dataKriteria = array();
        $tabelKriteria = jsonDecode($_POST['kriteria']);
        foreach ($tabelKriteria as $index => $row) {
          array_push($dataKriteria, array(
            'index' => $index,
            'keterangan' => $row[0],
            'nilai' => $row[1],
          ));
        }

        $dataDetail = array();
        $tabelDetail = jsonDecode($_POST['detail']);
        foreach ($tabelDetail as $index => $row) {
          array_push($dataDetail, array(
            'index' => $index,
            'keterangan' => $row[0],
          ));
        }

        $this->nama = $_POST['nama'];
        $this->referensi = jsonEncode($_POST['referensi']);
        $this->kriteria = jsonEncode($dataKriteria);
        $this->detail = jsonEncode($dataDetail);
        $this->deskripsi = $_POST['deskripsi'];

        if ($this->db->insert('mlembaran_rm', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $dataKriteria = array();
        $tabelKriteria = jsonDecode($_POST['kriteria']);
        foreach ($tabelKriteria as $index => $row) {
          array_push($dataKriteria, array(
            'index' => $index,
            'keterangan' => $row[0],
            'nilai' => $row[1],
          ));
        }

        $dataDetail = array();
        $tabelDetail = jsonDecode($_POST['detail']);
        foreach ($tabelDetail as $index => $row) {
          array_push($dataDetail, array(
            'index' => $index,
            'keterangan' => $row[0],
          ));
        }

        $this->nama = $_POST['nama'];
        $this->referensi = jsonEncode($_POST['referensi']);
        $this->kriteria = jsonEncode($dataKriteria);
        $this->detail = jsonEncode($dataDetail);
        $this->deskripsi = $_POST['deskripsi'];

        if ($this->db->update('mlembaran_rm', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
        return $status;
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('mlembaran_rm', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
