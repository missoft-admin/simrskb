<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Mkontrabon_model extends CI_Model
{
    public function index_table()
    {
        $this->load->library('datatables');
        $this->datatables->where('staktif', 1);
        $this->datatables->from('mkontrabon');
        return print_r($this->datatables->generate());
    }
	public function get_data(){
		$q="select *from mkontrabon";
		$query=$this->db->query($q);
		return $query->row_array();
	}
	public function get_data_hari(){
		$q="select harikbo from mkontrabon";
		$query=$this->db->query($q);
		return $query->row('harikbo');
	}
	public function list_his(){
		$q="SELECT *from mkontrabon_his
			LEFT JOIN mhari ON mhari.id=mkontrabon_his.harikbo ORDER BY mkontrabon_his.id DESC";
		$query=$this->db->query($q);
		return $query->result();
	}
	
    public function save()
    {
		// print_r($this->input->post('user_id'));exit();
        
            $this->db->set('tgl_edit', date('Y-m-d H:i:s'));
            $this->db->set('harikbo', $this->input->post('harikbo'));
            $this->db->set('edit_by', $this->session->userdata('user_id'));
            $this->db->set('user_edit', $this->session->userdata('user_name'));
            $this->db->set('minnominalceq', RemoveComma($this->input->post('minnominalceq')));
            $this->db->set('biayaceq', RemoveComma($this->input->post('biayaceq')));
            $this->db->set('biayamaterai',RemoveComma($this->input->post('biayamaterai')));
            $this->db->set('biaya_tf',RemoveComma($this->input->post('biaya_tf')));
            $this->db->set('jml_hari', RemoveComma($this->input->post('jml_hari')));
            $this->db->where('id', $this->input->post('id'));
            if ($this->db->update('mkontrabon')) {
				$this->db->where('user_id <>','');
				$this->db->delete('mkontrabon_user_validasi');
				$user = $this->input->post('user_id_validasi');
				foreach ($user as $r) {
						$data = array(
					        'user_id' => $r
							);
							$this->db->insert('mkontrabon_user_validasi', $data);
				}
				$this->db->where('user_id <>','');
				$this->db->delete('mkontrabon_user_aktivasi');
				$user = $this->input->post('user_id_aktivasi');
				foreach ($user as $r) {
						$data = array(
					        'user_id' => $r
							);
							$this->db->insert('mkontrabon_user_aktivasi', $data);
				}
                return true;
            } else {
                return false;
            }
       
        // $this->output->set_output(json_encode($data));
    }

    public function aktifasi()
    {
        $id = $this->uri->segment(3);
        if ($id) {
            $this->db->set('status', 0);
            $this->db->where('status', 1);
            $this->db->where('id !=', $id);
            $update = $this->db->update('mkontrabon');
            if ($update) {
                $this->db->set('status', 1);
                $this->db->where('id', $id);
                $this->db->update('mkontrabon');
                return true;
            } else {
                return false;
            }
        }
    }

    public function non_aktifasi()
    {
        $id = $this->uri->segment(3);
        if ($id) {
          $this->db->set('status', 0);
          $this->db->where('id', $id);
          if ($this->db->update('mkontrabon')) {
              return true;
          } else {
              return false;
          }
        }
    }

    public function hapus()
    {
        $id = $this->uri->segment(3);
        if ($id) {
            $this->db->set('status', 0);
            $this->db->set('staktif', 0);
            $this->db->where('id', $id);
            $update = $this->db->update('mkontrabon');
            if ($update) {
                return true;
            } else {
                return false;
            }
        }
    }
}

/* End of file Mkontrabon_model.php */
/* Location: ./application/models/Mkontrabon_model.php */
