<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Treservasi_bed_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function list_ruang(){
		$q="SELECT *FROM mruangan H
			WHERE H.`status`='1' AND H.idtipe='1'
			ORDER BY H.id ASC";
		return $this->db->query($q)->result();
	}
	function list_kelas(){
		$q="SELECT *FROM mkelas H
			WHERE H.`status`='1' 
			ORDER BY H.id ASC";
		return $this->db->query($q)->result();
	}
	function list_bed($idruangan='',$idkelas=''){
		$where='';
		if ($idruangan!=''){
			$where .=" AND H.idruangan='$idruangan'";
		}
		if ($idkelas!=''){
			$where .=" AND H.idkelas='$idkelas'";
		}
		$q="SELECT *FROM mbed H
			WHERE H.`status`='1' ".$where."
			ORDER BY H.id ASC";
		return $this->db->query($q)->result();
	}
	function saveData(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data=array(
			'notransaksi' => $this->input->post('notransaksi'),
			'asal_reservasi' => $this->input->post('asal_reservasi'),
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'nopendaftaran' => $this->input->post('nopendaftaran'),
			'perencanaan_id' => $this->input->post('perencanaan_id'),
			'idpasien' => $this->input->post('idpasien'),
			'no_medrec' => $this->input->post('no_medrec'),
			'namapasien' => $this->input->post('namapasien'),
			'idtipe' => $this->input->post('idtipe'),
			'idpoliklinik' => $this->input->post('idpoliklinik'),
			'iddokter_perujuk' => $this->input->post('iddokter_perujuk'),
			'dpjp' => $this->input->post('dpjp'),
			'nopermintaan' => $this->input->post('nopermintaan'),
			'diagnosa' => $this->input->post('diagnosa'),
			'idruangan' => $this->input->post('idruangan'),
			'idkelas' => $this->input->post('idkelas'),
			'idbed' => $this->input->post('idbed'),
			'prioritas' => $this->input->post('prioritas'),
			'rencana_masuk' => YMDFormat($this->input->post('rencana_masuk')),
			'idruangan_alter' => $this->input->post('idruangan_alter'),
			'idkelas_alter' => $this->input->post('idkelas_alter'),
			'idbed_alter' => $this->input->post('idbed_alter'),
			'tipe_pemohon' => $this->input->post('tipe_pemohon'),
			'nama_pemohon' => $this->input->post('nama_pemohon'),
			'nohp_pemohon' => $this->input->post('nohp_pemohon'),
			'alamat_pemohon' => $this->input->post('alamat_pemohon'),
			'catatan' => $this->input->post('catatan'),
			'created_date' => date('Y-m-d H:i:s'),
			'created_ppa' => $login_ppa_id,
			// 'edited_date' => $this->input->post('edited_date'),
			// 'edited_ppa' => $this->input->post('edited_ppa'),
			'status_reservasi' => 1,
		);
		
		$hasil=$this->db->insert('treservasi_bed',$data);
		
		return $hasil;
		// print_r($data);exit;
	}
	function updateData($id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		
		$data=array(
			'nopendaftaran' => $this->input->post('nopendaftaran'),
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'iddokter_perujuk' => $this->input->post('iddokter_perujuk'),
			'dpjp' => $this->input->post('dpjp'),
			'nopermintaan' => $this->input->post('nopermintaan'),
			'diagnosa' => $this->input->post('diagnosa'),
			'idruangan' => $this->input->post('idruangan'),
			'idkelas' => $this->input->post('idkelas'),
			'idbed' => $this->input->post('idbed'),
			'prioritas' => $this->input->post('prioritas'),
			'rencana_masuk' => YMDFormat($this->input->post('rencana_masuk')),
			'idruangan_alter' => $this->input->post('idruangan_alter'),
			'idkelas_alter' => $this->input->post('idkelas_alter'),
			'idbed_alter' => $this->input->post('idbed_alter'),
			'tipe_pemohon' => $this->input->post('tipe_pemohon'),
			'nama_pemohon' => $this->input->post('nama_pemohon'),
			'nohp_pemohon' => $this->input->post('nohp_pemohon'),
			'alamat_pemohon' => $this->input->post('alamat_pemohon'),
			'catatan' => $this->input->post('catatan'),
			'edited_date' => date('Y-m-d H:i:s'),
			'edited_ppa' => $login_ppa_id,
			'status_reservasi' => 1,
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('treservasi_bed',$data);
		
		return $hasil;
		// print_r($data);exit;
	}
}
