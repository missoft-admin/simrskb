<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Trujukan_rumahsakit_rincian_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function stopPeriode($idtipe, $idfeerujukan)
	{
		$this->db->set('status_stop', '1');
		$this->db->where('id', $idfeerujukan);
		if ($this->db->update('tfeerujukan_rumahsakit')) {
			// $this->db->query("CALL setNextPeriodeFeeRujukanRSHold($idtipe, $idfeerujukan)");
			return true;
		}
	}

	public function nextPeriode($data)
	{
		$idfeerujukanSebelumnya = $data['idfeerujukan'];
		$idfeerujukan = $this->checkStatusFeeRujukanRS($data['idrujukan'], $data['periode_pembayaran']);
		if ($idfeerujukan == null) {
			$dataFeeRujukanRS = [
				'idasalpasien' => $data['idasalpasien'],
				'idrujukan' => $data['idrujukan'],
				'namarujukan' => $data['namarujukan'],
				'idtipe' => $data['idtipe'],
				'tanggal_pembayaran' => $data['periode_pembayaran'],
				'tanggal_jatuhtempo' => $data['periode_jatuhtempo'],
				'nominal' => 0,
				'created_at' => date('Y-m-d H:i:s'),
				'created_by' => $this->session->userdata('user_id')
			];

			if ($this->db->insert('tfeerujukan_rumahsakit', $dataFeeRujukanRS)) {
				$idfeerujukan = $this->db->insert_id();
			}
		}

		$this->db->set('idfeerujukan', $idfeerujukan);
		$this->db->set('tanggal_pembayaran_selanjutnya', null);
		$this->db->set('tanggal_jatuhtempo_selanjutnya', null);
		$this->db->set('idfeerujukan_selanjutnya', null);
		$this->db->where('idfeerujukan', $idfeerujukanSebelumnya);
		if ($this->db->update('tfeerujukan_rumahsakit_detail')) {
			return true;
		}
	}

	public function processApproval($id)
	{
		$this->db->set('status_approval', 2);
		$this->db->where('id', $id);
		if ($this->db->update('tfeerujukan_rumahsakit')) {
			return true;
		}
	}

	public function checkStatusFeeRujukanRS($idrujukan, $tanggal_pembayaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$this->db->where('idrujukan', $idrujukan);
		$this->db->where('tanggal_pembayaran', $tanggal_pembayaran);
		$this->db->where('status_stop', '0');
		$this->db->where('status', '1');
		$query = $this->db->get('tfeerujukan_rumahsakit');
		$row = $query->row();

		if ($query->num_rows() > 0) {
			return $row->id;
		} else {
			return null;
		}
	}
}