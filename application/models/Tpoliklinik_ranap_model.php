<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpoliklinik_ranap_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function list_header_path($idtipe,$idkelompokpasien,$idrekanan=0){
		$idrekanan=($idkelompokpasien==1?$idrekanan:0);
		$q="SELECT *FROM (
			SELECT  M.id as kp,0 as idrekanan,M.tfisioterapi FROM mpasien_kelompok M
			UNION ALL
			SELECT 1 as kp,M.id as idrekanan,M.tfisioterapi FROM mrekanan M WHERE (M.tfisioterapi > 0) 
			) T WHERE T.kp='$idkelompokpasien' AND (T.idrekanan='0' OR T.idrekanan='$idrekanan')

			ORDER BY T.idrekanan DESC";
		// print_r($q);exit;
		$row=$this->db->query($q)->row();
		$idlayanan='';
		$idlayanan=$row->tfisioterapi;
		
		return $idlayanan;
	}
	function get_data_konsul($pendaftaran_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT * FROM tpoliklinik_konsul H WHERE H.status_konsul='1' AND H.pendaftaran_id='$pendaftaran_id' AND H.created_ppa='$login_ppa_id'";
		return $this->db->query($q)->row_array();
	}
	function list_dokter($idpoli){
		$q="SELECT M.id,M.nama FROM `mpoliklinik_konsul_dokter` H
				INNER JOIN mdokter M ON M.id=H.iddokter
				WHERE H.idpoliklinik='$idpoli'";
		return $this->db->query($q)->result();
	}
	function list_dokter_all(){
		$q="SELECT M.id,M.nama FROM `mdokter` M WHERE M.status='1'
				";
		return $this->db->query($q)->result();
	}
	function list_poliklinik(){
		$q="SELECT M.id,M.nama FROM mpoliklinik_konsul H INNER JOIN mpoliklinik M ON M.id=H.idpoliklinik";
		return $this->db->query($q)->result();
	}
	function list_ppa_dokter(){
		$q="SELECT H.id,H.nama FROM mppa H WHERE H.tipepegawai='2' AND H.jenis_profesi_id='1'";
		return $this->db->query($q)->result();
	}
	function list_tujuan(){
		$q="SELECT M.id,M.nama FROM app_reservasi_poli H
INNER JOIN mpoliklinik M ON M.id=H.idpoli";
		return $this->db->query($q)->result();
	}
	function list_tujuan_fisio(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT MP.id,MP.nama FROM `app_reservasi_poli` H
			INNER JOIN app_reservasi_poli_user U ON U.idpoli=H.idpoli AND U.iduser='$user_id'
			INNER JOIN mpoliklinik MP ON MP.id=H.idpoli";
		return $this->db->query($q)->result();
	}
	function list_tujuan_fisio_array(){
		$user_id=$this->session->userdata('user_id');
		$q="SELECT GROUP_CONCAT(MP.id) as id FROM `app_reservasi_poli` H
			INNER JOIN app_reservasi_poli_user U ON U.idpoli=H.idpoli AND U.iduser='$user_id'
			INNER JOIN mpoliklinik MP ON MP.id=H.idpoli";
		return $this->db->query($q)->row('id');
	}
	function get_data_assesmen($pendaftaran_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT * FROM tpoliklinik_rm_order H WHERE H.pendaftaran_id='$pendaftaran_id' AND H.status_assemen='1' AND H.created_ppa='$login_ppa_id'";
		return $this->db->query($q)->row_array();
	}
	function get_data_assesmen_per($pendaftaran_id,$st_ranap='0'){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		if ($st_ranap=='0'){
		$q="SELECT * FROM tpoliklinik_ranap_perencanaan H WHERE H.pendaftaran_id='$pendaftaran_id' AND H.status_assemen='1' AND H.created_ppa='$login_ppa_id'";
			
		}else{
		$q="SELECT * FROM tpoliklinik_ranap_perencanaan H WHERE H.pendaftaran_id_ranap='$pendaftaran_id' AND H.status_assemen='1' AND H.created_ppa='$login_ppa_id'";
			
		}
		return $this->db->query($q)->row_array();
	}
	function get_data_assesmen_per_bedah($pendaftaran_id,$st_ranap='0'){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		if ($st_ranap=='0'){
		$q="SELECT * FROM tpoliklinik_ranap_perencanaan_bedah H WHERE H.pendaftaran_id='$pendaftaran_id' AND H.status_assemen='1' AND (H.created_ppa='$login_ppa_id' OR H.edited_ppa='$login_ppa_id')";
		}else{
			
		$q="SELECT * FROM tpoliklinik_ranap_perencanaan_bedah H WHERE H.pendaftaran_id_ranap='$pendaftaran_id' AND H.status_assemen='1' AND (H.created_ppa='$login_ppa_id' OR H.edited_ppa='$login_ppa_id')";
		}
		return $this->db->query($q)->row_array();
	}
	function get_data_assesmen_rencana_biaya($pendaftaran_id,$st_ranap){
		
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		if ($st_ranap=='1'){
		$q="SELECT * FROM tpoliklinik_ranap_rencana_biaya H WHERE H.pendaftaran_id_ranap='$pendaftaran_id' AND H.status_assemen IN (1,3) AND (H.created_ppa='$login_ppa_id' OR H.edited_ppa='$login_ppa_id')";
			
		}else{
		$q="SELECT * FROM tpoliklinik_ranap_rencana_biaya H WHERE H.pendaftaran_id='$pendaftaran_id' AND H.status_assemen IN (1,3) AND (H.created_ppa='$login_ppa_id' OR H.edited_ppa='$login_ppa_id')";
			
		}
		return $this->db->query($q)->row_array();
	}
	function get_data_assesmen_trx($assesmen_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT * FROM tpoliklinik_rm_order H WHERE H.assesmen_id='$assesmen_id'";
		return $this->db->query($q)->row_array();
	}
	function get_data_assesmen_trx_per($assesmen_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT * FROM tpoliklinik_ranap_perencanaan H WHERE H.assesmen_id='$assesmen_id'";
		return $this->db->query($q)->row_array();
	}
	function get_data_assesmen_trx_per_bedah($assesmen_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT * FROM tpoliklinik_ranap_perencanaan_bedah H WHERE H.assesmen_id='$assesmen_id'";
		return $this->db->query($q)->row_array();
	}
	function get_data_assesmen_trx_rencana_biaya($assesmen_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT mppa.nama as nama_dpjp, H.* 
			FROM tpoliklinik_ranap_rencana_biaya H 
			LEFT JOIN mdokter mppa ON mppa.id=H.dpjp
			WHERE H.assesmen_id='$assesmen_id'";
		return $this->db->query($q)->row_array();
	}
	// function get_data_assesmen_rencana_biaya($assesmen_id){
		// $login_ppa_id=$this->session->userdata('login_ppa_id');
		// $q="SELECT * FROM tpoliklinik_ranap_rencana_biaya H WHERE H.assesmen_id='$assesmen_id'";
		// return $this->db->query($q)->row_array();
	// }
	function setting_label_rm(){
		$q="SELECT 
			pesan_informasi,judul_ina,judul_eng,tujuan_ina,tujuan_eng,tujuan_req,dokter_ina,dokter_eng,dokter_req,waktu_ina,waktu_eng,waktu_req,prioritas_ina,prioritas_eng,prioritas_req,catatan_ina,catatan_eng,catatan_req,ket_ina,ket_eng,ket_req,sebanyak_ina,sebanyak_eng,sebanyak_req
			,selama_ina,selama_eng,selama_req,kontrol_ina,kontrol_eng,kontrol_req,detail_ina,detail_eng,detail_req,diagnosa_ina,diagnosa_eng,diagnosa_req

		FROM setting_rm_label H ";
		return $this->db->query($q)->row_array();
	}
	function setting_label_perencanaan(){
		$q="SELECT 
			judul_per_ina,judul_per_eng,paragraph_1_ina,paragraph_1_eng,paragraph_2_ina,paragraph_2_eng,nama_pasien_ina,nama_pasien_eng,ttl_ina,ttl_eng,diagnosa_per_ina,diagnosa_per_eng,catatan_ina,catatan_eng,tipe_ina,tipe_eng,dpjp_ina,dpjp_eng,rencana_masuk_ina,rencana_masuk_eng,dilakukan_pemeriksaan_ina,dilakukan_pemeriksaan_eng,rencana_pelayanan_ina,rencana_pelayanan_eng,paragraph_3_ina,paragraph_3_eng,footer_ina,footer_eng

			FROM setting_rencana_ranap_label H ";
		return $this->db->query($q)->row_array();
	}
	function setting_label_estimasi_biaya(){
		$q="SELECT 
			judul_per_ina,judul_per_eng,paragraph_1_ina,paragraph_1_eng,paragraph_2_ina,paragraph_2_eng,nama_pasien_ina,nama_pasien_eng,ttl_ina,ttl_eng,diagnosa_per_ina,diagnosa_per_eng,nama_tindakan_ina,nama_tindakan_eng,catatan_ina,catatan_eng,tipe_ina,tipe_eng,icu_ina,icu_eng,cito_ina,cito_eng,dokter_ina,dokter_eng,rencana_implant_ina,rencana_implant_eng,table_no_ina,table_no_eng,table_alat_ina,table_alat_eng,table_ukuran_ina,table_ukuran_eng,table_merk_ina,table_merk_eng,table_catatan_ina,table_catatan_eng,table_jumlah_ina,table_jumlah_eng,table_biaya_ina,table_biaya_eng,table_aksi_ina,table_aksi_eng,kel_op_ina,kel_op_eng,jenis_ina,jenis_eng,lama_ina,lama_eng,kel_pasien_ina,kel_pasien_eng,asuransi_ina,asuransi_eng,section_biaya_ina,section_biaya_eng,kamar_ina,kamar_eng,pakai_obat_ina,pakai_obat_eng,biaya_implan_ina,biaya_implan_eng,jasa_dokter_operator_ina,jasa_dokter_operator_eng,jasa_dokter_anes_ina,jasa_dokter_anes_eng,jasa_asisten_ina,jasa_asisten_eng,total_biaya_ina,total_biaya_eng,section_biaya_ranap_ina,section_biaya_ranap_eng,biaya_ranap_ina,biaya_ranap_eng,biaya_ruang_ina,biaya_ruang_eng,biaya_obat_ina,biaya_obat_eng,biaya_penunjang_ina,biaya_penunjang_eng,biaya_visit_ina,biaya_visit_eng,biaya_icu_ina,biaya_icu_eng,total_biaya_ranap_ina,total_biaya_ranap_eng,total_estimasi_biaya_ina,total_estimasi_biaya_eng,yang_menjelaskan_ina,yang_menjelaskan_eng,petugas_biling_ina,petugas_biling_eng,dpjp_ina,dpjp_eng,ttd_pasien_ina,ttd_pasien_eng
			,table_footer_ina,table_footer_eng
			,ket_no_ina
			,ket_no_eng
			,ket_penjelasan_ina
			,ket_penjelasan_eng
			,ket_paraf_ina
			,ket_paraf_eng
			,kelas_diambil_ina,kelas_diambil_eng


			
			FROM setting_rencana_biaya_label H ";
		return $this->db->query($q)->row_array();
	}
	function setting_label_perencanaan_bedah(){
		$q="SELECT 
			judul_per_ina,judul_per_eng,nama_pasien_ina,nama_pasien_eng,ttl_ina,ttl_eng,diagnosa_per_ina,diagnosa_per_eng,catatan_ina,catatan_eng,tipe_ina,tipe_eng,dokter_ina,dokter_eng,rencana_masuk_ina,rencana_masuk_eng,tindakan_ina,tindakan_eng,footer_ina,footer_eng

			FROM setting_rencana_bedah_label H ";
		return $this->db->query($q)->row_array();
	}
	function get_ttd_dokter($id){
		$q="SELECT *FROM mdokter WHERE id='$id'";
		$data=$this->db->query($q)->row();
		$nama_lengkap ="Nama Pemberi Asuhan : ".$data->nama;
		$nama_lengkap .="
						NIP : ".$data->nip;
        $this->load->library('Barcode_generator');
        return base64_encode($this->barcode_generator->output_image('jpg', 'qr-h',$nama_lengkap,array()));
	}
	function get_data_assesmen_rencana_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_ranap_perencanaan_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_ranap_perencanaan H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_assesmen_rencana_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT dengan_diagnosa as dengan_diagnosa_asal,catatan as catatan_asal,tipe as tipe_asal,dpjp as dpjp_asal,tanggal_masuk as tanggal_masuk_asal,rencana_pemeriksaan as rencana_pemeriksaan_asal,rencana_pelayanan as rencana_pelayanan_asal
				FROM tpoliklinik_ranap_perencanaan_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_assesmen_rencana_bedah_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_ranap_perencanaan_bedah_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_ranap_perencanaan_bedah H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_assesmen_rencana_bedah_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT dengan_diagnosa as dengan_diagnosa_asal,catatan as catatan_asal,tipe as tipe_asal,dpjp as dpjp_asal,tanggal_bedah as tanggal_bedah_asal,rencana_tindakan as rencana_tindakan_asal
				FROM tpoliklinik_ranap_perencanaan_bedah_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_assesmen_estimasi_biaya_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_ranap_rencana_biaya_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_ranap_rencana_biaya H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_assesmen_estimasi_biasa_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
			dengan_diagnosa as dengan_diagnosa_asal,catatan as catatan_asal,tipe as tipe_asal,dpjp as dpjp_asal,icu as icu_asal,cito as cito_asal,rencana_tindakan as rencana_tindakan_asal,total_estimasi as total_estimasi_asal
				FROM tpoliklinik_ranap_rencana_biaya_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	//MPP
	function get_data_mpp($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='1'){
		$q="SELECT * FROM tranap_mpp WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			
		}else{
			
		$q="SELECT * FROM tranap_mpp WHERE pendaftaran_id='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		// print_r($q);exit;
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_mpp_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_mpp WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_mpp_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_mpp_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_mpp H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_mpp_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				total_skor as total_skor_asal,kesimpulan_mpp_id as kesimpulan_mpp_id_asal,kesimpulan_mpp_text as kesimpulan_mpp_text_asal,tindakan_id as tindakan_id_asal,tindakan_nama as tindakan_nama_asal,keterangan as keterangan_asal
				FROM tranap_mpp_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function list_template_mpp(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_mpp H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	//MPP Evaluasi
	function get_data_evaluasi_mpp($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='1'){
		$q="SELECT * FROM tranap_evaluasi_mpp WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			
		}else{
			
		$q="SELECT * FROM tranap_evaluasi_mpp WHERE pendaftaran_id='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		// print_r($q);exit;
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_evaluasi_mpp_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_evaluasi_mpp WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_evaluasi_mpp_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_evaluasi_mpp_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_evaluasi_mpp H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_evaluasi_mpp_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_input as tanggal_input_asal,
				indentifikasi_skrining as indentifikasi_skrining_asal,fisik as fisik_asal,riwayat_kesehatan as riwayat_kesehatan_asal,perilaku as perilaku_asal,kesehatan_mental as kesehatan_mental_asal,lingkungan as lingkungan_asal,dukungan_kel as dukungan_kel_asal,finansial as finansial_asal,finansial_ket as finansial_ket_asal,obat_alternative as obat_alternative_asal,obat_alternative_lain as obat_alternative_lain_asal,trauma as trauma_asal,pemahanan as pemahanan_asal,harapan as harapan_asal,perkiraan_rawat as perkiraan_rawat_asal,discharge_planing as discharge_planing_asal,discharge_planing_lain as discharge_planing_lain_asal,perencanaan_lanjutan as perencanaan_lanjutan_asal,aspek_legal as aspek_legal_asal,aspek_legal_lain as aspek_legal_lain_asal,kesempatan as kesempatan_asal,perencanaan_mpp as perencanaan_mpp_asal,jangka_pendek as jangka_pendek_asal,jangka_panjang as jangka_panjang_asal,kebutuhan_perjalanan as kebutuhan_perjalanan_asal,lain_lain as lain_lain_asal,rencana_penunjang as rencana_penunjang_asal,rencana_penunjang_lain as rencana_penunjang_lain_asal,tempat_perawatan as tempat_perawatan_asal,tempat_perawatan_lain as tempat_perawatan_lain_asal,revisi as revisi_asal
				FROM tranap_evaluasi_mpp_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function list_template_evaluasi_mpp(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_evaluasi_mpp H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	//MPP Catatan Implentasi
	function get_data_imp_mpp($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='1'){
		$q="SELECT * FROM tranap_imp_mpp WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			
		}else{
			
		$q="SELECT * FROM tranap_imp_mpp WHERE pendaftaran_id='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		// print_r($q);exit;
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_imp_mpp_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_imp_mpp WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_imp_mpp_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_imp_mpp_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_imp_mpp H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_imp_mpp_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_input as tanggal_input_asal,
				tanggal_implementasi as tanggal_implementasi_asal,item_kegiatan as item_kegiatan_asal,catatan as catatan_asal

				FROM tranap_imp_mpp_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function list_template_imp_mpp(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_imp_mpp H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
}
