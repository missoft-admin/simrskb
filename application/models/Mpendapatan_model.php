<?php

class Mpendapatan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getNoAkun()
    {
        $this->db->order_by('makun_nomor.noakun', 'ASC');
        // $this->db->where('makun_nomor.stbayar', '1');
        $this->db->where('makun_nomor.status', '1');
        $query = $this->db->get('makun_nomor');
        return $query->result();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mpendapatan');
        return $query->row();
    }

    public function saveData()
    {
        $this->keterangan   = $_POST['keterangan'];
        $this->idakun       = $_POST['idakun'];
        $this->noakun       = $_POST['noakun'];
        $this->status 		  = 1;

        if ($this->db->insert('mpendapatan', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->keterangan   = $_POST['keterangan'];
		$this->idakun       = $_POST['idakun'];
        $this->noakun       = $_POST['noakun'];
        $this->status 		  = 1;

        if ($this->db->update('mpendapatan', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('mpendapatan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
