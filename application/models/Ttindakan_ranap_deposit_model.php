<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ttindakan_ranap_deposit_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function list_dokter(){
		$q="SELECT MP.id,MP.nama FROM `mjadwal_dokter` H
			LEFT JOIN mdokter MP ON MP.id=H.iddokter
			WHERE MP.`status`='1'
			GROUP BY H.iddokter
";
		return $this->db->query($q)->result();
	}
	
	function list_poli($idtipe=''){
		$q="SELECT *FROM mpoliklinik H WHERE H.`status`='1' AND H.idtipe='$idtipe'";
		return $this->db->query($q)->result();
	}
	public function simpan_proses_peretujuan($id) {

		$this->db->where('deposit_id',$id);
		$this->db->delete('trawatinap_deposit_approval');
		$q="SELECT S.step,U.`name` as user_nama,S.proses_setuju,S.proses_tolak,S.deskrpisi,S.iduser FROM trawatinap_deposit H
				INNER JOIN mlogic_deposit S ON S.tipe_deposit=H.jenis_deposit_id
				INNER JOIN musers U ON U.id=S.iduser
				WHERE H.id='$id' AND S.status='1'
				ORDER BY S.step,S.id";
		$list_user=$this->db->query($q)->result();
		$step=999;
		$idlogic='';
		foreach ($list_user as $row){
			if ($row->step < $step){
				$step=$row->step;
			}
			$data=array(
				'deposit_id' => $id,
				'step' => $row->step,
				// 'idlogic' => $row->idlogic,
				'iduser' => $row->iduser,
				'user_nama' => $row->user_nama,
				'proses_setuju' => $row->proses_setuju,
				'proses_tolak' => $row->proses_tolak,
				'deskrpisi' => $row->deskrpisi,
				'approve' => 0,
			);
			// $idlogic=$row->idlogic;
			$this->db->insert('trawatinap_deposit_approval', $data);
		}

		$this->db->update('trawatinap_deposit_approval',array('st_aktif'=>1),array('deposit_id'=>$id,'step'=>$step));
		$alasanhapus=$this->input->post('alasanhapus');
		$data=array(
			'hapus_proses' => '1',
			'alasanhapus' => $alasanhapus,
			'iduserdelete' => $this->session->userdata('user_id'),
			'deleted_date' => date('Y-m-d H:i:s'),
			
		);
		$this->db->where('id',$id);
		return $this->db->update('trawatinap_deposit', $data);


    }
	
}
