<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Organization_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function saveData($data)
    {
        $this->db->on_duplicate('satu_sehat_organization', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            $this->error_message = 'Penyimpanan Gagal';
            return false;
        }
    }
}
