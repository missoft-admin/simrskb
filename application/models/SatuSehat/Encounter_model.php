<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Encounter_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $query = $this->db->query('
            SELECT
                satu_sehat_encounter.id,
                satu_sehat_encounter.encounter_id,
                tpoliklinik_pendaftaran.id AS transaction_id,
                "Encounter" AS resource_type,
                IF (satu_sehat_condition.condition_id IS NOT NULL, "finished", "in-progress") AS status_encounter,
                "AMB" AS class_code,
                "ambulatory" AS class_name,
                satu_sehat_patient.patient_id AS patient_id,
                satu_sehat_patient.name AS patient_name,
                "ATND" AS participant_type_code,
                "attender" AS participant_type_name,
                satu_sehat_practitioner.practitioner_id AS practitioner_id,
                satu_sehat_practitioner.name AS practitioner_name,
                IF (
                    tpoliklinik_pendaftaran.reservasi_cara = 0, 
                    CONCAT(DATE_FORMAT(tpoliklinik_pendaftaran.tanggaldaftar, "%Y-%m-%dT%H:%i:%s"), "+07:00"),
                    CONCAT(DATE_FORMAT(app_reservasi_tanggal_pasien.checkin_date, "%Y-%m-%dT%H:%i:%s"), "+07:00")
                ) AS arrived_start_date,
                COALESCE(
                    CONCAT(DATE_FORMAT(poli_ttv.created_date, "%Y-%m-%dT%H:%i:%s"),"+07:00"),
                    CONCAT(DATE_FORMAT(tpoliklinik_pendaftaran.tanggaldaftar, "%Y-%m-%dT%H:%i:%s"), "+07:00")
                ) AS arrived_end_date,
                COALESCE(
                    CONCAT(DATE_FORMAT(poli_ttv.created_date, "%Y-%m-%dT%H:%i:%s"),"+07:00"),
                    CONCAT(DATE_FORMAT(tpoliklinik_pendaftaran.tanggaldaftar, "%Y-%m-%dT%H:%i:%s"), "+07:00")
                ) AS inprogress_start_date,
                COALESCE(
                    CONCAT(DATE_FORMAT(tkasir.tanggal, "%Y-%m-%dT%H:%i:%s"), "+07:00"),
                    CONCAT(DATE_FORMAT(tpoliklinik_pendaftaran.tanggaldaftar, "%Y-%m-%dT%H:%i:%s"), "+07:00")
                ) AS inprogress_end_date,
                COALESCE(
                    CONCAT(DATE_FORMAT(tkasir.tanggal, "%Y-%m-%dT%H:%i:%s"), "+07:00"),
                    CONCAT(DATE_FORMAT(tpoliklinik_pendaftaran.tanggaldaftar, "%Y-%m-%dT%H:%i:%s"), "+07:00")
                ) AS finished_start_date,
                COALESCE(
                    CONCAT(DATE_FORMAT(tkasir.tanggal, "%Y-%m-%dT%H:%i:%s"), "+07:00"),
                    CONCAT(DATE_FORMAT(tpoliklinik_pendaftaran.tanggaldaftar, "%Y-%m-%dT%H:%i:%s"), "+07:00")
                ) AS finished_end_date,
                satu_sehat_location.location_id AS location_id,
                satu_sehat_location.location_description AS location_name,
                IF (satu_sehat_condition.condition_id IS NOT NULL, satu_sehat_condition.condition_id, icd_10.kode) AS diagnosis_condition_id,
                IF (satu_sehat_condition.condition_name IS NOT NULL, satu_sehat_condition.condition_name, icd_10.deskripsi) AS diagnosis_condition_name,
                "DD" AS diagnosis_code,
                "Discharge diagnosis" AS diagnosis_name,
                (SELECT organization_id FROM satu_sehat_organization LIMIT 1) AS organization_id,
                tpoliklinik_pendaftaran.nopendaftaran AS reference_transacation,
                tpoliklinik_pendaftaran.no_medrec AS reference_medical_record,
                tpoliklinik_pendaftaran.namapasien AS reference_patient_name,
                mdokter.nama AS reference_doctor_name
            FROM
                tpoliklinik_pendaftaran
            LEFT JOIN mdokter ON mdokter.id = tpoliklinik_pendaftaran.iddokter
            LEFT JOIN mppa ON mppa.pegawai_id = mdokter.id AND mppa.tipepegawai = 2
            LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
            LEFT JOIN tkasir ON tkasir.idtipe IN (1, 2) AND tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.status = 2
            LEFT JOIN app_reservasi_tanggal ON app_reservasi_tanggal.tanggal = DATE(tpoliklinik_pendaftaran.tanggal) AND app_reservasi_tanggal.reservasi_tipe_id = 1 AND app_reservasi_tanggal.iddokter = tpoliklinik_pendaftaran.iddokter AND app_reservasi_tanggal.idpoli = tpoliklinik_pendaftaran.idpoliklinik
            LEFT JOIN mjadwal_dokter ON mjadwal_dokter.id = app_reservasi_tanggal.jadwal_id
            LEFT JOIN mlokasi_ruangan ON mlokasi_ruangan.poliklinik_id = tpoliklinik_pendaftaran.idpoliklinik
            LEFT JOIN satu_sehat_location ON satu_sehat_location.reference_location_id = mlokasi_ruangan.id
            LEFT JOIN satu_sehat_patient ON satu_sehat_patient.pasien_id = tpoliklinik_pendaftaran.idpasien
            LEFT JOIN satu_sehat_practitioner ON satu_sehat_practitioner.ppa_id = mppa.id
            LEFT JOIN satu_sehat_encounter ON satu_sehat_encounter.transaction_id = tpoliklinik_pendaftaran.id
            LEFT JOIN satu_sehat_condition ON satu_sehat_condition.encounter_id = satu_sehat_encounter.encounter_id
            LEFT JOIN trm_layanan_berkas ON trm_layanan_berkas.id_trx = tpoliklinik_pendaftaran.id
            LEFT JOIN trm_berkas_icd10 ON trm_berkas_icd10.idberkas = trm_layanan_berkas.id AND trm_berkas_icd10.jenis_id = 1 AND trm_berkas_icd10.STATUS = 1 
            LEFT JOIN icd_10 ON icd_10.id = trm_berkas_icd10.icd_id 
            LEFT JOIN app_reservasi_tanggal_pasien ON app_reservasi_tanggal_pasien.id = tpoliklinik_pendaftaran.reservasi_id
            LEFT JOIN (SELECT pendaftaran_id, created_date FROM tpoliklinik_ttv WHERE pendaftaran_id = "' . $id . '" AND status_ttv = 2 ORDER BY id DESC LIMIT 1) AS poli_ttv ON poli_ttv.pendaftaran_id = tpoliklinik_pendaftaran.id
            WHERE
                tpoliklinik_pendaftaran.id = ' . $id . '
                AND tpoliklinik_pendaftaran.status = 1 AND tpoliklinik_pendaftaran.status_reservasi = 3
                AND satu_sehat_location.location_status = "active"
            GROUP BY
                tpoliklinik_pendaftaran.id');

        // print_r($this->db->last_query());exit();
        
        return $query->row();
    }

    public function saveData($data)
    {
        $this->db->on_duplicate('satu_sehat_encounter', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            $this->error_message = 'Penyimpanan Gagal';
            return false;
        }
    }
}
