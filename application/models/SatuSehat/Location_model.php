<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Location_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('satu_sehat_location');

        return $query->row();
    }

    public function saveData($data)
    {
        $this->db->on_duplicate('satu_sehat_location', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            $this->error_message = 'Penyimpanan Gagal';
            return false;
        }
    }
}
