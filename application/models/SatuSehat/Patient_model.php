<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Patient_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->select('
            mfpasien.id,
            satu_sehat_patient.patient_id,
            COALESCE(
                CASE 
                    WHEN mfpasien.ktp IS NULL OR mfpasien.ktp = "" OR mfpasien.ktp = "-" THEN mfpasien.nik 
                    ELSE mfpasien.ktp 
                END, 
                mfpasien.nik
            ) AS identifier,
            mfpasien.nik,
            mfpasien.ktp,
            mfpasien.nama AS name,
            mfpasien.telepon AS phone,
            (CASE
                WHEN mfpasien.jenis_kelamin = 1 THEN "male" 
                ELSE "female" END
            ) AS gender,
            mfpasien.tempat_lahir AS birth_place,
            DATE(mfpasien.tanggal_lahir) AS birth_date,
            mfpasien.alamat_jalan_ktp AS address_home,
            mfpasien.tempat_lahir AS city,
            mfpasien.kodepos_ktp AS postal_code,
            mfpasien.provinsi_id_ktp AS province_code,
            mfpasien.kabupaten_id_ktp AS city_code,
            mfpasien.kecamatan_id_ktp AS district_code,
            mfpasien.kelurahan_id_ktp AS village_code,
            mfpasien.rt_ktp AS rt,
            mfpasien.rw_ktp AS rw');
        $this->db->join('satu_sehat_patient', 'satu_sehat_patient.patient_id = mfpasien.id', 'LEFT');
        $this->db->where('mfpasien.id', $id);
        $query = $this->db->get('mfpasien');

        return $query->row();
    }

    public function saveData()
    {
        $data = array(
            'pasien_id' => $_POST['id'],
            'patient_id' => $_POST['patient_id'],
            'name' => $_POST['patient_name'],
            'gender' => $_POST['patient_gender'],
            'birthdate' => $_POST['patient_birthdate']
        );

        $this->db->on_duplicate('satu_sehat_patient', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            $this->error_message = 'Penyimpanan Gagal';
            return false;
        }
    }

    public function saveHistory($patient_id, $type, $identifier, $status) {
        $data = [
            'patient_id' => $patient_id,
            'date' => date("Y-m-d"),
            'type' => $type,
            'identifier' => $identifier,
            'status' => $status,
            'user_process' => $this->session->userdata('user_id')
        ];

        $this->db->insert('satu_sehat_patient_history', $data);
    }

    public function getHistoryById($id) {
        $this->db->select('satu_sehat_patient_history.*, musers.name AS user_name');
        $this->db->from('satu_sehat_patient_history');
        $this->db->join('musers', 'musers.id = satu_sehat_patient_history.user_process');
        $this->db->where('satu_sehat_patient_history.patient_id', $id);
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getPatientsByDateRange($startDate, $endDate) {
        $this->db->select('mfpasien.id, mfpasien.nik, mfpasien.ktp,
            mfpasien.nama AS name,
            (CASE
                WHEN mfpasien.jenis_kelamin = 1 THEN "male" 
                ELSE "female" END
            ) AS gender,
            DATE(mfpasien.tanggal_lahir) AS birth_date');
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.idpasien = mfpasien.id', 'LEFT');
        $this->db->join('satu_sehat_patient', 'satu_sehat_patient.patient_id = mfpasien.id', 'LEFT');
        $this->db->where('tpoliklinik_pendaftaran.tanggal >=', $startDate);
        $this->db->where('tpoliklinik_pendaftaran.tanggal <=', $endDate);
        $this->db->where('satu_sehat_patient.patient_id IS NULL');
        $this->db->group_by('mfpasien.id');
        $query = $this->db->get('mfpasien');
        return $query->result();
    }

}
