<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Practitioner_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->select('mppa.id,
            mppa.nik AS ppa_nik,
            mppa.nama AS ppa_nama,
            satu_sehat_practitioner.practitioner_id,
            satu_sehat_practitioner.name AS practitioner_name,
            satu_sehat_practitioner.gender AS practitioner_gender,
            satu_sehat_practitioner.birthdate AS practitioner_birthdate');
        $this->db->join('satu_sehat_practitioner', 'satu_sehat_practitioner.ppa_id = mppa.id', 'LEFT');
        $this->db->where('mppa.id', $id);
        $query = $this->db->get('mppa');

        return $query->row();
    }

    public function saveData()
    {
        $data = array(
            'ppa_id' => $_POST['id'],
            'practitioner_id' => $_POST['practitioner_id'],
            'name' => $_POST['practitioner_name'],
            'gender' => $_POST['practitioner_gender'],
            'birthdate' => $_POST['practitioner_birthdate']
        );

        $this->db->on_duplicate('satu_sehat_practitioner', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            $this->error_message = 'Penyimpanan Gagal';
            return false;
        }
    }
}
