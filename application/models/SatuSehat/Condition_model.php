<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Condition_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $query = $this->db->query('
            SELECT
                satu_sehat_condition.id,
                satu_sehat_encounter.encounter_id,
                satu_sehat_encounter.transaction_id,
                satu_sehat_encounter.status_encounter,
                satu_sehat_encounter.patient_id,
                satu_sehat_encounter.patient_name,
                satu_sehat_encounter.practitioner_name,
                satu_sehat_encounter.location_name,
                satu_sehat_encounter.diagnosis_condition_name,
                tpoliklinik_pendaftaran.nopendaftaran AS reference_transacation,
                tpoliklinik_pendaftaran.no_medrec AS reference_medical_record,
                tpoliklinik_pendaftaran.namapasien AS reference_patient_name,
                mdokter.nama AS reference_doctor_name,
                satu_sehat_condition.condition_id,
                COALESCE(satu_sehat_condition.category_code, "encounter-diagnosis") AS category_code,
                COALESCE(satu_sehat_condition.category_name, "Encounter Diagnosis") AS category_name,
                COALESCE(satu_sehat_condition.condition_code, satu_sehat_encounter.diagnosis_condition_id) AS condition_code,
                COALESCE(satu_sehat_condition.condition_name, satu_sehat_encounter.diagnosis_condition_name) AS condition_name,
                COALESCE(satu_sehat_condition.encounter_display, 
                    CONCAT(
                        "Kunjungan ", 
                        tpoliklinik_pendaftaran.namapasien, 
                        " di hari ", 
                        CONCAT(
                            CASE DAYOFWEEK(tpoliklinik_pendaftaran.tanggal)
                                WHEN 1 THEN "Minggu"
                                WHEN 2 THEN "Senin"
                                WHEN 3 THEN "Selasa"
                                WHEN 4 THEN "Rabu"
                                WHEN 5 THEN "Kamis"
                                WHEN 6 THEN "Jumat"
                                WHEN 7 THEN "Sabtu"
                            END,
                            DATE_FORMAT(tpoliklinik_pendaftaran.tanggal, ", %d "),
                            CASE MONTH(tpoliklinik_pendaftaran.tanggal)
                                WHEN 1 THEN "Januari"
                                WHEN 2 THEN "Februari"
                                WHEN 3 THEN "Maret"
                                WHEN 4 THEN "April"
                                WHEN 5 THEN "Mei"
                                WHEN 6 THEN "Juni"
                                WHEN 7 THEN "Juli"
                                WHEN 8 THEN "Agustus"
                                WHEN 9 THEN "September"
                                WHEN 10 THEN "Oktober"
                                WHEN 11 THEN "November"
                                WHEN 12 THEN "Desember"
                            END,
                            DATE_FORMAT(tpoliklinik_pendaftaran.tanggal, " %Y")
                        )
                    )
                ) AS encounter_display,
                IF
                    ( satu_sehat_condition.condition_id <> "", 1, 0 ) AS status_connect
            FROM
                satu_sehat_encounter
            LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = satu_sehat_encounter.transaction_id
            LEFT JOIN mdokter ON mdokter.id = tpoliklinik_pendaftaran.iddokter
            LEFT JOIN satu_sehat_condition ON satu_sehat_condition.encounter_id = satu_sehat_encounter.encounter_id
            WHERE
                satu_sehat_encounter.id = ' . $id);

        return $query->row();
    }

    public function saveData($data)
    {
        $this->db->on_duplicate('satu_sehat_condition', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            $this->error_message = 'Penyimpanan Gagal';
            return false;
        }
    }
}
