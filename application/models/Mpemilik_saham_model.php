<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpemilik_saham_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $q="SELECT 
			M.id,M.tipe_pemilik
			,CASE WHEN M.tipe_pemilik='1' THEN MP.nama WHEN M.tipe_pemilik='2' THEN MD.nama ELSE M.nama END as nama
			,CASE WHEN M.tipe_pemilik='1' THEN 'PEGAWAI' WHEN M.tipe_pemilik='2' THEN 'DOKTER' ELSE 'NON PEGAWAI' END as tipe_nama
			,M.npwp,M.keterangan,M.`status`,M.idpeg_dok
			from mpemilik_saham M
			LEFT JOIN mpegawai MP ON MP.id=M.idpeg_dok AND M.tipe_pemilik='1'
			LEFT JOIN mdokter MD ON MD.id=M.idpeg_dok AND M.tipe_pemilik='2'			
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	public function list_pegawai()
    {
        $this->db->where('status', 1);
        $this->db->order_by('nama','ASC');
        $query = $this->db->get('mpegawai');
        return $query->result();
    }
	public function list_dokter()
    {
        $this->db->where('status', 1);
        $this->db->order_by('nama','ASC');
        $query = $this->db->get('mdokter');
        return $query->result();
    }
	public function list_bank()
    {
        // $this->db->where('status', 1);
        $this->db->order_by('bank','ASC');
        $query = $this->db->get('ref_bank');
        return $query->result();
    }

    public function getStatusAvailableApoteker()
    {
        $this->db->where('idkategori', 6);
        $query = $this->db->get('mpemilik_saham');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function saveData()
    {
        $this->tipe_pemilik 	= $_POST['tipe_pemilik'];
        $this->nama 	= $_POST['nama'];
		if ($this->input->post('tipe_pemilik')=='1'){
			$this->idpeg_dok 	= $_POST['idpegawai'];			
			$this->nama 	= '';			
		}
		if ($this->input->post('tipe_pemilik')=='2'){
			$this->idpeg_dok 	= $_POST['iddokter'];			
			$this->nama 	= '';			
		}
		if ($this->input->post('tipe_pemilik')=='3'){
			$this->idpeg_dok 	= null;			
		}
        $this->npwp 			= $_POST['npwp'];
        $this->keterangan 				= $_POST['keterangan'];
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mpemilik_saham', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
       $this->tipe_pemilik 	= $_POST['tipe_pemilik'];
        $this->nama 	= $_POST['nama'];
		if ($this->input->post('tipe_pemilik')=='1'){
			$this->idpeg_dok 	= $_POST['idpegawai'];			
			$this->nama 	= '';			
		}
		if ($this->input->post('tipe_pemilik')=='2'){
			$this->idpeg_dok 	= $_POST['iddokter'];			
			$this->nama 	= '';			
		}
		if ($this->input->post('tipe_pemilik')=='3'){
			$this->idpeg_dok 	= null;			
		}
        $this->npwp 			= $_POST['npwp'];
        $this->keterangan 				= $_POST['keterangan'];
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mpemilik_saham', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mpemilik_saham', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mpemilik_saham', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
