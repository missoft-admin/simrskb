<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Apm_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function get_index_setting(){
		$q="SELECT *
			 FROM apm_setting
			WHERE id='1'";
		return $this->db->query($q)->row_array();
	}
	function list_running(){
		$q="SELECT * FROM apm_setting_running_text M WHERE M.`status`='1' ORDER BY M.id";
		return $this->db->query($q)->result();
	}
	function list_slider(){
		$q="SELECT * FROM apm_setting_file M WHERE M.`status`='1' ORDER BY M.id";
		return $this->db->query($q)->result();
	}
	
	
}
