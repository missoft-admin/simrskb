<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tpiutang_rincian_model extends CI_Model
{
  
   public function detail($id){
	   $q="SELECT TK.id,TK.no_piutang,TK.tanggal_id,TK.idpengaturan,
					TK.jml_faktur,TK.total_tagihan,TK.tanggal_tagihan,TK.`status`,TK.status_kirim
					,S.deskripsi
					FROM tpiutang TK
					LEFT JOIN mpiutang_setting S ON S.id=TK.tanggal_id
					WHERE TK.id='$id'";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function detail_trx($id,$tipe,$idpeg){
	   $where='';
	   if ($tipe=='1'){
		   $where=" AND D.idpegawai='$idpeg'";
	   }else{
		   $where=" AND D.iddokter='$idpeg'";
	   }
	   $q="SELECT TK.id,TK.no_piutang,TK.tanggal_id,TK.idpengaturan,
					TK.jml_faktur,TK.total_tagihan,TK.tanggal_tagihan,TK.`status`,TK.status_kirim
					,S.deskripsi,CASE WHEN D.tipepegawai=1 THEN MP.nama ELSE MD.nama END as atas_nama,D.iddokter,D.idpegawai,D.tipepegawai,D.tipe
					FROM tpiutang TK
					LEFT JOIN mpiutang_setting S ON S.id=TK.tanggal_id
					LEFT JOIN tpiutang_detail D ON D.piutang_id=TK.id
					LEFT JOIN mdokter MD ON MD.id=D.iddokter AND D.tipepegawai='2'
					LEFT JOIN mpegawai MP ON MP.id=D.idpegawai AND D.tipepegawai='1'
					WHERE TK.id='$id' AND D.tipepegawai='$tipe' ".$where."
					GROUP BY TK.id
					";
		// print_r($q);exit();
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function list_kelompok_pasien(){
	   $q="SELECT id,nama from mpasien_kelompok 
				WHERE mpasien_kelompok.`status`='1' AND mpasien_kelompok.id !='5'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_deskripsi(){
	   $q="SELECT D.id,CONCAT(H.nama,' - ',D.deskripsi) as nama FROM mpiutang H
INNER JOIN mpiutang_setting D ON H.id=D.idpengaturan
WHERE H.`status`='1'
ORDER BY H.nama ASC,D.tanggal_hari ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_rekanan(){
	   $q="SELECT id,nama from mrekanan 
				WHERE mrekanan.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
