<?php

class Tverifikasi_deposit_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getPotonganDokter($iddokter, $idasalpasien)
	{
		if ($idasalpasien != 3) {
			// Rawat Inap
			$this->db->select('potonganrspagi AS potonganrs, pajak');
		} else {
			// Rawat Jalan & IGD
			$this->db->select('potonganrsranap AS potonganrs, pajakranap AS pajak');
		}
		$this->db->where('id', $iddokter);
		$this->db->where('status', 1);
		$query = $this->db->get('mdokter');
		return $query->row();
	}

	public function getNominalStatus($idradiologi, $idstatus, $idkelas)
	{
		if ($idstatus == 1) {
			// Status : Langsung
			$this->db->select('mtarif_radiologi_detail.nominallangsung AS nominalstatus');
		} elseif ($idstatus == 2) {
			// Status : Menolak
			$this->db->select('mtarif_radiologi_detail.nominalmenolak AS nominalstatus');
		} elseif ($idstatus == 3) {
			// Status : Kembali
			$this->db->select('mtarif_radiologi_detail.nominalkembali AS nominalstatus');
		}
		$this->db->join('mtarif_radiologi_detail', 'mtarif_radiologi_detail.idtarif = mtarif_radiologi.id');
		$this->db->where('mtarif_radiologi_detail.kelas', $idkelas);
		$this->db->where('mtarif_radiologi.id', $idradiologi);
		$this->db->where('mtarif_radiologi.status', 1);
		$query = $this->db->get('mtarif_radiologi');
		return $query->row();
	}

	public function getHeader($id)
	{
		$q = "SELECT H.id,H.namapasien,H.no_medrec,H.nopendaftaran,H.tanggaldaftar,KL.nama as kelas,MB.nama as bed 
				,MR.nama as ruangan
				FROM `trawatinap_pendaftaran` H
				LEFT JOIN mkelas KL ON KL.id=H.idkelas
				LEFT JOIN mbed MB ON MB.id=H.idbed
				LEFT JOIN mruangan MR ON MR.id=H.idruangan
				WHERE H.id='$id'";
		$query = $this->db->query($q);
		return $query->row_array();
	}

	public function getList($idrawatinap, $id = '')
	{
		$where = '';
		if ($id != '') {
			$where = " AND H.id='$id'";
		}
		$q = "SELECT H.id,H.nodeposit,H.tanggal,H.idmetodepembayaran,H.idbank,M.nama as bank,H.iduserinput 
,U.`name` as nama_user,H.nominal,H.terimadari
FROM trawatinap_deposit H
LEFT JOIN mbank M ON M.id=H.idbank
LEFT JOIN musers U ON U.id=H.iduserinput
WHERE H.idrawatinap='$idrawatinap' AND H.`status`='1' " . $where;
		$query = $this->db->query($q);
		return $query->result();
	}
}
