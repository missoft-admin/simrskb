<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tkontrabon_verifikasi_model extends CI_Model
{
   public function list_distributor(){
	   $q="SELECT id,nama from mdistributor 
				WHERE mdistributor.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function insert_tretur_terima($tpengembalian_id,$tpenerimaan_id=''){
	   $q="SELECT H.id as idpengembalian
			,'$tpenerimaan_id' as idpenerimaan,H.tanggal as tanggal_pengembalian
			,P.tanggalpenerimaan as tanggal_penerimaan,H.nopengembalian,P.nopenerimaan as nopenerimaan
			,H.iddistributor,M.nama as distributor,H.jenis_retur,0 as nominal_retur
			,0 as nominal_ganti
			,A.id as idpenerimaan_asal,A.tanggalpenerimaan tanggal_penerimaan_asal,A.nopenerimaan as nopenerimaan_asal
			FROM `tgudang_pengembalian` H
			LEFT JOIN tgudang_penerimaan P ON P.id='$tpenerimaan_id'
			LEFT JOIN tgudang_penerimaan A ON A.id=H.idpenerimaan
			LEFT JOIN mdistributor M ON M.id=H.iddistributor
			WHERE H.id='$tpengembalian_id'";
		$header=$this->db->query($q)->row_array();
		
		$header['created_by'] = $this->session->userdata('user_id');
		$header['created_nama']=$this->session->userdata('user_name');
		$header['created_date'] =date('Y-m-d H:i:s');
		$this->db->insert('tretur_penerimaan',$header);
		$idretur=$this->db->insert_id();
		$q="SELECT D.id as  iddet_trx,D.idtipe,B.idkategori,D.idbarang,D.nobatch,D.kuantitas,D.opsisatuan,D.kuantitas_terima
			,D.harga,D.tot_harga,D.nominal_ppn,D.nominal_diskon,D.total_ppn,D.total_diskon
			,D.id_penerimaan_detail,D.expired_date,B.nama as namabarang
			FROM tgudang_pengembalian_detail D
			LEFT JOIN view_barang_all B ON B.idtipe=D.idtipe AND B.id=D.idbarang
			WHERE D.idpengembalian='$tpengembalian_id'";
		$rows=$this->db->query($q)->result();
		foreach ($rows as $r){
			$detail=array(
				'idretur' =>$idretur,
				'iddet_trx' =>$r->iddet_trx,
				'idtipe' =>$r->idtipe,
				'idkategori' =>$r->idkategori,
				'idbarang' =>$r->idbarang,
				'namabarang' =>$r->namabarang,
				'nobatch' =>$r->nobatch,
				'kuantitas' =>$r->kuantitas,
				'opsisatuan' =>$r->opsisatuan,
				'kuantitas_terima' =>$r->kuantitas_terima,
				'harga' =>$r->harga,
				'tot_harga' =>$r->tot_harga,
				'nominal_harga' =>$r->tot_harga + $r->total_diskon - $r->total_ppn,
				'nominal_diskon' =>$r->nominal_diskon,
				'nominal_ppn' =>$r->nominal_ppn,
				'total_diskon' =>$r->total_diskon,
				'total_ppn' =>$r->total_ppn,
				'status' =>1,
			);
			$this->db->insert('tretur_penerimaan_detail',$detail);
		}
		$q="SELECT D.id as  iddet_trx,D.idtipe,B.idkategori,D.idbarang,D.nobatch,D.kuantitas,D.opsisatuan,'0' as kuantitas_terima
			,D.harga,D.totalharga as tot_harga,D.nominalppn as nominal_ppn,D.nominaldiskon as nominal_diskon,D.nominalppn * D.kuantitas as total_ppn,D.nominaldiskon*D.kuantitas as total_diskon
			,D.id as id_penerimaan_detail,B.nama as namabarang
			FROM tgudang_penerimaan_detail D
			LEFT JOIN view_barang_all B ON B.idtipe=D.idtipe AND B.id=D.idbarang
			WHERE D.idpenerimaan='$tpenerimaan_id'";
		$rows=$this->db->query($q)->result();
		foreach ($rows as $r){
			$detail=array(
				'idretur' =>$idretur,
				'iddet_trx' =>$r->iddet_trx,
				'idtipe' =>$r->idtipe,
				'idkategori' =>$r->idkategori,
				'idbarang' =>$r->idbarang,
				'namabarang' =>$r->namabarang,
				'nobatch' =>$r->nobatch,
				'kuantitas' =>$r->kuantitas,
				'opsisatuan' =>$r->opsisatuan,
				'kuantitas_terima' =>$r->kuantitas_terima,
				'harga' =>$r->harga,
				'tot_harga' =>$r->tot_harga,
				'nominal_harga' =>$r->tot_harga + $r->total_diskon - $r->total_ppn,
				'nominal_diskon' =>$r->nominal_diskon,
				'nominal_ppn' =>$r->nominal_ppn,
				'total_diskon' =>$r->total_diskon,
				'total_ppn' =>$r->total_ppn,
				'status' =>1,
			);
			$this->db->insert('tretur_penerimaan_ganti',$detail);
		}
		return true;
   }
   public function insert_jurnal_pembelian($id){
	   
	   $tipe_bayar='1';
	   $idakun_kredit='1';
	   $q="SELECT H.id as idtransaksi
			,H.tanggalpenerimaan as tanggal_transaksi,H.nopenerimaan as notransaksi,H.tipe_bayar
			,H.tgl_terima as tanggal_terima,H.nofakturexternal,H.iddistributor,MD.nama as distributor
			,S.batas_batal,S.idakun_kredit,S.st_auto_posting
			FROM tgudang_penerimaan H
			LEFT JOIN msetting_jurnal_pembelian S ON S.id='1'
			LEFT JOIN mdistributor MD ON MD.id=H.iddistributor
			WHERE H.id='$id'";
		$row=$this->db->query($q)->row();
		$st_auto_posting=$row->st_auto_posting;
		$tipe_bayar=$row->tipe_bayar;
		$idakun_kredit=$row->idakun_kredit;
		$data_header=array(
			'idtransaksi' => $row->idtransaksi,
			'asal_trx' => 1,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'tipe_bayar' => $row->tipe_bayar,
			'tanggal_terima' => $row->tanggal_terima,
			'nofakturexternal' => $row->nofakturexternal,
			'iddistributor' => $row->iddistributor,
			'distributor' => $row->distributor,
			'status' => 1,
			'st_posting' => 0,
			'st_auto_posting' => $row->st_auto_posting,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
            'created_date'=>date('Y-m-d H:i:s')
			

		);
		$st_auto_posting=$row->st_auto_posting;
		$this->db->insert('tvalidasi_gudang',$data_header);
		$idvalidasi=$this->db->insert_id();
		$q="SELECT T.*
			,AB.noakun as noakun_beli,AB.namaakun as namaakun_beli 
			,AP.noakun as noakun_ppn,AP.namaakun as namaakun_ppn
			,AD.noakun as noakun_diskon,AD.namaakun as namaakun_diskon
			FROM (SELECT H.id,H.nofakturexternal,D.idtipe,B.idkategori,D.idbarang
			,K.nama as nama_kategori,B.nama as nama_barang
			,compare_value(MAX(IF(SB.idbarang = D.idbarang,SB.idakun,NULL)),
										MAX(IF(SB.idbarang = 0 AND SB.idkategori = B.idkategori,SB.idakun,NULL)),
									  MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe = D.idtipe ,SB.idakun,NULL))) idakun_beli

			,compare_value(MAX(IF(SP.idbarang = D.idbarang,SP.idakun,NULL)),
				MAX(IF(SP.idbarang = 0 AND SP.idkategori = B.idkategori,SP.idakun,NULL)),
			  MAX(IF(SP.idbarang = 0 AND SP.idkategori = 0 AND SP.idtipe = D.idtipe ,SP.idakun,NULL))) idakun_ppn
			,compare_value(MAX(IF(SD.idbarang = D.idbarang,SD.idakun,NULL)),
				MAX(IF(SD.idbarang = 0 AND SD.idkategori = B.idkategori,SD.idakun,NULL)),
			  MAX(IF(SD.idbarang = 0 AND SD.idkategori = 0 AND SD.idtipe = D.idtipe ,SD.idakun,NULL))) idakun_diskon
				,(D.harga_asli * D.kuantitas) as nominal_beli
				,(D.nominalppn * D.kuantitas) as nominal_ppn
				,(D.nominaldiskon * D.kuantitas) as nominal_diskon,D.id as iddet
				
			FROM tgudang_penerimaan H
			INNER JOIN tgudang_penerimaan_detail D ON D.idpenerimaan=H.id AND D.`status`='1'
			INNER JOIN view_barang_all B ON B.idtipe=D.idtipe AND B.id=D.idbarang
			LEFT JOIN mdata_kategori K ON K.id=B.idkategori
			LEFT JOIN msetting_jurnal_pembelian_beli SB ON SB.idtipe=D.idtipe
			LEFT JOIN msetting_jurnal_pembelian_ppn SP ON SP.idtipe=D.idtipe
			LEFT JOIN msetting_jurnal_pembelian_diskon SD ON SD.idtipe=D.idtipe

			WHERE H.id='$id'
			GROUP BY  D.id
			) T
			LEFT JOIN makun_nomor AB ON AB.id=T.idakun_beli
			LEFT JOIN makun_nomor AP ON AP.id=T.idakun_ppn
			LEFT JOIN makun_nomor AD ON AD.id=T.idakun_diskon";
		$rows=$this->db->query($q)->result();
		$total_transaksi=0;
		foreach($rows as $r){
			$data_detail=array(
				'idvalidasi' => $idvalidasi ,
				'idpenerimaan' => $id,
				'idet' => $r->iddet,
				'asal_trx' => 1,
				'idtipe' => $r->idtipe,
				'idkategori' => $r->idkategori,
				'idbarang' => $r->idbarang,
				'nama_barang' => $r->nama_barang,
				'nominal_beli' => $r->nominal_beli,
				'nominal_ppn' => $r->nominal_ppn,
				'nominal_diskon' => $r->nominal_diskon,
				'idakun_beli' => $r->idakun_beli,
				'idakun_ppn' => $r->idakun_ppn,
				'idakun_diskon' => $r->idakun_diskon,
				'noakun_beli' => $r->noakun_beli,
				'namaakun_beli' => $r->namaakun_beli,
				'noakun_ppn' => $r->noakun_ppn,
				'namaakun_ppn' => $r->namaakun_ppn,
				'noakun_diskon' => $r->noakun_diskon,
				'namaakun_diskon' => $r->namaakun_diskon,
				'posisi_beli' => 'D',
				'posisi_ppn' => 'D',
				'posisi_diskon' => 'K',
			);
			$total_transaksi=$total_transaksi + $r->nominal_beli + $r->nominal_ppn - $r->nominal_diskon;
			$this->db->insert('tvalidasi_gudang_detail',$data_detail);
		}
		//INSERT PEMBAYARAN
		if ($tipe_bayar=='1'){//Tunai
			$q="SELECT SK.idakun,SK.noakun,A.namaakun,B.nominal_bayar as nominal,'K' as posisi_akun
				FROM tgudang_penerimaan_pembayaran B
				INNER JOIN msumber_kas SK ON SK.id=B.sumber_kas_id
				INNER JOIN makun_nomor A ON A.id=SK.idakun
				WHERE B.penerimaan_id='$id' AND B.status='1'";			
		}else{
			$q="SELECT id as idakun,A.noakun,A.namaakun,".$total_transaksi." as nominal,'K' as posisi_akun FROM makun_nomor A WHERE A.id='$idakun_kredit'";			
		}
		// print_r($q);exit();
		$rows=$this->db->query($q)->result();
		foreach($rows as $r){
			$data_detail=array(
				'idvalidasi' => $idvalidasi ,
				'idpenerimaan' => $id,
				'idakun' => $r->idakun,
				'noakun' => $r->noakun,
				'namaakun' => $r->namaakun,
				'nominal' => $r->nominal,
				'posisi_akun' => $r->posisi_akun,
				
			);
			$this->db->insert('tvalidasi_gudang_bayar',$data_detail);
		}
		if ($st_auto_posting=='1'){
			$data_header=array(
				
				'st_posting' => 1,
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
				

			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_gudang',$data_header);
		}
		return true;
   }
    public function insert_jurnal_pembelian_pengajuan($id){
	   
	   $tipe_bayar='1';
	   $idakun_kredit='1';
	   $q="SELECT H.id as idtransaksi
			,H.tanggal_pengajuan as tanggal_transaksi,H.no_pengajuan as notransaksi,2 as tipe_bayar
			,H.tanggal_pengajuan as tanggal_terima,' ' as nofakturexternal,H.idvendor as iddistributor,MD.nama as distributor
			,S.batas_batal,S.idakun_kredit,S.st_auto_posting
			FROM rka_pengajuan H
			LEFT JOIN msetting_jurnal_pembelian S ON S.id='1'
			LEFT JOIN mvendor MD ON MD.id=H.idvendor
			WHERE H.id='$id'";
		$row=$this->db->query($q)->row();
		$st_auto_posting=$row->st_auto_posting;
		$tipe_bayar=$row->tipe_bayar;
		$idakun_kredit=$row->idakun_kredit;
		$data_header=array(
			'idtransaksi' => $row->idtransaksi,
			'asal_trx' => 2,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'tipe_bayar' => $row->tipe_bayar,
			'tanggal_terima' => $row->tanggal_terima,
			'nofakturexternal' => $row->nofakturexternal,
			'iddistributor' => $row->iddistributor,
			'distributor' => $row->distributor,
			'status' => 1,
			'st_posting' => 0,
			'st_auto_posting' => $row->st_auto_posting,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
            'created_date'=>date('Y-m-d H:i:s')
			

		);
		$st_auto_posting=$row->st_auto_posting;
		$this->db->insert('tvalidasi_gudang',$data_header);
		$idvalidasi=$this->db->insert_id();
		$q="SELECT D.id as iddet,D.nama_barang
			,(D.kuantitas*D.harga_pokok) as nominal_beli 
			,(D.kuantitas*D.harga_ppn) as nominal_ppn 
			,(D.kuantitas*D.harga_diskon) as nominal_diskon,M.idakun as idakun_beli,M.idakun_ppn as idakun_ppn,M.idakun_diskon as idakun_diskon
			,AB.noakun as noakun_beli,AB.namaakun as namaakun_beli
			,AP.noakun as noakun_ppn,AP.namaakun as namaakun_ppn
			,AD.noakun as noakun_diskon,AD.namaakun as namaakun_diskon
			FROM rka_pengajuan H
			LEFT JOIN rka_pengajuan_detail D ON D.idpengajuan=H.id
			LEFT JOIN mklasifikasi M ON M.id=H.idklasifikasi
			LEFT JOIN makun_nomor AB ON AB.id=M.idakun
			LEFT JOIN makun_nomor AP ON AP.id=M.idakun_ppn
			LEFT JOIN makun_nomor AD ON AD.id=M.idakun_diskon
			WHERE H.id='$id'";
		$rows=$this->db->query($q)->result();
		$total_transaksi=0;
		foreach($rows as $r){
			$data_detail=array(
				'idvalidasi' => $idvalidasi ,
				'idpenerimaan' => $id,
				'idet' => $r->iddet,
				// 'idtipe' => $r->idtipe,
				// 'idkategori' => $r->idkategori,
				// 'idbarang' => $r->idbarang,
				'asal_trx' => 2,
				'nama_barang' => $r->nama_barang,
				'nominal_beli' => $r->nominal_beli,
				'nominal_ppn' => $r->nominal_ppn,
				'nominal_diskon' => $r->nominal_diskon,
				'idakun_beli' => $r->idakun_beli,
				'idakun_ppn' => $r->idakun_ppn,
				'idakun_diskon' => $r->idakun_diskon,
				'noakun_beli' => $r->noakun_beli,
				'namaakun_beli' => $r->namaakun_beli,
				'noakun_ppn' => $r->noakun_ppn,
				'namaakun_ppn' => $r->namaakun_ppn,
				'noakun_diskon' => $r->noakun_diskon,
				'namaakun_diskon' => $r->namaakun_diskon,
				'posisi_beli' => 'D',
				'posisi_ppn' => 'D',
				'posisi_diskon' => 'K',
			);
			$total_transaksi=$total_transaksi + $r->nominal_beli + $r->nominal_ppn - $r->nominal_diskon;
			$this->db->insert('tvalidasi_gudang_detail',$data_detail);
		}
		//INSERT PEMBAYARAN
		
			$q="SELECT id as idakun,A.noakun,A.namaakun,".$total_transaksi." as nominal,'K' as posisi_akun FROM makun_nomor A WHERE A.id='$idakun_kredit'";			
		// print_r($q);exit();
		$rows=$this->db->query($q)->result();
		foreach($rows as $r){
			$data_detail=array(
				'idvalidasi' => $idvalidasi ,
				'idpenerimaan' => $id,
				'idakun' => $r->idakun,
				'noakun' => $r->noakun,
				'namaakun' => $r->namaakun,
				'nominal' => $r->nominal,
				'posisi_akun' => $r->posisi_akun,
				
			);
			$this->db->insert('tvalidasi_gudang_bayar',$data_detail);
		}
		if ($st_auto_posting=='1'){
			$data_header=array(
				
				'st_posting' => 1,
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
				

			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_gudang',$data_header);
		}
		return true;
   }
   public function insert_jurnal_pembelian_kas($id){
	   
	   $tipe_bayar='1';
	   $idakun_kredit='1';
	   $q="SELECT H.id as idtransaksi
			,H.tanggalpenerimaan as tanggal_transaksi,H.nopenerimaan as notransaksi,H.tipe_bayar
			,H.tgl_terima as tanggal_terima,H.nofakturexternal,H.iddistributor,MD.nama as distributor
			,S.batas_batal,S.idakun_kredit,S.st_auto_posting,H.totalharga as nominal
			FROM tgudang_penerimaan H
			LEFT JOIN msetting_jurnal_pembelian S ON S.id='1'
			LEFT JOIN mdistributor MD ON MD.id=H.iddistributor
			WHERE H.id='$id'";
		$row=$this->db->query($q)->row();
		$data_header=array(
			'id_reff' => 8,
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => date('Y-m-d'),
			'notransaksi' => $row->notransaksi,
			'keterangan' => 'PEMBELIAN OBAT TUNAI KE '.$row->distributor.' ('.HumanDateShort($row->tanggal_transaksi).')',
			'nominal' => $row->nominal,
			// 'idakun' => $row->idakun,
			// 'posisi_akun' => $row->posisi_akun,
			'status' => 1,
			'st_auto_posting' => $row->st_auto_posting,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
            'created_date'=>date('Y-m-d H:i:s')
		);
		// print_r($data_header);exit();
		$this->db->insert('tvalidasi_kas',$data_header);
		$idvalidasi=$this->db->insert_id();
		
		$st_auto_posting=$row->st_auto_posting;
		$idakun_kredit=$row->idakun_kredit;
		$data_header=array(
			'idvalidasi' => $idvalidasi,
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'tipe_bayar' => $row->tipe_bayar,
			'tanggal_terima' => $row->tanggal_terima,
			'nofakturexternal' => $row->nofakturexternal,
			'iddistributor' => $row->iddistributor,
			'distributor' => $row->distributor,
			'status' => 1,
			'st_posting' => 0,
			'st_auto_posting' => $row->st_auto_posting,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
            'created_date'=>date('Y-m-d H:i:s')
			

		);
		$st_auto_posting=$row->st_auto_posting;
		$this->db->insert('tvalidasi_kas_08_gudang',$data_header);
		$q="SELECT T.*
			,AB.noakun as noakun_beli,AB.namaakun as namaakun_beli 
			,AP.noakun as noakun_ppn,AP.namaakun as namaakun_ppn
			,AD.noakun as noakun_diskon,AD.namaakun as namaakun_diskon
			FROM (SELECT H.id,H.nofakturexternal,D.idtipe,B.idkategori,D.idbarang
			,K.nama as nama_kategori,B.nama as nama_barang
			,compare_value(MAX(IF(SB.idbarang = D.idbarang,SB.idakun,NULL)),
										MAX(IF(SB.idbarang = 0 AND SB.idkategori = B.idkategori,SB.idakun,NULL)),
									  MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe = D.idtipe ,SB.idakun,NULL))) idakun_beli

			,compare_value(MAX(IF(SP.idbarang = D.idbarang,SP.idakun,NULL)),
				MAX(IF(SP.idbarang = 0 AND SP.idkategori = B.idkategori,SP.idakun,NULL)),
			  MAX(IF(SP.idbarang = 0 AND SP.idkategori = 0 AND SP.idtipe = D.idtipe ,SP.idakun,NULL))) idakun_ppn
			,compare_value(MAX(IF(SD.idbarang = D.idbarang,SD.idakun,NULL)),
				MAX(IF(SD.idbarang = 0 AND SD.idkategori = B.idkategori,SD.idakun,NULL)),
			  MAX(IF(SD.idbarang = 0 AND SD.idkategori = 0 AND SD.idtipe = D.idtipe ,SD.idakun,NULL))) idakun_diskon
				,(D.harga_asli * D.kuantitas) as nominal_beli
				,(D.nominalppn * D.kuantitas) as nominal_ppn
				,(D.nominaldiskon * D.kuantitas) as nominal_diskon,D.id as iddet
				
			FROM tgudang_penerimaan H
			INNER JOIN tgudang_penerimaan_detail D ON D.idpenerimaan=H.id AND D.`status`='1'
			INNER JOIN view_barang_all B ON B.idtipe=D.idtipe AND B.id=D.idbarang
			LEFT JOIN mdata_kategori K ON K.id=B.idkategori
			LEFT JOIN msetting_jurnal_pembelian_beli SB ON SB.idtipe=D.idtipe
			LEFT JOIN msetting_jurnal_pembelian_ppn SP ON SP.idtipe=D.idtipe
			LEFT JOIN msetting_jurnal_pembelian_diskon SD ON SD.idtipe=D.idtipe

			WHERE H.id='$id'
			GROUP BY  D.id
			) T
			LEFT JOIN makun_nomor AB ON AB.id=T.idakun_beli
			LEFT JOIN makun_nomor AP ON AP.id=T.idakun_ppn
			LEFT JOIN makun_nomor AD ON AD.id=T.idakun_diskon";
		$rows=$this->db->query($q)->result();
		$total_transaksi=0;
		foreach($rows as $r){
			$data_detail=array(
				'idvalidasi' => $idvalidasi ,
				'idpenerimaan' => $id,
				'idet' => $r->iddet,
				'idtipe' => $r->idtipe,
				'idkategori' => $r->idkategori,
				'idbarang' => $r->idbarang,
				'nama_barang' => $r->nama_barang,
				'nominal_beli' => $r->nominal_beli,
				'nominal_ppn' => $r->nominal_ppn,
				'nominal_diskon' => $r->nominal_diskon,
				'idakun_beli' => $r->idakun_beli,
				'idakun_ppn' => $r->idakun_ppn,
				'idakun_diskon' => $r->idakun_diskon,
				'noakun_beli' => $r->noakun_beli,
				'namaakun_beli' => $r->namaakun_beli,
				'noakun_ppn' => $r->noakun_ppn,
				'namaakun_ppn' => $r->namaakun_ppn,
				'noakun_diskon' => $r->noakun_diskon,
				'namaakun_diskon' => $r->namaakun_diskon,
				'posisi_beli' => 'D',
				'posisi_ppn' => 'D',
				'posisi_diskon' => 'K',
			);
			$total_transaksi=$total_transaksi + $r->nominal_beli + $r->nominal_ppn - $r->nominal_diskon;
			$this->db->insert('tvalidasi_kas_08_gudang_detail',$data_detail);
		}
		//INSERT PEMBAYARAN
		$q="SELECT 
			H.id as idbayar_id,H.jenis_kas_id,J.nama as jenis_kas_nama
			,H.sumber_kas_id,S.nama as sumber_nama
			,S.bank_id as bankid,mbank.nama as bank
			,H.idmetode,RM.metode_bayar as metode_nama,H.nominal_bayar, S.idakun,'K' as posisi_akun
			FROM `tgudang_penerimaan_pembayaran` H
			LEFT JOIN mjenis_kas J ON J.id=H.jenis_kas_id
			LEFT JOIN msumber_kas S ON S.id=H.sumber_kas_id
			LEFT JOIN ref_metode RM ON RM.id=H.idmetode
			LEFT JOIN mbank ON mbank.id=S.bank_id
			WHERE H.penerimaan_id='$id' AND H.`status`='1'";
		$rows=$this->db->query($q)->result();
		foreach($rows as $r){
			$data_bayar=array(
				'idvalidasi' =>$idvalidasi,
				'idbayar_id' =>$r->idbayar_id,
				'jenis_kas_id' =>$r->jenis_kas_id,
				'jenis_kas_nama' =>$r->jenis_kas_nama,
				'sumber_kas_id' =>$r->sumber_kas_id,
				'sumber_nama' =>$r->sumber_nama,
				'bank' =>$r->bank,
				'bankid' =>$r->bankid,
				'idmetode' =>$r->idmetode,
				'metode_nama' =>$r->metode_nama,
				'nominal_bayar' =>$r->nominal_bayar,
				'idakun' =>$r->idakun,
				'posisi_akun' =>$r->posisi_akun,

			);
			$this->db->insert('tvalidasi_kas_08_gudang_bayar',$data_bayar);
		}
		if ($st_auto_posting=='1'){
			$data_header=array(
				
				'st_posting' => 1,
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
				

			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_kas',$data_header);
		}
		return true;
   }
   public function insert_jurnal_retur_hutang($id){//RS Kurang Bayar menjadi Hutang
		$q_udpate="UPDATE tvalidasi_retur SET `status`='0' WHERE asal_transaksi='2' AND idtransaksi='$id'";
		$this->db->query($q_udpate);
	   $idpengembalian='';
	   $q="SELECT '2' as asal_transaksi,H.id as idtransaksi,
			'' as idtransaksi_detail,H.retur_id as idretur,CURRENT_DATE() as tanggal_transaksi
			,H.nopenerimaan as notransaksi,R.nopengembalian as nopengembalian,H.jenis_retur,'2' as status_penerimaan
			,H.nominal_asal as nominal_retur,H.totalharga as nominal_ganti,0 as nominal_bayar,H.totalharga-H.nominal_asal as nominal_hutang
			,compare_value_2(MAX(IF(SH.iddistributor=H.iddistributor,SH.idakun,NULL)), MAX(IF(SH.iddistributor=0,SH.idakun,NULL))) as idakun_hutang
			,H.iddistributor as iddistributor,MD.nama as distributor,S.st_auto_posting
			FROM tgudang_penerimaan H
			LEFT JOIN tgudang_pengembalian R ON R.id=H.retur_id
			LEFT JOIN msetting_jurnal_retur_hutang SH ON SH.setting_id='1' AND SH.tipe_distributor='1'
			LEFT JOIN mdistributor MD ON MD.id=H.iddistributor 
			LEFT JOIN msetting_jurnal_retur S ON S.id='1'
			WHERE H.id='$id'";
		$row=$this->db->query($q)->row();
		$st_auto_posting=$row->st_auto_posting;
		$data_header=array(
			'asal_transaksi' => $row->asal_transaksi,
			'idtransaksi' => $row->idtransaksi,
			'idtransaksi_detail' => $row->idtransaksi_detail,
			'idretur' => $row->idretur,
			'tanggal_transaksi' => $row->tanggal_transaksi,
			'notransaksi' => $row->notransaksi,
			'nopengembalian' => $row->nopengembalian,
			'jenis_retur' => $row->jenis_retur,
			'status_penerimaan' => $row->status_penerimaan,
			'nominal_retur' => $row->nominal_retur,
			'nominal_ganti' => $row->nominal_ganti,
			'nominal_bayar' => $row->nominal_bayar,
			'nominal_hutang' => $row->nominal_hutang,
			'idakun_hutang' => $row->idakun_hutang,
			'iddistributor' => $row->iddistributor,
			'distributor' => $row->distributor,
			'status' => '1',
			'st_auto_posting' => $row->st_auto_posting,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
            'created_date'=>date('Y-m-d H:i:s')
		);
		$st_auto_posting=$row->st_auto_posting;
		$idpengembalian=$row->idretur;
		$this->db->insert('tvalidasi_retur',$data_header);
		$idvalidasi=$this->db->insert_id();
		
		$q="SELECT D.id as  iddet_trx,D.idtipe,B.idkategori,D.idbarang,D.nobatch,D.kuantitas,D.opsisatuan,D.kuantitas_terima
			,D.harga,D.tot_harga,D.nominal_ppn,D.nominal_diskon,D.total_ppn,D.total_diskon
			,D.id_penerimaan_detail,D.expired_date,B.nama as namabarang
			,compare_value(MAX(IF(SB.idbarang = D.idbarang,SB.idakun,NULL)),MAX(IF(SB.idbarang = 0 AND SB.idkategori = B.idkategori,SB.idakun,NULL)),MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe = D.idtipe ,SB.idakun,NULL))) idakun_beli
			,compare_value(MAX(IF(SP.idbarang = D.idbarang,SP.idakun,NULL)),
						MAX(IF(SP.idbarang = 0 AND SP.idkategori = B.idkategori,SP.idakun,NULL)),
						MAX(IF(SP.idbarang = 0 AND SP.idkategori = 0 AND SP.idtipe = D.idtipe ,SP.idakun,NULL))) idakun_ppn
						,compare_value(MAX(IF(SD.idbarang = D.idbarang,SD.idakun,NULL)),
				MAX(IF(SD.idbarang = 0 AND SD.idkategori = B.idkategori,SD.idakun,NULL)),
			  MAX(IF(SD.idbarang = 0 AND SD.idkategori = 0 AND SD.idtipe = D.idtipe ,SD.idakun,NULL))) idakun_diskon,'K' as posisi_beli,'K' as posisi_ppn,'D' as posisi_diskon
			
			FROM tgudang_pengembalian_detail D
			LEFT JOIN view_barang_all B ON B.idtipe=D.idtipe AND B.id=D.idbarang
			LEFT JOIN msetting_jurnal_pembelian_beli SB ON SB.idtipe=D.idtipe
			LEFT JOIN msetting_jurnal_pembelian_ppn SP ON SP.idtipe=D.idtipe
			LEFT JOIN msetting_jurnal_pembelian_diskon SD ON SD.idtipe=D.idtipe
			WHERE D.idpengembalian='$idpengembalian'";
		$rows=$this->db->query($q)->result();
		$total_transaksi=0;
		foreach($rows as $r){
			$data_detail=array(
				'idvalidasi' => $idvalidasi ,
				'idretur' => $idpengembalian ,
				'iddet_trx' => $r->iddet_trx,
				'idtipe' => $r->idtipe,
				'idkategori' => $r->idkategori,
				'idbarang' => $r->idbarang,
				'namabarang' => $r->namabarang,
				'nobatch' => $r->nobatch,
				'kuantitas' => $r->kuantitas,
				'opsisatuan' => $r->opsisatuan,
				'kuantitas_terima' => $r->kuantitas_terima,
				'harga' => $r->harga,
				'tot_harga' => $r->tot_harga,
				'nominal_diskon' => $r->nominal_diskon,
				'nominal_ppn' => $r->nominal_ppn,
				'total_diskon' => $r->total_diskon,
				'total_ppn' => $r->total_ppn,
				'nominal_harga' => $r->tot_harga + $r->total_diskon - $r->total_ppn,
				'status' => 1,
				'idakun_beli' => $r->idakun_beli,
				'idakun_ppn' => $r->idakun_ppn,
				'idakun_diskon' => $r->idakun_diskon,
				'posisi_beli' => $r->posisi_beli,
				'posisi_ppn' => $r->posisi_ppn,
				'posisi_diskon' => $r->posisi_diskon,

			);
			// $total_transaksi=$total_transaksi + $r->nominal_beli + $r->nominal_ppn - $r->nominal_diskon;
			$this->db->insert('tvalidasi_retur_barang',$data_detail);
		}
		$q="SELECT D.id as  iddet_trx,D.idtipe,B.idkategori,D.idbarang,D.nobatch,D.kuantitas,D.opsisatuan,D.kuantitas as kuantitas_terima
			,D.harga,D.totalharga as tot_harga,D.nominalppn as nominal_ppn,D.nominaldiskon as nominal_diskon,D.nominalppn*D.kuantitasretur as total_ppn,D.nominaldiskon * D.kuantitas as total_diskon
			,D.id as id_penerimaan_detail,D.tanggalkadaluarsa as expired_date,B.nama as namabarang
			,compare_value(MAX(IF(SB.idbarang = D.idbarang,SB.idakun,NULL)),MAX(IF(SB.idbarang = 0 AND SB.idkategori = B.idkategori,SB.idakun,NULL)),MAX(IF(SB.idbarang = 0 AND SB.idkategori = 						0 AND SB.idtipe = D.idtipe ,SB.idakun,NULL))) idakun_beli
						,compare_value(MAX(IF(SP.idbarang = D.idbarang,SP.idakun,NULL)),
						MAX(IF(SP.idbarang = 0 AND SP.idkategori = B.idkategori,SP.idakun,NULL)),
						MAX(IF(SP.idbarang = 0 AND SP.idkategori = 0 AND SP.idtipe = D.idtipe ,SP.idakun,NULL))) idakun_ppn
						,compare_value(MAX(IF(SD.idbarang = D.idbarang,SD.idakun,NULL)),
				MAX(IF(SD.idbarang = 0 AND SD.idkategori = B.idkategori,SD.idakun,NULL)),
			  MAX(IF(SD.idbarang = 0 AND SD.idkategori = 0 AND SD.idtipe = D.idtipe ,SD.idakun,NULL))) idakun_diskon,'D' as posisi_beli,'D' as posisi_ppn,'K' as posisi_diskon
			
			FROM tgudang_penerimaan_detail D
			LEFT JOIN view_barang_all B ON B.idtipe=D.idtipe AND B.id=D.idbarang
			LEFT JOIN msetting_jurnal_pembelian_beli SB ON SB.idtipe=D.idtipe
			LEFT JOIN msetting_jurnal_pembelian_ppn SP ON SP.idtipe=D.idtipe
			LEFT JOIN msetting_jurnal_pembelian_diskon SD ON SD.idtipe=D.idtipe
			WHERE D.idpenerimaan='$id'";
		$rows=$this->db->query($q)->result();
		$total_transaksi=0;
		foreach($rows as $r){
			$data_detail=array(
				'idvalidasi' => $idvalidasi ,
				'idretur' => $id ,
				'iddet_trx' => $r->iddet_trx,
				'idtipe' => $r->idtipe,
				'idkategori' => $r->idkategori,
				'idbarang' => $r->idbarang,
				'namabarang' => $r->namabarang,
				'nobatch' => $r->nobatch,
				'kuantitas' => $r->kuantitas,
				'opsisatuan' => $r->opsisatuan,
				'kuantitas_terima' => $r->kuantitas_terima,
				'harga' => $r->harga,
				'tot_harga' => $r->tot_harga,
				'nominal_diskon' => $r->nominal_diskon,
				'nominal_ppn' => $r->nominal_ppn,
				'total_diskon' => $r->total_diskon,
				'total_ppn' => $r->total_ppn,
				'nominal_harga' => $r->tot_harga + $r->total_diskon - $r->total_ppn,
				'status' => 1,
				'idakun_beli' => $r->idakun_beli,
				'idakun_ppn' => $r->idakun_ppn,
				'idakun_diskon' => $r->idakun_diskon,
				'posisi_beli' => $r->posisi_beli,
				'posisi_ppn' => $r->posisi_ppn,
				'posisi_diskon' => $r->posisi_diskon,

			);
			// $total_transaksi=$total_transaksi + $r->nominal_beli + $r->nominal_ppn - $r->nominal_diskon;
			$this->db->insert('tvalidasi_retur_ganti',$data_detail);
		}
		
		if ($st_auto_posting=='1'){
			$data_header=array(
				
				'st_posting' => 1,
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
				

			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_retur',$data_header);
		}
		return true;
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
