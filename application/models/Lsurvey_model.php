<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lsurvey_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function list_survey(){
		$q="SELECT *FROM msurvey_kepuasan WHERE staktif='1'";
		return $this->db->query($q)->result();
	}
    public function getInfoBarang($idtipe, $idbarang)
    {
        $this->db->where('id', $idbarang);
        $this->db->where('status', '1');
        if ($idtipe == 1) {
            $query = $this->db->get('mdata_alkes');
        } elseif ($idtipe == 2) {
            $query = $this->db->get('mdata_implan');
        } elseif ($idtipe == 3) {
            $query = $this->db->get('mdata_obat');
        } elseif ($idtipe == 4) {
            $query = $this->db->get('mdata_logistik');
        }
        return $query->row();
    }
	function get_default(){
		$iduser=$user=$this->session->userdata('user_id');;
		$q="SELECT unitpelayananiddefault from musers WHERE id='$iduser'";
		return $this->db->query($q)->row('unitpelayananiddefault');
	}
	public function get_namaUnit($id){
		if ($id=='#'){
			// $user=$this->session->userdata('user_id');
			// $q="SELECT nama from munitpelayanan
				// WHERE id IN (SELECT U.idunitpelayanan from munitpelayanan_user U WHERE U.userid='$user')";
			return 'ALL By User Acces';
		}else{
			$q="SELECT nama from munitpelayanan WHERE id='$id'";
		}
		$query=$this->db->query($q);
		$row=$query->result_array();
		$arr = array_map (function($value){
				return $value['nama'];
			} , $row);
			 return (implode(', ',$arr));

	}
	public function get_namaTipe($id){
		if ($id=='#'){
			// $user=$this->session->userdata('user_id');
			// $q="SELECT nama from munitpelayanan
				// WHERE id IN (SELECT U.idunitpelayanan from munitpelayanan_user U WHERE U.userid='$user')";
			return 'ALL By User Acces';
		}else{
			$q="SELECT nama_tipe as nama FROM  mdata_tipebarang WHERE id='$id'";
		}
		$query=$this->db->query($q);
		$row=$query->result_array();
		$arr = array_map (function($value){
				return $value['nama'];
			} , $row);
			 return (implode(', ',$arr));

	}
	public function get_namaKategori($id){
		if ($id=='#'){
			// $user=$this->session->userdata('user_id');
			// $q="SELECT nama from munitpelayanan
				// WHERE id IN (SELECT U.idunitpelayanan from munitpelayanan_user U WHERE U.userid='$user')";
			return 'ALL By User Acces';
		}else{
			$q="SELECT nama FROM  mdata_kategori WHERE id='$id'";
		}
		$query=$this->db->query($q);
		$row=$query->result_array();
		$arr = array_map (function($value){
				return $value['nama'];
			} , $row);
			 return (implode(', ',$arr));

	}
	public function get_namaStok($id){
		if ($id=='#'){
			return 'ALL By User Acces';
		}elseif ($id=='1'){
			return 'Available';
		}elseif ($id=='2'){
			return 'Back To Order';
		}elseif ($id=='3'){
			return 'Stok Minimum';
		}
	}
    public function getUnitPelayanan()
    {
        $this->db->where('status', '1');
        $query = $this->db->get('munitpelayanan');
        return $query->result();
    }
	public function list_jenis()
    {
        // $this->db->where('status', '1');
        $query = $this->db->get('lkartustok_ref');
        return $query->result();
    }
	public function get_info($idtipe,$idbarang,$idunit)
    {
        $q="SELECT U.nama as namaunit, S.idunitpelayanan,S.idtipe,S.idbarang,B.nama as namabarang,B.kode,B.namatipe,B.idkategori, K.nama as namakategori,
			B.idsatuan,Sat.nama as namasatuan,S.stok,B.stokreorder,B.stokminimum,B.hargabeli
			FROM msurvey S
			LEFT JOIN munitpelayanan U ON U.id=S.idunitpelayanan
			INNER JOIN view_barang B ON B.idtipe=S.idtipe AND B.id=S.idbarang
			LEFT JOIN mdata_kategori K ON K.id=B.idkategori
			LEFT JOIN msatuan Sat ON Sat.id=B.idsatuan
			WHERE S.idunitpelayanan='$idunit' AND S.idtipe='$idtipe' AND S.idbarang='$idbarang'";

		$query=$this->db->query($q);
        // $query = $this->db->get('munitpelayanan');
        return $query->row_array();
    }

    public function getKartuStok($idtipe, $idbarang)
    {
        $this->db->select('lkartustok.*, lkartustok_ref.nama_trx AS jenis_trx, munitpelayanan.nama AS namaunitpelayanan');
        $this->db->join('munitpelayanan', 'munitpelayanan.id = lkartustok.idunitpelayanan');
        $this->db->join('lkartustok_ref', 'lkartustok_ref.id = lkartustok.tipetransaksi');
        $this->db->where('lkartustok.idtipe', $idtipe);
        $this->db->where('lkartustok.idbarang', $idbarang);
        $this->db->where('lkartustok.status', '1');
        $query = $this->db->get('lkartustok');
        return $query->result();
    }
}
