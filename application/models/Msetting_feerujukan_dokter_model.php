<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Msetting_feerujukan_dokter_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {
        $query = $this->db->get('msetting_feerujukan_dokter');
        return $query->row();
    }

    public function updateData()
    {
        $this->tipe_pasien = json_encode($_POST['tipe_pasien']);
        $this->asal_pasien = json_encode($_POST['asal_pasien']);
        $this->poliklinik = json_encode($_POST['poliklinik']);
        $this->tipe_dokter = json_encode($_POST['tipe_dokter']);
        $this->nama_dokter = json_encode($_POST['nama_dokter']);
        $this->hitung_operasi = json_encode($_POST['hitung_operasi']);
        $this->hitung_visite = json_encode($_POST['hitung_visite']);

        if ($this->db->update('msetting_feerujukan_dokter', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
