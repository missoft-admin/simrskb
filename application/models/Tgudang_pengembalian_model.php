<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO + ACENG DJUHADI
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tgudang_pengembalian_model extends CI_Model
{
    public function save()
    {
        $status         = 1;
        $detail_value   = json_decode($this->input->post('detail_value'));
        
        $get_unique = array_map('unserialize', array_unique(array_map(function ($arr) {
            return serialize(array(
                0 => $arr[12],//IDDISTRIBUTOR
                1 => $arr[16],//Jenis pengebalian
                2 => $arr[17],//Jenis Retur
                3 => $arr[25]));//IDPenerimaan
        }, $detail_value)));

		// print_r($detail_value);exit();
        foreach ($get_unique as $row_reff) {
            
            // var default
            $totalbarang           = 0;
            $totalharga            = 0;
            $totalpengganti            = 0;
            $alasan                = '';

            // this input
            $this->nopengembalian   = '-';
            $this->idpenerimaan    	= $row_reff[3];
            $this->iddistributor    = $row_reff[0];
            $this->jenis            = $row_reff[1];
            $this->jenis_retur      = $row_reff[2];
            $this->status           = 1;
            $this->id_user_created  = $this->session->userdata("user_id");
            $this->nama_user_created= $this->session->userdata("user_name");
            $this->tgl_created= date('Y-m-d H:i:s');
            // $this->nopenerimaan     = $row_reff[4];



            if ($this->db->insert('tgudang_pengembalian', $this)) {
                $idpengembalian = $this->db->insert_id();
                $idpemesanan        = 0;
                $iddet=0;
                if ($row_reff[2] == 1) {
                    if (in_array($row_reff[3], array(2,3))) { //Ganti Barang otomatis masuk pesanan
                        // $data_pesanan_head = array('tanggal'        => date('Y-m-d'),
                                                   // 'iddistributor'  => $row_reff[1],
                                                   // 'status'         => 1,
                                                   // 'statusretur'    => 1,
                                                   // 'retur_id'       => $idpengembalian,
                        // );
                        // if ($this->db->insert('tgudang_pemesanan', $data_pesanan_head)) {
                            // $idpemesanan = $this->db->insert_id();
                        // }
                    }
                } else {
                    //Pemusnahan
                }
                
               
                
                foreach ($detail_value as $r) {
                    if ($row_reff[0] == $r[12] && $row_reff[1] == $r[16] && $row_reff[2] == $r[17] && $row_reff[3] == $r[25]) {
                        $detail 					= array();
                        
                        $detail['idpengembalian'] 	= $idpengembalian;
                        $detail['idtipe'] 			= $r[10];
                        $detail['idbarang'] 		= $r[14];
                        $detail['nobatch'] 		    = $r[3];
                        $detail['kuantitas'] 		= $r[24];
                        $detail['opsisatuan'] 		= $r[23];
                        $detail['expired_date'] 	= YMDFormat($r[22]);
                        $detail['harga'] 			= RemoveComma($r[4]);
                        $detail['tot_harga'] 		= RemoveComma($r[9]);
                        $detail['status'] 			= 1;
                        $detail['id_penerimaan_detail'] = $r[21];
                        // $detail['expired_date'] = $r[22];
						// print_r($detail);exit();
                        if ($this->db->insert('tgudang_pengembalian_detail', $detail)) {
							$iddet = $this->db->insert_id();
                            $totalbarang     = $totalbarang + RemoveComma($r[24]);
                            $totalharga      = $totalharga + RemoveComma($r[9]);
                            $alasan          .= $r[7].", ";
                        }
                        
                        // if ($idpemesanan) {
                        if ($r[16] == '1') { //Insert Retur
                            if ($r[17] == '3') { //Insert Ganti barang beda
                                foreach ($r[19] as $rbeda) {
                                    $detailpesanan 			        = array();
                                    $detailpesanan['idpengembalian'] 	= $idpengembalian;
                                    $detailpesanan['idpengembalian_det'] 	= $iddet;
                                    $detailpesanan['idtipe']        = $rbeda[6];
                                    $detailpesanan['idbarang']      = $rbeda[7];
                                    $detailpesanan['opsisatuan']    = $rbeda[8];
                                    $detailpesanan['kuantitas']     = $rbeda[3];
                                    $detailpesanan['harga']         = $rbeda[2];
                                    // $detailpesanan['expired_date']  = $rbeda[5];
                                    $detailpesanan['tot_harga']     = $rbeda[5];
                                    $detailpesanan['id_penerimaan_detail']     = $r[21];
                                    $detailpesanan['status']        = 1;
                                    $this->db->insert('tgudang_pengembalian_ganti', $detailpesanan);
									$totalpengganti=$totalpengganti + $rbeda[5];
                                }
                            } else { //Insert Ganti barang sama
                                $detailpesanan 			        = array();
                                $detailpesanan['idpengembalian'] 	= $idpengembalian;
                                $detailpesanan['idpengembalian_det'] 	= $iddet;
                                $detailpesanan['idtipe']        = $r[10];
                                $detailpesanan['idbarang']      = $r[14];
                                $detailpesanan['opsisatuan']    = $r[23];
                                $detailpesanan['kuantitas']     = $r[24];
                                $detailpesanan['harga']         = RemoveComma($r[4]);
								$detailpesanan['id_penerimaan_detail']     = $r[21];
								$detailpesanan['tot_harga'] 		= RemoveComma($r[9]);
                                $detailpesanan['status']        = 1;
                                $this->db->insert('tgudang_pengembalian_ganti', $detailpesanan);
								$totalpengganti=$totalpengganti + RemoveComma($r[9]);
                            }
                        }
                    }
                }
            }
            
            $alasan = substr($alasan, 0, -2);
            // update total transaksi
            $this->db->set('totalpengganti', $totalpengganti);
            $this->db->set('totalbarang', $totalbarang);
            $this->db->set('totalharga', $totalharga);
            $this->db->set('alasan', $alasan);
            $this->db->where('id', $idpengembalian);
            if ($this->db->update('tgudang_pengembalian')) {
                
            }
        }
        return true;
    }
	public function save_edit()
    {
        $jenis         = $this->input->post('xjenis');;
        $jenis_retur         = $this->input->post('xjenis_retur');;
        $alasan         = $this->input->post('alasan');;
        $id         = $this->input->post('id');;
        $status         = 1;
        $iddet         = '';
        $detail_value   = json_decode($this->input->post('detail_value'));
        
		$this->db->set('id_user_edit', $this->session->userdata("user_id"));
		$this->db->set('nama_user_edit', $this->session->userdata("user_name"));
		$this->db->set('tgl_edit', date('Y-m-d H:s:i'));
		$this->db->set('alasan', $alasan);
		$this->db->where('id', $id);
		$this->db->update('tgudang_pengembalian');
        
        foreach ($detail_value as $row_reff) {
           
			
			if ($row_reff[24]=='1'){//Jika Ada Proses Edit
				$detail=array();
				$detail['idpengembalian'] 	= $id;
				$detail['idtipe'] 			= $row_reff[10];
				$detail['idbarang'] 		= $row_reff[11];
				$detail['nobatch'] 		    = $row_reff[7];
				$detail['kuantitas'] 		= $row_reff[13];
				$detail['opsisatuan'] 		= $row_reff[12];
				$detail['expired_date'] 	= YMDFormat($row_reff[6]);
				$detail['harga'] 			= RemoveComma($row_reff[21]);
				$detail['tot_harga'] 		= RemoveComma($row_reff[22]);
				$detail['status'] 			= 1;
				$detail['id_penerimaan_detail'] = $row_reff[20];
				// print_r($row_reff[23]);exit();
				$this->db->where('id', $row_reff[25]);
				$this->db->update('tgudang_pengembalian_detail',$detail);				
				$iddet=$row_reff[25];
				if ($jenis== '1') { //Insert Retur
					$this->delete_pengganti($iddet);
					if ($jenis_retur == '3') { //Insert Ganti barang beda
						foreach ($row_reff[23] as $rbeda) {
							$detailpesanan 			        = array();
							$detailpesanan['idpengembalian'] 	= $id;
							$detailpesanan['idpengembalian_det'] 	= $iddet;
							$detailpesanan['idtipe']        = $rbeda[6];
							$detailpesanan['idbarang']      = $rbeda[7];
							$detailpesanan['opsisatuan']    = $rbeda[8];
							$detailpesanan['kuantitas']     = $rbeda[3];
							$detailpesanan['harga']         = $rbeda[2];
							// $detailpesanan['expired_date']  = $rbeda[5];
							$detailpesanan['tot_harga']     = $rbeda[5];
							$detailpesanan['id_penerimaan_detail']     = $row_reff[20];
							$detailpesanan['status']        = 1;
							$this->db->insert('tgudang_pengembalian_ganti', $detailpesanan);
						}
					} else { //Insert Ganti barang sama
						$detailpesanan 			        = array();
						$detailpesanan['idpengembalian'] 	= $idpengembalian;
						$detailpesanan['idpengembalian_det'] 	= $iddet;
						$detailpesanan['idtipe']        = $row_reff[10];
						$detailpesanan['idbarang']      = $row_reff[11];
						$detailpesanan['opsisatuan']    = $row_reff[12];
						$detailpesanan['kuantitas']     = $row_reff[13];
						$detailpesanan['harga']         = RemoveComma($row_reff[21]);
						$detailpesanan['id_penerimaan_detail']     = $row_reff[20];
						$detailpesanan['tot_harga'] 		= RemoveComma($row_reff[22]);
						$detailpesanan['status']        = 1;
						$this->db->insert('tgudang_pengembalian_ganti', $detailpesanan);
					}
				}
                   
			}
        }
        return true;
    }
	public function delete_pengganti($idpengembalian_det){
		// print_r($idpengembalian_det);exit();
		$this->db->where('idpengembalian_det',$idpengembalian_det);
		$this->db->delete('tgudang_pengembalian_ganti');
	}
    public function get_nobatch($idtipe, $idbarang)
    {
        $this->db->select('nobatch');
        $this->db->where('idtipe', $idtipe);
        $this->db->where('idbarang', $idbarang);
        $this->db->group_by('nobatch');
        return $this->db->get('tgudang_penerimaan_detail')->result();
    }

    public function get_idpemesanan($nobatch)
    {
        $query = $this->db->query(
            'SELECT
			(SELECT id FROM tgudang_pemesanan WHERE id = idpemesanan) id,
			(SELECT nopemesanan FROM tgudang_pemesanan WHERE id = idpemesanan) nopemesanan,
			(SELECT nama FROM mdistributor WHERE id = (SELECT iddistributor FROM tgudang_pemesanan WHERE id = idpemesanan) ) nama
			FROM tgudang_penerimaan
			WHERE id IN (
			SELECT
			idpenerimaan
			FROM tgudang_penerimaan_detail a
			WHERE nobatch = '.$nobatch.'
			GROUP BY idpenerimaan )
			GROUP BY idpemesanan'
        );
        return $query->row();
    }

    public function get_kuantitas_barang($nobatch, $idtipe, $idbarang)
    {
        $this->db->select_sum('kuantitas');
        $this->db->where('nobatch', $nobatch);
        $this->db->where('idtipe', $idtipe);
        $this->db->where('idbarang', $idbarang);
        return $this->db->get('tgudang_penerimaan_detail')->row();
    }

    public function getIdDistributor($idpemesanan)
    {
        $this->db->where('id', $idpemesanan);
        return $this->db->get('tgudang_pemesanan')->row()->iddistributor;
    }

    public function viewHead($id)
    {
        $this->db->select('a.id,DATE_FORMAT(a.tanggal,"%d-%m-%Y")tanggal, a.nopengembalian, a.totalbarang, a.totalharga,a.jenis,a.alasan,a.jenis_retur, c.nama as distributor,terima.nofakturexternal,pesanan.nopemesanan');
        $this->db->from('tgudang_pengembalian a');
        $this->db->join('mdistributor c', 'c.id = a.iddistributor', 'left');
        $this->db->join('tgudang_penerimaan terima', 'terima.id = a.idpenerimaan', 'left');
        $this->db->join('tgudang_pemesanan pesanan', 'pesanan.id = terima.idpemesanan', 'left');
        $this->db->where('a.id', $id);
        return $this->db->get()->row_array();
    }
	public function head($id)
    {
        $q="SELECT  
			H.id,H.idpenerimaan,H.tanggal,H.nopengembalian,H.iddistributor,M.nama as nama_distributor,H.jenis,H.jenis_retur,
			CASE WHEN H.jenis='1' THEN 'RETUR' ELSE 'PEMUSNAHAN' END as jenis_nama,
			CASE WHEN H.jenis_retur='1' THEN 'GANTI UANG' WHEN H.jenis_retur='2' THEN 'GANTI BARANG SAMA' WHEN H.jenis_retur='3' THEN 'GANTI BARANG BERBEDA' ELSE 'PEMUSNAHAN' END as retur_nama,H.`status`,H.alasan,
			H.nama_user_created,H.totalharga,H.totalpengganti,H.nama_user_edit,H.nama_user_hapus,tgl_created,tgl_edit,tgl_hapus,id_user_verifikasi,nama_user_verifikasi,tgl_verifikasi
			from tgudang_pengembalian H
			LEFT JOIN mdistributor M ON M.id=H.iddistributor
			WHERE H.id='$id'";
		$query=$this->db->query($q);
        return $query->row_array();
    }
	public function detail($id)
    {
        $q="SELECT D.*,B.nama as nama_barang,CASE WHEN D.opsisatuan='1' THEN 'KECIL' ELSE 'BESAR' END as jenissatuan,
				CASE WHEN D.opsisatuan='1' THEN sk.nama ELSE sb.nama END as namasatuan,sk.nama as satuan_kecil,sb.nama as satuan_besar,
				B.namatipe,B.hargabeli,B.hargabeli_besar 
				from tgudang_pengembalian_detail D
				LEFT JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe
				LEFT JOIN msatuan sb ON sb.id=B.idsatuanbesar
				LEFT JOIN msatuan sk ON sk.id=B.idsatuan
				WHERE D.idpengembalian='$id'";
		$query=$this->db->query($q);
        return $query->result();
    }

    public function viewDetail($idpengembalian)
    {
        $this->db->select('tgudang_pengembalian_detail.*,view_barang.nama');
        $this->db->where('idpengembalian', $idpengembalian);
        $this->db->from('tgudang_pengembalian_detail');
        $this->db->join('view_barang', 'tgudang_pengembalian_detail.idbarang = view_barang.id AND tgudang_pengembalian_detail.idtipe = view_barang.idtipe');
        return $this->db->get()->result();
    }

    public function getNamaBarang($idtipe, $idbarang)
    {
        $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        $this->db->from($table[$idtipe]);
        $this->db->where('id', $idbarang);
        return $this->db->get()->row()->nama;
    }
    
    public function getNoBatch($nobatch)
    {
		$user_id=$this->session->userdata('user_id');
		// $user_id=3;
		$q="SELECT GROUP_CONCAT(T.idtipe) as idtipe FROM (SELECT CASE WHEN U.gudangtipe='1' THEN '1,2,3' ELSE '4' END as idtipe FROM `musers_tipegudang` U
			WHERE U.iduser='$user_id'
			ORDER BY U.gudangtipe) T
			";
		$arr=$this->db->query($q)->row('idtipe');
		
		$q="SELECT D.nobatch,COALESCE(O.nama,A.nama,I.nama,L.nama) as nama,D.id FROM `tgudang_penerimaan_detail` D
			LEFT JOIN mdata_obat O ON O.id=D.idbarang AND D.idtipe='3'
			LEFT JOIN mdata_alkes A ON A.id=D.idbarang AND D.idtipe='1'
			LEFT JOIN mdata_implan I ON I.id=D.idbarang AND D.idtipe='2'
			LEFT JOIN mdata_logistik L ON L.id=D.idbarang AND D.idtipe='4'

			WHERE (D.nobatch LIKE '%".$nobatch."%' OR O.nama LIKE '%".$nobatch."%' OR A.nama LIKE '%".$nobatch."%' OR I.nama LIKE '%".$nobatch."%' OR L.nama LIKE '%".$nobatch."%')
			AND D.idtipe IN (".$arr.")";
		// print_r($q);exit();
        // $arr_user_gudang = array();
        // $this->db->select('gudangtipe');
        // $this->db->from('musers_tipegudang');
        // $this->db->where('iduser', $this->session->userdata('user_id'));
        // $get_user_gudang = $this->db->get();
        // $gudangtipe = $get_user_gudang->row()->gudangtipe;
        // // if($get_user_gudang->num_rows() == 1) {
            // // if($gudangtipe == 1)  $this->db->where_in('detail.idtipe', array(1,2,3) );
            // // else $this->db->where_in('detail.idtipe', array(4) );
        // // }
		// if($get_user_gudang->num_rows() == 1) {
            // if($gudangtipe == 1)  $this->db->where_in('detail.idtipe', array(1,2,3) );
            // else $this->db->where_in('detail.idtipe', array(4) );
        // }
        // $this->db->select('nobatch,view_barang.nama,detail.id');
        // $this->db->from('tgudang_penerimaan_detail detail');
        // $this->db->join('view_barang', 'view_barang.idtipe = detail.idtipe AND view_barang.id = detail.idbarang');
        // if($nobatch) {
            // $this->db->like('detail.nobatch', $nobatch, 'after');
            // $this->db->or_like('view_barang.nama', $nobatch, 'after');
        // }
        // $this->db->group_by('detail.idtipe,detail.idbarang,detail.nobatch');
        $query = $this->db->query($q);
        return $query->result_array();

    }
    
    public function getProductList($seachText)
    {
        $this->db->from('view_barang');
        $this->db->like('nama', $seachText);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function getPenerimaan()
    {
		$idbarang=$this->input->post('idbarang');
		$idtipe=$this->input->post('idtipe');
		$nobatch=$this->input->post('nobatch');
		
		// $idbarang='1';
		// $idtipe='2';
		// $nobatch='FF1';
        // $this->db->select("head.id,detail.id as iddet, head.nopenerimaan,DATE_FORMAT(head.tanggalpenerimaan,'%d-%m-%Y')tanggalpenerimaan,detail.kuantitas,mdistributor.nama nama_distributor", false);
        // $this->db->from('tgudang_penerimaan head');
        // $this->db->join('tgudang_pemesanan', 'head.idpemesanan = tgudang_pemesanan.id');
        // $this->db->join('mdistributor', 'tgudang_pemesanan.iddistributor = mdistributor.id');
        // $this->db->join('tgudang_penerimaan_detail detail', 'head.id = detail.idpenerimaan');
        // $this->db->where('detail.nobatch', $nobatch);
        // $this->db->order_by('head.nopenerimaan');
        // $this->db->group_by('head.id');
		$q="SELECT D.id,H.nopenerimaan,DATE_FORMAT(H.tanggalpenerimaan,'%d-%m-%Y')tanggalpenerimaan,D.opsisatuan,D.kuantitas,M.nama as nama_distributor FROM tgudang_penerimaan H
			LEFT JOIN tgudang_penerimaan_detail D ON H.id=D.idpenerimaan
			LEFT JOIN mdistributor M ON M.id=H.iddistributor
			WHERE D.idbarang='$idbarang' AND D.idtipe='$idtipe' AND D.nobatch='$nobatch'";
		$query=$this->db->query($q);
        // $query = $this->db->get();
        return $query->result_array();
    }
    
    public function getPenerimaanDetail($id)
    {
        // $this->db->select("detail.tanggalkadaluarsa as 'expired_date',detail.id as 'detail_id',
		// head.id ,head.nopenerimaan,DATE_FORMAT(head.tanggalpenerimaan,'%d-%m-%y')tanggalpenerimaan,detail.idbarang,
		// detail.kuantitas,detail.harga,view_barang.nama,view_barang.idtipe,tgudang_pemesanan.iddistributor,
		// mdistributor.nama nama_distributor", false);
        // $this->db->from('tgudang_penerimaan head');
        // $this->db->join('tgudang_pemesanan', 'head.idpemesanan = tgudang_pemesanan.id');
        // $this->db->join('mdistributor', 'tgudang_pemesanan.iddistributor = mdistributor.id');
        // $this->db->join('tgudang_penerimaan_detail detail', 'head.id = detail.idpenerimaan');
        // $this->db->join('view_barang', 'view_barang.idtipe = detail.idtipe AND view_barang.id = detail.idbarang');
        // $this->db->where('head.id', $id);
        // $this->db->where('detail.nobatch', $nobatch);
        // $query = $this->db->get();
		$q="SELECT H.id,H.nopenerimaan,DATE_FORMAT(H.tanggalpenerimaan,'%d-%m-%Y') AS tanggalpenerimaan,D.opsisatuan,D.kuantitas,M.nama as nama_distributor,
			H.iddistributor,D.idtipe,D.idbarang,B.nama,D.harga,D.id as detail_id,DATE_FORMAT(D.tanggalkadaluarsa, '%d-%m-%Y') as expired_date,
			CASE WHEN D.opsisatuan='1' THEN SK.nama ELSE SB.nama END as satuan,CASE WHEN D.opsisatuan='1' THEN 'KECIL' ELSE 'BESAR' END as jenis_satuan,
			CASE WHEN D.opsisatuan='1' THEN B.hargabeli ELSE B.hargabeli_besar END as harga_master,B.jumlahsatuanbesar,D.nobatch,SB.nama as satuan_besar,SK.nama as satuan_kecil
			,B.hargabeli as harga_kecil, B.hargabeli_besar as harga_besar
			FROM tgudang_penerimaan H
			LEFT JOIN tgudang_penerimaan_detail D ON H.id=D.idpenerimaan
			LEFT JOIN mdistributor M ON M.id=H.iddistributor
			LEFT JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe
			LEFT JOIN msatuan SB ON SB.id=B.idsatuanbesar
			LEFT JOIN msatuan SK ON SK.id=B.idsatuan
			WHERE D.id='$id'";
		$query=$this->db->query($q);
		
        return $query->row_array();
    }
	public function getPenerimaan_detail($id)
    {
        $q="SELECT 
		D.idbarang,D.idtipe,D.nobatch
		from tgudang_penerimaan_detail D where D.id='$id'";
		$query=$this->db->query($q);
        return $query->row_array();
    }
	public function get_mbarang($id,$idtipe)
    {
        $q="SELECT *from view_barang where id='$id' and idtipe='$idtipe'";
		$query=$this->db->query($q);
        return $query->row_array();
    }
	public function Get_barang_penerimaan_detail($id)
    {
        $q="SELECT 
		D.id,D.idbarang,D.idtipe,D.nobatch,B.nama 
		from tgudang_penerimaan_detail D 
		LEFT JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe
		where D.idpenerimaan='$id'";
		$query=$this->db->query($q);
        return $query->result_array();
    }
	public function verifikasi($id)
    {
		// print_r($id);exit();
		$xjenis=$this->input->post('xjenis');
		$xjenis_retur=$this->input->post('xjenis_retur');
        $data=array();
		$data['id_user_verifikasi'] =$this->session->userdata("user_id");
		$data['nama_user_verifikasi'] =$this->session->userdata("user_name");
		$data['tgl_verifikasi'] =date('Y-m-d H:i:s');
		$data['status'] ='2';
		$this->db->where('id',$id);
		if ($this->db->update('tgudang_pengembalian',$data)){
			if ($xjenis=='1' && $xjenis_retur !='1'){
				$tipe='';
				$idheader='';
				$q="SELECT *FROM (
					SELECT T.jenis_tipe,CASE WHEN T.jenis_tipe ='NON_LOGISTIK' THEN '1' ELSE '2' END tipepemesanan,H.id,H.idpengembalian, H.idbarang,H.idtipe,H.kuantitas,H.opsisatuan,H.harga,H.tot_harga,
					B.jumlahsatuanbesar,CASE WHEN H.opsisatuan='1' THEN H.kuantitas ELSE H.kuantitas * B.jumlahsatuanbesar END as kuantitas_kecil
					FROM tgudang_pengembalian_ganti H 
					LEFT JOIN mdata_tipebarang T ON T.id=H.idtipe 
					LEFT JOIN view_barang B ON B.id=H.idbarang AND B.idtipe=H.idtipe
					WHERE H.idpengembalian='$id' AND H.`status`='1') T ORDER BY T.tipepemesanan";
				$rows=$this->db->query($q);
				$rows=$rows->result();
				foreach ($rows as $row){
					if ($tipe!=$row->tipepemesanan){
						$idheader=$this->create_header($row->idpengembalian,$row->tipepemesanan);
						//CREATE HEADER
						$tipe=$row->tipepemesanan;
					}
					$data_detail=array();
					$data_detail['idpemesanan']=$idheader;
					$data_detail['idtipe']=$row->idtipe;
					$data_detail['idbarang']=$row->idbarang;
					$data_detail['opsisatuan']=$row->opsisatuan;
					$data_detail['kuantitas']=$row->kuantitas;
					$data_detail['kuantitas_kecil']=$row->kuantitas_kecil;
					$data_detail['harga']=$row->harga;
					$data_detail['harga_total']=$row->tot_harga;
					$data_detail['status']='1';
					$data_detail['st_konversi']='1';
					
						// print_r($data_detail);exit();
					$this->db->insert('tgudang_pemesanan_detail',$data_detail);
					
					$this->db->set('totalbarang', '`totalbarang` + '.$row->kuantitas, false);
					$this->db->set('totalharga', '`totalharga` + '.$row->tot_harga, false);
					$this->db->where('id', $idheader);
					$this->db->update('tgudang_pemesanan');
				}
            }                                        			
			return true;
		}else{
			return false;
		}
        // return $query->result_array();
    }
	function create_header($idpengembalian,$idtipe){
		$q="INSERT INTO tgudang_pemesanan (tipepemesanan,tanggal,nopemesanan,iddistributor,totalbarang,totalharga,statusretur,retur_id,stdraft,userpemesanan,tgl_pemesanan,status,st_retur,id_user_pemesanan)
				SELECT '$idtipe' AS tipepemesanan,NOW( ) AS tanggal,'-' AS nopemesanan,H.iddistributor,
					'0' AS totalbarang,'0' AS totalharga,'1' AS statusretur,H.id AS retur_id,
					'0' AS stdraft,H.nama_user_verifikasi AS userpemesanan,
					NOW( ) AS tgl_pemesanan,'3' AS STATUS,'1' AS st_retur,H.id_user_verifikasi AS id_user_pemesanan 
				FROM tgudang_pengembalian H  WHERE id = '$idpengembalian'";
		$this->db->query($q);
		return $this->db->insert_id();		
	}
}

/* End of file Tgudang_pengembalian_model.php */
/* Location: ./application/models/Tgudang_pengembalian_model.php */
