<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Munitpelayanan_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getSpecified($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('munitpelayanan');
        return $query->row();
    }

    public function saveData() {
        $this->nama       = $_POST['nama'];
        $this->lokasi     = $_POST['lokasi'];
        $this->deskripsi  = $_POST['deskripsi'];
		$this->st_otomatis_terima  = $_POST['st_otomatis_terima'];
        $this->jml_hari_terima  = RemoveComma($_POST['jml_hari_terima']);
        $this->created_by  = $this->session->userdata('user_id');
        $this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('munitpelayanan', $this)) {
            $idunitpeminta = $this->db->insert_id();
            $unitpelayanan_permintaan = $this->input->post('unitpelayanan_permintaan');
            foreach ($unitpelayanan_permintaan as $r) {
                $this->db->set('idunitpeminta', $idunitpeminta);
                $this->db->set('idunitpenerima', $r);
                $this->db->insert('munitpelayanan_permintaan');
            }

            $unitpelayanan_tipebarang = $this->input->post('tipebarang');
            foreach ($unitpelayanan_tipebarang as $r) {
                $this->db->set('idunitpelayanan', $idunitpeminta);
                $this->db->set('idtipe', $r);
                $this->db->insert('munitpelayanan_tipebarang');
            }

            $unitpelayanan_user = $this->input->post('user');
            foreach ($unitpelayanan_user as $r) {
                $this->db->set('idunitpelayanan', $idunitpeminta);
                $this->db->set('userid', $r);
                $this->db->insert('munitpelayanan_user');
            }
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function save_user_setting(){
		$idunit = $_POST['id'];
		$this->db->where('idunit', $idunit);
		$delete = $this->db->delete('munitpelayanan_user_setting');
		if ($delete) {
			$unitpelayanan_user = $this->input->post('user');
			foreach ($unitpelayanan_user as $r) {
				$this->db->set('idunit', $idunit);
				$this->db->set('userid', $r);
				$this->db->insert('munitpelayanan_user_setting');
			}
		}

		return true;
	}
    public function updateData() {
        $this->nama       = $_POST['nama'];
        $this->lokasi     = $_POST['lokasi'];
        $this->deskripsi  = $_POST['deskripsi'];
        $this->st_otomatis_terima  = $_POST['st_otomatis_terima'];
        $this->jml_hari_terima  = RemoveComma($_POST['jml_hari_terima']);

        $this->edited_by  = $this->session->userdata('user_id');
        $this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('munitpelayanan', $this, ['id' => $_POST['id']])) {
            $idunitpeminta = $_POST['id'];
            $this->db->where('idunitpeminta', $idunitpeminta);
            $delete = $this->db->delete('munitpelayanan_permintaan');
            if ($delete) {
                $unitpelayanan_permintaan = $this->input->post('unitpelayanan_permintaan');
                foreach ($unitpelayanan_permintaan as $r) {
                    $this->db->set('idunitpeminta', $idunitpeminta);
                    $this->db->set('idunitpenerima', $r);
                    $this->db->insert('munitpelayanan_permintaan');
                }
            }
            $this->db->where('idunitpelayanan', $idunitpeminta);
            $delete = $this->db->delete('munitpelayanan_tipebarang');
            if ($delete) {
                $unitpelayanan_tipebarang = $this->input->post('tipebarang');
                foreach ($unitpelayanan_tipebarang as $r) {
                    $this->db->set('idunitpelayanan', $idunitpeminta);
                    $this->db->set('idtipe', $r);
                    $this->db->insert('munitpelayanan_tipebarang');
                }
            }

            $this->db->where('idunitpelayanan', $idunitpeminta);
            $delete = $this->db->delete('munitpelayanan_user');
            if ($delete) {
                $unitpelayanan_user = $this->input->post('user');
                foreach ($unitpelayanan_user as $r) {
                    $this->db->set('idunitpelayanan', $idunitpeminta);
                    $this->db->set('userid', $r);
                    $this->db->insert('munitpelayanan_user');
                }
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id) {
        $this->status = 0;

        $this->deleted_by  = $this->session->userdata('user_id');
        $this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('munitpelayanan', $this, ['id' => $id])) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function getDefaultIdUnitPelayananUser() {
        $this->db->select('unitpelayananiddefault as idunitpelayanan');
        $this->db->from('musers');
        $this->db->where('id', $this->session->userdata('user_id'));
        $query = $this->db->get();
        return $query->row_array()['idunitpelayanan'];
    }
    public function getDefaultUnitPelayananUser() {
        $this->db->select('munitpelayanan.id, munitpelayanan.nama as nama');
        $this->db->where('munitpelayanan_user.userid', $this->session->userdata('user_id'));
        $this->db->join('munitpelayanan', 'munitpelayanan_user.idunitpelayanan = munitpelayanan.id', 'left');
        return $this->db->get('munitpelayanan_user')->result();
    }
}
