<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
KEGUNAAN		: Untuk Insert Semua
|--------------------------------------------------------------------------
|
*/

class Tvalidasi_model extends CI_Model
{  
	function GenerateValidasiPendapatanRajalInfoMedis($id){
		$q="SELECT H.tanggal as tanggal_transaksi,H.id as idtransaksi,H.notransaksi,H.nama_pemohon,H.tanggal,H.keterangan 
			,SUM(D.diskon_all) as nominal_diskon_all,D.group_diskon_all as idgroup_diskon_all,B.nominal_round,S.group_round_rajal as idgroup_round
			,'K' as posisi_round,'D' as posisi_diskon_all,H.created_by,SUM(D.nominal_akhir) as nominal
			FROM `trm_pengajuan_informasi` H
			LEFT JOIN trm_pengajuan_informasi_detail D ON D.idtransaksi=H.id
			LEFT JOIN trm_pembayaran_informasi B ON B.idpengajuan=H.id
			LEFT JOIN msetting_jurnal_pendapatan_general S ON S.id='1'
			WHERE H.id='$id'
			GROUP BY H.id";
		$dataHeader=$this->db->query($q)->row();
		$tanggal_transaksi=date('Y-m-d');
		$tipe_trx='6';
		$idvalidasi='';
		$idheader='';
		$q="SELECT H.id as idvalidasi FROM tvalidasi_pendapatan_rajal H WHERE H.tipe_trx='$tipe_trx' AND H.tanggal_transaksi='$tanggal_transaksi' AND H.st_posting='0'";
		// print_r($q);	exit();
		$idvalidasi=$this->db->query($q)->row('idvalidasi');
		if ($idvalidasi==''){
			$data_validasi=array(
				'tanggal_transaksi'=> $tanggal_transaksi,
				'tipe_trx'=> $tipe_trx,
				'status'=> 1,
			);
			$this->db->insert('tvalidasi_pendapatan_rajal',$data_validasi);
			$idvalidasi=$this->db->insert_id();
		}
		$data_detail=array(
			'idvalidasi' =>$idvalidasi,
			'idtipe' =>$tipe_trx,
			'tanggal_transaksi' =>$dataHeader->tanggal_transaksi,
			'idtransaksi' =>$dataHeader->idtransaksi,
			'notransaksi' =>$dataHeader->notransaksi,
			'nama_pemohon' =>$dataHeader->nama_pemohon,
			'tanggal' =>$dataHeader->tanggal,
			'keterangan' =>$dataHeader->keterangan,
			'nominal_diskon_all' =>$dataHeader->nominal_diskon_all,
			'idgroup_diskon_all' =>$dataHeader->idgroup_diskon_all,
			// 'idakun_diskon_all' =>$dataHeader->idakun_diskon_all,
			'nominal' =>$dataHeader->nominal,
			'nominal_round' =>$dataHeader->nominal_round,
			'idgroup_round' =>$dataHeader->idgroup_round,
			// 'idakun_round' =>$dataHeader->idakun_round,
			'posisi_diskon_all' =>$dataHeader->posisi_diskon_all,
			'posisi_round' =>$dataHeader->posisi_round,
			'userid_trx' =>$dataHeader->created_by,
			'date_trx' =>$dataHeader->tanggal,
			'userid_verif' =>$this->session->userdata('user_id'),
			'user_verif' =>$this->session->userdata('user_name'),
			'date_verif' =>date('Y-m-d H:i:s'),
		);
		
		$this->db->insert('tvalidasi_pendapatan_rajal_info_medis',$data_detail);
		$idheader=$this->db->insert_id();
		
		$q="SELECT D.id as idtransaksi,H.notransaksi as notransaksi,D.idberkas,D.tanggal_pengajuan,D.tanggal_kunjungan,D.pengajuan_ke 
		,D.jenis_pengajuan,D.estimasi_selesai,D.tarif_rs as nominal_rs,D.tarif_dokter as nominal_dokter,D.diskon_rp as diskon_rp,D.nominal_akhir
		,B.idpasien,B.no_medrec,B.namapasien,B.title,B.tujuan,D.idsetting
		,S.group_rs as idgroup_rs,S.group_dokter as idgroup_dokter,S.group_diskon as idgroup_diskon,M.nama as jenis_pengajuan_nama
		FROM trm_pengajuan_informasi_detail D
		LEFT JOIN mpengajuan_skd M ON M.id=D.jenis_pengajuan
		LEFT JOIN trm_pengajuan_informasi H ON H.id=D.idtransaksi
		LEFT JOIN trm_layanan_berkas B ON B.id=D.idberkas
		LEFT JOIN msetting_tarif_pengajuan_detail S ON S.idsetting=D.idsetting AND S.idpengajuan=D.jenis_pengajuan AND S.permintaan_ke=D.pengajuan_ke
		WHERE D.idtransaksi='$id' AND D.status='1'";
		$data=$this->db->query($q)->result();
		foreach ($data as $row){
			$data_detail=array(
				'idvalidasi' =>$idvalidasi,
				'idhead' =>$idheader,
				'tujuan' =>$row->tujuan,
				// 'tanggal_transaksi' =>$row->tanggal_transaksi,
				'idtransaksi' =>$row->idtransaksi,
				'notransaksi' =>$row->notransaksi,
				'idberkas' =>$row->idberkas,
				'tanggal_kunjungan' =>$row->tanggal_kunjungan,
				'tanggal_pengajuan' =>$row->tanggal_pengajuan,
				'jenis_pengajuan' =>$row->jenis_pengajuan,
				'jenis_pengajuan_nama' =>$row->jenis_pengajuan_nama,
				'pengajuan_ke' =>$row->pengajuan_ke,
				'estimasi_selesai' =>$row->estimasi_selesai,
				'nominal_rs' =>$row->nominal_rs,
				'nominal_dokter' =>$row->nominal_dokter,
				'diskon_rp' =>$row->diskon_rp,
				'nominal_akhir' =>$row->nominal_akhir,
				// 'pendaftaran_id' =>$row->pendaftaran_id,
				'idpasien' =>$row->idpasien,
				'no_medrec' =>$row->no_medrec,
				'title' =>$row->title,
				'namapasien' =>$row->namapasien,
				'idgroup_dokter' =>$row->idgroup_dokter,
				// 'idakun_dokter' =>$row->idakun_dokter,
				'idgroup_rs' =>$row->idgroup_rs,
				// 'idakun_rs' =>$row->idakun_rs,
				'idgroup_diskon' =>$row->idgroup_diskon,
				'posisi_dokter' =>'K',
				'posisi_rs' =>'K',
				'posisi_diskon' =>'D',

			);
			$this->db->insert('tvalidasi_pendapatan_rajal_info_medis_detail',$data_detail);
		}
	
		$q_bayar="SELECT H.id as idbayar_id,H.idmetode as idmetode
					,H.idbank as bankid,mbank.nama as bank,H.nominal as nominal_bayar
					,S.id as sumber_kas_id,S.nama as sumber_nama
					,J.id as jenis_kas_id,J.nama as jenis_kas_nama
					,'D' as posisi_akun
					,CASE WHEN H.idmetode='1' THEN ST.idakun_tunai_rajal ELSE S.idakun END as idakun
					FROM trm_pembayaran_informasi_detail H 
					LEFT JOIN mbank ON mbank.id=H.idbank
					LEFT JOIN msumber_kas S ON S.bank_id=H.idbank AND H.idmetode !='1'
					LEFT JOIN mjenis_kas J ON J.id=S.jenis_kas_id
					LEFT JOIN msetting_jurnal_pendapatan_general ST ON ST.id='1' AND H.idmetode='1'
					WHERE H.idpengajuan='$id' ";
			$row_bayar=$this->db->query($q_bayar)->result();
			foreach($row_bayar as $r){
				$nominal_bayar=$r->nominal_bayar;
				
				if ($nominal_bayar > 0){					
					$data = array(
						'idvalidasi' => $idvalidasi,
						'idhead' => $idheader,
						'idbayar_id' => $r->idbayar_id,
						'jenis_kas_id' => $r->jenis_kas_id,
						'jenis_kas_nama' => $r->jenis_kas_nama,
						'sumber_kas_id' => $r->sumber_kas_id,
						'sumber_nama' => $r->sumber_nama,
						'bank' => $r->bank,
						'bankid' => $r->bankid,
						'idmetode' => $r->idmetode,
						'metode_nama' => metodePembayaran($r->idmetode),
						'nominal_bayar' => $nominal_bayar,
						'idakun' => $r->idakun,
						'posisi_akun' => $r->posisi_akun,
					);
					$this->db->insert('tvalidasi_pendapatan_rajal_info_medis_bayar',$data);
				}
			}
		// print_r($idheader);exit();
		return true;
	}
	function GenerateValidasiPendapatanRajalRefund($idrefund,$tipe_refund){
		if ($tipe_refund=='1'){
			$q="SELECT 
			FR.asalrujukan,FR.idkategori,TP.idpoliklinik
			,MP.no_medrec,MP.title,MP.nama as namapasien,MK.nama as namakelompok,H.* 
			FROM trefund H
			LEFT JOIN tpasien_pengembalian PK ON PK.id=H.idtransaksi
			LEFT JOIN tpasien_penjualan FR ON FR.id=PK.idpenjualan
			LEFT JOIN tpoliklinik_tindakan TT ON TT.id=FR.idtindakan
			LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TT.idpendaftaran
			LEFT JOIN mfpasien MP ON MP.id=H.idpasien
			LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
			WHERE H.id='$idrefund'";
		}else{
			$q="SELECT 
			TP.idtipe as asalrujukan,TP.idpoliklinik
			,MP.no_medrec,MP.title,MP.nama as namapasien,MK.nama as namakelompok,H.* 
			FROM trefund H
			LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idtransaksi
			LEFT JOIN mfpasien MP ON MP.id=H.idpasien
			LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
			WHERE H.id='$idrefund'";
		}
		$dataRefund=$this->db->query($q)->row();
		
		$tipe_trx='1';
		$idtipe='4';
		$tanggal_transaksi=YMDFormat($dataRefund->tanggal);
		$idtransaksi=$dataRefund->idtransaksi;
		$tipe_refund=$dataRefund->tipe;
		if ($dataRefund->asalrujukan=='0'){
			$idpoliklinik=$dataRefund->idkategori;
		}else{
			$idpoliklinik=$dataRefund->idpoliklinik;			
		}
		$asalrujukan=$dataRefund->asalrujukan;
		// print_r($asalrujukan);exit();
		$idvalidasi='';
		$idheader='';
		$q="SELECT H.id as idvalidasi FROM tvalidasi_pendapatan_rajal H WHERE H.tipe_trx='$tipe_trx' AND H.tanggal_transaksi='$tanggal_transaksi' AND H.st_posting='0'";
		// print_r($q);	exit();
		$idvalidasi=$this->db->query($q)->row('idvalidasi');
		if ($idvalidasi==''){
			$data_validasi=array(
				'tanggal_transaksi'=> $tanggal_transaksi,
				'tipe_trx'=> $tipe_trx,
				'status'=> 1,
			);
			$this->db->insert('tvalidasi_pendapatan_rajal',$data_validasi);
			$idvalidasi=$this->db->insert_id();
		}
		$data_detail=array(
			'idvalidasi' =>$idvalidasi,
			'idtipe' =>$idtipe,
			'tanggal_transaksi' =>$tanggal_transaksi,
			'tanggal_pendaftaran' =>$tanggal_transaksi,
			'notransaksi' =>$dataRefund->norefund,
			'pendaftaran_id' =>$dataRefund->id,
			'tipe_refund' =>$tipe_refund,
			'kasir_id' =>null,
			'idpoliklinik' =>$dataRefund->idpoliklinik,
			'idpasien' =>$dataRefund->idpasien,
			'no_medrec' =>$dataRefund->no_medrec,
			'nokasir' =>$dataRefund->notransaksi,
			'title' =>$dataRefund->title,
			'namapasien' =>$dataRefund->namapasien,
			'namakelompok' =>$dataRefund->namakelompok,
			'idkelompok' =>$dataRefund->idkelompokpasien,
			'idrekanan' =>'',
			'namarekanan' =>'',
			'nominal' =>$dataRefund->nominal,
			'nominal_tunai' =>$dataRefund->nominal,
			'nominal_non_tunai' =>0,
			'nominal_diskon' =>0,
			'nominal_gabungan' =>0,
			'st_gabungan' =>0,
			'userid_trx' =>$dataRefund->transaksi_by,
			'date_trx' =>$dataRefund->transaksi_date,
			'userid_verif' =>$this->session->userdata('user_id'),
			'user_verif' =>$this->session->userdata('user_name'),
			'date_verif' =>date('Y-m-d H:i:s'),
		);
		// print_r($data_detail);exit();
		
		$this->db->insert('tvalidasi_pendapatan_rajal_detail',$data_detail);
		$idheader=$this->db->insert_id();
		if ($tipe_refund=='1'){//Refund Obat
			$q="SELECT H.id as iddetail,H.idtipe,B.nama as namatarif,H.idbarang as idtarif,H.harga_dasar as hargadasar 
				,H.margin,H.harga as hargajual,H.kuantitas,H.diskon,H.diskon_rp,0 as tuslah,(H.harga-H.diskon_rp) * H.kuantitas as totalkeseluruhan
				FROM tpasien_pengembalian_detail H
				LEFT JOIN view_barang_all B ON B.id=H.idbarang AND B.idtipe=H.idtipe
				WHERE H.idpengembalian='$idtransaksi'";
			$list=$this->db->query($q)->result();
			foreach ($list as $row){
				$this->Tvalidasi_model->insert_tarif_farmasi($idheader,$row,$asalrujukan,$idpoliklinik,$row->idtipe,$idvalidasi);				
			}
			$data_edit=array(
				'posisi_penjualan'=>'D',
				'posisi_diskon'=>'K',
				'posisi_tuslah'=>'D',
			);
			
			$this->db->where('idhead',$idheader);
			$this->db->update('tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan',$data_edit);
			
		}else{//Transaksi
			$q="SELECT H.reference,H.tipe,H.idtransaksi as iddetail,H.idtarif as idtarif,H.tindakan as namatarif 
				,H.jasasarana,H.jasapelayanan,H.bhp,H.biayaperawatan
				,H.jasasarana_disc,H.jasapelayanan_disc,H.bhp_disc,H.biayaperawatan_disc
				,(H.jasasarana+H.jasapelayanan+H.bhp+H.biayaperawatan) - (H.jasasarana_disc+H.jasapelayanan_disc+H.bhp_disc+H.biayaperawatan_disc) as subtotal
				,H.kuantitas,H.total,H.diskon,H.totalkeseluruhan
				FROM `trefund_detail` H
				WHERE H.idrefund='$idrefund' AND H.tipe NOT IN('TINDAKAN RAWAT JALAN - OBAT','TINDAKAN RAWAT JALAN - ALAT KESEHATAN','TINDAKAN FARMASI - OBAT','TINDAKAN FARMASI - ALAT KESEHATAN')";
			$list_refund=$this->db->query($q)->result();
			foreach($list_refund as $row){
				if ($row->tipe=='ADMINISTRASI'){
					$this->Tvalidasi_model->insert_tarif_rajal($idheader,$row,'mtarif_administrasi',1,$idvalidasi);
				}
				if ($row->tipe=='TINDAKAN RAWAT JALAN - PELAYANAN'){
					$this->Tvalidasi_model->insert_tarif_rajal($idheader,$row,'mtarif_rawatjalan',2,$idvalidasi);
				}
			
				if ($row->tipe=='LABORATORIUM'){
					$this->Tvalidasi_model->insert_tarif_rajal($idheader,$row,'mtarif_laboratorium_detail',3,$idvalidasi);
				}
				if ($row->tipe=='RADIOLOGI'){
					$this->Tvalidasi_model->insert_tarif_rajal($idheader,$row,'mtarif_radiologi_detail',4,$idvalidasi);
				}
				if ($row->tipe=='FISIOTERAPI'){
					$this->Tvalidasi_model->insert_tarif_rajal($idheader,$row,'mtarif_fisioterapi_detail',5,$idvalidasi);
				}
			}
			// //OBAT-OBATAN
			$q="SELECT H.reference,H.tipe,H.idtransaksi as iddetail,H.idtarif as idtarif,H.tindakan as namatarif 
				,0 as hargadasar,0 as margin,H.total as hargajual				
				,H.kuantitas,H.total,H.diskon,H.totalkeseluruhan,H.diskon as diskon_rp
				FROM `trefund_detail` H
				WHERE H.idrefund='$idrefund'  AND H.tipe IN('TINDAKAN RAWAT JALAN - OBAT','TINDAKAN RAWAT JALAN - ALAT KESEHATAN','TINDAKAN FARMASI - OBAT','TINDAKAN FARMASI - ALAT KESEHATAN')";
			$list_refund=$this->db->query($q)->result();
			foreach($list_refund as $row){
				
				if ($row->tipe=='TINDAKAN RAWAT JALAN - OBAT'){
					$this->Tvalidasi_model->insert_tarif_obat_alkes($idheader,$row,$asalrujukan,$idpoliklinik,3,$idvalidasi);
				}
				if ($row->tipe=='TINDAKAN RAWAT JALAN - ALAT KESEHATAN'){
					$this->Tvalidasi_model->insert_tarif_obat_alkes($idheader,$row,$asalrujukan,$idpoliklinik,1,$idvalidasi);
				}
				
				if ($row->tipe=='TINDAKAN FARMASI - OBAT' && $row->reference=='tpasien_penjualan_nonracikan'){
					$this->Tvalidasi_model->insert_tarif_farmasi($idheader,$row,$asalrujukan,$idpoliklinik,3,$idvalidasi);
				}
				if ($row->tipe=='TINDAKAN FARMASI - ALAT KESEHATAN'){
					$this->Tvalidasi_model->insert_tarif_farmasi($idheader,$row,$asalrujukan,$idpoliklinik,1,$idvalidasi);
				}
				//RACIKAN
				if ($row->tipe=='TINDAKAN FARMASI - OBAT' && $row->reference=='tpasien_penjualan_racikan'){
					// $this->Tvalidasi_model->insert_tarif_farmasi_racikan($idheader,$row,$idtipe,$idpoliklinik,3,$idvalidasi);
					$this->Tvalidasi_model->insert_tarif_farmasi_racikan($idheader,$row,$asalrujukan,$idpoliklinik,3,$idvalidasi);
				}
				
			
			}
			$this->db->update('tvalidasi_pendapatan_rajal_detail_tindakan_obat',array('posisi_penjualan'=>'D','posisi_diskon'=>'K'),array('idhead'=>$idheader));
			$this->db->update('tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan',array('posisi_penjualan'=>'D','posisi_tuslah'=>'D','posisi_diskon'=>'K'),array('idhead'=>$idheader));
			$this->db->update('tvalidasi_pendapatan_rajal_detail_farmasi_racikan',array('posisi_tuslah'=>'D'),array('idhead'=>$idheader));
			$this->db->update('tvalidasi_pendapatan_rajal_detail_farmasi_racikan_detail',array('posisi_penjualan'=>'D','posisi_diskon'=>'K'),array('idhead'=>$idheader));
			
		}
		$q="SELECT H.idakun FROM msetting_jurnal_refund_deposit H WHERE H.idmetode='1'";
		$idakun=$this->db->query($q)->row('idakun');
		$data_bayar=array(
			'idvalidasi' => $idvalidasi,
			'idhead' => $idheader,
			'idmetode' => 1,
			'metode_nama' => 'Tunai',
			'nominal_bayar' => $dataRefund->nominal,
			'idakun' => $idakun,
			'posisi_akun' => 'K',
		);
		$this->db->insert('tvalidasi_pendapatan_rajal_bayar',$data_bayar);
		// print_r($idvalidasi);exit();
		return true;
	}
	//INSERT DARI KIRIM [PIUTANG, TAGIHAN KARYAWAN, TIDAK TERTAGIH]
	function GenerateValidasiPendapatanRajal($idpendaftaran,$kasir_id,$tipe_trx='2',$tanggal_transaksi=''){
		if ($this->cek_dupilicate($idpendaftaran,$kasir_id)==false){//Jika Belum ada \
			$dataPendaftaran=$this->get_data_pendaftaran($kasir_id);
			
			$q="SELECT H.id as kasir_id,CONCAT(MAX(CASE WHEN D.idmetode IN (1,2,3,4) THEN '1' ELSE 0 END) 
			,MAX(CASE WHEN D.idmetode =7 THEN '1' ELSE 0 END)   
			,MAX(CASE WHEN D.idmetode =5 THEN '1' ELSE 0 END)   
			,MAX(CASE WHEN D.idmetode =6 THEN '1' ELSE 0 END) ) as cara_bayar
			,SUM(CASE WHEN D.idmetode IN (1,2,3,4)THEN D.nominal ELSE 0 END) as total_tunai
			,SUM(CASE WHEN D.idmetode NOT IN (1,2,3,4)THEN D.nominal ELSE 0 END) as total_non_tunai
			FROM tkasir H
			LEFT JOIN tkasir_pembayaran D ON D.idkasir=H.id AND D.`status`='1'
			WHERE H.id='$kasir_id'
			GROUP BY H.id";
			if ($tanggal_transaksi==''){
				$tanggal_transaksi=date('Y-m-d');				
			}
			$row_bayar=$this->db->query($q)->row();
			$cara_bayar=$row_bayar->cara_bayar;
			$total_tunai=$row_bayar->total_tunai;
			$total_non_tunai=$row_bayar->total_non_tunai;
			
			$idtipe = $dataPendaftaran['idtipe'];
			$idpoliklinik = $dataPendaftaran['idpoliklinik'];
			
			$idgroup_gabungan=0;
			$st_insert_detail='1';
			$idvalidasi='';
			$idheader='';
			
			if ($total_tunai>0){//Gabungan
				// print_r('GABUGNAN');exit();
			}
			$q="SELECT H.id as idvalidasi FROM tvalidasi_pendapatan_rajal H WHERE H.tipe_trx='$tipe_trx' AND H.tanggal_transaksi='$tanggal_transaksi' AND H.st_posting='0'";
			// print_r($q);	exit();
			$idvalidasi=$this->db->query($q)->row('idvalidasi');
			if ($idvalidasi==''){
				$data_validasi=array(
					'tanggal_transaksi'=> $tanggal_transaksi,
					'tipe_trx'=> $tipe_trx,
					'status'=> 1,
				);
				$this->db->insert('tvalidasi_pendapatan_rajal',$data_validasi);
				$idvalidasi=$this->db->insert_id();
			}
			if ($idvalidasi!=''){
				$idheader=$this->Tvalidasi_model->InsertJurnalPendapatanRajalHead($dataPendaftaran,$idvalidasi,$total_tunai,$total_non_tunai,$tipe_trx);
			}
			
			//INSERT DETAIL
			//OBAT FARMASI RACIKAN
			foreach ($this->Tpoliklinik_verifikasi_model->viewRincianFarmasiObatRacikan($idpendaftaran) as $row) {
				if ($st_insert_detail=='1'){//Jika Detail Di insert
					$this->Tvalidasi_model->insert_tarif_farmasi_racikan($idheader,$row,$idtipe,$idpoliklinik,3,$idvalidasi);
				}
			}
				// print_r($row);exit();
			//OBAT FARMASI
			foreach ($this->Tpoliklinik_verifikasi_model->viewRincianFarmasi($idpendaftaran,3) as $row) {
				// print_r($row);exit();
				if ($st_insert_detail=='1'){//Jika Detail Di insert
					$this->Tvalidasi_model->insert_tarif_farmasi($idheader,$row,$idtipe,$idpoliklinik,3,$idvalidasi);
				}
			}
			//ALKES FARMASI
			foreach ($this->Tpoliklinik_verifikasi_model->viewRincianFarmasi($idpendaftaran,1) as $row) {
				if ($st_insert_detail=='1'){//Jika Detail Di insert
					$this->Tvalidasi_model->insert_tarif_farmasi($idheader,$row,$idtipe,$idpoliklinik,1,$idvalidasi);
				}
			}
			//IMPLAN FARMASI
			foreach ($this->Tpoliklinik_verifikasi_model->viewRincianFarmasi($idpendaftaran,2) as $row) {
				// print_r($row);exit;
				if ($st_insert_detail=='1'){//Jika Detail Di insert
					$this->Tvalidasi_model->insert_tarif_farmasi($idheader,$row,$idtipe,$idpoliklinik,2,$idvalidasi);
				}
			}
					
			//OBAT IGD
			foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRajalObat($idpendaftaran) as $row) {
				if ($st_insert_detail=='1'){//Jika Detail Di insert
					$this->Tvalidasi_model->insert_tarif_obat_alkes($idheader,$row,$idtipe,$idpoliklinik,3,$idvalidasi);
				}
			}
			//ALKES IGD
			foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRajalAlkes($idpendaftaran) as $row) {
				if ($st_insert_detail=='1'){//Jika Detail Di insert
					$this->Tvalidasi_model->insert_tarif_obat_alkes($idheader,$row,$idtipe,$idpoliklinik,1,$idvalidasi);
				}
			}
				// print_r($dataPendaftaran);exit();
			
			// [BAGI HASIL] ADMINISTRASI
			foreach ($this->Tpoliklinik_verifikasi_model->viewRincianAdministrasi($idpendaftaran) as $row) {
				if ($st_insert_detail=='1'){//Jika Detail Di insert
					$this->Tvalidasi_model->insert_tarif_rajal($idheader,$row,'mtarif_administrasi',1,$idvalidasi);
				}
			}
			

			// JASA DOKTER RAWAT JALAN
			foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRajalTindakan($idpendaftaran) as $row) {				
				if ($st_insert_detail=='1'){//Jika Detail Di insert
					$this->Tvalidasi_model->insert_tarif_rajal($idheader,$row,'mtarif_rawatjalan',2,$idvalidasi);
				}
				
			}

			// JASA DOKTER LABORATORIUM
			foreach ($this->Tpoliklinik_verifikasi_model->viewRincianLaboratorium($idpendaftaran) as $row) {
				
				if ($st_insert_detail=='1'){//Jika Detail Di insert
					$this->Tvalidasi_model->insert_tarif_rajal($idheader,$row,'mtarif_laboratorium_detail',3,$idvalidasi);
				}
			}

			// JASA DOKTER RADIOLOGI
			foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRadiologi($idpendaftaran) as $index => $row) {
				
				if ($st_insert_detail=='1'){//Jika Detail Di insert
					$this->Tvalidasi_model->insert_tarif_rajal($idheader,$row,'mtarif_radiologi_detail',4,$idvalidasi);
				}
			}

			// JASA DOKTER FISIOTERAPI
			foreach ($this->Tpoliklinik_verifikasi_model->viewRincianFisioterapi($idpendaftaran) as $row) {				
				if ($st_insert_detail=='1'){//Jika Detail Di insert
					$this->Tvalidasi_model->insert_tarif_rajal($idheader,$row,'mtarif_fisioterapi_detail',5,$idvalidasi);
				}
			}
		}
		return true;
		
	}
	function get_data_pendaftaran($id){
		$this->db->select("tpoliklinik_pendaftaran.id AS idpendaftaran,
                  tpoliklinik_tindakan.id AS idtindakan, tkasir.id AS idkasir,
                  IF(tpoliklinik_pendaftaran.idtipepasien = 1, 'Pasien RS', 'Pasien Pribadi') AS jenispasien,
                  tpoliklinik_pendaftaran.idkelompokpasien AS idkelompok,
                  mpasien_kelompok.nama AS namakelompok,
                  tpoliklinik_pendaftaran.idrekanan,
                  mrekanan.nama AS namarekanan,DATE(tkasir.tanggal) as tanggal_kasir,tkasir.idtipe,
                  tpoliklinik_pendaftaran.tanggaldaftar AS tanggal_pemeriksaan
				  ,tpoliklinik_pendaftaran.nopendaftaran
				  ,tpoliklinik_pendaftaran.idpasien
				  ,tpoliklinik_pendaftaran.title
				  ,tpoliklinik_pendaftaran.namapasien
				  ,tpoliklinik_pendaftaran.no_medrec
				  ,tpoliklinik_pendaftaran.idpoliklinik
				  ,tpoliklinik_pendaftaran.created_date
				  ,tkasir.nokasir
				  ,tkasir.created_by
				  ");

				$this->db->join('tpoliklinik_tindakan', 'tpoliklinik_tindakan.id = tkasir.idtindakan AND idtipe IN (1,2)');
				$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran');
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
				$this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
				$this->db->where('tkasir.id', $id);
				$this->db->where('tkasir.status', 2);
				$this->db->limit(1);
				$query = $this->db->get('tkasir');
				$row = $query->row();

				$dataPendaftaran = [
					'tanggal_kasir' => $row->tanggal_kasir,
					'tanggal_pemeriksaan' => $row->tanggal_pemeriksaan,
					'idpendaftaran' => $row->idpendaftaran,
					'jenis_pasien' => $row->jenispasien,
					'idkelompok' => $row->idkelompok,
					'namakelompok' => $row->namakelompok,
					'idrekanan' => $row->idrekanan,
					'namarekanan' => $row->namarekanan,
					'kasir_id' => $row->idkasir,
					'nopendaftaran' => $row->nopendaftaran,
					'idpasien' => $row->idpasien,
					'title' => $row->title,
					'namapasien' => $row->namapasien,
					'no_medrec' => $row->no_medrec,
					'idtipe' => $row->idtipe,
					'idpoliklinik' => $row->idpoliklinik,
					'created_by' => $row->created_by,
					'nokasir' => $row->nokasir,
					'created_date' => $row->created_date,
					'status_asuransi' => ($row->idkelompok != 5 ? 1 : 0),
				];
		return $dataPendaftaran;
	}
	function cek_dupilicate($idpendaftaran,$kasir_id){
		$q="SELECT H.id,H.tipe_trx
			 FROM tvalidasi_pendapatan_rajal_detail D
			LEFT JOIN tvalidasi_pendapatan_rajal H ON H.id=D.idvalidasi
			WHERE D.pendaftaran_id='$idpendaftaran' AND D.kasir_id='$kasir_id' AND H.tipe_trx !='1'";
			// print_r($q);exit;
		if ($this->db->query($q)->row('id')){
			return true;
		}else{
			return false;
		}

	}
	
	//END DARI KIRIM [PIUTANG, TAGIHAN KARYAWAN, TIDAK TERTAGIH]
	function InsertJurnalPendapatanRajalHead($dataPendaftaran,$idvalidasi,$total_tunai,$nominal_non_tunai,$tipe_trx='1'){
		$idpendaftaran = $dataPendaftaran['idpendaftaran'];
        $jenis_pasien = $dataPendaftaran['jenis_pasien'];
        $idkelompok = $dataPendaftaran['idkelompok'];
        $namakelompok = $dataPendaftaran['namakelompok'];
        $idrekanan = $dataPendaftaran['idrekanan'];
        $namarekanan = $dataPendaftaran['namarekanan'];
        $tanggal_pemeriksaan = $dataPendaftaran['tanggal_pemeriksaan'];
        $status_asuransi = $dataPendaftaran['status_asuransi'];
        $nopendaftaran = $dataPendaftaran['nopendaftaran'];
        $kasir_id = $dataPendaftaran['kasir_id'];
        $idtipe = $dataPendaftaran['idtipe'];
        $tanggal_kasir = $dataPendaftaran['tanggal_kasir'];
        $idpasien = $dataPendaftaran['idpasien'];
        $no_medrec = $dataPendaftaran['no_medrec'];
        $title = $dataPendaftaran['title'];
        $namapasien = $dataPendaftaran['namapasien'];
        $nokasir = $dataPendaftaran['nokasir'];
        $created_by = $dataPendaftaran['created_by'];
        $created_date = $dataPendaftaran['created_date'];
        $idpoliklinik = $dataPendaftaran['idpoliklinik'];
		$st_gabungan='0';
		$nominal_gabungan='0';
		$idgroup_gabungan=null;
		$idakun_gabungan=null;
		if ($tipe_trx=='1'){
			$posisi_gabungan='K';	
			$nominal=$total_tunai;
			$where_bayar="AND H.idmetode IN(1,2,3,4)";
		}else{
			$where_bayar="AND H.idmetode NOT IN(1,2,3,4)";
			$posisi_gabungan='D';		
			$nominal=$total_tunai+$nominal_non_tunai;
		}
		if ($total_tunai > 0 && $nominal_non_tunai > 0){
			$nominal_gabungan=$total_tunai;
			$st_gabungan=1;
			$q="SELECT 
				compare_value_2(MAX(IF(H.idrekanan = '$idrekanan',H.idgroup,NULL)),MAX(IF(H.idrekanan=0 AND H.idkelompokpasien ='$idkelompok' ,H.idgroup,NULL))) as idgroup
				FROM msetting_jurnal_pendapatan_general_rajal H
				WHERE H.idkelompokpasien='$idkelompok'";				
			$idgroup_gabungan=$this->db->query($q)->row('idgroup');
			if ($tipe_trx !='1`'){
				$q="SELECT D.idgroup_gabungan
					 FROM tvalidasi_pendapatan_rajal_detail D
					LEFT JOIN tvalidasi_pendapatan_rajal H ON H.id=D.idvalidasi
					WHERE D.pendaftaran_id='$idpendaftaran' AND D.kasir_id='$kasir_id' AND H.tipe_trx ='1'";
				$tmp_group_gabungan=$this->db->query($q)->row('idgroup_gabungan');
				if ($tmp_group_gabungan){
					$idgroup_gabungan=$tmp_group_gabungan;
				}
			}
		}
		if ($tipe_trx=='1'){//Transaksi
			
			$q_diskon="SELECT H.diskon,S.group_diskon_rajal,S.group_round_rajal,H.nominal_round FROM `tkasir` H
				LEFT JOIN msetting_jurnal_pendapatan_general S ON S.id='1'
				LEFT JOIN mgroup_pembayaran G ON G.id=S.group_diskon_rajal
				WHERE H.id='$kasir_id'";
				$row_diskon=$this->db->query($q_diskon)->row();
				$nominal_diskon=$row_diskon->diskon;
				$idgroup_diskon=$row_diskon->group_diskon_rajal;
				$nominal_round=$row_diskon->nominal_round;
				$idgroup_round=$row_diskon->group_round_rajal;
		}else{
			$nominal_diskon=0;
			$nominal_round=0;
			$idgroup_diskon=null;
			$idgroup_round=null;
		}
		$q="SELECT H.group_round_rajal as idgroup_round,G.idakun as idakun_round
			 FROM msetting_jurnal_pendapatan_general H
			 LEFT JOIN mgroup_pembayaran_detail G ON G.idgroup=H.group_round_rajal
			 ";
			$data_round=$this->db->query($q)->row();	
			$idgroup_round=$data_round->idgroup_round;
			$idakun_round=$data_round->idakun_round;
			
		// print_r($idgroup_gabungan);exit();
		$data_detail=array(
			'idvalidasi' =>$idvalidasi,
			'idtipe' =>$idtipe,
			'tanggal_transaksi' =>$tanggal_kasir,
			'tanggal_pendaftaran' =>$tanggal_pemeriksaan,
			'notransaksi' =>$nopendaftaran,
			'kasir_id' =>$kasir_id,
			'pendaftaran_id' =>$idpendaftaran,
			'idpoliklinik' =>$idpoliklinik,
			'idpasien' =>$idpasien,
			'no_medrec' =>$no_medrec,
			'nokasir' =>$nokasir,
			'title' =>$title,
			'namapasien' =>$namapasien,
			'namakelompok' =>$namakelompok,
			'idkelompok' =>$idkelompok,
			'idrekanan' =>$idrekanan,
			'namarekanan' =>$namarekanan,
			'nominal' =>$nominal,
			'nominal_tunai' =>$total_tunai,
			'nominal_non_tunai' =>$nominal_non_tunai,
			'nominal_diskon' =>$nominal_diskon,
			'nominal_gabungan' =>$nominal_gabungan,
			'idgroup_diskon' =>$idgroup_diskon,
			'posisi_diskon' =>'D',
			'st_gabungan' =>$st_gabungan,
			'idgroup_gabungan' =>$idgroup_gabungan,
			'posisi_gabungan' =>$posisi_gabungan,
			'nominal_round' =>$nominal_round,
			'idgroup_round' =>$idgroup_round,
			'idakun_round' =>$idakun_round,
			'posisi_round' =>'K',
			'userid_trx' =>$created_by,
			'date_trx' =>$created_date,
			'userid_verif' =>$this->session->userdata('user_id'),
			'user_verif' =>$this->session->userdata('user_name'),
			'date_verif' =>date('Y-m-d H:i:s'),
		);
		$this->db->insert('tvalidasi_pendapatan_rajal_detail',$data_detail);
		$idhead=$this->db->insert_id();
		if ($tipe_trx=='1'){
			$kembalian=$this->db->query("SELECT IFNULL(H.kembalian,0) as kembalian FROM `tkasir` H WHERE H.id='$kasir_id'")->row('kembalian');
			$q_bayar="SELECT H.id as idbayar_id,H.idmetode as idmetode
					,H.idbank as bankid,mbank.nama as bank,H.nominal as nominal_bayar
					,S.id as sumber_kas_id,S.nama as sumber_nama
					,J.id as jenis_kas_id,J.nama as jenis_kas_nama
					,'D' as posisi_akun
					,CASE WHEN H.idmetode='1' THEN ST.idakun_tunai_rajal ELSE S.idakun END as idakun
					FROM tkasir_pembayaran H 
					LEFT JOIN mbank ON mbank.id=H.idbank
					LEFT JOIN msumber_kas S ON S.bank_id=H.idbank AND H.idmetode !='1'
					LEFT JOIN mjenis_kas J ON J.id=S.jenis_kas_id
					LEFT JOIN msetting_jurnal_pendapatan_general ST ON ST.id='1' AND H.idmetode='1'
					WHERE H.idkasir='$kasir_id' ".$where_bayar;
			$row_bayar=$this->db->query($q_bayar)->result();
			foreach($row_bayar as $r){
				$nominal_bayar=$r->nominal_bayar;
				if ($r->idmetode=='1'){					
					if ($kembalian > 0){
						if ($kembalian <= $nominal_bayar){
							$nominal_bayar = $nominal_bayar - $kembalian;
							$kembalian=0;
						}else{
							$kembalian = $kembalian - $nominal_bayar;
							$nominal_bayar=0;
						}
					}
				}
				// print_r($row_bayar);exit();
				if ($nominal_bayar > 0){					
					$data = array(
						'idvalidasi' => $idvalidasi,
						'idhead' => $idhead,
						'idbayar_id' => $r->idbayar_id,
						'jenis_kas_id' => $r->jenis_kas_id,
						'jenis_kas_nama' => $r->jenis_kas_nama,
						'sumber_kas_id' => $r->sumber_kas_id,
						'sumber_nama' => $r->sumber_nama,
						'bank' => $r->bank,
						'bankid' => $r->bankid,
						'idmetode' => $r->idmetode,
						'metode_nama' => metodePembayaran($r->idmetode),
						'nominal_bayar' => $nominal_bayar,
						'idakun' => $r->idakun,
						'posisi_akun' => $r->posisi_akun,
					);
					$this->db->insert('tvalidasi_pendapatan_rajal_bayar',$data);
				}
			}
		}else{
			$q_bayar="SELECT T.*,M.idakun FROM (SELECT H.id as idbayar_id,H.idmetode as idmetode,H.tipekontraktor,H.idkontraktor
				,H.nominal as nominal_bayar
				,'D' as posisi_akun
				,
				CASE 
				WHEN H.idmetode='5' THEN SK.group_karyawan_rajal  
				WHEN H.idmetode='6' THEN ST.group_tidak_tertagih_rajal  
				WHEN H.idmetode='7' THEN 
					compare_value_2(MAX(IF(SA.idrekanan = H.idkontraktor AND H.tipekontraktor='1',SA.idgroup,NULL)),
											MAX(IF(SA.idrekanan = 0 AND SA.idkelompokpasien = H.tipekontraktor,SA.idgroup,NULL))) 
					
				END as group_akun
				,CASE WHEN H.idmetode='7' THEN CONCAT(MK.nama,' ',IFNULL(R.nama,'')) ELSE '' END as rekanan
				FROM tkasir_pembayaran H 
				LEFT JOIN msetting_jurnal_pendapatan_general SK ON SK.id='1'
				LEFT JOIN msetting_jurnal_pendapatan_general ST ON ST.id='1'
				LEFT JOIN msetting_jurnal_pendapatan_general_rajal_piutang SA ON SA.setting_id='1' AND SA.idkelompokpasien=H.tipekontraktor
				LEFT JOIN mrekanan R ON R.id=H.idkontraktor AND H.tipekontraktor='1'
				LEFT JOIN mpasien_kelompok MK ON MK.id=H.tipekontraktor AND H.idmetode='7'
				WHERE H.idkasir='$kasir_id' AND H.idmetode NOT IN(1,2,3,4)
				GROUP BY H.id
				) T 
				LEFT JOIN mgroup_pembayaran_detail M ON M.idgroup=T.group_akun";
			$row_bayar=$this->db->query($q_bayar)->result();
			foreach($row_bayar as $r){
				// print_r($row_bayar);exit();
				$data = array(
					'idvalidasi' => $idvalidasi,
					'idhead' => $idhead,
					'idbayar_id' => $r->idbayar_id,
					'idmetode' => $r->idmetode,
					'tipekontraktor' => $r->tipekontraktor,
					'idkontraktor' => $r->idkontraktor,
					'metode_nama' => metodePembayaranKasirRajal($r->idmetode).' '.$r->rekanan,
					'nominal_bayar' => $r->nominal_bayar,
					'group_akun' => $r->group_akun,
					'idakun' => $r->idakun,
					'posisi_akun' => $r->posisi_akun,
				);
				$this->db->insert('tvalidasi_pendapatan_rajal_bayar_non_tunai',$data);
			}
		}
		return $idhead;
		
	}
	function insert_tarif_obat_bebas($id){
		
		$q="SELECT H.group_round_rajal as idgroup_round,G.idakun as idakun_round
			 FROM msetting_jurnal_pendapatan_general H
			 LEFT JOIN mgroup_pembayaran_detail G ON G.idgroup=H.group_round_rajal
			 ";
		$data_round=$this->db->query($q)->row();	
		$idgroup_round=$data_round->idgroup_round;
		$idakun_round=$data_round->idakun_round;
		$q="SELECT 
			K.idtindakan,H.id,K.idtipe,K.tanggal as tanggal_transaksi,DATE(K.tanggal) as tanggal_kasir,H.tanggal as tanggal_pendaftaran,H.nopenjualan as notransaksi
			,K.id as kasir_id,H.id as pendaftaran_id,H.idpasien,H.nomedrec as no_medrec,H.nama as namapasien,H.idkelompokpasien as idkelompok
			,MP.nama as namakelompok,K.nokasir,K.nominal_round
			,CASE WHEN B.idmetode IN(1,2,3,4) THEN SUM(B.nominal) ELSE 0 END as nominal
			,CASE WHEN B.idmetode NOT IN(1,2,3,4) THEN SUM(B.nominal) ELSE 0 END as nominal_non_tunai
			,H.idkategori,K.diskon as nominal_diskon,S.group_diskon_rajal,K.edited_by,K.edited_nama,K.tanggal
			FROM tkasir K
			LEFT JOIN tpasien_penjualan H ON H.id=K.idtindakan
			LEFT JOIN mpasien_kelompok MP ON MP.id=H.idkelompokpasien
			LEFT JOIN tkasir_pembayaran B ON B.idkasir=K.id
			LEFT JOIN msetting_jurnal_pendapatan_general S ON S.id='1'
			LEFT JOIN mgroup_pembayaran G ON G.id=S.group_diskon_rajal
			
			WHERE K.id='$id'
			GROUP BY K.id";
		$row=$this->db->query($q)->row();
		// print_r($row);exit();
		$idpoliklinik=$row->idkategori;
		$pendaftaran_id=$row->pendaftaran_id;
		$kasir_id=$row->kasir_id;
		$total_tunai=$row->nominal;
		
		$nominal_round=$row->nominal_round;
		$nominal_diskon=$row->nominal_diskon;
		$idgroup_diskon=$row->group_diskon_rajal;
		
		
		$nominal_non_tunai=$row->nominal_non_tunai;
		$tanggal_kasir=$row->tanggal_kasir;
		if ($total_tunai > 0 && $nominal_non_tunai=='0'){
			$st_insert_detail='1';
		}
		
		if ($total_tunai > 0){
			$q="SELECT H.id as idvalidasi FROM tvalidasi_pendapatan_rajal H WHERE H.tipe_trx='1' AND H.tanggal_transaksi='$tanggal_kasir' AND H.st_posting='0'";
			$idvalidasi=$this->db->query($q)->row('idvalidasi');
			if ($idvalidasi==''){
				$data_validasi=array(
					'tanggal_transaksi'=> $tanggal_kasir,
					'tipe_trx'=> 1,
					'status'=> 1,
				);
				$this->db->insert('tvalidasi_pendapatan_rajal',$data_validasi);
				$idvalidasi=$this->db->insert_id();
			}
			// print_r($idvalidasi);exit();
		}
		
		$st_gabungan='0';
		$idgroup_gabungan=null;
		$posisi_gabungan='K';
		if ($total_tunai > 0 && $nominal_non_tunai > 0){
			$st_gabungan=1;
			$q="SELECT H.idgroup FROM `msetting_jurnal_pendapatan_general_rajal` H
				WHERE H.idkelompokpasien='5' AND H.idrekanan='0'";
			$idgroup_gabungan=$this->db->query($q)->row('idgroup');
		}
		// print_r($idgroup_gabungan);exit();
		$data_detail=array(
			'idvalidasi' =>$idvalidasi,
			'idtipe' =>$row->idtipe,
			'nokasir' =>$row->nokasir,
			'tanggal_transaksi' =>$row->tanggal_kasir,
			'tanggal_pendaftaran' =>$row->tanggal_pendaftaran,
			'notransaksi' =>$row->notransaksi,
			'kasir_id' =>$row->kasir_id,
			'pendaftaran_id' =>$row->pendaftaran_id,
			'idpasien' =>$row->idpasien,
			'no_medrec' =>$row->no_medrec,
			'title' =>'',
			'namapasien' =>$row->namapasien,
			'nominal' =>$total_tunai,
			'nominal_non_tunai' =>$nominal_non_tunai,
			'st_gabungan' =>$st_gabungan,
			'idgroup_gabungan' =>$idgroup_gabungan,
			'posisi_gabungan' =>$posisi_gabungan,
			'idkelompok' =>$row->idkelompok,
			'namakelompok' =>$row->namakelompok,
			'idgroup_diskon' =>$idgroup_diskon,
			'nominal_diskon' =>$nominal_diskon,
			'posisi_diskon' =>'D',
			'idgroup_round' =>$idgroup_round,
			'idakun_round' =>$idakun_round,
			'nominal_round' =>$nominal_round,
			'posisi_round' =>'K',
			'user_trx' =>$row->edited_nama,
			'userid_trx' =>$row->edited_by,
			'date_trx' =>$row->tanggal_pendaftaran,
			'user_verif' => $this->session->userdata('user_name'),
			'userid_verif' => $this->session->userdata('user_id'),
			'date_verif' => date('Y-m-d H:i:s'),


		);
		$this->db->insert('tvalidasi_pendapatan_rajal_detail',$data_detail);
		$idhead=$this->db->insert_id();
		$q_bayar="SELECT H.id as idbayar_id,H.idmetode as idmetode
				,H.idbank as bankid,mbank.nama as bank,H.nominal as nominal_bayar
				,S.id as sumber_kas_id,S.nama as sumber_nama
				,J.id as jenis_kas_id,J.nama as jenis_kas_nama
				,'D' as posisi_akun
				,CASE WHEN H.idmetode='1' THEN ST.idakun_tunai_rajal ELSE S.idakun END as idakun
				FROM tkasir_pembayaran H 
				LEFT JOIN mbank ON mbank.id=H.idbank
				LEFT JOIN msumber_kas S ON S.bank_id=H.idbank AND H.idmetode !='1'
				LEFT JOIN mjenis_kas J ON J.id=S.jenis_kas_id
				LEFT JOIN msetting_jurnal_pendapatan_general ST ON ST.id='1' AND H.idmetode='1'
				WHERE H.idkasir='$kasir_id' AND H.idmetode IN(1,2,3,4)";
		$row_bayar=$this->db->query($q_bayar)->result();
		foreach($row_bayar as $r){
			// print_r($row_bayar);exit();
			$data = array(
				'idvalidasi' => $idvalidasi,
				'idhead' => $idhead,
				'idbayar_id' => $r->idbayar_id,
				'jenis_kas_id' => $r->jenis_kas_id,
				'jenis_kas_nama' => $r->jenis_kas_nama,
				'sumber_kas_id' => $r->sumber_kas_id,
				'sumber_nama' => $r->sumber_nama,
				'bank' => $r->bank,
				'bankid' => $r->bankid,
				'idmetode' => $r->idmetode,
				'metode_nama' => metodePembayaran($r->idmetode),
				'nominal_bayar' => $r->nominal_bayar,
				'idakun' => $r->idakun,
				'posisi_akun' => $r->posisi_akun,
			);
			$this->db->insert('tvalidasi_pendapatan_rajal_bayar',$data);
		}
		
			if ($st_insert_detail=='1'){//Jika Detail Di insert
					//INSERT DETAIL NON RACIKAN
					$q_far="SELECT
							tpasien_penjualan_nonracikan.id AS iddetail,       
							tpasien_penjualan.nopenjualan AS nopenjualan,
							tpasien_penjualan.tanggal AS tanggal,
							tpasien_penjualan_nonracikan.idunit AS idunit,
							munitpelayanan.nama AS namaunit,
							view_barang.id AS idtarif,
							view_barang.nama AS namatarif,
							tpasien_penjualan_nonracikan.kuantitas,
							tpasien_penjualan_nonracikan.harga_dasar AS hargadasar,
							tpasien_penjualan_nonracikan.margin,
							tpasien_penjualan_nonracikan.harga AS hargajual,
							tpasien_penjualan_nonracikan.diskon,
							tpasien_penjualan_nonracikan.diskon_rp,
							tpasien_penjualan_nonracikan.tuslah,
							tpasien_penjualan_nonracikan.totalharga AS totalkeseluruhan,
									tpasien_penjualan_nonracikan.idtipe
						  FROM
							tpasien_penjualan 
							INNER JOIN tpasien_penjualan_nonracikan ON tpasien_penjualan_nonracikan.idpenjualan = tpasien_penjualan.id AND tpasien_penjualan_nonracikan.status = 1
							LEFT JOIN view_barang ON view_barang.id = tpasien_penjualan_nonracikan.idbarang AND view_barang.idtipe = tpasien_penjualan_nonracikan.idtipe
							LEFT JOIN munitpelayanan ON munitpelayanan.id = tpasien_penjualan_nonracikan.idunit
						  WHERE
							tpasien_penjualan.id = '$pendaftaran_id'				
							AND tpasien_penjualan.status != 0";
					// print_r($q_far);exit();
					$list_far=$this->db->query($q_far)->result();
					foreach ($list_far as $row_far) {
							// print_r($row_far);exit();
							$this->insert_tarif_farmasi($idhead,$row_far,0,$idpoliklinik,$row->idtipe,$idvalidasi);
					}
					//RACIKAN
					$q_far="SELECT
							tpasien_penjualan_racikan.id AS iddetail,
							tpasien_penjualan.nopenjualan AS nopenjualan,
							tpasien_penjualan.tanggal AS tanggal,
							0 AS idtarif,
							tpasien_penjualan_racikan.namaracikan AS namatarif,
							tpasien_penjualan_racikan.kuantitas,
							tpasien_penjualan_racikan.totalharga AS hargadasar,
							0 AS margin,
							tpasien_penjualan_racikan.harga AS harga,
							tpasien_penjualan_racikan.totalharga AS hargajual,
							tpasien_penjualan_racikan.tuslah AS tuslah,
							0 AS diskon,
							tpasien_penjualan_racikan.totalharga AS totalkeseluruhan
						  FROM
							tpasien_penjualan
							INNER JOIN tpasien_penjualan_racikan ON tpasien_penjualan_racikan.idpenjualan = tpasien_penjualan.id AND tpasien_penjualan_racikan.`status` = '1'
						  WHERE
							tpasien_penjualan.id = '$pendaftaran_id'
							AND tpasien_penjualan.status != 0
							AND tpasien_penjualan_racikan.id IS NOT NULL";
					$list_far=$this->db->query($q_far)->result();
					foreach ($list_far as $row_far) {
						$this->insert_tarif_farmasi_racikan($idhead,$row_far,0,$idpoliklinik,$row->idtipe,$idvalidasi);
						// $this->insert_tarif_farmasi_racikan($idheader,$row,$idtipe,$idpoliklinik,3,$idvalidasi);
					}
		
			}
		return true;
		
	}
	// function insert_tarif_rajal_gabungan($idhead,$row,$idvalidasi){
		// $idkelompok=$row->idkelompok;
		// $idrekanan=$row->idrekanan;
	// }
	function insert_tarif_rajal($idhead,$row,$ref,$idjenis,$idvalidasi){
		$idtarif=$row->idtarif;
		$where='';
		if ($idjenis > 2){
			$where ="WHERE H.idtarif='$idtarif' AND H.kelas=0";
		}else{
			$where =" WHERE H.id='$idtarif'";			
		}		
		if ($idjenis=='1'){
			 // print_r($row);exit();
		}
		$q="SELECT H.group_jasasarana,H.group_jasapelayanan,H.group_bhp,H.group_biayaperawatan
			,H.group_jasasarana_diskon,H.group_jasapelayanan_diskon,H.group_bhp_diskon,H.group_biayaperawatan_diskon
			FROM ".$ref." H
			 ".$where;
		$row_tarif=$this->db->query($q)->row();
		$group_diskon_all=$this->get_diskon_all_tarif($idtarif,$idjenis);
		$data_detail=array(
			'idvalidasi' => $idvalidasi,
			'idhead' => $idhead,
			'ref_tabel' => $ref,
			'idjenis' => $idjenis,
			'iddetail' => $row->iddetail,
			'idtarif' => $row->idtarif,
			'namatarif' => $row->namatarif,
			'jasasarana' => $row->jasasarana,
			'jasapelayanan' => $row->jasapelayanan,
			'bhp' => $row->bhp,
			'biayaperawatan' => $row->biayaperawatan,
			
			'jasasarana_diskon' => $row->jasasarana_disc,
			'jasapelayanan_diskon' => $row->jasapelayanan_disc,
			'bhp_diskon' => $row->bhp_disc,
			'biayaperawatan_diskon' => $row->biayaperawatan_disc,
			
			'subtotal' => $row->subtotal,
			'kuantitas' => $row->kuantitas,
			'total' => $row->total,
			'diskon' => $row->diskon,
			'diskon_rp' => $row->diskon,
			'totalkeseluruhan' => $row->totalkeseluruhan,
			
			'group_jasasarana' => $row_tarif->group_jasasarana,
			'group_jasapelayanan' => $row_tarif->group_jasapelayanan,
			'group_bhp' => $row_tarif->group_bhp,
			'group_biayaperawatan' => $row_tarif->group_biayaperawatan,
			'group_jasasarana_diskon' => $row_tarif->group_jasasarana_diskon,
			'group_jasapelayanan_diskon' => $row_tarif->group_jasapelayanan_diskon,
			'group_bhp_diskon' => $row_tarif->group_bhp_diskon,
			'group_biayaperawatan_diskon' => $row_tarif->group_biayaperawatan_diskon,
			'group_diskon_all' => $group_diskon_all,
		);
		$this->db->insert('tvalidasi_pendapatan_rajal_detail_tindakan',$data_detail);
		
		return true;
	}
	function get_diskon_all_tarif($idtarif,$idjenis){
		if ($idjenis=='1'){
			$q="SELECT H.group_diskon_all FROM mtarif_administrasi H WHERE H.id='$idtarif'";
		}
		if ($idjenis=='2'){
			$q="SELECT H.group_diskon_all FROM mtarif_rawatjalan H WHERE H.id='$idtarif'";
		}
		if ($idjenis=='3'){
			$q="SELECT H.group_diskon_all FROM mtarif_laboratorium H WHERE H.id='$idtarif'";
		}
		if ($idjenis=='4'){
			$q="SELECT H.group_diskon_all FROM mtarif_radiologi H WHERE H.id='$idtarif'";
		}
		if ($idjenis=='5'){
			$q="SELECT H.group_diskon_all FROM mtarif_fisioterapi H WHERE H.id='$idtarif'";
		}
		return $this->db->query($q)->row('group_diskon_all');
	}
	function insert_tarif_obat_alkes($idhead,$row,$asalrujukan,$idpoliklinik,$idtipe,$idvalidasi){//IGD
		$idtarif=$row->idtarif;
		// print_r($row);exit();
		$q="SELECT H.idtipe,H.id,H.idkategori,K.nama as kategori,H.nama as nambarang 
			,compare_value_5(MAX(IF(SB.idbarang =H.id,SB.idgroup_penjualan,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = H.idkategori,SB.idgroup_penjualan,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_penjualan,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='$idpoliklinik',SB.idgroup_penjualan,NULL))
						,MAX(IF(SB.asalrujukan='$asalrujukan' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_penjualan,NULL))) idgroup_penjualan
			,compare_value_5(MAX(IF(SB.idbarang =H.id,SB.idgroup_diskon,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = H.idkategori,SB.idgroup_diskon,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_diskon,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='$idpoliklinik',SB.idgroup_diskon,NULL))
						,MAX(IF(SB.asalrujukan='$asalrujukan' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_diskon,NULL))) idgroup_diskon
			FROM view_barang_all H
			LEFT JOIN mdata_kategori K ON K.id=H.idkategori
			LEFT JOIN msetting_jurnal_pendapatan_jual_rajal SB ON SB.asalrujukan='$asalrujukan'
			WHERE H.id='$idtarif' AND H.idtipe='$idtipe'
			GROUP BY H.idtipe,H.id";
		$row_tarif=$this->db->query($q)->row();
		
		$data_detail=array(
			'idvalidasi' => $idvalidasi,
			'iddetail' => $row->iddetail,
			'idhead' => $idhead,
			'idtipe' => $idtipe,
			'idkategori' => $row_tarif->idkategori,
			'idbarang' => $idtarif,
			'namabarang' => $row->namatarif,
			'namakategori' => $row_tarif->kategori,
			'namatipe' => GetTipeKategoriBarang($idtipe),
			'hargadasar' => $row->hargadasar,
			'margin' => $row->margin,
			'hargajual' => $row->hargajual,
			'kuantitas' => $row->kuantitas,
			// 'total' => $row->total,
			'diskon' => $row->diskon,
			'diskon_rp' => $row->diskon,
			'totalkeseluruhan' => $row->totalkeseluruhan,
			'idgroup_penjualan' => $row_tarif->idgroup_penjualan,
			'idgroup_diskon' => $row_tarif->idgroup_diskon,
			'posisi_penjualan' => 'K',
			'posisi_diskon' => 'D',			
		);
		$this->db->insert('tvalidasi_pendapatan_rajal_detail_tindakan_obat',$data_detail);
		
		return true;
	}
	function insert_tarif_farmasi($idhead,$row,$asalrujukan,$idpoliklinik,$idtipe,$idvalidasi){//FARMASI NON RACIKAN
		// print_r($row);exit();
		$idtarif=$row->idtarif;
		$q="SELECT H.idtipe,H.id,H.idkategori,K.nama as kategori,H.nama as nambarang 
			,compare_value_5(MAX(IF(SB.idbarang =H.id,SB.idgroup_penjualan,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = H.idkategori,SB.idgroup_penjualan,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_penjualan,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='$idpoliklinik',SB.idgroup_penjualan,NULL))
						,MAX(IF(SB.asalrujukan='$asalrujukan' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_penjualan,NULL))) idgroup_penjualan
			,compare_value_5(MAX(IF(SB.idbarang =H.id,SB.idgroup_diskon,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = H.idkategori,SB.idgroup_diskon,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_diskon,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='$idpoliklinik',SB.idgroup_diskon,NULL))
						,MAX(IF(SB.asalrujukan='$asalrujukan' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_diskon,NULL))) idgroup_diskon
			,compare_value_5(MAX(IF(SB.idbarang =H.id,SB.idgroup_tuslah,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = H.idkategori,SB.idgroup_tuslah,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_tuslah,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='$idpoliklinik',SB.idgroup_tuslah,NULL))
						,MAX(IF(SB.asalrujukan='$asalrujukan' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_tuslah,NULL))) idgroup_tuslah
			FROM view_barang_all H
			LEFT JOIN mdata_kategori K ON K.id=H.idkategori
			LEFT JOIN msetting_jurnal_pendapatan_farmasi_nr SB ON SB.asalrujukan='$asalrujukan'
			WHERE H.id='$idtarif' AND H.idtipe='$idtipe'
			GROUP BY H.idtipe,H.id";
		$row_tarif=$this->db->query($q)->row();
		// $diskon_rp=$row->diskon * $row->hargajual /100;
		if ($row_tarif){
				$data_detail=array(
					'idvalidasi' => $idvalidasi,
					'idhead' => $idhead,
					'idtipe' => $idtipe,
					'iddetail' => $row->iddetail,
					'idkategori' => $row_tarif->idkategori,
					'idbarang' => $idtarif,
					'namabarang' => $row->namatarif,
					'namakategori' => $row_tarif->kategori,
					'namatipe' => GetTipeKategoriBarang($idtipe),
					'hargadasar' => $row->hargadasar,
					'margin' => $row->margin,
					'hargajual' => $row->hargajual,
					'kuantitas' => $row->kuantitas,
					// 'total' => $row->total,
					'diskon' => $row->diskon,
					'diskon_rp' => $row->diskon_rp,
					'total_diskon' => $row->diskon_rp * $row->kuantitas,
					'total_jual' => $row->hargajual * $row->kuantitas,
					'tuslah' => $row->tuslah,
					'totalkeseluruhan' => $row->totalkeseluruhan,
					'idgroup_penjualan' => $row_tarif->idgroup_penjualan,
					'idgroup_diskon' => $row_tarif->idgroup_diskon,
					'idgroup_tuslah' => $row_tarif->idgroup_tuslah,
					'posisi_penjualan' => 'K',
					'posisi_diskon' => 'D',			
					'posisi_tuslah' => 'K',			
				);
				$this->db->insert('tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan',$data_detail);
		}
		return true;
	}
	function insert_tarif_farmasi_racikan($idhead,$row,$asalrujukan,$idpoliklinik,$idtipe,$idvalidasi){//FARMASI RACIKAN
		$idtarif=$row->idtarif;
		$idgroup_tuslah=$this->db->query("SELECT H.group_tuslah_racikan FROM msetting_jurnal_pendapatan_general H")->row('group_tuslah_racikan');
		$id=$row->iddetail;
		// print_r($row);exit();
		
		$data_detail=array(
			'idvalidasi' => $idvalidasi,
			'idhead' => $idhead,
			// 'idtipe' => $idtipe,
			'iddetail' => $row->iddetail,
			'namabarang' => $row->namatarif,
			'hargadasar' => $row->harga,
			'margin' => $row->margin,
			'tuslah' => $row->tuslah,
			'kuantitas' => $row->kuantitas,
			// 'diskon_rp' => $row->total,
			'diskon' => $row->diskon,
			// 'diskon_rp' => $row->diskon * $row->hargajual /100,
			'totalkeseluruhan' => $row->totalkeseluruhan,
			'idgroup_tuslah' => $idgroup_tuslah,
			'posisi_tuslah' => 'K',			
		);
		$this->db->insert('tvalidasi_pendapatan_rajal_detail_farmasi_racikan',$data_detail);
		$idracikan=$this->db->insert_id();
		$q="SELECT H.id as iddetail,H.idtipe,B.idkategori,H.idbarang,K.nama as kategori,B.nama as namatarif 
			,H.harga_dasar,H.margin,H.kuantitas,H.harga as hargajual,H.diskon,H.totalharga as totalkeseluruhan,H.diskon_rp
			,compare_value_5(MAX(IF(SB.idbarang =H.idbarang,SB.idgroup_penjualan,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = B.idkategori,SB.idgroup_penjualan,NULL))
									,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_penjualan,NULL))
									,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='1',SB.idgroup_penjualan,NULL))
									,MAX(IF(SB.asalrujukan='1' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_penjualan,NULL))) idgroup_penjualan
						,compare_value_5(MAX(IF(SB.idbarang =H.idbarang,SB.idgroup_diskon,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = B.idkategori,SB.idgroup_diskon,NULL))
									,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_diskon,NULL))
									,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='1',SB.idgroup_diskon,NULL))
									,MAX(IF(SB.asalrujukan='1' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_diskon,NULL))) idgroup_diskon
			FROM tpasien_penjualan_racikan_obat H
			LEFT JOIN view_barang_all B ON B.idtipe=H.idtipe AND B.id=H.idbarang
			LEFT JOIN mdata_kategori K ON K.id=B.idkategori
			LEFT JOIN msetting_jurnal_pendapatan_farmasi_r SB ON SB.asalrujukan='1'
			WHERE H.idracikan='$id'
			GROUP BY H.id";
			// print_r($q);exit();
		$row_tarif=$this->db->query($q)->result();
		foreach($row_tarif as $r){
			$data_detail=array(
				'idvalidasi' => $idvalidasi,
				'idhead' => $idhead,
				'idracikan' => $idracikan,
				'idtipe' => $r->idtipe,
				'iddetail' => $r->iddetail,
				'idkategori' => $r->idkategori,
				'idbarang' => $r->idbarang,
				'namabarang' => $r->namatarif,
				'namakategori' => $r->kategori,
				'namatipe' => GetTipeKategoriBarang($r->idtipe),
				'hargadasar' => $r->harga_dasar,
				'margin' => $r->margin,
				'hargajual' => $r->hargajual,
				'kuantitas' => $r->kuantitas,
				'diskon' => $r->diskon,
				'diskon_rp' => $r->diskon_rp,
				'total_jual' => $r->hargajual * $r->kuantitas,
				'total_diskon' => ($r->diskon_rp * $r->kuantitas),
				'totalkeseluruhan' => $r->totalkeseluruhan,
				'idgroup_penjualan' => $r->idgroup_penjualan,
				'idgroup_diskon' => $r->idgroup_diskon,
				'posisi_penjualan' => 'K',
				'posisi_diskon' => 'D',			
			);
			$this->db->insert('tvalidasi_pendapatan_rajal_detail_farmasi_racikan_detail',$data_detail);
		}
		
		return true;
	}
	
	public function InsertJurnalKas_HonorDokter($id){
	   
	   $q="SELECT H.id as idtransaksi,H.tanggal_trx_bayar as tanggal,H.tanggal_pembayaran,H.notransaksi,H.iddokter,H.namadokter,M.idkategori,MD.nama as kategori
			,(H.nominal) as nominal
			,(H.nominal_beban_jasa) as nominal_beban
			,(H.nominal_pajak_dokter) as nominal_pajak
			,(H.nominal_piutang_pembelian_obat) as nominal_pembelian
			,(H.nominal_potongan_kasbon) as nominal_kasbon 
			,(H.nominal_potongan_berobat) as nominal_berobat
			,(H.nominal_potongan_lainlain) as nominal_potongan
			,compare_value(MAX(IF(SB.iddokter = H.iddokter,SB.idakun_beban,NULL)),MAX(IF(SB.iddokter = 0 AND SB.idkategori = M.idkategori,SB.idakun_beban,NULL))
			,MAX(IF(SB.iddokter = 0 AND SB.idkategori = 0,SB.idakun_beban,NULL))) idakun_beban
			,compare_value(MAX(IF(SB.iddokter = H.iddokter,SB.idakun_pajak,NULL)),MAX(IF(SB.iddokter = 0 AND SB.idkategori = M.idkategori,SB.idakun_pajak,NULL))
			,MAX(IF(SB.iddokter = 0 AND SB.idkategori = 0,SB.idakun_pajak,NULL))) idakun_pajak
			,compare_value(MAX(IF(SB.iddokter = H.iddokter,SB.idakun_pembelian,NULL)),MAX(IF(SB.iddokter = 0 AND SB.idkategori = M.idkategori,SB.idakun_pembelian,NULL))
			,MAX(IF(SB.iddokter = 0 AND SB.idkategori = 0,SB.idakun_pembelian,NULL))) idakun_pembelian
			,compare_value(MAX(IF(SB.iddokter = H.iddokter,SB.idakun_berobat,NULL)),MAX(IF(SB.iddokter = 0 AND SB.idkategori = M.idkategori,SB.idakun_berobat,NULL))
			,MAX(IF(SB.iddokter = 0 AND SB.idkategori = 0,SB.idakun_berobat,NULL))) idakun_berobat
			,compare_value(MAX(IF(SB.iddokter = H.iddokter,SB.idakun_kasbon,NULL)),MAX(IF(SB.iddokter = 0 AND SB.idkategori = M.idkategori,SB.idakun_kasbon,NULL))
			,MAX(IF(SB.iddokter = 0 AND SB.idkategori = 0,SB.idakun_kasbon,NULL))) idakun_kasbon
			,compare_value(MAX(IF(SB.iddokter = H.iddokter,SB.idakun_potongan,NULL)),MAX(IF(SB.iddokter = 0 AND SB.idkategori = M.idkategori,SB.idakun_potongan,NULL))
			,MAX(IF(SB.iddokter = 0 AND SB.idkategori = 0,SB.idakun_potongan,NULL))) idakun_potongan
			,S.st_auto_posting
			FROM thonor_dokter H
			LEFT JOIN thonor_dokter_detail D ON D.idhonor=H.id
			LEFT JOIN mdokter M ON M.id=H.iddokter
			LEFT JOIN mdokter_kategori MD ON MD.id=M.idkategori
			LEFT JOIN msetting_jurnal_honor_detail SB ON SB.setting_id='1'
			LEFT JOIN msetting_jurnal_honor S ON S.id='1'
			WHERE H.id='$id'";
		$row=$this->db->query($q)->row();
		$data_header=array(
			'id_reff' => 9,
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal,
			'notransaksi' => $row->notransaksi,
			'keterangan' => 'HONOR DOKTER : '.$row->namadokter.' ('.HumanDateShort($row->tanggal_pembayaran).')',
			'nominal' => $row->nominal,
			// 'idakun' => $row->idakun,
			// 'posisi_akun' => $row->posisi_akun,
			'status' => 1,
			'st_auto_posting' => $row->st_auto_posting,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
            'created_date'=>date('Y-m-d H:i:s')
		);
		// print_r($data_header);exit();
		$this->db->insert('tvalidasi_kas',$data_header);
		$idvalidasi=$this->db->insert_id();
		
		$st_auto_posting=$row->st_auto_posting;
		$data_header=array(
			'idvalidasi' => $idvalidasi,
			'idtransaksi' => $row->idtransaksi,
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => $row->tanggal,
			'notransaksi' => $row->notransaksi,
			'iddokter' => $row->iddokter,
			'idkategori' => $row->idkategori,
			'namadokter' => $row->namadokter,
			'kategori' => $row->kategori,
			// 'catatan' => $row->catatan,
			'nominal' => $row->nominal,
			'idakun_beban' => $row->idakun_beban,
			'posisi_beban' => 'D',
			'idakun_pajak' => $row->idakun_pajak,
			'posisi_pajak' => 'K',
			'idakun_pembelian' => $row->idakun_pembelian,
			'posisi_pembelian' => 'K',
			'idakun_berobat' => $row->idakun_berobat,
			'posisi_berobat' => 'K',
			'idakun_kasbon' => $row->idakun_kasbon,
			'posisi_kasbon' => 'K',
			'idakun_potongan' => $row->idakun_potongan,
			'posisi_potongan' => 'K',
			'nominal_beban' => $row->nominal_beban,
			'nominal_pajak' => $row->nominal_pajak,
			'nominal_pembelian' => $row->nominal_pembelian,
			'nominal_berobat' => $row->nominal_berobat,
			'nominal_kasbon' => $row->nominal_kasbon,
			'nominal_potongan' => $row->nominal_potongan,
			'status' => 1,
			'st_posting' => 0,
			// 'st_auto_posting' => $row->st_auto_posting,
			// 'created_by'=>$this->session->userdata('user_id'),
			// 'created_nama'=>$this->session->userdata('user_name'),
            // 'created_date'=>date('Y-m-d H:i:s')
			

		);
		$st_auto_posting=$row->st_auto_posting;
		$this->db->insert('tvalidasi_kas_09_honor',$data_header);
		
		//INSERT PEMBAYARAN
		$q="SELECT 
			H.id as idbayar_id,H.jenis_kas_id,J.nama as jenis_kas_nama
			,H.sumber_kas_id,S.nama as sumber_nama
			,S.bank_id as bankid,mbank.nama as bank
			,H.idmetode,RM.metode_bayar as metode_nama,H.nominal_bayar, S.idakun,'K' as posisi_akun
			FROM thonor_pembayaran H
			LEFT JOIN mjenis_kas J ON J.id=H.jenis_kas_id
			LEFT JOIN msumber_kas S ON S.id=H.sumber_kas_id
			LEFT JOIN ref_metode RM ON RM.id=H.idmetode
			LEFT JOIN mbank ON mbank.id=S.bank_id
			WHERE H.idhonor='$id' AND H.`status`='1'";
		$rows=$this->db->query($q)->result();
		foreach($rows as $r){
			$data_bayar=array(
				'idvalidasi' =>$idvalidasi,
				'idhonor' =>$id,
				'idbayar_id' =>$r->idbayar_id,
				'jenis_kas_id' =>$r->jenis_kas_id,
				'jenis_kas_nama' =>$r->jenis_kas_nama,
				'sumber_kas_id' =>$r->sumber_kas_id,
				'sumber_nama' =>$r->sumber_nama,
				'bank' =>$r->bank,
				'bankid' =>$r->bankid,
				'idmetode' =>$r->idmetode,
				'metode_nama' =>$r->metode_nama,
				'nominal_bayar' =>$r->nominal_bayar,
				'idakun' =>$r->idakun,
				'posisi_akun' =>$r->posisi_akun,

			);
			$this->db->insert('tvalidasi_kas_09_honor_bayar',$data_bayar);
		}
		if ($st_auto_posting=='1'){
			$data_header=array(				
				'st_posting' => 1,
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
				

			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_kas',$data_header);
		}
		return true;
   }
   
   ///RANAP
   function GenerateValidasiPendapatanRanapRefund($idrefund,$tipe_refund){
	   if ($tipe_refund=='0'){
		   $q="SELECT H.tanggal as tanggal_transaksi,'5' as tipe_trx,RI.idtipe,RI.tanggaldaftar as tanggal_pendaftaran,H.tipe as tipe_refund,RI.id as pendaftaran_id,RI.nopendaftaran as notransaksi,RI.namapasien,RI.no_medrec,RI.idpasien
				,RI.idkelompokpasien as idkelompok,MK.nama as namakelompok,TP.idpoliklinik,RI.idrekanan,MR.nama as namarekanan
				,RI.idruangan,RI.idkelas,RI.idbed,RI.iddokterpenanggungjawab as iddokter,MD.nama as dokter,H.totalrefund as nominal
				FROM trefund H
				LEFT JOIN trefund_detail D ON D.idrefund=H.id
				LEFT JOIN trawatinap_deposit Def ON Def.id=D.idtransaksi
				LEFT JOIN trawatinap_pendaftaran RI ON RI.id=Def.idrawatinap
				LEFT JOIN mpasien_kelompok MK ON MK.id=RI.idkelompokpasien
				LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=RI.idpoliklinik
				LEFT JOIN mrekanan MR ON MR.id=RI.idrekanan
				LEFT JOIN mdokter MD ON MD.id=RI.iddokterpenanggungjawab
				WHERE H.id='$idrefund'
				GROUP BY H.id";
		  $data_header=$this->db->query($q)->row_array();
			$this->db->insert('tvalidasi_pendapatan_ranap',$data_header);
			$idvalidasi=$this->db->insert_id();
			$total_refund=$data_header['nominal'];
			$q="SELECT H.id as iddetail,H.norefund as notransaksi,DATE(H.tanggal) as tanggal,H.metode as idmetode,H.totalrefund as nominal,S.idakun,'K' as posisi_akun  
												FROM trefund H
												LEFT JOIN msetting_jurnal_refund_deposit S ON S.idmetode=H.metode
												WHERE H.id='$idrefund' AND H.tipe='0' AND H.metode='1' ";
			$row_refund=$this->db->query($q)->result();
			if ($row_refund){
				$st_insert_all=1;
				foreach($row_refund as $row){
					$data=array(
						'idvalidasi' =>$idvalidasi,
						'tipe' =>2,
						'tanggal' =>YMDFormat($row->tanggal),
						'iddetail' =>$row->iddetail,
						'notransaksi' =>$row->notransaksi,
						'idmetode' =>$row->idmetode,
						'nominal' =>$row->nominal,
						'idakun' =>$row->idakun,
						'posisi_akun' =>$row->posisi_akun,
						'nominal_refund' =>0,
					);
					$this->db->insert('tvalidasi_pendapatan_ranap_bayar_deposit',$data);
				}
			}
			$q="SELECT D.id as iddetail,D.nodeposit as notransaksi,D.tanggal,D.idmetodepembayaran as idmetode,D.idbank,D.nominal,S.idakun,'D' as posisi_akun   from trefund_detail H
			LEFT JOIN trawatinap_deposit D ON D.id=H.idtransaksi
			LEFT JOIN msetting_jurnal_deposit_pendapatan S ON S.idtipe=D.idmetodepembayaran
			WHERE H.idrefund='$idrefund'
			ORDER BY D.id DESC";
			$list_deposit=$this->db->query($q)->result();
			$sisa_deposit=$total_refund;
			foreach($list_deposit as $row){
				if ($sisa_deposit > $row->nominal){
					$nominal=$row->nominal;
					$nominal_refund=0;
				}else{
					$nominal=$sisa_deposit;
					$nominal_refund=0;
					
				}			
				$sisa_deposit=$sisa_deposit-$nominal;
				$data=array(
					'idvalidasi' =>$idvalidasi,
					'tipe' =>1,
					'tanggal' =>YMDFormat($row->tanggal),
					'iddetail' =>$row->iddetail,
					'notransaksi' =>$row->notransaksi,
					'idmetode' =>$row->idmetode,
					'nominal' =>$nominal,
					'idakun' =>$row->idakun,
					'posisi_akun' =>$row->posisi_akun,
					'nominal_refund' =>$nominal_refund,
				);
				$this->db->insert('tvalidasi_pendapatan_ranap_bayar_deposit',$data);
			}
	   
	   }
	   
	   // print_r('Refund');exit();
   }
   function GenerateValidasiPendapatanRanap($idpendaftaran,$kasir_id,$tipe_trx='2',$tanggal_transaksi=''){
		if ($this->cek_dupilicateRanap($idpendaftaran,$kasir_id)==false){//Jika Belum ada \
			$dataPendaftaran=$this->get_data_pendaftaranRanap($kasir_id);
			$q="SELECT H.tanggal,H.totalharusdibayar,H.total,H.diskonrp,H.diskonpersen,H.nominal_round,H.deposit
			,SUM(CASE WHEN D.idmetode IN (1,2,3,4) THEN D.nominal ELSE 0 END) as tunai
			,SUM(CASE WHEN D.idmetode NOT IN (1,2,3,4) THEN D.nominal ELSE 0 END) as non_tunai
			FROM trawatinap_tindakan_pembayaran H
			LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON D.idtindakan=H.id
			WHERE H.id='$kasir_id' 
			GROUP BY H.id";
			$row_kasir=$this->db->query($q)->row();
			$total_deposit=$row_kasir->deposit;
			$total_tunai=$row_kasir->tunai+$total_deposit;
			$total_non_tunai=$row_kasir->non_tunai;
			$tanggal_kasir=$row_kasir->tanggal;
			// print_r($dataPendaftaran);exit();
			$kelas=$dataPendaftaran['idkelas'];
			$idruangan=$dataPendaftaran['idruangan'];
			$idgroup_gabungan=0;
			$st_insert_detail='1';
			$idvalidasi='';
			$idheader='';

			$idvalidasi=$this->Tvalidasi_model->InsertJurnalPendapatanRanaplHead($dataPendaftaran,$tanggal_transaksi,$total_tunai,$total_non_tunai,$row_kasir,$tipe_trx);
			// if ($row_kasir->deposit > 0){
				// $this->insert_deposit($idpendaftaran,$idvalidasi);			
			// }
			foreach ($this->Trawatinap_verifikasi_model->viewRincianAdministrasiRawatInap($idpendaftaran) as $row) {
				$this->insert_tarif_ranap_adm($idvalidasi,$row,$dataPendaftaran);				
			}
			$this->insert_obat_alkes_ranap($idpendaftaran,$idvalidasi,$idruangan,$kelas);
			 // [BAGI HASIL] RUANG KAMAR OPERASI
			foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiKamar($idpendaftaran) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_operasi_detail',2,$kelas);				
			}
			 // [BAGI HASIL] SEWA ALAT KAMAR OPERASI
			foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiSewaAlat($idpendaftaran) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_operasi_sewaalat_detail',3,$kelas);				
			}
			foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 1) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_rawatinap_detail',4,$kelas);				
			}
			foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 2) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_rawatinap_detail',5,$kelas);				
			}
			// JASA DOKTER RAWAT INAP - AMBULANCE
			foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 5) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_rawatinap_detail',6,$kelas);				
			}
			// JASA DOKTER RAWAT INAP - SEWA ALAT
			foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 4) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_rawatinap_detail',7,$kelas);				
			}
			// JASA DOKTER RAWAT INAP - LAIN LAIN
			foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 6) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_rawatinap_detail',8,$kelas);				
			}
			// JASA DOKTER LABORATORIUM - UMUM
			foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 1) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_laboratorium_detail',9,$kelas);				
			}
			foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 2) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_laboratorium_detail',10,$kelas);				
			}
			// JASA DOKTER LABORATORIUM - PMI
			foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 3) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_laboratorium_detail',11,$kelas);				
			}
			 // JASA DOKTER RADIOLOGI - X-RAY
			foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 1) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_radiologi_detail',12,$kelas);				
			}
			// JASA DOKTER RADIOLOGI - USG
			foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 2) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_radiologi_detail',13,$kelas);				
			}
			// JASA DOKTER RADIOLOGI - CT SCAN
			foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 3) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_radiologi_detail',14,$kelas);				
			}
			foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 4) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_radiologi_detail',15,$kelas);				
			}
			 // JASA DOKTER RADIOLOGI - BMD
			foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 5) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_radiologi_detail',16,$kelas);				
			}
			// JASA DOKTER FISIOTERAPI
			foreach ($this->Trawatinap_verifikasi_model->viewRincianFisioterapi($idpendaftaran) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_fisioterapi_detail',17,$kelas);				
			}
			$dataVisite = $this->Trawatinap_verifikasi_model->viewRincianRanapVisite($idpendaftaran);
			foreach ($dataVisite as $index => $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_visitedokter_detail',18,$kelas);				
			}
			// JASA DOKTER OPERATOR
			foreach ($this->Trawatinap_verifikasi_model->viewFeeOperasiJasaDokterOpertor($idpendaftaran) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_operasi_detail',19,$kelas);				
			}
			// JASA DOKTER ANESTHESI       
			foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterAnesthesi($idpendaftaran) as $row) {
				$this->insert_tarif_dokter_ko($idvalidasi,$row,'tkamaroperasi_jasada',1,$kelas);				
			}
			// JASA ASISTEN
			foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaAsisten($idpendaftaran) as $row) {
					$this->insert_tarif_dokter_ko_asisten($idvalidasi,$row,'tkamaroperasi_jasada',1,$kelas);				
			}
			// JASA DOKTER RAWAT JALAN
			foreach ($this->Trawatinap_verifikasi_model->viewRincianRajalTindakanFilteredIGD($idpendaftaran) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_rawatjalan',20,$kelas);				
			}
			// ADMIN RAJAL
			foreach ($this->Trawatinap_verifikasi_model->viewRincianAdministrasiRawatJalan($idpendaftaran) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_administrasi',21,$kelas);				
			}
			foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapRuangan($idpendaftaran) as $row) {
				$this->insert_tarif_ranap($idvalidasi,$row,'mtarif_ruangperawatan_detail',22,$kelas);				
			}
			
		}
		return true;
		
	}
	function cek_dupilicateRanap($idpendaftaran,$kasir_id){
		$q="SELECT H.id FROM tvalidasi_pendapatan_ranap H WHERE H.pendaftaran_id='$idpendaftaran' AND H.kasir_id='$kasir_id' AND H.tipe_trx NOT IN (2,3,4)";
		if ($this->db->query($q)->row('id')){
			return true;
		}else{
			return false;
		}

	}
	function get_data_pendaftaranRanap($id){
		$this->db->select("trawatinap_pendaftaran.id AS idpendaftaran,
                	trawatinap_pendaftaran.nopendaftaran,
                	trawatinap_pendaftaran.idpasien,
                	trawatinap_pendaftaran.no_medrec,
                	trawatinap_pendaftaran.title,
                	trawatinap_pendaftaran.namapasien,
                	trawatinap_pendaftaran.idtipe,
                	trawatinap_pendaftaran.idtipepasien,
                	trawatinap_pendaftaran.idkelas,
                	trawatinap_pendaftaran.idruangan,
                	trawatinap_pendaftaran.idbed,
                	IF(trawatinap_pendaftaran.idtipepasien = 1, 'Pasien RS', 'Pasien Pribadi') AS jenispasien,
                	trawatinap_pendaftaran.idkelompokpasien AS idkelompok,
                	mpasien_kelompok.nama AS namakelompok,
                	trawatinap_pendaftaran.idrekanan,
                	mrekanan.nama AS namarekanan,
                	trawatinap_pendaftaran.tanggaldaftar AS tanggal_pemeriksaan,
                  mdokterperujuk.idkategori AS idkategori_perujuk,
                  mdokterperujuk_kategori.nama AS namakategori_perujuk,
                  mdokterperujuk.id AS iddokter_perujuk,
                  mdokterperujuk.nama AS namadokter_perujuk,
                  mdokterperujuk.pajakranap AS pajak_perujuk,
                  mdokterperujuk.potonganrsranap AS potonganrs_perujuk,
                  tpoliklinik_pendaftaran.idtipe AS idasalpasien,
                  tpoliklinik_pendaftaran.idpoliklinik AS idpoliklinik");
				$this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = trawatinap_tindakan_pembayaran.idtindakan');
				$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik');
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien', 'LEFT');
				$this->db->join('mrekanan', 'mrekanan.id = trawatinap_pendaftaran.idrekanan', 'LEFT');
				$this->db->join('mdokter mdokterperujuk', 'mdokterperujuk.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
				$this->db->join('mdokter_kategori mdokterperujuk_kategori', 'mdokterperujuk_kategori.id = mdokterperujuk.idkategori', 'LEFT');
				$this->db->where('trawatinap_tindakan_pembayaran.id', $id);
				$this->db->where('trawatinap_tindakan_pembayaran.statusbatal', 0);
				$this->db->limit(1);
				$query = $this->db->get('trawatinap_tindakan_pembayaran');
				$row = $query->row();

				// Get Status Pembayaran Kasir Rawat Jalan
				$statusKasirRajal = $this->Trawatinap_tindakan_model->getDataPoliTindakan($row->idpendaftaran)->status_kasir;
				$dataPendaftaran = [
					'tanggal_pemeriksaan' => $row->tanggal_pemeriksaan,
					'idpasien' => $row->idpasien,
					'no_medrec' => $row->no_medrec,
					'title' => $row->title,
					'namapasien' => $row->namapasien,
					'kasir_id' => $id,
					'idkelas' => $row->idkelas,
					'idruangan' => $row->idruangan,
					'idbed' => $row->idbed,
					'idtipe' => $row->idtipe,
					'nopendaftaran' => $row->nopendaftaran,
					'idpendaftaran' => $row->idpendaftaran,
					'idtipepasien' => $row->idtipepasien,
					'idasalpasien' => $row->idasalpasien,
					'idpoliklinik' => $row->idpoliklinik,
					'jenis_pasien' => $row->jenispasien,
					'idkelompok' => $row->idkelompok,
					'namakelompok' => $row->namakelompok,
					'idrekanan' => $row->idrekanan,
					'namarekanan' => $row->namarekanan,
					'idkategori_perujuk' => $row->idkategori_perujuk,
					'namakategori_perujuk' => $row->namakategori_perujuk,
					'iddokter_perujuk' => $row->iddokter_perujuk,
					'namadokter_perujuk' => $row->namadokter_perujuk,
					'potonganrs_perujuk' => $row->potonganrs_perujuk,
					'pajak_perujuk' => $row->pajak_perujuk,
					'status_asuransi' => ($row->idkelompok != 5 ? 1 : 0),
					'status_kasir_rajal' => $statusKasirRajal,
				];
		return $dataPendaftaran;
	}
   function InsertJurnalPendapatanRanaplHead($dataPendaftaran,$tanggal_kasir,$total_tunai,$total_non_tunai,$row_kasir,$tipe_trx='1'){
	   $kasir_id=$dataPendaftaran['kasir_id'];
	   $st_gabungan=0;
	   $nominal_gabungan=0;
	   $nominal=$total_tunai;
	   $idgroup_gabungan=null;
		$idakun_gabungan=null;
		$idrekanan=$dataPendaftaran['idrekanan'];
		$idkelompok=$dataPendaftaran['idkelompok'];
		if ($total_tunai > 0 && $total_non_tunai > 0){
			$st_gabungan=1;
			// $nominal=$total_tunai;
			$nominal_gabungan=$total_tunai;
			$q="SELECT 
				compare_value_2(MAX(IF(H.idrekanan = '$idrekanan',H.idgroup,NULL)),MAX(IF(H.idrekanan=0 AND H.idkelompokpasien ='$idkelompok' ,H.idgroup,NULL))) as idgroup
				FROM msetting_jurnal_pendapatan_general_ranap H
				WHERE H.idkelompokpasien='$idkelompok'";				
			$idgroup_gabungan=$this->db->query($q)->row('idgroup');
			
		}
		
		if ($tipe_trx=='1'){
			$q_diskon="SELECT H.group_diskon_ranap,H.group_round_ranap FROM msetting_jurnal_pendapatan_general H WHERE H.id='1'";
			
			
			$row_diskon=$this->db->query($q_diskon)->row();
			$idgroup_diskon=$row_diskon->group_diskon_ranap;
			$idgroup_round=$row_diskon->group_round_ranap;
			if ($st_gabungan=='1'){
				$nominal_diskon=0;
				$nominal_round=0;
			}else{
				$nominal_diskon=$row_kasir->diskonrp;
				$nominal_round=$row_kasir->nominal_round;
			}
			
			
			$posisi_gabungan='K';	
			$where_bayar="AND D.idmetode IN(1,2,3,4)";
		}else{
			$nominal_diskon=$row_kasir->diskonrp;
			$nominal_round=$row_kasir->nominal_round;
			$idgroup_diskon=null;
			$idgroup_round=null;
			$posisi_gabungan='D';	
			$nominal=$total_tunai+$total_non_tunai;
			$where_bayar="AND D.idmetode NOT IN(1,2,3,4)";
		}
		$posisi_diskon='D';
		$posisi_round='K';
		$data_header=array(
			'tanggal_transaksi' => $tanggal_kasir,
			'tipe_trx' => $tipe_trx,
			'idtipe' => $dataPendaftaran['idtipe'],
			'tipe_refund' => null,
			'tanggal_pendaftaran' => $dataPendaftaran['tanggal_pemeriksaan'],
			'notransaksi' => $dataPendaftaran['nopendaftaran'],
			'pendaftaran_id' => $dataPendaftaran['idpendaftaran'],
			'kasir_id' => $dataPendaftaran['kasir_id'],
			'idpasien' => $dataPendaftaran['idpasien'],
			'title' => $dataPendaftaran['title'],
			'namapasien' => $dataPendaftaran['namapasien'],
			'no_medrec' => $dataPendaftaran['no_medrec'],
			'namapasien' => $dataPendaftaran['namapasien'],
			'idasalpasien' => $dataPendaftaran['idasalpasien'],
			'idkelompok' => $dataPendaftaran['idkelompok'],
			'namakelompok' => $dataPendaftaran['namakelompok'],
			'idpoliklinik' => $dataPendaftaran['idpoliklinik'],
			'idrekanan' => $dataPendaftaran['idrekanan'],
			'namarekanan' => $dataPendaftaran['namarekanan'],
			'idruangan' => $dataPendaftaran['idruangan'],
			'idkelas' => $dataPendaftaran['idkelas'],
			'idbed' => $dataPendaftaran['idbed'],
			'iddokter' => $dataPendaftaran['iddokter_perujuk'],
			'dokter' => $dataPendaftaran['namadokter_perujuk'],
			'nominal' => $nominal,
			'nominal_non_tunai' => $total_non_tunai,
			'nominal_tunai' => $total_tunai,
			'st_gabungan' => $st_gabungan,
			'nominal_gabungan' => $nominal_gabungan,
			'idgroup_gabungan' => $idgroup_gabungan,
			'posisi_gabungan' => $posisi_gabungan,
			'posisi_round' => $posisi_round,
			'posisi_diskon' => $posisi_diskon,
			'nominal_round' => $nominal_round,
			'nominal_diskon' => $nominal_diskon,
			'idgroup_diskon' => $idgroup_diskon,
			'idgroup_round' => $idgroup_round,
			'total' => $row_kasir->total,
			'diskonpersen' => $row_kasir->diskonpersen,
			'totalharusdibayar' => $row_kasir->totalharusdibayar,
			'deposit' => $row_kasir->deposit,

		);
		$this->db->insert('tvalidasi_pendapatan_ranap',$data_header);
		$idvalidasi=$this->db->insert_id();
		if ($tipe_trx=='1'){	
			$q_bayar="SELECT D.id as idbayar_id,D.idmetode as idmetode
					,D.idbank as bankid,mbank.nama as bank,D.nominal as nominal_bayar
					,S.id as sumber_kas_id,S.nama as sumber_nama
					,J.id as jenis_kas_id,J.nama as jenis_kas_nama
					,'D' as posisi_akun
					,CASE WHEN D.idmetode='1' THEN ST.idakun_tunai_ranap ELSE S.idakun END as idakun
					FROM trawatinap_tindakan_pembayaran H 
					LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON D.idtindakan=H.id
					LEFT JOIN mbank ON mbank.id=D.idbank
					LEFT JOIN msumber_kas S ON S.bank_id=D.idbank AND D.idmetode !='1'
					LEFT JOIN mjenis_kas J ON J.id=S.jenis_kas_id
					LEFT JOIN msetting_jurnal_pendapatan_general ST ON ST.id='1' AND D.idmetode='1'
					WHERE H.id='$kasir_id' ".$where_bayar;
			$row_bayar=$this->db->query($q_bayar)->result();
			foreach($row_bayar as $r){
				// print_r($row_bayar);exit();
				$data = array(
					'idvalidasi' => $idvalidasi,
					'idbayar_id' => $r->idbayar_id,
					'jenis_kas_id' => $r->jenis_kas_id,
					'jenis_kas_nama' => $r->jenis_kas_nama,
					'sumber_kas_id' => $r->sumber_kas_id,
					'sumber_nama' => $r->sumber_nama,
					'bank' => $r->bank,
					'bankid' => $r->bankid,
					'idmetode' => $r->idmetode,
					'metode_nama' => metodePembayaran($r->idmetode),
					'nominal_bayar' => $r->nominal_bayar,
					'idakun' => $r->idakun,
					'posisi_akun' => $r->posisi_akun,
				);
				$this->db->insert('tvalidasi_pendapatan_ranap_bayar',$data);
			}
		}else{
			$q_bayar="SELECT T.*,M.idakun FROM (SELECT H.id as idbayar_id,H.idmetode as idmetode,H.tipekontraktor,H.idkontraktor
				,H.nominal as nominal_bayar
				,'D' as posisi_akun
				,
				CASE 
				WHEN H.idmetode='5' THEN SK.group_karyawan_ranap  
				WHEN H.idmetode='6' THEN SK.group_karyawan_ranap  
				WHEN H.idmetode='7' THEN ST.group_tidak_tertagih_ranap  
				WHEN H.idmetode='8' THEN 
					compare_value_2(MAX(IF(SA.idrekanan = H.idkontraktor AND H.tipekontraktor='1',SA.idgroup,NULL)),
											MAX(IF(SA.idrekanan = 0 AND SA.idkelompokpasien = H.tipekontraktor,SA.idgroup,NULL))) 
					
				END as group_akun
				,CASE WHEN H.idmetode='7' THEN CONCAT(MK.nama,' ',IFNULL(R.nama,'')) ELSE '' END as rekanan
				FROM trawatinap_tindakan_pembayaran_detail H 
				LEFT JOIN msetting_jurnal_pendapatan_general SK ON SK.id='1'
				LEFT JOIN msetting_jurnal_pendapatan_general ST ON ST.id='1'
				LEFT JOIN msetting_jurnal_pendapatan_general_rajal_piutang SA ON SA.setting_id='1' AND SA.idkelompokpasien=H.tipekontraktor
				LEFT JOIN mrekanan R ON R.id=H.idkontraktor AND H.tipekontraktor='1'
				LEFT JOIN mpasien_kelompok MK ON MK.id=H.tipekontraktor AND H.idmetode='8'
				WHERE H.idtindakan='$kasir_id' AND H.idmetode NOT IN(1,2,3,4)
				GROUP BY H.id
				) T 
				LEFT JOIN mgroup_pembayaran_detail M ON M.idgroup=T.group_akun";
			$row_bayar=$this->db->query($q_bayar)->result();
			foreach($row_bayar as $r){
				// print_r($row_bayar);exit();
				$data = array(
					'idvalidasi' => $idvalidasi,
					'idbayar_id' => $r->idbayar_id,
					'idmetode' => $r->idmetode,
					'tipekontraktor' => $r->tipekontraktor,
					'idkontraktor' => $r->idkontraktor,
					'metode_nama' => metodePembayaran($r->idmetode).' '.$r->rekanan,
					'nominal_bayar' => $r->nominal_bayar,
					'group_akun' => $r->group_akun,
					'idakun' => $r->idakun,
					'posisi_akun' => $r->posisi_akun,
				);
				$this->db->insert('tvalidasi_pendapatan_ranap_bayar_non_tunai',$data);
			}
			
		}
		// print_r($data_header);exit();
		return $idvalidasi;
	}
	function insert_tarif_ranap($idvalidasi,$row,$ref,$idjenis,$idkelas){
		$arr_dokter=array('19');
		$nama_dokter='';
		$iddokter='';
		// if ($idjenis < 20){
			// return true;
		// }
		
		if (in_array($idjenis,$arr_dokter)){
			$nama_dokter=$row->namadokter;
			$iddokter=$row->iddokter;
			// print_r('DOKTER');exit;
		}
		$idtarif=$row->idtarif;
		$where='';
		if ($idjenis=='1' || $idjenis=='20' || $idjenis=='21'){
			$where =" WHERE H.id='$idtarif'";			
		}else{
			$where ="WHERE H.idtarif='$idtarif' AND H.kelas='$idkelas'";
		}		
		
		$q="SELECT H.group_jasasarana,H.group_jasapelayanan,H.group_bhp,H.group_biayaperawatan
			,H.group_jasasarana_diskon,H.group_jasapelayanan_diskon,H.group_bhp_diskon,H.group_biayaperawatan_diskon
			FROM ".$ref." H
			 ".$where;
		$row_tarif=$this->db->query($q)->row();
		$group_diskon_all=$this->get_diskon_all_tarif_ranap($idtarif,$idjenis,$ref);
		if ($idjenis=='2'){
			 // print_r($row_tarif);exit();
		}
		$data_detail=array(
			'idvalidasi' => $idvalidasi,
			'ref_tabel' => $ref,
			'idjenis' => $idjenis,
			'iddetail' => $row->iddetail,
			'idkelas' => $idkelas,
			'idtarif' => $row->idtarif,
			'namatarif' => $row->namatarif,
			'iddokter' => $iddokter,
			'nama_dokter' => $nama_dokter,
			
			'jasasarana' => $row->jasasarana,
			'jasapelayanan' => $row->jasapelayanan,
			'bhp' => $row->bhp,
			'biayaperawatan' => $row->biayaperawatan,			
			'jasasarana_diskon' => $row->jasasarana_disc,
			'jasapelayanan_diskon' => $row->jasapelayanan_disc,
			'bhp_diskon' => $row->bhp_disc,
			'biayaperawatan_diskon' => $row->biayaperawatan_disc,
			
			'subtotal' => $row->subtotal,
			'kuantitas' => $row->kuantitas,
			'total' => $row->total,
			'diskon' => $row->diskon,
			'diskon_rp' => $row->diskon,
			'totalkeseluruhan' => $row->totalkeseluruhan,
			
			'group_jasasarana' => $row_tarif->group_jasasarana,
			'group_jasapelayanan' => $row_tarif->group_jasapelayanan,
			'group_bhp' => $row_tarif->group_bhp,
			'group_biayaperawatan' => $row_tarif->group_biayaperawatan,
			'group_jasasarana_diskon' => $row_tarif->group_jasasarana_diskon,
			'group_jasapelayanan_diskon' => $row_tarif->group_jasapelayanan_diskon,
			'group_bhp_diskon' => $row_tarif->group_bhp_diskon,
			'group_biayaperawatan_diskon' => $row_tarif->group_biayaperawatan_diskon,
			'group_diskon_all' => $group_diskon_all,
		);
		$this->db->insert('tvalidasi_pendapatan_ranap_tindakan',$data_detail);
		// print_r($data_detail);exit();
		return true;
	}
	function insert_tarif_ranap_adm($idvalidasi,$row,$dataPendaftaran){
		$idkelompok=$dataPendaftaran['idkelompok'];
		$idrekanan=$dataPendaftaran['idrekanan'];
		$group_tarif=null;
		$group_diskon=null;
		
		$q="SELECT 
			compare_value_2(MAX(IF(SA.idrekanan = '$idrekanan',SA.idgroup,NULL)),MAX(IF(SA.idrekanan = 0 AND SA.idkelompokpasien ='$idkelompok',SA.idgroup,NULL))) as group_tarif
			,compare_value_2(MAX(IF(SA.idrekanan = '$idrekanan',SA.idgroup_diskon,NULL)),MAX(IF(SA.idrekanan = 0 AND SA.idkelompokpasien ='$idkelompok',SA.idgroup_diskon,NULL))) as group_diskon
			FROM 
			msetting_jurnal_pendapatan_general_ranap_adm SA
			WHERE SA.idkelompokpasien='$idkelompok'";
		$setting=$this->db->query($q)->row();
		if ($setting){
			$group_tarif=$setting->group_tarif;
			$group_diskon=$setting->group_diskon;
		}
		$data_detail=array(
			'idvalidasi' => $idvalidasi,
			'ref_tabel' => 'mtarif_administrasi',
			'iddetail' => $row->iddetail,
			'idtarif' => $row->idtarif,
			'namatarif' => $row->namatarif,			
			'subtotal' => $row->subtotal,
			'kuantitas' => $row->kuantitas,
			'total' => $row->total,
			'diskon_rp' => $row->diskon,
			'totalkeseluruhan' => $row->totalkeseluruhan,
			'group_tarif' => $group_tarif,
			'group_diskon' => $group_diskon,
			
		);
		$this->db->insert('tvalidasi_pendapatan_ranap_adm',$data_detail);
		// print_r($row);exit();		
		// print_r($data_detail);exit();
		return true;
	}
	function insert_tarif_dokter_ko($idvalidasi,$row,$ref,$idjenis,$idkelas){
		$nama_dokter='';
		$iddokter='';
		
		$nama_dokter=$row->namadokter;
		$iddokter=$row->iddokter;
		$idtipe=$row->idtipe;			
		$idtarif=$row->idtarif;
		// $where='';
		// if ($idjenis > 1){
			// $tabel ="WHERE H.idtarif='$idtarif' AND H.kelas='$idkelas'";
		// }else{
			// $tabel =" WHERE H.id='$idtarif'";			
		// }		
		
		$q="SELECT 
			compare_value_4(MAX(IF(SB.iddokter = '$iddokter',SB.idgroup_penjualan,NULL)),MAX(IF(SB.iddokter = 0 AND SB.idkelas ='$idkelas',SB.idgroup_penjualan,NULL))
			,MAX(IF(SB.iddokter = 0 AND SB.idkelas = 0 AND SB.idtipe = '$idtipe',SB.idgroup_penjualan,NULL)),MAX(IF(SB.iddokter = 0 AND SB.idkelas = 0 AND SB.idtipe = 0,SB.idgroup_penjualan,NULL))) group_tarif
			,compare_value_4(MAX(IF(SB.iddokter = '$iddokter',SB.idgroup_diskon,NULL)),MAX(IF(SB.iddokter = 0 AND SB.idkelas ='$idkelas',SB.idgroup_diskon,NULL))
			,MAX(IF(SB.iddokter = 0 AND SB.idkelas = 0 AND SB.idtipe = '$idtipe',SB.idgroup_diskon,NULL)),MAX(IF(SB.iddokter = 0 AND SB.idkelas = 0 AND SB.idtipe = 0,SB.idgroup_diskon,NULL))) group_diskon
			FROM msetting_jurnal_pendapatan_ko_da SB
			WHERE SB.setting_id='1'";
		$row_tarif=$this->db->query($q)->row();

		$data_detail=array(
			'idvalidasi' => $idvalidasi,
			'ref_tabel' => $ref,
			'idjenis' => $idjenis,
			'iddetail' => $row->iddetail,
			'idkelas' => $idkelas,
		
			'jenis_operasi_id' => $row->jenis_operasi_id,
			'idtipe' => $row->idtipe,
			'namatarif' => $row->namatarif,
			'iddokter' => $iddokter,
			'nama_dokter' => $nama_dokter,
			'subtotal' => $row->subtotal,
			'persen' => $row->persen,
			'kuantitas' => $row->kuantitas,
			'total' => $row->total,
			// 'diskon' => $row->diskon / $row->total * 100,
			'diskon_rp' => $row->diskon,
			'totalkeseluruhan' => $row->grandtotal,
			
			'group_tarif' => $row_tarif->group_tarif,
			'group_diskon' => $row_tarif->group_diskon,
			
		);
		$this->db->insert('tvalidasi_pendapatan_ranap_dokter_ko',$data_detail);
		// print_r($data_detail);exit();
		return true;
	}
	function insert_tarif_dokter_ko_asisten($idvalidasi,$row,$ref,$idjenis,$idkelas){
		$arr_dokter=array('19');
		$nama_dokter='';
		$iddokter='';
		
		$jenis_asisten=$row->jenis_asisten;
		$nama_dokter=$row->namapegawai;
		$iddokter=$row->idpegawai;
		$idtipe=$row->idtipe;			
		$idtarif=$row->idtarif;
		// $where='';
		// if ($idjenis > 1){
			// $tabel ="WHERE H.idtarif='$idtarif' AND H.kelas='$idkelas'";
		// }else{
			// $tabel =" WHERE H.id='$idtarif'";			
		// }		
		if ($row->namatarif=='Jasa Asisten Operator'){
			$tabel="msetting_jurnal_pendapatan_ko_dao";
		}else{			
			$tabel="msetting_jurnal_pendapatan_ko_daa";
		}
		$q="SELECT 
			compare_value_5(MAX(IF(SB.iddokter = '$iddokter',SB.idgroup_penjualan,NULL)),MAX(IF(SB.iddokter = 0 AND SB.jenis_asisten ='$jenis_asisten',SB.idgroup_penjualan,NULL))
			,MAX(IF(SB.iddokter = 0 AND SB.jenis_asisten = 0 AND SB.idkelas = '$idkelas',SB.idgroup_penjualan,NULL)),MAX(IF(SB.iddokter = 0 AND SB.idkelas = 0 AND SB.jenis_asisten = 0 AND SB.idtipe='$idtipe',SB.idgroup_penjualan,NULL)),MAX(IF(SB.iddokter = 0 AND SB.idkelas = 0 AND SB.jenis_asisten = 0 AND SB.idtipe=0,SB.idgroup_penjualan,NULL))) group_tarif
			,compare_value_5(MAX(IF(SB.iddokter = '$iddokter',SB.idgroup_diskon,NULL)),MAX(IF(SB.iddokter = 0 AND SB.jenis_asisten ='$jenis_asisten',SB.idgroup_diskon,NULL))
			,MAX(IF(SB.iddokter = 0 AND SB.jenis_asisten = 0 AND SB.idkelas = '$idkelas',SB.idgroup_diskon,NULL)),MAX(IF(SB.iddokter = 0 AND SB.idkelas = 0 AND SB.jenis_asisten = 0 AND SB.idtipe='$idtipe',SB.idgroup_diskon,NULL)),MAX(IF(SB.iddokter = 0 AND SB.idkelas = 0 AND SB.jenis_asisten = 0 AND SB.idtipe=0,SB.idgroup_diskon,NULL))) group_diskon			
			FROM ".$tabel." SB
			WHERE SB.setting_id='1'";
		$row_tarif=$this->db->query($q)->row();

		$data_detail=array(
			'idvalidasi' => $idvalidasi,
			'ref_tabel' => $ref,
			'idjenis' => $idjenis,
			'iddetail' => $row->iddetail,
			'idkelas' => $idkelas,
			'jenis_operasi_id' => $row->jenis_operasi_id,
			'jenis_asisten' => $row->jenis_asisten,
			'idtipe' => $row->idtipe,
			'namatarif' => $row->namatarif,
			'iddokter' => $iddokter,
			'nama_dokter' => $nama_dokter,
			'subtotal' => $row->subtotal,
			'persen' => $row->persen,
			'kuantitas' => $row->kuantitas,
			'total' => $row->total,
			// 'diskon' => $row->diskon / $row->total * 100,
			'diskon_rp' => $row->diskon,
			'totalkeseluruhan' => $row->grandtotal,
			
			'group_tarif' => $row_tarif->group_tarif,
			'group_diskon' => $row_tarif->group_diskon,
			
		);
		$this->db->insert('tvalidasi_pendapatan_ranap_dokter_ko_asisten',$data_detail);
		// print_r($data_detail);exit();
		return true;
	}
	function get_diskon_all_tarif_ranap($idtarif,$idjenis,$ref){
		$ref=str_replace('_detail','',$ref);
		if ($idjenis=='1'){
			$q="SELECT H.group_diskon_all FROM mtarif_administrasi H WHERE H.id='$idtarif'";
		}else{
			$q="SELECT H.group_diskon_all FROM ".$ref." H WHERE H.id='$idtarif'";
		}
	
		return $this->db->query($q)->row('group_diskon_all');
	}
	function insert_obat_alkes_ranap($idpendaftaran,$idvalidasi,$idruangan,$idkelas){
		$tipe_operasi=$this->db->query("SELECT H.tipe FROM tkamaroperasi_pendaftaran H WHERE H.idpendaftaran='$idpendaftaran'")->row('tipe');
		$row_tp=$this->db->query("SELECT TP.idpoliklinik,TP.idtipe from trawatinap_pendaftaran H 
				LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpoliklinik WHERE H.id='$idpendaftaran'")->row();
		$asalrujukan=$row_tp->idtipe;
		$idpoliklinik=$row_tp->idpoliklinik;
		
		foreach($this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran,'1') as $row){//IGD FARMASI ALKES
			$idbarang=$row->idtarif;
			$this->insert_detail_obat_alkes_ranap_IGD($row,$idbarang,'1',$idvalidasi,$asalrujukan,$idpoliklinik,'1',$row->statusracikan);
		}
		foreach($this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran,'3') as $row){//IGD FARMASI OBAT
				$idbarang=$row->idtarif;
				$this->insert_detail_obat_alkes_ranap_IGD($row,$idbarang,'3',$idvalidasi,$asalrujukan,$idpoliklinik,'1',$row->statusracikan);
		}
		// print_r('Asa Rujukan '.$asalrujukan.' Poi '.$idpoliklinik);exit();
		// $this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapObat($idpendaftaran);
			foreach($this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapObat($idpendaftaran) as $row){ //RANAP FARMASI OBAT
				// print_r($row);exit();
				$idbarang=$row->idbarang;
				$this->insert_detail_obat_alkes_ranap($row,$idbarang,'3',$idvalidasi,$idruangan,$idkelas,'4','1',$row->statusracikan);
			}
			foreach($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapObat($idpendaftaran) as $row){ //RETUR RANAP FARMASI OBAT
				// print_r($row);exit();
				$idbarang=$row->idtarif;
				$this->insert_detail_obat_alkes_ranap($row,$idbarang,'3',$idvalidasi,$idruangan,$idkelas,'4','1',$row->statusracikan);
			}
			
			foreach($this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapAlkes($idpendaftaran) as $row){ //RANAP FARMASI ALKES
				// print_r($row);exit();
				$idbarang=$row->idtarif;
				$this->insert_detail_obat_alkes_ranap($row,$idbarang,'1',$idvalidasi,$idruangan,$idkelas,'5','1',$row->statusracikan);
			}
			foreach($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapAlkes($idpendaftaran) as $row){ //RETUR RANAP FARMASI ALKES
				// print_r($row);exit();
				$idbarang=$row->idtarif;
				$this->insert_detail_obat_alkes_ranap($row,$idbarang,'1',$idvalidasi,$idruangan,$idkelas,'5','1',$row->statusracikan);
			}
			
			// print_r('BERES RANAP');exit();
			foreach($this->Trawatinap_verifikasi_model->viewRincianRanapObat($idpendaftaran) as $row){ //RANAP OBAT
				$idbarang=$row->idobat;
				$this->insert_detail_obat_alkes_ranap($row,$idbarang,'3',$idvalidasi,$idruangan,$idkelas,'1');
			}
			foreach($this->Trawatinap_verifikasi_model->viewRincianRanapAlkes($idpendaftaran) as $row){  //RANAP ALKES BANTU
				$idbarang=$row->idalkes;
				$this->insert_detail_obat_alkes_ranap($row,$idbarang,'1',$idvalidasi,$idruangan,$idkelas,'2');
			}
			foreach($this->Trawatinap_verifikasi_model->viewRincianRanapAlkesBantu($idpendaftaran) as $row){ //RANAP ALKES BANTU
				$idbarang=$row->idalkes;
				$this->insert_detail_obat_alkes_ranap($row,$idbarang,'1',$idvalidasi,$idruangan,$idkelas,'3');
			}
			
			foreach($this->Trawatinap_verifikasi_model->viewRincianOperasiNarcose($idpendaftaran) as $row){//KO NARCOSE
				$idbarang=$row->idtarif;
				$this->insert_detail_obat_alkes_ranap_ko($row,$idbarang,'3',$idvalidasi,$tipe_operasi,$idkelas,'1');
			}
			foreach($this->Trawatinap_verifikasi_model->viewRincianOperasiObat($idpendaftaran) as $row){//KO OBAT
				$idbarang=$row->idtarif;
				$this->insert_detail_obat_alkes_ranap_ko($row,$idbarang,'3',$idvalidasi,$tipe_operasi,$idkelas,'2');
			}
			foreach($this->Trawatinap_verifikasi_model->viewRincianOperasiAlkes($idpendaftaran) as $row){//KO ALKES
				$idbarang=$row->idtarif;
				$this->insert_detail_obat_alkes_ranap_ko($row,$idbarang,'1',$idvalidasi,$tipe_operasi,$idkelas,'3');
			}
			foreach($this->Trawatinap_verifikasi_model->viewRincianOperasiImplan($idpendaftaran) as $row){//KO IMPLAN
				$idbarang=$row->idtarif;
				$this->insert_detail_obat_alkes_ranap_ko($row,$idbarang,'2',$idvalidasi,$tipe_operasi,$idkelas,'4');
			}
			
			foreach($this->Trawatinap_verifikasi_model->viewRincianRajalObatFilteredIGD($idpendaftaran,$asalrujukan) as $row){//IGD OBAT
				$idbarang=$row->idtarif;
				$this->insert_detail_obat_alkes_ranap_IGD($row,$idbarang,'3',$idvalidasi,$asalrujukan,$idpoliklinik);
			}
			foreach($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRajalObat($idpendaftaran) as $row){//RETUR IGD OBAT
				$idbarang=$row->idtarif;
				$this->insert_detail_obat_alkes_ranap_IGD($row,$idbarang,'3',$idvalidasi,$asalrujukan,$idpoliklinik);
			}
			
			
			
			foreach($this->Trawatinap_verifikasi_model->viewRincianRajalAlkesFilteredIGD($idpendaftaran) as $row){//IGD ALKES
				$idbarang=$row->idtarif;
				$this->insert_detail_obat_alkes_ranap_IGD($row,$idbarang,'1',$idvalidasi,$asalrujukan,$idpoliklinik);
			}
			foreach($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRajalAlkes($idpendaftaran) as $row){//RETUR IGD ALKES
				$idbarang=$row->idtarif;
				$this->insert_detail_obat_alkes_ranap_IGD($row,$idbarang,'1',$idvalidasi,$asalrujukan,$idpoliklinik);
			}
			// print_r('FINISH OBAT');exit();
			
			return true;
	}
	function insert_detail_obat_alkes_ranap_IGD($row,$idbarang,$idtipe,$idvalidasi,$asalrujukan,$idpoliklinik,$tipe_farmasi='0',$statusracikan='0'){	
		$idtarif=$idbarang;
		$tuslah=0;
		$idkategori='';$namakategori='';$idgroup_penjualan=null;$idgroup_diskon=null;$idgroup_tuslah=null;$nopenjualan='';
		if ($tipe_farmasi=='1'){
			$tuslah=$row->tuslah;
			$nopenjualan=$row->nopenjualan;
			if($statusracikan=='0'){				
				$row_tarif=$this->get_setting_farmasi_nr($idbarang,$idtipe,$asalrujukan,$idpoliklinik);
				$idgroup_tuslah=$row_tarif->idgroup_tuslah;
				$idkategori=$row_tarif->idkategori;
				$namakategori=$row_tarif->namakategori;
				$idgroup_penjualan=$row_tarif->idgroup_penjualan;
				$idgroup_diskon=$row_tarif->idgroup_diskon;
			}else{
				$idgroup_tuslah=$this->db->query("SELECT H.group_tuslah_racikan FROM msetting_jurnal_pendapatan_general H")->row('group_tuslah_racikan');
			}
		}else{
			$q="SELECT H.idtipe,H.id,H.idkategori,K.nama as namakategori,H.nama as nambarang 
				,compare_value_5(MAX(IF(SB.idbarang =H.id,SB.idgroup_penjualan,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = H.idkategori,SB.idgroup_penjualan,NULL))
							,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_penjualan,NULL))
							,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='$idpoliklinik',SB.idgroup_penjualan,NULL))
							,MAX(IF(SB.asalrujukan='$asalrujukan' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_penjualan,NULL))) idgroup_penjualan
				,compare_value_5(MAX(IF(SB.idbarang =H.id,SB.idgroup_diskon,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = H.idkategori,SB.idgroup_diskon,NULL))
							,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_diskon,NULL))
							,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='$idpoliklinik',SB.idgroup_diskon,NULL))
							,MAX(IF(SB.asalrujukan='$asalrujukan' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_diskon,NULL))) idgroup_diskon
				FROM view_barang_all H
				LEFT JOIN mdata_kategori K ON K.id=H.idkategori
				LEFT JOIN msetting_jurnal_pendapatan_jual_rajal SB ON SB.asalrujukan='$asalrujukan'
				WHERE H.id='$idtarif' AND H.idtipe='$idtipe'
				GROUP BY H.idtipe,H.id";
			$row_tarif=$this->db->query($q)->row();
			if ($row_tarif){
					$idkategori=$row_tarif->idkategori;
					$namakategori=$row_tarif->namakategori;
					$idgroup_penjualan=$row_tarif->idgroup_penjualan;
					$idgroup_diskon=$row_tarif->idgroup_diskon;
			}
		}
		
		$data_detail=array(
			'idvalidasi' => $idvalidasi,
			'iddetail' => $row->iddetail,
			'idtipe' => $idtipe,
			'idkategori' => $idkategori,
			'tipe_farmasi' => $tipe_farmasi,
			'statusracikan' => $statusracikan,
			'nopenjualan' => $nopenjualan,
			'idbarang' => $idtarif,
			'namabarang' => $row->namatarif,
			'namakategori' => $namakategori,
			'namatipe' => GetTipeKategoriBarang($idtipe),
			'hargadasar' => $row->hargadasar,
			'margin' => $row->margin,
			'hargajual' => $row->hargajual,
			'kuantitas' => $row->kuantitas,
			// 'total' => $row->total,
			// 'diskon' => $row->diskon,
			'diskon_rp' => $row->diskon,
			'tuslah' => $tuslah,
			'totalkeseluruhan' => $row->totalkeseluruhan,
			'idgroup_penjualan' => $idgroup_penjualan,
			'idgroup_diskon' => $idgroup_diskon,
			'idgroup_tuslah' => $idgroup_tuslah,
			'posisi_penjualan' => 'K',
			'posisi_diskon' => 'D',		
			'posisi_tuslah' => 'K',	
		);
		$this->db->insert('tvalidasi_pendapatan_ranap_obat_alkes_igd',$data_detail);
		if ($statusracikan=='1'){
			$idracikan=$this->db->insert_id();
			$id=$row->iddetail;
				$q="SELECT H.id as iddetail,H.idtipe,B.idkategori,H.idbarang,K.nama as kategori,B.nama as namatarif 
			,H.harga_dasar,H.margin,H.kuantitas,H.harga as hargajual,H.diskon,H.totalharga as totalkeseluruhan,H.diskon_rp
			,compare_value_5(MAX(IF(SB.idbarang =H.idbarang,SB.idgroup_penjualan,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = B.idkategori,SB.idgroup_penjualan,NULL))
									,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_penjualan,NULL))
									,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='1',SB.idgroup_penjualan,NULL))
									,MAX(IF(SB.asalrujukan='1' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_penjualan,NULL))) idgroup_penjualan
						,compare_value_5(MAX(IF(SB.idbarang =H.idbarang,SB.idgroup_diskon,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = B.idkategori,SB.idgroup_diskon,NULL))
									,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_diskon,NULL))
									,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='1',SB.idgroup_diskon,NULL))
									,MAX(IF(SB.asalrujukan='1' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_diskon,NULL))) idgroup_diskon
			FROM tpasien_penjualan_racikan_obat H
			LEFT JOIN view_barang_all B ON B.idtipe=H.idtipe AND B.id=H.idbarang
			LEFT JOIN mdata_kategori K ON K.id=B.idkategori
			LEFT JOIN msetting_jurnal_pendapatan_farmasi_r SB ON SB.asalrujukan='1'
			WHERE H.idracikan='$id'
			GROUP BY H.id";
			// print_r($q);exit();
			$row_tarif=$this->db->query($q)->result();
			foreach($row_tarif as $r){
				$data_detail=array(
					'idvalidasi' => $idvalidasi,
					// 'idhead' => $idhead,
					'idracikan' => $idracikan,
					'idtipe' => $r->idtipe,
					'iddetail' => $r->iddetail,
					'idkategori' => $r->idkategori,
					'idbarang' => $r->idbarang,
					'namabarang' => $r->namatarif,
					'namakategori' => $r->kategori,
					'namatipe' => GetTipeKategoriBarang($r->idtipe),
					'hargadasar' => $r->harga_dasar,
					'margin' => $r->margin,
					'hargajual' => $r->hargajual,
					'kuantitas' => $r->kuantitas,
					'diskon' => $r->diskon,
					'diskon_rp' => $r->diskon_rp,
					'total_jual' => $r->hargajual * $r->kuantitas,
					'total_diskon' => ($r->diskon_rp * $r->kuantitas),
					'totalkeseluruhan' => $r->totalkeseluruhan,
					'idgroup_penjualan' => $r->idgroup_penjualan,
					'idgroup_diskon' => $r->idgroup_diskon,
					'posisi_penjualan' => 'K',
					'posisi_diskon' => 'D',			
				);
				$this->db->insert('tvalidasi_pendapatan_ranap_obat_alkes_igd_racikan',$data_detail);
			}
			
		}
		
	}
	function insert_detail_obat_alkes_ranap($row,$idbarang,$idtipe,$idvalidasi,$idruangan,$idkelas,$asal,$tipe_farmasi='0',$statusracikan='0'){
		$tuslah=0;
		$idkategori='';$namakategori='';$idgroup_penjualan=null;$idgroup_diskon=null;$idgroup_tuslah=null;$nopenjualan='';
		
		if ($tipe_farmasi=='1'){
			$tuslah=$row->tuslah;
			$nopenjualan=$row->nopenjualan;
			if($statusracikan=='0'){				
				$row_tarif=$this->get_setting_farmasi_nr($idbarang,$idtipe,'3',$idkelas);
				$idgroup_tuslah=$row_tarif->idgroup_tuslah;
				$idkategori=$row_tarif->idkategori;
				$namakategori=$row_tarif->namakategori;
				$idgroup_penjualan=$row_tarif->idgroup_penjualan;
				$idgroup_diskon=$row_tarif->idgroup_diskon;
			}else{
				$idgroup_tuslah=$this->db->query("SELECT H.group_tuslah_racikan FROM msetting_jurnal_pendapatan_general H")->row('group_tuslah_racikan');
			}
		}else{
			$q="SELECT H.idtipe,H.id,H.idkategori,K.nama as namakategori,H.nama as nambarang 
				,compare_value_5(MAX(IF(SB.idbarang =H.id,SB.idgroup_penjualan,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = H.idkategori,SB.idgroup_penjualan,NULL))
							,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_penjualan,NULL))
							,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idkelas='$idkelas',SB.idgroup_penjualan,NULL))
							,MAX(IF(SB.idruangan='$idruangan' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idkelas=0 AND SB.idkategori=0,SB.idgroup_penjualan,NULL))) idgroup_penjualan
				,compare_value_5(MAX(IF(SB.idbarang =H.id,SB.idgroup_diskon,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = H.idkategori,SB.idgroup_diskon,NULL))
							,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_diskon,NULL))
							,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idkelas='$idkelas',SB.idgroup_diskon,NULL))
							,MAX(IF(SB.idruangan='$idruangan' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idkelas=0 AND SB.idkategori=0,SB.idgroup_diskon,NULL))) idgroup_diskon
				FROM view_barang_all H
				LEFT JOIN mdata_kategori K ON K.id=H.idkategori
				LEFT JOIN msetting_jurnal_pendapatan_jual_ranap SB ON SB.idruangan='$idruangan'
				WHERE H.id='$idbarang' AND H.idtipe='$idtipe'
				GROUP BY H.idtipe,H.id";		
			$row_tarif=$this->db->query($q)->row();
			if ($row_tarif){
				$idkategori=$row_tarif->idkategori;
				$namakategori=$row_tarif->namakategori;
				$idgroup_penjualan=$row_tarif->idgroup_penjualan;
				$idgroup_diskon=$row_tarif->idgroup_diskon;
			}
		}
		
			$data_detail=array(
				'idvalidasi' => $idvalidasi,
				'iddetail' => $row->iddetail,
				'asal' => $asal,
				'tipe_farmasi' => $tipe_farmasi,
				'statusracikan' => $statusracikan,
				'nopenjualan' => $nopenjualan,
				'idtipe' => $idtipe,
				'idbarang' => $idbarang,
				'namabarang' => $row->namatarif,
				'namatipe' => GetTipeKategoriBarang($idtipe),
				'hargadasar' => $row->hargadasar,
				'margin' => $row->margin,
				'hargajual' => $row->hargajual,
				'kuantitas' => $row->kuantitas,
				// 'total' => $row->total,
				'diskon' => $row->diskon,
				'diskon_rp' => $row->diskon,
				'tuslah' => $tuslah,
				'totalkeseluruhan' => $row->totalkeseluruhan,
				'idkategori' => $idkategori,
				'namakategori' => $namakategori,
				'idgroup_penjualan' => $idgroup_penjualan,
				'idgroup_diskon' => $idgroup_diskon,
				'idgroup_tuslah' => $idgroup_tuslah,
				'posisi_penjualan' => 'K',
				'posisi_diskon' => 'D',			
				'posisi_tuslah' => 'K',			
			);
			// print_r($row);exit();
			$this->db->insert('tvalidasi_pendapatan_ranap_obat_alkes',$data_detail);
			if ($statusracikan=='1'){
				$idracikan=$this->db->insert_id();
				$id=$row->iddetail;
					$q="SELECT H.id as iddetail,H.idtipe,B.idkategori,H.idbarang,K.nama as kategori,B.nama as namatarif 
				,H.harga_dasar,H.margin,H.kuantitas,H.harga as hargajual,H.diskon,H.totalharga as totalkeseluruhan,H.diskon_rp
				,compare_value_5(MAX(IF(SB.idbarang =H.idbarang,SB.idgroup_penjualan,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = B.idkategori,SB.idgroup_penjualan,NULL))
										,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_penjualan,NULL))
										,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='1',SB.idgroup_penjualan,NULL))
										,MAX(IF(SB.asalrujukan='1' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_penjualan,NULL))) idgroup_penjualan
							,compare_value_5(MAX(IF(SB.idbarang =H.idbarang,SB.idgroup_diskon,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = B.idkategori,SB.idgroup_diskon,NULL))
										,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_diskon,NULL))
										,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='1',SB.idgroup_diskon,NULL))
										,MAX(IF(SB.asalrujukan='1' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_diskon,NULL))) idgroup_diskon
				FROM tpasien_penjualan_racikan_obat H
				LEFT JOIN view_barang_all B ON B.idtipe=H.idtipe AND B.id=H.idbarang
				LEFT JOIN mdata_kategori K ON K.id=B.idkategori
				LEFT JOIN msetting_jurnal_pendapatan_farmasi_r SB ON SB.asalrujukan='1'
				WHERE H.idracikan='$id'
				GROUP BY H.id";
				// print_r($q);exit();
				$row_tarif=$this->db->query($q)->result();
				foreach($row_tarif as $r){
					$data_detail=array(
						'idvalidasi' => $idvalidasi,
						// 'idhead' => $idhead,
						'idracikan' => $idracikan,
						'idtipe' => $r->idtipe,
						'iddetail' => $r->iddetail,
						'idkategori' => $r->idkategori,
						'idbarang' => $r->idbarang,
						'namabarang' => $r->namatarif,
						'namakategori' => $r->kategori,
						'namatipe' => GetTipeKategoriBarang($r->idtipe),
						'hargadasar' => $r->harga_dasar,
						'margin' => $r->margin,
						'hargajual' => $r->hargajual,
						'kuantitas' => $r->kuantitas,
						'diskon' => $r->diskon,
						'diskon_rp' => $r->diskon_rp,
						'total_jual' => $r->hargajual * $r->kuantitas,
						'total_diskon' => ($r->diskon_rp * $r->kuantitas),
						'totalkeseluruhan' => $r->totalkeseluruhan,
						'idgroup_penjualan' => $r->idgroup_penjualan,
						'idgroup_diskon' => $r->idgroup_diskon,
						'posisi_penjualan' => 'K',
						'posisi_diskon' => 'D',			
					);
					$this->db->insert('tvalidasi_pendapatan_ranap_obat_alkes_racikan',$data_detail);
				}
				
			}
	}
	function insert_detail_obat_alkes_ranap_ko($row,$idbarang,$tipe_barang,$idvalidasi,$idtipe,$idkelas,$jenis){		
		$q="SELECT H.idtipe,H.id,H.idkategori,K.nama as kategori,H.nama as nambarang 
			,compare_value_5(
							MAX(IF(SB.idbarang =H.id,SB.idgroup_penjualan,NULL))
						,MAX(IF(SB.idbarang=0 AND SB.idkategori = H.idkategori,SB.idgroup_penjualan,NULL))						
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idkelas='$idkelas',SB.idgroup_penjualan,NULL))
						,MAX(IF(SB.idtipe='$idtipe' AND SB.idbarang=0  AND SB.idkelas=0 AND SB.idkategori=0,SB.idgroup_penjualan,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idkelas = 0 AND SB.idtipe=0,SB.idgroup_penjualan,NULL))
						) idgroup_penjualan
			,compare_value_5(
							MAX(IF(SB.idbarang =H.id,SB.idgroup_diskon,NULL))
						,MAX(IF(SB.idbarang=0 AND SB.idkategori = H.idkategori,SB.idgroup_diskon,NULL))						
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idkelas='$idkelas',SB.idgroup_diskon,NULL))
						,MAX(IF(SB.idtipe='$idtipe' AND SB.idbarang=0  AND SB.idkelas=0 AND SB.idkategori=0,SB.idgroup_diskon,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idkelas = 0 AND SB.idtipe=0,SB.idgroup_diskon,NULL))
						) idgroup_diskon
			FROM view_barang_all H
			LEFT JOIN mdata_kategori K ON K.id=H.idkategori
			LEFT JOIN msetting_jurnal_pendapatan_jual_ko SB ON SB.jenis='$jenis'
			WHERE H.id='$idbarang' AND H.idtipe='$tipe_barang'
			GROUP BY H.idtipe,H.id";
		$row_tarif=$this->db->query($q)->row();
		if ($row_tarif){
			$data_detail=array(
				'idvalidasi' => $idvalidasi,
				'iddetail' => $row->iddetail,
				'jenis' => $jenis,
				'idtipe' => $tipe_barang,
				'idkategori' => $row_tarif->idkategori,
				'idbarang' => $idbarang,
				'namabarang' => $row->namatarif,
				'namakategori' => $row_tarif->kategori,
				'namatipe' => GetTipeKategoriBarang($tipe_barang),
				'hargadasar' => $row->hargadasar,
				'margin' => $row->margin,
				'hargajual' => $row->hargajual,
				'kuantitas' => $row->kuantitas,
				// 'total' => $row->total,
				// 'diskon' => $row->diskon,
				'diskon_rp' => $row->diskon,
				'totalkeseluruhan' => $row->totalkeseluruhan,
				'idgroup_penjualan' => $row_tarif->idgroup_penjualan,
				'idgroup_diskon' => $row_tarif->idgroup_diskon,
				'posisi_penjualan' => 'K',
				'posisi_diskon' => 'D',			
			);
			$this->db->insert('tvalidasi_pendapatan_ranap_obat_alkes_ko',$data_detail);
		}
	}
	
	function get_setting_farmasi_nr($idtarif,$idtipe,$asalrujukan,$idpoliklinik){
		// $idtarif=$row->idtarif;
		$q="SELECT H.idtipe,H.id,H.idkategori,K.nama as namakategori,H.nama as nambarang 
			,compare_value_5(MAX(IF(SB.idbarang =H.id,SB.idgroup_penjualan,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = H.idkategori,SB.idgroup_penjualan,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_penjualan,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='$idpoliklinik',SB.idgroup_penjualan,NULL))
						,MAX(IF(SB.asalrujukan='$asalrujukan' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_penjualan,NULL))) idgroup_penjualan
			,compare_value_5(MAX(IF(SB.idbarang =H.id,SB.idgroup_diskon,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = H.idkategori,SB.idgroup_diskon,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_diskon,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='$idpoliklinik',SB.idgroup_diskon,NULL))
						,MAX(IF(SB.asalrujukan='$asalrujukan' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_diskon,NULL))) idgroup_diskon
			,compare_value_5(MAX(IF(SB.idbarang =H.id,SB.idgroup_tuslah,NULL)),MAX(IF(SB.idbarang=0 AND SB.idkategori = H.idkategori,SB.idgroup_tuslah,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=H.idtipe,SB.idgroup_tuslah,NULL))
						,MAX(IF(SB.idbarang = 0 AND SB.idkategori = 0 AND SB.idtipe=0 AND SB.idpoli_kelas='$idpoliklinik',SB.idgroup_tuslah,NULL))
						,MAX(IF(SB.asalrujukan='$asalrujukan' AND SB.idbarang=0 AND SB.idtipe=0 AND SB.idpoli_kelas=0 AND SB.idkategori=0 AND SB.idpoli_kelas=0,SB.idgroup_tuslah,NULL))) idgroup_tuslah
			FROM view_barang_all H
			LEFT JOIN mdata_kategori K ON K.id=H.idkategori
			LEFT JOIN msetting_jurnal_pendapatan_farmasi_nr SB ON SB.asalrujukan='$asalrujukan'
			WHERE H.id='$idtarif' AND H.idtipe='$idtipe'
			GROUP BY H.idtipe,H.id";
		$row_tarif=$this->db->query($q)->row();
		return $row_tarif;
	}
	function insert_deposit($idpendaftaran,$idvalidasi='1'){
		$q="SELECT H.total as total_tagihan,H.pembayaran,H.deposit,DATE(H.tanggal) as tanggal_bayar FROM `trawatinap_tindakan_pembayaran` H WHERE H.idtindakan='$idpendaftaran' AND H.statusbatal='0' ";
		$row_kasir=$this->db->query($q)->row();
		$tanggal_bayar=$row_kasir->tanggal_bayar;
		$deposit=$row_kasir->deposit;
		// $deposit=7000000;
		$pembayaran=$row_kasir->pembayaran;
		$total_tagihan=$row_kasir->total_tagihan;
		if (($total_tagihan-$pembayaran)< $deposit){
			$nominal_deposit=$total_tagihan - $pembayaran;
		}else{
			$nominal_deposit=$deposit;
		}
		$total_refund=$row_kasir->deposit-$nominal_deposit;
		$sisa_deposit=$nominal_deposit;
		$st_insert_all=0;
		if ($total_refund > 0){
			$q="SELECT H.id as iddetail,H.norefund as notransaksi,DATE(H.tanggal) as tanggal,H.metode as idmetode,H.totalrefund as nominal,S.idakun,'K' as posisi_akun  
											FROM trefund H
											LEFT JOIN msetting_jurnal_refund_deposit S ON S.idmetode=H.metode
											WHERE H.idtransaksi='$idpendaftaran' AND H.tipe='0' AND H.metode='1' AND DATE(H.tanggal)='$tanggal_bayar' AND H.st_verifikasi='1'";
		// print_r($q);exit();
			$row_refund=$this->db->query($q)->result();
			if ($row_refund){
				$st_insert_all=1;
				foreach($row_refund as $row){
					$data=array(
						'idvalidasi' =>$idvalidasi,
						'tipe' =>2,
						'tanggal' =>YMDFormat($row->tanggal),
						'iddetail' =>$row->iddetail,
						'notransaksi' =>$row->notransaksi,
						'idmetode' =>$row->idmetode,
						'nominal' =>$row->nominal,
						'idakun' =>$row->idakun,
						'posisi_akun' =>$row->posisi_akun,
						'nominal_refund' =>0,
					);
					$this->db->insert('tvalidasi_pendapatan_ranap_bayar_deposit',$data);
				}
				$this->db->query("DELETE FROM tvalidasi_pendapatan_ranap WHERE tipe_trx='5' AND pendaftaran_id='$idpendaftaran'");
			}
		}
		$q="SELECT H.id as iddetail,H.nodeposit as notransaksi,H.tanggal,H.idmetodepembayaran as idmetode,H.idbank,H.nominal,S.idakun,'D' as posisi_akun 
				FROM trawatinap_deposit H 
				LEFT JOIN msetting_jurnal_deposit_pendapatan S ON S.idtipe=H.idmetodepembayaran
				WHERE H.idrawatinap='$idpendaftaran' AND H.`status`='1' ORDER BY H.id ASC";
		$list_deposit=$this->db->query($q)->result();
		
		foreach($list_deposit as $row){
			if ($st_insert_all==0){
				if ($sisa_deposit > $row->nominal){
					$nominal=$row->nominal;
					$nominal_refund=0;
				}else{
					$nominal=$sisa_deposit;
					$nominal_refund=$row->nominal - $sisa_deposit;
					
				}
			}else{
				$nominal=$row->nominal;
				$nominal_refund=0;
			}
			
			$sisa_deposit=$sisa_deposit-$nominal;
			$data=array(
				'idvalidasi' =>$idvalidasi,
				'tipe' =>1,
				'tanggal' =>YMDFormat($row->tanggal),
				'iddetail' =>$row->iddetail,
				'notransaksi' =>$row->notransaksi,
				'idmetode' =>$row->idmetode,
				'nominal' =>$nominal,
				'idakun' =>$row->idakun,
				'posisi_akun' =>$row->posisi_akun,
				'nominal_refund' =>$nominal_refund,
			);
			$this->db->insert('tvalidasi_pendapatan_ranap_bayar_deposit',$data);
		}
		
	}
	
	
}

//RAWAT INAP


/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
