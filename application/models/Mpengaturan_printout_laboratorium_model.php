<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpengaturan_printout_laboratorium_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('merm_pengaturan_printout_laboratorium');
        return $query->row();
    }

    public function saveData()
    {
        $this->label_header = $_POST['label_header'];
        $this->label_header_eng = $_POST['label_header_eng'];
        $this->label_subheader = $_POST['label_subheader'];
        $this->label_subheader_eng = $_POST['label_subheader_eng'];
        $this->label_footer = $_POST['label_footer'];
        $this->label_footer_eng = $_POST['label_footer_eng'];
        $this->tampilkan_tanggal_jam_cetak = $_POST['tampilkan_tanggal_jam_cetak'];
        $this->tampilkan_tanda_tangan = $_POST['tampilkan_tanda_tangan'];

        if ($this->upload(true)) {
            $this->db->where('id', 1);
            if ($this->db->update('merm_pengaturan_printout_laboratorium', $this)) {
                return true;
            }
        }

        return false;
    }

    public function upload($update = false)
    {
        if (!file_exists('assets/upload/pengaturan_printout_laboratorium')) {
            mkdir('assets/upload/pengaturan_printout_laboratorium', 0755, true);
        }

        if (isset($_FILES['logo'])) {
            if ($_FILES['logo']['name'] != '') {
                $config['upload_path'] = './assets/upload/pengaturan_printout_laboratorium/';
                $config['allowed_types'] = 'jpg|jpeg|bmp|png';
                $config['encrypt_name']  = true;

                $this->upload->initialize($config);

                if ($this->upload->do_upload('logo')) {
                    $image_upload = $this->upload->data();
                    $this->logo = $image_upload['file_name'];

                    if ($update == true) {
                        $this->removeImage($_POST['id']);
                    }

                    return true;
                } else {
                    $this->error_message = $this->upload->display_errors();
                    print_r($this->error_message);exit();
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public function removeImage($id)
    {
        $row = $this->getSpecified($id);
        if (file_exists('./assets/upload/pengaturan_printout_laboratorium/'.$row->logo) && $row->logo !='') {
            unlink('./assets/upload/pengaturan_printout_laboratorium/'.$row->logo);
        }
    }
}
