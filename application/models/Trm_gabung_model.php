<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_gabung_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function saveData() {
        $this->idpasien_lama  = $_POST['idpasien'];
        $this->idpasien_baru  = $_POST['idpasien_new'];
        $this->alasan         = $_POST['alasan'];
        $this->created_at     = date("Y-m-d H:i:s");
        $this->created_by     = $this->session->userdata('user_id');

        if ($this->db->insert('trm_gabung', $this)) {
            $data = array();
            $data['inf_gabungmedrec'] = '{"tanggal":"'.$this->created_at.'","asal_pasien_id":"'.$_POST['idpasien'].'","ke_pasien_id":"'.$_POST['idpasien_new'].'"}';
            $this->db->where('id', $_POST['idpasien']);
            $this->db->update('mfpasien', $data);
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
