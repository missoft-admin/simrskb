<?php

class Trujukan_fisioterapi_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getSpecified($id)
	{
		$this->db->select('trujukan_fisioterapi.tanggal, trujukan_fisioterapi.iddokterperujuk, trujukan_fisioterapi.idpegawai, trujukan_fisioterapi.tanggal AS tanggalrujukan, trujukan_fisioterapi.norujukan, mfpasien.no_medrec AS nomedrec,
        mfpasien.nama AS namapasien, mfpasien.jenis_kelamin AS jeniskelamin, mfpasien.alamat_jalan AS alamat, mfpasien.tanggal_lahir AS tanggallahir,
        mfpasien.title, mfpasien.umur_tahun, mfpasien.umur_bulan, mfpasien.umur_hari, tpoliklinik_pendaftaran.id AS idpendaftaran, tpoliklinik_pendaftaran.tanggaldaftar, tpoliklinik_pendaftaran.nopendaftaran,
        provinsi.nama AS prov, kota.nama AS kota, kecamatan.nama AS kec, kelurahan.nama AS kel, trujukan_fisioterapi.asalrujukan,
        tpoliklinik_pendaftaran.idkelompokpasien, mpasien_kelompok.nama AS namakelompok, COALESCE(mrekanan.id,0) AS idrekanan, mrekanan.nama AS namarekanan, mdokter_perujuk.nama AS namadokterperujuk, COALESCE(trawatinap_pendaftaran.idkelas, 0) AS idkelas');
		$this->db->join('trawatinap_pendaftaran', 'trujukan_fisioterapi.idtindakan = trawatinap_pendaftaran.id AND trujukan_fisioterapi.asalrujukan = 3', 'LEFT');
		$this->db->join('tpoliklinik_tindakan', 'tpoliklinik_tindakan.id = trujukan_fisioterapi.idtindakan AND trujukan_fisioterapi.asalrujukan IN (1, 2)', 'LEFT');
		$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran OR tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT');
		$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien');
		$this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
		$this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien');
		$this->db->join('mdokter mdokter_perujuk', 'mdokter_perujuk.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');

		$this->db->join('mfwilayah provinsi', 'provinsi.id = mfpasien.provinsi_id', 'LEFT');
		$this->db->join('mfwilayah kota', 'kota.id = mfpasien.kabupaten_id', 'LEFT');
		$this->db->join('mfwilayah kecamatan', 'kecamatan.id = mfpasien.kecamatan_id', 'LEFT');
		$this->db->join('mfwilayah kelurahan', 'kelurahan.id = mfpasien.kelurahan_id', 'LEFT');
		$this->db->where('trujukan_fisioterapi.id', $id);
		$query = $this->db->get('trujukan_fisioterapi');
		return $query->row();
	}

	public function getListTarifFisioterapi()
	{
		$this->db->where('status', 1);
		$this->db->order_by('path', 'ASC');
		$query = $this->db->get('mtarif_fisioterapi');
		return $query->result();
	}

	public function getListDokterPerujuk()
	{
		// $this->db->where("idkategori", 1);
		$this->db->where('status', 1);
		$query = $this->db->get('mdokter');
		return $query->result();
	}

	public function getLisPegawaiFisioterapi()
	{
		$this->db->where('mpegawai.idkategori', '3');
		$this->db->where('mpegawai.status', '1');
		$query = $this->db->get('mpegawai');
		return $query->result();
	}

	public function getListDetail($idrujukan)
	{
		$this->db->select('trujukan_fisioterapi_detail.*, mtarif_fisioterapi.level, mtarif_fisioterapi.nama');
		$this->db->join('mtarif_fisioterapi', 'mtarif_fisioterapi.id = trujukan_fisioterapi_detail.idfisioterapi');
		$this->db->where('trujukan_fisioterapi_detail.idrujukan', $idrujukan);
		$query = $this->db->get('trujukan_fisioterapi_detail');
		return $query->result();
	}

	public function getAllParent($idkelompokpasien, $idrekanan, $headerpath = 0, $level = 999)
	{
		// Get ID Root
		$this->db->select('MAX(CAST(headerpath AS SIGNED)) + 1 AS idroot');
		$this->db->where('status', '1');
		$query = $this->db->get('mtarif_fisioterapi');
		$row = $query->row();

		if ($headerpath != 0) {
			$idroot = $headerpath;
		} else {
			$idroot = ($row->idroot ? $row->idroot : 1);
		}

		$this->db->select('mtarif_fisioterapi.path, mtarif_fisioterapi.level, mtarif_fisioterapi.nama');

		// Get Option
		if ($idkelompokpasien == 1) {
			$this->db->select('mtarif_fisioterapi.path');
			$this->db->join('mrekanan', 'mrekanan.tfisioterapi = mtarif_fisioterapi.id');
			$this->db->where('mrekanan.id', $idrekanan);
			$query = $this->db->get('mtarif_fisioterapi');

			if ($query->num_rows() > 0) {
				$this->db->join('mrekanan', 'mrekanan.tfisioterapi = mtarif_fisioterapi.id');
				$this->db->where('mrekanan.id', $idrekanan);
			} else {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tfisioterapi = mtarif_fisioterapi.id');
				$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
			}
		} else {
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tfisioterapi = mtarif_fisioterapi.id');
			$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
		}

		$this->db->where('mtarif_fisioterapi.idkelompok', '1');
		$this->db->where('mtarif_fisioterapi.status', '1');
		$this->db->order_by('mtarif_fisioterapi.path', 'ASC');
		$query = $this->db->get('mtarif_fisioterapi');
		$result = $query->result();

		$selected = ($level == 0 ? 'selected' : '');
		$data = '';
		if ($query->num_rows() > 0) {
			foreach ($result as $row) {
				if ($headerpath != 0 && $level != 0) {
					$selected = ($headerpath == $row->path ? 'selected' : '');
				} else {
					$selected = '';
				}
				$data .= '<option value="' . $row->path . '" ' . $selected . '>' . TreeView($row->level, $row->nama) . '</option>';
			}
		}
		return $data;
	}

	public function save()
	{
		$this->idpegawai = $_POST['idpegawai'];
		$this->iddokterperujuk = $_POST['iddokterperujuk'];
		$this->status = 3;

		$this->tanggal = YMDFormat($_POST['tanggalrujukan']) . ' ' . $_POST['wakturujukan'];
		$this->iduser_tindakan = $this->session->userdata('user_id');
		$this->tanggal_tindakan = date('Y-m-d H:i:s');

		if ($this->db->update('trujukan_fisioterapi', $this, ['id' => $_POST['id']])) {
			$this->db->where('idrujukan', $_POST['id']);
			if ($this->db->delete('trujukan_fisioterapi_detail')) {
				$query = $this->db->query("SELECT
                	mdokter.id,
                	(CASE
                		WHEN DATE_FORMAT(trujukan_fisioterapi.tanggal_tindakan, '%H:%i') < '12:00' THEN
                			mdokter.potonganrspagi
                		ELSE mdokter.potonganrssiang
                	END) AS potongan_rs,
                	mdokter.pajak AS pajak_dokter
                FROM trujukan_fisioterapi
                JOIN mdokter ON mdokter.id = trujukan_fisioterapi.iddokterperujuk
                WHERE trujukan_fisioterapi.id = " . $_POST['id']);
				$dokter = $query->row();

				$tindakan_list = json_decode($_POST['tindakan-value']);
				foreach ($tindakan_list as $row) {
					$tindakan = [];
					$tindakan['idrujukan'] = $_POST['id'];
					$tindakan['idfisioterapi'] = $row[10];
					$tindakan['jasasarana'] = RemoveComma($row[1]);
					$tindakan['jasapelayanan'] = RemoveComma($row[2]);
					$tindakan['bhp'] = RemoveComma($row[3]);
					$tindakan['biayaperawatan'] = RemoveComma($row[4]);
					$tindakan['total'] = RemoveComma($row[5]);
					$tindakan['kuantitas'] = RemoveComma($row[6]);
					$tindakan['diskon'] = RemoveComma($row[8]);
					$tindakan['totalkeseluruhan'] = RemoveComma($row[9]);
					$tindakan['statusverifikasi'] = $row[11];
					$tindakan['status'] = 1;

					$tindakan['iddokter'] = $_POST['iddokterperujuk'];
					$tindakan['potongan_rs'] = $dokter->potongan_rs;
					$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

					$this->db->insert('trujukan_fisioterapi_detail', $tindakan);
				}
			}
		}

		$this->db->update(
			'tpoliklinik_pendaftaran',
			['statustindakan' => '1'],
			['id' => $_POST['idpendaftaran']]
		);

		return true;
	}

	public function cancelTrx($id)
	{
		$this->status = 0;

		if ($this->db->update('trujukan_fisioterapi', $this, ['id' => $id])) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}
}
