<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
class Tpendaftaran_ranap_erm_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function cetak_invasif($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$data=$this->setting_invasif_label();
			// print_r($data);exit;
		$q="
			SELECT 
			
			H.*,mhari.nama_hari
			FROM `tranap_invasif` H
			LEFT JOIN date_row ON date_row.tanggal=DATE(tanggal_input)
			LEFT JOIN mhari ON mhari.id=date_row.hari
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header as judul_header_ina,judul_header_eng,judul_footer as judul_footer_ina,footer_eng as judul_footer_eng
			FROM setting_invasif
			";
		$data_detail=$this->db->query($q)->row_array();
		if ($data_detail){
			
		}else{
			$data_detail=array();
		}
		$pendaftaran_id=($data_header['tipe_rj_ri']=='3'?$data_header['pendaftaran_id_ranap']:$data_header['pendaftaran_id']);
		$st_ranap=($data_header['tipe_rj_ri']=='3'?1:0);
		$data_pasien=$this->get_data_pasien($pendaftaran_id,$st_ranap);
		$data=array_merge($data,$data_header,$data_detail,$data_pasien);
		// print_r($data);exit;
        $data['title']=$data_detail['judul_header_ina'].' '.$data['no_medrec'];
        $nama_file=$data_detail['judul_header_ina'] .' '.$data_pasien['no_medrec'];
		$data = array_merge($data, backend_info());
        $html = $this->load->view('Tpendaftaran_ranap_erm/pdf_invasif', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);exit;
		
        // (Optional) Setup the paper size and orientation
        // $dompdf->setPaper(array(0,0,609.4488,935.433), 'portrait');
		$dompdf->setPaper('A4', 'portrait');
        // Render the HTML as PDF
        $dompdf->render();
		if ($st_create=='1'){
			// $nama_file=$data['judul_per_ina'].' '.$data_transaksi->nopermintaan.'.pdf';
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
		}
        // // Output the generated PDF to Browser
	}
	function cetak_info_ods($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT *FROM setting_assesmen_pulang_label";
		$data=$this->setting_info_ods_label();
			// print_r($data);exit;
		$q="
			SELECT 
			
			H.*,mhari.nama_hari
			FROM `tranap_info_ods` H
			LEFT JOIN date_row ON date_row.tanggal=H.tanggal_opr
			LEFT JOIN mhari ON mhari.id=date_row.hari
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header as judul_header_ina,judul_header_eng,footer_ina as judul_footer_ina,footer_eng as judul_footer_eng
			FROM setting_info_ods
			";
		$data_detail=$this->db->query($q)->row_array();
		if ($data_detail){
			
		}else{
			$data_detail=array();
		}
		$data_pasien=$this->get_data_pasien($data_header['pendaftaran_id'],$data_header['st_ranap']);
		$data=array_merge($data,$data_header,$data_detail,$data_pasien);
		// print_r($data);exit;
        $data['title']='FORMULIR PEMEBERIAN INFORMASI ONE DAY SURGEY' .' '.$data['no_medrec'];
        $nama_file=$data['judul_header_ina'] .' '.$data['no_medrec'];
		$data = array_merge($data, backend_info());
        $html = $this->load->view('Tpendaftaran_ranap_erm/pdf_info_ods', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);exit;
		
        // (Optional) Setup the paper size and orientation
        // $dompdf->setPaper(array(0,0,609.4488,935.433), 'portrait');
		$dompdf->setPaper('A4', 'portrait');
        // Render the HTML as PDF
        $dompdf->render();
		if ($st_create=='1'){
			// $nama_file=$data['judul_per_ina'].' '.$data_transaksi->nopermintaan.'.pdf';
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
		}
        // // Output the generated PDF to Browser
	}
	function setting_tindakan_anestesi_label(){
		$q="SELECT *
			FROM `setting_tindakan_anestesi_keterangan` LIMIT 1

			";
		return $this->db->query($q)->row_array();
	}
	function setting_tindakan_risiko_label(){
		$q="SELECT *
			FROM `setting_risiko_tinggi_keterangan` LIMIT 1

			";
		return $this->db->query($q)->row_array();
	}
	function setting_tindakan_darah_label(){
		$q="SELECT *
			FROM `setting_tf_darah_keterangan` LIMIT 1

			";
		return $this->db->query($q)->row_array();
	}
	
	function setting_tindakan_ic_label(){
		$q="SELECT *
			FROM `setting_ic_keterangan` LIMIT 1

			";
		return $this->db->query($q)->row_array();
	}
	function setting_tindakan_icu_label(){
		$q="SELECT *
			FROM `setting_icu_keterangan` LIMIT 1

			";
		return $this->db->query($q)->row_array();
	}
	function setting_tindakan_isolasi_label(){
		$q="SELECT *
			FROM `setting_isolasi_keterangan` LIMIT 1

			";
		return $this->db->query($q)->row_array();
	}
	function setting_tindakan_dnr_label(){
		$q="SELECT *
			FROM `setting_dnr_keterangan` LIMIT 1

			";
		return $this->db->query($q)->row_array();
	}
	function cetak_tindakan_anestesi($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$data=$this->setting_tindakan_anestesi_label();
		$q="
			SELECT 
			
			H.*,mhari.nama_hari
			FROM `tranap_tindakan_anestesi` H
			LEFT JOIN date_row ON date_row.tanggal=DATE(H.tanggal_input)
			LEFT JOIN mhari ON mhari.id=date_row.hari
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_header=$this->db->query($q)->row_array();
			// print_r($data_header);exit;
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header_logo=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header as judul_header_ina,judul_header_eng,judul_footer as judul_footer_ina,footer_eng as judul_footer_eng
			FROM setting_tindakan_anestesi
			";
		$data_detail=$this->db->query($q)->row_array();
		if ($data_detail){
			
		}else{
			$data_detail=array();
		}
		if ($data_header['tipe_rj_ri']=='3'){//RANAP
			$data_pasien=$this->get_data_pasien($data_header['pendaftaran_id_ranap'],1);
		}else{
			$data_pasien=$this->get_data_pasien($data_header['pendaftaran_id'],0);
		}
		$data_header_logo['isi_surat']=1;
		$data=array_merge($data,$data_header_logo,$data_header,$data_detail,$data_pasien);
        $data['title']=$data_detail['judul_header_ina'].' '.$data['no_medrec'];
        $nama_file=$data['judul_header_ina'] .' '.$data['no_medrec'];
		$data = array_merge($data, backend_info());
        $html = $this->load->view('Tpendaftaran_ranap_erm/pdf_tindakan_anestesi', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);exit;
		
        // (Optional) Setup the paper size and orientation
        // $dompdf->setPaper(array(0,0,609.4488,935.433), 'portrait');
		$dompdf->setPaper('A4', 'portrait');
        // Render the HTML as PDF
        $dompdf->render();
		if ($st_create=='1'){
			// $nama_file=$data['judul_per_ina'].' '.$data_transaksi->nopermintaan.'.pdf';
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
		}
        // // Output the generated PDF to Browser
	}
	function cetak_tindakan_risiko($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$data=$this->setting_tindakan_risiko_label();
		$q="
			SELECT 
			
			H.*,mhari.nama_hari
			FROM `tranap_risiko_tinggi` H
			LEFT JOIN date_row ON date_row.tanggal=DATE(H.tanggal_input)
			LEFT JOIN mhari ON mhari.id=date_row.hari
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_header=$this->db->query($q)->row_array();
			// print_r($data_header);exit;
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header_logo=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header as judul_header_ina,judul_header_eng,judul_footer as judul_footer_ina,footer_eng as judul_footer_eng
			FROM setting_risiko_tinggi
			";
		$data_detail=$this->db->query($q)->row_array();
		if ($data_detail){
			
		}else{
			$data_detail=array();
		}
		if ($data_header['tipe_rj_ri']=='3'){//RANAP
			$data_pasien=$this->get_data_pasien($data_header['pendaftaran_id_ranap'],1);
		}else{
			$data_pasien=$this->get_data_pasien($data_header['pendaftaran_id'],0);
		}
		$data_header_logo['isi_surat']=1;
		$data=array_merge($data,$data_header_logo,$data_header,$data_detail,$data_pasien);
        $data['title']=$data_detail['judul_header_ina'].' '.$data['no_medrec'];
        $nama_file=$data['judul_header_ina'] .' '.$data['no_medrec'];
		$data = array_merge($data, backend_info());
        $html = $this->load->view('Tpendaftaran_ranap_erm/pdf_tindakan_risiko', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);exit;
		
        // (Optional) Setup the paper size and orientation
        // $dompdf->setPaper(array(0,0,609.4488,935.433), 'portrait');
		$dompdf->setPaper('A4', 'portrait');
        // Render the HTML as PDF
        $dompdf->render();
		if ($st_create=='1'){
			// $nama_file=$data['judul_per_ina'].' '.$data_transaksi->nopermintaan.'.pdf';
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
		}
        // // Output the generated PDF to Browser
	}
	function cetak_tindakan_darah($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$data=$this->setting_tindakan_darah_label();
		$q="
			SELECT 
			
			H.*,mhari.nama_hari
			FROM `tranap_tf_darah` H
			LEFT JOIN date_row ON date_row.tanggal=DATE(H.tanggal_input)
			LEFT JOIN mhari ON mhari.id=date_row.hari
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_header=$this->db->query($q)->row_array();
			// print_r($data_header);exit;
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header_logo=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header as judul_header_ina,judul_header_eng,judul_footer as judul_footer_ina,footer_eng as judul_footer_eng
			FROM setting_tf_darah
			";
		$data_detail=$this->db->query($q)->row_array();
		if ($data_detail){
			
		}else{
			$data_detail=array();
		}
		if ($data_header['tipe_rj_ri']=='3'){//RANAP
			$data_pasien=$this->get_data_pasien($data_header['pendaftaran_id_ranap'],1);
		}else{
			$data_pasien=$this->get_data_pasien($data_header['pendaftaran_id'],0);
		}
		$data_header_logo['isi_surat']=1;
		$data=array_merge($data,$data_header_logo,$data_header,$data_detail,$data_pasien);
        $data['title']=$data_detail['judul_header_ina'].' '.$data['no_medrec'];
        $nama_file=$data['judul_header_ina'] .' '.$data['no_medrec'];
		$data = array_merge($data, backend_info());
        $html = $this->load->view('Tpendaftaran_ranap_erm/pdf_tindakan_darah', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);exit;
		
        // (Optional) Setup the paper size and orientation
        // $dompdf->setPaper(array(0,0,609.4488,935.433), 'portrait');
		$dompdf->setPaper('A4', 'portrait');
        // Render the HTML as PDF
        $dompdf->render();
		if ($st_create=='1'){
			// $nama_file=$data['judul_per_ina'].' '.$data_transaksi->nopermintaan.'.pdf';
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
		}
        // // Output the generated PDF to Browser
	}
	function cetak_tindakan_ic($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$data=$this->setting_tindakan_ic_label();
		$q="
			SELECT 
			
			H.*,mhari.nama_hari
			FROM `tpoliklinik_ic` H
			LEFT JOIN date_row ON date_row.tanggal=DATE(H.tanggal_input)
			LEFT JOIN mhari ON mhari.id=date_row.hari
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_header=$this->db->query($q)->row_array();
			// print_r($data_header);exit;
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header_logo=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header as judul_header_ina,judul_header_eng,judul_footer as judul_footer_ina,footer_eng as judul_footer_eng
			FROM setting_ic
			";
		$data_detail=$this->db->query($q)->row_array();
		if ($data_detail){
			
		}else{
			$data_detail=array();
		}
		if ($data_header['st_ranap']=='1'){//RANAP
			$data_pasien=$this->get_data_pasien($data_header['pendaftaran_id'],1);
		}else{
			$data_pasien=$this->get_data_pasien($data_header['pendaftaran_id'],0);
		}
		$data_header_logo['isi_surat']=1;
		$data=array_merge($data,$data_header_logo,$data_header,$data_detail,$data_pasien);
        $data['title']=$data_detail['judul_header_ina'].' '.$data['no_medrec'];
        $nama_file=$data['judul_header_ina'] .' '.$data['no_medrec'];
		$data = array_merge($data, backend_info());
		// print_r($data);exit;
        $html = $this->load->view('Tpendaftaran_ranap_erm/pdf_tindakan_ic', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);exit;
		
        // (Optional) Setup the paper size and orientation
        // $dompdf->setPaper(array(0,0,609.4488,935.433), 'portrait');
		$dompdf->setPaper('A4', 'portrait');
        // Render the HTML as PDF
        $dompdf->render();
		if ($st_create=='1'){
			// $nama_file=$data['judul_per_ina'].' '.$data_transaksi->nopermintaan.'.pdf';
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
		}
        // // Output the generated PDF to Browser
	}
	function cetak_tindakan_icu($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$data=$this->setting_tindakan_icu_label();
		$q="
			SELECT 
			
			H.*,mhari.nama_hari
			FROM `tpoliklinik_icu` H
			LEFT JOIN date_row ON date_row.tanggal=DATE(H.tanggal_input)
			LEFT JOIN mhari ON mhari.id=date_row.hari
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_header=$this->db->query($q)->row_array();
			// print_r($data_header);exit;
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header_logo=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header as judul_header_ina,judul_header_eng,judul_footer as judul_footer_ina,footer_eng as judul_footer_eng
			FROM setting_icu
			";
		$data_detail=$this->db->query($q)->row_array();
		if ($data_detail){
			
		}else{
			$data_detail=array();
		}
		if ($data_header['st_ranap']=='1'){//RANAP
			$data_pasien=$this->get_data_pasien($data_header['pendaftaran_id'],1);
		}else{
			$data_pasien=$this->get_data_pasien($data_header['pendaftaran_id'],0);
		}
		$data_header_logo['isi_surat']=1;
		$data=array_merge($data,$data_header_logo,$data_header,$data_detail,$data_pasien);
        $data['title']=$data_detail['judul_header_ina'].' '.$data['no_medrec'];
        $nama_file=$data['judul_header_ina'] .' '.$data['no_medrec'];
		$data = array_merge($data, backend_info());
		// print_r($data);exit;
        $html = $this->load->view('Tpendaftaran_ranap_erm/pdf_tindakan_icu', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);exit;
		
        // (Optional) Setup the paper size and orientation
        // $dompdf->setPaper(array(0,0,609.4488,935.433), 'portrait');
		$dompdf->setPaper('A4', 'portrait');
        // Render the HTML as PDF
        $dompdf->render();
		if ($st_create=='1'){
			// $nama_file=$data['judul_per_ina'].' '.$data_transaksi->nopermintaan.'.pdf';
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
		}
        // // Output the generated PDF to Browser
	}
	function cetak_tindakan_isolasi($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$data=$this->setting_tindakan_isolasi_label();
		$q="
			SELECT 
			
			H.*,mhari.nama_hari
			FROM `tpoliklinik_isolasi` H
			LEFT JOIN date_row ON date_row.tanggal=DATE(H.tanggal_input)
			LEFT JOIN mhari ON mhari.id=date_row.hari
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_header=$this->db->query($q)->row_array();
			// print_r($data_header);exit;
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header_logo=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header as judul_header_ina,judul_header_eng,judul_footer as judul_footer_ina,footer_eng as judul_footer_eng
			FROM setting_isolasi
			";
		$data_detail=$this->db->query($q)->row_array();
		if ($data_detail){
			
		}else{
			$data_detail=array();
		}
		if ($data_header['st_ranap']=='1'){//RANAP
			$data_pasien=$this->get_data_pasien($data_header['pendaftaran_id'],1);
		}else{
			$data_pasien=$this->get_data_pasien($data_header['pendaftaran_id'],0);
		}
		$data_header_logo['isi_surat']=1;
		$data=array_merge($data,$data_header_logo,$data_header,$data_detail,$data_pasien);
        $data['title']=$data_detail['judul_header_ina'].' '.$data['no_medrec'];
        $nama_file=$data['judul_header_ina'] .' '.$data['no_medrec'];
		$data = array_merge($data, backend_info());
		// print_r($data);exit;
        $html = $this->load->view('Tpendaftaran_ranap_erm/pdf_tindakan_isolasi', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);exit;
		
        // (Optional) Setup the paper size and orientation
        // $dompdf->setPaper(array(0,0,609.4488,935.433), 'portrait');
		$dompdf->setPaper('A4', 'portrait');
        // Render the HTML as PDF
        $dompdf->render();
		if ($st_create=='1'){
			// $nama_file=$data['judul_per_ina'].' '.$data_transaksi->nopermintaan.'.pdf';
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
		}
        // // Output the generated PDF to Browser
	}
	function cetak_tindakan_dnr($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$data=$this->setting_tindakan_dnr_label();
		$q="
			SELECT 
			
			H.*,mhari.nama_hari
			FROM `tpoliklinik_dnr` H
			LEFT JOIN date_row ON date_row.tanggal=DATE(H.tanggal_input)
			LEFT JOIN mhari ON mhari.id=date_row.hari
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_header=$this->db->query($q)->row_array();
			// print_r($data_header);exit;
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header_logo=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header as judul_header_ina,judul_header_eng,judul_footer as judul_footer_ina,footer_eng as judul_footer_eng
			FROM setting_dnr
			";
		$data_detail=$this->db->query($q)->row_array();
		if ($data_detail){
			
		}else{
			$data_detail=array();
		}
		if ($data_header['st_ranap']=='1'){//RANAP
			$data_pasien=$this->get_data_pasien($data_header['pendaftaran_id'],1);
		}else{
			$data_pasien=$this->get_data_pasien($data_header['pendaftaran_id'],0);
		}
		$data_header_logo['isi_surat']=1;
		$data=array_merge($data,$data_header_logo,$data_header,$data_detail,$data_pasien);
        $data['title']=$data_detail['judul_header_ina'].' '.$data['no_medrec'];
        $nama_file=$data['judul_header_ina'] .' '.$data['no_medrec'];
		$data = array_merge($data, backend_info());
		// print_r($data);exit;
        $html = $this->load->view('Tpendaftaran_ranap_erm/pdf_tindakan_dnr', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);exit;
		
        // (Optional) Setup the paper size and orientation
        // $dompdf->setPaper(array(0,0,609.4488,935.433), 'portrait');
		$dompdf->setPaper('A4', 'portrait');
        // Render the HTML as PDF
        $dompdf->render();
		if ($st_create=='1'){
			// $nama_file=$data['judul_per_ina'].' '.$data_transaksi->nopermintaan.'.pdf';
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
		}
        // // Output the generated PDF to Browser
	}
	function cetak_ringkasan_pulang($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT *FROM setting_assesmen_pulang_label";
		$data=$this->db->query($q)->row_array();
			// print_r($data);exit;
		$q="
			SELECT 
			mppa.nama as petugas_terima,MP.no_medrec,MP.namapasien,CONCAT(MP.umurtahun,' Tahun ',MP.umurbulan,' Bulan ',MP.umurhari,' Hari') as umur
			,JK.ref as jenis_kelamin,MP.tanggal_lahir
			,H.*
			FROM `tranap_assesmen_pulang` H
			INNER JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id
			LEFT JOIN merm_referensi JK ON JK.nilai=MP.jenis_kelamin AND JK.ref_head_id='1'
			LEFT JOIN mppa ON mppa.id=H.diserahkan
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header_ina,judul_header_eng,judul_footer_ina,judul_footer_eng
			FROM setting_assesmen_pulang
			";
		$data_detail=$this->db->query($q)->row_array();
		if ($data_detail){
			
		}else{
			$data_detail=array();
		}
		$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan,makral_satuan,mcahaya_satuan,mgcs_eye_satuan,mgcs_motion_satuan,mgcs_voice_satuan,mpupil_satuan,mspo2_satuan,mgds_satuan")->row_array();
		$data=array_merge($data,$data_header,$data_detail,$data_satuan_ttv);
		// print_r($data);exit;
        $data['title']=$data['judul_header_ina'] .' '.$data['namapasien'];
        $nama_file=$data['judul_header_ina'] .' '.$data['namapasien'];
		$data = array_merge($data, backend_info());
        $html = $this->load->view('Tpendaftaran_ranap_erm/pdf_ringkasan', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);
		
        // (Optional) Setup the paper size and orientation
        // $dompdf->setPaper(array(0,0,609.4488,935.433), 'portrait');
		$dompdf->setPaper('A4', 'portrait');
        // Render the HTML as PDF
        $dompdf->render();
		if ($st_create=='1'){
			// $nama_file=$data['judul_per_ina'].' '.$data_transaksi->nopermintaan.'.pdf';
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
		}
        // // Output the generated PDF to Browser
	}
	function cetak_assesmen_ringkasan_pulang($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT *FROM setting_ringkasan_pulang_label";
		$data=$this->db->query($q)->row_array();
			// print_r($data);exit;
		$q="
			SELECT 
			mdokter.nama as petugas_terima,MP.no_medrec,MP.namapasien,CONCAT(MP.umurtahun,' Tahun ',MP.umurbulan,' Bulan ',MP.umurhari,' Hari') as umur
			,JK.ref as jenis_kelamin,MP.tanggal_lahir,MP.no_medrec
			,H.*
			FROM `tranap_ringkasan_pulang` H
			INNER JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id_ranap
			LEFT JOIN merm_referensi JK ON JK.nilai=MP.jenis_kelamin AND JK.ref_head_id='1'
			LEFT JOIN mdokter ON mdokter.id=H.dpjp
			WHERE H.assesmen_id='$assesmen_id'
		";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header_ina,judul_header_eng,judul_footer_ina,judul_footer_eng
			FROM setting_ringkasan_pulang
			";
		$data_detail=$this->db->query($q)->row_array();
		if ($data_detail){
			
		}else{
			$data_detail=array();
		}
		$data=array_merge($data,$data_header,$data_detail);
		// print_r($data);exit;
        $data['title']=$data['judul_header_ina'] .' '.$data['no_medrec'];
        $nama_file=$data['judul_header_ina'] .' '.$data['no_medrec'];
		$data = array_merge($data, backend_info());
        $html = $this->load->view('Tpendaftaran_ranap_erm/pdf_ringkasan_pulang', $data,true);
        $dompdf->loadHtml($html);
		// print_r($html);
		
        // (Optional) Setup the paper size and orientation
        // $dompdf->setPaper(array(0,0,609.4488,935.433), 'portrait');
		$dompdf->setPaper('A4', 'portrait');
        // Render the HTML as PDF
        $dompdf->render();
		if ($st_create=='1'){
			// $nama_file=$data['judul_per_ina'].' '.$data_transaksi->nopermintaan.'.pdf';
			$nama_file=$data['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
			$dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
		}
        // // Output the generated PDF to Browser
	}
	function get_data_assesmen_ri($pendaftaran_id_ranap){
		$q="SELECT *
			 FROM tranap_assesmen_ri WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_ruangan($pendaftaran_id_ranap){
		$q="
				SELECT H.id as history_id,H.idruangan as idruangan_asal,H.idkelas as idkelas_asal,H.idbed as idbed_asal,H.tanggaldari as tanggaldari_asal,H.tanggalsampai  as tanggalsampai_asal
				FROM `trawatinap_history_bed` H
				WHERE H.idrawatinap='$pendaftaran_id_ranap'
				ORDER BY H.tanggaldari DESC 
				LIMIT 1
			";
		return $this->db->query($q)->row_array();
	}
	function setting_assesmen_ri(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			FROM `setting_asses_ri` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function list_template_assesmen_ri(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_assesmen_ri H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	
	function get_data_assesmen_ri_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_assesmen_ri WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_assesmen_ri_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_assesmen_ri_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_assesmen_ri H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_assesmen_ri_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				st_anamnesa as st_anamnesa_asal,keterangan_hapus as keterangan_hapus_asal,keluhan_utama as keluhan_utama_asal,nama_anamnesa as nama_anamnesa_asal,hubungan_anamnesa as hubungan_anamnesa_asal,st_riwayat_penyakit as st_riwayat_penyakit_asal,riwayat_penyakit as riwayat_penyakit_asal,riwayat_penyakit_lainnya as riwayat_penyakit_lainnya_asal,keluhan_utama_list as keluhan_utama_list_asal,keluhan_utama_text as keluhan_utama_text_asal,riwayat_penyakit_pernah_list as riwayat_penyakit_pernah_list_asal,riwayat_penyakit_pernah as riwayat_penyakit_pernah_asal,pengobatan_list as pengobatan_list_asal,pengobatan_keterangan as pengobatan_keterangan_asal,tingkat_kesadaran as tingkat_kesadaran_asal,keadaan_umum as keadaan_umum_asal,keadaan_gizi as keadaan_gizi_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,suhu as suhu_asal,nadi as nadi_asal,nafas as nafas_asal,riwayat_alergi as riwayat_alergi_asal,st_riwayat_penyakit_menurun as st_riwayat_penyakit_menurun_asal,penyakit_menurun_id as penyakit_menurun_id_asal,penyakit_menurun_id_nama as penyakit_menurun_id_nama_asal,nama_orang_penyakit_menurun as nama_orang_penyakit_menurun_asal,status_penyakit_menurun as status_penyakit_menurun_asal,status_penyakit_menurun_nama as status_penyakit_menurun_nama_asal,st_riwayat_penyakit_menular as st_riwayat_penyakit_menular_asal,penyakit_menular_id as penyakit_menular_id_asal,penyakit_menular_id_nama as penyakit_menular_id_nama_asal,nama_orang_penyakit_menular as nama_orang_penyakit_menular_asal,status_penyakit_menular as status_penyakit_menular_asal,status_penyakit_menular_nama as status_penyakit_menular_nama_asal,proteksi_kesehatan as proteksi_kesehatan_asal,proteksi_keterangan as proteksi_keterangan_asal,batuk as batuk_asal,warna_sputum as warna_sputum_asal,bunyi_nafas as bunyi_nafas_asal,sesak_nafas_saat as sesak_nafas_saat_asal,bentuk_dada as bentuk_dada_asal,tipe_pernafasan_hidung as tipe_pernafasan_hidung_asal,alat_bantu_nafas as alat_bantu_nafas_asal,merokok as merokok_asal,merokok_sejak as merokok_sejak_asal,merokok_per_hari as merokok_per_hari_asal,jenis_rokok as jenis_rokok_asal,keluhan_lain as keluhan_lain_asal,nyeri_dada as nyeri_dada_asal,palpitasi as palpitasi_asal,pusing as pusing_asal,pingsan as pingsan_asal,pendarahan_pada as pendarahan_pada_asal,ket_pendarahan as ket_pendarahan_asal,oedama as oedama_asal,oedama_ket as oedama_ket_asal,hematoma as hematoma_asal,hematoma_ket as hematoma_ket_asal,piechi as piechi_asal,piechi_ket as piechi_ket_asal,ics as ics_asal,ics_ket as ics_ket_asal,capitary as capitary_asal,capitary_ket as capitary_ket_asal,limfe as limfe_asal,limfe_lokasi as limfe_lokasi_asal,cordis as cordis_asal,jenis_alat_bantu as jenis_alat_bantu_asal,kelainan_cordis as kelainan_cordis_asal,skrining_nutrisi_id as skrining_nutrisi_id_asal,ref_nilai_id as ref_nilai_id_asal,nama_hasil_pengkajian as nama_hasil_pengkajian_asal,tindakan_nutrisi as tindakan_nutrisi_asal,total_skor_nutrisi as total_skor_nutrisi_asal,syaraf_sadar as syaraf_sadar_asal,syaraf_gcs as syaraf_gcs_asal,syaraf_motorik as syaraf_motorik_asal,syaraf_verbal as syaraf_verbal_asal,syaraf_buka_mata as syaraf_buka_mata_asal,kesemutan as kesemutan_asal,kejang as kejang_asal,parase as parase_asal,paralise as paralise_asal,bingung as bingung_asal,koordinasi as koordinasi_asal,kaku_duduk as kaku_duduk_asal,trimus as trimus_asal,nyeri_kepala as nyeri_kepala_asal,nyeri_kepala_lokasi as nyeri_kepala_lokasi_asal,nyari_kepala_kelainan as nyari_kepala_kelainan_asal,penglihatan as penglihatan_asal,conjugtiva as conjugtiva_asal,sklera as sklera_asal,keadaan_mata as keadaan_mata_asal,kornea as kornea_asal,alat_bantu_mata as alat_bantu_mata_asal,keluhan_lain_mata as keluhan_lain_mata_asal,berdengung as berdengung_asal,nyeri_telinga as nyeri_telinga_asal,cairan_telinga as cairan_telinga_asal,plulurent as plulurent_asal,keadaan_pendengaran as keadaan_pendengaran_asal,keluhan_lain_pendengaran as keluhan_lain_pendengaran_asal,septum as septum_asal,mucosa as mucosa_asal,polip as polip_asal,epistaxis as epistaxis_asal,sekret as sekret_asal,kelainan_penciuman as kelainan_penciuman_asal,bak_x as bak_x_asal,bak_warna as bak_warna_asal,bak_jumlah as bak_jumlah_asal,bak_freq as bak_freq_asal,bab_freq as bab_freq_asal,bab_konsisten as bab_konsisten_asal,bab_warna as bab_warna_asal,obat_pencahar as obat_pencahar_asal,bak_saat_ini as bak_saat_ini_asal,alat_saat_ini as alat_saat_ini_asal,sejak_saat_ini as sejak_saat_ini_asal,bak_x_saat_ini as bak_x_saat_ini_asal,bak_jumlah_saat_ini as bak_jumlah_saat_ini_asal,bak_warna_saat_ini as bak_warna_saat_ini_asal,bab_saat_ini as bab_saat_ini_asal,ket_saat_ini as ket_saat_ini_asal,bab_x_saat_ini as bab_x_saat_ini_asal,bab_warna_saat_ini as bab_warna_saat_ini_asal,bab_bau_saat_ini as bab_bau_saat_ini_asal,bab_keringat_saat_ini as bab_keringat_saat_ini_asal,keluhan_saat_ini as keluhan_saat_ini_asal,bentuk_bahu as bentuk_bahu_asal,bentuk_tulang as bentuk_tulang_asal,rentan_gerak_atas as rentan_gerak_atas_asal,rentan_gerak_bawah as rentan_gerak_bawah_asal,gerak_motorik as gerak_motorik_asal,uji_otot_id as uji_otot_id_asal,nilai_otot_1 as nilai_otot_1_asal,nilai_otot_2 as nilai_otot_2_asal,nilai_otot_3 as nilai_otot_3_asal,nilai_otot_4 as nilai_otot_4_asal,keseimbangan as keseimbangan_asal,cara_berjalan as cara_berjalan_asal,alat_bantu as alat_bantu_asal,dimana as dimana_asal,sejak as sejak_asal,waktu_tidur_siang as waktu_tidur_siang_asal,waktu_tidur_malam as waktu_tidur_malam_asal,penghantar as penghantar_asal,obat_tidur as obat_tidur_asal,jenis_obat as jenis_obat_asal,dosis as dosis_asal,sejak_obat_tidur as sejak_obat_tidur_asal,insomnia as insomnia_asal,waktu_luang as waktu_luang_asal,hobby as hobby_asal,rutinitas as rutinitas_asal,kelainan_pola as kelainan_pola_asal,warna_kulit as warna_kulit_asal,bergelembung as bergelembung_asal,lecet as lecet_asal,luka as luka_asal,decubitus as decubitus_asal,luka_bakar as luka_bakar_asal,grade_luka_bakar as grade_luka_bakar_asal,persen_luka_bakar as persen_luka_bakar_asal,turgor as turgor_asal,keadaan_kulit as keadaan_kulit_asal,keadaan_kulit_kepala as keadaan_kulit_kepala_asal,mandi_x as mandi_x_asal,gosok_gigi_x as gosok_gigi_x_asal,ganti_baju_x as ganti_baju_x_asal,cuci_rambut_x as cuci_rambut_x_asal,dilakukan_secara as dilakukan_secara_asal,kelainan_kulit as kelainan_kulit_asal,jk_pasien as jk_pasien_asal,payudara as payudara_asal,kelainan_payudara as kelainan_payudara_asal,ukuran_payudara as ukuran_payudara_asal,testis as testis_asal,lokasi_kelainan_testis as lokasi_kelainan_testis_asal,ukuranan_kelainan_testis as ukuranan_kelainan_testis_asal,jenis_kelainan_testis as jenis_kelainan_testis_asal,penis as penis_asal,lokasi_kelainan_penis as lokasi_kelainan_penis_asal,jenis_kelainan_penis as jenis_kelainan_penis_asal,cairan_keluar_penis as cairan_keluar_penis_asal,jenis_cairan_penis as jenis_cairan_penis_asal,genitalia as genitalia_asal,lokasi_kelainan_genitalia as lokasi_kelainan_genitalia_asal,ukuran_kelainan_genitalia as ukuran_kelainan_genitalia_asal,jenis_kelainan_genitalia as jenis_kelainan_genitalia_asal,prolaps as prolaps_asal,lokasi_kelainan_prolaps as lokasi_kelainan_prolaps_asal,ukuran_kelainan_prolaps as ukuran_kelainan_prolaps_asal,jenis_kelainan_prolaps as jenis_kelainan_prolaps_asal,fluor as fluor_asal,infertil as infertil_asal,infertil_sebutkan as infertil_sebutkan_asal,fertile_g as fertile_g_asal,fertile_p as fertile_p_asal,fertile_ah as fertile_ah_asal,fertile_am as fertile_am_asal,fertile_ab as fertile_ab_asal,umur_kehamilan as umur_kehamilan_asal,hpl as hpl_asal,siklus as siklus_asal,lama_siklus as lama_siklus_asal,teratur_siklus as teratur_siklus_asal,sakit_haid as sakit_haid_asal,haid_terakhir as haid_terakhir_asal,jenis_pendarahan as jenis_pendarahan_asal,jumlah_pendarahan as jumlah_pendarahan_asal,laktasi as laktasi_asal,menyusui as menyusui_asal,menyusui_lama as menyusui_lama_asal,alasan_tiak_menyusui as alasan_tiak_menyusui_asal,kb as kb_asal,kb_lama as kb_lama_asal,kb_keluhan as kb_keluhan_asal,alasan_tidak_kb as alasan_tidak_kb_asal,seksualitas as seksualitas_asal,kesulitan_seksualitas as kesulitan_seksualitas_asal,kelain_seksualitas as kelain_seksualitas_asal,komunikasi as komunikasi_asal,kemampuan_bicara as kemampuan_bicara_asal,bahasa as bahasa_asal,ekpresi as ekpresi_asal,pasien_tinggal as pasien_tinggal_asal,orang_terdekat as orang_terdekat_asal,pengambil_keputusan as pengambil_keputusan_asal,hungan_keluarga as hungan_keluarga_asal,kedudukan_dalam_masyarakat as kedudukan_dalam_masyarakat_asal,kelainan_komunikasi as kelainan_komunikasi_asal,ibadah as ibadah_asal,tempat_ibadah as tempat_ibadah_asal,berdoa as berdoa_asal,bantuan_yang_dibutuhkan as bantuan_yang_dibutuhkan_asal,persepsi_sehat as persepsi_sehat_asal,harapan as harapan_asal,pekerjaan_utama as pekerjaan_utama_asal,tempat_kerja as tempat_kerja_asal,jabatan as jabatan_asal,pekerjaan_tambahan as pekerjaan_tambahan_asal,dibantu_pekerjaan as dibantu_pekerjaan_asal,biaya_hidup as biaya_hidup_asal,lain_pekerjaan as lain_pekerjaan_asal,penanggung_biaya_rs as penanggung_biaya_rs_asal,penanggung_nama_kelaurga as penanggung_nama_kelaurga_asal,keluhan_sosial as keluhan_sosial_asal,rumah as rumah_asal,ventilasi as ventilasi_asal,sinar_matahari as sinar_matahari_asal,jendela as jendela_asal,wc as wc_asal,bab_tempat as bab_tempat_asal,air_cucian as air_cucian_asal,tempat_sampah as tempat_sampah_asal,tempat_pembuangan as tempat_pembuangan_asal,sumber_air_minum as sumber_air_minum_asal,sumber_air_minum_tempat as sumber_air_minum_tempat_asal,lokasi_tinggal as lokasi_tinggal_asal,wabah_terjangkit as wabah_terjangkit_asal,sebutkan_tempat_wabah as sebutkan_tempat_wabah_asal,pupil_kiri as pupil_kiri_asal,pupil_kiri_cahaya as pupil_kiri_cahaya_asal,pupil_kanan as pupil_kanan_asal,pupil_kanan_cahaya as pupil_kanan_cahaya_asal
				FROM tranap_assesmen_ri_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	//RISIKO JATUH
	function get_data_risiko_jatuh($pendaftaran_id_ranap){
		$q="SELECT *
			 FROM tranap_risiko_jatuh WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function setting_risiko_jatuh(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			FROM `setting_risiko_jatuh` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function list_template_risiko_jatuh(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_risiko_jatuh H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	function list_template_assesmen_lokasi_operasi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_lokasi_operasi H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function get_data_risiko_jatuh_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_risiko_jatuh WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_risiko_jatuh_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_risiko_jatuh_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_risiko_jatuh H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_risiko_jatuh_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				mrisiko_jatuh_id as mrisiko_jatuh_id_asal,tanggal_pengkajian as tanggal_pengkajian_asal,ref_nilai_id as ref_nilai_id_asal,total_skor_risiko_jatuh as total_skor_risiko_jatuh_asal,nama_hasil_pengkajian as nama_hasil_pengkajian_asal,nama_tindakan as nama_tindakan_asal
				FROM tranap_risiko_jatuh_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_dpjp(){
		$q="SELECT H.judul_header,H.judul_footer, H.judul_header_eng,H.judul_footer_eng 
			FROM `setting_dpjp` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	//PINDAH DPJP
	function setting_pindah_dpjp(){
		$q="
			SELECT 
			H1.judul_header,H1.judul_header_eng,H1.st_edit_catatan,H1.lama_edit,H1.orang_edit,H1.st_hapus_catatan,H1.lama_hapus,H1.orang_hapus,H1.st_duplikasi_catatan,H1.lama_duplikasi,H1.orang_duplikasi
			,H2.paragraf_1_ina,H2.paragraf_1_eng,H2.pilih_ttd_ina,H2.pilih_ttd_eng,H2.nama_ttd_ina,H2.nama_ttd_eng,H2.ttl_ttd_ina,H2.ttl_ttd_eng,H2.umur_ttd_ina,H2.umur_ttd_eng,H2.alamat_ttd_ina,H2.alamat_ttd_eng,H2.provinsi_ttd_ina,H2.provinsi_ttd_eng,H2.kab_ttd_ina,H2.kab_ttd_eng,H2.kec_ttd_ina,H2.kec_ttd_eng,H2.desa_ttd_ina,H2.desa_ttd_eng,H2.kodepos_ttd_ina,H2.kodepos_ttd_eng,H2.rw_ttd_ina,H2.rw_ttd_eng,H2.rt_ttd_ina,H2.rt_ttd_eng,H2.paragraf_2_ina,H2.paragraf_2_eng,H2.dari_dokter_ina,H2.dari_dokter_eng,H2.jadi_dokter_ina,H2.jadi_dokter_eng,H2.paragraf_3_ina,H2.paragraf_3_eng,H2.noreg_ina,H2.noreg_eng,H2.nama_pasien_ina,H2.nama_pasien_eng,H2.nomedrec_ina,H2.nomedrec_eng,H2.ttl_pasien_ina,H2.ttl_pasien_eng,H2.umur_pasien_ina,H2.umur_pasien_eng,H2.jk_ina,H2.jk_eng,H2.ruangan_ina,H2.ruangan_eng,H2.kelas_ina,H2.kelas_eng,H2.bed_ina,H2.bed_eng,H2.paragraf_4_ina,H2.paragraf_4_eng,H2.yg_pernyataan_ina,H2.yg_pernyataan_eng,H2.saksi_rs_ina,H2.saksi_rs_eng,H2.ttd_dpjp_lama_ina,H2.ttd_dpjp_lama_eng,H2.ttd_dpjp_baru_ina,H2.ttd_dpjp_baru_eng
			,H2.hubungan_ina,H2.hubungan_eng
			FROM setting_pindah_dpjp H1,setting_pindah_dpjp_keterangan  H2
		";
		return $this->db->query($q)->row_array();
	}
	function get_data_pindah_dpjp($pendaftaran_id_ranap,$st_ranap='1'){
		// $q="SELECT *
			 // FROM tranap_pindah_dpjp WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		// print_r($q);exit;
		if ($st_ranap=='1'){
			
		$q="SELECT 
				MD1.nama as nama_dokter_awal,MD2.nama as nama_dokter_baru
				,H.*,mppa.nama as nama_petugas
				FROM tranap_pindah_dpjp H 
				LEFT JOIN mdokter MD1 ON MD1.id=H.iddokter_awal
				LEFT JOIN mdokter MD2 ON MD2.id=H.iddokter_baru
				LEFT JOIN mppa ON mppa.id=H.petugas_id
				WHERE H.pendaftaran_id_ranap='$pendaftaran_id_ranap' AND H.st_ranap='$st_ranap' AND H.status_assemen IN (1,3) ORDER BY H.assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			
		}else{
			$q="SELECT 
				MD1.nama as nama_dokter_awal,MD2.nama as nama_dokter_baru
				,H.*,mppa.nama as nama_petugas
				FROM tranap_pindah_dpjp H 
				LEFT JOIN mdokter MD1 ON MD1.id=H.iddokter_awal
				LEFT JOIN mdokter MD2 ON MD2.id=H.iddokter_baru
				LEFT JOIN mppa ON mppa.id=H.petugas_id
				WHERE H.pendaftaran_id='$pendaftaran_id_ranap' AND H.st_ranap='$st_ranap' AND H.status_assemen IN (1,3) ORDER BY H.assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
		}
			return $hasil;
	}
	
	function get_data_pindah_dpjp_trx($assesmen_id){
		$q="SELECT 
				MD1.nama as nama_dokter_awal,MD2.nama as nama_dokter_baru
				,H.*,mppa.nama as nama_petugas
				FROM tranap_pindah_dpjp H 
				LEFT JOIN mdokter MD1 ON MD1.id=H.iddokter_awal
				LEFT JOIN mdokter MD2 ON MD2.id=H.iddokter_baru
				LEFT JOIN mppa ON mppa.id=H.petugas_id
				WHERE H.assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_pindah_dpjp_trx_his($assesmen_id,$versi_edit){
		$q="SELECT 
			MD1.nama as nama_dokter_awal,MD2.nama as nama_dokter_baru
				,H.*,mppa.nama as nama_petugas
			FROM (SELECT *
			 FROM tranap_pindah_dpjp_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_pindah_dpjp H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ) H
			 LEFT JOIN mdokter MD1 ON MD1.id=H.iddokter_awal
			LEFT JOIN mdokter MD2 ON MD2.id=H.iddokter_baru
			LEFT JOIN mppa ON mppa.id=H.petugas_id
			 
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_pindah_dpjp_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				pilih_ttd_id as pilih_ttd_id_asal,nama_ttd as nama_ttd_asal,ttl_ttd as ttl_ttd_asal,umur_ttd as umur_ttd_asal,alamat_ttd as alamat_ttd_asal,provinsi_id_ttd as provinsi_id_ttd_asal,kabupaten_id_ttd as kabupaten_id_ttd_asal,kecamatan_id_ttd as kecamatan_id_ttd_asal,kelurahan_id_ttd as kelurahan_id_ttd_asal,rw_ttd as rw_ttd_asal,rt_ttd as rt_ttd_asal,kodepos_ttd as kodepos_ttd_asal,hubungan_ttd as hubungan_ttd_asal,iddokter_awal as iddokter_awal_asal,iddokter_baru as iddokter_baru_asal,ttd_pernyataan as ttd_pernyataan_asal,nip_petugas as nip_petugas_asal,petugas_id as petugas_id_asal,st_jawab as st_jawab_asal,jawaban as jawaban_asal,tanggal_jawab as tanggal_jawab_asal,tandatangan_pembuat as tandatangan_pembuat_asal
				FROM tranap_pindah_dpjp_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	//PERMINTAAN dpjp
	//PINDAH DPJP
	function setting_permintaan_dpjp(){
		$q="
			SELECT 
			H1.judul_header,H1.judul_header_eng,H1.st_edit_catatan,H1.lama_edit,H1.orang_edit,H1.st_hapus_catatan,H1.lama_hapus,H1.orang_hapus,H1.st_duplikasi_catatan,H1.lama_duplikasi,H1.orang_duplikasi
			,H2.paragraf_1_ina,H2.paragraf_1_eng,H2.pilih_ttd_ina,H2.pilih_ttd_eng,H2.nama_ttd_ina,H2.nama_ttd_eng,H2.ttl_ttd_ina,H2.ttl_ttd_eng,H2.umur_ttd_ina,H2.umur_ttd_eng,H2.alamat_ttd_ina,H2.alamat_ttd_eng
			,H2.paragraf_2_ina,H2.paragraf_2_eng,H2.dari_dokter_ina,H2.dari_dokter_eng,H2.jadi_dokter_ina,H2.jadi_dokter_eng,H2.paragraf_3_ina,H2.paragraf_3_eng,H2.noreg_ina,H2.noreg_eng,H2.nama_pasien_ina,H2.nama_pasien_eng,H2.nomedrec_ina,H2.nomedrec_eng,H2.ttl_pasien_ina,H2.ttl_pasien_eng,H2.umur_pasien_ina,H2.umur_pasien_eng,H2.jk_ina,H2.jk_eng
			,H2.alasan_ina,H2.alasan_eng
			,H2.paragraf_4_ina,H2.paragraf_4_eng,H2.yg_pernyataan_ina,H2.yg_pernyataan_eng,H2.saksi_rs_ina,H2.saksi_rs_eng,H2.ttd_dpjp_lama_ina,H2.ttd_dpjp_lama_eng,H2.ttd_dpjp_baru_ina,H2.ttd_dpjp_baru_eng
			,H2.hubungan_ina,H2.hubungan_eng
			FROM setting_permintaan_dpjp H1,setting_permintaan_dpjp_keterangan  H2
		";
		return $this->db->query($q)->row_array();
	}
	function get_data_permintaan_dpjp($pendaftaran_id_ranap,$st_ranap='1'){
		// $q="SELECT *
			 // FROM tranap_permintaan_dpjp WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		// print_r($q);exit;
		if ($st_ranap=='1'){
			
		$q="SELECT 
				MD1.nama as nama_dokter
				,H.*,mppa.nama as nama_petugas
				FROM tranap_permintaan_dpjp H 
				LEFT JOIN mdokter MD1 ON MD1.id=H.iddokter
				LEFT JOIN mppa ON mppa.id=H.petugas_id
				WHERE H.pendaftaran_id_ranap='$pendaftaran_id_ranap' AND H.st_ranap='$st_ranap' AND H.status_assemen IN (1,3) ORDER BY H.assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			
		}else{
			$q="SELECT 
				MD1.nama as nama_dokter
				,H.*,mppa.nama as nama_petugas
				FROM tranap_permintaan_dpjp H 
				LEFT JOIN mdokter MD1 ON MD1.id=H.iddokter
				LEFT JOIN mppa ON mppa.id=H.petugas_id
				WHERE H.pendaftaran_id='$pendaftaran_id_ranap' AND H.st_ranap='$st_ranap' AND H.status_assemen IN (1,3) ORDER BY H.assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
		}
			return $hasil;
	}
	
	function get_data_permintaan_dpjp_trx($assesmen_id){
		$q="SELECT 
				MD1.nama as nama_dokter
				,H.*,mppa.nama as nama_petugas
				FROM tranap_permintaan_dpjp H 
				LEFT JOIN mdokter MD1 ON MD1.id=H.iddokter
				LEFT JOIN mppa ON mppa.id=H.petugas_id
				WHERE H.assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_permintaan_dpjp_trx_his($assesmen_id,$versi_edit){
		$q="SELECT 
			MD1.nama as nama_dokter
				,H.*,mppa.nama as nama_petugas
			FROM (SELECT *
			 FROM tranap_permintaan_dpjp_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_permintaan_dpjp H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ) H
			 LEFT JOIN mdokter MD1 ON MD1.id=H.iddokter
			LEFT JOIN mppa ON mppa.id=H.petugas_id
			 
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_permintaan_dpjp_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				pilih_ttd_id as pilih_ttd_id_asal,nama_ttd as nama_ttd_asal,ttl_ttd as ttl_ttd_asal,umur_ttd as umur_ttd_asal,alamat_ttd as alamat_ttd_asal
				,hubungan_ttd as hubungan_ttd_asal,iddokter as iddokter_asal,ttd_pernyataan as ttd_pernyataan_asal,nip_petugas as nip_petugas_asal,petugas_id as petugas_id_asal
				,alasan as alasan_asal
				FROM tranap_permintaan_dpjp_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	//LOKASI OPERASI
	function setting_lokasi_operasi(){
		$q="
			SELECT 
			H1.*
			FROM setting_lokasi_operasi H1
		";
		return $this->db->query($q)->row_array();
	}
	function get_data_lokasi_operasi($pendaftaran_id_ranap,$st_ranap='0'){
		if ($st_ranap=='0'){
			$q="SELECT * FROM tranap_lokasi_operasi WHERE pendaftaran_id='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}else{
			$q="SELECT * FROM tranap_lokasi_operasi WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		
		// print_r($q);exit;
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_lokasi_operasi_trx($assesmen_id){
		$q="SELECT 
				H.*
				FROM tranap_lokasi_operasi H 
				WHERE H.assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_lokasi_operasi_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_lokasi_operasi_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_lokasi_operasi H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_lokasi_operasi_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				gambar_tubuh as gambar_tubuh_asal,txt_keterangan as txt_keterangan_asal,gambar_id as gambar_id_asal,template_gambar as template_gambar_asal,diagnosa as diagnosa_asal,rencana_tindakan as rencana_tindakan_asal,tanggal_operasi as tanggal_operasi_asal,ttd_nama as ttd_nama_asal,ttd_pasien as ttd_pasien_asal

				FROM tranap_lokasi_operasi_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_lokalis_lokasi_operasi($assesmen_id,$gambar_id,$versi_edit='#'){
		if ($versi_edit=='#'){
		$q="SELECT *FROM tranap_lokasi_operasi_lokalis WHERE assesmen_id='$assesmen_id' AND gambar_id='$gambar_id'";
			
		}else{
			$q="
				SELECT *FROM(
					SELECT D.jml_edit as versi_edit,H.* 
					FROM tranap_lokasi_operasi_lokalis H
					LEFT JOIN tranap_lokasi_operasi D ON D.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'

					UNION ALL

					SELECT H.* 
					FROM tranap_lokasi_operasi_x_his_lokalis H
					WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'
					) T 

					WHERE T.versi_edit='$versi_edit'
					ORDER BY T.`index` ASC
			";
		}
		// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	
	//SGA
	function get_data_sga($pendaftaran_id_ranap){
		$q="SELECT *
			 FROM tranap_sga WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		// print_r($q);exit;
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_sga_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_sga WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_sga_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_sga_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_sga H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_sga_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				berat_badan_biasa as berat_badan_biasa_asal,berat_badan as berat_badan_asal,waktu as waktu_asal,tinggi_badan as tinggi_badan_asal,nilai_imt as nilai_imt_asal,nilai_imt_id as nilai_imt_id_asal,diagnosa_medis as diagnosa_medis_asal,nilai_sga as nilai_sga_asal,nilai_sga_id as nilai_sga_id_asal,penilaian_sga as penilaian_sga_asal,tindakan_sga as tindakan_sga_asal,keterangan as keterangan_asal
				,nilai_imt_text as nilai_imt_text_asal
				FROM tranap_sga_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function list_template_sga(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_sga H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	//SGA
	function get_data_gizi_anak($pendaftaran_id_ranap){
		$q="SELECT *
			 FROM tranap_gizi_anak WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		// print_r($q);exit;
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_gizi_anak_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_gizi_anak WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_gizi_anak_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_gizi_anak_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_gizi_anak H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_gizi_anak_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				tanggal_pemeriksaan as tanggal_pemeriksaan_asal,berat_badan as berat_badan_asal,bmi as bmi_asal,bb_tb as bb_tb_asal,total_skor as total_skor_asal,kesimpulan_gizi_id as kesimpulan_gizi_id_asal,kesimpulan_gizi_text as kesimpulan_gizi_text_asal,tindakan_id as tindakan_id_asal,tindakan_nama as tindakan_nama_asal,keterangan as keterangan_asal
				FROM tranap_gizi_anak_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function list_template_gizi_anak(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_gizi_anak H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	//Gizi Lanjutan
	//SGA
	function get_data_gizi_lanjutan($pendaftaran_id_ranap){
		$q="SELECT *
			 FROM tranap_gizi_lanjutan WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		// print_r($q);exit;
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_gizi_lanjutan_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_gizi_lanjutan WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_gizi_lanjutan_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_gizi_lanjutan_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_gizi_lanjutan H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_gizi_lanjutan_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				mobilitas as mobilitas_asal,perokok as perokok_asal,riwayat_medis as riwayat_medis_asal,diagnosa_medis as diagnosa_medis_asal,pantangan_makan as pantangan_makan_asal,riwayat_alergi as riwayat_alergi_asal,tisak_suka_makanan as tisak_suka_makanan_asal,pengalaman_konseling as pengalaman_konseling_asal,berat_badan_biasa as berat_badan_biasa_asal,berat_badan as berat_badan_asal,waktu as waktu_asal,tinggi_badan as tinggi_badan_asal,nilai_imt as nilai_imt_asal,nilai_imt_id as nilai_imt_id_asal,nilai_imt_text as nilai_imt_text_asal,penurunan_bb_persen as penurunan_bb_persen_asal,penurunan_bb_bulan as penurunan_bb_bulan_asal,pengukuran_lain as pengukuran_lain_asal,bioikimia as bioikimia_asal,udem as udem_asal,artofi as artofi_asal,mual as mual_asal,nafsu_makan as nafsu_makan_asal,muntah as muntah_asal,hilang_lemak as hilang_lemak_asal,kembung as kembung_asal,diare as diare_asal,konstipasi as konstipasi_asal,menelan as menelan_asal,mengunyah as mengunyah_asal,ttv as ttv_asal,data_lain as data_lain_asal
				FROM tranap_gizi_lanjutan_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function list_template_gizi_lanjutan(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_gizi_lanjutan H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	//
	function get_data_discarge($pendaftaran_id_ranap){
		$q="SELECT *
			 FROM tranap_discarge WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		// print_r($q);exit;
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_discarge_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_discarge WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_discarge_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_discarge_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_discarge H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_discarge_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				tanggal_masuk_rs as tanggal_masuk_rs_asal,rencana_pemulangan as rencana_pemulangan_asal,alasan_masuk_rs as alasan_masuk_rs_asal,diagnosa_medis as diagnosa_medis_asal,pengaruh_ranap_pasien as pengaruh_ranap_pasien_asal,pengaruh_ranap_pasien_detail as pengaruh_ranap_pasien_detail_asal,pengaruh_pekerjaan as pengaruh_pekerjaan_asal,pengaruh_pekerjaan_detail as pengaruh_pekerjaan_detail_asal,pengaruh_keuangan as pengaruh_keuangan_asal,pengaruh_keuangan_detail as pengaruh_keuangan_detail_asal,antisipasi_masalah_pulang as antisipasi_masalah_pulang_asal,antisipasi_masalah_pulang_detail as antisipasi_masalah_pulang_detail_asal,bantuan_diperlukan_hal as bantuan_diperlukan_hal_asal,bantuan_diperlukan_hal_detail as bantuan_diperlukan_hal_detail_asal,adakah_yg_membantu as adakah_yg_membantu_asal,adakah_yg_membantu_detail as adakah_yg_membantu_detail_asal,tinggal_sendiri as tinggal_sendiri_asal,tinggal_sendiri_detail as tinggal_sendiri_detail_asal,menggunakan_peralatan_medis as menggunakan_peralatan_medis_asal,menggunakan_peralatan_medis_detail as menggunakan_peralatan_medis_detail_asal,memerlukan_alat_bantu as memerlukan_alat_bantu_asal,memerlukan_alat_bantu_detail as memerlukan_alat_bantu_detail_asal,memerlukan_perawatn_khusus as memerlukan_perawatn_khusus_asal,memerlukan_perawatn_khusus_detail as memerlukan_perawatn_khusus_detail_asal,memerlukan_kebutuhan_pribadi as memerlukan_kebutuhan_pribadi_asal,memerlukan_kebutuhan_pribadi_detail as memerlukan_kebutuhan_pribadi_detail_asal,memiliki_nyeri_kronis as memiliki_nyeri_kronis_asal,memiliki_nyeri_kronis_detail as memiliki_nyeri_kronis_detail_asal,memerlukan_edukasi as memerlukan_edukasi_asal,memerlukan_edukasi_detail as memerlukan_edukasi_detail_asal,memerlukan_ket_khusus as memerlukan_ket_khusus_asal,memerlukan_ket_khusus_detail as memerlukan_ket_khusus_detail_asal
				FROM tranap_discarge_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function list_template_discarge(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_discarge H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	//REKON
	function get_data_rekon_obat($pendaftaran_id_ranap){
		$q="SELECT *
			 FROM tranap_rekon_obat WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		// print_r($q);exit;
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_rekon_obat_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_rekon_obat WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_rekon_obat_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_rekon_obat_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_rekon_obat H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_rekon_obat_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				tanggal_masuk_rs as tanggal_masuk_rs_asal,obat_sebelum_masuk as obat_sebelum_masuk_asal,obat_alergi as obat_alergi_asal,fisik_serahkan as fisik_serahkan_asal,tanggal_serahkan as tanggal_serahkan_asal,ket_serahkan as ket_serahkan_asal,hasil_rekon as hasil_rekon_asal,intruksi as intruksi_asal,obat_dibawa_pulang as obat_dibawa_pulang_asal,petugas_id as petugas_id_asal,ttd_in_pasien as ttd_in_pasien_asal,ttd_in_pasien_nama as ttd_in_pasien_nama_asal,ttd_in_pasien_tanggal as ttd_in_pasien_tanggal_asal,ttd_out_pasien as ttd_out_pasien_asal,ttd_out_pasien_nama as ttd_out_pasien_nama_asal,ttd_out_pasien_tanggal as ttd_out_pasien_tanggal_asal,ttd_petugas_in as ttd_petugas_in_asal,ttd_perugas_in_tanggal as ttd_perugas_in_tanggal_asal,ttd_petugas_out as ttd_petugas_out_asal,ttd_perugas_out_tanggal as ttd_perugas_out_tanggal_asal
				FROM tranap_rekon_obat_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function list_template_rekon_obat(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_rekon_obat H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	//INTRA
	function get_data_intra_hospital($pendaftaran_id_ranap,$st_ranap='1'){
		if ($st_ranap=='1'){
			$q="SELECT *
			 FROM tranap_intra_hospital WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}else{
			$q="SELECT *
			 FROM tranap_intra_hospital WHERE pendaftaran_id='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		
		// print_r($q);exit;
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_intra_hospital_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_intra_hospital WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_intra_hospital_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_intra_hospital_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_intra_hospital H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_intra_hospital_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_input as tanggal_input_asal,
				dpjp_utama as dpjp_utama_asal,dpjp_pendamping as dpjp_pendamping_asal,idunit_asal as idunit_asal_asal,idunit_tujuan as idunit_tujuan_asal,tanggal_pindah as tanggal_pindah_asal,alasan_masuk as alasan_masuk_asal,diagnosa_masuk as diagnosa_masuk_asal,diagnosa_sekarang as diagnosa_sekarang_asal,keadaan_umum as keadaan_umum_asal,kesadaran as kesadaran_asal,keluhan as keluhan_asal,riwayat_penyakit as riwayat_penyakit_asal,alasan_transfer as alasan_transfer_asal,tingkat_kesadaran as tingkat_kesadaran_asal,nadi as nadi_asal,nafas as nafas_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,suhu as suhu_asal,spo2 as spo2_asal,pemeriksaan_diagnosis as pemeriksaan_diagnosis_asal,pemeriksaan_diagnosis_ket as pemeriksaan_diagnosis_ket_asal,tindakan_medis_sudah as tindakan_medis_sudah_asal,infus as infus_asal,obat_injeksi as obat_injeksi_asal,obat_oral as obat_oral_asal,obat_lain as obat_lain_asal,ppa_penyerah as ppa_penyerah_asal,ppa_penerima as ppa_penerima_asal,st_terima as st_terima_asal,tanggal_terima as tanggal_terima_asal,riwayat_alergi as riwayat_alergi_asal
				FROM tranap_intra_hospital_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function list_template_intra_hospital(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_intra_hospital H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	//TF
	function get_data_tf_hospital($pendaftaran_id_ranap,$st_ranap='1'){
		if ($st_ranap=='1'){
			$q="SELECT *
			 FROM tranap_tf_hospital WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}else{
			$q="SELECT *
			 FROM tranap_tf_hospital WHERE pendaftaran_id='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		
		// print_r($q);exit;
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_tf_hospital_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_tf_hospital WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_tf_hospital_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_tf_hospital_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_tf_hospital H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_tf_hospital_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_input as tanggal_input_asal,
				tujuan_rs as tujuan_rs_asal,tujuan_detail as tujuan_detail_asal,staff_kontak as staff_kontak_asal,tanggal_kontak as tanggal_kontak_asal,staff_terima as staff_terima_asal,notelepon_kontak as notelepon_kontak_asal,ambulance_berangkat as ambulance_berangkat_asal,ambulance_tiba as ambulance_tiba_asal,alasan_merujuk as alasan_merujuk_asal,keterangan_merujuk as keterangan_merujuk_asal,diagnosa_medis as diagnosa_medis_asal,iddokter_merujuk as iddokter_merujuk_asal,riwayat_alergi as riwayat_alergi_asal,alergi_detail as alergi_detail_asal,pengobatan as pengobatan_asal,pengobatan_detail as pengobatan_detail_asal,riwayat_penyakit as riwayat_penyakit_asal,riwayat_penyakit_detail as riwayat_penyakit_detail_asal,tindakan as tindakan_asal,intake_oral as intake_oral_asal,gcs_eye as gcs_eye_asal,gcs_voice as gcs_voice_asal,gcs_motion as gcs_motion_asal,pupil_1 as pupil_1_asal,pupil_2 as pupil_2_asal,reflex_cahaya_1 as reflex_cahaya_1_asal,reflex_cahaya_2 as reflex_cahaya_2_asal,nadi as nadi_asal,nafas as nafas_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,suhu as suhu_asal,spo2 as spo2_asal,peralatan_medis as peralatan_medis_asal,peralatan_medis_detail_id as peralatan_medis_detail_id_asal,peralatan_medis_detail as peralatan_medis_detail_asal,perawatan_lanjutan as perawatan_lanjutan_asal,kejadian_klinis as kejadian_klinis_asal,waktu_serah_terima as waktu_serah_terima_asal,staff_perujuk as staff_perujuk_asal,staff_penerima as staff_penerima_asal,ttd_penerima as ttd_penerima_asal,ttd_tanggal as ttd_tanggal_asal
				FROM tranap_tf_hospital_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function list_template_tf_hospital(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_tf_hospital H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	//IK
	function get_data_implementasi_kep($pendaftaran_id_ranap,$st_ranap='1'){
		if ($st_ranap=='1'){
			$q="SELECT *
			 FROM tranap_implementasi_kep WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND st_ranap='$st_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}else{
			$q="SELECT *
			 FROM tranap_implementasi_kep WHERE pendaftaran_id='$pendaftaran_id_ranap' AND st_ranap='$st_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		
		// print_r($q);exit;
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_implementasi_kep_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_implementasi_kep WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_implementasi_kep_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_implementasi_kep_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_implementasi_kep H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_implementasi_kep_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_input as tanggal_input_asal,
				tujuan_rs as tujuan_rs_asal,tujuan_detail as tujuan_detail_asal,staff_kontak as staff_kontak_asal,tanggal_kontak as tanggal_kontak_asal,staff_terima as staff_terima_asal,notelepon_kontak as notelepon_kontak_asal,ambulance_berangkat as ambulance_berangkat_asal,ambulance_tiba as ambulance_tiba_asal,alasan_merujuk as alasan_merujuk_asal,keterangan_merujuk as keterangan_merujuk_asal,diagnosa_medis as diagnosa_medis_asal,iddokter_merujuk as iddokter_merujuk_asal,riwayat_alergi as riwayat_alergi_asal,alergi_detail as alergi_detail_asal,pengobatan as pengobatan_asal,pengobatan_detail as pengobatan_detail_asal,riwayat_penyakit as riwayat_penyakit_asal,riwayat_penyakit_detail as riwayat_penyakit_detail_asal,tindakan as tindakan_asal,intake_oral as intake_oral_asal,gcs_eye as gcs_eye_asal,gcs_voice as gcs_voice_asal,gcs_motion as gcs_motion_asal,pupil_1 as pupil_1_asal,pupil_2 as pupil_2_asal,reflex_cahaya_1 as reflex_cahaya_1_asal,reflex_cahaya_2 as reflex_cahaya_2_asal,nadi as nadi_asal,nafas as nafas_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,suhu as suhu_asal,spo2 as spo2_asal,peralatan_medis as peralatan_medis_asal,peralatan_medis_detail_id as peralatan_medis_detail_id_asal,peralatan_medis_detail as peralatan_medis_detail_asal,perawatan_lanjutan as perawatan_lanjutan_asal,kejadian_klinis as kejadian_klinis_asal,waktu_serah_terima as waktu_serah_terima_asal,staff_perujuk as staff_perujuk_asal,staff_penerima as staff_penerima_asal,ttd_penerima as ttd_penerima_asal,ttd_tanggal as ttd_tanggal_asal
				FROM tranap_implementasi_kep_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function list_template_implementasi_kep(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_implementasi_kep H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	//TIMBANG
	function get_data_timbang($pendaftaran_id_ranap){
		$q="SELECT *
			 FROM tranap_timbang WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		// print_r($q);exit;
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_timbang_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_timbang WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_timbang_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_timbang_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_timbang H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_timbang_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_input as tanggal_input_asal,
				tanggal_terima as tanggal_terima_asal,shift as shift_asal,staff_penyerah as staff_penyerah_asal,staff_penerima as staff_penerima_asal,keadaan as keadaan_asal,intervensi_telah as intervensi_telah_asal,intervensi_belum as intervensi_belum_asal,pesan_khusus as pesan_khusus_asal,st_terima as st_terima_asal,tgl_terima as tgl_terima_asal,ppa_terima as ppa_terima_asal
				FROM tranap_timbang_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function list_template_timbang(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_timbang H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	//IC
	function list_template_tindakan_anestesi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_tindakan_anestesi H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function setting_tindakan_anestesi(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_tindakan_anestesi` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function get_data_tindakan_anestesi($pendaftaran_id,$tipe_rj_ri){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		if ($tipe_rj_ri=='3'){
			$q="SELECT * FROM tranap_tindakan_anestesi WHERE pendaftaran_id_ranap='$pendaftaran_id'  AND tipe_rj_ri='$tipe_rj_ri' AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			
		}else{
			
			$q="SELECT * FROM tranap_tindakan_anestesi WHERE pendaftaran_id='$pendaftaran_id' AND tipe_rj_ri='$tipe_rj_ri' AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	
	function logic_akses_tindakan_anestesi_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_tindakan_anestesi,H.st_lihat as st_lihat_tindakan_anestesi,H.st_edit as st_edit_tindakan_anestesi,H.st_hapus as st_hapus_tindakan_anestesi,H.st_cetak as st_cetak_tindakan_anestesi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_tindakan_anestesi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_tindakan_anestesi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_tindakan_anestesi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_tindakan_anestesi'=>'0',
				'st_input_tindakan_anestesi'=>'0',
				'st_edit_tindakan_anestesi'=>'0',
				'st_hapus_tindakan_anestesi'=>'0',
				'st_cetak_tindakan_anestesi'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	
	function logic_akses_tindakan_anestesi($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_tindakan_anestesi,H.st_lihat as st_lihat_tindakan_anestesi,H.st_edit as st_edit_tindakan_anestesi,H.st_hapus as st_hapus_tindakan_anestesi,H.st_cetak as st_cetak_tindakan_anestesi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_tindakan_anestesi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_tindakan_anestesi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_tindakan_anestesi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_tindakan_anestesi'=>'0',
				'st_input_tindakan_anestesi'=>'0',
				'st_edit_tindakan_anestesi'=>'0',
				'st_hapus_tindakan_anestesi'=>'0',
				'st_cetak_tindakan_anestesi'=>'0',
			);
		}
		return $hasil;
		
	}
	
	function get_data_tindakan_anestesi_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_tindakan_anestesi WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_tindakan_anestesi_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_tindakan_anestesi_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_tindakan_anestesi H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_tindakan_anestesi_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				dokter_pelaksana as dokter_pelaksana_asal,tanggal_informasi as tanggal_informasi_asal,pemberi_info as pemberi_info_asal,pendamping as pendamping_asal,penerima_info as penerima_info_asal,hubungan_id as hubungan_id_asal,st_auto_ttd_user as st_auto_ttd_user_asal,ttd_dokter_pelaksana as ttd_dokter_pelaksana_asal,ttd_penerima_info as ttd_penerima_info_asal,nama as nama_asal,umur as umur_asal,jenis_kelamin as jenis_kelamin_asal,alamat as alamat_asal,no_ktp as no_ktp_asal,hereby as hereby_asal,untuk_tindakan as untuk_tindakan_asal,terhadap as terhadap_asal,nama_pasien as nama_pasien_asal,tanggal_lahir_pasien as tanggal_lahir_pasien_asal,umur_pasien as umur_pasien_asal,jenis_kelamin_pasien as jenis_kelamin_pasien_asal,no_medrec as no_medrec_asal,tanggal_pernyataan as tanggal_pernyataan_asal,yang_menyatakan as yang_menyatakan_asal,ttd_menyatakan as ttd_menyatakan_asal,saksi_rs as saksi_rs_asal,ttd_saksi_rs as ttd_saksi_rs_asal,saksi_keluarga as saksi_keluarga_asal,ttd_saksi as ttd_saksi_asal
				FROM tranap_tindakan_anestesi_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_user_pelaksana_logic_tindakan_anestesi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *FROM setting_tindakan_anestesi_user,setting_tindakan_anestesi_keterangan";
		$data=$this->db->query($q)->row_array();
		if ($data['st_setting_dokter']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}else{
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_tindakan_anestesi_user_list` H
					WHERE H.jenis_akses='1' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_tindakan_anestesi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_tindakan_anestesi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}
		if ($data['st_setting_pemberi_info']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pemberi']=$this->db->query($q)->result();
		}else{
			$data['list_pemberi']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_tindakan_anestesi_user_list` H
					WHERE H.jenis_akses='2' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_tindakan_anestesi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_tindakan_anestesi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pemberi']=$this->db->query($q)->result();
		}
		if ($data['st_setting_petugas_pendamping']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pendamping']=$this->db->query($q)->result();
		}else{
			$data['list_pendamping']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_tindakan_anestesi_user_list` H
					WHERE H.jenis_akses='3' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_tindakan_anestesi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_tindakan_anestesi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pendamping']=$this->db->query($q)->result();
		}
		
		// print_r($data);exit;
		return $data;
	}
	function list_template_risiko_tinggi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_risiko_tinggi H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function setting_risiko_tinggi(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_risiko_tinggi` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function get_data_risiko_tinggi($pendaftaran_id,$tipe_rj_ri){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		if ($tipe_rj_ri=='3'){
			$q="SELECT * FROM tranap_risiko_tinggi WHERE pendaftaran_id_ranap='$pendaftaran_id'  AND tipe_rj_ri='$tipe_rj_ri' AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			
		}else{
			
			$q="SELECT * FROM tranap_risiko_tinggi WHERE pendaftaran_id='$pendaftaran_id' AND tipe_rj_ri='$tipe_rj_ri' AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	
	function logic_akses_risiko_tinggi_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_risiko_tinggi,H.st_lihat as st_lihat_risiko_tinggi,H.st_edit as st_edit_risiko_tinggi,H.st_hapus as st_hapus_risiko_tinggi,H.st_cetak as st_cetak_risiko_tinggi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_risiko_tinggi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_risiko_tinggi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_risiko_tinggi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_risiko_tinggi'=>'0',
				'st_input_risiko_tinggi'=>'0',
				'st_edit_risiko_tinggi'=>'0',
				'st_hapus_risiko_tinggi'=>'0',
				'st_cetak_risiko_tinggi'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	
	function logic_akses_risiko_tinggi($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_risiko_tinggi,H.st_lihat as st_lihat_risiko_tinggi,H.st_edit as st_edit_risiko_tinggi,H.st_hapus as st_hapus_risiko_tinggi,H.st_cetak as st_cetak_risiko_tinggi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_risiko_tinggi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_risiko_tinggi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_risiko_tinggi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_risiko_tinggi'=>'0',
				'st_input_risiko_tinggi'=>'0',
				'st_edit_risiko_tinggi'=>'0',
				'st_hapus_risiko_tinggi'=>'0',
				'st_cetak_risiko_tinggi'=>'0',
			);
		}
		return $hasil;
		
	}
	
	function get_data_risiko_tinggi_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_risiko_tinggi WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_risiko_tinggi_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_risiko_tinggi_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_risiko_tinggi H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_risiko_tinggi_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				dokter_pelaksana as dokter_pelaksana_asal,tanggal_informasi as tanggal_informasi_asal,pemberi_info as pemberi_info_asal,pendamping as pendamping_asal,penerima_info as penerima_info_asal,hubungan_id as hubungan_id_asal,st_auto_ttd_user as st_auto_ttd_user_asal,ttd_dokter_pelaksana as ttd_dokter_pelaksana_asal,ttd_penerima_info as ttd_penerima_info_asal,nama as nama_asal,umur as umur_asal,jenis_kelamin as jenis_kelamin_asal,alamat as alamat_asal,no_ktp as no_ktp_asal,hereby as hereby_asal,untuk_tindakan as untuk_tindakan_asal,terhadap as terhadap_asal,nama_pasien as nama_pasien_asal,tanggal_lahir_pasien as tanggal_lahir_pasien_asal,umur_pasien as umur_pasien_asal,jenis_kelamin_pasien as jenis_kelamin_pasien_asal,no_medrec as no_medrec_asal,tanggal_pernyataan as tanggal_pernyataan_asal,yang_menyatakan as yang_menyatakan_asal,ttd_menyatakan as ttd_menyatakan_asal,saksi_rs as saksi_rs_asal,ttd_saksi_rs as ttd_saksi_rs_asal,saksi_keluarga as saksi_keluarga_asal,ttd_saksi as ttd_saksi_asal
				FROM tranap_risiko_tinggi_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_user_pelaksana_logic_risiko_tinggi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *FROM setting_risiko_tinggi_user,setting_risiko_tinggi_keterangan";
		$data=$this->db->query($q)->row_array();
		if ($data['st_setting_dokter']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}else{
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_risiko_tinggi_user_list` H
					WHERE H.jenis_akses='1' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_risiko_tinggi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_risiko_tinggi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}
		if ($data['st_setting_pemberi_info']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pemberi']=$this->db->query($q)->result();
		}else{
			$data['list_pemberi']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_risiko_tinggi_user_list` H
					WHERE H.jenis_akses='2' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_risiko_tinggi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_risiko_tinggi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pemberi']=$this->db->query($q)->result();
		}
		if ($data['st_setting_petugas_pendamping']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pendamping']=$this->db->query($q)->result();
		}else{
			$data['list_pendamping']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_risiko_tinggi_user_list` H
					WHERE H.jenis_akses='3' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_risiko_tinggi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_risiko_tinggi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pendamping']=$this->db->query($q)->result();
		}
		
		// print_r($data);exit;
		return $data;
	}
	
	//FISIO RI
	
	function get_default_warna_fisio_ri(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT jenis_drawing,warna FROM trawatinap_fisio_default WHERE mppa_id='$login_ppa_id'";
		$data=$this->db->query($q)->row_array();
		if ($data){
			
		}else{
			$data=array(
				'warna'=>'#ff2525',
				'jenis_drawing'=>'bulat',
			);
		}
		return $data;
	}
	function get_data_fisio_ri_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				st_anamnesa as st_anamnesa_asal,keluhan_utama as keluhan_utama_asal,nama_anamnesa as nama_anamnesa_asal,hubungan_anamnesa as hubungan_anamnesa_asal,diagnosa_medis as diagnosa_medis_asal,diagnosa_medis_nama as diagnosa_medis_nama_asal,template_gambar as template_gambar_asal,gambar_tubuh as gambar_tubuh_asal,txt_keterangan as txt_keterangan_asal,gambar_id as gambar_id_asal,nadi as nadi_asal,nafas as nafas_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,suhu as suhu_asal,tinggi_badan as tinggi_badan_asal,berat_badan as berat_badan_asal,nyeri_tekan as nyeri_tekan_asal,nyeri_gerak as nyeri_gerak_asal,nyeri_diam as nyeri_diam_asal,palpasi as palpasi_asal,lepas_gerak_sendi as lepas_gerak_sendi_asal,kekuatan_otot as kekuatan_otot_asal,ket_nyeri as ket_nyeri_asal,jenis_nyeri as jenis_nyeri_asal,statis as statis_asal,dinamis as dinamis_asal,kognotif as kognotif_asal,auskultasi as auskultasi_asal,antropometri as antropometri_asal,hasil_pemeriksaan as hasil_pemeriksaan_asal,pemeriksaan_khusus as pemeriksaan_khusus_asal,diagnosa_fisio_nama as diagnosa_fisio_nama_asal,diagnosa_fisio as diagnosa_fisio_asal,impairment as impairment_asal,functional_limitasi as functional_limitasi_asal,disability as disability_asal,total_skor_nyeri as total_skor_nyeri_asal
				FROM trawatinap_fisio_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	// function setting_fisio_ri(){
		// $q="SELECT H.judul_header,H.judul_footer 
			// ,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			// FROM `setting_fisio_ri` H

			// WHERE H.id='1'";
		// return $this->db->query($q)->row_array();
	// }
	function get_data_fisio_ri($pendaftaran_id_ranap){
		$q="SELECT *
			 FROM trawatinap_fisio WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function get_data_fisio_ri_trx($assesmen_id){
		$q="SELECT *
			 FROM trawatinap_fisio WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_fisio_ri_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM trawatinap_fisio_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM trawatinap_fisio H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_fisio_ri(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			FROM `setting_fisio_ri` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function list_template_fisio_ri(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM trawatinap_fisio H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	//ASSESMEN PULANG
	function get_data_assesmen_pulang($pendaftaran_id){
		$q="SELECT *
			 FROM tranap_assesmen_pulang WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	function get_data_assesmen_pulang_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_assesmen_pulang WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_assesmen_pulang_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_assesmen_pulang_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_assesmen_pulang H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_assesmen_pulang_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				alasan as alasan_asal,mobilisasi as mobilisasi_asal,mobilisasi_lain as mobilisasi_lain_asal,alkes as alkes_asal,perawatan as perawatan_asal,perawatan_lain as perawatan_lain_asal,jam_tv as jam_tv_asal,tingkat_kesadaran as tingkat_kesadaran_asal,nadi as nadi_asal,nafas as nafas_asal,spo2 as spo2_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,suhu as suhu_asal,tinggi_badan as tinggi_badan_asal,berat_badan as berat_badan_asal,obat as obat_asal,efek as efek_asal,nyeri as nyeri_asal,batasan_cairan as batasan_cairan_asal,evaluasi as evaluasi_asal,skr as skr_asal,asuransi as asuransi_asal,diserahkan as diserahkan_asal,diterima_pasien_nama as diterima_pasien_nama_asal,diterima_pasien_ttd as diterima_pasien_ttd_asal
				FROM tranap_assesmen_pulang_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_assesmen_pulang(){
		$q="SELECT 
			H.judul_header_ina,H.judul_header_eng,H.judul_footer_ina,H.judul_footer_eng,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.header_pengkajian,footer_pengkajian
			FROM `setting_assesmen_pulang` H

			";
		return $this->db->query($q)->row_array();
	}
	function list_template_assesmen_pulang(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_assesmen_pulang H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function logic_akses_assesmen_pulang($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_assesmen_pulang,H.st_lihat as st_lihat_assesmen_pulang,H.st_edit as st_edit_assesmen_pulang,H.st_hapus as st_hapus_assesmen_pulang,H.st_cetak as st_cetak_assesmen_pulang
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_assesmen_pulang_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_assesmen_pulang_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_assesmen_pulang_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_assesmen_pulang'=>'0',
				'st_input_assesmen_pulang'=>'0',
				'st_edit_assesmen_pulang'=>'0',
				'st_hapus_assesmen_pulang'=>'0',
				'st_cetak_assesmen_pulang'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	//AKSEP
	function get_data_assesmen_askep($pendaftaran_id){
		$q="SELECT *
			 FROM tranap_assesmen_askep WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	function get_data_assesmen_askep_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_assesmen_askep WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_assesmen_askep_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_assesmen_askep_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_assesmen_askep H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_assesmen_askep_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				tanggal_input as tanggal_input_asal,
				diagnosa as diagnosa_asal,tindakan as tindakan_asal,tanggal_bedah as tanggal_bedah_asal,tingkat_kesadaran_awal as tingkat_kesadaran_awal_asal,gcs_e as gcs_e_asal,gcs_m as gcs_m_asal,gcs_v as gcs_v_asal,pupil_1 as pupil_1_asal,pupil_2 as pupil_2_asal,pernafasan as pernafasan_asal,pernafasan_lain as pernafasan_lain_asal,jam_tv as jam_tv_asal,tingkat_kesadaran as tingkat_kesadaran_asal,nadi as nadi_asal,nafas as nafas_asal,spo2 as spo2_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,suhu as suhu_asal,tinggi_badan as tinggi_badan_asal,berat_badan as berat_badan_asal,visite_do as visite_do_asal,visite_da as visite_da_asal,riwayat_penyakit_id as riwayat_penyakit_id_asal,riwayat_penyakit_nama as riwayat_penyakit_nama_asal,riwayat_operasi as riwayat_operasi_asal,riwayat_operasi_ket as riwayat_operasi_ket_asal,pem_lab as pem_lab_asal,hasil_lab as hasil_lab_asal,persiapan_darah as persiapan_darah_asal,jenis_darah as jenis_darah_asal,jumlah_darah as jumlah_darah_asal,alat_invasi as alat_invasi_asal,minum_jamu as minum_jamu_asal,jenis_jamu as jenis_jamu_asal,hasil_rad as hasil_rad_asal,jenis_rad as jenis_rad_asal,lembar_rad as lembar_rad_asal,st_emosional as st_emosional_asal,icu as icu_asal,icu_acc as icu_acc_asal,izin_operasi as izin_operasi_asal,izin_anestesi as izin_anestesi_asal,tiba_dikamar_beda as tiba_dikamar_beda_asal,tingkat_kesadaran_awal_opr as tingkat_kesadaran_awal_opr_asal,gcs_e_opr as gcs_e_opr_asal,gcs_m_opr as gcs_m_opr_asal,gcs_v_opr as gcs_v_opr_asal,pupil_1_opr as pupil_1_opr_asal,pupil_2_opr as pupil_2_opr_asal,pernafasan_opr as pernafasan_opr_asal,pernafasan_lain_opr as pernafasan_lain_opr_asal,perawat_ruangan_id as perawat_ruangan_id_asal,perawat_ruangan_ttd as perawat_ruangan_ttd_asal,perawat_ruangan_bedah_id as perawat_ruangan_bedah_id_asal,perawat_ruangan_bedah_ttd as perawat_ruangan_bedah_ttd_asal,catatan_diagnosa as catatan_diagnosa_asal,respon_mata as respon_mata_asal,respon_verbal as respon_verbal_asal,respon_motorik as respon_motorik_asal,tipe_opr as tipe_opr_asal,jenis_bius as jenis_bius_asal,kesadaran_opr as kesadaran_opr_asal,st_emosi_opr as st_emosi_opr_asal,canul as canul_asal,jenis_operasi as jenis_operasi_asal,posisi_operasi as posisi_operasi_asal,diawasi as diawasi_asal,posisi_lengan as posisi_lengan_asal,kulit as kulit_asal,urine as urine_asal,dipasang_oleh as dipasang_oleh_asal,disinfeksi as disinfeksi_asal,disinfeksi_lain as disinfeksi_lain_asal,posisi_alat as posisi_alat_asal,posisi_alat_lain as posisi_alat_lain_asal,diatermi as diatermi_asal,lokasi_netral as lokasi_netral_asal,dipasang_netral as dipasang_netral_asal,pemeriksaan_kulit as pemeriksaan_kulit_asal,pemeriksaan_kulit_setelah as pemeriksaan_kulit_setelah_asal,pemasangan_warm as pemasangan_warm_asal,jenis_warm as jenis_warm_asal,jam_mulai as jam_mulai_asal,jam_selesai as jam_selesai_asal,pemakaian_implan as pemakaian_implan_asal,jenis_implan as jenis_implan_asal,lokasi_implan as lokasi_implan_asal,drain as drain_asal,jenis_drain as jenis_drain_asal,lokasi_drain as lokasi_drain_asal,irigasi_luka as irigasi_luka_asal,irigasi_luka_dengan as irigasi_luka_dengan_asal,irigasi_luka_lain as irigasi_luka_lain_asal,tampon as tampon_asal,tampon_lokasi as tampon_lokasi_asal,catatan_all as catatan_all_asal,perawat_assisten_id as perawat_assisten_id_asal,perawat_assisten_ttd as perawat_assisten_ttd_asal,perawat_sirkuler_id as perawat_sirkuler_id_asal,perawat_sirkuler_ttd as perawat_sirkuler_ttd_asal,perawat_anestesi_id as perawat_anestesi_id_asal,perawat_anestesi_ttd as perawat_anestesi_ttd_asal,tab_wizard as tab_wizard_asal,jam_tv_opr as jam_tv_opr_asal,tingkat_kesadaran_opr as tingkat_kesadaran_opr_asal,nadi_opr as nadi_opr_asal,nafas_opr as nafas_opr_asal,spo2_opr as spo2_opr_asal,td_sistole_opr as td_sistole_opr_asal,td_diastole_opr as td_diastole_opr_asal,suhu_opr as suhu_opr_asal,tinggi_badan_opr as tinggi_badan_opr_asal,berat_badan_opr as berat_badan_opr_asal,st_tornique as st_tornique_asal,keseimbangan_cairan as keseimbangan_cairan_asal,ruang_pemulihan as ruang_pemulihan_asal,masuk_jam as masuk_jam_asal,keluar_jam as keluar_jam_asal,kembali_ke as kembali_ke_asal,ruangan_lainnya as ruangan_lainnya_asal,keadaan_paska as keadaan_paska_asal,tingkat_kesadaran_paska as tingkat_kesadaran_paska_asal,gcs_e_paska as gcs_e_paska_asal,gcs_m_paska as gcs_m_paska_asal,gcs_v_paska as gcs_v_paska_asal,pupil_1_paska as pupil_1_paska_asal,pupil_2_paska as pupil_2_paska_asal,kulit_datang as kulit_datang_asal,kulit_keluar as kulit_keluar_asal,sirkulasi_paska as sirkulasi_paska_asal,posisi_paska as posisi_paska_asal,pendarahan_paska as pendarahan_paska_asal,lokasi_pendarahan_paska as lokasi_pendarahan_paska_asal,muntah_paska as muntah_paska_asal,mukosa_mulut as mukosa_mulut_asal,jaringan_pa as jaringan_pa_asal,jaringan_pa_dari as jaringan_pa_dari_asal,skrining_nyeri as skrining_nyeri_asal,risko_jatuh_paska as risko_jatuh_paska_asal,memanggil_perawat as memanggil_perawat_asal,perawat as perawat_asal,perawat_datang as perawat_datang_asal,pemberitahuan as pemberitahuan_asal,keterangan_pemberitahuan as keterangan_pemberitahuan_asal,keadaan_ruangan as keadaan_ruangan_asal,gcs_e_ruangan as gcs_e_ruangan_asal,gcs_m_ruangan as gcs_m_ruangan_asal,gcs_v_ruangan as gcs_v_ruangan_asal,pupil_1_ruangan as pupil_1_ruangan_asal,pupil_2_ruangan as pupil_2_ruangan_asal,jam_tv_ruangan as jam_tv_ruangan_asal,tingkat_kesadaran_ruangan as tingkat_kesadaran_ruangan_asal,nadi_ruangan as nadi_ruangan_asal,nafas_ruangan as nafas_ruangan_asal,spo2_ruangan as spo2_ruangan_asal,td_sistole_ruangan as td_sistole_ruangan_asal,td_diastole_ruangan as td_diastole_ruangan_asal,suhu_ruangan as suhu_ruangan_asal,catatan_ruangan as catatan_ruangan_asal,perawat_ruangan_5_id as perawat_ruangan_5_id_asal,perawat_ruangan_5_ttd as perawat_ruangan_5_ttd_asal,perawat_pemullihan_5_id as perawat_pemullihan_5_id_asal,perawat_pemullihan_5_ttd as perawat_pemullihan_5_ttd_asal
				FROM tranap_assesmen_askep_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_assesmen_askep(){
		$q="SELECT 
			H.judul_header_ina,H.judul_header_eng,H.judul_footer_ina,H.judul_footer_eng,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.header_pengkajian,footer_pengkajian
			FROM `setting_assesmen_askep` H

			";
		return $this->db->query($q)->row_array();
	}
	function list_template_assesmen_askep(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_assesmen_askep H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function logic_akses_assesmen_askep($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_assesmen_askep,H.st_lihat as st_lihat_assesmen_askep,H.st_edit as st_edit_assesmen_askep,H.st_hapus as st_hapus_assesmen_askep,H.st_cetak as st_cetak_assesmen_askep
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_assesmen_askep_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_assesmen_askep_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_assesmen_askep_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_assesmen_askep'=>'0',
				'st_input_assesmen_askep'=>'0',
				'st_edit_assesmen_askep'=>'0',
				'st_hapus_assesmen_askep'=>'0',
				'st_cetak_assesmen_askep'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	//CHECKLIST KESELAMATAN
	function get_data_assesmen_keselamatan($pendaftaran_id,$st_ranap){
		$q="SELECT *
			 FROM tranap_assesmen_keselamatan WHERE pendaftaran_id='$pendaftaran_id' AND st_ranap='$st_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	function get_data_assesmen_keselamatan_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_assesmen_keselamatan WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_assesmen_keselamatan_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_assesmen_keselamatan_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_assesmen_keselamatan H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_assesmen_keselamatan_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				tanggal_input as tanggal_input_asal
				
				FROM tranap_assesmen_keselamatan_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_keselamatan(){
		$q="SELECT 
			H.judul_header_ina,H.judul_header_eng,H.judul_footer_ina,H.judul_footer_eng,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.header_pengkajian,footer_pengkajian
			,H.label_sig_in_ina,H.label_sig_in_eng,H.label_time_out_ina,H.label_time_out_eng,H.label_sign_out_ina,H.label_sign_out_eng
			FROM `setting_keselamatan` H

			";
		return $this->db->query($q)->row_array();
	}
	function list_template_assesmen_keselamatan(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_assesmen_keselamatan H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function logic_akses_assesmen_keselamatan($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_assesmen_keselamatan,H.st_lihat as st_lihat_assesmen_keselamatan,H.st_edit as st_edit_assesmen_keselamatan,H.st_hapus as st_hapus_assesmen_keselamatan,H.st_cetak as st_cetak_assesmen_keselamatan
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_keselamatan_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_keselamatan_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_keselamatan_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_assesmen_keselamatan'=>'0',
				'st_input_assesmen_keselamatan'=>'0',
				'st_edit_assesmen_keselamatan'=>'0',
				'st_hapus_assesmen_keselamatan'=>'0',
				'st_cetak_assesmen_keselamatan'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	//LAP BEDAH
	function get_data_lap_bedah($pendaftaran_id,$st_ranap){
		$q="SELECT *
			 FROM tranap_lap_bedah WHERE pendaftaran_id='$pendaftaran_id' AND st_ranap='$st_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	function get_data_lap_bedah_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_lap_bedah WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_lap_bedah_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_lap_bedah_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_lap_bedah H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_lap_bedah_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				template_assesmen_id as template_assesmen_id_asal,nama_template as nama_template_asal,jumlah_template as jumlah_template_asal,tanggal_input as tanggal_input_asal,pendaftaran_id as pendaftaran_id_asal,idpasien as idpasien_asal,created_date as created_date_asal,created_ppa as created_ppa_asal,template_id as template_id_asal,status_assemen as status_assemen_asal,deleted_ppa as deleted_ppa_asal,deleted_date as deleted_date_asal,st_edited as st_edited_asal,alasan_id as alasan_id_asal,keterangan_hapus as keterangan_hapus_asal,alasan_edit_id as alasan_edit_id_asal,keterangan_edit as keterangan_edit_asal,jml_edit as jml_edit_asal,edited_ppa as edited_ppa_asal,edited_date as edited_date_asal,jenis_bedah as jenis_bedah_asal,tanggal_opr as tanggal_opr_asal,ruang_opr as ruang_opr_asal,mulai as mulai_asal,selesai as selesai_asal,dokter_bedah as dokter_bedah_asal,assisten_bedah as assisten_bedah_asal,assisten_bedah_luar as assisten_bedah_luar_asal,perawat_instrumen as perawat_instrumen_asal,da as da_asal,perawat_anestesi as perawat_anestesi_asal,diagnosa_pra as diagnosa_pra_asal,diagnosa_pasca as diagnosa_pasca_asal,tindakan as tindakan_asal,disinfektan as disinfektan_asal,indikasi as indikasi_asal,posisi as posisi_asal,pemasangan_implan as pemasangan_implan_asal,jenis_implan as jenis_implan_asal,dikirim as dikirim_asal,asal_jaringan as asal_jaringan_asal,transfusi_darah as transfusi_darah_asal,jumlah_darah as jumlah_darah_asal,pendarahan as pendarahan_asal,klasifikasi_opr as klasifikasi_opr_asal,jenis_opr as jenis_opr_asal,jenis_anestesi as jenis_anestesi_asal,obat_anestesi as obat_anestesi_asal,macam_sayatan as macam_sayatan_asal,laporan_pemedahan as laporan_pemedahan_asal,komplikasi as komplikasi_asal,intruksi_post as intruksi_post_asal
				
				FROM tranap_lap_bedah_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_lap_bedah(){
		$q="SELECT 
			H.judul_header_ina,H.judul_header_eng,H.judul_footer_ina,H.judul_footer_eng,H.st_edit_catatan,H.lama_edit
			,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.header_pengkajian,footer_pengkajian,catatan_ina
			FROM `setting_lap_bedah` H

			";
		return $this->db->query($q)->row_array();
	}
	function list_template_lap_bedah(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_lap_bedah H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function logic_akses_lap_bedah($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_lap_bedah,H.st_lihat as st_lihat_lap_bedah,H.st_edit as st_edit_lap_bedah,H.st_hapus as st_hapus_lap_bedah,H.st_cetak as st_cetak_lap_bedah
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_lap_bedah_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_lap_bedah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_lap_bedah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_lap_bedah'=>'0',
				'st_input_lap_bedah'=>'0',
				'st_edit_lap_bedah'=>'0',
				'st_hapus_lap_bedah'=>'0',
				'st_cetak_lap_bedah'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	//PRA SEDASI
	function get_data_pra_sedasi($pendaftaran_id){
		$q="SELECT *
			 FROM tranap_pra_sedasi WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	function get_data_pra_sedasi_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_pra_sedasi WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_pra_sedasi_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_pra_sedasi_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_pra_sedasi H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_pra_sedasi_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				tanggal_step_1 as tanggal_step_1_asal,ruang_opr as ruang_opr_asal,jenis_bedah as jenis_bedah_asal,diagnosa as diagnosa_asal,jenis_opr as jenis_opr_asal,subject_penyakit as subject_penyakit_asal,subject_penyakit_nama as subject_penyakit_nama_asal,keterangan_step_1 as keterangan_step_1_asal,gcs_e as gcs_e_asal,gcs_m as gcs_m_asal,gcs_v as gcs_v_asal,nadi as nadi_asal,nafas as nafas_asal,respirasi as respirasi_asal,spo2 as spo2_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,suhu as suhu_asal,tinggi_badan as tinggi_badan_asal,berat_badan as berat_badan_asal,mallampati as mallampati_asal,gigi as gigi_asal,gol_darah as gol_darah_asal,leher as leher_asal,keterangan_obj as keterangan_obj_asal,kardio as kardio_asal,masalah_lain as masalah_lain_asal,lab as lab_asal,ekg as ekg_asal,status_fisik as status_fisik_asal,perencanaan_anestesi as perencanaan_anestesi_asal,perawat_anes_id as perawat_anes_id_asal,perawat_anes_ttd as perawat_anes_ttd_asal,tanggal_step_2 as tanggal_step_2_asal,ruang_opr_2 as ruang_opr_2_asal,jenis_bedah_2 as jenis_bedah_2_asal,da as da_asal,penata_anestesi as penata_anestesi_asal,jam_tv_2 as jam_tv_2_asal,tingkat_kesadaran_2 as tingkat_kesadaran_2_asal,nadi_2 as nadi_2_asal,nafas_2 as nafas_2_asal,spo2_2 as spo2_2_asal,td_sistole_2 as td_sistole_2_asal,td_diastole_2 as td_diastole_2_asal,suhu_2 as suhu_2_asal,cath as cath_asal,keterangan_penilain_induksi as keterangan_penilain_induksi_asal,induksi as induksi_asal,induksi_lain as induksi_lain_asal,ett as ett_asal,teknik as teknik_asal,teknik_ket as teknik_ket_asal,pengaturan_nafas as pengaturan_nafas_asal,pengaturan_nafas_ket as pengaturan_nafas_ket_asal,ventilator as ventilator_asal,ventilator_volume as ventilator_volume_asal,ventilator_rr as ventilator_rr_asal,masalah_durante as masalah_durante_asal,tindakan_durante as tindakan_durante_asal,blok_teknik as blok_teknik_asal,blok_lokasi as blok_lokasi_asal,jenis_obat as jenis_obat_asal,dosis as dosis_asal,perawat_anes_id_2 as perawat_anes_id_2_asal,perawat_anes_ttd_2 as perawat_anes_ttd_2_asal,tanggal_step_3 as tanggal_step_3_asal,ruang_opr_3 as ruang_opr_3_asal,jenis_bedah_3 as jenis_bedah_3_asal,tiba_pemulihan as tiba_pemulihan_asal,petugas_penyerah_id as petugas_penyerah_id_asal,petugas_penyerah_ttd as petugas_penyerah_ttd_asal,petugas_penerima_id as petugas_penerima_id_asal,petugas_penerima_ttd as petugas_penerima_ttd_asal,intruksi_pasca_anestesi as intruksi_pasca_anestesi_asal,tingkat_kesadaran_3 as tingkat_kesadaran_3_asal,td_sistole_3 as td_sistole_3_asal,td_diastole_3 as td_diastole_3_asal,spo2_3 as spo2_3_asal,respirasi_id_3 as respirasi_id_3_asal,respirasi_3 as respirasi_3_asal,total_skor_nyeri as total_skor_nyeri_asal,nama_kajian as nama_kajian_asal,singkatan_kajian as singkatan_kajian_asal,mnyeri_id as mnyeri_id_asal,komplikasi as komplikasi_asal,komplikasi_tindakan as komplikasi_tindakan_asal,metodi_penilain as metodi_penilain_asal,keterangan_penilaian as keterangan_penilaian_asal,lama_pasien as lama_pasien_asal,pindah_jam as pindah_jam_asal,keruang_ranap as keruang_ranap_asal,petugas_penyerah_id_3 as petugas_penyerah_id_3_asal,petugas_penyerah_ttd_3 as petugas_penyerah_ttd_3_asal,petugas_penerima_id_3 as petugas_penerima_id_3_asal,petugas_penerima_ttd_3 as petugas_penerima_ttd_3_asal,intruksi_pasca_anestesi_3 as intruksi_pasca_anestesi_3_asal,observasi as observasi_asal,observasi_posisi as observasi_posisi_asal,puasa as puasa_asal,mual as mual_asal,manajemen_nyeri as manajemen_nyeri_asal,penunjang as penunjang_asal,perawat_anes_id_3 as perawat_anes_id_3_asal,perawat_anes_ttd_3 as perawat_anes_ttd_3_asal
				
				FROM tranap_pra_sedasi_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_pra_sedasi(){
		$q="SELECT 
			H.judul_header_ina,H.judul_header_eng,H.judul_footer_ina,H.judul_footer_eng,H.st_edit_catatan,H.lama_edit
			,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.header_pengkajian,footer_pengkajian,catatan_ina
			FROM `setting_pra_sedasi` H

			";
		return $this->db->query($q)->row_array();
	}
	function list_template_pra_sedasi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_pra_sedasi H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function logic_akses_pra_sedasi($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_pra_sedasi,H.st_lihat as st_lihat_pra_sedasi,H.st_edit as st_edit_pra_sedasi,H.st_hapus as st_hapus_pra_sedasi,H.st_cetak as st_cetak_pra_sedasi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_pra_sedasi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_pra_sedasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_pra_sedasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_pra_sedasi'=>'0',
				'st_input_pra_sedasi'=>'0',
				'st_edit_pra_sedasi'=>'0',
				'st_hapus_pra_sedasi'=>'0',
				'st_cetak_pra_sedasi'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	//RINGKASAN PULANG
	function get_data_ringkasan_pulang($pendaftaran_id){
		$q="SELECT *
			 FROM tranap_ringkasan_pulang WHERE pendaftaran_id_ranap='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	function get_data_ringkasan_pulang_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_ringkasan_pulang WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_ringkasan_pulang_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_ringkasan_pulang_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_ringkasan_pulang H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_ringkasan_pulang_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
			indikasi_rawat as indikasi_rawat_asal,ringkasan as ringkasan_asal,pemeriksaan as pemeriksaan_asal,penunjang as penunjang_asal,konsultasi as konsultasi_asal,therapi as therapi_asal,therapi_pulang as therapi_pulang_asal,komplikasi as komplikasi_asal,prognis as prognis_asal,cara_pulang as cara_pulang_asal,intuksi_fu as intuksi_fu_asal,tanggal_kontrol as tanggal_kontrol_asal,keterangan as keterangan_asal,diagnosa_utama as diagnosa_utama_asal,diagnosa_utama_id as diagnosa_utama_id_asal,dpjp as dpjp_asal,dpjp_ttd as dpjp_ttd_asal
			FROM tranap_ringkasan_pulang_x_his 
			WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_ringkasan_pulang(){
		$q="SELECT 
			H.judul_header_ina,H.judul_footer_ina,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.judul_header_eng,H.judul_footer_eng
			FROM `setting_ringkasan_pulang` H

			";
		return $this->db->query($q)->row_array();
	}
	function setting_label_ringkasan_pulang(){
		$q="SELECT 
			logo,alamat_rs,phone_rs,web_rs,label_identitas_ina,label_identitas_eng,no_reg_ina,no_reg_eng,no_rm_ina,no_rm_eng,nama_pasien_ina,nama_pasien_eng,ttl_ina,ttl_eng,umur_ina,umur_eng,jk_ina,jk_eng,indikasi_rawat_ina,indikasi_rawat_eng,ringkasan_ina,ringkasan_eng,pemeriksaan_ina,pemeriksaan_eng,penunjang_ina,penunjang_eng,konsultasi_ina,konsultasi_eng,therapi_ina,therapi_eng,therapi_pulang_ina,therapi_pulang_eng,komplikasi_ina,komplikasi_eng,prognis_ina,prognis_eng,cara_pulang_ina,cara_pulang_eng,intuksi_fu_ina,intuksi_fu_eng,keterangan_ina,keterangan_eng,diagnosa_utama_ina,diagnosa_utama_eng,diagnosa_tambahan_ina,diagnosa_tambahan_eng,prosedure_ina,prosedure_eng,icd_10_ina,icd_10_eng,icd_9_ina,icd_9_eng,dpjp_ina,dpjp_eng,keluarga_ina,keluarga_eng,notes_ina,notes_eng
			FROM `setting_ringkasan_pulang_label` LIMIT 1

			";
		return $this->db->query($q)->row_array();
	}
	function list_template_ringkasan_pulang(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_ringkasan_pulang H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function logic_akses_ringkasan_pulang($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_ringkasan_pulang,H.st_lihat as st_lihat_ringkasan_pulang,H.st_edit as st_edit_ringkasan_pulang,H.st_hapus as st_hapus_ringkasan_pulang,H.st_cetak as st_cetak_ringkasan_pulang
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_ringkasan_pulang_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_ringkasan_pulang_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_ringkasan_pulang_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_ringkasan_pulang'=>'0',
				'st_input_ringkasan_pulang'=>'0',
				'st_edit_ringkasan_pulang'=>'0',
				'st_hapus_ringkasan_pulang'=>'0',
				'st_cetak_ringkasan_pulang'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	//RINGKASAN PULANG
	function get_data_pra_bedah($pendaftaran_id,$st_ranap='1'){
		if ($st_ranap=='1'){
			$q="SELECT *
			 FROM tranap_pra_bedah WHERE pendaftaran_id_ranap='$pendaftaran_id' AND st_ranap='1'  AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
		}else{
			$q="SELECT *
			 FROM tranap_pra_bedah WHERE pendaftaran_id='$pendaftaran_id' AND st_ranap='0' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
		}
		
			// print_r($q);exit;
			return $hasil;
	}
	function get_data_pra_bedah_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_pra_bedah WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_pra_bedah_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_pra_bedah_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_pra_bedah H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_pra_bedah_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT keluhan_utama as keluhan_utama_asal,
			riwayat_penyakit as riwayat_penyakit_asal,riwayat_penyakit_dahulu as riwayat_penyakit_dahulu_asal,pemeriksaan_fisik as pemeriksaan_fisik_asal,pemeriksaan_penunjang as pemeriksaan_penunjang_asal,indikasi_ranap as indikasi_ranap_asal,diagnosa_pra_bedah as diagnosa_pra_bedah_asal,diagnosa_banding as diagnosa_banding_asal,rencana_tindakan as rencana_tindakan_asal,penyulit as penyulit_asal,dpjp as dpjp_asal
			FROM tranap_pra_bedah_x_his 
			WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_pra_bedah(){
		$q="SELECT 
			H.judul_header_ina,H.judul_footer_ina,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.judul_header_eng,H.judul_footer_eng
			FROM `setting_pra_bedah` H

			";
		return $this->db->query($q)->row_array();
	}
	function setting_label_pra_bedah(){
		$q="SELECT 
			logo,alamat_rs,phone_rs,web_rs,label_identitas_ina,label_identitas_eng,no_reg_ina,no_reg_eng,no_rm_ina,no_rm_eng,nama_pasien_ina,nama_pasien_eng,ttl_ina,ttl_eng,umur_ina,umur_eng,jk_ina,jk_eng,indikasi_rawat_ina,indikasi_rawat_eng,ringkasan_ina,ringkasan_eng,pemeriksaan_ina,pemeriksaan_eng,penunjang_ina,penunjang_eng,konsultasi_ina,konsultasi_eng,therapi_ina,therapi_eng,therapi_pulang_ina,therapi_pulang_eng,komplikasi_ina,komplikasi_eng,prognis_ina,prognis_eng,cara_pulang_ina,cara_pulang_eng,intuksi_fu_ina,intuksi_fu_eng,keterangan_ina,keterangan_eng,diagnosa_utama_ina,diagnosa_utama_eng,diagnosa_tambahan_ina,diagnosa_tambahan_eng,prosedure_ina,prosedure_eng,icd_10_ina,icd_10_eng,icd_9_ina,icd_9_eng,dpjp_ina,dpjp_eng,keluarga_ina,keluarga_eng,notes_ina,notes_eng
			FROM `setting_pra_bedah_label` LIMIT 1

			";
		return $this->db->query($q)->row_array();
	}
	function list_template_pra_bedah(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_pra_bedah H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function logic_akses_pra_bedah($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_pra_bedah,H.st_lihat as st_lihat_pra_bedah,H.st_edit as st_edit_pra_bedah,H.st_hapus as st_hapus_pra_bedah,H.st_cetak as st_cetak_pra_bedah
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_pra_bedah_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_pra_bedah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_pra_bedah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_pra_bedah'=>'0',
				'st_input_pra_bedah'=>'0',
				'st_edit_pra_bedah'=>'0',
				'st_hapus_pra_bedah'=>'0',
				'st_cetak_pra_bedah'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	//APS
	function get_data_aps($pendaftaran_id,$st_ranap='1'){
		if ($st_ranap=='1'){
			$q="SELECT *
			 FROM tranap_aps WHERE pendaftaran_id_ranap='$pendaftaran_id' AND st_ranap='1'  AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
		}else{
			$q="SELECT *
			 FROM tranap_aps WHERE pendaftaran_id='$pendaftaran_id' AND st_ranap='0' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
		}
		
			// print_r($q);exit;
			return $hasil;
	}
	function get_data_aps_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_aps WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_aps_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_aps_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_aps H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_aps_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
			pilih_ttd_id as pilih_ttd_id_asal,nama_ttd as nama_ttd_asal,ttl_ttd as ttl_ttd_asal,umur_ttd as umur_ttd_asal,alamat_ttd as alamat_ttd_asal,hubungan_ttd as hubungan_ttd_asal,atas_permintan as atas_permintan_asal,ttd_pernyataan as ttd_pernyataan_asal,nip_petugas as nip_petugas_asal,petugas_id as petugas_id_asal,tandatangan_pembuat as tandatangan_pembuat_asal,saksi_kel as saksi_kel_asal,saksi_kel_ttd

			FROM tranap_aps_his 
			WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_aps(){
		$q="SELECT 
			H.judul_header,H.judul_header_eng
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan
			,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.footer_ina,H.footer_eng
			FROM `setting_aps` H

			";
		return $this->db->query($q)->row_array();
	}
	function setting_label_aps(){
		$q="SELECT 
			paragraf_1_ina,paragraf_1_eng,pilih_ttd_ina,pilih_ttd_eng,nama_ttd_ina,nama_ttd_eng,ttl_ttd_ina,ttl_ttd_eng,umur_ttd_ina,umur_ttd_eng,alamat_ttd_ina,alamat_ttd_eng,hubungan_ina,hubungan_eng,atas_permintaan_ina,atas_permintaan_eng,terhadap_ina,terhadap_eng,noreg_ina,noreg_eng,nama_pasien_ina,nama_pasien_eng,nomedrec_ina,nomedrec_eng,ttl_pasien_ina,ttl_pasien_eng,umur_pasien_ina,umur_pasien_eng,jk_ina,jk_eng,paragraf_3_ina,paragraf_3_eng,paragraf_4_ina,paragraf_4_eng,yg_pernyataan_ina,yg_pernyataan_eng,saksi_rs_ina,saksi_rs_eng,saksi_kel_ina,saksi_kel_eng
			FROM `setting_aps_keterangan` LIMIT 1

			";
		return $this->db->query($q)->row_array();
	}
	function list_template_aps(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_aps H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function logic_akses_aps($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_aps,H.st_lihat as st_lihat_aps,H.st_edit as st_edit_aps,H.st_hapus as st_hapus_aps,H.st_cetak as st_cetak_aps
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_aps_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_aps_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_aps_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_aps'=>'0',
				'st_input_aps'=>'0',
				'st_edit_aps'=>'0',
				'st_hapus_aps'=>'0',
				'st_cetak_aps'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	public function cetak_aps($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header,judul_header_eng,footer_ina,footer_eng
			FROM setting_aps  LIMIT 1";
		$data_judul=$this->db->query($q)->row_array();
		
		$q="SELECT 
			*
			FROM setting_aps_keterangan  LIMIT 1";
		$data_format=$this->db->query($q)->row_array();
		// print_r($data_format);exit;
		$q="SELECT 
			H.* 
			FROM tranap_aps H
			WHERE H.assesmen_id='$assesmen_id'";
		$data_assesmen=$this->db->query($q)->row_array();
        $data_assesmen['title']=$data_judul['judul_header'] .' - '.$data_assesmen['assesmen_id'];
        $data_assesmen['isi_surat']=1;
		if ($data_assesmen['st_ranap']=='1'){
			$data_pasien=$this->get_data_pasien($data_assesmen['pendaftaran_id_ranap'],1);
			
		}else{
			
			$data_pasien=$this->get_data_pasien($data_assesmen['pendaftaran_id'],0);
		}
        // $data_assesmen['judul_surat']='';
		// print_r($data);exit;
		$data = array_merge($data_pasien,$data_assesmen,$data_judul,$data_format,$data_header, backend_info());
		
        $html = $this->load->view('Tpendaftaran_ranap_erm/pdf_aps', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
		if ($st_create=='1'){
			$nama_file=$data_assesmen['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	}
	
	//ROHANI
	
	function get_data_rohani($pendaftaran_id,$st_ranap='1'){
		if ($st_ranap=='1'){
			$q="SELECT *
			 FROM tranap_rohani WHERE pendaftaran_id_ranap='$pendaftaran_id' AND st_ranap='1'  AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
		}else{
			$q="SELECT *
			 FROM tranap_rohani WHERE pendaftaran_id='$pendaftaran_id' AND st_ranap='0' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
		}
		
			// print_r($q);exit;
			return $hasil;
	}
	function get_data_rohani_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_rohani WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_rohani_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_rohani_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_rohani H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_rohani_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
			pilih_ttd_id as pilih_ttd_id_asal,nama_ttd as nama_ttd_asal,ttl_ttd as ttl_ttd_asal,umur_ttd as umur_ttd_asal,alamat_ttd as alamat_ttd_asal,hubungan_ttd as hubungan_ttd_asal
			,agama_id as agama_id_asal,ttd_pernyataan as ttd_pernyataan_asal,nip_petugas as nip_petugas_asal,petugas_id as petugas_id_asal,tandatangan_pembuat as tandatangan_pembuat_asal,saksi_kel as saksi_kel_asal,saksi_kel_ttd
			
			FROM tranap_rohani_his 
			WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_rohani(){
		$q="SELECT 
			H.judul_header,H.judul_header_eng
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan
			,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.footer_ina,H.footer_eng
			FROM `setting_rohani` H

			";
		return $this->db->query($q)->row_array();
	}
	function setting_label_rohani(){
		$q="SELECT 
			paragraf_1_ina,paragraf_1_eng,pilih_ttd_ina,pilih_ttd_eng,nama_ttd_ina,nama_ttd_eng,ttl_ttd_ina,ttl_ttd_eng,umur_ttd_ina,umur_ttd_eng,alamat_ttd_ina,alamat_ttd_eng,hubungan_ina,hubungan_eng,atas_permintaan_ina,atas_permintaan_eng,terhadap_ina,terhadap_eng,noreg_ina,noreg_eng,nama_pasien_ina,nama_pasien_eng,nomedrec_ina,nomedrec_eng,ttl_pasien_ina,ttl_pasien_eng,umur_pasien_ina,umur_pasien_eng,jk_ina,jk_eng,paragraf_3_ina,paragraf_3_eng,paragraf_4_ina,paragraf_4_eng,yg_pernyataan_ina,yg_pernyataan_eng,saksi_rs_ina,saksi_rs_eng,saksi_kel_ina,saksi_kel_eng
			,kepercayaan_ina,kepercayaan_eng,yg_menerima_ina,yg_menerima_eng

			FROM `setting_rohani_keterangan` LIMIT 1

			";
		return $this->db->query($q)->row_array();
	}
	function list_template_rohani(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_rohani H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function logic_akses_rohani($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_rohani,H.st_lihat as st_lihat_rohani,H.st_edit as st_edit_rohani,H.st_hapus as st_hapus_rohani,H.st_cetak as st_cetak_rohani
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_rohani_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_rohani_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_rohani_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_rohani'=>'0',
				'st_input_rohani'=>'0',
				'st_edit_rohani'=>'0',
				'st_hapus_rohani'=>'0',
				'st_cetak_rohani'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	public function cetak_rohani($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header,judul_header_eng,footer_ina,footer_eng
			FROM setting_rohani  LIMIT 1";
		$data_judul=$this->db->query($q)->row_array();
		
		$q="SELECT 
			*
			FROM setting_rohani_keterangan  LIMIT 1";
		$data_format=$this->db->query($q)->row_array();
		// print_r($data_format);exit;
		$q="SELECT 
			H.* 
			FROM tranap_rohani H
			WHERE H.assesmen_id='$assesmen_id'";
		$data_assesmen=$this->db->query($q)->row_array();
        $data_assesmen['title']=$data_judul['judul_header'] .' - '.$data_assesmen['assesmen_id'];
        $data_assesmen['isi_surat']=1;
		if ($data_assesmen['st_ranap']=='1'){
			$data_pasien=$this->get_data_pasien($data_assesmen['pendaftaran_id_ranap'],1);
			
		}else{
			
			$data_pasien=$this->get_data_pasien($data_assesmen['pendaftaran_id'],0);
		}
        // $data_assesmen['judul_surat']='';
		// print_r($data);exit;
		$data = array_merge($data_pasien,$data_assesmen,$data_judul,$data_format,$data_header, backend_info());
		
        $html = $this->load->view('Tpendaftaran_ranap_erm/pdf_rohani', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
		if ($st_create=='1'){
			$nama_file=$data_assesmen['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	}

	//PRIVASI
	function get_data_privasi($pendaftaran_id,$st_ranap='1'){
		if ($st_ranap=='1'){
			$q="SELECT *
			 FROM tranap_privasi WHERE pendaftaran_id_ranap='$pendaftaran_id' AND st_ranap='1'  AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
		}else{
			$q="SELECT *
			 FROM tranap_privasi WHERE pendaftaran_id='$pendaftaran_id' AND st_ranap='0' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
		}
		
			// print_r($q);exit;
			return $hasil;
	}
	function get_data_privasi_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_privasi WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_privasi_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_privasi_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_privasi H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_privasi_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
			pilih_ttd_id as pilih_ttd_id_asal,nama_ttd as nama_ttd_asal,ttl_ttd as ttl_ttd_asal,umur_ttd as umur_ttd_asal,alamat_ttd as alamat_ttd_asal,hubungan_ttd as hubungan_ttd_asal
			,hereby as hereby_asal,izin as izin_asal,ttd_pernyataan as ttd_pernyataan_asal,nip_petugas as nip_petugas_asal,petugas_id as petugas_id_asal,tandatangan_pembuat as tandatangan_pembuat_asal,saksi_kel as saksi_kel_asal,saksi_kel_ttd
			
			FROM tranap_privasi_his 
			WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_privasi(){
		$q="SELECT 
			H.judul_header,H.judul_header_eng
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan
			,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.footer_ina,H.footer_eng
			FROM `setting_privasi` H

			";
		return $this->db->query($q)->row_array();
	}
	function setting_label_privasi(){
		$q="SELECT 
			*

			FROM `setting_privasi_keterangan` LIMIT 1

			";
		return $this->db->query($q)->row_array();
	}
	function list_template_privasi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_privasi H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function logic_akses_privasi($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_privasi,H.st_lihat as st_lihat_privasi,H.st_edit as st_edit_privasi,H.st_hapus as st_hapus_privasi,H.st_cetak as st_cetak_privasi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_privasi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_privasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_privasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_privasi'=>'0',
				'st_input_privasi'=>'0',
				'st_edit_privasi'=>'0',
				'st_hapus_privasi'=>'0',
				'st_cetak_privasi'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	public function cetak_privasi($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header,judul_header_eng,footer_ina,footer_eng
			FROM setting_privasi  LIMIT 1";
		$data_judul=$this->db->query($q)->row_array();
		
		$q="SELECT 
			*
			FROM setting_privasi_keterangan  LIMIT 1";
		$data_format=$this->db->query($q)->row_array();
		// print_r($data_format);exit;
		$q="SELECT 
			H.* 
			FROM tranap_privasi H
			WHERE H.assesmen_id='$assesmen_id'";
		$data_assesmen=$this->db->query($q)->row_array();
        $data_assesmen['title']=$data_judul['judul_header'] .' - '.$data_assesmen['assesmen_id'];
        $data_assesmen['isi_surat']=1;
		if ($data_assesmen['st_ranap']=='1'){
			$data_pasien=$this->get_data_pasien($data_assesmen['pendaftaran_id_ranap'],1);
			
		}else{
			
			$data_pasien=$this->get_data_pasien($data_assesmen['pendaftaran_id'],0);
		}
        // $data_assesmen['judul_surat']='';
		// print_r($data);exit;
		$data = array_merge($data_pasien,$data_assesmen,$data_judul,$data_format,$data_header, backend_info());
		
        $html = $this->load->view('Tpendaftaran_ranap_erm/pdf_privasi', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
		if ($st_create=='1'){
			$nama_file=$data_assesmen['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	}
	function get_data_pasien($pendaftaran_id,$st_ranap){
		if ($st_ranap=='1'){
			$q="
				SELECT H.nopendaftaran,H.no_medrec,H.namapasien,J.ref as jk,H.tanggal_lahir 
				,H.umurhari,H.umurbulan,H.umurtahun,H.alamatpasien
				FROM trawatinap_pendaftaran H
				LEFT JOIN merm_referensi J ON J.nilai=H.jenis_kelamin AND J.ref_head_id='1'
				WHERE H.id='$pendaftaran_id'
			";
		}else{
			$q="
				SELECT H.nopendaftaran,H.no_medrec,H.namapasien,J.ref as jk,H.tanggal_lahir 
				,H.umurhari,H.umurbulan,H.umurtahun,H.alamatpasien
				FROM tpoliklinik_pendaftaran H
				LEFT JOIN merm_referensi J ON J.nilai=H.jenis_kelamin AND J.ref_head_id='1'
				WHERE H.id='$pendaftaran_id'
			";
		}
		return $this->db->query($q)->row_array();
	}
	//LAP BEDAH
	function get_data_info_ods($pendaftaran_id,$st_ranap){
		$q="SELECT *
			 FROM tranap_info_ods WHERE pendaftaran_id='$pendaftaran_id' AND st_ranap='$st_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	function get_data_info_ods_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_info_ods WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_info_ods_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_info_ods_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_info_ods H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_info_ods_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_input as tanggal_input_asal,
				dpjp_ppa as dpjp_ppa_asal,rencana_bedah as rencana_bedah_asal,tanggal_opr as tanggal_opr_asal,mulai as mulai_asal,jenis_bius as jenis_bius_asal,pengencer_darah as pengencer_darah_asal,detail_pengencer as detail_pengencer_asal
				,jenis_pasien as jenis_pasien_asal,informasi as informasi_asal,puasa_mulai as puasa_mulai_asal

				FROM tranap_info_ods_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_info_ods(){
		$q="SELECT 
			H.judul_header as judul_header_ina,H.judul_header_eng,H.footer_ina as judul_footer_ina,H.footer_eng as judul_footer_eng,H.st_edit_catatan,H.lama_edit
			,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi
			,H.orang_duplikasi
			FROM `setting_info_ods` H

			";
		return $this->db->query($q)->row_array();
	}
	function setting_info_ods_label(){
		$q="SELECT 
			logo,alamat_rs,phone_rs,web_rs,label_identitas_ina,label_identitas_eng,no_reg_ina,no_reg_eng,no_rm_ina,no_rm_eng,nama_pasien_ina,nama_pasien_eng,ttl_ina,ttl_eng,umur_ina,umur_eng,jk_ina,jk_eng,paragraf_1_ina,paragraf_1_eng,dpjp_ina,dpjp_eng,rencana_ina,rencana_eng,tanggal_tindakan_ina,tanggal_tindakan_eng,waktu_tindakan_ina,waktu_tindakan_eng,jenis_bius_ina,jenis_bius_eng,minum_ina,minum_eng,jika_ya_ina,jika_ya_eng,info_ina,info_eng,paragraf_2_ina,paragraf_2_eng,paragraf_3_ina,paragraf_3_eng,paragraf_4_ina,paragraf_4_eng,label_pasien_ina,label_pasien_eng,label_info_ina,label_info_eng,label_puasa_ina,label_puasa_eng,paragraf_5_ina,paragraf_5_eng,paragraf_dok_ina,paragraf_dok_eng,label_status_ina,label_status_eng,label_ket_ina,label_ket_eng,paragraf_6_ina,paragraf_6_eng,paragraf_7_ina,paragraf_7_eng,label_catatan_ina,label_catatan_eng,content_catatan_ina,content_catatan_eng,label_keluarga_ina,label_keluarga_eng
			,label_petugas_ina,label_petugas_eng
			FROM `setting_info_ods_label` LIMIT 1

			";
		return $this->db->query($q)->row_array();
	}
	function setting_invasif_label(){
		$q="SELECT 
			
			* FROM `setting_invasif_keterangan` LIMIT 1

			";
		return $this->db->query($q)->row_array();
	}
	function list_template_info_ods(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_info_ods H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function logic_akses_info_ods($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_info_ods,H.st_lihat as st_lihat_info_ods,H.st_edit as st_edit_info_ods,H.st_hapus as st_hapus_info_ods,H.st_cetak as st_cetak_info_ods
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_info_ods_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_info_ods_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_info_ods_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_info_ods'=>'0',
				'st_input_info_ods'=>'0',
				'st_edit_info_ods'=>'0',
				'st_hapus_info_ods'=>'0',
				'st_cetak_info_ods'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	//TF DARAH
	function list_template_tf_darah(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_tf_darah H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function setting_tf_darah(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_tf_darah` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function get_data_tf_darah($pendaftaran_id,$tipe_rj_ri){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		if ($tipe_rj_ri=='3'){
			$q="SELECT * FROM tranap_tf_darah WHERE pendaftaran_id_ranap='$pendaftaran_id'  AND tipe_rj_ri='$tipe_rj_ri' AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			
		}else{
			
			$q="SELECT * FROM tranap_tf_darah WHERE pendaftaran_id='$pendaftaran_id' AND tipe_rj_ri='$tipe_rj_ri' AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	
	function logic_akses_tf_darah_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_tf_darah,H.st_lihat as st_lihat_tf_darah,H.st_edit as st_edit_tf_darah,H.st_hapus as st_hapus_tf_darah,H.st_cetak as st_cetak_tf_darah
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_tf_darah_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_tf_darah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_tf_darah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_tf_darah'=>'0',
				'st_input_tf_darah'=>'0',
				'st_edit_tf_darah'=>'0',
				'st_hapus_tf_darah'=>'0',
				'st_cetak_tf_darah'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	
	function logic_akses_tf_darah($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_tf_darah,H.st_lihat as st_lihat_tf_darah,H.st_edit as st_edit_tf_darah,H.st_hapus as st_hapus_tf_darah,H.st_cetak as st_cetak_tf_darah
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_tf_darah_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_tf_darah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_tf_darah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_tf_darah'=>'0',
				'st_input_tf_darah'=>'0',
				'st_edit_tf_darah'=>'0',
				'st_hapus_tf_darah'=>'0',
				'st_cetak_tf_darah'=>'0',
			);
		}
		return $hasil;
		
	}
	
	function get_data_tf_darah_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_tf_darah WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_tf_darah_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_tf_darah_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_tf_darah H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_tf_darah_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				dokter_pelaksana as dokter_pelaksana_asal,tanggal_informasi as tanggal_informasi_asal,pemberi_info as pemberi_info_asal,pendamping as pendamping_asal,penerima_info as penerima_info_asal,hubungan_id as hubungan_id_asal,st_auto_ttd_user as st_auto_ttd_user_asal,ttd_dokter_pelaksana as ttd_dokter_pelaksana_asal,ttd_penerima_info as ttd_penerima_info_asal,nama as nama_asal,umur as umur_asal,jenis_kelamin as jenis_kelamin_asal,alamat as alamat_asal,no_ktp as no_ktp_asal,hereby as hereby_asal,untuk_tindakan as untuk_tindakan_asal,terhadap as terhadap_asal,nama_pasien as nama_pasien_asal,tanggal_lahir_pasien as tanggal_lahir_pasien_asal,umur_pasien as umur_pasien_asal,jenis_kelamin_pasien as jenis_kelamin_pasien_asal,no_medrec as no_medrec_asal,tanggal_pernyataan as tanggal_pernyataan_asal,yang_menyatakan as yang_menyatakan_asal,ttd_menyatakan as ttd_menyatakan_asal,saksi_rs as saksi_rs_asal,ttd_saksi_rs as ttd_saksi_rs_asal,saksi_keluarga as saksi_keluarga_asal,ttd_saksi as ttd_saksi_asal
				FROM tranap_tf_darah_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_user_pelaksana_logic_tf_darah(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *FROM setting_tf_darah_user,setting_tf_darah_keterangan";
		$data=$this->db->query($q)->row_array();
		if ($data['st_setting_dokter']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}else{
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_tf_darah_user_list` H
					WHERE H.jenis_akses='1' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_tf_darah_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_tf_darah_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}
		if ($data['st_setting_pemberi_info']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pemberi']=$this->db->query($q)->result();
		}else{
			$data['list_pemberi']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_tf_darah_user_list` H
					WHERE H.jenis_akses='2' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_tf_darah_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_tf_darah_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pemberi']=$this->db->query($q)->result();
		}
		if ($data['st_setting_petugas_pendamping']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pendamping']=$this->db->query($q)->result();
		}else{
			$data['list_pendamping']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_tf_darah_user_list` H
					WHERE H.jenis_akses='3' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_tf_darah_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_tf_darah_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pendamping']=$this->db->query($q)->result();
		}
		
		// print_r($data);exit;
		return $data;
	}
	
	// INVASIF
	//TF DARAH
	function list_template_invasif(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_invasif H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function setting_invasif(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_invasif` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function get_data_invasif($pendaftaran_id,$tipe_rj_ri){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		
		if ($tipe_rj_ri=='1'){//RANAP
			$q="SELECT * FROM tranap_invasif WHERE pendaftaran_id_ranap='$pendaftaran_id'  AND tipe_rj_ri='3' AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			
		}else{
			
			$q="SELECT * FROM tranap_invasif WHERE pendaftaran_id='$pendaftaran_id' AND tipe_rj_ri IN (1,2) AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
			// print_r($tipe_rj_ri);exit;
			// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			return $hasil;
	}
	
	function logic_akses_invasif_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_invasif,H.st_lihat as st_lihat_invasif,H.st_edit as st_edit_invasif,H.st_hapus as st_hapus_invasif,H.st_cetak as st_cetak_invasif
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_invasif_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_invasif_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_invasif_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_invasif'=>'0',
				'st_input_invasif'=>'0',
				'st_edit_invasif'=>'0',
				'st_hapus_invasif'=>'0',
				'st_cetak_invasif'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	
	function logic_akses_invasif($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_invasif,H.st_lihat as st_lihat_invasif,H.st_edit as st_edit_invasif,H.st_hapus as st_hapus_invasif,H.st_cetak as st_cetak_invasif
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_invasif_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_invasif_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_invasif_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_invasif'=>'0',
				'st_input_invasif'=>'0',
				'st_edit_invasif'=>'0',
				'st_hapus_invasif'=>'0',
				'st_cetak_invasif'=>'0',
			);
		}
		return $hasil;
		
	}
	
	function get_data_invasif_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_invasif WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_invasif_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_invasif_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_invasif H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_invasif_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				dokter_pelaksana as dokter_pelaksana_asal,tanggal_informasi as tanggal_informasi_asal,pemberi_info as pemberi_info_asal,pendamping as pendamping_asal,penerima_info as penerima_info_asal,hubungan_id as hubungan_id_asal,st_auto_ttd_user as st_auto_ttd_user_asal,ttd_dokter_pelaksana as ttd_dokter_pelaksana_asal,ttd_penerima_info as ttd_penerima_info_asal,nama as nama_asal,umur as umur_asal,jenis_kelamin as jenis_kelamin_asal,alamat as alamat_asal,no_ktp as no_ktp_asal,hereby as hereby_asal,untuk_tindakan as untuk_tindakan_asal,terhadap as terhadap_asal,nama_pasien as nama_pasien_asal,tanggal_lahir_pasien as tanggal_lahir_pasien_asal,umur_pasien as umur_pasien_asal,jenis_kelamin_pasien as jenis_kelamin_pasien_asal,no_medrec as no_medrec_asal,tanggal_pernyataan as tanggal_pernyataan_asal,yang_menyatakan as yang_menyatakan_asal,ttd_menyatakan as ttd_menyatakan_asal,saksi_rs as saksi_rs_asal,ttd_saksi_rs as ttd_saksi_rs_asal,saksi_keluarga as saksi_keluarga_asal,ttd_saksi as ttd_saksi_asal
				FROM tranap_invasif_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_user_pelaksana_logic_invasif(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *FROM setting_invasif_user,setting_invasif_keterangan";
		$data=$this->db->query($q)->row_array();
		if ($data['st_setting_dokter']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}else{
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_invasif_user_list` H
					WHERE H.jenis_akses='1' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_invasif_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_invasif_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}
		
		// print_r($data);exit;
		return $data;
	}
	
		//PRIVASI
	function get_data_opinion($pendaftaran_id,$st_ranap='1'){
		if ($st_ranap=='1'){
			$q="SELECT *
			 FROM tranap_opinion WHERE pendaftaran_id_ranap='$pendaftaran_id' AND st_ranap='1'  AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
		}else{
			$q="SELECT *
			 FROM tranap_opinion WHERE pendaftaran_id='$pendaftaran_id' AND st_ranap='0' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
		}
		
			// print_r($q);exit;
			return $hasil;
	}
	function get_data_opinion_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_opinion WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_opinion_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_opinion_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_opinion H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_opinion_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
			pilih_ttd_id as pilih_ttd_id_asal,nama_ttd as nama_ttd_asal,ttl_ttd as ttl_ttd_asal,umur_ttd as umur_ttd_asal,alamat_ttd as alamat_ttd_asal,hubungan_ttd as hubungan_ttd_asal
			,hereby as hereby_asal,ttd_pernyataan as ttd_pernyataan_asal,nip_petugas as nip_petugas_asal,petugas_id as petugas_id_asal,tandatangan_pembuat as tandatangan_pembuat_asal,saksi_kel as saksi_kel_asal,saksi_kel_ttd
			
			FROM tranap_opinion_his 
			WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_opinion(){
		$q="SELECT 
			H.judul_header,H.judul_header_eng
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan
			,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.footer_ina,H.footer_eng
			FROM `setting_opinion` H

			";
		return $this->db->query($q)->row_array();
	}
	function setting_label_opinion(){
		$q="SELECT 
			*

			FROM `setting_opinion_keterangan` LIMIT 1

			";
		return $this->db->query($q)->row_array();
	}
	function list_template_opinion(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_opinion H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function logic_akses_opinion($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_opinion,H.st_lihat as st_lihat_opinion,H.st_edit as st_edit_opinion,H.st_hapus as st_hapus_opinion,H.st_cetak as st_cetak_opinion
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_opinion_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_opinion_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_opinion_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_opinion'=>'0',
				'st_input_opinion'=>'0',
				'st_edit_opinion'=>'0',
				'st_hapus_opinion'=>'0',
				'st_cetak_opinion'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	public function cetak_opinion($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header,judul_header_eng,footer_ina,footer_eng
			FROM setting_opinion  LIMIT 1";
		$data_judul=$this->db->query($q)->row_array();
		
		$q="SELECT 
			*
			FROM setting_opinion_keterangan  LIMIT 1";
		$data_format=$this->db->query($q)->row_array();
		// print_r($data_format);exit;
		$q="SELECT 
			H.* 
			FROM tranap_opinion H
			WHERE H.assesmen_id='$assesmen_id'";
		$data_assesmen=$this->db->query($q)->row_array();
        $data_assesmen['title']=$data_judul['judul_header'] .' - '.$data_assesmen['assesmen_id'];
        $data_assesmen['isi_surat']=1;
		if ($data_assesmen['st_ranap']=='1'){
			$data_pasien=$this->get_data_pasien($data_assesmen['pendaftaran_id_ranap'],1);
			
		}else{
			
			$data_pasien=$this->get_data_pasien($data_assesmen['pendaftaran_id'],0);
		}
        // $data_assesmen['judul_surat']='';
		// print_r($data);exit;
		$data = array_merge($data_pasien,$data_assesmen,$data_judul,$data_format,$data_header, backend_info());
		
        $html = $this->load->view('Tpendaftaran_ranap_erm/pdf_opinion', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
		if ($st_create=='1'){
			$nama_file=$data_assesmen['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	}
}
