<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mdistributor_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mdistributor');
        return $query->row();
    }

    public function saveData()
    {
        $this->kode 		= $_POST['kode'];
        $this->nama 		= $_POST['nama'];
        $this->alamat 	= $_POST['alamat'];
        $this->telepon 	= $_POST['telepon'];
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mdistributor', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->kode 		= $_POST['kode'];
        $this->nama 		= $_POST['nama'];
        $this->alamat 	= $_POST['alamat'];
        $this->telepon 	= $_POST['telepon'];
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mdistributor', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mdistributor', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function load_all()
    {
        $query = $this->db->get('mdistributor');
        return $query->result();
    }
}
