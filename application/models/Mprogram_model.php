<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mprogram_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getSpecified($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('mprogram_rka');
        return $query->row();
    }

    public function saveData() {
        $this->nama       = $_POST['nama'];
        $this->urutan     = $_POST['urutan'];
        $this->idprespektif  = $_POST['idprespektif'];
        $this->kode  = $_POST['kode'];
        $this->deskripsi  = $_POST['deskripsi'];
        $this->created_by  = $this->session->userdata('user_id');
        $this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('mprogram_rka', $this)) {
           
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	
    public function updateData() {
        $this->nama       = $_POST['nama'];
        $this->urutan     = $_POST['urutan'];
		$this->idprespektif  = $_POST['idprespektif'];
		$this->kode  = $_POST['kode'];
        $this->deskripsi  = $_POST['deskripsi'];

        $this->edited_by  = $this->session->userdata('user_id');
        $this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mprogram_rka', $this, ['id' => $_POST['id']])) {
            
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id) {
        $this->status = 0;

        $this->deleted_by  = $this->session->userdata('user_id');
        $this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mprogram_rka', $this, ['id' => $id])) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    
}
