<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpoliklinik_tindakan_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getSpecifiedPendaftaran($id)
	{
		$this->db->select('tpoliklinik_pendaftaran.*, mfpasien.*,
    tpoliklinik_pendaftaran.id as idpendaftaran, mfpasien.nama as namapasien,
     mrekanan.nama AS namaperusahaan, mdokter.nama AS namadokter, mpasien_kelompok.nama AS namakelompok,
    tpoliklinik_tindakan.rujukfarmasi, tpoliklinik_tindakan.rujuklaboratorium, tpoliklinik_tindakan.rujukradiologi,
    tpoliklinik_tindakan.rujukfisioterapi,mpoliklinik.nama as namapoli, tkasir.status AS statuskasir');
		$this->db->from('tpoliklinik_pendaftaran');
		$this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien');
		$this->db->join('mrekanan', 'mrekanan.id = mfpasien.rekanan_id', 'LEFT');
		$this->db->join('mdokter', 'mdokter.id = tpoliklinik_pendaftaran.iddokter');
		$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien');
		$this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik');
		$this->db->join('tpoliklinik_tindakan', 'tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id', 'LEFT');
		$this->db->join('tkasir', 'tkasir.idtipe IN (1,2) AND tkasir.idtindakan = tpoliklinik_tindakan.id', 'LEFT');
		$this->db->where('tpoliklinik_pendaftaran.id', $id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_data_pasien($id)
	{
		$sql = "SELECT H.idpasien,H.namapasien,H.umurhari,H.umurbulan,H.umurtahun,H.iddokter from tpoliklinik_pendaftaran H
				WHERE H.id='$id'";
		$query = $this->db->query($sql);
		return $query->row();
	}

	public function getSpecifiedTindakan($id)
	{
		$this->db->where('idpendaftaran', $id);
		$this->db->where('status', 1);
		$query = $this->db->get('tpoliklinik_tindakan');
		return $query->row();
	}

	public function getListUnitPelayanan()
	{
		$this->db->select('munitpelayanan.id, munitpelayanan.nama');
		$this->db->where('munitpelayanan_user.userid', $this->session->userdata('user_id'));
		$this->db->join('munitpelayanan', 'munitpelayanan_user.idunitpelayanan = munitpelayanan.id', 'left');
		$this->db->from('munitpelayanan_user');
		$query = $this->db->get();
		return $query->result();
	}

	public function getDefaultUnitPelayananUser()
	{
		$this->db->select('unitpelayananiddefault as idunitpelayanan');
		$this->db->from('musers');
		$this->db->where('id', $this->session->userdata('user_id'));
		$query = $this->db->get();
		return $query->row_array()['idunitpelayanan'];
	}

	public function getListPelayanan($idtindakan)
	{
		$this->db->select('tpoliklinik_pelayanan.*, mdokter.nama AS namadokter, mtarif_rawatjalan.nama AS namatarif');
		$this->db->join('mdokter', 'mdokter.id = tpoliklinik_pelayanan.iddokter', 'LEFT');
		$this->db->join('mtarif_rawatjalan', 'mtarif_rawatjalan.id = tpoliklinik_pelayanan.idpelayanan');
		$this->db->where('tpoliklinik_pelayanan.idtindakan', $idtindakan);
		$this->db->where('tpoliklinik_pelayanan.status', 1);
		$query = $this->db->get('tpoliklinik_pelayanan');
		return $query->result();
	}

	public function getListObat($idtindakan)
	{
		$this->db->select('tpoliklinik_obat.*, mdata_obat.kode AS kodeobat, mdata_obat.nama AS namaobat, msatuan.nama AS namasatuan');
		$this->db->join('mdata_obat', 'mdata_obat.id = tpoliklinik_obat.idobat');
		$this->db->join('msatuan', 'msatuan.id = mdata_obat.idsatuankecil');
		$this->db->where('tpoliklinik_obat.idtindakan', $idtindakan);
		$this->db->where('tpoliklinik_obat.status', 1);
		$query = $this->db->get('tpoliklinik_obat');
		return $query->result();
	}

	public function getListAlkes($idtindakan)
	{
		$this->db->select('tpoliklinik_alkes.*, mdata_alkes.kode AS kodealkes, mdata_alkes.nama AS namaalkes, msatuan.nama AS namasatuan');
		$this->db->join('mdata_alkes', 'mdata_alkes.id = tpoliklinik_alkes.idalkes');
		$this->db->join('msatuan', 'msatuan.id = mdata_alkes.idsatuankecil');
		$this->db->where('tpoliklinik_alkes.idtindakan', $idtindakan);
		$this->db->where('tpoliklinik_alkes.status', 1);
		$query = $this->db->get('tpoliklinik_alkes');
		return $query->result();
	}

	public function saveData()
	{
		$this->idpendaftaran = $_POST['idpendaftaran'];
		$this->iddokter1 = $_POST['iddokter1'];
		if (isset($_POST['iddokter2'])) {
			$this->iddokter2 = $_POST['iddokter2'];
		}
		if (isset($_POST['iddokter3'])) {
			$this->iddokter3 = $_POST['iddokter3'];
		}
		$this->keluhan = $_POST['keluhan'];
		$this->tinggibadan = $_POST['tinggibadan'];
		$this->suhubadan = $_POST['suhubadan'];
		$this->beratbadan = $_POST['beratbadan'];
		$this->lingkarkepala = $_POST['lingkarkepala'];
		$this->sistole = $_POST['sistole'];
		$this->disastole = $_POST['disastole'];
		$this->keterangan = $_POST['keterangan'];
		$this->statuskasus = (isset($_POST['statuskasus']) ? $_POST['statuskasus'] : 0);
		$this->diagnosa = $_POST['diagnosa'];
		$this->alergiobat = $_POST['alergiobat'];
		$this->tanggalkontrol = $_POST['tanggalkontrol'];
		$this->tindaklanjut = (isset($_POST['tindaklanjut']) ? $_POST['tindaklanjut'] : 0);
		$this->sebabluar = (isset($_POST['sebabluar']) ? $_POST['sebabluar'] : 0);

		if ($this->tindaklanjut == 4) {
			$this->menolakdirawat = (isset($_POST['menolakdirawat']) ? $_POST['menolakdirawat'] : 0);
		}

		if (isset($_POST['rujukfarmasi'])) {
			$this->rujukfarmasi = $_POST['rujukfarmasi'];
			$this->iduser_rujuk_farmasi = $this->session->userdata('user_id');
			$this->tanggal_rujuk_farmasi = date('Y-m-d H:i:s');
		} else {
			$this->rujukfarmasi = 0;
		}

		if (isset($_POST['rujuklaboratorium'])) {
			$this->rujuklaboratorium = $_POST['rujuklaboratorium'];
			$this->iduser_rujuk_laboratorium = $this->session->userdata('user_id');
			$this->tanggal_rujuk_laboratorium = date('Y-m-d H:i:s');
		} else {
			$this->rujuklaboratorium = 0;
		}

		if (isset($_POST['rujukradiologi'])) {
			$this->rujukradiologi = $_POST['rujukradiologi'];
			$this->iduser_rujuk_radiologi = $this->session->userdata('user_id');
			$this->tanggal_rujuk_radiologi = date('Y-m-d H:i:s');
		} else {
			$this->rujukradiologi = 0;
		}

		if (isset($_POST['rujukfisioterapi'])) {
			$this->rujukfisioterapi = $_POST['rujukfisioterapi'];
			$this->iduser_rujuk_fisioterapi = $this->session->userdata('user_id');
			$this->tanggal_rujuk_fisioterapi = date('Y-m-d H:i:s');
		} else {
			$this->rujukfisioterapi = 0;
		}

		$this->iduser_tindakan = $this->session->userdata('user_id');
		$this->tanggal_tindakan = date('Y-m-d H:i:s');

		if ($this->db->insert('tpoliklinik_tindakan', $this)) {
			$idtindakan = $this->db->insert_id();

			$query = $this->db->query("SELECT
              mdokter.id,
              (CASE
                WHEN DATE_FORMAT(tpoliklinik_tindakan.tanggal_tindakan, '%H:%i') < '12:00' THEN
                  mdokter.potonganrspagi
                ELSE mdokter.potonganrssiang
              END) AS potongan_rs,
              mdokter.pajak AS pajak_dokter
            FROM tpoliklinik_tindakan
            JOIN mdokter ON mdokter.id = tpoliklinik_tindakan.iddokter1
            WHERE tpoliklinik_tindakan.id = $idtindakan");
			$dokter = $query->row();

			$tindakan = json_decode($_POST['tindakan-value']);
			foreach ($tindakan as $row) {
				$tindakan = [];
				$tindakan['idtindakan'] = $idtindakan;
				if ($_POST['idtipe'] == 2) {
					$tindakan['iddokter'] = $row[0];
				} else {
					$tindakan['iddokter'] = $this->iddokter1;
				}
				$tindakan['idpelayanan'] = $row[12];
				$tindakan['jasasarana'] = RemoveComma($row[3]);
				$tindakan['jasapelayanan'] = RemoveComma($row[4]);
				$tindakan['jasapelayanan_disc'] = RemoveComma($row[10]);
				$tindakan['bhp'] = RemoveComma($row[5]);
				$tindakan['biayaperawatan'] = RemoveComma($row[6]);
				$tindakan['total'] = RemoveComma($row[7]);
				$tindakan['kuantitas'] = RemoveComma($row[8]);
				$tindakan['totalkeseluruhan'] = RemoveComma($row[11]);

				$tindakan['pot_rs'] = $dokter->potongan_rs;
				$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

				$this->db->insert('tpoliklinik_pelayanan', $tindakan);
			}

			$obat = json_decode($_POST['obat-value']);
			foreach ($obat as $row) {
				$obat = [];
				$obat['idtindakan'] = $idtindakan;
				$obat['idunit'] = $row[9];
				$obat['idobat'] = $row[8];
				$obat['hargadasar'] = RemoveComma($row[3]);
				$obat['margin'] = RemoveComma($row[4]);
				$obat['hargajual'] = RemoveComma($row[5]);
				$obat['kuantitas'] = RemoveComma($row[6]);
				$obat['totalkeseluruhan'] = RemoveComma($row[7]);
				$obat['statusstok'] = $row[10];

				$this->db->insert('tpoliklinik_obat', $obat);
			}

			$alkes = json_decode($_POST['alkes-value']);
			foreach ($alkes as $row) {
				$alkes = [];
				$alkes['idtindakan'] = $idtindakan;
				$alkes['idunit'] = $row[9];
				$alkes['idalkes'] = $row[8];
				$alkes['hargadasar'] = RemoveComma($row[3]);
				$alkes['margin'] = RemoveComma($row[4]);
				$alkes['hargajual'] = RemoveComma($row[5]);
				$alkes['kuantitas'] = RemoveComma($row[6]);
				$alkes['totalkeseluruhan'] = RemoveComma($row[7]);

				$this->db->insert('tpoliklinik_alkes', $alkes);
			}

			if (isset($_POST['idkelompok_diagnosa'])) {
				$kelompokDiagnosa = $_POST['idkelompok_diagnosa'];
				foreach ($kelompokDiagnosa as $idkelompok) {
					$data = [];
					$data['idpendaftaran'] = $_POST['idpendaftaran'];
					$data['idkelompok_diagnosa'] = $idkelompok;

					$this->db->insert('tpoliklinik_diagnosa', $data);
				}
			}

			$this->db->update(
				'tpoliklinik_pendaftaran',
				[
					'idtipepasien' => $_POST['idtipepasien'],
					'iddokter' => $this->iddokter1,
					'statustindakan' => '1'
				],
				[
					'id' => $this->idpendaftaran
				]
			);

			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function updateData()
	{
		$idtindakan = $_POST['id'];

		$this->idpendaftaran = $_POST['idpendaftaran'];
		$this->iddokter1 = $_POST['iddokter1'];
		if (isset($_POST['iddokter2'])) {
			$this->iddokter2 = $_POST['iddokter2'];
		}
		if (isset($_POST['iddokter3'])) {
			$this->iddokter3 = $_POST['iddokter3'];
		}
		$this->keluhan = $_POST['keluhan'];
		$this->tinggibadan = $_POST['tinggibadan'];
		$this->suhubadan = $_POST['suhubadan'];
		$this->beratbadan = $_POST['beratbadan'];
		$this->lingkarkepala = $_POST['lingkarkepala'];
		$this->sistole = $_POST['sistole'];
		$this->disastole = $_POST['disastole'];
		$this->keterangan = $_POST['keterangan'];
		$this->statuskasus = (isset($_POST['statuskasus']) ? $_POST['statuskasus'] : 0);
		$this->diagnosa = $_POST['diagnosa'];
		$this->alergiobat = $_POST['alergiobat'];
		$this->tanggalkontrol = $_POST['tanggalkontrol'];
		$this->tindaklanjut = (isset($_POST['tindaklanjut']) ? $_POST['tindaklanjut'] : 0);
		$this->sebabluar = (isset($_POST['sebabluar']) ? $_POST['sebabluar'] : 0);

		if ($this->tindaklanjut == 4) {
			$this->menolakdirawat = (isset($_POST['menolakdirawat']) ? $_POST['menolakdirawat'] : 0);
		}

		if (isset($_POST['rujukfarmasi'])) {
			$this->rujukfarmasi = $_POST['rujukfarmasi'];
			$this->iduser_rujuk_farmasi = $this->session->userdata('user_id');
			$this->tanggal_rujuk_farmasi = date('Y-m-d H:i:s');
		} else {
			$this->rujukfarmasi = 0;
		}

		if (isset($_POST['rujuklaboratorium'])) {
			$this->rujuklaboratorium = $_POST['rujuklaboratorium'];
			$this->iduser_rujuk_laboratorium = $this->session->userdata('user_id');
			$this->tanggal_rujuk_laboratorium = date('Y-m-d H:i:s');
		} else {
			$this->rujuklaboratorium = 0;
		}

		if (isset($_POST['rujukradiologi'])) {
			$this->rujukradiologi = $_POST['rujukradiologi'];
			$this->iduser_rujuk_radiologi = $this->session->userdata('user_id');
			$this->tanggal_rujuk_radiologi = date('Y-m-d H:i:s');
		} else {
			$this->rujukradiologi = 0;
		}

		if (isset($_POST['rujukfisioterapi'])) {
			$this->rujukfisioterapi = $_POST['rujukfisioterapi'];
			$this->iduser_rujuk_fisioterapi = $this->session->userdata('user_id');
			$this->tanggal_rujuk_fisioterapi = date('Y-m-d H:i:s');
		} else {
			$this->rujukfisioterapi = 0;
		}

		$this->db->where('id', $idtindakan);
		if ($this->db->update('tpoliklinik_tindakan', $this)) {
			$this->db->where('idtindakan', $idtindakan);
			if ($this->db->delete('tpoliklinik_pelayanan')) {
				$tindakan = json_decode($_POST['tindakan-value']);

				$query = $this->db->query("SELECT
                  mdokter.id,
                  (CASE
                    WHEN DATE_FORMAT(tpoliklinik_tindakan.tanggal_tindakan, '%H:%i') < '12:00' THEN
                      mdokter.potonganrspagi
                    ELSE mdokter.potonganrssiang
                  END) AS potongan_rs,
                  mdokter.pajak AS pajak_dokter
                FROM tpoliklinik_tindakan
                JOIN mdokter ON mdokter.id = tpoliklinik_tindakan.iddokter1
                WHERE tpoliklinik_tindakan.id = $idtindakan");
				$dokter = $query->row();

				foreach ($tindakan as $row) {
					$tindakan = [];
					$tindakan['idtindakan'] = $idtindakan;
					if ($_POST['idtipe'] == 2) {
						$tindakan['iddokter'] = $row[0];
					} else {
						$tindakan['iddokter'] = $this->iddokter1;
					}
					$tindakan['idpelayanan'] = $row[12];
					$tindakan['jasasarana'] = RemoveComma($row[3]);
					$tindakan['jasapelayanan'] = RemoveComma($row[4]);
					$tindakan['jasapelayanan_disc'] = RemoveComma($row[10]);
					$tindakan['bhp'] = RemoveComma($row[5]);
					$tindakan['biayaperawatan'] = RemoveComma($row[6]);
					$tindakan['total'] = RemoveComma($row[7]);
					$tindakan['kuantitas'] = RemoveComma($row[8]);
					$tindakan['totalkeseluruhan'] = RemoveComma($row[11]);
					$tindakan['statusverifikasi'] = $row[13];

					$tindakan['pot_rs'] = $dokter->potongan_rs;
					$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

					$this->db->insert('tpoliklinik_pelayanan', $tindakan);
				}
			}

			$this->db->where('idtindakan', $idtindakan);
			if ($this->db->delete('tpoliklinik_obat')) {
				$obat = json_decode($_POST['obat-value']);
				foreach ($obat as $row) {
					$obat = [];
					$obat['idtindakan'] = $idtindakan;
					$obat['idunit'] = $row[9];
					$obat['idobat'] = $row[8];
					$obat['hargadasar'] = RemoveComma($row[3]);
					$obat['margin'] = RemoveComma($row[4]);
					$obat['hargajual'] = RemoveComma($row[5]);
					$obat['kuantitas'] = RemoveComma($row[6]);
					$obat['totalkeseluruhan'] = RemoveComma($row[7]);
					$obat['statusstok'] = $row[10];
					$obat['statusverifikasi'] = $row[11];

					$this->db->insert('tpoliklinik_obat', $obat);
				}
			}

			$this->db->where('idtindakan', $idtindakan);
			if ($this->db->delete('tpoliklinik_alkes')) {
				$alkes = json_decode($_POST['alkes-value']);
				foreach ($alkes as $row) {
					$alkes = [];
					$alkes['idtindakan'] = $idtindakan;
					$alkes['idunit'] = $row[9];
					$alkes['idalkes'] = $row[8];
					$alkes['hargadasar'] = RemoveComma($row[3]);
					$alkes['margin'] = RemoveComma($row[4]);
					$alkes['hargajual'] = RemoveComma($row[5]);
					$alkes['kuantitas'] = RemoveComma($row[6]);
					$alkes['totalkeseluruhan'] = RemoveComma($row[7]);
					$alkes['statusverifikasi'] = $row[10];

					$this->db->insert('tpoliklinik_alkes', $alkes);
				}
			}

			$this->db->where('idpendaftaran', $_POST['idpendaftaran']);
			if ($this->db->delete('tpoliklinik_diagnosa')) {
				if (isset($_POST['idkelompok_diagnosa'])) {
					$kelompokDiagnosa = $_POST['idkelompok_diagnosa'];
					foreach ($kelompokDiagnosa as $idkelompok) {
						$data = [];
						$data['idpendaftaran'] = $_POST['idpendaftaran'];
						$data['idkelompok_diagnosa'] = $idkelompok;

						$this->db->insert('tpoliklinik_diagnosa', $data);
					}
				}
			}

			$this->db->update(
				'tpoliklinik_pendaftaran',
				[
					'idtipepasien' => $_POST['idtipepasien'],
					'iddokter' => $this->iddokter1,
					'statustindakan' => '1'
				],
				[
					'id' => $this->idpendaftaran
				]
			);

			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function softDelete($id)
	{
		$this->status = 0;

		if ($this->db->update('tpoliklinik_tindakan', $this, ['id' => $id])) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function getSebabLuar()
	{
		$this->db->select('id, nama');
		$this->db->where('status', '1');
		$query = $this->db->get('msebab_luar');
		return $query->result();
	}

	public function getKelompokDiagnosa()
	{
		$this->db->select('id, nama');
		$this->db->where('status', '1');
		$query = $this->db->get('mkelompok_diagnosa');
		return $query->result();
	}

	public function getSelectedKelompokDiagnosa($idpendaftaran)
	{
		$this->db->select('GROUP_CONCAT(idkelompok_diagnosa) AS id');
		$this->db->where('idpendaftaran', $idpendaftaran);
		$this->db->order_by('tpoliklinik_diagnosa.id', 'ASC');
		$query = $this->db->get('tpoliklinik_diagnosa');

		// $result = explode(',', $query->row()->id);
		$result = $query->row()->id;
		return $result;
	}
}
