<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mtujuan_farmasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function list_layanan($tujuan_id=''){
		$q="SELECT H.id,CONCAT(H.kode,' - ',H.nama_pelayanan) as pelayanan 
			,CASE WHEN M.pelayanan_id IS NOT NULL THEN 'selected' ELSE '' END as pilih
			FROM antrian_pelayanan H
			LEFT JOIN mtujuan_farmasi M ON M.pelayanan_id=H.id AND M.tujuan_id='$tujuan_id'
			WHERE H.`status`='1' ORDER BY H.urutan";
		return $this->db->query($q)->result();
	}
	public function list_user($tujuan_id=''){
		$q="SELECT H.id,H.`name` as nama_user
			,CASE WHEN M.userid IS NOT NULL THEN 'selected' ELSE '' END as pilih
			FROM musers H
			LEFT JOIN mtujuan_farmasi_user M ON M.userid=H.id AND M.tujuan_id='$tujuan_id'
			WHERE H.`status`='1' ORDER BY H.id";
		return $this->db->query($q)->result();
	}
	
	public function getSpecified($id)
    {
        $q="SELECT 
			*
			from mtujuan_farmasi M
					
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	
    public function saveData()
    {
		// print_r($this->input->post());exit;
        $this->nama_tujuan 	= $_POST['nama_tujuan'];		
        $this->idunit 	= $_POST['idunit'];		
        $this->mtujuan_antrian_id 	= $_POST['mtujuan_antrian_id'];		
        $this->apoteker_id 	= $_POST['apoteker_id'];		
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		$this->status  = 1;

        if ($this->db->insert('mtujuan_farmasi', $this)) {
			$tujuan_id=$this->db->insert_id();
			$userid=$this->input->post('userid');
			
			foreach($userid as $index=>$val){
				$data_user['tujuan_id']=$tujuan_id;
				$data_user['userid']=$val;
				$this->db->insert('mtujuan_farmasi_user',$data_user);
			}
			
            return $tujuan_id;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
		$tujuan_id=$_POST['id'];
        $this->nama_tujuan 	= $_POST['nama_tujuan'];		
        $this->idunit 	= $_POST['idunit'];		
		 $this->mtujuan_antrian_id 	= $_POST['mtujuan_antrian_id'];		
        $this->apoteker_id 	= $_POST['apoteker_id'];
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mtujuan_farmasi', $this, array('id' => $_POST['id']))) {
			$userid=$this->input->post('userid');
			$this->db->where('tujuan_id',$tujuan_id);
			$this->db->delete('mtujuan_farmasi_user');
			foreach($userid as $index=>$val){
				$data_user['tujuan_id']=$tujuan_id;
				$data_user['userid']=$val;
				$this->db->insert('mtujuan_farmasi_user',$data_user);
			}
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function upload_sound($update = false)
    {
        if (!file_exists('assets/upload/sound_antrian')) {
            mkdir('assets/upload/sound_antrian', 0755, true);
        }

        if (isset($_FILES['file_counter'])) {
            if ($_FILES['file_counter']['name'] != '') {
                $config['upload_path'] = './assets/upload/sound_antrian/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'wav|mp3';
				// $config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('file_counter')) {
                    $image_upload = $this->upload->data();
                    $this->file_counter = $image_upload['file_name'];

                    if ($update == true) {
                        $this->remove_sound($this->input->post('id'));
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
			// print_r('TIDAK ADA');exit;
                return true;
            }
					// print_r($this->foto);exit;
        } else {
			// print_r('TIDAK ADA');exit;
            return true;
        }
		
    }
	
    public function softDelete($id)
    {
        $this->status = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mtujuan_farmasi', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mtujuan_farmasi', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
