<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tintra_hospital_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function list_unit_user(){
		$user_id=$this->session->userdata('user_id');
		$q="
			SELECT M.* FROM munitpelayanan_user H
			INNER JOIN munitpelayanan M ON M.id=H.idunitpelayanan
			INNER JOIN munitpelayanan_tujuan T ON T.idunit=M.id
			WHERE H.userid='$user_id' AND M.`status`='1'
		";
		return $this->db->query($q)->result();
	}
	function list_unit(){
		$user_id=$this->session->userdata('user_id');
		$q="
			SELECT M.* FROM munitpelayanan_tujuan H
			INNER JOIN munitpelayanan M ON M.id=H.idunit

		";
		return $this->db->query($q)->result();
	}
	function get_data_intra_hospital($pendaftaran_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT * FROM tpoliklinik_intra_hospital H WHERE H.status_intra_hospital='1' AND H.pendaftaran_id='$pendaftaran_id' AND H.created_ppa='$login_ppa_id'";
		return $this->db->query($q)->row_array();
	}
	function list_dokter($idpoli){
		$q="SELECT M.id,M.nama FROM `mpoliklinik_intra_hospital_dokter` H
				INNER JOIN mdokter M ON M.id=H.iddokter
				WHERE H.idpoliklinik='$idpoli'";
		return $this->db->query($q)->result();
	}
	function list_dokter_all(){
		$q="SELECT M.id,M.nama FROM `mpoliklinik_intra_hospital_dokter` H
				INNER JOIN mdokter M ON M.id=H.iddokter
				";
		return $this->db->query($q)->result();
	}
	function list_poliklinik(){
		$q="SELECT M.id,M.nama FROM mpoliklinik_intra_hospital H INNER JOIN mpoliklinik M ON M.id=H.idpoliklinik";
		return $this->db->query($q)->result();
	}
}
