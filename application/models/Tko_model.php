<?php
defined('BASEPATH') or exit('No direct script access allowed');
// @Muhamad Ali Nurdin

class Tko_model extends CI_Model
{
    public function getStatusLockKasirRanap($idpendaftaran)
    {
        $this->db->select('trawatinap_pendaftaran.statuskasir');
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1');
        $this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id OR (trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 2)');
        $this->db->where('trawatinap_pendaftaran.id', $idpendaftaran);
        $query = $this->db->get('tkamaroperasi_pendaftaran');
        return $query->row();
    }
	public function get_tarif_default($jenis,$kelas){
		$q="SELECT H.id,H.nama,M.nama as kelas,H.idtipe,H.idjenis,D.bhp,D.biayaperawatan,D.jasapelayanan,D.jasasarana,D.total from mtarif_operasi H
			LEFT JOIN mtarif_operasi_detail D ON D.idtarif=H.id
			LEFT JOIN mkelas M ON M.id=D.kelas
			WHERE H.idtipe='3' AND H.`status`='1' AND H.idjenis='$jenis' AND D.kelas='$kelas'";
		return $this->db->query($q)->row('id');

	}
	public function load_tarif_do($jenis_operasi_id,$kelas_tarif_id){
		$q="SELECT H.id,H.nama as nama_tarif,D.jasasarana,D.jasapelayanan,D.bhp,
		D.biayaperawatan,D.total,J.nama as jenis_operasi
		from mtarif_operasi H
			LEFT JOIN mtarif_operasi_detail D ON D.idtarif=H.id AND D.kelas='$kelas_tarif_id'
			LEFT JOIN mjenis_operasi J ON J.id=H.idjenis
			WHERE H.idtipe='3' AND H.idjenis='$jenis_operasi_id' AND D.`status`='1' AND H.`status`='1'";
		$query=$this->db->query($q);
		$result= $query->row();
		if ($result){
			return $result;

		}else{
			return 'error';
		}
	}
	function get_kasir_lock($id){
		$q="SELECT COALESCE(RI.statuskasir,0) as st_lock from tkamaroperasi_pendaftaran H
			LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.idpendaftaran
			WHERE H.id='$id'";
		return $this->db->query($q)->row('st_lock');
	}
	public function get_operasi_ruangan($id){
		$q="SELECT H.id FROM tkamaroperasi_ruangan H WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result=$query->row('id');
		if ($result){
			return $result;
		}else{
			return 0;
		}
	}
	public function load_tarif_ruangan($jenis_operasi_id,$kelas_tarif_id){
		$q="SELECT H.id,H.nama as nama_tarif,D.jasasarana,D.jasapelayanan,D.bhp,
		D.biayaperawatan,D.total,J.nama as jenis_operasi
		from mtarif_operasi H
			LEFT JOIN mtarif_operasi_detail D ON D.idtarif=H.id AND D.kelas='$kelas_tarif_id'
			LEFT JOIN mjenis_operasi J ON J.id=H.idjenis
			WHERE H.idtipe='1' AND H.idjenis='$jenis_operasi_id' AND D.`status`='1' AND H.`status`='1'";
		$query=$this->db->query($q);
		$result= $query->row();
		if ($result){
			return $result;

		}else{
			return 'error';
		}
	}
	public function load_tarif_fullcare($ruang_id,$kelas_tarif_id,$idkelompokpasien){
		$q="SELECT  K.trawatinap_fullcare5,T.nama,D.* from mpasien_kelompok K
			LEFT JOIN mtarif_rawatinap T ON T.id=K.trawatinap_fullcare5 AND T.idruangan='$ruang_id'
			INNER JOIN mtarif_rawatinap_detail D ON D.idtarif=T.id AND D.kelas='$kelas_tarif_id'
			WHERE K.id='$idkelompokpasien'";
		$query=$this->db->query($q);
		$result= $query->row();
		if ($result){
			return $result;

		}else{
			return 'error';
		}
	}
	public function load_tarif_sewa($jenis_operasi_id,$kelas_tarif_id,$alat_id){
		$q="SELECT *FROM mtarif_operasi_sewaalat_detail D
			WHERE D.idtarif='$alat_id' AND D.idjenis='$jenis_operasi_id' AND D.kelas='$kelas_tarif_id'";
		$query=$this->db->query($q);
		$result= $query->row();
		if ($result){
			return $result;

		}else{
			return 'error';
		}
	}
	public function get_default(){
		$user=$this->session->userdata('user_id');
		$q="SELECT unitpelayananiddefault from musers where id='$user'";
		$query=$this->db->query($q);
		$result= $query->row('unitpelayananiddefault');
		if ($result){
			return $result;
		}else{
			return '#';
		}
	}
	public function kelompok_tindakan_list($id){
		$q="SELECT T.id,T.nama as text, CASE WHEN  H.id IS NULL THEN '' ELSE 'selected' END as selected from mkelompok_tindakan T
			LEFT JOIN tkamaroperasi_tindakan H ON H.kelompok_tindakan_id=T.id AND H.idpendaftaranoperasi='$id'
			WHERE T.`status`='1' ORDER BY H.id";
		$query=$this->db->query($q);
		$result= $query->result();
		if ($result){
			return $result;
		}else{
			return '';
		}
	}
	public function kelompok_diagnosa_list($id){
		$q="SELECT T.id,T.nama as text, CASE WHEN  H.id IS NULL THEN '' ELSE 'selected' END as selected from mkelompok_diagnosa T
			LEFT JOIN tkamaroperasi_diagnosa H ON H.kelompok_diagnosa_id=T.id AND H.idpendaftaranoperasi='$id'
			WHERE T.`status`='1' ORDER BY H.id";
		$query=$this->db->query($q);
		$result= $query->result();
		if ($result){
			return $result;
		}else{
			return '';
		}
	}
	public function list_dokter_anestesi(){
		$cari=$this->input->post('search');
		$q="SELECT *from (
			SELECT D.id,D.nama,'1' as tipe from mdokter D
			WHERE D.idkategori='4' AND D.nama like '%".$cari."%'
			) TBL";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function tind_dokter_operator($id){
		$q="SELECT  TJ.nama as jenis_operasi_nama,TH.nama as tarif_nama, H.* from tkamaroperasi_jasado H
LEFT JOIN mtarif_operasi TH ON TH.id=H.id_tarif
LEFT JOIN mjenis_operasi TJ ON TJ.id=TH.idjenis
LEFT JOIN mtarif_operasi_detail TD ON TD.idtarif=TH.id AND TD.kelas=H.kelas_tarif
			WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function tind_dokter_anatesi($id){
		$q="SELECT *from tkamaroperasi_jasada H
			WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function tind_daa($id){
		$q="SELECT *from tkamaroperasi_jasadaa H
			WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function tind_dao($id){
		$q="SELECT *from tkamaroperasi_jasaao H
			WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function tind_narcose($id){
		$q="SELECT B.nama,M.nama as nama_unit,S.nama as satuan, H.* from tkamaroperasi_narcose H
		INNER JOIN view_barang B ON B.id=H.idobat AND B.idtipe=H.idtipe
		INNER JOIN munitpelayanan M ON M.id=H.idunit
		INNER JOIN msatuan S ON S.id=B.idsatuan
			WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function tind_obat($id){
		$q="SELECT B.nama,M.nama as nama_unit,S.nama as satuan, H.* from tkamaroperasi_obat H
		INNER JOIN view_barang B ON B.id=H.idobat AND B.idtipe=H.idtipe
		INNER JOIN munitpelayanan M ON M.id=H.idunit
		INNER JOIN msatuan S ON S.id=B.idsatuan
			WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function tind_alkes($id){
		$q="SELECT B.nama,M.nama as nama_unit,S.nama as satuan, H.* from tkamaroperasi_alkes H
		INNER JOIN view_barang B ON B.id=H.idobat AND B.idtipe=H.idtipe
		INNER JOIN munitpelayanan M ON M.id=H.idunit
		INNER JOIN msatuan S ON S.id=B.idsatuan
			WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function tind_implan($id){
		$q="SELECT B.nama,M.nama as nama_unit,S.nama as satuan, H.* from tkamaroperasi_implan H
		INNER JOIN view_barang B ON B.id=H.idobat AND B.idtipe=H.idtipe
		INNER JOIN munitpelayanan M ON M.id=H.idunit
		INNER JOIN msatuan S ON S.id=B.idsatuan
			WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function tind_sewa($id){
		$q="SELECT M.nama as nama_alat,H.* from tkamaroperasi_sewaalat H
INNER JOIN mtarif_operasi_sewaalat M ON M.id=H.idalat
WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function tind_konsulen($id){
		$q="SELECT iddokter from tkamaroperasi_konsulen H
			WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result= $query->result();
		$array=array();
		foreach ($result as $row){
			$array[]=$row->iddokter;
		}

		return $array;

	}
	public function tind_instrumen($id){
		$q="SELECT idpegawai from tkamaroperasi_instrumen H
			WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result= $query->result();
		$array=array();
		foreach ($result as $row){
			$array[]=$row->idpegawai;
		}

		return $array;

	}
	public function tind_sirkuler($id){
		$q="SELECT idpegawai from tkamaroperasi_sirkuler H
			WHERE H.idpendaftaranoperasi='$id'";
		$query=$this->db->query($q);
		$result= $query->result();
		$array=array();
		foreach ($result as $row){
			$array[]=$row->idpegawai;
		}

		return $array;

	}
	public function list_dokter_pegawai_anestesi(){
		$cari=$this->input->post('search');
		$q="SELECT *from (
			SELECT D.id,D.nama,'1' as tipe from mdokter D
			WHERE D.idkategori='4'
			UNION
			SELECT P.id,P.nama, '2' tipe From mpegawai P
			WHERE P.idkategori='4') TBL WHERE nama like '%".$cari."%'";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function get_dokter_asisten_operator(){
		$cari=$this->input->post('search');
		$q="SELECT *from (
			SELECT D.id,D.nama,'1' as tipe from mdokter D

			UNION
			SELECT P.id,P.nama, '2' tipe From mpegawai P
			WHERE P.idkategori='2') TBL WHERE nama like '%".$cari."%'";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function list_instrumen(){
		$q="SELECT *from (SELECT P.id,P.nama From mpegawai P
WHERE P.idkategori IN (2,4)) TBL";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function list_unit(){
		$iduser=$this->session->userdata('user_id');
		$q="SELECT M.id,M.nama from munitpelayanan_user U
		LEFT JOIN munitpelayanan M ON M.id=U.idunitpelayanan
		WHERE U.userid='$iduser'";
		// print_r($q);exit();
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function list_alat(){
		$q="SELECT CASE WHEN H.idkelompok <> '0' THEN '0' ELSE H.id END as id,H.nama,H.`level`
			from mtarif_operasi_sewaalat H
			WHERE H.`status`='1'
			ORDER BY path";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function getAllParent($headerpath=0, $level=999)
    {
        # Get ID Root
        // $this->db->select('MAX(CAST(headerpath AS SIGNED)) + 1 AS idroot');
        // $this->db->where('status', '1');
        // $query = $this->db->get('mtarif_operasi_sewaalat');
        // $row = $query->row();

        // if ($headerpath != 0) {
            // $idroot = $headerpath;
        // } else {
            // $idroot = ($row->idroot ? $row->idroot : 1);
        // }

        # Get Option
        $this->db->where('status', '1');
        $this->db->order_by('path', 'ASC');
        $query = $this->db->get('mtarif_operasi_sewaalat');
        $result = $query->result();
		$data='';
        $selected = ($level == 0 ? 'selected' : '');
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                // if ($headerpath != 0 && $level != 0) {
                    // $selected = ($headerpath == $row->path ? 'selected' : '');
                // } else {
                    // $selected = '';
                // }
                $data .= '<option value="'.$row->id.'" '.$selected.'>'.TreeView($row->level, $row->nama).'</option>';
            }
        }
        return $data;
    }
	public function js_obat(){
		$search=$this->input->post('search');
		$idunit=$this->input->post('idunit');
		$idtipe=$this->input->post('idtipe');
		$idkategori=$this->input->post('idkategori');
		$q="SELECT G.idunitpelayanan,G.idtipe,G.idbarang,B.nama,B.hargabeli,B.idsatuan from mgudang_stok G
			INNER JOIN view_barang B ON B.id=G.idbarang AND B.idtipe=G.idtipe
			WHERE G.idunitpelayanan='$idunit' AND G.idtipe='$idtipe' AND (B.nama LIKE '%".$search."%' OR B.kode LIKE '%".$search."%')";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function js_obat_narcose(){
		$search=$this->input->post('search');
		$idunit=$this->input->post('idunit');
		$idtipe=$this->input->post('idtipe');
		$idkategori=$this->input->post('idkategori');
		// $q="SELECT G.idunitpelayanan,G.idtipe,G.idbarang,B.nama,B.hargabeli,B.idsatuan from mgudang_stok G
			// INNER JOIN view_barang B ON B.id=G.idbarang AND B.idtipe=G.idtipe
			// WHERE G.idunitpelayanan='$idunit' AND G.idtipe='$idtipe' AND (B.nama LIKE '%".$search."%') AND B.idkategori IN (
							// SELECT id FROM (
									// SELECT *FROM view_kategori_alkes
									// UNION
									// SELECT *FROM view_kategori_implan
									// UNION
									// SELECT *FROM view_kategori_obat
									// UNION
									// SELECT *FROM view_kategori_logistik
							// ) T1
							// where T1.path like '00001%')";
		$q="SELECT G.idunitpelayanan,G.idtipe,G.idbarang,B.nama,B.hargabeli,B.idsatuan from mgudang_stok G
			INNER JOIN view_barang B ON B.id=G.idbarang AND B.idtipe=G.idtipe
			WHERE G.idunitpelayanan='$idunit' AND G.idtipe='$idtipe' AND (B.nama LIKE '%".$search."%' OR B.kode LIKE '%".$search."%')";
		$query=$this->db->query($q);
		$result= $query->result();

		return $result;

	}
	public function get_barang($idbarang,$idtipe,$idkelompokpasien){
		$q="SELECT idbarang,idtipe, nama,satuan,COALESCE(hargadasar,0) as hargadasar,COALESCE(margin,25) as margin FROM (SELECT B.id as idbarang,B.idtipe, B.nama,S.nama as satuan,B.hargadasar,
				CASE
					WHEN '$idkelompokpasien'='1' THEN B.marginasuransi
				  WHEN '$idkelompokpasien'='2' THEN B.marginjasaraharja
				  WHEN '$idkelompokpasien'='3' THEN B.marginbpjskesehatan
				  WHEN '$idkelompokpasien'='4' THEN B.marginbpjstenagakerja
					ELSE B.marginumum
				 END as margin
				FROM view_barang B
				LEFT JOIN msatuan S ON S.id=B.idsatuan
				WHERE
				B.id='$idbarang' AND B.idtipe='$idtipe') AS TBL";
		$query=$this->db->query($q);
		$result= $query->row();

		return $result;

	}

	public function find_pasien($id){
		$q="SELECT H.id,
			CASE WHEN H.idtipe='1' THEN 'RAWAT INAP' ELSE 'ODS' END as tipe,H.idtipe,
			CASE WHEN H.idtipe='1' THEN H.id ELSE P.id END as idpendaftaran,
			CASE WHEN H.idtipe='1' THEN 1 ELSE 2 END as idasalpendaftaran,H.idkelas,
			H.no_medrec,H.namapasien,DATE_FORMAT(H.tanggal_lahir,'%d-%m-%Y') as tanggal_lahir,H.idpasien,H.umurhari,H.umurbulan,H.umurtahun
			,R.nama as ruang,K.nama as kelas,B.nama as bed,D.nama as dpjp,P.iddokter,rujuk.nama as perujuk,Kp.nama as kelompokpasien,Rek.nama as asuransi,H.iddokterpenanggungjawab
			from trawatinap_pendaftaran H
			LEFT JOIN mruangan R ON R.id=H.idruangan
			LEFT JOIN mkelas K ON K.id=H.idkelas
			LEFT JOIN mbed B ON B.id=H.idbed
			LEFT JOIN mdokter D ON D.id=H.iddokterpenanggungjawab
			LEFT JOIN tpoliklinik_pendaftaran P ON P.id=H.idpoliklinik
			LEFT JOIN mpasien_kelompok Kp ON Kp.id=H.idkelompokpasien
			LEFT JOIN mdokter rujuk ON rujuk.id=P.iddokter
			LEFT JOIN mrekanan Rek On Rek.id=H.idrekanan
			WHERE H.id='$id'";
		$query=$this->db->query($q);
		return $query->row();
	}
	public function get_edit($id){
		$q="SELECT H.id, H.idpendaftaran,H.idasalpendaftaran,H.idpasien,
			H.waktumulaioperasi,H.waktuselesaioperasi,
			CASE WHEN RI.idtipe IS null THEN 'ODS' WHEN RI.idtipe='1' THEN 'RAWAT INAP' WHEN RI.idtipe='2' THEN 'ODS' END as tipe,
			CASE WHEN H.idasalpendaftaran='1' THEN TP.namapasien ELSE RI.namapasien END as namapasien,
			CASE WHEN H.idasalpendaftaran='1' THEN TP.no_medrec ELSE RI.no_medrec END as no_medrec,
			CASE WHEN H.idasalpendaftaran='1' THEN DATE_FORMAT(TP.tanggal_lahir,'%d-%m-%Y') ELSE DATE_FORMAT(RI.tanggal_lahir,'%d-%m-%Y') END as tanggal_lahir,
			CASE WHEN H.idasalpendaftaran='1' THEN TP.umurtahun ELSE RI.umurtahun END as umurtahun,
			CASE WHEN H.idasalpendaftaran='1' THEN TP.umurbulan ELSE RI.umurbulan END as umurbulan,
			CASE WHEN H.idasalpendaftaran='1' THEN TP.umurhari ELSE RI.umurhari END as umurhari,
			CASE WHEN H.idasalpendaftaran='1' THEN RP.nama ELSE RR.nama END as asuransi,H.catatan,
			K.nama as kelas,B.nama as bed,
			TP.idkelompokpasien,P.nama as kelompokpasien,
			RI.id as ri_id,TP.id as poli_id,
			COALESCE(H.dpjp_id,'0') as dpjp,DATE_FORMAT(H.tanggaloperasi,'%d-%m-%Y') as tanggaloperasi,H.diagnosa,H.operasi,
			COALESCE(H.jenis_operasi_id,'0') as jenis_operasi_id,
			COALESCE(H.jenis_anestesi_id,'0') as jenis_anestesi_id,
			COALESCE(kelompok_operasi_id,'0') as kelompok_operasi_id,
			COALESCE(kelompok_tindakan_id,'0') as kelompok_tindakan_id,
			COALESCE(kelas_tarif_id,'0') as kelas_tarif_id,
			COALESCE(ruang_id,'0') as ruang_id,
			COALESCE(kelompk_diagnosa_id,'0') as kelompk_diagnosa_id,
			H.namapetugas,
			H.tanggal_diajukan,H.idapprove,U.`name` as user_setuju,H.tanggal_disetujui,H.status
			from tkamaroperasi_pendaftaran H
			LEFT JOIN musers U ON U.id=H.idapprove
			LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.idpendaftaran
			LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpendaftaran
			LEFT JOIN mpasien_kelompok P ON P.id=TP.idkelompokpasien
			LEFT JOIN mrekanan RP ON RP.id=TP.idrekanan
			LEFT JOIN mrekanan RR ON RR.id=RI.idrekanan
			LEFT JOIN mkelas K ON K.id=RI.idkelas
			LEFT JOIN mbed B ON B.id=RI.idbed
			WHERE H.id='$id'";
		$query=$this->db->query($q);
		return $query->row();
	}
	public function get_tindakan($id){
		$q="SELECT H.id, H.idpendaftaran,H.idasalpendaftaran,H.idpasien,
			H.waktumulaioperasi,H.waktuselesaioperasi,
			CASE WHEN RI.idtipe IS null THEN 'ODS' WHEN RI.idtipe='1' THEN 'RAWAT INAP' WHEN RI.idtipe='2' THEN 'ODS' END as tipe,
			CASE WHEN H.idasalpendaftaran='1' THEN TP.namapasien ELSE RI.namapasien END as namapasien,
			CASE WHEN H.idasalpendaftaran='1' THEN TP.no_medrec ELSE RI.no_medrec END as no_medrec,
			CASE WHEN H.idasalpendaftaran='1' THEN DATE_FORMAT(TP.tanggal_lahir,'%d-%m-%Y') ELSE DATE_FORMAT(RI.tanggal_lahir,'%d-%m-%Y') END as tanggal_lahir,
			CASE WHEN H.idasalpendaftaran='1' THEN TP.umurtahun ELSE RI.umurtahun END as umurtahun,
			CASE WHEN H.idasalpendaftaran='1' THEN TP.umurbulan ELSE RI.umurbulan END as umurbulan,
			CASE WHEN H.idasalpendaftaran='1' THEN TP.umurhari ELSE RI.umurhari END as umurhari,
			CASE WHEN H.idasalpendaftaran='1' THEN TP.idrekanan ELSE RI.idrekanan END as idrekanan,
			CASE WHEN H.idasalpendaftaran='1' THEN TP.idkelompokpasien ELSE RI.idkelompokpasien END as idkelompokpasien,
			CASE WHEN H.idasalpendaftaran='1' THEN RP.nama ELSE RR.nama END as asuransi,
			K.nama as kelas,B.nama as bed,H.catatan,
			P.nama as kelompokpasien,
			RI.id as ri_id,TP.id as poli_id,
			COALESCE(H.dpjp_id,'0') as dpjp,DATE_FORMAT(H.tanggaloperasi,'%d-%m-%Y') as tanggaloperasi,H.diagnosa,H.operasi,
			COALESCE(H.jenis_operasi_id,'0') as jenis_operasi_id,
			COALESCE(H.jenis_anestesi_id,'0') as jenis_anestesi_id,
			COALESCE(kelompok_operasi_id,'0') as kelompok_operasi_id,
			COALESCE(kelompok_tindakan_id,'0') as kelompok_tindakan_id,
			COALESCE(kelas_tarif_id,'0') as kelas_tarif_id,
			COALESCE(ruang_id,'0') as ruang_id,
			COALESCE(kelompk_diagnosa_id,'0') as kelompk_diagnosa_id,
			H.namapetugas,
			H.tanggal_diajukan,H.idapprove,U.`name` as user_setuju,H.tanggal_disetujui,Dok.nama as perujuk,
			persen_da,persen_daa,persen_dao,total_obat,total_narcose,total_alkes,total_implan,total_sewa
			,RI.statuskasir
			from tkamaroperasi_pendaftaran H
			LEFT JOIN musers U ON U.id=H.idapprove
			LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.idpendaftaran
			LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpendaftaran
			LEFT JOIN mpasien_kelompok P ON P.id=RI.idkelompokpasien
			LEFT JOIN mrekanan RP ON RP.id=TP.idrekanan
			LEFT JOIN mrekanan RR ON RR.id=RI.idrekanan
			LEFT JOIN mkelas K ON K.id=RI.idkelas
			LEFT JOIN mbed B ON B.id=RI.idbed
			LEFT JOIN mdokter Dok ON Dok.id=TP.iddokter
			WHERE H.id='$id'";
		$query=$this->db->query($q);
		return $query->row_array();
	}
    // @checlist &start
    // @get petugas(kamaroperasi)
    public function getpetugasko()
    {
        $this->db->select('id, nama')
            ->where('idkategori', 2);
        $query = $this->db->get('mpegawai');
        return $query->result();
    }
	public function mjenis_operasi()
  {
      $this->db->select('mjenis_operasi.id, mjenis_operasi.nama');
      $this->db->where('mjenis_operasi.status', 1);
      $query = $this->db->get('mjenis_operasi');
      return $query->result();
  }
	public function mjenis_operasi_tindakan($idkelompokpasien, $idrekanan)
  {
      if ($idkelompokpasien == 1 && $idrekanan != '') {
        $this->db->select('mjenis_operasi.id, mjenis_operasi.nama');
        $this->db->join('mrekanan_operasi', 'mrekanan_operasi.idjenisoperasi = mjenis_operasi.id');
        $this->db->where('mrekanan_operasi.idrekanan', $idrekanan);
        $this->db->where('mjenis_operasi.status', 1);
        $query = $this->db->get('mjenis_operasi');
        if ($query->num_rows() > 0) {
          return $query->result();
        } else {
          $this->db->select('mjenis_operasi.id, mjenis_operasi.nama');
          $this->db->join('mpasien_kelompok_operasi', 'mpasien_kelompok_operasi.idjenisoperasi = mjenis_operasi.id');
          $this->db->where('mpasien_kelompok_operasi.idkelompokpasien', $idkelompokpasien);
          $this->db->where('mjenis_operasi.status', 1);
          $query = $this->db->get('mjenis_operasi');

          return $query->result();
        }
      } else {
        $this->db->select('mjenis_operasi.id, mjenis_operasi.nama');
        $this->db->join('mpasien_kelompok_operasi', 'mpasien_kelompok_operasi.idjenisoperasi = mjenis_operasi.id');
        $this->db->where('mpasien_kelompok_operasi.idkelompokpasien', $idkelompokpasien);
        $this->db->where('mjenis_operasi.status', 1);
        $query = $this->db->get('mjenis_operasi');

        return $query->result();
      }
  }
	public function mkelas()
    {
        $this->db->select('id, nama')
            ->where('status', 1);
        $query = $this->db->get('mkelas');
        return $query->result();
    }
	public function mkelompok_operasi()
    {
        $this->db->select('id, nama')
            ->where('status', 1);
        $query = $this->db->get('mkelompok_operasi');
        return $query->result();
    }
	public function mjenis_anaesthesi()
    {
        $this->db->select('id, nama')
            ->where('status', 1);
        $query = $this->db->get('mjenis_anaesthesi');
        return $query->result();
    }
	public function mruangan()
    {
        $this->db->select('id, nama');
        $this->db->where('idtipe', 2);
        $this->db->where('status', 1);
        $query = $this->db->get('mruangan');
        return $query->result();
    }
    // @get petugas(anaesthesi)
    public function getpetugasanaesthesi()
    {
        $this->db->select('id, nama')
            ->where('idkategori', 4);
        $query = $this->db->get('mpegawai');
        return $query->result();
    }
    // get dokter(umum)
    public function getdokterumum($kategori = null)
    {
        $this->db->select('id, nama');
        if ($kategori != null) {
            $this->db->where('idkategori', $kategori);
        }
        $query = $this->db->get('mdokter');
        return $query->result();
    }
	public function kelas_list()
    {
        $this->db->select('id, nama');
        $this->db->where('status','1');

        $query = $this->db->get('mkelas');
        return $query->result();
    }
    // get ruangan(kamaroperasi)
    public function getruangan()
    {
        $this->db->where('idtipe', 2);
        $query = $this->db->get('mruangan');
        return $query->result();
    }
    // @get munitpelayanan
    public function getunitpelayanan()
    {
        $this->db->select('munitpelayanan.id as id, munitpelayanan.nama as nama ');
        $this->db->from('mpermission_unitpelayanan');
        $this->db->join('musers', 'mpermission_unitpelayanan.userid = musers.id', 'left');
        $this->db->join('munitpelayanan', 'mpermission_unitpelayanan.unipelayananid = munitpelayanan.id', 'left');
        $this->db->where('mpermission_unitpelayanan.userid', $this->session->userdata('user_id'));
        $this->db->where('munitpelayanan.status', 1);
        $query = $this->db->get();
        return $query->result();
    }
    // @get jenisoperasi(kamaroperasi)
    public function getjenisoperasi()
    {
        $query = $this->db->get('mjenis_operasi');
        return $query->result();
    }
    // @get keteranganoperasi(kamaroperasi)
    public function getketeranganoperasi()
    {
        $query = $this->db->get('mketerangan_operasi');
        return $query->result();
    }
    // @get jesnianaesthesi(kamaroperasi)
    public function getjenisanaesthesi()
    {
        $query = $this->db->get('mjenis_anaesthesi');
        return $query->result();
    }
    // @get estimasi(kamaroperasi)
    public function getestimasi($id)
    {
        $this->db->select('testimasi.id, testimasi.tanggal, testimasi.status, tkamaroperasi_pendaftaran.idestimasi')
            ->join('tkamaroperasi_pendaftaran', 'testimasi.idrawatinap = tkamaroperasi_pendaftaran.idpendaftaran', 'left')
            ->where('testimasi.idrawatinap', $id)
            ->group_by('testimasi.id');

        $query = $this->db->get('testimasi');
        return $query->result();
    }
    // @get estimasidetail(kamaroperasi)
    public function getestimasidetail($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('testimasi');
        return $query->result();
    }
    // @get estimasiimplan(kamaroperasi)
    public function getestimasiimplan($id)
    {
        $this->db->select('mdata_implan.nama, testimasi_implan.hargajual, testimasi_implan.kuantitas, testimasi_implan.statusimplan')
            ->join('mdata_implan', 'testimasi_implan.idimplan = mdata_implan.id', 'left')
            ->where('testimasi_implan.idtestimasi', $id);

        $query = $this->db->get('testimasi_implan');
        return $query->result();
    }
    // @get totalestimasi(kamaroperasi)
    public function gettotalestimasi($id)
    {
        $statusnotif = null;
        $this->db->select('idrawatinap')
            ->where('statusnotif', $statusnotif)
            ->where('idrawatinap', $id);
        $query = $this->db->get('testimasi');
        return $query->result();
    }

    // @get pasienl(kamaroperasi)
    public function getpasienko_l($asalpasien = null, $tglawal = null, $tglakhir = null, $waktu = null, $status = null, $idtipe = null, $dokter = null, $filter_jam = null)
    {
        if ($asalpasien != null) {
            $this->db->where('view_index_tkamaroperasi.super', $asalpasien);
        }

        if ($tglawal != null && $tglakhir != '') {
            $this->db->where('view_index_tkamaroperasi.tanggaloperasi >=', $tglawal);
            $this->db->where('view_index_tkamaroperasi.tanggaloperasi <=', $tglakhir);
        } elseif ($tglawal != null) {
            $this->db->where('view_index_tkamaroperasi.tanggaloperasi', $tglawal);
        } elseif ($tglakhir != null) {
            $this->db->where('view_index_tkamaroperasi.tanggaloperasi', $tglakhir);
        }

        if ($waktu != null && $filter_jam == 1) {
            $this->db->where('view_index_tkamaroperasi.waktumulaioperasi', $waktu);
        }

        if ($status != null) {
            if ($status == 1) {
                $this->db->where('view_index_tkamaroperasi.status', 1)
                    ->or_where('view_index_tkamaroperasi.status', 5);
            } else {
                $this->db->where('view_index_tkamaroperasi.status', $status);
            }
        }

        if ($idtipe != null) {
            $this->db->where('view_index_tkamaroperasi.idtipe', $idtipe);
        }

        if ($dokter != null) {
            $this->db->where('view_index_tkamaroperasi.dokter', $dokter);
        }

        $query = $this->db->get('view_index_tkamaroperasi');
        return $query->result();
    }
    // @change to view because much relation with other table
    // @get pasiend(kamaroperasi)
    public function getpasienko_d($id)
    {
        // $this->db->select('id, idpendaftaran, no_medrec, namapasien, umur_tahun, umur_bulan, umur_hari, kelompokpasien, dokter, kelastarif, ruanginap, diagnosa, operasi, tanggaloperasi, waktumulaioperasi, waktuselesaioperasi, statuskasir');
        $this->db->where('id', $id);
        $query = $this->db->get('view_index_tkamaroperasi');
        return $query->result();
    }
    public function getpasienko_detail($id)
    {
        // $this->db->select('id, idpendaftaran, no_medrec, namapasien, umur_tahun, umur_bulan, umur_hari, kelompokpasien, dokter, kelastarif, ruanginap, diagnosa, operasi, tanggaloperasi, waktumulaioperasi, waktuselesaioperasi, statuskasir');
        $this->db->where('id', $id);
        $query = $this->db->get('view_index_tkamaroperasi_detail');
        return $query->result();
    }

    public function getDokterOperatorPrint($id)
    {
        $this->db->select('GROUP_CONCAT(namadokteroperator SEPARATOR ", ") AS namadokteroperator');
        $this->db->group_by('idtindakan');
        $this->db->where('idtindakan', $id);
        $query = $this->db->get('tkamaroperasi_jasado');
        return $query->row()->namadokteroperator;
    }

    public function getCatatanPrint($id)
    {
        $this->db->select('catatan');
        $this->db->where('idpendaftaranoperasi', $id);
        $query = $this->db->get('tkamaroperasi_tindakan');
        return $query->row()->catatan;
    }

    public function getdokterAnesthesiPrint($id)
    {
        $this->db->select('GROUP_CONCAT(namadokteranesthesi SEPARATOR ", ") AS namadokteranesthesi');
        $this->db->group_by('idtindakan');
        $this->db->where('idtindakan', $id);
        $query = $this->db->get('tkamaroperasi_jasada');
        return $query->row()->namadokteranesthesi;
    }

    // @get pasient(kamaroperasi)
    public function getpasienko_t($id)
    {
        $this->db->where('idpendaftaranoperasi', $id);
        $query = $this->db->get('tkamaroperasi_tindakan');
        return $query->result();
    }
    // @------------------------------------------------------------------------
    // @get allnarcose(kamaroperasi)
    public function getallnarcose($id)
    {
        $query = $this->db->select('tkamaroperasi_narcose.tanggal,
        tkamaroperasi_narcose.statusverifikasi,
        tkamaroperasi_pendaftaran.statuskasir,
        tkamaroperasi_narcose.idobat as id,
        mdata_obat.kode,
        mdata_obat.nama,
        tkamaroperasi_narcose.hargajual as harga,
        sum(tkamaroperasi_narcose.kuantitas) as kuantitas,
        sum(tkamaroperasi_narcose.totalkeseluruhan) as subtotal,
        munitpelayanan.nama AS unitpelayanan')
            ->join('tkamaroperasi_pendaftaran', 'tkamaroperasi_pendaftaran.id = tkamaroperasi_narcose.idtindakan', 'left')
            ->join('mdata_obat', 'mdata_obat.id = tkamaroperasi_narcose.idobat', 'left')
            ->join('munitpelayanan', 'munitpelayanan.id = tkamaroperasi_narcose.idunit', 'left')
            ->where('idtindakan', $id)
            ->group_by('mdata_obat.kode')
            ->get('tkamaroperasi_narcose');
        return $query->result();
    }
    // @get allobat(kamaroperasi)
    public function getallobat($id)
    {
        $query = $this->db->select('tkamaroperasi_obat.tanggal,
        tkamaroperasi_obat.statusverifikasi,
        tkamaroperasi_pendaftaran.statuskasir,
        tkamaroperasi_obat.idobat as id,
        mdata_obat.kode,
        mdata_obat.nama,
        tkamaroperasi_obat.hargajual as harga,
        sum(tkamaroperasi_obat.kuantitas) as kuantitas,
        sum(tkamaroperasi_obat.totalkeseluruhan) as subtotal,
        munitpelayanan.nama AS unitpelayanan')
            ->join('tkamaroperasi_pendaftaran', 'tkamaroperasi_pendaftaran.id = tkamaroperasi_obat.idtindakan', 'left')
            ->join('mdata_obat', 'mdata_obat.id = tkamaroperasi_obat.idobat', 'left')
            ->join('munitpelayanan', 'munitpelayanan.id = tkamaroperasi_obat.idunit', 'left')
            ->where('idtindakan', $id)
            ->group_by('mdata_obat.kode')
            ->get('tkamaroperasi_obat');
        return $query->result();
    }
    // @get allalkes(kamaroperasi)
    public function getallalkes($id)
    {
        $query = $this->db->select('tkamaroperasi_alkes.tanggal,
        tkamaroperasi_alkes.statusverifikasi,
        tkamaroperasi_pendaftaran.statuskasir,
        tkamaroperasi_alkes.idalkes as id,
        mdata_alkes.kode,
        mdata_alkes.nama,
        tkamaroperasi_alkes.hargajual as harga,
        sum(tkamaroperasi_alkes.kuantitas) as kuantitas,
        sum(tkamaroperasi_alkes.totalkeseluruhan) as subtotal,
        munitpelayanan.nama AS unitpelayanan')
            ->join('tkamaroperasi_pendaftaran', 'tkamaroperasi_pendaftaran.id = tkamaroperasi_alkes.idtindakan', 'left')
            ->join('mdata_alkes', 'mdata_alkes.id = tkamaroperasi_alkes.idalkes', 'left')
            ->join('munitpelayanan', 'munitpelayanan.id = tkamaroperasi_alkes.idunit', 'left')
            ->where('idtindakan', $id)
            ->group_by('mdata_alkes.kode')
            ->get('tkamaroperasi_alkes');
        return $query->result();
    }
    // @get getallimplant(kamaroperasi)
    public function getallimplant($id)
    {
        $query = $this->db->select('tkamaroperasi_implan.tanggal,
        tkamaroperasi_implan.statusverifikasi,
        tkamaroperasi_pendaftaran.statuskasir,
        tkamaroperasi_implan.idimplant as id,
        mdata_implan.kode,
        mdata_implan.nama,
        tkamaroperasi_implan.hargajual as harga,
        sum(tkamaroperasi_implan.kuantitas) as kuantitas,
        sum(tkamaroperasi_implan.totalkeseluruhan) as subtotal')
            ->join('tkamaroperasi_pendaftaran', 'tkamaroperasi_pendaftaran.id = tkamaroperasi_implan.idtindakan', 'left')
            ->join('mdata_implan', 'mdata_implan.id = tkamaroperasi_implan.idimplant', 'left')
            ->where('idtindakan', $id)
            ->group_by('mdata_implan.kode')
            ->get('tkamaroperasi_implan');
        return $query->result();
    }
    // @get getallsewaalat(kamaroperasi)
    public function getallsewaalat($id)
    {
        $query = $this->db->select('tkamaroperasi_sewaalat.id as idst,
        tkamaroperasi_sewaalat.tanggal,
        tkamaroperasi_sewaalat.statusverifikasi,
        tkamaroperasi_pendaftaran.statuskasir,
        tkamaroperasi_sewaalat.idalat as id,
        tkamaroperasi_sewaalat.idalat as kode,
        mtarif_operasi_sewaalat.nama,
        tkamaroperasi_sewaalat.total as harga,
        sum(tkamaroperasi_sewaalat.kuantitas) as kuantitas,
        sum(tkamaroperasi_sewaalat.totalkeseluruhan) as subtotal,
        tkamaroperasi_sewaalat.jasasarana,
        tkamaroperasi_sewaalat.jasapelayanan,
        tkamaroperasi_sewaalat.bhp,
        tkamaroperasi_sewaalat.biayaperawatan')
            ->join('tkamaroperasi_pendaftaran', 'tkamaroperasi_pendaftaran.id = tkamaroperasi_sewaalat.idtindakan', 'left')
            ->join('mtarif_operasi_sewaalat', 'mtarif_operasi_sewaalat.id = tkamaroperasi_sewaalat.idalat', 'left')
            ->where('tkamaroperasi_sewaalat.idtindakan', $id)
            ->group_by('tkamaroperasi_sewaalat.idalat')
            ->get('tkamaroperasi_sewaalat');
        return $query->result();
    }

    // @get all-sewaalat(kamaroperasi)
    // @------------------------------------------------------------------------
    public function getidsewaalat($id)
    {
        $query = $this->db->select('tkamaroperasi_sewaalat.id as idst, tkamaroperasi_sewaalat.tanggal, tkamaroperasi_sewaalat.statusverifikasi, tkamaroperasi_pendaftaran.statuskasir, tkamaroperasi_sewaalat.idalat as id, tkamaroperasi_sewaalat.idalat as kode, mtarif_operasi_sewaalat.nama, tkamaroperasi_sewaalat.total as harga, tkamaroperasi_sewaalat.kuantitas as kuantitas, tkamaroperasi_sewaalat.totalkeseluruhan as subtotal, tkamaroperasi_sewaalat.jasasarana, tkamaroperasi_sewaalat.jasapelayanan, tkamaroperasi_sewaalat.bhp, tkamaroperasi_sewaalat.biayaperawatan')
            ->join('tkamaroperasi_pendaftaran', 'tkamaroperasi_pendaftaran.id = tkamaroperasi_sewaalat.idtindakan', 'left')
            ->join('mtarif_operasi_sewaalat', 'mtarif_operasi_sewaalat.id = tkamaroperasi_sewaalat.idalat', 'left')
            ->where('idtindakan', $id)
            ->get('tkamaroperasi_sewaalat');
        return $query->result();
    }

    // @get check-sewaalat(kamaroperasi)
    // public function getchecksewaalat($id, $kelas, $jenis) {
    //   $this->db->select('idtarif, jasasarana, jasapelayanan, bhp, biayaperawatan, total');
    //   $this->db->where(array('idtarif' => $id, 'kelas' => $kelas, 'idjenis' => $jenis));
    //   $query = $this->db->get('view_mdata_sewaalat');
    //   return $query->result();
    // }

    // @get change-sewaalat(kamaroperasi)
    // public function changesewaalat($data, $id, $idko) {
    //   $this->db->set($data)->where(array('idtindakan' => $id, 'idalat' => $idko));
    //   $this->db->update('tkamaroperasi_sewaalat');
    //   return true;
    // }
    // @------------------------------------------------------------------------

    // @save tindakan(kamaroperasi)
    public function save($data, $status, $id)
    {
        if ($status == 0) {
            $query = $this->db->insert('tkamaroperasi_tindakan', $data);
        } else {
            $this->db->set($data)->where('idpendaftaranoperasi', $id);
            $query = $this->db->update('tkamaroperasi_tindakan');
        }
        return true;
    }
    // @update status(kamaroperasi)
    public function updatestatus($id, $diagnosa, $operasi)
    {
        $this->db->set(array('status' => 4, 'diagnosa' => $diagnosa, 'operasi' => $operasi))
            ->where('id', $id)
            ->update('tkamaroperasi_pendaftaran');
        return true;
    }
    /* @checklist &end */

    // @penggunaan - penggunaan

    // @--------------------------------------------------------------------------
    public function dnarcose($id)
    {
        $this->db->select('id')
            ->where('idobat', $id);
        $query = $this->db->get('tkamaroperasi_narcose');
        return $query->result();
    }

    public function dobat($id)
    {
        $this->db->select('id')
            ->where('idobat', $id);
        $query = $this->db->get('tkamaroperasi_obat');
        return $query->result();
    }

    public function dalkes($id)
    {
        $this->db->select('id')
            ->where('idalkes', $id);
        $query = $this->db->get('tkamaroperasi_alkes');
        return $query->result();
    }

    public function dimplan($id)
    {
        $this->db->select('id')
            ->where('idimplant', $id);
        $query = $this->db->get('tkamaroperasi_implan');
        return $query->result();
    }

    public function dalat($id)
    {
        $this->db->select('id')
            ->where('idalat', $id);
        $query = $this->db->get('tkamaroperasi_sewaalat');
        return $query->result();
    }

    // @delete penggunaan(kamaroperasi)
    public function deletepenggunaan($id, $check, $idtindakan)
    {
        if ($check == 1) {
            $query = $this->dnarcose($id);
            foreach ($query as $rows) {
                $this->db->where('id', $rows->id);
                $this->db->where('idtindakan', $idtindakan);
                $this->db->delete('tkamaroperasi_narcose');
            }
            return $query;
        }
        if ($check == 2) {
            $query = $this->dobat($id);
            foreach ($query as $rows) {
                $this->db->where('id', $rows->id);
                $this->db->where('idtindakan', $idtindakan);
                $this->db->delete('tkamaroperasi_obat');
            }
            return $query;
        }
        if ($check == 3) {
            $query = $this->dalkes($id);
            foreach ($query as $rows) {
                $this->db->where('id', $rows->id);
                $this->db->where('idtindakan', $idtindakan);
                $this->db->delete('tkamaroperasi_alkes');
            }
            return $query;
        }
        if ($check == 4) {
            $query = $this->dimplan($id);
            foreach ($query as $rows) {
                $this->db->where('id', $rows->id);
                $this->db->where('idtindakan', $idtindakan);
                $this->db->delete('tkamaroperasi_implan');
            }
            return $query;
        }
        if ($check == 5) {
            $query = $this->dalat($id);
            foreach ($query as $rows) {
                $this->db->where('id', $rows->id);
                $this->db->where('idtindakan', $idtindakan);
                $this->db->delete('tkamaroperasi_sewaalat');
            }
            return $query;
        }
    }
    // @--------------------------------------------------------------------------

    public function getpasienko_de($id)
    {
        // $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran', 'left')
        //      ->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'left')
        //      ->select('tkamaroperasi_pendaftaran.id, mfpasien.nama, tkamaroperasi_pendaftaran.diagnosa, tkamaroperasi_pendaftaran.operasi, tkamaroperasi_pendaftaran.tanggaloperasi, tkamaroperasi_pendaftaran.waktumulaioperasi, tkamaroperasi_pendaftaran.status, tpoliklinik_pendaftaran.rencana');
        // $this->db->where('tkamaroperasi_pendaftaran.id', $id);
        // $query = $this->db->get('tkamaroperasi_pendaftaran');
        $this->db->where('id', $id);
        $query = $this->db->get('view_index_tkamaroperasi');
        return $query->result();
    }

    public function checkkopendaftaran($time, $date)
    {
        $this->db->where('status', 3);
        $this->db->where('waktumulaioperasi', $time);
        $this->db->where('tanggaloperasi', $date);
        $query = $this->db->get('tkamaroperasi_pendaftaran');
        return $query->result();
    }

    // @fix
    public function checkkoid($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('trawatinap_pendaftaran');
        return $query->result();
    }

    public function givestatus($nopendaftaran, $status)
    {
        if ($status == 1) {
            $data = array('statustindakan' => 2);
        } else {
            $data = array('statustindakan' => 0);
        }

        $this->db->set($data);
        $this->db->where('nopendaftaran', $nopendaftaran);
        $this->db->update('tpoliklinik_pendaftaran', $data);
    }

    public function approveko($id, $idapprove, $idpendaftaran)
    {
        if (empty($this->checkkoid($idpendaftaran))) {
            $data = array(
                'idapprove' => $idapprove,
                'tanggal_disetujui' => date("Y-m-d H:i:s"),
                'status' => 5,
            );
        } else {
            $data = array(
                'idapprove' => $idapprove,
                'tanggal_disetujui' => date("Y-m-d H:i:s"),
                'status' => 3,
            );
        }

        $this->givestatus($idpendaftaran, 1);
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update('tkamaroperasi_pendaftaran', $data);
        return true;
    }

    // @ ------------------------------------------------------------------------

    public function deletepasienko($id, $idpendaftaran)
    {
        $this->givestatus($idpendaftaran, 2);
        $this->db->set(array('statustampil' => 0));
        $this->db->where('id', $id);
        $this->db->update('tkamaroperasi_pendaftaran');
        return true;
    }

    public function updatepasienko($id, $data)
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update('tkamaroperasi_pendaftaran');
        return true;
    }

    // @save penggunaan narcose
    public function savedatanarcose($data)
    {
        $detail = json_decode($data);
        foreach ($detail as $rows) {
            $this->db->set(
                array(
                    'tanggal' => date("Y-m-d"),
                    'idtindakan' => $rows[9],
                    'idunit' => $rows[2],
                    'idobat' => $rows[11],
                    'hargadasar' => $rows[7],
                    'margin' => $rows[8],
                    'hargajual' => $rows[3],
                    'kuantitas' => $rows[4],
                    'totalkeseluruhan' => $rows[5],
                    'dari' => "tko",
                    'tanggal_transaksi' => date("Y-m-d H:i:s"),
                    'iduser_transaksi' => $this->session->userdata('user_id'),
                )
            );
            $this->db->insert('tkamaroperasi_narcose');
        }

        return true;
    }

    // @save penggunaan obat
    public function savedataobat($data)
    {
        $detail = json_decode($data);
        foreach ($detail as $rows) {
            $this->db->set(
                array(
                    'tanggal' => date("Y-m-d"),
                    'idtindakan' => $rows[9],
                    'idunit' => $rows[2],
                    'idobat' => $rows[11],
                    'hargadasar' => $rows[7],
                    'margin' => $rows[8],
                    'hargajual' => $rows[3],
                    'kuantitas' => $rows[4],
                    'totalkeseluruhan' => $rows[5],
                    'dari' => "tko",
                    'tanggal_transaksi' => date("Y-m-d H:i:s"),
                    'iduser_transaksi' => $this->session->userdata('user_id'),
                )
            );
            $this->db->insert('tkamaroperasi_obat');
        }

        return true;
    }

    // @save penggunaan alkes
    public function savedataalkes($data)
    {
        $detail = json_decode($data);
        foreach ($detail as $rows) {
            $this->db->set(
                array(
                    'tanggal' => date("Y-m-d"),
                    'idtindakan' => $rows[9],
                    'idunit' => $rows[2],
                    'idalkes' => $rows[11],
                    'hargadasar' => $rows[7],
                    'margin' => $rows[8],
                    'hargajual' => $rows[3],
                    'kuantitas' => $rows[4],
                    'totalkeseluruhan' => $rows[5],
                    'dari' => "tko",
                    'tanggal_transaksi' => date("Y-m-d H:i:s"),
                    'iduser_transaksi' => $this->session->userdata('user_id'),
                )
            );
            $this->db->insert('tkamaroperasi_alkes');
        }

        return true;
    }

    // @save penggunaan implant
    public function savedataimplant($data)
    {
        $detail = json_decode($data);
        foreach ($detail as $rows) {
            $this->db->set(
                array(
                    'tanggal' => date("Y-m-d"),
                    'idtindakan' => $rows[9],
                    'idimplant' => $rows[11],
                    'hargadasar' => $rows[7],
                    'margin' => $rows[8],
                    'hargajual' => $rows[3],
                    'kuantitas' => $rows[4],
                    'totalkeseluruhan' => $rows[5],
                    'dari' => "tko",
                )
            );
            $this->db->insert('tkamaroperasi_implan');
        }

        return true;
    }

    // @save penggunaan sewaalat
    public function savedataalat($data)
    {
        $detail = json_decode($data);
        foreach ($detail as $rows) {
            $this->db->set(
                array(
                    'tanggal' => date("Y-m-d"),
                    'idtindakan' => $rows[9],
                    'idalat' => $rows[0],
                    'jasasarana' => $rows[7],
                    'jasapelayanan' => $rows[8],
                    'total' => $rows[3],
                    'kuantitas' => $rows[4],
                    'totalkeseluruhan' => $rows[5],
                    'biayaperawatan' => $rows[2],
                    'bhp' => $rows[11],
                    'dari' => "tko",
                    'statuscarm' => 0,
                    'tanggal_transaksi' => date("Y-m-d H:i:s"),
                    'iduser_transaksi' => $this->session->userdata('user_id'),
                )
            );
            $this->db->insert('tkamaroperasi_sewaalat');
        }

        return true;
    }

    /* @checklist &start */
    public function getstok($id, $tipe, $kategori = null, $kelompokpasien = null, $nama = null)
    {
        // @ 1 (Alat Kesehatan)
        if ($tipe == 1) {
            $this->getmargin($kelompokpasien);
            $this->db->join('mdata_alkes', 'mgudang_stok.idbarang = mdata_alkes.id', 'left')
                ->join('msatuan', 'mdata_alkes.idsatuanbesar = msatuan.id', 'left')
                ->select('mdata_alkes.kode, mdata_alkes.nama as namaalkes, msatuan.nama as satuan,mdata_alkes.hargadasar as harga, mdata_alkes.id as idalkes, msatuan.id as idsatuan,mdata_alkes.status');
        }
        // @ 2 (Implan)
        if ($tipe == 2) {
            $this->db->join('view_mdata_implant', 'mgudang_stok.idbarang = view_mdata_implant.id', 'left')
                ->select('view_mdata_implant.kode, view_mdata_implant.namaimplan, view_mdata_implant.hargadasar, view_mdata_implant.margin, view_mdata_implant.id');
        }
        // @ 3 (Narcose) & (Obat)
        // @----------------------------------------------------------------------------
        if ($tipe == 3) {
            $this->getmargin($kelompokpasien);
            $this->db->join('mdata_obat', 'mgudang_stok.idbarang = mdata_obat.id', 'left')
                ->join('msatuan', 'mdata_obat.idsatuanbesar = msatuan.id', 'left')
                ->select(
                    'mdata_obat.kode,
                  mdata_obat.nama as namaobat,
                  msatuan.nama as satuan,
                  mdata_obat.hargadasar as harga,
                  mdata_obat.id as idobat,
                  msatuan.id as idsatuan,
                  mdata_obat.status'
                );

            if ($kategori != null) {
                $this->db->where('mdata_obat.idkategori', $kategori);
            } else {
                $this->db->where('mdata_obat.idkategori !=', $kategori);
            }
            if ($nama != null) {
                $this->db->where('mdata_obat.nama', $nama);
            }
        }
        // @----------------------------------------------------------------------------

        $this->db->select('mgudang_stok.stok');
        $this->db->where(array('mgudang_stok.idunitpelayanan' => $id, 'mgudang_stok.idtipe' => $tipe));
        $query = $this->db->get('mgudang_stok');
        // print_r($this->db->last_query());exit();
        return $query->result();
    }
    /* @checklist &end */

    public function getstoksewaalat($kelas, $jenis, $nama = null)
    {
        if ($nama != null) {
            $this->db->where(array('nama' => $nama));
        }
        $this->db->where(array('kelas' => $kelas, 'idjenis' => $jenis));
        $query = $this->db->get('view_mdata_sewaalat');
        return $query->result();
    }

    public function deletesewaalat($id, $idko)
    {
        $this->db->where(array('idtindakan' => $id, 'id' => $idko));
        $this->db->delete('tkamaroperasi_sewaalat');
        return true;
    }

    // @JULY &start
    public function getbiayakamar($kelas, $jenis)
    {
        $this->db->where(array('kelas' => $kelas, 'idjenis' => $jenis));
        $query = $this->db->get('view_mdata_sewakamar');
        return $query->result();
    }

    public function getbiayajasmed($kelas, $jenis)
    {
        $this->db->where(array('kelas' => $kelas, 'idjenis' => $jenis));
        $query = $this->db->get('view_mdata_tindakanok');
        return $query->result();
    }
    // @JULY &end

    public function biayanarcose($id)
    {
        $this->db->select('sum(totalkeseluruhan) as biayanarcose')
            ->where('idtindakan', $id);
        $query = $this->db->get('tkamaroperasi_narcose');
        return $query->result();
    }

    public function biayaobat($id)
    {
        $this->db->select('sum(totalkeseluruhan) as biayaobat')
            ->where('idtindakan', $id);
        $query = $this->db->get('tkamaroperasi_obat');
        return $query->result();
    }

    public function biayaalkes($id)
    {
        $this->db->select('sum(totalkeseluruhan) as biayaalkes')
            ->where('idtindakan', $id);
        $query = $this->db->get('tkamaroperasi_alkes');
        return $query->result();
    }

    public function biayaimplan($id)
    {
        $this->db->select('sum(totalkeseluruhan) as biayaimplan')
            ->where('idtindakan', $id);
        $query = $this->db->get('tkamaroperasi_implan');
        return $query->result();
    }

    public function biayasewaalat($id)
    {
        $this->db->select('sum(totalkeseluruhan) as biayaalat')
            ->where('idtindakan', $id);
        $query = $this->db->get('tkamaroperasi_sewaalat');
        return $query->result();
    }

    // @getmargin
    public function getmargin($kelompokpasien)
    {
        switch ($kelompokpasien) {
            case 0:
                return $this->db->select('marginumum as margin');
                break;
            case 1:
                return $this->db->select('marginasuransi as margin');
                break;
            case 2:
                return $this->db->select('marginjasaraharja as margin');
                break;
            case 3:
                return $this->db->select('marginbpjskesehatan as margin');
                break;
            case 4:
                return $this->db->select('marginbpjstenagakerja as margin');
                break;
        }
    }

    public function gettanggalko($choose)
    {
        $this->db->select('tanggaloperasi')
            ->group_by('tanggaloperasi');
        if ($choose == 1) {
            $this->db->order_by('tanggaloperasi', 'DESC');
        } else {
            $this->db->order_by('tanggaloperasi', 'ASC');
        }
        $query = $this->db->get('tkamaroperasi_pendaftaran', 1);
        return $query->result();
    }
    // revision 28 &end

    /* @jasamedis (kamaroperasi) &start */
    public function savejasadokter($iddokter, $namadokter, $jenisdokter, $idtindakan, $nominal = null, $persen = null)
    {
        if ($jenisdokter == 1) {
            $data = array(
                'idtindakan' => $idtindakan,
                'iddokteroperator' => $iddokter,
                'namadokteroperator' => $namadokter,
                'nominal' => $nominal,
                'statusverif' => 0,
                'status' => 0,
                'tanggal_transaksi' => date("Y-m-d H:i:s"),
                'iduser_transaksi' => $this->session->userdata('user_id'),
            );
            $this->db->insert('tkamaroperasi_jasado', $data);
        } else {
            $data = array(
                'idtindakan' => $idtindakan,
                'iddokteranesthesi' => $iddokter,
                'namadokteranesthesi' => $namadokter,
                'persen' => $persen,
                'statusverif' => 0,
                'status' => 0,
                'tanggal_transaksi' => date("Y-m-d H:i:s"),
                'iduser_transaksi' => $this->session->userdata('user_id'),
            );
            $this->db->insert('tkamaroperasi_jasada', $data);
        }
        return true;
    }

    // @dokteroperator
    public function getdokteroperator($idtindakan, $iddokter = null)
    {
        if ($iddokter != null) {
            $this->db->where(['iddokteroperator' => $iddokter]);
        }
        $this->db->where(['idtindakan' => $idtindakan]);
        $query = $this->db->get('tkamaroperasi_jasado');
        return $query->result();
    }

    public function updatedokteroperator($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->set('version', 'version+1', false);
        $this->db->update('tkamaroperasi_jasado', $data);
        return true;
    }

    public function deletedokteroperator($id, $idtindakan)
    {
        $this->db->where(array('iddokteroperator' => $id, 'idtindakan' => $idtindakan));
        $this->db->delete('tkamaroperasi_jasado');
        return true;
    }

    // @dokteranesthesi
    public function getdokteranesthesi($idtindakan, $iddokter = null)
    {
        if ($iddokter != null) {
            $this->db->where(['iddokteranesthesi' => $iddokter]);
        }
        $this->db->where(['idtindakan' => $idtindakan]);
        $query = $this->db->get('tkamaroperasi_jasada');
        return $query->result();
    }

    public function updatedokteranesthesi($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->set('version', 'version+1', false);
        $this->db->update('tkamaroperasi_jasada', $data);
        return true;
    }

    public function deletedokteranesthesi($id, $idtindakan)
    {
        $this->db->where(['iddokteranesthesi' => $id, 'idtindakan' => $idtindakan]);
        $this->db->delete('tkamaroperasi_jasada');
        return true;
    }
    /* @jasamedis (kamaroperasi) &end */

    // @make:function tkamaroperasi_sewaalat
    public function getsewaalat($id)
    {
        $this->db->select('idalat, kuantitas');
        $this->db->where(['idtindakan' => $id, 'statusverifikasi' => 0]);
        $query = $this->db->get('tkamaroperasi_sewaalat');
        return $query->result();
    }

    // @make:function view_mdata_sewaalat
    public function getalat($idalat, $kelas, $jenis)
    {
        $this->db->select('jasasarana, jasapelayanan, bhp, biayaperawatan, total')
            ->where(['idtarif' => $idalat, 'kelas' => $kelas, 'idjenis' => $jenis]);
        $query = $this->db->get('view_mdata_sewaalat');
        return $query->result();
    }

    // @make:function updatesewaalat
    public function updatesewaalat($idtindakan, $idalat, $data)
    {
        $this->db->where(['idtindakan' => $idtindakan, 'idalat' => $idalat]);
        $this->db->update('tkamaroperasi_sewaalat', $data);
        return true;
    }

    // @make:function gettindakan
    public function gettindakan($id)
    {
        $this->db->select('kelasoperasi, jenisoperasi, biayapenggunaan, biayatotal')
            ->where('idpendaftaranoperasi', $id);
        $query = $this->db->get('tkamaroperasi_tindakan');
        return $query->result();
    }

    public function updatetindakan($id, $total)
    {
        $this->db->where('idpendaftaranoperasi', $id);
        $this->db->update('tkamaroperasi_tindakan', ['biayatotal' => $total]);
        return true;
    }

    public function getListUnitPelayanan()
    {
        $this->db->select('munitpelayanan.id, munitpelayanan.nama');
        $this->db->where('munitpelayanan_user.userid', $this->session->userdata('user_id'));
        $this->db->join('munitpelayanan', 'munitpelayanan_user.idunitpelayanan = munitpelayanan.id', 'left');
        $this->db->from('munitpelayanan_user');
        $query = $this->db->get();
        return $query->result();
    }

    public function getDefaultUnitPelayananUser()
    {
        $this->db->select('unitpelayananiddefault as idunitpelayanan');
        $this->db->from('musers');
        $this->db->where('id', $this->session->userdata('user_id'));
        $query = $this->db->get();
        return $query->row_array()['idunitpelayanan'];
    }

    public function setFullCareOK($idpendaftaran, $row)
    {
        $data = array(
            'idpendaftaran' => $idpendaftaran,
            'idtarif' => $row->idtarif,
            'namatarif' => $row->namatarif,
            'jasasarana' => $row->jasasarana,
            'jasapelayanan' => $row->jasapelayanan,
            'bhp' => $row->bhp,
            'biayaperawatan' => $row->biayaperawatan,
            'total' => $row->total,
            'totalkeseluruhan' => $row->total,
        );

        $insert_query = $this->db->insert_string('tkamaroperasi_fullcare', $data);
        $insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
        if ($this->db->query($insert_query)) {
            return true;
        } else {
            return false;
        }
    }
}
