<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Thonor_dokter_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getSpecifiedById($id)
	{
		$this->db->select('thonor_dokter.*, mdokter.npwp AS npwpdokter');
		$this->db->join('mdokter', 'mdokter.id = thonor_dokter.iddokter');
		$this->db->where('thonor_dokter.id', $id);
		$this->db->where('thonor_dokter.status', '1');
		$query = $this->db->get('thonor_dokter');
		return $query->row();
	}

	public function queryBuilderStatusHoldAsuransi($table)
	{
		return "(CASE
			WHEN thonor_dokter_detail.status_asuransi = 1 AND setting_penyerahan_asuransi.$table IS NULL
			THEN 1
			WHEN thonor_dokter_detail.status_asuransi = 1 AND setting_penyerahan_asuransi.$table = 1
			THEN 0
			WHEN thonor_dokter_detail.status_asuransi = 1 AND setting_penyerahan_asuransi.$table = 2
			THEN IF (COALESCE(MIN(tklaim_detail.status_lunas), 0) = 1, 0, 1)
			WHEN thonor_dokter_detail.status_asuransi = 0
			THEN 0
		END)";
	}

	public function queryBuilderStatusHoldBerkasAnalisa($table)
	{
		return "(CASE
			WHEN COALESCE(berkas_analisa.status_hold, 0) = 1 AND setting_penyerahan_hold.$table IS NULL
			THEN 1
			WHEN COALESCE(berkas_analisa.status_hold, 0) = 1 AND setting_penyerahan_hold.$table = 1
			THEN 0
			WHEN COALESCE(berkas_analisa.status_hold, 0) = 1 AND setting_penyerahan_hold.$table = 2
			THEN 1
			WHEN COALESCE(berkas_analisa.status_hold, 0) = 0
			THEN 0
		END)";
	}

	public function queryBuilderBerkasAnalisa()
	{
		return "SELECT
			trm_berkas_analisa.idberkas,
			trm_berkas_analisa.idpendaftaran,
			trm_berkas_analisa.asalpendaftaran,
			trm_berkas_analisa_dokter.iddokter,
			trm_berkas_analisa_dokter.namadokter,
			IF(trm_berkas_analisa.hold = 0, 0,
			IF(
				SUM(CASE
				WHEN trm_berkas_analisa.kriteria_nilai = '+' THEN 1
				WHEN trm_berkas_analisa.kriteria_nilai = '-' THEN -1
				WHEN trm_berkas_analisa.kriteria_nilai = '=' THEN 0
				END) = COUNT(trm_berkas_analisa.idlembaran),
				0, 1
			)
			) AS status_hold
		FROM
			trm_berkas_analisa
		JOIN trm_berkas_analisa_dokter ON trm_berkas_analisa_dokter.idberkas = trm_berkas_analisa.idberkas AND trm_berkas_analisa_dokter.idanalisa = trm_berkas_analisa.idanalisa AND trm_berkas_analisa_dokter.idlembaran = trm_berkas_analisa.idlembaran
		GROUP BY
			trm_berkas_analisa_dokter.idberkas,
			trm_berkas_analisa_dokter.idlembaran,
			trm_berkas_analisa_dokter.iddokter";
	}

	public function calcTotalPendapatan($id, $statusHold)
	{
		$query = $this->db->query('SELECT
			SUM(COALESCE(result.jasamedis, 0)) AS total_pendapatan_brutto,
			SUM(COALESCE(result.nominal_potongan_rs, 0)) AS total_potongan_rs,
			SUM(COALESCE(result.nominal_pajak_dokter, 0)) AS total_pajak_dokter,
			SUM(COALESCE(result.nominal_potongan_perujuk, 0)) AS total_potongan_perujuk
		FROM
		(
			SELECT
			jenis_transaksi,
			jenis_tindakan,
			reference_table,
			jasamedis,
			nominal_potongan_rs,
			nominal_pajak_dokter,
			nominal_potongan_perujuk,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_rajal') . ' AS status_hold_asuransi_rajal,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_rajal') . ' AS status_hold_berkas_rajal,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_rujukan_ranap') . ' AS status_hold_asuransi_rujukan_ranap,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_rujukan_ranap') . ' AS status_hold_berkas_rujukan_ranap,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_ranap') . ' AS status_hold_asuransi_ranap,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_ranap') . ' AS status_hold_berkas_ranap,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_radiologi') . ' AS status_hold_asuransi_radiologi,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_radiologi') . ' AS status_hold_berkas_radiologi,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_laboratorium') . ' AS status_hold_asuransi_laboratorium,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_laboratorium') . ' AS status_hold_berkas_laboratorium,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_fisioterapi') . ' AS status_hold_asuransi_fisioterapi,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_fisioterapi') . ' AS status_hold_berkas_fisioterapi,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_anesthesi') . ' AS status_hold_asuransi_anesthesi,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_anesthesi') . " AS status_hold_berkas_anesthesi
			FROM
			thonor_dokter_detail
			LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_asuransi ON setting_penyerahan_asuransi.idtipe = 1 AND setting_penyerahan_asuransi.idkategori = thonor_dokter_detail.idkategori
			LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_hold ON setting_penyerahan_hold.idtipe = 2 AND setting_penyerahan_hold.idkategori = thonor_dokter_detail.idkategori
			LEFT JOIN tklaim_detail ON ((thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND tklaim_detail.tipe = '1') OR (thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND tklaim_detail.tipe = '2')) AND tklaim_detail.pendaftaran_id = thonor_dokter_detail.idtransaksi
			LEFT JOIN tklaim ON tklaim.id = tklaim_detail.klaim_id
			LEFT JOIN (" . $this->queryBuilderBerkasAnalisa() . ") AS berkas_analisa ON ( ( thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND berkas_analisa.asalpendaftaran = 'rawatjalan' ) OR ( thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND berkas_analisa.asalpendaftaran = 'rawatinap' ) ) AND thonor_dokter_detail.idtransaksi = berkas_analisa.idpendaftaran AND thonor_dokter_detail.iddokter = berkas_analisa.iddokter
			WHERE
			((thonor_dokter_detail.idhonor = '$id' AND thonor_dokter_detail.idhonor_selanjutnya IS NULL) OR thonor_dokter_detail.idhonor_selanjutnya = '$id')
			AND thonor_dokter_detail.jenis_transaksi != 'pengeluaran'
			AND thonor_dokter_detail.status_otherincome = '0'
			GROUP BY
			thonor_dokter_detail.id
		) AS result
		WHERE
			IF ( result.jenis_transaksi = 'poliklinik' AND result.jenis_tindakan = 'JASA DOKTER RAWAT JALAN', IF ( (result.status_hold_asuransi_rajal + result.status_hold_berkas_rajal) <> 0, 1, 0), 0 ) = '$statusHold' AND
			IF ( result.jenis_transaksi = 'rawatinap' AND ( result.jenis_tindakan = 'JASA DOKTER FEE RUJUKAN' OR result.jenis_tindakan = 'JASA DOKTER RAWAT JALAN' ), IF ( (result.status_hold_asuransi_rujukan_ranap + result.status_hold_berkas_rujukan_ranap) <> 0, 1, 0), 0 ) = '$statusHold' AND
			IF ( result.jenis_tindakan IN ('JASA DOKTER RAWAT INAP (FULL CARE)', 'JASA DOKTER RAWAT INAP (ECG)', 'JASA DOKTER RAWAT INAP (AMBULANCE)', 'JASA DOKTER RAWAT INAP (SEWA ALAT)', 'JASA DOKTER RAWAT INAP (LAIN LAIN)', 'JASA DOKTER VISITE', 'JASA DOKTER OPERATOR'), IF ( (result.status_hold_asuransi_ranap + result.status_hold_berkas_ranap) <> 0, 1, 0), 0 ) = '$statusHold' AND
			IF ( result.reference_table = 'trujukan_radiologi_detail', IF ( (result.status_hold_asuransi_radiologi + result.status_hold_berkas_radiologi) <> 0, 1, 0), 0 ) = '$statusHold' AND
			IF ( result.reference_table = 'trujukan_laboratorium_detail', IF ( (result.status_hold_asuransi_laboratorium + result.status_hold_berkas_laboratorium) <> 0, 1, 0), 0 ) = '$statusHold' AND
			IF ( result.reference_table = 'trujukan_fisioterapi_detail', IF ( (result.status_hold_asuransi_fisioterapi + result.status_hold_berkas_fisioterapi) <> 0, 1, 0), 0 ) = '$statusHold' AND
			IF ( result.jenis_tindakan = 'JASA DOKTER ANESTHESI', IF ( (result.status_hold_asuransi_anesthesi + result.status_hold_berkas_anesthesi) <> 0, 1, 0), 0 ) = '$statusHold'
		");

		return $query->row();
	}

	public function calcTotalPengeluaran($id, $statusHold)
	{
		$query = $this->db->query('SELECT
			SUM(COALESCE(result.jasamedis_netto, 0)) AS total_potongan_pribadi
		FROM
		(
			SELECT
			jenis_transaksi,
			jenis_tindakan,
			reference_table,
			jasamedis_netto,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_rajal') . ' AS status_hold_asuransi_rajal,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_rajal') . ' AS status_hold_berkas_rajal,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_rujukan_ranap') . ' AS status_hold_asuransi_rujukan_ranap,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_rujukan_ranap') . ' AS status_hold_berkas_rujukan_ranap,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_ranap') . ' AS status_hold_asuransi_ranap,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_ranap') . ' AS status_hold_berkas_ranap,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_radiologi') . ' AS status_hold_asuransi_radiologi,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_radiologi') . ' AS status_hold_berkas_radiologi,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_laboratorium') . ' AS status_hold_asuransi_laboratorium,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_laboratorium') . ' AS status_hold_berkas_laboratorium,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_fisioterapi') . ' AS status_hold_asuransi_fisioterapi,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_fisioterapi') . ' AS status_hold_berkas_fisioterapi,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_anesthesi') . ' AS status_hold_asuransi_anesthesi,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_anesthesi') . " AS status_hold_berkas_anesthesi
			FROM
			thonor_dokter_detail
			LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_asuransi ON setting_penyerahan_asuransi.idtipe = 1 AND setting_penyerahan_asuransi.idkategori = thonor_dokter_detail.idkategori
			LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_hold ON setting_penyerahan_hold.idtipe = 2 AND setting_penyerahan_hold.idkategori = thonor_dokter_detail.idkategori
			LEFT JOIN tklaim_detail ON ((thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND tklaim_detail.tipe = '1') OR (thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND tklaim_detail.tipe = '2')) AND tklaim_detail.pendaftaran_id = thonor_dokter_detail.idtransaksi
			LEFT JOIN tklaim ON tklaim.id = tklaim_detail.klaim_id
			LEFT JOIN (" . $this->queryBuilderBerkasAnalisa() . ") AS berkas_analisa ON ( ( thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND berkas_analisa.asalpendaftaran = 'rawatjalan' ) OR ( thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND berkas_analisa.asalpendaftaran = 'rawatinap' ) ) AND thonor_dokter_detail.idtransaksi = berkas_analisa.idpendaftaran AND thonor_dokter_detail.iddokter = berkas_analisa.iddokter
			WHERE
			((thonor_dokter_detail.idhonor = '$id' AND thonor_dokter_detail.idhonor_selanjutnya IS NULL) OR thonor_dokter_detail.idhonor_selanjutnya = '$id')
			AND thonor_dokter_detail.jenis_transaksi = 'pengeluaran'
			AND thonor_dokter_detail.status_otherincome = '0'
			GROUP BY
			thonor_dokter_detail.id
		) AS result
		WHERE
			IF ( result.jenis_transaksi = 'poliklinik' AND result.jenis_tindakan = 'JASA DOKTER RAWAT JALAN', IF ( (result.status_hold_asuransi_rajal + result.status_hold_berkas_rajal) <> 0, 1, 0), 0 ) = '$statusHold' AND
			IF ( result.jenis_transaksi = 'rawatinap' AND ( result.jenis_tindakan = 'JASA DOKTER FEE RUJUKAN' OR result.jenis_tindakan = 'JASA DOKTER RAWAT JALAN' ), IF ( (result.status_hold_asuransi_rujukan_ranap + result.status_hold_berkas_rujukan_ranap) <> 0, 1, 0), 0 ) = '$statusHold' AND
			IF ( result.jenis_tindakan IN ('JASA DOKTER RAWAT INAP (FULL CARE)', 'JASA DOKTER RAWAT INAP (ECG)', 'JASA DOKTER RAWAT INAP (AMBULANCE)', 'JASA DOKTER RAWAT INAP (SEWA ALAT)', 'JASA DOKTER RAWAT INAP (LAIN LAIN)', 'JASA DOKTER VISITE', 'JASA DOKTER OPERATOR'), IF ( (result.status_hold_asuransi_ranap + result.status_hold_berkas_ranap) <> 0, 1, 0), 0 ) = '$statusHold' AND
			IF ( result.reference_table = 'trujukan_radiologi_detail', IF ( (result.status_hold_asuransi_radiologi + result.status_hold_berkas_radiologi) <> 0, 1, 0), 0 ) = '$statusHold' AND
			IF ( result.reference_table = 'trujukan_laboratorium_detail', IF ( (result.status_hold_asuransi_laboratorium + result.status_hold_berkas_laboratorium) <> 0, 1, 0), 0 ) = '$statusHold' AND
			IF ( result.reference_table = 'trujukan_fisioterapi_detail', IF ( (result.status_hold_asuransi_fisioterapi + result.status_hold_berkas_fisioterapi) <> 0, 1, 0), 0 ) = '$statusHold' AND
			IF ( result.jenis_tindakan = 'JASA DOKTER ANESTHESI', IF ( (result.status_hold_asuransi_anesthesi + result.status_hold_berkas_anesthesi) <> 0, 1, 0), 0 ) = '$statusHold'
		");

		return $query->row();
	}

	public function getPrintOutRadiologi($id)
	{
		$result = $this->db->query("SELECT
			thonor_dokter_detail.tanggal_pemeriksaan,
			(CASE
				WHEN thonor_dokter_detail.jenis_transaksi = 'poliklinik' THEN
				tpoliklinik_pendaftaran.no_medrec
				WHEN thonor_dokter_detail.jenis_transaksi = 'rawatinap' THEN
				trawatinap_pendaftaran.no_medrec
			END) AS nomedrec,
			(CASE
				WHEN thonor_dokter_detail.jenis_transaksi = 'poliklinik' THEN
				tpoliklinik_pendaftaran.namapasien
				WHEN thonor_dokter_detail.jenis_transaksi = 'rawatinap' THEN
				trawatinap_pendaftaran.namapasien
			END) AS namapasien,
			thonor_dokter_detail.namakelompok,
			(CASE
			WHEN trujukan_radiologi_detail.idtipe = '1' THEN
				'X-Ray'
			WHEN trujukan_radiologi_detail.idtipe = '2' THEN
				'USG'
			WHEN trujukan_radiologi_detail.idtipe = '3' THEN
				'CT Scan'
			WHEN trujukan_radiologi_detail.idtipe = '4' THEN
				'MRI'
			WHEN trujukan_radiologi_detail.idtipe = '5' THEN
				'BMD'
			END) AS jenispemeriksaan,
			thonor_dokter_detail.namatarif AS namapemeriksaan,
			(CASE
			WHEN thonor_dokter_detail.status_expertise = '1' THEN
				'Langsung'
			WHEN thonor_dokter_detail.status_expertise = '2' THEN
				'Menolak'
			WHEN thonor_dokter_detail.status_expertise = '3' THEN
				'Kembali'
			ELSE
				'-'
			END) AS status_expertise,
			thonor_dokter_detail.jasamedis,
			thonor_dokter_detail.potongan_rs,
			thonor_dokter_detail.nominal_potongan_rs,
			thonor_dokter_detail.pajak_dokter,
			thonor_dokter_detail.nominal_pajak_dokter,
			thonor_dokter_detail.jasamedis_netto
		FROM
			thonor_dokter_detail
		LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = thonor_dokter_detail.idtransaksi AND thonor_dokter_detail.jenis_transaksi = 'rawatinap'
		LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = thonor_dokter_detail.idtransaksi AND thonor_dokter_detail.jenis_transaksi = 'poliklinik'
		LEFT JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
		LEFT JOIN trujukan_radiologi_detail ON trujukan_radiologi_detail.id = thonor_dokter_detail.iddetail
		WHERE
			thonor_dokter_detail.reference_table = 'trujukan_radiologi_detail' AND
			thonor_dokter_detail.idhonor = $id")->result();
		//LEFT JOIN tpoliklinik_pendaftaran ON (tpoliklinik_pendaftaran.id = thonor_dokter_detail.idtransaksi AND thonor_dokter_detail.jenis_transaksi = 'poliklinik') OR tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
		
		return $result;
	}

	public function getPrintOutRawatJalan($id)
	{
		$result = $this->db->query("SELECT
			thonor_dokter_detail.tanggal_pemeriksaan,
			(CASE
				WHEN thonor_dokter_detail.jenis_transaksi = 'poliklinik' THEN
				tpoliklinik_pendaftaran.no_medrec
				WHEN thonor_dokter_detail.jenis_transaksi = 'rawatinap' THEN
				trawatinap_pendaftaran.no_medrec
			END) AS nomedrec,
			(CASE
				WHEN thonor_dokter_detail.jenis_transaksi = 'poliklinik' THEN
				tpoliklinik_pendaftaran.namapasien
				WHEN thonor_dokter_detail.jenis_transaksi = 'rawatinap' THEN
				trawatinap_pendaftaran.namapasien
			END) AS namapasien,
			thonor_dokter_detail.namakelompok,
			thonor_dokter_detail.namarekanan,
			thonor_dokter_detail.jenis_pasien AS jenispasien,
			thonor_dokter_detail.namatarif AS namapemeriksaan,
			thonor_dokter_detail.jasamedis,
			thonor_dokter_detail.potongan_rs,
			thonor_dokter_detail.nominal_potongan_rs,
			thonor_dokter_detail.pajak_dokter,
			thonor_dokter_detail.nominal_pajak_dokter,
			thonor_dokter_detail.jasamedis_netto
		FROM
			thonor_dokter_detail
		LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = thonor_dokter_detail.idtransaksi AND thonor_dokter_detail.jenis_transaksi = 'rawatinap'
		LEFT JOIN tpoliklinik_pendaftaran ON (tpoliklinik_pendaftaran.id = thonor_dokter_detail.idtransaksi AND thonor_dokter_detail.jenis_transaksi = 'poliklinik') OR tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
		LEFT JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
		LEFT JOIN trujukan_radiologi_detail ON trujukan_radiologi_detail.id = thonor_dokter_detail.iddetail
		WHERE
			thonor_dokter_detail.reference_table = 'tpoliklinik_pelayanan' AND
			thonor_dokter_detail.idhonor = $id")->result();

		return $result;
	}

	public function getDokterOperator($id)
	{
		$result = $this->db->query("SELECT
			tkamaroperasi_jasado.iddokteroperator AS iddokter
		FROM
			tkamaroperasi_jasado
			JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_jasado.idpendaftaranoperasi
			JOIN tkamaroperasi_tindakan ON tkamaroperasi_tindakan.idpendaftaranoperasi = tkamaroperasi_tindakan.idpendaftaranoperasi
			LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran  AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
			LEFT JOIN trawatinap_pendaftaran ON ( trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 2 ) OR ( trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id )
			JOIN mjenis_operasi ON mjenis_operasi.id = tkamaroperasi_pendaftaran.jenis_operasi_id
			JOIN mdokter ON mdokter.id = tkamaroperasi_jasado.iddokteroperator
			JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
		WHERE
			trawatinap_pendaftaran.id = $id
		GROUP BY
			tkamaroperasi_jasado.iddokteroperator")->result();

		$data = [];
		foreach ($result as $index => $row) {
			$data[$index] = $row->iddokter;
		}

		return $data;
	}

	public function getPendapatanRawatJalan($data)
	{
		$idhonor = $data['idhonor'];
		$status_periode = $data['status_periode']; // Periode Saat Ini or Periode Sebelumnya
		$jenis_pasien = $data['jenis_pasien']; // Pasien RS or Pasien Pribadi
		$idkelompok = implode(',', $data['idkelompok']); // Kelompok Pasien
		$status_hold = $data['status_hold']; // Status Hold

		$query = $this->db->query('SELECT
			GROUP_CONCAT(list_id) AS list_id,
			tanggal_pemeriksaan,
			namakelompok,
			SUM(jasamedis) AS jasamedis,
			SUM(nominal_potongan_rs) AS nominal_potongan_rs,
			SUM(nominal_pajak_dokter) AS nominal_pajak_dokter,
			SUM(jasamedis_netto) AS jasamedis_netto,
			status_hold_asuransi,
			status_hold_berkas
		FROM
		(
			SELECT
			GROUP_CONCAT(thonor_dokter_detail.id) AS list_id,
			thonor_dokter_detail.tanggal_pemeriksaan,
			thonor_dokter_detail.idkelompok,
			(CASE
				WHEN thonor_dokter_detail.idkelompok = 1 THEN
					CONCAT(thonor_dokter_detail.namakelompok, " <br><b>", thonor_dokter_detail.namarekanan,"</b>")
				ELSE thonor_dokter_detail.namakelompok
			END) AS namakelompok,
			thonor_dokter_detail.jasamedis,
			thonor_dokter_detail.nominal_potongan_rs,
			thonor_dokter_detail.nominal_pajak_dokter,
			thonor_dokter_detail.jasamedis_netto,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_rajal') . ' AS status_hold_asuransi,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_rajal') . " AS status_hold_berkas
			FROM
			thonor_dokter_detail
			LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_asuransi ON setting_penyerahan_asuransi.idtipe = 1 AND setting_penyerahan_asuransi.idkategori = thonor_dokter_detail.idkategori
			LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_hold ON setting_penyerahan_hold.idtipe = 2 AND setting_penyerahan_hold.idkategori = thonor_dokter_detail.idkategori
			LEFT JOIN tklaim_detail ON ( ( thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND tklaim_detail.tipe = '1' ) OR ( thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND tklaim_detail.tipe = '2' ) ) AND tklaim_detail.pendaftaran_id = thonor_dokter_detail.idtransaksi
			LEFT JOIN tklaim ON tklaim.id = tklaim_detail.klaim_id
			LEFT JOIN (" . $this->queryBuilderBerkasAnalisa() . ") AS berkas_analisa ON ( ( thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND berkas_analisa.asalpendaftaran = 'rawatjalan' ) OR ( thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND berkas_analisa.asalpendaftaran = 'rawatinap' ) ) AND thonor_dokter_detail.idtransaksi = berkas_analisa.idpendaftaran AND thonor_dokter_detail.iddokter = berkas_analisa.iddokter
			WHERE
			thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND
			thonor_dokter_detail.jenis_tindakan = 'JASA DOKTER RAWAT JALAN' AND
			thonor_dokter_detail.status_otherincome = '0' AND
			thonor_dokter_detail.jenis_pasien = '$jenis_pasien' AND
			thonor_dokter_detail.idkelompok IN ($idkelompok) AND
			(CASE
				WHEN '$status_periode' = 'Periode Sebelumnya' THEN
				thonor_dokter_detail.idhonor_selanjutnya = '$idhonor'
				WHEN '$status_periode' = 'Periode Saat Ini' THEN
				thonor_dokter_detail.idhonor = '$idhonor'
				AND thonor_dokter_detail.idhonor_selanjutnya IS NULL
			END)
			GROUP BY
				thonor_dokter_detail.id
			ORDER BY
				thonor_dokter_detail.tanggal_pemeriksaan,
				thonor_dokter_detail.namakelompok,
				thonor_dokter_detail.namarekanan
		) AS result
		WHERE
			IF ( (result.status_hold_asuransi + result.status_hold_berkas) <> 0, 1, 0) = '$status_hold'
		GROUP BY
			result.tanggal_pemeriksaan,
			result.idkelompok,
			result.status_hold_asuransi,
			result.status_hold_berkas");

		return $query->result();
	}

	public function getPendapatanRujukanRawatInap($data)
	{
		$idhonor = $data['idhonor'];
		$jenis_section = $data['jenis_section']; // Operasi or Konservatif
		$status_hold = $data['status_hold']; // Status Hold

		$query = $this->db->query('SELECT
			GROUP_CONCAT(iddetail) AS list_id,
			tanggal_pemeriksaan,
			asalpasien,
			namapoliklinik,
			namapasien,
			idkelompok,
			namakelompok,
			SUM(jasamedis) AS jasamedis,
			SUM(potongan_rs) AS potongan_rs,
			SUM(nominal_potongan_rs) AS nominal_potongan_rs,
			SUM(potongan_perujuk) AS potongan_perujuk,
			SUM(nominal_potongan_perujuk) AS nominal_potongan_perujuk,
			SUM(pajak_dokter) AS pajak_dokter,
			SUM(nominal_pajak_dokter) AS nominal_pajak_dokter,
			SUM(jasamedis_netto) AS jasamedis_netto
		FROM
		(
			SELECT
				thonor_dokter_detail.idtransaksi,
				thonor_dokter_detail.id AS iddetail,
				thonor_dokter_detail.tanggal_pemeriksaan,
				thonor_dokter_detail.jenis_pasien AS asalpasien,
				mpoliklinik.nama AS namapoliklinik,
				trawatinap_pendaftaran.namapasien,
				thonor_dokter_detail.idkelompok,
				(CASE
					WHEN thonor_dokter_detail.idkelompok = 1 THEN
						CONCAT(thonor_dokter_detail.namakelompok, " <br><b>", thonor_dokter_detail.namarekanan,"</b>")
					ELSE thonor_dokter_detail.namakelompok
				END) AS namakelompok,
				thonor_dokter_detail.jasamedis,
				thonor_dokter_detail.potongan_rs,
				thonor_dokter_detail.nominal_potongan_rs,
				thonor_dokter_detail.potongan_perujuk,
				thonor_dokter_detail.nominal_potongan_perujuk,
				thonor_dokter_detail.pajak_dokter,
				thonor_dokter_detail.nominal_pajak_dokter,
				thonor_dokter_detail.jasamedis_netto,
				' . $this->queryBuilderStatusHoldAsuransi('pendapatan_rujukan_ranap') . ' AS status_hold_asuransi,
				' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_rujukan_ranap') . " AS status_hold_berkas
			FROM
				thonor_dokter_detail
			JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = thonor_dokter_detail.idtransaksi AND thonor_dokter_detail.jenis_transaksi = 'rawatinap'
			JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
			LEFT JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
			LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_asuransi ON setting_penyerahan_asuransi.idtipe = 1 AND setting_penyerahan_asuransi.idkategori = thonor_dokter_detail.idkategori
			LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_hold ON setting_penyerahan_hold.idtipe = 2 AND setting_penyerahan_hold.idkategori = thonor_dokter_detail.idkategori
			LEFT JOIN tklaim_detail ON ( ( thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND tklaim_detail.tipe = '1' ) OR ( thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND tklaim_detail.tipe = '2' ) ) AND tklaim_detail.pendaftaran_id = thonor_dokter_detail.idtransaksi
			LEFT JOIN tklaim ON tklaim.id = tklaim_detail.klaim_id
			LEFT JOIN (" . $this->queryBuilderBerkasAnalisa() . ") AS berkas_analisa ON ( ( thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND berkas_analisa.asalpendaftaran = 'rawatjalan' ) OR ( thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND berkas_analisa.asalpendaftaran = 'rawatinap' ) ) AND thonor_dokter_detail.idtransaksi = berkas_analisa.idpendaftaran AND thonor_dokter_detail.iddokter = berkas_analisa.iddokter
			WHERE
				thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND
				thonor_dokter_detail.status_otherincome = '0' AND
				((thonor_dokter_detail.idhonor = '$idhonor' AND thonor_dokter_detail.idhonor_selanjutnya IS NULL) OR thonor_dokter_detail.idhonor_selanjutnya = '$idhonor') AND
				(CASE
					WHEN '$jenis_section' = 'Operasi' THEN
					thonor_dokter_detail.jenis_tindakan = 'JASA DOKTER FEE RUJUKAN'
					WHEN '$jenis_section' = 'Konservatif' THEN
					thonor_dokter_detail.jenis_tindakan = 'JASA DOKTER RAWAT JALAN'
				END)
			GROUP BY
				thonor_dokter_detail.id
			ORDER BY
				thonor_dokter_detail.tanggal_pemeriksaan,
				thonor_dokter_detail.namakelompok
		) AS result
		WHERE
			IF ( (result.status_hold_asuransi + result.status_hold_berkas) <> 0, 1, 0) = '$status_hold'
		GROUP BY
			result.idtransaksi,
			result.tanggal_pemeriksaan,
			result.idkelompok,
			result.status_hold_asuransi,
			result.status_hold_berkas");

		return $query->result();
	}

	public function getPendapatanRawatInap($data)
	{
		$idhonor = $data['idhonor'];
		$status_periode = $data['status_periode']; // Periode Saat Ini or Periode Sebelumnya
		$jenis_pasien = $data['jenis_pasien']; // Pasien RS or Pasien Pribadi
		$section_operasi = $data['section_operasi']; // Operasi or Konservatif
		$status_hold = $data['status_hold']; // Status Hold

		$jenis_tindakan = implode("','", [
			'JASA DOKTER RAWAT INAP (FULL CARE)',
			'JASA DOKTER RAWAT INAP (ECG)',
			'JASA DOKTER RAWAT INAP (AMBULANCE)',
			'JASA DOKTER RAWAT INAP (SEWA ALAT)',
			'JASA DOKTER RAWAT INAP (LAIN LAIN)',
			'JASA DOKTER VISITE',
			'JASA DOKTER OPERATOR'
		]);

		$query = $this->db->query('SELECT
			list_id,
			tanggal_pemeriksaan,
			asalpasien,
			namapoliklinik,
			namapasien,
			namakelompok,
			jasamedis AS jasamedis,
			potongan_rs AS potongan_rs,
			nominal_potongan_rs AS nominal_potongan_rs,
			nominal_potongan_perujuk AS nominal_potongan_perujuk,
			pajak_dokter AS pajak_dokter,
			nominal_pajak_dokter AS nominal_pajak_dokter,
			jasamedis_netto AS jasamedis_netto
		FROM
		(
			SELECT
				temp.idtransaksi,
				GROUP_CONCAT(temp.id) AS list_id,
				temp.tanggal_pemeriksaan,
				temp.asalpasien,
				temp.namapoliklinik,
				temp.namapasien,
				temp.idkelompok,
				temp.namakelompok,
				SUM(temp.jasamedis) AS jasamedis,
				SUM(temp.potongan_rs) AS potongan_rs,
				SUM(temp.nominal_potongan_rs) AS nominal_potongan_rs,
				SUM(temp.nominal_potongan_perujuk) AS nominal_potongan_perujuk,
				SUM(temp.pajak_dokter) AS pajak_dokter,
				SUM(temp.nominal_pajak_dokter) AS nominal_pajak_dokter,
				SUM(temp.jasamedis_netto) AS jasamedis_netto,
				temp.status_hold_asuransi,
				temp.status_hold_berkas
			FROM
			(
				SELECT
					thonor_dokter_detail.id,
					thonor_dokter_detail.tanggal_pemeriksaan,
					thonor_dokter_detail.jenis_pasien AS asalpasien,
					mpoliklinik.nama AS namapoliklinik,
					trawatinap_pendaftaran.namapasien,
					thonor_dokter_detail.idkelompok,
					(CASE
						WHEN thonor_dokter_detail.idkelompok = 1 THEN
							CONCAT(thonor_dokter_detail.namakelompok, " <br><b>", thonor_dokter_detail.namarekanan,"</b>")
						ELSE thonor_dokter_detail.namakelompok
					END) AS namakelompok,
					thonor_dokter_detail.jasamedis AS jasamedis,
					thonor_dokter_detail.potongan_rs AS potongan_rs,
					thonor_dokter_detail.nominal_potongan_rs AS nominal_potongan_rs,
					thonor_dokter_detail.nominal_potongan_perujuk AS nominal_potongan_perujuk,
					thonor_dokter_detail.pajak_dokter AS pajak_dokter,
					thonor_dokter_detail.nominal_pajak_dokter AS nominal_pajak_dokter,
					thonor_dokter_detail.jasamedis_netto AS jasamedis_netto,
					' . $this->queryBuilderStatusHoldAsuransi('pendapatan_ranap') . ' AS status_hold_asuransi,
					' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_ranap') . " AS status_hold_berkas,
					thonor_dokter_detail.idtransaksi
				FROM
					thonor_dokter_detail
				JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = thonor_dokter_detail.idtransaksi
				LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
				LEFT JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
				LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_asuransi ON setting_penyerahan_asuransi.idtipe = 1 AND setting_penyerahan_asuransi.idkategori = thonor_dokter_detail.idkategori
				LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_hold ON setting_penyerahan_hold.idtipe = 2 AND setting_penyerahan_hold.idkategori = thonor_dokter_detail.idkategori
				LEFT JOIN tklaim_detail ON ( ( thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND tklaim_detail.tipe = '1' ) OR ( thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND tklaim_detail.tipe = '2' ) ) AND tklaim_detail.pendaftaran_id = thonor_dokter_detail.idtransaksi
				LEFT JOIN tklaim ON tklaim.id = tklaim_detail.klaim_id
				LEFT JOIN (" . $this->queryBuilderBerkasAnalisa() . ") AS berkas_analisa ON ( ( thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND berkas_analisa.asalpendaftaran = 'rawatjalan' ) OR ( thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND berkas_analisa.asalpendaftaran = 'rawatinap' ) ) AND thonor_dokter_detail.idtransaksi = berkas_analisa.idpendaftaran AND thonor_dokter_detail.iddokter = berkas_analisa.iddokter
				WHERE
					thonor_dokter_detail.jenis_pasien = '$jenis_pasien' AND
					thonor_dokter_detail.jenis_tindakan IN ('$jenis_tindakan') AND
					(CASE
						WHEN '$status_periode' = 'Periode Sebelumnya' THEN
						thonor_dokter_detail.idhonor_selanjutnya = '$idhonor'
						WHEN '$status_periode' = 'Periode Saat Ini' THEN
						thonor_dokter_detail.idhonor = '$idhonor'
						AND thonor_dokter_detail.idhonor_selanjutnya IS NULL
						AND thonor_dokter_detail.section_operasi = '$section_operasi'
					END)
				GROUP BY
					thonor_dokter_detail.idtransaksi,
					thonor_dokter_detail.id
			ORDER BY
				thonor_dokter_detail.tanggal_pemeriksaan,
				thonor_dokter_detail.namakelompok
			) AS temp
			GROUP BY
				temp.idtransaksi
		) AS result
		WHERE
			IF ( (result.status_hold_asuransi + result.status_hold_berkas) <> 0, 1, 0) = '$status_hold'
		GROUP BY
			result.idtransaksi,
			result.tanggal_pemeriksaan,
			result.idkelompok,
			result.status_hold_asuransi,
			result.status_hold_berkas");

		return $query->result();
	}

	public function getPendapatanRadiologi($data)
	{
		$idhonor = $data['idhonor'];
		$status_periode = $data['status_periode']; // Periode Saat Ini or Periode Sebelumnya
		$idtipe_radiologi = $data['idtipe_radiologi']; // X-RAY, MRI, USG, CT-SCAN, BMD
		$status_expertise = $data['status_expertise']; // Langsung. Menolak or Kembali
		$status_hold = $data['status_hold']; // Status Hold

		$whereTipeRadiologi = ($idtipe_radiologi != '' ? "mtarif_radiologi.idtipe = '$idtipe_radiologi' AND" : '');
		$whereStatusExpertise = ($idtipe_radiologi == '1' ? "AND thonor_dokter_detail.status_expertise = '$status_expertise'" : '');

		$query = $this->db->query('SELECT
			list_id,
			tanggal_pemeriksaan,
			idkelompok,
			namakelompok,
			jumlah_pemeriksaan,
			jasamedis,
			idtipe_radiologi,
			nominal_potongan_rs,
			nominal_pajak_dokter,
			jasamedis_netto,
			status_hold_asuransi,
			status_hold_berkas
		FROM
		(
			SELECT
				GROUP_CONCAT(temp.id) AS list_id,
				temp.tanggal_pemeriksaan,
				temp.idkelompok,
				temp.namakelompok,
				COUNT(temp.id) AS jumlah_pemeriksaan,
				SUM(temp.jasamedis) AS jasamedis,
				temp.idtipe_radiologi,
				SUM(temp.nominal_potongan_rs) AS nominal_potongan_rs,
				SUM(temp.nominal_pajak_dokter) AS nominal_pajak_dokter,
				SUM(temp.jasamedis_netto) AS jasamedis_netto,
				temp.status_hold_asuransi,
				temp.status_hold_berkas
			FROM
			(
				SELECT
				thonor_dokter_detail.id,
				thonor_dokter_detail.tanggal_pemeriksaan,
				thonor_dokter_detail.idkelompok,
				(CASE
					WHEN thonor_dokter_detail.idkelompok = 1 THEN
						CONCAT(thonor_dokter_detail.namakelompok, " <br><b>", thonor_dokter_detail.namarekanan,"</b>")
					ELSE thonor_dokter_detail.namakelompok
				END) AS namakelompok,
				thonor_dokter_detail.id AS jumlah_pemeriksaan,
				thonor_dokter_detail.jasamedis AS jasamedis,
				mtarif_radiologi.idtipe AS idtipe_radiologi,
				thonor_dokter_detail.nominal_potongan_rs AS nominal_potongan_rs,
				thonor_dokter_detail.nominal_pajak_dokter AS nominal_pajak_dokter,
				thonor_dokter_detail.jasamedis_netto AS jasamedis_netto,
				' . $this->queryBuilderStatusHoldAsuransi('pendapatan_radiologi') . ' AS status_hold_asuransi,
				' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_radiologi') . " AS status_hold_berkas
				FROM
				thonor_dokter_detail
				JOIN mtarif_radiologi ON mtarif_radiologi.id = thonor_dokter_detail.idtarif
				LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_asuransi ON setting_penyerahan_asuransi.idtipe = 1 AND setting_penyerahan_asuransi.idkategori = thonor_dokter_detail.idkategori
				LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_hold ON setting_penyerahan_hold.idtipe = 2 AND setting_penyerahan_hold.idkategori = thonor_dokter_detail.idkategori
				LEFT JOIN tklaim_detail ON ((thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND tklaim_detail.tipe = '1') OR (thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND tklaim_detail.tipe = '2')) AND tklaim_detail.pendaftaran_id = thonor_dokter_detail.idtransaksi
				LEFT JOIN tklaim ON tklaim.id = tklaim_detail.klaim_id
				LEFT JOIN (" . $this->queryBuilderBerkasAnalisa() . ") AS berkas_analisa ON ( ( thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND berkas_analisa.asalpendaftaran = 'rawatjalan' ) OR ( thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND berkas_analisa.asalpendaftaran = 'rawatinap' ) ) AND thonor_dokter_detail.idtransaksi = berkas_analisa.idpendaftaran AND thonor_dokter_detail.iddokter = berkas_analisa.iddokter
				WHERE
				thonor_dokter_detail.jenis_tindakan LIKE 'JASA DOKTER RADIOLOGI%' AND
				" . $whereTipeRadiologi . "
				(CASE
					WHEN '$status_periode' = 'Periode Sebelumnya' THEN
					thonor_dokter_detail.idhonor_selanjutnya = '$idhonor'
					WHEN '$status_periode' = 'Periode Saat Ini' THEN
					thonor_dokter_detail.idhonor = '$idhonor'
					AND thonor_dokter_detail.idhonor_selanjutnya IS NULL
					" . $whereStatusExpertise . "
				END)
				GROUP BY
					thonor_dokter_detail.id
				ORDER BY
					thonor_dokter_detail.tanggal_pemeriksaan,
					thonor_dokter_detail.namakelompok,
					thonor_dokter_detail.namarekanan
			) AS temp
			GROUP BY
				temp.tanggal_pemeriksaan
		) AS result
		WHERE
			IF ( (result.status_hold_asuransi + result.status_hold_berkas) <> 0, 1, 0) = '$status_hold'");

		return $query->result();
	}

	public function getPendapatanLaboratorium($data)
	{
		$idhonor = $data['idhonor'];
		$status_periode = $data['status_periode']; // Periode Saat Ini or Periode Sebelumnya
		$status_hold = $data['status_hold']; // Status Hold

		$jenis_tindakan = implode("','", [
			'JASA DOKTER LABORATORIUM',
			'JASA DOKTER LABORATORIUM (UMUM)',
			'JASA DOKTER LABORATORIUM (PATHOLOGI ANATOMI)',
			'JASA DOKTER LABORATORIUM (PMI)',
		]);

		$query = $this->db->query('SELECT
			id,
			namapasien,
			tanggal_pemeriksaan,
			reference_table,
			idtransaksi,
			iddetail,
			idtarif,
			namatarif,
			iddokter,
			namadokter,
			namakelompok,
			jasamedis,
			potongan_rs,
			pajak_dokter,
			nominal_potongan_rs,
			nominal_pajak_dokter,
			jasamedis_netto,
			tanggal_pembayaran,
			tanggal_jatuhtempo,
			status_expertise,
			status_hold_asuransi,
			status_hold_berkas
		FROM
		(
			SELECT
			thonor_dokter_detail.id,
			COALESCE(trawatinap_pendaftaran.namapasien, tpoliklinik_pendaftaran.namapasien) AS namapasien,
			thonor_dokter_detail.tanggal_pemeriksaan,
			thonor_dokter_detail.reference_table,
			thonor_dokter_detail.idtransaksi,
			thonor_dokter_detail.iddetail,
			thonor_dokter_detail.idtarif,
			thonor_dokter_detail.namatarif,
			thonor_dokter_detail.iddokter,
			thonor_dokter_detail.namadokter,
			(CASE
				WHEN thonor_dokter_detail.idkelompok = 1 THEN
					CONCAT(thonor_dokter_detail.namakelompok, " <br><b>", thonor_dokter_detail.namarekanan,"</b>")
				ELSE thonor_dokter_detail.namakelompok
			END) AS namakelompok,
			thonor_dokter_detail.jasamedis,
			thonor_dokter_detail.potongan_rs,
			thonor_dokter_detail.pajak_dokter,
			thonor_dokter_detail.nominal_potongan_rs,
			thonor_dokter_detail.nominal_pajak_dokter,
			thonor_dokter_detail.jasamedis_netto,
			thonor_dokter_detail.tanggal_pembayaran,
			thonor_dokter_detail.tanggal_jatuhtempo,
			thonor_dokter_detail.status_expertise,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_laboratorium') . ' AS status_hold_asuransi,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_laboratorium') . " AS status_hold_berkas
			FROM
			thonor_dokter_detail
			LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = thonor_dokter_detail.idtransaksi AND thonor_dokter_detail.jenis_transaksi = 'poliklinik'
			LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = thonor_dokter_detail.idtransaksi AND thonor_dokter_detail.jenis_transaksi = 'rawatinap'
			LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_asuransi ON setting_penyerahan_asuransi.idtipe = 1 AND setting_penyerahan_asuransi.idkategori = thonor_dokter_detail.idkategori
			LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_hold ON setting_penyerahan_hold.idtipe = 2 AND setting_penyerahan_hold.idkategori = thonor_dokter_detail.idkategori
			LEFT JOIN tklaim_detail ON ((thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND tklaim_detail.tipe = '1') OR (thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND tklaim_detail.tipe = '2')) AND tklaim_detail.pendaftaran_id = thonor_dokter_detail.idtransaksi
			LEFT JOIN tklaim ON tklaim.id = tklaim_detail.klaim_id
			LEFT JOIN (" . $this->queryBuilderBerkasAnalisa() . ") AS berkas_analisa ON ( ( thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND berkas_analisa.asalpendaftaran = 'rawatjalan' ) OR ( thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND berkas_analisa.asalpendaftaran = 'rawatinap' ) ) AND thonor_dokter_detail.idtransaksi = berkas_analisa.idpendaftaran AND thonor_dokter_detail.iddokter = berkas_analisa.iddokter
			WHERE
			thonor_dokter_detail.jenis_tindakan IN ('$jenis_tindakan') AND
			(CASE
				WHEN '$status_periode' = 'Periode Sebelumnya' THEN
				thonor_dokter_detail.idhonor_selanjutnya = '$idhonor'
				WHEN '$status_periode' = 'Periode Saat Ini' THEN
				thonor_dokter_detail.idhonor = '$idhonor'
				AND thonor_dokter_detail.idhonor_selanjutnya IS NULL
			END)
			GROUP BY
				thonor_dokter_detail.id
			ORDER BY
				thonor_dokter_detail.tanggal_pemeriksaan,
				thonor_dokter_detail.namakelompok,
				thonor_dokter_detail.namarekanan
		) AS result
		WHERE
			IF ( (result.status_hold_asuransi + result.status_hold_berkas) <> 0, 1, 0) = '$status_hold'");

		return $query->result();
	}

	public function getPendapatanFisioterapi($data)
	{
		$idhonor = $data['idhonor'];
		$status_periode = $data['status_periode']; // Periode Saat Ini or Periode Sebelumnya
		$status_hold = $data['status_hold']; // Status Hold

		$query = $this->db->query('SELECT
			id,
			namapasien,
			tanggal_pemeriksaan,
			reference_table,
			idtransaksi,
			iddetail,
			idtarif,
			namatarif,
			iddokter,
			namadokter,
			namakelompok,
			jasamedis,
			potongan_rs,
			pajak_dokter,
			nominal_potongan_rs,
			nominal_pajak_dokter,
			jasamedis_netto,
			tanggal_pembayaran,
			tanggal_jatuhtempo,
			status_expertise,
			status_hold_asuransi,
			status_hold_berkas
		FROM
		(
			SELECT
			thonor_dokter_detail.id,
			COALESCE(trawatinap_pendaftaran.namapasien, tpoliklinik_pendaftaran.namapasien) AS namapasien,
			thonor_dokter_detail.tanggal_pemeriksaan,
			thonor_dokter_detail.reference_table,
			thonor_dokter_detail.idtransaksi,
			thonor_dokter_detail.iddetail,
			thonor_dokter_detail.idtarif,
			thonor_dokter_detail.namatarif,
			thonor_dokter_detail.iddokter,
			thonor_dokter_detail.namadokter,
			(CASE
				WHEN thonor_dokter_detail.idkelompok = 1 THEN
					CONCAT(thonor_dokter_detail.namakelompok, " <br><b>", thonor_dokter_detail.namarekanan,"</b>")
				ELSE thonor_dokter_detail.namakelompok
			END) AS namakelompok,
			thonor_dokter_detail.jasamedis,
			thonor_dokter_detail.potongan_rs,
			thonor_dokter_detail.pajak_dokter,
			thonor_dokter_detail.nominal_potongan_rs,
			thonor_dokter_detail.nominal_pajak_dokter,
			thonor_dokter_detail.jasamedis_netto,
			thonor_dokter_detail.tanggal_pembayaran,
			thonor_dokter_detail.tanggal_jatuhtempo,
			thonor_dokter_detail.status_expertise,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_fisioterapi') . ' AS status_hold_asuransi,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_fisioterapi') . " AS status_hold_berkas
			FROM
			thonor_dokter_detail
			LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = thonor_dokter_detail.idtransaksi AND thonor_dokter_detail.jenis_transaksi = 'poliklinik'
			LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = thonor_dokter_detail.idtransaksi AND thonor_dokter_detail.jenis_transaksi = 'rawatinap'
			LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_asuransi ON setting_penyerahan_asuransi.idtipe = 1 AND setting_penyerahan_asuransi.idkategori = thonor_dokter_detail.idkategori
			LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_hold ON setting_penyerahan_hold.idtipe = 2 AND setting_penyerahan_hold.idkategori = thonor_dokter_detail.idkategori
			LEFT JOIN tklaim_detail ON ((thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND tklaim_detail.tipe = '1') OR (thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND tklaim_detail.tipe = '2')) AND tklaim_detail.pendaftaran_id = thonor_dokter_detail.idtransaksi
			LEFT JOIN tklaim ON tklaim.id = tklaim_detail.klaim_id
			LEFT JOIN (" . $this->queryBuilderBerkasAnalisa() . ") AS berkas_analisa ON ( ( thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND berkas_analisa.asalpendaftaran = 'rawatjalan' ) OR ( thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND berkas_analisa.asalpendaftaran = 'rawatinap' ) ) AND thonor_dokter_detail.idtransaksi = berkas_analisa.idpendaftaran AND thonor_dokter_detail.iddokter = berkas_analisa.iddokter
			WHERE
			thonor_dokter_detail.jenis_tindakan = 'JASA DOKTER FISIOTERAPI' AND
			(CASE
				WHEN '$status_periode' = 'Periode Sebelumnya' THEN
				thonor_dokter_detail.idhonor_selanjutnya = '$idhonor'
				WHEN '$status_periode' = 'Periode Saat Ini' THEN
				thonor_dokter_detail.idhonor = '$idhonor'
				AND thonor_dokter_detail.idhonor_selanjutnya IS NULL
			END)
			GROUP BY
				thonor_dokter_detail.id
			ORDER BY
				thonor_dokter_detail.tanggal_pemeriksaan,
				thonor_dokter_detail.namakelompok,
				thonor_dokter_detail.namarekanan
		) AS result
		WHERE
			IF ( (result.status_hold_asuransi + result.status_hold_berkas) <> 0, 1, 0) = '$status_hold'");

		return $query->result();
	}

	public function getPendapatanAnesthesi($data)
	{
		$idhonor = $data['idhonor'];
		$status_periode = $data['status_periode']; // Periode Saat Ini or Periode Sebelumnya
		$jenis_pasien = $data['jenis_pasien']; // Pasien RS or Pasien Pribadi
		$status_hold = $data['status_hold']; // Status Hold

		$query = $this->db->query('SELECT
			id,
			asalpasien,
			namapoliklinik,
			namapasien,
			tanggal_pemeriksaan,
			reference_table,
			idtransaksi,
			iddetail,
			idtarif,
			namatarif,
			iddokter,
			namadokter,
			namakelompok,
			jasamedis,
			potongan_rs,
			pajak_dokter,
			nominal_potongan_rs,
			nominal_pajak_dokter,
			jasamedis_netto,
			tanggal_pembayaran,
			tanggal_jatuhtempo,
			status_expertise,
			jenisoperasi,
			namaoperasi,
			status_hold_asuransi,
			status_hold_berkas
		FROM
		(
			SELECT
			thonor_dokter_detail.id,
			thonor_dokter_detail.jenis_pasien AS asalpasien,
			mpoliklinik.nama AS namapoliklinik,
			trawatinap_pendaftaran.namapasien,
			thonor_dokter_detail.tanggal_pemeriksaan,
			thonor_dokter_detail.reference_table,
			thonor_dokter_detail.idtransaksi,
			thonor_dokter_detail.iddetail,
			thonor_dokter_detail.idtarif,
			thonor_dokter_detail.namatarif,
			thonor_dokter_detail.iddokter,
			thonor_dokter_detail.namadokter,
			(CASE
				WHEN thonor_dokter_detail.idkelompok = 1 THEN
					CONCAT(thonor_dokter_detail.namakelompok, " <br><b>", thonor_dokter_detail.namarekanan,"</b>")
				ELSE thonor_dokter_detail.namakelompok
			END) AS namakelompok,
			thonor_dokter_detail.jasamedis,
			thonor_dokter_detail.potongan_rs,
			thonor_dokter_detail.pajak_dokter,
			thonor_dokter_detail.nominal_potongan_rs,
			thonor_dokter_detail.nominal_pajak_dokter,
			thonor_dokter_detail.jasamedis_netto,
			thonor_dokter_detail.tanggal_pembayaran,
			thonor_dokter_detail.tanggal_jatuhtempo,
			thonor_dokter_detail.status_expertise,
			mjenis_operasi.nama AS jenisoperasi,
			tkamaroperasi_pendaftaran.operasi AS namaoperasi,
			' . $this->queryBuilderStatusHoldAsuransi('pendapatan_anesthesi') . ' AS status_hold_asuransi,
			' . $this->queryBuilderStatusHoldBerkasAnalisa('pendapatan_anesthesi') . " AS status_hold_berkas
			FROM
			thonor_dokter_detail
			JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = thonor_dokter_detail.idtransaksi
			JOIN tkamaroperasi_jasada ON tkamaroperasi_jasada.id = thonor_dokter_detail.iddetail
			JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_jasada.idpendaftaranoperasi
			JOIN mjenis_operasi ON mjenis_operasi.id = tkamaroperasi_pendaftaran.jenis_operasi_id
			LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_asuransi ON setting_penyerahan_asuransi.idtipe = 1 AND setting_penyerahan_asuransi.idkategori = thonor_dokter_detail.idkategori
			LEFT JOIN msetting_penyerahan_honor_dokter AS setting_penyerahan_hold ON setting_penyerahan_hold.idtipe = 2 AND setting_penyerahan_hold.idkategori = thonor_dokter_detail.idkategori
			LEFT JOIN tklaim_detail ON ((thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND tklaim_detail.tipe = '1') OR (thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND tklaim_detail.tipe = '2')) AND tklaim_detail.pendaftaran_id = thonor_dokter_detail.idtransaksi
			LEFT JOIN tklaim ON tklaim.id = tklaim_detail.klaim_id
			LEFT JOIN (" . $this->queryBuilderBerkasAnalisa() . ") AS berkas_analisa ON ( ( thonor_dokter_detail.jenis_transaksi = 'poliklinik' AND berkas_analisa.asalpendaftaran = 'rawatjalan' ) OR ( thonor_dokter_detail.jenis_transaksi = 'rawatinap' AND berkas_analisa.asalpendaftaran = 'rawatinap' ) ) AND thonor_dokter_detail.idtransaksi = berkas_analisa.idpendaftaran AND thonor_dokter_detail.iddokter = berkas_analisa.iddokter
			LEFT JOIN mpoliklinik ON mpoliklinik.id = trawatinap_pendaftaran.idpoliklinik
			WHERE
			thonor_dokter_detail.jenis_tindakan = 'JASA DOKTER ANESTHESI' AND
			(CASE
				WHEN '$status_periode' = 'Periode Sebelumnya' THEN
				thonor_dokter_detail.idhonor_selanjutnya = '$idhonor'
				WHEN '$status_periode' = 'Periode Saat Ini' THEN
				thonor_dokter_detail.idhonor = '$idhonor'
				AND thonor_dokter_detail.idhonor_selanjutnya IS NULL
				AND thonor_dokter_detail.jenis_pasien = '$jenis_pasien'
			END)
			GROUP BY
			thonor_dokter_detail.id
		) AS result
		WHERE
			IF ( (result.status_hold_asuransi + result.status_hold_berkas) <> 0, 1, 0) = '$status_hold'");

		return $query->result();
	}

	public function getPendapatanLainLain($data)
	{
		$idhonor = $data['idhonor'];

		$this->db->select('thonor_dokter_detail.id,
			thonor_dokter_detail.tanggal_pemeriksaan,
			thonor_dokter_detail.namatarif,
			thonor_dokter_detail.jasamedis,
			thonor_dokter_detail.nominal_potongan_rs,
			thonor_dokter_detail.nominal_pajak_dokter,
			thonor_dokter_detail.jasamedis_netto');

		$this->db->where('jenis_transaksi', 'pendapatan');
		$this->db->where('jenis_tindakan', 'PENDAPATAN LAIN LAIN');

		$this->db->where('thonor_dokter_detail.idhonor', $idhonor);

		$query = $this->db->get('thonor_dokter_detail');
		return $query->result();
	}

	public function getPendapatanNonPelayanan($data)
	{
		$idhonor = $data['idhonor'];

		$this->db->select('thonor_dokter_detail.id,
			thonor_dokter_detail.idtransaksi,
			thonor_dokter_detail.tanggal_pemeriksaan,
			thonor_dokter_detail.namatarif,
			thonor_dokter_detail.jasamedis,
			thonor_dokter_detail.nominal_potongan_rs,
			thonor_dokter_detail.nominal_pajak_dokter,
			thonor_dokter_detail.jasamedis_netto,
			tpendapatan_nonpelayanan.tanggal_pembayaran,
			tpendapatan_nonpelayanan.status_pendapatan,
			tpendapatan_nonpelayanan.status_periode');

		$this->db->join('tpendapatan_nonpelayanan', 'tpendapatan_nonpelayanan.id = thonor_dokter_detail.idtransaksi');

		$this->db->where('jenis_transaksi', 'pendapatan');
		$this->db->where('jenis_tindakan', 'PENDAPATAN NON PELAYANAN');

		$this->db->where('thonor_dokter_detail.idhonor', $idhonor);

		$query = $this->db->get('thonor_dokter_detail');
		return $query->result();
	}

	public function getPendapatanTransport($data)
	{
		$idhonor = $data['idhonor'];

		$this->db->select("thonor_dokter_detail.id,
			thonor_dokter_detail.idtransaksi,
			COUNT(DISTINCT thonor_dokter_detail.idtransaksi) AS hari_kerja,
			DAYOFMONTH(LAST_DAY(CONCAT(mdokter_jadwal.periode, '-01'))) - COUNT(mdokter_jadwal.id) AS hari_libur,
			COUNT(thonor_dokter_detail.id) AS total_jaga,
			SUM(thonor_dokter_detail.jasamedis) AS total_transport,
			SUM(thonor_dokter_detail.nominal_potongan_rs) AS total_potongan_rs,
			SUM(thonor_dokter_detail.nominal_pajak_dokter) AS total_pajak_dokter,
			SUM(thonor_dokter_detail.jasamedis_netto) AS total_netto");

		$this->db->join('mdokter_jadwal', 'mdokter_jadwal.id = thonor_dokter_detail.idtransaksi');

		$this->db->where('jenis_transaksi', 'pendapatan');
		$this->db->where('jenis_tindakan', 'PENDAPATAN TRANSPORT');

		$this->db->where('thonor_dokter_detail.idhonor', $idhonor);

		$this->db->group_by('thonor_dokter_detail.idhonor');
		$this->db->group_by('thonor_dokter_detail.iddokter');

		$query = $this->db->get('thonor_dokter_detail');
		return $query->result();
	}

	public function getPendapatanInformasiMedis($data)
	{
		$idhonor = $data['idhonor'];

		$this->db->select('thonor_dokter_detail.id,
			thonor_dokter_detail.idtransaksi,
			thonor_dokter_detail.tanggal_pemeriksaan,
			thonor_dokter_detail.namatarif,
			thonor_dokter_detail.jasamedis,
			thonor_dokter_detail.nominal_potongan_rs,
			thonor_dokter_detail.nominal_pajak_dokter,
			thonor_dokter_detail.jasamedis_netto');

		$this->db->where('jenis_transaksi', 'pendapatan');
		$this->db->where('jenis_tindakan', 'PENDAPATAN INFORMASI MEDIS');

		$this->db->where('thonor_dokter_detail.idhonor', $idhonor);

		$query = $this->db->get('thonor_dokter_detail');
		return $query->result();
	}

	public function getPotonganKasbon($data)
	{
		$idhonor = $data['idhonor'];

		$this->db->select('thonor_dokter_detail.id,
			thonor_dokter_detail.idtransaksi,
			thonor_dokter_detail.tanggal_pemeriksaan,
			thonor_dokter_detail.namatarif,
			thonor_dokter_detail.jasamedis,');

		$this->db->where('jenis_transaksi', 'pengeluaran');
		$this->db->where('jenis_tindakan', 'POTONGAN KASBON');
		$this->db->where('thonor_dokter_detail.idhonor', $idhonor);

		$query = $this->db->get('thonor_dokter_detail');
		return $query->result();
	}

	public function getPotonganBerobat($data)
	{
		$idhonor = $data['idhonor'];

		$this->db->select('thonor_dokter_detail.id,
			thonor_dokter_detail.idtransaksi,
			thonor_dokter_detail.tanggal_pemeriksaan,
			thonor_dokter_detail.namatarif,
			thonor_dokter_detail.jasamedis,');

		$this->db->where('jenis_transaksi', 'pengeluaran');
		$this->db->where('jenis_tindakan', 'POTONGAN BEROBAT');

		$this->db->where('thonor_dokter_detail.idhonor', $idhonor);

		$query = $this->db->get('thonor_dokter_detail');
		return $query->result();
	}

	public function getPotonganLainLain($data)
	{
		$idhonor = $data['idhonor'];

		$this->db->select('thonor_dokter_detail.id,
			thonor_dokter_detail.idtransaksi,
			thonor_dokter_detail.tanggal_pemeriksaan,
			thonor_dokter_detail.namatarif,
			thonor_dokter_detail.jasamedis,
			mpotongan_dokter.nama AS jenis_potongan,
			tpotongan_dokter.tanggal_pembayaran,
			tpotongan_dokter.status_potongan,
			tpotongan_dokter.status_periode');

		$this->db->join('tpotongan_dokter', 'tpotongan_dokter.id = thonor_dokter_detail.idtransaksi');
		$this->db->join('mpotongan_dokter', 'mpotongan_dokter.id = tpotongan_dokter.idpotongan');

		$this->db->where('jenis_transaksi', 'pengeluaran');
		$this->db->where('jenis_tindakan', 'POTONGAN LAIN LAIN');

		$this->db->where('thonor_dokter_detail.idhonor', $idhonor);

		$query = $this->db->get('thonor_dokter_detail');
		return $query->result();
	}

	public function getNominalPotonganKasbon($idhonor)
	{
		$this->db->select('SUM(thonor_dokter_detail.jasamedis) AS total');

		$this->db->where('jenis_transaksi', 'pengeluaran');
		$this->db->where('jenis_tindakan', 'POTONGAN KASBON');

		$this->db->where('thonor_dokter_detail.idhonor', $idhonor);

		$query = $this->db->get('thonor_dokter_detail');
		return $query->row()->total;
	}

	public function getNominalPiutangPembelianObat($idhonor)
	{
		$this->db->select('SUM(thonor_dokter_detail.jasamedis) AS total');

		$this->db->join('tpiutang_detail', 'tpiutang_detail.id = thonor_dokter_detail.iddetail');

		$this->db->where('jenis_transaksi', 'pengeluaran');
		$this->db->where('jenis_tindakan', 'POTONGAN BEROBAT');

		$this->db->where('thonor_dokter_detail.idhonor', $idhonor);
		$this->db->where_in('tpiutang_detail.tipe', [3, 4]);

		$query = $this->db->get('thonor_dokter_detail');
		return $query->row()->total;
	}

	public function getNominalPotonganBerobat($idhonor)
	{
		$this->db->select('SUM(thonor_dokter_detail.jasamedis) AS total');

		$this->db->join('tpiutang_detail', 'tpiutang_detail.id = thonor_dokter_detail.iddetail');

		$this->db->where('jenis_transaksi', 'pengeluaran');
		$this->db->where('jenis_tindakan', 'POTONGAN BEROBAT');

		$this->db->where('thonor_dokter_detail.idhonor', $idhonor);
		$this->db->where_in('tpiutang_detail.tipe', [1, 2]);

		$query = $this->db->get('thonor_dokter_detail');
		return $query->row()->total;
	}

	public function getNominalPotonganLainLain($idhonor)
	{
		$this->db->select('SUM(thonor_dokter_detail.jasamedis) AS total');

		$this->db->join('tpotongan_dokter', 'tpotongan_dokter.id = thonor_dokter_detail.idtransaksi');
		$this->db->join('mpotongan_dokter', 'mpotongan_dokter.id = tpotongan_dokter.idpotongan');

		$this->db->where('jenis_transaksi', 'pengeluaran');
		$this->db->where('jenis_tindakan', 'POTONGAN LAIN LAIN');

		$this->db->where('thonor_dokter_detail.idhonor', $idhonor);

		$query = $this->db->get('thonor_dokter_detail');
		return $query->row()->total;
	}

	public function checkStatusHonorDokter($iddokter, $tanggal_pembayaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$this->db->where('iddokter', $iddokter);
		$this->db->where('tanggal_pembayaran', $tanggal_pembayaran);
		$this->db->where('status_stop', '0');
		$this->db->where('status', '1');
		$query = $this->db->get('thonor_dokter');
		$row = $query->row();

		if ($query->num_rows() > 0) {
			return $row->id;
		} else {
			return null;
		}
	}

	public function checkStatusHonorDokterStopped($iddokter, $tanggal_pembayaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$this->db->where('iddokter', $iddokter);
		$this->db->where('tanggal_pembayaran', $tanggal_pembayaran);
		$this->db->where('status_stop', '1');
		$this->db->where('status', '1');
		$query = $this->db->get('thonor_dokter');
		$row = $query->row();

		if ($query->num_rows() > 0) {
			return $row->id;
		} else {
			return null;
		}
	}

	public function checkHonorDokterByDetail($data)
	{
		$this->db->join('thonor_dokter', 'thonor_dokter.id = thonor_dokter_detail.idhonor');
		$this->db->where('thonor_dokter_detail.idtransaksi', $data['idtransaksi']);
		$this->db->where('thonor_dokter_detail.reference_table', $data['reference_table']);
		$this->db->where('thonor_dokter_detail.iddetail', $data['iddetail']);
		$this->db->where('thonor_dokter.status_stop', '1');
		$query = $this->db->get('thonor_dokter_detail');

		if ($query->num_rows() > 0) {
			return 'STATUS_STOP';
		} else {
			return 'STATUS_AVAILABLE';
		}
	}

	public function generateTransaksiHonorDokterRawatJalan($dataPendaftaran)
	{
		$idpendaftaran = $dataPendaftaran['idpendaftaran'];
		$jenis_pasien = $dataPendaftaran['jenis_pasien'];
		$idkelompok = $dataPendaftaran['idkelompok'];
		$namakelompok = $dataPendaftaran['namakelompok'];
		$idrekanan = $dataPendaftaran['idrekanan'];
		$namarekanan = $dataPendaftaran['namarekanan'];
		$tanggal_pemeriksaan = $dataPendaftaran['tanggal_pemeriksaan'];
		$status_asuransi = $dataPendaftaran['status_asuransi'];
		$kasir_id = $dataPendaftaran['kasir_id'];
		$idpoliklinik = $dataPendaftaran['idpoliklinik'];
		$tanggal_kasir = $dataPendaftaran['tanggal_kasir'];
		$idtipe = $dataPendaftaran['idtipe'];

		// ** PROSES NON JASA DOKTER
		$idvalidasi = '';
		$idheader = '';
		$st_insert_detail = '0';
		$jenis_transaksi_id = '1';

		$q = "SELECT H.id as kasir_id,CONCAT(MAX(CASE WHEN D.idmetode IN (1,2,3,4) THEN '1' ELSE 0 END) 
			,MAX(CASE WHEN D.idmetode =7 THEN '1' ELSE 0 END)   
			,MAX(CASE WHEN D.idmetode =5 THEN '1' ELSE 0 END)   
			,MAX(CASE WHEN D.idmetode =6 THEN '1' ELSE 0 END) ) as cara_bayar
			,SUM(CASE WHEN D.idmetode IN (1,2,3,4)THEN D.nominal ELSE 0 END) as total_tunai
			,SUM(CASE WHEN D.idmetode NOT IN (1,2,3,4)THEN D.nominal ELSE 0 END) as total_non_tunai
			FROM tkasir H
			LEFT JOIN tkasir_pembayaran D ON D.idkasir=H.id AND D.`status`='1'
			WHERE H.id='$kasir_id'
			GROUP BY H.id";

		$row_bayar = $this->db->query($q)->row();
		$cara_bayar = $row_bayar->cara_bayar;
		$total_tunai = $row_bayar->total_tunai;
		$total_non_tunai = $row_bayar->total_non_tunai;

		if ($total_tunai > 0) {
			$q = "SELECT H.id as idvalidasi FROM tvalidasi_pendapatan_rajal H WHERE H.tipe_trx='1' AND H.tanggal_transaksi='$tanggal_kasir' AND H.st_posting='0'";
			$idvalidasi = $this->db->query($q)->row('idvalidasi');
			if ($idvalidasi == '') {
				$data_validasi = [
					'tanggal_transaksi' => $tanggal_kasir,
					'tipe_trx' => 1,
					'status' => 1,
				];
				$this->db->insert('tvalidasi_pendapatan_rajal', $data_validasi);
				$idvalidasi = $this->db->insert_id();
			}
		}

		if ($idvalidasi != '') {
			// print_r($dataPendaftaran);exit;
			$idheader = $this->Tvalidasi_model->InsertJurnalPendapatanRajalHead($dataPendaftaran, $idvalidasi, $total_tunai, $total_non_tunai, '1');
		}

		if ($total_tunai > 0 && $total_non_tunai == '0') {
			$st_insert_detail = '1';
		}
		// print_r($st_insert_detail);exit;
		// OBAT FARMASI RACIKAN
		foreach ($this->Tpoliklinik_verifikasi_model->viewRincianFarmasiObatRacikan($idpendaftaran) as $row) {
			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_farmasi_racikan($idheader, $row, $idtipe, $idpoliklinik, 3, $idvalidasi);
			}
		}

		// OBAT FARMASI
		foreach ($this->Tpoliklinik_verifikasi_model->viewRincianFarmasi($idpendaftaran, 3) as $row) {
			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_farmasi($idheader, $row, $idtipe, $idpoliklinik, 3, $idvalidasi);
			}
		}

		// ALKES FARMASI
		foreach ($this->Tpoliklinik_verifikasi_model->viewRincianFarmasi($idpendaftaran, 1) as $row) {
			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_farmasi($idheader, $row, $idtipe, $idpoliklinik, 1, $idvalidasi);
			}
		}
		// IMPLAN FARMASI
		foreach ($this->Tpoliklinik_verifikasi_model->viewRincianFarmasi($idpendaftaran, 2) as $row) {
			// print_r($row);exit;
			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_farmasi($idheader, $row, $idtipe, $idpoliklinik, 2, $idvalidasi);
			}
		}

		// OBAT IGD
		foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRajalObat($idpendaftaran) as $row) {
			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_obat_alkes($idheader, $row, $idtipe, $idpoliklinik, 3, $idvalidasi);
			}
		}

		// ALKES IGD
		foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRajalAlkes($idpendaftaran) as $row) {
			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_obat_alkes($idheader, $row, $idtipe, $idpoliklinik, 1, $idvalidasi);
			}
		}

		// ADMINISTRASI RAWAT JALAN
		foreach ($this->Tpoliklinik_verifikasi_model->viewRincianAdministrasi($idpendaftaran) as $row) {
			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'ADMINISTRASI RAWAT JALAN', 'tpoliklinik_administrasi', 1);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_rajal($idheader, $row, 'mtarif_administrasi', 1, $idvalidasi);
			}
		}

		// ** PROSES JASA DOKTER
		// JASA DOKTER RAWAT JALAN
		foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRajalTindakan($idpendaftaran) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasapelayanan > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('tpoliklinik_pelayanan');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'poliklinik',
					'jenis_tindakan' => 'JASA DOKTER RAWAT JALAN',
					'reference_table' => 'tpoliklinik_pelayanan',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idtarif,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_asuransi' => $status_asuransi
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF RAWAT JALAN', 'tpoliklinik_pelayanan', 2);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_rajal($idheader, $row, 'mtarif_rawatjalan', 2, $idvalidasi);
			}
		}

		// JASA DOKTER LABORATORIUM
		foreach ($this->Tpoliklinik_verifikasi_model->viewRincianLaboratorium($idpendaftaran) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasapelayanan > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trujukan_laboratorium_detail');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'poliklinik',
					'jenis_tindakan' => 'JASA DOKTER LABORATORIUM',
					'reference_table' => 'trujukan_laboratorium_detail',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idtarif,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_asuransi' => $status_asuransi
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF LABORATORIUM RAWAT JALAN', 'trujukan_laboratorium_detail', 6);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_rajal($idheader, $row, 'mtarif_laboratorium_detail', 3, $idvalidasi);
			}
		}

		// JASA DOKTER RADIOLOGI
		foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRadiologi($idpendaftaran) as $index => $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasamedis > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trujukan_radiologi_detail');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'poliklinik',
					'jenis_tindakan' => 'JASA DOKTER RADIOLOGI',
					'reference_table' => 'trujukan_radiologi_detail',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idtarif,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_expertise' => $row->status_expertise,
					'status_asuransi' => $status_asuransi
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF RADIOLOGI RAWAT JALAN', 'trujukan_radiologi_detail', 5);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_rajal($idheader, $row, 'mtarif_radiologi_detail', 4, $idvalidasi);
			}
		}

		// JASA DOKTER FISIOTERAPI
		foreach ($this->Tpoliklinik_verifikasi_model->viewRincianFisioterapi($idpendaftaran) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasapelayanan > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trujukan_fisioterapi_detail');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'poliklinik',
					'jenis_tindakan' => 'JASA DOKTER FISIOTERAPI',
					'reference_table' => 'trujukan_fisioterapi_detail',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idtarif,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_asuransi' => $status_asuransi
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF FISIOTERAPI RAWAT JALAN', 'trujukan_fisioterapi_detail', 7);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_rajal($idheader, $row, 'mtarif_fisioterapi_detail', 5, $idvalidasi);
			}
		}
		return true;
	}

	public function generateTransaksiHonorDokterRawatInap($dataPendaftaran)
	{
		// print_r($dataPendaftaran);exit;
		$tanggal_pemeriksaan = $dataPendaftaran['tanggal_pemeriksaan'];
		$idpendaftaran = $dataPendaftaran['idpendaftaran'];
		$idtipepasien = $dataPendaftaran['idtipepasien'];
		$idasalpasien = $dataPendaftaran['idasalpasien'];
		$idpoliklinik = $dataPendaftaran['idpoliklinik'];
		$jenis_pasien = $dataPendaftaran['jenis_pasien'];
		$idkelompok = $dataPendaftaran['idkelompok'];
		$namakelompok = $dataPendaftaran['namakelompok'];
		$idrekanan = $dataPendaftaran['idrekanan'];
		$namarekanan = $dataPendaftaran['namarekanan'];
		$idkategori_perujuk = $dataPendaftaran['idkategori_perujuk'];
		$namakategori_perujuk = $dataPendaftaran['namakategori_perujuk'];
		$iddokter_perujuk = $dataPendaftaran['iddokter_perujuk'];
		$namadokter_perujuk = $dataPendaftaran['namadokter_perujuk'];
		$potonganrs_perujuk = $dataPendaftaran['potonganrs_perujuk'];
		$pajak_perujuk = $dataPendaftaran['pajak_perujuk'];
		$status_asuransi = $dataPendaftaran['status_asuransi'];
		$status_kasir_rajal = $dataPendaftaran['status_kasir_rajal'];
		$idDokterOperator = $this->getDokterOperator($idpendaftaran);

		// ** PROSES NON JASA DOKTER
		$jenis_transaksi_id = '2';
		$kasir_id = $dataPendaftaran['kasir_id'];
		$st_insert_detail = '0';
		$kelas = $dataPendaftaran['idkelas'];
		$idruangan = $dataPendaftaran['idruangan'];

		$q = "SELECT H.tanggal,H.totalharusdibayar,H.total,H.diskonrp,H.diskonpersen,H.nominal_round,H.deposit
			,SUM(CASE WHEN D.idmetode IN (1,2,3,4) THEN D.nominal ELSE 0 END) as tunai
			,SUM(CASE WHEN D.idmetode NOT IN (1,2,3,4) THEN D.nominal ELSE 0 END) as non_tunai
			FROM trawatinap_tindakan_pembayaran H
			LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON D.idtindakan=H.id
			WHERE H.id='$kasir_id' 
			GROUP BY H.id";
		$row_kasir = $this->db->query($q)->row();
		$total_deposit = $row_kasir->deposit;
		$total_tunai = $row_kasir->tunai + $total_deposit;
		$total_non_tunai = $row_kasir->non_tunai;
		$tanggal_kasir = $row_kasir->tanggal;

		if ($total_tunai > 0 && $total_non_tunai == '0') {
			$st_insert_detail = '1';
		}

		if ($total_tunai > 0) {
			$idvalidasi = $this->Tvalidasi_model->InsertJurnalPendapatanRanaplHead($dataPendaftaran, $tanggal_kasir, $total_tunai, $total_non_tunai, $row_kasir);
		}

		// DEPOSIT RAWAT INAP
		if ($row_kasir->deposit > 0) {
			// [VALIDASI]
			$this->Tvalidasi_model->insert_deposit($idpendaftaran, $idvalidasi);
		}

		// ADMINISTRASI RAWAT INAP
		foreach ($this->Trawatinap_verifikasi_model->viewRincianAdministrasiRawatInap($idpendaftaran) as $row) {
			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'ADMINISTRASI RAWAT INAP', 'trawatinap_administrasi', 1);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap_adm($idvalidasi, $row, $dataPendaftaran);
			}
		}

		// OBAT-ALKES
		if ($st_insert_detail == '1') {
			// [VALIDASI]
			$this->Tvalidasi_model->insert_obat_alkes_ranap($idpendaftaran, $idvalidasi, $idruangan, $kelas);
		}

		// RUANG KAMAR OPERASI
		foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiKamar($idpendaftaran) as $row) {
			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'RUANG KAMAR OPERASI', 'tkamaroperasi_ruangan', 9);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_operasi_detail', 2, $kelas);
			}
		}

		// SEWA ALAT KAMAR OPERASI
		foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiSewaAlat($idpendaftaran) as $row) {
			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_operasi_sewaalat_detail', 3, $kelas);
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'SEWA ALAT KAMAR OPERASI', 'tkamaroperasi_sewaalat', 8);
		}

		// DOKTER ASISTEN
		foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaAsisten($idpendaftaran) as $row) {
			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_dokter_ko_asisten($idvalidasi, $row, 'tkamaroperasi_jasada', 1, $kelas);
			}
		}

		// ADMINISTRASI RAWAT JALAN
		foreach ($this->Trawatinap_verifikasi_model->viewRincianAdministrasiRawatJalan($idpendaftaran) as $row) {
			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_administrasi', 21, $kelas);
			}
		}

		// RUANGAN RANAP
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapRuangan($idpendaftaran) as $row) {
			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_ruangperawatan_detail', 22, $kelas);
			}
		}

		// ** PROSES JASA DOKTER
		// JASA DOKTER RAWAT INAP - FULL CARE
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 1) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasapelayanan > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trawatinap_tindakan');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER RAWAT INAP (FULL CARE)',
					'reference_table' => 'trawatinap_tindakan',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idtarif,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_asuransi' => $status_asuransi,
					'section_operasi' => (in_array($row->iddokter, $idDokterOperator) ? 1 : 0)
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF TINDAKAN RAWAT INAP', 'trawatinap_tindakan', 3);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_rawatinap_detail', 4, $kelas);
			}
		}

		// JASA DOKTER RAWAT INAP - ECG
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 2) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasapelayanan > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trawatinap_tindakan');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER RAWAT INAP (ECG)',
					'reference_table' => 'trawatinap_tindakan',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idtarif,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_asuransi' => $status_asuransi,
					'section_operasi' => (in_array($row->iddokter, $idDokterOperator) ? 1 : 0)
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF TINDAKAN RAWAT INAP', 'trawatinap_tindakan', 3);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_rawatinap_detail', 5, $kelas);
			}
		}

		// JASA DOKTER RAWAT INAP - AMBULANCE
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 5) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasapelayanan > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trawatinap_tindakan');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER RAWAT INAP (AMBULANCE)',
					'reference_table' => 'trawatinap_tindakan',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idtarif,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_asuransi' => $status_asuransi,
					'section_operasi' => (in_array($row->iddokter, $idDokterOperator) ? 1 : 0)
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF TINDAKAN RAWAT INAP', 'trawatinap_tindakan', 3);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_rawatinap_detail', 6, $kelas);
			}
		}

		// JASA DOKTER RAWAT INAP - SEWA ALAT
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 4) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasapelayanan > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trawatinap_tindakan');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER RAWAT INAP (SEWA ALAT)',
					'reference_table' => 'trawatinap_tindakan',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idtarif,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_asuransi' => $status_asuransi,
					'section_operasi' => (in_array($row->iddokter, $idDokterOperator) ? 1 : 0)
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF TINDAKAN RAWAT INAP', 'trawatinap_tindakan', 3);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_rawatinap_detail', 7, $kelas);
			}
		}

		// JASA DOKTER RAWAT INAP - LAIN LAIN
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 6) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasapelayanan > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trawatinap_tindakan');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER RAWAT INAP (LAIN LAIN)',
					'reference_table' => 'trawatinap_tindakan',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idtarif,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_asuransi' => $status_asuransi,
					'section_operasi' => (in_array($row->iddokter, $idDokterOperator) ? 1 : 0)
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF TINDAKAN RAWAT INAP', 'trawatinap_tindakan', 3);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_rawatinap_detail', 8, $kelas);
			}
		}

		// JASA DOKTER LABORATORIUM - UMUM
		foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 1) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasapelayanan > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trujukan_laboratorium_detail');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER LABORATORIUM (UMUM)',
					'reference_table' => 'trujukan_laboratorium_detail',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idlaboratorium,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_asuransi' => $status_asuransi
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF LABORATORIUM RAWAT INAP', 'trujukan_laboratorium_detail', 6);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_laboratorium_detail', 9, $kelas);
			}
		}

		// JASA DOKTER LABORATORIUM - PATHOLOGI ANATOMI
		foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 2) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasapelayanan > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trujukan_laboratorium_detail');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER LABORATORIUM (PATHOLOGI ANATOMI)',
					'reference_table' => 'trujukan_laboratorium_detail',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idlaboratorium,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_asuransi' => $status_asuransi
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF LABORATORIUM RAWAT INAP', 'trujukan_laboratorium_detail', 6);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_laboratorium_detail', 10, $kelas);
			}
		}

		// JASA DOKTER LABORATORIUM - PMI
		foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 3) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasapelayanan > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trujukan_laboratorium_detail');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER LABORATORIUM (PMI)',
					'reference_table' => 'trujukan_laboratorium_detail',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idlaboratorium,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_asuransi' => $status_asuransi
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF LABORATORIUM RAWAT INAP', 'trujukan_laboratorium_detail', 6);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_laboratorium_detail', 11, $kelas);
			}
		}

		// JASA DOKTER RADIOLOGI - X-RAY
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 1) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasamedis > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trujukan_radiologi_detail');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER RADIOLOGI (X-RAY)',
					'reference_table' => 'trujukan_radiologi_detail',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idradiologi,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_expertise' => $row->status_expertise,
					'status_asuransi' => $status_asuransi
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF RADIOLOGI RAWAT INAP', 'trujukan_radiologi_detail', 5);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_radiologi_detail', 12, $kelas);
			}
		}

		// JASA DOKTER RADIOLOGI - USG
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 2) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasamedis > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trujukan_radiologi_detail');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER RADIOLOGI (USG)',
					'reference_table' => 'trujukan_radiologi_detail',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idradiologi,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_expertise' => $row->status_expertise,
					'status_asuransi' => $status_asuransi
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF RADIOLOGI RAWAT INAP', 'trujukan_radiologi_detail', 5);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_radiologi_detail', 13, $kelas);
			}
		}

		// JASA DOKTER RADIOLOGI - CT SCAN
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 3) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasamedis > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trujukan_radiologi_detail');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER RADIOLOGI (CT-SCAN)',
					'reference_table' => 'trujukan_radiologi_detail',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idradiologi,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_expertise' => $row->status_expertise,
					'status_asuransi' => $status_asuransi
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF RADIOLOGI RAWAT INAP', 'trujukan_radiologi_detail', 5);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_radiologi_detail', 14, $kelas);
			}
		}

		// JASA DOKTER RADIOLOGI - MRI
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 4) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasamedis > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trujukan_radiologi_detail');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER RADIOLOGI (MRI)',
					'reference_table' => 'trujukan_radiologi_detail',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idradiologi,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_expertise' => $row->status_expertise,
					'status_asuransi' => $status_asuransi
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF RADIOLOGI RAWAT INAP', 'trujukan_radiologi_detail', 5);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_radiologi_detail', 15, $kelas);
			}
		}

		// JASA DOKTER RADIOLOGI - BMD
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 5) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasamedis > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trujukan_radiologi_detail');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER RADIOLOGI (BMD)',
					'reference_table' => 'trujukan_radiologi_detail',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idradiologi,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_expertise' => $row->status_expertise,
					'status_asuransi' => $status_asuransi
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF RADIOLOGI RAWAT INAP', 'trujukan_radiologi_detail', 5);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_radiologi_detail', 16, $kelas);
			}
		}

		// JASA DOKTER FISIOTERAPI
		foreach ($this->Trawatinap_verifikasi_model->viewRincianFisioterapi($idpendaftaran) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasapelayanan > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('trujukan_fisioterapi_detail');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER FISIOTERAPI',
					'reference_table' => 'trujukan_fisioterapi_detail',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idtarif,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_asuransi' => $status_asuransi
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF FISIOTERAPI RAWAT INAP', 'trujukan_fisioterapi_detail', 7);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_fisioterapi_detail', 17, $kelas);
			}
		}

		// JASA DOKTER VISITE
		$FeeJasaDokterVisite = [];
		$dataVisite = $this->Trawatinap_verifikasi_model->viewRincianRanapVisite($idpendaftaran);
		$dataLogicRange = $this->Trawatinap_verifikasi_model->viewFeeJasaDokterVisite($idpendaftaran);
		foreach ($dataVisite as $index => $row) {
			if (COUNT($dataLogicRange) > 0) {
				foreach ($dataLogicRange as $logicRange) {
					$list_dokter = explode(',', $logicRange->iddokter);
					$FeeJasaDokterVisite[$index]['iddetail'] = $row->iddetail;
					$FeeJasaDokterVisite[$index]['idtarif'] = $row->idtarif;
					$FeeJasaDokterVisite[$index]['namatarif'] = $row->namatarif;
					$FeeJasaDokterVisite[$index]['namaruangan'] = $row->namaruangan;
					$FeeJasaDokterVisite[$index]['tanggal'] = $row->tanggal;
					$FeeJasaDokterVisite[$index]['iddokter'] = $row->iddokter;
					$FeeJasaDokterVisite[$index]['idkategori'] = $row->idkategori;
					$FeeJasaDokterVisite[$index]['namakategori'] = $row->namakategori;
					$FeeJasaDokterVisite[$index]['namadokter'] = $row->namadokter;
					$FeeJasaDokterVisite[$index]['persentaseperujuk'] = $row->persentaseperujuk;
					$FeeJasaDokterVisite[$index]['jasamedis'] = $row->jasamedis;
					$FeeJasaDokterVisite[$index]['potongan_rs'] = $row->potongan_rs;
					$FeeJasaDokterVisite[$index]['potonganfeers'] = $row->potonganfeers;
					$FeeJasaDokterVisite[$index]['pajak_dokter'] = $row->pajak_dokter;
					$FeeJasaDokterVisite[$index]['periode_pembayaran'] = $row->periode_pembayaran;
					$FeeJasaDokterVisite[$index]['periode_jatuhtempo'] = $row->periode_jatuhtempo;
					$FeeJasaDokterVisite[$index]['status_jasamedis'] = $row->status_jasamedis;
					$FeeJasaDokterVisite[$index]['diskon'] = $row->diskon;

					if (in_array($row->iddokter, $list_dokter)) {
						if ($logicRange->urutan_operasi == 1) {
							if ($row->tanggal <= $logicRange->tanggal_next) {
								$FeeJasaDokterVisite[$index]['urutan_operasi'] = $logicRange->urutan_operasi;
								$FeeJasaDokterVisite[$index]['urutan_visite'] = $logicRange->urutan_operasi;
							}
						} else {
							if ($row->tanggal >= $logicRange->tanggal && $row->tanggal <= $logicRange->tanggal_next) {
								$FeeJasaDokterVisite[$index]['urutan_operasi'] = $logicRange->urutan_operasi;
								$FeeJasaDokterVisite[$index]['urutan_visite'] = $logicRange->urutan_operasi;
							}
						}
					} else {
						$FeeJasaDokterVisite[$index]['urutan_operasi'] = 0;
						$FeeJasaDokterVisite[$index]['urutan_visite'] = 0;
					}
				}
			} else {
				$FeeJasaDokterVisite[$index]['iddetail'] = $row->iddetail;
				$FeeJasaDokterVisite[$index]['idtarif'] = $row->idtarif;
				$FeeJasaDokterVisite[$index]['namatarif'] = $row->namatarif;
				$FeeJasaDokterVisite[$index]['namaruangan'] = $row->namaruangan;
				$FeeJasaDokterVisite[$index]['tanggal'] = $row->tanggal;
				$FeeJasaDokterVisite[$index]['iddokter'] = $row->iddokter;
				$FeeJasaDokterVisite[$index]['idkategori'] = $row->idkategori;
				$FeeJasaDokterVisite[$index]['namakategori'] = $row->namakategori;
				$FeeJasaDokterVisite[$index]['namadokter'] = $row->namadokter;
				$FeeJasaDokterVisite[$index]['persentaseperujuk'] = $row->persentaseperujuk;
				$FeeJasaDokterVisite[$index]['jasamedis'] = $row->jasamedis;
				$FeeJasaDokterVisite[$index]['potongan_rs'] = $row->potongan_rs;
				$FeeJasaDokterVisite[$index]['potonganfeers'] = $row->potonganfeers;
				$FeeJasaDokterVisite[$index]['pajak_dokter'] = $row->pajak_dokter;
				$FeeJasaDokterVisite[$index]['periode_pembayaran'] = $row->periode_pembayaran;
				$FeeJasaDokterVisite[$index]['periode_jatuhtempo'] = $row->periode_jatuhtempo;
				$FeeJasaDokterVisite[$index]['status_jasamedis'] = $row->status_jasamedis;
				$FeeJasaDokterVisite[$index]['diskon'] = $row->diskon;
				$FeeJasaDokterVisite[$index]['urutan_operasi'] = 0;
				$FeeJasaDokterVisite[$index]['urutan_visite'] = 0;
			}

			// [BAGI HASIL]
			$this->Mbagi_hasil_model->insert_tbagi_hasil($dataPendaftaran, $row, $jenis_transaksi_id, 'TARIF VISITE RAWAT INAP', 'trawatinap_visite', 4);

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_visitedokter_detail', 18, $kelas);
			}
		}

		$PerujukVisite = 0;
		foreach ($FeeJasaDokterVisite as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row['jasamedis'] > 0) {
				$urutan_operasi = (isset($row['urutan_operasi']) ? $row['urutan_operasi'] : '0');
				$urutan_visite = (isset($row['urutan_visite']) ? $row['urutan_visite'] : '0');

				$status_perujuk = logicFeeRujukan($idtipepasien, $idasalpasien, $idpoliklinik, $idkategori_perujuk, $row['iddokter'], $urutan_operasi, $urutan_visite, $status_kasir_rajal);
				$potongan_rs = ($status_perujuk ? $row['potonganfeers'] : $row['potongan_rs']);
				$persentase_perujuk = ($status_perujuk ? $row['persentaseperujuk'] : 0);
				$jasamedis = ($row['jasamedis'] - $row['diskon']);
				$nominal_perujuk = $jasamedis * ($persentase_perujuk / 100);
				$nominal_potongan_rs = $jasamedis * ($potongan_rs / 100);
				$nominal_pajak_dokter = ($jasamedis - $nominal_perujuk) * ($row['pajak_dokter'] / 100);
				$jasamedis_netto = $jasamedis - $nominal_perujuk - $nominal_potongan_rs - $nominal_pajak_dokter;

				$PerujukVisite = $PerujukVisite + $nominal_perujuk;

				$tanggal_pembayaran = ($row['periode_pembayaran'] != '' ? $row['periode_pembayaran'] : YMDFormat(getPembayaranHonorDokter($row['iddokter'])));
				$tanggal_jatuhtempo = ($row['periode_jatuhtempo'] != '' ? $row['periode_jatuhtempo'] : YMDFormat(getJatuhTempoHonorDokter($row['iddokter'])));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row['iddetail']);
				$this->db->update('trawatinap_visite');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row['iddokter'], $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row['iddokter'], $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row['iddokter'],
							'namadokter' => $row['namadokter'],
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER VISITE',
					'reference_table' => 'trawatinap_visite',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row['iddetail'],
					'idtarif' => $row['idtarif'],
					'namatarif' => $row['namatarif'],
					'idkategori' => $row['idkategori'],
					'namakategori' => $row['namakategori'],
					'iddokter' => $row['iddokter'],
					'namadokter' => $row['namadokter'],
					'jasamedis' => $jasamedis,
					'potongan_rs' => $potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'potongan_perujuk' => $persentase_perujuk,
					'nominal_potongan_perujuk' => $nominal_perujuk,
					'pajak_dokter' => $row['pajak_dokter'],
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $row['tanggal'],
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_perujuk' => $status_perujuk,
					'status_asuransi' => $status_asuransi,
					'section_operasi' => (in_array($row['iddokter'], $idDokterOperator) ? 1 : 0)
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}
		}

		// JASA DOKTER OPERATOR
		$PerujukOperator = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewFeeOperasiJasaDokterOpertor($idpendaftaran) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasamedis > 0) {
				$status_perujuk = logicFeeRujukan($idtipepasien, $idasalpasien, $idpoliklinik, $idkategori_perujuk, $row->iddokter, $row->urutan_operasi, 1, $status_kasir_rajal);
				$jasamedis = ($row->jasamedis - $row->diskon);
				$persentase_perujuk = ($status_perujuk ? $row->persentaseperujuk : 0);
				$potongan_rs = ($status_perujuk ? $row->potonganfeers : $row->potongan_rs);
				$nominal_perujuk = $jasamedis * ($persentase_perujuk / 100);
				$nominal_potongan_rs = $jasamedis * ($potongan_rs / 100);
				$nominal_pajak_dokter = ($jasamedis - $nominal_perujuk) * ($row->pajak_dokter / 100);
				$jasamedis_netto = $jasamedis - $nominal_perujuk - $nominal_potongan_rs - $nominal_pajak_dokter;

				$PerujukOperator = $PerujukOperator + $nominal_perujuk;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('tkamaroperasi_jasado');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER OPERATOR',
					'reference_table' => 'tkamaroperasi_jasado',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idtarif,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $jasamedis,
					'potongan_rs' => $potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'potongan_perujuk' => $persentase_perujuk,
					'nominal_potongan_perujuk' => $nominal_perujuk,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_perujuk' => $status_perujuk,
					'status_asuransi' => $status_asuransi,
					'section_operasi' => (in_array($row->iddokter, $idDokterOperator) ? 1 : 0)
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_operasi_detail', 19, $kelas);
			}
		}

		// JASA DOKTER ANESTHESI
		foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterAnesthesi($idpendaftaran) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			$jasamedis = $row->grandtotal;
			$nominal_potongan_rs = $jasamedis * ($row->potongan_rs / 100);
			$nominal_pajak_dokter = $jasamedis * ($row->pajak_dokter / 100);
			$jasamedis_netto = $jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

			$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
			$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

			$this->db->set('periode_pembayaran', $tanggal_pembayaran);
			$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
			$this->db->where('id', $row->iddetail);
			$this->db->update('tkamaroperasi_jasada');

			$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
			if ($isStopPeriode == null) {
				$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

				if ($idhonor == null) {
					$dataHonorDokter = [
						'iddokter' => $row->iddokter,
						'namadokter' => $row->namadokter,
						'tanggal_pembayaran' => $tanggal_pembayaran,
						'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
						'nominal' => 0,
						'created_at' => date('Y-m-d H:i:s'),
						'created_by' => $this->session->userdata('user_id')
					];

					if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
						$idhonor = $this->db->insert_id();
					}
				}
			}

			$dataDetailHonorDokter = [
				'idhonor' => $idhonor,
				'idtransaksi' => $idpendaftaran,
				'jenis_transaksi' => 'rawatinap',
				'jenis_tindakan' => 'JASA DOKTER ANESTHESI',
				'reference_table' => 'tkamaroperasi_jasada',
				'jenis_pasien' => $jenis_pasien,
				'idkelompok' => $idkelompok,
				'namakelompok' => $namakelompok,
				'idrekanan' => $idrekanan,
				'namarekanan' => $namarekanan,
				'iddetail' => $row->iddetail,
				'idtarif' => $row->idtarif,
				'namatarif' => $row->namatarif,
				'idkategori' => $row->idkategori,
				'namakategori' => $row->namakategori,
				'iddokter' => $row->iddokter,
				'namadokter' => $row->namadokter,
				'jasamedis' => $jasamedis,
				'potongan_rs' => $row->potongan_rs,
				'nominal_potongan_rs' => $nominal_potongan_rs,
				'pajak_dokter' => $row->pajak_dokter,
				'nominal_pajak_dokter' => $nominal_pajak_dokter,
				'jasamedis_netto' => $jasamedis_netto,
				'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
				'tanggal_pembayaran' => $tanggal_pembayaran,
				'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
				'status_asuransi' => $status_asuransi
			];

			if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
				$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
			}

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_dokter_ko($idvalidasi, $row, 'tkamaroperasi_jasada', 1, $kelas);
			}
		}

		// JASA DOKTER FEE RUJUKAN
		$idDokterFeeRujukan = '';
		$FeeRujukanDokter = $this->db->query("SELECT tfeerujukan_dokter.*,
				mdokter_kategori.id AS idkategori,
				mdokter_kategori.nama AS namakategori
			FROM
				tfeerujukan_dokter
			JOIN mdokter ON mdokter.id = tfeerujukan_dokter.iddokter
			JOIN mdokter_kategori ON mdokter_kategori.id = mdokter.idkategori
			WHERE
				idrawatinap = $idpendaftaran")->result();

		if (COUNT($FeeRujukanDokter) > 0) {
			foreach ($FeeRujukanDokter as $row) {
				// Reset ID Honor
				$idhonor = 0;

				if ($row->jasamedis > 0) {
					// $nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
					// $nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
					// $jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

					$potongan_rs = 0;
					$nominal_potongan_rs = 0;
					$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
					$jasamedis_netto = $row->jasamedis - $nominal_pajak_dokter;

					$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
					$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

					$this->db->set('periode_pembayaran', $tanggal_pembayaran);
					$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
					$this->db->where('idrawatinap', $row->idrawatinap);
					$this->db->update('tfeerujukan_dokter');

					$idDokterFeeRujukan = $row->iddokter;

					$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
					if ($isStopPeriode == null) {
						$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

						if ($idhonor == null) {
							$dataHonorDokter = [
								'iddokter' => $row->iddokter,
								'namadokter' => $row->namadokter,
								'tanggal_pembayaran' => $tanggal_pembayaran,
								'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
								'nominal' => 0,
								'created_at' => date('Y-m-d H:i:s'),
								'created_by' => $this->session->userdata('user_id')
							];

							if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
								$idhonor = $this->db->insert_id();
							}
						}
					}

					$dataDetailHonorDokter = [
						'idhonor' => $idhonor,
						'idtransaksi' => $idpendaftaran,
						'jenis_transaksi' => 'rawatinap',
						'jenis_tindakan' => 'JASA DOKTER FEE RUJUKAN',
						'reference_table' => 'tfeerujukan_dokter',
						'jenis_pasien' => $jenis_pasien,
						'idkelompok' => $idkelompok,
						'namakelompok' => $namakelompok,
						'idrekanan' => $idrekanan,
						'namarekanan' => $namarekanan,
						'namatarif' => 'Fee Rujukan Dokter',
						'idkategori' => $row->idkategori,
						'namakategori' => $row->namakategori,
						'iddokter' => $row->iddokter,
						'namadokter' => $row->namadokter,
						'jasamedis' => $row->jasamedis,
						'potongan_rs' => $potongan_rs,
						'nominal_potongan_rs' => $nominal_potongan_rs,
						'pajak_dokter' => $row->pajak_dokter,
						'nominal_pajak_dokter' => $nominal_pajak_dokter,
						'jasamedis_netto' => $jasamedis_netto,
						'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
						'tanggal_pembayaran' => $tanggal_pembayaran,
						'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
						'status_asuransi' => $status_asuransi
					];

					if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
						$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
					}
				}
			}
		} else {
			$nominal_perujuk = $PerujukVisite + $PerujukOperator;
			if ($nominal_perujuk > 0) {
				// Reset ID Honor
				$idhonor = 0;

				// $nominal_potongan_rs = $nominal_perujuk * ($potonganrs_perujuk / 100);
				// $nominal_pajak_dokter = $nominal_perujuk * ($pajak_perujuk / 100);
				// $jasamedis_netto = $nominal_perujuk - $nominal_potongan_rs - $nominal_pajak_dokter;

				$potongan_rs = 0;
				$nominal_potongan_rs = 0;
				$nominal_pajak_dokter = $nominal_perujuk * ($pajak_perujuk / 100);
				$jasamedis_netto = $nominal_perujuk - $nominal_pajak_dokter;

				$namadokter_perujuk = ($namadokter_perujuk != null ? $namadokter_perujuk : '-');
				$tanggal_pembayaran = YMDFormat(getPembayaranHonorDokter($iddokter_perujuk));
				$tanggal_jatuhtempo = YMDFormat(getJatuhTempoHonorDokter($iddokter_perujuk));

				$dataFeeRujukanDokter = [];
				$dataFeeRujukanDokter['idrawatinap'] = $idpendaftaran;
				$dataFeeRujukanDokter['iddokter'] = $iddokter_perujuk;
				$dataFeeRujukanDokter['namadokter'] = $namadokter_perujuk;
				$dataFeeRujukanDokter['jasamedis'] = $nominal_perujuk;
				$dataFeeRujukanDokter['pajak_dokter'] = $pajak_perujuk;
				$dataFeeRujukanDokter['potongan_rs'] = $potonganrs_perujuk;
				$dataFeeRujukanDokter['periode_pembayaran'] = $tanggal_pembayaran;
				$dataFeeRujukanDokter['periode_jatuhtempo'] = $tanggal_jatuhtempo;

				if ($this->db->insert('tfeerujukan_dokter', $dataFeeRujukanDokter)) {
					$idFeeRujukan = $this->db->insert_id();
					$idDokterFeeRujukan = $iddokter_perujuk;

					$isStopPeriode = $this->checkStatusHonorDokterStopped($iddokter_perujuk, $tanggal_pembayaran);
					if ($isStopPeriode == null) {
						$idhonor = $this->checkStatusHonorDokter($iddokter_perujuk, $tanggal_pembayaran);

						if ($idhonor == null) {
							$dataHonorDokter = [
								'iddokter' => $iddokter_perujuk,
								'namadokter' => $namadokter_perujuk,
								'tanggal_pembayaran' => $tanggal_pembayaran,
								'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
								'nominal' => 0,
								'created_at' => date('Y-m-d H:i:s'),
								'created_by' => $this->session->userdata('user_id')
							];

							if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
								$idhonor = $this->db->insert_id();
							}
						}
					}

					$dataDetailHonorDokter = [
						'idhonor' => $idhonor,
						'idtransaksi' => $idpendaftaran,
						'jenis_transaksi' => 'rawatinap',
						'jenis_tindakan' => 'JASA DOKTER FEE RUJUKAN',
						'reference_table' => 'tfeerujukan_dokter',
						'jenis_pasien' => $jenis_pasien,
						'idkelompok' => $idkelompok,
						'namakelompok' => $namakelompok,
						'idrekanan' => $idrekanan,
						'namarekanan' => $namarekanan,
						'namatarif' => 'Fee Rujukan Dokter',
						'idkategori' => $idkategori_perujuk,
						'namakategori' => $namakategori_perujuk,
						'iddokter' => $iddokter_perujuk,
						'namadokter' => $namadokter_perujuk,
						'jasamedis' => $nominal_perujuk,
						'potongan_rs' => $potongan_rs,
						'nominal_potongan_rs' => $nominal_potongan_rs,
						'pajak_dokter' => $pajak_perujuk,
						'nominal_pajak_dokter' => $nominal_pajak_dokter,
						'jasamedis_netto' => $jasamedis_netto,
						'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
						'tanggal_pembayaran' => $tanggal_pembayaran,
						'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
						'status_asuransi' => $status_asuransi
					];

					if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
						$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
					}
				}
			}
		}

		// JASA DOKTER RAWAT JALAN
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRajalTindakanFilteredIGD($idpendaftaran) as $row) {
			// Reset ID Honor
			$idhonor = 0;

			if ($row->jasamedis > 0) {
				$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
				$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
				$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

				$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
				$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

				$this->db->set('periode_pembayaran', $tanggal_pembayaran);
				$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
				$this->db->where('id', $row->iddetail);
				$this->db->update('tpoliklinik_pelayanan');

				$isStopPeriode = $this->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
				if ($isStopPeriode == null) {
					$idhonor = $this->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

					if ($idhonor == null) {
						$dataHonorDokter = [
							'iddokter' => $row->iddokter,
							'namadokter' => $row->namadokter,
							'tanggal_pembayaran' => $tanggal_pembayaran,
							'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
							'nominal' => 0,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $this->session->userdata('user_id')
						];

						if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
							$idhonor = $this->db->insert_id();
						}
					}
				}

				$dataDetailHonorDokter = [
					'idhonor' => $idhonor,
					'idtransaksi' => $idpendaftaran,
					'jenis_transaksi' => 'rawatinap',
					'jenis_tindakan' => 'JASA DOKTER RAWAT JALAN',
					'reference_table' => 'tpoliklinik_pelayanan',
					'jenis_pasien' => $jenis_pasien,
					'idkelompok' => $idkelompok,
					'namakelompok' => $namakelompok,
					'idrekanan' => $idrekanan,
					'namarekanan' => $namarekanan,
					'iddetail' => $row->iddetail,
					'idtarif' => $row->idtarif,
					'namatarif' => $row->namatarif,
					'idkategori' => $row->idkategori,
					'namakategori' => $row->namakategori,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'jasamedis' => $row->jasamedis,
					'potongan_rs' => $row->potongan_rs,
					'nominal_potongan_rs' => $nominal_potongan_rs,
					'pajak_dokter' => $row->pajak_dokter,
					'nominal_pajak_dokter' => $nominal_pajak_dokter,
					'jasamedis_netto' => $jasamedis_netto,
					'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
					'tanggal_pembayaran' => $tanggal_pembayaran,
					'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
					'status_asuransi' => $status_asuransi,
					'status_otherincome' => ($idDokterFeeRujukan == $row->iddokter ? 1 : 0),
				];

				if ($this->checkHonorDokterByDetail($dataDetailHonorDokter) == 'STATUS_AVAILABLE') {
					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}

			// [VALIDASI]
			if ($st_insert_detail == '1') {
				$this->Tvalidasi_model->insert_tarif_ranap($idvalidasi, $row, 'mtarif_rawatjalan', 20, $kelas);
			}
		}

		return true;
	}

	public function stopPeriode($idhonor)
	{
		$this->db->set('status_stop', '1');
		$this->db->where('id', $idhonor);
		if ($this->db->update('thonor_dokter')) {
			$this->db->query("CALL setNextPeriodeHonorHold($idhonor)");
			return true;
		}
	}

	public function unStopPeriode($idhonor)
	{
		$this->db->set('status_stop', '0');
		$this->db->where('id', $idhonor);
		if ($this->db->update('thonor_dokter')) {
			return true;
		}
	}

	public function nextPeriode($data)
	{
		$idhonorSebelumnya = $data['idhonor'];
		$idhonor = $this->checkStatusHonorDokter($data['iddokter'], $data['periode_pembayaran']);
		if ($idhonor == null) {
			$dataHonorDokter = [
				'iddokter' => $data['iddokter'],
				'namadokter' => $data['namadokter'],
				'tanggal_pembayaran' => $data['periode_pembayaran'],
				'tanggal_jatuhtempo' => $data['periode_jatuhtempo'],
				'nominal' => 0,
				'created_at' => date('Y-m-d H:i:s'),
				'created_by' => $this->session->userdata('user_id')
			];

			if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
				$idhonor = $this->db->insert_id();
			}
		}

		$this->db->set('idhonor', $idhonor);
		$this->db->set('tanggal_pembayaran_selanjutnya', null);
		$this->db->set('tanggal_jatuhtempo_selanjutnya', null);
		$this->db->set('idhonor_selanjutnya', null);
		$this->db->where('idhonor', $idhonorSebelumnya);
		if ($this->db->update('thonor_dokter_detail')) {
			return true;
		}
	}

	public function getHeadDokumenUpload($idhonor)
	{
		$this->db->where('id', $idhonor);
		$query = $this->db->get('thonor_dokter');
		return $query->row();
	}

	public function getListUploadedDocument($idhonor)
	{
		$this->db->where('idhonor', $idhonor);
		$query = $this->db->get('thonor_dokter_dokumen');
		return $query->result();
	}
}
