<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mtarif_sewaalat_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDetailTarif($id)
    {
        $this->db->select('mtarif_operasi_sewaalat_detail.*, mjenis_operasi.nama,mkelas.nama as namakelas');
        $this->db->join('mjenis_operasi', 'mjenis_operasi.id = mtarif_operasi_sewaalat_detail.idjenis');
        $this->db->join('mkelas', 'mkelas.id = mtarif_operasi_sewaalat_detail.kelas');
        $this->db->where('mtarif_operasi_sewaalat_detail.idtarif', $id);
        $this->db->where('mtarif_operasi_sewaalat_detail.status', '1');
        $this->db->order_by('mtarif_operasi_sewaalat_detail.idjenis', 'ASC');
        $this->db->order_by('mtarif_operasi_sewaalat_detail.kelas', 'ASC');
        $query = $this->db->get('mtarif_operasi_sewaalat_detail');
        return $query->result();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mtarif_operasi_sewaalat');
        return $query->row();
    }
    public function insert_new($id)
    {
        $q="INSERT INTO mtarif_operasi_sewaalat_detail
			SELECT '$id' as idtarif,mjenis_operasi.id as idjenis,mkelas.id as kelas,'0' as jasasarana,'0' as jasapelayanan,'0' as bhp,'0' as biayaperawatan,'0' as total,'1' as status FROM mjenis_operasi,mkelas where mjenis_operasi.id NOT IN (
				SELECT idjenis From mtarif_operasi_sewaalat_detail WHERE idtarif='$id'
			) AND mjenis_operasi.`status`='1'
			UNION
			SELECT
			'$id' as idtarif,mjenis_operasi.id as idjenis,mkelas.id as kelas,'0' as jasasarana,'0' as jasapelayanan,'0' as bhp,'0' as biayaperawatan,'0' as total,'1' as status
			from mkelas,mjenis_operasi WHERE mkelas.id NOT IN (SELECT kelas From mtarif_operasi_sewaalat_detail WHERE idtarif='$id') AND mkelas.`status`='1'";
        $this->db->query($q);
        // $query = $this->db->get('mtarif_operasi_sewaalat');
        return true;
    }

    public function saveData()
    {
        $this->nama         = $_POST['nama'];
        $this->idkelompok   = $_POST['idkelompok'];
        $this->headerpath   = $_POST['headerpath'];
        $this->path         = $_POST['path'];
        $this->level        = $_POST['level'];
        $this->created_by  = $this->session->userdata('user_id');
        $this->created_date  = date('Y-m-d H:i:s');


        if ($this->db->insert('mtarif_operasi_sewaalat', $this)) {
            $idtarif = $this->db->insert_id();
            $detail_value = json_decode($_POST['detail_value']);
            foreach ($detail_value as $row) {
                if (isset($row[1])) {
                    $detail = array();
                    $detail['idtarif']          = $idtarif;
                    $detail['idjenis']          = $row[1];
                    $detail['kelas']            = $row[2];
                    $detail['jasasarana']       = RemoveComma($row[3]);
                    $detail['jasapelayanan']    = RemoveComma($row[4]);
                    $detail['bhp']              = RemoveComma($row[5]);
                    $detail['biayaperawatan']   = RemoveComma($row[6]);
                    $detail['total']            = RemoveComma($row[7]);

                    $this->db->insert('mtarif_operasi_sewaalat_detail', $detail);
                }
            }

            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama						= $_POST['nama'];
        $this->idkelompok			= $_POST['idkelompok'];
        $this->edited_by  = $this->session->userdata('user_id');
        $this->edited_date  = date('Y-m-d H:i:s');
        // print_r(json_decode($_POST['detail_value']));exit();
        if ($this->db->update('mtarif_operasi_sewaalat', $this, array('id' => $_POST['id']))) {
            $idtarif = $_POST['id'];
            $this->db->where('idtarif', $idtarif);
            if ($this->db->delete('mtarif_operasi_sewaalat_detail')) {
                $detail_value = json_decode($_POST['detail_value']);
                foreach ($detail_value as $row) {
                    if (isset($row[1])) {
                        $detail = array();
                        $detail['idtarif']          = $idtarif;
                        $detail['idjenis']          = $row[1];
                        $detail['kelas']            = $row[2];
                        $detail['jasasarana']       = RemoveComma($row[3]);
                        $detail['jasapelayanan']    = RemoveComma($row[4]);
                        $detail['bhp']              = RemoveComma($row[5]);
                        $detail['biayaperawatan']   = RemoveComma($row[6]);
                        // $detail['total']            = RemoveComma($row[6]);

                        $this->db->insert('mtarif_operasi_sewaalat_detail', $detail);
                    }
                }
            }

            // IF CHANGE PARENT
            if ($_POST['headerpath'] != $_POST['old_headerpath']) {
                $this->db->query("call updateTarifSewaAlat('".$_POST['id']."','".$_POST['headerpath']."')");
            }
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
        $this->deleted_by  = $this->session->userdata('user_id');
        $this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('mtarif_operasi_sewaalat', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function getAllParent($headerpath=0, $level=999)
    {
        # Get ID Root
        $this->db->select('MAX(CAST(headerpath AS SIGNED)) + 1 AS idroot');
        $this->db->where('status', '1');
        $query = $this->db->get('mtarif_operasi_sewaalat');
        $row = $query->row();

        if ($headerpath != 0) {
            $idroot = $headerpath;
        } else {
            $idroot = ($row->idroot ? $row->idroot : 1);
        }

        # Get Option
        $this->db->where('status', '1');
        $this->db->order_by('path', 'ASC');
        $query = $this->db->get('mtarif_operasi_sewaalat');
        $result = $query->result();

        $selected = ($level == 0 ? 'selected' : '');
        $data = '<option value="'.$idroot.'" '.$selected.'>Root</option>';
        if ($query->num_rows() > 0) {
            foreach ($result as $row) {
                if ($headerpath != 0 && $level != 0) {
                    $selected = ($headerpath == $row->path ? 'selected' : '');
                } else {
                    $selected = '';
                }
                $data .= '<option value="'.$row->path.'" '.$selected.'>'.TreeView($row->level, $row->nama).'</option>';
            }
        }
        return $data;
    }

    public function getPathLevel($headerpath)
    {
        $strquery = "SELECT LEVEL + 1 AS level, CASE WHEN ( SELECT COUNT(headerpath) FROM mtarif_operasi_sewaalat WHERE headerpath = t1.headerpath ) = 0 THEN CONCAT(t1.path, '.1') ELSE  CONCAT(t1.path,'.',  IF ( ( SELECT COUNT(headerpath) FROM mtarif_operasi_sewaalat WHERE headerpath = t1.path ) = 0, ( SELECT COUNT(headerpath) FROM mtarif_operasi_sewaalat WHERE headerpath = t1.path ) + 1, IF ( LENGTH(t1.path) = '1',( SELECT COUNT(headerpath) FROM mtarif_operasi_sewaalat WHERE headerpath = t1.path ), ( SELECT COUNT(headerpath) FROM mtarif_operasi_sewaalat WHERE headerpath = t1.path ) + 1 ) ) ) END AS path FROM mtarif_operasi_sewaalat t1 WHERE t1.path = '".$headerpath."'";
        $query = $this->db->query($strquery);
        $row = $query->row();

        if ($query->num_rows() > 0) {
            $data['path']  = $row->path;
            $data['level'] = $row->level;
        } else {
            $data['path']  = 1;
            $data['level'] = 0;
        }
        return $data;
    }

    public function updateSetting()
    {
        $settingList = json_decode($_POST['setting_value']);
        foreach ($settingList as $row) {
          if ($row[1] != 'SKIP') {
            $data = array();
            $data['group_jasasarana']     = $row[4];
            $data['group_jasapelayanan']  = $row[5];
            $data['group_bhp']            = $row[6];
            $data['group_biayaperawatan'] = $row[7];

            $this->db->update(
                'mtarif_operasi_sewaalat_detail',
                $data,
                array(
                'idtarif' => $row[0],
                'idjenis' => $row[1],
                'kelas' => $row[2]
              )
            );
          }
        }

        return true;
    }
	public function updateSetting_diskon()
    {
        $settingList = json_decode($_POST['setting_value']);
        foreach ($settingList as $row) {
          if ($row[1] != 'SKIP') {
            $data = array();
            $data['group_jasasarana_diskon']     = $row[4];
            $data['group_jasapelayanan_diskon']  = $row[5];
            $data['group_bhp_diskon']            = $row[6];
            $data['group_biayaperawatan_diskon'] = $row[7];

            $this->db->update(
                'mtarif_operasi_sewaalat_detail',
                $data,
                array(
                'idtarif' => $row[0],
                'idjenis' => $row[1],
                'kelas' => $row[2]
              )
            );
          }
        }
		$data_edit=array('group_diskon_all' => $this->input->post('group_diskon_all'));
		$this->db->where('id',$this->input->post('id'));
		$this->db->update('mtarif_operasi_sewaalat',$data_edit);
        return true;
    }
}
