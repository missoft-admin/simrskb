<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lpenerimaan_gudang_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($filter)
    {
        $tipe_gudang = $filter['tipe_gudang'];
		$periode_tanggal = $filter['periode_tanggal'];
		$tanggal_dari = $filter['tanggal_dari'];
		$tanggal_sampai = $filter['tanggal_sampai'];
		$distributor_id = $filter['distributor_id'];
		$tipe_barang = $filter['tipe_barang'];
		$order_by = $filter['order_by'];

        // Build filter
		$where = [];

		// Filter tipe gudang
		if (!empty($tipe_gudang)) {
			$where[] = "tgudang_penerimaan.tipepenerimaan IN (" . implode(',', $tipe_gudang) . ")";
		}

		// Filter tanggal range
		if (!empty($tanggal_dari) && !empty($tanggal_sampai)) {
			$where[] = "tgudang_penerimaan.tgl_terima BETWEEN STR_TO_DATE('$tanggal_dari', '%d/%m/%Y') AND STR_TO_DATE('$tanggal_sampai', '%d/%m/%Y')";
		} else {
			$where[] = "tgudang_penerimaan.tgl_terima = '" . date("Y-m-d") . "'";
        }

		// Filter distributor
		if (!empty($distributor_id)) {
			$where[] = "mdistributor.id IN (" . implode(',', $distributor_id) . ")";
		}

		// Filter tipe barang
		if (!empty($tipe_barang)) {
			$where[] = "tgudang_penerimaan_detail.idtipe IN (" . implode(',', $tipe_barang) . ")";
		}

		// Gabungkan semua filter
		$where_clause = !empty($where) ? 'WHERE ' . implode(' AND ', $where) : '';

        $query = "SELECT
            tgudang_penerimaan.id,
            tgudang_penerimaan.idpemesanan,
            merm_pengaturan_laporan_gudang.label_name AS tipe_gudang,
            mdistributor.nama AS nama_distributor, 
            tgudang_penerimaan.tgl_terima AS tanggal_terima, 
            tgudang_penerimaan.nopenerimaan AS nomor_penerimaan, 
            tgudang_penerimaan.nofakturexternal AS nomor_faktur,  
            tgudang_penerimaan.tanggaljatuhtempo AS tanggal_jatuh_tempo,
            view_barang_all.kode AS kode_barang, 
            view_barang_all.nama AS nama_barang, 
            tgudang_penerimaan_detail.namasatuan AS nama_satuan, 
            tgudang_penerimaan_detail.harga_asli AS harga_satuan, 
            tgudang_penerimaan_detail.kuantitas AS qty, 
            (tgudang_penerimaan_detail.harga_asli * tgudang_penerimaan_detail.kuantitas) AS jumlah_harga, 
            tgudang_penerimaan_detail.diskon AS disc, 
            tgudang_penerimaan_detail.nominaldiskon * tgudang_penerimaan_detail.kuantitas AS jumlah_disc, 
            tgudang_penerimaan_detail.ppn AS ppn, 
            (tgudang_penerimaan_detail.nominalppn * tgudang_penerimaan_detail.kuantitas) AS nominal_ppn, 
            (tgudang_penerimaan_detail.harga_asli + tgudang_penerimaan_detail.nominalppn) * tgudang_penerimaan_detail.kuantitas AS nilai_persediaan, 
            (tgudang_penerimaan_detail.harga_after_ppn * tgudang_penerimaan_detail.kuantitas) AS nilai_faktur,
            tgudang_penerimaan_detail.tanggalkadaluarsa AS tanggal_kadaluarsa, 
            (CASE
                WHEN tgudang_penerimaan.tipe_bayar = '1' THEN 'TUNAI'
                ELSE 'KREDIT'
            END) AS bayar,
            tgudang_penerimaan.keterangan AS keterangan,
            tgudang_pemesanan.tanggal AS tanggal_transaksi,
            tgudang_penerimaan.userpenerimaan AS user_terima,
            tgudang_pemesanan.nopemesanan AS nomor_pemesanan,
            tgudang_pemesanan.tgl_pemesanan AS tanggal_pemesanan
            FROM 
            tgudang_penerimaan
            LEFT JOIN tgudang_penerimaan_detail ON tgudang_penerimaan_detail.idpenerimaan = tgudang_penerimaan.id 
            LEFT JOIN tgudang_pemesanan ON tgudang_pemesanan.id = tgudang_penerimaan.idpemesanan
            INNER JOIN mdistributor ON tgudang_penerimaan.iddistributor = mdistributor.id 
            LEFT JOIN view_barang_all ON view_barang_all.id = tgudang_penerimaan_detail.idbarang AND view_barang_all.idtipe = tgudang_penerimaan_detail.idtipe
            LEFT JOIN merm_pengaturan_laporan_gudang ON merm_pengaturan_laporan_gudang.jenis_gudang = tgudang_penerimaan.tipepenerimaan AND merm_pengaturan_laporan_gudang.status = 1
            $where_clause";

        // Tambahkan order_by jika ada
        if (!empty($order_by)) {
            $query .= " ORDER BY " . implode(',', $order_by);
        }

        return $this->db->query($query)->result();
    }
}
