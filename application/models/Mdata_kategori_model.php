<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mdata_kategori_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_kategori_barang() {
        $this->db->select("
            idtipe,
            CASE WHEN idtipe = 1  THEN 'Alkes'
            WHEN idtipe = 2 THEN 'Implan'
            WHEN idtipe = 3  THEN 'Obat'
            ELSE 'Logistik' END AS nama            
        ");
        $this->db->from('musers_tipebarang');
        $this->db->where('iduser', $this->session->userdata('user_id'));
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mdata_kategori');
        return $query->row();
    }
	public function getParent($id='')
    {
		if ($id){			
			$this->db->where('id <>', $id);
		}
        $query = $this->db->get('mdata_kategori');
        return $query->result_array();
    }
	public function find_parent($idtipe='')
    {
		if ($idtipe){			
			$this->db->where('idtipe', $idtipe);
		}
        $query = $this->db->get('mdata_kategori');
        return $query->result_array();
    }
	public function list_parent($idtipe='')
    {
		if ($idtipe){			
			$this->db->where('idtipe', $idtipe);
		}
        $query = $this->db->get('mdata_kategori');
        return $query->result();
    }

    public function saveData()
    {
		$this->idparent   = $_POST['idparent'];
        $this->idtipe   = $_POST['idtipe'];
        $this->nama     = $_POST['nama'];
        $this->margin   = ($_POST['idtipe'] == '2' ? $_POST['margin'] : 0);
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		
        if ($this->db->insert('mdata_kategori', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->idparent   = $_POST['idparent'];
        $this->idtipe   = $_POST['idtipe'];
        $this->nama     = $_POST['nama'];
        $this->margin   = ($_POST['idtipe'] == '2' ? $_POST['margin'] : 0);
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mdata_kategori', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mdata_kategori', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
