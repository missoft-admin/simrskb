<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpasien_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mfpasien');
        return $query->row();
    }
	public function softDelete($id)
    {
        $this->status = 0;
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mfpasien', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Deleted Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $tahun = $this->input->post('tahun_lahir');
        $bulan = $this->input->post('bulan_lahir');
        $hari  = $this->input->post('hari_lahir');

        $data = array();
        $data['title'] 								= $this->input->post('title');
        $data['nama'] 								= $this->input->post('nama');
        $data['jenis_id'] 						= $this->input->post('jenisidentitas');
        $data['ktp'] 									= $this->input->post('noidentitas');
        $data['jenis_kelamin'] 				= $this->input->post('jeniskelamin');
        $data['alamat_jalan'] 				= $this->input->post('alamat');
        $data['provinsi_id'] 					= $this->input->post('provinsi');
        $data['kabupaten_id'] 				= $this->input->post('kabupaten');
        $data['kecamatan_id'] 				= $this->input->post('kecamatan');
        $data['kelurahan_id'] 				= $this->input->post('kelurahan');
        $data['kodepos'] 							= $this->input->post('kodepos');
        $data['hp'] 									= $this->input->post('nohp');
        $data['telepon'] 							= $this->input->post('telprumah');
        $data['email'] 								= $this->input->post('email');
        $data['tempat_lahir'] 				= $this->input->post('tempatlahir');
        $data['tanggal_lahir'] 				= date("$tahun-$bulan-$hari");
        $data['umur_tahun'] 					= $this->input->post('umurtahun');
        $data['umur_bulan'] 					= $this->input->post('umurbulan');
        $data['umur_hari'] 						= $this->input->post('umurhari');
        $data['golongan_darah'] 			= $this->input->post('golongandarah');
        $data['agama_id'] 						= $this->input->post('agama');
        $data['warganegara'] 					= $this->input->post('kewarganegaraan');
        $data['suku'] 								= $this->input->post('suku');
        $data['pendidikan_id'] 				= $this->input->post('pendidikan');
        $data['pekerjaan_id'] 				= $this->input->post('pekerjaan');
        $data['status_kawin'] 				= $this->input->post('statuskawin');
        $data['nama_keluarga'] 				= $this->input->post('nama_keluarga');
        $data['hubungan_dengan_pasien'] 				= $this->input->post('hubungan_dengan_pasien');
        $data['alamat_keluarga'] 				= $this->input->post('alamat_keluarga');
        $data['telepon_keluarga'] 				= $this->input->post('telepon_keluarga');
        $data['catatan'] 				= $this->input->post('catatan');
        $data['ktp_keluarga'] 				= $this->input->post('ktp_keluarga');
        $data['modified_by'] 				=$this->session->userdata('user_id');
        $data['modified'] 				= date('Y-m-d H:i:s');

        if ($this->db->update('mfpasien', $data, array('id' => $_POST['idpasien']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
