<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mlokasi_ruangan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mlokasi_ruangan');

        return $query->row();
    }

    public function saveData()
    {
        $this->kode = $_POST['kode'];
        $this->nama = $_POST['nama'];
        $this->deskripsi = $_POST['deskripsi'];
        $this->ruangan_id = $_POST['ruangan_id'];
        $this->poliklinik_id = $_POST['poliklinik_id'];
        $this->gedung_id = $_POST['gedung_id'];
        $this->lantai_id = $_POST['lantai_id'];
        
        $this->created_by = $this->session->userdata('user_id');
        $this->created_date = date('Y-m-d H:i:s');

        if ($this->db->insert('mlokasi_ruangan', $this)) {
            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;
    }

    public function updateData()
    {
        $this->kode = $_POST['kode'];
        $this->nama = $_POST['nama'];
        $this->deskripsi = $_POST['deskripsi'];
        $this->ruangan_id = $_POST['ruangan_id'];
        $this->poliklinik_id = $_POST['poliklinik_id'];
        $this->gedung_id = $_POST['gedung_id'];
        $this->lantai_id = $_POST['lantai_id'];
        
        $this->edited_by = $this->session->userdata('user_id');
        $this->edited_date = date('Y-m-d H:i:s');

        if ($this->db->update('mlokasi_ruangan', $this, ['id' => $_POST['id']])) {
            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;
    }

    public function softDelete($id)
    {
        $this->status = 0;
        $this->deleted_by = $this->session->userdata('user_id');
        $this->deleted_date = date('Y-m-d H:i:s');

        if ($this->db->update('mlokasi_ruangan', $this, ['id' => $id])) {
            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;
    }
}
