<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpotongan_dokter_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mpotongan_dokter');
        return $query->row();
    }

    public function saveData()
    {
        $this->nama 					= $_POST['nama'];

        if ($this->db->insert('mpotongan_dokter', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama 					= $_POST['nama'];

        if ($this->db->update('mpotongan_dokter', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('mpotongan_dokter', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
