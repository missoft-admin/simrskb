<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trawatinap_pendaftaran_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->select('trawatinap_pendaftaran.*,
          mpoliklinik.nama AS poliklinik, tpoliklinik_pendaftaran.idtipe AS idtipepoliklinik, tpoliklinik_pendaftaran.id AS idpoliklinik,
          tpoliklinik_pendaftaran.rencana, mdokter.nama AS dokterperujuk');
        $this->db->join('tpoliklinik_pendaftaran','tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mpoliklinik','mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mdokter','mdokter.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
        $this->db->where('trawatinap_pendaftaran.id', $id);

        $query = $this->db->get('trawatinap_pendaftaran');
        return $query->row();
    }

    public function getPrintInfo($idpendaftaran)
    {
        $this->db->select('mfpasien.no_medrec AS nomedrec, mfpasien.nama AS namapasien, mfpasien.jenis_kelamin AS jeniskelamin,
				mfpasien.tanggal_lahir AS tanggallahir, tpoliklinik_pendaftaran.umurtahun,
				tpoliklinik_pendaftaran.umurbulan,tpoliklinik_pendaftaran.umurhari,mfpasien.title,
				mpasien_kelompok.nama AS namakelompok, mdokter.nama AS namadokter,mpekerjaan.nama AS namapekerjaan,
				trawatinap_pendaftaran.tanggaldaftar, trawatinap_pendaftaran.idkelompokpasien, mkelas.nama AS namakelas,
				mbed.nama AS namabed, mrekanan.nama AS namarekanan,trawatinap_pendaftaran.idtipe,trawatinap_pendaftaran.tanggalcheckout,
				kab.nama as kab,kec.nama as kec,desa.nama as desa,mfpasien.alamat_jalan AS alamat');
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien', 'LEFT');
        $this->db->join('mdokter', 'mdokter.id = trawatinap_pendaftaran.iddokterpenanggungjawab', 'LEFT');
        $this->db->join('mkelas', 'mkelas.id = trawatinap_pendaftaran.idkelas', 'LEFT');
        $this->db->join('mbed', 'mbed.id = trawatinap_pendaftaran.idbed', 'LEFT');
        $this->db->join('mrekanan', 'mrekanan.id = trawatinap_pendaftaran.idrekanan', 'LEFT');
        $this->db->join('mpekerjaan', 'mpekerjaan.id = mfpasien.pekerjaan_id', 'LEFT');
        $this->db->join('mfwilayah kab', 'kab.id = trawatinap_pendaftaran.kabupaten_id', 'LEFT');
        $this->db->join('mfwilayah kec', 'kec.id = trawatinap_pendaftaran.kecamatan_id', 'LEFT');
        $this->db->join('mfwilayah desa', 'desa.id = trawatinap_pendaftaran.kecamatan_id', 'LEFT');
        $this->db->where('trawatinap_pendaftaran.id', $idpendaftaran);
        $query = $this->db->get('trawatinap_pendaftaran');
        return $query->row();
    }

    public function getPrintInfoAll()
    {
        $this->db->select('trawatinap_pendaftaran.no_medrec AS nomedrec, trawatinap_pendaftaran.namapasien, trawatinap_pendaftaran.jenis_kelamin AS jeniskelamin,
        trawatinap_pendaftaran.tanggal_lahir AS tanggallahir, trawatinap_pendaftaran.umurtahun, trawatinap_pendaftaran.umurbulan,
        trawatinap_pendaftaran.alamatpasien AS alamat, mpasien_kelompok.nama AS namakelompok, mdokter.nama AS namadokter,
        trawatinap_pendaftaran.tanggaldaftar, mkelas.nama AS namakelas, mbed.nama AS namabed, trawatinap_pendaftaran.idkelompokpasien, COALESCE(mrekanan.nama, "-") AS namarekanan');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien', 'LEFT');
        $this->db->join('mdokter', 'mdokter.id = trawatinap_pendaftaran.iddokterpenanggungjawab', 'LEFT');
        $this->db->join('mkelas', 'mkelas.id = trawatinap_pendaftaran.idkelas', 'LEFT');
        $this->db->join('mbed', 'mbed.id = trawatinap_pendaftaran.idbed', 'LEFT');
        $this->db->join('mrekanan', 'mrekanan.id = trawatinap_pendaftaran.idrekanan', 'LEFT');
        $this->db->where('trawatinap_pendaftaran.idtipe', '1');
        $this->db->where('trawatinap_pendaftaran.status', '1');
        $this->db->where('trawatinap_pendaftaran.statuscheckout', '0');
        $query = $this->db->get('trawatinap_pendaftaran');
        return $query->result();
    }

    public function getDokter()
    {
        $this->db->where('status', 1);
        $this->db->order_by('nama', 'ASC');
        $query = $this->db->get('mdokter');
        return $query->result();
    }

    public function getKelompokPasien()
    {
        $this->db->where('status', 1);
        $query = $this->db->get('mpasien_kelompok');
        return $query->result();
    }

    public function getRekanan()
    {
        $this->db->where('status', 1);
        $query = $this->db->get('mrekanan');
        return $query->result();
    }

    public function getKelas()
    {
        $this->db->where('status', 1);
        $query = $this->db->get('mkelas');
        return $query->result();
    }

    public function getIndexBed($idkelas = '')
    {
        $this->db->where('status', 1);
        if ($idkelas != '') {
            $this->db->where('idkelas', $idkelas);
        }
        $query = $this->db->get('mbed');
        return $query->result();
    }

    public function saveData()
    {
        $data = array();
        $data['idtipe'] = $this->input->post('idtipe');
        $data['idpoliklinik'] = $this->input->post('idpoliklinik');
        $data['tanggaldaftar'] = YMDFormat($this->input->post('tanggaldaftar')).' '.$this->input->post('waktudaftar');
        $data['idtipepasien'] = $this->input->post('idtipepasien');
        $data['idjenispasien'] = $this->input->post('idjenispasien');
        $data['idkelompokpasien'] = $this->input->post('idkelompokpasien');
        $data['idrekanan'] = $this->input->post('idrekanan');
        $data['idtarifbpjskesehatan'] = $this->input->post('idtarifbpjskesehatan');
        $data['idtarifbpjstenagakerja'] = $this->input->post('idtarifbpjstenagakerja');
        $data['idkelompokpasien2'] = $this->input->post('idkelompokpasien2');
        $data['idrekanan2'] = $this->input->post('idrekanan2');
        $data['idtarifbpjskesehatan2'] = $this->input->post('idtarifbpjskesehatan2');
        $data['idtarifbpjstenagakerja2'] = $this->input->post('idtarifbpjstenagakerja2');
        $data['iddokterpenanggungjawab'] = $this->input->post('iddokterpenanggungjawab');

        if ($this->input->post('idtipe') == 1) {
          $data['idruangan'] = $this->input->post('idruangan');
          $data['idkelas'] = $this->input->post('idkelas');
          $data['idbed'] = $this->input->post('idbed');
        } else {
          $data['idruangan'] = 1;
          $data['idkelas'] = 3;
          $data['idbed'] = 0;
        }

        $dataPasien = json_decode($this->input->post('datapasien'));
        $data['idpasien'] = $dataPasien->idpasien;
        $data['title'] = $dataPasien->title;
        $data['namapasien'] = $dataPasien->namapasien;
        $data['no_medrec'] = $dataPasien->no_medrec;
        $data['nohp'] = $dataPasien->nohp;
        $data['telepon'] = $dataPasien->telepon;
        $data['tempat_lahir'] = $dataPasien->tempat_lahir;
        $data['tanggal_lahir'] = $dataPasien->tanggal_lahir;
        $data['alamatpasien'] = $dataPasien->alamatpasien;
        $data['provinsi_id'] = $dataPasien->provinsi_id;
        $data['kabupaten_id'] = $dataPasien->kabupaten_id;
        $data['kecamatan_id'] = $dataPasien->kecamatan_id;
        $data['kelurahan_id'] = $dataPasien->kelurahan_id;
        $data['kodepos'] = $dataPasien->kodepos;
        $data['namapenanggungjawab'] = $dataPasien->namapenanggungjawab;
        $data['hubungan'] = $dataPasien->hubungan;
        $data['umurhari'] = $dataPasien->umurhari;
        $data['umurbulan'] = $dataPasien->umurbulan;
        $data['umurtahun'] = $dataPasien->umurtahun;
        $data['jenis_kelamin'] = $dataPasien->jenis_kelamin;

        $data['iduserinput'] = $this->session->userdata('user_id');
        $data['created_by'] = $this->session->userdata('user_id');
        $data['created_date'] = date('Y-m-d H:i:s');

        if ($this->db->insert('trawatinap_pendaftaran', $data)) {
            if($this->input->post('idtipe') == 1) {
                $this->db->set('kelas_tarif_id', $this->input->post('idkelas'));
                $this->db->set('ruang_id', $this->input->post('idruangan'));
            }
            $this->db->set('idpasien', $dataPasien->idpasien);
            $this->db->set('namapasien', $dataPasien->namapasien);
            $this->db->set('tanggallahir', $dataPasien->tanggal_lahir);
            $this->db->set('umurtahun', $dataPasien->umurtahun);
            $this->db->set('umurbulan', $dataPasien->umurbulan);
            $this->db->set('umurhari', $dataPasien->umurhari);
            $this->db->set('dpjp_id', $this->input->post('iddokterpenanggungjawab'));

            $this->db->set('tipe', $this->input->post('idtipe'));
            $this->db->set('idasalpendaftaran', '2');
            $this->db->set('idpendaftaran', $this->db->insert_id());
            $this->db->set('statusdatang', '1');
            $this->db->set('status', '1');
            $this->db->where('idpendaftaran', $data['idpoliklinik']);
            $this->db->where('idasalpendaftaran', 1);
            $this->db->update('tkamaroperasi_pendaftaran');
            return true;
        } else {
            return false;
        }
    }

    public function updateData()
    {
        $data = array();
        $data['idtipe'] = $this->input->post('idtipe');
        $data['idpoliklinik'] = $this->input->post('idpoliklinik');
        $data['tanggaldaftar'] = YMDFormat($this->input->post('tanggaldaftar')).' '.$this->input->post('waktudaftar');
        $data['idtipepasien'] = $this->input->post('idtipepasien');
        $data['idjenispasien'] = $this->input->post('idjenispasien');
        $data['idkelompokpasien'] = $this->input->post('idkelompokpasien');
        $data['idrekanan'] = $this->input->post('idrekanan');
        $data['idtarifbpjskesehatan'] = $this->input->post('idtarifbpjskesehatan');
        $data['idtarifbpjstenagakerja'] = $this->input->post('idtarifbpjstenagakerja');
        $data['idkelompokpasien2'] = $this->input->post('idkelompokpasien_cob');
        $data['idrekanan2'] = $this->input->post('idrekanan_cob');
        $data['idtarifbpjskesehatan2'] = $this->input->post('idtarifbpjskesehatan_cob');
        $data['idtarifbpjstenagakerja2'] = $this->input->post('idtarifbpjstenagakerja_cob');
        $data['iddokterpenanggungjawab'] = $this->input->post('iddokterpenanggungjawab');

        if ($this->input->post('idtipe') == 1) {
          $data['idruangan'] = $this->input->post('idruangan');
          $data['idkelas'] = $this->input->post('idkelas');
          $data['idbed'] = $this->input->post('idbed');
        } else {
          $data['idruangan'] = 1;
          $data['idkelas'] = 3;
          $data['idbed'] = 0;
        }

        $dataPasien = json_decode($this->input->post('datapasien'));
        $data['idpasien'] = $dataPasien->idpasien;
        $data['title'] = $dataPasien->title;
        $data['namapasien'] = $dataPasien->namapasien;
        $data['no_medrec'] = $dataPasien->no_medrec;
        $data['nohp'] = $dataPasien->nohp;
        $data['telepon'] = $dataPasien->telepon;
        $data['tempat_lahir'] = $dataPasien->tempat_lahir;
        $data['tanggal_lahir'] = $dataPasien->tanggal_lahir;
        $data['alamatpasien'] = $dataPasien->alamatpasien;
        $data['provinsi_id'] = $dataPasien->provinsi_id;
        $data['kabupaten_id'] = $dataPasien->kabupaten_id;
        $data['kecamatan_id'] = $dataPasien->kecamatan_id;
        $data['kelurahan_id'] = $dataPasien->kelurahan_id;
        $data['kodepos'] = $dataPasien->kodepos;
        $data['namapenanggungjawab'] = $dataPasien->namapenanggungjawab;
        $data['hubungan'] = $dataPasien->hubungan;
        $data['umurhari'] = $dataPasien->umurhari;
        $data['umurbulan'] = $dataPasien->umurbulan;
        $data['umurtahun'] = $dataPasien->umurtahun;
        $data['jenis_kelamin'] = $dataPasien->jenis_kelamin;

        $data['edited_by'] = $this->session->userdata('user_id');
        $data['edited_date'] = date('Y-m-d H:i:s');

        $this->db->where('id', $this->input->post('id'));
        if ($this->db->update('trawatinap_pendaftaran', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function getHistory($idpasien)
    {
        $this->db->select('mpoliklinik.nama AS poli,
  				mpoliklinik.idtipe, mdokter.nama as namadokter, tanggaldaftar, idpasien,
  				(CASE
      			WHEN tpoliklinik_pendaftaran.idtipe = 1 THEN "Poliklinik"
      			WHEN tpoliklinik_pendaftaran.idtipe = 2 THEN "IGD"
          END) AS tipe');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mdokter', 'mdokter.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
        $this->db->where('tpoliklinik_pendaftaran.idpasien', $idpasien);
        $query = $this->db->get('tpoliklinik_pendaftaran');
        return $query->result();
    }

    public function getStatusExistTransaction($idpasien, $idtipe)
    {
        $this->db->select('id, idtipe');
        $this->db->where('idpasien', $idpasien);
        $this->db->where('idtipe', $idtipe);
        if ($idtipe == 1) {
          $this->db->where('statuscheckout', '0');
        } else {
          $this->db->where('statustransaksi', '0');  
        }
        $this->db->where('status', '1');
        $query = $this->db->get('trawatinap_pendaftaran');

        if ($query->num_rows() > 0) {
          return true;
        } else {
          return false;
        }
    }

    public function getBed($idruangan, $idkelas, $id)
    {
        $this->db->select("(CASE WHEN trawatinap_pendaftaran.id IS NULL THEN mbed.id ELSE '0' END) AS id,
          (CASE
            WHEN trawatinap_pendaftaran.id IS NULL THEN mbed.nama
            WHEN trawatinap_pendaftaran.statuskasir = '1' THEN CONCAT(mbed.nama, ' (NOT READY)')
            WHEN trawatinap_pendaftaran.statuskasir = '0' THEN CONCAT(mbed.nama, ' (TERISI)')
          END) AS nama");

        if ($id != '') {
          $this->db->join("trawatinap_pendaftaran", "trawatinap_pendaftaran.idbed = mbed.id AND trawatinap_pendaftaran.statuscheckout = 0 AND trawatinap_pendaftaran.status = 1 AND trawatinap_pendaftaran.id != ".$id, "LEFT");
        } else {
          $this->db->join("trawatinap_pendaftaran", "trawatinap_pendaftaran.idbed = mbed.id AND trawatinap_pendaftaran.statuscheckout = 0 AND trawatinap_pendaftaran.status = 1", "LEFT");
        }

        $this->db->where("mbed.status", 1);
        $this->db->where("mbed.idruangan", $idruangan);
        $this->db->where("mbed.idkelas", $idkelas);
        $query = $this->db->get("mbed");
        return $query->result();
    }
}

/* End of file Tpoliklinik_rawatinap_model.php */
/* Location: ./application/models/Tpoliklinik_rawatinap_model.php */
