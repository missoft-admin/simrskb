<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tklaim_monitoring_model extends CI_Model
{
  
   public function detail($id){
	   $q="SELECT TK.id,TK.no_klaim,TK.tipe,CASE WHEN TK.idkelompokpasien !='1' THEN kel.nama ELSE R.nama END as rekanan_nama,TK.idkelompokpasien
					,TK.idrekanan,TK.jml_faktur,TK.total_tagihan,TK.tanggal_tagihan
					,DATEDIFF(TK.tanggal_kirim, TK.batas_kirim) as selisih,TK.jatuh_tempo_bayar,TK.kirim_date,TK.noresi
					,TK.`status`,TK.status_kirim
					FROM tklaim TK
					LEFT JOIN mpasien_kelompok kel ON kel.id=TK.idkelompokpasien
					LEFT JOIN mrekanan R ON R.id=TK.idrekanan

					WHERE TK.id='$id'";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function list_kelompok_pasien(){
	   $q="SELECT id,nama from mpasien_kelompok 
				WHERE mpasien_kelompok.`status`='1' AND mpasien_kelompok.id !='5'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_rekanan(){
	   $q="SELECT id,nama from mrekanan 
				WHERE mrekanan.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_bank(){
	   $q="SELECT id,nama,norekening from mbank 
				WHERE mbank.`status`='1'
				ORDER BY id ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function insert_jurnal_other_loss($id){
		// $id='60';
		// $tanggal='2021-08-08';
		$q="SELECT 
				D.klaim_id as idtransaksi,CURRENT_DATE as tanggal_transaksi,H.no_klaim as notransaksi,D.tipe
				,CASE WHEN H.tipe='1' THEN 'RAWAT JALAN' ELSE 'RAWAT INAP / ODS' END as tipe_nama
							,CASE WHEN H.idkelompokpasien !='1' THEN KP.nama ELSE MR.nama END as nama_rekanan
							,0 as nominal_bayar,D.tidak_terbayar as total_tagihan,S.st_auto_posting,S.idakun_tunai, H.idkelompokpasien,H.idrekanan
				FROM `tklaim_detail` D 
				LEFT JOIN tklaim H ON H.id=D.klaim_id
				LEFT JOIN mpasien_kelompok KP ON KP.id=H.idkelompokpasien
							LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
							LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=D.pendaftaran_id AND D.tipe='1'
				LEFT JOIN msetting_jurnal_klaim S ON S.id='1'
				WHERE D.id='$id'";
		$row=$this->db->query($q)->row();
		$data_header=array(
			'idtransaksi' => $row->idtransaksi,
			'tanggal_transaksi' => YMDFormat($row->tanggal_transaksi),
			'notransaksi' => $row->notransaksi,
			'tipe' => $row->tipe,
			'tipe_nama' => $row->tipe_nama,
			'idkelompokpasien' => $row->idkelompokpasien,
			'idrekanan' => $row->idrekanan,
			'nama_rekanan' => $row->nama_rekanan,
			'nominal_bayar' => $row->total_tagihan,
			'st_other_loss' => '1',
			'status' => '1',
			'st_auto_posting' => $row->st_auto_posting,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
            'created_date'=>date('Y-m-d H:i:s')
		);
		$st_auto_posting=$row->st_auto_posting;
		$this->db->insert('tvalidasi_klaim',$data_header);
		$idvalidasi=$this->db->insert_id();
		
		$q="SELECT D.id as idbayar_id,D.id as id_det,D.pendaftaran_id,H.idkelompokpasien,H.idrekanan,D.tipe
			,CASE WHEN D.tipe='1' THEN 'RAWAT JALAN' ELSE 'RAWAT INAP' END as nama_tipe
			,CASE WHEN D.tipe='1' THEN TP.nopendaftaran ELSE RI.nopendaftaran END as nopendaftaran
			,CASE WHEN D.tipe='1' THEN TP.no_medrec ELSE RI.no_medrec END as no_medrec
			,CASE WHEN D.tipe='1' THEN TP.title ELSE RI.title END as title
			,CASE WHEN D.tipe='1' THEN TP.namapasien ELSE RI.namapasien END as namapasien
			,CASE WHEN D.tipe='1' THEN MP.nama ELSE MK.nama END as poli
			,CASE WHEN D.tipe='1' THEN MD1.nama ELSE MD2.nama END as dokter
			,D.tidak_terbayar as nominal_piutang,0 as nominal_other_income,D.tidak_terbayar as nominal_other_loss
			,compare_value_2(MAX(IF(SP.idrekanan = H.idrekanan,SP.idakun,NULL)),
							MAX(IF(SP.idrekanan = 0 AND SP.idkelompokpasien = H.idkelompokpasien,SP.idakun,NULL))) idakun_piutang
			,compare_value_2(MAX(IF(SI.idrekanan = H.idrekanan,SI.idakun,NULL)),
							MAX(IF(SI.idrekanan = 0 AND SI.idkelompokpasien = H.idkelompokpasien,SI.idakun,NULL))) idakun_other_loss,0 as idakun_income
							,'K' as posisi_piutang,'K' as posisi_income,'D' as posisi_other_loss
		FROM tklaim_detail D 
			LEFT JOIN tklaim H ON H.id=D.klaim_id
			LEFT JOIN mpasien_kelompok KP ON KP.id=H.idkelompokpasien
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
			LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=D.pendaftaran_id AND D.tipe='1'
			LEFT JOIN mpoliklinik MP ON MP.id=TP.idpoliklinik AND D.tipe='1'
			LEFT JOIN mdokter MD1 ON MD1.id=TP.iddokter AND D.tipe='1'
			LEFT JOIN trawatinap_pendaftaran RI ON RI.id=D.pendaftaran_id AND D.tipe='2'
			LEFT JOIN mkelas MK ON RI.idkelas=MK.id AND D.tipe='2'
			LEFT JOIN mdokter MD2 ON RI.iddokterpenanggungjawab=MD2.id AND D.tipe='2'
			LEFT JOIN msetting_jurnal_klaim_piutang SP ON SP.idkelompokpasien=H.idkelompokpasien
			LEFT JOIN msetting_jurnal_klaim_loss SI ON SI.idkelompokpasien=H.idkelompokpasien
			WHERE D.id='$id' 
			GROUP BY D.id
			";
		$rows=$this->db->query($q)->result();
		$total_transaksi=0;
		foreach($rows as $r){
			$data_detail=array(
				'idvalidasi' => $idvalidasi ,
				'idbayar_id' =>$r->idbayar_id,
				'id_det' =>$r->id_det,
				'pendaftaran_id' =>$r->pendaftaran_id,
				'tipe' =>$r->tipe,
				'nopendaftaran' =>$r->nopendaftaran,
				'nama_tipe' =>$r->nama_tipe,
				'idkelompokpasien' =>$r->idkelompokpasien,
				'idrekanan' =>$r->idrekanan,
				'no_medrec' =>$r->no_medrec,
				'title' =>$r->title,
				'namapasien' =>$r->namapasien,
				'poli' =>$r->poli,
				'dokter' =>$r->dokter,
				'nominal_piutang' =>$r->nominal_piutang,
				'nominal_other_income' =>$r->nominal_other_income,
				'nominal_other_loss' =>$r->nominal_other_loss,
				'idakun_piutang' =>$r->idakun_piutang,
				'idakun_income' =>$r->idakun_income,
				'idakun_other_loss' =>$r->idakun_other_loss,
				'posisi_piutang' =>$r->posisi_piutang,
				'posisi_income' =>$r->posisi_income,
				'posisi_other_loss' =>$r->posisi_other_loss,
			);
			// $total_transaksi=$total_transaksi + $r->nominal_beli + $r->nominal_ppn - $r->nominal_diskon;
			$this->db->insert('tvalidasi_klaim_detail',$data_detail);
		}		
		
		if ($st_auto_posting=='1'){
			$data_header=array(
				
				'st_posting' => 1,
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
				

			);
			$this->db->where('id',$idvalidasi);
			$this->db->update('tvalidasi_klaim',$data_header);
		}
		return true;
	}
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
