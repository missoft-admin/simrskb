<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mtarif_harga_kamar_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mtarif_harga_kamar');
        return $query->row();
    }

    public function updateData()
    {
        $this->truang_perawatan	= $_POST['truang_perawatan'];
        $this->truang_hcu		= $_POST['truang_hcu'];
        $this->truang_icu		= $_POST['truang_icu'];
        $this->truang_isolasi	= $_POST['truang_isolasi'];
        if ($this->db->update('mtarif_harga_kamar', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
