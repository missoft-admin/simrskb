<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Lvalidasi_gudang_model extends CI_Model
{
   public function list_akun(){
	   $q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		return $list;
   }
   public function getHeader($id){
	   $q="SELECT * from tvalidasi_gudang 
				WHERE tvalidasi_gudang.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function saveData(){
	   
		$id=($this->input->post('id'));
		$iddet=($this->input->post('iddet'));
		$idakun_beli=($this->input->post('idakun_beli'));
		$idakun_ppn=($this->input->post('idakun_ppn'));
		$idakun_diskon=($this->input->post('idakun_diskon'));
		$st_posting=($this->input->post('st_posting'));
		$btn_simpan=($this->input->post('btn_simpan'));
		foreach($iddet as $index => $val){
			$row_beli=$this->get_nama_akun($idakun_beli[$index]);
			// print_r($idakun_beli[$index]);exit();
			// print_r($row_beli);exit();
			$row_ppn=$this->get_nama_akun($idakun_ppn[$index]);
			$row_diskon=$this->get_nama_akun($idakun_diskon[$index]);
			$detail=array(
				'idakun_beli'=>$idakun_beli[$index],
				'idakun_ppn'=>$idakun_ppn[$index],
				'idakun_diskon'=>$idakun_diskon[$index],
				
				'noakun_beli'=>$row_beli->noakun,
				'noakun_ppn'=>$row_ppn->noakun,
				'noakun_diskon'=>$row_diskon->noakun,
				
				'namaakun_beli'=>$row_beli->namaakun,
				'namaakun_ppn'=>$row_ppn->namaakun,
				'namaakun_diskon'=>$row_diskon->namaakun,
				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_gudang_detail',$detail);
		}
		if ($st_posting=='1'){
			$this->db->where('id',$id);
			$this->db->update('tvalidasi_gudang',array('st_posting'=>0));
		}
		if ($btn_simpan=='2'){
			$data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $id);
			$result=$this->db->update('tvalidasi_gudang', $data);
		}
		
		return true;
   }
   function get_nama_akun($id){
	   $q="SELECT *from makun_nomor A WHERE A.id='$id'";
	   return $this->db->query($q)->row();
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
