<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tcheckin_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function list_antrian(){
		$q="SELECT H.id,H.kodeantrian,H.tanggal FROM `antrian_harian` H
WHERE H.tanggal >= CURRENT_DATE()";
		return $this->db->query($q)->result();
	}
	function list_poli(){
		$q="SELECT id,nama FROM `mpoliklinik` H
			
			WHERE H.`status`='1'";
		return $this->db->query($q)->result();
	}
	function list_tanggal($idpoli,$iddokter){
		$q="SELECT H.tanggal,MC.sebab,MC.id as cuti_id,CASE WHEN MC.id IS NOT NULL THEN 'disabled' ELSE '' END st_disabel 
				,CONCAT(MR.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y') )as tanggal_nama,H.kodehari
				FROM `app_reservasi_tanggal` H
				LEFT JOIN mcuti_dokter MC ON MC.iddokter=H.iddokter AND H.tanggal BETWEEN MC.tanggal_dari AND MC.tanggal_sampai AND MC.`status`='1'
				LEFT JOIN merm_hari MR ON MR.kodehari=H.kodehari
				WHERE H.reservasi_tipe_id='1' AND H.iddokter='$iddokter' AND H.idpoli='$idpoli' AND H.tanggal >= CURRENT_DATE AND H.tanggal<DATE_ADD(CURRENT_DATE, INTERVAL 7 DAY) 
				GROUP BY H.tanggal";
		$data = $this->db->query($q)->result();
		return $data;
	}
	function list_jadwal($idpoli,$iddokter,$tanggal){
		$q="SELECT H.jadwal_id,CONCAT(DATE_FORMAT(JD.jam_dari,'%H:%i') ,' - ',DATE_FORMAT(JD.jam_sampai,'%H:%i') ) jam_nama
			,H.saldo_kuota,CASE WHEN H.saldo_kuota > 0 THEN '' ELSE 'disabled' END st_disabel
			FROM `app_reservasi_tanggal` H
			LEFT JOIN mjadwal_dokter JD ON JD.id=H.jadwal_id
			WHERE H.reservasi_tipe_id='1' AND H.iddokter='$iddokter' AND H.idpoli='$idpoli' AND H.tanggal = '$tanggal'
			GROUP BY H.tanggal,H.jadwal_id";
		$data = $this->db->query($q)->result();
		return $data;
	}
	function list_dokter($idpoli=''){
		if ($idpoli==''){
			$q="SELECT H.id,H.nama FROM `mdokter` H WHERE H.`status`='1'";
		}else{
			$q="SELECT MD.id,MD.nama FROM mpoliklinik_dokter H
			INNER JOIN mdokter MD ON MD.id=H.iddokter AND MD.`status`='1'
			WHERE H.`status`='1' AND H.idpoliklinik='$idpoli'";
		}
		
		return $this->db->query($q)->result();
	}
	function load_edit_poli($id){
		$q="SELECT MD.foto as foto_dokter,MD.jeniskelamin as jk_dokter,MD.nama as nama_dokter,P.nama as nama_poli,
			H.pendidikan_id as pendidikan,H.pekerjaan_id as pekerjaan
			,
			CASE 
			
			WHEN H.reservasi_tipe_id=1 THEN 
				CONCAT(merm_hari.namahari,', ',DATE_FORMAT(H.tanggal,'%d-%m-%Y'),' Pukul : ',DATE_FORMAT(MJ.jam_dari,'%H:%i'),' - ',DATE_FORMAT(MJ.jam_sampai,'%H:%i'))
			WHEN H.reservasi_tipe_id=2 THEN 
				CONCAT(merm_hari.namahari,', ',DATE_FORMAT(H.tanggal,'%d-%m-%Y'),' Pukul : ',CONCAT(MJ2.jam,'.00'),' - ',CONCAT(MJ2.jam_akhir,'.00')) END
			
			 as janji_temu
			,H.* 
			
			FROM `app_reservasi_tanggal_pasien` H
			LEFT JOIN mfpasien MP ON MP.id=H.idpasien
			LEFT JOIN merm_hari ON merm_hari.kodehari=H.kodehari
			LEFT JOIN mjadwal_dokter MJ ON MJ.id=H.jadwal_id
			LEFT JOIN mdokter MD ON MD.id=H.iddokter
			LEFT JOIN mpoliklinik P ON P.id=H.idpoli
			LEFT JOIN merm_jam MJ2 ON MJ2.jam_id=H.jam_id
			WHERE H.id='$id'";
			// print_r($q);exit;
		return $this->db->query($q)->row_array();
	}
	
	function get_gc($id){
		$q="SELECT app_id,jawaban_ttd,jawaban_perssetujuan,logo as logo_gc,judul as judul_gc,sub_header as sub_header_gc
		,sub_header_side as sub_header_side_gc,footer_1 as footer_1_gc,footer_2 as footer_2_gc,ttd_1,ttd_2,footer_form_1,footer_form_2 FROM app_reservasi_tanggal_pasien_gc_head WHERE app_id='$id'";
		return $this->db->query($q)->row_array();
	}
	function get_sp($id){
		$q="SELECT logo as logo_sp,judul as judul_sp FROM `merm_skrining_pasien`";
		return $this->db->query($q)->row_array();
	}
	function get_sc($id){
		$q="SELECT logo as logo_sc,judul as judul_sc FROM `merm_skrining_covid`";
		return $this->db->query($q)->row_array();
	}
	function list_content_gc($id){
		$q="SELECT  * FROM app_reservasi_tanggal_pasien_gc WHERE app_id='$id'";
		return $this->db->query($q)->result();
	}
	function list_cara_bayar(){
		$q="SELECT *FROM mpasien_kelompok H WHERE H.`status`='1'";
		return $this->db->query($q)->result();
	}
	function list_asuransi(){
		$q="SELECT M.* FROM setting_reservasi_petugas_asuransi H INNER JOIN mrekanan M ON M.id=H.idrekanan";
		return $this->db->query($q)->result();
	}
}
