<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Tstockopname_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getUnitPelayanan() {
        $this->db->where('status', '1');
        $query = $this->db->get('munitpelayanan');

        return $query->result();
    }

    public function getStatusStockOpname($periode) {
        $this->db->where('periode', $periode);
        $query = $this->db->get('tstockopname');

        return $query->num_rows();
    }
	public function list_periode() {
        $q="SELECT DISTINCT(periode) From tstockopname ";
		$query=$this->db->query($q);
		return $query->result();
    }
	public function list_periode_desc() {
        $q="SELECT DISTINCT(periode) FROM tstockopname ORDER BY periode DESC";
		$query=$this->db->query($q);
		return $query->result();
    }
	public function list_tipegudang() {
        $this->db->where('iduser', $this->session->userdata('user_id'));
        $query = $this->db->get('musers_tipegudang');

        return $query->result();
    }
	public function list_tipe($idunit) {
		$iduser=$this->session->userdata('user_id');
		// $idunit=$this->session->userdata('user_id');
		$q="SELECT M.id,M.nama_tipe as text FROM munitpelayanan_tipebarang H 
			LEFT JOIN mdata_tipebarang M ON M.id=H.idtipe
			WHERE H.idunitpelayanan='$idunit' 
			AND H.idtipe IN (
			SELECT T.idtipe from musers_tipebarang T
			WHERE T.iduser='$iduser'
			)";
		$query=$this->db->query($q);
        return $query->result();
    }
	public function list_tipe2($idunit) {
		$iduser=$this->session->userdata('user_id');
		// $idunit=$this->session->userdata('user_id');
		if ($idunit=='#'){
			$q="SELECT DISTINCT(M.id) FROM munitpelayanan_tipebarang H
					LEFT JOIN mdata_tipebarang M ON M.id=H.idtipe
					WHERE H.idunitpelayanan IN (SELECT MU.idunitpelayanan FROM munitpelayanan_user MU WHERE MU.userid='$iduser')
					AND H.idtipe IN (
					SELECT T.idtipe from musers_tipebarang T
					WHERE T.iduser='$iduser')";
		}else{
			$q="SELECT M.id FROM munitpelayanan_tipebarang H 
				LEFT JOIN mdata_tipebarang M ON M.id=H.idtipe
				WHERE H.idunitpelayanan='$idunit' 
				AND H.idtipe IN (
				SELECT T.idtipe from musers_tipebarang T
				WHERE T.iduser='$iduser'
				)";
		}
		$query=$this->db->query($q);
        $row= $query->result_array();
		$arr = array_map (function($value){
				return $value['id'];
			} , $row);
			 return (implode(', ',$arr));
    }
	public function list_kategori($idtipe) {
		
		$q="SELECT id,`level`,path,
				CASE 
					WHEN nama_L1 IS NOT NULL THEN nama_L1
					WHEN nama_L2 IS NOT NULL THEN nama_L2
					WHEN nama_L3 IS NOT NULL THEN nama_L3
					WHEN nama_L4 IS NOT NULL THEN nama_L4
					WHEN nama_L5 IS NOT NULL THEN nama_L5
					WHEN nama_L6 IS NOT NULL THEN nama_L6
					WHEN nama_L7 IS NOT NULL THEN nama_L7
				  ELSE NULL
				  
				END as nama FROM (
			SELECT *FROM view_kategori_alkes
			UNION
			SELECT *FROM view_kategori_implan
			UNION
			SELECT *FROM view_kategori_obat
			UNION
			SELECT *FROM view_kategori_logistik
			) T
			WHERE T.idtipe='$idtipe'";
		$query=$this->db->query($q);
        return $query->result();
    }
	public function cari_so($id) {
        $q="SELECT *from tstockopname H WHERE H.idunitpelayanan='$id' AND (H.`status`='1' OR H.`status`='2') LIMIT 1";
        $query = $this->db->query($q);
		$hasil=array();
		if ($query->num_rows()){
			$hasil['id']=$query->row('id');
			$hasil['nostockopname']=$query->row('nostockopname');
			$hasil['status']=$query->row('status');
			$hasil['tgl_so']=date("d-m-Y H:i:s", strtotime($query->row('tgl_so')));
			$hasil['tgl_input']=date("d-m-Y", strtotime($query->row('tgl_input')));
			// return $query->row('status');
		}else{
			$hasil['nostockopname']=$this->get_no();
			$hasil['id']='';
			$hasil['status']='0';
			$hasil['tgl_so']=date('d-m-Y H:i:s');
			$hasil['tgl_input']=date('d-m-Y');
			// return '0';
		}
		// print_r($hasil);exit();
		
		return $hasil;
        // return ;
    }
	public function get_so($id) {
        $q="SELECT H.*,U.nama as nama_unit from tstockopname H 
			LEFT JOIN munitpelayanan U ON U.id=H.idunitpelayanan
			 WHERE H.id='$id'";
        $query = $this->db->query($q);
		
		
		return $query->row_array();
        // return ;
    }
	public function list_so($id) {
        $q="SELECT SOD.so_id,SOD.idunitpelayanan,SOD.idbarang,SOD.idtipe,B.nama,
			B.namatipe,SOD.namauser_insert,SOD.namauser_update,SOD.status,
			SOD.stoksistem,SOD.stokfisik,B.hargabeli as hpp,SOD.catatan,SOD.selisihstok,
				datetime_update,datetime_insert
					from tstockopname_detail SOD 					
					INNER JOIN view_barang B ON B.id=SOD.idbarang AND B.idtipe=SOD.idtipe
					WHERE SOD.so_id='$id'  
					ORDER BY B.idtipe, B.nama";
        $query = $this->db->query($q);
		
		
		return $query->result();
        // return ;
    }
	public function list_format($id) {
        $q="SELECT B.nama,B.namatipe,S.stok as stoksistem,B.hargabeli as hpp from mgudang_stok S
				INNER JOIN view_barang B ON B.id=S.idbarang AND B.idtipe=S.idtipe

				WHERE S.idunitpelayanan='$id'
				ORDER BY S.idtipe,B.nama";
        $query = $this->db->query($q);
		
		
		return $query->result();
        // return ;
    }
	
	public function cari_periode($id) {
        $q="SELECT periode from tstockopname H WHERE H.id='$id'";
        $query = $this->db->query($q);
		$hasil=$query->row('periode');
		return $hasil;
        // return ;
    }
	function get_no() 
	{	$init="SO".date("y");
    	$query = $this->db->query("SELECT nostockopname FROM tstockopname WHERE nostockopname like '$init%' ORDER BY nostockopname DESC LIMIT 1");    	
		if ($row = $query->row()){
			$hasil=$row->nostockopname ;
			$hasil = substr($hasil,4,7);
			$hasil = $hasil + 1;
			$hasil = $init.substr("0000000",0, 7 - strlen($hasil)).$hasil;			
		}else{
			$hasil=$init."0000001";
		}
		return $hasil;	
	}
    public function saveData($status) {
        $periode = $this->input->post('tanggalopname');
        $idunitpelayanan = $this->input->post('idunit');
        $idtipe = $this->input->post('tipeopname');
        $jenisopname = $this->input->post('jenisopname');

        $data['idunitpelayanan'] = $idunitpelayanan;
        $data['idtipe'] = $idtipe;
        $data['periode'] = $periode;
        $data['jenisopname'] = $jenisopname;
        $data['status'] = $status;

        if ($this->db->insert('tstockopname', $data)) {
            $idstockopname = $this->db->insert_id();

            $idbarang = $this->input->post('idbarang');
            $stoksistem = $this->input->post('stoksistem');
            $stokfisik = $this->input->post('stokfisik');
            $catatan = $this->input->post('catatan');
            $hpp = $this->input->post('hpp');
            $idstok = $this->input->post('idstok');
            $statuses = $this->input->post('status');

            for ($i = 0; $i < count($idbarang); ++$i) {
                $selisih = intval($stoksistem[$i]) - intval($stokfisik[$i]);
                $barang = [];
                $barang['idstockopname'] = $idstockopname;
                $barang['idunitpelayanan'] = $idunitpelayanan;
                $barang['idtipe'] = $idtipe;

                $barang['idbarang'] = $idbarang[$i];
                $barang['stoksistem'] = $stoksistem[$i];
                $barang['stokfisik'] = $stokfisik[$i];
                $barang['selisihstok'] = $selisih;
                $barang['catatan'] = $catatan[$i];
                $barang['hpp'] = $hpp[$i];
                $barang['idstok'] = $idstok[$i];
                $barang['status'] = $statuses[$i];

                $barang['namauser_update'] = $this->session->userdata('user_name');
                $barang['datetime_update'] = date('Y-m-d H:i');

                $this->db->insert('tstockopname_detail', $barang);
                if (1 == $status) {
                    $nostockopname = $this->db->get('tstockopname', ['id' => $idstockopname])->row()->nostockopname;
                    $unitpelayanan = $this->db->get('munitpelayanan', ['id' => $idunitpelayanan])->row()->nama;
                    $in = ($selisih > 0) ? $selisih : 0;
                    $out = ($selisih < 0) ? $selisih * -1 : 0;
                    $this->updateKartuStok(
                        date('Y-m-d'),
                        $nostockopname,
                        $idunitpelayanan,
                        $idtipe,
                        $idbarang[$i],
                        'STOK OPNAME '.$unitpelayanan,
                        $in,
                        $out
                    );
                }
            }

            return true;
        }
        $this->error_message = 'Penyimpanan Gagal';

        return false;
    }

    public function edit($id) {
        $obj = [];
        $this->db->select("
            tstockopname.*,
            munitpelayanan.nama as 'namaunit',
            mdata_tipebarang.nama_tipe as 'namatipe',
            REPLACE(mdata_tipebarang.jenis_tipe,'_',' ') as 'jenistipe',
        ");
        $this->db->join('munitpelayanan', 'munitpelayanan.id=tstockopname.idunitpelayanan');
        $this->db->join('mdata_tipebarang', 'mdata_tipebarang.id=tstockopname.idtipe');
        $this->db->where('tstockopname.id', $id);
        $obj['opname'] = $this->db->get('tstockopname')->row();

        $this->db->select("tstockopname_detail.*,view_barang.nama as 'namabarang',view_barang.namatipe");
        $this->db->where('idstockopname', $id);
        $this->db->join('view_barang', 'view_barang.id=tstockopname_detail.idbarang AND view_barang.idtipe=tstockopname_detail.idtipe');
        $this->db->order_by('tstockopname_detail.idstok ASC');
        $obj['details'] = $this->db->get('tstockopname_detail')->result();

        return $obj;
    }

    public function update($status) {
        $id = $this->input->post('id');
        if ('' != $id) {
            $this->db->where('id', $id);
            $this->db->update('tstockopname', ['status' => $status]);

            $iddetail = $this->input->post('iddetail');
            $idbarang = $this->input->post('idbarang');
            $stoksistem = $this->input->post('stoksistem');
            $stokfisik = $this->input->post('stokfisik');
            $catatan = $this->input->post('catatan');
            $hpp = $this->input->post('hpp');
            $idstok = $this->input->post('idstok');
            $statuses = $this->input->post('status');
            $namauser_update = $this->input->post('namauser_update');
            $datetime_update = $this->input->post('datetime_update');

            for ($i = 0; $i < count($idbarang); ++$i) {
                $selisih = intval($stoksistem[$i]) - intval($stokfisik[$i]);
                $barang = [];
                $barang['idbarang'] = $idbarang[$i];
                $barang['stoksistem'] = $stoksistem[$i];
                $barang['stokfisik'] = $stokfisik[$i];
                $barang['selisihstok'] = intval($stoksistem[$i]) - intval($stokfisik[$i]);
                $barang['catatan'] = $catatan[$i];
                $barang['hpp'] = $hpp[$i];
                $barang['idstok'] = $idstok[$i];
                $barang['status'] = $statuses[$i];

                $barang['namauser_update'] = $namauser_update[$i];
                $barang['datetime_update'] = $datetime_update[$i];

                $this->db->where('id', $iddetail[$i]);
                $this->db->update('tstockopname_detail', $barang);

                if (1 == $status) {
                    $opname = $this->db->get('tstockopname', ['id' => $id])->row();
                    $unitpelayanan = $this->db->get('munitpelayanan', ['id' => $opname->idunitpelayanan])->row()->nama;
                    $in = ($selisih > 0) ? $selisih : 0;
                    $out = ($selisih < 0) ? $selisih * -1 : 0;
                    $this->updateKartuStok(
                        date('Y-m-d'),
                        $opname->nostockopname,
                        $opname->idunitpelayanan,
                        $opname->idtipe,
                        $idbarang[$i],
                        'STOK OPNAME '.$unitpelayanan,
                        $in,
                        $out
                    );
                }
            }

            return true;
        }
        $this->error_message = 'Sunting Gagal';

        return false;
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tstockopname');

        $this->db->where('idstockopname', $id);
        $this->db->delete('tstockopname_detail');
    }

    public function recap_detail($id) {
        $qry = "SELECT t.nama_tipe,SUM(d.hpp*d.stokfisik) as 'total'
        FROM tstockopname_detail d JOIN mdata_tipebarang t ON d.idtipe=t.id
        WHERE d.so_id=".$id.'
        GROUP BY t.id;';

        return $this->db->query($qry)->result();
    }

    public function updateKartuStok($date, $ref, $unit, $idtipe, $idbarang, $ket, $in, $out) {
        $data['tanggal'] = $date;
        $data['notransaksi'] = $ref;
        $data['idunitpelayanan'] = $unit;
        $data['idtipe'] = $idtipe;
        $data['idbarang'] = $idbarang;
        $data['keterangan'] = strtoupper($ket);
        $data['penerimaan'] = $in;
        $data['pengeluaran'] = $out;
        $data['status'] = 1;

        return $this->db->insert('lkartustok', $data);
    }
}
