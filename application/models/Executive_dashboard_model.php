<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Executive_dashboard_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_settings()
    {
        $this->db->where('id', 1);
        $query = $this->db->get('msetting_executive_dashboard');
        return $query->row();
    }

    public function get_settings_gudang($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('msetting_executive_dashboard_gudang');
        return $query->row();
    }
}
