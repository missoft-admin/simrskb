<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Trujukan_radiologi_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tpoliklinik_verifikasi_model');
		$this->load->model('Trawatinap_verifikasi_model');
		$this->load->model('Thonor_dokter_model');
	}

	public function getSpecified($id)
	{
		$this->db->select('trujukan_radiologi.id, trujukan_radiologi.tanggal AS tanggalrujukan,
        trujukan_radiologi.norujukan, trujukan_radiologi.asalrujukan, trujukan_radiologi.nomorfoto, trujukan_radiologi.tanggalpengambilan,
        musers.name AS namauser, trujukan_radiologi.jumlahfilm, trujukan_radiologi.jumlahexpose, trujukan_radiologi.qp, trujukan_radiologi.mas, trujukan_radiologi.idpemeriksa, trujukan_radiologi.tindakan,
        mfpasien.title, mfpasien.no_medrec AS nomedrec, mfpasien.nama AS namapasien, mfpasien.jenis_kelamin AS jeniskelamin,
        mfpasien.alamat_jalan AS alamat, trujukan_radiologi.tanggalexpertise, trujukan_radiologi.tindakan,
        tpoliklinik_pendaftaran.id AS idpendaftaran, tpoliklinik_pendaftaran.id AS idpoliklinik, mpoliklinik.nama AS namapoliklinik,
        provinsi.nama AS prov, kota.nama AS kota, kecamatan.nama AS kec, kelurahan.nama AS kel,
        DATE(mfpasien.tanggal_lahir) AS tanggallahir, mfpasien.umur_tahun, mfpasien.umur_bulan,
        mfpasien.umur_hari, tpoliklinik_pendaftaran.tanggaldaftar, tpoliklinik_pendaftaran.idtipe, tpoliklinik_pendaftaran.nopendaftaran,
        tpoliklinik_pendaftaran.idkelompokpasien, tpoliklinik_pendaftaran.idrekanan, mpasien_kelompok.nama AS namakelompok, mrekanan.nama AS namarekanan,
        trujukan_radiologi.iddokterradiologi, mdokterradiologi.nama AS namadokterradiologi, mdokterradiologi.nip AS nipdokterradiologi,
        trujukan_radiologi.iddokterperujuk, mdokterperujuk.nama AS namadokterperujuk, COALESCE(trawatinap_pendaftaran.idkelas, 0) AS idkelas,
        mkelas.nama AS namakelas, mruangan.nama AS namaruangan, mtarif_bpjstenagakerja.id AS idtarif_bpjstk, mtarif_bpjstenagakerja.nama AS tarif_bpjstk, mtarif_bpjskesehatan.kode AS kodebpjskesehatan, mtarif_bpjskesehatan.nama AS tarif_bpjs_kesehatan,
        mpegawai.nama AS namapetugas, trujukan_radiologi.tanggal_input_pemeriksaan, trujukan_radiologi.iduser_input_pemeriksaan, trujukan_radiologi.statuspasien, tkasir.status AS statuskasir');
		$this->db->join('trawatinap_pendaftaran', 'trujukan_radiologi.idtindakan = trawatinap_pendaftaran.id AND trujukan_radiologi.asalrujukan = 3', 'LEFT');
		$this->db->join('tpoliklinik_tindakan', 'tpoliklinik_tindakan.id = trujukan_radiologi.idtindakan AND trujukan_radiologi.asalrujukan IN (1, 2)', 'LEFT');
		$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran OR tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT');
		$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien');
		$this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
		$this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
		$this->db->join('mdokter mdokterperujuk', 'mdokterperujuk.id = trujukan_radiologi.iddokterperujuk', 'LEFT');
		$this->db->join('mdokter mdokterradiologi', 'mdokterradiologi.id = trujukan_radiologi.iddokterradiologi', 'LEFT');
		$this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
		$this->db->join('mkelas', 'mkelas.id = trawatinap_pendaftaran.idkelas', 'LEFT');
		$this->db->join('mruangan', 'mruangan.id = trawatinap_pendaftaran.idruangan', 'LEFT');
		$this->db->join('mtarif_bpjstenagakerja', 'mtarif_bpjstenagakerja.id = tpoliklinik_pendaftaran.idtarifbpjstenagakerja', 'LEFT');
		$this->db->join('mtarif_bpjskesehatan', 'mtarif_bpjskesehatan.id = tpoliklinik_pendaftaran.idtarifbpjskesehatan', 'LEFT');
		$this->db->join('musers', 'musers.id = trujukan_radiologi.iduser_input_pemeriksaan', 'LEFT');
		$this->db->join('mpegawai', 'mpegawai.id = trujukan_radiologi.idpemeriksa', 'LEFT');

		$this->db->join('mfwilayah provinsi', 'provinsi.id = mfpasien.provinsi_id', 'LEFT');
		$this->db->join('mfwilayah kota', 'kota.id = mfpasien.kabupaten_id', 'LEFT');
		$this->db->join('mfwilayah kecamatan', 'kecamatan.id = mfpasien.kecamatan_id', 'LEFT');
		$this->db->join('mfwilayah kelurahan', 'kelurahan.id = mfpasien.kelurahan_id', 'LEFT');

		$this->db->join('tkasir', 'tkasir.idtipe IN (1,2) AND tkasir.idtindakan = tpoliklinik_tindakan.id', 'LEFT');

		$this->db->where('trujukan_radiologi.id', $id);
		$query = $this->db->get('trujukan_radiologi');
		return $query->row();
	}

	public function getListTarifRadiologi()
	{
		$this->db->select("mtarif_radiologi.id, mtarif_radiologi.level, (CASE WHEN mtarif_radiologi.idtipe = 1 THEN CONCAT(mtarif_radiologi.nama, ' ( ', mtarif_radiologi_expose.nama, ' ', mtarif_radiologi_film.nama, ' )') ELSE mtarif_radiologi.nama END) AS nama");
		$this->db->join('mtarif_radiologi_expose', 'mtarif_radiologi_expose.id = mtarif_radiologi.idexpose', 'LEFT');
		$this->db->join('mtarif_radiologi_film', 'mtarif_radiologi_film.id = mtarif_radiologi.idfilm', 'LEFT');
		$this->db->where('mtarif_radiologi.status', 1);
		$this->db->order_by('mtarif_radiologi.path', 'ASC');
		$query = $this->db->get('mtarif_radiologi');
		return $query->result();
	}

	public function getListDokterPerujuk()
	{
		// $this->db->where("idkategori", 1);
		$this->db->where('status', 1);
		$query = $this->db->get('mdokter');
		return $query->result();
	}

	public function getListDokterRadiologi()
	{
		// $this->db->where("idkategori", 2);
		// $this->db->where("status", 1);
		// $query = $this->db->get("mdokter");
		$iduser = $this->session->userdata('user_id');
		$q = "SELECT M.id,M.nama from musers_dokter_radiologi H
				LEFT JOIN mdokter M ON M.id=H.iddokter
				WHERE H.iduser='$iduser'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function getListFilmRadiologi()
	{
		$this->db->where('status', 1);
		$query = $this->db->get('mtarif_radiologi_film');
		return $query->result();
	}

	public function getListExposeRadiologi()
	{
		$this->db->where('status', 1);
		$query = $this->db->get('mtarif_radiologi_expose');
		return $query->result();
	}

	public function getListPemeriksaRadiologi()
	{
		$this->db->where('status', 1);
		$this->db->order_by('nama', 'ASC');
		$query = $this->db->get('mpegawai');
		return $query->result();
	}

	public function getListTindakan()
	{
		$this->db->select('mtarif_radiologi.*, mtarif_radiologi_detail.*, mtarif_radiologi_expose.nama AS expose, mtarif_radiologi_film.nama AS film');
		$this->db->join('mtarif_radiologi_detail', 'mtarif_radiologi_detail.idtarif = mtarif_radiologi.id', 'LEFT');
		$this->db->join('mtarif_radiologi_expose', 'mtarif_radiologi_expose.id = mtarif_radiologi.idexpose', 'LEFT');
		$this->db->join('mtarif_radiologi_film', 'mtarif_radiologi_film.id = mtarif_radiologi.idfilm', 'LEFT');
		$this->db->where('mtarif_radiologi_detail.kelas', 1);
		$this->db->or_where('mtarif_radiologi_detail.kelas', null);
		$this->db->where('mtarif_radiologi.status', 1);
		$this->db->order_by('mtarif_radiologi.path', 'ASC');
		$query = $this->db->get('mtarif_radiologi');
		return $query->result();
	}

	public function getListDetail($idrujukan)
	{
		$this->db->select("trujukan_radiologi_detail.*, mtarif_radiologi.idtipe,
        trujukan_radiologi_review.tindakanyangdilakukan,
        trujukan_radiologi_review.hasilbacafoto,
        mtarif_radiologi.idtipe,
        (CASE WHEN mtarif_radiologi.idtipe = 1 AND mtarif_radiologi.idkelompok = 0 THEN
          CONCAT(mtarif_radiologi.nama, ' (', mtarif_radiologi_expose.nama, mtarif_radiologi_film.nama, ')')
        ELSE
          mtarif_radiologi.nama
        END) AS namatindakan");
		$this->db->join('mtarif_radiologi', 'mtarif_radiologi.id = trujukan_radiologi_detail.idradiologi');
		$this->db->join('mtarif_radiologi_expose', 'mtarif_radiologi_expose.id = mtarif_radiologi.idexpose', 'LEFT');
		$this->db->join('mtarif_radiologi_film', 'mtarif_radiologi_film.id = mtarif_radiologi.idfilm', 'LEFT');
		$this->db->join('trujukan_radiologi_review', 'trujukan_radiologi_review.idtipe = mtarif_radiologi.idtipe', 'LEFT');
		$this->db->where('trujukan_radiologi_detail.idrujukan', $idrujukan);
		$this->db->group_by('mtarif_radiologi.id');
		$query = $this->db->get('trujukan_radiologi_detail');
		return $query->result();
	}

	public function getListGroupLabel($idrujukan)
	{
		$this->db->select('mtarif_radiologi.idtipe');
		$this->db->join('mtarif_radiologi', 'mtarif_radiologi.id = trujukan_radiologi_detail.idradiologi');
		$this->db->group_by('mtarif_radiologi.idtipe');
		$query = $this->db->get('trujukan_radiologi_detail');
		return $query->result();
	}

	public function getListGroupLabelSelected($idrujukan, $array_in)
	{
		$array = implode("','", $array_in);

		$query = $this->db->query("SELECT
          GROUP_CONCAT( result.pemeriksaan SEPARATOR ',<br>&nbsp;&nbsp;' ) AS pemeriksaan,
          result.idtipe,
          posisi
        FROM
          (
            SELECT
                (CASE
                  WHEN mtarif_radiologi.idtipe = 1 AND mtarif_radiologi.idkelompok = 0 THEN
                      CONCAT(mtarif_radiologi.nama, ' (', mtarif_radiologi_expose.nama, mtarif_radiologi_film.nama, ')')
                  ELSE mtarif_radiologi.nama
                END) AS pemeriksaan,
                mtarif_radiologi.idtipe,
                trujukan_radiologi_review.tindakanyangdilakukan AS posisi
            FROM
                trujukan_radiologi_detail
                JOIN mtarif_radiologi ON mtarif_radiologi.id = trujukan_radiologi_detail.idradiologi
                LEFT JOIN mtarif_radiologi_expose ON mtarif_radiologi_expose.id = mtarif_radiologi.idexpose
                LEFT JOIN mtarif_radiologi_film ON mtarif_radiologi_film.id = mtarif_radiologi.idfilm
                LEFT JOIN trujukan_radiologi_review ON trujukan_radiologi_review.idtipe = mtarif_radiologi.idtipe
            WHERE
                trujukan_radiologi_detail.idrujukan = '" . $idrujukan . "'
                AND trujukan_radiologi_detail.idtipe IN ('" . $array . "')
            GROUP BY
                mtarif_radiologi.id
          ) AS result
        GROUP BY
          result.idtipe");
		return $query->result();
	}

	public function getListDetailGroup($idrujukan)
	{
		$this->db->select("trujukan_radiologi_detail.*, mtarif_radiologi.idtipe,
        trujukan_radiologi_review.tindakanyangdilakukan, trujukan_radiologi_review.hasilbacafoto,
        trujukan_radiologi_review.tanggal_expertise, trujukan_radiologi_review.iduser_expertise,
        (CASE WHEN mtarif_radiologi.idtipe = 1 THEN
          GROUP_CONCAT(mtarif_radiologi.nama, ' (', mtarif_radiologi_expose.nama, mtarif_radiologi_film.nama, ')')
        ELSE
          mtarif_radiologi.nama
        END) AS namatindakan");
		$this->db->join('mtarif_radiologi', 'mtarif_radiologi.id = trujukan_radiologi_detail.idradiologi');
		$this->db->join('mtarif_radiologi_expose', 'mtarif_radiologi_expose.id = mtarif_radiologi.idexpose', 'LEFT');
		$this->db->join('mtarif_radiologi_film', 'mtarif_radiologi_film.id = mtarif_radiologi.idfilm', 'LEFT');
		$this->db->join('trujukan_radiologi_review', 'trujukan_radiologi_review.idrujukan = trujukan_radiologi_detail.idrujukan AND trujukan_radiologi_review.idtipe = trujukan_radiologi_detail.idtipe', 'LEFT');
		$this->db->where('trujukan_radiologi_detail.idrujukan', $idrujukan);
		$this->db->group_by('mtarif_radiologi.idtipe');
		$query = $this->db->get('trujukan_radiologi_detail');
		return $query->result();
	}

	public function save()
	{
		$this->tanggal = YMDFormat($_POST['tanggalrujukan']) . ' ' . $_POST['wakturujukan'];
		$this->iddokterperujuk = $_POST['iddokterperujuk'];
		$this->iddokterradiologi = $_POST['iddokterradiologi'];
		$this->tanggalpengambilan = $_POST['tanggalpengambilan'];
		$this->nomorfoto = $_POST['nomorfoto'];
		$this->jumlahexpose = $_POST['jumlahexpose'];
		$this->jumlahfilm = $_POST['jumlahfilm'];
		$this->qp = $_POST['qp'];
		$this->mas = $_POST['mas'];
		$this->idpemeriksa = $_POST['idpemeriksa'];
		$this->tindakan = $_POST['tindakan'];
		$this->status = '2';

		if ($_POST['tanggal_input_pemeriksaan'] != '') {
			// Log Update
			$this->iduser_input_pemeriksaan = $_POST['iduser_input_pemeriksaan'];
			$this->tanggal_input_pemeriksaan = $_POST['tanggal_input_pemeriksaan'];

			$this->iduser_edit_pemeriksaan = $this->session->userdata('user_id');
			$this->tanggal_edit_pemeriksaan = date('Y-m-d H:i:s');
		} else {
			// Log Insert
			$this->iduser_input_pemeriksaan = $this->session->userdata('user_id');
			$this->tanggal_input_pemeriksaan = date('Y-m-d H:i:s');
		}

		if ($this->db->update('trujukan_radiologi', $this, ['id' => $_POST['id']])) {
			$this->db->where('idrujukan', $_POST['id']);
			if ($this->db->delete('trujukan_radiologi_detail')) {
				$queryDokter = $this->db->query("SELECT
                  mdokter.id,
                  (CASE
                    WHEN DATE_FORMAT(trujukan_radiologi.tanggal_input_pemeriksaan, '%H:%i') < '12:00' THEN
                      mdokter.potonganrspagi
                    ELSE mdokter.potonganrssiang
                  END) AS potongan_rs,
                  mdokter.pajak AS pajak_dokter
                FROM trujukan_radiologi
                JOIN mdokter ON mdokter.id = trujukan_radiologi.iddokterradiologi
                WHERE trujukan_radiologi.id = " . $_POST['id']);
				$dataDokter = $queryDokter->row();

				$tindakan_list = json_decode($_POST['tindakan-value']);
				foreach ($tindakan_list as $row) {
					$tindakan = [];
					$tindakan['idrujukan'] = $_POST['id'];
					$tindakan['idtipe'] = $row[11];
					$tindakan['idradiologi'] = $row[10];
					$tindakan['namatarif'] = $row[0];
					$tindakan['jasasarana'] = RemoveComma($row[1]);
					$tindakan['jasapelayanan'] = RemoveComma($row[2]);
					$tindakan['bhp'] = RemoveComma($row[3]);
					$tindakan['biayaperawatan'] = RemoveComma($row[4]);
					$tindakan['total'] = RemoveComma($row[5]);
					$tindakan['kuantitas'] = RemoveComma($row[6]);
					$tindakan['diskon'] = RemoveComma($row[8]);
					$tindakan['totalkeseluruhan'] = RemoveComma($row[9]);
					$tindakan['statusverifikasi'] = $row[12];
					$tindakan['status'] = 1;

					// Nominal Komponen Honor Dokter
					$tindakan['iddokter'] = $_POST['iddokterradiologi'];
					$tindakan['potongan_rs'] = $dataDokter->potongan_rs;
					$tindakan['pajak_dokter'] = $dataDokter->pajak_dokter;

					if ($this->db->insert('trujukan_radiologi_detail', $tindakan)) {
						$this->updateNominalJasaMedis($_POST['id'], $_POST['statuspasien'], $_POST['idkelas']);
					}
				}
			}
		}

		$this->db->update(
			'tpoliklinik_pendaftaran',
			['statustindakan' => '1'],
			['id' => $_POST['idpendaftaran']]
		);

		return true;
	}

	public function saveReview()
	{
		$this->iddokterradiologi = $_POST['iddokterradiologi'];
		$this->statuspasien = $_POST['statuspasien'];
		$this->tanggalexpertise = YMDFormat($_POST['tanggalexpertise']) . ' ' . $_POST['waktuexpertise'];

		if ($_POST['statuspasien'] == 3) {
			$this->tanggalkembali = date('Y-m-d H:i:s');
		}

		if ($this->db->update('trujukan_radiologi', $this, ['id' => $_POST['id']])) {
			$tipe = $_POST['idtipe'];
			$tindakanyangdilakukan = $_POST['tindakanyangdilakukan'];
			$hasilbacafoto = $_POST['hasilbacafoto'];
			$tanggal_expertise = $_POST['tanggal_expertise'];
			$iduser_expertise = $_POST['iduser_expertise'];

			$this->db->where('idrujukan', $_POST['id']);
			if ($this->db->delete('trujukan_radiologi_review')) {
				foreach ($tipe as $index => $idtipe) {
					$review = [];
					$review['idrujukan'] = $_POST['id'];
					$review['idtipe'] = $idtipe;
					$review['tindakanyangdilakukan'] = $tindakanyangdilakukan[$index];
					$review['hasilbacafoto'] = $hasilbacafoto[$index];
					$review['status'] = 1;

					if ($tanggal_expertise[$index] != '') {
						// Log Update Expertise
						$review['tanggal_ubah_expertise'] = date('Y-m-d H:i:s');
						$review['iduser_ubah_expertise'] = $this->session->userdata('user_id');

						$review['tanggal_expertise'] = $tanggal_expertise[$index];
						$review['iduser_expertise'] = $iduser_expertise[$index];
					} else {
						// Log Insert Expertise
						$review['tanggal_expertise'] = date('Y-m-d H:i:s');
						$review['iduser_expertise'] = $this->session->userdata('user_id');
					}

					$this->db->insert('trujukan_radiologi_review', $review);
				}
			}

			// Update Nominal Jasa Medis By Expertise
			$this->updateNominalJasaMedis($_POST['id'], $_POST['statuspasien'], $_POST['idkelas']);
		}
		return true;
	}

	public function cancelTrx($id)
	{
		// Status Batal Transaksi Radiologi
		$this->db->set('iduser_batal', $this->session->userdata('user_id'));
		$this->db->set('tanggal_batal', date('Y-m-d H:i:s'));
		$this->db->set('status', 0);
		$this->db->where('id', $id);

		if ($this->db->update('trujukan_radiologi')) {
			return true;
		} else {
			return false;
		}
	}

	public function rejectTrx($id, $statuspasien, $idkelas)
	{
		// Status Expertise [Menolak]
		$this->db->set('statuspasien', $statuspasien);
		$this->db->where('id', $id);

		if ($this->db->update('trujukan_radiologi')) {
			$this->updateNominalJasaMedis($id, $statuspasien, $idkelas);
			return true;
		} else {
			return false;
		}
	}

	public function updateNominalJasaMedis($idrujukan, $statuspasien, $idkelas)
	{
		$queryDokter = $this->db->query("SELECT
          mdokter.id AS iddokter,
          (CASE
            WHEN DATE_FORMAT(trujukan_radiologi.tanggal_input_pemeriksaan, '%H:%i') < '12:00' THEN
              mdokter.potonganrspagi
            ELSE mdokter.potonganrssiang
          END) AS potongan_rs,
          mdokter.pajak AS pajak_dokter
        FROM trujukan_radiologi
        JOIN mdokter ON mdokter.id = trujukan_radiologi.iddokterradiologi
        WHERE trujukan_radiologi.id = " . $idrujukan);
		$dataDokter = $queryDokter->row();

		$queryNominalJasaMedis = $this->db->query("SELECT
        	trujukan_radiologi_detail.id,
        	trujukan_radiologi_detail.idradiologi,
        	trujukan_radiologi.statuspasien,
        	(CASE
        		WHEN trujukan_radiologi.statuspasien = 1 THEN
        			mtarif_radiologi_detail.nominallangsung
        		WHEN trujukan_radiologi.statuspasien = 2 THEN
        			mtarif_radiologi_detail.nominalmenolak
        		WHEN trujukan_radiologi.statuspasien = 3 THEN
        			mtarif_radiologi_detail.nominalkembali
        		END) AS nominal_jasamedis
        FROM
        	trujukan_radiologi_detail
        JOIN trujukan_radiologi ON trujukan_radiologi.id = trujukan_radiologi_detail.idrujukan
        JOIN mtarif_radiologi ON mtarif_radiologi.id = trujukan_radiologi_detail.idradiologi
        JOIN mtarif_radiologi_detail ON mtarif_radiologi_detail.idtarif = mtarif_radiologi.id
        WHERE
        	trujukan_radiologi.id = $idrujukan AND
        	mtarif_radiologi_detail.kelas = $idkelas");

		$queryStatusKasir = $this->db->query("SELECT
        	(CASE
        		WHEN trujukan_radiologi.asalrujukan IN (1, 2) THEN
        			tpoliklinik_tindakan.idpendaftaran
        		ELSE
        			trawatinap_tindakan_pembayaran.idtindakan
        	END) AS idpendaftaran,
        	(CASE
        		WHEN trujukan_radiologi.asalrujukan IN (1, 2) THEN
        			tkasir.id
        		ELSE
        			trawatinap_tindakan_pembayaran.id
        	END) AS idkasir,
        	(CASE
        		WHEN trujukan_radiologi.asalrujukan IN (1, 2) THEN
        			'poliklinik'
        		ELSE
        			'rawatinap'
        	END) AS tipe_pendaftaran,
        	(CASE
        		WHEN trujukan_radiologi.asalrujukan IN (1, 2) THEN
        			COALESCE(tkasir.status_verifikasi, 0)
        		ELSE
        			COALESCE(trawatinap_tindakan_pembayaran.status_verifikasi, 0)
        	END) AS status_verifikasi
        FROM
        	trujukan_radiologi
        LEFT JOIN trawatinap_tindakan_pembayaran ON trawatinap_tindakan_pembayaran.idtindakan = trujukan_radiologi.idtindakan AND trujukan_radiologi.asalrujukan = 3
        LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = trujukan_radiologi.idtindakan AND trujukan_radiologi.asalrujukan IN (1, 2)
        LEFT JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN (1, 2)
        WHERE trujukan_radiologi.id = $idrujukan");

		// Update Nominal Jasa Medis By Expertise
		foreach ($queryNominalJasaMedis->result() as $row) {
			$this->db->set('iddokter', $dataDokter->iddokter);
			$this->db->set('potongan_rs', $dataDokter->potongan_rs);
			$this->db->set('pajak_dokter', $dataDokter->pajak_dokter);

			$this->db->set('status_tindakan', $statuspasien);
			$this->db->set('nominal_jasamedis', $row->nominal_jasamedis);
			$this->db->where('id', $row->id);

			if ($this->db->update('trujukan_radiologi_detail')) {
				// Reset Honor Dokter
				$this->db->where('reference_table', 'trujukan_radiologi_detail');
				$this->db->where('iddetail', $row->id);
				$this->db->delete('thonor_dokter_detail');
			}
		}

		// Check Status Kasir
		$rowStatus = $queryStatusKasir->row();
		if ($rowStatus) {
		    $idPendaftaran = $rowStatus->idpendaftaran;
		    $idKasir = $rowStatus->idkasir;
		    $tipePendaftaran = $rowStatus->tipe_pendaftaran;
		    $statusVerifikasiKasir = $rowStatus->status_verifikasi;
		
		    if ($statusVerifikasiKasir) {
		        // Create Honor Dokter
		        $this->createHonorDokter($tipePendaftaran, $idPendaftaran, $idKasir);
		    }
		}

		return true;
	}

	public function createHonorDokter($tipe_pendaftaran, $idpendaftaran, $idKasir)
	{
		if ($tipe_pendaftaran == 'poliklinik') {
			// Get Detail Info Poliklinik
			$this->db->select("tpoliklinik_pendaftaran.id AS idpendaftaran,
            tpoliklinik_tindakan.id AS idtindakan, tkasir.id AS idkasir,
            IF(tpoliklinik_pendaftaran.idtipepasien = 1, 'Pasien RS', 'Pasien Pribadi') AS jenispasien,
            tpoliklinik_pendaftaran.idkelompokpasien AS idkelompok,
            mpasien_kelompok.nama AS namakelompok,
            tpoliklinik_pendaftaran.idrekanan,
            mrekanan.nama AS namarekanan,
            tpoliklinik_pendaftaran.tanggaldaftar AS tanggal_pemeriksaan");
			$this->db->join('tpoliklinik_tindakan', 'tpoliklinik_tindakan.id = tkasir.idtindakan AND idtipe IN (1,2)');
			$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran');
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
			$this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
			$this->db->where('tkasir.id', $idKasir);
			$this->db->where('tkasir.status', 2);
			$this->db->limit(1);
			$query = $this->db->get('tkasir');
			$dataPoliklinik = $query->row();

			$tanggal_pemeriksaan = $dataPoliklinik->tanggal_pemeriksaan;
			$idpendaftaran = $dataPoliklinik->idpendaftaran;
			$jenis_pasien = $dataPoliklinik->jenispasien;
			$idkelompok = $dataPoliklinik->idkelompok;
			$namakelompok = $dataPoliklinik->namakelompok;
			$idrekanan = $dataPoliklinik->idrekanan;
			$namarekanan = $dataPoliklinik->namarekanan;
			$status_asuransi = ($dataPoliklinik->idkelompok != 5 ? 1 : 0);

			// JASA DOKTER RADIOLOGI [X-RAY] - RAWAT JALAN
			foreach ($this->Tpoliklinik_verifikasi_model->viewRincianRadiologi($idpendaftaran) as $index => $row) {
				if ($row->idtipe == 1 && $row->jasamedis > 0) {
					$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
					$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
					$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

					$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
					$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

					$this->db->set('periode_pembayaran', $tanggal_pembayaran);
					$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
					$this->db->where('id', $row->iddetail);
					$this->db->update('trujukan_radiologi_detail');

					$isStopPeriode = $this->Thonor_dokter_model->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
					if ($isStopPeriode == null) {
						$idhonor = $this->Thonor_dokter_model->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

						if ($idhonor == null) {
							$dataHonorDokter = [
								'iddokter' => $row->iddokter,
								'namadokter' => $row->namadokter,
								'tanggal_pembayaran' => $tanggal_pembayaran,
								'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
								'nominal' => 0,
								'created_at' => date('Y-m-d H:i:s'),
								'created_by' => $this->session->userdata('user_id')
							];

							if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
								$idhonor = $this->db->insert_id();
							}
						}
					}

					$dataDetailHonorDokter = [
						'idhonor' => $idhonor,
						'idtransaksi' => $idpendaftaran,
						'jenis_transaksi' => 'poliklinik',
						'jenis_tindakan' => 'JASA DOKTER RADIOLOGI',
						'reference_table' => 'trujukan_radiologi_detail',
						'jenis_pasien' => $jenis_pasien,
						'idkelompok' => $idkelompok,
						'namakelompok' => $namakelompok,
						'idrekanan' => $idrekanan,
						'namarekanan' => $namarekanan,
						'iddetail' => $row->iddetail,
						'idtarif' => $row->idtarif,
						'namatarif' => $row->namatarif,
						'idkategori' => $row->idkategori,
						'namakategori' => $row->namakategori,
						'iddokter' => $row->iddokter,
						'namadokter' => $row->namadokter,
						'jasamedis' => $row->jasamedis,
						'potongan_rs' => $row->potongan_rs,
						'nominal_potongan_rs' => $nominal_potongan_rs,
						'pajak_dokter' => $row->pajak_dokter,
						'nominal_pajak_dokter' => $nominal_pajak_dokter,
						'jasamedis_netto' => $jasamedis_netto,
						'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
						'tanggal_pembayaran' => $tanggal_pembayaran,
						'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
						'status_expertise' => $row->status_expertise,
						'status_asuransi' => $status_asuransi
					];

					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}
		} elseif ($tipe_pendaftaran == 'rawatinap') {
			// Get Detail Info Rawat Inap
			$this->db->select("trawatinap_pendaftaran.id AS idpendaftaran,
            IF(trawatinap_pendaftaran.idtipepasien = 1, 'Pasien RS', 'Pasien Pribadi') AS jenispasien,
            trawatinap_pendaftaran.idkelompokpasien AS idkelompok,
            mpasien_kelompok.nama AS namakelompok,
            trawatinap_pendaftaran.idrekanan,
            mrekanan.nama AS namarekanan,
            trawatinap_pendaftaran.tanggaldaftar AS tanggal_pemeriksaan");
			$this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = trawatinap_tindakan_pembayaran.idtindakan');
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien', 'LEFT');
			$this->db->join('mrekanan', 'mrekanan.id = trawatinap_pendaftaran.idrekanan', 'LEFT');
			$this->db->where('trawatinap_tindakan_pembayaran.id', $idKasir);
			$this->db->where('trawatinap_tindakan_pembayaran.statusbatal', 0);
			$this->db->limit(1);
			$query = $this->db->get('trawatinap_tindakan_pembayaran');
			$dataRanap = $query->row();

			$tanggal_pemeriksaan = $dataRanap->tanggal_pemeriksaan;
			$idpendaftaran = $dataRanap->idpendaftaran;
			$jenis_pasien = $dataRanap->jenispasien;
			$idkelompok = $dataRanap->idkelompok;
			$namakelompok = $dataRanap->namakelompok;
			$idrekanan = $dataRanap->idrekanan;
			$namarekanan = $dataRanap->namarekanan;
			$status_asuransi = ($dataRanap->idkelompok != 5 ? 1 : 0);

			// JASA DOKTER RADIOLOGI [X-RAY] - RAWAT INAP
			foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 1) as $row) {
				if ($row->jasamedis > 0) {
					$nominal_potongan_rs = $row->jasamedis * ($row->potongan_rs / 100);
					$nominal_pajak_dokter = $row->jasamedis * ($row->pajak_dokter / 100);
					$jasamedis_netto = $row->jasamedis - $nominal_potongan_rs - $nominal_pajak_dokter;

					$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : YMDFormat(getPembayaranHonorDokter($row->iddokter)));
					$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : YMDFormat(getJatuhTempoHonorDokter($row->iddokter)));

					$this->db->set('periode_pembayaran', $tanggal_pembayaran);
					$this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
					$this->db->where('id', $row->iddetail);
					$this->db->update('trujukan_radiologi_detail');

					$isStopPeriode = $this->Thonor_dokter_model->checkStatusHonorDokterStopped($row->iddokter, $tanggal_pembayaran);
					if ($isStopPeriode == null) {
						$idhonor = $this->Thonor_dokter_model->checkStatusHonorDokter($row->iddokter, $tanggal_pembayaran);

						if ($idhonor == null) {
							$dataHonorDokter = [
								'iddokter' => $row->iddokter,
								'namadokter' => $row->namadokter,
								'tanggal_pembayaran' => $tanggal_pembayaran,
								'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
								'nominal' => 0,
								'created_at' => date('Y-m-d H:i:s'),
								'created_by' => $this->session->userdata('user_id')
							];

							if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
								$idhonor = $this->db->insert_id();
							}
						}
					}

					$dataDetailHonorDokter = [
						'idhonor' => $idhonor,
						'idtransaksi' => $idpendaftaran,
						'jenis_transaksi' => 'rawatinap',
						'jenis_tindakan' => 'JASA DOKTER RADIOLOGI (X-RAY)',
						'reference_table' => 'trujukan_radiologi_detail',
						'jenis_pasien' => $jenis_pasien,
						'idkelompok' => $idkelompok,
						'namakelompok' => $namakelompok,
						'idrekanan' => $idrekanan,
						'namarekanan' => $namarekanan,
						'iddetail' => $row->iddetail,
						'idtarif' => $row->idradiologi,
						'namatarif' => $row->namatarif,
						'idkategori' => $row->idkategori,
						'namakategori' => $row->namakategori,
						'iddokter' => $row->iddokter,
						'namadokter' => $row->namadokter,
						'jasamedis' => $row->jasamedis,
						'potongan_rs' => $row->potongan_rs,
						'nominal_potongan_rs' => $nominal_potongan_rs,
						'pajak_dokter' => $row->pajak_dokter,
						'nominal_pajak_dokter' => $nominal_pajak_dokter,
						'jasamedis_netto' => $jasamedis_netto,
						'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
						'tanggal_pembayaran' => $tanggal_pembayaran,
						'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
						'status_expertise' => $row->status_expertise,
						'status_asuransi' => $status_asuransi
					];

					$this->db->replace('thonor_dokter_detail', $dataDetailHonorDokter);
				}
			}
		}
	}

	public function getHeadParentTarifRadiologi($idtipe, $idkelompokpasien, $idrekanan)
	{
		$this->db->select('mtarif_radiologi.path, mtarif_radiologi.level, mtarif_radiologi.nama');
		if ($idkelompokpasien == 1) {
			$this->db->select('mtarif_radiologi.path');
			$this->db->join('mrekanan', 'mrekanan.tradiologi_xray = mtarif_radiologi.id');
			$this->db->where('mrekanan.id', $idrekanan);
			$query = $this->db->get('mtarif_radiologi');

			if ($query->num_rows() > 0) {
				if ($idtipe == 1) {
					$this->db->join('mrekanan', 'mrekanan.tradiologi_xray = mtarif_radiologi.id');
				} elseif ($idtipe == 2) {
					$this->db->join('mrekanan', 'mrekanan.tradiologi_usg = mtarif_radiologi.id');
				} elseif ($idtipe == 3) {
					$this->db->join('mrekanan', 'mrekanan.tradiologi_ctscan = mtarif_radiologi.id');
				} elseif ($idtipe == 4) {
					$this->db->join('mrekanan', 'mrekanan.tradiologi_mri = mtarif_radiologi.id');				
				} elseif ($idtipe == 5) {
					$this->db->join('mrekanan', 'mrekanan.tradiologi_bmd = mtarif_radiologi.id');
				}
				$this->db->where('mrekanan.id', $idrekanan);
			} else {
				if ($idtipe == 1) {
					$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_xray = mtarif_radiologi.id');
				} elseif ($idtipe == 2) {
					$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_usg = mtarif_radiologi.id');
				} elseif ($idtipe == 3) {
					$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_ctscan = mtarif_radiologi.id');
				} elseif ($idtipe == 4) {
					$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_mri = mtarif_radiologi.id');				
				} elseif ($idtipe == 5) {
					$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_bmd = mtarif_radiologi.id');
				}
				$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
			}
		} else {
			if ($idtipe == 1) {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_xray = mtarif_radiologi.id');
			} elseif ($idtipe == 2) {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_usg = mtarif_radiologi.id');
			} elseif ($idtipe == 3) {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_ctscan = mtarif_radiologi.id');
			} elseif ($idtipe == 4) {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_mri = mtarif_radiologi.id');				
			} elseif ($idtipe == 5) {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tradiologi_bmd = mtarif_radiologi.id');
			}
			$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
		}

		$this->db->where('mtarif_radiologi.idkelompok', '1');
		$this->db->where('mtarif_radiologi.status', '1');
		$this->db->order_by('mtarif_radiologi.path', 'ASC');
		$query = $this->db->get('mtarif_radiologi');
		return $query->result();
	}
}
