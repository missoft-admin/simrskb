<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tgudang_pemesanan_model extends CI_Model
{
   public function list_pesanan_draft($id){
	   $q="SELECT D.id,D.idpemesanan,D.idtipe,D.idbarang,B.namatipe,B.kode,B.nama as namabarang,B.jumlahsatuanbesar,
		D.kuantitas_kecil,CEIL(D.kuantitas_kecil/B.jumlahsatuanbesar) as kuantitas_besar,(D.kuantitas_kecil * B.hargabeli) as harga
		,sk.nama as satuan_kecil,sb.nama as satuan_besar,D.opsisatuan
		from tgudang_pemesanan_detail D 
		LEFT JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe
		LEFT JOIN msatuan sk ON sk.id=B.idsatuan
		LEFT JOIN msatuan sb on sb.id=B.idsatuanbesar
		WHERE D.idpemesanan='$id' AND D.status='1'";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getBarang_cari() {
        $idtipe             = $this->input->post('idtipe');
		 if ($idtipe) {			
			$q="(SELECT B.id,B.idtipe,B.kode,B.namatipe,B.nama,CASE WHEN B.idtipe='4' THEN SL.stok ELSE SO.stok END as stok,B.hargabeli from view_barang B
					LEFT JOIN mgudang_stok SO ON SO.idbarang=B.id AND SO.idtipe=B.idtipe AND SO.idunitpelayanan='0'
					LEFT JOIN mgudang_stok SL ON SL.idbarang=B.id AND SL.idtipe=B.idtipe AND SL.idunitpelayanan='49'
					WHERE B.idtipe IN ($idtipe))T ";
			$this->load->library('datatables'); 
			$this->datatables->select('id,nama,namatipe,kode,stok,hargabeli'); 
			$this->datatables->from($q); 
			$this->datatables->add_column("Actions", "<center></center>", "id");
			$this->datatables->unset_column('id');
			$this->datatables->unset_column('idtipe');
			// echo $this->datatables->generate();
			
			 return print($this->datatables->generate());
			// $query=$this->db->query($q);
			// $res=$query->result();
			// $this->output->set_output(json_encode($res));
		 }
    }
	public function selectbarang_array_tipe() {
        $idtipe             = $this->input->post('idtipe');
		$idtipe=implode(",", $idtipe);
		// print_r($idtipe);exit();
        $idunitpelayanan    = $this->input->post('idunitpelayanan');
        $idunitke    = $this->input->post('idunitke');
		$term = $this->input->post('search');
       
			$q="SELECT B.idtipe,B.namatipe, B.kode,B.id,B.nama,S.stok from view_barang B
			LEFT JOIN mgudang_stok S ON S.idtipe=B.idtipe AND B.id=S.idbarang AND S.idunitpelayanan='$idunitpelayanan'
			WHERE  B.idtipe IN ($idtipe)  AND  (B.nama LIKE '%".$term."%' OR B.kode LIKE '%".$term."%' )
			LIMIT 100";
			// print_r($q);exit();
			$res=$this->db->query($q)->result_array();	
            $this->output->set_output(json_encode($res));
    }
	public function selected_barang_harga() {
        $idtipe             = $this->input->post('idtipe');
        $idbarang    = $this->input->post('idbarang');
		 if ($idtipe) {
			
			$q="SELECT B.id,B.nama,B.kode,COALESCE(B.hargabeli,0) as hargabeli,
				COALESCE(B.hargabeli_besar,0) as hargabeli_besar,B.jumlahsatuanbesar,sk.nama as satuan_kecil,sb.nama as satuan_besar 
				FROM view_barang B
				LEFT JOIN msatuan sk ON sk.id=B.idsatuan
				LEFT JOIN msatuan sb ON sb.id=B.idsatuanbesar
				WHERE  B.idtipe='$idtipe' AND B.id='$idbarang'";
			// print_r($q);exit();
			$query=$this->db->query($q);
			$res=$query->row();
			// $res=($res!=null)?$res->stok:0;
			$this->output->set_output(json_encode($res));
		 }
    }
   public function list_tipe(){
	   $id_user=$this->session->userdata('user_id');
	   $q="SELECT musers_tipebarang.idtipe,T.nama_tipe FROM musers_tipebarang
		LEFT JOIN mdata_tipebarang T ON T.id=musers_tipebarang.idtipe
		WHERE iduser='$id_user'";
		// print_r($q);exit();
		return $this->db->query($q)->result();
   }
   public function get_list_tipe($id_unit){
	   $id_user=$this->session->userdata('user_id');
	   $q="SELECT *FROM (
			SELECT musers_tipebarang.idtipe,T.nama_tipe FROM musers_tipebarang
			LEFT JOIN mdata_tipebarang T ON T.id=musers_tipebarang.idtipe
			WHERE iduser='$id_user'

			UNION ALL

			SELECT U.idtipe,T.nama_tipe from munitpelayanan_tipebarang U 
			LEFT JOIN mdata_tipebarang T ON T.id=U.idtipe
			WHERE U.idunitpelayanan='$id_unit'
			) TBL 

			GROUP BY TBL.idtipe
			HAVING COUNT(TBL.idtipe) > 1";
		// print_r($q);exit();
		return $this->db->query($q)->result();
   }
   public function list_distributor(){
	   $q="SELECT *FROM mdistributor
			WHERE `status`='1'
			ORDER BY nama";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function get_user_tipe($id){
	   $q="SELECT GROUP_CONCAT(H.gudangtipe) as user from musers_tipegudang H where H.iduser='$id'";
		$query=$this->db->query($q);
		
		return $query->row('user');
   }
   public function data_header($id){
	   $q="SELECT H.*,UE.`name` as nama_user_edit,UD.`name` as nama_user_hapus from tgudang_pemesanan H
			LEFT JOIN musers UE ON UE.id=H.edit_user
			LEFT JOIN musers UD on UD.id=H.hapus_user
			WHERE H.id='$id'";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function list_pesanan_edit($id){
	   $q="SELECT D.id,D.idpemesanan,D.idtipe,D.idbarang,B.namatipe,B.kode,B.nama as namabarang,B.jumlahsatuanbesar,
		D.kuantitas_kecil,CEIL(D.kuantitas_kecil/B.jumlahsatuanbesar) as kuantitas_besar,(D.kuantitas_kecil * B.hargabeli) as harga
		,sk.nama as satuan_kecil,sb.nama as satuan_besar,D.opsisatuan,B.hargabeli,B.hargabeli_besar 
		from tgudang_pemesanan_detail D 
		LEFT JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe
		LEFT JOIN msatuan sk ON sk.id=B.idsatuan
		LEFT JOIN msatuan sb on sb.id=B.idsatuanbesar
		WHERE D.idpemesanan='$id' AND D.status = '1'";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_pesanan_edit_penerimaan($id){
	   $q="SELECT D.id,D.idpemesanan,D.idtipe,D.idbarang,B.namatipe,B.kode,B.nama as namabarang,B.jumlahsatuanbesar,
		D.kuantitas_kecil,CEIL(D.kuantitas_kecil/B.jumlahsatuanbesar) as kuantitas_besar,(D.kuantitas_kecil * B.hargabeli) as harga
		,sk.nama as satuan_kecil,sb.nama as satuan_besar,D.opsisatuan,B.hargabeli,B.hargabeli_besar 
		,COALESCE((SELECT SUM(P.kuantitas) FROM tgudang_penerimaan_detail P WHERE P.idpemesanandetail=D.id AND P.`status`='1'),0) as sudah_terima
		from tgudang_pemesanan_detail D 
		LEFT JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe
		LEFT JOIN msatuan sk ON sk.id=B.idsatuan
		LEFT JOIN msatuan sb on sb.id=B.idsatuanbesar
		WHERE D.idpemesanan='$id' AND D.status = '1'";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_pesanan_edit_po($id){
	   $q="SELECT *FROM (SELECT D.id,D.idpemesanan,D.idtipe,D.idbarang,B.namatipe,B.kode,B.nama as namabarang,B.jumlahsatuanbesar,D.kuantitas,
		D.kuantitas_kecil,CEIL(D.kuantitas_kecil/B.jumlahsatuanbesar) as kuantitas_besar,(D.kuantitas_kecil * B.hargabeli) as harga
		,sk.nama as satuan_kecil,sb.nama as satuan_besar,D.opsisatuan,B.hargabeli,B.hargabeli_besar 
		,COALESCE((SELECT SUM(P.kuantitas) FROM tgudang_penerimaan_detail P WHERE P.idpemesanandetail=D.id AND P.`status`='1'),0) as sudah_terima
		,D.kuantitas - (SELECT sudah_terima) as sisa
		from tgudang_pemesanan_detail D 
		LEFT JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe
		LEFT JOIN msatuan sk ON sk.id=B.idsatuan
		LEFT JOIN msatuan sb on sb.id=B.idsatuanbesar
		WHERE D.idpemesanan='$id' AND D.status = '1') T WHERE T.sisa > 0";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function selectbarang_all() {
        $idtipe             = $this->input->post('idtipe');
        $idunitpelayanan    = $this->input->post('idunitpelayanan');
        $term = $this->input->post('search');
		$panjang=strlen($term);
		 if ($idtipe) {
			
			$q="SELECT B.id,B.nama
				FROM view_barang B				
				WHERE  B.idtipe='$idtipe' AND B.nama like '%".$term."%' LIMIT 100";
			$query=$this->db->query($q);
			$res=$query->result();
			$this->output->set_output(json_encode($res));
		 }
    }
	public function selectbarang() {
        $idtipe             = $this->input->post('idtipe');
        $term = $this->input->post('search');
		$idtipe=implode(",", $idtipe);
		// print_r($idtipe);exit();
		$panjang=strlen($term);
		if ($idtipe) {			
			$q="SELECT B.idtipe,B.nama, B.id,B.namatipe from view_barang B
				WHERE B.idtipe IN (".$idtipe.") AND (B.nama LIKE '%".$term."%' OR B.kode LIKE '%".$term."%') 
				LIMIT 100";
			$query=$this->db->query($q);
			$res=$query->result();
			$this->output->set_output(json_encode($res));
		}
    }
	public function get_harga(){
        
        $idbarang = $this->input->post('idbarang');
        $idtipe = $this->input->post('idtipe');
		
		$q="SELECT B.hargabeli as harga_kecil,B.hargabeli_besar as harga_besar,B.jumlahsatuanbesar  from view_barang B
		WHERE B.id='$idbarang' AND B.idtipe='$idtipe'";
		$arr=$this->db->query($q)->row();
		// print_r($arr);exit();
		$this->output->set_output(json_encode($arr));
	}
   public function get_satuan_barang()
    {
        $idtipe = $this->input->post('idtipe');
        $idbarang = $this->input->post('idbarang');
        $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        $this->db->from($table[$idtipe]);
        $this->db->where('id', $idbarang);
        $row = $this->db->get()->row();
        $res = array();
        if ($row->idsatuanbesar == $row->idsatuankecil) {
            $res[]['id'] = $row->idsatuanbesar;
            $res[]['text'] = $this->get_satuan_detail($row->idsatuanbesar)->nama;
        } else {
            $res[]['id'] = $row->idsatuanbesar;
            $res[]['text'] = $this->get_satuan_detail($row->idsatuanbesar)->nama;
            $res[]['id'] = $row->idsatuankecil;
            $res[]['text'] = $this->get_satuan_detail($row->idsatuankecil)->nama;
        }
        $this->output->set_output(json_encode($res));
    }

    public function get_satuan_detail($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('msatuan', 1)->row();
    }
	public function getsorted_distributor_2(){
		
        $idbarang = $this->input->post('idbarang');
        $idtipe = $this->input->post('idtipe');
		$term = $this->input->post('search');
		
		$q="SELECT T.id,CONCAT(T.nama,' (',T.jml,' Kali)') as text 
				FROM (
					SELECT D.id,D.nama,(SELECT COUNT(P.id)  From tgudang_pemesanan P 
					LEFT JOIN tgudang_pemesanan_detail Det ON Det.idpemesanan=P.id 
					Where P.iddistributor=D.id AND Det.idtipe='$idtipe' AND Det.idbarang='$idbarang') as jml 
					FROM mdistributor D
					WHERE D.`status`='1'  AND D.nama LIKE '%".$term."%' ) T 
					
					ORDER BY T.jml DESC,T.nama";
			// print
		$query=$this->db->query($q);
		$res=$query->result();
        $this->output->set_output(json_encode($res));
	}
    public function cek_distibutor(){
		
        $idbarang = $this->input->post('idbarang');
        $idtipe = $this->input->post('idtipe');
		$iddistributor = $this->input->post('iddistributor');
		
		$q="SELECT T.id,CONCAT(T.nama,' (',T.jml,' Kali)') as text 
				FROM (
					SELECT D.id,D.nama,(SELECT COUNT(P.id)  From tgudang_pemesanan P 
					LEFT JOIN tgudang_pemesanan_detail Det ON Det.idpemesanan=P.id 
					Where P.iddistributor=D.id AND Det.idtipe='$idtipe' AND Det.idbarang='$idbarang') as jml 
					FROM mdistributor D
					WHERE D.`status`='1' AND D.id='$iddistributor' ) T ";
			// print
		// print_r($q);exit();
		$query=$this->db->query($q);
		$res=$query->row();
        $this->output->set_output(json_encode($res));
	}
    public function get_detailbarang($idtipe, $idbarang)
    {
        $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        $this->db->from($table[$idtipe]);
        $this->db->where('id', $idbarang);
        return $this->db->get()->row();
    }

    public function update()
    {
        $detailValue    = $this->input->post('detailValue');
        $idPemesanan    = $this->input->post('idpemesanan');
        if (count($detailValue) > 0) {
            $this->db->where('idpemesanan', $idPemesanan);
            $delDet = $this->db->delete('tgudang_pemesanan_detail');
            if ($delDet) {
                $this->db->set('totalbarang', 0);
                $this->db->set('totalharga', 0);
                $this->db->where('id', $idPemesanan);
                $updateHead0 = $this->db->update('tgudang_pemesanan');
                if ($updateHead0) {
                    foreach ($detailValue as $r) {
                        $tipegudang = $r[6];
                        if ($tipegudang == 4) {
                            // gudang logistik

                            $distributor = $r[7];
                            $this->db->where('iddistributor', $r[7]);
                            $this->db->where('tipepemesanan', 2);
                            $this->db->where('status', 1);
                            $cekDist = $this->db->get('tgudang_pemesanan');
                            if ($cekDist->num_rows() > 0) {
                                $rDist          = $cekDist->row();

                                $this->db->where('idpemesanan', $rDist->id);
                                $this->db->where('idtipe', $r[6]);
                                $this->db->where('idbarang', $r[8]);
                                $cekBarang = $this->db->get('tgudang_pemesanan_detail');

                                $totalHarga     = $r[4]*$r[3];
                                $this->db->set('totalbarang', '`totalbarang` + '.$r[4], false);
                                $this->db->set('totalharga', '`totalharga` + '.$totalHarga, false);
                                $this->db->where('id', $rDist->id);
                                if ($this->db->update('tgudang_pemesanan')) {
                                    $this->db->where('idpemesanan', $rDist->id);
                                    $this->db->where('idtipe', $r[6]);
                                    $this->db->where('idbarang', $r[8]);
                                    $cekBarang = $this->db->get('tgudang_pemesanan_detail');
                                    if ($cekBarang->num_rows() > 0) {
                                        $rBarang        = $cekBarang->row();
                                        $totalHarga     = $r[4]*$r[3];
                                        $this->db->set('kuantitas', '`kuantitas` + '.$r[4], false);
                                        $this->db->set('harga', $r[3]);
                                        $this->db->set('opsisatuan', $r[10]);
                                        $this->db->where('idpemesanan', $rDist->id);
                                        $this->db->where('idtipe', $r[6]);
                                        $this->db->where('idbarang', $r[8]);
                                        if ($this->db->update('tgudang_pemesanan_detail')) {
                                            $status = true;
                                        }
                                    } else {
                                        $this->db->set('idpemesanan', $rDist->id);
                                        $this->db->set('idtipe', $r[6]);
                                        $this->db->set('idbarang', $r[8]);
                                        $this->db->set('kuantitas', $r[4]);
                                        $this->db->set('harga', $r[3]);
                                        $this->db->set('opsisatuan', $r[10]);
                                        $this->db->set('status', 1);
                                        if ($this->db->insert('tgudang_pemesanan_detail')) {
                                            $status = true;
                                        }
                                    }
                                }
                            } else {
                                $tipepemesanan = ($r[6] == 4) ? 2 : 1;
                                $this->db->set('nopemesanan', '-');
                                $this->db->set('tipepemesanan', $tipepemesanan);
                                $this->db->set('tanggal', date('Y-m-d H:i:s'));
                                $this->db->set('iddistributor', $distributor);
                                $this->db->set('totalbarang', $r[4]);
                                $this->db->set('totalharga', $r[3]);
                                $this->db->set('status', 1);
                                if ($this->db->insert('tgudang_pemesanan')) {
                                    $idpemesanan = $this->db->insert_id();
                                    $this->db->set('idpemesanan', $idpemesanan);
                                    $this->db->set('opsisatuan', $r[10]);
                                    $this->db->set('idtipe', $r[6]);
                                    $this->db->set('idbarang', $r[8]);
                                    $this->db->set('kuantitas', $r[4]);
                                    $this->db->set('harga', $r[3]);
                                    $this->db->set('status', 1);
                                    if ($this->db->insert('tgudang_pemesanan_detail')) {
                                        $status = true;
                                    }
                                }
                            }
                        } else {
                            // gudang non logistik
                            $distributor = $r[7];
                            $this->db->where('iddistributor', $r[7]);
                            $this->db->where('tipepemesanan', 1);
                            $this->db->where('status', 1);
                            $cekDist = $this->db->get('tgudang_pemesanan');
                            if ($cekDist->num_rows() > 0) {
                                $rDist          = $cekDist->row();

                                $this->db->where('idpemesanan', $rDist->id);
                                $this->db->where('idtipe', $r[6]);
                                $this->db->where('idbarang', $r[8]);
                                $cekBarang = $this->db->get('tgudang_pemesanan_detail');

                                $totalHarga     = $r[4]*$r[3];
                                $this->db->set('totalbarang', '`totalbarang` + '.$r[4], false);
                                $this->db->set('totalharga', '`totalharga` + '.$totalHarga, false);
                                $this->db->where('id', $rDist->id);
                                if ($this->db->update('tgudang_pemesanan')) {
                                    $this->db->where('idpemesanan', $rDist->id);
                                    $this->db->where('idtipe', $r[6]);
                                    $this->db->where('idbarang', $r[8]);
                                    $cekBarang = $this->db->get('tgudang_pemesanan_detail');
                                    if ($cekBarang->num_rows() > 0) {
                                        $rBarang        = $cekBarang->row();
                                        $totalHarga     = $r[4]*$r[3];
                                        $this->db->set('kuantitas', '`kuantitas` + '.$r[4], false);
                                        $this->db->set('harga', $r[3]);
                                        $this->db->set('opsisatuan', $r[10]);
                                        $this->db->where('idpemesanan', $rDist->id);
                                        $this->db->where('idtipe', $r[6]);
                                        $this->db->where('idbarang', $r[8]);
                                        if ($this->db->update('tgudang_pemesanan_detail')) {
                                            $status = true;
                                        }
                                    } else {
                                        $this->db->set('idpemesanan', $rDist->id);
                                        $this->db->set('idtipe', $r[6]);
                                        $this->db->set('idbarang', $r[8]);
                                        $this->db->set('kuantitas', $r[4]);
                                        $this->db->set('harga', $r[3]);
                                        $this->db->set('opsisatuan', $r[10]);
                                        $this->db->set('status', 1);
                                        if ($this->db->insert('tgudang_pemesanan_detail')) {
                                            $status = true;
                                        }
                                    }
                                }
                            } else {
                                $tipepemesanan = ($r[6] == 4) ? 2 : 1;
                                $this->db->set('nopemesanan', '-');
                                $this->db->set('tipepemesanan', $tipepemesanan);
                                $this->db->set('tanggal', date('Y-m-d H:i:s'));
                                $this->db->set('iddistributor', $distributor);
                                $this->db->set('totalbarang', $r[4]);
                                $this->db->set('totalharga', $r[3]);
                                $this->db->set('status', 1);
                                if ($this->db->insert('tgudang_pemesanan')) {
                                    $idpemesanan = $this->db->insert_id();
                                    $this->db->set('idpemesanan', $idpemesanan);
                                    $this->db->set('opsisatuan', $r[10]);
                                    $this->db->set('idtipe', $r[6]);
                                    $this->db->set('idbarang', $r[8]);
                                    $this->db->set('kuantitas', $r[4]);
                                    $this->db->set('harga', $r[3]);
                                    $this->db->set('status', 1);
                                    if ($this->db->insert('tgudang_pemesanan_detail')) {
                                        $status = true;
                                    }
                                }
                            }
                        }
                    }
                }

                $this->db->where('idpemesanan', $idPemesanan);
                $cekDetail = $this->db->get('tgudang_pemesanan_detail')->num_rows();
                if ($cekDetail < 1) {
                    $this->db->set('status', 5);
                    $this->db->where('id', $idPemesanan);
                    if ($this->db->update('tgudang_pemesanan')) {
                        $status = true;
                    }
                }
            }

            $status = true;
        }
        return $status;
    }
	public function save_edit(){
		// print_r($this->input->post());exit();
		$jml_edit='0';
		$xidistributor=$this->input->post('xidistributor');
		$iddistributor=$this->input->post('iddistributor');
		$xtipe_bayar=$this->input->post('xtipe_bayar');
		$tipe_bayar=$this->input->post('tipe_bayar');
		
		$idpemesanan=$this->input->post('idpemesanan');
		$idtipe=$this->input->post('e_idtipe');
		$idbarang=$this->input->post('e_idbarang');
		// print_r($idtipe);exit();
		$st_edit=$this->input->post('e_st_edit');
		$st_hapus=$this->input->post('e_st_hapus');
		$qty=$this->input->post('e_kuantitas_besar');
		$qty_kecil=$this->input->post('e_kuantitas_kecil');
		$id_det=$this->input->post('e_id_det');
		$namabarang=$this->input->post('nama_barang');
		$opsisatuan=$this->input->post('opsisatuan');
		$harga=$this->input->post('e_hargabeli');
		$harga_besar=$this->input->post('e_hargabeli_besar');
		foreach ($st_edit as $key => $val) {
			if ($st_edit[$key]=='1'){
				$jml_edit=$jml_edit +1 ;
				$data_update=array();
				$data_update['idtipe']=$idtipe[$key];
				$data_update['idbarang']=$idbarang[$key];
				$data_update['opsisatuan']=$opsisatuan[$key];
				$data_update['kuantitas']=$qty[$key];
				$data_update['kuantitas_kecil']=$qty_kecil[$key];
				$data_update['edit_by']=$this->session->userdata('user_id');
				$data_update['date_edit']=date('Y-m-d H:i:s');
				// $data_update['namabarang']=RemoveBintang($namabarang[$key]);
				if ($opsisatuan[$key]=='1'){
					$data_update['harga']=RemoveComma($harga[$key]);
					$data_update['harga_total']=$qty_kecil[$key]*RemoveComma($harga[$key]);
				}else{
					$data_update['harga']=RemoveComma($harga_besar[$key]);
					$data_update['harga_total']=$qty[$key]*RemoveComma($harga_besar[$key]);
				}
				if ($st_hapus[$key]=='1'){					
					$data_update['status']='0';
					$data_update['hapus_by']=$this->session->userdata('user_id');
					$data_update['hapus_date']=date('Y-m-d H:i:s');
				}
				// print_r($data_update);exit();
				// $data_update['kuantitas']=$qty[$key];
				if ($id_det[$key]=='0'){
					$data_update['idpemesanan']=$idpemesanan;
					$data_update['status']='1';
					
					$this->db->insert('tgudang_pemesanan_detail',$data_update); 
				}else{
					$this->db->where('id', $id_det[$key]);
					$this->db->update('tgudang_pemesanan_detail',$data_update); 
				}
			}
		}
		if ($jml_edit > 0 || ($xidistributor != $iddistributor || $tipe_bayar != $xtipe_bayar)){
			$head_update=array();
			$head_update['iddistributor']=$iddistributor;
			$head_update['tipe_bayar']= $tipe_bayar;
			$head_update['edit_user']=$this->session->userdata('user_id');
			$head_update['edit_date']=date('Y-m-d H:i:s');
			// print_r($head_update);exit();
			$this->db->where('id', $idpemesanan);
			$this->db->update('tgudang_pemesanan',$head_update); 
		}
		return true;
	}
	public function save_edit_pemesanan(){
		// print_r($this->input->post());exit();
		$jml_edit='0';
		$xidistributor=$this->input->post('xidistributor');
		$iddistributor=$this->input->post('iddistributor');
		
		$idpemesanan=$this->input->post('idpemesanan');
		$idtipe=$this->input->post('e_idtipe');
		$idbarang=$this->input->post('e_idbarang');
		// print_r($idtipe);exit();
		$st_edit=$this->input->post('e_st_edit');
		$st_hapus=$this->input->post('e_st_hapus');
		$qty=$this->input->post('e_kuantitas_besar');
		$qty_kecil=$this->input->post('e_kuantitas_kecil');
		$id_det=$this->input->post('e_id_det');
		$namabarang=$this->input->post('nama_barang');
		$opsisatuan=$this->input->post('opsisatuan');
		$harga=$this->input->post('e_hargabeli');
		$harga_besar=$this->input->post('e_hargabeli_besar');
		foreach ($st_edit as $key => $val) {
			if ($st_edit[$key]=='1'){
				$jml_edit=$jml_edit +1 ;
				$data_update=array();
				$data_update['idtipe']=$idtipe[$key];
				$data_update['idbarang']=$idbarang[$key];
				$data_update['opsisatuan']=$opsisatuan[$key];
				$data_update['kuantitas']=$qty[$key];
				$data_update['kuantitas_kecil']=$qty_kecil[$key];
				$data_update['edit_by']=$this->session->userdata('user_id');
				$data_update['date_edit']=date('Y-m-d H:i:s');
				// $data_update['namabarang']=RemoveBintang($namabarang[$key]);
				if ($opsisatuan[$key]=='1'){
					$data_update['harga']=RemoveComma($harga[$key]);
					$data_update['harga_total']=$qty_kecil[$key]*RemoveComma($harga[$key]);
				}else{
					$data_update['harga']=RemoveComma($harga_besar[$key]);
					$data_update['harga_total']=$qty[$key]*RemoveComma($harga_besar[$key]);
				}
				if ($st_hapus[$key]=='1'){					
					$data_update['status']='0';
					$data_update['hapus_by']=$this->session->userdata('user_id');
					$data_update['hapus_date']=date('Y-m-d H:i:s');
				}
				// print_r($data_update);exit();
				// $data_update['kuantitas']=$qty[$key];
				if ($id_det[$key]=='0'){
					$data_update['idpemesanan']=$idpemesanan;
					$data_update['status']='1';
					
					$this->db->insert('tgudang_pemesanan_detail',$data_update); 
				}else{
					$this->db->where('id', $id_det[$key]);
					$this->db->update('tgudang_pemesanan_detail',$data_update); 
				}
			}
		}
		if ($jml_edit > 0 || ($xidistributor != $iddistributor)){
			$head_update=array();
			$head_update['iddistributor']=$iddistributor;
			$head_update['edit_user']=$this->session->userdata('user_id');
			$head_update['edit_date']=date('Y-m-d H:i:s');
			$this->db->where('id', $idpemesanan);
			$this->db->update('tgudang_pemesanan',$head_update); 
		}
		return true;
	}
	public function save_edit_po(){
		// print_r($this->input->post());exit();
		$tanggal=date('Y-m-d H:i:s');
		
		$e_idpemesana_new=$this->input->post('e_idpemesana_new');
		$idtipe=$this->input->post('e_idtipe');
		$idbarang=$this->input->post('e_idbarang');
		$id=$this->input->post('id');
		$id_detail=$this->input->post('e_id_det');
		$namabarang=$this->input->post('e_namabarang');
		// $e_idpemesana_new=$this->input->post('e_idpemesana_new');
		$terima=$this->input->post('e_terima');
		$opsisatuan=$this->input->post('e_opsisatuan');
		$jumlahsatuanbesar=$this->input->post('e_jumlahsatuanbesar');
		$sisa=$this->input->post('e_sisa');
				// print_r($e_idpemesana_new);exit();
		
		foreach ($e_idpemesana_new as $key => $val) {
			
			if ($e_idpemesana_new[$key]!='#'){
				if ($terima[$key]==0){//ALIHKAN SEMUA
					$data_update=array();
					$data_update['tanggal']=$tanggal;
					$data_update['user_id']=$this->session->userdata('user_id');
					$data_update['user_name']=$this->session->userdata('user_name');
					$data_update['idtipe']=$idtipe[$key];
					$data_update['idbarang']=$idbarang[$key];
					$data_update['id_detail']=$id_detail[$key];
					$data_update['id_detail_new']=$id_detail[$key];
					$data_update['namabarang']=$namabarang[$key];
					$data_update['idpenerimaan_asal']=$id;
					$data_update['idpenerimaan_baru']=$e_idpemesana_new[$key];
					$data_update['opsisatuan']=$opsisatuan[$key];
					$data_update['kuantitas']=$sisa[$key];
					$data_update['jenis_pindah']=1;
					$data_update['alasan']='ALIHKAN SEMUA';
					
					if ($this->db->insert('tgudang_pindah_po',$data_update)){
						$this->db->where('id',$id_detail[$key]);
						$this->db->update('tgudang_pemesanan_detail',array('idpemesanan'=>$e_idpemesana_new[$key])); 
					}
				}else{//ALIHKAN SEBAGIAN
				
					$id_new=$this->auto_insert_alihan($id_detail[$key],$e_idpemesana_new[$key],$sisa[$key]);//INSERT KE DETAIL NEW
					$data_update=array();
					$data_update['tanggal']=$tanggal;
					$data_update['user_id']=$this->session->userdata('user_id');
					$data_update['user_name']=$this->session->userdata('user_name');
					$data_update['idtipe']=$idtipe[$key];
					$data_update['idbarang']=$idbarang[$key];
					$data_update['id_detail']=$id_detail[$key];
					$data_update['id_detail_new']=$id_new;
					$data_update['namabarang']=$namabarang[$key];
					$data_update['idpenerimaan_asal']=$id;
					$data_update['idpenerimaan_baru']=$e_idpemesana_new[$key];
					$data_update['opsisatuan']=$opsisatuan[$key];
					$data_update['kuantitas']=$sisa[$key];
					$data_update['jenis_pindah']=2;
					$data_update['alasan']='ALIHKAN SEBAGIAN';
					
					if ($this->db->insert('tgudang_pindah_po',$data_update)){
						$this->db->where('id',$id_detail[$key]);
						$this->db->update('tgudang_pemesanan_detail',array('kuantitas'=>$terima[$key],'kuantitas_kecil'=>$terima[$key]*$jumlahsatuanbesar[$key])); 
					}
				}
			}
		}
		$q_sisa="SELECT count(*) as jml_sisa FROM (SELECT D.id,D.kuantitas	
				,COALESCE((SELECT SUM(P.kuantitas) FROM tgudang_penerimaan_detail P WHERE P.idpemesanandetail=D.id AND P.`status`='1'),0) as sudah_terima
				,D.kuantitas - (SELECT sudah_terima) as sisa
				from tgudang_pemesanan_detail D 
				WHERE D.idpemesanan='$id' AND D.status = '1') T WHERE T.sisa > 0";
		// print_r($q_sisa);
		// exit();
		$jml_sisa=$this->db->query($q_sisa)->row('jml_sisa');
		if ($jml_sisa=='0'){//Jika Sudah Dianggap Selelsai Update Header Selesai
			$this->db->where('id',$id);
			$this->db->update('tgudang_pemesanan',array('status'=>5));
		}
		// $jml=$hasil->row('jml_sisa');
		
		return true;
	}
	function auto_insert_alihan($id_det,$idPemesanan_new,$kuantitas){
		$user_id=$this->session->userdata('user_id');
		$q="
			INSERT INTO tgudang_pemesanan_detail (idpemesanan,idtipe,idbarang,opsisatuan,kuantitas,kuantitas_kecil,harga,harga_total,status,harga_terakhir,stok_terakhir,id_asal,harga_sebelumnya,stok_sebelumnya,st_konversi,edit_by,date_edit)  
			SELECT '$idPemesanan_new' as idpemesanan,D.idtipe,D.idbarang,D.opsisatuan,'$kuantitas' as kuantitas,CASE WHEN D.opsisatuan='1' THEN ".$kuantitas." ELSE ".$kuantitas." * B.jumlahsatuanbesar END as kuantitas_kecil 
			,D.harga,D.harga*".$kuantitas." as harga_total,'1',D.harga_terakhir,D.stok_terakhir,D.id_asal,D.harga_sebelumnya,D.stok_sebelumnya,D.st_konversi,'$user_id' as edit_by,NOW() as date_edit
			from tgudang_pemesanan_detail D
			LEFT JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe

			WHERE D.id='$id_det'
		";
		// print_r($q);exit();
		$this->db->query($q);
		return $this->db->insert_id();
	}
	public function view_barang() {
		$idtipe             = $this->input->post('idtipe');
		$idtipe=implode(",", $idtipe);
        $idunitpelayanan    = $this->input->post('idunitpelayanan');
		 if ($idtipe) {
			
			$q="(SELECT B.id,B.nama,B.namatipe,B.kode,B.hargabeli,B.idtipe,
			COALESCE((SELECT mgudang_stok.stok from mgudang_stok 
			where mgudang_stok.idunitpelayanan='$idunitpelayanan' AND mgudang_stok.idtipe=B.idtipe AND mgudang_stok.idbarang=B.id),0) as stok 
			FROM view_barang B
			WHERE B.idtipe IN ($idtipe))T ";
			$this->load->library('datatables'); 
			$this->datatables->select('id,nama,namatipe,kode,stok,hargabeli,idtipe'); 
			$this->datatables->from($q); 
			$this->datatables->add_column("Actions", "<center></center>", "id");
			$this->datatables->unset_column('id');
			// echo $this->datatables->generate();
			
			 return print($this->datatables->generate());
			// $query=$this->db->query($q);
			// $res=$query->result();
			// $this->output->set_output(json_encode($res));
		 }
    }
    public function konfirmasi($id){
		
		$head_update=array();
		$head_update['id_user_konfirmasi']=$this->session->userdata('user_id');
		$head_update['nama_user_konfirmasi']=$this->session->userdata('user_name');
		$head_update['tanggal_konfirmasi']=date('Y-m-d H:i:s');
		$head_update['status']='2';
		$this->db->where('id', $id);
		$this->db->update('tgudang_pemesanan',$head_update); 
		//Approval
		$this->db->where('idpemesanan',$id);
		$this->db->delete('tgudang_pemesanan_approval');
		$q="SELECT S.iduser,S.step,U.`name` as user_nama,S.proses_setuju,S.proses_tolak
		FROM tgudang_pemesanan TR
		LEFT JOIN mlogic_gudang S ON TR.tipe_bayar=S.idtipe AND TR.tipepemesanan=S.tipepemesanan AND S.status='1'
		LEFT JOIN musers U ON U.id=S.iduser
		WHERE TR.id='$id' AND calculate_logic(S.operand, TR.totalharga, S.nominal)='1'
		ORDER BY S.step,S.id";
		$list_user=$this->db->query($q)->result();
		$step=999;
		$idlogic='';
		foreach ($list_user as $row){
			if ($row->step < $step){
				$step=$row->step;
			}
			$data=array(
				'idpemesanan' => $id,
				'step' => $row->step,
				'iduser' => $row->iduser,
				'user_nama' => $row->user_nama,
				'proses_setuju' => $row->proses_setuju,
				'proses_tolak' => $row->proses_tolak,
				'approve' => 0,
			);
			$this->db->insert('tgudang_pemesanan_approval', $data);
		}

		$this->db->update('tgudang_pemesanan_approval',array('st_aktif'=>1),array('idpemesanan'=>$id,'step'=>$step));

		
		return true;
	}
	public function save_pesan()
    {
        $detailValue = json_decode($this->input->post('detailValue'));
		// print_r($detailValue);exit();
		
        if (count($detailValue) > 0) {
            foreach ($detailValue as $r) {
                $tipegudang = $r[6];

                if ($tipegudang == 4) {
                    // gudang logistik

                    $distributor = $r[7];
                    $this->db->where('iddistributor', $r[7]);
                    $this->db->where('tipepemesanan', 2);
                    $this->db->where('status', 1);
                    $cekDist = $this->db->get('tgudang_pemesanan');
                    if ($cekDist->num_rows() > 0) {
                        $rDist          = $cekDist->row();
                        $totalHarga     = $r[4]*$r[3];
                        $this->db->set('totalbarang', '`totalbarang` + '.$r[4], false);
                        $this->db->set('totalharga', '`totalharga` + '.$totalHarga, false);
                        $this->db->where('id', $rDist->id);
                        if ($this->db->update('tgudang_pemesanan')) {
                            $this->db->where('idpemesanan', $rDist->id);
                            $this->db->where('idtipe', $r[6]);
                            $this->db->where('idbarang', $r[8]);
                            $cekBarang = $this->db->get('tgudang_pemesanan_detail');
                            if ($cekBarang->num_rows() > 0) {
                                $rBarang        = $cekBarang->row();
                                
                                $totalHarga     = $r[4]*$r[3];
                                $this->db->set('kuantitas', '`kuantitas` + '.$r[4], false);
								$this->db->set('kuantitas_kecil', '`kuantitas_kecil` + '.$r[13], false);
                                $this->db->set('harga', $r[3]);
                                $this->db->set('opsisatuan', $r[10]);
                                $this->db->where('idpemesanan', $rDist->id);
                                $this->db->where('idtipe', $r[6]);
                                $this->db->where('idbarang', $r[8]);
                                if ($this->db->update('tgudang_pemesanan_detail')) {
									$data_his['iddet_gudang']=$cekBarang->row()->id;
									$data_his['iddet_permintaan']=0;
									$data_his['unit_peminta']='0';
									$data_his['no_up_permintaan']='-';
									$data_his['tipepemesanan']='2';
									$data_his['idtipe']=$r[6];
									$data_his['idbarang']=$r[8];
									$data_his['opsisatuan']=$r[10];
									$data_his['kuantitas']=$r[4];
									$data_his['kuantitas_kecil']=$r[13];
									$data_his['iduser_peminta']=$this->session->userdata('user_id');
									$data_his['tanggal']=date('Y-m-d H:i:s');
									$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                    $status = true;
                                }
                            } else {
                                $this->db->set('idpemesanan', $rDist->id);
                                $this->db->set('opsisatuan', $r[10]);
                                $this->db->set('idtipe', $r[6]);
                                $this->db->set('idbarang', $r[8]);
                                $this->db->set('kuantitas', $r[4]);
								$this->db->set('harga_total', $r[4]*$r[3]);
								$this->db->set('kuantitas_kecil', $r[13]);
                                $this->db->set('harga', $r[3]);
                                $this->db->set('status', 1);
                                $this->db->set('harga_terakhir', $r[11]);
                                $this->db->set('stok_terakhir', $r[12]);
                                if ($this->db->insert('tgudang_pemesanan_detail')) {
									$data_his['iddet_gudang']=$this->db->insert_id();
									$data_his['iddet_permintaan']=0;
									$data_his['unit_peminta']='0';
									$data_his['no_up_permintaan']='-';
									$data_his['tipepemesanan']='2';
									$data_his['idtipe']=$r[6];
									$data_his['idbarang']=$r[8];
									$data_his['opsisatuan']=$r[10];
									$data_his['kuantitas']=$r[4];
									$data_his['kuantitas_kecil']=$r[13];
									$data_his['iduser_peminta']=$this->session->userdata('user_id');
									$data_his['tanggal']=date('Y-m-d H:i:s');
									$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                    $status = true;
                                }
                            }
                        }
                    } else {
                        $tipepemesanan = ($r[6] == 4) ? 2 : 1;
                        $totalHarga = $r[4]*$r[3];
                        $this->db->set('nopemesanan', '-');
                        $this->db->set('stdraft', '1');
                        $this->db->set('tipepemesanan', $tipepemesanan);
                        $this->db->set('tanggal', date('Y-m-d H:i:s'));
                        $this->db->set('iddistributor', $distributor);
                        $this->db->set('totalbarang', $r[4]);
                        $this->db->set('totalharga', $totalHarga);
                        $this->db->set('userpemesanan', $this->session->userdata('user_name'));
						$this->db->set('tgl_pemesanan', date('Y-m-d H:s:i'));
                        $this->db->set('status', 1);
                        if ($this->db->insert('tgudang_pemesanan')) {
                            $idpemesanan = $this->db->insert_id();

                            $this->db->set('idpemesanan', $idpemesanan);
                            $this->db->set('opsisatuan', $r[10]);
                            $this->db->set('idtipe', $r[6]);
                            $this->db->set('idbarang', $r[8]);
                            $this->db->set('kuantitas', $r[4]);
							$this->db->set('kuantitas_kecil', $r[13]);
                            $this->db->set('harga', $r[3]);
                            $this->db->set('status', 1);
                            $this->db->set('harga_terakhir', $r[11]);
                            $this->db->set('stok_terakhir', $r[12]);
                            if ($this->db->insert('tgudang_pemesanan_detail')) {
								$data_his['iddet_gudang']=$this->db->insert_id();
								$data_his['iddet_permintaan']=0;
								$data_his['unit_peminta']='0';
								$data_his['no_up_permintaan']='-';
								$data_his['tipepemesanan']='2';
								$data_his['idtipe']=$r[6];
								$data_his['idbarang']=$r[8];
								$data_his['opsisatuan']=$r[10];
								$data_his['kuantitas']=$r[4];
								$data_his['kuantitas_kecil']=$r[13];
								$data_his['iduser_peminta']=$this->session->userdata('user_id');
								$data_his['tanggal']=date('Y-m-d H:i:s');
								$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                $status = true;
                            }
                        }
                    }
                } else {

                    // gudang non logistik
                    $distributor = $r[7];
                    $this->db->where('iddistributor', $r[7]);
                    $this->db->where('tipepemesanan', 1);
                    $this->db->where('status', 1);
                    $cekDist = $this->db->get('tgudang_pemesanan');
                    if ($cekDist->num_rows() > 0) {
                        $rDist          = $cekDist->row();
                        $totalHarga     = $r[4]*$r[3];
                        $this->db->set('totalbarang', '`totalbarang` + '.$r[4], false);
                        $this->db->set('totalharga', '`totalharga` + '.$totalHarga, false);
                        $this->db->where('id', $rDist->id);
                        if ($this->db->update('tgudang_pemesanan')) {
                            $this->db->where('idpemesanan', $rDist->id);
                            $this->db->where('idtipe', $r[6]);
                            $this->db->where('idbarang', $r[8]);
                            $cekBarang = $this->db->get('tgudang_pemesanan_detail');
                            if ($cekBarang->num_rows() > 0) {
                                $rBarang        = $cekBarang->row();
                                
                                $totalHarga     = $r[4]*$r[3];
                                $this->db->set('kuantitas', '`kuantitas` + '.$r[4], false);
								$this->db->set('kuantitas_kecil', '`kuantitas_kecil` + '.$r[13], false);
                                $this->db->set('harga', $r[3]);
                                $this->db->set('opsisatuan', $r[10]);
                                $this->db->where('idpemesanan', $rDist->id);
                                $this->db->where('idtipe', $r[6]);
                                $this->db->where('idbarang', $r[8]);
                                if ($this->db->update('tgudang_pemesanan_detail')) {
									$data_his=array();
									$data_his['iddet_gudang']=$cekBarang->row()->id;
									$data_his['iddet_permintaan']=0;
									$data_his['unit_peminta']='0';
									$data_his['no_up_permintaan']='-';
									$data_his['tipepemesanan']='1';
									$data_his['idtipe']=$r[6];
									$data_his['idbarang']=$r[8];
									$data_his['opsisatuan']=$r[10];
									$data_his['kuantitas']=$r[4];
									$data_his['kuantitas_kecil']=$r[13];
									$data_his['iduser_peminta']=$this->session->userdata('user_id');
									$data_his['tanggal']=date('Y-m-d H:i:s');
									$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                    $status = true;
                                }
                            } else {
                                $this->db->set('idpemesanan', $rDist->id);
                                $this->db->set('opsisatuan', $r[10]);
                                $this->db->set('idtipe', $r[6]);
                                $this->db->set('idbarang', $r[8]);
                                $this->db->set('kuantitas', $r[4]);
                                $this->db->set('harga_total', $r[4]*$r[3]);
                                $this->db->set('kuantitas_kecil', $r[13]);
                                $this->db->set('harga', $r[3]);
                                $this->db->set('status', 1);
                                $this->db->set('harga_terakhir', $r[11]);
                                $this->db->set('stok_terakhir', $r[12]);
                                if ($this->db->insert('tgudang_pemesanan_detail')) {
									$data_his=array();
									$data_his['iddet_gudang']=$this->db->insert_id();
									$data_his['iddet_permintaan']=0;
									$data_his['unit_peminta']='0';
									$data_his['no_up_permintaan']='-';
									$data_his['tipepemesanan']='1';
									$data_his['idtipe']=$r[6];
									$data_his['idbarang']=$r[8];
									$data_his['opsisatuan']=$r[10];
									$data_his['kuantitas']=$r[4];
									$data_his['kuantitas_kecil']=$r[13];
									$data_his['iduser_peminta']=$this->session->userdata('user_id');
									$data_his['tanggal']=date('Y-m-d H:i:s');
									$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                    $status = true;
                                }
                            }
                        }
                    } else {
                        $tipepemesanan = ($r[6] == 4) ? 2 : 1;
                        $totalHarga = $r[4]*$r[3];
                        $this->db->set('nopemesanan', '-');
                        $this->db->set('stdraft', 1);
                        $this->db->set('tipepemesanan', $tipepemesanan);
                        $this->db->set('tanggal', date('Y-m-d H:i:s'));
                        $this->db->set('iddistributor', $distributor);
                        $this->db->set('totalbarang', $r[4]);
                        $this->db->set('totalharga', $totalHarga);
                        $this->db->set('userpemesanan', $this->session->userdata('user_name'));
                        $this->db->set('tgl_pemesanan', date('Y-m-d H:s:i'));
                        $this->db->set('status', 1);
                        if ($this->db->insert('tgudang_pemesanan')) {
                            $idpemesanan = $this->db->insert_id();

                            $this->db->set('idpemesanan', $idpemesanan);
                            $this->db->set('opsisatuan', $r[10]);
                            $this->db->set('idtipe', $r[6]);
                            $this->db->set('idbarang', $r[8]);
                            $this->db->set('kuantitas', $r[4]);
                            $this->db->set('kuantitas_kecil', $r[13]);
                            $this->db->set('harga', $r[3]);
                            $this->db->set('status', 1);
                            $this->db->set('harga_terakhir', $r[11]);
                            $this->db->set('stok_terakhir', $r[12]);
                            if ($this->db->insert('tgudang_pemesanan_detail')) {
								$data_his['iddet_gudang']=$this->db->insert_id();
								$data_his['iddet_permintaan']=0;
								$data_his['unit_peminta']='0';
								$data_his['no_up_permintaan']='-';
								$data_his['tipepemesanan']='1';
								$data_his['idtipe']=$r[6];
								$data_his['idbarang']=$r[8];
								$data_his['opsisatuan']=$r[10];
								$data_his['kuantitas']=$r[4];
								$data_his['kuantitas_kecil']=$r[13];
								$data_his['iduser_peminta']=$this->session->userdata('user_id');
								$data_his['tanggal']=date('Y-m-d H:i:s');
								$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                $status = true;
                            }
                        }
                    }
                }
            }
        }
        return $status;
    }
    public function save()
    {
		// print_r($this->input->post());exit();
        $detailValue = $this->input->post('detailValue');
		
        if (count($detailValue) > 0) {
            foreach ($detailValue as $r) {
                $tipegudang = $r[6];

                if ($tipegudang == 4) {
                    // gudang logistik

                    $distributor = $r[7];
                    $this->db->where('iddistributor', $r[7]);
                    $this->db->where('tipepemesanan', 2);
                    $this->db->where('status', 1);
                    $cekDist = $this->db->get('tgudang_pemesanan');
                    if ($cekDist->num_rows() > 0) {
                        $rDist          = $cekDist->row();
                        $totalHarga     = $r[4]*$r[3];
                        $this->db->set('totalbarang', '`totalbarang` + '.$r[4], false);
                        $this->db->set('totalharga', '`totalharga` + '.$totalHarga, false);
                        $this->db->where('id', $rDist->id);
                        if ($this->db->update('tgudang_pemesanan')) {
                            $this->db->where('idpemesanan', $rDist->id);
                            $this->db->where('idtipe', $r[6]);
                            $this->db->where('idbarang', $r[8]);
                            $cekBarang = $this->db->get('tgudang_pemesanan_detail');
                            if ($cekBarang->num_rows() > 0) {
                                $rBarang        = $cekBarang->row();
                                
                                $totalHarga     = $r[4]*$r[3];
                                $this->db->set('kuantitas', '`kuantitas` + '.$r[4], false);
								$this->db->set('kuantitas_kecil', '`kuantitas_kecil` + '.$r[13], false);
                                $this->db->set('harga', $r[3]);
                                $this->db->set('opsisatuan', $r[10]);
                                $this->db->where('idpemesanan', $rDist->id);
                                $this->db->where('idtipe', $r[6]);
                                $this->db->where('idbarang', $r[8]);
                                if ($this->db->update('tgudang_pemesanan_detail')) {
									$data_his['iddet_gudang']=$cekBarang->row()->id;
									$data_his['iddet_permintaan']=0;
									$data_his['unit_peminta']='0';
									$data_his['no_up_permintaan']='-';
									$data_his['tipepemesanan']='2';
									$data_his['idtipe']=$r[6];
									$data_his['idbarang']=$r[8];
									$data_his['opsisatuan']=$r[10];
									$data_his['kuantitas']=$r[4];
									$data_his['kuantitas_kecil']=$r[13];
									$data_his['iduser_peminta']=$this->session->userdata('user_id');
									$data_his['tanggal']=date('Y-m-d H:i:s');
									$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                    $status = true;
                                }
                            } else {
                                $this->db->set('idpemesanan', $rDist->id);
                                $this->db->set('opsisatuan', $r[10]);
                                $this->db->set('idtipe', $r[6]);
                                $this->db->set('idbarang', $r[8]);
                                $this->db->set('kuantitas', $r[4]);
								$this->db->set('kuantitas_kecil', $r[13]);
                                $this->db->set('harga', $r[3]);
                                $this->db->set('status', 1);
                                $this->db->set('harga_terakhir', $r[11]);
                                $this->db->set('stok_terakhir', $r[12]);
                                if ($this->db->insert('tgudang_pemesanan_detail')) {
									$data_his['iddet_gudang']=$this->db->insert_id();
									$data_his['iddet_permintaan']=0;
									$data_his['unit_peminta']='0';
									$data_his['no_up_permintaan']='-';
									$data_his['tipepemesanan']='2';
									$data_his['idtipe']=$r[6];
									$data_his['idbarang']=$r[8];
									$data_his['opsisatuan']=$r[10];
									$data_his['kuantitas']=$r[4];
									$data_his['kuantitas_kecil']=$r[13];
									$data_his['iduser_peminta']=$this->session->userdata('user_id');
									$data_his['tanggal']=date('Y-m-d H:i:s');
									$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                    $status = true;
                                }
                            }
                        }
                    } else {
                        $tipepemesanan = ($r[6] == 4) ? 2 : 1;
                        $totalHarga = $r[4]*$r[3];
                        $this->db->set('nopemesanan', '-');
                        $this->db->set('stdraft', '1');
                        $this->db->set('tipepemesanan', $tipepemesanan);
                        $this->db->set('tanggal', date('Y-m-d H:i:s'));
                        $this->db->set('iddistributor', $distributor);
                        $this->db->set('totalbarang', $r[4]);
                        $this->db->set('totalharga', $totalHarga);
                        $this->db->set('userpemesanan', $this->session->userdata('user_name'));
						$this->db->set('tgl_pemesanan', date('Y-m-d H:s:i'));
                        $this->db->set('status', 1);
                        if ($this->db->insert('tgudang_pemesanan')) {
                            $idpemesanan = $this->db->insert_id();

                            $this->db->set('idpemesanan', $idpemesanan);
                            $this->db->set('opsisatuan', $r[10]);
                            $this->db->set('idtipe', $r[6]);
                            $this->db->set('idbarang', $r[8]);
                            $this->db->set('kuantitas', $r[4]);
							$this->db->set('kuantitas_kecil', $r[13]);
                            $this->db->set('harga', $r[3]);
                            $this->db->set('status', 1);
                            $this->db->set('harga_terakhir', $r[11]);
                            $this->db->set('stok_terakhir', $r[12]);
                            if ($this->db->insert('tgudang_pemesanan_detail')) {
								$data_his['iddet_gudang']=$this->db->insert_id();
								$data_his['iddet_permintaan']=0;
								$data_his['unit_peminta']='0';
								$data_his['no_up_permintaan']='-';
								$data_his['tipepemesanan']='2';
								$data_his['idtipe']=$r[6];
								$data_his['idbarang']=$r[8];
								$data_his['opsisatuan']=$r[10];
								$data_his['kuantitas']=$r[4];
								$data_his['kuantitas_kecil']=$r[13];
								$data_his['iduser_peminta']=$this->session->userdata('user_id');
								$data_his['tanggal']=date('Y-m-d H:i:s');
								$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                $status = true;
                            }
                        }
                    }
                } else {

                    // gudang non logistik
                    $distributor = $r[7];
                    $this->db->where('iddistributor', $r[7]);
                    $this->db->where('tipepemesanan', 1);
                    $this->db->where('status', 1);
                    $cekDist = $this->db->get('tgudang_pemesanan');
                    if ($cekDist->num_rows() > 0) {
                        $rDist          = $cekDist->row();
                        $totalHarga     = $r[4]*$r[3];
                        $this->db->set('totalbarang', '`totalbarang` + '.$r[4], false);
                        $this->db->set('totalharga', '`totalharga` + '.$totalHarga, false);
                        $this->db->where('id', $rDist->id);
                        if ($this->db->update('tgudang_pemesanan')) {
                            $this->db->where('idpemesanan', $rDist->id);
                            $this->db->where('idtipe', $r[6]);
                            $this->db->where('idbarang', $r[8]);
                            $cekBarang = $this->db->get('tgudang_pemesanan_detail');
                            if ($cekBarang->num_rows() > 0) {
                                $rBarang        = $cekBarang->row();
                                
                                $totalHarga     = $r[4]*$r[3];
                                $this->db->set('kuantitas', '`kuantitas` + '.$r[4], false);
								$this->db->set('kuantitas_kecil', '`kuantitas_kecil` + '.$r[13], false);
                                $this->db->set('harga', $r[3]);
                                $this->db->set('opsisatuan', $r[10]);
                                $this->db->where('idpemesanan', $rDist->id);
                                $this->db->where('idtipe', $r[6]);
                                $this->db->where('idbarang', $r[8]);
                                if ($this->db->update('tgudang_pemesanan_detail')) {
									$data_his=array();
									$data_his['iddet_gudang']=$cekBarang->row()->id;
									$data_his['iddet_permintaan']=0;
									$data_his['unit_peminta']='0';
									$data_his['no_up_permintaan']='-';
									$data_his['tipepemesanan']='1';
									$data_his['idtipe']=$r[6];
									$data_his['idbarang']=$r[8];
									$data_his['opsisatuan']=$r[10];
									$data_his['kuantitas']=$r[4];
									$data_his['kuantitas_kecil']=$r[13];
									$data_his['iduser_peminta']=$this->session->userdata('user_id');
									$data_his['tanggal']=date('Y-m-d H:i:s');
									$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                    $status = true;
                                }
                            } else {
                                $this->db->set('idpemesanan', $rDist->id);
                                $this->db->set('opsisatuan', $r[10]);
                                $this->db->set('idtipe', $r[6]);
                                $this->db->set('idbarang', $r[8]);
                                $this->db->set('kuantitas', $r[4]);
                                $this->db->set('kuantitas_kecil', $r[13]);
                                $this->db->set('harga', $r[3]);
                                $this->db->set('status', 1);
                                $this->db->set('harga_terakhir', $r[11]);
                                $this->db->set('stok_terakhir', $r[12]);
                                if ($this->db->insert('tgudang_pemesanan_detail')) {
									$data_his=array();
									$data_his['iddet_gudang']=$this->db->insert_id();
									$data_his['iddet_permintaan']=0;
									$data_his['unit_peminta']='0';
									$data_his['no_up_permintaan']='-';
									$data_his['tipepemesanan']='1';
									$data_his['idtipe']=$r[6];
									$data_his['idbarang']=$r[8];
									$data_his['opsisatuan']=$r[10];
									$data_his['kuantitas']=$r[4];
									$data_his['kuantitas_kecil']=$r[13];
									$data_his['iduser_peminta']=$this->session->userdata('user_id');
									$data_his['tanggal']=date('Y-m-d H:i:s');
									$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                    $status = true;
                                }
                            }
                        }
                    } else {
                        $tipepemesanan = ($r[6] == 4) ? 2 : 1;
                        $totalHarga = $r[4]*$r[3];
                        $this->db->set('nopemesanan', '-');
                        $this->db->set('stdraft', 1);
                        $this->db->set('tipepemesanan', $tipepemesanan);
                        $this->db->set('tanggal', date('Y-m-d H:i:s'));
                        $this->db->set('iddistributor', $distributor);
                        $this->db->set('totalbarang', $r[4]);
                        $this->db->set('totalharga', $totalHarga);
                        $this->db->set('userpemesanan', $this->session->userdata('user_name'));
                        $this->db->set('tgl_pemesanan', date('Y-m-d H:s:i'));
                        $this->db->set('status', 1);
                        if ($this->db->insert('tgudang_pemesanan')) {
                            $idpemesanan = $this->db->insert_id();

                            $this->db->set('idpemesanan', $idpemesanan);
                            $this->db->set('opsisatuan', $r[10]);
                            $this->db->set('idtipe', $r[6]);
                            $this->db->set('idbarang', $r[8]);
                            $this->db->set('kuantitas', $r[4]);
                            $this->db->set('kuantitas_kecil', $r[13]);
                            $this->db->set('harga', $r[3]);
                            $this->db->set('status', 1);
                            $this->db->set('harga_terakhir', $r[11]);
                            $this->db->set('stok_terakhir', $r[12]);
                            if ($this->db->insert('tgudang_pemesanan_detail')) {
								$data_his['iddet_gudang']=$this->db->insert_id();
								$data_his['iddet_permintaan']=0;
								$data_his['unit_peminta']='0';
								$data_his['no_up_permintaan']='-';
								$data_his['tipepemesanan']='1';
								$data_his['idtipe']=$r[6];
								$data_his['idbarang']=$r[8];
								$data_his['opsisatuan']=$r[10];
								$data_his['kuantitas']=$r[4];
								$data_his['kuantitas_kecil']=$r[13];
								$data_his['iduser_peminta']=$this->session->userdata('user_id');
								$data_his['tanggal']=date('Y-m-d H:i:s');
								$this->db->insert('tgudang_pemesanan_history',$data_his);//Menyimpan History
                                $status = true;
                            }
                        }
                    }
                }
            }
        }
        return $status;
    }

    public function editPemesanan($id)
    {
        $this->db->select(
            'a.id, 
            a.idtipe,
            a.idbarang,
            b.iddistributor,
            a.harga,
            a.kuantitas,
            a.opsisatuan'
        );
        $this->db->where('a.idpemesanan', $id);
        $this->db->join('tgudang_pemesanan b', 'b.id = a.idpemesanan', 'left');
        return $this->db->get('tgudang_pemesanan_detail a')->result();
    }

    public function viewHead($id)
    {
        $this->db->select('a.id, a.nopemesanan, a.totalbarang, a.totalharga, b.nama as distributor');
        $this->db->from('tgudang_pemesanan a');
        $this->db->join('mdistributor b', 'b.id = a.iddistributor', 'left');
        $this->db->where('a.id', $id);
        return $this->db->get()->row_array();
    }

    public function viewDetail($idpemesanan)
    {
        $this->db->where('idpemesanan', $idpemesanan);
        return $this->db->get('tgudang_pemesanan_detail')->result();
    }

    public function getNamaBarang($idtipe, $idbarang)
    {
        $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        $this->db->from($table[$idtipe]);
        $this->db->where('id', $idbarang);
        return $this->db->get()->row()->nama;
    }

    public function getSatuanBarang($idtipe, $idbarang)
    {
        $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        if ($idtipe == 2 || $idtipe == 4) {
            return '-';
        } else {
            $this->db->select('b.nama');
            $this->db->join('msatuan b', 'b.id = a.idsatuanbesar', 'left');
            $this->db->where('a.id', $idbarang);
            return $this->db->get($table[$idtipe].' a', 1)->row()->nama;
        }
    }

    public function getAnyQty($idtipe, $idbarang, $idpemesanan)
    {
        $this->db->where('idpemesanan', $idpemesanan);
        $this->db->where('idtipe', $idtipe);
        $this->db->where('idbarang', $idbarang);
        $r = $this->db->get('tgudang_pemesanan_detail', 1)->row();
        if ($r->status == 1) {
            $jmlterima = 0;
            $this->db->select_sum('kuantitas', 'kuantitas');
            $this->db->where('b.idpemesanan', $idpemesanan);
            $this->db->where('a.idtipe', $idtipe);
            $this->db->where('a.idbarang', $idbarang);
            $this->db->join('tgudang_penerimaan b', 'b.id = a.idpenerimaan', 'left');
            $rk = $this->db->get('tgudang_penerimaan_detail a')->row();
            if ($rk) {
                $jmlterima = $rk->kuantitas;
            }

            $data['jmlpesan']   = $r->kuantitas;
            $data['jmlterima']  = $jmlterima;
            $data['selisih']    = $r->kuantitas-$jmlterima;
        } else {
            $data['jmlpesan']   = $r->kuantitas;
            $data['jmlterima']  = $r->kuantitas;
            $data['selisih']    = 0;
        }
        return $data;
    }

    public function getJmlPesanTerima($idpemesanan)
    {
        $this->db->select_sum('totalbarang', 'totalbarang');
        $this->db->where('idpemesanan', $idpemesanan);
        $rt = $this->db->get('tgudang_penerimaan', 1)->row();
        if ($rt->totalbarang == null) {
            $data['jmlterima'] = 0;
        } else {
            $data['jmlterima'] = $rt->totalbarang;
        }
        return $data;
    }


    public function getMaxDistributor($idtipe, $idbarang)
    {
        $table = "(SELECT
        iddistributor, total FROM (
        SELECT
        g.iddistributor,
        COUNT(g.iddistributor) total
        FROM tgudang_pemesanan_detail gdet
        INNER JOIN tgudang_pemesanan g ON g.id = gdet.idpemesanan
        WHERE idbarang = ".$idbarang." AND idtipe = ".$idtipe."
        AND g.status != 1 AND g.status != 0
        GROUP BY g.iddistributor ) t
        ORDER BY total DESC LIMIT 1) tbl";
        $this->db->select('iddistributor');
        return $this->db->get($table)->row();
    }

    public function getStokGudang($idtipe, $idbarang)
    {
        $this->db->where('idtipe', $idtipe);
        $this->db->where('idbarang', $idbarang);
        $this->db->where('idunitpelayanan', 0);
        $query = $this->db->get('mgudang_stok', 1)->row();
        if ($query) {
            return $query->stok;
        } else {
            return 0;
        }
    }

    public function print_po_head($id)
    {
        if ($id) {
            $this->db->where('gp.id', $id);
            $this->db->select('gp.*, dist.nama, dist.alamat, dist.telepon');
            $this->db->join('mdistributor dist', 'dist.id = gp.iddistributor', 'left');
            return $this->db->get('tgudang_pemesanan gp')->row_array();
        }
    }
    public function print_po_detail($id)
    {
        $this->db->where('det.idpemesanan', $id);
        return $this->db->get('tgudang_pemesanan_detail det')->result();
    }
    public function detailbarangdraft()
    {
        $idpemesanan = $this->uri->segment(3);
        if ($idpemesanan) {
            $tabletipe  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
            $namatipe  = array(null,'Alkes','Implan','Obat','Logistik');

            $this->db->select('det.idtipe,det.idbarang,det.kuantitas,det.harga');
            $this->db->where('det.idpemesanan', $idpemesanan);
            $result = $this->db->get('tgudang_pemesanan_detail det')->result_array();
            foreach ($result as $r) {
                $item = array();

                $aksi = '';
                if($r['idtipe'] == 4) {
                    $aksi .= '<select name="konversisatuan" class="form-control">';
                    $aksi .= '<option value="0" selected>TIDAK</option>';
                    $aksi .= '</select>';
                } else {
                    $aksi .= '<select name="konversisatuan" class="form-control">';
                    $aksi .= '<option value="1">YA</option>';
                    $aksi .= '<option value="0" selected>TIDAK</option>';
                    $aksi .= '</select>';
                }

                $item[] = $namatipe[$r['idtipe']];
                $item[] = $this->getNamaBarang($r['idtipe'], $r['idbarang']);
                $item[] = $r['kuantitas'];
                $item[] = $aksi;
                $item[] = $r['idtipe'];
                $item[] = $r['idbarang'];
                $item[] = 0;
                $items[] = $item;
            }

            $data = array('data' => $items);
            $this->output->set_output(json_encode($data));
        }
    }
    public function getdistributor()
    {
        $this->db->select('id, nama as text');
        $this->db->where('status', 1);
        if ($this->input->get('q')) {
            $this->db->like('nama', $this->input->get('q'), 'BOTH');
        }
        $res = $this->db->get('mdistributor')->result();
        $this->output->set_output(json_encode($res));
    }

    public function save_konfirmasi_pemesanan_draft()
    {
        $tablebarang = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');
        $detailvalue = json_decode($this->input->post('detailvalue'));
        $iddistributor = $this->input->post('iddistributor');

        foreach ($detailvalue as $r) {
            $kuantitas          = $r[2];
            $idtipe             = $r[4];
            $idbarang           = $r[5];
            $konversisatuan     = $r[6];
            $harga              = 0;
            $opsisatuan         = 1;
            $tipepemesanan      = 1;

            if ($idtipe == 4) {
                $tipepemesanan = 2;
            }
            if ($konversisatuan == 1) {
                $this->db->where('id', $idbarang);
                $detailbarang = $this->db->get($tablebarang[$idtipe], 1)->row();
                $harga = $detailbarang->hargasatuanbesar;
                $jumlahsatuanbesar = $detailbarang->jumlahsatuanbesar;
                $var1 = $kuantitas/$jumlahsatuanbesar;

                $qty = 0;
                if ($var1 < 1) {
                    $qty = 1;
                } else {
                    $qty = ceil($var1);
                }
                
                $opsisatuan = 2;
                $kuantitas = $qty;
            } else {
                $this->db->where('id', $idbarang);
                $detailbarang = $this->db->get($tablebarang[$idtipe], 1)->row();
                $harga = $detailbarang->hargabeli;
                $opsisatuan = 1;
            }
            

            $this->db->where('iddistributor', $iddistributor);
            $this->db->where('tipepemesanan', $tipepemesanan);
            $this->db->where('stdraft !=', 1);
            $this->db->where('stdraft !=', 2);
            $this->db->where('status', 1);
            $pemesanan = $this->db->get('tgudang_pemesanan');
            if ($pemesanan->num_rows() > 0) {
                $rowpemesanan = $pemesanan->row();
                $totalharga = $kuantitas*$harga;
                $this->db->set('totalbarang', '`totalbarang` + '.$kuantitas, false);
                $this->db->set('totalharga', '`totalharga` + '.$totalharga, false);
                $this->db->where('id', $rowpemesanan->id);
                $updatehead = $this->db->update('tgudang_pemesanan');
                if ($updatehead) {
                    $this->db->where('idpemesanan', $rowpemesanan->id);
                    $this->db->where('idtipe', $idtipe);
                    $this->db->where('idbarang', $idbarang);
                    $this->db->where('opsisatuan', $opsisatuan);
                    $pemesanandetail = $this->db->get('tgudang_pemesanan_detail');
                    if ($pemesanandetail->num_rows() > 0) {
                        $rowpemesanandetail = $pemesanandetail->row();
                        $this->db->set('kuantitas', '`kuantitas` + '.$kuantitas, false);
                        $this->db->set('harga', $harga);
                        $this->db->set('opsisatuan', $opsisatuan);
                        $this->db->set('idtipe', $idtipe);
                        $this->db->set('idbarang', $idbarang);
                        $this->db->where('idtipe', $idtipe);
                        $this->db->where('idbarang', $idbarang);
                        $this->db->where('idpemesanan', $rowpemesanan->id);
                        $updatedetail = $this->db->update('tgudang_pemesanan_detail');
                        ($updatedetail) ? $status = true : $status = false;
                    } else {
                        $rowpemesanandetail = $pemesanandetail->row();
                        $this->db->set('kuantitas', $kuantitas);
                        $this->db->set('harga', $harga);
                        $this->db->set('opsisatuan', $opsisatuan);
                        $this->db->set('idpemesanan', $rowpemesanan->id);
                        $this->db->set('idtipe', $idtipe);
                        $this->db->set('idbarang', $idbarang);
                        $this->db->set('status', 1);
                        $insertdetail = $this->db->insert('tgudang_pemesanan_detail');
                        ($insertdetail) ? $status = true : $status = false;
                    }
                }
            } else {
                $totalharga = $kuantitas*$harga;
                $this->db->set('nopemesanan', '-');
                $this->db->set('tipepemesanan', $tipepemesanan);
                $this->db->set('tanggal', date('Y-m-d H:i:s'));
                $this->db->set('iddistributor', $iddistributor);
                $this->db->set('totalbarang', $kuantitas);
                $this->db->set('totalharga', $totalharga);
                $this->db->set('status', 1);
                $inserthead = $this->db->insert('tgudang_pemesanan');
                if ($inserthead) {
                    $insertid = $this->db->insert_id();
                    $this->db->set('idpemesanan', $insertid);
                    $this->db->set('opsisatuan', $opsisatuan);
                    $this->db->set('idtipe', $idtipe);
                    $this->db->set('idbarang', $idbarang);
                    $this->db->set('kuantitas', $kuantitas);
                    $this->db->set('harga', $harga);
                    $this->db->set('status', 1);
                    $insertdetail = $this->db->insert('tgudang_pemesanan_detail');
                    ($insertdetail) ? $status = true : $status = false;
                }
            }
        }

        $this->db->set('stdraft', 2);
        $this->db->where('id', $this->input->post('id'));
        $updatestatusdraft = $this->db->update('tgudang_pemesanan');
        ($updatestatusdraft) ? $status = true : $status = false;

        return $status;
    }
	public function save_konfirmasi_pemesanan_draft2()
    {
		// print_r($this->input->post());exit();
		$kuantitas_pesan=$this->input->post('kuantitas_pesan');
		$konversi=$this->input->post('konversi');
		$id_det=$this->input->post('id_det');
		foreach ($id_det as $key => $val) {			
			$data_update=array();
			$data_update['opsisatuan']=$konversi[$key];
			$data_update['kuantitas']=$kuantitas_pesan[$key];
		// print_r($data_update);exit();
			$data_update['st_konversi']='1';
			$this->db->where('id', $id_det[$key]);
			$this->db->update('tgudang_pemesanan_detail',$data_update); 
			
		}
		$this->db->set('userpemesanan', $this->session->userdata('user_name'));
		$this->db->set('tgl_pemesanan', date('Y-m-d H:i:s'));
		$this->db->set('stdraft', 0);
		$this->db->set('tipe_bayar', $this->input->post('tipe_bayar'));
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('tgudang_pemesanan');
        return true;
    }

    public function getsorted_distributor($mode){
        $idbarang=$this->input->post("idbarang");
        $idtipe=$this->input->post("idtipe");

        $qry="(SELECT m.id,CONCAT(m.nama,' (',IFNULL(r.total,0),')') as 'text'
            FROM mdistributor m 
            LEFT JOIN
            (
                SELECT p.iddistributor,COUNT(p.iddistributor) as 'total'
                FROM tgudang_pemesanan p 
                JOIN tgudang_pemesanan_detail d ON p.id=d.idpemesanan
                WHERE p.id IS NOT NULL
                AND d.idbarang ='".$idbarang."'  
                AND d.idtipe = '".$idtipe."'
                AND p.`status` NOT IN (0,1)
                GROUP BY p.iddistributor
            ) r ON r.iddistributor=m.id
            ORDER BY r.total DESC) tbl";
        $res=$this->db->get($qry)->result();
        if($mode=='json')
            $this->output->set_output(json_encode($res));
        else{
            $data = "<option value=''>Pilih Opsi</option>";
            foreach ($res as $r)
                $data .= "<option value='".$r->id."'>".$r->text."</option>";
            echo $data;
        }
    }
	public function view_unit() {
        $iddet             = $this->input->post('iddet');
        // $iddet             = '10';
		 if ($iddet) {
			
			$q="(SELECT H.id,H.no_up_permintaan,H.unit_peminta,U.nama as unit,H.kuantitas,H.opsisatuan,B.nama as namabarang,
				CASE WHEN H.opsisatuan='1' THEN sk.nama ELSE sb.nama END as satuan,CASE WHEN H.opsisatuan='1' THEN 'KECIL' ELSE 'BESAR' END as jenis_satuan
				,COALESCE(GS.stok,0) as stok
				from tgudang_pemesanan_history H
				LEFT JOIN munitpelayanan U ON U.id=H.unit_peminta
				LEFT JOIN view_barang B ON B.id=H.idbarang AND B.idtipe=H.idtipe
				LEFT JOIN msatuan sk ON sk.id=B.idsatuan
				LEFT JOIN msatuan sb ON sb.id=B.idsatuanbesar
				LEFT JOIN mgudang_stok GS ON GS.idunitpelayanan=H.unit_peminta AND GS.idtipe=H.idtipe AND GS.idbarang=H.idbarang
				WHERE H.iddet_gudang='$iddet')T ";
			$this->load->library('datatables'); 
			$this->datatables->select('no_up_permintaan,unit,kuantitas,opsisatuan,satuan,jenis_satuan,stok'); 
			$this->datatables->from($q); 
			$this->datatables->add_column("Actions", "<center></center>", "id");
			$this->datatables->unset_column('id');
			// echo $this->datatables->generate();
			
			 return print($this->datatables->generate());
			// $query=$this->db->query($q);
			// $res=$query->result();
			// $this->output->set_output(json_encode($res));
		 }
    }
}

/* End of file Tgudang_pemesanan_model.php */
/* Location: ./application/models/Tgudang_pemesanan_model.php */
