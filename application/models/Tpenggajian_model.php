<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpenggajian_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        // $this->db->where('tgaji.id', $id);
        // $query = $this->db->get('tgaji');
        // return $query->row();
		$q="SELECT M.nama nama_jenis, H.* from tgaji H
			LEFT JOIN mjenis_gaji M ON M.id=H.idjenis
			WHERE H.id='$id'";
			return $this->db->query($q)->row();
    }

    public function getDetailData($id)
    {
        // $this->db->select('tgaji_detail.*, mvariable_rekapan.nama, mvariable_rekapan.idtipe');
        // $this->db->join('mvariable_rekapan', 'mvariable_rekapan.id = tgaji_detail.idvariable');
        // $this->db->where('tgaji_detail.idgaji', $id);
        // $this->db->where('tgaji_detail.idsub', 0);
        // $query = $this->db->get('tgaji_detail');
        // return $query->result();
		$q="SELECT * FROM (
			SELECT D.id,D.idsetting,D.idgaji,D.idvariable,D.idheader,D.idsub,D.nama_variable as nama
			,CASE WHEN D.idheader='0' THEN CONCAT(M.nourut,'.',M.id) ELSE CONCAT(MH.nourut,'.', M.idheader,'-',M.nourut) END as urutan
			,D.nominal,RK.id as id_var
			FROM tgaji_detail D
			LEFT JOIN mvariable M ON M.id=D.idvariable
			LEFT JOIN mvariable MH ON MH.id=D.idheader 
			LEFT JOIN mrekap_var RK ON RK.idsetting=D.idsetting
			WHERE D.idgaji='$id'
			) T ORDER BY T.urutan ASC";
		return $this->db->query($q)->result();
    }
	function list_jenis(){
		$this->db->select('mjenis_gaji.*, ');
        $this->db->where('mjenis_gaji.status', 1);
        $query = $this->db->get('mjenis_gaji');
        return $query->result();
	}
    public function getReviewData($id)
    {
        // $this->db->select('tgaji_detail.*,
          // (
            // CASE WHEN tgaji_detail.idsub != 0 THEN
              // mvariable_rekapan_sub.nama
            // ELSE
              // mvariable_rekapan.nama
            // END
          // ) AS nama,
          // mvariable_rekapan.idtipe,
          // mvariable_rekapan.idsub AS subrekapan');
        // $this->db->join('mvariable_rekapan', 'mvariable_rekapan.id = tgaji_detail.idvariable', 'LEFT');
        // $this->db->join('mvariable_rekapan_sub', 'mvariable_rekapan_sub.id = tgaji_detail.idsub', 'LEFT');
        // $this->db->where('tgaji_detail.idgaji', $id);
        // $this->db->order_by('tgaji_detail.idvariable');
        // $this->db->order_by('tgaji_detail.idsub');
        // $query = $this->db->get('tgaji_detail');
        // return $query->result();
		$q="SELECT * FROM (
			SELECT D.id,D.idsetting,D.idgaji,D.idvariable,D.idheader,D.idsub,D.nama_variable as nama
			,CASE WHEN D.idheader='0' THEN CONCAT(M.nourut,'.',M.id) ELSE CONCAT(MH.nourut,'.', M.idheader,'-',M.nourut) END as urutan
			,D.nominal,RK.id as id_var
			FROM tgaji_detail D
			LEFT JOIN mvariable M ON M.id=D.idvariable
			LEFT JOIN mvariable MH ON MH.id=D.idheader 
			LEFT JOIN mrekap_var RK ON RK.idsetting=D.idsetting
			WHERE D.idgaji='$id'
			) T ORDER BY T.urutan ASC";
		return $this->db->query($q)->result();
    }

    public function getAkunData($id, $idtipeakun, $idtipevariable)
    {
        $this->db->select('tgaji_akun.*, makun_nomor.namaakun');
        $this->db->join('makun_nomor', 'makun_nomor.noakun = tgaji_akun.noakun');
        $this->db->where('tgaji_akun.idvariable', $id);
        $this->db->where('tgaji_akun.tipeakun', $idtipeakun);
        $this->db->where('tgaji_akun.tipevariable', $idtipevariable);
        $query = $this->db->get('tgaji_akun');
        return $query->result();
    }

    public function getVariableRekapan($idtipe)
    {
        if ($idtipe != 0) {
            $this->db->where('idtipe', $idtipe);
        }
        $this->db->where('status', 1);
        $query = $this->db->get('mvariable_rekapan');
        return $query->result();
    }

    public function saveData()
    {
        $this->periode  = YMDFormat($_POST['periode']);
        $this->tahun  = $_POST['tahun'];
        $this->bulan  = $_POST['bulan'];
        $this->subyek = $_POST['subyek'];
        $this->idjenis = $_POST['idjenis'];
		$idjenis=$_POST['idjenis'];
		$this->created_by  = $this->session->userdata('user_id');
        $this->created_date  = date('Y-m-d H:i:s');
        if ($this->db->insert('tgaji', $this)) {
            $idgaji = $this->db->insert_id();
			$q="SELECT S.idvariable,S.idsub,'0' as nominal,S.id as idsetting,S.`status`,M.nama as nama_variable,M.idheader,M.idtipe from mjenis_gaji_setting S
				LEFT JOIN mvariable M ON M.id=S.idvariable
				WHERE S.idjenis='$idjenis'";
			$row=$this->db->query($q)->result();
			foreach ($row as $r){
				$data_det=array(
					'idgaji' =>$idgaji,
					'idvariable' =>$r->idvariable,
					'idtipe' =>$r->idtipe,
					'idsetting' =>$r->idsetting,
					'idsub' =>$r->idsub,
					'nominal' =>0,
					'status' =>'1',
					'nama_variable' =>$r->nama_variable,
					'idheader' =>$r->idheader,
					'created_by' =>$this->session->userdata('user_id'),
					'created_date' => date('Y-m-d H:i:s'),
				);
				$this->db->insert('tgaji_detail',$data_det);
			}
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function saveDataDetail($idgaji, $idvariable)
    {
        $this->db->where('mvariable_rekapan_sub.idvariable', $idvariable);
        $query = $this->db->get('mvariable_rekapan_sub');
        $result = $query->result();

        foreach ($result as $row) {
            $data = array();
            $data['idgaji']    = $idgaji;
            $data['idvariable'] = $row->idvariable;
            $data['idsub']      = $row->id;

            $this->db->insert('tgaji_detail', $data);
        }

        return true;
    }

    public function updateData()
    {
        $this->tahun  = $_POST['tahun'];
        $this->bulan  = $_POST['bulan'];
        $this->subyek = $_POST['subyek'];
		$this->edited_by  = $this->session->userdata('user_id');
        $this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('tgaji', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
        return $status;
    }

    public function updateReview()
    {
        $detaillist = json_decode($_POST['detail_value']);
		$iddet=$this->input->post('iddet');
		$nominal=$this->input->post('nominal');
        foreach ($iddet as $i=>$val) {
            $data = array();
            $data['nominal'] = RemoveComma($nominal[$i]);

            $this->db->where('id', $val);
            $this->db->update('tgaji_detail', $data);
        }

        return true;
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('tgaji', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
