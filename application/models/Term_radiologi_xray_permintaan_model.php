<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Term_radiologi_xray_permintaan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_tujuan_radiologi()
    {
        $user_id = $this->session->userdata('login_ppa_id');

        $this->db->select('merm_pengaturan_tujuan_radiologi.*');
        $this->db->from('merm_pengaturan_tujuan_radiologi');
        $this->db->join('merm_pengaturan_tujuan_radiologi_user_akses', 'merm_pengaturan_tujuan_radiologi_user_akses.idpengaturan = merm_pengaturan_tujuan_radiologi.id');
        $this->db->where('merm_pengaturan_tujuan_radiologi.status', '1');
        $this->db->where('merm_pengaturan_tujuan_radiologi.tipe_layanan', '1');
        $this->db->where('merm_pengaturan_tujuan_radiologi_user_akses.iduser', $user_id);

        $query = $this->db->get();
        return $query->result();
    }

    public function getDataPendaftaranRawatJalan($pendaftaran_id)
    {
        $this->db->select('
            tpoliklinik_pendaftaran.nopendaftaran AS nomor_pendaftaran,
            tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
            tpoliklinik_pendaftaran.namapasien AS nama_pasien,
            tpoliklinik_pendaftaran.nik AS nomor_ktp,
            tpoliklinik_pendaftaran.alamatpasien,
            referensi_jenis_kelamin.ref AS jenis_kelamin,
            tpoliklinik_pendaftaran.tanggal_lahir,
            tpoliklinik_pendaftaran.umurhari AS umur_hari,
            tpoliklinik_pendaftaran.umurbulan AS umur_bulan,
            tpoliklinik_pendaftaran.umurtahun AS umur_tahun,
            mpasien_kelompok.nama AS kelompok_pasien,
            mrekanan.nama AS nama_asuransi,
            (CASE
                WHEN tpoliklinik_pendaftaran.idtipe = 1 THEN "Poliklinik"
                WHEN tpoliklinik_pendaftaran.idtipe = 2 THEN "Instalasi Gawat Darurat (IGD)"
            END) AS tipe_kunjungan,
            mpoliklinik.nama AS nama_poliklinik,
            mdokter_dpjp.nama AS dpjp'
        );
        $this->db->from('tpoliklinik_pendaftaran');
        $this->db->join('merm_referensi referensi_jenis_kelamin', 'referensi_jenis_kelamin.nilai = tpoliklinik_pendaftaran.jenis_kelamin AND referensi_jenis_kelamin.ref_head_id = 1', 'LEFT');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
        $this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mdokter mdokter_dpjp', 'mdokter_dpjp.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
        $this->db->where('tpoliklinik_pendaftaran.id', $pendaftaran_id);

        $query = $this->db->get();
        return $query->row();
    }

    public function getDataPendaftaranRawatInap($pendaftaran_id)
    {
        $this->db->select('
            trawatinap_pendaftaran.nopendaftaran AS nomor_pendaftaran,
            tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
            tpoliklinik_pendaftaran.namapasien AS nama_pasien,
            tpoliklinik_pendaftaran.nik AS nomor_ktp,
            tpoliklinik_pendaftaran.alamatpasien,
            referensi_jenis_kelamin.ref AS jenis_kelamin,
            tpoliklinik_pendaftaran.tanggal_lahir,
            tpoliklinik_pendaftaran.umurhari AS umur_hari,
            tpoliklinik_pendaftaran.umurbulan AS umur_bulan,
            tpoliklinik_pendaftaran.umurtahun AS umur_tahun,
            mpasien_kelompok.nama AS kelompok_pasien,
            mrekanan.nama AS nama_asuransi,
            (CASE
                WHEN trawatinap_pendaftaran.idtipe = 1 THEN "Rawat Inap"
                WHEN trawatinap_pendaftaran.idtipe = 2 THEN "One Day Surgery (ODS)"
            END) AS tipe_kunjungan,
            mpoliklinik.nama AS nama_poliklinik,
            mdokter_dpjp.nama AS dpjp'
        );
        $this->db->from('trawatinap_pendaftaran');
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('merm_referensi referensi_jenis_kelamin', 'referensi_jenis_kelamin.nilai = tpoliklinik_pendaftaran.jenis_kelamin AND referensi_jenis_kelamin.ref_head_id = 1', 'LEFT');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
        $this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mdokter mdokter_dpjp', 'mdokter_dpjp.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
        $this->db->where('trawatinap_pendaftaran.id', $pendaftaran_id);

        $query = $this->db->get();
        return $query->row();
    }

    public function getTransaksiRadiologi($id)
    {
        $this->db->select('term_radiologi_xray.*,
            trujukan_radiologi.norujukan AS nomor_transaksi_radiologi');
        $this->db->from('term_radiologi_xray');
        $this->db->join('trujukan_radiologi', 'trujukan_radiologi.id = term_radiologi_xray.rujukan_id', 'LEFT');
        $this->db->where('term_radiologi_xray.id', $id);

        $query = $this->db->get();
        return $query->row();
    }
}
