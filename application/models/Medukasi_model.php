<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Medukasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // public function getAll()
    // {
    //     $this->db->where('staktif', '1');
    //     $this->db->order_by('judul', 'ASC');
    //     $this->db->get('medukasi');
    //     return $this->db->last_query();
    // }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('medukasi');
        return $query->row();
    }

    public function saveData()
    {
        $this->judul 					= $_POST['judul'];
        $this->isi 			= $_POST['isi'];
        $this->staktif 			  = 1;
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');

        if ($this->db->insert('medukasi', $this)) {
            return $this->db->insert_id();
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->judul 					= $_POST['judul'];
        $this->isi 			= $_POST['isi'];
        $this->staktif 			  = 1;
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');

        if ($this->db->update('medukasi', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->staktif = 0;
		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');

        if ($this->db->update('medukasi', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	function refresh_image($id){
		$q="SELECT  H.* from medukasi_dokumen H

			WHERE H.idtransaksi='$id'";
		// print_r($q);exit();
		$row= $this->db->query($q)->result();
		$tabel='';
		$no=1;
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/medukasi_dokumen/'.$r->filename.'" target="_blank">'.$r->filename.'</a></td>';
			$tabel .='<td class="text-left">'.$r->keterangan.'</td>';
			$tabel .='<td class="text-left">'.$r->upload_by_nama.'-'.HumanDateLong($r->upload_date).'</td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/medukasi_dokumen/'.$r->filename.'" target="_blank" data-toggle="tooltip" title="Preview" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                    <button title="Hapus" class="btn btn-danger btn-sm" onclick="hapus_file('.$r->id.')"><i class="fa fa-trash-o"></i></a></td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		return $tabel;
	}
}
