<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_edukasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_assesmen_setting(){
		$q="SELECT * FROM setting_edukasi H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_label(){
		$q="SELECT * FROM setting_edukasi_label";
		return $this->db->query($q)->row_array();
	}

	function save_assesmen(){
		$id =1;
		$this->judul_header = $this->input->post('judul_header');
		$this->judul_footer = $this->input->post('judul_footer');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->lama_edit = $this->input->post('lama_edit');
		$this->orang_edit = $this->input->post('orang_edit');
		$this->st_hapus_catatan = $this->input->post('st_hapus_catatan');
		$this->lama_hapus = $this->input->post('lama_hapus');
		$this->orang_hapus = $this->input->post('orang_hapus');
		$this->st_duplikasi_catatan = $this->input->post('st_duplikasi_catatan');
		$this->lama_duplikasi = $this->input->post('lama_duplikasi');
		$this->orang_duplikasi = $this->input->post('orang_duplikasi');
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_edukasi', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_paraf(){
		$id =1;
		$this->ttd_petugas_st_auto = $this->input->post('ttd_petugas_st_auto');
		$this->ttd_petugas_judul_ina = $this->input->post('ttd_petugas_judul_ina');
		$this->ttd_petugas_judul_eng = $this->input->post('ttd_petugas_judul_eng');
		$this->ttd_petugas_ket_gambar_ina = $this->input->post('ttd_petugas_ket_gambar_ina');
		$this->ttd_petugas_ket_gambar_eng = $this->input->post('ttd_petugas_ket_gambar_eng');
		$this->ttd_petugas_nama_ina = $this->input->post('ttd_petugas_nama_ina');
		$this->ttd_petugas_nama_eng = $this->input->post('ttd_petugas_nama_eng');
		$this->ttd_sasaran_judul_ina = $this->input->post('ttd_sasaran_judul_ina');
		$this->ttd_sasaran_judul_eng = $this->input->post('ttd_sasaran_judul_eng');
		$this->ttd_sasaran_ket_gambar_ina = $this->input->post('ttd_sasaran_ket_gambar_ina');
		$this->ttd_sasaran_ket_gambar_eng = $this->input->post('ttd_sasaran_ket_gambar_eng');
		$this->ttd_sasaran_nama_ina = $this->input->post('ttd_sasaran_nama_ina');
		$this->ttd_sasaran_nama_eng = $this->input->post('ttd_sasaran_nama_eng');

		if ($this->db->update('setting_edukasi_label', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_defaultsave_default(){
		$id =1;
		$this->st_spesifik_edukasi = $this->input->post('st_spesifik_edukasi');
		if (!empty($this->input->post('st_kunci_default_edukasi'))){
		$this->st_kunci_default_edukasi =($this->input->post('st_spesifik_edukasi')=='0'?'0':$this->input->post('st_kunci_default_edukasi'));
			
		}else{
			$this->st_kunci_default_edukasi =0;
		}
		
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('msetting_edukasi_default', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


