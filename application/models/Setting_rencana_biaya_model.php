<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_rencana_biaya_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_assesmen_setting(){
		$q="SELECT * FROM setting_rencana_biaya H WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_label(){
		$q="SELECT * FROM setting_rencana_biaya_label";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_persen(){
		$q="SELECT * FROM setting_rencana_biaya_persen";
		return $this->db->query($q)->row_array();
	}

	function save_assesmen(){
		$id =1;
		$this->judul_header = $this->input->post('judul_header');
		$this->judul_footer = $this->input->post('judul_footer');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->lama_edit = $this->input->post('lama_edit');
		$this->orang_edit = $this->input->post('orang_edit');
		$this->st_hapus_catatan = $this->input->post('st_hapus_catatan');
		$this->lama_hapus = $this->input->post('lama_hapus');
		$this->orang_hapus = $this->input->post('orang_hapus');
		$this->st_duplikasi_catatan = $this->input->post('st_duplikasi_catatan');
		$this->lama_duplikasi = $this->input->post('lama_duplikasi');
		$this->orang_duplikasi = $this->input->post('orang_duplikasi');
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_rencana_biaya', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_persen(){
		$id =1;
		$this->dokter_anestesi = $this->input->post('dokter_anestesi');
		$this->assisten_operator = $this->input->post('assisten_operator');
		$this->assisten_anestesi = $this->input->post('assisten_anestesi');
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('setting_rencana_biaya_persen', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_label(){
		$id =1;
		
		$this->judul_per_ina = $this->input->post('judul_per_ina');
		$this->judul_per_eng = $this->input->post('judul_per_eng');
		$this->nama_pasien_ina = $this->input->post('nama_pasien_ina');
		$this->nama_pasien_eng = $this->input->post('nama_pasien_eng');
		$this->ttl_ina = $this->input->post('ttl_ina');
		$this->ttl_eng = $this->input->post('ttl_eng');
		$this->diagnosa_per_ina = $this->input->post('diagnosa_per_ina');
		$this->diagnosa_per_eng = $this->input->post('diagnosa_per_eng');
		$this->nama_tindakan_ina = $this->input->post('nama_tindakan_ina');
		$this->nama_tindakan_eng = $this->input->post('nama_tindakan_eng');
		$this->catatan_ina = $this->input->post('catatan_ina');
		$this->catatan_eng = $this->input->post('catatan_eng');
		$this->tipe_ina = $this->input->post('tipe_ina');
		$this->tipe_eng = $this->input->post('tipe_eng');
		$this->icu_ina = $this->input->post('icu_ina');
		$this->icu_eng = $this->input->post('icu_eng');
		$this->cito_ina = $this->input->post('cito_ina');
		$this->cito_eng = $this->input->post('cito_eng');
		$this->dokter_ina = $this->input->post('dokter_ina');
		$this->dokter_eng = $this->input->post('dokter_eng');
		$this->rencana_implant_ina = $this->input->post('rencana_implant_ina');
		$this->rencana_implant_eng = $this->input->post('rencana_implant_eng');
		$this->table_no_ina = $this->input->post('table_no_ina');
		$this->table_no_eng = $this->input->post('table_no_eng');
		$this->table_alat_ina = $this->input->post('table_alat_ina');
		$this->table_alat_eng = $this->input->post('table_alat_eng');
		$this->table_ukuran_ina = $this->input->post('table_ukuran_ina');
		$this->table_ukuran_eng = $this->input->post('table_ukuran_eng');
		$this->table_merk_ina = $this->input->post('table_merk_ina');
		$this->table_merk_eng = $this->input->post('table_merk_eng');
		$this->table_catatan_ina = $this->input->post('table_catatan_ina');
		$this->table_catatan_eng = $this->input->post('table_catatan_eng');
		$this->table_jumlah_ina = $this->input->post('table_jumlah_ina');
		$this->table_jumlah_eng = $this->input->post('table_jumlah_eng');
		$this->table_biaya_ina = $this->input->post('table_biaya_ina');
		$this->table_biaya_eng = $this->input->post('table_biaya_eng');
		$this->table_aksi_ina = $this->input->post('table_aksi_ina');
		$this->table_aksi_eng = $this->input->post('table_aksi_eng');
		$this->table_footer_ina = $this->input->post('table_footer_ina');
		$this->table_footer_eng = $this->input->post('table_footer_eng');
		$this->kel_op_ina = $this->input->post('kel_op_ina');
		$this->kel_op_eng = $this->input->post('kel_op_eng');
		$this->jenis_ina = $this->input->post('jenis_ina');
		$this->jenis_eng = $this->input->post('jenis_eng');
		$this->lama_ina = $this->input->post('lama_ina');
		$this->lama_eng = $this->input->post('lama_eng');
		$this->kel_pasien_ina = $this->input->post('kel_pasien_ina');
		$this->kel_pasien_eng = $this->input->post('kel_pasien_eng');
		$this->asuransi_ina = $this->input->post('asuransi_ina');
		$this->asuransi_eng = $this->input->post('asuransi_eng');
		$this->section_biaya_ina = $this->input->post('section_biaya_ina');
		$this->section_biaya_eng = $this->input->post('section_biaya_eng');
		$this->kamar_ina = $this->input->post('kamar_ina');
		$this->kamar_eng = $this->input->post('kamar_eng');
		$this->pakai_obat_ina = $this->input->post('pakai_obat_ina');
		$this->pakai_obat_eng = $this->input->post('pakai_obat_eng');
		$this->biaya_implan_ina = $this->input->post('biaya_implan_ina');
		$this->biaya_implan_eng = $this->input->post('biaya_implan_eng');
		$this->jasa_dokter_operator_ina = $this->input->post('jasa_dokter_operator_ina');
		$this->jasa_dokter_operator_eng = $this->input->post('jasa_dokter_operator_eng');
		$this->jasa_dokter_anes_ina = $this->input->post('jasa_dokter_anes_ina');
		$this->jasa_dokter_anes_eng = $this->input->post('jasa_dokter_anes_eng');
		$this->jasa_asisten_ina = $this->input->post('jasa_asisten_ina');
		$this->jasa_asisten_eng = $this->input->post('jasa_asisten_eng');
		$this->total_biaya_ina = $this->input->post('total_biaya_ina');
		$this->total_biaya_eng = $this->input->post('total_biaya_eng');
		$this->section_biaya_ranap_ina = $this->input->post('section_biaya_ranap_ina');
		$this->section_biaya_ranap_eng = $this->input->post('section_biaya_ranap_eng');
		$this->biaya_ranap_ina = $this->input->post('biaya_ranap_ina');
		$this->biaya_ranap_eng = $this->input->post('biaya_ranap_eng');
		$this->biaya_ruang_ina = $this->input->post('biaya_ruang_ina');
		$this->biaya_ruang_eng = $this->input->post('biaya_ruang_eng');
		$this->biaya_obat_ina = $this->input->post('biaya_obat_ina');
		$this->biaya_obat_eng = $this->input->post('biaya_obat_eng');
		$this->biaya_penunjang_ina = $this->input->post('biaya_penunjang_ina');
		$this->biaya_penunjang_eng = $this->input->post('biaya_penunjang_eng');
		$this->biaya_visit_ina = $this->input->post('biaya_visit_ina');
		$this->biaya_visit_eng = $this->input->post('biaya_visit_eng');
		$this->biaya_icu_ina = $this->input->post('biaya_icu_ina');
		$this->biaya_icu_eng = $this->input->post('biaya_icu_eng');
		$this->total_biaya_ranap_ina = $this->input->post('total_biaya_ranap_ina');
		$this->total_biaya_ranap_eng = $this->input->post('total_biaya_ranap_eng');
		$this->total_estimasi_biaya_ina = $this->input->post('total_estimasi_biaya_ina');
		$this->total_estimasi_biaya_eng = $this->input->post('total_estimasi_biaya_eng');
		$this->yang_menjelaskan_ina = $this->input->post('yang_menjelaskan_ina');
		$this->yang_menjelaskan_eng = $this->input->post('yang_menjelaskan_eng');
		$this->petugas_biling_ina = $this->input->post('petugas_biling_ina');
		$this->petugas_biling_eng = $this->input->post('petugas_biling_eng');
		$this->dpjp_ina = $this->input->post('dpjp_ina');
		$this->dpjp_eng = $this->input->post('dpjp_eng');
		$this->ttd_pasien_ina = $this->input->post('ttd_pasien_ina');
		$this->ttd_pasien_eng	 = $this->input->post('ttd_pasien_eng');
		$this->ket_no_ina = $this->input->post('ket_no_ina');
		$this->ket_no_eng = $this->input->post('ket_no_eng');
		$this->ket_penjelasan_ina = $this->input->post('ket_penjelasan_ina');
		$this->ket_penjelasan_eng = $this->input->post('ket_penjelasan_eng');
		$this->ket_paraf_ina = $this->input->post('ket_paraf_ina');
		$this->ket_paraf_eng = $this->input->post('ket_paraf_eng');
		$this->kelas_diambil_ina = $this->input->post('kelas_diambil_ina');
		$this->kelas_diambil_eng = $this->input->post('kelas_diambil_eng');

		if ($this->db->update('setting_rencana_biaya_label', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_defaultsave_default(){
		$id =1;
		$this->st_spesifik_rencana_biaya = $this->input->post('st_spesifik_rencana_biaya');
		if (!empty($this->input->post('st_kunci_default_rencana_biaya'))){
		$this->st_kunci_default_rencana_biaya =($this->input->post('st_spesifik_rencana_biaya')=='0'?'0':$this->input->post('st_kunci_default_rencana_biaya'));
			
		}else{
			$this->st_kunci_default_rencana_biaya =0;
		}
		
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('msetting_rencana_biaya_default', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


