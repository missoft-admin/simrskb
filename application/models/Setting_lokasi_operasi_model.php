<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_lokasi_operasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_assesmen_ri_setting(){
		$q="SELECT * FROM setting_lokasi_operasi H LIMIT 1";
		return $this->db->query($q)->row_array();
	}
	function save_assesmen_ri(){
		// $id =1;
		$this->judul_header_ina = $this->input->post('judul_header_ina');
		$this->judul_header_eng = $this->input->post('judul_header_eng');
		$this->judul_footer_ina = $this->input->post('judul_footer_ina');
		$this->judul_footer_eng = $this->input->post('judul_footer_eng');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->lama_edit = $this->input->post('lama_edit');
		$this->orang_edit = $this->input->post('orang_edit');
		$this->st_hapus_catatan = $this->input->post('st_hapus_catatan');
		$this->lama_hapus = $this->input->post('lama_hapus');
		$this->orang_hapus = $this->input->post('orang_hapus');
		$this->st_duplikasi_catatan = $this->input->post('st_duplikasi_catatan');
		$this->lama_duplikasi = $this->input->post('lama_duplikasi');
		$this->orang_duplikasi = $this->input->post('orang_duplikasi');
		
		// $this->edited_by = $this->session->userdata('user_id');
		// $this->edited_date = date('Y-m-d H:i:s');
		// $this->db->where('id', $id);
		
		
		$this->diagnosa_ina = $this->input->post('diagnosa_ina');
		$this->diagnosa_eng = $this->input->post('diagnosa_eng');
		$this->rencana_tindakan_ina = $this->input->post('rencana_tindakan_ina');
		$this->rencana_tindakan_eng = $this->input->post('rencana_tindakan_eng');
		$this->tanggal_tindakan_ina = $this->input->post('tanggal_tindakan_ina');
		$this->tanggal_tindakan_eng = $this->input->post('tanggal_tindakan_eng');
		$this->paragraf_1_ina = $this->input->post('paragraf_1_ina');
		$this->paragraf_1_eng = $this->input->post('paragraf_1_eng');
		$this->pasien_ina = $this->input->post('pasien_ina');
		$this->pasien_eng = $this->input->post('pasien_eng');
		$this->dokter_ina = $this->input->post('dokter_ina');
		$this->dokter_eng = $this->input->post('dokter_eng');
	
		if ($this->db->update('setting_lokasi_operasi', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


