<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mtriage_pemeriksaan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	public function getSpecified($id)
    {
        $q="SELECT 
			*
			from mtriage_pemeriksaan M
					
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	
    public function saveData()
    {
		
        // $this->nourut 	= $_POST['nourut'];		
        $this->nama 	= $_POST['nama'];		
        $this->staktif 	= 1;		
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
        if ($this->db->insert('mtriage_pemeriksaan', $this)) {
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
		
		$template_id=$_POST['id'];
        $this->nama 	= $_POST['nama'];	
		// $this->nourut 	= $_POST['nourut'];		
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
        if ($this->db->update('mtriage_pemeriksaan', $this, array('id' => $_POST['id']))) {
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	
	
    public function softDelete($id)
    {
        $this->staktif = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mtriage_pemeriksaan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function pilih($id)
    {
		$q="UPDATE mtriage_pemeriksaan SET st_default='0' WHERE st_default='1'";
		$this->db->query($q);
        $this->st_default = 1;		
		
        if ($this->db->update('mtriage_pemeriksaan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->staktif = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('mtriage_pemeriksaan', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
