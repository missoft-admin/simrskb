<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tvalidasi_pendapatan_ranap_model extends CI_Model
{
   public function getHeader($id){
	   $q="SELECT 
			H.id as idvalidasi,H.tanggal_transaksi,H.tanggal_pendaftaran,H.no_medrec,H.pendaftaran_id as idpendaftaran,CONCAT(IFNULL(H.title,''),CASE WHEN H.title IS NULL THEN '' ELSE '. ' END,H.namapasien) as  namapasien,H.notransaksi
			,H.idkelompok,H.namakelompok,H.idrekanan,H.namarekanan,R.nama as ruangan,K.nama as kelas,B.nama as bed,H.dokter,H.idtipe
			,H.diskonpersen,H.total,H.nominal_diskon,H.nominal_round,H.totalharusdibayar,H.deposit,H.st_posting,H.st_gabungan,H.idgroup_gabungan,H.idgroup_diskon
			,H.nominal_gabungan,H.nominal_diskon,H.nominal_round,H.idgroup_round,H.tipe_trx
			FROM tvalidasi_pendapatan_ranap H
			LEFT JOIN mruangan R ON R.id=H.idruangan
			LEFT JOIN mbed B ON B.id=H.idbed
			LEFT JOIN mkelas K ON K.id=H.idkelas
			WHERE H.id='$id'";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function getAdmRanap($id){
	   $q="SELECT *FROM tvalidasi_pendapatan_ranap_adm H WHERE H.idvalidasi='$id'";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getTindakanRanap($id,$idjenis){
	   $q="SELECT *FROM tvalidasi_pendapatan_ranap_tindakan H WHERE H.idvalidasi='$id' AND H.idjenis='$idjenis'";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getObatAlkesRanap($id,$idtipe,$asal=''){
	   $where='';
	   if ($asal !=''){
		   $where =" AND  H.asal IN (".$asal.")";
	   }
	   $q="SELECT *FROM tvalidasi_pendapatan_ranap_obat_alkes H WHERE H.idvalidasi='$id' AND H.idtipe='$idtipe' ".$where;
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getObatAlkesIGD($id,$idtipe,$asal=''){
	   $where='';
	   if ($asal !=''){
		   $where =" AND  H.asal IN (".$asal.")";
	   }
	   $q="SELECT *FROM tvalidasi_pendapatan_ranap_obat_alkes_igd H WHERE H.idvalidasi='$id' AND H.idtipe='$idtipe' ".$where;
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getObatKO($id,$idtipe,$jenis){
	   
	   $q="SELECT *FROM tvalidasi_pendapatan_ranap_obat_alkes_ko H WHERE H.idvalidasi='$id' AND H.idtipe='$idtipe' AND H.jenis='$jenis' ";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getDokterAnastesi($id){
	   
	   $q="SELECT *FROM tvalidasi_pendapatan_ranap_dokter_ko H WHERE H.idvalidasi='$id' ";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getDokterAsisten($id){
	   
	   $q="SELECT *FROM tvalidasi_pendapatan_ranap_dokter_ko_asisten H WHERE H.idvalidasi='$id' ";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_pembayaran($idvalidasi)
    {
        $q="SELECT H.id,H.idvalidasi,H.idbayar_id,H.metode_nama as jenis,H.idmetode, CONCAT(H.metode_nama,' ',IFNULL(H.bank,' ')) metode_nama,H.nominal_bayar as nominal 
			,'tvalidasi_pendapatan_ranap_bayar' as reff,'1' as ref_id
			FROM tvalidasi_pendapatan_ranap_bayar H
			WHERE H.idvalidasi='$idvalidasi'
			UNION ALL
			SELECT H.id,H.idvalidasi,H.idbayar_id,'Non Tunai ' as jenis,H.idmetode,H.metode_nama,H.nominal_bayar as nominal 
			,'tvalidasi_pendapatan_ranap_bayar_non_tunai' as reff,2 as ref_id
			FROM tvalidasi_pendapatan_ranap_bayar_non_tunai H 
			WHERE H.idvalidasi='$idvalidasi'";
			// print_r($q);exit();
        $query = $this->db->query($q);
        return $query->result();
    }
	function GetStatusPosting($idvalidasi){
	   $q="SELECT H.st_posting FROM `tvalidasi_pendapatan_ranap` H WHERE H.id='$idvalidasi'";
	   $st=$this->db->query($q)->row('st_posting');
	   if ($st=='1'){
		   return 'disabled';
	   }else{
		   return '';
		   
	   }
   }
    
   public function list_group(){
	   $q="SELECT H.id,H.nama,A.noakun,A.namaakun FROM mgroup_pembayaran H
			LEFT JOIN mgroup_pembayaran_detail D ON D.idgroup=H.id
			LEFT JOIN makun_nomor A ON A.id=D.idakun";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_akun(){
	   $q="SELECT * FROM makun_nomor A";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function get_edit_detail_tindakan($id){
	   $q="SELECT * FROM `tvalidasi_pendapatan_ranap_tindakan` H WHERE H.id='$id'";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function list_obat_ranap($idvalidasi,$tipe,$asal){
	   $where='';
	   if ($asal != '' ){
		   $where=" AND H.asal IN (".$asal.")";
	   }
	   $q="SELECT *FROM tvalidasi_pendapatan_ranap_obat_alkes H
			WHERE H.idvalidasi='$idvalidasi' AND H.idtipe='$tipe' ".$where;
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_obat_ranap_racikan($idvalidasi,$tipe,$asal){
	   $where='';
	   if ($asal != '' ){
		   $where=" AND H.asal IN (".$asal.")";
	   }
	   $q="SELECT H.namabarang as namaracikan,D.* FROM tvalidasi_pendapatan_ranap_obat_alkes H
			LEFT JOIN tvalidasi_pendapatan_ranap_obat_alkes_racikan D ON D.idracikan=H.id
			WHERE H.statusracikan='1' AND H.idvalidasi='$idvalidasi' AND H.idtipe='$tipe' ".$where;
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_obat_igd($idvalidasi,$tipe,$asal){
	   $where='';
	   if ($asal != '' ){
		   $where=" AND H.asal IN (".$asal.")";
	   }
	   $q="SELECT *FROM tvalidasi_pendapatan_ranap_obat_alkes_igd H
			WHERE H.idvalidasi='$idvalidasi' AND H.idtipe='$tipe' ".$where;
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_obat_igd_racikan($idvalidasi,$tipe,$asal){
	   $where='';
	   // if ($asal != '' ){
		   // $where=" AND H.asal IN (".$asal.")";
	   // }
	   $q="SELECT H.namabarang as namaracikan,D.* FROM tvalidasi_pendapatan_ranap_obat_alkes_igd H
			LEFT JOIN tvalidasi_pendapatan_ranap_obat_alkes_igd_racikan D ON D.idracikan=H.id
			WHERE H.statusracikan='1' AND H.idvalidasi='$idvalidasi' AND H.idtipe='$tipe' ".$where;
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_obat_ko($idvalidasi,$tipe,$jenis){
	   $where='';
	   if ($jenis != '' ){
		   $where=" AND H.jenis='$jenis'";
	   }
	   $q="SELECT *FROM tvalidasi_pendapatan_ranap_obat_alkes_ko H
			WHERE H.idvalidasi='$idvalidasi' AND H.idtipe='$tipe' ".$where;
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_dokter_anesthesi($idvalidasi){
	  
	   $q="SELECT *FROM tvalidasi_pendapatan_ranap_dokter_ko H
			WHERE H.idvalidasi='$idvalidasi'";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_asisten($idvalidasi){
	  
	   $q="SELECT *FROM tvalidasi_pendapatan_ranap_dokter_ko_asisten H
			WHERE H.idvalidasi='$idvalidasi'";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_adm($idvalidasi){
	  
	   $q="SELECT *FROM tvalidasi_pendapatan_ranap_adm H
			WHERE H.idvalidasi='$idvalidasi'";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_tunai($idvalidasi)
    {
        $q="SELECT H.id,H.idvalidasi,H.idbayar_id,H.metode_nama as jenis,H.idmetode, CONCAT(H.metode_nama,' ',IFNULL(H.bank,'')) metode_nama,H.nominal_bayar as nominal 
			,'tvalidasi_pendapatan_ranap_bayar' as reff,'1' as ref_id,H.idakun
			FROM tvalidasi_pendapatan_ranap_bayar H
			WHERE H.idvalidasi='$idvalidasi'
			";
			// print_r($q);exit();
        $query = $this->db->query($q);
        return $query->result();
    }
	public function list_non_tunai($idvalidasi)
    {
        $q="
			SELECT H.id,H.idvalidasi,H.idbayar_id,'Non Tunai ' as jenis,H.idmetode,H.metode_nama,H.nominal_bayar as nominal 
			,'tvalidasi_pendapatan_ranap_bayar_non_tunai' as reff,2 as ref_id,H.group_akun
			FROM tvalidasi_pendapatan_ranap_bayar_non_tunai H 
			WHERE H.idvalidasi='$idvalidasi'";
			// print_r($q);exit();
        $query = $this->db->query($q);
        return $query->result();
	}
	public function list_group_null($id){
	   $where='';
	   $query=$this->query_ringkasan($id);
	   $q="SELECT T.idvalidasi,T.idakun,A.noakun,A.namaakun
				,(CASE WHEN T.posisi_akun='D' THEN IFNULL(T.nominal,0) ELSE 0 END) as debet 
				,(CASE WHEN T.posisi_akun='K' THEN IFNULL(T.nominal,0) ELSE 0 END) as kredit
				,T.ket as keterangan
				FROM (".$query.") T 
				LEFT JOIN makun_nomor A ON A.id=T.idakun
				WHERE T.nominal !='0' AND T.idakun IS NULL ".$where;
		$query=$this->db->query($q);
		return $query->result();
   }
   
   function query_ringkasan($idvalidasi){
	   
	   $q="
				SELECT H.idvalidasi,H.idakun,H.posisi_akun,H.nominal,CONCAT('DEPOSIT') as ket,null as idgroup FROM `tvalidasi_pendapatan_ranap_bayar_deposit` H
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.id as idvalidasi,H.idakun_round as idakun,H.posisi_round as posisi_akun,H.nominal_round as nominal,CONCAT('ROUND UP KASIR RAWAT JALAN') as ket,H.idgroup_round as idgroup
				FROM tvalidasi_pendapatan_ranap H WHERE H.id='$idvalidasi'
				UNION ALL
				SELECT H.id as idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,H.nominal_diskon as nominal,CONCAT('DISKON RAWAT JALAN') as ket,H.idgroup_diskon as idgroup
				FROM tvalidasi_pendapatan_ranap H WHERE H.id='$idvalidasi'
				UNION ALL
				SELECT H.id as idvalidasi,H.idakun_gabungan as idakun,H.posisi_gabungan as posisi_akun,H.nominal_gabungan as nominal,CONCAT('GABUNGAN RAWAT JALAN') as ket,H.idgroup_gabungan as idgroup
				FROM tvalidasi_pendapatan_ranap H WHERE H.id='$idvalidasi' AND H.st_gabungan='1'
				UNION ALL
				SELECT H.idvalidasi,H.idakun_tarif as idakun,'K' as posisi_akun,H.total as nominal,'ADMIN RAWAT INAP' as ket,H.group_tarif as idgroup
				FROM tvalidasi_pendapatan_ranap_adm H WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idvalidasi,H.idakun_diskon as idakun,'D' as posisi_akun,H.diskon_rp as nominal,'DISKON ADMIN RAWAT INAP' as ket,H.group_diskon as idgroup
				FROM tvalidasi_pendapatan_ranap_adm H WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idvalidasi,H.idakun as idakun,H.posisi_akun as posisi_akun,H.nominal_bayar as nominal,CONCAT('PEMBAYARAN ',H.metode_nama) as ket,null as idgroup
				FROM tvalidasi_pendapatan_ranap_bayar H WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idvalidasi,H.idakun as idakun,H.posisi_akun as posisi_akun,H.nominal_bayar as nominal,CONCAT('PEMBAYARAN NON TUNAI',H.metode_nama) as ket,H.group_akun as idgroup
				FROM tvalidasi_pendapatan_ranap_bayar_non_tunai H WHERE H.idvalidasi='$idvalidasi'

				UNION ALL
				SELECT H.idvalidasi,H.idakun_jasasarana as idakun,CASE WHEN D.tipe_refund IS NULL THEN 'K' ELSE 'D' END as posisi_akun,H.jasasarana*H.kuantitas as nominal,CONCAT('Jasa Sarana ',R.ref_jenis,' ',H.namatarif) as ket,H.group_jasasarana as idgroup
				FROM tvalidasi_pendapatan_ranap_tindakan H 
				LEFT JOIN tvalidasi_pendapatan_ranap D ON D.id=H.idvalidasi
				LEFT JOIN tvalidasi_pendapatan_ranap_01_ref R ON R.id=H.idjenis
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idvalidasi,H.idakun_jasapelayanan as idakun,CASE WHEN D.tipe_refund IS NULL THEN 'K' ELSE 'D' END as posisi_akun,H.jasapelayanan*H.kuantitas as nominal,CONCAT('Jasa Pelayanan ',R.ref_jenis,' ',H.namatarif) as ket,H.group_jasapelayanan as idgroup
				FROM tvalidasi_pendapatan_ranap_tindakan H 
				LEFT JOIN tvalidasi_pendapatan_ranap D ON D.id=H.idvalidasi
				LEFT JOIN tvalidasi_pendapatan_ranap_01_ref R ON R.id=H.idjenis
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idvalidasi,H.idakun_bhp as idakun,CASE WHEN D.tipe_refund IS NULL THEN 'K' ELSE 'D' END as posisi_akun,H.bhp*H.kuantitas as nominal,CONCAT('BHP ',R.ref_jenis,' ',H.namatarif) as ket,H.group_bhp as idgroup
				FROM tvalidasi_pendapatan_ranap_tindakan H 
				LEFT JOIN tvalidasi_pendapatan_ranap D ON D.id=H.idvalidasi
				LEFT JOIN tvalidasi_pendapatan_ranap_01_ref R ON R.id=H.idjenis
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idvalidasi,H.idakun_biayaperawatan as idakun,CASE WHEN D.tipe_refund IS NULL THEN 'K' ELSE 'D' END as posisi_akun,H.biayaperawatan*H.kuantitas as nominal,CONCAT('Biaya Perawatan ',R.ref_jenis,' ',H.namatarif) as ket,H.group_biayaperawatan as idgroup
				FROM tvalidasi_pendapatan_ranap_tindakan H 
				LEFT JOIN tvalidasi_pendapatan_ranap D ON D.id=H.idvalidasi
				LEFT JOIN tvalidasi_pendapatan_ranap_01_ref R ON R.id=H.idjenis
				WHERE H.idvalidasi='$idvalidasi'

				UNION ALL
				SELECT H.idvalidasi,H.idakun_jasasarana_diskon as idakun,CASE WHEN D.tipe_refund IS NOT  NULL THEN 'K' ELSE 'D' END as posisi_akun,H.jasasarana_diskon*H.kuantitas as nominal,CONCAT('Diskon Jasa Sarana ',R.ref_jenis,' ',H.namatarif) as ket,H.group_jasasarana_diskon as idgroup
				FROM tvalidasi_pendapatan_ranap_tindakan H 
				LEFT JOIN tvalidasi_pendapatan_ranap D ON D.id=H.idvalidasi
				LEFT JOIN tvalidasi_pendapatan_ranap_01_ref R ON R.id=H.idjenis
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idvalidasi,H.idakun_jasapelayanan_diskon as idakun,CASE WHEN D.tipe_refund IS NOT  NULL THEN 'K' ELSE 'D' END as posisi_akun,H.jasapelayanan_diskon*H.kuantitas as nominal,CONCAT('Diskon Jasa Pelayanan ',R.ref_jenis,' ',H.namatarif) as ket,H.group_jasapelayanan_diskon as idgroup
				FROM tvalidasi_pendapatan_ranap_tindakan H 
				LEFT JOIN tvalidasi_pendapatan_ranap D ON D.id=H.idvalidasi
				LEFT JOIN tvalidasi_pendapatan_ranap_01_ref R ON R.id=H.idjenis
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idvalidasi,H.idakun_bhp_diskon as idakun,CASE WHEN D.tipe_refund IS NOT  NULL THEN 'K' ELSE 'D' END as posisi_akun,H.bhp_diskon*H.kuantitas as nominal,CONCAT('Diskon BHP ',R.ref_jenis,' ',H.namatarif) as ket,H.group_bhp_diskon as idgroup
				FROM tvalidasi_pendapatan_ranap_tindakan H 
				LEFT JOIN tvalidasi_pendapatan_ranap D ON D.id=H.idvalidasi
				LEFT JOIN tvalidasi_pendapatan_ranap_01_ref R ON R.id=H.idjenis
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idvalidasi,H.idakun_biayaperawatan_diskon as idakun,CASE WHEN D.tipe_refund IS NOT  NULL THEN 'K' ELSE 'D' END as posisi_akun,H.biayaperawatan_diskon*H.kuantitas as nominal,CONCAT('Diskon Biaya Perawatan ',R.ref_jenis,' ',H.namatarif) as ket,H.group_biayaperawatan_diskon as idgroup
				FROM tvalidasi_pendapatan_ranap_tindakan H 
				LEFT JOIN tvalidasi_pendapatan_ranap D ON D.id=H.idvalidasi
				LEFT JOIN tvalidasi_pendapatan_ranap_01_ref R ON R.id=H.idjenis
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idvalidasi,H.idakun_diskon_all as idakun,CASE WHEN  D.tipe_refund IS NULL  THEN 'D' ELSE 'K' END as posisi_akun,H.diskon as nominal,CONCAT('DISKON ',R.ref_jenis,' ',H.namatarif) as ket,H.group_diskon_all as idgroup
				FROM tvalidasi_pendapatan_ranap_tindakan H 
				LEFT JOIN tvalidasi_pendapatan_ranap D ON D.id=H.idvalidasi
				LEFT JOIN tvalidasi_pendapatan_ranap_01_ref R ON R.id=H.idjenis
				WHERE H.idvalidasi='$idvalidasi' AND H.diskon > 0

				UNION ALL
				SELECT H.idvalidasi,H.idakun_tarif as idakun,'K' as posisi_akun,H.total as nominal,H.namatarif as ket,H.group_tarif as idgroup
				FROM tvalidasi_pendapatan_ranap_dokter_ko H 						
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idvalidasi,H.idakun_diskon as idakun,'D' as posisi_akun,H.diskon_rp as nominal,CONCAT('DISKON ',H.namatarif) as ket,H.group_diskon as idgroup
				FROM tvalidasi_pendapatan_ranap_dokter_ko H 						
				WHERE H.idvalidasi='$idvalidasi'

				UNION ALL
				SELECT H.idvalidasi,H.idakun_tarif as idakun,'K' as posisi_akun,H.total as nominal,H.namatarif as ket,H.group_tarif as idgroup
				FROM tvalidasi_pendapatan_ranap_dokter_ko_asisten H 						
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idvalidasi,H.idakun_diskon as idakun,'D' as posisi_akun,H.diskon_rp as nominal,CONCAT('DISKON ',H.namatarif) as ket,H.group_diskon as idgroup
				FROM tvalidasi_pendapatan_ranap_dokter_ko_asisten H 						
				WHERE H.idvalidasi='$idvalidasi'

				UNION ALL
				SELECT H.idvalidasi,H.idakun_penjualan as idakun,H.posisi_penjualan as posisi_akun,(H.hargajual * H.kuantitas) as nominal,CONCAT('Penjualan ',H.namatipe,' Rawat Inap ',CASE WHEN H.tipe_farmasi='1' THEN ' Farmasi' ELSE ' ' END) as ket,H.idgroup_penjualan as idgroup
				FROM tvalidasi_pendapatan_ranap_obat_alkes H 						
				WHERE H.idvalidasi='$idvalidasi' AND H.statusracikan='0'
				UNION ALL
				SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,(H.diskon_rp * H.kuantitas) as nominal,CONCAT('Diskon Penjualan ',H.namatipe,' Rawat Inap ',CASE WHEN H.tipe_farmasi='1' THEN ' Farmasi' ELSE ' ' END) as ket,H.idgroup_diskon as idgroup
				FROM tvalidasi_pendapatan_ranap_obat_alkes H 						
				WHERE H.idvalidasi='$idvalidasi' AND H.statusracikan='0' AND H.diskon_rp > 0
				UNION ALL
				SELECT H.idvalidasi,H.idakun_tuslah as idakun,H.posisi_tuslah as posisi_akun,H.tuslah as nominal,CONCAT('Tuslah Penjualan ',H.namatipe,' Rawat Inap ',CASE WHEN H.tipe_farmasi='1' THEN ' Farmasi' ELSE ' ' END) as ket,H.idgroup_tuslah as idgroup
				FROM tvalidasi_pendapatan_ranap_obat_alkes H 						
				WHERE H.idvalidasi='$idvalidasi' AND H.tuslah > 0						
				UNION ALL
				SELECT H.idvalidasi,H.idakun_penjualan as idakun,H.posisi_penjualan as posisi_akun,(H.hargajual * H.kuantitas) as nominal,CONCAT('Penjualan Racikan ',H.namatipe,' Rawat Inap ') as ket,H.idgroup_penjualan as idgroup
				FROM tvalidasi_pendapatan_ranap_obat_alkes_racikan H 						
				WHERE H.idvalidasi='$idvalidasi' 
				UNION ALL
				SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,(H.diskon_rp * H.kuantitas) as nominal,CONCAT('Diskon Penjualan Racikan',H.namatipe,' Rawat Inap ') as ket,H.idgroup_diskon as idgroup
				FROM tvalidasi_pendapatan_ranap_obat_alkes_racikan H 						
				WHERE H.idvalidasi='$idvalidasi'

				UNION ALL

				SELECT H.idvalidasi,H.idakun_penjualan as idakun,H.posisi_penjualan as posisi_akun,(H.hargajual * H.kuantitas) as nominal,CONCAT('Penjualan ',H.namatipe,' IGD ',CASE WHEN H.tipe_farmasi='1' THEN ' Farmasi' ELSE ' ' END) as ket,H.idgroup_penjualan as idgroup
				FROM tvalidasi_pendapatan_ranap_obat_alkes_igd H 						
				WHERE H.idvalidasi='$idvalidasi' AND H.statusracikan='0'
				UNION ALL
				SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,(H.diskon_rp * H.kuantitas) as nominal,CONCAT('Diskon Penjualan ',H.namatipe,' IGD ',CASE WHEN H.tipe_farmasi='1' THEN ' Farmasi' ELSE ' ' END) as ket,H.idgroup_diskon as idgroup
				FROM tvalidasi_pendapatan_ranap_obat_alkes_igd H 						
				WHERE H.idvalidasi='$idvalidasi' AND H.statusracikan='0' AND H.diskon_rp > 0
				UNION ALL
				SELECT H.idvalidasi,H.idakun_tuslah as idakun,H.posisi_tuslah as posisi_akun,H.tuslah as nominal,CONCAT('Tuslah Penjualan ',H.namatipe,' IGD ',CASE WHEN H.tipe_farmasi='1' THEN ' Farmasi' ELSE ' ' END) as ket,H.idgroup_tuslah as idgroup
				FROM tvalidasi_pendapatan_ranap_obat_alkes_igd H 						
				WHERE H.idvalidasi='$idvalidasi' AND H.tuslah > 0						
				UNION ALL
				SELECT H.idvalidasi,H.idakun_penjualan as idakun,H.posisi_penjualan as posisi_akun,(H.hargajual * H.kuantitas) as nominal,CONCAT('Penjualan Racikan ',H.namatipe,' IGD ') as ket,H.idgroup_penjualan as idgroup
				FROM tvalidasi_pendapatan_ranap_obat_alkes_igd_racikan H 						
				WHERE H.idvalidasi='$idvalidasi' 
				UNION ALL
				SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,(H.diskon_rp * H.kuantitas) as nominal,CONCAT('Diskon Penjualan Racikan',H.namatipe,' IGD ') as ket,H.idgroup_diskon as idgroup
				FROM tvalidasi_pendapatan_ranap_obat_alkes_igd_racikan H 						
				WHERE H.idvalidasi='$idvalidasi'

				UNION ALL

				SELECT H.idvalidasi,H.idakun_penjualan as idakun,H.posisi_penjualan as posisi_akun,(H.hargajual * H.kuantitas) as nominal,CONCAT('Penjualan ',H.namatipe,' KO ') as ket,H.idgroup_penjualan as idgroup
				FROM tvalidasi_pendapatan_ranap_obat_alkes_ko H 						
				WHERE H.idvalidasi='$idvalidasi' 
				UNION ALL
				SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,(H.diskon_rp * H.kuantitas) as nominal,CONCAT('Diskon Penjualan ',H.namatipe,' KO ') as ket,H.idgroup_diskon as idgroup
				FROM tvalidasi_pendapatan_ranap_obat_alkes_ko H 						
				WHERE H.idvalidasi='$idvalidasi'
			";
		return $q;
   }
   //BATAS HAPUS
 
	
    public function jml_sisa_verifikasi($tanggal){
	   $q="SELECT COUNT(H.id) as jml FROM tkasir H
			WHERE H.`status`='2' AND DATE(H.tanggal)='$tanggal' AND H.status_verifikasi='0'";
		$query=$this->db->query($q);
		return $query->row('jml');
   }
   
   
   public function list_rekanan(){
	   $q="SELECT id,nama from mrekanan 
				WHERE mrekanan.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
    public function list_kp(){
	   $q="SELECT id,nama from mpasien_kelompok 
				WHERE mpasien_kelompok.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   
 
   function get_nama_akun($id){
	   $q="SELECT *from makun_nomor A WHERE A.id='$id'";
	   return $this->db->query($q)->row();
   }
  
   
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
