<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trawatinap_tindakan_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function spAdministrasiRanap($idpendaftaran, $totalSebelumAdm)
	{
		return $this->db->query(
			"SELECT
            trawatinap_pendaftaran.id AS idrawatinap,
            trawatinap_pendaftaran.nopendaftaran,
            trawatinap_pendaftaran.tanggaldaftar AS tanggal,
            mtarif_administrasi.id AS idtarif,
            'Biaya Administrasi' AS namatarif,
            mtarif_administrasi.idtipe,
            mtarif_administrasi.idjenis,
            (" . $totalSebelumAdm . ' * (mtarif_administrasi.persentasetarif / 100)) AS totalkeseluruhan,
            (CASE
                WHEN (' . $totalSebelumAdm . ' * (mtarif_administrasi.persentasetarif / 100)) < mtarif_administrasi.mintarif THEN
                    mtarif_administrasi.mintarif
                WHEN (' . $totalSebelumAdm . ' * (mtarif_administrasi.persentasetarif / 100)) > mtarif_administrasi.maxtarif THEN
                    mtarif_administrasi.maxtarif
                ELSE
                    (' . $totalSebelumAdm . ' * (mtarif_administrasi.persentasetarif / 100))
            END) AS tarif
        FROM trawatinap_pendaftaran
        JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
        JOIN mtarif_administrasi ON mtarif_administrasi.id = mpasien_kelompok.tadm_rawatinap OR mtarif_administrasi.id = mpasien_kelompok.tkartu_rawatinap
        WHERE mtarif_administrasi.idjenis = 1 AND trawatinap_pendaftaran.id = ' . $idpendaftaran
		)->result();
	}

	public function rawatinap_tindakan_pembayaran($idtindakan)
	{
		$this->db->where('idtindakan', $idtindakan);
		$get = $this->db->get('trawatinap_tindakan_pembayaran', 1);
		if ($get->num_rows() > 0) {
			return $get->row_array();
		} else {
			return false;
		}
	}

	public function detail_pembayaran($idtindakan)
	{
		$this->db->select('trawatinap_tindakan_pembayaran_detail.*');
		$this->db->join('trawatinap_tindakan_pembayaran', 'trawatinap_tindakan_pembayaran.id = trawatinap_tindakan_pembayaran_detail.idtindakan');
		$this->db->where('trawatinap_tindakan_pembayaran_detail.idtindakan', $idtindakan);
		// $this->db->where('trawatinap_tindakan_pembayaran.statusbatal', 0);
		$query = $this->db->get('trawatinap_tindakan_pembayaran_detail');
		return $query->result();
	}

	public function count_metode_pembayaran($idmetode, $idtindakan)
	{
		$this->db->where('idmetode', $idmetode);
		$this->db->where('idtindakan', $idtindakan);
		$query = $this->db->count_all_results('trawatinap_tindakan_pembayaran_detail');
		return $query;
	}

	public function count_metode_pembayaran_non_kontraktor($idtindakan)
	{
		$this->db->where('idmetode !=', '8');
		$this->db->where('idtindakan', $idtindakan);
		$query = $this->db->count_all_results('trawatinap_tindakan_pembayaran_detail');
		return $query;
	}

	public function count_metode_pembayaran_bpjstk($idtindakan)
	{
		$this->db->where('tipekontraktor', 4);
		$this->db->where('idtindakan', $idtindakan);
		$query = $this->db->count_all_results('trawatinap_tindakan_pembayaran_detail');
		return $query;
	}

	public function count_metode_pembayaran_deposit($idmetode, $idtindakan)
	{
		$this->db->where('idmetodepembayaran', $idmetode);
		$this->db->where('idrawatinap', $idtindakan);
		$query = $this->db->count_all_results('trawatinap_deposit');
		return $query;
	}

	public function count_metode_pembayaran_deposit_all($idtindakan)
	{
		$this->db->where('idrawatinap', $idtindakan);
		$query = $this->db->count_all_results('trawatinap_deposit');
		return $query;
	}

	public function pembayaran($idtindakan)
	{
		$this->db->where('idtindakan', $idtindakan);
		$this->db->where('statusbatal', 0);
		$query = $this->db->get('trawatinap_tindakan_pembayaran');
		return $query->row();
	}

	public function sum_pembayaran($idtindakan)
	{
		$this->db->select_sum('pembayaran');
		$this->db->where('idtindakan', $idtindakan);
		$this->db->where('statusbatal', 0);
		$get = $this->db->get('trawatinap_tindakan_pembayaran');
		if ($get->num_rows() > 0) {
			return $get->row_array()['pembayaran'];
		} else {
			return false;
		}
	}

	public function simpan_transaksi()
	{
		$this->db->where('id', $this->input->post('idpendaftaran'));
		$this->db->set('statuspembayaran', 1);

		$subTotal = RemoveComma($this->input->post('subtotal'));
		$diskonRp = RemoveComma($this->input->post('diskon_rp'));
		$diskonPersen = RemoveComma($this->input->post('diskon_persen'));
		$totalTransaksi = RemoveComma($this->input->post('total'));
		$nominalDeposit = RemoveComma($this->input->post('deposit'));
		$totalHarusDibayar = RemoveComma($this->input->post('totalharusdibayar'));
		$nominalPembayaran = RemoveComma($this->input->post('pembayaran'));
		$sisaPembayaran = RemoveComma($this->input->post('sisa'));

		if ($this->db->update('trawatinap_pendaftaran')) {
			$dataPembayaran = [
				'tanggal' => date('Y-m-d H:i:s'),
				'idtindakan' => $this->input->post('idtindakan'),
				'subtotal' => $subTotal,
				'diskonrp' => $diskonRp,
				'diskonpersen' => $diskonPersen,
				'total' => $totalTransaksi,
				'deposit' => $nominalDeposit,
				'totalharusdibayar' => $totalHarusDibayar,
				'pembayaran' => $nominalPembayaran,
				'sisa' => $sisaPembayaran,
				'nominal_round' => ($totalTransaksi + $diskonRp) - $subTotal,
				'iduser_input' => $this->session->userdata('user_id')
			];

			// Check History Detail Pembayaran
			$this->db->where('idtindakan', $this->input->post('idtindakan'));
			$this->db->where('statusbatal', 0);
			$this->db->order_by('id', 'DESC');
			$this->db->limit(1);
			$pembayaran = $this->db->get('trawatinap_tindakan_pembayaran');
			if ($pembayaran->num_rows() > 0) {
				$idtindakan = $pembayaran->row()->id;
				$this->db->where('idtindakan', $idtindakan);
				$this->db->delete('trawatinap_tindakan_pembayaran_detail');
			} else {
				$idtindakan = 0;
			}

			if ($this->db->insert('trawatinap_tindakan_pembayaran', $dataPembayaran)) {
				if ($idtindakan == 0) {
					$idtindakan = $this->db->insert_id();
				}

				$detailPembayaran = json_decode($this->input->post('pos_tabel_pembayaran'));
				foreach ($detailPembayaran as $row) {
					$dataPembayaranDetail = [
						'idtindakan' => $idtindakan,
						'idmetode' => $row[0],
						'nominal' => RemoveComma($row[4]),
						'idpegawai' => $row[8],
						'idbank' => $row[9],
						'ket_cc' => $row[10],
						'idkontraktor' => $row[11],
						'jaminan' => $row[14],
						'tracenumber' => $row[15],
						'tipekontraktor' => $row[16],
						'keterangan' => $row[3],
					];

					$this->db->insert('trawatinap_tindakan_pembayaran_detail', $dataPembayaranDetail);
				}

				$data['status'] = 200;
			} else {
				$data['status'] = 500;
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	public function getDokterPJ()
	{
		$this->db->select('mdokter.id, mdokter.nama, mdokter_kategori.id AS idkategori, mdokter_kategori.nama AS namakategori');
		$this->db->join('mdokter_kategori', 'mdokter_kategori.id = mdokter.idkategori', 'LEFT');
		$this->db->where('mdokter.status', '1');
		$query = $this->db->get('mdokter');
		return $query->result();
	}

	public function getCurrentDokterPJ($idpendaftaran)
	{
		$this->db->select('trawatinap_pendaftaran.iddokterpenanggungjawab, mdokter.nama AS namadokterpenanggungjawab,
        (CASE
          WHEN trawatinap_pendaftaran.tanggalperubahandokter IS NULL THEN
            trawatinap_pendaftaran.tanggaldaftar
          ELSE
            trawatinap_pendaftaran.tanggalperubahandokter
          END
        ) AS tanggal');
		$this->db->join('mdokter', 'mdokter.id = trawatinap_pendaftaran.iddokterpenanggungjawab');
		$this->db->where('trawatinap_pendaftaran.id', $idpendaftaran);
		$this->db->where('trawatinap_pendaftaran.status', '1');
		$query = $this->db->get('trawatinap_pendaftaran');
		return $query->result();
	}

	public function getHistoryDokterPJ($idpendaftaran)
	{
		$this->db->select('trawatinap_history_dokter.iddokter, mdokter.nama AS namadokterpenanggungjawab,
        trawatinap_history_dokter.tanggaldari, trawatinap_history_dokter.tanggalsampai');
		$this->db->join('mdokter', 'mdokter.id = trawatinap_history_dokter.iddokter');
		$this->db->where('trawatinap_history_dokter.idrawatinap', $idpendaftaran);
		$this->db->where('trawatinap_history_dokter.status', '1');
		$this->db->order_by('trawatinap_history_dokter.id', 'DESC');
		$query = $this->db->get('trawatinap_history_dokter');
		return $query->result();
	}

	public function updateDokterPJ($idpendaftaran, $tanggalperubahan, $iddokterpj)
	{
		$this->db->set('iddokterpenanggungjawab', $iddokterpj);
		$this->db->set('tanggalperubahandokter', $tanggalperubahan);
		$this->db->where('id', $idpendaftaran);
		$this->db->update('trawatinap_pendaftaran');
	}

	// Pindah Bed
	public function getPegawaiRawatInap()
	{
		$this->db->select('id, nama');
		$this->db->where('idkategori', '5');
		$this->db->where('status', '1');
		$query = $this->db->get('mpegawai');
		return $query->result();
	}

	public function getRuangan()
	{
		$this->db->select('id, nama');
		$this->db->where('idtipe', '1');
		$this->db->where('status', '1');
		$query = $this->db->get('mruangan');
		return $query->result();
	}

	public function getKelas()
	{
		$this->db->select('id, nama');
		$this->db->where('status', '1');
		$query = $this->db->get('mkelas');
		return $query->result();
	}

	public function getBed($idruangan, $idkelas)
	{
		$this->db->select("(CASE WHEN trawatinap_pendaftaran.id IS NULL THEN mbed.id ELSE '0' END) AS id,
          (CASE
            WHEN trawatinap_pendaftaran.id IS NULL THEN mbed.nama
            WHEN trawatinap_pendaftaran.statuskasir = '1' THEN CONCAT(mbed.nama, ' (NOT READY)')
            WHEN trawatinap_pendaftaran.statuskasir = '0' THEN CONCAT(mbed.nama, ' (TERISI)')
          END) AS nama,
          (CASE
            WHEN trawatinap_pendaftaran.id IS NULL THEN 1
            WHEN trawatinap_pendaftaran.statuskasir = '1' THEN 0
            WHEN trawatinap_pendaftaran.statuskasir = '0' THEN 0
          END) AS status");
		$this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.idbed = mbed.id AND trawatinap_pendaftaran.statuscheckout = 0 AND trawatinap_pendaftaran.status = 1', 'LEFT');
		// $this->db->where_not_in('mbed.id', 'SELECT idbed FROM trawatinap_pendaftaran', false);
		$this->db->where('mbed.idruangan', $idruangan);
		$this->db->where('mbed.idkelas', $idkelas);
		$this->db->where('mbed.status', '1');
		$query = $this->db->get('mbed');
		return $query->result();
	}

	public function getHistoryPindahBed($idpendaftaran)
	{
		$this->db->select('trawatinap_history_bed.*,
        mpegawai.nama AS namapetugas,
        mruangan.nama AS namaruangan,
        mkelas.nama AS namakelas,
        mbed.nama AS namabed,
        DATEDIFF(trawatinap_history_bed.tanggalsampai, trawatinap_history_bed.tanggaldari) AS jumlahhari');
		$this->db->join('mpegawai', 'mpegawai.id = trawatinap_history_bed.idpegawai');
		$this->db->join('mruangan', 'mruangan.id = trawatinap_history_bed.idruangan');
		$this->db->join('mkelas', 'mkelas.id = trawatinap_history_bed.idkelas');
		$this->db->join('mbed', 'mbed.id = trawatinap_history_bed.idbed');
		$this->db->where('trawatinap_history_bed.idrawatinap', $idpendaftaran);
		$this->db->where('trawatinap_history_bed.status', '1');
		$query = $this->db->get('trawatinap_history_bed');
		return $query->result();
	}

	public function updateBed($id, $tanggal, $idruangan, $idkelas, $idbed)
	{
		$this->db->where('id', $id);
		$this->db->set('tanggalperubahanbed', YMDFormat($tanggal));
		$this->db->set('idruangan', $idruangan);
		$this->db->set('idkelas', $idkelas);
		$this->db->set('idbed', $idbed);
		if ($this->db->update('trawatinap_pendaftaran')) {
			return true;
		} else {
			return false;
		}
	}

	// Estimasi Biaya
	public function getHistoryEstimasiBiaya($idpendaftaran)
	{
		$this->db->select('testimasi.*,
        DATE_FORMAT(testimasi.tanggal, "%d/%m/%Y") AS tanggal');
		$this->db->where('testimasi.idrawatinap', $idpendaftaran);
		$query = $this->db->get('testimasi');
		return $query->result();
	}

	public function getDataEstimasiBiaya($idestimasi)
	{
		$this->db->select('testimasi.*, mjenis_operasi.nama AS namajenisoperasi');
		$this->db->join('mjenis_operasi', 'mjenis_operasi.id = testimasi.jenisoperasi', 'LEFT');
		$this->db->where('testimasi.id', $idestimasi);
		$query = $this->db->get('testimasi');
		return $query->row();
	}

	public function getDataEstimasiBiayaImplant($idestimasi)
	{
		$this->db->select('mdata_implan.nama AS namaimplan,
        testimasi_implan.hargajual AS harga,
        testimasi_implan.kuantitas AS kuantitas,
        testimasi_implan.statusimplan AS status');
		$this->db->join('mdata_implan', 'mdata_implan.id = testimasi_implan.idimplan', 'LEFT');
		$this->db->where('testimasi_implan.idtestimasi', $idestimasi);
		$query = $this->db->get('testimasi_implan');
		return $query->result();
	}

	// Aksi Tindakan, Alat Kesehatan & Visite DOKTER
	public function getHistoryTindakan($idpendaftaran)
	{
		$this->db->select('trawatinap_tindakan.*,
        DATE_FORMAT(trawatinap_tindakan.tanggal, "%d/%m/%Y") AS tanggal,
        mdokter.nama AS namadokter,
        mtarif_rawatinap.nama AS namatindakan,
        trawatinap_pendaftaran.nopendaftaran');
		$this->db->join('mdokter', 'mdokter.id = trawatinap_tindakan.iddokter', 'LEFT');
		$this->db->join('mtarif_rawatinap', 'mtarif_rawatinap.id = trawatinap_tindakan.idpelayanan', 'LEFT');
		$this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = trawatinap_tindakan.idrawatinap', 'LEFT');
		$this->db->where('trawatinap_tindakan.idrawatinap', $idpendaftaran);
		$this->db->where('trawatinap_tindakan.status', 1);
		$query = $this->db->get('trawatinap_tindakan');
		return $query->result();
	}

	public function getTindakanRawatInap($search)
	{
		$this->db->like('nama', $search);
		$this->db->where('idkelompok', 0);
		$this->db->where('status', 1);
		$query = $this->db->get('mtarif_rawatinap');
		return $query->result_array();
	}

	public function getDetailTindakanRawatInap($idtindakan, $idkelas)
	{
		$this->db->join('mtarif_rawatinap', 'mtarif_rawatinap.id = mtarif_rawatinap_detail.idtarif');
		$this->db->where('mtarif_rawatinap.status', 1);
		$this->db->where('mtarif_rawatinap.id', $idtindakan);
		$this->db->where('mtarif_rawatinap_detail.kelas', $idkelas);
		$query = $this->db->get('mtarif_rawatinap_detail');
		return $query->row();
	}

	public function getHistoryAlkes($idpendaftaran)
	{
		$this->db->select('trawatinap_alkes.*,
        DATE_FORMAT(trawatinap_alkes.tanggal, "%d/%m/%Y") AS tanggal,
        mdata_alkes.nama AS namaalkes,
        munitpelayanan.nama AS namaunitpelayanan,
        trawatinap_pendaftaran.nopendaftaran');
		$this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = trawatinap_alkes.idrawatinap', 'LEFT');
		$this->db->join('mdata_alkes', 'mdata_alkes.id = trawatinap_alkes.idalkes', 'LEFT');
		$this->db->join('munitpelayanan', 'munitpelayanan.id = trawatinap_alkes.idunit', 'LEFT');
		$this->db->where('trawatinap_alkes.idrawatinap', $idpendaftaran);
		$this->db->where('trawatinap_alkes.status', 1);
		$query = $this->db->get('trawatinap_alkes');
		return $query->result();
	}

	public function getHistoryObat($idpendaftaran)
	{
		$this->db->select('trawatinap_obat.*,
        DATE_FORMAT(trawatinap_obat.tanggal, "%d/%m/%Y") AS tanggal,
        mdata_obat.nama AS namaobat,
        munitpelayanan.nama AS namaunitpelayanan,
        trawatinap_pendaftaran.nopendaftaran');
		$this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = trawatinap_obat.idrawatinap', 'LEFT');
		$this->db->join('mdata_obat', 'mdata_obat.id = trawatinap_obat.idobat', 'LEFT');
		$this->db->join('munitpelayanan', 'munitpelayanan.id = trawatinap_obat.idunit', 'LEFT');
		$this->db->where('trawatinap_obat.idrawatinap', $idpendaftaran);
		$this->db->where('trawatinap_obat.status', 1);
		$query = $this->db->get('trawatinap_obat');
		return $query->result();
	}

	public function getHistoryVisiteDokter($idpendaftaran)
	{
		$this->db->select('trawatinap_visite.*,
        DATE_FORMAT(trawatinap_visite.tanggal, "%d/%m/%Y") AS tanggal,
        mdokter.nama AS namadokter,
        mruangan.nama AS namaruangan,
        trawatinap_pendaftaran.nopendaftaran');
		$this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = trawatinap_visite.idrawatinap', 'LEFT');
		$this->db->join('mdokter', 'mdokter.id = trawatinap_visite.iddokter', 'LEFT');
		$this->db->join('mruangan', 'mruangan.id = trawatinap_visite.idruangan', 'LEFT');
		$this->db->where('trawatinap_visite.idrawatinap', $idpendaftaran);
		$this->db->where('trawatinap_visite.status', 1);
		$query = $this->db->get('trawatinap_visite');
		return $query->result();
	}

	public function getCurrentRuangan($idpendaftaran)
	{
		$this->db->select('trawatinap_pendaftaran.idruangan, trawatinap_pendaftaran.idkelas, mruangan.nama AS namaruangan');
		$this->db->join('mruangan', 'mruangan.id = trawatinap_pendaftaran.idruangan');
		$this->db->where('trawatinap_pendaftaran.id', $idpendaftaran);
		$query = $this->db->get('trawatinap_pendaftaran');
		return $query->row();
	}

	public function getTarifRuangan($idkelompokpasien, $idruangan, $idkelas)
	{
		$this->db->select("(
        CASE
        	WHEN $idruangan = 1 THEN
        		mpasien_kelompok.truang_perawatan
        	WHEN $idruangan = 2 THEN
        		mpasien_kelompok.truang_hcu
        	WHEN $idruangan = 3 THEN
        		mpasien_kelompok.truang_icu
        	WHEN $idruangan = 4 THEN
        		mpasien_kelompok.truang_isolasi
        END
        ) AS idtarif");
		$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
		$query = $this->db->get('mpasien_kelompok');

		$idtarif = $query->row()->idtarif;
		if ($idtarif) {
			$this->db->select('idtarif, jasasarana, jasapelayanan, bhp, biayaperawatan, total');
			$this->db->where('idtarif', $idtarif);
			$this->db->where('kelas', $idkelas);
			$query = $this->db->get('mtarif_ruangperawatan_detail');
			$row = $query->row();

			return [
				'idtarif' => $row->idtarif,
				'jasasarana' => $row->jasasarana,
				'jasapelayanan' => $row->jasapelayanan,
				'bhp' => $row->bhp,
				'biayaperawatan' => $row->biayaperawatan,
				'total' => $row->total,
			];
		} else {
			return [
				'idtarif' => 0,
				'jasasarana' => 0,
				'jasapelayanan' => 0,
				'bhp' => 0,
				'biayaperawatan' => 0,
				'total' => 0,
			];
		}
	}

	public function getTarifVisiteDokter($idkategori, $idkelompokpasien, $idruangan, $idkelas)
	{
		$this->db->select("(
        CASE
        	WHEN $idkategori = 1 AND $idruangan = 1 THEN
        		mpasien_kelompok.trawatinap_visite1
        	WHEN $idkategori != 1 AND $idruangan = 1 THEN
        		mpasien_kelompok.trawatinap_visite1_spesial
        	WHEN $idkategori = 1 AND $idruangan = 2 THEN
        		mpasien_kelompok.trawatinap_visite2
        	WHEN $idkategori != 1 AND $idruangan = 2 THEN
        		mpasien_kelompok.trawatinap_visite2_spesial
        	WHEN $idkategori = 1 AND $idruangan = 3 THEN
        		mpasien_kelompok.trawatinap_visite3
        	WHEN $idkategori != 1 AND $idruangan = 3 THEN
        		mpasien_kelompok.trawatinap_visite3_spesial
        	WHEN $idkategori = 1 AND $idruangan = 4 THEN
        		mpasien_kelompok.trawatinap_visite4
        	WHEN $idkategori != 1 AND $idruangan = 4 THEN
        		mpasien_kelompok.trawatinap_visite4_spesial
        END
        ) AS idtarif");
		$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
		$query = $this->db->get('mpasien_kelompok');

		$idtarif = $query->row()->idtarif;
		if ($idtarif) {
			$this->db->select('idtarif, jasasarana, jasapelayanan, bhp, biayaperawatan, total');
			$this->db->where('idtarif', $idtarif);
			$this->db->where('kelas', $idkelas);
			$query = $this->db->get('mtarif_visitedokter_detail');

			return $query->row();
		} else {
			return [
				'idtarif' => 0,
				'jasasarana' => 0,
				'jasapelayanan' => 0,
				'bhp' => 0,
				'biayaperawatan' => 0,
				'total' => 0,
			];
		}
	}

	// Daftar Operasi
	public function getDataOperasi($idpendaftaran)
	{
		$this->db->select('tkamaroperasi_pendaftaran.*,
        DATE_FORMAT(tkamaroperasi_pendaftaran.tanggaloperasi, "%d/%m/%Y") AS tanggaloperasi');
		$this->db->where('idpendaftaran', $idpendaftaran);
		$this->db->where('tkamaroperasi_pendaftaran.idasalpendaftaran', 2);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get('tkamaroperasi_pendaftaran');
		return $query->row();
	}

	public function getHistoryOperasi($idpendaftaran)
	{
		$this->db->select('tkamaroperasi_pendaftaran.*, musers.name AS user_menyetujui');
		$this->db->join('musers', 'musers.id = tkamaroperasi_pendaftaran.idapprove', 'LEFT');
		$this->db->where('tkamaroperasi_pendaftaran.idpendaftaran', $idpendaftaran);
		$this->db->where('tkamaroperasi_pendaftaran.idasalpendaftaran', 2);
		$this->db->where('tkamaroperasi_pendaftaran.tipe', 1);
		$query = $this->db->get('tkamaroperasi_pendaftaran');
		return $query->result();
	}

	public function getDataKontraktor($tipe, $idrekanan)
	{
		if ($tipe == 1) {
			$this->db->select('nama');
			$this->db->where('id', $idrekanan);
			$query = $this->db->get('mrekanan');
		} else {
			$this->db->select('nama');
			$this->db->where('id', $tipe);
			$query = $this->db->get('mpasien_kelompok');
		}

		return $query->row();
	}

	public function getDataRawatInap($idpendaftaran)
	{
		$this->db->select('trawatinap_pendaftaran.id,
        DATE_FORMAT(trawatinap_pendaftaran.tanggaldaftar, "%d/%m/%Y") AS tanggaldaftar,
        trawatinap_pendaftaran.tanggaldaftar AS tanggalpendaftaran,
        trawatinap_pendaftaran.idtipe,
        trawatinap_pendaftaran.nopendaftaran,
        trawatinap_pendaftaran.idtipe,
        trawatinap_pendaftaran.idtipepasien,
        mfpasien.id AS idpasien,
        mfpasien.no_medrec AS nomedrec,
        mfpasien.title AS titlepasien,
        mfpasien.nama AS namapasien,
        mfpasien.alamat_jalan AS alamatpasien,
        mfpasien.hp,
        mfpasien.telepon,
        mfpasien.nama_keluarga,
        mfpasien.telepon_keluarga,
        (CASE WHEN mfpasien.jenis_kelamin = 1 THEN "Laki-laki" ELSE "Perempuan" END) AS jeniskelamin,
        CONCAT(tpoliklinik_pendaftaran.umurtahun, " Th ", tpoliklinik_pendaftaran.umurbulan, " Bln ",
        tpoliklinik_pendaftaran.umurhari, " Hr") AS umur,
        trawatinap_pendaftaran.idruangan,
        mruangan.nama AS namaruangan,
        trawatinap_pendaftaran.idkelas,
        mkelas.nama AS namakelas,
        trawatinap_pendaftaran.idbed,
        trawatinap_pendaftaran.idkelompokpasien,
        mpasien_kelompok.nama AS namakelompok,
        mbed.nama AS namabed,
        mrekanan.id AS idrekanan,
        mrekanan.nama AS namarekanan,
        mdokterperujuk.id AS iddokterperujuk,
        mdokterperujuk.idkategori AS idkategoriperujuk,
        mdokterperujuk.nama AS namadokterperujuk,
        mdokterperujuk.pajakranap AS pajak_perujuk,
        mdokterperujuk.potonganrsranap AS potonganrs_perujuk,
        mdokterpenanggungjawab.id AS iddokterpenanggungjawab,
        mdokterpenanggungjawab.nama AS namadokterpenanggungjawab,
        trawatinap_pendaftaran.rujukfarmasi,
        trawatinap_pendaftaran.rujukradiologi,
        trawatinap_pendaftaran.rujuklaboratorium,
        trawatinap_pendaftaran.rujukfisioterapi,
        trawatinap_pendaftaran.tanggalperubahandokter,
        trawatinap_pendaftaran.tanggalperubahanbed,
        trawatinap_pendaftaran.catatan,
        trawatinap_pendaftaran.statuscheckout,
        trawatinap_pendaftaran.statuskasir,
        trawatinap_pendaftaran.statusvalidasi,
        trawatinap_pendaftaran.statustransaksi,
        trawatinap_pendaftaran.statuspembayaran,
        tpoliklinik_pendaftaran.idtipe AS idasalpasien,
        tpoliklinik_pendaftaran.idpoliklinik AS idpoliklinik');
		$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT');
		$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien', 'LEFT');
		$this->db->join('mrekanan', 'mrekanan.id = trawatinap_pendaftaran.idrekanan', 'LEFT');
		$this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
		$this->db->join('mruangan', 'mruangan.id = trawatinap_pendaftaran.idruangan', 'LEFT');
		$this->db->join('mkelas', 'mkelas.id = trawatinap_pendaftaran.idkelas', 'LEFT');
		$this->db->join('mbed', 'mbed.id = trawatinap_pendaftaran.idbed', 'LEFT');
		$this->db->join('mdokter mdokterperujuk', 'mdokterperujuk.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
		$this->db->join('mdokter mdokterpenanggungjawab', 'mdokterpenanggungjawab.id = trawatinap_pendaftaran.iddokterpenanggungjawab', 'LEFT');
		$this->db->where('trawatinap_pendaftaran.id', $idpendaftaran);
		$query = $this->db->get('trawatinap_pendaftaran');
		return $query->row();
	}

	public function getDataPoliTindakan($idpendaftaran)
	{
		$this->db->select('tpoliklinik_tindakan.sebabluar, IF (tkasir.status = 2, 1, 0) AS status_kasir');
		$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT');
		$this->db->join('tpoliklinik_tindakan', 'tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id', 'LEFT');
		$this->db->join('tkasir', 'tkasir.idtipe IN (1, 2) AND tkasir.idtindakan = tpoliklinik_tindakan.id', 'LEFT');
		$this->db->where('trawatinap_pendaftaran.id', $idpendaftaran);
		$query = $this->db->get('trawatinap_pendaftaran');
		return $query->row();
	}

	public function getDataCheckout($idpendaftaran)
	{
		$this->db->select('trawatinap_checkout.*,
        DATE_FORMAT(trawatinap_checkout.tanggalkeluar, "%d/%m/%Y") AS tanggalkeluar,
        DATE_FORMAT(trawatinap_checkout.tanggalkontrol, "%d/%m/%Y") AS tanggalkontrol,
        DATE_FORMAT(trawatinap_pendaftaran.tanggaldaftar, "%d/%m/%Y") AS tanggaldaftar');
		$this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = trawatinap_checkout.idrawatinap');
		$this->db->where('trawatinap_checkout.idrawatinap', $idpendaftaran);
		$query = $this->db->get('trawatinap_checkout');
		return $query->row();
	}

	// Deposit
	public function getBank()
	{
		$this->db->where('status', 1);
		$this->db->order_by('nama', 'ASC');
		$query = $this->db->get('mbank');
		return $query->result();
	}

	public function getDeposit($iddeposit)
	{
		$this->db->select('trawatinap_deposit.id,
        DATE_FORMAT(trawatinap_deposit.tanggal, "%d %M %Y") AS tanggal,
        mfpasien.no_medrec AS nomedrec,
        mfpasien.nama AS namapasien,
        mkelas.nama AS namakelas,
        trawatinap_deposit.idmetodepembayaran,
        (CASE
          WHEN trawatinap_deposit.idmetodepembayaran = 1 THEN "Tunai"
          WHEN trawatinap_deposit.idmetodepembayaran = 2 THEN "Debit"
          WHEN trawatinap_deposit.idmetodepembayaran = 3 THEN "Kredit"
          WHEN trawatinap_deposit.idmetodepembayaran = 4 THEN "Transfer"
        END) AS metodepembayaran,
        mbank.nama AS namabank,
        trawatinap_deposit.nodeposit,
        trawatinap_deposit.nominal,
        trawatinap_deposit.terimadari,
        musers.name AS namapetugas');
		$this->db->join('mbank', 'mbank.id = trawatinap_deposit.idbank', 'LEFT');
		$this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = trawatinap_deposit.idrawatinap', 'LEFT');
		$this->db->join('mkelas', 'mkelas.id = trawatinap_pendaftaran.idkelas', 'LEFT');
		$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT');
		$this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
		$this->db->join('musers', 'musers.id = trawatinap_deposit.iduserinput', 'LEFT');
		$this->db->where('trawatinap_deposit.id', $iddeposit);
		$this->db->where('trawatinap_deposit.status', 1);
		$query = $this->db->get('trawatinap_deposit');
		return $query->row();
	}

	public function getHistoryDeposit($idpendaftaran)
	{
		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$this->db->select('trawatinap_deposit.id,
        trawatinap_deposit.idrawatinap,
        DATE_FORMAT(trawatinap_deposit.tanggal, "%d/%m/%Y") AS tanggal,
        mfpasien.no_medrec AS nomedrec,
        mfpasien.nama AS namapasien,
        mkelas.nama AS namakelas,
        trawatinap_deposit.idmetodepembayaran,
        (CASE
			WHEN trawatinap_deposit.idmetodepembayaran = 1 THEN "Tunai"
			WHEN trawatinap_deposit.idmetodepembayaran = 2 THEN "Debit"
			WHEN trawatinap_deposit.idmetodepembayaran = 3 THEN "Kredit"
			WHEN trawatinap_deposit.idmetodepembayaran = 4 THEN "Transfer"
        END) AS metodepembayaran,
        mbank.nama AS namabank,
        trawatinap_deposit.nominal,
        trawatinap_deposit.terimadari,
        musers.name AS namapetugas,
		trawatinap_pendaftaran.statustransaksi');
		$this->db->join('mbank', 'mbank.id = trawatinap_deposit.idbank', 'LEFT');
		$this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = trawatinap_deposit.idrawatinap', 'LEFT');
		$this->db->join('mkelas', 'mkelas.id = trawatinap_pendaftaran.idkelas', 'LEFT');
		$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT');
		$this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
		$this->db->join('musers', 'musers.id = trawatinap_deposit.iduserinput', 'LEFT');
		$this->db->where('trawatinap_deposit.idrawatinap', $idpendaftaran);
		$this->db->where('trawatinap_deposit.status', 1);
		$query = $this->db->get('trawatinap_deposit');

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return [];
		}
	}

	public function getTotalDeposit($idpendaftaran)
	{
		$this->db->select('COALESCE(SUM(trawatinap_deposit.nominal),0) AS totaldeposit');
		$this->db->where('trawatinap_deposit.idrawatinap', $idpendaftaran);
		$this->db->where('trawatinap_deposit.status', 1);
		$query = $this->db->get('trawatinap_deposit');
		return $query->row();
	}

	// Estimasi
	public function getImplan($search)
	{
		$this->db->like('nama', $search);
		$this->db->where('status', 1);
		$query = $this->db->get('mdata_implan');
		return $query->result_array();
	}

	public function getDetailImplan($idimplan)
	{
		$this->db->select('mdata_implan.*,
        mdata_kategori.margin,
        (mdata_implan.hargadasar + (mdata_implan.hargadasar * (mdata_kategori.margin / 100))) AS hargajual');
		$this->db->join('mdata_kategori', 'mdata_kategori.id = mdata_implan.idkategori');
		$this->db->where('mdata_implan.id', $idimplan);
		$this->db->where('mdata_implan.status', 1);
		$query = $this->db->get('mdata_implan');
		return $query->row();
	}

	public function getDetailFakturImplant($idfaktur)
	{
		$this->db->select('trawatinap_faktur_implant.*, GROUP_CONCAT(mdistributor.nama) AS namadistributor');
		$this->db->join('trawatinap_faktur_implant_distributor', 'trawatinap_faktur_implant_distributor.idfaktur = trawatinap_faktur_implant.id');
		$this->db->join('mdistributor', 'mdistributor.id = trawatinap_faktur_implant_distributor.iddistributor');
		$this->db->where('trawatinap_faktur_implant.id', $idfaktur);
		$this->db->where('trawatinap_faktur_implant.status', '1');
		$query = $this->db->get('trawatinap_faktur_implant');
		return $query->row();
	}

	public function getListUnitPelayanan()
	{
		$this->db->select('munitpelayanan.id, munitpelayanan.nama');
		$this->db->where('munitpelayanan_user.userid', $this->session->userdata('user_id'));
		$this->db->join('munitpelayanan', 'munitpelayanan_user.idunitpelayanan = munitpelayanan.id', 'left');
		$this->db->from('munitpelayanan_user');
		$query = $this->db->get();
		return $query->result();
	}

	public function getDefaultUnitPelayananUser()
	{
		$this->db->select('unitpelayananiddefault as idunitpelayanan');
		$this->db->from('musers');
		$this->db->where('id', $this->session->userdata('user_id'));
		$query = $this->db->get();
		return $query->row_array()['idunitpelayanan'];
	}

	public function getCountBatalTransaksi($idpendaftaran)
	{
		$this->db->where('idtindakan', $idpendaftaran);
		$this->db->where('statusbatal', '1');
		$query = $this->db->get('trawatinap_tindakan_pembayaran');
		return $query->num_rows();
	}

	public function getHistoryBatal($idpendaftaran)
	{
		$this->db->select('trawatinap_tindakan_pembayaran.tanggalbatal,
        trawatinap_tindakan_pembayaran.alasanbatal,
        malasan_batal.keterangan AS alasanbatal_label,
        trawatinap_tindakan_pembayaran.iduser_batal,
        musers.name AS namauser');
		$this->db->join('musers', 'musers.id = trawatinap_tindakan_pembayaran.iduser_batal');
		$this->db->join('malasan_batal', 'malasan_batal.id = trawatinap_tindakan_pembayaran.alasanbatal', 'LEFT');
		$this->db->where('idtindakan', $idpendaftaran);
		$this->db->where('statusbatal', '1');
		$query = $this->db->get('trawatinap_tindakan_pembayaran');
		return $query->result();
	}

	public function getHistoryProsesKwitansi($idpendaftaran)
	{
		$this->db->select('trawatinap_kwitansi.*,
        musers.name AS namauser');
		$this->db->join('musers', 'musers.id = trawatinap_kwitansi.iduser_input');
		$this->db->where('trawatinap_kwitansi.idrawatinap', $idpendaftaran);
		$this->db->where('trawatinap_kwitansi.status', '1');
		$query = $this->db->get('trawatinap_kwitansi');
		return $query->result();
	}

	public function getHistoryProsesKwitansiImplant($idpendaftaran)
	{
		$this->db->select('trawatinap_kwitansi_implant.*,
        musers.name AS namauser');
		$this->db->join('musers', 'musers.id = trawatinap_kwitansi_implant.iduser_input');
		$this->db->where('trawatinap_kwitansi_implant.idrawatinap', $idpendaftaran);
		$this->db->where('trawatinap_kwitansi_implant.status', '1');
		$query = $this->db->get('trawatinap_kwitansi_implant');
		return $query->result();
	}

	public function getLastKwitansi($idpendaftaran)
	{
		$this->db->select('trawatinap_kwitansi.*,
        musers.name AS namauser');
		$this->db->join('musers', 'musers.id = trawatinap_kwitansi.iduser_input');
		$this->db->where('trawatinap_kwitansi.idrawatinap', $idpendaftaran);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get('trawatinap_kwitansi');
		return $query->row();
	}

	public function getLastKwitansiImplant($idpendaftaran)
	{
		$this->db->select('trawatinap_kwitansi_implant.*,
        musers.name AS namauser');
		$this->db->join('musers', 'musers.id = trawatinap_kwitansi_implant.iduser_input');
		$this->db->where('trawatinap_kwitansi_implant.idrawatinap', $idpendaftaran);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get('trawatinap_kwitansi_implant');
		return $query->row();
	}

	public function getDetailKwitansi($id)
	{
		$this->db->select('trawatinap_kwitansi.*,
        musers.name AS namauser');
		$this->db->join('musers', 'musers.id = trawatinap_kwitansi.iduser_input');
		$this->db->where('trawatinap_kwitansi.id', $id);
		$query = $this->db->get('trawatinap_kwitansi');
		return $query->row();
	}

	public function getDetailKwitansiImplant($id)
	{
		$this->db->select('trawatinap_kwitansi_implant.*,
        musers.name AS namauser');
		$this->db->join('musers', 'musers.id = trawatinap_kwitansi_implant.iduser_input');
		$this->db->where('trawatinap_kwitansi_implant.id', $id);
		$query = $this->db->get('trawatinap_kwitansi_implant');
		return $query->row();
	}

	public function getTotalKwitansiImplant($id)
	{
		$query = $this->db->query("SELECT
			SUM(tkamaroperasi_implan.totalkeseluruhan) AS totalkeseluruhan
		FROM
			tkamaroperasi_implan
		LEFT JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_implan.idpendaftaranoperasi
		LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
		LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 2 OR trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
		LEFT JOIN mdata_implan ON mdata_implan.id = tkamaroperasi_implan.idobat
		LEFT JOIN munitpelayanan ON munitpelayanan.id = tkamaroperasi_implan.idunit
		LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tkamaroperasi_implan.id AND tverifikasi_transaksi_detail.id_kelompok = 26
		WHERE
			trawatinap_pendaftaran.id = $id
		");
		return $query->row()->totalkeseluruhan;
	}

	public function getTotalPembayaran($idrawatinap, $tipe_proses, $tipekontraktor, $idkontraktor)
	{
		$this->db->select('COALESCE(SUM(trawatinap_tindakan_pembayaran_detail.nominal), 0) AS totalpembayaran,
            trawatinap_tindakan_pembayaran.total,
            trawatinap_tindakan_pembayaran.deposit,
            trawatinap_tindakan_pembayaran.pembayaran');
		$this->db->join('trawatinap_tindakan_pembayaran_detail', 'trawatinap_tindakan_pembayaran_detail.idtindakan = trawatinap_tindakan_pembayaran.id', 'LEFT');
		$this->db->where('trawatinap_tindakan_pembayaran.idtindakan', $idrawatinap);
		$this->db->where('trawatinap_tindakan_pembayaran.statusbatal', 0);
		if ($tipe_proses != 0) {
			$this->db->where('trawatinap_tindakan_pembayaran_detail.idmetode', $tipe_proses);
			if ($tipe_proses == 8) {
				$this->db->where('trawatinap_tindakan_pembayaran_detail.tipekontraktor', $tipekontraktor);
				$this->db->where('trawatinap_tindakan_pembayaran_detail.idkontraktor', $idkontraktor);
			}
		}
		$query = $this->db->get('trawatinap_tindakan_pembayaran');
		$row = $query->row();

		if ($tipe_proses == 0) {
			$totalPembayaran = $row->total;
		} else {
			$totalPembayaran = $row->totalpembayaran;
		}

		$this->db->select('COALESCE(SUM(nominal), 0) AS totaldeposit');
		$this->db->where('idrawatinap', $idrawatinap);
		$this->db->where('status', 1);
		if ($tipe_proses != 0) {
			$this->db->where('trawatinap_deposit.idmetodepembayaran', $tipe_proses);
		}
		$query = $this->db->get('trawatinap_deposit');
		$row = $query->row();

		if ($tipe_proses != 0) {
			$totalPembayaran = $totalPembayaran + $row->totaldeposit;
		}

		return $totalPembayaran;
	}

	public function getTotalExcess($idrawatinap)
	{
		// NON KONTRAKTOR
		$this->db->select('trawatinap_tindakan_pembayaran.subtotal, COALESCE(SUM(trawatinap_tindakan_pembayaran_detail.nominal), 0) AS totalpembayaran');
		$this->db->join('trawatinap_tindakan_pembayaran_detail', 'trawatinap_tindakan_pembayaran_detail.idtindakan = trawatinap_tindakan_pembayaran.id', 'LEFT');
		$this->db->where('trawatinap_tindakan_pembayaran_detail.idmetode', '8');
		$this->db->where('trawatinap_tindakan_pembayaran.idtindakan', $idrawatinap);
		$this->db->where('trawatinap_tindakan_pembayaran.statusbatal', 0);
		$query = $this->db->get('trawatinap_tindakan_pembayaran');
		$row = $query->row();

		$totalNonKontraktor = $row->subtotal - $row->totalpembayaran;

		return $totalNonKontraktor;
	}

	public function getTotalBPJSTK($idrawatinap)
	{
		// ALL
		$this->db->select('COALESCE(SUM(trawatinap_tindakan_pembayaran_detail.nominal), 0) AS totalpembayaran,
            trawatinap_tindakan_pembayaran.total,
            trawatinap_tindakan_pembayaran.deposit,
            trawatinap_tindakan_pembayaran.pembayaran');
		$this->db->join('trawatinap_tindakan_pembayaran_detail', 'trawatinap_tindakan_pembayaran_detail.idtindakan = trawatinap_tindakan_pembayaran.id', 'LEFT');
		$this->db->where('trawatinap_tindakan_pembayaran.idtindakan', $idrawatinap);
		$this->db->where('trawatinap_tindakan_pembayaran.statusbatal', 0);
		$query = $this->db->get('trawatinap_tindakan_pembayaran');
		$pembayaran = $query->row();

		$totalPembayaran = $pembayaran->total;

		// IMPLAN
		$implan = $this->db->query("SELECT
            SUM(tkamaroperasi_implan.totalkeseluruhan) - SUM(tkamaroperasi_implan.diskon) AS total
        FROM
            tkamaroperasi_implan
        LEFT JOIN tkamaroperasi_pendaftaran ON tkamaroperasi_pendaftaran.id = tkamaroperasi_implan.idpendaftaranoperasi
        LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1
        LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran = 2 OR trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id
        LEFT JOIN mdata_implan ON mdata_implan.id = tkamaroperasi_implan.idobat
        LEFT JOIN munitpelayanan ON munitpelayanan.id = tkamaroperasi_implan.idunit
        LEFT JOIN tverifikasi_transaksi_detail ON tverifikasi_transaksi_detail.id_detail = tkamaroperasi_implan.id AND tverifikasi_transaksi_detail.id_kelompok = 26
        WHERE
            trawatinap_pendaftaran.id = $idrawatinap
        ")->row();

		$totalImplan = $implan->total;

		// Pembayaran Selain BPJSTK
		$this->db->select('COALESCE(SUM(trawatinap_tindakan_pembayaran_detail.nominal), 0) AS totalpembayaran,
            trawatinap_tindakan_pembayaran.total,
            trawatinap_tindakan_pembayaran.deposit,
            trawatinap_tindakan_pembayaran.pembayaran');
		$this->db->join('trawatinap_tindakan_pembayaran_detail', 'trawatinap_tindakan_pembayaran_detail.idtindakan = trawatinap_tindakan_pembayaran.id', 'LEFT');
		$this->db->where('trawatinap_tindakan_pembayaran.idtindakan', $idrawatinap);
		$this->db->where('trawatinap_tindakan_pembayaran.statusbatal', 0);
		$this->db->where('trawatinap_tindakan_pembayaran_detail.tipekontraktor != ', 4);
		$query = $this->db->get('trawatinap_tindakan_pembayaran');
		$nonbpjstk = $query->row();

		$totalNonBPJSTK = $nonbpjstk->totalpembayaran;

		return ($totalPembayaran - $totalImplan) - $totalNonBPJSTK;
	}

	public function getSebabLuar()
	{
		$this->db->select('id, nama');
		$this->db->where('status', '1');
		$query = $this->db->get('msebab_luar');
		return $query->result();
	}

	public function getKelompokDiagnosa()
	{
		$this->db->select('id, nama');
		$this->db->where('status', '1');
		$query = $this->db->get('mkelompok_diagnosa');
		return $query->result();
	}

	public function getTanggalPembayaran($idrawatinap)
	{
		$this->db->where('idtindakan', $idrawatinap);
		$this->db->where('statusbatal', 0);
		$query = $this->db->get('trawatinap_tindakan_pembayaran');
		$row = $query->row();

		if ($query->num_rows() > 0) {
			return $row->tanggal;
		} else {
			return date('Y-m-d h:i:s');
		}
	}

	public function getInfoPembayaran($idrawatinap)
	{
		$this->db->select('musers.name AS user_input, trawatinap_tindakan_pembayaran.*');
		$this->db->join('musers', 'musers.id = trawatinap_tindakan_pembayaran.iduser_input', 'LEFT');
		$this->db->where('trawatinap_tindakan_pembayaran.idtindakan', $idrawatinap);
		$this->db->where('trawatinap_tindakan_pembayaran.statusbatal', 0);
		$query = $this->db->get('trawatinap_tindakan_pembayaran');
		return $query->row();
	}

	public function getPembayaranKontraktor($idrawatinap, $tipe, $idkontraktor)
	{
		$this->db->select('SUM(trawatinap_tindakan_pembayaran_detail.nominal) AS tagihanrekanan');
		$this->db->join('trawatinap_tindakan_pembayaran', 'trawatinap_tindakan_pembayaran.id = trawatinap_tindakan_pembayaran_detail.idtindakan');
		$this->db->where('trawatinap_tindakan_pembayaran.idtindakan', $idrawatinap);
		$this->db->where('trawatinap_tindakan_pembayaran_detail.tipekontraktor', $tipe);
		$this->db->where('trawatinap_tindakan_pembayaran_detail.idkontraktor', $idkontraktor);
		$this->db->where('trawatinap_tindakan_pembayaran.statusbatal', 0);
		$query = $this->db->get('trawatinap_tindakan_pembayaran_detail');
		return $query->row();
	}

	public function get_nama_kontraktor($jenis)
	{
		if ($jenis == 1) {
			$this->db->where('status', 1);
			$this->db->select('id, nama');
			$query = $this->db->get('mrekanan');
			return $query->result();
		} elseif ($jenis == 2) {
			return [];
		} elseif ($jenis == 3) {
			$this->db->select('id, kode AS nama');
			$this->db->where('status', 1);
			$query = $this->db->get('mtarif_bpjskesehatan');
			return $query->result();
		} elseif ($jenis == 4) {
			$this->db->select('id, id AS nama');
			$this->db->where('status', 1);
			$query = $this->db->get('mtarif_bpjstenagakerja');
			return $query->result();
		}
	}

	public function list_kontraktor($idpendaftaran)
	{
		$this->db->select('
        trawatinap_tindakan_pembayaran_detail.idmetode,
        trawatinap_tindakan_pembayaran_detail.tipekontraktor,
        trawatinap_tindakan_pembayaran_detail.idkontraktor,
        trawatinap_tindakan_pembayaran_detail.keterangan,
        mpasien_kelompok.nama AS namakontraktor');
		$this->db->join('trawatinap_tindakan_pembayaran', 'trawatinap_tindakan_pembayaran.id = trawatinap_tindakan_pembayaran_detail.idtindakan');
		$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = trawatinap_tindakan_pembayaran_detail.tipekontraktor');
		$this->db->where('trawatinap_tindakan_pembayaran.idtindakan', $idpendaftaran);
		$this->db->where('trawatinap_tindakan_pembayaran_detail.idmetode', 8);
		$this->db->group_by('trawatinap_tindakan_pembayaran_detail.tipekontraktor');
		$this->db->group_by('trawatinap_tindakan_pembayaran_detail.idkontraktor');
		$query = $this->db->get('trawatinap_tindakan_pembayaran_detail');
		return $query->result();
	}

	public function getNoSuratTagihan($idrawatinap, $idmetode, $idrekanan)
	{
		$this->db->select('trawatinap_tagihan.nosurat');
		$this->db->where('trawatinap_tagihan.idrawatinap', $idrawatinap);
		$this->db->where('trawatinap_tagihan.idmetode', $idmetode);
		$this->db->where('trawatinap_tagihan.idrekanan', $idrekanan);
		$query = $this->db->get('trawatinap_tagihan');
		if ($query->num_rows() > 0) {
			return $query->row()->nosurat;
		} else {
			$data = [
				'idrawatinap' => $idrawatinap,
				'idmetode' => $idmetode,
				'idrekanan' => $idrekanan
			];

			if ($this->db->insert('trawatinap_tagihan', $data)) {
				$this->db->select('trawatinap_tagihan.nosurat');
				$this->db->where('trawatinap_tagihan.idrawatinap', $idrawatinap);
				$this->db->where('trawatinap_tagihan.idmetode', $idmetode);
				$this->db->where('trawatinap_tagihan.idrekanan', $idrekanan);
				$query = $this->db->get('trawatinap_tagihan');
				return $query->row()->nosurat;
			}
		}
	}
}

/* End of file Trawatinap_tindakan_model.php */
/* Location: ./application/models/Trawatinap_tindakan_model.php */
