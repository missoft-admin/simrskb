<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tbooking_rehab_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function list_poli(){
		$q="SELECT M.id,M.idtipe,M.nama
			FROM setting_reservasi_petugas_rm_poli H
			LEFT JOIN mpoliklinik M ON M.id=H.idpoli
			
			
			GROUP BY H.idpoli
			ORDER BY M.idtipe,M.id";
		return $this->db->query($q)->result();
	}
	function list_tanggal($idpoli){
		$q="SELECT H.tanggal
				,CONCAT(MR.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y') )as tanggal_nama,H.kodehari
				
				FROM `app_reservasi_tanggal` H
				LEFT JOIN merm_hari MR ON MR.kodehari=H.kodehari
				WHERE H.reservasi_tipe_id='2' AND H.idpoli='$idpoli' AND H.tanggal >= CURRENT_DATE AND H.tanggal<DATE_ADD(CURRENT_DATE, INTERVAL 7 DAY) 
				GROUP BY H.tanggal";
		return $this->db->query($q)->result();
	}
	public function list_jam($idpoli,$tanggal)
	{
		$q="SELECT H.jam_id,CONCAT(JD.jam ,'.00 - ',JD.jam_akhir,'.00' ) jam_nama
			,H.saldo_kuota,CASE WHEN COALESCE(H.saldo_kuota)  > 0 THEN '' ELSE 'disabled' END st_disabel
			FROM `app_reservasi_tanggal` H
			LEFT JOIN merm_jam JD ON JD.jam_id=H.jam_id
			WHERE H.reservasi_tipe_id='2' AND H.idpoli='$idpoli' AND H.tanggal = '$tanggal' AND H.saldo_kuota IS NOT NULL
			GROUP BY H.tanggal,H.jam_id";
		// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	function list_dokter(){
		// $q="SELECT MP.id,MP.nama FROM `mjadwal_dokter` H
			// LEFT JOIN mdokter MP ON MP.id=H.iddokter
			// WHERE MP.`status`='1'
			// GROUP BY H.iddokter
// ";
			$q="SELECT MD.id,MD.nama FROM `app_reservasi_poli_dokter` H INNER JOIN mdokter MD ON MD.id=H.iddokter";
		return $this->db->query($q)->result();
	}
	function load_edit_poli($id){
		$q="SELECT 
			H.pendidikan_id as pendidikan,H.pekerjaan_id as pekerjaan
			,CONCAT(merm_hari.namahari,', ',DATE_FORMAT(H.tanggal,'%d-%m-%Y'),' Pukul : ',CONCAT(MJ.jam,'.00'),' - ',CONCAT(MJ.jam_akhir,'.00')) as janji_temu
			,H.* 
			
			FROM `app_reservasi_tanggal_pasien` H
			LEFT JOIN merm_jam MJ ON MJ.jam_id=H.jam_id
			LEFT JOIN merm_hari ON merm_hari.kodehari=H.kodehari
			
			WHERE H.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function get_gc($id){
		$q="SELECT app_id,logo as logo_gc,jawaban_perssetujuan,judul as judul_gc,sub_header as sub_header_gc,sub_header_side as sub_header_side_gc,footer_1 as footer_1_gc,footer_2 as footer_2_gc,ttd_1,ttd_2,footer_form_1,footer_form_2 FROM app_reservasi_tanggal_pasien_gc_head WHERE app_id='$id'";
		return $this->db->query($q)->row_array();
	}
	function get_sp($id){
		$q="SELECT logo as logo_sp,judul as judul_sp FROM `merm_skrining_pasien`";
		return $this->db->query($q)->row_array();
	}
	function get_sc($id){
		$q="SELECT logo as logo_sc,judul as judul_sc FROM `merm_skrining_covid`";
		return $this->db->query($q)->row_array();
	}
	function list_content_gc($id){
		$q="SELECT  * FROM app_reservasi_tanggal_pasien_gc WHERE app_id='$id'";
		return $this->db->query($q)->result();
	}
	function get_notransaksi(){
		$q="SELECT GetNoTrx_reservasi() as notransaksi";
		return $this->db->query($q)->row('notransaksi');
	}
	function list_cara_bayar(){
		$q="SELECT *FROM mpasien_kelompok H WHERE H.`status`='1' AND H.id IN (SELECT idkelompokpasien FROM setting_reservasi_petugas_rm_bayar)";
		return $this->db->query($q)->result();
	}
	function list_asuransi(){
		$q="SELECT M.* FROM setting_reservasi_petugas_rm_asuransi H INNER JOIN mrekanan M ON M.id=H.idrekanan";
		return $this->db->query($q)->result();
	}
}
