<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tlaba_rugi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function get_template($id){
		$q="SELECT H.nama FROM `tlaba_rugi_setting` H
		WHERE H.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_section($id){
		$q="SELECT H.id,H.nama FROM `tlaba_rugi_setting_section` H
				WHERE H.template_id='$id' AND H.lev='0' AND H.tipe='1'";
		return $this->db->query($q)->result();
	}
	function list_kategori($id){
		$q="SELECT H.id,H.nama FROM `tlaba_rugi_setting_section` H
				WHERE H.template_id='$id' AND H.lev='1' AND H.tipe='1'";
		return $this->db->query($q)->result();
	}
	function list_akun($id){
		$q="SELECT H.idakun,CONCAT(A.noakun,' - ',A.namaakun) as akun,H.section_id FROM `tlaba_rugi_setting_section_akun` H
LEFT JOIN makun_nomor A ON A.id=H.idakun
WHERE H.template_id='$id' ";
		return $this->db->query($q)->result();
	}
}
