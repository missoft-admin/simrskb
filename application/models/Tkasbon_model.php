<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tkasbon_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->select('tkasbon.*, (CASE WHEN tkasbon.idtipe = 1 THEN mdokter.nama ELSE mpegawai.nama END) AS namapegawai');
        $this->db->join('mdokter', 'mdokter.id = tkasbon.idpegawai AND tkasbon.idtipe = 1', 'LEFT');
        $this->db->join('mpegawai', 'mpegawai.id = tkasbon.idpegawai AND tkasbon.idtipe = 2', 'LEFT');
        $this->db->where('tkasbon.id', $id);
        $query = $this->db->get('tkasbon');
        return $query->row();
    }

    public function getPegawai($idtipe)
    {
        if ($idtipe == 1) {
            $this->db->where('status', 1);
            $query = $this->db->get('mdokter');
            return $query->result();
        } else {
            $this->db->where('status', 1);
            $query = $this->db->get('mpegawai');
            return $query->result();
        }
    }

    public function saveData()
    {
        $this->tanggal 	   = YMDFormat($_POST['tanggal']);
        $this->idtipe 	   = $_POST['idtipe'];
        $this->idpegawai 	 = $_POST['idpegawai'];
        $this->catatan 		 = $_POST['catatan'];
        $this->nominal 		 = RemoveComma($_POST['nominal']);
        $this->alokasidana = $_POST['alokasidana'];
        $this->created_at  = date("Y-m-d H:i:s");
        $this->user_created  = $this->session->userdata('user_id');

        if ($this->db->insert('tkasbon', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->tanggal 	   = YMDFormat($_POST['tanggal']);
        $this->idtipe 	   = $_POST['idtipe'];
        $this->idpegawai 	 = $_POST['idpegawai'];
        $this->catatan 		 = $_POST['catatan'];
        $this->nominal 		 = RemoveComma($_POST['nominal']);
        $this->alokasidana = $_POST['alokasidana'];

        if ($this->db->update('tkasbon', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('tkasbon', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
