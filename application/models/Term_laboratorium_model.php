<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Term_laboratorium_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

	function get_dianosa_utama($pendaftaran_id, $asal_rujukan){
		$query = "SELECT * FROM (
			SELECT 
				1 AS asal_rujukan, 
				tpoliklinik_asmed.diagnosa_utama
			FROM
				tpoliklinik_asmed
			WHERE
				tpoliklinik_asmed.status_assemen = '2' AND
				tpoliklinik_asmed.pendaftaran_id = '$pendaftaran_id'
			
			UNION ALL
			
			SELECT
				2 AS asal_rujukan,
				tpoliklinik_asmed_igd.diagnosa_utama
			FROM
				tpoliklinik_asmed_igd
			WHERE
				tpoliklinik_asmed_igd.status_assemen = '2' AND
				tpoliklinik_asmed_igd.pendaftaran_id='$pendaftaran_id'
		) result
		WHERE
			result.asal_rujukan = '$asal_rujukan'
		LIMIT 1";

		$data = $this->db->query($query)->row('diagnosa_utama');
		if ($data){
			return $data;
		}else{
			return '';
		}
	}
}
