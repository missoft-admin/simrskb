<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Msetting_honor_dokter_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSettingAkun()
    {
        $this->db->select('msetting_akun_honor_dokter.*,
          mdokter_kategori.nama AS namakategori,
          CONCAT(makun_pendaptan.noakun, " ", makun_pendaptan.namaakun) AS namaakun_pendapatan,
          CONCAT(makun_pajak.noakun, " ", makun_pajak.namaakun) AS namaakun_pajak,
          CONCAT(makun_piutang.noakun, " ", makun_piutang.namaakun) AS namaakun_piutang');
        $this->db->join('mdokter_kategori', 'mdokter_kategori.id = msetting_akun_honor_dokter.idkategori');
        $this->db->join('makun_nomor makun_pendaptan', 'makun_pendaptan.id = msetting_akun_honor_dokter.idakun_pendapatan');
        $this->db->join('makun_nomor makun_pajak', 'makun_pajak.id = msetting_akun_honor_dokter.idakun_pajak');
        $this->db->join('makun_nomor makun_piutang', 'makun_piutang.id = msetting_akun_honor_dokter.idakun_piutang');
        $query = $this->db->get('msetting_akun_honor_dokter');
        return $query->result();
    }

    public function getSettingPenyerahan($idtipe)
    {
        $this->db->select('msetting_penyerahan_honor_dokter.*, mdokter_kategori.nama AS namakategori');
        $this->db->join('mdokter_kategori', 'mdokter_kategori.id = msetting_penyerahan_honor_dokter.idkategori');
        $this->db->where('msetting_penyerahan_honor_dokter.idtipe', $idtipe);
        $query = $this->db->get('msetting_penyerahan_honor_dokter');
        return $query->result();
    }

    public function updateData()
    {
        $setting_akun = json_decode($_POST['nomor_akun']);
        if (COUNT($setting_akun) > 0) {
          foreach ($setting_akun as $row) {
            $data = array();
            $data['idkategori'] = $row[0];
            $data['idakun_pendapatan'] = $row[2];
            $data['idakun_pajak'] = $row[4];
            $data['idakun_piutang'] = $row[6];

            $this->db->replace('msetting_akun_honor_dokter', $data);
          }
        }

        $setting_penyerahan_asuransi = json_decode($_POST['penyerahan_asuransi']);
        if (COUNT($setting_penyerahan_asuransi) > 0) {
          foreach ($setting_penyerahan_asuransi as $row) {
            $data = array();
            $data['idtipe'] = '1';
            $data['idkategori'] = $row[0];
            $data['pendapatan_rajal'] = $row[2];
            $data['pendapatan_rujukan_ranap'] = $row[4];
            $data['pendapatan_laboratorium'] = $row[6];
            $data['pendapatan_fisioterapi'] = $row[8];
            $data['pendapatan_radiologi'] = $row[10];
            $data['pendapatan_ranap'] = $row[12];
            $data['pendapatan_anesthesi'] = $row[14];

            $this->db->replace('msetting_penyerahan_honor_dokter', $data);
          }
        }

        $setting_penyerahan_hold = json_decode($_POST['penyerahan_hold']);
        if (COUNT($setting_penyerahan_hold) > 0) {
          foreach ($setting_penyerahan_hold as $row) {
            $data = array();
            $data['idtipe'] = '2';
            $data['idkategori'] = $row[0];
            $data['pendapatan_rajal'] = $row[2];
            $data['pendapatan_rujukan_ranap'] = $row[4];
            $data['pendapatan_laboratorium'] = $row[6];
            $data['pendapatan_fisioterapi'] = $row[8];
            $data['pendapatan_radiologi'] = $row[10];
            $data['pendapatan_ranap'] = $row[12];
            $data['pendapatan_anesthesi'] = $row[14];

            $this->db->replace('msetting_penyerahan_honor_dokter', $data);
          }
        }

        return true;
    }
}
