<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpengajuan_skd_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mpengajuan_skd');
        return $query->row();
    }

    public function saveData()
    {
        $this->nama = $_POST['nama'];
        $this->estimasi = $_POST['estimasi'];

        if ($this->db->insert('mpengajuan_skd', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama = $_POST['nama'];
        $this->estimasi = $_POST['estimasi'];

        if ($this->db->update('mpengajuan_skd', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('mpengajuan_skd', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
