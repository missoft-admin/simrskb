<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Term_radiologi_mri_expertise_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_data_transaksi($transaksi_id)
    {
        $this->db->select('
            term_radiologi_mri.*,
            COALESCE ( tpoliklinik_pendaftaran.no_medrec, poliklinik_ranap.nomor_medrec) AS nomor_medrec,
            COALESCE ( tpoliklinik_pendaftaran.namapasien, poliklinik_ranap.nama_pasien) AS nama_pasien,
            COALESCE ( tpoliklinik_pendaftaran.nik, poliklinik_ranap.nomor_ktp) AS nomor_ktp,
            COALESCE ( referensi_jenis_kelamin.ref, poliklinik_ranap.jenis_kelamin) AS jenis_kelamin,
            COALESCE ( tpoliklinik_pendaftaran.email, poliklinik_ranap.email) AS email,
            COALESCE ( tpoliklinik_pendaftaran.alamatpasien, poliklinik_ranap.alamat_pasien) AS alamatpasien,
            COALESCE ( tpoliklinik_pendaftaran.tanggal_lahir, poliklinik_ranap.tanggal_lahir) AS tanggal_lahir,
            COALESCE ( tpoliklinik_pendaftaran.umurhari, poliklinik_ranap.umur_hari) AS umur_hari,
            COALESCE ( tpoliklinik_pendaftaran.umurbulan, poliklinik_ranap.umur_bulan) AS umur_bulan,
            COALESCE ( tpoliklinik_pendaftaran.umurtahun, poliklinik_ranap.umur_tahun) AS umur_tahun,
            COALESCE ( mpasien_kelompok.nama, poliklinik_ranap.kelompok_pasien) AS kelompok_pasien,
            COALESCE ( mrekanan.nama, poliklinik_ranap.rekanan_asuransi) AS nama_asuransi,
            (CASE
                WHEN term_radiologi_mri.asal_rujukan = 1 THEN "Poliklinik"
                WHEN term_radiologi_mri.asal_rujukan = 2 THEN "Instalasi Gawat Darurat"
                WHEN term_radiologi_mri.asal_rujukan = 3 THEN "Rawat Inap"
                WHEN term_radiologi_mri.asal_rujukan = 4 THEN "One Day Surgery"
            END) AS tipe_kunjungan,
            mpoliklinik.nama AS nama_poliklinik,
            (CASE
                WHEN term_radiologi_mri.asal_rujukan IN (1, 2) THEN
                    tpoliklinik_pendaftaran.nopendaftaran
                WHEN term_radiologi_mri.asal_rujukan IN (3, 4) THEN
                    poliklinik_ranap.nopendaftaran
            END) AS nomor_pendaftaran,
            term_radiologi_mri.prioritas AS prioritas_pemeriksaan,
            mdokter_perujuk.nama AS dokter_perujuk,
            mdokter_penanggung_jawab.nama AS dokter_penanggung_jawab,
            mdokter_radiologi.nama AS dokter_radiologi,
            mdokter_radiologi.id AS dokter_radiologi_id,
            mdokter_radiologi.nip AS dokter_radiologi_nip,
            muser_created.nama AS user_created,
            muser_pemeriksa.nama AS user_input_pemeriksaan,
            muser_penginputan_foto.nama AS user_penginputan_foto,
            muser_penginputan_expertise.nama AS user_penginputan_expertise,
            term_radiologi_mri.created_at AS waktu_pembuatan,
            trujukan_radiologi.norujukan AS nomor_radiologi'
        );
        $this->db->from('term_radiologi_mri');
        $this->db->join('(SELECT
            trawatinap_pendaftaran.id,
            trawatinap_pendaftaran.nopendaftaran,
            tpoliklinik_pendaftaran.kode_antrian,
            tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
            tpoliklinik_pendaftaran.idpasien AS pasien_id,
            tpoliklinik_pendaftaran.namapasien AS nama_pasien,
            tpoliklinik_pendaftaran.nik AS nomor_ktp,
            referensi_jenis_kelamin.ref AS jenis_kelamin,
            tpoliklinik_pendaftaran.email,
            tpoliklinik_pendaftaran.alamatpasien AS alamat_pasien,
            tpoliklinik_pendaftaran.tanggal_lahir,
            tpoliklinik_pendaftaran.umurhari AS umur_hari,
            tpoliklinik_pendaftaran.umurbulan AS umur_bulan,
            tpoliklinik_pendaftaran.umurtahun AS umur_tahun,
            mpasien_kelompok.nama AS kelompok_pasien,
            mrekanan.nama AS rekanan_asuransi
        FROM
            trawatinap_pendaftaran
        INNER JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
        INNER JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
        LEFT JOIN mrekanan ON mrekanan.id = trawatinap_pendaftaran.idrekanan
        INNER JOIN merm_referensi referensi_jenis_kelamin ON referensi_jenis_kelamin.nilai = tpoliklinik_pendaftaran.jenis_kelamin AND referensi_jenis_kelamin.ref_head_id = 1) AS poliklinik_ranap', 'term_radiologi_mri.pendaftaran_id = poliklinik_ranap.id AND term_radiologi_mri.asal_rujukan IN (3, 4)', 'LEFT');
		$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_radiologi_mri.pendaftaran_id AND term_radiologi_mri.asal_rujukan IN (1, 2)', 'LEFT');
        $this->db->join('merm_referensi referensi_jenis_kelamin', 'referensi_jenis_kelamin.nilai = tpoliklinik_pendaftaran.jenis_kelamin AND referensi_jenis_kelamin.ref_head_id = 1', 'LEFT');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
        $this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('merm_pengaturan_tujuan_radiologi', 'merm_pengaturan_tujuan_radiologi.id = term_radiologi_mri.tujuan_radiologi', 'LEFT');
        $this->db->join('mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_radiologi_mri.dokter_perujuk_id', 'LEFT');
        $this->db->join('mdokter mdokter_penanggung_jawab', 'mdokter_penanggung_jawab.id = merm_pengaturan_tujuan_radiologi.dokter_penanggung_jawab', 'LEFT');
		$this->db->join('mdokter mdokter_radiologi', 'mdokter_radiologi.id = term_radiologi_mri.dokter_radiologi', 'LEFT');
        $this->db->join('mppa muser_created', 'muser_created.id = term_radiologi_mri.created_ppa', 'LEFT');
        $this->db->join('mppa muser_pemeriksa', 'muser_pemeriksa.id = term_radiologi_mri.petugas_pemeriksaan', 'LEFT');
		$this->db->join('mppa muser_penginputan_foto', 'muser_penginputan_foto.id = term_radiologi_mri.petugas_penginputan_foto', 'LEFT');
		$this->db->join('mppa muser_penginputan_expertise', 'muser_penginputan_expertise.id = term_radiologi_mri.petugas_penginputan_expertise', 'LEFT');
		$this->db->join('trujukan_radiologi', 'trujukan_radiologi.id = term_radiologi_mri.rujukan_id', 'LEFT');
        $this->db->where('term_radiologi_mri.id', $transaksi_id);

        $query = $this->db->get();
        return $query->row();
    }

    public function get_daftar_pemeriksaan($transaksi_id)
	{
		$this->db->select('term_radiologi_mri_pemeriksaan.*, 
            mppa.nama AS order_by,
            term_radiologi_mri.created_at AS order_at');
		$this->db->join('term_radiologi_mri', 'term_radiologi_mri.id = term_radiologi_mri_pemeriksaan.transaksi_id', 'LEFT');
		$this->db->join('mppa', 'mppa.id = term_radiologi_mri.created_ppa', 'LEFT');
		$this->db->where('term_radiologi_mri_pemeriksaan.transaksi_id', $transaksi_id);
		$this->db->where('term_radiologi_mri_pemeriksaan.status', '1');
		$query = $this->db->get('term_radiologi_mri_pemeriksaan');
		return $query->result();
	}

    public function get_detail_pemeriksaan($pemeriksaanId) {
        $this->db->where('id', $pemeriksaanId);
        $query = $this->db->get('term_radiologi_mri_pemeriksaan');

        return $query->row_array();
    }

    public function get_hasil_pemeriksaan_terakhir($transaksi_id) {
        $this->db->select('id');
        $this->db->where('transaksi_id', $transaksi_id);
        $this->db->where('status', 1);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('term_radiologi_mri_hasil_pemeriksaan');

        $result = $query->row();

        return $result ? $result->id : null;
    }

    public function get_version_hasil_pemeriksaan_terakhir($id) {
        $this->db->select_max('version');
        $this->db->where('transaksi_id', $id);
        $query = $this->db->get('term_radiologi_mri_hasil_pemeriksaan');
        $result = $query->row_array();

        return isset($result['version']) ? $result['version'] : 0;
    }

    public function get_riwayat_cetak_hasil_pemeriksaan($hasil_pemeriksaan_id) {
        $this->db->select('term_radiologi_mri_hasil_pemeriksaan_history.*,
            petugas_cetak_hasil.nama AS petugas_cetak_hasil,');
        $this->db->join('mppa petugas_cetak_hasil', 'petugas_cetak_hasil.id = term_radiologi_mri_hasil_pemeriksaan_history.petugas_cetak_hasil', 'LEFT');
        $this->db->order_by('urutan_cetak', 'DESC');
        $this->db->where('hasil_pemeriksaan_id', $hasil_pemeriksaan_id);
        $query = $this->db->get('term_radiologi_mri_hasil_pemeriksaan_history');
        
        return $query->result_array();
    }

    public function get_data_pengiriman_hasil_pemeriksaan_pertama($id)
    {
        $this->db->select('term_radiologi_mri_hasil_pemeriksaan_email.*,
            petugas_pengiriman_hasil.nama AS petugas_pengiriman_hasil,
            petugas_penerimaan_hasil.nama AS petugas_penerimaan_hasil');
        $this->db->join('term_radiologi_mri_hasil_pemeriksaan', 'term_radiologi_mri_hasil_pemeriksaan.id = term_radiologi_mri_hasil_pemeriksaan_email.hasil_pemeriksaan_id', 'left');
        $this->db->join('mppa petugas_pengiriman_hasil', 'petugas_pengiriman_hasil.id = term_radiologi_mri_hasil_pemeriksaan_email.petugas_pengiriman_hasil', 'LEFT');
        $this->db->join('mppa petugas_penerimaan_hasil', 'petugas_penerimaan_hasil.id = term_radiologi_mri_hasil_pemeriksaan_email.petugas_penerimaan_hasil', 'LEFT');
        $this->db->where('term_radiologi_mri_hasil_pemeriksaan.transaksi_id', $id);
        $this->db->limit(1);

        $query = $this->db->get('term_radiologi_mri_hasil_pemeriksaan_email');
        return $query->row();
    }
    
    public function get_file_hasil_pemeriksaan($id) {
        $this->db->select('id, nama_file');
        $this->db->where('transaksi_id', $id);
        $this->db->where('status', 1);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('term_radiologi_mri_hasil_pemeriksaan');
        return $query->row();
    }

    public function get_file_hasil_upload($id) {
        $this->db->select('term_radiologi_mri_pemeriksaan_file.id, term_radiologi_mri_pemeriksaan_file.filename');
        $this->db->join('term_radiologi_mri_pemeriksaan', 'term_radiologi_mri_pemeriksaan.id = term_radiologi_mri_pemeriksaan_file.pemeriksaan_id');
        $this->db->where('term_radiologi_mri_pemeriksaan.transaksi_id', $id);
        $this->db->where('term_radiologi_mri_pemeriksaan_file.status', 1);
        $query = $this->db->get('term_radiologi_mri_pemeriksaan_file');
        return $query->result();
    }

    public function get_email_bcc_hasil_upload($id) {
        $this->db->select('email');
        $this->db->where('pengaturan_id', $id);
        $query = $this->db->get('merm_pengaturan_pengiriman_email_hasil_upload_radiologi_bcc');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    public function get_email_pasien($pasienId) {
        $this->db->select('email');
        $this->db->where('id', $pasienId);
        $query = $this->db->get('mfpasien');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function get_email_pengiriman_hasil_pemeriksaan_terakhir($id) {
        $this->db->select('term_radiologi_mri_hasil_pemeriksaan_email.email_penerima AS email');
        $this->db->join('term_radiologi_mri_hasil_pemeriksaan_email', 'term_radiologi_mri_hasil_pemeriksaan_email.hasil_pemeriksaan_id = term_radiologi_mri_hasil_pemeriksaan.id');
        $this->db->where('term_radiologi_mri_hasil_pemeriksaan.transaksi_id', $id);
        $this->db->order_by('term_radiologi_mri_hasil_pemeriksaan_email.id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('term_radiologi_mri_hasil_pemeriksaan');

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return array();
        }
    }
    
    public function get_pengaturan_attachment_email()
    {
        $query = $this->db->get('merm_pengaturan_pengiriman_email_radiologi_file');
        return $query->result();
    }

    private function get_jumlah_cetak_hasil($id) {
        $this->db->select('jumlah_cetak');
        $this->db->where('id', $id);
        $query = $this->db->get('term_radiologi_mri_hasil_pemeriksaan');
        $result = $query->row_array();

        return isset($result['jumlah_cetak']) ? $result['jumlah_cetak'] : 0;
    }

    public function log_cetak_hasil($id)
    {
        $log_history_data = [
            'hasil_pemeriksaan_id' => $id,
            'urutan_cetak' => $this->get_jumlah_cetak_hasil($id) + 1,
            'waktu_cetak_hasil' => date('Y-m-d H:i:s'),
            'petugas_cetak_hasil' => $this->session->userdata('user_id'),
        ];

        $this->db->insert('term_radiologi_mri_hasil_pemeriksaan_history', $log_history_data);

        // Update jumlah cetak di tabel term_radiologi_mri_hasil_pemeriksaan
        $this->db->set('jumlah_cetak', $this->get_jumlah_cetak_hasil($id) + 1);
        $this->db->where('id', $id);
        $this->db->update('term_radiologi_mri_hasil_pemeriksaan');
    }

    public function log_kirim_hasil($data)
    {
        $log_history_data = [
            'hasil_pemeriksaan_id' => $data['hasil_pemeriksaan_id'],
            'waktu_pengiriman_hasil' => $data['waktu_pengiriman_hasil'],
            'petugas_pengiriman_hasil' => $data['petugas_pengiriman_hasil'],
            'waktu_penerimaan_hasil' => $data['waktu_penerimaan_hasil'],
            'petugas_penerimaan_hasil' => $data['petugas_penerimaan_hasil'],
            'penerima_bukan_petugas' => $data['penerima_bukan_petugas'],
            'email_penerima' => $data['email_penerima'],
        ];

        $this->db->insert('term_radiologi_mri_hasil_pemeriksaan_email', $log_history_data);
    }

    public function update_pemeriksaan_expertise($pemeriksaanId, $flagKritis, $klinis, $kesan, $usul, $hasil) {
        $data = array(
            'flag_kritis' => $flagKritis,
            'klinis' => $klinis,
            'kesan' => $kesan,
            'usul' => $usul,
            'hasil' => $hasil
        );

        $this->db->where('id', $pemeriksaanId);
        $this->db->update('term_radiologi_mri_pemeriksaan', $data);

        return ($this->db->affected_rows() > 0) ? true : false;
    }
}
