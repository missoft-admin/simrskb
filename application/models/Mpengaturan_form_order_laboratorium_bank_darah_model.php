<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpengaturan_form_order_laboratorium_bank_darah_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('merm_pengaturan_form_order_laboratorium_bank_darah');
        return $query->row();
    }

    public function saveData()
    {
        $this->pesan_informasi_berhasil = $_POST['pesan_informasi_berhasil'];
        
        $this->label_judul = $_POST['label_judul'];
        $this->label_judul_eng = $_POST['label_judul_eng'];

        $this->label_tujuan_laboratorium = $_POST['label_tujuan_laboratorium'];
        $this->label_tujuan_laboratorium_eng = $_POST['label_tujuan_laboratorium_eng'];
        
        if (isset($_POST['required_tujuan_laboratorium'])) {
            $this->required_tujuan_laboratorium = $_POST['required_tujuan_laboratorium'];
        }
        
        $this->label_dokter_peminta_pemeriksaan = $_POST['label_dokter_peminta_pemeriksaan'];
        $this->label_dokter_peminta_pemeriksaan_eng = $_POST['label_dokter_peminta_pemeriksaan_eng'];
        if (isset($_POST['required_dokter_peminta_pemeriksaan'])) {
            $this->required_dokter_peminta_pemeriksaan = $_POST['required_dokter_peminta_pemeriksaan'];
        }
        
        $this->label_diagnosa = $_POST['label_diagnosa'];
        $this->label_diagnosa_eng = $_POST['label_diagnosa_eng'];
        if (isset($_POST['required_diagnosa'])) {
            $this->required_diagnosa = $_POST['required_diagnosa'];
        }
        
        $this->label_catatan_pemeriksaan = $_POST['label_catatan_pemeriksaan'];
        $this->label_catatan_pemeriksaan_eng = $_POST['label_catatan_pemeriksaan_eng'];
        if (isset($_POST['required_catatan_pemeriksaan'])) {
            $this->required_catatan_pemeriksaan = $_POST['required_catatan_pemeriksaan'];
        }
        
        $this->label_waktu_permintaan = $_POST['label_waktu_permintaan'];
        $this->label_waktu_permintaan_eng = $_POST['label_waktu_permintaan_eng'];
        if (isset($_POST['required_waktu_permintaan'])) {
            $this->required_waktu_permintaan = $_POST['required_waktu_permintaan'];
        }
        
        $this->label_prioritas = $_POST['label_prioritas'];
        $this->label_prioritas_eng = $_POST['label_prioritas_eng'];
        if (isset($_POST['required_prioritas'])) {
            $this->required_prioritas = $_POST['required_prioritas'];
        }
        
        $this->label_indikasi_transfusi = $_POST['label_indikasi_transfusi'];
        $this->label_indikasi_transfusi_eng = $_POST['label_indikasi_transfusi_eng'];
        if (isset($_POST['required_indikasi_transfusi'])) {
            $this->required_indikasi_transfusi = $_POST['required_indikasi_transfusi'];
        }
        
        $this->label_hb = $_POST['label_hb'];
        $this->label_hb_eng = $_POST['label_hb_eng'];
        if (isset($_POST['required_hb'])) {
            $this->required_hb = $_POST['required_hb'];
        }
        
        $this->label_riwayat_transfusi_sebelumnya = $_POST['label_riwayat_transfusi_sebelumnya'];
        $this->label_riwayat_transfusi_sebelumnya_eng = $_POST['label_riwayat_transfusi_sebelumnya_eng'];
        if (isset($_POST['required_riwayat_transfusi_sebelumnya'])) {
            $this->required_riwayat_transfusi_sebelumnya = $_POST['required_riwayat_transfusi_sebelumnya'];
        }
        
        $this->label_riwayat_kehamilan_sebelumnya = $_POST['label_riwayat_kehamilan_sebelumnya'];
        $this->label_riwayat_kehamilan_sebelumnya_eng = $_POST['label_riwayat_kehamilan_sebelumnya_eng'];
        if (isset($_POST['required_riwayat_kehamilan_sebelumnya'])) {
            $this->required_riwayat_kehamilan_sebelumnya = $_POST['required_riwayat_kehamilan_sebelumnya'];
        }
        
        $this->label_keterangan_lainnya = $_POST['label_keterangan_lainnya'];
        $this->label_keterangan_lainnya_eng = $_POST['label_keterangan_lainnya_eng'];
        if (isset($_POST['required_keterangan_lainnya'])) {
            $this->required_keterangan_lainnya = $_POST['required_keterangan_lainnya'];
        }
        
        $this->label_catatan = $_POST['label_catatan'];
        $this->label_catatan_eng = $_POST['label_catatan_eng'];
        if (isset($_POST['required_catatan'])) {
            $this->required_catatan = $_POST['required_catatan}'];
        }

        $this->db->where('id', 1);
        if ($this->db->update('merm_pengaturan_form_order_laboratorium_bank_darah', $this)) {
            return true;
        }

        return false;
    }
}
