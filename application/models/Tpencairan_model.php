<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpencairan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('tpencairan');
        return $query->row();
    }
	public function get_detail($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('tpencairan');
        return $query->row_array();
    }
	public function get_jml_pencairan()
    {
       $q="SELECT COUNT(H.id) as jml FROM tpencairan H WHERE H.`status`='2'";
        return $this->db->query($q)->row('jml');
    }
	function get_pengajuan($tanggal){
		// print_r($tanggal);exit();
		$query = $this->db->query("call fc_pencairan_pengajuan('$tanggal');");
		
		$result = array('list_field_pengajuan' => $query->list_fields(), 'query_pengajuan' => $query->result());
		$query->next_result();
		$query->free_result();
		return $result;
		
	}
	function get_kasbon($tanggal){
		
		$query = $this->db->query("call fc_pencairan_kasbon('$tanggal');");
		$result = array('list_field_kasbon' => $query->list_fields(), 'query_kasbon' => $query->result());
		// print_r($result);exit();
		$query->next_result();
		$query->free_result();
		return $result;
		
	}
	function get_refund($tanggal){
		
		$query = $this->db->query("call fc_pencairan_refund('$tanggal');");
		
		$result = array('list_field_refund' => $query->list_fields(), 'query_refund' => $query->result());
		$query->next_result();
		$query->free_result();
		return $result;
		
	}
	function get_mutasi($tanggal){
		
		$query = $this->db->query("call fc_pencairan_mutasi('$tanggal');");
		
		$result = array('list_field_mutasi' => $query->list_fields(), 'query_mutasi' => $query->result());
		$query->next_result();
		$query->free_result();
		return $result;
		
	}
	function get_kbo($tanggal){
		
		$query = $this->db->query("call fc_pencairan_kontrabon('$tanggal');");
		
		$result = array('list_field_kbo' => $query->list_fields(), 'query_kbo' => $query->result());
		$query->next_result();
		$query->free_result();
		return $result;
		
	}
	function get_gaji($tanggal){
		
		$query = $this->db->query("call fc_pencairan_gaji('$tanggal');");
		
		$result = array('list_field_gaji' => $query->list_fields(), 'query_gaji' => $query->result());
		$query->next_result();
		$query->free_result();
		return $result;
		
	}
	function get_honor($tanggal){
		
		$query = $this->db->query("call fc_pencairan_honor('$tanggal');");
		
		$result = array('list_field_honor' => $query->list_fields(), 'query_honor' => $query->result());
		$query->next_result();
		$query->free_result();
		return $result;
		
	}
	function get_bagi_hasil($tanggal){
		
		$query = $this->db->query("call fc_pencairan_bagi_hasil('$tanggal');");
		
		$result = array('list_field_bagi_hasil' => $query->list_fields(), 'query_bagi_hasil' => $query->result());
		$query->next_result();
		$query->free_result();
		return $result;
		
	}
	function get_retur($tanggal){
		
		$query = $this->db->query("call fc_pencairan_retur('$tanggal');");
		
		$result = array('list_field_retur' => $query->list_fields(), 'query_retur' => $query->result());
		$query->next_result();
		$query->free_result();
		return $result;
		
	}
	function rekap_penggajian($tanggal){
		
		$q="SELECT T.sumber_kas_id,M.nama,M.bank_id,MB.nama as bank,SUM(T.nominal_bayar) as total FROM (
				SELECT B.sumber_kas_id,B.nominal_bayar  FROM rka_pengajuan_pembayaran B
				WHERE B.`status`='1' AND B.tanggal_pencairan='".YMDFormat($tanggal)."'

				UNION ALL 

				SELECT B.sumber_kas_id,B.nominal_bayar  FROM tkasbon_pembayaran B
				WHERE B.`status`='1' AND B.tanggal_pencairan='".YMDFormat($tanggal)."'

				UNION ALL

				SELECT B.sumber_kas_id,B.nominal_bayar  FROM trefund_pembayaran B
				WHERE B.`status`='1' AND B.tanggal_pencairan='".YMDFormat($tanggal)."'

				UNION ALL

				SELECT B.dari as sumber_kas_id,B.nominal as nominal_bayar  FROM tmutasi_kas B
				WHERE B.`status`='1' AND B.tanggal_trx='".YMDFormat($tanggal)."'

				UNION ALL

				SELECT B.sumber_kas_id,B.nominal_bayar  FROM tkontrabon_pembayaran B
				WHERE B.`status`='1' AND B.tanggal_pencairan='".YMDFormat($tanggal)."'

				UNION ALL

				SELECT B.sumber_kas_id,B.nominal_bayar  FROM trekap_master_pembayaran B
				WHERE B.`status`='1' AND B.tanggal_pencairan='".YMDFormat($tanggal)."'
				
				UNION ALL
				
				SELECT B.sumber_kas_id,B.nominal_bayar  FROM thonor_pembayaran B
				WHERE B.`status`='1' AND B.tanggal_pencairan='".YMDFormat($tanggal)."'
				
				UNION ALL
				
				SELECT B.sumber_kas_id,B.nominal_bayar  FROM tbagi_hasil_pembayaran B
				WHERE B.`status`='1' AND B.tanggal_pencairan='".YMDFormat($tanggal)."'
				
				
				UNION ALL
				
				SELECT B.sumber_kas_id,B.nominal_bayar  FROM tretur_penerimaan_pembayaran B
				INNER JOIN tretur_penerimaan H ON H.id	=	B.idretur
				WHERE B.`status`='1' AND H.status_penerimaan='2' AND B.tanggal_pencairan='".YMDFormat($tanggal)."'
				
				
				) T LEFT JOIN
				msumber_kas M ON M.id=T.sumber_kas_id
				LEFT JOIN mbank MB ON MB.id=M.bank_id
				GROUP BY T.sumber_kas_id";
		
		
		return $this->db->query($q)->result();
		
	}
    public function saveData()
    {
        $this->tanggal_pencairan     = YMDFormat($_POST['tanggal_pencairan']);
        $this->status	 = 1;
        $this->tanggal_trx	 = date('Y-m-d H:i:s');
        $this->created_date	 = date('Y-m-d H:i:s');
        $this->created_by	 = $this->session->userdata('user_id');
		
        if ($this->db->insert('tpencairan', $this)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function save_verifikasi()
    {
        $this->catatan     = ($_POST['catatan']);
        $this->edited_date	 = date('Y-m-d H:i:s');
        $this->edited_by	 = $this->session->userdata('user_id');
		$this->db->where('id',$_POST['id']);
		if ($this->input->post('btn_simpan')=='1'){//verifikasi
			$this->st_verifikasi     = 1;
		}
		if ($this->input->post('btn_simpan')=='2'){//verifikasi
			$this->status     = 2;
			// $this->st_verifikasi     = 1;
		}
		
        if ($this->db->update('tpencairan', $this)) {
			$this->db->where('idpencairan',$_POST['id']);
			$this->db->delete('tpencairan_detail');
			
			$sumber_kas_id=$this->input->post('sumber_kas_id');
			$nominal=$this->input->post('nomina');
			foreach($sumber_kas_id as $index=>$val){
				$data_detail=array(
					'idpencairan' =>$_POST['id'],
					'sumber_kas_id' =>$sumber_kas_id[$index],
					'nominal' =>RemoveComma($nominal[$index]),
				);
				$this->db->insert('tpencairan_detail',$data_detail);
			}
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function check_duplicate($tanggal) {
		
		$this->db->where('status',1);
		$this->db->where('tanggal_pencairan', YMDFormat($tanggal));
		$query = $this->db->get('tpencairan');
		$count_row = $query->num_rows();
		// print_r($this->db->last_query());exit();
		if ($count_row > 0) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

    public function updateData()
    {
        $this->tanggal     = YMDFormat($_POST['tanggal']);
        $this->deskripsi	 = $_POST['deskripsi'];
        $this->catatan	   = $_POST['catatan'];

        if ($this->db->update('tpencairan', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function getDataPencairan($idpencairan)
    {
        $this->db->where('id', $idpencairan);
        $query = $this->db->get('tpencairan');
        return $query->row();
    }

    public function getPengajuan($search)
    {
        $this->db->select('id, nopengajuan AS notransaksi');
        $this->db->like('nopengajuan', $search);
        $this->db->where('stpencairan', 0);
        $query = $this->db->get('tpengajuan');
        return $query->result_array();
    }

    public function getKontrabon($search)
    {
        $this->db->select('id, nokontrabon AS notransaksi');
        $this->db->like('nokontrabon', $search);
        $this->db->where('stpencairan', 0);
        $query = $this->db->get('tkontrabon');
        return $query->result_array();
    }

    public function getCarm($search)
    {
        $this->db->select('id, no_transaksi AS notransaksi');
        $this->db->like('no_transaksi', $search);
        $this->db->where('tcarm_payments.stpencairan', 0);
        $query = $this->db->get('tcarm_payments');
        return $query->result_array();
    }

    public function getHonorDokter($search)
    {
        $this->db->select('thonor_dokter.id, mdokter.nama AS namadokter, thonor_dokter.tahun, thonor_dokter.bulan');
        $this->db->join('mdokter', 'mdokter.id = thonor_dokter.iddokter');
        $this->db->like('mdokter.nama', $search);
        $this->db->where('thonor_dokter.stpencairan', 0);
        $query = $this->db->get('thonor_dokter');
        return $query->result_array();
    }

    public function getKasbon($search)
    {
        $this->db->select('tkasbon.*,
        (CASE
          WHEN tkasbon.idtipe = 1 THEN
            mdokter.nama
          ELSE
            mpegawai.nama
        END) AS namapegawai');
        $this->db->join('mdokter', 'mdokter.id = tkasbon.idpegawai', 'LEFT');
        $this->db->join('mpegawai', 'mpegawai.id = tkasbon.idpegawai', 'LEFT');
        $this->db->where("(mdokter.nama LIKE '%".$search."%' OR mpegawai.nama LIKE '%".$search."%')", null, false);
        $this->db->where('tkasbon.stpencairan', 0);
        $this->db->order_by('tkasbon.id');
        $query = $this->db->get('tkasbon');
        return $query->result_array();
    }

    public function getGajiKaryawan($search)
    {
        $this->db->select('id, notransaksi');
        $this->db->like('notransaksi', $search);
        $this->db->where('trekap_penggajian.stpencairan', 0);
        $query = $this->db->get('trekap_penggajian');
        return $query->result_array();
    }

    public function getFeeKlinik($search)
    {
        $this->db->select('tverifikasi_rujukan_klinik_fee.id,
          mrumahsakit.nama AS namarumahsakit,
          tverifikasi_rujukan_klinik_fee.bulan,
          tverifikasi_rujukan_klinik_fee.tahun');
        $this->db->join('mrumahsakit', 'mrumahsakit.id = tverifikasi_rujukan_klinik_fee.idrumahsakit');
        $this->db->like('mrumahsakit.nama', $search);
        $this->db->where('tverifikasi_rujukan_klinik_fee.stpencairan', 0);
        $query = $this->db->get('tverifikasi_rujukan_klinik_fee');
        return $query->result_array();
    }

    // Aksi
    // Pengajuan, Kontrabon, C-Arm, Honor Dokter, Kasbon, Gaji Karyawan, Fee Klinik

    // Header Group : History
    public function getHeaderGroupKontrabon($tanggal, $idcarabayar)
    {
        $this->db->select('tanggalkontrabon AS tanggal,
        carabayar AS idcarabayar,
        (CASE WHEN carabayar = 1 THEN "Cheq" ELSE "Tunai" END) AS carabayar');
        $this->db->group_by('tanggalkontrabon');
        $this->db->group_by('carabayar');
        $this->db->where('tanggalkontrabon', $tanggal);
        $this->db->where('carabayar', $idcarabayar);
        $query = $this->db->get('tkontrabon');
        return $query->row();
    }

    public function getHeaderGroupCarm($idtransaksi)
    {
        $this->db->select('id AS idtransaksi, no_transaksi AS notransaksi');
        $this->db->where('id', $idtransaksi);
        $query = $this->db->get('tcarm_payments');
        return $query->row();
    }

    public function getHeaderGroupGajiKaryawan($idtransaksi)
    {
        $this->db->select('id AS idtransaksi, notransaksi');
        $this->db->where('id', $idtransaksi);
        $query = $this->db->get('trekap_penggajian');
        return $query->row();
    }

    // History
    public function getHistoryPengajuan($idpencairan)
    {
        $this->db->select('tpengajuan.*,
      tpencairan_detail.id AS idrow,
      tpencairan_detail.idtransaksi,
      musers.name AS namapemohon,
      munitpelayanan.nama AS namaunit,
      (CASE
        WHEN tpengajuan.jenispembayaran = 1 THEN
          "Tunai"
        WHEN tpengajuan.jenispembayaran = 2 THEN
          "Kontrabon"
        WHEN tpengajuan.jenispembayaran = 3 THEN
          "Transfer"
        WHEN tpengajuan.jenispembayaran = 4 THEN
          "Termin By Progress"
        WHEN tpengajuan.jenispembayaran = 5 THEN
          "Termin By Fix (Cicilan)"
      END) AS tipe,
      "-" AS termin');
        $this->db->join('tpengajuan', 'tpengajuan.id = tpencairan_detail.idtransaksi');
        $this->db->join('musers', 'musers.id = tpengajuan.idpemohon');
        $this->db->join('munitpelayanan', 'munitpelayanan.id = tpengajuan.untukbagian');
        $this->db->where('tpencairan_detail.idpencairan', $idpencairan);
        $this->db->where('tpencairan_detail.idtipe', 'pengajuan');
        $this->db->where('tpencairan_detail.status', 1);
        $this->db->order_by('tpencairan_detail.id');
        $query = $this->db->get('tpencairan_detail');
        return $query->result();
    }

    public function getHistoryKontrabon($idpencairan)
    {
        $this->db->select('tkontrabon.*,
      tpencairan_detail.id AS idrow,
      tpencairan_detail.idtransaksi,
      (CASE
        WHEN tkontrabon.carabayar = 1 THEN
          "Cheq"
        WHEN tkontrabon.carabayar = 2 THEN
          "Cash"
      END) AS tipe');
        $this->db->join('tkontrabon', 'tkontrabon.id = tpencairan_detail.idtransaksi');
        $this->db->where('tpencairan_detail.idpencairan', $idpencairan);
        $this->db->where('tpencairan_detail.idtipe', 'kontrabon');
        $this->db->where('tpencairan_detail.status', 1);
        $this->db->order_by('tkontrabon.tanggalkontrabon');
        $this->db->order_by('tkontrabon.carabayar');
        $query = $this->db->get('tpencairan_detail');
        return $query->result();
    }

    public function getHistoryCarm($idpencairan)
    {
        $this->db->select('tcarm_payments_detail.id,
      tcarm_payments_detail.id_transaksi AS idtransaksi,
      tcarm_payments.no_transaksi AS notransaksi,
      tcarm_payments_detail.nama_pemilik AS namapemilik,
      tcarm_payments_detail.total');
        $this->db->join('tcarm_payments_detail', 'tcarm_payments_detail.id_transaksi = tcarm_payments.id');
        $this->db->join('tpencairan_detail', 'tpencairan_detail.idtransaksi = tcarm_payments_detail.id');
        $this->db->where('tpencairan_detail.idpencairan', $idpencairan);
        $this->db->where('tpencairan_detail.idtipe', 'carm');
        $this->db->order_by('tcarm_payments.id');
        $query = $this->db->get('tcarm_payments');
        return $query->result();
    }

    public function getHistoryHonorDokter($idpencairan)
    {
        $this->db->select('thonor_dokter.*,
      tpencairan_detail.id AS idrow,
      tpencairan_detail.idtransaksi,
      mdokter.nama AS namadokter');
        $this->db->join('thonor_dokter', 'thonor_dokter.id = tpencairan_detail.idtransaksi');
        $this->db->join('mdokter', 'mdokter.id = thonor_dokter.iddokter');
        $this->db->where('tpencairan_detail.idpencairan', $idpencairan);
        $this->db->where('tpencairan_detail.idtipe', 'honordokter');
        $this->db->where('tpencairan_detail.status', 1);
        $this->db->order_by('tpencairan_detail.id');
        $query = $this->db->get('tpencairan_detail');
        return $query->result();
    }

    public function getHistoryKasbon($idpencairan)
    {
        $this->db->select('tkasbon.*,
      (CASE
        WHEN tkasbon.idtipe = 1 THEN
          "Dokter"
        ELSE
          "Pegawai"
      END) AS tipe,
      (CASE
        WHEN tkasbon.idtipe = 1 THEN
          mdokter.nama
        ELSE
          mpegawai.nama
      END) AS namapegawai');
        $this->db->join('tkasbon', 'tkasbon.id = tpencairan_detail.idtransaksi');
        $this->db->join('mdokter', 'mdokter.id = tkasbon.idpegawai', 'LEFT');
        $this->db->join('mpegawai', 'mpegawai.id = tkasbon.idpegawai', 'LEFT');
        $this->db->where('tpencairan_detail.idpencairan', $idpencairan);
        $this->db->where('tpencairan_detail.idtipe', 'kasbon');
        $this->db->where('tpencairan_detail.status', 1);
        $this->db->order_by('tpencairan_detail.id');
        $query = $this->db->get('tpencairan_detail');
        return $query->result();
    }

    public function getHistoryGajiKaryawan($idpencairan)
    {
        $this->db->select('trekap_penggajian.notransaksi,
        trekap_penggajian.id AS idtransaksi,
        trekap_penggajian_detail.*,
        (
          CASE WHEN trekap_penggajian_detail.idsub != 0 THEN
            mvariable_rekapan_sub.nama
          ELSE
            mvariable_rekapan.nama
          END
        ) AS namavariable,
        mvariable_rekapan.idtipe,
        mvariable_rekapan.idsub AS subrekapan');
        $this->db->join('tpencairan_detail', 'tpencairan_detail.idtransaksi = trekap_penggajian_detail.id');
        $this->db->join('trekap_penggajian', 'trekap_penggajian.id = trekap_penggajian_detail.idrekap');
        $this->db->join('mvariable_rekapan', 'mvariable_rekapan.id = trekap_penggajian_detail.idvariable', 'LEFT');
        $this->db->join('mvariable_rekapan_sub', 'mvariable_rekapan_sub.id = trekap_penggajian_detail.idsub', 'LEFT');
        $this->db->where('tpencairan_detail.idpencairan', $idpencairan);
        $this->db->where('tpencairan_detail.idtipe', 'gajikaryawan');
        $this->db->order_by('trekap_penggajian_detail.idvariable');
        $this->db->order_by('trekap_penggajian_detail.idsub');
        $query = $this->db->get('trekap_penggajian_detail');
        return $query->result();
    }

    public function getHistoryFeeKlinik($idpencairan)
    {
        $this->db->select('tverifikasi_rujukan_klinik_fee.*,
      tpencairan_detail.id AS idrow,
      tpencairan_detail.idtransaksi,
      mrumahsakit.nama AS namarumahsakit');
        $this->db->join('tverifikasi_rujukan_klinik_fee', 'tverifikasi_rujukan_klinik_fee.id = tpencairan_detail.idtransaksi');
        $this->db->join('mrumahsakit', 'mrumahsakit.id = tverifikasi_rujukan_klinik_fee.idrumahsakit');
        $this->db->where('tpencairan_detail.idpencairan', $idpencairan);
        $this->db->where('tpencairan_detail.idtipe', 'feeklinik');
        $this->db->where('tpencairan_detail.status', 1);
        $this->db->order_by('tpencairan_detail.id');
        $query = $this->db->get('tpencairan_detail');
        return $query->result();
    }
}

/* End of file Tpencairan_model.php */
/* Location: ./application/models/Tpencairan_model.php */
