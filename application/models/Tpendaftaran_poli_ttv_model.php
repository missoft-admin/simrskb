<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpendaftaran_poli_ttv_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function logic_akses_form_erm($pendaftaran_id,$st_ranap='0'){
		
		$data_login_ppa=get_ppa_login();
		$data=array();
		$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan,makral_satuan,mcahaya_satuan,mgcs_eye_satuan,mgcs_motion_satuan,mgcs_voice_satuan,mpupil_satuan,mspo2_satuan,mgds_satuan")->row_array();
		if ($st_ranap=='1'){
			$header=$this->get_data_header_ranap($pendaftaran_id);
			$data['pendaftaran_bedah_id']='';
			// print_r($st_ranap);exit;
			$data['list_trx_kamar_bedah']=$this->list_kamar_bedah($pendaftaran_id,1);
			
		}else{
			$data['pendaftaran_bedah_id']='';
			$header=$this->get_data_header($pendaftaran_id);
			$data['list_trx_kamar_bedah']=$this->list_kamar_bedah($pendaftaran_id,0);
		}
			$data_ews=$this->get_header_ews($pendaftaran_id,$st_ranap);
		// print_r($data_ews);exit;
		// $idtipe_poli=$header['idtipe'];
		if ($st_ranap=='1'){
			$data_header_ttv=$this->get_header_ttv_ranap($pendaftaran_id);
		}else{
			$data_header_ttv=$this->get_header_ttv($pendaftaran_id);
		}
		
		$logic_akses_ttv=$this->logic_akses_ttv($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_assemen_rj=$this->logic_akses_assemen_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_asmed=$this->logic_akses_asmed_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_asmed_igd=$this->logic_akses_asmed_igd($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_nyeri=$this->logic_akses_nyeri_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_triage=$this->logic_akses_triage_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_assesmen_igd=$this->logic_akses_assesmen_igd($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_cppt=$this->logic_akses_cppt($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_ic=$this->logic_akses_ic($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_edukasi=$this->logic_akses_edukasi($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_eresep=$this->logic_akses_eresep($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_asmed_fisio=$this->logic_akses_asmed_fisio($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_rencana_ranap=$this->logic_akses_rencana_ranap($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_rencana_bedah=$this->logic_akses_rencana_bedah($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_estimasi_biaya=$this->logic_akses_estimasi_biaya($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		
		$logic_akses_ttv_ri=$this->logic_akses_ttv_ri($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_asmed_ri=$this->logic_akses_asmed_ri($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_assesmen_ri=$this->logic_akses_assesmen_ri($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_risiko_jatuh=$this->logic_akses_risiko_jatuh($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_dpjp=$this->logic_akses_dpjp($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_pindah_dpjp=$this->logic_akses_pindah_dpjp($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_lokasi_operasi=$this->logic_akses_lokasi_operasi($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_sga=$this->logic_akses_sga($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_gizi_anak=$this->logic_akses_gizi_anak($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_gizi_lanjutan=$this->logic_akses_gizi_lanjutan($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_discarge=$this->logic_akses_discarge($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_rekon_obat=$this->logic_akses_rekon_obat($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_intra_hospital=$this->logic_akses_intra_hospital($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_tf_hospital=$this->logic_akses_tf_hospital($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_implementasi_kep=$this->logic_akses_implementasi_kep($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_timbang=$this->logic_akses_timbang($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_tindakan_anestesi=$this->logic_akses_tindakan_anestesi($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_risiko_tinggi=$this->logic_akses_risiko_tinggi($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_komunikasi=$this->logic_akses_komunikasi($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		
		$logic_akses_nyeri_ri=$this->logic_akses_nyeri_ri($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_fisio_ri=$this->logic_akses_fisio_ri($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_mpp=$this->logic_akses_mpp($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_evaluasi_mpp=$this->logic_akses_evaluasi_mpp($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_imp_mpp=$this->logic_akses_imp_mpp($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_assesmen_pulang=$this->logic_akses_assesmen_pulang($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_assesmen_askep=$this->logic_akses_assesmen_askep($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_keselamatan=$this->logic_akses_keselamatan($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_lap_bedah=$this->logic_akses_lap_bedah($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_pra_sedasi=$this->logic_akses_pra_sedasi($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_ringkasan_pulang=$this->logic_akses_ringkasan_pulang($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_pra_bedah=$this->logic_akses_pra_bedah($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_survey=$this->logic_akses_survey($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_ews=$this->logic_akses_ews($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_pews=$this->logic_akses_pews($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_observasi=$this->logic_akses_observasi($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_permintaan_dpjp=$this->logic_akses_permintaan_dpjp($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_aps=$this->logic_akses_aps($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_info_ods=$this->logic_akses_info_ods($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_tf_darah=$this->logic_akses_tf_darah($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_invasif=$this->logic_akses_invasif($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_icu=$this->logic_akses_icu($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_isolasi=$this->logic_akses_isolasi($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_dnr=$this->logic_akses_dnr($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_rohani=$this->logic_akses_rohani($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_privasi=$this->logic_akses_privasi($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_opinion=$this->logic_akses_opinion($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		
		$data=array_merge($data,$logic_akses_opinion,$logic_akses_privasi,$logic_akses_rohani,$logic_akses_dnr,$logic_akses_isolasi,$logic_akses_icu,$logic_akses_invasif,$logic_akses_tf_darah,$logic_akses_info_ods,$logic_akses_aps,$data_ews,$logic_akses_permintaan_dpjp,$logic_akses_observasi,$logic_akses_pews,$logic_akses_ews,$logic_akses_survey,$logic_akses_pra_bedah,$logic_akses_ringkasan_pulang,$logic_akses_pra_sedasi,$logic_akses_lap_bedah,$logic_akses_keselamatan,$logic_akses_assesmen_askep,$logic_akses_assesmen_pulang,$logic_akses_imp_mpp,$logic_akses_evaluasi_mpp,$logic_akses_mpp,$logic_akses_fisio_ri,$logic_akses_nyeri_ri,$logic_akses_komunikasi,$logic_akses_risiko_tinggi,$logic_akses_tindakan_anestesi,$logic_akses_timbang,$logic_akses_implementasi_kep,$logic_akses_tf_hospital,$logic_akses_intra_hospital,$logic_akses_rekon_obat,$logic_akses_discarge,$logic_akses_gizi_lanjutan,$logic_akses_gizi_anak,$logic_akses_sga,$logic_akses_lokasi_operasi,$logic_akses_pindah_dpjp,$logic_akses_dpjp,$logic_akses_risiko_jatuh,$logic_akses_assesmen_ri,$logic_akses_asmed_ri,$logic_akses_ttv_ri,$logic_akses_rencana_ranap,$logic_akses_estimasi_biaya,$logic_akses_rencana_bedah,$logic_akses_asmed_fisio,$logic_akses_eresep,$header,$data,$data_login_ppa,$data_satuan_ttv,$data_header_ttv,$logic_akses_ttv,$logic_akses_assemen_rj,$logic_akses_asmed,$logic_akses_asmed_igd,$logic_akses_nyeri,$logic_akses_triage,$logic_akses_assesmen_igd,$logic_akses_ic,$logic_akses_cppt,$logic_akses_edukasi);
		// print_r($data);exit;
		return $data;
	}
	function get_header_ews($pendaftaran_id,$st_ranap){
		
		if ($st_ranap=='1'){
			$q="
				SELECT H.warna_ews as warna_ews_head,CONCAT(H.total_skor_ews,' (EWS DEWASA)') as total_skor_ews_head,H.hasil_ews  as hasil_ews_head FROM `tpoliklinik_ews` H
				WHERE H.st_ranap='$st_ranap' AND H.pendaftaran_id_ranap='$pendaftaran_id' AND H.status_assemen='2'
				ORDER BY H.tanggal_input DESC LIMIT 1
			";
		}else{
			$q="
				SELECT H.warna_ews as warna_ews_head,CONCAT(H.total_skor_ews,' (EWS DEWASA)') as total_skor_ews_head,H.hasil_ews  as hasil_ews_head FROM `tpoliklinik_ews` H
				WHERE H.st_ranap='$st_ranap' AND H.pendaftaran_id='$pendaftaran_id'  AND H.status_assemen='2'
				ORDER BY H.tanggal_input DESC LIMIT 1
			";
		}
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'warna_ews_head'=>'',
				'total_skor_ews_head'=>'',
				'hasil_ews_head'=>'',
			);
		}
		if ($st_ranap=='1'){
			$q="
				SELECT H.warna_hasil as warna_ews_head,CONCAT(H.total_skor_pews,' (PEWS)') as total_skor_ews_head,H.nama_hasil_pengkajian as hasil_ews_head FROM `tranap_pews` H
				WHERE H.st_ranap='1' AND H.pendaftaran_id_ranap='$pendaftaran_id' AND H.status_assemen='2'
				ORDER BY H.tanggal_input DESC LIMIT 1
			";
		}else{
			$q="
				SELECT H.warna_hasil as warna_ews_head,CONCAT(H.total_skor_pews,' (PEWS)') as total_skor_ews_head,H.nama_hasil_pengkajian as hasil_ews_head FROM `tranap_pews` H
				WHERE H.st_ranap='1' AND H.pendaftaran_id='$pendaftaran_id' AND H.status_assemen='2'
				ORDER BY H.tanggal_input DESC LIMIT 1
			";
		}
		$data=$this->db->query($q)->row();
		if ($data){
			$hasil=array(
				'warna_ews_head'=>$data->warna_ews_head,
				'total_skor_ews_head'=>$data->total_skor_ews_head,
				'hasil_ews_head'=>$data->hasil_ews_head,
			);
		}
		return $hasil;
	}
	function list_kamar_bedah($pendaftaran_id,$idasalpendaftaran){
		$q="
			SELECT H.id as pendaftaran_bedah_id,H.notransaksi,MD.nama as nama_dokter,H.diagnosa,H.operasi,H.tanggaloperasi 
			FROM tkamaroperasi_pendaftaran H
			LEFT JOIN mdokter MD ON MD.id=H.dpjp_id
			WHERE H.idpendaftaran='$pendaftaran_id' AND H.st_ranap='$idasalpendaftaran' AND H.`status`!='0' AND H.statussetuju='1' AND H.statusdatang='1'
		";
		// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	function get_data_header($pendaftaran_id){
		$q_alergi=get_alergi_sql();
		$q="SELECT MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
			,MK.nama as nama_kelompok,CASE WHEN H.idkelompokpasien=1 THEN MR.nama ELSE '' END as nama_rekanan
			,H.*,H.title as title_nama,H.id as pendaftaran_id,COALESCE(TPA.risiko_jatuh,TPA2.risiko_jatuh) as var_header_risiko_jatuh
			,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header,
			H.iddokter AS dpjp, H.idtipe as idtipe_poli, H.idtipe AS asal_rujukan,HP.id as pendaftaran_id_ranap
			,'poli' as  st_poli_st_ranap, H.idtipe AS asal_rujukan_poli_ranap,K.status as statuskasir,H.idtipe as asalrujukan,K.status as statuskasir_rajal,
			0 AS idkelas,'' nama_tindakan_setuju,'0' as status_proses_card_pass
			FROM tpoliklinik_pendaftaran H
			LEFT JOIN mdokter MD ON MD.id=H.iddokter
			INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
			LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
			INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
			LEFT JOIN tpoliklinik_assesmen TPA ON TPA.assesmen_id=H.assesmen_id AND H.idtipe='1'
			LEFT JOIN tpoliklinik_assesmen_igd TPA2 ON TPA2.assesmen_id=H.assesmen_id AND H.idtipe='2'
			LEFT JOIN trawatinap_pendaftaran HP ON HP.idpoliklinik=H.id AND HP.status = 1
			LEFT JOIN tpoliklinik_tindakan TT ON TT.idpendaftaran=H.id
			LEFT JOIN tkasir K ON K.idtindakan=TT.id AND (K.idtipe IN (1,2))
			LEFT JOIN (
				".$q_alergi."
			) A ON A.idpasien=H.idpasien 
			WHERE H.id='$pendaftaran_id'
			GROUP BY H.id";
		// print_r($q);exit;
		return $this->db->query($q)->row_array();
	}
	function get_data_header_ranap($pendaftaran_id){
		$q_risiko="
			SELECT * FROM `tranap_risiko_jatuh` H WHERE H.pendaftaran_id_ranap='$pendaftaran_id' AND H.status_assemen='2' ORDER BY H.tanggal_input DESC,H.created_date DESC  LIMIT 1
		";
		$q_nyeri="
			SELECT *FROM tranap_assesmen_nyeri H
				WHERE H.pendaftaran_id_ranap='$pendaftaran_id' AND H.status_assemen='2' ORDER BY H.assesmen_id DESC LIMIT 1
		";
		$q_alergi=get_alergi_sql();
		$q="SELECT MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk,MD.idkategori as idkategori_dokter
			,MK.nama as nama_kelompok,CASE WHEN H.idkelompokpasien=1 THEN MR.nama ELSE '' END as nama_rekanan
			,H.*,H.title as title_nama,H.id as pendaftaran_id_ranap,COALESCE(TPA2.nama_hasil_pengkajian) as var_header_risiko_jatuh
			,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header,
			H.iddokterpenanggungjawab AS dpjp,H.iddokterpenanggungjawab AS iddokter, H.idtipe_asal as idtipe_poli, H.idtipe AS asal_rujukan,HP.id as pendaftaran_id
			,MRU.nama as nama_ruangan,CLS.nama as nama_kelas,MB.nama as nama_bed
			,NY.total_skor_nyeri as risiko_nyeri_nilai,NY.singkatan_kajian as risiko_nyeri_singkatan,'ranap' as  st_poli_st_ranap, 
			(CASE WHEN H.idtipe = 1 THEN 3 ELSE 4 END) AS asal_rujukan_poli_ranap, 
			(CASE WHEN H.statuskasir = '1' OR H.statuspembayaran = '1' THEN 2 ELSE 0 END) as statuskasir,
			(CASE WHEN H.statuskasir = '1' OR H.statuspembayaran = '1' THEN 2 ELSE 0 END) as statuskasir_rajal,
			3 as asalrujukan,TT.nama_tindakan_setuju
			FROM trawatinap_pendaftaran H
			LEFT JOIN tpoliklinik_pendaftaran HP ON HP.id = H.idpoliklinik
			LEFT JOIN mdokter MD ON MD.id=H.iddokterpenanggungjawab
			LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik_asal
			LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
			LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
			LEFT JOIN mruangan MRU ON MRU.id=H.idruangan
			LEFT JOIN mkelas CLS ON CLS.id=H.idkelas
			LEFT JOIN mbed MB ON MB.id=H.idbed
			LEFT JOIN tpoliklinik_assesmen TPA ON TPA.assesmen_id=HP.assesmen_id AND HP.idtipe='1'
			LEFT JOIN (
				".$q_risiko."
			) TPA2 ON TPA2.pendaftaran_id_ranap=H.id
			LEFT JOIN (
				".$q_nyeri."
			) NY ON NY.pendaftaran_id_ranap=H.id
			LEFT JOIN (
				".$q_alergi."
			) A ON A.idpasien=H.idpasien
			LEFT JOIN (
				".get_tindakan_persetujuan()."
			) TT ON TT.idrawatinap=H.id
			
			WHERE H.id='$pendaftaran_id'
			GROUP BY H.id";
		return $this->db->query($q)->row_array();
	}
	function get_default_warna_asmed(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT jenis_drawing,warna FROM tpoliklinik_asmed_default WHERE mppa_id='$login_ppa_id'";
		$data=$this->db->query($q)->row_array();
		if ($data){
			
		}else{
			$data=array(
				'warna'=>'#ff2525',
				'jenis_drawing'=>'bulat',
			);
		}
		return $data;
	}
	
	function get_lokalis($assesmen_id,$gambar_id,$versi_edit='#'){
		if ($versi_edit=='#'){
		$q="SELECT *FROM tpoliklinik_asmed_lokalis WHERE assesmen_id='$assesmen_id' AND gambar_id='$gambar_id'";
			
		}else{
			$q="
				SELECT *FROM(
					SELECT D.jml_edit as versi_edit,H.* 
					FROM tpoliklinik_asmed_lokalis H
					LEFT JOIN tpoliklinik_asmed D ON D.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'

					UNION ALL

					SELECT H.* 
					FROM tpoliklinik_asmed_his_lokalis H
					WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'
					) T 

					WHERE T.versi_edit='$versi_edit'
					ORDER BY T.`index` ASC
			";
		}
		// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	function list_ruang(){
		$q="SELECT * FROM `mtujuan` M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function list_ppa(){
		$q="SELECT * FROM `mppa` M WHERE M.`staktif`='1'";
		return $this->db->query($q)->result();
	}
	function list_poli(){
		$q="SELECT MP.id,MP.nama FROM `mppa_poli` H
			INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
			WHERE H.setting_lainnya='1' AND H.idpoliklinik NOT IN (SELECT idpoliklinik FROM mpoliklinik_emergency)
			GROUP BY H.idpoliklinik";
			
			// $q="SELECT *FROM mpoliklinik MP WHERE MP.`status`='1'";
		return $this->db->query($q)->result();
	}
		function list_dokter(){
		$q="SELECT *FROM mdokter M
WHERE M.`status`='1'
";
		return $this->db->query($q)->result();
	}
	function list_menu_kiri($menu_atas){
		$q="SELECT H.menu_kiri FROM tindakan_menu H WHERE H.menu_atas='$menu_atas'";
		return json_encode(array_column($this->db->query($q)->result_array(),'menu_kiri'));
	}
	function logic_akses_ttv($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_ttv,H.st_lihat as st_lihat_ttv,H.st_edit as st_edit_ttv,H.st_hapus as st_hapus_ttv,H.st_cetak as st_cetak_ttv
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_ttv_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_ttv_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_ttv_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat'=>'0',
				'st_input'=>'0',
				'st_edit'=>'0',
				'st_hapus'=>'0',
			);
		}
		return $hasil;
		
	}
	function logic_akses_ttv_ri($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_ttv_ri,H.st_lihat as st_lihat_ttv_ri,H.st_edit as st_edit_ttv_ri,H.st_hapus as st_hapus_ttv_ri,H.st_cetak as st_cetak_ttv_ri
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_ttv_ri_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_ttv_ri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_ttv_ri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_ttv_ri'=>'0',
				'st_input_ttv_ri'=>'0',
				'st_edit_ttv_ri'=>'0',
				'st_hapus_ttv_ri'=>'0',
				'st_cetak_ttv_ri'=>'0',
			);
		}
		return $hasil;
		
	}
	function logic_akses_assesmen($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_ttv,H.st_lihat as st_lihat_ttv,H.st_edit as st_edit_ttv,H.st_hapus as st_hapus_ttv,H.st_cetak as st_cetak_ttv
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_assesmen_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_assesmen_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_assesmen_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_ttv'=>'0',
				'st_input_ttv'=>'0',
				'st_edit_ttv'=>'0',
				'st_hapus_ttv'=>'0',
				'st_cetak_ttv'=>'0',
			);
		}
		return $hasil;
		
	}
	
	function list_template_assement(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_assesmen H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function list_template_asmed(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_asmed H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	function list_template_triage(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_triage H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function setting_ttv(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			FROM `setting_ttv` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function setting_ttv_ri(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			FROM `setting_ttv_ri` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function setting_assesmen(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			FROM `setting_assesmen` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	
	function get_data_ttv($pendaftaran_id){
		$q="SELECT id as ttv_id,pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,spo2,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv,suplemen_oksigen
			 FROM tpoliklinik_ttv WHERE pendaftaran_id='$pendaftaran_id' AND status_ttv!=0 ORDER BY id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$hasil=array(
					'ttv_id' => '',
					'pendaftaran_id' => '',
					'tanggal_input' => '',
					'tingkat_kesadaran' => '',
					'nadi' => '',
					'nafas' => '',
					'spo2' => '',
					'td_sistole' => '',
					'td_diastole' => '',
					'suhu' => '',
					'tinggi_badan' => '',
					'berat_badan' => '',
					'status_ttv' => '0',
					'suplemen_oksigen' => '0',
				);
			}else{
				if ($hasil['status_ttv']>1){
					$hasil=array(
					'ttv_id' => '',
					'pendaftaran_id' => '',
					'tanggal_input' => '',
					'tingkat_kesadaran' => '',
					'nadi' => '',
					'nafas' => '',
					'spo2' => '',
					'td_sistole' => '',
					'td_diastole' => '',
					'suhu' => '',
					'tinggi_badan' => '',
					'berat_badan' => '',
					'status_ttv' => '0',
					'suplemen_oksigen' => '0',
				);
				}
			}
			return $hasil;
	}
	function get_data_ttv_all($pendaftaran_id){
		$q="SELECT id as ttv_id,pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv
			 FROM tpoliklinik_ttv WHERE pendaftaran_id='$pendaftaran_id' AND status_ttv!=0 ORDER BY id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$hasil=array(
					'ttv_id' => '',
					'pendaftaran_id' => '',
					'tanggal_input' => '',
					'tingkat_kesadaran' => '',
					'nadi' => '',
					'nafas' => '',
					'td_sistole' => '',
					'td_diastole' => '',
					'suhu' => '',
					'tinggi_badan' => '',
					'berat_badan' => '',
					'status_ttv' => '0',
				);
			}
			return $hasil;
	}
	function get_data_ttv_assesmen($assesmen_id){
		$q="SELECT assesmen_id as assesmen_id,pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,spo2,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv
			 FROM tpoliklinik_assesmen_ttv WHERE assesmen_id='$assesmen_id' AND status_ttv!=0 ORDER BY id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$hasil=array(
					'ttv_id' => '',
					'pendaftaran_id' => '',
					'tanggal_input' => '',
					'tingkat_kesadaran' => '',
					'nadi' => '',
					'nafas' => '',
					'spo2' => '',
					'td_sistole' => '',
					'td_diastole' => '',
					'suhu' => '',
					'tinggi_badan' => '',
					'berat_badan' => '',
					'status_ttv' => '0',
				);
			}
			return $hasil;
	}
	function get_data_ttv_assesmen_trx($assesmen_id,$versi_edit){
		$q=" 
			 SELECT assesmen_id as assesmen_id,pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,spo2,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv
			 FROM tpoliklinik_assesmen_his_ttv WHERE assesmen_id='$assesmen_id' AND status_ttv!=0 AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT D.assesmen_id as assesmen_id,D.pendaftaran_id,D.tanggal_input,D.tingkat_kesadaran,D.nadi,D.nafas,D.spo2,D.td_sistole,D.td_diastole,D.suhu
			,D.tinggi_badan,D.berat_badan,D.status_ttv
			 FROM tpoliklinik_assesmen_ttv D 
			 INNER JOIN tpoliklinik_assesmen H ON H.assesmen_id=D.assesmen_id
			 WHERE D.assesmen_id='$assesmen_id' AND status_ttv!=0 AND H.jml_edit='$versi_edit'
			 
			 ";
			 $hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$hasil=array(
					'ttv_id' => '',
					'pendaftaran_id' => '',
					'tanggal_input' => '',
					'tingkat_kesadaran' => '',
					'nadi' => '',
					'nafas' => '',
					'td_sistole' => '',
					'td_diastole' => '',
					'suhu' => '',
					'tinggi_badan' => '',
					'berat_badan' => '',
					'status_ttv' => '0',
				);
			}
			return $hasil;
	}
	
	function get_data_ttv_assesmen_trx_last($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tingkat_kesadaran as tingkat_kesadaran_asal,nadi as nadi_asal,nafas as nafas_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal
			,suhu as suhu_asal,tinggi_badan as tinggi_badan_asal,berat_badan as  berat_badan_asal,status_ttv as status_ttv_asal
			 FROM tpoliklinik_assesmen_his_ttv WHERE assesmen_id='$assesmen_id' AND status_ttv!=0 AND versi_edit='$versi_edit' ORDER BY id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$hasil=array(
					'ttv_id' => '',
					'pendaftaran_id' => '',
					'tanggal_input' => '',
					'tingkat_kesadaran_asal' => '',
					'nadi_asal' => '',
					'nafas_asal' => '',
					'td_sistole_asal' => '',
					'td_diastole_asal' => '',
					'suhu_asal' => '',
					'tinggi_badan_asal' => '',
					'berat_badan_asal' => '',
					'status_ttv_asal' => '0',
				);
			}
			return $hasil;
	}
	function get_data_assesmen($pendaftaran_id){
		$q="SELECT *
			 FROM tpoliklinik_assesmen WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_assesmen_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_assesmen WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_assesmen_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_assesmen_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_assesmen H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_assesmen_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT st_anamnesa as st_anamnesa_asal,keluhan_utama as keluhan_utama_asal,nama_anamnesa as nama_anamnesa_asal,hubungan_anamnesa as hubungan_anamnesa_asal,st_riwayat_penyakit as st_riwayat_penyakit_asal,riwayat_penyakit_lainnya as riwayat_penyakit_lainnya_asal,riwayat_alergi as riwayat_alergi_asal,riwayat_pengobatan as riwayat_pengobatan_asal,hubungan_anggota_keluarga as hubungan_anggota_keluarga_asal,st_psikologis as st_psikologis_asal,st_sosial_ekonomi as st_sosial_ekonomi_asal,st_spiritual as st_spiritual_asal,pendidikan as pendidikan_asal,pekerjaan as pekerjaan_asal,st_nafsu_makan as st_nafsu_makan_asal,st_turun_bb as st_turun_bb_asal,st_mual as st_mual_asal,st_muntah as st_muntah_asal,st_fungsional as st_fungsional_asal,st_alat_bantu as st_alat_bantu_asal,st_cacat as st_cacat_asal,cacat_lain as cacat_lain_asal,header_risiko_jatuh as header_risiko_jatuh_asal,footer_risiko_jatuh as footer_risiko_jatuh_asal,skor_pengkajian as skor_pengkajian_asal,risiko_jatuh as risiko_jatuh_asal,nama_tindakan as nama_tindakan_asal,st_tindakan as st_tindakan_asal,st_tindakan_action as st_tindakan_action_asal,st_edukasi as st_edukasi_asal,st_menerima_info as st_menerima_info_asal,st_hambatan_edukasi as st_hambatan_edukasi_asal,st_penerjemaah as st_penerjemaah_asal,penerjemaah as penerjemaah_asal,catatan_edukasi as catatan_edukasi_asal 
				FROM tpoliklinik_assesmen_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function list_edukasi($assesmen_id){
		$q="SELECT M.id,M.judul,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM `medukasi` M
			LEFT JOIN tpoliklinik_assesmen_edukasi H ON H.assesmen_id='$assesmen_id' AND H.medukasi_id=M.id
			WHERE M.staktif='1'";
		return $this->db->query($q)->result();
	}
	
	function list_edukasi_asmed($assesmen_id){
		$q="SELECT M.id,M.judul,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM `medukasi` M
			LEFT JOIN tpoliklinik_asmed_edukasi H ON H.assesmen_id='$assesmen_id' AND H.medukasi_id=M.id
			WHERE M.staktif='1'";
		return $this->db->query($q)->result();
	}
	function list_edukasi_his($assesmen_id,$versi_edit){
		$q="SELECT H.medukasi_id as id,M.judul,'selected' as pilih FROM (
			SELECT H.medukasi_id,H.versi_edit FROM tpoliklinik_assesmen_his_edukasi H WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT D.medukasi_id,H.jml_edit as versi_edit FROM tpoliklinik_assesmen_edukasi D 
			LEFT JOIN tpoliklinik_assesmen H ON H.assesmen_id=D.assesmen_id
			WHERE D.assesmen_id='$assesmen_id'
			) H 
			INNER JOIN medukasi M ON M.id=H.medukasi_id
			WHERE H.versi_edit='$versi_edit'";
		// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	function list_edukasi_asmed_his($assesmen_id,$versi_edit){
		$q="SELECT H.medukasi_id as id,M.judul,'selected' as pilih FROM (
			SELECT H.medukasi_id,H.versi_edit FROM tpoliklinik_asmed_his_edukasi H WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT D.medukasi_id,H.jml_edit as versi_edit FROM tpoliklinik_asmed_edukasi D 
			LEFT JOIN tpoliklinik_asmed H ON H.assesmen_id=D.assesmen_id
			WHERE D.assesmen_id='$assesmen_id'
			) H 
			INNER JOIN medukasi M ON M.id=H.medukasi_id
			WHERE H.versi_edit='$versi_edit'";
		// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	function perubahan_edukasi($assesmen_id,$versi_edit){
		if ($versi_edit!=0){
			$versi_edit_last=$versi_edit -1;
		}
		$hasil_1='';
		$hasil_2='';
		if ($versi_edit!=0){
			
			$q="SELECT H.versi_edit, GROUP_CONCAT(M.judul) as judul FROM (
				SELECT H.medukasi_id,H.versi_edit FROM tpoliklinik_assesmen_his_edukasi H WHERE H.assesmen_id='$assesmen_id'
				UNION ALL
				SELECT D.medukasi_id,H.jml_edit as versi_edit FROM tpoliklinik_assesmen_edukasi D 
				LEFT JOIN tpoliklinik_assesmen H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'
				) H 
				INNER JOIN medukasi M ON M.id=H.medukasi_id
				WHERE H.versi_edit='$versi_edit'
				GROUP BY H.versi_edit
			";
			$hasil_1=$this->db->query($q)->row('judul');
			$q="SELECT H.versi_edit, GROUP_CONCAT(M.judul) as judul FROM (
				SELECT H.medukasi_id,H.versi_edit FROM tpoliklinik_assesmen_his_edukasi H WHERE H.assesmen_id='$assesmen_id'
				UNION ALL
				SELECT D.medukasi_id,H.jml_edit as versi_edit FROM tpoliklinik_assesmen_edukasi D 
				LEFT JOIN tpoliklinik_assesmen H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'
				) H 
				INNER JOIN medukasi M ON M.id=H.medukasi_id
				WHERE H.versi_edit='$versi_edit_last'
				GROUP BY H.versi_edit
			";
			$hasil_2=$this->db->query($q)->row('judul');
			
		}
		$data_edukasi=array(
			'edukasi_current'=>$hasil_1,
			'edukasi_last'=>$hasil_2,
		);
		return $data_edukasi;
	}
	function list_riwayat_penyakit($assesmen_id){
		$q="SELECT M.nilai as id,M.ref as nama,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM merm_referensi M
			LEFT JOIN tpoliklinik_assesmen_riwayat_penyakit H ON H.assesmen_id='$assesmen_id' AND H.penyakit_id=M.nilai
			WHERE M.ref_head_id='25' AND M.nilai<>0";
		return $this->db->query($q)->result();
	}
	
	function list_riwayat_penyakit_asmed($assesmen_id,$jenis='1',$versi_edit='#'){
		if ($versi_edit=='#'){
			
		$nama_tabel='';
			if ($jenis=='1'){
				$nama_tabel='tpoliklinik_asmed_riwayat_penyakit';
			}
			if ($jenis=='2'){
				$nama_tabel='tpoliklinik_asmed_riwayat_penyakit_dahulu';
			}
			if ($jenis=='3'){
				$nama_tabel='tpoliklinik_asmed_riwayat_penyakit_keluarga';
			}
			$q="SELECT M.nilai as id,M.ref as nama,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM merm_referensi M
				LEFT JOIN ".$nama_tabel." H ON H.assesmen_id='$assesmen_id' AND H.penyakit_id=M.nilai
				WHERE M.ref_head_id='25' AND M.nilai<>0";
		}else{
			$q="
				SELECT M.nilai as id,H.jenis,M.ref as nama,CASE WHEN H.penyakit_id IS NOT NULL THEN 'selected' ELSE '' END as pilih 
				FROM merm_referensi M
				LEFT JOIN (
				SELECT 1 as jenis,H.jml_edit as versi_edit,D.penyakit_id FROM tpoliklinik_asmed H
				LEFT JOIN tpoliklinik_asmed_riwayat_penyakit D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id'

				UNION ALL
				SELECT 1 as jenis, H.versi_edit,H.penyakit_id FROM tpoliklinik_asmed_his_riwayat_penyakit H WHERE H.assesmen_id='$assesmen_id'

				UNION ALL

				SELECT 2 as jenis,H.jml_edit as versi_edit,D.penyakit_id FROM tpoliklinik_asmed H
				LEFT JOIN tpoliklinik_asmed_riwayat_penyakit_dahulu D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id'

				UNION ALL
				SELECT 2 as jenis, H.versi_edit,H.penyakit_id FROM tpoliklinik_asmed_his_riwayat_penyakit_dahulu H WHERE H.assesmen_id='$assesmen_id'

				UNION ALL

				SELECT 3 as jenis,H.jml_edit as versi_edit,D.penyakit_id FROM tpoliklinik_asmed H
				LEFT JOIN tpoliklinik_asmed_riwayat_penyakit_keluarga D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id'

				UNION ALL
				SELECT 3 as jenis, H.versi_edit,H.penyakit_id FROM tpoliklinik_asmed_his_riwayat_penyakit_keluarga H WHERE H.assesmen_id='$assesmen_id'
				) H ON H.penyakit_id=M.nilai
				WHERE M.ref_head_id='25' AND H.jenis='$jenis' AND H.versi_edit='$versi_edit'
			";
		}
		return $this->db->query($q)->result();
	}
	function get_header_ttv($pendaftaran_id){
		// $q="SELECT 
				// H.nadi as header_nadi
				// ,mnadi.warna as warna_nadi
				// ,H.nafas as  header_nafas,mnafas.warna as warna_nafas
				// ,H.td_sistole as header_td_sistole,mtd_sistole.warna as warna_sistole
				// ,H.td_diastole as header_td_diastole,mtd_diastole.warna as warna_diastole
				// ,H.suhu as header_suhu,msuhu.warna as warna_suhu
				// ,H.tinggi_badan as header_tinggi_badan
				// ,H.berat_badan as header_berat_badan
				// ,H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) as masa_tubuh,mberat.warna as warna_berat
				// FROM tpoliklinik_ttv H
				// LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
				// LEFT JOIN mnafas ON 12 BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
				// LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
				// LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
				// LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
				// LEFT JOIN mberat ON H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
				
				// WHERE pendaftaran_id='$pendaftaran_id' AND status_ttv > 1 
				// ORDER BY H.id DESC LIMIT 1";
			$q="
				SELECT *FROM (
					SELECT H.tanggal_input,
									H.nadi as header_nadi
									,mnadi.warna as warna_nadi
									,H.nafas as  header_nafas,mnafas.warna as warna_nafas
									,H.td_sistole as header_td_sistole,mtd_sistole.warna as warna_sistole
									,H.td_diastole as header_td_diastole,mtd_diastole.warna as warna_diastole
									,H.suhu as header_suhu,msuhu.warna as warna_suhu
									,H.tinggi_badan as header_tinggi_badan
									,H.berat_badan as header_berat_badan
									,H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) as masa_tubuh,mberat.warna as warna_berat
									,H.spo2 as header_spo2,mspo2.warna as warna_spo2,H.tingkat_kesadaran as header_tingkat_kesadaran
									FROM tpoliklinik_assesmen_igd H
									LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
									LEFT JOIN mnafas ON 12 BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
									LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
									LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
									LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
									LEFT JOIN mberat ON H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
									LEFT JOIN mspo2 ON H.spo2 BETWEEN mspo2.spo2_1 AND mspo2.spo2_2 AND mspo2.staktif='1'
									WHERE H.pendaftaran_id='$pendaftaran_id' AND H.status_assemen='2'
									
					UNION ALL

					SELECT H.tanggal_input,
									H.nadi as header_nadi
									,mnadi.warna as warna_nadi
									,H.nafas as  header_nafas,mnafas.warna as warna_nafas
									,H.td_sistole as header_td_sistole,mtd_sistole.warna as warna_sistole
									,H.td_diastole as header_td_diastole,mtd_diastole.warna as warna_diastole
									,H.suhu as header_suhu,msuhu.warna as warna_suhu
									,H.tinggi_badan as header_tinggi_badan
									,H.berat_badan as header_berat_badan
									,H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) as masa_tubuh,mberat.warna as warna_berat
									,H.spo2 as header_spo2,mspo2.warna as warna_spo2,H.tingkat_kesadaran as header_tingkat_kesadaran
									FROM tpoliklinik_ttv H
									LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
									LEFT JOIN mnafas ON 12 BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
									LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
									LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
									LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
									LEFT JOIN mberat ON H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
									LEFT JOIN mspo2 ON H.spo2 BETWEEN mspo2.spo2_1 AND mspo2.spo2_2 AND mspo2.staktif='1'
									WHERE pendaftaran_id='$pendaftaran_id' AND status_ttv =2 
									) T ORDER BY T.tanggal_input DESC
									LIMIT 1
									
			";
			// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$data_header=array(
					'header_nadi'=>'',
					'warna_nadi'=>'',
					'header_nafas'=>'',
					'warna_nafas'=>'',
					'header_td_sistole'=>'',
					'warna_sistole'=>'',
					'header_td_diastole'=>'',
					'warna_diastole'=>'',
					'header_suhu'=>'',
					'warna_suhu'=>'',
					'header_tinggi_badan'=>'',
					'warna_berat'=>'',
					'header_berat_badan'=>'',
					'header_spo2'=>'',
					'warna_spo2'=>'',
				);
			}else{$data_header=$hasil;}
			return $data_header;
	}
	function get_header_ttv_ranap($pendaftaran_id){
		
			$q="
				SELECT *FROM (
					".get_ttv_ranap_sql_pendaftaran($pendaftaran_id)."
					
				) T ORDER BY T.tanggal_input DESC LIMIT 1
									
			";
			 $hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$data_header=array(
					'header_nadi'=>'',
					'warna_nadi'=>'',
					'header_nafas'=>'',
					'warna_nafas'=>'',
					'header_td_sistole'=>'',
					'warna_sistole'=>'',
					'header_td_diastole'=>'',
					'warna_diastole'=>'',
					'header_suhu'=>'',
					'warna_suhu'=>'',
					'header_tinggi_badan'=>'',
					'warna_berat'=>'',
					'header_berat_badan'=>'',
					'header_spo2'=>'',
					'warna_spo2'=>'',
					'header_tingkat_kesadaran'=>'',
				);
			}else{$data_header=$hasil;}
			return $data_header;
	}
	function get_header_ttv_igd($assesmen_id){
		// $q="SELECT 
				// H.nadi as header_nadi
				// ,mnadi.warna as warna_nadi
				// ,H.nafas as  header_nafas,mnafas.warna as warna_nafas
				// ,H.td_sistole as header_td_sistole,mtd_sistole.warna as warna_sistole
				// ,H.td_diastole as header_td_diastole,mtd_diastole.warna as warna_diastole
				// ,H.suhu as header_suhu,msuhu.warna as warna_suhu
				// ,H.tinggi_badan as header_tinggi_badan
				// ,H.berat_badan as header_berat_badan
				// ,H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) as masa_tubuh,mberat.warna as warna_berat
				// FROM tpoliklinik_assesmen_igd H
				// LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
				// LEFT JOIN mnafas ON 12 BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
				// LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
				// LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
				// LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
				// LEFT JOIN mberat ON H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
				
				// WHERE H.assesmen_id='$assesmen_id' ";
			$q="
				SELECT *FROM (
					SELECT H.tanggal_input,
									H.nadi as header_nadi
									,mnadi.warna as warna_nadi
									,H.nafas as  header_nafas,mnafas.warna as warna_nafas
									,H.td_sistole as header_td_sistole,mtd_sistole.warna as warna_sistole
									,H.td_diastole as header_td_diastole,mtd_diastole.warna as warna_diastole
									,H.suhu as header_suhu,msuhu.warna as warna_suhu
									,H.tinggi_badan as header_tinggi_badan
									,H.berat_badan as header_berat_badan
									,H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) as masa_tubuh,mberat.warna as warna_berat
									FROM tpoliklinik_assesmen_igd H
									LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
									LEFT JOIN mnafas ON 12 BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
									LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
									LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
									LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
									LEFT JOIN mberat ON H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
									
									WHERE H.pendaftaran_id='$pendaftaran_id' AND H.status_assemen='2'
									
					UNION ALL

					SELECT H.tanggal_input,
									H.nadi as header_nadi
									,mnadi.warna as warna_nadi
									,H.nafas as  header_nafas,mnafas.warna as warna_nafas
									,H.td_sistole as header_td_sistole,mtd_sistole.warna as warna_sistole
									,H.td_diastole as header_td_diastole,mtd_diastole.warna as warna_diastole
									,H.suhu as header_suhu,msuhu.warna as warna_suhu
									,H.tinggi_badan as header_tinggi_badan
									,H.berat_badan as header_berat_badan
									,H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) as masa_tubuh,mberat.warna as warna_berat
									FROM tpoliklinik_ttv H
									LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
									LEFT JOIN mnafas ON 12 BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
									LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
									LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
									LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
									LEFT JOIN mberat ON H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
									
									WHERE pendaftaran_id='$pendaftaran_id' AND status_ttv =2 
									) T ORDER BY T.tanggal_input DESC
									LIMIT 1
									
			";
			 $hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$data_header=array(
					'header_nadi'=>'',
					'warna_nadi'=>'',
					'header_nafas'=>'',
					'warna_nafas'=>'',
					'header_td_sistole'=>'',
					'warna_sistole'=>'',
					'header_td_diastole'=>'',
					'warna_diastole'=>'',
					'header_suhu'=>'',
					'warna_suhu'=>'',
					'header_tinggi_badan'=>'',
					'warna_berat'=>'',
					'header_berat_badan'=>'',
				);
			}else{$data_header=$hasil;}
			return $data_header;
	}
	
	function logic_akses_assemen_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_assesmen,H.st_lihat as st_lihat_assesmen,H.st_edit as st_edit_assesmen,H.st_hapus as st_hapus_assesmen,H.st_cetak as st_cetak_assesmen
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_assesmen_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_assesmen_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_assesmen_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_assesmen'=>'0',
				'st_input_assesmen'=>'0',
				'st_edit_assesmen'=>'0',
				'st_hapus_assesmen'=>'0',
				'st_cetak_assesmen'=>'0',
			);
		}
		return $hasil;
		
	}
	
	//ASMED
	function get_data_asmed($pendaftaran_id){
		$q="SELECT *
			 FROM tpoliklinik_asmed WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function logic_akses_asmed_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_asmed,H.st_lihat as st_lihat_asmed,H.st_edit as st_edit_asmed,H.st_hapus as st_hapus_asmed,H.st_cetak as st_cetak_asmed
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_asmed_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_asmed_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_asmed_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_asmed'=>'0',
				'st_input_asmed'=>'0',
				'st_edit_asmed'=>'0',
				'st_hapus_asmed'=>'0',
				'st_cetak_asmed'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	function setting_asmed(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			,H.st_radiologi,H.st_lab,H.st_eresep

			FROM `setting_asmed` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	
	
	function get_data_asmed_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_asmed WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function logic_akses_asmed($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_ttv,H.st_lihat as st_lihat_ttv,H.st_edit as st_edit_ttv,H.st_hapus as st_hapus_ttv,H.st_cetak as st_cetak_ttv
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_asmed_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_asmed_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_asmed_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_ttv'=>'0',
				'st_input_ttv'=>'0',
				'st_edit_ttv'=>'0',
				'st_hapus_ttv'=>'0',
				'st_cetak_ttv'=>'0',
			);
		}
		return $hasil;
		
	}
	//FISIO
	//ASMED
	function get_default_warna_asmed_fisio(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT jenis_drawing,warna FROM tpoliklinik_asmed_fisio_default WHERE mppa_id='$login_ppa_id'";
		$data=$this->db->query($q)->row_array();
		if ($data){
			
		}else{
			$data=array(
				'warna'=>'#ff2525',
				'jenis_drawing'=>'bulat',
			);
		}
		return $data;
	}
	function setting_asmed_fisio(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_fisio` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function get_data_asmed_fisio($pendaftaran_id){
		$q="SELECT *
			 FROM tpoliklinik_asmed_fisio WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function logic_akses_asmed_fisio_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_asmed_fisio,H.st_lihat as st_lihat_asmed_fisio,H.st_edit as st_edit_asmed_fisio,H.st_hapus as st_hapus_asmed_fisio,H.st_cetak as st_cetak_asmed_fisio
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_fisio_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_fisio_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_fisio_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_asmed_fisio'=>'0',
				'st_input_asmed_fisio'=>'0',
				'st_edit_asmed_fisio'=>'0',
				'st_hapus_asmed_fisio'=>'0',
				'st_cetak_asmed_fisio'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	function setting_fisio(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			,H.st_radiologi,H.st_lab,H.st_eresep

			FROM `setting_fisio` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	
	function get_data_asmed_fisio_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_asmed_fisio WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	//NYERI
	function setting_nyeri(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_nyeri` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function get_data_nyeri($pendaftaran_id){
		$q="SELECT *
			 FROM tpoliklinik_nyeri WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function logic_akses_nyeri_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_nyeri,H.st_lihat as st_lihat_nyeri,H.st_edit as st_edit_nyeri,H.st_hapus as st_hapus_nyeri,H.st_cetak as st_cetak_nyeri
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_nyeri_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_nyeri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_nyeri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_nyeri'=>'0',
				'st_input_nyeri'=>'0',
				'st_edit_nyeri'=>'0',
				'st_hapus_nyeri'=>'0',
				'st_cetak_nyeri'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	function default_logic_nyeri($data){
		$idtipe=$data['idtipe'];
		$idpoli=$data['idpoliklinik'];
		$statuspasienbaru=$data['statuspasienbaru'];
		$pertemuan_id=$data['pertemuan_id'];
		$umur_tahun=$data['umurtahun'];
		$umur_bulan=$data['umurbulan'];
		$umur_hari=$data['umurhari'];
		$q="SELECT *FROM (
				SELECT '7' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe'
				UNION ALL
				SELECT '6.0' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli'
				UNION ALL
				SELECT '6.1' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='0'
				UNION ALL
				SELECT '5.0' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru'
				UNION ALL
				SELECT '5.1' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='0' AND H.statuspasienbaru='$statuspasienbaru'
				UNION ALL
				SELECT '4.0' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru' AND H.pertemuan_id='$pertemuan_id'
				UNION ALL
				SELECT '4.1' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='2' AND H.pertemuan_id='$pertemuan_id'
				UNION ALL
				SELECT '4.2' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='0' AND H.statuspasienbaru='2' AND H.pertemuan_id='$pertemuan_id'
				UNION ALL
				SELECT '4.3' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='0' AND H.statuspasienbaru='2' AND H.pertemuan_id='0'

				UNION ALL
				SELECT '3.0' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru' AND H.pertemuan_id='$pertemuan_id' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				UNION ALL
				SELECT '3.1' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				UNION ALL
				SELECT '3.2' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='2' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				UNION ALL
				SELECT '3.3' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='0' AND H.statuspasienbaru='2' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				UNION ALL
				SELECT '2.0' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru' AND H.pertemuan_id='$pertemuan_id' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan))
				UNION ALL
				SELECT '2.1' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan))
				UNION ALL
				SELECT '2.2' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='2' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan))
				UNION ALL
				SELECT '2.3' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='0' AND H.statuspasienbaru='2' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan))

				UNION ALL
				SELECT '1.0' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru' AND H.pertemuan_id='$pertemuan_id' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan)) AND (H.operand_hari != '0' AND calculate_logic(H.operand_hari,'$umur_hari',H.umur_hari))
				UNION ALL
				SELECT '1.1' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='$statuspasienbaru' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan)) AND (H.operand_hari != '0' AND calculate_logic(H.operand_hari,'$umur_hari',H.umur_hari))
				UNION ALL
				SELECT '1.2' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='$idpoli' AND H.statuspasienbaru='2' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan)) AND (H.operand_hari != '0' AND calculate_logic(H.operand_hari,'$umur_hari',H.umur_hari))
				UNION ALL
				SELECT '1.3' as lev, H.mnyeri_id,H.id FROM msetting_nyeri_default_detail H
				WHERE H.idtipe='$idtipe' AND H.idpoli='0' AND H.statuspasienbaru='2' AND H.pertemuan_id='0' AND (H.operand_tahun != '0' AND calculate_logic(H.operand_tahun,'$umur_tahun',H.umur_tahun))
				AND (H.operand_bulan != '0' AND calculate_logic(H.operand_bulan,'$umur_bulan',H.umur_bulan)) AND (H.operand_hari != '0' AND calculate_logic(H.operand_hari,'$umur_hari',H.umur_hari))
				) H ORDER BY H.lev asc
		LIMIT 1";
		$row=$this->db->query($q)->row('mnyeri_id');
		if ($row){
			return $row;
		}else{
			return 0;
		}
	}
	function setting_default_nyeri(){
		$q="SELECT st_spesifik_nyeri,st_kunci_default_nyeri FROM msetting_nyeri_default";
		return $this->db->query($q)->row_array();
	}
	function logic_akses_nyeri($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_nyeri,H.st_lihat as st_lihat_nyeri,H.st_edit as st_edit_nyeri,H.st_hapus as st_hapus_nyeri,H.st_cetak as st_cetak_nyeri
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_nyeri_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_nyeri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_nyeri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_nyeri'=>'0',
				'st_input_nyeri'=>'0',
				'st_edit_nyeri'=>'0',
				'st_hapus_nyeri'=>'0',
				'st_cetak_nyeri'=>'0',
			);
		}
		return $hasil;
		
	}
	function get_data_nyeri_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_nyeri WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_nyeri_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_nyeri_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_nyeri H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_nyeri_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT lokasi_nyeri as lokasi_nyeri_asal,penyebab_nyeri as penyebab_nyeri_asal,durasi_nyeri as durasi_nyeri_asal,frek_nyeri as frek_nyeri_asal,jenis_nyeri as jenis_nyeri_asal,skala_nyeri as skala_nyeri_asal,karakteristik_nyeri as karakteristik_nyeri_asal,nyeri_hilang as nyeri_hilang_asal,total_skor_nyeri as total_skor_nyeri_asal
				FROM tpoliklinik_nyeri_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_asmed_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT st_anamnesa as st_anamnesa_asal,keluhan_utama as keluhan_utama_asal,nama_anamnesa as nama_anamnesa_asal,hubungan_anamnesa as hubungan_anamnesa_asal
			,st_riwayat_penyakit as st_riwayat_penyakit_asal,riwayat_penyakit_lainnya as riwayat_penyakit_lainnya_asal
			,st_riwayat_penyakit_dahulu as st_riwayat_penyakit_dahulu_asal,riwayat_penyakit_dahulu as riwayat_penyakit_dahulu_asal
			,st_riwayat_penyakit_keluarga as st_riwayat_penyakit_keluarga_asal,riwayat_penyakit_keluarga as riwayat_penyakit_keluarga_asal
			,template_gambar as template_gambar_asal,gambar_tubuh as gambar_tubuh_asal,pemeriksaan_penunjang as pemeriksaan_penunjang_asal
			,rencana_asuhan as rencana_asuhan_asal,intruksi as intruksi_asal
			,rencana_kontrol as rencana_kontrol_asal,tanggal_kontrol as tanggal_kontrol_asal,idpoli_kontrol as idpoli_kontrol_asal
			,iddokter_kontrol as iddokter_kontrol_asal,st_edukasi as st_edukasi_asal,catatan_edukasi as catatan_edukasi_asal
			,diagnosa_utama as diagnosa_utama_asal,txt_keterangan as txt_keterangan_asal
				FROM tpoliklinik_asmed_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_asmed_fisio_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				st_anamnesa as st_anamnesa_asal,keluhan_utama as keluhan_utama_asal,nama_anamnesa as nama_anamnesa_asal,hubungan_anamnesa as hubungan_anamnesa_asal,diagnosa_medis as diagnosa_medis_asal,diagnosa_medis_nama as diagnosa_medis_nama_asal,template_gambar as template_gambar_asal,gambar_tubuh as gambar_tubuh_asal,txt_keterangan as txt_keterangan_asal,gambar_id as gambar_id_asal,nadi as nadi_asal,nafas as nafas_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,suhu as suhu_asal,tinggi_badan as tinggi_badan_asal,berat_badan as berat_badan_asal,nyeri_tekan as nyeri_tekan_asal,nyeri_gerak as nyeri_gerak_asal,nyeri_diam as nyeri_diam_asal,palpasi as palpasi_asal,lepas_gerak_sendi as lepas_gerak_sendi_asal,kekuatan_otot as kekuatan_otot_asal,ket_nyeri as ket_nyeri_asal,jenis_nyeri as jenis_nyeri_asal,statis as statis_asal,dinamis as dinamis_asal,kognotif as kognotif_asal,auskultasi as auskultasi_asal,antropometri as antropometri_asal,hasil_pemeriksaan as hasil_pemeriksaan_asal,pemeriksaan_khusus as pemeriksaan_khusus_asal,diagnosa_fisio_nama as diagnosa_fisio_nama_asal,diagnosa_fisio as diagnosa_fisio_asal,impairment as impairment_asal,functional_limitasi as functional_limitasi_asal,disability as disability_asal,total_skor_nyeri as total_skor_nyeri_asal
				FROM tpoliklinik_asmed_fisio_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_asmed_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_asmed_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_asmed H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_asmed_fisio_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_asmed_fisio_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_asmed_fisio H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	//TRIAGE
	function get_data_triage($pendaftaran_id){
		$q="SELECT *
			 FROM tpoliklinik_triage WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_triage(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_triage` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function logic_akses_triage_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_triage,H.st_lihat as st_lihat_triage,H.st_edit as st_edit_triage,H.st_hapus as st_hapus_triage,H.st_cetak as st_cetak_triage
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_triage_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_triage_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_triage_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_triage'=>'0',
				'st_input_triage'=>'0',
				'st_edit_triage'=>'0',
				'st_hapus_triage'=>'0',
				'st_cetak_triage'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function logic_akses_triage($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_triage,H.st_lihat as st_lihat_triage,H.st_edit as st_edit_triage,H.st_hapus as st_hapus_triage,H.st_cetak as st_cetak_triage
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_triage_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_triage_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_triage_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_triage'=>'0',
				'st_input_triage'=>'0',
				'st_edit_triage'=>'0',
				'st_hapus_triage'=>'0',
				'st_cetak_triage'=>'0',
			);
		}
		return $hasil;
		
	}
	function get_data_triage_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_triage WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_triage_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_triage_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_triage H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_triage_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_datang as tanggal_datang_asal,sarana_transport as sarana_transport_asal,surat_pengantar as surat_pengantar_asal,asal_rujukan as asal_rujukan_asal,cara_masuk as cara_masuk_asal,macam_kasus as macam_kasus_asal,gcs_eye as gcs_eye_asal,gcs_voice as gcs_voice_asal,gcs_motion as gcs_motion_asal,pupil_1 as pupil_1_asal,pupil_2 as pupil_2_asal,reflex_cahaya_1 as reflex_cahaya_1_asal,reflex_cahaya_2 as reflex_cahaya_2_asal,spo2 as spo2_asal,akral as akral_asal,nadi as nadi_asal,nafas as nafas_asal,suhu as suhu_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,tinggi_badan as tinggi_badan_asal,berat_badan as berat_badan_asal,kasus_alergi as kasus_alergi_asal,alergi as alergi_asal,st_psikologi as st_psikologi_asal,risiko_jatuh_morse as risiko_jatuh_morse_asal,risiko_pasien_anak as risiko_pasien_anak_asal,kategori_id as kategori_id_asal,pemeriksaan as pemeriksaan_asal,respon_time as respon_time_asal,ket_nadi as ket_nadi_asal,idasalpasien as idasalpasien_asal,idrujukan as idrujukan_asal,namapengantar as namapengantar_asal,teleponpengantar as teleponpengantar_asal
				FROM tpoliklinik_triage_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	//KOMUNIKASI
	function get_data_komunikasi($pendaftaran_id,$st_ranap){
		if ($st_ranap=='0'){
			$q="SELECT *
			 FROM tpoliklinik_komunikasi WHERE pendaftaran_id='$pendaftaran_id' AND st_ranap='$st_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}else{
			$q="SELECT *
			 FROM tpoliklinik_komunikasi WHERE pendaftaran_id_ranap='$pendaftaran_id' AND st_ranap='$st_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	// function setting_komunikasi(){
		// $q="SELECT H.judul_header,H.judul_footer 
			// ,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			// FROM `setting_komunikasi` H

			// WHERE H.id='1'";
		// return $this->db->query($q)->row_array();
	// }
	function logic_akses_komunikasi_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_komunikasi,H.st_lihat as st_lihat_komunikasi,H.st_edit as st_edit_komunikasi,H.st_hapus as st_hapus_komunikasi,H.st_cetak as st_cetak_komunikasi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_komunikasi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_komunikasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_komunikasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_komunikasi'=>'0',
				'st_input_komunikasi'=>'0',
				'st_edit_komunikasi'=>'0',
				'st_hapus_komunikasi'=>'0',
				'st_cetak_komunikasi'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function logic_akses_komunikasi($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_komunikasi,H.st_lihat as st_lihat_komunikasi,H.st_edit as st_edit_komunikasi,H.st_hapus as st_hapus_komunikasi,H.st_cetak as st_cetak_komunikasi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_komunikasi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_komunikasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_komunikasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_komunikasi'=>'0',
				'st_input_komunikasi'=>'0',
				'st_edit_komunikasi'=>'0',
				'st_hapus_komunikasi'=>'0',
				'st_cetak_komunikasi'=>'0',
			);
		}
		return $hasil;
		
	}
	function get_data_komunikasi_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_komunikasi WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_komunikasi_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_komunikasi_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_komunikasi H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_komunikasi_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_datang as tanggal_datang_asal,sarana_transport as sarana_transport_asal,surat_pengantar as surat_pengantar_asal,asal_rujukan as asal_rujukan_asal,cara_masuk as cara_masuk_asal,macam_kasus as macam_kasus_asal,gcs_eye as gcs_eye_asal,gcs_voice as gcs_voice_asal,gcs_motion as gcs_motion_asal,pupil_1 as pupil_1_asal,pupil_2 as pupil_2_asal,reflex_cahaya_1 as reflex_cahaya_1_asal,reflex_cahaya_2 as reflex_cahaya_2_asal,spo2 as spo2_asal,akral as akral_asal,nadi as nadi_asal,nafas as nafas_asal,suhu as suhu_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,tinggi_badan as tinggi_badan_asal,berat_badan as berat_badan_asal,kasus_alergi as kasus_alergi_asal,alergi as alergi_asal,st_psikologi as st_psikologi_asal,risiko_jatuh_morse as risiko_jatuh_morse_asal,risiko_pasien_anak as risiko_pasien_anak_asal,kategori_id as kategori_id_asal,pemeriksaan as pemeriksaan_asal,respon_time as respon_time_asal,ket_nadi as ket_nadi_asal,idasalpasien as idasalpasien_asal,idrujukan as idrujukan_asal,namapengantar as namapengantar_asal,teleponpengantar as teleponpengantar_asal
				FROM tpoliklinik_komunikasi_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	//ASMED IGD
	function get_data_asmed_igd($pendaftaran_id){
		$q="SELECT *
			 FROM tpoliklinik_asmed_igd WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function logic_akses_asmed_igd($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_asmed_igd,H.st_lihat as st_lihat_asmed_igd,H.st_edit as st_edit_asmed_igd,H.st_hapus as st_hapus_asmed_igd,H.st_cetak as st_cetak_asmed_igd
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_asmed_igd_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_asmed_igd_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_asmed_igd_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_asmed_igd'=>'0',
				'st_input_asmed_igd'=>'0',
				'st_edit_asmed_igd'=>'0',
				'st_hapus_asmed_igd'=>'0',
				'st_cetak_asmed_igd'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function logic_akses_asmed_fisio($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_asmed_fisio,H.st_lihat as st_lihat_asmed_fisio,H.st_edit as st_edit_asmed_fisio,H.st_hapus as st_hapus_asmed_fisio,H.st_cetak as st_cetak_asmed_fisio
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_fisio_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_fisio_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_fisio_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_asmed_fisio'=>'0',
				'st_input_asmed_fisio'=>'0',
				'st_edit_asmed_fisio'=>'0',
				'st_hapus_asmed_fisio'=>'0',
				'st_cetak_asmed_fisio'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	function setting_asmed_igd(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			,H.st_radiologi,H.st_lab,H.st_eresep

			FROM `setting_asmed_igd` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function get_data_asmed_igd_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_asmed_igd WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	// function logic_akses_asmed_igd($mppa_id,$spesialisasi_id,$profesi_id){
		// $q="SELECT H.st_input as st_input_ttv,H.st_lihat as st_lihat_ttv,H.st_edit as st_edit_ttv,H.st_hapus as st_hapus_ttv,H.st_cetak as st_cetak_ttv
			// FROM(
			// SELECT 1 as level_akses,H.* FROM setting_asmed_igd_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			// UNION ALL
			// SELECT 2 as level_akses,H.* FROM setting_asmed_igd_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			// UNION ALL
			// SELECT 3 as level_akses,H.* FROM setting_asmed_igd_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			// ) H ORDER BY H.level_akses ASC
			// LIMIT 1";
		// $hasil=$this->db->query($q)->row_array();
		// if ($hasil){
			
		// }else{
			// $hasil=array(
				// 'st_lihat_ttv'=>'0',
				// 'st_input_ttv'=>'0',
				// 'st_edit_ttv'=>'0',
				// 'st_hapus_ttv'=>'0',
				// 'st_cetak_ttv'=>'0',
			// );
		// }
		// return $hasil;
		
	// }
	function list_edukasi_asmed_igd($assesmen_id){
		$q="SELECT M.id,M.judul,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM `medukasi` M
			LEFT JOIN tpoliklinik_asmed_igd_edukasi H ON H.assesmen_id='$assesmen_id' AND H.medukasi_id=M.id
			WHERE M.staktif='1'";
		return $this->db->query($q)->result();
	}
	function list_riwayat_penyakit_asmed_igd($assesmen_id,$jenis='1',$versi_edit='#'){
		if ($versi_edit=='#'){
			
		$nama_tabel='';
			if ($jenis=='1'){
				$nama_tabel='tpoliklinik_asmed_igd_riwayat_penyakit';
			}
			if ($jenis=='2'){
				$nama_tabel='tpoliklinik_asmed_igd_riwayat_penyakit_dahulu';
			}
			if ($jenis=='3'){
				$nama_tabel='tpoliklinik_asmed_igd_riwayat_penyakit_keluarga';
			}
			$q="SELECT M.nilai as id,M.ref as nama,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM merm_referensi M
				LEFT JOIN ".$nama_tabel." H ON H.assesmen_id='$assesmen_id' AND H.penyakit_id=M.nilai
				WHERE M.ref_head_id='25' AND M.nilai<>0";
		}else{
			$q="
				SELECT M.nilai as id,H.jenis,M.ref as nama,CASE WHEN H.penyakit_id IS NOT NULL THEN 'selected' ELSE '' END as pilih 
				FROM merm_referensi M
				LEFT JOIN (
				SELECT 1 as jenis,H.jml_edit as versi_edit,D.penyakit_id FROM tpoliklinik_asmed_igd H
				LEFT JOIN tpoliklinik_asmed_igd_riwayat_penyakit D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id'

				UNION ALL
				SELECT 1 as jenis, H.versi_edit,H.penyakit_id FROM tpoliklinik_asmed_igd_his_riwayat_penyakit H WHERE H.assesmen_id='$assesmen_id'

				UNION ALL

				SELECT 2 as jenis,H.jml_edit as versi_edit,D.penyakit_id FROM tpoliklinik_asmed_igd H
				LEFT JOIN tpoliklinik_asmed_igd_riwayat_penyakit_dahulu D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id'

				UNION ALL
				SELECT 2 as jenis, H.versi_edit,H.penyakit_id FROM tpoliklinik_asmed_igd_his_riwayat_penyakit_dahulu H WHERE H.assesmen_id='$assesmen_id'

				UNION ALL

				SELECT 3 as jenis,H.jml_edit as versi_edit,D.penyakit_id FROM tpoliklinik_asmed_igd H
				LEFT JOIN tpoliklinik_asmed_igd_riwayat_penyakit_keluarga D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id'

				UNION ALL
				SELECT 3 as jenis, H.versi_edit,H.penyakit_id FROM tpoliklinik_asmed_igd_his_riwayat_penyakit_keluarga H WHERE H.assesmen_id='$assesmen_id'
				) H ON H.penyakit_id=M.nilai
				WHERE M.ref_head_id='25' AND H.jenis='$jenis' AND H.versi_edit='$versi_edit'
			";
		}
		return $this->db->query($q)->result();
	}
	function get_default_warna_asmed_igd(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT jenis_drawing,warna FROM tpoliklinik_asmed_igd_default WHERE mppa_id='$login_ppa_id'";
		$data=$this->db->query($q)->row_array();
		if ($data){
			
		}else{
			$data=array(
				'warna'=>'#ff2525',
				'jenis_drawing'=>'bulat',
			);
		}
		return $data;
	}
	
	function get_lokalis_igd($assesmen_id,$gambar_id,$versi_edit='#'){
		if ($versi_edit=='#'){
		$q="SELECT *FROM tpoliklinik_asmed_igd_lokalis WHERE assesmen_id='$assesmen_id' AND gambar_id='$gambar_id'";
			
		}else{
			$q="
				SELECT *FROM(
					SELECT D.jml_edit as versi_edit,H.* 
					FROM tpoliklinik_asmed_lokalis H
					LEFT JOIN tpoliklinik_asmed_igd D ON D.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'

					UNION ALL

					SELECT H.* 
					FROM tpoliklinik_asmed_igd_his_lokalis H
					WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'
					) T 

					WHERE T.versi_edit='$versi_edit'
					ORDER BY T.`index` ASC
			";
		}
		// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	function list_template_asmed_igd(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_asmed_igd H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	
	function get_data_asmed_igd_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_asmed_igd_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_asmed_igd H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_asmed_igd_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT st_anamnesa as st_anamnesa_asal,keluhan_utama as keluhan_utama_asal,nama_anamnesa as nama_anamnesa_asal,hubungan_anamnesa as hubungan_anamnesa_asal
			,st_riwayat_penyakit as st_riwayat_penyakit_asal,riwayat_penyakit_lainnya as riwayat_penyakit_lainnya_asal
			,st_riwayat_penyakit_dahulu as st_riwayat_penyakit_dahulu_asal,riwayat_penyakit_dahulu as riwayat_penyakit_dahulu_asal
			,st_riwayat_penyakit_keluarga as st_riwayat_penyakit_keluarga_asal,riwayat_penyakit_keluarga as riwayat_penyakit_keluarga_asal
			,template_gambar as template_gambar_asal,gambar_tubuh as gambar_tubuh_asal,pemeriksaan_penunjang as pemeriksaan_penunjang_asal
			,rencana_asuhan as rencana_asuhan_asal,intruksi as intruksi_asal
			,rencana_kontrol as rencana_kontrol_asal,tanggal_kontrol as tanggal_kontrol_asal,idpoli_kontrol as idpoli_kontrol_asal
			,iddokter_kontrol as iddokter_kontrol_asal,st_edukasi as st_edukasi_asal,catatan_edukasi as catatan_edukasi_asal
			,diagnosa_utama as diagnosa_utama_asal,txt_keterangan as txt_keterangan_asal
			,txt_keterangan_diagnosa as txt_keterangan_diagnosa_asal,keadaan_umum as keadaan_umum_asal,tingkat_kesadaran as tingkat_kesadaran_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,nafas as nafas_asal,gds as gds_asal,st_pulang as st_pulang_asal,tanggal_keluar as tanggal_keluar_asal
				FROM tpoliklinik_asmed_igd_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	// function list_edukasi_asmed_igd_his($assesmen_id){
		// $q="SELECT M.id,M.judul,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM `medukasi` M
			// LEFT JOIN tpoliklinik_asmed_igd_edukasi H ON H.assesmen_id='$assesmen_id' AND H.medukasi_id=M.id
			// WHERE M.staktif='1'";
		// return $this->db->query($q)->result();
	// }
	function list_edukasi_asmed_igd_his($assesmen_id,$versi_edit){
		$q="SELECT H.medukasi_id as id,M.judul,'selected' as pilih FROM (
			SELECT H.medukasi_id,H.versi_edit FROM tpoliklinik_asmed_igd_his_edukasi H WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT D.medukasi_id,H.jml_edit as versi_edit FROM tpoliklinik_asmed_igd_edukasi D 
			LEFT JOIN tpoliklinik_asmed_igd H ON H.assesmen_id=D.assesmen_id
			WHERE D.assesmen_id='$assesmen_id'
			) H 
			INNER JOIN medukasi M ON M.id=H.medukasi_id
			WHERE H.versi_edit='$versi_edit'";
		// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	//ASSESMEN IGD
	function get_data_assesmen_igd($pendaftaran_id){
		$q="SELECT *
			 FROM tpoliklinik_assesmen_igd WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	function get_data_assesmen_igd_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_assesmen_igd WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_assesmen_igd_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_assesmen_igd_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_assesmen_igd H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	
	function get_data_assesmen_igd_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT template_id as template_id_asal,
				tanggal_datang as tanggal_datang_asal,sarana_transport as sarana_transport_asal,surat_pengantar as surat_pengantar_asal,asal_rujukan as asal_rujukan_asal,cara_masuk as cara_masuk_asal,macam_kasus as macam_kasus_asal,idasalpasien as idasalpasien_asal,idrujukan as idrujukan_asal,namapengantar as namapengantar_asal,teleponpengantar as teleponpengantar_asal,st_kecelakaan as st_kecelakaan_asal,jenis_kecelakaan as jenis_kecelakaan_asal,jenis_kendaraan_pasien as jenis_kendaraan_pasien_asal,jenis_kendaraan_lawan as jenis_kendaraan_lawan_asal,lokasi_kecelakaan as lokasi_kecelakaan_asal,lokasi_lainnya as lokasi_lainnya_asal,st_hamil as st_hamil_asal,hamil_g as hamil_g_asal,hamil_p as hamil_p_asal,hamil_a as hamil_a_asal,st_menyusui as st_menyusui_asal,info_menyusui as info_menyusui_asal,keadaan_umum as keadaan_umum_asal,tingkat_kesadaran as tingkat_kesadaran_asal,nadi as nadi_asal,nafas as nafas_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,suhu as suhu_asal,tinggi_badan as tinggi_badan_asal,berat_badan as berat_badan_asal,agama as agama_asal,faktor_komorbid as faktor_komorbid_asal,penurunan_fungsi as penurunan_fungsi_asal,st_decubitus as st_decubitus_asal,st_p3 as st_p3_asal,st_riwayat_demam as st_riwayat_demam_asal,st_berkeringat_malam as st_berkeringat_malam_asal,st_pergi_area_wabah as st_pergi_area_wabah_asal,st_pemakaian_obat_panjang as st_pemakaian_obat_panjang_asal,st_turun_bb_tanpa_sebab as st_turun_bb_tanpa_sebab_asal,intruksi_keperawatan as intruksi_keperawatan_asal,keadaan_umum_keluar as keadaan_umum_keluar_asal,tingkat_kesadaran_keluar as tingkat_kesadaran_keluar_asal,td_sistole_keluar as td_sistole_keluar_asal,td_diastole_keluar as td_diastole_keluar_asal,nafas_keluar as nafas_keluar_asal,gds_keluar as gds_keluar_asal,st_pulang as st_pulang_asal,tanggal_keluar as tanggal_keluar_asal,rencana_kontrol as rencana_kontrol_asal,tanggal_kontrol as tanggal_kontrol_asal,idpoli_kontrol as idpoli_kontrol_asal,iddokter_kontrol as iddokter_kontrol_asal,st_anamnesa as st_anamnesa_asal,keluhan_utama as keluhan_utama_asal,nama_anamnesa as nama_anamnesa_asal,hubungan_anamnesa as hubungan_anamnesa_asal,st_riwayat_penyakit as st_riwayat_penyakit_asal,riwayat_penyakit_lainnya as riwayat_penyakit_lainnya_asal,riwayat_alergi as riwayat_alergi_asal,riwayat_pengobatan as riwayat_pengobatan_asal,hubungan_anggota_keluarga as hubungan_anggota_keluarga_asal,st_psikologis as st_psikologis_asal,st_sosial_ekonomi as st_sosial_ekonomi_asal,st_spiritual as st_spiritual_asal,pendidikan as pendidikan_asal,pekerjaan as pekerjaan_asal,st_nafsu_makan as st_nafsu_makan_asal,st_turun_bb as st_turun_bb_asal,st_mual as st_mual_asal,st_muntah as st_muntah_asal,st_fungsional as st_fungsional_asal,st_alat_bantu as st_alat_bantu_asal,st_cacat as st_cacat_asal,cacat_lain as cacat_lain_asal,header_risiko_jatuh as header_risiko_jatuh_asal,footer_risiko_jatuh as footer_risiko_jatuh_asal,skor_pengkajian as skor_pengkajian_asal,risiko_jatuh as risiko_jatuh_asal,nama_tindakan as nama_tindakan_asal,st_tindakan as st_tindakan_asal,st_tindakan_action as st_tindakan_action_asal,st_edukasi as st_edukasi_asal,st_menerima_info as st_menerima_info_asal,st_hambatan_edukasi as st_hambatan_edukasi_asal,st_penerjemaah as st_penerjemaah_asal,penerjemaah as penerjemaah_asal,catatan_edukasi as catatan_edukasi_asal
				FROM tpoliklinik_assesmen_igd_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_assesmen_igd(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			FROM `setting_assesmen_igd` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function list_template_assement_igd(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_assesmen_igd H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function logic_akses_assesmen_igd($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_assesmen_igd,H.st_lihat as st_lihat_assesmen_igd,H.st_edit as st_edit_assesmen_igd,H.st_hapus as st_hapus_assesmen_igd,H.st_cetak as st_cetak_assesmen_igd
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_assesmen_igd_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_assesmen_igd_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_assesmen_igd_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_assesmen_igd'=>'0',
				'st_input_assesmen_igd'=>'0',
				'st_edit_assesmen_igd'=>'0',
				'st_hapus_assesmen_igd'=>'0',
				'st_cetak_assesmen_igd'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function list_edukasi_assesmen_igd($assesmen_id){
		$q="SELECT M.id,M.judul,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM `medukasi` M
			LEFT JOIN tpoliklinik_assesmen_igd_edukasi H ON H.assesmen_id='$assesmen_id' AND H.medukasi_id=M.id
			WHERE M.staktif='1'";
		return $this->db->query($q)->result();
	}
	function list_riwayat_penyakit_assesmen_igd($assesmen_id){
		$q="SELECT M.nilai as id,M.ref as nama,CASE WHEN H.assesmen_id IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM merm_referensi M
			LEFT JOIN tpoliklinik_assesmen_igd_riwayat_penyakit H ON H.assesmen_id='$assesmen_id' AND H.penyakit_id=M.nilai
			WHERE M.ref_head_id='25' AND M.nilai<>0";
		return $this->db->query($q)->result();
	}
	function perubahan_edukasi_assesmen_igd($assesmen_id,$versi_edit){
		if ($versi_edit!=0){
			$versi_edit_last=$versi_edit -1;
		}
		$hasil_1='';
		$hasil_2='';
		if ($versi_edit!=0){
			
			$q="SELECT H.versi_edit, GROUP_CONCAT(M.judul) as judul FROM (
				SELECT H.medukasi_id,H.versi_edit FROM tpoliklinik_assesmen_igd_his_edukasi H WHERE H.assesmen_id='$assesmen_id'
				UNION ALL
				SELECT D.medukasi_id,H.jml_edit as versi_edit FROM tpoliklinik_assesmen_igd_edukasi D 
				LEFT JOIN tpoliklinik_assesmen_igd H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'
				) H 
				INNER JOIN medukasi M ON M.id=H.medukasi_id
				WHERE H.versi_edit='$versi_edit'
				GROUP BY H.versi_edit
			";
			$hasil_1=$this->db->query($q)->row('judul');
			$q="SELECT H.versi_edit, GROUP_CONCAT(M.judul) as judul FROM (
				SELECT H.medukasi_id,H.versi_edit FROM tpoliklinik_assesmen_igd_his_edukasi H WHERE H.assesmen_id='$assesmen_id'
				UNION ALL
				SELECT D.medukasi_id,H.jml_edit as versi_edit FROM tpoliklinik_assesmen_igd_edukasi D 
				LEFT JOIN tpoliklinik_assesmen_igd H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'
				) H 
				INNER JOIN medukasi M ON M.id=H.medukasi_id
				WHERE H.versi_edit='$versi_edit_last'
				GROUP BY H.versi_edit
			";
			$hasil_2=$this->db->query($q)->row('judul');
			
		}
		$data_edukasi=array(
			'edukasi_current'=>$hasil_1,
			'edukasi_last'=>$hasil_2,
		);
		return $data_edukasi;
	}
	//CPPT
	function list_template_cppt(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_cppt H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function setting_cppt(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_cppt` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function setting_cppt_field($profesi_id=''){
		if ($profesi_id==''){
			$login_profesi_id=$this->session->userdata('login_profesi_id');
		}else{
			$login_profesi_id=$profesi_id;
		}
		$q="SELECT field_subjectif,field_objectif,field_assesmen,field_planing,field_intruction,field_intervensi,field_evaluasi,field_reasessmen 
			,field_assesmen_a,field_diagnosis_a,field_intervensi_a,field_monitoring_a
			FROM setting_cppt_filed 
			WHERE profesi_id='$login_profesi_id'";
		$row=$this->db->query($q)->row_array();
		if ($row){
			$data=$row;
		}else{
			$data=array(
				'field_subjectif' =>'0',
				'field_objectif' =>'0',
				'field_assesmen' =>'0',
				'field_planing' =>'0',
				'field_intruction' =>'0',
				'field_intervensi' =>'0',
				'field_evaluasi' =>'0',
				'field_reasessmen' =>'0',
				'field_assesmen_a' =>'0',
				'field_diagnosis_a' =>'0',
				'field_intervensi_a' =>'0',
				'field_monitoring_a' =>'0',
			);
		}
		return $data;
	}
	function setting_komunikasi_field($profesi_id=''){
		if ($profesi_id==''){
			$login_profesi_id=$this->session->userdata('login_profesi_id');
			
		}else{
			$login_profesi_id=$profesi_id;
			
		}
		$q="SELECT st_sbar,st_kritis,st_notes
			FROM setting_komunikasi_akses_form 
			WHERE profesi_id='$login_profesi_id'";
		$row=$this->db->query($q)->row_array();
		if ($row){
			$data=$row;
		}else{
			$data=array(
				'st_sbar' =>'0',
				'st_kritis' =>'0',
				'st_notes' =>'0',
			);
		}
		return $data;
	}
	function get_data_cppt($pendaftaran_id,$st_ranap='0'){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		if ($st_ranap=='0'){
			$q="SELECT *
			 FROM tpoliklinik_cppt WHERE pendaftaran_id='$pendaftaran_id' AND st_ranap='0' AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}else{
			$q="SELECT *
			 FROM tpoliklinik_cppt WHERE pendaftaran_id_ranap='$pendaftaran_id' AND st_ranap='1' AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		}
		
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	
	function logic_akses_cppt_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_cppt,H.st_lihat as st_lihat_cppt,H.st_edit as st_edit_cppt,H.st_hapus as st_hapus_cppt,H.st_cetak as st_cetak_cppt
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_cppt_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_cppt_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_cppt_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_cppt'=>'0',
				'st_input_cppt'=>'0',
				'st_edit_cppt'=>'0',
				'st_hapus_cppt'=>'0',
				'st_cetak_cppt'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	
	function logic_akses_cppt($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_cppt,H.st_lihat as st_lihat_cppt,H.st_edit as st_edit_cppt,H.st_hapus as st_hapus_cppt,H.st_cetak as st_cetak_cppt
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_cppt_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_cppt_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_cppt_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_cppt'=>'0',
				'st_input_cppt'=>'0',
				'st_edit_cppt'=>'0',
				'st_hapus_cppt'=>'0',
				'st_cetak_cppt'=>'0',
			);
		}
		return $hasil;
		
	}
	function get_data_cppt_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_cppt WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_cppt_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_cppt_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_cppt H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_cppt_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT subjectif as subjectif_asal,objectif as objectif_asal,assemen as assemen_asal,planing as planing_asal,intruksi as intruksi_asal,intervensi as intervensi_asal,evaluasi as evaluasi_asal,reassesmen as reassesmen_asal,assesmen_a as assesmen_a_asal,diagnosis_a as diagnosis_a_asal,intervensi_a as intervensi_a_asal,monitoring_a as monitoring_a_asal
				FROM tpoliklinik_cppt_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	//CPPT
	function list_template_ic(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_ic H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function setting_ic(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_ic` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function get_data_ic($pendaftaran_id,$st_ranap){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *
			 FROM tpoliklinik_ic WHERE pendaftaran_id='$pendaftaran_id' AND st_ranap='$st_ranap'  AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	
	function logic_akses_ic_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_ic,H.st_lihat as st_lihat_ic,H.st_edit as st_edit_ic,H.st_hapus as st_hapus_ic,H.st_cetak as st_cetak_ic
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_ic_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_ic_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_ic_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_ic'=>'0',
				'st_input_ic'=>'0',
				'st_edit_ic'=>'0',
				'st_hapus_ic'=>'0',
				'st_cetak_ic'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	
	function logic_akses_ic($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_ic,H.st_lihat as st_lihat_ic,H.st_edit as st_edit_ic,H.st_hapus as st_hapus_ic,H.st_cetak as st_cetak_ic
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_ic_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_ic_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_ic_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_ic'=>'0',
				'st_input_ic'=>'0',
				'st_edit_ic'=>'0',
				'st_hapus_ic'=>'0',
				'st_cetak_ic'=>'0',
			);
		}
		return $hasil;
		
	}
	
	function get_data_ic_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_ic WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_ic_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_ic_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_ic H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_ic_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_input as tanggal_input_asal,
				dokter_pelaksana as dokter_pelaksana_asal,tanggal_informasi as tanggal_informasi_asal,pemberi_info as pemberi_info_asal,pendamping as pendamping_asal,penerima_info as penerima_info_asal,hubungan_id as hubungan_id_asal,st_auto_ttd_user as st_auto_ttd_user_asal,ttd_dokter_pelaksana as ttd_dokter_pelaksana_asal,ttd_penerima_info as ttd_penerima_info_asal,nama as nama_asal,umur as umur_asal,jenis_kelamin as jenis_kelamin_asal,alamat as alamat_asal,no_ktp as no_ktp_asal,hereby as hereby_asal,untuk_tindakan as untuk_tindakan_asal,terhadap as terhadap_asal,nama_pasien as nama_pasien_asal,tanggal_lahir_pasien as tanggal_lahir_pasien_asal,umur_pasien as umur_pasien_asal,jenis_kelamin_pasien as jenis_kelamin_pasien_asal,no_medrec as no_medrec_asal,tanggal_pernyataan as tanggal_pernyataan_asal,yang_menyatakan as yang_menyatakan_asal,ttd_menyatakan as ttd_menyatakan_asal,saksi_rs as saksi_rs_asal,ttd_saksi_rs as ttd_saksi_rs_asal,saksi_keluarga as saksi_keluarga_asal,ttd_saksi as ttd_saksi_asal
				FROM tpoliklinik_ic_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_user_pelaksana_logic_ic(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *FROM setting_ic_user,setting_ic_keterangan";
		$data=$this->db->query($q)->row_array();
		if ($data['st_setting_dokter']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}else{
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_ic_user_list` H
					WHERE H.jenis_akses='1' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_ic_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_ic_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}
		if ($data['st_setting_pemberi_info']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pemberi']=$this->db->query($q)->result();
		}else{
			$data['list_pemberi']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_ic_user_list` H
					WHERE H.jenis_akses='2' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_ic_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_ic_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pemberi']=$this->db->query($q)->result();
		}
		if ($data['st_setting_petugas_pendamping']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pendamping']=$this->db->query($q)->result();
		}else{
			$data['list_pendamping']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_ic_user_list` H
					WHERE H.jenis_akses='3' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_ic_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_ic_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pendamping']=$this->db->query($q)->result();
		}
		
		// print_r($data);exit;
		return $data;
	}
	
	//edukasi_current
	function logic_akses_edukasi($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_edukasi,H.st_lihat as st_lihat_edukasi,H.st_edit as st_edit_edukasi,H.st_hapus as st_hapus_edukasi,H.st_cetak as st_cetak_edukasi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_edukasi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_edukasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_edukasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_edukasi'=>'0',
				'st_input_edukasi'=>'0',
				'st_edit_edukasi'=>'0',
				'st_hapus_edukasi'=>'0',
				'st_cetak_edukasi'=>'0',
			);
		}
		return $hasil;
		
	}
	function logic_akses_eresep($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_eresep,H.st_lihat as st_lihat_eresep,H.st_edit as st_edit_eresep,H.st_hapus as st_hapus_eresep,H.st_cetak as st_cetak_eresep
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_eresep_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_eresep_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_eresep_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_eresep'=>'0',
				'st_input_eresep'=>'0',
				'st_edit_eresep'=>'0',
				'st_hapus_eresep'=>'0',
				'st_cetak_eresep'=>'0',
			);
		}
		return $hasil;
		
	}
	function list_template_edukasi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_edukasi H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function setting_edukasi(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_edukasi` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function get_data_edukasi($pendaftaran_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *
			 FROM tpoliklinik_edukasi WHERE pendaftaran_id='$pendaftaran_id' AND tipe_rj_ri='1' AND (created_ppa='$login_ppa_id' OR edited_ppa='$login_ppa_id') AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	function get_data_edukasi_ranap($pendaftaran_id){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *
			 FROM tpoliklinik_edukasi WHERE pendaftaran_id_ranap='$pendaftaran_id' AND tipe_rj_ri='3' AND (created_ppa='$login_ppa_id' OR edited_ppa='$login_ppa_id') AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	
	function logic_akses_edukasi_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_edukasi,H.st_lihat as st_lihat_edukasi,H.st_edit as st_edit_edukasi,H.st_hapus as st_hapus_edukasi,H.st_cetak as st_cetak_edukasi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_edukasi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_edukasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_edukasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_edukasi'=>'0',
				'st_input_edukasi'=>'0',
				'st_edit_edukasi'=>'0',
				'st_hapus_edukasi'=>'0',
				'st_cetak_edukasi'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	
	
	function get_data_edukasi_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_edukasi WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function setting_edukasi_ttd(){
		$q="SELECT *
			 FROM setting_edukasi_label";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_edukasi_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_edukasi_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_edukasi H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_edukasi_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT 
				tanggal_input as tanggal_input_asal,status_assemen as status_assemen_asal,profesi_id as profesi_id_asal,bahasa_array as bahasa_array_asal,st_penerjemaah as st_penerjemaah_asal,penerjemaah as penerjemaah_asal,pendidikan as pendidikan_asal,baca_tulis as baca_tulis_asal,cara_edukasi as cara_edukasi_asal,st_menerima_info as st_menerima_info_asal,hambatan_edukasi_array as hambatan_edukasi_array_asal,medukasi_array as medukasi_array_asal,ttd_petugas as ttd_petugas_asal,ttd_petugas_nama as ttd_petugas_nama_asal
				FROM tpoliklinik_edukasi_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}

	function list_template_asmed_fisio(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_asmed_fisio H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	
	function logic_akses_rencana_ranap($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_rencana_ranap,H.st_lihat as st_lihat_rencana_ranap,H.st_edit as st_edit_rencana_ranap,H.st_hapus as st_hapus_rencana_ranap,H.st_cetak as st_cetak_rencana_ranap
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_rencana_ranap_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_rencana_ranap_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_rencana_ranap_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_rencana_ranap'=>'0',
				'st_input_rencana_ranap'=>'0',
				'st_edit_rencana_ranap'=>'0',
				'st_hapus_rencana_ranap'=>'0',
				'st_cetak_rencana_ranap'=>'0',
			);
		}
		return $hasil;
		
	}
	function logic_akses_rencana_bedah($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_rencana_bedah,H.st_lihat as st_lihat_rencana_bedah,H.st_edit as st_edit_rencana_bedah,H.st_hapus as st_hapus_rencana_bedah,H.st_cetak as st_cetak_rencana_bedah
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_rencana_bedah_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_rencana_bedah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_rencana_bedah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_rencana_bedah'=>'0',
				'st_input_rencana_bedah'=>'0',
				'st_edit_rencana_bedah'=>'0',
				'st_hapus_rencana_bedah'=>'0',
				'st_cetak_rencana_bedah'=>'0',
			);
		}
		return $hasil;
		
	}
	function logic_akses_estimasi_biaya($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_estimasi_biaya,H.st_lihat as st_lihat_estimasi_biaya,H.st_edit as st_edit_estimasi_biaya,H.st_hapus as st_hapus_estimasi_biaya,H.st_cetak as st_cetak_estimasi_biaya
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_rencana_biaya_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_rencana_biaya_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_rencana_biaya_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_estimasi_biaya'=>'0',
				'st_input_estimasi_biaya'=>'0',
				'st_edit_estimasi_biaya'=>'0',
				'st_hapus_estimasi_biaya'=>'0',
				'st_cetak_estimasi_biaya'=>'0',
			);
		}
		return $hasil;
		
	}
	function get_data_ttv_ri($pendaftaran_id){
		$q="SELECT id as ttv_id,pendaftaran_id_ranap,tanggal_input,tingkat_kesadaran,nadi,nafas,spo2,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv,suplemen_oksigen
			 FROM tranap_ttv WHERE pendaftaran_id_ranap='$pendaftaran_id' AND status_ttv!=0 ORDER BY id DESC LIMIT 1";
		
			$hasil=$this->db->query($q)->row_array();
			if ($hasil==null){
				$hasil=array(
					'ttv_id' => '',
					'pendaftaran_id' => '',
					'tanggal_input' => '',
					'tingkat_kesadaran' => '',
					'nadi' => '',
					'nafas' => '',
					'spo2' => '',
					'td_sistole' => '',
					'td_diastole' => '',
					'suhu' => '',
					'tinggi_badan' => '',
					'berat_badan' => '',
					'status_ttv' => '0',
					'suplemen_oksigen' => '0',
				);
			}else{
				if ($hasil['status_ttv']>1){
					$hasil=array(
					'ttv_id' => '',
					'pendaftaran_id' => '',
					'tanggal_input' => '',
					'tingkat_kesadaran' => '',
					'nadi' => '',
					'nafas' => '',
					'spo2' => '',
					'td_sistole' => '',
					'td_diastole' => '',
					'suhu' => '',
					'tinggi_badan' => '',
					'berat_badan' => '',
					'status_ttv' => '0',
					'suplemen_oksigen' => '0',
				);
				}
			}
			return $hasil;
	}
	
	function get_default_warna_asmed_ri(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT jenis_drawing,warna FROM tranap_asmed_ri_default WHERE mppa_id='$login_ppa_id'";
		$data=$this->db->query($q)->row_array();
		if ($data){
			
		}else{
			$data=array(
				'warna'=>'#ff2525',
				'jenis_drawing'=>'bulat',
			);
		}
		return $data;
	}
	function get_data_asmed_ri($pendaftaran_id_ranap){
		$q="SELECT *
			 FROM tranap_asmed_ri WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function logic_akses_asmed_ri($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_asmed_ri,H.st_lihat as st_lihat_asmed_ri,H.st_edit as st_edit_asmed_ri,H.st_hapus as st_hapus_asmed_ri,H.st_cetak as st_cetak_asmed_ri
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_assesmen_ri_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_assesmen_ri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_assesmen_ri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_asmed_ri'=>'0',
				'st_input_asmed_ri'=>'0',
				'st_edit_asmed_ri'=>'0',
				'st_hapus_asmed_ri'=>'0',
				'st_cetak_asmed_ri'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	function setting_asmed_ri(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi
			FROM `setting_assesmen_ri` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function list_template_asmed_ri(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tranap_asmed_ri H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.template_id DESC";
		return $this->db->query($q)->result();
	}
	function list_template_komunikasi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_komunikasi H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function get_lokalis_ri($assesmen_id,$gambar_id,$versi_edit='#'){
		if ($versi_edit=='#'){
		$q="SELECT *FROM tranap_asmed_ri_lokalis WHERE assesmen_id='$assesmen_id' AND gambar_id='$gambar_id'";
			
		}else{
			$q="
				SELECT *FROM(
					SELECT D.jml_edit as versi_edit,H.* 
					FROM tranap_asmed_ri_lokalis H
					LEFT JOIN tranap_asmed_ri D ON D.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'

					UNION ALL

					SELECT H.* 
					FROM tranap_asmed_ri_x_his_lokalis H
					WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'
					) T 

					WHERE T.versi_edit='$versi_edit'
					ORDER BY T.`index` ASC
			";
		}
		// print_r($q);exit;
		return $this->db->query($q)->result();
	}
	function get_data_asmed_ri_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_asmed_ri WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_asmed_ri_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_asmed_ri_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_asmed_ri H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_asmed_ri_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT st_anamnesa as st_anamnesa_asal,keluhan_utama as keluhan_utama_asal,nama_anamnesa as nama_anamnesa_asal,hubungan_anamnesa as hubungan_anamnesa_asal,st_riwayat_penyakit as st_riwayat_penyakit_asal,riwayat_penyakit as riwayat_penyakit_asal,riwayat_penyakit_lainnya as riwayat_penyakit_lainnya_asal,st_riwayat_penyakit_dahulu as st_riwayat_penyakit_dahulu_asal,riwayat_penyakit_dahulu_list as riwayat_penyakit_dahulu_list_asal,riwayat_penyakit_dahulu as riwayat_penyakit_dahulu_asal,st_riwayat_penyakit_keluarga as st_riwayat_penyakit_keluarga_asal,riwayat_penyakit_keluarga as riwayat_penyakit_keluarga_asal,riwayat_penyakit_keluarga_list as riwayat_penyakit_keluarga_list_asal,tingkat_kesadaran as tingkat_kesadaran_asal,td_sistole as td_sistole_asal,td_diastole as td_diastole_asal,suhu as suhu_asal,nadi as nadi_asal,nafas as nafas_asal,keadaan_umum as keadaan_umum_asal,keadaan_gizi as keadaan_gizi_asal,template_gambar as template_gambar_asal,gambar_tubuh as gambar_tubuh_asal,txt_keterangan as txt_keterangan_asal,gambar_id as gambar_id_asal,pemeriksaan_penunjang as pemeriksaan_penunjang_asal,diagnosa_kerja as diagnosa_kerja_asal,diagnosa_banding as diagnosa_banding_asal,pengobatan as pengobatan_asal,rencana as rencana_asal
				FROM tranap_asmed_ri_x_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	//ASSESMEN RI
	function logic_akses_assesmen_ri($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_assesmen_ri,H.st_lihat as st_lihat_assesmen_ri,H.st_edit as st_edit_assesmen_ri,H.st_hapus as st_hapus_assesmen_ri,H.st_cetak as st_cetak_assesmen_ri
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_asses_ri_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_asses_ri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_asses_ri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_assesmen_ri'=>'0',
				'st_input_assesmen_ri'=>'0',
				'st_edit_assesmen_ri'=>'0',
				'st_hapus_assesmen_ri'=>'0',
				'st_cetak_assesmen_ri'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	//ASSESMEN risiko_jatuh RI
	function logic_akses_risiko_jatuh($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_risiko_jatuh,H.st_lihat as st_lihat_risiko_jatuh,H.st_edit as st_edit_risiko_jatuh,H.st_hapus as st_hapus_risiko_jatuh,H.st_cetak as st_cetak_risiko_jatuh
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_risiko_jatuh_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_risiko_jatuh_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_risiko_jatuh_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_risiko_jatuh'=>'0',
				'st_input_risiko_jatuh'=>'0',
				'st_edit_risiko_jatuh'=>'0',
				'st_hapus_risiko_jatuh'=>'0',
				'st_cetak_risiko_jatuh'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	//DPJP
	function logic_akses_dpjp($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_dpjp,H.st_lihat as st_lihat_dpjp,H.st_edit as st_edit_dpjp,H.st_hapus as st_hapus_dpjp,H.st_cetak as st_cetak_dpjp
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_dpjp_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_dpjp_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_dpjp_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_dpjp'=>'0',
				'st_input_dpjp'=>'0',
				'st_edit_dpjp'=>'0',
				'st_hapus_dpjp'=>'0',
				'st_cetak_dpjp'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	//PINDAH DPJP
	function logic_akses_pindah_dpjp($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_pindah_dpjp,H.st_lihat as st_lihat_pindah_dpjp,H.st_edit as st_edit_pindah_dpjp,H.st_hapus as st_hapus_pindah_dpjp,H.st_cetak as st_cetak_pindah_dpjp
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_pindah_dpjp_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_pindah_dpjp_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_pindah_dpjp_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_pindah_dpjp'=>'0',
				'st_input_pindah_dpjp'=>'0',
				'st_edit_pindah_dpjp'=>'0',
				'st_hapus_pindah_dpjp'=>'0',
				'st_cetak_pindah_dpjp'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	//PERMINTAAN DPJP
	//PINDAH DPJP
	function logic_akses_permintaan_dpjp($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_permintaan_dpjp,H.st_lihat as st_lihat_permintaan_dpjp,H.st_edit as st_edit_permintaan_dpjp,H.st_hapus as st_hapus_permintaan_dpjp,H.st_cetak as st_cetak_permintaan_dpjp
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_permintaan_dpjp_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_permintaan_dpjp_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_permintaan_dpjp_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_permintaan_dpjp'=>'0',
				'st_input_permintaan_dpjp'=>'0',
				'st_edit_permintaan_dpjp'=>'0',
				'st_hapus_permintaan_dpjp'=>'0',
				'st_cetak_permintaan_dpjp'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	function logic_akses_lokasi_operasi($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_lokasi_operasi,H.st_lihat as st_lihat_lokasi_operasi,H.st_edit as st_edit_lokasi_operasi,H.st_hapus as st_hapus_lokasi_operasi,H.st_cetak as st_cetak_lokasi_operasi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_lokasi_operasi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_lokasi_operasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_lokasi_operasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_lokasi_operasi'=>'0',
				'st_input_lokasi_operasi'=>'0',
				'st_edit_lokasi_operasi'=>'0',
				'st_hapus_lokasi_operasi'=>'0',
				'st_cetak_lokasi_operasi'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function logic_akses_sga($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_sga,H.st_lihat as st_lihat_sga,H.st_edit as st_edit_sga,H.st_hapus as st_hapus_sga,H.st_cetak as st_cetak_sga
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_sga_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_sga_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_sga_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_sga'=>'0',
				'st_input_sga'=>'0',
				'st_edit_sga'=>'0',
				'st_hapus_sga'=>'0',
				'st_cetak_sga'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_sga(){
		$q="SELECT H.* FROM setting_sga H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	
	function logic_akses_gizi_anak($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_gizi_anak,H.st_lihat as st_lihat_gizi_anak,H.st_edit as st_edit_gizi_anak,H.st_hapus as st_hapus_gizi_anak,H.st_cetak as st_cetak_gizi_anak
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_gizi_anak_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_gizi_anak_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_gizi_anak_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_gizi_anak'=>'0',
				'st_input_gizi_anak'=>'0',
				'st_edit_gizi_anak'=>'0',
				'st_hapus_gizi_anak'=>'0',
				'st_cetak_gizi_anak'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_gizi_anak(){
		$q="SELECT H.* FROM setting_gizi_anak H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	
	function logic_akses_gizi_lanjutan($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_gizi_lanjutan,H.st_lihat as st_lihat_gizi_lanjutan,H.st_edit as st_edit_gizi_lanjutan,H.st_hapus as st_hapus_gizi_lanjutan,H.st_cetak as st_cetak_gizi_lanjutan
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_gizi_lanjutan_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_gizi_lanjutan_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_gizi_lanjutan_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_gizi_lanjutan'=>'0',
				'st_input_gizi_lanjutan'=>'0',
				'st_edit_gizi_lanjutan'=>'0',
				'st_hapus_gizi_lanjutan'=>'0',
				'st_cetak_gizi_lanjutan'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_gizi_lanjutan(){
		$q="SELECT H.* FROM setting_gizi_lanjutan H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	
	function logic_akses_discarge($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_discarge,H.st_lihat as st_lihat_discarge,H.st_edit as st_edit_discarge,H.st_hapus as st_hapus_discarge,H.st_cetak as st_cetak_discarge
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_discarge_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_discarge_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_discarge_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_discarge'=>'0',
				'st_input_discarge'=>'0',
				'st_edit_discarge'=>'0',
				'st_hapus_discarge'=>'0',
				'st_cetak_discarge'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_discarge(){
		$q="SELECT H.* FROM setting_discarge H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_rekon_obat($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_rekon_obat,H.st_lihat as st_lihat_rekon_obat,H.st_edit as st_edit_rekon_obat,H.st_hapus as st_hapus_rekon_obat,H.st_cetak as st_cetak_rekon_obat
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_rekon_obat_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_rekon_obat_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_rekon_obat_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_rekon_obat'=>'0',
				'st_input_rekon_obat'=>'0',
				'st_edit_rekon_obat'=>'0',
				'st_hapus_rekon_obat'=>'0',
				'st_cetak_rekon_obat'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_rekon_obat(){
		$q="SELECT H.* FROM setting_rekon_obat H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_intra_hospital($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_intra_hospital,H.st_lihat as st_lihat_intra_hospital,H.st_edit as st_edit_intra_hospital,H.st_hapus as st_hapus_intra_hospital,H.st_cetak as st_cetak_intra_hospital
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_intra_hospital_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_intra_hospital_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_intra_hospital_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_intra_hospital'=>'0',
				'st_input_intra_hospital'=>'0',
				'st_edit_intra_hospital'=>'0',
				'st_hapus_intra_hospital'=>'0',
				'st_cetak_intra_hospital'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_intra_hospital(){
		$q="SELECT H.* FROM setting_intra_hospital H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_tf_hospital($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_tf_hospital,H.st_lihat as st_lihat_tf_hospital,H.st_edit as st_edit_tf_hospital,H.st_hapus as st_hapus_tf_hospital,H.st_cetak as st_cetak_tf_hospital
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_tf_hospital_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_tf_hospital_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_tf_hospital_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_tf_hospital'=>'0',
				'st_input_tf_hospital'=>'0',
				'st_edit_tf_hospital'=>'0',
				'st_hapus_tf_hospital'=>'0',
				'st_cetak_tf_hospital'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_tf_hospital(){
		$q="SELECT H.* FROM setting_tf_hospital H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	
	function logic_akses_implementasi_kep($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_implementasi_kep,H.st_lihat as st_lihat_implementasi_kep,H.st_edit as st_edit_implementasi_kep,H.st_hapus as st_hapus_implementasi_kep,H.st_cetak as st_cetak_implementasi_kep
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_implementasi_kep_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_implementasi_kep_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_implementasi_kep_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_implementasi_kep'=>'0',
				'st_input_implementasi_kep'=>'0',
				'st_edit_implementasi_kep'=>'0',
				'st_hapus_implementasi_kep'=>'0',
				'st_cetak_implementasi_kep'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_implementasi_kep(){
		$q="SELECT H.* FROM setting_implementasi_kep H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_timbang($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_timbang,H.st_lihat as st_lihat_timbang,H.st_edit as st_edit_timbang,H.st_hapus as st_hapus_timbang,H.st_cetak as st_cetak_timbang
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_timbang_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_timbang_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_timbang_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_timbang'=>'0',
				'st_input_timbang'=>'0',
				'st_edit_timbang'=>'0',
				'st_hapus_timbang'=>'0',
				'st_cetak_timbang'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_timbang(){
		$q="SELECT H.* FROM setting_timbang H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_tindakan_anestesi($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_tindakan_anestesi,H.st_lihat as st_lihat_tindakan_anestesi,H.st_edit as st_edit_tindakan_anestesi,H.st_hapus as st_hapus_tindakan_anestesi,H.st_cetak as st_cetak_tindakan_anestesi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_tindakan_anestesi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_tindakan_anestesi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_tindakan_anestesi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_tindakan_anestesi'=>'0',
				'st_input_tindakan_anestesi'=>'0',
				'st_edit_tindakan_anestesi'=>'0',
				'st_hapus_tindakan_anestesi'=>'0',
				'st_cetak_tindakan_anestesi'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_tindakan_anestesi(){
		$q="SELECT H.* FROM setting_tindakan_anestesi H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_risiko_tinggi($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_risiko_tinggi,H.st_lihat as st_lihat_risiko_tinggi,H.st_edit as st_edit_risiko_tinggi,H.st_hapus as st_hapus_risiko_tinggi,H.st_cetak as st_cetak_risiko_tinggi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_risiko_tinggi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_risiko_tinggi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_risiko_tinggi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_risiko_tinggi'=>'0',
				'st_input_risiko_tinggi'=>'0',
				'st_edit_risiko_tinggi'=>'0',
				'st_hapus_risiko_tinggi'=>'0',
				'st_cetak_risiko_tinggi'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_risiko_tinggi(){
		$q="SELECT H.* FROM setting_risiko_tinggi H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	
	//TF DARAH
	function logic_akses_tf_darah($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_tf_darah,H.st_lihat as st_lihat_tf_darah,H.st_edit as st_edit_tf_darah,H.st_hapus as st_hapus_tf_darah,H.st_cetak as st_cetak_tf_darah
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_tf_darah_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_tf_darah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_tf_darah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_tf_darah'=>'0',
				'st_input_tf_darah'=>'0',
				'st_edit_tf_darah'=>'0',
				'st_hapus_tf_darah'=>'0',
				'st_cetak_tf_darah'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_tf_darah(){
		$q="SELECT H.* FROM setting_tf_darah H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	
	//INVASIF
	
	function logic_akses_invasif($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_invasif,H.st_lihat as st_lihat_invasif,H.st_edit as st_edit_invasif,H.st_hapus as st_hapus_invasif,H.st_cetak as st_cetak_invasif
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_invasif_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_invasif_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_invasif_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_invasif'=>'0',
				'st_input_invasif'=>'0',
				'st_edit_invasif'=>'0',
				'st_hapus_invasif'=>'0',
				'st_cetak_invasif'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_invasif(){
		$q="SELECT H.* FROM setting_invasif H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	
	// function logic_akses_komunikasi($mppa_id,$spesialisasi_id,$profesi_id){
		// $q="SELECT H.st_input as st_input_komunikasi,H.st_lihat as st_lihat_komunikasi,H.st_edit as st_edit_komunikasi,H.st_hapus as st_hapus_komunikasi,H.st_cetak as st_cetak_komunikasi
			// FROM(
			// SELECT 1 as level_akses,H.* FROM setting_komunikasi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			// UNION ALL
			// SELECT 2 as level_akses,H.* FROM setting_komunikasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			// UNION ALL
			// SELECT 3 as level_akses,H.* FROM setting_komunikasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			// ) H ORDER BY H.level_akses ASC
			// LIMIT 1";
		// $hasil=$this->db->query($q)->row_array();
		// // print_r($hasil);exit;
		// if ($hasil){
			
		// }else{
			// $hasil=array(
				// 'st_lihat_komunikasi'=>'0',
				// 'st_input_komunikasi'=>'0',
				// 'st_edit_komunikasi'=>'0',
				// 'st_hapus_komunikasi'=>'0',
				// 'st_cetak_komunikasi'=>'0',
			// );
		// }
		// // print_r($hasil);exit;
		// return $hasil;
		
	// }
	function setting_komunikasi(){
		$q="SELECT H.* FROM setting_komunikasi H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	
	//NYERI RI
	//NYERI
	function setting_nyeri_ri(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_nyeri_ri` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	function get_data_nyeri_ri($pendaftaran_id_ranap){
		$q="SELECT *
			 FROM tranap_assesmen_nyeri WHERE pendaftaran_id_ranap='$pendaftaran_id_ranap' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function logic_akses_nyeri_ri_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_nyeri_ri,H.st_lihat as st_lihat_nyeri_ri,H.st_edit as st_edit_nyeri_ri,H.st_hapus as st_hapus_nyeri_ri,H.st_cetak as st_cetak_nyeri_ri
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_nyeri_ri_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_nyeri_ri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_nyeri_ri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_nyeri_ri'=>'0',
				'st_input_nyeri_ri'=>'0',
				'st_edit_nyeri_ri'=>'0',
				'st_hapus_nyeri_ri'=>'0',
				'st_cetak_nyeri_ri'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	function default_logic_nyeri_ri($data){
		$idtipe=$data['idtipe'];
		$idkelas=$data['idkelas'];
		
		$q="SELECT *FROM (
			SELECT '1' as lev,H.mnyeri_id  FROM `msetting_nyeri_ri_default_detail` H
			WHERE H.idtipe='$idtipe' AND H.idkelas='$idkelas'
			UNION ALL

			SELECT '2' as lev,H.mnyeri_id  FROM `msetting_nyeri_ri_default_detail` H
			WHERE H.idtipe='$idtipe' AND H.idkelas='0'
			) M ORDER BY M.lev LIMIT 1";
		$row=$this->db->query($q)->row('mnyeri_id');
		if ($row){
			return $row;
		}else{
			return 0;
		}
	}
	function setting_default_nyeri_ri(){
		$q="SELECT st_spesifik_nyeri_ri,st_kunci_default_nyeri_ri FROM msetting_nyeri_ri_default";
		return $this->db->query($q)->row_array();
	}
	function logic_akses_nyeri_ri($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_nyeri_ri,H.st_lihat as st_lihat_nyeri_ri,H.st_edit as st_edit_nyeri_ri,H.st_hapus as st_hapus_nyeri_ri,H.st_cetak as st_cetak_nyeri_ri
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_nyeri_ri_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_nyeri_ri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_nyeri_ri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_nyeri_ri'=>'0',
				'st_input_nyeri_ri'=>'0',
				'st_edit_nyeri_ri'=>'0',
				'st_hapus_nyeri_ri'=>'0',
				'st_cetak_nyeri_ri'=>'0',
			);
		}
		return $hasil;
		
	}
	function get_data_nyeri_ri_trx($assesmen_id){
		$q="SELECT *
			 FROM tranap_assesmen_nyeri WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_nyeri_ri_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tranap_assesmen_nyeri_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tranap_assesmen_nyeri H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_nyeri_ri_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT lokasi_nyeri as lokasi_nyeri_asal,penyebab_nyeri as penyebab_nyeri_asal,durasi_nyeri as durasi_nyeri_asal,frek_nyeri as frek_nyeri_asal,jenis_nyeri as jenis_nyeri_asal,skala_nyeri as skala_nyeri_asal,karakteristik_nyeri as karakteristik_nyeri_asal,nyeri_hilang as nyeri_hilang_asal,total_skor_nyeri as total_skor_nyeri_asal
				FROM tranap_assesmen_nyeri_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	
	function logic_akses_fisio_ri($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_fisio_ri,H.st_lihat as st_lihat_fisio_ri,H.st_edit as st_edit_fisio_ri,H.st_hapus as st_hapus_fisio_ri,H.st_cetak as st_cetak_fisio_ri
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_fisio_ri_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_fisio_ri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_fisio_ri_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_fisio_ri'=>'0',
				'st_input_fisio_ri'=>'0',
				'st_edit_fisio_ri'=>'0',
				'st_hapus_fisio_ri'=>'0',
				'st_cetak_fisio_ri'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function logic_akses_mpp($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_mpp,H.st_lihat as st_lihat_mpp,H.st_edit as st_edit_mpp,H.st_hapus as st_hapus_mpp,H.st_cetak as st_cetak_mpp
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_mpp_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_mpp_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_mpp_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_mpp'=>'0',
				'st_input_mpp'=>'0',
				'st_edit_mpp'=>'0',
				'st_hapus_mpp'=>'0',
				'st_cetak_mpp'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_mpp(){
		$q="SELECT H.* FROM setting_mpp H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	
	function logic_akses_evaluasi_mpp($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_evaluasi_mpp,H.st_lihat as st_lihat_evaluasi_mpp,H.st_edit as st_edit_evaluasi_mpp,H.st_hapus as st_hapus_evaluasi_mpp,H.st_cetak as st_cetak_evaluasi_mpp
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_evaluasi_mpp_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_evaluasi_mpp_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_evaluasi_mpp_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_evaluasi_mpp'=>'0',
				'st_input_evaluasi_mpp'=>'0',
				'st_edit_evaluasi_mpp'=>'0',
				'st_hapus_evaluasi_mpp'=>'0',
				'st_cetak_evaluasi_mpp'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_evaluasi_mpp(){
		$q="SELECT H.* FROM setting_evaluasi_mpp H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_imp_mpp($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_imp_mpp,H.st_lihat as st_lihat_imp_mpp,H.st_edit as st_edit_imp_mpp,H.st_hapus as st_hapus_imp_mpp,H.st_cetak as st_cetak_imp_mpp
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_imp_mpp_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_imp_mpp_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_imp_mpp_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_imp_mpp'=>'0',
				'st_input_imp_mpp'=>'0',
				'st_edit_imp_mpp'=>'0',
				'st_hapus_imp_mpp'=>'0',
				'st_cetak_imp_mpp'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_imp_mpp(){
		$q="SELECT H.* FROM setting_imp_mpp H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_assesmen_pulang($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_assesmen_pulang,H.st_lihat as st_lihat_assesmen_pulang,H.st_edit as st_edit_assesmen_pulang,H.st_hapus as st_hapus_assesmen_pulang,H.st_cetak as st_cetak_assesmen_pulang
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_assesmen_pulang_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_assesmen_pulang_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_assesmen_pulang_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_assesmen_pulang'=>'0',
				'st_input_assesmen_pulang'=>'0',
				'st_edit_assesmen_pulang'=>'0',
				'st_hapus_assesmen_pulang'=>'0',
				'st_cetak_assesmen_pulang'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_assesmen_pulang(){
		$q="SELECT H.* FROM setting_assesmen_pulang H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_lap_bedah($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_lap_bedah,H.st_lihat as st_lihat_lap_bedah,H.st_edit as st_edit_lap_bedah,H.st_hapus as st_hapus_lap_bedah,H.st_cetak as st_cetak_lap_bedah
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_lap_bedah_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_lap_bedah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_lap_bedah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_lap_bedah'=>'0',
				'st_input_lap_bedah'=>'0',
				'st_edit_lap_bedah'=>'0',
				'st_hapus_lap_bedah'=>'0',
				'st_cetak_lap_bedah'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_lap_bedah(){
		$q="SELECT H.* FROM setting_lap_bedah H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_assesmen_askep($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_assesmen_askep,H.st_lihat as st_lihat_assesmen_askep,H.st_edit as st_edit_assesmen_askep,H.st_hapus as st_hapus_assesmen_askep,H.st_cetak as st_cetak_assesmen_askep
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_assesmen_askep_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_assesmen_askep_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_assesmen_askep_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_assesmen_askep'=>'0',
				'st_input_assesmen_askep'=>'0',
				'st_edit_assesmen_askep'=>'0',
				'st_hapus_assesmen_askep'=>'0',
				'st_cetak_assesmen_askep'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_assesmen_askep(){
		$q="SELECT H.* FROM setting_assesmen_askep H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_keselamatan($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_keselamatan,H.st_lihat as st_lihat_keselamatan,H.st_edit as st_edit_keselamatan,H.st_hapus as st_hapus_keselamatan,H.st_cetak as st_cetak_keselamatan
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_keselamatan_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_keselamatan_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_keselamatan_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_keselamatan'=>'0',
				'st_input_keselamatan'=>'0',
				'st_edit_keselamatan'=>'0',
				'st_hapus_keselamatan'=>'0',
				'st_cetak_keselamatan'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_keselamatan(){
		$q="SELECT H.* FROM setting_keselamatan H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	
	function logic_akses_pra_sedasi($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_pra_sedasi,H.st_lihat as st_lihat_pra_sedasi,H.st_edit as st_edit_pra_sedasi,H.st_hapus as st_hapus_pra_sedasi,H.st_cetak as st_cetak_pra_sedasi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_pra_sedasi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_pra_sedasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_pra_sedasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_pra_sedasi'=>'0',
				'st_input_pra_sedasi'=>'0',
				'st_edit_pra_sedasi'=>'0',
				'st_hapus_pra_sedasi'=>'0',
				'st_cetak_pra_sedasi'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_pra_sedasi(){
		$q="SELECT H.* FROM setting_pra_sedasi H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_ringkasan_pulang($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_ringkasan_pulang,H.st_lihat as st_lihat_ringkasan_pulang,H.st_edit as st_edit_ringkasan_pulang,H.st_hapus as st_hapus_ringkasan_pulang,H.st_cetak as st_cetak_ringkasan_pulang
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_ringkasan_pulang_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_ringkasan_pulang_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_ringkasan_pulang_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_ringkasan_pulang'=>'0',
				'st_input_ringkasan_pulang'=>'0',
				'st_edit_ringkasan_pulang'=>'0',
				'st_hapus_ringkasan_pulang'=>'0',
				'st_cetak_ringkasan_pulang'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_ringkasan_pulang(){
		$q="SELECT H.judul_header_ina,H.judul_footer_ina,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.judul_header_eng,H.judul_footer_eng
		FROM setting_ringkasan_pulang H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_pra_bedah($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_pra_bedah,H.st_lihat as st_lihat_pra_bedah,H.st_edit as st_edit_pra_bedah,H.st_hapus as st_hapus_pra_bedah,H.st_cetak as st_cetak_pra_bedah
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_pra_bedah_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_pra_bedah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_pra_bedah_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_pra_bedah'=>'0',
				'st_input_pra_bedah'=>'0',
				'st_edit_pra_bedah'=>'0',
				'st_hapus_pra_bedah'=>'0',
				'st_cetak_pra_bedah'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_pra_bedah(){
		$q="SELECT H.judul_header_ina,H.judul_footer_ina,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.judul_header_eng,H.judul_footer_eng
		FROM setting_pra_bedah H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_survey($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_survey,H.st_lihat as st_lihat_survey,H.st_edit as st_edit_survey,H.st_hapus as st_hapus_survey,H.st_cetak as st_cetak_survey
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_survey_kepuasan_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_survey_kepuasan_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_survey_kepuasan_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_survey'=>'0',
				'st_input_survey'=>'0',
				'st_edit_survey'=>'0',
				'st_hapus_survey'=>'0',
				'st_cetak_survey'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_survey(){
		$q="SELECT H.judul_header_ina,H.judul_footer_ina,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.judul_header_eng,H.judul_footer_eng
		FROM setting_survey_kepuasan H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_ews($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_ews,H.st_lihat as st_lihat_ews,H.st_edit as st_edit_ews,H.st_hapus as st_hapus_ews,H.st_cetak as st_cetak_ews
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_ews_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_ews_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_ews_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_ews'=>'0',
				'st_input_ews'=>'0',
				'st_edit_ews'=>'0',
				'st_hapus_ews'=>'0',
				'st_cetak_ews'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_ews(){
		$q="SELECT H.judul_header_ina,H.judul_footer_ina,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.judul_header_eng,H.judul_footer_eng
		FROM setting_ews H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_pews($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_pews,H.st_lihat as st_lihat_pews,H.st_edit as st_edit_pews,H.st_hapus as st_hapus_pews,H.st_cetak as st_cetak_pews
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_pews_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_pews_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_pews_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_pews'=>'0',
				'st_input_pews'=>'0',
				'st_edit_pews'=>'0',
				'st_hapus_pews'=>'0',
				'st_cetak_pews'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_pews(){
		$q="SELECT H.judul_header_ina,H.judul_footer_ina,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.judul_header_eng,H.judul_footer_eng
		FROM setting_pews H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_observasi($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_observasi,H.st_lihat as st_lihat_observasi,H.st_edit as st_edit_observasi,H.st_hapus as st_hapus_observasi,H.st_cetak as st_cetak_observasi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_observasi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_observasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_observasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_observasi'=>'0',
				'st_input_observasi'=>'0',
				'st_edit_observasi'=>'0',
				'st_hapus_observasi'=>'0',
				'st_cetak_observasi'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_observasi(){
		$q="SELECT H.judul_header_ina,H.judul_footer_ina,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.judul_header_eng,H.judul_footer_eng
		FROM setting_observasi H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	
	function logic_akses_aps($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_aps,H.st_lihat as st_lihat_aps,H.st_edit as st_edit_aps,H.st_hapus as st_hapus_aps,H.st_cetak as st_cetak_aps
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_aps_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_aps_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_aps_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_aps'=>'0',
				'st_input_aps'=>'0',
				'st_edit_aps'=>'0',
				'st_hapus_aps'=>'0',
				'st_cetak_aps'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_aps(){
		$q="SELECT H.judul_header_ina,H.judul_footer_ina,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.judul_header_eng,H.judul_footer_eng
		FROM setting_aps H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	function logic_akses_info_ods($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_info_ods,H.st_lihat as st_lihat_info_ods,H.st_edit as st_edit_info_ods,H.st_hapus as st_hapus_info_ods,H.st_cetak as st_cetak_info_ods
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_info_ods_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_info_ods_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_info_ods_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_info_ods'=>'0',
				'st_input_info_ods'=>'0',
				'st_edit_info_ods'=>'0',
				'st_hapus_info_ods'=>'0',
				'st_cetak_info_ods'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_info_ods(){
		$q="SELECT H.judul_header_ina,H.judul_footer_ina,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.judul_header_eng,H.judul_footer_eng
		FROM setting_info_ods H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	
	//ICU
	function list_template_icu(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_icu H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function setting_icu(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_icu` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function get_data_icu($pendaftaran_id,$st_ranap){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *
			 FROM tpoliklinik_icu WHERE pendaftaran_id='$pendaftaran_id' AND st_ranap='$st_ranap'  AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	
	function logic_akses_icu_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_icu,H.st_lihat as st_lihat_icu,H.st_edit as st_edit_icu,H.st_hapus as st_hapus_icu,H.st_cetak as st_cetak_icu
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_icu_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_icu_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_icu_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_icu'=>'0',
				'st_input_icu'=>'0',
				'st_edit_icu'=>'0',
				'st_hapus_icu'=>'0',
				'st_cetak_icu'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	
	function logic_akses_icu($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_icu,H.st_lihat as st_lihat_icu,H.st_edit as st_edit_icu,H.st_hapus as st_hapus_icu,H.st_cetak as st_cetak_icu
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_icu_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_icu_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_icu_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_icu'=>'0',
				'st_input_icu'=>'0',
				'st_edit_icu'=>'0',
				'st_hapus_icu'=>'0',
				'st_cetak_icu'=>'0',
			);
		}
		return $hasil;
		
	}
	
	function get_data_icu_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_icu WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_icu_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_icu_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_icu H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_icu_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_input as tanggal_input_asal,
				dokter_pelaksana as dokter_pelaksana_asal,tanggal_informasi as tanggal_informasi_asal,pemberi_info as pemberi_info_asal,pendamping as pendamping_asal,penerima_info as penerima_info_asal,hubungan_id as hubungan_id_asal,st_auto_ttd_user as st_auto_ttd_user_asal,ttd_dokter_pelaksana as ttd_dokter_pelaksana_asal,ttd_penerima_info as ttd_penerima_info_asal,nama as nama_asal,umur as umur_asal,jenis_kelamin as jenis_kelamin_asal,alamat as alamat_asal,no_ktp as no_ktp_asal,hereby as hereby_asal,untuk_tindakan as untuk_tindakan_asal,terhadap as terhadap_asal,nama_pasien as nama_pasien_asal,tanggal_lahir_pasien as tanggal_lahir_pasien_asal,umur_pasien as umur_pasien_asal,jenis_kelamin_pasien as jenis_kelamin_pasien_asal,no_medrec as no_medrec_asal,tanggal_pernyataan as tanggal_pernyataan_asal,yang_menyatakan as yang_menyatakan_asal,ttd_menyatakan as ttd_menyatakan_asal,saksi_rs as saksi_rs_asal,ttd_saksi_rs as ttd_saksi_rs_asal,saksi_keluarga as saksi_keluarga_asal,ttd_saksi as ttd_saksi_asal
				FROM tpoliklinik_icu_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_user_pelaksana_logic_icu(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *FROM setting_icu_user,setting_icu_keterangan";
		$data=$this->db->query($q)->row_array();
		if ($data['st_setting_dokter']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}else{
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_icu_user_list` H
					WHERE H.jenis_akses='1' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_icu_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_icu_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}
		if ($data['st_setting_pemberi_info']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pemberi']=$this->db->query($q)->result();
		}else{
			$data['list_pemberi']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_icu_user_list` H
					WHERE H.jenis_akses='2' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_icu_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_icu_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pemberi']=$this->db->query($q)->result();
		}
		if ($data['st_setting_petugas_pendamping']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pendamping']=$this->db->query($q)->result();
		}else{
			$data['list_pendamping']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_icu_user_list` H
					WHERE H.jenis_akses='3' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_icu_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_icu_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pendamping']=$this->db->query($q)->result();
		}
		
		// print_r($data);exit;
		return $data;
	}
	
	//ISOLASI
	//ICU
	function list_template_isolasi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_isolasi H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function setting_isolasi(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_isolasi` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function get_data_isolasi($pendaftaran_id,$st_ranap){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *
			 FROM tpoliklinik_isolasi WHERE pendaftaran_id='$pendaftaran_id' AND st_ranap='$st_ranap'  AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	
	function logic_akses_isolasi_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_isolasi,H.st_lihat as st_lihat_isolasi,H.st_edit as st_edit_isolasi,H.st_hapus as st_hapus_isolasi,H.st_cetak as st_cetak_isolasi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_isolasi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_isolasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_isolasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_isolasi'=>'0',
				'st_input_isolasi'=>'0',
				'st_edit_isolasi'=>'0',
				'st_hapus_isolasi'=>'0',
				'st_cetak_isolasi'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	
	function logic_akses_isolasi($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_isolasi,H.st_lihat as st_lihat_isolasi,H.st_edit as st_edit_isolasi,H.st_hapus as st_hapus_isolasi,H.st_cetak as st_cetak_isolasi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_isolasi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_isolasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_isolasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_isolasi'=>'0',
				'st_input_isolasi'=>'0',
				'st_edit_isolasi'=>'0',
				'st_hapus_isolasi'=>'0',
				'st_cetak_isolasi'=>'0',
			);
		}
		return $hasil;
		
	}
	
	function get_data_isolasi_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_isolasi WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_isolasi_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_isolasi_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_isolasi H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_isolasi_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_input as tanggal_input_asal,
				dokter_pelaksana as dokter_pelaksana_asal,tanggal_informasi as tanggal_informasi_asal,pemberi_info as pemberi_info_asal,pendamping as pendamping_asal,penerima_info as penerima_info_asal,hubungan_id as hubungan_id_asal,st_auto_ttd_user as st_auto_ttd_user_asal,ttd_dokter_pelaksana as ttd_dokter_pelaksana_asal,ttd_penerima_info as ttd_penerima_info_asal,nama as nama_asal,umur as umur_asal,jenis_kelamin as jenis_kelamin_asal,alamat as alamat_asal,no_ktp as no_ktp_asal,hereby as hereby_asal,untuk_tindakan as untuk_tindakan_asal,terhadap as terhadap_asal,nama_pasien as nama_pasien_asal,tanggal_lahir_pasien as tanggal_lahir_pasien_asal,umur_pasien as umur_pasien_asal,jenis_kelamin_pasien as jenis_kelamin_pasien_asal,no_medrec as no_medrec_asal,tanggal_pernyataan as tanggal_pernyataan_asal,yang_menyatakan as yang_menyatakan_asal,ttd_menyatakan as ttd_menyatakan_asal,saksi_rs as saksi_rs_asal,ttd_saksi_rs as ttd_saksi_rs_asal,saksi_keluarga as saksi_keluarga_asal,ttd_saksi as ttd_saksi_asal
				FROM tpoliklinik_isolasi_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_user_pelaksana_logic_isolasi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *FROM setting_isolasi_user,setting_isolasi_keterangan";
		$data=$this->db->query($q)->row_array();
		if ($data['st_setting_dokter']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}else{
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_isolasi_user_list` H
					WHERE H.jenis_akses='1' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_isolasi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_isolasi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}
		if ($data['st_setting_pemberi_info']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pemberi']=$this->db->query($q)->result();
		}else{
			$data['list_pemberi']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_isolasi_user_list` H
					WHERE H.jenis_akses='2' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_isolasi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_isolasi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pemberi']=$this->db->query($q)->result();
		}
		if ($data['st_setting_petugas_pendamping']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pendamping']=$this->db->query($q)->result();
		}else{
			$data['list_pendamping']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_isolasi_user_list` H
					WHERE H.jenis_akses='3' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_isolasi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_isolasi_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pendamping']=$this->db->query($q)->result();
		}
		
		// print_r($data);exit;
		return $data;
	}
	
	//DNR
	function list_template_dnr(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
			 FROM tpoliklinik_dnr H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
			ORDER BY H.assesmen_id DESC";
		return $this->db->query($q)->result();
	}
	function setting_dnr(){
		$q="SELECT H.judul_header,H.judul_footer 
			,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi

			FROM `setting_dnr` H

			WHERE H.id='1'";
		return $this->db->query($q)->row_array();
	}
	
	function get_data_dnr($pendaftaran_id,$st_ranap){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *
			 FROM tpoliklinik_dnr WHERE pendaftaran_id='$pendaftaran_id' AND st_ranap='$st_ranap'  AND created_ppa='$login_ppa_id' AND status_assemen IN (1,3) ORDER BY assesmen_id DESC LIMIT 1";
			 $hasil=$this->db->query($q)->row_array();
			// print_r($q);exit;
			return $hasil;
	}
	
	function logic_akses_dnr_rj($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_dnr,H.st_lihat as st_lihat_dnr,H.st_edit as st_edit_dnr,H.st_hapus as st_hapus_dnr,H.st_cetak as st_cetak_dnr
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_dnr_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_dnr_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_dnr_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_dnr'=>'0',
				'st_input_dnr'=>'0',
				'st_edit_dnr'=>'0',
				'st_hapus_dnr'=>'0',
				'st_cetak_dnr'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	
	
	function logic_akses_dnr($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_dnr,H.st_lihat as st_lihat_dnr,H.st_edit as st_edit_dnr,H.st_hapus as st_hapus_dnr,H.st_cetak as st_cetak_dnr
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_dnr_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_dnr_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_dnr_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_dnr'=>'0',
				'st_input_dnr'=>'0',
				'st_edit_dnr'=>'0',
				'st_hapus_dnr'=>'0',
				'st_cetak_dnr'=>'0',
			);
		}
		return $hasil;
		
	}
	
	function get_data_dnr_trx($assesmen_id){
		$q="SELECT *
			 FROM tpoliklinik_dnr WHERE assesmen_id='$assesmen_id'";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_dnr_trx_his($assesmen_id,$versi_edit){
		$q="SELECT *
			 FROM tpoliklinik_dnr_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'
			 UNION ALL
			 SELECT H.jml_edit as versi_edit,H.*
			 FROM tpoliklinik_dnr H WHERE H.assesmen_id='$assesmen_id' AND H.jml_edit='$versi_edit'
			 ";
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_data_dnr_trx_asal($assesmen_id,$versi_edit){
		if ($versi_edit>0){
			$versi_edit = $versi_edit-1;
		}
		$q="SELECT tanggal_input as tanggal_input_asal,
				dokter_pelaksana as dokter_pelaksana_asal,tanggal_informasi as tanggal_informasi_asal,pemberi_info as pemberi_info_asal,pendamping as pendamping_asal,penerima_info as penerima_info_asal,hubungan_id as hubungan_id_asal,st_auto_ttd_user as st_auto_ttd_user_asal,ttd_dokter_pelaksana as ttd_dokter_pelaksana_asal,ttd_penerima_info as ttd_penerima_info_asal,nama as nama_asal,umur as umur_asal,jenis_kelamin as jenis_kelamin_asal,alamat as alamat_asal,no_ktp as no_ktp_asal,hereby as hereby_asal,untuk_tindakan as untuk_tindakan_asal,terhadap as terhadap_asal,nama_pasien as nama_pasien_asal,tanggal_lahir_pasien as tanggal_lahir_pasien_asal,umur_pasien as umur_pasien_asal,jenis_kelamin_pasien as jenis_kelamin_pasien_asal,no_medrec as no_medrec_asal,tanggal_pernyataan as tanggal_pernyataan_asal,yang_menyatakan as yang_menyatakan_asal,ttd_menyatakan as ttd_menyatakan_asal,saksi_rs as saksi_rs_asal,ttd_saksi_rs as ttd_saksi_rs_asal,saksi_keluarga as saksi_keluarga_asal,ttd_saksi as ttd_saksi_asal
				FROM tpoliklinik_dnr_his 
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$versi_edit'";
		// print_r($q);exit;
			 $hasil=$this->db->query($q)->row_array();
			
			return $hasil;
	}
	function get_user_pelaksana_logic_dnr(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT *FROM setting_dnr_user,setting_dnr_keterangan";
		$data=$this->db->query($q)->row_array();
		if ($data['st_setting_dokter']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}else{
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_dnr_user_list` H
					WHERE H.jenis_akses='1' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_dnr_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_dnr_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='1' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_dokter_pelaksana']=$this->db->query($q)->result();
		}
		if ($data['st_setting_pemberi_info']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pemberi']=$this->db->query($q)->result();
		}else{
			$data['list_pemberi']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_dnr_user_list` H
					WHERE H.jenis_akses='2' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_dnr_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_dnr_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='2' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pemberi']=$this->db->query($q)->result();
		}
		if ($data['st_setting_petugas_pendamping']=='1'){
			$q="SELECT *FROM mppa WHERE id='$login_ppa_id'";
			$data['list_pendamping']=$this->db->query($q)->result();
		}else{
			$data['list_pendamping']=array();
			$q="SELECT M.id,M.nama,M.tipepegawai FROM mppa M INNER JOIN (
					SELECT H.mppa_id FROM `setting_dnr_user_list` H
					WHERE H.jenis_akses='3' AND H.mppa_id!=0

					UNION 

					SELECT M.id as mppa_id FROM `setting_dnr_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id AND M.spesialisasi_id=H.spesialisasi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id !=0 AND H.mppa_id=0 AND M.staktif='1'

					UNION 

					SELECT M.id as mppa_id FROM `setting_dnr_user_list` H
					LEFT JOIN mppa M ON M.jenis_profesi_id=H.profesi_id
					WHERE H.jenis_akses='3' AND H.spesialisasi_id =0 AND H.mppa_id=0 AND M.staktif='1'
			) H ON H.mppa_id=M.id";
			$data['list_pendamping']=$this->db->query($q)->result();
		}
		
		// print_r($data);exit;
		return $data;
	}
	function logic_akses_rohani($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_rohani,H.st_lihat as st_lihat_rohani,H.st_edit as st_edit_rohani,H.st_hapus as st_hapus_rohani,H.st_cetak as st_cetak_rohani
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_rohani_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_rohani_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_rohani_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_rohani'=>'0',
				'st_input_rohani'=>'0',
				'st_edit_rohani'=>'0',
				'st_hapus_rohani'=>'0',
				'st_cetak_rohani'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_rohani(){
		$q="SELECT H.judul_header_ina,H.judul_footer_ina,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.judul_header_eng,H.judul_footer_eng
		FROM setting_rohani H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	
	function logic_akses_privasi($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_privasi,H.st_lihat as st_lihat_privasi,H.st_edit as st_edit_privasi,H.st_hapus as st_hapus_privasi,H.st_cetak as st_cetak_privasi
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_privasi_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_privasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_privasi_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_privasi'=>'0',
				'st_input_privasi'=>'0',
				'st_edit_privasi'=>'0',
				'st_hapus_privasi'=>'0',
				'st_cetak_privasi'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_privasi(){
		$q="SELECT H.judul_header_ina,H.judul_footer_ina,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.judul_header_eng,H.judul_footer_eng
		FROM setting_privasi H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
	
	function logic_akses_opinion($mppa_id,$spesialisasi_id,$profesi_id){
		$q="SELECT H.st_input as st_input_opinion,H.st_lihat as st_lihat_opinion,H.st_edit as st_edit_opinion,H.st_hapus as st_hapus_opinion,H.st_cetak as st_cetak_opinion
			FROM(
			SELECT 1 as level_akses,H.* FROM setting_opinion_akses H WHERE H.mppa_id='$mppa_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 2 as level_akses,H.* FROM setting_opinion_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='$spesialisasi_id' AND H.profesi_id='$profesi_id'
			UNION ALL
			SELECT 3 as level_akses,H.* FROM setting_opinion_akses H WHERE H.mppa_id='0' AND H.spesialisasi_id='0' AND H.profesi_id='$profesi_id'
			) H ORDER BY H.level_akses ASC
			LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		if ($hasil){
			
		}else{
			$hasil=array(
				'st_lihat_opinion'=>'0',
				'st_input_opinion'=>'0',
				'st_edit_opinion'=>'0',
				'st_hapus_opinion'=>'0',
				'st_cetak_opinion'=>'0',
			);
		}
		// print_r($hasil);exit;
		return $hasil;
		
	}
	function setting_opinion(){
		$q="SELECT H.judul_header_ina,H.judul_footer_ina,H.st_edit_catatan,H.lama_edit,H.orang_edit,H.st_hapus_catatan,H.lama_hapus,H.orang_hapus,H.st_duplikasi_catatan,H.lama_duplikasi,H.orang_duplikasi,H.edited_by,H.edited_date,H.judul_header_eng,H.judul_footer_eng
		FROM setting_opinion H LIMIT 1";
		$hasil=$this->db->query($q)->row_array();
		return $hasil;
		
	}
}
