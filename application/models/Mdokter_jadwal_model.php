<?php

class Mdokter_jadwal_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllDokterUmum()
    {
        $this->db->where('mdokter.idkategori', '1');
        $query = $this->db->get('mdokter');
        return $query->result();
    }

    public function checkStatusJadwal()
    {
        $this->db->where('periode', date('Y-m'));
        $query = $this->db->get('mdokter_jadwal');
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function checkCreatedInfo($periode)
    {
        $this->db->where('periode', $periode);
        $this->db->limit('1');
        $query = $this->db->get('mdokter_jadwal');
        return $query->row();
    }

    public function getListJadwal($periode)
    {
        $this->db->select('mdokter_jadwal.*,
          dokter_pagi.nama AS nama_dokter_pagi,
          dokter_siang.nama AS nama_dokter_siang,
          dokter_malam.nama AS nama_dokter_malam');
        $this->db->join('mdokter AS dokter_pagi', 'dokter_pagi.id = mdokter_jadwal.iddokter_pagi', 'LEFT');
        $this->db->join('mdokter AS dokter_siang', 'dokter_siang.id = mdokter_jadwal.iddokter_siang', 'LEFT');
        $this->db->join('mdokter AS dokter_malam', 'dokter_malam.id = mdokter_jadwal.iddokter_malam', 'LEFT');
        $this->db->where('periode', $periode);
        $query = $this->db->get('mdokter_jadwal');
        return $query->result();
    }

    public function save()
    {
        $createdInfo = $this->checkCreatedInfo($_POST['periode']);
        if ($createdInfo) {
            $created_by = $row_created->created_by;
            $created_at = $row_created->created_at;
        } else {
          $created_by = '';
          $created_at = '';
        }

        $this->db->where('periode', $_POST['periode']);
        if ($this->db->delete('mdokter_jadwal')) {
            $detail_list = json_decode($_POST['detail_value']);
            foreach ($detail_list as $row) {
                $detail = array();
                $detail['periode']         = $_POST['periode'];
                $detail['tanggal']         = $row[0];
                $detail['iddokter_pagi']   = $row[2];
                $detail['iddokter_siang']  = $row[4];
                $detail['iddokter_malam']  = $row[6];
                $detail['status_verifikasi']  = $row[9];

                if ($created_by == '') {
                    $detail['created_by'] = $this->session->userdata('user_id');
                    $detail['created_at'] = date('Y-m-d H:i:s');
                } else {
                    $detail['created_by'] = $created_by;
                    $detail['created_at'] = $created_at;
                    $detail['edited_by']  = $this->session->userdata('user_id');
                    $detail['edited_at']  = date('Y-m-d H:i:s');
                }

                $this->db->insert('mdokter_jadwal', $detail);
            }
        }

        return true;
    }
}
