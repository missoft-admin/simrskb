<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tbagi_hasil_model extends CI_Model
{
  
   public function list_biaya(){
	   $q="SELECT id,nama from mbiaya_operasional 
				WHERE mbiaya_operasional.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_mbagi_hasil(){
	   $q="SELECT id,nama from mbagi_hasil 
				WHERE mbagi_hasil.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function detail($id){
	   $q="SELECT * from tbagi_hasil 
				WHERE tbagi_hasil.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   function generate_ausansi_pending(){
	   $q="SELECT D.*,KD.klaim_id as tklaim_id_ref,KD.id as tkaim_id_det_ref
			FROM tbagi_hasil_detail D
			LEFT JOIN tbagi_hasil H ON H.id=D.tbagi_hasil_id
			LEFT JOIN tklaim_detail KD ON D.idtransaksi=KD.pendaftaran_id AND KD.tipe=D.jenis_transaksi_id AND KD.status_lunas='1'
			WHERE D.`status`='2'";
		$res=$this->db->query($q)->result();
		foreach($res as $row){
			$result_setting=$this->get_setting_bagi_hasil($row->id);
			if ($result_setting){
				$tbagi_hasil_id='';
				if ($result_setting->tbagi_hasil_id == null){//Jika Belum Ada di tbagi_hasil
					$data_header=array(
						'mbagi_hasil_id'=>$result_setting->mbagi_hasil_id,
						'nama_bagi_hasil'=>$result_setting->nama_bagi_hasil,
						'deskripsi'=>$result_setting->deskripsi,
						'tanggal_tagihan'=>$result_setting->next_date,
						'bagian_rs_persen'=>$result_setting->bagian_rs,
						'bagian_ps_persen'=>$result_setting->bagian_ps,
						'created_at' => date('Y-m-d H:i:s'),
						'created_by' => $this->session->userdata('user_id'),
						'status' => 1
					);
					$this->db->insert('tbagi_hasil',$data_header);
					$tbagi_hasil_id= $this->db->insert_id();
					
				}else{
					$tbagi_hasil_id=$result_setting->tbagi_hasil_id;				
				}
				// print_r($row);exit();
				$status_langsung=1;				
				$dataDetailBagiHasil = array(
				  'tbagi_hasil_id' => $tbagi_hasil_id,
				  'mbagi_hasil_pelayanan_id' => $row->mbagi_hasil_pelayanan_id,
				  'idtransaksi' => $row->idtransaksi,
				  'jenis_transaksi' =>$row->jenis_transaksi,
				  'jenis_transaksi_id' => $row->jenis_transaksi_id,
				  'jenis_tindakan' => $row->jenis_tindakan,
				  'reference_table' => $row->reference_table,
				  'tanggal' => YMDFormat($row->tanggal),
				  'jenis_pasien' => $row->jenis_pasien,
				  'idkelompok' => $row->idkelompok,
				  'namakelompok' => $row->namakelompok,
				  'idrekanan' => $row->idrekanan,
				  'namarekanan' => $row->namarekanan,
				  'iddetail' => $row->iddetail,
				  'idtarif' => $row->idtarif,
				  'namatarif' => $row->namatarif,
				  'kuantitas' => $row->kuantitas,
				  'status_asuransi' => $row->status_asuransi,
				  'jasasarana' => $row->jasasarana,
				  'jasapelayanan' => $row->jasapelayanan,
				  'bhp' => $row->bhp,
				  'biayaperawatan' => $row->biayaperawatan,
				  'nominal_jasasarana' => $row->nominal_jasasarana,
				  'nominal_jasapelayanan' => $row->nominal_jasapelayanan,
				  'nominal_bhp' => $row->nominal_bhp,
				  'nominal_biayaperawatan' => $row->nominal_biayaperawatan,			  
				  'total_pendapatan' => $row->total_pendapatan,			  
				  'tklaim_id' => $row->tklaim_id_ref,			  
				  'tkaim_id_det' => $row->tkaim_id_det_ref,			  
				  'status' => $status_langsung,//Jika 1=Langsung;2:Hold Asuransi
				);
				$hasil=$this->db->insert('tbagi_hasil_detail', $dataDetailBagiHasil);
				if ($hasil){
					$this->db->where('id',$row->id);
					$this->db->delete('tbagi_hasil_detail');
				}
			}
			
		}
		// print_r('BERAHSIL');
   }
   function get_setting_bagi_hasil($id){
	   $q="SELECT tbl.*,T.id as tbagi_hasil_id FROM (
			SELECT H.mbagi_hasil_id,M.nama as nama_bagi_hasil,M.keterangan as deskripsi,M.bagian_rs,M.bagian_ps
			,cek_next_date_bagi_hasil(H.mbagi_hasil_id) as next_date
			from tbagi_hasil_detail D
						LEFT JOIN tbagi_hasil H ON H.id=D.tbagi_hasil_id
						LEFT JOIN mbagi_hasil M ON M.id=H.mbagi_hasil_id
						WHERE D.id='$id'
			) tbl 
			LEFT JOIN tbagi_hasil T ON T.mbagi_hasil_id=tbl.mbagi_hasil_id AND T.tanggal_tagihan=tbl.next_date AND T.`status`=1 AND T.status_stop='0' LIMIT 1";
			return $this->db->query($q)->row();
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
