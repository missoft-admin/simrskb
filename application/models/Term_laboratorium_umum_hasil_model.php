<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Term_laboratorium_umum_hasil_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_tujuan_laboratorium()
    {
        $user_id = $this->session->userdata('login_ppa_id');

        $this->db->select('merm_pengaturan_tujuan_laboratorium.*');
        $this->db->from('merm_pengaturan_tujuan_laboratorium');
        $this->db->join('merm_pengaturan_tujuan_laboratorium_user_akses', 'merm_pengaturan_tujuan_laboratorium_user_akses.idpengaturan = merm_pengaturan_tujuan_laboratorium.id');
        $this->db->where('merm_pengaturan_tujuan_laboratorium.status', '1');
        $this->db->where('merm_pengaturan_tujuan_laboratorium.tipe_layanan', '1');
        $this->db->where('merm_pengaturan_tujuan_laboratorium_user_akses.iduser', $user_id);

        $query = $this->db->get();
        return $query->result();
    }

    public function get_data_transaksi($transaksi_id)
    {
        $this->db->select('
            term_laboratorium_umum.*,
            COALESCE ( tpoliklinik_pendaftaran.no_medrec, poliklinik_ranap.nomor_medrec) AS nomor_medrec,
            COALESCE ( tpoliklinik_pendaftaran.namapasien, poliklinik_ranap.nama_pasien) AS nama_pasien,
            COALESCE ( tpoliklinik_pendaftaran.nik, poliklinik_ranap.nomor_ktp) AS nomor_ktp,
            COALESCE ( referensi_jenis_kelamin.ref, poliklinik_ranap.jenis_kelamin) AS jenis_kelamin,
            COALESCE ( tpoliklinik_pendaftaran.email, poliklinik_ranap.email) AS email,
            COALESCE ( tpoliklinik_pendaftaran.alamatpasien, poliklinik_ranap.alamat_pasien) AS alamatpasien,
            COALESCE ( tpoliklinik_pendaftaran.tanggal_lahir, poliklinik_ranap.tanggal_lahir) AS tanggal_lahir,
            COALESCE ( tpoliklinik_pendaftaran.umurhari, poliklinik_ranap.umur_hari) AS umur_hari,
            COALESCE ( tpoliklinik_pendaftaran.umurbulan, poliklinik_ranap.umur_bulan) AS umur_bulan,
            COALESCE ( tpoliklinik_pendaftaran.umurtahun, poliklinik_ranap.umur_tahun) AS umur_tahun,
            COALESCE ( mpasien_kelompok.nama, poliklinik_ranap.kelompok_pasien) AS kelompok_pasien,
            COALESCE ( mrekanan.nama, poliklinik_ranap.rekanan_asuransi) AS nama_asuransi,
            (CASE
                WHEN term_laboratorium_umum.asal_rujukan = 1 THEN "Poliklinik"
                WHEN term_laboratorium_umum.asal_rujukan = 2 THEN "Instalasi Gawat Darurat"
                WHEN term_laboratorium_umum.asal_rujukan = 3 THEN "Rawat Inap"
                WHEN term_laboratorium_umum.asal_rujukan = 4 THEN "One Day Surgery"
            END) AS tipe_kunjungan,
            mpoliklinik.nama AS nama_poliklinik,
            (CASE
                WHEN term_laboratorium_umum.asal_rujukan IN (1, 2) THEN
                    tpoliklinik_pendaftaran.nopendaftaran
                WHEN term_laboratorium_umum.asal_rujukan IN (3, 4) THEN
                    poliklinik_ranap.nopendaftaran
            END) AS nomor_pendaftaran,
            term_laboratorium_umum.prioritas AS prioritas_pemeriksaan,
            mdokter_perujuk.nama AS dokter_perujuk,
            mdokter_laboratorium.nama AS dokter_laboratorium,
            muser_pemeriksa.nama AS user_input_pemeriksaan,
            muser_sampling.nama AS user_pengambilan_sample,
            muser_hasil.nama AS user_input_hasil,
            term_laboratorium_umum.created_at AS waktu_pembuatan,
            term_laboratorium_umum.waktu_pengambilan_sample AS tanggal_pengambilan_sample,
            trujukan_laboratorium.norujukan AS nomor_transaksi_laboratorium'
        );
        $this->db->from('term_laboratorium_umum');
        $this->db->join('(SELECT
            trawatinap_pendaftaran.id,
            trawatinap_pendaftaran.nopendaftaran,
            tpoliklinik_pendaftaran.kode_antrian,
            tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
            tpoliklinik_pendaftaran.idpasien AS pasien_id,
            tpoliklinik_pendaftaran.namapasien AS nama_pasien,
            tpoliklinik_pendaftaran.nik AS nomor_ktp,
            referensi_jenis_kelamin.ref AS jenis_kelamin,
            tpoliklinik_pendaftaran.email,
            tpoliklinik_pendaftaran.alamatpasien AS alamat_pasien,
            tpoliklinik_pendaftaran.tanggal_lahir,
            tpoliklinik_pendaftaran.umurhari AS umur_hari,
            tpoliklinik_pendaftaran.umurbulan AS umur_bulan,
            tpoliklinik_pendaftaran.umurtahun AS umur_tahun,
            mpasien_kelompok.nama AS kelompok_pasien,
            mrekanan.nama AS rekanan_asuransi
        FROM
            trawatinap_pendaftaran
        INNER JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
        INNER JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien
        LEFT JOIN mrekanan ON mrekanan.id = trawatinap_pendaftaran.idrekanan
        INNER JOIN merm_referensi referensi_jenis_kelamin ON referensi_jenis_kelamin.nilai = tpoliklinik_pendaftaran.jenis_kelamin AND referensi_jenis_kelamin.ref_head_id = 1) AS poliklinik_ranap', 'term_laboratorium_umum.pendaftaran_id = poliklinik_ranap.id AND term_laboratorium_umum.asal_rujukan IN (3, 4)', 'LEFT');
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_laboratorium_umum.pendaftaran_id AND term_laboratorium_umum.asal_rujukan IN (1, 2)', 'LEFT');
        $this->db->join('merm_referensi referensi_jenis_kelamin', 'referensi_jenis_kelamin.nilai = tpoliklinik_pendaftaran.jenis_kelamin AND referensi_jenis_kelamin.ref_head_id = 1', 'LEFT');
        $this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
        $this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
        $this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
        $this->db->join('mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_laboratorium_umum.dokter_perujuk_id', 'LEFT');
		$this->db->join('mdokter mdokter_laboratorium', 'mdokter_laboratorium.id = term_laboratorium_umum.dokter_laboratorium', 'LEFT');
        $this->db->join('mppa muser_pemeriksa', 'muser_pemeriksa.id = term_laboratorium_umum.created_ppa', 'LEFT');
		$this->db->join('mppa muser_sampling', 'muser_sampling.id = term_laboratorium_umum.petugas_pengambilan_sample', 'LEFT');
		$this->db->join('mppa muser_hasil', 'muser_hasil.id = term_laboratorium_umum.petugas_penginputan_hasil', 'LEFT');
        $this->db->join('trujukan_laboratorium', 'trujukan_laboratorium.id = term_laboratorium_umum.rujukan_id', 'LEFT');
        $this->db->where('term_laboratorium_umum.id', $transaksi_id);

        $query = $this->db->get();
        return $query->row();
    }

    public function get_petugas_proses()
	{
		$this->db->select('jenis_petugas_proses_id');
        $query = $this->db->get('merm_pengaturan_form_laboratorium_umum_hasil_petugas');

        if ($query->num_rows() > 0) {
            $result = $query->result_array();

            // Extracting the values into a simple array
            $jenisPetugasProsesIds = array_column($result, 'jenis_petugas_proses_id');

            // Query to get data from mppa table
            $this->db->select('id, nama');
            $this->db->from('mppa');
            $this->db->where('staktif', 1);
            $this->db->where_in('jenis_profesi_id', $jenisPetugasProsesIds);
            $queryMPPA = $this->db->get();

            if ($queryMPPA->num_rows() > 0) {
                return $queryMPPA->result();
            } else {
                return array();
            }
        } else {
            return array();
        }
	}

    public function get_daftar_pemeriksaan($id)
	{
        $query = $this->db->query('SELECT
            term_laboratorium_umum_pemeriksaan.*,
            merm_referensi_flag.ref AS flag,
            merm_referensi_sumber_spesimen.ref AS sumber_spesimen,
            mtarif_laboratorium.idkelompok,
            mtarif_laboratorium.idpaket,
            mtarif_laboratorium.LEVEL,
            mtarif_laboratorium.nama,
            mtarif_laboratorium.path,
            CAST(REPLACE(mtarif_laboratorium.path, ".", "") AS SIGNED) pathile,
            IF(st_paket = 0 AND mtarif_laboratorium.idpaket = 1, mtarif_laboratorium.idkelompok, term_laboratorium_umum_pemeriksaan.st_paket) AS kelompok
        FROM
            term_laboratorium_umum_pemeriksaan
            LEFT JOIN mtarif_laboratorium ON term_laboratorium_umum_pemeriksaan.idlaboratorium = mtarif_laboratorium.id
			LEFT JOIN merm_referensi AS merm_referensi_flag ON merm_referensi_flag.nilai = term_laboratorium_umum_pemeriksaan.flag AND merm_referensi_flag.ref_head_id = 118
			LEFT JOIN merm_referensi AS merm_referensi_sumber_spesimen ON merm_referensi_sumber_spesimen.nilai = term_laboratorium_umum_pemeriksaan.sumber_spesimen AND merm_referensi_sumber_spesimen.ref_head_id = 90
        WHERE
            term_laboratorium_umum_pemeriksaan.transaksi_id = ' . $id . ' AND
            term_laboratorium_umum_pemeriksaan.status = 1
        ORDER BY
            kelompok,
            SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(mtarif_laboratorium.path, "."), ".", 1), ".", -1) + 0,
            SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(mtarif_laboratorium.path, "."), ".", 2), ".", -1) + 0,
            SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(mtarif_laboratorium.path, "."), ".", 3), ".", -1) + 0,
            SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(mtarif_laboratorium.path, "."), ".", 4), ".", -1) + 0');
        return $query->result();
	}

    public function get_tarif_laboratorium($id)
	{
		$this->db->select('mtarif_laboratorium.nama,
            mtarif_laboratorium.metode,
            mtarif_laboratorium.sumber_spesimen,
            COALESCE(msatuan.singkatan, " -") AS singkatan_satuan,
            GROUP_CONCAT(COALESCE(mnilainormal_kriteria.kriteria, ""), " : ", mnilainormal_kriteria.nilai SEPARATOR "<br>") AS nilai_normal,
            GROUP_CONCAT(COALESCE(mnilainormal_kritis.kriteria, ""), " : ", mnilainormal_kritis.nilai SEPARATOR "<br>") AS nilai_kritis');
		$this->db->join('mnilainormal_pelayanan', 'mnilainormal_pelayanan.idtariflaboratorium = mtarif_laboratorium.id', 'LEFT');
		$this->db->join('mnilainormal', 'mnilainormal.id = mnilainormal_pelayanan.idnilainormal', 'LEFT');
		$this->db->join('mnilainormal_kriteria', 'mnilainormal_kriteria.idnilainormal = mnilainormal.id', 'LEFT');
		$this->db->join('mnilainormal_kritis', 'mnilainormal_kritis.idnilainormal = mnilainormal.id', 'LEFT');
		$this->db->join('msatuan', 'msatuan.id = mnilainormal.idsatuan', 'LEFT');
		$this->db->where('mtarif_laboratorium.id', $id);
		$this->db->where('mnilainormal.status', 1);
		$this->db->group_by('mnilainormal_pelayanan.id');
		$this->db->group_by('mnilainormal_pelayanan.idnilainormal');
		$this->db->group_by('mnilainormal_pelayanan.idtariflaboratorium');
		$query = $this->db->get('mtarif_laboratorium');
		return $query->row();
	}

    public function get_tarif_laboratorium_all()
	{
		$this->db->select('mtarif_laboratorium.id,mtarif_laboratorium.nama,
            mtarif_laboratorium.metode,
            mtarif_laboratorium.sumber_spesimen,
            COALESCE(msatuan.singkatan, " -") AS singkatan_satuan,
            GROUP_CONCAT(COALESCE(mnilainormal_kriteria.kriteria, ""), " : ", mnilainormal_kriteria.nilai SEPARATOR "<br>") AS nilai_normal,
            GROUP_CONCAT(COALESCE(mnilainormal_kritis.kriteria, ""), " : ", mnilainormal_kritis.nilai SEPARATOR "<br>") AS nilai_kritis');
		$this->db->join('mnilainormal_pelayanan', 'mnilainormal_pelayanan.idtariflaboratorium = mtarif_laboratorium.id', 'LEFT');
		$this->db->join('mnilainormal', 'mnilainormal.id = mnilainormal_pelayanan.idnilainormal', 'LEFT');
		$this->db->join('mnilainormal_kriteria', 'mnilainormal_kriteria.idnilainormal = mnilainormal.id', 'LEFT');
		$this->db->join('mnilainormal_kritis', 'mnilainormal_kritis.idnilainormal = mnilainormal.id', 'LEFT');
		$this->db->join('msatuan', 'msatuan.id = mnilainormal.idsatuan', 'LEFT');
		$this->db->where('mnilainormal.status', 1);
		$this->db->group_by('mnilainormal_pelayanan.id');
		$this->db->group_by('mnilainormal_pelayanan.idnilainormal');
		$this->db->group_by('mnilainormal_pelayanan.idtariflaboratorium');
		$query = $this->db->get('mtarif_laboratorium');
		return $query->result();
	}

    public function update_transaksi()
	{
		$this->db->set('nomor_laboratorium', $_POST['nomor_laboratorium']);
		$this->db->set('prioritas', $_POST['prioritas_pemeriksaan']);
		$this->db->set('pasien_puasa', $_POST['pasien_puasa']);
		$this->db->set('pengiriman_hasil', $_POST['pengiriman_hasil']);
		$this->db->set('waktu_pengambilan_sample', YMDTimeFormat($this->input->post('tanggal_pengambilan_sample') . ' ' . $this->input->post('waktu_pengambilan_sample')));
		$this->db->set('petugas_pengambilan_sample', $_POST['petugas_pengambilan_sample']);
		$this->db->set('waktu_pengujian_sample', YMDTimeFormat($this->input->post('tanggal_pengujian') . ' ' . $this->input->post('waktu_pengujian_sample')));
		$this->db->set('petugas_pengujian_sample', $_POST['petugas_pengujian_sample']);
		$this->db->set('dokter_laboratorium', $_POST['dokter_laboratorium']);
		$this->db->set('catatan', $_POST['catatan']);
		$this->db->set('interpretasi_hasil_pemeriksaan', $_POST['interpretasi_hasil_pemeriksaan']);
		$this->db->set('petugas_penginputan_hasil', $this->session->userdata('user_id'));
		$this->db->set('waktu_penginputan_hasil', date("Y-m-d H:i:s"));

        $formSubmit = $_POST['form_submit'];
        if ($formSubmit == 'form-submit') {
            $this->db->set('status_pemeriksaan', '5');
        } else if ($formSubmit == 'form-submit-and-validation') {
            $this->db->set('status_pemeriksaan', '6');
        }

		$this->db->where('id', $_POST['id']);
		if ($this->db->update('term_laboratorium_umum')) {
			$dataPemeriksaan = json_decode($_POST['data_pemeriksaan']);
			foreach ($dataPemeriksaan as $row) {
				$data = [
					'rujukan_keluar' => $row->rujukan_keluar,
					'hasil' => $row->hasil,
					'flag' => $row->flag,
					'flag_normal' => $row->flag_normal,
					'flag_kritis' => $row->flag_kritis,
					'satuan' => $row->satuan,
					'nilainormal' => $row->nilainormal,
					'nilaikritis' => $row->nilaikritis,
					'metode' => $row->metode,
					'sumber_spesimen' => $row->sumber_spesimen,
					'keterangan' => $row->keterangan,
					'petugas_proses' => $row->petugas_proses
				];

				$this->db->update('term_laboratorium_umum_pemeriksaan', $data, ['id' => $row->id]);
			}
		}

		return true;
	}

    public function get_hasil_pemeriksaan_terakhir($transaksi_id) {
        $this->db->select('id');
        $this->db->where('transaksi_id', $transaksi_id);
        $this->db->where('status', 1);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('term_laboratorium_umum_hasil_pemeriksaan');

        $result = $query->row();

        return $result ? $result->id : null;
    }

    public function get_version_hasil_pemeriksaan_terakhir($id) {
        $this->db->select_max('version');
        $this->db->where('transaksi_id', $id);
        $query = $this->db->get('term_laboratorium_umum_hasil_pemeriksaan');
        $result = $query->row_array();

        return isset($result['version']) ? $result['version'] : 0;
    }

    public function get_riwayat_cetak_hasil_pemeriksaan($hasil_pemeriksaan_id) {
        $this->db->select('term_laboratorium_umum_hasil_pemeriksaan_history.*,
            petugas_cetak_hasil.nama AS petugas_cetak_hasil,');
        $this->db->join('mppa petugas_cetak_hasil', 'petugas_cetak_hasil.id = term_laboratorium_umum_hasil_pemeriksaan_history.petugas_cetak_hasil', 'LEFT');
        $this->db->order_by('urutan_cetak', 'DESC');
        $this->db->where('hasil_pemeriksaan_id', $hasil_pemeriksaan_id);
        $query = $this->db->get('term_laboratorium_umum_hasil_pemeriksaan_history');
        
        return $query->result_array();
    }

    public function get_data_pengiriman_hasil_pemeriksaan_pertama($id)
    {
        $this->db->select('term_laboratorium_umum_hasil_pemeriksaan_email.*,
            petugas_pengiriman_hasil.nama AS petugas_pengiriman_hasil,
            petugas_penerimaan_hasil.nama AS petugas_penerimaan_hasil');
        $this->db->join('term_laboratorium_umum_hasil_pemeriksaan', 'term_laboratorium_umum_hasil_pemeriksaan.id = term_laboratorium_umum_hasil_pemeriksaan_email.hasil_pemeriksaan_id', 'left');
        $this->db->join('mppa petugas_pengiriman_hasil', 'petugas_pengiriman_hasil.id = term_laboratorium_umum_hasil_pemeriksaan_email.petugas_pengiriman_hasil', 'LEFT');
        $this->db->join('mppa petugas_penerimaan_hasil', 'petugas_penerimaan_hasil.id = term_laboratorium_umum_hasil_pemeriksaan_email.petugas_penerimaan_hasil', 'LEFT');
        $this->db->where('term_laboratorium_umum_hasil_pemeriksaan.transaksi_id', $id);
        $this->db->limit(1);

        $query = $this->db->get('term_laboratorium_umum_hasil_pemeriksaan_email');
        return $query->row();
    }
    
    public function get_file_hasil_pemeriksaan($id) {
        $this->db->select('id, nama_file');
        $this->db->where('transaksi_id', $id);
        $this->db->where('status', 1);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('term_laboratorium_umum_hasil_pemeriksaan');
        return $query->row();
    }

    public function get_email_pasien($pasienId) {
        $this->db->select('email');
        $this->db->where('id', $pasienId);
        $query = $this->db->get('mfpasien');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function get_email_pengiriman_hasil_pemeriksaan_terakhir($id) {
        $this->db->select('term_laboratorium_umum_hasil_pemeriksaan_email.email_penerima AS email');
        $this->db->join('term_laboratorium_umum_hasil_pemeriksaan_email', 'term_laboratorium_umum_hasil_pemeriksaan_email.hasil_pemeriksaan_id = term_laboratorium_umum_hasil_pemeriksaan.id');
        $this->db->where('term_laboratorium_umum_hasil_pemeriksaan.transaksi_id', $id);
        $this->db->order_by('term_laboratorium_umum_hasil_pemeriksaan_email.id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('term_laboratorium_umum_hasil_pemeriksaan');

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return array();
        }
    }
    
    public function get_pengaturan_attachment_email()
    {
        $query = $this->db->get('merm_pengaturan_pengiriman_email_laboratorium_file');
        return $query->result();
    }

    private function get_jumlah_cetak_hasil($id) {
        $this->db->select('jumlah_cetak');
        $this->db->where('id', $id);
        $query = $this->db->get('term_laboratorium_umum_hasil_pemeriksaan');
        $result = $query->row_array();

        return isset($result['jumlah_cetak']) ? $result['jumlah_cetak'] : 0;
    }

    public function log_cetak_hasil($id)
    {
        $log_history_data = [
            'hasil_pemeriksaan_id' => $id,
            'urutan_cetak' => $this->get_jumlah_cetak_hasil($id) + 1,
            'waktu_cetak_hasil' => date('Y-m-d H:i:s'),
            'petugas_cetak_hasil' => $this->session->userdata('user_id'),
        ];

        $this->db->insert('term_laboratorium_umum_hasil_pemeriksaan_history', $log_history_data);

        // Update jumlah cetak di tabel term_laboratorium_umum_hasil_pemeriksaan
        $this->db->set('jumlah_cetak', $this->get_jumlah_cetak_hasil($id) + 1);
        $this->db->where('id', $id);
        $this->db->update('term_laboratorium_umum_hasil_pemeriksaan');
    }

    public function log_kirim_hasil($data)
    {
        $log_history_data = [
            'hasil_pemeriksaan_id' => $data['hasil_pemeriksaan_id'],
            'waktu_pengiriman_hasil' => $data['waktu_pengiriman_hasil'],
            'petugas_pengiriman_hasil' => $data['petugas_pengiriman_hasil'],
            'waktu_penerimaan_hasil' => $data['waktu_penerimaan_hasil'],
            'petugas_penerimaan_hasil' => $data['petugas_penerimaan_hasil'],
            'penerima_bukan_petugas' => $data['penerima_bukan_petugas'],
            'email_penerima' => $data['email_penerima'],
        ];

        $this->db->insert('term_laboratorium_umum_hasil_pemeriksaan_email', $log_history_data);
    }
}
