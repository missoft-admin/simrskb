<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpasien_penjualan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getIndexPenjualan($sp)
    {
        $this->db->select('*');
        // $this->db->where('status', '1');
        $this->db->where('statuspasien', $sp);
        $query = $this->db->get('tpasien_penjualan');
        return $query->result();
    }


    public function getPasien($id)
    {
        $query = $this->db->query("SELECT
        	tpasien_penjualan.*,
        	tpoliklinik_pendaftaran.idasalpasien,
        	tpasien_penjualan.nomedrec AS no_medrec,
        	tpasien_penjualan.nama AS namapasien,
        	tpasien_penjualan.alamat AS alamat_jalan,
        	tpasien_penjualan.notelp AS telepon,
        	tpoliklinik_tindakan.alergiobat AS alergi_obat,
        	mpasien_kelompok.nama AS namakelompok,
        	mpoliklinik.nama AS namapoliklinik,
        	tpoliklinik_pendaftaran.idrujukan,
        	tpoliklinik_pendaftaran.idtipe,
        	mkelas.nama AS namakelas,
        	mbed.nama AS namabed
        FROM
        	tpasien_penjualan
        	LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = tpasien_penjualan.idtindakan AND tpasien_penjualan.asalrujukan = 3
        	LEFT JOIN tpoliklinik_tindakan ON tpasien_penjualan.idtindakan = tpoliklinik_tindakan.id AND tpasien_penjualan.asalrujukan IN (1,2)
        	LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran OR tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
        	LEFT JOIN mpasien_kelompok ON tpoliklinik_pendaftaran.idkelompokpasien = mpasien_kelompok.id OR trawatinap_pendaftaran.idkelompokpasien = mpasien_kelompok.id
        	LEFT JOIN mpoliklinik ON tpoliklinik_pendaftaran.idpoliklinik = mpoliklinik.id
        	LEFT JOIN mkelas ON mkelas.id = trawatinap_pendaftaran.idkelas
        	LEFT JOIN mbed ON mbed.id = trawatinap_pendaftaran.idbed
        WHERE
        	tpasien_penjualan.id = $id
			GROUP BY tpasien_penjualan.id
			");
        return $query;
    }
    
    // Start Fungsi Save data to table
    // fungsi save penindakan
    public function saveData($jual)
    {
        if ($this->input->post('form_edit')=='1') {
            $this->db->where('id', $this->input->post('id_penjualan'));
            if ($this->db->update('tpasien_penjualan', $jual)) {
				if ($this->input->post('st_rujukan_non_rujukan') == 'R') {
					$this->db->where('id', $this->input->post('idpendaftaran'));
					$this->db->update('tpoliklinik_pendaftaran', ['statustindakan' => '1']);
				}

                return true;
            } else {
                $this->error_message = "Penyimpanan Gagal";
                return false;
            }
        } else {
            if ($this->db->insert('tpasien_penjualan', $jual)) {
                if ($this->input->post('st_rujukan_non_rujukan') == 'R') {
					$this->db->where('id', $this->input->post('idpendaftaran'));
					$this->db->update('tpoliklinik_pendaftaran', ['statustindakan' => '1']);
				}

                return true;
            } else {
                $this->error_message = "Penyimpanan Gagal";
                return false;
            }
        }
    }
	function getBed($idkelas){
		$q="SELECT *from mbed B
		WHERE B.idkelas='$idkelas' AND B.`status`='1'";
		return $this->db->query($q)->result();
	}
    // fungsi save nonracikan
	public function get_ruangan_bed($id){
		$q="SELECT R.idruangan,R.idkelas,R.idbed from tpasien_penjualan P INNER JOIN trawatinap_pendaftaran R ON R.id=P.idtindakan
		where P.id='$id'";
		$query=$this->db->query($q);
		return $query->row();
	}
	public function list_tipe_barang(){
		$q="SELECT H.idtipe,T.nama_tipe from munitpelayanan_tipebarang H
		LEFT JOIN mdata_tipebarang T ON H.idtipe=T.id
		WHERE H.idunitpelayanan='1'";
		$query=$this->db->query($q);
		return $query->result();
	}
    public function get_harga_obat($id){
		$q="SELECT *From mdata_obat where id='$id'";
		$query=$this->db->query($q);
		return $query->row();
	}
    public function saveDataNonracikan($nonrac)
    {
        if ($this->db->insert('tpasien_penjualan_nonracikan', $nonrac)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    // fungsi save racikan
    public function saveDataRacikan($rac)
    {
        // print_r($rac);exit();
        if ($this->db->insert('tpasien_penjualan_racikan', $rac)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    // fungsi save obat racikan
    public function saveDataRacikanObat($obt)
    {
        if ($this->db->insert('tpasien_penjualan_racikan_obat', $obt)) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
    public function batalkan($id)
    {
        if ($this->db->update('tpasien_penjualan', array('status'=>0,'deleted_by'=>$this->session->userdata('user_id'),'deleted_nama'=>$this->session->userdata('user_name'),'deleted_date'=>date('Y-m-d H:i:s')), array('id' => $id))) {
            return true;
        } else {
            return false;
        }
    }
    public function update_total_barang($id, $total_barang)
    {
        if ($this->db->update('tpasien_penjualan', array('totalbarang'=>$total_barang), array('id' => $id))) {
            return true;
        } else {
            return false;
        }
    }
    public function serahkan($id)
    {
        $now=date('Y-m-d H:i:s', strtotime(date('H:i:s')));
        if ($this->db->update('tpasien_penjualan', array('status'=>3,'waktupenyerahan'=>$now,'date_serahkan'=>$now,'user_serahkan'=>$this->session->userdata('user_id')), array('id' => $id))) {
            return true;
        } else {
            return false;
        }
    }
    public function get_dataPenjualan($id)
    {
        $this->db->select('tpasien_penjualan.*');
        $this->db->from('tpasien_penjualan');
        $this->db->where('id', $id);
        $query=$this->db->get();
        return $query->row();
    }
    public function getIdPendaftaran($id)
    {
        $this->db->select('tpoliklinik_pendaftaran.id AS idpendaftaran');
        $this->db->from('tpasien_penjualan');
        $this->db->join('tpoliklinik_tindakan', 'tpoliklinik_tindakan.id = tpasien_penjualan.idtindakan');
        $this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran');
        $this->db->where('tpasien_penjualan.id', $id);
        $query=$this->db->get();
        $row=$query->row();
        if ($query->num_rows() > 0) {
            return $row->idpendaftaran;
        } else {
            return 0;
        }
    }
    public function get_data_lock($id)
    {
        $q="SELECT K.`status` from tkasir K
				WHERE K.idtindakan='$id' AND K.idtipe='3'";
		if ($this->db->query($q)->row('status')=='2'){
			return '1';
		}else{
			return '0';

		}
    }
    public function get_data_lock2($id,$asal)
    {
		if ($asal=='1' || $asal=='2' ) {
			$q="SELECT K.`status` from tkasir K
					WHERE K.idtindakan='$id' AND K.idtipe='$asal'";
			if ($this->db->query($q)->row('status')=='2'){
				return '1';
			}else{
				return '0';

			}
		}else{
			return '0';
		}
    }
    public function get_dokter_default($id,$asal)
    {
		if ($asal=='3'){
			$q="SELECT P.iddokterpenanggungjawab AS iddokter
				FROM tpasien_penjualan J
				LEFT JOIN trawatinap_pendaftaran P ON J.idtindakan = P.id
				WHERE
				J.id = '$id'";
		}else{
			$q="SELECT T.iddokter1 as iddokter from tpasien_penjualan J
				LEFT JOIN tpoliklinik_tindakan T ON J.idtindakan=T.id WHERE J.id='$id'";
		}
			// print_r($q);exit();
        $query=$this->db->query($q);

        return $query->row('iddokter');
    }
    public function get_apoteker()
    {
        $this->db->select('mpegawai.*');
        $this->db->from('mpegawai');
        $this->db->where('idkategori', 6);
        $this->db->limit(1);
        $query=$this->db->get();
        // print_r($query->row('id'));exit();
        return $query->row('id');
    }
    public function get_apoteker_by_penjualan($id)
    {
        $this->db->select('mpegawai.*');
        $this->db->from('tpasien_penjualan');
        $this->db->join('mpegawai', 'mpegawai.id=tpasien_penjualan.idpegawaiapoteker');
        $this->db->where('tpasien_penjualan.id', $id);
        $this->db->limit(1);
        $query=$this->db->get();
        return $query->row();
    }
	public function get_apoteker_by_setting()
    {
        $this->db->select('setting_eresep_print.sipa as nip,setting_eresep_print.apoteker as nama');
        $this->db->from('setting_eresep_print');
        $this->db->limit(1);
        $query=$this->db->get();
        return $query->row();
    }

    public function idracikan($table)
    {
        $q = $this->db->query("SELECT MAX(id) AS idmax FROM ".$table);
        $kd = "";
        if ($q->num_rows()>0) {
            foreach ($q->result() as $k) {
                $kd = ((int)$k->idmax)+1;
            }
        } else {
            $kd = "1";
        }
        return $kd;
    }

    public function getSUMObat($idobat=null)
    {
        return $this->db->select('(select
Sum(mgudang_stok.stok)
from mgudang_stok mgudang_stok where mgudang_stok.idbarang=t1.idbarang) AS stok,
                                t2.id as idobat,
                                t2.nama as namaObat,
                                t2.*,
                                t3.nama as namasatuan,
                                t3.singkatan')
                      ->join('mdata_obat t2', 't1.idbarang = t2.id')
                      ->join('msatuan t3', 't2.idsatuankecil = t3.id')
                      ->where('t1.idtipe', 3)
                      ->where('t1.idbarang', $idobat)
                      ->group_by('t1.idbarang')
                      ->get('mgudang_stok t1');
    }

    public function getObat($idobat=null)
    {
        return $this->db->select('t1.id as idstok,
                                t1.*,
                                t2.id as idobat,
                                t2.nama as namaObat,
                                t2.*,
                                t3.nama as namasatuan,
                                t3.singkatan')
                      ->join('mdata_obat t2', 't1.idbarang = t2.id')
                      ->join('msatuan t3', 't2.idsatuankecil = t3.id')
                      ->where('t1.idtipe', 3)
                      ->where('t1.id', $idobat)
                      // ->where('t2.id',$idobat)
                      ->get('mgudang_stok t1');
    }

    public function countRacik($id)
    {
        return $this->db->select('t1.*,
                                t1.id as idracik,
                                Count(t2.idracikan) as totalobat,
                                t1.totalharga,
                                t1.kuantitas AS qtyracik,
                                t1.harga AS hargaRAC,
                                t2.*,
                                t2.kuantitas AS qtyradet,
                                t2.harga AS hargaradet')
                      ->join('tpasien_penjualan_racikan_obat t2', 't2.idracikan = t1.id')
                      ->where('t1.idpenjualan', $id)
                      ->group_by("t1.idpenjualan")
                      ->group_by('t1.id')
                      ->get('tpasien_penjualan_racikan t1');
    }
    public function get_obat($searchText)
    {
        $this->db->select("view_barang_farmasi.*,mgudang_stok.stok");
        $this->db->from('view_barang_farmasi');
        $this->db->join('mgudang_stok', 'mgudang_stok.idbarang=view_barang_farmasi.id AND mgudang_stok.idunitpelayanan=1 AND mgudang_stok.idtipe=view_barang_farmasi.idtipe');
        $this->db->like('view_barang_farmasi.nama', $searchText);
        $this->db->or_like('view_barang_farmasi.kode', $searchText);
        $query = $this->db->get();
        // print_r($this->db->last_query());exit();
        return $query->result_array();
    }
    public function get_dokter_pegawai($searchText, $kategori)
    {
        if ($kategori=='2') {//Pegawai
            $this->db->select("mpegawai.id,mpegawai.nama,nip");
            $this->db->from('mpegawai');
            $this->db->like('mpegawai.nama', $searchText);
            $this->db->or_like('mpegawai.nip', $searchText);
        } else {//Dokter
            $this->db->select("mdokter.id,mdokter.nama,nip");
            $this->db->from('mdokter');
            $this->db->like('mdokter.nama', $searchText);
            $this->db->or_like('mdokter.nip', $searchText);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_obat_detail($id, $kelompok_pasien=0, $idtipe)
    {

        $q="SELECT view_barang_farmasi.*, msatuan.singkatan,(CASE
				WHEN $kelompok_pasien = 5 THEN
					marginumum
				WHEN $kelompok_pasien = 0 THEN
					marginumum
				WHEN $kelompok_pasien = 1 THEN
					marginasuransi
				WHEN $kelompok_pasien = 2 THEN
					marginjasaraharja
				WHEN $kelompok_pasien = 3 THEN
					marginbpjskesehatan
				WHEN $kelompok_pasien = 4 THEN
					marginbpjstenagakerja
			END) AS margin,mgudang_stok.stok
			 FROM view_barang_farmasi
			 INNER JOIN msatuan ON msatuan.id=view_barang_farmasi.idsatuankecil
			 INNER JOIN mgudang_stok ON mgudang_stok.idbarang = view_barang_farmasi.id AND mgudang_stok.idtipe=view_barang_farmasi.idtipe AND idunitpelayanan=1
			 WHERE view_barang_farmasi.id = '$id' AND view_barang_farmasi.idtipe='$idtipe'";
        $query=$this->db->query($q);
        return $query->row();
    }
    public function get_dokter_pegawai_detail($id, $kategori=0)
    {
        if ($kategori=='2') {//Pegawai
            $this->db->select("mpegawai.*");
            $this->db->from('mpegawai');
            $this->db->where('id', $id);
        } else {//Dokter
            $this->db->select("mdokter.*");
            $this->db->from('mdokter');
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        return $query->row();
    }
    public function get_header($id)
    {
        $this->db->select('tpasien_penjualan.*');
        $this->db->from('tpasien_penjualan');
        $this->db->where('id', $id);
        $query=$this->db->get();
        return $query->row();
    }
    public function list_data($id)
    {
        $this->db->select("tpasien_penjualan_nonracikan.*,view_barang_farmasi.nama");
        $this->db->from('tpasien_penjualan_nonracikan');
        $this->db->join('view_barang_farmasi', 'view_barang_farmasi.id=tpasien_penjualan_nonracikan.idbarang AND view_barang_farmasi.idtipe=tpasien_penjualan_nonracikan.idtipe');
        $this->db->where('tpasien_penjualan_nonracikan.idpenjualan', $id);
        $this->db->order_by('tpasien_penjualan_nonracikan.id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function list_dokter()
    {
        $this->db->select("mdokter.*");
        $this->db->from('mdokter');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_edit_list_racikan_tabel($id)
    {
        $this->db->select("tpasien_penjualan_racikan.*
		,tpasien_penjualan_racikan_obat.idracikan
		,tpasien_penjualan_racikan_obat.idbarang
		,tpasien_penjualan_racikan_obat.kuantitas as kuantitas_detail
		,tpasien_penjualan_racikan_obat.harga as harga_detail
		,tpasien_penjualan_racikan_obat.diskon as diskon_detail
		,tpasien_penjualan_racikan_obat.idtipe
		,msatuan.singkatan
		,view_barang_farmasi.nama");
        $this->db->from('tpasien_penjualan_racikan');
        $this->db->join('tpasien_penjualan_racikan_obat', 'tpasien_penjualan_racikan_obat.idracikan=tpasien_penjualan_racikan.id');
        $this->db->join('view_barang_farmasi', 'tpasien_penjualan_racikan_obat.idbarang=view_barang_farmasi.id AND tpasien_penjualan_racikan_obat.idtipe=view_barang_farmasi.idtipe');
        $this->db->join('msatuan', 'msatuan.id=view_barang_farmasi.idsatuankecil');
        $this->db->where('tpasien_penjualan_racikan.idpenjualan', $id);
        $query = $this->db->get();
        // print_r($this->db->last_query());exit();
        return $query->result();
    }
    public function get_edit_list_non_racikan($id)
    {
        $this->db->select("tpasien_penjualan_nonracikan.*,view_barang_farmasi.nama,msatuan.singkatan");
        $this->db->from('tpasien_penjualan_nonracikan');
        $this->db->join('view_barang_farmasi', 'view_barang_farmasi.id=tpasien_penjualan_nonracikan.idbarang AND view_barang_farmasi.idtipe=tpasien_penjualan_nonracikan.idtipe ');
        $this->db->join('msatuan', 'msatuan.id=view_barang_farmasi.idsatuankecil');
        $this->db->where('tpasien_penjualan_nonracikan.idpenjualan', $id);
        $this->db->order_by('tpasien_penjualan_nonracikan.id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function total_non_racikan($id)
    {
        $this->db->select("SUM(totalharga) as total");
        $this->db->from('tpasien_penjualan_nonracikan');
        $this->db->where('tpasien_penjualan_nonracikan.idpenjualan', $id);
        $query = $this->db->get();
        if ($query->row('total')) {
            return $query->row('total');
        } else {
            return 0;
        }
        // print_r( $query->row('total'));exit();
    }
    public function list_data_racikan($id)
    {
        // $this->db->select("tpasien_penjualan_racikan.*");
        // $this->db->from('tpasien_penjualan_racikan');
        // $this->db->where('tpasien_penjualan_racikan.idpenjualan', $id);
        // $query = $this->db->get();
		// print_r($this->db->last_query());exit();
		$q="SELECT H.*,GROUP_CONCAT(B.nama) as det_racikan FROM `tpasien_penjualan_racikan` H
			LEFT JOIN tpasien_penjualan_racikan_obat D ON D.idracikan = H.id
			LEFT JOIN view_barang B ON B.id=D.idbarang AND B.idtipe=D.idtipe
			WHERE H.`idpenjualan` = '$id'
			GROUP BY D.idracikan";
		$query=$this->db->query($q);
        return $query->result();
    }
    public function totalharga($id)
    {
        $this->db->select("tpasien_penjualan.totalharga as totalall,SUM(tpasien_penjualan_racikan.totalharga) as total_harga_racikan");
        $this->db->from('tpasien_penjualan');
        $this->db->join('tpasien_penjualan_racikan', 'tpasien_penjualan_racikan,idpenjualan=tpasien_penjualan.id');
        $this->db->where('tpasien_penjualan.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    public function get_edit_list_racikan($id)
    {
        // $this->db->select("tpasien_penjualan_racikan.*,count(tpasien_penjualan_racikan_obat.id) as total_row");
        // $this->db->from('tpasien_penjualan_racikan');
        // $this->db->join('tpasien_penjualan_racikan_obat','tpasien_penjualan_racikan_obat.idracikan=tpasien_penjualan_racikan.id');
        // $this->db->where('tpasien_penjualan_racikan.idpenjualan',$id);
        // $query = $this->db->get();

        // print_r($this->db->last_query());exit();
        $q="SELECT
			`tpasien_penjualan_racikan`.*, SUM(
				CASE WHEN (tpasien_penjualan_racikan_obat.idracikan = tpasien_penjualan_racikan.id) THEN
					1
				ELSE
					0
				END
			) AS total_row
		FROM
			`tpasien_penjualan_racikan`
		INNER JOIN `tpasien_penjualan_racikan_obat` ON `tpasien_penjualan_racikan_obat`.`idracikan` = `tpasien_penjualan_racikan`.`id`
		WHERE
			`tpasien_penjualan_racikan`.`idpenjualan` = '$id'
		GROUP BY tpasien_penjualan_racikan.id
			ORDER BY tpasien_penjualan_racikan.id ASC
			";
        $query=$this->db->query($q);
        // print_r($this->db->last_query());exit();
        return $query->result();
    }
    public function no_urut_akhir($id)
    {
        $this->db->select("MAX(id) as total_row");
        $this->db->from('tpasien_penjualan_racikan');
        $this->db->where('tpasien_penjualan_racikan.idpenjualan', $id);
        $query = $this->db->get();
        // print_r($query->row('total_row'));exit();
        return $query->row('total_row');
    }

    public function delete_non_racikan($id)
    {
        return $this->db->delete('tpasien_penjualan_nonracikan', array('idpenjualan' => $id));
        // print_r($id);exit();
    }
    public function delete_racikan($id)
    {
        $this->db->delete('tpasien_penjualan_racikan', array('idpenjualan' => $id));
    }
    public function list_data_ticket_obat($array_in, $idpenjualan)
    {
        $comma_separated = implode(",", $array_in);
        // $q="SELECT tpasien_penjualan_nonracikan.*,mdata_obat.nama as namaracikan
	  // ,tpasien_penjualan.nama,tpasien_penjualan.nomedrec,tpasien_penjualan.nopenjualan
	  // ,tpasien_penjualan.st_rujukan_non_rujukan,tpasien_penjualan.asalrujukan
	  // ,tpasien_penjualan.umurtahun,tpasien_penjualan.umurbulan,tpasien_penjualan.umurhari
	  // ,tpasien_penjualan.tgl_lahir,msatuan.singkatan as satuan,mdata_obat.idtipe,mfpasien.title,mfpasien.jenis_kelamin
	  // FROM tpasien_penjualan_nonracikan
		// LEFT JOIN mdata_obat ON mdata_obat.id=tpasien_penjualan_nonracikan.idbarang
		// LEFT JOIN msatuan ON mdata_obat.idsatuankecil=msatuan.id
		// LEFT JOIN tpasien_penjualan ON tpasien_penjualan.id=tpasien_penjualan_nonracikan.idpenjualan
		// LEFT JOIN mfpasien ON mfpasien.id=tpasien_penjualan.idpasien
	  // WHERE  idpenjualan='$idpenjualan'  AND tpasien_penjualan_nonracikan.id IN (".$comma_separated.")";

	  $q="
		SELECT 
			CONCAT( COALESCE ( MI.nama, ' ' ), ' ',  COALESCE (RO.dosis,''), ' ', COALESCE (SD.nama,'') ) AS label_obat_syrup,
	CONCAT( COALESCE ( MI.nama, ' ' ), ' ',  COALESCE (SDL.nama,'') ) AS label_obat_lain
		,tpasien_penjualan_nonracikan.*,mdata_obat.nama as namaracikan
		,tpasien_penjualan.nama,tpasien_penjualan.nomedrec,tpasien_penjualan.nopenjualan
		,tpasien_penjualan.st_rujukan_non_rujukan,tpasien_penjualan.asalrujukan
		,tpasien_penjualan.umurtahun,tpasien_penjualan.umurbulan,tpasien_penjualan.umurhari
		,tpasien_penjualan.tgl_lahir,msatuan.singkatan as satuan,mdata_obat.idtipe,mfpasien.title,mfpasien.jenis_kelamin
		FROM tpasien_penjualan_nonracikan
		LEFT JOIN mdata_obat ON mdata_obat.id=tpasien_penjualan_nonracikan.idbarang
		LEFT JOIN msatuan ON mdata_obat.idsatuankecil=msatuan.id
		LEFT JOIN tpasien_penjualan ON tpasien_penjualan.id=tpasien_penjualan_nonracikan.idpenjualan
		LEFT JOIN mfpasien ON mfpasien.id=tpasien_penjualan.idpasien
		LEFT JOIN tpoliklinik_e_resep_obat RO ON RO.id=tpasien_penjualan_nonracikan.assesmen_det_id
		LEFT JOIN minterval MI ON MI.id=RO.`interval`
		LEFT JOIN msatuan SD ON SD.id=mdata_obat.idsatuandosis
		LEFT JOIN msatuan SDL ON SDL.id=mdata_obat.idsatuanlabel
		WHERE  idpenjualan='$idpenjualan'  AND tpasien_penjualan_nonracikan.id IN (".$comma_separated.")";
		// print_r($q);exit;
        $query=$this->db->query($q);
        return $query->result();
    }
    public function list_data_ticket_racikan($array_in, $idpenjualan)
    {
        $comma_separated = implode(",", $array_in);
        $q="SELECT 
		CONCAT(COALESCE(MI.nama,' '),'','','','') as label_obat_syrup
		,CONCAT(COALESCE(MI.nama,' '),'','') as label_obat_lain
		,tpasien_penjualan_racikan.*,tpasien_penjualan.nama,tpasien_penjualan.nomedrec
		,tpasien_penjualan.nopenjualan,tpasien_penjualan.st_rujukan_non_rujukan,mfpasien.title,mfpasien.jenis_kelamin
		,tpasien_penjualan.asalrujukan
		,tpasien_penjualan.umurtahun,tpasien_penjualan.umurbulan,tpasien_penjualan.umurhari
		,tpasien_penjualan.tgl_lahir,'' as satuan,'1' as idtipe
		FROM tpasien_penjualan_racikan
		LEFT JOIN tpasien_penjualan ON tpasien_penjualan.id = tpasien_penjualan_racikan.idpenjualan
		LEFT JOIN mfpasien ON mfpasien.id=tpasien_penjualan.idpasien
		LEFT JOIN tpoliklinik_e_resep_obat_racikan RO ON RO.racikan_id=tpasien_penjualan_racikan.assesmen_det_id
		LEFT JOIN minterval MI ON MI.id=RO.`interval_racikan`
		WHERE  tpasien_penjualan_racikan.idpenjualan='$idpenjualan'  AND tpasien_penjualan_racikan.id IN (".$comma_separated.")";
        // print_r($q);exit();
        $query=$this->db->query($q);
        return $query->result();
    }
}
