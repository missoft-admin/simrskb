<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Msetting_jurnal_pendapatan_ko_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('msetting_jurnal_honor');
        return $query->row_array();
    }
	
	public function list_akun()
    {
        $q="SELECT *FROM makun_nomor  ORDER BY noakun ASC";
        return $this->db->query($q)->result();
    }
	
	
	
}
