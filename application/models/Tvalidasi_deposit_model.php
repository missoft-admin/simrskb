<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tvalidasi_deposit_model extends CI_Model
{
   public function list_bank(){
	   $q="SELECT id,nama from mbank 
				WHERE mbank.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function getHeader($id){
	   $q="SELECT tvalidasi_deposit.*,M.nama as kel_pasien from tvalidasi_deposit 
				LEFT JOIN mpasien_kelompok M ON M.id=tvalidasi_deposit.idkelompokpasien
				WHERE tvalidasi_deposit.`id`='$id'
				";
		$query=$this->db->query($q);
		return $query->row_array();
   }
   public function saveData(){
	   
		$id=($this->input->post('id'));
		// print_r($this->input->post());exit();
		$row_bayar=$this->get_nama_akun($this->input->post('idakun_bayar'));
		$row_pendapatan=$this->get_nama_akun($this->input->post('idakun_pendapatan'));
		$data_header=array(
			'idakun_bayar'=>$this->input->post('idakun_bayar'),
			'noakun_bayar'=>$row_bayar->noakun,
			'namaakun_bayar'=>$row_bayar->namaakun,
			'idakun_pendapatan'=>$this->input->post('idakun_pendapatan'),
			'noakun_pendapatan'=>$row_pendapatan->noakun,
			'namaakun_pendapatan'=>$row_pendapatan->namaakun,
		);
		$this->db->where('id',$id);
		$this->db->update('tvalidasi_deposit',$data_header);
		
		$st_posting=($this->input->post('st_posting'));
		$btn_simpan=($this->input->post('btn_simpan'));
		
		// if ($st_posting=='1'){
			$this->db->where('id',$id);
			$this->db->update('tvalidasi_deposit',array('st_posting'=>0));
		// }
		if ($btn_simpan=='2'){
			$data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $id);
			$result=$this->db->update('tvalidasi_deposit', $data);
		}
		
		return true;
   }
   function get_nama_akun($id){
	   $q="SELECT *from makun_nomor A WHERE A.id='$id'";
	   return $this->db->query($q)->row();
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
