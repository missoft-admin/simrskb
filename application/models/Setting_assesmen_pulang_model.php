<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_assesmen_pulang_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	
	//PENDAFTARAN
	public function get_assesmen_setting(){
		$q="SELECT * FROM setting_assesmen_pulang ";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_label(){
		$q="SELECT * FROM setting_assesmen_pulang_label";
		return $this->db->query($q)->row_array();
	}
	public function get_assesmen_user(){
		$q="SELECT * FROM setting_assesmen_pulang_user H";
		return $this->db->query($q)->row_array();
	}

	function save_assesmen(){
		// $id =1;
		$this->judul_header_ina = $this->input->post('judul_header_ina');
		$this->judul_header_eng = $this->input->post('judul_header_eng');
		$this->judul_footer_ina = $this->input->post('judul_footer_ina');
		$this->judul_footer_eng = $this->input->post('judul_footer_eng');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->st_edit_catatan = $this->input->post('st_edit_catatan');
		$this->lama_edit = $this->input->post('lama_edit');
		$this->orang_edit = $this->input->post('orang_edit');
		$this->st_hapus_catatan = $this->input->post('st_hapus_catatan');
		$this->lama_hapus = $this->input->post('lama_hapus');
		$this->orang_hapus = $this->input->post('orang_hapus');
		$this->st_duplikasi_catatan = $this->input->post('st_duplikasi_catatan');
		$this->lama_duplikasi = $this->input->post('lama_duplikasi');
		$this->orang_duplikasi = $this->input->post('orang_duplikasi');
		
			
		if ($this->db->update('setting_assesmen_pulang', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_label(){
		$this->alamat_rs = $this->input->post('alamat_rs');
		$this->phone_rs = $this->input->post('phone_rs');
		$this->web_rs = $this->input->post('web_rs');
		$this->no_reg_ina = $this->input->post('no_reg_ina');
		$this->no_reg_eng = $this->input->post('no_reg_eng');
		$this->no_rm_ina = $this->input->post('no_rm_ina');
		$this->no_rm_eng = $this->input->post('no_rm_eng');
		$this->nama_pasien_ina = $this->input->post('nama_pasien_ina');
		$this->nama_pasien_eng = $this->input->post('nama_pasien_eng');
		$this->ttl_ina = $this->input->post('ttl_ina');
		$this->ttl_eng = $this->input->post('ttl_eng');
		$this->umur_ina = $this->input->post('umur_ina');
		$this->umur_eng = $this->input->post('umur_eng');
		$this->jk_ina = $this->input->post('jk_ina');
		$this->jk_eng = $this->input->post('jk_eng');
		$this->kondisi_ina = $this->input->post('kondisi_ina');
		$this->kondisi_eng = $this->input->post('kondisi_eng');
		$this->alasan_ina = $this->input->post('alasan_ina');
		$this->alasan_eng = $this->input->post('alasan_eng');
		$this->mobilisasi_ina = $this->input->post('mobilisasi_ina');
		$this->mobilisasi_eng = $this->input->post('mobilisasi_eng');
		$this->mobilisasi_lain_ina = $this->input->post('mobilisasi_lain_ina');
		$this->mobilisasi_lain_eng = $this->input->post('mobilisasi_lain_eng');
		$this->alkes_ina = $this->input->post('alkes_ina');
		$this->alkes_eng = $this->input->post('alkes_eng');
		$this->perawatan_ina = $this->input->post('perawatan_ina');
		$this->perawatan_eng = $this->input->post('perawatan_eng');
		$this->perawatan_lain_ina = $this->input->post('perawatan_lain_ina');
		$this->perawatan_lain_eng = $this->input->post('perawatan_lain_eng');
		$this->keadaan_pulang_ina = $this->input->post('keadaan_pulang_ina');
		$this->keadaan_pulang_eng = $this->input->post('keadaan_pulang_eng');
		$this->tv_ina = $this->input->post('tv_ina');
		$this->tv_eng = $this->input->post('tv_eng');
		$this->tingkat_kesadaran_ina = $this->input->post('tingkat_kesadaran_ina');
		$this->tingkat_kesadaran_eng = $this->input->post('tingkat_kesadaran_eng');
		$this->tekanan_darah_ina = $this->input->post('tekanan_darah_ina');
		$this->tekanan_darah_eng = $this->input->post('tekanan_darah_eng');
		$this->suhu_ina = $this->input->post('suhu_ina');
		$this->suhu_eng = $this->input->post('suhu_eng');
		$this->nadi_ina = $this->input->post('nadi_ina');
		$this->nadi_eng = $this->input->post('nadi_eng');
		$this->nafas_ina = $this->input->post('nafas_ina');
		$this->nafas_eng = $this->input->post('nafas_eng');
		$this->spo2_ina = $this->input->post('spo2_ina');
		$this->spo2_eng = $this->input->post('spo2_eng');
		$this->tb_ina = $this->input->post('tb_ina');
		$this->tb_eng = $this->input->post('tb_eng');
		$this->bb_ina = $this->input->post('bb_ina');
		$this->bb_eng = $this->input->post('bb_eng');
		$this->diagnosa_kep_ina = $this->input->post('diagnosa_kep_ina');
		$this->diagnosa_kep_eng = $this->input->post('diagnosa_kep_eng');
		$this->tindakan_kep_ina = $this->input->post('tindakan_kep_ina');
		$this->tindakan_kep_eng = $this->input->post('tindakan_kep_eng');
		$this->evaluasi_ina = $this->input->post('evaluasi_ina');
		$this->evaluasi_eng = $this->input->post('evaluasi_eng');
		$this->obat_nyeri_ina = $this->input->post('obat_nyeri_ina');
		$this->obat_nyeri_eng = $this->input->post('obat_nyeri_eng');
		$this->efek_ina = $this->input->post('efek_ina');
		$this->efek_eng = $this->input->post('efek_eng');
		$this->bila_nyeri_ina = $this->input->post('bila_nyeri_ina');
		$this->bila_nyeri_eng = $this->input->post('bila_nyeri_eng');
		$this->batasan_ina = $this->input->post('batasan_ina');
		$this->batasan_eng = $this->input->post('batasan_eng');
		$this->edukasi_ina = $this->input->post('edukasi_ina');
		$this->edukasi_eng = $this->input->post('edukasi_eng');
		$this->hasil_ina = $this->input->post('hasil_ina');
		$this->hasil_eng = $this->input->post('hasil_eng');
		$this->rencana_ina = $this->input->post('rencana_ina');
		$this->rencana_eng = $this->input->post('rencana_eng');
		$this->skr_ina = $this->input->post('skr_ina');
		$this->skr_eng = $this->input->post('skr_eng');
		$this->asuransi_ina = $this->input->post('asuransi_ina');
		$this->asuransi_eng = $this->input->post('asuransi_eng');
		$this->diserahkan_ina = $this->input->post('diserahkan_ina');
		$this->diserahkan_eng = $this->input->post('diserahkan_eng');
		$this->diterima_ina = $this->input->post('diterima_ina');
		$this->diterima_eng = $this->input->post('diterima_eng');
		$this->notes_ina = $this->input->post('notes_ina');
		$this->notes_eng = $this->input->post('notes_eng');
		$this->upload_login_logo(true);
		if ($this->db->update('setting_assesmen_pulang_label', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	public function upload_login_logo($update = false)
    {
        if (!file_exists('assets/upload/app_setting')) {
            mkdir('assets/upload/app_setting', 0755, true);
        }
        if (isset($_FILES['logo'])) {
		// print_r('sini');exit;
            if ($_FILES['logo']['name'] != '') {
                $config['upload_path'] = './assets/upload/app_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('logo')) {
                    $image_upload = $this->upload->data();
                    $this->logo = $image_upload['file_name'];
					
                    if ($update == true) {
                        // $this->remove_image_logo(1);
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
                return true;
            }
					// print_r($this->foto);exit;
        } else {
            return true;
        }
		
    }
	function save_paraf(){
		$id =1;
		$this->ket_gambar_paraf_penerima_ina = $this->input->post('ket_gambar_paraf_penerima_ina');
		$this->ket_gambar_paraf_penerima_eng = $this->input->post('ket_gambar_paraf_penerima_eng');
		$this->ket_nama_paraf_penerima_ina = $this->input->post('ket_nama_paraf_penerima_ina');
		$this->ket_nama_paraf_penerima_eng = $this->input->post('ket_nama_paraf_penerima_eng');
		$this->ket_gambar_paraf_menyatakan_ina = $this->input->post('ket_gambar_paraf_menyatakan_ina');
		$this->ket_gambar_paraf_menyatakan_eng = $this->input->post('ket_gambar_paraf_menyatakan_eng');
		$this->ket_nama_paraf_menyatakan_ina = $this->input->post('ket_nama_paraf_menyatakan_ina');
		$this->ket_nama_paraf_menyatakan_eng = $this->input->post('ket_nama_paraf_menyatakan_eng');
		$this->ket_gambar_paraf_saksi_ina = $this->input->post('ket_gambar_paraf_saksi_ina');
		$this->ket_gambar_paraf_saksi_eng = $this->input->post('ket_gambar_paraf_saksi_eng');
		$this->ket_nama_paraf_saksi_ina = $this->input->post('ket_nama_paraf_saksi_ina');
		$this->ket_nama_paraf_saksi_eng = $this->input->post('ket_nama_paraf_saksi_eng');
		$this->ket_gambar_paraf_saksi_rs_ina = $this->input->post('ket_gambar_paraf_saksi_rs_ina');
		$this->ket_gambar_paraf_saksi_rs_eng = $this->input->post('ket_gambar_paraf_saksi_rs_eng');
		$this->ket_nama_paraf_saksi_rs_ina = $this->input->post('ket_nama_paraf_saksi_rs_ina');
		$this->ket_nama_paraf_saksi_rs_eng = $this->input->post('ket_nama_paraf_saksi_rs_eng');

		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		// $this->db->where('id', $id);
			
		if ($this->db->update('setting_assesmen_pulang_keterangan', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	function save_defaultsave_default(){
		$id =1;
		$this->st_spesifik_mpp = $this->input->post('st_spesifik_mpp');
		if (!empty($this->input->post('st_kunci_default_mpp'))){
		$this->st_kunci_default_mpp =($this->input->post('st_spesifik_mpp')=='0'?'0':$this->input->post('st_kunci_default_mpp'));
			
		}else{
			$this->st_kunci_default_mpp =0;
		}
		
		
		$this->edited_by = $this->session->userdata('user_id');
		$this->edited_date = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
			
		if ($this->db->update('msetting_assesmen_pulang_default', $this)) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function list_dokter(){
		$q="SELECT * FROM mdokter M WHERE M.`status`='1'";
		return $this->db->query($q)->result();
	}
	function get_mpoli($id){
		$q="SELECT M.id as idpoli,M.nama as nama_poli FROM mpoliklinik M WHERE M.id='$id'";
		return $this->db->query($q)->row_array();
	}
	function list_poli(){
		$q="SELECT A.idpoli,M.nama FROM app_reservasi_poli A LEFT JOIN mpoliklinik M ON A.idpoli=M.id";
		return $this->db->query($q)->result();
	}
}


