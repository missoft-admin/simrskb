<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Term_radiologi_mri_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

		function get_data_transaksi($id, $asal_rujukan = '', $status = '') {
			$this->db->select('term_radiologi_mri.*,
				(CASE
						WHEN term_radiologi_mri.asal_rujukan = 1 THEN "Poliklinik"
						WHEN term_radiologi_mri.asal_rujukan = 2 THEN "Instalasi Gawat Darurat"
						WHEN term_radiologi_mri.asal_rujukan = 3 THEN "Rawat Inap"
						WHEN term_radiologi_mri.asal_rujukan = 4 THEN "One Day Surgery"
				END) AS tujuan_pendaftaran,
				(CASE
						WHEN term_radiologi_mri.asal_rujukan IN (1, 2) THEN
								tpoliklinik_pendaftaran.tanggaldaftar
						WHEN term_radiologi_mri.asal_rujukan IN (3, 4) THEN
								poliklinik_ranap.tanggaldaftar
				END) AS tanggal_daftar,
				(CASE
						WHEN term_radiologi_mri.asal_rujukan IN (1, 2) THEN
								tpoliklinik_pendaftaran.nopendaftaran
						WHEN term_radiologi_mri.asal_rujukan IN (3, 4) THEN
								poliklinik_ranap.nopendaftaran
				END) AS nomor_registrasi,
				mdokter_perujuk.nama AS dokter_perujuk,
				(CASE
						WHEN term_radiologi_mri.asal_rujukan IN (1, 2) THEN
								mpoliklinik.nama
						WHEN term_radiologi_mri.asal_rujukan IN (3, 4) THEN
								mkelas.nama
				END) AS poliklinik,
				(CASE
						WHEN term_radiologi_mri.asal_rujukan IN (1, 2) THEN
								mpoliklinik.id
						WHEN term_radiologi_mri.asal_rujukan IN (3, 4) THEN
								mkelas.id
				END) AS poliklinik_kelas,
				(CASE
						WHEN term_radiologi_mri.asal_rujukan IN (1, 2) THEN
								tpoliklinik_pendaftaran.nik
						WHEN term_radiologi_mri.asal_rujukan IN (3, 4) THEN
								poliklinik_ranap.nik
				END) AS nomor_ktp,
				COALESCE ( referensi_jenis_kelamin.ref, poliklinik_ranap.jenis_kelamin_label) AS jenis_kelamin_label,
				COALESCE ( mfpasien.no_medrec, poliklinik_ranap.nomor_medrec) AS nomedrec,
				COALESCE ( mfpasien.nama, poliklinik_ranap.nama_pasien) AS nama_pasien,
				COALESCE ( mfpasien.title, poliklinik_ranap.title_pasien) AS title,
				COALESCE ( mfpasien.jenis_kelamin, poliklinik_ranap.jenis_kelamin) AS jenis_kelamin,
				COALESCE ( mfpasien.alamat_jalan, poliklinik_ranap.alamat_pasien) AS alamat_jalan,
				COALESCE ( mfpasien.tanggal_lahir, poliklinik_ranap.tanggal_lahir) AS tanggal_lahir,
				COALESCE ( mfpasien.umur_tahun, poliklinik_ranap.umur_tahun) AS umur_tahun,
				COALESCE ( mfpasien.umur_bulan, poliklinik_ranap.umur_bulan) AS umur_bulan,
				COALESCE ( mfpasien.umur_hari, poliklinik_ranap.umur_hari) AS umur_hari,
				COALESCE ( mfpasien.telepon, poliklinik_ranap.telepon) AS telepon,
				COALESCE ( mpasien_kelompok.nama, poliklinik_ranap.kelompok_pasien) AS kelompok_pasien,
				COALESCE ( mrekanan.nama, poliklinik_ranap.nama_asuransi) AS nama_asuransi,
				mpekerjaan.nama AS pekerjaan,
				mdokter_peminta.id AS dokter_peminta_id,
				mdokter_peminta.nama AS dokter_peminta,
				mdokter_radiologi.id AS dokter_radiologi_id,
				mdokter_radiologi.nama AS dokter_radiologi,
				merm_pengaturan_tujuan_radiologi.nama AS tujuan_radiologi_nama,
				referensi_prioritas.ref AS prioritas_label,
				referensi_pemeriksaan.ref AS status_pemeriksaan_label,
				mppa.nama AS created_ppa,
				mppa.nip AS nip_ppa,
				COALESCE(poliklinik_ranap.idkelas, 0) AS idkelas');
			$this->db->join('(SELECT
				trawatinap_pendaftaran.id,
				trawatinap_pendaftaran.tanggaldaftar,
				trawatinap_pendaftaran.nopendaftaran,
				tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
				tpoliklinik_pendaftaran.namapasien AS nama_pasien,
				tpoliklinik_pendaftaran.title AS title_pasien,
				tpoliklinik_pendaftaran.jenis_kelamin AS jenis_kelamin,
				tpoliklinik_pendaftaran.alamatpasien AS alamat_pasien,
				tpoliklinik_pendaftaran.tanggal_lahir,
				tpoliklinik_pendaftaran.umurhari AS umur_hari,
				tpoliklinik_pendaftaran.umurbulan AS umur_bulan,
				tpoliklinik_pendaftaran.umurtahun AS umur_tahun,
				tpoliklinik_pendaftaran.telepon,
				tpoliklinik_pendaftaran.nik,
				mpasien_kelompok.nama AS kelompok_pasien,
				trawatinap_pendaftaran.idkelas,
				mrekanan.nama AS nama_asuransi,
				referensi_jenis_kelamin.ref AS jenis_kelamin_label
			FROM
				trawatinap_pendaftaran
			INNER JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
			LEFT JOIN mrekanan ON mrekanan.id = trawatinap_pendaftaran.idrekanan
			LEFT JOIN merm_referensi referensi_jenis_kelamin ON referensi_jenis_kelamin.nilai = tpoliklinik_pendaftaran.jenis_kelamin AND referensi_jenis_kelamin.ref_head_id = 1
			INNER JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien) AS poliklinik_ranap', 'term_radiologi_mri.pendaftaran_id = poliklinik_ranap.id AND term_radiologi_mri.asal_rujukan IN (3, 4)', 'LEFT');
			$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = term_radiologi_mri.pendaftaran_id AND term_radiologi_mri.asal_rujukan IN (1, 2)', 'LEFT');
			$this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
			$this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
			$this->db->join('mdokter mdokter_perujuk', 'mdokter_perujuk.id = term_radiologi_mri.dokter_perujuk_id', 'LEFT');
			$this->db->join('mdokter mdokter_peminta', 'mdokter_peminta.id = term_radiologi_mri.dokter_peminta_id', 'LEFT');
			$this->db->join('mdokter mdokter_radiologi', 'mdokter_radiologi.id = term_radiologi_mri.dokter_radiologi', 'LEFT');
			$this->db->join('merm_pengaturan_tujuan_radiologi', 'merm_pengaturan_tujuan_radiologi.id = term_radiologi_mri.tujuan_radiologi', 'LEFT');
			$this->db->join('merm_referensi referensi_prioritas', 'referensi_prioritas.ref_head_id = 85 AND referensi_prioritas.nilai = term_radiologi_mri.prioritas', 'LEFT');
			$this->db->join('merm_referensi referensi_pemeriksaan', 'referensi_pemeriksaan.ref_head_id = 89 AND referensi_pemeriksaan.nilai = term_radiologi_mri.status_pemeriksaan', 'LEFT');
			$this->db->join('merm_referensi referensi_jenis_kelamin', 'referensi_jenis_kelamin.nilai = tpoliklinik_pendaftaran.jenis_kelamin AND referensi_jenis_kelamin.ref_head_id = 1', 'LEFT');
			$this->db->join('mppa', 'mppa.id = term_radiologi_mri.created_ppa', 'LEFT');
			$this->db->join('mpekerjaan', 'mpekerjaan.id = mfpasien.pekerjaan_id', 'LEFT');
			$this->db->join('mkelas', 'mkelas.id = poliklinik_ranap.idkelas', 'LEFT');
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
			$this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
			
			if ($status == 'draft') {
				if ($asal_rujukan == 'rawat_inap') {
					$this->db->where_in('term_radiologi_mri.asal_rujukan', [3, 4]);
				} else {
					$this->db->where_in('term_radiologi_mri.asal_rujukan', [1, 2]);
				}
				$this->db->where('term_radiologi_mri.pendaftaran_id', $id);
				$this->db->where('term_radiologi_mri.status_pemeriksaan', 1);
				$this->db->order_by('term_radiologi_mri.id', 'DESC');
				$this->db->limit(1);
			} else {
				$this->db->where('term_radiologi_mri.id', $id);
			}

			$query = $this->db->get('term_radiologi_mri');
			return $query->row();
	}

	public function getListPemeriksaan($transaksi_id)
	{
		$this->db->select('mtarif_radiologi.idkelompok, mtarif_radiologi.path,
				mtarif_radiologi.level, 
				(CASE
					WHEN mtarif_radiologi.idtipe = 1 AND mtarif_radiologi.idkelompok = 0 THEN
							CONCAT(CONCAT(merm_pemeriksaan_radiologi.nama_pemeriksaan, " ", COALESCE(posisi_pemeriksaan.ref, "")), " (", mtarif_radiologi_expose.nama, mtarif_radiologi_film.nama, ")")
					ELSE mtarif_radiologi.nama
				END) AS nama_pemeriksaan,
				term_radiologi_mri_pemeriksaan.*');
		$this->db->join('mtarif_radiologi', 'term_radiologi_mri_pemeriksaan.idradiologi = mtarif_radiologi.id', 'left');
		$this->db->join('merm_pemeriksaan_radiologi', 'merm_pemeriksaan_radiologi.tarif_radiologi_id = mtarif_radiologi.id', 'left');
		$this->db->join('mtarif_radiologi_expose', 'mtarif_radiologi_expose.id = mtarif_radiologi.idexpose', 'left');
		$this->db->join('mtarif_radiologi_film', 'mtarif_radiologi_film.id = mtarif_radiologi.idfilm', 'left');
		$this->db->join('merm_referensi posisi_pemeriksaan', 'posisi_pemeriksaan.ref_head_id = 247 AND posisi_pemeriksaan.nilai = merm_pemeriksaan_radiologi.posisi_pemeriksaan', 'left');
		$this->db->where('term_radiologi_mri_pemeriksaan.status', 1);
		$this->db->where('term_radiologi_mri_pemeriksaan.transaksi_id', $transaksi_id);
		$this->db->group_by('term_radiologi_mri_pemeriksaan.idpemeriksaan');
		$this->db->order_by('mtarif_radiologi.path', 'ASC');
		$this->db->from('term_radiologi_mri_pemeriksaan');
		$query = $this->db->get();
		return $query->result();
	}

	public function getSelectedPemeriksaan($transaksiId) {
			$this->db->select('term_radiologi_mri_pemeriksaan.id,
				term_radiologi_mri_pemeriksaan.iddokter,
				term_radiologi_mri_pemeriksaan.idtipe AS tipe_id,
				term_radiologi_mri_pemeriksaan.idpemeriksaan AS pemeriksaan_id,
				term_radiologi_mri_pemeriksaan.idradiologi AS radiologi_id,
				term_radiologi_mri_pemeriksaan.kelas,
				term_radiologi_mri_pemeriksaan.namatarif AS nama_tarif,
				term_radiologi_mri_pemeriksaan.namapemeriksaan AS nama_pemeriksaan,
				term_radiologi_mri_pemeriksaan.namapemeriksaan_detail AS nama_pemeriksaan_detail,
				mtarif_radiologi.path,
				term_radiologi_mri_pemeriksaan.jasasarana AS jasa_sarana,
				term_radiologi_mri_pemeriksaan.jasapelayanan AS jasa_pelayanan,
				term_radiologi_mri_pemeriksaan.bhp,
				term_radiologi_mri_pemeriksaan.biayaperawatan AS biaya_perawatan,
				term_radiologi_mri_pemeriksaan.total,
				term_radiologi_mri_pemeriksaan.kuantitas,
				term_radiologi_mri_pemeriksaan.diskon,
				term_radiologi_mri_pemeriksaan.totalkeseluruhan AS total_keseluruhan,
				term_radiologi_mri_pemeriksaan.statusverifikasi AS status_verifikasi,
				IF(term_radiologi_mri_pemeriksaan.status, 0, 1) AS status_delete,
				"1" AS status_database');
			$this->db->join('mtarif_radiologi', 'mtarif_radiologi.id = term_radiologi_mri_pemeriksaan.idradiologi');
			$query = $this->db->get_where('term_radiologi_mri_pemeriksaan', array('term_radiologi_mri_pemeriksaan.transaksi_id' => $transaksiId, 'term_radiologi_mri_pemeriksaan.status' => 1));

			return $query->result();
	}

	function logicTujuanRadiologi($tipe_layanan, $asal_pasien, $poliklinik, $dokter) {
			$this->db->select('merm_pengaturan_tujuan_radiologi.id,
				merm_pengaturan_tujuan_radiologi.nama');
			$this->db->from('merm_pengaturan_tujuan_radiologi_unit_akses');
			$this->db->join('merm_pengaturan_tujuan_radiologi', 'merm_pengaturan_tujuan_radiologi_unit_akses.idpengaturan = merm_pengaturan_tujuan_radiologi.id');
			$this->db->where('merm_pengaturan_tujuan_radiologi_unit_akses.tipe_layanan', $tipe_layanan);
			$this->db->where("(merm_pengaturan_tujuan_radiologi_unit_akses.asal_pasien = '0' OR merm_pengaturan_tujuan_radiologi_unit_akses.asal_pasien = '$asal_pasien')");
			$this->db->where("(merm_pengaturan_tujuan_radiologi_unit_akses.idpoliklinik = '0' OR merm_pengaturan_tujuan_radiologi_unit_akses.idpoliklinik = '$poliklinik')");
			$this->db->where("(merm_pengaturan_tujuan_radiologi_unit_akses.iddokter = '0' OR merm_pengaturan_tujuan_radiologi_unit_akses.iddokter = '$dokter')");
			$this->db->order_by('merm_pengaturan_tujuan_radiologi.id', 'ASC');

			$query = $this->db->get();
			return $query->result();
	}

	public function insertPemeriksaan()
	{
			$transaksi_id = $this->input->post('transaksi_id');

			// Delete records from term_radiologi_mri_pemeriksaan table
			$this->deleteTermRadiologiPemeriksaan($transaksi_id);
			
			if ($this->input->post('status_form') == 'edit_permintaan') {
				// Clone data to history tables
				$this->cloneToHistory($transaksi_id);

				// Update term_radiologi table
				$this->updateTermRadiologi($transaksi_id);
			} else {
				$data = [
						'pendaftaran_id' => $this->input->post('pendaftaran_id'),
						'pasien_id' => $this->input->post('pasien_id'),
						'jenis_pemeriksaan' => $this->input->post('jenis_pemeriksaan'),
						'rencana_pemeriksaan' => YMDFormat($this->input->post('rencana_pemeriksaan')),
						'tujuan_radiologi' => $this->input->post('tujuan_radiologi'),
						'dokter_perujuk_id' => $this->input->post('dokter_perujuk_id'),
						'dokter_peminta_id' => $this->input->post('dokter_peminta_id'),
						'diagnosa' => $this->input->post('diagnosa'),
						'catatan_permintaan' => $this->input->post('catatan_permintaan'),
						'waktu_permintaan' => YMDTimeFormat($this->input->post('tanggal_permintaan') . ' ' . $this->input->post('waktu_permintaan')),
						'prioritas' => $this->input->post('prioritas'),
						'pasien_puasa' => $this->input->post('pasien_puasa'),
						'pengiriman_hasil' => $this->input->post('pengiriman_hasil'),
						'alergi_bahan_kontras' => $this->input->post('alergi_bahan_kontras'),
						'pasien_hamil' => $this->input->post('pasien_hamil'),
						'status_pemeriksaan' => 2, // Menunggu Pemeriksaan
						'created_ppa' => $this->input->post('ppa_id'),
						'created_at' => date("Y-m-d H:i:s"),
						'petugas_pemeriksaan' => $this->input->post('ppa_id'),
						'waktu_pemeriksaan' => date("Y-m-d H:i:s"),
				];

				if ($this->input->post('ref_asal_rujukan')) {
					$data['asal_rujukan'] = $this->input->post('ref_asal_rujukan');
				}
				
				// Update data into term_radiologi table
				$this->db->where('id', $transaksi_id);
				$this->db->update('term_radiologi_mri', $data);
			}
			
			$data_pemeriksaan = json_decode($_POST['data_pemeriksaan']);
			foreach ($data_pemeriksaan as $pemeriksaan) {
					$tindakan = [];
					$tindakan['transaksi_id'] = $transaksi_id;
					$tindakan['idtipe'] = $pemeriksaan->tipe_id;
					$tindakan['idpemeriksaan'] = $pemeriksaan->pemeriksaan_id;
					$tindakan['idradiologi'] = $pemeriksaan->radiologi_id;
					$tindakan['kelas'] = $pemeriksaan->kelas;
					$tindakan['namatarif'] = $pemeriksaan->nama_tarif;
					$tindakan['namapemeriksaan'] = $pemeriksaan->nama_pemeriksaan;
					$tindakan['namapemeriksaan_detail'] = $pemeriksaan->nama_pemeriksaan_detail;
					$tindakan['jasasarana'] = RemoveComma($pemeriksaan->jasa_sarana);
					$tindakan['jasapelayanan'] = RemoveComma($pemeriksaan->jasa_pelayanan);
					$tindakan['bhp'] = RemoveComma($pemeriksaan->bhp);
					$tindakan['biayaperawatan'] = RemoveComma($pemeriksaan->biaya_perawatan);
					$tindakan['total'] = RemoveComma($pemeriksaan->total);
					$tindakan['kuantitas'] = RemoveComma($pemeriksaan->kuantitas);
					$tindakan['diskon'] = RemoveComma($pemeriksaan->diskon);
					$tindakan['totalkeseluruhan'] = RemoveComma($pemeriksaan->total_keseluruhan);
					$tindakan['statusverifikasi'] = $pemeriksaan->status_verifikasi;

					if ($this->db->insert('term_radiologi_mri_pemeriksaan', $tindakan)) {
							// $this->updateNominalJasaMedis($_POST['id'], $_POST['statuspasien'], $_POST['idkelas']);
					}
			}

			return true;
	}

	public function updatePemeriksaan()
	{
			$transaksi_id = $this->input->post('transaksi_id');

			$data = [
					'pendaftaran_id' => $this->input->post('pendaftaran_id'),
					'pasien_id' => $this->input->post('pasien_id'),
					'jenis_pemeriksaan' => $this->input->post('jenis_pemeriksaan'),
					'rencana_pemeriksaan' => YMDFormat($this->input->post('rencana_pemeriksaan')),
					'tujuan_radiologi' => $this->input->post('tujuan_radiologi'),
					'dokter_perujuk_id' => $this->input->post('dokter_perujuk_id'),
					'dokter_peminta_id' => $this->input->post('dokter_peminta_id'),
					'diagnosa' => $this->input->post('diagnosa'),
					'catatan_permintaan' => $this->input->post('catatan_permintaan'),
					'waktu_permintaan' => YMDTimeFormat($this->input->post('tanggal_permintaan') . ' ' . $this->input->post('waktu_permintaan')),
					'prioritas' => $this->input->post('prioritas'),
					'pasien_puasa' => $this->input->post('pasien_puasa'),
					'pengiriman_hasil' => $this->input->post('pengiriman_hasil'),
					'alergi_bahan_kontras' => $this->input->post('alergi_bahan_kontras'),
					'pasien_hamil' => $this->input->post('pasien_hamil'),
					'waktu_pemeriksaan' => YMDTimeFormat($this->input->post('tanggal_pemeriksaan') . ' ' . $this->input->post('waktu_pemeriksaan')),
					'petugas_pemeriksaan' => $this->input->post('petugas_pemeriksaan'),
					'nomor_foto' => $this->input->post('nomor_foto'),
					'dokter_radiologi' => $this->input->post('dokter_radiologi'),
					'jumlah_expose' => $this->input->post('jumlah_expose'),
					'jumlah_film' => $this->input->post('jumlah_film'),
					'qp' => $this->input->post('qp'),
					'mas' => $this->input->post('mas'),
					'posisi' => $this->input->post('posisi'),
					'edited_ppa' => $this->input->post('ppa_id'),
					'edited_at' => date("Y-m-d H:i:s"),
			];

			$formSubmit = $this->input->post('form_submit');
			if ($formSubmit == 'form-submit-process') {
				$data['status_pemeriksaan'] = 4;
				$data['status_panggil'] = 3;
			}

			// Update data into term_radiologi table
			$this->db->where('id', $transaksi_id);
			$this->db->update('term_radiologi_mri', $data);

			// Delete records from term_radiologi_mri_pemeriksaan table
			$this->deleteTermRadiologiPemeriksaan($transaksi_id);
			
			// Get dokter information
			$dokter = $this->getDokterInfo($transaksi_id);
			
			$data_pemeriksaan = json_decode($_POST['data_pemeriksaan']);

			// Fungsi untuk menyaring data yang memiliki status_delete = 0
			$filtered_data_pemeriksaan = array_filter($data_pemeriksaan, function($pemeriksaan) {
					return isset($pemeriksaan->status_delete) && $pemeriksaan->status_delete == 0;
			});

			$this->db->where('transaksi_id', $transaksi_id);
			$this->db->delete('term_radiologi_mri_pemeriksaan');

			foreach ($filtered_data_pemeriksaan as $pemeriksaan) {
					$tindakan = [];
					$tindakan['transaksi_id'] = $transaksi_id;
					$tindakan['idtipe'] = $pemeriksaan->tipe_id;
					$tindakan['idpemeriksaan'] = $pemeriksaan->pemeriksaan_id;
					$tindakan['idradiologi'] = $pemeriksaan->radiologi_id;
					$tindakan['kelas'] = $pemeriksaan->kelas;
					$tindakan['namatarif'] = $pemeriksaan->nama_tarif;
					$tindakan['namapemeriksaan'] = $pemeriksaan->nama_pemeriksaan;
					$tindakan['namapemeriksaan_detail'] = $pemeriksaan->nama_pemeriksaan_detail;
					$tindakan['jasasarana'] = RemoveComma($pemeriksaan->jasa_sarana);
					$tindakan['jasapelayanan'] = RemoveComma($pemeriksaan->jasa_pelayanan);
					$tindakan['bhp'] = RemoveComma($pemeriksaan->bhp);
					$tindakan['biayaperawatan'] = RemoveComma($pemeriksaan->biaya_perawatan);
					$tindakan['total'] = RemoveComma($pemeriksaan->total);
					$tindakan['kuantitas'] = RemoveComma($pemeriksaan->kuantitas);
					$tindakan['diskon'] = RemoveComma($pemeriksaan->diskon);
					$tindakan['totalkeseluruhan'] = RemoveComma($pemeriksaan->total_keseluruhan);
					$tindakan['statusverifikasi'] = $pemeriksaan->status_verifikasi;

					// Nominal Komponen Honor Dokter
					$tindakan['iddokter'] = $_POST['dokter_radiologi'];
					$tindakan['potongan_rs'] = $dokter->potongan_rs;
					$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

					if ($this->db->insert('term_radiologi_mri_pemeriksaan', $tindakan)) {
							// $this->updateNominalJasaMedis($_POST['id'], $_POST['statuspasien'], $_POST['idkelas']);
					}
			}

			return true;
	}

	private function cloneToHistory($transaksiId) {
			// Clone data from term_radiologi to history tables
			$this->db->where('id', $transaksiId);
			$data = $this->db->get('term_radiologi_mri')->row_array();

			if (!empty($data)) {
					// Set version information
					$version = $this->getVersionInfo($transaksiId);
					$data['id'] = ''; // Assuming 'id' is an auto-incremented primary key
					$data['versi_edit'] = $version;

					// Insert data into term_radiologi_mri_history
					$this->db->insert('term_radiologi_mri_history', $data);

					// Clone related data from term_radiologi_mri_pemeriksaan to history table
					$this->db->where('transaksi_id', $transaksiId);
					$pemeriksaanData = $this->db->get('term_radiologi_mri_pemeriksaan')->result_array();

					foreach ($pemeriksaanData as $row) {
							$row['id'] = ''; // Assuming 'id' is an auto-incremented primary key
							$row['versi_edit'] = $version;
							$this->db->on_duplicate('term_radiologi_mri_pemeriksaan_history', $row);
					}
			}
	}

    private function updateTermRadiologi($transaksiId) {
        $data = [
            'jumlah_edit' => $this->getVersionInfo($transaksiId) + 1,
						'pendaftaran_id' => $this->input->post('pendaftaran_id'),
						'pasien_id' => $this->input->post('pasien_id'),
						'jenis_pemeriksaan' => $this->input->post('jenis_pemeriksaan'),
						'rencana_pemeriksaan' => YMDFormat($this->input->post('rencana_pemeriksaan')),
						'tujuan_radiologi' => $this->input->post('tujuan_radiologi'),
						'dokter_perujuk_id' => $this->input->post('dokter_perujuk_id'),
						'dokter_peminta_id' => $this->input->post('dokter_peminta_id'),
						'diagnosa' => $this->input->post('diagnosa'),
						'catatan_permintaan' => $this->input->post('catatan_permintaan'),
						'waktu_permintaan' => YMDTimeFormat($this->input->post('tanggal_permintaan') . ' ' . $this->input->post('waktu_permintaan')),
						'prioritas' => $this->input->post('prioritas'),
						'pasien_puasa' => $this->input->post('pasien_puasa'),
						'pengiriman_hasil' => $this->input->post('pengiriman_hasil'),
						'alergi_bahan_kontras' => $this->input->post('alergi_bahan_kontras'),
						'pasien_hamil' => $this->input->post('pasien_hamil'),
						'status_pemeriksaan' => 2, // Menunggu Pemeriksaan
						'edited_ppa' => $this->input->post('ppa_id'),
						'edited_at' => date("Y-m-d H:i:s"),
        ];

        $this->db->where('id', $transaksiId);
        $this->db->update('term_radiologi_mri', $data);
    }

    private function deleteTermRadiologiPemeriksaan($transaksiId) {
        $this->db->where('transaksi_id', $transaksiId);
        $this->db->delete('term_radiologi_mri_pemeriksaan');
    }

		private function getVersionInfo($transaksiId) {
				$this->db->select('jumlah_edit');
				$this->db->where('id', $transaksiId);
				$result = $this->db->get('term_radiologi_mri')->row();

				return (!empty($result)) ? $result->jumlah_edit : 0;
		}

		public function splitOrder($transaksi_id)
		{
				$originalHeader = $this->db->get_where('term_radiologi_mri', ['id' => $transaksi_id])->row_array();
				unset($originalHeader['id']);

				// // Panggil stored procedure GetNoRadiologi untuk mendapatkan nomor_radiologi baru
				// $query = $this->db->query("SELECT GetNoRadiologi() AS nomor_radiologi");
				// $result = $query->row_array();
				// $query->next_result();
				// $query->free_result();

				// $originalHeader['nomor_radiologi'] = $result['nomor_radiologi'];

				// Insert data baru ke tabel term_radiologi dan dapatkan id baru
				$this->db->insert('term_radiologi_mri', $originalHeader);
				$newHeaderId = $this->db->insert_id();

				return $newHeaderId;
		}

		public function updatePemeriksaanWithSplit($transaksiId, $newTransaksiId, $selectedOrderIds)
		{
				foreach ($selectedOrderIds as $orderId) {
						// Get dokter information
						$dokter = $this->getDokterInfo($newTransaksiId);
						$pemeriksaan = get_by_field('id', $orderId, 'term_radiologi_mri_pemeriksaan');

						$tindakan = [];
						$tindakan['transaksi_id'] = $newTransaksiId;
						$tindakan['idtipe'] = $pemeriksaan->idtipe;
						$tindakan['idpemeriksaan'] = $pemeriksaan->idpemeriksaan;
						$tindakan['idradiologi'] = $pemeriksaan->idradiologi;
						$tindakan['kelas'] = $pemeriksaan->kelas;
						$tindakan['namatarif'] = $pemeriksaan->namatarif;
						$tindakan['namapemeriksaan'] = $pemeriksaan->namapemeriksaan;
						$tindakan['namapemeriksaan_detail'] = $pemeriksaan->namapemeriksaan_detail;
						$tindakan['jasasarana'] = RemoveComma($pemeriksaan->jasasarana);
						$tindakan['jasapelayanan'] = RemoveComma($pemeriksaan->jasapelayanan);
						$tindakan['bhp'] = RemoveComma($pemeriksaan->bhp);
						$tindakan['biayaperawatan'] = RemoveComma($pemeriksaan->biayaperawatan);
						$tindakan['total'] = RemoveComma($pemeriksaan->total);
						$tindakan['kuantitas'] = RemoveComma($pemeriksaan->kuantitas);
						$tindakan['diskon'] = RemoveComma($pemeriksaan->diskon);
						$tindakan['totalkeseluruhan'] = RemoveComma($pemeriksaan->totalkeseluruhan);
						$tindakan['statusverifikasi'] = $pemeriksaan->statusverifikasi;
						$tindakan['status'] = $pemeriksaan->status;

						// Nominal Komponen Honor Dokter
						$tindakan['iddokter'] = $_POST['dokter_radiologi'];
						$tindakan['potongan_rs'] = $dokter->potongan_rs;
						$tindakan['pajak_dokter'] = $dokter->pajak_dokter;

						if ($this->db->insert('term_radiologi_mri_pemeriksaan', $tindakan)) {
								// $this->updateNominalJasaMedis($_POST['id'], $_POST['statuspasien'], $_POST['idkelas']);
						}
						
						$this->db->where('id', $orderId);
						$this->db->where('transaksi_id', $transaksiId);
						$this->db->delete('term_radiologi_mri_pemeriksaan');
				}
		}

		private function getDokterInfo($transaksi_id)
		{
				$query = $this->db->query("SELECT
							mdokter.id,
							(CASE
									WHEN DATE_FORMAT(term_radiologi_mri.rencana_pemeriksaan, '%H:%i') < '12:00' THEN
											mdokter.potonganrspagi
									ELSE mdokter.potonganrssiang
							END) AS potongan_rs,
							mdokter.pajak AS pajak_dokter
					FROM term_radiologi_mri
					JOIN mdokter ON mdokter.id = term_radiologi_mri.dokter_radiologi
					WHERE term_radiologi_mri.id = " . $transaksi_id);
				
				if ($query->num_rows() > 0) {
							return $query->row();
				} else {
					$default_result = (object) [
							'id' => 0,
							'potongan_rs' => 0,
							'pajak_dokter' => 0,
					];

					return $default_result;
				}
		}

		public function getPetugasProsesPemeriksaanByTujuanRadiologi($tujuan_radiologi) {
        $this->db->select('merm_pengaturan_tujuan_radiologi_user_akses.iduser,
					mppa.nama AS nama_petugas');
        $this->db->from('merm_pengaturan_tujuan_radiologi_user_akses');
        $this->db->join('mppa', 'mppa.id = merm_pengaturan_tujuan_radiologi_user_akses.iduser');
        $this->db->where('merm_pengaturan_tujuan_radiologi_user_akses.idpengaturan', $tujuan_radiologi);
        
        return $this->db->get()->result();
    }

		public function getPetugasPenginputanFotoByTujuanRadiologi($tujuan_radiologi) {
        $this->db->select('merm_pengaturan_tujuan_radiologi_user_akses.iduser,
					mppa.nama AS nama_petugas');
        $this->db->from('merm_pengaturan_tujuan_radiologi_user_akses');
        $this->db->join('mppa', 'mppa.id = merm_pengaturan_tujuan_radiologi_user_akses.iduser');
        $this->db->where('merm_pengaturan_tujuan_radiologi_user_akses.idpengaturan', $tujuan_radiologi);
        
        return $this->db->get()->result();
    }

		public function getPetugasPenginputanExpertiseByTujuanRadiologi($tujuan_radiologi) {
        $this->db->select('merm_pengaturan_tujuan_radiologi_user_akses.iduser,
					mppa.nama AS nama_petugas');
        $this->db->from('merm_pengaturan_tujuan_radiologi_user_akses');
        $this->db->join('mppa', 'mppa.id = merm_pengaturan_tujuan_radiologi_user_akses.iduser');
        $this->db->where('merm_pengaturan_tujuan_radiologi_user_akses.idpengaturan', $tujuan_radiologi);
        
        return $this->db->get()->result();
    }

		public function getPetugasPengirimanHasilByTujuanRadiologi($tujuan_radiologi) {
        $this->db->select('merm_pengaturan_tujuan_radiologi_user_akses.iduser,
					mppa.nama AS nama_petugas');
        $this->db->from('merm_pengaturan_tujuan_radiologi_user_akses');
        $this->db->join('mppa', 'mppa.id = merm_pengaturan_tujuan_radiologi_user_akses.iduser');
        $this->db->where('merm_pengaturan_tujuan_radiologi_user_akses.idpengaturan', $tujuan_radiologi);
        
        return $this->db->get()->result();
    }

		public function getPetugasPenerimaanHasilByTujuanRadiologi($tujuan_radiologi) {
        $this->db->select('merm_pengaturan_tujuan_radiologi_user_akses.iduser,
					mppa.nama AS nama_petugas');
        $this->db->from('merm_pengaturan_tujuan_radiologi_user_akses');
        $this->db->join('mppa', 'mppa.id = merm_pengaturan_tujuan_radiologi_user_akses.iduser');
        $this->db->where('merm_pengaturan_tujuan_radiologi_user_akses.idpengaturan', $tujuan_radiologi);
        
        return $this->db->get()->result();
    }

		public function getDokterByTujuanRadiologi($tujuan_radiologi) {
        $this->db->select('merm_pengaturan_tujuan_radiologi_dokter.iddokter,
					mdokter.nama AS nama_dokter,
					merm_pengaturan_tujuan_radiologi_dokter.status_default');
        $this->db->from('merm_pengaturan_tujuan_radiologi_dokter');
        $this->db->join('mdokter', 'mdokter.id = merm_pengaturan_tujuan_radiologi_dokter.iddokter');
        $this->db->where('merm_pengaturan_tujuan_radiologi_dokter.idpengaturan', $tujuan_radiologi);
        
        return $this->db->get()->result();
    }

		public function getPemeriksaanData($transaksi_id) {
        $this->db->select('namapemeriksaan AS nama_pemeriksaan,
					total AS harga, kuantitas, diskon, totalkeseluruhan AS total_keseluruhan');
        $this->db->where('transaksi_id', $transaksi_id);
        $query = $this->db->get('term_radiologi_mri_pemeriksaan');
        
        return $query->result_array();
    }

		public function getIdTindakanByPendaftaran($idpendaftaran) {
        $this->db->select('id');
        $this->db->where('idpendaftaran', $idpendaftaran);
        $query = $this->db->get('tpoliklinik_tindakan');

        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        }

        return false;
    }

		public function getIdRujukanByIdTindakan($idtindakan) {
        $this->db->select('id');
        $this->db->where('idtindakan', $idtindakan);
        $query = $this->db->get('trujukan_radiologi');

        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        }

        return false;
    }

		public function getDataToClone($transaksi_id) {
        $this->db->where('transaksi_id', $transaksi_id);
        $query = $this->db->get('term_radiologi_mri_pemeriksaan');

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return false;
    }

		public function getTarifByKelas($transaksi_id, $kelas_id) {
			$query = $this->db->select('SUM(mtarif_radiologi_detail.total * term_radiologi_mri_pemeriksaan.kuantitas) AS total')
            ->from('mtarif_radiologi_detail')
            ->join('term_radiologi_mri_pemeriksaan', 'mtarif_radiologi_detail.idtarif = term_radiologi_mri_pemeriksaan.idradiologi', 'left')
            ->where('term_radiologi_mri_pemeriksaan.transaksi_id', $transaksi_id)
            ->where('mtarif_radiologi_detail.kelas', $kelas_id)
            ->get();

        return $query->row();
		}
		
		public function getHasilPemeriksaanTerakhir($transaksi_id) {
				$this->db->select('MAX(id) AS hasil_pemeriksaan_id');
				$this->db->from('term_radiologi_mri_hasil_pemeriksaan');
				$this->db->where('transaksi_id', $transaksi_id);
				$this->db->group_by('transaksi_id');

				$query = $this->db->get();

        if ($query->num_rows() > 0) {
						return $query->row()->hasil_pemeriksaan_id;
				} else {
						return null;
				}
		}

		public function getDataPasien($pasien_id)
    {
        $this->db->select('mfpasien.*, referensi_jenis_kelamin.ref AS jenis_kelamin');
        $this->db->join('merm_referensi referensi_jenis_kelamin', 'referensi_jenis_kelamin.nilai = mfpasien.jenis_kelamin AND referensi_jenis_kelamin.ref_head_id = 1', 'LEFT');
        $this->db->where('mfpasien.id', $pasien_id);

        $query = $this->db->get('mfpasien');
        return $query->row();
    }

		public function getPemeriksaanGroupLabel($transaksi_id)
		{
			$this->db->select('mtarif_radiologi.idtipe');
			$this->db->join('mtarif_radiologi', 'mtarif_radiologi.id = term_radiologi_mri_pemeriksaan.idradiologi');
			$this->db->group_by('mtarif_radiologi.idtipe');
			$this->db->where('term_radiologi_mri_pemeriksaan.transaksi_id', $transaksi_id);
			$query = $this->db->get('term_radiologi_mri_pemeriksaan');
			return $query->result();
		}

		public function getPemeriksaanGroupLabelSelected($transaksi_id, $array_in)
		{
			$array = implode("','", $array_in);

			$query = $this->db->query("SELECT
						GROUP_CONCAT( result.pemeriksaan SEPARATOR ',<br>&nbsp;&nbsp;' ) AS pemeriksaan,
						result.idtipe
					FROM
						(
							SELECT
								term_radiologi_mri_pemeriksaan.namapemeriksaan AS pemeriksaan,
								mtarif_radiologi.idtipe 
							FROM
								term_radiologi_mri_pemeriksaan
								JOIN mtarif_radiologi ON mtarif_radiologi.id = term_radiologi_mri_pemeriksaan.idradiologi
								LEFT JOIN mtarif_radiologi_expose ON mtarif_radiologi_expose.id = mtarif_radiologi.idexpose
								LEFT JOIN mtarif_radiologi_film ON mtarif_radiologi_film.id = mtarif_radiologi.idfilm 
							WHERE
									term_radiologi_mri_pemeriksaan.transaksi_id = '" . $transaksi_id . "'
									AND term_radiologi_mri_pemeriksaan.idtipe IN ('" . $array . "')
							GROUP BY
									mtarif_radiologi.id
						) AS result
					GROUP BY
						result.idtipe");
			return $query->result();
		}
}
