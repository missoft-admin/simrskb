<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manatomi_template_ri_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	public function getSpecified($id)
    {
        $q="SELECT 
			*
			from manatomi_template_ri M
					
			WHERE M.id='$id'";
        $query = $this->db->query($q);
        return $query->row_array();
    }
	
    public function saveData()
    {
		if ($_POST['btn_simpan']=='2'){//btn_simpan
			$this->st_default 	= 1;	
		}
		// print_r($this->input->post());exit;
        $this->nama_template_lokasi 	= $_POST['nama_template_lokasi'];		
		$this->created_by  = $this->session->userdata('user_id');
		$this->created_date  = date('Y-m-d H:i:s');
		$this->upload_login_logo(false);
        if ($this->db->insert('manatomi_template_ri', $this)) {
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
		if ($_POST['btn_simpan']=='2'){//btn_simpan
			$this->st_default 	= 1;	
		}
		$template_ri_id=$_POST['id'];
        $this->nama_template_lokasi 	= $_POST['nama_template_lokasi'];			
		$this->edited_by  = $this->session->userdata('user_id');
		$this->edited_date  = date('Y-m-d H:i:s');
		$this->upload_login_logo(true);
        if ($this->db->update('manatomi_template_ri', $this, array('id' => $_POST['id']))) {
			
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function upload_login_logo($update = false)
    {
        if (!file_exists('assets/upload/anatomi')) {
            mkdir('assets/upload/anatomi', 0755, true);
        }
			// print_r(isset($_FILES['gambar_tubuh']));exit;

        if (isset($_FILES['gambar_tubuh'])) {
            if ($_FILES['gambar_tubuh']['name'] != '') {
                $config['upload_path'] = './assets/upload/anatomi/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('gambar_tubuh')) {
                    $image_upload = $this->upload->data();
                    $this->gambar_tubuh = $image_upload['file_name'];
					// print_r($this->gambar_tubuh);exit;
                    if ($update == true) {
                        $this->remove_image($this->input->post('id'));
                    }
                    return true;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
                return true;
            }
					// print_r($this->foto);exit;
        } else {
            return true;
        }
		
    }
	public function remove_image($id)
    {
		$q="select gambar_tubuh From manatomi_template_ri H WHERE H.id='$id'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/anatomi/'.$row->gambar_tubuh) && $row->gambar_tubuh !='') {
            unlink('./assets/upload/anatomi/'.$row->gambar_tubuh);
        }
    }
    public function softDelete($id)
    {
        $this->staktif = 0;		
		$this->deleted_by  = $this->session->userdata('user_id');
		$this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('manatomi_template_ri', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function pilih($id)
    {
		$q="UPDATE manatomi_template_ri SET st_default='0' WHERE st_default='1'";
		$this->db->query($q);
        $this->st_default = 1;		
		
        if ($this->db->update('manatomi_template_ri', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
	public function aktifkan($id)
    {
        $this->status = 1;		
		// $this->deleted_by  = $this->session->userdata('user_id');
		// $this->deleted_date  = date('Y-m-d H:i:s');
		
        if ($this->db->update('manatomi_template_ri', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
