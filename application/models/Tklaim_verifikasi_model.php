<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: Acep Kursina
| EMAIL			: keorsina18@gmail.com
|--------------------------------------------------------------------------
|
*/

class Tklaim_verifikasi_model extends CI_Model
{
  
   public function list_kelompok_pasien(){
	   $q="SELECT id,nama from mpasien_kelompok 
				WHERE mpasien_kelompok.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
   public function list_rekanan(){
	   $q="SELECT id,nama from mrekanan 
				WHERE mrekanan.`status`='1'
				ORDER BY nama ASC";
		$query=$this->db->query($q);
		return $query->result();
   }
}

/* End of file Tkontrabon_verifikasi_model.php */
/* Location: ./application/models/Tkontrabon_verifikasi_model.php */
