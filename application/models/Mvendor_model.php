<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mvendor_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecified($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('mvendor');
        return $query->row();
    }

    public function saveData()
    {
        $this->nama 		   = $_POST['nama'];
        $this->alamat 	   = $_POST['alamat'];
        $this->telepon 	   = $_POST['telepon'];
        $this->norekening  = $_POST['norekening'];
        $this->cabang 	 = $_POST['cabang'];
        $this->atasnama 	 = $_POST['atasnama'];
        $this->bank 	     = $_POST['bank'];
        $this->bank_id 	     = $_POST['bank_id'];

        if ($this->db->insert('mvendor', $this)) {
			$iddistributor=$this->db->insert_id();
			if ($_POST['bank_id']!='#'){
				$data=array(
					'staktif' => 1,
					'tipe_distributor' => 3,
					'iddistributor' => $iddistributor,
					'bankid' => $_POST['bank_id'],
					'atasnama' => $_POST['atasnama'],
					'norekening' => $_POST['norekening'],
					'cabang' => $_POST['cabang'],
				);
				$this->db->insert('mdistributor_bank',$data);
			}
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function updateData()
    {
        $this->nama 		   = $_POST['nama'];
        $this->alamat 	   = $_POST['alamat'];
        $this->telepon 	   = $_POST['telepon'];
        // $this->norekening  = $_POST['norekening'];
        // $this->atasnama 	 = $_POST['atasnama'];
        // $this->cabang 	 = $_POST['cabang'];
        // $this->bank 	     = $_POST['bank'];
		// $this->bank_id 	     = $_POST['bank_id'];
        if ($this->db->update('mvendor', $this, array('id' => $_POST['id']))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    public function softDelete($id)
    {
        $this->status = 0;

        if ($this->db->update('mvendor', $this, array('id' => $id))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
