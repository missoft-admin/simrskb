<?php

class Mperiode_akun_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getAll($idkategori='')
	{
		$where='';
		if ($idkategori !=''){
			$where='';
		}
		$query = $this->db->query("
            SELECT
                makun_nomor.id,
                CONCAT( akun_header.noakun, ' - ', akun_header.namaakun ) AS noheader,
                makun_nomor.noakun,
                makun_nomor.namaakun,
                makun_nomor.level,
                mkategori_akun.nama AS namakategori,
                makun_nomor.status,
                makun_nomor.saldo
            FROM
                makun_nomor
                LEFT JOIN makun_nomor akun_header ON akun_header.noakun = makun_nomor.noheader 
                LEFT JOIN mkategori_akun mkategori_akun ON mkategori_akun.id = makun_nomor.idkategori 
            WHERE
                makun_nomor.status = '1' 
            GROUP BY
                makun_nomor.id 
            ORDER BY
                makun_nomor.noakun ASC
        ");
		return $query->result();
	}

	public function getSpecified($id)
	{
		$this->db->where("id", $id);
		$query = $this->db->get("mperiode_akun");
		return $query->row();
	}

	public function insertDuplicate()
	{
		$this->db->where('noakun', $_POST['noakun']);
		$query = $this->db->get('makun_nomor');
		if ($query->num_rows() > 0) {
			$this->update();
            return true;
		} else {
            $this->insert();
            return true;
		}
	}

	public function insert()
	{
        $noheader = ($_POST['tipeakun'] == '1' ? $_POST['noakun'] : $_POST['noheader']);
        $bertambah = ($_POST['bertambah'] == '0' ? $_POST['parent_bertambah'] : $_POST['bertambah']);
        $berkurang = ($_POST['berkurang'] == '0' ? $_POST['parent_berkurang'] : $_POST['berkurang']);
        $possaldo = ($_POST['possaldo'] == '0' ? $_POST['parent_possaldo'] : $_POST['possaldo']);
        $poslaporan = ($_POST['poslaporan'] == '0' ? $_POST['parent_poslaporan'] : $_POST['poslaporan']);

		$this->idkategori = $_POST['kategoriakun'];
		$this->idtipe = $_POST['tipeakun'];
        $this->noheader = $noheader;
        $this->noakun = $_POST['noakun'];
        $this->namaakun = $_POST['namaakun'];
        $this->level = $_POST['level'];
        $this->bertambah = $bertambah;
        $this->berkurang = $berkurang;
        $this->possaldo = $possaldo;
        $this->poslaporan = $poslaporan;

        if ($this->db->insert('makun_nomor', $this)) {
            return true;
        } else {
            $this->error_message = 'Penyimpanan Gagal';
            return false;
        }
	}

	public function update()
	{
        $noheader = ($_POST['tipeakun'] == '1' ? $_POST['noakun'] : $_POST['noheader']);
        $bertambah = ($_POST['bertambah'] == '0' ? $_POST['parent_bertambah'] : $_POST['bertambah']);
        $berkurang = ($_POST['berkurang'] == '0' ? $_POST['parent_berkurang'] : $_POST['berkurang']);
        $possaldo = ($_POST['possaldo'] == '0' ? $_POST['parent_possaldo'] : $_POST['possaldo']);
        $poslaporan = ($_POST['poslaporan'] == '0' ? $_POST['parent_poslaporan'] : $_POST['poslaporan']);

		$this->idkategori = $_POST['kategoriakun'];
		$this->idtipe = $_POST['tipeakun'];
        $this->noheader = $noheader;
        $this->noakun = $_POST['noakun'];
        $this->namaakun = $_POST['namaakun'];
        $this->level = $_POST['level'];
        $this->bertambah = $bertambah;
        $this->berkurang = $berkurang;
        $this->possaldo = $possaldo;
        $this->poslaporan = $poslaporan;

        $this->db->where('id', $_POST['id']);
        if ($this->db->update('makun_nomor', $this)) {
            return true;
        } else {
            $this->error_message = 'Penyimpanan Gagal';
            return false;
        }
	}

	public function remove($id)
	{
		$this->status = '0';

		if ($this->db->update('makun_nomor', $this, ['id' => $id])) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}
	public function aktifkan($id)
	{
		$this->status = '1';

		if ($this->db->update('makun_nomor', $this, ['id' => $id])) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

    public function checkDuplicateAkun()
	{
		$this->db->where('noakun', $_POST['noakun']);
		$query = $this->db->get('makun_nomor');
		if ($query->num_rows() > 0) {
			echo 'not available';
		} else {
			echo 'available';
		}
	}

    public function getNomorHeaderUtama($idkategori)
	{
		$this->db->where('idkategori', $idkategori);
		$this->db->where('level', 0);
		echo $this->db->count_all_results('makun_nomor') + 1;
	}

    public function getNomorAkunAvailable($idkategori, $noheader, $level)
	{
		$this->db->where('idkategori', $idkategori);
		$this->db->where('noheader', $noheader);
		$this->db->where('level', $level);
		echo $this->db->count_all_results('makun_nomor') + 1;
	}

    public function getAkunHeader($idkategori)
	{
		$this->db->where('idkategori', $idkategori);
		$this->db->where('level', 0);
		$query = $this->db->get('makun_nomor');
		return $query->result();
	}

    public function getSubAkunHeader($idkategori)
	{
		$this->db->where('idkategori', $idkategori);
		$this->db->where('level', 1);
		$query = $this->db->get('makun_nomor');
		return $query->result();
	}
	public function get_kategori($idkategori)
	{		
		return $this->db->query("SELECT H.nama FROM `mkategori_akun` H WHERE H.id='$idkategori'")->row('nama');
	}
	public function get_header($id)
	{		
		return $this->db->query("SELECT GROUP_CONCAT(H.noakun,'-',H.namaakun) as akun FROM makun_nomor H WHERE H.noakun IN (".$id.")")->row('akun');
	}
	public function get_akun($id)
	{		
		return $this->db->query("SELECT GROUP_CONCAT(H.noakun,'-',H.namaakun) as akun FROM makun_nomor H WHERE H.id IN (".$id.")")->row('akun');
	}
}
