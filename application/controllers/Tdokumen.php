<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tdokumen extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpendaftaran_poli_pasien_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->helper('path');
		
  }
	
	function upload($pendaftaran_id='',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		get_ppa_login();
		$data_login_ppa=get_ppa_login();
		$str='["1","2"]';
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($menu_kiri=='input_upload_rj'){
			$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id);
		}else{
			$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id,1);
			
		}
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tdokumen/upload/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		$data['dokumen_id'] 			= '';
		$data['status_dokumen'] 			= '';
		$data['error'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'Upload Document';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Upload Document ",'#'),
											  array("upload",'tdokumen')
											);
		$data = array_merge($data, backend_info());
		// $data = array_merge($data,$data_login_ppa,$data_satuan_ttv,$data_header_ttv,$data_assemen, backend_info());
		// print_r($data);exit;
		$data['trx_id']=$trx_id;
		
		$this->parser->parse('module_template', $data);
		
	}
	function simpan_doc(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$dokument_id=$this->input->post('dokument_id');
		$keterangan=$this->input->post('keterangan');
		
		$data=array(
			'idpasien' =>$this->input->post('idpasien'),
			'pendaftaran_id' =>$this->input->post('pendaftaran_id'),
			'pendaftaran_id_ranap' =>$this->input->post('pendaftaran_id_ranap'),
			'keterangan' => $keterangan,
		);
		if ($dokument_id){
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']= date('Y-m-d H:i:s');
			$this->db->where('id',$dokument_id);
			$output=$this->db->update('tdokument',$data);
		}else{
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']= date('Y-m-d H:i:s');
			$output=$this->db->insert('tdokument',$data);			
		}
		$this->output->set_output(json_encode($output));
	}
	function hapus_dokumen(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$id=$this->input->post('id');
		$data=array(
			'status' => 0,
			'deleted_ppa' => $login_ppa_id,
			'deleted_date' => date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		$output=$this->db->update('tdokument',$data);
		$this->output->set_output(json_encode($output));
	}
	function edit_dokumen(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$id=$this->input->post('id');
		$q="select *FROM tdokument WHERE id='$id'";
		$output=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($output));
	}
	function load_index()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$idpasien=$this->input->post('idpasien');
		$this->select = array();
		$from="
			(
				SELECT 
				COUNT(A.dokument_id) as jml_file
				,H.*,MP.nama as user_created
				FROM tdokument H
				LEFT JOIN mppa MP ON MP.id=H.created_ppa
				LEFT JOIN tdokument_file A ON A.dokument_id=H.id
				WHERE H.idpasien='$idpasien' AND H.status='1'
				GROUP BY H.id
			) as tbl
		";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('keterangan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$result = array();
			if ($r->jml_file>0){
			$btn_file ='<button onclick="file_upload('.$r->id.')"  type="button" title="File Upload"  class="btn btn-success btn-xs" ><i class="fa fa-file-picture-o"></i> '.($r->jml_file?$r->jml_file:'Belum ada ').' File</button>';	
				
			}else{
			$btn_file ='<button onclick="file_upload('.$r->id.')"  type="button" title="File Upload"  class="btn btn-danger btn-xs" ><i class="fa fa-file-picture-o"></i> '.($r->jml_file?$r->jml_file:'Belum ada ').' File</button>';	
				
			}
			$result[] = $no;
			$aksi='';
			$result[] = ($r->keterangan);
			$result[] = $btn_file;
			$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
			$aksi = '<div class="btn-group">';
			// if (UserAccesForm($user_acces_form,array('1607'))){
			$aksi .= '<button onclick="edit_dokumen('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs btn_edit"><i class="fa fa-pencil"></i></button>';	
			// }
			// if (UserAccesForm($user_acces_form,array('1608'))){
			$aksi .= '<button onclick="hapus_dokumen('.$r->id.')" type="button" title="Hapus Dokument" class="btn btn-danger btn-xs btn_edit"><i class="fa fa-trash"></i></button>';	
			// }
			$aksi .= '</div>';
			$result[] = $aksi;

			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
	}
	public function upload_file() {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		// $config['upload_path'] = './assets/upload/event/';
       $uploadDir = './assets/upload/file_dokument/';
		if (!empty($_FILES)) {
			 $idpasien = $this->input->post('idpasien');
			 $dokument_id = $this->input->post('dokument_id');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['dokument_id'] 	= $dokument_id;
			$detail['idpasien'] 	= $idpasien;
			$detail['file_name']		= $file_name;
			$detail['size']		= formatSizeUnits($size);
			$detail['tanggal_upload']	= date('Y-m-d H:i:s');
			$detail['user_upload']	= get_nama_ppa($login_ppa_id);
			$this->db->insert('tdokument_file', $detail);
		}
    }
	function refresh_image($info_id){	
		$disabel='';
		$q="SELECT *FROM tdokument_file H WHERE H.dokument_id='$info_id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/file_dokument/'.$r->file_name.'" target="_blank">'.substr($r->file_name,11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">'.$r->user_upload.'-'.HumanDateLong($r->tanggal_upload).'</td>';
			$tabel .='<td class="text-left"><button '.$disabel.' class="btn btn-xs btn-danger" type="button" title="Hapus" onclick="hapus_file('.$r->id.')"><li class="fa fa-trash-o"></li></button></td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
		}
		 
		$arr['detail'] = $tabel;
		$this->output->set_output(json_encode($arr));
	}
	function hapus_file(){
		$id=$this->input->post('id');
		$q="SELECT *FROM tdokument_file H WHERE H.id='$id'";
		$row = $this->db->query($q)->row();
		if(file_exists('./assets/upload/file_dokument/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/file_dokument/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from tdokument_file WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
}	
