<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
class Merm_general_ranap extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Merm_general_ranap_model');
		$this->load->helper('path');
		
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1445'))){
			$data=array();
			$data = $this->Merm_general_ranap_model->get_data();
			$data['list_jawaban'] = $this->Merm_general_ranap_model->list_jawaban();
			$data['jenis_isi'] 			= '1';
			$data['error'] 			= '';
			$data['title'] 			= 'Master General Consent';
			$data['content'] 		= 'Merm_general_ranap/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Apps Setting",'#'),
												  array("General Consist",'merm_general_ranap')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function preview(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1445'))){
			$data=array();
			$data = $this->Merm_general_ranap_model->get_data();
			$data['list_content'] = $this->Merm_general_ranap_model->list_content();
			// print_r($data['list_content']);exit;
			$data['error'] 			= '';
			$data['title'] 			= 'Preview General Consent';
			$data['content'] 		= 'Merm_general_ranap/preview';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Apps Setting",'#'),
												  array("General Consist",'Merm_general_ranap')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function export(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1445'))){
			$data=array();
			$data = $this->Merm_general_ranap_model->get_data();
			$data['list_content'] = $this->Merm_general_ranap_model->list_content();
			
			$options = new Options();


			$options->set('isRemoteEnabled', true);
			$dompdf = new Dompdf($options);
			$dompdf->set_option('isRemoteEnabled', TRUE);
			// $this->parser->parse('Tunit_permintaan/cetak_permintaan',array_merge($data,backend_info()));
			$html = $this->parser->parse('Merm_general_ranap/pdf',array_merge($data,backend_info()),TRUE);
			$html = $this->parser->parse_string($html,$data);
			// print_r($html);exit();
			$dompdf->loadHtml($html);
			$dompdf->setPaper('A4', 'portrait');
			$dompdf->set_option('isFontSubsettingEnabled', true);
			$dompdf->output();
			$dompdf->render();
			$dompdf->stream('General Consent Rawat Jalan.pdf', array("Attachment"=>0));
		}else{
			redirect('page404');
		}
	}
	function save(){
		if ($this->Merm_general_ranap_model->save()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('merm_general_ranap','location');
		}
	}
	function load_content(){
		$tabel='';
		$q="SELECT H.*,C.`name` as user_created,E.`name` as user_edited,GROUP_CONCAT(R.ref SEPARATOR ' / ') as ref FROM merm_general_ranap_isi H
			LEFT JOIN musers C ON C.id=H.created_by
			LEFT JOIN musers E ON E.id=H.edited_by
			LEFT JOIN merm_general_ranap_isi_jawaban M ON M.general_isi_id=H.id
			LEFT JOIN merm_referensi R ON R.id=M.ref_id
			WHERE H.`status`='1'
			
			GROUP BY H.id
			ORDER BY H.`no` ASC";
		$hasil=$this->db->query($q)->result();
		$no=1;
		foreach($hasil as $r){
			$tabel .='<tr>';
			$tabel .='<td class="text-center">'.$no.'</td>';
			$tabel .='<td>'.$r->isi.'</td>';
			$tabel .='<td class="hidden-xs">'.($r->jenis_isi=='1'?text_danger(jenis_isi($r->jenis_isi)).'<br>'.$r->ref:text_primary(jenis_isi($r->jenis_isi))).'</td>';
			$tabel .='<td class="hidden-xs">'.$r->user_created.'<br>'.($r->created_date?HumanDateLong($r->created_date):'').'</td>';
			$tabel .='<td class="text-center">';
			$tabel .='					<div class="btn-group">';
			$tabel .='						<button class="btn btn-xs btn-primary" type="button" onclick="edit_content('.$r->id.')" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></button>';
			$tabel .='						<button class="btn btn-xs btn-danger" type="button" onclick="hapus('.$r->id.')" data-toggle="tooltip" title="" data-original-title="Remove "><i class="fa fa-times"></i></button>';
			$tabel .='					</div>';
			$tabel .='				</td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		
				
		$this->output->set_output(json_encode($tabel));
	}
	function simpan_content(){
		$data['header_id']=1;
		$id=$this->input->post('idcontent');
		$data['no']=$this->input->post('no');
		$data['isi']=$this->input->post('isi');
		$data['jenis_isi']=$this->input->post('jenis_isi');
		if ($this->input->post('idcontent')){
			$general_isi_id=$id;
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$id);
			$hasil=$this->db->update('merm_general_ranap_isi',$data);
		}else{
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('merm_general_ranap_isi',$data);
			$general_isi_id=$this->db->insert_id();
		}
		$this->db->where('general_isi_id',$general_isi_id);		
		$this->db->delete('merm_general_ranap_isi_jawaban');
		$ref_id=$this->input->post('ref_id');
		// print_r($ref_id);exit;
		if ($ref_id){
			foreach($ref_id as $index=>$val){
				$data_det['general_isi_id']=$general_isi_id;
				$data_det['ref_id']=$val;
				$hasil=$this->db->insert('merm_general_ranap_isi_jawaban',$data_det);
			}
		}
		$this->output->set_output(json_encode($hasil));
	}
	function hapus(){
	  $id=$this->input->post('id');
	  $this->deleted_by = $this->session->userdata('user_id');
	  $this->deleted_date = date('Y-m-d H:i:s');
	  $this->status=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('merm_general_ranap_isi',$this);
	  
	  json_encode($hasil);
	  
	}
	function get_edit(){
	  $id=$this->input->post('id');
	  $q="SELECT *FROM merm_general_ranap_isi H WHERE H.id='$id'";
	  $hasil= $this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($hasil));
	  
	}
	function load_jawaban(){
	  $id=$this->input->post('id');
	  $q="SELECT H.id as ref_id,H.ref,CASE WHEN M.ref_id IS NOT NULL THEN 'selected' ELSE '' END as pilih 
			FROM merm_referensi H
			LEFT JOIN merm_general_ranap_isi_jawaban M ON M.ref_id=H.id AND M.general_isi_id='$id'
			WHERE H.ref_head_id='16' AND H.status='1'";
		
	  $hasil= $this->db->query($q)->result();
	  $data='';
	  foreach($hasil as $r){
		  $data .='<option '.$r->pilih.' value="'.$r->ref_id.'">'.$r->ref.'</option>';
	  }
	  $this->output->set_output(json_encode($data));
	  
	}
}
