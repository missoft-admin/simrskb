<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mnadi extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mnadi_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data=$this->Mnadi_model->get_nadi();
		if (UserAccesForm($user_acces_form,array('1605'))){
			
			$data['tab'] 			= $tab;
			$data['error'] 			= '';
			$data['title'] 			= 'Referensi Nadi';
			$data['content'] 		= 'Mnadi/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Referensi Nadi",'mnadi')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	

  function simpan_nadi(){
		$nadi_id=$this->input->post('nadi_id');
		$data=array(
			'nadi_1'=>$this->input->post('nadi_1'),
			'nadi_2'=>$this->input->post('nadi_2'),
			'kategori_nadi'=>$this->input->post('kategori_nadi'),
			'warna'=>$this->input->post('warna'),
			'staktif'=>1,
		);
		if ($nadi_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mnadi',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$nadi_id);
		    $hasil=$this->db->update('mnadi',$data);
		}
		  
		  json_encode($hasil);
	}
	function update_satuan(){
		$satuan_nadi=$this->input->post('satuan_nadi');
		$data=array(
			'satuan_nadi'=>$this->input->post('satuan_nadi'),
			
		);
		$hasil=$this->db->update('mnadi_satuan',$data);
		
		json_encode($hasil);
	}
	
	function load_nadi()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
							FROM `mnadi` H
							where H.staktif='1'
							ORDER BY H.nadi_1


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nadi_1','nadi_1','kategori_nadi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->nadi_1.' - '.$r->nadi_2);
          $result[] = $r->kategori_nadi;
          $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'" type="button" >'.$r->warna.'</button>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('1611'))){
		  $aksi .= '<button onclick="edit_nadi('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		   if (UserAccesForm($user_acces_form,array('1612'))){
		  $aksi .= '<button onclick="hapus_nadi('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_nadi(){
	  $nadi_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$nadi_id);
		$hasil=$this->db->update('mnadi',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_nadi(){
	  $nadi_id=$this->input->post('id');
	  $q="SELECT *FROM mnadi H WHERE H.id='$nadi_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
  
}
