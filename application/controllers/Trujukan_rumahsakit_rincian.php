<?php

defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Trujukan_rumahsakit_rincian extends CI_Controller
{
	/**
	 * Rincian Tagihan Fee Rujukan controller.
	 * Developer @gunalirezqimauludi
	 */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Trujukan_rumahsakit_rincian_model', 'model');
	}

	public function index()
	{
		$data = [
			'notransaksi' => '',
			'idrujukan' => '',
			'tanggaljatuhtempo_dari' => date('d/m/Y'),
			'tanggaljatuhtempo_sampai' => date('d/m/Y'),
			'jenis_berekanan' => '',
			'status' => '',
		];

		$data['error'] = '';
		$data['title'] = 'Rincian Tagihan Fee Rujukan';
		$data['content'] = 'Trujukan_rumahsakit_rincian/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rincian Tagihan Fee Rujukan', '#'],
			['List', 'trujukan_rumahsakit_rincian']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter()
	{
		$data = [
			'notransaksi' => $this->input->post('notransaksi'),
			'idrujukan' => $this->input->post('idrujukan'),
			'tanggaljatuhtempo_dari' => $this->input->post('tanggaljatuhtempo_dari'),
			'tanggaljatuhtempo_sampai' => $this->input->post('tanggaljatuhtempo_sampai'),
			'jenis_berekanan' => $this->input->post('jenis_berekanan'),
			'status' => $this->input->post('status'),
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Rincian Tagihan Fee Rujukan';
		$data['content'] = 'Trujukan_rumahsakit_rincian/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rincian Tagihan Fee Rujukan', '#'],
			['List', 'trujukan_rumahsakit_rincian']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function detail($id)
	{
		$data = [
			'idfeerujukan' => $id,
			'tanggal_dari' => '',
			'tanggal_sampai' => '',
			'nomedrec' => '',
			'namapasien' => '',
			'namapasien' => '',
			'idlayanan' => '',
			'idasalpasien' => ''
		];

		$data['error'] = '';
		$data['title'] = 'Rincian Transaksi Tagihan Fee Rujukan';
		$data['content'] = 'Trujukan_rumahsakit_rincian/detail';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rincian Transaksi Tagihan Fee Rujukan', '#'],
			['Rincian Transaksi', 'trujukan_rumahsakit_rincian']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter_detail($id)
	{
		$data = [
			'idfeerujukan' => $id,
			'tanggal_dari' => $this->input->post('tanggal_dari'),
			'tanggal_sampai' => $this->input->post('tanggal_sampai'),
			'nomedrec' => $this->input->post('nomedrec'),
			'namapasien' => $this->input->post('namapasien'),
			'namapasien' => $this->input->post('namapasien'),
			'idlayanan' => $this->input->post('idlayanan'),
			'idasalpasien' => $this->input->post('idasalpasien')
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Rincian Transaksi Tagihan Fee Rujukan';
		$data['content'] = 'Trujukan_rumahsakit_rincian/detail';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rincian Transaksi Tagihan Fee Rujukan', '#'],
			['List', 'trujukan_rumahsakit_rincian']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function process_approval($id)
	{
		if ($this->model->processApproval($id)) {
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'Proses Pengajuan Approval Fee Rujukan telah berhasil.');
			redirect('Trujukan_rumahsakit_rincian', 'location');
		}
	}

	public function next_periode()
	{
		$data = [
			'idasalpasien' => $this->input->post('idasalpasien'),
			'idtipe' => $this->input->post('idtipe'),
			'idfeerujukan' => $this->input->post('idfeerujukan'),
			'idrujukan' => $this->input->post('idrujukan'),
			'namarujukan' => $this->input->post('namarujukan'),
			'periode_sebelumnya' => YMDFormat($this->input->post('periode_sebelumnya')),
			'periode_pembayaran' => YMDFormat($this->input->post('periode_pembayaran')),
			'periode_jatuhtempo' => YMDFormat($this->input->post('periode_jatuhtempo')),
		];

		if ($data['periode_sebelumnya'] != $data['periode_pembayaran']) {
			if ($this->model->nextPeriode($data)) {
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'Periode Pembayaran Rincian Tagihan Fee Rujukan telah di alihkan.');
				redirect('Trujukan_rumahsakit_rincian', 'location');
			}
		} else {
			redirect('Trujukan_rumahsakit_rincian', 'location');
		}
	}

	public function getIndex($uri = 'index')
	{
		$this->select = [
			'tfeerujukan_rumahsakit.*', 'mrumahsakit.jenis_berekanan'
		];
		$this->from = 'tfeerujukan_rumahsakit';
		$this->join = [
			['mrumahsakit', 'mrumahsakit.id = tfeerujukan_rumahsakit.idrujukan', '']
		];

		$this->where = [];

		if ($uri == 'filter') {
			if ($this->session->userdata('notransaksi') != null) {
				$this->where = array_merge($this->where, ['notransaksi' => $this->session->userdata('notransaksi')]);
			}
			if ($this->session->userdata('idrujukan') != 0) {
				$this->where = array_merge($this->where, ['idrujukan' => $this->session->userdata('idrujukan')]);
			}
			if ($this->session->userdata('tanggaljatuhtempo_dari') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal_jatuhtempo) >=' => YMDFormat($this->session->userdata('tanggaljatuhtempo_dari'))]);
			}
			if ($this->session->userdata('tanggaljatuhtempo_sampai') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal_jatuhtempo) <=' => YMDFormat($this->session->userdata('tanggaljatuhtempo_sampai'))]);
			}
			if ($this->session->userdata('jenis_berekanan') != 0) {
				$this->where = array_merge($this->where, ['status' => $this->session->userdata('jenis_berekanan')]);
			}
			if ($this->session->userdata('status') != 0) {
				$this->where = array_merge($this->where, ['status' => $this->session->userdata('status')]);
			}
		}

		$this->order = [
			'id' => 'DESC'
		];

		$this->group = [];

		$this->column_search = ['notransaksi', 'namarujukan', 'tanggal_jatuhtempo', 'nominal'];
		$this->column_order = ['notransaksi', 'namarujukan', 'tanggal_jatuhtempo', 'nominal'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $row) {
			$no++;

			$result = [];

			$status_approval = '<br>' . '<br>' . status_approval_HD($row->status_approval, '');

			$action = '<div class="btn-group">';
			$action .= '<a href="' . site_url() . 'trujukan_rumahsakit_rincian/detail/' . $row->id . '" title="Rincian Fee Rujukan" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>';

			if ($row->status_stop == '0') {
				$action .= '<a href="' . site_url() . 'trujukan_rumahsakit_rincian/stop_periode/' . $row->idtipe . '/' . $row->id . '" data-namarujukan="' . $row->namarujukan . '" data-periodepembayaran="' . DMYFormat($row->tanggal_pembayaran) . '" title="Stop Periode Pembayaran" class="btn btn-danger btn-sm" onclick="actionStopPeriode(this); return false;"><i class="fa fa-stop-circle "></i></a>';
				$action .= '<a href="#" data-idasalpasien="' . $row->idasalpasien . '" data-idtipe="' . $row->idtipe . '" data-idfeerujukan="' . $row->id . '" data-idrujukan="' . $row->idrujukan . '" data-namarujukan="' . $row->namarujukan . '" data-periodepembayaran="' . DMYFormat($row->tanggal_pembayaran) . '" data-toggle="modal" data-target="#PeriodePembayaranModal" title="Perubahan Periode Pembayaran" class="btn btn-info btn-sm actionNextPeriode"><i class="fa fa-arrow-right"></i></a>';
			}

			if ($row->status_approval == '0') {
				$action .= '<a href="' . site_url() . 'trujukan_rumahsakit_rincian/process_approval/' . $row->id . '" data-namarujukan="' . $row->namarujukan . '" data-periodepembayaran="' . DMYFormat($row->tanggal_pembayaran) . '" title="Proses Approval" onclick="actionSendRequestApproval(this); return false;" class="btn btn-warning btn-sm"><i class="fa fa-paper-plane"></i></a>';
			}

			$action .= '<div class="btn-group">
				<button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
					<span class="fa fa-print"></span>
				</button>
				<ul class="dropdown-menu dropdown-menu-left">
					<li></li>
				</ul>
			</div>';
			$action .= '</div>';

			$result[] = $no;
			$result[] = $row->notransaksi;
			$result[] = $row->namarujukan;
			$result[] = JenisBerekanan($row->jenis_berekanan);
			$result[] = DMYFormat($row->tanggal_jatuhtempo);
			$result[] = number_format($row->nominal);
			$result[] = status_feerujukan($row->status_approval);
			$result[] = $action;

			$data[] = $result;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function getIndexDetail($uri = 'detail', $idfeerujukan)
	{
		$this->select = ['tfeerujukan_rumahsakit_detail.*', 'mrumahsakit.idkategori AS statusrekanan'];
		$this->from = 'tfeerujukan_rumahsakit_detail';
		$this->join = [
			['mrumahsakit', 'mrumahsakit.id = tfeerujukan_rumahsakit_detail.idrujukan', ''],
		];

		$this->where = [];
		$this->where = array_merge($this->where, ['tfeerujukan_rumahsakit_detail.idfeerujukan' => $idfeerujukan]);

		if ($uri == 'filter_detail') {
			if ($this->session->userdata('tanggal_dari') != null) {
				$this->where = array_merge($this->where, ['DATE(tfeerujukan_rumahsakit_detail.tanggal_pemeriksaan) >=' => YMDFormat($this->session->userdata('tanggal_dari'))]);
			}
			if ($this->session->userdata('tanggal_sampai') != null) {
				$this->where = array_merge($this->where, ['DATE(tfeerujukan_rumahsakit_detail.tanggal_pemeriksaan) <=' => YMDFormat($this->session->userdata('tanggal_sampai'))]);
			}
			if ($this->session->userdata('nomedrec') != null) {
				$this->where = array_merge($this->where, ['tfeerujukan_rumahsakit_detail.nomedrec' => $this->session->userdata('nomedrec')]);
			}
			if ($this->session->userdata('namapasien') != null) {
				$this->where = array_merge($this->where, ['tfeerujukan_rumahsakit_detail.namapasien LIKE' => '%' . $this->session->userdata('namapasien') . '%']);
			}
			if ($this->session->userdata('idlayanan') != 0) {
				$this->where = array_merge($this->where, ['tfeerujukan_rumahsakit_detail.idlayanan' => $this->session->userdata('idlayanan')]);
			}
			if ($this->session->userdata('idasalpasien') != 0) {
				$this->where = array_merge($this->where, ['tfeerujukan_rumahsakit_detail.idasalpasien' => $this->session->userdata('idasalpasien')]);
			}
		}

		$this->order = [
			'tfeerujukan_rumahsakit_detail.id' => 'DESC'
		];

		$this->group = [];

		$this->column_search = ['tfeerujukan_rumahsakit_detail.nomedrec', 'tfeerujukan_rumahsakit_detail.namapasien'];
		$this->column_order = ['tfeerujukan_rumahsakit_detail.nomedrec', 'tfeerujukan_rumahsakit_detail.namapasien'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $row) {
			$no++;

			$result = [];

			$action = '<div class="btn-group">';
			$action .= '<a href="#" title="Rincian" class="btn btn-danger btn-sm"><i class="fa fa-list"></i></a>';
			$action .= '<a href="' . site_url() . 'trujukan_rumahsakit_rincian/print_detail/' . $row->id . '" title="Print" class="btn btn-primary btn-sm"><i class="fa fa-print"></i></a>';
			$action .= '</div>';

			$result[] = $no;
			$result[] = $row->tanggal_pemeriksaan;
			$result[] = $row->nomedrec;
			$result[] = $row->namapasien;
			$result[] = $row->layanan;
			$result[] = $row->asalpasien;
			$result[] = $row->namarujukan;
			$result[] = StatusRekanan($row->statusrekanan);
			$result[] = $row->tanggal_pembayaran;
			$result[] = number_format($row->totaltransaksi);
			$result[] = number_format($row->nominalrujukan);
			$result[] = $action;

			$data[] = $result;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
