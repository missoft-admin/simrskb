<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Msetting_tarif_pengajuan extends CI_Controller
{
    /**
     * Setting Tarif Pengajuan controller.
     * Developer @
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Msetting_tarif_pengajuan_model', 'model');
        $this->load->helper('path');
    }

    public function index()
    {
        $data = array();
        $data['error'] 			= '';
        $data['title'] 			= 'Setting Tarif Pengajuan';
        $data['content'] 		= 'Msetting_tarif_pengajuan/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Setting Tarif Pengajuan",'#'),
            array("List",'msetting_tarif_pengajuan')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create()
    {
        $data = array(
            'id' => '',
            'nama' => '',
            'idpengajuan' =>  array(),
            'status' => '',
        );

        $data['error'] 			= '';
        $data['title'] 			= 'Tambah Setting Tarif Pengajuan';
        $data['content'] 		= 'Msetting_tarif_pengajuan/manage';
        $data['breadcrum']	= array(
            array("RSKB Halmahera",'#'),
            array("Setting Tarif Pengajuan",'#'),
            array("Tambah",'msetting_tarif_pengajuan')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id)
    {
        if ($id != '') {
            $row = $this->model->getSpecified($id);
            if (isset($row->id)) {
                $data = array(
                    'id' => $row->id,
                    'nama' => $row->nama,
                    'idpengajuan' => json_decode($row->idpengajuan),
                    'status' => $row->status,
                );

                $data['error'] 			= '';
                $data['title'] 			= 'Ubah Setting Tarif Pengajuan';
                $data['content']	 	= 'Msetting_tarif_pengajuan/manage';
                $data['breadcrum'] 	= array(
                    array("RSKB Halmahera",'#'),
                    array("Setting Tarif Pengajuan",'#'),
                		array("Ubah",'msetting_tarif_pengajuan')
                );

                $data['detail'] = $this->model->getDetailSetting($id);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('msetting_tarif_pengajuan/index', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('msetting_tarif_pengajuan/index');
        }
    }

	public function setting($id)
    {
        if ($id != '') {
            $row = $this->model->getSpecified($id);
            if (isset($row->id)) {
                $data = array(
                    'id' => $row->id,
                    'nama' => $row->nama,
                    'group_diskon_all' => $row->group_diskon_all,
                    'idpengajuan' => json_decode($row->idpengajuan),
                    'status' => $row->status,
                );

                $data['error'] 			= '';
                $data['title'] 			= 'Setting Akun Pendapatan Informasi Medis';
                $data['content']	 	= 'Msetting_tarif_pengajuan/manage_setting';
                $data['breadcrum'] 	= array(
                        array("RSKB Halmahera",'#'),
                        array("Setting Tarif Pengajuan",'#'),
                		array("Ubah",'msetting_tarif_pengajuan')
                );

                $data['detail'] = $this->model->getDetailSetting($id);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('msetting_tarif_pengajuan/index', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('msetting_tarif_pengajuan/index');
        }
    }

	function load_index(){
		
		$id=$this->input->post('id');
		$q="SELECT *from mgroup_pembayaran A  ORDER BY A.nama ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
        $q = "
			SELECT H.*,M.nama as nama_pengajuan
			FROM msetting_tarif_pengajuan_detail H
			LEFT JOIN mpengajuan_skd M ON M.id=H.idpengajuan
			WHERE H.idsetting='$id'
			ORDER BY H.idpengajuan
			";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		foreach($rows as $r){
			$no++;
			
			$aksi       = '<div class="btn-group">';
			$select_rs='<select name="group_rs[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->group_rs).'
						</select>';
			$select_dokter='<select name="group_dokter[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->group_dokter).'
						</select>';
			$select_diskon='<select name="group_diskon[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->group_diskon).'
						</select>';
		
			$tbl .='<tr>';
			$tbl .='<td>'.$r->nama_pengajuan.'</td>';
			$tbl .='<td>'.$r->permintaan_ke.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->tarif_rs,2).'<input type="hidden" name="iddet[]" value="'.$r->id.'"></td>';
			$tbl .='<td>'.$select_rs.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->tarif_dokter,2).'</td>';
			$tbl .='<td>'.$select_dokter.'</td>';
			$tbl .='<td>'.$select_diskon.'</td>';
			$tbl .='</tr>';
		}
		
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }

	function list_akun($list,$idakun){
		$hasil='';
		$hasil .='<option value="" '.($idakun==''?"selected":"").'>-Belum dipilih-</option>';
		foreach($list as $row){
			$hasil .='<option value="'.$row->id.'" '.($row->id==$idakun?"selected":"").'>'.$row->nama.'</option>';
		}
		return $hasil;
	}

	function save_setting(){
		$id=$this->input->post('id');
		// print_r($this->input->post());exit();
		// $id=$this->model->saveData();
		if($this->model->save_setting()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('msetting_tarif_pengajuan/setting/'.$id,'location');
		}
	
	}

    public function delete($id)
    {
        $this->model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('msetting_tarif_pengajuan/index', 'location');
    }

    public function save()
    {
        if ($this->input->post('id') == '') {
            if ($this->model->saveData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                redirect('msetting_tarif_pengajuan/index', 'location');
            }
        } else {
            if ($this->model->updateData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                redirect('msetting_tarif_pengajuan/index', 'location');
            }
        }
    }

    public function getIndex()
    {
        $this->select = array();
        $this->from   = 'msetting_tarif_pengajuan';
        $this->join 	= array();

        $this->order  = array(
          'id' => 'DESC'
        );

        $this->group  = array();

        $this->column_search = array('nama');
        $this->column_order = array('nama');

        $list = $this->datatable->get_datatables();

				$no = $_POST['start'];

				$data = array();
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->nama;
            $row[] = LabelSettingAnalisa($r->status);
            $row[] = '<div class="btn-group">
							<a href="'.site_url().'msetting_tarif_pengajuan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
							<a href="'.site_url().'msetting_tarif_pengajuan/setting/'.$r->id.'" data-toggle="tooltip" title="Setting" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>
							<a href="'.site_url().'msetting_tarif_pengajuan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
						</div>';

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
      );
        echo json_encode($output);
    }
}