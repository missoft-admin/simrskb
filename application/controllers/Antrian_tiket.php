<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
class Antrian_tiket extends CI_Controller {

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Antrian_tiket_model');
		$this->load->helper('path');
		
  }
	function index($auto_print='1'){
		
			$data = $this->Antrian_tiket_model->get_index_setting();
			$data['list_running'] = $this->Antrian_tiket_model->list_running();
			$data['title'] 			= 'APM Setting Index';
			$data['title_atas'] 		= 'Pengambilan Antrian';
			$data['content'] 		= 'Antrian_tiket/index';
			$data['auto_print'] 		= $auto_print;
			$data['error'] 		= '';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("APM Setting",'#'),
												  array("Index Setting",'apm_setting')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_apm', $data);
		
	}
	
  function refresh_button(){
	  //BUk Otomatis
	  $q="UPDATE antrian_pelayanan INNER JOIN (
					SELECT D.antrian_id,H.nama_pelayanan,D.jam_buka,D.jam_tutup
					,CASE WHEN D.jam_buka <=CURRENT_TIME THEN 1 ELSE 0 END st_open
					,0 open_by,'BUKA OTOMATIS' as open_nama,NOW() as open_date
					FROM date_row 
					INNER JOIN antrian_pelayanan_hari D ON D.kodehari=date_row.hari
					INNER JOIN antrian_pelayanan H ON H.id=D.antrian_id AND H.st_open='0' 
					WHERE date_row.tanggal=CURRENT_DATE() AND D.st_24_jam='0' AND  D.jam_buka <=CURRENT_TIME AND D.jam_tutup>=CURRENT_TIME AND H.st_open='0' AND D.auto_open='1'
								
					) T ON T.antrian_id=antrian_pelayanan.id
			SET antrian_pelayanan.st_open=1,antrian_pelayanan.open_by=T.open_by,antrian_pelayanan.open_date=T.open_date,antrian_pelayanan.open_nama=T.open_nama";
		$this->db->query($q);
		
		//TUTUP OTOTMATIS
	  $q="UPDATE antrian_pelayanan INNER JOIN (
			SELECT D.antrian_id,H.nama_pelayanan,D.jam_buka,D.jam_tutup
			,CASE WHEN D.jam_tutup <=CURRENT_TIME THEN 1 ELSE 0 END st_tutup
			,0 tutup_by,'TUTUP OTOMATIS' as tutup_nama,NOW() as tutup_date
			FROM date_row 
			INNER JOIN antrian_pelayanan_hari D ON D.kodehari=date_row.hari
			INNER JOIN antrian_pelayanan H ON H.id=D.antrian_id AND H.st_open='1' 
			WHERE date_row.tanggal=CURRENT_DATE() AND D.st_24_jam='0' AND D.jam_tutup <=CURRENT_TIME
			) T ON T.antrian_id=antrian_pelayanan.id AND T.st_tutup='1' AND antrian_pelayanan.st_open='1'
			SET antrian_pelayanan.st_open=0,antrian_pelayanan.tutup_by=T.tutup_by,antrian_pelayanan.tutup_date=T.tutup_date,antrian_pelayanan.tutup_nama=T.tutup_nama";
		$this->db->query($q);
		
		$q="SELECT D.antrian_id,AP.nama_pelayanan,AP.kode,AP.kuota
			,SUM(CASE WHEN AH.st_panggil='0' THEN 1 ELSE 0 END) as sisa_antrian
			,SUM(CASE WHEN AH.st_panggil IS NOT NULL THEN 1 ELSE 0 END) as total_antrian
			,COALESCE(MAX(AH.noantrian),0) +1 as antrian_ahir
			FROM date_row H
			INNER JOIN antrian_pelayanan_hari D ON D.kodehari=H.hari AND D.status=1 
			INNER JOIN antrian_pelayanan AP ON AP.id=D.antrian_id AND AP.st_open='1'
			LEFT JOIN antrian_harian AH ON AH.antrian_id=AP.id AND AH.tanggal=H.tanggal
			WHERE H.tanggal=CURRENT_DATE()
			GROUP BY AP.id
			ORDER BY AP.urutan";
			
		$hasil=$this->db->query($q)->result();
		$tabel='';
		foreach($hasil as $row){
			$antrian_ahir=$row->antrian_ahir;
			$no_antrian=$row->kode.str_pad($antrian_ahir,3,"0",STR_PAD_LEFT);
			$text_antrian='';
			$class_antrian='';
			if ($row->sisa_antrian>0){
				$text_antrian='<strong> MENUNGGU : '.$row->sisa_antrian.' ANTRIAN</strong>';
				$class_antrian='text-danger';
			}else{
				$class_antrian='text-success';
				
				$text_antrian='<strong> TIDAK ADA ANTRIAN </strong>';
			}
			$tabel .='<div class="col-sm-3">
					<a class="block block-rounded block-link-hover3 text-center" onclick="get_antrian('.$row->antrian_id.')" href="javascript:void(0)">
						<div class="block-content block-content-full bg-success">
							<div class="h2 font-w400 text-white">'.$row->nama_pelayanan.'</div>
							<div class="h1 font-w700 text-white push-5-t">'.$no_antrian.'</div>
						</div>
						<div class="block-content block-content-full block-content-mini  bg-white '.$class_antrian.'">
							<i class="fa fa-arrow-up"></i>'.$text_antrian.'
						</div>
					</a>
				</div>';
		}
		
		$this->output->set_output(json_encode($tabel));
  }
  
  function get_antrian(){
	  $antrian_id=$this->input->post('antrian_id');
	  $q="INSERT INTO antrian_harian (antrian_id,kodehari,namahari,kode,tanggal,waktu_ambil,st_panggil)
		SELECT '$antrian_id' as antrian_id,MH.kodehari,MH.namahari,AP.kode,DR.tanggal,NOW() as waktu_ambil,0 as st_panggil FROM `date_row` DR
		LEFT JOIN merm_hari MH ON MH.kodehari=DR.hari
		LEFT JOIN antrian_pelayanan AP ON AP.id='$antrian_id'

		WHERE DR.tanggal=CURRENT_DATE()";
		$hasil=$this->db->query($q);
		$this->output->set_output(json_encode($hasil));
  }
  function cetak_ulang(){
	  $data=array();
		$q="SELECT * FROM `antrian_print_out` ";
		$data=$this->db->query($q)->row_array();
		// print_r($data);exit;
		
		$data_last = $this->Antrian_tiket_model->last_data();
		$data=array_merge($data,$data_last);
		// print_r($data);exit;
		$options = new Options();
		$data['title']='CETAK TIKET';
		// $data = array_merge($data, backend_info());
		// $this->parser->parse('Antrian_tiket/tiket', $data);
		$html = $this->parser->parse('Antrian_tiket/tiket',array_merge($data,backend_info()),TRUE);
		  $this->load->view('Antrian_tiket/tiket',$html);
		$options->set('isRemoteEnabled', true);
		$dompdf = new Dompdf($options);
		$dompdf->set_option('isRemoteEnabled', TRUE);
		// $this->parser->parse('Tunit_permintaan/cetak_permintaan',array_merge($data,backend_info()));
		$html = $this->parser->parse_string($html,$data);
		$ukuran_point=28.34;
		$ukuran_lebar=$data['lebar_kertas']*$ukuran_point;
		
		// print_r($html);exit();
		$dompdf->loadHtml($html);
		$customPaper = array(0,0,$ukuran_lebar,$ukuran_lebar*4);
		$dompdf->set_paper($customPaper);					
		// $dompdf->setPaper('A4', 'portrait');
		$dompdf->set_option('isFontSubsettingEnabled', true);
		$dompdf->output();
		$dompdf->render();
		$dompdf->stream('Skrining Covid Rawat Jalan.pdf', array("Attachment"=>0));
  }	
	
}
