<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mgds extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mgds_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data=$this->Mgds_model->get_gds();
		if (UserAccesForm($user_acces_form,array('1743'))){
			
			$data['tab'] 			= $tab;
			$data['error'] 			= '';
			$data['title'] 			= 'Referensi GDS';
			$data['content'] 		= 'Mgds/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Referensi GDS",'mgds')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	

  function simpan_gds(){
		$gds_id=$this->input->post('gds_id');
		$data=array(
			'gds_1'=>$this->input->post('gds_1'),
			'gds_2'=>$this->input->post('gds_2'),
			'kategori_gds'=>$this->input->post('kategori_gds'),
			'warna'=>$this->input->post('warna'),
			'staktif'=>1,
		);
		if ($gds_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mgds',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$gds_id);
		    $hasil=$this->db->update('mgds',$data);
		}
		  
		  json_encode($hasil);
	}
	function update_satuan(){
		$satuan_gds=$this->input->post('satuan_gds');
		$data=array(
			'satuan_gds'=>$this->input->post('satuan_gds'),
			
		);
		$hasil=$this->db->update('mgds_satuan',$data);
		
		json_encode($hasil);
	}
	
	function load_gds()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
							FROM `mgds` H
							where H.staktif='1'
							ORDER BY H.gds_1


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('gds_1','gds_1','kategori_gds');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->gds_1.' - '.$r->gds_2);
          $result[] = $r->kategori_gds;
          $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'" type="button" >'.$r->warna.'</button>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('1745'))){
		  $aksi .= '<button onclick="edit_gds('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		   if (UserAccesForm($user_acces_form,array('1746'))){
		  $aksi .= '<button onclick="hapus_gds('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_gds(){
	  $gds_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$gds_id);
		$hasil=$this->db->update('mgds',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_gds(){
	  $gds_id=$this->input->post('id');
	  $q="SELECT *FROM mgds H WHERE H.id='$gds_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
  
}
