<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Tstockopname extends CI_Controller {

    /**
     * Stock Opname controller
     * Developer Acep Kursina
     */

    public function __construct() {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Tstockopname_model','model');
        $this->load->model('munitpelayanan_model', 'unitpelayanan');
		$this->load->model('Mdata_obat_model');
		$this->load->helper('url');
    }

    public function index($iddariunit='#') {
        // print_r(date('d'));exit();
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('267'))){
			$data=array();
			$data['gudangtipe'] = '#';
			$data['iddariunit'] = $iddariunit;
			$data['bulan'] = date('m');
			$data['tahun'] = date('Y');
			$data['tgl_so'] = date('d-m-Y H:i:s');
			$data['tgl_input'] = date('d-m-Y');
			// print_r($data);exit();
			$data['list_unitpelayanan'] = $this->unitpelayanan->getDefaultUnitPelayananUser();
			// $data['list_tipe'] = $this->model->list_tipe();
			$data['list_tipegudang'] = $this->model->list_tipegudang();
			$data['error'] = '';
			$data['title'] = 'Stock Opname';
			$data['content'] = 'Tstockopname/index';
			$data['tombol'] = 'index';
			$data['breadcrum'] = [
				["RSKB Halmahera",'#'],
				["Stock Opname",'tstockopname/index'],
				["List",'#']
			];

			$data['nostockopname'] = $this->model->get_no();
			// print_r($data['list_unitpelayanan']);exit();
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);

		}else{
			redirect('page404');
		}
    }
	public function rekap($id='#') {
        // print_r(date('d'));exit();
		$data=array();
		$data=$this->model->get_so($id);
		// print_r($data);exit();
		$data['error'] = '';
        $data['title'] = 'Stock Opname Result';
        $data['content'] = 'Tstockopname/rekap';
        $data['tombol'] = 'rekap';
        $data['breadcrum'] = [
            ["RSKB Halmahera",'#'],
            ["Stock Opname",'tstockopname/index'],
            ["History Result",'#']
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	public function print_data($id)
    {
        // instantiate and use the dompdf class
        // $dompdf = new Dompdf();

        $data = $this->model->get_so($id);
        $data['nama_periode'] = get_bulan(substr($data['periode'],4,2)).' '.substr($data['periode'],0,4);
		// $per=

        $row_detail = $this->model->list_so($id);
		// print_r($row_detail);exit();
		$recap=$this->model->recap_detail($id);
		// print_r($recap);exit();
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Stokopname');

        // Set Logo
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $logo = FCPATH.'/assets/upload/logo/logoreport.jpg';
        $objDrawing->setPath($logo);
        $objDrawing->setCoordinates('B1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setHeight(70);
        $objDrawing->setWidth(40);
        $objDrawing->setWorksheet($activeSheet);

        // Set Header
        $activeSheet->setCellValue('B1', "                                      RUMAH SAKIT KHUSUS BEDAH HALMAHERA SIAGA");
        $activeSheet->setCellValue('B2', "                                      Jl.LLRE.Martadinata no.28, Telp: +6222-4206061");
        $activeSheet->setCellValue('B3', "                                      Bandung, 40115");
        $activeSheet->mergeCells('B1:AA1');
        $activeSheet->mergeCells('B2:AA2');
        $activeSheet->mergeCells('B3:AA3');
        $activeSheet->getStyle('B1:AA1')->getFont()->setSize(11);
        $activeSheet->getStyle('B2:AA2')->getFont()->setSize(11);
        $activeSheet->getStyle('B3:AA3')->getFont()->setSize(11);

		// Set Title
		$activeSheet->setCellValue('B5', "REKAPITULASI STOKOPNAME");
		$activeSheet->mergeCells('B5:M5');
		$activeSheet->getStyle('B5')->getFont()->setBold(true)->setSize(14);
		$activeSheet->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		// Set Periode
		$activeSheet->setCellValue('B7', "Tanggal SO");
		$activeSheet->setCellValue('C7', HumanDateLong($data['tgl_so']));

		$activeSheet->setCellValue('B8', "No SO");
		$activeSheet->setCellValue('C8', $data['nostockopname']);

		$activeSheet->setCellValue('B9', "Unit Pelayanan");
		$activeSheet->setCellValue('C9', $data['nama_unit']);
		
		$activeSheet->setCellValue('B10', "User Finished");
		$activeSheet->setCellValue('C10', $data['user_finish']);

		$activeSheet->setCellValue('E7', "Tanggal Input");
		$activeSheet->setCellValue('F7', HumanDateLong($data['tgl_input']));

		$activeSheet->setCellValue('E8', "Periode");
		$activeSheet->setCellValue('F8', $data['nama_periode']);

		$activeSheet->setCellValue('E9', "User Created");
		$activeSheet->setCellValue('F9', $data['user_create']);
		
		$activeSheet->setCellValue('E10', "Tanggal Selesai");
		$activeSheet->setCellValue('F10', $data['tgl_finish']);
		$activeSheet->mergeCells('F7:G7');
		$activeSheet->mergeCells('F8:G8');
		$activeSheet->mergeCells('F9:G9');
		$activeSheet->mergeCells('F10:G10');
		
		// $activeSheet->mergeCells('A7:B7');
		// $activeSheet->mergeCells('A8:B8');
		// $activeSheet->mergeCells('A9:B9');
		// $activeSheet->mergeCells('A10:B10');
		// Set Header Column
		$activeSheet->setCellValue('B12', "NO");
		$activeSheet->setCellValue('C12', "TANGGAL");
		$activeSheet->setCellValue('D12', "TIPE");
		$activeSheet->setCellValue('E12', "STATUS");
		$activeSheet->setCellValue('F12', "STOK SISTEM");
		$activeSheet->setCellValue('G12', "STOK FISIK");
		$activeSheet->setCellValue('H12', "SELISIH");
		$activeSheet->setCellValue('I12', "CATATAN");
		$activeSheet->setCellValue('J12', "HPP");
		$activeSheet->setCellValue('K12', "NILAI");
		$activeSheet->setCellValue('L12', "NILAI SELISIH");
		$activeSheet->setCellValue('M12', "USER");
		$activeSheet->getStyle('B12:M12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("B12:M12")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
	  'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK,
					)
				),
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Calibri'
				)
			)
		);

		$x = 12;
		$sisaStok = 0;
		$jml=0;
        if (COUNT($row_detail)) {
            foreach ($row_detail as $row) {
				$x = $x+1;
				$jml = $jml+1;
				$hpp= (int)$row->hpp;
				$stok=(int)$row->stokfisik;
				$stok_selisih=(int)$row->selisihstok;
				$nilai=$hpp * $stok;
				$nilai_selisih=$hpp * $stok_selisih;

				// Set Content
	            $activeSheet->setCellValue("B$x", $jml);
	            $activeSheet->setCellValue("C$x", $row->nama);
	            $activeSheet->setCellValue("D$x", $row->namatipe);
	            $activeSheet->setCellValue("E$x", ($row->status<>2?"SESUAI":"TIDAK SESUAI"));
	            $activeSheet->setCellValue("F$x", $row->stoksistem);
	            $activeSheet->setCellValue("G$x", $row->stokfisik);
	            $activeSheet->setCellValue("H$x", $row->selisihstok);
	            $activeSheet->setCellValue("I$x", $row->catatan);
	            $activeSheet->setCellValue("J$x", $row->hpp);
				// $nilai=$row->hpp*$row->stokfisik;
				
	            $activeSheet->setCellValue("K$x", $nilai);
	            $activeSheet->setCellValue("L$x", $nilai_selisih);
	            $activeSheet->setCellValue("M$x", ($row->namauser_update)?$row->namauser_update.' '.HumanDateLong($row->datetime_update):$row->namauser_insert.' '.HumanDateLong($row->datetime_insert));
				
				$activeSheet->getStyle("D$x:E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("M$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:H$x")->getNumberFormat()->setFormatCode('#,##0');
				$activeSheet->getStyle("J$x:L$x")->getNumberFormat()->setFormatCode('#,##0');
				// $activeSheet->getStyle("H$x:M$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$activeSheet->getStyle("B$x:M$x")->applyFromArray(
					array(
						'borders' => array(
							'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
							)
						)
					)
				);
			}
		}

	    $x = $x+2;
		$awal=$x;
		$activeSheet->setCellValue("B$x", "REKAPITULASI");
    	$activeSheet->mergeCells("B$x:M$x");
		$jml=0;
		foreach ($recap as $row) {
			$x = $x+1;$jml = $jml+1;
			$activeSheet->setCellValue("B$x", $jml);
			$activeSheet->setCellValue("C$x", $row->nama_tipe);
			$activeSheet->setCellValue("D$x", $row->total);
			$activeSheet->getStyle("D$x")->getNumberFormat()->setFormatCode('#,##0');
			$activeSheet->setCellValue("E$x",'    ('.terbilang($row->total).')');
			$activeSheet->mergeCells("E$x:M$x");
		}
		$activeSheet->getStyle("B$awal:M$x")->applyFromArray(
        array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                ),
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Calibri'
				)
            )
        );
		 // $activeSheet->mergeCells('B$x:M$x');
	    $x = $x+2;
    	// Set Info Print
    	$activeSheet->setCellValue("E$x", 'Sistem Informasi RSKB Halmahera - Tanggal Cetak '.date("d-m-Y - H:i:s"));
    	$activeSheet->getStyle("E$x")->getFont()->setSize(11);
    	$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$activeSheet->mergeCells("E$x:M$x");
    	$x = $x+2;
		
    	// Set Auto Width Column
    	$activeSheet->getColumnDimension("A")->setWidth(5);
    	$activeSheet->getColumnDimension("B")->setAutoSize(true);
    	$activeSheet->getColumnDimension("C")->setAutoSize(true);
    	$activeSheet->getColumnDimension("D")->setAutoSize(true);
    	$activeSheet->getColumnDimension("E")->setAutoSize(true);
    	$activeSheet->getColumnDimension("F")->setAutoSize(true);
    	$activeSheet->getColumnDimension("G")->setAutoSize(true);
    	$activeSheet->getColumnDimension("H")->setAutoSize(true);
    	$activeSheet->getColumnDimension("I")->setAutoSize(true);
    	$activeSheet->getColumnDimension("J")->setAutoSize(true);
    	$activeSheet->getColumnDimension("K")->setAutoSize(true);
    	$activeSheet->getColumnDimension("L")->setAutoSize(true);
    	$activeSheet->getColumnDimension("M")->setAutoSize(true);

    	header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    	header('Content-Disposition: attachment;filename="'.$data['nostockopname'].' Stock Opname Report '.$data['nama_unit'].'.xls"');
    	header('Cache-Control: max-age=0');

    	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    	$objWriter->save('php://output');
    }
	private function export_laporan_xls($record,$periode)
    {
       
    }
	public function format_data($id='1')
    {
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();

        $data = $this->model->get_so($id);
        $data['nama_periode'] = get_bulan(substr($data['periode'],4,2)).' '.substr($data['periode'],0,4);
		// $per=
		// print_r($data);exit();

		// print_r($data);exit();
        $data['details'] = $this->model->list_format($data['idunitpelayanan']);
		// $data['recap']=$this->model->recap_detail($id);
		$data=array_merge($data,backend_info());
        $html = $this->load->view('Tstockopname/format_data', $data, true);
        $html = $this->parser->parse_string($html,$data);
		// print_r($html);exit();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Rincian Global.pdf', array("Attachment"=>0));
    }
	public function history() {
        $data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('274'))){
			$data=array();
			$data['gudangtipe'] = '#';
			$data['iddariunit'] = '#';
			$data['periode'] ="#";
			$data['tgl_so'] = date('d-m-Y');
			// print_r($data);exit();
			$data['list_unitpelayanan'] = $this->unitpelayanan->getDefaultUnitPelayananUser();
			// $data['list_tipe'] = $this->model->list_tipe();
			$data['list_periode'] = $this->model->list_periode();
			$data['tanggaldari'] = '';
			$data['tanggalsampai'] = '';
			$data['status_so'] = '#';
			$data['error'] = '';
			$data['title'] = 'History';
			$data['content'] = 'Tstockopname/history';
			$data['tombol'] = 'history';
			$data['breadcrum'] = [
				["RSKB Halmahera",'#'],
				["Stock Opname",'tstockopname/index'],
				["History",'#']
			];

			$data['nostockopname'] = $this->model->get_no();
			// print_r($data['list_unitpelayanan']);exit();
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);

		}else{
			redirect('page404');
		}
    }
	function list_tipe($val) {
        $result=$this->model->list_tipe($val);
        echo json_encode($result);
    }
	function list_periode($val='') {
		// $tgl_sekarang=date("Y-m-d", strtotime('2020-01-02'));
		// $forOdNextMonth= date('Ym', strtotime("-1 month", strtotime($tgl_sekarang)));
		// print_r($forOdNextMonth);exit();
		if ($val==''){
			if (date('d')<6){
				$bulan_sebelumnya= date('Ym', strtotime("-1 month", strtotime(date('Y-m-d'))));
				$result=array(
					array('id'=>date('Ym'),'text'=>get_bulan(date('m')).' '.date('Y')),
					array('id'=>$bulan_sebelumnya,'text'=>get_bulan(substr($bulan_sebelumnya,4,2)).' '.substr($bulan_sebelumnya,0,4))

				);
			}else{
				$result=array(array('id'=>date('Ym'),'text'=>get_bulan(date('m')).' '.date('Y')));
			}
		}else{
			$row=$this->model->cari_periode($val);
			$result=array(array('id'=>$row,'text'=>get_bulan(substr($row,4,2)).' '.substr($row,0,4)));
		}
        // $result=$this->model->list_tipe($val);
        echo json_encode($result);
    }
	function list_kategori($val) {
        $result=$this->model->list_kategori($val);
        echo json_encode($result);
    }
	public function _list($data) {
        $this->session->set_userdata($data);
        $data['error'] = '';
        $data['title'] = 'Stock Opname';
        $data['content'] = 'Tstockopname/index';
        $data['breadcrum'] = [
            ["RSKB Halmahera",'#'],
            ["Stock Opname",'tstockopname/index'],
            ["List",'#']
        ];

        $data['list_unitpelayanan'] = $this->unitpelayanan->getDefaultUnitPelayananUser();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
    public function filter() {
        $data = [
            'tanggaldari' => $this->input->post('tanggaldari'),
            'tanggalsampai' => $this->input->post('tanggalsampai'),
            'idunit' => $this->input->post('idunit'),
            'codeunit' => $this->input->post('codeunit'),
            'tipeopname' => $this->input->post('tipeopname'),
        ];
        $this->_list($data);
    }
    public function ajax_list() {
        $tanggaldari = $this->session->userdata('tanggaldari');
        $tanggalsampai = $this->session->userdata('tanggalsampai');
        $idunit = $this->session->userdata('idunit');
        $codeunit = $this->session->userdata('codeunit');
        $tipeopname = $this->session->userdata('tipeopname');
        $from="(
            SELECT
                s.*,
                u.nama as 'namaunit',
                (CASE
                WHEN s.idtipe = 1 THEN 'Alkes'
                WHEN s.idtipe = 2 THEN 'Implan'
                WHEN s.idtipe = 3 THEN 'Obat'
                WHEN s.idtipe = 4 THEN 'Logistik'
                END) as 'tipeopname'
            FROM tstockopname s
            JOIN munitpelayanan u ON u.id=s.idunitpelayanan
            WHERE s.id IS NOT NULL ";
        if ($tanggaldari!="") {
            $from.=" AND s.periode >='".$tanggaldari."'";
        }
        if ($tanggalsampai!="") {
            $from.=" AND s.periode <='".$tanggalsampai."'";
        }
        if ($idunit!="") {
            $from.=" AND s.idunitpelayanan='".$idunit."'";
        }
        if ($codeunit!="") {
            $from.=" AND s.nostockopname LIKE '%".$codeunit."%'";
        }
        if ($tipeopname!="") {
            $from.=" AND s.idtipe='".$tipeopname."'";
        }
        $from.=") tbl";
        $this->load->library("datatables");
        $this->datatables->from($from);
        return print_r($this->datatables->generate());
    }
    public function create() {
        $data['error'] 			= '';
        $data['title'] 			= 'Tambah Stock Opname';
        $data['content'] 		= 'Tstockopname/create';
        $data['breadcrum']		= [["RSKB Halmahera",'#'],
            ["Stock Opname",'#'],
            ["Create",'']];

        $data['list_unitpelayanan'] = $this->unitpelayanan->getDefaultUnitPelayananUser();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	 public function create_so() {
        $iddariunit     = $this->input->post('iddariunit');
        $bulan     = $this->input->post('bulan');
        $tgl_input     = date("Y-m-d", strtotime($this->input->post('tgl_input')));
		// $tgl_sekarang=date("Y-m-d", strtotime('2020-01-02'));
		$periode=$bulan;
		$user=$this->session->userdata('user_name');
		// $periode=$tahun.$bulan;
	    $nostockopname = $this->model->get_no();

		$q="INSERT INTO tstockopname VALUES(null,NOW(),'$periode','$nostockopname',$iddariunit,'1','0','1','$tgl_input','$user',null,null)";

		$result = $this->db->query($q);

		if ($result) {
		  return true;
		}else{
			return false;
		}
    }
	public function set_tidak_sesuai() {

        $id_so     = $this->input->post('id_so');
        $idstok     = $this->input->post('idstok');
        $idunit     = $this->input->post('idunit');
        $idtipe     = $this->input->post('idtipe');
        $idbarang     = $this->input->post('idbarang');
        $catatan     = $this->input->post('catatan');
        $stokfisik     = RemoveComma($this->input->post('stokfisik'));

		$q="UPDATE tstockopname_detail SET stokfisik='$stokfisik',catatan='$catatan'
		WHERE so_id='$id_so' AND idstok='$idstok'  AND idunitpelayanan='$idunit'  AND idtipe='$idtipe'  AND idbarang='$idbarang'  ";

		$result = $this->db->query($q);

		if ($result) {
			return true;
		}else{
			return false;
		}
    }
	public function set_semua_sesuai() {

        $id_so     = $this->input->post('id_so');
        $idstok     = $this->input->post('idstok');
        $idunit     = $this->input->post('idunit');
        $idtipe     = $this->input->post('idtipe');
        $idbarang     = $this->input->post('idbarang');
        $catatan     = $this->input->post('catatan');
        $stokfisik     = RemoveComma($this->input->post('stokfisik'));

		$q="UPDATE tstockopname_detail SET status='1'
		WHERE so_id='$id_so' AND idstok='$idstok'  AND idunitpelayanan='$idunit'  AND idtipe='$idtipe'  AND idbarang='$idbarang'  ";

		$result = $this->db->query($q);

		if ($result) {
			return true;
		}else{
			return false;
		}
    }
	public function set_sesuai() {

        $id_so     = $this->input->post('id_so');
        $idstok     = $this->input->post('idstok');
        $idunit     = $this->input->post('idunit');
        $idtipe     = $this->input->post('idtipe');
        $idbarang     = $this->input->post('idbarang');
        $catatan     = $this->input->post('catatan');
        $stokfisik     = RemoveComma($this->input->post('stokfisik'));

		$q="UPDATE tstockopname_detail SET status='1', stokfisik='$stokfisik'
		WHERE so_id='$id_so' AND idstok='$idstok'  AND idunitpelayanan='$idunit'  AND idtipe='$idtipe'  AND idbarang='$idbarang'  ";

		$result = $this->db->query($q);

		if ($result) {
			return true;
		}else{
			return false;
		}
    }
	public function set_reset() {

        $id_so     = $this->input->post('id_so');
        $idstok     = $this->input->post('idstok');
        $idunit     = $this->input->post('idunit');
        $idtipe     = $this->input->post('idtipe');
        $idbarang     = $this->input->post('idbarang');
        $catatan     = $this->input->post('catatan');
        $stokfisik     = RemoveComma($this->input->post('stokfisik'));

		$q="UPDATE tstockopname_detail SET status='0',selisihstok='0', stokfisik='$stokfisik',catatan=''
		WHERE so_id='$id_so' AND idstok='$idstok'  AND idunitpelayanan='$idunit'  AND idtipe='$idtipe'  AND idbarang='$idbarang'  ";

		$result = $this->db->query($q);

		if ($result) {
			return true;
		}else{
			return false;
		}
    }
	public function pause_so() {
        $id     = $this->input->post('id');

		$q="UPDATE tstockopname SET status='2' WHERE id='$id'";

		$result = $this->db->query($q);

		if ($result) {
		  return true;
		}else{
			return false;
		}
    }
	public function resume() {
        $id     = $this->input->post('id');

		$q="UPDATE tstockopname SET status='1' WHERE id='$id'";

		$result = $this->db->query($q);

		if ($result) {
		  return true;
		}else{
			return false;
		}
    }
	public function selesai_so() {
        $id     = $this->input->post('id');
        $user     = $this->session->userdata('user_name');
        $tgl=date('Y-m-d H:i:s');
		$q="UPDATE tstockopname SET status='3',user_finish='$user',tgl_finish='$tgl' WHERE id='$id'";

		$result = $this->db->query($q);

		if ($result) {
		  return true;
		}else{
			return false;
		}
    }
	function cari_so($id)
	{
		$arr =$this->model->cari_so($id);
		$this->output->set_output(json_encode($arr));
	}

    public function create_ajax_list() {
        $idunit=$this->input->post('idunit');
        $idtipe = $this->input->post('idtipe');

        $from="(SELECT mgudang_stok.*,
            view_barang.nama AS namabarang,
            view_barang.namatipe AS tipebarang,
            tstockopname_detail.stokfisik AS stokfisik,
            tstockopname_detail.selisihstok AS selisihstok,
            tstockopname_detail.catatan AS catatan,
            view_barang.hargabeli as hpp";
        $from.= ' FROM mgudang_stok';
        $from.= ' JOIN view_barang ON view_barang.id=mgudang_stok.idbarang AND mgudang_stok.idtipe=view_barang.idtipe';
        $from.= ' LEFT JOIN tstockopname_detail ON tstockopname_detail.idunitpelayanan = mgudang_stok.idunitpelayanan AND tstockopname_detail.idbarang = mgudang_stok.idbarang AND tstockopname_detail.idtipe = mgudang_stok.idtipe';
        $from.= ' LEFT JOIN tstockopname ON tstockopname.id = tstockopname_detail.idstockopname';

        $from.=' WHERE mgudang_stok.id IS NOT NULL';
        if ('' != $idunit) {
            $from.=" AND mgudang_stok.idunitpelayanan = '".$idunit."'";
        } else {
            $from.=" AND mgudang_stok.idunitpelayanan =-1";
        }
        if ('' != $idtipe) {
            $from.=" AND mgudang_stok.idtipe = '".$idtipe."'";
        }
        $from.=' ORDER BY view_barang.nama ASC) tbl';
        $this->load->library('datatables');
        $this->datatables->from($from);
        return print_r($this->datatables->generate());
    }

    public function save($mode="") {
        if ($mode=="draft") {
            $this->model->saveData(2);
        } else {
            $this->model->saveData(1);
        }

        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah disimpan.');
        redirect('tstockopname/index', 'location');
    }

    public function edit($id) {
        $data['error'] 			= '';
        $data['title'] 			= 'Sunting Stock Opname';
        $data['content'] 		= 'Tstockopname/edit';
        $data['breadcrum']		= [["RSKB Halmahera",'#'],
            ["Stock Opname",'#'],
            ["Sunting",'']];

        $data['data'] = $this->model->edit($id);
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
    public function view($id) {
        $data['error'] 			= '';
        $data['title'] 			= 'Stock Opname';
        $data['content'] 		= 'Tstockopname/view';
        $data['breadcrum']		= [["RSKB Halmahera",'#'],
            ["Stock Opname",'#'],
            ["Lihat",'']];

        $data['data'] = $this->model->edit($id);
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
    public function update($mode="") {
        if ($mode=="draft") {
            $this->model->update(2);
        } else {
            $this->model->update(1);
        }

        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah disunting.');
        redirect('tstockopname/index', 'location');
    }
    public function delete($id){
        $this->model->delete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah dihapus.');
        redirect('tstockopname/index', 'location');
    }
    public function getIndex($uri) {
        $periode = date("Y").'-'.date("m");
        if ($this->session->userdata('bulan') != null && $this->session->userdata('tahun') != null) {
            $periode = $this->session->userdata('tahun').'-'.$this->session->userdata('bulan');
        }

        $this->select = ['mgudang_stok.*', '(CASE
                WHEN mgudang_stok.idtipe = 1 THEN
                    mdata_alkes.nama
                WHEN mgudang_stok.idtipe = 2 THEN
                    mdata_implan.nama
                WHEN mgudang_stok.idtipe = 3 THEN
                    mdata_obat.nama
                WHEN mgudang_stok.idtipe = 4 THEN
                    mdata_logistik.nama
                END) AS namabarang',
            'tstockopname_detail.stokfisik AS stokfisik_new',
            'tstockopname_detail.selisihstok AS selisihstok_new',
            'tstockopname_detail.catatan AS catatan_new'];
        $this->from   = 'mgudang_stok';
        $this->join   = [
            ['mdata_alkes', 'mdata_alkes.id = mgudang_stok.idbarang AND mgudang_stok.idtipe = 1', 'LEFT'],
            ['mdata_implan', 'mdata_implan.id = mgudang_stok.idbarang AND mgudang_stok.idtipe = 2', 'LEFT'],
            ['mdata_obat', 'mdata_obat.id = mgudang_stok.idbarang AND mgudang_stok.idtipe = 3', 'LEFT'],
            ['mdata_logistik', 'mdata_logistik.id = mgudang_stok.idbarang AND mgudang_stok.idtipe = 4', 'LEFT'],
            ['tstockopname_detail', 'tstockopname_detail.idunitpelayanan = mgudang_stok.idunitpelayanan AND tstockopname_detail.idbarang = mgudang_stok.idbarang AND tstockopname_detail.idtipe = mgudang_stok.idtipe', 'LEFT'],
            ['tstockopname', 'tstockopname.id = tstockopname_detail.idstockopname AND tstockopname.periode = "'.$periode.'"', 'LEFT']
        ];

        $this->where  = [];
        if ($uri != 'filter') {
            $this->where  = [
                'mgudang_stok.idunitpelayanan' => '1',
                'mgudang_stok.idtipe' => '1',
                'mgudang_stok.status' => '1'
            ];
        } else {
            if ($this->session->userdata('idunit') != null) {
                $this->where = array_merge($this->where, ['mgudang_stok.idunitpelayanan' => $this->session->userdata('idunit')]);
            }
            if ($this->session->userdata('idtipe') != null) {
                $this->where = array_merge($this->where, ['mgudang_stok.idtipe' => $this->session->userdata('idtipe')]);
            }
            $this->where = array_merge($this->where, ['mgudang_stok.status' => '1']);
        }

        $this->order  = [
            'mgudang_stok.id' => 'DESC'
        ];
        $this->group  = [];

        $this->column_search   = ['mgudang_stok.namabarang'];
        $this->column_order    = ['mgudang_stok.namabarang'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = [];

            $stokfisik = ($r->stokfisik_new ? $r->stokfisik_new : $r->stok);
            $selisihstok = ($r->selisihstok_new ? $r->selisihstok_new : 0);
            $catatan = ($r->catatan_new ? $r->catatan_new : '');

            $row[] = $r->idbarang;
            $row[] = $r->namabarang;
            $row[] = number_format($r->stok);
            $row[] = '<input class="form-control input-md number stokfisik" type="text" value="'.number_format($stokfisik).'">';
            $row[] = number_format($selisihstok);
            $row[] = '<textarea class="form-control catatan">'.$catatan.'</textarea>';

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        ];
        echo json_encode($output);
    }
    public function printopname($id,$mode="direct"){
        $data['data'] = $this->model->edit($id);
        $data['recap']=$this->model->recap_detail($id);
        $opname=(object)$data['data']['opname'];

        if ($mode=="pdf") {
            $dompdf = new Dompdf();
            $html = $this->load->view('Tstockopname/print', $data, true);
            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'landscape');
            $dompdf->render();
            $dompdf->stream('stockopname.pdf', array("Attachment"=>0));
        }else{
            $data['error'] 			= '';
            $data['title'] 			= 'Print '.$opname->nostockopname;
            $data['content'] 		= 'Tstockopname/print';
            $data['breadcrum']		= [["RSKB Halmahera",'#'],
                ["Stock Opname",'tstockopname'],
                ["Sunting",'']];

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        }
    }
	function getBarang()
    {
		$iddariunit     = $this->input->post('idunit');
		$id     		= $this->input->post('id');
		$idtipe     	= $this->input->post('idtipe');
		$nama_barang    = $this->input->post('nama_barang');
		$idkategori    = $this->input->post('idkategori');
		$iduser=$this->session->userdata('user_id');

		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
    		$this->join 	= array();
    		$this->where  = array();
		$where='';
        if ($idtipe  != '#'){
			$where .=" AND S.idtipe='$idtipe'";
		}else{
			$array_idtipe=$this->model->list_tipe2($iddariunit);
			$where .=" AND S.idtipe IN (".$array_idtipe.")";
			// $where .=" AND S.idtipe IN( SELECT M.id FROM munitpelayanan_tipebarang H
					// LEFT JOIN mdata_tipebarang M ON M.id=H.idtipe
					// WHERE H.idunitpelayanan='$iddariunit'
					// AND H.idtipe IN (
					// SELECT T.idtipe from musers_tipebarang T
					// WHERE T.iduser='$iduser')
					// )";
		}
		if ($idkategori  != '#'){
			// $rowkat=$this->Mdata_obat_model->get_array_kategori($idkategori);

			$where .=" AND B.idkategori IN (
							SELECT id FROM (
									SELECT *FROM view_kategori_alkes
									UNION
									SELECT *FROM view_kategori_implan
									UNION
									SELECT *FROM view_kategori_obat
									UNION
									SELECT *FROM view_kategori_logistik
							) T1
							where T1.path like '$idkategori%'
			)";
		}
		if ($nama_barang  != ''){
			$where .=" AND B.nama LIKE '%".$nama_barang."%' ";
		}
			$from="(SELECT S.id,S.idunitpelayanan,S.idbarang,S.idtipe,B.nama,
			B.namatipe,SOD.namauser_insert,
			S.stok,SOD.stoksistem,SOD.stokfisik,B.hargabeli as hpp,SOD.catatan,SOD.selisihstok,
					CASE WHEN SOD.idstok IS NULL THEN '0' ELSE '1' END as st_lihat,SOD.status,
					SOD.so_id
					from mgudang_stok S
					LEFT JOIN tstockopname_detail SOD ON SOD.idstok=S.id AND SOD.so_id='$id'
					INNER JOIN view_barang B ON B.id=S.idbarang AND B.idtipe=S.idtipe
					WHERE S.idunitpelayanan='$iddariunit' AND S.`status`='1' ".$where."
					ORDER BY B.nama) as tbl";

    		$this->order  = array();
    		$this->group  = array();
    		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = ($r->st_lihat)?$r->status:'0';
            $row[] = $no;
            $row[] = $r->nama;
            $row[] = $r->namatipe;
            $row[] = ($r->stok < 0) ? text_danger(number_format($r->stok,0)):number_format($r->stok,0);
			if ($r->st_lihat=='1'){
				$row[] = ($r->stoksistem < 0) ? text_danger(number_format($r->stoksistem,0)):number_format($r->stoksistem,0);
				$row[] = ($r->stokfisik < 0) ? text_danger(number_format($r->stokfisik,0)):number_format($r->stokfisik,0);
				$row[] =($r->selisihstok < 0) ? text_danger(number_format($r->selisihstok,0)):number_format($r->selisihstok,0);
			}else{
				$row[] = ($r->stok < 0) ? text_danger(number_format($r->stok,0)):number_format($r->stok,0);
				$row[] = $r->stok;
				$row[] = 0;
			}

            $row[] = $r->catatan;
			$status="";
			if ($r->status=='1'){
				$status .='<button class="btn btn-xs btn-success" title="Sesuai"><i class="fa fa-check"></i> Sesuai</button>';
			}else if ($r->status=='2'){
				$status .='<button class="btn btn-xs btn-danger" title="Sesuai"><i class="fa fa-exchange"></i> Tidak Sesuai</button>';

			}

            $row[] =number_format($r->hpp,0);
			if ($r->st_lihat=='1'){
				$row[] = number_format($r->hpp * $r->stokfisik,0);
				$row[] = number_format($r->hpp * $r->selisihstok,0);
				$row[] = '<div class="btn-group"><button class="btn btn-xs btn-info push-5-r push-10" type="button" title="DiLihat / Cek Oleh : '.$r->namauser_insert.'"><i class="fa fa-eye"></i></button>'.$status.'</div>';
			}else{
				$row[] = number_format($r->hpp * $r->stok,0);
				$row[] = 0;
				$row[] = '<div class="btn-group"><button class="btn btn-xs btn-info push-5-r push-10" type="button" title="Belum Pernah Dilihat"><i class="fa fa-eye-slash"></i></button>'.$status.'</div>';
			}
			$aksi       = '<div class="btn-group">';
			 if (UserAccesForm($user_acces_form,array('273'))){
				if ($r->status=='0'){
					$aksi 		.= '<button class="btn btn-xs btn-info sesuai" title="Click Jika Sesuai"><i class="fa fa-check"></i></button>';
					$aksi 		.= '<button class="btn btn-xs btn-danger tidak" title="Click Tidak Sesuai"><i class="fa fa-close"></i></button>';


				}else{
					if ($r->st_lihat=='1'){
						$aksi 		.= '<button class="btn btn-xs btn-danger reset" title="Reset"><i class="fa fa-refresh"></i> Reset</button>';
					}else{
						$aksi 		.= '<button class="btn btn-xs btn-info sesuai" title="Click Jika Sesuai"><i class="fa fa-check"></i></button>';
						$aksi 		.= '<button class="btn btn-xs btn-danger tidak" title="Click Tidak Sesuai"><i class="fa fa-close"></i></button>';

					}
				}
			 }
			 if (UserAccesForm($user_acces_form,array('272'))){
			$aksi 		.= '<a target="_blank" href="'.site_url().'lgudang_stok/kartustok/'.$r->idtipe.'/'.$r->idbarang.'/'.$r->idunitpelayanan.'"  class="btn btn-xs btn-success" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';
			 }
			$aksi.='</div>';
			$row[] = $aksi;
			$row[] = $id;//14
			$row[] = $r->id;//15
			$row[] = $r->idunitpelayanan;//16
			$row[] = $r->idtipe;//17
			$row[] = $r->idbarang;//18
            $data[] = $row;
			$q="INSERT INTO tstockopname_detail(`so_id`,`idstok`,`idunitpelayanan`,`idtipe`,`idbarang`,`stoksistem`,`stokfisik`,`selisihstok`,`catatan`,`status`,`hpp`,`namauser_update`,`namauser_insert`,`datetime_update`,`datetime_insert`,`tgl_input`)
					VALUES('$id','".$r->id."','".$r->idunitpelayanan."','".$r->idtipe."','".$r->idbarang."','".$r->stok."','".$r->stok."','0','-','0','".$r->hpp."'
					,'-','".$this->session->userdata('user_name')."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','".date('Y-m-d')."')
				ON DUPLICATE KEY UPDATE
				namauser_update = '".$this->session->userdata('user_name')."',
				datetime_update = '".date('Y-m-d H:i:s')."' ";
			$this->db->query($q);
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	function getBarangRekap()
    {
		$id     		= $this->input->post('id');
		$iddariunit     		= $this->input->post('iddariunit');
		$idtipe     	= $this->input->post('idtipe');
		$nama_barang    = $this->input->post('nama_barang');
		$idkategori    = $this->input->post('idkategori');
		$iduser=$this->session->userdata('user_id');

		// $data_user=get_acces();
		// $user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
    		$this->join 	= array();
    		$this->where  = array();
		$where='';
        if ($idtipe  != '#'){
			$where .=" AND SOD.idtipe='$idtipe'";
		}else{
			$array_idtipe=$this->model->list_tipe2($iddariunit);
			$where .=" AND SOD.idtipe IN (".$array_idtipe.")";
			
		}
		if ($idkategori  != '#'){
			// $rowkat=$this->Mdata_obat_model->get_array_kategori($idkategori);

			$where .=" AND B.idkategori IN (
							SELECT id FROM (
									SELECT *FROM view_kategori_alkes
									UNION
									SELECT *FROM view_kategori_implan
									UNION
									SELECT *FROM view_kategori_obat
									UNION
									SELECT *FROM view_kategori_logistik
							) T1
							where T1.path like '$idkategori%'
			)";
		}
		if ($nama_barang  != ''){
			$where .=" AND B.nama LIKE '%".$nama_barang."%' ";
		}
		// $where='';

			$from="(SELECT SOD.so_id,SOD.idunitpelayanan,SOD.idbarang,SOD.idtipe,B.nama,
			B.namatipe,SOD.namauser_insert,SOD.namauser_update,
			SOD.stoksistem,SOD.stokfisik,B.hargabeli as hpp,SOD.catatan,SOD.selisihstok
					from tstockopname_detail SOD
					INNER JOIN view_barang_all B ON B.id=SOD.idbarang AND B.idtipe=SOD.idtipe
					WHERE SOD.so_id='$id' ".$where."
					ORDER BY B.nama) as tbl";

    		$this->order  = array();
    		$this->group  = array();
    		$this->from   = $from;
// print_r($from);exit();
        $this->column_search   = array('nama','namatipe');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $r->so_id;
            $row[] = $no;
            $row[] = $r->nama;
            $row[] = $r->namatipe;

			$row[] = ($r->stoksistem < 0) ? text_danger(number_format($r->stoksistem,0)):number_format($r->stoksistem,0);
			$row[] = ($r->stokfisik < 0) ? text_danger(number_format($r->stokfisik,0)):number_format($r->stokfisik,0);
			$row[] =($r->selisihstok < 0) ? text_danger(number_format($r->selisihstok,0)):number_format($r->selisihstok,0);

            $row[] = text_warning($r->catatan);
            $row[] =number_format($r->hpp,0);

			$row[] = number_format($r->hpp * $r->stokfisik,0);
			$row[] = number_format($r->hpp * $r->selisihstok,0);
			$row[] = '<button class="btn btn-xs btn-info push-5-r push-10" type="button" title="Update By : '.$r->namauser_insert.'"><i class="fa fa-eye"></i></button>';

			$aksi       = '<div class="btn-group">';

			// $aksi 		.= '<button class="btn btn-xs btn-success ks" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</button>';
			$aksi 		.= '<a target="_blank" href="'.site_url().'lgudang_stok/kartustok/'.$r->idtipe.'/'.$r->idbarang.'/'.$r->idunitpelayanan.'"  class="btn btn-xs btn-success" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';
            $aksi.='</div>';
			$row[] = $aksi;
			$row[] = $r->so_id;
			$row[] = $r->idunitpelayanan;
			$row[] = $r->idtipe;
			$row[] = $r->idbarang;
            $data[] = $row;

        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	function getKS()
    {
		$iddariunit     = $this->input->post('idunit');
		$idbarang     		= $this->input->post('idbarang');
		$idtipe     	= $this->input->post('idtipe');
		$tanggaldari     	= $this->input->post('tanggaldari');
		$tanggalsampai    	= $this->input->post('tanggalsampai');
		$iduser=$this->session->userdata('user_id');

		// $data_user=get_acces();
		// $user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
    		$this->join 	= array();
    		$this->where  = array();
			$where='';
			if ('' != $tanggaldari) {
				$where .= " AND (DATE(K.tanggal) >='".YMDFormat($tanggaldari)."'";
			}
			if ('' != $tanggalsampai) {
				$where .= " AND DATE(K.tanggal) <='".YMDFormat($tanggalsampai)."')";
			}
			$from="(SELECT
					K.tanggal,K.notransaksi,K.keterangan,B.namatipe,B.nama,K.penerimaan,K.pengeluaran
					FROM lkartustok K
					INNER JOIN view_barang B ON B.id=K.idbarang AND B.idtipe=K.idtipe
					WHERE K.idunitpelayanan='$iddariunit' AND K.idtipe='$idtipe' AND K.idbarang='$idbarang' ".$where." ORDER BY K.id DESC) as tbl";

    		$this->order  = array();
    		$this->group  = array();
    		$this->from   = $from;
// print_r($from);exit();
        $this->column_search   = array('notransaksi','keterangan');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] =HumanDate($r->tanggal);
            $row[] = $r->notransaksi;
            $row[] = $r->keterangan;
            $row[] =($r->penerimaan)?number_format($r->penerimaan,0):'';
            $row[] =($r->pengeluaran)?number_format($r->pengeluaran,0):'';
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	function getHistory()
    {
		$iddariunit     = $this->input->post('iddariunit');
		$periode     		= $this->input->post('periode');
		$tanggaldari     	= $this->input->post('tanggaldari');
		$tanggalsampai    = $this->input->post('tanggalsampai');
		$status_so    = $this->input->post('status_so');
		$iduser=$this->session->userdata('user_id');

		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
    		$this->join 	= array();
    		$this->where  = array();
		$where='';
        if ($iddariunit  != '#'){
			$where .=" AND H.idunitpelayanan='$iddariunit'";
		}else{
			$where .=" AND H.idunitpelayanan IN(
									SELECT U.idunitpelayanan FROM munitpelayanan_user U
									WHERE U.userid='$iduser'
					)";
		}
		if ($periode  != '#'){
			$where .=" AND H.periode ='$periode'";
		}
		if ($status_so  != '#'){
			$where .=" AND H.status ='$status_so'";
		}
		if ('' != $tanggaldari) {
            $where .= " AND (DATE(H.tgl_so) >='".YMDFormat($tanggaldari)."'";
        }
        if ('' != $tanggalsampai) {
            $where .= " AND DATE(H.tgl_so) <='".YMDFormat($tanggalsampai)."')";
        }
			$from="(SELECT H.id,H.periode,H.tgl_so,H.nostockopname,H.idunitpelayanan,M.nama as namaunit,H.`status`,
					CASE WHEN H.`status`='1' THEN 'ON PROGRESS' WHEN H.`status`='2' THEN 'PAUSE' ELSE 'SELESAI' END status_nama
					from tstockopname H
					LEFT JOIN munitpelayanan M ON H.idunitpelayanan=M.id
					WHERE H.nostockopname <> '' ".$where.") as tbl";
			// print_r($from);exit();
    		$this->order  = array();
    		$this->group  = array();
    		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $r->id;
            $row[] = $no;
            $row[] = HumanDateLong($r->tgl_so);
            $row[] = $r->nostockopname;
            $row[] = $r->namaunit;
			if ($r->status=='1'){
				$row[] =text_info($r->status_nama);

			}elseif ($r->status=='2'){
				$row[] =text_warning($r->status_nama);
			}else{
				$row[] =text_success($r->status_nama);			}

			$aksi       = '<div class="btn-group">';
			if ($r->status=='2'){
				if (UserAccesForm($user_acces_form,array('276'))){
				$aksi 		.= '<button class="btn btn-xs btn-success resume" title="Resume / Lanjutkan"><i class="fa fa-arrow-right"></i> RESUME </button>';
				}
			}elseif ($r->status=='3'){
				if (UserAccesForm($user_acces_form,array('275'))){
				$aksi 		.= '<a  href="'.site_url().'tstockopname/print_data/'.$r->id.'" target="_blank" class="btn btn-xs btn-info print" title="Print"><i class="fa fa-print"></i> Print</a>';
				}
				if (UserAccesForm($user_acces_form,array('277'))){
				$aksi 		.= '<a href="'.site_url().'tstockopname/rekap/'.$r->id.'" class="btn btn-xs btn-danger reply" title="History"><i class="fa fa-history"></i></a>';
				}
			}
            $aksi.='</div>';
			$row[] = $aksi;
			$row[] = $r->idunitpelayanan;

            $data[] = $row;

        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
}
