<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_pendapatan_jual_rajal extends CI_Controller {

	/**
	 * Setting Jurnal Penjualan Obat Rawat Jalan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_pendapatan_jual_rajal_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_pendapatan_jual_rajal_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_pendapatan_jual_rajal_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Penjualan Obat Rawat Jalan';
		$data['content'] 		= 'Msetting_jurnal_pendapatan_jual_rajal/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Penjualan Obat Rawat Jalan",'#'),
									    			array("List",'mlogic_rajal')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function list_pegawai(){
		$idtipe=$this->input->post('idtipe');
		$idkategori=$this->input->post('idkategori');
		$where='';
		if ($idkategori !='#'){
			$where .=" AND M.idkategori='$idkategori'";
		}
		if ($idtipe=='1'){
			$q="SELECT * FROM mdokter M WHERE M.status='1' ".$where." ORDER BY M.nama ASC";
		}else{
			$q="SELECT * FROM mpegawai M  M WHERE M.status='1' ".$where." ORDER BY M.nama ASC";
		}
		
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function list_poli_kelas($idtipe){
		if ($idtipe=='1'){//Poli
			$q="SELECT * FROM mpoliklinik M WHERE M.id !='22' ORDER BY M.nama ASC";
		}elseif ($idtipe=='2'){ // RANAP
			$q="SELECT * FROM mpoliklinik M WHERE M.id ='22' ORDER BY M.nama ASC";
			// $q="SELECT * FROM mkelas M ORDER BY M.nama ASC";			
		}
		$opsi='';
		if ($idtipe !='#'){
				
			$row=$this->db->query($q)->result();
			foreach($row as $r){
				$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
			}
			
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	
	
	//Penjualan Obat Rawat Jalan
	function load_rajal()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.*
					,CASE WHEN H.asalrujukan='1' THEN MP.nama 
					 WHEN H.asalrujukan='2' THEN MP.nama 
					 WHEN H.asalrujukan='3' THEN MK.nama END as nama_poli
					 ,K.nama as kategori,T.nama_tipe,B.nama as namabarang
					 ,GH1.nama as nama_group_pembelian
					 ,GH2.nama as nama_group_diskon
					FROM msetting_jurnal_pendapatan_jual_rajal H
					LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli_kelas AND H.asalrujukan IN(1,2)
					LEFT JOIN mkelas MK ON MK.id=H.idpoli_kelas AND H.asalrujukan IN(3)
					LEFT JOIN mdata_kategori K ON K.id=H.idkategori
					LEFT JOIN mdata_tipebarang T ON T.id=H.idtipe
					LEFT JOIN view_barang_all B ON B.id=H.idbarang AND B.idtipe=H.idtipe
					LEFT JOIN mgroup_pembayaran GH1 ON GH1.id=H.idgroup_penjualan
					LEFT JOIN mgroup_pembayaran GH2 ON GH2.id=H.idgroup_diskon
					
					GROUP BY H.id
					ORDER BY H.asalrujukan,H.idtipe,H.idkategori,H.idbarang


				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = asal_rujukan($r->asalrujukan);
			if ($r->asalrujukan=='0'){
				$row[] = idpoli_kelas($r->idpoli_kelas);				
			}else{
				$row[] = ($r->idpoli_kelas=='0'?text_default('All'):$r->nama_poli);				
			}
            $row[] = ($r->idtipe=='0'?text_default('All Tipe'):$r->nama_tipe);
            $row[] = ($r->idkategori=='0'?text_default('All kategori'):$r->kategori);
            $row[] = ($r->idbarang=='0'?text_default('All Barang'):$r->namabarang);
            $row[] = $r->nama_group_pembelian.' ('.$r->idgroup_penjualan.')';
            $row[] = $r->nama_group_diskon.' ('.$r->idgroup_diskon.')';
			$aksi 		= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_rajal('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_rajal(){
		$id_edit=$this->input->post('id_edit');
		$asalrujukan=($this->input->post('asalrujukan')=='#'?0:$this->input->post('asalrujukan'));
		$idpoli_kelas=($this->input->post('idpoli_kelas')=='#'?0:$this->input->post('idpoli_kelas'));
		$idtipe=($this->input->post('idtipe')=='#'?0:$this->input->post('idtipe'));
		$idkategori=($this->input->post('idkategori')=='#'?0:$this->input->post('idkategori'));
		$idbarang=($this->input->post('idbarang')=='#'?0:$this->input->post('idbarang'));
		$idgroup_penjualan=($this->input->post('idgroup_penjualan')=='#'?0:$this->input->post('idgroup_penjualan'));
		$idgroup_diskon=($this->input->post('idgroup_diskon')=='#'?0:$this->input->post('idgroup_diskon'));
		$data=array(
			'setting_id'=>1,
			'asalrujukan'=>$asalrujukan,
			'idpoli_kelas'=>$idpoli_kelas,
			'idtipe'=>$idtipe,
			'idkategori'=>$idkategori,
			'idbarang'=>$idbarang,
			'idgroup_penjualan'=>$idgroup_penjualan,
			'idgroup_diskon'=>$idgroup_diskon,
		);
		
		if ($this->cek_duplicate_rajal($asalrujukan,$idpoli_kelas,$idtipe,$idkategori,$idbarang)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pendapatan_jual_rajal',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_rajal($asalrujukan,$idpoli_kelas,$idtipe,$idkategori,$idbarang){
		$gabung=$asalrujukan.'-'.$idpoli_kelas.'-'.$idtipe.'-'.$idkategori.'-'.$idbarang;
		$q="SELECT *FROM msetting_jurnal_pendapatan_jual_rajal S
			WHERE CONCAT(S.asalrujukan,'-',S.idpoli_kelas,'-',S.idtipe,'-',S.idkategori,'-',S.idbarang)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_rajal($id)
	{		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_pendapatan_jual_rajal');
		echo json_encode($result);
	}
	
}
