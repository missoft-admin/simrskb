<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msurat_template extends CI_Controller {

	/**
	 * Master Template Surat controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Msurat_template_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Template Surat';
		$data['content'] 		= 'Msurat_template/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Template Surat",'#'),
									    			array("List",'msurat_template')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'jenis_surat' 					=> '',
			'kode' 		=> '',
			'isi_surat' 		=> '',
			'staktif' 				=> '1',
			'judul_ina' => '',
			'judul_eng' => '',
			'footer_ina' => '',
			'footer_eng' => '',

		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Template Surat';
		$data['content'] 		= 'Msurat_template/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Template Surat",'#'),
									    			array("Tambah",'msurat_template')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Msurat_template_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'jenis_surat' 					=> $row->jenis_surat,
					'kode' 					=> $row->kode,
					'isi_surat' 		=> $row->isi_surat,
					'staktif' 				=> $row->staktif,
					'judul_ina' => $row->judul_ina,
					'judul_eng' => $row->judul_eng,
					'footer_ina' => $row->footer_ina,
					'footer_eng' => $row->footer_eng,

				);
				$var_data=$this->Msurat_template_model->load_var($id);
				// $var_data=array();
				// print_r($var_data);exit;
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Template Surat';
				$data['content']	 	= 'Msurat_template/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Template Surat",'#'),
											    			array("Ubah",'msurat_template')
															);

				$data = array_merge($data,$var_data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('msurat_template','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('msurat_template');
		}
	}
	
	function delete($id){
		$this->Msurat_template_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('msurat_template','location');
	}

	function save(){
		$this->form_validation->set_rules('jenis_surat', 'Judul', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->Msurat_template_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disi_suratmpan.');
					redirect('msurat_template/update/'.$id,'location');
				}
			} else {
				if($this->Msurat_template_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disi_suratmpan.');
					redirect('msurat_template/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Msurat_template/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Template Surat';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Template Surat",'#'),
															array("Tambah",'msurat_template')
													);
		}else{
			$data['title'] = 'Ubah Master Template Surat';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Template Surat",'#'),
															array("Ubah",'msurat_template')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	$data_user=get_acces();
	$user_acces_form=$data_user['user_acces_form'];
	$this->select = array();
		$from="
				(
					SELECT M.ref as jenis,H.* 
					FROM msurat_template H
					LEFT JOIN merm_referensi M ON M.nilai=H.jenis_surat AND M.ref_head_id='411'
					WHERE H.staktif='1'
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('jenis','kode');
		$this->column_order  = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->jenis;
          $row[] = $r->kode;
		$aksi = '<div class="btn-group">';
       
		if (UserAccesForm($user_acces_form,array('2249'))){
            $aksi .= '<a href="'.site_url().'msurat_template/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
        }
        if (UserAccesForm($user_acces_form,array('2250'))){
            $aksi .= '<a href="#" data-urlindex="'.site_url().'msurat_template" data-urlremove="'.site_url().'msurat_template/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
        }
        $aksi .= '</div>';
		$row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
}
