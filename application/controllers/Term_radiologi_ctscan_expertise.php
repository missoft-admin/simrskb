<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

require 'vendor/autoload.php';

use Dompdf\Dompdf;
use Dompdf\Options;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Term_radiologi_ctscan_expertise extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Term_radiologi_model', 'erm_radiologi_model');
        $this->load->model('Term_radiologi_ctscan_model', 'erm_radiologi_ctscan_model');
        $this->load->model('Term_radiologi_ctscan_expertise_model', 'model');
    }

    public function index($status_pemeriksaan = '#', $status_cito_expertise = '#'): void
    {
        // $data_user = get_acces();
        // $user_acces_form = $data_user['user_acces_form'];

        // if (UserAccesForm($user_acces_form, ['1584'])) {
            $data = [
                'tanggal' => date("d/m/Y"),
                'status_pemeriksaan' => $status_pemeriksaan,
                'status_cito_expertise' => $status_cito_expertise,
            ];

            $data['error'] = '';
            $data['title'] = 'Radiologi';
            $data['content'] = 'Term_radiologi_ctscan_expertise/index';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Radiologi', '#'],
                ['Expertise', 'term_radiologi_ctscan_expertise'],
            ];

            $ppa = get_ppa_login();
            $data['login_pegawai_id'] = $ppa['login_pegawai_id'];

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        // } else {
        //     redirect('page404');
        // }
    }

    public function proses($id, $status_form = ''): void
    {
        // $data_user = get_acces();
        // $user_acces_form = $data_user['user_acces_form'];

        // if (UserAccesForm($user_acces_form, ['1584'])) {
            $ppa = get_ppa_login();
            $row = $this->erm_radiologi_ctscan_model->get_data_transaksi($id);
            $data = [
                'id' => $row->id,
                'nomor_permintaan' => $row->nomor_permintaan,
                'nomor_radiologi' => $row->nomor_radiologi,
                'tujuan_radiologi' => $row->tujuan_radiologi,
                'tujuan_radiologi_label' => $row->tujuan_radiologi_nama,
                'dokter_peminta_id' => $row->dokter_peminta_id,
                'diagnosa' => $row->diagnosa,
                'catatan_permintaan' => $row->catatan_permintaan,
                'tanggal_permintaan' => DMYFormat($row->waktu_permintaan),
                'waktu_permintaan' => HISTimeFormat($row->waktu_permintaan),
                'prioritas' => $row->prioritas,
                'pasien_puasa' => $row->pasien_puasa,
                'pengiriman_hasil' => $row->pengiriman_hasil,
                'alergi_bahan_kontras' => $row->alergi_bahan_kontras,
                'pasien_hamil' => $row->pasien_hamil,
                'tanggal_pemeriksaan' => DMYFormat($row->waktu_pemeriksaan),
                'waktu_pemeriksaan' => HISTimeFormat($row->waktu_pemeriksaan),
                'petugas_pemeriksaan' => $row->petugas_pemeriksaan,
                'nomor_foto' => $row->nomor_foto,
                'dokter_radiologi' => $ppa['login_tipepegawai'] == 2 ? ($ppa['login_pegawai_id'] == $row->dokter_radiologi_id ? $row->dokter_radiologi_id : $ppa['login_pegawai_id']) : $row->dokter_radiologi_id,
                'jumlah_expose' => $row->jumlah_expose,
                'jumlah_film' => $row->jumlah_film,
                'qp' => $row->qp,
                'mas' => $row->mas,
                'posisi' => $row->posisi,
                'kelas_id' => $row->idkelas,
            ];

            $data['error'] = '';
            $data['title'] = 'Ekspertise';
            $data['content'] = 'Term_radiologi_ctscan_expertise/manage';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Radiologi', '#'],
                ['Ekspertise', 'term_radiologi_ctscan_expertise'],
            ];

            $data['hasil_pemeriksaan_id'] = $this->model->get_hasil_pemeriksaan_terakhir($id);

            $data['pengaturan_form'] = (array) get_row('merm_pengaturan_form_order_radiologi_ctscan');

            // Check Setting Expertise
            $data['tipe_layanan'] = 3;
            $data['asal_pasien'] = $row->asal_rujukan;
            $data['poliklinik_kelas'] = $row->poliklinik_kelas;

            if ($status_form == 'edit_hasil_pemeriksaan') {
                $data['status_form'] = 'edit_hasil_pemeriksaan';
            } else if ($status_form == 'lihat_hasil_pemeriksaan') {
                $data['status_form'] = 'lihat_hasil_pemeriksaan';
            } else {
                $data['status_form'] = '';
            }

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        // } else {
        //     redirect('page404');
        // }
    }

    public function save(): void
    {
		$id = $this->input->post('id');

        // Update Dokter Radiologi & Status Expertise
        $statusExpertise = $this->db->get_where('term_radiologi_ctscan', array('id' => $id))->row()->status_expertise;
        if ($statusExpertise == 0) {
            $this->db->set('status_expertise', 1);
        }
        
        $this->db->set('dokter_radiologi', $this->input->post('dokter_radiologi'));
        $this->db->where('id', $id);
        $this->db->update('term_radiologi_ctscan');
        
		$formSubmit = $this->input->post('form_submit');
        if ($formSubmit == 'form-submit-and-validation') {
            // Update Status Pemeriksaan
            $this->db->set('waktu_penginputan_expertise', date('Y-m-d H:i:s'));
            $this->db->set('petugas_penginputan_expertise', $this->session->userdata('user_id'));
            $this->db->set('status_pemeriksaan', 6);
            $this->db->where('id', $id);
            $this->db->update('term_radiologi_ctscan');

            $version = $this->model->get_version_hasil_pemeriksaan_terakhir($id) + 1;
            
            // Generate Expertise FORMAT 1
            $log_data = $this->generate_hasil_pemeriksaan($id, 'FORMAT_1', $version);
            $log_data['transaksi_id'] = $id;
            $log_data['version'] = $version;
            if ($this->db->insert('term_radiologi_ctscan_hasil_pemeriksaan', $log_data)) {
                $hasi_pemeriksaan_id = $this->db->insert_id();
                
                // Generate Expertise FORMAT 2
                $log_data = $this->generate_hasil_pemeriksaan($id, 'FORMAT_2', $version);
                $log_data['transaksi_id'] = $id;
                $log_data['version'] = $version;

                $this->db->where('id', $hasi_pemeriksaan_id);
                $this->db->update('term_radiologi_ctscan_hasil_pemeriksaan', $log_data);
            }

            // Update Nominal Jasa Medis [RUJUKAN RADIOLOGI]
            $dataRujukan = $this->db->get_where('term_radiologi_ctscan', array('id' => $id))->row();
            $rujukanId = $dataRujukan->rujukan_id;
            $statusExpertise = $dataRujukan->status_expertise;
            $kelasId = $this->input->post('kelas_id');
            $this->erm_radiologi_model->updateNominalJasaMedis($rujukanId, $statusExpertise, $kelasId);
		}
        
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'Data telah disimpan.');
        redirect("term_radiologi_ctscan_expertise", 'location');
    }

    public function riwayat_hasil_pemeriksaan($id): void
    {
        // $data_user = get_acces();
        // $user_acces_form = $data_user['user_acces_form'];

        // if (UserAccesForm($user_acces_form, ['1584'])) {
            $row = $this->model->get_data_transaksi($id);
            $data = [
                'id' => $row->id,
                'pendaftaran_id' => $row->pendaftaran_id,
                'pasien_id' => $row->pasien_id,
                'nomor_medrec' => $row->nomor_medrec,
                'nama_pasien' => $row->nama_pasien,
                'nomor_ktp' => $row->nomor_ktp,
                'jenis_kelamin' => $row->jenis_kelamin,
                'email' => $row->email,
                'alamat_pasien' => $row->alamatpasien,
                'tanggal_lahir' => $row->tanggal_lahir,
                'umur_hari' => $row->umur_hari,
                'umur_bulan' => $row->umur_bulan,
                'umur_tahun' => $row->umur_tahun,
                'kelompok_pasien' => $row->kelompok_pasien,
                'tipe_kunjungan' => $row->tipe_kunjungan,
                'nama_asuransi' => $row->nama_asuransi,
                'nama_poliklinik' => $row->nama_poliklinik,
                'nomor_pendaftaran' => $row->nomor_pendaftaran,
                'nomor_permintaan' => $row->nomor_permintaan,
                'waktu_pembuatan' => $row->waktu_pembuatan,
                'waktu_permintaan' => $row->waktu_permintaan,
                'rencana_pemeriksaan' => DMYFormat($row->rencana_pemeriksaan),
                'diagnosa' => $row->diagnosa,
                'catatan_permintaan' => $row->catatan_permintaan,
                'nomor_transaksi_radiologi' => $row->nomor_radiologi,
                'nomor_foto' => $row->nomor_foto,
                'prioritas_pemeriksaan' => $row->prioritas_pemeriksaan,
                'pasien_puasa' => $row->pasien_puasa,
                'pengiriman_hasil' => $row->pengiriman_hasil,
                'alergi_bahan_kontras' => $row->alergi_bahan_kontras,
                'pasien_hamil' => $row->pasien_hamil,
                'jumlah_expose' => $row->jumlah_expose,
                'jumlah_film' => $row->jumlah_film,
                'qp' => $row->qp,
                'mas' => $row->mas,
                'posisi' => $row->posisi,
                'tanggal_pemeriksaan' => $row->waktu_pemeriksaan ? DMYFormat($row->waktu_pemeriksaan) : '',
                'waktu_pemeriksaan' => $row->waktu_pemeriksaan ? HISTimeFormat($row->waktu_pemeriksaan) : '',
                'petugas_pemeriksaan' => $row->petugas_pemeriksaan,
                'tanggal_penginputan_foto' => $row->waktu_penginputan_foto ? DMYFormat($row->waktu_penginputan_foto) : '',
                'waktu_penginputan_foto' => $row->waktu_penginputan_foto ? HISTimeFormat($row->waktu_penginputan_foto) : '',
                'petugas_penginputan_foto' => $row->petugas_penginputan_foto,
                'tanggal_penginputan_expertise' => $row->waktu_penginputan_expertise ? DMYFormat($row->waktu_penginputan_expertise) : '',
                'waktu_penginputan_expertise' => $row->waktu_penginputan_expertise ? HISTimeFormat($row->waktu_penginputan_expertise) : '',
                'petugas_penginputan_expertise' => $row->petugas_penginputan_expertise,
                'tanggal_pengiriman_hasil' => $row->waktu_pengiriman_hasil ? DMYFormat($row->waktu_pengiriman_hasil) : '',
                'waktu_pengiriman_hasil' => $row->waktu_pengiriman_hasil ? HISTimeFormat($row->waktu_pengiriman_hasil) : '',
                'petugas_pengiriman_hasil' => $row->petugas_pengiriman_hasil,
                'tanggal_penerimaan_hasil' => $row->waktu_penerimaan_hasil ? DMYFormat($row->waktu_penerimaan_hasil) : '',
                'waktu_penerimaan_hasil' => $row->waktu_penerimaan_hasil ? HISTimeFormat($row->waktu_penerimaan_hasil) : '',
                'petugas_penerimaan_hasil' => $row->petugas_penerimaan_hasil,
                'tujuan_radiologi' => $row->tujuan_radiologi,
            ];

            $data['error'] = '';
            $data['title'] = 'Riwayat Hasil';
            $data['content'] = 'Term_radiologi_ctscan_expertise/riwayat_hasil_pemeriksaan';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Radiologi', '#'],
                ['Riwayat Hasil', 'term_radiologi_ctscan_expertise'],
            ];

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        // } else {
        //     redirect('page404');
        // }
    }

    public function riwayat_pengiriman_email_hasil_pemeriksaan($id): void
    {
        // $data_user = get_acces();
        // $user_acces_form = $data_user['user_acces_form'];

        // if (UserAccesForm($user_acces_form, ['1584'])) {
            $row = $this->model->get_data_pengiriman_hasil_pemeriksaan_pertama($id);
            $data = [
                'id' => $id,
                'tanggal_pengiriman_hasil' => DMYFormat($row->waktu_pengiriman_hasil),
                'waktu_pengiriman_hasil' => HISTimeFormat($row->waktu_pengiriman_hasil),
                'tanggal_penerimaan_hasil' => DMYFormat($row->waktu_penerimaan_hasil),
                'waktu_penerimaan_hasil' => HISTimeFormat($row->waktu_penerimaan_hasil),
                'petugas_pengirim_hasil' => $row->petugas_pengiriman_hasil,
                'petugas_penerimaan_hasil' => $row->petugas_penerimaan_hasil,
                'penerima_bukan_petugas' => $row->penerima_bukan_petugas,
                'email_tujuan' => $row->email_penerima,
            ];

            $data['error'] = '';
            $data['title'] = 'Riwayat Pengiriman Hasil';
            $data['content'] = 'Term_radiologi_ctscan_expertise/riwayat_pengiriman_email';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Radiologi', '#'],
                ['Riwayat Hasil', 'term_radiologi_ctscan_expertise'],
            ];

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        // } else {
        //     redirect('page404');
        // }
    }

    public function batal_hasil_pemeriksaan($transaksi_id) {
        $hasilPemeriksaanId = $this->input->post('hasil_pemeriksaan_id');
        $alasanPembatalan = $this->input->post('alasan');

        $this->db->trans_start();

        // Update Expertise
        $this->db->set('waktu_pembatalan_hasil', date('Y-m-d H:i:s'));
        $this->db->set('petugas_pembatalan_hasil', $this->session->userdata('user_id'));
        $this->db->set('alasan_pembatalan', $alasanPembatalan);
        $this->db->set('status', 0);
        $this->db->where('id', $hasilPemeriksaanId);
        $this->db->update('term_radiologi_ctscan_hasil_pemeriksaan');

        // Update Transaksi
        $this->db->set('status_pemeriksaan', 5);
        $this->db->where('id', $transaksi_id);
        $this->db->update('term_radiologi_ctscan');

        $this->db->trans_complete();

        if ($this->db->trans_status() == FALSE) {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        } else {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();

        exit;
    }

    public function generate_hasil_pemeriksaan($id, $type = 'FORMAT_2', $version = '1', $preview = '')
    {
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);

        $row = $this->model->get_data_transaksi($id);

        $data = [
            'id' => $id,
            'nomor_register' => $row->nomor_permintaan,
            'nomor_medrec' => $row->nomor_medrec,
            'nama_pasien' => $row->nama_pasien,
            'kelompok_pasien' => $row->kelompok_pasien,
            'alamat_pasien' => $row->alamatpasien,
            'rekanan_asuransi' => $row->nama_asuransi,
            'dokter_perujuk' => $row->dokter_perujuk,
            'dokter_penanggung_jawab' => $row->dokter_penanggung_jawab,
            'tanggal_lahir' => $row->tanggal_lahir,
            'nomor_radiologi' => $row->nomor_radiologi,
            'umur_tahun' => $row->umur_tahun,
            'umur_bulan' => $row->umur_bulan,
            'umur_hari' => $row->umur_hari,
            'asal_rujukan' => $row->asal_rujukan,
            'dokter_radiologi' => $row->dokter_radiologi,
            'dokter_radiologi_id' => $row->dokter_radiologi_id,
            'dokter_radiologi_nip' => $row->dokter_radiologi_nip,
            'cetakan_ke' => $version,
            'user_created' => $row->user_created,
            'user_input_pemeriksaan' => $row->user_input_pemeriksaan,
            'tanggal_input_pemeriksaan' => $row->waktu_pemeriksaan,
            'user_input_foto' => $row->petugas_penginputan_foto,
            'tanggal_input_foto' => $row->waktu_penginputan_foto,
            'user_input_expertise' => $row->petugas_penginputan_expertise,
            'tanggal_input_expertise' => $row->waktu_penginputan_expertise,
            'user_cetak_id' => $this->session->userdata('login_ppa_id'),
            'user_cetak' => get_nama_ppa($this->session->userdata('login_ppa_id')),
            'tanggal_cetak' => date('Y-m-d H:i:s'),
        ];

        $data['error'] = '';
        $data['title'] = 'Expertise Radiologi';

        $data['daftar_pemeriksaan'] = $this->model->get_daftar_pemeriksaan($id);
        $data['pengaturan_printout'] = (array) get_by_field('tipe', $type, 'merm_pengaturan_printout_radiologi_expertise');
        
        $data = array_merge($data, backend_info());
        
        if ($type == 'FORMAT_1') {
            $html = $this->load->view('Term_radiologi_ctscan_expertise/print/hasil_pemeriksaan_format_1', $data, true);
        } else if ($type == 'FORMAT_2') {
            $html = $this->load->view('Term_radiologi_ctscan_expertise/print/hasil_pemeriksaan_format_2', $data, true);
        }

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');

        // Generate PDF
        $dompdf->render();

        if ('preview' === $preview) {
            $dompdf->stream('Expertise.pdf', ['Attachment' => 0]);
        } else {
            $filePdf = $dompdf->output();
    
            $fileName = "HASIL_RAD_" . strtoupper(str_replace(' ', '_', $row->nomor_medrec)) . "_" . strtoupper(str_replace(' ', '_', $row->nama_pasien)) . "_" . strtoupper(str_replace(' ', '_', $row->nomor_radiologi)) . "_V" . $version . '_' . $type;
            $folderPath = "report/hasil_pemeriksaan_radiologi_ctscan/{$id}";
            $filePath = "report/hasil_pemeriksaan_radiologi_ctscan/{$id}/{$fileName}.pdf";
            if (!file_exists($folderPath)) {
                mkdir($folderPath, 0755, true);
            }
            
            file_put_contents($filePath, $filePdf);
            
            $originalFileName = "HASIL_RAD_" . strtoupper(str_replace(' ', '_', $row->nomor_medrec)) . "_" . strtoupper(str_replace(' ', '_', $row->nama_pasien)) . "_" . strtoupper(str_replace(' ', '_', $row->nomor_radiologi)) . "_V" . $version;
            return [
                'waktu_pemeriksaan' => $row->waktu_pemeriksaan,
                'petugas_pemeriksaan' => $row->petugas_pemeriksaan,
                'waktu_penginputan_foto' => $row->waktu_pemeriksaan,
                'petugas_penginputan_foto' => $row->petugas_penginputan_foto,
                'waktu_penginputan_expertise' => $row->waktu_pemeriksaan,
                'petugas_penginputan_expertise' => $row->petugas_penginputan_expertise,
                'nama_file' => "{$originalFileName}.pdf",
            ];
        }
    }

    public function cetak_hasil_pemeriksaan($hasil_pemeriksaan_id, $format) {
        // Lakukan logging sebelum mengarahkan ke file PDF
        $this->model->log_cetak_hasil($hasil_pemeriksaan_id);

        // Get Data Pemeriksaan
        $row = get_by_field('id', $hasil_pemeriksaan_id, 'term_radiologi_ctscan_hasil_pemeriksaan');
        $nama_file = str_replace('.pdf', "_{$format}.pdf", $row->nama_file);

        // Redirect ke file PDF
        redirect(base_url('report/hasil_pemeriksaan_radiologi_ctscan/' . $row->transaksi_id . '/' . $nama_file));
    }

    public function riwayat_cetak_hasil_pemeriksaan($hasil_pemeriksaan_id) {
        $data['status'] = 'success';
        $data['data'] = $this->model->get_riwayat_cetak_hasil_pemeriksaan($hasil_pemeriksaan_id);
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function get_email_pasien($pasienId) {
        $emailPasien = $this->model->get_email_pasien($pasienId)[0];
        echo json_encode($emailPasien);
    }

    public function get_email_pengiriman_hasil_pemeriksaan_terakhir($id) {
        $emailPasien = $this->model->get_email_pengiriman_hasil_pemeriksaan_terakhir($id);
        echo json_encode($emailPasien);
    }

    public function kirim_email_hasil_pemeriksaan()
    {
        $id = $this->input->post('transaksi_id');
        $dataUpdate = $this->prepareDataUpdate();

        $this->db->where('id', $id);
        $this->db->update('term_radiologi_ctscan', $dataUpdate);

        $pengaturan_email = (array) get_row('merm_pengaturan_pengiriman_email_radiologi');
        $data_transaksi = $this->model->get_data_transaksi($id);
        $email_tujuan = $this->input->post('email_kirim_hasil');
        $emailArray = explode(',', $email_tujuan);

        $file = $this->model->get_file_hasil_pemeriksaan($id);
        $hasil_pemeriksaan_id = $file->id;
        $hasil_pemeriksaan_file = $file->nama_file;

        $fileName = $hasil_pemeriksaan_file;
        $folderPath = "report/hasil_pemeriksaan_radiologi_ctscan/{$id}";
        $filePath = "{$folderPath}/{$fileName}";

        $this->model->log_kirim_hasil($this->prepareLogDataEmail($hasil_pemeriksaan_id, $dataUpdate));

        $this->sendEmail($pengaturan_email, $data_transaksi, $emailArray, $filePath);
    }

    public function kirim_ulang_email_hasil_pemeriksaan()
    {
        $id = $this->input->post('transaksi_id');
        $pengaturan_email = (array) get_row('merm_pengaturan_pengiriman_email_radiologi');
        $data_transaksi = $this->model->get_data_transaksi($id);
        $email_tujuan = $this->input->post('email');
        $emailArray = explode(',', $email_tujuan);

        $file = $this->model->get_file_hasil_pemeriksaan($id);
        $hasil_pemeriksaan_file = $file->nama_file;

        $fileName = $hasil_pemeriksaan_file;
        $folderPath = "report/hasil_pemeriksaan_radiologi_ctscan/{$id}";
        $filePath = "{$folderPath}/{$fileName}";

        $this->sendEmail($pengaturan_email, $data_transaksi, $emailArray, $filePath);
    }

    public function kirim_email_hasil_upload()
    {
        $id = $this->input->post('transaksi_id');

        // Update Logs
        $tanggalPengiriman = $this->input->post('tanggal_pengiriman');
        $waktuPengiriman = $this->input->post('waktu_pengiriman');

        $dataUpdate = [
            'waktu_pengiriman_hasil_upload' => YMDTimeFormat($tanggalPengiriman . ' ' . $waktuPengiriman),
        ];

        $this->db->where('id', $id);
        $this->db->update('term_radiologi_ctscan', $dataUpdate);

        // Get Settings
        $pengaturan_email = (array) get_row('merm_pengaturan_pengiriman_email_hasil_upload_radiologi');
        
        $data_transaksi = $this->model->get_data_transaksi($id);
        $email_tujuan = $this->input->post('email_kirim_hasil');
        $emailArray = explode(',', $email_tujuan);

        $emailBcc = $this->model->get_email_bcc_hasil_upload(1);
        $attachmentUpload = $this->model->get_file_hasil_upload($id);

        $this->sendEmailUpload($pengaturan_email, $data_transaksi, $emailBcc, $emailArray, $attachmentUpload);
    }

    public function getIndex($ppa_id = ''): void
    {
        // $data_user = get_acces();
        // $user_acces_form = $data_user['user_acces_form'];

        $dokter_peminta = $this->input->post('dokter_peminta');
        $asal_pasien = $this->input->post('asal_pasien');
        $status_cito_expertise = $this->input->post('status_cito_expertise');
        $tanggal_dari = YMDFormat($this->input->post('tanggal_dari'));
        $tanggal_sampai = YMDFormat($this->input->post('tanggal_sampai'));
        $nama_pasien = $this->input->post('nama_pasien');
        $nomor_medrec = $this->input->post('nomor_medrec');
        $nomor_pendaftaran = $this->input->post('nomor_pendaftaran');
        $nomor_radiologi = $this->input->post('nomor_radiologi');
        $kelompok_pasien = $this->input->post('kelompok_pasien');
        $nomor_permintaan = $this->input->post('nomor_permintaan');
        $status_expertise = $this->input->post('status_expertise');

        $where = '';
        if ('0' != $dokter_peminta) {
            $where .= " AND (term_radiologi_ctscan.dokter_peminta_id) = '{$dokter_peminta}'";
        }
        if ('0' != $asal_pasien) {
            $where .= " AND (tpoliklinik_pendaftaran.idtipe) = '{$asal_pasien}'";
        }
        if ('#' != $status_cito_expertise) {
            $where .= " AND (term_radiologi_ctscan.prioritas) = '{$status_cito_expertise}'";
        }
        if ('' != $tanggal_dari) {
            $where .= " AND DATE(term_radiologi_ctscan.rencana_pemeriksaan) >= '{$tanggal_dari}'";
        }
        if ('' != $tanggal_sampai) {
            $where .= " AND DATE(term_radiologi_ctscan.rencana_pemeriksaan) <= '{$tanggal_sampai}'";
        }
        if ('' != $nama_pasien) {
            $where .= " AND (mfpasien.nama) LIKE '%".$nama_pasien."%'";
        }
        if ('' != $nomor_medrec) {
            $where .= " AND (mfpasien.no_medrec) = '{$nomor_medrec}'";
        }
        if ('' != $nomor_pendaftaran) {
            $where .= " AND (tpoliklinik_pendaftaran.nopendaftaran) = '{$nomor_pendaftaran}'";
        }
        if ('' != $nomor_radiologi) {
            $where .= " AND (term_radiologi_ctscan.nomor_foto) = '{$nomor_radiologi}'";
        }
        if ('0' != $kelompok_pasien) {
            $where .= " AND (tpoliklinik_pendaftaran.idkelompokpasien) = '{$kelompok_pasien}'";
        }
        if ('' != $nomor_permintaan) {
            $where .= " AND (term_radiologi_ctscan.nomor_permintaan) = '{$nomor_permintaan}'";
        }
        if ('#' != $status_expertise) {
            $where .= " AND (term_radiologi_ctscan.status_expertise) = '{$status_expertise}'";
        }
        if ('' != $ppa_id) {
            $where .= " AND (term_radiologi_ctscan.dokter_radiologi) = '{$ppa_id}'";
        }

        $this->select = [];
        $from = "
        (
        SELECT
            result.*,
            hasil_pemeriksaan.hasil_pemeriksaan_id 
            FROM
                (
                SELECT
                    term_radiologi_ctscan.id,
                    term_radiologi_ctscan.asal_rujukan,
                    (
                    CASE
                        WHEN term_radiologi_ctscan.asal_rujukan = 1 THEN
                        'Poliklinik' 
                        WHEN term_radiologi_ctscan.asal_rujukan = 2 THEN
                        'Instalasi Gawat Darurat (IGD)' 
                        WHEN term_radiologi_ctscan.asal_rujukan = 3 THEN
                        'Rawat Inap' 
                        WHEN term_radiologi_ctscan.asal_rujukan = 4 THEN
                        'One Day Surgery (ODS)' 
                    END 
                    ) AS asal_pasien,
                    (
                    CASE            
                        WHEN term_radiologi_ctscan.asal_rujukan IN ( 1, 2 ) THEN
                        tpoliklinik_pendaftaran.nopendaftaran 
                        WHEN term_radiologi_ctscan.asal_rujukan IN ( 3, 4 ) THEN
                        poliklinik_ranap.nopendaftaran 
                    END 
                    ) AS nomor_pendaftaran,
                    (
                    CASE                    
                        WHEN term_radiologi_ctscan.asal_rujukan IN ( 1, 2 ) THEN
                        tpoliklinik_pendaftaran.tanggaldaftar 
                        WHEN term_radiologi_ctscan.asal_rujukan IN ( 3, 4 ) THEN
                        poliklinik_ranap.tanggaldaftar 
                    END 
                    ) AS tanggal_pendaftaran,
                    term_radiologi_ctscan.nomor_permintaan,
                    term_radiologi_ctscan.nomor_foto,
                    term_radiologi_ctscan.rencana_pemeriksaan,
                    term_radiologi_ctscan.nomor_antrian,
                    tpoliklinik_pendaftaran.kode_antrian,
                    COALESCE ( mfpasien.id, poliklinik_ranap.pasien_id ) AS pasien_id,
                    COALESCE ( mfpasien.no_medrec, poliklinik_ranap.nomor_medrec ) AS nomor_medrec,
                    COALESCE ( mfpasien.title, poliklinik_ranap.title_pasien ) AS title_pasien,
                    COALESCE ( mfpasien.nama, poliklinik_ranap.nama_pasien ) AS nama_pasien,
                    COALESCE ( mfpasien.jenis_kelamin, poliklinik_ranap.jenis_kelamin ) AS jenis_kelamin,
                    COALESCE ( mfpasien.tanggal_lahir, poliklinik_ranap.tanggal_lahir ) AS tanggal_lahir,
                    COALESCE ( mfpasien.umur_tahun, poliklinik_ranap.umur_tahun ) AS umur_tahun,
                    COALESCE ( mfpasien.umur_bulan, poliklinik_ranap.umur_bulan ) AS umur_bulan,
                    COALESCE ( mfpasien.umur_hari, poliklinik_ranap.umur_hari ) AS umur_hari,
                    referensi_jenis_kelamin.ref AS jenis_kelamin_label,
                    mpasien_kelompok.nama AS kelompok_pasien,
                    mrekanan.nama AS nama_rekanan,
                    term_radiologi_ctscan.status_pemeriksaan,
                    (
                    CASE                    
                        WHEN term_radiologi_ctscan.asal_rujukan IN ( 1, 2 ) THEN
                        tpoliklinik_pendaftaran.id 
                        WHEN term_radiologi_ctscan.asal_rujukan IN ( 3, 4 ) THEN
                        poliklinik_ranap.id 
                    END 
                    ) AS pendaftaran_id,
                    term_radiologi_ctscan.tujuan_radiologi,
                    term_radiologi_ctscan.jumlah_edit,
                    SUM( term_radiologi_ctscan_pemeriksaan.totalkeseluruhan ) AS total_tarif,
                    referensi_prioritas.ref AS prioritas_label,
                    mdokter.id AS id_dokter_radiologi,
                    mdokter.nama AS dokter_radiologi,
                    term_radiologi_ctscan.prioritas,
                    term_radiologi_ctscan.status_expertise,
                    CASE
                        WHEN assesmen.id IS NOT NULL THEN
                            2 ELSE 0 
                    END AS riwayat_alergi,
                    mpoliklinik.nama AS nama_poliklinik,
                    trujukan_radiologi.norujukan AS nomor_radiologi,
                    tkasir.status AS statuskasir,
                    poliklinik_ranap.id AS idpendaftaran_ranap,
                    COALESCE ( tpoliklinik_pendaftaran.id, poliklinik_ranap.idpendaftaran_rajal ) AS idpendaftaran_rajal
                FROM
                    term_radiologi_ctscan
                LEFT JOIN (
                SELECT
                    trawatinap_pendaftaran.id,
                    trawatinap_pendaftaran.tanggaldaftar,
                    trawatinap_pendaftaran.nopendaftaran,
                    tpoliklinik_pendaftaran.kode_antrian,
                    tpoliklinik_pendaftaran.idpasien AS pasien_id,
                    tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
                    tpoliklinik_pendaftaran.title AS title_pasien,
                    tpoliklinik_pendaftaran.namapasien AS nama_pasien,
                    tpoliklinik_pendaftaran.jenis_kelamin AS jenis_kelamin,
                    tpoliklinik_pendaftaran.tanggal_lahir AS tanggal_lahir,
                    tpoliklinik_pendaftaran.umurtahun AS umur_tahun,
                    tpoliklinik_pendaftaran.umurbulan AS umur_bulan,
                    tpoliklinik_pendaftaran.umurhari AS umur_hari,
                    mpasien_kelompok.nama AS kelompok_pasien,
                    tpoliklinik_pendaftaran.id AS idpendaftaran_rajal
                FROM
                    trawatinap_pendaftaran
                    INNER JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
                    INNER JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien 
                ) AS poliklinik_ranap ON term_radiologi_ctscan.pendaftaran_id = poliklinik_ranap.id 
                AND term_radiologi_ctscan.asal_rujukan IN ( 3, 4 )
                LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = term_radiologi_ctscan.pendaftaran_id 
                AND term_radiologi_ctscan.asal_rujukan IN ( 1, 2 )
                LEFT JOIN mfpasien ON mfpasien.id = tpoliklinik_pendaftaran.idpasien
                LEFT JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
                LEFT JOIN mrekanan ON mrekanan.id = tpoliklinik_pendaftaran.idrekanan
                LEFT JOIN merm_pengaturan_tujuan_radiologi ON merm_pengaturan_tujuan_radiologi.id = term_radiologi_ctscan.tujuan_radiologi
                LEFT JOIN term_radiologi_ctscan_pemeriksaan ON term_radiologi_ctscan_pemeriksaan.transaksi_id = term_radiologi_ctscan.id
                LEFT JOIN merm_referensi referensi_prioritas ON referensi_prioritas.ref_head_id = 85 
                AND referensi_prioritas.nilai = term_radiologi_ctscan.prioritas
                LEFT JOIN mdokter ON mdokter.id = term_radiologi_ctscan.dokter_radiologi
                LEFT JOIN merm_referensi referensi_jenis_kelamin ON referensi_jenis_kelamin.ref_head_id = '1' 
                AND referensi_jenis_kelamin.nilai = mfpasien.jenis_kelamin
                LEFT JOIN ( SELECT * FROM `tpoliklinik_assesmen_igd_alergi` WHERE status_assemen = '2' UNION ALL SELECT * FROM `tpoliklinik_assesmen_alergi` WHERE status_assemen = '2' ) assesmen ON assesmen.idpasien = mfpasien.id 
                AND assesmen.status_assemen = '2'
                LEFT JOIN mpoliklinik ON mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik
                LEFT JOIN trujukan_radiologi ON trujukan_radiologi.id = term_radiologi_ctscan.rujukan_id 
                LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			    LEFT JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN (1,2)
            WHERE
                term_radiologi_ctscan.status_pemeriksaan NOT IN ( 1, 2, 3 ) 
                AND merm_pengaturan_tujuan_radiologi.tipe_layanan = '3' ".$where.' 
            GROUP BY
                term_radiologi_ctscan.id 
            ) AS result
            LEFT JOIN ( SELECT transaksi_id, MAX( id ) AS hasil_pemeriksaan_id FROM term_radiologi_ctscan_hasil_pemeriksaan GROUP BY transaksi_id ) AS hasil_pemeriksaan ON result.id = hasil_pemeriksaan.transaksi_id 
        ) AS tbl 
        WHERE
            tbl.id IS NOT NULL
        ';
        
        $this->from = $from;
        $this->join = [];

        $this->order = [];
        $this->group = [];
        $this->column_search = ['nomor_permintaan'];
        $this->column_order = [];

        $list = $this->datatable->get_datatables(true);
        $data = [];
        $no = $_POST['start'];

        $ppa = get_ppa_login();
        $tipe_pegawai = $ppa['login_tipepegawai'];
        $pegawai_id = $ppa['login_pegawai_id'];
        
        foreach ($list as $r) {
            ++$no;
            $result = [];

            $def_image='';
            if ($r->jenis_kelamin == '1') {
                $def_image = 'def_1.png';
            } else {
                $def_image = 'def_2.png';
            }
            
            $asal_rujukan = (in_array($r->asal_rujukan, [1, 2]) ? 'rawat_jalan' : 'rawat_inap');
            $status_ownership = ($ppa['login_tipepegawai'] == 2 && $ppa['login_pegawai_id'] == $r->id_dokter_radiologi);

            $result[] = '<table class="block-table text-left">
                            <tbody>
                                <tr>
                                    <td class="bg-white" style="width: 100%;">
                                        ' . ($r->status_pemeriksaan < 6 ? '<div class="push-5-t"><a href="#" onclick="prosesExpertise(' . $r->id . ')" data-toggle="tooltip" title="Periksa" class="btn btn-block btn-primary btn-xs"><i class="fa fa-user-md"></i> PERIKSA</a></div>' : '') . '
                                        ' . ($r->status_pemeriksaan >= 6 && $status_ownership ? '<div class="push-5-t"><a href="#" type="button" data-toggle="modal" data-target="#modalAlasanPembatalan" title="Batal Hasil" class="btn btn-block btn-danger btn-xs" onclick="batalHasilPemeriksaan('.$r->id.', '.$r->hasil_pemeriksaan_id.')"><i class="fa fa-trash"></i> BATALKAN HASIL</a></div>' : '') . '
                                        <div class="btn-group push-5-t" style="width: 100%;">
                                            <button class="btn btn-success btn-xs btn-block dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-print"></i> CETAK
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                '. ($r->status_pemeriksaan >= 2 ? '<li class=""><a href="#" onclick="cetakPemeriksaan(' . $r->id . ')">Pemeriksaan</a></li>' : '') .'
                                                '. ($r->status_pemeriksaan >= 2 ? '<li class=""><a href="#" onclick="cetakBuktiPemeriksaan(' . $r->id . ')">Bukti Pemeriksaan</a></li>' : '') .'
                                                '. ($r->status_pemeriksaan >= 2 ? '<li class=""><a href="#" onclick="cetakBuktiPemeriksaanSmall(' . $r->id . ')">Bukti Pemeriksaan (Small)</a></li>' : '') .'
                                                '. ($r->status_pemeriksaan >= 2 ? '<li class=""><a href="#" onclick="cetakBuktiPengambilanPemeriksaan(' . $r->id . ')">Bukti Pengambilan Pemeriksaan</a></li>' : '') .'
                                                '. ($r->status_pemeriksaan >= 2 ? '<li class=""><a href="#" onclick="cetakBuktiPengambilanPemeriksaanSmall(' . $r->id . ')">Bukti Pengambilan Pemeriksaan (Small)</a></li>' : '') .'
                                                '. ($r->status_pemeriksaan >= 6 ? '<li class=""><a href="#" onclick="cetakHasilPemeriksaan(' . $r->hasil_pemeriksaan_id . ', \'FORMAT_1\')">Hasil Pemeriksaan - Format 1</a></li>' : '') .'
                                                '. ($r->status_pemeriksaan >= 6 ? '<li class=""><a href="#" onclick="cetakHasilPemeriksaan(' . $r->hasil_pemeriksaan_id . ', \'FORMAT_2\')">Hasil Pemeriksaan - Format 2</a></li>' : '') .'
                                            </ul>
                                        </div>
                                        <div class="btn-group push-5-t" style="width: 100%;">
                                            <button class="btn btn-warning btn-block btn-xs dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                                                <span class="fa fa-caret-down"></span> PANEL KENDALI
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                '. ($r->status_pemeriksaan >= 5 ? '<li class=""><a href="#" data-toggle="modal" data-target="#modal-kirim-hasil-upload" onclick="kirimEmailHasilUpload('.$r->id.', '.$r->pasien_id.', '.$r->tujuan_radiologi.')">Kirim Hasil Upload</a></li>' : '') .'
                                                '. ($r->status_pemeriksaan == 6 ? '<li class=""><a href="#" data-toggle="modal" data-target="#modal-kirim-hasil" onclick="kirimEmailHasilPemeriksaan('.$r->id.', '.$r->pasien_id.', '.$r->tujuan_radiologi.')">Kirim Hasil Pemeriksaan</a></li>' : '') .'
                                                '. ($r->status_pemeriksaan >= 7 ? '<li class=""><a href="#" onclick="kirimUlangEmailHasilPemeriksaan('.$r->id.')" data-toggle="modal" data-target="#modal-kirim-ulang-hasil">Kirim Email Ulang</a></li>' : '') .'
                                                '. ($r->status_pemeriksaan >= 7 ? '<li class=""><a href="#" onclick="riwayatPengirimanEmailHasilPemeriksaan(' . $r->id . ')">Riwayat Pengiriman</a></li>' : '') .'
                                                '. ($r->status_pemeriksaan >= 6 ? '<li class=""><a href="#" onclick="riwayatHasilPemeriksaan(' . $r->id . ')">Riwayat Hasil</a></li>' : '') .'
                                                <li class=""><a href="#" onclick="riwayatRadiologi(' . $r->pendaftaran_id . ', ' . $r->pasien_id . ')">Riwayat Radiologi</a></li>
                                                <li class=""><a href="#" onclick="dataRadiologi(\'' . $asal_rujukan . '\', ' . $r->pendaftaran_id . ', ' . $r->id . ')">Data Radiologi</a></li>
                                                <li class=""><a href="#" onclick="stikerIdentitas(' . $r->id . ')">Stiker Identitas</a></li>
                                                <li class=""><a href="#" onclick="labelTabungDarah(' . $r->id . ')">Label Tabung Darah</a></li>
                                                <li class=""><a href="#" onclick="labelRadiologi(' . $r->id .')">Label Radiologi</a></li>
                                                '.($r->statuskasir != 2 ? '<li class=""><a href="#" data-toggle="modal" data-target="#modal-ubah-kelas-tarif" data-transaksi-id="' . $r->id . '" data-total-tarif="' . $r->total_tarif . '" id="ubah-kelas-tarif">Ubah Kelas Tarif</a></li>' : '').'
                                                <li class=""><a href="#" onclick="inputBMHPTagihan(' . $r->id . ')">Input BMHP Tagihan</a></li>
                                                <li class=""><a href="#" onclick="inputBMHPNonTagihan(' . $r->id . ')">Input BMHP Non Tagihan</a></li>
                                                <li class=""><a href="#" onclick="uploadFotoRadiologi(' . $r->id . ')">Upload Foto Radiologi</a></li>
                                                '.($r->idpendaftaran_rajal ? '<li class=""><a href="' . site_url().'tpendaftaran_poli_ttv/tindakan/' . $r->idpendaftaran_rajal . '/erm_rj" target="_blank">ERM Rawat Jalan</a></li>' : ''). '
                                                '.($r->idpendaftaran_ranap ? '<li class=""><a href="' . site_url().'tpendaftaran_ranap_erm/tindakan/' . $r->idpendaftaran_ranap . '/erm_ri" target="_blank">ERM Rawat Inap</a></li>' : ''). '
                                            </ul>
                                        </div>
                                        ' . ($r->status_pemeriksaan < 6 ? '<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Final Transaksi" class="btn btn-block btn-success btn-xs"><i class="fa fa-save"></i> FINAL TRANSAKSI</button></div>' : '') . '
                                        ' . ($r->status_pemeriksaan >= 6 ? '<div class="push-5-t"><button type="button" onclick="lihatExpertise(' . $r->id . ')" data-toggle="tooltip" title="Lihat Ekspertise" class="btn btn-block btn-default btn-xs"><i class="fa fa-eye"></i> LIHAT EKSPERTISE</button></div>' : '') . '
                                    </td>
                                </tr>
                            </tbody>
                        </table>';

            $result[] = '<table class="block-table text-left">
                            <tbody>
                                <tr>
                                    <td style="width: 15%;">
                                        <div class="text-center">
                                            <img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
                                        </div>
                                        <div class="text-center bg-danger text-white h5 push-5-t">
                                            <strong>'.$r->prioritas_label.'</strong>
                                        </div>
                                    </td>
                                    <td class="bg-white" style="width: 85%;">
                                        <div class="h5 font-w700 text-primary">'.$r->title_pasien.'. '.$r->nama_pasien.' - '.$r->nomor_medrec.'</div>
                                        <div class="h5 text-muted  push-5-t"> '.$r->jenis_kelamin_label.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umur_tahun.' Tahun '.$r->umur_bulan.' Bulan '.$r->umur_hari.' Hari </div>
                                        <div class="h5 text-muted text-uppercase push-5-t"> 
                                        <div class="btn-group" role="group">
                                            <button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi).'</button>
                                            <button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->pasien_id.')"><i class="fa fa-info"></i></button>
                                        </div>
                                        <div class="h5 text-muted  push-5-t"> '.$r->nomor_radiologi.' | '.HumanDateLong($r->rencana_pemeriksaan).' | '.$r->dokter_radiologi.'</div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>';

            $result[] = '<table class="block-table text-left">
                            <tbody>
                                <tr>
                                    
                                    <td class="bg-white" style="width: 100%;">
                                        <div class="h5 text-primary"> REGISTRATION INFO</div>
                                        <div class="push-5-t"> '.$r->nomor_pendaftaran.', '.HumanDateLong($r->tanggal_pendaftaran).'</div>
                                        <div class="text-muted text-uppercase">'.GetAsalRujukanR($r->asal_rujukan).' - '.$r->nama_poliklinik.'</div>
                                        <div class="text-muted"><i class="fa fa-user-md"></i> '.($r->dokter_radiologi).'</div>
                                        <div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->nama_rekanan ? $r->nama_rekanan : $r->kelompok_pasien).'</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>';

            $result[] = '<table class="block-table text-center">
                            <tbody>
                                <tr>
                                    
                                    <td class="bg-white" style="width: 100%;">
                                        ' . StatusPemeriksaanRadiologi($r->status_pemeriksaan, '12px') . '
                                        ' . StatusExpertiseRadiologi($r->status_expertise, '12px') . '
                                        <div class="h4 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggal_pendaftaran).'</strong></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>';

            $data[] = $result;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(true),
            'recordsFiltered' => $this->datatable->count_all(true),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function getIndexRiwayatHasil($id): void
    {
        $this->select = [
            'term_radiologi_ctscan_hasil_pemeriksaan.*,
            term_radiologi_ctscan_hasil_pemeriksaan.id AS hasil_pemeriksaan_id,
            petugas_pemeriksaan.nama AS petugas_pemeriksaan,
            petugas_penginputan_foto.nama AS petugas_penginputan_foto,
            petugas_penginputan_expertise.nama AS petugas_penginputan_expertise,
            COALESCE(petugas_pembatalan_hasil.nama, "-") AS petugas_pembatalan_hasil,
            COALESCE(term_radiologi_ctscan_hasil_pemeriksaan.alasan_pembatalan, "-") AS alasan_pembatalan'
        ];

        $this->from = 'term_radiologi_ctscan_hasil_pemeriksaan';

        $this->join = [
            ['mppa petugas_pemeriksaan', 'petugas_pemeriksaan.id = term_radiologi_ctscan_hasil_pemeriksaan.petugas_pemeriksaan', 'LEFT'],
            ['mppa petugas_penginputan_foto', 'petugas_penginputan_foto.id = term_radiologi_ctscan_hasil_pemeriksaan.petugas_penginputan_foto', 'LEFT'],
            ['mppa petugas_penginputan_expertise', 'petugas_penginputan_expertise.id = term_radiologi_ctscan_hasil_pemeriksaan.petugas_penginputan_expertise', 'LEFT'],
            ['mppa petugas_pembatalan_hasil', 'petugas_pembatalan_hasil.id = term_radiologi_ctscan_hasil_pemeriksaan.petugas_pembatalan_hasil', 'LEFT'],
        ];

        $this->where = [
            'term_radiologi_ctscan_hasil_pemeriksaan.transaksi_id' => $id
        ];

        $this->order = [];
        $this->group = [];
        $this->column_search = [];
        $this->column_order = [];

        $list = $this->datatable->get_datatables(true);
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $result = [];
            $result[] = $r->version;
            $result[] = $r->petugas_pemeriksaan . '<br>' . $r->waktu_pemeriksaan;
            $result[] = $r->petugas_penginputan_foto . '<br>' . $r->waktu_penginputan_foto;
            $result[] = $r->petugas_penginputan_expertise . '<br>' . $r->waktu_penginputan_expertise;
            $result[] = StatusRow($r->status);
            $result[] = $r->petugas_pembatalan_hasil . '<br>' . $r->waktu_pembatalan_hasil;
            $result[] = $r->jumlah_cetak;
            $result[] = $r->alasan_pembatalan;
            $result[] = '<a href="#">' . $r->nama_file . '</a>';

            $result[] = '<div class="btn-group">
                <div class="btn-group">
                    <button class="btn btn-success btn-xs btn-block dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-print"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li class=""><a href="#" onclick="cetakHasilPemeriksaan(' . $r->hasil_pemeriksaan_id . ', \'FORMAT_1\')">Format 1</a></li>
                        <li class=""><a href="#" onclick="cetakHasilPemeriksaan(' . $r->hasil_pemeriksaan_id . ', \'FORMAT_2\')">Format 2</a></li>
                    </ul>
                </div>
                <a href="#" class="btn btn-primary btn-xs" onclick="riwayatCetakHasilPemeriksaan(' . $r->id . ')" data-toggle="modal" data-target="#modal-riwayat-cetak-hasil-pemeriksaan"><i class="fa fa-list"></i></a>
            </div>';

            $data[] = $result;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(true),
            'recordsFiltered' => $this->datatable->count_all(true),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function getIndexRiwayatPengirimanHasil($id): void
    {
        $this->select = [
            'term_radiologi_ctscan_hasil_pemeriksaan_email.*,
            term_radiologi_ctscan_hasil_pemeriksaan.nama_file,
            petugas_pengiriman_hasil.nama AS petugas_pengiriman_hasil'
        ];

        $this->from = 'term_radiologi_ctscan_hasil_pemeriksaan_email';

        $this->join = [
            ['term_radiologi_ctscan_hasil_pemeriksaan', 'term_radiologi_ctscan_hasil_pemeriksaan.id = term_radiologi_ctscan_hasil_pemeriksaan_email.hasil_pemeriksaan_id', 'LEFT'],
            ['mppa petugas_pengiriman_hasil', 'petugas_pengiriman_hasil.id = term_radiologi_ctscan_hasil_pemeriksaan_email.petugas_pengiriman_hasil', 'LEFT'],
        ];

        $this->where = [
            'term_radiologi_ctscan_hasil_pemeriksaan.transaksi_id' => $id
        ];

        $this->order = [];
        $this->group = [];
        $this->column_search = [];
        $this->column_order = [];

        $list = $this->datatable->get_datatables(true);
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $result = [];
            $result[] = $no;
            $result[] = $r->petugas_pengiriman_hasil;
            $result[] = $r->waktu_pengiriman_hasil;

            $formattedEmails = '<span class="label label-primary">' . str_replace(',', '</span>&nbsp;<span class="label label-primary">', $r->email_penerima) . '</span>';
            $result[] = $formattedEmails;
            $result[] = '<a href="#">' . $r->nama_file . '</a>';

            $result[] = '<div class="btn-group">
                <button class="btn btn-success btn-xs btn-block dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-print"></i>
                </button>
                <ul class="dropdown-menu pull-right">
                    <li class=""><a href="#" onclick="cetakHasilPemeriksaan(' . $r->hasil_pemeriksaan_id . ', \'FORMAT_1\')">Format 1</a></li>
                    <li class=""><a href="#" onclick="cetakHasilPemeriksaan(' . $r->hasil_pemeriksaan_id . ', \'FORMAT_2\')">Format 2</a></li>
                </ul>
            </div>';

            $data[] = $result;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(true),
            'recordsFiltered' => $this->datatable->count_all(true),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    // Fungsi-fungsi helper
    private function prepareDataUpdate()
    {
        $tanggalPengiriman = $this->input->post('tanggal_pengiriman');
        $waktuPengiriman = $this->input->post('waktu_pengiriman');
        $tanggalPenerimaan = $this->input->post('tanggal_penerimaan');
        $waktuPenerimaan = $this->input->post('waktu_penerimaan');
        $petugasPengirimHasil = $this->input->post('petugas_pengirim_hasil');
        $petugasPenerimaHasil = $this->input->post('petugas_penerima_hasil');

        return [
            'status_pemeriksaan' => 7,
            'waktu_pengiriman_hasil' => YMDTimeFormat($tanggalPengiriman . ' ' . $waktuPengiriman),
            'petugas_pengiriman_hasil' => $petugasPengirimHasil,
            'waktu_penerimaan_hasil' => YMDTimeFormat($tanggalPenerimaan . ' ' . $waktuPenerimaan),
            'petugas_penerimaan_hasil' => $petugasPenerimaHasil,
        ];
    }

    private function prepareLogDataEmail($hasilPemeriksaanId, $dataUpdate)
    {
        return [
            'hasil_pemeriksaan_id' => $hasilPemeriksaanId,
            'waktu_pengiriman_hasil' => $dataUpdate['waktu_pengiriman_hasil'],
            'petugas_pengiriman_hasil' => $dataUpdate['petugas_pengiriman_hasil'],
            'waktu_penerimaan_hasil' => $dataUpdate['waktu_penerimaan_hasil'],
            'petugas_penerimaan_hasil' => $dataUpdate['petugas_penerimaan_hasil'],
            'penerima_bukan_petugas' => $this->input->post('penerima_bukan_petugas'),
            'email_penerima' => $this->input->post('email_kirim_hasil')
        ];
    }

    private function sendEmail($pengaturan_email, $data_transaksi, $emailArray, $filePath)
    {
        $mail = new PHPMailer(true);

        try {
            // Server settings
            $this->configureEmail($mail);

            // Recipients
            $mail->setFrom('erm@halmaherasiaga.com', 'SIMRS RSKB Halmahera Siaga');
            foreach ($emailArray as $email) {
                $mail->addAddress($email, $data_transaksi->nama_pasien);
            }

            // Attachments
            $pengaturan_attachment_email = $this->model->get_pengaturan_attachment_email();
            foreach ($pengaturan_attachment_email as $attachment) {
                $file_hasil_id = $attachment->file_hasil_id;

                if ($file_hasil_id == 1) {
                    $fileFormat1 = str_replace('.pdf', '_FORMAT_1.pdf', $filePath);
                    $mail->addAttachment($fileFormat1);
                }

                if ($file_hasil_id == 2) {
                    $fileFormat2 = str_replace('.pdf', '_FORMAT_2.pdf', $filePath);
                    $mail->addAttachment($fileFormat2);
                }
            }

            // Content
            $bodyHTML = str_replace('{Nama_Pasien}', '<b>' . $data_transaksi->nama_pasien . '</b>', $pengaturan_email['body']) . '<br>' . $pengaturan_email['footer_email'];

            $mail->isHTML(true);
            $mail->Subject = strip_tags($pengaturan_email['judul']);
            $mail->Body = $bodyHTML;

            $mail->send();

            $response = [
                'status' => true,
                'message' => 'success',
            ];

            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT))
                ->_display();

            exit;
        } catch (Exception $e) {
            $response = [
                'status' => false,
                'message' => "Message could not be sent. Mailer Error: {$mail->ErrorInfo}",
            ];

            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT))
                ->_display();

            exit;
        }
    }

    private function sendEmailUpload($pengaturan_email, $data_transaksi, $emailBcc, $emailArray, $attachmentUpload)
    {
        $mail = new PHPMailer(true);

        try {
            // Server settings
            $this->configureEmail($mail);

            // Recipients
            $mail->setFrom('erm@halmaherasiaga.com', 'SIMRS RSKB Halmahera Siaga');
            foreach ($emailArray as $email) {
                $mail->addAddress($email, $data_transaksi->nama_pasien);
            }

            // BCC
            foreach ($emailBcc as $bcc) {
                $mail->addBCC($bcc->email);
            }

            // Attachments
            foreach ($attachmentUpload as $attachment) {
                $mail->addAttachment('assets/upload/foto_radiologi_xray/' . $attachment->filename);
            }

            // Content
            $bodyHTML = str_replace('{Nama_Pasien}', '<b>' . $data_transaksi->nama_pasien . '</b>', $pengaturan_email['body']) . '<br>' . $pengaturan_email['footer_email'];

            $mail->isHTML(true);
            $mail->Subject = strip_tags($pengaturan_email['judul']);
            $mail->Body = $bodyHTML;
            
            $mail->send();

            $response = [
                'status' => true,
                'message' => 'success',
            ];

            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT))
                ->_display();

            exit;
        } catch (Exception $e) {
            $response = [
                'status' => false,
                'message' => "Message could not be sent. Mailer Error: {$mail->ErrorInfo}",
            ];

            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT))
                ->_display();

            exit;
        }
    }

    private function configureEmail($mail)
    {
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->SMTPOptions = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ];

        $mail->Host = 'mail.halmaherasiaga.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'erm@halmaherasiaga.com';
        $mail->Password = '{T,_Xun}9{@5';
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
        $mail->Port = 465;
    }

    public function get_data_pemeriksaan($transaksi_id) {
        $daftar_pemeriksaan = $this->model->get_daftar_pemeriksaan($transaksi_id);
        
        $html = '';
        foreach ($daftar_pemeriksaan as $row) {
            $html .= '<tr>';
            $html .= '<td>
                <a href="#" class="btn-open" data-pemeriksaan-id="' . $row->id . '" data-pemeriksaan-nama="' . $row->namapemeriksaan . '">
                    ' . $row->namapemeriksaan . '<br>
                    <i>Order By : ' . $row->order_by . '</i><br>
                    <i>' . $row->order_at . '</i>
                </a>
            </td>';
            $html .= '</tr>';
        }

        header('Content-Type: text/html');
        echo $html;
    }

    public function get_detail_pemeriksaan() {
        $pemeriksaanId = $this->input->post('pemeriksaan_id');
        $pemeriksaanData = $this->model->get_detail_pemeriksaan($pemeriksaanId);
        echo json_encode($pemeriksaanData);
    }

    public function update_pemeriksaan_expertise() {
        $pemeriksaanId = $this->input->post('pemeriksaan_id');
        $flagKritis = $this->input->post('flag_kritis');
        $klinis = $this->input->post('klinis');
        $kesan = $this->input->post('kesan');
        $usul = $this->input->post('usul');
        $hasil = $this->input->post('hasil');
        
        // Update data
        $result = $this->model->update_pemeriksaan_expertise($pemeriksaanId, $flagKritis, $klinis, $kesan, $usul, $hasil);

        // Send response
        if ($result) {
            echo json_encode(array('success' => true, 'message' => 'Data updated successfully'));
        } else {
            echo json_encode(array('success' => false, 'message' => 'Failed to update data'));
        }
    }
}
