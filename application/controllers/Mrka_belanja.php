<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrka_belanja extends CI_Controller {

	/**
	 * Create RKA controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mrka_belanja_model');
		$this->load->model('Mrka_kegiatan_model');
		$this->load->model('Mrka_pengajuan_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }
	function insert_validasi_rka($id){
		$this->Mrka_belanja_model->insert_validasi_rka($id);
	}
	function index(){
		$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d'));
		date_add($date1,date_interval_create_from_date_string("-30 days"));
		date_add($date2,date_interval_create_from_date_string("0 days"));
		$date1= date_format($date1,"d-m-Y");
		$date2= date_format($date2,"d-m-Y");

		$data = array(
			'tanggal_pengajuan2'=>$date1,
			'tanggal_pengajuan1'=>$date2,
			'tanggal_dibutuhkan1'=>'',
			'tanggal_dibutuhkan2'=>'',
			'idjenis'=>'#',
		);
		$data['list_unit_ganti'] 		= $this->Mrka_belanja_model->list_unit_ganti();
		$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
		$data['list_jenis'] 		= $this->Mrka_belanja_model->list_jenis_all();
		$data['error'] 			= '';
		$data['title'] 			= 'Belanja';
		$data['content'] 		= 'Mrka_belanja/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("List RKA",'#'),
									    			array("List",'mrka_pengajuan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	
	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  $iduser=$this->session->userdata('user_id');
	  
	  $no_pengajuan=$this->input->post('no_pengajuan');
	  $nama_kegiatan=$this->input->post('nama_kegiatan');
	  $tipe_rka=$this->input->post('tipe_rka');
	  $idunit_pengaju=$this->input->post('idunit_pengaju');
	  $idjenis=$this->input->post('idjenis');
	  $tanggal_pengajuan1= ($this->input->post('tanggal_pengajuan1'));
	  $tanggal_pengajuan2= ($this->input->post('tanggal_pengajuan2'));
	  $tanggal_dibutuhkan1= ($this->input->post('tanggal_dibutuhkan1'));
	  $tanggal_dibutuhkan2= ($this->input->post('tanggal_dibutuhkan2'));
	  $where='';
	  // $where .=" AND H.status='3'";
	  if ($no_pengajuan !=''){
		  $where .=" AND H.no_pengajuan='$no_pengajuan'";
	  }
	  if ($nama_kegiatan !=''){
		  $where .=" AND H.nama_kegiatan='$nama_kegiatan'";
	  }
	  if ($tipe_rka !='#'){
		  $where .=" AND H.tipe_rka='$tipe_rka'";
	  }
	  if ($idunit_pengaju !='#'){
		  $where .=" AND H.idunit_pengaju='$idunit_pengaju'";
	  }
	  if ($idjenis !='#'){
		  $where .=" AND H.idjenis='$idjenis'";
	  }
	  if ($tanggal_pengajuan1 !='' || $tanggal_pengajuan2 !=''){
		  $where .=" AND (H.tanggal_pengajuan >='".YMDFormat($tanggal_pengajuan1)."' AND H.tanggal_pengajuan <='".YMDFormat($tanggal_pengajuan2)."' )";
	  }
	  if ($tanggal_dibutuhkan1 !='' || $tanggal_dibutuhkan2 !=''){
		  $where .=" AND (H.tanggal_dibutuhkan >='".YMDFormat($tanggal_dibutuhkan1)."' AND H.tanggal_dibutuhkan <='".YMDFormat($tanggal_dibutuhkan2)."' )";
	  }
	  $where .="AND B.iduser IN ('$iduser')";
	  $from="(
				SELECT H.id,H.no_pengajuan,H.created_date,H.tanggal_pengajuan,H.tipe_rka,H.nama_kegiatan,H.catatan 
				,U.nama as unit_pengaju,H.idunit_pengaju,H.grand_total,H.`status`,H.idjenis
				,H.tanggal_dibutuhkan,H.idunit,cek_logic(H.id) as nama_logic				
				,J.nama as jenis,UL.nama as nama_unit_lain
				,H.st_bendahara,H.st_user_unit,H.st_user_unit_lain
				,H.st_notif,H.st_pencairan,H.nominal_bayar
				,H.st_validasi,H.nominal_bayar_verifikasi,H.nominal_pencairan,H.st_berjenjang
				from rka_pengajuan H
				LEFT JOIN munitpelayanan U ON U.id=H.idunit_pengaju
				LEFT JOIN mjenis_pengajuan_rka J ON J.id=H.idjenis
				LEFT JOIN munitpelayanan UL ON UL.id=H.unit_lain
				LEFT JOIN rka_pengajuan_user_proses B ON B.idrka=H.id
				WHERE H.`status` IN ('3','4','5') ".$where."
				GROUP BY H.id
				) as tbl";
	// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama_kegiatan','no_pengajuan','catatan');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $iduser=$this->session->userdata('user_id');
      foreach ($list as $r) {
		  
          $no++;
          $row = array();
		  
		  $unit='';
		  $status_pengajuan='';
		  $btn_disable='';
		  // if ($r->nominal_bayar != $r->grand_total){
			  // $btn_disable='disabled';
		  // }
		  $notif='';
		  $pencairan='';
		  if ($r->st_notif){
			$notif='<span class="label label-primary">Sudah Notif</span>';	
			$unit='<span class="label label-success">ACTIVE</span>';	
		  }else{
			  $unit='<span class="label label-default">NOT ACTIVE</span>';
		  }
		  if ($r->st_pencairan){
			$pencairan='<span class="label label-danger">Bisa Dicairkan</span>';			  
					  
		  }else{
			 
		  }
		  $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('1411'))){
		  $aksi .= '<a href="'.site_url().'mrka_belanja/proses_belanja/'.$r->id.'/disabled" data-toggle="tooltip" title="Detail" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>';

		 }
		  if ($r->status!='5'){
			  if ($r->st_notif){
				if (UserAccesForm($user_acces_form,array('1412'))){
			   $aksi .= '<a href="'.site_url().'mrka_belanja/proses_belanja/'.$r->id.'" data-toggle="tooltip" title="Proses Pengajuan" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>';
			   }
			   }
			  // if ($r->st_pencairan){
			  if ($r->st_validasi){
				   if (UserAccesForm($user_acces_form,array('1413'))){
					$aksi .= '<button type="button" data-toggle="tooltip" '.$btn_disable.' title="Selesaikan" class="btn btn-primary btn-sm selesai"><i class="glyphicon glyphicon-ok"></i></button>';
				   }
			   }
			   // }
				if (UserAccesForm($user_acces_form,array('1414'))){
				 $aksi .= '<button type="button" data-toggle="tooltip" title="Ganti Unit Mengerjakan" class="btn btn-danger btn-sm ganti"><i class="glyphicon glyphicon-menu-right"></i></button>';
				}

		  }
		 
				if (UserAccesForm($user_acces_form,array('1415'))){
			$aksi .= '<a href="#" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
				}
			$aksi .= '</div>';
		
			if ($r->status!='1'){
			  if ($r->status=='3'){
				$nama_logic='<button title="Lihat User" class="btn btn-danger btn-xs user"><i class="si si-user"></i></button>';
			  }else{
				$nama_logic='';
			  }			 
		  }
		 $catatan='';
		  if ($r->catatan){
			  $catatan='<br>'.'<p class="text-primary"><i class="fa fa-sticky-note-o"></i> '.$r->catatan.'</p>';
		  }
          $row[] = $r->id;
          $row[] = $r->id;
          $row[] = $r->id;
          $row[] = $no;//3
          $row[] = $r->no_pengajuan.'<br>'.HumanDateLong($r->created_date);
          $row[] = HumanDateShort($r->tanggal_pengajuan);
          $row[] = tipe_rka($r->tipe_rka);
          $row[] = $r->nama_kegiatan.$catatan;
          $row[] = $r->unit_pengaju;
          $row[] = HumanDateShort($r->tanggal_dibutuhkan);
          $row[] = number_format($r->grand_total,0);
          $row[] = $r->jenis;
		  
		  $row[] = status_pengajuan($r->status).' '.$nama_logic.'<br>'.$notif.' '.$pencairan;
		  
		
          $row[] = $unit;
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function get_respon($idrka,$step){
	  $iduser=$this->session->userdata('user_id');
	  $q="SELECT *from rka_pengajuan_approval AP
			WHERE AP.st_aktif='1' AND AP.iduser='$iduser' AND AP.idrka='$idrka'";
	  return $this->db->query($q)->result();
  }
  function load_user_approval()
    {
	  $idrka=$this->input->post('idrka');	 
	  $where='';
	  $from="(
				SELECT rka_pengajuan.grand_total,U.`name` as user_nama, mlogic_detail.*  FROM rka_pengajuan 
				LEFT JOIN mlogic_unit ON  rka_pengajuan.idunit_pengaju = mlogic_unit.idunit
				LEFT JOIN mlogic_detail ON  mlogic_unit.idlogic = mlogic_detail.idlogic AND rka_pengajuan.tipe_rka = mlogic_detail.tipe_rka AND rka_pengajuan.idjenis = mlogic_detail.idjenis AND mlogic_detail.`status` = 1
				LEFT JOIN  musers U ON U.id=mlogic_detail.iduser
				WHERE rka_pengajuan.id ='$idrka' AND  calculate_logic(mlogic_detail.operand,rka_pengajuan.grand_total,mlogic_detail.nominal) = 1
				ORDER BY mlogic_detail.step,mlogic_detail.id
				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('user_nama');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
          $row[] = $r->step;
          $row[] = $r->user_nama;
          $row[] = ($r->proses_setuju=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
          $row[] = ($r->proses_tolak=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
         
          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
    function setuju_batal($id,$status){
		$q="call update_rka('$id', $status) ";
		$result=$this->db->query($q);
		echo json_encode($result);
	}
	function setuju_notif($id,$status){
		$q="UPDATE rka_pengajuan set st_notif='1' WHERE id='$id'";
		$result=$this->db->query($q);
		echo json_encode($result);
	}
	function setuju_pencairan($id,$status){
		$q="UPDATE rka_pengajuan set st_pencairan='1',status='4' WHERE id='$id'";
		$result=$this->db->query($q);
		echo json_encode($result);
	}
	function save_informasi(){
		$idrka=$this->input->post('idrka');
		$tgl_reminder=$this->input->post('tgl_reminder');
		$informasi=$this->input->post('informasi');
		$informasi_id=$this->input->post('informasi_id');
		if ($informasi_id==''){
			$data=array(
				'tanggal'=>date('Y-m-d H:i:s'),
				'user_nama'=>$this->session->userdata('user_name'),
				'user_id'=>$this->session->userdata('user_id'),
				'informasi'=>$informasi,
				'idrka'=>$idrka,
				'tgl_reminder'=>YMDFormat($tgl_reminder),
			);
			$result=$this->db->insert('rka_pengajuan_info',$data);
		}else{
			$data=array(
				'user_nama'=>$this->session->userdata('user_name'),
				'user_id'=>$this->session->userdata('user_id'),
				'informasi'=>$informasi,
				'tgl_reminder'=>YMDFormat($tgl_reminder),
			);
			$result=$this->db->update('rka_pengajuan_info',$data,array('id',$informasi_id));
		}
		echo json_encode($result);
	}
	function generate_cicilan($id){
		$q="SELECT *FROM rka_pengajuan WHERE id='$id'";
		$data_header=$this->db->query($q)->row_array();
		$nama_kegiatan=$data_header['nama_kegiatan'];
		if ($data_header['cara_pembayaran']=='2'){//CICILAN
			$q1="SELECT *from rka_pengajuan_cicilan where idpengajuan='$id'";
			$result=$this->db->query($q1)->result();
			foreach ($result as $row){
				$data_header['nama_kegiatan']=$nama_kegiatan.' '.$row->keterangan_cicilan;
				$data_header['tanggal_dibutuhkan']=YMDFormat($row->tanggal_cicilan);
				$data_header['cara_pembayaran']=1;
				$data_header['jenis_pembayaran']=2;
				$data_header['grand_total']=$row->nominal_cicilan;
				$data_header['status']=1;	
				$data_header['st_bendahara']=0;	
				$data_header['st_notif']=0;	
				$data_header['st_pencairan']=0;	
				$data_header['st_user_unit']=0;	
				$data_header['st_user_unit_lain']=0;	
				$data_header['st_user_bendahara']=0;	
				$data_header['unit_lain']=0;	
				$data_header['st_generate_cicilan']=1;	
				$data_header['id']=null;	
				$data_header['idpengajuan_asal']=$id;	
				// print_r($data_header);exit();
				$this->db->insert('rka_pengajuan',$data_header);
				$idpengajuan=$this->db->insert_id();
				
				$data_detail=array(
					'idpengajuan' => $idpengajuan,
					'nama_barang' => $nama_kegiatan.' '.$row->keterangan_cicilan,
					'merk_barang' => $nama_kegiatan.' '.$row->keterangan_cicilan,
					'kuantitas'=>1,
					'satuan'=>'-',
					'harga_satuan'=>$row->nominal_cicilan,
					'total_harga'=>$row->nominal_cicilan,
					'keterangan'=>$nama_kegiatan.' '.$row->keterangan_cicilan,
					'status'=>1,
					
					'created_by'=> $this->session->userdata('user_id'),
					'created_nama'=> $this->session->userdata('user_name'),
					'created_date'=>  date('Y-m-d H:i:s'),
				  ); 
				  $this->db->insert('rka_pengajuan_detail', $data_detail);
				  
			}
		}elseif ($data_header['cara_pembayaran']=='3'){//TERMIN FIX
			$q1="SELECT *from rka_pengajuan_termin where idpengajuan='$id'";
			$result=$this->db->query($q1)->result();
			foreach ($result as $row){
				$data_header['nama_kegiatan']=$nama_kegiatan.' '.$row->judul_termin;
				$data_header['tanggal_dibutuhkan']=YMDFormat($row->tanggal_termin);
				$data_header['cara_pembayaran']=1;
				$data_header['jenis_pembayaran']=2;
				$data_header['grand_total']=$row->nominal_termin;
				$data_header['status']=1;	
				$data_header['st_bendahara']=0;	
				$data_header['st_notif']=0;	
				$data_header['st_pencairan']=0;	
				$data_header['st_user_unit']=0;	
				$data_header['st_user_unit_lain']=0;	
				$data_header['st_user_bendahara']=0;	
				$data_header['unit_lain']=0;	
				$data_header['st_generate_cicilan']=1;	
				$data_header['id']=null;	
				$data_header['idpengajuan_asal']=$id;	
				// print_r($data_header);exit();
				$this->db->insert('rka_pengajuan',$data_header);
				$idpengajuan=$this->db->insert_id();
				
				$data_detail=array(
					'idpengajuan' => $idpengajuan,
					'nama_barang' => $nama_kegiatan.' '.$row->judul_termin,
					'merk_barang' => $nama_kegiatan.' '.$row->judul_termin,
					'kuantitas'=>1,
					'satuan'=>'-',
					'harga_satuan'=>$row->nominal_termin,
					'total_harga'=>$row->nominal_termin,
					'keterangan'=>$nama_kegiatan.' '.$row->deskripsi_termin,
					'status'=>1,
					
					'created_by'=> $this->session->userdata('user_id'),
					'created_nama'=> $this->session->userdata('user_name'),
					'created_date'=>  date('Y-m-d H:i:s'),
				  ); 
				  $this->db->insert('rka_pengajuan_detail', $data_detail);
			}
		}
		$result=$this->db->update('rka_pengajuan',array('status'=>'99'),array('id'=>$id));
		echo json_encode($result);
		// print_r($data_header);exit();
	}
	function save_pembayaran(){
		$idpembayaran=$this->input->post('idpembayaran');
		$idrka=$this->input->post('idrka');
		$jenis_kas_id=$this->input->post('jenis_kas_id');
		$sumber_kas_id=$this->input->post('sumber_kas_id');
		$nominal_bayar= RemoveComma($this->input->post('nominal_bayar'));
		$idmetode=$this->input->post('idmetode');
		$ket_pembayaran=$this->input->post('ket_pembayaran');
		$tanggal_pencairan=$this->input->post('tanggal_pencairan');
		
		if ($idpembayaran==''){
			$data=array(
				'idrka'=>$idrka,
				'jenis_kas_id'=>$jenis_kas_id,
				'sumber_kas_id'=>$sumber_kas_id,
				'nominal_bayar'=>$nominal_bayar,
				'idmetode'=>$idmetode,
				'ket_pembayaran'=>$ket_pembayaran,
				'tanggal_pencairan'=>YMDFormat($tanggal_pencairan),
				'created_by'=>$this->session->userdata('user_id'),
				'created_date'=>date('Y-m-d H:i:s'),
			);
			$result=$this->db->insert('rka_pengajuan_pembayaran',$data);
		}else{
			$data=array(
				'idrka'=>$idrka,
				'jenis_kas_id'=>$jenis_kas_id,
				'sumber_kas_id'=>$sumber_kas_id,
				'nominal_bayar'=>$nominal_bayar,
				'idmetode'=>$idmetode,
				'ket_pembayaran'=>$ket_pembayaran,
				'tanggal_pencairan'=>YMDFormat($tanggal_pencairan),
				'edited_by'=>$this->session->userdata('user_id'),
				'edited_date'=>date('Y-m-d H:i:s'),
			);
			$result=$this->db->update('rka_pengajuan_pembayaran',$data,array('id'=>$idpembayaran));
		}
		echo json_encode($result);
	}
	function hapus_informasi($id){
		$result=$this->db->query("Delete FROM rka_pengajuan_info WHERE id='$id'");
		echo json_encode($result);
	}
	function selesai(){
		$id=$this->input->post('id');
		// $this->db->query("UPDATE rka_pengajuan_pembayaran SET st_verifikasi='1' WHERE idrka='$id'");
		$bukti_pembayaran=$this->input->post('bukti_pembayaran');
		$result=$this->db->query("UPDATE rka_pengajuan set status='5',bukti_pembayaran='$bukti_pembayaran' WHERE id='$id'");
		// $this->Mrka_belanja_model->insert_validasi_rka($id);
		echo json_encode($result);
	}
	function ganti_unit(){
		
		$idpengajuan=$this->input->post('idpengajuan');
		$idunit_asal=$this->input->post('idunit_asal');
		$idunit_ganti=$this->input->post('idunit_ganti');
		$q="DELETE FROM rka_pengajuan_bendahara WHERE idrka='$idpengajuan' AND idunit='$idunit_asal'";
		$this->db->query($q);
		
		$q="INSERT INTO rka_pengajuan_bendahara (id,idrka,tipe,iduser,nama_user,status,idunit)  
					SELECT null,'$idpengajuan' as idrka,'2' as tipe,S.userid as iduser, M.`name` as nama_user,'1' as status,'$idunit_ganti' from munitpelayanan_user_setting S
					LEFT JOIN musers M ON S.userid=M.id
					WHERE S.idunit='$idunit_ganti'";
		$result=$this->db->query($q);
		// $result=$this->db->query("UPDATE rka_pengajuan set status='5',bukti_pembayaran='$bukti_pembayaran' WHERE id='$id'");
		echo json_encode($result);
	}
	function hapus_pembayaran(){
		$id=$this->input->post('id');
		$result=$this->db->query("Delete FROM rka_pengajuan_pembayaran WHERE id='$id'");
		echo json_encode($result);
	}
	function load_informasi($id){
		$q="SELECT *FROM rka_pengajuan_info H WHERE H.idrka='$id' ORDER BY H.tgl_reminder ASC";
		$row=$this->db->query($q)->result();
		$content='';
		$no=1;
		
		foreach($row as $r){
			$st_user_lain='';
			$content .='<tr>';
			
			$content .='<td>'.$no.'</td>';
			$content .='<td>'.HumanDateShort($r->tanggal).'</td>';
			$content .='<td>'.($r->informasi).'</td>';
			$content .='<td>'.($r->user_nama).'</td>';
			$content .='<td>'.HumanDateShort($r->tgl_reminder).'</td>';
			$content .='<td><button type="button" class="btn btn-sm btn-success edit_info"  title="Edit"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-sm btn-danger hapus_info"  title="Hapus"><i class="fa fa-times"></i></button></td>';
			$content .='<td hidden>'.($r->id).'</td>';
			$content .='</tr>';
			$no=$no+1;
		}
		$arr['detail']=$content;
		$this->output->set_output(json_encode($arr));
	
	}
	
	function save(){
		$id=$this->Mrka_belanja_model->saveData();
		if($id==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mrka_approval','location');
		}
	}
	function tolak(){
		$id=$this->input->post('id_approval');
		$alasan_tolak=$this->input->post('alasan_tolak');
		$q="call update_rka('$id', 2) ";
		$this->db->query($q);
		$result=$this->db->update('rka_pengajuan_approval',array('alasan_tolak'=>$alasan_tolak),array('id'=>$id));
		echo json_encode($result);
	}
	function proses_belanja($id,$disabel=''){
		
			$data = $this->Mrka_pengajuan_model->getSpecified($id);
			$data['tanggal_dibutuhkan']=HumanDateShort($data['tanggal_dibutuhkan']);
			$data['tanggal_pengajuan']=HumanDateShort($data['tanggal_pengajuan']);
			$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
			$data['list_unit_lain'] 		= $this->Mrka_belanja_model->list_unit_lain();
			$data['list_jenis'] 		= $this->Mrka_pengajuan_model->list_jenis($data['tipe_rka']);
			$data['list_detail'] 		= $this->Mrka_pengajuan_model->list_detail($id);
			$data['list_vendor'] 	= $this->Mrka_pengajuan_model->list_vendor();
			$data['list_klasifikasi'] 		= $this->Mrka_pengajuan_model->list_klasifikasi();
			$data['list_dp_cicilan'] 	= $this->Mrka_pengajuan_model->list_cicilan($id,1);
			$data['list_cicilan'] 	= $this->Mrka_pengajuan_model->list_cicilan($id,2);
			$data['list_jenis_kas'] 	= $this->Mrka_belanja_model->list_jenis_kas();
			// print_r($data['list_cicilan'] );exit();
			$data['list_dp_termin'] 	=  $this->Mrka_pengajuan_model->list_dp_termin($id);
			$data['list_rka'] 	= $this->Mrka_pengajuan_model->list_rka();
			$data['list_prespektif'] 	= $this->Mrka_pengajuan_model->list_prespektif();
			$data['disabel'] 			= $disabel;
			$data['error'] 			= '';
			$data['title'] 			= 'Review Pengajuan Kegiatan';
			$data['content'] 		= 'Mrka_belanja/manage';
			$data['breadcrum']	= array(
															array("RSKB Halmahera",'#'),
															array("Create RKA",'#'),
														array("Tambah",'mrka_pengajuan')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		
	}
	public function upload() {
       $uploadDir = './assets/upload/rka_pengajuan';
		if (!empty($_FILES)) {
			 $idrka = $this->input->post('idrka');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['idrka'] 	= $idrka;
			$detail['file_name']		= $file_name;
			$detail['size']		= formatSizeUnits($size);
			$detail['tanggal_upload']	= date('Y-m-d H:i:s');
			$detail['user_upload']	= $this->session->userdata('user_name');
			$this->db->insert('rka_pengajuan_upload', $detail);
		}
    }
	function get_pembayaran($id){		
		$arr['detail'] = $this->Mrka_belanja_model->get_pembayaran($id);
		$arr['detail']['tanggal_pencairan']=HumanDateShort($arr['detail']['tanggal_pencairan']);
		$this->output->set_output(json_encode($arr));
	}
	function refresh_image($id){		
		$arr['detail'] = $this->Mrka_belanja_model->refresh_image($id);
		$this->output->set_output(json_encode($arr));
	}
	function refresh_pembayaran($id){	
// rka_pengajuan_pembayaran	
		$q="SELECT  H.*,JK.nama as jenis_kas,SK.nama as sumber_kas from rka_pengajuan_pembayaran H
			LEFT JOIN mjenis_kas JK ON JK.id=H.jenis_kas_id
			LEFT JOIN msumber_kas SK ON SK.id=H.sumber_kas_id
			WHERE H.idrka='$id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		$total=0;
		$no=1;
		foreach ($row as $r){
			$total=$total+$r->nominal_bayar;
			
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-center">'.$r->jenis_kas.'</td>';
			$tabel .='<td class="text-center">'.$r->sumber_kas.'</td>';
			$tabel .='<td class="text-center">'.metodePembayaran_bendahara($r->idmetode).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal_bayar).'</td>';
			$tabel .='<td class="text-center">'.($r->tanggal_pencairan?HumanDateShort($r->tanggal_pencairan):'').'</td>';
			$tabel .='<td class="text-left">'.$r->ket_pembayaran.'</td>';
			$tabel .='<td><button type="button" class="btn btn-sm btn-success edit_pembayaran"  title="Edit"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-sm btn-danger hapus_pembayaran"  title="Hapus"><i class="fa fa-times"></i></button></td>';
			// $tabel .='<td><button type="button" class="btn btn-sm btn-danger hapus_pembayaran"  title="Hapus"><i class="fa fa-times"></i></button></td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		
		$arr['detail'] = $tabel;
		$arr['nominal_bayar'] = $total;
		$this->output->set_output(json_encode($arr));
	}
	function list_sumber_kas(){		
		$jenis_kas_id=$this->input->post('jenis_kas_id');
		$opsi='';
		$q="SELECT *from msumber_kas M WHERE M.jenis_kas_id='$jenis_kas_id'";
		$rows=$this->db->query($q)->result();
		$opsi .='<option value="#" selected>- Pilih Opsi -</option>';
		foreach($rows as $row){
			$opsi .='<option value="'.$row->id.'">'.$row->nama.'</option>';
		}
		$arr['detail'] = $opsi;
		$this->output->set_output(json_encode($arr));
	}
	function hapus_file(){
		$id=$this->input->post('id');
		$row = $this->Mrka_belanja_model->get_file_name($id);
		if(file_exists('./assets/upload/rka_pengajuan/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/rka_pengajuan/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from rka_pengajuan_upload WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
}
