<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tvalidasi_retur extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tvalidasi_retur_model','model');
	}

	function index() {
		$data=array(
			'st_posting'=>'#',
			'tipe'=>'#',
			'status_penerimaan'=>'#',
			'nojurnal'=>'',
			'no_terima'=>'',
			'jenis_retur'=>'',
			'status'=>'#',
			'asal_transaksi'=>'',
			'tanggalterima2'=>'',
			'tanggal_trx1'=>'',
			'tanggal_trx2'=>'',
		);
		$data['error'] 			= '';
		$data['list_distributor'] 	= $this->model->list_distributor();
		$data['title'] 			= 'Validasi Jurnal Retur';
		$data['content'] 		= 'Tvalidasi_retur/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Retur",'tverifikasi_transaksi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function detail($id,$disabel='') {
		$data=$this->model->getHeader($id);
		// print_r($data);exit();
		$data['error'] 			= '';
		$data['disabel'] 			= $disabel;
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['title'] 			= 'Detail Validasi Akuntansi';
		$data['content'] 		= 'Tvalidasi_retur/detail';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Retur",'tverifikasi_transaksi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		$batas_batal=$this->db->query("SELECT batas_batal FROM msetting_jurnal_retur WHERE id='1'")->row('batas_batal');
		
		$iddistributor=$this->input->post('iddistributor');
		$nojurnal=$this->input->post('nojurnal');
		$no_terima=$this->input->post('no_terima');
		$jenis_retur=$this->input->post('jenis_retur');
		$status_penerimaan=$this->input->post('status_penerimaan');
		$asal_transaksi=$this->input->post('asal_transaksi');
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		$st_posting=$this->input->post('st_posting');
		$where='';
		if ($iddistributor !='#'){
			$where .=" AND H.iddistributor='$iddistributor' ";
		}
		if ($st_posting !='#'){
			$where .=" AND H.st_posting='$st_posting' ";
		}
		
		if ($nojurnal !=''){
			$where .=" AND H.nojurnal LIKE '%".$nojurnal."%' ";
		}
		if ($no_terima !=''){
			$where .=" AND H.notransaksi LIKE '%".$no_terima."%' ";
		}
		if ($jenis_retur !='#'){
			$where .=" AND H.jenis_retur='$jenis_retur' ";
		}
		if ($status_penerimaan !='#'){
			$where .=" AND H.status_penerimaan='$status_penerimaan' ";
		}
		if ($asal_transaksi !='#'){
			$where .=" AND H.asal_transaksi='$asal_transaksi' ";
		}
		
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(H.tanggal_transaksi) >='".YMDFormat($tanggal_trx1)."' AND DATE(H.tanggal_transaksi) <='".YMDFormat($tanggal_trx2)."'";
        }
        $from = "(
					SELECT DATEDIFF(NOW(), H.posting_date) as umur_posting,H.idretur,
					H.id,H.st_posting,H.nojurnal,H.notransaksi,H.jenis_retur,H.status_penerimaan,H.asal_transaksi,H.idtransaksi
					,H.tanggal_transaksi,H.iddistributor,H.distributor,H.nominal_retur,H.nominal_ganti,H.nominal_bayar,H.nominal_hutang
					,P.idpengembalian
					FROM tvalidasi_retur H
					LEFT JOIN tretur_penerimaan P ON P.id=H.idtransaksi AND H.asal_transaksi='1'
					WHERE H.`status` !='0' ".$where."

					ORDER BY H.id DESC
				) as tbl";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nofakturexternal','distributor','notransaksi');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

			
        foreach ($list as $r) {
            $no++;
            $row = array();
			$disabel_btn='';
			// if ($r->stverif_kbo>=3){
				// $disabel_btn='disabled';
			// }
			if ($r->umur_posting>$batas_batal){
				$disabel_btn='disabled';
			}
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tkontrabon_verifikasi/');
            $url_gudang_terima        = site_url('tgudang_penerimaan/');
            $url_gudang        = site_url('tgudang_pengembalian/');
            $url_validasi        = site_url('tvalidasi_retur/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->id;
            $row[] = $r->st_posting;
            $row[] = $no;
            // $row[] = $this->tipe_pemesanan($r->tipe,$r->jenis_retur).$status_retur;
            $row[] = $r->nojurnal;
            $row[] = $r->notransaksi;
            $row[] = tipe_pemesanan_kbo(1,$r->jenis_retur);
            $row[] = status_penerimaan($r->status_penerimaan);
            $row[] = asal_transaksi_retur($r->asal_transaksi);
            $row[] = HumanDateShort($r->tanggal_transaksi);
            $row[] = $r->distributor;//9
            $row[] = number_format($r->nominal_retur,2);
            $row[] = number_format($r->nominal_ganti,2);
            $row[] = number_format($r->nominal_bayar-$r->nominal_hutang,2);
            $row[] = ($r->st_posting=='1'?text_success('TELAH DIPOSTING'):text_default('MENUGGU DIPOSTING'));
			if ($r->asal_transaksi=='2'){
					$aksi .= '<a href="'.$url_gudang.'detail/'.$r->idretur.'" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
				
			}else{
					$aksi .= '<a href="'.$url_gudang.'detail/'.$r->idpengembalian.'" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
					if ($r->idretur==null){
						$this->db->update('tvalidasi_retur',array('idretur'=>$r->idpengembalian),array('id'=>$r->id));						
					}
				
			}
					$aksi .= '<a href="'.$url_validasi.'detail/'.$r->id.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
					if ($r->st_posting=='1'){
					$aksi .= '<button title="Batal Posting" '.$disabel_btn.' class="btn btn-xs btn-danger batalkan"><i class="fa fa-close"></i></button>';						
					}else{
					$aksi .= '<button title="Posting" class="btn btn-xs btn-success posting"><i class="si si-arrow-right"></i></button>';
						
					}
					$aksi .= '<button title="Print" class="btn btn-xs btn-success"><i class="fa fa-print"></i></button>';
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	
	function status_penerimaan($tipe){
		if ($tipe=='1'){
			$tipe='<span class="label label-success">TUNAI</span>';
		}elseif($tipe=='2'){
			$tipe='<span class="label label-danger">KREDIT</span>';
		}else{
			$tipe='<span class="label label-default">-</span>';
		}
		
		return $tipe;
	}
	
	public function posting()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'1',
			'posting_by'=>$this->session->userdata('user_id'),
			'posting_nama'=>$this->session->userdata('user_name'),
            'posting_date'=>date('Y-m-d H:i:s')
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_retur', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function posting_all()
    {
        $id = $this->input->post('id');
		foreach($id as $index=>$val){
			 $data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $val);
			$result=$this->db->update('tvalidasi_retur', $data);
		}
       
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	
	public function batalkan()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'0',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_retur', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function get_transaksi_detail($iddet,$tipe='1')
    {
		if ($tipe=='1'){
        $q="SELECT *from tvalidasi_retur_barang D WHERE D.id='$iddet'";
			
		}else{
			
        $q="SELECT *from tvalidasi_retur_ganti D WHERE D.id='$iddet'";
		}
       
        $result=$this->db->query($q)->row_array();
        $this->output->set_output(json_encode($result));
    }
	function save_detail(){
		// print_r($this->input->post());exit();
		// $id=$this->model->saveData();
		if($this->model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tvalidasi_retur','location');
		}
	
	}
	function load_retur(){
		
		$id=$this->input->post('id');
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT D.id,K.nama_tipe as nama_tipe,D.namabarang,D.nominal_harga,IFNULL(D.total_ppn,0) as total_ppn,D.total_diskon,D.idakun_beli,D.idakun_ppn,D.idakun_diskon 
				
				from tvalidasi_retur_barang D
				LEFT JOIN mdata_tipebarang K ON K.id=D.idtipe
				WHERE D.idvalidasi='$id'
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			
			$aksi       = '<div class="btn-group">';
			$select_beli='<select name="idakun_beli[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_beli).'
						</select>';
			$select_ppn='<select name="idakun_ppn[]" '.$disabel.' '.($r->total_ppn=='0'?"disabled":"").' class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_ppn).'
						</select>';
			$select_diskon='<select name="idakun_diskon[]" '.$disabel.' '.($r->total_diskon=='0'?"disabled":"").' class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_diskon).'
						</select>';
		
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td>'.$r->nama_tipe.'<input type="hidden" name="iddet[]" value="'.$r->id.'"></td>';
			$tbl .='<td>'.$r->namabarang.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_harga,2).'</td>';
			$tbl .='<td>'.$select_beli.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->total_ppn,2).'</td>';
			$tbl .='<td>'.$select_ppn.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->total_diskon,2).'</td>';
			$tbl .='<td>'.$select_diskon.'</td>';
				$aksi .= '<button type="button" title="Lihat Detail" class="btn btn-default btn-sm" onclick="get_transaksi_detail('.$r->id.',1)"><i class="fa fa-eye"></i></button>';
			$tbl .='<td class="text-center">'.$aksi.'</td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_ganti(){
		
		$id=$this->input->post('id');
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT D.id,K.nama_tipe as nama_tipe,D.namabarang,D.nominal_harga,IFNULL(D.total_ppn,0) as total_ppn,D.total_diskon,D.idakun_beli,D.idakun_ppn,D.idakun_diskon 
				
				from tvalidasi_retur_ganti D
				LEFT JOIN mdata_tipebarang K ON K.id=D.idtipe
				WHERE D.idvalidasi='$id'
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			
			$aksi       = '<div class="btn-group">';
			$select_beli='<select name="idakun_beli_ganti[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_beli).'
						</select>';
			$select_ppn='<select name="idakun_ppn_ganti[]" '.$disabel.' '.($r->total_ppn=='0'?"disabled":"").' class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_ppn).'
						</select>';
			$select_diskon='<select name="idakun_diskon_ganti[]" '.$disabel.' '.($r->total_diskon=='0'?"disabled":"").' class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_diskon).'
						</select>';
		
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td>'.$r->nama_tipe.'<input type="hidden" name="iddet_ganti[]" value="'.$r->id.'"></td>';
			$tbl .='<td>'.$r->namabarang.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_harga,2).'</td>';
			$tbl .='<td>'.$select_beli.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->total_ppn,2).'</td>';
			$tbl .='<td>'.$select_ppn.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->total_diskon,2).'</td>';
			$tbl .='<td>'.$select_diskon.'</td>';
				$aksi .= '<button type="button" title="Lihat Detail" class="btn btn-default btn-sm" onclick="get_transaksi_detail('.$r->id.',2)"><i class="fa fa-eye"></i></button>';
			$tbl .='<td class="text-center">'.$aksi.'</td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_bayar(){
		
		$id=$this->input->post('id');
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT D.id,M.nama as sumberkas,R.metode_bayar, D.jenis_kas_id,sumber_kas_id,D.idmetode,D.nominal_bayar,D.idakun
						FROM `tvalidasi_retur_bayar` D
						LEFT JOIN msumber_kas M ON M.id=D.sumber_kas_id
						LEFT JOIN ref_metode R ON R.id=D.idmetode
						WHERE D.idvalidasi='$id'
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			
			$aksi       = '<div class="btn-group">';
			$select_akun='<select name="idakun_bayar[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun).'
						</select>';
			
		
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td>'.$r->sumberkas.'<input type="hidden" name="iddet_bayar[]" value="'.$r->id.'"></td>';
			$tbl .='<td>'.$r->metode_bayar.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_bayar,2).'</td>';			
			$tbl .='<td>'.$select_akun.'</td>';
				$aksi .= '<button type="button" title="Lihat Detail" class="btn btn-default btn-sm" onclick="get_transaksi_detail_bayar('.$r->id.')"><i class="fa fa-eye"></i></button>';
			$tbl .='<td class="text-center">'.$aksi.'</td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_ringkasan(){
		
		$id=$this->input->post('id');
		
		$disabel=$this->input->post('disabel');
		
        $q = "
				SELECT * FROM (
					SELECT A.noakun,A.namaakun,SUM(T.debet) as debet,SUM(T.kredit) as kredit,GROUP_CONCAT(T.ket) as ket FROM (
					SELECT H.idakun_beli as idakun
					,SUM(CASE WHEN H.posisi_beli='D' THEN H.nominal_harga ELSE 0 END) as debet 
					,SUM(CASE WHEN H.posisi_beli='K' THEN H.nominal_harga ELSE 0 END) as kredit 
					,'RETUR BARANG' as ket,'1' as urutan
					from tvalidasi_retur_barang H
					WHERE H.idvalidasi='$id'
					UNION ALL
					SELECT H.idakun_ppn as idakun
					,SUM(CASE WHEN H.posisi_ppn='D' THEN H.total_ppn ELSE 0 END) as debet 
					,IFNULL(SUM(CASE WHEN H.posisi_ppn='K' THEN H.total_ppn ELSE 0 END),0) as kredit 
					,'RETUR (PPN)' as ket,'3' as urutan
					from tvalidasi_retur_barang H
					WHERE H.idvalidasi='$id'
					UNION ALL
					SELECT H.idakun_diskon as idakun
					,SUM(CASE WHEN H.posisi_diskon='D' THEN H.total_diskon ELSE 0 END) as debet 
					,SUM(CASE WHEN H.posisi_diskon='K' THEN H.total_diskon ELSE 0 END) as kredit 
					,'RETUR (DISKON)' as ket,'2' as urutan
					from tvalidasi_retur_barang H
					WHERE H.idvalidasi='$id'

					UNION ALL

					SELECT H.idakun_beli as idakun
					,SUM(CASE WHEN H.posisi_beli='D' THEN H.nominal_harga ELSE 0 END) as debet 
					,SUM(CASE WHEN H.posisi_beli='K' THEN H.nominal_harga ELSE 0 END) as kredit 
					,'PENGGANTI RETUR' as ket,'4' as urutan
					from tvalidasi_retur_ganti H
					WHERE H.idvalidasi='$id'
					UNION ALL
					SELECT H.idakun_ppn as idakun
					,IFNULL(SUM(CASE WHEN H.posisi_ppn='D' THEN H.total_ppn ELSE 0 END),0) as debet 
					,SUM(CASE WHEN H.posisi_ppn='K' THEN H.total_ppn ELSE 0 END) as kredit 
					,'PENGGANTI PPN' as ket,'6' as urutan
					from tvalidasi_retur_ganti H
					WHERE H.idvalidasi='$id'
					UNION ALL
					SELECT H.idakun_diskon as idakun
					,SUM(CASE WHEN H.posisi_diskon='D' THEN H.total_diskon ELSE 0 END) as debet 
					,SUM(CASE WHEN H.posisi_diskon='K' THEN H.total_diskon ELSE 0 END) as kredit 
					,'PENGGANTI DISKON' as ket,'5' as urutan
					from tvalidasi_retur_ganti H
					WHERE H.idvalidasi='$id'

					UNION ALL

					SELECT H.idakun
					,SUM(CASE WHEN H.posisi_akun='D' THEN H.nominal_bayar ELSE 0 END) as debet 
					,SUM(CASE WHEN H.posisi_akun='K' THEN H.nominal_bayar ELSE 0 END) as kredit 
					,'PEMBAYARAN' as ket,'7' as urutan
					from tvalidasi_retur_bayar H
					WHERE H.idvalidasi='$id'
					) T 
					LEFT JOIN makun_nomor A ON A.id=T.idakun
					WHERE (T.debet) > 0
					GROUP BY T.idakun
					ORDER BY urutan
					) KD

					UNION ALL

					SELECT * FROM (
					SELECT A.noakun,A.namaakun,SUM(T.debet) as debet,SUM(T.kredit) as kredit,GROUP_CONCAT(T.ket) as ket FROM (
					SELECT H.idakun_beli as idakun
					,SUM(CASE WHEN H.posisi_beli='D' THEN H.nominal_harga ELSE 0 END) as debet 
					,SUM(CASE WHEN H.posisi_beli='K' THEN H.nominal_harga ELSE 0 END) as kredit 
					,'RETUR BARANG' as ket,'1' as urutan
					from tvalidasi_retur_barang H
					WHERE H.idvalidasi='$id'
					UNION ALL
					SELECT H.idakun_ppn as idakun
					,SUM(CASE WHEN H.posisi_ppn='D' THEN H.total_ppn ELSE 0 END) as debet 
					,IFNULL(SUM(CASE WHEN H.posisi_ppn='K' THEN H.total_ppn ELSE 0 END),0) as kredit 
					,'RETUR (PPN)' as ket,'3' as urutan
					from tvalidasi_retur_barang H
					WHERE H.idvalidasi='$id'
					UNION ALL
					SELECT H.idakun_diskon as idakun
					,SUM(CASE WHEN H.posisi_diskon='D' THEN H.total_diskon ELSE 0 END) as debet 
					,SUM(CASE WHEN H.posisi_diskon='K' THEN H.total_diskon ELSE 0 END) as kredit 
					,'RETUR (DISKON)' as ket,'2' as urutan
					from tvalidasi_retur_barang H
					WHERE H.idvalidasi='$id'

					UNION ALL

					SELECT H.idakun_beli as idakun
					,SUM(CASE WHEN H.posisi_beli='D' THEN H.nominal_harga ELSE 0 END) as debet 
					,SUM(CASE WHEN H.posisi_beli='K' THEN H.nominal_harga ELSE 0 END) as kredit 
					,'PENGGANTI RETUR' as ket,'4' as urutan
					from tvalidasi_retur_ganti H
					WHERE H.idvalidasi='$id'
					UNION ALL
					SELECT H.idakun_ppn as idakun
					,IFNULL(SUM(CASE WHEN H.posisi_ppn='D' THEN H.total_ppn ELSE 0 END),0) as debet 
					,SUM(CASE WHEN H.posisi_ppn='K' THEN H.total_ppn ELSE 0 END) as kredit 
					,'PENGGANTI PPN' as ket,'6' as urutan
					from tvalidasi_retur_ganti H
					WHERE H.idvalidasi='$id'
					UNION ALL
					SELECT H.idakun_diskon as idakun
					,SUM(CASE WHEN H.posisi_diskon='D' THEN H.total_diskon ELSE 0 END) as debet 
					,SUM(CASE WHEN H.posisi_diskon='K' THEN H.total_diskon ELSE 0 END) as kredit 
					,'PENGGANTI DISKON' as ket,'5' as urutan
					from tvalidasi_retur_ganti H
					WHERE H.idvalidasi='$id'

					UNION ALL

					SELECT H.idakun
					,SUM(CASE WHEN H.posisi_akun='D' THEN H.nominal_bayar ELSE 0 END) as debet 
					,SUM(CASE WHEN H.posisi_akun='K' THEN H.nominal_bayar ELSE 0 END) as kredit 
					,'PEMBAYARAN' as ket,'7' as urutan
					from tvalidasi_retur_bayar H
					WHERE H.idvalidasi='$id'
					
					UNION ALL
					SELECT H.idakun_hutang as idakun,0 as debent,H.nominal_hutang as kredit 
					,CONCAT('PENAMBAH HUTANG ',H.distributor,' - No. Retur',H.nopengembalian) as ket,8 as  urutan
					FROM `tvalidasi_retur` H
					WHERE H.id='$id' AND H.nominal_hutang > 0
					) T 
					LEFT JOIN makun_nomor A ON A.id=T.idakun
					WHERE (T.kredit) > 0
					GROUP BY T.idakun
					ORDER BY urutan
					) KK


				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total_D=0;
		$total_K=0;
		foreach($rows as $r){
			$no++;
			$total_D=$total_D + $r->debet;
			$total_K=$total_K + $r->kredit;
			
			
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td>'.$r->noakun.'</td>';
			$tbl .='<td>'.$r->namaakun.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->debet,2).'</td>';
			$tbl .='<td class="text-right">'.number_format($r->kredit,2).'</td>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<td colspan="3" class="text-right"><strong>TOTAL</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_D,2).'</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_K,2).'</strong></td>';
		
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function list_akun($list,$idakun){
		$hasil='';
		foreach($list as $row){
			$hasil .='<option value="'.$row->id.'" '.($row->id==$idakun?"selected":"").'>'.$row->noakun.' - '.$row->namaakun.'</option>';
		}
		return $hasil;
	}
}
