<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpasca_anestesi extends CI_Controller {

	/**
	 * Master Penialain Pemulihan Pasca Anestesi controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpasca_anestesi_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Penialain Pemulihan Pasca Anestesi';
		$data['content'] 		= 'Mpasca_anestesi/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Penialain Pemulihan Pasca Anestesi",'#'),
									    			array("List",'mpasca_anestesi')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'isi_footer' 		=> '',
			'isi_header' 		=> '',
			'idtipe' 		=> '',
			'staktif' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Penialain Pemulihan Pasca Anestesi';
		$data['content'] 		= 'Mpasca_anestesi/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Penialain Pemulihan Pasca Anestesi",'#'),
									    			array("Tambah",'mpasca_anestesi')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mpasca_anestesi_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'isi_header' 		=> $row->isi_header,
					'idtipe' 		=> $row->idtipe,
					'isi_footer' 		=> $row->isi_footer,
					'staktif' 				=> $row->staktif
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Penialain Pemulihan Pasca Anestesi';
				$data['content']	 	= 'Mpasca_anestesi/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Penialain Pemulihan Pasca Anestesi",'#'),
											    			array("Ubah",'mpasca_anestesi')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mpasca_anestesi','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpasca_anestesi');
		}
	}
	function setting_range_nilai($id){
		if($id != ''){
			$row = $this->Mpasca_anestesi_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'isi_header' 		=> $row->isi_header,
					'idtipe' 		=> $row->idtipe,
					'isi_footer' 		=> $row->isi_footer,
					'staktif' 				=> $row->staktif
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Penialain Pemulihan Pasca Anestesi';
				$data['content']	 	= 'Mpasca_anestesi/manage_setting';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Penialain Pemulihan Pasca Anestesi",'#'),
											    			array("Ubah",'mpasca_anestesi')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mpasca_anestesi','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpasca_anestesi');
		}
	}

	function delete($id){
		$this->Mpasca_anestesi_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mpasca_anestesi','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('isi_header', 'Isi Header', 'trim|required');
		$this->form_validation->set_rules('idtipe', 'Tipe', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->Mpasca_anestesi_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mpasca_anestesi/update/'.$id,'location');
				}
			} else {
				if($this->Mpasca_anestesi_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mpasca_anestesi/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mpasca_anestesi/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Penialain Pemulihan Pasca Anestesi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Penialain Pemulihan Pasca Anestesi",'#'),
															array("Tambah",'mpasca_anestesi')
													);
		}else{
			$data['title'] = 'Ubah Master Penialain Pemulihan Pasca Anestesi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Penialain Pemulihan Pasca Anestesi",'#'),
															array("Ubah",'mpasca_anestesi')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
			$this->from   = 'mpasca_anestesi';
			$this->join 	= array();
			$this->where  = array(
				'staktif' => '1'
			);
			$this->order  = array(
				'id' => 'ASC'
			);
			$this->group  = array();
		
		$this->column_search   = array('nama','isi_header');
      $this->column_order    = array('nama','isi_header');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama.'        '.($r->idtipe=='1'?text_danger('DEFAULT'):'');
		$aksi = '<div class="btn-group">';
        if (UserAccesForm($user_acces_form,array('2245'))){
            $aksi .= '<a href="'.site_url().'mpasca_anestesi/setting_range_nilai/'.$r->id.'" data-toggle="tooltip" title="Setting Logic Nilai" class="btn btn-success btn-sm"><i class="si si-settings"></i></a>';
        }
		if (UserAccesForm($user_acces_form,array('2243'))){
            $aksi .= '<a href="'.site_url().'mpasca_anestesi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
        }
        if (UserAccesForm($user_acces_form,array('2244'))){
            $aksi .= '<a href="#" data-urlindex="'.site_url().'mpasca_anestesi" data-urlremove="'.site_url().'mpasca_anestesi/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
        }
        $aksi .= '</div>';
		$row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_parameter()
	{
			$mrisiko_id=$this->input->post('mrisiko_id');
		
			$this->select = array();
			$from="
					(
						SELECT H.*,
						GROUP_CONCAT(CONCAT(M.deskripsi_nama,' (',M.skor,')') SEPARATOR '#') as jawaban
						FROM `mpasca_anestesi_param` H
						LEFT JOIN mpasca_anestesi_param_skor M ON M.parameter_id=H.id AND M.staktif='1'
						where H.staktif='1' AND H.mrisiko_id='$mrisiko_id'
						GROUP BY H.id
						ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('parameter_nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->parameter_nama.'<br>'.($r->jawaban?'&nbsp;&nbsp;'.$this->render_jawaban_detail($r->id):'');
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="add_jawaban('.$r->id.')" type="button" title="Add Value" class="btn btn-warning btn-xs "><i class="fa fa-plus"></i></button>';	
		  $aksi .= '<button onclick="edit_parameter('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_parameter('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function render_jawaban($jawaban){
	  $hasil='';
	  $arr=explode('#',$jawaban);
	  foreach($arr as $index=>$val){
		  $hasil .='<span class="badge ">'.$val.'</span> ';
	  }
	  return $hasil;
  }
  function load_nilai()
	{
			$mrisiko_id=$this->input->post('mrisiko_id');
		
			$this->select = array();
			$from="
					(
						SELECT H.*
						
						FROM `mpasca_anestesi_setting_nilai` H
						where H.staktif='1' AND H.mrisiko_id='$mrisiko_id'
						GROUP BY H.id
						ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('ref_nilai');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->skor_1.' - '.$r->skor_2;
          $result[] = $r->ref_nilai;
          // $result[] = ($r->st_tindakan=='1'?text_success('YA'):text_default('TIDAK'));
          // $result[] = $r->nama_tindakan;
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_nilai('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_nilai('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function simpan_nilai(){
		$parameter_id=$this->input->post('parameter_id');
		$mrisiko_id=$this->input->post('mrisiko_id');
		$skor_1=$this->input->post('skor_1');
		$skor_2=$this->input->post('skor_2');
		$ref_nilai=$this->input->post('ref_nilai');
		$ref_nilai_id=$this->input->post('ref_nilai_id');
		$data=array(
			'mrisiko_id'=>$this->input->post('mrisiko_id'),
			'skor_1'=>$this->input->post('skor_1'),
			'skor_2'=>$this->input->post('skor_2'),
			'ref_nilai'=>$this->input->post('ref_nilai'),
			'ref_nilai_id'=>$this->input->post('ref_nilai_id'),
			// 'st_tindakan'=>$this->input->post('st_tindakan'),
			// 'nama_tindakan'=>$this->input->post('nama_tindakan'),
			'staktif'=>1,
		);
		if ($parameter_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mpasca_anestesi_setting_nilai',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$parameter_id);
		    $hasil=$this->db->update('mpasca_anestesi_setting_nilai',$data);
		}
		  
		  json_encode($hasil);
	}
	function simpan_parameter(){
		$mrisiko_id=$this->input->post('mrisiko_id');
		$parameter_id=$this->input->post('parameter_id');
		$data=array(
			'mrisiko_id'=>$this->input->post('mrisiko_id'),
			'parameter_nama'=>$this->input->post('parameter_nama'),
			'staktif'=>1,
		);
		if ($parameter_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mpasca_anestesi_param',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$parameter_id);
		    $hasil=$this->db->update('mpasca_anestesi_param',$data);
		}
		  
		  json_encode($hasil);
	}
	function simpan_jawaban(){
		$skor_id=$this->input->post('skor_id');
		$parameter_id=$this->input->post('parameter_id');
		$data=array(
			'parameter_id'=>$this->input->post('parameter_id'),
			'skor'=>$this->input->post('skor'),
			'jenis'=>$this->input->post('jenis'),
			'deskripsi_nama'=>$this->input->post('deskripsi_nama'),
			'staktif'=>1,
		);
		if ($skor_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mpasca_anestesi_param_skor',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$skor_id);
		    $hasil=$this->db->update('mpasca_anestesi_param_skor',$data);
		}
		  
		  json_encode($hasil);
	}
	function hapus_nilai(){
	  $parameter_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$parameter_id);
		$hasil=$this->db->update('mpasca_anestesi_setting_nilai',$data);
	  
	  json_encode($hasil);
	  
  }
  function hapus_parameter(){
	  $parameter_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$parameter_id);
		$hasil=$this->db->update('mpasca_anestesi_param',$data);
	  
	  json_encode($hasil);
	  
  }
  function hapus_jawaban(){
	  $skor_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$skor_id);
		$hasil=$this->db->update('mpasca_anestesi_param_skor',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_parameter(){
	  $parameter_id=$this->input->post('id');
	  $q="SELECT *FROM mpasca_anestesi_param H WHERE H.id='$parameter_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function find_nilai(){
	  $parameter_id=$this->input->post('id');
	  $q="SELECT *FROM mpasca_anestesi_setting_nilai H WHERE H.id='$parameter_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function find_jawaban(){
	  $skor_id=$this->input->post('id');
	  $q="SELECT *FROM mpasca_anestesi_param_skor H WHERE H.id='$skor_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function load_jawaban()
	{
		$parameter_id=$this->input->post('parameter_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*,M.ref
							FROM `mpasca_anestesi_param_skor` H
							LEFT JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='407'
							where H.staktif='1' AND H.parameter_id='$parameter_id'
							ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('deskripsi_nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->skor;
          // $result[] = $this->render_jawaban_detail($r->id);
          $result[] = ($r->ref);
          $result[] = $r->deskripsi_nama;
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_jawaban('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_jawaban('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function render_jawaban_detail($parameter_id){
	   $q="SELECT M.ref as nama_jenis,GROUP_CONCAT(H.deskripsi_nama,' (',H.skor,')' SEPARATOR '#') as jawaban_opr 
FROM `mpasca_anestesi_param_skor` H
			LEFT JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='407'
			WHERE H.parameter_id='$parameter_id' AND H.staktif='1'
			GROUP BY H.jenis";
	  $hasil='';
	  $list_data=$this->db->query($q)->result();
	  foreach ($list_data as $r){
		  $jawaban_opr=$r->jawaban_opr;
		  $arr=explode('#',$jawaban_opr);
		  $hasil .=($r->nama_jenis).' : ';
		  foreach($arr as $index=>$val){
			  $hasil .='<span class="badge badge-primary">'.$val.'</span> ';
		  }
		  $hasil .='<br>';
		  $hasil .='<br>';
	  }
	  return $hasil;
  }
}
