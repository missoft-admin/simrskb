<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Malasan_batal extends CI_Controller {

	/**
	 * Alasan Batal controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Malasan_batal_model');
  }

	function index(){
		
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Alasan Batal';
		$data['content'] 		= 'Malasan_batal/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Alasan Batal",'#'),
									    			array("List",'malasan_batal')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'keterangan' 			=> '',
			'status' 				=> '1'
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Alasan Batal';
		$data['content'] 		= 'Malasan_batal/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Alasan Batal",'#'),
									    			array("Tambah",'malasan_batal')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Malasan_batal_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'keterangan' 			=> $row->keterangan,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Alasan Batal';
				$data['content']	 	= 'Malasan_batal/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Alasan Batal",'#'),
											    			array("Ubah",'malasan_batal')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('malasan_batal','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('malasan_batal');
		}
	}

	function delete($id){
		
		$this->Malasan_batal_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('malasan_batal','location');
	}

	function save(){
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Malasan_batal_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('malasan_batal','location');
				}
			} else {
				if($this->Malasan_batal_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('malasan_batal','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Malasan_batal/manage';

		if($id==''){
			$data['title'] = 'Tambah Alasan Batal';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Alasan Batal",'#'),
															array("Tambah",'malasan_batal')
													);
		}else{
			$data['title'] = 'Ubah Alasan Batal';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Alasan Batal",'#'),
															array("Ubah",'malasan_batal')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
			$this->from   = 'malasan_batal';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'id' => 'ASC'
			);
			$this->group  = array();

      $this->column_search   = array('keterangan');
      $this->column_order    = array('keterangan');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->keterangan;
	        $aksi = '<div class="btn-group">';
	            if (UserAccesForm($user_acces_form,array('213'))){
	                $aksi .= '<a href="'.site_url().'malasan_batal/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
	            }
	            if (UserAccesForm($user_acces_form,array('214'))){
	                $aksi .= '<a href="#" data-urlindex="'.site_url().'mrak" data-urlremove="'.site_url().'malasan_batal/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
	            }
	            $aksi .= '</div>';
	        $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
}
