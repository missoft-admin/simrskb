<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpotongan_dokter extends CI_Controller {

	/**
	 * Potongan Dokter controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpotongan_dokter_model');
  }

	function index(){

		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Potongan Dokter';
		$data['content'] 		= 'Mpotongan_dokter/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Potongan Dokter",'#'),
									    			array("List",'mpotongan_dokter')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){

		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Potongan Dokter';
		$data['content'] 		= 'Mpotongan_dokter/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Potongan Dokter",'#'),
									    			array("Tambah",'mpotongan_dokter')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){

		if($id != ''){
			$row = $this->Mpotongan_dokter_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Potongan Dokter';
				$data['content']	 	= 'Mpotongan_dokter/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Potongan Dokter",'#'),
											    			array("Ubah",'mpotongan_dokter')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mpotongan_dokter','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpotongan_dokter');
		}
	}

	function delete($id){

		$this->Mpotongan_dokter_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mpotongan_dokter','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mpotongan_dokter_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mpotongan_dokter','location');
				}
			} else {
				if($this->Mpotongan_dokter_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mpotongan_dokter','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mpotongan_dokter/manage';

		if($id==''){
			$data['title'] = 'Tambah Potongan Dokter';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Potongan Dokter",'#'),
															array("Tambah",'mpotongan_dokter')
													);
		}else{
			$data['title'] = 'Ubah Potongan Dokter';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Potongan Dokter",'#'),
															array("Ubah",'mpotongan_dokter')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
			$this->select = array();
			$this->from   = 'mpotongan_dokter';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'nama' => 'ASC'
			);
			$this->group  = array();

      $this->column_search   = array('nama');
      $this->column_order    = array('nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama;
          $aksi = '<div class="btn-group">';
          // if(button_roles('mpotongan_dokter/update')) {
	          $aksi .= '<a href="'.site_url().'mpotongan_dokter/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
          // }
          // if(button_roles('mpotongan_dokter/delete')) {
          	$aksi .= '<a href="#" data-urlindex="'.site_url().'mpotongan_dokter" data-urlremove="'.site_url().'mpotongan_dokter/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
          // }
          $aksi .= '</div>';

          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
}
