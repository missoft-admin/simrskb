<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_checkin_petugas extends CI_Controller {

	

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Setting_checkin_petugas_model');
		$this->load->helper('path');
		
  }
	
	function pendaftaran($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1574'))){
			// $data = $this->Setting_checkin_petugas_model->get_pendaftaran_setting();
			// print_r($data);exit;
			$data['tab'] 			= $tab;
			$data['idtipe'] 			= '#';
			$data['idpoli'] 			= '#';
			$data['idtipe_2'] 			= '#';
			$data['idpoli_2'] 			= '0';
			$data['jenis_pertemuan_id'] 			= '#';
			$data['iddokter'] 			= '0';
			$data['list_dokter'] 			= $this->Setting_checkin_petugas_model->list_dokter();
			$data['st_gc'] 			= '1';
			$data['st_sp'] 			= '1';
			$data['st_sc'] 			= '1';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Checkin Via Petugas';
			$data['content'] 		= 'Setting_checkin_petugas/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Setting Checkin Via Petugas",'#'),
												  array("Pendaftaran Setting",'setting_checkin_petugas/pendaftaran')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function pendaftaran_rm($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1575'))){
			// $data = $this->Setting_checkin_petugas_model->get_pendaftaran_rm_setting();
			// print_r('SINI');exit;
			$data['tab'] 			= $tab;
			$data['idtipe'] 			= '#';
			$data['idpoli'] 			= '#';
			$data['idtipe_2'] 			= '#';
			$data['idpoli_2'] 			= '0';
			$data['jenis_pertemuan_id'] 			= '#';
			$data['iddokter'] 			= '0';
			$data['list_dokter'] 			= $this->Setting_checkin_petugas_model->list_dokter();
			$data['st_gc'] 			= '1';
			$data['st_sp'] 			= '1';
			$data['st_sc'] 			= '1';
			$data['error'] 			= '';
						$data['title'] 			= 'Setting Checkin RM Via Petugas';
			$data['content'] 		= 'Setting_checkin_petugas/index_rm';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Setting Checkin Via Petugas",'#'),
												  array("Pendaftaran Setting",'setting_checkin_petugas/pendaftaran_rm')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function save_pendaftaran(){
		if ($this->Setting_checkin_petugas_model->save_pendaftaran()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_checkin_petugas/pendaftaran/1','location');
		}
	}
	function save_pendaftaran_rm(){
		if ($this->Setting_checkin_petugas_model->save_pendaftaran_rm()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_checkin_petugas/pendaftaran_rm/1','location');
		}
	}
	function simpan_poli(){
		$this->idpoli=$this->input->post('idpoli');
		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');
		
		  $hasil=$this->db->insert('setting_checkin_petugas_poli',$this);
		  
		  json_encode($hasil);
	}
	function simpan_poli_rm(){
		$this->idpoli=$this->input->post('idpoli');
		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');
		
		  $hasil=$this->db->insert('setting_checkin_petugas_rm_poli',$this);
		  
		  json_encode($hasil);
	}
	
	
	function list_poli(){
		$idtipe=$this->input->post('idtipe');
		$q="SELECT H.id,H.nama FROM `mpoliklinik` H
			LEFT JOIN setting_checkin_petugas_poli T ON T.idpoli=H.id 
			WHERE H.idtipe='$idtipe' AND T.idpoli IS NULL AND H.`status`='1'";
			
		$hasil=$this->db->query($q)->result();
		$tabel='<option value="#" selected>- Pilih Poliklinik -</option>';
		foreach($hasil as $row){
			$tabel .='<option value="'.$row->id.'" >'.$row->nama.'</option>';
		}
		$this->output->set_output(json_encode($tabel));
	}
	function list_poli_rm(){
		$idtipe=$this->input->post('idtipe');
		$q="SELECT H.id,H.nama FROM `mpoliklinik` H
			LEFT JOIN setting_checkin_petugas_rm_poli T ON T.idpoli=H.id 
			WHERE H.idtipe='$idtipe' AND T.idpoli IS NULL AND H.`status`='1' AND H.id IN (SELECT idpoli FROM app_reservasi_poli )";
			
		$hasil=$this->db->query($q)->result();
		$tabel='<option value="#" selected>- Pilih Poliklinik -</option>';
		foreach($hasil as $row){
			$tabel .='<option value="'.$row->id.'" >'.$row->nama.'</option>';
		}
		$this->output->set_output(json_encode($tabel));
	}
	
	function load_poliklinik()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.idtipe,M.nama
						FROM setting_checkin_petugas_poli H
						LEFT JOIN mpoliklinik M ON M.id=H.idpoli
						ORDER BY M.idtipe,M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = GetAsalRujukan($r->idtipe);
          $result[] = $r->nama;
		  if (UserAccesForm($user_acces_form,array('1472'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_poli('.$r->id.')" type="button" title="Non Aktifkan" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_poliklinik_rm()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.idtipe,M.nama
						FROM setting_checkin_petugas_rm_poli H
						LEFT JOIN mpoliklinik M ON M.id=H.idpoli
						ORDER BY M.idtipe,M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = GetAsalRujukan($r->idtipe);
          $result[] = $r->nama;
		  if (UserAccesForm($user_acces_form,array('1490'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_poli('.$r->id.')" type="button" title="Non Aktifkan" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
 
  function hapus_poli(){
	  $id=$this->input->post('id');
	  $this->db->where('idpoli',$id);
	  $hasil=$this->db->delete('setting_checkin_petugas_poli',$this);
	  
	  json_encode($hasil);
	  
  }
  function hapus_poli_rm(){
	  $id=$this->input->post('id');
	  $this->db->where('idpoli',$id);
	  $hasil=$this->db->delete('setting_checkin_petugas_rm_poli',$this);
	  
	  json_encode($hasil);
	  
  }
function list_kp(){
	$q="SELECT H.id,H.nama FROM `mpasien_kelompok` H
		LEFT JOIN setting_checkin_petugas_bayar T ON T.idkelompokpasien=H.id 
		WHERE T.idkelompokpasien IS NULL AND H.`status`='1'";
		
	$hasil=$this->db->query($q)->result();
	$tabel='<option value="#" selected>- Pilih Kelompok Pasien -</option>';
	foreach($hasil as $row){
		$tabel .='<option value="'.$row->id.'" >'.$row->nama.'</option>';
	}
	$this->output->set_output(json_encode($tabel));
}
	function list_kp_rm(){
	$q="SELECT H.id,H.nama FROM `mpasien_kelompok` H
		LEFT JOIN setting_checkin_petugas_rm_bayar T ON T.idkelompokpasien=H.id 
		WHERE T.idkelompokpasien IS NULL AND H.`status`='1'";
		
	$hasil=$this->db->query($q)->result();
	$tabel='<option value="#" selected>- Pilih Kelompok Pasien -</option>';
	foreach($hasil as $row){
		$tabel .='<option value="'.$row->id.'" >'.$row->nama.'</option>';
	}
	$this->output->set_output(json_encode($tabel));
	}
	function load_kelompok_pasien()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama,GROUP_CONCAT(MR.nama) as rekanan
						FROM setting_checkin_petugas_bayar H
						LEFT JOIN mpasien_kelompok M ON M.id=H.idkelompokpasien
						LEFT JOIN setting_checkin_petugas_asuransi A ON H.idkelompokpasien=A.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=A.idrekanan
						GROUP BY H.idkelompokpasien
						ORDER BY M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama','rekanan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
         
          $result[] = '<strong>'.$r->nama.'</strong>'.($r->rekanan?'<br>( '.$r->rekanan.' )':'');
          $aksi = '<div class="btn-group">';
		  if ($r->id=='1'){
			  if (UserAccesForm($user_acces_form,array('1475'))){
				$aksi .= '<button onclick="load_rekanan('.$r->id.')" type="button" title="List Asuransi" class="btn btn-primary btn-xs "><i class="si si-settings"></i></button>';	
			  
			}
		  }
		  if (UserAccesForm($user_acces_form,array('1474'))){
		  $aksi .= '<button onclick="hapus_kelompok('.$r->id.')" type="button" title="Non Aktifkan" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		 $aksi .= '</div>';
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
    }
	function load_kelompok_pasien_rm()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama,GROUP_CONCAT(MR.nama) as rekanan
						FROM setting_checkin_petugas_rm_bayar H
						LEFT JOIN mpasien_kelompok M ON M.id=H.idkelompokpasien
						LEFT JOIN setting_checkin_petugas_rm_asuransi A ON H.idkelompokpasien=A.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=A.idrekanan
						GROUP BY H.idkelompokpasien
						ORDER BY M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama','rekanan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
         
          $result[] = '<strong>'.$r->nama.'</strong>'.($r->rekanan?'<br>( '.$r->rekanan.' )':'');
          $aksi = '<div class="btn-group">';
		  if ($r->id=='1'){
			  if (UserAccesForm($user_acces_form,array('1493'))){
				$aksi .= '<button onclick="load_rekanan('.$r->id.')" type="button" title="List Asuransi" class="btn btn-primary btn-xs "><i class="si si-settings"></i></button>';	
			  
			}
		  }
		  if (UserAccesForm($user_acces_form,array('1492'))){
		  $aksi .= '<button onclick="hapus_kelompok('.$r->id.')" type="button" title="Non Aktifkan" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		 $aksi .= '</div>';
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
    }
	function load_rekanan()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama,CASE WHEN H.idrekanan IS NOT NULL THEN '1' ELSE '0' END as pilih FROM `mrekanan` M
						LEFT JOIN setting_checkin_petugas_asuransi H ON M.id=H.idrekanan
						ORDER BY M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
         
          $result[] = $r->nama;
		  $aksi='';
		  if ($r->pilih){
			$aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save('.$r->id.','.$r->pilih.')" type="checkbox" checked><span></span>
					</label>';	
		  }else{
			  $aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save('.$r->id.','.$r->pilih.')" type="checkbox"><span></span>
					</label>';	
		  }
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
    }
	function load_rekanan_rm()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama,CASE WHEN H.idrekanan IS NOT NULL THEN '1' ELSE '0' END as pilih FROM `mrekanan` M
						LEFT JOIN setting_checkin_petugas_rm_asuransi H ON M.id=H.idrekanan
						ORDER BY M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
         
          $result[] = $r->nama;
		  $aksi='';
		  if ($r->pilih){
			$aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save('.$r->id.','.$r->pilih.')" type="checkbox" checked><span></span>
					</label>';	
		  }else{
			  $aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save('.$r->id.','.$r->pilih.')" type="checkbox"><span></span>
					</label>';	
		  }
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
    }
	function simpan_kp(){
		$this->idkelompokpasien=$this->input->post('idkelompokpasien');
		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');
		
		  $hasil=$this->db->insert('setting_checkin_petugas_bayar',$this);
		  
		  json_encode($hasil);
	}
	function simpan_kp_rm(){
		$this->idkelompokpasien=$this->input->post('idkelompokpasien');
		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');
		
		  $hasil=$this->db->insert('setting_checkin_petugas_rm_bayar',$this);
		  
		  json_encode($hasil);
	}
	
	function check_save(){
		$pilih=$this->input->post('pilih');
		$idrekanan=$this->input->post('id');
		if ($pilih=='1'){//Asalnya Terpilih
			$this->db->where('idrekanan',$idrekanan);
			$hasil=$this->db->delete('setting_checkin_petugas_asuransi');
		}else{
			$this->idkelompokpasien=1;
			$this->idrekanan=$this->input->post('id');
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
			
			  $hasil=$this->db->insert('setting_checkin_petugas_asuransi',$this);
		}
		
		  
		  json_encode($hasil);
	}
	function check_save_rm(){
		$pilih=$this->input->post('pilih');
		$idrekanan=$this->input->post('id');
		if ($pilih=='1'){//Asalnya Terpilih
			$this->db->where('idrekanan',$idrekanan);
			$hasil=$this->db->delete('setting_checkin_petugas_rm_asuransi');
		}else{
			$this->idkelompokpasien=1;
			$this->idrekanan=$this->input->post('id');
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
			
			  $hasil=$this->db->insert('setting_checkin_petugas_rm_asuransi',$this);
		}
		
		  
		  json_encode($hasil);
	}
	
	function hapus_kelompok(){
	  $id=$this->input->post('id');
	  $this->db->where('idkelompokpasien',$id);
	  $hasil=$this->db->delete('setting_checkin_petugas_bayar',$this);
	  
	  json_encode($hasil);
	  
  }
  function hapus_kelompok_rm(){
	  $id=$this->input->post('id');
	  $this->db->where('idkelompokpasien',$id);
	  $hasil=$this->db->delete('setting_checkin_petugas_rm_bayar',$this);
	  
	  json_encode($hasil);
	  
  }
  
  function simpan_logic(){
		
		$this->jenis_pertemuan_id=$this->input->post('jenis_pertemuan_id');
		$this->idtipe=$this->input->post('idtipe');
		$this->idpoliklinik=$this->input->post('idpoli');
		$this->iddokter=$this->input->post('iddokter');
		$this->st_gc=$this->input->post('st_gc');
		$this->st_sp=$this->input->post('st_sp');
		$this->st_sc=$this->input->post('st_sc');
		
		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');
		
		  $hasil=$this->db->insert('setting_checkin_petugas_logic',$this);
		  
		  json_encode($hasil);
	}
	function simpan_logic_rm(){
		
		$this->jenis_pertemuan_id=$this->input->post('jenis_pertemuan_id');
		$this->idtipe=$this->input->post('idtipe');
		$this->idpoliklinik=$this->input->post('idpoli');
		$this->iddokter=$this->input->post('iddokter');
		$this->st_gc=$this->input->post('st_gc');
		$this->st_sp=$this->input->post('st_sp');
		$this->st_sc=$this->input->post('st_sc');
		
		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');
		
		  $hasil=$this->db->insert('setting_checkin_petugas_rm_logic',$this);
		  
		  json_encode($hasil);
	}
	function list_poli_2(){
		$idtipe=$this->input->post('idtipe');
		$jenis_pertemuan_id=$this->input->post('jenis_pertemuan_id');
		$q="SELECT H.id,H.nama FROM `mpoliklinik` H
			WHERE H.idtipe='$idtipe' AND H.`status`='1'";
			
		$hasil=$this->db->query($q)->result();
		$tabel='<option value="0" selected>- All Poliklinik -</option>';
		if ($jenis_pertemuan_id!='#'){
			foreach($hasil as $row){
				$tabel .='<option value="'.$row->id.'" >'.$row->nama.'</option>';
			}
		}
		
		$this->output->set_output(json_encode($tabel));
	}
	function list_poli_2_rm(){
		$idtipe=$this->input->post('idtipe');
		$jenis_pertemuan_id=$this->input->post('jenis_pertemuan_id');
		$q="SELECT H.id,H.nama FROM `mpoliklinik` H
			WHERE H.idtipe='$idtipe' AND H.`status`='1'";
			
		$hasil=$this->db->query($q)->result();
		$tabel='<option value="0" selected>- All Poliklinik -</option>';
		if ($jenis_pertemuan_id!='#'){
			foreach($hasil as $row){
				$tabel .='<option value="'.$row->id.'" >'.$row->nama.'</option>';
			}
		}
		
		$this->output->set_output(json_encode($tabel));
	}
	function load_logic()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT 
							H.*,MP.nama as poli,MD.nama as dokter 
							FROM setting_checkin_petugas_logic H
							LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
							LEFT JOIN mdokter MD ON MD.id=H.iddokter
							ORDER BY H.jenis_pertemuan_id,H.idtipe,H.idpoliklinik
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('dokter','poli');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = GetJenisKunjungan($r->jenis_pertemuan_id);
          $result[] = GetAsalRujukan($r->idtipe);
          $result[] = ($r->idpoliklinik=='0'?text_default('ALL POLIKLINIK'):$r->poli);
          $result[] = ($r->iddokter=='0'?text_default('ALL DOKTER'):$r->dokter);
          $result[] = ($r->st_gc?'YA':text_danger('TIDAK'));
          $result[] = ($r->st_sp?'YA':text_danger('TIDAK'));
          $result[] = ($r->st_sc?'YA':text_danger('TIDAK'));
		  if (UserAccesForm($user_acces_form,array('1478'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_logic('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_logic_rm()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT 
							H.*,MP.nama as poli,MD.nama as dokter 
							FROM setting_checkin_petugas_rm_logic H
							LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
							LEFT JOIN mdokter MD ON MD.id=H.iddokter
							ORDER BY H.jenis_pertemuan_id,H.idtipe,H.idpoliklinik
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('dokter','poli');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = GetJenisKunjungan($r->jenis_pertemuan_id);
          $result[] = GetAsalRujukan($r->idtipe);
          $result[] = ($r->idpoliklinik=='0'?text_default('ALL POLIKLINIK'):$r->poli);
          $result[] = ($r->iddokter=='0'?text_default('ALL DOKTER'):$r->dokter);
          $result[] = ($r->st_gc?'YA':text_danger('TIDAK'));
          $result[] = ($r->st_sp?'YA':text_danger('TIDAK'));
          $result[] = ($r->st_sc?'YA':text_danger('TIDAK'));
		  if (UserAccesForm($user_acces_form,array('1488'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_logic('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_logic(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_checkin_petugas_logic',$this);
	  
	  json_encode($hasil);
	  
  }
  function hapus_logic_rm(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_checkin_petugas_rm_logic',$this);
	  
	  json_encode($hasil);
	  
  }
 
}
