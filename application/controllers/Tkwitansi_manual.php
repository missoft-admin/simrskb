<?php
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Tkwitansi_manual extends CI_Controller {

  /**
    * Kwitansi Manual Controller.
    * Developer @GunaliRezqiMauludi
    */

	function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tkwitansi_manual_model');
		$this->load->model('Trawatinap_tindakan_model');
	}

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Kwitansi Manual';
		$data['content'] 		= 'Tkwitansi_manual/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Verifikator",'#'),
									    			array("Kwitansi Manual",'Tkwitansi_manual')
													);

		$data['list_index'] = $this->Tkwitansi_manual_model->getAll();

		$data = array_merge($data,backend_info());
		$this->parser->parse('module_template',$data);
	}

	function add() {
		$data = array(
			'id' 						=> '',
			'tanggal' 			=> date('d/m/Y'),
			'waktu' 				=> date('H:i:s'),
			'pasien_id' 		=> '',
			'idmetode' 		=> '',
			'idbank' 		=> '',
			'ket_cc' 		=> '',
			'trace_number' 		=> '',
			'nominal' 			=> '',
			'jaminan' 			=> '',
			'keterangan' 		=> '',
			'tipe' 					=> '',
			'status' 				=> '',
		);
		$data['error'] 			= '';
		$data['tanggal'] 		 = date("Y-m-d");
		$data['title'] 			= 'Add Kwitansi Manual';
		$data['content'] 		= 'Tkwitansi_manual/manage';
		$data['breadcrum']	 = array(
														array("RSKB Halmahera",'#'),
														array("Verifikator",'#'),
									    			array("Add Kwitansi Manual",'Tkwitansi_manual')
													);

		$data['list_pasien'] = $this->Tkwitansi_manual_model->getAllPasien();
		$data['list_bank']  = $this->Trawatinap_tindakan_model->getBank();

		$data = array_merge($data,backend_info());
		$this->parser->parse('module_template',$data);
	}

	function edit($id){
		if($id != ''){
			$row = $this->Tkwitansi_manual_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'tanggal' 			=> DMYFormat($row->tanggal),
					'waktu' 				=> HISTimeFormat($row->tanggal),
					'pasien_id' 		=> $row->pasien_id,
					'idmetode' 		=> $row->idmetode,
					'idbank' 		=> $row->idbank,
					'ket_cc' 		=> $row->ket_cc,
					'trace_number' 		=> $row->trace_number,
					'nominal' 			=> $row->nominal,
					'jaminan' 			=> $row->jaminan,
					'keterangan' 		=> $row->keterangan,
					'tipe' 					=> $row->tipe,
					'status' 				=> $row->status,
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Edit Kwitansi Manual';
				$data['content']	 	= 'Tkwitansi_manual/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Kwitansi Manual",'#'),
											    			array("Edit",'Tkwitansi_manual')
															);

				$data['list_pasien'] = $this->Tkwitansi_manual_model->getAllPasien();
				$data['list_bank']  = $this->Trawatinap_tindakan_model->getBank();

				$data = array_merge($data,backend_info());
				$this->parser->parse('module_template',$data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('tkwitansi_manual','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('tkwitansi_manual');
		}
	}

	function save(){
		$this->form_validation->set_rules('pasien_id', 'Pasien', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Tkwitansi_manual_model->save()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
				}
			} else {
				if($this->Tkwitansi_manual_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('tkwitansi_manual/redirect/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 			 = validation_errors();
		$data['content'] 		 = 'Tkwitansi_manual/manage';
		$data['list_pasien'] = $this->Tkwitansi_manual_model->getAllPasien();

		if($id==''){
			$data['title'] = 'Add Kwitansi Manual';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Kwitansi Manual",'#'),
															array("Add",'Tkwitansi_manual')
														);
		}else{
			$data['title'] = 'Edit Kwitansi Manual';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Kwitansi Manual",'#'),
															array("Edit",'Tkwitansi_manual')
														);
		}

		$data = array_merge($data,backend_info());
		$this->parser->parse('module_template',$data);
	}

	function remove($id){
		$this->Tkwitansi_manual_model->remove($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('tkwitansi_manual','location');
	}

	function redirect($idkwitansi){
		$data['idkwitansi'] = $idkwitansi;
		$data = array_merge($data, backend_info());
		$this->parser->parse('Tkwitansi_manual/redirect',$data);
	}

	public function print_kwitansi($idkwitansi){
		// instantiate and use the dompdf class
		$dompdf = new Dompdf();

		$row = $this->Tkwitansi_manual_model->getSpecified($idkwitansi);

		$data = array(
			'id' 						=> $row->id,
			'tanggal' 			=> $row->tanggal,
			'pasien_id' 		=> $row->pasien_id,
			'nama_pasien' 	=> $row->nama_pasien,
			'nominal' 			=> $row->nominal,
			'jaminan' 			=> $row->jaminan,
			'keterangan' 		=> $row->keterangan,
			'tipe' 					=> $row->tipe,
			'status' 				=> $row->status,
		);

		$html = $this->load->view('Tkwitansi_manual/report', $data, true);

		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();

		// // Output the generated PDF to Browser
		$dompdf->stream('Kwitansi.pdf', array("Attachment"=>0));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
