<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrka_approval extends CI_Controller {

	/**
	 * Create RKA controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mrka_approval_model');
		$this->load->model('Mrka_kegiatan_model');
		$this->load->model('Mrka_pengajuan_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array(
			'tanggal_pengajuan2'=>'',
			'tanggal_pengajuan1'=>'',
			'tanggal_dibutuhkan1'=>'',
			'tanggal_dibutuhkan2'=>'',
			'idjenis'=>'#',
			'approve'=>'0',
		);
		$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
		$data['list_jenis'] 		= $this->Mrka_approval_model->list_jenis_all();
		$data['error'] 			= '';
		$data['title'] 			= 'Approval';
		$data['content'] 		= 'Mrka_approval/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("List RKA",'#'),
									    			array("List",'mrka_pengajuan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	
	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  $iduser=$this->session->userdata('user_id');		
	  $q="SELECT GROUP_CONCAT(S.idunit) as unit_sendiri from munitpelayanan_user_setting S
			WHERE S.userid='$iduser'";
	  $unit_sendiri=$this->db->query($q)->row('unit_sendiri');
	  $no_pengajuan=$this->input->post('no_pengajuan');
	  $nama_kegiatan=$this->input->post('nama_kegiatan');
	  $tipe_rka=$this->input->post('tipe_rka');
	  $idunit_pengaju=$this->input->post('idunit_pengaju');
	  $idjenis=$this->input->post('idjenis');
	  $tanggal_pengajuan1= ($this->input->post('tanggal_pengajuan1'));
	  $tanggal_pengajuan2= ($this->input->post('tanggal_pengajuan2'));
	  $tanggal_dibutuhkan1= ($this->input->post('tanggal_dibutuhkan1'));
	  $tanggal_dibutuhkan2= ($this->input->post('tanggal_dibutuhkan2'));
	  $approve= ($this->input->post('approve'));
	  
	  $where='';
	  
	  if ($no_pengajuan !=''){
		  $where .=" AND H.no_pengajuan='$no_pengajuan'";
	  }
	  if ($nama_kegiatan !=''){
		  $where .=" AND H.nama_kegiatan='$nama_kegiatan'";
	  }
	  if ($tipe_rka !='#'){
		  $where .=" AND H.tipe_rka='$tipe_rka'";
	  }
	  if ($idunit_pengaju !='#'){
		  $where .=" AND H.idunit_pengaju='$idunit_pengaju'";
	  }
	  if ($idjenis !='#'){
		  $where .=" AND H.idjenis='$idjenis'";
	  }
	  if ($approve !='#'){
		  if ($approve=='0'){
			  $where .="  AND AP.approve='$approve'";
			  $where .=" OR (H.status='2' AND H.st_bendahara='1')";
		  }else{
			$where .="  AND AP.approve='$approve'";
			  
		  }
	  }
	  if ($tanggal_pengajuan1 !='' || $tanggal_pengajuan2 !=''){
		  $where .=" AND (H.tanggal_pengajuan >='".YMDFormat($tanggal_pengajuan1)."' AND H.tanggal_pengajuan <='".YMDFormat($tanggal_pengajuan2)."' )";
	  }
	  if ($tanggal_dibutuhkan1 !='' || $tanggal_dibutuhkan2 !=''){
		  $where .=" AND (H.tanggal_dibutuhkan >='".YMDFormat($tanggal_dibutuhkan1)."' AND H.tanggal_dibutuhkan <='".YMDFormat($tanggal_dibutuhkan2)."' )";
	  }
		
	  $from="(
				SELECT H.idpengajuan_asal,H.id,H.no_pengajuan,H.created_date,H.tanggal_pengajuan,H.tipe_rka,H.nama_kegiatan,H.catatan 
				,U.nama as unit_pengaju,H.idunit_pengaju,H.grand_total,H.`status`,H.idjenis
				,H.tanggal_dibutuhkan,H.idunit,cek_logic(H.id) as nama_logic
				,J.nama as jenis,GROUP_CONCAT(AP.user_nama) as nama_user,GROUP_CONCAT(AP.approve) as st_approve,MAX(AP.step) as step,H.st_bendahara
				from rka_pengajuan H
				LEFT JOIN rka_pengajuan_approval AP ON AP.idrka=H.id AND AP.st_aktif='1'
				LEFT JOIN munitpelayanan U ON U.id=H.idunit_pengaju
				LEFT JOIN mjenis_pengajuan_rka J ON J.id=H.idjenis
				WHERE AP.iduser IN ('".$iduser."') ".$where."
				GROUP BY H.id
				ORDER BY H.id DESC
				) as tbl";
	// print_r($tanggal_dibutuhkan1);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama_kegiatan','no_pengajuan','catatan');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	 
      foreach ($list as $r) {
		  
          $no++;
          $row = array();
		  $button_asal='';
		  if ($r->idpengajuan_asal){
			  $button_asal='<br><a href="'.site_url().'mrka_pengajuan/update/'.$r->idpengajuan_asal.'/disabled" target="_blank" title="Lihat Pengajau Asal"><i class="fa fa-folder-open-o"></i></a>';
		  }
		  
		  $nama_logic=$r->nama_logic;//$r->unit_pengaju;;
		  $respon='';
		  $status_pengajuan='';
		  $aksi = '<div class="btn-group">';
		  $query=$this->get_respon($r->id,$r->step);
		  
		$aksi .= '<a href="'.site_url().'mrka_pengajuan/update/'.$r->id.'/disabled" data-toggle="tooltip" title="Detail" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>';
		$aksi .= '<button title="Upload Dokument" class="btn btn-warning btn-sm" onclick="upload_image('.$r->id.')"><i class="fa fa-file-image-o"></i></button>';
		// $hasil=$this->cek_user($unit_sendiri,$r->idunit_pengaju);
		// if ($hasil=='1'){
		if ($r->status=='2'){
		$aksi .= '<a href="'.site_url().'mrka_pengajuan/update/'.$r->id.'/0/mrka_approval" data-toggle="tooltip" title="Detail" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>';
		}
		// }
		if ($query){
			foreach ($query as $res){
				if ($res->iduser==$iduser && $res->step==$r->step){
					if ($res->approve=='0'){
					  $aksi .= '<button title="Setuju" class="btn btn-primary btn-sm setuju" data-id="'.$res->id.'"><i class="fa fa-check"></i></button>';
					  $aksi .= '<button title="Tolak" class="btn btn-danger btn-sm tolak" data-id="'.$res->id.'"><i class="si si-ban"></i></button>';
					}else{
					  if ($r->status=='2'){						  
						$aksi .= '<button title="Batalkan Pesetujuan / Tolak" class="btn btn-primary btn-sm batal" data-id="'.$res->id.'"><i class="fa fa-refresh"></i></button>';
					  }
					}
				}
				if ($res->iduser==$iduser){
					$respon .='<span class="label label-warning" data-toggle="tooltip">STEP '.$res->step.'</span> : '.status_approval($res->approve).'<br>';
				}
			}
			  
			  
		  }else{
			   $respon='';
		  }
		  if ($r->status=='2' && $r->st_bendahara=='1'){
			  $status_pengajuan='<span class="label label-success" data-toggle="tooltip">PERSETUJUAN SELESAI</span>';
			  $aksi .= '<a href="'.site_url().'mrka_approval/next_bendahara/'.$r->id.'" data-toggle="tooltip" title="Proses Ke Bendahara" class="btn btn-success btn-sm"><i class="si si-arrow-right"></i> Next</a>';
		  }
		  if ($r->status=='2' && $r->st_bendahara=='2'){
			  $status_pengajuan='<span class="label label-danger" data-toggle="tooltip">PERSETUJUAN DITOLAK</span>';
		  }
		$aksi .= '<a href="#" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
			
			$aksi .= '</div>';
		  if ($r->status!='1'){
			  if ($r->status=='2'){
				$nama_logic='<button title="Lihat User" class="btn btn-danger btn-xs user"><i class="si si-user"></i></button>';
			  }else{
				$nama_logic='';
			  }			 
		  }
		  $catatan='';
		  if ($r->catatan){
			  $catatan='<br>'.'<p class="text-primary"><i class="fa fa-sticky-note-o"></i> '.$r->catatan.'</p>';
		  }
          $row[] = $r->id;
          $row[] = $r->id;
          $row[] = $r->id;
          $row[] = $no;//3
          $row[] = $r->no_pengajuan.'<br>'.HumanDateLong($r->created_date);
          $row[] = HumanDateShort($r->tanggal_pengajuan).$button_asal;
          $row[] = tipe_rka($r->tipe_rka);
          $row[] = $r->nama_kegiatan.$catatan;
          $row[] = $r->unit_pengaju;
          $row[] = HumanDateShort($r->tanggal_dibutuhkan);
          $row[] = number_format($r->grand_total,0);
          $row[] = $r->jenis;
		  if ($r->status=='2' && $r->st_bendahara!='0'){
			$row[] = $status_pengajuan.' '.$nama_logic;;
		  }else{
			$row[] = status_pengajuan($r->status,$r->step).' '.$nama_logic;//.($status=='1'?$nama_logic:'');
			   
		  }
          $row[] = $respon;
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function cek_user($str,$idunit){
		// $str="2,31,11";
		if ($str != ''){
			$arr=explode(',',$str);
			if (in_array($idunit, $arr)){
				return "1";
			}else{
				return "0";
			}
		}else{
			return "0";
		}
	}

  function get_respon($idrka,$step){
	  $iduser=$this->session->userdata('user_id');
	  $q="SELECT *from rka_pengajuan_approval AP
			WHERE AP.st_aktif='1' AND AP.iduser='$iduser' AND AP.idrka='$idrka'";
	  return $this->db->query($q)->result();
  }
  function load_user_approval()
    {
	  $idrka=$this->input->post('idrka');	 
	  $where='';
	  $from="(
				SELECT rka_pengajuan.grand_total,U.`name` as user_nama, mlogic_detail.*  FROM rka_pengajuan 
				LEFT JOIN mlogic_unit ON  rka_pengajuan.idunit_pengaju = mlogic_unit.idunit
				LEFT JOIN mlogic_detail ON  mlogic_unit.idlogic = mlogic_detail.idlogic AND rka_pengajuan.tipe_rka = mlogic_detail.tipe_rka AND rka_pengajuan.idjenis = mlogic_detail.idjenis AND mlogic_detail.`status` = 1
				LEFT JOIN  musers U ON U.id=mlogic_detail.iduser
				WHERE rka_pengajuan.id ='$idrka' AND  calculate_logic(mlogic_detail.operand,rka_pengajuan.grand_total,mlogic_detail.nominal) = 1
				ORDER BY mlogic_detail.step,mlogic_detail.id
				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('user_nama');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
          $row[] = $r->step;
          $row[] = $r->user_nama;
          $row[] = ($r->proses_setuju=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
          $row[] = ($r->proses_tolak=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
         
          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
    function setuju_batal($id,$status,$idrka=''){
		// $arr=array();
		$q="call update_rka('$id', $status) ";
		$result=$this->db->query($q);
		// $arr['st_insert']=$result;
		$arr=$this->db->query("SELECT H.st_bendahara FROM rka_pengajuan H  WHERE H.id='$idrka' AND H.status='2'")->row('st_bendahara');
		$this->output->set_output(json_encode($arr));
	}
	function save(){
		$id=$this->Mrka_approval_model->saveData();
		if($id==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mrka_approval','location');
		}
	}
	function tolak(){
		$id=$this->input->post('id_approval');
		$alasan_tolak=$this->input->post('alasan_tolak');
		$q="call update_rka('$id', 2) ";
		$this->db->query($q);
		$result=$this->db->update('rka_pengajuan_approval',array('alasan_tolak'=>$alasan_tolak),array('id'=>$id));
		echo json_encode($result);
	}
	function next_bendahara($id,$disabel=''){
		
			$data = $this->Mrka_pengajuan_model->getSpecified($id);
			// print_r($data);exit();
			$data['tanggal_dibutuhkan']=HumanDateShort($data['tanggal_dibutuhkan']);
			$data['tanggal_pengajuan']=HumanDateShort($data['tanggal_pengajuan']);
			$data['st_user_bendahara'] 		=1;
			$data['st_user_unit'] 		=1;
			$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
			$data['list_unit_lain'] 		= $this->Mrka_approval_model->list_unit_lain();
			$data['list_jenis'] 		= $this->Mrka_pengajuan_model->list_jenis($data['tipe_rka']);
			// $data['list_detail'] 		= $this->Mrka_pengajuan_model->list_detail($id);
			$data['list_detail'] 		=array();
			$data['list_vendor'] 	= $this->Mrka_pengajuan_model->list_vendor();
			$data['list_dp_cicilan'] 	= $this->Mrka_pengajuan_model->list_cicilan($id,1);
			$data['list_cicilan'] 	= $this->Mrka_pengajuan_model->list_cicilan($id,2);
			// print_r($data['list_cicilan'] );exit();
			$data['list_dp_termin'] 	=  $this->Mrka_pengajuan_model->list_dp_termin($id);
			$data['list_rka'] 	= $this->Mrka_pengajuan_model->list_rka();
			$data['list_prespektif'] 	= $this->Mrka_pengajuan_model->list_prespektif();
			$data['disabel'] 			= $disabel;
			$data['error'] 			= '';
			$data['title'] 			= 'Review Pengajuan Kegiatan';
			$data['content'] 		= 'Mrka_approval/manage';
			$data['breadcrum']	= array(
															array("RSKB Halmahera",'#'),
															array("Create RKA",'#'),
														array("Tambah",'mrka_pengajuan')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		
	}

}
