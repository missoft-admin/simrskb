<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use Hashids\Hashids;
class Ttindakan_ranap_pasien extends CI_Controller {

	/**
	 * Checkin controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbooking_rehab_model');
		$this->load->model('Tbooking_model');
		$this->load->model('Ttindakan_ranap_pasien_model');
		$this->load->model('Tpoliklinik_pendaftaran_model');
		$this->load->model('Tcheckin_model');
		$this->load->helper('path');
		// print_r('sini');exit;
		
  }
	function index(){
		$log['path_tindakan']='ttindakan_ranap_pasien';
		$this->session->set_userdata($log);
		 get_ppa_login();
		 // print_r($this->session->userdata());exit;
		 $login_tipepegawai=$this->session->userdata('login_tipepegawai');
		 if ($login_tipepegawai=='2'){
			 $login_pegawai_id=$this->session->userdata('login_pegawai_id');
		 }else{
			 $login_pegawai_id='#';
		 }
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1938'))){
			$data = array();
			$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("-60 days"));
			$date2= date_format($date2,"d/m/Y");
			
			$data['list_reservasi_cara'] 			= list_reservasi_cara();
			// print_r($data['list_reservasi_cara']);exit;
			
			$data['list_poli'] 			= $this->Ttindakan_ranap_pasien_model->list_poli();
			$data['list_dokter'] 			= $this->Ttindakan_ranap_pasien_model->list_dokter();
			$data['tanggal_1'] 			= $date2;
			$data['tanggal_2'] 			= date('d/m/Y');
			$data['tab_utama'] 			= 3;
			$data['iddokter'] 			= $login_pegawai_id;
			$data['idalasan'] 			= '#';
			$data['namapasien'] 			= '';
			$data['no_medrec'] 			= '';
			// print_r($data);exit;
			$data['error'] 			= '';
			$data['title'] 			= 'Pendaftaran Rawat Inap';
			$data['content'] 		= 'Ttindakan_ranap_pasien/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Rawat Inap & ODS",'#'),
												  array("My Pasien",'ttindakan_ranap_pasien')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			// $iddokter =($this->input->post('iddokter')!='#'?$this->input->post('iddokter'):0);
			// $where .=" AND (H.iddokterpenanggungjawab='$iddokter' OR HP.iddokter IN (".$iddokter.")) ";
			// // print_r($this->input->post());exit;
			// $tanggal_1 =$this->input->post('tanggal_1');
			// $tanggal_2 =$this->input->post('tanggal_2');
			// $idtipe =$this->input->post('idtipe');
			// $idtipe_asal =$this->input->post('idtipe_asal');
			// $cari_pasien =$this->input->post('cari_pasien');
			// $nopendaftaran=$this->input->post('nopendaftaran');
			// $idruangan=$this->input->post('idruangan');
			// $idkelas=$this->input->post('idkelas');
			// $idbed=$this->input->post('idbed');
			// $norujukan=$this->input->post('norujukan');
			// $iddokter_perujuk=$this->input->post('iddokter_perujuk');
			// $idkelompokpasien=$this->input->post('idkelompokpasien');
			// $idrekanan=$this->input->post('idrekanan');
			// $status_tindakan_index=$this->input->post('status_tindakan_index');
			// $idpoliklinik_array=$this->input->post('idpoliklinik_array');
			// if ($idpoliklinik_array){
				// $idpoliklinik_array=implode(", ", $idpoliklinik_array);
				// $where .=" AND (H.idpoliklinik_asal IN (".$idpoliklinik_array.")) ";
			// }
			// $iddokter_perujuk_array=$this->input->post('iddokter_perujuk_array');
			// if ($iddokter_perujuk_array){
				// $iddokter_perujuk_array=implode(", ", $iddokter_perujuk_array);
				// $where .=" AND (H.iddokter_perujuk IN (".$iddokter_perujuk_array.")) ";
			// }
			// $iddokter_dpjp=$this->input->post('iddokter_dpjp');
			// if ($iddokter_dpjp){
				// $iddokter_dpjp=implode(", ", $iddokter_dpjp);
				// $where .=" AND (H.iddokterpenanggungjawab IN (".$iddokter_dpjp.")) ";
			// }
			// $dpjp_pendukung_array=$this->input->post('dpjp_pendukung_array');
			// if ($dpjp_pendukung_array){
				// $dpjp_pendukung_array=implode(", ", $dpjp_pendukung_array);
				// $where .=" AND (HP.iddokter IN (".$dpjp_pendukung_array.")) ";
			// }
			// $idkelompokpasien_array=$this->input->post('idkelompokpasien_array');
			// if ($idkelompokpasien_array){
				// $idkelompokpasien_array=implode(", ", $idkelompokpasien_array);
				// $where .=" AND (H.idkelompokpasien IN (".$idkelompokpasien_array.")) ";
			// }
			// $idrekanan_array=$this->input->post('idrekanan_array');
			// if ($idrekanan_array){
				// $idrekanan_array=implode(", ", $idrekanan_array);
				// $where .=" AND (H.idrekanan IN (".$idrekanan_array.")) ";
			// }
			// $idruangan_array=$this->input->post('idruangan_array');
			// if ($idruangan_array){
				// $idruangan_array=implode(", ", $idruangan_array);
				// $where .=" AND (H.idruangan IN (".$idruangan_array.")) ";
			// }
			// $idkelas_array=$this->input->post('idkelas_array');
			// if ($idkelas_array){
				// $idkelas_array=implode(", ", $idkelas_array);
				// $where .=" AND (H.idkelas IN (".$idkelas_array.")) ";
			// }
			// $idbed_array=$this->input->post('idbed_array');
			// if ($idbed_array){
				// $idbed_array=implode(", ", $idbed_array);
				// $where .=" AND (H.idkelas IN (".$idbed_array.")) ";
			// }
			// $status_tindakan_array=$this->input->post('status_tindakan_array');
			// if ($status_tindakan_array){
				// $where .=" AND (";
				// $where_tindakan='';
				
				// if (in_array('2', $status_tindakan_array)){
					// // if ($statuskasir=='1' && $statuspembayaran=='0'){
					// $where_tindakan .=($where_tindakan==''?'':' OR ')."(H.statuskasir='1' AND H.statuspembayaran='0') ";
				// }
				// if (in_array('3', $status_tindakan_array)){
					// $where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.st_terima_ranap='1' AND H.status='1') ";
				// }
				// if (in_array('4', $status_tindakan_array)){
					// $where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.statuspembayaran='1') ";
				// }
				// if (in_array('5', $status_tindakan_array)){
					// $where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.statuscheckout='1') ";
				// }
				// $where .=$where_tindakan.")";
			// }
			// $no_medrec=$this->input->post('no_medrec');
			// if ($no_medrec !=''){
				// $where .=" AND (H.no_medrec LIKE '%".$no_medrec."%') ";
			// }
			// $namapasien=$this->input->post('namapasien');
			// if ($namapasien !=''){
				// $where .=" AND (H.namapasien LIKE '%".$namapasien."%') ";
			// }
			
			// if ($idrekanan!='#'){
				// $where .=" AND (H.idrekanan='$idrekanan') ";
			// }
			// if ($idkelompokpasien!='#'){
				// $where .=" AND (H.idkelompokpasien='$idkelompokpasien') ";
			// }
			// if ($iddokter_perujuk!='#'){
				// $where .=" AND (H.iddokter_perujuk='$iddokter_perujuk') ";
			// }
			// if ($idbed!='#'){
				// $where .=" AND (H.idbed='$idbed') ";
			// }
			// if ($idruangan!='#'){
				// $where .=" AND (H.idruangan='$idruangan') ";
			// }
			// if ($idkelas!='#'){
				// $where .=" AND (H.idkelas='$idkelas') ";
			// }
			// if ($idtipe!='#'){
				// $where .=" AND (H.idtipe='$idtipe') ";
			// }
			// if ($idtipe_asal!='#'){
				// $where .=" AND (H.idtipe_asal='$idtipe_asal') ";
			// }
			// if ($norujukan !=''){
				// $where .=" AND (H.nopermintaan LIKE '%".$norujukan."%') ";
			// }
			// if ($nopendaftaran !=''){
				// $where .=" AND (H.nopendaftaran LIKE '%".$nopendaftaran."%' OR H.nopendaftaran_poli LIKE '%".$nopendaftaran."%') ";
			// }
			
			// if ($cari_pasien !=''){
				// $where .=" AND (H.namapasien LIKE '%".$cari_pasien."%' OR H.no_medrec LIKE '%".$cari_pasien."%') ";
			// }
			// // print_r($where);exit;
			// $tab =$this->input->post('tab');
			// if ($tab=='2'){
				// $where .=" AND (H.statuskasir='1'  AND H.statuspembayaran='0') ";
			// }
			
			// if ($tab=='3'){
				// $where .=" AND (H.st_terima_ranap='1' AND H.status='1') ";
			// }
			// if ($tab=='4'){
				// $where .=" AND (H.statuspembayaran='1') ";
			// }
			// if ($tab=='5'){
				// $where .=" AND (H.statuscheckout='1') ";
			// }
			// if ($status_tindakan_index=='2'){
				// $where .=" AND (H.statuskasir='1'  AND H.statuspembayaran='0') ";
			// }
			
			// if ($status_tindakan_index=='3'){
				// $where .=" AND (H.st_terima_ranap='1' AND H.status='1') ";
			// }
			// if ($status_tindakan_index=='4'){
				// $where .=" AND (H.statuspembayaran='1') ";
			// }
			// if ($status_tindakan_index=='5'){
				// $where .=" AND (H.statuscheckout='1') ";
			// }
			
			// if ($tanggal_1 !=''){
				// $where .=" AND DATE(H.tanggaldaftar) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggaldaftar) <='".YMDFormat($tanggal_2)."'";
			// }else{
				// $where .=" AND DATE(H.tanggaldaftar) >='".date('Y-m-d')."'";
			// }
			// $tanggal_1_trx =$this->input->post('tanggal_1_trx');
			// if ($tanggal_1_trx !=''){
				// $where .=" AND DATE(H.tanggaldaftar) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggaldaftar) <='".YMDFormat($tanggal_2_trx)."'";
			// }
			// $tanggal_1_pulang =$this->input->post('tanggal_1_pulang');
			// if ($tanggal_1_pulang !=''){
				// $where .=" AND DATE(H.tanggalcheckout) >='".YMDFormat($tanggal_1_pulang)."' AND DATE(H.tanggalcheckout) <='".YMDFormat($tanggal_2_pulang)."'";
			// }
			
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$iddokter =$this->input->post('iddokter');
			$idtipe =$this->input->post('idtipe');
			$idtipe_asal =$this->input->post('idtipe_asal');
			$cari_pasien =$this->input->post('cari_pasien');
			$nopendaftaran=$this->input->post('nopendaftaran');
			$idruangan=$this->input->post('idruangan');
			$idkelas=$this->input->post('idkelas');
			$idbed=$this->input->post('idbed');
			$norujukan=$this->input->post('norujukan');
			$iddokter_perujuk=$this->input->post('iddokter_perujuk');
			$idkelompokpasien=$this->input->post('idkelompokpasien');
			$idrekanan=$this->input->post('idrekanan');
			$status_tindakan_index=$this->input->post('status_tindakan_index');
			
			$idpoliklinik_array=$this->input->post('idpoliklinik_array');
			if ($idpoliklinik_array){
				$idpoliklinik_array=implode(", ", $idpoliklinik_array);
				$where .=" AND (H.idpoliklinik_asal IN (".$idpoliklinik_array.")) ";
			}
			$iddokter_perujuk_array=$this->input->post('iddokter_perujuk_array');
			if ($iddokter_perujuk_array){
				$iddokter_perujuk_array=implode(", ", $iddokter_perujuk_array);
				$where .=" AND (H.iddokter_perujuk IN (".$iddokter_perujuk_array.")) ";
			}
			$iddokter_dpjp=$this->input->post('iddokter_dpjp');
			if ($iddokter_dpjp){
				$iddokter_dpjp=implode(", ", $iddokter_dpjp);
				$where .=" AND (H.iddokterpenanggungjawab IN (".$iddokter_dpjp.")) ";
			}
			$dpjp_pendukung_array=$this->input->post('dpjp_pendukung_array');
			if ($dpjp_pendukung_array){
				$dpjp_pendukung_array=implode(", ", $dpjp_pendukung_array);
				$where .=" AND (HP.iddokter IN (".$dpjp_pendukung_array.")) ";
			}
			$idkelompokpasien_array=$this->input->post('idkelompokpasien_array');
			if ($idkelompokpasien_array){
				$idkelompokpasien_array=implode(", ", $idkelompokpasien_array);
				$where .=" AND (H.idkelompokpasien IN (".$idkelompokpasien_array.")) ";
			}
			$idrekanan_array=$this->input->post('idrekanan_array');
			if ($idrekanan_array){
				$idrekanan_array=implode(", ", $idrekanan_array);
				$where .=" AND (H.idrekanan IN (".$idrekanan_array.")) ";
			}
			$idruangan_array=$this->input->post('idruangan_array');
			if ($idruangan_array){
				$idruangan_array=implode(", ", $idruangan_array);
				$where .=" AND (H.idruangan IN (".$idruangan_array.")) ";
			}
			$idkelas_array=$this->input->post('idkelas_array');
			if ($idkelas_array){
				$idkelas_array=implode(", ", $idkelas_array);
				$where .=" AND (H.idkelas IN (".$idkelas_array.")) ";
			}
			$idbed_array=$this->input->post('idbed_array');
			if ($idbed_array){
				$idbed_array=implode(", ", $idbed_array);
				$where .=" AND (H.idkelas IN (".$idbed_array.")) ";
			}
			$status_tindakan_array=$this->input->post('status_tindakan_array');
			if ($status_tindakan_array){
				$where .=" AND (";
				$where_tindakan='';
				
				if (in_array('2', $status_tindakan_array)){
					// if ($statuskasir=='1' && $statuspembayaran=='0'){
					$where_tindakan .=($where_tindakan==''?'':' OR ')."(H.statuskasir='1' AND H.statuspembayaran='0') ";
				}
				if (in_array('3', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.st_terima_ranap='1' AND H.status='1') ";
				}
				if (in_array('4', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.statuspembayaran='1') ";
				}
				if (in_array('5', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.statuscheckout='1') ";
				}
				$where .=$where_tindakan.")";
			}
			$no_medrec=$this->input->post('no_medrec');
			if ($no_medrec !=''){
				$where .=" AND (H.no_medrec LIKE '%".$no_medrec."%') ";
			}
			$namapasien=$this->input->post('namapasien');
			if ($namapasien !=''){
				$where .=" AND (H.namapasien LIKE '%".$namapasien."%') ";
			}
			
			
			if ($iddokter!='#'){
				$where .=" AND (H.iddokterpenanggungjawab='$iddokter') ";
			}
			if ($idrekanan!='#'){
				$where .=" AND (H.idrekanan='$idrekanan') ";
			}
			if ($idkelompokpasien!='#'){
				$where .=" AND (H.idkelompokpasien='$idkelompokpasien') ";
			}
			if ($iddokter_perujuk!='#'){
				$where .=" AND (H.iddokter_perujuk='$iddokter_perujuk') ";
			}
			if ($idbed!='#'){
				$where .=" AND (H.idbed='$idbed') ";
			}
			if ($idruangan!='#'){
				$where .=" AND (H.idruangan='$idruangan') ";
			}
			if ($idkelas!='#'){
				$where .=" AND (H.idkelas='$idkelas') ";
			}
			if ($idtipe!='#'){
				$where .=" AND (H.idtipe='$idtipe') ";
			}
			if ($idtipe_asal!='#'){
				$where .=" AND (H.idtipe_asal='$idtipe_asal') ";
			}
			if ($norujukan !=''){
				$where .=" AND (H.nopermintaan LIKE '%".$norujukan."%') ";
			}
			if ($nopendaftaran !=''){
				$where .=" AND (H.nopendaftaran LIKE '%".$nopendaftaran."%' OR H.nopendaftaran_poli LIKE '%".$nopendaftaran."%') ";
			}
			
			if ($cari_pasien !=''){
				$where .=" AND (H.namapasien LIKE '%".$cari_pasien."%' OR H.no_medrec LIKE '%".$cari_pasien."%') ";
			}
			// print_r($where);exit;
			$tab =$this->input->post('tab');
			if ($tab=='2'){
				$where .=" AND (H.statuskasir='1'  AND H.statuspembayaran='0') ";
			}
			
			if ($tab=='3'){
				$where .=" AND (H.st_terima_ranap='1' AND H.status='1' AND H.statuscheckout='0') ";
			}
			if ($tab=='4'){
				$where .=" AND (H.statuspembayaran='1') ";
			}
			if ($tab=='5'){
				$where .=" AND (H.statuscheckout='1') ";
			}
			if ($status_tindakan_index=='2'){
				$where .=" AND (H.statuskasir='1'  AND H.statuspembayaran='0') ";
			}
			
			if ($status_tindakan_index=='3'){
				$where .=" AND (H.st_terima_ranap='1' AND H.status='1') ";
			}
			if ($status_tindakan_index=='4'){
				$where .=" AND (H.statuspembayaran='1') ";
			}
			if ($status_tindakan_index=='5'){
				$where .=" AND (H.statuscheckout='1') ";
				if ($tanggal_1 !=''){
					$where .=" AND DATE(H.tanggaldaftar) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggaldaftar) <='".YMDFormat($tanggal_2)."'";
				}else{
					$where .=" AND DATE(H.tanggaldaftar) >='".date('Y-m-d')."'";
				}
			}
			
			
			$tanggal_1_trx =$this->input->post('tanggal_1_trx');
			if ($tanggal_1_trx !=''){
				$where .=" AND DATE(H.tanggaldaftar) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggaldaftar) <='".YMDFormat($tanggal_2_trx)."'";
			}
			$tanggal_1_pulang =$this->input->post('tanggal_1_pulang');
			if ($tanggal_1_pulang !=''){
				$where .=" AND DATE(H.tanggalcheckout) >='".YMDFormat($tanggal_1_pulang)."' AND DATE(H.tanggalcheckout) <='".YMDFormat($tanggal_2_pulang)."'";
			}
			
			$this->select = array();
			$from="
					(
						SELECT MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
						,MK.nama as nama_kelompok,MR.nama as nama_rekanan,MDP.nama as nama_dokter_poli
						,H.*,MKL.nama as nama_kelas 
						,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header
						,MB.nama as nama_bed,TPL.tanggal,MRU.nama as nama_ruangan,AH.kodeantrian as kode_antrian
						,UH.name as nama_hapus,GROUP_CONCAT(MDHP.nama SEPARATOR ', ') as nama_dokter_pendamping
						,TP.suhu,TP.nadi,TP.nafas,TP.td_sistole,TP.td_diastole,TP.tinggi_badan,TP.berat_badan
						,mnadi.warna as warna_nadi
						,mnafas.warna as warna_nafas
						,mtd_sistole.warna as warna_sistole
						,mtd_diastole.warna as warna_diastole
						,msuhu.warna as warna_suhu
						,TP.berat_badan/((TP.tinggi_badan/100)*2) as masa_tubuh,mberat.warna as warna_berat,TT.nama_tindakan_setuju		
						FROM trawatinap_pendaftaran H
						LEFT JOIN trawatinap_pendaftaran_dpjp HP ON HP.pendaftaran_id=H.id
						LEFT JOIN mdokter MDHP ON MDHP.id=HP.iddokter
						LEFT JOIN tpoliklinik_pendaftaran TPL ON TPL.id=H.idpoliklinik
						LEFT JOIN mruangan MRU ON MRU.id=H.idruangan
						LEFT JOIN mkelas MKL ON MKL.id=H.idkelas
						LEFT JOIN antrian_harian AH ON AH.id=H.antrian_id
						LEFT JOIN mdokter MDP ON MDP.id=TPL.iddokter
						LEFT JOIN mdokter MD ON MD.id=H.iddokterpenanggungjawab
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik_asal
						LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
						LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						LEFT JOIN mbed MB on MB.id=H.idbed						
						LEFT JOIN musers UH on UH.id=H.deleted_by	
						LEFT JOIN (
							".get_alergi_sql()."
						) A ON A.idpasien=H.idpasien	
						LEFT JOIN (
							".get_tindakan_persetujuan()."
						) TT ON TT.idrawatinap=H.id
						LEFT JOIN (SELECT * FROM (SELECT * FROM tranap_ttv WHERE status_ttv > 1 ORDER BY tanggal_input DESC) XX GROUP BY pendaftaran_id_ranap) TP ON TP.pendaftaran_id_ranap = H.id
						LEFT JOIN mnadi ON TP.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
						LEFT JOIN mnafas ON TP.nafas BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
						LEFT JOIN mtd_sistole ON TP.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
						LEFT JOIN mtd_diastole ON TP.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
						LEFT JOIN msuhu ON TP.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
						LEFT JOIN mberat ON TP.berat_badan/((TP.tinggi_badan/100)*2) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
						
						WHERE H.idtipe IS NOT NULL ".$where."
						GROUP BY H.id
						
						ORDER BY H.tanggaldaftar DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();
      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
		$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $def_image='';
		  $btn_persetujuan_pulang=panel_persetujuan_pulang($r->status_proses_card_pass,$r->nopendaftaran);
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  if ($r->idtipe=='1'){
			  $info_bed='<div class="h4 push-10-t text-primary"><button class="btn btn-xs btn-block btn-primary push-10" type="button"><i class="fa fa-bed pull-left"></i> '.($r->nama_bed).'</button></strong></div>';
		  }else{
			  $info_bed='';
		  }
		   if ($r->nama_tindakan_setuju){
			  
			  $btn_tindakan_setujua='
				<div class="text-danger font-s13 push-5-t text-bold">
				<button class="btn btn-success  btn-xs" type="button" onclick="show_list_tindakan('.$r->id.')">'.$r->nama_tindakan_setuju.'</button>
				</div>
			  ';
		  }else{
			  $btn_tindakan_setujua='';
		  }
		  $div_terima='';
		  $div_hapus='';
		  if ($r->st_terima_ranap=='1'){
			  $div_terima='<div class="text-primary text-uppercase"><i class="fa fa-bed"></i> Tanggal Terima : '.HumanDateLong($r->tanggal_terima).'</div>';
			  $div_terima.='<div class="text-primary text-uppercase"><i class="fa fa-user"></i> User Terima : '.($r->user_nama_terima).'</div>';
		  }
		  if ($r->status=='0'){
			  $div_hapus='<div class="text-danger text-uppercase"><i class="fa fa-trash"></i> Tanggal Batal : '.HumanDateLong($r->deleted_date).'</div>';
			  $div_hapus.='<div class="text-danger text-uppercase"><i class="fa fa-user"></i> User  : '.($r->nama_hapus).'</div>';
		  }
		  $btn_checkout='';
		  $btn_terima='';
		  $btn_layani='';
		  $btn_panel='';
		  $pedamping='';
		  if ($r->nama_dokter_pendamping){
			  $pedamping='<div class="text-wrap text-danger"><i class="fa fa-user-md"></i> PENDAMPING : '.($r->nama_dokter_pendamping).'</div>';
		  }
			  if ($r->status!='0'){
					  $btn_layani='<div class="push-5-t"><a href="'.base_url().'tpendaftaran_ranap_erm/tindakan/'.$r->id.'/erm_ri" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block btn-primary btn-xs menu_click"><i class="fa fa-user-md"></i> LAYANI</a> </div>';
				  if ($r->st_terima_ranap=='1' && $r->statuspembayaran=='1'){
					  if (UserAccesForm($user_acces_form,array('1931'))){
						$btn_checkout='<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Checkout" onclick="show_modal_checkout('.$r->id.')" class="btn btn-block btn-danger btn-xs"><i class="si si-logout"></i> DATA CHECKOUT</a> </div>';
					  }
					 
				  }
					  $btn_panel=div_panel_kendali_ri($user_acces_form,$r->id);
			  }
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									'.$btn_layani.'
									'.$btn_panel.'
									'.$btn_checkout.'
									'.$btn_terima.'
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
										<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
									</div>
									<div class="text-primary font-s13 push-5-t">'.$r->nopendaftaran_poli.' | '.HumanDateShort($r->tanggal).' | '.GetAsalRujukanR($r->idtipe_asal).' | '.$r->nama_poli.' | '.$r->nama_dokter_poli.'</div>
									<div class="text-primary font-s13 push-5-t">'.$btn_tindakan_setujua.$btn_persetujuan_pulang.'</div>
									
									</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_5='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Suhu Tubuh :</div>
									<div class=" text-muted " style="color:'.$r->warna_suhu.'"> '.($r->suhu).' '.$data_satuan_ttv['satuan_suhu'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frek Nadi :</div>
									<div class=" text-muted " style="color:'.$r->warna_nadi.'"> '.($r->nadi).' '.$data_satuan_ttv['satuan_nadi'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frek Nafas :</div>
									<div class=" text-muted " style="color:'.$r->warna_nafas.'"> '.($r->nafas).' '.$data_satuan_ttv['satuan_nafas'].'</div>
								</td>
								
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Tekanan Darah :</div>
									<div class=" text-muted "> <label  class="text-muted" style="color:'.$r->warna_sistole.'">'.($r->td_sistole.' '.$data_satuan_ttv['satuan_td'].'</label> / <label class="text-muted " style="color:'.$r->warna_diastole.'">'.$r->td_diastole).' '.$data_satuan_ttv['satuan_td'].'</label></div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Berat Badan :</div>
									<div class=" text-muted " style="color:'.$r->warna_berat.'"> '.($r->berat_badan).' Kg</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Tinggi Badan :</div>
									<div class=" text-muted style="color:'.$r->warna_berat.'""> '.($r->tinggi_badan).' Cm</div>
								</td>
								
							</tr>
						</tbody>
					</table>';
		  
          $result[] = $btn_5;
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class=" text-uppercase">'.($r->nama_ruangan).' - '.$r->nama_kelas.' - '.$r->nama_bed.'</div>
									<div class=""><i class="fa fa-user-md"></i> DPJP : '.($r->nama_dokter).'</div>
									'.$pedamping.'
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->idkelompokpasien=='1'?'ASURANSI : '.$r->nama_rekanan:$r->nama_kelompok).'</div>
									'.$div_terima.'
									'.$div_hapus.'
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.$this->status_ranap($r->status,$r->st_terima_ranap,$r->statuscheckout,$r->statuspembayaran,$r->statuskasir).'</div>
									'.$info_bed.'
									<div class="h5 push-10-t text-primary"><strong>'.GetTipeRujukanRawatInap($r->idtipe).'</strong></div>
									<div class="h5 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggaldaftar).'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function status_ranap($status,$st_terima_ranap,$statuscheckout,$statuspembayaran,$statuskasir){
	  $label='<button class="btn btn-xs btn-block btn-primary push-10" type="button"><i class="fa fa-facebook pull-left "></i> Login with Facebook</button>';
	  if ($status=='0'){
		  $label='<button class="btn btn-xs btn-block btn-danger push-10" type="button"><i class="fa fa-times pull-left "></i>DIBATALKAN</button>';
	  }else{
		  if ($st_terima_ranap=='0'){
			  $label='<button class="btn btn-xs btn-block btn-info push-10" type="button"><i class="fa fa-wrench pull-left "></i>MENUNGGU DIRAWAT</button>';
		  }
		  if ($st_terima_ranap=='1' && $statuscheckout=='0'){
			  $label='<button class="btn btn-xs btn-block btn-warning push-10" type="button"><i class="fa fa-bed pull-left"></i>DIRAWAT</button>';
		  }
		 
		  if ($statuskasir=='1' && $statuspembayaran=='0'){
			  $label='<button class="btn btn-xs btn-block btn-danger push-10" type="button">MENUNGGU TRANSAKSI</button>';
		  }elseif ($statuskasir=='1' && $statuspembayaran=='1'){
			  $label='<button class="btn btn-xs btn-block btn-primary push-10" type="button">SELESAI</button>';
		  }
		   
		  if ($statuscheckout=='1'){
			  $label='<button class="btn btn-xs btn-block btn-success push-10" type="button">PULANG</button>';
		  }
	  }
	  
	  return $label;
  }
	
}
