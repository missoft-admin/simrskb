<?php
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Tpendapatan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Tpendapatan_model');
        $this->load->model('Mrka_bendahara_model');
    }

    public function index()
    {
        $data = array(
          'idpendapatan' => '',
          'iduser' => '',
          'tanggaldari' =>'',
          'tanggalsampai' => '',
        );
        $data['error']      = '';
        $data['title']      = 'Transaksi Pendapatan Lain-lain';
        $data['content']    = 'Tpendapatan/index';
        $data['breadcrum']  = array(
                                 array("RSKB Halmahera",'dashboard'),
                                 array("Bendahara",'#'),
                                 array("Transaksi Pendapatan Lain-lain",'tpendapatan'),
                                 array("List",'')
                              );

        $data['list_biaya'] = $this->Tpendapatan_model->getListBiaya();
        $data['list_user'] = $this->Tpendapatan_model->getListUser();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filter()
    {
        $data = array(
          'idpendapatan' => $this->input->post('idpendapatan'),
          'iduser' => $this->input->post('iduser'),
          'tanggaldari' => $this->input->post('tanggaldari'),
          'tanggalsampai' => $this->input->post('tanggalsampai'),
        );

    		$this->session->set_userdata($data);

        $data['error']      = '';
        $data['title']      = 'Transaksi Pendapatan Lain-lain';
        $data['content']    = 'Tpendapatan/index';
        $data['breadcrum']  = array(
                                 array("RSKB Halmahera",'dashboard'),
                                 array("Bendahara",'#'),
                                 array("Transaksi Pendapatan Lain-lain",'tpendapatan'),
                                 array("List",'')
                              );

        $data['list_biaya'] = $this->Tpendapatan_model->getListBiaya();
        $data['list_user'] = $this->Tpendapatan_model->getListUser();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create($disabel='')
    {
        $data = array(
            'id'            => '',
            'notransaksi'   => '',
            'tanggal'       => date("Y-m-d"),
            'idpendapatan'  => '',
            'keterangan'    => '',
            'terimadari'    => '',
            'noakun'        => '',
            'debit'         => '',
            'kredit'        => '',
            'status'        => '',
            'statusjurnal'  => '',
            'disabel'  => $disabel,
          );
		$data['list_pembayaran'] 	= array();
        $data['error']      = '';
        $data['title']      = 'Tambah Transaksi Pendapatan Lain-lain';
        $data['content']    = 'Tpendapatan/manage';
        $data['breadcrum']  = array(
                                 array("RSKB Halmahera",'dashboard'),
                                 array("Bendahara",'#'),
                                 array("Transaksi Pendapatan Lain-lain",'tpendapatan'),
                                 array("Tambah",'')
                              );

		$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
        $data['list_biaya'] = $this->Tpendapatan_model->getListBiaya();
        $data['list_dari'] = $this->Tpendapatan_model->list_dari();
        $data['list_kasbesar'] = $this->Tpendapatan_model->getListKasBesar();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	function refresh_dari(){
		
		$q="SELECT * FROM mdari M
			WHERE M.`status`='1'
			ORDER BY M.nama ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function refresh_tipe(){
		
		$q="SELECT * FROM mpendapatan M
			WHERE M.`status`='1'
			ORDER BY M.keterangan ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->keterangan.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
    public function update($id,$disabel='')
    {
      if($id != ''){
        $data = $this->Tpendapatan_model->getSpecified($id);
       
          $data['disabel']      = $disabel;
          $data['error']      = '';
          $data['title']      = 'Ubah Transaksi Pendapatan Lain-lain';
          $data['content']    = 'Tpendapatan/manage';
          $data['breadcrum']  = array(
                                 array("RSKB Halmahera",'dashboard'),
                                 array("Bendahara",'#'),
                                 array("Transaksi Pendapatan Lain-lain",'tpendapatan'),
                                 array("Ubah",'')
                              );

        $data['list_biaya'] = $this->Tpendapatan_model->getListBiaya();
        $data['list_kasbesar'] = $this->Tpendapatan_model->getListKasBesar();
        $data['list_dari'] = $this->Tpendapatan_model->list_dari();
        $data['list_pembayaran'] = $this->Tpendapatan_model->getListPembayaran($id);
		$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
        $data = array_merge($data,backend_info());
        $this->parser->parse('module_template',$data);
       
      }else{
        $this->session->set_flashdata('error',true);
        $this->session->set_flashdata('message_flash','data tidak ditemukan.');
        redirect('Tpendapatan');
      }
    }

    public function save()
    {
		// print_r($this->input->post());exit();
		if($this->input->post('idtransaksi') == '' ) {
			if($this->Tpendapatan_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tpendapatan','location');
			}
		} else {
			if($this->Tpendapatan_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tpendapatan','location');
			}
		}
      // $data = array();
	  // if ($this->input->post('idtransaksi')==''){
		  // $this->Tpendapatan_model->saveData();
		  
	  // }else{
		  
	  // }
      // $data['idtransaksi'] = $this->Tpendapatan_model->saveData();
      // $data = array_merge($data, backend_info());
      // $this->parser->parse('Tpendapatan/redirect', $data);
    }

    public function after_save()
    {
      $this->session->set_flashdata('confirm', true);
      $this->session->set_flashdata('message_flash', 'data telah disimpan.');
      redirect('Tpendapatan', 'location');
    }

    public function approve($idtransaksi)
    {
        if ($this->Tpendapatan_model->updateData($idtransaksi)) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('Tpendapatan', 'location');
        }
    }

    public function kwitansi($id)
    {
      // instantiate and use the dompdf class
      $dompdf = new Dompdf();

      if($id != ''){
        $data = $this->Tpendapatan_model->getSpecifiedHeader($id);
		
          $data['error']      = '';
          $data['title']      = 'Invoice Pendapatan Lain-lain';
          $data['content']    = 'Tpendapatan/kwitansi';
          $data['breadcrum']  = array(
                                 array("RSKB Halmahera",'dashboard'),
                                 array("Bendahara",'#'),
                                 array("Transaksi Pendapatan Lain-lain",'tpendapatan'),
                                 array("Invoice",'')
                              );

          $data['list_biaya'] = $this->Tpendapatan_model->getListBiaya();
          $data['list_kasbesar'] = $this->Tpendapatan_model->getListKasBesar();
          $data['list_pembayaran'] = $this->Tpendapatan_model->getListPembayaran($id);

          $html = $this->load->view('Tpendapatan/kwitansi', $data, true);

          $dompdf->loadHtml($html);

          // (Optional) Setup the paper size and orientation
          $dompdf->setPaper('A4', 'portrait');

          // Render the HTML as PDF
          $dompdf->render();

          // // Output the generated PDF to Browser
          $dompdf->stream('Kwitansi Bendahara.pdf', array("Attachment"=>false));
          exit(0);
        }else{
          $this->session->set_flashdata('error',true);
          $this->session->set_flashdata('message_flash','data tidak ditemukan.');
          redirect('Tpendapatan','location');
        }
      // }else{
        // $this->session->set_flashdata('error',true);
        // $this->session->set_flashdata('message_flash','data tidak ditemukan.');
        // redirect('Tpendapatan');
      // }
    }

    function getIndex($uri='')
    {
		// idpendapatan:idpendapatan,
		// tanggaldari:tanggaldari,
		// tanggalsampai:tanggalsampai,
		// iduser:iduser,
		
		$idpendapatan=$this->input->post('idpendapatan');
		$tanggaldari=$this->input->post('tanggaldari');
		$tanggalsampai=$this->input->post('tanggalsampai');
		$iduser=$this->input->post('iduser');
		$where='';
		
		if ($idpendapatan !='#'){
			$where .=" AND H.idpendapatan='$idpendapatan'";
		}
		if ($tanggaldari !=''){
			$where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggaldari)."'";
		}
		if ($tanggalsampai !=''){
			$where .=" AND DATE(H.tanggal) <='".YMDFormat($tanggalsampai)."'";
		}
		if ($iduser !='#'){
			$where .=" AND H.user_created='$iduser'";
		}
		
		$from="(
			SELECT H.*,M.keterangan as nama_pendapatan,DR.nama terima_nama,COUNT(DISTINCT Doc.id) as doc FROM tbendahara_pendapatan H
			LEFT JOIN mpendapatan M ON H.idpendapatan=M.id
			LEFT JOIN mdari DR ON H.terimadari=DR.id
			LEFT JOIN tbendahara_pendapatan_dokumen Doc ON Doc.idtransaksi=H.id
			WHERE H.id IS NOT NULL ".$where."
			GROUP BY H.id
		)as tbl";
		
		$this->select = array();
		$this->from   = $from;
		$this->join 	= array();
		$this->where  = array();

		$this->order  = array();
		$this->group  = array();

        $this->column_search   = array('notransaksi','nama_pendapatan','keterangan','terima_nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $action = '';
			if ($r->status=='1'){
				$action .= '<a href="'.site_url().'tpendapatan/update/'.$r->id.'/disabled" data-toggle="tooltip" title="Lihat" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
			
				if ($r->st_verifikasi=='0'){
					$action .= '<button class="btn btn-primary btn-xs verif"><i class="fa fa-check"></i></button>';
					$action .= '<a href="'.site_url().'tpendapatan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>';
					$action .= '<button class="btn btn-danger btn-xs hapus"><i class="fa fa-trash"></i></button>';
				}else{
					
				}
				$action .= '<a href="'.site_url().'tpendapatan/kwitansi/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Cetak Kwitansi" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
				if ($r->doc){
				$action .= '<a href="'.base_url().'tpendapatan/upload_document/'.$r->id.'" title="Upload Document" class="btn btn-xs btn-warning" ><i class="fa fa-file-image-o"></i></a>';
			  }else{
				$action .= '<a href="'.base_url().'tpendapatan/upload_document/'.$r->id.'" title="Upload Document" class="btn btn-xs btn-danger" ><i class="fa fa-upload"></i></a>';
				  
			  }
			}else{
				$action='<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">DIBATALKAN</span>';
			}
  			
		  
            $row[] = $no;
  			$row[] = $r->notransaksi;
  			$row[] = HumanDateShort($r->tanggal);
            $row[] = $r->nama_pendapatan;
            $row[] = $r->keterangan;
            $row[] = $r->terima_nama;
            $row[] = number_format($r->nominal);
            $row[] = '<div class="btn-group">'.$action.'</div>';
            $row[] = $r->id;

            $data[] = $row;
        }
        $output = array(
  	      "draw" => $_POST['draw'],
  	      "recordsTotal" => $this->datatable->count_all(),
  	      "recordsFiltered" => $this->datatable->count_all(),
  	      "data" => $data
        );
        echo json_encode($output);
    }
	function verif($id){
		// $id=$this->input->post('id');
		$this->Tpendapatan_model->insert_jurnal_pendapatan($id);
		$result=$this->db->query("UPDATE tbendahara_pendapatan set st_verifikasi='1' WHERE id='$id'");
		echo json_encode($result);
	}
	function hapus($id){
		// $id=$this->input->post('id');
		$result=$this->db->query("UPDATE tbendahara_pendapatan set status='0' WHERE id='$id'");
		echo json_encode($result);
	}
    public function get_noakun($id)
    {
        $arr = $this->Tpendapatan_model->getNoAkun($id);
        $this->output->set_output(json_encode($arr));
    }
	public function upload_document($idtransaksi)
    {
       
        $data = $this->Tpendapatan_model->getSpecifiedHeader($idtransaksi);
		// print_r($data);exit();
       

        $data['error']      = '';
        $data['title'] = 'Upload Dokumen Pendapatan';
        $data['content'] = 'Tpendapatan/upload_dokumen';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Pendapatan", 'Tpendapatan/index'),
            array("Upload Dokumen", '#')
        );

        // $data['listFiles'] = array();
        $data['listFiles'] = $this->Tpendapatan_model->getListUploadedDocument($idtransaksi);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	public function upload_files()
    {
        $files = array();
        foreach ($_FILES as $file) {
            if ($file['name'] != '') {
                $config['upload_path'] = './assets/upload/pendapatan/';
                $config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';

                $files[] = $file['name'];

                $this->upload->initialize($config);

                if ($this->upload->do_upload('file')) {
                    $image_upload = $this->upload->data();
                    $table = 'tbendahara_pendapatan_dokumen';

                    $data = array();
                    $data['idtransaksi']  = $this->input->post('idtransaksi');
                    $data['keterangan']  = $this->input->post('keterangan');
                    $data['upload_by']  = $this->session->userdata('user_id');
                    $data['upload_by_nama']  = $this->session->userdata('user_name');
                    $data['upload_date']  = date('Y-m-d H:i:s');
                    $data['filename'] = $image_upload['file_name'];
                    $data['size'] = formatSizeUnits($file['size']);

                    $this->db->insert($table, $data);

                    return true;
                } else {
                    print_r($this->upload->display_errors());
                    exit();

                    return false;
                }
            } else {
                return true;
            }
        }
    }
	function refresh_image($id){		
		$arr['detail'] = $this->Tpendapatan_model->refresh_image($id);
		$this->output->set_output(json_encode($arr));
	}
	 public function delete_file($idfile)
    {
        $this->db->where('id', $idfile);
        $query = $this->db->get('tbendahara_pendapatan_dokumen');
        $row = $query->row();
        if ($query->num_rows() > 0) {
            $this->db->where('id', $idfile);
            if ($this->db->delete('tbendahara_pendapatan_dokumen')) {
                if (file_exists('./assets/upload/pendapatan/'.$row->filename) && $row->filename !='') {
                    unlink('./assets/upload/pendapatan/'.$row->filename);
                }
            }
        }

        return true;
    }
	function insert_jurnal_pendapatan($id){
		$this->Tpendapatan_model->insert_jurnal_pendapatan($id);
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
