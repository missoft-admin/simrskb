<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Erm_jenis_operasi extends CI_Controller {

	/**
	 * Jenis Operasi controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Erm_jenis_operasi_model');
  }

	function index(){
		
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Jenis Operasi';
		$data['content'] 		= 'Erm_jenis_operasi/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Jenis Operasi",'#'),
									    			array("List",'erm_jenis_operasi')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'kelompok_id' 					=> '',
			'status' 				=> '1'
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Jenis Operasi';
		$data['content'] 		= 'Erm_jenis_operasi/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Jenis Operasi",'#'),
									    			array("Tambah",'erm_jenis_operasi')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Erm_jenis_operasi_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'kelompok_id' 					=> $row->kelompok_id,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Jenis Operasi';
				$data['content']	 	= 'Erm_jenis_operasi/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Jenis Operasi",'#'),
											    			array("Ubah",'erm_jenis_operasi')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('erm_jenis_operasi','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('erm_jenis_operasi');
		}
	}

	function delete($id){
		// print_r($id);exit();
		$this->Erm_jenis_operasi_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('erm_jenis_operasi','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Erm_jenis_operasi_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('erm_jenis_operasi','location');
				}
			} else {
				if($this->Erm_jenis_operasi_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('erm_jenis_operasi','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 		= validation_errors();
		$data['content'] 	= 'Erm_jenis_operasi/manage';

		if($id==''){
			$data['title'] = 'Tambah Jenis Operasi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Jenis Operasi",'#'),
															array("Tambah",'erm_jenis_operasi')
													);
		}else{
			$data['title'] = 'Ubah Jenis Operasi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Jenis Operasi",'#'),
															array("Ubah",'erm_jenis_operasi')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	  
	$data_user=get_acces();
	$user_acces_form=$data_user['user_acces_form'];
	$this->select = array();
	$from="
			(
				SELECT H.*,M.ref as nama_kelompok FROM erm_jenis_operasi H
			LEFT JOIN merm_referensi M ON M.nilai=H.kelompok_id AND M.ref_head_id='124'
			WHERE H.`status`='1'
			ORDER BY H.id DESC
			) as tbl
		";
	// print_r($from);exit();
	$this->from   = $from;
	$this->join 	= array();
	
	
	$this->order  = array();
	$this->group  = array();
	$this->column_search = array('nama_dokter','namapasien','no_medrec');
	$this->column_order  = array();


      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = $r->nama_kelompok;
          $aksi = '<div class="btn-group">';
          
		  if (UserAccesForm($user_acces_form,array('1900'))){
          	$aksi .= '<a href="'.site_url().'erm_jenis_operasi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
          }
          
		  if (UserAccesForm($user_acces_form,array('1901'))){
          	$aksi .= '<a href="#" data-urlindex="'.site_url().'erm_jenis_operasi" data-urlremove="'.site_url().'erm_jenis_operasi/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
          }
          $aksi .= '</div>';
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function ajaxSave(){
		if ($this->Erm_jenis_operasi_model->saveData()) {
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			$data['response']="data telah di simpan";
			$data['code']=200;
			$data['id']=$this->db->insert_id();
			$data['nama']=$this->input->post("nama");
			echo json_encode($data);
		}
	}
}
