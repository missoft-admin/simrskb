<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mvariable extends CI_Controller {

	/**
	 * Variable controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mvariable_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Variable';
		$data['content'] 		= 'Mvariable/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Variable",'#'),
									    			array("List",'mvariable')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'tipe_id'			=> '#',
			'jenis_id'			=> '#',
			'status' 				=> '',
		);

		// $data['list_akun'] 			= $this->Mvariable_model->list_user('');
		// $data['list_kas'] 			= $this->Mvariable_model->list_kas('');
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Variable';
		$data['content'] 		= 'Mvariable/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Variable",'#'),
									    			array("Tambah",'mvariable')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mvariable_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'tipe_id' 		=> $row->tipe_id,
					'jenis_id' 		=> $row->jenis_id,
					
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Variable';
				$data['content']	 	= 'Mvariable/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Variable",'#'),
											    			array("Ubah",'mvariable')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mvariable','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mvariable');
		}
	}

	function delete($id){
		$this->Mvariable_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mvariable','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		// print_r($this->input->post());exit();
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mvariable_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mvariable','location');
				}
			} else {
				if($this->Mvariable_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mvariable','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}
	
	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mvariable/manage';

		if($id==''){
			$data['title'] = 'Tambah Variable';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Variable",'#'),
															array("Tambah",'mvariable')
													);
		}else{
			$data['title'] = 'Ubah Variable';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Variable",'#'),
															array("Ubah",'mvariable')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
			$from="(SELECT M.id,M.nama
,CASE WHEN M.tipe_id='1' THEN 'PENDAPATAN' ELSE 'PENGELUARAN' END tipe
,CASE WHEN M.tipe_id='0' THEN 'NON SUB' ELSE 'SUB' END jenis,M.jenis_id,M.tipe_id,M.status

FROM mvariable M) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama','tipe','jenis');
      $this->column_order    = array('nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					if ($r->status=='1'){
					// if (UserAccesForm($user_acces_form,array('131'))){
						$aksi .= '<a href="'.site_url().'mvariable/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
					// }
					// if (UserAccesForm($user_acces_form,array('132'))){
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mvariable" data-urlremove="'.site_url().'mvariable/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
					// }
					}else{
					
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mvariable" data-urlremove="'.site_url().'mvariable/aktifkan/'.$r->id.'" data-toggle="tooltip" title="Aktifkan" class="btn btn-success btn-sm aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
					}
			        $aksi .= '</div>';

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = ($r->tipe_id=='1'?'<span class="label label-primary">Pendapatan</span>':'<span class="label label-danger">Potongan</span>');
          $row[] = ($r->jenis_id==''?'<span class="label label-primary">Sub</span>':'<span class="label label-danger">Non Sub</span>');
          $row[] = StatusBarang($r->status);
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function aktifkan($id){
		
		$this->Mvariable_model->aktifkan($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah aktif kembali.');
		redirect('mvariable','location');
	}

}
