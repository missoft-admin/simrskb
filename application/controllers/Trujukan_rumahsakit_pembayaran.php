<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';
// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

class Trujukan_rumahsakit_pembayaran extends CI_Controller
{

    /**
     * Pembayaran Rujukan Rumah Sakit controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Trujukan_rumahsakit_pembayaran_model', 'model');
    }
    
    public function index()
    {
        $data = [
			'notransaksi' => '',
			'idrujukan' => '',
			'tanggaljatuhtempo_dari' => date('d/m/Y'),
			'tanggaljatuhtempo_sampai' => date('d/m/Y'),
			'status' => '',
		];

        $data['error'] = '';
        $data['title'] = 'Pembayaran Rujukan Rumah Sakit';
        $data['content'] = 'Trujukan_rumahsakit_pembayaran/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Pembayaran Rujukan Rumah Sakit",'#'),
            array("List",'trujukan_rumahsakit_pembayaran')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filter()
    {
        $data = [
			'notransaksi' => $this->input->post('notransaksi'),
			'idrujukan' => $this->input->post('idrujukan'),
			'tanggaljatuhtempo_dari' => $this->input->post('tanggaljatuhtempo_dari'),
			'tanggaljatuhtempo_sampai' => $this->input->post('tanggaljatuhtempo_sampai'),
			'status' => $this->input->post('status'),
		];

        $this->session->set_userdata($data);

        $data['error'] = '';
        $data['title'] = 'Pembayaran Rujukan Rumah Sakit';
        $data['content'] = 'Trujukan_rumahsakit_pembayaran/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Pembayaran Rujukan Rumah Sakit",'#'),
            array("Filter",'trujukan_rumahsakit_pembayaran')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function proses_transaction($id)
    {
        if ($id != '') {
            $row = $this->model->getDetailTransaction($id);
            if (isset($row->id)) {
                $data = array(
                    'id' => $row->id,
                    'notransaksi' => $row->notransaksi,
                    'namarujukan' => $row->namarujukan,
                    'tanggal_jatuhtempo' => $row->tanggal_jatuhtempo,
                    'nominal' => number_format($row->nominal),
                );

                $data['error'] = '';
                $data['title'] = "Pembayaran Rujukan Rumah Sakit";
                $data['content'] = 'Trujukan_rumahsakit_pembayaran/transaction';
                $data['breadcrum'] = array(
                    array("RSKB Halmahera",'#'),
                    array("Pembayaran Rujukan Rumah Sakit",'#'),
                    array("Transaksi",'Trujukan_rumahsakit_pembayaran')
                );

                
                $data['nominal_pembayaran'] = 0;
                $data['sisa_pembayaran'] = 0;

                $pembayaran = $this->model->getPembayaran($id);

                if ($pembayaran) {
                    $data['detail_pembayaran'] = $this->model->getDetailPembayaran($id);
                    $data['nominal_pembayaran'] = 0;
                    $data['sisa_pembayaran'] = 0;
                } else {
                    $data['detail_pembayaran'] = array();
                    $data['nominal_pembayaran'] = 0;
                    $data['sisa_pembayaran'] = 0;
                }
                
                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('trujukan_rumahsakit_pembayaran', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('trujukan_rumahsakit_pembayaran');
        }
    }

    public function approve_transaction($id)
    {
        if ($this->model->approveTransaction($id)) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'transaksi telah disetujui.');
            redirect('trujukan_rumahsakit_pembayaran', 'location');
        }
    }

    public function print_transaction($id)
    {
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $options->set('enable-javascript', true);
        $options->set('javascript-delay', 13500);
        $options->set('enable-smart-shrinking', true);
        $options->set('no-stop-slow-scripts', true);
        $dompdf = new Dompdf($options);

        $row = $this->model->getPengajuan($id);
        if ($row) {
            $data = array();
            $data['idtransaksi'] = $row->id;
            $data['no_pengajuan'] = $row->no_pengajuan;
            $data['tanggal_pengajuan'] = DMYFormat($row->tanggal_pengajuan);
            $data['nama_pemohon'] = $row->nama_pemohon;
            $data['sub_total'] = $row->sub_total;
            $data['discount'] = $row->diskon_rp;
            $data['grand_total'] = $row->grand_total;
            $data['selisih_pembayaran'] = $row->selisih_pembayaran < 0 ? $row->selisih_pembayaran * -1 : $row->selisih_pembayaran;
            $data['user_created'] = $row->user_created;
            $data['print_version'] = 1;
            $data['print_user'] = $this->session->userdata('user_name');
        }

        $data['list_detail_pengajuan'] = $this->model->getDetailPengajuan($id);
        
        $html = $this->load->view('Trujukan_rumahsakit_pembayaran/print', $data, true);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream('Faktur Transaksi Pembayaran Informasi Medis.pdf', array("Attachment"=>0));

    }

    public function save_transaction()
    {
        if ($this->model->saveTransaction()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'transaksi pembayaran telah disimpan.');
            redirect('trujukan_rumahsakit_pembayaran', 'location');
        }
    }

    public function getIndex($uri = 'index')
	{
		$this->select = [
			'tfeerujukan_rumahsakit.*', 'mrumahsakit.jenis_berekanan'
		];
		$this->from = 'tfeerujukan_rumahsakit';
		$this->join = [
			['mrumahsakit', 'mrumahsakit.id = tfeerujukan_rumahsakit.idrujukan', '']
		];

		$this->where = [];
		$this->where_in = ['status_approval' => [3, 5, 6]];

		if ($uri == 'filter') {
			if ($this->session->userdata('notransaksi') != null) {
				$this->where = array_merge($this->where, ['notransaksi' => $this->session->userdata('notransaksi')]);
			}
			if ($this->session->userdata('idrujukan') != 0) {
				$this->where = array_merge($this->where, ['idrujukan' => $this->session->userdata('idrujukan')]);
			}
			if ($this->session->userdata('tanggaljatuhtempo_dari') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal_jatuhtempo) >=' => YMDFormat($this->session->userdata('tanggaljatuhtempo_dari'))]);
			}
			if ($this->session->userdata('tanggaljatuhtempo_sampai') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal_jatuhtempo) <=' => YMDFormat($this->session->userdata('tanggaljatuhtempo_sampai'))]);
			}
			if ($this->session->userdata('status') != 0) {
				$this->where = array_merge($this->where, ['status' => $this->session->userdata('status')]);
			}
		}

		$this->order = [
			'id' => 'DESC'
		];

		$this->group = [];

		$this->column_search = ['notransaksi', 'namarujukan', 'tanggal_jatuhtempo', 'nominal'];
		$this->column_order = ['notransaksi', 'namarujukan', 'tanggal_jatuhtempo', 'nominal'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $row) {
			$no++;

			$result = [];

			$action = '<div class="btn-group">';
			$action .= '<a href="' . site_url() . 'trujukan_rumahsakit_rincian/detail/' . $row->id . '" target="_blank" title="Rincian Fee Rujukan" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>';
			$action .= '<a href="' . site_url() . 'trujukan_rumahsakit_rincian/print_transaction/' . $row->id . '" target="_blank" title="Print Rincian Fee Rujukan" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
			if ($row->status_approval == 3) {
				$action .= '<a href="' . site_url() . 'trujukan_rumahsakit_pembayaran/proses_transaction/' . $row->id . '" title="Proses Pembayaran Fee Rujukan" class="btn btn-warning btn-sm"><i class="fa fa-credit-card"></i></a>';
			}
			if ($row->status_approval == 5) {
                $action .= '<a href="' . site_url() . 'trujukan_rumahsakit_pembayaran/approve_transaction/' . $row->id . '" title="Verifikasi Pembayaran Fee Rujukan" class="btn btn-danger btn-sm"><i class="fa fa-check"></i></a>';
			}
			$action .= '</div>';

			$result[] = $no;
			$result[] = $row->notransaksi;
			$result[] = $row->namarujukan;
			$result[] = DMYFormat($row->tanggal_jatuhtempo);
			$result[] = number_format($row->nominal);
			$result[] = status_feerujukan($row->status_approval);
			$result[] = $action;

			$data[] = $result;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}
}
