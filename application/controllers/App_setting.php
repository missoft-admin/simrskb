<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_setting extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('App_setting_model');
		$this->load->helper('path');
		
  }

	function login(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1444'))){
			$data = $this->App_setting_model->get_login_setting();
			$data['error'] 			= '';
			$data['title'] 			= 'Apps Setting Login';
			$data['content'] 		= 'App_setting/login';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Apps Setting",'#'),
												  array("Setting",'app_setting')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function save_login(){
		if ($this->App_setting_model->save_login()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('app_setting/login','location');
		}
		
	}
	
	
	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1445'))){
			$data = $this->App_setting_model->get_index_setting();
			$data['error'] 			= '';
			$data['title'] 			= 'Apps Setting Index';
			$data['content'] 		= 'App_setting/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Apps Setting",'#'),
												  array("Index Setting",'app_setting')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function save_index(){
		if ($this->App_setting_model->save_index()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('app_setting/index','location');
		}
		
	}
	public function upload_files()
    {
        $files = array();
        foreach ($_FILES as $file) {
            if ($file['name'] != '') {
                $config['upload_path'] = './assets/upload/app_setting/';
                $config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';

                $files[] = $file['name'];
				$new_name = time().'-'.$file['name'];
				$config['file_name'] = $new_name;
                $this->upload->initialize($config);

                if ($this->upload->do_upload('file')) {
                    $image_upload = $this->upload->data();
                    $table = 'app_setting_file';

                    $data = array();
                    // $data['idtransaksi']  = $this->input->post('idtransaksi');
                    // $data['keterangan']  = $this->input->post('keterangan');
                    $data['upload_by']  = $this->session->userdata('user_id');
                    $data['upload_by_nama']  = $this->session->userdata('user_name');
                    $data['upload_date']  = date('Y-m-d H:i:s');
                    $data['filename'] = $image_upload['file_name'];
                    $data['size'] = formatSizeUnits($file['size']);
					
                    $this->db->insert($table, $data);

                    return true;
                } else {
                    print_r($this->upload->display_errors());
                    exit();

                    return false;
                }
            } else {
                return true;
            }
        }
    }
	function refresh_image(){		
		$arr['detail'] = $this->App_setting_model->refresh_image();
		$this->output->set_output(json_encode($arr));
	}
	function removeFile(){
		$id=$this->input->post('id');
		$this->db->where('id', $id);
        $query = $this->db->get('app_setting_file');
        $row = $query->row();
		if(file_exists('./assets/upload/app_setting/'.$row->filename) && $row->filename !='') {
			unlink('./assets/upload/app_setting/'.$row->filename);
		}else{
			
		}
		$result=$this->db->query("delete from app_setting_file WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	function info($tab='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('1446'))){
				$tab='1';
			}elseif (UserAccesForm($user_acces_form,array('1453'))){
				$tab='2';
			}
		}
		if (UserAccesForm($user_acces_form,array('1446','1453'))){
			$data = array();
			// print_r($data);exit;
			$data['tab'] 			= $tab;
			$data['created_date1_info'] 			= '';
			$data['created_date2_info'] 			= '';
			$data['date_start1_info'] 			= '';
			$data['date_start2_info'] 			= '';
			$data['created_date1_event'] 			= '';
			$data['created_date2_event'] 			= '';
			$data['date_start1_event'] 			= '';
			$data['date_start2_event'] 			= '';
			$data['error'] 			= '';
			$data['title'] 			= 'Apps Setting Informasi';
			$data['content'] 		= 'App_setting/info';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Apps Setting",'#'),
												  array("Informasi Setting",'app_setting/info')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	//INFORMASI
	function add_info(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1446'))){
			$data = array();
			$data['id'] 			= '';
			$data['isi'] 			= '';
			$data['status'] 			= '0';
			$data['date_start'] 			= date('d-m-Y');
			$data['date_end'] 			= date('d-m-Y');
			$data['gambar'] 			= '';
			$data['judul'] 			= '';
			$data['error'] 			= '';
			$data['title'] 			= 'Apps Setting Informasi';
			$data['content'] 		= 'App_setting/add_info';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Apps Setting",'#'),
												  array("Informasi Setting",'app_setting/info')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function update_info($id){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1448'))){
			$data = $this->App_setting_model->get_informasi($id);
			
			$data['error'] 			= '';
			$data['title'] 			= 'Apps Setting Informasi Edit';
			$data['content'] 		= 'App_setting/add_info';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Apps Setting",'#'),
												  array("Informasi Setting",'app_setting/info')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function save_info(){
		if($this->input->post('id') == '' ) {
			$id=$this->App_setting_model->saveDataInfo();
			if($id){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				// redirect('app_setting/update_info/'.$id,'location');
				redirect('app_setting/info/1','location');
			}
		} else {
			if($this->App_setting_model->updateDataInfo()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('app_setting/info/1','location');
			}
		}
		
		
	}
	
	function getIndex_info()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$created_date_1 = $this->input->post('created_date_1');
			$created_date_2 = $this->input->post('created_date_2');
			$date_start1 = $this->input->post('date_start1');
			$date_start2 = $this->input->post('date_start2');
			$status = $this->input->post('status');
			
			if ($status !='#'){
				$where .=" AND (H.status) ='".($status)."'";
			}
			if ($created_date_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($created_date_1)."' AND DATE(H.created_date) <='".ormat($created_date_2)."'";
			}
			if ($date_start1 !=''){
				$where .=" AND DATE(H.date_start) >='".YMDFormat($date_start1)."' AND DATE(H.date_start) <='".YMDFormat($date_start2)."'";
			}
			
			$this->select = array();
			$from="
					(
						SELECT H.*,U.`name` as user_publish FROM `app_informasi` H
						LEFT JOIN  musers U ON U.id=H.published_by

						WHERE H.id IS NOT NULL ".$where."
					) as tbl WHERE id IS NOT NULL 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
          $result[] = HumanDateShort_exp($r->created_date);
          $result[] = HumanDateShort_exp($r->date_start);
          $result[] = $r->judul;
          $result[] = StatusInfo($r->status);
          $result[] = HumanDateShort_exp($r->date_end);
          $result[] =($r->published_by?$r->user_publish.'<br>'.HumanDateLong($r->published_date):'');
          $aksi = '<div class="btn-group">';
		  if ($r->status!='0'){
			  
				if (UserAccesForm($user_acces_form,array('1449'))){
				if ($r->status=='1'){
					$aksi .= '<button onclick="publish_info('.$r->id.')" type="button" data-toggle="tooltip" title="Publish" class="btn btn-default btn-xs"><i class="fa fa-send"></i></button>';
					
				}
				}
				if (UserAccesForm($user_acces_form,array('1448'))){
				$aksi .= '<a href="'.site_url().'app_setting/update_info/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="si si-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1451'))){
				if ($r->status=='3'){
					$aksi .= '<button onclick="modal_aktifasi_info('.$r->id.')"  type="button" title="Aktifkan" class="btn btn-warning btn-xs aktifData"><i class="fa fa-refresh"></i> </button>';
					
				}
				}
				if (UserAccesForm($user_acces_form,array('1450'))){
				if ($r->status =='2'){
					$aksi .= '<button onclick="unPublish_info('.$r->id.')"  type="button" title="Non Aktifkan" class="btn btn-danger btn-xs "><i class="fa fa-times"></i></button>';				
				}
				}
				if (UserAccesForm($user_acces_form,array('1452'))){
				if ($r->status=='1'){
					$aksi .= '<button disabled data-toggle="tooltip" title="Print" class="btn btn-success btn-xs"><i class="fa fa-print"></i></button>';
				}
				}
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function publish_info(){
	  $id=$this->input->post('id');
	  $this->published_by = $this->session->userdata('user_id');
	  $this->published_date = date('Y-m-d H:i:s');
	  $this->status=2;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('app_informasi',$this);
	  
	  json_encode($hasil);
	  
  }
  function aktifkan_info(){
	  $id=$this->input->post('id');
	  // $date_end=$this->input->post('date_end');
	  $this->published_by = $this->session->userdata('user_id');
	  $this->published_date = date('Y-m-d H:i:s');
	  $this->date_end = YMDFormat($this->input->post('date_end'));
	  $this->status=2;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('app_informasi',$this);
	  
	  json_encode($hasil);
	  
  }
  function unPublish_info(){
	  $id=$this->input->post('id');
	  // $this->published_by = $this->session->userdata('user_id');
	  // $this->published_date = date('Y-m-d H:i:s');
	  $this->status=3;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('app_informasi',$this);
	  
	  json_encode($hasil);
	  
  }
  
  //EVENT
  function add_event(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1454'))){
			$data = array();
			$data['id'] 			= '';
			$data['isi'] 			= '';
			$data['status'] 			= '0';
			$data['date_start'] 			= date('d-m-Y');
			$data['date_end'] 			= date('d-m-Y');
			$data['gambar'] 			= '';
			$data['judul'] 			= '';
			$data['error'] 			= '';
			$data['title'] 			= 'Apps Setting Event';
			$data['content'] 		= 'App_setting/add_event';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Apps Setting",'#'),
												  array("Event Setting",'app_setting/info')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function update_event($id){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1455'))){
			$data = $this->App_setting_model->get_event($id);
			
			$data['error'] 			= '';
			$data['title'] 			= 'Apps Setting Event Edit';
			$data['content'] 		= 'App_setting/add_event';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Apps Setting",'#'),
												  array("Event Setting",'app_setting/info/2')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function save_event(){
		if($this->input->post('id') == '' ) {
			$id=$this->App_setting_model->saveDataEvent();
			if($id){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				// redirect('app_setting/update_event/'.$id,'location');
				redirect('app_setting/info/2','location');
			}
		} else {
			if($this->App_setting_model->updateDataEvent()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('app_setting/info/2','location');
			}
		}
		
		
	}
	function getIndex_event()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$created_date_1 = $this->input->post('created_date_1');
			$created_date_2 = $this->input->post('created_date_2');
			$date_start1 = $this->input->post('date_start1');
			$date_start2 = $this->input->post('date_start2');
			$status = $this->input->post('status');
			
			if ($status !='#'){
				$where .=" AND (H.status) ='".($status)."'";
			}
			if ($created_date_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($created_date_1)."' AND DATE(H.created_date) <='".YMDFormat($created_date_2)."'";
			}
			if ($date_start1 !=''){
				$where .=" AND DATE(H.date_start) >='".YMDFormat($date_start1)."' AND DATE(H.date_start) <='".YMDFormat($date_start2)."'";
			}
			
			$this->select = array();
			$from="
					(
						SELECT H.*,U.`name` as user_publish FROM `app_event` H
						LEFT JOIN  musers U ON U.id=H.published_by

						WHERE H.id IS NOT NULL ".$where."
					) as tbl WHERE id IS NOT NULL 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
          $result[] = HumanDateShort_exp($r->created_date);
          $result[] = HumanDateShort_exp($r->date_start);
          $result[] = $r->judul;
          $result[] = StatusInfo($r->status);
          $result[] = HumanDateShort_exp($r->date_end);
          $result[] =($r->published_by?$r->user_publish.'<br>'.HumanDateLong($r->published_date):'');
          $aksi = '<div class="btn-group">';
		  if ($r->status!='0'){
			  
				if (UserAccesForm($user_acces_form,array('1456'))){
				if ($r->status=='1'){
					$aksi .= '<button onclick="publish_event('.$r->id.')"  type="button" data-toggle="tooltip" title="Publish" class="btn btn-default btn-xs"><i class="fa fa-send"></i></button>';
					
				}
				}
				if (UserAccesForm($user_acces_form,array('1455'))){
				$aksi .= '<a href="'.site_url().'app_setting/update_event/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="si si-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1458'))){
				if ($r->status=='3'){
					$aksi .= '<button onclick="modal_aktifasi_event('.$r->id.')"  type="button" title="Aktifkan" class="btn btn-warning btn-xs aktifData"><i class="fa fa-refresh"></i> </button>';
					
				}
				}
				if (UserAccesForm($user_acces_form,array('1457'))){
				if ($r->status =='2'){
					$aksi .= '<button onclick="unPublish_event('.$r->id.')"  type="button" title="Non Aktifkan" class="btn btn-danger btn-xs "><i class="fa fa-times"></i></button>';				
				}
				}
				if (UserAccesForm($user_acces_form,array('1459'))){
				if ($r->status=='1'){
					$aksi .= '<button disabled data-toggle="tooltip" title="Print" class="btn btn-success btn-xs"><i class="fa fa-print"></i></button>';
				}
				}
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function publish_event(){
	  $id=$this->input->post('id');
	  $this->published_by = $this->session->userdata('user_id');
	  $this->published_date = date('Y-m-d H:i:s');
	  $this->status=2;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('app_event',$this);
	  
	  json_encode($hasil);
	  
  }
  function aktifkan_event(){
	  $id=$this->input->post('id');
	  // $date_end=$this->input->post('date_end');
	  $this->published_by = $this->session->userdata('user_id');
	  $this->published_date = date('Y-m-d H:i:s');
	  $this->date_end = YMDFormat($this->input->post('date_end'));
	  $this->status=2;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('app_event',$this);
	  
	  json_encode($hasil);
	  
  }
  function unPublish_event(){
	  $id=$this->input->post('id');
	  // $this->published_by = $this->session->userdata('user_id');
	  // $this->published_date = date('Y-m-d H:i:s');
	  $this->status=3;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('app_event',$this);
	  
	  json_encode($hasil);
	  
  }
  
  //ABOUT US
  function about_us(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1460'))){
			$data = $this->App_setting_model->get_about_setting();
			$data['error'] 			= '';
			$data['title'] 			= 'Apps Setting About Us';
			$data['content'] 		= 'App_setting/about';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Apps Setting",'#'),
												  array("Setting",'app_setting/about_us')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function save_about(){
		if ($this->App_setting_model->save_about()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('app_setting/about_us','location');
		}
		
	}
	
	function pendaftaran($tab='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('1469'))){
				$tab='1';
			}elseif (UserAccesForm($user_acces_form,array('1470'))){
				$tab='2';
			}
		}
		if (UserAccesForm($user_acces_form,array('1469','1470'))){
			$data = $this->App_setting_model->get_pendaftaran_setting();
			// print_r($data);exit;
			$data['tab'] 			= $tab;
			$data['idtipe'] 			= '#';
			$data['idpoli'] 			= '#';
			$data['idtipe_2'] 			= '#';
			$data['idpoli_2'] 			= '0';
			$data['jenis_pertemuan_id'] 			= '#';
			$data['iddokter'] 			= '0';
			$data['list_dokter'] 			= $this->App_setting_model->list_dokter();
			$data['st_gc'] 			= '1';
			$data['st_sp'] 			= '1';
			$data['st_sc'] 			= '1';
			$data['error'] 			= '';
			$data['title'] 			= 'Apps Setting Pendaftaran';
			$data['content'] 		= 'App_setting/pendaftaran';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Apps Setting",'#'),
												  array("Pendaftaran Setting",'app_setting/pendaftaran')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function pendaftaran_rm($tab='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('1485'))){
				$tab='1';
			}elseif (UserAccesForm($user_acces_form,array('1486'))){
				$tab='2';
			}
		}
		if (UserAccesForm($user_acces_form,array('1485','1486'))){
			$data = $this->App_setting_model->get_pendaftaran_rm_setting();
			// print_r($data);exit;
			$data['tab'] 			= $tab;
			$data['idtipe'] 			= '#';
			$data['idpoli'] 			= '#';
			$data['idtipe_2'] 			= '#';
			$data['idpoli_2'] 			= '0';
			$data['jenis_pertemuan_id'] 			= '#';
			$data['iddokter'] 			= '0';
			$data['list_dokter'] 			= $this->App_setting_model->list_dokter();
			$data['st_gc'] 			= '1';
			$data['st_sp'] 			= '1';
			$data['st_sc'] 			= '1';
			$data['error'] 			= '';
			$data['title'] 			= 'Apps Setting Pendaftaran Rehabilitas Medis';
			$data['content'] 		= 'App_setting/pendaftaran_rm';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Apps Setting",'#'),
												  array("Pendaftaran Setting Rehabitas Medis",'app_setting/pendaftaran_rm')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function save_pendaftaran(){
		if ($this->App_setting_model->save_pendaftaran()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('app_setting/pendaftaran/1','location');
		}
	}
	function save_pendaftaran_rm(){
		if ($this->App_setting_model->save_pendaftaran_rm()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('app_setting/pendaftaran_rm/1','location');
		}
	}
	function simpan_poli(){
		$this->idpoli=$this->input->post('idpoli');
		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');
		
		  $hasil=$this->db->insert('app_pendaftaran_poli',$this);
		  
		  json_encode($hasil);
	}
	function simpan_setting(){
		$idpoli=$this->input->post('idpoli');
		$iduser=$this->input->post('iduser');
		$iddokter=$this->input->post('iddokter');
		$tujuan_antrian_id=$this->input->post('tujuan_antrian_id');
		// print_r($iduser);exit;
		$this->db->where('idpoli',$idpoli);
		$hasil=$this->db->update('app_reservasi_poli',array('iddokter'=>$iddokter,'tujuan_antrian_id'=>$tujuan_antrian_id));
		
		$this->db->where('idpoli',$idpoli);
		$this->db->delete('app_reservasi_poli_user');
		if ($iduser){
			foreach($iduser as $index=>$val){
				$data=array(
					'idpoli' => $idpoli,
					'iduser' => $val,
				);
				// print_r($data);exit;
				$this->db->insert('app_reservasi_poli_user',$data);
			}
		}
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_poli_rm(){
		$this->idpoli=$this->input->post('idpoli');
		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');
		
		$hasil=$this->db->insert('app_pendaftaran_rm_poli',$this);
		  
		  json_encode($hasil);
	}
	
	
	function list_poli(){
		$idtipe=$this->input->post('idtipe');
		$q="SELECT H.id,H.nama FROM `mpoliklinik` H
			LEFT JOIN app_pendaftaran_poli T ON T.idpoli=H.id 
			WHERE H.idtipe='$idtipe' AND T.idpoli IS NULL AND H.`status`='1'";
			
		$hasil=$this->db->query($q)->result();
		$tabel='<option value="#" selected>- Pilih Poliklinik -</option>';
		foreach($hasil as $row){
			$tabel .='<option value="'.$row->id.'" >'.$row->nama.'</option>';
		}
		$this->output->set_output(json_encode($tabel));
	}
	function list_poli_rm(){
		$idtipe=$this->input->post('idtipe');
		$q="SELECT H.id,H.nama FROM `mpoliklinik` H
			LEFT JOIN app_pendaftaran_rm_poli T ON T.idpoli=H.id 
			WHERE H.idtipe='$idtipe' AND T.idpoli IS NULL AND H.`status`='1'";
			
		$hasil=$this->db->query($q)->result();
		$tabel='<option value="#" selected>- Pilih Poliklinik -</option>';
		foreach($hasil as $row){
			$tabel .='<option value="'.$row->id.'" >'.$row->nama.'</option>';
		}
		$this->output->set_output(json_encode($tabel));
	}
	// function list_poli_rm(){
		// $idtipe=$this->input->post('idtipe');
		// $q="SELECT H.id,H.nama FROM `mpoliklinik` H
			// LEFT JOIN app_pendaftaran_rm_poli T ON T.idpoli=H.id 
			// WHERE H.idtipe='$idtipe' AND T.idpoli IS NULL AND H.`status`='1'";
			
		// $hasil=$this->db->query($q)->result();
		// $tabel='<option value="#" selected>- Pilih Poliklinik -</option>';
		// foreach($hasil as $row){
			// $tabel .='<option value="'.$row->id.'" >'.$row->nama.'</option>';
		// }
		// $this->output->set_output(json_encode($tabel));
	// }
	
	function load_poliklinik()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.idtipe,M.nama
						FROM app_pendaftaran_poli H
						LEFT JOIN mpoliklinik M ON M.id=H.idpoli
						ORDER BY M.idtipe,M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = GetAsalRujukan($r->idtipe);
          $result[] = $r->nama;
		  if (UserAccesForm($user_acces_form,array('1472'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_poli('.$r->id.')" type="button" title="Non Aktifkan" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_poliklinik_rm()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.idtipe,M.nama
						FROM app_pendaftaran_rm_poli H
						LEFT JOIN mpoliklinik M ON M.id=H.idpoli
						ORDER BY M.idtipe,M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = GetAsalRujukan($r->idtipe);
          $result[] = $r->nama;
		  if (UserAccesForm($user_acces_form,array('1490'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_poli('.$r->id.')" type="button" title="Non Aktifkan" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
 
  function hapus_poli(){
	  $id=$this->input->post('id');
	  $this->db->where('idpoli',$id);
	  $hasil=$this->db->delete('app_pendaftaran_poli',$this);
	  
	  json_encode($hasil);
	  
  }
  function hapus_poli_rm(){
	  $id=$this->input->post('id');
	  $this->db->where('idpoli',$id);
	  $hasil=$this->db->delete('app_pendaftaran_rm_poli',$this);
	  
	  json_encode($hasil);
	  
  }
function list_kp(){
	$q="SELECT H.id,H.nama FROM `mpasien_kelompok` H
		LEFT JOIN app_pendaftaran_bayar T ON T.idkelompokpasien=H.id 
		WHERE T.idkelompokpasien IS NULL AND H.`status`='1'";
		
	$hasil=$this->db->query($q)->result();
	$tabel='<option value="#" selected>- Pilih Kelompok Pasien -</option>';
	foreach($hasil as $row){
		$tabel .='<option value="'.$row->id.'" >'.$row->nama.'</option>';
	}
	$this->output->set_output(json_encode($tabel));
}
	function list_kp_rm(){
	$q="SELECT H.id,H.nama FROM `mpasien_kelompok` H
		LEFT JOIN app_pendaftaran_rm_bayar T ON T.idkelompokpasien=H.id 
		WHERE T.idkelompokpasien IS NULL AND H.`status`='1'";
		
	$hasil=$this->db->query($q)->result();
	$tabel='<option value="#" selected>- Pilih Kelompok Pasien -</option>';
	foreach($hasil as $row){
		$tabel .='<option value="'.$row->id.'" >'.$row->nama.'</option>';
	}
	$this->output->set_output(json_encode($tabel));
	}
	function load_kelompok_pasien()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama,GROUP_CONCAT(MR.nama) as rekanan
						FROM app_pendaftaran_bayar H
						LEFT JOIN mpasien_kelompok M ON M.id=H.idkelompokpasien
						LEFT JOIN app_pendaftaran_asuransi A ON H.idkelompokpasien=A.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=A.idrekanan
						GROUP BY H.idkelompokpasien
						ORDER BY M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama','rekanan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
         
          $result[] = '<strong>'.$r->nama.'</strong>'.($r->rekanan?'<br>( '.$r->rekanan.' )':'');
          $aksi = '<div class="btn-group">';
		  if ($r->id=='1'){
			  if (UserAccesForm($user_acces_form,array('1475'))){
				$aksi .= '<button onclick="load_rekanan('.$r->id.')" type="button" title="List Asuransi" class="btn btn-primary btn-xs "><i class="si si-settings"></i></button>';	
			  
			}
		  }
		  if (UserAccesForm($user_acces_form,array('1474'))){
		  $aksi .= '<button onclick="hapus_kelompok('.$r->id.')" type="button" title="Non Aktifkan" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		 $aksi .= '</div>';
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
    }
	function load_kelompok_pasien_rm()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama,GROUP_CONCAT(MR.nama) as rekanan
						FROM app_pendaftaran_rm_bayar H
						LEFT JOIN mpasien_kelompok M ON M.id=H.idkelompokpasien
						LEFT JOIN app_pendaftaran_rm_asuransi A ON H.idkelompokpasien=A.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=A.idrekanan
						GROUP BY H.idkelompokpasien
						ORDER BY M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama','rekanan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
         
          $result[] = '<strong>'.$r->nama.'</strong>'.($r->rekanan?'<br>( '.$r->rekanan.' )':'');
          $aksi = '<div class="btn-group">';
		  if ($r->id=='1'){
			  if (UserAccesForm($user_acces_form,array('1493'))){
				$aksi .= '<button onclick="load_rekanan('.$r->id.')" type="button" title="List Asuransi" class="btn btn-primary btn-xs "><i class="si si-settings"></i></button>';	
			  
			}
		  }
		  if (UserAccesForm($user_acces_form,array('1492'))){
		  $aksi .= '<button onclick="hapus_kelompok('.$r->id.')" type="button" title="Non Aktifkan" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		 $aksi .= '</div>';
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
    }
	function load_rekanan()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama,CASE WHEN H.idrekanan IS NOT NULL THEN '1' ELSE '0' END as pilih FROM `mrekanan` M
						LEFT JOIN app_pendaftaran_asuransi H ON M.id=H.idrekanan
						ORDER BY M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
         
          $result[] = $r->nama;
		  $aksi='';
		  if ($r->pilih){
			$aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save('.$r->id.','.$r->pilih.')" type="checkbox" checked><span></span>
					</label>';	
		  }else{
			  $aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save('.$r->id.','.$r->pilih.')" type="checkbox"><span></span>
					</label>';	
		  }
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
    }
	function load_rekanan_rm()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama,CASE WHEN H.idrekanan IS NOT NULL THEN '1' ELSE '0' END as pilih FROM `mrekanan` M
						LEFT JOIN app_pendaftaran_rm_asuransi H ON M.id=H.idrekanan
						ORDER BY M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
         
          $result[] = $r->nama;
		  $aksi='';
		  if ($r->pilih){
			$aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save('.$r->id.','.$r->pilih.')" type="checkbox" checked><span></span>
					</label>';	
		  }else{
			  $aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save('.$r->id.','.$r->pilih.')" type="checkbox"><span></span>
					</label>';	
		  }
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
    }
	function simpan_kp(){
		$this->idkelompokpasien=$this->input->post('idkelompokpasien');
		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');
		
		  $hasil=$this->db->insert('app_pendaftaran_bayar',$this);
		  
		  json_encode($hasil);
	}
	function simpan_kp_rm(){
		$this->idkelompokpasien=$this->input->post('idkelompokpasien');
		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');
		
		  $hasil=$this->db->insert('app_pendaftaran_rm_bayar',$this);
		  
		  json_encode($hasil);
	}
	
	function check_save(){
		$pilih=$this->input->post('pilih');
		$idrekanan=$this->input->post('id');
		if ($pilih=='1'){//Asalnya Terpilih
			$this->db->where('idrekanan',$idrekanan);
			$hasil=$this->db->delete('app_pendaftaran_asuransi');
		}else{
			$this->idkelompokpasien=1;
			$this->idrekanan=$this->input->post('id');
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
			
			  $hasil=$this->db->insert('app_pendaftaran_asuransi',$this);
		}
		
		  
		  json_encode($hasil);
	}
	function check_save_rm(){
		$pilih=$this->input->post('pilih');
		$idrekanan=$this->input->post('id');
		if ($pilih=='1'){//Asalnya Terpilih
			$this->db->where('idrekanan',$idrekanan);
			$hasil=$this->db->delete('app_pendaftaran_rm_asuransi');
		}else{
			$this->idkelompokpasien=1;
			$this->idrekanan=$this->input->post('id');
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
			
			  $hasil=$this->db->insert('app_pendaftaran_rm_asuransi',$this);
		}
		
		  
		  json_encode($hasil);
	}
	
	function hapus_kelompok(){
	  $id=$this->input->post('id');
	  $this->db->where('idkelompokpasien',$id);
	  $hasil=$this->db->delete('app_pendaftaran_bayar',$this);
	  
	  json_encode($hasil);
	  
  }
  function hapus_kelompok_rm(){
	  $id=$this->input->post('id');
	  $this->db->where('idkelompokpasien',$id);
	  $hasil=$this->db->delete('app_pendaftaran_rm_bayar',$this);
	  
	  json_encode($hasil);
	  
  }
  
  function simpan_logic(){
		
		$this->jenis_pertemuan_id=$this->input->post('jenis_pertemuan_id');
		$this->idtipe=$this->input->post('idtipe');
		$this->idpoliklinik=$this->input->post('idpoli');
		$this->iddokter=$this->input->post('iddokter');
		$this->st_gc=$this->input->post('st_gc');
		$this->st_sp=$this->input->post('st_sp');
		$this->st_sc=$this->input->post('st_sc');
		
		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');
		
		  $hasil=$this->db->insert('app_pendaftaran_logic',$this);
		  
		  json_encode($hasil);
	}
	function simpan_logic_rm(){
		
		$this->jenis_pertemuan_id=$this->input->post('jenis_pertemuan_id');
		$this->idtipe=$this->input->post('idtipe');
		$this->idpoliklinik=$this->input->post('idpoli');
		$this->iddokter=$this->input->post('iddokter');
		$this->st_gc=$this->input->post('st_gc');
		$this->st_sp=$this->input->post('st_sp');
		$this->st_sc=$this->input->post('st_sc');
		
		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');
		
		  $hasil=$this->db->insert('app_pendaftaran_rm_logic',$this);
		  
		  json_encode($hasil);
	}
	function list_poli_2(){
		$idtipe=$this->input->post('idtipe');
		$jenis_pertemuan_id=$this->input->post('jenis_pertemuan_id');
		$q="SELECT H.id,H.nama FROM `mpoliklinik` H
			LEFT JOIN app_pendaftaran_logic T ON T.idpoliklinik=H.id AND T.jenis_pertemuan_id='$jenis_pertemuan_id' 
			WHERE H.idtipe='$idtipe' AND T.idpoliklinik IS NULL AND H.`status`='1'";
			
		$hasil=$this->db->query($q)->result();
		$tabel='<option value="0" selected>- All Poliklinik -</option>';
		if ($jenis_pertemuan_id!='#'){
			foreach($hasil as $row){
				$tabel .='<option value="'.$row->id.'" >'.$row->nama.'</option>';
			}
		}
		
		$this->output->set_output(json_encode($tabel));
	}
	function list_poli_2_rm(){
		$idtipe=$this->input->post('idtipe');
		$jenis_pertemuan_id=$this->input->post('jenis_pertemuan_id');
		$q="SELECT H.id,H.nama FROM `mpoliklinik` H
			LEFT JOIN app_pendaftaran_rm_logic T ON T.idpoliklinik=H.id AND T.jenis_pertemuan_id='$jenis_pertemuan_id' 
			WHERE H.idtipe='$idtipe' AND T.idpoliklinik IS NULL AND H.`status`='1'";
			
		$hasil=$this->db->query($q)->result();
		$tabel='<option value="0" selected>- All Poliklinik -</option>';
		if ($jenis_pertemuan_id!='#'){
			foreach($hasil as $row){
				$tabel .='<option value="'.$row->id.'" >'.$row->nama.'</option>';
			}
		}
		
		$this->output->set_output(json_encode($tabel));
	}
	function load_logic()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT 
							H.*,MP.nama as poli,MD.nama as dokter 
							FROM app_pendaftaran_logic H
							LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
							LEFT JOIN mdokter MD ON MD.id=H.iddokter
							ORDER BY H.jenis_pertemuan_id,H.idtipe,H.idpoliklinik
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('dokter','poli');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = GetJenisKunjungan($r->jenis_pertemuan_id);
          $result[] = GetAsalRujukan($r->idtipe);
          $result[] = ($r->idpoliklinik=='0'?text_default('ALL POLIKLINIK'):$r->poli);
          $result[] = ($r->iddokter=='0'?text_default('ALL DOKTER'):$r->dokter);
          $result[] = ($r->st_gc?'YA':text_danger('TIDAK'));
          $result[] = ($r->st_sp?'YA':text_danger('TIDAK'));
          $result[] = ($r->st_sc?'YA':text_danger('TIDAK'));
		  if (UserAccesForm($user_acces_form,array('1478'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_logic('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_logic_rm()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT 
							H.*,MP.nama as poli,MD.nama as dokter 
							FROM app_pendaftaran_rm_logic H
							LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
							LEFT JOIN mdokter MD ON MD.id=H.iddokter
							ORDER BY H.jenis_pertemuan_id,H.idtipe,H.idpoliklinik
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('dokter','poli');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = GetJenisKunjungan($r->jenis_pertemuan_id);
          $result[] = GetAsalRujukan($r->idtipe);
          $result[] = ($r->idpoliklinik=='0'?text_default('ALL POLIKLINIK'):$r->poli);
          $result[] = ($r->iddokter=='0'?text_default('ALL DOKTER'):$r->dokter);
          $result[] = ($r->st_gc?'YA':text_danger('TIDAK'));
          $result[] = ($r->st_sp?'YA':text_danger('TIDAK'));
          $result[] = ($r->st_sc?'YA':text_danger('TIDAK'));
		  if (UserAccesForm($user_acces_form,array('1488'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_logic('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_logic(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('app_pendaftaran_logic',$this);
	  
	  json_encode($hasil);
	  
  }
  function hapus_logic_rm(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('app_pendaftaran_rm_logic',$this);
	  
	  json_encode($hasil);
	  
  }
  //SLot
  function tujuan(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1480','1479'))){
			$data = $this->App_setting_model->get_pendaftaran_setting();
			// print_r($data);exit;
			$data['idtipe'] 			= '#';
			$data['idpoli'] 			= '#';
			$data['idtipe_2'] 			= '#';
			$data['idpoli_2'] 			= '0';
			$data['jenis_pertemuan_id'] 			= '#';
			$data['iddokter'] 			= '0';
			$data['list_dokter'] 			= $this->App_setting_model->list_dokter();
			$data['st_gc'] 			= '1';
			$data['st_sp'] 			= '1';
			$data['st_sc'] 			= '1';
			$data['error'] 			= '';
			$data['title'] 			= 'Apps Setting Slot Reservasi';
			$data['content'] 		= 'App_setting/tujuan';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Tujuan Rehabilitas Medis",'#'),
												  array("Pengaturan Tujuan",'app_setting/tujuan')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function list_poli_tujuan(){
		$idtipe=$this->input->post('idtipe');
		$q="SELECT H.id,H.nama FROM `mpoliklinik` H
			LEFT JOIN app_reservasi_poli T ON T.idpoli=H.id 
			WHERE H.idtipe='$idtipe' AND T.idpoli IS NULL AND H.`status`='1'";
			
		$hasil=$this->db->query($q)->result();
		$tabel='<option value="#" selected>- Pilih Poliklinik -</option>';
		foreach($hasil as $row){
			$tabel .='<option value="'.$row->id.'" >'.$row->nama.'</option>';
		}
		$this->output->set_output(json_encode($tabel));
	}
	function simpan_poli_tujuan(){
		$this->idpoli=$this->input->post('idpoli');
		$this->created_by = $this->session->userdata('user_id');
		$this->created_date = date('Y-m-d H:i:s');
		
		  $hasil=$this->db->insert('app_reservasi_poli',$this);
		  
		  json_encode($hasil);
	}
	function load_poliklinik_tujuan()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.idtipe,M.nama,GROUP_CONCAT(MD.nama) as nama_dokter
							FROM app_reservasi_poli H
							LEFT JOIN mpoliklinik M ON M.id=H.idpoli
							LEFT JOIN app_reservasi_poli_dokter D ON D.idpoli=H.idpoli
							LEFT JOIN mdokter MD ON MD.id=D.iddokter
							GROUP BY H.idpoli
							ORDER BY M.idtipe,M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama','nama_dokter');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
		  $nama_dokter='';
		  if ($r->nama_dokter){
			  $nama_dokter='<br>( '.$r->nama_dokter.' )';
		  }else{
			  $nama_dokter='<br>'.text_danger('Belum ada dokter');
		  }
          $result[] = GetAsalRujukan($r->idtipe);
          $result[] = '<strong>'.$r->nama.'</strong>'.$nama_dokter;
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('1481'))){
			$aksi .= '<button class="btn btn-xs btn-primary" type="button" onclick="load_dokter(\''.$r->id.'\',\''.$r->nama.'\')"  title="Setting Dokter"><i class="si si-settings"></i> Dokter</button>';	
		  // $aksi .= '<button onclick="load_dokter('.$r->id.',\'.$r->nama.')" type="button" title="Non Aktifkan" class="btn btn-primary btn-xs "><i class="si si-settings"></i></button>';	
			// 'posting(\''.$r->klaim_id.'\',\''.$r->tanggal_pembayaran.'\')'
		  }
			$aksi .= '<button class="btn btn-xs btn-success" type="button" onclick="load_setting(\''.$r->id.'\',\''.$r->nama.'\')"  title="Setting Poli"><i class="si si-settings"></i> Setting</button>';	
		  if (UserAccesForm($user_acces_form,array('1482'))){
		  $aksi .= '<button onclick="hapus_poli_tujuan('.$r->id.')" type="button" title="Non Aktifkan" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  
  
  function hapus_poli_tujuan(){
	  $id=$this->input->post('id');
	  $this->db->where('idpoli',$id);
	  $hasil=$this->db->delete('app_reservasi_poli',$this);
	  
	  json_encode($hasil);
	  
  }
  function load_dokter()
	{
			$id=$this->input->post('id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama,CASE WHEN H.iddokter IS NOT NULL THEN '1' ELSE '0' END as pilih 
						FROM mpoliklinik_dokter MS 
						INNER JOIN mdokter M ON MS.iddokter=M.id
						LEFT JOIN app_reservasi_poli_dokter H ON H.iddokter=M.id
						WHERE M.`status`='1' AND MS.idpoliklinik='$id' AND MS.`status`='1'
						ORDER BY M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
         
          $result[] = $r->nama;
		  $aksi='';
		  if ($r->pilih){
			$aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save_dokter('.$id.','.$r->id.','.$r->pilih.')" type="checkbox" checked><span></span>
					</label>';	
		  }else{
			  $aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save_dokter('.$id.','.$r->id.','.$r->pilih.')" type="checkbox"><span></span>
					</label>';	
		  }
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
    }
	function check_save_dokter(){
		$pilih=$this->input->post('pilih');
		$idpoli=$this->input->post('idpoli');
		$iddokter=$this->input->post('iddokter');
		if ($pilih=='1'){//Asalnya Terpilih
			$this->db->where('idpoli',$idpoli);
			$this->db->where('iddokter',$iddokter);
			$hasil=$this->db->delete('app_reservasi_poli_dokter');
		}else{
			$this->idpoli=$idpoli;
			$this->iddokter=$iddokter;
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
			
			  $hasil=$this->db->insert('app_reservasi_poli_dokter',$this);
		}
		
		  
		  json_encode($hasil);
	}
	//SLot
  function reservasi(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1480','1483'))){
			$data['error']='';
			$data['title'] 			= 'Apps Setting Jadwal Reservasi';
			$data['content'] 		= 'App_setting/reservasi';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Jadwal Reservasi",'#'),
												  array("Pengaturan Reservasi",'app_setting/reservasi')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function load_index_jadwal()
	{
			$q="SELECT D.idpoli,D.kodehari,D.jam_id,CONCAT(MJ.jam,'.00 ') jam ,CONCAT(MJ.jam_akhir,'.00 ') as jam_akhir,MJ.urutan FROM app_reservasi_poli_jadwal D
			LEFT JOIN merm_jam MJ ON MJ.jam_id=D.jam_id

			WHERE D.kuota IS NOT NULL
			ORDER BY D.idpoli,D.kodehari,MJ.urutan";
			$data_event=$this->db->query($q)->result_array();
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.idtipe,M.nama
							FROM app_reservasi_poli H
							LEFT JOIN mpoliklinik M ON M.id=H.idpoli
							GROUP BY H.idpoli
							ORDER BY M.idtipe,M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
		  $nama_dokter='';
		
          $result[] = '<strong>'.$r->nama.'</strong>';
          $result[] = $this->group_jam_jadwal($r->id,2,$data_event);
          $result[] = $this->group_jam_jadwal($r->id,3,$data_event);
          $result[] = $this->group_jam_jadwal($r->id,4,$data_event);
          $result[] = $this->group_jam_jadwal($r->id,5,$data_event);
          $result[] = $this->group_jam_jadwal($r->id,6,$data_event);
          $result[] = $this->group_jam_jadwal($r->id,7,$data_event);
          $result[] = $this->group_jam_jadwal($r->id,1,$data_event);
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('1484'))){
			$aksi .= '<a href="'.site_url().'app_setting/reservasi_jadwal_harian/'.$r->id.'" class="btn btn-xs btn-success" type="button" title="Slot Harian"><i class="fa fa-pencil"></i> </a>';	
		  }
		  if (UserAccesForm($user_acces_form,array('1483'))){
			$aksi .= '<a href="'.site_url().'app_setting/reservasi_poli_jadwal/'.$r->id.'" class="btn btn-xs btn-primary" type="button"  title="Setting Jadwal"><i class="si si-settings"></i> </a>';	
		  }
		  
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function group_jam_jadwal($idpoli,$kodehari,$data_jadwal){
		$akhir=0;
		$tulisan_group='';
		$tulisan='';
		$txt_jam_akhir='';
		
		$data_event = array_filter($data_jadwal, function($var) use ($idpoli,$kodehari) { 
			return ($var['idpoli'] == $idpoli && $var['kodehari'] == $kodehari);
		});
		$list_pasien='';
		// print_r();exit;
		if ($data_event){
			$draw_event = true;
			$list_pasien='<p class="nice-copy">
									';
			foreach($data_event as $r){
				if ($tulisan_group==''){
					$tulisan_group=$r['jam'];
					$txt_jam_akhir=$r['jam_akhir'];
				}else{
					if ($r['jam']!=$txt_jam_akhir){
						$tulisan_group .=' s/d '.$txt_jam_akhir;
						$tulisan_group=text_group($tulisan_group);
						$tulisan .=$tulisan_group;
						$tulisan_group=$r['jam'];
						$txt_jam_akhir=$r['jam_akhir'];
					}else{
						$txt_jam_akhir=$r['jam_akhir'];
					}
				}
			// $list_pasien .='<span class="label label-success"><i class="si si-user"></i> '.$row['no_medrec'].' - '.$row['namapasien'].' ( '.$row['kelompok_pasien'].($row['rekanan']?' | '.$row['rekanan']:'').' )'.'</span> ';
			}
				$list_pasien .='</p>';
			
		}else{
			
		}
		
		
		if ($tulisan_group!=''){
			
		$tulisan_group .=' s/d '.$txt_jam_akhir;
		$tulisan_group=text_group($tulisan_group);
		$tulisan .=$tulisan_group;
		}
		return $tulisan;
	}
  function group_jam($idpoli,$kodehari){
		$q="SELECT D.idpoli,D.kodehari,D.jam_id,CONCAT(MJ.jam,'.00 ') jam ,CONCAT(MJ.jam_akhir,'.00 ') as jam_akhir,MJ.urutan FROM app_reservasi_poli_jadwal D
			LEFT JOIN merm_jam MJ ON MJ.jam_id=D.jam_id

			WHERE D.kuota IS NOT NULL AND D.idpoli='$idpoli' AND D.kodehari='$kodehari'
			ORDER BY D.idpoli,D.kodehari,MJ.urutan";
			$hasil=$this->db->query($q)->result();
		$akhir=0;
		$tulisan_group='';
		$tulisan='';
		$txt_jam_akhir='';
		foreach($hasil as $r){
			if ($tulisan_group==''){
				$tulisan_group=$r->jam;
				$txt_jam_akhir=$r->jam_akhir;
			}else{
				if ($r->jam !=$txt_jam_akhir){
					$tulisan_group .=' s/d '.$txt_jam_akhir;
					$tulisan_group=text_group($tulisan_group);
					$tulisan .=$tulisan_group;
					$tulisan_group=$r->jam;
					$txt_jam_akhir=$r->jam_akhir;
				}else{
					$txt_jam_akhir=$r->jam_akhir;
				}
			}
			
			
		}
		if ($tulisan_group!=''){
			
		$tulisan_group .=' s/d '.$txt_jam_akhir;
		$tulisan_group=text_group($tulisan_group);
		$tulisan .=$tulisan_group;
		}
		return $tulisan;
	}
  function reservasi_poli_jadwal($idpoli){
	  $q="INSERT INTO app_reservasi_poli_jadwal (idpoli,kodehari,jam_id)
			SELECT M.* FROM (
			SELECT '$idpoli' as idpoli,merm_hari.kodehari,merm_jam.jam_id FROM merm_jam, merm_hari 
			) M 
			LEFT JOIN app_reservasi_poli_jadwal A ON A.idpoli=M.idpoli
			WHERE A.idpoli IS NULL 
			ORDER BY M.idpoli,M.kodehari,M.jam_id ASC
			";
	  $this->db->query($q);
	  $data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1483'))){
			$data = $this->App_setting_model->get_mpoli($idpoli);
			
			$data['error'] 			= '';
			$data['title'] 			= 'Pengaturan Slot';
			$data['content'] 		= 'App_setting/reservasi_jadwal';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Jadwal Reservasi",'#'),
												  array("Pengaturan Slot",'app_setting/reservasi')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	  // echo 'sini';
  }
  function load_template_jadwal_poli($idpoli){
	  $q="SELECT H.idpoli,H.jam_id,CONCAT(MJ.jam,'.00') as jam 
			,SUM(CASE WHEN H.kodehari='1' THEN H.kuota ELSE null END) minggu
			,SUM(CASE WHEN H.kodehari='1' THEN H.st_reservasi_online ELSE null END) st_minggu
			,SUM(CASE WHEN H.kodehari='2' THEN H.kuota ELSE null END) senin
			,SUM(CASE WHEN H.kodehari='2' THEN H.st_reservasi_online ELSE null END) st_senin
			,SUM(CASE WHEN H.kodehari='3' THEN H.kuota ELSE null END) selasa
			,SUM(CASE WHEN H.kodehari='3' THEN H.st_reservasi_online ELSE null END) st_selasa
			,SUM(CASE WHEN H.kodehari='4' THEN H.kuota ELSE null END) rabu
			,SUM(CASE WHEN H.kodehari='4' THEN H.st_reservasi_online ELSE null END) st_rabu
			,SUM(CASE WHEN H.kodehari='5' THEN H.kuota ELSE null END) kamis
			,SUM(CASE WHEN H.kodehari='5' THEN H.st_reservasi_online ELSE null END) st_kamis
			,SUM(CASE WHEN H.kodehari='6' THEN H.kuota ELSE null END) jumat
			,SUM(CASE WHEN H.kodehari='6' THEN H.st_reservasi_online ELSE null END) st_jumat
			,SUM(CASE WHEN H.kodehari='7' THEN H.kuota ELSE null END) sabtu
			,SUM(CASE WHEN H.kodehari='7' THEN H.st_reservasi_online ELSE null END) st_sabtu
			FROM `app_reservasi_poli_jadwal` H
			LEFT JOIN merm_hari MH ON MH.kodehari=H.kodehari
			LEFT JOIN merm_jam MJ ON MJ.jam_id=H.jam_id
			WHERE H.idpoli='$idpoli'
			GROUP BY H.idpoli,H.jam_id
			ORDER BY MJ.urutan ASC
			";
		$hasil=$this->db->query($q)->result();
		$tabel='';
		$no=0;
		foreach($hasil as $row){
			$no++;
			
			$tabel .='<tr>';
			$tabel .='<td class="text-center">'.$no.'</td>';
			$tabel .='<td class="text-center">'.$row->jam.'</td>';
			$tabel .='<td class="text-center">'.$this->generate_btn($row->idpoli,2,$row->jam_id,$row->senin,$row->st_senin).'</td>';
			$tabel .='<td class="text-center">'.$this->generate_btn($row->idpoli,3,$row->jam_id,$row->selasa,$row->st_selasa).'</td>';
			$tabel .='<td class="text-center">'.$this->generate_btn($row->idpoli,4,$row->jam_id,$row->rabu,$row->st_rabu).'</td>';
			$tabel .='<td class="text-center">'.$this->generate_btn($row->idpoli,5,$row->jam_id,$row->kamis,$row->st_kamis).'</td>';
			$tabel .='<td class="text-center">'.$this->generate_btn($row->idpoli,6,$row->jam_id,$row->jumat,$row->st_jumat).'</td>';
			$tabel .='<td class="text-center">'.$this->generate_btn($row->idpoli,7,$row->jam_id,$row->sabtu,$row->st_sabtu).'</td>';
			$tabel .='<td class="text-center">'.$this->generate_btn($row->idpoli,1,$row->jam_id,$row->minggu,$row->st_minggu).'</td>';
			$tabel .='</tr>';
		}
		$arr['tabel']=$tabel;
		$this->output->set_output(json_encode($arr));
  }
  
  function generate_btn($idpoli,$kodehari,$jam_id,$kuota,$st_reservasi_online){
	  if ($kuota==null){
		  $btn='
			<div class="text-primary pull-right">
				<button class="btn btn-xs btn-default" onclick="add_kuota('.$idpoli.','.$kodehari.','.$jam_id.')"  type="button" data-toggle="tooltip" title="Add Jadwal" data-original-title="Remove Jadwal"><i class="fa fa-plus text-primary"></i></button>
			</div>
			<div class="list-timeline-time text-danger">No Schedule</div>
			';
	  }else{
		  $btn='
			<div class="text-primary pull-right">
			<div class="btn-group">
				<button class="btn btn-xs btn-default" onclick="add_kuota('.$idpoli.','.$kodehari.','.$jam_id.')"  type="button" data-toggle="tooltip" title="Edit Jadwal" data-original-title="Edit Jadwal"><i class="fa fa-pencil text-success"></i></button>
				<button class="btn btn-xs btn-default" onclick="hapus_kuota('.$idpoli.','.$kodehari.','.$jam_id.')"  type="button" data-toggle="tooltip" title="Remove Jadwal" data-original-title="Remove Jadwal"><i class="fa fa-times text-danger"></i></button>
			</div>
			</div>
			<h5 class="text-primary">'.($kuota=='999'?'Tanpa Batas':$kuota.' Pasien').' <br>'.($st_reservasi_online=='1'?text_primary('ONLINE'):text_danger('OFFLINE')).'</h5>
			';
	  }
	  return $btn;
  }
  function hapus_kuota(){
	  $idpoli=$this->input->post('idpoli');
	  $kodehari=$this->input->post('kodehari');
	  $jam_id=$this->input->post('jam_id');
	  $this->db->where('idpoli',$idpoli);
	  $this->db->where('kodehari',$kodehari);
	  $this->db->where('jam_id',$jam_id);
	  $data_edit=array(
		'kuota' =>null,
	  );
	  $arr=$this->db->update('app_reservasi_poli_jadwal',$data_edit);
	  // print_r($arr);exit;
	  $this->output->set_output(json_encode($arr));
  }
  
  function save_kuota(){
	  $idpoli=$this->input->post('idpoli');
	  $kodehari=$this->input->post('kodehari');
	  $jam_id=$this->input->post('jam_id');
	  $kuota=$this->input->post('kuota');
	  $st_reservasi_online=$this->input->post('st_reservasi_online');
	  $this->db->where('idpoli',$idpoli);
	  $this->db->where('kodehari',$kodehari);
	  $this->db->where('jam_id',$jam_id);
	  $data_edit=array(
		'kuota' =>$kuota,
		'st_reservasi_online' =>$st_reservasi_online,
		'edited_name' =>$this->session->userdata('user_name'),
		'edited_date' =>date('Y-m-d H:i:s'),
	  );
	  // print_r($data_edit);exit;
	  $arr=$this->db->update('app_reservasi_poli_jadwal',$data_edit);
	  $this->output->set_output(json_encode($arr));
  }
  function save_catatan(){
	  $idpoli=$this->input->post('idpoli');
	  $kodehari=$this->input->post('kodehari');
	  $jam_id=$this->input->post('jam_id');
	  $catatan=$this->input->post('catatan');
	  $this->db->where('idpoli',$idpoli);
	  $this->db->where('kodehari',$kodehari);
	  $this->db->where('jam_id',$jam_id);
	  $data_edit=array(
		'catatan' =>$catatan,
		'posted_name' =>$this->session->userdata('user_name'),
		'posted_date' =>date('Y-m-d H:i:s'),
	  );
	  $arr=$this->db->update('app_reservasi_poli_jadwal',$data_edit);
	  // print_r($arr);exit;
	  $this->output->set_output(json_encode($arr));
  }
  function get_kuota(){
	  $idpoli=$this->input->post('idpoli');
	  $kodehari=$this->input->post('kodehari');
	  $jam_id=$this->input->post('jam_id');
	 $q="SELECT COALESCE(H.kuota,0) as kuota,H.catatan,MH.namahari,CONCAT(MJ.jam,'.00') as jam,H.st_reservasi_online 
			FROM app_reservasi_poli_jadwal H
			LEFT JOIN merm_hari MH ON MH.kodehari=H.kodehari
			LEFT JOIN merm_jam MJ ON MJ.jam_id=H.jam_id 
			WHERE H.idpoli='$idpoli' AND H.kodehari='$kodehari' AND H.jam_id='$jam_id'";
	  $arr=$this->db->query($q)->row_array();
	  // print_r($arr);exit;
	  $this->output->set_output(json_encode($arr));
  }
  function load_catatan()
	{
			$idpoli=$this->input->post('idpoli');
			  $kodehari=$this->input->post('kodehari');
			  $jam_id=$this->input->post('jam_id');
			$this->select = array();
			$from="
					(
						SELECT * FROM `app_reservasi_poli_jadwal_his` H
						WHERE H.idpoli='$idpoli' AND H.kodehari='$kodehari' AND H.jam_id='$jam_id'
						ORDER BY H.posted_date DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('catatan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
         
          $result[] = $r->catatan;
          $result[] = $r->posted_name.'<br>'.DMYTimeFormat($r->posted_date);
		 
          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
    }
	
	function reservasi_jadwal_harian($idpoli,$tab='1'){
	
	  // print_r($idpoli);exit;
	  $data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1484'))){
			$tahun=date('Y');
			$bulan=date('m');
			$q="SELECT D.tanggal,DATE_ADD(D.tanggal,INTERVAL 6 DAY) as tanggal_next   FROM date_row D

WHERE D.tanggal BETWEEN DATE_ADD(CURRENT_DATE(),INTERVAL -6 DAY) AND CURRENT_DATE() AND D.hari='2'";
			$tanggal=$this->db->query($q)->row('tanggal');
			
			$data['tanggal'] 			= $tanggal;
			$data['tanggaldari'] 			= DMYFormat(date('Y-m-01'));
			$data['tanggalsampai'] 			= DMYFormat(date("Y-m-t", strtotime($tanggal)));
			
			// $data['tanggal'] 			= date('2023-07-10');//1;month;2:week;3:day
			$data['list_poli'] = $this->App_setting_model->list_poli();
			$data['idpoli'] 			= $idpoli;//1;month;2:week;3:day
			$data['idpoli2'] 			= $idpoli;//1;month;2:week;3:day
			$data['present'] 			= '1';//1;month;2:week;3:day
			$data['tab'] 			= $tab;
			$data['present2'] 			= '1';
			$data['error'] 			= '';
			$data['title'] 			= 'Pengaturan Slot Harian';
			$data['content'] 		= 'App_setting/reservasi_jadwal_harian';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Jadwal Reservasi",'#'),
												  array("Pengaturan Slot Harian",'app_setting/reservasi_harian')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	  // echo 'sini';
  }
  
  function refresh_tanggal(){
	  $tanggal=$this->input->post('tanggal');
	  $present=$this->input->post('present');
	  $st_plus_minus=$this->input->post('st_plus_minus');
	  $date1=date_create($tanggal);
	  if ($st_plus_minus>0){
		  if ($present=='1'){
			   date_add($date1,date_interval_create_from_date_string("1 month"));
		  }
		  if ($present=='2'){
			   date_add($date1,date_interval_create_from_date_string("7 days"));
		  }
		  if ($present=='3'){
			   date_add($date1,date_interval_create_from_date_string("1 days"));
		  }
	  }else{
		  if ($present=='1'){
			   date_add($date1,date_interval_create_from_date_string("-1 month"));
		  }
		  if ($present=='2'){
			   date_add($date1,date_interval_create_from_date_string("-7 days"));
		  }
		  if ($present=='3'){
			   date_add($date1,date_interval_create_from_date_string("-1 days"));
		  }

	  }
	 
	  $date1= date_format($date1,"Y-m-d");
	  $arr['tanggal']=$date1;
	  $this->output->set_output(json_encode($arr));
  }
  function get_setting(){
	  $idpoli=$this->input->post('idpoli');
	  $q="SELECT iddokter,tujuan_antrian_id FROM app_reservasi_poli where idpoli='$idpoli'";
	  $iddokter=$this->db->query($q)->row('iddokter');
	  $tujuan_antrian_id=$this->db->query($q)->row('tujuan_antrian_id');
	  $data['iddokter']=$iddokter;
	  $data['tujuan_antrian_id']=$tujuan_antrian_id;
	  $q="SELECT H.id,H.`name` as nama,CASE WHEN U.iduser IS NOT NULL THEN 'selected' ELSE '' END as pilih FROM `musers` H
			LEFT JOIN app_reservasi_poli_user U ON U.iduser=H.id AND U.idpoli='$idpoli'
			WHERE H.`status`='1'";
	  $opsi='';
	  $list_user=$this->db->query($q)->result();
	  foreach($list_user as $r){
		  $opsi .='<option value="'.$r->id.'" '.$r->pilih.'>'.$r->nama.'</option>';
	  }
	  $data['list_user']=$opsi;
	  $this->output->set_output(json_encode($data));
  }
  function refresh_tanggal2(){
	  $tanggaldari=YMDFormat($this->input->post('tanggaldari'));
	  $tanggalsampai=YMDFormat($this->input->post('tanggalsampai'));
	  $present=$this->input->post('present');
	  $st_plus_minus=$this->input->post('st_plus_minus');
	  $date1=date_create($tanggaldari);
	  $date2=date_create($tanggalsampai);
	  // print_r($date2);exit;
	  if ($st_plus_minus==0){
		  if ($present=='1'){
			   $d=cal_days_in_month(CAL_GREGORIAN,MFormat($tanggaldari),YFormat($tanggaldari))-1;
			   date_add($date1,date_interval_create_from_date_string("0 month"));
			   
			   $date2=date_create(date_format($date1,"d-m-Y"));
			   date_add($date2,date_interval_create_from_date_string("$d days"));
		  }
		  if ($present=='2'){
			   date_add($date1,date_interval_create_from_date_string("0 days"));
			   $date2=date_create(date_format($date1,"d-m-Y"));
			   date_add($date2,date_interval_create_from_date_string("7 days"));
		  }
		  if ($present=='3'){
			   date_add($date1,date_interval_create_from_date_string("0 days"));
			   $date2=date_create(date_format($date1,"d-m-Y"));
			   date_add($date2,date_interval_create_from_date_string("0 days"));
		  }
		 
	  }elseif ($st_plus_minus>0){
		 if ($present=='1'){
			   $d=cal_days_in_month(CAL_GREGORIAN,MFormat($tanggaldari),YFormat($tanggaldari))-1;
			   date_add($date1,date_interval_create_from_date_string("1 month"));
			   
			   $date2=date_create(date_format($date1,"d-m-Y"));
			   date_add($date2,date_interval_create_from_date_string("$d days"));
		  }
		  if ($present=='2'){
			   date_add($date1,date_interval_create_from_date_string("7 days"));
			   $date2=date_create(date_format($date1,"d-m-Y"));
			   date_add($date2,date_interval_create_from_date_string("7 days"));
		  }
		  if ($present=='3'){
			   date_add($date1,date_interval_create_from_date_string("1 days"));
			   $date2=date_create(date_format($date1,"d-m-Y"));
			   date_add($date2,date_interval_create_from_date_string("0 days"));
		  }
		  
	  }else{
		 if ($present=='1'){
			   $d=cal_days_in_month(CAL_GREGORIAN,MFormat($tanggaldari),YFormat($tanggaldari))-1;
			   date_add($date1,date_interval_create_from_date_string("-1 month"));
			   
			   $date2=date_create(date_format($date1,"d-m-Y"));
			   date_add($date2,date_interval_create_from_date_string("$d days"));
		  }
		  if ($present=='2'){
			   date_add($date1,date_interval_create_from_date_string("-7 days"));
			   $date2=date_create(date_format($date1,"d-m-Y"));
			   date_add($date2,date_interval_create_from_date_string("7 days"));
		  }
		  if ($present=='3'){
			   date_add($date1,date_interval_create_from_date_string("-1 days"));
			   $date2=date_create(date_format($date1,"d-m-Y"));
			   date_add($date2,date_interval_create_from_date_string("0 days"));
		  } 
	  }
	  if ($present=='3'){
			$date2=date_create(date('Y-m-d'));
			$date1=date_create(date('Y-m-d'));
			// date_add($date1,date_interval_create_from_date_string("-1 days"));
		   
	  }else{
		  
	  }
	  $date1= date_format($date1,"d-m-Y");
	  $date2= date_format($date2,"d-m-Y");
	  $arr['tanggaldari']=$date1;
	  $arr['tanggalsampai']=$date2;
	  $this->output->set_output(json_encode($arr));
  }
  function refresh_tanggal_petugas(){
	  $tanggalpetugas=YMDFormat($this->input->post('tanggalpetugas'));
	  $st_plus_minus=$this->input->post('st_plus_minus');
	  $date1=date_create($tanggalpetugas);
	  // print_r($st_plus_minus);exit;
	  if ($st_plus_minus<0){
		  date_add($date1,date_interval_create_from_date_string("-1 days"));
		  
	  }elseif ($st_plus_minus>0){
		  date_add($date1,date_interval_create_from_date_string("1 days"));
		 
	  }
	 
	  $date1= date_format($date1,"d/m/Y");
	  
	  $arr['tanggalpetugas']=$date1;
	  $this->output->set_output(json_encode($arr));
  }
  
  function hasil_generate_jadwal(){
	  $idpoli=$this->input->post('idpoli');
	  $tanggal=$this->input->post('tanggal');
	  $present=$this->input->post('present');
	  $date1=date_create($tanggal);
	  $date2=date_create($tanggal);
	  date_add($date2,date_interval_create_from_date_string("7 days"));
	  $date2= date_format($date2,"Y-m-d");
	  // print_r($date1);exit;
	  $hari=date_format($date1,"d");
	  $tahun=date_format($date1,"Y");
	  $bulan=date_format($date1,"m");
	  if ($present=='1'){//Month
		$nama_periode=convert_month_year($bulan,$tahun);
	  }
	  if ($present=='2'){//Week
		$nama_periode=DMYFormat2($tanggal).' s/d '.DMYFormat2($date2);//'MINGGUAN';
	  }
	  if ($present=='3'){//dAYS
		$nama_periode=DMYFormat2($tanggal);//'MINGGUAN';
	  }
	  $q="SELECT H.tanggal,H.idpoli,H.kodehari,SUM(COALESCE(H.saldo_kuota,0)) as saldo_kuota 
			FROM app_reservasi_tanggal H 
			WHERE H.idpoli='$idpoli' AND YEAR(H.tanggal)='$tahun' AND MONTH(H.tanggal)='$bulan'

			GROUP BY H.tanggal,H.idpoli";
		$list_penjadwalan = $this->db->query($q)->result();
		$list_penjadwalan_formated = [];
		foreach ($list_penjadwalan as $value) {
			
			$list_penjadwalan_formated[($value->tanggal)] = [
				'tanggal' => YMDFormat($value->tanggal),
				'text' => $this->tex_kuota($value->saldo_kuota),
				'href' => site_url().'app_setting/reservasi_jadwal_harian_detail/'.$value->idpoli.'/'.$value->tanggal,
				'status' => $this->tex_status($value->saldo_kuota),
				'judul' => $this->judul_kuota($value->saldo_kuota),
			];
		}
		
                    
	if ($present=='1'){
		$arr['tabel']=build_html_calendar_detail($tahun, $bulan, $list_penjadwalan_formated);
	}
	if ($present=='2'){
		$q="SELECT *FROM date_row D WHERE D.tanggal >='$tanggal' AND D.tanggal <'$date2'
ORDER BY D.tanggal ASC";
		$list_tanggal=$this->db->query($q)->result();
		// print_r($list_tanggal);exit;
		$arr['tabel']=build_html_calendar_detail_week($tanggal,$list_tanggal, $list_penjadwalan_formated,'');
	
	}
	if ($present=='3'){
		$no_hari=date('N', strtotime($tanggal));
		// print_r($no_hari);exit;
	$arr['tabel']=build_html_calendar_detail_day($tanggal,'',$no_hari,$list_penjadwalan_formated);
	
	}
	$arr['nama_periode']=$nama_periode;
	$this->output->set_output(json_encode($arr));
}

function tex_status($saldo_kuota){
	if ($saldo_kuota<=0){
		return ('FULL');
	}else{
		return ('AVAILABLE');
	}
}
function tex_kuota($saldo_kuota){
	if ($saldo_kuota<=0){
		return ('FULL');
	}else{
		return ('SISA SLOT : '.$saldo_kuota);
	}
}
function judul_kuota($saldo_kuota){
	if ($saldo_kuota<=0){
		return 'FULL';
	}else{
		return ('SISA SLOT : '.$saldo_kuota);
	}
}
function reservasi_jadwal_harian_detail($idpoli,$tanggal,$jam_id=''){
	
	  // print_r($idpoli);exit;
	  $data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1484'))){
			
			$data['list_poli'] = $this->App_setting_model->list_poli();
			$data['idpoli'] 			= $idpoli;//1;month;2:week;3:day
			$data['tanggal'] 			= $tanggal;//1;month;2:week;3:day
			$data['present'] 			= '1';//1;month;2:week;3:day
			$data['error'] 			= '';
			$data['jam_id'] 			= $jam_id;
			$data['title'] 			= 'Pengaturan Slot Harian Detail';
			$data['content'] 		= 'App_setting/reservasi_jadwal_harian_detail';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Jadwal Reservasi",'#'),
												  array("Pengaturan Slot Harian",'app_setting/reservasi_harian')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	  // echo 'sini';
  }
  function load_reservasi_detail(){
	  $idpoli=$this->input->post('idpoli');
	  $tanggal=$this->input->post('tanggal');
	  $jam_id=$this->input->post('jam_id');
	  $where="";
	  $where2="";
	  if ($jam_id!=''){
		  $where=" AND H.jam_id='$jam_id'";
	  }
	  if ($jam_id!=''){
		  $where2=" AND D.jam_id='$jam_id'";
	  }
	  $nama_hari='';
	  $q="SELECT H.id,H.tanggal,H.idpoli,H.jam_id,H.kodehari,H.idpasien,H.namapasien,H.no_medrec,idkelompokpasien
			,MK.nama as kelompok_pasien,MR.nama as rekanan
			FROM `app_reservasi_tanggal_pasien` H
			LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
			WHERE H.tanggal='$tanggal' AND H.idpoli='$idpoli' ".$where;
		$pasien=$this->db->query($q)->result_array();
		// print_r($pasien);exit;
			
	  $q="SELECT D.tanggal,D.idpoli,D.kodehari,D.jam_id,CONCAT(MJ.jam,'.00 - ',MJ.jam_akhir,'.00') as jam,D.kuota,D.jumlah_reservasi,D.saldo_kuota,MH.namahari
				FROM app_reservasi_tanggal D
				LEFT JOIN merm_jam MJ ON MJ.jam_id=D.jam_id
				LEFT JOIN merm_hari MH ON MH.kodehari=D.kodehari
				WHERE D.idpoli='$idpoli' AND D.tanggal='$tanggal' ".$where2."

				ORDER BY MJ.urutan
			";
		$hasil=$this->db->query($q)->result();
		$tabel='';
		$no=0;
		foreach($hasil as $row){
			$no++;
			$nama_hari=$row->namahari;
			$tabel .='<tr>';
			$tabel .='<td class="text-center">'.$no.'</td>';
			$tabel .='<td class="text-center">'.$row->jam.$this->generate_ketersediaan($row->saldo_kuota).'</td>';
			$tabel .='<td class="text-left">'.$this->generate_btn_detail($row->idpoli,$row->tanggal,$row->jam_id,$row->saldo_kuota,$pasien).'</td>';
			
			$tabel .='</tr>';
		}
		$arr['tabel']=$tabel;
		if ($nama_hari==''){
			$arr['nama_periode']=text_danger('NO RECORD');
		}else{
			$arr['nama_periode']=$nama_hari.', '.DMYFormat2($tanggal);
		}
		$this->output->set_output(json_encode($arr));
  }
  function generate_ketersediaan($saldo_kuota){
	  if ($saldo_kuota!=null){
		   if ($saldo_kuota <1){
			  $hasil=' <br>'.text_danger('FULL');
		  }else{
			  $hasil=' <br>'.text_primary('TERSEDIA');
		  }
		  
	  }else{
		  $hasil='';
	  }
	  return $hasil;
  }
  function generate_ketersediaan_saldo($saldo_kuota){
	  if ($saldo_kuota!=null){
		   if ($saldo_kuota <1){
			  $hasil=' <br>'.text_danger('FULL');
		  }else{
			  $hasil=' <br>'.text_default('TERSEDIA '.$saldo_kuota.' SLOT');
		  }
		  
	  }else{
		  $hasil='';
	  }
	  return $hasil;
  }
  function generate_btn_detail($idpoli,$kodehari,$jam_id,$kuota,$pasien){
	  $btn_disabel='';
	  if ($kodehari < date('Y-m-d')){
		  $btn_disabel='disabled';
	  }
	  if ($kuota==null){
		  $btn='
			<div class="text-primary pull-right">
				<button class="btn btn-xs btn-default" '.$btn_disabel.' onclick="add_kuota('.$idpoli.','.$kodehari.','.$jam_id.')"  type="button" data-toggle="tooltip" title="Add Jadwal" data-original-title="Remove Jadwal"><i class="fa fa-plus text-primary"></i></button>
			</div>
			<div class="list-timeline-time text-danger">No Schedule</div>
			';
	  }else{
		  $hasil='';
		  // if ($kuota <1){
			  // $hasil=text_danger('FULL');
		  // }else{
			  // $hasil=text_primary('TERSEDIA '.$kuota.' Slot');
		  // }
		  $data_event = array_filter($pasien, function($var) use ($jam_id) { 
			return ($var['jam_id'] == $jam_id);
			});
			$list_pasien='';
			if ($data_event){
				$draw_event = true;
				$list_pasien='<p class="nice-copy">
                                        ';
				foreach($data_event as $row){
				$list_pasien .='<span class="label label-success"><i class="si si-user"></i> '.$row['no_medrec'].' - '.$row['namapasien'].' ( '.$row['kelompok_pasien'].($row['rekanan']?' | '.$row['rekanan']:'').' )'.'</span> ';
				}
					$list_pasien .='</p>';
				if ($kuota>0){
					$hasil=text_default('SISA '.$kuota.' SLOT');
					
				}
			}else{
				if ($kuota>0){
					$hasil=text_default('SISA '.$kuota.' SLOT');
					
				}
			}
		  $btn='
			<div class="text-primary pull-right">
				<div class="btn-group">
					<button class="btn btn-xs btn-default" '.$btn_disabel.' onclick="add_kuota('.$idpoli.','.$kodehari.','.$jam_id.')"  type="button" data-toggle="tooltip" title="Edit Jadwal" data-original-title="Edit Jadwal"><i class="fa fa-pencil text-success"></i></button>';
					if ($kuota>0){
					$btn.='<button class="btn btn-xs btn-default" '.$btn_disabel.' onclick="stop_kuota('.$idpoli.','.$kodehari.','.$jam_id.')"  type="button" data-toggle="tooltip" title="Remove Jadwal" data-original-title="Remove Jadwal"><i class="si si-ban text-danger"></i></button>';
						
					}
					
				$btn.='</div>
			</div>
			'.$list_pasien.$hasil.'
			';
	  }
	  return $btn;
  }
  function hasil_generate_ls(){
	  $idpoli=$this->input->post('idpoli');
	  $tanggaldari=YMDFormat($this->input->post('tanggaldari'));
	  $tanggalsampai=YMDFormat($this->input->post('tanggalsampai'));
	 
	  
	  $q="SELECT D.tanggal,D.idpoli,D.kodehari,D.jam_id,CONCAT(MJ.jam,'.00 - ',MJ.jam_akhir,'.00') as jam,D.kuota,D.jumlah_reservasi,D.saldo_kuota,MH.namahari
				FROM app_reservasi_tanggal D
				LEFT JOIN merm_jam MJ ON MJ.jam_id=D.jam_id
				LEFT JOIN merm_hari MH ON MH.kodehari=D.kodehari
				WHERE D.idpoli='$idpoli' AND (D.tanggal BETWEEN '$tanggaldari' AND '$tanggalsampai')

				ORDER BY MJ.urutan
			";
		$hasil_kuota=$this->db->query($q)->result_array();
		
		$q="SELECT H.id,H.tanggal,H.idpoli,H.jam_id,H.kodehari,H.idpasien,H.namapasien,H.no_medrec,idkelompokpasien
			,MK.nama as kelompok_pasien,MR.nama as rekanan
			FROM `app_reservasi_tanggal_pasien` H
			LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
			WHERE (H.tanggal BETWEEN '$tanggaldari' AND '$tanggalsampai') AND H.idpoli='$idpoli'";
	  $pasien=$this->db->query($q)->result_array();
	  $q="SELECT D.*,MH.namahari FROM date_row D LEFT JOIN merm_hari MH ON MH.kodehari=D.hari WHERE D.tanggal BETWEEN '$tanggaldari' AND '$tanggalsampai'";
	  $record=$this->db->query($q)->result();
	  
	  $tabel='<table class="table table table-bordered table-condensed" id="index_ls">';
	  $tabel .='<thead>
					<tr>';
			$tabel .='<td class="text-center">TIME</td>';
	  foreach($record as $r){
			$tabel .='<td class="text-center">'.$r->namahari.', '.DMYFormat2($r->tanggal).'</td>';
	  }
	  $tabel .='</tr></thead>';
	  $tabel .='<tbody>';
	  
	  $tabel .=$this->generate_detail_ls($hasil_kuota,$record,$pasien,$idpoli);
	  
	  $tabel .='</tbody>';
	  $tabel .='</tabel>';
	 
	 $arr['tabel']=$tabel;
	$this->output->set_output(json_encode($arr));
}
function generate_detail_ls($record_detail,$record,$pasien,$idpoli){
	// print_r($record_detail);exit;
	$q="SELECT MJ.jam_id,CONCAT(MJ.jam,'.00') as jam FROM merm_jam MJ ORDER BY MJ.urutan";
	$hasil=$this->db->query($q)->result();
	$tabel ='';
	foreach($hasil as $r){
		$jam_id=$r->jam_id;
	$tabel .='<tr>';
		$tabel .='<td>'.$r->jam.'</td>';
		foreach($record as $tgl){
			$tgl_harian=$tgl->tanggal;
			// reservasi_jadwal_harian_detail
			$btn=' <a href="'.base_url().'app_setting/reservasi_jadwal_harian_detail/'.$idpoli.'/'.$tgl_harian.'/'.$jam_id.'" target="_blank" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
			 $data_kuota = array_filter($record_detail, function($var) use ($jam_id,$tgl_harian) { 
			return ($var['jam_id'] == $jam_id && $var['tanggal']==$tgl_harian);
			});
			$list_kuota='';
			$list_pasien='';
			// print_r($data_kuota);exit;
			$data_kuota = reset($data_kuota);
			if ($data_kuota){
				$list_kuota='<p class="nice-copy">';
				if ($data_kuota['kuota']==null){
					$list_kuota='<div class="list-timeline-time text-danger">No Schedule</div>'.$btn;
				}else{
					// if ($data_kuota['kuota']>0){
						$list_kuota=$this->generate_ketersediaan_saldo($data_kuota['saldo_kuota']).$btn;
					// }
				}
				
				$list_kuota .='</p>';
			}else{
				$list_kuota='';
			}
			
			$data_event = array_filter($pasien, function($var) use ($jam_id,$tgl_harian) { 
			return ($var['jam_id'] == $jam_id && $var['tanggal']==$tgl_harian);
			});
			$list_pasien='';
			if ($data_event){
				$draw_event = true;
				$list_pasien='<p class="nice-copy">
                                        ';
				foreach($data_event as $row){
				$list_pasien .='<span class="label label-success"><i class="si si-user"></i> '.$row['no_medrec'].' - '.$row['namapasien'].' ( '.$row['kelompok_pasien'].($row['rekanan']?' | '.$row['rekanan']:'').' )'.'</span> ';
				}
					$list_pasien .='</p>';
				
			}
			$tabel .='<td>'.$list_pasien.$list_kuota.'</td>';
			
		}
	$tabel .='</tr>';
	}
	return $tabel;
}
  function refresh_tanggal_detail(){
	  $tanggal=$this->input->post('tanggal');
	  $st_plus_minus=$this->input->post('st_plus_minus');
	  $date1=date_create($tanggal);
	  if ($st_plus_minus>0){
		  date_add($date1,date_interval_create_from_date_string("1 days"));
	  }else{
		  date_add($date1,date_interval_create_from_date_string("-1 days"));
	  }
	 
	  $date1= date_format($date1,"Y-m-d");
	  $arr['tanggal']=$date1;
	  $this->output->set_output(json_encode($arr));
  }
  function stop_kuota(){
	  $idpoli=$this->input->post('idpoli');
	  $tanggal=$this->input->post('tanggal');
	  $jam_id=$this->input->post('jam_id');
	  $q="update app_reservasi_tanggal set app_reservasi_tanggal.kuota=app_reservasi_tanggal.jumlah_reservasi 
WHERE app_reservasi_tanggal.idpoli='$idpoli' AND app_reservasi_tanggal.tanggal='$tanggal' AND app_reservasi_tanggal.jam_id='$jam_id'";
	  // print_r($q);exit;
	  $arr=$this->db->query($q);
	  $this->output->set_output(json_encode($arr));
  }
   function get_kuota_detail(){
	  $idpoli=$this->input->post('idpoli');
	  $tanggal=$this->input->post('tanggal');
	  $jam_id=$this->input->post('jam_id');
	 $q="SELECT COALESCE(H.kuota,0) as kuota,H.catatan,CONCAT(MH.namahari,', ',DATE_FORMAT(H.tanggal,'%d-%m-%Y')) as namahari,CONCAT(MJ.jam,'.00') as jam,H.st_reservasi_online 
			FROM app_reservasi_tanggal H
			LEFT JOIN merm_hari MH ON MH.kodehari=H.kodehari
			LEFT JOIN merm_jam MJ ON MJ.jam_id=H.jam_id 
			WHERE H.idpoli='$idpoli' AND H.tanggal='$tanggal' AND H.jam_id='$jam_id'";
	  $arr=$this->db->query($q)->row_array();
	  // print_r($arr);exit;
	  $this->output->set_output(json_encode($arr));
  }
  function save_kuota_detail(){
	  $idpoli=$this->input->post('idpoli');
	  $tanggal=$this->input->post('tanggal');
	  $jam_id=$this->input->post('jam_id');
	  $kuota=$this->input->post('kuota');
	  $st_reservasi_online=$this->input->post('st_reservasi_online');
	  $this->db->where('idpoli',$idpoli);
	  $this->db->where('tanggal',$tanggal);
	  $this->db->where('jam_id',$jam_id);
	  $data_edit=array(
		'st_edited' =>1,
		'kuota' =>$kuota,
		'st_reservasi_online' =>$st_reservasi_online,
		'edited_name' =>$this->session->userdata('user_name'),
		'edited_date' =>date('Y-m-d H:i:s'),
	  );
	  // print_r($tanggal);exit;
	  $arr=$this->db->update('app_reservasi_tanggal',$data_edit);
	  $this->output->set_output(json_encode($arr));
  }
  function save_catatan_detail(){
	  $idpoli=$this->input->post('idpoli');
	  $tanggal=$this->input->post('tanggal');
	  $jam_id=$this->input->post('jam_id');
	  $catatan=$this->input->post('catatan');
	  $this->db->where('idpoli',$idpoli);
	  $this->db->where('tanggal',$tanggal);
	  $this->db->where('jam_id',$jam_id);
	  // print_r($idpoli);exit;
	  $data_edit=array(
		'st_edited' =>1,
		'catatan' =>$catatan,
		'edited_name' =>$this->session->userdata('user_name'),
		'edited_date' =>date('Y-m-d H:i:s'),
	  );
	  $arr=$this->db->update('app_reservasi_tanggal',$data_edit);
	  $this->output->set_output(json_encode($arr));
  }
}
