<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    /**
     * Dashboard controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->model('Tpencairan_model');
        $this->load->model('Widgets_laboratorium_model');
    }

    public function index()
    {
		$q="SELECT K.id,H.idunitpenerima,K.kuantitas,K.kuantitas_terima,K.kuantitas-K.kuantitas_terima as sisa,U.st_otomatis_terima 
		,U.jml_hari_terima,HOUR(TIMEDIFF(NOW(),K.tanggal_kirim))/24 as lama_hari,K.tanggal_kirim
		FROM `tunit_pengiriman` K
		INNER JOIN tunit_permintaan H ON H.id=K.idpermintaan
		INNER JOIN munitpelayanan U ON U.id=H.idunitpenerima
		WHERE K.kuantitas != K.kuantitas_terima AND U.st_otomatis_terima='1'";
		$row_otomatis=$this->db->query($q)->result();
		foreach($row_otomatis as $row){
			$st_update='0';
			if ($row->jml_hari_terima=='0'){
				$st_update='1';
			}else{
				if ($row->lama_hari >= $row->jml_hari_terima){
					$st_update='1';
				}
			}
			if ($st_update=='1'){				
				$data['tanggal_terima']=date('Y-m-d H:i:s');
				$data['kuantitas_terima']=$row->sisa;
				$data['user_terima']=0;
				$data['user_nama_terima']='System';			
				$this->db->where('id',$row->id);
				$this->db->update('tunit_pengiriman', $data);
			}
		}
		auto_insert_Setoran_RJ(date('Y-m-d'));
		// auto_insert_Setoran_RJ('2023-03-08');
		insertPoliklinikTindakan_All();
        $data = array();
        $data['tanggal_1']      = date('d-m-Y');
        $data['tanggal_2']      = date('d-m-Y');
        $data['title']      = 'Dashboard';
        $data['content']    = 'dashboard';
        $data['breadcrum']  = array(
            array("Dashboard",'#'),
            array("Index",'')
        );

				$tanggal_dari = date('Y-m-') . '01';
				$tanggal_sampai = date('Y-m-t');

				$data['widget_permintaan_laboratorium'] = array(
					'tanggal_dari' => DMYFormat($tanggal_dari),
					'tanggal_sampai' => DMYFormat($tanggal_sampai),
					'total' => $this->Widgets_laboratorium_model->getTotalPermintaanLaboratorium($tanggal_dari, $tanggal_sampai),
					'items' => $this->Widgets_laboratorium_model->getItemPermintaanLaboratorium($tanggal_dari, $tanggal_sampai),
				);

				$data['widget_permintaan_laboratorium_pa'] = array(
					'tanggal_dari' => DMYFormat($tanggal_dari),
					'tanggal_sampai' => DMYFormat($tanggal_sampai),
					'total' => $this->Widgets_laboratorium_model->getTotalPermintaanLaboratoriumPA($tanggal_dari, $tanggal_sampai),
					'items' => $this->Widgets_laboratorium_model->getItemPermintaanLaboratoriumPA($tanggal_dari, $tanggal_sampai),
				);

				$data['widget_permintaan_laboratorium_bd'] = array(
					'tanggal_dari' => DMYFormat($tanggal_dari),
					'tanggal_sampai' => DMYFormat($tanggal_sampai),
					'total' => $this->Widgets_laboratorium_model->getTotalPermintaanLaboratoriumBD($tanggal_dari, $tanggal_sampai),
					'items' => $this->Widgets_laboratorium_model->getItemPermintaanLaboratoriumBD($tanggal_dari, $tanggal_sampai),
				);

				$data['widget_permintaan_radiologi_xray'] = array(
					'tanggal_dari' => DMYFormat($tanggal_dari),
					'tanggal_sampai' => DMYFormat($tanggal_sampai),
					'total' => $this->Widgets_laboratorium_model->getTotalPermintaanRadiologiXray($tanggal_dari, $tanggal_sampai),
					'items' => $this->Widgets_laboratorium_model->getItemPermintaanRadiologiXray($tanggal_dari, $tanggal_sampai),
				);

				$data['widget_permintaan_radiologi_usg'] = array(
					'tanggal_dari' => DMYFormat($tanggal_dari),
					'tanggal_sampai' => DMYFormat($tanggal_sampai),
					'total' => $this->Widgets_laboratorium_model->getTotalPermintaanRadiologiUsg($tanggal_dari, $tanggal_sampai),
					'items' => $this->Widgets_laboratorium_model->getItemPermintaanRadiologiUsg($tanggal_dari, $tanggal_sampai),
				);

				$data['widget_permintaan_radiologi_ctscan'] = array(
					'tanggal_dari' => DMYFormat($tanggal_dari),
					'tanggal_sampai' => DMYFormat($tanggal_sampai),
					'total' => $this->Widgets_laboratorium_model->getTotalPermintaanRadiologiCtscan($tanggal_dari, $tanggal_sampai),
					'items' => $this->Widgets_laboratorium_model->getItemPermintaanRadiologiCtscan($tanggal_dari, $tanggal_sampai),
				);

				$data['widget_permintaan_radiologi_mri'] = array(
					'tanggal_dari' => DMYFormat($tanggal_dari),
					'tanggal_sampai' => DMYFormat($tanggal_sampai),
					'total' => $this->Widgets_laboratorium_model->getTotalPermintaanRadiologiMri($tanggal_dari, $tanggal_sampai),
					'items' => $this->Widgets_laboratorium_model->getItemPermintaanRadiologiMri($tanggal_dari, $tanggal_sampai),
				);

				$data['widget_permintaan_radiologi_lainnya'] = array(
					'tanggal_dari' => DMYFormat($tanggal_dari),
					'tanggal_sampai' => DMYFormat($tanggal_sampai),
					'total' => $this->Widgets_laboratorium_model->getTotalPermintaanRadiologiLainnya($tanggal_dari, $tanggal_sampai),
					'items' => $this->Widgets_laboratorium_model->getItemPermintaanRadiologiLainnya($tanggal_dari, $tanggal_sampai),
				);

        $data['jml_pencairan'] = $this->Tpencairan_model->get_jml_pencairan();
		$setting_deposit=$this->db->query("SELECT *FROM setting_deposit")->row_array();
        $data = array_merge($data,$setting_deposit, backend_info());
        $this->parser->parse('module_template', $data);
    }
	function load_waktu(){
		$waktu_deposit=($this->input->post('waktu_deposit'));
		$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d'));
		if ($waktu_deposit=='1'){
			date_add($date1,date_interval_create_from_date_string("-1 days"));
			date_add($date2,date_interval_create_from_date_string("-$waktu_deposit days"));
		}else{
			date_add($date1,date_interval_create_from_date_string("0 days"));
			date_add($date2,date_interval_create_from_date_string("-$waktu_deposit days"));
		}
		
		$date1= date_format($date1,"d-m-Y");
		$date2= date_format($date2,"d-m-Y");
		$hasil['tanggal_deposit_1']=$date2;
		$hasil['tanggal_deposit_2']=$date1;
		$this->output->set_output(json_encode($hasil));
	}
    public function php_info()
    {
        phpinfo();
    }

		function load_pembelian(){
				$tanggal_1=YMDFormat($this->input->post('tanggal_1'));
				$tanggal_2=YMDFormat($this->input->post('tanggal_2'));
				
				$q="SELECT M.id,M.nama_tipe,COALESCE(T.total_harga,0) as total  FROM mdata_tipebarang M
				LEFT JOIN(
					SELECT H.tgl_terima,D.idtipe,SUM(D.totalharga) as total_harga FROM tgudang_penerimaan H
					LEFT JOIN tgudang_penerimaan_detail D ON D.idpenerimaan=H.id
					WHERE H.tgl_terima BETWEEN '".($tanggal_1)."' AND '".($tanggal_2)."'
					GROUP BY D.idtipe
				) T ON T.idtipe=M.id";
				$hasil=$this->db->query($q)->result();
				$tabel='';
				$total=0;
				foreach($hasil as $r){
					
					$tabel .='<tr>';
					$tabel .='<td class="text-right">'.$r->id.'</td>';
					$tabel .='<td class="text-left">'.$r->nama_tipe.'</td>';
					$tabel .='<td class="text-right">'.number_format($r->total,2).'</td>';
					$tabel .='</tr>';
					$total=$total+$r->total;
				}
				$tabel .='<tr>';
				$tabel .='<td class="text-center" colspan="2"><strong>TOTAL</strong></td>';
				$tabel .='<td class="text-right text-bold"><strong>'.number_format($total,2).'</strong></td>';
				$tabel .='</tr>';
				$this->output->set_output(json_encode($tabel));
		}
		function load_mpp(){
				$tanggal_1=YMDFormat($this->input->post('tanggal_1'));
				$tanggal_2=YMDFormat($this->input->post('tanggal_2'));
				$jml_mpp=0;
				$where='';
				if ($tanggal_1!=''){
					$where .=" AND H.tanggal_input BETWEEN '".($tanggal_1)."' AND '".($tanggal_2)."'";
				}
				$q="SELECT 
					H.created_ppa,H.created_date,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,H.keterangan_edit,H.jml_edit,H.st_edited
					,CASE WHEN H.st_ranap='1' THEN MP.no_medrec ELSE MJ.no_medrec END no_medrec
					,CASE WHEN H.st_ranap='1' THEN MP.namapasien ELSE MJ.namapasien END namapasien
					,CASE WHEN H.st_ranap='1' THEN MP.nopendaftaran ELSE MJ.nopendaftaran END nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MP.tanggaldaftar ELSE MJ.tanggaldaftar END tanggaldaftar
					,CASE WHEN H.st_ranap='1' THEN MDP.nama ELSE MD.nama END nama_dokter
					,H.idtipe,H.kesimpulan_mpp_text,H.tindakan_nama
					,MR.nama as nama_ruangan,MK.nama as nama_kelas,MB.nama as nama_bed
					,H.tanggal_input as tanggal_pengkajian,H.assesmen_id,H.pendaftaran_id_ranap,H.st_ranap,H.pendaftaran_id,MPP.nama as nama_poli
					FROM `tranap_mpp` H
					LEFT JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id_ranap  AND H.st_ranap='1'
					LEFT JOIN tpoliklinik_pendaftaran MJ ON MJ.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN mpoliklinik MPP ON MPP.id=MJ.idpoliklinik
					LEFT JOIN mruangan MR ON MR.id=MP.idruangan
					LEFT JOIN mkelas MK ON MK.id=MP.idkelas
					LEFT JOIN mbed MB ON MB.id=MP.idbed
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					LEFT JOIN mdokter MD ON MD.id=MJ.iddokter
					LEFT JOIN mdokter MDP ON MDP.id=MP.iddokterpenanggungjawab
					WHERE H.status_assemen='2' ".$where;
				$hasil=$this->db->query($q)->result();
				$tabel='';
				$total=0;
				foreach($hasil as $r){
					$jml_mpp=$jml_mpp+1;
					$idtipe=$r->idtipe;
					if ($r->st_ranap=='1'){
						$idtipe=$r->idtipe+2;
					}
					$tabel .='<tr>';
					$tabel .='<td class="text-center">'.$r->nopendaftaran.'<br>'.HumanDateLong($r->tanggal_pengkajian).'</td>';
					$tabel .='<td class="text-center">'.GetTipePasienPiutang($idtipe).'</td>';
					$tabel .='<td class="text-center">'.$r->no_medrec.'<br>'.($r->namapasien).'</td>';
					if ($r->st_ranap=='1'){
						$tabel .='<td class="text-center">'.GetAsalRujukanRI($r->idtipe).(' <strong>'.$r->nama_ruangan.'</strong>'.($r->nama_kelas?' - '.$r->nama_kelas:'').($r->nama_bed?' - '.$r->nama_bed:'')).'</td>';
					}else{
						$tabel .='<td class="text-center">'.GetAsalRujukanKwitansi($r->idtipe).' - <strong>'.$r->nama_poli.'</strong></td>';
						
					}
					if ($r->st_ranap=='1'){
						$btn_lihat='<a class="btn btn-default menu_click  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_mpp/input_mpp/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
					}else{
						$btn_lihat='<a class="btn btn-default menu_click  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/0/erm_mpp/input_mpp/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
					}
					$tabel .='<td class="text-center">'.($r->kesimpulan_mpp_text).'</td>';
					$tabel .='<td class="text-center">'.($r->tindakan_nama).'</td>';
					$tabel .='<td class="text-center">'.($btn_lihat).'</td>';
					$tabel .='</tr>';
				}
				$data['tabel']=$tabel;
				$data['jml_mpp']=$jml_mpp;
				$this->output->set_output(json_encode($data));
		}
		function load_evaluasi_mpp(){
				$tanggal_1=YMDFormat($this->input->post('tanggal_1'));
				$tanggal_2=YMDFormat($this->input->post('tanggal_2'));
				$jml_mpp=0;
				$where='';
				if ($tanggal_1!=''){
					$where .=" AND H.tanggal_input BETWEEN '".($tanggal_1)."' AND '".($tanggal_2)."'";
				}
				$q="SELECT 
					H.created_ppa,H.created_date,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,H.keterangan_edit,H.jml_edit,H.st_edited
					,CASE WHEN H.st_ranap='1' THEN MP.no_medrec ELSE MJ.no_medrec END no_medrec
					,CASE WHEN H.st_ranap='1' THEN MP.namapasien ELSE MJ.namapasien END namapasien
					,CASE WHEN H.st_ranap='1' THEN MP.nopendaftaran ELSE MJ.nopendaftaran END nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MP.tanggaldaftar ELSE MJ.tanggaldaftar END tanggaldaftar
					,CASE WHEN H.st_ranap='1' THEN MDP.nama ELSE MD.nama END nama_dokter
					,H.idtipe
					,MR.nama as nama_ruangan,MK.nama as nama_kelas,MB.nama as nama_bed
					,H.tanggal_input as tanggal_pengkajian,H.assesmen_id,H.pendaftaran_id_ranap,H.st_ranap,H.pendaftaran_id,MPP.nama as nama_poli
					FROM `tranap_evaluasi_mpp` H
					LEFT JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id_ranap  AND H.st_ranap='1'
					LEFT JOIN tpoliklinik_pendaftaran MJ ON MJ.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN mpoliklinik MPP ON MPP.id=MJ.idpoliklinik
					LEFT JOIN mruangan MR ON MR.id=MP.idruangan
					LEFT JOIN mkelas MK ON MK.id=MP.idkelas
					LEFT JOIN mbed MB ON MB.id=MP.idbed
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					LEFT JOIN mdokter MD ON MD.id=MJ.iddokter
					LEFT JOIN mdokter MDP ON MDP.id=MP.iddokterpenanggungjawab
					WHERE H.status_assemen='2' ".$where;
				$hasil=$this->db->query($q)->result();
				$tabel='';
				$total=0;
				foreach($hasil as $r){
					$jml_mpp=$jml_mpp+1;
					$idtipe=$r->idtipe;
					if ($r->st_ranap=='1'){
						$idtipe=$r->idtipe+2;
					}
					$tabel .='<tr>';
					$tabel .='<td class="text-center">'.$r->nopendaftaran.'<br>'.HumanDateLong($r->tanggal_pengkajian).'</td>';
					$tabel .='<td class="text-center">'.GetTipePasienPiutang($idtipe).'</td>';
					$tabel .='<td class="text-center">'.$r->no_medrec.'<br>'.($r->namapasien).'</td>';
					if ($r->st_ranap=='1'){
						$tabel .='<td class="text-center">'.GetAsalRujukanRI($r->idtipe).(' <strong>'.$r->nama_ruangan.'</strong>'.($r->nama_kelas?' - '.$r->nama_kelas:'').($r->nama_bed?' - '.$r->nama_bed:'')).'</td>';
					}else{
						$tabel .='<td class="text-center">'.GetAsalRujukanKwitansi($r->idtipe).' - <strong>'.$r->nama_poli.'</strong></td>';
						
					}
					if ($r->st_ranap=='1'){
						$btn_lihat='<a class="btn btn-default menu_click  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_mpp/input_evaluasi_mpp/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
					}else{
						$btn_lihat='<a class="btn btn-default menu_click  btn-xs" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/0/erm_mpp/input_evaluasi_mpp/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
					}
					$tabel .='<td class="text-center">'.($btn_lihat).'</td>';
					$tabel .='</tr>';
				}
				$data['tabel']=$tabel;
				$data['jml_mpp']=$jml_mpp;
				$this->output->set_output(json_encode($data));
		}
		function load_index_deposit()
		{
				$data_user=get_acces();
				$user_acces_form=$data_user['user_acces_form'];
				// tipe_pemilik:tipe_pemilik,status:status,nama:nama
				$where='';
				
				$tanggal_1 =$this->input->post('tanggal_1');
				$tanggal_2 =$this->input->post('tanggal_2');
				$satus_deposit =$this->input->post('satus_deposit');
				
				if ($satus_deposit=='1'){
					$where .=" AND H.st_proses='0' AND H.status='1'";
				}
				if ($satus_deposit=='2'){
					$where .=" AND H.st_proses='1' AND H.status='1'";
				}
				if ($satus_deposit=='3'){
					$where .=" AND H.status='0'";
				}
				$this->select = array();
				$from="
						(
							SELECT MP.no_medrec,MP.namapasien,H.* 
							,(CASE
								WHEN H.idmetodepembayaran = 1 THEN 'Tunai'
								WHEN H.idmetodepembayaran = 2 THEN 'Debit'
								WHEN H.idmetodepembayaran = 3 THEN 'Kredit'
								WHEN H.idmetodepembayaran = 4 THEN 'Transfer'
							END) AS metodepembayaran,MB.nama as nama_bank
							FROM `trawatinap_deposit` H
							INNER JOIN trawatinap_pendaftaran MP ON MP.id=H.idrawatinap
							LEFT JOIN mbank MB ON MB.id=H.idbank
							WHERE (H.tanggal >='".YMDFormat($tanggal_1)."' AND H.tanggal <='".YMDFormat($tanggal_2)."') ".$where."
							ORDER BY H.id DESC
						) as tbl  
					";
				// print_r($from);exit();
				$this->from   = $from;
				$this->join 	= array();
				
				
				$this->order  = array();
				$this->group  = array();
				$this->column_search = array('namapasien','no_medrec','nodeposit','terimadari');
				$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			  $no++;
			  $result = array();
			  $btn_pdf='';
			  $result[] = ($r->nodeposit);
			  $result[] = HumanDateShort($r->tanggal);
			  $result[] = $r->no_medrec.'<br>'.$r->namapasien;
			  $result[] = number_format($r->nominal,0);
			  $result[] = $r->metodepembayaran.($r->nama_bank?' - '.$r->nama_bank:'');
			  $nama_status='';
			  if ($r->status=='0'){
				  $nama_status=text_danger('DIBATALKAN');
			  }else{
				  if ($r->st_proses=='1'){
					  if ($r->hapus_proses=='0'){
							$nama_status=text_success('TELAH DIPROSES');
					  }
					  if ($r->hapus_proses=='1'){
						  if ($r->st_approval=='0'){
								$nama_status=text_default('MENUNGGU APPROVAL PEMBATALAN');
						  }	
						  if ($r->st_approval=='1'){
								$nama_status=text_warning('MENUNGGU PENGHAPUSAN');
						  }	
						  if ($r->st_approval=='2'){
								$nama_status=text_success('TELAH DIPROSES');
						  }	
					  }	
				  }else{
					  $nama_status=text_default('MENUNGGU TRANSAKSI');
				  }
			  }
			  $result[] = $nama_status;
			  $result[] = HumanDateLong($r->created_date);
			  if ($r->head_id){
			  if ($r->st_proses=='1'){
				$btn_pdf='<a href="'.base_url().'assets/upload/deposit/'.$r->nodeposit.'.pdf" title="Cetak PDF" target="_blank" class="btn btn-xs btn-success" ><i class="fa fa-file-pdf-o"></i></a>';
			  }
			  if ($r->status=='0' && $r->hapus_proses=='1'){
				$btn_pdf='<a href="'.base_url().'assets/upload/deposit/'.$r->nodeposit.'.pdf" title="Cetak PDF" target="_blank" class="btn btn-xs btn-success" ><i class="fa fa-file-pdf-o"></i></a>';
			  }
			  }
			  $aksi = '<div class="btn-group">';
						$aksi .= $btn_pdf;
			  if ($r->head_id){
				$aksi .= '<button onclick="show_modal_deposit_header_add('.$r->idrawatinap.','.$r->head_id.')"  type="button" data-toggle="tooltip" title="TRANSAKSI DEPOSIT"  class="btn btn-warning btn-xs"><i class="fa fa-list-ul"></i> </button>';
			  }
			  $aksi .= '</div>';
			  $result[] = $aksi;

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
	  function load_index_approval_deposit()
		{
				$where='';
				$user_id=$this->session->userdata('user_id');
				
				
				$this->select = array();
				$from="
						(
							SELECT H.alasanhapus,H.nodeposit,MP.no_medrec,MP.namapasien,H.tanggal,H.deleted_date,H.nominal,A.*
							FROM trawatinap_deposit_approval A
							INNER JOIN trawatinap_deposit H ON H.id=A.deposit_id
							INNER JOIN trawatinap_pendaftaran MP ON MP.id=H.idrawatinap
							WHERE A.approve='0' AND A.iduser='$user_id' AND A.st_aktif='1'
							ORDER BY A.id ASC

						) as tbl  
					";
				// print_r($from);exit();
				$this->from   = $from;
				$this->join 	= array();
				
				
				$this->order  = array();
				$this->group  = array();
				$this->column_search = array('namapasien','no_medrec','nodeposit');
				$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			  $no++;
			  $result = array();
			  $btn_pdf='';
			  $result[] = ($r->nodeposit);
			  $result[] = HumanDateShort($r->tanggal);
			  $result[] = $r->no_medrec.'<br>'.$r->namapasien;
			  $result[] = number_format($r->nominal,0);
			  $result[] = ($r->alasanhapus);
			 
			  $result[] = HumanDateLong($r->deleted_date);
			  $result[] ='STEP '. ($r->step).'<br>'.$r->deskrpisi;
			 
			  $aksi = '<div class="btn-group">';
					$aksi .= '<button title="Setuju" class="btn btn-primary btn-xs setuju" data-id="'.$r->id.'"><i class="fa fa-check"></i> Setuju</button> ';
					  $aksi .= '<button title="Tolak" class="btn btn-danger btn-xs tolak" data-id="'.$r->id.'"><i class="si si-ban"></i></button>';
			  $aksi .= '</div>';
			  $result[] = $aksi;

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
	  function load_index_approval_kwitansi()
		{
				$where='';
				$user_id=$this->session->userdata('user_id');
				
				
				$this->select = array();
				$from="
						(
							SELECT H.alasanhapus,CONCAT(H.kodekwitansi,H.nokwitansi) as nokwitansi,MP.no_medrec,MP.namapasien,H.tanggal,H.deleted_date,H.nominal,A.*
							,M.jenis_kwitansi,MP.nopendaftaran
							FROM trawatinap_kwitansi_approval A
							INNER JOIN trawatinap_kwitansi H ON H.id=A.kwitansi_id
							LEFT JOIN mjenis_kwitansi M ON M.id=H.tipe_proses
							INNER JOIN trawatinap_pendaftaran MP ON MP.id=H.idrawatinap
							WHERE A.approve='0' AND A.iduser='$user_id' AND A.st_aktif='1'
							ORDER BY A.id ASC

						) as tbl  
					";
				// print_r($from);exit();
				$this->from   = $from;
				$this->join 	= array();
				
				
				$this->order  = array();
				$this->group  = array();
				$this->column_search = array('namapasien','no_medrec','nokwitansi','kodekwitansi');
				$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			  $no++;
			  $result = array();
			  $btn_pdf='';
			  $result[] = ($r->nopendaftaran.'<br>'.$r->nokwitansi);
			  $result[] = HumanDateShort($r->tanggal);
			  $result[] = $r->no_medrec.'<br>'.$r->namapasien;
			  $result[] = ($r->jenis_kwitansi);
			  $result[] = number_format($r->nominal,0);
			  $result[] = ($r->alasanhapus);
			 
			  $result[] = HumanDateLong($r->deleted_date);
			  $result[] ='STEP '. ($r->step).'<br>'.$r->deskrpisi;
			 
			  $aksi = '<div class="btn-group">';
					$aksi .= '<button title="Setuju" class="btn btn-primary btn-xs setuju_kwitansi" data-id="'.$r->id.'"><i class="fa fa-check"></i> Setuju</button> ';
					  $aksi .= '<button title="Tolak" class="btn btn-danger btn-xs tolak_kwitansi" data-id="'.$r->id.'"><i class="si si-ban"></i></button>';
			  $aksi .= '</div>';
			  $result[] = $aksi;

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
	  function load_jumlah_selesai(){
		  $tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$satus_deposit =$this->input->post('satus_deposit');
		  $q="
			SELECT COALESCE(SUM(1),0) as jml_selesai
			FROM `trawatinap_pendaftaran` H
			WHERE H.status_selesai_verifikasi > 0 AND DATE(H.tanggal_selesai_verifikasi) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal_selesai_verifikasi) <='".YMDFormat($tanggal_2)."'
		  ";
		  $hasil=$this->db->query($q)->row_array();
		  if ($hasil){
			  
		  }else{
			  $hasil['jml_selesai']=0;
		  }
		  $this->output->set_output(json_encode($hasil));
	  }
	  function load_index_trx_selesai()
		{
				$where='';
				$user_id=$this->session->userdata('user_id');
				$tanggal_1 =$this->input->post('tanggal_1');
				$tanggal_2 =$this->input->post('tanggal_2');
				
				$this->select = array();
				$from="
						(
							SELECT MU.`name` as nama_user_selesai,H.*,PH.pembayaran FROM trawatinap_pendaftaran H
							LEFT JOIN trawatinap_tindakan_pembayaran PH ON PH.idtindakan=H.id AND PH.statusbatal ='0'
							LEFT JOIN musers MU ON MU.id=H.user_id_selesai_verifikasi
							WHERE H.status_selesai_verifikasi > 0 AND DATE(H.tanggal_selesai_verifikasi) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal_selesai_verifikasi) <='".YMDFormat($tanggal_2)."'
							GROUP BY H.id
							ORDER BY H.tanggal_selesai_verifikasi ASC
						) as tbl  
					";
				// print_r($from);exit();
				$this->from   = $from;
				$this->join 	= array();
				
				
				$this->order  = array();
				$this->group  = array();
				$this->column_search = array('namapasien','no_medrec');
				$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			  $no++;
			  $result = array();
			  $btn_pdf='';
			  $result[] = ($r->nopendaftaran);
			  $result[] = HumanDateShort($r->tanggaldaftar);
			  $result[] = $r->no_medrec.'<br>'.$r->namapasien;
			  $result[] = number_format($r->pembayaran,0);
			  $result[] = ($r->status_selesai_verifikasi=='1'?text_primary('SELESAI'):text_success('SELSAI VERIFIKASI'));
			  $result[] = ($r->status_selesai_verifikasi=='1'?$r->nama_user_selesai.'<br>'.HumanDateLong($r->tanggal_selesai_verifikasi):'');
			  $result[] = ($r->status_selesai_verifikasi=='2'?$r->nama_user_selesai.'<br>'.HumanDateLong($r->tanggal_selesai_verifikasi):'');
			  $aksi = '<div class="btn-group">';
			  $aksi .= '</div>';
			  // $result[] = $aksi;

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
	  function load_jumlah_card(){
		  $tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$satus_deposit =$this->input->post('satus_deposit');
		  $q="
			SELECT COALESCE(SUM(1),0) as jml_card
			FROM `trawatinap_pendaftaran` H
			WHERE H.status_proses_card_pass > 0 AND DATE(H.tanggal_proses_card_pass) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal_proses_card_pass) <='".YMDFormat($tanggal_2)."'
		  ";
		  $hasil=$this->db->query($q)->row_array();
		  if ($hasil){
			  
		  }else{
			  $hasil['jml_card']=0;
		  }
		  $this->output->set_output(json_encode($hasil));
	  }
	  function load_index_trx_card()
		{
				$where='';
				$user_id=$this->session->userdata('user_id');
				$tanggal_1 =$this->input->post('tanggal_1');
				$tanggal_2 =$this->input->post('tanggal_2');
				
				$this->select = array();
				$from="
						(
							SELECT MU.`name` as nama_user_card,H.*,PH.pembayaran 
							FROM trawatinap_pendaftaran H
							LEFT JOIN trawatinap_tindakan_pembayaran PH ON PH.idtindakan=H.id AND PH.statusbatal ='0'
							LEFT JOIN musers MU ON MU.id=H.user_id_proses_card_pass
							WHERE H.status_proses_card_pass > 0 AND DATE(H.tanggal_proses_card_pass) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal_proses_card_pass) <='".YMDFormat($tanggal_2)."'
							GROUP BY H.id
							ORDER BY H.tanggal_proses_card_pass ASC
						) as tbl  
					";
				// print_r($from);exit();
				$this->from   = $from;
				$this->join 	= array();
				
				
				$this->order  = array();
				$this->group  = array();
				$this->column_search = array('namapasien','no_medrec');
				$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			  $no++;
			  $result = array();
			  $btn_pdf='';
			  $result[] = ($r->nopendaftaran);
			  $result[] = HumanDateShort($r->tanggaldaftar);
			  $result[] = $r->no_medrec.'<br>'.$r->namapasien;
			  $result[] = number_format($r->pembayaran,0);
			  $result[] = ($r->status_proses_card_pass=='1'?text_primary('SELESAI'):text_success('SELSAI VERIFIKASI'));
			  $result[] = ($r->status_proses_card_pass=='1'?$r->nama_user_card.'<br>'.HumanDateLong($r->tanggal_proses_card_pass):'');
			  $result[] = ($r->info_proses_card_pass);
			  $aksi = '<div class="btn-group">';
			  $aksi .= '<a href="'.base_url().'tbilling_ranap/cetak_card_pass/'.$r->id.'" title="Card pass" target="_blank" class="btn btn-xs btn-warning"><i class="fa fa-list-ul"></i> </a>';
			  $aksi .= '</div>';
			  $result[] = $aksi;
			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
	  function setuju_batal_kwitansi($id,$status){
		// $arr=array();
		$q="call update_approval_kwitansi('$id', $status) ";
		$result=$this->db->query($q);
		// $arr['st_insert']=$result;
		// $arr=$this->db->query("SELECT H.st_bendahara FROM tkasbon H  WHERE H.id='$idrka' AND H.status='2'")->row('st_bendahara');
		$this->output->set_output(json_encode($result));
	}
	function tolak_kwitansi(){
		$id=$this->input->post('id_approval');
		$alasan_tolak=$this->input->post('alasan_tolak');
		$q="call update_approval_kwitansi('$id', 2) ";
		$this->db->query($q);
		$result=$this->db->update('trawatinap_kwitansi_approval',array('alasan_tolak'=>$alasan_tolak),array('id'=>$id));
		echo json_encode($result);
	}
	  function load_jumlah_approval_kwitansi(){
		 $user_id=$this->session->userdata('user_id');
		  $q="
			SELECT COUNT(A.id) as jml_deposit
			FROM trawatinap_kwitansi_approval A
			INNER JOIN trawatinap_kwitansi H ON H.id=A.kwitansi_id
			INNER JOIN trawatinap_pendaftaran MP ON MP.id=H.idrawatinap
			WHERE A.approve='0' AND A.iduser='$user_id' AND A.st_aktif='1'
		  ";
		  $hasil=$this->db->query($q)->row_array();
		  if ($hasil){
			  
		  }else{
			  $hasil['jml_deposit']=0;
		  }
		  $this->output->set_output(json_encode($hasil));
	  }
	  function load_jumlah_approval_deposit(){
		 $user_id=$this->session->userdata('user_id');
		  $q="
			SELECT COUNT(A.id) as jml_deposit
			FROM trawatinap_deposit_approval A
			INNER JOIN trawatinap_deposit H ON H.id=A.deposit_id
			INNER JOIN trawatinap_pendaftaran MP ON MP.id=H.idrawatinap
			WHERE A.approve='0' AND A.iduser='$user_id' AND A.st_aktif='1'
		  ";
		  $hasil=$this->db->query($q)->row_array();
		  if ($hasil){
			  
		  }else{
			  $hasil['jml_deposit']=0;
		  }
		  $this->output->set_output(json_encode($hasil));
	  }
	  function load_jumlah_deposit(){
		  $tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$satus_deposit =$this->input->post('satus_deposit');
		  $q="
			SELECT COALESCE(SUM(H.nominal),0) as total_deposit,COALESCE(SUM(1),0) as jml_deposit
			FROM `trawatinap_deposit` H
			WHERE H.status <> '0' AND H.st_proses='1' AND (H.tanggal >='".YMDFormat($tanggal_1)."' AND H.tanggal <='".YMDFormat($tanggal_2)."')
		  ";
		  $hasil=$this->db->query($q)->row_array();
		  if ($hasil){
			  
		  }else{
			  $hasil['total_deposit']=0;
			  $hasil['jml_deposit']=0;
		  }
		  $this->output->set_output(json_encode($hasil));
	  }
	  
	  function last_id_tindakan($id){
		  $last_id='';
		  $q="
			SELECT H.id as last_id FROM trawatinap_deposit_head H
				WHERE H.idrawatinap='$id' AND H.st_persetujuan='1'
				ORDER BY H.setuju_date DESC
				LIMIT 1
		";
		$last_id=$this->db->query($q)->row('last_id');
		return $last_id;
	  }
	  function load_index_tindakan()
		{
			$q="SELECT st_setuju_tanpa_deposit FROM setting_deposit";
		$st_setuju_tanpa_deposit=$this->db->query($q)->row('st_setuju_tanpa_deposit');
				$data_user=get_acces();
				$user_acces_form=$data_user['user_acces_form'];
				// tipe_pemilik:tipe_pemilik,status:status,nama:nama
				$where='';
				
				$tanggal_1 =$this->input->post('tanggal_1');
				$tanggal_2 =$this->input->post('tanggal_2');
				$satus_deposit =$this->input->post('satus_deposit');
				
				if ($satus_deposit=='1'){
					$where .=" AND H.st_proses='0' AND H.status='1'";
				}
				if ($satus_deposit=='2'){
					$where .=" AND H.st_proses='1' AND H.status='1'";
				}
				if ($satus_deposit=='3'){
					$where .=" AND H.status='0'";
				}
				$this->select = array();
				$from="
						(
							SELECT MP.nopendaftaran,MP.title,MP.namapasien,MP.no_medrec,MU.`name`  as nama_user,SUM(COALESCE(D.nominal,0)) as nominal_deposit,TD.total_deposit_all,H.* 
							FROM trawatinap_deposit_head H
							LEFT JOIN trawatinap_deposit D ON D.head_id=H.id AND D.st_proses='1' AND D.`status`='1'
							LEFT JOIN (
											SELECT D.idrawatinap,SUM(D.nominal) as total_deposit_all 
												FROM trawatinap_deposit D
												WHERE D.`status`='1' AND D.st_proses='1'
												GROUP BY D.idrawatinap
										) TD ON TD.idrawatinap=H.idrawatinap
							INNER JOIN trawatinap_pendaftaran MP ON MP.id=H.idrawatinap
							LEFT JOIN musers MU ON MU.id=H.setuju_user_id
							WHERE DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."' AND H.tipe_deposit='1'
							GROUP BY H.id
						) as tbl  
					";
				// print_r($from);exit();
				$this->from   = $from;
				$this->join 	= array();
				
				
				$this->order  = array();
				$this->group  = array();
				$this->column_search = array('namapasien','no_medrec','nopendaftaran','deskripsi','nama_tindakan');
				$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			  $aksi='';
				$status='';
				if ($r->st_persetujuan=='0'){
					if (UserAccesForm($user_acces_form,array('2622'))){
						if ($st_setuju_tanpa_deposit=='0'){
							if ($r->nominal_deposit>0){
								$aksi='<button class="btn btn-xs btn-success" type="button" onclick="show_setuju_tindakan('.$r->id.')" title="Setuju Tindakan"><li class="fa fa-check"></li> SETUJU</button>';
							}else{
								$aksi='<button class="btn btn-xs btn-success" type="button" disabled onclick="show_setuju_tindakan('.$r->id.')" title="Setuju Tindakan"><li class="fa fa-check"></li> SETUJU</button>';
								
							}
						}else{
							$aksi='<button class="btn btn-xs btn-success" type="button" onclick="show_setuju_tindakan('.$r->id.')" title="Setuju Tindakan"><li class="fa fa-check"></li> SETUJU</button>';
						}
					}
					$aksi .= ' <button onclick="show_modal_deposit_header_add('.$r->idrawatinap.','.$r->id.')"  type="button" data-toggle="tooltip" title="TRANSAKSI DEPOSIT"  class="btn btn-warning btn-xs"><i class="fa fa-list-ul"></i> </button>';
					$status=text_default('BELUM DISETUJUI');
				}else{
					$last_id=$this->last_id_tindakan($r->idrawatinap);
					if ($last_id==$r->id){
						$status=text_primary('TINDAKAN SAAT INI');
					}else{
						$status=text_danger('SELESAI');
					}
					$aksi=text_success('SUDAH DISETUJUI');
					$aksi .= '<br> <br> <button onclick="show_modal_deposit_header_add('.$r->idrawatinap.','.$r->id.')"  type="button" data-toggle="tooltip" title="TRANSAKSI DEPOSIT"  class="btn btn-warning btn-xs"><i class="fa fa-list-ul"></i> </button>';
				}
				if ($r->st_persetujuan=='0'){
					$btn_cetak='<br><br><a class="btn btn-danger btn-xs" target="_blank" href="'.site_url().'ttindakan_ranap_deposit/cetak_estimasi_biaya/'.$r->perencanaan_id.'/'.$r->id.'/0"  type="button" title="Estimas Biaya" type="button"><i class="fa fa-file-pdf-o"></i></a>';
				}else{
					$btn_cetak='';
					$btn_cetak='<br><br><a href="'.base_url().'assets/upload/deposit/'.$r->nopermintaan.'.pdf" title="Estimasi Biaya" target="_blank" class="btn btn-danger btn-xs" ><i class="fa fa-file-pdf-o"></i></a>';
				}
				
			  $no++;
			  $result = array();
			  $btn_pdf='';
			  $result[] = ($r->nopendaftaran);
			  $result[] = $r->title.'. '.$r->namapasien.' - '.$r->no_medrec;
			  $result[] = ($r->setuju_date?HumanDateShort($r->setuju_date):text_default('BELUM DISETUJUI'));
			  $result[] = ($r->deskripsi.' '.$r->nama_tindakan);
			  $result[] = ($r->st_persetujuan?$r->nama_user.'<br>'.HumanDateLong($r->setuju_date).'<br>':'').($r->nominal_deposit==0?text_warning('BELUM  DEPOSIT'):text_primary('TELAH DEPOSIT'));
			  $result[] = $status.$btn_cetak;
			  $result[] = $aksi;
			 
			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
}
