<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrekanan extends CI_Controller {

	/**
	 * Rekanan Asuransi controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mrekanan_model');
		$this->load->helper('path');
  }

	function index(){

		$data = array();
		$data['error'] 			= '';
		$data['tanggal_1'] 			= '';
		$data['tanggal_2'] 			= '';
		$data['title'] 			= 'Rekanan Asuransi';
		$data['content'] 		= 'Mrekanan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Rekanan Asuransi",'#'),
									    			array("List",'mrekanan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){

		$data = array(
			'id' 															=> '',
			'tipe_rekanan' 														=> '',
			'nama' 														=> '',
			'namalengkap' 										=> '',
			'alamat' 													=> '',
			'telepon' 												=> '',
			'fax' 														=> '',
			'email' 													=> '',
			'jenispks' 												=> '',
			'prosedur' 												=> '',
			'penggunaanformulir' 							=> '',
			'alokasi' 												=> '',
			'tadm_rawatjalan'									=> '',
			'tkartu_rawatjalan'								=> '',
			'tadm_rawatinap'									=> '',
			'tkartu_rawatinap'								=> '',
			'toperasi_sewaalat'								=> '',
			'toperasi_tindakan'								=> '',
			'toperasi_jenis'									=> array(),
			'tradiologi_xray'									=> '',
			'tradiologi_usg'									=> '',
			'tradiologi_ctscan'								=> '',
			'tradiologi_mri'									=> '',
			'tradiologi_bmd'									=> '',
			'tlaboratorium_umum'							=> '',
			'tlaboratorium_pa'								=> '',
			'tlaboratorium_pmi'								=> '',
			'tfisioterapi'										=> '',
			'trawatjalan'											=> '',
			'trawatinap_fullcare1'						=> '',
			'trawatinap_ecg1'									=> '',
			'trawatinap_visite1'							=> '',
			'trawatinap_sewaalat1'						=> '',
			'trawatinap_ambulance1'						=> '',
			'trawatinap_fullcare2'						=> '',
			'trawatinap_ecg2'									=> '',
			'trawatinap_visite2'							=> '',
			'trawatinap_sewaalat2'						=> '',
			'trawatinap_ambulance2'						=> '',
			'trawatinap_fullcare3'						=> '',
			'trawatinap_ecg3'									=> '',
			'trawatinap_visite3'							=> '',
			'trawatinap_sewaalat3'						=> '',
			'trawatinap_ambulance3'						=> '',
			'trawatinap_fullcare4'						=> '',
			'trawatinap_ecg4'									=> '',
			'trawatinap_visite4'							=> '',
			'trawatinap_sewaalat4'						=> '',
			'trawatinap_ambulance4'						=> '',
			'truang_perawatan'								=> '',
			'truang_hcu'											=> '',
			'truang_icu'											=> '',
			'truang_isolasi'									=> '',
			'besaran_diskon'									=> '',
			'masa_berlaku'									=> '',
			'pic'									=> '',
			'status' 													=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Rekanan Asuransi';
		$data['content'] 		= 'Mrekanan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Rekanan Asuransi",'#'),
									    			array("Tambah",'mrekanan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){

		if($id != ''){
			$row = $this->Mrekanan_model->getSpecified($id);
			$jenis_operasi = $this->Mrekanan_model->getJenisOperasi($id);
			if(isset($row->id)){
				$data = array(
					'id' 															=> $row->id,
					'tipe_rekanan' 														=> $row->tipe_rekanan,
					'nama' 														=> $row->nama,
					'namalengkap' 										=> $row->namalengkap,
					'alamat' 													=> $row->alamat,
					'telepon' 												=> $row->telepon,
					'fax' 														=> $row->fax,
					'email' 													=> $row->email,
					'jenispks' 												=> $row->jenispks,
					'prosedur' 												=> $row->prosedur,
					'penggunaanformulir' 							=> $row->penggunaanformulir,
					'alokasi' 												=> $row->alokasi,
					'tadm_rawatjalan'									=> $row->tadm_rawatjalan,
					'tkartu_rawatjalan'								=> $row->tkartu_rawatjalan,
					'tadm_rawatinap'									=> $row->tadm_rawatinap,
					'tkartu_rawatinap'								=> $row->tkartu_rawatinap,
					'toperasi_sewaalat'								=> $row->toperasi_sewaalat,
					'toperasi_tindakan'								=> $row->toperasi_tindakan,
					'toperasi_jenis'									=> $jenis_operasi,
					'tradiologi_xray'									=> $row->tradiologi_xray,
					'tradiologi_usg'									=> $row->tradiologi_usg,
					'tradiologi_ctscan'								=> $row->tradiologi_ctscan,
					'tradiologi_mri'									=> $row->tradiologi_mri,
					'tradiologi_bmd'									=> $row->tradiologi_bmd,
					'tlaboratorium_umum'							=> $row->tlaboratorium_umum,
					'tlaboratorium_pa'								=> $row->tlaboratorium_pa,
					'tlaboratorium_pmi'								=> $row->tlaboratorium_pmi,
					'tfisioterapi'										=> $row->tfisioterapi,
					'trawatjalan'											=> $row->trawatjalan,
					'tigd'														=> $row->tigd,
					'trawatinap_fullcare1'						=> $row->trawatinap_fullcare1,
					'trawatinap_ecg1'									=> $row->trawatinap_ecg1,
					'trawatinap_visite1'							=> $row->trawatinap_visite1,
					'trawatinap_sewaalat1'						=> $row->trawatinap_sewaalat1,
					'trawatinap_ambulance1'						=> $row->trawatinap_ambulance1,
					'trawatinap_fullcare2'						=> $row->trawatinap_fullcare2,
					'trawatinap_ecg2'									=> $row->trawatinap_ecg2,
					'trawatinap_visite2'							=> $row->trawatinap_visite2,
					'trawatinap_sewaalat2'						=> $row->trawatinap_sewaalat2,
					'trawatinap_ambulance2'						=> $row->trawatinap_ambulance2,
					'trawatinap_fullcare3'						=> $row->trawatinap_fullcare3,
					'trawatinap_ecg3'									=> $row->trawatinap_ecg3,
					'trawatinap_visite3'							=> $row->trawatinap_visite3,
					'trawatinap_sewaalat3'						=> $row->trawatinap_sewaalat3,
					'trawatinap_ambulance3'						=> $row->trawatinap_ambulance3,
					'trawatinap_fullcare4'						=> $row->trawatinap_fullcare4,
					'trawatinap_ecg4'									=> $row->trawatinap_ecg4,
					'trawatinap_visite4'							=> $row->trawatinap_visite4,
					'trawatinap_sewaalat4'						=> $row->trawatinap_sewaalat4,
					'trawatinap_ambulance4'						=> $row->trawatinap_ambulance4,
					'truang_perawatan'								=> $row->truang_perawatan,
					'truang_hcu'											=> $row->truang_hcu,
					'truang_icu'											=> $row->truang_icu,
					'truang_isolasi'									=> $row->truang_isolasi,
					'status' 													=> $row->status,
					'pic' 													=> $row->pic,
					'besaran_diskon' 													=> $row->besaran_diskon,
					'masa_berlaku' 													=> ($row->masa_berlaku?HumanDateShort($row->masa_berlaku):''),
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Rekanan Asuransi';
				$data['content']	 	= 'Mrekanan/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Rekanan Asuransi",'#'),
											    			array("Ubah",'mrekanan')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mrekanan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mrekanan');
		}
	}
	function view($id){

		if($id != ''){
			$row = $this->Mrekanan_model->getSpecified($id);
			$jenis_operasi = $this->Mrekanan_model->getJenisOperasi($id);
			if(isset($row->id)){
				$data = array(
					'id' 															=> $row->id,
					'nama' 														=> $row->nama,
					'namalengkap' 										=> $row->namalengkap,
					'alamat' 													=> $row->alamat,
					'telepon' 												=> $row->telepon,
					'fax' 														=> $row->fax,
					'email' 													=> $row->email,
					'jenispks' 												=> $row->jenispks,
					'prosedur' 												=> $row->prosedur,
					'penggunaanformulir' 							=> $row->penggunaanformulir,
					'alokasi' 												=> $row->alokasi,
					'tadm_rawatjalan'									=> $row->tadm_rawatjalan,
					'tkartu_rawatjalan'								=> $row->tkartu_rawatjalan,
					'tadm_rawatinap'									=> $row->tadm_rawatinap,
					'tkartu_rawatinap'								=> $row->tkartu_rawatinap,
					'toperasi_sewaalat'								=> $row->toperasi_sewaalat,
					'toperasi_tindakan'								=> $row->toperasi_tindakan,
					'toperasi_jenis'									=> $jenis_operasi,
					'tradiologi_xray'									=> $row->tradiologi_xray,
					'tradiologi_usg'									=> $row->tradiologi_usg,
					'tradiologi_ctscan'								=> $row->tradiologi_ctscan,
					'tradiologi_mri'									=> $row->tradiologi_mri,
					'tradiologi_bmd'									=> $row->tradiologi_bmd,
					'tlaboratorium_umum'							=> $row->tlaboratorium_umum,
					'tlaboratorium_pa'								=> $row->tlaboratorium_pa,
					'tlaboratorium_pmi'								=> $row->tlaboratorium_pmi,
					'tfisioterapi'										=> $row->tfisioterapi,
					'tigd'														=> $row->tigd,
					'trawatjalan'											=> $row->trawatjalan,
					'trawatinap_fullcare1'						=> $row->trawatinap_fullcare1,
					'trawatinap_ecg1'									=> $row->trawatinap_ecg1,
					'trawatinap_visite1'							=> $row->trawatinap_visite1,
					'trawatinap_sewaalat1'						=> $row->trawatinap_sewaalat1,
					'trawatinap_ambulance1'						=> $row->trawatinap_ambulance1,
					'trawatinap_fullcare2'						=> $row->trawatinap_fullcare2,
					'trawatinap_ecg2'									=> $row->trawatinap_ecg2,
					'trawatinap_visite2'							=> $row->trawatinap_visite2,
					'trawatinap_sewaalat2'						=> $row->trawatinap_sewaalat2,
					'trawatinap_ambulance2'						=> $row->trawatinap_ambulance2,
					'trawatinap_fullcare3'						=> $row->trawatinap_fullcare3,
					'trawatinap_ecg3'									=> $row->trawatinap_ecg3,
					'trawatinap_visite3'							=> $row->trawatinap_visite3,
					'trawatinap_sewaalat3'						=> $row->trawatinap_sewaalat3,
					'trawatinap_ambulance3'						=> $row->trawatinap_ambulance3,
					'trawatinap_fullcare4'						=> $row->trawatinap_fullcare4,
					'trawatinap_ecg4'									=> $row->trawatinap_ecg4,
					'trawatinap_visite4'							=> $row->trawatinap_visite4,
					'trawatinap_sewaalat4'						=> $row->trawatinap_sewaalat4,
					'trawatinap_ambulance4'						=> $row->trawatinap_ambulance4,
					'truang_perawatan'								=> $row->truang_perawatan,
					'truang_hcu'											=> $row->truang_hcu,
					'truang_icu'											=> $row->truang_icu,
					'truang_isolasi'									=> $row->truang_isolasi,
					'status' 													=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Rekanan Asuransi';
				$data['content']	 	= 'Mrekanan/view';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Rekanan Asuransi",'#'),
											    			array("Ubah",'mrekanan')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mrekanan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mrekanan');
		}
	}

	function delete($id){

		$this->Mrekanan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mrekanan','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('namalengkap', 'Nama Lengkap', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'trim|required');
		$this->form_validation->set_rules('fax', 'Fax', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('jenispks', 'Jenis PKS', 'trim|required');
		$this->form_validation->set_rules('prosedur', 'Prosedur', 'trim|required');
		$this->form_validation->set_rules('penggunaanformulir', 'Penggunaan Formulir', 'trim|required');
		$this->form_validation->set_rules('alokasi', 'Alokasi', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mrekanan_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mrekanan','location');
				}
			} else {
				if($this->Mrekanan_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mrekanan','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 		= validation_errors();
		$data['content'] 	= 'Mrekanan/manage';

		if($id==''){
			$data['title'] = 'Tambah Rekanan Asuransi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Rekanan Asuransi",'#'),
															array("Tambah",'mrekanan')
													);
		}else{
			$data['title'] = 'Ubah Rekanan Asuransi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Rekanan Asuransi",'#'),
															array("Ubah",'mrekanan')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	  $data_user=get_acces();
	  $tipe_rekanan=$this->input->post('tipe_rekanan');
	  $tanggal_2=$this->input->post('tanggal_2');
	  $tanggal_1=$this->input->post('tanggal_1');
	  $where='';
	  if ($tipe_rekanan!='0'){
		  $where .=" AND R.tipe_rekanan='$tipe_rekanan'";
	  }
	  if ($tanggal_1 !=''){
			$where .=" AND DATE(R.masa_berlaku) >='".YMDFormat($tanggal_1)."' AND DATE(R.masa_berlaku) <='".YMDFormat($tanggal_2)."'";
	  }
	  $user_acces_form=$data_user['user_acces_form'];

			$this->select = array();
			$from="(
					SELECT R.id,R.nama,R.tipe_rekanan,R.masa_berlaku,R.besaran_diskon,R.pic
					,GROUP_CONCAT(S.tanggal_hari ORDER BY tanggal_hari ASC SEPARATOR ', ') as tanggal_tagihan,R.`status`,R.namalengkap,R.alamat,R.telepon 
					FROM mrekanan R
					LEFT JOIN mrekanan_setting S ON S.idrekanan=R.id AND S.`status`='1'
					WHERE R.`status`='1' ".$where."
					GROUP BY R.id
					ORDER BY R.nama ASC
			)tbl ";
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array(
			);
			$this->order  = array(
			);
			$this->group  = array();

      $this->column_search   = array('nama','alamat','telepon');
      $this->column_order    = array('nama','alamat','telepon');

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
		  $tipe='';
		  if ($r->tipe_rekanan=='1'){
			  $tipe='&nbsp;&nbsp;&nbsp;'.text_primary('ASURANSI');
		  }else{
			  
			  $tipe='&nbsp;&nbsp;&nbsp;'.text_success('REKANAN');
		  }
          $row[] = $no;
          $row[] = $r->nama.$tipe;
          $row[] = $r->namalengkap;
          $row[] = $r->alamat;
          $row[] = $r->telepon;
          $row[] = ($r->masa_berlaku?HumanDateShort($r->masa_berlaku).' '.($r->masa_berlaku < date('Y-m-d')?text_danger('Expired'):''):text_default('BELUM ADA'));
          $row[] = number_format($r->besaran_diskon).' %';
          $row[] = ($r->pic);
          $row[] ='<span class="label label-danger">'. $r->tanggal_tagihan.'</span>';
			$aksi = '<div class="btn-group">';
			if (UserAccesForm($user_acces_form,array('38'))){
			$aksi .= '<a href="'.site_url().'mrekanan/view/'.$r->id.'" data-toggle="tooltip" title="View" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
			}
			if (UserAccesForm($user_acces_form,array('35','36'))){
				$aksi .= '<a href="'.site_url().'mrekanan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<a href="'.site_url().'mrekanan/setting/'.$r->id.'" data-toggle="tooltip" title="Setting Jatuh Tempo" class="btn btn-success btn-sm"><i class="si si-settings"></i></a>';
			}
			if (UserAccesForm($user_acces_form,array('37'))){
				$aksi .= '<a href="#" data-urlindex="'.site_url().'mrekanan" data-urlremove="'.site_url().'mrekanan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
			}



			$aksi .= '</div>';
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function setting($id){

	if($id != ''){
		$row = $this->Mrekanan_model->getSpecified($id);
		if(isset($row->id)){
			$data = array(
				'id' 															=> $row->id,
				'nama' 														=> $row->nama,

			);

			$data['error'] 			= '';
			$data['title'] 			= 'Setting Jatuh Tempo';
			$data['content']	 	= 'Mrekanan/setting';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Setting Jatuh Tempo",'#'),
														array("Setting",'mrekanan')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpasien_kelompok','location');
		}
	}else{
		$this->session->set_flashdata('error',true);
		$this->session->set_flashdata('message_flash','data tidak ditemukan.');
		redirect('mpasien_kelompok');
	}
}
  function Load_Jatuhtempo()
    {
		$idrekanan     		= $this->input->post('idrekanan');


		$this->select = array();
		$this->join 	= array();
		$this->where  = array();


		$from="(SELECT *from mrekanan_setting
				WHERE status='1' AND idrekanan='$idrekanan'
				ORDER BY tanggal_hari ASC
			) as tbl  ";

		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();
		$url        = site_url('mpasien_kelompok/');
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

           $row[] = $r->id;//0
			$row[] = $r->tipe;//1
            $row[] = ($r->tipe=='1')?'<span class="label label-success">Rawat Jalan</span>':'<span class="label label-primary">Rawat Inap</span>';//2
            $row[] = $r->tanggal_hari;
			$row[] = $r->batas_kirim;
			$row[] = $r->jml_hari;
            $row[] =$r->created_nama.'-'.HumanDateLong($r->updated_date);

			$aksi       = '<div class="btn-group">';
			$aksi .= '<button class="btn btn-xs btn-primary edit" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button class="btn btn-xs btn-danger hapus" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';

			$aksi.='</div>';
			$row[] = $aksi;
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	public function save_jt()
    {
        $idrekanan = $this->input->post('idrekanan');
        $tanggal_hari = $this->input->post('tanggal_hari');
        $jml_hari = $this->input->post('jml_hari');
        $batas_kirim = $this->input->post('batas_kirim');
        $tipe = $this->input->post('tipe');
		$data =array(
            'tipe'=>$tipe,
            'idrekanan'=>$idrekanan,
            'tanggal_hari'=>$tanggal_hari,
            'jml_hari'=>$jml_hari,
            'batas_kirim'=>$batas_kirim,
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
			'created_date'=>date('Y-m-d H:i:s'),
			'updated_by'=>$this->session->userdata('user_id'),
			'updated_nama'=>$this->session->userdata('user_name'),
			'updated_date'=>date('Y-m-d H:i:s'),


        );
		$result=$this->db->insert('mrekanan_setting',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function hapus_data()
    {
        $id_jt = $this->input->post('id_jt');

		$data =array(
            'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_nama'=>$this->session->userdata('user_name'),
			'deleted_date'=>date('Y-m-d H:i:s'),


        );
		$this->db->where('id',$id_jt);;
		$result=$this->db->update('mrekanan_setting',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function update_date()
    {
        $id_jt = $this->input->post('id_jt');
         $tanggal_hari = $this->input->post('tanggal_hari');
        $jml_hari = $this->input->post('jml_hari');
        $batas_kirim = $this->input->post('batas_kirim');
       $tipe = $this->input->post('tipe');
		$data =array(
            'tipe'=>$tipe,
            'tanggal_hari'=>$tanggal_hari,
            'jml_hari'=>$jml_hari,
            'batas_kirim'=>$batas_kirim,
			'updated_by'=>$this->session->userdata('user_id'),
			'updated_nama'=>$this->session->userdata('user_name'),
			'updated_date'=>date('Y-m-d H:i:s'),



        );
		$this->db->where('id',$id_jt);;
		$result=$this->db->update('mrekanan_setting',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	function cek_duplicate($idrekanan,$tanggal_hari,$tipe,$id_jt=''){
		if ($id_jt==''){
			$q="SELECT COUNT(M.id)  as jml From mrekanan_setting M
				WHERE M.idrekanan='$idrekanan' AND M.tipe='$tipe' AND M.tanggal_hari='$tanggal_hari' AND M.`status`='1'";
		}else{
			$q="SELECT COUNT(M.id)  as jml,M.tanggal_hari From mrekanan_setting M
				WHERE M.idrekanan='$idrekanan' AND M.tipe='$tipe' AND M.tanggal_hari='$tanggal_hari' AND M.`status`='1' AND M.id !='$id_jt'";
		}

		$arr['detail'] =$this->db->query($q)->row_array();
		// $data['jml']=$row;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($data));

	}
	public function upload() {
       $uploadDir = './assets/upload/mrekanan';
		if (!empty($_FILES)) {
			 $idrka = $this->input->post('idrka');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['idrekanan'] 	= $idrka;
			$detail['file_name']		= $file_name;
			$detail['size']		= formatSizeUnits($size);
			$detail['tanggal_upload']	= date('Y-m-d H:i:s');
			$detail['user_upload']	= $this->session->userdata('user_name');
			$this->db->insert('mrekanan_upload', $detail);
		}
    }
	function refresh_image($id){		
		$arr['detail'] = $this->Mrekanan_model->refresh_image($id);
		$this->output->set_output(json_encode($arr));
	}
	function hapus_file(){
		$id=$this->input->post('id');
		$row = $this->Mrekanan_model->get_file_name($id);
		if(file_exists('./assets/upload/mrekanan/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/mrekanan/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from mrekanan_upload WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
}
