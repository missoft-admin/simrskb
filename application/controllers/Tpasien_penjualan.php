<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

class Tpasien_penjualan extends CI_Controller
{

    /*
     ##################################################
     ###### Farmasi - Penjualan Obat. 			  #####
     ###### Developer @Deni Purnama				  #####
     ###### Email: denipurnama371@gmail.com 	  #####
     ##################################################
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Tpasien_penjualan_model', 'tpaspen');
        $this->load->model('Trawatinap_pendaftaran_model', 'Trawatinap_pendaftaran_model');
    }

    public function index($tab='0')
    {
        // print_r(site_url());exit();

        $data = array(
            'namapasien' => '',
            'tanggaldari' => date('d/m/Y', strtotime("-1 week")),
            'tanggalsampai' => date("d/m/Y"),
            'idasalpasien' => '',
            'nopenjualan' => '',
            'nomedrec' => '',
            'idkelompokpasien' => '',
            'status' => '',
        );

        $data['error'] 			= '';
        $data['tab'] 				= $tab;
        $data['title'] 			= 'Penjualan';
        $data['content'] 		= 'Tpasien_penjualan/index';
        $data['breadcrum'] 	= array(
                                  array("RSKB Halmahera",'#'),
                                  array("Farmasi",'#'),
                                  array("Penjualan",'tpasien_penjualan')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filter()
    {
        $data = array(
          'namapasien' => $this->input->post('namapasien'),
          'tanggaldari' => $this->input->post('tanggaldari'),
          'tanggalsampai' => $this->input->post('tanggalsampai'),
          'idasalpasien' => $this->input->post('idasalpasien'),
          'idkelompokpasien' => $this->input->post('idkelompokpasien'),
          'status' => $this->input->post('status'),
          'nopenjualan' => $this->input->post('nopenjualan'),
          'nomedrec' => $this->input->post('nomedrec'),
        );

        // print_r($data);exit();
        $this->session->set_userdata($data);

        $data['error'] 			= '';
        $data['tab'] 				= 0;
        $data['title'] 			= 'Penjualan';
        $data['content'] 		= 'Tpasien_penjualan/index';
        $data['breadcrum'] 	= array(
                                  array("RSKB Halmahera",'#'),
                                  array("Farmasi",'#'),
                                  array("Penjualan",'tpasien_penjualan')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function non_rujukan($tab=0)
    {
        $data = array(
            'namapasien' => '',
            'tanggaldari' => date("d/m/Y"),
            'tanggalsampai' => date("d/m/Y"),
            'idasalpasien' => '',
			'nopenjualan' => '',
            'nomedrec' => '',
            'idkelompokpasien' => '',
            'status' => '',
        );

        $data['error'] 			= '';
        $data['tab'] 				= $tab;
        $data['title'] 			= 'Penjualan';
        $data['content'] 		= 'Tpasien_penjualan/index';
        $data['breadcrum'] 	= array(
                                    array("RSKB Halmahera",'#'),
                                    array("Farmasi",'#'),
                                    array("Penjualan",'tpasien_penjualan')
                                );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filternonrujukan($tab=1)
    {
        $data = array(
            'namapasien' => $this->input->post('namapasien'),
            'tanggaldari' => $this->input->post('tanggaldari'),
            'tanggalsampai' => $this->input->post('tanggalsampai'),
            'idasalpasien' => $this->input->post('idasalpasien'),
            'idkelompokpasien' => $this->input->post('idkelompokpasien'),
            'status' => $this->input->post('status'),
			'nopenjualan' => $this->input->post('nopenjualan'),
            'nomedrec' => $this->input->post('nomedrec'),
        );

        $this->session->set_userdata($data);

        $data['error'] 			= '';
        $data['tab'] 				= $tab;
        $data['title'] 			= 'Penjualan';
        $data['content'] 		= 'Tpasien_penjualan/index';
        $data['breadcrum'] 	= array(
                                    array("RSKB Halmahera",'#'),
                                    array("Farmasi",'#'),
                                    array("Penjualan",'tpasien_penjualan')
                                );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create($id=0,$disabel='')
    {
        $data = array();
        $data['error']      = '';
        $data['title']      = 'Tambah Penjualan';
        $data['content']    = 'Tpasien_penjualan/manage';
        $data['breadcrum']  = array(
                                  array("RSKB Halmahera",'#'),
                                  array("Farmasi",'#'),
                                  array("Tambah Penjualan",'tpasien_penjualan/create')
                              );
		$data['disabel']=$disabel;

        if (!empty($id)) {
            $idtest = $id;
            $row=$this->tpaspen->get_dataPenjualan($id);
			$data['st_lock']=0;
            $data['tanggal']=HumanDateLong($row->tanggal);
			// print_r($data['tanggal']);exit();
            $data['idpegawaiapoteker']=$row->idpegawaiapoteker;
            $data['totalbarang']=$row->totalbarang;
            $data['asalrujukan']=$row->asalrujukan;
            $data['statusresep']=$row->statusresep;
            $data['getIDpenjualan']=$idtest;
            $data['tab']='0';

            $data['iddokter']=$row->iddokter;
            $data['nopenjualan']=$row->nopenjualan;
            $data['listpasien1']=$this->tpaspen->getPasien($idtest)->result();
            $data['st_rujukan_non_rujukan']='R';
            $data['form_edit']=1;
			// print_r($row);exit();
            if ($data['iddokter']=='' || $data['iddokter']=='0') {
                $data['iddokter']=$this->tpaspen->get_dokter_default($id,$row->asalrujukan);
                // print_r($data['iddokter']);exit();
            }

            $data['idruangan']=$row->idruangan;
            $data['idkelas']=$row->idkelas;
            $data['idbed']=$row->idbed;

            $data['list_bed']      =$this->tpaspen->getBed($data['idkelas']);
        // $data['list_dokter']=$this->tpaspen->list_dokter();
        } else {
            $data['idpegawaiapoteker']=$this->tpaspen->get_apoteker();
            $data['st_rujukan_non_rujukan']='N';
			$data['tanggal']=date('d-m-Y H:i:s');
            $data['st_lock']=0;
            $data['totalbarang']=0;
            $data['statusresep']=1;
            $data['idkategori']=0;
            $data['idpasien']=0;
            $data['dokter_pegawai_id']=0;
            $data['nama']='';
            $data['asalrujukan']=0;
            $data['alamat']='';
            $data['telprumah']='';
            $data['no_medrec']='';
            $data['form_edit']=0;
            $data['tab']='1';
        }

        $data['list_tipe_barang']=$this->tpaspen->list_tipe_barang();
        $data['list_dokter']=$this->tpaspen->list_dokter();
        $data['list_non_racikan']=array();
        $data['list_racikan']=array();
        $data['id_penjualan']=$id;
        $data['no_urut']=0;
        $data['no_urut_akhir']=0;
        $data['total_non_racikan']=0;
        $data['status']=2;
        $data['tgl_lahir']='';
        $data['totalall']=0;
        $data['total_harga_racikan']=0;
        $data['list_racikan_tabel']=array();

        $data['idpendaftaran']=$this->tpaspen->getIdPendaftaran($id);

        // print_r($data['iddokter']);exit();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function edit($id=0,$disabel='0')
    {
        $data = array();
        $data['error']      = '';
        $data['title']      = 'Edit Penjualan';
        $data['content']    = 'Tpasien_penjualan/manage';
        $data['breadcrum']  = array(
                                  array("RSKB Halmahera",'#'),
                                  array("Farmasi",'#'),
                                  array("Edit Penjualan",'tpasien_penjualan/create')
                              );
		if ($disabel=='0'){
			$data['disabel']='';
		}else{
			$data['disabel']='disabled';
		}
        if (!empty($id)) {

            $idtest = $id;
            $row=$this->tpaspen->get_dataPenjualan($id);

			// print_r($row);exit();
			if ($row->asalrujukan=='0'){
				$data['st_lock']=$this->tpaspen->get_data_lock($id);
			}else{
				$data['st_lock']=$this->tpaspen->get_data_lock2($row->idtindakan,$row->asalrujukan);
			}


            $data['tanggal']=HumanDateLong($row->tanggal);
            $data['iddokter']=$row->iddokter;
            // print_r($data['iddokter']);exit();
            $data['idruangan']=$row->idruangan;
            $data['idkelas']=$row->idkelas;
            $data['idbed']=$row->idbed;
            // $data['list_bed']      =$this->Trawatinap_pendaftaran_model->getBed($data['idkelas']);
			$data['list_bed']      =$this->tpaspen->getBed($data['idkelas']);
            $data['list_dokter']=$this->tpaspen->list_dokter();
            $data['asalrujukan']=$row->asalrujukan;
            $data['statusresep']=$row->statusresep;
            $data['idkategori']=$row->idkategori;
            $data['idpasien']=$row->idpasien;
            $data['dokter_pegawai_id']=$row->idpasien;
            $data['nama']=$row->nama;
            $data['alamat']=$row->alamat;
            $data['telprumah']=$row->notelp;
            $data['no_medrec']=$row->nomedrec;
            $data['tgl_lahir']=$row->tgl_lahir;
            $data['wakturujukan']=$row->wakturujukan;
            $data['waktupenyerahan']=$row->waktupenyerahan;
            $data['totalbarang']=$row->totalbarang;
            // print_r($data['tgl_lahir']);exit();
            $data['idpegawaiapoteker']=$row->idpegawaiapoteker;
            $data['totalall']=$row->totalharga;
            // print_r($data['idpasien']);exit();
            // print_r($data);exit();
            $data['getIDpenjualan']=$idtest;
            $data['nopenjualan']=$row->nopenjualan;
            $data['listpasien1']=$this->tpaspen->getPasien($idtest)->result();

            // print_r(($data['listpasien1']));exit();

            $row_deni=(array) $data['listpasien1'][0];
            $data['id_penjualan']=$id;
            $data['form_edit']=1;
            $data['st_rujukan_non_rujukan']=$row_deni['st_rujukan_non_rujukan'];
            $data['status']=$row_deni['status'];
            $data['list_non_racikan']=$this->tpaspen->get_edit_list_non_racikan($id);
            // print_r($data['list_non_racikan']);exit();
            $data['total_non_racikan']=$this->tpaspen->total_non_racikan($id);
            $data['list_racikan']=$this->tpaspen->get_edit_list_racikan($id);
            $data['no_urut']=($this->tpaspen->no_urut_akhir($id));
            $data['no_urut']=$data['no_urut']+1;
            $data['no_urut_akhir']=$data['no_urut'];
            $data['list_racikan_tabel']=$this->tpaspen->get_edit_list_racikan_tabel($id);
            // print_r($data['list_racikan_tabel']);exit();
            $row_harga=$this->tpaspen->totalharga($id);
            if ($row_harga->total_harga_racikan) {
                $data['total_harga_racikan']=$row_harga->total_harga_racikan;
            } else {
                // $data['totalall']=0;
                $data['total_harga_racikan']=0;
            }
            // print_r($data['total_harga_racikan']);exit();
        // $data['totalharga']=$this->tpaspen->totalharga($id);
        }

        $data['idpendaftaran'] = $this->tpaspen->getIdPendaftaran($id);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function view()
    {
        $data = array();
        $data['error'] 			= '';
        $data['title'] 			= 'View Penjualan';
        $data['content'] 		= 'Tpasien_penjualan/view_detail';
        $data['breadcrum'] 	= array(
                                  array("RSKB Halmahera",'#'),
                                  array("Penjualan",'#'),
                                  array("View",'tpasien_penjualan/view_detail')
                              );

        if (!empty($_GET['id'])) {
            $idtest = decriptURL($_GET['id']);
            // print_r($idtest);exit();
            $data['getIDpenjualan']=$idtest;
            $data['listpasien1']=$this->tpaspen->getPasien($idtest)->result();
        }
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function save()
    {
        $total_barang=0;
        $idpost			= $this->input->post('id_penjualan');
        $form_edit			= $this->input->post('form_edit');
        // print_r($this->input->post());exit();
        $st_rujukan_non_rujukan	= $this->input->post('st_rujukan_non_rujukan');
        date_default_timezone_set('Asia/Jakarta');
        $setwaktu= $this->input->post('manage-input-tanggal');
        $now=date('Y-m-d H:i:s', strtotime($setwaktu));
        // print_r($this->input->post('manage-select-resep').' '.$this->input->post('dokter_pegawai_id'));exit();
        // if ($form_edit==0){
        // $jual['id']			= $idpost;
        // }
        if ($st_rujukan_non_rujukan=='N') {
            if ($form_edit==1) {
                $jual['id']			= $idpost;
            }
            $jual['tanggal']	    = YMDTimeFormat($this->input->post('manage-input-tanggal'));
            $jual['waktutindakan']	    = $now;
            $jual['nopenjualan']	= createKode('tpasien_penjualan', 'nopenjualan', 'FR');
            $jual['idtindakan']	= null;
            $jual['idkelompokpasien']	= $this->input->post('idkelompokpasien');
            $jual['idkategori']	= $this->input->post('idkategori');
            $jual['idpasien']		= $this->input->post('idpasien');
            $jual['nomedrec']	= $this->input->post('no_medrec');
            $jual['nama']         = $this->input->post('nama');
            $jual['alamat']       = $this->input->post('alamat');
            $jual['notelp']       = $this->input->post('telprumah');
            $jual['totalbarang']  = $this->input->post('totalbarang');
            $jual['st_rujukan_non_rujukan']   = $this->input->post('st_rujukan_non_rujukan');
            $jual['totalharga']   = $this->input->post('gt_all');
            $jual['waktupenyerahan']   = $this->input->post('waktupenyerahan');
            $jual['idpegawaiapoteker']   = $this->tpaspen->get_apoteker();

            $jual['statusresep']  = $this->input->post('manage-select-resep');
            $jual['tgl_lahir']  = $this->input->post('tgl_lahir');
            $jual['umurtahun']  = getUmur2($this->input->post('tgl_lahir'), 'y');
            $jual['umurbulan']  = getUmur2($this->input->post('tgl_lahir'), 'm');
            $jual['umurhari']  = getUmur2($this->input->post('tgl_lahir'), 'd');
            $jual['statuspasien']	= 0;
            $jual['status']       = 2;
        } else {
            $jual['tanggal']	    = YMDTimeFormat($this->input->post('manage-input-tanggal'));
            $jual['waktutindakan']	    = $now;

            $jual['totalbarang']  = $this->input->post('totalbarang');
            $jual['totalharga']   = $this->input->post('gt_all');
            $jual['iddokter']   = $this->input->post('iddokter');
            $jual['idkelas']   = $this->input->post('idkelas');
            $jual['idbed']   = $this->input->post('idbed');

            $jual['idpegawaiapoteker']   = $this->tpaspen->get_apoteker();
            $jual['status']       = 2;
        }
        $jual['date_tindakan']   = date('Y-m-d H:i:s');
        if ($form_edit==1) {
            $jual['status']   = $this->input->post('status');

            $jual['edited_date']   = date('Y-m-d H:i:s');
            $jual['edited_by']   =  $this->session->userdata('user_id');
            $jual['edited_nama']   =  $this->session->userdata('user_name');
        }else{
            $jual['created_date']   = date('Y-m-d H:i:s');
            $jual['created_by']   =  $this->session->userdata('user_id');
            $jual['created_nama']   =  $this->session->userdata('user_name');
        }
        if ($this->tpaspen->saveData($jual)) {
            $status = true;
            if (!empty($idpost)) {
                // jika ada
                $iDet=$idpost;
            } else {
                // jika tidak ada
                $iDet=$this->db->insert_id();
            }
            if ($form_edit==1) {
                $this->tpaspen->delete_non_racikan($idpost);
            }
            $detail_list_nonracikan = json_decode($this->input->post('pos_tabel_non_racikan'));
            // print_r($detail_list_nonracikan);exit();
            foreach ($detail_list_nonracikan as $DL):
                    $nonrac = array();
            $nonrac['idpenjualan']= $iDet;
            $nonrac['idbarang']= $DL[1];
            $nonrac['carapakai']= $DL[5];
            $nonrac['jenispenggunaan']= str_replace('<', '', substr($DL[6], 0, 3));
            $minum_obat=json_decode($this->input->post('pos_minum_obat'));
            $my_value = $DL[14];
            $filtered_array = array_filter($minum_obat, function ($var) use ($my_value) {
                return ($var[0] == $my_value);
            });
            // print_r(json_encode($filtered_array));exit();
            $nonrac['waktu_minum']= json_encode($filtered_array);

            $nonrac['kuantitas']= $DL[7];
            $nonrac['harga']= RemoveComma($DL[2]);
            $nonrac['diskon']= $DL[8];
            $nonrac['diskon_rp']= $DL[8]/100 * RemoveComma($DL[2]);
		    // $nonrac['diskon_rp']	= $DL[6]*RemoveComma($D[2]);
            $nonrac['tuslah']= RemoveComma($DL[9]);
            $nonrac['expire_date']= YMDFormat($DL[10]);
            $nonrac['totalharga']= RemoveComma($DL[12]);
            $nonrac['statusverifikasi']= RemoveComma($DL[15]);
            $nonrac['idtipe']= RemoveComma($DL[16]);
            $nonrac['status']= 1;
            $harga=$this->tpaspen->get_harga_obat($DL[1]);
            if ($harga) {
                $nonrac['harga_dasar']= $harga->hargadasar;
                if ($this->input->post('idkelompokpasien')=='1') {//ASURANSI
                    $nonrac['margin']= $harga->marginasuransi;
                } elseif ($this->input->post('idkelompokpasien')=='2') {//JASA RAHARJA
                    $nonrac['margin']= $harga->marginjasaraharja;
                } elseif ($this->input->post('idkelompokpasien')=='3') {//BPJS Kesehatan
                    $nonrac['margin']= $harga->marginbpjskesehatan;
                } elseif ($this->input->post('idkelompokpasien')=='4') {//BPJS Ketenagakerjaan
                    $nonrac['margin']= $harga->marginbpjstenagakerja;
                } elseif ($this->input->post('idkelompokpasien')=='5') {//UMUM
                    $nonrac['margin']= $harga->marginumum;
                }
            }
            $total_barang=$total_barang+1;
            // print_r($nonrac);exit();
            if ($this->tpaspen->saveDataNonracikan($nonrac)):
                            $status = true;
            endif;
            endforeach;
            //Insert Ke Racikan
            if ($form_edit==1) {
                $this->tpaspen->delete_racikan($idpost);
            }
            $TrowDetiD = $this->tpaspen->idracikan('tpasien_penjualan_racikan');
            $detail_list_racikan = json_decode($this->input->post('pos_tabel_racikan'));
            foreach ($detail_list_racikan as $row):
                    // $rac['id']=$TrowDetiD;
                    $total_barang=$total_barang+1;
            $rac['idpenjualan']= $iDet;
            $rac['namaracikan']= $row[1];
            $rac['jenisracikan']= $row[17];
            $rac['kuantitas']= $row[6];
            $rac['harga']= RemoveComma($row[15]);
            $rac['carapakai']= $row[4];
            // $rac['jenispenggunaan']= $row[5];
            $rac['jenispenggunaan']= str_replace('<', '', substr($row[5], 0, 3));
            $minum_racikan=json_decode($this->input->post('pos_minum_racikan'));
            $my_value =str_replace('tabel', '', $row[14]);
            $filtered_array = array_filter($minum_racikan, function ($var) use ($my_value) {
                return ($var[0] == $my_value);
            });
            // print_r($my_value);exit();
            $rac['waktu_minum']= json_encode($filtered_array);

            $rac['tuslah']= RemoveComma($row[7]);
            $rac['expire_date']= YMDFormat($row[8]);
            $rac['totalharga']= RemoveComma($row[11]);
            $rac['status']= 1;
            $rac['statusverifikasi']	= $row[18];

            if ($this->tpaspen->saveDataRacikan($rac)):
                        $iDet2=$this->db->insert_id();
            $detail_pos_tabel = json_decode($this->input->post('pos_tabel'.$row[0]));
            foreach ($detail_pos_tabel as $D):
                            $detOb=array();
            $detOb['idracikan']	= $iDet2;
            $detOb['idbarang']	= $D[1];
            $detOb['kuantitas']	= $D[5];
            $detOb['harga']		= RemoveComma($D[2]);
            $detOb['diskon']	= $D[6];
            $detOb['diskon_rp']= $D[6]/100 * RemoveComma($D[2]);
           
            $detOb['totalharga']	= $D[7];
            $detOb['idtipe']	= $D[11];
            $detOb['status']	= 1;
            $harga=$this->tpaspen->get_harga_obat($D[1]);
            if ($harga) {
                $detOb['harga_dasar']= $harga->hargadasar;
                if ($this->input->post('idkelompokpasien')=='1') {//ASURANSI
                    $detOb['margin']= $harga->marginasuransi;
                } elseif ($this->input->post('idkelompokpasien')=='2') {//JASA RAHARJA
                    $detOb['margin']= $harga->marginjasaraharja;
                } elseif ($this->input->post('idkelompokpasien')=='3') {//BPJS Kesehatan
                    $detOb['margin']= $harga->marginbpjskesehatan;
                } elseif ($this->input->post('idkelompokpasien')=='4') {//BPJS Ketenagakerjaan
                    $detOb['margin']= $harga->marginbpjstenagakerja;
                } elseif ($this->input->post('idkelompokpasien')=='5') {//UMUM
                    $detOb['margin']= $harga->marginumum;
                }
            }

            if ($this->tpaspen->saveDataRacikanObat($detOb)) {
                $status = true;
            }
            endforeach;
            endif;
            endforeach;
            $this->tpaspen->update_total_barang($iDet, $total_barang);
        } else {
            $status = false;
        }

        if ($status) {
            $_SESSION['status']='ToastrSukses("Data berhasil disimpan","Info")';
            if (!empty($idpost)) {
                redirect('tpasien_penjualan/index', 'location');
            } else {
                redirect('tpasien_penjualan/non_rujukan/1', 'location');
            }
        } else {
            $_SESSION['status']='Toastr("Data gagal disimpan","Info")';
            redirect('tpasien_penjualan/index', 'location');
        }
    }

    public function batalkan($id)
    {
        if ($this->tpaspen->batalkan($id)) {
            $_SESSION['status']='ToastrSukses("Pembatalan Berhasil","Info")';
            redirect('tpasien_penjualan/index', 'location');
        } else {
            $_SESSION['status']='Toastr("Pembatalan Gagal","Info")';
            redirect('tpasien_penjualan/index', 'location');
        }
    }

    public function serahkan($id)
    {
        if ($this->tpaspen->serahkan($id)) {
            $_SESSION['status']='ToastrSukses("Update Berhasil","Info")';
            redirect('tpasien_penjualan/index', 'location');
        } else {
            $_SESSION['status']='Toastr("Update Gagal","Info")';
            redirect('tpasien_penjualan/index', 'location');
        }
    }

    public function get_rujukan($uri='filter')
    {
        $this->select = array('tpasien_penjualan.*','trawatinap_pendaftaran.idtipe');

        $this->from   = 'tpasien_penjualan';

        $this->join 	=array(
          array('trawatinap_pendaftaran', 'tpasien_penjualan.idtindakan = trawatinap_pendaftaran.id AND tpasien_penjualan.asalrujukan = 3', 'LEFT')
        );

        // FILTER
        $this->where  = array('idtindakan <> '=>null);
        if ($uri == 'filter') {
            if ($this->session->userdata('namapasien') != null) {
                $this->where = array_merge($this->where, array('tpasien_penjualan.nama LIKE' => '%'.$this->session->userdata('namapasien').'%'));
            }
            if ($this->session->userdata('tanggaldari') != null) {
                $this->where = array_merge($this->where, array('DATE(tpasien_penjualan.tanggal) >= ' => YMDFormat($this->session->userdata('tanggaldari'))));
            }
            if ($this->session->userdata('tanggalsampai') != null) {
                $this->where = array_merge($this->where, array('DATE(tpasien_penjualan.tanggal) <= ' => YMDFormat($this->session->userdata('tanggalsampai'))));
            }
            if ($this->session->userdata('idasalpasien') != "0") {
                $this->where = array_merge($this->where, array('tpasien_penjualan.asalrujukan' => $this->session->userdata('idasalpasien')));
            }
            if ($this->session->userdata('idkelompokpasien') != "#") {
                $this->where = array_merge($this->where, array('tpasien_penjualan.idkelompokpasien' => $this->session->userdata('idkelompokpasien')));
            }
            if ($this->session->userdata('status') != "0") {
                $this->where = array_merge($this->where, array('tpasien_penjualan.status' => $this->session->userdata('status')));
            }
			if ($this->session->userdata('nopenjualan') != "") {
                $this->where = array_merge($this->where, array('tpasien_penjualan.nopenjualan' => $this->session->userdata('nopenjualan')));
            }
			if ($this->session->userdata('nomedrec') != "") {
                $this->where = array_merge($this->where, array('tpasien_penjualan.nomedrec' => $this->session->userdata('nomedrec')));
            }
        } else {
			$tgl_sampai=date('Y-m-d', strtotime("-1 week"));
			$this->where = array_merge($this->where, array('DATE(tpasien_penjualan.tanggal) >=' => YMDFormat($tgl_sampai)));
            $this->where = array_merge($this->where, array('DATE(tpasien_penjualan.tanggal) <=' => date("Y-m-d")));
        }

        $this->order  = array(
            'tpasien_penjualan.id' => 'DESC'
          );

        $this->group  = array();

        $this->column_search   = array('tpasien_penjualan.tanggal','tpasien_penjualan.nopenjualan','tpasien_penjualan.idpasien','tpasien_penjualan.nama','tpasien_penjualan.alamat','tpasien_penjualan.notelp');
        $this->column_order   = array('tpasien_penjualan.tanggal','tpasien_penjualan.nopenjualan','tpasien_penjualan.idpasien','tpasien_penjualan.nama','tpasien_penjualan.alamat','tpasien_penjualan.notelp');
        $list = $this->datatable->get_datatables();

        // print_r($this->db->last_query());exit();

		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $action = '';
            $closed = '';
            if ($r->status == 0) {
                $action = '<label>-</label>';
            } elseif ($r->status == 1) {
                $action .= '<div class="btn-group">';
                if (UserAccesForm($user_acces_form,array('1046'))){
                    $action .= '<a href="'.site_url().'tpasien_penjualan/create/'.$r->id.'" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Tindakan"><i class="fa fa-pencil"></i></a>';
                }
                if (UserAccesForm($user_acces_form,array('1208'))){
                    $action .= '<a href="'.site_url().'tpasien_penjualan/batalkan/'.$r->id.'" data-toggle="tooltip" title="Batalkan" class="btn btn-danger btn-sm" onclick="remove_row(this); return true;"><i class="fa fa-trash"></i></a>';
                }
                $action .= '</div>';
            } elseif ($r->status == 2) {
                $action .= '<div class="btn-group">';
                if (UserAccesForm($user_acces_form,array('1207'))){
                    $action .= '<a href="'.site_url().'tpasien_penjualan/serahkan/'.$r->id.'" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Serahkan" onclick="serahkan_row(this); return true;"><i class="fa fa-check"></i></a>';
                }
				if (UserAccesForm($user_acces_form,array('1209','1210','1211'))){
                $action .= '<div class="btn-group">';
                $action .= '<div class="btn-group dropup">';
                $action .= '<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"><span class="fa fa-print"></span></button>';
                $action .= '<ul class="dropdown-menu"><li>';
                if (UserAccesForm($user_acces_form,array('1209'))){
                    $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi/'.$r->id.'">Kwitansi</a>';
                }
               if (UserAccesForm($user_acces_form,array('1210'))){
                    $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi_small/'.$r->id.'">Kwitansi Farmasi</a>';
                }
                if (UserAccesForm($user_acces_form,array('1211'))){
                    $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/manage_print/'.$r->id.'">E-Ticket</a>';
                }
                $action .= '</li></ul>';
                $action .= '</div>';
                $action .= '</div>';
				}
                $action .= '&nbsp;';
                $action .= '<div class="btn-group">';
				if (UserAccesForm($user_acces_form,array('1206'))){
                    $action .= '<a href="'.site_url().'tpasien_penjualan/edit/'.$r->id.'" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>';
                }
				if (UserAccesForm($user_acces_form,array('1208'))){
					if ($r->closed_by_kasir =='0'){
                    $action .= '<a href="'.site_url().'tpasien_penjualan/batalkan/'.$r->id.'" data-toggle="tooltip" title="Batalkan" class="btn btn-danger btn-sm" onclick="remove_row(this); return true;"><i class="fa fa-trash"></i></a>';
					}
			   }
                $action .= '</div>';
            } else {
                if ($r->closed_by_kasir==0) {
                    $closed.='<div class="btn-group">';
                    if (UserAccesForm($user_acces_form,array('1206'))){
                        $closed.='<a href="'.site_url().'tpasien_penjualan/edit/'.$r->id.'" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>';
                    }
                    $closed.='</div>';
                } else {
                    $closed='';
                }
				if (UserAccesForm($user_acces_form,array('1209','1210','1211'))){
                $action .= '<div class="btn-group">';
                $action .= '<div class="btn-group dropup">';
                $action .= '<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"> <span class="fa fa-print"></span> </button>';
                $action .= '<ul class="dropdown-menu"> <li>';
                if (UserAccesForm($user_acces_form,array('1209'))){
                    $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi/'.$r->id.'">Kwitansi</a>';
                }
                if (UserAccesForm($user_acces_form,array('1210'))){
                    $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi_small/'.$r->id.'">Kwitansi Farmasi</a>';
                }
                if (UserAccesForm($user_acces_form,array('1211'))){
                    $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/manage_print/'.$r->id.'">E-Ticket</a>';
                }
                $action .= '</li></ul>';
                $action .= '</div>';
                $action .= '</div>';
				}
                $action .= $closed;
            }

            $row[] = $no;
            $row[] = $r->tanggal;
            $row[] = $r->nopenjualan;
            $row[] = $r->nomedrec;
            $row[] = $r->nama;

            if ($r->idtipe=='2') {
                $row[] ='One Day Surgery (ODS)';
            } else {
                $row[] = GetAsalRujukan($r->asalrujukan);
            }

            $row[] = StatusPembelianFarmasi($r->status);

            $row[] = $action;

            $data[] = $row;
        }
        $output = array(
              "draw" => $_POST['draw'],
              "recordsTotal" => $this->datatable->count_all(),
              "recordsFiltered" => $this->datatable->count_all(),
              "data" => $data
            );
        echo json_encode($output);
    }

    public function get_non_rujukan($uri='filternonrujukan')
    {
        $this->select = array('tpasien_penjualan.*','tkasir.status as st_kasir');

        $this->from   = 'tpasien_penjualan';
        $this->join 	= array(
			array('tkasir', 'tkasir.idtindakan = tpasien_penjualan.id AND tkasir.idtipe=3', 'LEFT')
		);

        // FILTER
        $this->where  = array('tpasien_penjualan.idtindakan' => null);
        if ($uri == 'filternonrujukan') {
            if ($this->session->userdata('namapasien') != null) {
                $this->where = array_merge($this->where, array('tpasien_penjualan.nama LIKE' => '%'.$this->session->userdata('namapasien').'%'));
            }
            if ($this->session->userdata('tanggaldari') != null) {
                $this->where = array_merge($this->where, array('DATE(tpasien_penjualan.tanggal) >= ' => YMDFormat($this->session->userdata('tanggaldari'))));
            }
            if ($this->session->userdata('tanggalsampai') != null) {
                $this->where = array_merge($this->where, array('DATE(tpasien_penjualan.tanggal) <= ' => YMDFormat($this->session->userdata('tanggalsampai'))));
            }
            if ($this->session->userdata('status') != "0") {
                $this->where = array_merge($this->where, array('tpasien_penjualan.status' => $this->session->userdata('status')));
            }
			if ($this->session->userdata('nopenjualan') != "") {
                $this->where = array_merge($this->where, array('tpasien_penjualan.nopenjualan' => $this->session->userdata('nopenjualan')));
            }
			if ($this->session->userdata('nomedrec') != "") {
                $this->where = array_merge($this->where, array('tpasien_penjualan.nomedrec' => $this->session->userdata('nomedrec')));
            }
        } else {
            // $this->where = array_merge($this->where, array('DATE(tpasien_penjualan.tanggal)' => date("Y-m-d")));
			$tgl_sampai=date('Y-m-d', strtotime("-1 week"));
			$this->where = array_merge($this->where, array('DATE(tpasien_penjualan.tanggal) >=' => YMDFormat($tgl_sampai)));
            $this->where = array_merge($this->where, array('DATE(tpasien_penjualan.tanggal) <=' => date("Y-m-d")));
        }

        $this->order  = array(
          'tpasien_penjualan.id' => 'DESC'
        );

        $this->group  = array();

        $this->column_search   = array('tanggal','nopenjualan','idpasien','nama','alamat','notelp');
        $this->column_order    = array('tanggal','nopenjualan','idpasien','nama','alamat','notelp');
        $list = $this->datatable->get_datatables();

        $data = array();
        $no = $_POST['start'];

		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

        foreach ($list as $r) {
            $no++;
            $row = array();

            $action = '';
            if ($r->status == 0) {
                $action = '<label>-</label>';
            } elseif ($r->status == 1) {
                $action .= '<div class="btn-group">';
				if (UserAccesForm($user_acces_form,array('1212'))){
                $action .= '<a href="'.site_url().'tpasien_penjualan/create/'.$r->id.'" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Tindakan"><i class="fa fa-pencil"></i></a>';
                }
				if (UserAccesForm($user_acces_form,array('1217'))){
				$action .= '<a href="'.site_url().'tpasien_penjualan/batalkan/'.$r->id.'" data-urlindex="'.site_url().'tpasien_penjualan" data-urlremove="'.site_url().'tpasien_penjualan/delete/'.$r->id.'" data-toggle="tooltip" title="Batalkan" class="btn btn-danger btn-sm removeData" onclick="remove_row(this); return false;"><i class="fa fa-trash"></i></a>';
                }
				$action .= '</div>';
            } elseif ($r->status == 2) {
                $action .= '<div class="btn-group">';
				if (UserAccesForm($user_acces_form,array('1213'))){
                $action .= '<a href="'.site_url().'tpasien_penjualan/serahkan/'.$r->id.'" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Serahkan"><i class="fa fa-check"></i></a>';
                }
				if (UserAccesForm($user_acces_form,array('1214','1215','1216'))){
				$action .= '<div class="btn-group">';
                $action .= '<div class="btn-group dropup">';
                $action .= '<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"> <span class="fa fa-print"></span> </button>';
                $action .= '<ul class="dropdown-menu"> <li>';
				if (UserAccesForm($user_acces_form,array('1214'))){
                $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi/'.$r->id.'">Kwitansi</a>';
				}
				if (UserAccesForm($user_acces_form,array('1215'))){
                $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi_small/'.$r->id.'">Kwitansi Farmasi</a>';
				}
				if (UserAccesForm($user_acces_form,array('1216'))){
                $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/manage_print/'.$r->id.'">E-Ticket</a>';
				}
                $action .= '</li></ul>';
                $action .= '</div>';
                $action .= '</div>';
                $action .= '</div>';
                $action .= '&nbsp;';
				}
                $action .= '<div class="btn-group">';
				if (UserAccesForm($user_acces_form,array('1212'))){
                $action .= '<a href="'.site_url().'tpasien_penjualan/edit/'.$r->id.'" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>';
                }
				if (UserAccesForm($user_acces_form,array('1217'))){
					if ($r->st_kasir !='2'){
						$action .= '<a href="'.site_url().'tpasien_penjualan/batalkan/'.$r->id.'" data-urlindex="'.site_url().'tpasien_penjualan" data-urlremove="'.site_url().'tpasien_penjualan/delete/'.$r->id.'" data-toggle="tooltip" title="Batalkan" class="btn btn-danger btn-sm removeData" onclick="remove_row(this); return false;"><i class="fa fa-trash"></i></a>';
					}
                }
				$action .= '</div>';
            } elseif ($r->status == 3) {
				if (UserAccesForm($user_acces_form,array('1214','1215','1216'))){
                $action .= '<div class="btn-group dropup">';
                $action .= '<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"> <span class="fa fa-print"></span> </button>';
                $action .= '<ul class="dropdown-menu"> <li>';
                if (UserAccesForm($user_acces_form,array('1214'))){
                $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi/'.$r->id.'">Kwitansi</a>';
				}
				if (UserAccesForm($user_acces_form,array('1215'))){
                $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/print_transaksi_small/'.$r->id.'">Kwitansi Farmasi</a>';
				}
				if (UserAccesForm($user_acces_form,array('1216'))){
                $action .= '<a tabindex="-1" target="_blank" href="'.site_url().'tpasien_penjualan/manage_print/'.$r->id.'">E-Ticket</a>';
				}
                $action .= '</li></ul>';
                $action .= '</div>';
                $action .= '</div>';
                $action .= '</div>';
				}
                $action .= '&nbsp;';
                if ($r->closed_by_kasir=='0') {
                    $action .= '<div class="btn-group">';
                    $action .= '<a href="'.site_url().'tpasien_penjualan/edit/'.$r->id.'" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>';
                }
                $action .= '</div>';
            } else {
                $action = '<label>-</label>';
            }

            $row[] = $no;
            $row[] = HumanDateLong($r->tanggal);
            $row[] = GetKategoriJualNonRujukan($r->idkategori);
            $row[] = $r->nopenjualan;
            // $row[] = $r->st_kasir;
            $row[] = $r->nama;

            $row[] = number_format($r->totalharga, 0);
            $row[] = StatusPembelianFarmasi($r->status);

            $row[] = $action;

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function get_obat_rujukan()
    {
        $iddariunit     = $this->input->post('idunit');
        $idtipe     	= $this->input->post('idtipe');
        $idkategori    = $this->input->post('idkategori');
        $iduser=$this->session->userdata('user_id');

        $where='';
        if ($idtipe  != '#') {
            $where .=" AND S.idtipe='$idtipe'";
        } else {
            $where .=" AND S.idtipe IN( SELECT H.idtipe from munitpelayanan_tipebarang H
										LEFT JOIN mdata_tipebarang T ON H.idtipe=T.id
										WHERE H.idunitpelayanan='1'
					)";
        }
        if ($idkategori  != '#') {
            $where .=" AND B.idkategori IN (
							SELECT id FROM (
									SELECT *FROM view_kategori_alkes
									UNION
									SELECT *FROM view_kategori_implan
									UNION
									SELECT *FROM view_kategori_obat
									UNION
									SELECT *FROM view_kategori_logistik
							) T1
							where T1.path like '$idkategori%'
			)";
        }
        $from="(SELECT S.idunitpelayanan,S.idtipe,S.idbarang as id,B.nama,B.idkategori,B.kode,B.namatipe,B.catatan,B.stokminimum,B.stokreorder,S.stok as stok_asli,msatuan.nama as satuan from mgudang_stok S
		INNER JOIN view_barang B ON B.idtipe=S.idtipe AND B.id=S.idbarang
		INNER JOIN msatuan ON msatuan.id=B.idsatuan
		WHERE S.idunitpelayanan='1' ".$where." ORDER BY B.idtipe,B.nama) as tbl";


        $this->select = array();
        $this->join 	= array();
        $this->where  = array();

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        // print_r($from);exit();
        $this->column_search   = array('kode','nama','satuan','stok_asli','catatan');
        $this->column_order    = array('kode','nama','satuan','stok_asli','catatan');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        // print_r($list);exit();
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->kode;
            $row[] = "<a href='#' class='selectObat' data-idobat='".$r->id."' data-idtipe='".$r->idtipe."' data-dismiss='modal'>".$r->nama."</span></a>";
            $row[] = $r->satuan;

            $row[] = WarningStok($r->stok_asli, $r->stokreorder, $r->stokminimum);
            $row[] = $r->catatan;
            $aksi       = '<div class="btn-group">';
            $aksi 		.= '<a target="_blank" href="'.site_url().'lgudang_stok/kartustok/'.$r->idtipe.'/'.$r->id.'/'.$r->idunitpelayanan.'" class="btn btn-xs btn-danger ks" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';
            $aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }
        $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->datatable->count_all(),
      "recordsFiltered" => $this->datatable->count_all(),
      "data" => $data
    );
        echo json_encode($output);
    }
    public function get_obat_rujukan2()
    {
        $iddariunit     = $this->input->post('idunit');
        $idtipe     	= $this->input->post('idtipe');
        $idkategori    = $this->input->post('idkategori');
        $iduser=$this->session->userdata('user_id');

        $where='';
        if ($idtipe  != '#') {
            $where .=" AND S.idtipe='$idtipe'";
        } else {
            $where .=" AND S.idtipe IN( SELECT H.idtipe from munitpelayanan_tipebarang H
										LEFT JOIN mdata_tipebarang T ON H.idtipe=T.id
										WHERE H.idunitpelayanan='1'
					)";
        }
        if ($idkategori  != '#') {
            $where .=" AND B.idkategori IN (
							SELECT id FROM (
									SELECT *FROM view_kategori_alkes
									UNION
									SELECT *FROM view_kategori_implan
									UNION
									SELECT *FROM view_kategori_obat
									UNION
									SELECT *FROM view_kategori_logistik
							) T1
							where T1.path like '$idkategori%'
			)";
        }
        $from="(SELECT S.idunitpelayanan,S.idtipe,S.idbarang as id,B.nama,B.idkategori,B.kode,B.namatipe,B.catatan,B.stokminimum,B.stokreorder,S.stok as stok_asli,msatuan.nama as satuan from mgudang_stok S
		INNER JOIN view_barang B ON B.idtipe=S.idtipe AND B.id=S.idbarang
		INNER JOIN msatuan ON msatuan.id=B.idsatuan
		WHERE S.idunitpelayanan='1' ".$where." ORDER BY B.idtipe,B.nama) as tbl";


        $this->select = array();
        $this->join 	= array();
        $this->where  = array();

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        // print_r($from);exit();
        $this->column_search   = array('kode','nama','satuan','stok_asli','catatan');
        $this->column_order    = array('kode','nama','satuan','stok_asli','catatan');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        // print_r($list);exit();
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->kode;
            $row[] = "<a href='#' class='selectObat2' data-idobat='".$r->id."' data-idtipe='".$r->idtipe."' data-dismiss='modal'>".$r->nama."</span></a>";
            $row[] = $r->satuan;

            $row[] = WarningStok($r->stok_asli, $r->stokreorder, $r->stokminimum);
            $row[] = $r->catatan;
            $aksi       = '<div class="btn-group">';
            $aksi 		.= '<a target="_blank" href="'.site_url().'lgudang_stok/kartustok/'.$r->idtipe.'/'.$r->id.'/'.$r->idunitpelayanan.'" class="btn btn-xs btn-danger ks" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';
            $aksi.='</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->datatable->count_all(),
      "recordsFiltered" => $this->datatable->count_all(),
      "data" => $data
    );
        echo json_encode($output);
    }
    public function get_obat()
    {
        $cari 	= $this->input->post('search');
        $data_obat = $this->tpaspen->get_obat($cari);
        $this->output->set_output(json_encode($data_obat));
    }
    public function get_dokter_pegawai()
    {
        $cari 	= $this->input->post('search');
        $kategori 	= $this->input->post('kategori');
        $data_obat = $this->tpaspen->get_dokter_pegawai($cari, $kategori);
        $this->output->set_output(json_encode($data_obat));
    }
    public function get_obat_detail($id, $kelompokpasien, $idtipe)
    {
        $data_obat = $this->tpaspen->get_obat_detail($id, $kelompokpasien, $idtipe);
        $this->output->set_output(json_encode($data_obat));
    }
    public function get_dokter_pegawai_detail($id, $kategori)
    {
        $dokter_pegawai = $this->tpaspen->get_dokter_pegawai_detail($id, $kategori);
        $this->output->set_output(json_encode($dokter_pegawai));
    }
    public function print_transaksi($id)
    {
        // instantiate and use the dompdf class
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);
        $data = array();
        $data['title'] 				= 'Dashboard';
        $data['error'] 		= '';
        $data['list_data'] 		= $this->tpaspen->list_data($id);
        $data['list_data_racikan'] 		= $this->tpaspen->list_data_racikan($id);
        $header 		= $this->tpaspen->get_header($id);
        if ($header) {
            $data['id']=$header->id;
            $data['nopenjualan']=$header->nopenjualan;
            $data['nomedrec']=$header->nomedrec;
            $data['nama']=$header->nama;
            $data['umur']=$header->umurtahun;
            $data['umur_bulan']=$header->umurbulan;
            $data['umur_hari']=$header->umurhari;
            $data['asalrujukan']=GetAsalRujukan($header->asalrujukan);
            $data['resep']=$header->statusresep;
            $data['totalharga']=$header->totalharga;
            $data['created_nama']=$header->created_nama;
            $data['edited_nama']=$header->edited_nama;
        }
        $html = $this->parser->parse('Tpasien_penjualan/kwitansi_besar', array_merge($data, backend_info()), true);
        $html = $this->parser->parse_string($html, $data);
        // print_r($header);exit();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Faktur .pdf', array("Attachment"=>0));
    }
    public function print_transaksi_small($id)
    {
        // instantiate and use the dompdf class
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);
        $data = array();
        $data['title'] 				= 'Dashboard';
        $data['error'] 		= '';
        $data['list_data'] 		= $this->tpaspen->list_data($id);
        $data['list_data_racikan'] 		= $this->tpaspen->list_data_racikan($id);
        $header 		= $this->tpaspen->get_header($id);
		// print_r($header);exit();
        if ($header) {
            $data['id']=$header->id;
            $data['nopenjualan']=$header->nopenjualan;
            $data['tanggal']=HumanDateLong($header->tanggal);
            $data['nomedrec']=$header->nomedrec;
            $data['nama']=$header->nama;
            $data['umur']=$header->umurtahun;
            $data['umur_bulan']=$header->umurbulan;
            $data['umur_hari']=$header->umurhari;
            $data['asalrujukan']=GetAsalRujukan($header->asalrujukan);
            $data['resep']=$header->statusresep;
            $data['totalharga']=$header->totalharga;
            $data['created_nama']=$header->created_nama;
            $data['edited_nama']=$header->edited_nama;
        }
        $html = $this->parser->parse('Tpasien_penjualan/kwitansi_kecil', array_merge($data, backend_info()), true);
        $html = $this->parser->parse_string($html, $data);
        // print_r($html);exit();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation

        // $customPaper = array(0,0,226,4000);
		 // $customPaper = array(0,0,226,4000);
        // $dompdf->set_paper($customPaper);
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Faktur .pdf', array("Attachment"=>0));
    }
    public function manage_print($id,$type_etiket='0')
    {
        $data = array();
        $data['error'] 			= '';
        $data['start_awal'] 	= 1;
        $data['idpenjualan'] 	= $id;
        $data['type_etiket'] 	= $type_etiket;
        $data['tab'] 			= 0;
        $data['title'] 			= 'Print E-Ticket';
        $data['content'] 		= 'Tpasien_penjualan/manage_print';
        $data['breadcrum'] 	= array(
                                                        array("RSKB Halmahera",'#'),
                                                        array("Farmasi",'tpasien_penjualan'),
                                                        array("Print",'tpasien_penjualan/manage_print')
                                                    );

        $data['list_data'] 				= $this->tpaspen->list_data($id);
        $data['list_data_racikan'] 		= $this->tpaspen->list_data_racikan($id);
        // print_r($data['list_data_racikan']);exit();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
    public function print_e_ticket()
    {
        $data = array();

        $idpenjualan=$this->input->post('idpenjualan');
        $type_etiket=$this->input->post('type_etiket');
        $chck_obat=$this->input->post('chck_obat');
        $idjual_obat=$this->input->post('idjual_obat');
        $where_obat=array();
        $i=0;
        if ($chck_obat) {
            foreach ($chck_obat as $index => $value) {
                if ($value==1) {
                    $where_obat[$i]=$idjual_obat[$index];
                    $i=$i+1;
                }
            }
        }
        $chck_racikan=$this->input->post('chck_racikan');
        $idjual_racikan=$this->input->post('idjual_racikan');
        $where_racikan=array();
        // print_r($chck_racikan);exit();
        $i=0;
        if ($chck_racikan) {
            foreach ($chck_racikan as $index => $value) {
                if ($value==1) {
                    $where_racikan[$i]=$idjual_racikan[$index];
                    $i=$i+1;
                }
            }
        }

        // $obat_kosong = set_obat_kosong();
        $obat_kosong = new stdClass();
        $obat_kosong->id = '';
        $obat_kosong->idpenjualan = '';
        $obat_kosong->namaracikan = '';
        $obat_kosong->carapakai = '';
        $obat_kosong->jenispenggunaan = '';
        $obat_kosong->kuantitas = '';
        $obat_kosong->harga = '';
        $obat_kosong->diskon = '';
        $obat_kosong->tuslah = '';
        $obat_kosong->totalharga = '';
        $obat_kosong->status = '';


        $data['start_awal']=$this->input->post('start_awal');
        $data['start_akhir']=$this->input->post('start_awal') + $this->input->post('counter')-1;

        $data_kosong = array();
        for ($i=$data['start_awal'];$i>1;$i--) {
            $data_kosong[] = $obat_kosong;
        }
        // print_r($chck_racikan);exit();
        if ($where_obat) {
            $data['list_data_ticket_obat']	 = $this->tpaspen->list_data_ticket_obat($where_obat, $idpenjualan);
        // $data['list_data_ticket_obat']	 = array_merge($data_kosong,$data['list_data_ticket_obat']);
        } else {
            $data['list_data_ticket_obat']	 = array();
        }
        // print_r($data['list_data_ticket_obat']);exit;
        if ($where_racikan) {
            $data['list_data_ticket_racikan']	 = $this->tpaspen->list_data_ticket_racikan($where_racikan, $idpenjualan);
        // print_r($data['list_data_ticket_racikan']);exit();
            // $data['list_data_ticket_racikan']	 = array_merge($data_kosong,$data['list_data_ticket_racikan']);
        } else {
            $data['list_data_ticket_racikan']=array();
        }

        $data['list_data_ticket_obat_all'] = array_merge($data_kosong, $data['list_data_ticket_obat'], $data['list_data_ticket_racikan']);

        // print_r("<pre>");
        // print_r($data['list_data_ticket_obat']);
        // print_r("<br/>");

        // print_r($data['list_data_ticket_racikan']);
        // print_r("</pre>");
        // exit();
        // $data_apoteker=$this->tpaspen->get_apoteker_by_penjualan($idpenjualan);
        $data_apoteker=$this->tpaspen->get_apoteker_by_setting();
		// print_r($data_apoteker);exit;
        if ($data_apoteker) {
            $data['sipa']=$data_apoteker->nip;
            $data['nama_apoteker']=$data_apoteker->nama;
        } else {
            $data['sipa']='';
            $data['nama_apoteker']='';
        }
		$data['type_etiket']=$type_etiket;
        // print_r($data);exit();
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);

        $html = $this->parser->parse('Tpasien_penjualan/print_sticker', array_merge($data, backend_info()), true);
        // print_r($html);exit();
        $html = $this->parser->parse_string($html, $data);

        $dompdf->loadHtml($html);
        // (Optional) Setup the paper size and orientation

        // $customPaper = array(0,0,462,600);
        // $dompdf->set_paper($customPaper, 'portrait');
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Faktur .pdf', array("Attachment"=>0));
    }
    public function getBed($idruangan, $idkelas, $id='')
    {
        $this->db->select("mbed.*");
        $this->db->where("idruangan", $idruangan);
        $this->db->where("idkelas", $idkelas);
        // $this->db->where_not_in("mbed.id", 'SELECT idbed FROM trawatinap_pendaftaran', FALSE);
        $query = $this->db->get("mbed");
        // if ($id!=''){
        // $q="SELECT M.id,M.nama,
        // (CASE WHEN T.id IS NULL then M.nama ELSE CONCAT(M.nama,' (NOT READY)') END) as namabed,
        // (CASE WHEN T.id IS NULL then M.id ELSE 0 END) as idbed from mbed M
        // LEFT JOIN trawatinap_pendaftaran T ON T.idbed=M.id AND T.statuscheckout='0' AND T.id != '$id'
        // WHERE M.status=1 AND M.idruangan='$idruangan' AND M.idkelas='$idkelas'";
        // }else{
        // $q="SELECT M.id,M.nama,
        // (CASE WHEN T.id IS NULL then M.nama ELSE CONCAT(M.nama,' (NOT READY)') END) as namabed,
        // (CASE WHEN T.id IS NULL then M.id ELSE 0 END) as idbed from mbed M
        // LEFT JOIN trawatinap_pendaftaran T ON T.idbed=M.id AND T.statuscheckout='0'
        // WHERE M.status=1 AND M.idruangan='$idruangan' AND M.idkelas='$idkelas'";
        // }

        // $query=$this->db->query($q);
        $this->output->set_output(json_encode($query->result()));
    }
}
