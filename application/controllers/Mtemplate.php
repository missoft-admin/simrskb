<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtemplate extends CI_Controller {

	
	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtemplate_model');
		$this->load->model('Antrian_layanan_model');
		$this->load->helper('path');
		
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1652'))){
			$data = array();
			$data['error'] 			= '';
			$data['title'] 			= 'Template';
			$data['content'] 		= 'Mtemplate/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Template",'#'),
												  array("List",'mtemplate')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create(){
		
		$data = array(
			'id'            => '',
			'nama' 					=> '',
		);

		$data['error'] 			= '';
		// $data['list_layanan'] 			= $this->Mtemplate_model->list_layanan('');
		// $data['list_sound'] 			= $this->Antrian_layanan_model->list_sound();
		// $data['list_user'] 			= $this->Mtemplate_model->list_user('');
		$data['title'] 			= 'Tambah Template';
		$data['content'] 		= 'Mtemplate/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Template",'#'),
								            array("Tambah",'mtemplate')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$data= $this->Mtemplate_model->getSpecified($id);
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Template';
			$data['content']    = 'Mtemplate/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Template",'#'),
										array("Ubah",'mtemplate')
										);

			// $data['statusAvailableApoteker'] = $this->Mtemplate_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtemplate');
		}
	}

	function delete($id){
		
		$result=$this->Mtemplate_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('mtemplate','location');
	}
	function aktifkan($id){
		
		$result=$this->Mtemplate_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){
		if($this->input->post('id') == '' ) {
			if($this->Mtemplate_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mtemplate/create','location');
			}
		} else {
			if($this->Mtemplate_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mtemplate/index','location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mtemplate/manage';

		if($id==''){
			$data['title'] = 'Tambah Template';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Template",'#'),
							               array("Tambah",'mtemplate')
								           );
		}else{
			$data['title'] = 'Ubah Template';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Template",'#'),
							               array("Ubah",'mtemplate')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$where='';
			
			$this->select = array();
			$from="
					(
						select H.*
						FROM mtemplate H 
						
						ORDER BY H.id
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
          $result[] = $r->nama;
         
          $aksi = '<div class="btn-group">';
			if (UserAccesForm($user_acces_form,array('1654'))){
			$aksi .= '<a href="'.site_url().'mtemplate/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
			}
			if (UserAccesForm($user_acces_form,array('1655'))){
			$aksi .= '<button title="Hapus" type="button" onclick="removeData('.$r->id.')" class="btn btn-danger btn-xs "><i class="fa fa-trash-o"></i></button>';
			}
		
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
	function load_data_template(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$template_id=$this->input->post('template_id');
		$q="
			SELECT *FROM (

			SELECT '0' as lev,H.id,CONCAT(LPAD(H.no_urut,3,'000'),H.id) as nourut,H.no_urut,H.nama_section as nama,'' as parent_id 
			FROM `mtemplate_section` H WHERE H.staktif='1' AND H.template_id='$template_id'
			
			UNION ALL

			SELECT '1' as lev,H.id,CONCAT(LPAD(S.no_urut,3,'000'),S.id,'-',LPAD(H.no_urut,3,'000')) as nourut,H.no_urut,H.nama_kategori as nama ,H.section_id as parent_id 
			FROM `mtemplate_kategori` H 
			INNER JOIN mtemplate_section S ON S.id=H.section_id
			WHERE H.staktif='1' AND H.template_id='$template_id'
			
			
			UNION ALL

			SELECT '2' as lev,H.id,CONCAT(LPAD(S.no_urut,3,'000'),S.id,'-',LPAD(K.no_urut,3,'000'),'-',LPAD(H.no_urut,3,'000')) as nourut,H.no_urut,H.nama_data as nama, '' as parent_id
			FROM `mtemplate_kategori_data` H 
			INNER JOIN mtemplate_section S ON S.id=H.section_id
			INNER JOIN mtemplate_kategori K ON K.id=H.kategori_id
			WHERE H.staktif='1' AND H.template_id='$template_id'


			) T ORDER BY T.nourut
		
		";
		$hasil=$this->db->query($q)->result();
		$tabel='';
		$no=0;
		foreach($hasil as $r){
			$no++;
			$btn_add='';
			$btn_action='';
			$warna_bg='#fff';
			
			if ($r->lev=='0'){
				$warna_bg="#8acfff";
				if (UserAccesForm($user_acces_form,array('1663'))){
				$btn_add ='&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-xs btn-success" type="button" onclick="add_kategori('.$r->id.')"><i class="fa fa-plus"> </i> Kategori</button>';		
				}				
				if (UserAccesForm($user_acces_form,array('1661'))){
				$btn_action .='<button class="btn btn-xs btn-success" type="button" title="Edit Section" onclick="edit_section('.$r->id.')"><i class="fa fa-pencil"> </i> </button>';
				}				
				if (UserAccesForm($user_acces_form,array('1662'))){
				$btn_action .='<button class="btn btn-xs btn-danger" type="button" title="Hapus Section" onclick="hapus_section('.$r->id.')"><i class="fa fa-trash"> </i> </button>';		
				}				
			}
			if ($r->lev=='1'){
				$warna_bg="#e3e3e3";
				if (UserAccesForm($user_acces_form,array('1666'))){
				$btn_add ='&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-xs btn-default" type="button" onclick="add_kategori_data('.$r->id.','.$r->parent_id.')"><i class="fa fa-plus"> </i> Detail</button>';		
				}				
				if (UserAccesForm($user_acces_form,array('1664'))){
				$btn_action .='<button class="btn btn-xs btn-success" type="button" title="Edit Kategori" onclick="edit_kategori('.$r->id.')"><i class="fa fa-pencil"> </i> </button>';		
				}				
				if (UserAccesForm($user_acces_form,array('1665'))){
				$btn_action .='<button class="btn btn-xs btn-danger" type="button" title="Hapus Kategori" onclick="hapus_kategori('.$r->id.')"><i class="fa fa-trash"> </i> </button>';		
				}				
			}
			if ($r->lev=='2'){
				$btn_add ='';		
				if (UserAccesForm($user_acces_form,array('1667'))){
				$btn_action .='<button class="btn btn-xs btn-success" type="button" title="Edit Data" onclick="edit_kategori_data('.$r->id.')"><i class="fa fa-pencil"> </i> </button>';		
				}				
				if (UserAccesForm($user_acces_form,array('1668'))){
				$btn_action .='<button class="btn btn-xs btn-danger" type="button" title="Hapus Data" onclick="hapus_kategori_data('.$r->id.')"><i class="fa fa-trash"> </i> </button>';		
				}				
			}
			$tabel .='<tr style="background-color:'.$warna_bg.'; font-weight: bold;">';
				$tabel .='<td>'.$r->no_urut.'</td>';
				$tabel .='<td>'.TreeView($r->lev,$r->nama).$btn_add.'</td>';
				$tabel .='<td>'.$btn_action.'</td>';
			$tabel .='</tr>';
		}
		// print_r($tabel);exit;
		$data['tabel']=$tabel;
		$this->output->set_output(json_encode($data));
		// echo json_encode($tabel);
	}
	function simpan_section(){
		$template_id=$this->input->post('template_id');
		$nama_section=$this->input->post('nama_section');
		$no_urut=$this->input->post('no_urut');
		$section_id=$this->input->post('section_id');
		$data=array(
			'template_id'=>$template_id,
			'nama_section'=>$nama_section,
			'no_urut'=>$no_urut,
		);
		if ($section_id==''){
			$hasil=$this->db->insert('mtemplate_section',$data);
		}else{
			$this->db->where('id',$section_id);
			$hasil=$this->db->update('mtemplate_section',$data);
		}
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		
		  
		  echo json_encode($hasil);
	}
	function get_data_section(){
		$section_id=$this->input->post('section_id');
		$q="SELECT '0' as lev,H.id,LPAD(H.no_urut,3,'000') as nourut,H.no_urut,H.nama_section as nama FROM `mtemplate_section` H WHERE H.staktif='1' AND H.id='$section_id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	
	function hapus_section(){
		$id=$this->input->post('id');
		$data['staktif']=0;
		$this->db->where('id',$id);
		$hasil=$this->db->update('mtemplate_section',$data);
		json_encode($hasil);
	}
	function simpan_kategori(){
		$template_id=$this->input->post('template_id');
		$section_id=$this->input->post('section_id');
		$nama_kategori=$this->input->post('nama_kategori');
		$no_urut=$this->input->post('no_urut');
		$kategori_id=$this->input->post('kategori_id');
		$data=array(
			'template_id'=>$template_id,
			'section_id'=>$section_id,
			'nama_kategori'=>$nama_kategori,
			'no_urut'=>$no_urut,
		);
		if ($kategori_id==''){
			$hasil=$this->db->insert('mtemplate_kategori',$data);
		}else{
			$this->db->where('id',$kategori_id);
			$hasil=$this->db->update('mtemplate_kategori',$data);
		}
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		
		  
		  echo json_encode($hasil);
	}
	function get_data_kategori(){
		$kategori_id=$this->input->post('kategori_id');
		$q="SELECT '0' as lev,H.id,LPAD(H.no_urut,3,'000') as nourut,H.no_urut,H.nama_kategori as nama,H.section_id FROM `mtemplate_kategori` H WHERE H.staktif='1' AND H.id='$kategori_id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	
	function hapus_kategori(){
		$id=$this->input->post('id');
		$data['staktif']=0;
		$this->db->where('id',$id);
		$hasil=$this->db->update('mtemplate_kategori',$data);
		json_encode($hasil);
	}
	//DATA
	function simpan_kategori_data(){
		$template_id=$this->input->post('template_id');
		$kategori_id=$this->input->post('kategori_id');
		$section_id=$this->input->post('section_id');
		$nama_kategori_data=$this->input->post('nama_kategori_data');
		$no_urut=$this->input->post('no_urut');
		$kategori_data_id=$this->input->post('kategori_data_id');
		$data=array(
			'template_id'=>$template_id,
			'section_id'=>$section_id,
			'kategori_id'=>$kategori_id,
			'nama_data'=>$nama_kategori_data,
			'no_urut'=>$no_urut,
		);
		if ($kategori_data_id==''){
			$hasil=$this->db->insert('mtemplate_kategori_data',$data);
		}else{
			$this->db->where('id',$kategori_data_id);
			$hasil=$this->db->update('mtemplate_kategori_data',$data);
		}
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		
		  
		  echo json_encode($hasil);
	}
	function get_data_kategori_data(){
		$kategori_data_id=$this->input->post('kategori_data_id');
		$q="SELECT '0' as lev,H.id,LPAD(H.no_urut,3,'000') as nourut,H.no_urut,H.nama_data as nama,H.section_id,H.kategori_id FROM `mtemplate_kategori_data` H WHERE H.staktif='1' AND H.id='$kategori_data_id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	
	function hapus_kategori_data(){
		$id=$this->input->post('id');
		$data['staktif']=0;
		$this->db->where('id',$id);
		$hasil=$this->db->update('mtemplate_kategori_data',$data);
		json_encode($hasil);
	}
}
