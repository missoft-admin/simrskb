<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mjenis_kas extends CI_Controller {

	/**
	 * Master Jenis Kas controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mjenis_kas_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Jenis Kas';
		$data['content'] 		= 'Mjenis_kas/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master Jenis Kas",'#'),
									    			array("List",'mjenis_kas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'deskripsi'			=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Jenis Kas';
		$data['content'] 		= 'Mjenis_kas/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master Jenis Kas",'#'),
									    			array("Tambah",'mjenis_kas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mjenis_kas_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'deskripsi' 		=> $row->deskripsi,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Jenis Kas';
				$data['content']	 	= 'Mjenis_kas/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Jenis Kas",'#'),
											    			array("Ubah",'mjenis_kas')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mjenis_kas','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mjenis_kas');
		}
	}

	function delete($id){
		$this->Mjenis_kas_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mjenis_kas','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		// print_r($this->input->post());exit();
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mjenis_kas_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mjenis_kas','location');
				}
			} else {
				if($this->Mjenis_kas_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mjenis_kas','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}
	
	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mjenis_kas/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Jenis Kas';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Jenis Kas",'#'),
															array("Tambah",'mjenis_kas')
													);
		}else{
			$data['title'] = 'Ubah Master Jenis Kas';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Jenis Kas",'#'),
															array("Ubah",'mjenis_kas')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$this->from   = 'mjenis_kas';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama');
      $this->column_order    = array('nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					// if (UserAccesForm($user_acces_form,array('131'))){
						$aksi .= '<a href="'.site_url().'mjenis_kas/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
					// }
					// if (UserAccesForm($user_acces_form,array('132'))){
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mjenis_kas" data-urlremove="'.site_url().'mjenis_kas/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
					// }
					
			        $aksi .= '</div>';

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = $r->deskripsi;
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }

}
