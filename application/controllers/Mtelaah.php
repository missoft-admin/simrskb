<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtelaah extends CI_Controller {

	/**
	 * Master Pengaturan Aspek Telaah controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtelaah_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Pengaturan Aspek Telaah';
		$data['content'] 		= 'Mtelaah/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Pengaturan Aspek Telaah",'#'),
									    			array("List",'mtelaah')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'nourut' 					=> '',
			
			'staktif' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Pengaturan Aspek Telaah';
		$data['content'] 		= 'Mtelaah/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Pengaturan Aspek Telaah",'#'),
									    			array("Tambah",'mtelaah')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mtelaah_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'nourut' 					=> $row->nourut,
					
					'staktif' 				=> $row->staktif
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Pengaturan Aspek Telaah';
				$data['content']	 	= 'Mtelaah/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Pengaturan Aspek Telaah",'#'),
											    			array("Ubah",'mtelaah')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtelaah','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtelaah');
		}
	}
	function setting_range_nilai($id){
		if($id != ''){
			$row = $this->Mtelaah_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'isi_header' 		=> $row->isi_header,
					'idtipe' 		=> $row->idtipe,
					'isi_footer' 		=> $row->isi_footer,
					'staktif' 				=> $row->staktif
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Pengaturan Aspek Telaah';
				$data['content']	 	= 'Mtelaah/manage_setting';
				$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Master Pengaturan Aspek Telaah",'#'),
									array("Ubah",'mtelaah')
									);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtelaah','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtelaah');
		}
	}

	function delete($id){
		$this->Mtelaah_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mtelaah','location');
	}

	function save(){
		$this->form_validation->set_rules('nourut', 'Nama', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->Mtelaah_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtelaah/update/'.$id,'location');
				}
			} else {
				if($this->Mtelaah_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtelaah/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mtelaah/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Pengaturan Aspek Telaah';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Pengaturan Aspek Telaah",'#'),
															array("Tambah",'mtelaah')
													);
		}else{
			$data['title'] = 'Ubah Master Pengaturan Aspek Telaah';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Pengaturan Aspek Telaah",'#'),
															array("Ubah",'mtelaah')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	function getIndex(){
		$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
		$btn_disabel_edit='';
		
		$this->select = array();
		$from="
			(
				SELECT *FROM (
				SELECT H.id,H.nourut as no_asli,LPAD(H.nourut,3,'0') as nourut, '0' as lev,H.nama,'' as nilai,'' as st_resep,'' as st_obat,H.id as mtelaah_id,'' as  opsi_default FROM mtelaah H
				WHERE H.staktif='1'
				UNION ALL
				SELECT D.id,D.nourut as no_asli,CONCAT(LPAD(H.nourut,3,'0'),'-',LPAD(D.nourut,3,'0')) as nourut, '1' as lev,D.nama_detail as nama,D.opsi_jawab as nilai,D.st_resep as st_resep,D.st_obat as st_obat,D.mtelaah_id 
				,D.opsi_default
				FROM  mtelaah H
				INNER JOIN mtelaah_detail D ON D.mtelaah_id=H.id
				WHERE H.staktif='1' AND D.staktif='1'
				) T ORDER BY T.nourut ASC
			) as tbl
		";
		$this->from   = $from;
		$this->join 	= array();$this->order  = array();$this->group  = array();
		$this->column_search = array('nama');$this->column_order  = array();
		$list = $this->datatable->get_datatables(true);
		 $arr_nilai=list_variable_ref(109);
      foreach ($list as $r) {
          $result = array();
		  $arr_jawaban=$arr=explode(',',$r->nilai);
		  $aksi='';
		  if ($r->lev=='0'){
			  $result[]=$r->no_asli;
			  $result[]=$r->nama;
			  $result[]='';
			  $result[]='';
			  $result[]='';
			  $result[]='';
		  }else{
			  $result[] = $this->opsi_nourut($r->no_asli).'<input class="id" type="hidden" value="'.$r->id.'"><input class="mtelaah_id" type="hidden" value="'.$r->mtelaah_id.'">';
			  $result[] = '<input type="text" class=" nama_detail" style="width:100%" value="'.$r->nama.'" required="" aria-required="true">';;
			  $result[] = $this->opsi_jawaban($arr_nilai,$arr_jawaban);
			  $result[] = $this->opsi_jawaban_default($arr_nilai,$r->opsi_default);
			  $input_resep='<label class="css-input css-checkbox css-checkbox-primary">';
			  $input_resep .='<input class="chk_resep" '.('1'==$r->st_resep?'checked':'').' type="checkbox"><span></span></label>';
			  $input_resep .='<input class="st_resep" type="hidden" value="'.$r->st_resep.'">';
			  $input_obat='<label class="css-input css-checkbox css-checkbox-primary">';
			  $input_obat .='<input class="chk_obat" '.('1'==$r->st_obat?'checked':'').' type="checkbox"><span></span></label>';
			  $input_obat .='<input class="st_obat" type="hidden" value="'.$r->st_obat.'">';
			  $result[] = $input_resep;
			  $result[] = $input_obat;
		  }
         
        
          $aksi = '<div class="btn-group">';
			if ($r->lev=='0'){
				if (UserAccesForm($user_acces_form,array('1829'))){
					$aksi .= '<a href="'.site_url().'mtelaah/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1830'))){
				$aksi .= '<button onclick="hapus_telaah_header('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
				}
			}else{
				if (UserAccesForm($user_acces_form,array('1830'))){
				$aksi .= '<button onclick="hapus_telaah('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
				}
			}
			
		  $aksi .= '</div>';
          $result[] = $aksi;
          $result[] = $r->lev;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
	}
	// function getIndex()
  // {
			// $data_user=get_acces();
			// $user_acces_form=$data_user['user_acces_form'];
			// $this->select = array();
			// $this->from   = 'mtelaah';
			// $this->join 	= array();
			// $this->where  = array(
				// 'staktif' => '1'
			// );
			// $this->order  = array(
				// 'nama' => 'ASC'
			// );
			// $this->group  = array();
		
		// $this->column_search   = array('nama');
      // $this->column_order    = array('nama');

      // $list = $this->datatable->get_datatables();
      // $data = array();
      // $no = $_POST['start'];
      // foreach ($list as $r) {
          // $no++;
          // $row = array();

          // $row[] = $no;
          // $row[] = $r->nama;
		// $aksi = '<div class="btn-group">';
       
		// if (UserAccesForm($user_acces_form,array('1829'))){
            // $aksi .= '<a href="'.site_url().'mtelaah/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
        // }
        // if (UserAccesForm($user_acces_form,array('1830'))){
            // $aksi .= '<a href="#" data-urlindex="'.site_url().'mtelaah" data-urlremove="'.site_url().'mtelaah/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
        // }
        // $aksi .= '</div>';
		// $row[] = $aksi;

          // $data[] = $row;
      // }
      // $output = array(
	      // "draw" => $_POST['draw'],
	      // "recordsTotal" => $this->datatable->count_all(),
	      // "recordsFiltered" => $this->datatable->count_all(),
	      // "data" => $data
      // );
      // echo json_encode($output);
  // }
  
	function simpan_telaah(){
		$mtelaah_id=$this->input->post('mtelaah_id');
		$telaah_id=$this->input->post('telaah_id');
		$nourut=$this->input->post('nourut');
		$opsi_jawab=$this->input->post('opsi_jawab');
		$opsi_default=$this->input->post('opsi_default');
		$opsi_jawab=implode(", ", $opsi_jawab);
		$data=array(
			'mtelaah_id'=>$this->input->post('mtelaah_id'),
			'nourut'=>$nourut,
			'nama_detail'=>$this->input->post('nama_detail'),
			'opsi_jawab'=>$opsi_jawab,
			'opsi_default'=>$opsi_default,
			'st_obat'=>$this->input->post('st_obat'),
			'st_resep'=>$this->input->post('st_resep'),
			'staktif'=>1,
		);
		if ($telaah_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mtelaah_detail',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$telaah_id);
		    $hasil=$this->db->update('mtelaah_detail',$data);
		}
		  
		  $this->output->set_output(json_encode($hasil));
	}
	
  function hapus_telaah(){
	  $telaah_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$telaah_id);
		$hasil=$this->db->update('mtelaah_detail',$data);
	  
	  $this->output->set_output(json_encode($hasil));
	  
  }
  function hapus_telaah_header(){
	  $id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$id);
		$hasil=$this->db->update('mtelaah',$data);
	  
	  $this->output->set_output(json_encode($hasil));
	  
  }
 
  function load_detail()
	{
		$mtelaah_id=$this->input->post('mtelaah_id');
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		$this->select = array();
		$from="
				(
					SELECT H.*
					FROM `mtelaah_detail` H
					where H.staktif='1' AND H.mtelaah_id='$mtelaah_id'
					ORDER BY H.nourut
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama_detail');
		$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
	  $arr_nilai=list_variable_ref(109);
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $arr_jawaban=$arr=explode(',',$r->opsi_jawab);
		  $aksi='';
          $result[] = $this->opsi_nourut($r->nourut).'<input class="id" type="hidden" value="'.$r->id.'">';
          $result[] = '<input type="text" class="form-control nama_detail" style="width:100%" value="'.$r->nama_detail.'" required="" aria-required="true">';;
          $result[] = $this->opsi_jawaban($arr_nilai,$arr_jawaban);
          $result[] = $this->opsi_jawaban_default($arr_nilai,$r->opsi_default);
		  $input_resep='<label class="css-input css-checkbox css-checkbox-primary">';
		  $input_resep .='<input class="chk_resep" '.('1'==$r->st_resep?'checked':'').' type="checkbox"><span></span></label>';
		  $input_resep .='<input class="st_resep" type="hidden" value="'.$r->st_resep.'">';
		  $input_obat='<label class="css-input css-checkbox css-checkbox-primary">';
		  $input_obat .='<input class="chk_obat" '.('1'==$r->st_obat?'checked':'').' type="checkbox"><span></span></label>';
		  $input_obat .='<input class="st_obat" type="hidden" value="'.$r->st_obat.'">';
          $result[] = $input_resep;
          $result[] = $input_obat;
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_telaah('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function opsi_nourut($id){
		$opsi='';
		$opsi .='<select tabindex="8" class="form-control opsi_change nourut" style="width: 100%;" data-placeholder="No">';
		$opsi .='	<option value="">Pilih No Urut</option>';
			for($i=1;$i<=100;$i++){
			$opsi .='<option value="'.$i.'" '.($id==$i?'selected':'').'>'.$i.'</option>';
			}
		$opsi .='</select>';
		return $opsi;
	}
	function opsi_jawaban($list_data,$arr_jawaban){
		$opsi='<select tabindex="8"  class="js-select2 form-control opsi_jawab opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>';
		foreach($list_data as $row){
			$opsi .='<option value="'.$row->id.'" '.(in_array($row->id,$arr_jawaban)?'selected':'').'>'.$row->nama.'</option>';
		}
		$opsi .='</select>';
		return $opsi;
	}
	function opsi_jawaban_default($list_data,$id){
		$opsi='<select tabindex="8"  class="js-select2 form-control opsi_default opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">';
		$opsi .='<option value="" '.(($id=='')?'selected':'').'></option>';
		foreach($list_data as $row){
			$opsi .='<option value="'.$row->id.'" '.(($row->id==$id)?'selected':'').'>'.$row->nama.'</option>';
		}
		$opsi .='</select>';
		return $opsi;
	}
}
