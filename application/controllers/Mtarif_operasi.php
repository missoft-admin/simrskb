<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mtarif_operasi extends CI_Controller
{

    /**
     * Tarif Operasi controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mtarif_operasi_model');
        $this->load->model('Mtarif_administrasi_model');
    }

    public function index($idtipe=1)
    {
        $data = array(
            'idtipe' => $idtipe,
            'idjenis' => '',
        );
        $data['list_jenis'] 			= $this->Mtarif_operasi_model->list_jenis();
        $data['error'] 			= '';
        $data['index_filter'] 			= 'index';
        $data['title'] 			= 'Tarif Operasi';
        $data['content'] 		= 'Mtarif_operasi/index';
        $data['breadcrum'] 	= array(
                                                        array("RSKB Halmahera",'#'),
                                                        array("Tarif Operasi",'mtarif_operasi/index'),
                                                        array("List",'#')
                                                    );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filter()
    {
        $data = array(
            'idtipe' => $this->input->post('idtipe'),
            'idjenis' => $this->input->post('idjenis'),
        );
        // print_r($data);exit();
        $data['list_jenis'] 			= $this->Mtarif_operasi_model->list_jenis();
        $this->session->set_userdata($data);

        $data['index_filter'] 			= 'filter';
        $data['error'] 			= '';
        $data['title'] 			= 'Tarif Operasi';
        $data['content'] 		= 'Mtarif_operasi/index';
        $data['breadcrum'] 	= array(
                                                        array("RSKB Halmahera",'#'),
                                                        array("Tarif Operasi",'mtarif_operasi/index'),
                                                        array("List",'#')
                                                    );
        // print_r($this->session->userdata('idjenis'));exit();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create($idtipe)
    {
        $data = array(
            'id' 			=> '',
            'idtipe' 	=> $idtipe,
            'idjenis' => '',
            'nama' 		=> '',
            'group_diskon_all' 		=> '',
            'status' 	=> ''
        );

        $data['error'] 			= '';
        $data['list_jenis'] 			= $this->Mtarif_operasi_model->list_jenis();
        $data['title'] 			= 'Tambah Tarif Operasi';
        $data['content'] 		= 'Mtarif_operasi/manage';
        $data['breadcrum']	= array(
                                                        array("RSKB Halmahera",'#'),
                                                        array("Tarif Operasi",'#'),
                                                    array("Tambah",'mtarif_operasi')
                                                    );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id)
    {
        if ($id != '') {
            $row = $this->Mtarif_operasi_model->getSpecified($id);
            if (isset($row->id)) {
                $data = array(
                    'id' 		  => $row->id,
                    'idtipe'  => $row->idtipe,
                    'idjenis' => $row->idjenis,
                    'nama' 	  => $row->nama,
                    'status'  => $row->status
                );
                $data['list_jenis'] 			= $this->Mtarif_operasi_model->list_jenis();
                $data['error'] 			= '';
                $data['title'] 			= 'Ubah Tarif Operasi';
                $data['content']	 	= 'Mtarif_operasi/manage';
                $data['breadcrum'] 	= array(
                                                                array("RSKB Halmahera",'#'),
                                                                array("Tarif Operasi",'#'),
                                                            array("Ubah",'mtarif_operasi')
                                                            );

                $data['list_tarif'] = $this->Mtarif_operasi_model->getAllTarif($row->id);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mtarif_operasi/index', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mtarif_operasi/index', 'location');
        }
    }

    public function delete($id)
    {
        $this->Mtarif_operasi_model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mtarif_operasi/index', 'location');
    }

    public function save()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');

        if ($this->form_validation->run() == true) {
            if ($this->input->post('id') == '') {
                if ($this->Mtarif_operasi_model->saveData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mtarif_operasi/index/'.$this->input->post('idtipe').'/'.$this->input->post('idjenis'), 'location');
                }
            } else {
                if ($this->Mtarif_operasi_model->updateData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mtarif_operasi/index/'.$this->input->post('idtipe').'/'.$this->input->post('idjenis'), 'location');
                }
            }
        } else {
            $this->failed_save($this->input->post('id'));
        }
    }

    public function failed_save($id)
    {
        $data = $this->input->post();
        $data['error'] 			 = validation_errors();
        $data['content'] 		 = 'Mtarif_operasi/manage';

        if ($id=='') {
            $data['title'] = 'Tambah Satuan';
            $data['breadcrum'] = array(
                                                            array("RSKB Halmahera",'#'),
                                                            array("Satuan",'#'),
                                                            array("Tambah",'mtarif_operasi')
                                                    );
        } else {
            $data['title'] = 'Ubah Satuan';
            $data['breadcrum'] = array(
                                                            array("RSKB Halmahera",'#'),
                                                            array("Satuan",'#'),
                                                            array("Ubah",'mtarif_operasi')
                                                    );
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function getIndex($uri)
    {
        // print_r($uri);exit();
        $data_user=get_acces();
        $user_acces_form=$data_user['user_acces_form'];

        $this->select = array();
        $this->from   = 'mtarif_operasi';
        $this->join 	= array();
        $this->where  = array();

        // FILTER
        if ($uri == 'filter') {
            if ($this->session->userdata('idtipe') != "#") {
                $this->where = array_merge($this->where, array('idtipe' => $this->session->userdata('idtipe')));
            }
            if ($this->session->userdata('idjenis') != "#") {
                $this->where = array_merge($this->where, array('idjenis' => $this->session->userdata('idjenis')));
            }
            $this->where = array_merge($this->where, array('status' => '1'));
        } else {
            $this->where = array_merge($this->where, array('status' => '1'));
        }

        $this->order  = array(
                'id' => 'DESC'
            );
        $this->group  = array();

        if (UserAccesForm($user_acces_form, array('225'))) {
            $this->column_search   = array('nama');
        } else {
            $this->column_search   = array();
        }
        $this->column_order    = array('nama');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->nama;
            $aksi = '<div class="btn-group">';
            if (UserAccesForm($user_acces_form, array('227'))) {
                $aksi .= '<a href="'.site_url().'mtarif_operasi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
            }
            if (UserAccesForm($user_acces_form, array('228'))) {
                $aksi .= '<a href="#" data-urlindex="'.site_url().'mtarif_operasi" data-urlremove="'.site_url().'mtarif_operasi/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
            }
            $aksi .= '<a href="'.site_url().'mtarif_operasi/setting/'.$r->id.'" data-toggle="tooltip" title="Setting" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>';
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->datatable->count_all(),
                "recordsFiltered" => $this->datatable->count_all(),
                "data" => $data
            );
        echo json_encode($output);
    }

    public function setting($id)
    {
        if ($id != '') {
            $row = $this->Mtarif_operasi_model->getSpecified($id);
            if (isset($row->id)) {
								$data = array(
										'id' 		  => $row->id,
										'idtipe'  => $row->idtipe,
										'idjenis' => $row->idjenis,
										'nama' 	  => $row->nama,
										'status'  => $row->status,
										'group_diskon_all'  => $row->group_diskon_all
								);

                $data['error']      = '';
                $data['title']      = 'Setting Group Pembayaran Tarif Operasi';
                $data['content']    = 'Mtarif_operasi/setting';
                $data['breadcrum']  = array(
	                  array("RSKB Halmahera",'#'),
	                  array("Tarif Operasi",'mtarif_operasi/index'),
	                  array("Setting",'mtarif_operasi')
                );

                $data['list_tarif'] = $this->Mtarif_operasi_model->getAllTarif($row->id);
                $data['list_group_pembayaran'] = $this->Mtarif_administrasi_model->getGroupPembayaran();

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mtarif_operasi/index/'.$uri, 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mtarif_operasi/index/'.$uri, 'location');
        }
    }

    public function save_setting()
    {
        if ($this->Mtarif_operasi_model->updateSetting()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('mtarif_operasi', 'location');
        }
    }
}
