<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpengaturan_form_order_radiologi_mri extends CI_Controller {

	/**
	 * Pengaturan Form Permintaan Radiologi MRI controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpengaturan_form_order_radiologi_mri_model');
		$this->load->helper('path');
  }

	function index()
	{
		$row = $this->Mpengaturan_form_order_radiologi_mri_model->getSpecified(1);
		if(isset($row->id)){
			$data = array(
				'id' => $row->id,
				'pesan_informasi_berhasil' => $row->pesan_informasi_berhasil,
				'label_judul' => $row->label_judul,
				'label_judul_eng' => $row->label_judul_eng,
				'label_tujuan_radiologi' => $row->label_tujuan_radiologi,
				'label_tujuan_radiologi_eng' => $row->label_tujuan_radiologi_eng,
				'required_tujuan_radiologi' => $row->required_tujuan_radiologi,
				'label_dokter_peminta_pemeriksaan' => $row->label_dokter_peminta_pemeriksaan,
				'label_dokter_peminta_pemeriksaan_eng' => $row->label_dokter_peminta_pemeriksaan_eng,
				'required_dokter_peminta_pemeriksaan' => $row->required_dokter_peminta_pemeriksaan,
				'label_diagnosa' => $row->label_diagnosa,
				'label_diagnosa_eng' => $row->label_diagnosa_eng,
				'required_diagnosa' => $row->required_diagnosa,
				'label_catatan_pemeriksaan' => $row->label_catatan_pemeriksaan,
				'label_catatan_pemeriksaan_eng' => $row->label_catatan_pemeriksaan_eng,
				'required_catatan_pemeriksaan' => $row->required_catatan_pemeriksaan,
				'label_waktu_pemeriksaan' => $row->label_waktu_pemeriksaan,
				'label_waktu_pemeriksaan_eng' => $row->label_waktu_pemeriksaan_eng,
				'required_waktu_pemeriksaan' => $row->required_waktu_pemeriksaan,
				'label_prioritas' => $row->label_prioritas,
				'label_prioritas_eng' => $row->label_prioritas_eng,
				'required_prioritas' => $row->required_prioritas,
				'label_pasien_puasa' => $row->label_pasien_puasa,
				'label_pasien_puasa_eng' => $row->label_pasien_puasa_eng,
				'required_pasien_puasa' => $row->required_pasien_puasa,
				'label_pengiriman_hasil' => $row->label_pengiriman_hasil,
				'label_pengiriman_hasil_eng' => $row->label_pengiriman_hasil_eng,
				'required_pengiriman_hasil' => $row->required_pengiriman_hasil,
				'label_alergi_bahan_kontras' => $row->label_alergi_bahan_kontras,
				'label_alergi_bahan_kontras_eng' => $row->label_alergi_bahan_kontras_eng,
				'required_alergi_bahan_kontras' => $row->required_alergi_bahan_kontras,
				'label_pasien_hamil' => $row->label_pasien_hamil,
				'label_pasien_hamil_eng' => $row->label_pasien_hamil_eng,
				'required_pasien_hamil' => $row->required_pasien_hamil,
				'label_catatan' => $row->label_catatan,
				'label_catatan_eng' => $row->label_catatan_eng,
				'required_catatan' => $row->required_catatan,
			);
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Pengaturan Form Permintaan Radiologi MRI';
			$data['content']	 	= 'Mpengaturan_form_order_radiologi_mri/index';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Pengaturan Form Permintaan Radiologi MRI",'#'),
															array("Ubah",'mpengaturan_form_order_radiologi_mri')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpengaturan_form_order_radiologi_mri/index','location');
		}
	}

	function save()
	{
		if($this->Mpengaturan_form_order_radiologi_mri_model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mpengaturan_form_order_radiologi_mri/index','location');
		}
	}
}
