<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';
use Dompdf\Dompdf;
use Dompdf\Options;

class Tkasir extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tkasir_model', 'tkasir_model');
		$this->load->model('Tpasien_penjualan_model', 'tpaspen');
	}

	public function index()
	{
		$data = [
			'namapasien' => '',
			'tanggaldari' => date('d/m/Y'),
			'tanggalsampai' => date('d/m/Y'),
			'idasalpasien' => 999,
			'idkelompokpasien' => 999,
			'status' => 999,
			'iddokter' => 999,
			'idpoliklinik' => 999,
			'idtipe' => 999,
			'noreg' => '',
			'nomedrec' => '',
		];
		$data['list_alasan'] = $this->tkasir_model->getAlasan();
		$data['tab'] = 0;
		$data['idalasan'] = '7';
		$data['error'] = '';
		$data['title'] = 'Kasir';
		$data['content'] = 'Tkasir/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Kasir', '#'],
			['List', 'tkasir']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter()
	{
		$data = [
			'namapasien' => $this->input->post('namapasien'),
			'tanggaldari' => $this->input->post('tanggaldari'),
			'tanggalsampai' => $this->input->post('tanggalsampai'),
			'idasalpasien' => $this->input->post('idasalpasien'),
			'idkelompokpasien' => $this->input->post('idkelompokpasien'),
			'iddokter' => $this->input->post('iddokter'),
			'idpoliklinik' => $this->input->post('idpoliklinik'),
			'status' => $this->input->post('status'),
			'idtipe' => $this->input->post('idtipe'),
			'noreg' => $this->input->post('noreg'),
			'nomedrec' => $this->input->post('nomedrec'),
		];

		$data['list_alasan'] = $this->tkasir_model->getAlasan();
		$data['idalasan'] = '7';
		$data['tab'] = 0;
		$data['error'] = '';
		$data['title'] = 'Kasir';
		$data['content'] = 'Tkasir/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Kasir', '#'],
			['List', 'tkasir']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function find_kontraktor($id, $jenis)
	{
		$arr['detail'] = $this->tkasir_model->get_nama_kontraktor($id, $jenis);
		$this->output->set_output(json_encode($arr));
	}

	public function transaksi($id = 0, $idkasir = 0, $form_edit = 0, $form_verif = 0)
	{
		$data = [];
		$data['id'] = $id;
		$data['idkasir'] = $idkasir;
		$data['form_edit'] = $form_edit;
		$data['form_verif'] = $form_verif;
		$data['diskon_rp'] = 0;
		$data['diskon_persen'] = 0;
		$data['bayar_rp'] = 0;
		$data['sisa_rp'] = 0;
		$list_kontraktor = [];
		$data_header = $this->tkasir_model->get_header_poli($id);
		if ($data_header) {
			$data['nama'] = $data_header->nama;
			$data['alamat'] = $data_header->alamat;
			$data['nomedrec'] = $data_header->no_medrec;
			$data['tanggal'] = $data_header->tanggaldaftar;
			$data['notrx'] = $data_header->nopendaftaran;
			$arr_asuransi_id = [];
			$arr_asuransi_nama = [];
			if (($data_header->idkelompokpasien) && $data_header->idkelompokpasien < 5) {
				$arr_asuransi_id[0] = $data_header->idkelompokpasien;
				$arr_asuransi_nama[0] = $data_header->kelompok;
			}
			if (($data_header->idkelompokpasien2) && $data_header->idkelompokpasien2 < 5) {
				$arr_asuransi_id[1] = $data_header->idkelompokpasien2;
				$arr_asuransi_nama[1] = $data_header->kelompok2;
			}
			$data['arr_asuransi_id'] = $arr_asuransi_id;
			$data['arr_asuransi_nama'] = $arr_asuransi_nama;
			$data['kelompok'] = $data_header->kelompok;
			$data['kelompok2'] = $data_header->kelompok2;
			$data['dokter'] = $data_header->dokter;
			$data['poli'] = $data_header->poli;
			$data['tarif_bpjs_kes'] = $data_header->tarif_bpjs_kes;
			$data['tarif_bpjs_kes2'] = $data_header->tarif_bpjs_kes2;
			$data['tarif_bpjs_ket'] = $data_header->tarif_bpjs_ket;
			$data['tarif_bpjs_ket2'] = $data_header->tarif_bpjs_ket2;
			$data['rekanan'] = $data_header->rekanan;
			$data['rekanan2'] = $data_header->rekanan2;
			$data['idkelompokpasien'] = $data_header->idkelompokpasien;
			$data['idkelompokpasien2'] = $data_header->idkelompokpasien2;
			$data['idjenispasien'] = $data_header->idjenispasien;
			$data['idtipe'] = $data_header->idtipe;
			$data['nohp'] = $data_header->nohp;
			$data['telepon'] = $data_header->telepon;
			$data['namapenanggungjawab'] = $data_header->namapenanggungjawab;
			$data['teleponpenanggungjawab'] = $data_header->teleponpenanggungjawab;
			$data['hubungan'] = $data_header->hubungan;
		}

		$data['list_bank'] = $this->tkasir_model->getBank();
		$data['list_tindakan'] = $this->tkasir_model->list_tindakan($id);
		$data['list_admin'] = $this->tkasir_model->list_admin($id);
		$data['list_tarif_admin'] = $this->tkasir_model->list_tarif_admin();
		$data['list_lab'] = $this->tkasir_model->list_lab($id);
		$data['list_radiologi'] = $this->tkasir_model->list_radiologi($id);
		$data['list_fisio'] = $this->tkasir_model->list_fisio($id);
		$data['list_farmasi_obat'] = $this->tkasir_model->list_farmasi($id, 3);
		$data['list_farmasi_racikan'] = $this->tkasir_model->list_farmasi_racikan($id);
		$data['list_farmasi_alkes'] = $this->tkasir_model->list_farmasi($id, 1);
		$data['list_farmasi_implan'] = $this->tkasir_model->list_farmasi($id, 2);
		$data['list_igd_obat'] = $this->tkasir_model->list_igd_obat($id);
		$data['list_igd_alkes'] = $this->tkasir_model->list_igd_alkes($id);

		if ($form_edit == '1') {
			$data['list_pembayaran'] = $this->tkasir_model->list_pembayaran($idkasir);
			$data_header = $this->tkasir_model->get_kasir($idkasir);
			if ($data_header) {
				$data['diskon_rp'] = $data_header->diskon;
				$data['bayar_rp'] = $data_header->total_bayar;
			}
		}

		$data['error'] = '';
		$data['title'] = 'Kasir Poliklinik';
		$data['content'] = 'Tkasir/manage';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Kasir', '#'],
			['Kasir Poliklinik', 'tkasir/transaksi']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function transaksi_ob($id = 0, $idkasir = 0, $form_edit = 0)
	{
		$data = [];
		$data['error'] = '';
		$data['title'] = 'Kasir Obat Bebas';
		$data['bayar_rp'] = '0';
		$data['idkasir'] = $idkasir;
		$data['sisa_rp'] = '0';
		$data['gt_rp'] = '0';
		$data['diskon_rp'] = '0';
		$data['diskon_persen'] = '0';
		$data['form_edit'] = $form_edit;

		if (!empty($id)) {
			$idtest = $id;
			$row = $this->tpaspen->get_dataPenjualan($id);

			$data['tgl_trx'] = $row->tanggal;
			$data['asalrujukan'] = $row->asalrujukan;
			$data['statusresep'] = $row->statusresep;
			$data['idkategori'] = $row->idkategori;
			$data['idpasien'] = $row->idpasien;
			$data['dokter_pegawai_id'] = $row->idpasien;
			$data['nama'] = $row->nama;
			$data['alamat'] = $row->alamat;
			$data['telprumah'] = $row->notelp;
			$data['no_medrec'] = $row->nomedrec;
			$data['tgl_lahir'] = $row->tgl_lahir;
			$data['wakturujukan'] = $row->wakturujukan;
			$data['waktupenyerahan'] = $row->waktupenyerahan;
			$data['totalbarang'] = $row->totalbarang;
			$data['idpegawaiapoteker'] = $row->idpegawaiapoteker;
			$data['totalall'] = $row->totalharga;
			$data['getIDpenjualan'] = $idtest;
			$data['nopenjualan'] = $row->nopenjualan;
			$data['listpasien1'] = $this->tpaspen->getPasien($idtest)->result();

			$row_deni = (array) $data['listpasien1'][0];

			$data['id_penjualan'] = $id;
			$data['st_rujukan_non_rujukan'] = $row_deni['st_rujukan_non_rujukan'];
			$data['status'] = $row_deni['status'];
			$data['list_non_racikan'] = $this->tpaspen->get_edit_list_non_racikan($id);
			$data['total_non_racikan'] = $this->tpaspen->total_non_racikan($id);
			$data['list_racikan'] = $this->tpaspen->get_edit_list_racikan($id);
			$data['no_urut'] = $this->tpaspen->no_urut_akhir($id);
			$data['no_urut'] = $data['no_urut'] + 1;
			$data['no_urut_akhir'] = $data['no_urut'];
			$data['list_racikan_tabel'] = $this->tpaspen->get_edit_list_racikan_tabel($id);

			$row_harga = $this->tpaspen->totalharga($id);
			if ($row_harga->total_harga_racikan) {
				$data['total_harga_racikan'] = $row_harga->total_harga_racikan;
			} else {
				$data['total_harga_racikan'] = 0;
			}
		}

		$data['arr_asuransi_id'] = $this->tkasir_model->get_tipe_kontraktor();
		if ($form_edit == '1') {
			$data['list_pembayaran'] = $this->tkasir_model->list_pembayaran($idkasir);
			$data_header = $this->tkasir_model->get_kasir($idkasir);
			if ($data_header) {
				$data['diskon_rp'] = $data_header->diskon;
				$data['bayar_rp'] = $data_header->total_bayar;
			}
		}

		$data['content'] = 'Tkasir/manage_ob';
		$data['list_bank'] = $this->tkasir_model->getBank();
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Kasir', '#'],
			['Obat Bebas', 'tkasir/create']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function print_transaksi($id = 'N', $idkasir = '0', $st_cetak = '0')
	{
		$data['id'] = $id;
		$data['idkasir'] = $idkasir;
		$data['st_cetak'] = $st_cetak;
		$data['base_url'] = $this->config->base_url();
		if ($st_cetak == '1') {
			$this->tkasir_model->update_cetak($idkasir, '1');
		}
		$data = array_merge($data, backend_info());
		$this->load->view('Tkasir/print_nota_rincian', $data);
	}

	public function print_rincian_biaya($idpendaftaran)
	{
		$dompdf = new Dompdf();

		$radiologi = $this->db->query("SELECT
        	SUM(CASE WHEN trujukan_radiologi_detail.idtipe = 1 THEN trujukan_radiologi_detail.totalkeseluruhan ELSE 0 END) AS xray,
        	SUM(CASE WHEN trujukan_radiologi_detail.idtipe = 2 THEN trujukan_radiologi_detail.totalkeseluruhan ELSE 0 END) AS usg,
        	SUM(CASE WHEN trujukan_radiologi_detail.idtipe = 5 THEN trujukan_radiologi_detail.totalkeseluruhan ELSE 0 END) AS bmd
        FROM
        	tpoliklinik_pendaftaran
        JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
        JOIN trujukan_radiologi ON tpoliklinik_tindakan.id = trujukan_radiologi.idtindakan AND trujukan_radiologi.asalrujukan IN ( 1, 2 )
        JOIN trujukan_radiologi_detail ON trujukan_radiologi_detail.idrujukan = trujukan_radiologi.id
        JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
        WHERE tpoliklinik_pendaftaran.id = '" . $idpendaftaran . "' AND trujukan_radiologi.status <> 0");

		$fisioterapi = $this->db->query("SELECT
        	SUM(trujukan_fisioterapi_detail.totalkeseluruhan) AS totalkeseluruhan
        FROM
        	tpoliklinik_pendaftaran
        JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
        JOIN trujukan_fisioterapi ON tpoliklinik_tindakan.id = trujukan_fisioterapi.idtindakan AND trujukan_fisioterapi.asalrujukan IN ( 1, 2 )
        JOIN trujukan_fisioterapi_detail ON trujukan_fisioterapi_detail.idrujukan = trujukan_fisioterapi.id
        JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
        WHERE tpoliklinik_pendaftaran.id = '" . $idpendaftaran . "' AND trujukan_fisioterapi.status <> 0");

		$laboratorium = $this->db->query("SELECT
        	SUM(trujukan_laboratorium_detail.totalkeseluruhan) AS totalkeseluruhan
        FROM
        	tpoliklinik_pendaftaran
        JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
        JOIN trujukan_laboratorium ON tpoliklinik_tindakan.id = trujukan_laboratorium.idtindakan AND trujukan_laboratorium.asalrujukan IN ( 1, 2 )
        JOIN trujukan_laboratorium_detail ON trujukan_laboratorium_detail.idrujukan = trujukan_laboratorium.id
        JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
        WHERE tpoliklinik_pendaftaran.id = '" . $idpendaftaran . "' AND trujukan_laboratorium.status <> 0");

		$farmasi = $this->db->query("SELECT
        	SUM( view_union_farmasi.totalharga ) AS totalkeseluruhan
        FROM
        	view_union_farmasi
        	JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = view_union_farmasi.idtindakan
        	AND view_union_farmasi.asalrujukan IN ( 1, 2 )
        	JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
        	JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id
        	AND tkasir.idtipe IN ( 1, 2 )
        WHERE tpoliklinik_pendaftaran.id = '" . $idpendaftaran . "' AND view_union_farmasi.STATUS <> 0");

		$administrasi = $this->db->query("SELECT
        	SUM( totalkeseluruhan ) AS totalkeseluruhan
        FROM
        	tpoliklinik_administrasi
        WHERE idpendaftaran = '" . $idpendaftaran . "' AND status <> 0");

		$alkes_poliklinik = $this->db->query("SELECT
        	SUM(tpoliklinik_alkes.totalkeseluruhan) AS totalkeseluruhan
        FROM
        	tpoliklinik_alkes
        LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpoliklinik_alkes.idtindakan
        LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
        JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
        WHERE
        	tpoliklinik_pendaftaran.id = '" . $idpendaftaran . "' AND tpoliklinik_alkes.status = 1");

		$tindakan_rajal = $this->db->query("SELECT
        	SUM(CASE WHEN mdokter.idkategori IN (2,3,4,5) THEN tpoliklinik_pelayanan.jasapelayanan ELSE 0 END) AS jasa_dokter_spesialis,
        	SUM(CASE WHEN mdokter.idkategori = 1 THEN tpoliklinik_pelayanan.jasapelayanan ELSE 0 END) AS jasa_dokter_umum,
        	SUM(tpoliklinik_pelayanan.biayaperawatan) AS jasa_keperawatan,
        	SUM(tpoliklinik_pelayanan.jasasarana+tpoliklinik_pelayanan.bhp) AS tindakan_rajal
        FROM
        	tpoliklinik_pelayanan
        LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpoliklinik_pelayanan.idtindakan
        LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
        LEFT JOIN mdokter ON mdokter.id = tpoliklinik_pelayanan.iddokter
        WHERE
        	tpoliklinik_pendaftaran.id = '" . $idpendaftaran . "' AND tpoliklinik_pelayanan.status = 1");

		$rajal_obat = $this->db->query("SELECT
        	SUM(tpoliklinik_obat.totalkeseluruhan) AS totalkeseluruhan
        FROM
        	tpoliklinik_obat
        LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpoliklinik_obat.idtindakan
        LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
        WHERE
        	tpoliklinik_pendaftaran.id = '" . $idpendaftaran . "' AND tpoliklinik_obat.status = 1");

		$pembayaran = $this->db->query("SELECT
        	SUM(CASE WHEN tkasir_pembayaran.idmetode IN (1, 2, 3, 4) THEN tkasir_pembayaran.nominal ELSE 0 END) AS jumlah_ditanggung_pasien,
        	SUM(CASE WHEN tkasir_pembayaran.idmetode = 7 THEN tkasir_pembayaran.nominal ELSE 0 END) AS jumlah_tagihan
        FROM
        	tkasir_pembayaran
        JOIN tkasir ON tkasir.id = tkasir_pembayaran.idkasir
        JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tkasir.idtindakan
        JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
        WHERE tpoliklinik_pendaftaran.id = '" . $idpendaftaran . "' AND tkasir.status = 2");

		$jumlah = $tindakan_rajal->row()->jasa_dokter_spesialis + $tindakan_rajal->row()->jasa_dokter_umum + $alkes_poliklinik->row()->totalkeseluruhan + $tindakan_rajal->row()->tindakan_rajal +
				  $radiologi->row()->xray + $fisioterapi->row()->totalkeseluruhan + $radiologi->row()->bmd + $radiologi->row()->usg + $laboratorium->row()->totalkeseluruhan + $farmasi->row()->totalkeseluruhan +
				  $rajal_obat->row()->totalkeseluruhan + $administrasi->row()->totalkeseluruhan + $tindakan_rajal->row()->jasa_keperawatan;

		$row = $this->tkasir_model->get_header_poli($idpendaftaran);
		$data = [
			'nopendaftaran' => $row->nopendaftaran,
			'tanggaldaftar' => HumanDateShort($row->tanggal),
			'nomedrec' => $row->no_medrec,
			'namapasien' => $row->nama,
			'idkelompokpasien' => $row->idkelompokpasien,
			'namakelompok' => $row->kelompok,
			'namadokter' => $row->dokter,
			'namarekanan' => $row->rekanan,
			'jasa_dokter_spesialis' => number_format($tindakan_rajal->row()->jasa_dokter_spesialis),
			'jasa_dokter_umum' => number_format($tindakan_rajal->row()->jasa_dokter_umum),
			'alkes_poliklinik' => number_format($alkes_poliklinik->row()->totalkeseluruhan),
			'tindakan_rajal' => number_format($tindakan_rajal->row()->tindakan_rajal),
			'radiologi' => number_format($radiologi->row()->xray),
			'fisioterapi' => number_format($fisioterapi->row()->totalkeseluruhan),
			'bmd' => number_format($radiologi->row()->bmd),
			'usg' => number_format($radiologi->row()->usg),
			'laboratorium' => number_format($laboratorium->row()->totalkeseluruhan),
			'farmasi' => number_format($farmasi->row()->totalkeseluruhan + $rajal_obat->row()->totalkeseluruhan),
			'administrasi' => number_format($administrasi->row()->totalkeseluruhan),
			'jasa_keperawatan' => number_format($tindakan_rajal->row()->jasa_keperawatan),
			'jumlah' => number_format($jumlah),
			'jumlah_ditanggung_pasien' => number_format($pembayaran->row()->jumlah_ditanggung_pasien),
			'jumlah_tagihan' => number_format($pembayaran->row()->jumlah_tagihan),
		];

		$html = $this->load->view('Tkasir/print/print_rincian_biaya', $data, true);

		$dompdf->loadHtml($html);
		$dompdf->setPaper('A4', 'portrait');

		$dompdf->render();
		$dompdf->stream('Rincian Biaya.pdf', ['Attachment' => 0]);
	}

	public function get_print_transaksi($id, $idkasir)
	{
		$options = new Options();
		$options->set('isRemoteEnabled', true);
		$options->set('enable-javascript', true);
		$options->set('javascript-delay', 13500);
		$options->set('enable-smart-shrinking', true);
		$options->set('no-stop-slow-scripts', true);

		$dompdf = new Dompdf($options);

		$data = [];
		if ($id == 'N') {
			$row = $this->tkasir_model->getHeaderInfoFaktur_NR($idkasir);
			$data['namapoli'] = 'NON RUJUKAN';
			$data['pelayanan'] = 'PENJUALAN OBAT BEBAS';
			$data['nokasir'] = $row->nokasir;
			$data['nomedrec'] = $row->nomedrec;
			$data['tanggal'] = $row->tanggal;
			$data['namapasien'] = $row->nama;
			$data['alamat'] = $row->alamat;
			$data['namadokter'] = '';

			$data['payor'] = '';
			$data['list_non_racikan'] = $this->tkasir_model->list_non_racikan($idkasir);

			$data['list_racikan'] = $this->tkasir_model->list_racikan($idkasir);
		} else {
			$row = $this->tkasir_model->getHeaderInfoFaktur($id);
			if ($row) {
				$data['namapoli'] = $row->namapoli;
				$data['pelayanan'] = GetAsalRujukan($row->idtipe);
				$data['nokasir'] = $row->nopendaftaran;
				$data['nomedrec'] = $row->nomedrec;
				// $data['tanggal'] = $row->tanggal_kasir;
				$data['tanggal'] = $row->tanggal_kasir;
				$data['namapasien'] = $row->namapasien;
				$data['alamat'] = $row->alamatpasien;
				$data['namadokter'] = $row->namadokter;
				if ($row->idkelompokpasien == '1') {
					$data['payor'] = $row->namarekanan;
				} elseif ($row->idkelompokpasien == '2') {
					$data['payor'] = 'JASA RAHARJA';
				} elseif ($row->idkelompokpasien == '3') {
					$data['payor'] = 'BPJS Kesehatan';
				} elseif ($row->idkelompokpasien == '4') {
					$data['payor'] = 'BPJS Ketenagakerjaan';
				} elseif ($row->idkelompokpasien == '5') {
					$data['payor'] = 'UMUM';
				} elseif ($row->idkelompokpasien == '6') {
					$data['payor'] = 'Baru';
				} else {
					$data['payor'] = '';
				}
			}
			$data['list_farmasi_racikan'] = $this->tkasir_model->list_farmasi_racikan($id);
		}

		$data['list_bank'] = $this->tkasir_model->getBank();
		$data['list_admin'] = $this->tkasir_model->list_admin($id);
		$data['list_tindakan'] = $this->tkasir_model->list_tindakan($id);
		$data['list_lab'] = $this->tkasir_model->list_lab($id);
		$data['list_radiologi'] = $this->tkasir_model->list_radiologi($id);
		$data['list_fisio'] = $this->tkasir_model->list_fisio($id);
		$data['list_farmasi_obat'] = $this->tkasir_model->list_farmasi($id, 3);
		$data['list_farmasi_alkes'] = $this->tkasir_model->list_farmasi($id, 1);
		$data['list_farmasi_implan'] = $this->tkasir_model->list_farmasi($id, 2);
		$data['list_igd_obat'] = $this->tkasir_model->list_igd_obat($id);
		$data['list_igd_alkes'] = $this->tkasir_model->list_igd_alkes($id);
		$data['list_bayar'] = $this->tkasir_model->list_bayar($idkasir);

		$row = $this->tkasir_model->get_data_kasir($idkasir);
		$data['nama_kasir'] = $row->namakasir;
		$data['kembalian'] = $row->kembalian;
		$data['user_cetak'] = $row->namacetak;
		$data['jml_cetak'] = $row->totalcetakfaktur;
		$data['tgl_cetak'] = $row->tgl_cetak;
		$data['jenis_id'] = $id;

		$html = $this->load->view('Tkasir/nota_rincian', $data, true);
		$dompdf->loadHtml($html);
		$dompdf->setPaper('A4', 'portrait');
		$dompdf->render();
		$dompdf->stream('Faktur Transaksi Kasir.pdf', ['Attachment' => 0]);
	}

	public function save()
	{
		$idkasir = $this->input->post('idkasir');
		$pos_tabel_pembayaran = json_decode($this->input->post('pos_tabel_pembayaran'));

		$this->tkasir_model->UpdateTkasir($idkasir, 'ob');
		$this->tkasir_model->DeletePembayaran($idkasir);

		foreach ($pos_tabel_pembayaran as $row) {
			$detBayar = [];

			$detBayar['idkasir'] = $idkasir;
			$detBayar['idtipe'] = 1;
			$detBayar['idmetode'] = $row[7];
			if ($row[7] == '5') {
				$detBayar['tipepegawai'] = 1;
			}
			$detBayar['idpegawai'] = $row[8];
			$detBayar['idbank'] = $row[9];
			$detBayar['ket_cc'] = $row[10];
			$detBayar['idkontraktor'] = $row[11];
			$detBayar['nominal'] = RemoveComma($row[4]);
			$detBayar['keterangan'] = $row[12];
			$detBayar['stekses'] = 0;
			$detBayar['jaminan'] = $row[14];

			$detBayar['trace_number'] = $row[15];
			$detBayar['iddokter'] = $row[16];
			$detBayar['tipepegawai'] = $row[17];
			$detBayar['tipekontraktor'] = $row[18];
			$detBayar['status'] = 1;

			if ($this->tkasir_model->saveDataPembayaran($detBayar)) {
				$status = true;
			}
		}
		redirect('tkasir/index', 'location');
	}

	public function save_rajal()
	{
		$ar_post = json_decode($this->input->post('pos_tabel_detail'), true);
		$arr_pt_admin = array_filter($ar_post, function ($var) {
			//Filter Hanya yg Administrasi saja
			return (isset($var[6]) && $var[6] == 1 && $var[7] == 'administrasi');
		});
		$arr_pt_igd = array_filter($ar_post, function ($var) {
			//Filter Hanya yg IGD saja
			return (isset($var[6]) && $var[6] == 1 && $var[7] == 'pt_igd');
		});
		$arr_pt_lab = array_filter($ar_post, function ($var) {
			//Filter Hanya yg LAB saja
			return (isset($var[6]) && $var[6] == 1 && $var[7] == 'lab');
		});
		$arr_pt_radiologi = array_filter($ar_post, function ($var) {
			//Filter Hanya yg Radiologi saja
			return (isset($var[6]) && $var[6] == 1 && $var[7] == 'radiologi');
		});
		$arr_pt_fisio = array_filter($ar_post, function ($var) {
			//Filter Hanya yg Fisio saja
			return (isset($var[6]) && $var[6] == 1 && $var[7] == 'fisio');
		});

		$pos_tabel_hapus_igd = json_decode($this->input->post('pos_tabel_hapus_igd'));
		$arr_hapus_igd = array_filter($pos_tabel_hapus_igd, function ($var) {
			//Filter Hanya IGD
			return ($var[0] == 'pt_igd');
		});
		$arr_hapus_admin = array_filter($pos_tabel_hapus_igd, function ($var) {
			//Filter Hanya Admin
			return ($var[0] == 'administrasi');
		});

		if ($arr_pt_admin) {
			//Jika ada perubahan di tabel maka akan di update dulu tabel nya Administrasi
			foreach ($arr_pt_admin as $row):
				$iddetail = $row[9];
			$detIGD = [];
			$detIGD['jasasarana'] = RemoveComma($row[12]);
			$detIGD['jasapelayanan'] = RemoveComma($row[13]);
			$detIGD['bhp'] = RemoveComma($row[14]);
			$detIGD['biayaperawatan'] = RemoveComma($row[15]);

			$detIGD['jasasarana_disc'] = RemoveComma($row[20]);
			$detIGD['jasapelayanan_disc'] = RemoveComma($row[21]);
			$detIGD['bhp_disc'] = RemoveComma($row[22]);
			$detIGD['biayaperawatan_disc'] = RemoveComma($row[23]);

			$detIGD['total'] = RemoveComma($row[16]);
			$detIGD['kuantitas'] = RemoveComma($row[17]);
			$detIGD['diskon'] = RemoveComma($row[18]);
			$detIGD['totalkeseluruhan'] = RemoveComma($row[19]);

			if ($this->tkasir_model->update_tpoliklinik_admin($detIGD, $iddetail)) {
				$status = true;
			}
			endforeach;
		}
		if ($arr_pt_igd) {
			//Jika ada perubahan di tabel maka akan di update dulu tabel nya IGD Tindakan
			foreach ($arr_pt_igd as $row):
				$iddetail = $row[9];
			$detIGD = [];
			$detIGD['jasasarana'] = RemoveComma($row[12]);
			$detIGD['jasapelayanan'] = RemoveComma($row[13]);
			$detIGD['bhp'] = RemoveComma($row[14]);
			$detIGD['biayaperawatan'] = RemoveComma($row[15]);

			$detIGD['jasasarana_disc'] = RemoveComma($row[20]);
			$detIGD['jasapelayanan_disc'] = RemoveComma($row[21]);
			$detIGD['bhp_disc'] = RemoveComma($row[22]);
			$detIGD['biayaperawatan_disc'] = RemoveComma($row[23]);

			$detIGD['total'] = RemoveComma($row[16]);
			$detIGD['kuantitas'] = RemoveComma($row[17]);
			$detIGD['diskon'] = RemoveComma($row[18]);
			$detIGD['totalkeseluruhan'] = RemoveComma($row[19]);

			if ($this->tkasir_model->update_tpoliklinik_pelayanan($detIGD, $iddetail)) {
				$status = true;
			}
			endforeach;
		}
		if ($arr_pt_lab) {
			//Jika ada perubahan di tabel maka akan di update dulu tabel nya Lab
			foreach ($arr_pt_lab as $row):
				$iddetail = $row[9];
			$detIGD = [];
			$detIGD['jasasarana'] = RemoveComma($row[12]);
			$detIGD['jasapelayanan'] = RemoveComma($row[13]);
			$detIGD['bhp'] = RemoveComma($row[14]);
			$detIGD['biayaperawatan'] = RemoveComma($row[15]);

			$detIGD['jasasarana_disc'] = RemoveComma($row[20]);
			$detIGD['jasapelayanan_disc'] = RemoveComma($row[21]);
			$detIGD['bhp_disc'] = RemoveComma($row[22]);
			$detIGD['biayaperawatan_disc'] = RemoveComma($row[23]);

			$detIGD['total'] = RemoveComma($row[16]);
			$detIGD['kuantitas'] = RemoveComma($row[17]);
			$detIGD['diskon'] = RemoveComma($row[18]);
			$detIGD['totalkeseluruhan'] = RemoveComma($row[19]);

			if ($this->tkasir_model->update_tpoliklinik_lab($detIGD, $iddetail)) {
				$status = true;
			}
			endforeach;
		}
		if ($arr_pt_radiologi) {
			//Jika ada perubahan di tabel maka akan di update dulu tabel nya RADIOLOGI
			foreach ($arr_pt_radiologi as $row):
				$iddetail = $row[9];
			$detIGD = [];
			$detIGD['jasasarana'] = RemoveComma($row[12]);
			$detIGD['jasapelayanan'] = RemoveComma($row[13]);
			$detIGD['bhp'] = RemoveComma($row[14]);
			$detIGD['biayaperawatan'] = RemoveComma($row[15]);

			$detIGD['jasasarana_disc'] = RemoveComma($row[20]);
			$detIGD['jasapelayanan_disc'] = RemoveComma($row[21]);
			$detIGD['bhp_disc'] = RemoveComma($row[22]);
			$detIGD['biayaperawatan_disc'] = RemoveComma($row[23]);

			$detIGD['total'] = RemoveComma($row[16]);
			$detIGD['kuantitas'] = RemoveComma($row[17]);
			$detIGD['diskon'] = RemoveComma($row[18]);
			$detIGD['totalkeseluruhan'] = RemoveComma($row[19]);

			if ($this->tkasir_model->update_tpoliklinik_radiologi($detIGD, $iddetail)) {
				$status = true;
			}
			endforeach;
		}
		if ($arr_pt_fisio) {
			//Jika ada perubahan di tabel maka akan di update dulu tabel nya FISIOTERAPI
			foreach ($arr_pt_fisio as $row):
				$iddetail = $row[9];
			$detIGD = [];
			$detIGD['jasasarana'] = RemoveComma($row[12]);
			$detIGD['jasapelayanan'] = RemoveComma($row[13]);
			$detIGD['bhp'] = RemoveComma($row[14]);
			$detIGD['biayaperawatan'] = RemoveComma($row[15]);

			$detIGD['jasasarana_disc'] = RemoveComma($row[20]);
			$detIGD['jasapelayanan_disc'] = RemoveComma($row[21]);
			$detIGD['bhp_disc'] = RemoveComma($row[22]);
			$detIGD['biayaperawatan_disc'] = RemoveComma($row[23]);

			$detIGD['total'] = RemoveComma($row[16]);
			$detIGD['kuantitas'] = RemoveComma($row[17]);
			$detIGD['diskon'] = RemoveComma($row[18]);
			$detIGD['totalkeseluruhan'] = RemoveComma($row[19]);

			if ($this->tkasir_model->update_tpoliklinik_fisio($detIGD, $iddetail)) {
				$status = true;
			}
			endforeach;
		}

		if ($arr_hapus_igd) {
			//Jika ada yang di delete ditabel nya IGD Tindakan
			foreach ($arr_hapus_igd as $row):
				$iddetail = $row[1];

			if ($this->tkasir_model->hapus_tpoliklinik_pelayanan($iddetail)) {
				$status = true;
			}
			endforeach;
		}
		if ($arr_hapus_admin) {
			//Jika ada yang di delete ditabel nya IGD Tindakan
			foreach ($arr_hapus_admin as $row):
				$iddetail = $row[1];

			if ($this->tkasir_model->hapus_tpoliklinik_admin($iddetail)) {
				$status = true;
			}
			endforeach;
		}
		// exit();
		$pos_tabel_pembayaran = json_decode($this->input->post('pos_tabel_pembayaran'));

		$idkasir = $this->input->post('idkasir');
		$this->tkasir_model->UpdateTkasir($idkasir, 'rajal');
		$this->tkasir_model->DeletePembayaran($idkasir);
		$detail_pos_tabel = json_decode($this->input->post('pos_tabel_pembayaran'));
		foreach ($pos_tabel_pembayaran as $row):
			$detBayar = [];
		$detBayar['idkasir'] = $idkasir;
		$detBayar['idtipe'] = 1;
		$detBayar['idmetode'] = $row[7];
		$detBayar['idpegawai'] = $row[8];
		$detBayar['idbank'] = $row[9];
		$detBayar['ket_cc'] = $row[10];
		$detBayar['idkontraktor'] = $row[11];
		$detBayar['nominal'] = RemoveComma($row[4]);
		$detBayar['keterangan'] = $row[12];
		$detBayar['stekses'] = 0;
		$detBayar['jaminan'] = $row[14];
		$detBayar['trace_number'] = $row[15];
		$detBayar['iddokter'] = $row[16];
		$detBayar['tipepegawai'] = $row[17];
		$detBayar['tipekontraktor'] = $row[18];
		$detBayar['status'] = 1;
		if ($this->tkasir_model->saveDataPembayaran($detBayar)) {
			$status = true;
		}
		endforeach;
		redirect('tkasir/index', 'location');
	}

	public function batalkan($id = 0)
	{
		$id = $this->input->post('idkasir');
		if ($this->tkasir_model->batalkan($id)) {
			$_SESSION['status'] = 'ToastrSukses("Pembatalan Berhasil","Info")';
			redirect('tkasir/index', 'location');
		} else {
			$_SESSION['status'] = 'Toastr("Pembatalan Gagal","Info")';
			redirect('tkasir/index', 'location');
		}
	}

	public function print_kwitansi($idkasir, $st_cetak = '0')
	{
		$idkasir = $this->input->post('idkasir_cetak');
		$idprint = $this->input->post('idprint');
		$jenisprint = $this->input->post('jenisprint');
		$penyetor = $this->input->post('penyetor');
		$deskripsi_kwitansi = $this->input->post('deskripsi_kwitansi');
		$cara = $this->input->post('deskripsi_kwitansi');
		$data = [
			'idkasir' => $idkasir,
			'idprint' => $idprint,
			'jenisprint' => $jenisprint,
			'penyetor' => $penyetor,
			'deskripsi_kwitansi' => $deskripsi_kwitansi,
			'st_cetak' => $st_cetak
		];

		$data['base_url'] = $this->config->base_url();
		if ($st_cetak == '1') {
			$this->tkasir_model->update_cetak($idkasir, '1', $data['idprint']);
		}
		$data = array_merge($data, backend_info());
		$this->load->view('Tkasir/print_kwitansi', $data);
	}

	public function cetak_kwitansi()
	{
		$idkasir = $this->input->post('idkasir');
		$idprint = $this->input->post('idprint');
		$jenisprint = $this->input->post('jenisprint');
		$penyetor = $this->input->post('penyetor');
		$deskripsi_kwitansi = $this->input->post('deskripsi_kwitansi');
		$cara = $this->input->post('deskripsi_kwitansi');
		$st_cetak = $this->input->post('st_cetak');

		$data = [
			'idkasir' => $idkasir,
			'idprint' => $idprint,
			'jenisprint' => $jenisprint,
			'penyetor' => $penyetor,
			'deskripsi_kwitansi' => $deskripsi_kwitansi,
		];

		$data = [
			'penyetor' => $penyetor,
			'deskripsi_kwitansi' => $deskripsi_kwitansi
		];
		$this->db->where('id', $idkasir);
		$this->db->update('tkasir', $data);

		$options = new Options();

		$options->set('isRemoteEnabled', true);

		$options->set('enable-javascript', true);
		$options->set('javascript-delay', 13500);
		$options->set('enable-smart-shrinking', true);
		$options->set('no-stop-slow-scripts', true);

		$dompdf = new Dompdf($options);

		$data = [];
		$jenis_rujukan = $this->tkasir_model->jenis_rujukan($idkasir);
		if ($jenis_rujukan == '3') {
			$row = $this->tkasir_model->getHeaderInfoFaktur_NR($idkasir);
			$data['namapoli'] = 'NON RUJUKAN';
			$data['pelayanan'] = 'PENJUALAN OBAT BEBAS';
			$data['nokasir'] = $row->nokasir;
			$data['nomedrec'] = $row->nomedrec;
			$data['tanggal'] = $row->tanggal;
			$data['namapasien'] = $row->nama;
			$data['alamat'] = $row->alamat;
			$data['namadokter'] = '';

			$data['payor'] = '';
			$data['list_non_racikan'] = $this->tkasir_model->list_non_racikan($idkasir);
			$data['list_racikan'] = $this->tkasir_model->list_racikan($idkasir);
		} else {
			$row = $this->tkasir_model->getHeaderKwitansi($idkasir);

			if ($row) {
				$data['namapoli'] = $row->namapoli;
				$data['pelayanan'] = GetAsalRujukanKwitansi($row->idtipe);
				$data['nokasir'] = $row->nopendaftaran;
				$data['nomedrec'] = $row->no_medrec;
				$data['tanggal'] = $row->tanggaldaftar;
				$data['namapasien'] = $row->namapasien;
				$data['alamat'] = $row->alamatpasien;
				$data['namadokter'] = $row->namadokter;
			}
		}
		if ($st_cetak == '1') {
			$this->tkasir_model->update_cetak($idkasir, '2', $idprint);
		}

		$row = $this->tkasir_model->get_data_kasir($idkasir, $idprint);
		$data['nama_kasir'] = $row->namakasir;
		$data['total_terima'] = $row->bayar - $row->kembalian;
		$data['kembalian'] = $row->kembalian;
		$data['user_cetak'] = $row->namacetak;
		$data['jml_cetak'] = $row->totalcetakkwitansi;
		$data['tgl_cetak'] = ($row->tgl_cetak) ? $row->tgl_cetak : date('Y-m-d H:i:s');
		if ($row->penyetor == '') {
			$data['penyetor'] = $data['namapasien'] ;
		} else {
			$data['penyetor'] = $row->penyetor;
		}
		if ($row->deskripsi_kwitansi == '') {
			$data['deskripsi_kwitansi'] = $data['pelayanan'];
		} else {
			$data['deskripsi_kwitansi'] = $row->deskripsi_kwitansi;
		}

		if ($jenisprint == '0') {
			$data['cara'] = '';
		} else {
			$data['cara'] = 'Dengan Cara [' . metode_pembayaran_kasir($jenisprint) . ']';
		}

		$data['tgl_trx'] = HumanDateLong($this->tkasir_model->get_tgl_trx($idkasir));
		$html = $this->load->view('Tkasir/kwitansi', $data, true);
		$dompdf->loadHtml($html);
		$dompdf->setPaper('A4', 'portrait');
		$dompdf->render();
		$dompdf->stream('Faktur Transaksi Kasir.pdf', ['Attachment' => 0]);
	}

	public function get_kontraktor()
	{
		$tipekontraktor = $this->input->post('tipekontraktor');
		$cari = $this->input->post('search');
		$data = $this->tkasir_model->get_kontraktor($tipekontraktor, $cari);
		$this->output->set_output(json_encode($data));
	}

	public function get_tarif_admin()
	{
		$idadmin = $this->input->post('idadmin');
		$data = $this->tkasir_model->get_tarif_admin($idadmin);
		$this->output->set_output(json_encode($data));
	}

	public function save_tarif_admin()
	{
		$idadmin = $this->input->post('idadmin');
		$idpendaftaran = $this->input->post('idpendaftaran');
		$idadministrasi = $this->input->post('idadministrasi');
		if ($idadministrasi == '1') {
			$nama_biaya = 'Biaya Administrasi';
		} else {
			$nama_biaya = 'Biaya Cetak Kartu';
		}

		$q = "INSERT INTO tpoliklinik_administrasi (idpendaftaran,idadministrasi,namatarif,jasasarana,jasapelayanan,bhp,biayaperawatan,total,kuantitas,totalkeseluruhan,status)
			SELECT '$idpendaftaran' as idpendaftaran,H.idjenis as idadministrasi,'$nama_biaya' as namatarif,H.jasasarana,H.jasapelayanan,H.bhp,H.biayaperawatan,H.total,1 as kuantitas,H.total as totalkeseluruhan,'1' as status
			from mtarif_administrasi H
			WHERE H.id='$idadmin'";
		$query = $this->db->query($q);
		if ($query) {
			$data = true;
		} else {
			$data = false;
		}
		$this->output->set_output(json_encode($data));
	}

	public function hapus_tarif_admin()
	{
		$id = $this->input->post('id');

		$q = "UPDATE tpoliklinik_administrasi set status='0' WHERE id='$id'";
		$query = $this->db->query($q);
		if ($query) {
			$data = true;
		} else {
			$data = false;
		}
		$this->output->set_output(json_encode($data));
	}

	public function get_pegawai()
	{
		$cari = $this->input->post('search');
		$data = $this->tkasir_model->get_pegawai($cari);
		$this->output->set_output(json_encode($data));
	}

	public function get_dokter()
	{
		$cari = $this->input->post('search');
		$data = $this->tkasir_model->get_dokter($cari);
		$this->output->set_output(json_encode($data));
	}

	public function test_array()
	{
		$kalimat = 'satu, dua, tiga, empat, lima';
		$id = '1, 2, 3, 4, 5';
		$arr_kalimat = explode(', ', $kalimat);
		$arr_id = explode(', ', $id);
		$kata = '';
		foreach ($arr_kalimat as $x => $x_value) {
			$kata .= $arr_id[$x] . '.' . $arr_kalimat[$x];
		}
		echo $kata;
	}

	public function getIndex($uri = 'index')
	{
		$this->select = [];
		$this->join = [];
		$this->where = [];

		$where = '';

		if ($this->input->post('tanggaldari') == '') {
			$where = " WHERE DATE(K.tanggal)='" . date('Y-m-d') . "'";
		} else {
			$where = " WHERE DATE(K.tanggal)>='" . YMDFormat($this->input->post('tanggaldari')) . "' AND DATE(K.tanggal)<='" . YMDFormat($this->input->post('tanggalsampai')) . "'";
		}
		if ($this->input->post('idasalpasien') != '999') {
			$where .= " AND K.idtipe='" . $this->input->post('idasalpasien') . "'";
		}
		if ($this->input->post('idkelompokpasien') != '999') {
			$where .= " AND (pk.id='" . $this->input->post('idkelompokpasien') . "' OR pk_ob.id='" . $this->input->post('idkelompokpasien') . "')";
		}
		if ($this->input->post('iddokter') != '999') {
			$where .= " AND P.iddokter='" . $this->input->post('iddokter') . "'";
		}
		if ($this->input->post('idpoliklinik') != '999') {
			$where .= " AND P.idpoliklinik='" . $this->input->post('idpoliklinik') . "'";
		}
		if ($this->input->post('status') != '999') {
			if ($this->input->post('status')=='0'){
				$where .= " AND K.`status`='" . $this->input->post('status') . "' OR P.status='0'";
			}else{
				$where .= " AND K.`status`='" . $this->input->post('status') . "' AND P.status !='0'";
			}
		}
		if ($this->input->post('noreg') != '') {
			$where .= " AND (P.nopendaftaran LIKE '%" . $this->input->post('noreg') . "%' OR OB.nopenjualan LIKE '%" . $this->input->post('noreg') . "%')";
		}
		if ($this->input->post('nomedrec') != '') {
			$where .= " AND (OB.nomedrec = '" . $this->input->post('nomedrec') . "' OR P.no_medrec ='" . $this->input->post('nomedrec') . "')";
		}
		if ($this->input->post('namapasien') != '') {
			$where .= " AND (OB.nama LIKE '%" . $this->input->post('namapasien') . "%' OR P.namapasien LIKE '%" . $this->input->post('namapasien') . "%')";
		}

		$from = "(SELECT K.id,K.idtindakan
        ,CASE WHEN K.idtipe='3' THEN K.tanggal ELSE K.tanggal END as tanggal,K.idtipe,K.nokasir,P.iddokter,P.idpoliklinik
				,CASE WHEN K.idtipe='3' THEN 'N' ELSE P.id END as idpendaftaran
				,CASE WHEN K.idtipe='3' THEN OB.nopenjualan ELSE P.nopendaftaran END as notrx
				,CASE WHEN K.idtipe='3' THEN OB.nomedrec ELSE P.no_medrec END as no_medrec
				,CASE WHEN K.idtipe='3' THEN '' ELSE P.title END as title
				,CASE WHEN K.idtipe='3' THEN OB.nama ELSE P.namapasien END as nama
				,CASE WHEN K.idtipe='3' THEN '' ELSE mp.nama END as namapoli
				,CASE WHEN K.idtipe='3' THEN pk_ob.id ELSE pk.id END as idkelompokpasien
				,CASE WHEN K.idtipe='3' THEN pk_ob.nama ELSE pk.nama END as namakelompok
				,CASE WHEN K.idtipe='3' THEN '0' ELSE T.rujukfarmasi END as rujukfarmasi
				,CASE WHEN K.idtipe='3' THEN '0' ELSE T.rujuklaboratorium END as rujuklaboratorium
				,CASE WHEN K.idtipe='3' THEN '0' ELSE T.rujukradiologi END as rujukradiologi
				,CASE WHEN K.idtipe='3' THEN '0' ELSE IF(T.rujukfisioterapi='',0,T.rujukfisioterapi)  END as rujukfisioterapi
				,RFa.`status` as far_status
				,RR.`status` as rad_status
				,RL.`status` as lab_status
				,RF.`status` as fis_status
				,K.`status`
				,K.`status_verifikasi`
				,GROUP_CONCAT(B.idmetode) as idmetode
				,GROUP_CONCAT(B.id) as idbayar
				,P.statusrencana,RI.id as ri_id
				,CASE WHEN K.idtipe = '3' THEN OB.status ELSE T.status END AS status_tindakan
				,P.status as status_pendaftaran
				from tkasir K
				LEFT JOIN tpoliklinik_tindakan T ON T.id=K.idtindakan AND K.idtipe IN (1,2)
				LEFT JOIN tpoliklinik_pendaftaran P ON T.idpendaftaran=P.id
				LEFT JOIN mpoliklinik mp ON mp.id=P.idpoliklinik
				LEFT JOIN mpasien_kelompok pk ON pk.id=P.idkelompokpasien
				LEFT JOIN tpasien_penjualan OB ON OB.id=K.idtindakan AND K.idtipe='3'
				LEFT JOIN mpasien_kelompok pk_ob ON pk_ob.id=OB.idkelompokpasien
				LEFT JOIN trujukan_radiologi RR ON RR.idtindakan=T.id AND T.rujukradiologi='1'
				LEFT JOIN trujukan_laboratorium RL ON RL.idtindakan=T.id AND T.rujuklaboratorium='1'
				LEFT JOIN trujukan_fisioterapi RF ON RF.idtindakan=T.id AND T.rujukfisioterapi='1'
				LEFT JOIN tpasien_penjualan RFa ON RFa.idtindakan=T.id AND T.rujukfarmasi='1'
				LEFT JOIN tkasir_pembayaran B ON B.idkasir=K.id
				LEFT JOIN trawatinap_pendaftaran RI ON RI.idpoliklinik=P.id AND RI.`status` != 0

				" . $where . '
				GROUP BY K.id

				ORDER BY K.id DESC) as tbl
        WHERE tbl.status_tindakan != 0';
		// print_r($from);exit;
		$this->order = [];
		$this->group = [];
		$this->from = $from;

		$this->column_search = [];
		$this->column_order = [];

		$list = $this->datatable->get_datatables();

		$data = [];
		$no = $_POST['start'];

		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		$url_dok_rajal = site_url('tverifikasi_transaksi/upload_document/tkasir/');

		foreach ($list as $r) {
			$no++;
			$row = [];
			$action = '';
			if ($r->status == 0) {
				//Dibatalkan
				$tipeAction = '';
				if ($r->idtipe == '3') {
					if (button_roles('tkasir/transaksi_ob')) {
						$tipeAction = '<a href="' . site_url() . 'tkasir/transaksi_ob/' . $r->idtindakan . '/' . $r->id . '/1' . '" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Edit Transaksi"><i class="fa fa-pencil"></i></a>';
					}
				} else {
					if (UserAccesForm($user_acces_form, ['1072'])) {
						$tipeAction = '<a href="' . site_url() . 'tkasir/transaksi/' . $r->idpendaftaran . '/' . $r->id . '/1' . '" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Transaksi"><i class="fa fa-pencil"></i></a>';
					}
				}

				if (UserAccesForm($user_acces_form, ['1089'])) {
					$action .= $tipeAction . '<div class="btn-group">
                     <div class="btn-group dropright">
                       <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                         <span class="fa fa-print"></span>
                       </button>
                     <ul class="dropdown-menu">
                       <li>
                         <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_transaksi/' . $r->idpendaftaran . '/' . $r->id . '/1' . '">Rincian Pembayaran</a>
                         <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_rincian_biaya/' . $r->idpendaftaran . '">Rincian Biaya</a>
                       </li>
                     </ul>
                     </div>
                   </div>';
				}
			} elseif ($r->status == 1) {
				//Menunggu Di Proses

				$tipeAction = '';
				if ($r->idtipe == '3') {
					if (UserAccesForm($user_acces_form, ['1072'])) {
						$tipeAction = '<a href="' . site_url() . 'tkasir/transaksi_ob/' . $r->idtindakan . '/' . $r->id . '/0' . '" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Transaksi"><i class="fa fa-pencil"></i></a>';
					}
				} else {
					if (UserAccesForm($user_acces_form, ['1072'])) {
						if ($r->ri_id == null) {
							$tipeAction = '<a href="' . site_url() . 'tkasir/transaksi/' . $r->idpendaftaran . '/' . $r->id . '/0' . '" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Transaksi"><i class="fa fa-pencil"></i></a>';
						}
					}
				}
				$action = '<div class="btn-group">' . $tipeAction;
				if (UserAccesForm($user_acces_form, ['1089'])) {
					$action .= '<div class="btn-group">
                      <div class="btn-group dropright">
                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                          <span class="fa fa-print"></span>
                        </button>
                      <ul class="dropdown-menu">
                        <li>
                          <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_transaksi/' . $r->idpendaftaran . '/' . $r->id . '">Rincian Pembayaran</a>
                          <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_rincian_biaya/' . $r->idpendaftaran . '">Rincian Biaya</a>
                        </li>
                      </ul>
                      </div>
                    </div>';
				}
				if (UserAccesForm($user_acces_form, ['1088'])) {
					// $action .= '';
				}
				$action .= '</div>';
			} elseif ($r->status == 2) {
				//SELESAI
				//Telah di proses
				$tipeAction = '';
				$action = '<div class="btn-group">' . $tipeAction;
				if (UserAccesForm($user_acces_form, ['1089'])) {
					$action .= '<div class="btn-group">
                      <div class="btn-group dropright">
                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                          <span class="fa fa-print"></span>
                        </button>
                      <ul class="dropdown-menu">
                        <li>
                          <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_transaksi/' . $r->idpendaftaran . '/' . $r->id . '">Rincian Pembayaran</a>
                          <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_rincian_biaya/' . $r->idpendaftaran . '">Rincian Biaya</a>
                        </li>
                      </ul>
                      </div>
                    </div>';

					$action .= '<div class="btn-group" role="group">
								<button class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-expanded="false"  data-toggle="tooltip" title="Kwitansi">
									<span class="fa fa-list-alt "></span>
								</button>
								<ul class="dropdown-menu dropdown-menu-right">';
					$action .= '<li><a tabindex="-1" data-toggle="modal" href="#modalKwitansi" onclick="ShowOld(' . $r->id . ',0,0);">Kwitansi All</a></li><li class="divider"></li>';
					$list_bayar = $r->idmetode;
					$list_bayar_id = $r->idbayar;
					$arr_bayar = explode(',', $list_bayar);
					$arr_bayar_id = explode(',', $list_bayar_id);
					foreach ($arr_bayar as $x => $x_value) {
						if (metode_pembayaran_kasir($arr_bayar[$x]) != '') {
							$action .= '<li><a tabindex="-1" data-toggle="modal" href="#modalKwitansi" onclick="ShowOld(' . $r->id . ',' . $arr_bayar_id[$x] . ',' . $arr_bayar[$x] . ');">Print Out Kwitansi ' . metode_pembayaran_kasir($arr_bayar[$x]) . '</a></li>';
						}
					}

					$action .= '</ul></div>';
				}
				if (UserAccesForm($user_acces_form, ['1088'])) {
					if ($r->status_verifikasi == 0) {
						$action .= '<button data-idkasir="' . $r->id . '" class="btn btn-danger btn-sm selectedBatal" id="btn_pembayaran" data-toggle="modal"  data-target="#modalPembatalan"  type="button"><i class="fa fa-trash"></i></button>';
					}
				}
				$action .= '<a class="view btn btn-sm btn-danger" href="' . $url_dok_rajal . $r->id . '" target="_blank"  type="button"  title="Upload"><i class="fa fa-upload"></i></a>';
				$action .= '</div>';
			} else {
				$action = '';
			}

			$row[] = $no;
			$row[] = $r->tanggal;
			$row[] = $r->notrx;
			$row[] = $r->no_medrec;

			if ($r->title <> '') {
				$row[] = $r->title . '. ' . $r->nama;
			} else {
				$row[] = $r->nama;
			}

			$row[] = $r->namapoli;
			$row[] = $r->namakelompok;
			$row[] = '<p class="nice-copy">' .
											StatusIndexRujukan2($r->rujukfarmasi, 'FR', $r->far_status) . '&nbsp;' .
											StatusIndexRujukan2($r->rujuklaboratorium, 'LB', $r->lab_status) . '&nbsp;' .
											StatusIndexRujukan2($r->rujukradiologi, 'RD', $r->rad_status) . '&nbsp;' .
											StatusIndexRujukan2($r->rujukfisioterapi, 'FI', $r->fis_status) . '&nbsp;' .
										'</p>';
			$label_rencana = '';
			if ($r->statusrencana > 0) {
				$label_rencana = GetStatusRencana($r->statusrencana);
			}
			if ($r->status_pendaftaran =='0'){
				$action='';
			}
			$row[] = $action;
			if ($r->ri_id != null && $r->status == '1') {
				$row[] = $label_rencana;
			} else {
				if ($r->status_pendaftaran!='0'){
				$row[] = StatusKasir($r->status) . '<br>' . $label_rencana;
					
				}else{
					$row[] = text_danger('DIBATALKAN PENDAFTARAN');
				}
			}

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function find_old($idkasir)
	{
		$row = $this->tkasir_model->find_old($idkasir);
		$arr['penyetor'] = $row->penyetor;
		$arr['deskripsi_kwitansi'] = $row->deskripsi_kwitansi;
		$this->output->set_output(json_encode($arr));
	}

	public function kwitansi_verifikasi($id)
	{
		$dompdf = new Dompdf();
		$q = "SELECT PD.keterangan as deskripsi_kwitansi,TP.no_medrec as nomedrec,TP.namapasien as penyetor,PD.nominal as total_terima,TP.tanggaldaftar as tgl_trx
			,K.edited_nama as nama_kasir
			from tkasir_pembayaran PD
			LEFT JOIN tkasir K ON PD.idkasir=K.id
			LEFT JOIN tpoliklinik_tindakan TD ON TD.id=K.idtindakan
			LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TD.idpendaftaran

			WHERE PD.id='$id'";
		$data = $this->db->query($q)->row_array();
		$data = array_merge($data, backend_info());
		$data['tgl_cetak'] = date('Y-m-d H:i:s');
		$data['jml_cetak'] = '1';
		$data['user_cetak'] = $this->session->userdata('user_name');
		$html = $this->load->view('Tkasir/kwitansi_verifikasi', $data, true);
		$html = $this->parser->parse_string($html, $data);
		$dompdf->loadHtml($html);
		$dompdf->setPaper('A4', 'portrait');
		$dompdf->render();
		$dompdf->stream('Rekap Kontrabon.pdf', ['Attachment' => 0]);

		$row = $this->tkasir_model->find_old($idkasir);
		$arr['penyetor'] = $row->penyetor;
		$arr['deskripsi_kwitansi'] = $row->deskripsi_kwitansi;
		$this->output->set_output(json_encode($arr));
	}
}
