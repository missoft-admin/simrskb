<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tvalidasi_kas extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('tvalidasi_kas_model','model');
		$this->load->model('Mrka_pengajuan_model');
	}

	function index() {
		$data=array(
			'st_posting'=>'#',
			'idkelompokpasien'=>'#',
			'idrekanan'=>'#',
			'nojurnal'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'#',
			'tanggalterima1'=>'',
			'tanggalterima2'=>'',
			'tanggal_trx1'=>'',
			'tanggal_trx2'=>'',
		);
		$data['error'] 			= '';
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		// print_r($data['list_rekanan']);exit();
		$data['list_kp'] 	= $this->model->list_kp();
		$data['title'] 			= 'Validasi Jurnal Kas';
		$data['content'] 		= 'Tvalidasi_kas/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Kas",'tvalidasi_kas/index'),
									array("List",'#')
								);

		// print_r($data);exit();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        $userid = $this->session->userdata('user_id');
		// $batas_batal=$this->db->query("SELECT batas_batal FROM msetting_jurnal_kas WHERE id='1'")->row('batas_batal');
		
		$id_reff=$this->input->post('id_reff');
		$nojurnal=$this->input->post('nojurnal');
		$no_terima=$this->input->post('no_terima');
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		$st_posting=$this->input->post('st_posting');
		$where='';
		
		
		if ($id_reff !='#'){
			$where .=" AND H.id_reff='$id_reff' ";
		}
		if ($st_posting !='#'){
			$where .=" AND H.st_posting='$st_posting' ";
		}
		
		if ($nojurnal !=''){
			$where .=" AND H.nojurnal='$nojurnal' ";
		}
		if ($no_terima !=''){
			$where .=" AND H.notransaksi='$no_terima' ";
		}
		
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(H.tanggal_transaksi) >='".YMDFormat($tanggal_trx1)."' AND DATE(H.tanggal_transaksi) <='".YMDFormat($tanggal_trx2)."'";
        }
        $from = "(
					SELECT H.id,H.idtransaksi,H.st_posting,H.nojurnal,H.notransaksi,H.tanggal_transaksi,H.keterangan,H.id_reff,R.nama as tipe
					,H.nominal
					FROM tvalidasi_kas H 
					LEFT JOIN tvalidasi_kas_00_ref R ON R.id=H.id_reff
					WHERE H.`status`='1' ".$where."
				) as tbl";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nojurnal','nama_bank','notransaksi','keterangan_kas','nama_rekanan');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

			
        foreach ($list as $r) {
            $no++;
            $row = array();
			$disabel_btn='';
			
			// if ($r->umur_posting>$batas_batal){
				// $disabel_btn='disabled';
			// }
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url_kas        = site_url('tpendapatan/');
            $url       = site_url('tklaim_monitoring/');
            $url_validasi        = site_url('tvalidasi_kas/');
            $url_batal       = site_url('tgudang_pengembalian/');
            $row[] = $r->id;
            $row[] = $r->st_posting;
            $row[] = $no;
            $row[] = $r->nojurnal;
            $row[] = HumanDateShort($r->tanggal_transaksi);
            $row[] = text_primary($r->tipe);
            $row[] = $r->notransaksi;
            $row[] = $r->keterangan;
            $row[] = number_format($r->nominal,2);
           
            $row[] = ($r->st_posting=='1'?text_success('TELAH DIPOSTING'):text_default('MENUGGU DIPOSTING'));
					
			if ($r->id_reff=='1'){//kasbon
				$aksi .= '<a href="'.site_url('tkasbon/').'update/'.$r->idtransaksi.'/disabled" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
				$aksi .= '<a href="'.$url_validasi.'detail/'.$r->id.'/'.$r->id_reff.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
			}
			if ($r->id_reff=='2'){//Refund
				$aksi .= '<a href="'.site_url('trefund_kas/').'pembayaran/'.$r->idtransaksi.'/disabled" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
				$aksi .= '<a href="'.$url_validasi.'detail/'.$r->id.'/'.$r->id_reff.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
			}
			if ($r->id_reff=='3'){//Bagi Hasil
				$aksi .= '<a href="'.site_url('tbagi_hasil_bayar/').'update/'.$r->idtransaksi.'/disabled" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
				$aksi .= '<a href="'.$url_validasi.'detail/'.$r->id.'/'.$r->id_reff.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
			}
			if ($r->id_reff=='4'){//Gaji
				$aksi .= '<a href="'.site_url('trekap/').'detail/'.$r->idtransaksi.'/disabled" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
				$aksi .= '<a href="'.$url_validasi.'detail/'.$r->id.'/'.$r->id_reff.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
			}
			
			if ($r->id_reff=='5'){//Mutasi
				$aksi .= '<a href="'.site_url('tmutasi_kas/').'update/'.$r->idtransaksi.'/disabled" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
				$aksi .= '<a href="'.$url_validasi.'detail/'.$r->id.'/'.$r->id_reff.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
			}			
			if ($r->id_reff=='6'){//Pengajuan
				$aksi .= '<a href="'.site_url('mrka_belanja/').'proses_belanja/'.$r->idtransaksi.'/disabled" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
				$aksi .= '<a href="'.$url_validasi.'detail/'.$r->id.'/'.$r->id_reff.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
			}
			if ($r->id_reff=='7'){//Penyesuaian
				$aksi .= '<a href="'.site_url('tpenyesuaian_kas/').'edit/'.$r->idtransaksi.'/disabled" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
				$aksi .= '<a href="'.$url_validasi.'detail/'.$r->id.'/'.$r->id_reff.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
			}
			if ($r->id_reff=='8'){//PEMBELIAN OBAT
				$aksi .= '<a href="'.site_url('tgudang_penerimaan/').'detail/'.$r->idtransaksi.'/disabled" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
				$aksi .= '<a href="'.$url_validasi.'detail/'.$r->id.'/'.$r->id_reff.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
			}	
			if ($r->id_reff=='9'){//HONOR DOKTER
				$aksi .= '<a href="'.site_url('thonor_bayar/').'detail_bayar/'.$r->idtransaksi.'/disabled" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
				$aksi .= '<a href="'.$url_validasi.'detail/'.$r->id.'/'.$r->id_reff.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
			}			
			
			
			
			if ($r->st_posting=='1'){
			$aksi .= '<button title="Batal Posting" '.$disabel_btn.' class="btn btn-xs btn-danger batalkan"><i class="fa fa-close"></i></button>';						
			}else{
			$aksi .= '<button title="Posting" class="btn btn-xs btn-success posting"><i class="si si-arrow-right"></i></button>';
				
			}
					// $aksi .= '<a href="'.$url_validasi.'print_kwitansi_deposit/'.$r->idtransaksi.'" target="_blank" title="Print Kwitansi" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';
					// $aksi .= '<button title="Print" class="btn btn-xs btn-success"><i class="fa fa-print"></i></button>';
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	
	public function posting()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'1',
			'posting_by'=>$this->session->userdata('user_id'),
			'posting_nama'=>$this->session->userdata('user_name'),
            'posting_date'=>date('Y-m-d H:i:s')
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_kas', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function posting_all()
    {
        $id = $this->input->post('id');
		foreach($id as $index=>$val){
			 $data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $val);
			$result=$this->db->update('tvalidasi_kas', $data);
		}
       
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	
	public function batalkan()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'0',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_kas', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	function detail($id,$id_reff,$disabel='') {
		$data=$this->model->getHeader($id,$id_reff);
		$data['error'] 			= '';
		$data['disabel'] 			= $disabel;
		if ($data['st_posting']=='1'){
			$data['disabel'] 			= 'disabled';
		}
		$data['list_akun'] 	= $this->model->list_akun();
		$data['id_reff'] 			= $id_reff;
		$data['title'] 			= 'Detail Validasi Akuntansi';
		if ($id_reff=='1'){//KASBON
			$data['content'] 		= 'Tvalidasi_kas/detail_kasbon';			
		}
		if ($id_reff=='2'){//REFUND
			if ($data['idtipe']=='0' || $data['idtipe']=='1'){
				$data['content'] 		= 'Tvalidasi_kas/detail_refund_deposit';		
			}				
		}
		if ($id_reff=='3'){//BAGI HASIl
			$data['content'] 		= 'Tvalidasi_kas/detail_bagi_hasil';			
		}
		if ($id_reff=='4'){//GAJI
			$data['content'] 		= 'Tvalidasi_kas/detail_gaji';			
		}
		
		if ($id_reff=='5'){//MUTASI
			$data['content'] 		= 'Tvalidasi_kas/detail_mutasi';			
		}
		if ($id_reff=='6'){//PENGAJUAN
			$data['list_detail'] 		= $this->Mrka_pengajuan_model->list_detail($data['idtransaksi']);
			$data['content'] 		= 'Tvalidasi_kas/detail_pengajuan';			
		}
		if ($id_reff=='7'){//PENYESUAIAN
			$data['content'] 		= 'Tvalidasi_kas/detail_penyesuaian';			
		}
		if ($id_reff=='8'){//PEMBELIAN OBAT
			$data['content'] 		= 'Tvalidasi_kas/detail_gudang';			
		}
		if ($id_reff=='9'){//HONOR DOKTER
			$data['content'] 		= 'Tvalidasi_kas/detail_honor';			
		}
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Kas",'tvalidasi_kas/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	// public function get_transaksi_detail($iddet)
    // {
        // $q="SELECT B.nama as nama_barang,D.* FROM `tgudang_penerimaan_detail` D
			// LEFT JOIN view_barang_all B ON B.id=D.idbarang AND B.idkelompokpasien=D.idkelompokpasien
			// WHERE D.id='$iddet'";
       
        // $result=$this->db->query($q)->row_array();
        // $this->output->set_output(json_encode($result));
    // }
	function save_detail(){
		// print_r($this->input->post());exit();
		// $id=$this->model->saveData();
		$id=$this->input->post('idvalidasi');
		$id_reff=$this->input->post('id_reff');
		if($this->model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tvalidasi_kas/detail/'.$id.'/'.$id_reff,'location');
		}
	
	}
	function load_bayar(){
		
		$id_reff=$this->input->post('id_reff');
		$id=$this->input->post('id');
		$idvalidasi=$this->input->post('idvalidasi');
		$nama_tabel_bayar=$this->db->query("SELECT M.nama_tabel_bayar FROM tvalidasi_kas H 
									LEFT JOIN tvalidasi_kas_00_ref M ON M.id=H.id_reff
									WHERE H.id='$idvalidasi'")->row('nama_tabel_bayar');
		// print_r($nama_tabel_bayar);exit();
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		$btn_eye='';
        $q = "SELECT *FROM ".$nama_tabel_bayar." H WHERE H.idvalidasi='$idvalidasi'";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			
			$aksi       = '<div class="btn-group">';
			if ($id_reff=='3'){
				$btn_eye = '<a href="'.site_url('tbagi_hasil_bayar/').'detail_bayar/'.$r->idbayar_id.'/disabled" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';	
			}
			$select_akun='<select name="idakun_bayar[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun).'
						</select>';
			
			$aksi .=$btn_eye;
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td><input type="hidden" name="iddet_bayar[]" value="'.$r->id.'">';
			$tbl .='<td class="text-center">'.$r->jenis_kas_nama.'</td>';
			$tbl .='<td class="text-center">'.$r->sumber_nama.'</td>';
			$tbl .='<td class="text-center">'.$r->bank.'</td>';
			$tbl .='<td class="text-center">'.$r->metode_nama.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_bayar,2).'</td>';
			$tbl .='<td >'.$select_akun.'</td>';
			$aksi .='</div>';
			$tbl .='<td class="text-center">'.$aksi.'</td>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_pengembalian(){
		
		$id_reff=$this->input->post('id_reff');
		$id=$this->input->post('id');
		$idvalidasi=$this->input->post('idvalidasi');
		
		// print_r($nama_tabel_bayar);exit();
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		$btn_eye='';
        $q = "SELECT *FROM tvalidasi_kas_06_pengajuan_pengembalian H WHERE H.idvalidasi='$idvalidasi'";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			
			$aksi       = '<div class="btn-group">';
			
			$select_akun='<select name="idakun_pengembalian[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun).'
						</select>';
			
			$aksi .=$btn_eye;
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td><input type="hidden" name="iddet_pengembalian[]" value="'.$r->id.'">';
			$tbl .='<td class="text-center">'.$r->jenis_kas_nama.'</td>';
			$tbl .='<td class="text-center">'.$r->sumber_nama.'</td>';
			$tbl .='<td class="text-center">'.$r->bank.'</td>';
			$tbl .='<td class="text-center">'.$r->metode_nama.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_bayar,2).'</td>';
			$tbl .='<td >'.$select_akun.'</td>';
			$aksi .='</div>';
			$tbl .='<td class="text-center">'.$aksi.'</td>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_piutang_pengajuan(){
		
		$id_reff=$this->input->post('id_reff');
		$id=$this->input->post('id');
		$idvalidasi=$this->input->post('idvalidasi');
		
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		
		$disabel=$this->input->post('disabel');
		$btn_eye='';
        $q = "SELECT H.*,A.noakun,A.namaakun from tvalidasi_kas_06_pengajuan_piutang H
			LEFT JOIN makun_nomor A ON A.id=H.idakun
			WHERE H.idvalidasi='$idvalidasi'";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			
			$aksi       = '<div class="btn-group">';
			
			$select_akun='<select name="idakun_piutang[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun).'
						</select>';
			
			$aksi .=$btn_eye;
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td><input type="hidden" name="iddet_piutang[]" value="'.$r->id.'">';
			
			$tbl .='<td >'.$select_akun.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal,2).'</td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_ps(){
		
		$id=$this->input->post('id');
		$idvalidasi=$this->input->post('idvalidasi');
		
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		// if ($id_reff=='1'){
			
		// }
        $q = "SELECT *FROM tvalidasi_kas_03_bagi_hasil_detail H WHERE H.idvalidasi='$idvalidasi'";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			
			$aksi       = '<div class="btn-group">';
			$select_akun='<select name="idakun_ps[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun).'
						</select>';
			
		
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td><input type="hidden" name="iddet_ps[]" value="'.$r->id.'">';
			$tbl .='<td class="text-left">'.$r->nama_pemilik.'</td>';
			$tbl .='<td class="text-left">'.$r->nama_tipe.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->jumlah_lembar,0).'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal,0).'</td>';
			$tbl .='<td class="text-left">'.$r->bank.' - '.$r->atas_nama.'</td>';
			$tbl .='<td class="text-center">'.$r->metode.'</td>';
			$tbl .='<td class="text-center">'.HumanDateShort($r->tanggal_bayar).'</td>';
			$tbl .='<td>'.$select_akun.'</td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_detail_gudang(){
		
		$id=$this->input->post('id');
		$idvalidasi=$this->input->post('idvalidasi');
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT D.id,K.nama_tipe as nama_tipe,D.nama_barang,D.nominal_beli,D.nominal_ppn,D.nominal_diskon,D.idakun_beli,D.idakun_ppn,D.idakun_diskon 
				,D.idet
				from tvalidasi_kas_08_gudang_detail D
				LEFT JOIN mdata_tipebarang K ON K.id=D.idtipe
				WHERE D.idvalidasi='$idvalidasi'
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			
			$aksi       = '<div class="btn-group">';
			$select_beli='<select name="idakun_beli[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_beli).'
						</select>';
			$select_ppn='<select name="idakun_ppn[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_ppn).'
						</select>';
			$select_diskon='<select name="idakun_diskon[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_diskon).'
						</select>';
		
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td>'.$r->nama_tipe.'<input type="hidden" name="iddet_gudang[]" value="'.$r->id.'"></td>';
			$tbl .='<td>'.$r->nama_barang.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_beli,2).'</td>';
			$tbl .='<td>'.$select_beli.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_ppn,2).'</td>';
			$tbl .='<td>'.$select_ppn.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_diskon,2).'</td>';
			$tbl .='<td>'.$select_diskon.'</td>';
				$aksi .= '<button type="button" title="Lihat Detail" class="btn btn-default btn-sm" onclick="get_transaksi_detail('.$r->idet.')"><i class="fa fa-eye"></i></button>';
			$tbl .='<td class="text-center">'.$aksi.'</td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		// $tbl .='<td colspan="4" class="text-right"><strong>TOTAL</strong></td>';
		// $tbl .='<td class="text-right"><strong>'.number_format($total,0).'</strong></td>';
		// $tbl .='<td></td>';
		// $tbl .='<td></td>';
		// $tbl .='<td></td>';
		// $tbl .='<td></td>';
		
		// $tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_obat_refund(){
		
		$id=$this->input->post('id');
		$idvalidasi=$this->input->post('idvalidasi');
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT *
				from tvalidasi_kas_02_refund_obat D
				WHERE D.idvalidasi='$idvalidasi'
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			
			$aksi       = '<div class="btn-group">';
			$select_beli='<select name="idakun_beli[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_beli).'
						</select>';
			
			$select_diskon='<select name="idakun_diskon[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_diskon).'
						</select>';
		
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td>'.$r->namatipe.'<input type="hidden" name="iddet_refund_obat[]" value="'.$r->id.'"></td>';
			$tbl .='<td>'.$r->nama_barang.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_beli,2).'</td>';
			$tbl .='<td>'.$select_beli.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_diskon,2).'</td>';
			$tbl .='<td>'.$select_diskon.'</td>';
				$aksi .= '<button type="button" title="Lihat Detail" class="btn btn-default btn-sm" onclick="get_transaksi_detail('.$r->iddet.')"><i class="fa fa-eye"></i></button>';
			$tbl .='<td class="text-center" hidden>'.$aksi.'</td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		// $tbl .='<td colspan="4" class="text-right"><strong>TOTAL</strong></td>';
		// $tbl .='<td class="text-right"><strong>'.number_format($total,0).'</strong></td>';
		// $tbl .='<td></td>';
		// $tbl .='<td></td>';
		// $tbl .='<td></td>';
		// $tbl .='<td></td>';
		
		// $tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_detail_pengajuan(){
		
		$id=$this->input->post('id');
		$idvalidasi=$this->input->post('idvalidasi');
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT D.*
				from tvalidasi_kas_06_pengajuan_detail D
				WHERE D.idvalidasi='$idvalidasi'
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			
			$aksi       = '<div class="btn-group">';
			$select_beli='<select name="idakun_beli[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_beli).'
						</select>';
			$select_ppn='<select name="idakun_ppn[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_ppn).'
						</select>';
			$select_diskon='<select name="idakun_diskon[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_diskon).'
						</select>';
		
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td>'.$r->nama_barang.'<input type="hidden" name="iddet_pengajuan[]" value="'.$r->id.'"></td>';
			$tbl .='<td>'.$r->merk_barang.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_beli,2).'</td>';
			$tbl .='<td>'.$select_beli.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_ppn,2).'</td>';
			$tbl .='<td>'.$select_ppn.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_diskon,2).'</td>';
			$tbl .='<td>'.$select_diskon.'</td>';
				$aksi .= '<button type="button" title="Lihat Detail" class="btn btn-default btn-sm" onclick="get_transaksi_detail('.$r->iddet.')"><i class="fa fa-eye"></i></button>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
	
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	public function get_transaksi_detail($iddet)
    {
        $q="SELECT B.nama as nama_barang,D.* FROM `tgudang_penerimaan_detail` D
LEFT JOIN view_barang_all B ON B.id=D.idbarang AND B.idtipe=D.idtipe
WHERE D.id='$iddet'";
       
        $result=$this->db->query($q)->row_array();
        $this->output->set_output(json_encode($result));
    }
	function load_detail_gaji(){
		
		$id=$this->input->post('id');
		$idvalidasi=$this->input->post('idvalidasi');
		
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		// if ($id_reff=='1'){
			
		// }
        $q = "SELECT *FROM tvalidasi_kas_04_gaji_detail H WHERE H.idvalidasi='$idvalidasi' ORDER BY H.idjenis,H.id";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		$baris=1;
		$jenis='';
		foreach($rows as $r){
			$no++;
			
			$aksi       = '<div class="btn-group">';
			$select_akun='<select name="idakun_gaji[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun).'
						</select>';
			
		
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'<input type="hidden" name="iddet_gaji[]" value="'.$r->id.'"></td>';
			$tbl .='<td class="text-left">'.($r->idjenis % 2==true?text_primary($r->nama_jenis):text_success($r->nama_jenis)).'</td>';
			$tbl .='<td class="text-left">'.$r->nama_variable.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal,2).'</td>';
			$tbl .='<td>'.$select_akun.'</td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_ringkasan(){
		
		$id=$this->input->post('id');
		$idvalidasi=$this->input->post('idvalidasi');
		$id_reff=$this->input->post('id_reff');
		
		$disabel=$this->input->post('disabel');
		if ($id_reff=='4'){
			$q="
			SELECT T.idakun,A.noakun,A.namaakun
				,(CASE WHEN T.posisi_akun='D' THEN T.nominal ELSE 0 END) as debet
				,(CASE WHEN T.posisi_akun='K' THEN T.nominal ELSE 0 END) as kredit

				FROM(
				SELECT H.id,H.idakun,H.posisi_akun,H.nominal as nominal FROM tvalidasi_kas_04_gaji_detail H
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.id,H.idakun,H.posisi_akun,H.nominal_bayar as nominal FROM tvalidasi_kas_04_gaji_bayar H
				WHERE H.idvalidasi='$idvalidasi'
				) T 
				LEFT JOIN makun_nomor A ON A.id=T.idakun
				WHERE T.nominal > 0
				ORDER BY T.posisi_akun,T.id ASC
			";
		}else{
        $q = "
				SELECT T.idakun,A.noakun,A.namaakun
				,SUM(CASE WHEN T.posisi_akun='D' THEN T.nominal ELSE 0 END) as debet
				,SUM(CASE WHEN T.posisi_akun='K' THEN T.nominal ELSE 0 END) as kredit

				FROM(
				
				SELECT H.idakun,H.posisi_akun,H.nominal from tvalidasi_kas_01_kasbon H 
				WHERE H.idvalidasi='$idvalidasi'

				UNION ALL

				SELECT H.idakun,H.posisi_akun,H.nominal_bayar as nominal FROM tvalidasi_kas_01_kasbon_bayar H
				WHERE H.idvalidasi='$idvalidasi'
				
				UNION ALL
				
				SELECT H.idakun,H.posisi_akun,H.nominal from tvalidasi_kas_02_refund H 
				WHERE H.idvalidasi='$idvalidasi' AND H.idtipe='0'
				
				UNION ALL
				SELECT H.idakun,H.posisi_akun,H.nominal_bayar as nominal FROM tvalidasi_kas_02_refund_bayar H
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idakun_beli as  idakun,H.posisi_beli posisi_akun,H.nominal_beli as nominal FROM tvalidasi_kas_02_refund_obat H
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idakun_diskon as  idakun,H.posisi_diskon posisi_akun,H.nominal_diskon as nominal FROM tvalidasi_kas_02_refund_obat H
				WHERE H.idvalidasi='$idvalidasi'
				
				UNION ALL
				SELECT H.idakun,H.posisi_akun,H.nominal FROM tvalidasi_kas_03_bagi_hasil_detail H
				WHERE H.idvalidasi='$idvalidasi'
				
				
				UNION ALL
				SELECT H.idakun,H.posisi_akun,H.nominal_bayar as nominal FROM tvalidasi_kas_03_bagi_hasil_bayar H
				WHERE H.idvalidasi='$idvalidasi'
				
				UNION ALL
				SELECT H.idakun_dari as idakun,H.posisi_akun_dari as posisi_akun,H.nominal as nominal FROM tvalidasi_kas_05_mutasi H
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idakun_ke as idakun,H.posisi_akun_ke as posisi_akun,H.nominal as nominal FROM tvalidasi_kas_05_mutasi H
				WHERE H.idvalidasi='$idvalidasi'
				
				UNION ALL
				SELECT H.idakun_beli as idakun,H.posisi_beli posisi_akun,H.nominal_beli as nominal FROM tvalidasi_kas_06_pengajuan_detail H
				WHERE H.idvalidasi='$idvalidasi'
				
				UNION ALL
				SELECT H.idakun_diskon as idakun,H.posisi_diskon posisi_akun,H.nominal_diskon as nominal FROM tvalidasi_kas_06_pengajuan_detail H
								WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idakun_ppn as idakun,H.posisi_ppn posisi_akun,H.nominal_ppn as nominal FROM tvalidasi_kas_06_pengajuan_detail H
								WHERE H.idvalidasi='$idvalidasi'
				
				UNION ALL
				SELECT H.idakun as idakun,H.posisi_akun,H.nominal FROM tvalidasi_kas_06_pengajuan_piutang H
				WHERE H.idvalidasi='$idvalidasi'
				
				UNION ALL
				SELECT H.idakun,H.posisi_akun,H.nominal_bayar as nominal FROM tvalidasi_kas_06_pengajuan_pengembalian H
				WHERE H.idvalidasi='$idvalidasi'
				
				UNION ALL
				SELECT H.idakun,H.posisi_akun,H.nominal_bayar as nominal FROM tvalidasi_kas_06_pengajuan_bayar H
				WHERE H.idvalidasi='$idvalidasi'
				
				UNION ALL
				SELECT H.idakun1 as idakun,H.posisi_akun1 posisi_akun,H.nominal as nominal FROM tvalidasi_kas_07_penyesuaian H
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idakun2 as idakun,H.posisi_akun2 posisi_akun,H.nominal as nominal FROM tvalidasi_kas_07_penyesuaian H
				WHERE H.idvalidasi='$idvalidasi'
				
				UNION ALL
				SELECT H.idakun_beli as idakun,H.posisi_beli posisi_akun,H.nominal_beli as nominal FROM tvalidasi_kas_08_gudang_detail H
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idakun_diskon as idakun,H.posisi_diskon posisi_akun,H.nominal_diskon as nominal FROM tvalidasi_kas_08_gudang_detail H
								WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT H.idakun_ppn as idakun,H.posisi_ppn posisi_akun,H.nominal_ppn as nominal FROM tvalidasi_kas_08_gudang_detail H
								WHERE H.idvalidasi='$idvalidasi'
				UNION ALL				
				SELECT H.idakun as idakun,H.posisi_akun as posisi_akun,H.nominal_bayar as nominal FROM tvalidasi_kas_08_gudang_bayar H
								WHERE H.idvalidasi='$idvalidasi'
				UNION ALL				
				SELECT H.idakun as idakun,H.posisi_akun as posisi_akun,H.nominal_bayar as nominal FROM tvalidasi_kas_09_honor_bayar H
				WHERE H.idvalidasi='$idvalidasi'

				UNION ALL 
				SELECT  H.idakun_beban as idakun,H.posisi_beban as posisi_akun,H.nominal_beban as nominal FROM tvalidasi_kas_09_honor H
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL 
				SELECT  H.idakun_pajak as idakun,H.posisi_pajak as posisi_akun,H.nominal_pajak as nominal FROM tvalidasi_kas_09_honor H
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT  H.idakun_pembelian as idakun,H.posisi_pembelian as posisi_akun,H.nominal_pembelian as nominal FROM tvalidasi_kas_09_honor H
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT  H.idakun_berobat as idakun,H.posisi_berobat as posisi_akun,H.nominal_berobat as nominal FROM tvalidasi_kas_09_honor H
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT  H.idakun_kasbon as idakun,H.posisi_kasbon as posisi_akun,H.nominal_kasbon as nominal FROM tvalidasi_kas_09_honor H
				WHERE H.idvalidasi='$idvalidasi'
				UNION ALL
				SELECT  H.idakun_potongan as idakun,H.posisi_potongan as posisi_akun,H.nominal_potongan as nominal FROM tvalidasi_kas_09_honor H
				WHERE H.idvalidasi='$idvalidasi'
						

				) T 
				LEFT JOIN makun_nomor A ON A.id=T.idakun
				WHERE T.nominal > 0
				GROUP BY T.idakun,T.posisi_akun
				ORDER BY T.posisi_akun
				";
		}
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total_D=0;
		$total_K=0;
		foreach($rows as $r){
			$no++;
			$total_D=$total_D + $r->debet;
			$total_K=$total_K + $r->kredit;
			
			
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td class="text-right">'.$r->noakun.'</td>';
			$tbl .='<td>'.$r->namaakun.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->debet,2).'</td>';
			$tbl .='<td class="text-right">'.number_format($r->kredit,2).'</td>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<td colspan="3" class="text-right"><strong>TOTAL</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_D,2).'</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_K,2).'</strong></td>';
		
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function list_akun($list,$idakun){
		$hasil='';
		foreach($list as $row){
			$hasil .='<option value="'.$row->id.'" '.($row->id==$idakun?"selected":"").'>'.$row->noakun.' - '.$row->namaakun.'</option>';
		}
		$hasil .='<option value="0" '.($idakun=="0"?"selected":"").'>TIDAK ADA</option>';
		return $hasil;
	}
}
