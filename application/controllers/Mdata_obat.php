<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdata_obat extends CI_Controller {

	/**
	 * Obat controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mdata_obat_model');
		$this->load->helper('path');
  }

	function index($idkategori='0'){
		// $row=$this->Mdata_obat_model->get_array_kategori('00024-00025');
		// print_r($this->input->post());exit();
		$data = array();
		$data['idkategori'] 			= $idkategori;
		$data['error'] 			= '';
		$data['title'] 			= 'Obat';
		$data['content'] 		= 'Mdata_obat/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Obat",'#'),
									    			array("List",'mdata_obat')
													);
		$data['list_kategori'] = $this->Mdata_obat_model->getKategori();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 											=> '',
			'kode' 										=> '',
			'idkategori' 							=> '',
			'nama' 										=> '',
			'komposisi' 							=> '',
			'indikasi' 								=> '',
			'idtipe' 									=> '',
			'idsatuanbesar' 					=> '',
			'hargasatuanbesar' 				=> '',
			'jumlahsatuanbesar' 			=> '',
			'idsatuankecil' 					=> '',
			'ppn'            		      => '',
			'hargabeli'            		=> '',
			'hargadasar'            	=> '',
			'idrakgudang' 						=> '',
			'idrakfarmasi' 						=> '',
			'alokasiumum' 						=> '',
			'alokasiasuransi' 				=> '',
			'alokasijasaraharja' 			=> '',
			'alokasibpjskesehatan' 		=> '',
			'alokasibpjstenagakerja' 	=> '',
			'marginumum' 							=> '',
			'marginasuransi' 					=> '',
			'marginjasaraharja' 			=> '',
			'marginbpjskesehatan' 		=> '',
			'marginbpjstenagakerja' 	=> '',
			'stokreorder' 						=> '',
			'stokminimum' 						=> '',
			'catatan' 								=> '',
			'status' 									=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Obat';
		$data['content'] 		= 'Mdata_obat/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Obat",'#'),
									    			array("Tambah",'mdata_obat')
													);

		$data['list_kategori'] = $this->Mdata_obat_model->getKategori();
		$data['list_satuan'] = $this->Mdata_obat_model->getSatuan();
		$data['list_rakgudang'] = $this->Mdata_obat_model->getRak(1);
		$data['list_rakfarmasi'] = $this->Mdata_obat_model->getRak(2);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Mdata_obat_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 											=> $row->id,
					'kode' 										=> $row->kode,
					'idkategori' 							=> $row->idkategori,
					'nama' 										=> $row->nama,
					'komposisi' 							=> $row->komposisi,
					'indikasi' 								=> $row->indikasi,
					'idtipe' 									=> $row->idtipe,
					'idsatuanbesar' 					=> $row->idsatuanbesar,
					'hargasatuanbesar' 				=> $row->hargasatuanbesar,
					'jumlahsatuanbesar' 			=> $row->jumlahsatuanbesar,
					'idsatuankecil' 					=> $row->idsatuankecil,
					'ppn'                     => $row->ppn,
					'hargabeli'               => $row->hargabeli,
					'hargadasar'              => $row->hargadasar,
					'idrakgudang' 						=> $row->idrakgudang,
					'idrakfarmasi' 						=> $row->idrakfarmasi,
					'alokasiumum' 						=> $row->alokasiumum,
					'alokasiasuransi' 				=> $row->alokasiasuransi,
					'alokasijasaraharja' 			=> $row->alokasijasaraharja,
					'alokasibpjskesehatan' 		=> $row->alokasibpjskesehatan,
					'alokasibpjstenagakerja' 	=> $row->alokasibpjstenagakerja,
					'marginumum' 							=> $row->marginumum,
					'marginasuransi' 					=> $row->marginasuransi,
					'marginjasaraharja' 			=> $row->marginjasaraharja,
					'marginbpjskesehatan' 		=> $row->marginbpjskesehatan,
					'marginbpjstenagakerja' 	=> $row->marginbpjstenagakerja,
					'stokreorder' 						=> $row->stokreorder,
					'stokminimum' 						=> $row->stokminimum,
					'catatan' 								=> $row->catatan,
					'status' 									=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Obat';
				$data['content']	 	= 'Mdata_obat/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Obat",'#'),
											    			array("Ubah",'mdata_obat')
															);

				$data['list_kategori'] = $this->Mdata_obat_model->getKategori();
				$data['list_satuan'] = $this->Mdata_obat_model->getSatuan();
				$data['list_rakgudang'] = $this->Mdata_obat_model->getRak(1);
        $data['list_rakfarmasi'] = $this->Mdata_obat_model->getRak(2);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mdata_obat','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mdata_obat');
		}
	}

	function delete($id){
		
		$this->Mdata_obat_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mdata_obat','location');
	}
	function aktifkan($id){
		
		$this->Mdata_obat_model->aktifkan($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah aktif kembali.');
		redirect('mdata_obat','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mdata_obat_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mdata_obat','location');
				}
			} else {
				if($this->Mdata_obat_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mdata_obat','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		print_r($data);exit();
		$data['error'] 					= validation_errors();
		$data['content'] 				= 'Mdata_obat/manage';

		$data['list_kategori'] = $this->Mdata_obat_model->getKategori();
		$data['list_satuan'] = $this->Mdata_obat_model->getSatuan();
		$data['list_rakgudang'] = $this->Mdata_obat_model->getRak(1);
		$data['list_rakfarmasi'] = $this->Mdata_obat_model->getRak(2);

		if($id==''){
			$data['title'] = 'Tambah Obat';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Obat",'#'),
															array("Tambah",'mdata_obat')
													);
		}else{
			$data['title'] = 'Ubah Obat';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Obat",'#'),
															array("Ubah",'mdata_obat')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	function filter(){
		
		// // print_r($arr);exit();
		// // $idkategori
		// if($this->input->post('idkategori') != '0'){
			// $idkategori = $this->input->post('idkategori');
			// redirect("mdata_obat/index",'location');
		// }else{
			// redirect('mdata_obat/index/0','location');
		// }
		$data = array();
		$data['idkategori'] 			= $this->input->post('idkategori');
		$data['error'] 			= '';
		$data['title'] 			= 'Obat';
		$data['content'] 		= 'Mdata_obat/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Obat",'#'),
									    			array("List",'mdata_obat')
													);
		$data['list_kategori'] = $this->Mdata_obat_model->getKategori();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function getIndex($idkategori='0')
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		$idkategori=$this->input->post('idkategori');
		$row=$this->Mdata_obat_model->get_array_kategori($idkategori);
		// print_r($row);exit();
			$this->select = array('mdata_obat.*', 'mdata_kategori.nama AS namakategori');
			$this->from   = 'mdata_obat';
			$this->join 	= array(
				array("mdata_kategori", "mdata_kategori.id = mdata_obat.idkategori", "")
			);
				// $this->where  = array(
				// 'mdata_obat.status' => '1'
				// );
			// $row = array('38','39');
				$this->where_in = array(
					'mdata_obat.idkategori' => $row
				);
				// $this->or_where = array(
					// 'mdata_obat.idkategori' => '38',
					// 'mdata_obat.idkategori' => '49',
					// 'mdata_obat.idkategori' => '79'
				// );
				
				// $this->session->set_userdata('sessionOrWhere', $this->or_where);
				// $this->where = array_merge($this->where, array('mdata_obat.idkategori' => '79'));
			// if ($idkategori !='0'){
				
				// $names = array('38');
				// $this->where_in  = array(
					// 'mdata_obat.idkategori' => $names
				// );
				// // $this->where_in=array('mdata_obat.idkategori'=> $names);
			// }else{
				// $this->where  = array(
				// 'mdata_obat.status' => '1'
				// );
			// }
			
			$this->order  = array(
				'mdata_obat.kode' => 'DESC'
			);
			$this->group  = array();

			$this->column_search   = array('mdata_obat.kode','mdata_kategori.nama','mdata_obat.nama');
			$this->column_order    = array('mdata_obat.kode','mdata_kategori.nama','mdata_obat.nama');
			
			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->kode;
					$row[] = $r->nama;
					$row[] = $r->namakategori;
					$row[] = number_format($r->hargasatuanbesar,0);
					$row[] = number_format($r->hargadasar,0);
					$row[] = StatusBarang($r->status);
					$aksi = '<div class="btn-group">';
					if ($r->status=='1'){
						if (UserAccesForm($user_acces_form,array('113','114','115','116'))){
							$aksi .= '<a href="'.site_url().'mdata_obat/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
						}
						if (UserAccesForm($user_acces_form,array('117'))){
							$aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_obat" data-urlremove="'.site_url().'mdata_obat/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
						}
					}else{
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_obat" data-urlremove="'.site_url().'mdata_obat/aktifkan/'.$r->id.'" data-toggle="tooltip" title="Aktifkan" class="btn btn-success btn-sm aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
					}
		            $aksi .= '</div>';
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
}
