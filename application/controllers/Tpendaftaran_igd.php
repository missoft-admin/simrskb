<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpendaftaran_igd extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Tpendaftaran_poli_model');
		$this->load->helper('path');
		
  }

 
	function tindakan($pendaftaran_id='',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data_satuan_ttv=array();
		$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
		// $data=array();
		$data=$this->Tpendaftaran_poli_ttv_model->get_data_header($pendaftaran_id);
		
		
		// $data=$this->db->query($q)->row_array();
		// print_r($default_logic);exit;
		$data['st_cetak']=0;
		$data_header_ttv=$this->Tpendaftaran_poli_ttv_model->get_header_ttv($pendaftaran_id);
		
		$data_login_ppa=get_ppa_login();
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		// $data['array_menu_kiri'] 	= $this->Tpendaftaran_poli_ttv_model->list_menu_kiri($menu_atas);
		// $data['array_menu_kiri'] 	= array('input_ttv','input_data');
		
		// print_r(json_encode($data['array_menu_kiri']));exit;
		$data['idpoli'] 			= '#';
		
		$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
		$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
		$data['assesmen_id'] 			= '';
		$data['status_assesmen'] 			= '1';
		$data['error'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'My Pasien';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Tindakan Poliklinik",'#'),
											  array("My Pasien",'tpendaftaran_poli_ttv')
											);
		$logic_akses_ttv=$this->Tpendaftaran_poli_ttv_model->logic_akses_ttv($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_assemen_rj=$this->Tpendaftaran_poli_ttv_model->logic_akses_assemen_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_asmed=$this->Tpendaftaran_poli_ttv_model->logic_akses_asmed_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_nyeri=$this->Tpendaftaran_poli_ttv_model->logic_akses_nyeri_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		
		if ($menu_kiri=='input_ttv'){
			$data_ttv=$this->Tpendaftaran_poli_ttv_model->get_data_ttv($pendaftaran_id);
			$setting_ttv=$this->Tpendaftaran_poli_ttv_model->setting_ttv();
			$data = array_merge($data_ttv,$data,$setting_ttv);
		}
		
		if ($menu_kiri=='his_assesmen_rj'){
			$data_assemen['riwayat_alergi']=$data['riwayat_alergi_header'];
			// print_r($data);exit;
		}
		
		if ($menu_kiri=='input_assesmen_rj' || $menu_kiri=='his_assesmen_rj'){
			
			if ($trx_id!='#'){
				if ($menu_kiri=='his_assesmen_rj'){
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_assesmen_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpendaftaran_poli_ttv_model->get_data_assesmen_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_assesmen_trx($trx_id);
				// print_r($data_assemen);exit;
				}
				
			}else{
				$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_assesmen($pendaftaran_id);
				
				
			}
			if ($data_assemen){
				
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['ttd_nama']='';
				$data_assemen['ttd_hubungan']='';
				$data_assemen['jenis_ttd']='1';
				
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			if ($menu_kiri=='his_assesmen_rj'){
				$data_ttv=$this->Tpendaftaran_poli_ttv_model->get_data_ttv_assesmen_trx($data_assemen['assesmen_id'],$versi_edit);
				$data_ttv_2=$this->Tpendaftaran_poli_ttv_model->get_data_ttv_assesmen_trx_last($data_assemen['assesmen_id'],$versi_edit);
				// $list_edukasi=$this->Tpendaftaran_poli_ttv_model->list_edukasi_his($data_assemen['assesmen_id'],$versi_edit);
				$data_edukasi=$this->Tpendaftaran_poli_ttv_model->perubahan_edukasi($data_assemen['assesmen_id'],$versi_edit);
				$data_ttv = array_merge($data_ttv,$data_ttv_2,$data_edukasi);
				
				
			}else{
				$data_ttv=$this->Tpendaftaran_poli_ttv_model->get_data_ttv_assesmen($data_assemen['assesmen_id']);
				$data_assemen['riwayat_alergi']=$data['riwayat_alergi_header'];
				$list_edukasi=$this->Tpendaftaran_poli_ttv_model->list_edukasi($data_assemen['assesmen_id']);
				$data_assemen['list_edukasi']=$list_edukasi;
				$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
				$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
				$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			}
			$data_assemen['template_id']=$this->db->query("SELECT id FROM `mrisiko_jatuh` WHERE idtipe='1' AND staktif='1' LIMIT 1")->row('id');
			$list_riwayat_penyakit=$this->Tpendaftaran_poli_ttv_model->list_riwayat_penyakit($data_assemen['assesmen_id']);
			$list_template_assement=$this->Tpendaftaran_poli_ttv_model->list_template_assement();
			$data_assemen['list_template_assement']=$list_template_assement;
			$data_assemen['list_riwayat_penyakit']=$list_riwayat_penyakit;
			// print_r($list_edukasi);exit;
			
			$setting_assesmen=$this->Tpendaftaran_poli_ttv_model->setting_assesmen();
			$data = array_merge($data,$setting_assesmen,$data_ttv,$data_assemen);
			$data['pendaftaran_id']=$pendaftaran_id;
		}
		
		// print_r($data);exit;
		$data = array_merge($data,$data_login_ppa,$data_satuan_ttv,$data_header_ttv,$logic_akses_ttv,$logic_akses_assemen_rj,$logic_akses_asmed,$logic_akses_nyeri, backend_info());
			// print_r($list_template_assement);exit;
		$this->parser->parse('module_template', $data);
		
	}
	
}
