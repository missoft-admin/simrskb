<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_gabung extends CI_Controller
{

    /**
     * Gabung Rekam Medis controller.
     * Developer @GunaliRezqiMauludi
     */

    function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Trm_gabung_model', 'model');
    }

    function index($idpasien='')
    {
		
        $data = array();
        $data['error']     = '';
        $data['title']     = 'Gabung Rekam Medis';
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1193'))==false && UserAccesForm($user_acces_form,array('1224'))==true){
			redirect('trm_gabung_history');
			
		}else{
		$data['content']   = 'Trm_gabung/manage';
		}
	
        $data['breadcrum'] = array(
					array("RSKB Halmahera",'#'),
          array("Gabung Rekam Medis",'#'),
          array("Tambah",'trm_gabung')
        );
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        $data['idpasien']  = $idpasien;
		
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function save(){
			if($this->model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('trm_gabung','location');
			}
  	}

    function getPasien()
    {
			$this->select = array();
    		$this->join 	= array();
    		$this->where  = array();

        if(isset($_POST['snomedrec']) && $_POST['snomedrec'] != ''){
          $this->where = array_merge($this->where, array('mfpasien.no_medrec LIKE ' => '%'.$_POST['snomedrec'].'%'));
        }

        if(isset($_POST['snamapasien']) && $_POST['snamapasien'] != ''){
          $this->where = array_merge($this->where, array('mfpasien.nama LIKE ' => '%'.$_POST['snamapasien'].'%'));
        }

        if(isset($_POST['snama_keluarga']) && $_POST['snama_keluarga'] != ''){
          $this->where = array_merge($this->where, array('mfpasien.nama_keluarga' => $_POST['snama_keluarga']));
        }

        if(isset($_POST['salamat']) && $_POST['salamat'] != ''){
          $this->where = array_merge($this->where, array('mfpasien.alamat_jalan LIKE ' => '%'.$_POST['salamat'].'%'));
        }

        if(isset($_POST['stanggallahir']) && $_POST['stanggallahir'] != ''){
          $this->where = array_merge($this->where, array('DATE(mfpasien.tanggal_lahir) LIKE ' => '%'.YMDFormat($_POST['stanggallahir']).'%'));
        }

        if(isset($_POST['snotelepon']) && $_POST['snotelepon'] != ''){
          $this->where = array_merge($this->where, array('mfpasien.telepon LIKE ' => '%'.$_POST['snotelepon'].'%'));
        }

    		$this->order  = array(
    			'id' => 'DESC'
    		);
    		$this->group  = array();
    		$this->from   = 'mfpasien';

        $this->column_search   = array('no_medrec','nama','nama_keluarga','tanggal_lahir', 'alamat_jalan', 'telepon', 'telepon_keluarga');
        $this->column_order    = array('no_medrec','nama','nama_keluarga','tanggal_lahir', 'alamat_jalan', 'telepon', 'telepon_keluarga');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = "<a href='#' class='selectPasien' data-idpasien='".$r->id."' data-dismiss='modal'>".$r->no_medrec."</span></a>";
            $row[] = $r->nama;
            $row[] = ($r->jenis_kelamin == 1 ? 'Laki-laki' : 'Perempuan');
            $row[] = HumanDateShort($r->tanggal_lahir);
            $row[] = $r->alamat_jalan;
            $row[] = $r->telepon;
            $row[] = $r->nama_keluarga;
            $row[] = $r->telepon_keluarga;

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
}
