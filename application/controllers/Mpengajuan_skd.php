<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Mpengajuan_skd extends CI_Controller
{
    /**
     * Pengajuan SKD controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mpengajuan_skd_model');
    }

    public function index()
    {
        $data = array();
        $data['error'] 			= '';
        $data['title'] 			= 'Pengajuan SKD';
        $data['content'] 		= 'Mpengajuan_skd/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Pengajuan SKD",'#'),
            array("List",'mpengajuan_skd')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create()
    {
        $data = array(
            'id' => '',
            'nama' => '',
            'estimasi' => '',
            'status' => ''
        );

        $data['error'] 			= '';
        $data['title'] 			= 'Tambah Pengajuan SKD';
        $data['content'] 		= 'Mpengajuan_skd/manage';
        $data['breadcrum']	= array(
            array("RSKB Halmahera",'#'),
            array("Pengajuan SKD",'#'),
            array("Tambah",'mpengajuan_skd')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id)
    {
        if ($id != '') {
            $row = $this->Mpengajuan_skd_model->getSpecified($id);
            if (isset($row->id)) {
                $data = array(
                    'id' => $row->id,
                    'nama' => $row->nama,
                    'estimasi' => $row->estimasi,
                    'status' => $row->status,
                );

                $data['error'] 			= '';
                $data['title'] 			= 'Ubah Pengajuan SKD';
                $data['content']	 	= 'Mpengajuan_skd/manage';
                $data['breadcrum'] 	= array(
                    array("RSKB Halmahera",'#'),
                    array("Pengajuan SKD",'#'),
                array("Ubah",'mpengajuan_skd')
                );

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mpengajuan_skd/index', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mpengajuan_skd/index');
        }
    }

    public function save()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');
        $this->form_validation->set_rules('estimasi', 'Estimasi', 'trim|required');

        if ($this->form_validation->run() == true) {
            if ($this->input->post('id') == '') {
                if ($this->Mpengajuan_skd_model->saveData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mpengajuan_skd/index', 'location');
                }
            } else {
                if ($this->Mpengajuan_skd_model->updateData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mpengajuan_skd/index', 'location');
                }
            }
        } else {
            $this->failed_save($this->input->post('id'));
        }
    }

    public function delete($id)
    {
        if ($this->Mpengajuan_skd_model->softDelete($id)) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah dihapus.');
            redirect('mpengajuan_skd/index', 'location');
        }
    }

    public function failed_save($id)
    {
        $data = $this->input->post();
        $data['error'] = validation_errors();
        $data['content'] = 'Mpengajuan_skd/manage';

        if ($id=='') {
            $data['title'] = 'Tambah Pengajuan SKD';
            $data['breadcrum'] = array(
                    array("RSKB Halmahera",'#'),
                    array("Pengajuan SKD",'#'),
                    array("Tambah",'mpengajuan_skd')
            );
        } else {
            $data['title'] = 'Ubah Pengajuan SKD';
            $data['breadcrum'] = array(
                    array("RSKB Halmahera",'#'),
                    array("Pengajuan SKD",'#'),
                    array("Ubah",'mpengajuan_skd')
            );
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function getIndex()
    {
        $this->select = array('*');
        $this->from   = 'mpengajuan_skd';
        $this->join 	= array();

        $this->order  = array(
                'id' => 'DESC'
            );

        $this->group  = array();

        $this->column_search   = array('nama');

        $this->column_order    = array('nama');

        $list = $this->datatable->get_datatables();

        $no = $_POST['start'];

        $data = array();
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->nama;
            $row[] = $r->estimasi;
            $row[] = LabelSettingAnalisa($r->status);
            $row[] = '<div class="btn-group">
							<a href="'.site_url().'mpengajuan_skd/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
							<a href="'.site_url().'mpengajuan_skd/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-success btn-sm"><i class="fa fa-trash"></i></a>
						</div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );

        echo json_encode($output);
    }
}
