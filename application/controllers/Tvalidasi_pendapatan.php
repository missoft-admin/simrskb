<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tvalidasi_pendapatan extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tvalidasi_pendapatan_model','model');
	}

	function index() {
		$data=array(
			'st_posting'=>'#',
			'tipe'=>'#',
			'iddari'=>'#',
			'nojurnal'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'#',
			'tanggalterima1'=>'',
			'tanggalterima2'=>'',
			'tanggal_trx1'=>'',
			'tanggal_trx2'=>'',
		);
		$data['error'] 			= '';
		$data['list_pendapatan'] 	= $this->model->list_pendapatan();
		$data['list_dari'] 	= $this->model->list_dari();
		$data['title'] 			= 'Validasi Jurnal Pendapatan Lain - Lain';
		$data['content'] 		= 'Tvalidasi_pendapatan/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Pendapatan Lain - Lain",'tvalidasi_pendapatan/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function detail($id,$disabel='') {
		$data=$this->model->getHeader($id);
		// print_r($data);exit();
		$data['error'] 			= '';
		$data['disabel'] 			= $disabel;
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['title'] 			= 'Detail Validasi Akuntansi';
		$data['content'] 		= 'Tvalidasi_pendapatan/detail';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Pendapatan Lain - Lain",'tvalidasi_pendapatan/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        $userid = $this->session->userdata('user_id');
		$batas_batal=$this->db->query("SELECT batas_batal FROM msetting_jurnal_pendapatan WHERE id='1'")->row('batas_batal');
		
		$idpendapatan=$this->input->post('idpendapatan');
		$nojurnal=$this->input->post('nojurnal');
		$no_terima=$this->input->post('no_terima');
		$iddari=$this->input->post('iddari');
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		$st_posting=$this->input->post('st_posting');
		$where='';
		
		
		if ($idpendapatan !='#'){
			$where .=" AND H.idpendapatan='$idpendapatan' ";
		}
		if ($st_posting !='#'){
			$where .=" AND H.st_posting='$st_posting' ";
		}
		
		if ($nojurnal !=''){
			$where .=" AND H.nojurnal='$nojurnal' ";
		}
		if ($no_terima !=''){
			$where .=" AND H.notransaksi='$no_terima' ";
		}
		
		if ($iddari !='#'){
			$where .=" AND H.st_iddari='$iddari' ";
		}
		
		
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(H.tanggal_transaksi) >='".YMDFormat($tanggal_trx1)."' AND DATE(H.tanggal_transaksi) <='".YMDFormat($tanggal_trx2)."'";
        }
        $from = "(
					SELECT H.id,H.idtransaksi,H.st_posting,H.nojurnal,H.notransaksi,H.tanggal_transaksi,H.nama_pendapatan,H.keterangan_pendapatan
					,H.nominal_pendapatan,IFNULL(DATEDIFF(NOW(), H.posting_date),0) as umur_posting,H.terimadar_nama
					FROM tvalidasi_pendapatan H

					WHERE H.`status`='1' ".$where."
				) as tbl";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nojurnal','nama_bank','notransaksi','keterangan_pendapatan','nama_pendapatan');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

			
        foreach ($list as $r) {
            $no++;
            $row = array();
			$disabel_btn='';
			
			if ($r->umur_posting>$batas_batal){
				$disabel_btn='disabled';
			}
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url_pendapatan        = site_url('tpendapatan/');
            $url_deposit        = site_url('tverifikasi_deposit/');
            $url_validasi        = site_url('tvalidasi_pendapatan/');
            $url_batal       = site_url('tgudang_pengembalian/');
            $row[] = $r->id;
            $row[] = $r->st_posting;
            $row[] = $no;
            $row[] = $r->nojurnal;
            $row[] = $r->notransaksi;
            $row[] = HumanDateShort($r->tanggal_transaksi);
            $row[] = $r->nama_pendapatan;
            $row[] = $r->keterangan_pendapatan;
            $row[] = $r->terimadar_nama;
            $row[] = number_format($r->nominal_pendapatan,2);
           
            $row[] = ($r->st_posting=='1'?text_success('TELAH DIPOSTING'):text_default('MENUGGU DIPOSTING'));
					$aksi .= '<a href="'.$url_pendapatan.'update/'.$r->idtransaksi.'/disabled" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
					$aksi .= '<a href="'.$url_validasi.'detail/'.$r->id.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
					if ($r->st_posting=='1'){
					$aksi .= '<button title="Batal Posting" '.$disabel_btn.' class="btn btn-xs btn-danger batalkan"><i class="fa fa-close"></i></button>';						
					}else{
					$aksi .= '<button title="Posting" class="btn btn-xs btn-success posting"><i class="si si-arrow-right"></i></button>';
						
					}
					$aksi .= '<a href="'.$url_validasi.'print_kwitansi_deposit/'.$r->idtransaksi.'" target="_blank" title="Print Kwitansi" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';
					// $aksi .= '<button title="Print" class="btn btn-xs btn-success"><i class="fa fa-print"></i></button>';
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	
	function iddari($tipe){
		if ($tipe=='1'){
			$tipe='<span class="label label-success">TUNAI</span>';
		}elseif($tipe=='2'){
			$tipe='<span class="label label-danger">KREDIT</span>';
		}else{
			$tipe='<span class="label label-default">-</span>';
		}
		
		return $tipe;
	}
	
	public function posting()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'1',
			'posting_by'=>$this->session->userdata('user_id'),
			'posting_nama'=>$this->session->userdata('user_name'),
            'posting_date'=>date('Y-m-d H:i:s')
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_pendapatan', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function posting_all()
    {
        $id = $this->input->post('id');
		foreach($id as $index=>$val){
			 $data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $val);
			$result=$this->db->update('tvalidasi_pendapatan', $data);
		}
       
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	
	public function batalkan()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'0',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_pendapatan', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function get_transaksi_detail($iddet)
    {
        $q="SELECT B.nama as nama_barang,D.* FROM `tgudang_penerimaan_detail` D
LEFT JOIN view_barang_all B ON B.id=D.idbarang AND B.idpendapatan=D.idpendapatan
WHERE D.id='$iddet'";
       
        $result=$this->db->query($q)->row_array();
        $this->output->set_output(json_encode($result));
    }
	function save_detail(){
		// print_r($this->input->post());exit();
		// $id=$this->model->saveData();
		if($this->model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tvalidasi_pendapatan','location');
		}
	
	}
	function load_detail(){
		
		$id=$this->input->post('id');
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT D.id,K.nama_tipe as nama_tipe,D.nama_barang,D.nominal_beli,D.nominal_ppn,D.nominal_diskon,D.idakun_beli,D.idakun_ppn,D.idakun_diskon 
				,D.idet
				from tvalidasi_pendapatan_detail D
				LEFT JOIN mdata_tipebarang K ON K.id=D.idpendapatan
				WHERE D.idvalidasi='$id'
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			
			$aksi       = '<div class="btn-group">';
			$select_beli='<select name="idakun_beli[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_beli).'
						</select>';
			$select_ppn='<select name="idakun_ppn[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_ppn).'
						</select>';
			$select_diskon='<select name="idakun_diskon[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_diskon).'
						</select>';
		
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td>'.$r->nama_tipe.'<input type="hidden" name="iddet[]" value="'.$r->id.'"></td>';
			$tbl .='<td>'.$r->nama_barang.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_beli,2).'</td>';
			$tbl .='<td>'.$select_beli.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_ppn,2).'</td>';
			$tbl .='<td>'.$select_ppn.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_diskon,2).'</td>';
			$tbl .='<td>'.$select_diskon.'</td>';
				$aksi .= '<button type="button" title="Lihat Detail" class="btn btn-default btn-sm" onclick="get_transaksi_detail('.$r->idet.')"><i class="fa fa-eye"></i></button>';
			$tbl .='<td class="text-center">'.$aksi.'</td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		// $tbl .='<td colspan="4" class="text-right"><strong>TOTAL</strong></td>';
		// $tbl .='<td class="text-right"><strong>'.number_format($total,0).'</strong></td>';
		// $tbl .='<td></td>';
		// $tbl .='<td></td>';
		// $tbl .='<td></td>';
		// $tbl .='<td></td>';
		
		// $tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_ringkasan(){
		
		$id=$this->input->post('id');
		
		$disabel=$this->input->post('disabel');
		
        $q = "
				SELECT * FROM (
				SELECT 
				H.tanggal_transaksi as tanggal,H.idtransaksi as ref_tabel_id,'tgudang_penerimaan' as ref_tabel
				,'tvalidasi_pendapatan' as ref_tabel_validasi,H.id as idvalidasi,H.notransaksi,D.idakun_beli as idakun,D.noakun_beli as noakun,D.namaakun_beli as namaakun
				,SUM(CASE WHEN D.posisi_beli='D' THEN D.nominal_beli ELSE 0 END) debet
				,SUM(CASE WHEN D.posisi_beli='K' THEN D.nominal_beli ELSE 0 END) kredit
				,'PEMBELIAN' as keterangan,CASE WHEN H.st_auto_posting='1' THEN 'AUTO POSTING' ELSE 'MANUAL' END as keterangan_code
				,H.created_by,H.created_date,H.created_nama
				FROM tvalidasi_pendapatan H
				LEFT JOIN tvalidasi_pendapatan_detail D ON D.idvalidasi=H.id
				WHERE H.id='$id'
				GROUP BY H.id,D.idakun_beli


				UNION ALL


				SELECT 
				H.tanggal_transaksi as tanggal,H.idtransaksi as ref_tabel_id,'tgudang_penerimaan' as ref_tabel
				,'tvalidasi_pendapatan' as ref_tabel_validasi,H.id as idvalidasi,H.notransaksi,D.idakun_ppn as idakun,D.noakun_ppn as noakun,D.namaakun_ppn as namaakun
				,SUM(CASE WHEN D.posisi_ppn='D' THEN D.nominal_ppn ELSE 0 END) debet
				,SUM(CASE WHEN D.posisi_ppn='K' THEN D.nominal_ppn ELSE 0 END) kredit
				,'PPN' as keterangan,CASE WHEN H.st_auto_posting='1' THEN 'AUTO POSTING' ELSE 'MANUAL' END as keterangan_code
				,H.created_by,H.created_date,H.created_nama
				FROM tvalidasi_pendapatan H
				LEFT JOIN tvalidasi_pendapatan_detail D ON D.idvalidasi=H.id
				WHERE H.id='$id'
				GROUP BY H.id,D.idakun_ppn

				UNION ALL

				SELECT 
				H.tanggal_transaksi as tanggal,H.idtransaksi as ref_tabel_id,'tgudang_penerimaan' as ref_tabel
				,'tvalidasi_pendapatan' as ref_tabel_validasi,H.id as idvalidasi,H.notransaksi,D.idakun_diskon as idakun,D.noakun_diskon as noakun,D.namaakun_diskon as namaakun
				,SUM(CASE WHEN D.posisi_diskon='D' THEN D.nominal_diskon ELSE 0 END) debet
				,SUM(CASE WHEN D.posisi_diskon='K' THEN D.nominal_diskon ELSE 0 END) kredit
				,'DISKON' as keterangan,CASE WHEN H.st_auto_posting='1' THEN 'AUTO POSTING' ELSE 'MANUAL' END as keterangan_code
				,H.created_by,H.created_date,H.created_nama
				FROM tvalidasi_pendapatan H
				LEFT JOIN tvalidasi_pendapatan_detail D ON D.idvalidasi=H.id
				WHERE H.id='$id'
				GROUP BY H.id,D.idakun_diskon

				UNION ALL

				SELECT 
				H.tanggal_transaksi as tanggal,H.idtransaksi as ref_tabel_id,'tgudang_penerimaan' as ref_tabel
				,'tvalidasi_pendapatan' as ref_tabel_validasi,H.id as idvalidasi,H.notransaksi,D.idakun as idakun,D.noakun as noakun,D.namaakun as namaakun
				,SUM(CASE WHEN D.posisi_akun='D' THEN D.nominal ELSE 0 END) debet
				,SUM(CASE WHEN D.posisi_akun='K' THEN D.nominal ELSE 0 END) kredit
				,'PEMBAYARAN' as keterangan,CASE WHEN H.st_auto_posting='1' THEN 'AUTO POSTING' ELSE 'MANUAL' END as keterangan_code
				,H.created_by,H.created_date,H.created_nama
				FROM tvalidasi_pendapatan H
				LEFT JOIN tvalidasi_pendapatan_bayar D ON D.idvalidasi=H.id
				WHERE H.id='$id'
				GROUP BY H.id,D.idakun

				) T WHERE (T.debet+T.kredit) > 0



				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total_D=0;
		$total_K=0;
		foreach($rows as $r){
			$no++;
			$total_D=$total_D + $r->debet;
			$total_K=$total_K + $r->kredit;
			
			
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td>'.$r->noakun.'</td>';
			$tbl .='<td>'.$r->namaakun.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->debet,2).'</td>';
			$tbl .='<td class="text-right">'.number_format($r->kredit,2).'</td>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<td colspan="3" class="text-right"><strong>TOTAL</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_D,2).'</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_K,2).'</strong></td>';
		
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function list_akun($list,$idakun){
		$hasil='';
		foreach($list as $row){
			$hasil .='<option value="'.$row->id.'" '.($row->id==$idakun?"selected":"").'>'.$row->noakun.' - '.$row->namaakun.'</option>';
		}
		return $hasil;
	}
}
