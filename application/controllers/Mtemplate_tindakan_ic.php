<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtemplate_tindakan_ic extends CI_Controller {

	/**
	 * Master Template Tindakan Pembedahan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtemplate_tindakan_ic_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Template Tindakan Pembedahan';
		$data['content'] 		= 'Mtemplate_tindakan_ic/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Template Tindakan Pembedahan",'#'),
									    			array("List",'mtemplate_tindakan_ic')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'staktif' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Template Tindakan Pembedahan';
		$data['content'] 		= 'Mtemplate_tindakan_ic/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Template Tindakan Pembedahan",'#'),
									    			array("Tambah",'mtemplate_tindakan_ic')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mtemplate_tindakan_ic_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					
					'staktif' 				=> $row->staktif
				);
				$data['inisial'] 			= '';
				$data['st_nilai'] 			= '1';
				$data['st_default'] 			= '0';
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Template Tindakan Pembedahan';
				$data['content']	 	= 'Mtemplate_tindakan_ic/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Template Tindakan Pembedahan",'#'),
											    			array("Ubah",'mtemplate_tindakan_ic')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtemplate_tindakan_ic','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtemplate_tindakan_ic');
		}
	}
	

	function delete($id){
		$this->Mtemplate_tindakan_ic_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mtemplate_tindakan_ic','location');
	}
	function load_index_informasi_tindakan_ic(){
	  $id=$this->input->post('id');
	 
	  $q="SELECT H.*,M.nama,M.nama_english,M.nourut FROM mtemplate_tindakan_ic_informasi H
			LEFT JOIN mjenis_info M ON M.id=H.tindakan_id
			WHERE H.template_id='$id'

			ORDER BY M.nourut";
	  $list_info=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_info as $r){
		  // style="text-align: center;"
		  $tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="informasi_id" value="'.$r->id.'">'.$r->nourut.'</td>';
			$tabel .='<td class="text-center"><strong>'.$r->nama.'</strong><br><i>'.$r->nama_english.'</i></td>';
			$tabel .='<td><textarea class="js-summernote form-control auto_blur_tabel " name="story" rows="3" style="width:100%">'.$r->isi_informasi.'</textarea></td>';
		  $tabel .='</tr>';
	  } 
	 
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function update_isi_informasi_tindakan_ic(){
		$assesmen_id=$this->input->post('assesmen_id');
		$id=$this->input->post('informasi_id');
		$isi_informasi=$this->input->post('isi');
		
		
		$data=array(
			'isi_informasi' => $isi_informasi,
			);
		$this->db->where('id',$id);
		$result=$this->db->update('mtemplate_tindakan_ic_informasi',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
	}
	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->Mtemplate_tindakan_ic_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtemplate_tindakan_ic/update/'.$id,'location');
				}
			} else {
				if($this->Mtemplate_tindakan_ic_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtemplate_tindakan_ic/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mtemplate_tindakan_ic/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Template Tindakan Pembedahan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Template Tindakan Pembedahan",'#'),
															array("Tambah",'mtemplate_tindakan_ic')
													);
		}else{
			$data['title'] = 'Ubah Master Template Tindakan Pembedahan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Template Tindakan Pembedahan",'#'),
															array("Ubah",'mtemplate_tindakan_ic')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
			$this->from   = 'mtemplate_tindakan_ic';
			$this->join 	= array();
			$this->where  = array(
				'staktif' => '1'
			);
			$this->order  = array(
				'nama' => 'ASC'
			);
			$this->group  = array();
		
		$this->column_search   = array('nama','isi_header');
      $this->column_order    = array('nama','isi_header');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama;
		$aksi = '<div class="btn-group">';
       
		if (UserAccesForm($user_acces_form,array('2419'))){
            $aksi .= '<a href="'.site_url().'mtemplate_tindakan_ic/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
        }
        if (UserAccesForm($user_acces_form,array('2420'))){
            $aksi .= '<a href="#" data-urlindex="'.site_url().'mtemplate_tindakan_ic" data-urlremove="'.site_url().'mtemplate_tindakan_ic/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
        }
        $aksi .= '</div>';
		$row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
}
