<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tgudang_verifikasi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('tgudang_verifikasi_model','model');
		$this->load->model('Tgudang_pemesanan_model','gudang_model');
	}

	function index($status='3') {
		$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d'));
		date_add($date1,date_interval_create_from_date_string("-30 days"));
		date_add($date2,date_interval_create_from_date_string("15 days"));
		$date1= date_format($date1,"Y-m-d");
		$date2= date_format($date2,"Y-m-d");

		$data = array(
            'filter_distributor' => '#',
            'statuspesan' => $status,
            'tanggaldari' => HumanDateShort($date1),
            'tanggalsampai' => HumanDateShort(date('Y-m-d')),
        );
		$this->session->set_userdata($data);
		$data['error'] 			= '';
		$data['list_distributor'] 			= $this->model->list_distributor();
		$data['title'] 			= 'Verifikasi Pemesanan Gudang';
		$data['content'] 		= 'Tgudang_verifikasi/index';
		// $data['tanggaldari'] 		= date('d-m-Y');
		// $data['tanggalsampai'] 		= date('d-m-Y');
		$data['breadcrum'] 	= array(array("RSKB Halmahera",'#'),
							  array("tgudang_pemesanan",'#'),
							  array("List",'tgudang_pemesanan'));
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	public function filter() {
		// print_r($this->input->post('tanggaldari'));exit();
        $data = array(
            'filter_distributor' => $this->input->post('filter_distributor'),
            'statuspesan' => $this->input->post('statuspesan'),
            'tanggaldari' => $this->input->post('tanggaldari'),
            'tanggalsampai' => $this->input->post('tanggalsampai'),
        );
		$this->session->set_userdata($data);
		$data['list_distributor'] 			= $this->model->list_distributor();
        
        $data['error'] = '';
        $data['title'] = 'Pemesanan Gudang';
        $data['content'] = 'Tgudang_verifikasi/index';
        $data['breadcrum'] 	= array(array("RSKB Halmahera",'#'),
							  array("tgudang_pemesanan",'#'),
							  array("Filter",'tgudang_pemesanan'));
		// print_r($data);exit();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	public function getsorted_distributor_2($idtipe='3',$idbarang='1'){
		$this->model->getsorted_distributor_2($idtipe,$idbarang);
	}
	function view() {
		$id = $this->uri->segment(3);
		if($id) {
			$data 					= $this->model->viewHead($id);
			$data['error'] 			= '';
			$data['title'] 			= 'Detail Pemesanan Gudang';
			$data['content'] 		= 'Tgudang_verifikasi/view';
			$data['breadcrum'] 	= array(array("RSKB Halmahera",'#'),
								  array("Pemesanan Gudang",'tgudang_pemesanan'),
								  array("View",'#'));
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);			
		} else {
			show_404();
		}
	}
	public function selectbarang_all() {
        $this->model->selectbarang_all();
    }
	public function selected_distributor() {
        $this->model->selected_distributor();
    }
	function create() {
		$data = array(
			'id' 					=> '',
			'nopemesanan' 			=> '',
			'iddistributor' 		=> '',
			'totalbarang' 			=> '',
			'totalharga' 			=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Pemesanan Gudang';
		$data['content'] 		= 'Tgudang_verifikasi/manage';
		$data['breadcrum']		= array(array("RSKB Halmahera",'#'),
							  		array("Pemesanan Gudang",'#'),
							  		array("Create",''));
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function save(){
		$this->model->save();
	}
	public function save_edit(){
		if ($this->model->save_edit()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
            redirect('tgudang_approval/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_approval/index', 'refresh');
        }
		// print_r($this->input->post());exit();
	}
	public function save_finalisasi(){
		if ($this->model->save_finalisasi()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
            redirect('tgudang_verifikasi/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_verifikasi/index', 'refresh');
        }
		// print_r($this->input->post());exit();
	}
	public function save_edit_pesanan(){
		if ($this->model->save_edit_pesanan()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
            redirect('tgudang_verifikasi/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_verifikasi/index', 'refresh');
        }
		// print_r($this->input->post());exit();
	}
	public function setujui($id){
		if ($this->model->setujui($id)) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disetujui.');
            redirect('tgudang_verifikasi/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_verifikasi/index', 'refresh');
        }
		// print_r($this->input->post());exit();
	}
	function update() {
		$this->model->update();
	}

    function ajax_list(){
        $userid = $this->session->userdata('user_id');
        $from = "(SELECT
        tgudang_pemesanan.*,
        mdistributor.nama
        FROM tgudang_pemesanan
        LEFT JOIN mdistributor ON tgudang_pemesanan.iddistributor = mdistributor.id
        WHERE tgudang_pemesanan.stdraft != 2
        AND tipepemesanan IN (SELECT gudangtipe FROM musers_tipegudang WHERE iduser = ".$userid.") ) as tbl";
        $this->load->library('datatables');
        $this->datatables->add_column('action', '', 'id');
        $this->datatables->from($from);
        return print_r($this->datatables->generate());
    }
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        $userid = $this->session->userdata('user_id');
		$filter_distributor = $this->session->userdata('filter_distributor');
        $statuspesan = $this->session->userdata('statuspesan');
        $tanggaldari = $this->session->userdata('tanggaldari');
        $tanggalsampai = $this->session->userdata('tanggalsampai');
		$tipe_gudang=$this->gudang_model->get_user_tipe($userid);
		
		$where='';
		
		if ($statuspesan!="#") {
			if ($statuspesan=='10'){				
				$where.=" AND H.stdraft='1' AND H.status='1'";	
			}elseif($statuspesan=='1'){
				$where.=" AND H.stdraft='0' AND H.status='1'";		
			}else{
				$where.=" AND H.status='".$statuspesan."'";
			}
        }
		// if ($no_trx!="") {					
			// $where.=" AND H.nopemesanan LIKE '%".$no_trx."%'";			
        // }
		if ($filter_distributor!="#") {					
			$where.=" AND H.iddistributor='$filter_distributor'";			
        }
		if ('' != $tanggaldari) {
            $where .= " AND DATE(H.tanggal) >='".YMDFormat($tanggaldari)."'";
        }
        if ('' != $tanggalsampai) {
            $where .= " AND DATE(H.tanggal) <='".YMDFormat($tanggalsampai)."'";
        }
		
        $from = "(SELECT H.id,H.tanggal,H.st_retur,H.st_batal_by,CASE WHEN H.tipepemesanan='1' THEN 'NON LOGISTIK' ELSE 'LOGISTIK' END as tipe_pemesanan,H.tipepemesanan,
				H.nopemesanan,H2.nopemesanan as nopemesanan_asal,H.stdraft,H.`status`,COUNT(D.id) as item,H.iddistributor,M.nama as nama_distributor,H.tipe_bayar
				FROM tgudang_pemesanan H
				LEFT JOIN tgudang_pemesanan H2 ON H2.id=H.id_asal
				LEFT JOIN tgudang_pemesanan_detail D ON H.id=D.idpemesanan AND D.status='1'
				LEFT JOIN mdistributor M ON M.id=H.iddistributor
				WHERE H.tipepemesanan IN (".$tipe_gudang.") AND H.status <> '1' AND H.stdraft='0' ".$where."
				GROUP BY H.id
				ORDER BY H.id DESC ) as tbl  ";
				
		// if ($statuspesan!="#") {
			// if ($statuspesan=='10'){				
				// $from.=" AND stdraft='1' AND status='1'";	
			// }elseif($statuspesan=='1'){
				// $from.=" AND stdraft='0' AND status='1'";		
			// }else{
				// $from.=" AND status='".$statuspesan."'";
			// }
        // }
		// if ($filter_distributor!="#") {					
			// $from.=" AND iddistributor='$filter_distributor'";			
        // }
		// if ('' != $tanggaldari) {
            // $from .= " AND DATE(tanggal) >='".YMDFormat($tanggaldari)."'";
        // }
        // if ('' != $tanggalsampai) {
            // $from .= " AND DATE(tanggal) <='".YMDFormat($tanggalsampai)."'";
        // }
		
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array('id' => 'desc');
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_setting     = '';
            $url        = site_url('tgudang_verifikasi/');
            $url2        = site_url('tgudang_pemesanan/');
			if ($r->status=='2'){
				$status_setting='<button title="Lihat User" class="btn btn-danger btn-xs user" onclick="load_user('.$r->id.')"><i class="si si-user"></i></button>';
			}
			

            $row[] = $r->id;
            $row[] = $no;
            $row[] = ($r->tipepemesanan=='1'?'<label class="label label-default">'.$r->nopemesanan.'</label>':'<label class="label label-default">'.$r->nopemesanan.'</label>').' '.($r->nopemesanan_asal?'<label class="label label-danger">'.$r->nopemesanan_asal.'</label>':'').''.($r->st_retur?'<label class="label label-warning">RETUR</label>':'');
            $row[] = $r->tanggal;
            $row[] = $r->tipe_pemesanan;
            $row[] = $r->nama_distributor.' <br>'.($r->tipe_bayar=='1'?text_success('TUNAI'):text_primary('KREDIT'));
            $row[] = $r->item;
			if ($r->stdraft=='1'){
				
				// $aksi .= '<a class="view btn btn-xs btn-info" type="button" data-target="#modalDetailPemesanan" data-toggle="modal" title="Detail"><i class="fa fa-eye"></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-info" href="'.$url.'detail/'.$r->id.'" type="button"  title="Detail"><i class="fa fa-eye"></i></a>';
				if ($r->status=='0'){
					
					$status='<span class="label label-danger">DIBATALKAN</span>';
				}else{
					$status='<span class="label label-info">DRAFT</span>';
				}
			}else{
				$aksi .= '<a class="view btn btn-xs btn-info" href="'.$url.'detail/'.$r->id.'" type="button"  title="Detail"><i class="fa fa-eye"></i></a>';
				if ($r->status=='0'){
					if ($r->st_batal_by=='3'){
						$status='<span class="label label-danger">DIBATALKAN KEUANGAN</span>';
					}else{
						$status='<span class="label label-danger">DIBATALKAN</span>';
						
					}
				}elseif($r->status=='1'){
					$status=status_pesan_gudang($r->status);
					
				}elseif($r->status=='2'){
					$status=status_pesan_gudang($r->status);
					if ($r->st_retur=='0'){
						// if (UserAccesForm($user_acces_form,array('1128'))){
						// $aksi .= '<a href="'.$url.'edit/'.$r->id.'" type="button" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>';
						// }
					}
					// if (UserAccesForm($user_acces_form,array('1126'))){
					// $aksi .= '<a href="'.$url.'setujui/'.$r->id.'" type="button" title="Setujui" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Setujui</a>';
					// }
					// if (UserAccesForm($user_acces_form,array('1127'))){
					// $aksi .= '<a href="'.$url.'tolak/'.$r->id.'" type="button" title="Tolak" class="btn btn-xs btn-danger"><i class="fa fa-times"></i> Tolak</a>';
					// }
					if (UserAccesForm($user_acces_form,array('1137'))){
					$aksi .= '<a href="'.$url2.'print_data/'.$r->id.'" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-print"></i></a>';
					}
				}elseif($r->status=='3'){//PRoses Pemesanan
					$status=status_pesan_gudang($r->status);
					if ($r->st_retur=='0'){
						if (UserAccesForm($user_acces_form,array('1132'))){
						$aksi .= '<a href="'.$url.'edit_pesanan/'.$r->id.'" type="button" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>';
						}
					}
					if (UserAccesForm($user_acces_form,array('1131'))){
					$aksi .= '<a href="'.$url.'finalisasi/'.$r->id.'" type="button" title="Proses Pemesanan" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Proses Pemesanan</a>';
					}
					if (UserAccesForm($user_acces_form,array('1137'))){
					$aksi .= '<a href="'.$url2.'print_data/'.$r->id.'" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-print"></i></a>';
					}
				}elseif($r->status=='4'){
					$status=status_pesan_gudang($r->status);
					if ($r->st_retur=='0'){
						if (UserAccesForm($user_acces_form,array('1132'))){
						$aksi .= '<a href="'.$url.'edit_pesanan/'.$r->id.'" type="button" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>';
						}
					}
					if (UserAccesForm($user_acces_form,array('1137'))){
					$aksi .= '<a href="'.$url2.'print_data/'.$r->id.'" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-print"></i></a>';
					}
				}else{
					$status=status_pesan_gudang($r->status);
					if (UserAccesForm($user_acces_form,array('1137'))){
					$aksi .= '<a href="'.$url2.'print_data/'.$r->id.'" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-print"></i></a>';
					}
				}
			}
            $row[] = $status.' '.$status_setting;
            // $row[] = $r->status;
			// $row[] = $st_kirim;
            // $row[] = $r->user_dariunit;
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function his_beli($idtipe,$idbarang) {
		
		$data= $this->model->info_barang($idtipe,$idbarang);
		// print_r($data);exit();
		$data['tanggaldari'] 			= '';
		$data['tanggalsampai'] 			= '';
		$data['error'] 			= '';
		$data['title'] 			= 'History Pembelian';
		$data['content'] 		= 'Tgudang_verifikasi/his_beli';
		$data['breadcrum'] 	= array(array("RSKB Halmahera",'#'),
							  array("tgudang_pemesanan",'#'),
							  array("List",'tgudang_pemesanan'));
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_total_his_beli() {
    	$idtipe=$this->input->post('idtipe');
		$idbarang=$this->input->post('idbarang');
		$tanggaldari=$this->input->post('tanggaldari');
		$tanggalsampai=$this->input->post('tanggalsampai');
		$where='';
		if ('' != $tanggaldari) {
            $where .= " AND DATE(H.tanggalpenerimaan) >='".YMDFormat($tanggaldari)."'";
        }
        if ('' != $tanggalsampai) {
            $where .= " AND DATE(H.tanggalpenerimaan) <='".YMDFormat($tanggalsampai)."'";
        }
		
		$q="SELECT SUM(D.nominalppn) as total_ppn,SUM(D.nominaldiskon) as total_diskon,SUM(D.totalharga) as total_harga FROM tgudang_penerimaan H
			LEFT JOIN tgudang_penerimaan_detail D ON H.id=D.idpenerimaan
			WHERE D.idtipe='3' AND D.idbarang='1' ".$where."
			GROUP BY D.idtipe,D.idbarang";
		$result=$this->db->query($q)->row_array();
		if ($result){
			
		}else{
			$result['total_ppn']=0;
			$result['total_diskon']=0;
			$result['total_harga']=0;
		}
    	$this->output->set_output(json_encode($result));
    }
	function Load_His_Beli(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        $userid = $this->session->userdata('user_id');
		$idtipe=$this->input->post('idtipe');
		$idbarang=$this->input->post('idbarang');
		$tanggaldari=$this->input->post('tanggaldari');
		$tanggalsampai=$this->input->post('tanggalsampai');
		
		// $tipe_gudang=$this->gudang_model->get_user_tipe($userid);
        $from = "(SELECT H.id,H.tanggalpenerimaan,D.kuantitas,D.namasatuan,D.harga,D.ppn,D.nominalppn,D.diskon,D.nominaldiskon,D.totalharga,D.harga_sebelumnya,H.userpenerimaan FROM tgudang_penerimaan H
			LEFT JOIN tgudang_penerimaan_detail D ON H.id=D.idpenerimaan
			WHERE D.idtipe='$idtipe' AND D.idbarang='$idbarang' ORDER BY H.id DESC) as tbl WHERE id <> ''";
				
		
		if ('' != $tanggaldari) {
            $from .= " AND DATE(tanggalpenerimaan) >='".YMDFormat($tanggaldari)."'";
        }
        if ('' != $tanggalsampai) {
            $from .= " AND DATE(tanggalpenerimaan) <='".YMDFormat($tanggalsampai)."'";
        }
		
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array('id' => 'desc');
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $status     = '';
            $url        = site_url('tgudang_verifikasi/');
            $url2        = site_url('tgudang_pemesanan/');
            $url3       = site_url('tgudang_penerimaan/');

			

            $row[] = $r->id;
            $row[] = $no;
            $row[] = HumanDateLong($r->tanggalpenerimaan);
            $row[] = $r->kuantitas.' '.$r->namasatuan;
            $row[] = number_format($r->harga,0);
            $row[] = $r->ppn;
            $row[] = number_format($r->nominalppn,0);
            $row[] = $r->diskon;
            $row[] = number_format($r->nominaldiskon,0);
            $row[] = number_format($r->totalharga,0);
            $row[] = $r->userpenerimaan;
			if ($r->harga==$r->harga_sebelumnya){
				$status='<span class="label label-info"><i class="fa fa-arrows-h"></i> Sama</span>';
			}
			if ($r->harga < $r->harga_sebelumnya){
				$status='<span class="label label-success"><i class="fa fa-arrow-down"></i> Turun</span>';
			}
			if ($r->harga > $r->harga_sebelumnya){
				$status='<span class="label label-danger"><i class="fa fa-arrow-up"></i> Naik</span>';
			}
			$row[] = $status;
			$aksi .= '<a href="'.$url3.'print_data/'.$r->id.'" target="_blank" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';
			
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function his_jual($idtipe,$idbarang) {
		
		$data= $this->model->info_barang($idtipe,$idbarang);
		// print_r($data);exit();
		$data['tanggaldari'] 			= '';
		$data['tanggalsampai'] 			= '';
		$data['error'] 			= '';
		$data['title'] 			= 'History Penjualan';
		$data['content'] 		= 'Tgudang_verifikasi/his_jual';
		$data['breadcrum'] 	= array(array("RSKB Halmahera",'#'),
							  array("tgudang_pemesanan",'#'),
							  array("List",'tgudang_pemesanan'));
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_total_his_jual() {
    	$idtipe=$this->input->post('idtipe');
		$idbarang=$this->input->post('idbarang');
		$tanggaldari=$this->input->post('tanggaldari');
		$tanggalsampai=$this->input->post('tanggalsampai');
		$where='';
		if ('' != $tanggaldari) {
            $where .= " AND DATE(H.tanggalpenerimaan) >='".YMDFormat($tanggaldari)."'";
        }
        if ('' != $tanggalsampai) {
            $where .= " AND DATE(H.tanggalpenerimaan) <='".YMDFormat($tanggalsampai)."'";
        }
		
		$q="SELECT SUM(D.nominalppn) as total_ppn,SUM(D.nominaldiskon) as total_diskon,SUM(D.totalharga) as total_harga FROM tgudang_penerimaan H
			LEFT JOIN tgudang_penerimaan_detail D ON H.id=D.idpenerimaan
			WHERE D.idtipe='3' AND D.idbarang='1' ".$where."
			GROUP BY D.idtipe,D.idbarang";
		$result=$this->db->query($q)->row_array();
		if ($result){
			
		}else{
			$result['total_ppn']=0;
			$result['total_diskon']=0;
			$result['total_harga']=0;
		}
    	$this->output->set_output(json_encode($result));
    }
	function Load_His_Jual(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        $userid = $this->session->userdata('user_id');
		$idtipe=$this->input->post('idtipe');
		$idbarang=$this->input->post('idbarang');
		$tanggaldari=$this->input->post('tanggaldari');
		$tanggalsampai=$this->input->post('tanggalsampai');
		
		// $tipe_gudang=$this->gudang_model->get_user_tipe($userid);
        $from = "(SELECT H.id,H.tanggalpenerimaan,D.kuantitas,D.namasatuan,D.harga,D.ppn,D.nominalppn,D.diskon,D.nominaldiskon,D.totalharga,D.harga_sebelumnya,H.userpenerimaan FROM tgudang_penerimaan H
			LEFT JOIN tgudang_penerimaan_detail D ON H.id=D.idpenerimaan
			WHERE D.idtipe='$idtipe' AND D.idbarang='$idbarang' ORDER BY H.id DESC) as tbl WHERE id <> ''";
				
		
		if ('' != $tanggaldari) {
            $from .= " AND DATE(tanggalpenerimaan) >='".YMDFormat($tanggaldari)."'";
        }
        if ('' != $tanggalsampai) {
            $from .= " AND DATE(tanggalpenerimaan) <='".YMDFormat($tanggalsampai)."'";
        }
		
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array('id' => 'desc');
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $status     = '';
            $url        = site_url('tgudang_verifikasi/');
            $url2        = site_url('tgudang_pemesanan/');
            $url3       = site_url('tgudang_penerimaan/');

			

            $row[] = $r->id;
            $row[] = $no;
            $row[] = HumanDateLong($r->tanggalpenerimaan);
            $row[] = $r->kuantitas.' '.$r->namasatuan;
            $row[] = number_format($r->harga,0);
            $row[] = $r->ppn;
            $row[] = number_format($r->nominalppn,0);
            $row[] = $r->diskon;
            $row[] = number_format($r->nominaldiskon,0);
            $row[] = number_format($r->totalharga,0);
            $row[] = $r->userpenerimaan;
			if ($r->harga==$r->harga_sebelumnya){
				$status='<span class="label label-info"><i class="fa fa-arrows-h"></i> Sama</span>';
			}
			if ($r->harga < $r->harga_sebelumnya){
				$status='<span class="label label-success"><i class="fa fa-arrow-down"></i> Turun</span>';
			}
			if ($r->harga > $r->harga_sebelumnya){
				$status='<span class="label label-danger"><i class="fa fa-arrow-up"></i> Naik</span>';
			}
			$row[] = $status;
			$aksi .= '<a href="'.$url3.'print_data/'.$r->id.'" target="_blank" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';
			
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }

    function acc($id) {
        if($id) $this->db->update('tgudang_pemesanan', array('status' => 2), array('id' => $id) );
    }

    function cancel($id) {
        if($id) $this->db->update('tgudang_pemesanan', array('status' => 0,'iduser_batal' => $this->session->userdata("user_id"),'tgl_batal' => date('Y-m-d H:i:s')), array('id' => $id) );
    }   
	

	public function tolak($id){
		if ($this->model->tolak($id)) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil Dibatalkan.');
            redirect('tgudang_verifikasi/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_verifikasi/index', 'refresh');
        }
		// print_r($this->input->post());exit();
	}
    function get_list_barang($id){
        if($id) {
            $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');

            $this->load->library('datatables');
            if($id == 2 || $id == 4) {
            	$this->datatables->select('id,kode,nama,IF(hargabeli IS NULL,0,hargabeli) as hargabeli,catatan');
	            $this->datatables->add_column('satuan','-');
            } else {            	
	            $this->datatables->select('a.id,a.kode,a.nama,IF(a.hargabeli IS NULL,0,a.hargabeli) as hargabeli,b.nama as satuan,catatan');
	            $this->datatables->join('msatuan b', 'a.idsatuanbesar = b.id', 'left');
            }
            $this->datatables->from($table[$id] . ' as a');
            return print_r($this->datatables->generate());
        }
    }

    function get_distributor() {
        $query = get_all('mdistributor', array('status' => 1) );
        $data = "<option value=''>Pilih Opsi</option>";
        if($query) {
            foreach ($query as $r) {
                $data .= "<option value='".$r->id."'>".$r->nama."</option>";
            }
        }
        echo $data;
    }

    function get_barang_detail() {
    	$kode 		= $this->input->post('kodebarang');
    	$tipe 		= $this->input->post('idtipe');
		$table 		= array(null, 'mdata_alkes', 'mdata_implan', 'mdata_obat', 'mdata_logistik');
		$barang 	= get_by_field('kode', $kode, $table[ $tipe ] );
		$data 		= array();

		$data['id']				= $barang->id;
		if( $barang->hargabeli == null) {
			$data['hargabeli']		= 0;
		} else {
			$data['hargabeli']		= $barang->hargabeli;
		}

		$this->output->set_output(json_encode($data));
    }

    function viewDetail() {
    	$idPemesanan = $this->input->post('idpemesanan');
    	if($idPemesanan) {
    		if($this->model->viewDetail($idPemesanan)) {
	    		$result = $this->model->viewDetail($idPemesanan);
    		} else {
	    		$result = array();
    		}
    	} else {
    		$result = array();
    	}
    	$data = array('data' => $result);
    	$this->output->set_output(json_encode($data));
    }

    function getSatuanBarang() {
    	$idtipe = $this->input->post('idtipe');
    	$idbarang = $this->input->post('idbarang');
    	if($idtipe && $idbarang) {
    		$result = $this->model->getSatuanBarang($idtipe,$idbarang);
    	} else {
    		$result = '-';
    	}
    	echo $result;
    }

    function getAnyQty() {
    	$idpemesanan = $this->input->post('idpemesanan');
    	$idtipe = $this->input->post('idtipe');
    	$idbarang = $this->input->post('idbarang');
    	if($idtipe && $idbarang && $idpemesanan) {
    		$result = $this->model->getAnyQty($idtipe,$idbarang,$idpemesanan);
    	} else {
    		$result = array();
    	}
    	$this->output->set_output(json_encode($result));
    }

    function getJmlPesanTerima() {
    	$idpemesanan = $this->input->post('idpemesanan');
    	if($idpemesanan) {
    		if($this->model->getJmlPesanTerima($idpemesanan)) {
	    		$result = $this->model->getJmlPesanTerima($idpemesanan);
    		} else {
    			$result = array();
    		}
    	} else {
    		$result = array();
    	}
    	$this->output->set_output(json_encode($result));
    }
	

    function getNamaBarang() {
    	$idtipe = $this->input->post('idtipe');
    	$idbarang = $this->input->post('idbarang');
    	if($idtipe && $idbarang) {
    		$result = $this->model->getNamaBarang($idtipe, $idbarang);
    	} else {
    		$result = '-';
    	}
    	echo $result;
    }

    function getMaxDistributor() {
        $idtipe = $this->input->post('idtipe');
        $idbarang = $this->input->post('idbarang');
        if($idtipe && $idbarang) {
            $result = $this->model->getMaxDistributor($idtipe, $idbarang);
        } else {
            $result = '';
        }
        echo $result;
    }

    function getStokGudang() {
        $idtipe = $this->input->post('idtipe');
        $idbarang = $this->input->post('idbarang');
        if($idtipe && $idbarang) {
            $result = $this->model->getStokGudang($idtipe, $idbarang);
        } else {
            $result = '0';
        }
        echo $result;
    }

    public function get_satuan_barang() {
        $this->model->get_satuan_barang();
    }
    public function get_harga_beli() {
        $opsisatuan = $this->input->post('opsisatuan');
        $idbarang = $this->input->post('idbarang');
        $idtipe = $this->input->post('idtipe');
        if($idbarang && $idtipe) {
            $row = $this->model->get_detailbarang($idtipe,$idbarang);
            if($opsisatuan == 2) {
                $harga = $row->hargasatuanbesar;
            } else {
                $harga = $row->hargabeli;
            }
            $this->output->set_output(json_encode($harga));
        }
    }

    public function print_po() {
        $id = $this->uri->segment(3);
        if($id) {
            $data                   = $this->model->print_po_head($id);
            $data['detail']         = $this->model->print_po_detail($data['id']);
            $data['error']          = '';
            $data['title']          = 'Print Pemesanan';
            $data = array_merge($data, backend_info());
            $this->parser->parse('Tgudang_verifikasi/print_po', $data);        
        }
    }

    public function konfirmasi_pemesanan_draft($id) {
        // $id = $this->uri->segment(3);
        $data = $this->db->where('id',$id)->get('tgudang_pemesanan')->row_array();
		
        $data['namadistributor'] = get_by_field('id',$data['iddistributor'],'mdistributor')->nama;
        $data['error']          = '';
        $data['list_detail']    = $this->model->list_pesanan_draft($id);
        $data['title']          = 'Konfirmasi Pemesanan Draft';
        $data['content']        = 'Tgudang_verifikasi/konfirmasi_pemesanan_draft';
        $data['breadcrum']      = array(array("RSKB Halmahera",'#'),
                                    array("Pemesanan Gudang",'#'),
                                    array("Konfirmasi Pemesanan Draft",''));
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);        
    }
	public function edit($id,$approval_id='') {
        $data = $this->db->where('id',$id)->get('tgudang_pemesanan')->row_array();
		// print_r($data);exit();
        $data['namadistributor'] = get_by_field('id',$data['iddistributor'],'mdistributor')->nama;
        $data['error']          = '';
        $data['approval_id']          = $approval_id;
        $data['list_detail']    = $this->model->list_pesanan_edit($id);
        $data['title']          = 'Edit Pemesanan';
        $data['content']        = 'Tgudang_verifikasi/edit';
        $data['breadcrum']      = array(array("RSKB Halmahera",'#'),
                                    array("Pemesanan Gudang",'#'),
                                    array("Edit Pemesanan",''));
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);        
    }
	function find_rekap()
	{
		$idbarang=$this->input->post('idbarang');
		$idtipe=$this->input->post('idtipe');
		$row = $this->model->find_beli($idtipe,$idbarang);
		if ($row){
			$arr=$row;
		}else{
			$arr['kuantitas']='';
			$arr['total']='';
			$arr['satuan_kecil']='';
			$arr['satuan_besar']='';
			$arr['jmlsatuanbesar']='';
		}
		$row2 = $this->model->find_jual($idtipe,$idbarang);
		if ($row2){
			$arr['kuantitas_jual']=$row2->kuantitas_jual;
			$arr['total_jual']=$row2->total_jual;
			$arr['satuan_kecil_jual']=$row2->satuan_kecil_jual;
			$arr['satuan_besar_jual']=$row2->satuan_besar_jual;
			$arr['jmlsatuanbesar_jual']=$row2->jmlsatuanbesar_jual;
		}else{
			$arr['kuantitas_jual']='';
			$arr['total_jual']='';
			$arr['satuan_kecil_jual']='';
			$arr['satuan_besar_jual']='';
			$arr['jmlsatuanbesar_jual']='';
			
		}
		$this->output->set_output(json_encode($arr));
	}

	public function finalisasi($id) {
        $data = $this->gudang_model->data_header($id);
		// print_r($data);exit();
        $data['namadistributor'] = get_by_field('id',$data['iddistributor'],'mdistributor')->nama;
        $data['error']          = '';
        $data['list_detail']    = $this->model->list_pesanan_edit($id);
		// print_r($data['list_detail']);exit();
        $data['title']          = 'Proses Pemesanan';
        $data['content']        = 'Tgudang_verifikasi/finalisasi';
        $data['breadcrum']      = array(array("RSKB Halmahera",'#'),
                                    array("Pemesanan Gudang",'#'),
                                    array("Proses Pemesanan",''));
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);        
    }
	public function edit_pesanan($id) {
        // $id = $this->uri->segment(3);
        $data = $this->gudang_model->data_header($id);
		// print_r($data);exit();
        $data['namadistributor'] = get_by_field('id',$data['iddistributor'],'mdistributor')->nama;
        $data['error']          = '';
        $data['list_detail']    = $this->model->list_pesanan_edit($id);
        $data['list_distributor'] 			= $this->model->list_distributor();
		// print_r($data['list_detail']);exit();
        $data['title']          = 'Edit Pemesanan & Edit Distributor';
        $data['content']        = 'Tgudang_verifikasi/edit_pesanan';
        $data['breadcrum']      = array(array("RSKB Halmahera",'#'),
                                    array("Pemesanan Gudang",'#'),
                                    array("Edit Pemesanan",''));
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);        
    }
	public function detail($id) {
        $data = $this->gudang_model->data_header($id);
		// print_r( $data);exit();
        $data['namadistributor'] = get_by_field('id',$data['iddistributor'],'mdistributor')->nama;
        $data['error']          = '';
        $data['list_detail']    = $this->model->list_pesanan_edit($id);
        $data['title']          = 'Detail Pemesanan';
        $data['content']        = 'Tgudang_verifikasi/detail';
        $data['breadcrum']      = array(array("RSKB Halmahera",'#'),
                                    array("Pemesanan Gudang",'#'),
                                    array("Detail Pemesanan",''));
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);        
    }

    public function save_konfirmasi_pemesanan_draft2() {
        
		if($this->model->save_konfirmasi_pemesanan_draft2()) {
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
			redirect('tgudang_verifikasi/index','refresh');
		}else{
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
			redirect('tgudang_verifikasi/index','refresh');
		}
       
    }
	public function save_konfirmasi_pemesanan_draft() {
        $this->form_validation->set_rules('detailvalue', 'DataTable', 'trim|required|min_length[10]');
        if($this->form_validation->run() == true) {
            if($this->model->save_konfirmasi_pemesanan_draft()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
                redirect('tgudang_verifikasi/index','refresh');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_verifikasi/index','refresh');
        }
    }

    public function detailbarangdraft() {
        $this->model->detailbarangdraft();
    }

    public function testing() {
        $var1 = 25/100;

        $qty = 0;
        if($var1 < 1) {
            $qty = 1;
        } else {
            $qty = ceil($var1);
        }        
    }

    public function getdistributor() {
        $this->model->getdistributor();
    }
	public function view_unit() {
        $this->model->view_unit();
    }
    public function select_tipe() {
        $data = array();
        $this->db->select('gudangtipe');
        $this->db->where('iduser', $this->session->userdata('user_id'));
        $get = $this->db->get('musers_tipegudang');
        $row = $get->row_array();
        if($get->num_rows() == 1) {
            if($row['gudangtipe'] == 1) {
                $data[1] = 'Alkes';
                $data[2] = 'Implant';
                $data[3] = 'Obat';
            } else {
                $data[4] = 'Logistik';
            }
        } else {
            $data[1] = 'Alkes';
            $data[2] = 'Implant';
            $data[3] = 'Obat';            
            $data[4] = 'Logistik';            
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

}	

/* End of file tgudang_pemesanan.php */
/* Location: ./application/controllers/tgudang_pemesanan.php */
/* End of file Tgudang_pemesanan.php */
/* Location: ./application/controllers/Tgudang_pemesanan.php */
