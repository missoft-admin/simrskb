<?php defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';
use Dompdf\Dompdf;

class Tkontrabon extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tkontrabon_model','model'); 
		$this->load->model('Mrka_bendahara_model','Mrka_bendahara_model'); 
	}
	public function save_pembayaran()
    {
		$id=$this->model->saveData();
		if($id){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tkontrabon/kontrabon_kas/'.$this->input->post('tkontrabon_id').'/1','location');
		}
	}
	function index($tanggal='') {
		// $btn_post=
		if ($tanggal){
			$btn_post 	= $this->input->post('btn_post');
			$tanggalkontrabon 	= $tanggal;
		}else{
		$btn_post 	= $this->input->post('btn_post');
			$tanggalkontrabon 	= $this->input->post('tanggalkontrabon');
			
			if ($btn_post=='2'){
				$this->model->insert_tkbo(YMDFormat($tanggalkontrabon));			
			}
			if ($btn_post=='3'){
		// print_r($btn_post);exit();
				$this->model->delete_tkbo(YMDFormat($tanggalkontrabon));			
			}
		}
		$stkontrabon 		= $this->input->post('stkontrabon');
		
		$idkbo=0;
		$st_kbo=0;
		$total_bayar=0;
		$jml_list=0;$jml_minus=0;
		
		$jml_cash=0;$jml_tf=0;$jml_cheq=0;$jml_tunai_tf=0;
		
		$tot_cash=0;$tot_tf=0;$tot_cheq=0;$tot_tunai_tf=0;
		
		$jml_dist=0;$jml_vendor=0;
		
		$nominal_alkes=0;$nominal_implan=0;$nominal_obat=0;$nominal_logistik=0;$nominal_retur_uang=0;$nominal_pengajuan=0;
		$nominal_barang=0;
		$jml_detail=0;$jml_trx_kas=0;
		if($tanggalkontrabon) {
			$tanggalkontrabon 	= $tanggalkontrabon;
			$r=$this->model->get_st_kb(YMDFormat($tanggalkontrabon));
			if ($r){
				$idkbo=$r->id;
				$st_kbo=$r->status;
				$jml_detail=$r->jml_detail;
				$jml_trx_kas=$r->jml_trx_kas;
				$jml_cash=$r->jml_tunai+$r->jml_tunai_tf;
				$jml_tf=$r->jml_tf;
				$jml_cheq=$r->jml_cheq;
				$tot_cash=$r->nominal_tunai+$r->nominal_tunai_tf;
				$tot_cheq=$r->nominal_cheq;
				$tot_tf=$r->nominal_tf;
				$jml_dist=$r->jml_dist;
				$nominal_alkes=$r->nominal_alkes;
				$nominal_implan=$r->nominal_implan;
				$nominal_obat=$r->nominal_obat;
				$nominal_logistik=$r->nominal_logistik;
				$nominal_retur_uang=$r->retur_uang;
				$nominal_pengajuan=$r->nominal_pengajuan;
				$nominal_barang=$nominal_alkes+$nominal_implan+$nominal_obat+$nominal_logistik-$nominal_retur_uang+$nominal_pengajuan;
				$total_bayar=$r->totalnominal;
				$jml_list=$this->model->get_jumlah_belum_verifikasi(YMDFormat($tanggalkontrabon));
				$jml_minus=$this->model->get_jml_minus($idkbo);
				$data['list_user_aktifasi'] 			= $this->model->list_user_aktifasi($idkbo);
			}else{
				// print_r('sini');exit();
				$st_kbo='0';
				$jml_list=$this->model->get_jumlah_belum_verifikasi(YMDFormat($tanggalkontrabon));
			}
		} else {
			$tanggalkontrabon 	= '';
			$st_kbo='0';
			
			$jml_list=$this->model->get_jumlah_belum_verifikasi(YMDFormat($tanggalkontrabon));
		}
		// print_r($jml_list);exit();
		$data['jml_trx_kas'] 			= $jml_trx_kas;
		$data['jml_detail'] 			= $jml_detail;
		$data['sisa_belum_trx_kas'] 	= $jml_detail - $jml_trx_kas;
		$data['jml_minus'] 			= $jml_minus;
		$data['jml_list'] 			= $jml_list;
		$data['idkbo'] 			= $idkbo;
		$data['st_kbo'] 			= $st_kbo;
		$data['error'] 				= '';
		$data['tanggalkontrabon'] 	= $tanggalkontrabon;
		$data['status_kbo'] 		= '1';
		$data['tot_cash'] 		= $tot_cash;
		$data['tot_cheq'] 		= $tot_cheq;
		$data['tot_tf'] 		= $tot_tf;
		$data['count_cheq'] 		= $jml_cheq;
		$data['count_cash'] 		= $jml_cash;
		$data['count_tf'] 			= $jml_tf;
		$data['total_pembayaran'] 	= $total_bayar;
		$data['total_vendor'] 		= $jml_vendor;
		$data['total_distributor'] 	= $jml_dist;
		$data['total_detail_barang_alkes'] 		= $nominal_alkes;
		$data['total_detail_barang_implant'] 	= $nominal_implan;
		$data['total_detail_barang_obat'] 		= $nominal_obat;
		$data['total_detail_barang_logistik'] 	= $nominal_logistik;
		$data['nominal_pengajuan'] 	= $nominal_pengajuan;
		$data['total_retur_uang_kbo'] 	= $nominal_retur_uang;
		$data['nominal_barang'] 	= $nominal_barang;
		
		$data['list_distributor'] 		= $this->model->list_distributor();
		$data['list_user'] 		= $this->model->get_user_aktifasi();
		$data['user_aktifasi'] 		= '';
		$data['userkonfirmasi'] 		= '';
		// print_r($data);exit();
		$data['title'] 				= 'Kontrabon';
		$data['content'] 			= 'Tkontrabon/index';
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'), 
										array("Kontrabon",'tkontrabon/index'), 
										array("List",'#') 
									);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function data_verifikasi() {
		
		
		// $data['list_kontrabon'] 	= $this->model->list_kontrabon();
		$data['title'] 				= 'LIST KONTRABON VERIFIKASI';
		$data['error'] 				= '';
		$data['content'] 			= 'Tkontrabon/index_verifikasi';
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'), 
										array("Kontrabon",'tkontrabon/index'), 
										array("List",'#') 
									);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function print_per_distributor($id,$tipe='1'){
		$dompdf = new Dompdf();

        $data = $this->model->get_header_kbo_detail($id);
		if ($tipe=='1'){
				$data['ket']='KONTRABON';		
		}else{
				$data['ket']='PELUNASAN';		
			
		}
		// print_r($data);exit();
        $data['list_detail'] = $this->model->get_detail_kbo($id);
		$data=array_merge($data,backend_info());
        $html = $this->load->view('Tkontrabon/print_distributor', $data, true);
        $html = $this->parser->parse_string($html,$data);
		// print_r($html);exit();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Rincian Kontrabon.pdf', array("Attachment"=>0));
	}
	function print_rekap($id,$versi){
		$dompdf = new Dompdf();

        // $data = $this->model->get_header_kbo_detail($id);
		$data=$this->model->header_rekap_report($id);;
        $data['list_detail'] = $this->model->list_rekap_report($id);
		$data=array_merge($data,backend_info());
        $html = $this->load->view('Tkontrabon/print_rekap_1', $data, true);
        $html = $this->parser->parse_string($html,$data);
		// print_r($html);exit();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Rekap Kontrabon.pdf', array("Attachment"=>0));
		
	}
	function print_rekap_detail($id,$versi){
		$dompdf = new Dompdf();

        // $data = $this->model->get_header_kbo_detail($id);
		$data=$this->model->header_rekap_report($id);;
        $data['list_detail'] = $this->model->list_rekap_detail_report($id);
		$data=array_merge($data,backend_info());
        $html = $this->load->view('Tkontrabon/print_rekap_2', $data, true);
        $html = $this->parser->parse_string($html,$data);
		// print_r($html);exit();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Rekap Kontrabon Detail.pdf', array("Attachment"=>0));
		
	}
	function detail_kontrabon($tkontrabon_id,$readonly='0') {
				
		$data=$this->model->get_header_kbo_detail($tkontrabon_id);
		// print_r($data);exit();
		if ($data){
		
			$row_master=$this->model->get_mkontrabon();
			if ($row_master){
					$totalnominal=$data['totalnominal'];
					$data['b_tf']=$row_master->biaya_tf;
					$data['b_cheq']=$row_master->biayaceq;
					$data['b_materai']=$row_master->biayamaterai;
					$data['est_tunai']=$totalnominal;
					$data['est_cheq']=$totalnominal-$data['b_cheq']-$data['b_materai'];
					$data['est_tf']=$totalnominal-$data['b_tf'];
			}
			// print_r($data);exit();
			// $data['list_distributor'] 		= $this->model->list_distributor();
			$data['list_bank'] 		= $this->model->list_bank();
			$data['list_bank_dist'] 		= $this->model->list_bank_dist($data['iddistributor']);
			$data['error'] 				= '';
			$data['lock'] 				= '';
			// $data['totalnominal'] 				= '0';
			if ($readonly=='0'){
				$data['disabel'] 				= 'disabled';			
			}else{
				if ($readonly=='1'){
					$data['disabel'] 				= '';					
				}else{
					$data['disabel'] 				= 'disabled';	
				}
				$data['lock'] 				= $readonly;
				
			}
			$data['rekening_id_asal'] 	= $data['rekening_id'];
			$data['no_po'] 				= '';
			$data['st_verifikasi'] 				= '1';
			$data['no_terima'] 				= '';
			$data['no_fakur'] 				= '';
			$data['tgl_trx'] 				= '';
			$data['tgl_trx2'] 				= '';
			$data['tipe'] 				= '#';
			// $data['carabayar'] 				= '0';
			$data['user_aktifasi'] 		= '';
			$data['userkonfirmasi'] 		= '';
			$data['title'] 				= 'Detail Kontrabon';
			$data['content'] 			= 'Tkontrabon/detail_kontrabon';//nama_dist
		}else{
			$data['content'] 			= 'Tkontrabon/page404';			
		}
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'), 
										array("Kontrabon",'tkontrabon/index'), 
										array("List",'#') 
									);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function kontrabon_kas($tkontrabon_id,$readonly='0') {
				
		$data=$this->model->get_header_kbo_detail($tkontrabon_id);
		// print_r($data);exit();
		if ($data){
		
			$row_master=$this->model->get_mkontrabon();
			if ($row_master){
					$totalnominal=$data['totalnominal'];
					$data['b_tf']=$row_master->biaya_tf;
					$data['b_cheq']=$row_master->biayaceq;
					$data['b_materai']=$row_master->biayamaterai;
					$data['est_tunai']=$totalnominal;
					$data['est_cheq']=$totalnominal-$data['b_cheq']-$data['b_materai'];
					$data['est_tf']=$totalnominal-$data['b_tf'];
			}
			// print_r($data);exit();
			// $data['list_distributor'] 		= $this->model->list_distributor();
			$data['list_pembayaran'] 		= $this->model->list_pembayaran($tkontrabon_id);
			$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
			$data['list_bank'] 		= $this->model->list_bank();
			$data['list_bank_dist'] 		= $this->model->list_bank_dist($data['iddistributor']);
			$data['error'] 				= '';
			// $data['totalnominal'] 				= '0';
			if ($readonly=='0'){
				$data['disabel'] 				= 'disabled';			
			}else{
				$data['disabel'] 				= '';
			}
			$data['no_po'] 				= '';
			$data['st_verifikasi'] 				= '1';
			$data['no_terima'] 				= '';
			$data['no_fakur'] 				= '';
			$data['tgl_trx'] 				= '';
			$data['tgl_trx2'] 				= '';
			$data['tipe'] 				= '#';
			// $data['carabayar'] 				= '0';
			$data['user_aktifasi'] 		= '';
			$data['userkonfirmasi'] 		= '';
			$data['title'] 				= 'Detail Kontrabon';
			$data['content'] 			= 'Tkontrabon/pembayaran';
		}else{
			$data['content'] 			= 'Tkontrabon/page404';			
		}
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'), 
										array("Kontrabon",'tkontrabon/index'), 
										array("List",'#') 
									);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function detail_kontrabon_nv($iddistributor,$tanggal,$readonly='0') {
				
		$data=$this->model->get_header_kbo_detail_nv($iddistributor,$tanggal);
		// print_r($data);exit();
		// if ($data){
		
			$row_master=$this->model->get_mkontrabon();
			if ($row_master){
					$totalnominal=$data['totalnominal'];
					$data['b_tf']=$row_master->biaya_tf;
					$data['b_cheq']=$row_master->biayaceq;
					$data['b_materai']=$row_master->biayamaterai;
					$data['est_tunai']=$totalnominal;
					$data['est_cheq']=$totalnominal-$data['b_cheq']-$data['b_materai'];
					$data['est_tf']=$totalnominal-$data['b_tf'];
			}
			// print_r($data);exit();
			// $data['list_distributor'] 		= $this->model->list_distributor();
			$data['list_bank'] 		= $this->model->list_bank();
			$data['list_bank_dist'] 		= $this->model->list_bank_dist($iddistributor);
			$data['error'] 				= '';
			// $data['totalnominal'] 				= '0';
			if ($readonly=='0'){
				$data['disabel'] 				= 'disabled';			
			}else{
				$data['disabel'] 				= '';
			}
			// $data['iddistributor'] 				= $iddistributor;
			// $data['tanggal'] 				= '';
			$data['lock'] 				= '';
			$data['st_verifikasi'] 				= '0';
			$data['no_po'] 				= '';
			$data['no_terima'] 				= '';
			$data['no_fakur'] 				= '';
			$data['tgl_trx'] 				= '';
			$data['tgl_trx2'] 				= '';
			$data['st_kbo'] 				= '0';
			$data['tipe'] 				= '#';
			// $data['carabayar'] 				= '0';
			$data['user_aktifasi'] 		= '';
			$data['userkonfirmasi'] 		= '';
			$data['title'] 				= 'Detail Kontrabon';
			$data['content'] 			= 'Tkontrabon/detail_kontrabon';
		// }else{
			// $data['content'] 			= 'Tkontrabon/page404';			
		// }
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'), 
										array("Kontrabon",'tkontrabon/index'), 
										array("List",'#') 
									);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function loadKbo()
    {
		$tanggalkontrabon     = YMDFormat($this->input->post('tanggalkontrabon'));
		$iddistributor     		= $this->input->post('iddistributor');
		$nokontrabon     	= $this->input->post('nokontrabon');
		$cara_bayar    = $this->input->post('cara_bayar');
		$status    = $this->input->post('status');
		$iduser=$this->session->userdata('user_id');
				
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		$where='';
        if ($iddistributor !='#'){
			$where .=" AND H.iddistributor='$iddistributor'";
		}
		$from="(SELECT T1.id,T1.nokontrabon,T1.tanggaljatuhtempo,T1.iddistributor,T1.nama_distributor,T1.jml_faktur,T1.tot_tagihan
				,CASE WHEN T1.tot_tagihan > S.minnominalceq THEN T1.tot_tagihan ELSE 0 END as tot_cheq
				,CASE WHEN T1.tot_tagihan < S.minnominalceq THEN T1.tot_tagihan ELSE 0 END as tot_tunai
				,0 as tot_tf,99 as status,T1.group_id,S.biayaceq,S.biayamaterai,S.biaya_tf,'0' as  stpencairan
				FROM (
					SELECT id,nokontrabon,tanggaljatuhtempo,iddistributor,nama_distributor,jml_faktur,SUM(tot_tagihan) as tot_tagihan,group_id FROM (SELECT H.id,'' as nokontrabon,H.tanggaljatuhtempo,H.iddistributor,MD.nama as nama_distributor, COUNT(H.id) as jml_faktur,SUM(H.totalharga-IFNULL(H.nominal_asal,0)) as tot_tagihan,GROUP_CONCAT(H.id) as group_id
							from tgudang_penerimaan as H
							LEFT JOIN mdistributor MD ON MD.id=H.iddistributor
							WHERE DATE(H.tanggaljatuhtempo)='$tanggalkontrabon' AND H.stverif_kbo IN (1,2) AND H.`status` = '1' AND H.tipe_kembali!='1' ".$where."
							GROUP BY H.tanggaljatuhtempo,H.iddistributor
							
							UNION ALL
							
							SELECT H.id,'' as nokontrabon,H.tanggalkontrabon as tanggaljatuhtempo,H.iddistributor,M.nama as nama_distributor,COUNT(H.id) as jml_faktur,SUM(H.totalharga*-1) as tot_tagihan,GROUP_CONCAT(H.id) as group_id
							from tgudang_pengembalian H
							LEFT JOIN tgudang_penerimaan P ON P.id=H.idpenerimaan					
							LEFT JOIN mdistributor M ON M.id=H.iddistributor
							WHERE H.jenis='1' AND H.jenis_retur='1' AND H.`status`='2' AND H.stverif_kbo='1' AND H.tanggalkontrabon='$tanggalkontrabon' ".$where."
							GROUP BY H.tanggalkontrabon,H.iddistributor) TT GROUP BY TT.tanggaljatuhtempo,TT.iddistributor
				
				UNION ALL
				
					SELECT  H.id,'' as nokontrabon,H.tanggal_kontrabon as  tanggaljatuhtempo,H.idvendor as iddistributor,M.nama as namadistributor,COUNT(H.id) jml_faktur
					,SUM(H.grand_total) as tot_tagihan,GROUP_CONCAT(H.id) as group_id
						from rka_pengajuan H
						LEFT JOIN mvendor M ON M.id=H.idvendor
						WHERE DATE(H.tanggal_kontrabon)='$tanggalkontrabon' AND H.stverif_kbo IN (1,2) 
						GROUP BY H.tanggal_kontrabon,H.idvendor
					
				) T1,mkontrabon S
				) as tbl";
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();
		$url        = site_url('tkontrabon/');
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
			if ($r->nokontrabon){
				$row[] = $r->nokontrabon;				
			}else{
				$row[] = '<span class="label label-danger">Belum Verifikasi</span>';
				
			}
			// $total_cheq=0;
			if ($r->tot_cheq > 0){
				$total_cheq=$r->tot_cheq - $r->biayamaterai - $r->biayaceq;			
			}else{
				$total_cheq=$r->tot_cheq;				
			}
			// ( > 0?$:$r->tot_cheq);
			$total_tf=($r->tot_tf>0?$r->tot_tf - $r->biaya_tf:$r->tot_tf);
			$total_tunai=$r->tot_tunai;
			// $total_tagihan=$total_cheq + $total_tf + $total_tunai;
			$total_tagihan=$r->tot_tf + $r->tot_cheq + $r->tot_tunai;
            $row[] = $r->nama_distributor;
            $row[] = $r->jml_faktur;
            $row[] =  '';
            $row[] = number_format($total_tagihan,2);
            $row[] = number_format($total_cheq,2);
            $row[] = number_format($total_tunai,2);
            $row[] = number_format($total_tf,2);
            $row[] =  '';
            $row[] =  '';
			$aksi       = '<div class="btn-group">';
			$aksi .= '<a href="'.$url.'detail_kontrabon_nv/'.$r->iddistributor.'/'.$r->tanggaljatuhtempo.'" type="button" title="View" class="btn btn-xs btn-default" target="_blank"><i class="fa fa-eye"></i></a>';
			$aksi .= '<button title="Print" class="btn btn-xs btn-success"><i class="fa fa-print"></i></button>';
			$aksi.='</div>';
			$row[] = $aksi;
			
			$row[] = $r->id;//14
			$row[] = $r->iddistributor;//15
			$row[] = $r->stpencairan;//16
			
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	function LoadBank()
    {
		$iddistributor     		= $this->input->post('iddistributor');
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT H.id,H.tipe_distributor,H.iddistributor,
			M.kodebank,M.bank,H.cabang,H.atasnama,H.norekening,H.staktif,H.bankid
			FROM mdistributor_bank H
			LEFT JOIN ref_bank M ON M.id=H.bankid
			WHERE H.iddistributor='$iddistributor' ORDER BY staktif DESC
			) as tbl";
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('atasnama','norekening','cabang','bank');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $r->id;
            $row[] = $r->bankid;
            // $row[] = $no;
            // $row[] = $r->kodebank;
            $row[] = $r->bank;
            $row[] = $r->cabang;
            $row[] = $r->atasnama;
            $row[] = $r->norekening;
			if ($r->staktif){
				$row[] = '<span class="label label-success">Active</span>';			
			}else{
				$row[] = '<span class="label label-danger">Not Active</span>';
				
			}
			$aksi = '<div class="btn-group">';
			if ($r->staktif=='0'){
				$aksi .= '<button data-toggle="tooltip" title="Pilih" class="btn btn-success btn-xs" onclick="aktivasi_bank('.$r->id.','.$r->tipe_distributor.','.$r->iddistributor.')"><i class="fa fa-check"></i> Actived</button>';
				
			}
			$aksi .= '<button data-toggle="tooltip" title="Edit" class="btn btn-info btn-xs edit_rekening"><i class="fa fa-pencil"></i></button>';
			// $aksi .= '<a href="#btabs-animated-slideleft-add"><i class="fa fa-pencil"></i> Add / Edit Rekening</a>';
			$aksi .= '</div>';
            $row[] = $aksi;
            
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	function loadKbo_approve()
    {
		$iduser=$this->session->userdata('user_id');
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT H.tanggal_kbo,COUNT(D.id) as jml_dist,SUM(D.nominal_tf) as nominal_tf,SUM(D.nominal_cheq) as nominal_cheq,SUM(nominal_tunai) as nominal_tunai FROM tkontrabon_info H
				INNER JOIN tkontrabon_user U ON H.id=U.kontrabon_info_id
				LEFT JOIN tkontrabon D ON D.kontrabon_info_id=H.id
				WHERE H.`status`='3' AND U.user_id='$iduser' AND U.approve = 0
				GROUP BY H.id
			) as tbl";
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			$aksi='';
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggal_kbo);
            $row[] = $r->jml_dist;
            $row[] = number_format($r->nominal_cheq,2);
            $row[] = number_format($r->nominal_tunai,2);
            $row[] = number_format($r->nominal_tf,2);
            $row[] = number_format($r->nominal_tf + $r->nominal_tunai + $r->nominal_cheq ,2);
			$aksi .= '<button title="Lihat Detail Kontrabon" class="btn btn-xs btn-success approve_kontrabon"><i class="fa fa-eye"></i> Lihat Kontrabon</button>';
            $row[] = $aksi;
            
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

	function loadKbo_verifikasi()
    {
		$tipe_distributor     = $this->input->post('tipe_distributor');
		if ($tipe_distributor=='#'){
			$tipe_distributor='1';
		}
		$st_kbo     = ($this->input->post('st_kbo'));
		$tanggalkontrabon     = YMDFormat($this->input->post('tanggalkontrabon'));
		$idkbo     		= $this->input->post('idkbo');
		$iddistributor     		= $this->input->post('iddistributor');
		$nokontrabon     	= $this->input->post('nokontrabon');
		$cara_bayar    = $this->input->post('cara_bayar');
		$status    = $this->input->post('status');
		$iduser=$this->session->userdata('user_id');
				
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		$where='';
		$where2='';
        if ($iddistributor !='#'){
			$where .=" AND H.iddistributor='$iddistributor' AND H.tipe_distributor='$tipe_distributor'";
		}
		if ($nokontrabon !=''){
			$where .=" AND H.nokontrabon='$nokontrabon'";
		}
		if ($cara_bayar !='#'){
			$where .=" AND H.st_cara_bayar='$cara_bayar'";
		}
		if ($status !='#'){
			$where2 .=" AND stpencairan='$status'";
		}
		$from="(SELECT H.id,H.nokontrabon,H.tanggalkontrabon as tanggaljatuhtempo,H.iddistributor
		,CASE WHEN H.tipe_distributor='3' THEN MV.nama ELSE M.nama END as nama_distributor
		,COUNT(D.id) as jml_faktur
				,SUM(D.nominal_bayar) as tot_tagihan,(H.nominal_cheq-H.biayacek-H.biayamaterai) as tot_cheq,H.nominal_tunai as tot_tunai,(H.nominal_tf-H.biaya_tf) as tot_tf,H.stpencairan
				,CASE WHEN H.nominal_cheq > 0 THEN 1  WHEN H.nominal_tunai > 0 THEN 2  WHEN H.nominal_tf > 0 THEN 3 ELSE 4 END as cara_bayar,H.st_cara_bayar,H.last_update,
				B.jml_bayar,B.nominal_bayar as total_bayar
				,H.st_trx_kas,(H.biayacek+H.biayamaterai+H.biaya_tf) as biaya
				FROM tkontrabon_info I
				INNER JOIN tkontrabon H ON I.id=H.kontrabon_info_id
				INNER JOIN tkontrabon_detail D ON H.id=D.idkontrabon
				LEFT JOIN mdistributor M ON M.id=H.iddistributor AND H.tipe_distributor='1'
				LEFT JOIN mvendor MV ON MV.id=H.iddistributor AND H.tipe_distributor='3'
				LEFT JOIN (
					SELECT B.idkontrabon,SUM(B.nominal_bayar) as nominal_bayar, COUNT(B.id) as jml_bayar from tkontrabon_pembayaran B 
					WHERE B.`status`='1'
					GROUP BY B.idkontrabon
				) B ON B.idkontrabon=H.id
				WHERE H.kontrabon_info_id ='$idkbo' ".$where."
				GROUP BY H.id
			) as tbl WHERE id IS NOT NULL ".$where2;
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();
		$url        = site_url('tkontrabon/');
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
			$total_harus_bayar=$r->tot_tagihan - ($r->biaya);
			$btn_disabled='';
			$status_siap_serahkan=0;
			if ($total_harus_bayar==$r->total_bayar){
				$status_siap_serahkan=1;
			}
            $no++;
            $row = array();

            $row[] = $no;
			if ($r->nokontrabon){
				$row[] = $r->nokontrabon;				
			}else{
				$row[] = '<span class="label label-danger">Belum Verifikasi</span>';
				
			}
            $row[] = $r->nama_distributor;
            $row[] = $r->jml_faktur;
			if ($r->last_update){
				$row[] =  HumanDateLong($r->last_update);
			}else{
				$row[] =  '';
			}
			if ($r->tot_tagihan < 0){
				$row[] ='<span class="label label-danger">'. number_format($r->tot_tagihan).'</span>';
				$row[] ='<span class="label label-danger">'. number_format($r->tot_cheq).'</span>';
				$row[] ='<span class="label label-danger">'. number_format($r->tot_tunai).'</span>';
				$row[] ='<span class="label label-danger">'. number_format($r->tot_tf).'</span>';
				$btn_disabled="disabled";
			}else{
				$row[] = number_format($r->tot_tagihan,2);
				$row[] = number_format($r->tot_cheq,2);
				$row[] = number_format($r->tot_tunai,2);
				$row[] = number_format($r->tot_tf,2);
			}
            
            $row[] =  cara_bayar($r->st_cara_bayar);
            $row[] =  status_bayar($r->stpencairan).' <br><br>'.($status_siap_serahkan=='1'?'<span class="label label-success"><i class="fa fa-check"></i> Transaksi Kas</span>':'<span class="label label-danger"><i class="fa fa-close"></i> Transaksi Kas</span>');
			
			$aksi       = '<div class="btn-group">';
				$aksi .= '<a href="'.$url.'detail_kontrabon/'.$r->id.'" type="button" title="View" class="btn btn-xs btn-default" target="_blank"><i class="fa fa-eye"></i></a>';
				if ($st_kbo <3){
				$aksi .= '<a href="'.$url.'detail_kontrabon/'.$r->id.'/1" type="button" title="Detail" class="btn btn-xs btn-primary" target="_blank"><i class="fa fa-navicon"></i></a>';
				$aksi .= '<button title="Edit" class="btn btn-xs btn-info edit_tanggal"><i class="fa fa-pencil"></i></button>';
				}
				if ($st_kbo >=2){
					// $aksi .= '<button title="Pembayaran " class="btn btn-xs btn-primary add_pembayaran_kas"><i class="fa fa-credit-card"></i></button>';	
					if ($r->stpencairan=='0'){	
						if ($status_siap_serahkan=='1'){
						$aksi .= '<a href="'.$url.'kontrabon_kas/'.$r->id.'/1" type="button" '.$btn_disabled.'title="Pembayaran" class="btn btn-xs btn-default" target="_blank"><i class="fa fa-credit-card"></i></a>';
							
						}else{
						$aksi .= '<a href="'.$url.'kontrabon_kas/'.$r->id.'/1" type="button" '.$btn_disabled.'title="Pembayaran" class="btn btn-xs btn-primary" target="_blank"><i class="fa fa-credit-card"></i></a>';
							
						}
					}
				}
				$aksi .= '<div class="btn-group">
                      <div class="btn-group dropright">
                        <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown">
                          <span class="fa fa-print"></span>
                        </button>
                      <ul class="dropdown-menu">
                        <li>
                          <a tabindex="-1" target="_blank" href="'.$url.'print_per_distributor/'.$r->id.'/1">Kontrabon</a>
                          <a tabindex="-1" target="_blank" href="'.$url.'print_per_distributor/'.$r->id.'/2">Pelunasan</a>
                        </li>
                      </ul>
                      </div>
                    </div>';
				// $aksi .= '<a href="'.$url.'print_per_distributor/'.$r->id.'/1" title="Print Kontrabon" target="_blank" type="button"  class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';
				// $aksi .= '<a href="'.$url.'print_per_distributor/'.$r->id.'/2" title="Print Pelunasan" target="_blank" type="button"  class="btn btn-xs btn-primary"><i class="fa fa-print"></i></a>';
				if ($st_kbo =='4'){
					if ($r->stpencairan=='0'){
						if ($status_siap_serahkan=='1'){
							$aksi .= '<button title="Serahkan" '.($st_kbo!='4'? "disabled": "").' class="btn btn-xs btn-warning serahkan"><i class="fa fa-send-o"></i></button>';
						}
					}else{
						$aksi .= '<button title="Edit Serahkan" '.($st_kbo!='4'? "disabled": "").' class="btn btn-xs btn-success serahkan_edit"><i class="fa fa-file-image-o"></i></button>';
						
					}
				}
			$aksi.='</div>';
			$row[] = $aksi;
			
			$row[] = $r->id;//12
			$row[] = $r->iddistributor;//13
			$row[] = $r->stpencairan;//14
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function loadKbo_detail()
    {
		$tipe_distributor     		= $this->input->post('tipe_distributor');
		$iddistributor     		= $this->input->post('iddistributor');
		$disabel     		= $this->input->post('disabel');
		$tkontrabon_id     		= $this->input->post('tkontrabon_id');
		$tipe=$this->input->post('tipe');
		$no_po=$this->input->post('no_po');
		$no_terima=$this->input->post('no_terima');
		$no_fakur=$this->input->post('no_fakur');
		$st_verifikasi=$this->input->post('st_verifikasi');
		$tgl_trx=YMDFormat($this->input->post('tgl_trx'));
		$tgl_trx2=YMDFormat($this->input->post('tgl_trx2'));
		$tanggalkontrabon=YMDFormat($this->input->post('tanggalkontrabon'));
		
		$iduser=$this->session->userdata('user_id');
				
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		$where='';
		$where2='';
        
		if ($tipe !='#'){
			$where2 .=" AND tipe='$tipe'";
		}		
		if ($no_po !=''){
			$where2 .=" AND nopemesanan='$no_po'";
		}		
		if ($no_terima !=''){
			$where2 .=" AND nopenerimaan='$no_terima'";
		}
		if ($no_fakur !=''){
			$where2 .=" AND nofakturexternal='$no_fakur'";
		}
		if ($st_verifikasi=='1'){
		$from="(SELECT D.idpenerimaan,D.tipe,T.nopenerimaan,P.nopemesanan,T.tanggalpenerimaan,T.tgl_terima,T.nofakturexternal,D.nominal_bayar as totalharga,D.st_retur,
					D.jenis_retur,D.tipe_kembali,COUNT(L.id) as file,D.id 
					FROM tkontrabon_detail D
					LEFT JOIN tgudang_penerimaan T ON T.id=D.idpenerimaan AND D.tipe='1'
					LEFT JOIN tgudang_pemesanan P ON P.id=T.idpemesanan
					LEFT JOIN tgudang_penerimaan_lampiran L ON L.idpenerimaan=T.id

					WHERE D.idkontrabon='$tkontrabon_id' AND D.tipe='1'
					GROUP BY T.id

					UNION ALL

					SELECT D.idpenerimaan,D.tipe,T.nopenerimaan,P.nopemesanan,T.tanggalpenerimaan,T.tgl_terima,T.nofakturexternal,
					D.nominal_bayar as totalharga,D.st_retur,D.jenis_retur,D.tipe_kembali,'0' as file,D.id 
					FROM tkontrabon_detail D
					LEFT JOIN tgudang_pengembalian H ON H.id=D.idpenerimaan
					LEFT JOIN tgudang_penerimaan T ON T.id=H.idpenerimaan
					LEFT JOIN tgudang_pemesanan P ON P.id=T.idpemesanan
					WHERE D.idkontrabon='$tkontrabon_id' AND D.tipe='2'
					
					UNION ALL
					
					SELECT D.idpenerimaan,D.tipe,T.no_pengajuan as nopenerimaan,'-' as nopemesanan,T.tanggal_pengajuan as tanggalpenerimaan,T.tanggal_dibutuhkan as tgl_terima,'' as nofakturexternal,D.nominal_bayar as totalharga,D.st_retur,
					D.jenis_retur,D.tipe_kembali,0 as file,D.id 
					FROM tkontrabon_detail D
					LEFT JOIN rka_pengajuan T ON T.id=D.idpenerimaan AND D.tipe='3'
					
					WHERE D.idkontrabon='$tkontrabon_id' AND D.tipe='3'
					GROUP BY T.id
					
			) as tbl WHERE tipe IS NOT NULL ".$where2;
		}else{
			$from="(
			
					SELECT T.id as idpenerimaan,'1' as tipe,T.nopenerimaan,P.nopemesanan,T.tanggalpenerimaan,T.tgl_terima,T.nofakturexternal,T.totalharga-IFNULL(T.nominal_asal,0) as totalharga,T.st_retur,
					T.jenis_retur,T.tipe_kembali as tipe_kembali,COUNT(L.id) as file,T.id 
					FROM tgudang_penerimaan T					
					LEFT JOIN tgudang_pemesanan P ON P.id=T.idpemesanan
					LEFT JOIN tgudang_penerimaan_lampiran L ON L.idpenerimaan=T.id
					WHERE T.tanggaljatuhtempo='$tanggalkontrabon' AND T.iddistributor='$iddistributor' AND T.`status`='1' AND T.stverif_kbo IN (1,2) AND T.tipe_kembali !='1'
					GROUP BY T.id 
					
					UNION ALL
					
					SELECT H.id as idpenerimaan,'2' as tipe,H.nopengembalian as nopenerimaan,'' as nopemesanan,H.tanggal as tanggalpenerimaan,H.tanggal as tgl_terima,'' as nofakturexternal,H.totalharga * -1 as totalharga 	
					,'1' as st_retur,'1' as jenis_retur,H.tipe_kembali,'' as file,H.id
					from tgudang_pengembalian H
					LEFT JOIN tgudang_penerimaan P ON P.id=H.idpenerimaan					
					LEFT JOIN mdistributor M ON M.id=H.iddistributor
					WHERE H.jenis='1' AND H.jenis_retur='1' AND H.`status`='2' AND H.stverif_kbo='1' AND H.tanggalkontrabon='$tanggalkontrabon' AND H.tipe_kembali !='1' AND H.iddistributor='$iddistributor'


					UNION ALL
					
					SELECT  H.id as idpenerimaan,'3' as tipe,H.no_pengajuan as nopenerimaan,'' as nopemesanan, H.tanggal_pengajuan as  tanggalpenerimaan
					,H.tanggal_dibutuhkan as tgl_terima,'' nofakturexternal,H.grand_total as totalharga,0 as st_retur,0 as jenis_retur,0 as tipe_kembali
					,0 as file,H.id
					from rka_pengajuan H
					
					WHERE DATE(H.tanggal_kontrabon)='$tanggalkontrabon' AND H.idvendor='$iddistributor' AND H.stverif_kbo IN (1,2) 
					GROUP BY H.tanggal_kontrabon,H.idvendor
					
				
				) as tbl WHERE tipe IS NOT NULL ".$where2;
		}
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();
		$url        = site_url('tkontrabon/');
		$url_gudang        = site_url('tgudang_penerimaan/');
		$url_pengajuan        = site_url('mrka_pengajuan/');
        $url_batal       = site_url('tgudang_pengembalian/');
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
			$status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $no;
			$row[] = $this->tipe_pemesanan($r->tipe,$r->jenis_retur).$status_retur;		
            $row[] = $r->nopenerimaan;
            $row[] = $r->nopemesanan;
            $row[] =HumanDateShort($r->tanggalpenerimaan);
            $row[] = HumanDateShort($r->tgl_terima);
            $row[] = $r->nofakturexternal;
            $row[] = number_format($r->totalharga,2);
            
			$aksi       = '<div class="btn-group">';
			if ($r->tipe !='3'){
				if ($r->jenis_retur!='1') {
					$aksi .= '<a class="view btn btn-xs btn-default" href="'.$url_gudang.'detail/'.$r->idpenerimaan.'/1'.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-eye"></i></a>';
				}else{
					$aksi .= '<a class="view btn btn-xs btn-default" href="'.$url_batal.'detail/'.$r->idpenerimaan.'/1'.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-eye"></i></a>';
					
				}
				if ($r->jenis_retur!='1') {
					if ($r->file){						
						$aksi .= '<button type="button" title="Lampiran" class="btn btn-xs btn-primary lihat_lampiran"><i class="fa fa-photo"></i></button>';
					}
					$aksi .= '<a href="'.$url_gudang.'edit/'.$r->idpenerimaan.'/'.$r->id.'/'.$tkontrabon_id.'" type="button" title="Edit" class="btn btn-xs btn-info" '.$disabel.'><i class="fa fa-pencil"></i></a>';
				}
				if ($r->jenis_retur!='1') {
					$aksi .= '<a href="'.$url_gudang.'print_data/'.$r->idpenerimaan.'" type="button" target="_blank" title="Edit" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';				
				}else{
					
					$aksi .= '<a href="'.$url_batal.'print_pengembalin/'.$r->idpenerimaan.'" type="button" target="_blank" title="Edit" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';				
				}
			}else{
				$aksi .= '<a class="view btn btn-xs btn-default" href="'.$url_pengajuan.'update/'.$r->idpenerimaan.'/disabled'.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-eye"></i></a>';
				// $aksi .= '<a href="'.$url_pengajuan.'update/'.$r->idpenerimaan.'/'.$r->id.'/'.$tkontrabon_id.'" type="button" title="Edit" class="btn btn-xs btn-info" '.$disabel.'><i class="fa fa-pencil"></i></a>';
			}
			$aksi.='</div>';
			$row[] = $aksi;
			$row[] = $r->idpenerimaan;
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	function tipe_pemesanan($tipe,$jenis_retur){
		if ($tipe=='1'){
			if ($jenis_retur=='0'){
				$tipe='<span class="label label-primary">PEMESANAN</span>';
			}elseif ($jenis_retur=='1'){
				$tipe='<span class="label label-danger">RETUR UANG</span>';
			}elseif($jenis_retur=='2'){
				$tipe='<span class="label label-default">RETUR GANTI BARANG</span>';
			}elseif($jenis_retur=='3'){
				$tipe='<span class="label label-success">RETUR BEDA</span>';
			}
		}elseif ($tipe=='2'){
			if ($jenis_retur=='1'){
				$tipe='<span class="label label-danger">RETUR UANG</span>';
			}elseif($jenis_retur=='2'){
				$tipe='<span class="label label-default">RETUR GANTI BARANG</span>';
			}elseif($jenis_retur=='3'){
				$tipe='<span class="label label-success">RETUR BEDA</span>';
			}
		}elseif ($tipe=='3'){
			$tipe='<span class="label label-warning">PENGAJUAN</span>';
		}
		
		return $tipe;
	}
	public function update_pembayaran()
    {
        $id = $this->input->post('id');
        $carabayar = $this->input->post('carabayar');
        $rekening_id = $this->input->post('rekening_id');
        $biaya_tf = RemoveComma($this->input->post('biaya_tf'));
        $biayamaterai = RemoveComma($this->input->post('biayamaterai'));
        $biayacek = RemoveComma($this->input->post('biayacek'));
        $grandtotalnominal = RemoveComma($this->input->post('grandtotalnominal'));
        $totalnominal = RemoveComma($this->input->post('ringkasan_total'));
		$nominal_cheq=0;
		$nominal_tunai=0;
		$nominal_tunai_tf=0;
		$nominal_tf=0;
		$bankid=null;
		if ($carabayar=='1'){
			$nominal_cheq=$totalnominal;
		}
		if ($carabayar=='2'){
			$nominal_tunai=$totalnominal;
		}
		if ($carabayar=='4'){
			$nominal_tunai_tf=$totalnominal;
		}
		if ($carabayar=='3'){
			$nominal_tf=$totalnominal;
		}
		
		if ($carabayar =='3' || $carabayar =='4'){
			if ($rekening_id !='#'){
				$q="SELECT bankid,iddistributor from mdistributor_bank H
					WHERE H.id='$rekening_id'";
					$row=$this->db->query($q)->row();
				if ($row){
					$bankid=$row->bankid;
					$iddistributor=$row->iddistributor;
					$sql="UPDATE mdistributor_bank SET staktif='0' WHERE iddistributor='$iddistributor'";
					$this->db->query($sql);
					$sql="UPDATE mdistributor_bank SET staktif='1' WHERE id='$rekening_id'";
					$this->db->query($sql);
				}
			}
		}else{
			$rekening_id='';
			$bankid='';
		}
        $data =array(
            'st_cara_bayar'=>$carabayar,
            'nominal_cheq'=>$nominal_cheq,
            'nominal_tunai'=>$nominal_tunai,
            'nominal_tunai_tf'=>$nominal_tunai_tf,
            'nominal_tf'=>$nominal_tf,
            'biaya_tf'=>$biaya_tf,
            'biayamaterai'=>$biayamaterai,
            'biayacek'=>$biayacek,
            'grandtotalnominal'=>$grandtotalnominal,
            'rekening_id'=>$rekening_id,
            'bankid'=>$bankid,
            'last_update'=>date('Y-m-d H:i:s'),
        );
		// print_r($data);
        $this->db->where('id', $id);
        $result=$this->db->update('tkontrabon', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function save_bank()
    {
		
        $idrekening = $this->input->post('idrekening');
        $tipe_distributor = $this->input->post('tipe_distributor');
        $iddistributor = $this->input->post('iddistributor');
        $bankid = $this->input->post('bankid');
        $cabang = $this->input->post('cabang');
        $norekening = $this->input->post('norekening');
        $atasnama = $this->input->post('atasnama');
		// if ($tipe_distributor){}
        $sql="UPDATE mdistributor_bank SET staktif='0' WHERE iddistributor='$iddistributor' AND tipe_distributor='$tipe_distributor'";
		$this->db->query($sql);
        $data =array(
            'iddistributor'=>$iddistributor,
            'tipe_distributor'=>$tipe_distributor,
            'bankid'=>$bankid,
            'cabang'=>$cabang,
            'norekening'=>$norekening,
            'atasnama'=>$atasnama,
            'staktif'=>'1',
        );
		if ($idrekening){
			$this->db->where('id',$idrekening);
			$result=$this->db->update('mdistributor_bank', $data);			
		}else{
			$result=$this->db->insert('mdistributor_bank', $data);
			
		}
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function aktivasi_bank()
    {
		
        $idrekening = $this->input->post('idrekening');
        $tipe_distributor = $this->input->post('tipe_distributor');
        $iddistributor = $this->input->post('iddistributor');
       
        $sql="UPDATE mdistributor_bank SET staktif='0' WHERE iddistributor='$iddistributor' AND tipe_distributor='$tipe_distributor'";
		$this->db->query($sql);
        $data =array(
            
            'staktif'=>'1',
        );
		
		$this->db->where('id',$idrekening);
		$result=$this->db->update('mdistributor_bank', $data);			
		
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function start_validasi()
    {
		
        $idkbo = $this->input->post('idkbo');
        $st_kbo = $this->input->post('st_kbo');
        
		$data =array(
            'status'=>$st_kbo,
            'user_finish_verifikasi'=>$this->session->userdata('user_name'),
            'tanggal_finish_verifikasi'=>date('Y-m-d H:i:s'),
        );
		$this->db->where('id',$idkbo);
		$result=$this->db->update('tkontrabon_info',$data);
        
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function start_aktifasi()
    {
		
        $idkbo = $this->input->post('idkbo');
        $st_kbo = $this->input->post('st_kbo');
        
		$data =array(
            'status'=>$st_kbo,
            'user_finish_validasi'=>$this->session->userdata('user_name'),
            'tanggal_finish_validasi'=>date('Y-m-d H:i:s'),
        );
		$this->db->where('id',$idkbo);
		$this->db->update('tkontrabon_info',$data);
		
		$this->db->where('kontrabon_info_id',$idkbo);
		$this->db->delete('tkontrabon_user');
		
        $q="INSERT INTO tkontrabon_user (kontrabon_info_id,user_id,approve)
				SELECT '$idkbo', U.user_id,'0' FROM mkontrabon_user_aktivasi U";
		$result=$this->db->query($q);
		
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function batal_validasi()
    {
		
        $idkbo = $this->input->post('idkbo');
        $st_kbo = $this->input->post('st_kbo');
        // print_r($idkbo);exit();
		$data =array(
            'status'=>$st_kbo,
        );
		$this->db->where('id',$idkbo);
		$result=$this->db->update('tkontrabon_info',$data);
		
		
		// $result=$this->db->update('tkontrabon_info',$data);
		
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function serahkan()
    {
		
        $id = $this->input->post('id');
        $tgl_serahkan = YMDFormat($this->input->post('tgl_serahkan'));
        $penerima = $this->input->post('penerima');
        // $this->model->insert_jurnal_hutang($id);
		$this->model->insert_jurnal_hutang($id);
		// exit();
		$data =array(
            'stpencairan'=>'1',
            'nama_penerima'=>$penerima,
            'user_penyerahan'=>$this->session->userdata('user_name'),
            'tanggal_penyerahan'=>$tgl_serahkan,
        );
		$this->db->where('id',$id);
		$result=$this->db->update('tkontrabon',$data);
		
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function serahkan_all()
    {
		
        $id = $this->input->post('id');
        $arr_id = $this->input->post('arr_id');
        $tgl_serahkan = YMDFormat($this->input->post('tgl_serahkan'));
        $penerima = $this->input->post('penerima');
        
		$data =array(
            'stpencairan'=>'1',
            'nama_penerima'=>$penerima,
            'user_penyerahan'=>$this->session->userdata('user_name'),
            'tanggal_penyerahan'=>$tgl_serahkan,
        );
		$this->db->where('kontrabon_info_id',$id);
		$this->db->where('stpencairan','0');
		$result=$this->db->update('tkontrabon',$data);
		foreach($arr_id as $index=>$val){
			$this->model->insert_jurnal_hutang($val);
		}
	
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function start_setuju()
    {
		
        $idkbo = $this->input->post('idkbo');
        $st_kbo = $this->input->post('st_kbo');
        
		$data =array(
            'approve'=>1,
           
        );
		$this->db->where('kontrabon_info_id',$idkbo);
		$this->db->where('user_id',$this->session->userdata('user_id'));
		$result=$this->db->update('tkontrabon_user',$data);
		
		
		
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function finish_validasi()
    {
		
        $idkbo = $this->input->post('idkbo');
        $st_kbo = $this->input->post('st_kbo');
        
		$data =array(
            'status'=>$st_kbo,
            'user_finish_verifikasi'=>$this->session->userdata('user_name'),
            'tanggal_finish_verifikasi'=>date('Y-m-d H:i:s'),
        );
		$this->db->where('id',$idkbo);
		$result=$this->db->update('tkontrabon_info',$data);
        
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function edit_tanggal()
    {
		
        $id = $this->input->post('id');
        $tgl_edit_kbo = YMDFormat($this->input->post('tgl_edit_kbo'));
		
		$q="SELECT H.id from tkontrabon_info H
			WHERE H.tanggal_kbo='$tgl_edit_kbo' AND H.`status` > 1";
		$row=$this->db->query($q)->row('id');
		if ($row) {
			$result=false;
		}else{
			$q="SELECT idpenerimaan,tipe,jenis_retur,st_retur from tkontrabon_detail D
				WHERE D.idkontrabon='$id'";		
			$row=$this->db->query($q)->result();
			foreach ($row as $r){
				if ($r->jenis_retur=='1' && $r->tipe=='2'){
					$sql="UPDATE tgudang_pengembalian SET tanggalkontrabon='$tgl_edit_kbo' WHERE id='".$r->idpenerimaan."'";
					$this->db->query($sql);
				}else{
					$sql="UPDATE tgudang_penerimaan SET tanggaljatuhtempo='$tgl_edit_kbo' WHERE id='".$r->idpenerimaan."'";
					$this->db->query($sql);
				}
				
			}
			$sql="DELETE FROM tkontrabon_detail  WHERE idkontrabon='$id'";
			$result=$this->db->query($sql);
		}
		
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function cek_tanggal(){
		$tgl_edit_kbo = YMDFormat($this->input->post('tgl_edit_kbo'));
		$q="SELECT H.id from tkontrabon_info H
			WHERE H.tanggal_kbo='$tgl_edit_kbo' AND H.`status` > 1";
		$row=$this->db->query($q)->row('id');
			// print_r($row);exit();
		if ($row){
			echo 'error';
		}else{
			echo '1';
		}
	}
	public function pilih_trx()
    {
	
        $idpenerimaan = $this->input->post('idpenerimaan');
        $tanggalkontrabon =YMDFormat($this->input->post('tanggalkontrabon'));
        $kontrabon_info_id = $this->input->post('kontrabon_info_id');
        $tkontrabon_id = $this->input->post('tkontrabon_id');
        
        $sql="UPDATE tgudang_penerimaan SET tanggaljatuhtempo='$tanggalkontrabon',stverif_kbo='2' WHERE id='$idpenerimaan'";
		$this->db->query($sql);
		$data_det=array(
			'idkontrabon'=>$tkontrabon_id,
			'tipe'=>'1',
			'idpenerimaan'=>$idpenerimaan,
			'status'=>'1',
		);
		$result=$this->db->insert('tkontrabon_detail', $data_det);
		$this->output->set_output(json_encode($result));
        
    }
	public function pilih_retur()
    {
	
        $idpenerimaan = $this->input->post('idpenerimaan');
        $tanggalkontrabon =YMDFormat($this->input->post('tanggalkontrabon'));
        $kontrabon_info_id = $this->input->post('kontrabon_info_id');
        $tkontrabon_id = $this->input->post('tkontrabon_id');
        
        $sql="UPDATE tgudang_pengembalian SET tanggalkontrabon='$tanggalkontrabon' WHERE id='$idpenerimaan'";
		$this->db->query($sql);
		$data_det=array(
			'idkontrabon'=>$tkontrabon_id,
			'tipe'=>'2',
			'idpenerimaan'=>$idpenerimaan,
			'status'=>'1',
		);
		$result=$this->db->insert('tkontrabon_detail', $data_det);
		$this->output->set_output(json_encode($result));
        
    }
	function get_index_faktur(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$tipe=$this->input->post('tipe');
		$iddistributor=$this->input->post('iddistributor');
		$no_po=$this->input->post('no_po');
		$no_terima=$this->input->post('no_terima');
		$no_fakur=$this->input->post('no_fakur');
		$cara_bayar=$this->input->post('cara_bayar');
		$status=$this->input->post('status');
		$tanggalterima1=$this->input->post('tanggalterima1');
		$tanggalterima2=$this->input->post('tanggalterima2');
		$tanggaljt1=$this->input->post('tanggaljt1');
		$tanggaljt2=$this->input->post('tanggaljt2');
		$where1='';
		$where='';
		$where2='';
		if ($tipe !='#'){
			$where2 .=" AND tipe='$tipe' ";
		}
		if ($iddistributor !='#'){
			$where .=" AND H.iddistributor='$iddistributor' ";
			$where1 .=" AND H.iddistributor='$iddistributor' ";
		}
		if ($no_po !=''){
			$where .=" AND P.nopemesanan='$no_po' ";
			$where1 .=" AND P1.nopemesanan='$no_po' ";
		}
		if ($no_terima !=''){
			$where .=" AND H.nopenerimaan='$no_terima' ";
			$where1 .=" AND P.nopenerimaan='$no_terima' ";
		}
		if ($no_fakur !=''){
			$where .=" AND H.nofakturexternal='$no_fakur' ";
			$where1 .=" AND P.nofakturexternal='$no_fakur' ";
		}
		if ($cara_bayar !='#'){
			$where .=" AND H.tipe_bayar='$cara_bayar' ";
			$where1 .=" AND P.tipe_bayar='$cara_bayar' ";
		}
		// if ($status !='#'){
			// $where .=" AND H.stverif_kbo='$status' ";
			// $where1 .=" AND H.stverif_kbo='$status' ";
		// }
		if ('' != $tanggalterima1) {
            $where .= " AND DATE(H.tgl_terima) >='".YMDFormat($tanggalterima1)."' AND DATE(H.tgl_terima) <='".YMDFormat($tanggalterima2)."'";
        }
		if ('' != $tanggaljt1) {
            $where .= " AND DATE(H.tanggaljatuhtempo) >='".YMDFormat($tanggaljt1)."' AND DATE(H.tanggaljatuhtempo) <='".YMDFormat($tanggaljt2)."'";
        }
        $from = "(
					SELECT CASE WHEN H.st_retur=0 THEN 1 ELSE 2 END as tipe,H.jenis_retur,H.id,H.tipepenerimaan,H.nopenerimaan,H.tanggalpenerimaan as tgl_trx,
					H.tgl_terima,H.tipe_bayar,H.tanggaljatuhtempo,H.nofakturexternal,
					H.iddistributor,H.totalharga,H.`status`,H.stverif_kbo
					,P.nopemesanan,M.nama as namadistributor,'0' as tipe_kembali
					from tgudang_penerimaan H
					LEFT JOIN tgudang_pemesanan P ON H.idpemesanan=P.id
					LEFT JOIN mdistributor M ON M.id=H.iddistributor
					WHERE H.stverif_kbo IN ('1','2') ".$where."
					
					UNION ALL 
					
					SELECT '2' as tipe,'1' as jenis_retur,H.id,P.tipepenerimaan,P.nopenerimaan,H.tanggal as tgl_trx,P.tgl_terima as tgl_terima,P.tipe_bayar,
					H.tanggalkontrabon as tanggaljatuhtempo,P.nofakturexternal
					,H.iddistributor,H.totalharga,P.`status`,H.stverif_kbo,P1.nopemesanan,M.nama as namadistributor,H.tipe_kembali
					from tgudang_pengembalian H
					LEFT JOIN tgudang_penerimaan P ON P.id=H.idpenerimaan
					LEFT JOIN tgudang_pemesanan P1 ON P1.id=P.idpemesanan
					LEFT JOIN mdistributor M ON M.id=H.iddistributor
					WHERE H.jenis='1' AND H.jenis_retur='1' AND H.`status`='2' AND P.tipe_bayar='2' AND H.stverif_kbo ='1' ".$where1."
				) as tbl WHERE id != '0' ".$where2." ORDER BY id ASC";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tkontrabon_verifikasi/');
            $url_gudang        = site_url('tgudang_penerimaan/');
            $url_batal       = site_url('tgudang_pengembalian/');
            $row[] = $r->tipe;
            $row[] = $r->id;
            $row[] = $no;
			$status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $this->tipe_pemesanan($r->tipe,$r->jenis_retur).$status_retur;
            $row[] = $r->nopenerimaan;
            $row[] = $r->nopemesanan;
            $row[] = HumanDateShort($r->tgl_trx);
            $row[] = HumanDateShort($r->tgl_terima);
            $row[] = $r->nofakturexternal;
            $row[] = $r->namadistributor;
            $row[] = number_format($r->totalharga,2);
            $row[] = $this->cara_bayar($r->tipe_bayar);
			if ($r->jenis_retur!='1') {
				$aksi .= '<button class="view btn btn-xs btn-success pilih_trx"  type="button"  title="Pilih"><i class="fa fa-check-square-o"></i> Pilih</button>';
			}else{
				$aksi .= '<button class="view btn btn-xs btn-danger pilih_retur" type="button"  title="Pilih"><i class="fa fa-check-square-o"></i> Pilih Retur</button>';
				
			}
			
			if ($r->tanggaljatuhtempo){				
				$row[] = HumanDateShort($r->tanggaljatuhtempo);
			}else{
				$row[] = '<span class="label label-warning">BELUM DITENTUKAN</span>';				
			}
				$row[] =$this->status($r->stverif_kbo);
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function list_index_lampiran(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$id=$this->input->post('id');
		
        $from = "(SELECT *from tgudang_penerimaan_lampiran 
					WHERE idpenerimaan='$id' ORDER BY id ASC
				) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tkontrabon_verifikasi/');
           
				$aksi .= '<a href="'.base_url().'assets/upload/penerimaan/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a>';
			$aksi.='</div>';
            
            $row[] = $no;
            $row[] = $aksi;
            $row[] = $r->size;
            $row[] = $r->user_upload.'-'.HumanDateLong($r->tanggal_upload);
            $row[] = '';
            
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function status_tipe_kembali($status){
		if ($status=='1'){
			$status=' <span class="label label-warning">TUNAI</span>';
		}elseif($status=='2'){
			$status=' <span class="label label-primary">KBO</span>';
		}else{
			$status='';
		}
		
		return $status;
	}
	function cara_bayar($tipe){
		if ($tipe=='1'){
			$tipe='<span class="label label-success">TUNAI</span>';
		}elseif($tipe=='2'){
			$tipe='<span class="label label-danger">KREDIT</span>';
		}else{
			$tipe='<span class="label label-default">-</span>';
		}
		
		return $tipe;
	}
	function status($status){
		if ($status=='0'){
			$status='<span class="label label-danger">UNVERIFIED</span>';
		}elseif($status=='1'){
			$status='<span class="label label-primary">VERIFIED</span>';
		}elseif($status=='2'){
			$status='<span class="label label-success">ACTIVATION</span>';
		}elseif($status=='3'){
			$status='<span class="label label-warning">KONTRABON</span>';
		}else{
			$status='<span class="label label-default">-</span>';
		}
		
		return $status;
	}
	public function upload_bukti() {
       $uploadDir = './assets/upload/bukti_penyerahan';
		if (!empty($_FILES)) {
			 $idkontrabon = $this->input->post('idkontrabon');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['idkontrabon'] 	= $idkontrabon;
			$detail['file_name']		= $file_name;
			$detail['size']		= formatSizeUnits($size);
			$detail['tanggal_upload']	= date('Y-m-d H:i:s');
			$detail['user_upload']	= $this->session->userdata('user_name');
			$this->db->insert('tkontrabon_upload', $detail);
		}
    }
	public function upload_bukti_semua() {
       $uploadDir = './assets/upload/bukti_penyerahan';
		if (!empty($_FILES)) {
			 $idkbo_all = $this->input->post('idkbo_all');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			$row=$this->db->query("SELECT GROUP_CONCAT(id) as idkontrabon from tkontrabon WHERE kontrabon_info_id='$idkbo_all' AND stpencairan='0' GROUP BY kontrabon_info_id")->row('idkontrabon');
			$idkontrabon=explode(',',$row);
			foreach ($idkontrabon as $key => $value){			
				$detail 					= array();
				$detail['idkontrabon']	 	= $value;
				$detail['file_name']		= $file_name;
				$detail['size']				= formatSizeUnits($size);
				$detail['tanggal_upload']	= date('Y-m-d H:i:s');
				$detail['user_upload']		= $this->session->userdata('user_name');
				$this->db->insert('tkontrabon_upload', $detail);
			}
			
		}
		
    }
	function get_data_serahkan($id){
		
		
		$arr['header'] = $this->model->get_data_serahkan($id);
		$arr['detail'] = $this->model->get_file_serahkan($id);
		$this->output->set_output(json_encode($arr));

	}
	function refresh_image($id){		
		
		$arr['detail'] = $this->model->get_file_serahkan($id);
		$this->output->set_output(json_encode($arr));

	}
	function hapus_file(){
		$id=$this->input->post('id');
		$row = $this->model->get_file_name($id);
		if(file_exists('./assets/upload/bukti_penyerahan/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/bukti_penyerahan/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from tkontrabon_upload WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	function refresh_vendor($tipe_distributor='1'){
		if ($tipe_distributor=='1'){			
			$q="SELECT * FROM mdistributor M WHERE M.`status`='1' ORDER BY M.nama ASC";
		}else{
			$q="SELECT * FROM mvendor M WHERE M.`status`='1' ORDER BY M.nama ASC";
		}
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	function insert_jurnal_hutang($id){
		$this->model->insert_jurnal_hutang($id);
	}

	
}

/* End of file Tkontrabon.php */
/* Location: ./application/controllers/Tkontrabon.php */