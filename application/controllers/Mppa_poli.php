<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use Hashids\Hashids;
class Mppa_poli extends CI_Controller {

	/**
	 * Checkin controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mppa_poli_model');
		$this->load->helper('path');
		
  }
	function index($tab='1'){
		

		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1589'))){
			$data = array();
			
			$data['tipepegawai'] 			= '0';
			$data['error'] 			= '';
			$data['title'] 			= 'Pengaturan Data ERM';
			$data['content'] 		= 'Mppa_poli/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master",'#'),
												  array("PPA Poliklinik",'mppa_poli')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function add(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1590'))){
			$data=array(
				'tipepegawai' =>'1',
				'tgl_hari'=>'',
				'tgl_bulan'=>'',
				'tgl_tahun'=>'',
				'pegawai_id' =>'',
				'nik' =>'',
				'nama' =>'',
				'jenis_kelamin' =>'',
				'tanggal_lahir' =>'',
				'tempat_lahir' =>'',
				'pendidikan_id' =>'',
				'jenis_profesi_id' =>'',
				'alamat' =>'',
				'provinsi_id' =>'12',
				'kabupaten_id' =>'213',
				'kecamatan_id' =>'',
				'kelurahan_id' =>'',
				'kodepos' =>'',
				'rt' =>'',
				'rw' =>'',
				'alamat_ktp' =>'',
				'provinsi_id_ktp' =>'12',
				'kabupaten_id_ktp' =>'213',
				'kecamatan_id_ktp' =>'',
				'kelurahan_id_ktp' =>'',
				'kodepos_ktp' =>'',
				'rt_ktp' =>'',
				'rw_ktp' =>'',
				'gelar_depan' =>'',
				'gelar_belakang' =>'',
				'nip' =>'',
				'umur_tahun' =>'',
				'umur_bulan' =>'',
				'umur_hari' =>'',
				'agama_id' =>'',
				'spesialisasi_id' =>'',
				'chk_st_domisili' =>'1',
				'warganegara_id' =>'1',
				'warganegara_id_ktp' =>'1',

			);
			$data['list_kontak'] 			= array();
			$data['list_identitas'] 			= array();
			
			$data['statuspasienbaru'] 			= '0';
			$data['id'] 			= '';
			$data['error'] 			= '';
			$data['title'] 			= 'Tambah PPA';
			$data['content'] 		= 'Mppa_poli/add';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master",'#'),
												  array("PPA",'mppa')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function getPoli()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// nama_dokter: nama_dokter,
					// nama_poli: nama_poli,
					// idtipe: idtipe,
					// pegawai_id: pegawai_id,
					// tipepegawai: tipepegawai,
					// mppa_id: mppa_id,
					
		$where='';
		$where2='';
		$where3='';
		$nama_poli =$this->input->post('nama_poli');
		$nama_dokter =$this->input->post('nama_dokter');
		$idtipe =$this->input->post('idtipe');
		$tipepegawai =$this->input->post('tipepegawai');
		$pegawai_id =$this->input->post('pegawai_id');
		$mppa_id =$this->input->post('mppa_id');
		
		if ($nama_poli !=''){
			$where .=" AND (M.nama) LIKE '%".($nama_poli)."%'";
		}
		
		if ($idtipe !='#'){
			$where .=" AND (M.idtipe) ='".($idtipe)."'";
		}
		
		$this->select = array();
		$from="
				(
						SELECT H.idpoliklinik,H.iddokter,M.idtipe,M.nama,MP.id 
						,CASE WHEN MP2.idpoliklinik IS NULL THEN 0 ELSE 1 END st_ketemu
						,MP2.setting_dokter,MP2.setting_lainnya
						FROM mpoliklinik_dokter H
						INNER JOIN  mpoliklinik M ON H.idpoliklinik=M.id
						LEFT JOIN mppa MP ON MP.pegawai_id=H.iddokter AND MP.tipepegawai='2' AND MP.staktif='1'
						LEFT JOIN mppa_poli MP2 oN MP2.idpoliklinik=H.idpoliklinik AND MP2.mppa_id=MP.id
						WHERE H.iddokter='$pegawai_id' ".$where."
						GROUP BY H.idpoliklinik
						ORDER BY M.idtipe
				) as tbl 
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama','nip');
		$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();
		  $result[] = $no;
		  $result[] = asal_rujukan($r->idtipe);
		  $result[] = $r->nama;
		  $aksi_dokter='';
		  $aksi_lain='';
		  
		  if ($r->setting_dokter){
			$aksi_dokter .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save_dokter('.$mppa_id.','.$r->idpoliklinik.',1)" type="checkbox" checked><span></span>
					</label>';	
		  }else{
			  $aksi_dokter .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save_dokter('.$mppa_id.','.$r->idpoliklinik.',0)" type="checkbox"><span></span>
					</label>';	
		  }
		  if ($r->setting_lainnya){
			$aksi_lain .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save_lain('.$mppa_id.','.$r->idpoliklinik.',1)" type="checkbox" checked><span></span>
					</label>';	
		  }else{
			  $aksi_lain .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save_lain('.$mppa_id.','.$r->idpoliklinik.',0)" type="checkbox"><span></span>
					</label>';	
		  }
		  
		  $result[] = $aksi_dokter;
		  $result[] = $aksi_lain;
		  

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function check_save_dokter(){
		$pilih=($this->input->post('pilih')=='1'?0:1);
		$idpoliklinik=$this->input->post('idpoliklinik');
		$mppa_id=$this->input->post('mppa_id');
		$q="INSERT INTO mppa_poli (mppa_id,idpoliklinik,setting_dokter) VALUES ('$mppa_id','$idpoliklinik','$pilih') 
			ON DUPLICATE KEY UPDATE setting_dokter='$pilih'";
		// print_r($q);exit;
		$hasil=$this->db->query($q);
		  json_encode($hasil);
	}
	function check_save_lain(){
		$pilih=($this->input->post('pilih')=='1'?0:1);
		$idpoliklinik=$this->input->post('idpoliklinik');
		$mppa_id=$this->input->post('mppa_id');
		$q="INSERT INTO mppa_poli (mppa_id,idpoliklinik,setting_lainnya) VALUES ('$mppa_id','$idpoliklinik','$pilih') 
			ON DUPLICATE KEY UPDATE setting_lainnya='$pilih'";
		// print_r($q);exit;
		$hasil=$this->db->query($q);
		  json_encode($hasil);
	}
  function LoadUser()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$where2='';
		$where3='';
		$nip =$this->input->post('snip');
		$namapasien =$this->input->post('snamapegawai');
		$staktif =$this->input->post('sstaktif');
		$tipepegawai =$this->input->post('stipepegawai');
		
		if ($namapasien !=''){
			$where .=" AND (H.nama) LIKE '%".($namapasien)."%'";
			$where2 .=" AND (H.nama) LIKE '%".($namapasien)."%'";
		}
		if ($nip !=''){
			$where .=" AND (H.nip) LIKE '%".($nip)."%'";
			$where2 .=" AND (H.nip) LIKE '%".($nip)."%'";
		}
		
		if ($staktif !='#'){
			$where3 .=" AND (st_terdaftar) ='".($staktif)."'";
		}
		if ($tipepegawai !='#'){
			$where3 .=" AND (tipepegawai) ='".($tipepegawai)."'";
		}
		
		
		$this->select = array();
		$from="
				(
					SELECT H.id,H.`name` as nama_user,H.username,H.roleid,MR.nama as nama_role 
					,CASE WHEN M.id IS NOT NULL THEN 1 ELSE 0 END as st_terpilih
					FROM musers H
					LEFT JOIN mroles MR ON MR.id=H.roleid
					LEFT JOIN mppa M ON M.user_id=H.id AND M.staktif='1'
					WHERE H.`status`='1'
				) as tbl 
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama_user','username','nama_role');
		$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();
		  $result[] = $no;
		  $result[] = $r->nama_user;
		  $result[] = $r->username;
		  $result[] = $r->nama_role;
		  $result[] = ($r->st_terpilih=='0'?text_default('BELUM TERPILIH'):text_success('TERPILIH'));
		 
		  $aksi = '<div class="btn-group">';
				$aksi .= '<button onclick="pilih_user('.$r->id.')" type="button" data-toggle="tooltip" title="Pililh" class="btn btn-primary btn-xs"><i class="fa fa-check"></i> Pilih</button>';
				
		  $aksi .= '</div>';
		  if ($r->st_terpilih=='1'){
			  $aksi='';
		  }
		  $result[] = $aksi;

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			
			
			$this->select = array();
			$from="
					(
						SELECT H.id,H.tipepegawai,H.nama,H.nip,H.pegawai_id 
						,SUM(CASE WHEN MP.setting_dokter=1 OR MP.setting_lainnya=1 THEN 1 ELSE 0 END) jml_poli
						FROM `mppa` H
						LEFT JOIN mppa_poli MP ON MP.mppa_id=H.id 
						WHERE H.staktif='1' AND H.tipepegawai='2'
						GROUP BY H.id
						ORDER BY H.id DESC
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array(); 
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nip','nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
		  $nama_dokter="'".$r->nama."'";
          $result = array();
          $result[] = $no;
          $result[] = ($r->tipepegawai=='1'?('PEGAWAI'):('DOKTER'));
          $result[] = $r->nip;
          $result[] = $r->nama;
          $result[] = $r->jml_poli;
          $aksi = '<div class="btn-group">';
				if (UserAccesForm($user_acces_form,array('1595'))){
					$aksi .= '<button onclick="setting_poli('.$r->id.','.$r->tipepegawai.','.$r->pegawai_id.','.$nama_dokter.')" type="button" data-toggle="tooltip" title="Atur User Akses" class="btn btn-primary btn-xs"><i class="si si-settings"></i></button>';
				}
				
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
}
