<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use Hashids\Hashids;
class Tcheckin extends CI_Controller {

	/**
	 * Checkin controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbooking_rehab_model');
		$this->load->model('Tbooking_model');
		$this->load->model('Tcheckin_model');
		$this->load->model('Tpoliklinik_pendaftaran_model');
		$this->load->helper('path');
		
  }


	function index($tab='0'){
		$q="UPDATE app_reservasi_tanggal_pasien SET st_validasi=1,status_reservasi='2' WHERE st_validasi=0 AND st_auto_validasi=1";
		$this->db->query($q);
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('1566'))){
				$tab='1';
			}elseif (UserAccesForm($user_acces_form,array('1567'))){
				$tab='2';
			}elseif (UserAccesForm($user_acces_form,array('1568'))){
				$tab='3';
			}elseif (UserAccesForm($user_acces_form,array('1569'))){
				$tab='4';
			}
		}
		if (UserAccesForm($user_acces_form,array('1566','1567','1568','1569'))){
			$data = array();
			$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("7 days"));
			$date2= date_format($date2,"d/m/Y");
			
			$data['list_reservasi_cara'] 			= list_reservasi_cara();
			// print_r($data['list_reservasi_cara']);exit;
			
			$data['list_poli'] 			= $this->Tcheckin_model->list_poli();
			$data['list_dokter'] 			= $this->Tcheckin_model->list_dokter();
			$data['tanggal_1'] 			= date('d/m/Y');
			$data['tanggal_2'] 			= $date2;
			$data['waktu_reservasi_1'] 			= '';
			$data['waktu_reservasi_2'] 			= '';
			$data['namapasien'] 			= '';
			$data['no_medrec'] 			= '';
			$data['present'] 			= '2';
			$data['tab'] 			= $tab;
			$tahun=date('Y');
			$bulan=date('m');
			$q="SELECT D.tanggal,DATE_ADD(D.tanggal,INTERVAL -2 DAY) as tanggal_next   FROM date_row D

WHERE D.tanggal BETWEEN DATE_ADD(CURRENT_DATE(),INTERVAL -6 DAY) AND CURRENT_DATE() AND D.hari='2'";
			$tanggal=$this->db->query($q)->row();
			$data['tanggaldari'] 			= DMYFormat($tanggal->tanggal_next);
			$data['tanggalsampai'] 			= DMYFormat($tanggal->tanggal);
			$data['error'] 			= '';
			$data['title'] 			= 'Checkin Poliklinik';
			$data['content'] 		= 'Tcheckin/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Reservasi",'#'),
												  array("Poliklinik Reservasi Dokter",'tcheckin')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function detail($id,$antrian_id=''){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($antrian_id!=''){
			$this->db->where('id',$id);
			$this->db->update('app_reservasi_tanggal_pasien',array('antrian_id'=>$antrian_id));
		}
		$data=$this->Tcheckin_model->load_edit_poli($id);
		// print_r($data);exit;
		$tanggallahir=DMYFormat2($data['tanggal_lahir']);
		$data['tgl_hari']=substr($tanggallahir,0,2);
		$data['tgl_bulan']=substr($tanggallahir,3,2);
		// print_r($tanggallahir.'-'.$data['tgl_bulan']);exit;
		$data['tgl_tahun']=substr($tanggallahir,6,4);
		$data['tanggaldaftar']=DMYFormat($data['waktu_reservasi']);
		$data['waktudaftar']=HISTimeFormat($data['waktu_reservasi']);
		$data['ktp']=($data['noidentitas']);
		$data['kelompokpasien']=($data['idkelompokpasien']);
		if ($data['antrian_id']==''){
			$data['antrian_id']=$antrian_id;
		}
		$data['list_poli'] 			= $this->Tcheckin_model->list_poli();
		$data['list_cara_bayar'] 			= $this->Tcheckin_model->list_cara_bayar();
		$data['list_asuransi'] 			= $this->Tcheckin_model->list_asuransi();
		$data['list_dokter'] 			= $this->Tcheckin_model->list_dokter($data['idpoli']);
		$data['list_antrian'] 			= $this->Tcheckin_model->list_antrian();
		$data['content'] 		= 'Tcheckin/detail';
		$data['breadcrum'] 	= array(
							  array("RSKB Halmahera",'#'),
							  array("Checkin",'#'),
							  array("Poliklinik Reservasi",'tcheckin')
							);
								
		$data['tab']='3';
		
		// $data['tab']='1';
		// if ($data['st_sp']=='1'){			
			// $data['tab']='5';
		// }
		// if ($data['st_sc']=='1'){			
			// $data['tab']='6';
		// }
		// print_r($data['pertemuan_id']);exit;
		if ($data['reservasi_tipe_id']=='1'){
			$hasil=get_logic_checkin_poli_petugas($data['pertemuan_id'],$data['idpoli'],$data['iddokter']);
		}else{
			$hasil=get_logic_checkin_rehab_petugas($data['pertemuan_id'],$data['idpoli'],$data['iddokter']);
		}
		if ($hasil){
			if ($hasil->st_gc=='1' && $data['st_gc']=='0'){
				$data_gc=$this->Tcheckin_model->get_gc($id);
				if (!$data_gc){
					// print_r('tidak _ada');exit;
					// $q="INSERT INTO app_reservasi_tanggal_pasien_gc_head (app_id,logo,judul,sub_header,sub_header_side,footer_1,footer_2,ttd_1,ttd_2,footer_form_1,footer_form_2)
					// SELECT '$id' as app_id,logo,judul,sub_header,sub_header_side,footer_1,footer_2,ttd_1,ttd_2,footer_form_1,footer_form_2
					// FROM merm_general WHERE id='1'";
					// $this->db->query($q);
					// $q="INSERT INTO app_reservasi_tanggal_pasien_gc (app_id,no,pertanyaan,group_jawaban_id,group_jawaban_nama,jawaban_id,jawaban_nama,jenis_isi)
						// SELECT '$id',H.`no`,H.isi,GROUP_CONCAT(J.ref_id) as group_jawaban_id,GROUP_CONCAT(R.ref) as group_jawaban_nama,0 as jawaban_id,null as jawaban_nama,jenis_isi FROM merm_general_isi H
						// LEFT JOIN merm_general_isi_jawaban J ON J.general_isi_id=H.id
						// LEFT JOIN merm_referensi R ON R.id=J.ref_id
						// WHERE H.`status`='1'

						// GROUP BY H.id
						// ORDER BY H.`no`";
					// $this->db->query($q);
					
				}
				$data['st_gc']='1';
				if ($data['st_general']=='1'){
					$data['st_general']=0;
				}
			}
			if ($hasil->st_sp=='1' && $data['st_sp']=='0'){
				$q="SELECT *FROM app_reservasi_tanggal_pasien_sp WHERE app_id='$id'";
				$data_sp=$this->db->query($q)->row();
					// print_r($data_sp);exit;
				if (!$data_sp){
					// $q="INSERT INTO app_reservasi_tanggal_pasien_sp (app_id,no,pertanyaan,group_jawaban_id,group_jawaban_nama,jawaban_id,jawaban_nama,jenis_isi)
					// SELECT '$id',H.`no`,H.isi,GROUP_CONCAT(J.ref_id) as group_jawaban_id,GROUP_CONCAT(R.ref) as group_jawaban_nama,0 as jawaban_id,null as jawaban_nama,jenis_isi 
					// FROM merm_skrining_pasien_isi H
					// LEFT JOIN merm_skrining_pasien_isi_jawaban J ON J.general_isi_id=H.id
					// LEFT JOIN merm_referensi R ON R.id=J.ref_id
					// WHERE H.`status`='1'
					// GROUP BY H.id
					// ORDER BY H.`no`";
					// $this->db->query($q);
					
				}
				$data['st_sp']='1';
				if ($data['st_skrining']=='1'){
					$data['st_skrining']=0;
				}
			}
			if ($hasil->st_sc=='1' && $data['st_sc']=='0'){
				$q="SELECT *FROM app_reservasi_tanggal_pasien_sc WHERE app_id='$id'";
				$data_sc=$this->db->query($q)->row();
				if (!$data_sc){
					// print_r('tidak _ada');exit;
					// $q="INSERT INTO app_reservasi_tanggal_pasien_sc (app_id,no,pertanyaan,group_jawaban_id,group_jawaban_nama,jawaban_id,jawaban_nama,jenis_isi)
					// SELECT '$id',H.`no`,H.isi,GROUP_CONCAT(J.ref_id) as group_jawaban_id,GROUP_CONCAT(R.ref) as group_jawaban_nama,0 as jawaban_id,null as jawaban_nama,jenis_isi 
					// FROM merm_skrining_covid_isi H
					// LEFT JOIN merm_skrining_covid_isi_jawaban J ON J.general_isi_id=H.id
					// LEFT JOIN merm_referensi R ON R.id=J.ref_id
					// WHERE H.`status`='1'
					// GROUP BY H.id
					// ORDER BY H.`no`";
					// $this->db->query($q);
					
				}
				$data['st_sc']='1';
				if ($data['st_covid']=='1'){
					$data['st_covid']=0;
				}
			}
		}
		if ($data['st_sc']=='1'){	
			
			$data_sc=$this->Tbooking_model->get_sc($id);
			$data = array_merge($data, $data_sc);
		}
		if ($data['st_sp']=='1'){	
			
			$data_sp=$this->Tcheckin_model->get_sp($id);

			$data = array_merge($data, $data_sp);
		}
		if ($data['st_gc']){	
			
			$data_gc=$this->Tcheckin_model->get_gc($id);

			$data = array_merge($data, $data_gc);
		}
		
		if ($data['st_sc']=='1' && $data['st_covid']=='0' && $data['st_skrining']=='1'){	
			
			$data['tab']='6';
		}
		if ($data['st_sp']=='1' && $data['st_general']=='1' && $data['st_skrining']=='0'){	
			
			$data['tab']='5';
		}
		if ($data['st_gc']=='1' && $data['st_general']=='0'){	
			
			$data['tab']='4';
		}
		// $data['tab']='3';
		// print_r($data['idpoli']);exit;
		$data['error']='';
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		
	}
	function update_antrian_id(){
	  $app_id=$this->input->post('app_id');
	  $antrian_id=$this->input->post('antrian_id');
	  $data=array(
		'antrian_id' =>$antrian_id
	  );
	  $this->db->where('id',$app_id);
	  $result=$this->db->update('app_reservasi_tanggal_pasien',$data);
	  $this->output->set_output(json_encode($result));
		}
	function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$reservasi_cara =$this->input->post('reservasi_cara');
			$statuspasienbaru =$this->input->post('statuspasienbaru');
			$status_reservasi =$this->input->post('status_reservasi');
			$status_kehadiran =$this->input->post('status_kehadiran');
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$no_medrec =$this->input->post('no_medrec');
			$namapasien =$this->input->post('namapasien');
			$idpoli =$this->input->post('idpoli');
			$iddokter =$this->input->post('iddokter');
			$waktu_reservasi_1 =$this->input->post('waktu_reservasi_1');
			$waktu_reservasi_2 =$this->input->post('waktu_reservasi_2');
			$notransaksi =$this->input->post('notransaksi');
			$kode_booking =$this->input->post('kode_booking');
			$tab =$this->input->post('tab');
			if ($tab=='4'){
				
				$where .=" AND (H.status_reservasi) = '0'";
			}
			if ($notransaksi !=''){
				$where .=" AND (H.notransaksi) = '".($notransaksi)."'";
			}
			if ($kode_booking !=''){
				$where .=" AND (H.kode_booking) = '".($kode_booking)."'";
			}
			if ($namapasien !=''){
				$where .=" AND (H.namapasien) LIKE '%".($namapasien)."%'";
			}
			if ($no_medrec !=''){
				$where .=" AND (H.no_medrec) LIKE '%".($no_medrec)."%'";
			}
			if ($idpoli !='#'){
				$where .=" AND (H.idpoli) ='".($idpoli)."'";
			}
			if ($iddokter !='#'){
				$where .=" AND (H.iddokter) ='".($iddokter)."'";
			}
			if ($reservasi_cara !='#'){
				$where .=" AND (H.reservasi_cara) ='".($reservasi_cara)."'";
			}
			if ($statuspasienbaru !='#'){
				$where .=" AND (H.statuspasienbaru) ='".($statuspasienbaru)."'";
			}
			if ($status_kehadiran !='#'){
				$where .=" AND (H.status_kehadiran) ='".($status_kehadiran)."'";
			}
			if ($status_reservasi !='#'){
				if ($status_reservasi=='2'){
				$where .=" AND (H.status_reservasi) >='".($status_reservasi)."'";
					
				}else{
					
				$where .=" AND (H.status_reservasi) ='".($status_reservasi)."'";
				}
			}
			
			
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(H.tanggal) >='".date('Y-m-d')."'";
			}
			if ($waktu_reservasi_1 !=''){
				$where .=" AND DATE(H.waktu_reservasi) >='".YMDFormat($waktu_reservasi_1)."' AND DATE(H.waktu_reservasi) <='".YMDFormat($waktu_reservasi_2)."'";
			}
			
			$this->select = array();
			$from="
					(
						SELECT H.notransaksi,H.id,H.reservasi_tipe_id,H.reservasi_cara,H.statuspasienbaru,H.waktu_reservasi,H.tanggal,H.st_validasi 
						,H.no_medrec,H.namapasien,P.nama as nama_poli,MD.nama as nama_dokter,H.status_reservasi,H.status_kehadiran
						,CASE WHEN H.reservasi_tipe_id=1 THEN CONCAT(DATE_FORMAT(MJ.jam_dari,'%H:%i'),' - ',DATE_FORMAT(MJ.jam_sampai,'%H:%i')) 
						ELSE CONCAT(MJ2.jam,'.00',' - ',MJ2.jam_akhir,'.00') END as jam
						,MH.namahari,H.kodehari,H.noantrian
						,(H.st_general+H.st_skrining+H.st_covid) as jml_skrining
						,H.kode_antrian,H.kode_booking
						,H.st_gc,H.st_general,H.st_sp,H.st_skrining,H.st_sc,H.st_covid
						FROM app_reservasi_tanggal_pasien H
						INNER JOIN mpoliklinik P ON P.id=H.idpoli
						LEFT JOIN mdokter MD ON MD.id=H.iddokter
						LEFT JOIN mjadwal_dokter MJ ON MJ.id=H.jadwal_id
						LEFT JOIN merm_hari MH ON MH.kodehari=H.kodehari
						LEFT JOIN merm_jam MJ2 ON MJ2.jam_id=H.jam_id
						WHERE H.reservasi_tipe_id IS NOT NULL ".$where."
						ORDER BY H.tanggal ASC,H.id ASC
					) as tbl WHERE id IS NOT NULL 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $label_skrining='';
		  $disabel_verif='';
		  // if ($r->jml_skrining<3){
			  // $label_skrining='<br><br>'.'<a href="'.base_url().'tcheckin/edit_poli/'.$r->id.'" type="button" data-toggle="tooltip" title="BELUM SKRINING" class="btn btn-danger btn-xs"><i class="fa fa-eye"></i> BELUM SKRINING</a>';;
			  // // $disabel_verif='disabled';
		  // }
		  $label_skrining='<br>'.label_skrining($r->st_gc,$r->st_general,$r->st_sp,$r->st_skrining,$r->st_sc,$r->st_covid);
          $result[] = $no;
          $result[] ='<strong>'. $r->notransaksi.'</strong><br>'.jenis_reservasi($r->reservasi_cara);
          $result[] = HumanDateLong($r->waktu_reservasi);
          $result[] = ($r->statuspasienbaru=='0'?text_primary('Pasien Lama'):text_danger('Pasien Baru'));
          $result[] = $r->no_medrec.'<br>'.$r->namapasien;
          $result[] = '<strong>'.$r->nama_poli.'</strong><br>'.$r->nama_dokter;
          $result[] ='<strong>'. $r->namahari.', '.HumanDateShort_exp($r->tanggal).'</strong><br>'.($r->reservasi_tipe_id=='1'?text_primary($r->jam):text_success($r->jam));
          $result[] = $r->kode_antrian.'<br>'.$r->kode_booking;
          $result[] = status_reservasi($r->status_reservasi).$label_skrining;
          $result[] = ($r->status_kehadiran=='0'?text_default('Menunggu Kehadiran'):text_success('Sudah Hadir'));
          $aksi = '<div class="btn-group">';
			 if ($r->status_reservasi=='2'){
				$aksi .= '<a href="'.base_url().'tcheckin/detail/'.$r->id.'" type="button" data-toggle="tooltip" title="Lihat Detail" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
			 } 
			 if ($r->status_reservasi=='3'){
				$aksi .= '<a href="'.base_url().'tcheckin/print_poli" type="button" data-toggle="tooltip" title="Print Bukti" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
			 } 
			
			
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function save_register_sp(){//Skrining pasien
		// print_r($this->input->post());exit;
		$id = $this->input->post('id');
		$btn_simpan = $this->input->post('btn_simpan');
		$data=array(
			'st_sp' =>1,
			'st_skrining' =>1,
			'user_sp' =>$this->session->userdata('user_name'),
		);
		if ($btn_simpan=='2'){//Selesai
			$data['status_reservasi']=3;
			$data['checkin_date']=date('Y-m-d H:i:s');
			$data['checkin_by']= $this->session->userdata('user_id');
		}
		$this->db->where('id',$id);
		$hasil=$this->db->update('app_reservasi_tanggal_pasien',$data);
		if ($hasil){			
			if ($btn_simpan=='2'){//Selesai
				redirect('tcheckin','location');						
			}else{
				redirect('tcheckin/detail/'.$id,'location');						
			}
		}
		
	}
	function save_register_gc(){//Skrining pasien
		// print_r($this->input->post());exit;
		$id = $this->input->post('id');
		$btn_simpan = $this->input->post('btn_simpan');
		$data=array(
			'st_gc' =>1,
			'st_general' =>1,
			'user_gc' =>$this->session->userdata('user_name'),
		);
		if ($btn_simpan=='2'){//Selesai
			$data['status_reservasi']=3;
			$data['checkin_date']=date('Y-m-d H:i:s');
			$data['checkin_by']= $this->session->userdata('user_id');
		}
		$this->db->where('id',$id);
		$hasil=$this->db->update('app_reservasi_tanggal_pasien',$data);
		if ($hasil){			
			if ($btn_simpan=='2'){//Selesai
				redirect('tcheckin','location');						
			}else{
				redirect('tcheckin/detail/'.$id,'location');						
			}
		}
		
	}
	function save_register_sc(){//Skrining Covid
		// print_r($this->input->post());exit;
		$id = $this->input->post('id');
		$btn_simpan = $this->input->post('btn_simpan');
		$data=array(
			'st_sc' =>1,
			'st_covid' =>1,
			'user_sc' =>$this->session->userdata('user_name'),
		);
		if ($btn_simpan=='2'){//Selesai
			$data['status_reservasi']=3;
			$data['checkin_date']=date('Y-m-d H:i:s');
			$data['checkin_by']= $this->session->userdata('user_id');
		}
		$this->db->where('id',$id);
		$hasil=$this->db->update('app_reservasi_tanggal_pasien',$data);
		if ($hasil){			
			if ($btn_simpan=='2'){//Selesai
				redirect('tcheckin','location');						
			}else{
				redirect('tcheckin/detail/'.$id,'location');						
			}
		}
		
	}
	function save_register_rincian(){//Skrining Covid
		// print_r($this->input->post());exit;
		$id = $this->input->post('id');
		$btn_checkin = $this->input->post('btn_checkin');
		$antrian_id = $this->input->post('antrian_id');
		
		$data=array(
			'antrian_id' =>$antrian_id,
		);
		if ($btn_checkin){//Selesai
			$data['status_reservasi']=3;
			$data['checkin_date']=date('Y-m-d H:i:s');
			$data['checkin_by']= $this->session->userdata('user_id');
		}
		$this->db->where('id',$id);
		$hasil=$this->db->update('app_reservasi_tanggal_pasien',$data);
		if ($hasil){	
			if ($btn_checkin){
			redirect('tcheckin','location');	
				
			}else{
			redirect('tcheckin/detail/'.$id,'location');	
				
			}
		}
		
	}
	function save_register_pasien(){//Save Pasien
		$id = $this->input->post('id');
		$chk_st_pengantar = ($this->input->post('chk_st_pengantar')?1:0);
		$chk_st_domisili = ($this->input->post('chk_st_domisili')?1:0);
		// print_r($this->input->post());exit;
		$data=array(
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'catatan' => $this->input->post('catatan'),
				'title' => $this->input->post('title'),
				'namapasien' => $this->input->post('namapasien'),
				'nohp' => $this->input->post('nohp'),
				'telepon' => $this->input->post('telprumah'),
				'email' => $this->input->post('email'),
				'golongan_darah' => $this->input->post('golongan_darah'),
				'tempat_lahir' => $this->input->post('tempatlahir'),
				// 'tanggal_lahir' => $tanggallahir,
				'alamatpasien' => $this->input->post('alamatpasien'),
				'agama_id' => $this->input->post('agama_id'),
				'statuskawin' => $this->input->post('statuskawin'),
				'pendidikan_id' => $this->input->post('pendidikan_id'),
				'pekerjaan_id' => $this->input->post('pekerjaan_id'),
				'warganegara' => $this->input->post('warganegara'),
				'provinsi_id' => $this->input->post('provinsi'),
				'kabupaten_id' => $this->input->post('kabupaten'),
				'kecamatan_id' => $this->input->post('kecamatan'),
				'kelurahan_id' => $this->input->post('kelurahan'),
				'rw' => $this->input->post('rw'),
				'rt' => $this->input->post('rt'),
				'kodepos' => $this->input->post('kodepos'),
				'pendidikan_id' => $this->input->post('pendidikan'),
				'pekerjaan_id' => $this->input->post('pekerjaan'),
				'jenis_id' => $this->input->post('jenis_id'),
				'noidentitas' => $this->input->post('noidentitas'),
				'namapenanggungjawab' => $this->input->post('namapenanggungjawab'),
				'teleponpenanggungjawab' => $this->input->post('telepon'),
				'noidentitaspenanggungjawab' => $this->input->post('noidentitaspenanggung'),
				'alamatpenanggungjawab' => $this->input->post('alamatpenanggungjawab'),
				'umurhari' => $this->input->post('umurhari'),
				'umurbulan' => $this->input->post('umurbulan'),
				'umurtahun' => $this->input->post('umurtahun'),
				'hubungan' => $this->input->post('hubungan'),
				'suku_id' => $this->input->post('suku_id'),
				'suku' => $this->input->post('suku'),
				'namapengantar' => $this->input->post('namapengantar'),
				'hubungan_pengantar' => $this->input->post('hubungan_pengantar'),
				'alamatpengantar' => $this->input->post('alamatpengantar'),
				'teleponpengantar' => $this->input->post('teleponpengantar'),
				'noidentitaspengantar' => $this->input->post('noidentitaspengantar'),
				'alamatpasien_ktp' => $this->input->post('alamatpasien_ktp'),
				'provinsi_id_ktp' => $this->input->post('provinsi_id_ktp'),
				'kabupaten_id_ktp' => $this->input->post('kabupaten_id_ktp'),
				'kecamatan_id_ktp' => $this->input->post('kecamatan_id_ktp'),
				'kelurahan_id_ktp' => $this->input->post('kelurahan_id_ktp'),
				'rw_ktp' => $this->input->post('rw_ktp'),
				'rt_ktp' => $this->input->post('rt_ktp'),
				'kodepos_ktp' => $this->input->post('kodepos_ktp'),
				'chk_st_pengantar' => $chk_st_pengantar,
				'chk_st_domisili' => $chk_st_domisili,
				
				'edited_by' => $this->session->userdata('user_id'),
				'edited_date' => date('Y-m-d H:i:s'),
				
			);
			
			


		// print_r($data);exit;



		// $idkelompokpasien = $this->input->post('kelompokpasien2');
		// $idrekanan = ($idkelompokpasien=='1'?$this->input->post('idrekanan2'):0);
		// $pertemuan_id = $this->input->post('pertemuan_id2');
		// $data=array(
			// 'idkelompokpasien' =>$idkelompokpasien,
			// 'idrekanan' =>$idrekanan,
			// 'pertemuan_id' =>$pertemuan_id,
			// 'edited_by' =>$this->session->userdata('user_id'),
			// 'edited_date' =>date('Y-m-d H:i:s'),
		// );
		$this->db->where('id',$id);
		$hasil=$this->db->update('app_reservasi_tanggal_pasien',$data);
		if ($hasil){			
			redirect('tcheckin/detail/'.$id,'location');	
		}
		
	}
}
