<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mspo2 extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mspo2_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data=$this->Mspo2_model->get_spo2();
		// print_r($data);exit;
		if (UserAccesForm($user_acces_form,array('1730'))){
			
			$data['tab'] 			= $tab;
			$data['error'] 			= '';
			$data['title'] 			= 'Referensi SPO2';
			$data['content'] 		= 'Mspo2/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Referensi SPO2",'mspo2')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	

  function simpan_spo2(){
		$spo2_id=$this->input->post('spo2_id');
		$data=array(
			'spo2_1'=>$this->input->post('spo2_1'),
			'spo2_2'=>$this->input->post('spo2_2'),
			'kategori_spo2'=>$this->input->post('kategori_spo2'),
			'warna'=>$this->input->post('warna'),
			'staktif'=>1,
		);
		if ($spo2_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mspo2',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$spo2_id);
		    $hasil=$this->db->update('mspo2',$data);
		}
		  
		  json_encode($hasil);
	}
	function update_satuan(){
		$satuan_spo2=$this->input->post('satuan_spo2');
		$data=array(
			'satuan_spo2'=>$this->input->post('satuan_spo2'),
			
		);
		$hasil=$this->db->update('mspo2_satuan',$data);
		
		json_encode($hasil);
	}
	
	function load_spo2()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
							FROM `mspo2` H
							where H.staktif='1'
							ORDER BY H.spo2_1


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('spo2_1','spo2_1','kategori_spo2');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->spo2_1.' - '.$r->spo2_2);
          $result[] = $r->kategori_spo2;
          $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'" type="button" >'.$r->warna.'</button>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('1732'))){
		  $aksi .= '<button onclick="edit_spo2('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		   if (UserAccesForm($user_acces_form,array('1733'))){
		  $aksi .= '<button onclick="hapus_spo2('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_spo2(){
	  $spo2_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$spo2_id);
		$hasil=$this->db->update('mspo2',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_spo2(){
	  $spo2_id=$this->input->post('id');
	  $q="SELECT *FROM mspo2 H WHERE H.id='$spo2_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
  
}
