<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpasien extends CI_Controller {

	/**
	 * Pasien controller.
	 * Developer @RendyIchtiarSaputra
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpasien_model');
		$this->load->helper('path');
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('19'))){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Pasien';
		$data['content'] 		= 'Mpasien/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Pasien",'#'),
									    			array("List",'mpasien')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Mpasien_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
          'idpasien'                  	=> $row->id,
          'no_medrec'                  	=> $row->no_medrec,
          'title'                       => $row->title,
          'nama'                        => $row->nama,
					'jenis_id'                    => $row->jenis_id,
					'ktp'                         => $row->ktp,
          'jenis_kelamin'               => $row->jenis_kelamin,
          'alamat_jalan'                => $row->alamat_jalan,
          'provinsi_id'                 => $row->provinsi_id,
          'kabupaten_id'                => $row->kabupaten_id,
          'kecamatan_id'                => $row->kecamatan_id,
          'kelurahan_id'                => $row->kelurahan_id,
          'kodepos'                     => $row->kodepos,
          'hp'                          => $row->hp,
          'telepon'                     => $row->telepon,
          'email'                       => $row->email,
          'tempat_lahir'                => $row->tempat_lahir,
          'tanggal_lahir'               => $row->tanggal_lahir,
          'umur_tahun'                  => $row->umur_tahun,
          'umur_bulan'                  => $row->umur_bulan,
          'umur_hari'                   => $row->umur_hari,
          'golongan_darah'              => $row->golongan_darah,
          'agama_id'                    => $row->agama_id,
          'warganegara'                 => $row->warganegara,
          'catatan'                 => $row->catatan,
          'suku'                        => $row->suku,
					'pendidikan_id'               => $row->pendidikan_id,
					'pekerjaan_id'                => $row->pekerjaan_id,
          'status_kawin'                => $row->status_kawin,
					'nama_keluarga'               => $row->nama_keluarga,
          'hubungan_dengan_pasien'      => $row->hubungan_dengan_pasien,
          'alamat_keluarga'             => $row->alamat_keluarga,
          'telepon_keluarga'            => $row->telepon_keluarga,
          'ktp_keluarga'                => $row->ktp_keluarga
				);
		// print_r($data);exit();
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Pasien';
				$data['content']	 	= 'Mpasien/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Pasien",'#'),
											    			array("Ubah",'mpasien')
															);

				$string = $row->tanggal_lahir;
				$timestamp = strtotime($string);
				$data['tgl_hari']  = date("d", $timestamp);
				$data['tgl_bulan'] = date("m", $timestamp);
				$data['tgl_tahun'] = date("Y", $timestamp);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mpasien','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpasien');
		}
	}

	function save(){
		if($this->Mpasien_model->updateData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mpasien','location');
		}
	}

	function getIndex()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array(
				'mfpasien.status' => '1'
				);
		$this->order  = array(
			'id' => 'DESC'
		);
		$this->group  = array();
		$this->from   = 'mfpasien';

		$this->column_search   = array('no_medrec','nama','tanggal_lahir','alamat_jalan');
		$this->column_order    = array('no_medrec','nama','tanggal_lahir','alamat_jalan');

		$list = $this->datatable->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $row) {
				$no++;
				$result = array();

				$result[] = $no;
				$result[] = $row->no_medrec;
				$result[] = $row->nama;
				$result[] = GetJenisKelamin($row->jenis_kelamin);
				$result[] = $row->tanggal_lahir;
				$result[] = $row->alamat_jalan;
				$result[] = GetStatusKawin($row->status_kawin);
				$aksi = '<div class="btn-group">';
				if (UserAccesForm($user_acces_form,array('20'))){
					$aksi .= '<a href="'.site_url().'mpasien/update/'.$row->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('21'))){
		                $aksi .= '<a href="#" data-urlindex="'.site_url().'mpasien/" data-urlremove="'.site_url().'mpasien/delete/'.$row->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
		            }
				$aksi .= '</div>';
				$result[] = $aksi;
				$data[] = $result;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_all(),
			"data" => $data
		);
		echo json_encode($output);
	}
	function delete($id){
		
		$this->Mpasien_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mrak/index','location');
	}
}
