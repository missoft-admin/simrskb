<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tkonsul extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tkonsul_model');
		$this->load->model('Tpendaftaran_poli_pasien_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->helper('path');
		
  }
	function index($tab='1'){
		$log['path_tindakan']='tkonsul';
		$this->session->set_userdata($log);
		$user_id=$this->session->userdata('user_id');
		// print_r($this->session->userdata('path_tindakan'));exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$q="SELECT H.id as login_mppa_id,H.pegawai_id as login_iddokter,H.pegawai_id as iddokter FROM mppa H WHERE H.user_id='$user_id' AND H.tipepegawai='2' AND H.staktif='1'";
		// $login_iddokter=$this->db->query($q)->row('login_iddokter');	
		if (UserAccesForm($user_acces_form,array('1796'))){
			$data = $this->db->query($q)->row_array();	
			if ($data){
				
				$data['list_poli'] 			= $this->Tkonsul_model->list_poliklinik();
				$data['list_dokter'] 			= $this->Tpendaftaran_poli_pasien_model->list_dokter();
				
				$data['idpoli'] 			= '#';
				$data['namapasien'] 			= '';
				$data['tab'] 			= $tab;
				
				$data['tanggal_1'] 			=  DMYFormat(date('Y-m-d'));
				$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
				$data['error'] 			= '';
				$data['title'] 			= 'My Pasien';
				$data['content'] 		= 'Tkonsul/index';
				$data['breadcrum'] 	= array(
													  array("RSKB Halmahera",'#'),
													  array("Tindakan Poliklinik",'#'),
													  array("My Pasien",'tpendaftaran_poli_pasien')
													);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				redirect('page404');
			}
		}else{
			redirect('page404');
		}
	}
	
	function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
						
			$idpoli =$this->input->post('idpoli');
			$iddokter =$this->input->post('iddokter');
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$pencarian =$this->input->post('pencarian');
			$tab =$this->input->post('tab');
			if ($tanggal_1 !=''){
				$where .=" AND DATE(TK.tanggal_input) >='".YMDFormat($tanggal_1)."' AND DATE(TK.tanggal_input) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(TK.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($pencarian!=''){
				$where .=" AND (H.namapasien LIKE '%".$pencarian."%')";
				$where .=" OR (H.no_medrec LIKE '%".$pencarian."%')";
			}
			if ($tab=='2'){
				$where .=" AND (TK.st_jawab) = '0'";
			}
			if ($tab=='3'){
				$where .=" AND (TK.st_jawab) = '1'";
			}
			if ($idpoli!='#'){
				$where .=" AND (H.idpoliklinik) = '$idpoli'";
			}
		
			// print_r($tab);exit;
			$this->select = array();
			$from="
					(
						SELECT H.id,H.umurhari,H.umurbulan,H.umurtahun,H.tanggal_lahir,H.kode_antrian,H.jenis_kelamin,H.title,H.nopendaftaran,H.tanggaldaftar,H.idtipe,H.tanggal
						,H.namapasien,H.no_medrec
						,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header
						,MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
						,MK.nama as nama_kelompok,MR.nama as nama_rekanan,
						MDD.nama as nama_dokter_dari,H.idkelompokpasien,
						TK.* FROM tpoliklinik_konsul TK
						INNER JOIN tpoliklinik_pendaftaran H ON H.id=TK.pendaftaran_id
						LEFT JOIN (
								".get_alergi_sql()."
								) A ON A.idpasien=H.idpasien
						INNER JOIN mdokter MD ON MD.id=H.iddokter
						INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
						INNER JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
						INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						LEFT JOIN mdokter MDD ON MDD.id=TK.iddokter_dari
						WHERE TK.status_konsul='2' AND (TK.iddokter='$iddokter' OR (TK.iddokter='0' AND TK.idpoliklinik IN (SELECT idpoliklinik FROM mpoliklinik_konsul_dokter WHERE iddokter='$iddokter'))) AND TK.iddokter_dari!='$iddokter' ".$where."
						GROUP BY TK.konsul_id
						ORDER BY TK.tanggal_input DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  $btn_1='';
		  if ($r->st_ranap=='1'){
			  $link_jawab='<a href="'.base_url().'tkonsul/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_konsul/jawab_konsul/'.$r->konsul_id.'" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block btn-primary btn-xs menu_click"><i class="fa fa-user-md"></i> '.($r->st_jawab=='0'?'JAWAB':'LIHAT JAWABAN').'</a>';
		  }else{
			  $link_jawab='<a href="'.base_url().'tkonsul/tindakan/'.$r->pendaftaran_id.'/0/erm_konsul/jawab_konsul/'.$r->konsul_id.'" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block btn-primary btn-xs menu_click"><i class="fa fa-user-md"></i> '.($r->st_jawab=='0'?'JAWAB':'LIHAT JAWABAN').'</a>';
			  
		  }
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="push-5-t"> '.$link_jawab.'</div>
									'.div_panel_kendali($user_acces_form,$r->id).'
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
										<div class="btn-group" role="group">
											<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
											<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
										</div>
									</div>

									<br>

									'.generate_erm_pemeriksaan_dropdown($r->id, $r->idtipe).'
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_5='';
		  $btn_5 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 text-primary">Alasan Konsul</div>
									<div class="text-muted"> '.$r->konsul_alasan.'</div>
									<div class="h5 text-primary push-5-t">Konsul Yang Diminta</div>
									<div class="text-muted"> '.$r->konsul_diminta.'</div>
									<div class="text-muted push-5-t"><i class="fa fa-user-md"></i> '.($r->nama_dokter_dari).'<br>('.HumanDateLong($r->tanggal_input).')</div>
									
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_5;
		  
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class="text-muted text-uppercase">'.GetAsalRujukanR($r->idtipe).' - '.$r->nama_poli.'</div>
									<div class="text-muted"><i class="fa fa-user-md"></i> '.($r->nama_dokter).'</div>
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->idkelompokpasien=='1'?'ASURANSI : '.$r->nama_rekanan:$r->nama_kelompok).'</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.($r->st_jawab?text_success('TELAH DIJAWAB'):text_warning('BELUM DIJAWAB')).'</div>
									<div class="h4 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggal).'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function tindakan($pendaftaran_id='',$st_ranap,$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		get_ppa_login();
		$data_login_ppa=get_ppa_login();
		$str='["1","2"]';
		// print_r($data_login_ppa);exit;
		if ($data_login_ppa['login_tipepegawai']=='2'){
			$login_pegawai_id=$data_login_ppa['login_pegawai_id'];
		}else{
			$login_pegawai_id='0';
		}
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// $data_satuan_ttv=array();
		$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan,makral_satuan,mcahaya_satuan,mgcs_eye_satuan,mgcs_motion_satuan,mgcs_voice_satuan,mpupil_satuan,mspo2_satuan,mgds_satuan")->row_array();
		// // $data=array();
		// $data=$this->Tpendaftaran_poli_ttv_model->get_data_header($pendaftaran_id);
		// $logic_akses_ttv=$this->Tpendaftaran_poli_ttv_model->logic_akses_ttv($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		// $logic_akses_assemen_rj=$this->Tpendaftaran_poli_ttv_model->logic_akses_assemen_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		// $logic_akses_asmed=$this->Tpendaftaran_poli_ttv_model->logic_akses_asmed_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		// $logic_akses_asmed_igd=$this->Tpendaftaran_poli_ttv_model->logic_akses_asmed_igd($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		// $logic_akses_nyeri=$this->Tpendaftaran_poli_ttv_model->logic_akses_nyeri_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		// $logic_akses_triage=$this->Tpendaftaran_poli_ttv_model->logic_akses_triage_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		// $logic_akses_assesmen_igd=$this->Tpendaftaran_poli_ttv_model->logic_akses_assesmen_igd($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		// $logic_akses_cppt=$this->Tpendaftaran_poli_ttv_model->logic_akses_cppt($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		// $logic_akses_ic=$this->Tpendaftaran_poli_ttv_model->logic_akses_ic($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		// $logic_akses_edukasi=$this->Tpendaftaran_poli_ttv_model->logic_akses_edukasi($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		// // print_r($logic_akses_assemen_rj);exit;
		
		// // $data=$this->db->query($q)->row_array();
		// // print_r($data);exit;
		// $data['tittle']='Konsultasi';
		// $data['st_cetak']=0;
		// $data['ttd']='';
			// $data_header_ttv=$this->Tpendaftaran_poli_ttv_model->get_header_ttv($pendaftaran_id);
		$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id,$st_ranap);
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tkonsul/tindakan/'.$pendaftaran_id.'/';
		$data['st_ranap'] 			= $st_ranap;
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		// $data['array_menu_kiri'] 	= $this->Tkonsul_model->list_menu_kiri($menu_atas);
		// $data['array_menu_kiri'] 	= array('input_ttv','input_data');
		
		// print_r(json_encode($data['array_menu_kiri']));exit;
		$data['idpoli'] 			= '#';
		
		$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
		$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
		$data['konsul_id'] 			= '';
		$data['status_konsul'] 			= '';
		$data['st_edited'] 			= '0';
		$data['error'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'Konsultasi';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Konsultasi ",'#'),
											  array("Dokter",'tkonsul')
											);
		$data_assemen=array('idpoliklinik'=>'','iddokter_dari'=>'');
		// print_r($data_assemen);exit;
		//Input Baru
		if ($menu_kiri=='input_konsul'){
		
			$data_assemen=$this->Tkonsul_model->get_data_konsul($pendaftaran_id,$st_ranap);
			
			if ($data_assemen){
			}else{
				$data_assemen=array('idpoliklinik'=>'','iddokter_dari'=>'');
				$data_assemen['konsul_id']='';
				$data_assemen['idpoliklinik']='';
				$data_assemen['login_pegawai_id']=$login_pegawai_id;
			}
			$data_assemen['trx_id']=$trx_id;
			
			$data = array_merge($data,$data_assemen);
		}
		//History
		if ($menu_kiri=='his_konsul' || $menu_kiri=='jawab_konsul' || $menu_kiri=='all_konsul'){
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['konsul_id']='';
			$data_assemen['idpoliklinik']='';
			$data_assemen['login_pegawai_id']=$login_pegawai_id;
			// print_r($data_assemen);exit;
			$data = array_merge($data,$data_assemen);
		}
		//History
		
		// $data['pendaftaran_id']=$pendaftaran_id;
		$data['list_dokter']=$this->Tkonsul_model->list_dokter($data_assemen['idpoliklinik']);
		$data['list_dokter_all']=$this->Tkonsul_model->list_dokter_all();
		$data['list_poli']=$this->Tkonsul_model->list_poliklinik();
		// print_r($data['list_dokter_all']);exit;
		$data = array_merge($data, backend_info());
		// $data = array_merge($data,$data_login_ppa,$data_satuan_ttv,$data_header_ttv,$data_assemen, backend_info());
		// print_r($data);exit;
		$this->parser->parse('module_template', $data);
		
	}
	function create_konsul(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $iddokter_dari=$this->input->post('iddokter_dari');
		
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'iddokter_dari' => $iddokter_dari,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_konsul' => 1,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_konsul',$data);
	  $konsul_id=$this->db->insert_id();
	  
	  
	  $this->output->set_output(json_encode($hasil));
    }
	function save_konsul(){
		get_ppa_login();
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$konsul_id=$this->input->post('konsul_id');
		$st_edited=$this->input->post('st_edited');
		$status_konsul=$this->input->post('status_konsul');
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'status_konsul' => $this->input->post('status_konsul'),
			'idpoliklinik' => $this->input->post('idpoliklinik'),
			'iddokter' => $this->input->post('iddokter'),
			'konsul_alasan' => $this->input->post('konsul_alasan'),
			'konsul_diminta' => $this->input->post('konsul_diminta'),
			'konsul_jawaban' => $this->input->post('konsul_jawaban'),
			'konsul_anjuran' => $this->input->post('konsul_anjuran'),

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			// $jml_edit=$this->db->query("SELECT COUNT(konsul_id) as jml_edit FROM tpoliklinik_cppt_his WHERE konsul_id='$konsul_id'")->row('jml_edit');
			// // IF (){
				
			// // }
			// // $jml_edit=$jml_edit +1;
			// $data['jml_edit']=$jml_edit;
			// $data['edited_ppa']=$login_ppa_id;
			// $data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_konsul=='2'){
			$data['st_edited']=0;
			$data['st_kirim']=1;
			$data['kirim_mppa']=$login_ppa_id;
			$data['kirim_date']=date('Y-m-d H:i:s');
		}
		// print_r($konsul_id);
		// exit;
		$this->db->where('konsul_id',$konsul_id);
		$result=$this->db->update('tpoliklinik_konsul',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_edit_konsul(){
		get_ppa_login();
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$konsul_id=$this->input->post('konsul_id');
		
		$data=array(
			'konsul_alasan' => $this->input->post('konsul_alasan'),
			'konsul_diminta' => $this->input->post('konsul_diminta'),

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data['edited_ppa']=$login_ppa_id;
		$data['edited_date']=date('Y-m-d H:i:s');
	
		$this->db->where('konsul_id',$konsul_id);
		$result=$this->db->update('tpoliklinik_konsul',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_edit_jawab(){
		get_ppa_login();
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$konsul_id=$this->input->post('konsul_id');
		
		$data=array(
			'konsul_jawaban' => $this->input->post('konsul_jawaban'),
			'konsul_anjuran' => $this->input->post('konsul_anjuran'),

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data['edited_ppa']=$login_ppa_id;
		$data['edited_date']=date('Y-m-d H:i:s');
	
		$this->db->where('konsul_id',$konsul_id);
		$result=$this->db->update('tpoliklinik_konsul',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_data(){
		get_ppa_login();
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$konsul_id=$this->input->post('konsul_id');
		
		$data=array(
			'status_konsul' => 0,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
	
		$this->db->where('konsul_id',$konsul_id);
		$result=$this->db->update('tpoliklinik_konsul',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function kirim_jawaban(){
		get_ppa_login();
		$konsul_id=$this->input->post('konsul_id');
		$data=array(
			'konsul_jawaban' => $this->input->post('konsul_jawaban'),
			'konsul_anjuran' => $this->input->post('konsul_anjuran'),

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$data['jawaban_ppa']=$login_ppa_id;
		$data['jawaban_date']=date('Y-m-d H:i:s');
		
		$data['st_jawab']=1;
		// exit;
		$this->db->where('konsul_id',$konsul_id);
		$result=$this->db->update('tpoliklinik_konsul',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	  function 	find_dokter(){
		  $idpoli=$this->input->post('idpoliklinik');
		  if ($idpoli!='0'){
			  $q="SELECT M.id,M.nama FROM `mpoliklinik_konsul_dokter` H
				INNER JOIN mdokter M ON M.id=H.iddokter
				WHERE H.idpoliklinik='$idpoli'";
		  }else{
			  $q="SELECT M.id,M.nama FROM `mpoliklinik_konsul_dokter` H
				INNER JOIN mdokter M ON M.id=H.iddokter
				";
		  }
		  // print_r($q);exit;
		  
		  $opsi='<option value="0" selected>Dokter Belum dipilih</option>';
		  if ($idpoli!='#'){
			  $hasil=$this->db->query($q)->result();
			  foreach ($hasil as $r){
				$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
				  
			  }
		  }
		  $this->output->set_output(json_encode($opsi));
	  }
	  function find_konsul(){
		  $konsul_id=$this->input->post('konsul_id');
		  $q="SELECT 
				MP.nopendaftaran,MP.tanggaldaftar
				,UC.nama as nama_ppa,UC.nik as nik_ppa
				,H.* ,P.nama as nama_poli,MD.nama as nama_dokter
				,MDD.nama as nama_dokter_dari
				FROM tpoliklinik_konsul H
				INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
				LEFT JOIN mpoliklinik P ON P.id=H.idpoliklinik
				LEFT JOIN mdokter MD ON MD.id=H.iddokter
				LEFT JOIN mdokter MDD ON MDD.id=H.iddokter_dari
				LEFT JOIN mppa UC ON UC.id=H.created_ppa
				WHERE H.status_konsul='2' AND H.konsul_id='$konsul_id'
				";
		  
		  $data=$this->db->query($q)->row_array();
		  $data['tglpendaftaran']=HumanDateShort($data['tanggal_input']);
		  $data['waktupendaftaran']=HumanTime($data['tanggal_input']);
		  $this->output->set_output(json_encode($data));
	  }
	  function batal_konsul(){
		  $login_ppa_id=$this->session->userdata('login_ppa_id');
		  $konsul_id=$this->input->post('konsul_id');
		  $st_edited=$this->input->post('st_edited');
		  $data=array(
			'deleted_ppa' => $login_ppa_id,
			'deleted_date' => date('Y-m-d H:i:s'),
			
		  );
		 
		  if ($st_edited=='1'){
				
		  }else{
			  $data['status_konsul']='0';
			  
		  }
		  $this->db->where('konsul_id',$konsul_id);
		  $hasil=$this->db->update('tpoliklinik_konsul',$data);
		  $this->output->set_output(json_encode($hasil));
	  }
	  function list_history_konsul(){
			$login_tipepegawai=$this->session->userdata('login_tipepegawai');
			$login_pegawai_id=$this->session->userdata('login_pegawai_id');
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
			$login_profesi_id=$this->session->userdata('login_profesi_id');
			$iddokter_dari='';
			if ($login_tipepegawai=='2'){
				$iddokter_dari=$login_pegawai_id;
				
			}
			
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$where='';
			$tanggal_input_2=$this->input->post('tanggal_input_2');
			$tanggal_input_1=$this->input->post('tanggal_input_1');
			$tgl_daftar_2=$this->input->post('tgl_daftar_2');
			$tgl_daftar_1=$this->input->post('tgl_daftar_1');
			$mppa_id=$this->input->post('mppa_id');
			$iddokter=$this->input->post('iddokter');
			$idpoli=$this->input->post('idpoli');
			$notransaksi=$this->input->post('notransaksi');
			$konsul_id=$this->input->post('konsul_id');
			$idpasien=$this->input->post('idpasien');
			$st_jawab=$this->input->post('st_jawab');
			if ($notransaksi!=''){
				$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
			}
			if ($st_jawab!='#'){
				$where .=" AND H.st_jawab = '".$st_jawab."'";
			}
			if ($iddokter!='#'){
				$where .=" AND H.iddokter = '".$iddokter."'";
			}
			if ($idpoli!='#'){
				$where .=" AND H.idpoliklinik = '".$idpoli."'";
			}
			if ($mppa_id!='#'){
				$where .=" AND H.created_ppa = '".$mppa_id."'";
			}
			if ($tanggal_input_1 !=''){
				$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
			}
			if ($tgl_daftar_1 !=''){
				$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
			}
			$akses_general=$this->db->query("SELECT *FROM setting_assesmen")->row_array();
			
			$this->select = array();
			$from="
					(
						SELECT 
						MP.nopendaftaran,MP.tanggaldaftar
						,H.* ,P.nama as nama_poli,MD.nama as nama_dokter,MJ.nama nama_penjawab,MDD.nama as nama_dokter_dari
						FROM tpoliklinik_konsul H
						INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
						LEFT JOIN mpoliklinik P ON P.id=H.idpoliklinik
						LEFT JOIN mdokter MD ON MD.id=H.iddokter
						LEFT JOIN mdokter MDD ON MDD.id=H.iddokter_dari
						LEFT JOIN mppa MJ ON MJ.id=H.jawaban_ppa
						WHERE H.status_konsul='2' AND H.idpasien='$idpasien'  AND H.iddokter_dari='$iddokter_dari' ".$where."
						ORDER BY H.tanggal_input DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nopendaftaran');
			$this->column_order  = array();

			$list = $this->datatable->get_datatables(true);
			$data = array();
			$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;
			$btn_disabel_edit='';
			
			$result = array();
			$btn_lihat='<button class="btn btn-primary  btn-xs" onclick="lihat_data('.$r->konsul_id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat Detail</button>';
			$result[] = $no;
			$aksi='';
			$aksi_print='<button class="btn btn-success  btn-xs"  type="button" title="Print" type="button"><i class="fa fa-print"></i></button>';
			// 2356
			if (UserAccesForm($user_acces_form,array('2356'))){
				$aksi_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_data('.$r->konsul_id.')" type="button" title="Hapus" type="button"><i class="fa fa-times"></i></button>';
			}else{
				$aksi_hapus='';
			}
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_lihat;	
			$aksi .= $aksi_hapus;	
			$aksi .= '</div>';
			$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
			$result[] = ($r->notransaksi).'<br>'.HumanDateLong($r->tanggal_input);
			$result[] = ($r->nama_dokter_dari);
			$result[] = ($r->nama_poli);
			$result[] = ($r->nama_dokter?$r->nama_dokter:text_danger('TIDAK DITENTUKAN'));
			$result[] = ($r->konsul_alasan);
			$result[] = ($r->st_jawab=='1'?text_success('TELAH ADA JAWABAN'):text_default('MENUNGGU JAWABAN')).($r->nama_penjawab?'<br><br>('.($r->nama_penjawab.')'):'');
			$result[] = $aksi;
			$data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
	  function list_history_konsul_all(){
			$login_tipepegawai=$this->session->userdata('login_tipepegawai');
			$login_pegawai_id=$this->session->userdata('login_pegawai_id');
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
			$login_profesi_id=$this->session->userdata('login_profesi_id');
			$iddokter_dari='';
			if ($login_tipepegawai=='2'){
				$iddokter_dari=$login_pegawai_id;
				
			}
			
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$where='';
			$tanggal_input_2=$this->input->post('tanggal_input_2');
			$tanggal_input_1=$this->input->post('tanggal_input_1');
			$tgl_daftar_2=$this->input->post('tgl_daftar_2');
			$tgl_daftar_1=$this->input->post('tgl_daftar_1');
			$iddokter_dari=$this->input->post('iddokter_dari');
			$iddokter=$this->input->post('iddokter');
			$idpoli=$this->input->post('idpoli');
			$notransaksi=$this->input->post('notransaksi');
			$konsul_id=$this->input->post('konsul_id');
			$idpasien=$this->input->post('idpasien');
			$st_jawab=$this->input->post('st_jawab');
			if ($notransaksi!=''){
				$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
			}
			if ($st_jawab!='#'){
				$where .=" AND H.st_jawab = '".$st_jawab."'";
			}
			if ($iddokter!='#' && $iddokter!='0'){
				$where .=" AND H.iddokter = '".$iddokter."'";
			}
			if ($idpoli!='#'){
				$where .=" AND H.idpoliklinik = '".$idpoli."'";
			}
			if ($iddokter_dari!='#'){
				$where .=" AND H.iddokter_dari = '".$iddokter_dari."'";
			}
			if ($tanggal_input_1 !=''){
				$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
			}
			if ($tgl_daftar_1 !=''){
				$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
			}
			
			$this->select = array();
			$from="
					(
						SELECT 
						MP.nopendaftaran,MP.tanggaldaftar
						,H.* ,P.nama as nama_poli,MD.nama as nama_dokter,MJ.nama nama_penjawab,MDD.nama as nama_dokter_dari
						FROM tpoliklinik_konsul H
						INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
						LEFT JOIN mpoliklinik P ON P.id=H.idpoliklinik
						LEFT JOIN mdokter MD ON MD.id=H.iddokter
						LEFT JOIN mdokter MDD ON MDD.id=H.iddokter_dari
						LEFT JOIN mppa MJ ON MJ.id=H.jawaban_ppa
						WHERE H.status_konsul='2' AND H.idpasien='$idpasien'  ".$where."
						ORDER BY H.tanggal_input DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nopendaftaran');
			$this->column_order  = array();

			$list = $this->datatable->get_datatables(true);
			$data = array();
			$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;
			$btn_disabel_edit='';
			
			$result = array();
			$btn_lihat='<button class="btn btn-primary  btn-xs" onclick="lihat_data('.$r->konsul_id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat Detail</button>';
			$result[] = $no;
			$aksi='';
			$aksi_print='<button class="btn btn-success  btn-xs"  type="button" title="Print" type="button"><i class="fa fa-print"></i></button>';
			if (UserAccesForm($user_acces_form,array('2356'))){
				$aksi_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_data('.$r->konsul_id.')" type="button" title="Hapus" type="button"><i class="fa fa-times"></i></button>';
			}else{
				$aksi_hapus='';
			}
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_lihat;	
			$aksi .= $aksi_hapus;	
			$aksi .= '</div>';
			$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
			$result[] = ($r->notransaksi).'<br>'.HumanDateLong($r->tanggal_input);
			$result[] = ($r->nama_dokter_dari);
			$result[] = ($r->nama_poli);
			$result[] = ($r->nama_dokter?$r->nama_dokter:text_danger('TIDAK DITENTUKAN'));
			$result[] = ($r->konsul_alasan);
			$result[] = ($r->st_jawab=='1'?text_success('TELAH ADA JAWABAN'):text_default('MENUNGGU JAWABAN')).($r->nama_penjawab?'<br><br>('.($r->nama_penjawab.')'):'');
			$result[] = $aksi;
			$data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
	  function list_history_konsul_dokter(){
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$login_pegawai_id=$this->session->userdata('login_pegawai_id');
			$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
			$login_profesi_id=$this->session->userdata('login_profesi_id');
			
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
		
			$tanggal_input_2=$this->input->post('tanggal_input_2');
			$tanggal_input_1=$this->input->post('tanggal_input_1');
			$tgl_daftar_2=$this->input->post('tgl_daftar_2');
			$tgl_daftar_1=$this->input->post('tgl_daftar_1');
			$mppa_id=$this->input->post('mppa_id');
			$iddokter=$this->input->post('iddokter');
			$idpoli=$this->input->post('idpoli');
			$notransaksi=$this->input->post('notransaksi');
			$konsul_id=$this->input->post('konsul_id');
			$idpasien=$this->input->post('idpasien');
			$st_jawab=$this->input->post('st_jawab');
			$where='';
			if ($notransaksi!=''){
				$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
			}
			if ($iddokter!='#'){
				$where .=" AND (H.iddokter = '".$iddokter."' OR (H.iddokter=0 AND H.idpoliklinik IN (SELECT idpoliklinik FROM mpoliklinik_konsul_dokter WHERE iddokter='$iddokter'))) ";
			}
			if ($idpoli!='#'){
				$where .=" AND H.idpoliklinik = '".$idpoli."'";
			}else{
				// $where .=" AND H.idpoliklinik IN (SELECT idpoliklinik FROM mpoliklinik_konsul_dokter WHERE iddokter='$login_pegawai_id') AND H.iddokter_dari !='$login_pegawai_id'";
				
			}
			if ($st_jawab!='#'){
				$where .=" AND H.st_jawab = '".$st_jawab."'";
			}
			if ($mppa_id!='#'){
				$where .=" AND H.created_ppa = '".$mppa_id."'";
			}
			if ($tanggal_input_1 !=''){
				$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
			}
			if ($tgl_daftar_1 !=''){
				$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
			}
			$akses_general=$this->db->query("SELECT *FROM setting_assesmen")->row_array();
			
			$this->select = array();
			$from="
					(
						SELECT 
						MP.nopendaftaran,MP.tanggaldaftar
						,H.* ,P.nama as nama_poli,MD.nama as nama_dokter,MJ.nama nama_penjawab,MDD.nama as nama_dokter_dari
						FROM tpoliklinik_konsul H
						LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
						LEFT JOIN mpoliklinik P ON P.id=H.idpoliklinik
						LEFT JOIN mdokter MD ON MD.id=H.iddokter
						LEFT JOIN mppa MJ ON MJ.id=H.jawaban_ppa
						LEFT JOIN mdokter MDD ON MDD.id=H.iddokter_dari
						WHERE H.status_konsul='2' AND H.idpasien='$idpasien' ".$where."
						ORDER BY H.tanggal_input DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nopendaftaran');
			$this->column_order  = array();

			$list = $this->datatable->get_datatables(true);
			$data = array();
			$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;
			$btn_disabel_edit='';
			
			$result = array();
			$btn_lihat='<button class="btn btn-primary  btn-xs" onclick="lihat_data('.$r->konsul_id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat Detail</button>';
			$result[] = $no;
			$aksi='';
			$aksi_print='<button class="btn btn-success  btn-xs"  type="button" title="Print" type="button"><i class="fa fa-print"></i></button>';
			if (UserAccesForm($user_acces_form,array('2356'))){
				$aksi_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_data('.$r->konsul_id.')" type="button" title="Hapus" type="button"><i class="fa fa-times"></i></button>';
			}else{
				$aksi_hapus='';
			}
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_lihat;	
			$aksi .= $aksi_hapus;	
			$aksi .= '</div>';
			$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
			$result[] = ($r->notransaksi).'<br>'.HumanDateLong($r->tanggal_input);
			$result[] = ($r->nama_dokter_dari);
			$result[] = ($r->nama_poli);
			$result[] = ($r->nama_dokter?$r->nama_dokter:text_danger('TIDAK DITENTUKAN'));
			$result[] = ($r->konsul_alasan);
			$result[] = ($r->st_jawab=='1'?text_success('TELAH ADA JAWABAN'):text_default('MENUNGGU JAWABAN')).($r->nama_penjawab?'<br><br>('.($r->nama_penjawab.')'):'');
			$result[] = $aksi;
			$data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
}	
