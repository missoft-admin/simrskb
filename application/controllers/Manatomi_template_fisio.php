<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manatomi_template_fisio extends CI_Controller {

	
	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Manatomi_template_fisio_model');
		$this->load->helper('path');
		
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1869'))){
			$data = array();
			$data['error'] 			= '';
			$data['title'] 			= 'Template Gambar Fisioterapi';
			$data['content'] 		= 'Manatomi_template_fisio/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Template Gambar Fisioterapi",'#'),
												  array("List",'manatomi_template_fisio')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create(){
		
		$data = array(
			'id'            => '',
			'nama_template_lokasi' 					=> '',
			'gambar_tubuh' 					=> '',
		);

		$data['error'] 			= '';
		// $data['list_layanan'] 			= $this->Manatomi_template_fisio_model->list_layanan('');
		// $data['list_sound'] 			= $this->Antrian_layanan_model->list_sound();
		// $data['list_user'] 			= $this->Manatomi_template_fisio_model->list_user('');
		$data['title'] 			= 'Tambah Template Gambar Fisioterapi';
		$data['content'] 		= 'Manatomi_template_fisio/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Template Gambar Fisioterapi",'#'),
								            array("Tambah",'manatomi_template_fisio')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$data= $this->Manatomi_template_fisio_model->getSpecified($id);
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Template Gambar Fisioterapi';
			$data['content']    = 'Manatomi_template_fisio/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Template Gambar Fisioterapi",'#'),
										array("Ubah",'manatomi_template_fisio')
										);

			// $data['statusAvailableApoteker'] = $this->Manatomi_template_fisio_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('manatomi_template_fisio');
		}
	}

	function delete($id){
		
		$result=$this->Manatomi_template_fisio_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('manatomi_template_fisio','location');
	}
	function pilih($id){
		
		$result=$this->Manatomi_template_fisio_model->pilih($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('manatomi_template_fisio','location');
	}
	function aktifkan($id){
		
		$result=$this->Manatomi_template_fisio_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){
		if ($this->input->post('btn_simpan')=='2'){
			$q="UPDATE manatomi_template_fisio SET st_default='0' WHERE st_default='1'";
			$this->db->query($q);
		}
		if($this->input->post('id') == '' ) {
			if($this->Manatomi_template_fisio_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('manatomi_template_fisio/create','location');
			}
		} else {
			if($this->Manatomi_template_fisio_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('manatomi_template_fisio/index','location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Manatomi_template_fisio/manage';

		if($id==''){
			$data['title'] = 'Tambah Template Gambar Fisioterapi';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Template Gambar Fisioterapi",'#'),
							               array("Tambah",'manatomi_template_fisio')
								           );
		}else{
			$data['title'] = 'Ubah Template Gambar Fisioterapi';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Template Gambar Fisioterapi",'#'),
							               array("Ubah",'manatomi_template_fisio')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$where='';
			
			$this->select = array();
			$from="
					(
						select H.*
						FROM manatomi_template_fisio H 
						where H.staktif='1'
						ORDER BY H.id
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template_lokasi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
          $result[] = $r->nama_template_lokasi;
          $result[] = ($r->st_default=='1'?text_primary('SEDANG DIGUNAKAN'):text_default('TIDAK DIGUNAKAN'));
         
          $aksi = '<div class="btn-group">';
			if (UserAccesForm($user_acces_form,array('1871'))){
			$aksi .= '<a href="'.site_url().'manatomi_template_fisio/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
			}
			if ($r->st_default=='0'){
				
				if (UserAccesForm($user_acces_form,array('1873'))){
					$aksi .= '<button title="Gunakan" type="button" onclick="pilih('.$r->id.')" class="btn btn-warning btn-xs "><i class="si si-check"></i></button>';
				}
			}
			if ($r->st_default=='0'){
				if (UserAccesForm($user_acces_form,array('1872'))){
					$aksi .= '<button title="Hapus" type="button" onclick="removeData('.$r->id.')" class="btn btn-danger btn-xs "><i class="fa fa-trash-o"></i></button>';
				}
			}
		
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
	
}
