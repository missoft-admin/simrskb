<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mvariable extends CI_Controller {

	/**
	 * Variable Gaji controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mvariable_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Variable Gaji';
		$data['content'] 		= 'Mvariable/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Variable Gaji",'mvariable/index'),
														array("List",'#')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 							=> '',
			'nourut' 						=> '1',
			'nama' 						=> '',
			'idtipe' 					=> '1',
			'idsub' 					=> '0',
			'status' 					=> '',
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Variable Gaji';
		$data['content'] 		= 'Mvariable/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Variable Gaji",'#'),
									    			array("Tambah",'mvariable')
													);

		$data['list_sub'] 	= array();
		$data['list_akun_pengeluaran_debit'] 	= array();
		$data['list_akun_pengeluaran_kredit'] = array();
		$data['list_akun_pendapatan_debit'] 	= array();
		$data['list_akun_pendapatan_kredit'] 	= array();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mvariable_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 							 => $row->id,
					'nourut' 						 => $row->nourut,
					'nama' 						 => $row->nama,
					'idtipe' 					 => $row->idtipe,
					'idsub' 					 => $row->idsub,
					'status' 					 => $row->status,
				);

				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Variable Gaji';
				$data['content']	 	= 'Mvariable/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Variable Gaji",'#'),
											    			array("Ubah",'mvariable')
															);

				// $data['list_sub'] 	= $this->Mvariable_model->getSubData($id);
				// $data['list_akun_pengeluaran_debit'] 	= $this->Mvariable_model->getAkunData($id, 'D', '1');
				// $data['list_akun_pengeluaran_kredit'] = $this->Mvariable_model->getAkunData($id, 'K', '1');
				// $data['list_akun_pendapatan_debit'] 	= $this->Mvariable_model->getAkunData($id, 'D', '2');
				// $data['list_akun_pendapatan_kredit'] 	= $this->Mvariable_model->getAkunData($id, 'K', '2');

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mvariable','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mvariable','location');
		}
	}

	function delete($id){
		$this->Mvariable_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mvariable','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$idsub=$this->input->post('idsub');
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->Mvariable_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					if ($idsub=='0'){
						redirect('mvariable/index/'.$this->input->post('idtipe'),'location');
					}else{						
						redirect('mvariable/update/'.$id,'location');
					}
					
				}
			} else {
				if($this->Mvariable_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					if ($idsub=='0'){
						redirect('mvariable/index/'.$this->input->post('idtipe'),'location');
					}else{						
						redirect('mvariable/update/'.$this->input->post('id'),'location');
					}
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 			 = validation_errors();
		$data['content'] 		 = 'Mvariable/manage';

		if($id==''){
			$data['title'] = 'Tambah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Tambah",'mvariable')
													);
		}else{
			$data['title'] = 'Ubah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Ubah",'mvariable')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	function simpan_add(){
		$nama=$this->input->post('nama');
		$idtipe=$this->input->post('idtipe');
		$nourut=$this->input->post('nourut');
		$idheader=$this->input->post('idheader');
		$idsub='2';
		$status='1';
		$data_det=array(
			'nama'=>$nama,
			'idtipe'=>$idtipe,
			'nourut'=>$nourut,
			'idheader'=>$idheader,
			'idsub'=>$idsub,
			'status'=>$status,
			'created_by'=>$this->session->userdata('user_id'),
			'created_date'=>date('Y-m-d H:i:s'),
		);
		$result=$this->db->insert('mvariable',$data_det);
		$this->output->set_output(json_encode($result));
	}
	function simpan_edit(){
		$tedit=$this->input->post('tedit');
		$nama=$this->input->post('nama');
		$idtipe=$this->input->post('idtipe');
		$nourut=$this->input->post('nourut');
		$idheader=$this->input->post('idheader');
		$idsub='2';
		$status='1';
		$data_det=array(
			'nama'=>$nama,
			'idtipe'=>$idtipe,
			'nourut'=>$nourut,
			'edited_by'=>$this->session->userdata('user_id'),
			'edited_date'=>date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$tedit);
		$result=$this->db->update('mvariable',$data_det);
		$this->output->set_output(json_encode($result));
	}
	function hapus(){
		$tedit=$this->input->post('id');
		
		$status='0';
		$data_det=array(
			'status'=>$status,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_date'=>date('Y-m-d H:i:s'),
			
		);
		$this->db->where('id',$tedit);
		$result=$this->db->update('mvariable',$data_det);
		$this->output->set_output(json_encode($result));
	}
	function getIndex()
	{
			$this->select = array();
			$from="(
						SELECT *FROM (
						SELECT  M.id
						,M.nama
						,M.idtipe,M.idsub,M.idheader,CASE WHEN M.idtipe='1' THEN 'Pendapatan' ELSE 'Potongan' END as nama_tipe,
						CASE WHEN M.idheader='0' THEN CONCAT(M.nourut,'.',M.id) ELSE CONCAT(MH.nourut,'.', M.idheader,'.',M.nourut) END as urutan,MH.`status`
						from mvariable M
						LEFT JOIN mvariable MH ON MH.id=M.idheader AND MH.`status`!='0'
						WHERE M.`status`='1'
						) as T 
						WHERE (T.urutan IS NOT NULL)
						ORDER BY T.urutan ASC
					)as tbl";
			// $this->from   = "mvariable";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

			$this->column_search   = array('nama','nama_tipe');
			$this->column_order    = array();

			$list = $this->datatable->get_datatables(true);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = ($r->idsub=='2'?' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   └─ '.$r->nama:$r->nama);
					$row[] = ($r->idtipe == 1 ? '<span class="label label-success">Pendapatan</span>' : '<span class="label label-danger">Potongan</span>');
					$row[] = ($r->idsub == 1 ? 'Sub' : 'Non Sub');
					// $row[] = ($r->urutan);
					$aksi = '<div class="btn-group">';
					if ($r->idsub !='2'){
					// if(button_roles('mvariable/update')) {
						$aksi .= '<a href="'.site_url().'mvariable/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
					// }
					// if(button_roles('mvariable/delete')) {
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mvariable" data-urlremove="'.site_url().'mvariable/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
					// }
					}
					$aksi .= '</div>';
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(true),
				"recordsFiltered" => $this->datatable->count_all(true),
				"data" => $data
			);
			echo json_encode($output);
	}
	
	function get_list_variable($id='1')
	{
		$html = '';
		
		$q="SELECT *FROM mvariable 
		WHERE idheader='$id' AND status='1' ORDER BY nourut";
		$result = $this->db->query($q)->result();

		foreach ($result as $row) {
			$html .= '<tr>';
			$html .= '  <td style="display:none">'.$row->id.'</td>';
			$html .= '  <td>'.$row->nourut.'</td>';
			$html .= '  <td>'.$row->nama.'</td>';
			$html .= '<td><div class="btn-group">';
			$html .= "<button type='button' title='Edit Variable' class='btn btn-xs btn-primary editSub'><i class='fa fa-pencil'></i></button>";
			$html .= "<button type='button' title='Hapus Variable' class='btn btn-xs btn-danger hapus'><i class='glyphicon glyphicon-remove'></i></button>";
			$html .= '</div></td>';
			$html .= '</tr>';
		}

		echo $html;
	}
}
