<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tretur extends CI_Controller {

/* -------------------------------------------------------------- */
	public function index()
	{
        $data['error']      = '';
        $data['title']      = 'Retur';
        $data['content']    = 'Tretur/index';
        $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Retur",'#'));

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
	}

	public function manage()
    {
        $data['error']      = '';
        $data['title']      = 'Create Retur';
        $data['content']    = 'Tretur/manage';
        $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Retur",'#'), array("Create",'#'));

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
/* -------------------------------------------------------------- */

    public function showListTransaksi() {
        $model = $this->model->getListTransaksi();
    }
}

?>