<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpoliklinik_rm_rj extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpoliklinik_rm_rj_model');
		$this->load->model('Tpendaftaran_poli_pasien_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Tbooking_rehab_model');
		$this->load->model('Tpoliklinik_rm_order_model');
		$this->load->helper('path');
		
  }
  function update_program(){
	  $q="
			SELECT H.id
			,CASE WHEN T.program_rm_id IS NOT NULL THEN T.program_rm_id ELSE 0 END as program_rm_id
			,CASE WHEN T.tipe_rm IS NOT NULL THEN T.tipe_rm ELSE 0 END as tipe_rm
			,CASE WHEN T.iddokter_peminta IS NOT NULL THEN T.iddokter_peminta ELSE 0 END as iddokter_peminta

			FROM tpoliklinik_pendaftaran H
			LEFT JOIN (
			SELECT *FROM view_program_rm H WHERE H.created_date IN (select max(created_date) from view_program_rm group by idpasien,tujuan_id) 
			) T ON T.idpasien=H.idpasien AND H.idpoliklinik=T.tujuan_id
			WHERE H.tanggal IS NOT NULL  AND H.idpoliklinik IN (SELECT idpoli FROM app_reservasi_poli ) AND H.program_rm_id IS NULL
	  ";
	  $list_data=$this->db->query($q)->result();
	  foreach ($list_data as $row){
		  $data_update=array(
			'program_rm_id' => $row->program_rm_id,
			'tipe_rm' => $row->tipe_rm,
			'program_rm_iddokter' => $row->iddokter_peminta,
		  );
		  $this->db->where('id',$row->id);
		  $this->db->update('tpoliklinik_pendaftaran',$data_update);
	  }
  }
	function index($tab='0'){
		get_ppa_login();
		$log['path_tindakan']='tpoliklinik_rm_rj';
		$this->session->set_userdata($log);
		// print_r($this->session->userdata());exit;
		$q="UPDATE app_reservasi_tanggal_pasien SET st_validasi=1,status_reservasi='2' WHERE st_validasi=0 AND st_auto_validasi=1";
		$this->db->query($q);
		
		
		// $this->update_program();
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$show_all=1;
		if ($tab=='0'){	
			if (UserAccesForm($user_acces_form,array('1879'))){
				$tab='7';
				$show_all=0;
			}elseif (UserAccesForm($user_acces_form,array('1878'))){
				$tab='6';
				$show_all=1;		
			}elseif (UserAccesForm($user_acces_form,array('1874'))){
				$tab='2';
			}elseif (UserAccesForm($user_acces_form,array('1875'))){
				$tab='3';
			}elseif (UserAccesForm($user_acces_form,array('1876'))){
				$tab='4';
			}elseif (UserAccesForm($user_acces_form,array('1877'))){
				$tab='5';
			}
		}
		// $tab=6;
		$tab_detail=1;
		
		if (UserAccesForm($user_acces_form,array('1879','1878','1874','1875','1876','1877'))){
			$data = array();
			$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("7 days"));
			$date2= date_format($date2,"d/m/Y");
			$login_ppa_id=$this->session->userdata('login_ppa_id');		
			$data['list_reservasi_cara'] 			= list_reservasi_cara();
			// print_r($data['list_reservasi_cara']);exit;
			
			$data['list_poli'] 			= $this->Tpoliklinik_rm_rj_model->list_tujuan_fisio();
			$data['list_dokter'] 			= $this->Tbooking_rehab_model->list_dokter();
			$data['jumlah_poli'] 			= count($data['list_poli']);
			$data['tanggalpetugas'] 			= date('d/m/Y');
			$data['tanggal_1'] 			= date('d/m/Y');
			$data['tanggal_2'] 			= $date2;
			$data['waktu_reservasi_1'] 			= '';
			$data['waktu_reservasi_2'] 			= '';
			$data['namapasien'] 			= '';
			$data['no_medrec'] 			= '';
			$data['present'] 			= '2';
			$data['present5'] 			= '1';
			$data['tab'] 			= $tab;
			$data['tab_detail'] 			= $tab_detail;
			$data['show_all'] 			= $show_all;
			$data['tab_all'] 			= 1;
			$tahun=date('Y');
			$bulan=date('m');
			$q="SELECT D.tanggal,DATE_ADD(D.tanggal,INTERVAL 6 DAY) as tanggal_next   FROM date_row D

WHERE D.tanggal BETWEEN DATE_ADD(CURRENT_DATE(),INTERVAL -6 DAY) AND CURRENT_DATE() AND D.hari='2'";
			$tanggal=$this->db->query($q)->row();
			$data['tanggaldari'] 			= DMYFormat($tanggal->tanggal);
			$data['tanggalsampai'] 			= DMYFormat($tanggal->tanggal_next);
			$data['tanggal'] 			= $tanggal->tanggal;
			$data['tanggal_antrian'] 			= DMYFormat(date('Y-m-d'));
			// $data['tanggal_antrian'] 			= DMYFormat(date('2023-12-30'));
			$data['login_ppa_id'] 			= $login_ppa_id;
			$data['error'] 			= '';
			$data['title'] 			= 'Booking Poliklinik';
			$data['content'] 		= 'Tpoliklinik_rm_rj/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Rehabilitas Medis",'#'),
												  array("Rawat Jalan",'tpoliklinik_rm_rj')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function getIndex_daftar()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// tipe_pemilik:tipe_pemilik,status:status,nama:nama
		$where='';
		$reservasi_cara =$this->input->post('reservasi_cara');
		$statuspasienbaru =$this->input->post('statuspasienbaru');
		$status_kehadiran =$this->input->post('status_kehadiran');
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$no_medrec =$this->input->post('no_medrec');
		$namapasien =$this->input->post('namapasien');
		$idpoli =$this->input->post('idpoli');
		$iddokter =$this->input->post('iddokter');
		$waktu_reservasi_1 =$this->input->post('waktu_reservasi_1');
		$waktu_reservasi_2 =$this->input->post('waktu_reservasi_2');
		$where .=" AND (H.status_reservasi) >='2'";
		if ($namapasien !=''){
			$where .=" AND (H.namapasien) LIKE '%".($namapasien)."%'";
		}
		if ($no_medrec !=''){
			$where .=" AND (H.no_medrec) LIKE '%".($no_medrec)."%'";
		}
		if ($idpoli !='#'){
			$where .=" AND (H.idpoli) ='".($idpoli)."'";
		}
		if ($iddokter !='#'){
			$where .=" AND (H.iddokter) ='".($iddokter)."'";
		}
		if ($reservasi_cara !='#'){
			$where .=" AND (H.reservasi_cara) ='".($reservasi_cara)."'";
		}
		if ($statuspasienbaru !='#'){
			$where .=" AND (H.statuspasienbaru) ='".($statuspasienbaru)."'";
		}
		if ($status_kehadiran !='#'){
			$where .=" AND (H.status_kehadiran) ='".($status_kehadiran)."'";
		}
		
		if ($tanggal_1 !=''){
			$where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_2)."'";
		}else{
			$where .=" AND DATE(H.tanggal) >='".date('Y-m-d')."'";
		}
		if ($waktu_reservasi_1 !=''){
			$where .=" AND DATE(H.waktu_reservasi) >='".YMDFormat($waktu_reservasi_1)."' AND DATE(H.waktu_reservasi) <='".YMDFormat($waktu_reservasi_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT H.id,H.reservasi_tipe_id,H.reservasi_cara,H.statuspasienbaru,H.waktu_reservasi,H.tanggal 
					,H.no_medrec,H.namapasien,P.nama as nama_poli,'' as nama_dokter,H.status_reservasi,H.status_kehadiran
					,CONCAT(MJ.jam,'.00',' - ',MJ.jam_akhir,'.00') as jam,MH.namahari,H.kodehari,H.noantrian
					,CASE WHEN H.idkelompokpasien=1 THEN MR.nama ELSE MK.nama END as asuransi,H.idpoliklinik,H.idkelompokpasien
					,H.st_gc,H.st_general,H.st_sp,H.st_skrining,H.st_sc,H.st_covid
					,H.iddokter,MD.nama as dokter
					FROM app_reservasi_tanggal_pasien H
					INNER JOIN mpoliklinik P ON P.id=H.idpoli
					INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
					LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
					LEFT JOIN mdokter MD ON MD.id=H.iddokter
					LEFT JOIN merm_jam MJ ON MJ.jam_id=H.jam_id
					LEFT JOIN merm_hari MH ON MH.kodehari=H.kodehari
					WHERE H.reservasi_tipe_id='2' ".$where."
					ORDER BY H.tanggal ASC,H.noantrian ASC
				) as tbl WHERE id IS NOT NULL 
			";
			// SELECT H.id,H.reservasi_tipe_id,H.reservasi_cara,H.statuspasienbaru,H.waktu_reservasi,H.tanggal 
						// ,H.no_medrec,H.namapasien,P.nama as nama_poli,H.status_reservasi,H.status_kehadiran
						// ,CONCAT(MJ.jam,'.00',' - ',MJ.jam_akhir,'.00') as jam,MH.namahari,H.kodehari,H.noantrian
						// ,(H.st_general+H.st_skrining+H.st_covid) as jml_skrining
						// FROM app_reservasi_tanggal_pasien H
						// INNER JOIN mpoliklinik P ON P.id=H.idpoli
						// LEFT JOIN merm_jam MJ ON MJ.jam_id=H.jam_id
						// LEFT JOIN merm_hari MH ON MH.kodehari=H.kodehari
						// WHERE H.reservasi_tipe_id='2' ".$where."
						// ORDER BY H.tanggal ASC,H.id ASC
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama_dokter','nama_poli','namapasien');
		$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();
		  $btn_dokter='';
		  $label_skrining='<br><br>'.label_skrining($r->st_gc,$r->st_general,$r->st_sp,$r->st_skrining,$r->st_sc,$r->st_covid);
		  if ($r->dokter==''){
				if (UserAccesForm($user_acces_form,array('1516'))){
					$btn_dokter='<br>'.'<button onclick="set_modal_petugas('.$r->id.',2,'.$r->idpoliklinik.','.$r->iddokter.')" type="button" data-toggle="tooltip" title="Tentukan Petugas" class="btn btn-danger btn-xs"><i class="si si-user-following"></i></button>';;
				}
		  }else{
			  $btn_dokter_edit='';
				if (UserAccesForm($user_acces_form,array('1517'))){
					$btn_dokter_edit='<button class="btn btn-xs btn-default" onclick="set_modal_petugas('.$r->id.',2,'.$r->idpoliklinik.','.$r->iddokter.')" type="button" data-toggle="tooltip" title="" data-original-title="Edit Petugas"><i class="fa fa-pencil"></i></button>';
					
				}
			  $btn_dokter='<br>'.$r->dokter.' &nbsp; &nbsp;'.$btn_dokter_edit;
		  }
		  
		  $result[] = $no;
		  $result[] = jenis_reservasi($r->reservasi_cara);
		  $result[] = HumanDateLong($r->waktu_reservasi);
		  $result[] = ($r->statuspasienbaru=='0'?text_primary('Pasien Lama'):text_danger('Pasien Baru'));
		  $result[] = $r->no_medrec.'<br>'.$r->namapasien;
		  $result[] = '<strong>'.$r->nama_poli.'</strong>'.$btn_dokter;
		  $result[] ='<strong>'. $r->namahari.', '.HumanDateShort_exp($r->tanggal).'</strong><br>'.text_primary($r->jam);
		  $result[] = $r->noantrian;
		  $result[] = status_reservasi($r->status_reservasi).$label_skrining;
		  $result[] = $r->asuransi;
		  // $result[] = ($r->status_kehadiran=='0'?text_default('Menunggu Kehadiran'):text_success('Sudah Hadir'));
		  $aksi = '<div class="btn-group">';
			  
				
				$aksi .= '<a href="'.base_url().'tbooking_rehab/print_poli" type="button" data-toggle="tooltip" title="Print Bukti" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
				
		  $aksi .= '</div>';
		  $result[] = $aksi;

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
   function hasil_generate(){
			$idpoli=$this->input->post('idpoli');
		$iddokter=$this->input->post('iddokter');
		  $tanggaldari=YMDFormat($this->input->post('tanggaldari'));
		  $tanggalsampai=YMDFormat($this->input->post('tanggalsampai'));
		  $where_dokter='';
		  $where_poli='';
		  // if ($iddokter!='#'){
			  // $where_dokter .=" AND H.iddokter='$iddokter'";
		  // }
		  if ($idpoli!='#'){
			  $where_dokter .=" AND H.idpoliklinik='$idpoli'";
			  $where_poli .=" AND D.idpoli='$idpoli'";
		  }
		 
		  $q_jam="SELECT M.jam_id,CONCAT(M.jam,'.00 - ',M.jam_akhir,'.00') as jam FROM merm_jam M ORDER BY M.urutan ASC";
		  $list_jam=$this->db->query($q_jam)->result();
		  
		  $q="SELECT D.tanggal,D.idpoli,D.iddokter,D.kodehari,D.jam_id,CONCAT(MJ.jam,'.00 - ',MJ.jam_akhir,'.00') as jam,D.kuota,D.jumlah_reservasi,D.saldo_kuota,MH.namahari,D.kodehari,D.catatan
					FROM app_reservasi_tanggal D
					INNER JOIN merm_jam MJ ON MJ.jam_id=D.jam_id
					LEFT JOIN merm_hari MH ON MH.kodehari=D.kodehari
				
					WHERE (D.tanggal BETWEEN '$tanggaldari' AND '$tanggalsampai') AND D.reservasi_tipe_id='2' AND D.kuota IS NOT NULL ".$where_poli."

					ORDER BY D.tanggal ASC
				";
			$record_detail=$this->db->query($q)->result_array();

			$q="SELECT D.id,D.tanggal,D.idpoli,D.iddokter,D.kodehari,D.jam_id,CONCAT(MJ.jam,'.00 - ',MJ.jam_akhir,'.00') as jam,MH.namahari
				,D.idpasien,D.no_medrec,D.namapasien,D.idkelompokpasien,KP.nama as kelompok_pasien
				,CASE WHEN D.idkelompokpasien = '1' THEN MR.nama ELSE KP.nama END as asuransi 
				,D.status_reservasi,D.status_kehadiran,MD.nama as nama_dokter
									FROM app_reservasi_tanggal_pasien D
									INNER JOIN merm_jam MJ ON MJ.jam_id=D.jam_id
									LEFT JOIN merm_hari MH ON MH.kodehari=D.kodehari
									LEFT JOIN mpasien_kelompok KP ON KP.id=D.idkelompokpasien
									LEFT JOIN mrekanan MR ON MR.id=D.idrekanan
									LEFT JOIN mdokter MD ON MD.id=D.iddokter
									WHERE (D.tanggal BETWEEN '$tanggaldari' AND '$tanggalsampai')  AND D.reservasi_tipe_id='2' ".$where_poli."

									ORDER BY D.tanggal ASC
				";
				// print_r($q);exit;
			$record_pasien=$this->db->query($q)->result_array();
			
		  $q="
				SELECT D.*,MH.namahari,CASE WHEN ML.id IS NOT NULL THEN 1 ELSE 0 END st_libur 
				FROM date_row D 
				LEFT JOIN mholiday ML ON D.tanggal BETWEEN ML.tgl_libur_awal AND ML.tgl_libur_akhir
				LEFT JOIN merm_hari MH ON MH.kodehari=D.hari 
				WHERE D.tanggal BETWEEN '$tanggaldari' AND '$tanggalsampai'";
		  $record=$this->db->query($q)->result();
		  
		  $tabel='<table class="table table table-bordered table-condensed" id="index_ls">';
		  $tabel .='<thead>';
		  $header='<tr>';
				$header .='<td class="text-center column-day" style="width:15%">JAM</td>';
		  foreach($record as $r){
				if ($r->st_libur){
				$header .='<td class="text-center column-libur">'.$r->namahari.'<br>'.DMYFormat2($r->tanggal).'</td>';
					
				}else{
				$header .='<td class="text-center column-day">'.$r->namahari.'<br>'.DMYFormat2($r->tanggal).'</td>';
					
				}
		  }
		  $tabel .=$header;
		  $tabel .='</thead>';
		  $tabel .='</thead>';
		  $tabel .='<tbody>';
		  
		  $tabel .=$this->generate_detail_ls($list_jam,$record_detail,$record,$idpoli,$record_pasien);
		  $tabel .='';
		  
		  $tabel .='</tbody>';
		  $tabel .='</tabel>';
		 
		 $arr['tabel']=$tabel;
		$this->output->set_output(json_encode($arr));
	}
	function generate_detail_ls($list_jam,$record_detail,$record,$idpoli,$record_pasien){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$jml_colspan=count($record);
		$tabel ='';
		foreach($list_jam as $r){
			$jam_id=$r->jam_id;
			$tabel .='<tr>';
			$tabel .='<td class="text-center"><strong>'.$r->jam.'</strong></td>';
			
			foreach($record as $tgl){
				$tgl_harian2="'".$tgl->tanggal."'";
				$tgl_harian=$tgl->tanggal;
				$list_kuota='';
				$list_saldo='';
				$class_libur='';
				if ($tgl->st_libur){
					$class_libur=' default ';
				}else{
					
					$class_libur=' success ';
				}
				$data_kuota = array_filter($record_detail, function($var) use ($tgl_harian,$jam_id) { 
				return ($var['tanggal'] == $tgl_harian && $var['jam_id'] == $jam_id);
				});
				// $tgl="'".$tgl->tanggal."'";
				if ($data_kuota){
					foreach($data_kuota as $data){
						$catatan=($data['catatan']?' '.text_default($data['catatan']).'<br><br>':'');
						$kodehari=$data['kodehari'];
						$btn_add='';
						$saldo_kuota=$data['saldo_kuota'];
						// $btn_add=' <a href="'.site_url().'tbooking_rehab/add_reservasi/" target="_blank" class="btn btn-xs btn-success"><i class="fa fa-plus"></i>'.$tgl_harian.'</a><br>';
						if ($tgl_harian>=date('Y-m-d')){
							// if ($saldo_kuota>0){
							// if (UserAccesForm($user_acces_form,array('1508'))){
								// $btn_add=' <a href="'.site_url().'tbooking_rehab/add_reservasi/'.$idpoli.'/'.$tgl_harian.'/'.$jam_id.'" target="_blank" class="btn btn-xs btn-success"><i class="fa fa-plus"></i> '.$saldo_kuota.'</a>';
								
							// }
							// }
							// if (UserAccesForm($user_acces_form,array('1572'))){
							// $btn_add .=' <button class="btn btn-xs btn-default"  onclick="add_kuota('.$idpoli.','.$kodehari.','.$jam_id.','.$tgl_harian2.')"  type="button" data-toggle="tooltip" title="Edit Jadwal" data-original-title="Edit Jadwal"><i class="fa fa-pencil text-success"></i></button>';
							// }
							// if (UserAccesForm($user_acces_form,array('1573'))){
							// if ($saldo_kuota>0){
								// $btn_add.='<button class="btn btn-xs btn-default"  onclick="stop_kuota('.$idpoli.','.$kodehari.','.$jam_id.','.$tgl_harian2.')"  type="button" data-toggle="tooltip" title="Remove Jadwal" data-original-title="Remove Jadwal"><i class="si si-ban text-danger"></i></button>';
									
							// }
							// }
								// $btn_add .=' <a href="'.site_url().'app_setting/reservasi_poli_jadwal/'.$idpoli.'" title="Setting Global" target="_blank" class="btn btn-xs btn-primary"><i class="si si-settings "></i></a>';
								
						}
						if ($tgl->st_libur){
							$btn_add='';
						}
						if ($saldo_kuota<=0){
							$list_saldo='<br>'.text_danger('FULL');
						}else{
							$list_saldo='<br>'.text_primary('SISA KUOTA '.$saldo_kuota);
							
						}
						$jam_id=$data['jam_id'];
						$data_pasien = array_filter($record_pasien, function($var) use ($tgl_harian,$jam_id) { 
						return ($var['tanggal'] == $tgl_harian && $var['jam_id'] == $jam_id);
						});
							$list_kuota .='
								<div class="block block-link-hover3" >
										<div class="timetable '.$class_libur.' " >
											<div class="room"><br>'.$catatan.' ('.$saldo_kuota.' / '.$data['kuota'].' SLOT)'.($saldo_kuota>0?$btn_add:'  '.text_danger('FULL').' '.$btn_add).'<br></div>';
							$list_pasien='';
								if ($data_pasien){
									foreach($data_pasien as $pasien){
										$nama_dokter=$pasien['nama_dokter'];
										$iddokter=$pasien['iddokter'];
										$str_pasien=$pasien['no_medrec'].' - '.$pasien['namapasien'].' | '.($pasien['asuransi']?$pasien['asuransi']:$pasien['kelompok_pasien']);
										$list_pasien .=$this->data_pasien($str_pasien,$pasien['status_reservasi'],$pasien['id'],$nama_dokter,$iddokter,$idpoli);
									}						
								}else{
									$list_pasien='';
								}		
							$list_kuota .=$list_pasien;				
							$list_kuota .='				
										</div> 
								</div>
							';
					}
				}else{
					$list_kuota .='<div class="list-timeline-time text-danger">No Schedule</div>';
				}
				
				$tabel .='<td class="text-center">'.$list_kuota.'</td>';
				
			}
		$tabel .='</tr>';
		}
		return $tabel;
	}
	function hasil_generate_petugas(){
			$idpoli=$this->input->post('idpoli');
			$iddokter=$this->input->post('iddokter');
		  $tanggalpetugas=YMDFormat($this->input->post('tanggalpetugas'));
		  $where_dokter='';
		  $where_poli='';
		  $where_header='';
		  if ($iddokter!='#'){
			  $where_dokter .=" AND H.iddokter='$iddokter'";
			  $where_header .=" AND H.iddokter='$iddokter'";
			  $where_poli .=" AND D.iddokter='$iddokter'";
		  }
		  if ($idpoli!='#'){
			  $where_dokter .=" AND H.idpoliklinik='$idpoli'";
			  $where_poli .=" AND D.idpoli='$idpoli'";
			  $where_header .=" AND H.idpoli='$idpoli'";
		  }
		 
		  $q_jam="SELECT M.jam_id,CONCAT(M.jam,'.00 - ',M.jam_akhir,'.00') as jam FROM merm_jam M ORDER BY M.urutan ASC";
		  $list_jam=$this->db->query($q_jam)->result();
		  
		 

			$q="SELECT D.id,D.tanggal,D.idpoli,D.iddokter,D.kodehari,D.jam_id,CONCAT(MJ.jam,'.00 - ',MJ.jam_akhir,'.00') as jam,MH.namahari
				,D.idpasien,D.no_medrec,D.namapasien,D.idkelompokpasien,KP.nama as kelompok_pasien
				,CASE WHEN D.idkelompokpasien = '1' THEN MR.nama ELSE KP.nama END as asuransi 
				,D.status_reservasi,D.status_kehadiran,MD.nama as nama_dokter
									FROM app_reservasi_tanggal_pasien D
									INNER JOIN merm_jam MJ ON MJ.jam_id=D.jam_id
									LEFT JOIN merm_hari MH ON MH.kodehari=D.kodehari
									LEFT JOIN mpasien_kelompok KP ON KP.id=D.idkelompokpasien
									LEFT JOIN mrekanan MR ON MR.id=D.idrekanan
									LEFT JOIN mdokter MD ON MD.id=D.iddokter
									WHERE (D.tanggal='$tanggalpetugas')  AND D.reservasi_tipe_id='2' ".$where_poli."

									ORDER BY D.tanggal ASC
				";
			$record_pasien=$this->db->query($q)->result_array();
			// print_r($record_pasien);exit;
		  $q="SELECT H.iddokter,M.nama FROM app_reservasi_tanggal_pasien H
				LEFT JOIN mdokter M ON M.id=H.iddokter
				WHERE H.tanggal='$tanggalpetugas' AND H.reservasi_tipe_id='2' ".$where_header."
				GROUP BY H.iddokter
				ORDER BY M.nama";
		  $record=$this->db->query($q)->result();
		  
		  if ($record){
		  $tabel='<table class="table table table-bordered table-condensed" id="index_petugas">';
		  $tabel .='<thead>';
		  $header='<tr>';
		  $header .='<td class="text-center column-day" style="width:15%">JAM</td>';
		  foreach($record as $r){
				if ($r->iddokter){
				$header .='<td class="text-center column-day"><i class="si si-user-following"></i> '.$r->nama.'</td>';
					
				}else{
					
				$header .='<td class="text-center column-nothing text-danger"><i class="si si-user-unfollow"></i> BELUM ADA DOKTER</td>';
				}
		  }
		  $tabel .=$header;
		  $tabel .='</thead>';
		  $tabel .='</thead>';
		  $tabel .='<tbody>';
		  foreach($list_jam as $r){
			  $jam_id=$r->jam_id;
			  $tabel .='<tr>';
			  $tabel .='<td class="text-center"><strong>'.$r->jam.'</strong></td>';
			  
			   foreach($record as $row_dokter){
					$list_hasil='';
					$iddokter=$row_dokter->iddokter;
					$data_pasien = array_filter($record_pasien, function($var) use ($jam_id,$iddokter) { 
					return ($var['jam_id'] == $jam_id && $var['iddokter'] == $iddokter);
					});
					if ($data_pasien){
					foreach($data_pasien as $pasien){
						$nama_dokter=$pasien['nama_dokter'];
						$str_pasien=$pasien['no_medrec'].' - '.$pasien['namapasien'].' | '.($pasien['asuransi']?$pasien['asuransi']:$pasien['kelompok_pasien']);
						$list_hasil .=$this->data_pasien_jadwal($str_pasien,$pasien['status_reservasi'],$pasien['id'],$nama_dokter,$iddokter,$idpoli);
					}
					$tabel .='<td class="text-left">'.$list_hasil.'</td>';
					}else{
					$tabel .='<td><div class="list-timeline-time text-danger">No Schedule</div></td>';
					}	
			   }
			  
			  $tabel .='</tr>';
			  
		  }
		  
		  $tabel .='</tbody>';
		  $tabel .='</tabel>';
		  }else{
			  $tabel='<div class="col-md-12">
                                    <!-- Danger Alert -->
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h3 class="font-w300 push-15">Tidak ada jadwal</h3>
                                        <p>Opps, Belum Ada Jadwal <a class="alert-link" href="javascript:void(0)"> Rehabilitas Medis</a>!</p>
                                    </div>
                                    <!-- END Danger Alert -->
                                </div>';
		  }
		 
		 $arr['tabel']=$tabel;
		$this->output->set_output(json_encode($arr));
	}
	function data_pasien_jadwal($pasien,$status_reservasi,$id,$nama_dokter,$iddokter,$idpoli){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// $hasil='<p class="nice-copy2">';
		$nama_class="";
			$hasil='';		
		if ($status_reservasi=='0'){
			$nama_class='btn-danger';
		}
		if ($status_reservasi=='1'){
			$nama_class='btn-default';
		}
		if ($status_reservasi=='2'){
			$nama_class='btn-default';
		}
		if ($status_reservasi=='3'){
			$nama_class='btn-primary';
		}
		if ($status_reservasi=='4'){
			$nama_class='btn-success';
		}
		$hasil .='<div class="btn-group">';
		$hasil .='<a href="'.site_url().'tbooking_rehab/edit_poli/'.$id.'" title="'.$nama_dokter.'" target="_blank" class="btn btn-xs '.$nama_class.'">'.$pasien.'</a>';
		$hasil .=' <button onclick="set_modal_petugas('.$id.',4,'.$idpoli.','.$iddokter.')" type="button" data-toggle="tooltip" title="Tentukan Petugas" class="btn btn-danger btn-xs"><i class="si si-user-following"></i></button>';
		// if ($iddokter){
			// if (UserAccesForm($user_acces_form,array('1517'))){
		// $hasil .='<button onclick="set_modal_petugas('.$id.',3,'.$iddokter.')" type="button" data-toggle="tooltip" title="Ganti Petugas" class="btn btn-danger btn-xs"><i class="fa fa fa-pencil"></i></button>';
			// }
		// }else{
			// if (UserAccesForm($user_acces_form,array('1516'))){
		// $hasil .='<button onclick="set_modal_petugas('.$id.',3,'.$iddokter.')" type="button" data-toggle="tooltip" title="Tentukan Petugas" class="btn btn-danger btn-xs"><i class="si si-user-following"></i></button>';
			
		// }
		// }
		$hasil .='</div>';
		// $hasil .='</p>';
		return $hasil;
	}
	function index_all_patien(){
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// tipe_pemilik:tipe_pemilik,status:status,nama:nama
		$where='';
		$where_2='';
		$where_tipe_1='';
		$where_tipe_2='';
		$user_id=$this->session->userdata('user_id');		
		$login_ppa_id=$this->session->userdata('login_ppa_id');		
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');		
			// print_r($this->session->userdata());exit;
		$all =$this->input->post('all');
		$tanggal =$this->input->post('tanggal');
		$iddokter_patien =$this->input->post('iddokter_patien');
		$idpoli =$this->input->post('idpoli');
		$no_medrec =$this->input->post('no_medrec');
		$namapasien =$this->input->post('namapasien');
		$status_transaksi =$this->input->post('status_transaksi');
		
		$where .=" AND DATE(H.tanggal)='".YMDFormat($tanggal)."' ";
		
		if ($idpoli!='#'){
			$where .="
				AND H.idpoliklinik='$idpoli'
			";
		}else{
			$where .="
				AND H.idpoliklinik IN (
			SELECT MP.id FROM `app_reservasi_poli` H
			INNER JOIN app_reservasi_poli_user U ON U.idpoli=H.idpoli AND U.iduser='$user_id'
			INNER JOIN mpoliklinik MP ON MP.id=H.idpoli)
			";
		}
		if ($iddokter_patien!='#'){
			$where .=" AND H.program_rm_iddokter='$iddokter_patien'";
		}
		if ($no_medrec!=''){
			$where .=" AND H.no_medrec LIKE '%".$no_medrec."%'";
		}
		if ($namapasien!=''){
			$where .=" AND H.namapasien LIKE '%".$namapasien."%'";
		}
		if ($status_transaksi!='1'){
			if ($status_transaksi=='2'){
				$where .=" AND H.st_panggil='0'";
				
			}
			if ($status_transaksi=='3'){
				$where .=" AND H.st_panggil='1'";
				
			}
			if ($status_transaksi=='4'){
				$where .=" AND H.st_panggil='2'";
				
			}
		}
		if ($all=='0'){
			$where .=" AND H.iddokter='$login_pegawai_id'";
		}
		$this->select = array();
		$from="
		(
			SELECT H.id,H.umurhari,H.umurbulan,H.umurtahun,H.tanggal_lahir,H.jenis_kelamin,H.title,H.tanggaldaftar,H.idtipe,H.tanggal,H.namapasien,H.no_medrec 
					,H.kode_antrian,H.nopendaftaran,H.st_tindak_fisio,H.st_panggil
					,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header
					,MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
					,MK.nama as nama_kelompok,MR.nama as nama_rekanan,H.idpasien,H.program_rm_id,H.tipe_rm
					,CASE WHEN H.tipe_rm=1 THEN RMO.nopermintaan ELSE RMR.nopermintaan END as nopermintaan
					,CASE WHEN H.tipe_rm=1 THEN RMO.created_date ELSE RMR.created_date END as tanggal_permintaan
					,MDD.nama as dokter_peminta,H.status_reservasi,H.idpoliklinik,H.iddokter,A.tujuan_antrian_id
					,CONCAT(MJ2.jam ,'.00 - ',MJ2.jam_akhir,'.00') as jam
			FROM tpoliklinik_pendaftaran H
			LEFT JOIN (
							".get_alergi_sql()."
						) A ON A.idpasien=H.idpasien
			LEFT JOIN mdokter MD ON MD.id=H.iddokter
			INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
			INNER JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
			INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
			LEFT JOIN tpoliklinik_rm_order RMO ON RMO.assesmen_id=H.program_rm_id AND H.tipe_rm='1'
			LEFT JOIN tpoliklinik_rm_perencanaan RMR ON RMR.assesmen_id=H.program_rm_id AND H.tipe_rm='2'
			LEFT JOIN mppa MDD ON MDD.id=H.program_rm_iddokter
			LEFT JOIN merm_jam MJ2 ON MJ2.jam_id=H.jadwal_id
			LEFT JOIN app_reservasi_poli A ON A.idpoli=H.idpoliklinik
			WHERE  H.tanggal IS NOT NULL AND H.`status` > 0 ".$where."
			GROUP BY H.id
			ORDER BY MJ2.jam,H.noantrian
		) as tbl
		";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama_dokter','namapasien','no_medrec');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
		$no++;
		$result = array();
		$def_image='';
		if ($r->jenis_kelamin=='1'){
		  $def_image='def_1.png';
		}else{
		  $def_image='def_2.png';
		  
		}
		$btn_petugas='&nbsp;&nbsp;&nbsp;<button onclick="set_modal_petugas_poli('.$r->id.',6,'.$r->idpoliklinik.','.$r->iddokter.')" class="btn btn-default btn-xs pull-5-l"><i class="fa fa-pencil"></i></button>';
		$btn_rj='<a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->id.'/erm_rj/input_asmed_fisio" class="btn btn-primary btn-xs"><i class="fa fa-user-md"></i> LAYANI</a>';
		$btn_pilih='<div class="push-5-t"><a href="'.base_url().'tpoliklinik_rm_rj/tindakan/'.$r->id.'/erm_rm/pilih_program" class="btn btn-success btn-xs"><i class="si si-notebook"></i> PROGRAM REHABILITAS</a><div>';
		$btn_1='';
		$btn_1 .='<table class="block-table text-left" width="10%">
					<tbody>
						<tr>
							
							<td class="bg-white" style="width: 100%;">
								<div class="push-5-t"> '.$btn_rj.'</div>
								'.div_panel_kendali($user_acces_form,$r->id).'
								'.$btn_pilih.'
							</td>
						</tr>
					</tbody>
				</table>';
				

		$result[] = $btn_1;
		$btn_2='';
		$btn_2 .='<table class="block-table text-left">
					<tbody>
						<tr>
							<td style="width: 15%;">
								<div class="text-center">
									<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
								</div>
								<div class="text-center bg-warning h5 push-5-t"><strong>
									'.$r->kode_antrian.'</strong>
								</div>
							</td>
							<td class="bg-white" style="width: 85%;">
								<div class="h5 font-w700 text-primary">'.$r->namapasien.' - '.$r->no_medrec.'</div>
								<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
								<div class="h5 text-muted text-uppercase push-5-t"> 
								<button class="btn btn-success btn-sm push-10-r" onclick="call_antrian_manual('.$r->id.','.$r->tujuan_antrian_id.')" type="button" title="Recall"><i class="glyphicon glyphicon-volume-up"></i></button>
								<div class="btn-group" role="group">
									<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
									<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
								</div>
								
								</div>
								<div class="push-5-t"><strong> '.$r->nopermintaan.' | '.($r->tanggal_permintaan?HumanDateShort($r->tanggal_permintaan):'').' | '.$r->dokter_peminta.'</strong></div>
							</td>
						</tr>
					</tbody>
				</table>';
		$result[] = $btn_2;


		$btn_3='';
		$btn_3 .='<table class="block-table text-left">
					<tbody>
						<tr>
							
							<td class="bg-white" style="width: 100%;">
								<div class="h5 text-primary"> REGISTRATION INFO </div>
								<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
								<div class="text-muted text-uppercase">'.GetAsalRujukanR($r->idtipe).' - '.$r->nama_poli.'</div>
								<div class="text-muted"><i class="fa fa-user-md"></i> '.($r->nama_dokter?$r->nama_dokter:'<span class="text-danger"><strong>[BELUM DITENTUKAN]</strong></span>').$btn_petugas.'</div>
								<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->nama_rekanan?$r->nama_rekanan:$r->nama_kelompok).'</div>
							</td>
						</tr>
					</tbody>
				</table>';
		$result[] = $btn_3;
		$btn_4='';
		$btn_4 .='<table class="block-table text-center">
					<tbody>
						<tr>
							
							<td class="bg-white" style="width: 100%;">
								'.$this->status_tindakan_fisio(HumanDateShort($r->tanggal).' | '.$r->jam,$r->st_tindak_fisio,$r->status_reservasi,$r->st_panggil).'
							</td>
						</tr>
					</tbody>
				</table>';
		$result[] =$btn_4;


		$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
	}
	function simpan_petugas(){
		$id=$this->input->post('id');
		$iddokter=$this->input->post('iddokter');
		$jenis_update=$this->input->post('jenis_update');
		
		$this->iddokter=$iddokter;
		
		if ($jenis_update=='poli'){
			// print_r($id);exit;
			$this->db->where('id',$id);
			$hasil=$this->db->update('tpoliklinik_pendaftaran',array('iddokter'=>$iddokter));
		}else{
			$this->db->where('id',$id);
			$hasil=$this->db->update('app_reservasi_tanggal_pasien',array('iddokter'=>$iddokter));

			$this->db->where('reservasi_id',$id);
			$hasil=$this->db->update('tpoliklinik_pendaftaran',array('iddokter'=>$iddokter));
		}
		// print_r($iddokter.' '.$id);exit;
		

		$this->output->set_output(json_encode($hasil));
	}
	function status_tindakan_fisio($tanggal,$st_tindak_fisio,$status_reservasi,$st_panggil){
		
		$hasil='';
		
		$hasil .='<div class="h5 push-10-t"><strong>'.($st_tindak_fisio=='0'?text_danger('BELUM DITINDAK'):text_primary('TELAH DITINDAK')).'</strong></div>';
		if ($status_reservasi<3){
		$hasil .='<div class="h5 push-10-t"><strong>'.status_reservasi($status_reservasi).'</strong></div>';
			
		}else{
		$hasil .='<div class="h5 push-10-t"><strong>'.StatusPanggilFarmasi($st_panggil).'</strong></div>';
			
		}
		$hasil .='<div class="h5 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i> '.($tanggal).'</strong></div>';
		
		return $hasil;
	}
	
	function tindakan($pendaftaran_id='',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		get_ppa_login();
		$data_login_ppa=get_ppa_login();
		// print_r($trx_id);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// $data_satuan_ttv=array();
		// $data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan,makral_satuan,mcahaya_satuan,mgcs_eye_satuan,mgcs_motion_satuan,mgcs_voice_satuan,mpupil_satuan,mspo2_satuan,mgds_satuan")->row_array();
		
		$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id);
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tpoliklinik_rm_order/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		$data['error'] 			= '';
		$data['status_assemen'] 			= '0';
		$data['assesmen_id'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'Konsultasi';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
							  array("RSKB Halmahera",'#'),
							  array("Konsultasi ",'#'),
							  array("Dokter",'tpoliklinik_rm_order')
							);
		$data_assemen=array('idpoli'=>'','iddokter_peminta'=>'');
		// print_r($data_assemen);exit;
		//Input Baru
		if ($menu_kiri=='input_order'){
			// print_r($data['idtarif_header']);exit;
			$data_label=$this->Tpoliklinik_rm_order_model->setting_label_rm();
			if ($trx_id=='#'){
				$data_assemen=$this->Tpoliklinik_rm_order_model->get_data_assesmen($pendaftaran_id);
				
			}else{
			// print_r($menu_kiri);exit;
				$data_assemen=$this->Tpoliklinik_rm_order_model->get_data_assesmen_trx($trx_id);
			}
			if ($data_assemen){
			}else{
				// print_r($data);exit;
				$data['idtarif_header']=$this->Tpoliklinik_rm_order_model->list_header_path($data['idtipe_poli'],$data['idkelompokpasien'],$data['idrekanan']);
				$data_assemen=array('iddokter_peminta'=>'');
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
			}
			$data = array_merge($data, $data_label,$data_assemen);
		}
		//PERENCANAAN
		if ($menu_kiri=='input_perencanaan'){
			$data_label=$this->Tpoliklinik_rm_order_model->setting_label_rm_perencanaan();
			if ($trx_id=='#'){
				$data_assemen=$this->Tpoliklinik_rm_order_model->get_data_assesmen_per($pendaftaran_id);
				
			}else{
			// print_r($menu_kiri);exit;
				$data_assemen=$this->Tpoliklinik_rm_order_model->get_data_assesmen_trx_per($trx_id);
			}
			if ($data_assemen){
			}else{
				$data_assemen=array('iddokter_peminta'=>'');
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
			}
				
			$data = array_merge($data, $data_label,$data_assemen);
		}
		//PILLIH PROGRAM
		if ($menu_kiri=='pilih_program'){
			// print_r($data);exit;
			$data_label=$this->Tpoliklinik_rm_order_model->setting_label_rm_perencanaan();
			if ($trx_id=='#'){
				$data_assemen=$this->Tpoliklinik_rm_order_model->get_data_assesmen_per($pendaftaran_id);
				
			}else{
			// print_r($menu_kiri);exit;
				$data_assemen=$this->Tpoliklinik_rm_order_model->get_data_assesmen_trx_per($trx_id);
			}
			if ($data_assemen){
			}else{
				$data_assemen=array('iddokter_peminta'=>'');
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
			}
				
			$data = array_merge($data);
		}
		
		
		$data['trx_id']=$trx_id;
		$data['pendaftaran_id']=$pendaftaran_id;
		$data['list_ppa_dokter']=$this->Tpoliklinik_rm_order_model->list_ppa_dokter();
		$data['list_tujuan']=$this->Tpoliklinik_rm_order_model->list_tujuan();
		$data['list_dokter']=$this->Tpoliklinik_rm_order_model->list_dokter_all();
		$data['list_dokter_all']=$this->Tpoliklinik_rm_order_model->list_dokter_all();
		// print_r($data['list_dokter_all']);exit;
		$data['list_poli']=$this->Tpoliklinik_rm_order_model->list_poliklinik();
		$data = array_merge($data, backend_info());
		// $data = array_merge($data,$data_login_ppa,$data_satuan_ttv,$data_header_ttv,$data_assemen, backend_info());
		$this->parser->parse('module_template', $data);
		
	}
	function refres_div_counter(){
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');	
		$user_id=$this->session->userdata('user_id');	
		$idpoli =$this->input->post('idpoli');
		$iddokter =$this->input->post('iddokter');
		$show_all =$this->input->post('show_all');
		$st_login =1;
		$tanggal =YMDFormat($this->input->post('tanggal'));
		$where='';
		$where_awal='';
		if ($show_all=='0'){//Semua Dokter
			$where .="AND H.iddokter='$login_pegawai_id'";
			$where_awal .="AND M.iddokter='$login_pegawai_id'";
		}		
		if ($idpoli!='#'){
			$where .=" AND H.idpoliklinik='$idpoli'";
			$where_awal .=" AND M.idpoli='$idpoli'";
		}else{
			$where .=" AND H.idpoliklinik IN (
			SELECT MP.id FROM `app_reservasi_poli` H
			INNER JOIN app_reservasi_poli_user U ON U.idpoli=H.idpoli AND U.iduser='$user_id'
			INNER JOIN mpoliklinik MP ON MP.id=H.idpoli) ";
			
			$where_awal .=" AND M.id IN (
			SELECT H.tujuan_antrian_id FROM `app_reservasi_poli` H
			INNER JOIN app_reservasi_poli_user U ON U.idpoli=H.idpoli AND U.iduser='$user_id'
			INNER JOIN mpoliklinik MP ON MP.id=H.idpoli) ";
		}
		
		$id_next='0';
		$id_layani='0';
		$div_tabel='';
		$q="SELECT H.id,H.namapasien,H.no_medrec,H.kode_antrian,H.noantrian FROM `mtujuan` M
			INNER JOIN tpoliklinik_pendaftaran H ON H.id=M.pendaftaran_dilayani
			WHERE M.antrian_tanggal='$tanggal' ".$where;
		// print_r($q);exit;
		$data_next=$this->db->query($q)->row_array();
		if ($data_next){
			$data_next['button_disabel']='';
			$id_layani=$data_next['id'];
		}else{
			$data_next=array(
				'noantrian'=>'',
				'id'=>'',
				'kode_antrian'=>'-',
				'no_medrec'=>'-',
				'namapasien'=>'-',
				'button_disabel'=>'disabled',
			);
		}
		if ($st_login=='0'){
			$data_next=array(
				'noantrian'=>'',
				'id'=>'',
				'kode_antrian'=>'-',
				'no_medrec'=>'-',
				'namapasien'=>'-',
				'button_disabel'=>'disabled',
			);
		}
		$div_tabel.='
			<div class="col-sm-3">
				<div class="block block-themed">
					<div class="block-header bg-danger" style="border-bottom: 2px solid white;padding:10">
						<h3 class="block-title text-center" >ANTRIAN DILAYANI</h3>
					</div>
					<div class="content-mini content-mini-full bg-danger">
						<div>
							<div class="h2 text-center text-white">'.$data_next['kode_antrian'].'</div>
							<div class="h5 text-center text-white">'.$data_next['no_medrec'].'</div>
							<div class="h5 text-center text-white push-10">'.$data_next['namapasien'].'</div>
							<div class="text-center ">
							</div>
						</div>
						<div class="text-center ">
								<button class="btn btn-block btn-success" '.$data_next['button_disabel'].' type="button" onclick="recall_manual('.$data_next['id'].')" title="Panggil Ulang"><i class="si si-refresh pull-left"></i> PANGGIL ULANG</button>
						</div>
					</div>
				</div>
			</div>
		';
		$q="SELECT H.id,H.namapasien,H.no_medrec,H.kode_antrian,H.noantrian,A.tujuan_antrian_id 
			FROM tpoliklinik_pendaftaran H
			LEFT JOIN merm_jam MJ2 ON MJ2.jam_id=H.jadwal_id
			LEFT JOIN app_reservasi_poli A ON A.idpoli=H.idpoliklinik
			WHERE H.tanggal='$tanggal' 
			".$where."
			AND H.st_panggil='0'

			ORDER BY MJ2.jam,H.noantrian ASC
			LIMIT 1
			";
		$data_next=$this->db->query($q)->row_array();
		if ($data_next){
			$data_next['button_disabel']='';
			$id_next=$data_next['id'];
			$tujuan_antrian_id=$data_next['tujuan_antrian_id'];
		}else{
			$tujuan_antrian_id='';
			$data_next=array(
				'noantrian'=>'',
				'id'=>'',
				'kode_antrian'=>'-',
				'no_medrec'=>'-',
				'namapasien'=>'-',
				'button_disabel'=>'disabled',
				'tujuan_antrian_id'=>'',
			);
		}
		
		$div_tabel.='<div class="col-sm-3">
							<div class="block block-themed">
								<div class="block-header bg-success" style="border-bottom: 2px solid white;padding:10">
									<h3 class="block-title text-center" >ANTRIAN SELANJUTNYA</h3>
								</div>
								<div class="content-mini content-mini-full bg-success">
									<div>
										<div class="h2 text-center text-white">'.$data_next['kode_antrian'].'</div>
										<div class="h5 text-center text-white">'.$data_next['no_medrec'].'</div>
										<div class="h5 text-center text-white push-10">'.$data_next['namapasien'].'</div>
									</div>
									<div class="text-center ">
										<button class="btn btn-block btn-primary " onclick="call_antrian_manual('.$data_next['id'].','.$data_next['tujuan_antrian_id'].')" '.$data_next['button_disabel'].' title="Lanjut Panggil" type="button"><i class="glyphicon glyphicon-volume-up pull-left"></i> '.($id_layani=='0'?'PANGGIL':'SELANJUTNYA').'</button>
									</div>
								</div>
								
							</div>
							
						</div>';
		$q="SELECT SUM(1) as total_antrian 
			,SUM(CASE WHEN H.st_panggil='0' THEN 1 ELSE 0 END) as sisa_antrian
			FROM tpoliklinik_pendaftaran H

			WHERE H.tanggal='$tanggal' 
			".$where."

			

			";
		$data_next=$this->db->query($q)->row_array();
		if ($id_layani=='0'){
			$button_disabel='disabled';
		}else{
			$button_disabel='';
			
		}
		if ($data_next['sisa_antrian']=='0'){
			$button_disabel='disabled';
			
		}
		
		$div_tabel.='
			<div class="col-sm-3">
				<div class="block block-themed">
					<div class="block-header bg-success" style="border-bottom: 2px solid white;padding:10">
						<h3 class="block-title text-center" >SISA ANTRIAN</h3>
					</div>
					<div class="content-mini content-mini-full bg-success">
						<div>
							<div class="h2 text-center text-white">'.$data_next['sisa_antrian'].'</div>
							<div class="h5 text-center text-white">&nbsp;</div>
							<div class="h5 text-center text-white push-10">&nbsp;</div>
						</div>
						<div class="text-center ">
							<button class="btn btn-block btn-warning" type="button" '.$button_disabel.' onclick="call_lewati('.$id_layani.','.$id_next.','.$tujuan_antrian_id.')"  title="LEWATI"><i class="fa fa-mail-forward pull-left"></i> LEWATI</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-sm-3">
				<div class="block block-themed">
					<div class="block-header bg-success" style="border-bottom: 2px solid white;padding:10">
						<h3 class="block-title text-center" >TOTAL ANTRIAN</h3>
					</div>
					<div class="content-mini content-mini-full bg-success">
						<div>
							<div class="h1 font-w700 text-center text-white">'.$data_next['total_antrian'].'</div>
						</div>
					</div>
				</div>
			</div>
		';
		$this->output->set_output(json_encode($div_tabel));
	}
	function call_antrian(){
		$antrian_id=$this->input->post('antrian_id');
		
		$data=$this->db->query("SELECT H.id,H.noantrian,H.kode_antrian,H.idpoliklinik,H.iddokter,H.tanggal FROM tpoliklinik_pendaftaran H WHERE H.id='$antrian_id'")->row();
		$idpoli=$data->idpoliklinik;
		$iddokter=$data->iddokter;
		$noantrian=$data->noantrian;
		$tanggal=$data->tanggal;
		$ruangan_id=$this->input->post('ruangan_id');
		// $noantrian=$this->input->post('noantrian');
		// $tanggal='$tanggal';
		$q2="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM mtujuan_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.tujuan_id='$ruangan_id' AND H.`status`='1'
			ORDER BY H.nourut ASC";
		$file_3=$this->db->query($q2)->row('file');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		
		if ($data){
			$kode_number=str_pad($noantrian,3,"0",STR_PAD_LEFT);
			// $this->db->where('id',$data->id);
			// $this->db->update('antrian_harian',array('st_panggil'=>1));
			$sound_play='';
			// $pelayanan_id=$data->antrian_id;
			
			
			$q1="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_poli_kode_dokter H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_dokter_id
			WHERE H.idpoli='$idpoli' AND H.iddokter='$iddokter'";
			$file_dokter=$this->db->query($q1)->row('file');
			
			$q1="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_poli_kode_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.idpoli='$idpoli' AND H.`status`='1'
			ORDER BY H.nourut ASC";
			$file_1=$this->db->query($q1)->row('file');
			if ($file_dokter){
				$file_1=$file_1.':'.$file_dokter;
			}
			
			$file_2=terbilang_sound($kode_number);
			$sound_play=$file_1.':'.$file_2.':'.$file_3;
			$data_insert=array(
				'pendaftaran_id' => $data->id,
				'idpoli' => $idpoli,
				'iddokter' => $iddokter,
				'ruangan_id' => $ruangan_id,
				'kode_antrian' => $data->kode_antrian,
				'noantrian' => $data->noantrian,
				// 'kode_number' => $data->kode_number,
				'sound_play' => $sound_play,
				// 'tanggal' => $data->tanggal,
				'tanggal' => date('Y-m-d'),
				'waktu_panggil' => date('Y-m-d H:i:s'),
				'user_call' => $user_id,
				'user_call_nama' => $user_nama_login,
			);
			$this->db->insert('antrian_harian_poli_call',$data_insert);
			$hasil=$data_insert;
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
		
	}
}	
