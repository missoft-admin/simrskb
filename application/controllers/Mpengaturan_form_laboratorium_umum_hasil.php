<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpengaturan_form_laboratorium_umum_hasil extends CI_Controller {

	/**
	 * Pengaturan Form Input Hasil Laboratorium Umum controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpengaturan_form_laboratorium_umum_hasil_model');
  }

	function index()
	{
		$row = $this->Mpengaturan_form_laboratorium_umum_hasil_model->getSpecified(1);
		if(isset($row->id)){
			$data = array(
				'id' => $row->id,
				'edit_nomor_laboratorium' => $row->edit_nomor_laboratorium,
				'edit_prioritas_pemeriksaan' => $row->edit_prioritas_pemeriksaan,
				'edit_waktu_pengambilan' => $row->edit_waktu_pengambilan,
				'edit_petugas_pengambilan' => $row->edit_petugas_pengambilan,
				'edit_nilai_kritis' => $row->edit_nilai_kritis,
				'action_tidak_normal' => $row->action_tidak_normal,
				'action_flag' => $row->action_flag,
				'warna_flag' => $row->warna_flag,
				'edit_pasien_puasa' => $row->edit_pasien_puasa,
				'edit_pengiriman_hasil' => $row->edit_pengiriman_hasil,
				'edit_satuan' => $row->edit_satuan,
				'edit_nilai_normal_rujukan' => $row->edit_nilai_normal_rujukan,
				'action_nilai_kritis' => $row->action_nilai_kritis,
				'action_periksa_keluar' => $row->action_periksa_keluar,
				'edit_metode' => $row->edit_metode,
				'edit_sumber_spesimen' => $row->edit_sumber_spesimen,
				'warna_tidak_normal' => $row->warna_tidak_normal,
				'warna_nilai_kritis' => $row->warna_nilai_kritis,
			);

			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Pengaturan Form Input Hasil Laboratorium Umum';
			$data['content']	 	= 'Mpengaturan_form_laboratorium_umum_hasil/index';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Pengaturan Form Input Hasil Laboratorium",'#'),
															array("Ubah",'mpengaturan_form_laboratorium_umum_hasil')
														);

														
			$data['jenis_petugas_proses'] = $this->Mpengaturan_form_laboratorium_umum_hasil_model->getJenisPetugasProses();

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpengaturan_form_laboratorium_umum_hasil/index','location');
		}
	}

	function save()
	{
		if($this->Mpengaturan_form_laboratorium_umum_hasil_model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mpengaturan_form_laboratorium_umum_hasil/index','location');
		}
	}
}
