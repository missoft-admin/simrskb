<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mgroup_pembayaran extends CI_Controller {

	/**
	 * Group Pembayaran controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mgroup_pembayaran_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Group Pembayaran';
		$data['content'] 		= 'Mgroup_pembayaran/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Group Pembayaran",'mgroup_pembayaran/index'),
														array("List",'#')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 							=> '',
			'nama' 						=> '',
			'status' 					=> '',
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Group Pembayaran';
		$data['content'] 		= 'Mgroup_pembayaran/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Group Pembayaran",'#'),
									    			array("Tambah",'mgroup_pembayaran')
													);

		$data['list_akun'] 	= array();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mgroup_pembayaran_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 							 => $row->id,
					'nama' 						 => $row->nama,
					'status' 					 => $row->status,
				);

				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Group Pembayaran';
				$data['content']	 	= 'Mgroup_pembayaran/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Group Pembayaran",'#'),
											    			array("Ubah",'mgroup_pembayaran')
															);

				$data['list_akun'] 	= $this->Mgroup_pembayaran_model->getAkunData($id);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mgroup_pembayaran','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mgroup_pembayaran','location');
		}
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mgroup_pembayaran_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mgroup_pembayaran/index/'.$this->input->post('idtipe'),'location');
				}
			} else {
				if($this->Mgroup_pembayaran_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mgroup_pembayaran/index/'.$this->input->post('idtipe'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 			 = validation_errors();
		$data['content'] 		 = 'Mgroup_pembayaran/manage';

		if($id==''){
			$data['title'] = 'Tambah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Tambah",'mgroup_pembayaran')
													);
		}else{
			$data['title'] = 'Ubah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Ubah",'mgroup_pembayaran')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function delete($id){
		$this->Mgroup_pembayaran_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mgroup_pembayaran','location');
	}

	function getIndex()
	{
			$this->select = array('mgroup_pembayaran.*', 'GROUP_CONCAT(makun_nomor.noakun," - ",makun_nomor.namaakun) AS noakun');
			$this->from   = 'mgroup_pembayaran';
			$this->join 	= array(
				array("mgroup_pembayaran_detail", "mgroup_pembayaran_detail.idgroup = mgroup_pembayaran.id", "LEFT"),
				array("makun_nomor", "makun_nomor.id = mgroup_pembayaran_detail.idakun", "LEFT")
			);
			$this->where  = array(
				'mgroup_pembayaran.status' => '1'
			);
			$this->order  = array(
				'mgroup_pembayaran.id' => 'ASC'
			);
			$this->group  = array(
				'mgroup_pembayaran.id'
			);

			$this->column_search   = array('mgroup_pembayaran.nama');
			$this->column_order    = array('mgroup_pembayaran.nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$aksi = '<div class="btn-group">
						<a href="'.site_url().'mgroup_pembayaran/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
						<a href="#" data-urlindex="'.site_url().'mgroup_pembayaran" data-urlremove="'.site_url().'mgroup_pembayaran/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>
					</div>';

					$row[] = $no;
					$row[] = $r->nama;
					$row[] = $r->noakun;
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
}
