<?php
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
class Tneraca_lajur extends CI_Controller
{
	/**
	 * No. Akuntansi controller.
	 * Developer @gunalirezqimauludi
	 */

	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tneraca_lajur_model', 'model');
		$this->load->model('Mkategori_akun_model');
	}
	function index($idakun='',$disabel=''){
		$row=$this->model->getSpecified($idakun);
		$awal='';
		if ($row){
			$data=array(
				'idakun' =>$row->id,
				'noakun' =>$row->noakun,
				'namaakun' =>$row->namaakun,
				'tanggal_trx1' =>$awal,
				'tanggal_trx2' =>date('d-m-Y'),
				'notransaksi' =>'',
				'nojurnal' =>'',
				'posisi' =>'',
				'saldoawal' =>'',
			);
			
		}else{
			$data=array(
				'idakun' =>'',
				'noakun' =>'',
				'namaakun' =>'',
				'tanggal_trx1' =>$awal,
				'tanggal_trx2' =>date('d-m-Y'),
				'notransaksi' =>'',
				'nojurnal' =>'',
				'posisi' =>'',
				'saldoawal' =>'',
			);
		}
		// print_r($data);exit();
		$data['list_akun'] = $this->Mkategori_akun_model->list_akun();
		$data['list_ref'] = $this->model->list_ref();
		$data['disabel'] = $disabel;
		$data['idakun_arr'] = '';
		$data['error'] = '';
		$data['title'] = 'Neraca Lajur';
		$data['content'] = 'Tneraca_lajur/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', 'dashboard'],
			['Akuntansi', '#'],
			['Neraca Lajur', 'Tneraca_lajur'],
			['List', '']
		];

		$data = array_merge($data, backend_info());
		// print_r($data['idperiode_akuntansi']);exit();
		$this->parser->parse('module_template', $data);
	}
	
	function load_neraca(){
		$where='';
		
		$idkategori     		= $this->input->post('idkategori');		
		$possaldo     		= $this->input->post('possaldo');		
		$poslaporan     		= $this->input->post('poslaporan');		
		$header_akun     		= $this->input->post('header_akun');		
		$tanggal_trx1     		= $this->input->post('tanggal_trx1');		
		$tanggal_trx2     		= $this->input->post('tanggal_trx2');	
		$idakun_arr     		= $this->input->post('idakun_arr');	
		if ($idkategori){
			$idkategori=implode(',',$idkategori);
			$where .=" AND H.idkategori IN (".$idkategori.")";
		}
		if ($possaldo){
			$possaldo=implode(',',$possaldo);
			$where .=" AND H.possaldo IN (".$possaldo.")";
		}
		if ($poslaporan){
			$poslaporan=implode(',',$poslaporan);
			$where .=" AND H.poslaporan IN (".$poslaporan.")";
		}
		if ($header_akun){
			$header_akun=implode(',',$header_akun);
			$where .=" AND H.noheader IN (".$header_akun.")";
		}
		if ($idakun_arr){
			$idakun_arr=implode(',',$idakun_arr);
			$where .=" AND H.id IN (".$idakun_arr.")";
		}
		$q = "
				SELECT H.id,H.noheader,H.idkategori,H.kategori,H.noakun,H.namaakun,H.possaldo,H.poslaporan,H.sa_debet,H.sa_kredit 
				,IF(H.poslaporan='NRC',IF(H.possaldo='DB',H.sa_debet + H.debet-H.kredit,0),0) nr_debet
				,IF(H.poslaporan='NRC',IF(H.possaldo='CR',H.sa_kredit + H.kredit-H.debet,0),0) nr_kredit
				,IF(H.poslaporan='LR',IF(H.possaldo='DB',H.sa_debet + H.debet-H.kredit,0),0) lr_debet
				,IF(H.poslaporan='LR',IF(H.possaldo='CR',H.sa_kredit + H.kredit-H.debet,0),0) lr_kredit
				,IF (H.possaldo='DB',(H.sa_debet + H.debet) - (H.sa_kredit + H.kredit),0) as ns_debet
				,IF (H.possaldo='CR',(H.sa_kredit + H.kredit)-(H.sa_debet + H.debet),0) as ns_kredit
				FROM (
					SELECT H.id,H.noheader,H.noakun,H.namaakun,H.possaldo,H.poslaporan,H.idkategori,K.nama as kategori
					,CASE WHEN H.possaldo='DB' THEN get_saldo_awal_akun(H.id,'".YMDFormat($tanggal_trx1)."',1) ELSE 0 END as sa_debet 
					,CASE WHEN H.possaldo='CR' THEN get_saldo_awal_akun(H.id,'".YMDFormat($tanggal_trx1)."',1) ELSE 0 END as sa_kredit
					,IFNULL(SUM(J.debet),0) as debet,IFNULL(SUM(J.kredit),0) as kredit
					from makun_nomor H
					LEFT JOIN mkategori_akun K ON K.id=H.idkategori
					LEFT JOIN jurnal_umum J ON J.idakun=H.id AND DATE(J.tanggal)>='".YMDFormat($tanggal_trx1)."' AND DATE(J.tanggal)<='".YMDFormat($tanggal_trx2)."' AND J.status='1'
					
					GROUP BY H.id
					ORDER BY H.noakun
				) H WHERE H.id IS NOT NULL ".$where."
			";
			// print_r($q);exit();
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$ns_tot_debet=0;$ns_tot_kredit=0;
		$lr_tot_debet=0;$lr_tot_kredit=0;
		$nr_tot_debet=0;$nr_tot_kredit=0;
		$sa_tot_debet=0;$sa_tot_kredit=0;
		// $total_K=0;
		foreach($rows as $r){
			$no++;
			$ns_tot_debet=$ns_tot_debet + $r->ns_debet;
			$ns_tot_kredit=$ns_tot_kredit + $r->ns_kredit;
			$lr_tot_debet=$lr_tot_debet + $r->lr_debet;
			$lr_tot_kredit=$lr_tot_kredit + $r->lr_kredit;
			
			$nr_tot_debet=$nr_tot_debet + $r->nr_debet;
			$nr_tot_kredit=$nr_tot_kredit + $r->nr_kredit;
			$sa_tot_debet=$sa_tot_debet + $r->sa_debet;
			$sa_tot_kredit=$sa_tot_kredit + $r->sa_kredit;
			
			
			$tbl .='<tr>';
			$tbl .='<td>'.$r->noakun.'</td>';
			$tbl .='<td ><a href="'.site_url().'tbuku_besar/index/'.$r->id.'/'.DMYFormat2(YMDFormat($tanggal_trx1)).'/'.DMYFormat2(YMDFormat($tanggal_trx2)).'" target="_blank">'.$r->namaakun.' ('.$r->id.')</a></td>';
			$tbl .='<td >'.$r->possaldo.'</td>';
			$tbl .='<td>'.number_format($r->ns_debet,2, ".", ",").'</td>';
			$tbl .='<td>'.number_format($r->ns_kredit,2, ".", ",").'</td>';
			$tbl .='<td >'.$r->poslaporan.'</td>';
			$tbl .='<td>'.number_format($r->lr_debet,2, ".", ",").'</td>';
			$tbl .='<td>'.number_format($r->lr_kredit,2, ".", ",").'</td>';
			$tbl .='<td>'.number_format($r->nr_debet,2, ".", ",").'</td>';
			$tbl .='<td>'.number_format($r->nr_kredit,2, ".", ",").'</td>';
			$tbl .='<td>'.number_format($r->sa_debet,2, ".", ",").'</td>';
			$tbl .='<td>'.number_format($r->sa_kredit,2, ".", ",").'</td>';
			$tbl .='</tr>';
		}
		
		$arr['tabel']=$tbl;
		$arr['ns_tot_debet']=number_format($ns_tot_debet,2, ".", ",");
		$arr['ns_tot_kredit']=number_format($ns_tot_kredit,2, ".", ",");
		$arr['lr_tot_debet']=number_format($lr_tot_debet,2, ".", ",");
		$arr['lr_tot_kredit']=number_format($lr_tot_kredit,2, ".", ",");
		
		$arr['nr_tot_debet']=number_format($nr_tot_debet,2, ".", ",");
		$arr['nr_tot_kredit']=number_format($nr_tot_kredit,2, ".", ",");
		
		$arr['sa_tot_debet']=number_format($sa_tot_debet,2, ".", ",");
		$arr['sa_tot_kredit']=number_format($sa_tot_kredit,2, ".", ",");
		
		$arr['bersih_lr']=number_format($lr_tot_kredit-$lr_tot_debet,2, ".", ",");
		$arr['bersih_nr']=number_format($nr_tot_debet-$nr_tot_kredit,2, ".", ",");
		
		$arr['bersih_nr_debet']=number_format($nr_tot_debet,2, ".", ",");
		$arr['bersih_nr_kredit']=number_format($nr_tot_kredit + $nr_tot_debet-$nr_tot_kredit,2, ".", ",");
		
		$arr['bersih_lr_debet']=number_format($lr_tot_debet + ($lr_tot_kredit-$lr_tot_debet),2, ".", ",");
		$arr['bersih_lr_kredit']=number_format($lr_tot_kredit,2, ".", ",");
		
		$this->output->set_output(json_encode($arr));
    
	}
	
	public function export()
    {
		// print_r($this->input->post());exit();
        ini_set('memory_limit', '-1');
        set_time_limit(1000);
		$data=array(
			'tanggal_1' =>HumanDateShort($this->input->post('tanggal_trx1')),
			'tanggal_2' =>HumanDateShort($this->input->post('tanggal_trx2')),
			'kategori' =>'Semua Kategori',
			'possaldo' =>'-',
			'poslaporan' =>'-',
			'header_akun' =>'-',
			'kategori' =>'-',
			'akun' =>'-',
			'judul' =>'NERACA LAJUR',
		);
		$where='';	
		$tanggal_trx1     		= $this->input->post('tanggal_trx1');
		$tanggal_trx2     		= $this->input->post('tanggal_trx2');
		$idkategori     		= $this->input->post('idkategori_arr');
		$possaldo     		= $this->input->post('possaldo');
		$poslaporan     		= $this->input->post('poslaporan');
		$header_akun     	= $this->input->post('header_akun');
		$idakun_arr     	= $this->input->post('idakun_arr');
		// print_r($idkategori);exit();
		if ($idkategori){
			$idkategori=implode(',',$idkategori);
			$data['kategori']=$this->db->query("SELECT GROUP_CONCAT(H.nama) as kategori FROM mkategori_akun H WHERE H.id IN (".$idkategori.")")->row('kategori');;
			$where .=" AND H.idkategori IN (".$idkategori.")";
		}
		if ($possaldo){
			$possaldo=implode(',',$possaldo);
			$where .=" AND H.possaldo IN (".$possaldo.")";
			$data['possaldo']=$possaldo;
		}
		if ($poslaporan){
			$poslaporan=implode(',',$poslaporan);
			$where .=" AND H.poslaporan IN (".$poslaporan.")";
			$data['poslaporan']=$poslaporan;
		}
		if ($header_akun){
			$header_akun=implode(',',$header_akun);
			$where .=" AND H.noheader IN (".$header_akun.")";
			$data['header_akun']=$this->db->query("SELECT GROUP_CONCAT(H.namaakun) as hasil FROM makun_nomor H WHERE H.noakun IN (".$header_akun.")")->row('hasil');;
		}
		if ($idakun_arr){
			$idakun_arr=implode(',',$idakun_arr);
			$where .=" AND H.id IN (".$idakun_arr.")";
			$data['akun']=$this->db->query("SELECT GROUP_CONCAT(H.namaakun) as hasil FROM makun_nomor H WHERE H.id IN (".$idakun_arr.")")->row('hasil');;
		}
		// print_r($data);exit();
        $from = "
					SELECT H.id,H.noheader,H.idkategori,H.kategori,H.noakun,H.namaakun,H.possaldo,H.poslaporan,H.sa_debet,H.sa_kredit 
				,IF(H.poslaporan='NRC',IF(H.possaldo='DB',H.sa_debet + H.debet-H.kredit,0),0) nr_debet
				,IF(H.poslaporan='NRC',IF(H.possaldo='CR',H.sa_kredit + H.kredit-H.debet,0),0) nr_kredit
				,IF(H.poslaporan='LR',IF(H.possaldo='DB',H.sa_debet + H.debet-H.kredit,0),0) lr_debet
				,IF(H.poslaporan='LR',IF(H.possaldo='CR',H.sa_kredit + H.kredit-H.debet,0),0) lr_kredit
				,IF (H.possaldo='DB',(H.sa_debet + H.debet) - (H.sa_kredit + H.kredit),0) as ns_debet
				,IF (H.possaldo='CR',(H.sa_kredit + H.kredit)-(H.sa_debet + H.debet),0) as ns_kredit
				FROM (
					SELECT H.id,H.noheader,H.noakun,H.namaakun,H.possaldo,H.poslaporan,H.idkategori,K.nama as kategori
					,CASE WHEN H.possaldo='DB' THEN get_saldo_awal_akun(H.id,'".YMDFormat($tanggal_trx1)."',1) ELSE 0 END as sa_debet 
					,CASE WHEN H.possaldo='CR' THEN get_saldo_awal_akun(H.id,'".YMDFormat($tanggal_trx1)."',1) ELSE 0 END as sa_kredit
					,IFNULL(SUM(J.debet),0) as debet,IFNULL(SUM(J.kredit),0) as kredit
					from makun_nomor H
					LEFT JOIN mkategori_akun K ON K.id=H.idkategori
					LEFT JOIN jurnal_umum J ON J.idakun=H.id AND DATE(J.tanggal)>='".YMDFormat($tanggal_trx1)."' AND DATE(J.tanggal)<='".YMDFormat($tanggal_trx2)."' AND J.status='1'
					
					GROUP BY H.id
					ORDER BY H.noakun
				) H WHERE H.id IS NOT NULL ".$where."
				";
        $btn    = $this->input->post('btn_export');
        $row_detail=$this->db->query($from)->result();
        
		if ($btn=='1'){
			$this->excel($row_detail,$data);
		}
		if ($btn=='2'){
			// print_r($data);exit();
			$this->pdf($row_detail,$data);
		}
		if ($btn=='3'){
			$this->pdf($row_detail,$data);
		}
    }
	public function pdf($row_detail,$row){
		// print_r($row_detail);exit();
		$dompdf = new Dompdf();
		$data=array();
		$data=array_merge($data,$row);
        
        $data['detail'] = $row_detail;
		// print_r($data);exit();
		$data = array_merge($data, backend_info());

        $html = $this->load->view('Tneraca_lajur/pdf', $data, true);
		// print_r($html	);exit();
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('No. Akuntansi.pdf', array("Attachment"=>0));
	}
	function excel($row_detail,$data){
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle($data['judul']);

        // Set Logo
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $logo = FCPATH.'/assets/upload/logo/logoreport.jpg';
        $objDrawing->setPath($logo);
        $objDrawing->setCoordinates('B1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setHeight(70);
        $objDrawing->setWidth(40);
        $objDrawing->setWorksheet($activeSheet);

        // Set Header
        $activeSheet->setCellValue('B1', "                                      RUMAH SAKIT KHUSUS BEDAH HALMAHERA SIAGA");
        $activeSheet->setCellValue('B2', "                                      Jl.LLRE.Martadinata no.28, Telp: +6222-4206061");
        $activeSheet->setCellValue('B3', "                                      Bandung, 40115");
        $activeSheet->mergeCells('B1:AA1');
        $activeSheet->mergeCells('B2:AA2');
        $activeSheet->mergeCells('B3:AA3');
        $activeSheet->getStyle('B1:AA1')->getFont()->setSize(11);
        $activeSheet->getStyle('B2:AA2')->getFont()->setSize(11);
        $activeSheet->getStyle('B3:AA3')->getFont()->setSize(11);
	
		
		$activeSheet->setCellValue('B5', "PERIODE ");
		$activeSheet->setCellValue('C5', ": ".$data['tanggal_1']." s/d ".$data['tanggal_2']);
        $activeSheet->setCellValue('B6', "KATEGORI ");
		$activeSheet->setCellValue('C6', ": ".$data['kategori']);
        $activeSheet->setCellValue('B7', "POS SALDO ");
		$activeSheet->setCellValue('C7', ": ".$data['possaldo']);
        $activeSheet->setCellValue('B8', "POS LAPORAN");
		$activeSheet->setCellValue('C8', ": ".$data['poslaporan']);
		$activeSheet->setCellValue('B9', "NO AKUN");
		$activeSheet->setCellValue('C9', ": ".$data['akun']);
		$activeSheet->setCellValue('B10', "HEADER AKUN");
		$activeSheet->setCellValue('C10', ": ".$data['header_akun']);
		
		// Set Title
		$activeSheet->setCellValue('B12', $data['judul']);
		$activeSheet->mergeCells('B12:O12');
		$activeSheet->getStyle('B12')->getFont()->setBold(true)->setSize(14);
		$activeSheet->getStyle('B12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->setCellValue('B14', "NO");
		$activeSheet->mergeCells('B14:B15');
		$activeSheet->setCellValue('C14', "KATEGORI");
		$activeSheet->mergeCells('C14:C15');
		$activeSheet->setCellValue('D14', "NO AKUN");
		$activeSheet->mergeCells('D14:D15');
		$activeSheet->setCellValue('E14', "NAMA AKUN");
		$activeSheet->mergeCells('E14:E15');
		$activeSheet->setCellValue('F14', "POS SALDO");
		$activeSheet->mergeCells('F14:F15');
		$activeSheet->setCellValue('G14', "NERACA SALDO");
		$activeSheet->setCellValue('H14', "NERACA SALDO");
		$activeSheet->mergeCells('G14:H14');
		$activeSheet->setCellValue('G15', "DEBIT");
		$activeSheet->setCellValue('H15', "KREDIT");
		$activeSheet->setCellValue('I14', "POS LAPORAN");
		$activeSheet->mergeCells('I14:I15');
		$activeSheet->setCellValue('J14', "LABA RUGI");
		$activeSheet->setCellValue('K14', "LABA RUGI");
		$activeSheet->mergeCells('J14:K14');
		$activeSheet->setCellValue('J15', "DEBIT`");
		$activeSheet->setCellValue('K15', "KREDIT");
		$activeSheet->setCellValue('L14', "NERACA");
		$activeSheet->setCellValue('M14', "NERACA");
		$activeSheet->mergeCells('L14:M14');
		$activeSheet->setCellValue('L15', "DEBIT");
		$activeSheet->setCellValue('M15', "KREDIT");
		$activeSheet->setCellValue('N14', "SALDO AWAL");
		$activeSheet->setCellValue('O14', "SALDO AWAL");
		$activeSheet->mergeCells('N14:O14');
		$activeSheet->setCellValue('N15', "DEBIT");
		$activeSheet->setCellValue('O15', "KREDIT");
		$activeSheet->getStyle('B14:O15')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("B14:O15")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
	  'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK,
					)
				),
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Calibri'
				)
			)
		);

		$x = 16;
		$debet = 0;
		$kredit= 0;
		$no=0;
        if (COUNT($row_detail)) {
            foreach ($row_detail as $row) {
				$no =$no+1;
				
			
	            $activeSheet->setCellValue("B$x", number_format($no));
	            $activeSheet->setCellValue("C$x", $row->kategori);
	            $activeSheet->setCellValue("D$x", $row->noakun);
	            $activeSheet->setCellValue("E$x", ($row->namaakun));
	            $activeSheet->setCellValue("F$x", $row->possaldo);
	            $activeSheet->setCellValue("G$x", ($row->ns_debet));
	            $activeSheet->setCellValue("H$x", ($row->ns_kredit));
	            $activeSheet->setCellValue("I$x", ($row->poslaporan));
	            $activeSheet->setCellValue("J$x", ($row->lr_debet));
	            $activeSheet->setCellValue("K$x", ($row->lr_kredit));
	            $activeSheet->setCellValue("L$x", ($row->nr_debet));
	            $activeSheet->setCellValue("M$x", ($row->nr_kredit));
	            $activeSheet->setCellValue("N$x", ($row->sa_debet));
	            $activeSheet->setCellValue("O$x", ($row->sa_kredit));				
				$x = $x+1;
	           
				
				
			}
		}
		// print_r($x);exit();
		$activeSheet->setCellValue("B".$x,"JUMLAH");
		$activeSheet->mergeCells("B$x:F$x");
		$activeSheet->setCellValue("G".$x,"=SUM(G16:G".($x-1).")");
		$activeSheet->setCellValue("H".$x,"=SUM(H16:H".($x-1).")");
		$activeSheet->setCellValue("J".$x,"=SUM(J16:J".($x-1).")");
		$activeSheet->setCellValue("K".$x,"=SUM(K16:K".($x-1).")");
		$activeSheet->setCellValue("L".$x,"=SUM(L16:L".($x-1).")");
		$activeSheet->setCellValue("M".$x,"=SUM(M16:M".($x-1).")");
		$activeSheet->setCellValue("N".$x,"=SUM(N16:N".($x-1).")");
		$activeSheet->setCellValue("O".$x,"=SUM(O16:O".($x-1).")");
		// $activeSheet->setCellValue($abjad.$x,"=SUM(".$abjad."7:".$abjad.($x-1).")");
		$x = $x+1;
		$activeSheet->setCellValue("B".$x,"JUMLAH (RUGI) BERSIH");
		$activeSheet->mergeCells("B$x:F$x");
		$activeSheet->setCellValue("J".$x,"=SUM(K16:K".($x-2).")-SUM(J16:J".($x-2).")");
		$activeSheet->setCellValue("M".$x,"=SUM(L16:L".($x-2).")-SUM(M16:M".($x-2).")");
		$x = $x+1;
		$activeSheet->setCellValue("B".$x,"JUMLAH SETELAH LABA RUGI");
		$activeSheet->mergeCells("B$x:F$x");
		$activeSheet->setCellValue("J".$x,"=SUM(J".($x-2).":J".($x-1).")");
		$activeSheet->setCellValue("K".$x,"=SUM(K".($x-2).":K".($x-1).")");
		$activeSheet->setCellValue("L".$x,"=SUM(L".($x-2).":L".($x-1).")");
		$activeSheet->setCellValue("M".$x,"=SUM(M".($x-2).":M".($x-1).")");
		$activeSheet->getStyle("F16:F$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("I16:I$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("G16:H$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		// $activeSheet->getStyle("G16:H$x")->getNumberFormat()->setFormatCode('#,##0');
		$activeSheet->getStyle("G16:H".$x)->getNumberFormat()->applyFromArray(
        array('code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1));
		$activeSheet->getStyle("J16:O$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$activeSheet->getStyle("J16:O$x")->getNumberFormat()->applyFromArray(
        array('code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1));
		// $activeSheet->getStyle("E11:E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$activeSheet->getStyle("B16:O$x")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
					)
				)
			)
		);
		
    	// Set Auto Width Column
    	$activeSheet->getColumnDimension("A")->setWidth(5);
    	$activeSheet->getColumnDimension("D")->setAutoSize(true);
    	$activeSheet->getColumnDimension("E")->setAutoSize(true);
    	$activeSheet->getColumnDimension("F")->setWidth(15);
    	$activeSheet->getColumnDimension("G")->setAutoSize(true);
    	$activeSheet->getColumnDimension("H")->setAutoSize(true);
    	$activeSheet->getColumnDimension("I")->setWidth(15);
    	$activeSheet->getColumnDimension("J")->setAutoSize(true);
    	$activeSheet->getColumnDimension("K")->setAutoSize(true);
    	$activeSheet->getColumnDimension("L")->setAutoSize(true);
    	$activeSheet->getColumnDimension("M")->setAutoSize(true);
    	$activeSheet->getColumnDimension("N")->setAutoSize(true);
    	$activeSheet->getColumnDimension("O")->setAutoSize(true);

    	header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    	header('Content-Disposition: attachment;filename="'.$data['judul'].' '.date('Y-m-d-h-i-s').'.xls"');
    	header('Cache-Control: max-age=0');

    	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    	$objWriter->save('php://output');
	}
	
}
?>