<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_cover_barang extends CI_Controller {

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Setting_cover_barang_model');
		$this->load->helper('path');
		
  }


	function index($tab='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('1811'))){
				$tab='1';
			}
		}
		if (UserAccesForm($user_acces_form,array('1811'))){
			$data['tab'] 			= $tab;
			
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Setting Coverage';
			$data['content'] 		= 'Setting_cover_barang/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Setting Coverage Setting",'setting_cover_barang/index')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	public function get_obat()
    {
        $cari 	= $this->input->post('search');
        $idtipe 	= $this->input->post('idtipe');
        $idkategori 	= $this->input->post('idkategori');
		$where='';
		if ($idkategori !='0'){
			$where .=" AND H.idkategori='$idkategori'";
		}
        // $idkategori 	= $this->input->post('idkategori');
        $q="SELECT H.* FROM (
				SELECT '1' as idtipe,id as idbarang,kode,idkategori,nama FROM mdata_alkes  WHERE status='1'
				UNION ALL
				SELECT '2' as idtipe,id as idbarang,kode,idkategori,nama FROM mdata_implan  WHERE status='1'
				UNION ALL
				SELECT '3' as idtipe,id as idbarang,kode,idkategori,nama FROM mdata_obat WHERE status='1'
				UNION ALL
				SELECT '4' as idtipe,id as idbarang,kode,idkategori,nama FROM mdata_logistik WHERE status='1'
				) H 
				
				WHERE H.idtipe='$idtipe' AND H.nama LIKE '%".$cari."%' ".$where."
				ORDER BY nama
				";
		$data_obat=$this->db->query($q)->result_array();
        $this->output->set_output(json_encode($data_obat));
    }
	function get_poli(){
		$idtipe=$this->input->post('idtipe');
		if ($idtipe>0){
			if ($idtipe<=2 && $idtipe>0){
				$idtipe=$idtipe;
			}else{
				$idtipe='3';
			}
		}
		$q="
			SELECT *FROM (
			SELECT H.idtipe,H.id,H.nama FROM mpoliklinik H WHERE H.`status`='1' AND H.nama IS NOT NULL

			UNION ALL

			SELECT 3  as idtipe,H.id,H.nama FROM mkelas H  WHERE H.`status`='1'
			) T  WHERE T.idtipe='$idtipe'
			";
		$hasil=$this->db->query($q)->result();
		$opsi='<option value="0" selected>SEMUA</option>';
		foreach($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.($r->nama).'</option>';
		}
		$this->output->set_output(json_encode($opsi));
	}
	function get_asuransi(){
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		if ($idkelompokpasien=='1'){
			
			$q="
			SELECT *FROM mrekanan where status='1'
			";
			$hasil=$this->db->query($q)->result();
		}else{
			$hasil=array();
		}
		$opsi='<option value="0" selected>SEMUA</option>';
		foreach($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.($r->nama).'</option>';
		}
		$this->output->set_output(json_encode($opsi));
	}
	function simpan_cover(){
		$user_id=$this->session->userdata('user_id');
		$idtipe_barang = $this->input->post('idtipe_barang');
		$idkategori_barang = $this->input->post('idkategori_barang');
		$idbarang = $this->input->post('idbarang');
		$asal_pasien = $this->input->post('asal_pasien');
		$idpoli_kelas = $this->input->post('idpoli_kelas');
		$idkelompokpasien = $this->input->post('idkelompokpasien');
		$idrekanan = $this->input->post('idrekanan');
		$cover = $this->input->post('cover');
		$q="SELECT * FROM setting_cover_barang 
		WHERE 
		idtipe_barang='".$idtipe_barang."' AND 
		idkategori_barang='".$idkategori_barang."' AND 
		idbarang='".$idbarang."' AND 
		asal_pasien='".$asal_pasien."' AND 
		idpoli_kelas='".$idpoli_kelas."' AND 
		idkelompokpasien='".$idkelompokpasien."' AND 
		idrekanan='".$idrekanan."' AND 
		cover='".$cover."'";
		$row=$this->db->query($q)->row();
		if ($row){
			$hasil=false;
		}else{
			$data=array(
				'idtipe_barang' => $idtipe_barang,
				'idkategori_barang' => $idkategori_barang,
				'idbarang' => $idbarang,
				'asal_pasien' => $asal_pasien,
				'idpoli_kelas' => $idpoli_kelas,
				'idkelompokpasien' => $idkelompokpasien,
				'idrekanan' => $idrekanan,
				'cover' => $cover,
				'created_by' => $user_id,
				'created_date' => date('Y-m-d H:i:s'),

			);
			$hasil=$this->db->insert('setting_cover_barang',$data);
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function get_kategori(){
		$idtipe 	= $this->input->post('idtipe');
		$hasil=array();
		if ($idtipe=='1'){//ALKES
			$q="SELECT id,`level`,path,
				CASE 
					WHEN nama_L1 IS NOT NULL THEN nama_L1
					WHEN nama_L2 IS NOT NULL THEN nama_L2
					WHEN nama_L3 IS NOT NULL THEN nama_L3
					WHEN nama_L4 IS NOT NULL THEN nama_L4
					WHEN nama_L5 IS NOT NULL THEN nama_L5
					WHEN nama_L6 IS NOT NULL THEN nama_L6
					WHEN nama_L7 IS NOT NULL THEN nama_L7
				  ELSE NULL
				  
				END as nama
				 FROM view_kategori_alkes";
			$hasil=$this->db->query($q)->result();
		}
		if ($idtipe=='2'){//IMPPLAN
			$q="SELECT id,`level`,path,
				CASE 
					WHEN nama_L1 IS NOT NULL THEN nama_L1
					WHEN nama_L2 IS NOT NULL THEN nama_L2
					WHEN nama_L3 IS NOT NULL THEN nama_L3
					WHEN nama_L4 IS NOT NULL THEN nama_L4
					WHEN nama_L5 IS NOT NULL THEN nama_L5
					WHEN nama_L6 IS NOT NULL THEN nama_L6
					WHEN nama_L7 IS NOT NULL THEN nama_L7
				  ELSE NULL
				  
				END as nama
				 FROM view_kategori_implan";
			$hasil=$this->db->query($q)->result();
		}
		if ($idtipe=='3'){//OBAT
			$q="SELECT id,`level`,path,
				CASE 
					WHEN nama_L1 IS NOT NULL THEN nama_L1
					WHEN nama_L2 IS NOT NULL THEN nama_L2
					WHEN nama_L3 IS NOT NULL THEN nama_L3
					WHEN nama_L4 IS NOT NULL THEN nama_L4
					WHEN nama_L5 IS NOT NULL THEN nama_L5
					WHEN nama_L6 IS NOT NULL THEN nama_L6
					WHEN nama_L7 IS NOT NULL THEN nama_L7
				  ELSE NULL
				  
				END as nama
				 FROM view_kategori_obat";
			$hasil=$this->db->query($q)->result();
		}
		if ($idtipe=='4'){//LOG
			$q="SELECT id,`level`,path,
				CASE 
					WHEN nama_L1 IS NOT NULL THEN nama_L1
					WHEN nama_L2 IS NOT NULL THEN nama_L2
					WHEN nama_L3 IS NOT NULL THEN nama_L3
					WHEN nama_L4 IS NOT NULL THEN nama_L4
					WHEN nama_L5 IS NOT NULL THEN nama_L5
					WHEN nama_L6 IS NOT NULL THEN nama_L6
					WHEN nama_L7 IS NOT NULL THEN nama_L7
				  ELSE NULL
				  
				END as nama
				 FROM view_kategori_logistik";
			$hasil=$this->db->query($q)->result();
		}
		$opsi='<option value="0" selected>SEMUA</option>';
		foreach($hasil as $r){
			
		$opsi .='<option value="'.$r->id.'">'.TreeView($r->level,$r->nama).'</option>';
		}
		$this->output->set_output(json_encode($opsi));
	}
	
	function load_cover()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT TB.nama_tipe,K.nama as nama_kategori
						,CASE WHEN H.idkelompokpasien=1 THEN MR.nama ELSE '' END as nama_rekanan
						,CASE WHEN H.asal_pasien IN (1,2) THEN MP.nama ELSE MK.nama END nama_poli
						,PK.nama as nama_kelompok
						,B.nama as nama_barang
						,H.* FROM setting_cover_barang H
						LEFT JOIN view_barang_all B ON B.id=H.idbarang AND B.idtipe=H.idtipe_barang
						LEFT JOIN mdata_tipebarang TB ON TB.id=H.idtipe_barang
						LEFT JOIN mdata_kategori K ON K.id=H.idkategori_barang
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli_kelas AND H.asal_pasien IN (1,2)
						LEFT JOIN mkelas MK ON MK.id=H.idpoli_kelas AND H.asal_pasien IN (2,3)
						LEFT JOIN mpasien_kelompok PK ON PK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan AND H.idkelompokpasien='1'
						ORDER BY H.id ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array();
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->idtipe_barang==0?text_default('SEMUA'):$r->nama_tipe);
          $result[] = ($r->idkategori_barang=='0'?text_default('SEMUA'):$r->nama_kategori);
          $result[] = ($r->idbarang=='0'?text_default('SEMUA'):$r->nama_barang);
          $result[] = ($r->asal_pasien=='0'?text_default('SEMUA'):GetTipePasienPiutang($r->asal_pasien));
          $result[] = ($r->idpoli_kelas=='0'?text_default('SEMUA'):$r->nama_poli);
          $result[] = ($r->idkelompokpasien=='0'?text_default('SEMUA'):$r->nama_kelompok);
          $result[] = ($r->idrekanan=='0'?text_default('SEMUA'):$r->nama_rekanan);
          $result[] = $this->jenis_cover($r->cover);
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_cover('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function jenis_cover($id){
		$hasil='';
		if ($id=='0'){
			$hasil ='TIDAK TAMPIL';
		}elseif($id=='1'){
			$hasil ='NOTIFIKASI TIDAK DICOVER';
		}elseif($id=='2'){
			$hasil ='TIDAK DAPAT DIPILIH';
		
		}
		return $hasil;
	}
	function jenis_resep($id){
		$hasil='';
		if ($id=='0'){
			$hasil =text_default('TIDAK DITENTUKAN');
		}elseif($id=='1'){
			$hasil ='YA';
		}elseif($id=='2'){
			$hasil ='TIDAK';
		
		}
		return $hasil;
	}
    function hapus_cover(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_cover_barang');
	  
	  json_encode($hasil);
	  
  }
  
}
