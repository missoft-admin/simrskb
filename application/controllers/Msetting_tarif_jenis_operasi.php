<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Msetting_tarif_jenis_operasi extends CI_Controller
{
    /**
     * Pengaturan Tarif Jenis Operasi controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Msetting_tarif_jenis_operasi_model');
        $this->load->helper('path');
    }

    public function index(): void
    {
        $data = [];
        $data['error'] = '';
        $data['title'] = 'Pengaturan Tarif Jenis Operasi';
        $data['content'] = 'Msetting_tarif_jenis_operasi/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Pengaturan Tarif Jenis Operasi', '#'],
            ['List', 'msetting_tarif_jenis_operasi'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	function getIndex2()
	{
		
			$this->select = array();
			$from="
					(
						SELECT H.id,H.nama,COUNT(M.id) as jml_tarif FROM mrekanan H 
						LEFT JOIN msetting_tarif_jenis_operasi M ON M.idrekanan=H.id
						WHERE H.`status`='1' 
						GROUP BY H.id
						ORDER BY H.id ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
       foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = $r->nama.($r->jml_tarif>0?'<span class="badge badge-danger pull-right">'.$r->jml_tarif.' TARIF</span>':'<span class="badge badge-default pull-right">TIDAK ADA TARIF</span>');

            $aksi = '<div class="btn-group">';
            $aksi .= '<a href="'.site_url().'msetting_tarif_jenis_operasi/detail/1/'.$r->id.'" data-toggle="tooltip" title="Setting Tarif" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>';
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
  }
    public function getIndex(): void
    {
        $this->select = array();
		$from="
				(
					SELECT H.id,H.nama,COUNT(M.id) as jml_tarif FROM mpasien_kelompok H 
					LEFT JOIN msetting_tarif_jenis_operasi M ON M.idrekanan=0 AND M.idkelompokpasien=H.id
					WHERE H.`status`='1' 
					GROUP BY H.id
					ORDER BY H.id ASC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama');
		$this->column_order  = array();
        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = $r->nama.($r->jml_tarif>0?'<span class="badge badge-danger pull-right">'.$r->jml_tarif.' TARIF</span>':'<span class="badge badge-default pull-right">TIDAK ADA TARIF</span>');

            $aksi = '<div class="btn-group">';
            $aksi .= '<a href="'.site_url().'msetting_tarif_jenis_operasi/detail/'.$r->id.'/0" data-toggle="tooltip" title="Setting Tarif" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>';
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }
	function detail($idkelompokpasien,$idrekanan='0'){
		$data=$this->Msetting_tarif_jenis_operasi_model->get_detail($idkelompokpasien,$idrekanan);
		$data['list_tarif']=$this->Msetting_tarif_jenis_operasi_model->get_tarif_kelompok_pasien($idkelompokpasien,$idrekanan);
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Tarif Kelompok Pasien';
		$data['content'] 		= 'Msetting_tarif_jenis_operasi/detail';
		$data['breadcrum'] 	= array(
								  array("RSKB Halmahera",'#'),
								  array("Bedah",'#'),
								  array("Pengaturan Estimasi Biaya",'setting_rencana_biaya/index')
								);
		// print_r($data);exit;
		$data = array_merge($data,backend_info());
		$this->parser->parse('module_template', $data);
	}
	function simpan_tarif(){
		$idkelompokpasien=($this->input->post('idkelompokpasien'));
		$idrekanan=($this->input->post('idrekanan'));
		$kelompok_operasi_id=($this->input->post('kelompok_operasi_id'));
		$cito=($this->input->post('cito'));
		$jenis_operasi_id=($this->input->post('jenis_operasi_id'));
		$idjenisoperasi=($this->input->post('idjenisoperasi'));
		$hasil=$this->cek_duplicate_tarif($idkelompokpasien,$idrekanan,$kelompok_operasi_id,$cito,$jenis_operasi_id);
		if ($hasil==null){	
			$this->idkelompokpasien = $idkelompokpasien;
			$this->idrekanan = $idrekanan;
			$this->kelompok_operasi_id = $kelompok_operasi_id;
			$this->cito = $cito;
			$this->jenis_operasi_id = $jenis_operasi_id;
			$this->idjenisoperasi = $idjenisoperasi;
			
			$hasil=$this->db->insert('msetting_tarif_jenis_operasi',$this);
		}else{
			$hasil=null;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_tarif($idkelompokpasien,$idrekanan,$kelompok_operasi_id,$cito,$jenis_operasi_id){
		$q="SELECT *FROM msetting_tarif_jenis_operasi WHERE idkelompokpasien='$idkelompokpasien' AND idrekanan='$idrekanan' 
				AND kelompok_operasi_id='$kelompok_operasi_id' AND cito='$cito' AND jenis_operasi_id='$jenis_operasi_id'";
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	function load_tarif()
	{
			$idkelompokpasien=$this->input->post('idkelompokpasien');	
			$idrekanan=$this->input->post('idrekanan');	
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT 
						H.*,K.nama as nama_kel_operasi,C.ref as nama_cito,J.nama as nama_jenis
						,T.nama as nama_tarif
						FROM msetting_tarif_jenis_operasi H
						INNER JOIN mkelompok_operasi K ON K.id=H.kelompok_operasi_id
						LEFT JOIN merm_referensi C ON C.nilai=H.cito AND C.ref_head_id='123'
						LEFT JOIN erm_jenis_operasi J ON J.id=H.jenis_operasi_id
						LEFT JOIN mjenis_operasi T ON T.id=H.idjenisoperasi
						WHERE H.idkelompokpasien='$idkelompokpasien' AND H.idrekanan='$idrekanan'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_kel_operasi','nama_cito','nama_jenis');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->nama_kel_operasi);
          $result[] = ($r->nama_cito);
          $result[] = ($r->nama_jenis);
          $result[] = ($r->nama_tarif);
         
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_tarif('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_tarif(){
	  $id=$this->input->post('id');
	 
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('msetting_tarif_jenis_operasi');
	  
	  json_encode($hasil);
	  
	}
}
