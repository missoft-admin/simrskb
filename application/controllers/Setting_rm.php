<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_rm extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Setting_rm_model');
		$this->load->helper('path');
		
  }


	function index($tab='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('1847'))){
				$tab='1';
			}
			if (UserAccesForm($user_acces_form,array('1848'))){
				$tab='2';
			}
		}
		
		if (UserAccesForm($user_acces_form,array('1847','1848'))){
			$data_per = $this->Setting_rm_model->get_rm_setting_label_per();
			// print_r($data_per);exit;
			$data = $this->Setting_rm_model->get_rm_setting_label();
			$data['tab'] 			= $tab;
			$data['error'] 			= '';
			
			$data['title'] 			= 'Setting Rehabilitas Medis';
			$data['content'] 		= 'Setting_rm/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Rehabilitas Medis Setting",'setting_rm/index')
												);

			$data = array_merge($data,$data_per, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function save_rm_label(){
		if ($this->Setting_rm_model->save_rm_label()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_rm/index/1','location');
		}
	}
	function save_rm_label_per(){
		if ($this->Setting_rm_model->save_rm_label_per()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_rm/index/2','location');
		}
	}

  
}
