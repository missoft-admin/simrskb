<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tbagi_hasil_approval extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbagi_hasil_approval_model','model');
	}

	function index() {
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		$tgl_terakhir = date('Y-m-d', strtotime($hari_ini));
		$data=array(
			'status'=>'2',
			'mbagi_hasil_id'=>'#',
			'deskripsi'=>'',
			'no_terima'=>'',
			'tanggal_tagihan1'=>'',
			'tanggal_tagihan2'=>'',
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_mbagi_hasil'] 	= $this->model->list_mbagi_hasil();
		$data['title'] 			= 'Approval Bagi Hasil';
		$data['content'] 		= 'Tbagi_hasil_approval/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Bagi Hasil Transaksi",'tbagi_hasil_approval/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		 $iduser=$this->session->userdata('user_id');	

		$notransaksi=$this->input->post('notransaksi');
		$mbagi_hasil_id=$this->input->post('mbagi_hasil_id');
		$status=$this->input->post('status');
		
		$tanggal_tagihan1=$this->input->post('tanggal_tagihan1');
		$tanggal_tagihan2=$this->input->post('tanggal_tagihan2');
		
		$where='';
		if ($notransaksi !=''){
			$where .=" AND H.notransaksi='$notransaksi' ";
		}
		
		if ($mbagi_hasil_id !='#'){
			$where .=" AND H.mbagi_hasil_id='$mbagi_hasil_id' ";
		}
		if ('' != $tanggal_tagihan1) {
            $where .= " AND DATE(H.tanggal_trx) >='".YMDFormat($tanggal_tagihan1)."' AND DATE(H.tanggal_trx) <='".YMDFormat($tanggal_tagihan2)."'";
        }
		
		if ($status !='#'){
			$where .=" AND H.status='$status' ";
		}


        $from = "(
					SELECT H.id,H.nama_bagi_hasil,H.tanggal_trx,H.notransaksi,H.mbagi_hasil_id 
					,H.pendapatan_bersih_rs,H.pendapatan_bersih_ps,H.`status`,H.status_approval,(SELECT MAX(AP.step) FROM tbagi_hasil_bayar_approval AP WHERE AP.st_aktif='1' AND AP.tbagi_hasil_bayar_id=H.id) as step
					FROM `tbagi_hasil_bayar_head` H
					LEFT JOIN tbagi_hasil_bayar_approval A ON A.tbagi_hasil_bayar_id=H.id 
					WHERE A.iduser IN ('".$iduser."')".$where."
	GROUP BY H.id

				) as tbl ";


		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nama_bagi_hasil','notransaksi');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $status     = '';
			$respon='';
            $status_setting     = '';
            $url        = site_url('tbagi_hasil/');
            if ($r->status=='2'){
				$status_setting='<button title="Lihat User" class="btn btn-danger btn-xs user" onclick="load_user('.$r->id.')"><i class="si si-user"></i></button>';
			}
			$query=$this->get_respon($r->id,$r->step);
			if ($query){
			foreach ($query as $res){
				if ($res->iduser==$iduser && $res->step==$r->step){
					if ($res->approve=='0'){
					  $aksi .= '<button title="Setuju" class="btn btn-success btn-xs setuju" onclick="setuju('.$res->id.','.$r->id.')"><i class="fa fa-check"></i> SETUJUI</button>';
					  $aksi .= '<button title="Tolak" class="btn btn-danger btn-xs tolak" onclick="tolak('.$res->id.','.$r->id.')"><i class="si si-ban"></i> TOLAK</button>';
					}else{
					  if ($r->status=='2'){
					  $aksi .= '<button title="Batalkan Pesetujuan / Tolak" class="btn btn-success btn-xs batal" onclick="batal('.$res->id.','.$r->id.')"><i class="fa fa-refresh"></i> Batalkan</button>';
						  
					  }
					}
				}
				if ($res->iduser==$iduser){
					$respon .='<span class="label label-warning" data-toggle="tooltip">STEP '.$res->step.'</span> : '.status_approval($res->approve).'<br>';
				}
			}
			  
			  
		  }else{
			   $respon='';
		  }
            $row[] = $r->id;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggal_trx);
            $row[] = $r->notransaksi;
            $row[] = $r->nama_bagi_hasil;
            $row[] = number_format($r->pendapatan_bersih_ps,0);
            $row[] = number_format($r->pendapatan_bersih_rs,0);
            $row[] = status_bagi_hasil_bayar($r->status,$r->step).' '.$status_setting;			
            $row[] = $respon;//8            
				
			$aksi.='</div>';
            $row[] = $aksi;//8            
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function get_respon($id,$step){
	  $iduser=$this->session->userdata('user_id');
	  $q="SELECT *from tbagi_hasil_bayar_approval AP
			WHERE AP.st_aktif='1' AND AP.iduser='$iduser' AND AP.tbagi_hasil_bayar_id='$id'";
	  return $this->db->query($q)->result();
  }
	function list_user($id){
		$q="SELECT *FROM tbagi_hasil_bayar_approval H WHERE H.tbagi_hasil_bayar_id='$id'";
		$row=$this->db->query($q)->result();
		$content='';
		$no=1;
		$no_step="1";
		foreach($row as $r){
			$st_user_lain='';
			$content .='<tr class="'.($r->st_aktif=='1' && $r->approve=='0'?'success':'active').'">';
			if ($no_step != $r->step){
				$no_step=$r->step;
			}
			if ($no_step % 2){
				$content .='<td> <span class="label label-default" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
			}else{
				$content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
				// $content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span></td>';
			}
			if ($r->approve !='0'){
				if ($r->iduser !=$r->user_eksekusi){
					$st_user_lain=' <span class="label label-warning" data-toggle="tooltip">Oleh User Lain</span>';
				}
			}
			$content .='<td><i class="si si-users fa-2x text-black-op"></i>  '.$r->user_nama.'</td>';
			$content .='<td>'.($r->st_aktif=='1'?status_approval($r->approve):'').' '.$st_user_lain.'</td>';			
			$content .='</tr>';
			
		}
		$arr['detail']=$content;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($content));
	}
	function setuju_batal($id,$status,$idrka=''){
		// $arr=array();
		$q="call update_tbagi_hasil_approval('$id', $status) ";
		$result=$this->db->query($q);
		// // $arr['st_insert']=$result;
		// $arr=$this->db->query("SELECT H.st_bendahara FROM rka_pengajuan H  WHERE H.id='$idrka' AND H.status='2'")->row('st_bendahara');
		$this->output->set_output(json_encode($result));
	}
	function tolak(){
		$id=$this->input->post('id_approval');
		$alasan_tolak=$this->input->post('alasan_tolak');
		$q="call update_tbagi_hasil_approval('$id', 2) ";
		$this->db->query($q);
		$result=$this->db->update('tbagi_hasil_bayar_approval',array('alasan_tolak'=>$alasan_tolak),array('id'=>$id));
		echo json_encode($result);
	}
}
