<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

class Trujukan_fisioterapi extends CI_Controller
{
	/**
	 * Rujukan Fisioterapi controller.
	 * Developer @gunalirezqimauludi
	 */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Trujukan_fisioterapi_model');
	}

	public function index()
	{
		$data = [
			'nomedrec' => '',
			'namapasien' => '',
			'idasalpasien' => '',
			'iddokterperujuk' => '',
			'idpetugas' => '',
			'idtindakan' => '',
			'tanggaldari' => date('d/m/Y', strtotime('-1 day')),
			'tanggalsampai' => date('d/m/Y'),
		];
		$data['error'] = '';
		$data['title'] = 'Rujukan Fisioterapi';
		$data['content'] = 'Trujukan_fisioterapi/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rujukan Fisioterapi', '#'],
			['List', 'trujukan_fisioterapi']
		];

		$data['list_tindakan'] = $this->Trujukan_fisioterapi_model->getListTarifFisioterapi();
		$data['list_dokterperujuk'] = $this->Trujukan_fisioterapi_model->getListDokterPerujuk();
		$data['list_petugas'] = $this->Trujukan_fisioterapi_model->getLisPegawaiFisioterapi();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter()
	{
		$data = [
			'nomedrec' => $this->input->post('nomedrec'),
			'namapasien' => $this->input->post('namapasien'),
			'idasalpasien' => $this->input->post('idasalpasien'),
			'iddokterperujuk' => $this->input->post('iddokterperujuk'),
			'idpetugas' => $this->input->post('idpetugas'),
			'idtindakan' => $this->input->post('idtindakan'),
			'tanggaldari' => $this->input->post('tanggaldari'),
			'tanggalsampai' => $this->input->post('tanggalsampai'),
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Rujukan Fisioterapi';
		$data['content'] = 'Trujukan_fisioterapi/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rujukan Fisioterapi', '#'],
			['List', 'trujukan_fisioterapi']
		];

		$data['list_tindakan'] = $this->Trujukan_fisioterapi_model->getListTarifFisioterapi();
		$data['list_dokterperujuk'] = $this->Trujukan_fisioterapi_model->getListDokterPerujuk();
		$data['list_petugas'] = $this->Trujukan_fisioterapi_model->getLisPegawaiFisioterapi();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function create($id)
	{
		if ($id != '') {
			$row = $this->Trujukan_fisioterapi_model->getSpecified($id);
			$data = [
				'id' => $id,
				'idpendaftaran' => $row->idpendaftaran,
				'tanggalrujukan' => date('d/m/Y'),
				'wakturujukan' => date('H:i:s'),
				'norujukan' => $row->norujukan,
				'nomedrec' => $row->nomedrec,
				'namapasien' => "$row->title $row->namapasien",
				'jeniskelamin' => ($row->jeniskelamin == 1 ? 'Laki-laki' : 'Perempuan'),
				'alamat' => "$row->alamat, $row->prov, $row->kota, $row->kec, $row->kel",
				'tanggallahir' => $row->tanggallahir,
				'umur_tahun' => $row->umur_tahun,
				'umur_bulan' => $row->umur_bulan,
				'umur_hari' => $row->umur_hari,
				'namarekanan' => $row->namarekanan,
				'iddokterperujuk' => $row->iddokterperujuk,
				'namadokterperujuk' => $row->namadokterperujuk,
				'idkelompokpasien' => $row->idkelompokpasien,
				'idrekanan' => $row->idrekanan,
				'namakelompok' => $row->namakelompok,
				'idkelas' => $row->idkelas,
				'idpegawai' => $row->idpegawai
			];
			$data['error'] = '';
			$data['title'] = 'Tindakan Rujukan Fisioterapi';
			$data['content'] = 'Trujukan_fisioterapi/manage';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['Rujukan Fisioterapi', '#'],
				['Tindakan', 'trujukan_fisioterapi']
			];

			$data['list_dokterperujuk'] = $this->Trujukan_fisioterapi_model->getListDokterPerujuk();
			$data['list_pegawai'] = $this->Trujukan_fisioterapi_model->getLisPegawaiFisioterapi();
			$data['list_detail'] = $this->Trujukan_fisioterapi_model->getListDetail($id);
			$data['list_parent'] = $this->Trujukan_fisioterapi_model->getAllParent($row->idkelompokpasien, $row->idrekanan);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_fisioterapi/index');
		}
	}

	public function delete($id)
	{
		$this->Trujukan_fisioterapi_model->cancelTrx($id);
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'data telah terhapus.');
		redirect('trujukan_fisioterapi/index', 'location');
	}

	public function save()
	{
		$data = [];
		$data['idrujukan'] = $_POST['id'];
		if ($this->Trujukan_fisioterapi_model->save()) {
			$data = array_merge($data, backend_info());
			$this->parser->parse('Trujukan_fisioterapi/redirect/save_tindakan', $data);
		}
	}

	public function after_save()
	{
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'data telah disimpan.');
		redirect('trujukan_fisioterapi/index', 'location');
	}

	/* Method Print */
	public function print_bukti_pemeriksaan($id)
	{
		if ($id != '') {
			$row = $this->Trujukan_fisioterapi_model->getSpecified($id);
			$data = [
				'id' => $id,
				'tanggalrujukan' => $row->tanggalrujukan,
				'norujukan' => $row->norujukan,
				'nomedrec' => $row->nomedrec,
				'namapasien' => $row->namapasien,
				'jeniskelamin' => GetJenisKelamin($row->jeniskelamin),
				'alamat' => $row->alamat,
				'tanggallahir' => $row->tanggallahir,
				'umur_tahun' => $row->umur_tahun,
				'umur_bulan' => $row->umur_bulan,
				'umur_hari' => $row->umur_hari,
				'namarekanan' => $row->namarekanan,
				'iddokterperujuk' => $row->iddokterperujuk,
				'namadokterperujuk' => $row->namadokterperujuk,
				'idkelompokpasien' => $row->idkelompokpasien,
				'namakelompok' => $row->namakelompok,
				'asalrujukan' => $row->asalrujukan
			];

			$data['error'] = '';
			$data['title'] = 'Pemeriksaan Fisioterapi';

			$data['listTindakan'] = $this->Trujukan_fisioterapi_model->getListDetail($id);
			$data = array_merge($data, backend_info());
			$this->parser->parse('Trujukan_fisioterapi/print/bukti_pemeriksaan', $data);
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_fisioterapi/index');
		}
	}

	public function print_bukti_pemeriksaan_thermal($id)
	{
		// instantiate and use the dompdf class
		$options = new Options();
		$options->set('isRemoteEnabled', true);
		$dompdf = new Dompdf($options);

		$data = [];
		$data['listTindakan'] = $this->Trujukan_fisioterapi_model->getListDetail($id);

		$row = $this->Trujukan_fisioterapi_model->getSpecified($id);
		if ($row) {
			$data['norujukan'] = $row->norujukan;
			$data['tanggal'] = $row->tanggal;
			$data['nomedrec'] = $row->nomedrec;
			$data['namapasien'] = $row->namapasien;
			$data['tanggallahir'] = $row->tanggallahir;
			$data['umur'] = $row->umur_tahun;
			$data['umurbulan'] = $row->umur_bulan;
			$data['umurhari'] = $row->umur_hari;
			$data['asalrujukan'] = GetAsalRujukan($row->asalrujukan);
			$data['dokterperujuk'] = $row->namadokterperujuk;
			$data['kelompokpasien'] = $row->namakelompok;
			$data['tarifaktif'] = '-';
			$data['userinput'] = 'Nama User Input';
		}

		$html = $this->parser->parse('Trujukan_fisioterapi/print/bukti_pemeriksaan_thermal', array_merge($data, backend_info()), true);
		$html = $this->parser->parse_string($html, $data);

		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$customPaperThermal = [0, 0, 226, 4000];
		$dompdf->set_paper($customPaperThermal);

		// Render the HTML as PDF
		$dompdf->render();

		// // Output the generated PDF to Browser
		$dompdf->stream('Bukti Pemeriksaan Fisioterapi.pdf', ['Attachment' => 0]);
	}

	public function getIndex($uri)
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		$this->select = [
			'trujukan_fisioterapi.*',
			'tpoliklinik_pendaftaran.tanggaldaftar',
			'tpoliklinik_pendaftaran.id AS idpendaftaran',
			'(CASE
        		WHEN trujukan_fisioterapi.asalrujukan = 3 THEN
        			trawatinap_pendaftaran.nopendaftaran
        		ELSE
        			tpoliklinik_pendaftaran.nopendaftaran
        	END) AS nopendaftaran',
			'(CASE
        		WHEN trujukan_fisioterapi.asalrujukan = 3 THEN
        			trawatinap_pendaftaran.idtipe
        		ELSE
        			tpoliklinik_pendaftaran.idtipe
        	END) AS idtipe',
			'mfpasien.no_medrec',
			'mfpasien.nama'
		];

		$this->from = 'trujukan_fisioterapi';

		$this->join = [
			['trawatinap_pendaftaran', 'trujukan_fisioterapi.idtindakan = trawatinap_pendaftaran.id AND trujukan_fisioterapi.asalrujukan = 3', 'LEFT'],
			['tpoliklinik_tindakan', 'tpoliklinik_tindakan.id = trujukan_fisioterapi.idtindakan AND trujukan_fisioterapi.asalrujukan IN (1, 2) AND tpoliklinik_tindakan.status = 1', 'LEFT'],
			['trujukan_fisioterapi_detail', 'trujukan_fisioterapi_detail.idrujukan = trujukan_fisioterapi.id', 'LEFT'],
			['tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran OR tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT'],
			['mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', '']
		];

		$this->where = [];

		// FILTER
		if ($uri == 'filter') {
			if ($this->session->userdata('nomedrec') != null) {
				$this->where = array_merge($this->where, ['mfpasien.no_medrec LIKE' => '%' . $this->session->userdata('nomedrec') . '%']);
			}
			if ($this->session->userdata('namapasien') != null) {
				$this->where = array_merge($this->where, ['mfpasien.nama LIKE' => '%' . $this->session->userdata('namapasien') . '%']);
			}
			if ($this->session->userdata('idasalpasien') != 0) {
				$this->where = array_merge($this->where, ['trujukan_fisioterapi.asalrujukan' => $this->session->userdata('idasalpasien')]);
			}
			if ($this->session->userdata('iddokterperujuk') != 0) {
				$this->where = array_merge($this->where, ['trujukan_fisioterapi.iddokterperujuk' => $this->session->userdata('iddokterperujuk')]);
			}
			if ($this->session->userdata('idpetugas') != 0) {
				$this->where = array_merge($this->where, ['trujukan_fisioterapi.idpegawai' => $this->session->userdata('idpetugas')]);
			}
			if ($this->session->userdata('idtindakan') != '0') {
				$this->where = array_merge($this->where, ['trujukan_fisioterapi_detail.idfisioterapi' => $this->session->userdata('idtindakan')]);
			}
			if ($this->session->userdata('tanggaldari') != null) {
				$this->where = array_merge($this->where, ['DATE(trujukan_fisioterapi.tanggal) >=' => YMDFormat($this->session->userdata('tanggaldari'))]);
			}
			if ($this->session->userdata('tanggalsampai') != null) {
				$this->where = array_merge($this->where, ['DATE(trujukan_fisioterapi.tanggal) <=' => YMDFormat($this->session->userdata('tanggalsampai'))]);
			}
		} else {
			$this->where = array_merge($this->where, ['DATE(trujukan_fisioterapi.tanggal)' => date('Y-m-d')]);
		}

		$this->order = [
			'tpoliklinik_pendaftaran.tanggaldaftar' => 'DESC',
			'tpoliklinik_pendaftaran.noantrian' => 'ASC',
		];

		$this->group = ['trujukan_fisioterapi.id'];

		$this->column_search = ['trujukan_fisioterapi.tanggal', 'trujukan_fisioterapi.noantrian', 'trujukan_fisioterapi.norujukan', 'tpoliklinik_pendaftaran.nopendaftaran', 'trawatinap_pendaftaran.nopendaftaran', 'mfpasien.no_medrec', 'mfpasien.nama'];
		$this->column_order = ['trujukan_fisioterapi.tanggal', 'trujukan_fisioterapi.noantrian', 'trujukan_fisioterapi.norujukan', 'tpoliklinik_pendaftaran.nopendaftaran', 'trawatinap_pendaftaran.nopendaftaran', 'mfpasien.no_medrec', 'mfpasien.nama'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			if ($r->status == 0) {
				$action = '<label>-</label>';
			} elseif ($r->status == 1) {
				$action = '<div class="btn-group">';
				if (UserAccesForm($user_acces_form, ['1033'])) {
					$action .= '<a href="' . site_url() . 'trujukan_fisioterapi/create/' . $r->id . '" data-toggle="tooltip" title="Tindakan" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form, ['1040'])) {
					$action .= '<a href="' . site_url() . 'trujukan_fisioterapi/delete/' . $r->id . '" data-urlindex="' . site_url() . 'trujukan_fisioterapi" data-urlremove="' . site_url() . 'trujukan_fisioterapi/delete/' . $r->id . '" data-toggle="tooltip" title="Batalkan" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
				}
				if (UserAccesForm($user_acces_form, ['1041'])) {
					$action .= '<div class="btn-group">
                            <div class="btn-group dropup">
                              <button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                <span class="fa fa-print"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li>
                                  <a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/manage_print/' . $r->idpendaftaran . '">Sticker ID</a>
                                </li>
                              </ul>
                            </div>
                          </div>
        	             </div>';
				}
			} elseif ($r->status == 3) {
				$action = '<div class="btn-group">';
				if (UserAccesForm($user_acces_form, ['1039'])) {
					$action .= '<a href="' . site_url() . 'trujukan_fisioterapi/create/' . $r->id . '" data-toggle="tooltip" title="Lihat" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>';
				}
				if (UserAccesForm($user_acces_form, ['1041', '1042', '1043'])) {
					$action .= '<div class="btn-group">
                            <div class="btn-group dropup">
                              <button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                <span class="fa fa-print"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li>';
					if (UserAccesForm($user_acces_form, ['1041'])) {
						$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/manage_print/' . $r->idpendaftaran . '">Sticker ID</a>';
					}
					if (UserAccesForm($user_acces_form, ['1042'])) {
						$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_fisioterapi/print_bukti_pemeriksaan/' . $r->id . '">Bukti Pemeriksaan</a>';
					}
					if (UserAccesForm($user_acces_form, ['1043'])) {
						$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_fisioterapi/print_bukti_pemeriksaan_thermal/' . $r->id . '">Bukti Pemeriksaan (Small)</a>';
					}
					$action .= '    </li>
                              </ul>
                            </div>
                          </div>
        	             </div>';
				}
			}

			$row[] = $no;
			$row[] = $r->tanggal;
			$row[] = $r->noantrian;
			$row[] = $r->norujukan;
			$row[] = $r->nopendaftaran;
			$row[] = $r->no_medrec;
			$row[] = $r->nama;
			if ($r->asalrujukan == 3 && $r->idtipe == 2) {
				$row[] = 'One Day Surgery (ODS)';
			} else {
				$row[] = GetAsalRujukan($r->asalrujukan);
			}
			$row[] = StatusRujukan($r->status);
			$row[] = $action;

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function getTindakan($idkelas, $idkelompokpasien, $idrekanan, $idparent)
	{
		$this->db->select('mtarif_fisioterapi.path');
		if ($idkelompokpasien == 1) {
			$this->db->select('mtarif_fisioterapi.path');
			$this->db->join('mrekanan', 'mrekanan.tfisioterapi = mtarif_fisioterapi.id');
			$this->db->where('mrekanan.id', $idrekanan);
			$query = $this->db->get('mtarif_fisioterapi');

			if ($query->num_rows() > 0) {
				$this->db->join('mrekanan', 'mrekanan.tfisioterapi = mtarif_fisioterapi.id');
				$this->db->where('mrekanan.id', $idrekanan);
			} else {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tfisioterapi = mtarif_fisioterapi.id');
				$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
			}
		} else {
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tfisioterapi = mtarif_fisioterapi.id');
			$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
		}
		$query = $this->db->get('mtarif_fisioterapi');
		$row = $query->row();

		$this->select = [
			'mtarif_fisioterapi.*',
			'mtarif_fisioterapi_detail.*'
		];

		$this->from = 'mtarif_fisioterapi';

		$this->join = [
			['mtarif_fisioterapi_detail', 'mtarif_fisioterapi_detail.idtarif = mtarif_fisioterapi.id', 'LEFT'],
		];

		$this->where = [
			'mtarif_fisioterapi_detail.kelas' => $idkelas,
			'mtarif_fisioterapi_detail.status' => '1',
			'mtarif_fisioterapi.headerpath' => $idparent,
			'mtarif_fisioterapi.path LIKE' => $row->path . '%',
		];

		$this->order = [
			"SUBSTRING_INDEX(CONCAT( mtarif_fisioterapi.path ,'.'),'.',1) + 0,
			SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT( mtarif_fisioterapi.path ,'.'),'.',2),'.',-1) + 0,
			SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT( mtarif_fisioterapi.path ,'.'),'.',3),'.',-1) + 0,
			SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT( mtarif_fisioterapi.path ,'.'),'.',4),'.',-1) + 0" => '',
		];

		$this->group = [];

		$this->column_search = ['mtarif_fisioterapi.nama'];
		$this->column_order = ['mtarif_fisioterapi.nama'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			if ($r->idkelompok == 1) {
				$action = '<a style="cursor: pointer;">' . TreeView($r->level, $r->nama) . '</a>';
			} else {
				$action = '<a style="cursor: pointer;" data-idtindakan="' . $r->id . '" id="tindakan-select" href="javascript:void(0)">' . TreeView($r->level, $r->nama) . '</a>';
			}

			$row[] = $no;
			$row[] = $action;
			$row[] = number_format($r->jasasarana);
			$row[] = number_format($r->jasapelayanan);
			$row[] = number_format($r->bhp);
			$row[] = number_format($r->biayaperawatan);
			$row[] = number_format($r->total);

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
