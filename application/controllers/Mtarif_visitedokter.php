<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtarif_visitedokter extends CI_Controller {

	/**
	 * Tarif Visite Dokter controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtarif_visitedokter_model');
		$this->load->model('Mtarif_administrasi_model');
  }

	function index($idruangan=1){
		$data = array();
		$data['error'] 			= '';
		$data['idruangan']  = $idruangan;
		$data['title'] 			= 'Tarif Visite Dokter';
		$data['content'] 		= 'Mtarif_visitedokter/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif Visite Dokter",'mtarif_visitedokter/index'),
														array("List",'#')
													);

		$data['list_ruangan'] = $this->Mtarif_visitedokter_model->getAllRuangan();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function filter(){
		if($this->input->post('idruangan') != ''){
			$idtipe = $this->input->post('idtipe');
			$idruangan = $this->input->post('idruangan');
			redirect("mtarif_visitedokter/index/$idruangan",'location');
		}else{
			redirect('mtarif_visitedokter/index/1','location');
		}
	}

	function create($idruangan){
		$data = array(
			'id' 							=> '',
			'idruangan' 			=> $idruangan,
			'nama' 						=> '',
			'status' 					=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Tarif Visite Dokter';
		$data['content'] 		= 'Mtarif_visitedokter/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif Visite Dokter",'#'),
									    			array("Tambah",'mtarif_visitedokter')
													);

		$data['list_ruangan'] = $this->Mtarif_visitedokter_model->getAllRuangan();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($idruangan, $id){
		if($id != ''){
			$row = $this->Mtarif_visitedokter_model->getSpecified($id);
			// print_r();exit();
			if(isset($row->id)){
				$data = array(
					'id' 							=> $row->id,
					'idruangan' 			=> $row->idruangan,
					'nama' 						=> $row->nama,
					'status' 					=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Tarif Visite Dokter';
				$data['content']	 	= 'Mtarif_visitedokter/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Tarif Visite Dokter",'#'),
											    			array("Ubah",'mtarif_visitedokter')
															);

				$data['list_ruangan'] = $this->Mtarif_visitedokter_model->getAllRuangan();
				$data['list_tarif'] = $this->Mtarif_visitedokter_model->getAllTarif($row->id);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtarif_visitedokter/index/'.$idruangan, 'location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtarif_visitedokter/index/'.$idruangan, 'location');
		}
	}

	function delete($idruangan, $id){
		$this->Mtarif_visitedokter_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mtarif_visitedokter/index/'.$idruangan, 'location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mtarif_visitedokter_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_visitedokter/index/'.$this->input->post('idruangan'), 'location');
				}
			} else {
				if($this->Mtarif_visitedokter_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_visitedokter/index/'.$this->input->post('idruangan'), 'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 			 = validation_errors();
		$data['content'] 		 = 'Mtarif_visitedokter/manage';

		$data['list_parent'] = $this->Mtarif_visitedokter_model->getAllParent();

		if($id==''){
			$data['title'] 			= 'Tambah Tarif Visite Dokter';
			$data['breadcrum']	= array(
															array("RSKB Halmahera",'#'),
															array("Tarif Visite Dokter",'#'),
										    			array("Tambah",'mtarif_visitedokter')
														);
		}else{
			$data['title'] 			= 'Ubah Tarif Visite Dokter';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Tarif Visite Dokter",'#'),
															array("Ubah",'mtarif_visitedokter')
														);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex($idruangan)
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];

			$this->select = array();
			$this->from   = 'mtarif_visitedokter';
			$this->join 	= array();
			$this->where  = array(
				'idruangan' => $idruangan,
				'status' => '1'
			);
			$this->order  = array('');
			$this->group  = array();

			$this->column_search   = array('nama');
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->nama;

					$aksi = '<div class="btn-group">';
		            if (UserAccesForm($user_acces_form,array('146'))){
		                $aksi .= '<a href="'.site_url().'mtarif_visitedokter/update/'.$idruangan.'/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
		            }
		            if (UserAccesForm($user_acces_form,array('147'))){
		                $aksi .= '<a href="'.site_url().'mtarif_visitedokter/delete/'.$idruangan.'/'.$r->id.'" data-urlindex="'.site_url().'mtarif_visitedokter" data-urlremove="'.site_url().'mtarif_visitedokter/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
		            }
								$aksi .= '<a href="'.site_url().'mtarif_visitedokter/setting/'.$idruangan.'/'.$r->id.'" data-toggle="tooltip" title="Setting" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>';
		            $aksi .= '</div>';

					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}

	function setting($idruangan, $id){
		if($id != ''){
			$row = $this->Mtarif_visitedokter_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 							=> $row->id,
					'idruangan' 			=> $row->idruangan,
					'nama' 						=> $row->nama,
					'group_diskon_all' 					=> $row->group_diskon_all,
					'status' 					=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Setting Group Pembayaran Tarif Visite Dokter';
				$data['content']	 	= 'Mtarif_visitedokter/setting';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Tarif Visite Dokter",'#'),
											    			array("Setting",'mtarif_visitedokter')
															);

				$data['list_ruangan'] = $this->Mtarif_visitedokter_model->getAllRuangan();
				$data['list_tarif'] = $this->Mtarif_visitedokter_model->getAllTarif($row->id);
				$data['list_group_pembayaran'] = $this->Mtarif_administrasi_model->getGroupPembayaran();

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtarif_visitedokter/index/'.$idruangan, 'location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtarif_visitedokter/index/'.$idruangan, 'location');
		}
	}

	function save_setting() {
		if($this->Mtarif_visitedokter_model->updateSetting()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mtarif_visitedokter/index/'.$this->input->post('idtipe'), 'location');
		}
	}
}
