<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Mpengaturan_jatuh_tempo extends CI_Controller
{
	/**
	 * Setting Jatuh Tempo Pembayaran controller.
	 * Developer @gunalirezqimauludi
	 */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpengaturan_jatuh_tempo_model', 'model');
		$this->load->helper('path');
	}

	public function index()
	{
		$data = [];
		$data['error'] = '';
		$data['title'] = 'Setting Jatuh Tempo Pembayaran';
		$data['content'] = 'Mpengaturan_jatuh_tempo/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Setting Jatuh Tempo Pembayaran', '#'],
			['List', 'mpengaturan_jatuh_tempo']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function create()
	{
		$data = [
			'id' => '',
			'nama' => '',
			'status' => '',
			'idtemp' => hash('ripemd160', $this->session->userdata('user_id') . '' . date("Y-m-d")),
		];

		$data['error'] = '';
		$data['title'] = 'Tambah Setting Jatuh Tempo Pembayaran';
		$data['content'] = 'Mpengaturan_jatuh_tempo/manage';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Setting Jatuh Tempo Pembayaran', '#'],
			['Tambah', 'mpengaturan_jatuh_tempo']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function update($id)
	{
		if ($id != '') {
			$row = $this->model->getSpecified($id);
			if (isset($row->id)) {
				$data = [
					'id' => $row->id,
					'nama' => $row->nama,
					'status' => $row->status,
					'idtemp' => $row->id,
				];

				$data['error'] = '';
				$data['title'] = 'Ubah Setting Jatuh Tempo Pembayaran';
				$data['content'] = 'Mpengaturan_jatuh_tempo/manage';
				$data['breadcrum'] = [
					['RSKB Halmahera', '#'],
					['Setting Jatuh Tempo Pembayaran', '#'],
					['Ubah', 'mpengaturan_jatuh_tempo']
				];

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			} else {
				$this->session->set_flashdata('error', true);
				$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
				redirect('mpengaturan_jatuh_tempo/index', 'location');
			}
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('mpengaturan_jatuh_tempo/index');
		}
	}

	public function delete($id)
	{
		$this->model->softDelete($id);
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'data telah terhapus.');
		redirect('mpengaturan_jatuh_tempo/index', 'location');
	}

	public function save()
	{
		if ($this->input->post('id') == '') {
			if ($this->model->saveData()) {
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect('mpengaturan_jatuh_tempo/index', 'location');
			}
		} else {
			if ($this->model->updateData()) {
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect('mpengaturan_jatuh_tempo/index', 'location');
			}
		}
	}

	public function getIndex()
	{
		$this->select = [
			'msetting_pembayaran_rumahsakit.id',
			'msetting_pembayaran_rumahsakit.nama',
			'jatuhtempo.tanggal',
			'rujukan.jumlahrs',
			'rujukan.jumlahklinik',
			'msetting_pembayaran_rumahsakit.status',
		];
		$this->from = 'msetting_pembayaran_rumahsakit';
		$this->join = [
			['(SELECT idsetting, GROUP_CONCAT(tanggal) AS tanggal FROM msetting_pembayaran_rumahsakit_jatuhtempo GROUP BY idsetting) jatuhtempo', 'jatuhtempo.idsetting = msetting_pembayaran_rumahsakit.id', ''],
			['(SELECT idsetting, SUM(CASE WHEN tiperujukan=2 THEN 1 ELSE 0 END) AS jumlahrs,SUM(CASE WHEN tiperujukan=1 THEN 1 ELSE 0 END) AS jumlahklinik FROM msetting_pembayaran_rumahsakit_rujukan GROUP BY idsetting) rujukan', 'rujukan.idsetting = msetting_pembayaran_rumahsakit.id', ''],
		];

		$this->order = [
			'msetting_pembayaran_rumahsakit.id' => 'DESC'
		];

		$this->group = [
			'msetting_pembayaran_rumahsakit.id'
		];

		$this->column_search = ['msetting_pembayaran_rumahsakit.nama'];
		$this->column_order = ['msetting_pembayaran_rumahsakit.nama'];
		
		$list = $this->datatable->get_datatables();

		$no = $_POST['start'];

		$data = [];
		foreach ($list as $r) {
			$no++;
			$row = [];

			$row[] = $no;
			$row[] = $r->nama;
			$row[] = $r->tanggal;
			$row[] = $r->jumlahrs;
			$row[] = $r->jumlahklinik;
			$row[] = StatusJatuhTempo($r->status);
			$row[] = '<div class="btn-group">
				<a href="' . site_url() . 'mpengaturan_jatuh_tempo/update/' . $r->id . '" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
				<a href="' . site_url() . 'mpengaturan_jatuh_tempo/delete/' . $r->id . '" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
			</div>';

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function getJatuhTempo($idsetting)
	{
		$data = $this->model->getDetailJatuhTempo($idsetting);
		$this->output->set_output(json_encode($data));
	}

	public function getRujukan($idsetting)
	{
		$data = $this->model->getDetailRujukan($idsetting);
		$this->output->set_output(json_encode($data));
	}

	public function getOpsiRujukan($idtipe)
	{
		$data = get_all('mrumahsakit', ['idtipe' => $idtipe, 'status' => 1]);
		$this->output->set_output(json_encode($data));
	}

	public function saveJatuhTempo()
	{
		$data = [];
		$data['idsetting'] = $this->input->post('idsetting');
		$data['tanggal'] = $this->input->post('tanggal');
		$data['jatuhtempo'] = $this->input->post('jatuhtempo');
		$data['created_by'] = $this->session->userdata('user_id');
		$data['created_at'] = date('Y-m-d H:i:s');

		if ($this->db->insert('msetting_pembayaran_rumahsakit_jatuhtempo', $data)) {
			return true;
		}
	}

	public function removeJatuhTempo($id)
	{
		$this->db->where('id', $id);
		if ($this->db->delete('msetting_pembayaran_rumahsakit_jatuhtempo')) {
			return true;
		}
	}

	public function saveRujukan()
	{
		$data = [];
		$data['idsetting'] = $this->input->post('idsetting');
		$data['tiperujukan'] = $this->input->post('tiperujukan');
		$data['idrujukan'] = $this->input->post('idrujukan');
		$data['namarujukan'] = $this->input->post('namarujukan');

		if ($this->db->insert('msetting_pembayaran_rumahsakit_rujukan', $data)) {
			return true;
		}
	}

	public function removeRujukan($id)
	{
		$this->db->where('id', $id);
		if ($this->db->delete('msetting_pembayaran_rumahsakit_rujukan')) {
			return true;
		}
	}
}
