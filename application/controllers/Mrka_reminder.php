<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrka_reminder extends CI_Controller {

	/**
	 * Create RKA controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('mrka_reminder_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Reminder RKA';
		$data['content'] 		= 'Mrka_reminder/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Create RKA",'#'),
									    			array("List",'mrka_reminder')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	
	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  $user_id=$this->session->userdata('user_id');
	  
	  $periode1=$this->input->post('periode1');
	  $periode2=$this->input->post('periode2');
	  $idprespektif=$this->input->post('idprespektif');
	  $idprogram=$this->input->post('idprogram');
	  $where='';
	  if ($periode1 !='#'){
		  $where .=" AND KN.bulan >='$periode1'";
	  }
	  if ($periode2 !='#'){
		  $where .=" AND KN.bulan <='$periode2'";
	  }
	  if ($idprespektif !='#'){
		  $where .=" AND K.idprespektif='$idprespektif'";
	  }
	  if ($idprogram !='#'){
		  $where .=" AND K.idprogram='$idprogram'";
	  }
		
	  $from="(	  
		SELECT M.periode,M.nama as rka_nama,K.id as idrka_kegiatan, K.idrka,K.idkegiatan, K.idprogram,K.idprespektif,K.idunit
		,MK.nama as kegiatan,prog.nama as program,MP.nama as prespektif
		,KN.bulan,KN.nominal,KN.st_diajukan,KN.st_setting,KD.tanggal_bayar,KD.st_diajukan as st_diajukan2
		 from mrka_kegiatan K
		 INNER JOIN mrka_kegiatan_nominal KN ON KN.idrka_kegiatan=K.id AND KN.nominal > 0
		 LEFT JOIN mrka_kegiatan_detail KD ON KD.idrka_kegiatan=KN.idrka_kegiatan AND KD.bulan=KN.bulan	
		LEFT JOIN mrka M ON M.id=K.idrka
		LEFT JOIN mkegiatan_rka MK ON MK.id=K.idkegiatan
		LEFT JOIN mprogram_rka prog ON prog.id=K.idprogram
		LEFT JOIN mprespektif_rka MP ON MP.id=K.idprespektif

		WHERE M.`status`='3' ".$where."
		
		AND K.idunit IN (SELECT idunit from munitpelayanan_user_setting US WHERE US.userid='$user_id')
		GROUP BY K.id,KN.bulan,KD.tanggal_bayar
				) as tbl";
	// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('kegiatan');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
			$row[] = $r->idrka_kegiatan;
			$row[] = $r->idrka;
			$row[] = $r->idprespektif;
			$row[] = $r->idprogram;
			$row[] = $r->idkegiatan;
			$row[] = $no;
			if ($r->st_diajukan2 =='1'){
				$status_setting='<span class="label label-primary">SUDAH DIAJUKAN</span>';
			}else{
				$status_setting=status_setting($r->st_setting);
			}
			$row[] = $r->rka_nama;
			if ($r->tanggal_bayar){
				$tanggal_bayar=HumanDateShort($r->tanggal_bayar);
			}else{
				$tanggal_bayar='';
			}
			$row[] = $r->kegiatan.' '.$tanggal_bayar;
			$row[] = $r->program;
			$row[] = $r->prespektif;
			$row[] = MONTHFormat($r->bulan).' '.$r->periode.'<br>'.$status_setting;
			$row[] = number_format($r->nominal,0);
			  
			$aksi = '<div class="btn-group">';
			if ($r->st_diajukan2 =='0'){
				if (UserAccesForm($user_acces_form,array('1389'))){
					$aksi .= '<a href="'.site_url().'mrka_kegiatan/edit_add_kegiatan/'.$r->idrka_kegiatan.'/disabled" data-toggle="tooltip" title="Lihat" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>';			
				}
			if ($r->st_setting == '0'){
				if (UserAccesForm($user_acces_form,array('1427'))){
				$aksi .= '<a href="'.site_url().'mrka_pengajuan/create_rka/'.$r->idrka_kegiatan.'" data-toggle="tooltip" title="Tambah Detail Pengajuan" class="btn btn-danger btn-sm"><i class="si si-arrow-right"></i></a>';			
				}
			}else{
				if (UserAccesForm($user_acces_form,array('1390'))){
				$aksi .= '<a href="'.site_url().'mrka_pengajuan/load_detail_setting/'.$r->idrka_kegiatan.'/'.$r->bulan.'/'.$r->tanggal_bayar.'" data-toggle="tooltip" title="Load Detail" class="btn btn-success btn-sm"><i class="si si-arrow-right"></i></a>';			
				
				}
			}
			}
			$aksi .= '</div>';

			$row[] = $aksi;
			$data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
}
