<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use Dompdf\Dompdf;

class Trefund_kas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Trefund_kas_model', 'model');
        $this->load->model('Tpoliklinik_tindakan_model');
        $this->load->model('Mrka_bendahara_model');
    }
	function insert_validasi_refund($id){
		$this->model->insert_validasi_refund($id);
	}
    public function index()
    {
        $data = array(
            'nomedrec'      => "",
            'namapasien'    => "",
            'tiperefund'    => "#",
            'tanggalawal'   => '',
            'tanggalakhir'  => ''
        );

        $this->session->set_userdata($data);

        $data['error']      = '';
        $data['title']      = 'Refund Proses';
        $data['content']    = 'Trefund_kas/index';
        $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Refund",'#'));

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	
	  $nomedrec=$this->input->post('nomedrec');
	  $namapasien=$this->input->post('namapasien');
	  $tiperefund=$this->input->post('tiperefund');
	  $tanggal_refund_awal=$this->input->post('tanggal_refund_awal');
	  $tanggal_refund_akhir=$this->input->post('tanggal_refund_akhir');
	 
	  $iduser=$this->session->userdata('user_id');
	  $where='';
	  if ($nomedrec !=''){
		  $where .=" AND P.no_medrec='$nomedrec'";
	  }
	  if ($namapasien !=''){
		  $where .=" AND P.nama LIKE '%".$namapasien."%'";
	  }
	  if ($tiperefund !='#'){
		  $where .=" AND H.tipe='$tiperefund'";
	  }
	  if ($tanggal_refund_awal !=''){
		  $where .=" AND H.tanggal >='".YMDFormat($tanggal_refund_awal)."'";
	  }
	  if ($tanggal_refund_akhir !=''){
		  $where .=" AND H.tanggal <='".YMDFormat($tanggal_refund_akhir)."'";
	  }
	  
	  $from="(
				SELECT 
				H.id,H.tanggal,H.norefund,H.notransaksi,H.totalrefund,H.tipe,CASE WHEN H.tipe='0' THEN 'DEPOSIT' WHEN H.tipe='1' THEN 'OBAT' ELSE 'TRANSAKSI' END as tipe_nama
				,P.no_medrec,P.title,P.nama,H.nominal,H.alasan,H.norekening,H.bank,H.metode
				,st_approval,st_bayar,st_transaksi,MAX(AP.step) as step,H.st_verifikasi,COUNT(DISTINCT Doc.id) as doc
				FROM trefund H
				LEFT JOIN trefund_approval AP ON AP.idrefund=H.id AND AP.st_aktif='1'
				LEFT JOIN mfpasien P ON P.id=H.idpasien
				LEFT JOIN trefund_dokumen Doc ON Doc.idtransaksi=H.id
				WHERE H.`status`='1' AND H.metode !='1' ".$where."
				GROUP BY H.id 
				ORDER BY H.id DESC
				) as tbl";
	// print_r($tiperefund);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama','jenis_kas','bank');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $iduser=$this->session->userdata('user_id');	
      foreach ($list as $r) {
            $no++;
            $row = array();
			$query=$this->get_respon($r->id,$r->step);
            $action = '';
            $status = '';
			if ($r->st_transaksi=='1'){
				if ($r->st_approval=='1'){
					$status='<span class="label label-primary">DISETUJUI</span>';
				}
				if ($r->st_approval=='2'){
					$status='<span class="label label-danger">DITOLAK</span>';
				}
				if ($r->st_approval=='0'){
					$status='<span class="label label-success">PROSES PERSETUJUAN</span> <button title="Lihat User" class="btn btn-danger btn-xs user"><i class="si si-user"></i></button>';
				}
				if ($r->st_bayar=='1'){
					$status='<span class="label label-success">TELAH DITRANSAKSIKAN</span>';
				}
				
			}elseif($r->st_transaksi=='0'){
				$status='<span class="label label-danger">BELUM DIPROSES</span>';
			}
				$action .= '<a href="'.base_url().'trefund_kas/pembayaran/'.$r->id.'/disabled" class="btn btn-xs btn-default" ><i class="fa fa-eye"></i></a>';
				// $action  .= '<button class="btn btn-xs btn-default lihat-refund" data-idtransaksi="'.$r->id.'" data-norefund="'.$r->norefund.'" data-notransaksi="'.$r->notransaksi.'" data-nominalrefund="'.number_format($r->totalrefund).'"><i class="fa fa-eye	"></i></button>&nbsp;';
				if ($r->st_transaksi=='0'){
					$action  .= '<button class="btn btn-xs btn-primary edit-refund" data-idtransaksi="'.$r->id.'" data-norefund="'.$r->norefund.'" data-notransaksi="'.$r->notransaksi.'" data-nominalrefund="'.number_format($r->totalrefund).'"><i class="fa fa-pencil"></i></button>&nbsp;';
				}
				// $action .= '<a href="'.site_url().'tpenyesuaian_kas/create/'.$r->id.'" data-toggle="tooltip" title="Create" class="btn btn-danger btn-sm"><i class="fa fa-check"></i></a>';
				// $action .= '<a href="'.site_url().'tpenyesuaian_kas/kwitansi/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Cetak Kwitansi" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
				 switch ($r->tipe) {
					case 0:
						$tipe = '<label class="label label-primary text-uppercase">Deposit</label>';
						if (UserAccesForm($user_acces_form, array('1098'))) {
							$action .= '<a href="'.base_url()."Trefund/fakturRefundDeposit/".$r->id.'" class="btn btn-xs btn-success print-refund" target="_blank"><i class="fa fa-print"></i></a>';
						}
						break;
					case 1:
						$tipe = '<label class="label label-success text-uppercase">Retur Obat</label>';
						if (UserAccesForm($user_acces_form, array('1098'))) {
							$action .= '<a href="'.base_url()."Trefund/fakturRefundObat/".$r->id.'" class="btn btn-xs btn-success print-refund" target="_blank"><i class="fa fa-print"></i></a>';
						}
						break;
					case 2:
						$tipe = '<label class="label label-default text-uppercase">Transaksi</label>';
						if (UserAccesForm($user_acces_form, array('1098'))) {
							$action .= '<a href="'.base_url()."Trefund/fakturRefundTransaksi/".$r->id.'" class="btn btn-xs btn-success print-refund" target="_blank"><i class="fa fa-print"></i></a>';
						}
					   break;
				}
			if ($r->st_transaksi=='0' || $r->st_approval=='2'){
				$action .= '<button class="btn btn-xs btn-danger approval" title="Proses Approval" data-tipe_refund="'.$r->tipe.'" data-nominal="'.$r->nominal.'" data-id="'.$r->id.'"><i class="fa fa-send"></i></button>';
			}
			if ($r->st_approval=='1' && $r->st_bayar=='0'){
				$action .= '<a href="'.base_url().'trefund_kas/pembayaran/'.$r->id.'" class="btn btn-xs btn-primary bayar" title="Proses Pembayaran" data-tipe_refund="'.$r->tipe.'" data-nominal="'.$r->nominal.'" data-id="'.$r->id.'"><i class="fa fa-credit-card"></i></a>';
			}
			if ($r->st_verifikasi=='0' && $r->st_bayar=='1'){
				$action .= '<a href="'.base_url().'trefund_kas/pembayaran/'.$r->id.'" class="btn btn-xs btn-default bayar" title="Proses Pembayaran" data-tipe_refund="'.$r->tipe.'" data-nominal="'.$r->nominal.'" data-id="'.$r->id.'"><i class="fa fa-credit-card"></i></a>';
				if ($r->tipe !='2'){
				$action .= '<button class="btn btn-xs btn-primary verif" title="Proses Verifikasi"><i class="fa fa-check"></i> Verifikasi</button>';
					
				}
			}
			if ($r->doc){
				$action .= '<a href="'.base_url().'trefund_kas/upload_document/'.$r->id.'" title="Upload Document" class="btn btn-xs btn-warning" ><i class="fa fa-file-image-o"></i></a>';
			}else{
				$action .= '<a href="'.base_url().'trefund_kas/upload_document/'.$r->id.'" title="Upload Document" class="btn btn-xs btn-danger" ><i class="fa fa-upload"></i></a>';
				  
			}
			$respon='';
			if ($query){
			foreach ($query as $res){
				if ($res->iduser==$iduser && $res->step==$r->step){
					if ($res->approve=='0'){
					  $action .= '<button title="Setuju" class="btn btn-primary btn-xs setuju" data-id="'.$res->id.'"><i class="fa fa-check"></i> Setuju</button>';
					  $action .= '<button title="Tolak" class="btn btn-danger btn-xs tolak" data-id="'.$res->id.'"><i class="si si-ban"></i></button>';
					}else{
						
					  // $action .= '<button title="Batalkan Pesetujuan / Tolak" class="btn btn-primary btn-xs batal" data-id="'.$res->id.'"><i class="fa fa-refresh"></i></button>';
					}
				}
				if ($res->iduser==$iduser){
					$respon .='<span class="label label-warning" data-toggle="tooltip">STEP '.$res->step.'</span> : '.status_approval($res->approve).'<br>';
				}
			}
			  
			  
		  }else{
			   $respon='';
		  }
		  if ($r->st_bayar=='1'){
			  if ($r->st_verifikasi=='1'){
				  $respon ='<span class="label label-success" data-toggle="tooltip">SUDAH DIVERIFIKASI</span>';
			  }else{
				  $respon ='<span class="label label-danger" data-toggle="tooltip">BELUM DIVERIFIKASI</span>';
			  }
		  }
			// $query=$this->get_respon($r->id,$r->step);
            $row[] = $r->id;
            $row[] = $no;
  			$row[] = HumanDateLong($r->tanggal);
  			$row[] = $tipe;
  			$row[] = $r->norefund;
  			$row[] = $r->no_medrec;
  			$row[] = $r->nama;
            $row[] = number_format($r->nominal);  			
  			$row[] = $r->alasan;
  			$row[] = metode_pembayaran_label($r->metode);
  			$row[] = $r->bank;
  			$row[] = $r->norekening;
  			$row[] = $status.'<br><br>'.$respon;
            $row[] = '<div class="btn-group">'.$action.'</div>';
            $data[] = $row;
        }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function get_respon($idrefund,$step){
	  $iduser=$this->session->userdata('user_id');
	  $q="SELECT *from trefund_approval AP
			WHERE AP.st_aktif='1' AND AP.iduser='$iduser' AND AP.idrefund='$idrefund'";
	  return $this->db->query($q)->result();
  }
  function setuju_batal($id,$status,$idrefund=''){
		// $arr=array();
		$q="call update_refund('$id', $status) ";
		$result=$this->db->query($q);
		// $arr['st_insert']=$result;
		// $arr=$this->db->query("SELECT H.st_bendahara FROM trefund H  WHERE H.id='$idrka' AND H.status='2'")->row('st_bendahara');
		$this->output->set_output(json_encode($result));
	}
	function verifikasi($id){
		// $arr=array();
		$q="update trefund SET st_verifikasi='1' WHERE id='$id'";
		$result=$this->db->query($q);
		// $arr['st_insert']=$result;
		// $arr=$this->db->query("SELECT H.st_bendahara FROM trefund H  WHERE H.id='$idrka' AND H.status='2'")->row('st_bendahara');
		$this->model->insert_validasi_refund($id);//Validasi
		$this->output->set_output(json_encode($result));
	}
	function tolak(){
		$id=$this->input->post('id_approval');
		$alasan_tolak=$this->input->post('alasan_tolak');
		$q="call update_refund('$id', 2) ";
		$this->db->query($q);
		$result=$this->db->update('trefund_approval',array('alasan_tolak'=>$alasan_tolak),array('id'=>$id));
		echo json_encode($result);
	}
	function load_user_approval()
    {
	  $tipe_refund=$this->input->post('tipe_refund');	 
	  $nominal_trx=$this->input->post('nominal_trx');	 
	  $where='';
	  $from="(
				SELECT S.step,U.`name` as user_nama,S.proses_setuju,S.proses_tolak FROM mlogic_refund S
				LEFT JOIN musers U ON U.id=S.iduser
				WHERE S.tipe_refund='$tipe_refund' AND calculate_logic(S.operand, ".$nominal_trx.", S.nominal)='1'
				ORDER BY S.step,S.id
				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('user_nama');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $no_step='';
      foreach ($list as $r) {
          $no++;
          $row = array();
		  if ($no_step != $r->step){
				$no_step=$r->step;
		  }
		  if ($no_step % 2){		 
			$row[] = '<span class="label label-default">STEP '.$r->step.'</span>';
		  }else{
			   $row[] = '<span class="label label-primary">STEP '.$r->step.'</span>';
		  }
          $row[] = $r->user_nama;
          $row[] = ($r->proses_setuju=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
          $row[] = ($r->proses_tolak=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
         
          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function list_user($id){
		$q="SELECT *FROM trefund_approval H WHERE H.idrefund='$id'";
		$row=$this->db->query($q)->result();
		$content='';
		$no=1;
		$no_step="1";
		foreach($row as $r){
			$st_user_lain='';
			$content .='<tr class="'.($r->st_aktif=='1' && $r->approve=='0'?'success':'active').'">';
			if ($no_step != $r->step){
				$no_step=$r->step;
			}
			if ($no_step % 2){
				$content .='<td> <span class="label label-default" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
			}else{
				$content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span></td>';
			}
			if ($r->approve !='0'){
				if ($r->iduser !=$r->user_eksekusi){
					$st_user_lain=' <span class="label label-warning" data-toggle="tooltip">Oleh User Lain</span>';
				}
			}
			$content .='<td><i class="si si-users fa-2x text-black-op"></i>  '.$r->user_nama.'</td>';
			$content .='<td>'.($r->st_aktif=='1'?status_approval($r->approve):'').' '.$st_user_lain.'</td>';			
			$content .='</tr>';
			
		}
		$arr['detail']=$content;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($content));
	}
  function simpan_proses_peretujuan($id){
		
		$result=$this->model->simpan_proses_peretujuan($id);
		echo json_encode($result);
	}
    public function pembayaran($id,$disabel='')
    {
		$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d'));
		date_add($date1,date_interval_create_from_date_string("-1 days"));
		date_add($date2,date_interval_create_from_date_string("15 days"));
		$date1= date_format($date1,"Y-m-d");
		$date2= date_format($date2,"Y-m-d");

		// print_r($date1);exit();
		$data=$this->model->get_refund($id);
		// print_r($data);exit();
        $data['list_pembayaran']      = $this->model->list_pembayaran($id);
		$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
        $data['date1']      = $date1;
        $data['st_edit']      = '0';
        $data['error']      = '';
        $data['title']      = 'Pembayaran';
        $data['disabel']      = $disabel;
        $data['content']    = 'Trefund_kas/manage';
        $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Refund",'#'), array("Tambah",'#'));

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filter()
    {
        $data = array(
            'nomedrec'      => $this->input->post('nomedrec'),
            'namapasien'    => $this->input->post('namapasien'),
            'tiperefund'    => $this->input->post('tiperefund'),
            'tanggalawal'   => $this->input->post('tanggal_refund_awal'),
            'tanggalakhir'  => $this->input->post('tanggal_refund_akhir')
        );

        $this->session->set_userdata($data);

        $data['error']      = '';
        $data['title']      = 'Refund';
        $data['content']    = 'Trefund_kas/index';
        $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Refund",'#'));

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	
    
    public function save()
    {
		$id=$this->model->saveData();
		if($id){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('trefund_kas','location');
		}
	}
    public function saveRefund()
    {
        $data = array(
            'tanggal' => date("Y-m-d h:i:s", strtotime(YMDFormat($this->input->post('tanggal')).' '.$this->input->post('jam'))),
            'tipe' => $this->input->post('tiperefund'),
            'tipetransaksi' => $this->input->post('tipetransaksi'),
            'idtransaksi' => $this->input->post('idtransaksi'),
            'notransaksi' => $this->input->post('notransaksi'),
            'idpasien' => $this->input->post('idpasien'),
            'idkelompokpasien' => $this->input->post('idkelompokpasien'),
            'metode'     => $this->input->post('metode'),
            'totaltransaksi'  => RemoveComma($this->input->post('totaltransaksi')),
            'totalpembayaran'  => RemoveComma($this->input->post('totalpembayaran')),
            'totaldeposit' => RemoveComma($this->input->post('totaldeposit')),
            'totalrefund'  => RemoveComma($this->input->post('totalrefund')),
            'bank' => $this->input->post('bank'),
            'norekening' => $this->input->post('norekening'),
            'nominal'  => RemoveComma($this->input->post('nominal')),
            'alasan' => $this->input->post('alasan'),
            'created_by' => $this->session->userdata('user_id')
        );

        if ($this->db->insert('trefund', $data)) {
            $detail = json_decode($this->input->post('detail'));
            $idrefund = $this->db->insert_id();

            foreach ($detail as $row) {
                $data = array();
                $data['idrefund'] = $idrefund;
                if ($this->input->post('tiperefund') != '2') {
                    $data['reference'] = $row[0];
                    $data['idtransaksi'] = $row[1];
                } else {
                    $data['reference'] = $row->reference;
                    $data['idtransaksi'] = $row->iddetail;
                    $data['tanggal'] = $row->tanggal;
                    $data['tipe'] = $row->tipe;
                    $data['tindakan'] = $row->tindakan;
                    $data['kuantitas'] = $row->kuantitas;
                    $data['harga'] = $row->harga;
                    $data['totalharga'] = $row->total;
                }

                $this->db->insert('trefund_detail', $data);
            }

            $status	= ['status' => 200];
            $this->output->set_output(json_encode($status));
        }
    }

    public function saveRefundTransaksi()
    {
        $detail = $this->input->post('details');

        $data = array(
            'idtransaksi' => $this->input->post('idtransaksi'),
            'tanggal' => date("Y-m-d", strtotime($this->input->post('tanggal_refund'))),
            'noregistrasi' => $this->input->post('no_registrasi'),
            'nomedrec' => $this->input->post('no_medrec'),
            'tipe_refund' => $this->input->post('tipe_refund'),
            'metode_refund' => $this->input->post('metode_refund'),
            'bank_refund' => $this->input->post('bank_refund'),
            'norek_refund' => $this->input->post('norek_refund'),
            'nominal_refund' => $this->input->post('nominal_refund'),
            'total' => 0,
            'total_transaksi' => 0,
            'alasan' => $this->input->post('alasan'),
            'user_created' => $this->session->userdata('user_id'),
            'status' => 1
        );

        $result = $this->model->saveRefundTransaksi($data, $detail);

        if ($result) {
            $status = ['status' => 200];
            $this->output->set_output(json_encode($status));
        }
    }

    public function removeRefund($id)
    {
        $alasan = $this->input->post('alasan');
        $result = $this->model->removeRefund($id, $alasan);

        if ($result) {
            $status	= ['status' => 200];
            $this->output->set_output(json_encode($status));
        }
    }

    function updatePembayaranRefund()
    {
        $this->metode = $this->input->post('metode');
        $this->bank = $this->input->post('bank');
        $this->norekening = $this->input->post('norekening');
        $this->alasan = $this->input->post('alasan');

        if ($this->db->update('trefund', $this, array('id' =>$this->input->post('idtransaksi')))) {
            $status	= ['status' => 200];
            $this->output->set_output(json_encode($status));
        }
    }

    public function searchList()
    {
        $tiperefund = $this->input->post('tiperefund');
        $tanggalawal = YMDFormat($this->input->post('tanggalawal'));
        $tanggalakhir = YMDFormat($this->input->post('tanggalakhir'));
        $idtipetransaksi = $this->input->post('idtipetransaksi');

        $data = array();

        if ($tiperefund == '0') {
            $result = $this->model->getRefundDeposit($tanggalawal, $tanggalakhir, $idtipetransaksi);
        } elseif ($tiperefund == '1') {
            $result = $this->model->getRefundObat($tanggalawal, $tanggalakhir, $idtipetransaksi);
        } elseif ($tiperefund == '2') {
            $result = $this->model->getRefundTransaksi($tanggalawal, $tanggalakhir, $idtipetransaksi);
        }

        foreach ($result as $row) {
            $rows = array();

            $rows[] = '<label class="label label-primary text-uppercase">' . $row->tipetransaksi. '</label>';
            ;
            $rows[] = $row->notransaksi;
            $rows[] = $row->nomedrec;
            $rows[] = $row->namapasien;
            $rows[] = $row->namakelompokpasien;
            $rows[] = $row->namadokter;

            if ($tiperefund == '2') {
                $rows[] = '<button class="btn btn-success btn-xs text-uppercase selectTransaksi" style="font-size:10px;" data-reference="'.$row->reference.'" data-idtransaksi="'.$row->id.'">Pilih Transaksi</button>';
            } else {
                $rows[] = '<button class="btn btn-success btn-xs text-uppercase selectTransaksi" style="font-size:10px;" data-idtransaksi="'.$row->id.'">Pilih Transaksi</button>';
            }

            $data[] = $rows;
        }

        $output = array(
          "recordsTotal" => count($result),
          "recordsFiltered" => count($result),
          "data" => $data
        );

        echo json_encode($output);
    }

    public function getInfoRefund($id)
    {
        $row = $this->model->getInfoRefund($id);

        $this->output->set_output(json_encode($row));
    }

    public function fakturRefundDeposit($id)
    {
        $dompdf = new Dompdf();
        $row = $this->model->getInfoRefund($id);

        $data = array(
            'notransaksi' 		 => $row->notransaksi,
            'tanggaltransaksi' => $row->tanggaltransaksi,
            'norefund' 		     => $row->norefund,
            'tanggalrefund'	   => $row->tanggalrefund,
            'tiperefund' 		   => $row->tiperefund,
            'nomedrec' 		     => $row->nomedrec,
            'namapasien' 		   => $row->namapasien,
            'kelompokpasien'   => $row->kelompokpasien,
            'alasan'           => $row->alasan,
            'idmetode'         => $row->idmetode,
            'metode'           => $row->metode,
            'bank'             => $row->bank,
            'norekening'       => $row->norekening,
            'totaltransaksi'   => $row->totaltransaksi,
            'totaldeposit'     => $row->totaldeposit,
            'totalrefund'      => $row->totalrefund,
        );

        $data['list_deposit'] = $this->model->getFakturRefundDeposit($id);

        $html = $this->load->view('Trefund_kas/print/faktur_refund_deposit', $data, true);

        $dompdf->loadHtml($html);

        $customPaper = array(0,0,480,600);
        $dompdf->setPaper($customPaper);

        // $dompdf->setPaper('A3', 'portrait');
        $dompdf->render();
        $dompdf->stream('Faktur Refund Deposit.pdf', array("Attachment"=>0));
    }

    public function fakturRefundObat($id)
    {
        $dompdf = new Dompdf();
        $row = $this->model->getInfoRefund($id);

        $data = array(
            'notransaksi' 		 => $row->notransaksi,
            'tanggaltransaksi' => $row->tanggaltransaksi,
            'norefund' 		     => $row->norefund,
            'tanggalrefund'	   => $row->tanggalrefund,
            'tiperefund' 		   => $row->tiperefund,
            'nomedrec' 		     => $row->nomedrec,
            'namapasien' 		   => $row->namapasien,
            'kelompokpasien'   => $row->kelompokpasien,
            'alasan'           => $row->alasan,
            'idmetode'         => $row->idmetode,
            'metode'           => $row->metode,
            'bank'             => $row->bank,
            'norekening'       => $row->norekening,
            'totaltransaksi'   => $row->totaltransaksi,
            'totaldeposit'     => $row->totaldeposit,
            'totalrefund'      => $row->totalrefund,
        );

        $data['list_obat'] = $this->model->getFakturRefundObat($id);

        $html = $this->load->view('Trefund_kas/print/faktur_refund_obat', $data, true);

        $dompdf->loadHtml($html);

        $customPaper = array(0,0,480,600);
        $dompdf->setPaper($customPaper);

        // $dompdf->setPaper('A3', 'portrait');
        $dompdf->render();
        $dompdf->stream('Faktur Refund Deposit.pdf', array("Attachment"=>0));
    }

    public function fakturRefundTransaksi($id)
    {
        $dompdf = new Dompdf();
        $row = $this->model->getInfoRefund($id);

        $data = array(
            'notransaksi' 		 => $row->notransaksi,
            'tanggaltransaksi' => $row->tanggaltransaksi,
            'norefund' 		     => $row->norefund,
            'tanggalrefund'	   => $row->tanggalrefund,
            'tiperefund' 		   => $row->tiperefund,
            'nomedrec' 		     => $row->nomedrec,
            'namapasien' 		   => $row->namapasien,
            'kelompokpasien'   => $row->kelompokpasien,
            'alasan'           => $row->alasan,
            'idmetode'         => $row->idmetode,
            'metode'           => $row->metode,
            'bank'             => $row->bank,
            'norekening'       => $row->norekening,
            'totaltransaksi'   => $row->totaltransaksi,
            'totaldeposit'     => $row->totaldeposit,
            'totalrefund'      => $row->totalrefund,
        );

        $data['list_transaksi'] = $this->model->getFakturRefundTransaksi($id);

        $html = $this->load->view('Trefund_kas/print/faktur_refund_transaksi', $data, true);

        $dompdf->loadHtml($html);

        $customPaper = array(0,0,480,600);
        $dompdf->setPaper($customPaper);

        // $dompdf->setPaper('A3', 'portrait');
        $dompdf->render();
        $dompdf->stream('Faktur Refund Deposit.pdf', array("Attachment"=>0));
    }

    // Refund Deposit
    public function getRefundDeposit()
    {
        $empty  = (object)array('id' => '', 'text' => '');
        $result = $this->model->getRefundDeposit();
        array_unshift($result, $empty);

        $this->output->set_output(json_encode($result));
    }

    public function getInfoRefundDeposit($id)
    {
        $row = $this->model->getInfoRefundDeposit($id);

        $this->output->set_output(json_encode($row));
    }

    // Refund Retur Obat Rawat Jalan
    public function getRefundObat()
    {
        $empty  = (object)array('id' => '', 'text' => '');
        $result = $this->model->getRefundObat();
        array_unshift($result, $empty);

        $this->output->set_output(json_encode($result));
    }

    public function getInfoRefundObat($id)
    {
        $row = $this->model->getInfoRefundObat($id);

        $this->output->set_output(json_encode($row));
    }

    public function getDetailRefundObat($id)
    {
        $row = $this->model->getDetailRefundObat($id);

        $this->output->set_output(json_encode($row));
    }

    // Refund Transaksi Rawat Jalan & Rawat Inap
    public function getRefundTransaksi()
    {
        $empty  = (object)array('id' => '', 'text' => '');
        $result = $this->model->getRefundTransaksi();
        array_unshift($result, $empty);

        $this->output->set_output(json_encode($result));
    }

    public function getInfoRefundTransaksi($reference, $id)
    {
        $row = $this->model->getInfoRefundTransaksi($reference, $id);

        $this->output->set_output(json_encode($row));
    }

    public function getDetailRefundTransaksiRawatJalan($id)
    {
        $rowTindakan = $this->Tpoliklinik_tindakan_model->getSpecifiedTindakan($id);

        $data = array();
        $data['idpendaftaran'] = $id;
        $data['listPelayanan']	= $this->Tpoliklinik_tindakan_model->getListPelayanan($rowTindakan->id);
        $data['listObat']	= $this->Tpoliklinik_tindakan_model->getListObat($rowTindakan->id);
        $data['listAlkes']	= $this->Tpoliklinik_tindakan_model->getListAlkes($rowTindakan->id);
        $data['listLaboratorium']	= $this->model->getListLaboratorium($rowTindakan->idpendaftaran);
        $data['listRadiologi']	= $this->model->getListRadiologi($rowTindakan->idpendaftaran);
        $data['listFisioterapi']	= $this->model->getListFisioterapi($rowTindakan->idpendaftaran);
        $data['listFarmasi']	= $this->model->getListFarmasi($rowTindakan->idpendaftaran);
        $data['listAdministrasi']	= $this->model->getListAdministrasi($rowTindakan->idpendaftaran);

        $this->load->view('Trefund_kas/transaksi_rawatjalan', $data);
    }

    public function getDetailRefundTransaksiRawatInap($id)
    {
        $data = array();
        $data['idpendaftaran'] = $id;

        $this->load->view('Trefund_kas/transaksi_rawatinap', $data);
    }
	public function upload_document($idtransaksi)
    {
       
        $data = $this->model->getSpecifiedHeader($idtransaksi);
		// print_r($data);exit();
       

        $data['error']      = '';
        $data['title'] = 'Upload Dokumen Refund';
        $data['content'] = 'Trefund_kas/upload_dokumen';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Refund", 'Trefund_kas/index'),
            array("Upload Dokumen", '#')
        );

        // $data['listFiles'] = array();
        $data['listFiles'] = $this->model->getListUploadedDocument($idtransaksi);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	public function upload_files()
    {
        $files = array();
        foreach ($_FILES as $file) {
            if ($file['name'] != '') {
                $config['upload_path'] = './assets/upload/refund/';
                $config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';

                $files[] = $file['name'];

                $this->upload->initialize($config);

                if ($this->upload->do_upload('file')) {
                    $image_upload = $this->upload->data();
                    $table = 'trefund_dokumen';

                    $data = array();
                    $data['idtransaksi']  = $this->input->post('idtransaksi');
                    $data['keterangan']  = $this->input->post('keterangan');
                    $data['upload_by']  = $this->session->userdata('user_id');
                    $data['upload_by_nama']  = $this->session->userdata('user_name');
                    $data['upload_date']  = date('Y-m-d H:i:s');
                    $data['filename'] = $image_upload['file_name'];
                    $data['size'] = formatSizeUnits($file['size']);

                    $this->db->insert($table, $data);

                    return true;
                } else {
                    print_r($this->upload->display_errors());
                    exit();

                    return false;
                }
            } else {
                return true;
            }
        }
    }
	function refresh_image($id){		
		$arr['detail'] = $this->model->refresh_image($id);
		$this->output->set_output(json_encode($arr));
	}
	 public function delete_file($idfile)
    {
        $this->db->where('id', $idfile);
        $query = $this->db->get('trefund_dokumen');
        $row = $query->row();
        if ($query->num_rows() > 0) {
            $this->db->where('id', $idfile);
            if ($this->db->delete('trefund_dokumen')) {
                if (file_exists('./assets/upload/refund/'.$row->filename) && $row->filename !='') {
                    unlink('./assets/upload/refund/'.$row->filename);
                }
            }
        }

        return true;
    }
}
