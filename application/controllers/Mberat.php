<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mberat extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mberat_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1621'))){
			
			$data['tab'] 			= $tab;
			$data['spesialisasi_id'] 			= '#';
			$data['profesi_id'] 			= '#';
			$data['mppa_id'] 			= '#';
			$data['st_lihat'] 			= '0';
			$data['st_input'] 			= '0';
			$data['st_edit'] 			= '0';
			$data['st_hapus'] 			= '0';
			$data['st_cetak'] 			= '0';
			$data['error'] 			= '';
			$data['title'] 			= 'Referensi Berat Badan Ideal';
			$data['content'] 		= 'Mberat/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Referensi Berat Badan Ideal",'mberat')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	

  function simpan_berat(){
		$berat_id=$this->input->post('berat_id');
		$data=array(
			'berat_1'=>$this->input->post('berat_1'),
			'berat_2'=>$this->input->post('berat_2'),
			'kategori_berat'=>$this->input->post('kategori_berat'),
			'warna'=>$this->input->post('warna'),
			'staktif'=>1,
		);
		if ($berat_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mberat',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$berat_id);
		    $hasil=$this->db->update('mberat',$data);
		}
		  
		  json_encode($hasil);
	}
	
	function load_berat()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
							FROM `mberat` H
							where H.staktif='1'
							ORDER BY H.berat_1


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('berat_1','berat_1','kategori_berat');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->berat_1.' - '.$r->berat_2);
          $result[] = $r->kategori_berat;
          $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'" type="button" >'.$r->warna.'</button>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('1623'))){
		  $aksi .= '<button onclick="edit_berat('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		   if (UserAccesForm($user_acces_form,array('1624'))){
		  $aksi .= '<button onclick="hapus_berat('.$r->id.')" type="button" title="Hapus Suhu" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_berat(){
	  $berat_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$berat_id);
		$hasil=$this->db->update('mberat',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_berat(){
	  $berat_id=$this->input->post('id');
	  $q="SELECT *FROM mberat H WHERE H.id='$berat_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
  
}
