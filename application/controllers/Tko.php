<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
// 0 : DiBatalkan
// 1 :
// 2 : Pasien Sudah Datang
// 3 : Siap Operasi
// 4 : Sudah Dioperasi
// 5 : Belum Datang

// Developer : @Muhamad Ali Nurdin (&Kamar Operasi)
class Tko extends CI_Controller
{
    // @
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->load->model('Tko_model', 'model');
        $this->load->model('Mtarif_sewaalat_model');
    }

    // @1
    public function index()
    {
		
        $data = array(
            'nomedrec' => '',
            'namapasien' => '',
            'asalpasien' => '',
            'status' => '',
            'tanggaldari' => DMYFormat2(date('Y-m-d')),
            'tanggalsampai' => DMYFormat2(date('Y-m-d')),
            'dokter' => '',
            'filter_jam' => '',
            'idtipe' => '',
            'waktu' => date("H:i"),
            'dokter' => '',
            'filter_jam' => '',
        );

        $this->session->set_userdata($data);

        $data['dokterumum'] = $this->model->getdokterumum();
        $data['kelas_list'] = $this->model->kelas_list();
        $data['mjenis_operasi'] = $this->model->mjenis_operasi();
        $data['mruangan'] = $this->model->mruangan();
        $data['mkelas'] = $this->model->mkelas();
        $data['mkelompok_operasi'] = $this->model->mkelompok_operasi();
        $data['mjenis_anaesthesi'] = $this->model->mjenis_anaesthesi();

        $data['error'] = '';
        $data['title'] = 'Kamar Operasi';
        $data['content'] = 'Tko/index';
        $data['breadcrum'] = array(array("RSKB Halmahera", '#'), array("Kamar Operasi", '#'));
        $data['petugasko'] = $this->model->getpetugasko();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
    public function get_tarif_edit($jenis, $kelas)
    {
        $q="SELECT H.id,H.nama,M.nama as kelas,H.idtipe,H.idjenis,D.bhp,D.biayaperawatan,D.jasapelayanan,D.jasasarana,D.total from mtarif_operasi H
			LEFT JOIN mtarif_operasi_detail D ON D.idtarif=H.id
			LEFT JOIN mkelas M ON M.id=D.kelas
			WHERE H.idtipe='3' AND H.`status`='1' AND H.idjenis='$jenis' AND D.kelas='$kelas'";

        $arr= $this->db->query($q)->result();
        $this->output->set_output(json_encode($arr));
    }

    public function tindakan($id, $st_lock='0')
    {
        $data=$this->model->get_tindakan($id);
		// print_r($data);exit();
        $data['tarif_default'] = $this->model->get_tarif_default($data['jenis_operasi_id'], $data['kelas_tarif_id']);

        // print_r($data['idkelompokpasien']);exit();
        $this->session->set_userdata($data);

        $data['list_dokter_anestesi'] = $this->model->list_dokter_anestesi();
        $data['unitpelayananiddefault'] = $this->model->get_default();
        $data['dokterumum'] = $this->model->getdokterumum();
        $data['kelas_list'] = $this->model->kelas_list();
        $data['mjenis_operasi'] = $this->model->mjenis_operasi_tindakan($data['idkelompokpasien'], $data['idrekanan']);
        // print_r($data['mjenis_operasi']);exit();
        $data['mruangan'] = $this->model->mruangan();
        $data['mkelas'] = $this->model->mkelas();
        $data['mkelompok_operasi'] = $this->model->mkelompok_operasi();
        $data['mjenis_anaesthesi'] = $this->model->mjenis_anaesthesi();
        $data['list_instrumen'] = $this->model->list_instrumen();
        $data['list_unit'] = $this->model->list_unit();
        // $data['list_alat'] = $this->model->list_alat();
        $data['st_lock'] = $data['statuskasir'];
        $data['list_alat'] = $this->model->list_alat();
        $data['kelompok_tindakan_list'] = $this->model->kelompok_tindakan_list($id);
        $data['kelompok_diagnosa_list'] = $this->model->kelompok_diagnosa_list($id);

        $data['tind_dokter_operator'] = $this->model->tind_dokter_operator($id);
        // print_r($data['tind_dokter_operator']);exit();

        $data['tind_dokter_anatesi'] = $this->model->tind_dokter_anatesi($id);
        $data['tind_daa'] = $this->model->tind_daa($id);
        $data['tind_dao'] = $this->model->tind_dao($id);
        $data['tind_konsulen'] = $this->model->tind_konsulen($id);
        $data['tind_instrumen'] = $this->model->tind_instrumen($id);
        $data['tind_sirkuler'] = $this->model->tind_sirkuler($id);
        $data['tind_narcose'] = $this->model->tind_narcose($id);
        $data['tind_obat'] = $this->model->tind_obat($id);
        $data['tind_alkes'] = $this->model->tind_alkes($id);
        $data['tind_implan'] = $this->model->tind_implan($id);
        $data['tind_sewa'] = $this->model->tind_sewa($id);

        $data['error'] = '';
        $data['title'] = 'Tindakan Kamar Operasi';
        $data['content'] = 'Tko/manage';
        $data['breadcrum'] = array(array("RSKB Halmahera", '#'), array("Kamar Operasi", '#'));
        $data['petugasko'] = $this->model->getpetugasko();
        // print_r($data);exit();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
    public function get_data_obat()
    {
        $iddariunit     = $this->input->post('idunit');
        $idtipe     	= $this->input->post('idtipe');
        $idkategori    = $this->input->post('idkategori');
        $iduser=$this->session->userdata('user_id');

        $where='';
        if ($idtipe  != '#') {
            $where .=" AND S.idtipe='$idtipe'";
        } else {
            $where .=" AND S.idtipe IN( SELECT H.idtipe from munitpelayanan_tipebarang H
										LEFT JOIN mdata_tipebarang T ON H.idtipe=T.id
										WHERE H.idunitpelayanan='1'
					)";
        }
        // if ($idkategori  != ''){

        // $where .=" AND B.idkategori IN (
        // SELECT id FROM (
        // SELECT *FROM view_kategori_alkes
        // UNION
        // SELECT *FROM view_kategori_implan
        // UNION
        // SELECT *FROM view_kategori_obat
        // UNION
        // SELECT *FROM view_kategori_logistik
        // ) T1
        // where T1.path like '$idkategori%'
        // )";
        // }
        $from="(SELECT S.idunitpelayanan,S.idtipe,S.idbarang as id,B.nama,B.idkategori,B.kode,B.namatipe,B.catatan,B.stokminimum,B.stokreorder,S.stok as stok_asli,msatuan.nama as satuan from mgudang_stok S
		INNER JOIN view_barang B ON B.idtipe=S.idtipe AND B.id=S.idbarang
		LEFT JOIN msatuan ON msatuan.id=B.idsatuan
		WHERE S.idunitpelayanan='$iddariunit' ".$where." ORDER BY B.idtipe,B.nama) as tbl";

        // print_r($from);

        $this->select = array();
        $this->join 	= array();
        $this->where  = array();

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        // print_r($from);exit();
        $this->column_search   = array('kode','nama','satuan','stok_asli','catatan');
        $this->column_order    = array('kode','nama','satuan','stok_asli','catatan');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        // print_r($list);exit();
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->kode;
            $row[] = "<a href='#' class='selectObat2' data-idobat='".$r->id."' data-idtipe='".$r->idtipe."' data-dismiss='modal'>".$r->nama."</span></a>";
            $row[] = $r->satuan;

            $row[] = WarningStok($r->stok_asli, $r->stokreorder, $r->stokminimum);
            $row[] = $r->catatan;
            $aksi       = '<div class="btn-group">';
            $aksi 		.= '<a target="_blank" href="'.site_url().'lgudang_stok/kartustok/'.$r->idtipe.'/'.$r->id.'/'.$r->idunitpelayanan.'" class="btn btn-xs btn-danger ks" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';
            $aksi.='</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->datatable->count_all(),
      "recordsFiltered" => $this->datatable->count_all(),
      "data" => $data
    );
        echo json_encode($output);
    }

    public function get_data_sewa($idkelompokpasien)
    {
        $data_user=get_acces();
        $user_acces_form=$data_user['user_acces_form'];

        if ($idkelompokpasien == 1) {
            // $this->db->select('mtarif_laboratorium.path');
            // $this->db->join('mrekanan', 'mrekanan.tlaboratorium_umum = mtarif_laboratorium.id');
            // $this->db->where('mrekanan.id', $idrekanan);
            // $query = $this->db->get('mtarif_laboratorium');
            //
            // if ($query->num_rows() > 0) {
            //     $row = $query->row();
            // } else {
                $this->db->select('mtarif_operasi_sewaalat.path');
                $this->db->join('mpasien_kelompok', 'mpasien_kelompok.toperasi_sewaalat = mtarif_operasi_sewaalat.id');
                $this->db->where('mpasien_kelompok.id', $idkelompokpasien);
                $query = $this->db->get('mtarif_operasi_sewaalat');
                $row = $query->row();
            // }
        } else {
            $this->db->select('mtarif_operasi_sewaalat.path');
            $this->db->join('mpasien_kelompok', 'mpasien_kelompok.toperasi_sewaalat = mtarif_operasi_sewaalat.id');
            $this->db->where('mpasien_kelompok.id', $idkelompokpasien);
            $query = $this->db->get('mtarif_operasi_sewaalat');
            $row = $query->row();
        }

        $this->select = array();
        $this->from   = 'mtarif_operasi_sewaalat';
        $this->join 	= array();

        $this->where  = array(
          'path LIKE' => $row->path.'%',
          'status' => '1'
        );

        $this->order  = array(
          'path' => 'ASC'
        );

        $this->group  = array();

        if (UserAccesForm($user_acces_form, array('174'))) {
            $this->column_search   = array('nama');
        } else {
            $this->column_search   = array();
        }
        $this->column_order    = array('nama');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $r->id;
            $row[] = $no;
            $row[] = TreeView($r->level, $r->nama);
            if ($r->idkelompok=='1') {
                $aksi='';
            } else {
                $aksi = '<div class="btn-group">';
                $aksi .= '<button title="Pilih" class="btn btn-success btn-sm pilih_sewa"><i class="fa fa-check"></i> Pilih</button>';
                $aksi .= '</div>';
            }
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->datatable->count_all(),
                "recordsFiltered" => $this->datatable->count_all(),
                "data" => $data
            );
        echo json_encode($output);
    }
    public function setuju_tolak()
    {
        $id     = $this->input->post('id');
        $status     = $this->input->post('status');
        if ($status=='1') {
            $idapprove=$this->session->userdata('user_id');
            $tanggal_disetujui=date('Y-m-d H:s:i');
            $q="UPDATE tkamaroperasi_pendaftaran SET statussetuju='$status',idapprove='$idapprove',tanggal_disetujui='$tanggal_disetujui' WHERE id='$id'";
        } else {
            $idtolak=$this->session->userdata('user_id');
            $tanggal_tolak=date('Y-m-d H:s:i');
            $q="UPDATE tkamaroperasi_pendaftaran SET statussetuju='$status',idtolak='$idtolak',tanggal_tolak='$tanggal_tolak' WHERE id='$id'";
        }

        $result = $this->db->query($q);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    public function batalkan()
    {
        $id     = $this->input->post('id');

        // $idtolak=$this->session->userdata('user_id');
        // $tanggal_tolak=date('Y-m-d H:s:i');
        $q="UPDATE tkamaroperasi_pendaftaran SET status='0' WHERE id='$id'";



        $result = $this->db->query($q);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    public function simpan_add()
    {
        $data=array(
            'idasalpendaftaran'=>'2',
            'idpendaftaran'=>$this->input->post('idpendaftaran'),
            'tipe'=>$this->input->post('idasalpendaftaran'),
            'idpasien'=>$this->input->post('idpasien'),
            'namapasien'=>$this->input->post('namapasien'),
            'tanggaloperasi'=>YMDFormat($this->input->post('tanggaloperasi')),
            'waktumulaioperasi'=>$this->input->post('waktumulaioperasi'),
            'waktuselesaioperasi'=>$this->input->post('waktuselesaioperasi'),
            'diagnosa'=>$this->input->post('diagnosa'),
            'operasi'=>$this->input->post('operasi'),
            'jenis_operasi_id'=>$this->input->post('jenis_operasi_id'),
            'jenis_anestesi_id'=>$this->input->post('jenis_anestesi_id'),
            'kelompok_operasi_id'=>$this->input->post('kelompok_operasi_id'),
            // 'kelompok_tindakan_id'=>$this->input->post('kelompok_tindakan_id'),
            'kelas_tarif_id'=>$this->input->post('kelas_tarif_id'),
            'ruang_id'=>$this->input->post('ruang_id'),
            // 'kelompk_diagnosa_id'=>$this->input->post('kelompk_diagnosa_id'),
            'namapetugas'=>$this->session->userdata('user_name'),
            'idapprove'=>$this->session->userdata('user_id'),
            'catatan'=>$this->input->post('catatan'),
            'dpjp_id'=>$this->input->post('dpjp_id'),
            'statussetuju'=>'1',
            'statusdatang'=>'1',
            'status'=>'1',
            'tanggal_diajukan'=>date('Y-m-d H:i:s'),
            'tanggal_disetujui'=>date('Y-m-d H:i:s'),
        );

        $kelompok_tindakan_id=$this->input->post('kelompok_tindakan_id');
        $kelompk_diagnosa_id=$this->input->post('kelompk_diagnosa_id');

        // print_r($data);
        $result = $this->db->insert('tkamaroperasi_pendaftaran', $data);
        $id_insert=$this->db->insert_id();
        ;
        if ($result) {
            if ($kelompok_tindakan_id) {
                $arr=$kelompok_tindakan_id;
                foreach ($arr as $value) {
                    // $print_r($value);exit();
                    $data_tindakan=array(
                        'idpendaftaranoperasi'=>$id_insert,
                        'kelompok_tindakan_id'=>$value,
                    );
                    $this->db->insert('tkamaroperasi_tindakan', $data_tindakan);
                }
            }
            if ($kelompk_diagnosa_id) {
                $arr=$kelompk_diagnosa_id;
                foreach ($arr as $value) {
                    // $print_r($value);exit();
                    $data_tindakan=array(
                        'idpendaftaranoperasi'=>$id_insert,
                        'kelompok_diagnosa_id'=>$value,
                    );
                    $this->db->insert('tkamaroperasi_diagnosa', $data_tindakan);
                }
            }

            return true;
        } else {
            return false;
        }
    }
    public function simpan_gabung()
    {
        $id=$this->input->post('idjadwal');
        $data=array(
            'tipe'=>$this->input->post('idtipe'),
            'idasalpendaftaran'=>$this->input->post('idasalpendaftaran'),
            'idpendaftaran'=>$this->input->post('idpendaftaran'),
            'idpasien'=>$this->input->post('idpasien'),
        );

        // print_r($data);
        $this->db->where('id', $id);
        $result = $this->db->update('tkamaroperasi_pendaftaran', $data);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    public function simpan_edit()
    {
        $data=array(
            'tanggaloperasi'=>YMDFormat($this->input->post('tanggaloperasi')),
            'waktumulaioperasi'=>$this->input->post('waktumulaioperasi'),
            'waktuselesaioperasi'=>$this->input->post('waktuselesaioperasi'),
            'diagnosa'=>$this->input->post('diagnosa'),
            'operasi'=>$this->input->post('operasi'),
            'jenis_operasi_id'=>$this->input->post('jenis_operasi_id'),
            'jenis_anestesi_id'=>$this->input->post('jenis_anestesi_id'),
            'kelompok_operasi_id'=>$this->input->post('kelompok_operasi_id'),
            // 'kelompok_tindakan_id'=>$this->input->post('kelompok_tindakan_id'),
            'kelas_tarif_id'=>$this->input->post('kelas_tarif_id'),
            'ruang_id'=>$this->input->post('ruang_id'),
            // 'kelompk_diagnosa_id'=>$this->input->post('kelompk_diagnosa_id'),
            'catatan'=>$this->input->post('catatan'),
            'dpjp_id'=>$this->input->post('dpjp_id'),
        );
        $kelompok_tindakan_id=$this->input->post('kelompok_tindakan_id');
        $kelompk_diagnosa_id=$this->input->post('kelompk_diagnosa_id');

        if ($this->input->post('statussetuju')=='1') {
            $data['statussetuju']='1';
            $data['tanggal_disetujui']=date('Y-m-d H:i:s');
            $data['idapprove']=$this->session->userdata('user_id');
        } elseif ($this->input->post('statussetuju')=='2') {
            $data['statussetuju']='2';
            $data['tanggal_tolak']=date('Y-m-d H:i:s');
            $data['idtolak']=$this->session->userdata('user_id');
        }

        // print_r($data);
        $this->db->where('id', $this->input->post('id'));
        $result = $this->db->update('tkamaroperasi_pendaftaran', $data);
        if ($result) {
            // $kelompok_tindakan_id='5,4';
            // $arr=explode(",",$kelompok_tindakan_id,0);
            $this->db->delete('tkamaroperasi_tindakan', array('idpendaftaranoperasi'=>$this->input->post('id')));
            $this->db->delete('tkamaroperasi_diagnosa', array('idpendaftaranoperasi'=>$this->input->post('id')));
            if ($kelompok_tindakan_id) {
                $arr=$kelompok_tindakan_id;
                foreach ($arr as $value) {
                    // $print_r($value);exit();
                    $data_tindakan=array(
                        'idpendaftaranoperasi'=>$this->input->post('id'),
                        'kelompok_tindakan_id'=>$value,
                    );
                    $this->db->insert('tkamaroperasi_tindakan', $data_tindakan);
                }
            }
            if ($kelompk_diagnosa_id) {
                $arr=$kelompk_diagnosa_id;
                foreach ($arr as $value) {
                    // $print_r($value);exit();
                    $data_tindakan=array(
                        'idpendaftaranoperasi'=>$this->input->post('id'),
                        'kelompok_diagnosa_id'=>$value,
                    );
                    $this->db->insert('tkamaroperasi_diagnosa', $data_tindakan);
                }
            }
            return true;
        } else {
            return false;
        }
    }
    public function loadKO()
    {
        $idtipe     = $this->input->post('idtipe');
        $tanggaldari     		= $this->input->post('tanggaldari');
        $tanggalsampai     	= $this->input->post('tanggalsampai');
        $waktu     	= $this->input->post('waktu');
        $iddokter_operator     	= $this->input->post('iddokter_operator');
        $iddokter_anestesi     	= $this->input->post('iddokter_anestesi');
        $status     	= $this->input->post('status');
        $nomedrec     	= $this->input->post('nomedrec');
        $namapasien     	= $this->input->post('namapasien');
        // $iduser=$this->session->userdata('user_id');

        $data_user=get_acces();
        $user_acces_form=$data_user['user_acces_form'];
        $this->select = array();
        $this->join 	= array();
        $this->where  = array();
        $where='';
        $where_dokter=" WHERE H.id <> ''";
        if ($iddokter_operator !='#') {
            $where_dokter .=" AND O.iddokteroperator ='$iddokter_operator'";
        }
        if ($status !='#') {
            if ($status=='1') {
                $where .=" AND (KO.statussetuju='0' AND KO.statusdatang='0' AND status='1')";
            }
            if ($status=='2') {
                $where .=" AND (KO.statussetuju='1' AND KO.statusdatang='0' AND status='1')";
            }
            if ($status=='3') {
                $where .=" AND (KO.statussetuju='0' AND KO.statusdatang='1' AND status='1')";
            }
            if ($status=='4') {
                $where .=" AND (KO.statussetuju='1' AND KO.statusdatang='1' AND status='1')";
            }
            if ($status=='5') {
                $where .=" AND (KO.statussetuju='2')";
            }
            if ($status=='6') {
                $where .=" AND (KO.status='0')";
            }
            if ($status=='7') {
                $where .=" AND (KO.status='2')";
            }
        }
        if ($iddokter_anestesi !='#') {
            $where_dokter .=" AND A.iddokteranastesi ='$iddokter_anestesi'";
        }
        if ($idtipe !='#') {
            $where .=" AND tipe_id='$idtipe'";
        }

        if ($waktu !='') {
            $where .=" AND TIME_FORMAT(waktumulaioperasi, '%H:%i')='".HumanTimeShort($waktu)."'";
        }
        if ($namapasien !='') {
            $where .=" AND namapasien LIKE '%".$namapasien."%'";
        }
        if ($nomedrec !='') {
            $where .=" AND nomedrec LIKE '%".$nomedrec."%'";
        }
        // print_r($namapasien);exit();
        if ($tanggaldari != '' && $tanggalsampai != '') {
            $this->where = array_merge($this->where, array('DATE(tanggaloperasi) >=' => YMDFormat($tanggaldari)));
            $this->where = array_merge($this->where, array('DATE(tanggaloperasi) <=' => YMDFormat($tanggalsampai)));
        }
        $status_datang='';
        $status_acc='';
        $from="(SELECT *FROM (SELECT H.id,
				H.waktumulaioperasi,H.waktuselesaioperasi,H.tanggaloperasi,ruang.nama as ruang_operasi,
				CASE WHEN H.tipe='1' THEN 'RAWAT INAP' WHEN H.tipe='2' THEN 'ODS' END as tipe,
				H.tipe as tipe_id,
				CASE WHEN H.idasalpendaftaran='1' THEN TP.namapasien ELSE RI.namapasien END as namapasien,
				CASE WHEN H.idasalpendaftaran='1' THEN TP.no_medrec ELSE RI.no_medrec END as nomedrec,
				CASE WHEN H.idasalpendaftaran='1' THEN TP.tanggal_lahir ELSE RI.tanggal_lahir END as tanggallahir,
				K.nama as kelas,B.nama as bed,
				H.diagnosa,H.operasi,H.statussetuju,H.statusdatang,H.`status`,RI.statuskasir,RI.statuspembayaran
				from tkamaroperasi_pendaftaran H
				LEFT JOIN tkamaroperasi_jasado O ON O.idpendaftaranoperasi=H.id
				LEFT JOIN tkamaroperasi_jasada A ON A.idpendaftaranoperasi=H.id
				LEFT JOIN mruangan ruang ON ruang.id=H.ruang_id
				LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.idpendaftaran
				LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpendaftaran
				LEFT JOIN mkelas K ON K.id=RI.idkelas
				LEFT JOIN mbed B ON B.id=RI.idbed
				".$where_dokter."
				GROUP BY H.id
				ORDER BY H.id DESC) KO WHERE id <> '' ".$where.") as tbl";
        // print_r($from);exit();
        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;

        $this->column_search   = array('namapasien','ruang_operasi','nomedrec','kelas','bed','diagnosa','operasi');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $r->id;
            $row[] = $no;
            $row[] = ($r->tipe=='ODS'?'<span class="label label-primary">'.$r->tipe.'</span>':'<span class="label label-success">'.$r->tipe.'</span>');
            $row[] = HumanDateShort($r->tanggaloperasi);
            $row[] = HumanTimeShort($r->waktumulaioperasi);
            $row[] = $r->ruang_operasi;
            $row[] = $r->nomedrec;
            $row[] = $r->namapasien;
            $row[] = HumanDateShort($r->tanggallahir);
            $row[] = $r->kelas.($r->bed?' / '.$r->bed:'');
            $row[] = $r->diagnosa;
            $row[] = $r->operasi;
            if ($r->statussetuju=='1') {
                $status_acc='<span class="label label-primary">Disetujui</span>';
            } else {
                $status_acc='<span class="label label-default">Persetujuan</span>';
            }
            if ($r->statusdatang=='1') {
                $status_datang='<span class="label label-primary">Pasien Sudah Datang</span>';
            } else {
                $status_datang='<span class="label label-default">Pasien Belum Datang</span>';
            }

            $status="";
            $aksi       = '<div class="btn-group">';
            if ($r->status=='1') {
                $status .=$status_acc.' '.$status_datang;
            // $status='<span class="label label-warning">DIALIHKAN</span>';
            } elseif ($r->status=='2') {
                $status='<span class="label label-success">SELESAI</span>';
            } elseif ($r->status=='0') {
                $status='<span class="label label-danger">DIBATALKAN</span>';
            }
            if ($r->statuskasir=='1') {
                $status .='<span class="label label-danger"><i class="fa fa-lock"></i></span>';
            }
            if ($r->status !='0') {
                if ($r->statuskasir ==0) {
                    if (UserAccesForm($user_acces_form, array('952'))) {
                        $aksi 		.= '<button class="btn btn-xs btn-info edit" title="Edit"><i class="fa fa-pencil"></i></button>';
                    }else{
						
					}
                }else{
                        // $aksi 		.= '<button class="btn btn-xs btn-info edit" title="Edit"><i class="fa fa-pencil"></i></button>';
					
				}
            }
            if ($r->statussetuju=='0') {
                if (UserAccesForm($user_acces_form, array('953'))) {
                    $aksi 		.= '<button class="btn btn-xs btn-primary setuju" title="Click Jika disetujui"><i class="fa fa-check"></i></button>';
                }
                if (UserAccesForm($user_acces_form, array('954'))) {
                    $aksi 		.= '<button class="btn btn-xs btn-danger tolak" title="Click Tidak Setuju"><i class="fa fa-close"></i> Tolak</button>';
                }
            } elseif ($r->statussetuju=='1') {
                if ($r->statuskasir =='0') {//Jika Belum Close Kasir
                    if ($r->status !='0') {
                        if ($r->statusdatang=='1') {
                            if (UserAccesForm($user_acces_form, array('957'))) {
                                $aksi 		.= '<a href="'.site_url().'tko/tindakan/'.$r->id.'" class="btn btn-xs btn-success tindakan" title="Tindakan"><i class="fa fa-keyboard-o"></i></a>';
                            }
                        }
                        if ($r->status=='1') {
                            if (UserAccesForm($user_acces_form, array('993'))) {
                                $aksi 		.= '<button class="btn btn-xs btn-danger batal" title="Batalkan Jadwal"><i class="fa fa-close"></i></button>';
                            }
                        }
                        if (UserAccesForm($user_acces_form, array('991'))) {
                            $aksi 		.= '<button class="btn btn-xs btn-primary gabung" title="Gabungkan Jadwal"><i class="fa fa-external-link-square"></i></button>';
                        }
                        if (UserAccesForm($user_acces_form, array('992'))) {
                            $aksi 		.= '<button class="btn btn-xs btn-success estimasi" title="Estimasi"><i class="fa fa-file-archive-o"></i></button>';
                        }
                        $aksi 		.= '<button class="btn btn-xs btn-success print" title="Print"><i class="fa fa-print"></i></button>';
                    }
                } else {
                    if ($r->statusdatang=='1') {
                        if (UserAccesForm($user_acces_form, array('957'))) {
                            $aksi 		.= '<a href="'.site_url().'tko/tindakan/'.$r->id.'" class="btn btn-xs btn-warning tindakan" title="Tindakan"><i class="fa fa-keyboard-o"></i></a>';
                        }
                    }
                    if (UserAccesForm($user_acces_form, array('992'))) {
                        $aksi 		.= '<button class="btn btn-xs btn-success estimasi" title="Estimasi"><i class="fa fa-file-archive-o"></i></button>';
                    }
                    $aksi 		.= '<button class="btn btn-xs btn-success print" title="Print"><i class="fa fa-print"></i></button>';
                }
            } else {
                $status ='<span class="label label-danger">DITOLAK</span>';
            }

            $aksi.='</div>';
            $row[] = $status;
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
    public function save_tindakan()
    {
        $st_lock = $this->input->post('st_lock');
        $id = $this->input->post('id_tindakan');
        $kelompok_tindakan_id = $this->input->post('kelompok_tindakan_id');
        $kelompok_diagnosa_id = $this->input->post('kelompok_diagnosa_id');

        if ($st_lock=='0') {//hanya belum di lock
            $data_header = array(
                'dpjp_id' => $this->input->post('dpjp'),
                'tanggaloperasi' => YMDFormat($this->input->post('tanggaloperasi')),
                'waktumulaioperasi' => HumanTime($this->input->post('waktuawal')),
                'waktuselesaioperasi' => HumanTime($this->input->post('waktuakhir')),
                'diagnosa' => $this->input->post('diagnosa'),
                'operasi' => $this->input->post('operasi'),
                'jenis_operasi_id' => $this->input->post('jenis_operasi_id'),
                'jenis_anestesi_id' => $this->input->post('jenis_anestesi_id'),
                'kelompok_operasi_id' => $this->input->post('kelompok_operasi_id'),
                'kelas_tarif_id' => $this->input->post('kelas_tarif_id'),
                'ruang_id' => $this->input->post('ruang_id'),
                'catatan' => $this->input->post('tcatatan'),
                'persen_da' => $this->input->post('persen_da'),
                'total_acuan_da' => RemoveComma($this->input->post('total_da')),
                'persen_daa' => $this->input->post('persen_daa'),
                'total_acuan_daa' => RemoveComma($this->input->post('total_daa')),
                'persen_dao' => $this->input->post('persen_dao'),
                'total_acuan_dao' => RemoveComma($this->input->post('total_dao')),
                // 'statusverif' => 1,
                'total_do' => 0,
                'total_da' => 0,
                'total_daa' => 0,
                'total_dao' => 0,
                'total_narcose' => 0,
                'total_obat' => 0,
                'total_alkes' => 0,
                'total_implan' => 0,
                'total_sewa' => RemoveComma($this->input->post('gt_alat')),
                'status' => 2,
            );
        } else {
            $data_header = array(
                'dpjp_id' => $this->input->post('dpjp'),
                'tanggaloperasi' => YMDFormat($this->input->post('tanggaloperasi')),
                'waktumulaioperasi' => HumanTime($this->input->post('waktuawal')),
                'waktuselesaioperasi' => HumanTime($this->input->post('waktuakhir')),
                'diagnosa' => $this->input->post('diagnosa'),
                'operasi' => $this->input->post('operasi'),
                'jenis_anestesi_id' => $this->input->post('jenis_anestesi_id'),
                'kelompok_operasi_id' => $this->input->post('kelompok_operasi_id'),
                'catatan' => $this->input->post('tcatatan'),

            );
        }
        // print_r($data_header);exit();
        $this->db->where('id', $id);
        $this->db->update('tkamaroperasi_pendaftaran', $data_header);
        // print_r('SINI');exit();
        if ($st_lock=='0') {//hanya belum di lock

        //FULLCARE
            $this->db->delete('tkamaroperasi_fullcare', array('idpendaftaran'=>$id));
            // print_r($this->input->post('id_tarif_fullcare'));exit();
            if ($this->input->post('id_tarif_fullcare')) {
                $data_fc=array(
                'idpendaftaran'=>$id,
                'idtarif'=>$this->input->post('id_tarif_fullcare'),
                'namatarif'=>$this->input->post('nama_tarif_fullcare'),
                'jasasarana'=>$this->input->post('sarana_fullcare'),
                'jasasarana_disc'=>0,
                'jasapelayanan'=>$this->input->post('pelayanan_fullcare'),
                'jasapelayanan_disc'=>0,
                'bhp'=>$this->input->post('bhp_fullcare'),
                'bhp_disc'=>0,
                'biayaperawatan'=>$this->input->post('perawatan_fullcare'),
                'biayaperawatan_disc'=>0,
                'total'=>RemoveComma($this->input->post('tarif_fullcare')),
                'kuantitas'=>1,
                'diskon'=>1,
                'totalkeseluruhan'=>RemoveComma($this->input->post('total_fullcare')),
                'statusverifikasi'=>0,
                'status'=>1,
            );
                $this->db->insert('tkamaroperasi_fullcare', $data_fc);
            }
            $this->db->delete('tkamaroperasi_tindakan', array('idpendaftaranoperasi'=>$id));
            $this->db->delete('tkamaroperasi_diagnosa', array('idpendaftaranoperasi'=>$id));
            if ($kelompok_tindakan_id) {
                foreach ($kelompok_tindakan_id as $key => $value) {
                    // print_r($value);exit();
                    $data_tindakan=array(
                    'idpendaftaranoperasi'=>$id,
                    'kelompok_tindakan_id'=>$value,
                );
                    // print_r($data_tindakan);exit();
                    $this->db->insert('tkamaroperasi_tindakan', $data_tindakan);
                }
            }
            // print_r($kelompok_diagnosa_id);exit();
            if ($kelompok_diagnosa_id) {
                foreach ($kelompok_diagnosa_id as $key => $value) {
                    // $print_r($value);exit();
                    $data_tindakan=array(
                    'idpendaftaranoperasi'=>$id,
                    'kelompok_diagnosa_id'=>$value,
                );
                    $this->db->insert('tkamaroperasi_diagnosa', $data_tindakan);
                }
            }

            $id_ruangan=$this->model->get_operasi_ruangan($id);
            $data_ruangan=array(
                'idpendaftaranoperasi'=>$id,
                'id_tarif'=>$this->input->post('id_tarif_ruangan'),
                'kelas_tarif'=>$this->input->post('kelas_tarif_id'),
                'nama_tarif'=>$this->input->post('nama_tarif_ruangan'),
                'iduser_transaksi'=>$this->session->userdata('user_id'),
                'tanggal_transaksi'=>date('Y-m-d H:i:s'),
                'jasasarana'=>$this->input->post('sarana_ruangan'),
                'jasapelayanan'=>$this->input->post('pelayanan_ruangan'),
                'bhp'=>$this->input->post('bhp_ruangan'),
                'biayaperawatan'=>$this->input->post('perawatan_ruangan'),
                'total'=>RemoveComma($this->input->post('tarif_ruangan')),
                'totalkeseluruhan'=>RemoveComma($this->input->post('tarif_ruangan')),
                'statusverif'=>'0',
                'status'=>'1',
            );
            if ($id_ruangan) {
                $this->db->where('id', $id_ruangan);
                $this->db->update('tkamaroperasi_ruangan', $data_ruangan);
            } else {
                $this->db->insert('tkamaroperasi_ruangan', $data_ruangan);
            }
            //DELETE NARCOSE,OBAT,ALKES, IMPLAN
            $id_hapus = $this->input->post('id_hapus');

            $jenis_hapus = $this->input->post('jenis_hapus');
            // print_r($jenis_hapus);exit();

            // print_r($jenis_hapus[0].' '.$id_hapus[0]);exit();
            if ($id_hapus) {
                foreach ($id_hapus as $key => $value) {
                    if ($jenis_hapus[$key]=='narcose') {
                        $q="DELETE FROM tkamaroperasi_narcose WHERE id='$value'";
                    }
                    if ($jenis_hapus[$key]=='obat') {
                        $q="DELETE FROM tkamaroperasi_obat WHERE id='$value'";
                    }
                    if ($jenis_hapus[$key]=='alkes') {
                        $q="DELETE FROM tkamaroperasi_alkes WHERE id='$value'";
                    }
                    if ($jenis_hapus[$key]=='implan') {
                        $q="DELETE FROM tkamaroperasi_implan WHERE id='$value'";
                    }
                    if ($jenis_hapus[$key]=='alat') {
                        $q="DELETE FROM tkamaroperasi_sewaalat WHERE id='$value'";
                    }
                    // print_r($q);exit();
                    $this->db->query($q);
                }
            }

            //DOKTER OPERATOR
            $id_do_detail = $this->input->post('id_do_detail');
            $nama_do_detail = $this->input->post('nama_do_detail');
            $nama_tarif_do_detail = $this->input->post('nama_tarif_do_detail');
            // print_r($this->input->post('nama_tarif_do_detail2'));exit();
            $tarif_do_detail = $this->input->post('tarif_do_detail');
            $id_tarif_do_detail = $this->input->post('id_tarif_do_detail');
            $sarana_do_detail = $this->input->post('sarana_do_detail');
            $jasapelayanan_do_detail = $this->input->post('jasapelayanan_do_detail');
            $bhp_do_detail = $this->input->post('bhp_do_detail');
            $biayaperawatan_do_detail = $this->input->post('biayaperawatan_do_detail');
            $this->db->where('idpendaftaranoperasi', $id);
            $this->db->delete('tkamaroperasi_jasado');
            if ($id_do_detail) {
                foreach ($id_do_detail as $key => $value) {
                    $data_do=array(
                'idpendaftaranoperasi'=>$id,
                'iddokteroperator'=>$id_do_detail[$key],
                'namadokteroperator'=>$nama_do_detail[$key],
                'nama_tarif'=>$nama_tarif_do_detail[$key],
                'id_tarif'=>$id_tarif_do_detail[$key],
                'kelas_tarif'=>$this->input->post('kelas_tarif_id'),
                'iduser_transaksi'=>$this->session->userdata('user_id'),
                'tanggal_transaksi'=>date('Y-m-d H:i:s'),
                'jasasarana'=>$sarana_do_detail[$key],
                'jasapelayanan'=>$jasapelayanan_do_detail[$key],
                'bhp'=>$bhp_do_detail[$key],
                'biayaperawatan'=>$biayaperawatan_do_detail[$key],
                'total'=>RemoveComma($tarif_do_detail[$key]),
                'totalkeseluruhan'=>RemoveComma($tarif_do_detail[$key]),
                'statusverif'=>'0',
                'status'=>'1',
            );
                    if ($this->db->insert('tkamaroperasi_jasado', $data_do)) {
                        $lastInsertId = $this->db->insert_id();
                        $query = $this->db->query("SELECT idpendaftaran FROM tkamaroperasi_pendaftaran WHERE id = $id");
                        $pendaftaranOK = $query->row();

                        $idrawatinap = $pendaftaranOK->idpendaftaran;
                        $iddokter = $id_do_detail[$key];
                        $dokter = GetPotonganDokterRanap($idrawatinap, $iddokter);

                        $this->db->set('pajak', $dokter->pajak_dokter);
                        $this->db->set('potongan', $dokter->potongan_rs);

                        $this->db->where('id', $lastInsertId);
                        $this->db->update('tkamaroperasi_jasado');
                    }
                }
            }
            //DOKTER ANASTHESI
            $id_da_detail = $this->input->post('id_da_detail');
            $id_nama_da_detail = $this->input->post('id_nama_da_detail');
            $tarif_da_detail = $this->input->post('tarif_da_detail');
            $persen_da_detail = $this->input->post('persen_da_detail');

            // print_r($id);exit();
            $this->db->where('idpendaftaranoperasi', $id);
            $this->db->delete('tkamaroperasi_jasada');
            if ($id_da_detail) {
                foreach ($id_da_detail as $key => $value) {
                    $data_da=array(
                'idpendaftaranoperasi'=>$id,
                'iddokteranastesi'=>$id_da_detail[$key],
                'namadokteranastesi'=>$id_nama_da_detail[$key],
                'jenis_operasi_id'=>$this->input->post('jenis_operasi_id'),
                'total_acuan'=>RemoveComma($this->input->post('total_da')),
                'iduser_transaksi'=>$this->session->userdata('user_id'),
                'tanggal_transaksi'=>date('Y-m-d H:i:s'),
                'persen'=>$persen_da_detail[$key],
                'totalkeseluruhan'=>RemoveComma($tarif_da_detail[$key]),
                'statusverif'=>'0',
                'status'=>'1',
            );
                    if ($this->db->insert('tkamaroperasi_jasada', $data_da)) {
                        $lastInsertId = $this->db->insert_id();
                        $query = $this->db->query("SELECT idpendaftaran FROM tkamaroperasi_pendaftaran WHERE id = $id");
                        $pendaftaranOK = $query->row();

                        $idrawatinap = $pendaftaranOK->idpendaftaran;
                        $iddokter = $id_da_detail[$key];
                        $dokter = GetPotonganDokterRanap($idrawatinap, $iddokter);

                        $this->db->set('pajak', $dokter->pajak_dokter);
                        $this->db->set('potongan', $dokter->potongan_rs);

                        $this->db->where('id', $lastInsertId);
                        $this->db->update('tkamaroperasi_jasada');
                    }
                }
            }
            //ASISTEN ANASTHESI
            $id_daa_detail = $this->input->post('id_daa_detail');
            $id_nama_daa_detail = $this->input->post('id_nama_daa_detail');
            $tarif_daa_detail = $this->input->post('tarif_daa_detail');
            $persen_daa_detail = $this->input->post('persen_daa_detail');
            $jenis_sisten_detail = $this->input->post('jenis_sisten_detail');

            $this->db->where('idpendaftaranoperasi', $id);
            $this->db->delete('tkamaroperasi_jasadaa');
            if ($id_daa_detail) {
                foreach ($id_daa_detail as $key => $value) {
                    $data_da=array(
                'idpendaftaranoperasi'=>$id,
                'id_asisten_da'=>$id_daa_detail[$key],
                'namaasisten_da'=>$id_nama_daa_detail[$key],
                'jenis_asisten_da'=>$jenis_sisten_detail[$key],
                'jenis_operasi_id'=>$this->input->post('jenis_operasi_id'),
                'total_acuan'=>RemoveComma($this->input->post('total_daa')),
                'iduser_transaksi'=>$this->session->userdata('user_id'),
                'tanggal_transaksi'=>date('Y-m-d H:i:s'),
                'persen'=>$persen_daa_detail[$key],
                'totalkeseluruhan'=>RemoveComma($tarif_daa_detail[$key]),
                'statusverif'=>'0',
                'status'=>'1',
            );
                    $this->db->insert('tkamaroperasi_jasadaa', $data_da);
                }
            }
            //ASISTEN OPERATOR

            $id_dao_detail = $this->input->post('id_dao_detail');
            $id_nama_dao_detail = $this->input->post('id_nama_dao_detail');
            $tarif_dao_detail = $this->input->post('tarif_dao_detail');
            $persen_dao_detail = $this->input->post('persen_dao_detail');
            $jenis_asisten_dao_detail = $this->input->post('jenis_asisten_dao_detail');

            $this->db->where('idpendaftaranoperasi', $id);
            $this->db->delete('tkamaroperasi_jasaao');
            if ($id_dao_detail) {
                foreach ($id_dao_detail as $key => $value) {
                    $data_da=array(
                    'idpendaftaranoperasi'=>$id,
                    'id_asisten_dao'=>$id_dao_detail[$key],
                    'namaasisten_dao'=>$id_nama_dao_detail[$key],
                    'jenis_asisten_dao'=>$jenis_asisten_dao_detail[$key],
                    'jenis_operasi_id'=>$this->input->post('jenis_operasi_id'),
                    'total_acuan'=>RemoveComma($this->input->post('total_dao')),
                    'iduser_transaksi'=>$this->session->userdata('user_id'),
                    'tanggal_transaksi'=>date('Y-m-d H:i:s'),
                    'persen'=>$persen_dao_detail[$key],
                    'totalkeseluruhan'=>RemoveComma($tarif_dao_detail[$key]),
                    'statusverif'=>'0',
                    'status'=>'1',
                );
                    $this->db->insert('tkamaroperasi_jasaao', $data_da);
                }
            }
            $konsulen_id = $this->input->post('konsulen_id');
            $this->db->where('idpendaftaranoperasi', $id);
            $this->db->delete('tkamaroperasi_konsulen');
            if ($konsulen_id) {
                foreach ($konsulen_id as $key => $value) {
                    $data=array(
                    'idpendaftaranoperasi'=>$id,
                    'iddokter'=>$value
                );
                    $this->db->insert('tkamaroperasi_konsulen', $data);
                }
            }
            $instrumen_id = $this->input->post('instrumen_id');
            $this->db->where('idpendaftaranoperasi', $id);
            $this->db->delete('tkamaroperasi_instrumen');
            if ($instrumen_id) {
                foreach ($instrumen_id as $key => $value) {
                    $data=array(
                    'idpendaftaranoperasi'=>$id,
                    'idpegawai'=>$value
                );
                    $this->db->insert('tkamaroperasi_instrumen', $data);
                }
            }
            $sirkuler_id = $this->input->post('sirkuler_id');
            $this->db->where('idpendaftaranoperasi', $id);
            $this->db->delete('tkamaroperasi_sirkuler');
            if ($sirkuler_id) {
                foreach ($sirkuler_id as $key => $value) {
                    $data=array(
                    'idpendaftaranoperasi'=>$id,
                    'idpegawai'=>$value
                );
                    $this->db->insert('tkamaroperasi_sirkuler', $data);
                }
            }

            //ST EDIT NARCOSE,OBAT,IMPLAN
            $st_edit_narcose = $this->input->post('st_edit_narcose');
            // print_r(in_array('1',$st_edit_narcose));exit();
            $st_edit_obat = $this->input->post('st_edit_obat');
            $st_edit_alkes = $this->input->post('st_edit_alkes');
            $st_edit_implan = $this->input->post('st_edit_implan');
            $st_edit_alat = $this->input->post('st_edit_alat');

            //Narcose
            $idunit_narcose_detail = $this->input->post('idunit_narcose_detail');
            $id_narcose_detail = $this->input->post('id_narcose_detail');
            $harga_narcose_detail = $this->input->post('harga_narcose_detail');
            $kuantitas_narcose_detail = $this->input->post('kuantitas_narcose_detail');
            $total_narcose_detail = $this->input->post('total_narcose_detail');
            $harga_dasar_narcose_detail = $this->input->post('harga_dasar_narcose_detail');
            $margin_narcose_detail = $this->input->post('margin_narcose_detail');
            $idtipe_narcose_detail = $this->input->post('idtipe_narcose_detail');
            $idtipe_narcose_detail = $this->input->post('idtipe_narcose_detail');
            $id_trx_narcose_detail = $this->input->post('id_trx_narcose_detail');

            // $this->db->where('idpendaftaranoperasi',$id);
            // $this->db->delete('tkamaroperasi_narcose');
            // print_r($idunit_narcose_detail);exit();
            if ($idunit_narcose_detail) {
                if (in_array('1', $st_edit_narcose)) {
                    foreach ($idunit_narcose_detail as $key => $value) {
                        if ($st_edit_narcose[$key]=='1') {
                            $data=array(
                        'idpendaftaranoperasi'=>$id,
                        'idunit'=>$idunit_narcose_detail[$key],
                        'idtipe'=>$idtipe_narcose_detail[$key],
                        'idobat'=>$id_narcose_detail[$key],
                        'hargadasar'=>RemoveComma($harga_dasar_narcose_detail[$key]),
                        'margin'=>RemoveComma($margin_narcose_detail[$key]),
                        'hargajual'=>RemoveComma($harga_narcose_detail[$key]),
                        'kuantitas'=>RemoveComma($kuantitas_narcose_detail[$key]),
                        'totalkeseluruhan'=>RemoveComma($total_narcose_detail[$key]),
                        'iduser_transaksi'=>$this->session->userdata('user_id'),
                        'nama_user_transaksi'=>$this->session->userdata('user_name'),
                        'tanggal_transaksi'=>date('Y-m-d H:i:s'),
                        'statusverifikasi'=>'0',
                    );
                            // print_r($id_trx_narcose_detail[$key]);exit();
                            if ($id_trx_narcose_detail[$key]=='0') {
                                $this->db->insert('tkamaroperasi_narcose', $data);
                            } else {
                                $this->db->where('id', $id_trx_narcose_detail[$key]);
                                $this->db->update('tkamaroperasi_narcose', $data);
                            }
                        }
                    }
                }
            }
            //Obat
            $idunit_obat_detail = $this->input->post('idunit_obat_detail');
            $id_obat_detail = $this->input->post('id_obat_detail');
            $harga_obat_detail = $this->input->post('harga_obat_detail');
            $kuantitas_obat_detail = $this->input->post('kuantitas_obat_detail');
            $total_obat_detail = $this->input->post('total_obat_detail');
            $harga_dasar_obat_detail = $this->input->post('harga_dasar_obat_detail');
            $margin_obat_detail = $this->input->post('margin_obat_detail');
            $idtipe_obat_detail = $this->input->post('idtipe_obat_detail');
            $id_trx_obat = $this->input->post('id_trx_obat_detail');

            // $this->db->where('idpendaftaranoperasi',$id);
            // $this->db->delete('tkamaroperasi_obat');
            if ($idunit_obat_detail) {
                if (in_array('1', $st_edit_obat)) {
                    foreach ($idunit_obat_detail as $key => $value) {
                        if ($st_edit_obat[$key]=='1') {
                            $data=array(
                        'idpendaftaranoperasi'=>$id,
                        'idunit'=>$idunit_obat_detail[$key],
                        'idtipe'=>$idtipe_obat_detail[$key],
                        'idobat'=>$id_obat_detail[$key],
                        'hargadasar'=>RemoveComma($harga_dasar_obat_detail[$key]),
                        'margin'=>RemoveComma($margin_obat_detail[$key]),
                        'hargajual'=>RemoveComma($harga_obat_detail[$key]),
                        'kuantitas'=>RemoveComma($kuantitas_obat_detail[$key]),
                        'totalkeseluruhan'=>RemoveComma($total_obat_detail[$key]),
                        'iduser_transaksi'=>$this->session->userdata('user_id'),
                        'nama_user_transaksi'=>$this->session->userdata('user_name'),
                        'tanggal_transaksi'=>date('Y-m-d H:i:s'),
                        'statusverifikasi'=>'0',
                    );
                            // print_r($id_trx_obat[$key]);exit();
                            if ($id_trx_obat[$key]==0) {
                                $this->db->insert('tkamaroperasi_obat', $data);
                            } else {
                                $this->db->where('id', $id_trx_obat[$key]);
                                $this->db->update('tkamaroperasi_obat', $data);
                            }
                        }
                    }
                }
            }
            //Implan
            $idunit_implan_detail = $this->input->post('idunit_implan_detail');
            $id_implan_detail = $this->input->post('id_implan_detail');
            $harga_implan_detail = $this->input->post('harga_implan_detail');
            $kuantitas_implan_detail = $this->input->post('kuantitas_implan_detail');
            $total_implan_detail = $this->input->post('total_implan_detail');
            $harga_dasar_implan_detail = $this->input->post('harga_dasar_implan_detail');
            $margin_implan_detail = $this->input->post('margin_implan_detail');
            $idtipe_implan_detail = $this->input->post('idtipe_implan_detail');
            $id_trx_implan = $this->input->post('id_trx_implan_detail');

            // $this->db->where('idpendaftaranoperasi',$id);
            // $this->db->delete('tkamaroperasi_implan');
            if ($idunit_implan_detail) {
                if (in_array('1', $st_edit_implan)) {
                    foreach ($idunit_implan_detail as $key => $value) {
                        if ($st_edit_implan[$key]=='1') {
                            $data=array(
                        'idpendaftaranoperasi'=>$id,
                        'idunit'=>$idunit_implan_detail[$key],
                        'idtipe'=>$idtipe_implan_detail[$key],
                        'idobat'=>$id_implan_detail[$key],
                        'hargadasar'=>RemoveComma($harga_dasar_implan_detail[$key]),
                        'margin'=>RemoveComma($margin_implan_detail[$key]),
                        'hargajual'=>RemoveComma($harga_implan_detail[$key]),
                        'kuantitas'=>RemoveComma($kuantitas_implan_detail[$key]),
                        'totalkeseluruhan'=>RemoveComma($total_implan_detail[$key]),
                        'iduser_transaksi'=>$this->session->userdata('user_id'),
                        'nama_user_transaksi'=>$this->session->userdata('user_name'),
                        'tanggal_transaksi'=>date('Y-m-d H:i:s'),
                        'statusverifikasi'=>'0',
                    );
                            if ($id_trx_implan[$key]=='0') {
                                $this->db->insert('tkamaroperasi_implan', $data);
                            } else {
                                $this->db->where('id', $id_trx_implan[$key]);
                                $this->db->update('tkamaroperasi_implan', $data);
                            }
                        }
                    }
                }
            }
            //Alkes
            $idunit_alkes_detail = $this->input->post('idunit_alkes_detail');
            $id_alkes_detail = $this->input->post('id_alkes_detail');
            $harga_alkes_detail = $this->input->post('harga_alkes_detail');
            $kuantitas_alkes_detail = $this->input->post('kuantitas_alkes_detail');
            $total_alkes_detail = $this->input->post('total_alkes_detail');
            $harga_dasar_alkes_detail = $this->input->post('harga_dasar_alkes_detail');
            $margin_alkes_detail = $this->input->post('margin_alkes_detail');
            $idtipe_alkes_detail = $this->input->post('idtipe_alkes_detail');
            $id_trx_alkes = $this->input->post('id_trx_alkes_detail');

            // $this->db->where('idpendaftaranoperasi',$id);
            // $this->db->delete('tkamaroperasi_alkes');
            if ($idunit_alkes_detail) {
                if (in_array('1', $st_edit_alkes)) {
                    foreach ($idunit_alkes_detail as $key => $value) {
                        if ($st_edit_alkes[$key]=='1') {
                            $data=array(
                        'idpendaftaranoperasi'=>$id,
                        'idunit'=>$idunit_alkes_detail[$key],
                        'idtipe'=>$idtipe_alkes_detail[$key],
                        'idobat'=>$id_alkes_detail[$key],
                        'hargadasar'=>RemoveComma($harga_dasar_alkes_detail[$key]),
                        'margin'=>RemoveComma($margin_alkes_detail[$key]),
                        'hargajual'=>RemoveComma($harga_alkes_detail[$key]),
                        'kuantitas'=>RemoveComma($kuantitas_alkes_detail[$key]),
                        'totalkeseluruhan'=>RemoveComma($total_alkes_detail[$key]),
                        'iduser_transaksi'=>$this->session->userdata('user_id'),
                        'nama_user_transaksi'=>$this->session->userdata('user_name'),
                        'tanggal_transaksi'=>date('Y-m-d H:i:s'),
                        'statusverifikasi'=>'0',
                    );
                            if ($id_trx_alkes[$key]=='0') {
                                $this->db->insert('tkamaroperasi_alkes', $data);
                            } else {
                                $this->db->where('id', $id_trx_alkes[$key]);
                                $this->db->update('tkamaroperasi_alkes', $data);
                            }
                        }
                    }
                }
            }
            //Sewa Alat
            $idalat = $this->input->post('id_alat_detail');
            $jasasarana = $this->input->post('sarana_alat_detail');
            $jasapelayanan = $this->input->post('pelayanan_alat_detail');
            $bhp = $this->input->post('bhp_alat_detail');
            $biayaperawatan = $this->input->post('biayaperawatan_alat_detail');
            $total = $this->input->post('tarif_alat_detail');
            $kuantitas = $this->input->post('kuantitas_alat_detail');
            $totalkeseluruhan = $this->input->post('total_alat_detail');


            $id_alat_trx_detail = $this->input->post('id_alat_trx_detail');
            $st_edit_alat = $this->input->post('st_edit_alat');

            // $this->db->where('idpendaftaranoperasi',$id);
            // $this->db->delete('tkamaroperasi_sewaalat');
            if ($idalat) {
                if (in_array('1', $st_edit_alat)) {
                    foreach ($idalat as $key => $value) {
                        if ($st_edit_alat[$key]=='1') {
                            $data=array(
                            'idpendaftaranoperasi'=>$id,
                            'idalat'=>$idalat[$key],
                            'jasasarana'=>RemoveComma($jasasarana[$key]),
                            'jasapelayanan'=>RemoveComma($jasapelayanan[$key]),
                            'bhp'=>RemoveComma($bhp[$key]),
                            'biayaperawatan'=>RemoveComma($biayaperawatan[$key]),
                            'total'=>RemoveComma($total[$key]),
                            'kuantitas'=>RemoveComma($kuantitas[$key]),
                            'totalkeseluruhan'=>RemoveComma($totalkeseluruhan[$key]),
                            'iduser_transaksi'=>$this->session->userdata('user_id'),
                            'nama_user_transaksi'=>$this->session->userdata('user_name'),
                            'tanggal_transaksi'=>date('Y-m-d H:i:s'),
                            'statusverifikasi'=>'0',
                        );
                            if ($id_alat_trx_detail[$key]=='0') {
                                $this->db->insert('tkamaroperasi_sewaalat', $data);
                            } else {
                                $this->db->where('id', $id_alat_trx_detail[$key]);
                                $this->db->update('tkamaroperasi_sewaalat', $data);
                            }
                            // $this->db->insert('tkamaroperasi_sewaalat',$data);
                        }
                    }
                }
            }
            // print_r('SINI');exit();
        }

        // print_r($data);exit();

        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'Data Berhasil Tersimpan !');
        redirect('tko', 'location');
    }
    public function LoadPasien()
    {
        $snomedrec     = $this->input->post('snomedrec');
        $snamapasien     		= $this->input->post('snamapasien');
        $stanggallahir     	= $this->input->post('stanggallahir');
        $skelas    = $this->input->post('skelas');
        $sdpjp    = $this->input->post('sdpjp');
        $sperujuk    = $this->input->post('sperujuk');
        $sasal    = $this->input->post('sasal');
        $iduser=$this->session->userdata('user_id');

        $data_user=get_acces();
        $user_acces_form=$data_user['user_acces_form'];
        $this->select = array();
        $this->join 	= array();
        $this->where  = array();
        $where='';
        if ($snomedrec  != '') {
            $where .=" AND H.no_medrec LIKE '%".$snomedrec."%'";
        }
        if ($snamapasien  != '') {
            $where .=" AND H.namapasien LIKE '%".$snamapasien."%'";
        }
        if ('' != $stanggallahir) {
            $where .= " AND DATE(H.tanggal_lahir) ='".YMDFormat($stanggallahir)."'";
        }
        if ($skelas  != '#') {
            $where .=" AND H.idkelas = '$skelas'";
        }
        if ($sdpjp  != '#') {
            $where .=" AND H.iddokterpenanggungjawab = '$sdpjp'";
        }
        if ($sperujuk  != '#') {
            $where .=" AND P.iddokter = '$sperujuk'";
        }
        if ($sasal  != '#') {
            $where .=" AND H.idtipe = '$sasal'";
        }
        $from="(SELECT H.id,
				CASE WHEN H.idtipe='1' THEN 'RAWAT INAP' ELSE 'ODS' END as tipe,
				CASE WHEN H.idtipe='1' THEN H.id ELSE P.id END as idpendaftaran,
				H.idtipe,H.no_medrec,H.namapasien,H.tanggal_lahir,H.idpasien
				,R.nama as ruang,K.nama as kelas,B.nama as bed,D.nama as dpjp,P.iddokter,rujuk.nama as perujuk,H.idkelas
				from trawatinap_pendaftaran H
				LEFT JOIN mruangan R ON R.id=H.idruangan
				LEFT JOIN mkelas K ON K.id=H.idkelas
				LEFT JOIN mbed B ON B.id=H.idbed
				LEFT JOIN mdokter D ON D.id=H.iddokterpenanggungjawab
				LEFT JOIN tpoliklinik_pendaftaran P ON P.id=H.idpoliklinik
				LEFT JOIN mdokter rujuk ON rujuk.id=P.iddokter
				WHERE H.statuscheckout='0' AND H.status='1' AND H.statuspembayaran='0' ".$where.") as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;

        $this->column_search   = array('namapasien','tipe','no_medrec','kelas','bed','dpjp','perujuk');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = ($r->idtipe=='2'?'<span class="label label-primary">'.$r->tipe.'</span>':'<span class="label label-success">'.$r->tipe.'</span>');
            $row[] = "<a href='#' class='selectPasien' data-id='".$r->id."' data-dismiss='modal'>".$r->no_medrec."</span></a>";
            $row[] = $r->namapasien;
            $row[] = HumanDateShort($r->tanggal_lahir);
            $row[] = $r->kelas.'-'.$r->bed;
            $row[] = $r->dpjp;
            $row[] = $r->perujuk;
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
    public function LoadJadwal()
    {
        $snomedrec     = $this->input->post('snomedrec');
        $snamapasien     		= $this->input->post('snamapasien');
        $stanggallahir     	= $this->input->post('stanggallahir');
        $skelas    = $this->input->post('skelas');
        $sdiagnosa    = $this->input->post('sdiagnosa');
        $soperasi    = $this->input->post('soperasi');
        $sasal    = $this->input->post('sasal');
        $iduser=$this->session->userdata('user_id');

        $data_user=get_acces();
        $user_acces_form=$data_user['user_acces_form'];
        $this->select = array();
        $this->join 	= array();
        $this->where  = array();
        $where='';
        if ($snomedrec  != '') {
            $where .=" AND nomedrec LIKE '%".$snomedrec."%'";
        }
        if ($snamapasien  != '') {
            $where .=" AND namapasien LIKE '%".$snamapasien."%'";
        }
        if ($soperasi  != '') {
            $where .=" AND operasi LIKE '%".$soperasi."%'";
        }
        if ($sdiagnosa  != '') {
            $where .=" AND diagnosa LIKE '%".$sdiagnosa."%'";
        }
        if ('' != $stanggallahir) {
            $where .= " AND DATE(tanggal_lahir) ='".YMDFormat($stanggallahir)."'";
        }
        // kelas_tarif_id
        if ($skelas  != '#') {
            $where .=" AND kelas_tarif_id = '$skelas'";
        }
        if ($sasal  != '#') {
            $where .=" AND tipe_id = '$sasal'";
        }

        $from="(SELECT *FROM (SELECT H.id,RI.idtipe,
				H.waktumulaioperasi,H.waktuselesaioperasi,H.tanggaloperasi,ruang.nama as ruang_operasi,
				CASE WHEN RI.idtipe IS null THEN 'ODS' WHEN RI.idtipe='1' THEN 'RAWAT INAP' WHEN RI.idtipe='2' THEN 'ODS' END as tipe,
				CASE WHEN RI.idtipe IS null THEN '2' WHEN RI.idtipe='1' THEN '1' WHEN RI.idtipe='2' THEN '2' END as tipe_id,
				CASE WHEN H.idasalpendaftaran='1' THEN TP.namapasien ELSE RI.namapasien END as namapasien,
				CASE WHEN H.idasalpendaftaran='1' THEN TP.no_medrec ELSE RI.no_medrec END as nomedrec,
				CASE WHEN H.idasalpendaftaran='1' THEN TP.tanggal_lahir ELSE RI.tanggal_lahir END as tanggallahir,
				K.nama as kelas,B.nama as bed,
				H.diagnosa,H.operasi,H.statussetuju,H.statusdatang,H.`status`,H.kelas_tarif_id
				from tkamaroperasi_pendaftaran H
				LEFT JOIN mruangan ruang ON ruang.id=H.ruang_id
				LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.idpendaftaran
				LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpendaftaran
				LEFT JOIN mkelas K ON K.id=H.kelas_tarif_id
				LEFT JOIN mbed B ON B.id=RI.idbed
				ORDER BY H.id DESC) KO WHERE id <> '' ".$where.") as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;

        $this->column_search   = array('namapasien','tipe','nomedrec','diagnosa','operasi');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = ($r->tipe=='ODS'?'<span class="label label-primary">'.$r->tipe.'</span>':'<span class="label label-success">'.$r->tipe.'</span>');
            $row[] = "<a href='#' class='selectJadwal' data-id='".$r->id."' data-dismiss='modal'>".$r->nomedrec."</span></a>";
            $row[] = $r->namapasien;
            $row[] = HumanDateShort($r->tanggallahir);
            $row[] = $r->kelas;
            $row[] = $r->diagnosa;
            $row[] = $r->operasi;
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
    public function find_pasien($id)
    {
        $arr = $this->model->find_pasien($id);
        $this->output->set_output(json_encode($arr));
    }
    public function get_dokter_anestesi()
    {
        $arr = $this->model->list_dokter_anestesi();
        echo json_encode($arr);
    }
    public function js_obat()
    {
        $arr = $this->model->js_obat();
        echo json_encode($arr);
    }
    public function kelompok_tindakan_list($id=0)
    {
        $arr = $this->model->kelompok_tindakan_list($id);
        echo json_encode($arr);
    }
    public function kelompok_diagnosa_list($id=0)
    {
        $arr = $this->model->kelompok_diagnosa_list($id);
        echo json_encode($arr);
    }
    public function js_obat_narcose()
    {
        $arr = $this->model->js_obat_narcose();
        echo json_encode($arr);
    }
    public function get_dokter_pegawai_anestesi()
    {
        $arr = $this->model->list_dokter_pegawai_anestesi();
        echo json_encode($arr);
    }
    public function get_dokter_asisten_operator()
    {
        $arr = $this->model->get_dokter_asisten_operator();
        echo json_encode($arr);
    }
    public function load_tarif_do()
    {
        $jenis_operasi_id     = $this->input->post('jenis_operasi_id');
        $kelas_tarif_id     = $this->input->post('kelas_tarif_id');
        $arr = $this->model->load_tarif_do($jenis_operasi_id, $kelas_tarif_id);
        $this->output->set_output(json_encode($arr));
    }
    public function load_tarif_ruangan()
    {
        $jenis_operasi_id     = $this->input->post('jenis_operasi_id');
        $kelas_tarif_id     = $this->input->post('kelas_tarif_id');
        $arr = $this->model->load_tarif_ruangan($jenis_operasi_id, $kelas_tarif_id);
        $this->output->set_output(json_encode($arr));
    }
    public function load_tarif_fullcare()
    {
        $ruang_id     = $this->input->post('ruang_id');
        $kelas_tarif_id     = $this->input->post('kelas_tarif_id');
        $idkelompokpasien     = $this->input->post('idkelompokpasien');
        $arr = $this->model->load_tarif_fullcare($ruang_id, $kelas_tarif_id, $idkelompokpasien);
        $this->output->set_output(json_encode($arr));
    }
    public function load_tarif_sewa()
    {
        $jenis_operasi_id     = $this->input->post('jenis_operasi_id');
        $kelas_tarif_id     = $this->input->post('kelas_tarif_id');
        $id_alat     = $this->input->post('id_alat');
        $arr = $this->model->load_tarif_sewa($jenis_operasi_id, $kelas_tarif_id, $id_alat);
        $this->output->set_output(json_encode($arr));
    }
    public function get_edit($id)
    {
        $arr = $this->model->get_edit($id);
        $this->output->set_output(json_encode($arr));
    }
    public function get_barang($idbarang, $idtipe, $idkelompokpasien)
    {
        $arr = $this->model->get_barang($idbarang, $idtipe, $idkelompokpasien);
        $this->output->set_output(json_encode($arr));
    }

    // @2
    public function filter()
    {
        $data = array(
            'asalpasien' => $this->input->post('asalpasien'),
            'status' => $this->input->post('status'),
            'tglawal' => $this->input->post('tglawal'),
            'tglakhir' => $this->input->post('tglakhir'),
            'idtipe' => $this->input->post('idtipe'),
            'waktu' => $this->input->post('waktu'),
            'dokter' => $this->input->post('dokter'),
            'filter_jam' => $this->input->post('filter_jam'),
        );

        $this->session->set_userdata($data);

        $data['error'] = '';
        $data['title'] = 'Kamar Operasi';
        $data['content'] = 'Tko/index';
        $data['breadcrum'] = array(array("RSKB Halmahera", '#'), array("Kamar Operasi", '#'));
        $data['petugasko'] = $this->model->getpetugasko();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    /* @section index &end */

    // @3
    public function manage($id, $idpendaftaran)
    {
        $data = array();
        $data['error'] = '';
        $data['title'] = 'Tindakan Kamar Operasi';
        $data['content'] = 'Tko/manage';
        $data['breadcrum'] = array(array("RSKB Halmahera", '#'), array("Kamar Operasi", '#'), array("Tindakan Kamar Operasi", '#'));

        $data['idpendaftaran'] = $idpendaftaran;
        $data['dokterumum'] = $this->model->getdokterumum();
        $data['dokteranasthesi'] = $this->model->getdokterumum(4);
        $data['petugasko'] = $this->model->getpetugasko();
        $data['petugasas'] = $this->model->getpetugasanaesthesi();
        $data['pasienkod'] = $this->model->getpasienko_d($id); // it's not done yet !
        $data['pasienkot'] = $this->model->getpasienko_t($id); // it's done !
        $data['unitpelayanan'] = $this->model->getListUnitPelayanan();
        $data['unitpelayanandef'] = $this->model->getDefaultUnitPelayananUser();
        $data['ruangoperasi'] = $this->model->getruangan();
        $data['jeniso'] = $this->model->getjenisoperasi();
        $data['keterangano'] = $this->model->getketeranganoperasi();
        $data['jenisa'] = $this->model->getjenisanaesthesi();
        $data['daftarestimasi'] = $this->model->getestimasi($idpendaftaran);
        $data['totalestimasi'] = $this->model->gettotalestimasi($idpendaftaran);

        $statusLockKasir = $this->model->getStatusLockKasirRanap($idpendaftaran);
        $data['statuslock'] = (isset($statusLockKasir) ? $statusLockKasir->statuskasir : 0);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    // @4
    public function getbiayapenggunaan($id)
    {
        $b1 = $this->model->biayanarcose($id);
        $b2 = $this->model->biayaobat($id);
        $b3 = $this->model->biayaalkes($id);
        $b4 = $this->model->biayaimplan($id);
        $b5 = $this->model->biayasewaalat($id);
        return $biaya = $b1[0]->biayanarcose + $b2[0]->biayaobat + $b3[0]->biayaalkes + $b4[0]->biayaimplan + $b5[0]->biayaalat;
    }

    // @4
    public function getbiayakamar()
    {
        $kelas = $this->input->post('kelas');
        $jenis = $this->input->post('jenis');

        $result = $this->model->getbiayakamar($kelas, $jenis);
        $this->output->set_output(json_encode($result));
    }

    public function getbiayajasmed()
    {
        $kelas = $this->input->post('kelas');
        $jenis = $this->input->post('jenis');

        $result = $this->model->getbiayajasmed($kelas, $jenis);
        $this->output->set_output(json_encode($result));
    }

    public function splitdokter($dokter)
    {
        $dokteru = "";
        $no = 1;
        if (!empty($dokter)) {
            foreach ($dokter as $rows) {
                $condition = count($dokter) - $no;
                ($condition == 0) ? $dokteru .= $rows : $dokteru .= $rows . ",";
                $no++;
            }
        }

        return $dokteru;
    }

    public function getkelas($romawi)
    {
        // 1 : I | 2 : II | 3 : III
        $kelas;
        if ($romawi === "POLIKLINIK / IGD") {
            $kelas = 0;
        } elseif ($romawi === "I") {
            $kelas = 1;
        } elseif ($romawi === "II") {
            $kelas = 2;
        } elseif ($romawi === "III") {
            $kelas = 3;
        } elseif ($romawi === "UTAMA") {
            $kelas = 4;
        }
        return $kelas;
    }

    // @4
    public function save()
    {
        $id = $this->input->post('idpasien');
        $kelasoperasi = $this->input->post('kelastarif');
        $jenisoperasi = $this->input->post('jenisoperasi');

        $data = array(
            'idpendaftaranoperasi' => $id,
            'kelasoperasi' => $this->getkelas($kelasoperasi),
            'jenisoperasi' => $jenisoperasi,
            'keteranganoperasi' => $this->input->post('ketoperasi'),
            'jenisanesthesi' => $this->input->post('jenisanasi'),
            'tanggaloperasi' => $this->input->post('tanggaloperasi'),
            'ruanganoperasi' => $this->input->post('ruangoperasi'),
            'waktumulaioperasi' => $this->input->post('waktuawal'),
            'waktuselesaioperasi' => $this->input->post('waktuakhir'),
            'nominaljasa' => $this->input->post('jasmed'),
            'persendokteranesthesi' => $this->input->post('dapersen'),
            'idasistenoperator' => $this->input->post('assoperator'),
            'persenasistenoperator' => $this->input->post('aopercent'),
            'idasistenanesthesi' => $this->input->post('assanasi'),
            'persenasistenanesthesi' => $this->input->post('anpercent'),
            'iddokterkonsulen' => $this->input->post('dokterkonsulen'),
            'idinstrument' => $this->input->post('instruments'),
            'biayapenggunaan' => $this->getbiayapenggunaan($id),
            'biayakamar' => $this->input->post('biayakamar'),
            'catatan' => $this->input->post('catatan'),
            'statusverif' => 1,
            'status' => 1,
        );

        if (count($this->model->getsewaalat($id)) != 0) {
            $arr_sewaalat = $this->model->getsewaalat($id);
            foreach ($arr_sewaalat as $arr) {
                $arr_alat = $this->model->getalat($arr->idalat, $this->getkelas($kelasoperasi), $jenisoperasi);
                $data_a = [
                    'jasasarana' => $arr_alat[0]->jasasarana,
                    'jasapelayanan' => $arr_alat[0]->jasapelayanan,
                    'bhp' => $arr_alat[0]->bhp,
                    'biayaperawatan' => $arr_alat[0]->biayaperawatan,
                    'total' => $arr_alat[0]->total,
                    'totalkeseluruhan' => $arr_alat[0]->total * $arr->kuantitas,
                ];
                $this->model->updatesewaalat($id, $arr->idalat, $data_a);
            }
        }

        $status = $this->input->post('status');
        $result = $this->model->save($data, $status, $id);
        if ($result) {
            $check = $this->model->updatestatus($id, $diagnosa = $this->input->post('diagnosa'), $this->input->post('operasi'));
            if ($check) {

                // *** SET AUTO TARIF FULL CARE { @gunalirezqimauludi 2020-05-11 }

                // *** end

                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'Sudah Tersimpan !');
                redirect('tkamar_operasi', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Belum Tersimpan !');
            redirect('tkamar_operasi', 'location');
        }
    }


    /* @section index &start */
    // @get tanggal(kamaroperasi) ASC & DESC
    public function gettanggalko()
    {
        $choose = $this->input->post('choose');
        $result = $this->model->gettanggalko($choose);
        $this->output->set_output(json_encode($result));
    }
    // @get pasien(kamaroperasi)
    // public function loadKO()
    // {
    // $asalpasien = $this->session->userdata('asalpasien');
    // $tglawal = $this->session->userdata('tglawal');
    // $tglakhir = $this->session->userdata('tglakhir');
    // $waktu = $this->session->userdata('waktu');
    // $status = $this->session->userdata('status');
    // $idtipe = $this->session->userdata('idtipe');
    // $dokter = $this->session->userdata('dokter');
    // $filter_jam = $this->session->userdata('filter_jam');

    // $result = $this->model->getpasienko_l($asalpasien, $tglawal, $tglakhir, $waktu, $status, $idtipe, $dokter, $filter_jam);
    // $data = array('data' => $result);
    // $this->output->set_output(json_encode($data));
    // }
    // @get pasiend(kamaroperasi)
    public function getpasienko_de()
    {
        $id = $_GET['id'];
        $result = $this->model->getpasienko_de($id);
        $this->output->set_output(json_encode($result));
    }
    // @check (kamaroperasi)
    public function checkko()
    {
        $time = $_GET['time'];
        $date = $_GET['date'];
        $result = $this->model->checkkopendaftaran($time, $date);
        $this->output->set_output(json_encode($result));
    }
    // @approve (kamaroperasi)
    public function approveko()
    {
        $id = $this->input->post('id');
        $idapprove = $this->input->post('idapprove');
        $idpendaftaran = $this->input->post('idpendaftaran');

        $approve = $this->model->approveko($id, $idapprove, $idpendaftaran);
        if ($approve) {
            $this->getpasienko_l();
        }
    }
    // @update (kamaroperasi)
    public function updateko()
    {
        $id = $this->input->post('id');
        $data = array(
            'diagnosa' => $this->input->post('diagnosa'),
            'operasi' => $this->input->post('dooperasi'),
            'tanggaloperasi' => $this->input->post('tanggaloperasi'),
            'waktumulaioperasi' => $this->input->post('jamoperasi'),
        );

        $update = $this->model->updatepasienko($id, $data);
        if ($update) {
            $this->getpasienko_l();
        }
    }
    // @delete (kamaroperasi)
    public function deleteko()
    {
        $id = $this->input->post('id');
        $idpendaftaran = $this->input->post('idpendaftaran');

        $delete = $this->model->deletepasienko($id, $idpendaftaran);
        if ($delete) {
            $this->getpasienko_l();
        }
    }
    /* @section index &end */

    /* section manage &start */
    // @save
    public function savepenggunaan()
    {
        $check = $this->input->post('check');
        $detail = $this->input->post('detail');
        switch ($check) {
            case '1':
                $result = $this->model->savedatanarcose($detail);
                break;
            case '2':
                $result = $this->model->savedataobat($detail);
                break;
            case '3':
                $result = $this->model->savedataalkes($detail);
                break;
            case '4':
                $result = $this->model->savedataimplant($detail);
                break;
            case '5':
                $result = $this->model->savedataalat($detail);
                break;
        }

        if ($result == true) {
            $this->getpasienko_l();
        }
    }
    // @delete
    public function deletepenggunaan()
    {
        $id = $this->input->post('id');
        $check = $this->input->post('check');

        $idtindakan = $this->input->post('idtindakan');

        $query = $this->model->deletepenggunaan($id, $check, $idtindakan);
        $this->output->set_output(json_encode($query));
    }

    /* penggunaan - penggunaan &start */
    // @get narcose(kamaroperasi)
    public function getallnarcose($id)
    {
        $result = $this->model->getallnarcose($id);
        $data['data'] = $result;
        $this->output->set_output(json_encode($data));
    }
    // @get obat(kamaroperasi)
    public function getallobat($id)
    {
        $result = $this->model->getallobat($id);
        $data['data'] = $result;
        $this->output->set_output(json_encode($data));
    }
    // @get alkes(kamaroperasi)
    public function getallalkes($id)
    {
        $result = $this->model->getallalkes($id);
        $data['data'] = $result;
        $this->output->set_output(json_encode($data));
    }
    // @get implan(kamaroperasi)
    public function getallimplant($id)
    {
        $result = $this->model->getallimplant($id);
        $data['data'] = $result;
        $this->output->set_output(json_encode($data));
    }
    // @get sewaalat(kamaroperasi)
    // @------------------------------------------------------------------------
    public function getallsewaalat($id)
    {
        $result = $this->model->getallsewaalat($id);
        $data['data'] = $result;
        $this->output->set_output(json_encode($data));
    }
    // @------------------------------------------------------------------------
    /* penggunaan- penggunaan &end */

    /* @stok (kamaroperasi) &start */
    // @get stok(narcose, obat, alkes, implan) &kamaroperasi
    public function formula($hargadasar, $margin)
    {
        if ($margin == 0) {
            $hargajual = $hargadasar;
        } else {
            $hargajual = ((int) $hargadasar * ((int) $margin / 100)) + (int) $hargadasar;
        }
        return $hargajual;
    }
    // @------------------------------------------------------------------------
    public function getstok()
    {
        $idunit = $this->input->post('idunit');
        $idtipe = $this->input->post('idtipe');
        $idkategori = $this->input->post('idkategori');
        $kelompokpasien = $this->input->post('kelompokpasien');
        $nama = $this->input->post('nama');

        $result = $this->model->getstok($idunit, $idtipe, $idkategori, $kelompokpasien, $nama);
        $this->output->set_output(json_encode($result));
    }

    // @make:function getstokobat &start
    public function getstokd()
    {
        $idunit = $this->input->post('idunit');
        $idtipe = $this->input->post('idtipe');
        $idkategori = $this->input->post('idkategori');
        $kelompokpasien = $this->input->post('kelompokpasien');

        $result = $this->model->getstok($idunit, $idtipe, $idkategori, $kelompokpasien);

        $data = array();

        switch ($idtipe) {
            // @show (Alat Kesehatan)
            case 1:
                foreach ($result as $row) {
                    $rows = array();

                    $rows[] = $row->kode;
                    $rows[] = $row->namaalkes;
                    $rows[] = number_format($this->formula($row->harga, $row->margin));
                    $rows[] = $row->harga;
                    $rows[] = $row->margin;
                    $rows[] = 3;
                    $rows[] = $row->idalkes;
                    $rows[] = $idunit;
                    $rows[] = '<label class="label label-success">' . $row->stok . '</label>';
                    $rows[] = $this->formula($row->harga, $row->margin);

                    $data[] = $rows;
                }
                break;

            // @show (Narcose) & (Obat)
            case 3:
                if ($idkategori != null) {
                    foreach ($result as $row) {
                        $rows = array();

                        $rows[] = $row->kode;
                        $rows[] = $row->namaobat;
                        $rows[] = number_format($this->formula($row->harga, $row->margin));
                        $rows[] = $row->harga;
                        $rows[] = $row->margin;
                        $rows[] = 1;
                        $rows[] = $row->idobat;
                        $rows[] = $idunit;
                        $rows[] = '<label class="label label-success">' . number_format($row->stok) . '</label>';
                        $rows[] = $this->formula($row->harga, $row->margin);

                        $data[] = $rows;
                    }
                } else {
                    foreach ($result as $row) {
                        $rows = array();

                        $rows[] = $row->kode;
                        $rows[] = $row->namaobat;
                        $rows[] = number_format($this->formula($row->harga, $row->margin));
                        $rows[] = $row->harga;
                        $rows[] = $row->margin;
                        $rows[] = 2;
                        $rows[] = $row->idobat;
                        $rows[] = $idunit;
                        $rows[] = '<label class="label label-success">' . $row->stok . '</label>';
                        $rows[] = $this->formula($row->harga, $row->margin);

                        $data[] = $rows;
                    }
                }
                break;
            case 2:
                foreach ($result as $row) {
                    $rows = array();

                    $rows[] = $row->kode;
                    $rows[] = $row->namaimplan;
                    $rows[] = number_format($this->formula($row->hargadasar, $row->margin));
                    $rows[] = $row->hargadasar;
                    $rows[] = $row->margin;
                    $rows[] = 4;
                    $rows[] = $row->id;
                    $rows[] = 0;
                    $rows[] = '<label class="label label-success">' . $row->stok . '</label>';
                    $rows[] = $this->formula($row->hargadasar, $row->margin);

                    $data[] = $rows;
                }
                break;
        }

        $output = array(
            // "draw" => $_POST['draw'],
            "recordsTotal" => count($result),
            "recordsFiltered" => count($result),
            "data" => $data,
        );

        echo json_encode($output);
    }
    // @make:function getstokobat *error &end

    // @------------------------------------------------------------------------
    // @get stok(sewaalat) &kamaroperasi
    public function getstoksewaalat()
    {
        $kelas = $this->input->post('kelas');
        $jenis = $this->input->post('jenis');

        $result = $this->model->getstoksewaalat($kelas, $jenis);
        $this->output->set_output(json_encode($result));
    }

    // @make:function getstoksewaalat &start
    public function getstoksewalatd()
    {
        $kelas = $this->input->post('kelas');
        $jenis = $this->input->post('jenis');

        $result = $this->model->getstoksewaalat($kelas, $jenis);

        $data = array();
        foreach ($result as $row) {
            $rows = array();

            $rows[] = $row->idtarif;
            $rows[] = $row->nama;
            $rows[] = number_format($row->total);
            $rows[] = $row->jasasarana;
            $rows[] = $row->jasapelayanan;
            $rows[] = 5;
            $rows[] = $row->bhp;
            $rows[] = $row->biayaperawatan;
            $rows[] = $row->total;

            $data[] = $rows;
        }

        $output = array(
            "recordsTotal" => count($result),
            "recordsFiltered" => count($result),
            "data" => $data,
        );

        echo json_encode($output);
    }
    // @make:function getstoksewaalat &end

    // @get searchstok(narcose) &kamaroperasi
    public function searchstok_na()
    {
        $idunit = 3;
        $idtipe = 3;
        $idkategori = 1;

        $result = $this->model->getstok($idunit, $idtipe, $idkategori);
        $this->output->set_output(json_encode($result));
    }
    // @get searchstok(obat) &kamaroperasi
    public function searchstok_ob()
    {
        $idunit = 3;
        $idtipe = 3;
        $idkategori = 2;

        $result = $this->model->getstok($idunit, $idtipe, $idkategori);
        $this->output->set_output(json_encode($result));
    }
    // @get searchstok(alkes) &kamaroperasi
    public function searchstok_al()
    {
        $idunit = 3;
        $idtipe = 1;

        $result = $this->model->getstok($idunit, $idtipe);
        $this->output->set_output(json_encode($result));
    }
    // @get searchstok(implan) &kamaroperasi
    public function searchstok_im()
    {
        $idunit = 3;
        $idtipe = 2;

        $result = $this->model->getstok($idunit, $idtipe);
        $this->output->set_output(json_encode($result));
    }
    // @get searchstok(sewaalat) &kamaroperasi
    public function searchstok_at()
    {
        $kelas = $this->input->post('kelas');
        $jenis = $this->input->post('jenis');
        $nama = $this->input->post('nama');

        $result = $this->model->getstoksewaalat($kelas, $jenis, $nama);
        $this->output->set_output(json_encode($result));
    }
    /* @stok (kamaroperasi) &end */

    /* @estimasi (kamaroperasi) &start */
    // @get estimasi(kamaroperasi)
    public function getestimasi($id)
    {
        $result = $this->model->getestimasi($id);
        $data['data'] = $result;
        $this->output->set_output(json_encode($data));
    }
    // @get estimasidetail(kamaroperasi)
    public function getestimasidetail()
    {
        $id = $this->input->post('id');

        $result = $this->model->getestimasidetail($id);
        $this->output->set_output(json_encode($result));
    }
    // @get estimasiimplan(kamaroperasi)
    public function getestimasiimplan()
    {
        $id = $this->input->post('id');

        $result = $this->model->getestimasiimplan($id);
        $this->output->set_output(json_encode($result));
    }
    /* @estimasi (kamaroperasi) &end */

    /* @jasamedis (kamaroperasi) &start */
    public function savejasadokter()
    {
        $iddokter = $this->input->post('iddokter');
        $namadokter = $this->input->post('namadokter');
        $jenisdokter = $this->input->post('jenisdokter');
        $idtindakan = $this->input->post('idtindakan');
        $nominal = $this->input->post('nominal');
        $persen = $this->input->post('persen');
        $this->model->savejasadokter($iddokter, $namadokter, $jenisdokter, $idtindakan, $nominal, $persen);
    }

    public function getdokteroperator()
    {
        $idtindakan = $this->input->post('idtindakan');
        $iddokter = $this->input->post('iddokter');
        $result = $this->model->getdokteroperator($idtindakan, $iddokter);
        $this->output->set_output(json_encode($result));
    }

    public function updatedokteroperator()
    {
        $id = $this->input->post('id');
        $data = [
            'iddokteroperator' => $this->input->post('iddokter'),
            'namadokteroperator' => $this->input->post('namadokter'),
            'tanggal_ubah' => date("Y-m-d H:i:s"),
            'iduser_ubah' => $this->session->userdata('user_id'),
        ];
        $this->model->updatedokteroperator($id, $data);
    }

    public function deletedokteroperator()
    {
        $id = $this->input->post('id');
        $idtindakan = $this->input->post('idtindakan');
        $this->model->deletedokteroperator($id, $idtindakan);
    }

    public function getdokteranesthesi()
    {
        $idtindakan = $this->input->post('idtindakan');
        $iddokter = $this->input->post('iddokter');
        $result = $this->model->getdokteranesthesi($idtindakan, $iddokter);
        $this->output->set_output(json_encode($result));
    }

    public function updatedokteranesthesi()
    {
        $id = $this->input->post('id');
        $data = [
            'iddokteranesthesi' => $this->input->post('iddokter'),
            'namadokteranesthesi' => $this->input->post('namadokter'),
            'tanggal_ubah' => date("Y-m-d H:i:s"),
            'iduser_ubah' => $this->session->userdata('user_id'),
        ];
        $this->model->updatedokteranesthesi($id, $data);
    }

    public function deletedokteranesthesi()
    {
        $id = $this->input->post('id');
        $idtindakan = $this->input->post('idtindakan');
        $this->model->deletedokteranesthesi($id, $idtindakan);
    }
    /* @jasamedis (kamaroperasi) &end */

    /* Method Print */
    public function print_rincian($id)
    {
        if ($id != '') {
            $data['pasienkod'] = $this->model->getpasienko_detail($id); // it's not done yet !\

            $data['dokterOperator'] = $this->model->getDokterOperatorPrint($id);
            $data['dokterAnesthesi'] = $this->model->getdokterAnesthesiPrint($id);
            $data['catatan'] = $this->model->getCatatanPrint($id);

            $data['penggunaanNarcose'] = $this->model->getallnarcose($id);
            $data['penggunaanObat'] = $this->model->getallobat($id);
            $data['penggunaanAlkes'] = $this->model->getallalkes($id);
            $data['penggunaanImplant'] = $this->model->getallimplant($id);
            $data['penggunaanSewaAlat'] = $this->model->getallsewaalat($id);

            $data['error'] = '';
            $data['title'] = 'Rincian Kamar Operasi';

            // $data['listTindakan'] = $this->model->getListDetail($id);
            $data = array_merge($data, backend_info());
            $this->parser->parse('Tko/print/rincian', $data);
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('tkamar_operasi/index');
        }
    }

    public function getTarifFullcareOK($idpendaftaran)
    {
        $this->db->select('id, idkelompokpasien, COALESCE(idkelas,0) AS idkelas');
        $this->db->where('id', $idpendaftaran);
        $query = $this->db->get('view_index_tkamaroperasi');
        $row = $query->row();

        if ($query->num_rows() > 0) {
            if ($row->idkelas != 0) {
                $this->db->select('mtarif_rawatinap.id AS idtarif,
            mtarif_rawatinap.nama AS namatarif,
            mtarif_rawatinap.path,
            mtarif_rawatinap_detail.jasasarana,
            mtarif_rawatinap_detail.jasapelayanan,
            mtarif_rawatinap_detail.bhp,
            mtarif_rawatinap_detail.biayaperawatan,
            mtarif_rawatinap_detail.total');
                $this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_fullcare5 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 6');
                $this->db->join('mtarif_rawatinap_detail', 'mtarif_rawatinap_detail.idtarif = mtarif_rawatinap.id AND mtarif_rawatinap_detail.kelas = ' . $row->idkelas);
                $this->db->where('mpasien_kelompok.id', $row->idkelompokpasien);
                $query = $this->db->get('mtarif_rawatinap');

                $data = $query->row();
                if ($this->model->setFullCareOK($idpendaftaran, $data)) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return true;
    }
}
