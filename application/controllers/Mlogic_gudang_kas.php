<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlogic_gudang_kas extends CI_Controller {

	/**
	 * Setting Proses Kas controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('mlogic_gudang_kas_model');
  }

	function index(){
		
		$data = $this->mlogic_gudang_kas_model->getSpecified('1');
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Proses Kas';
		$data['content'] 		= 'Mlogic_gudang_kas/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Proses Kas",'#'),
									    			array("List",'mlogic_kasbon')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function list_kategori($idtipe){
		// $idtipe=$this->input->post('idtipe');
		$q="SELECT * FROM mdata_kategori M
			WHERE M.`status`='1' AND M.idtipe='$idtipe'
			ORDER BY M.nama ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	public function list_barang()
    {
        $cari 	= $this->input->post('search');
        $idtipe 	= $this->input->post('idtipe');
        $idkategori 	= $this->input->post('idkategori');
		$where='';
		if ($idkategori!='#' && $idkategori!='0'){
			$where .=" AND M.idkategori='$idkategori' ";
		}
		if ($cari){
			$where .=" AND (M.nama LIKE '%".$cari."%' OR M.kode LIKE '%".$cari."%')";
		}
		$q="SELECT *FROM view_barang M WHERE M.idtipe='$idtipe' ".$where." LIMIT 100";
		// print_r($q);exit();
        $data_obat = $this->db->query($q)->result_array();
        $this->output->set_output(json_encode($data_obat));
    }
	
	//Pembelian
	function load_index()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT S.id,S.idtipe,MT.nama_tipe,S.idkategori,S.operand,S.nominal
				,CASE WHEN S.idkategori='0' THEN 'Semua Kategori' ELSE MK.nama END as kategori 
				,CASE WHEN S.idbarang=0 THEN 'Semua Barang' ELSE CONCAT(B.kode,' - ',B.nama) END as barang,S.idbarang,S.iduser,A.name as username 
				FROM `mlogic_gudang_kas` S
				LEFT JOIN mdata_tipebarang MT ON MT.id=S.idtipe
				LEFT JOIN mdata_kategori MK ON MK.id=S.idkategori AND S.idkategori !=0
				LEFT JOIN view_barang B ON S.idbarang!=0 AND B.idtipe=S.idtipe AND S.idbarang=B.id
				LEFT JOIN musers A ON A.id=S.iduser 
				ORDER BY S.iduser,S.idtipe,S.idkategori,S.idbarang
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('barang','kategori','nama_tipe','username');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = $r->nama_tipe;
            $row[] = ($r->idkategori=='0')?text_default($r->kategori):$r->kategori;
            $row[] = ($r->idbarang=='0')?text_default($r->barang):$r->barang;
            $row[] = (($r->operand=='>=' && $r->nominal=='0')?'BEBAS':$r->operand.' '.number_format($r->nominal,0));
            $row[] = $r->username;
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_beli('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_beli(){
		$id_edit=$this->input->post('id_edit');
		$data=array(
			'setting_id'=>1,
			'idtipe'=>$this->input->post('idtipe'),
			'idkategori'=>$this->input->post('idkategori'),
			'idbarang'=>$this->input->post('idbarang'),
			'iduser'=>$this->input->post('iduser'),			
			'operand'=>$this->input->post('operand'),
			'nominal'=>RemoveComma($this->input->post('nominal')),
							
		);
		
		if ($this->cek_duplicate_beli($data['idtipe'],$data['idkategori'],$data['idbarang'],$data['iduser'])){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('mlogic_gudang_kas',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_beli($idtipe,$idkategori='0',$idbarang='0',$iduser){
		$gabung=$iduser.'-'.$idtipe.'-'.$idkategori.'-'.$idbarang;
		
		$q="SELECT CONCAT(S.iduser,'-',S.idtipe,'-',S.idkategori,'-',S.idbarang),S.* FROM mlogic_gudang_kas S
			WHERE CONCAT(S.iduser,'-',S.idtipe,'-',S.idkategori,'-',S.idbarang)='$gabung' ";
		// print_r($q);
		return $this->db->query($q)->row('id');
	}	
	function hapus_beli($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('mlogic_gudang_kas');
		echo json_encode($result);
	}
	
	
}
