<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_rencana_biaya extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Setting_rencana_biaya_model');
		$this->load->helper('path');
		
  }


	function index($tab='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('1902'))){
				$tab='1';
			}elseif (UserAccesForm($user_acces_form,array('1903'))){
				$tab='2';
			}elseif (UserAccesForm($user_acces_form,array('1904'))){
				$tab='3';
			}elseif (UserAccesForm($user_acces_form,array('1905'))){
				$tab='4';
			}elseif (UserAccesForm($user_acces_form,array('1906'))){
				$tab='5';
			}elseif (UserAccesForm($user_acces_form,array('1907'))){
				$tab='6';
			}
		}
		if (UserAccesForm($user_acces_form,array('1902','1903','1904','1905'))){
			$data = $this->Setting_rencana_biaya_model->get_assesmen_setting();
			$data_label = $this->Setting_rencana_biaya_model->get_assesmen_label();
			$data_persen = $this->Setting_rencana_biaya_model->get_assesmen_persen();
			// print_r($data_label);exit;
			$data['tab'] 			= $tab;
			$data['spesialisasi_id'] 			= '#';
			$data['profesi_id'] 			= '#';
			$data['mppa_id'] 			= '#';
			$data['st_lihat'] 			= '0';
			$data['st_input'] 			= '0';
			$data['st_edit'] 			= '0';
			$data['st_hapus'] 			= '0';
			$data['jenis_isi'] 			= '0';
			$data['st_cetak'] 			= '0';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Estimasi Biaya';
			$data['content'] 		= 'Setting_rencana_biaya/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Bedah",'#'),
												  array("Pengaturan Estimasi Biaya",'setting_rencana_biaya/index')
												);

			$data = array_merge($data,$data_label,$data_persen,backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function save_assesmen(){
		if ($this->Setting_rencana_biaya_model->save_assesmen()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_rencana_biaya/index/1','location');
		}
	}
	function save_label(){
		if ($this->Setting_rencana_biaya_model->save_label()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data tandatangan telah disimpan.');
			redirect('setting_rencana_biaya/index/3','location');
		}
	}
	function save_persen(){
		if ($this->Setting_rencana_biaya_model->save_persen()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data tandatangan telah disimpan.');
			redirect('setting_rencana_biaya/index/6','location');
		}
	}
	function save_default(){
		if ($this->Setting_rencana_biaya_model->save_defaultsave_default()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_rencana_biaya/index/3','location');
		}
	}

  function simpan_hak_akses(){
		$hasil=$this->cek_duplicate_hak_akses($this->input->post('profesi_id'),$this->input->post('spesialisasi_id'),$this->input->post('mppa_id'));
		if ($hasil==null){	
			$this->profesi_id=$this->input->post('profesi_id');
			$this->spesialisasi_id=$this->input->post('spesialisasi_id');
			$this->mppa_id=$this->input->post('mppa_id');
			$this->st_lihat=$this->input->post('st_lihat');
			$this->st_input=$this->input->post('st_input');
			$this->st_edit=$this->input->post('st_edit');
			$this->st_hapus=$this->input->post('st_hapus');
			$this->st_cetak=$this->input->post('st_cetak');
			
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
		
			$hasil=$this->db->insert('setting_rencana_biaya_akses',$this);
		}else{
			$hasil=null;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_hak_akses($profesi_id,$spesialisasi_id,$mppa_id){
		$q="SELECT *FROM setting_rencana_biaya_akses WHERE profesi_id='$profesi_id' AND spesialisasi_id='$spesialisasi_id' AND mppa_id='$mppa_id'";
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	function simpan_formulir(){
		// print_r();exit;
		$hasil=$this->get_duplicat_logic_formulir($this->input->post('idtipe'),$this->input->post('idpoli'),$this->input->post('statuspasienbaru'),$this->input->post('pertemuan_id'),$this->input->post('st_tujuan_terakhir'),$this->input->post('operand'),$this->input->post('lama_terakhir_tujan'));
		if ($hasil==null){
			
			$this->idtipe=$this->input->post('idtipe');
			$this->idpoli=$this->input->post('idpoli');
			$this->statuspasienbaru=$this->input->post('statuspasienbaru');
			$this->pertemuan_id=$this->input->post('pertemuan_id');
			$this->st_tujuan_terakhir=$this->input->post('st_tujuan_terakhir');
			$this->operand=$this->input->post('operand');
			$this->lama_terakhir_tujan=$this->input->post('lama_terakhir_tujan');
			$this->st_formulir=$this->input->post('st_formulir');
			
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
			
		  $hasil=$this->db->insert('setting_rencana_biaya_tampil_formulir',$this);
		}else{
			$hasil=null;
		}
		  
		  $this->output->set_output(json_encode($hasil));
	}
	
	function get_duplicat_logic_formulir($idtipe,$idpoli,$statuspasienbaru,$pertemuan_id,$st_tujuan_terakhir,$operand,$lama_terakhir_tujan){
		$$idpoli=($idpoli=='#'?0:$idpoli);
		$$pertemuan_id=($pertemuan_id=='#'?0:$pertemuan_id);
		if ($operand!='0'){
			
		$where=" AND lama_terakhir_tujan='$lama_terakhir_tujan'";
		}else{
		$where=" ";
			
		}
		$q="SELECT *FROM setting_rencana_biaya_tampil_formulir WHERE idtipe='$idtipe' AND idpoli='$idpoli' AND statuspasienbaru='$statuspasienbaru'  AND st_tujuan_terakhir='$st_tujuan_terakhir' 
				AND pertemuan_id='$pertemuan_id' AND operand='$operand' ".$where;
		// print_r($q);exit;
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	function load_hak_akses()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.id,H.profesi_id,P.ref as profesi
							,H.spesialisasi_id,CASE WHEN H.spesialisasi_id=0 THEN 'SEMUA' ELSE S.ref END as spesialisasi 
							,H.mppa_id,M.nama as nama_ppa,H.st_lihat,H.st_input,H.st_edit,H.st_hapus,H.st_cetak
							FROM `setting_rencana_biaya_akses` H
							LEFT JOIN merm_referensi P ON P.nilai=H.profesi_id AND P.ref_head_id='21' AND P.`status`='1'
							LEFT JOIN merm_referensi S ON S.nilai=H.spesialisasi_id AND S.ref_head_id='22'  AND S.`status`='1'
							LEFT JOIN mppa M ON M.id=H.mppa_id AND M.staktif='1'
							ORDER BY H.profesi_id,H.spesialisasi_id



					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_ppa','spesialisasi','profesi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->profesi);
          $result[] = ($r->spesialisasi_id=='0'?text_default($r->spesialisasi):$r->spesialisasi);
          $result[] = ($r->mppa_id=='0'?text_default('SEMUA'):$r->nama_ppa);
          $result[] = ($r->st_lihat?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_input?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_edit?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_hapus?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_cetak?'IZINKAN':text_danger('TIDAK'));
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_hak_akses('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_hak_akses(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_rencana_biaya_akses',$this);
	  
	  json_encode($hasil);
	  
  }

  function find_mppa($profesi_id){
	  $q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='$profesi_id' AND H.staktif='1'";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($profesi_id!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
  function find_poli($idtipe){
	  $q="SELECT *FROM mpoliklinik H WHERE H.idtipe='$idtipe' AND H.`status`='1'";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($idtipe!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
	function load_formulir()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT 
						CASE WHEN H.idpoli !='0' THEN MP.nama ELSE 'ALL' END as nama_poli
						,CASE WHEN H.pertemuan_id !='0' THEN K.ref ELSE 'ALL' END as kasus
						,CASE WHEN H.idtipe ='1' THEN 'IGD' ELSE 'POLIKLINIK' END as nama_tipe
						,H.* FROM setting_rencana_biaya_tampil_formulir H
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli
						LEFT JOIN merm_referensi K ON K.nilai=H.pertemuan_id AND K.ref_head_id='15'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_poli','kasus','nama_tipe');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = GetTipePasienPiutang($r->idtipe);
          $result[] = ($r->idpoli=='0'?text_default($r->nama_poli):$r->nama_poli);
          $result[] = GetPertemuan($r->statuspasienbaru);
          $result[] = ($r->pertemuan_id=='0'?text_default($r->kasus):$r->kasus);
          $result[] = ($r->st_tujuan_terakhir=='1'?'YA':text_danger('TIDAK'));
          $result[] = ($r->operand=='0'?text_default('TIDAK DITENTUKAN'):$r->operand.' '.$r->lama_terakhir_tujan);
          $result[] = GetFormulir($r->st_formulir);
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_formulir('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_default()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT *FROM (SELECT 
						CASE WHEN H.idpoli !='0' THEN MP.nama ELSE 'ALL' END as nama_poli
						,CASE WHEN H.pertemuan_id !='0' THEN K.ref ELSE 'ALL' END as kasus
						,CASE WHEN H.idtipe ='1' THEN 'IGD' ELSE 'POLIKLINIK' END as nama_tipe
						,MN.singkatan
						,H.* FROM msetting_rencana_biaya_default_detail H
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli
						LEFT JOIN mRencana Bedah MN ON MN.id=H.mRencana Bedah_id
						LEFT JOIN merm_referensi K ON K.nilai=H.pertemuan_id AND K.ref_head_id='15'
						) T ORDER BY T.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_poli','kasus','nama_tipe');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = GetTipePasienPiutang($r->idtipe);
          $result[] = ($r->idpoli=='0'?text_default($r->nama_poli):$r->nama_poli);
          $result[] = GetPertemuan($r->statuspasienbaru);
          $result[] = ($r->pertemuan_id=='0'?text_default($r->kasus):$r->kasus);
          $result[] = ($r->operand_tahun=='0'?text_default('TIDAK DITENTUKAN'):$r->operand_tahun.' '.$r->umur_tahun);
          $result[] = ($r->operand_bulan=='0'?text_default('TIDAK DITENTUKAN'):$r->operand_bulan.' '.$r->umur_bulan);
          $result[] = ($r->operand_hari=='0'?text_default('TIDAK DITENTUKAN'):$r->operand_hari.' '.$r->umur_hari);
          $result[] = ($r->singkatan);
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_default('.$r->id.')" type="button" title="Hapus Default" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
    function hapus_formulir(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_rencana_biaya_tampil_formulir',$this);
	  
	  json_encode($hasil);
	  
  }
  function hapus_default(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('msetting_rencana_biaya_default_detail',$this);
	  
	  json_encode($hasil);
	  
  }
  function simpan_content(){
		$data['header_id']=1;
		$id=$this->input->post('idcontent');
		$data['no']=$this->input->post('no');
		$data['isi']=$this->input->post('isi');
		$data['jenis_isi']=$this->input->post('jenis_isi');
		if ($this->input->post('idcontent')){
			$general_isi_id=$id;
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$id);
			$hasil=$this->db->update('setting_rencana_biaya_isi',$data);
		}else{
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('setting_rencana_biaya_isi',$data);
			$general_isi_id=$this->db->insert_id();
		}
		$this->db->where('general_isi_id',$general_isi_id);		
		$this->db->delete('setting_rencana_biaya_isi_jawaban');
		$ref_id=$this->input->post('ref_id');
		// print_r($ref_id);exit;
		if ($ref_id){
			foreach($ref_id as $index=>$val){
				$data_det['general_isi_id']=$general_isi_id;
				$data_det['ref_id']=$val;
				$hasil=$this->db->insert('setting_rencana_biaya_isi_jawaban',$data_det);
			}
		}
		$this->output->set_output(json_encode($hasil));
	}
	function load_jawaban(){
	  $id=$this->input->post('id');
	  $q="SELECT H.id as ref_id,H.ref,CASE WHEN M.ref_id IS NOT NULL THEN 'selected' ELSE '' END as pilih 
			FROM merm_referensi H
			LEFT JOIN setting_rencana_biaya_isi_jawaban M ON M.ref_id=H.id AND M.general_isi_id='$id'
			WHERE H.ref_head_id='16' AND H.status='1'";
		
	  $hasil= $this->db->query($q)->result();
	  $data='';
	  foreach($hasil as $r){
		  $data .='<option '.$r->pilih.' value="'.$r->ref_id.'">'.$r->ref.'</option>';
	  }
	  $this->output->set_output(json_encode($data));
	  
	}
	function load_content(){
		$tabel='';
		$q="SELECT H.*,C.`name` as user_created,E.`name` as user_edited,GROUP_CONCAT(R.ref SEPARATOR ' / ') as ref FROM setting_rencana_biaya_isi H
			LEFT JOIN musers C ON C.id=H.created_by
			LEFT JOIN musers E ON E.id=H.edited_by
			LEFT JOIN setting_rencana_biaya_isi_jawaban M ON M.general_isi_id=H.id
			LEFT JOIN merm_referensi R ON R.id=M.ref_id
			WHERE H.`status`='1'
			
			GROUP BY H.id
			ORDER BY H.`no` ASC";
		$hasil=$this->db->query($q)->result();
		$no=1;
		foreach($hasil as $r){
			$tabel .='<tr>';
			$tabel .='<td class="text-center">'.$no.'</td>';
			$tabel .='<td>'.$r->isi.'</td>';
			$tabel .='<td class="hidden-xs">'.($r->jenis_isi=='1'?text_danger(jenis_isi($r->jenis_isi)).'<br>'.$r->ref:text_primary(jenis_isi($r->jenis_isi))).'</td>';
			$tabel .='<td class="hidden-xs">'.$r->user_created.'<br>'.($r->created_date?HumanDateLong($r->created_date):'').'</td>';
			$tabel .='<td class="text-center">';
			$tabel .='					<div class="btn-group">';
			$tabel .='						<button class="btn btn-xs btn-primary" type="button" onclick="edit_content('.$r->id.')" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></button>';
			$tabel .='						<button class="btn btn-xs btn-danger" type="button" onclick="hapus('.$r->id.')" data-toggle="tooltip" title="" data-original-title="Remove "><i class="fa fa-times"></i></button>';
			$tabel .='					</div>';
			$tabel .='				</td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		
				
		$this->output->set_output(json_encode($tabel));
	}
	function hapus(){
	  $id=$this->input->post('id');
	  $this->deleted_by = $this->session->userdata('user_id');
	  $this->deleted_date = date('Y-m-d H:i:s');
	  $this->status=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('setting_rencana_biaya_isi',$this);
	  
	  json_encode($hasil);
	  
	}
	function get_edit(){
	  $id=$this->input->post('id');
	  $q="SELECT *FROM setting_rencana_biaya_isi H WHERE H.id='$id'";
	  $hasil= $this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($hasil));
	  
	}
	function find_jenis_operasi($id='')
	{
		$opsi='';
		$opsi .='<option value="" selected> Pilih Opsi </option>';
		if ($id!=''){
			
			$q="SELECT *FROM erm_jenis_operasi H WHERE H.kelompok_id='$id'";
			$list_data=$this->db->query($q)->result();
			
			foreach ($list_data as $r){
				$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			}
		}
		$arr=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function simpan_obat(){
		$hasil=$this->cek_duplicate_obat($this->input->post('kelompok_operasi_id'),$this->input->post('jenis_operasi_id'));
		if ($hasil==null){	
			$this->kelompok_operasi_id = $this->input->post('kelompok_operasi_id');
			$this->jenis_operasi_id = $this->input->post('jenis_operasi_id');
			$this->harga_obat_1 = $this->input->post('harga_obat_1');
			$this->harga_obat_2 = $this->input->post('harga_obat_2');
			$this->harga_obat_3 = $this->input->post('harga_obat_3');
			$this->harga_obat_u = $this->input->post('harga_obat_u');
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
		
			$hasil=$this->db->insert('setting_rencana_biaya_obat',$this);
		}else{
			$hasil=null;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_obat($kelompok_operasi_id,$jenis_operasi_id){
		$q="SELECT *FROM setting_rencana_biaya_obat WHERE kelompok_operasi_id='$kelompok_operasi_id' AND jenis_operasi_id='$jenis_operasi_id' AND status='1'";
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	function load_obat()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT K.nama as kelompok,J.nama as jenis,H.* 
						FROM setting_rencana_biaya_obat H
						LEFT JOIN mkelompok_operasi K ON K.id=H.kelompok_operasi_id 
						LEFT JOIN erm_jenis_operasi J ON J.id=H.jenis_operasi_id
						WHERE H.status='1'
						ORDER BY H.id ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('kelompok','nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->kelompok);
          $result[] = ($r->jenis);
          $result[] = number_format($r->harga_obat_3);
          $result[] = number_format($r->harga_obat_2);
          $result[] = number_format($r->harga_obat_1);
          $result[] = number_format($r->harga_obat_u);
         
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_obat('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_obat(){
	  $id=$this->input->post('id');
	  $this->deleted_by = $this->session->userdata('user_id');
	  $this->deleted_date = date('Y-m-d H:i:s');
	  $this->status=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('setting_rencana_biaya_obat',$this);
	  
	  json_encode($hasil);
	  
	}
	
	//RANAP
	function simpan_ranap(){
		$idrekanan=($this->input->post('idkelompok_pasien')==1?$this->input->post('idrekanan'):0);
		$hasil=$this->cek_duplicate_ranap($this->input->post('icu'),$this->input->post('idkelompok_pasien'),$idrekanan);
		if ($hasil==null){	
			$this->icu = $this->input->post('icu');
			$this->idkelompok_pasien = $this->input->post('idkelompok_pasien');
			$this->idrekanan = $idrekanan;
			$this->harga_1 = $this->input->post('harga_1');
			$this->harga_2 = $this->input->post('harga_2');
			$this->harga_3 = $this->input->post('harga_3');
			$this->harga_u = $this->input->post('harga_u');
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
		
			$hasil=$this->db->insert('setting_rencana_biaya_ranap',$this);
		}else{
			$hasil=null;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_ranap($icu,$idkelompok_pasien,$idrekanan){
		$q="SELECT *FROM setting_rencana_biaya_ranap WHERE icu='$icu' AND idkelompok_pasien='$idkelompok_pasien' AND idrekanan='$idrekanan' AND status='1'";
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	function load_ranap()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT KP.nama as nama_kelompok_pasien,I.ref as nama_icu,MR.nama as nama_rekanan
						,H.* 
						FROM setting_rencana_biaya_ranap H
						LEFT JOIN mpasien_kelompok KP ON KP.id=H.idkelompok_pasien
						LEFT JOIN merm_referensi I ON I.nilai=H.icu AND I.ref_head_id='122'
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						WHERE H.status='1'
						ORDER BY H.id ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_kelompok_pasien','nama_icu','nama_rekanan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->icu=='0'?text_default('SEMUA'):$r->nama_icu);
          $result[] = ($r->idkelompok_pasien=='0'?text_default('SEMUA'):$r->nama_kelompok_pasien);
          $result[] = ($r->idrekanan=='0'?text_default('SEMUA'):$r->nama_rekanan);
          $result[] = number_format($r->harga_3);
          $result[] = number_format($r->harga_2);
          $result[] = number_format($r->harga_1);
          $result[] = number_format($r->harga_u);
         
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_ranap('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_ranap(){
	  $id=$this->input->post('id');
	  $this->deleted_by = $this->session->userdata('user_id');
	  $this->deleted_date = date('Y-m-d H:i:s');
	  $this->status=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('setting_rencana_biaya_ranap',$this);
	  
	  json_encode($hasil);
	  
	}
  //PENUNJANG
	function simpan_penunjang(){
		$idrekanan=($this->input->post('idkelompok_pasien')==1?$this->input->post('idrekanan'):0);
		$hasil=$this->cek_duplicate_penunjang($this->input->post('icu'),$this->input->post('idkelompok_pasien'),$idrekanan);
		if ($hasil==null){	
			$this->icu = $this->input->post('icu');
			$this->idkelompok_pasien = $this->input->post('idkelompok_pasien');
			$this->idrekanan = $idrekanan;
			$this->harga_1 = $this->input->post('harga_1');
			$this->harga_2 = $this->input->post('harga_2');
			$this->harga_3 = $this->input->post('harga_3');
			$this->harga_u = $this->input->post('harga_u');
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
		
			$hasil=$this->db->insert('setting_rencana_biaya_penunjang',$this);
		}else{
			$hasil=null;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_penunjang($icu,$idkelompok_pasien,$idrekanan){
		$q="SELECT *FROM setting_rencana_biaya_penunjang WHERE icu='$icu' AND idkelompok_pasien='$idkelompok_pasien' AND idrekanan='$idrekanan' AND status='1'";
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	function load_penunjang()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT KP.nama as nama_kelompok_pasien,I.ref as nama_icu,MR.nama as nama_rekanan
						,H.* 
						FROM setting_rencana_biaya_penunjang H
						LEFT JOIN mpasien_kelompok KP ON KP.id=H.idkelompok_pasien
						LEFT JOIN merm_referensi I ON I.nilai=H.icu AND I.ref_head_id='122'
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						WHERE H.status='1'
						ORDER BY H.id ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_kelompok_pasien','nama_icu','nama_rekanan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->icu=='0'?text_default('SEMUA'):$r->nama_icu);
          $result[] = ($r->idkelompok_pasien=='0'?text_default('SEMUA'):$r->nama_kelompok_pasien);
          $result[] = ($r->idrekanan=='0'?text_default('SEMUA'):$r->nama_rekanan);
          $result[] = number_format($r->harga_3);
          $result[] = number_format($r->harga_2);
          $result[] = number_format($r->harga_1);
          $result[] = number_format($r->harga_u);
         
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_penunjang('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_penunjang(){
	  $id=$this->input->post('id');
	  $this->deleted_by = $this->session->userdata('user_id');
	  $this->deleted_date = date('Y-m-d H:i:s');
	  $this->status=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('setting_rencana_biaya_penunjang',$this);
	  
	  json_encode($hasil);
	  
	}
	
	//VISIT
	function simpan_visit(){
		$idrekanan=($this->input->post('idkelompok_pasien')==1?$this->input->post('idrekanan'):0);
		$hasil=$this->cek_duplicate_visit($this->input->post('icu'),$this->input->post('idkelompok_pasien'),$idrekanan);
		if ($hasil==null){	
			$this->icu = $this->input->post('icu');
			$this->idkelompok_pasien = $this->input->post('idkelompok_pasien');
			$this->idrekanan = $idrekanan;
			$this->harga_1 = $this->input->post('harga_1');
			$this->harga_2 = $this->input->post('harga_2');
			$this->harga_3 = $this->input->post('harga_3');
			$this->harga_u = $this->input->post('harga_u');
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
		
			$hasil=$this->db->insert('setting_rencana_biaya_visit',$this);
		}else{
			$hasil=null;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_visit($icu,$idkelompok_pasien,$idrekanan){
		$q="SELECT *FROM setting_rencana_biaya_visit WHERE icu='$icu' AND idkelompok_pasien='$idkelompok_pasien' AND idrekanan='$idrekanan' AND status='1'";
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	function load_visit()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT KP.nama as nama_kelompok_pasien,I.ref as nama_icu,MR.nama as nama_rekanan
						,H.* 
						FROM setting_rencana_biaya_visit H
						LEFT JOIN mpasien_kelompok KP ON KP.id=H.idkelompok_pasien
						LEFT JOIN merm_referensi I ON I.nilai=H.icu AND I.ref_head_id='122'
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						WHERE H.status='1'
						ORDER BY H.id ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_kelompok_pasien','nama_icu','nama_rekanan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->icu=='0'?text_default('SEMUA'):$r->nama_icu);
          $result[] = ($r->idkelompok_pasien=='0'?text_default('SEMUA'):$r->nama_kelompok_pasien);
          $result[] = ($r->idrekanan=='0'?text_default('SEMUA'):$r->nama_rekanan);
          $result[] = number_format($r->harga_3);
          $result[] = number_format($r->harga_2);
          $result[] = number_format($r->harga_1);
          $result[] = number_format($r->harga_u);
         
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_visit('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_visit(){
	  $id=$this->input->post('id');
	  $this->deleted_by = $this->session->userdata('user_id');
	  $this->deleted_date = date('Y-m-d H:i:s');
	  $this->status=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('setting_rencana_biaya_visit',$this);
	  
	  json_encode($hasil);
	  
	}
}	
