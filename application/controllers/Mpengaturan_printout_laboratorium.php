<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpengaturan_printout_laboratorium extends CI_Controller {

	/**
	 * Pengaturan Printout Permintaan Laboratorium controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpengaturan_printout_laboratorium_model');
		$this->load->helper('path');
  }

	function index()
	{
		$row = $this->Mpengaturan_printout_laboratorium_model->getSpecified(1);
		if(isset($row->id)){
			$data = array(
				'id' => $row->id,
				'logo' => $row->logo,
				'label_header' => $row->label_header,
				'label_header_eng' => $row->label_header_eng,
				'label_subheader' => $row->label_subheader,
				'label_subheader_eng' => $row->label_subheader_eng,
				'label_footer' => $row->label_footer,
				'label_footer_eng' => $row->label_footer_eng,
				'tampilkan_tanggal_jam_cetak' => $row->tampilkan_tanggal_jam_cetak,
				'tampilkan_tanda_tangan' => $row->tampilkan_tanda_tangan,
			);

			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Pengaturan Printout Permintaan Laboratorium';
			$data['content']	 	= 'Mpengaturan_printout_laboratorium/index';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Pengaturan Printout Permintaan Laboratorium",'#'),
															array("Ubah",'mpengaturan_printout_laboratorium')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpengaturan_printout_laboratorium/index','location');
		}
	}

	function save()
	{
		if($this->Mpengaturan_printout_laboratorium_model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mpengaturan_printout_laboratorium/index','location');
		}
	}
}
