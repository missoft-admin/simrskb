<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mkelas extends CI_Controller {

	/**
	 * Kelas controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mkelas_model');
  }

	function index(){
		
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Kelas';
		$data['content'] 		= 'Mkelas/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Kelas",'#'),
									    			array("List",'mkelas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Kelas';
		$data['content'] 		= 'Mkelas/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Kelas",'#'),
									    			array("Tambah",'mkelas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Mkelas_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Kelas';
				$data['content']	 	= 'Mkelas/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Kelas",'#'),
											    			array("Ubah",'mkelas')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mkelas','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mkelas');
		}
	}

	function delete($id){
		
		$this->Mkelas_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mkelas','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mkelas_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mkelas','location');
				}
			} else {
				if($this->Mkelas_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mkelas','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mkelas/manage';

		if($id==''){
			$data['title'] = 'Tambah Kelas';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Kelas",'#'),
															array("Tambah",'mkelas')
													);
		}else{
			$data['title'] = 'Ubah Kelas';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Kelas",'#'),
															array("Ubah",'mkelas')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
	{
			$this->select = array();
			$this->from   = 'mkelas';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'id' => 'DESC'
			);
			$this->group  = array();

			$this->column_search   = array('nama');
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->nama;
					$aksi = '<div class="btn-group">';
		            if(button_roles('mkelas/update')) {
		                $aksi .= '<a href="'.site_url().'mkelas/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
		            }
		            if(button_roles('mkelas/delete')) {
		                $aksi .= '<a href="#" data-urlindex="'.site_url().'mkelas" data-urlremove="'.site_url().'mkelas/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
		            }
		            $aksi .= '</div>';
  					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
}
