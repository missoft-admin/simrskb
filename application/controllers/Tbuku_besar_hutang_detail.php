<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
class Tbuku_besar_hutang_detail extends CI_Controller {

	/**
	 * Detail Buku Besar Pembantu Hutang controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbuku_besar_hutang_setting_model');
		$this->load->model('Tbuku_besar_hutang_detail_model','model');
  }

	function index($tipe='',$iddistributor=''){
		$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d'));
		date_add($date1,date_interval_create_from_date_string("-130 days"));
		date_add($date2,date_interval_create_from_date_string("0 days"));
		$date1= date_format($date1,"Y-m-d");
		$date2= date_format($date2,"Y-m-d");

		$data = array();
		$data['list_distributor'] 			= $this->Tbuku_besar_hutang_setting_model->list_distributor();
		// print_r($data['list_distributor']);exit();
		$data['tanggal_trx1'] 			= HumanDateShort($date1);
		$data['tanggal_trx2'] 			= HumanDateShort($date2);
		// $data['tipe'] 			= $tipe;
		// $data['iddistributor'] 	= $iddistributor;
		$data['iddistributor_gabungan'] 	= '';
		if ($iddistributor !=''){
			$data['iddistributor_gabungan'] 	= $tipe.'-'.$iddistributor;
		}
		$data['error'] 			= '';
		$data['title'] 			= 'Detail Buku Besar Pembantu Hutang';
		$data['content'] 		= 'Tbuku_besar_hutang_detail/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Detail Buku Besar Pembantu Hutang",'#'),
									    			array("List",'mdistributor')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function getIndex()
	{
		// GetTipeDistributor
		$where='';
		$iddistributor=$this->input->post('iddistributor');
		$tipe_transaksi=$this->input->post('tipe_transaksi');
		
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		if ($iddistributor){
			$iddistributor=implode(',',$iddistributor);			
		}
		if ($tipe_transaksi){
			$tipe_transaksi=implode(',',$tipe_transaksi);			
		}
		
	
		if ($tanggal_trx1 !=''){
			$where .=" AND H.tanggal_transaksi >='".YMDFormat($tanggal_trx1)."' AND H.tanggal_transaksi <='".YMDFormat($tanggal_trx2)."'";
		}
		if ($iddistributor){
		$where .=" AND CONCAT(H.tipe_distributor,'-',H.iddistributor) IN (".$iddistributor.")";
			
		}
		if ($tipe_transaksi){
		$where .=" AND (H.tipe_transaksi) IN (".$tipe_transaksi.")";
			
		}
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

		$this->select = array();
		$from="
			(
				SELECT CONCAT(H.tipe_distributor,'-',H.iddistributor) as str_distributor,H.*,CONCAT(A.noakun,' - ',A.namaakun) as akun,COALESCE(MD.kode,'') as kode from tbuku_besar_hutang H
				INNER JOIN makun_nomor A ON A.id=H.idakun
				LEFT JOIN mdistributor MD ON MD.id=H.iddistributor AND H.tipe_distributor='1'
				WHERE H.id IS NOT NULL ".$where."
				ORDER BY H.tipe_distributor,H.iddistributor,H.tanggal_transaksi ASC
			)tbl
		";
		$this->from   = $from;
		$this->join 	= array();
		$this->where  = array();
		$this->order  = array();
		$this->group  = array();
		$saldo_awal=0;
		$this->column_search   = array();
		$this->column_order    = array();

		$list = $this->datatable->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$str_distributor='';
		foreach ($list as $r) {
		  $no++;
		  $row = array();
		  if ($str_distributor != $r->str_distributor){
			  if ($tanggal_trx1==''){
				  $tanggal_trx1=$r->tanggal_transaksi;
			  }
			$saldo_awal=get_saldo_awal_hutang($r->tipe_distributor,$r->iddistributor,YMDFormat($tanggal_trx1));
			$str_distributor = $r->str_distributor;
		  }
		   $url        = site_url('tkontrabon_verifikasi/');
            $url_gudang        = site_url('tgudang_penerimaan/');
            $url_pengajuan        = site_url('mrka_pengajuan/');
            $url_batal       = site_url('tgudang_pengembalian/');
		  $row[] = HumanDateShort($r->tanggal_transaksi);
		  $row[] = $r->kode;
		  $row[] = $r->nama_distributor;
		  $row[] = $this->tipe_pemesanan2($r->tipe_transaksi);
		  $row[] = $r->notransaksi;
		  $row[] = number_format($saldo_awal,2);
		  $row[] = number_format($r->debet,2);
		  $row[] = number_format($r->kredit,2);
		  $saldo_awal=$saldo_awal + $r->kredit - $r->debet;
		  $row[] = number_format($saldo_awal,2);
			   $aksi = '<div class="btn-group">';          
			  if ($r->tipe_transaksi =='1'){
					$aksi .= '<a class="view btn btn-xs btn-default" href="'.$url_gudang.'detail/'.$r->idtransaksi.'/1'.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-eye"></i></a>';
					$aksi .= '<a href="'.$url_gudang.'print_data/'.$r->idtransaksi.'" type="button" target="_blank" title="Print" target="_blank" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';				
			  }elseif ($r->tipe_transaksi =='2'){
					$aksi .= '<a class="view btn btn-xs btn-default" href="'.$url_batal.'detail/'.$r->idtransaksi.'/1'.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-eye"></i></a>';	
					$aksi .= '<a href="'.$url_batal.'print_pengembalin/'.$r->idtransaksi.'" type="button" target="_blank" title="Edit" target="_blank" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';				
			  }elseif ($r->tipe_transaksi =='3'){
					$aksi .= '<a class="view btn btn-xs btn-default" href="'.$url_pengajuan.'update/'.$r->idtransaksi.'/disabled'.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-eye"></i></a>';
				  
			  }
			// if ($r->ref_tabel=='tgudang_penerimaan') {
			// }else{				
			// }
		// }else{
		// }
		
					// $aksi .= '<button data-toggle="tooltip" title="Lihat" class="btn btn-default btn-sm" ><i class="fa fa-eye"></i></button>';          
					// $aksi .= '<button data-toggle="tooltip" title="Print" class="btn btn-success btn-sm" ><i class="fa fa-print"></i></button>';          
			  $aksi .= '</div>';
		
		  $row[] = $aksi;

		  $data[] = $row;
		}
		$output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(),
		  "recordsFiltered" => $this->datatable->count_all(),
		  "data" => $data
		);
		echo json_encode($output);
	}

	
	function tipe_pemesanan2($tipe){
		if ($tipe=='1'){
			$tipe='<span class="label label-primary">PEMBELIAN</span>';
		}
		if ($tipe=='2'){
			$tipe='<span class="label label-danger">RETUR</span>';
			
		}
		if ($tipe=='3'){
			$tipe='<span class="label label-warning">PENGAJUAN</span>';
		}
		if ($tipe=='4'){
			$tipe='<span class="label label-warning">PEMBAYARAN</span>';
		}
		return $tipe;
	}
	public function export()
    {
		// print_r($this->input->post());exit();
        ini_set('memory_limit', '-1');
        set_time_limit(1000);
        $where='';
		$iddistributor=$this->input->post('iddistributor');
		$tipe_transaksi=$this->input->post('tipe_transaksi');
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		$data=array(
			'judul'=>'BUKU BESAR PEMBANTU HUTANG',
			'distributor'=>'-All Distributor-',
			'tanggal'=>'',
			'tanggal_trx1'=>$tanggal_trx1,
		);
		
		if ($tanggal_trx1 !=''){
			$where .=" AND H.tanggal_transaksi >='".YMDFormat($tanggal_trx1)."' AND H.tanggal_transaksi <='".YMDFormat($tanggal_trx2)."'";
			$data['tanggal']=HumanDateShort(YMDFormat($tanggal_trx1)).' s/d '.HumanDateShort(YMDFormat($tanggal_trx2));
		}
		if ($iddistributor){
			$iddistributor=implode(',',$iddistributor);		
			$where .=" AND CONCAT(H.tipe_distributor,'-',H.iddistributor) IN (".$iddistributor.")";
			$data['distributor']=$this->model->getDistributor($iddistributor);
			// print_r($data['distributor']);exit();
		}
		if ($tipe_transaksi){
			$tipe_transaksi=implode(',',$tipe_transaksi);	
			$where .=" AND (H.tipe_transaksi) IN (".$tipe_transaksi.")";
		}
		
		$from="
				SELECT CONCAT(H.tipe_distributor,'-',H.iddistributor) as str_distributor,H.*,CONCAT(A.noakun,' - ',A.namaakun) as akun,COALESCE(MD.kode,'') as kode from tbuku_besar_hutang H
				INNER JOIN makun_nomor A ON A.id=H.idakun
				LEFT JOIN mdistributor MD ON MD.id=H.iddistributor AND H.tipe_distributor='1'
				WHERE H.id IS NOT NULL ".$where."
				ORDER BY H.tipe_distributor,H.iddistributor,H.tanggal_transaksi ASC
			";
				// print_r($from);exit();
        $btn    = $this->input->post('btn_export');
        $row_detail=$this->db->query($from)->result();
        
		if ($btn=='1'){
			$this->excel($row_detail,$data);
		}
		if ($btn=='2'){
			$this->pdf($row_detail,$data);
		}
		if ($btn=='3'){
			$this->pdf($row_detail,$data);
		}
		
		
		
    }
	public function pdf($row_detail,$header){
		// print_r($header);exit();
		$dompdf = new Dompdf();
		$data=array();
		$data=array_merge($data,$header);
        
        $data['detail'] = $row_detail;

		$data = array_merge($data, backend_info());

        $html = $this->load->view('Tbuku_besar_hutang_detail/pdf', $data, true);
		// print_r($html	);exit();
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Jurnal Kas.pdf', array("Attachment"=>0));
	}
	
	function excel($row_detail,$data){
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle($data['judul']);

        // Set Logo
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $logo = FCPATH.'/assets/upload/logo/logoreport.jpg';
        $objDrawing->setPath($logo);
        $objDrawing->setCoordinates('B1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setHeight(70);
        $objDrawing->setWidth(40);
        $objDrawing->setWorksheet($activeSheet);

        // Set Header
        $activeSheet->setCellValue('B1', "                                      RUMAH SAKIT KHUSUS BEDAH HALMAHERA SIAGA");
        $activeSheet->setCellValue('B2', "                                      Jl.LLRE.Martadinata no.28, Telp: +6222-4206061");
        $activeSheet->setCellValue('B3', "                                      Bandung, 40115");
        $activeSheet->mergeCells('B1:AA1');
        $activeSheet->mergeCells('B2:AA2');
        $activeSheet->mergeCells('B3:AA3');
        $activeSheet->getStyle('B1:AA1')->getFont()->setSize(11);
        $activeSheet->getStyle('B2:AA2')->getFont()->setSize(11);
        $activeSheet->getStyle('B3:AA3')->getFont()->setSize(11);

		$activeSheet->setCellValue('B5', "DISTRIBUTOR");
		$activeSheet->setCellValue('C5', $data['distributor']);
		$activeSheet->setCellValue('B6', "TANGGAL");
		$activeSheet->setCellValue('C6', $data['tanggal']);
		// $activeSheet->setCellValue('B7', "TANGGAL");
		// $activeSheet->setCellValue('C7', $data['tanggal1'].' s/d'. $data['tanggal2']);
		
		// Set Title
		$activeSheet->setCellValue('B9', $data['judul']);
		$activeSheet->mergeCells('B9:H9');
		$activeSheet->getStyle('B9')->getFont()->setBold(true)->setSize(14);
		$activeSheet->getStyle('B9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$x = 11;
		$activeSheet->setCellValue("B$x", "TANGGAL");
		$activeSheet->setCellValue("C$x", "DISTRIBUTOR");
		$activeSheet->setCellValue("D$x", "TIPE");
		$activeSheet->setCellValue("E$x", "KETERANGAN");
		$activeSheet->setCellValue("F$x", "SALDO AWAL");
		$activeSheet->setCellValue("G$x", "DEBET");
		$activeSheet->setCellValue("H$x", "KREDIT");
		$activeSheet->setCellValue("I$x", "SALDO AKHIR");
		$activeSheet->getStyle("B$x:I$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("B$x:I$x")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
	  'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK,
					)
				),
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Calibri'
				)
			)
		);

		
		$debet = 0;
		$kredit= 0;
		$saldo_awal=0;
		$saldo_akhir=0;
		$str_distributor='';
		$tanggal_trx1=$data['tanggal_trx1'];
        if (COUNT($row_detail)) {
            foreach ($row_detail as $row) {
				$x = $x+1;
				if ($str_distributor != $row->str_distributor){
					if ($tanggal_trx1==''){
						  $data['tanggal_trx1']=$row->tanggal_transaksi;
					}
					$saldo_awal=get_saldo_awal_hutang($row->tipe_distributor,$row->iddistributor,YMDFormat($tanggal_trx1));
					$str_distributor = $row->str_distributor;
				}
				
	            $activeSheet->setCellValue("B$x", HumanDateShort($row->tanggal_transaksi));
	            $activeSheet->setCellValue("C$x", $row->nama_distributor);
	            $activeSheet->setCellValue("D$x", $this->model->tipe_pemesanan_biasa($row->tipe_transaksi));
	            $activeSheet->setCellValue("E$x", $row->notransaksi);
	            $activeSheet->setCellValue("F$x", $saldo_awal);
	            $activeSheet->setCellValue("G$x", $row->debet);
	            $activeSheet->setCellValue("H$x", $row->kredit);
				$saldo_awal=(int)$saldo_awal+(int)$row->kredit-(int)$row->debet;
				// $kredit=(int)$kredit+(int)$row->kredit;
				// $saldo_awal=$saldo_awal + $row->kredit - $row->debet;
	            $activeSheet->setCellValue("I$x", $saldo_awal);
				
	           
				
				
			}
		}
		// $x = $x+1;
		
		$activeSheet->getStyle("B11:D$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("H11:I$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$activeSheet->getStyle("E11:E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$activeSheet->getStyle("F11:I$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$activeSheet->getStyle("F11:I$x")->getNumberFormat()->setFormatCode('#,##0');
		$activeSheet->getStyle("B11:I$x")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
					)
				)
			)
		);
		
    	// Set Auto Width Column
    	$activeSheet->getColumnDimension("A")->setWidth(5);
    	$activeSheet->getColumnDimension("B")->setAutoSize(true);
    	$activeSheet->getColumnDimension("C")->setAutoSize(true);
    	$activeSheet->getColumnDimension("D")->setAutoSize(true);
    	$activeSheet->getColumnDimension("E")->setAutoSize(true);
    	$activeSheet->getColumnDimension("F")->setAutoSize(true);
    	$activeSheet->getColumnDimension("G")->setAutoSize(true);
    	$activeSheet->getColumnDimension("H")->setAutoSize(true);
    	$activeSheet->getColumnDimension("i")->setAutoSize(true);
    	// $activeSheet->getColumnDimension("H")->setWidth(45);
    	// $activeSheet->getColumnDimension("H")->setAutoSize(true);

    	header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    	header('Content-Disposition: attachment;filename="'.$data['judul'].' '.date('Y-m-d-h-i-s').'.xls"');
    	header('Cache-Control: max-age=0');

    	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    	$objWriter->save('php://output');
	}
	
}
