<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manatomi extends CI_Controller {

	
	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Manatomi_model');
		$this->load->helper('path');
		
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1679'))){
			$data = array();
			$data['error'] 			= '';
			$data['title'] 			= 'Anatomi Tubuh';
			$data['content'] 		= 'Manatomi/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Anatomi Tubuh",'#'),
												  array("List",'manatomi')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create(){
		
		$data = array(
			'id'            => '',
			'nama' 					=> '',
			'nama_english' 					=> '',
			'nourut' 					=> '',
		);

		$data['error'] 			= '';
		// $data['list_layanan'] 			= $this->Manatomi_model->list_layanan('');
		// $data['list_sound'] 			= $this->Antrian_layanan_model->list_sound();
		// $data['list_user'] 			= $this->Manatomi_model->list_user('');
		$data['title'] 			= 'Tambah Anatomi Tubuh';
		$data['content'] 		= 'Manatomi/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Anatomi Tubuh",'#'),
								            array("Tambah",'manatomi')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$data= $this->Manatomi_model->getSpecified($id);
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Anatomi Tubuh';
			$data['content']    = 'Manatomi/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Anatomi Tubuh",'#'),
										array("Ubah",'manatomi')
										);

			// $data['statusAvailableApoteker'] = $this->Manatomi_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('manatomi');
		}
	}

	function delete($id){
		
		$result=$this->Manatomi_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('manatomi','location');
	}
	function pilih($id){
		
		$result=$this->Manatomi_model->pilih($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('manatomi','location');
	}
	function aktifkan($id){
		
		$result=$this->Manatomi_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){
		if ($this->input->post('btn_simpan')=='2'){
			$q="UPDATE manatomi SET staktif='0' WHERE staktif='1'";
			$this->db->query($q);
		}
		if($this->input->post('id') == '' ) {
			if($this->Manatomi_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('manatomi/create','location');
			}
		} else {
			if($this->Manatomi_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('manatomi/index','location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Manatomi/manage';

		if($id==''){
			$data['title'] = 'Tambah Anatomi Tubuh';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Anatomi Tubuh",'#'),
							               array("Tambah",'manatomi')
								           );
		}else{
			$data['title'] = 'Ubah Anatomi Tubuh';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Anatomi Tubuh",'#'),
							               array("Ubah",'manatomi')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$where='';
			
			$this->select = array();
			$from="
					(
						select H.*
						FROM manatomi H 
						
						ORDER BY H.nourut
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $r->nourut;
          $result[] = $r->nama.' - '.$r->nama_english;
          $result[] = ($r->staktif=='1'?text_primary('AKTIF'):text_default('TIDAK AKTIF'));
         
          $aksi = '<div class="btn-group">';
			if (UserAccesForm($user_acces_form,array('1681'))){
			$aksi .= '<a href="'.site_url().'manatomi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
			}
			if ($r->staktif=='0'){
				
				if (UserAccesForm($user_acces_form,array('1683'))){
					$aksi .= '<button title="Aktifkan" type="button" onclick="aktifkan('.$r->id.')" class="btn btn-warning btn-xs "><i class="si si-check"></i></button>';
				}
			}
			if ($r->staktif=='1'){
				if (UserAccesForm($user_acces_form,array('1682'))){
					$aksi .= '<button title="Hapus" type="button" onclick="removeData('.$r->id.')" class="btn btn-danger btn-xs "><i class="fa fa-trash-o"></i></button>';
				}
			}
		
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
	
}
