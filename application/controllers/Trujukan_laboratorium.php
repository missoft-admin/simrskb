<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

class Trujukan_laboratorium extends CI_Controller
{
	/**
	 * Rujukan Laboratorium controller.
	 * Developer @gunalirezqimauludi
	 */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Trujukan_laboratorium_model');
		$this->load->model('Mtarif_laboratorium_model');
	}

	public function index()
	{
		$data = [
			'nomedrec' => '',
			'namapasien' => '',
			'idasalpasien' => '',
			'idtipe' => '1',
			'idtindakan' => '',
			'iddokterperujuk' => '',
			'iddokterlaboratorium' => '',
			'tanggaldari' => date('d/m/Y', strtotime('-1 day')),
			'tanggalsampai' => date('d/m/Y'),
			'status' => '',
		];

		$data['error'] = '';
		$data['title'] = 'Rujukan Laboratorium';
		$data['content'] = 'Trujukan_laboratorium/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rujukan Laboratorium', '#'],
			['List Pasien', 'Trujukan_laboratorium']
		];

		$data['list_level0'] = $this->Mtarif_laboratorium_model->getLevel0(1);
		$data['list_tindakan'] = $this->Trujukan_laboratorium_model->getListTarifLaboratorium();
		$data['list_dokterperujuk'] = $this->Trujukan_laboratorium_model->getListDokterPerujuk();
		$data['list_dokterlaboratorium'] = $this->Trujukan_laboratorium_model->getListDokterLaboratorium();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter()
	{
		$data = [
			'nomedrec' => $this->input->post('nomedrec'),
			'namapasien' => $this->input->post('namapasien'),
			'idasalpasien' => $this->input->post('idasalpasien'),
			'idtipe' => $this->input->post('idtipe'),
			'idtindakan' => $this->input->post('idtindakan'),
			'iddokterperujuk' => $this->input->post('iddokterperujuk'),
			'iddokterlaboratorium' => $this->input->post('iddokterlaboratorium'),
			'tanggaldari' => $this->input->post('tanggaldari'),
			'tanggalsampai' => $this->input->post('tanggalsampai'),
			'status' => $this->input->post('status'),
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Rujukan Laboratorium';
		$data['content'] = 'Trujukan_laboratorium/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rujukan Laboratorium', '#'],
			['List Pasien', 'Trujukan_laboratorium']
		];

		$data['list_level0'] = $this->Mtarif_laboratorium_model->getLevel0($this->input->post('idtipe'));
		$data['list_tindakan'] = $this->Trujukan_laboratorium_model->getListTarifLaboratorium();
		$data['list_dokterperujuk'] = $this->Trujukan_laboratorium_model->getListDokterPerujuk();
		$data['list_dokterlaboratorium'] = $this->Trujukan_laboratorium_model->getListDokterLaboratorium();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function create($id)
	{
		if ($id != '') {
			$row = $this->Trujukan_laboratorium_model->getSpecified($id);
			$data = [
				'id' => $id,
				'idpendaftaran' => $row->idpendaftaran,
				'tanggalrujukan' => date('d/m/Y'),
				'wakturujukan' => date('H:i:s'),
				'norujukan' => $row->norujukan,
				'nomedrec' => $row->nomedrec,
				'namapasien' => $row->namapasien,
				'jeniskelamin' => GetJenisKelamin($row->jeniskelamin),
				'alamat' => $row->alamat,
				'tanggallahir' => $row->tanggallahir,
				'umur_tahun' => $row->umur_tahun,
				'umur_bulan' => $row->umur_bulan,
				'umur_hari' => $row->umur_hari,
				'namarekanan' => $row->namarekanan,
				'iddokterperujuk' => $row->iddokterperujuk,
				'namadokterperujuk' => $row->namadokterperujuk,
				'iddokterlaboratorium' => ($row->iddokterlaboratorium ? $row->iddokterlaboratorium : 167),
				'namadokterlaboratorium' => $row->namadokterlaboratorium,
				'idtipe' => $row->idtipe,
				'asalrujukan' => $row->asalrujukan,
				'namadokterpoliklinik' => $row->namadokterpoliklinik,
				'namapoliklinik' => $row->namapoliklinik,
				'idkelompokpasien' => $row->idkelompokpasien,
				'idrekanan' => $row->idrekanan,
				'namarekanan' => $row->namarekanan,
				'namakelompok' => $row->namakelompok,
				'idkelas' => $row->idkelas,
				'kelas' => $row->kelas,
				'bed' => $row->bed,
				'namadokterdpjp' => $row->namadokterdpjp,
				'catatan' => $row->catatan,

				'idtarif_bpjstk' => $row->idtarif_bpjstk,
				'tarif_bpjstk' => $row->tarif_bpjstk,
				'kodebpjskesehatan' => $row->kodebpjskesehatan,
				'tarif_bpjs_kesehatan' => $row->tarif_bpjs_kesehatan,

				'idpengambilsample' => $row->idpengambilsample,
				'tanggal_pengambilan_sample' => ($row->tanggal_pengambilan_sample ? DMYTimeFormat($row->tanggal_pengambilan_sample) : date('d/m/Y H:i:s')),
				'idpengujisample' => $row->idpengujisample,
				'tanggal_pengujian_sample' => ($row->tanggal_pengujian_sample ? DMYTimeFormat($row->tanggal_pengujian_sample) : date('d/m/Y H:i:s')),
				'idpengirimhasil' => $row->idpengirimhasil,
				'tanggal_pengiriman_hasil' => ($row->tanggal_pengiriman_hasil ? DMYTimeFormat($row->tanggal_pengiriman_hasil) : date('d/m/Y H:i:s')),
				'idpenerimahasil' => $row->idpenerimahasil,
				'tanggal_penerimaan_hasil' => ($row->tanggal_penerimaan_hasil ? DMYTimeFormat($row->tanggal_penerimaan_hasil) : date('d/m/Y H:i:s')),

				'stedit' => ($row->tanggal_input_pemeriksaan ? 1 : 0),
			];
			$data['error'] = '';
			$data['title'] = 'Create Rujukan Laboratorium';
			$data['content'] = 'Trujukan_laboratorium/manage';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['Rujukan Laboratorium', '#'],
				['Create', 'Trujukan_laboratorium']
			];

			$data['list_dokterperujuk'] = $this->Trujukan_laboratorium_model->getListDokterPerujuk();
			$data['list_dokterlaboratorium'] = $this->Trujukan_laboratorium_model->getListDokterLaboratorium();
			$data['list_level0'] = $this->Trujukan_laboratorium_model->getHeadParentTarifLaboratorium(1, $row->idkelompokpasien, $row->idrekanan);
			$data['list_pegawai'] = $this->Trujukan_laboratorium_model->getPegawai();

			$data['status'] = 2;

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_laboratorium/index');
		}
	}

	public function edit($id)
	{
		if ($id != '') {
			$row = $this->Trujukan_laboratorium_model->getSpecified($id);
			$data = [
				'id' => $id,
				'idpendaftaran' => $row->idpendaftaran,
				'tanggalrujukan' => DMYFormat($row->tanggalrujukan),
				'wakturujukan' => HISTimeFormat($row->tanggalrujukan),
				'norujukan' => $row->norujukan,
				'nomedrec' => $row->nomedrec,
				'namapasien' => $row->namapasien,
				'jeniskelamin' => GetJenisKelamin($row->jeniskelamin),
				'alamat' => $row->alamat,
				'tanggallahir' => $row->tanggallahir,
				'umur_tahun' => $row->umur_tahun,
				'umur_bulan' => $row->umur_bulan,
				'umur_hari' => $row->umur_hari,
				'iddokterperujuk' => $row->iddokterperujuk,
				'namadokterperujuk' => $row->namadokterperujuk,
				'iddokterlaboratorium' => $row->iddokterlaboratorium,
				'namadokterlaboratorium' => $row->namadokterlaboratorium,
				'idtipe' => $row->idtipe,
				'asalrujukan' => $row->asalrujukan,
				'namadokterpoliklinik' => $row->namadokterpoliklinik,
				'namapoliklinik' => $row->namapoliklinik,
				'idkelompokpasien' => $row->idkelompokpasien,
				'idrekanan' => $row->idrekanan,
				'namarekanan' => $row->namarekanan,
				'namakelompok' => $row->namakelompok,
				'idkelas' => $row->idkelas,
				'kelas' => $row->kelas,
				'bed' => $row->bed,
				'namadokterdpjp' => $row->namadokterdpjp,
				'catatan' => $row->catatan,

				'namaasuransi' => $row->namarekanan,
				'idtarif_bpjstk' => $row->idtarif_bpjstk,
				'tarif_bpjstk' => $row->tarif_bpjstk,
				'kodebpjskesehatan' => $row->kodebpjskesehatan,
				'tarif_bpjs_kesehatan' => $row->tarif_bpjs_kesehatan,

				'idpengambilsample' => $row->idpengambilsample,
				'tanggal_pengambilan_sample' => ($row->tanggal_pengambilan_sample ? DMYTimeFormat($row->tanggal_pengambilan_sample) : ''),
				'idpengujisample' => $row->idpengujisample,
				'tanggal_pengujian_sample' => ($row->tanggal_pengujian_sample ? DMYTimeFormat($row->tanggal_pengujian_sample) : ''),
				'idpengirimhasil' => $row->idpengirimhasil,
				'tanggal_pengiriman_hasil' => ($row->tanggal_pengiriman_hasil ? DMYTimeFormat($row->tanggal_pengiriman_hasil) : ''),
				'idpenerimahasil' => $row->idpenerimahasil,
				'tanggal_penerimaan_hasil' => ($row->tanggal_penerimaan_hasil ? DMYTimeFormat($row->tanggal_penerimaan_hasil) : ''),

				'stedit' => ($row->tanggal_input_pemeriksaan ? 1 : 0),
			];
			$data['error'] = '';
			$data['title'] = 'Create Rujukan Laboratorium';
			$data['content'] = 'Trujukan_laboratorium/manage';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['Rujukan Laboratorium', '#'],
				['Create', 'Trujukan_laboratorium']
			];

			$data['list_dokterperujuk'] = $this->Trujukan_laboratorium_model->getListDokterPerujuk();
			$data['list_dokterlaboratorium'] = $this->Trujukan_laboratorium_model->getListDokterLaboratorium();
			$data['list_level0'] = $this->Trujukan_laboratorium_model->getHeadParentTarifLaboratorium(1, $row->idkelompokpasien, $row->idrekanan);
			$data['list_detail'] = $this->Trujukan_laboratorium_model->getListDetail($id);

			$data['list_pegawai'] = $this->Trujukan_laboratorium_model->getPegawai();

			if ($row->status == 1) {
				$data['status'] = 2;
			} elseif ($row->status == 2) {
				$data['status'] = 2;
			} elseif ($row->status == 3) {
				$data['status'] = 3;
			}

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_laboratorium/index');
		}
	}

	public function review($id)
	{
		if ($id != '') {
			$row = $this->Trujukan_laboratorium_model->getSpecified($id);
			$data = [
				'id' => $id,
				'tanggalrujukan' => DMYFormat($row->tanggalrujukan),
				'wakturujukan' => HISTimeFormat($row->tanggalrujukan),
				'norujukan' => $row->norujukan,
				'nomedrec' => $row->nomedrec,
				'namapasien' => $row->namapasien,
				'jeniskelamin' => GetJenisKelamin($row->jeniskelamin),
				'alamat' => $row->alamat,
				'tanggallahir' => $row->tanggallahir,
				'umur_tahun' => $row->umur_tahun,
				'umur_bulan' => $row->umur_bulan,
				'umur_hari' => $row->umur_hari,
				'namarekanan' => $row->namarekanan,
				'iddokterperujuk' => $row->iddokterperujuk,
				'namadokterperujuk' => $row->namadokterperujuk,
				'iddokterlaboratorium' => $row->iddokterlaboratorium,
				'namadokterlaboratorium' => $row->namadokterlaboratorium,
				'idtipe' => $row->idtipe,
				'asalrujukan' => $row->asalrujukan,
				'namadokterpoliklinik' => $row->namadokterpoliklinik,
				'namapoliklinik' => $row->namapoliklinik,
				'idkelompokpasien' => $row->idkelompokpasien,
				'namarekanan' => $row->namarekanan,
				'namakelompok' => $row->namakelompok,
				'idkelas' => $row->idkelas,
				'kelas' => $row->kelas,
				'bed' => $row->bed,
				'namadokterdpjp' => $row->namadokterdpjp,
				'catatan' => $row->catatan,

				'idkelompokpasien' => $row->idkelompokpasien,
				'kelompokpasien' => $row->namakelompok,
				'namaasuransi' => $row->namarekanan,
				'idtarif_bpjstk' => $row->idtarif_bpjstk,
				'tarif_bpjstk' => $row->tarif_bpjstk,
				'kodebpjskesehatan' => $row->kodebpjskesehatan,
				'tarif_bpjs_kesehatan' => $row->tarif_bpjs_kesehatan,

				'idpengambilsample' => $row->idpengambilsample,
				'tanggal_pengambilan_sample' => ($row->tanggal_pengambilan_sample ? DMYTimeFormat($row->tanggal_pengambilan_sample) : ''),
				'idpengujisample' => $row->idpengujisample,
				'tanggal_pengujian_sample' => ($row->tanggal_pengujian_sample ? DMYTimeFormat($row->tanggal_pengujian_sample) : ''),
				'idpengirimhasil' => $row->idpengirimhasil,
				'tanggal_pengiriman_hasil' => ($row->tanggal_pengiriman_hasil ? DMYTimeFormat($row->tanggal_pengiriman_hasil) : ''),
				'idpenerimahasil' => $row->idpenerimahasil,
				'tanggal_penerimaan_hasil' => ($row->tanggal_penerimaan_hasil ? DMYTimeFormat($row->tanggal_penerimaan_hasil) : ''),

				'stedit' => ($row->tanggal_input_hasil ? 1 : 0),
			];

			$data['error'] = '';
			$data['title'] = 'Detail Rujukan Laboratorium';
			$data['content'] = 'Trujukan_laboratorium/review';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['Hasil Tindakan Rujukan Laboratorium', '#'],
				['Hasil', 'Trujukan_laboratorium']
			];

			$data['list_dokterperujuk'] = $this->Trujukan_laboratorium_model->getListDokterPerujuk();
			$data['list_dokterlaboratorium'] = $this->Trujukan_laboratorium_model->getListDokterLaboratorium();
			$data['list_tindakan'] = $this->Trujukan_laboratorium_model->getListReview($id);

			$data['list_pegawai'] = $this->Trujukan_laboratorium_model->getPegawai();

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_laboratorium/index');
		}
	}

	// function view($id)
	// {
	//     if ($id != '') {
	//         $row = $this->Trujukan_laboratorium_model->getSpecified($id);
	//         $data = array(
	//           'id'                 => $id,
	//           'tanggalrujukan'     => $row->tanggalrujukan,
	//           'norujukan'          => $row->norujukan,
	//           'nomedrec'           => $row->nomedrec,
	//           'namapasien'         => $row->namapasien,
	//           'jeniskelamin'       => GetJenisKelamin($row->jeniskelamin),
	//           'alamat'             => $row->alamat,
	//           'tanggallahir'       => $row->tanggallahir,
	//           'umur_tahun'         => $row->umur_tahun,
	//           'umur_bulan'         => $row->umur_bulan,
	//           'umur_hari'          => $row->umur_hari,
	//           'namarekanan'        => $row->namarekanan,
	//           'namadokter'         => $row->namadokter,
	//           'idkelompokpasien'   => $row->idkelompokpasien,
	//           'namakelompok'       => $row->namakelompok,
	//           'catatan'            => $row->catatan
	//         );
	//
	//         $data['error']      = '';
	//         $data['title']      = 'Detail Rujukan Laboratorium';
	//         $data['content']    = 'Trujukan_laboratorium/view';
	//         $data['breadcrum']  = array(
	//                                 array("RSKB Halmahera",'#'),
	//                                 array("Hasil Tindakan Rujukan Laboratorium",'#'),
	//                                 array("Hasil",'Trujukan_laboratorium')
	//                               );
	//
	//         $data['list_tindakan'] = $this->Trujukan_laboratorium_model->getListDetail($id);
	//         $data = array_merge($data, backend_info());
	//         $this->parser->parse('module_template', $data);
	//     } else {
	//         $this->session->set_flashdata('error', true);
	//         $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
	//         redirect('trujukan_laboratorium/index');
	//     }
	// }

	public function print_bukti_pemeriksaan($id)
	{
		if ($id != '') {
			$row = $this->Trujukan_laboratorium_model->getSpecified($id);
			$data = [
				'id' => $id,
				'tanggalrujukan' => $row->tanggalrujukan,
				'norujukan' => $row->norujukan,
				'nomedrec' => $row->nomedrec,
				'namapasien' => $row->namapasien,
				'jeniskelamin' => GetJenisKelamin($row->jeniskelamin),
				'alamat' => $row->alamat,
				'tanggallahir' => $row->tanggallahir,
				'umur_tahun' => $row->umur_tahun,
				'umur_bulan' => $row->umur_bulan,
				'umur_hari' => $row->umur_hari,
				'namarekanan' => $row->namarekanan,
				'iddokterperujuk' => $row->iddokterperujuk,
				'namadokterperujuk' => $row->namadokterperujuk,
				'iddokterlaboratorium' => $row->iddokterlaboratorium,
				'namadokterlaboratorium' => $row->namadokterlaboratorium,
				'idkelompokpasien' => $row->idkelompokpasien,
				'namakelompok' => $row->namakelompok,
				'catatan' => $row->catatan,
				'asalrujukan' => $row->asalrujukan
			];

			$data['error'] = '';
			$data['title'] = 'Pemeriksaan Laboratorium';

			$data['list_tindakan'] = $this->Trujukan_laboratorium_model->getListDetail($id);
			$data = array_merge($data, backend_info());
			$this->parser->parse('Trujukan_laboratorium/print/bukti_pemeriksaan', $data);
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_laboratorium/index');
		}
	}

	public function print_review($id)
	{
		if ($id != '') {
			$row = $this->Trujukan_laboratorium_model->getSpecified($id);
			$data = [
				'id' => $id,
				'tanggalrujukan' => $row->tanggalrujukan,
				'norujukan' => $row->norujukan,
				'noantrian' => $row->noantrian,
				'nomedrec' => $row->nomedrec,
				'namapasien' => $row->namapasien,
				'jeniskelamin' => GetJenisKelamin($row->jeniskelamin),
				'alamat' => $row->alamat,
				'tanggallahir' => $row->tanggallahir,
				'umur_tahun' => $row->umur_tahun,
				'umur_bulan' => $row->umur_bulan,
				'umur_hari' => $row->umur_hari,
				'namarekanan' => $row->namarekanan,
				'iddokterperujuk' => $row->iddokterperujuk,
				'namadokterperujuk' => $row->namadokterperujuk,
				'iddokterlaboratorium' => $row->iddokterlaboratorium,
				'namadokterlaboratorium' => $row->namadokterlaboratorium,
				'idkelompokpasien' => $row->idkelompokpasien,
				'namakelompok' => $row->namakelompok,
				'catatan' => $row->catatan,
				'asalrujukan' => $row->asalrujukan,
				'nolab' => $row->nolab,

				'cetakanke' => $row->cetakanke,
				'user_input_pemeriksaan' => $row->user_input_pemeriksaan,
				'tanggal_input_pemeriksaan' => $row->tanggal_input_pemeriksaan,
				'user_input_sampling' => $row->user_pengambilan_sample,
				'tanggal_input_sampling' => $row->tanggal_pengambilan_sample,
				'user_input_cetak' => $this->session->userdata('user_name'),
				'tanggal_input_cetak' => date('Y-m-d H:i:s'),
				'user_input_hasil' => $row->user_input_hasil,
			];

			$data['error'] = '';
			$data['title'] = 'Hasil Pemeriksaan Laboratorium';

			// Update Log Cetak
			$this->Trujukan_laboratorium_model->updateLogCetak($id);

			$data['list_tindakan'] = $this->Trujukan_laboratorium_model->getListReview($id);
			$data = array_merge($data, backend_info());
			$this->parser->parse('Trujukan_laboratorium/print/hasil_pemeriksaan', $data);
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_laboratorium/index');
		}
	}

	public function print_copy($id)
	{
		if ($id != '') {
			$row = $this->Trujukan_laboratorium_model->getSpecified($id);
			$data = [
				'id' => $id,
				'tanggalrujukan' => $row->tanggalrujukan,
				'norujukan' => $row->norujukan,
				'noantrian' => $row->noantrian,
				'nomedrec' => $row->nomedrec,
				'namapasien' => $row->namapasien,
				'jeniskelamin' => GetJenisKelamin($row->jeniskelamin),
				'alamat' => $row->alamat,
				'tanggallahir' => $row->tanggallahir,
				'umur_tahun' => $row->umur_tahun,
				'umur_bulan' => $row->umur_bulan,
				'umur_hari' => $row->umur_hari,
				'namarekanan' => $row->namarekanan,
				'iddokterperujuk' => $row->iddokterperujuk,
				'namadokterperujuk' => $row->namadokterperujuk,
				'iddokterlaboratorium' => $row->iddokterlaboratorium,
				'namadokterlaboratorium' => $row->namadokterlaboratorium,
				'idkelompokpasien' => $row->idkelompokpasien,
				'namakelompok' => $row->namakelompok,
				'catatan' => $row->catatan,
				'asalrujukan' => $row->asalrujukan,

				'cetakanke' => $row->cetakanke,
				'user_input_pemeriksaan' => $row->user_input_pemeriksaan,
				'tanggal_input_pemeriksaan' => $row->tanggal_input_pemeriksaan,
				'user_input_sampling' => $row->user_pengambilan_sample,
				'tanggal_input_sampling' => $row->tanggal_pengambilan_sample,
				'user_input_cetak' => $this->session->userdata('user_name'),
				'tanggal_input_cetak' => date('Y-m-d H:i:s'),
				'user_input_hasil' => $row->user_input_hasil,
			];

			$data['error'] = '';
			$data['title'] = 'Hasil Pemeriksaan Laboratorium';

			$data['list_tindakan'] = $this->Trujukan_laboratorium_model->getListReview($id);
			$data = array_merge($data, backend_info());
			$this->parser->parse('Trujukan_laboratorium/print/hasil_pemeriksaan_copy', $data);
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_laboratorium/index');
		}
	}

	public function print_bukti_pengambilan($id)
	{
		if ($id != '') {
			$data = [];
			$data['listTindakan'] = $this->Trujukan_laboratorium_model->getListDetail($id);

			$row = $this->Trujukan_laboratorium_model->getSpecified($id);
			if ($row) {
				$data['norujukan'] = $row->norujukan;
				$data['tanggal'] = $row->tanggalrujukan;
				$data['tanggal_pengambilan'] = $row->tanggal_penerimaan_hasil;
				$data['nomedrec'] = $row->nomedrec;
				$data['namapasien'] = $row->namapasien;
				$data['tanggallahir'] = $row->tanggallahir;
				$data['umur'] = $row->umur_tahun;
				$data['umurbulan'] = $row->umur_bulan;
				$data['umurhari'] = $row->umur_hari;
				$data['asalrujukan'] = GetAsalRujukan($row->asalrujukan);
				$data['dokterperujuk'] = $row->namadokterperujuk;
				$data['kelompokpasien'] = $row->namakelompok;
				$data['tarifaktif'] = '-';
				$data['userinput'] = 'Nama User Input';
			}

			$data['error'] = '';
			$data['title'] = 'Bukti Pengambilan Laboratorium';

			$data = array_merge($data, backend_info());
			$this->parser->parse('Trujukan_laboratorium/print/bukti_pengambilan', $data);
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('trujukan_laboratorium/index');
		}
	}

	public function print_bukti_pemeriksaan_thermal($id)
	{
		// instantiate and use the dompdf class
		$options = new Options();
		$options->set('isRemoteEnabled', true);
		$dompdf = new Dompdf($options);

		$data = [];
		$data['listTindakan'] = $this->Trujukan_laboratorium_model->getListDetail($id);

		$row = $this->Trujukan_laboratorium_model->getSpecified($id);
		if ($row) {
			$data['norujukan'] = $row->norujukan;
			$data['tanggal'] = $row->tanggalrujukan;
			$data['nomedrec'] = $row->nomedrec;
			$data['namapasien'] = $row->namapasien;
			$data['tanggallahir'] = $row->tanggallahir;
			$data['umur'] = $row->umur_tahun;
			$data['umurbulan'] = $row->umur_bulan;
			$data['umurhari'] = $row->umur_hari;
			$data['asalrujukan'] = GetAsalRujukan($row->asalrujukan);
			$data['dokterperujuk'] = $row->namadokterperujuk;
			$data['kelompokpasien'] = $row->namakelompok;
			$data['tarifaktif'] = '-';
			$data['userinput'] = 'Nama User Input';
		}

		$html = $this->parser->parse('Trujukan_laboratorium/print/bukti_pemeriksaan_thermal', array_merge($data, backend_info()), true);
		$html = $this->parser->parse_string($html, $data);

		$dompdf->loadHtml($html);

		// // (Optional) Setup the paper size and orientation
		// $customPaperThermal = array(0,0,226,4000);
		// $dompdf->set_paper($customPaperThermal);

		// Render the HTML as PDF
		$dompdf->render();

		// // Output the generated PDF to Browser
		$dompdf->stream('Bukti Pemeriksaan laboratorium.pdf', ['Attachment' => 0]);
	}

	public function print_bukti_pengambilan_thermal($id)
	{
		// instantiate and use the dompdf class
		$options = new Options();
		$options->set('isRemoteEnabled', true);
		$dompdf = new Dompdf($options);

		$data = [];
		$data['listTindakan'] = $this->Trujukan_laboratorium_model->getListDetail($id);

		$row = $this->Trujukan_laboratorium_model->getSpecified($id);
		if ($row) {
			$data['norujukan'] = $row->norujukan;
			$data['tanggal'] = $row->tanggalrujukan;
			$data['nomedrec'] = $row->nomedrec;
			$data['namapasien'] = $row->namapasien;
			$data['tanggallahir'] = $row->tanggallahir;
			$data['umur'] = $row->umur_tahun;
			$data['umurbulan'] = $row->umur_bulan;
			$data['umurhari'] = $row->umur_hari;
			$data['asalrujukan'] = GetAsalRujukan($row->asalrujukan);
			$data['dokterperujuk'] = $row->namadokterperujuk;
			$data['kelompokpasien'] = $row->namakelompok;
			$data['tarifaktif'] = '-';
			$data['userinput'] = 'Nama User Input';
		}

		$html = $this->parser->parse('Trujukan_laboratorium/print/bukti_pengambilan_thermal', array_merge($data, backend_info()), true);
		$html = $this->parser->parse_string($html, $data);

		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$customPaperThermal = [0, 0, 226, 4000];
		$dompdf->set_paper($customPaperThermal);

		// Render the HTML as PDF
		$dompdf->render();

		// // Output the generated PDF to Browser
		$dompdf->stream('Bukti Pengambilan Laboratorium.pdf', ['Attachment' => 0]);
	}

	public function save()
	{
		$data = [];
		$data['idrujukan'] = $_POST['id'];
		if ($this->Trujukan_laboratorium_model->save()) {
			$data = array_merge($data, backend_info());
			$this->parser->parse('Trujukan_laboratorium/redirect/save_tindakan', $data);
		}
	}

	public function save_review()
	{
		$data = [];
		$data['idrujukan'] = $_POST['id'];
		if ($this->Trujukan_laboratorium_model->saveReview()) {
			$data = array_merge($data, backend_info());
			$this->parser->parse('Trujukan_laboratorium/redirect/save_review', $data);
		}
	}

	public function after_save()
	{
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'data telah disimpan.');
		redirect('Trujukan_laboratorium/index', 'location');
	}

	public function delete($id)
	{
		$this->Trujukan_laboratorium_model->deleteTrx($id);
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'data telah terhapus.');
		redirect('trujukan_laboratorium/index', 'location');
	}

	public function getIndex($uri)
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		$this->select = [
			'trujukan_laboratorium.*',
			'tpoliklinik_pendaftaran.tanggaldaftar',
			'tpoliklinik_pendaftaran.id AS idpendaftaran',
			'(CASE
        		WHEN trujukan_laboratorium.asalrujukan = 3 THEN
        			trawatinap_pendaftaran.nopendaftaran
        		ELSE
        			tpoliklinik_pendaftaran.nopendaftaran
        	END) AS nopendaftaran',
			'(CASE
        		WHEN trujukan_laboratorium.asalrujukan = 3 THEN
        			trawatinap_pendaftaran.idtipe
        		ELSE
        			tpoliklinik_pendaftaran.idtipe
        	END) AS idtipe',
			'mfpasien.no_medrec',
			'mfpasien.alamat_jalan AS alamat',
			'mfpasien.nama',
			'tkasir.status AS statuskasir'
		];

		$this->from = 'trujukan_laboratorium';

		$this->join = [
			['trawatinap_pendaftaran', 'trujukan_laboratorium.idtindakan = trawatinap_pendaftaran.id AND trujukan_laboratorium.asalrujukan = 3', 'LEFT'],
			['tpoliklinik_tindakan', 'tpoliklinik_tindakan.id = trujukan_laboratorium.idtindakan AND trujukan_laboratorium.asalrujukan IN (1, 2) AND tpoliklinik_tindakan.status = 1', 'LEFT'],
			['trujukan_laboratorium_detail', 'trujukan_laboratorium_detail.idrujukan = trujukan_laboratorium.id', 'LEFT'],
			['tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran OR tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT'],
			['mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', ''],
			['tkasir', 'tkasir.idtipe IN (1,2) AND tkasir.idtindakan = tpoliklinik_tindakan.id', 'LEFT']
		];

		$this->where = [];

		// FILTER
		if ($uri == 'filter') {
			if ($this->session->userdata('nomedrec') != null) {
				$this->where = array_merge($this->where, ['mfpasien.no_medrec LIKE' => '%' . $this->session->userdata('nomedrec') . '%']);
			}
			if ($this->session->userdata('namapasien') != null) {
				$this->where = array_merge($this->where, ['mfpasien.nama LIKE' => '%' . $this->session->userdata('namapasien') . '%']);
			}
			if ($this->session->userdata('idasalpasien') != 0) {
				$this->where = array_merge($this->where, ['trujukan_laboratorium.asalrujukan' => $this->session->userdata('idasalpasien')]);
			}
			if ($this->session->userdata('tanggaldari') != null) {
				$this->where = array_merge($this->where, ['DATE(trujukan_laboratorium.tanggal) >=' => YMDFormat($this->session->userdata('tanggaldari'))]);
			}
			if ($this->session->userdata('tanggalsampai') != null) {
				$this->where = array_merge($this->where, ['DATE(trujukan_laboratorium.tanggal) <=' => YMDFormat($this->session->userdata('tanggalsampai'))]);
			}
			if ($this->session->userdata('idtindakan') != '0') {
				$this->where = array_merge($this->where, ['trujukan_laboratorium_detail.idlaboratorium' => $this->session->userdata('idtindakan')]);
			}
			if ($this->session->userdata('iddokterperujuk') != '0') {
				$this->where = array_merge($this->where, ['trujukan_laboratorium.iddokterperujuk' => $this->session->userdata('iddokterperujuk')]);
			}
			if ($this->session->userdata('iddokterlaboratorium') != '0') {
				$this->where = array_merge($this->where, ['trujukan_laboratorium.iddokterradiologi' => $this->session->userdata('iddokterradiologi')]);
			}
			if ($this->session->userdata('status') != '#') {
				$this->where = array_merge($this->where, ['trujukan_laboratorium.status' => $this->session->userdata('status')]);
			}
		} else {
			$this->where = array_merge($this->where, ['DATE(trujukan_laboratorium.tanggal)' => date('Y-m-d')]);
		}

		$this->order = [
			'trujukan_laboratorium.tanggal' => 'ASC',
			'trujukan_laboratorium.noantrian' => 'ASC',
		];
		$this->group = ['trujukan_laboratorium.id'];

		$this->column_search = ['trujukan_laboratorium.tanggal', 'trujukan_laboratorium.noantrian', 'trujukan_laboratorium.norujukan', 'trawatinap_pendaftaran.nopendaftaran', 'tpoliklinik_pendaftaran.nopendaftaran', 'mfpasien.no_medrec', 'mfpasien.nama'];
		$this->column_order = ['trujukan_laboratorium.tanggal', 'trujukan_laboratorium.noantrian', 'trujukan_laboratorium.norujukan', 'trawatinap_pendaftaran.nopendaftaran', 'tpoliklinik_pendaftaran.nopendaftaran', 'mfpasien.no_medrec', 'mfpasien.nama'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			if ($r->status == 0) {
				$action = '<label>-</label>';
			} elseif ($r->status == 1) {
				$action = '<div class="btn-group">';
				if (UserAccesForm($user_acces_form, ['1015'])) {
					$action .= '<a href="' . site_url() . 'trujukan_laboratorium/create/' . $r->id . '" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Tindakan"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form, ['1020'])) {
					$action .= '<a href="#" data-urlindex="' . site_url() . 'trujukan_laboratorium" data-urlremove="' . site_url() . 'trujukan_laboratorium/delete/' . $r->id . '" data-messagewarning="Batalkan record tersebut?" data-messagesuccess="Data telah dibatalkan." data-toggle="tooltip" title="Batalkan" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash"></i></a>';
				}
				$action .= '          <div class="btn-group">
                          <div class="btn-group dropup">';
				$action .= '<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                              <span class="fa fa-print"></span>
                            </button>';
				$action .= '<ul class="dropdown-menu">
                              <li>
                                <a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/manage_print/' . $r->idpendaftaran . '">Sticker ID</a>
                              </li>
                            </ul>';
				$action .= '     </div>
                        </div>
                      </div>';
			} elseif ($r->status == 2) {
				$action = '<div class="btn-group">';
				if (UserAccesForm($user_acces_form, ['1022'])) {
					$action .= '<a href="' . site_url() . 'trujukan_laboratorium/review/' . $r->id . '" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Hasil"><i class="fa fa-list"></i></a>';
				}
				if (UserAccesForm($user_acces_form, ['1024', '1025'])) {
					$action .= '<div class="btn-group">
                          <div class="btn-group dropup">';
					$action .= '<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                              <span class="fa fa-print"></span>
                            </button>';
					$action .= '<ul class="dropdown-menu">
                              <li>';
					if (UserAccesForm($user_acces_form, ['1024'])) {
						$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/manage_print/' . $r->idpendaftaran . '">Sticker ID</a>';
					}
					if (UserAccesForm($user_acces_form, ['1025'])) {
						$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_laboratorium/print_bukti_pemeriksaan/' . $r->id . '">Cetak Pemeriksaan</a>';
					}
					$action .= '</li>
                            </ul>';
					$action .= '      </div>
                        </div>
                        </div>
                        &nbsp;';
				}
				if (UserAccesForm($user_acces_form, ['1021'])) {
					$action .= '<div class="btn-group">
                          <a href="' . site_url() . 'trujukan_laboratorium/edit/' . $r->id . '" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>
                        </div>';
				}
			} elseif ($r->status == 3) {
				$action = '<div class="btn-group">';
				if (UserAccesForm($user_acces_form, ['1023'])) {
					$action .= '<a href="' . site_url() . 'trujukan_laboratorium/review/' . $r->id . '" data-toggle="tooltip" title="Lihat" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>';
				}
				$action .= '<div class="btn-group">
                            <div class="btn-group dropup">';
				$action .= '       <button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                <span class="fa fa-print"></span>
                              </button>';
				$action .= '      <ul class="dropdown-menu">
                                <li>';
				if (UserAccesForm($user_acces_form, ['1024'])) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'tpoliklinik_pendaftaran/manage_print/' . $r->idpendaftaran . '">Sticker ID</a>';
				}
				if (UserAccesForm($user_acces_form, ['1025'])) {
					$action .= '          <a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_laboratorium/print_bukti_pemeriksaan/' . $r->id . '">Cetak Bukti Pemeriksaan</a>';
				}
				if (UserAccesForm($user_acces_form, ['1026'])) {
					$action .= '          <a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_laboratorium/print_bukti_pemeriksaan_thermal/' . $r->id . '">Cetak Bukti Pemeriksaan (Small)</a>';
				}
				if (UserAccesForm($user_acces_form, ['1027'])) {
					$action .= '          <a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_laboratorium/print_review/' . $r->id . '">Cetak Hasil</a>';
				}
				if (UserAccesForm($user_acces_form, ['1028'])) {
					$action .= '          <a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_laboratorium/print_copy/' . $r->id . '">Cetak Hasil (Copy)</a>';
				}
				if (UserAccesForm($user_acces_form, ['1029'])) {
					$action .= '          <a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_laboratorium/print_bukti_pengambilan/' . $r->id . '">Cetak Bukti Pengambilan</a>';
				}
				if (UserAccesForm($user_acces_form, ['1030'])) {
					$action .= '          <a tabindex="-1" target="_blank" href="' . site_url() . 'trujukan_laboratorium/print_bukti_pengambilan_thermal/' . $r->id . '">Cetak Bukti Pengambilan (Small)</a>';
				}
				$action .= '         </li>
                              </ul>
                            </div>
                          </div>
                        </div>&nbsp;';

				if (UserAccesForm($user_acces_form, ['1021'])) {
					$action .= '<div class="btn-group">
                          <a href="' . site_url() . 'trujukan_laboratorium/edit/' . $r->id . '" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>
                        </div>';
				}
			}

			if ($r->statuskasir != 2) {
				$action .= '&nbsp;<a href="#" data-urlindex="' . site_url() . 'trujukan_laboratorium" data-urlremove="' . site_url() . 'trujukan_laboratorium/delete/' . $r->id . '" data-messagewarning="Batalkan record tersebut?" data-messagesuccess="Data telah dibatalkan." data-toggle="tooltip" title="Batalkan" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash"></i></a>';
			}

			$row[] = $no;
			$row[] = $r->tanggal;
			$row[] = $r->nolab;
			$row[] = $r->noantrian;
			$row[] = $r->norujukan;
			$row[] = $r->nopendaftaran;
			$row[] = $r->no_medrec;
			$row[] = $r->nama;
			if ($r->asalrujukan == 3 && $r->idtipe == 2) {
				$row[] = 'One Day Surgery (ODS)';
			} else {
				$row[] = GetAsalRujukan($r->asalrujukan);
			}
			$row[] = StatusRujukan($r->status);
			$row[] = $action;

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function getTindakan()
	{
		$idkelas = $this->input->post('idkelas');
		$idkelompokpasien = $this->input->post('idkelompokpasien');
		$idrekanan = $this->input->post('idrekanan');
		$idtipe = $this->input->post('idtipe');
		$idpaket = $this->input->post('idpaket');
		$idtindakan = $this->input->post('idtindakan');
		$idsubheader = json_decode($this->input->post('idsubheader'), true);

		if ($idkelompokpasien == 1) {
			$this->db->select('mtarif_laboratorium.path');
			$this->db->join('mrekanan', 'mrekanan.tlaboratorium_umum = mtarif_laboratorium.id');
			$this->db->where('mrekanan.id', $idrekanan);
			$query = $this->db->get('mtarif_laboratorium');

			if ($query->num_rows() > 0) {
				$row = $query->row();
			} else {
				$this->db->select('mtarif_laboratorium.path');
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_umum = mtarif_laboratorium.id');
				$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
				$query = $this->db->get('mtarif_laboratorium');
				$row = $query->row();
			}
		} else {
			$this->db->select('mtarif_laboratorium.path');
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tlaboratorium_umum = mtarif_laboratorium.id');
			$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
			$query = $this->db->get('mtarif_laboratorium');
			$row = $query->row();
		}

		if ($query->num_rows() > 0) {
			$this->select = [
				'mtarif_laboratorium.id',
				'mtarif_laboratorium.idtipe',
				'mtarif_laboratorium.nama',
				'mtarif_laboratorium.idkelompok',
				'mtarif_laboratorium.idpaket',
				'mtarif_laboratorium.headerpath',
				'mtarif_laboratorium.path',
				'mtarif_laboratorium.level',
				'mtarif_laboratorium_detail.*'
			];

			$this->from = 'mtarif_laboratorium';

			$this->join = [
				['mtarif_laboratorium_detail', 'mtarif_laboratorium_detail.idtarif = mtarif_laboratorium.id', 'LEFT'],
				['mtarif_laboratorium mtarif_paket', 'mtarif_paket.idpaket = 1  AND mtarif_laboratorium_detail.idtarif = mtarif_paket.id AND LEFT ( mtarif_laboratorium.path, LENGTH( mtarif_paket.path ) ) = mtarif_paket.path AND mtarif_laboratorium.path <> mtarif_paket.path', 'LEFT']
			];

			if ($idtindakan != '0') {
				$this->where = [
					'mtarif_laboratorium_detail.kelas' => $idkelas,
					'mtarif_laboratorium_detail.status' => '1',
					'mtarif_laboratorium.idtipe' => $idtipe,
					'mtarif_laboratorium.path LIKE' => $idtindakan . '%'
				];
			} else {
				$this->where = [
					'mtarif_laboratorium_detail.kelas' => $idkelas,
					'mtarif_laboratorium_detail.status' => '1',
					'mtarif_laboratorium.idtipe' => $idtipe,
					'mtarif_laboratorium.path LIKE' => $row->path . '.%'
				];
			}

			if ($idpaket != '2') {
				$this->where = array_merge($this->where, ['mtarif_laboratorium.idpaket' => $idpaket]);
			}

			$this->or_where = [];

			if ($idsubheader) {
				foreach ($idsubheader as $key => $value) {
					$this->or_where[$key] = ['mtarif_laboratorium.path LIKE' => $value . '%'];
				}
			}

			$this->session->set_userdata('sessionOrWhere', $this->or_where);
			
			$this->order = [
				"SUBSTRING_INDEX(CONCAT( mtarif_laboratorium.path ,'.'),'.',1) + 0,
				SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT( mtarif_laboratorium.path ,'.'),'.',2),'.',-1) + 0,
				SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT( mtarif_laboratorium.path ,'.'),'.',3),'.',-1) + 0,
				SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT( mtarif_laboratorium.path ,'.'),'.',4),'.',-1) + 0" => '',
			];

			$this->group = ['mtarif_laboratorium.id'];

			$this->column_search = ['mtarif_laboratorium.nama'];
			$this->column_order = ['mtarif_laboratorium.nama'];

			$list = $this->datatable->get_datatables();
			
			$data = [];
			$no = $_POST['start'];
			foreach ($list as $r) {
				$no++;
				$row = [];

				// if($r->level == 0){
				//    $action = '<a style="cursor: pointer;">'.TreeView($r->level, $r->nama).'</a>';
				// }else{
				//    $action = '<a style="cursor: pointer;" data-idtindakan="'.$r->id.'" data-path="'.$r->path.'" id="tindakan-select" href="javascript:void(0)">'.($r->idpaket == 1 ? '<span class="label label-success" style="width:100%">'.TreeView($r->level, $r->nama).'</span>' : TreeView($r->level, $r->nama)).'</a>';
				// }

				if ($r->idkelompok == 1 && $r->idpaket == 0) {
					$action = TreeView($r->level, $r->nama);
				} else {
					$action = '<a style="cursor: pointer;" data-idtindakan="' . $r->id . '" data-path="' . $r->path . '" id="tindakan-select" href="javascript:void(0)">' . ($r->idpaket == 1 ? '<span class="label label-success" style="width:100%">' . TreeView($r->level, $r->nama) . '</span>' : TreeView($r->level, $r->nama)) . '</a>';
				}

				$row[] = $no;
				$row[] = $action;
				$row[] = number_format($r->jasasarana);
				$row[] = number_format($r->jasapelayanan);
				$row[] = number_format($r->bhp);
				$row[] = number_format($r->biayaperawatan);
				$row[] = number_format($r->total);

				$data[] = $row;
			}
			$output = [
				'draw' => $_POST['draw'],
				'recordsTotal' => $this->datatable->count_all(),
				'recordsFiltered' => $this->datatable->count_all(),
				'data' => $data
			];
			echo json_encode($output);
		} else {
			$output = [
				'draw' => '',
				'recordsTotal' => 0,
				'recordsFiltered' => 0,
				'data' => ''
			];
			echo json_encode($output);
		}
	}

	function find_subparent($idtipe, $idtindakan)
	{
		$query = $this->db->query("SELECT
			mtarif_laboratorium.id,
			mtarif_laboratorium.nama,
			mtarif_laboratorium.path,
			mtarif_laboratorium.level
		FROM
			mtarif_laboratorium
		WHERE
			mtarif_laboratorium.idtipe = '$idtipe' 
			AND mtarif_laboratorium.path LIKE '$idtindakan%' 
			AND mtarif_laboratorium.idkelompok = '1'
			AND mtarif_laboratorium.level = '1'
		GROUP BY
			mtarif_laboratorium.id 
		ORDER BY
			INET_ATON(mtarif_laboratorium.path) ASC");

		$arr = "";
		foreach($query->result() as $row){
			$arr = $arr.'<option value="'.$row->path.'">'.TreeView($row->level, $row->nama).'</option>';
		}

		$this->output->set_output(json_encode($arr));
	}
}

/* End of file Trujukan_laboratorium.php */
/* Location: ./application/controllers/Trujukan_laboratorium.php */
