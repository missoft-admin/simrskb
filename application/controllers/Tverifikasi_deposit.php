<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tverifikasi_deposit extends CI_Controller
{
	/**
	 * Verifikasi Delete Deposit controller.
	 * Developer @gunalirezqimauludi
	 */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tverifikasi_deposit_model');
	}

	public function index()
	{
		$data = [
			'namapasien' => '',
			'tanggaldari' => date('d/m/Y'),
			'tanggalsampai' => date('d/m/Y')
		];

		$data['error'] = '';
		$data['title'] = 'Verifikasi Delete Deposit';
		$data['content'] = 'Tverifikasi_deposit/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Verifikasi Delete Deposit', '#'],
			['List', 'tverifikasi_deposit']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function view_deposit($idrawat_inap, $id = '')
	{
		$data = $this->Tverifikasi_deposit_model->getHeader($idrawat_inap);
		$data['list'] = $this->Tverifikasi_deposit_model->getList($idrawat_inap, $id);
		$data['error'] = '';
		$data['title'] = 'Verifikasi Delete Deposit';
		$data['content'] = 'Tverifikasi_deposit/detail';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Verifikasi Delete Deposit', '#'],
			['List', 'tverifikasi_deposit']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter()
	{
		$data = [
			'namapasien' => $this->input->post('namapasien'),
			'tanggaldari' => $this->input->post('tanggaldari'),
			'tanggalsampai' => $this->input->post('tanggalsampai')
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Verifikasi Delete Deposit';
		$data['content'] = 'Tverifikasi_deposit/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Verifikasi Delete Deposit', '#'],
			['List', 'tverifikasi_deposit']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function updateTransaksi()
	{
		$this->db->set('statusverifikasi', $this->input->post('statusverifikasi'));
		if ($this->input->post('statusverifikasi') == 0) {
			$this->db->set('alasantidaksesuai', $this->input->post('alasantidaksesuai'));
		}
		$this->db->where('id', $this->input->post('iddeposit'));
		if ($this->db->update('trawatinap_deposit')) {
			return true;
		} else {
			return false;
		}
	}

	public function getIndex($uri)
	{
		$this->select = [];
		$this->from = 'view_verifikasi_deposit_index';
		$this->join = [];

		$this->where = [];
		if ($uri == 'filter') {
			if ($this->session->userdata('namapasien') != null) {
				$this->where = array_merge($this->where, ['namapasien LIKE' => '%' . $this->session->userdata('namapasien') . '%']);
			}
			if ($this->session->userdata('tanggaldari') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal) >=' => YMDFormat($this->session->userdata('tanggaldari'))]);
			}
			if ($this->session->userdata('tanggalsampai') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal) <=' => YMDFormat($this->session->userdata('tanggalsampai'))]);
			}
		}

		$this->order = [
			'tanggal' => 'DESC'
		];

		$this->group = [];

		$this->column_search = ['nomedrec', 'namapasien', 'namabank', 'namauserinput', 'namauserdelete'];
		$this->column_order = ['nomedrec', 'namapasien', 'namabank', 'namauserinput', 'namauserdelete'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $row) {
			$no++;
			$result = [];

			$action = '';

			if ($row->statusverifikasi == '') {
				if ($row->status == 0) {
					$action .= '<a href="#" class="btn btn-xs btn-success rowEdit rowVerifikasi" data-iddeposit="' . $row->id . '"><i class="fa fa-check"></i> Sesuai</a>';
					$action .= '<a href="#" class="btn btn-xs btn-danger rowEdit rowUnverifikasi" data-iddeposit="' . $row->id . '" data-toggle="modal" data-target="#RejectNoteModal"><i class="fa fa-times"></i> Tidak Sesuai</a>';
				} else {
					$action .= '-';
				}
			} elseif ($row->statusverifikasi == 0) {
				$action .= '<span class="label label-md label-danger">Tidak Disetujui</span>';
			} elseif ($row->statusverifikasi == 1) {
				$action .= '<span class="label label-md label-success">Disetujui</span>';
			} else {
				$action .= '-';
			}

			$result[] = $no;
			$result[] = $row->tanggal;
			$result[] = $row->nomedrec;
			$result[] = $row->namapasien;
			$result[] = metodePembayaran($row->idmetodepembayaran);
			$result[] = $row->namabank;
			$result[] = number_format($row->nominaldeposit);
			$result[] = $row->namauserinput;
			$result[] = StatusDeleteDeposit($row->status, $row->statusverifikasi);
			$result[] = $row->deleted_date . ' - ' . $row->namauserdelete;
			$result[] = $row->alasanhapus;
			$result[] = $action;

			$data[] = $result;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
