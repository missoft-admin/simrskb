<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_pendapatan_jual_ko extends CI_Controller {

	/**
	 * Setting Jurnal Penjualan Kamar Operasi controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_pendapatan_jual_ko_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_pendapatan_jual_ko_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_pendapatan_jual_ko_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Penjualan Kamar Operasi';
		$data['content'] 		= 'Msetting_jurnal_pendapatan_jual_ko/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Penjualan Kamar Operasi",'#'),
									    			array("List",'mlogic_narcose')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	
	function cek_duplicate($jenis,$idkelas,$idtipe,$idkategori,$idbarang){
		$gabung=$jenis.'-'.$idkelas.'-'.$idtipe.'-'.$idkategori.'-'.$idbarang;
		$q="SELECT *FROM msetting_jurnal_pendapatan_jual_ko S
			WHERE CONCAT(S.jenis,'-',S.idkelas,'-',S.idtipe,'-',S.idkategori,'-',S.idbarang)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus($id)
	{		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_pendapatan_jual_ko');
		echo json_encode($result);
	}
	function load_setting()
    {
		$jenis=$this->input->post('jenis');
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.*
				,MK.nama as kelas
				,K.nama as kategori
				,T.nama_tipe
				,B.nama as namabarang
					 ,GH1.nama as nama_group_pembelian
					 ,GH2.nama as nama_group_diskon					 
					FROM msetting_jurnal_pendapatan_jual_ko H
					
					LEFT JOIN mkelas MK ON MK.id=H.idkelas 
					LEFT JOIN mdata_kategori K ON K.id=H.idkategori
					LEFT JOIN mdata_tipebarang T ON T.id=H.idtipe
					LEFT JOIN view_barang_all B ON B.id=H.idbarang AND B.idtipe=H.idtipe
					LEFT JOIN mgroup_pembayaran GH1 ON GH1.id=H.idgroup_penjualan
					LEFT JOIN mgroup_pembayaran GH2 ON GH2.id=H.idgroup_diskon
					WHERE H.jenis='$jenis'
					GROUP BY H.id
					ORDER BY H.jenis,H.idtipe,H.idkategori,H.idbarang

				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = ($r->idtipe=='0'?text_default('All Tipe'):GetTipeRujukanRawatInap($r->idtipe));
            $row[] = ($r->idkelas=='0'?text_default('All kelas'):$r->kelas);
            $row[] = ($r->idkategori=='0'?text_default('All kategori'):$r->kategori);
            $row[] = ($r->idbarang=='0'?text_default('All Barang'):$r->namabarang);
            $row[] = $r->nama_group_pembelian;
            $row[] = $r->nama_group_diskon;
			$aksi 		= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus('.$r->id.','.$r->jenis.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	//Narcose	
	
	function simpan_narcose(){
		$id_edit=$this->input->post('id_edit');
		$idtipe=($this->input->post('idtipe')=='#'?0:$this->input->post('idtipe'));
		$tipe_barang=($this->input->post('tipe_barang')=='#'?0:$this->input->post('tipe_barang'));
		$idkelas=($this->input->post('idkelas')=='#'?0:$this->input->post('idkelas'));
		$idkategori=($this->input->post('idkategori')=='#'?0:$this->input->post('idkategori'));
		$idbarang=($this->input->post('idbarang')=='#'?0:$this->input->post('idbarang'));
		$idgroup_penjualan=($this->input->post('idgroup_penjualan')=='#'?0:$this->input->post('idgroup_penjualan'));
		$idgroup_diskon=($this->input->post('idgroup_diskon')=='#'?0:$this->input->post('idgroup_diskon'));
		$jenis='1';
		$data=array(
			'setting_id'=>1,
			'jenis'=>$jenis,
			'idtipe'=>$idtipe,
			'tipe_barang'=>$tipe_barang,
			'idkelas'=>$idkelas,
			'idkategori'=>$idkategori,
			'idbarang'=>$idbarang,
			'idgroup_penjualan'=>$idgroup_penjualan,
			'idgroup_diskon'=>$idgroup_diskon,
		);
		
		if ($this->cek_duplicate($jenis,$idkelas,$idtipe,$idkategori,$idbarang)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pendapatan_jual_ko',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	//Obat
	function simpan_obat(){
		$id_edit=$this->input->post('id_edit');
		$idtipe=($this->input->post('idtipe')=='#'?0:$this->input->post('idtipe'));
		$tipe_barang=($this->input->post('tipe_barang')=='#'?0:$this->input->post('tipe_barang'));
		$idkelas=($this->input->post('idkelas')=='#'?0:$this->input->post('idkelas'));
		$idkategori=($this->input->post('idkategori')=='#'?0:$this->input->post('idkategori'));
		$idbarang=($this->input->post('idbarang')=='#'?0:$this->input->post('idbarang'));
		$idgroup_penjualan=($this->input->post('idgroup_penjualan')=='#'?0:$this->input->post('idgroup_penjualan'));
		$idgroup_diskon=($this->input->post('idgroup_diskon')=='#'?0:$this->input->post('idgroup_diskon'));
		$jenis='2';
		$data=array(
			'setting_id'=>1,
			'jenis'=>$jenis,
			'idtipe'=>$idtipe,
			'tipe_barang'=>$tipe_barang,
			'idkelas'=>$idkelas,
			'idkategori'=>$idkategori,
			'idbarang'=>$idbarang,
			'idgroup_penjualan'=>$idgroup_penjualan,
			'idgroup_diskon'=>$idgroup_diskon,
		);
		
		if ($this->cek_duplicate($jenis,$idkelas,$idtipe,$idkategori,$idbarang)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pendapatan_jual_ko',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	//Alkes
	function simpan_alkes(){
		$id_edit=$this->input->post('id_edit');
		$idtipe=($this->input->post('idtipe')=='#'?0:$this->input->post('idtipe'));
		$tipe_barang=($this->input->post('tipe_barang')=='#'?0:$this->input->post('tipe_barang'));
		$idkelas=($this->input->post('idkelas')=='#'?0:$this->input->post('idkelas'));
		$idkategori=($this->input->post('idkategori')=='#'?0:$this->input->post('idkategori'));
		$idbarang=($this->input->post('idbarang')=='#'?0:$this->input->post('idbarang'));
		$idgroup_penjualan=($this->input->post('idgroup_penjualan')=='#'?0:$this->input->post('idgroup_penjualan'));
		$idgroup_diskon=($this->input->post('idgroup_diskon')=='#'?0:$this->input->post('idgroup_diskon'));
		$jenis='3';
		$data=array(
			'setting_id'=>1,
			'jenis'=>$jenis,
			'idtipe'=>$idtipe,
			'tipe_barang'=>$tipe_barang,
			'idkelas'=>$idkelas,
			'idkategori'=>$idkategori,
			'idbarang'=>$idbarang,
			'idgroup_penjualan'=>$idgroup_penjualan,
			'idgroup_diskon'=>$idgroup_diskon,
		);
		
		if ($this->cek_duplicate($jenis,$idkelas,$idtipe,$idkategori,$idbarang)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pendapatan_jual_ko',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	//Implan
	function simpan_implan(){
		$id_edit=$this->input->post('id_edit');
		$idtipe=($this->input->post('idtipe')=='#'?0:$this->input->post('idtipe'));
		$tipe_barang=($this->input->post('tipe_barang')=='#'?0:$this->input->post('tipe_barang'));
		$idkelas=($this->input->post('idkelas')=='#'?0:$this->input->post('idkelas'));
		$idkategori=($this->input->post('idkategori')=='#'?0:$this->input->post('idkategori'));
		$idbarang=($this->input->post('idbarang')=='#'?0:$this->input->post('idbarang'));
		$idgroup_penjualan=($this->input->post('idgroup_penjualan')=='#'?0:$this->input->post('idgroup_penjualan'));
		$idgroup_diskon=($this->input->post('idgroup_diskon')=='#'?0:$this->input->post('idgroup_diskon'));
		$jenis='4';
		$data=array(
			'setting_id'=>1,
			'jenis'=>$jenis,
			'idtipe'=>$idtipe,
			'tipe_barang'=>$tipe_barang,
			'idkelas'=>$idkelas,
			'idkategori'=>$idkategori,
			'idbarang'=>$idbarang,
			'idgroup_penjualan'=>$idgroup_penjualan,
			'idgroup_diskon'=>$idgroup_diskon,
		);
		
		if ($this->cek_duplicate($jenis,$idkelas,$idtipe,$idkategori,$idbarang)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pendapatan_jual_ko',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
}
