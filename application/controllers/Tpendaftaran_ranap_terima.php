<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use Hashids\Hashids;
class Tpendaftaran_ranap_terima extends CI_Controller {

	/**
	 * Checkin controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbooking_rehab_model');
		$this->load->model('Tbooking_model');
		$this->load->model('Tpendaftaran_ranap_terima_model');
		$this->load->model('Tpoliklinik_pendaftaran_model');
		$this->load->model('Tcheckin_model');
		$this->load->helper('path');
		// print_r('sini');exit;
		
  }
	function index($tab='2'){
		insert_awal_history_bed();
		 get_ppa_login();
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab!='#'){			
			if (UserAccesForm($user_acces_form,array('1929'))){
				$tab='3';
			}elseif (UserAccesForm($user_acces_form,array('1928'))){
				$tab='2';
			}elseif (UserAccesForm($user_acces_form,array('1927'))){
				$tab='1';
			}elseif (UserAccesForm($user_acces_form,array('1926'))){
				$tab='0';
			}
		}else{
			$tab_detail='2';
		}
		if (UserAccesForm($user_acces_form,array('1577'))){
			$data = array();
			$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("-7 days"));
			$date2= date_format($date2,"d/m/Y");
			
			$data['list_reservasi_cara'] 			= list_reservasi_cara();
			// print_r($data['list_reservasi_cara']);exit;
			
			$data['list_poli'] 			= $this->Tpendaftaran_ranap_terima_model->list_poli();
			$data['list_dokter'] 			= $this->Tpendaftaran_ranap_terima_model->list_dokter();
			$data['tanggal_1'] 			= $date2;
			$data['tanggal_2'] 			= date('d/m/Y');
			$data['tab_utama'] 			= 3;
			$data['idalasan'] 			= '#';
			$data['waktu_reservasi_1'] 			= '';
			$data['waktu_reservasi_2'] 			= '';
			$data['namapasien'] 			= '';
			$data['no_medrec'] 			= '';
			$data['tab_detail'] 			='2';
			$data['tab'] 			= $tab;
			$tahun=date('Y');
			$bulan=date('m');
			$q="SELECT D.tanggal,DATE_ADD(D.tanggal,INTERVAL 6 DAY) as tanggal_next   FROM date_row D WHERE D.tanggal BETWEEN DATE_ADD(CURRENT_DATE(),INTERVAL -6 DAY) AND CURRENT_DATE()";
			$tanggal=$this->db->query($q)->row();
			$data['tanggaldari'] 			= DMYFormat($tanggal->tanggal);
			$data['tanggalsampai'] 			= DMYFormat($tanggal->tanggal_next);
			$data['error'] 			= '';
			$data['title'] 			= 'Pendaftaran Rawat Inap';
			$data['content'] 		= 'Tpendaftaran_ranap_terima/index';
			$data['list_alasan'] = $this->Tpoliklinik_pendaftaran_model->getAlasan();
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Pendaftaran",'#'),
												  array("Rawat Inap",'tpendaftaran_ranap_terima')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
						
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			// $pencarian =$this->input->post('pencarian');
			$tab =$this->input->post('tab');
			if ($tab=='2'){
				$where .=" AND (H.st_terima_ranap='0' AND H.status='1') ";
			}
			if ($tab=='3'){
				$where .=" AND (H.st_terima_ranap='1' AND H.status='1') ";
			}
			if ($tab=='4'){
				$where .=" AND (H.status='0') ";
			}
			if ($tab=='5'){
				$where .=" AND (H.statuscheckout='1') ";
			}
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.tanggaldaftar) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggaldaftar) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(H.tanggaldaftar) >='".date('Y-m-d')."'";
			}
			// if ($pencarian!=''){
				// $where .=" AND (H.namapasien LIKE '%".$pencarian."%')";
				// $where .=" OR (H.no_medrec LIKE '%".$pencarian."%')";
			// }
			// if ($tab=='2'){
				// $where .=" AND (H.status_input_dokter) = '0'";
			// }
			// if ($tab=='3'){
				// $where .=" AND (H.status_input_dokter) = '1'";
			// }
			// if ($idpoli!='#'){
				// $where .=" AND (H.idpoliklinik) = '$idpoli'";
			// }else{
				// $where .=" AND (H.idpoliklinik NOT IN (SELECT idpoliklinik FROM mpoliklinik_emergency)) ";
				// $where .=" AND (H.idpoliklinik NOT IN (SELECT idpoliklinik FROM mpoliklinik_direct)) ";
			// }
		
			// print_r($tab);exit;
			$this->select = array();
			$from="
					(
						SELECT MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
						,MK.nama as nama_kelompok,MR.nama as nama_rekanan,MDP.nama as nama_dokter_poli
						,H.*,MKL.nama as nama_kelas 
						,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header
						,MB.nama as nama_bed,TP.tanggal,MRU.nama as nama_ruangan,AH.kodeantrian as kode_antrian
						FROM trawatinap_pendaftaran H
						LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpoliklinik
						LEFT JOIN (
							SELECT * FROM `tpoliklinik_assesmen_igd_alergi`
							WHERE status_assemen='2'
							UNION ALL
							SELECT * FROM `tpoliklinik_assesmen_alergi`
							WHERE status_assemen='2'
						) A ON A.idpasien=H.idpasien AND A.status_assemen='2'
						LEFT JOIN mruangan MRU ON MRU.id=H.idruangan
						LEFT JOIN mkelas MKL ON MKL.id=H.idkelas
						LEFT JOIN antrian_harian AH ON AH.id=H.antrian_id
						LEFT JOIN mdokter MDP ON MDP.id=TP.iddokter
						LEFT JOIN mdokter MD ON MD.id=H.iddokterpenanggungjawab
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik_asal
						LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
						LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						LEFT JOIN mbed MB on MB.id=H.idbed						
						WHERE H.idtipe IS NOT NULL ".$where."
						GROUP BY H.id
						
						ORDER BY H.tanggaldaftar DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		  $label_skrining='<i class="fa fa-file-text-o"></i> &nbsp;'.label_skrining_ranap($r->st_lm,$r->st_lembar,$r->st_sp,$r->st_pernyataan,$r->st_gc,$r->st_general,$r->st_hk,$r->st_hak);
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  $btn_hapus='';
		  $btn_terima='';
		  $btn_layani='';
		  $btn_panel='';
		  
			  if ($r->status!='0'){
				  $btn_layani='<div class="push-5-t"><a href="'.base_url().'tpendaftaran_ranap_terima/edit_pendaftaran/'.$r->id.'" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block btn-primary btn-xs menu_click"><i class="fa fa-user-md"></i> LAYANI</a> </div>';
				  if (UserAccesForm($user_acces_form,array('1931'))){
					$btn_hapus='<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Hapus" onclick="show_modal_batal('.$r->id.')" class="btn btn-block btn-danger btn-xs"><i class="fa fa-trash"></i> BATAL PENDAFTARAN</a> </div>';
				  }
				  if (UserAccesForm($user_acces_form,array('1932'))){
						$btn_terima='<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Terima Rawat inap"  class="btn btn-block btn-success btn-xs"><i class="fa fa-bed"></i> TERIMA RAWAT INAP</a> </div>';
				  }
				  $btn_panel=div_panel_kendali($user_acces_form,$r->id);
			  }
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									'.$btn_layani.'
									'.$btn_panel.'
									'.$btn_hapus.'
									'.$btn_terima.'
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
										<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
									</div>
									<div class="text-primary font-s13 push-5-t">'.$r->nopendaftaran_poli.' | '.HumanDateShort($r->tanggal).' | '.GetAsalRujukanR($r->idtipe_asal).' | '.$r->nama_poli.' | '.$r->nama_dokter_poli.'</div>
									<div class="text-primary font-s13 push-5-t">'.$label_skrining.'</div>
									
									</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_5='<table class="block-table text-left">
						
					</table>';
		  
          $result[] = $btn_5;
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class="text-muted text-uppercase">'.($r->nama_ruangan).' - '.$r->nama_kelas.' - '.$r->nama_bed.'</div>
									<div class="text-muted"><i class="fa fa-user-md"></i> '.($r->nama_dokter).'</div>
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->nama_rekanan?$r->nama_rekanan:$r->nama_kelompok).'</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.$this->status_ranap($r->status,$r->st_terima_ranap,$r->statuscheckout).'</div>
									<div class="h4 push-10-t text-primary"><button class="btn btn-xs btn-block btn-primary push-10" type="button"><i class="fa fa-bed pull-left"></i> '.($r->nama_bed).'</button></strong></div>
									<div class="h4 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggaldaftar).'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function status_ranap($status,$st_terima_ranap,$statuscheckout){
	  $label='<button class="btn btn-xs btn-block btn-primary push-10" type="button"><i class="fa fa-facebook pull-left "></i> Login with Facebook</button>';
	  if ($status=='0'){
		  $label='<button class="btn btn-xs btn-block btn-danger push-10" type="button"><i class="fa fa-times pull-left "></i>DIBATALKAN</button>';
	  }else{
		  if ($st_terima_ranap=='0'){
			  $label='<button class="btn btn-xs btn-block btn-info push-10" type="button"><i class="fa fa-wrench pull-left "></i>MENUNGGU DIRAWAT</button>';
		  }
		  if ($st_terima_ranap=='1' && $statuscheckout=='0'){
			  $label='<button class="btn btn-xs btn-block btn-warning push-10" type="button"><i class="fa fa-exclamation-circle pull-left"></i>DIRAWAT</button>';
		  }
		  if ($statuscheckout=='1'){
			  $label='<button class="btn btn-xs btn-block btn-success push-10" type="button">PULANG</button>';
		  }
	  }
	  
	  return $label;
  }
	
	function get_notif(){
		$q="SELECT H.id,H.sound_play FROM `trawatinap_pendaftaran` H WHERE H.st_terima_ranap='1' AND H.st_notif_sound='0' AND H.sound_play!='' AND H.st_play='1'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function update_notif(){
		$id=$this->input->post('id');
		$q="UPDATE `trawatinap_pendaftaran` H SET st_notif_sound='1' WHERE H.id='$id'";
		$hasil=$this->db->query($q);
		$this->output->set_output(json_encode($hasil));
	}
}
