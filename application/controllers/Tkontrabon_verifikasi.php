<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tkontrabon_verifikasi extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tkontrabon_verifikasi_model','model');
	}

	function index() {
		$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d'));
		// $date2=(date('Y-m-d'));
		date_add($date1,date_interval_create_from_date_string("-1 days"));
		date_add($date2,date_interval_create_from_date_string("0 days"));
		$date1= date_format($date1,"d-m-Y");
		$date2= date_format($date2,"d-m-Y");

		$data=array(
			'tipe'=>'1',
			'cara_bayar'=>'2',
			'no_po'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'0',
			'tanggalterima1'=>$date1,
			'tanggalterima2'=>$date2,
			'iddistributor'=>'#',
			'tanggaljt1'=>'',
			'tanggaljt2'=>'',
		);
		$data['error'] 			= '';
		$data['list_distributor'] 	= $this->model->list_distributor();
		$data['title'] 			= 'Verifikasi Kontrabon';
		$data['content'] 		= 'Tkontrabon_verifikasi/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Verifikasi Transaksi",'tverifikasi_transaksi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$tipe=$this->input->post('tipe');
		$iddistributor=$this->input->post('iddistributor');
		$no_po=$this->input->post('no_po');
		$no_terima=$this->input->post('no_terima');
		$no_fakur=$this->input->post('no_fakur');
		$cara_bayar=$this->input->post('cara_bayar');
		$status=$this->input->post('status');
		$tanggalterima1=$this->input->post('tanggalterima1');
		$tanggalterima2=$this->input->post('tanggalterima2');
		$tanggaljt1=$this->input->post('tanggaljt1');
		$tanggaljt2=$this->input->post('tanggaljt2');
		$where1='';
		$where='';
		$where2='';
		$where_pengajuan='';
		if ($tipe !='#'){
			$where2 .=" AND tipe='$tipe' ";
		}
		if ($iddistributor !='#'){
			$where .=" AND H.iddistributor='$iddistributor' ";
			$where1 .=" AND H.iddistributor='$iddistributor' ";
			$where_pengajuan .=" AND H.idvendor='$iddistributor' ";
			
		}
		if ($no_po !=''){
			$where .=" AND P.nopemesanan='$no_po' ";
			$where1 .=" AND P1.nopemesanan='$no_po' ";
			$where_pengajuan .=" AND H.no_pengajuan='$no_po' ";
		}
		if ($no_terima !=''){
			$where .=" AND H.nopenerimaan='$no_terima' ";
			$where1 .=" AND P.nopenerimaan='$no_terima' ";
		}
		if ($no_fakur !=''){
			$where .=" AND H.nofakturexternal='$no_fakur' ";
			$where1 .=" AND P.nofakturexternal='$no_fakur' ";
		}
		if ($cara_bayar !='#'){
			$where .=" AND H.tipe_bayar='$cara_bayar' ";
			$where1 .=" AND P.tipe_bayar='$cara_bayar' ";
		}
		if ($status !='#'){
			$where .=" AND H.stverif_kbo='$status' ";
			$where1 .=" AND H.stverif_kbo='$status' ";
		}
		if ('' != $tanggalterima1) {
            $where .= " AND DATE(H.tgl_terima) >='".YMDFormat($tanggalterima1)."' AND DATE(H.tgl_terima) <='".YMDFormat($tanggalterima2)."'";
        }
		if ('' != $tanggaljt1) {
            $where .= " AND DATE(H.tanggaljatuhtempo) >='".YMDFormat($tanggaljt1)."' AND DATE(H.tanggaljatuhtempo) <='".YMDFormat($tanggaljt2)."'";
            $where_pengajuan .= " AND DATE(H.tanggal_kontrabon) >='".YMDFormat($tanggaljt1)."' AND DATE(H.tanggal_kontrabon) <='".YMDFormat($tanggaljt2)."'";
        }
        $from = "(
					SELECT CASE WHEN H.st_retur=0 THEN 1 ELSE 2 END as tipe,H.jenis_retur,H.id,H.tipepenerimaan,H.nopenerimaan,H.tanggalpenerimaan as tgl_trx,
					H.tgl_terima,H.tipe_bayar,H.tanggaljatuhtempo,H.nofakturexternal,
					H.iddistributor,H.totalharga,H.`status`,H.stverif_kbo
					,P.nopemesanan,M.nama as namadistributor,'0' as tipe_kembali
					,H.total_pembayaran,'tgudang_penerimaan' as ref_tabel,TV.st_posting
					from tgudang_penerimaan H
					LEFT JOIN tgudang_pemesanan P ON H.idpemesanan=P.id
					LEFT JOIN mdistributor M ON M.id=H.iddistributor
					LEFT JOIN tvalidasi_gudang TV ON TV.idtransaksi=H.id AND TV.asal_trx='1'
					WHERE H.status !='0' ".$where."
					
					UNION ALL 
					
					SELECT '2' as tipe,'1' as jenis_retur,H.id,P.tipepenerimaan,H.nopengembalian as nopenerimaan,H.tanggal as tgl_trx,P.tgl_terima as tgl_terima,P.tipe_bayar,
					H.tanggalkontrabon as tanggaljatuhtempo,P.nofakturexternal
					,H.iddistributor,H.totalharga,P.`status`,H.stverif_kbo,P1.nopemesanan,M.nama as namadistributor,H.tipe_kembali
					,0 as total_pembayaran,'tgudang_pengembalian' as ref_tabel,0 as st_posting
					from tgudang_pengembalian H
					LEFT JOIN tgudang_penerimaan P ON P.id=H.idpenerimaan
					LEFT JOIN tgudang_pemesanan P1 ON P1.id=P.idpemesanan
					LEFT JOIN mdistributor M ON M.id=H.iddistributor
					WHERE H.jenis='1' AND H.jenis_retur='1' AND H.`status`='2' ".$where1."
					
					UNION ALL
					
					SELECT '3' as tipe,'' as jenis_retur,H.id,'0' as tipepenerimaan,H.no_pengajuan as nopenerimaan,H.tanggal_pengajuan as tgl_trx 
					,H.tanggal_dibutuhkan as tgl_terima,'2' as tipe_bayar,H.tanggal_kontrabon as tanggaljatuhtempo,'' as nofakturexternal
					,H.idvendor as iddistributor,H.grand_total as totalharga,H.`status`,H.stverif_kbo,'' as nopemesanan,M.nama as namadistributor
					,'0' as tipe_kembali,'0' as total_pembayaran,'rka_pengajuan' as ref_tabel,0 as st_posting
					from rka_pengajuan H
					LEFT JOIN mvendor M ON M.id=H.idvendor
					WHERE H.`status` > 2 AND H.jenis_pembayaran='4' ".$where_pengajuan."
				) as tbl WHERE id != '0' ".$where2." ORDER BY id ASC";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $verif_name     = '';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tkontrabon_verifikasi/');
            $url_gudang        = site_url('tgudang_penerimaan/');
            $url_bayar        = site_url('tgudang_penerimaan/bayar/');
            $url_pengajuan        = site_url('mrka_pengajuan/');
            $url_batal       = site_url('tgudang_pengembalian/');
			$status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->tipe;
            $row[] = $r->id;
            $row[] = $no;
            $row[] = $this->tipe_pemesanan($r->tipe,$r->jenis_retur).$status_retur;
            $row[] = $r->nopenerimaan;
            $row[] = $r->nopemesanan;
            $row[] = HumanDateShort($r->tgl_trx);
            $row[] = HumanDateShort($r->tgl_terima);
            $row[] = $r->nofakturexternal;
            $row[] = $r->namadistributor;
            $row[] = number_format($r->totalharga,2);
            $row[] = $this->cara_bayar($r->tipe_bayar);
			if ($r->tipe !='3'){
				if ($r->ref_tabel=='tgudang_penerimaan') {
					$aksi .= '<a class="view btn btn-xs btn-default" href="'.$url_gudang.'detail/'.$r->id.'/1'.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-eye"></i></a>';
				}else{				
					$aksi .= '<a class="view btn btn-xs btn-default" href="'.$url_batal.'detail/'.$r->id.'/1'.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-eye"></i></a>';				
				}
			}else{
				$aksi .= '<a class="view btn btn-xs btn-default" href="'.$url_pengajuan.'update/'.$r->id.'/disabled'.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-eye"></i></a>';
			}
			if ($r->stverif_kbo=='0'){//BELUM DIVERIFIKASI
				if ($r->tipe !='3'){//TIPE 1:PENERIMAAN : 2: RETUR : 3: DARI PENGAJUAN
					if ($r->ref_tabel=='tgudang_penerimaan') {
						$aksi .= '<a href="'.$url_gudang.'edit/'.$r->id.'/1'.'" type="button" title="Edit" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>';
					}
					if ($r->jenis_retur!='1' && $r->tipe =='1') {//jenis retur 1:ganti uang 2: ganti barang sama 3: ganti barang beda
						if ($r->tipe_bayar=='1'){//1 : JIKA TUNAI 2:KREDIT
							if ($r->totalharga==$r->total_pembayaran){//Jika sudah ada Transaksi Kas
								$aksi .= '<button title="Verifikasi" class="btn btn-xs btn-primary verifikasi_kas"><i class="fa fa-check"></i></button>';
								$verif_name='verif_kas';
							}else{
								$aksi .= '<a href="'.$url_bayar.$r->id.'" title="Pembayaran Tunai" class="btn btn-xs btn-danger" target="_blank"><i class="fa fa-credit-card"></i></a>';	
								// $aksi .= '<button title="Verifikasi" disabled class="btn btn-xs btn-default verifikasi_kas"><i class="fa fa-check"></i></button>';	
								// $verif_name='verif_beli';
							}
						}else{							
								$aksi .= '<button title="Verifikasi" class="btn btn-xs btn-primary verifikasi"><i class="fa fa-check"></i></button>';	
								$verif_name='verif_beli';
						}
					}else{
						$aksi .= '<button title="Verifikasi" class="btn btn-xs btn-primary" onclick="get_retur_data(\''.$r->ref_tabel.'\','.$r->id.')"><i class="fa fa-check"></i></button>';
					}
				}else{
					//update/74
					$aksi .= '<a href="'.$url_pengajuan.'update/'.$r->id.'" type="button" title="Edit" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>';
					$aksi .= '<button title="Verifikasi" class="btn btn-xs btn-primary verifikasi_pengajuan"><i class="fa fa-check"></i></button>';
					$verif_name='verif_rka';
				}
			}elseif ($r->stverif_kbo=='1'){//VERIFIED		
				if ($r->tipe !='3'){
					if ($r->jenis_retur!='1') {
						$aksi .= '<a href="'.$url_gudang.'edit/'.$r->id.'/2'.'" type="button" title="Edit" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>';
					}
					if ($r->jenis_retur!='1') {
						if ($r->tipe_bayar=='1'){//1 : JIKA TUNAI 2:KREDIT
						$aksi .= '<button title="Batalkan Verifikasi" class="btn btn-xs btn-danger batal_verifikasi_tunai"><i class="fa fa-times"></i></button>';
						
						}else{
							
						$aksi .= '<button title="Batalkan Verifikasi" class="btn btn-xs btn-danger batal_verifikasi"><i class="fa fa-times"></i></button>';
						}
					}elseif ($r->jenis_retur!='1') {
						$aksi .= '<button title="Batalkan Verifikasi" class="btn btn-xs btn-danger batal_verifikasi_retur"><i class="fa fa-times"></i></button>';
					}
				}else{
					$aksi .= '<a href="'.$url_pengajuan.'update/'.$r->id.'" type="button" title="Edit" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>';
					$aksi .= '<button title="Batalkan Verifikasi" class="btn btn-xs btn-danger batal_verifikasi_pengajuan"><i class="fa fa-times"></i></button>';
				}
			}elseif ($r->stverif_kbo=='2'){//ACTIVATION
				$aksi .= '<a href="'.$url_gudang.'edit/'.$r->id.'/3'.'" type="button" title="Edit" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<button type="button" title="Batalkan Activation" class="btn btn-xs btn-danger"><i class="fa fa-chevron-left batal_activation"></i></button>';
			}
			//print_pengembalin
			if ($r->tipe !='3'){
				if ($r->jenis_retur!='1') {
				$aksi .= '<a href="'.$url_gudang.'print_data/'.$r->id.'" type="button" target="_blank" title="Edit" target="_blank" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';				
				}else{
					
				$aksi .= '<a href="'.$url_batal.'print_pengembalin/'.$r->id.'" type="button" target="_blank" title="Edit" target="_blank" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';				
				}
			}else{
				
			}
			if ($r->tanggaljatuhtempo){	
				if ($r->tipe_bayar=='1'){
					if ($r->totalharga==$r->total_pembayaran){//Jika sudah ada Transaksi Kas
						$row[] = '<span class="label label-success"><i class="fa fa-check"></i> TRANSAKSI KAS</span>';				
					}else{
						$row[] = '<span class="label label-danger"><i class="fa fa-close"></i> TRANSAKSI KAS</span>';				
						
					}
				}else{
					$row[] = HumanDateShort($r->tanggaljatuhtempo);			
					
				}
				
			}else{
				if ($r->tipe=='1'){
					if ($r->totalharga==$r->total_pembayaran){//Jika sudah ada Transaksi Kas
						$row[] = '<span class="label label-success"><i class="fa fa-check"></i> TRANSAKSI KAS</span>';				
					}else{
						$row[] = '<span class="label label-danger"><i class="fa fa-close"></i> TRANSAKSI KAS</span>';				
						
					}
				}else{
					$row[] = '<span class="label label-warning">BELUM DITENTUKAN</span>';				
					
				}
				
			}
				$row[] =$this->status($r->stverif_kbo);
			$aksi.='</div>';
            $row[] = $aksi;
            $row[] = $r->stverif_kbo;
            $row[] = $verif_name;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function tipe_pemesanan($tipe,$jenis_retur){
		if ($tipe=='1'){
			$tipe='<span class="label label-primary">PEMESANAN</span>';
		}else{
			if ($jenis_retur=='1'){
				$tipe='<span class="label label-danger">RETUR UANG</span>';
			}elseif($jenis_retur=='2'){
				$tipe='<span class="label label-default">RETUR GANTI BARANG</span>';
			}elseif($jenis_retur=='3'){
				$tipe='<span class="label label-success">RETUR BEDA</span>';
			}
		}
		if ($tipe=='3'){
			$tipe='<span class="label label-warning">PENGAJUAN</span>';
		}
		return $tipe;
	}
	function cara_bayar($tipe){
		if ($tipe=='1'){
			$tipe='<span class="label label-success">TUNAI</span>';
		}elseif($tipe=='2'){
			$tipe='<span class="label label-danger">KREDIT</span>';
		}else{
			$tipe='<span class="label label-default">-</span>';
		}
		
		return $tipe;
	}
	function status($status){
		if ($status=='0'){
			$status='<span class="label label-danger">UNVERIFIED</span>';
		}elseif($status=='1'){
			$status='<span class="label label-primary">VERIFIED</span>';
		}elseif($status=='2'){
			$status='<span class="label label-success">ACTIVATION</span>';
		}elseif($status=='3' || $status=='4'){
			$status='<span class="label label-warning">KONTRABON</span>';
		}else{
			$status='<span class="label label-default">-</span>';
		}
		
		return $status;
	}
	function status_tipe_kembali($status){
		if ($status=='1'){
			$status=' <span class="label label-warning">TUNAI</span>';
		}elseif($status=='2'){
			$status=' <span class="label label-primary">KBO</span>';
		}else{
			$status='';
		}
		
		return $status;
	}
	public function verifikasi()
    {
        $id = $this->input->post('id');
		$result=$this->model->insert_jurnal_pembelian($id);
        $data =array(
            'stverif_kbo'=>'1',
			'user_verifikasi_kbo'=>$this->session->userdata('user_name'),
            'tgl_verifikasi_kbo'=>date('Y-m-d H:i:s')
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tgudang_penerimaan', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function verifikasi_kas()
    {
        $id = $this->input->post('id');
		$this->db->where('penerimaan_id',$id);
		$this->db->update('tgudang_penerimaan_pembayaran',array('st_insert_jurnal'=>1));
		$result=$this->model->insert_jurnal_pembelian_kas($id);
        $data =array(
            'stverif_kbo'=>'1',
			'user_verifikasi_kbo'=>$this->session->userdata('user_name'),
            'tgl_verifikasi_kbo'=>date('Y-m-d H:i:s')
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tgudang_penerimaan', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function verifikasi_pengajuan()
    {
        $id = $this->input->post('id');
        $data =array(
            'stverif_kbo'=>'1',
			'user_verifikasi_kbo'=>$this->session->userdata('user_name'),
            'tgl_verifikasi_kbo'=>date('Y-m-d H:i:s')
        );
        $this->db->where('id', $id);
        $result=$this->db->update('rka_pengajuan', $data);
        if ($result) {
			// insert_jurnal_pembelian_pengajuan
			$this->model->insert_jurnal_pembelian_pengajuan($id);
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function verifikasi_per_page()
    {     
		
		$id = $this->input->post('id');
		// print_r($id);exit();
		$tipe = $this->input->post('tipe');
		$verif_nama = $this->input->post('verif_nama');
		foreach($id as $index=>$val){
			if ($verif_nama[$index]=='verif_beli'){//Jurnal Beli
				$this->model->insert_jurnal_pembelian($val);
			}
			if ($verif_nama[$index]=='verif_kas'){//Jurnal Kas
				$this->model->insert_jurnal_pembelian_kas($val);
			}
			if ($verif_nama[$index]=='verif_rka'){//Jurnal RKA
				// $this->model->insert_jurnal_pembelian_kas($id);
			}
			$data =array(
				'stverif_kbo'=>'1',
				'user_verifikasi_kbo'=>$this->session->userdata('user_name'),
				'tgl_verifikasi_kbo'=>date('Y-m-d H:i:s')
			);
			if ($tipe[$index]=='3'){
				$this->db->where('id', $val);
				$this->db->update('rka_pengajuan', $data);
			}else{
				$this->db->where('id', $val);
				$this->db->update('tgudang_penerimaan', $data);			
			}
		}
		$result=true;
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function verifikasi_all()
    {     
		
		$id = $this->input->post('id');
        $data =array(
            'stverif_kbo'=>'1',
            'user_verifikasi_kbo'=>$this->session->userdata('user_name'),
            'tgl_verifikasi_kbo'=>date('Y-m-d H:i:s')
        );
        $this->db->where_in('id', $id);
        $result=$this->db->update('tgudang_penerimaan', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function update_retur_uang()
    {
        // $id = $this->input->post('id');
        $tipe_kembali = $this->input->post('tipe_kembali');
        $tgl_kbo = $this->input->post('tipe_kembali');
        $trx_id = $this->input->post('trx_id');
        $ref_tabel = $this->input->post('ref_tabel');
		if ($tipe_kembali=='1'){
			$tgl_kbo = null;			
		}else{
			$tgl_kbo = YMDFormat($this->input->post('tgl_kbo'));			
		}
        if ($ref_tabel=='tgudang_penerimaan'){
			$data =array(
				'tipe_kembali'=>$tipe_kembali,
				'tanggaljatuhtempo'=>$tgl_kbo,
			);
			
		}else{
			$data =array(
				'tipe_kembali'=>$tipe_kembali,
				'tanggalkontrabon'=>$tgl_kbo,
			);
		}
		
        $this->db->where('id', $trx_id);
        $result=$this->db->update("$ref_tabel", $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function update_retur_uang_verifikasi()
    {
         $id = $this->input->post('id');
        $tipe_kembali = $this->input->post('tipe_kembali');
        $tgl_kbo = $this->input->post('tipe_kembali');
		$tpenerimaan_id = $this->input->post('tpenerimaan_id');
		$tpengembalian_id = $this->input->post('tpengembalian_id');
		$trx_id = $this->input->post('trx_id');
        $ref_tabel = $this->input->post('ref_tabel');
        $st_penerimaan = $this->input->post('st_penerimaan');
		// print_r($ref_tabel);exit();
		if ($tipe_kembali=='1'){
			$tgl_kbo = null;			
		}else{
			$tgl_kbo = YMDFormat($this->input->post('tgl_kbo'));	
			
		}
        
        if ($ref_tabel=='tgudang_penerimaan'){
			$data =array(
				'tipe_kembali'=>$tipe_kembali,
				'tanggaljatuhtempo'=>$tgl_kbo,
				'stverif_kbo'=>'1',
			);
			
		}else{
			$data =array(
				'tipe_kembali'=>$tipe_kembali,
				'tanggalkontrabon'=>$tgl_kbo,
				'stverif_kbo'=>'1',
			);
		}
		
        $this->db->where('id', $trx_id);
        $result=$this->db->update("$ref_tabel", $data);
		if ($tipe_kembali=='1'){
			if ($st_penerimaan=='0'){//Jika BALANCE
				$result=$this->model->insert_jurnal_retur_hutang($tpenerimaan_id);						
			}else{
				$result=$this->model->insert_tretur_terima($tpengembalian_id,$tpenerimaan_id);					
			}
		}else{
			if ($st_penerimaan=='2' && $tpenerimaan_id !=''){//Jika RS Harus Bayar Ke Distributor
				$result=$this->model->insert_jurnal_retur_hutang($tpenerimaan_id);						
			}
		}
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function batal_verifikasi()
    {
        $id = $this->input->post('id');
        $data =array(
            'status'=>'0',
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_nama'=>$this->session->userdata('user_name'),
            'deleted_date'=>date('Y-m-d H:i:s')
        );
        $this->db->where('asal_trx', '1');
        $this->db->where('idtransaksi', $id);
        $this->db->update('tvalidasi_gudang', $data);
		
		$data =array(
            'stverif_kbo'=>'0',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tgudang_penerimaan', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function batal_verifikasi_tunai()
    {
        $id = $this->input->post('id');
		$this->db->where('penerimaan_id',$id);
		$this->db->update('tgudang_penerimaan_pembayaran',array('st_insert_jurnal'=>0));
        $data =array(
            'status'=>'0',
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_nama'=>$this->session->userdata('user_name'),
            'deleted_date'=>date('Y-m-d H:i:s')
        );
        $this->db->where('id_reff',8);
        $this->db->where('idtransaksi', $id);
        $this->db->update('tvalidasi_kas', $data);
		
		$data =array(
            'stverif_kbo'=>'0',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tgudang_penerimaan', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function batal_verifikasi_pengajuan()
    {
        $id = $this->input->post('id');
		$data =array(
            'status'=>'0',
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_nama'=>$this->session->userdata('user_name'),
            'deleted_date'=>date('Y-m-d H:i:s')
        );
        $this->db->where('asal_trx', '2');
        $this->db->where('idtransaksi', $id);
        $this->db->update('tvalidasi_gudang', $data);
		
        $data =array(
            'stverif_kbo'=>'0',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('rka_pengajuan', $data);
        if ($result) {
			// $q="UPDATE tvalidasi_gudang SET status='0' WHERE asal_trx='2' AND idtransaksi='$id'";
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function batal_verifikasi_retur()
    {
        $id = $this->input->post('id');
        $data =array(
            'stverif_kbo'=>'0',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tgudang_pengembalian', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function get_retur($id)
    {
        $q="SELECT H.tipe_kembali from tgudang_pengembalian H WHERE H.id='$id'";
		$result=$this->db->query($q)->row('tipe_kembali');
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function get_retur_data($ref_tabel='',$id)
    {
        if ($ref_tabel=='tgudang_penerimaan'){//dari Retur ganti Barang
			$q="SELECT H.id,H.retur_id as tpengembalian_id,H.id as tpenerimaan_id, H.nominal_asal as totalharga,H.totalharga as totalpengganti,H.iddistributor,M.nama as distributor
				,H.tipe_kembali,H.jenis_retur,H.tanggaljatuhtempo as tgl_kbo
				FROM tgudang_penerimaan H
				LEFT JOIN tgudang_pengembalian TP ON TP.id=H.retur_id
				LEFT JOIN mdistributor M ON M.id=H.iddistributor
				WHERE H.id='$id'";
		}else{
			$q="SELECT H.id,H.id as tpengembalian_id,'' as tpenerimaan_id, totalharga,0 as totalpengganti,H.iddistributor,M.nama as distributor 
				,H.tipe_kembali,H.jenis_retur,H.tanggalkontrabon as tgl_kbo
				FROM tgudang_pengembalian H
				LEFT JOIN mdistributor M ON M.id=H.iddistributor
				WHERE H.id='$id'";
		}
		$result=$this->db->query($q)->row_array();
		if ($result['tgl_kbo']==''){
			$result['tgl_kbo']=date('Y-m-d');
		}
		$result['tanggalkontrabon'] =HumanDateShort($result['tgl_kbo']);
		$result['jenis_retur_nama']= tipe_pemesanan_kbo(1,$result['jenis_retur']);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function batal_activation()
    {
        $id = $this->input->post('id');
        $data =array(
            'stverif_kbo'=>'1',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tgudang_penerimaan', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	function load_distributor($tipe){
		if ($tipe=='3'){
			$q="SELECT * FROM mvendor M WHERE M.`status`='1' ORDER BY M.nama ASC";
		}else{			
			$q="SELECT * FROM mdistributor M WHERE M.`status`='1' ORDER BY M.nama ASC";
		}
		
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}


}
