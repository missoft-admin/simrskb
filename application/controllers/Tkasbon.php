<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

class Tkasbon extends CI_Controller {

	/**
	 * Kasbon controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tkasbon_model');
  }

	function index(){
		$data = array(
			'idtipe'		=> '1',
			'iduser' 			=> '',
			'tanggaldari' => date("d/m/Y"),
			'tanggalsampai' => date("d/m/Y")
		);
		$data['error'] 			= '';
		$data['title'] 			= 'Kasbon';
		$data['content'] 		= 'Tkasbon/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Kasbon",'#'),
									    			array("List",'tkasbon')
													);

		$data['list_user'] = $this->Tkasbon_model->getPegawai(1);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function filter(){
		$data = array(
			'idtipe'		=> $this->input->post('idtipe'),
			'iduser' 			=> $this->input->post('iduser'),
			'tanggaldari' => $this->input->post('tanggaldari'),
			'tanggalsampai' => $this->input->post('tanggalsampai')
		);

		$this->session->set_userdata($data);

		$data['error'] 			= '';
		$data['title'] 			= 'Kasbon';
		$data['content'] 		= 'Tkasbon/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Kasbon",'#'),
									    			array("List",'tkasbon')
													);

		$data['list_user'] = $this->Tkasbon_model->getPegawai($this->input->post('idtipe'));

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'tanggal' 			=> date("Y-m-d"),
			'idtipe' 				=> '',
			'idpegawai'			=> '',
			'catatan' 			=> '',
			'nominal' 			=> '',
			'alokasidana' 	=> '',
			'status' 				=> ''
		);

		$data['disabel'] 			= '';
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Kasbon';
		$data['content'] 		= 'Tkasbon/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Kasbon",'#'),
									    			array("Tambah",'tkasbon')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id,$disabel=''){
		if($id != ''){
			$row = $this->Tkasbon_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'tanggal' 			=> $row->tanggal,
					'idtipe' 				=> $row->idtipe,
					'idpegawai' 		=> $row->idpegawai,
					'catatan' 			=> $row->catatan,
					'nominal' 			=> $row->nominal,
					'alokasidana' 	=> $row->alokasidana,
					'status' 				=> $row->status
				);
				$data['disabel'] 			= $disabel;
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Kasbon';
				$data['content']	 	= 'Tkasbon/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Kasbon",'#'),
											    			array("Ubah",'tkasbon')
															);

				$data['list_pegawai'] = $this->Tkasbon_model->getPegawai($row->idtipe);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('tkasbon/index','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('tkasbon/index');
		}
	}

	function delete($id){
		$this->Tkasbon_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('tkasbon/index','location');
	}

	function save(){
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
		$this->form_validation->set_rules('idtipe', 'Tipe', 'trim|required');
		$this->form_validation->set_rules('idpegawai', 'Pegawai', 'trim|required');
		$this->form_validation->set_rules('catatan', 'Catatan', 'trim|required');
		$this->form_validation->set_rules('nominal', 'Nominal', 'trim|required');
		$this->form_validation->set_rules('alokasidana', 'Alokasi Dana', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Tkasbon_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('tkasbon/index','location');
				}
			} else {
				if($this->Tkasbon_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('tkasbon/index','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Tkasbon/manage';

		if($id==''){
			$data['title'] = 'Tambah Kasbon';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Kasbon",'#'),
															array("Tambah",'tkasbon')
													);
		}else{
			$data['title'] = 'Ubah Kasbon';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Kasbon",'#'),
															array("Ubah",'tkasbon')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex($uri='')
  {
			$this->select = array('tkasbon.*', '(CASE WHEN tkasbon.idtipe = 1 THEN mdokter.nama ELSE mpegawai.nama END) AS namapegawai');
			$this->from   = 'tkasbon';
			$this->join 	= array(
				array('mdokter', 'mdokter.id = tkasbon.idpegawai', 'LEFT'),
				array('mpegawai', 'mpegawai.id = tkasbon.idpegawai', 'LEFT'),
			);
			$this->where  = array(
				'tkasbon.status' => '1'
			);

			// FILTER
			if($uri == 'filter'){
				if ($this->session->userdata('idtipe')!='#'){
				$this->where = array_merge($this->where, array('tkasbon.idtipe' => $this->session->userdata('idtipe')));
					
				
				
				if ($this->session->userdata('idtipe') == "1") {
					if ($this->session->userdata('iduser') != "#") {
						$this->where = array_merge($this->where, array('mdokter.id' => $this->session->userdata('iduser')));
					}
				}else{
					if ($this->session->userdata('iduser') != "#") {
						$this->where = array_merge($this->where, array('mpegawai.id' => $this->session->userdata('iduser')));
					}
				}
				}
				if ($this->session->userdata('tanggaldari') != null && $this->session->userdata('tanggalsampai') != null) {
					$this->where = array_merge($this->where, array('DATE(tkasbon.tanggal) >=' => YMDFormat($this->session->userdata('tanggaldari'))));
					$this->where = array_merge($this->where, array('DATE(tkasbon.tanggal) <=' => YMDFormat($this->session->userdata('tanggalsampai'))));
				}
			}else{
				$this->where = array_merge($this->where, array('DATE(tkasbon.tanggal)' => date("Y-m-d")));
			}

			$this->order  = array(
				'tkasbon.id' => 'DESC'
			);
			$this->group  = array();

      $this->column_search   = array('tkasbon.tanggal', 'mdokter.nama', 'mpegawai.nama', 'tkasbon.catatan');
      $this->column_order    = array('tkasbon.tanggal', 'mdokter.nama', 'mpegawai.nama', 'tkasbon.catatan');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
					$row[] = DMYFormat($r->tanggal);
          $row[] = ($r->idtipe == 1 ? 'Dokter' : 'Pegawai');
          $row[] = $r->namapegawai;
          $row[] = $r->catatan;
          $row[] = number_format($r->nominal);
					$row[] = GetStatusAlokasiDana($r->alokasidana);
					$row[] = StatusKasbon($r->stpencairan);

			$aksi = '<div class="btn-group">';
			if ($r->st_transaksi=='0'){
			$aksi .= '<a href="'.site_url().'tkasbon/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
			$aksi .= '<a href="#" data-urlindex="'.site_url().'tkasbon" data-urlremove="'.site_url().'tkasbon/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
			}
			$aksi .= '<a href="'.site_url().'tkasbon/printout/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Print Bukti Kasbon" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
			$aksi .= '</div>';
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }

	function getPegawai()
	{
			$idtipe = $this->input->post('idtipe');
			if($idtipe == 1){
				$data = get_all('mdokter', array('status' => 1));
			}else{
				$data = get_all('mpegawai', array('status' => 1));
			}
			$this->output->set_output(json_encode($data));
	}

	public function printout($id)
	{
			$row = $this->Tkasbon_model->getSpecified($id);
			$data = array(
				'id' 						=> $row->id,
				'notransaksi' 	=> $row->notransaksi,
				'tanggal' 			=> HumanDateLong($row->created_at),
				'idtipe' 				=> $row->idtipe,
				'namapegawai' 	=> $row->namapegawai,
				'nominal' 			=> number_format($row->nominal, 2, ',', '.'),
				'terbilang' 			=> ucwords(strtolower(numbers_to_words($row->nominal).' Rupiah')),
				'deskripsi' 			=> $row->catatan,
				'alokasidana' 	=> GetStatusAlokasiDana($row->alokasidana)
			);

			$options = new Options();
			$options->set('isRemoteEnabled', true);
			$dompdf = new Dompdf($options);

			$html = $this->parser->parse('Tkasbon/print', array_merge($data, backend_info()), true);
			$html = $this->parser->parse_string($html, $data);

			$dompdf->loadHtml($html);

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper('A4', 'portrait');

			// Render the HTML as PDF
			$dompdf->render();

			// // Output the generated PDF to Browser
			$dompdf->stream('Faktur.pdf', array("Attachment"=>0));
	}
}
