<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_setoran extends CI_Controller {

	/**
	 * Setting JurnalSetoran Kas controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_setoran_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_setoran_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_setoran_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting JurnalSetoran Kas';
		$data['content'] 		= 'Msetting_jurnal_setoran/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting JurnalSetoran Kas",'#'),
									    			array("List",'mlogic_kasbon')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	
	function update(){
		$id_edit=1;
		$data=array(
			'st_auto_posting'=>$this->input->post('st_auto_posting'),
			'idakun_tunai'=>$this->input->post('idakun_tunai'),
			'batas_batal'=>$this->input->post('batas_batal'),
		);
		// print_r($data);exit();
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_setoran',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	
}
