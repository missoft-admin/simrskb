<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tgudang_approval extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tgudang_approval_model','model');
		$this->load->model('tgudang_pemesanan_model');
	}

	function index() {
	$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d'));
		date_add($date1,date_interval_create_from_date_string("-30 days"));
		date_add($date2,date_interval_create_from_date_string("15 days"));
		$date1= date_format($date1,"Y-m-d");
		$date2= date_format($date2,"Y-m-d");
		$data = array(
            'filter_distributor' => '#',
            'statuspesan' => '#',
            'tanggaldari' => HumanDateShort($date1),
            'tanggalsampai' => HumanDateShort(date('Y-m-d')),
            'no_trx' => '',
        );
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_distributor'] 			= $this->tgudang_pemesanan_model->list_distributor();
		$data['title'] 			= 'Approval Keuangan';
		$data['content'] 		= 'Tgudang_approval/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Keuangan Transaksi",'tbagi_hasil_approval/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		 $iduser=$this->session->userdata('user_id');	

		$notransaksi=$this->input->post('notransaksi');
		$filter_distributor=$this->input->post('filter_distributor');
		$status=$this->input->post('status');
		
		$tanggal_tagihan1=$this->input->post('tanggal_tagihan1');
		$tanggal_tagihan2=$this->input->post('tanggal_tagihan2');
		
		$where='';
		if ($notransaksi !=''){
			$where .=" AND H.nopemesanan='$notransaksi' ";
		}
		
		if ($filter_distributor !='#'){
			$where .=" AND H.iddistributor='$filter_distributor' ";
		}
		if ('' != $tanggal_tagihan1) {
            $where .= " AND DATE(H.tanggal) >='".YMDFormat($tanggal_tagihan1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_tagihan2)."'";
        }
		
		if ($status !='#'){
			$where .=" AND H.status='$status' ";
		}


        $from = "(
					SELECT 
					(SELECT MAX(AP.step) FROM tgudang_pemesanan_approval AP WHERE AP.st_aktif='1' AND AP.idpemesanan=H.id) as step,
					H.id,H.tanggal,H.st_retur,H.st_batal_by,CASE WHEN H.tipepemesanan='1' THEN 'NON LOGISTIK' ELSE 'LOGISTIK' END as tipe_pemesanan,H.tipepemesanan,
					H.nopemesanan,H2.nopemesanan as nopemesanan_asal,H.stdraft,H.`status`,COUNT(D.id) as item,H.iddistributor,M.nama as nama_distributor
					,H.tipe_bayar
					FROM tgudang_pemesanan H
					LEFT JOIN tgudang_pemesanan H2 ON H2.id=H.id_asal
					LEFT JOIN tgudang_pemesanan_detail D ON H.id=D.idpemesanan AND D.status='1'
					LEFT JOIN mdistributor M ON M.id=H.iddistributor
					LEFT JOIN tgudang_pemesanan_approval A ON A.idpemesanan=H.id 
					WHERE  H.status = '2' AND H.st_approval='0' AND A.iduser IN (".$userid.") ".$where."
					GROUP BY H.id
					ORDER BY H.id DESC

				) as tbl ";


		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nama_bagi_hasil','notransaksi');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $status     = '';
			$respon='';
            $status_setting     = '';
            $url        = site_url('tgudang_verifikasi/');
			$url2        = site_url('tgudang_pemesanan/');
			$cara_bayar='';
			if ($r->tipe_bayar=='1'){
				$cara_bayar=text_success('TUNAI');
				
			}else{
				$cara_bayar=text_primary('KREDIT');
			}
            if ($r->status=='2'){
				$status_setting='<button title="Lihat User" class="btn btn-danger btn-xs user" onclick="load_user('.$r->id.')"><i class="si si-user"></i></button>';
			}
			$query=$this->get_respon($r->id,$r->step);
			if ($query){
			foreach ($query as $res){
				if ($res->iduser==$iduser && $res->step==$r->step){
					if ($res->approve=='0'){
					  $aksi .= '<button title="Setuju" class="btn btn-success btn-xs setuju" onclick="setuju('.$res->id.','.$r->id.')"><i class="fa fa-check"></i> SETUJUI</button>';
					  $aksi .= '<button title="Tolak" class="btn btn-danger btn-xs tolak" onclick="tolak('.$res->id.','.$r->id.')"><i class="si si-ban"></i> TOLAK</button>';
					  if ($r->st_retur=='0'){
							if (UserAccesForm($user_acces_form,array('1128'))){
							$aksi .= '<a href="'.$url.'edit/'.$r->id.'/'.$res->id.'"  type="button" title="Edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>';
							}
							
						}	
					}else{
					  if ($r->status=='2'){
					  $aksi .= '<button title="Batalkan Pesetujuan / Tolak" class="btn btn-success btn-xs batal" onclick="batal('.$res->id.','.$r->id.')"><i class="fa fa-refresh"></i> Batalkan</button>';
						  
					  }
					}
				}
				if ($res->iduser==$iduser){
					$respon .='<span class="label label-warning" data-toggle="tooltip">STEP '.$res->step.'</span> : '.status_approval($res->approve).'<br>';
				}
			}
			  
			  
		  }else{
			   $respon='';
		  }
			$status=status_pesan_gudang($r->status);
           $row[] = $r->id;
            $row[] = $no;
            $row[] = ($r->tipepemesanan=='1'?'<label class="label label-default">'.$r->nopemesanan.'</label>':'<label class="label label-default">'.$r->nopemesanan.'</label>').' '.($r->nopemesanan_asal?'<label class="label label-danger">'.$r->nopemesanan_asal.'</label>':'').''.($r->st_retur?'<label class="label label-warning">RETUR</label>':'');
            $row[] = HumanDateLong($r->tanggal);
            $row[] = $r->tipe_pemesanan;
            $row[] = $r->nama_distributor.' <br>'.$cara_bayar;
            $row[] = $r->item;
            // $row[] = status_bagi_hasil_bayar($r->status,$r->step).' '.$status_setting;			
            $row[] = $status.' '.$status_setting;       
            $row[] = $respon;//8    
				$aksi .= '<a class="view btn btn-xs btn-default" target="_blank" href="'.$url.'detail/'.$r->id.'" type="button"  title="Detail"><i class="fa fa-eye"></i></a>';
			
			if (UserAccesForm($user_acces_form,array('1137'))){
				$aksi .= '<a href="'.$url2.'print_data/'.$r->id.'" target="_blank" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';
				}
			$aksi.='</div>';
            $row[] = $aksi;//8            
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function get_respon($id,$step){
	  $iduser=$this->session->userdata('user_id');
	  $q="SELECT *from tgudang_pemesanan_approval AP
			WHERE AP.st_aktif='1' AND AP.iduser='$iduser' AND AP.idpemesanan='$id'";
	  return $this->db->query($q)->result();
  }
	function list_user($id){
		$q="SELECT *FROM tgudang_pemesanan_approval H WHERE H.idpemesanan='$id'";
		$row=$this->db->query($q)->result();
		$content='';
		$no=1;
		$no_step="1";
		foreach($row as $r){
			$st_user_lain='';
			$content .='<tr class="'.($r->st_aktif=='1' && $r->approve=='0'?'success':'active').'">';
			if ($no_step != $r->step){
				$no_step=$r->step;
			}
			if ($no_step % 2){
				$content .='<td> <span class="label label-default" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
			}else{
				$content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
				// $content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span></td>';
			}
			if ($r->approve !='0'){
				if ($r->iduser !=$r->user_eksekusi){
					$st_user_lain=' <span class="label label-warning" data-toggle="tooltip">Oleh User Lain</span>';
				}
			}
			$content .='<td><i class="si si-users fa-2x text-black-op"></i>  '.$r->user_nama.'</td>';
			$content .='<td>'.($r->st_aktif=='1'?status_approval($r->approve):'').' '.$st_user_lain.'</td>';			
			$content .='</tr>';
			
		}
		$arr['detail']=$content;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($content));
	}
	function setuju_batal($id,$status,$idrka=''){
		// $arr=array();
		$q="call update_gudang_approval('$id', $status) ";
		$result=$this->db->query($q);
		// // $arr['st_insert']=$result;
		// $arr=$this->db->query("SELECT H.st_bendahara FROM rka_pengajuan H  WHERE H.id='$idrka' AND H.status='2'")->row('st_bendahara');
		$this->output->set_output(json_encode($result));
	}
	function tolak(){
		$id=$this->input->post('id_approval');
		$alasan_tolak=$this->input->post('alasan_tolak');
		$q="call update_gudang_approval('$id', 2) ";
		$this->db->query($q);
		$result=$this->db->update('tgudang_pemesanan_approval',array('alasan_tolak'=>$alasan_tolak),array('id'=>$id));
		echo json_encode($result);
	}
	function load_user_approval()
    {
	  $id=$this->input->post('id');	 
	  $where='';
	  $from="(
				SELECT S.id,S.iduser,S.step,U.`name` as user_nama,S.proses_setuju,S.proses_tolak
				FROM tgudang_pemesanan TR
				LEFT JOIN mlogic_gudang S ON TR.tipe_bayar=S.idtipe AND TR.tipepemesanan=S.tipepemesanan AND S.status='1'
				LEFT JOIN musers U ON U.id=S.iduser
				WHERE TR.id='$id' AND calculate_logic(S.operand, TR.totalharga, S.nominal)='1'
				ORDER BY S.step,S.id
				) as tbl";
				// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('user_nama');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $no_step='';
      foreach ($list as $r) {
          $no++;
          $row = array();
		  if ($no_step != $r->step){
				$no_step=$r->step;
		  }
		  if ($no_step % 2){		 
			$row[] = '<span class="label label-default">STEP '.$r->step.'</span>';
		  }else{
			   $row[] = '<span class="label label-primary">STEP '.$r->step.'</span>';
		  }
          $row[] = $r->user_nama;
          $row[] = ($r->proses_setuju=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
          $row[] = ($r->proses_tolak=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
         
          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
}
