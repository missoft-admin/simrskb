<?php

defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Thonor_dokter extends CI_Controller
{
    /**
     * Honor Dokter controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Thonor_dokter_model', 'model');
        $this->load->model('Trawatinap_verifikasi_model');
        $this->load->model('Mbagi_hasil_model');
        $this->load->model('Tvalidasi_model');
    }

    public function index()
    {
        $data = array(
          'notransaksi' => '',
          'idkategori_dokter' => [],
          'iddokter' => '',
          'tanggalpembayaran_dari' => '',
          'tanggalpembayaran_sampai' => '',
          'tanggaljatuhtempo_dari' => '',
          'tanggaljatuhtempo_sampai' => '',
          'status' => ''
        );

        $data['error']      = '';
        $data['title']      = 'Honor Dokter';
        $data['content']    = 'Thonor_dokter/index';
        $data['breadcrum']  = array(
                                                    array("RSKB Halmahera",'#'),
                                                    array("Honor Dokter",'#'),
                                                    array("List",'thonor_dokter')
                                                    );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filter()
    {
        $data = array(
          'notransaksi' => $this->input->post('notransaksi'),
          'idkategori_dokter' => isset($this->input->post('idkategori_dokter')[0]) ? $this->input->post('idkategori_dokter') : [],
          'iddokter' => $this->input->post('iddokter'),
          'tanggalpembayaran_dari' => $this->input->post('tanggalpembayaran_dari'),
          'tanggalpembayaran_sampai' => $this->input->post('tanggalpembayaran_sampai'),
          'tanggaljatuhtempo_dari' => $this->input->post('tanggaljatuhtempo_dari'),
          'tanggaljatuhtempo_sampai' => $this->input->post('tanggaljatuhtempo_sampai'),
          'status' => $this->input->post('status')
        );

        $this->session->set_userdata($data);

        $data['error']      = '';
        $data['title']      = 'Honor Dokter';
        $data['content']    = 'Thonor_dokter/index';
        $data['breadcrum']  = array(
                                                    array("RSKB Halmahera",'#'),
                                                    array("Honor Dokter",'#'),
                                                    array("List",'thonor_dokter')
                                                    );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function detail_honor($id)
    {
        $row = $this->model->getSpecifiedById($id);
        if ($row->id) {
            $data = array(
              'id' => $row->id,
              'notransaksi' => $row->notransaksi,
              'tanggal_pembayaran' => $row->tanggal_pembayaran,
              'tanggal_jatuhtempo' => $row->tanggal_jatuhtempo,
              'iddokter' => $row->iddokter,
              'namadokter' => $row->namadokter,
              'namabank' => 'BCA',
              'nonpwp' => $row->npwpdokter,
              'norekening' => '0000000000',
              'open_block' => true,
            );

            $data['error']      = '';
            $data['title']      = 'Rincian Honor Dokter';
            $data['content']    = 'Thonor_dokter/detail_honor';
            $data['breadcrum']  = array(
                                  array("RSKB Halmahera",'#'),
                                  array("Honor Dokter",'#'),
                                  array("Rincian Honor",'thonor_dokter')
                                );

            $pendapatan = $this->model->calcTotalPendapatan($id, 0);
            $pengeluaran = $this->model->calcTotalPengeluaran($id, 0);

            $data['total_pendapatan_brutto'] = number_format($pendapatan->total_pendapatan_brutto);
            $data['total_potongan_rs'] = number_format($pendapatan->total_potongan_rs);
            $data['total_beban_jasa_dokter'] = number_format($pendapatan->total_pendapatan_brutto - $pendapatan->total_potongan_rs);
            $data['total_pajak_dokter'] = number_format($pendapatan->total_pajak_dokter);
            $data['total_potongan_perujuk'] = number_format($pendapatan->total_potongan_perujuk);
            $data['total_potongan_pribadi'] = number_format($pengeluaran->total_potongan_pribadi);
            $data['total_pendapatan_netto'] = number_format($pendapatan->total_pendapatan_brutto - $pendapatan->total_potongan_rs - $pendapatan->total_pajak_dokter - $pendapatan->total_potongan_perujuk  - $pengeluaran->total_potongan_pribadi);

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        }
    }

    public function detail_tindakan($array)
    {
        $dataDetail = json_decode(base64url_decode($array));

        $data = array();
        $data['error']      = '';
        $data['title']      = 'Rincian Tindakan Honor Dokter / '.$dataDetail->title;
        $data['content']    = 'Thonor_dokter/detail_tindakan';
        $data['breadcrum']  = array(
                                array("RSKB Halmahera",'#'),
                                array("Honor Dokter",'#'),
                                array("Rincian Tindakan",'thonor_dokter')
                              );

        $data['data'] = $array;

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function stop_periode($idhonor)
    {
      if ($this->model->stopPeriode($idhonor)) {
          $this->session->set_flashdata('confirm', true);
          $this->session->set_flashdata('message_flash', 'Periode Pembayaran Honor Dokter telah di hentikan.');
          redirect('Thonor_dokter', 'location');
      }
    }

    public function unstop_periode($idhonor)
    {
      if ($this->model->unStopPeriode($idhonor)) {
          $this->session->set_flashdata('confirm', true);
          $this->session->set_flashdata('message_flash', 'Periode Pembayaran Honor Dokter telah di aktifkan kembali.');
          redirect('Thonor_dokter', 'location');
      }
    }

    public function next_periode()
    {
      $data = array(
        'idhonor' => $this->input->post('idhonor'),
        'iddokter' => $this->input->post('iddokter'),
        'namadokter' => $this->input->post('namadokter'),
        'periode_sebelumnya' => YMDFormat($this->input->post('periode_sebelumnya')),
        'periode_pembayaran' => YMDFormat($this->input->post('periode_pembayaran')),
        'periode_jatuhtempo' => YMDFormat($this->input->post('periode_jatuhtempo')),
      );

      if ($data['periode_sebelumnya'] != $data['periode_pembayaran']) {
        if ($this->model->nextPeriode($data)) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Periode Pembayaran Honor Dokter telah di alihkan.');
            redirect('Thonor_dokter', 'location');
        }
      } else {
        redirect('Thonor_dokter', 'location');
      }
    }
    
    public function print_rekap_pendapatan($idhonor)
    {
        $row = $this->db->query("SELECT
          thonor_dokter.tanggal_pembayaran,
          thonor_dokter.namadokter, mdokter.npwp AS npwpdokter
        FROM
          thonor_dokter
        LEFT JOIN mdokter ON mdokter.id = thonor_dokter.iddokter
        WHERE thonor_dokter.id = $idhonor")->row();

        $dompdf = new Dompdf();

        $data = array(
          'id' => $idhonor,
          'title' => 'Rekapitulasi Pendapatan Honor Dokter',
          'titleReport' => 'REKAPITULASI PENDAPATAN HONOR DOKTER',
          'namaDokter' => $row->namadokter,
          'npwpDokter' => $row->npwpdokter,
          'tanggalPembayaran' => $row->tanggal_pembayaran,
          'userFinance' => $this->session->userdata('user_name'),
          'userPrint' => $this->session->userdata('user_name'),
          'datePrint' => date("d-M-Y H:i:s"),
          'statusHold' => '0',
        );

        $pendapatan = $this->model->calcTotalPendapatan($idhonor, 0);
        $pengeluaran = $this->model->calcTotalPengeluaran($idhonor, 0);

        $data['total_pendapatan_brutto'] = number_format($pendapatan->total_pendapatan_brutto);
        $data['total_potongan_rs'] = number_format($pendapatan->total_potongan_rs);
        $data['total_beban_jasa_dokter'] = number_format($pendapatan->total_pendapatan_brutto - $pendapatan->total_potongan_rs);
        $data['total_pajak_dokter'] = number_format($pendapatan->total_pajak_dokter);
        $data['total_potongan_perujuk'] = number_format($pendapatan->total_potongan_perujuk);
        $data['total_potongan_pribadi'] = number_format($pengeluaran->total_potongan_pribadi);
        $data['total_pendapatan_netto'] = number_format($pendapatan->total_pendapatan_brutto - $pendapatan->total_potongan_rs - $pendapatan->total_pajak_dokter - $pendapatan->total_potongan_perujuk  - $pengeluaran->total_potongan_pribadi);
        $data['total_pendapatan_netto_terbilang'] = terbilang($pendapatan->total_pendapatan_brutto - $pendapatan->total_potongan_rs - $pendapatan->total_pajak_dokter - $pendapatan->total_potongan_perujuk  - $pengeluaran->total_potongan_pribadi);
        $data['total_pendapatan_netto_terbilang'] = terbilang($pendapatan->total_pendapatan_brutto - $pendapatan->total_potongan_rs - $pendapatan->total_pajak_dokter - $pendapatan->total_potongan_perujuk  - $pengeluaran->total_potongan_pribadi);

        $html = $this->load->view('Thonor_dokter/print/print_rekap_pendapatan', $data, true);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A3', 'landscape');
        $dompdf->render();
        $dompdf->stream('Rekapitulasi Pendapatan Honor Dokter.pdf', array("Attachment" => 0));
    }

    public function print_rekap_pendapatan_tunda($idhonor)
    {
        $row = $this->db->query("SELECT
          thonor_dokter.tanggal_pembayaran,
          thonor_dokter.namadokter, mdokter.npwp AS npwpdokter
        FROM
          thonor_dokter
        LEFT JOIN mdokter ON mdokter.id = thonor_dokter.iddokter
        WHERE thonor_dokter.id = $idhonor")->row();

        $dompdf = new Dompdf();

        $data = array(
          'id' => $idhonor,
          'title' => 'Rekapitulasi Pendapatan Tunda Honor Dokter',
          'titleReport' => 'REKAPITULASI PENDAPATAN TUNDA HONOR DOKTER',
          'namaDokter' => $row->namadokter,
          'npwpDokter' => $row->npwpdokter,
          'tanggalPembayaran' => $row->tanggal_pembayaran,
          'userFinance' => $this->session->userdata('user_name'),
          'userPrint' => $this->session->userdata('user_name'),
          'datePrint' => date("d-M-Y H:i:s"),
          'statusHold' => '1',
        );

        $pendapatan = $this->model->calcTotalPendapatan($idhonor, 1);
        $pengeluaran = $this->model->calcTotalPengeluaran($idhonor, 1);

        $data['total_pendapatan_brutto'] = number_format($pendapatan->total_pendapatan_brutto);
        $data['total_potongan_rs'] = number_format($pendapatan->total_potongan_rs);
        $data['total_beban_jasa_dokter'] = number_format($pendapatan->total_pendapatan_brutto - $pendapatan->total_potongan_rs);
        $data['total_pajak_dokter'] = number_format($pendapatan->total_pajak_dokter);
        $data['total_potongan_perujuk'] = number_format($pendapatan->total_potongan_perujuk);
        $data['total_potongan_pribadi'] = number_format($pengeluaran->total_potongan_pribadi);
        $data['total_pendapatan_netto'] = number_format($pendapatan->total_pendapatan_brutto - $pendapatan->total_potongan_rs - $pendapatan->total_pajak_dokter - $pendapatan->total_potongan_perujuk  - $pengeluaran->total_potongan_pribadi);

        $this->load->view('Thonor_dokter/print/print_rekap_pendapatan', $data);
        $html = $this->load->view('Thonor_dokter/print/print_rekap_pendapatan', $data, true);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream('Rekapitulasi Pendapatan Tunda Honor Dokter.pdf', array("Attachment" => 0));
    }

    public function print_detail_radiologi($idhonor)
    {
        $row = $this->db->query("SELECT tanggal_pembayaran, namadokter FROM thonor_dokter WHERE id = $idhonor")->row();
        $dompdf = new Dompdf();

        $data = array(
          'id' => $idhonor,
          'title' => 'Detail Pendapatan Radiologi',
          'titleReport' => 'DETAIL PENDAPATAN RADIOLOGI',
          'namaDokter' => $row->namadokter,
          'tanggalPembayaran' => $row->tanggal_pembayaran,
          'userPrint' => $this->session->userdata('user_name'),
          'datePrint' => date("d-M-Y H:i:s"),
        );

        $html = $this->load->view('Thonor_dokter/print/print_detail_radiologi', $data, true);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream('Rincian Pendapatan Radiologi.pdf', array("Attachment" => 0));
    }

    public function print_detail_rawat_jalan($idhonor)
    {
        $row = $this->db->query("SELECT tanggal_pembayaran, namadokter FROM thonor_dokter WHERE id = $idhonor")->row();
        ;
        $dompdf = new Dompdf();

        $data = array(
          'id' => $idhonor,
          'title' => 'Detail Pendapatan Rawat Jalan',
          'titleReport' => 'DETAIL PENDAPATAN RAWAT JALAN',
          'namaDokter' => $row->namadokter,
          'tanggalPembayaran' => $row->tanggal_pembayaran,
          'userPrint' => $this->session->userdata('user_name'),
          'datePrint' => date("d-M-Y H:i:s"),
        );

        $html = $this->load->view('Thonor_dokter/print/print_detail_rawat_jalan', $data, true);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream('Rincian Pendapatan Rawat Jalan.pdf', array("Attachment" => 0));
    }

    public function upload_document($idhonor)
    {
        $row = $this->model->getHeadDokumenUpload($idhonor);

        $data = [
          'idhonor' => $row->id,
          'notransaksi' => $row->notransaksi,
          'namadokter' => $row->namadokter,
          'tanggal_pembayaran' => $row->tanggal_pembayaran,
          'tanggal_jatuhtempo' => $row->tanggal_jatuhtempo,
        ];

        $data['error'] = '';
        $data['title'] = 'Upload Dokumen';
        $data['content'] = 'Thonor_dokter/upload_dokumen';
        $data['breadcrum'] = [
          ['RSKB Halmahera', '#'],
          ['Honor Dokter', 'Thonor_dokter/index'],
          ['Upload Dokumen', '#']
        ];

        $data['listFiles'] = $this->model->getListUploadedDocument($idhonor);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function upload_files()
    {
      if (!file_exists('assets/upload/honor_dokter')) {
        mkdir('assets/upload/honor_dokter', 0755, true);
      }

      $files = [];
      foreach ($_FILES as $file) {
        if ($file['name'] != '') {
          $config['upload_path'] = './assets/upload/honor_dokter/';
          $config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';

          $files[] = $file['name'];

          $this->upload->initialize($config);

          if ($this->upload->do_upload('file')) {
            $file_upload = $this->upload->data();

            $data = [];
            $data['idhonor'] = $this->input->post('idhonor');
            $data['filename'] = $file_upload['file_name'];
            $data['size'] = formatSizeUnits($file['size']);

            $this->db->insert('thonor_dokter_dokumen', $data);

            return true;
          } else {
            print_r($this->upload->display_errors());
            exit();

            return false;
          }
        } else {
          return true;
        }
      }
    }

    public function delete_file($idfile)
    {
      $this->db->where('id', $idfile);
      $query = $this->db->get('thonor_dokter_dokumen');
      $row = $query->row();

      if ($query->num_rows() > 0) {
        $this->db->where('id', $idfile);
        if ($this->db->delete('thonor_dokter_dokumen')) {
          if (file_exists('./assets/upload/honor_dokter/' . $row->filename) && $row->filename != '') {
            unlink('./assets/upload/honor_dokter/' . $row->filename);
          }
        }
      }

      return true;
    }

    public function getIndex($uri = 'index')
    {
        $this->select = array('thonor_dokter.*');
        $this->from   = 'thonor_dokter';
        $this->join 	= array(
          array('mdokter', 'mdokter.id = thonor_dokter.iddokter', ''),
          array('mdokter_kategori', 'mdokter_kategori.id = mdokter.idkategori', ''),
        );

        $this->where  = array();

        if ($uri == 'filter') {
            if ($this->session->userdata('notransaksi') != null) {
                $this->where = array_merge($this->where, array('thonor_dokter.notransaksi' => $this->session->userdata('notransaksi')));
            }
            if ($this->session->userdata('idkategori_dokter')) {
                $this->where_in = array('mdokter_kategori.id' => $this->session->userdata('idkategori_dokter'));
            }
            if ($this->session->userdata('iddokter') != 0) {
                $this->where = array_merge($this->where, array('thonor_dokter.iddokter' => $this->session->userdata('iddokter')));
            }
            if ($this->session->userdata('tanggalpembayaran_dari') != null) {
                $this->where = array_merge($this->where, array('DATE(thonor_dokter.tanggal_pembayaran) >=' => YMDFormat($this->session->userdata('tanggalpembayaran_dari'))));
            }
            if ($this->session->userdata('tanggalpembayaran_sampai') != null) {
                $this->where = array_merge($this->where, array('DATE(thonor_dokter.tanggal_pembayaran) <=' => YMDFormat($this->session->userdata('tanggalpembayaran_sampai'))));
            }
            if ($this->session->userdata('tanggaljatuhtempo_dari') != null) {
                $this->where = array_merge($this->where, array('DATE(thonor_dokter.tanggal_jatuhtempo) >=' => YMDFormat($this->session->userdata('tanggaljatuhtempo_dari'))));
            }
            if ($this->session->userdata('tanggaljatuhtempo_sampai') != null) {
                $this->where = array_merge($this->where, array('DATE(thonor_dokter.tanggal_jatuhtempo) <=' => YMDFormat($this->session->userdata('tanggaljatuhtempo_sampai'))));
            }
            if ($this->session->userdata('status') != 0) {
                $this->where = array_merge($this->where, array('thonor_dokter.status' => $this->session->userdata('status')));
            }
        }

        $this->order  = array(
          'thonor_dokter.id' => 'DESC'
        );

        $this->group  = array();

        $this->column_search  = array('thonor_dokter.notransaksi', 'thonor_dokter.namadokter', 'thonor_dokter.tanggal_pembayaran', 'thonor_dokter.tanggal_jatuhtempo', 'thonor_dokter.nominal');
        $this->column_order  = array('thonor_dokter.notransaksi', 'thonor_dokter.namadokter', 'thonor_dokter.tanggal_pembayaran', 'thonor_dokter.tanggal_jatuhtempo', 'thonor_dokter.nominal');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row) {
            $no++;

            $result = array();
            
            if ($row->status_stop) {
              $total_pendapatan_netto = $row->nominal;
            } else {
              // $pendapatan = $this->model->calcTotalPendapatan($row->id, 0);
              // $pengeluaran = $this->model->calcTotalPengeluaran($row->id, 0);
              // $total_pendapatan_netto = $pendapatan->total_pendapatan_brutto - $pendapatan->total_potongan_rs - $pendapatan->total_pajak_dokter - $pengeluaran->total_potongan_pribadi;
              $total_pendapatan_netto = 0;
            }

            $status_approval='<br>'.'<br>'.status_approval_HD($row->status_approval, '');

            $action = '<div class="btn-group">';
            $action .= '<a href="'.site_url().'thonor_dokter/detail_honor/'.$row->id.'" title="Rincian Honorarium Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>';
            
            if ($row->status_stop=='0') {
                $action .= '<a href="'.site_url().'thonor_dokter/stop_periode/'.$row->id.'" data-namadokter="'.$row->namadokter.'" data-periodepembayaran="'.DMYFormat($row->tanggal_pembayaran).'" title="Stop Periode Pembayaran" class="btn btn-danger btn-sm" onclick="actionStopPeriode(this); return false;"><i class="fa fa-stop-circle "></i></a>';
                $action .= '<a href="#" data-idhonor="'.$row->id.'" data-iddokter="'.$row->iddokter.'" data-namadokter="'.$row->namadokter.'" data-periodepembayaran="'.DMYFormat($row->tanggal_pembayaran).'" data-toggle="modal" data-target="#PeriodePembayaranModal" title="Perubahan Periode Pembayaran" class="btn btn-info btn-sm actionNextPeriode"><i class="fa fa-arrow-right"></i></a>';
            } else {
              if ($row->status_stop=='0' && $row->tanggal_pembayaran <= date("Y-m-d")) {
                $action .= '<a href="'.site_url().'thonor_dokter/unstop_periode/'.$row->id.'" data-namadokter="'.$row->namadokter.'" data-periodepembayaran="'.DMYFormat($row->tanggal_pembayaran).'" title="Unstop Periode Pembayaran" class="btn btn-primary btn-sm" onclick="actionUnStopPeriode(this); return false;"><i class="fa fa-play-circle "></i></a>';
              }
            }

            if ($row->status_approval=='0' || $row->status_approval=='3') {
                $action .= '<button onclick="kirim('.$row->id.')" title="Proses Approval" class="btn btn-warning btn-sm"><i class="fa fa-paper-plane"></i></button>';
            }

            $action .= '<a href="' . site_url() . 'thonor_dokter/upload_document/' . $row->id . '" title="Upload Dokumen" class="btn btn-danger btn-sm"><i class="fa fa-upload"></i></a>';

            $action .= '<div class="btn-group">
                          <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                            <span class="fa fa-print"></span>
                          </button>
                          <ul class="dropdown-menu dropdown-menu-left">
                            <li>
                              <a tabindex="-1" target="_blank" href="'.site_url().'thonor_dokter/print_rekap_pendapatan/'.$row->id.'">Rekap Pendapatan</a>
                              <a tabindex="-1" target="_blank" href="'.site_url().'thonor_dokter/print_rekap_pendapatan_tunda/'.$row->id.'">Rekap Pendapatan Tunda</a>
                              <a tabindex="-1" target="_blank" href="'.site_url().'thonor_dokter/print_detail_radiologi/'.$row->id.'">Detail Radiologi</a>
                              <a tabindex="-1" target="_blank" href="'.site_url().'thonor_dokter/print_detail_rawat_jalan/'.$row->id.'">Detail Rawat Jalan</a>
                            </li>
                          </ul>
                        </div>';
            $action .= '</div>';

            $result[] = $no;
            $result[] = $row->notransaksi;
            $result[] = $row->namadokter;
            $result[] = DMYFormat($row->tanggal_pembayaran);
            $result[] = DMYFormat($row->tanggal_jatuhtempo);
            $result[] = number_format($total_pendapatan_netto);
            $result[] = StatusPembayaranKlinik($row->status_pembayaran).$status_approval;
            $result[] = $action;

            $data[] = $result;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function getDetailTindakan($data)
    {
        $dataDetail = json_decode(base64url_decode($data));

        $this->select = array(
          'thonor_dokter_detail.*',
          '(CASE
              WHEN thonor_dokter_detail.jenis_transaksi = "poliklinik" THEN
                tpoliklinik_pendaftaran.no_medrec
              WHEN thonor_dokter_detail.jenis_transaksi = "rawatinap" THEN
                trawatinap_pendaftaran.no_medrec
          END) AS nomedrec',
          '(CASE
              WHEN thonor_dokter_detail.jenis_transaksi = "poliklinik" THEN
                tpoliklinik_pendaftaran.namapasien
              WHEN thonor_dokter_detail.jenis_transaksi = "rawatinap" THEN
                trawatinap_pendaftaran.namapasien
          END) AS namapasien',
          '(CASE
              WHEN thonor_dokter_detail.jenis_transaksi = "poliklinik" THEN
                IF (tpoliklinik_pendaftaran.idtipe = 1, "Poliklinik", "Instalasi Gawat Darurat (IGD)")
              WHEN thonor_dokter_detail.jenis_transaksi = "rawatinap" AND thonor_dokter_detail.jenis_tindakan = "JASA DOKTER RAWAT JALAN" THEN
                IF (tpoliklinik_pendaftaran.idtipe = 1, "Poliklinik", "Instalasi Gawat Darurat (IGD)")
              ELSE
                IF (trawatinap_pendaftaran.idtipe = 1, "Rawat Inap", "One Day Surgery (ODS)")
          END) AS tujuan',
          '(CASE
              WHEN thonor_dokter_detail.jenis_transaksi = "poliklinik" THEN
                mpoliklinik.nama
              WHEN thonor_dokter_detail.jenis_transaksi = "rawatinap" AND thonor_dokter_detail.jenis_tindakan = "JASA DOKTER RAWAT JALAN" THEN
                mpoliklinik.nama
              ELSE
                "-"
          END) AS poliklinik'
        );
        $this->from = 'thonor_dokter_detail';
        $this->join = array(
            array('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = thonor_dokter_detail.idtransaksi AND thonor_dokter_detail.jenis_transaksi = "rawatinap"', 'LEFT'),
            array('tpoliklinik_pendaftaran', '(tpoliklinik_pendaftaran.id = thonor_dokter_detail.idtransaksi AND thonor_dokter_detail.jenis_transaksi = "poliklinik") OR tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT'),
            array('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT'),
        );

        $this->where = array();
        $this->where_in = array('thonor_dokter_detail.id' => $dataDetail->list_id);

        $this->order = array(
          'thonor_dokter_detail.tanggal_pemeriksaan' => 'DESC'
        );

        $this->group = array();

        $this->column_search = array('trawatinap_pendaftaran.namapasien','tpoliklinik_pendaftaran.namapasien','thonor_dokter_detail.namatarif');
        $this->column_order = array();

        $list = $this->datatable->get_datatables();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row) {
            $no++;
            $result = array();

            $dataModal = array(
              'table' => $row->reference_table,
              'idrawatinap' => $row->idtransaksi,
              'iddetail' => $row->iddetail,
              'idtarif' => $row->idtarif,
              'namatarif' => $row->namatarif,
              'iddokter' => $row->iddokter,
              'namadokter' => $row->namadokter,
              'jasamedis' => $row->jasamedis,
              'potongan_perujuk' => $row->potongan_perujuk,
              'persentase_perujuk' => $row->potongan_perujuk,
              'potongan_rs' => $row->potongan_rs,
              'pajak_dokter' => $row->pajak_dokter,
              'nominal_potongan_rs' => $row->nominal_potongan_rs,
              'nominal_pajak_dokter' => $row->nominal_pajak_dokter,
              'nominal_perujuk' => $row->nominal_potongan_perujuk,
              'nominal_potongan_perujuk' => $row->nominal_potongan_perujuk,
              'jasamedis_netto' => $row->jasamedis_netto,
              'periode_pembayaran' => DMYFormat($row->tanggal_pembayaran),
              'periode_jatuhtempo' => DMYFormat($row->tanggal_jatuhtempo),
              'status_expertise' => $row->status_expertise,
              'status_verifikasi' => 1,
            );

            $result[] = $no . ' (<label style="color: red;">' . $row->id . '</label>)';
            $result[] = DMYFormat($row->tanggal_pemeriksaan);
            $result[] = $row->nomedrec;
            $result[] = $row->namapasien;
            $result[] = $row->tujuan;
            $result[] = $row->poliklinik;
            $result[] = $row->namatarif;
            $result[] = number_format($row->jasamedis);
            $result[] = number_format($row->potongan_perujuk, 2);
            $result[] = number_format($row->potongan_rs, 2);
            $result[] = number_format($row->pajak_dokter, 2);
            $result[] = number_format($row->nominal_potongan_perujuk);
            $result[] = number_format($row->nominal_potongan_rs);
            $result[] = number_format($row->nominal_pajak_dokter);
            $result[] = number_format($row->jasamedis_netto);
            $result[] = DMYFormat($row->tanggal_pembayaran);
            $result[] = StatusHoldHonor($dataDetail->status_hold);

            if ($row->reference_table == 'tfeerujukan_dokter') {
                $result[] = '<div class="btn-group">
                  <button class="btn btn-success btn-sm editJasaMedis" title="Edit Tindakan" data-toggle="modal" data-target="#EditJasaMedisModal" data-item="'.encodeURIComponent(json_encode($dataModal)).'"><i class="fa fa-pencil"></i></button>
                  <button class="btn btn-primary btn-sm showDetailPerujuk" title="Rincian Perujuk" data-toggle="modal" data-target="#RincianPerujukModal" data-idtransaksi="'.$row->idtransaksi.'"><i class="fa fa-list"></i></button>
                </div>';
            } else {
                $result[] = '<button class="btn btn-success btn-sm editJasaMedis" title="Edit Tindakan" data-toggle="modal" data-target="#EditJasaMedisModal" data-item="'.encodeURIComponent(json_encode($dataModal)).'"><i class="fa fa-pencil"></i></button>';
            }

            $data[] = $result;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function saveDataPendapatanLainLain()
    {
        $iddokter = $this->input->post('iddokter');
        $dokter = $this->db->query("SELECT mdokter.id,
          mdokter.nama,
          mdokter_kategori.id AS idkategori,
          mdokter_kategori.nama AS namakategori
        FROM
          mdokter
        JOIN mdokter_kategori ON mdokter_kategori.id=mdokter.idkategori
        WHERE
          mdokter.id = $iddokter AND
          mdokter.status = 1")->row();

        if ($this->input->post('idrow') != '') {
            $this->updateDataPendapatanLainLain($dokter);
        } else {
            $iddetail = $this->db->query("SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='rskbhalm_simrs' AND TABLE_NAME='thonor_dokter_detail'")->row()->AUTO_INCREMENT;

            $data = array();
            $data['reference_table']      = 'thonor_dokter_detail';
            $data['iddetail']             = $iddetail;
            $data['idhonor']              = $this->input->post('idhonor');
            $data['jenis_transaksi']      = $this->input->post('jenis_transaksi');
            $data['jenis_tindakan']       = $this->input->post('jenis_tindakan');
            $data['namatarif']            = $this->input->post('namatarif');
            $data['idkategori']           = $dokter->idkategori;
            $data['namakategori']         = $dokter->namakategori;
            $data['iddokter']             = $this->input->post('iddokter');
            $data['namadokter']           = $this->input->post('namadokter');
            $data['jasamedis']            = RemoveComma($this->input->post('jasamedis'));
            $data['potongan_rs']          = $this->input->post('potongan_rs');
            $data['nominal_potongan_rs']  = RemoveComma($this->input->post('nominal_potongan_rs'));
            $data['pajak_dokter']         = $this->input->post('pajak_dokter');
            $data['nominal_pajak_dokter'] = RemoveComma($this->input->post('nominal_pajak_dokter'));
            $data['jasamedis_netto']      = RemoveComma($this->input->post('jasamedis_netto'));
            $data['tanggal_pemeriksaan']  = YMDFormat($this->input->post('tanggal_pemeriksaan'));
            $data['tanggal_pembayaran']   = $this->input->post('tanggal_pembayaran');
            $data['tanggal_jatuhtempo']   = $this->input->post('tanggal_jatuhtempo');

            $this->db->insert('thonor_dokter_detail', $data);
        }
    }

    public function updateDataPendapatanLainLain($dokter)
    {
        $this->db->set('idhonor', $this->input->post('idhonor'));
        $this->db->set('jenis_transaksi', $this->input->post('jenis_transaksi'));
        $this->db->set('jenis_tindakan', $this->input->post('jenis_tindakan'));
        $this->db->set('namatarif', $this->input->post('namatarif'));
        $this->db->set('idkategori', $dokter->idkategori);
        $this->db->set('namakategori', $dokter->namakategori);
        $this->db->set('iddokter', $this->input->post('iddokter'));
        $this->db->set('namadokter', $this->input->post('namadokter'));
        $this->db->set('jasamedis', $this->input->post('jasamedis'));
        $this->db->set('potongan_rs', $this->input->post('potongan_rs'));
        $this->db->set('nominal_potongan_rs', $this->input->post('nominal_potongan_rs'));
        $this->db->set('pajak_dokter', $this->input->post('pajak_dokter'));
        $this->db->set('nominal_pajak_dokter', $this->input->post('nominal_pajak_dokter'));
        $this->db->set('jasamedis_netto', $this->input->post('jasamedis_netto'));
        $this->db->set('tanggal_pemeriksaan', $this->input->post('tanggal_pemeriksaan'));
        $this->db->set('tanggal_pembayaran', $this->input->post('tanggal_pembayaran'));
        $this->db->set('tanggal_jatuhtempo', $this->input->post('tanggal_jatuhtempo'));

        $this->db->where('id', $this->input->post('idrow'));
        $this->db->update('thonor_dokter_detail');
    }

    public function removeDataPendapatanLainLain()
    {
        $this->db->where('id', $this->input->post('idrow'));
        if ($this->db->delete('thonor_dokter_detail')) {
            return true;
        } else {
            return false;
        }
    }

    public function getRincianPerujuk($idtransaksi)
    {
        $this->db->where('idtransaksi', $idtransaksi);
        $this->db->where('status_perujuk', '1');
        $query = $this->db->get('thonor_dokter_detail');

        header('Content-Type: application/json');
        echo json_encode($query->result(), JSON_PRETTY_PRINT);
    }

    public function load_user_approval()
    {
        $id = $this->input->post('id');
        $where = '';
        $from = "(
          SELECT S.id,S.step,S.proses_setuju,S.proses_tolak,U.`name` as user_nama,H.nominal 
          FROM thonor_dokter H
          LEFT JOIN mdokter MD ON MD.id=H.iddokter
          LEFT JOIN mlogic_honor S ON S.idtipe=MD.idkategori AND S.`status`='1'
          LEFT JOIN musers U ON U.id=S.iduser
          WHERE H.id='$id'  AND calculate_logic(S.operand,H.nominal,S.nominal) = 1
          ORDER BY S.step ASC,S.id ASC
				) as tbl";

        $this->select = array();
        $this->from   = $from;
        $this->join 	= array();
        $this->where  = array();
        $this->order  = array();
        $this->group  = array();

        $this->column_search   = array('user_nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $no_step='';
        foreach ($list as $r) {
            $no++;
            $row = array();

            if ($no_step != $r->step) {
                $no_step=$r->step;
            }

            if ($no_step % 2) {
                $row[] = '<span class="label label-default">STEP '.$r->step.'</span>';
            } else {
                $row[] = '<span class="label label-primary">STEP '.$r->step.'</span>';
            }

            $row[] = $r->user_nama;
            $row[] = ($r->proses_setuju=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
            $row[] = ($r->proses_tolak=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
      );
        echo json_encode($output);
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
