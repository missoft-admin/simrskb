<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Munitpelayanan extends CI_Controller {

	/**
	 * Unit Pelayanan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Munitpelayanan_model');
		$this->load->helper('path');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Unit Pelayanan';
		$data['content'] 		= 'Munitpelayanan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Unit Pelayanan",'#'),
									    			array("List",'munitpelayanan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'lokasi' 				=> '',
			'deskripsi'			=> '',
			'status' 				=> '',
			'st_otomatis_terima' 				=> '0',
			'jml_hari_terima' 				=> '0',
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Unit Pelayanan';
		$data['content'] 		= 'Munitpelayanan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Unit Pelayanan",'#'),
									    			array("Tambah",'munitpelayanan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Munitpelayanan_model->getSpecified($id);
			// print_r($row);exit();
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'lokasi' 				=> $row->lokasi,
					'deskripsi' 		=> $row->deskripsi,
					'status' 				=> $row->status,
					'st_otomatis_terima' 				=> $row->st_otomatis_terima,
					'jml_hari_terima' 				=> $row->jml_hari_terima,
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Unit Pelayanan';
				$data['content']	 	= 'Munitpelayanan/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Unit Pelayanan",'#'),
											    			array("Ubah",'munitpelayanan')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('munitpelayanan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('munitpelayanan');
		}
	}

	function delete($id){
		$this->Munitpelayanan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('munitpelayanan','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Munitpelayanan_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('munitpelayanan','location');
				}
			} else {
				if($this->Munitpelayanan_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('munitpelayanan','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}
	function save_user_setting(){
		
		if($this->Munitpelayanan_model->save_user_setting()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('munitpelayanan','location');
		}
	
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Munitpelayanan/manage';

		if($id==''){
			$data['title'] = 'Tambah Unit Pelayanan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Unit Pelayanan",'#'),
															array("Tambah",'munitpelayanan')
													);
		}else{
			$data['title'] = 'Ubah Unit Pelayanan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Unit Pelayanan",'#'),
															array("Ubah",'munitpelayanan')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$this->from   = 'munitpelayanan';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'nama' => 'ASC'
			);
			$this->group  = array();

      $this->column_search   = array('nama');
      $this->column_order    = array('nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					if($r->statuslock == 0){
				        if (UserAccesForm($user_acces_form,array('131'))){
				            $aksi .= '<a href="'.site_url().'munitpelayanan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
				            $aksi .= '<a href="'.site_url().'munitpelayanan/setting/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-success btn-sm"><i class="si si-settings"></i></a>';
				        }
				        if (UserAccesForm($user_acces_form,array('132'))){
				            $aksi .= '<a href="#" data-urlindex="'.site_url().'munitpelayanan" data-urlremove="'.site_url().'munitpelayanan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
				        }
					}else{
						$aksi = '<a href="#" data-toggle="tooltip" title="Terkunci" class="btn btn-danger btn-sm"><i class="fa fa-lock"></i></a>';
					}
			        $aksi .= '</div>';

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }

  public function get_unitpelayanan() {
  	$this->db->select('id, nama as text');
  	$res = $this->db->get('munitpelayanan')->result_array();
  	$this->output->set_output(json_encode($res));
  }

  public function get_unitpelayanan_permintaan() {
  	$id = $this->uri->segment(3);
  	$this->db->select('munitpelayanan_permintaan.idunitpenerima as id, munitpelayanan.nama as text');
  	$this->db->where('munitpelayanan_permintaan.idunitpeminta', $id);
  	$this->db->join('munitpelayanan', 'munitpelayanan_permintaan.idunitpenerima = munitpelayanan.id', 'left');
  	$res = $this->db->get('munitpelayanan_permintaan')->result_array();
  	$this->output->set_output(json_encode($res));
  }

  public function get_unitpelayanan_tipebarang() {
  	$id = $this->uri->segment(3);
  	$this->db->select("
  		idtipe as id, 
		CASE WHEN idtipe = 1  THEN 'Alkes'
		WHEN idtipe = 2 THEN 'Implan'
		WHEN idtipe = 1  THEN 'Obat'
		ELSE 'Logistik' END AS text
  	");
  	$this->db->where('munitpelayanan_tipebarang.idunitpelayanan', $id);
  	$this->db->join('munitpelayanan', 'munitpelayanan_tipebarang.idunitpelayanan = munitpelayanan.id', 'left');
  	$res = $this->db->get('munitpelayanan_tipebarang')->result_array();
  	$this->output->set_output(json_encode($res));
  }

  public function get_user() {
  	$this->db->select('id, name as text');
  	$res = $this->db->get('musers')->result_array();
  	$this->output->set_output(json_encode($res));
  }

  public function get_unitpelayanan_user() {
  	$id = $this->uri->segment(3);
  	$this->db->select('munitpelayanan_user.userid as id, musers.name as text');
  	$this->db->where('munitpelayanan_user.idunitpelayanan', $id);
  	$this->db->join('munitpelayanan', 'munitpelayanan_user.idunitpelayanan = munitpelayanan.id', 'left');
  	$this->db->join('musers', 'munitpelayanan_user.userid = musers.id', 'left');
  	$res = $this->db->get('munitpelayanan_user')->result_array();
  	$this->output->set_output(json_encode($res));
  }
  public function get_user_setting() {
  	$id = $this->uri->segment(3);
  	$this->db->select('munitpelayanan_user_setting.userid as id, musers.name as text');
  	$this->db->where('munitpelayanan_user_setting.idunit', $id);
  	$this->db->join('munitpelayanan', 'munitpelayanan_user_setting.idunit = munitpelayanan.id', 'left');
  	$this->db->join('musers', 'munitpelayanan_user_setting.userid = musers.id', 'left');
  	$res = $this->db->get('munitpelayanan_user_setting')->result_array();
  	$this->output->set_output(json_encode($res));
  }
  function setting($id){

	if($id != ''){
		$row = $this->Munitpelayanan_model->getSpecified($id);
		if(isset($row->id)){
			$data = array(
				'id' 															=> $row->id,
				'nama' 														=> $row->nama,
				
			);

			$data['error'] 			= '';
			$data['title'] 			= 'Setting User';
			$data['content']	 	= 'Munitpelayanan/setting';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Setting Jatuh Tempo",'#'),
														array("Setting",'mrekanan')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpasien_kelompok','location');
		}
	}else{
		$this->session->set_flashdata('error',true);
		$this->session->set_flashdata('message_flash','data tidak ditemukan.');
		redirect('mpasien_kelompok');
	}
 }
}
