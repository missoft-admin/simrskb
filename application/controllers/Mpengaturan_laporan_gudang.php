<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mpengaturan_laporan_gudang extends CI_Controller
{
    /**
     * Pengaturan Laporan Gudang controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mpengaturan_laporan_gudang_model');
    }

    public function index(): void
    {
        // $row = $this->Mpengaturan_laporan_gudang_model->getSpecified($id);
        // if (isset($row->id)) {
            // $data = [
            //     'id' => $row->id,
            //     'nama' => $row->nama,
            //     'tipe_layanan' => $row->tipe_layanan,
            //     'tujuan' => $row->tujuan,
            //     'status_file_audio' => $row->status_file_audio,
            //     'dokter_penanggung_jawab' => $row->dokter_penanggung_jawab,
            // ];
            $data = [];
            $data['error'] = '';
            $data['title'] = 'Pengaturan Laporan Gudang';
            $data['content'] = 'Mpengaturan_laporan_gudang/index';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Pengaturan Laporan Gudang', '#'],
                ['Ubah', 'mpengaturan_laporan_gudang'],
            ];

            $data['list_jenis_gudang'] = $this->Mpengaturan_laporan_gudang_model->getListJenisGudang();
            $data['list_email'] = $this->Mpengaturan_laporan_gudang_model->getEmail();

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        // } else {
        //     $this->session->set_flashdata('error', true);
        //     $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
        //     redirect('mpengaturan_laporan_gudang', 'location');
        // }
    }

    public function save(): void
    {
        if ($this->Mpengaturan_laporan_gudang_model->updateData()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data berhasil diubah.');
            redirect('mpengaturan_laporan_gudang', 'location');
        }
    }
}

// End of file welcome.php
// Location: ./system/application/controllers/welcome.php
