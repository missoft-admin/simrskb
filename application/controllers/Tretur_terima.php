<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tretur_terima extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tretur_terima_model','model');
		$this->load->model('Mrka_bendahara_model');
	}

	function index() {
		$data=array(
			'status'=>'#',
			'cara_bayar'=>'#',
			'nopengembalian'=>'',
			'status'=>'#',
			'tanggal_trx1'=>'',
			'tanggal_trx2'=>'',
		);
		$data['error'] 			= '';
		$data['list_distributor'] 	= $this->model->list_distributor();
		$data['title'] 			= 'Penerimaan Pembayaran Retur Tunai';
		$data['content'] 		= 'Tretur_terima/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Penerimaan Retur",'Tretur_terima/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        $userid = $this->session->userdata('user_id');
		// $batas_batal=$this->db->query("SELECT batas_batal FROM msetting_jurnal_deposit WHERE id='1'")->row('batas_batal');
		
		$iddistributor=$this->input->post('iddistributor');
		$nopenerimaan=$this->input->post('nopenerimaan');
		$nopengembalian=$this->input->post('nopengembalian');
		$jenis_retur=$this->input->post('jenis_retur');
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		$status=$this->input->post('status');
		$where='';
		
		if ($iddistributor !='#'){
			$where .=" AND H.iddistributor = '".$iddistributor."'";
		}
		if ($nopenerimaan !=''){
			$where .=" AND H.nopenerimaan_asal LIKE '%".$nopenerimaan."%'";
		}
		
		if ($jenis_retur !='#'){
			$where .=" AND H.jenis_retur='$jenis_retur' ";
		}
		if ($status !='#'){
			$where .=" AND H.st_trx='$status' ";
		}
		
		if ($nopengembalian !=''){
			$where .=" AND H.nopengembalian LIKE '%".$nopengembalian."%'";
		}
		
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(H.tanggal_pengembalian) >='".YMDFormat($tanggal_trx1)."' AND DATE(H.tanggal_pengembalian) <='".YMDFormat($tanggal_trx2)."'";
        }
        $from = "(
					SELECT H.id,H.nopenerimaan_asal as nopenerimaan,H.nopengembalian,H.jenis_retur,H.tanggal_pengembalian,H.distributor,H.nominal_retur,H.nominal_ganti
					,(H.nominal_retur-H.nominal_ganti) as nominal,H.`status`,H.idpengembalian,H.st_trx
					FROM tretur_penerimaan H 
					WHERE H.`status`!='0' AND H.status_penerimaan IN ('1','0') ".$where."

				) as tbl";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nopenerimaan','nopengembalian','distributor');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

			
        foreach ($list as $r) {
            $no++;
            $row = array();
			$disabel_btn='';
			
			// if ($r->umur_posting>$batas_batal){
				// $disabel_btn='disabled';
			// }
            $aksi       = '<div class="btn-group">';
            $status     = '';
			if ($r->status=='1'){
				$status     = '<br><br>'.text_danger('MENUNGGU VERIFIKASI');
			}
			if ($r->status=='2'){
				$status     = '<br><br>'.text_primary('SELESAI VERIFIKASI');
			}
            $status_retur     = '';
          
            $row[] = $r->id;
            $row[] = $r->status;
            $row[] = $r->st_trx;
            $row[] = $no;
            $row[] = $r->nopenerimaan;
            $row[] = $r->nopengembalian;
            $row[] = tipe_pemesanan_kbo(1,$r->jenis_retur);
            $row[] = HumanDateShort($r->tanggal_pengembalian);
            $row[] = $r->distributor;
            $row[] = number_format($r->nominal,2);
           
            $url_pengembalian        = site_url('tgudang_pengembalian/');
            $url_retur        = site_url('tretur_terima/');
            $url_batal       = site_url('tgudang_pengembalian/');
            $row[] = ($r->st_trx=='1'?text_success('SELESAI PEMBAYARAN'):text_default('MENUGGU PEMBAYARAN')).$status;
					$aksi .= '<a href="'.$url_pengembalian.'detail/'.$r->idpengembalian.'" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
					$aksi .= '<a href="'.$url_retur.'detail/'.$r->id.'" target="_blank" title="Transaksi" class="btn btn-xs btn-info"><i class="fa fa-list-ol"></i></a>';
					if ($r->nominal!=0){
						if ($r->status=='1'){
							$aksi .= '<a href="'.$url_retur.'bayar/'.$r->id.'" target="_blank" title="Pembayaran" class="btn btn-xs btn-primary"><i class="fa fa-credit-card"></i></a>';
							
						}else{
							$aksi .= '<a href="'.$url_retur.'bayar/'.$r->id.'/disabled" target="_blank" title="Pembayaran" class="btn btn-xs btn-default"><i class="fa fa-credit-card"></i></a>';
							
						}
					}else{
						if ($r->st_trx=='0'){
							$aksi .= '<button  title="Verifikasi" class="btn btn-xs btn-success posting"><i class="fa fa-check"></i></button>';					
							
						}
					}
					if ($r->st_trx=='1' && $r->status=='1'){
						$aksi .= '<button  title="Verifikasi" class="btn btn-xs btn-success posting"><i class="fa fa-check"></i></button>';						
					}
					
					// $aksi .= '<a href="'.$url_retur.'print_kwitansi_deposit/'.$r->idpengembalian.'" target="_blank" title="Print Kwitansi" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';
					$aksi .= '<button title="Print" disabled class="btn btn-xs btn-success"><i class="fa fa-print"></i></button>';
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function detail($id,$disabel='') {
		$data=$this->model->getHeader($id);
		// print_r($data);exit();
		$data['error'] 			= '';
		$data['disabel'] 			= $disabel;
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['title'] 			= 'Detail Transaksi';
		$data['content'] 		= 'Tretur_terima/detail';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Penerimaan Retur",'Tretur_terima/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function bayar($id,$disabel='') {
		$data=$this->model->getHeader($id);
		// print_r($data);exit();
		$data['nominal'] 			= $data['nominal_retur'] - $data['nominal_ganti'];
		$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
		$data['error'] 			= '';
		$data['disabel'] 			= $disabel;
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['title'] 			= 'Detail Transaksi';
		 $data['list_pembayaran']      = $this->model->list_pembayaran($id);
		$data['content'] 		= 'Tretur_terima/manage_bayar';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Penerimaan Retur",'Tretur_terima/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	public function save()
    {
		$id=$this->model->saveData();
		if($id){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tretur_terima','location');
		}
	}
	public function posting()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_trx'=>'1',
            'status'=>'2',
			'posting_by'=>$this->session->userdata('user_id'),
			'posting_nama'=>$this->session->userdata('user_name'),
            'posting_date'=>date('Y-m-d H:i:s')
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tretur_penerimaan', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function posting_all()
    {
        $id = $this->input->post('id');
		foreach($id as $index=>$val){
			 $data =array(
				'status'=>'2',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $val);
			$result=$this->db->update('tretur_penerimaan', $data);
		}
       
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	
	public function batalkan()
    {
        $id = $this->input->post('id');
        $data =array(
            'status'=>'0',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('Tretur_terima', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function get_transaksi_detail($iddet)
    {
        $q="SELECT B.nama as nama_barang,D.* FROM `tgudang_penerimaan_detail` D
LEFT JOIN view_barang_all B ON B.id=D.idbarang AND B.jenis_retur=D.jenis_retur
WHERE D.id='$iddet'";
       
        $result=$this->db->query($q)->row_array();
        $this->output->set_output(json_encode($result));
    }
	function save_detail(){
		// print_r($this->input->post());exit();
		// $id=$this->model->saveData();
		if($this->model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('Tretur_terima','location');
		}
	
	}
	function load_detail(){
		
		$id=$this->input->post('id');
		$q="SELECT *from makun_nomor A ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT D.id,K.nama_tipe as nama_tipe,D.nama_barang,D.nominal_beli,D.nominal_ppn,D.nominal_diskon,D.idakun_beli,D.idakun_ppn,D.idakun_diskon 
				,D.idet
				from Tretur_terima_detail D
				LEFT JOIN mdata_tipebarang K ON K.id=D.jenis_retur
				WHERE D.idvalidasi='$id'
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			
			$aksi       = '<div class="btn-group">';
			$select_beli='<select name="idakun_beli[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_beli).'
						</select>';
			$select_ppn='<select name="idakun_ppn[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_ppn).'
						</select>';
			$select_diskon='<select name="idakun_diskon[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_diskon).'
						</select>';
		
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td>'.$r->nama_tipe.'<input type="hidden" name="iddet[]" value="'.$r->id.'"></td>';
			$tbl .='<td>'.$r->nama_barang.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_beli,2).'</td>';
			$tbl .='<td>'.$select_beli.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_ppn,2).'</td>';
			$tbl .='<td>'.$select_ppn.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_diskon,2).'</td>';
			$tbl .='<td>'.$select_diskon.'</td>';
				$aksi .= '<button type="button" title="Lihat Detail" class="btn btn-default btn-sm" onclick="get_transaksi_detail('.$r->idet.')"><i class="fa fa-eye"></i></button>';
			$tbl .='<td class="text-center">'.$aksi.'</td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		// $tbl .='<td colspan="4" class="text-right"><strong>TOTAL</strong></td>';
		// $tbl .='<td class="text-right"><strong>'.number_format($total,0).'</strong></td>';
		// $tbl .='<td></td>';
		// $tbl .='<td></td>';
		// $tbl .='<td></td>';
		// $tbl .='<td></td>';
		
		// $tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_ringkasan(){
		
		$id=$this->input->post('id');
		
		$disabel=$this->input->post('disabel');
		
        $q = "
				SELECT * FROM (
				SELECT 
				H.tanggal_transaksi as tanggal,H.idtransaksi as ref_tabel_id,'tgudang_penerimaan' as ref_tabel
				,'Tretur_terima' as ref_tabel_validasi,H.id as idvalidasi,H.notransaksi,D.idakun_beli as idakun,D.noakun_beli as noakun,D.namaakun_beli as namaakun
				,SUM(CASE WHEN D.posisi_beli='D' THEN D.nominal_beli ELSE 0 END) debet
				,SUM(CASE WHEN D.posisi_beli='K' THEN D.nominal_beli ELSE 0 END) kredit
				,'PEMBELIAN' as keterangan,CASE WHEN H.st_auto_posting='1' THEN 'AUTO POSTING' ELSE 'MANUAL' END as keterangan_code
				,H.created_by,H.created_date,H.created_nama
				FROM Tretur_terima H
				LEFT JOIN Tretur_terima_detail D ON D.idvalidasi=H.id
				WHERE H.id='$id'
				GROUP BY H.id,D.idakun_beli


				UNION ALL


				SELECT 
				H.tanggal_transaksi as tanggal,H.idtransaksi as ref_tabel_id,'tgudang_penerimaan' as ref_tabel
				,'Tretur_terima' as ref_tabel_validasi,H.id as idvalidasi,H.notransaksi,D.idakun_ppn as idakun,D.noakun_ppn as noakun,D.namaakun_ppn as namaakun
				,SUM(CASE WHEN D.posisi_ppn='D' THEN D.nominal_ppn ELSE 0 END) debet
				,SUM(CASE WHEN D.posisi_ppn='K' THEN D.nominal_ppn ELSE 0 END) kredit
				,'PPN' as keterangan,CASE WHEN H.st_auto_posting='1' THEN 'AUTO POSTING' ELSE 'MANUAL' END as keterangan_code
				,H.created_by,H.created_date,H.created_nama
				FROM Tretur_terima H
				LEFT JOIN Tretur_terima_detail D ON D.idvalidasi=H.id
				WHERE H.id='$id'
				GROUP BY H.id,D.idakun_ppn

				UNION ALL

				SELECT 
				H.tanggal_transaksi as tanggal,H.idtransaksi as ref_tabel_id,'tgudang_penerimaan' as ref_tabel
				,'Tretur_terima' as ref_tabel_validasi,H.id as idvalidasi,H.notransaksi,D.idakun_diskon as idakun,D.noakun_diskon as noakun,D.namaakun_diskon as namaakun
				,SUM(CASE WHEN D.posisi_diskon='D' THEN D.nominal_diskon ELSE 0 END) debet
				,SUM(CASE WHEN D.posisi_diskon='K' THEN D.nominal_diskon ELSE 0 END) kredit
				,'DISKON' as keterangan,CASE WHEN H.st_auto_posting='1' THEN 'AUTO POSTING' ELSE 'MANUAL' END as keterangan_code
				,H.created_by,H.created_date,H.created_nama
				FROM Tretur_terima H
				LEFT JOIN Tretur_terima_detail D ON D.idvalidasi=H.id
				WHERE H.id='$id'
				GROUP BY H.id,D.idakun_diskon

				UNION ALL

				SELECT 
				H.tanggal_transaksi as tanggal,H.idtransaksi as ref_tabel_id,'tgudang_penerimaan' as ref_tabel
				,'Tretur_terima' as ref_tabel_validasi,H.id as idvalidasi,H.notransaksi,D.idakun as idakun,D.noakun as noakun,D.namaakun as namaakun
				,SUM(CASE WHEN D.posisi_akun='D' THEN D.nominal ELSE 0 END) debet
				,SUM(CASE WHEN D.posisi_akun='K' THEN D.nominal ELSE 0 END) kredit
				,'PEMBAYARAN' as keterangan,CASE WHEN H.st_auto_posting='1' THEN 'AUTO POSTING' ELSE 'MANUAL' END as keterangan_code
				,H.created_by,H.created_date,H.created_nama
				FROM Tretur_terima H
				LEFT JOIN Tretur_terima_bayar D ON D.idvalidasi=H.id
				WHERE H.id='$id'
				GROUP BY H.id,D.idakun

				) T WHERE (T.debet+T.kredit) > 0



				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total_D=0;
		$total_K=0;
		foreach($rows as $r){
			$no++;
			$total_D=$total_D + $r->debet;
			$total_K=$total_K + $r->kredit;
			
			
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td>'.$r->noakun.'</td>';
			$tbl .='<td>'.$r->namaakun.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->debet,2).'</td>';
			$tbl .='<td class="text-right">'.number_format($r->kredit,2).'</td>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<td colspan="3" class="text-right"><strong>TOTAL</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_D,2).'</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_K,2).'</strong></td>';
		
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function list_akun($list,$idakun){
		$hasil='';
		foreach($list as $row){
			$hasil .='<option value="'.$row->id.'" '.($row->id==$idakun?"selected":"").'>'.$row->noakun.' - '.$row->namaakun.'</option>';
		}
		return $hasil;
	}
}
