<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mjenis_persetujuan_darah extends CI_Controller {

	
	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mjenis_persetujuan_darah_model');
		$this->load->helper('path');
		
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('2072'))){
			$data = array();
			$data['error'] 			= '';
			$data['title'] 			= 'Jenis Persetujuan Darah';
			$data['content'] 		= 'Mjenis_persetujuan_darah/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Jenis Persetujuan",'#'),
												  array("List",'mjenis_persetujuan_darah')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create(){
		
		$data = array(
			'id'            => '',
			'nama' 					=> '',
			'label_ina' 					=> '',
			'label_eng' 					=> '',
		);

		$data['error'] 			= '';
		// $data['list_layanan'] 			= $this->Mjenis_persetujuan_darah_model->list_layanan('');
		// $data['list_sound'] 			= $this->Antrian_layanan_model->list_sound();
		// $data['list_user'] 			= $this->Mjenis_persetujuan_darah_model->list_user('');
		$data['title'] 			= 'Tambah Jenis Persetujuan';
		$data['content'] 		= 'Mjenis_persetujuan_darah/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Jenis Persetujuan",'#'),
								            array("Tambah",'mjenis_persetujuan_darah')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$data= $this->Mjenis_persetujuan_darah_model->getSpecified($id);
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Jenis Persetujuan';
			$data['content']    = 'Mjenis_persetujuan_darah/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Jenis Persetujuan",'#'),
										array("Ubah",'mjenis_persetujuan_darah')
										);

			// $data['statusAvailableApoteker'] = $this->Mjenis_persetujuan_darah_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mjenis_persetujuan_darah');
		}
	}

	function delete($id){
		
		$result=$this->Mjenis_persetujuan_darah_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('mjenis_persetujuan_darah','location');
	}
	function pilih($id){
		
		$result=$this->Mjenis_persetujuan_darah_model->pilih($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('mjenis_persetujuan_darah','location');
	}
	function aktifkan($id){
		
		$result=$this->Mjenis_persetujuan_darah_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){
		if ($this->input->post('btn_simpan')=='2'){
			$q="UPDATE mjenis_persetujuan_darah SET staktif='0' WHERE staktif='1'";
			$this->db->query($q);
		}
		if($this->input->post('id') == '' ) {
			if($this->Mjenis_persetujuan_darah_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mjenis_persetujuan_darah/create','location');
			}
		} else {
			if($this->Mjenis_persetujuan_darah_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mjenis_persetujuan_darah/index','location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mjenis_persetujuan_darah/manage';

		if($id==''){
			$data['title'] = 'Tambah Jenis Persetujuan';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Jenis Persetujuan",'#'),
							               array("Tambah",'mjenis_persetujuan_darah')
								           );
		}else{
			$data['title'] = 'Ubah Jenis Persetujuan';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Jenis Persetujuan",'#'),
							               array("Ubah",'mjenis_persetujuan_darah')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$where='';
			
			$this->select = array();
			$from="
					(
						select H.*
						FROM mjenis_persetujuan_darah H 
						
						ORDER BY H.id
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $r->id;
          $result[] = $r->nama;
          $result[] = $r->label_ina.' <br> </i>'.$r->label_eng.'</i>';
          $result[] = ($r->staktif=='1'?text_primary('AKTIF'):text_default('TIDAK AKTIF'));
         
          $aksi = '<div class="btn-group">';
			if (UserAccesForm($user_acces_form,array('2074'))){
			$aksi .= '<a href="'.site_url().'mjenis_persetujuan_darah/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
			}
			if ($r->staktif=='0'){
				
				if (UserAccesForm($user_acces_form,array('2076'))){
					$aksi .= '<button title="Aktifkan" type="button" onclick="aktifkan('.$r->id.')" class="btn btn-warning btn-xs "><i class="si si-check"></i></button>';
				}
			}
			if ($r->staktif=='1'){
				if (UserAccesForm($user_acces_form,array('2075'))){
					$aksi .= '<button title="Hapus" type="button" onclick="removeData('.$r->id.')" class="btn btn-danger btn-xs "><i class="fa fa-trash-o"></i></button>';
				}
			}
		
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
	
}
