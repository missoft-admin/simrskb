<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mresources extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    PermissionUserLoggedIn($this->session);
    $this->load->model('Mresources_model', 'model');
  }

  function index(){
    
    $data = array();
    $data['error'] 			= '';
    $data['title'] 			= 'Resource';
    $data['content'] 		= 'Mresources/index';
    $data['breadcrum'] 		= array(array("RSKB Halmahera",'#'), array("Resource",'#'));

    $data['list_parent'] = $this->model->getAllParent();

    $data = array_merge($data, backend_info());
    $this->parser->parse('module_template', $data);
  }

  // @
  function get_child_level($headerpath) {
    $arr = $this->model->getPathLevel($headerpath);
    $this->output->set_output(json_encode($arr));
  }

  // @
  function save() {
    $data = [
      'nama'       => $this->input->post('nama-resource'),
      'tipe'       => $this->input->post('tipe-resource'),
      'idkelompok' => $this->input->post('jenis-resource'),
      'headerpath' => $this->input->post('parent-id'),
      'path'       => $this->input->post('path'),
      'level'      => $this->input->post('level'),
      'status'     => 1,
    ];

    $result = $this->model->saveData($data);

    if ($result) {
      $this->session->set_flashdata('confirm', true);
      $this->session->set_flashdata('message_flash', 'data telah disimpan.');
      redirect('mresources/index', 'location');
    }
  }

  function tipeResource($tipe) {
    switch ($tipe) {
      case 0:
        $result = "Action";
        break;
        case 1:
          $result = "Controller";
          break;
          case 2:
            $result = "Module";
            break;
            case 3:
              $result = "Other";
              break;
    }
    return $result;
  }

  public function showResource() {
    $result = $this->model->getAllResource();

    $data = array();

    $no = 1;
    foreach ($result as $row) {
      $rows = array();

      $button = '';
      if(button_roles('mresources/deleteResource')) {
        $button += '<button class="btn btn-xs btn-danger delete-resource" data-value="'.$row->id.'"><i class="fa fa-trash"></i>&nbsp; Hapus</button>';
      }

      $rows[] = $no++;
      $rows[] = TreeView($row->level, $row->nama);
      $rows[] = '<label class="label label-primary text-uppercase">'.$this->tipeResource($row->tipe).'</label>';
      $rows[] = $button;

      $data[] = $rows;
    }

    $output = array(
      "recordsTotal"    => count($result),
      "recordsFiltered" => count($result),
      "data"            => $data
    );

    echo json_encode($output);
  }

  function deleteResource() {
    
    $id = $this->input->post('id');

    $result = $this->model->deleteResource($id);
    if ($result) {
      return true;
    }
  }

}
