<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mpemeriksaan_radiologi extends CI_Controller
{
    /**
     * Pemeriksaan Radiologi controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mpemeriksaan_radiologi_model');
        $this->load->model('Mtarif_administrasi_model');
    }

    public function index(): void
    {
        $data = [];
        $data['error'] = '';
        $data['title'] = 'Pemeriksaan Radiologi';
        $data['content'] = 'Mpemeriksaan_radiologi/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Pemeriksaan Radiologi', 'mpemeriksaan_radiologi/index'],
            ['List', '#'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create(): void
    {
        $data = [
            'id' => '',
            'tipe_pemeriksaan' => '',
            'jenis_pemeriksaan' => '',
            'anatomi_tubuh' => '',
            'kode' => '',
            'nama_pemeriksaan' => '',
            'nama_pemeriksaan_eng' => '',
            'posisi_pemeriksaan' => '',
            'urutan_tampil' => '',
            'tarif_radiologi_id' => '',
            'tarif_radiologi_tipe' => '',
            'tarif_radiologi_nama' => '',
        ];

        $data['error'] = '';
        $data['title'] = 'Tambah Pemeriksaan Radiologi';
        $data['content'] = 'Mpemeriksaan_radiologi/manage';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Pemeriksaan Radiologi', '#'],
            ['Tambah', 'mpemeriksaan_radiologi'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id): void
    {
        if ('' !== $id) {
            $row = $this->Mpemeriksaan_radiologi_model->getSpecified($id);
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'tipe_pemeriksaan' => $row->tipe_pemeriksaan,
                    'jenis_pemeriksaan' => $row->jenis_pemeriksaan,
                    'anatomi_tubuh' => $row->anatomi_tubuh,
                    'kode' => $row->kode,
                    'nama_pemeriksaan' => $row->nama_pemeriksaan,
                    'nama_pemeriksaan_eng' => $row->nama_pemeriksaan_eng,
                    'posisi_pemeriksaan' => $row->posisi_pemeriksaan,
                    'urutan_tampil' => $row->urutan_tampil,
                    'tarif_radiologi_id' => $row->tarif_radiologi_id,
                    'tarif_radiologi_tipe' => $row->tarif_radiologi_tipe,
                    'tarif_radiologi_nama' => $row->tarif_radiologi_nama,
                ];
                $data['error'] = '';
                $data['title'] = 'Ubah Pemeriksaan Radiologi';
                $data['content'] = 'Mpemeriksaan_radiologi/manage';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Pemeriksaan Radiologi', '#'],
                    ['Ubah', 'mpemeriksaan_radiologi'],
                ];

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mpemeriksaan_radiologi', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mpemeriksaan_radiologi', 'location');
        }
    }

    public function delete($id): void
    {
        $this->Mpemeriksaan_radiologi_model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mpemeriksaan_radiologi', 'location');
    }

    public function save(): void
    {
        $this->form_validation->set_rules('tipe_pemeriksaan', 'Tipe Pemeriksaan', 'trim|required');
        $this->form_validation->set_rules('jenis_pemeriksaan', 'Jenis Pemeriksaan', 'trim|required');
        $this->form_validation->set_rules('nama_pemeriksaan', 'Nama Pemeriksaan', 'trim|required');

        if (true == $this->form_validation->run()) {
            if ('' == $this->input->post('id')) {
                if ($this->Mpemeriksaan_radiologi_model->saveData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mpemeriksaan_radiologi', 'location');
                }
            } else {
                if ($this->Mpemeriksaan_radiologi_model->updateData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mpemeriksaan_radiologi', 'location');
                }
            }
        } else {
            $this->failed_save($this->input->post('id'));
        }
    }

    public function failed_save($id): void
    {
        $data = $this->input->post();
        $data['error'] = validation_errors();
        $data['content'] = 'Mpemeriksaan_radiologi/manage';

        if ('' == $id) {
            $data['title'] = 'Tambah Satuan';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Satuan', '#'],
                ['Tambah', 'mpemeriksaan_radiologi'],
            ];
        } else {
            $data['title'] = 'Ubah Satuan';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Satuan', '#'],
                ['Ubah', 'mpemeriksaan_radiologi'],
            ];
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function getIndex(): void
    {
        $tipe_pemeriksaan = $this->input->post('tipe_pemeriksaan');
        $jenis_pemeriksaan = $this->input->post('jenis_pemeriksaan');
        $anatomi_tubuh = $this->input->post('anatomi_tubuh');
        $posisi_pemeriksaan = $this->input->post('posisi_pemeriksaan');
        $nama_pemeriksaan = $this->input->post('nama_pemeriksaan');
        $status = $this->input->post('status');

        $this->select = [
            'merm_pemeriksaan_radiologi.id',
            'merm_referensi.ref AS tipe_pemeriksaan',
            'merm_pemeriksaan_radiologi.kode',
            'merm_pemeriksaan_radiologi.nama_pemeriksaan',
            'mtarif_radiologi_detail.total AS harga',
            'merm_pemeriksaan_radiologi.status'
        ];

        $this->from = 'merm_pemeriksaan_radiologi';

        $this->join = [
            ['mtarif_radiologi_detail', 'mtarif_radiologi_detail.idtarif = merm_pemeriksaan_radiologi.tarif_radiologi_id AND mtarif_radiologi_detail.kelas = 0', 'LEFT'],
            ['merm_referensi', 'merm_referensi.nilai = merm_pemeriksaan_radiologi.tipe_pemeriksaan AND merm_referensi.ref_head_id = "245" AND merm_referensi.status = "1"', 'LEFT'],
        ];

        $this->where = [];
        if (!empty($tipe_pemeriksaan)) {
            $this->db->where_in('merm_pemeriksaan_radiologi.tipe_pemeriksaan', $tipe_pemeriksaan);
        }

        if (!empty($jenis_pemeriksaan)) {
            $this->db->where_in('merm_pemeriksaan_radiologi.jenis_pemeriksaan', $jenis_pemeriksaan);
        }

        if (!empty($anatomi_tubuh)) {
            $this->db->where_in('merm_pemeriksaan_radiologi.anatomi_tubuh', $anatomi_tubuh);
        }

        if (!empty($posisi_pemeriksaan)) {
            $this->db->where_in('merm_pemeriksaan_radiologi.posisi_pemeriksaan', $posisi_pemeriksaan);
        }

        if (!empty($nama_pemeriksaan)) {
            $this->db->like('merm_pemeriksaan_radiologi.nama_pemeriksaan', $nama_pemeriksaan);
        }

        if ('' != $status) {
            $this->db->where('merm_pemeriksaan_radiologi.status', $status);
        }

        $this->order = [
            'merm_pemeriksaan_radiologi.kode' => 'ASC',
        ];

        $this->group = [];

        $this->column_search = ['nama_pemeriksaan'];

        $this->column_order = ['nama_pemeriksaan'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;

            $aksi = '<div class="btn-group">';
            $aksi .= '<a href="'.site_url().'mpemeriksaan_radiologi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
            $aksi .= '<a href="#" data-urlindex="'.site_url().'mpemeriksaan_radiologi" data-urlremove="'.site_url().'mpemeriksaan_radiologi/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
            $aksi .= '</div>';

            $row = [];
            $row[] = $no;
            $row[] = $r->tipe_pemeriksaan;
            $row[] = $r->kode;
            $row[] = $r->nama_pemeriksaan;
            $row[] = number_format((int) $r->harga);
            $row[] = StatusRow($r->status);
            $row[] = $aksi;

            $data[] = $row;
        }

        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];

        echo json_encode($output);
    }

    public function get_daftar_pemeriksaan(): void
    {
        $tipe = $this->input->post('tipe');
        $head_parent = $this->input->post('head_parent');
        $sub_parent = json_decode($this->input->post('sub_parent'));

        $this->select = ['mtarif_radiologi.*',
            'merm_referensi.ref AS tipe',
            '(
					CASE WHEN mtarif_radiologi.idtipe = 1 AND mtarif_radiologi.idkelompok = 0 THEN
            CONCAT(mtarif_radiologi.nama,  " (", mtarif_radiologi_expose.nama, mtarif_radiologi_film.nama, ")")
					ELSE
						mtarif_radiologi.nama
					END
				) AS namatarif', ];
        $this->from = 'mtarif_radiologi';
        $this->join = [
            ['mtarif_radiologi_expose', 'mtarif_radiologi_expose.id = mtarif_radiologi.idexpose', 'LEFT'],
            ['mtarif_radiologi_film', 'mtarif_radiologi_film.id = mtarif_radiologi.idfilm', 'LEFT'],
            ['merm_referensi', 'merm_referensi.nilai = mtarif_radiologi.idtipe AND merm_referensi.ref_head_id = "245" AND merm_referensi.status = "1"', 'LEFT'],
        ];

        $whereArray = [
            'mtarif_radiologi.status' => '1',
        ];
        $orWhereArray = [];

        if ('0' !== $tipe) {
            $whereArray = array_merge($whereArray, [
                'mtarif_radiologi.idtipe' => $tipe,
            ]);
        }

        if (null == $sub_parent) {
            if ('0' !== $head_parent) {
                $whereArray = array_merge($whereArray, [
                    'left(mtarif_radiologi.path,'.strlen($head_parent).')' => $head_parent,
                ]);
            }
        } else {
            foreach ($sub_parent as $val) {
                $orWhereArray[] = ['LEFT(mtarif_radiologi.path, '.strlen($val).')' => $val];
            }
        }

        $this->where = $whereArray;
        $this->or_where = $orWhereArray;

        $this->order = [
            'mtarif_radiologi.path' => 'ASC',
        ];

        $this->group = [];

        $this->column_search = ['mtarif_radiologi.nama'];
        $this->column_order = ['mtarif_radiologi.nama'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = TreeView($r->level, $r->namatarif);

            if (0 == $r->idkelompok) {
                $row[] = '<button type="button" class="btn btn-primary btn-sm select-tarif"
                    data-id="'.$r->id.'"
                    data-tipe="'.$r->tipe.'"
                    data-nama="'.TreeView($r->level, $r->namatarif).'"
                    data-dismiss="modal"
                >
                    <i class="fa fa-level-down"></i> UBAH TARIF
                </button>';
            } else {
                $row[] = '';
            }

            $row[] = $r->idkelompok;

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];

        echo json_encode($output);
    }

    public function get_tarif_pemeriksaan($tarif_id): void
    {
        $detail = $this->Mpemeriksaan_radiologi_model->getTarifPemeriksaan($tarif_id);
        echo json_encode($detail);
    }

    public function get_data_referensi($ref_id, $selected = ''): void
    {
        $option = '';
        foreach (list_variable_ref($ref_id) as $row) {
            $option .= '<option value="'.$row->id.'" '.(1 == $row->st_default ? 'selected' : '').' '.($row->id == $selected ? 'selected' : '').'>'.$row->nama.'</option>';
        }

        echo json_encode($option);
    }
}
