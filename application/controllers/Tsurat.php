<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class Tsurat extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tsurat_model');
		$this->load->model('Term_radiologi_xray_expertise_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Tsurat_model');
		$this->load->helper('path');
		
  }
  function get_email_pasien(){
	  $idpasien=$this->input->post('idpasien');
	   $emailPasien = $this->Term_radiologi_xray_expertise_model->get_email_pasien($idpasien);
	  if ($emailPasien){
		  
	   $emailPasien = $emailPasien[0];
	  }
        echo json_encode($emailPasien);
	  // $q="SELECT email FROM `mfpasien` M WHERE M.id='$idpasien'";
	  // $hasil=$this->db->query($q)->result_array();
	  // $this->output->set_output(json_encode($hasil));
  }
  function create_pdf($assesmen_id,$jenis_surat='1'){
	  $this->cetak_surat_sakit($assesmen_id,1);
  }
  public function kirim_email()
    {
		$assesmen_id=$this->input->post('assesmen_id');
		$jenis_surat=$this->input->post('jenis_surat');
		$list_email=$this->input->post('list_email');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
        $data_udpate=array(
			'assesmen_id' => $assesmen_id,
			'sent_by' => $login_ppa_id,
			'sent_date' => date('Y-m-d H:i:s'),
			'jenis_surat' => $jenis_surat,
			'alamat_email' => $list_email,
			'status_terkirim' => 0,

		);
		if ($jenis_surat=='1'){$this->cetak_surat_sakit($assesmen_id,1);};
		if ($jenis_surat=='2'){$this->cetak_surat_sehat($assesmen_id,1);};
		if ($jenis_surat=='3'){$this->cetak_surat_kontrol($assesmen_id,1);};
		if ($jenis_surat=='4'){$this->cetak_surat_lanjut($assesmen_id,1);};
		if ($jenis_surat=='5'){$this->cetak_surat_rawat($assesmen_id,1);};
		if ($jenis_surat=='6'){$this->cetak_surat_external($assesmen_id,1);};
		if ($jenis_surat=='7'){$this->cetak_surat_amputasi($assesmen_id,1);};
		if ($jenis_surat=='8'){$this->cetak_surat_diagnosa($assesmen_id,1);};
		if ($jenis_surat=='9'){$this->cetak_surat_skdp($assesmen_id,1);};
        $hasil=$this->db->insert('tsurat_kirim_email', $data_udpate);
		$id_log=$this->db->insert_id();
        $emailArray = explode(',', $list_email);
		$q="
			SELECT H.judul_ina as judul
			,CONCAT('<p>','Kepada Yth, {nama_pasien}<br><br>Terlampir ',H.judul_ina,' atas nama {nama_pasien}<br><br>Atas Perhatiannya Kami ucapkan terima kasih</p>') as body
			,'RSKB Halmahera Siaga' as footer_email FROM msurat_template H WHERE H.jenis_surat='$jenis_surat' LIMIT 1
		";
		$pengaturan_email=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
        // $pengaturan_email = (array) get_row('merm_pengaturan_pengiriman_email_laboratorium');
		if ($jenis_surat=='1'){$q="SELECT *from tsurat_sakit WHERE assesmen_id='$assesmen_id'";}
		if ($jenis_surat=='2'){$q="SELECT *from tsurat_sehat WHERE assesmen_id='$assesmen_id'";}
		if ($jenis_surat=='3'){$q="SELECT *from tsurat_kontrol WHERE assesmen_id='$assesmen_id'";}
		if ($jenis_surat=='4'){$q="SELECT *from tsurat_lanjut WHERE assesmen_id='$assesmen_id'";}
		if ($jenis_surat=='5'){$q="SELECT *from tsurat_rawat WHERE assesmen_id='$assesmen_id'";}
		if ($jenis_surat=='6'){$q="SELECT *from tsurat_external WHERE assesmen_id='$assesmen_id'";}
		if ($jenis_surat=='7'){$q="SELECT *from tsurat_amputasi WHERE assesmen_id='$assesmen_id'";}
		if ($jenis_surat=='8'){$q="SELECT *from tsurat_diagnosa WHERE assesmen_id='$assesmen_id'";}
		if ($jenis_surat=='9'){$q="SELECT *from tsurat_skdp WHERE assesmen_id='$assesmen_id'";}
        $data_transaksi = $this->db->query($q)->row();
        // $email_tujuan = $this->input->post('email_kirim_hasil');

        // $file = $this->model->get_file_hasil_pemeriksaan($id);
        // $hasil_pemeriksaan_id = $file->id;
        // $hasil_pemeriksaan_file = $file->nama_file;

        // $fileName = $hasil_pemeriksaan_file;
        // $folderPath = "report/hasil_pemeriksaan_laboratorium_bank_darah/{$id}";
        $filePath = "";

        // $this->model->log_kirim_hasil($this->prepareLogDataEmail($hasil_pemeriksaan_id, $dataUpdate));

        $this->sendEmail($pengaturan_email, $data_transaksi, $emailArray, $id_log);
    }
	private function sendEmail($pengaturan_email, $data_transaksi, $emailArray, $id_log)
	{
		// print_r($data_transaksi);exit;
		$nama_file=$pengaturan_email['judul'].' '.$data_transaksi->nopermintaan.'.pdf';
		$uploadDir_asset = './assets/upload/lap_bedah/';
		$mail = new PHPMailer(true);
		$file_hapus=$uploadDir_asset.$nama_file;
		try {
			// Server settings
			$this->configureEmail($mail);

			// Recipients
			$mail->setFrom('erm@halmaherasiaga.com', 'SIMRS RSKB Halmahera Siaga');
			foreach ($emailArray as $email) {
				$mail->addAddress($email, $data_transaksi->nama_pasien);
			}
			$mail->addAttachment($uploadDir_asset.$nama_file);

			// Attachments
			// $pengaturan_attachment_email = $this->model->get_pengaturan_attachment_email();
			// foreach ($pengaturan_attachment_email as $attachment) {
				// $file_hasil_id = $attachment->file_hasil_id;

				// if ($file_hasil_id == 1) {
					// $fileFormat1 = str_replace('.pdf', '_FORMAT_1.pdf', $filePath);
				// }

				// if ($file_hasil_id == 2) {
					// $fileFormat2 = str_replace('.pdf', '_FORMAT_2.pdf', $filePath);
					// $mail->addAttachment($fileFormat2);
				// }
			// }

			// Content
			$bodyHTML = str_replace('{nama_pasien}', '<b>' . $data_transaksi->nama_pasien . '</b>', $pengaturan_email['body']) . '<br>' . $pengaturan_email['footer_email'];

			$mail->isHTML(true);
			$mail->Subject = strip_tags($pengaturan_email['judul']);
			$mail->Body = $bodyHTML;

			$mail->send();

			$response = [
				'status' => true,
				'message' => 'success',
			];
			$this->db->where('id',$id_log);
			$this->db->update('tsurat_kirim_email',array('status_terkirim'=>1));
			unlink($file_hapus);
			
			$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response, JSON_PRETTY_PRINT))
				->_display();

			exit;
		} catch (Exception $e) {
			$response = [
				'status' => false,
				'message' => "Message could not be sent. Mailer Error: {$mail->ErrorInfo}",
			];

			$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response, JSON_PRETTY_PRINT))
				->_display();

			exit;
		}
	}
	 private function configureEmail($mail)
    {
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->SMTPOptions = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ];

        $mail->Host = 'mail.halmaherasiaga.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'erm@halmaherasiaga.com';
        $mail->Password = '{T,_Xun}9{@5';
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
        $mail->Port = 465;
    }
  function list_surat_cetak(){
		$assesmen_id=$this->input->post('assesmen_id');
		$jenis_surat=$this->input->post('jenis_surat');
		$this->select = array();
		$from="
				(
					SELECT H.*,mppa.nama FROM tsurat_cetak H
					LEFT JOIN mppa ON mppa.id=H.printed_by
					WHERE H.assesmen_id='$assesmen_id' AND H.jenis_surat='$jenis_surat'
					ORDER BY H.id ASC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			
		$result[] = ($r->printed_number);
		$result[] = ($r->nama);
		$result[] = HumanDateLong($r->printed_date);
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  public function show_cetak($assesmen_id,$jenis_surat='1',$st_cetak='0'){
		// $st_cetak='0';
		// print_r($this->session->userdata());exit;
		$data['assesmen_id']=$assesmen_id;
		$data['jenis_surat']=$jenis_surat;
		if ($jenis_surat=='1'){
			$data['function_surat']='cetak_surat_sakit';
		}
		if ($jenis_surat=='2'){
			$data['function_surat']='cetak_surat_sehat';
		}
		if ($jenis_surat=='3'){
			$data['function_surat']='cetak_surat_kontrol';
		}
		if ($jenis_surat=='4'){
			$data['function_surat']='cetak_surat_lanjut';
		}
		if ($jenis_surat=='5'){
			$data['function_surat']='cetak_surat_rawat';
		}
		if ($jenis_surat=='6'){
			$data['function_surat']='cetak_surat_external';
		}
		if ($jenis_surat=='7'){
			$data['function_surat']='cetak_surat_amputasi';
		}
		if ($jenis_surat=='8'){
			$data['function_surat']='cetak_surat_diagnosa';
		}
		if ($jenis_surat=='9'){
			$data['function_surat']='cetak_surat_skdp';
		}
		if ($jenis_surat=='99'){//Permintaan DPJP
			$data['function_surat']='cetak_permintaan_djp';
		}
		
		$data['st_cetak']=$st_cetak;
		$data['ukuran']='A4';
		$data['base_url'] = $this->config->base_url();
		if ($st_cetak == '1') {
			$data_cetak=$this->Tsurat_model->update_cetak($assesmen_id, $jenis_surat);
		}
		$data = array_merge($data, backend_info());
		// print_r($data);
		$this->load->view('Tsurat/print_show', $data);
	}
  function list_surat_email(){
		$assesmen_id=$this->input->post('assesmen_id');
		$jenis_surat=$this->input->post('jenis_surat');
		$this->select = array();
		$from="
				(
					SELECT H.*,mppa.nama FROM tsurat_kirim_email H
					LEFT JOIN mppa ON mppa.id=H.sent_by
					WHERE H.assesmen_id='$assesmen_id' AND H.jenis_surat='$jenis_surat'
					ORDER BY H.id ASC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
			
		$result[] = ($no);
		$result[] = ($r->nama);
		$result[] = HumanDateLong($r->sent_date);
		$result[] = ($r->alamat_email);
		$result[] = ($r->status_terkirim=='1'?text_success('TERKIRIM'):text_danger('GAGAL'));
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function tindakan($pendaftaran_id='',$st_ranap='0',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		get_ppa_login();
		$data_login_ppa=get_ppa_login();
		// print_r($trx_id);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// $data_satuan_ttv=array();
		
		$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id,$st_ranap);
		// print_r($data);exit;
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		}else{
		  $def_image='def_2.png';
		  
		}
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tsurat/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		$data['error'] 			= '';
		$data['status_assemen'] 			= '0';
		$data['assesmen_id'] 			= '';
		$data['st_ranap'] 			= $st_ranap;
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
							  array("RSKB Halmahera",'#'),
							  array("Konsultasi ",'#'),
							  array("Dokter",'tsurat')
							);
		$data_assemen=array('idpoli'=>'','iddokter_peminta'=>'');
		// print_r($data_assemen);exit;
		if ($menu_kiri=='input_surat_skdp' || $menu_kiri=='his_surat_skdp'){
			if ($trx_id != '#'){
				if ($menu_kiri=='his_surat_skdp'){
					
				}else{
					$data_assemen=$this->Tsurat_model->get_data_surat_skdp_trx($trx_id);
				}
				
			}else{
				$data_assemen=$this->Tsurat_model->get_data_surat_skdp($pendaftaran_id,$st_ranap);
				
				
			}
			if ($data_assemen){
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			
			$setting_assesmen=$this->Tsurat_model->setting_surat(9);
			$setting_assesmen_detail=$this->Tsurat_model->setting_surat_detail(9);
			$data = array_merge($data,$data_assemen,$setting_assesmen,$setting_assesmen_detail);
			// print_r($data);exit;
		}
		
		//Surat diagnosa
		if ($menu_kiri=='input_surat_diagnosa' || $menu_kiri=='his_surat_diagnosa'){
			if ($trx_id != '#'){
				if ($menu_kiri=='his_surat_diagnosa'){
					
				}else{
					$data_assemen=$this->Tsurat_model->get_data_surat_diagnosa_trx($trx_id);
				}
				
			}else{
				$data_assemen=$this->Tsurat_model->get_data_surat_diagnosa($pendaftaran_id,$st_ranap);
				
				
			}
			if ($data_assemen){
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			
			$setting_assesmen=$this->Tsurat_model->setting_surat(8);
			$setting_assesmen_detail=$this->Tsurat_model->setting_surat_detail(8);
			$data = array_merge($data,$data_assemen,$setting_assesmen,$setting_assesmen_detail);
			// print_r($data);exit;
		}
		//Surat Keterangan Rawat
		if ($menu_kiri=='input_surat_rawat' || $menu_kiri=='his_surat_rawat'){
			if ($trx_id != '#'){
				if ($menu_kiri=='his_surat_rawat'){
					
				}else{
					$data_assemen=$this->Tsurat_model->get_data_surat_rawat_trx($trx_id);
				}
				
			}else{
				$data_assemen=$this->Tsurat_model->get_data_surat_rawat($pendaftaran_id,$st_ranap);
				
				
			}
			if ($data_assemen){
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			
			$setting_assesmen=$this->Tsurat_model->setting_surat(5);
			$setting_assesmen_detail=$this->Tsurat_model->setting_surat_detail(5);
			// print_r($setting_assesmen_detail);exit;
			$data = array_merge($data,$data_assemen,$setting_assesmen,$setting_assesmen_detail);
		}
		//Surat Keterangan Lanjut
		if ($menu_kiri=='input_surat_lanjut' || $menu_kiri=='his_surat_lanjut'){
			if ($trx_id != '#'){
				if ($menu_kiri=='his_surat_lanjut'){
					
				}else{
					$data_assemen=$this->Tsurat_model->get_data_surat_lanjut_trx($trx_id);
				}
				
			}else{
				$data_assemen=$this->Tsurat_model->get_data_surat_lanjut($pendaftaran_id,$st_ranap);
				
				
			}
			if ($data_assemen){
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			
			$setting_assesmen=$this->Tsurat_model->setting_surat(4);
			$setting_assesmen_detail=$this->Tsurat_model->setting_surat_detail(4);
			// print_r($setting_assesmen_detail);exit;
			$data = array_merge($data,$data_assemen,$setting_assesmen,$setting_assesmen_detail);
		}
		//Surat Keterangan Kontrol
		if ($menu_kiri=='input_surat_kontrol' || $menu_kiri=='his_surat_kontrol'){
			if ($trx_id != '#'){
				if ($menu_kiri=='his_surat_kontrol'){
					
				}else{
					$data_assemen=$this->Tsurat_model->get_data_surat_kontrol_trx($trx_id);
				}
				
			}else{
				$data_assemen=$this->Tsurat_model->get_data_surat_kontrol($pendaftaran_id,$st_ranap);
				
				
			}
			if ($data_assemen){
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			
			$setting_assesmen=$this->Tsurat_model->setting_surat(3);
			$setting_assesmen_detail=$this->Tsurat_model->setting_surat_detail(3);
			// print_r($setting_assesmen_detail);exit;
			$data = array_merge($data,$data_assemen,$setting_assesmen,$setting_assesmen_detail);
		}
		
		//Surat Keterangan sakit
		if ($menu_kiri=='input_surat_sakit' || $menu_kiri=='his_surat_sakit'){
			if ($trx_id != '#'){
				if ($menu_kiri=='his_surat_sakit'){
					$data_assemen=$this->Tsurat_model->get_data_surat_sakit_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tsurat_model->get_data_surat_sakit_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
				}else{
					$data_assemen=$this->Tsurat_model->get_data_surat_sakit_trx($trx_id);
				}
				
			}else{
				$data_assemen=$this->Tsurat_model->get_data_surat_sakit($pendaftaran_id,$st_ranap);
				
				
			}
			if ($data_assemen){
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			
			$setting_assesmen=$this->Tsurat_model->setting_surat(1);
			$data = array_merge($data,$data_assemen,$setting_assesmen);
		}
		//Surat Keterangan sakit
		if ($menu_kiri=='input_surat_sehat' || $menu_kiri=='his_surat_sehat'){
			if ($trx_id != '#'){
				if ($menu_kiri=='his_surat_sehat'){
					
				}else{
					$data_assemen=$this->Tsurat_model->get_data_surat_sehat_trx($trx_id);
				}
				
			}else{
				$data_assemen=$this->Tsurat_model->get_data_surat_sehat($pendaftaran_id,$st_ranap);
				
				
			}
			if ($data_assemen){
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			
			$setting_assesmen=$this->Tsurat_model->setting_surat(2);
			$setting_assesmen_detail=$this->Tsurat_model->setting_surat_detail(2);
			$data = array_merge($data,$data_assemen,$setting_assesmen,$setting_assesmen_detail);
		}
		//Surat Pengantar External
		if ($menu_kiri=='input_surat_external' || $menu_kiri=='his_surat_external'){
			if ($trx_id != '#'){
				if ($menu_kiri=='his_surat_external'){
					
				}else{
					$data_assemen=$this->Tsurat_model->get_data_surat_external_trx($trx_id);
				}
				
			}else{
				$data_assemen=$this->Tsurat_model->get_data_surat_external($pendaftaran_id,$st_ranap);
				
				
			}
			if ($data_assemen){
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			
			$setting_assesmen=$this->Tsurat_model->setting_surat(6);
			$setting_assesmen_detail=$this->Tsurat_model->setting_surat_detail(6);
			$data = array_merge($data,$data_assemen,$setting_assesmen,$setting_assesmen_detail);
		}
		//AMPUTASI
		//Surat Pengantar External
		if ($menu_kiri=='input_surat_amputasi' || $menu_kiri=='his_surat_amputasi'){
			if ($trx_id != '#'){
				if ($menu_kiri=='his_surat_amputasi'){
					
				}else{
					$data_assemen=$this->Tsurat_model->get_data_surat_amputasi_trx($trx_id,$st_ranap);
				}
				
			}else{
				$data_assemen=$this->Tsurat_model->get_data_surat_amputasi($pendaftaran_id,$st_ranap);
				
				
			}
			if ($data_assemen){
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
			}
			
			$q="SELECT H.assesmen_id,H.tanggal_opr,H.tindakan FROM tranap_lap_bedah H
				WHERE H.pendaftaran_id='$pendaftaran_id' AND H.st_ranap='$st_ranap' AND H.status_assemen='2' ORDER BY H.tanggal_input ASC";
			$data_assemen['list_pra_bedah']=$this->db->query($q)->result();
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			$setting_assesmen=$this->Tsurat_model->setting_surat(7);
			$setting_assesmen_detail=$this->Tsurat_model->setting_surat_detail(7);
			$data = array_merge($data,$data_assemen,$setting_assesmen,$setting_assesmen_detail);
		}
		
		$data['versi_edit']=$versi_edit;
		$data['trx_id']=$trx_id;
		// $data['pendaftaran_id']=$pendaftaran_id;
		// $data['list_ppa_dokter']=$this->Tsurat_model->list_ppa_dokter();
		// $data['list_tujuan']=$this->Tsurat_model->list_tujuan();
		// $data['list_dokter']=$this->Tsurat_model->list_dokter_all();
		// $data['list_dokter_all']=$this->Tsurat_model->list_dokter_all();
		// print_r($data['list_dokter_all']);exit;
		$data['list_poli']=$this->Tsurat_model->list_poliklinik();
		$data = array_merge($data, backend_info());
		// $data = array_merge($data,$data_login_ppa,$data_satuan_ttv,$data_header_ttv,$data_assemen, backend_info());
		$this->parser->parse('module_template', $data);
		
	}
	function selisih_tanggal_surat_sakit(){
		$tgl_1=YMDFormat($this->input->post('tgl_1'));
		$tgl_2=YMDFormat($this->input->post('tgl_2'));
		// print_r($tgl_2);
		// $date1=date_create($tgl_1);
		// $date2=date_create($tgl_2);
		$diff=selisih_tanggal($tgl_1,$tgl_2);
		$this->output->set_output(json_encode($diff));
	}
	function get_selisih_add_surat_sakit(){
		$tgl_1=YMDFormat($this->input->post('tgl_1'));
		$lama=($this->input->post('lama'))-1;
		// print_r($tgl_2);
		// $date1=date_create($tgl_1);
		// $date2=date_create($tgl_2);
		$diff=HumanDateShort(addDayToDate($tgl_1,$lama));
		$this->output->set_output(json_encode($diff));
	}
	function create_assesmen_surat_sakit(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');
	  
	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
			$dpjp=$login_pegawai_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
		  $dpjp=$iddokter;
	  }
	  $data=array(
		'tanggal_input' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'idtipe' => $idtipe,
		'idpoliklinik' => $idpoliklinik,
		'dpjp' => $dpjp,
		'iddokter_peminta' => $iddokter_peminta,
		'created_date' => date('Y-m-d H:i:s'),
		'lama' => 1,
		'mulai' => date('Y-m-d'),
		'selesai' => date('Y-m-d'),
		'created_ppa' => $login_ppa_id,
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tsurat_sakit',$data);
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	function simpan_assesmen_surat_sakit(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$data=array(
			'tanggal_input' => $tanggal_input,
			'pekerjaan_pasien' => $this->input->post('pekerjaan_pasien'),
			'oleh_karena' => $this->input->post('oleh_karena'),
			'lama' => $this->input->post('lama'),
			'dpjp' => $this->input->post('dpjp'),
			'mulai' => YMDFormat($this->input->post('mulai')),
			'selesai' => YMDFormat($this->input->post('selesai')),
			'status_assemen' =>  $this->input->post('status_assemen'),

		);
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			// $jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tsurat_sakit_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tsurat_sakit',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function batal_assesmen_surat_sakit(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	 
	 
	  $data['status_assemen']='0';
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tsurat_sakit',$data);
	  $this->output->set_output(json_encode($hasil));
	}
	function list_surat_sakit(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_sakit H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' AND H.created_ppa='$login_ppa_id' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		$jenis_surat='1';
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_sakit/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_sakit/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
			$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
			$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/1/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2253'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2255'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2256'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dpjp);
		$result[] =$r->lama;
		$result[] = HumanDateShort($r->mulai).' s/d '.HumanDateShort($r->selesai);
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  function list_surat_sakit_history(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_sakit H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$jenis_surat='1';
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_sakit/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_sakit/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
				$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
				$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/1/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2253'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2255'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2256'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dpjp);
		$result[] =$r->lama;
		$result[] = HumanDateShort($r->mulai).' s/d '.HumanDateShort($r->selesai);
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
	public function cetak_surat_sakit($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			*
			FROM msurat_template,msurat_template_var_1 WHERE jenis_surat='1' LIMIT 1";
		$data_format=$this->db->query($q)->row_array();
		// print_r($data_format);exit;
		$q="SELECT 
			H.* ,P.ref nama_pekerjaan,A.ref as alasan
			FROM tsurat_sakit H
			LEFT JOIN merm_referensi P ON P.nilai=H.pekerjaan_pasien AND P.ref_head_id='6'
			LEFT JOIN merm_referensi A ON A.nilai=H.oleh_karena AND A.ref_head_id='412'
			WHERE H.assesmen_id='$assesmen_id'";
		$data_assesmen=$this->db->query($q)->row_array();
        $data_assesmen['title']=$data_format['judul_ina'] .' '.$data_assesmen['nopermintaan'];
        // $data_assesmen['judul_surat']='';
		// print_r($data);exit;
		$data = array_merge($data_assesmen,$data_format,$data_header, backend_info());
		
        $html = $this->load->view('Tsurat/pdf_surat_sakit', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
		if ($st_create=='1'){
			$nama_file=$data_assesmen['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	}
	
	//SURAT SEHAT
	function create_assesmen_surat_sehat(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');
	  
	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
			$dpjp=$login_pegawai_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
		  $dpjp=$iddokter;
	  }
	  $data=array(
		'tanggal_input' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'idtipe' => $idtipe,
		'idpoliklinik' => $idpoliklinik,
		'dpjp' => $dpjp,
		'iddokter_peminta' => $iddokter_peminta,
		'created_date' => date('Y-m-d H:i:s'),
		'created_ppa' => $login_ppa_id,
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tsurat_sehat',$data);
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	function simpan_assesmen_surat_sehat(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$data=array(
			'tanggal_input' => $tanggal_input,
			'dpjp' => $this->input->post('dpjp'),
			'pekerjaan_pasien' => $this->input->post('pekerjaan_pasien'),
			'status_assemen' =>  $this->input->post('status_assemen'),
			'tb' => $this->input->post('tb'),
			'bb' => $this->input->post('bb'),
			'tekanan_darah' => $this->input->post('tekanan_darah'),
			'buta' => $this->input->post('buta'),
			'keadaan' => $this->input->post('keadaan'),
			'informasi' => $this->input->post('informasi'),
		);
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			// $jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tsurat_sehat_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tsurat_sehat',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function batal_assesmen_surat_sehat(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	 
	 
	  $data['status_assemen']='0';
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tsurat_sehat',$data);
	  $this->output->set_output(json_encode($hasil));
	}
	function list_surat_sehat(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_sehat H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' AND H.created_ppa='$login_ppa_id' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		$jenis_surat='2';
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_sehat/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_sehat/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
			$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
			$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2260'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2262'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2263'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dpjp);
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  function list_surat_sehat_history(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_sehat H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$jenis_surat='2';
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_sehat/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_sehat/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
				$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
				$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2253'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2255'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2256'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dpjp);
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
	public function cetak_surat_sehat($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			*
			FROM msurat_template,msurat_template_var_2 WHERE jenis_surat='2' LIMIT 1";
		$data_format=$this->db->query($q)->row_array();
		// print_r($data_format);exit;
		$q="SELECT 
			H.* ,P.ref nama_pekerjaan,B.ref as ket_buta,K.ref ket_keadaan
			FROM tsurat_sehat H
			LEFT JOIN merm_referensi P ON P.nilai=H.pekerjaan_pasien AND P.ref_head_id='6'
			LEFT JOIN merm_referensi B ON B.nilai=H.buta AND B.ref_head_id='413'
			LEFT JOIN merm_referensi K ON K.nilai=H.keadaan AND K.ref_head_id='414'
			WHERE H.assesmen_id='$assesmen_id'";
		$data_assesmen=$this->db->query($q)->row_array();
        $data_assesmen['title']=$data_format['judul_ina'] .' '.$data_assesmen['nopermintaan'];
        // $data_assesmen['judul_surat']='';
		// print_r($data);exit;
		$data = array_merge($data_assesmen,$data_format,$data_header, backend_info());
		
        $html = $this->load->view('Tsurat/pdf_surat_sehat', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
		if ($st_create=='1'){
			$nama_file=$data_assesmen['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	}
	//SURAT SEHAT
	function create_assesmen_surat_kontrol(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');
	  $date2=date_create(date('Y-m-d'));
	  date_add($date2,date_interval_create_from_date_string("7 days"));
		$date2= date_format($date2,"Y-m-d");

	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
			$dpjp=$login_pegawai_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
		  $dpjp=$iddokter;
	  }
	  $data=array(
		'tanggal_input' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'idtipe' => $idtipe,
		'idpoliklinik' => $idpoliklinik,
		'dpjp' => $dpjp,
		'iddokter_peminta' => $iddokter_peminta,
		'tanggal_pelayanan' => date('Y-m-d'),
		'tanggal_kontrol' => $date2,
		'created_date' => date('Y-m-d H:i:s'),
		'created_ppa' => $login_ppa_id,
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tsurat_kontrol',$data);
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	function simpan_assesmen_surat_kontrol(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$data=array(
			'tanggal_input' => $tanggal_input,
			'dpjp' => $this->input->post('dpjp'),
			'pekerjaan_pasien' => $this->input->post('pekerjaan_pasien'),
			'status_assemen' =>  $this->input->post('status_assemen'),
			'tanggal_pelayanan' => YMDFormat($this->input->post('tanggal_pelayanan')),
			'tanggal_kontrol' => YMDFormat($this->input->post('tanggal_kontrol')),
			'diagnosa' => $this->input->post('diagnosa'),
		);
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			// $jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tsurat_kontrol_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tsurat_kontrol',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function batal_assesmen_surat_kontrol(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	 
	 
	  $data['status_assemen']='0';
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tsurat_kontrol',$data);
	  $this->output->set_output(json_encode($hasil));
	}
	function list_surat_kontrol(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_kontrol H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' AND H.created_ppa='$login_ppa_id' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		$jenis_surat='3';
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_kontrol/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_kontrol/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
			$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
			$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2269'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2271'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2272'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dpjp);
		$result[] = ($r->diagnosa);
		$result[] = HumanDateShort($r->tanggal_kontrol);
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  function list_surat_kontrol_history(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_kontrol H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$jenis_surat='3';
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_kontrol/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_kontrol/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
				$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
				$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2269'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2271'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2272'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dpjp);
		$result[] = ($r->diagnosa);
		$result[] = HumanDateShort($r->tanggal_kontrol);
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
	public function cetak_surat_kontrol($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			*
			FROM msurat_template,msurat_template_var_3 WHERE jenis_surat='3' LIMIT 1";
		$data_format=$this->db->query($q)->row_array();
		// print_r($data_format);exit;
		$q="SELECT 
			H.* ,P.ref nama_pekerjaan,JK.ref as nama_jk
			FROM tsurat_kontrol H
			LEFT JOIN merm_referensi P ON P.nilai=H.pekerjaan_pasien AND P.ref_head_id='6'
			LEFT JOIN merm_referensi JK ON JK.nilai=H.jk AND JK.ref_head_id='1'
			WHERE H.assesmen_id='$assesmen_id'";
		$data_assesmen=$this->db->query($q)->row_array();
        $data_assesmen['title']=$data_format['judul_ina'] .' '.$data_assesmen['nopermintaan'];
        // $data_assesmen['judul_surat']='';
		// print_r($data);exit;
		$data = array_merge($data_assesmen,$data_format,$data_header, backend_info());
		
        $html = $this->load->view('Tsurat/pdf_surat_kontrol', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
		if ($st_create=='1'){
			$nama_file=$data_assesmen['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	}
	
	//SURAT LANJUT
	function create_assesmen_surat_lanjut(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');

	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
			$dpjp=$login_pegawai_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
		  $dpjp=$iddokter;
	  }
	  $data=array(
		'tanggal_input' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'idtipe' => $idtipe,
		'idpoliklinik' => $idpoliklinik,
		'dpjp' => $dpjp,
		'iddokter_peminta' => $iddokter_peminta,
		'created_date' => date('Y-m-d H:i:s'),
		'created_ppa' => $login_ppa_id,
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tsurat_lanjut',$data);
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	function simpan_assesmen_surat_lanjut(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$data=array(
			'tanggal_input' => $tanggal_input,
			'dpjp' => $this->input->post('dpjp'),
			'status_assemen' =>  $this->input->post('status_assemen'),
			'kepada' => $this->input->post('kepada'),
			'up' => $this->input->post('up'),
			'di' => $this->input->post('di'),
			'diagnosa' => $this->input->post('diagnosa'),
			'tindakan' => $this->input->post('tindakan'),

		);
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			// $jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tsurat_lanjut_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tsurat_lanjut',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function batal_assesmen_surat_lanjut(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	 
	 
	  $data['status_assemen']='0';
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tsurat_lanjut',$data);
	  $this->output->set_output(json_encode($hasil));
	}
	function list_surat_lanjut(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_lanjut H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' AND H.created_ppa='$login_ppa_id' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		$jenis_surat='4';
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_lanjut/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_lanjut/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
			$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
			$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2275'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2277'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2278'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dpjp);
		$result[] = ($r->kepada);
		$result[] = ($r->di);
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  function list_surat_lanjut_history(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_lanjut H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$jenis_surat='4';
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_lanjut/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_lanjut/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
				$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
				$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2253'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2255'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2256'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dpjp);
		$result[] = ($r->kepada);
		$result[] = ($r->di);
		// $result[] = HumanDateShort($r->tanggal_input);
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
	public function cetak_surat_lanjut($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			*
			FROM msurat_template,msurat_template_var_4 WHERE jenis_surat='4' LIMIT 1";
		$data_format=$this->db->query($q)->row_array();
		// print_r($data_format);exit;
		$q="SELECT 
			H.* 
			FROM tsurat_lanjut H
			WHERE H.assesmen_id='$assesmen_id'";
		$data_assesmen=$this->db->query($q)->row_array();
        $data_assesmen['title']=$data_format['judul_ina'] .' '.$data_assesmen['nopermintaan'];
        // $data_assesmen['judul_surat']='';
		// print_r($data);exit;
		$data = array_merge($data_assesmen,$data_format,$data_header, backend_info());
		
        $html = $this->load->view('Tsurat/pdf_surat_lanjut', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
		if ($st_create=='1'){
			$nama_file=$data_assesmen['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	}
	function selisih_tanggal_surat_rawat(){
		$tgl_1=YMDFormat($this->input->post('tgl_1'));
		$tgl_2=YMDFormat($this->input->post('tgl_2'));
		// print_r($tgl_2);
		// $date1=date_create($tgl_1);
		// $date2=date_create($tgl_2);
		$diff=selisih_tanggal($tgl_1,$tgl_2);
		$this->output->set_output(json_encode($diff));
	}
	function get_selisih_add_surat_rawat(){
		$tgl_1=YMDFormat($this->input->post('tgl_1'));
		$lama=($this->input->post('lama'))-1;
		// print_r($tgl_2);
		// $date1=date_create($tgl_1);
		// $date2=date_create($tgl_2);
		$diff=HumanDateShort(addDayToDate($tgl_1,$lama));
		$this->output->set_output(json_encode($diff));
	}
	function create_assesmen_surat_rawat(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');
	  
	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
			$dpjp=$login_pegawai_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
		  $dpjp=$iddokter;
	  }
	  $data=array(
		'tanggal_input' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'idtipe' => $idtipe,
		'idpoliklinik' => $idpoliklinik,
		'dpjp' => $dpjp,
		'iddokter_peminta' => $iddokter_peminta,
		'created_date' => date('Y-m-d H:i:s'),
		'lama' => 1,
		'mulai' => date('Y-m-d'),
		'selesai' => date('Y-m-d'),
		'created_ppa' => $login_ppa_id,
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tsurat_rawat',$data);
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	function simpan_assesmen_surat_rawat(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$data=array(
			'tanggal_input' => $tanggal_input,
			'pekerjaan_pasien' => $this->input->post('pekerjaan_pasien'),
			// 'oleh_karena' => $this->input->post('oleh_karena'),
			'lama' => $this->input->post('lama'),
			'dpjp' => $this->input->post('dpjp'),
			'mulai' => YMDFormat($this->input->post('mulai')),
			'selesai' => YMDFormat($this->input->post('selesai')),
			'status_assemen' =>  $this->input->post('status_assemen'),

		);
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			// $jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tsurat_rawat_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tsurat_rawat',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function batal_assesmen_surat_rawat(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	 
	 
	  $data['status_assemen']='0';
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tsurat_rawat',$data);
	  $this->output->set_output(json_encode($hasil));
	}
	function list_surat_rawat(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_rawat H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' AND H.created_ppa='$login_ppa_id' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		$jenis_surat='5';
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_rawat/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_rawat/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
			$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
			$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2281'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2283'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2284'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dpjp);
		$result[] =$r->lama;
		$result[] = HumanDateShort($r->mulai).' s/d '.HumanDateShort($r->selesai);
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  function list_surat_rawat_history(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_rawat H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$jenis_surat='5';
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_rawat/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_rawat/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
				$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
				$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2281'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2283'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2284'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dpjp);
		$result[] =$r->lama;
		$result[] = HumanDateShort($r->mulai).' s/d '.HumanDateShort($r->selesai);
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
	public function cetak_surat_rawat($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			*
			FROM msurat_template,msurat_template_var_5 WHERE jenis_surat='5' LIMIT 1";
		$data_format=$this->db->query($q)->row_array();
		// print_r($data_format);exit;
		$q="SELECT 
			H.* ,P.ref nama_pekerjaan,A.ref as alasan
			FROM tsurat_rawat H
			LEFT JOIN merm_referensi P ON P.nilai=H.pekerjaan_pasien AND P.ref_head_id='6'
			LEFT JOIN merm_referensi A ON A.nilai=H.oleh_karena AND A.ref_head_id='412'
			WHERE H.assesmen_id='$assesmen_id'";
		$data_assesmen=$this->db->query($q)->row_array();
        $data_assesmen['title']=$data_format['judul_ina'] .' '.$data_assesmen['nopermintaan'];
        // $data_assesmen['judul_surat']='';
		// print_r($data);exit;
		$data = array_merge($data_assesmen,$data_format,$data_header, backend_info());
		
        $html = $this->load->view('Tsurat/pdf_surat_rawat', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
		if ($st_create=='1'){
			$nama_file=$data_assesmen['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	}
	//SURAT LANJUT
	function create_assesmen_surat_external(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');

	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
			$dpjp=$login_pegawai_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
		  $dpjp=$iddokter;
	  }
	  $data=array(
		'tanggal_input' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'idtipe' => $idtipe,
		'idpoliklinik' => $idpoliklinik,
		'dpjp' => $dpjp,
		'iddokter_peminta' => $iddokter_peminta,
		'created_date' => date('Y-m-d H:i:s'),
		'created_ppa' => $login_ppa_id,
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tsurat_external',$data);
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	function simpan_assesmen_surat_external(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$data=array(
			'tanggal_input' => $tanggal_input,
			'dpjp' => $this->input->post('dpjp'),
			'status_assemen' =>  $this->input->post('status_assemen'),
			'kepada' => $this->input->post('kepada'),
			'mohon' => $this->input->post('mohon'),
			'detail' => $this->input->post('detail'),
			'diagnosa' => $this->input->post('diagnosa'),

		);
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			// $jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tsurat_external_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tsurat_external',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function batal_assesmen_surat_external(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	 
	 
	  $data['status_assemen']='0';
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tsurat_external',$data);
	  $this->output->set_output(json_encode($hasil));
	}
	function list_surat_external(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_external H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' AND H.created_ppa='$login_ppa_id' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		$jenis_surat='6';
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_external/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_external/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
			$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
			$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2346'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2348'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2349'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dpjp);
		$result[] = ($r->kepada);
		$result[] = ($r->mohon);
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  function list_surat_external_history(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_external H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$jenis_surat='6';
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_external/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_external/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
				$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
				$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2346'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2348'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2349'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dpjp);
		$result[] = ($r->kepada);
		$result[] = HumanDateShort($r->mohon);
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
	public function cetak_surat_external($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			*
			FROM msurat_template,msurat_template_var_6 WHERE jenis_surat='6' LIMIT 1";
		$data_format=$this->db->query($q)->row_array();
		// print_r($data_format);exit;
		$q="SELECT 
			H.* 
			FROM tsurat_external H
			WHERE H.assesmen_id='$assesmen_id'";
		$data_assesmen=$this->db->query($q)->row_array();
        $data_assesmen['title']=$data_format['judul_ina'] .' '.$data_assesmen['nopermintaan'];
        // $data_assesmen['judul_surat']='';
		// print_r($data);exit;
		$data = array_merge($data_assesmen,$data_format,$data_header, backend_info());
		
        $html = $this->load->view('Tsurat/pdf_surat_external', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
		if ($st_create=='1'){
			$nama_file=$data_assesmen['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	}
	public function cetak_permintaan_djp($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			judul_header,judul_header_eng,footer_ina,footer_eng
			FROM setting_permintaan_dpjp  LIMIT 1";
		$data_judul=$this->db->query($q)->row_array();
		
		$q="SELECT 
			*
			FROM setting_permintaan_dpjp_keterangan  LIMIT 1";
		$data_format=$this->db->query($q)->row_array();
		// print_r($data_format);exit;
		$q="SELECT 
			CASE WHEN H.st_ranap='1' THEN RI.no_medrec ELSE RJ.no_medrec END no_medrec
			,CASE WHEN H.st_ranap='1' THEN RI.nopendaftaran ELSE RJ.nopendaftaran END nopendaftaran
			,CASE WHEN H.st_ranap='1' THEN RI.namapasien ELSE RJ.namapasien END namapasien
			,CASE WHEN H.st_ranap='1' THEN RI.tanggal_lahir ELSE RJ.tanggal_lahir END tanggal_lahir
			,CASE WHEN H.st_ranap='1' THEN RI.umurhari ELSE RJ.umurhari END umurhari
			,CASE WHEN H.st_ranap='1' THEN RI.umurbulan ELSE RJ.umurbulan END umurbulan
			,CASE WHEN H.st_ranap='1' THEN RI.umurtahun ELSE RJ.umurtahun END umurtahun
			,CASE WHEN H.st_ranap='1' THEN RI.jenis_kelamin ELSE RJ.jenis_kelamin END jenis_kelamin
			,MD1.nama as nama_dokter
			,H.* 
			
			FROM tranap_permintaan_dpjp H
			LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.pendaftaran_id_ranap AND H.st_ranap='1'
			LEFT JOIN tpoliklinik_pendaftaran RJ ON RJ.id=H.pendaftaran_id AND H.st_ranap='0'
			LEFT JOIN mdokter MD1 ON MD1.id=H.iddokter
			WHERE H.assesmen_id='$assesmen_id'";
		$data_assesmen=$this->db->query($q)->row_array();
        $data_assesmen['title']=$data_judul['judul_header'] .' - '.$data_assesmen['assesmen_id'];
        $data_assesmen['isi_surat']=1;
        // $data_assesmen['judul_surat']='';
		// print_r($data);exit;
		$data = array_merge($data_assesmen,$data_judul,$data_format,$data_header, backend_info());
		
        $html = $this->load->view('Tsurat/pdf_permintaan_djp', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
		if ($st_create=='1'){
			$nama_file=$data_assesmen['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	}
	
	//SURAT AMPUTASI
	function create_assesmen_surat_amputasi(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');
	  
	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
			$dpjp=$login_pegawai_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
		  $dpjp=$iddokter;
	  }
	  $data=array(
		'tanggal_input' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'mengetahui' => $login_ppa_id,
		'saksi_rs' => $login_ppa_id,
		
		'created_date' => date('Y-m-d H:i:s'),
		'created_ppa' => $login_ppa_id,
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tsurat_amputasi',$data);
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	function load_lap_bedah_surat_amputasi(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $st_ranap=$this->input->post('st_ranap');
	  $q="SELECT H.assesmen_id,DATE_FORMAT(H.tanggal_opr,'%d-%m-%Y') as tanggal_tindakan,H.tindakan,H.diagnosa_pasca as diagnosa FROM tranap_lap_bedah H
			WHERE H.assesmen_id='$assesmen_id' AND H.status_assemen='2' ORDER BY H.tanggal_input ASC";
	  // print_r($q);exit;	
	  $hasil=$this->db->query($q)->row_array();
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	function load_alamat_amputasi(){
	  $id=$this->input->post('id');
	  $q="SELECT H.alamat_jalan as alamat FROM `mfpasien` H WHERE H.id='$id'";
	  // print_r($q);exit;	
	  $hasil=$this->db->query($q)->row_array();
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	function simpan_assesmen_surat_amputasi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$tanggal_tindakan= ($this->input->post('tanggal_tindakan')?YMDFormat($this->input->post('tanggal_tindakan')):null);
		$data=array(
			'status_assemen' => $status_assemen,
			'tanggal_input' => $tanggal_input,
			'diagnosa' => $this->input->post('diagnosa'),
			'tindakan' => $this->input->post('tindakan'),
			'tanggal_tindakan' => $tanggal_tindakan,
			'nama_tubuh' => $this->input->post('nama_tubuh'),
			'alamat_pulang' => $this->input->post('alamat_pulang'),
			'nama' => $this->input->post('nama'),
			'alamat' => $this->input->post('alamat'),
			'nik' => $this->input->post('nik'),
			'hubungan' => $this->input->post('hubungan'),
		);
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			// $jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tsurat_amputasi_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tsurat_amputasi',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function simpan_saksi_amputasi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'mengetahui' => $this->input->post('mengetahui'),
			'saksi_rs' => $this->input->post('saksi_rs'),
			
		);
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tsurat_amputasi',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function batal_assesmen_surat_amputasi(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	 
	 
	  $data['status_assemen']='0';
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tsurat_amputasi',$data);
	  $this->output->set_output(json_encode($hasil));
	}
	function list_surat_amputasi(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit
					,CASE WHEN H.st_ranap='1' THEN MR.tanggaldaftar ELSE MP.tanggaldaftar END tanggaldaftar
					,CASE WHEN H.st_ranap='1' THEN MR.namapasien ELSE MP.namapasien END namapasien
					,CASE WHEN H.st_ranap='1' THEN MR.no_medrec ELSE MP.no_medrec END no_medrec
					FROM tsurat_amputasi H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap  AND H.st_ranap='1'
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' AND H.created_ppa='$login_ppa_id' ".$where."
					
				) as tbl ORDER BY tbl.tanggal_input DESC
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		$jenis_surat='7';
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_amputasi/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_amputasi/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
			$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
			$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2281'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2283'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2284'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->namapasien).'<br>'.$r->no_medrec.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = ($r->pemohon);
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  function list_surat_amputasi_history(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit
					,CASE WHEN H.st_ranap='1' THEN MR.tanggaldaftar ELSE MP.tanggaldaftar END tanggaldaftar
					,CASE WHEN H.st_ranap='1' THEN MR.namapasien ELSE MP.namapasien END namapasien
					,CASE WHEN H.st_ranap='1' THEN MR.no_medrec ELSE MP.no_medrec END no_medrec
					FROM tsurat_amputasi H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap  AND H.st_ranap='1'
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','namapasien','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$jenis_surat='7';
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_amputasi/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_amputasi/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
				$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
				$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2281'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2283'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2284'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->namapasien).'<br>'.$r->no_medrec.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = ($r->pemohon);
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
	public function cetak_surat_amputasi($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			*
			FROM msurat_template,msurat_template_var_7 WHERE jenis_surat='7' LIMIT 1";
		$data_format=$this->db->query($q)->row_array();
		// print_r($data_format);exit;
		$q="SELECT 
			H.*
		
			FROM tsurat_amputasi H
			
			WHERE H.assesmen_id='$assesmen_id'";
		$st_ranap=$this->db->query($q)->row('st_ranap');
		$data_assesmen=$this->Tsurat_model->get_data_surat_amputasi_trx($assesmen_id,$st_ranap);
		// print_r($data_assesmen['nopermintaan']);exit;
        $data_assesmen['title']=$data_format['judul_ina'] .' '.$data_assesmen['nopermintaan'];
        // $data_assesmen['title']='TEST';
        // $data_assesmen['judul_surat']='';
		$data = array_merge($data_assesmen,$data_format,$data_header, backend_info());
		
        $html = $this->load->view('Tsurat/pdf_surat_amputasi', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('f4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
		if ($st_create=='1'){
			$nama_file=$data_assesmen['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	}
	function save_ttd_amputasi(){
		
		$assesmen_id = $this->input->post('assesmen_id');
		$nama_field = $this->input->post('nama_field');
		$jawaban_ttd = $this->input->post('signature64');
		$nama_ttd = $this->input->post('nama_ttd');
		$data=array(
			"$nama_field".'_ttd' =>$jawaban_ttd,
			"$nama_field" =>$nama_ttd,
		);
		// print_r($id);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update('tsurat_amputasi',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function edit_pilih_surat_amputasi(){
		  $login_ppa_id=$this->session->userdata('login_ppa_id');
		  $login_nip_ppa=$this->session->userdata('login_nip_ppa');
		  $tanggal_input= date('Y-m-d H:i:s');
		  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		  $pendaftaran_id=$this->input->post('pendaftaran_id');
		  $pilih_ttd_id=$this->input->post('pilih_ttd_id');
		  $idpasien=$this->input->post('idpasien');
		  $assesmen_id=$this->input->post('assesmen_id');
		  $st_ranap=$this->input->post('st_ranap');
		 
		 if ($st_ranap=='1'){
			$data=$this->get_pilih_ttd_surat_amptuasi($pilih_ttd_id,$pendaftaran_id_ranap,$st_ranap);
		  }else{
			$data=$this->get_pilih_ttd_surat_amptuasi($pilih_ttd_id,$pendaftaran_id,$st_ranap);
		  }
		  // $data['pilih_ttd_id']=$pilih_ttd_id;
		  // // print_r($data);
		  // $this->db->where('assesmen_id',$assesmen_id);
		  // $hasil=$this->db->update('tranap_permintaan_dpjp',$data);
		  
		  $this->output->set_output(json_encode($data));
    }
	function get_pilih_ttd_surat_amptuasi($id,$pendaftaran_id_ranap,$st_ranap='1'){
		if ($st_ranap=='1'){
			if ($id=='1'){
				$q="SELECT 
					H.namapenanggungjawab as nama_ttd,H.tanggal_lahirpenanggungjawab as ttl_ttd,CONCAT(H.umur_tahunpenanggungjawab,' Tahun ',H.umur_bulanpenanggungjawab,' Bulan ',H.umur_haripenanggungjawab,' Hari') as umur_ttd
					,H.alamatpenanggungjawab as alamat_ttd,H.provinsi_idpenanggungjawab as provinsi_id_ttd,H.kabupaten_idpenanggungjawab as kabupaten_id_ttd,H.kecamatan_idpenanggungjawab as kecamatan_id_ttd
					,H.kelurahan_idpenanggungjawab as kelurahan_id_ttd,H.rwpenanggungjawab as rw_ttd,H.rtpenanggungjawab as rt_ttd,H.kodepospenanggungjawab as kodepos_ttd
					,H.hubunganpenanggungjawab as hubungan_ttd,H.iddokterpenanggungjawab as iddokter
					
					FROM trawatinap_pendaftaran H

					WHERE H.id='$pendaftaran_id_ranap'";
			}
			if ($id=='2'){
				$q="
					SELECT 
					H.namapengantar as nama_ttd,H.tanggal_lahirpengantar as ttl_ttd,CONCAT(H.umur_tahunpengantar,' Tahun ',H.umur_bulanpengantar,' Bulan ',H.umur_haripengantar,' Hari') as umur_ttd
					,H.alamatpengantar as alamat_ttd,H.provinsi_idpengantar as provinsi_id_ttd,H.kabupaten_idpengantar as kabupaten_id_ttd,H.kecamatan_idpengantar as kecamatan_id_ttd
					,H.kelurahan_idpengantar as kelurahan_id_ttd,H.rwpengantar as rw_ttd,H.rtpengantar as rt_ttd,H.kodepospengantar as kodepos_ttd
					,H.hubunganpengantar as hubungan_ttd,H.iddokterpenanggungjawab as iddokter

					FROM trawatinap_pendaftaran H

					WHERE H.id='$pendaftaran_id_ranap'
					";
			}
			if ($id=='3'){
				$q="
					SELECT 
					H.namapasien as nama_ttd,H.tanggal_lahir as ttl_ttd,CONCAT(H.umurtahun,' Tahun ',H.umurbulan,' Bulan ',H.umurhari,' Hari') as umur_ttd
					,H.alamatpasien as alamat_ttd,H.provinsi_id as provinsi_id_ttd,H.kabupaten_id as kabupaten_id_ttd,H.kecamatan_id as kecamatan_id_ttd
					,H.kelurahan_id as kelurahan_id_ttd,H.rw as rw_ttd,H.rt as rt_ttd,H.kodepos as kodepos_ttd
					,H.hubungan as hubungan_ttd,H.iddokterpenanggungjawab as iddokter

					FROM trawatinap_pendaftaran H

					WHERE H.id='$pendaftaran_id_ranap'
					";
			}
			if ($id=='4'){
				$q="
					SELECT 
					null as nama_ttd,null as ttl_ttd,null as umur_ttd,null as alamat_ttd,null as provinsi_id_ttd,null as kabupaten_id_ttd,null as kecamatan_id_ttd,null as kelurahan_id_ttd,null as rw_ttd,null as rt_ttd,null as kodepos_ttd,null as hubungan_ttd
					,H.iddokterpenanggungjawab as iddokter

					FROM trawatinap_pendaftaran H

					WHERE H.id='$pendaftaran_id_ranap'
					";
			}
		}else{
			if ($id=='1'){
				$q="SELECT 
					H.namapenanggungjawab as nama_ttd,null as ttl_ttd,'' as umur_ttd
					,H.alamatpenanggungjawab as alamat_ttd,null as provinsi_id_ttd,null as kabupaten_id_ttd,null as kecamatan_id_ttd
					,null as kelurahan_id_ttd,null as rw_ttd,null as rt_ttd,null as kodepos_ttd
					,null as hubungan_ttd,H.iddokter as iddokter
					
					FROM tpoliklinik_pendaftaran H

					WHERE H.id='$pendaftaran_id_ranap'";
			}
			if ($id=='2'){
				$q="
					SELECT 
					H.namapengantar as nama_ttd,'' as ttl_ttd,'' as umur_ttd
					,H.alamatpengantar as alamat_ttd,null as provinsi_id_ttd,null as kabupaten_id_ttd,null as kecamatan_id_ttd
					,null as kelurahan_id_ttd,null as rw_ttd,null as rt_ttd,null as kodepos_ttd
					,H.hubungan_pengantar as hubungan_ttd,H.iddokter as iddokter

					FROM tpoliklinik_pendaftaran H

					WHERE H.id='$pendaftaran_id_ranap'
					";
			}
			if ($id=='3'){
				$q="
					SELECT 
					H.namapasien as nama_ttd,H.tanggal_lahir as ttl_ttd,CONCAT(H.umurtahun,' Tahun ',H.umurbulan,' Bulan ',H.umurhari,' Hari') as umur_ttd
					,H.alamatpasien as alamat_ttd,H.provinsi_id as provinsi_id_ttd,H.kabupaten_id as kabupaten_id_ttd,H.kecamatan_id as kecamatan_id_ttd
					,H.kelurahan_id as kelurahan_id_ttd,H.rw as rw_ttd,H.rt as rt_ttd,H.kodepos as kodepos_ttd
					,H.hubungan as hubungan_ttd,H.iddokter as iddokter

					FROM tpoliklinik_pendaftaran H

					WHERE H.id='$pendaftaran_id_ranap'
					";
			}
			if ($id=='4'){
				$q="
					SELECT 
					null as nama_ttd,null as ttl_ttd,null as umur_ttd,null as alamat_ttd,null as provinsi_id_ttd,null as kabupaten_id_ttd,null as kecamatan_id_ttd,null as kelurahan_id_ttd,null as rw_ttd,null as rt_ttd,null as kodepos_ttd,null as hubungan_ttd
					,H.iddokter as iddokter

					FROM tpoliklinik_pendaftaran H

					WHERE H.id='$pendaftaran_id_ranap'
					";
			}
		}
		return $this->db->query($q)->row_array();
	}
	function selisih_tanggal_surat_diagnosa(){
		$tgl_1=YMDFormat($this->input->post('tgl_1'));
		$tgl_2=YMDFormat($this->input->post('tgl_2'));
		// print_r($tgl_2);
		// $date1=date_create($tgl_1);
		// $date2=date_create($tgl_2);
		$diff=selisih_tanggal($tgl_1,$tgl_2);
		$this->output->set_output(json_encode($diff));
	}
	function get_selisih_add_surat_diagnosa(){
		$tgl_1=YMDFormat($this->input->post('tgl_1'));
		$lama=($this->input->post('lama'))-1;
		// print_r($tgl_2);
		// $date1=date_create($tgl_1);
		// $date2=date_create($tgl_2);
		$diff=HumanDateShort(addDayToDate($tgl_1,$lama));
		$this->output->set_output(json_encode($diff));
	}
	function create_assesmen_surat_diagnosa(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');
	  
	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
			$dpjp=$login_pegawai_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
		  $dpjp=$iddokter;
	  }
	  if ($st_ranap=='1'){
		  $q="SELECT DATE(H.tanggaldaftar) as tanggal_pelayanan_awal
			,CASE WHEN H.statuscheckout='1' THEN DATE(H.tanggalcheckout) ELSE null END as tanggal_pelayanan_akhir 
			FROM `trawatinap_pendaftaran` H
			WHERE H.id='$pendaftaran_id_ranap'";
		  $data_tanggal=$this->db->query($q)->row();
		  
	  }else{
		  $q="
			SELECT DATE(H.tanggal) as tanggal_pelayanan_awal
			,null as tanggal_pelayanan_akhir 
			FROM `tpoliklinik_pendaftaran` H
			WHERE H.id='$pendaftaran_id'
		  ";
		  $data_tanggal=$this->db->query($q)->row();
	  }
	  $data=array(
		'tanggal_input' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'idtipe' => $idtipe,
		'dpjp' => $dpjp,
		'created_date' => date('Y-m-d H:i:s'),
		'created_ppa' => $login_ppa_id,
		'tanggal_pelayanan_awal' => $data_tanggal->tanggal_pelayanan_awal,
		'tanggal_pelayanan_akhir' => $data_tanggal->tanggal_pelayanan_akhir,
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tsurat_diagnosa',$data);
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	function simpan_assesmen_surat_diagnosa(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$data=array(
			'tanggal_input' => $tanggal_input,
			'status_assemen' =>  $this->input->post('status_assemen'),
			'dpjp' => $this->input->post('dpjp'),
			'riwayat_penyakit' => $this->input->post('riwayat_penyakit'),
			'penunjang' => $this->input->post('penunjang'),
			'diagnosa_akhir' => $this->input->post('diagnosa_akhir'),
			'terapi' => $this->input->post('terapi'),
			'tindakan' => $this->input->post('tindakan'),
			'keadaan' => $this->input->post('keadaan'),
		);
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			// $jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tsurat_diagnosa_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tsurat_diagnosa',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function ganti_tanggal_diagnosa(){
		$assesmen_id=$this->input->post('assesmen_id');
		$tanggal_1=$this->input->post('tanggal_1');
		$tanggal_2=$this->input->post('tanggal_2');
		
		$data=array(
			'tanggal_pelayanan_awal' => YMDFormat($tanggal_1),
			'tanggal_pelayanan_akhir' => ($tanggal_2=='0'?null:YMDFormat($tanggal_2)),
			
		);
		// print_r($data);exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tsurat_diagnosa',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function batal_assesmen_surat_diagnosa(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	 
	 
	  $data['status_assemen']='0';
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tsurat_diagnosa',$data);
	  $this->output->set_output(json_encode($hasil));
	}
	function list_surat_diagnosa(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_diagnosa H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' AND H.created_ppa='$login_ppa_id' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		$jenis_surat='8';
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_diagnosa/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_diagnosa/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
			$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
			$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2556'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2558'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2559'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = $r->nama_dpjp;
		$result[] = HumanDateShort($r->tanggal_pelayanan_awal).($r->tanggal_pelayanan_akhir ? ' s/d '.($r->tanggal_pelayanan_akhir):'');
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  function list_surat_diagnosa_history(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_diagnosa H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$jenis_surat='8';
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_diagnosa/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_diagnosa/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
				$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
				$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2556'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2558'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2559'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = $r->nama_dpjp;
		$result[] = HumanDateShort($r->tanggal_pelayanan_awal).($r->tanggal_pelayanan_akhir ? ' s/d '.($r->tanggal_pelayanan_akhir):'');
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
	public function cetak_surat_diagnosa($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			*
			FROM msurat_template,msurat_template_var_8 WHERE jenis_surat='8' LIMIT 1";
		$data_format=$this->db->query($q)->row_array();
		// print_r($data_format);exit;
		$q="SELECT H.st_ranap FROM tsurat_diagnosa H
			WHERE H.assesmen_id='$assesmen_id'";
		$st_ranap=$this->db->query($q)->row('st_ranap');
		$data_assesmen=$this->Tsurat_model->get_data_surat_diagnosa_cetak($assesmen_id,$st_ranap);
        $data_assesmen['title']=$data_format['judul_ina'] .' '.$data_assesmen['nopermintaan'];
        // $data_assesmen['judul_surat']='';
		// print_r($data);exit;
		$data = array_merge($data_assesmen,$data_format,$data_header, backend_info());
		
        $html = $this->load->view('Tsurat/pdf_surat_diagnosa', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
		if ($st_create=='1'){
			$nama_file=$data_assesmen['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	}
	function generate_rj_diagnosa(){
		$assesmen_id=$this->input->post('assesmen_id');


		$q="SELECT H.keluhan_utama,H.diagnosa_utama,H.rencana_asuhan as rencana  FROM tpoliklinik_asmed H WHERE H.assesmen_id='$assesmen_id'";
		$riwayat_penyakit=$this->db->query($q)->row('keluhan_utama');
		$diagnosa_akhir=$this->db->query($q)->row('diagnosa_utama');
		$tindakan=$this->db->query($q)->row('rencana');

		$data['riwayat_penyakit']=$riwayat_penyakit;
		$data['diagnosa_akhir']=$diagnosa_akhir;
		$data['tindakan']=$tindakan;

		$this->output->set_output(json_encode($data));
	}
	function generate_igd_diagnosa(){
		$assesmen_id=$this->input->post('assesmen_id');


		$q="SELECT H.keluhan_utama,H.diagnosa_utama,H.rencana_asuhan as rencana  FROM tpoliklinik_asmed_igd H WHERE H.assesmen_id='$assesmen_id'";
		$riwayat_penyakit=$this->db->query($q)->row('keluhan_utama');
		$diagnosa_akhir=$this->db->query($q)->row('diagnosa_utama');
		$tindakan=$this->db->query($q)->row('rencana');

		$data['riwayat_penyakit']=$riwayat_penyakit;
		$data['diagnosa_akhir']=$diagnosa_akhir;
		$data['tindakan']=$tindakan;

		$this->output->set_output(json_encode($data));
	}
	function generate_ranap_diagnosa(){
		$assesmen_id=$this->input->post('assesmen_id');


		$q="SELECT H.keluhan_utama,H.diagnosa_kerja as diagnosa_utama,H.rencana as rencana  FROM tranap_asmed_ri H WHERE H.assesmen_id='$assesmen_id'";
		$riwayat_penyakit=$this->db->query($q)->row('keluhan_utama');
		$diagnosa_akhir=$this->db->query($q)->row('diagnosa_utama');
		$tindakan=$this->db->query($q)->row('rencana');

		$data['riwayat_penyakit']=$riwayat_penyakit;
		$data['diagnosa_akhir']=$diagnosa_akhir;
		$data['tindakan']=$tindakan;

		$this->output->set_output(json_encode($data));
	}
	function generate_cppt_diagnosa(){
		$assesmen_id=$this->input->post('assesmen_id');


		$q="SELECT H.subjectif as keluhan_utama,H.assemen as diagnosa_utama,H.planing as rencana  FROM tpoliklinik_cppt H WHERE H.assesmen_id='$assesmen_id'";
		$riwayat_penyakit=$this->db->query($q)->row('keluhan_utama');
		$diagnosa_akhir=$this->db->query($q)->row('diagnosa_utama');
		$tindakan=$this->db->query($q)->row('rencana');

		$data['riwayat_penyakit']=$riwayat_penyakit;
		$data['diagnosa_akhir']=$diagnosa_akhir;
		$data['tindakan']=$tindakan;

		$this->output->set_output(json_encode($data));
	}
	function generate_ringkasan_diagnosa(){
		$assesmen_id=$this->input->post('assesmen_id');


		$q="SELECT H.indikasi_rawat as keluhan_utama,H.diagnosa_utama as diagnosa_utama,'' as rencana,M.ref as prognis 
		FROM tranap_ringkasan_pulang H 
		LEFT JOIN merm_referensi M ON M.nilai=H.prognis AND M.ref_head_id='415'
		WHERE H.assesmen_id='$assesmen_id'";
		$riwayat_penyakit=$this->db->query($q)->row('keluhan_utama');
		$diagnosa_akhir=$this->db->query($q)->row('diagnosa_utama');
		$prognis=$this->db->query($q)->row('prognis');
		$tindakan='';

		$data['riwayat_penyakit']=$riwayat_penyakit;
		// $data['tindakan']=$tindakan;
		$data['prognis']=$prognis;
		$q="SELECT MD.nama FROM tranap_ringkasan_pulang_diagnosa H
		LEFT JOIN mdiagnosa_medis MD ON MD.id=H.mdiagnosa_id 
		WHERE H.assesmen_id='$assesmen_id'";
		$list_diagnosa=$this->db->query($q)->result();
		if ($diagnosa_akhir){
			$diagnosa_akhir='<li>'.$diagnosa_akhir.'</i>';
		}
		foreach ($list_diagnosa as $r){
			$diagnosa_akhir .='<li>'.$r->nama.'</i>';
		}
		if ($diagnosa_akhir){
			$diagnosa_akhir='<ul>'.$diagnosa_akhir.'</ul>';
		}
		$q="SELECT MD.nama FROM tranap_ringkasan_pulang_tindakan H
		LEFT JOIN mtindakan_medis MD ON MD.id=H.mtindakan_id
		WHERE H.assesmen_id='$assesmen_id'";
		$list_tindakan=$this->db->query($q)->result();
		
		foreach ($list_tindakan as $r){
			$tindakan .='<li>'.$r->nama.'</i>';
		}
		if ($tindakan){
			$tindakan='<ul>'.$tindakan.'</ul>';
		}
		
		
		$data['diagnosa_akhir']=$diagnosa_akhir;
		$data['tindakan']=$tindakan;
		$this->output->set_output(json_encode($data));
	}
	function generate_penunjang_diagnosa(){
		$st_ranap=$this->input->post('st_ranap');
		$pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$var_get=$this->input->post('var_get');
		
		if ($st_ranap=='1'){
			$where=" AND H.asal_rujukan='3'";
		}else{
			$where=" AND H.asal_rujukan !='3'";
			
		}
			$q="SELECT GROUP_CONCAT(D.namapemeriksaan SEPARATOR ', ') as hasil  FROM `term_radiologi_xray` H 
				LEFT JOIN term_radiologi_xray_pemeriksaan D ON D.transaksi_id=H.id
				WHERE H.pendaftaran_id='$pendaftaran_id_ranap' ".$where." AND H.status_pemeriksaan > 1";
			$hasil_xray=$this->db->query($q)->row('hasil');
			if ($hasil_xray){
				$hasil_xray='<li><strong>X-Ray :</strong> '.$hasil_xray.'</li>';
			}
			$q="
				SELECT GROUP_CONCAT(REPLACE(H.namatarif,'└─','') SEPARATOR ', ') as hasil FROM  (
					SELECT * FROM (
						SELECT H.pendaftaran_id, D.idlaboratorium,D.namatarif,D.statusrincianpaket,D.st_paket
						FROM `term_laboratorium_umum` H 
						LEFT JOIN term_laboratorium_umum_pemeriksaan D ON D.transaksi_id=H.id 
						LEFT JOIN mtarif_laboratorium M ON M.id=D.idlaboratorium
						WHERE H.pendaftaran_id='$pendaftaran_id_ranap' ".$where." AND H.status_pemeriksaan > 1 AND D.totalkeseluruhan>0  AND M.idkelompok='0'
					) H GROUP BY H.idlaboratorium				
				) H GROUP BY H.pendaftaran_id
			";
			$hasil_lu=$this->db->query($q)->row('hasil');
			if ($hasil_lu){
				$hasil_lu='<li><strong>Laboratorium :</strong> '.$hasil_lu.'</li>';
			}
			$q="SELECT GROUP_CONCAT(REPLACE(D.namatarif,'└─','') SEPARATOR ', ') as hasil  FROM `term_laboratorium_patologi_anatomi` H 
				LEFT JOIN term_laboratorium_patologi_anatomi_pemeriksaan D ON D.transaksi_id=H.id
				WHERE H.pendaftaran_id='$pendaftaran_id_ranap' ".$where." AND H.status_pemeriksaan > 1 AND D.totalkeseluruhan>0";
			$hasil_lpa=$this->db->query($q)->row('hasil');
			if ($hasil_lpa){
				$hasil_lpa='<li><strong>Patologi Anatomi :</strong> '.$hasil_lpa.'</li>';
			}
			$q="SELECT GROUP_CONCAT(REPLACE(D.namatarif,'└─','') SEPARATOR ', ') as hasil  FROM `term_laboratorium_bank_darah` H 
				LEFT JOIN term_laboratorium_bank_darah_pemeriksaan D ON D.transaksi_id=H.id
				WHERE H.pendaftaran_id='$pendaftaran_id_ranap' ".$where." AND H.status_pemeriksaan > 1 AND D.totalkeseluruhan>0";
			$hasil_lba=$this->db->query($q)->row('hasil');
			if ($hasil_lba){
				$hasil_lba='<li><strong>Bank Darah :</strong> '.$hasil_lba.'</li>';
			}
			$q="SELECT GROUP_CONCAT(REPLACE(D.namatarif,'└─','') SEPARATOR ', ') as hasil  FROM `term_radiologi_usg` H 
				LEFT JOIN term_radiologi_usg_pemeriksaan D ON D.transaksi_id=H.id
				WHERE H.pendaftaran_id='$pendaftaran_id_ranap' ".$where." AND H.status_pemeriksaan > 1 AND D.totalkeseluruhan>0";
			$hasil_lrad=$this->db->query($q)->row('hasil');
			if ($hasil_lrad){
				$hasil_lrad='<li><strong>USG :</strong> '.$hasil_lrad.'</li>';
			}
			$q="SELECT GROUP_CONCAT(REPLACE(D.namatarif,'└─','') SEPARATOR ', ') as hasil  FROM `term_radiologi_mri` H 
				LEFT JOIN term_radiologi_mri_pemeriksaan D ON D.transaksi_id=H.id
				WHERE H.pendaftaran_id='$pendaftaran_id_ranap' ".$where." AND H.status_pemeriksaan > 1 AND D.totalkeseluruhan>0";
			$hasil_lmri=$this->db->query($q)->row('hasil');
			if ($hasil_lmri){
				$hasil_lmri='<li><strong>MRI :</strong> '.$hasil_lmri.'</li>';
			}
			$q="SELECT GROUP_CONCAT(REPLACE(D.namatarif,'└─','') SEPARATOR ', ') as hasil  FROM `term_radiologi_ctscan` H 
				LEFT JOIN term_radiologi_ctscan_pemeriksaan D ON D.transaksi_id=H.id
				WHERE H.pendaftaran_id='$pendaftaran_id_ranap' ".$where." AND H.status_pemeriksaan > 1 AND D.totalkeseluruhan>0";
			$hasil_lct=$this->db->query($q)->row('hasil');
			if ($hasil_lct){
				$hasil_lct='<li><strong>CT-CSAN :</strong> '.$hasil_lct.'</li>';
			}
			$q="SELECT GROUP_CONCAT(REPLACE(D.namatarif,'└─','') SEPARATOR ', ') as hasil  FROM `term_radiologi_lainnya` H 
				LEFT JOIN term_radiologi_lainnya_pemeriksaan D ON D.transaksi_id=H.id
				WHERE H.pendaftaran_id='$pendaftaran_id_ranap' ".$where." AND H.status_pemeriksaan > 1 AND D.totalkeseluruhan > 0";
			$hasil_llain=$this->db->query($q)->row('hasil');
			if ($hasil_llain){
				$hasil_llain='<li><strong>LAINNYA :</strong> '.$hasil_llain.'</li>';
			}
			
			if ($var_get=='all'){
				$hasil=$hasil_lu.$hasil_lpa.$hasil_lba.$hasil_xray.$hasil_lrad.$hasil_lmri.$hasil_lct.$hasil_llain;
			}
			if ($var_get=='Laboratorium'){
				$hasil=$hasil_lu;
			}
			if ($var_get=='X-Ray'){
				$hasil=$hasil_xray;
			}
			if ($var_get=='Patologi Anatomi'){
				$hasil=$hasil_lpa;
			}
			if ($var_get=='Bank Darah'){
				$hasil=$hasil_lba;
			}
			if ($var_get=='USG'){
				$hasil=$hasil_lrad;
			}
			if ($var_get=='MRI'){
				$hasil=$hasil_lmri;
			}
			if ($var_get=='CT-CSAN'){
				$hasil=$hasil_lct;
			}
			if ($var_get=='LAINNYA'){
				$hasil=$hasil_llain;
			}
			
			if ($hasil){
				$hasil='<ul>'.$hasil.'</ul>';
			}
			$data['hasil']=$hasil;
			$this->output->set_output(json_encode($data));
	}
	function generate_penunjang_satuan_diagnosa(){
		$trx_id=$this->input->post('trx_id');
		$var_get=$this->input->post('var_get');
		$where='';
		
			$q="SELECT GROUP_CONCAT(D.namapemeriksaan SEPARATOR ', ') as hasil  FROM `term_radiologi_xray` H 
				LEFT JOIN term_radiologi_xray_pemeriksaan D ON D.transaksi_id=H.id
				WHERE H.id='$trx_id' ".$where." AND H.status_pemeriksaan > 1";
			$hasil_xray=$this->db->query($q)->row('hasil');
			if ($hasil_xray){
				$hasil_xray='<li><strong>X-Ray :</strong> '.$hasil_xray.'</li>';
			}
			$q="
				SELECT GROUP_CONCAT(REPLACE(H.namatarif,'└─','') SEPARATOR ', ') as hasil FROM  (
					SELECT * FROM (
						SELECT H.pendaftaran_id, D.idlaboratorium,D.namatarif,D.statusrincianpaket,D.st_paket
						FROM `term_laboratorium_umum` H 
						LEFT JOIN term_laboratorium_umum_pemeriksaan D ON D.transaksi_id=H.id 
						LEFT JOIN mtarif_laboratorium M ON M.id=D.idlaboratorium
						WHERE H.id='$trx_id' ".$where." AND H.status_pemeriksaan > 1 AND D.totalkeseluruhan>0  AND M.idkelompok='0'
					) H GROUP BY H.idlaboratorium				
				) H GROUP BY H.pendaftaran_id
			";
			$hasil_lu=$this->db->query($q)->row('hasil');
			if ($hasil_lu){
				$hasil_lu='<li><strong>Laboratorium :</strong> '.$hasil_lu.'</li>';
			}
			$q="SELECT GROUP_CONCAT(REPLACE(D.namatarif,'└─','') SEPARATOR ', ') as hasil  FROM `term_laboratorium_patologi_anatomi` H 
				LEFT JOIN term_laboratorium_patologi_anatomi_pemeriksaan D ON D.transaksi_id=H.id
				WHERE H.id='$trx_id' ".$where." AND H.status_pemeriksaan > 1 AND D.totalkeseluruhan>0";
			$hasil_lpa=$this->db->query($q)->row('hasil');
			if ($hasil_lpa){
				$hasil_lpa='<li><strong>Patologi Anatomi :</strong> '.$hasil_lpa.'</li>';
			}
			$q="SELECT GROUP_CONCAT(REPLACE(D.namatarif,'└─','') SEPARATOR ', ') as hasil  FROM `term_laboratorium_bank_darah` H 
				LEFT JOIN term_laboratorium_bank_darah_pemeriksaan D ON D.transaksi_id=H.id
				WHERE H.id='$trx_id' ".$where." AND H.status_pemeriksaan > 1 AND D.totalkeseluruhan>0";
			$hasil_lba=$this->db->query($q)->row('hasil');
			if ($hasil_lba){
				$hasil_lba='<li><strong>Bank Darah :</strong> '.$hasil_lba.'</li>';
			}
			$q="SELECT GROUP_CONCAT(REPLACE(D.namatarif,'└─','') SEPARATOR ', ') as hasil  FROM `term_radiologi_usg` H 
				LEFT JOIN term_radiologi_usg_pemeriksaan D ON D.transaksi_id=H.id
				WHERE H.id='$trx_id' ".$where." AND H.status_pemeriksaan > 1 AND D.totalkeseluruhan>0";
			$hasil_lrad=$this->db->query($q)->row('hasil');
			if ($hasil_lrad){
				$hasil_lrad='<li><strong>USG :</strong> '.$hasil_lrad.'</li>';
			}
			$q="SELECT GROUP_CONCAT(REPLACE(D.namatarif,'└─','') SEPARATOR ', ') as hasil  FROM `term_radiologi_mri` H 
				LEFT JOIN term_radiologi_mri_pemeriksaan D ON D.transaksi_id=H.id
				WHERE H.id='$trx_id' ".$where." AND H.status_pemeriksaan > 1 AND D.totalkeseluruhan>0";
			$hasil_lmri=$this->db->query($q)->row('hasil');
			if ($hasil_lmri){
				$hasil_lmri='<li><strong>MRI :</strong> '.$hasil_lmri.'</li>';
			}
			$q="SELECT GROUP_CONCAT(REPLACE(D.namatarif,'└─','') SEPARATOR ', ') as hasil  FROM `term_radiologi_ctscan` H 
				LEFT JOIN term_radiologi_ctscan_pemeriksaan D ON D.transaksi_id=H.id
				WHERE H.id='$trx_id' ".$where." AND H.status_pemeriksaan > 1 AND D.totalkeseluruhan>0";
			$hasil_lct=$this->db->query($q)->row('hasil');
			if ($hasil_lct){
				$hasil_lct='<li><strong>CT-CSAN :</strong> '.$hasil_lct.'</li>';
			}
			$q="SELECT GROUP_CONCAT(REPLACE(D.namatarif,'└─','') SEPARATOR ', ') as hasil  FROM `term_radiologi_lainnya` H 
				LEFT JOIN term_radiologi_lainnya_pemeriksaan D ON D.transaksi_id=H.id
				WHERE H.id='$trx_id' ".$where." AND H.status_pemeriksaan > 1 AND D.totalkeseluruhan > 0";
			$hasil_llain=$this->db->query($q)->row('hasil');
			if ($hasil_llain){
				$hasil_llain='<li><strong>LAINNYA :</strong> '.$hasil_llain.'</li>';
			}
			
			if ($var_get=='all'){
				$hasil=$hasil_lu.$hasil_lpa.$hasil_lba.$hasil_xray.$hasil_lrad.$hasil_lmri.$hasil_lct.$hasil_llain;
			}
			if ($var_get=='Laboratorium'){
				$hasil=$hasil_lu;
			}
			if ($var_get=='X-Ray'){
				$hasil=$hasil_xray;
			}
			if ($var_get=='Patologi Anatomi'){
				$hasil=$hasil_lpa;
			}
			if ($var_get=='Bank Darah'){
				$hasil=$hasil_lba;
			}
			if ($var_get=='USG'){
				$hasil=$hasil_lrad;
			}
			if ($var_get=='MRI'){
				$hasil=$hasil_lmri;
			}
			if ($var_get=='CT-CSAN'){
				$hasil=$hasil_lct;
			}
			if ($var_get=='LAINNYA'){
				$hasil=$hasil_llain;
			}
			
			if ($hasil){
				$hasil='<ul>'.$hasil.'</ul>';
			}
			$data['hasil']=$hasil;
			$this->output->set_output(json_encode($data));
	}
	function generate_terapi_diagnosa(){
		$trx_id=$this->input->post('trx_id');
		$where='';
		$hasil='';
			$q="SELECT GROUP_CONCAT('<li>',T.nama,' ',COALESCE(T.nama_interval,''),'</li>' SEPARATOR '') as hasil FROM (
					SELECT O.idbarang,MO.nama,MI.nama as nama_interval 
					FROM tpoliklinik_e_resep H 
					LEFT JOIN tpoliklinik_e_resep_obat O ON O.idtipe='3' AND O.assesmen_id=H.assesmen_id
					LEFT JOIN mdata_obat MO ON MO.id=O.idbarang
					LEFT JOIN minterval MI ON MI.id=O.`interval`
					WHERE H.assesmen_id='$trx_id' 
					GROUP BY O.idbarang
					) T 
					";
			$hasil=$this->db->query($q)->row('hasil');
			if ($hasil){
				$hasil='<ul>'.$hasil.'</ul>';
			}
			$data['hasil']=$hasil;
			$this->output->set_output(json_encode($data));
	}
	function generate_tindakan_diagnosa(){
		$trx_id=$this->input->post('trx_id');
		$where='';
		$hasil='';
			$q="
			SELECT H.tindakan FROM `tranap_lap_bedah` H
			WHERE H.assesmen_id='$trx_id'
					";
			$hasil=$this->db->query($q)->row('tindakan');
			if ($hasil){
				$hasil='<ul><li>'.$hasil.'</li></ul>';
			}
			$data['hasil']=$hasil;
			$this->output->set_output(json_encode($data));
	}
	function getIndexPoliCari()	{
		$data_user=get_acces();
		$where='';
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$notransaksi =$this->input->post('notransaksi');
		$idpasien =$this->input->post('idpasien');
		$st_ranap =$this->input->post('st_ranap');
		$pendaftaran_id =$this->input->post('pendaftaran_id');
		$pendaftaran_id_ranap =$this->input->post('pendaftaran_id_ranap');
		if ($tanggal_1!=''){
			$where .=" AND ((H.tanggal >= '".YMDFormat($tanggal_1)."' AND H.tanggal <= '".YMDFormat($tanggal_2)."') OR (DATE(H.tanggaldaftar) >= '".YMDFormat($tanggal_1)."' AND DATE(H.tanggaldaftar) <= '".YMDFormat($tanggal_2)."'))";
		}
		
		if ($notransaksi!=''){
			$where .=" AND (H.nopendaftaran) LIKE '%".$notransaksi."%'";
		}
		
		// print_r($tab);exit;
		$this->select = array();
		if ($st_ranap=='0'){
			
		
			$from="
				(
					SELECT H.id,H.tanggal as tanggal_1,null as tanggal_2,H.nopendaftaran,MD.nama as nama_dokter
					,H.namapasien,H.no_medrec

					FROM `tpoliklinik_pendaftaran` H
					LEFT JOIN mdokter MD ON MD.id=H.iddokter
					WHERE H.idpasien='$idpasien' AND H.`status` !='0' ".$where."
					ORDER BY H.tanggal DESC
				) as tbl
			";
			
		}else{
			$from="
				(
					SELECT H.id,DATE(H.tanggaldaftar) as tanggal_1,CASE WHEN H.statuscheckout='1' THEN DATE(H.tanggalcheckout) ELSE '0' END as tanggal_2,H.nopendaftaran,MD.nama as nama_dokter
					,H.namapasien,H.no_medrec

					FROM `trawatinap_pendaftaran` H
					LEFT JOIN mdokter MD ON MD.id=H.iddokterpenanggungjawab
					WHERE H.idpasien='$idpasien' AND H.`status` !='0' ".$where."
					ORDER BY H.tanggaldaftar DESC
				) as tbl
			";
		}
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array();
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$tgl_1="'".$r->tanggal_1."'";
			$tgl_2=($r->tanggal_2?"'".$r->tanggal_2."'":'0');
			$no++;
			$aksi='';
			if ($st_ranap=='1'){
				if ($pendaftaran_id_ranap==$r->id){
					$aksi .= text_danger('SEDANG DIGUNAKAN');	
				}else{
					$aksi .= '<button onclick="pilih_data_pendaftaran('.$tgl_1.','.$tgl_2.')" type="button" title="Pilih" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Pilih</button>';	
				}
				
			}else{
				if ($pendaftaran_id==$r->id){
					$aksi .= text_danger('SEDANG DIGUNAKAN');	
				}else{
					$aksi .= '<button onclick="pilih_data_pendaftaran('.$tgl_1.',0)" type="button" title="Pilih" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Pilih</button>';	
				}
			}
			$result = array();
			$result[] =$no;
			$result[] =$aksi;
			$result[] =$r->nopendaftaran;
			$result[] =tanggal_indo_DMY($r->tanggal_1).($r->tanggal_2?' s.d '.tanggal_indo_DMY($r->tanggal_2):'');
			$result[] =$r->no_medrec;
			$result[] =$r->namapasien;
			$result[] =$r->nama_dokter;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
  }
  function create_assesmen_surat_skdp(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');
	  
	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
			$dpjp=$login_pegawai_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
		  $dpjp=$iddokter;
	  }
	  if ($st_ranap=='1'){
		  $q="SELECT DATE(H.tanggaldaftar) as tanggal_pelayanan_awal
			,CASE WHEN H.statuscheckout='1' THEN DATE(H.tanggalcheckout) ELSE null END as tanggal_pelayanan_akhir 
			FROM `trawatinap_pendaftaran` H
			WHERE H.id='$pendaftaran_id_ranap'";
		  $data_tanggal=$this->db->query($q)->row();
		  
	  }else{
		  $q="
			SELECT DATE(H.tanggal) as tanggal_pelayanan_awal
			,null as tanggal_pelayanan_akhir 
			FROM `tpoliklinik_pendaftaran` H
			WHERE H.id='$pendaftaran_id'
		  ";
		  $data_tanggal=$this->db->query($q)->row();
	  }
	  $data=array(
		'tanggal_input' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'idtipe' => $idtipe,
		'dpjp' => $dpjp,
		'created_date' => date('Y-m-d H:i:s'),
		'created_ppa' => $login_ppa_id,
		'tanggal_pelayanan_awal' => $data_tanggal->tanggal_pelayanan_awal,
		'tanggal_pelayanan_akhir' => $data_tanggal->tanggal_pelayanan_akhir,
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tsurat_skdp',$data);
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	function simpan_assesmen_surat_skdp(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$data=array(
			'tanggal_input' => $tanggal_input,
			'status_assemen' =>  $this->input->post('status_assemen'),
			'dpjp' => $this->input->post('dpjp'),
			'ket_lainnya' => $this->input->post('ket_lainnya'),
		);
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			// $jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tsurat_skdp_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tsurat_skdp',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function ganti_tanggal_skdp(){
		$assesmen_id=$this->input->post('assesmen_id');
		$tanggal_1=$this->input->post('tanggal_1');
		$tanggal_2=$this->input->post('tanggal_2');
		
		$data=array(
			'tanggal_pelayanan_awal' => YMDFormat($tanggal_1),
			'tanggal_pelayanan_akhir' => ($tanggal_2=='0'?null:YMDFormat($tanggal_2)),
			
		);
		// print_r($data);exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tsurat_skdp',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function batal_assesmen_surat_skdp(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	 
	 
	  $data['status_assemen']='0';
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tsurat_skdp',$data);
	  $this->output->set_output(json_encode($hasil));
	}
	function list_surat_skdp(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_skdp H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' AND H.created_ppa='$login_ppa_id' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		$jenis_surat='9';
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_skdp/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_skdp/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
			$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
			$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2562'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2564'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2565'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = $r->nama_dpjp;
		$result[] = HumanDateShort($r->tanggal_pelayanan_awal).($r->tanggal_pelayanan_akhir ? ' s/d '.($r->tanggal_pelayanan_akhir):'');
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  function list_surat_skdp_history(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$dpjp=$this->input->post('dpjp');
		$poli=$this->input->post('poli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR H.nopermintaan LIKE '%".$notransaksi."%')";
		}
		
		if ($dpjp!='#'){
			$where .=" AND H.dpjp = '".$dpjp."'";
		}
		if ($poli!='#'){
			$where .=" AND H.idpoliklinik = '".$poli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.created_date) as selisih,
					MD.nama as nama_dpjp,H.*
					,CASE WHEN H.st_ranap='1' THEN MR.nopendaftaran ELSE MP.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MRU.nama ELSE M.nama END as nama_poli
					,UC.nama as user_created,UE.nama as nama_edit,MP.tanggaldaftar
					FROM tsurat_skdp H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					LEFT JOIN mpoliklinik M ON M.id=MP.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
					
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dpjp','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		
	foreach ($list as $r) {	
		$jenis_surat='9';
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$no++;
			$aksi='';
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			if ($r->st_ranap=='1'){
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id_ranap.'/'.$r->st_ranap.'/erm_surat/input_surat_skdp/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}else{
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tsurat/tindakan/'.$r->pendaftaran_id.'/'.$r->st_ranap.'/erm_surat/input_surat_skdp/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
				
			}
			if ($r->nopermintaan){
				$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
				$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';
				
			}else{
				$btn_email='';
				$btn_email_his='';
			}
			$btn_cetak='<a class="btn btn-success  btn-xs" target="_blank" href="'.site_url().'tsurat/show_cetak/'.$r->assesmen_id.'/'.$jenis_surat.'/0"  type="button" title="Cetak " type="button"><i class="fa fa-print"></i></a>';
			$btn_cetak_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Cetak" onclick="show_cetak_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-user text-danger"></i></button>';
			$aksi=$btn_lihat;
			if (UserAccesForm($user_acces_form,array('2562'))){
				$aksi .=$btn_email;
				$aksi .=$btn_email_his;
			}
			if (UserAccesForm($user_acces_form,array('2564'))){
				$aksi .=$btn_cetak;
			}
			if (UserAccesForm($user_acces_form,array('2565'))){
				$btn_cetak_his=$btn_cetak_his;
			}else{
				$btn_cetak_his='';
			}
			
			$aksi_edit='';
			$btn_verifikasi='';
			$ket_edit='';
			
		// .$aksi_edit.$ket_edit;
		$result[] = ($r->nopendaftaran).'<br>'.HumanDateLong($r->tanggaldaftar);
		$result[] = '<span>'.($r->nama_pasien).'<br>'.$r->nomedrec_pasien.'</span>';
		$result[] = ($r->nopermintaan).'<br>'.HumanDateLong($r->tanggal_input);
		$result[] = ($r->nama_poli);
		$result[] = $r->nama_dpjp;
		$result[] = HumanDateShort($r->tanggal_pelayanan_awal).($r->tanggal_pelayanan_akhir ? ' s/d '.($r->tanggal_pelayanan_akhir):'');
		$result[] =($r->jumlah_cetak>0?text_default($r->jumlah_cetak.' x Cetak'):text_danger('BELUM CETAK')).$btn_cetak_his;
		$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
	public function cetak_surat_skdp($assesmen_id,$st_create='0'){
		// use Dompdf\Options;
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);
		$data=array();
		$q="SELECT 
			nama_form,logo_form,alamat_form,telepone_form,email_form,kota_form,header_color,footer_color

			FROM mheader_surat ";
		$data_header=$this->db->query($q)->row_array();
		$q="SELECT 
			*
			FROM msurat_template,msurat_template_var_9 WHERE jenis_surat='9' LIMIT 1";
		$data_format=$this->db->query($q)->row_array();
		// print_r($data_format);exit;
		$q="SELECT H.st_ranap FROM tsurat_skdp H
			WHERE H.assesmen_id='$assesmen_id'";
		$st_ranap=$this->db->query($q)->row('st_ranap');
		$data_assesmen=$this->Tsurat_model->get_data_surat_skdp_cetak($assesmen_id,$st_ranap);
        $data_assesmen['title']=$data_format['judul_ina'] .' '.$data_assesmen['nopermintaan'];
        // $data_assesmen['judul_surat']='';
		// print_r($data);exit;
		$data = array_merge($data_assesmen,$data_format,$data_header, backend_info());
		
        $html = $this->load->view('Tsurat/pdf_surat_skdp', $data,true);
		// print_r($html);exit;
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
		if ($st_create=='1'){
			$nama_file=$data_assesmen['title'].'.pdf';
			$uploadDir_asset = './assets/upload/lap_bedah/';
			$output = $dompdf->output();
			file_put_contents($uploadDir_asset.$nama_file, $output);     
			$this->output->set_content_type($html);
		}else{
        $dompdf->stream($data['title'].'.pdf', array("Attachment"=>0));
			
		}
	}
}	
