<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mlokasi_ruangan extends CI_Controller
{
    /**
     * Lokasi Ruangan controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mlokasi_ruangan_model');
    }

    public function index(): void
    {
        $data = [];
        $data['error'] = '';
        $data['title'] = 'Lokasi Ruangan';
        $data['content'] = 'Mlokasi_ruangan/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Lokasi Ruangan', '#'],
            ['List', 'mlokasi_ruangan'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create(): void
    {
        $data = [
            'id' => '',
            'kode' => '',
            'nama' => '',
            'deskripsi' => '',
            'ruangan_id' => '',
            'poliklinik_id' => '',
            'gedung_id' => '',
            'lantai_id' => '',
            'status' => '',
        ];

        $data['error'] = '';
        $data['title'] = 'Tambah Lokasi Ruangan';
        $data['content'] = 'Mlokasi_ruangan/manage';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Lokasi Ruangan', '#'],
            ['Tambah', 'mlokasi_ruangan'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id): void
    {
        if ('' !== $id) {
            $row = $this->Mlokasi_ruangan_model->getSpecified($id);
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'kode' => $row->kode,
                    'nama' => $row->nama,
                    'deskripsi' => $row->deskripsi,
                    'ruangan_id' => $row->ruangan_id,
                    'poliklinik_id' => $row->poliklinik_id,
                    'gedung_id' => $row->gedung_id,
                    'lantai_id' => $row->lantai_id,
                    'status' => $row->status,
                ];

                $data['error'] = '';
                $data['title'] = 'Ubah Lokasi Ruangan';
                $data['content'] = 'Mlokasi_ruangan/manage';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Lokasi Ruangan', '#'],
                    ['Ubah', 'mlokasi_ruangan'],
                ];

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mlokasi_ruangan', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mlokasi_ruangan');
        }
    }

    public function delete($id): void
    {
        $this->Mlokasi_ruangan_model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mlokasi_ruangan', 'location');
    }

    public function save(): void
    {
        $this->form_validation->set_rules('kode', 'Kode', 'trim|required');
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');

        if (true === $this->form_validation->run()) {
            if ('' === $this->input->post('id')) {
                if ($this->Mlokasi_ruangan_model->saveData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mlokasi_ruangan', 'location');
                }
            } else {
                if ($this->Mlokasi_ruangan_model->updateData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mlokasi_ruangan', 'location');
                }
            }
        } else {
            $this->failed_save($this->input->post('id'));
        }
    }

    public function failed_save($id): void
    {
        $data = $this->input->post();
        $data['error'] = validation_errors();
        $data['content'] = 'Mlokasi_ruangan/manage';

        if ('' === $id) {
            $data['title'] = 'Tambah Lokasi Ruangan';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Lokasi Ruangan', '#'],
                ['Tambah', 'mlokasi_ruangan'],
            ];
        } else {
            $data['title'] = 'Ubah Lokasi Ruangan';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Lokasi Ruangan', '#'],
                ['Ubah', 'mlokasi_ruangan'],
            ];
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function getIndex(): void
    {
        $data_user = get_acces();
        $user_acces_form = $data_user['user_acces_form'];

        $this->select = [];
        $this->from = 'mlokasi_ruangan';
        $this->join = [];
        $this->where = [
            'status' => '1',
        ];
        $this->order = [
            'id' => 'DESC',
        ];
        $this->group = [];

        $this->column_search = ['nama'];
        $this->column_order = ['nama'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = $r->kode;
            $row[] = $r->nama;
            $row[] = $r->deskripsi;
            $aksi = '<div class="btn-group">';
            $aksi .= '<a href="'.site_url().'mlokasi_ruangan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
            $aksi .= '<a href="#" data-urlindex="'.site_url().'mlokasi_ruangan" data-urlremove="'.site_url().'mlokasi_ruangan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }
}
