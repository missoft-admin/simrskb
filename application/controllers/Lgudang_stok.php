<?php
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

defined('BASEPATH') or exit('No direct script access allowed');

class Lgudang_stok extends CI_Controller
{

    /**
     * Laporan Stok Gudang controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Lgudang_stok_model');
        $this->load->model('munitpelayanan_model', 'unitpelayanan');
        $this->load->model('Tstockopname_model', 'Tstockopname_model');
    }

    public function index()
    {
        $data = array(
            'idunitpelayanan' => '#',
            'idtipe' => '#',
            'idkategori' => '#',
            'statusstok' => '#',
        );

        $data['error'] 			= '';
        $data['title'] 			= 'Laporan Stok Gudang';
        $data['content'] 		= 'Lgudang_stok/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Laporan Stok Gudang",'lgudang_stok/index'),
            array("List",'#')
        );

		$data['list_unitpelayanan'] = $this->unitpelayanan->getDefaultUnitPelayananUser();
		$data['idunitpelayanan'] = $this->Lgudang_stok_model->get_default();
		$data['list_tipe'] = $this->Tstockopname_model->list_tipe($data['idunitpelayanan']);
		// print_r($data['list_tipe']);exit();

		// $array_idtipe=$this->Tstockopname_model->list_tipe2('1');
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
    public function kartustok($idtipe, $idbarang, $idunit='0')
    {
		$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d'));
		date_add($date1,date_interval_create_from_date_string("-30 days"));
		date_add($date2,date_interval_create_from_date_string("0 days"));
		$date1= date_format($date1,"Y-m-d");
		$date2= date_format($date2,"Y-m-d");

        $data = $this->Lgudang_stok_model->get_info($idtipe, $idbarang, $idunit);
        $data['list_jenis'] = $this->Lgudang_stok_model->list_jenis();
        $data['jenis_trx'] 			= '#';
        $data['tanggaldari'] 			= HumanDateShort($date1);
        $data['tanggalsampai'] 			= HumanDateShort($date2);
        $data['idtipe'] 			= $idtipe;
        $data['idbarang'] 			= $idbarang;
        $data['idunit'] 			= $idunit;
        $data['error'] 			= '';
        $data['title'] 			= 'Kartu Stok';
        $data['content'] 		= 'Lgudang_stok/kartustok';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Laporan Stok Gudang",'lgudang_stok/index'),
            array("List",'#')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function getKS()
    {
        $iddariunit     = $this->input->post('idunit');
        $idbarang     		= $this->input->post('idbarang');
        $idtipe     	= $this->input->post('idtipe');
        $tanggaldari     	= $this->input->post('tanggaldari');
        $tanggalsampai    	= $this->input->post('tanggalsampai');
        $jenis_trx    	= $this->input->post('jenis_trx');
        $iduser=$this->session->userdata('user_id');

        // $data_user=get_acces();
        // $user_acces_form=$data_user['user_acces_form'];
        $this->select = array();
        $this->join 	= array();
        $this->where  = array();
        $where='';
        if ($jenis_trx != "#") {
            $where .= " AND K.tipetransaksi='$jenis_trx'";
        }
        if ('' != $tanggaldari) {
            $where .= " AND (DATE(K.tanggal) >='".YMDFormat($tanggaldari)."'";
        }
        if ('' != $tanggalsampai) {
            $where .= " AND DATE(K.tanggal) <='".YMDFormat($tanggalsampai)."')";
        }
        $from="(SELECT K.tanggal,K.user,K.notransaksi,R.nama_trx,K.keterangan,B.namatipe,B.nama,K.penerimaan,K.pengeluaran,K.stok,K.tujuan,K.link
					FROM lkartustok K
					INNER JOIN view_barang B ON B.id=K.idbarang AND B.idtipe=K.idtipe
					LEFT JOIN munitpelayanan Unit ON Unit.id=K.tujuan
					LEFT JOIN lkartustok_ref R ON R.id=K.tipetransaksi
					WHERE K.idunitpelayanan='$iddariunit' AND K.idtipe='$idtipe' AND K.idbarang='$idbarang' ".$where." ORDER BY K.id DESC) as tbl";

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        // print_r($from);exit();
        $this->column_search   = array('notransaksi','keterangan');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();

        $no = $_POST['start'];
        foreach ($list as $r) {
            $link='javascript:void(0)';
            $target='';
            if ($r->link !='' && $r->link !='#') {
                $link=site_url().$r->link;
                $target='target="_blank"';
            }
            $no++;
            $row = array();
            $row[] =HumanDateLong($r->tanggal);
            $row[] = $r->user;
            $row[] = $r->nama_trx;
            $row[] = $r->notransaksi;
            $row[] = $r->tujuan;
            $row[] = $r->keterangan;
            $row[] =($r->penerimaan)?number_format($r->penerimaan, 0):'0';
            $row[] =($r->pengeluaran)?number_format($r->pengeluaran, 0):'0';
            $row[] =($r->stok)?number_format($r->stok, 0):'0';
            $aksi       = '<div class="btn-group">';
            $aksi 		.= '<a '.$target.' href="'.$link.'" class="btn btn-xs btn-primary" title="Kartu Stok"><i class="fa fa-print"></i> Cetak Bukti Transaksi</a>';
            $aksi.='</div>';
            if ($target) {
                $row[] = $aksi;
            } else {
                $row[] = '';
            }
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
    public function detail($idtipe="", $idbarang="", $mode=0)
    {
        if ($mode!=0) {
            $this->load->library("datatables");
            $this->datatables->select('lkartustok.*, munitpelayanan.nama AS namaunitpelayanan');
            $this->datatables->join('munitpelayanan', 'munitpelayanan.id = lkartustok.idunitpelayanan');
            $this->datatables->where('lkartustok.idtipe', $idtipe);
            $this->datatables->where('lkartustok.idbarang', $idbarang);
            $this->datatables->where('lkartustok.status', '1');
            $this->datatables->from('lkartustok');
            return print_r($this->datatables->generate());
        } else {
            if ($idtipe != '' && $idbarang != '') {
                $row = $this->Lgudang_stok_model->getInfoBarang($idtipe, $idbarang);
                if (isset($row->id)) {
                    $data['error'] 			= '';
                    $data['title'] 			= 'Laporan Kartu Stok';
                    $data['content'] 		= 'Lgudang_stok/kartustok';
                    $data['breadcrum'] 	= array(
                        array("RSKB Halmahera",'#'),
                        array("Laporan Kartu Stok",'lgudang_stok/detail'),
                        array("List",'#')
                    );

                    $data['kodebarang'] = $row->kode;
                    $data['namabarang'] = $row->nama;
                    $data['kartustok']  = $this->Lgudang_stok_model->getKartuStok($idtipe, $idbarang);

                    $data = array_merge($data, backend_info());
                    $this->parser->parse('module_template', $data);
                } else {
                    $this->session->set_flashdata('error', true);
                    $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                    redirect('lgudang_stok/index/0', 'location');
                }
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('lgudang_stok/index/0');
            }
        }
    }

    public function filter()
    {
        $data = array(
            'idunitpelayanan' => $this->input->post('idunitpelayanan'),
            'idtipe' => $this->input->post('idtipe'),
            'idkategori' => $this->input->post('idkategori'),
            'statusstok' => $this->input->post('statusstok'),
        );

        $this->session->set_userdata($data);

        $data['error'] 			= '';
        $data['title'] 			= 'Laporan Stok Gudang';
        $data['content'] 		= 'Lgudang_stok/index';
        $data['breadcrum'] 	= array(
                                                        array("RSKB Halmahera",'#'),
                                                        array("Laporan Stok Gudang",'lgudang_stok/index'),
                                                        array("List",'#')
                                                    );

        $data['list_unitpelayanan'] = $this->Lgudang_stok_model->getUnitPelayanan();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function getIndex($uri)
    {
        $this->select = array('mgudang_stok.*',
            '(CASE
				WHEN mgudang_stok.idtipe = 1 THEN
					mdata_alkes.kode
				WHEN mgudang_stok.idtipe = 2 THEN
					mdata_implan.kode
				WHEN mgudang_stok.idtipe = 3 THEN
					mdata_obat.kode
				WHEN mgudang_stok.idtipe = 4 THEN
					mdata_logistik.kode
				END) AS kodebarang',
            '(CASE
				WHEN mgudang_stok.idtipe = 1 THEN
					mdata_alkes.nama
				WHEN mgudang_stok.idtipe = 2 THEN
					mdata_implan.nama
				WHEN mgudang_stok.idtipe = 3 THEN
					mdata_obat.nama
				WHEN mgudang_stok.idtipe = 4 THEN
					mdata_logistik.nama
				END) AS namabarang',
            '(CASE
				WHEN mgudang_stok.idtipe = 1 THEN
					mkategori_alkes.nama
				WHEN mgudang_stok.idtipe = 2 THEN
					mkategori_implan.nama
				WHEN mgudang_stok.idtipe = 3 THEN
					mkategori_obat.nama
				WHEN mgudang_stok.idtipe = 4 THEN
					mkategori_logistik.nama
				END) AS namakategori',
            '(CASE
				WHEN mgudang_stok.idtipe = 1 THEN
					msatuan_alkes.nama
				WHEN mgudang_stok.idtipe = 2 THEN
					msatuan_implan.nama
				WHEN mgudang_stok.idtipe = 3 THEN
					msatuan_obat.nama
				WHEN mgudang_stok.idtipe = 4 THEN
					msatuan_logistik.nama
				END) AS namasatuan',
            '(CASE
				WHEN mgudang_stok.idtipe = 1 THEN
					mdata_alkes.stokreorder
				WHEN mgudang_stok.idtipe = 2 THEN
					0
				WHEN mgudang_stok.idtipe = 3 THEN
					mdata_obat.stokreorder
				WHEN mgudang_stok.idtipe = 4 THEN
					mdata_logistik.stokreorder
				END) AS stokreorder',
            '(CASE
				WHEN mgudang_stok.idtipe = 1 THEN
					mdata_alkes.stokminimum
				WHEN mgudang_stok.idtipe = 2 THEN
					0
				WHEN mgudang_stok.idtipe = 3 THEN
					mdata_obat.stokminimum
				WHEN mgudang_stok.idtipe = 4 THEN
					mdata_logistik.stokreorder
				END) AS stokminimum');
        $this->from   = 'mgudang_stok';
        $this->join   = array(
            array('munitpelayanan', 'munitpelayanan.id = mgudang_stok.idunitpelayanan', 'LEFT'),
            array('mdata_alkes', 'mdata_alkes.id = mgudang_stok.idbarang AND mgudang_stok.idtipe = 1', 'LEFT'),
            array('mdata_implan', 'mdata_implan.id = mgudang_stok.idbarang AND mgudang_stok.idtipe = 2', 'LEFT'),
            array('mdata_obat', 'mdata_obat.id = mgudang_stok.idbarang AND mgudang_stok.idtipe = 3', 'LEFT'),
            array('mdata_logistik', 'mdata_logistik.id = mgudang_stok.idbarang AND mgudang_stok.idtipe = 4', 'LEFT'),
            array('mdata_kategori mkategori_alkes', 'mkategori_alkes.idtipe = 1 AND mdata_alkes.idkategori = mkategori_alkes.id', 'LEFT'),
            array('mdata_kategori mkategori_implan', 'mkategori_implan.idtipe = 2 AND mdata_implan.idkategori = mkategori_alkes.id', 'LEFT'),
            array('mdata_kategori mkategori_obat', 'mkategori_obat.idtipe = 3 AND mdata_obat.idkategori = mkategori_obat.id', 'LEFT'),
            array('mdata_kategori mkategori_logistik', 'mkategori_logistik.idtipe = 4 AND mdata_logistik.idkategori = mkategori_logistik.id', 'LEFT'),
            array('msatuan msatuan_alkes', 'mdata_alkes.idsatuanbesar = msatuan_alkes.id', 'LEFT'),
            array('msatuan msatuan_implan', 'mdata_implan.idsatuan = msatuan_implan.id', 'LEFT'),
            array('msatuan msatuan_obat', 'mdata_obat.idsatuanbesar = msatuan_obat.id', 'LEFT'),
            array('msatuan msatuan_logistik', 'mdata_obat.idsatuanbesar = msatuan_logistik.id', 'LEFT')
        );

        // FILTER
        if ($uri == 'filter') {
            $this->where  = array();
            if ($this->session->userdata('idunitpelayanan') != "#") {
                $this->where = array_merge($this->where, array('mgudang_stok.idunitpelayanan' => $this->session->userdata('idunitpelayanan')));
            }
            if ($this->session->userdata('idtipe') != "#") {
                $this->where = array_merge($this->where, array('mgudang_stok.idtipe' => $this->session->userdata('idtipe')));
            }

            $this->where = array_merge($this->where, array('mgudang_stok.status' => '1'));
        } else {
            $this->where  = array(
                'mgudang_stok.idunitpelayanan' => '0',
                'mgudang_stok.status' => '1'
            );
        }

        $this->order  = array(
            'mgudang_stok.id' => 'DESC'
        );

        $this->group  = array();

        $this->column_search   = array('mgudang_stok.idunitpelayanan');
        $this->column_order    = array('mgudang_stok.idunitpelayanan');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];

        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->kodebarang;
            $row[] = $r->namabarang;
            $row[] = GetTipeKategoriBarang($r->idtipe);
            $row[] = $r->namakategori;
            $row[] = $r->namasatuan;
            $row[] = WarningStok($r->stok, $r->stokreorder, $r->stokminimum);
            $row[] = LabelWarningStok($r->stok, $r->stokreorder, $r->stokminimum);
            $aksi = '<div class="btn-group">';
            $aksi .= '<a href="'.site_url().'lgudang_stok/detail/'.$r->idtipe.'/'.$r->idbarang.'" data-toggle="tooltip" title="Kartu Stok" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i> Kartu Stok</a>';
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->datatable->count_all(),
                "recordsFiltered" => $this->datatable->count_all(),
                "data" => $data
            );
        echo json_encode($output);
    }

    public function getBarang()
    {
        $iddariunit     = $this->input->post('idunit');
        $id     		= $this->input->post('id');
        $idtipe     	= $this->input->post('idtipe');
        $idkategori    = $this->input->post('idkategori');
        $statusstok    = $this->input->post('statusstok');
        $order   = $this->input->post('order_by');
        $iduser=$this->session->userdata('user_id');

        $var_order='';

        $this->select = array();
        $this->join 	= array();
        $this->where  = array();
        $where=" WHERE B.nama !=''";
        $where_stok='';
        $group_by='';
        $order_by=' ORDER BY namatipe,namabarang ASC';
        if ($order  != null) {
            $var_order=implode(',', $order);
            $order_by=" ORDER BY ".$var_order." ASC";
        }

        if ($iddariunit  != '#') {
            $where.=" AND S.idunitpelayanan='$iddariunit'";
            $group_by=' GROUP BY  idunitpelayanan, idtipe, idbarang ';
        } else {
            $group_by=' GROUP BY  idtipe, idbarang ';
        }
        if ($idtipe  != '#') {
            $where .=" AND S.idtipe='$idtipe'";
        } else {
			$array_idtipe=$this->Tstockopname_model->list_tipe2($iddariunit);
			$where .=" AND S.idtipe IN (".$array_idtipe.")";
		}
        // }
        if ($idkategori  != '#') {
            $where .=" AND B.idkategori IN (
							SELECT id FROM (
									SELECT *FROM view_kategori_alkes
									UNION
									SELECT *FROM view_kategori_implan
									UNION
									SELECT *FROM view_kategori_obat
									UNION
									SELECT *FROM view_kategori_logistik
							) T1
							where T1.path like '$idkategori%'
			)";
        }
        if ($statusstok  != '#') {
            if ($statusstok=='1') {
                $where_stok .=" WHERE stok > stokreorder";
            }
            if ($statusstok=='2') {
                $where_stok .=" WHERE (stok <= stokreorder AND stok >= stokminimum)";
            }
            if ($statusstok=='3') {
                $where_stok .=" WHERE stok <= stokminimum";
            }
        }
        // namatipe,namabarang,namakategori
        $from="(SELECT *FROM (SELECT B.kode,B.nama as namabarang,B.namatipe,B.idkategori,K.nama as namakategori,msatuan.nama as namasatuan,S.stok,B.hargadasar as hpp,
				B.stokreorder,B.stokminimum,S.idtipe,S.idbarang,S.idunitpelayanan
				from mgudang_stok S
				INNER JOIN view_barang B ON B.idtipe=S.idtipe AND B.id=S.idbarang
				INNER JOIN mdata_kategori K ON K.id=B.idkategori
				INNER JOIN msatuan ON msatuan.id=B.idsatuan
				 ".$where."
				) TT ".$order_by.") as tbl ".$where_stok;
		// print_r($from);exit();
        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        $this->column_search   = array('namabarang','namakategori','namatipe','kode');
        $this->column_order    = array('namabarang','namakategori','namatipe','kode','stok');
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->kode;
            $row[] = $r->namabarang;
            $row[] = $r->namatipe;
            $row[] = $r->namakategori;
            $row[] = $r->namasatuan;
            $row[] = ($r->stok < 0) ? text_danger(number_format($r->stok, 0)):number_format($r->stok, 0);
            $row[] = number_format($r->hpp, 0);
            $row[] = number_format($r->hpp * $r->stok, 0);
            if ($r->stok > $r->stokreorder) {
                $row[] = text_success('Available');
            } else {
                if ($r->stok > $r->stokminimum) {
                    $row[] = text_warning('Back To Order');
                } else {
                    $row[] = text_danger('Stok Minimum');
                }
            }

            $aksi       = '<div class="btn-group">';
            $aksi 		.= '<a target="_blank" href="'.site_url().'lgudang_stok/kartustok/'.$r->idtipe.'/'.$r->idbarang.'/'.$r->idunitpelayanan.'" class="btn btn-xs btn-success ks" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';
            $aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

    public function print_kartustok($idtipe, $idbarang, $idunit)
    {
        $dompdf = new Dompdf();

        $row = $this->Lgudang_stok_model->getInfoBarang($idtipe, $idbarang);

				$data = $this->Lgudang_stok_model->get_info($idtipe, $idbarang, $idunit);
        $data['kartustok'] = $this->Lgudang_stok_model->getKartuStok($idtipe, $idbarang);

				$data = array_merge($data, backend_info());

        $html = $this->load->view('Lgudang_stok/kartustok_print', $data, true);

        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Kartu Stok.pdf', array("Attachment"=>0));
    }

    public function print_barang()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(1000);
        $iddariunit     = $this->input->post('idunitpelayanan');
        $idtipe     	= $this->input->post('idtipe');
        $idkategori    = $this->input->post('idkategori');
        $statusstok    = $this->input->post('statusstok');
        $order   = $this->input->post('order_by');
        $iduser=$this->session->userdata('user_id');
        $idbarang='1';
        if ($order) {
            $group=$order[0];
        } else {
            $group="namatipe";
        }
        $data=array();

        $dompdf = new Dompdf();
        $where=" WHERE B.nama !=''";
        $where_stok='';
        $group_by='';
        $order_by=' ORDER BY namatipe,namabarang ASC';
        if ($order  != null) {
            $var_order=implode(',', $order);
            // $order_by=" order by namatipe, namabarang, namakategori ASC";
            $order_by=" ORDER BY ".$var_order." ASC";
        }

       if ($iddariunit  != '#') {
            $where.=" AND S.idunitpelayanan='$iddariunit'";
            $group_by=' GROUP BY  idunitpelayanan, idtipe, idbarang ';
        } else {
            $group_by=' GROUP BY  idtipe, idbarang ';
        }
        if ($idtipe  != '#') {
            $where .=" AND S.idtipe='$idtipe'";
        } else {
			$array_idtipe=$this->Tstockopname_model->list_tipe2($iddariunit);
			$where .=" AND S.idtipe IN (".$array_idtipe.")";
		}
        if ($idkategori  != '#') {
            $where .=" AND B.idkategori IN (
							SELECT id FROM (
									SELECT *FROM view_kategori_alkes
									UNION
									SELECT *FROM view_kategori_implan
									UNION
									SELECT *FROM view_kategori_obat
									UNION
									SELECT *FROM view_kategori_logistik
							) T1
							where T1.path like '$idkategori%'
			)";
        }
        if ($statusstok  != '#') {
            if ($statusstok=='1') {
                $where_stok .=" WHERE stok > stokreorder";
            }
            if ($statusstok=='2') {
                $where_stok .=" WHERE (stok <= stokreorder AND stok >= stokminimum)";
            }
            if ($statusstok=='3') {
                $where_stok .=" WHERE stok <= stokminimum";
            }
        }
       

		$from="SELECT *FROM (SELECT B.kode,B.nama as namabarang,B.namatipe,B.idkategori,K.nama as namakategori,msatuan.nama as namasatuan,S.stok,B.hargadasar as hpp,
				B.stokreorder,B.stokminimum,S.idtipe,S.idbarang,S.idunitpelayanan
				from mgudang_stok S
				INNER JOIN view_barang B ON B.idtipe=S.idtipe AND B.id=S.idbarang
				INNER JOIN mdata_kategori K ON K.id=B.idkategori
				INNER JOIN msatuan ON msatuan.id=B.idsatuan
				 ".$where."
				) TT ".$where_stok." ".$order_by;
		// print_r($from);exit();
        $query=$this->db->query($from);
        $row_detail=$query->result();
		 $data['group'] = $group;
        $data['namaunit'] = $this->Lgudang_stok_model->get_namaUnit($iddariunit);
        $data['namatipe'] = $this->Lgudang_stok_model->get_namaTipe($idtipe);
        $data['namakategori'] = $this->Lgudang_stok_model->get_namaKategori($idkategori);
        $data['namastok'] = $this->Lgudang_stok_model->get_namaStok($statusstok);
        $data['nama_periode'] = get_bulan(date('m')).' '.date('Y');
		
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Stokopname');

        // Set Logo
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $logo = FCPATH.'/assets/upload/logo/logoreport.jpg';
        $objDrawing->setPath($logo);
        $objDrawing->setCoordinates('B1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setHeight(70);
        $objDrawing->setWidth(40);
        $objDrawing->setWorksheet($activeSheet);

        // Set Header
        $activeSheet->setCellValue('B1', "                                      RUMAH SAKIT KHUSUS BEDAH HALMAHERA SIAGA");
        $activeSheet->setCellValue('B2', "                                      Jl.LLRE.Martadinata no.28, Telp: +6222-4206061");
        $activeSheet->setCellValue('B3', "                                      Bandung, 40115");
        $activeSheet->mergeCells('B1:AA1');
        $activeSheet->mergeCells('B2:AA2');
        $activeSheet->mergeCells('B3:AA3');
        $activeSheet->getStyle('B1:AA1')->getFont()->setSize(11);
        $activeSheet->getStyle('B2:AA2')->getFont()->setSize(11);
        $activeSheet->getStyle('B3:AA3')->getFont()->setSize(11);

		// Set Title
		$activeSheet->setCellValue('B5', "LAPORAN STOK GUDANG ".strtoupper($data['namaunit']));
		$activeSheet->mergeCells('B5:M5');
		$activeSheet->getStyle('B5')->getFont()->setBold(true)->setSize(14);
		$activeSheet->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		// Set Periode
		$activeSheet->setCellValue('B7', "Tanggal Cetak");
		$activeSheet->setCellValue('C7', HumanDateLong(date('Y-m-d H:i:s')));

		$activeSheet->setCellValue('B8', "Unit Pelayanan");
		$activeSheet->setCellValue('C8', $data['namaunit']);

		$activeSheet->setCellValue('B9', "Kategori");
		$activeSheet->setCellValue('C9', $data['namakategori']);
		
		$activeSheet->setCellValue('B10', "User Cetak");
		$activeSheet->setCellValue('C10', $this->session->userdata('user_name'));

		$activeSheet->setCellValue('E7', "Periode Cetak");
		$activeSheet->setCellValue('F7', $data['nama_periode']);

		$activeSheet->setCellValue('E8', "Tipe Barang");
		$activeSheet->setCellValue('F8', $data['namatipe']);

		$activeSheet->setCellValue('E9', "Status Stok");
		$activeSheet->setCellValue('F9', $data['namastok']);
		
		// $activeSheet->setCellValue('E10', "Tanggal Selesai");
		// $activeSheet->setCellValue('F10', $data['tgl_finish']);
		$activeSheet->mergeCells('F7:G7');
		$activeSheet->mergeCells('F8:G8');
		$activeSheet->mergeCells('F9:G9');
		$activeSheet->mergeCells('F10:G10');
		
		// $activeSheet->mergeCells('A7:B7');
		// $activeSheet->mergeCells('A8:B8');
		// $activeSheet->mergeCells('A9:B9');
		// $activeSheet->mergeCells('A10:B10');
		// Set Header Column
		$activeSheet->setCellValue('B12', "NO");
		$activeSheet->setCellValue('C12', "NAMA BARANG");
		$activeSheet->setCellValue('D12', "TIPE");
		$activeSheet->setCellValue('E12', "SATUAN");
		$activeSheet->setCellValue('F12', "HARGA SATUAN");
		$activeSheet->setCellValue('G12', "CURRENT STOK");
		$activeSheet->setCellValue('H12', "STATUS");
		$activeSheet->setCellValue('I12', "TOTAL HARGA");
		$activeSheet->getStyle('B12:I12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("B12:I12")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
	  'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK,
					)
				),
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Calibri'
				)
			)
		);

		$x = 12;
		$sisaStok = 0;
		$jml=0;
		$jml_barang=0;
		$tot_harga=0;
		$jml_all=0;
		$jml_harga_all=0;
		if ($group=='namakategori'){
			$cari='KATEGORI';		  
		}elseif ($group=='namatipe'){
			$cari='TIPE';		  
		}else{
			$cari='';
		}
		// print_r($cari);exit();
		$cari_id="";
	    $cari_nama="";
		 $namatipe='';
		 $idtipe='';
        if (COUNT($row_detail)) {
            foreach ($row_detail as $row) {
				$x = $x+1;
				$jml = $jml+1;
				$stok=(int)$row->stok;
				$jml_barang=(int)$jml_barang+(int)$row->stok;
				$jml_all=(int)$jml_all+(int)$row->stok;
				$hpp=(int)$row->hpp;
				$tot=$stok*$hpp;
				$tot_harga=(int)$tot_harga+(int)$tot;
				$jml_harga_all=(int)$jml_harga_all+(int)$tot;
				if ($cari!=''){
					if ($cari=='TIPE'){
						$cari_id=$row->idtipe;
						$cari_nama=$row->namatipe;
					}elseif ($cari=='KATEGORI'){
						$cari_id=$row->idkategori;
						$cari_nama=$row->namakategori;
					}
						if ($idtipe==''){
							$activeSheet->setCellValue("B$x", $cari.' BARANG : '.$cari_nama);
							$activeSheet->mergeCells("B$x:I$x");
							$x = $x+1;
							$idtipe=$cari_id;
						}else{
							if ($idtipe !=$cari_id){
								$activeSheet->setCellValue("B$x",'TOTAL '.$cari.' BARANG : '.$namatipe);
								$activeSheet->mergeCells("B$x:F$x");
								$activeSheet->setCellValue("G$x",$jml_barang);
								$activeSheet->setCellValue("H$x",'');
								$activeSheet->setCellValue("I$x",$tot_harga);
								$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('#,##0');
								$activeSheet->getStyle("I$x")->getNumberFormat()->setFormatCode('#,##0');
								$idtipe=$cari_id;
								$jml=1;
								$jml_barang=0;
								$tot_harga=0;
								$x = $x+1;  
							}
						}
				 }	
						$namatipe=$cari_nama;
				// Set Content
	            $activeSheet->setCellValue("B$x", $jml);
	            $activeSheet->setCellValue("C$x", $row->namabarang);
	            $activeSheet->setCellValue("D$x", $row->namatipe);
	            $activeSheet->setCellValue("E$x", $row->namasatuan);
	            $activeSheet->setCellValue("F$x", $row->hpp);
	            $activeSheet->setCellValue("G$x", $row->stok);
				
					$status='';
					if ($row->stok > $row->stokreorder){
						$status='Available';
					}elseif (($row->stok <= $row->stokreorder) && ($row->stok > $row->stokminimum)){
						$status='Back To Order';
					}else{
						$status='Stok Minimum';
					}
				
				
	            $activeSheet->setCellValue("H$x", $status);
	            $activeSheet->setCellValue("I$x", $tot);
	           
	           
				$activeSheet->getStyle("D$x:E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("H$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('#,##0');
				$activeSheet->getStyle("I$x")->getNumberFormat()->setFormatCode('#,##0');
				
			}
		}
		$x = $x+1;
		$activeSheet->setCellValue("B$x",'TOTAL '.$cari.' BARANG : '.$namatipe);
		$activeSheet->mergeCells("B$x:F$x");
		$activeSheet->setCellValue("G$x",$jml_barang);
		$activeSheet->setCellValue("H$x",'');
		$activeSheet->setCellValue("I$x",$tot_harga);
		$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('#,##0');
		$activeSheet->getStyle("I$x")->getNumberFormat()->setFormatCode('#,##0');
		$x = $x+1;
		$activeSheet->setCellValue("B$x",'TOTAL ALL');
		$activeSheet->mergeCells("B$x:F$x");
		$activeSheet->setCellValue("G$x",$jml_all);
		$activeSheet->setCellValue("H$x",'');
		$activeSheet->setCellValue("I$x",$jml_harga_all);
	    $activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('#,##0');
		$activeSheet->getStyle("I$x")->getNumberFormat()->setFormatCode('#,##0');
		$activeSheet->getStyle("B12:I$x")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
					)
				)
			)
		);
		
    	// Set Auto Width Column
    	$activeSheet->getColumnDimension("A")->setWidth(5);
    	$activeSheet->getColumnDimension("B")->setAutoSize(true);
    	$activeSheet->getColumnDimension("C")->setAutoSize(true);
    	$activeSheet->getColumnDimension("D")->setAutoSize(true);
    	$activeSheet->getColumnDimension("E")->setAutoSize(true);
    	$activeSheet->getColumnDimension("F")->setAutoSize(true);
    	$activeSheet->getColumnDimension("G")->setAutoSize(true);
    	$activeSheet->getColumnDimension("H")->setAutoSize(true);
    	$activeSheet->getColumnDimension("I")->setAutoSize(true);
    	$activeSheet->getColumnDimension("J")->setAutoSize(true);
    	$activeSheet->getColumnDimension("K")->setAutoSize(true);
    	$activeSheet->getColumnDimension("L")->setAutoSize(true);
    	$activeSheet->getColumnDimension("M")->setAutoSize(true);

    	header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    	header('Content-Disposition: attachment;filename="'.$data['namaunit'].' : Laporan Stok Barang '.date('Y/m/d H:i:s').'.xls"');
    	header('Cache-Control: max-age=0');

    	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    	$objWriter->save('php://output');
		

        // $data['group'] = $group;
        // $data['namaunit'] = $this->Lgudang_stok_model->get_namaUnit($iddariunit);
        // $data['namatipe'] = $this->Lgudang_stok_model->get_namaTipe($idtipe);
        // $data['namakategori'] = $this->Lgudang_stok_model->get_namaKategori($idkategori);
        // $data['namastok'] = $this->Lgudang_stok_model->get_namaStok($statusstok);
        // $data['nama_periode'] = get_bulan(date('m')).' '.date('Y');

        // $data=array_merge($data, backend_info());
        // $html = $this->load->view('Lgudang_stok/print_barang', $data, true);
        // $html = $this->parser->parse_string($html, $data);
        // // print_r(date('d'));exit();
        // $dompdf->loadHtml($html);

        // // (Optional) Setup the paper size and orientation
        // $dompdf->setPaper('A4', 'portrait');

        // // Render the HTML as PDF
        // $dompdf->render();

        // // // Output the generated PDF to Browser
        // $dompdf->stream('Rincian Global.pdf', array("Attachment"=>0));
    }

		public function export_excel($idtipe, $idbarang, $idunit)
    {
				$header = $this->Lgudang_stok_model->get_info($idtipe, $idbarang, $idunit);
				$data = $this->Lgudang_stok_model->getKartuStok($idtipe, $idbarang);

				$this->load->library('excel');
				$this->excel->setActiveSheetIndex(0);
				$activeSheet = $this->excel->getActiveSheet();
				$activeSheet->setTitle('Kartu Stok');

        // Set Logo
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $logo = FCPATH.'/assets/upload/logo/logoreport.jpg';
        $objDrawing->setPath($logo);
        $objDrawing->setCoordinates('B1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setHeight(70);
        $objDrawing->setWidth(30);
        $objDrawing->setWorksheet($activeSheet);

        // Set Header
        $activeSheet->setCellValue('B1', "                                      RUMAH SAKIT KHUSUS BEDAH HALMAHERA SIAGA");
        $activeSheet->setCellValue('B2', "                                      Jl.LLRE.Martadinata no.28, Telp: +6222-4206061");
        $activeSheet->setCellValue('B3', "                                      Bandung, 40115");
        $activeSheet->mergeCells('B1:AA1');
        $activeSheet->mergeCells('B2:AA2');
        $activeSheet->mergeCells('B3:AA3');
        $activeSheet->getStyle('B1:AA1')->getFont()->setSize(11);
        $activeSheet->getStyle('B2:AA2')->getFont()->setSize(11);
        $activeSheet->getStyle('B3:AA3')->getFont()->setSize(11);

				// Set Title
				$activeSheet->setCellValue('B5', "KARTU STOK");
				$activeSheet->mergeCells('B5:J5');
				$activeSheet->getStyle('B5')->getFont()->setBold(true)->setSize(14);
				$activeSheet->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				// Set Periode
				$activeSheet->setCellValue('B7', "Unit Pelayanan");
				$activeSheet->setCellValue('C7', $header['namaunit']);

				$activeSheet->setCellValue('B8', "Kode Barang");
				$activeSheet->setCellValue('C8', $header['kode']);

				$activeSheet->setCellValue('B9', "Satuan Kecil");
				$activeSheet->setCellValue('C9', $header['namasatuan']);

				$activeSheet->setCellValue('E7', "Nama Barang");
				$activeSheet->setCellValue('F7', $header['namabarang']);

				$activeSheet->setCellValue('E8', "Tipe Barang");
				$activeSheet->setCellValue('F8', $header['namatipe']);

				$activeSheet->setCellValue('E9', "Kategori");
				$activeSheet->setCellValue('F9', $header['namakategori']);

				// Set Header Column
				$activeSheet->setCellValue('B11', "Tanggal");
				$activeSheet->setCellValue('C11', "User");
				$activeSheet->setCellValue('D11', "Jenis Transaksi");
				$activeSheet->setCellValue('E11', "No. Transaksi");
				$activeSheet->setCellValue('F11', "Tujuan");
				$activeSheet->setCellValue('G11', "Keterangan");
				$activeSheet->setCellValue('H11', "Penerimaan");
				$activeSheet->setCellValue('I11', "Pengeluaran");
				$activeSheet->setCellValue('J11', "Sisa Stok");
				$activeSheet->getStyle('B11:J12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("B11:J12")->applyFromArray(
					array(
						'borders' => array(
							'allborders' => array(
							  'style' => PHPExcel_Style_Border::BORDER_THIN,
			        ),
              'top' => array(
								'style' => PHPExcel_Style_Border::BORDER_THICK,
							)
						),
						'font'  => array(
							'bold'  => true,
							'size'  => 11,
							'name'  => 'Calibri'
						)
					)
				);

				$x = 11;
				$sisaStok = 0;

        if (COUNT($data)) {
            foreach ($data as $row) {
    					$x = $x+1;

							$sisaStok = $sisaStok + $row->penerimaan - $row->pengeluaran;

	    				// Set Content
	            $activeSheet->setCellValue("B$x", $row->tanggal);
	            $activeSheet->setCellValue("C$x", $row->user);
	            $activeSheet->setCellValue("D$x", $row->jenis_trx);
	            $activeSheet->setCellValue("E$x", $row->notransaksi);
	            $activeSheet->setCellValue("F$x", $row->tujuan);
	            $activeSheet->setCellValue("G$x", $row->keterangan);
	            $activeSheet->setCellValue("H$x", $row->penerimaan);
	            $activeSheet->setCellValue("I$x", $row->pengeluaran);
	            $activeSheet->setCellValue("J$x", $sisaStok);
	    				$activeSheet->getStyle("B$x:C$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    				$activeSheet->getStyle("E$x:F$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	            $activeSheet->getStyle("H$x:J$x")->getNumberFormat()->setFormatCode('#,##0');
			    		$activeSheet->getStyle("H$x:J$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			    		$activeSheet->getStyle("B$x:J$x")->applyFromArray(
			        		array(
			        			'borders' => array(
			        				'allborders' => array(
			        						'style' => PHPExcel_Style_Border::BORDER_THIN,
			        				)
			        			)
			        		)
			    		);
        }
			}

	    $x = $x+1;

    	// Set Info Print
    	$activeSheet->setCellValue("B$x", 'Sistem Informasi RSKB Halmahera - Tanggal Cetak '.date("d/m/Y - h:i:s"));
    	$activeSheet->getStyle("B$x")->getFont()->setSize(11);
    	$activeSheet->mergeCells("B$x:J$x");
    	$activeSheet->getStyle("B$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

    	$x = $x+1;

    	// Set Auto Width Column
    	$activeSheet->getColumnDimension("A")->setWidth(5);
    	$activeSheet->getColumnDimension("B")->setAutoSize(true);
    	$activeSheet->getColumnDimension("C")->setAutoSize(true);
    	$activeSheet->getColumnDimension("D")->setAutoSize(true);
    	$activeSheet->getColumnDimension("E")->setAutoSize(true);
    	$activeSheet->getColumnDimension("F")->setAutoSize(true);
    	$activeSheet->getColumnDimension("G")->setAutoSize(true);
    	$activeSheet->getColumnDimension("H")->setAutoSize(true);
    	$activeSheet->getColumnDimension("I")->setAutoSize(true);
    	$activeSheet->getColumnDimension("J")->setAutoSize(true);

    	header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    	header('Content-Disposition: attachment;filename="Kartu Stok.xls"');
    	header('Cache-Control: max-age=0');

    	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    	$objWriter->save('php://output');
    }
}
