<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtarif_harga_kamar extends CI_Controller {

	/**
	 * Kelompok Pasien controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtarif_harga_kamar_model');
		$this->load->helper('path');
  }

	function index(){
		$id=1;
		if($id != ''){
			$row = $this->Mtarif_harga_kamar_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 															=> $row->id,
					'truang_perawatan'								=> $row->truang_perawatan,
					'truang_hcu'											=> $row->truang_hcu,
					'truang_icu'											=> $row->truang_icu,
					'truang_isolasi'									=> $row->truang_isolasi,
				);

				$data['error'] 			= '';
				$data['title'] 			= 'Pengaturan Harga Kamar';
				$data['content']	 	= 'Mtarif_harga_kamar/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Setting Harga Kamar",'#'),
											    			array("Pengaturan",'mtarif_harga_kamar')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtarif_harga_kamar','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtarif_harga_kamar');
		}
	}

	function save(){
				// print_r('sini');exit;
			if($this->Mtarif_harga_kamar_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mtarif_harga_kamar','location');
			}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mtarif_harga_kamar/manage';

		$data['title'] = 'Ubah Kelompok Pasien';
		$data['breadcrum'] = array(
														array("RSKB Halmahera",'#'),
														array("Kelompok Pasien",'#'),
														array("Ubah",'mtarif_harga_kamar')
												);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

}
