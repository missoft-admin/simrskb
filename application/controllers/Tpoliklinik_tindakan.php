<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpoliklinik_tindakan extends CI_Controller
{
	/**
	 * Tindakan Poliklinik controller.
	 * Developer @denipurnama & @gunalirezqimauludi
	 */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpoliklinik_pendaftaran_model');
		$this->load->model('Tpoliklinik_tindakan_model');
	}

	public function index()
	{
		$data = [
			'nomedrec' => '',
			'namapasien' => '',
			'idkelompokpasien' => '',
			'idrekanan' => '',
			'iddokter' => '',
			// 'idtipe' => '',
			'idpoliklinik' => '',
			'tanggaldari' => date('d/m/Y'),
			'tanggalsampai' => date('t/m/Y'),
			'status' => '9',
		];
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form, ['332'])) {
			$data['idtipe'] = '1';
		}
		if (UserAccesForm($user_acces_form, ['1332'])) {
			$data['idtipe'] = '2';
		}
		if (UserAccesForm($user_acces_form, ['331'])) {
			$data['idtipe'] = '#';
		}
		$this->session->set_userdata($data);
		$data['error'] = '';
		$data['title'] = 'Tindakan Poliklinik & IGD';
		$data['content'] = 'Tpoliklinik_tindakan/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Tindakan Poliklinik & IGD', '#'],
			['List Pasien', 'tpoliklinik_tindakan']
		];

		$data['list_kelompok_pasien'] = $this->Tpoliklinik_pendaftaran_model->getKelompokPasien();
		$data['list_dokter'] = $this->Tpoliklinik_pendaftaran_model->getDokter();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter()
	{
		$data = [
			'nomedrec' => $this->input->post('nomedrec'),
			'namapasien' => $this->input->post('namapasien'),
			'idkelompokpasien' => $this->input->post('idkelompokpasien'),
			'idrekanan' => $this->input->post('idrekanan'),
			'iddokter' => $this->input->post('iddokter'),
			'idtipe' => $this->input->post('idtipe'),
			'idpoliklinik' => $this->input->post('idpoliklinik'),
			'tanggaldari' => $this->input->post('tanggaldari'),
			'tanggalsampai' => $this->input->post('tanggalsampai'),
			'status' => $this->input->post('status'),
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Tindakan Poliklinik & IGD';
		$data['content'] = 'Tpoliklinik_tindakan/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Tindakan Poliklinik & IGD', '#'],
			['List Pasien', 'tpoliklinik_tindakan']
		];

		$data['list_kelompok_pasien'] = $this->Tpoliklinik_pendaftaran_model->getKelompokPasien();
		$data['list_dokter'] = $this->Tpoliklinik_pendaftaran_model->getDokter();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function create($id)
	{
		if ($id != '') {
			$row = $this->Tpoliklinik_tindakan_model->getSpecifiedPendaftaran($id);
			// print_r($row);exit();
			if (isset($row->id)) {
				$data = [
					'idpendaftaran' => $row->idpendaftaran,
					'idtipepasien' => $row->idtipepasien,
					'title_nama' => $row->title,
					'idtipe' => $row->idtipe,
					'idpoliklinik' => $row->idpoliklinik,
					'no_medrec' => $row->no_medrec,
					'tanggal_lahir' => $row->tanggal_lahir,
					'tanggaldaftar' => $row->tanggaldaftar,
					'nopendaftaran' => $row->nopendaftaran,
					'namapasien' => $row->namapasien,
					'alamat' => $row->alamat_jalan,
					'umur_tahun' => $row->umur_tahun,
					'umur_bulan' => $row->umur_bulan,
					'umur_hari' => $row->umur_hari,
					'idkelompokpasien' => $row->idkelompokpasien,
					'namakelompok' => $row->namakelompok,
					'namaperusahaan' => $row->namaperusahaan,
					'namadokter' => $row->namadokter,
					'status_kawin' => $row->status_kawin,
					'nama_keluarga' => $row->nama_keluarga,
					'namapoli' => $row->namapoli,
					'hp' => $row->hp,
					'statuskasir' => $row->statuskasir,
					'idrekanan' => $row->idrekanan,

					'id' => '',
					'iddokter1' => $row->iddokter,
					'iddokter2' => '',
					'iddokter3' => '',
					'keluhan' => '',
					'tinggibadan' => '',
					'suhubadan' => '',
					'beratbadan' => '',
					'lingkarkepala' => '',
					'sistole' => '',
					'disastole' => '',
					'keterangan' => '',
					'statuskasus' => '',
					'diagnosa' => '',
					'alergiobat' => '',
					'tanggalkontrol' => date('Y-m-d'),
					'tindaklanjut' => '1',
					'sebabluar' => '',
					'menolakdirawat' => '',
					'rujukfarmasi' => '',
					'rujuklaboratorium' => '',
					'rujukradiologi' => '',
					'rujukfisioterapi' => '',
					'idkelompok_diagnosa' => '',
				];

				$data['error'] = '';
				$data['title'] = 'Create Tindakan Poliklinik & IGD';
				$data['content'] = 'Tpoliklinik_tindakan/manage';
				$data['breadcrum'] = [
					['RSKB Halmahera', '#'],
					['Tindakan Poliklinik & IGD', '#'],
					['Create', 'tpoliklinik_tindakan']
				];
				$data['list_unitpelayanan'] = $this->Tpoliklinik_tindakan_model->getListUnitPelayanan();
				$data['list_sebab_luar'] = $this->Tpoliklinik_tindakan_model->getSebabLuar();
				$data['list_kelompok_diagnosa'] = $this->Tpoliklinik_tindakan_model->getKelompokDiagnosa();
				$data['idKelompokDiagnosa'] = '';
				// print_r($data);exit();

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			} else {
				$this->session->set_flashdata('error', true);
				$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
				redirect('tpoliklinik_tindakan/index', 'location');
			}
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('tpoliklinik_tindakan/index');
		}
	}

	public function edit($id)
	{
		if ($id != '') {
			$row = $this->Tpoliklinik_tindakan_model->getSpecifiedPendaftaran($id);
			$rowTindakan = $this->Tpoliklinik_tindakan_model->getSpecifiedTindakan($id);
			if (isset($row->id)) {
				$data = [
					'idpendaftaran' => $row->idpendaftaran,
					'idtipepasien' => $row->idtipepasien,
					'title_nama' => $row->title,
					'idtipe' => $row->idtipe,
					'tanggal_lahir' => $row->tanggal_lahir,
					'idpoliklinik' => $row->idpoliklinik,
					'no_medrec' => $row->no_medrec,
					'tanggaldaftar' => $row->tanggaldaftar,
					'nopendaftaran' => $row->nopendaftaran,
					'namapasien' => $row->namapasien,
					'alamat' => $row->alamat_jalan,
					'umur_tahun' => $row->umur_tahun,
					'umur_bulan' => $row->umur_bulan,
					'umur_hari' => $row->umur_hari,
					'idkelompokpasien' => $row->idkelompokpasien,
					'namakelompok' => $row->namakelompok,
					'namaperusahaan' => $row->namaperusahaan,
					'iddokter' => $row->iddokter,
					'namadokter' => $row->namadokter,
					'status_kawin' => $row->status_kawin,
					'nama_keluarga' => $row->nama_keluarga,
					'namapoli' => $row->namapoli,
					'hp' => $row->hp,
					'statuskasir' => $row->statuskasir,
					'idrekanan' => $row->idrekanan,

					'id' => $rowTindakan->id,
					'iddokter1' => $rowTindakan->iddokter1,
					'iddokter2' => $rowTindakan->iddokter2,
					'iddokter3' => $rowTindakan->iddokter3,
					'keluhan' => $rowTindakan->keluhan,
					'tinggibadan' => $rowTindakan->tinggibadan,
					'suhubadan' => $rowTindakan->suhubadan,
					'beratbadan' => $rowTindakan->beratbadan,
					'lingkarkepala' => $rowTindakan->lingkarkepala,
					'sistole' => $rowTindakan->sistole,
					'disastole' => $rowTindakan->disastole,
					'keterangan' => $rowTindakan->keterangan,
					'statuskasus' => $rowTindakan->statuskasus,
					'diagnosa' => $rowTindakan->diagnosa,
					'alergiobat' => $rowTindakan->alergiobat,
					'tanggalkontrol' => $rowTindakan->tanggalkontrol,
					'tindaklanjut' => $rowTindakan->tindaklanjut,
					'sebabluar' => $rowTindakan->sebabluar,
					'menolakdirawat' => $rowTindakan->menolakdirawat,
					'rujukfarmasi' => $rowTindakan->rujukfarmasi,
					'rujuklaboratorium' => $rowTindakan->rujuklaboratorium,
					'rujukradiologi' => $rowTindakan->rujukradiologi,
					'rujukfisioterapi' => $rowTindakan->rujukfisioterapi,
				];

				$data['error'] = '';
				$data['title'] = 'Edit Tindakan Poliklinik & IGD';
				$data['content'] = 'Tpoliklinik_tindakan/manage';
				$data['breadcrum'] = [
					['RSKB Halmahera', '#'],
					['Tindakan Poliklinik & IGD', '#'],
					['Create', 'tpoliklinik_tindakan']
				];

				$data['list_unitpelayanan'] = $this->Tpoliklinik_tindakan_model->getListUnitPelayanan();
				$data['list_sebab_luar'] = $this->Tpoliklinik_tindakan_model->getSebabLuar();
				$data['list_kelompok_diagnosa'] = $this->Tpoliklinik_tindakan_model->getKelompokDiagnosa();
				$data['list_pelayanan'] = $this->Tpoliklinik_tindakan_model->getListPelayanan($rowTindakan->id);
				$data['list_obat'] = $this->Tpoliklinik_tindakan_model->getListObat($rowTindakan->id);
				$data['list_alkes'] = $this->Tpoliklinik_tindakan_model->getListAlkes($rowTindakan->id);
				$data['idKelompokDiagnosa'] = $this->Tpoliklinik_tindakan_model->getSelectedKelompokDiagnosa($id);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			} else {
				$this->session->set_flashdata('error', true);
				$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
				redirect('tpoliklinik_tindakan/index', 'location');
			}
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('tpoliklinik_tindakan/index');
		}
	}

	public function save()
	{
		$id = $this->input->post('id');
		if ($id == '') {
			if ($this->Tpoliklinik_tindakan_model->saveData()) {
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect('tpoliklinik_tindakan/index', 'location');
			}
		} else {
			if ($this->Tpoliklinik_tindakan_model->updateData($id)) {
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah diupdate.');
				redirect('tpoliklinik_tindakan/index', 'location');
			}
		}
	}

	public function delete($id)
	{
		$this->Tpoliklinik_tindakan_model->softDelete($id);
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'data telah terhapus.');
		redirect('tpoliklinik_tindakan/index', 'location');
	}

	public function daftarOperasi()
	{
		$data = [];
		$data['tipe'] = $this->input->post('tiperencana');
		$data['tanggaloperasi'] = $this->input->post('tanggaloperasi');
		$data['diagnosa'] = $this->input->post('diagnosaoperasi');
		$data['operasi'] = $this->input->post('operasi');
		$data['waktumulaioperasi'] = $this->input->post('jamoperasi');
		$data['namapetugas'] = $this->input->post('petugasoperasi');
		$data['catatan'] = $this->input->post('catatanoperasi');
		$data['idpendaftaran'] = $this->input->post('idpendaftaran');
		$data['status'] = '1';
		$data['statusdatang'] = '0';
		$data['statussetuju'] = '0';
		$data['idasalpendaftaran'] = 1;
		$data['tanggal_diajukan'] = date('Y-m-d H:i:s');
		$row = $this->Tpoliklinik_tindakan_model->get_data_pasien($data['idpendaftaran']);
		$data['idpasien'] = $row->idpasien;
		$data['dpjp_id'] = $row->iddokter;
		if ($this->db->insert('tkamaroperasi_pendaftaran', $data)) {
			$this->db->where('id', $data['idpendaftaran']);
			return $this->db->update('tpoliklinik_pendaftaran', ['rencana' => $this->input->post('tiperencana')]);
		}
	}

	public function daftarRawatInap()
	{
		$this->db->where('id', $this->input->post('idpendaftaran'));
		$this->db->update('tpoliklinik_pendaftaran', ['rencana' => $this->input->post('tiperencana')]);
	}

	public function getIndex($uri)
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		$this->select = [
			'tpoliklinik_pendaftaran.id as idpendaftaran',
			'tpoliklinik_pendaftaran.*',
			'mfpasien.nama as namapasien',
			'mfpasien.no_medrec',
			'mfpasien.alamat_jalan',
			'mdokter.nama AS nmdokter',
			'mpoliklinik.nama AS namapoliklinik',
			'mfpasien.jenis_kelamin',
			'tpoliklinik_tindakan.id AS idtindakan',
			'COALESCE(tpoliklinik_tindakan.status, 0) as statustindak',
			'COALESCE(tpoliklinik_tindakan.rujukfarmasi, 0) AS rujukfarmasi',
			'COALESCE(tpoliklinik_tindakan.rujuklaboratorium, 0) AS rujuklaboratorium',
			'COALESCE(tpoliklinik_tindakan.rujukradiologi, 0) AS rujukradiologi',
			'COALESCE(tpoliklinik_tindakan.rujukfisioterapi, 0) AS rujukfisioterapi',
			'tkasir.status AS statuskasir'
		];

		$this->from = 'tpoliklinik_pendaftaran';

		$this->join = [
			['mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT'],
			['tpoliklinik_tindakan', 'tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id', 'LEFT'],
			['mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT'],
			['mdokter', 'mdokter.id = tpoliklinik_pendaftaran.iddokter', 'LEFT'],
			['mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT'],
			['mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT'],
			['tkasir', 'tkasir.idtipe IN (1,2) AND tkasir.idtindakan = tpoliklinik_tindakan.id', 'LEFT']
		];

		// FILTER
		$this->where = [
			'COALESCE(statusredirect, 0) = ' => 0,
			'tpoliklinik_pendaftaran.status = ' => 1,
		];

		if (navigation_roles('tpoliklinik_tindakan/getIndexPoli') || navigation_roles('tpoliklinik_tindakan/getIndexIgd')) {
		} elseif (navigation_roles('tpoliklinik_tindakan/getIndexPoli')) {
			$this->where = ['tpoliklinik_pendaftaran.idtipe' => 1];
		} else {
			$this->where = ['tpoliklinik_pendaftaran.idtipe' => 2];
		}

		if ($uri == 'filter') {
			if ($this->session->userdata('nomedrec') != null) {
				$this->where = array_merge($this->where, ['mfpasien.no_medrec LIKE' => '%' . $this->session->userdata('nomedrec') . '%']);
			}
			if ($this->session->userdata('namapasien') != null) {
				$this->where = array_merge($this->where, ['mfpasien.nama LIKE' => '%' . $this->session->userdata('namapasien') . '%']);
			}
			if ($this->session->userdata('idkelompokpasien') != '#') {
				$this->where = array_merge($this->where, ['mpasien_kelompok.id' => $this->session->userdata('idkelompokpasien')]);

				if ($this->session->userdata('idkelompokpasien') == 1) {
					if ($this->session->userdata('idrekanan') != '#') {
						$this->where = array_merge($this->where, ['mrekanan.id' => $this->session->userdata('idrekanan')]);
					}
				}
			}
			if ($this->session->userdata('iddokter') != '#') {
				$this->where = array_merge($this->where, ['mdokter.id' => $this->session->userdata('iddokter')]);
			}
			if ($this->session->userdata('idtipe') != '#') {
				$this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.idtipe' => $this->session->userdata('idtipe')]);
			}
			if ($this->session->userdata('idpoliklinik') != '#') {
				$this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.idpoliklinik' => $this->session->userdata('idpoliklinik')]);
			}
			if ($this->session->userdata('tanggaldari') != null) {
				$this->where = array_merge($this->where, ['DATE(tpoliklinik_pendaftaran.tanggaldaftar) >=' => YMDFormat($this->session->userdata('tanggaldari'))]);
			}
			if ($this->session->userdata('tanggalsampai') != null) {
				$this->where = array_merge($this->where, ['DATE(tpoliklinik_pendaftaran.tanggaldaftar) <=' => YMDFormat($this->session->userdata('tanggalsampai'))]);
			}
			if ($this->session->userdata('status') != '9') {
				if ($this->session->userdata('status') == '3') {
					$this->where = array_merge($this->where, ['tkasir.status' => 2]);
				} elseif ($this->session->userdata('status') == '0') {
					$this->where = array_merge($this->where, ['COALESCE(tpoliklinik_tindakan.STATUS, 0) =' => $this->session->userdata('status')]);
				} else {
					$this->where = array_merge($this->where, ['COALESCE(tpoliklinik_tindakan.STATUS, 0) =' => $this->session->userdata('status')]);
					$this->where = array_merge($this->where, ['tkasir.status !=' => 2]);
				}
			}
		} else {
			$this->where = array_merge($this->where, ['DATE(tpoliklinik_pendaftaran.tanggaldaftar)' => date('Y-m-d')]);
			if ($this->session->userdata('idtipe') != '#') {
				$this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.idtipe' => $this->session->userdata('idtipe')]);
			}
		}

		$this->order = [
			'tpoliklinik_pendaftaran.tanggaldaftar' => 'DESC',
			'tpoliklinik_pendaftaran.noantrian' => 'ASC',
		];

		$this->group = ['tpoliklinik_pendaftaran.id'];

		$this->column_search = ['tpoliklinik_pendaftaran.nopendaftaran', 'tpoliklinik_pendaftaran.tanggaldaftar', 'tpoliklinik_pendaftaran.noantrian', 'mfpasien.no_medrec', 'mfpasien.nama', 'mfpasien.alamat_jalan', 'mpoliklinik.nama', 'mpasien_kelompok.nama'];
		$this->column_order = ['tpoliklinik_pendaftaran.nopendaftaran', 'tpoliklinik_pendaftaran.tanggaldaftar', 'tpoliklinik_pendaftaran.noantrian', 'mfpasien.no_medrec', 'mfpasien.nama', 'mfpasien.alamat_jalan', 'mpoliklinik.nama', 'mpasien_kelompok.nama'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			$action = '';
			if ($r->statustindak == '0') {
				$action = '<div class="btn-group">';
				if (UserAccesForm($user_acces_form, ['333'])) {
					$action .= '<a href="' . site_url() . 'tpoliklinik_tindakan/create/' . $r->idpendaftaran . '" data-toggle="tooltip" title="Tindak" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form, ['337'])) {
					$action .= '<a href="#" class="btn btn-success btn-sm getDataRencana" title="Rujuk" data-idpendaftaran="' . $r->idpendaftaran . '" data-tanggallahir="' . $r->tanggal_lahir . '" data-umur="' . $r->umurtahun . ' Th ' . $r->umurbulan . ' Bln ' . $r->umurhari . ' Hr"  data-jeniskelamin="' . GetJenisKelamin($r->jenis_kelamin) . '" data-toggle="modal" data-target="#rencana-modal"><i class="fa fa-tags"></i></a>';
				}
				if (UserAccesForm($user_acces_form, ['338'])) {
					$action .= '<a href="' . site_url() . 'tpoliklinik_pendaftaran/print_document/' . $r->idpendaftaran . '/3" class="btn btn-warning btn-sm" target="_blank">Kartu Status</a>';
				}
				$action .= '</div>';
			} elseif ($r->statustindak == '1') {
				$action = '<div class="btn-group">';
				if (UserAccesForm($user_acces_form, ['340'])) {
					$action .= '<a href="' . site_url() . 'tpoliklinik_tindakan/edit/' . $r->idpendaftaran . '" data-toggle="tooltip" title="Lihat" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
				}
				if (UserAccesForm($user_acces_form, ['337'])) {
					$action .= '<a href="#" class="btn btn-success btn-sm getDataRencana" title="Rujuk" data-idpendaftaran="' . $r->idpendaftaran . '" data-tanggallahir="' . $r->tanggal_lahir . '" data-umur="' . $r->umurtahun . ' Th ' . $r->umurbulan . ' Bln ' . $r->umurhari . ' Hr"  data-jeniskelamin="' . GetJenisKelamin($r->jenis_kelamin) . '" data-toggle="modal" data-target="#rencana-modal"><i class="fa fa-tags"></i></a>';
				}
				if (UserAccesForm($user_acces_form, ['338'])) {
					$action .= '<a href="' . site_url() . 'tpoliklinik_pendaftaran/print_document/' . $r->idpendaftaran . '/3" class="btn btn-success btn-sm" target="_blank">Kartu Status</a>';
				}
				$action .= '</div>';
			} elseif ($r->statustindak == '2') {
				$action = '-';
			}

			$row[] = $no;
			$row[] = '<span data-idpendaftaran="' . $r->id . '">' . $r->nopendaftaran . '</span>';
			$row[] = $r->tanggaldaftar;
			$row[] = $r->noantrian;
			$row[] = $r->no_medrec;
			$row[] = $r->namapasien;
			$row[] = $r->alamat_jalan;
			$row[] = $r->namapoliklinik;
			$row[] = $r->nmdokter;
			$row[] = '<p class="nice-copy">' .
											StatusIndexRujukan($r->rujukfarmasi, 'FR') . '&nbsp;' .
											StatusIndexRujukan($r->rujuklaboratorium, 'LB') . '&nbsp;' .
											StatusIndexRujukan($r->rujukradiologi, 'RD') . '&nbsp;' .
											StatusIndexRujukan($r->rujukfisioterapi, 'FI') . '&nbsp;' .
										'</p>';

			if ($r->statuskasir == 2) {
				$row[] = '<span data-toggle="tooltip" title="Sudah ditindak" class="label label-success"><i class="fa fa-check"></i> Telah Ditransaksikan</span>';
			} else {
				$row[] = StatusTindakan($r->statustindak);
			}

			$row[] = $action;

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function getTindakanRawatJalan($idtipe, $idkelompokpasien, $idrekanan = 0)
	{
		// Get Path
		$this->db->select('mtarif_rawatjalan.path');
		if ($idkelompokpasien == 1) {
			$this->db->select('mtarif_rawatjalan.path');
			if ($idtipe == 1) {
				$this->db->join('mrekanan', 'mrekanan.trawatjalan = mtarif_rawatjalan.id');
			} else {
				$this->db->join('mrekanan', 'mrekanan.tigd = mtarif_rawatjalan.id');
			}
			$this->db->where('mrekanan.id', $idrekanan);
			$query = $this->db->get('mtarif_rawatjalan');

			if ($query->num_rows() > 0) {
				if ($idtipe == 1) {
					$this->db->join('mrekanan', 'mrekanan.trawatjalan = mtarif_rawatjalan.id');
				} else {
					$this->db->join('mrekanan', 'mrekanan.tigd = mtarif_rawatjalan.id');
				}
				$this->db->where('mrekanan.id', $idrekanan);
			} else {
				if ($idtipe == 1) {
					$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatjalan = mtarif_rawatjalan.id');
				} else {
					$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tigd = mtarif_rawatjalan.id');
				}
				$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
			}
		} else {
			if ($idtipe == 1) {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatjalan = mtarif_rawatjalan.id');
			} else {
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.tigd = mtarif_rawatjalan.id');
			}
			$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
		}
		$query = $this->db->get('mtarif_rawatjalan');
		$row = $query->row();

		$rowpath = '';
		if ($row) {
			$rowpath = $row->path;
		}

		// Get Tindakan
		$this->select = [];

		$this->from = 'mtarif_rawatjalan';

		$this->join = [];

		$this->where = [
			'status' => 1,
			'mtarif_rawatjalan.path LIKE' => $rowpath . '%'
		];

		$this->order = [
			'path' => 'ASC'
		];
		$this->group = [];

		$this->column_search = ['nama'];
		$this->column_order = ['nama'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			if ($r->idkelompok == 1) {
				$namatarif = TreeView($r->level, $r->nama);
			} else {
				$namatarif = '<a href="#" data-idtindakan="' . $r->id . '" id="tindakan-select">' . TreeView($r->level, $r->nama) . '</a>';
			}

			$row[] = $no;
			$row[] = $namatarif;
			$row[] = number_format($r->jasasarana);
			$row[] = number_format($r->jasapelayanan);
			$row[] = number_format($r->bhp);
			$row[] = number_format($r->biayaperawatan);
			$row[] = number_format($r->total);

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function getObatUnit($idunitpelayanan, $idkelompokpasien)
	{
		$this->select = [
			'mgudang_stok.*',
			'mdata_obat.id AS idobat',
			'mdata_obat.kode AS kodeobat',
			'mdata_obat.nama AS namaobat',
			'mdata_obat.hargadasar AS hargadasarobat',
			'mdata_obat.stokreorder',
			'mdata_obat.stokminimum',
			'(CASE
					WHEN 5 = ' . $idkelompokpasien . ' THEN mdata_obat.marginumum
					WHEN 1 = ' . $idkelompokpasien . ' THEN mdata_obat.marginasuransi
					WHEN 2 = ' . $idkelompokpasien . ' THEN mdata_obat.marginjasaraharja
					WHEN 3 = ' . $idkelompokpasien . ' THEN mdata_obat.marginbpjskesehatan
					WHEN 4 = ' . $idkelompokpasien . ' THEN mdata_obat.marginbpjstenagakerja
					END) AS marginobat',
			'msatuan.nama AS satuanobat'
		];

		$this->from = 'mgudang_stok';

		$this->join = [
			['mdata_obat', 'mdata_obat.id = mgudang_stok.idbarang AND mgudang_stok.idtipe = 3', ''],
			['msatuan', 'msatuan.id = mdata_obat.idsatuankecil', '']
		];

		$this->where = [
			'mdata_obat.status' => 1,
			'mgudang_stok.idunitpelayanan' => $idunitpelayanan
		];

		$this->order = [
			'mdata_obat.nama' => 'ASC'
		];
		$this->group = [];

		$this->column_search = ['mdata_obat.kode', 'mdata_obat.nama'];
		$this->column_order = ['mdata_obat.kode', 'mdata_obat.nama'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			$hargajualobat = ($r->hargadasarobat + ($r->hargadasarobat * ($r->marginobat / 100)));

			$statusstok = ($r->stok <= 0 ? 'Minus Stock ' . $r->stok : 'Stock ' . $r->stok);
			$row[] = '<a href="#" id="obat-select" data-idobat="' . $r->idobat . '" data-hargadasar="' . number_format($r->hargadasarobat) . '" data-margin="' . number_format($r->marginobat) . '" data-statusstok="' . $statusstok . '" data-stok="' . $r->stok . '">' . $r->kodeobat . '</a>';
			$row[] = $r->namaobat;
			$row[] = $r->satuanobat;
			$row[] = number_format(ceiling($hargajualobat, 100));
			$row[] = WarningStok($r->stok, $r->stokreorder, $r->stokminimum);
			$aksi = '<div class="btn-group">';
			$aksi .= '<a target="_blank" href="' . site_url() . 'lgudang_stok/kartustok/' . $r->idtipe . '/' . $r->idbarang . '/' . $r->idunitpelayanan . '" class="btn btn-xs btn-danger ks" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';
			$aksi .= '</div>';
			$row[] = $aksi;
			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function getAlkesUnit($idunitpelayanan, $idkelompokpasien)
	{
		$this->select = [
			'mgudang_stok.*',
			'mdata_alkes.id AS idalkes',
			'mdata_alkes.kode AS kodealkes',
			'mdata_alkes.nama AS namaalkes',
			'mdata_alkes.hargadasar AS hargadasaralkes',
			'mdata_alkes.stokreorder',
			'mdata_alkes.stokminimum',
			'(CASE
					WHEN 5 = ' . $idkelompokpasien . ' THEN mdata_alkes.marginumum
					WHEN 1 = ' . $idkelompokpasien . ' THEN mdata_alkes.marginasuransi
					WHEN 2 = ' . $idkelompokpasien . ' THEN mdata_alkes.marginjasaraharja
					WHEN 3 = ' . $idkelompokpasien . ' THEN mdata_alkes.marginbpjskesehatan
					WHEN 4 = ' . $idkelompokpasien . ' THEN mdata_alkes.marginbpjstenagakerja
					END) AS marginalkes',
			'msatuan.nama AS satuanalkes'
		];

		$this->from = 'mgudang_stok';

		$this->join = [
			['mdata_alkes', 'mdata_alkes.id = mgudang_stok.idbarang AND mgudang_stok.idtipe = 1', ''],
			['msatuan', 'msatuan.id = mdata_alkes.idsatuankecil', '']
		];

		$this->where = [
			'mdata_alkes.status' => 1,
			'mgudang_stok.idunitpelayanan' => $idunitpelayanan
		];

		$this->order = [
			'mdata_alkes.nama' => 'ASC'
		];
		$this->group = [];

		$this->column_search = ['mdata_alkes.kode', 'mdata_alkes.nama'];
		$this->column_order = ['mdata_alkes.kode', 'mdata_alkes.nama'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			$hargajualalkes = ($r->hargadasaralkes + ($r->hargadasaralkes * ($r->marginalkes / 100)));

			$row[] = '<a href="#" id="alkes-select" data-idalkes="' . $r->idalkes . '" data-hargadasar="' . number_format($r->hargadasaralkes) . '" data-margin="' . number_format($r->marginalkes) . '">' . $r->kodealkes . '</a>';
			$row[] = $r->namaalkes;
			$row[] = $r->satuanalkes;
			$row[] = number_format(ceiling($hargajualalkes, 100));
			$row[] = WarningStok($r->stok, $r->stokreorder, $r->stokminimum);
			$aksi = '<div class="btn-group">';
			$aksi .= '<a target="_blank" href="' . site_url() . 'lgudang_stok/kartustok/' . $r->idtipe . '/' . $r->idbarang . '/' . $r->idunitpelayanan . '" class="btn btn-xs btn-danger ks" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';
			$aksi .= '</div>';
			$row[] = $aksi;

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}
}
