<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mdokter extends CI_Controller
{
	/**
	 * Dokter controller.
	 * Developer @gunalirezqimauludi
	 */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mdokter_model');
		$this->load->model('Mpemilik_saham_model');
		$this->load->helper('path');
	}

	public function index($idkategori = '0')
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form, ['6'])) {
			$data = [];
			$data['idkategori'] = $idkategori;
			$data['error'] = '';
			$data['title'] = 'Dokter';
			$data['content'] = 'Mdokter/index';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['Dokter', '#'],
				['List', 'mdokter']
			];

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		} else {
			redirect('page404');
		}
	}

	public function filter()
	{
		if ($this->input->post('idkategori') != '0') {
			$idkategori = $this->input->post('idkategori');
			redirect("mdokter/index/$idkategori", 'location');
		} else {
			redirect('mdokter/index/0', 'location');
		}
	}

	public function create()
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form, ['8'])) {
			$data = [
				'id' => '',
				'idkategori' => '',
				'foto' => '',
				'nip' => '',
				'nama' => '',
				'jeniskelamin' => '',
				'alamat' => '',
				'tempatlahir' => '',
				'tanggallahir' => '',
				'telepon' => '',
				'email' => '',
				'npwp' => '',
				'pajak' => '',
				'potonganrspagi' => '',
				'potonganrssiang' => '',
				'potonganusg' => '',
				'potonganrsranap' => '',
				'potonganrsranapsiang' => '',
				'pajakranap' => '',
				'potonganrsods' => '',
				'potonganrsodssiang' => '',
				'potonganfeers' => '',
				'potonganfeerspribadi' => '',
				'pajakods' => '',
				'nominaltransport' => '',
				'persentaseperujuk' => '',
				'status' => ''
			];

			$data['error'] = '';
			$data['title'] = 'Tambah Dokter';
			$data['content'] = 'Mdokter/manage';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['Dokter', '#'],
				['Tambah', 'mdokter']
			];

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		} else {
			redirect('page404');
		}
	}

	public function update($id)
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form, ['9'])) {
			if ($id != '') {
				$row = $this->Mdokter_model->getSpecified($id);
				if (isset($row->id)) {
					$data = [
						'id' => $row->id,
						'foto' => $row->foto,
						'idkategori' => $row->idkategori,
						'nip' => $row->nip,
						'nama' => $row->nama,
						'jeniskelamin' => $row->jeniskelamin,
						'alamat' => $row->alamat,
						'tempatlahir' => $row->tempatlahir,
						'tanggallahir' => $row->tanggallahir,
						'telepon' => $row->telepon,
						'email' => $row->email,
						'npwp' => $row->npwp,
						'pajak' => $row->pajak,
						'potonganrspagi' => $row->potonganrspagi,
						'potonganrssiang' => $row->potonganrssiang,
						'potonganusg' => $row->potonganusg,
						'potonganrsranap' => $row->potonganrsranap,
						'potonganrsranapsiang' => $row->potonganrsranapsiang,
						'pajakranap' => $row->pajakranap,
						'potonganrsods' => $row->potonganrsods,
						'potonganrsodssiang' => $row->potonganrsodssiang,
						'potonganfeers' => $row->potonganfeers,
						'potonganfeerspribadi' => $row->potonganfeerspribadi,
						'pajakods' => $row->pajakods,
						'nominaltransport' => $row->nominaltransport,
						'persentaseperujuk' => $row->persentaseperujuk,
						'status' => $row->status
					];
					$data['error'] = '';
					$data['title'] = 'Ubah Dokter';
					$data['content'] = 'Mdokter/manage';
					$data['breadcrum'] = [
						['RSKB Halmahera', '#'],
						['Dokter', '#'],
						['Ubah', 'mdokter']
					];

					$data = array_merge($data, backend_info());
					$this->parser->parse('module_template', $data);
				} else {
					$this->session->set_flashdata('error', true);
					$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
					redirect('mdokter', 'location');
				}
			} else {
				$this->session->set_flashdata('error', true);
				$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
				redirect('mdokter');
			}
		} else {
			redirect('page404');
		}
	}

	public function rekening($id)
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form, ['9'])) {
			if ($id != '') {
				$data = $this->Mdokter_model->getSpecifiedHeader($id);

				$data['error'] = '';
				$data['title'] = 'Rekening Dokter';
				$data['content'] = 'Mdokter/manage_rekening';
				$data['breadcrum'] = [
					['RSKB Halmahera', '#'],
					['Dokter', '#'],
					['Rekening Dokter', 'mdokter/rekening']
				];
				$data['list_bank'] = $this->Mpemilik_saham_model->list_bank();
				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			} else {
				$this->session->set_flashdata('error', true);
				$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
				redirect('mdokter');
			}
		} else {
			redirect('page404');
		}
	}

	public function enable($id)
	{
		$this->Mdokter_model->changeStatus($id, 1);
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'dokter berhasil diaktifkan.');
		redirect('mdokter', 'location');
	}

	public function disable($id)
	{
		$this->Mdokter_model->changeStatus($id, 0);
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'dokter berhasil di non aktifkan.');
		redirect('mdokter', 'location');
	}

	public function save()
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		// print_r('ID:'.$this->input->post('id').' ACCES : '.UserAccesForm($user_acces_form,array('12')));exit();

		if ($this->input->post('id') != '') {
			if (UserAccesForm($user_acces_form, ['12']) == '1') {
				$this->form_validation->set_rules('idkategori', 'Kategori Dokter', 'trim|required|min_length[1]');
			}
		} else {
			$this->form_validation->set_rules('idkategori', 'Kategori Dokter', 'trim|required|min_length[1]');
		}

		$this->form_validation->set_rules('nip', 'NIP', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('jeniskelamin', 'Jenis Kelamin', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('tempatlahir', 'Tempat Lahir', 'trim|required');
		$this->form_validation->set_rules('tanggallahir', 'Tanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('npwp', 'NPWP', 'trim|required');
		$this->form_validation->set_rules('pajak', 'Pajak', 'trim|required');
		$this->form_validation->set_rules('potonganrspagi', 'Potongan Rajal Pagi', 'trim|required');
		$this->form_validation->set_rules('potonganrssiang', 'Potongan Rajal Siang', 'trim|required');
		$this->form_validation->set_rules('potonganusg', 'Potongan USG', 'trim|required');
		$this->form_validation->set_rules('pajakranap', 'Pajak Ranap', 'trim|required');
		$this->form_validation->set_rules('pajakods', 'Pajak ODS', 'trim|required');
		$this->form_validation->set_rules('potonganrsranap', 'Potongan Ranap Pasien RS', 'trim|required');
		$this->form_validation->set_rules('potonganrsranapsiang', 'Potongan Ranap Pasien Pribadi', 'trim|required');
		$this->form_validation->set_rules('potonganrsods', 'Potongan ODS Pasien RS', 'trim|required');
		$this->form_validation->set_rules('potonganrsodssiang', 'Potongan ODS Pasien Pribadi', 'trim|required');
		$this->form_validation->set_rules('potonganfeers', 'Potongan Fee Rujukan Pasien RS', 'trim|required');
		$this->form_validation->set_rules('potonganfeerspribadi', 'Potongan Fee Rujukan Pasien Pribadi', 'trim|required');
		$this->form_validation->set_rules('pajakods', 'Pajak ODS', 'trim|required');
		$this->form_validation->set_rules('nominaltransport', 'Nominal Transport', 'trim|required');
		$this->form_validation->set_rules('persentaseperujuk', 'Persentase Perujuk', 'trim|required');

		if ($this->form_validation->run() == true) {
			if ($this->input->post('id') == '') {
				if ($this->Mdokter_model->saveData()) {
					$this->session->set_flashdata('confirm', true);
					$this->session->set_flashdata('message_flash', 'data telah disimpan.');
					redirect('mdokter', 'location');
				}
			} else {
				if ($this->Mdokter_model->updateData()) {
					$this->session->set_flashdata('confirm', true);
					$this->session->set_flashdata('message_flash', 'data telah disimpan.');
					redirect('mdokter', 'location');
				}
			}
		} else {
			$this->failed_save($this->input->post('id'));
		}
	}

	public function failed_save($id)
	{
		$data = $this->input->post();
		$data['error'] = validation_errors();
		$data['content'] = 'Mdokter/manage';

		if ($id == '') {
			$data['title'] = 'Tambah Dokter';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['Dokter', '#'],
				['Tambah', 'mdokter']
			];
		} else {
			$data['title'] = 'Ubah Dokter';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['Dokter', '#'],
				['Ubah', 'mdokter']
			];
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function getIndex($idkategori = '0')
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];

		$this->select = [];
		$this->from = 'mdokter';
		$this->join = [];
		if ($idkategori != '0') {
			$this->where = [
				'idkategori' => $idkategori,
				'status' => '1'
			];
		} else {
			$this->where = [
				'status' => '1'
			];
		}
		$this->order = [
			'nama' => 'ASC'
		];
		$this->group = [];
		if (UserAccesForm($user_acces_form, ['7'])) {
			$this->column_search = ['nip', 'nama'];
		} else {
			$this->column_search = [];
		}
		$this->column_order = ['nip', 'nama'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $row) {
			$no++;
			$result = [];

			$action = '';

			if (UserAccesForm($user_acces_form, ['9'])) {
				$action .= '<a href="' . site_url() . 'mdokter/update/' . $row->id . '" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
			}

			if ($row->status == 1) {
				if (UserAccesForm($user_acces_form, ['10'])) {
					$action .= '<a href="' . site_url() . 'mdokter/disable/' . $row->id . '" data-toggle="tooltip" title="Non Aktifkan" class="btn btn-danger btn-sm"><i class="fa fa-ban"></i></a>';
				}
			} else {
				if (UserAccesForm($user_acces_form, ['10'])) {
					$action .= '<a href="' . site_url() . 'mdokter/enable/' . $row->id . '" data-toggle="tooltip" title="Aktifkan" class="btn btn-success btn-sm"><i class="fa fa-reply"></i></a>';
				}
			}

			$action .= '<a href="' . site_url() . 'mdokter/setting/' . $row->id . '" data-toggle="tooltip" title="Setting Pembayaran" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>';
			$action .= '<a href="' . site_url() . 'mdokter/rekening/' . $row->id . '" data-toggle="tooltip" title="Setting Pembayaran" class="btn btn-primary btn-sm"><i class="fa fa-credit-card"></i></a>';

			$result[] = $no;
			$result[] = $row->nip;
			$result[] = $row->nama;
			$result[] = GetJenisKelamin($row->jeniskelamin);
			$result[] = GetKategoriDokter($row->idkategori);
			$result[] = $row->pajak . '%';
			$result[] = $row->potonganrspagi . '%';
			$result[] = $row->potonganrssiang . '%';
			$result[] = '<div class="btn-group">' . $action . '</div>';

			$data[] = $result;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function setting($iddokter, $idsetting = '')
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form, ['9'])) {
			if ($iddokter != '') {
				$row = $this->Mdokter_model->getSpecified($iddokter);

				if ($idsetting) {
					$setting = $this->Mdokter_model->getSpecifiedSettingPembayaran($idsetting);
					$id = $setting->id;
					$tanggal = $setting->tanggal;
					$jatuh_tempo = $setting->jatuh_tempo;
				} else {
					$id = '';
					$tanggal = '';
					$jatuh_tempo = '';
				}

				if (isset($row->id)) {
					$data = [
						'id' => $id,
						'iddokter' => $row->id,
						'nama_dokter' => $row->nama,
						'kategori_dokter' => GetKategoriDokter($row->idkategori),
						'tanggal' => $tanggal,
						'jatuh_tempo' => $jatuh_tempo,
					];
					$data['error'] = '';
					$data['title'] = 'Setting Pembayaran';
					$data['content'] = 'Mdokter/setting';
					$data['breadcrum'] = [
						['RSKB Halmahera', '#'],
						['Dokter', '#'],
						['Setting Pembayaran', 'mdokter']
					];

					$data['listSetting'] = $this->Mdokter_model->getListSettingPembayaran($row->id);

					$data = array_merge($data, backend_info());
					$this->parser->parse('module_template', $data);
				} else {
					$this->session->set_flashdata('error', true);
					$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
					redirect('mdokter', 'location');
				}
			} else {
				$this->session->set_flashdata('error', true);
				$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
				redirect('mdokter');
			}
		} else {
			redirect('page404');
		}
	}

	public function save_setting()
	{
		if ($this->input->post('id') == '') {
			if ($this->Mdokter_model->saveSettingPembayaran()) {
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect('mdokter/setting/' . $this->input->post('iddokter'), 'location');
			}
		} else {
			if ($this->Mdokter_model->updateSettingPembayaran()) {
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect('mdokter/setting/' . $this->input->post('iddokter'), 'location');
			}
		}
	}

	public function delete_setting($iddokter, $id)
	{
		$this->Mdokter_model->deleteSetting($id, 0);
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'dokter berhasil di non aktifkan.');
		redirect('mdokter/setting/' . $iddokter, 'location');
	}

	public function LoadRekening()
	{
		$iddokter = $this->input->post('iddokter');

		$this->select = [];
		$this->join = [];
		$this->where = [];

		$from = "(SELECT M.*,B.bank from mdokter_bank M
				LEFT JOIN ref_bank B ON B.id=M.idbank
				WHERE M.iddokter='$iddokter' AND M.status='1'
				ORDER BY pilih_default DESC
			) as tbl  ";

		// print_r($from);exit();
		$this->order = [];
		$this->group = [];
		$this->from = $from;

		$this->column_search = [];
		$this->column_order = [];
		$url = site_url('mpasien_kelompok/');
		$list = $this->datatable->get_datatables(true);
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			$row[] = $r->id; //0
			$row[] = $r->idbank; //1
			$row[] = $r->bank;
			$row[] = $r->norek;
			$row[] = $r->atas_nama;
			$row[] = ($r->pilih_default == '1' ? text_success('Primary') : text_default('Secondary'));
			$row[] = $r->keterangan;

			$aksi = '<div class="btn-group">';
			$aksi .= '<button class="btn btn-xs btn-primary edit" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button class="btn btn-xs btn-danger hapus" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';

			$aksi .= '</div>';
			$row[] = $aksi;
			$row[] = $r->pilih_default;
			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(true),
			'recordsFiltered' => $this->datatable->count_all(true),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function save_rekening()
	{
		// data: {idbank: idbank,norek: norek,iddokter:iddokter,atas_nama:atas_nama},
		$iddokter = $this->input->post('iddokter');
		$idbank = $this->input->post('idbank');
		$norek = $this->input->post('norek');
		$atas_nama = $this->input->post('atas_nama');
		$keterangan = $this->input->post('keterangan');
		$pilih_default = $this->input->post('pilih_default');
		if ($pilih_default == '1') {
			$this->db->query("update mdokter_bank SET pilih_default='0' WHERE iddokter='$iddokter'");
		}
		$data = [
			'iddokter' => $iddokter,
			'idbank' => $idbank,
			'norek' => $norek,
			'atas_nama' => $atas_nama,
			'keterangan' => $keterangan,
			'pilih_default' => $pilih_default,
			'created_by' => $this->session->userdata('user_id'),
			'created_nama' => $this->session->userdata('user_name'),
			'created_date' => date('Y-m-d H:i:s'),
			'updated_by' => $this->session->userdata('user_id'),
			'updated_nama' => $this->session->userdata('user_name'),
			'updated_date' => date('Y-m-d H:i:s'),

		];
		$result = $this->db->insert('mdokter_bank', $data);

		if ($result) {
			$this->output->set_output(json_encode($result));
		} else {
			$this->output->set_output(json_encode($result));
		}
	}

	public function update_rekening()
	{
		$rek_id = $this->input->post('rek_id');
		$iddokter = $this->input->post('iddokter');
		$idbank = $this->input->post('idbank');
		$norek = $this->input->post('norek');
		$atas_nama = $this->input->post('atas_nama');
		$keterangan = $this->input->post('keterangan');
		$pilih_default = $this->input->post('pilih_default');
		if ($pilih_default == '1') {
			$this->db->query("update mdokter_bank SET pilih_default='0' WHERE iddokter='$iddokter'");
		}
		$data = [
			'iddokter' => $iddokter,
			'idbank' => $idbank,
			'norek' => $norek,
			'atas_nama' => $atas_nama,
			'keterangan' => $keterangan,
			'pilih_default' => $pilih_default,
			'updated_by' => $this->session->userdata('user_id'),
			'updated_nama' => $this->session->userdata('user_name'),
			'updated_date' => date('Y-m-d H:i:s'),

		];

		$this->db->where('id', $rek_id);
		;
		$result = $this->db->update('mdokter_bank', $data);

		if ($result) {
			$this->output->set_output(json_encode($result));
		} else {
			$this->output->set_output(json_encode($result));
		}
	}

	public function hapus_data()
	{
		$rek_id = $this->input->post('rek_id');

		$data = [
			'status' => 0,
			'deleted_by' => $this->session->userdata('user_id'),
			'deleted_nama' => $this->session->userdata('user_name'),
			'deleted_date' => date('Y-m-d H:i:s'),

		];
		$this->db->where('id', $rek_id);
		;
		$result = $this->db->update('mdokter_bank', $data);

		if ($result) {
			$this->output->set_output(json_encode($result));
		} else {
			$this->output->set_output(json_encode($result));
		}
	}
}
