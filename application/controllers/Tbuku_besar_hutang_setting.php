<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tbuku_besar_hutang_setting extends CI_Controller {

	/**
	 * Setting Saldo Awal Hutang controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbuku_besar_hutang_setting_model','model');
		$this->load->model('tvalidasi_pendapatan_rajal_model');
  }

	function index(){
		
		$data = array();
		$data['list_distributor'] 			= $this->model->list_distributor();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Saldo Awal Hutang';
		$data['content'] 		= 'Tbuku_besar_hutang_setting/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Saldo Awal Hutang",'#'),
									    			array("List",'mdistributor')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function getIndex()
	{
		// GetTipeDistributor
		$where='';
		$iddistributor=$this->input->post('iddistributor');
		$tipe_distributor=$this->input->post('tipe_distributor');
		if ($iddistributor){
			$iddistributor=implode(',',$iddistributor);			
		}
		if ($tipe_distributor){
			$tipe_distributor=implode(',',$tipe_distributor);			
		}
		if ($iddistributor){
			$where .=" AND M.iddistributor IN (".$iddistributor.")";
		}
		if ($tipe_distributor){
			$where .=" AND M.tipe IN (".$tipe_distributor.")";
		}
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

		$this->select = array();
		$from="
			(
				SELECT M.*,H.debet,H.kredit FROM (
				SELECT CONCAT('1-',H.id) as iddistributor,H.nama,'1' as tipe,H.id,H.st_verifikasi from mdistributor H 
				UNION ALL
				SELECT CONCAT('3-',H.id) as iddistributor,H.nama,'3' as tipe,H.id,H.st_verifikasi from mvendor H 
				) M 
				LEFT JOIN (
					SELECT H.iddistributor,H.tipe_distributor,SUM(H.debet) as debet,SUM(H.kredit) as kredit 
					FROM tbuku_besar_hutang H
					GROUP BY H.iddistributor,H.tipe_distributor
				) H ON H.iddistributor=M.id AND H.tipe_distributor=M.tipe
					
				WHERE M.id is not null ".$where."
				ORDER BY M.nama
				
			)tbl
		";
		$this->from   = $from;
		$this->join 	= array();
		$this->where  = array();
		$this->order  = array(
			'nama' => 'ASC'
		);
		$this->group  = array();

		$this->column_search   = array('nama');
		$this->column_order    = array();

		$list = $this->datatable->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
		  $no++;
		  $row = array();

		  $row[] = $no;
		  $row[] = GetTipeDistributor($r->tipe);
		  $row[] = $r->nama.($r->st_verifikasi?'<br>'.text_danger('TELAH DIVERIFIKASI'):'');
		  $row[] = number_format($r->debet,2);
		  $row[] = number_format($r->kredit,2);
		  $aksi = '<div class="btn-group">';          
				$aksi .= '<a href="'.site_url().'tbuku_besar_hutang_setting/update/'.$r->tipe.'/'.$r->id.'" data-toggle="tooltip" title="Atur Saldo" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></a>';          
				$aksi .= '<a href="'.site_url().'tbuku_besar_hutang_detail/index/'.$r->tipe.'/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Detail" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>';          
				if ($r->st_verifikasi=='0'){
					$aksi .= '<button data-toggle="tooltip" title="Verifikasi Saldo" class="btn btn-success btn-sm"><i class="fa fa-check" onclick="verifikasi('.$r->tipe.','.$r->id.')"> Verifikasi</i></button>';          
					
				}
				// $aksi .= '<a href="'.site_url().'tbuku_besar_hutang_setting/update/'.$r->tipe.'/'.$r->id.'" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';          
		  $aksi .= '</div>';
		  $row[] = $aksi;

		  $data[] = $row;
		}
		$output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(),
		  "recordsFiltered" => $this->datatable->count_all(),
		  "data" => $data
		);
		echo json_encode($output);
	}
	

	function update($tipe,$id){
		$data=$this->model->getSpecified($tipe,$id);
		// print_r($data);exit();
		if($data){
			if ($data['st_verifikasi']=='1'){
				$data['disabel']='disabled';
			}else{
				
				$data['disabel']='';
			}
			$data['list_akun'] = $this->tvalidasi_pendapatan_rajal_model->list_akun();
			$data['cari_tipe'] 			= '1';
			$data['tanggal_trx1'] 			= '';
			$data['tanggal_trx2'] 			= '';
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Setting Saldo Awal Hutang';
			$data['content']	 	= 'Tbuku_besar_hutang_setting/manage';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Setting Saldo Awal Hutang",'#'),
														array("Ubah",'mdistributor')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mdistributor');
		}
	}
	function load_gudang(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$tipe=$this->input->post('tipe');
		$no_terima=$this->input->post('xnotransaksi');
		$iddistributor=$this->input->post('iddistributor');
		$no_fakur=$this->input->post('no_fakur');
		$tanggalterima1=$this->input->post('xtgl1');
		$tanggalterima2=$this->input->post('xtgl2');
		
		
		$where1='';
		$where='';
		$where2='';
		$where_pengajuan='';
		if ($tipe !='#'){
			$where2 .=" AND tipe='$tipe' ";
		}
		if ($iddistributor !='#'){
			$where .=" AND H.iddistributor='$iddistributor' ";
			$where1 .=" AND H.iddistributor='$iddistributor' ";
			$where_pengajuan .=" AND H.idvendor='$iddistributor' ";
			
		}
		
		if ($no_terima !=''){
			$where .=" AND H.nopenerimaan='$no_terima' ";
			$where1 .=" AND P.nopenerimaan='$no_terima' ";
		}
		if ($no_fakur !=''){
			$where .=" AND H.nofakturexternal='$no_fakur' ";
			$where1 .=" AND P.nofakturexternal='$no_fakur' ";
		}
		
		
		if ('' != $tanggalterima1) {
            $where .= " AND DATE(H.tgl_terima) >='".YMDFormat($tanggalterima1)."' AND DATE(H.tgl_terima) <='".YMDFormat($tanggalterima2)."'";
        }
		
        $from = "(
					SELECT H.*,TB.id as id_trx FROM (
									SELECT 1 as tipe,H.jenis_retur,H.id,H.tipepenerimaan,H.nopenerimaan,H.tanggalpenerimaan as tgl_trx,
									H.tgl_terima,H.tipe_bayar,H.tanggaljatuhtempo,H.nofakturexternal,
									H.iddistributor,H.totalharga,H.`status`,H.stverif_kbo
									,P.nopemesanan,M.nama as namadistributor,'0' as tipe_kembali
									,H.total_pembayaran,'tgudang_penerimaan' as ref_tabel
									from tgudang_penerimaan H
									LEFT JOIN tgudang_pemesanan P ON H.idpemesanan=P.id
									LEFT JOIN mdistributor M ON M.id=H.iddistributor
									WHERE H.status !='0' ".$where." AND H.tipe_bayar='2'
									
									UNION ALL 
									
									SELECT '2' as tipe,'1' as jenis_retur,H.id,P.tipepenerimaan,H.nopengembalian as nopenerimaan,H.tanggal as tgl_trx,P.tgl_terima as tgl_terima,P.tipe_bayar,
									H.tanggalkontrabon as tanggaljatuhtempo,P.nofakturexternal
									,H.iddistributor,H.totalharga,P.`status`,H.stverif_kbo,P1.nopemesanan,M.nama as namadistributor,H.tipe_kembali
									,0 as total_pembayaran,'tgudang_pengembalian' as ref_tabel
									from tgudang_pengembalian H
									LEFT JOIN tgudang_penerimaan P ON P.id=H.idpenerimaan
									LEFT JOIN tgudang_pemesanan P1 ON P1.id=P.idpemesanan
									LEFT JOIN mdistributor M ON M.id=H.iddistributor
									WHERE H.jenis='1' AND H.jenis_retur='1' AND H.`status`='2' ".$where1." AND P.tipe_bayar='2'
									
									UNION ALL
									
									SELECT '3' as tipe,'' as jenis_retur,H.id,'0' as tipepenerimaan,H.no_pengajuan as nopenerimaan,H.tanggal_pengajuan as tgl_trx 
									,H.tanggal_dibutuhkan as tgl_terima,'2' as tipe_bayar,H.tanggal_kontrabon as tanggaljatuhtempo,'' as nofakturexternal
									,H.idvendor as iddistributor,H.grand_total as totalharga,H.`status`,H.stverif_kbo,'' as nopemesanan,M.nama as namadistributor
									,'0' as tipe_kembali,'0' as total_pembayaran,'rka_pengajuan' as ref_tabel
									from rka_pengajuan H
									LEFT JOIN mvendor M ON M.id=H.idvendor
									WHERE H.`status` > 2 AND H.jenis_pembayaran='4' ".$where_pengajuan."
				) H
				LEFT JOIN tbuku_besar_hutang TB ON TB.idtransaksi=H.id AND H.tipe=TB.tipe_transaksi AND H.iddistributor=TB.iddistributor
				) as tbl WHERE id != '0' ".$where2." ORDER BY id_trx,tgl_trx DESC";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

		
        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $verif_name     = '';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tkontrabon_verifikasi/');
            $url_gudang        = site_url('tgudang_penerimaan/');
            $url_pengajuan        = site_url('mrka_pengajuan/');
            $url_batal       = site_url('tgudang_pengembalian/');
			$status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->tipe;
            $row[] = $r->id;
            // $row[] = $r->id;
            $row[] = $no;
            $row[] = $this->tipe_pemesanan($r->tipe,$r->jenis_retur).$status_retur;
            $row[] = $r->namadistributor.'  '.$this->cara_bayar($r->tipe_bayar);;
            $row[] = $r->nopenerimaan;
            $row[] = $r->nofakturexternal;
            $row[] = HumanDateShort($r->tgl_terima);
            $row[] = number_format($r->totalharga,2);
            if ($r->id_trx){
				$aksi = text_danger('SUDAH DIPILIH');
			}else{
				$aksi .= '<button class="view btn btn-xs btn-primary" onclick="pilih_hutang('.$r->tipe.','.$r->id.')" type="button"  title="Detail"><i class="fa fa-check"></i> Pilih</button>';
			$aksi.='</div>';
				
			}
			
            $row[] = $aksi;
            $row[] = $r->stverif_kbo;
            $row[] = $verif_name;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(true),
            "recordsFiltered" => $this->datatable->count_all(true),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function status_tipe_kembali($status){
		if ($status=='1'){
			$status=' <span class="label label-warning">TUNAI</span>';
		}elseif($status=='2'){
			$status=' <span class="label label-primary">KBO</span>';
		}else{
			$status='';
		}
		
		return $status;
	}
	function tipe_pemesanan($tipe,$jenis_retur){
		if ($tipe=='1'){
			$tipe='<span class="label label-primary">PEMESANAN</span>';
		}else{
			if ($jenis_retur=='1'){
				$tipe='<span class="label label-danger">RETUR UANG</span>';
			}elseif($jenis_retur=='2'){
				$tipe='<span class="label label-default">RETUR GANTI BARANG</span>';
			}elseif($jenis_retur=='3'){
				$tipe='<span class="label label-success">RETUR BEDA</span>';
			}
		}
		if ($tipe=='3'){
			$tipe='<span class="label label-warning">PENGAJUAN</span>';
		}
		return $tipe;
	}
	function tipe_pemesanan2($tipe){
		if ($tipe=='1'){
			$tipe='<span class="label label-primary">PEMESANAN</span>';
		}else{
			$tipe='<span class="label label-danger">RETUR</span>';
			
		}
		if ($tipe=='3'){
			$tipe='<span class="label label-warning">PENGAJUAN</span>';
		}
		return $tipe;
	}
	function cara_bayar($tipe){
		if ($tipe=='1'){
			$tipe='<span class="label label-success">TUNAI</span>';
		}elseif($tipe=='2'){
			$tipe='<span class="label label-danger">KREDIT</span>';
		}else{
			$tipe='<span class="label label-default">-</span>';
		}
		
		return $tipe;
	}
	function get_hutang(){
		$idtransaksi=$this->input->post('idtransaksi');
		$tipe=$this->input->post('tipe');
		$idedit=$this->input->post('idedit');
		if ($idedit==''){
			if ($tipe=='1'){
				$q="SELECT H.id as idtransaksi,'1' as tipe_transaksi,'1' as tipe_distributor
					,DATE_FORMAT(H.tgl_trx,'%d-%m-%Y') as tanggal_transaksi,H.iddistributor,H.namadistributor 
					,H.nopenerimaan as notransaksi,H.nofakturexternal as nofaktur,H.totalharga as nominal
					,0 as debet,H.totalharga as kredit,'K' as posisi_akun
					FROM (
					SELECT 
					CASE WHEN H.st_retur=0 THEN 1 ELSE 2 END as tipe,H.jenis_retur,H.id,H.tipepenerimaan,H.nopenerimaan,H.tanggalpenerimaan as tgl_trx,
					H.tgl_terima,H.tipe_bayar,H.tanggaljatuhtempo,H.nofakturexternal,
					H.iddistributor,H.totalharga,H.`status`,H.stverif_kbo
					,P.nopemesanan,M.nama as namadistributor,'0' as tipe_kembali
					,H.total_pembayaran,'tgudang_penerimaan' as ref_tabel
					from tgudang_penerimaan H
					LEFT JOIN tgudang_pemesanan P ON H.idpemesanan=P.id
					LEFT JOIN mdistributor M ON M.id=H.iddistributor
					WHERE H.id='$idtransaksi') H";				
			}elseif($tipe=='2'){
				$q="
					SELECT H.id as idtransaksi,'2' as tipe_transaksi,'1' as tipe_distributor
					,DATE_FORMAT(H.tgl_trx,'%d-%m-%Y') as tanggal_transaksi,H.iddistributor,H.namadistributor 
					,H.nopenerimaan as notransaksi,H.nofakturexternal as nofaktur,H.totalharga as nominal
					,H.totalharga as debet,0 as kredit,'D' as posisi_akun
					FROM (

					SELECT '2' as tipe,'1' as jenis_retur,H.id,P.tipepenerimaan,H.nopengembalian as nopenerimaan,H.tanggal as tgl_trx,P.tgl_terima as tgl_terima,P.tipe_bayar,
					H.tanggalkontrabon as tanggaljatuhtempo,P.nofakturexternal
					,H.iddistributor,H.totalharga,P.`status`,H.stverif_kbo,P1.nopemesanan,M.nama as namadistributor,H.tipe_kembali
					,0 as total_pembayaran,'tgudang_pengembalian' as ref_tabel
					from tgudang_pengembalian H
					LEFT JOIN tgudang_penerimaan P ON P.id=H.idpenerimaan
					LEFT JOIN tgudang_pemesanan P1 ON P1.id=P.idpemesanan
					LEFT JOIN mdistributor M ON M.id=H.iddistributor
					WHERE H.id='$idtransaksi'
					) H
				";
			}else{
				$q="SELECT H.id as idtransaksi,'3' as tipe_transaksi,'3' as tipe_distributor
					,DATE_FORMAT(H.tgl_trx,'%d-%m-%Y') as tanggal_transaksi,H.iddistributor,H.namadistributor 
					,H.nopenerimaan as notransaksi,H.nofakturexternal as nofaktur,H.totalharga as nominal
					,0 as debet,H.totalharga as kredit,'D' as posisi_akun
					FROM (

					SELECT '3' as tipe,'' as jenis_retur,H.id,'0' as tipepenerimaan,H.no_pengajuan as nopenerimaan,H.tanggal_pengajuan as tgl_trx 
					,H.tanggal_dibutuhkan as tgl_terima,'2' as tipe_bayar,H.tanggal_kontrabon as tanggaljatuhtempo,'' as nofakturexternal
					,H.idvendor as iddistributor,H.grand_total as totalharga,H.`status`,H.stverif_kbo,'' as nopemesanan,M.nama as namadistributor
					,'0' as tipe_kembali,'0' as total_pembayaran,'rka_pengajuan' as ref_tabel
					from rka_pengajuan H
					LEFT JOIN mvendor M ON M.id=H.idvendor
					WHERE H.id='$idtransaksi'
					) H";
			}
		}else{
			
		}
		// print_r($q);exit();
		$arr=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($arr));
	}
	function load_transaksi_hutang()
	{
		// GetTipeDistributor
		$where='';
		$iddistributor=$this->input->post('iddistributor');
		$tipe_distributor=$this->input->post('tipe_distributor');
		$notransaksi=$this->input->post('notransaksi');
		$nofaktur=$this->input->post('nofaktur');
		$tipe=$this->input->post('tipe');
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		if ($notransaksi !=''){
			$where .=" AND H.notransaksi LIKE '%".$notransaksi."%'";
		}
		if ($nofaktur !=''){
			$where .=" AND H.nofaktur LIKE '%".$nofaktur."%'";
		}
		if ($tipe !='#'){
			$where .=" AND H.tipe_transaksi='".$tipe."'";
		}
		if ($tanggal_trx1 !=''){
			$where .=" AND H.tanggal_transaksi >='".YMDFormat($tanggal_trx1)."' AND H.tanggal_transaksi <='".YMDFormat($tanggal_trx2)."'";
		}
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

		$this->select = array();
		$from="
			(
				SELECT H.*,CONCAT(A.noakun,' - ',A.namaakun) as akun from tbuku_besar_hutang H
				INNER JOIN makun_nomor A ON A.id=H.idakun
				WHERE H.iddistributor='$iddistributor' AND H.tipe_distributor='$tipe_distributor' ".$where."
				ORDER BY H.id DESC
			)tbl
		";
		$this->from   = $from;
		$this->join 	= array();
		$this->where  = array();
		$this->order  = array();
		$this->group  = array();

		$this->column_search   = array();
		$this->column_order    = array();

		$list = $this->datatable->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
		  $no++;
		  $row = array();

		  $row[] = $r->notransaksi.'<br>'.$this->tipe_pemesanan2($r->tipe_transaksi);
		  $row[] = $r->nofaktur;
		  $row[] = HumanDateShort($r->tanggal_transaksi);
		  $row[] = number_format($r->nominal,2);
		  $row[] = $r->akun;
		  if ($r->st_verifikasi=='0'){
			   $aksi = '<div class="btn-group">';          
					$aksi .= '<button data-toggle="tooltip" title="Edit" class="btn btn-primary btn-sm edit" onclick="get_edit('.$r->id.')"><i class="fa fa-pencil"></i></button>';          
					$aksi .= '<button data-toggle="tooltip" title="Delete" class="btn btn-danger btn-sm hapus" onclick="hapus('.$r->id.')"><i class="fa fa-close"></i></button>';          
			  $aksi .= '</div>';
		  }else{
			  $aksi=text_success('TELAH DIVERIFIKASI');
		  }
		 
		  $row[] = $aksi;

		  $data[] = $row;
		}
		$output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(),
		  "recordsFiltered" => $this->datatable->count_all(),
		  "data" => $data
		);
		echo json_encode($output);
	}

	function get_edit(){
		$idtransaksi=$this->input->post('idtransaksi');
		$q="SELECT H.*,CONCAT(A.noakun,' - ',A.namaakun) as akun from tbuku_besar_hutang H
				INNER JOIN makun_nomor A ON A.id=H.idakun
				WHERE H.id='$idtransaksi'";				
		
		$arr=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($arr));
	}
	function hapus_hutang($id){
		$arr=$this->db->delete('tbuku_besar_hutang',array('id'=>$id));		
		$this->output->set_output(json_encode($arr));
	}
	function verifikasi($tipe,$id){
		// $arr=$this->db->delete('tbuku_besar_hutang',array('id'=>$id));	
		if ($tipe=='1'){
			$this->db->where('id',$id);
			$arr=$this->db->update('mdistributor',array('st_verifikasi'=>1));
		}else{
			$this->db->where('id',$id);
			$arr=$this->db->update('mvendor',array('st_verifikasi'=>1));
		}
		$this->output->set_output(json_encode($arr));
	}
	function simpan_hutang(){
		$tipe_transaksi=$this->input->post('tipe_transaksi');
		$idedit=$this->input->post('idedit');
		$idakun=$this->input->post('idakun');
		$nominal= RemoveComma($this->input->post('nominal'));
		if ($tipe_transaksi=='2'){
			$posisi='D';
			$debet=$nominal;
			$kredit=0;
		}else{
			$posisi='K';	
			$kredit=$nominal;
			$debet=0;
		}
		
		if ($idedit==''){
			$data=array(
				'idtransaksi' => $this->input->post('idtransaksi'),
				'tipe_transaksi' => $this->input->post('tipe_transaksi'),
				'tipe_distributor' => $this->input->post('tipe_distributor'),
				'tanggal_transaksi' => YMDFormat($this->input->post('tanggal_transaksi')),
				// 'pierode' => YMFormat($this->input->post('tanggal_transaksi')),
				// 'periode_nama' => get_periode(YMFormat($this->input->post('tanggal_transaksi'))),
				'iddistributor' => $this->input->post('iddistributor'),
				'nama_distributor' => $this->input->post('nama_distributor'),
				'notransaksi' => $this->input->post('notransaksi'),
				'nofaktur' => $this->input->post('nofaktur'),
				'nominal' => $nominal,
				'debet' => $debet,
				'kredit' =>	$kredit,
				'posisi' => $posisi,
				'st_saldo_awal' => 1,
				'idakun' => $this->input->post('idakun'),
			);
			$result=$this->db->insert('tbuku_besar_hutang',$data);
		}else{
			$data=array(				
				'idakun' => $this->input->post('idakun'),
			);
			$this->db->where('id',$idedit);
			$result=$this->db->update('tbuku_besar_hutang',$data);
		}	
		
		$this->output->set_output(json_encode($result));
	}
}
