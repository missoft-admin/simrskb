<?php

defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use Dompdf\Dompdf;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

class Thonor_email extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();
		$this->load->model('Thonor_dokter_model', 'model');
	}

    public function generateRekapPendapatan($idhonor)
    {
        $row = $this->db->query("SELECT thonor_dokter.tanggal_pembayaran, thonor_dokter.namadokter, mdokter.npwp AS npwpdokter FROM thonor_dokter LEFT JOIN mdokter ON mdokter.id = thonor_dokter.iddokter WHERE thonor_dokter.id = $idhonor")->row();

        $filePeriod = date('Ym', strtotime($row->tanggal_pembayaran));
        $fileName = $row->namadokter;
        $folderPath = "report/honor_dokter/{$filePeriod}/$fileName";
        $filePath = "report/honor_dokter/{$filePeriod}/$fileName/Rekapitulasi Pendapatan Honor Dokter.pdf";
        
        if (!file_exists($filePath)) {
            $dompdf = new Dompdf();

            $data = array(
                'id' => $idhonor,
                'title' => 'Rekapitulasi Pendapatan Honor Dokter',
                'titleReport' => 'REKAPITULASI PENDAPATAN HONOR DOKTER',
                'namaDokter' => $row->namadokter,
                'npwpDokter' => $row->npwpdokter,
                'tanggalPembayaran' => $row->tanggal_pembayaran,
                'userFinance' => $this->session->userdata('user_name'),
                'userPrint' => $this->session->userdata('user_name'),
                'datePrint' => date("d-M-Y H:i:s"),
                'statusHold' => '0',
            );

            $pendapatan = $this->model->calcTotalPendapatan($idhonor, 0);
            $pengeluaran = $this->model->calcTotalPengeluaran($idhonor, 0);

            $data['total_pendapatan_brutto'] = number_format($pendapatan->total_pendapatan_brutto);
            $data['total_potongan_rs'] = number_format($pendapatan->total_potongan_rs);
            $data['total_beban_jasa_dokter'] = number_format($pendapatan->total_pendapatan_brutto - $pendapatan->total_potongan_rs);
            $data['total_pajak_dokter'] = number_format($pendapatan->total_pajak_dokter);
            $data['total_potongan_pribadi'] = number_format($pengeluaran->total_potongan_pribadi);
            $data['total_pendapatan_netto'] = number_format($pendapatan->total_pendapatan_brutto - $pendapatan->total_potongan_rs - $pendapatan->total_pajak_dokter - $pengeluaran->total_potongan_pribadi);

            $html = $this->load->view('Thonor_dokter/print/print_rekap_pendapatan', $data, true);

            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'portrait');

            // Generate PDF
            $dompdf->render();
            $filePdf = $dompdf->output();

            if (!file_exists($folderPath)) {
                mkdir($folderPath, 0755, true);
            }

            file_put_contents($filePath, $filePdf);
        }
    }

    public function generateRekapPendapatanTunda($idhonor)
    {
        $row = $this->db->query("SELECT thonor_dokter.tanggal_pembayaran, thonor_dokter.namadokter, mdokter.npwp AS npwpdokter FROM thonor_dokter LEFT JOIN mdokter ON mdokter.id = thonor_dokter.iddokter WHERE thonor_dokter.id = $idhonor")->row();

        $filePeriod = date('Ym', strtotime($row->tanggal_pembayaran));
		$fileName = $row->namadokter;
		$folderPath = "report/honor_dokter/{$filePeriod}/$fileName";
		$filePath = "report/honor_dokter/{$filePeriod}/$fileName/Rekapitulasi Pendapatan Tunda Honor Dokter.pdf";

        if (!file_exists($filePath)) {
            $dompdf = new Dompdf();

            $data = array(
                'id' => $idhonor,
                'title' => 'Rekapitulasi Pendapatan Tunda Honor Dokter',
                'titleReport' => 'REKAPITULASI PENDAPATAN TUNDA HONOR DOKTER',
                'namaDokter' => $row->namadokter,
                'npwpDokter' => $row->npwpdokter,
                'tanggalPembayaran' => $row->tanggal_pembayaran,
                'userFinance' => $this->session->userdata('user_name'),
                'userPrint' => $this->session->userdata('user_name'),
                'datePrint' => date("d-M-Y H:i:s"),
                'statusHold' => '1',
            );

            $pendapatan = $this->model->calcTotalPendapatan($idhonor, 1);
            $pengeluaran = $this->model->calcTotalPengeluaran($idhonor, 1);

            $data['total_pendapatan_brutto'] = number_format($pendapatan->total_pendapatan_brutto);
            $data['total_potongan_rs'] = number_format($pendapatan->total_potongan_rs);
            $data['total_beban_jasa_dokter'] = number_format($pendapatan->total_pendapatan_brutto - $pendapatan->total_potongan_rs);
            $data['total_pajak_dokter'] = number_format($pendapatan->total_pajak_dokter);
            $data['total_potongan_pribadi'] = number_format($pengeluaran->total_potongan_pribadi);
            $data['total_pendapatan_netto'] = number_format($pendapatan->total_pendapatan_brutto - $pendapatan->total_potongan_rs - $pendapatan->total_pajak_dokter - $pengeluaran->total_potongan_pribadi);

            $this->load->view('Thonor_dokter/print/print_rekap_pendapatan', $data);
            $html = $this->load->view('Thonor_dokter/print/print_rekap_pendapatan', $data, true);

            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'portrait');

            // Generate PDF
            $dompdf->render();
            $filePdf = $dompdf->output();

            if (!file_exists($folderPath)) {
                mkdir($folderPath, 0755, true);
            }

			file_put_contents($filePath, $filePdf);
		}
    }

    public function generateDetailRadiologi($idhonor)
    {
        $row = $this->db->query("SELECT tanggal_pembayaran, namadokter FROM thonor_dokter WHERE id = $idhonor")->row();
        
        $filePeriod = date('Ym', strtotime($row->tanggal_pembayaran));
        $fileName = $row->namadokter;
        $folderPath = "report/honor_dokter/{$filePeriod}/$fileName";
        $filePath = "report/honor_dokter/{$filePeriod}/$fileName/Rincian Pendapatan Radiologi.pdf";

        if (!file_exists($filePath)) {
            $dompdf = new Dompdf();

            $data = array(
                'id' => $idhonor,
                'title' => 'Detail Pendapatan Radiologi',
                'titleReport' => 'DETAIL PENDAPATAN RADIOLOGI',
                'namaDokter' => $row->namadokter,
                'tanggalPembayaran' => $row->tanggal_pembayaran,
                'userPrint' => $this->session->userdata('user_name'),
                'datePrint' => date("d-M-Y H:i:s"),
            );

            $html = $this->load->view('Thonor_dokter/print/print_detail_radiologi', $data, true);

            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'portrait');

            // Generate PDF
            $dompdf->render();
            $filePdf = $dompdf->output();

            if (!file_exists($folderPath)) {
                mkdir($folderPath, 0755, true);
            }

			file_put_contents($filePath, $filePdf);
		}
    }

    public function generateDetailRawatJalan($idhonor)
    {
        $row = $this->db->query("SELECT tanggal_pembayaran, namadokter FROM thonor_dokter WHERE id = $idhonor")->row();
        
        $filePeriod = date('Ym', strtotime($row->tanggal_pembayaran));
        $fileName = $row->namadokter;
        $folderPath = "report/honor_dokter/{$filePeriod}/$fileName";
        $filePath = "report/honor_dokter/{$filePeriod}/$fileName/Rincian Pendapatan Rawat Jalan.pdf";

        if (!file_exists($filePath)) {
            $dompdf = new Dompdf();

            $data = array(
                'id' => $idhonor,
                'title' => 'Detail Pendapatan Rawat Jalan',
                'titleReport' => 'DETAIL PENDAPATAN RAWAT JALAN',
                'namaDokter' => $row->namadokter,
                'tanggalPembayaran' => $row->tanggal_pembayaran,
                'userPrint' => $this->session->userdata('user_name'),
                'datePrint' => date("d-M-Y H:i:s"),
            );

            $html = $this->load->view('Thonor_dokter/print/print_detail_rawat_jalan', $data, true);

            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'portrait');
            
            // Generate PDF
            $dompdf->render();
            $filePdf = $dompdf->output();

            if (!file_exists($folderPath)) {
                mkdir($folderPath, 0755, true);
            }
		
			file_put_contents($filePath, $filePdf);
		}
    }

    public function generateReport($idhonor)
    {
        $this->generateRekapPendapatan($idhonor);
        $this->generateRekapPendapatanTunda($idhonor);
        $this->generateDetailRadiologi($idhonor);
        $this->generateDetailRawatJalan($idhonor);
    }

    public function getSettingEmail()
    {
        $this->db->limit('1');
        $query = $this->db->get('msetting_honor_dokter_email');
        return $query->row();
    }

    public function getDokumenLainnya($idhonor)
    {
        $this->db->where('idhonor', $idhonor);
        $query = $this->db->get('thonor_pembayaran_dokumen');
        return $query->result();
    }

    public function updateStatusEmailHonor($idhonor)
    {
        $this->db->set('status_email', 1);
        $this->db->where('id', $idhonor);
        $this->db->update('thonor_dokter');
    }

    public function send($idhonor)
    {
        $honorDokter = $this->db->query("SELECT thonor_dokter.tanggal_pembayaran, thonor_dokter.namadokter, mdokter.email FROM thonor_dokter JOIN mdokter ON mdokter.id = thonor_dokter.iddokter WHERE thonor_dokter.id = $idhonor")->row();
        $periodName = HumanDateMy($honorDokter->tanggal_pembayaran);
        $settingEmail = $this->getSettingEmail();
        $dokumenLainnya = $this->getDokumenLainnya($idhonor);

        $this->generateReport($idhonor);

        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = 0;
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
            $mail->isSMTP();
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $mail->Host = 'mail.halmaherasiaga.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'erm@halmaherasiaga.com';
            $mail->Password = '{T,_Xun}9{@5';
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
            $mail->Port = 465;

            //Recipients
            $mail->setFrom('erm@halmaherasiaga.com', 'Bendahara RSKB Halmahera');
            $mail->addAddress($honorDokter->email, $honorDokter->namadokter);

            //Attachments
            $filePeriod = date('Ym', strtotime($honorDokter->tanggal_pembayaran));
            $fileName = $honorDokter->namadokter;
            $folderPath = "report/honor_dokter/{$filePeriod}/$fileName";

            if ($settingEmail->rincian_pendapatan) {
                $mail->addAttachment("{$folderPath}/Rekapitulasi Pendapatan Honor Dokter.pdf");
            }

            if ($settingEmail->rincian_pendapatan_tunda) {
                $mail->addAttachment("{$folderPath}/Rekapitulasi Pendapatan Tunda Honor Dokter.pdf");
            }

            if ($settingEmail->rincian_radiologi) {
                $mail->addAttachment("{$folderPath}/Rincian Pendapatan Radiologi.pdf");
            }
            
            if ($settingEmail->rincian_rawat_jalan) {
                $mail->addAttachment("{$folderPath}/Rincian Pendapatan Rawat Jalan.pdf");
            }

            if ($settingEmail->dokumen_lainnya) {
                foreach ($dokumenLainnya as $dokumen) {
                    $fileDokumentLainnya = 'assets/upload/honor_pembayaran/' . $dokumen->filename;
                    $mail->addAttachment($fileDokumentLainnya);
                }
            }

            //Content
            $mail->isHTML(true);
            $mail->Subject = "Honor Dokter Periode $periodName";
            $mail->Body = "Kepada Yth<br>
            $honorDokter->namadokter <br>
            Berikut terlampir slip Honor dan Bukti Transfer Bulan $periodName<br>
            Atas perhatianya saya ucapkan terima kasih <br>
            <br>
            <br>
            Indri Septiyani<br> 
            Bagian Keuangan (Bendahara)<br> 
            RS Khusus Bedah Halmahera Siaga<br> 
            Jl. L.L.R.E Martadinata No.28<br> 
            Bandung 40115<br>
            Telp : (022) 4206717 : 4206061<br>
            Fax : (022) 4216436<br> 
            Website : www.halmaherasiaga.com";

            $mail->send();

            $this->updateStatusEmailHonor($idhonor);

            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Email berhasil dikirim.');
            redirect('thonor_bayar', 'location');
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
}