<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_feerujukan_dokter extends CI_Controller {

	/**
	 * Setting Fee Rujukan Dokter controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Msetting_feerujukan_dokter_model');
  }

	function index() {
		$row = $this->Msetting_feerujukan_dokter_model->getAll();
		if ($row) {
			$data = array(
				'tipe_pasien' => json_decode(json_decode($row->tipe_pasien)),
				'asal_pasien' => json_decode(json_decode($row->asal_pasien)),
				'poliklinik' => json_decode(json_decode($row->poliklinik)),
				'tipe_dokter' => json_decode(json_decode($row->tipe_dokter)),
				'nama_dokter' => json_decode(json_decode($row->nama_dokter)),
				'hitung_operasi' => json_decode(json_decode($row->hitung_operasi)),
				'hitung_visite' => json_decode(json_decode($row->hitung_visite))
			);

			$data['error'] 			= '';
			$data['title'] 			= 'Setting Fee Rujukan Dokter';
			$data['content'] 		= 'Msetting_feerujukan_dokter/index';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Setting Fee Rujukan Dokter",'#'),
										    			array("List",'msetting_feerujukan_dokter')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}
	}

	function update() {
		return $this->Msetting_feerujukan_dokter_model->updateData();
	}
}
