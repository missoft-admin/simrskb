<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tbagi_hasil_bayar extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbagi_hasil_bayar_model','model');
		$this->load->model('Mrka_bendahara_model');
	}
	function insert_validasi_bagi_hasil($id){
		$this->model->insert_validasi_bagi_hasil($id);
	}
	function index() {
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		$tgl_terakhir = date('Y-m-d', strtotime($hari_ini));
		$data=array(
			'status'=>'#',
			'mbagi_hasil_id'=>'#',
			'deskripsi'=>'',
			'no_terima'=>'',
			'tanggal_tagihan1'=>'',
			'tanggal_tagihan2'=>'',
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_mbagi_hasil'] 	= $this->model->list_mbagi_hasil();
		$data['title'] 			= 'Transaksi Pembayaran Bagi Hasil';
		$data['content'] 		= 'Tbagi_hasil_bayar/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Bagi Hasil Transaksi",'tbagi_hasil_bayar/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function create(){
		
		
		$data['id'] 				= '';
		$data['mbagi_hasil_id'] 				= '#';
		$data['list_trx'] 				= '';
		$data['disabel'] 				= '';
		$data['error'] 				= '';
		$data['list_bagi_hasil'] 	= $this->model->list_bagi_hasil();
		
		$data['title'] 				= 'Create Pembyaran Bagi Hasil';
		$data['content'] 			= 'Tbagi_hasil_bayar/manage';
			
		
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'), 
										array("Bagi Hasil Transaksi",'tbagi_hasil/index'), 
										array("List",'#') 
									);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		// print_r($id);
	}
	function update($id,$disabel=''){
		$data=$this->model->get_header($id);
		
		// $data['id'] 				= $id;
		$data['disabel'] 				= $disabel;
		$data['error'] 				= '';
		$data['list_bagi_hasil'] 	= $this->model->list_bagi_hasil();
		
		$data['title'] 				= 'Create Pembyaran Bagi Hasil';
		$data['content'] 			= 'Tbagi_hasil_bayar/manage';
			
		
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'), 
										array("Bagi Hasil Transaksi",'tbagi_hasil/index'), 
										array("List",'#') 
									);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		// print_r($id);
	}
	function detail($id,$disabel=''){
		$data=$this->model->get_header($id);
		// print_r($data);exit();
		// $data['id'] 				= $id;
		$data['disabel'] 				= $disabel;
		$data['error'] 				= '';
		$data['list_bagi_hasil'] 	= $this->model->list_bagi_hasil();
		
		$data['title'] 				= 'Pembyaran Bagi Hasil';
		$data['content'] 			= 'Tbagi_hasil_bayar/detail';
			
		
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'), 
										array("Bagi Hasil Transaksi",'tbagi_hasil/index'), 
										array("List",'#') 
									);
		// print_r($data['content']);exit();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		// print_r($id);
	}
	function save(){
		// print_r($this->input->post());exit();
		if($this->input->post('id') == '' ) {
			$id=$this->model->saveData();
			if($id){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tbagi_hasil_bayar/index','location');
			}
		} else {
			if($this->model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tbagi_hasil_bayar/index','location');
			}
		}
		
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
	

		$notransaksi=$this->input->post('notransaksi');
		$mbagi_hasil_id=$this->input->post('mbagi_hasil_id');
		$status=$this->input->post('status');
		
		$tanggal_tagihan1=$this->input->post('tanggal_tagihan1');
		$tanggal_tagihan2=$this->input->post('tanggal_tagihan2');
		
		$where='';
		if ($notransaksi !=''){
			$where .=" AND H.notransaksi='$notransaksi' ";
		}
		
		if ($mbagi_hasil_id !='#'){
			$where .=" AND H.mbagi_hasil_id='$mbagi_hasil_id' ";
		}
		if ('' != $tanggal_tagihan1) {
            $where .= " AND DATE(H.tanggal_trx) >='".YMDFormat($tanggal_tagihan1)."' AND DATE(H.tanggal_trx) <='".YMDFormat($tanggal_tagihan2)."'";
        }
		
		if ($status !='#'){
			$where .=" AND H.status='$status' ";
		}


        $from = "(
					SELECT H.id,H.nama_bagi_hasil,H.tanggal_trx,H.notransaksi,H.mbagi_hasil_id 
					,H.pendapatan_bersih_rs,H.pendapatan_bersih_ps,H.`status`,H.status_approval,COUNT(S.id) as jml_setting
					,(D.jml_det-D.jml_trx) as siap_selesai
					FROM `tbagi_hasil_bayar_head` H
					
					LEFT JOIN mbagi_hasil_logic S ON S.mbagi_hasil_id=H.mbagi_hasil_id AND S.`status`='1'
					LEFT JOIN (
						SELECT TBL.tbagi_hasil_bayar_id as id, COUNT(TBL.id) as jml_det,SUM(st_trx) as jml_trx FROM tbagi_hasil_bayar_detail TBL 
						GROUP BY TBL.tbagi_hasil_bayar_id
					) D ON D.id=H.id
					WHERE H.id is not null ".$where."
					GROUP BY H.id

				) as tbl ";


		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nama_bagi_hasil','notransaksi');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_setting     = '';
            $url        = site_url('tbagi_hasil/');
            if ($r->jml_setting=='0'){
				$status_setting='<span class="label label-danger">Belum Disetting</span>';
			}else{
				if ($r->status=='2'){
					$status_setting .='<br>  <button title="Lihat User" class="btn btn-danger btn-xs user" onclick="load_user('.$r->id.')"><i class="si si-user"></i></button>';
				}
				if ($r->status_approval=='2'){
					$status_setting .='<br><span class="label label-danger">PERSETUJUAN DITOLAK</span>';
				}
			}
			
            $row[] = $r->id;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggal_trx);
            $row[] = $r->notransaksi;
            $row[] = $r->nama_bagi_hasil;
            $row[] = number_format($r->pendapatan_bersih_ps,0);
            $row[] = number_format($r->pendapatan_bersih_rs,0);
            $row[] = status_bagi_hasil_bayar($r->status).' '.$status_setting;			
				if ($r->status !='0'){
					$aksi .= '<a href="'.site_url().'tbagi_hasil_bayar/update/'.$r->id.'/disabled"  target="_blank" data-toggle="tooltip" title="Detail" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
					if ($r->status =='1'){//DRAFT
						$aksi .= '<a href="'.site_url().'tbagi_hasil_bayar/update/'.$r->id.'"  target="_blank" data-toggle="tooltip" title="Detail" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>';
						$aksi .= '<button type="button" title="Hapus" onclick="hapus_header('.$r->id.')" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>';
						$aksi .= '<button type="button" title="Approval" '.($r->jml_setting=='0'?"disabled":"").' onclick="kirim('.$r->id.')" class="btn btn-primary btn-xs"><i class="fa fa-send"></i></button>';
					}
					if ($r->status=='2' && $r->status_approval=='2'){
						$aksi .= '<button type="button" title="Approval" '.($r->jml_setting=='0'?"disabled":"").' onclick="kirim('.$r->id.')" class="btn btn-primary btn-xs"><i class="fa fa-send"></i></button>';
					}
					if ($r->status =='3' && $r->status_approval=='1'){//SETUJUI
						$aksi .= '<a href="'.site_url().'tbagi_hasil_bayar/detail/'.$r->id.'"  target="_blank" data-toggle="tooltip" title="Pembayaran" class="btn btn-primary btn-xs"><i class="si si-credit-card"></i></a>';
						// $aksi .= '<button type="button" title="Hapus" class="btn btn-primary btn-xs"><i class="si si-credit-card"></i></button>';
					}
					if ($r->status =='3' && $r->siap_selesai=='0'){//SIAP VERIFIKASI
						$aksi .= '<button type="button" title="Verifikasi" class="btn btn-success btn-xs selesai"  onclick="selesaikan('.$r->id.')"><i class="fa fa-check"></i> Selesaikan</button>';
						// $aksi .= '<a href="'.site_url().'tbagi_hasil_bayar/detail/'.$r->id.'"  target="_blank" data-toggle="tooltip" title="Pembayaran" class="btn btn-primary btn-xs"><i class="si si-credit-card"></i></a>';
					}
						$aksi .= '<button type="button" title="Print" class="btn btn-success btn-xs"><i class="fa fa-print"></i></button>';
				}else{
					
				}
			$aksi.='</div>';
            $row[] = $aksi;//8            
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_trx(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
	

		$id=$this->input->post('id');				
		$mbagi_hasil_id=$this->input->post('mbagi_hasil_id');				
		$tgl_trx=$this->input->post('tanggal_trx');
		$list_trx=$this->input->post('list_trx');
		$where='';
		if ($id==''){
			$where .=" AND H.`status`='2'";
		}else{
			$where .=" AND H.`status`IN ('2','3')";
		}
		if ($list_trx !='') {
            $where .= " AND H.id NOT IN (".$list_trx.")";
        }
		if ('' != $tgl_trx) {
            $where .= " AND DATE(H.tanggal_tagihan) ='".YMDFormat($tgl_trx)."'";
        }
        $from = "(
					SELECT H.id,H.mbagi_hasil_id,H.notransaksi,H.deskripsi,H.nama_bagi_hasil,H.tanggal_tagihan,H.total_pendapatan,H.pendapatan_bersih_rs,H.pendapatan_bersih_ps FROM tbagi_hasil H
					WHERE H.mbagi_hasil_id='$mbagi_hasil_id' ".$where."
				) as tbl WHERE id is not null   ORDER BY tanggal_tagihan ASC";

			//WHERE H.`status`='2' AND H.mbagi_hasil_id='$mbagi_hasil_id' ".$where."
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }
        foreach ($list as $r) {
            $no++;
            $row = array();
			 $aksi       = '<div class="btn-group">';
            $row[] = $r->id;
            $row[] = $no;
            $row[] = $r->notransaksi;
            $row[] = $r->deskripsi;
            $row[] = HumanDateShort($r->tanggal_tagihan);
            $row[] = number_format($r->total_pendapatan,0);
            $row[] = number_format($r->pendapatan_bersih_rs,0);
            $row[] = number_format($r->pendapatan_bersih_ps,0);
				$aksi .= '<button data-toggle="tooltip" title="Pilih" class="btn btn-primary btn-xs pilih"><i class="fa fa-check"></i> Pilih</button>';
				$aksi.='</div>';
            $row[] = $aksi;//8            
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_hasil(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
	
		$list_trx=$this->input->post('list_trx');
		$disabel=$this->input->post('disabel');
		$where='';
		if ($list_trx !='') {
            $where .= " AND H.id IN (".$list_trx.")";
        }else{
			$where .= " AND H.id=0";
		}
		
        $from = "(
					SELECT H.id,H.mbagi_hasil_id,H.notransaksi,H.deskripsi,H.nama_bagi_hasil,H.tanggal_tagihan,H.total_pendapatan,H.pendapatan_bersih_rs,H.pendapatan_bersih_ps 
					,total_biaya
					FROM tbagi_hasil H
					WHERE H.`status`!='0' ".$where."
				) as tbl WHERE id is not null   ORDER BY tanggal_tagihan ASC";

			//
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }
        foreach ($list as $r) {
            $no++;
            $row = array();
			 $aksi       = '<div class="btn-group">';
            $row[] = $r->id;
            $row[] = $no;
            $row[] = $r->notransaksi;
            $row[] = $r->deskripsi;
            $row[] = HumanDateShort($r->tanggal_tagihan);
            $row[] = number_format($r->total_pendapatan,0);
            $row[] = number_format($r->total_biaya,0);
            $row[] = number_format($r->pendapatan_bersih_rs,0);
            $row[] = number_format($r->pendapatan_bersih_ps,0);
			$row[] = '<button type="button" class="btn btn-success btn-xs">Selesai Diproses</button>';
            $aksi .= '<button type="button" data-toggle="tooltip" '.$disabel.' title="Pilih" class="btn btn-danger btn-xs hapus"><i class="fa fa-trash-o"></i></button>';
			$aksi.='</div>';
            $row[] = $aksi;//8            
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_generate(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
	

		$list_trx=$this->input->post('list_trx');
		
        $q = "
					SELECT H.tbagi_hasil_id,H.id_pemilik_saham,H.nama_pemilik,H.tipe_pemilik,SUM(H.jumlah_lembar) as jumlah_lembar,sum(H.total_pendapatan) as total_pendapatan 
					FROM tbagi_hasil_pemilik_saham H
					WHERE H.tbagi_hasil_id IN (".$list_trx.")
					GROUP BY H.id_pemilik_saham
				";
		$tbl='';
		$tgl='<div class="input-group date" style="width: 100%;">
											<input class="js-datepicker form-control input tgl"  type="text" name="tanggal_bayar[]" value="" data-date-format="dd-mm-yyyy"/>
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
											</div>		';
		$rows=$this->db->query($q)->result();
		$select_metode='<select name="idmetode[]"   class="js-select2 form-control opsi opsi_metode" style="width: 100%;">
							<option value="#" selected>- Pilih -</option>'.getMetode().'
						</select>';
		$no=0;
		foreach($rows as $r){
			$no++;
			$selct_rek='<select name="pemilik_rek_id[]"   class="js-select2 form-control opsi opsi_norek"  style="width: 100%;">
							<option value="#" selected>- Tidak Diisi -</option>'.getRekeningPS($r->id_pemilik_saham).'
						</select>';
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'<input type="hidden" name="tipe_pemilik[]" value="'.$r->tipe_pemilik.'"></td>';
			$tbl .='<td>'.$r->nama_pemilik.'<input type="hidden" name="nama_pemilik[]" value="'.$r->nama_pemilik.'"></td>';//id_pemilik_saham
			$tbl .='<td>'.GetTipePemilikSaham($r->tipe_pemilik).'<input type="hidden" name="id_pemilik_saham[]" value="'.$r->id_pemilik_saham.'"></td>';
			$tbl .='<td class="text-center">'.number_format($r->jumlah_lembar,0).' <input type="hidden" name="jumlah_lembar[]" value="'.$r->jumlah_lembar.'"></td>';
			$tbl .='<td class="text-right">'.number_format($r->total_pendapatan,0).'<input type="hidden" name="total_pendapatan[]" value="'.$r->total_pendapatan.'"></td>';
			$tbl .='<td class="text-left">'.$selct_rek.'</td>';
			$tbl .='<td class="text-left">'.$select_metode.'</td>';
			$tbl .='<td class="text-left">'.$tgl.'</td>';
			// $tbl .='<td></td>';
			$tbl .='</tr>';
		}
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_generate_edit(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
	

		$id=$this->input->post('id');
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT H.id_pemilik_saham,H.nama_pemilik,H.tipe_pemilik,H.jumlah_lembar as jumlah_lembar,H.total_pendapatan,H.idmetode,H.pemilik_rek_id,H.tanggal_bayar from tbagi_hasil_bayar_detail H
				WHERE H.tbagi_hasil_bayar_id='$id'
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		foreach($rows as $r){
			$no++;
			$select_metode='<select name="idmetode[]" '.$disabel.'  class="js-select2 form-control opsi opsi_metode" style="width: 100%;">
							'.getMetodeEdit($r->idmetode).'
						</select>';
						// '.getMetodeEdit($r->idmetode).'
			$selct_rek='<select name="pemilik_rek_id[]" '.$disabel.'  class="js-select2 form-control opsi opsi_norek"  style="width: 100%;">
							<option value="#" selected>- Tidak Diisi -</option>'.getRekeningPSEdit($r->id_pemilik_saham,$r->pemilik_rek_id).'
						</select>';
			$tgl='<div class="input-group date" style="width: 100%;">
				<input class="js-datepicker form-control input tgl" '.$disabel.' type="text" name="tanggal_bayar[]" value="'.HumanDateShort($r->tanggal_bayar).'" data-date-format="dd-mm-yyyy"/>
				<span class="input-group-addon">
					<span class="fa fa-calendar"></span>
				</span>
				</div>		';
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'<input type="hidden" name="tipe_pemilik[]" value="'.$r->tipe_pemilik.'"></td>';
			$tbl .='<td>'.$r->nama_pemilik.'<input type="hidden" name="nama_pemilik[]" value="'.$r->nama_pemilik.'"></td>';//id_pemilik_saham
			$tbl .='<td>'.GetTipePemilikSaham($r->tipe_pemilik).'<input type="hidden" name="id_pemilik_saham[]" value="'.$r->id_pemilik_saham.'"></td>';
			$tbl .='<td class="text-center">'.number_format($r->jumlah_lembar,0).' <input type="hidden" name="jumlah_lembar[]" value="'.$r->jumlah_lembar.'"></td>';
			$tbl .='<td class="text-right">'.number_format($r->total_pendapatan,0).'<input type="hidden" name="total_pendapatan[]" value="'.$r->total_pendapatan.'"></td>';
			$tbl .='<td class="text-left">'.$selct_rek.'</td>';
			$tbl .='<td class="text-left">'.$select_metode.'</td>';
			$tbl .='<td class="text-left">'.$tgl.'</td>';
			// $tbl .='<td></td>';
			$tbl .='</tr>';
		}
		
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_generate_edit_bayar(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
	

		$id=$this->input->post('id');
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT H.id,H.id_pemilik_saham,H.nama_pemilik,H.tipe_pemilik,H.jumlah_lembar as jumlah_lembar,H.total_pendapatan,H.idmetode,H.pemilik_rek_id
				,H.tanggal_bayar,H.st_trx 
				from tbagi_hasil_bayar_detail H
				WHERE H.tbagi_hasil_bayar_id='$id'
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			$total=$total + $r->total_pendapatan;
			if ($r->st_trx=='1'){
				$st_trx ='<br><span class="label label-success">Sudah ditransaksikan</span>';
			}else{
				$st_trx ='<br><span class="label label-danger">Belum ada Transaksi</span>';
			}
			$aksi       = '<div class="btn-group">';
			$select_metode='<select name="idmetode[]" '.$disabel.'  class="js-select2 form-control opsi opsi_metode" style="width: 100%;">
							'.getMetodeEdit($r->idmetode).'
						</select>';
						// '.getMetodeEdit($r->idmetode).'
			$selct_rek='<select name="pemilik_rek_id[]" '.$disabel.'  class="js-select2 form-control opsi opsi_norek"  style="width: 100%;">
							<option value="#" selected>- Tidak Diisi -</option>'.getRekeningPSEdit($r->id_pemilik_saham,$r->pemilik_rek_id).'
						</select>';
			$tgl='<div class="input-group date" style="width: 100%;">
				<input class="js-datepicker form-control input tgl" '.$disabel.' type="text" name="tanggal_bayar[]" value="'.HumanDateShort($r->tanggal_bayar).'" data-date-format="dd-mm-yyyy"/>
				<span class="input-group-addon">
					<span class="fa fa-calendar"></span>
				</span>
				</div>		';
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'<input type="hidden" name="tipe_pemilik[]" value="'.$r->tipe_pemilik.'"></td>';
			$tbl .='<td>'.$r->nama_pemilik.'<input type="hidden" name="nama_pemilik[]" value="'.$r->nama_pemilik.'"></td>';//id_pemilik_saham
			$tbl .='<td>'.GetTipePemilikSaham($r->tipe_pemilik).'<input type="hidden" name="id_pemilik_saham[]" value="'.$r->id_pemilik_saham.'"></td>';
			$tbl .='<td class="text-center">'.number_format($r->jumlah_lembar,0).' <input type="hidden" name="jumlah_lembar[]" value="'.$r->jumlah_lembar.'"></td>';
			$tbl .='<td class="text-right">'.number_format($r->total_pendapatan,0).$st_trx.'<input type="hidden" name="total_pendapatan[]" value="'.$r->total_pendapatan.'"></td>';
			$tbl .='<td class="text-left">'.$selct_rek.'</td>';
			$tbl .='<td class="text-left">'.$select_metode.'</td>';
			$tbl .='<td class="text-left">'.$tgl.'</td>';
				$aksi .= '<button type="button" title="Simpan" class="btn btn-success btn-sm simpan" hidden><i class="fa fa-save"></i></button>';
				$aksi .= '<a href="'.site_url().'tbagi_hasil_bayar/detail_bayar/'.$r->id.'/'.$disabel.'"  data-toggle="tooltip" title="Pembayaran" class="btn btn-primary btn-sm "><i class="si si-credit-card"></i></a>';
			$tbl .='<td class="text-left">'.$aksi.'<input type="hidden" name="iddet[]" class="iddet" value="'.$r->id.'"></td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<td colspan="4" class="text-right"><strong>TOTAL</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total,0).'</strong></td>';
		$tbl .='<td></td>';
		$tbl .='<td></td>';
		$tbl .='<td></td>';
		$tbl .='<td></td>';
		
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function hapus_header($id){
	
		$data =array(
            'list_tbagi_hasil_id'=>'',
            'pendapatan_bersih_rs'=>0,
            'pendapatan_bersih_ps'=>0,
            'jumlah_lembar'=>0,
            'nominal_dibagikan'=>0,
            'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_date'=>date('Y-m-d H:i:s'),


        );
		$this->db->where('id',$id);;
		$result=$this->db->update('tbagi_hasil_bayar_head',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    
	}
	function kirim($id){
	
		$data =array(            
            'status'=>2,
        );
		$this->db->where('id',$id);;
		$result=$this->db->update('tbagi_hasil_bayar_head',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    
	}
	function selesaikan($id){
	
		$data =array(            
            'status'=>4,
            'finish_by'=>$this->session->userdata('user_id'),
			'finish_date'=>date('Y-m-d H:i:s'),
        );
		$this->db->where('id',$id);;
		$result=$this->db->update('tbagi_hasil_bayar_head',$data);
		$this->model->insert_validasi_bagi_hasil($id);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    
	}
	function update_row(){
		//data: {iddet: iddet,idmetode: idmetode,norek: norek,tgl: tgl},
		$iddet=$this->input->post('iddet');
		$idmetode=$this->input->post('idmetode');
		$pemilik_rek_id=$this->input->post('norek');
		$tgl=YMDFormat($this->input->post('tgl'));
		$data =array(
            
            'idmetode'=>$idmetode,
            'pemilik_rek_id'=>$pemilik_rek_id,
            'tanggal_bayar'=>$tgl,


        );
		$row=array();
		if ($pemilik_rek_id !='#'){
			$q="SELECT M.norek,M.idbank,M.atas_nama,R.bank FROM mpemilik_saham_rek M
				LEFT JOIN ref_bank R ON R.id=M.idbank
				WHERE M.id='$pemilik_rek_id'";
			$row=$this->db->query($q)->row_array();
			
		}
		$data = array_merge($data, $row);
		$this->db->where('id',$iddet);;
		$result=$this->db->update('tbagi_hasil_bayar_detail',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    
	}
	function load_user_approval()
    {
	  $id=$this->input->post('id');	 
	  $where='';
	  $from="(
				SELECT S.id,S.step,S.proses_setuju,S.proses_tolak,U.`name` as user_nama,H.nominal_dibagikan FROM tbagi_hasil_bayar_head H
				LEFT JOIN mbagi_hasil_logic S ON S.mbagi_hasil_id=H.mbagi_hasil_id AND S.`status`='1'
				LEFT JOIN musers U ON U.id=S.iduser
				WHERE H.id='$id'  AND calculate_logic(S.operand,H.nominal_dibagikan,S.nominal) = 1
				ORDER BY S.step ASC,S.id ASC
				) as tbl";
				// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('user_nama');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $no_step='';
      foreach ($list as $r) {
          $no++;
          $row = array();
		  if ($no_step != $r->step){
				$no_step=$r->step;
		  }
		  if ($no_step % 2){		 
			$row[] = '<span class="label label-default">STEP '.$r->step.'</span>';
		  }else{
			   $row[] = '<span class="label label-primary">STEP '.$r->step.'</span>';
		  }
          $row[] = $r->user_nama;
          $row[] = ($r->proses_setuju=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
          $row[] = ($r->proses_tolak=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
         
          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function simpan_proses_peretujuan($id){
	
	$result=$this->model->simpan_proses_peretujuan($id);
	echo json_encode($result);
  }
  public function detail_bayar($id,$disabel='')
    {
		$data=$this->model->get_header_bayar($id);
		// print_r($data);exit();
        $data['list_pembayaran']      = $this->model->list_pembayaran($id);
		$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
        $data['st_edit']      = '0';
        $data['error']      = '';
        $data['title']      = 'Pembayaran';
        $data['disabel']      = $disabel;
        $data['content']    = 'Tbagi_hasil_bayar/manage_bayar';
        $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Bagi Hasil Pembayaran",'#'), array("Tambah",'#'));

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	public function save_pembayaran()
    {
		$tbagi_hasil_bayar_id=$this->input->post('tbagi_hasil_bayar_id');
		// $id=$this->model->save_pembayaran();
		if($this->model->save_pembayaran()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tbagi_hasil_bayar/detail/'.$tbagi_hasil_bayar_id,'location');
		}
	}

}
