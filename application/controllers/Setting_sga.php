<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_sga extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Setting_sga_model');
		$this->load->helper('path');
		
  }


	function index($tab='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('2002'))){
				$tab='1';
			}elseif (UserAccesForm($user_acces_form,array('2003'))){
				$tab='2';
			}elseif (UserAccesForm($user_acces_form,array('2004'))){
				$tab='3';
			}elseif (UserAccesForm($user_acces_form,array('2005'))){
				$tab='4';
			}
		}
		if (UserAccesForm($user_acces_form,array('2002','2003','2004','2005'))){
			$data = $this->Setting_sga_model->get_assesmen_setting();
			// $data_label = $this->Setting_sga_model->get_assesmen_label();
			// $data_user = $this->Setting_sga_model->get_assesmen_user();
			// print_r($data_spesifik);exit;
			$data['tab'] 			= $tab;
			$data['spesialisasi_id'] 			= '#';
			$data['profesi_id'] 			= '#';
			$data['mppa_id'] 			= '#';
			$data['st_lihat'] 			= '0';
			$data['st_input'] 			= '0';
			$data['st_edit'] 			= '0';
			$data['st_hapus'] 			= '0';
			$data['st_cetak'] 			= '0';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting SGA';
			$data['content'] 		= 'Setting_sga/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Pengkajian SGA Setting",'setting_sga/index')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function load_hak_akses()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.id,H.profesi_id,P.ref as profesi
							,H.spesialisasi_id,CASE WHEN H.spesialisasi_id=0 THEN 'SEMUA' ELSE S.ref END as spesialisasi 
							,H.mppa_id,M.nama as nama_ppa,H.st_lihat,H.st_input,H.st_edit,H.st_hapus,H.st_cetak
							FROM `setting_sga_akses` H
							LEFT JOIN merm_referensi P ON P.nilai=H.profesi_id AND P.ref_head_id='21' AND P.`status`='1'
							LEFT JOIN merm_referensi S ON S.nilai=H.spesialisasi_id AND S.ref_head_id='22'  AND S.`status`='1'
							LEFT JOIN mppa M ON M.id=H.mppa_id AND M.staktif='1'
							ORDER BY H.profesi_id,H.spesialisasi_id



					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_ppa','spesialisasi','profesi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->profesi);
          $result[] = ($r->spesialisasi_id=='0'?text_default($r->spesialisasi):$r->spesialisasi);
          $result[] = ($r->mppa_id=='0'?text_default('SEMUA'):$r->nama_ppa);
          $result[] = ($r->st_lihat?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_input?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_edit?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_hapus?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_cetak?'IZINKAN':text_danger('TIDAK'));
		  // if (UserAccesForm($user_acces_form,array('1603'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_hak_akses('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  // }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function save_assesmen(){
		if ($this->Setting_sga_model->save_assesmen()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_sga/index/1','location');
		}
	}
	
	function save_label(){
		if ($this->Setting_sga_model->save_label()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_sga/index/3','location');
		}
	}
	function simpan_param(){
		$param_id=$this->input->post('param_id');
		$data=array(
			'parameter'=>$this->input->post('parameter'),
			'staktif'=>1,
		);
		if ($param_id==''){
		    $hasil=$this->db->insert('setting_sga_param',$data);
		}else{
			$this->db->where('id',$param_id);
		    $hasil=$this->db->update('setting_sga_param',$data);
		}
		  
		  json_encode($hasil);
	}
	function load_param()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*,GROUP_CONCAT(CONCAT(M.deskripsi_nama,'  ( ',M1.ref,' )') SEPARATOR '#') as jawaban
						FROM `setting_sga_param` H
						LEFT JOIN setting_sga_param_skor M ON M.parameter_id=H.id AND M.staktif='1'
						LEFT JOIN merm_referensi M1 ON M1.nilai=M.skor AND M1.ref_head_id='255'
						where H.staktif='1'
						GROUP BY H.id
						ORDER BY H.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('parameter');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          // $result[] = $r->parameter;
		  // $jawaban='';
		  // $list_jawaban=$arr=explode(',',$r->jawaban);
		  // foreach ($list_jawaban as $index=>$val){
			  // $jawaban .= text_default($val).' ';
		  // }
          $result[] = $r->parameter.($r->jawaban?'<br>'.$this->render_jawaban($r->jawaban):'');;
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="add_jawaban('.$r->id.')" type="button" title="Add Value" class="btn btn-warning btn-xs "><i class="fa fa-plus"></i></button>';	
		  $aksi .= '<button onclick="edit_param('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_param('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_param(){
	  $param_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		// $data['deleted_by']=$this->session->userdata('user_id');
		// $data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$param_id);
		$hasil=$this->db->update('setting_sga_param',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_param(){
	  $param_id=$this->input->post('id');
	  $q="SELECT *FROM setting_sga_param H WHERE H.id='$param_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  //NILAI
  
  function simpan_nilai(){
		$nilai_id=$this->input->post('nilai_id');
		$data=array(
			'tindakan'=>$this->input->post('tindakan'),
			'nilai_parameter'=>$this->input->post('nilai_parameter'),
			'staktif'=>1,
		);
		if ($nilai_id==''){
		    $hasil=$this->db->insert('setting_sga_nilai',$data);
		}else{
			$this->db->where('id',$nilai_id);
		    $hasil=$this->db->update('setting_sga_nilai',$data);
		}
		  
		  json_encode($hasil);
	}
	function load_nilai()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*,M.ref as jawaban
						FROM `setting_sga_nilai` H
						INNER JOIN merm_referensi M ON M.nilai=H.nilai_parameter AND M.ref_head_id='255'
						where H.staktif='1'
						
						ORDER BY H.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('tindakan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
		 
          $result[] = $r->jawaban;
          $result[] = $r->tindakan;
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_nilai('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_nilai('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_nilai(){
	  $nilai_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		// $data['deleted_by']=$this->session->userdata('user_id');
		// $data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$nilai_id);
		$hasil=$this->db->update('setting_sga_nilai',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_nilai(){
	  $nilai_id=$this->input->post('id');
	  $q="SELECT *FROM setting_sga_nilai H WHERE H.id='$nilai_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  //hasil
  //NILAI
  
  function simpan_hasil(){
		$hasil_id=$this->input->post('hasil_id');
		$nilai_imt=$this->input->post('nilai_imt');
		$nilai_parameter=$this->input->post('nilai_param');
		$nilai_gizi=$this->input->post('nilai_gizi');
		if ($this->cek_duplicate_hasil($nilai_imt,$nilai_parameter,$nilai_gizi,$hasil_id)){
			$hasil=false;
		}else{
			$data=array(
				'nilai_imt'=>$this->input->post('nilai_imt'),
				'nilai_parameter'=>$this->input->post('nilai_param'),
				'nilai_gizi'=>$this->input->post('nilai_gizi'),
				'staktif'=>1,
			);
			if ($hasil_id==''){
				$hasil=$this->db->insert('setting_sga_hasil',$data);
			}else{
				$this->db->where('id',$hasil_id);
				$hasil=$this->db->update('setting_sga_hasil',$data);
			}
		}
		  
		  $this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_hasil($nilai_imt,$nilai_parameter,$nilai_gizi,$nilai_id){
		$q="SELECT *FROM setting_sga_hasil WHERE  nilai_imt='$nilai_imt' AND nilai_parameter='$nilai_parameter' AND staktif='1' AND id !='$nilai_id'";
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	function load_hasil()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*,M.ref as param,MG.ref as gizi,MB.kategori_berat as imt
						FROM `setting_sga_hasil` H
						INNER JOIN merm_referensi M ON M.nilai=H.nilai_parameter AND M.ref_head_id='255'
						INNER JOIN merm_referensi MG ON MG.nilai=H.nilai_gizi AND MG.ref_head_id='256'
						INNER JOIN mberat MB ON MB.id=H.nilai_imt
						where H.staktif='1'
						
						ORDER BY H.nilai_imt,H.nilai_parameter,H.nilai_gizi
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nilai_imt');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
		 
          $result[] = $r->imt;
          $result[] = $r->param;
          $result[] = $r->gizi;
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_hasil('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_hasil('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_hasil(){
	  $hasil_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		// $data['deleted_by']=$this->session->userdata('user_id');
		// $data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$hasil_id);
		$hasil=$this->db->update('setting_sga_hasil',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_hasil(){
	  $hasil_id=$this->input->post('id');
	  $q="SELECT *FROM setting_sga_hasil H WHERE H.id='$hasil_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
  function simpan_hak_akses(){
		$hasil=$this->cek_duplicate_hak_akses($this->input->post('profesi_id'),$this->input->post('spesialisasi_id'),$this->input->post('mppa_id'));
		if ($hasil==null){	
			$this->profesi_id=$this->input->post('profesi_id');
			$this->spesialisasi_id=$this->input->post('spesialisasi_id');
			$this->mppa_id=$this->input->post('mppa_id');
			$this->st_lihat=$this->input->post('st_lihat');
			$this->st_input=$this->input->post('st_input');
			$this->st_edit=$this->input->post('st_edit');
			$this->st_hapus=$this->input->post('st_hapus');
			$this->st_cetak=$this->input->post('st_cetak');
			
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
		
			$hasil=$this->db->insert('setting_sga_akses',$this);
		}else{
			$hasil=null;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_hak_akses($profesi_id,$spesialisasi_id,$mppa_id){
		$q="SELECT *FROM setting_sga_akses WHERE  profesi_id='$profesi_id' AND spesialisasi_id='$spesialisasi_id' AND mppa_id='$mppa_id'";
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	
  function hapus_hak_akses(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_sga_akses',$this);
	  
	  json_encode($hasil);
	  
  }
  
  function find_mppa($profesi_id){
	  $q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='$profesi_id' AND H.staktif='1'";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($profesi_id!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
  function find_poli($idtipe){
	  $q="SELECT *FROM mpoliklinik H WHERE H.idtipe='$idtipe' AND H.`status`='1'";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($idtipe!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
   function render_jawaban($jawaban){
	  $hasil='';
	  $arr=explode('#',$jawaban);
	  foreach($arr as $index=>$val){
		  $hasil .='<span class="badge ">'.$val.'</span> ';
	  }
	  return $hasil;
  }
   function load_jawaban()
	{
		$parameter_id=$this->input->post('parameter_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*,M.ref as nama_skor
							FROM `setting_sga_param_skor` H
							INNER JOIN merm_referensi M ON M.nilai=H.skor AND M.ref_head_id='255'
							where H.staktif='1' AND H.parameter_id='$parameter_id'
							ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('deskripsi_nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->nama_skor;
          $result[] = $r->deskripsi_nama;
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_jawaban('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_jawaban('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function simpan_jawaban(){
		$skor_id=$this->input->post('skor_id');
		$parameter_id=$this->input->post('parameter_id');
		$data=array(
			'parameter_id'=>$this->input->post('parameter_id'),
			'skor'=>$this->input->post('skor'),
			'deskripsi_nama'=>$this->input->post('deskripsi_nama'),
			'staktif'=>1,
		);
		if ($skor_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('setting_sga_param_skor',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$skor_id);
		    $hasil=$this->db->update('setting_sga_param_skor',$data);
		}
		  
		  json_encode($hasil);
	}
	function find_jawaban(){
	  $skor_id=$this->input->post('id');
	  $q="SELECT *FROM setting_sga_param_skor H WHERE H.id='$skor_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
	 function hapus_jawaban(){
	  $skor_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$skor_id);
		$hasil=$this->db->update('setting_sga_param_skor',$data);
	  
	  json_encode($hasil);
	  
  }
	
}
