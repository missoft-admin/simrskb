<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Treservasi_bed extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Treservasi_bed_model');
		$this->load->model('Tpendaftaran_poli_ttv_model','ttv_model');
		$this->load->helper('path');
		
  }

  function index($tab='1'){
	    $log['path_tindakan']='treservasi_bed';
		$this->session->set_userdata($log);
		get_ppa_login();
		// print_r($this->session->userdata());exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1911'))){
			$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("-14 days"));
			$date2= date_format($date2,"d/m/Y");
			// print_r($data);exit;	
			// $data['list_ruangan'] 			= $this->ttv_model->list_ruangan();
			$data['list_kelas'] 			= $this->Treservasi_bed_model->list_kelas();
			$data['list_dokter'] 			= $this->ttv_model->list_dokter();
			$data['list_ruang'] 			= $this->Treservasi_bed_model->list_ruang();
			$data['list_poli'] 			= get_all('mpoliklinik',array('status'=>1));
			// print_r($data['list_poli']);exit;
			$data['idkelas'] 			= '#';
			$data['ruangan_id'] 			= '#';
			$data['idbed'] 			= '#';
			$data['namapasien'] 			= '';
			$data['tab_detail'] 			= 1;
			$data['tab'] 			= $tab;
			
			$data['tanggal_1'] 			= $date2;
			$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
			$data['error'] 			= '';
			$data['title'] 			= 'Perawat';
			$data['content'] 		= 'Treservasi_bed/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Tindakan Poliklinik",'#'),
												  array("Perawat",'treservasi_bed')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function edit($id){
	    $data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$date2=date_create(date('Y-m-d'));
		date_add($date2,date_interval_create_from_date_string("-14 days"));
		$date2= date_format($date2,"d/m/Y");
		// print_r($pendaftaran_id);exit;
		if (UserAccesForm($user_acces_form,array('1915'))){
			$q="
			SELECT MP.id as pendaftaran_id,MP.idpasien,MP.namapasien,MP.no_medrec,CASE WHEN MP.idtipe='1' THEN 'POLIKLINIK' ELSE 'IGD' END as asal_poli 
			,P.nama as nama_poli,MP.idpoliklinik,MP.idtipe,MD.nama as nama_dokter,MP.iddokter as dpjp
			,MP.namapenanggungjawab as nama_pemohon,MP.teleponpenanggungjawab as nohp_pemohon,MP.alamatpenanggungjawab as alamat_pemohon
			,MP.iddokter as iddokter_perujuk
			FROM tpoliklinik_pendaftaran MP 
			INNER JOIN mpoliklinik P ON MP.idpoliklinik=P.id
			INNER JOIN mdokter MD ON MD.id=MP.iddokter
				WHERE MP.id='$id'
			";
			
			$q="
			SELECT 
			CASE WHEN H.idtipe='1' THEN 'POLIKLINIK' ELSE 'IGD' END as asal_poli,P.nama as nama_poli ,MD.nama as nama_dokter
			,H.*
			FROM treservasi_bed H
			INNER JOIN mpoliklinik P ON H.idpoliklinik=P.id
			INNER JOIN mdokter MD ON MD.id=H.iddokter_perujuk
			WHERE H.id='$id'
			";
			
			$data=$this->db->query($q)->row_array();
			// print_r($data);exit;
			$data['tanggal_1'] 			= $date2;
			$data['tanggal_2'] 			= date('d/m/Y');
			$data['rencana_masuk'] 			= ($data['rencana_masuk']?HumanDateShort($data['rencana_masuk']):'');
			$data['list_ruang'] 			= $this->Treservasi_bed_model->list_ruang();
			$data['list_kelas'] 			= $this->Treservasi_bed_model->list_kelas();
			$data['list_bed'] 			= $this->Treservasi_bed_model->list_bed($data['idruangan'],$data['idkelas']);
			$data['error'] 			= '';
			$data['title'] 			= 'Edit Reservasi Bed';
			$data['content'] 		= 'Treservasi_bed/manage';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Reservasi Bed",'#'),
												  array("Edit",'treservasi_bed')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
  }
	function getIndex_all()	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// tipe_pemilik:tipe_pemilik,status:status,nama:nama
		$where='';
		$tab_detail =$this->input->post('tab_detail');
		if ($tab_detail=='2'){
			$where .=" AND (H.status_reservasi) = '1'";
		}
		if ($tab_detail=='3'){
			$where .=" AND (H.status_reservasi) = '2'";
		}
		if ($tab_detail=='4'){
			$where .=" AND (H.status_reservasi) = '0'";
		}
		$idbed =$this->input->post('idbed');
		$idkelas =$this->input->post('idkelas');
		$idruangan =$this->input->post('idruangan');
		$notransaksi =$this->input->post('notransaksi');
		$no_medrec =$this->input->post('no_medrec');
		$namapasien =$this->input->post('namapasien');
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$tanggal_input_1 =$this->input->post('tanggal_input_1');
		$tanggal_input_2 =$this->input->post('tanggal_input_2');
		if ($tanggal_1!=''){
			$where .=" AND H.rencana_masuk >= '".YMDFormat($tanggal_1)."' AND H.rencana_masuk <= '".YMDFormat($tanggal_2)."'";
		}
		if ($tanggal_input_1!=''){
			$where .=" AND DATE(H.created_date) >= '".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <= '".YMDFormat($tanggal_input_2)."'";
		}
		if ($idruangan!='#'){
			$where .=" AND (H.idruangan) = '$idruangan'";
		}
		if ($idkelas!='#'){
			$where .=" AND (H.idkelas) = '$idkelas'";
		}
		if ($idbed!='#'){
			$where .=" AND (H.id) = '$idbed'";
		}
		
		if ($no_medrec!=''){
			$where .=" AND (H.no_medrec) LIKE '%".$no_medrec."%'";
		}
		if ($namapasien!=''){
			$where .=" AND (H.namapasien) LIKE '%".$namapasien."%'";
		}
		// print_r($tab);exit;
		$this->select = array();
		$from="
				(
					SELECT 
					MR.nama as nama_ruangan,MK.nama as nama_kelas,MB.nama as nama_bed,MD.nama as nama_dokter
					,H.*
					FROM treservasi_bed H
					INNER JOIN mruangan MR ON MR.id=H.idruangan
					INNER JOIN mkelas MK ON MK.id=H.idkelas
					LEFT JOIN mbed MB ON MB.id=H.idbed
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					WHERE H.id IS NOT NULL ".$where."
					ORDER BY H.rencana_masuk ASC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array();
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			
			$aksi ='<div class="btn-group btn-group-sm" role="group">';
			if ($r->status_reservasi>0){
				if ($r->status_reservasi=='1'){
					if (UserAccesForm($user_acces_form,array('1915'))){
					$aksi .= '<a href="'.base_url().'treservasi_bed/edit/'.$r->id.'" type="button" title="Edit" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';	
					}
					if (UserAccesForm($user_acces_form,array('1914'))){
					// $aksi .= '<a href="'.base_url().'treservasi_bed/edit/'.$r->id.'" type="button" title="Proses" class="btn btn-warning btn-xs"><i class="si si-arrow-right"></i></a>';	
					$aksi .= '<a href="'.base_url().'tpendaftaran_ranap/add_daftar_reservasi/'.$r->id.'"  type="button" title="Proses" class="btn btn-warning btn-xs"><i class="si si-arrow-right"></i></a>';	
					// $aksi .= '<a href="'.base_url().'tpendaftaran_ranap/add_daftar_reservasi/'.$r->id.'"  type="button" title="Proses" class="btn btn-primary btn-xs"><i class="si si-arrow-right"></i> Daftarkan</a>';	
					}
					if (UserAccesForm($user_acces_form,array('1916'))){
					$aksi .= '<button type="button" onclick="batal('.$r->id.')" title="Batalkan" class="btn btn-danger btn-xs"><i class="si si-close"></i></a>';	
					}
				}
				if (UserAccesForm($user_acces_form,array('1917'))){
				$aksi .= '<button type="button" title="Cetak" class="btn btn-success btn-xs"><i class="si si-printer"></i></a>';	
				}
			}
			$aksi .='</div>';
			$result = array();
			$result[] =$no;
			$result[] =HumanDateLong($r->created_date);
			$result[] =$r->no_medrec.'<br>'.$r->namapasien;
			$result[] =$r->nama_ruangan;
			$result[] =$r->nama_kelas;
			$result[] =($r->idbed==0?text_default('TIDAK DITENTUKAN'):$r->nama_bed);
			$result[] =HumanDateShort($r->rencana_masuk);
			$result[] =$r->nama_dokter;
			$result[] =($r->prioritas==1?text_danger('CITO'):text_info('NON CITO'));
			$result[] =Status_Reservasi_Bed($r->status_reservasi);
			$result[] =$aksi;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
  }
  function getIndexPoli()	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$idtipe =$this->input->post('idtipe');
		$idpoli =$this->input->post('idpoli');
		$iddokter =$this->input->post('iddokter');
		$notransaksi =$this->input->post('notransaksi');
		$no_medrec =$this->input->post('no_medrec');
		$namapasien =$this->input->post('namapasien');

		if ($tanggal_1!=''){
			$where .=" AND H.tanggal >= '".YMDFormat($tanggal_1)."' AND H.tanggal <= '".YMDFormat($tanggal_2)."'";
		}
		if ($idtipe!='#'){
			$where .=" AND (H.idtipe) = '$idtipe'";
		}
		if ($idpoli!='#'){
			$where .=" AND (H.idpoliklinik) = '$idpoli'";
		}
		if ($iddokter!='#'){
			$where .=" AND (H.iddokter) = '$iddokter'";
		}
		if ($notransaksi!=''){
			$where .=" AND (H.nopendaftaran) LIKE '%".$notransaksi."%'";
		}
		if ($no_medrec!=''){
			$where .=" AND (H.no_medrec) LIKE '%".$no_medrec."%'";
		}
		if ($namapasien!=''){
			$where .=" AND (H.namapasien) LIKE '%".$namapasien."%'";
		}

		// print_r($tab);exit;
		$this->select = array();
		$from="
				(
					SELECT H.*,MD.nama as nama_dokter,M.nama as nama_poli_asal FROM tpoliklinik_pendaftaran H
					INNER JOIN mdokter MD ON MD.id=H.iddokter
					LEFT JOIN mpoliklinik M oN M.id=H.idpoliklinik
					WHERE H.id IS NOT NULL ".$where."
					ORDER BY H.tanggal DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array();
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$aksi='';
			if (UserAccesForm($user_acces_form,array('1912'))){
			$aksi .= '<a href="'.base_url().'treservasi_bed/proses_rj/'.$r->id.'" type="button" title="Proses Reservasi Bed" class="btn btn-success btn-xs"><i class="si si-cursor"></i> Reserv</a>';	
			}
			$result = array();
			$result[] =$no;
			$result[] =$aksi;
			$result[] =$r->nopendaftaran;
			$result[] =$r->tanggaldaftar;
			$result[] =$r->no_medrec;
			$result[] =$r->namapasien;
			$result[] =$r->nama_dokter;
			$result[] =GetTipePasienPiutang($r->idtipe).'<br>'.$r->nama_poli_asal;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
  }
  function getIndexPoliCari()	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$idpasien =$this->input->post('idpasien');
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$notransaksi =$this->input->post('notransaksi');

		if ($tanggal_1!=''){
			$where .=" AND H.tanggal >= '".YMDFormat($tanggal_1)."' AND H.tanggal <= '".YMDFormat($tanggal_2)."'";
		}
		
		if ($notransaksi!=''){
			$where .=" AND (H.nopendaftaran) LIKE '%".$notransaksi."%'";
		}
		
		// print_r($tab);exit;
		$this->select = array();
		$from="
				(
					SELECT H.*,MD.nama as nama_dokter,M.nama as nama_poli_asal FROM tpoliklinik_pendaftaran H
					INNER JOIN mdokter MD ON MD.id=H.iddokter
					LEFT JOIN mpoliklinik M oN M.id=H.idpoliklinik
					WHERE H.idpasien='$idpasien' ".$where."
					ORDER BY H.tanggal DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array();
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$aksi='';
			$aksi .= '<button onclick="get_data_pendaftaran('.$r->id.')" type="button" title="Pilih" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Pilih</button>';	
			$result = array();
			$result[] =$no;
			$result[] =$aksi;
			$result[] =$r->nopendaftaran;
			$result[] =$r->tanggaldaftar;
			$result[] =$r->no_medrec;
			$result[] =$r->namapasien;
			$result[] =$r->nama_dokter;
			$result[] =GetTipePasienPiutang($r->idtipe).'<br>'.$r->nama_poli_asal;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
  }
  function get_data_pendaftaran(){
	  $pendaftaran_id=$this->input->post('id');
		$q="
		SELECT MP.id as pendaftaran_id,MP.idpasien,MP.namapasien,MP.no_medrec,MP.nopendaftaran,CASE WHEN MP.idtipe='1' THEN 'POLIKLINIK' ELSE 'IGD' END as asal_poli 
		,P.nama as nama_poli,MP.idpoliklinik,MP.idtipe,MD.nama as nama_dokter,MP.iddokter as dpjp
		,MP.namapenanggungjawab as nama_pemohon,MP.teleponpenanggungjawab as nohp_pemohon,MP.alamatpenanggungjawab as alamat_pemohon
		,MP.iddokter as iddokter_perujuk
		FROM tpoliklinik_pendaftaran MP 
		INNER JOIN mpoliklinik P ON MP.idpoliklinik=P.id
		INNER JOIN mdokter MD ON MD.id=MP.iddokter
			WHERE MP.id='$pendaftaran_id'
		";
		$data=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($data));
  }
  function get_data_perencanaan(){
	  $perencanaan_id=$this->input->post('id');
		$q="
			SELECT MP.id as pendaftaran_id,MP.idpasien,MP.namapasien,MP.nopendaftaran,MP.no_medrec,CASE WHEN MP.idtipe='1' THEN 'POLIKLINIK' ELSE 'IGD' END as asal_poli 
			,P.nama as nama_poli,MP.idpoliklinik,MP.idtipe,MD.nama as nama_dokter,H.dpjp
			,MP.namapenanggungjawab as nama_pemohon,MP.teleponpenanggungjawab as nohp_pemohon,MP.alamatpenanggungjawab as alamat_pemohon
			,MP.iddokter as iddokter_perujuk,H.dengan_diagnosa as diagnosa,H.nopermintaan,H.assesmen_id as perencanaan_id,H.tanggal_masuk as rencana_masuk
			FROM tpoliklinik_ranap_perencanaan H 
			INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
			INNER JOIN mpoliklinik P ON MP.idpoliklinik=P.id
			INNER JOIN mdokter MD ON MD.id=MP.iddokter
				WHERE H.assesmen_id='$perencanaan_id' AND H.status_assemen='2'
			";
		$data=$this->db->query($q)->row_array();
		$data['diagnosa']=strip_tags($data['diagnosa']);
		$this->output->set_output(json_encode($data));
  }
  function getIndexRencana()	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$idtipe =$this->input->post('idtipe');
		$idpoli =$this->input->post('idpoli');
		$iddokter =$this->input->post('iddokter');
		$notransaksi =$this->input->post('notransaksi');
		$no_medrec =$this->input->post('no_medrec');
		$namapasien =$this->input->post('namapasien');

		if ($tanggal_1!=''){
			$where .=" AND DATE(H.created_date) >= '".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <= '".YMDFormat($tanggal_2)."'";
		}
		if ($idtipe!='#'){
			$where .=" AND (H.idtipe) = '$idtipe'";
		}
		if ($idpoli!='#'){
			$where .=" AND (H.idpoliklinik) = '$idpoli'";
		}
		if ($iddokter!='#'){
			$where .=" AND (MP.iddokter) = '$iddokter'";
		}
		if ($notransaksi!=''){
			$where .=" AND (H.nopermintaan) LIKE '%".$notransaksi."%'";
		}
		if ($no_medrec!=''){
			$where .=" AND (MP.no_medrec) LIKE '%".$no_medrec."%'";
		}
		if ($namapasien!=''){
			$where .=" AND (MP.namapasien) LIKE '%".$namapasien."%'";
		}

		// print_r($tab);exit;
		$this->select = array();
		$from="
				(
					SELECT MP.nopendaftaran,MP.tanggal,MD.nama as nama_dokter,MP.idtipe as idtipe_poli,M.nama as nama_poli_asal,H.* 
					FROM tpoliklinik_ranap_perencanaan H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN mppa MD ON MD.id=H.iddokter_peminta
					LEFT JOIN mpoliklinik M oN M.id=MP.idpoliklinik
					WHERE H.status_assemen='2' ".$where."
					ORDER BY H.tanggal_input DESC 
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array();
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$aksi='';
			if (UserAccesForm($user_acces_form,array('1913'))){
			$aksi .= '<a href="'.base_url().'treservasi_bed/proses_rencana/'.$r->assesmen_id.'/'.$r->pendaftaran_id.'" type="button" title="Proses Reservasi Bed" class="btn btn-success btn-xs"><i class="si si-cursor"></i> Reserv</a>';	
			}
			$result = array();
			$result[] =$no;
			$result[] =$aksi;
			$result[] =$r->nopermintaan.'<br>'.HumanDateLong($r->created_date);
			$result[] =GetTipePasienPiutang($r->tipe);
			$result[] =$r->nopendaftaran.'<br>'.HumanDateShort($r->tanggal);
			$result[] =$r->nomedrec_pasien.'<br>'.$r->nama_pasien;
			$result[] =$r->nama_dokter;
			$result[] =GetTipePasienPiutang($r->idtipe_poli).'<br>'.$r->nama_poli_asal;
			$result[] =$r->dengan_diagnosa;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
  }
  function getIndexRencanaCari()	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$idpasien =$this->input->post('idpasien');
		$nopermintaan =$this->input->post('nopermintaan');
		
		if ($nopermintaan!=''){
			$where .=" AND (H.nopermintaan) LIKE '%".$nopermintaan."%'";
		}
		if ($tanggal_1!=''){
			$where .=" AND DATE(H.created_date) >= '".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <= '".YMDFormat($tanggal_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT MP.nopendaftaran,MP.tanggal,MD.nama as nama_dokter,MP.idtipe as idtipe_poli,M.nama as nama_poli_asal,H.* 
					FROM tpoliklinik_ranap_perencanaan H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN mppa MD ON MD.id=H.iddokter_peminta
					LEFT JOIN mpoliklinik M oN M.id=MP.idpoliklinik
					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
					ORDER BY H.tanggal_input DESC 
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array();
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$aksi='';
			$aksi .= '<button onclick="get_data_perencanaan('.$r->assesmen_id.')" type="button" title="Pilih" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Pilih</button>';
			$result = array();
			$result[] =$no;
			$result[] =$aksi;
			$result[] =$r->nopermintaan.'<br>'.HumanDateLong($r->created_date);
			$result[] =GetTipePasienPiutang($r->tipe);
			$result[] =$r->nopendaftaran.'<br>'.HumanDateShort($r->tanggal);
			$result[] =$r->nomedrec_pasien.'<br>'.$r->nama_pasien;
			$result[] =$r->nama_dokter;
			$result[] =GetTipePasienPiutang($r->idtipe_poli).'<br>'.$r->nama_poli_asal;
			$result[] =$r->dengan_diagnosa;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
  }
  function proses_rj($pendaftaran_id){
	    $data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$date2=date_create(date('Y-m-d'));
		date_add($date2,date_interval_create_from_date_string("-14 days"));
		$date2= date_format($date2,"d/m/Y");
		// print_r($pendaftaran_id);exit;
		if (UserAccesForm($user_acces_form,array('1912'))){
			$q="
			SELECT MP.id as pendaftaran_id,MP.idpasien,MP.namapasien,MP.no_medrec,MP.nopendaftaran,CASE WHEN MP.idtipe='1' THEN 'POLIKLINIK' ELSE 'IGD' END as asal_poli 
			,P.nama as nama_poli,MP.idpoliklinik,MP.idtipe,MD.nama as nama_dokter,MP.iddokter as dpjp
			,MP.namapenanggungjawab as nama_pemohon,MP.teleponpenanggungjawab as nohp_pemohon,MP.alamatpenanggungjawab as alamat_pemohon
			,MP.iddokter as iddokter_perujuk
			FROM tpoliklinik_pendaftaran MP 
			INNER JOIN mpoliklinik P ON MP.idpoliklinik=P.id
			INNER JOIN mdokter MD ON MD.id=MP.iddokter
				WHERE MP.id='$pendaftaran_id'
			";
			$data=$this->db->query($q)->row_array();
			$data['tanggal_1'] 			= $date2;
			$data['tanggal_2'] 			= date('d/m/Y');
			$data['tipe_pemohon'] 			= '1';
			$data['asal_reservasi'] 			= '1';
			$data['nopermintaan'] 			= '';
			$data['perencanaan_id'] 			= '';
			$data['diagnosa'] 			= '';
			$data['idruangan'] 			= '';
			$data['idkelas'] 			= '';
			$data['idbed'] 			= '';
			$data['prioritas'] 			= '';
			$data['rencana_masuk'] 			= '';
			$data['idruangan_alter'] 			= '';
			$data['idkelas_alter'] 			= '';
			$data['idbed_alter'] 			= '';
			
			$data['alamat_pemohon'] 			= '';
			$data['catatan'] 			= '';
			$data['list_ruang'] 			= $this->Treservasi_bed_model->list_ruang();
			$data['list_kelas'] 			= $this->Treservasi_bed_model->list_kelas();
			$data['list_bed'] 			= $this->Treservasi_bed_model->list_bed();
			$data['id'] 			= '';
			$data['error'] 			= '';
			$data['title'] 			= 'Proses Reservasi Bed';
			$data['content'] 		= 'Treservasi_bed/manage';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Reservasi Bed",'#'),
												  array("Tambah",'treservasi_bed')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
  }
  
  function proses_rencana($perencanaan_id,$pendaftaran_id){
	    $data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// print_r($pendaftaran_id);exit;
		if (UserAccesForm($user_acces_form,array('1913'))){
			$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("-14 days"));
			$date2= date_format($date2,"d/m/Y");
			$q="
			SELECT MP.id as pendaftaran_id,MP.idpasien,MP.namapasien,MP.nopendaftaran,MP.no_medrec,CASE WHEN MP.idtipe='1' THEN 'POLIKLINIK' ELSE 'IGD' END as asal_poli 
			,P.nama as nama_poli,MP.idpoliklinik,MP.idtipe,MD.nama as nama_dokter,H.dpjp
			,MP.namapenanggungjawab as nama_pemohon,MP.teleponpenanggungjawab as nohp_pemohon,MP.alamatpenanggungjawab as alamat_pemohon
			,MP.iddokter as iddokter_perujuk,H.dengan_diagnosa as diagnosa,H.nopermintaan,H.assesmen_id as perencanaan_id,H.tanggal_masuk as rencana_masuk
			FROM tpoliklinik_ranap_perencanaan H 
			INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
			INNER JOIN mpoliklinik P ON MP.idpoliklinik=P.id
			INNER JOIN mdokter MD ON MD.id=MP.iddokter
				WHERE H.assesmen_id='$perencanaan_id'
			";
			$data=$this->db->query($q)->row_array();
			$data['tanggal_1'] 			= $date2;
			$data['tanggal_2'] 			= date('d/m/Y');
			$data['rencana_masuk'] 			= ($data['rencana_masuk']?HumanDateShort($data['rencana_masuk']):'');
			$data['tipe_pemohon'] 			= '1';
			$data['asal_reservasi'] 			= '2';
			$data['idruangan'] 			= '';
			$data['idkelas'] 			= '';
			$data['idbed'] 			= '';
			$data['prioritas'] 			= '';
			$data['idruangan_alter'] 			= '';
			$data['idkelas_alter'] 			= '';
			$data['idbed_alter'] 			= '';
			
			$data['alamat_pemohon'] 			= '';
			$data['catatan'] 			= '';
			$data['list_ruang'] 			= $this->Treservasi_bed_model->list_ruang();
			$data['list_kelas'] 			= $this->Treservasi_bed_model->list_kelas();
			$data['list_bed'] 			= $this->Treservasi_bed_model->list_bed();
			$data['id'] 			= '';
			$data['error'] 			= '';
			$data['title'] 			= 'Proses Reservasi Bed';
			$data['content'] 		= 'Treservasi_bed/manage';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Reservasi Bed",'#'),
												  array("Tambah",'treservasi_bed')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
  }
	function get_bed(){
		$where='';
		$idruangan=$this->input->post('idruangan');
		$idkelas=$this->input->post('idkelas');
		if ($idruangan!='#'){
			$where .=" AND H.idruangan='$idruangan'";
		}
		if ($idkelas!='#'){
			$where .=" AND H.idkelas='$idkelas'";
		}
		$q="SELECT H.*,MK.nama as nama_kelas 
		FROM mbed H
		LEFT JOIN mkelas MK ON MK.id=H.idkelas 
		WHERE H.`status`='1' ".$where." ORDER BY H.id ASC";
		$list_data=$this->db->query($q)->result();
		$opsi='';
		if ($list_data){
			foreach ($list_data as $r){
				$opsi .='<option value='.$r->id.'>'.$r->nama_kelas.' - '.$r->nama.'</option>';
			}
		}
		$data['detail']=$opsi;
		$this->output->set_output(json_encode($data));
	}
	function batal(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$where='';
		$id=$this->input->post('id');
		$data=array(
			'status_reservasi' => 0,
			'deleted_ppa' => $login_ppa_id,
			'deleted_date' => date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		$this->db->update('treservasi_bed',$data);
		$this->output->set_output(json_encode($data));
	}
	function get_fasilitas(){
		$where='';
		$mfasilitas_id=$this->input->post('mfasilitas_id');
		
		$q="SELECT H.*,M.nama as nama_fasilitas FROM `mfasilitas_bed_foto` H
			INNER JOIN mfasilitas_bed M ON M.id=H.mfasilitas_id
			WHERE H.mfasilitas_id='$mfasilitas_id'";
		$list_data=$this->db->query($q)->result();
		$opsi='';
		$uploadDir = './assets/upload/foto_fasilitas/';
		if ($list_data){
			foreach ($list_data as $r){
				$file_gambar = base_url().'assets/upload/foto_fasilitas/'.$r->filename;
				$opsi .='<div class="col-lg-6 animated fadeIn">
								<div class="img-container">
									<img class="img-responsive" src="'.$uploadDir.$r->filename.'" alt="">
									<div class="img-options">
										<div class="img-options-content">
											<h3 class="font-w400 text-white push-5">'.$r->nama_fasilitas.'</h3>
											<h4 class="h6 font-w400 text-white-op push-15">'.$r->size.'</h4>
											<a class="btn btn-sm btn-default" href="'.$file_gambar.'" target="_blank"><i class="fa fa-eye"></i> Lihat</a>
										</div>
									</div>
								</div>
							</div>';
			}
		}
		$data['detail']=$opsi;
		$this->output->set_output(json_encode($data));
	}
	function simpan(){
		$id = $this->input->post('id');
		// print_r($id);exit;
		if ($id == '') {
				$id=$this->Treservasi_bed_model->saveData();
				// print_r($id);exit;
				if ($id) {
					$data = [];
					$data['idpendaftaran'] = $id;
					$data = array_merge($data, backend_info());
					 $this->session->set_flashdata('confirm', true);
					$this->session->set_flashdata('message_flash', 'Sudah Tersimpan !');
					if ($btn_simpan=='1'){
						redirect('treservasi_bed','location');	
					}else{
						redirect('treservasi_bed','location');	
					}
				}
			} else {
				if ($this->Treservasi_bed_model->updateData($id)) {
					$data = [];
					// $data['idpendaftaran'] = $id;
					$data = array_merge($data, backend_info());
					 $this->session->set_flashdata('confirm', true);
					$this->session->set_flashdata('message_flash', 'Sudah Tersimpan !');
					if ($btn_simpan=='1'){
						redirect('treservasi_bed','location');	
						
					}else{
						
						redirect('treservasi_bed','location');	
					}
				}
			}
	}
}	
