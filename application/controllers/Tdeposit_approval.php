<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tdeposit_approval extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tdeposit_approval_model');
		$this->load->model('Tpendaftaran_poli_pasien_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->helper('path');
		
  }
	function index($tab='1'){
		$log['path_tindakan']='tdeposit_approval';
		$this->session->set_userdata($log);
		$user_id=$this->session->userdata('user_id');
		// print_r($this->session->userdata('path_tindakan'));exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$q="SELECT H.id as login_mppa_id,H.pegawai_id as login_iddokter,H.pegawai_id as iddokter FROM mppa H WHERE H.user_id='$user_id' AND H.tipepegawai='2' AND H.staktif='1'";
		// $login_iddokter=$this->db->query($q)->row('login_iddokter');	
		if (UserAccesForm($user_acces_form,array('2609'))){
			$data = $this->db->query($q)->row_array();	
			if ($data){
				
				$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("-30 days"));
			$date2= date_format($date2,"d/m/Y");
				
				$data['idpoli'] 			= '#';
				$data['namapasien'] 			= '';
				$data['tab'] 			= $tab;
				
				$data['tanggal_1'] 			= $date2;
				$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
				$data['error'] 			= '';
				$data['title'] 			= 'Approval Pembatalan Deposit';
				$data['content'] 		= 'Tdeposit_approval/index';
				$data['breadcrum'] 	= array(
													  array("RSKB Halmahera",'#'),
													  array("Rawan Inap & ODS Deposit",'#'),
													  array("Approval Pembatalan Deposit",'tdeposit_approval')
													);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				redirect('page404');
			}
		}else{
			redirect('page404');
		}
	}
	
	function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$iduser=$this->session->userdata('user_id');		
			
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$no_medrec =$this->input->post('no_medrec');
			$nama_pasien =$this->input->post('nama_pasien');
			$tab =$this->input->post('tab');
			if ($tab=='1'){
				$where .=" AND H.st_approval = '0' ";
				$where .=" AND A.st_aktif='1' AND A.approve='0' ";
			}
			if ($tab=='2'){
				$where .=" AND  H.st_approval <> '0' ";
			}
			if ($tab=='3'){
				$where .="  AND H.st_approval='1' ";
			}
			if ($tab=='4'){
				$where .="  AND H.st_approval='2' ";
			}
			if ($tab=='5'){
				// $where .=" AND A.st_aktif='1' AND A.approve='0' ";
			}
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.deleted_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.deleted_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(H.deleted_date) >='".date('Y-m-d')."'";
			}
			if ($no_medrec!=''){
				$where .=" AND (MH.no_medrec LIKE '%".$no_medrec."%')";
			}
			if ($nama_pasien!=''){
				$where .=" AND (MH.namapasien LIKE '%".$nama_pasien."%')";
			}
			// if ($tab=='2'){
				// // $where .=" AND ((TK.st_jawab = '0' AND TK.iddokter_awal='$iddokter') OR (TK.st_jawab_baru = '0' AND TK.iddokter_baru='$iddokter') )";
			// }
			// if ($tab=='3'){
				// // $where .=" AND ((TK.st_jawab = '1' AND TK.iddokter_awal='$iddokter') OR (TK.st_jawab_baru = '1' AND TK.iddokter_baru='$iddokter') )";
			// }
			
			// print_r($tab);exit;
			$this->select = array();
			if ($tab=='1' || $tab=='5'){
				$from="
					(
						SELECT DH.tipe_deposit,DH.deskripsi,MJ.nama as jenis_deposit,MH.no_medrec,MH.namapasien
						,MU.`name` as nama_user,MC.`name` as nama_user_created,H.* 
						,(CASE
							WHEN H.idmetodepembayaran = 1 THEN 'Tunai'
							WHEN H.idmetodepembayaran = 2 THEN 'Debit'
							WHEN H.idmetodepembayaran = 3 THEN 'Kredit'
							WHEN H.idmetodepembayaran = 4 THEN 'Transfer'
						END) AS metodepembayaran,MB.nama as nama_bank,A.step
						FROM `trawatinap_deposit` H
						LEFT JOIN musers MU ON MU.id=H.iduserdelete
						LEFT JOIN musers MC ON MC.id=H.iduserinput
						INNER JOIN trawatinap_deposit_head DH ON DH.id=H.head_id
						LEFT JOIN mdeposit_jenis MJ ON MJ.id=H.jenis_deposit_id
						INNER JOIN trawatinap_pendaftaran MH ON MH.id=H.idrawatinap
						LEFT JOIN trawatinap_deposit_approval A ON A.deposit_id=H.id  
						LEFT JOIN mbank MB ON MB.id=H.idbank
						WHERE H.hapus_proses='1' AND A.iduser='$iduser'  ".$where."
						GROUP BY H.id
						ORDER BY H.created_date DESC, A.step DESC
					) as tbl
				";
			}else{
				$from="
					(
						SELECT DH.tipe_deposit,DH.deskripsi,MJ.nama as jenis_deposit,MH.no_medrec,MH.namapasien
						,MU.`name` as nama_user,MC.`name` as nama_user_created,H.* 
						,(CASE
							WHEN H.idmetodepembayaran = 1 THEN 'Tunai'
							WHEN H.idmetodepembayaran = 2 THEN 'Debit'
							WHEN H.idmetodepembayaran = 3 THEN 'Kredit'
							WHEN H.idmetodepembayaran = 4 THEN 'Transfer'
						END) AS metodepembayaran,MB.nama as nama_bank,A.step
						FROM `trawatinap_deposit` H
						LEFT JOIN musers MU ON MU.id=H.iduserdelete
						LEFT JOIN musers MC ON MC.id=H.iduserinput
						INNER JOIN trawatinap_deposit_head DH ON DH.id=H.head_id
						LEFT JOIN mdeposit_jenis MJ ON MJ.id=H.jenis_deposit_id
						INNER JOIN trawatinap_pendaftaran MH ON MH.id=H.idrawatinap
						LEFT JOIN trawatinap_deposit_approval A ON A.deposit_id=H.id  
						LEFT JOIN mbank MB ON MB.id=H.idbank
						WHERE H.hapus_proses='1' AND A.iduser='$iduser'  ".$where."
						GROUP BY H.id
					) as tbl
				";
			}
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('deskripsi','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
		  $respon='';
		  $action='';
		  // $action='
				// <div class="btn-group btn-group-xs" role="group">
					
				// </div>
			// ';
		   $btn_detail=' <button class="btn btn-default btn-xs" title="Lihat Detail" onclick="lihat_detail('.$r->id.')" type="button"><i class="fa fa-eye"></i></button>';
		   $btn_trx= '<button onclick="show_modal_deposit_header_add('.$r->idrawatinap.','.$r->head_id.')"  type="button" data-toggle="tooltip" title="TRANSAKSI DEPOSIT"  class="btn btn-warning btn-xs"><i class="fa fa-list-ul"></i> </button>';
          $result = array();
		  $query=$this->get_respon($r->id,$r->step);
		  if ($query){
			foreach ($query as $res){
		    $btn_approval='<button class="btn btn-danger btn-xs" title="Lihat Proses Approval" onclick="list_user('.$res->deposit_id.')" type="button"><i class="si si-user-following"></i> STEP '.$res->step.'</button>';
				if ($res->iduser==$iduser && $res->step==$r->step){
					if ($res->approve=='0'){
					  $action .= '<button title="Setuju" class="btn btn-primary btn-xs setuju" data-id="'.$res->id.'"><i class="fa fa-check"></i> Setuju</button> ';
					  $action .= '<button title="Tolak" class="btn btn-danger btn-xs tolak" data-id="'.$res->id.'"><i class="si si-ban"></i></button>';
					}else{

					  // $action .= '<button title="Batalkan Pesetujuan / Tolak" class="btn btn-primary btn-xs batal" data-id="'.$res->id.'"><i class="fa fa-refresh"></i></button>';
					}
				}
				// if ($res->iduser==$iduser){
					$respon .=$btn_approval.' '.status_approval($res->approve).'<br><br>';
				// }
			}
		  }
		  if ($action==''){
			 $action=status_approval($r->st_approval);
		  }
		  $result[]=$action.$btn_detail.$btn_trx;
		  $result[]=$respon;
		  $result[]=$r->alasanhapus;
		  $result[]=$r->nama_user.'<br>'.HumanDateLong($r->deleted_date);
		  $result[]='<td class="text-center">'.($r->tipe_deposit=='1'?text_warning('PEMBEDAHAN'):text_success('KONSERVATIF')).'<br>'.$r->deskripsi.'</td>';
		  $result[]=$r->no_medrec.'<br>'.($r->namapasien);
		  $result[]=$r->nodeposit.'<br>'.($r->nama_user_created).'<br>'.HumanDateLong($r->created_date);
		  $result[]=$r->jenis_deposit.'<br>'.HumanDateShort($r->tanggal);
		  $result[]=$r->metodepembayaran.($r->nama_bank?'<br>'.$r->nama_bank:'');
		  $result[]=number_format($r->nominal,0);
		  $result[]='';
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function setuju_batal($id,$status){
		// $arr=array();
		$q="call update_approval_deposit('$id', $status) ";
		$result=$this->db->query($q);
		// $arr['st_insert']=$result;
		// $arr=$this->db->query("SELECT H.st_bendahara FROM tkasbon H  WHERE H.id='$idrka' AND H.status='2'")->row('st_bendahara');
		$this->output->set_output(json_encode($result));
	}
  function get_respon($deposit_id,$step){
	  $iduser=$this->session->userdata('user_id');
	  $q="SELECT *from trawatinap_deposit_approval AP
			WHERE AP.st_aktif='1' AND AP.deposit_id='$deposit_id'";
	  return $this->db->query($q)->result();
  }
 function tolak(){
		$id=$this->input->post('id_approval');
		$alasan_tolak=$this->input->post('alasan_tolak');
		$q="call update_approval_deposit('$id', 2) ";
		$this->db->query($q);
		$result=$this->db->update('trawatinap_deposit_approval',array('alasan_tolak'=>$alasan_tolak),array('id'=>$id));
		echo json_encode($result);
	}
}	
