<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_gaji extends CI_Controller {

	/**
	 * Setting Jurnal Rekap Gaji controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_gaji_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_gaji_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_gaji_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Rekap Gaji';
		$data['content'] 		= 'Msetting_jurnal_gaji/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Rekap Gaji",'#'),
									    			array("List",'mlogic_gaji')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function list_kategori($idtipe){
		if ($idtipe=='1'){
			$q="SELECT * FROM mdokter_kategori M ORDER BY M.nama ASC";
		}else{
			$q="SELECT * FROM mpegawai_kategori M ORDER BY M.nama ASC";
			
		}
		$opsi='';
		if ($idtipe !='#'){
			
			$row=$this->db->query($q)->result();
			foreach($row as $r){
				$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
			}
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	
	function update(){
		$id_edit=1;
		$data=array(
			'st_auto_posting'=>$this->input->post('st_auto_posting'),
			// 'idakun_kredit'=>$this->input->post('idakun_kredit'),
			'batas_batal'=>$this->input->post('batas_batal'),
		);
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_gaji',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	//Rekap Gaji
	function load_gaji()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.id,H.idjenis,H.idvariable,H.idakun
				,J.nama as jenis,V.nama as variable,CONCAT(A.noakun,' - ',A.namaakun) as akun
				FROM `msetting_jurnal_gaji_detail` H
				LEFT JOIN mjenis_gaji J ON J.id=H.idjenis
				LEFT JOIN mvariable V ON V.id=H.idvariable
				LEFT JOIN makun_nomor A ON A.id=H.idakun
				ORDER BY H.idjenis,H.idvariable,H.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','akun','jenis','variable');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = ($r->jenis);
           $row[] = ($r->idvariable=='0'?text_default('All Variable'):$r->variable);
            $row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_gaji('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_gaji(){
		$id_edit=$this->input->post('id_edit');
		$idjenis=($this->input->post('idjenis')=='#'?0:$this->input->post('idjenis'));
		$idvariable=($this->input->post('idvariable')=='#'?0:$this->input->post('idvariable'));
		$data=array(
			'setting_id'=>1,
			'idjenis'=>$idjenis,
			'idvariable'=>$idvariable,
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_gaji($idjenis,$idvariable)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_gaji_detail',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_gaji($idjenis,$idvariable){
		$gabung=$idjenis.'-'.$idvariable;
		$q="SELECT *FROM msetting_jurnal_gaji_detail S
			WHERE CONCAT(S.idjenis,'-',S.idvariable)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_gaji($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_gaji_detail');
		echo json_encode($result);
	}
	
}
