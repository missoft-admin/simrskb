<?php

class term_radiologi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->model('Term_radiologi_model', 'model');
    }

    public function check_setting_expertise_pemeriksaan() {
        $tipe_layanan = $this->input->post('tipe_layanan');
        $pemeriksaan_id = $this->input->post('pemeriksaan_id');
        $asal_pasien = $this->input->post('asal_pasien');
        $poliklinik_kelas = $this->input->post('poliklinik_kelas');
        $dokter_radiologi = $this->input->post('dokter_radiologi');

        $setting_expertise_pemeriksaan = $this->model->check_setting_expertise_pemeriksaan($tipe_layanan, $pemeriksaan_id, $asal_pasien, $poliklinik_kelas, $dokter_radiologi);
        echo json_encode($setting_expertise_pemeriksaan);
    }

    public function preview_photo($file_id, $tipe, $pemeriksaan_id)
    {
        if ($tipe == 1) {
            $row = get_by_field('id', $file_id, 'term_radiologi_xray_pemeriksaan_file');
            $photo_url = base_url().'assets/upload/foto_radiologi_xray/';
            $list_photo = getwhere('pemeriksaan_id', $pemeriksaan_id, 'term_radiologi_xray_pemeriksaan_file')->result();
        } else if ($tipe == 2) {
            $row = get_by_field('id', $file_id, 'term_radiologi_usg_pemeriksaan_file');
            $photo_url = base_url().'assets/upload/foto_radiologi_usg/';
            $list_photo = getwhere('pemeriksaan_id', $pemeriksaan_id, 'term_radiologi_usg_pemeriksaan_file')->result();
        } else if ($tipe == 3) {
            $row = get_by_field('id', $file_id, 'term_radiologi_ctscan_pemeriksaan_file');
            $photo_url = base_url().'assets/upload/foto_radiologi_ctscan/';
            $list_photo = getwhere('pemeriksaan_id', $pemeriksaan_id, 'term_radiologi_ctscan_pemeriksaan_file')->result();
        } else if ($tipe == 4) {
            $row = get_by_field('id', $file_id, 'term_radiologi_mri_pemeriksaan_file');
            $photo_url = base_url().'assets/upload/foto_radiologi_mri/';
            $list_photo = getwhere('pemeriksaan_id', $pemeriksaan_id, 'term_radiologi_mri_pemeriksaan_file')->result();
        } else if ($tipe == 5) {
            $row = get_by_field('id', $file_id, 'term_radiologi_lainnya_pemeriksaan_file');
            $photo_url = base_url().'assets/upload/foto_radiologi_lainnya/';
            $list_photo = getwhere('pemeriksaan_id', $pemeriksaan_id, 'term_radiologi_lainnya_pemeriksaan_file')->result();
        }
    
        $current_photo = $photo_url . $row->filename;
        $current_photo_filename = $row->filename;
        $current_photo_uploaded_at = HumanDateLong($row->upload_at);

        $data = [
            'tipe' => $tipe,
            'photo_url' => $photo_url,
            'current_photo' => $current_photo,
            'current_photo_filename' => $current_photo_filename,
            'current_photo_uploaded_at' => $current_photo_uploaded_at,
            'list_photo' => $list_photo,
        ];

        $this->load->view('Term_radiologi/photo_viewer', $data);
    }

    public function preview_dicom($file_id, $tipe)
    {
        if ($tipe == 1) {
            $row = get_by_field('id', $file_id, 'term_radiologi_xray_pemeriksaan_file');
            $url = base_url().'assets/upload/foto_radiologi_xray/' . $row->filename;
        } else if ($tipe == 2) {
            $row = get_by_field('id', $file_id, 'term_radiologi_usg_pemeriksaan_file');
            $url = base_url().'assets/upload/foto_radiologi_usg/' . $row->filename;
        } else if ($tipe == 3) {
            $row = get_by_field('id', $file_id, 'term_radiologi_ctscan_pemeriksaan_file');
            $url = base_url().'assets/upload/foto_radiologi_ctscan/' . $row->filename;
        } else if ($tipe == 4) {
            $row = get_by_field('id', $file_id, 'term_radiologi_mri_pemeriksaan_file');
            $url = base_url().'assets/upload/foto_radiologi_mri/' . $row->filename;
        } else if ($tipe == 5) {
            $row = get_by_field('id', $file_id, 'term_radiologi_lainnya_pemeriksaan_file');
            $url = base_url().'assets/upload/foto_radiologi_lainnya/' . $row->filename;
        }

        $data = [
            'url' => $url,
        ];

        $this->load->view('Term_radiologi/dicom_viewer', $data);
    }
}
