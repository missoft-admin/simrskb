<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use Hashids\Hashids;
class Tpendaftaran_ranap extends CI_Controller {

	/**
	 * Checkin controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbooking_rehab_model');
		$this->load->model('Tbooking_model');
		$this->load->model('Tpendaftaran_ranap_model');
		$this->load->model('Tpoliklinik_pendaftaran_model');
		$this->load->model('Tcheckin_model');
		$this->load->helper('path');
		// print_r('sini');exit;
		
  }
	function index($tab='#'){
		 insert_awal_history_bed();
		 get_ppa_login();
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab!='#'){			
			if (UserAccesForm($user_acces_form,array('1929'))){
				$tab='3';
			}elseif (UserAccesForm($user_acces_form,array('1928'))){
				$tab='2';
			}elseif (UserAccesForm($user_acces_form,array('1927'))){
				$tab='1';
			}elseif (UserAccesForm($user_acces_form,array('1926'))){
				$tab='0';
			}
		}else{
			$tab='0';
		}
		if (UserAccesForm($user_acces_form,array('1577'))){
			$data = array();
			$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("-7 days"));
			$date2= date_format($date2,"d/m/Y");
			
			$data['list_reservasi_cara'] 			= list_reservasi_cara();
			// print_r($data['list_reservasi_cara']);exit;
			
			$data['list_poli'] 			= $this->Tpendaftaran_ranap_model->list_poli();
			$data['list_dokter'] 			= $this->Tpendaftaran_ranap_model->list_dokter();
			$data['tanggal_1'] 			= $date2;
			$data['tanggal_2'] 			= date('d/m/Y');
			$data['tab_utama'] 			= 1;
			$data['idalasan'] 			= '#';
			$data['waktu_reservasi_1'] 			= '';
			$data['waktu_reservasi_2'] 			= '';
			$data['namapasien'] 			= '';
			$data['no_medrec'] 			= '';
			$data['tab_detail'] 			=1;
			$data['tab'] 			= $tab;
			$tahun=date('Y');
			$bulan=date('m');
			$q="SELECT D.tanggal,DATE_ADD(D.tanggal,INTERVAL 6 DAY) as tanggal_next   FROM date_row D WHERE D.tanggal BETWEEN DATE_ADD(CURRENT_DATE(),INTERVAL -6 DAY) AND CURRENT_DATE()";
			$tanggal=$this->db->query($q)->row();
			$data['tanggaldari'] 			= DMYFormat($tanggal->tanggal);
			$data['tanggalsampai'] 			= DMYFormat($tanggal->tanggal_next);
			$data['error'] 			= '';
			$data['title'] 			= 'Pendaftaran Rawat Inap';
			$data['content'] 		= 'Tpendaftaran_ranap/index';
			$data['list_alasan'] = $this->Tpoliklinik_pendaftaran_model->getAlasan();
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Pendaftaran",'#'),
												  array("Rawat Inap",'tpendaftaran_ranap')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			// print_r($this->input->post());exit;
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$iddokter =$this->input->post('iddokter');
			$idtipe =$this->input->post('idtipe');
			$idtipe_asal =$this->input->post('idtipe_asal');
			$cari_pasien =$this->input->post('cari_pasien');
			$nopendaftaran=$this->input->post('nopendaftaran');
			$idruangan=$this->input->post('idruangan');
			$idkelas=$this->input->post('idkelas');
			$idbed=$this->input->post('idbed');
			$norujukan=$this->input->post('norujukan');
			$iddokter_perujuk=$this->input->post('iddokter_perujuk');
			$idkelompokpasien=$this->input->post('idkelompokpasien');
			$idrekanan=$this->input->post('idrekanan');
			if ($iddokter!='#'){
				$where .=" AND (H.iddokterpenanggungjawab='$iddokter') ";
			}
			if ($idrekanan!='#'){
				$where .=" AND (H.idrekanan='$idrekanan') ";
			}
			if ($idkelompokpasien!='#'){
				$where .=" AND (H.idkelompokpasien='$idkelompokpasien') ";
			}
			if ($iddokter_perujuk!='#'){
				$where .=" AND (H.iddokter_perujuk='$iddokter_perujuk') ";
			}
			if ($idbed!='#'){
				$where .=" AND (H.idbed='$idbed') ";
			}
			if ($idruangan!='#'){
				$where .=" AND (H.idruangan='$idruangan') ";
			}
			if ($idkelas!='#'){
				$where .=" AND (H.idkelas='$idkelas') ";
			}
			if ($idtipe!='#'){
				$where .=" AND (H.idtipe='$idtipe') ";
			}
			if ($idtipe_asal!='#'){
				$where .=" AND (H.idtipe_asal='$idtipe_asal') ";
			}
			if ($norujukan !=''){
				$where .=" AND (H.nopermintaan LIKE '%".$norujukan."%') ";
			}
			if ($nopendaftaran !=''){
				$where .=" AND (H.nopendaftaran LIKE '%".$nopendaftaran."%' OR H.nopendaftaran_poli LIKE '%".$nopendaftaran."%') ";
			}
			if ($cari_pasien !=''){
				$where .=" AND (H.namapasien LIKE '%".$cari_pasien."%' OR H.no_medrec LIKE '%".$cari_pasien."%') ";
			}
			// print_r($where);exit;
			// $pencarian =$this->input->post('pencarian');
			$tab =$this->input->post('tab');
			if ($tab=='2'){
				$where .=" AND (H.st_terima_ranap='0' AND H.status='1') ";
			}
			
			if ($tab=='3'){
				$where .=" AND (H.st_terima_ranap='1' AND H.status='1') ";
			}
			if ($tab=='4'){
				$where .=" AND (H.status='0') ";
			}
			if ($tab=='5'){
				$where .=" AND (H.statuscheckout='1') ";
			}
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.tanggaldaftar) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggaldaftar) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(H.tanggaldaftar) >='".date('Y-m-d')."'";
			}
			// if ($pencarian!=''){
				// $where .=" AND (H.namapasien LIKE '%".$pencarian."%')";
				// $where .=" OR (H.no_medrec LIKE '%".$pencarian."%')";
			// }
			// if ($tab=='2'){
				// $where .=" AND (H.status_input_dokter) = '0'";
			// }
			// if ($tab=='3'){
				// $where .=" AND (H.status_input_dokter) = '1'";
			// }
			// if ($idpoli!='#'){
				// $where .=" AND (H.idpoliklinik) = '$idpoli'";
			// }else{
				// $where .=" AND (H.idpoliklinik NOT IN (SELECT idpoliklinik FROM mpoliklinik_emergency)) ";
				// $where .=" AND (H.idpoliklinik NOT IN (SELECT idpoliklinik FROM mpoliklinik_direct)) ";
			// }
		
			// print_r($tab);exit;
			$this->select = array();
			$from="
					(
						SELECT MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
						,MK.nama as nama_kelompok,MR.nama as nama_rekanan,MDP.nama as nama_dokter_poli
						,H.*,MKL.nama as nama_kelas 
						,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header
						,MB.nama as nama_bed,TP.tanggal,MRU.nama as nama_ruangan,AH.kodeantrian as kode_antrian
						,UH.name as nama_hapus
						FROM trawatinap_pendaftaran H
						LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpoliklinik
						LEFT JOIN (
							".get_alergi_sql()."
						) A ON A.idpasien=H.idpasien
						LEFT JOIN mruangan MRU ON MRU.id=H.idruangan
						LEFT JOIN mkelas MKL ON MKL.id=H.idkelas
						LEFT JOIN antrian_harian AH ON AH.id=H.antrian_id
						LEFT JOIN mdokter MDP ON MDP.id=TP.iddokter
						LEFT JOIN mdokter MD ON MD.id=H.iddokterpenanggungjawab
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik_asal
						LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
						LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						LEFT JOIN mbed MB on MB.id=H.idbed						
						LEFT JOIN musers UH on UH.id=H.deleted_by						
						WHERE H.idtipe IS NOT NULL ".$where."
						GROUP BY H.id
						
						ORDER BY H.tanggaldaftar DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		  $label_skrining='<i class="fa fa-file-text-o"></i> &nbsp;'.label_skrining_ranap($r->st_lm,$r->st_lembar,$r->st_sp,$r->st_pernyataan,$r->st_gc,$r->st_general,$r->st_hk,$r->st_hak);
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  $div_terima='';
		  $div_hapus='';
		  if ($r->st_terima_ranap=='1'){
			  $div_terima='<div class="text-primary text-uppercase"><i class="fa fa-bed"></i> Tanggal Terima : '.HumanDateLong($r->tanggal_terima).'</div>';
			  $div_terima.='<div class="text-primary text-uppercase"><i class="fa fa-user"></i> User Terima : '.($r->user_nama_terima).'</div>';
		  }
		  if ($r->status=='0'){
			  $div_hapus='<div class="text-danger text-uppercase"><i class="fa fa-trash"></i> Tanggal Batal : '.HumanDateLong($r->deleted_date).'</div>';
			  $div_hapus.='<div class="text-danger text-uppercase"><i class="fa fa-user"></i> User  : '.($r->nama_hapus).'</div>';
		  }
		  $btn_hapus='';
		  $btn_terima='';
		  $btn_layani='';
		  $btn_panel='';
		  $btn_erm='';
		  
			  if ($r->status!='0'){
					  $btn_layani='<div class="push-5-t"><a href="'.base_url().'tpendaftaran_ranap/edit_pendaftaran/'.$r->id.'" target="_blank" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block btn-primary btn-xs menu_click"><i class="fa fa-user-md"></i> LAYANI</a> </div>';
				  if ($r->st_terima_ranap=='0'){
					  if (UserAccesForm($user_acces_form,array('1931'))){
						$btn_hapus='<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Hapus" onclick="show_modal_batal('.$r->id.')" class="btn btn-block btn-danger btn-xs"><i class="fa fa-trash"></i> BATAL PENDAFTARAN</a> </div>';
					  }
					  if (UserAccesForm($user_acces_form,array('1932'))){
							$btn_terima='<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Terima Rawat inap" onclick="modal_terima('.$r->id.')"  class="btn btn-block btn-success btn-xs"><i class="fa fa-bed"></i> TERIMA RAWAT INAP</a> </div>';
					  }
				  }
					  $btn_panel=div_panel_kendali_ri($user_acces_form,$r->id);
			  }
		  $btn_erm='<div class="push-5-t"><a href="'.base_url().'tpendaftaran_ranap_erm/tindakan/'.$r->id.'/erm_ri" target="_blank"  data-toggle="tooltip" title="Data ERM" target="_blank" class="btn btn-block btn-success btn-xs"><i class="fa fa-file-o"></i> DATA ERM</a> </div>';
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									'.$btn_layani.'
									'.$btn_panel.'
									'.$btn_erm.'
									'.$btn_hapus.'
									'.$btn_terima.'
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
										<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
									</div>
									<div class="text-primary font-s13 push-5-t">'.$r->nopendaftaran_poli.' | '.HumanDateShort($r->tanggal).' | '.GetAsalRujukanR($r->idtipe_asal).' | '.$r->nama_poli.' | '.$r->nama_dokter_poli.'</div>
									<div class="text-primary font-s13 push-5-t">'.$label_skrining.'</div>
									
									</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_5='<table class="block-table text-left">
						
					</table>';
		  
          $result[] = $btn_5;
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class=" text-uppercase">'.($r->nama_ruangan).' - '.$r->nama_kelas.' - '.$r->nama_bed.'</div>
									<div class=""><i class="fa fa-user-md"></i> '.($r->nama_dokter).'</div>
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->nama_rekanan?$r->nama_rekanan:$r->nama_kelompok).'</div>
									'.$div_terima.'
									'.$div_hapus.'
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.$this->status_ranap($r->status,$r->st_terima_ranap,$r->statuscheckout).'</div>
									<div class="h4 push-10-t text-primary"><button class="btn btn-xs btn-block btn-primary push-10" type="button"><i class="fa fa-bed pull-left"></i> '.($r->nama_bed).'</button></strong></div>
									<div class="h5 push-10-t text-primary"><strong>'.GetTipeRujukanRawatInap($r->idtipe).'</strong></div>
									<div class="h5 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggaldaftar).'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function status_ranap($status,$st_terima_ranap,$statuscheckout){
	  $label='<button class="btn btn-xs btn-block btn-primary push-10" type="button"><i class="fa fa-facebook pull-left "></i> Login with Facebook</button>';
	  if ($status=='0'){
		  $label='<button class="btn btn-xs btn-block btn-danger push-10" type="button"><i class="fa fa-times pull-left "></i>DIBATALKAN</button>';
	  }else{
		  if ($st_terima_ranap=='0'){
			  $label='<button class="btn btn-xs btn-block btn-info push-10" type="button"><i class="fa fa-wrench pull-left "></i>MENUNGGU DIRAWAT</button>';
		  }
		  if ($st_terima_ranap=='1' && $statuscheckout=='0'){
			  $label='<button class="btn btn-xs btn-block btn-warning push-10" type="button"><i class="fa fa-bed pull-left"></i>DIRAWAT</button>';
		  }
		  if ($statuscheckout=='1'){
			  $label='<button class="btn btn-xs btn-block btn-success push-10" type="button">PULANG</button>';
		  }
	  }
	  
	  return $label;
  }
	function getIndexPoli()	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$idtipe =$this->input->post('idtipe');
		$idpoli =$this->input->post('idpoli');
		$iddokter =$this->input->post('iddokter');
		$notransaksi =$this->input->post('notransaksi');
		$no_medrec =$this->input->post('no_medrec');
		$namapasien =$this->input->post('namapasien');
		$where_kunjungan_ranap =get_where_kunjungan_ranap();

		if ($tanggal_1!=''){
			$where .=" AND DATE(H.tanggaldaftar) >= '".YMDFormat($tanggal_1)."' AND DATE(H.tanggaldaftar) <= '".YMDFormat($tanggal_2)."'";
		}
		if ($idtipe!='#'){
			$where .=" AND (H.idtipe) = '$idtipe'";
		}
		if ($idpoli!='#'){
			$where .=" AND (H.idpoliklinik) = '$idpoli'";
		}
		if ($iddokter!='#'){
			$where .=" AND (H.iddokter) = '$iddokter'";
		}
		if ($notransaksi!=''){
			$where .=" AND (H.nopendaftaran) LIKE '%".$notransaksi."%'";
		}
		if ($no_medrec!=''){
			$where .=" AND (H.no_medrec) LIKE '%".$no_medrec."%'";
		}
		if ($namapasien!=''){
			$where .=" AND (H.namapasien) LIKE '%".$namapasien."%'";
		}

		// print_r($tab);exit;
		$this->select = array();
		$from="
				(
					SELECT H.*,MD.nama as nama_dokter,M.nama as nama_poli_asal 
					FROM tpoliklinik_pendaftaran H
					INNER JOIN mdokter MD ON MD.id=H.iddokter
					LEFT JOIN mpoliklinik M oN M.id=H.idpoliklinik
					WHERE H.id IS NOT NULL ".$where." 
					".$where_kunjungan_ranap."
					ORDER BY H.tanggal DESC
				) as tbl
			";
		// print_r($from);exit;
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array();
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$aksi='';
			// if (UserAccesForm($user_acces_form,array('1912'))){
			$aksi .= '<a href="'.base_url().'tpendaftaran_ranap/add_daftar_rj/'.$r->id.'" type="button" title="Daftar Rawat inap" class="btn btn-success btn-xs"><i class="si si-cursor"></i> Daftarkan</a>';	
			// }
			$result = array();
			$result[] =$no;
			$result[] =$aksi;
			$result[] =$r->nopendaftaran;
			$result[] =$r->tanggaldaftar;
			$result[] =$r->no_medrec;
			$result[] =$r->namapasien;
			$result[] =$r->nama_dokter;
			$result[] =GetTipePasienPiutang($r->idtipe).'<br>'.$r->nama_poli_asal;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
  }
	function getIndexRencana()	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$idtipe =$this->input->post('idtipe');
		$idpoli =$this->input->post('idpoli');
		$iddokter =$this->input->post('iddokter');
		$notransaksi =$this->input->post('notransaksi');
		$no_medrec =$this->input->post('no_medrec');
		$namapasien =$this->input->post('namapasien');

		if ($tanggal_1!=''){
			$where .=" AND DATE(H.created_date) >= '".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <= '".YMDFormat($tanggal_2)."'";
		}
		if ($idtipe!='#'){
			$where .=" AND (H.idtipe) = '$idtipe'";
		}
		if ($idpoli!='#'){
			$where .=" AND (H.idpoliklinik) = '$idpoli'";
		}
		if ($iddokter!='#'){
			$where .=" AND (MP.iddokter) = '$iddokter'";
		}
		if ($notransaksi!=''){
			$where .=" AND (H.nopermintaan) LIKE '%".$notransaksi."%'";
		}
		if ($no_medrec!=''){
			$where .=" AND (MP.no_medrec) LIKE '%".$no_medrec."%'";
		}
		if ($namapasien!=''){
			$where .=" AND (MP.namapasien) LIKE '%".$namapasien."%'";
		}

		// print_r($tab);exit;
		$this->select = array();
		$from="
				(
					SELECT MP.nopendaftaran,MP.tanggal,MD.nama as nama_dokter
					,MP.idtipe as idtipe_poli,M.nama as nama_poli_asal
					,H.* FROM tpoliklinik_ranap_perencanaan H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mppa MD ON MD.id=H.iddokter_peminta
					LEFT JOIN mpoliklinik M oN M.id=MP.idpoliklinik
					WHERE H.status_assemen='2' ".$where."
					ORDER BY H.tanggal_input DESC 
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array();
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$aksi='';
			// if (UserAccesForm($user_acces_form,array('1913'))){
			// $aksi .= '<a href="'.base_url().'treservasi_bed/proses_rencana/'.$r->assesmen_id.'/'.$r->pendaftaran_id.'" type="button" title="Proses Reservasi Bed" class="btn btn-success btn-xs"><i class="si si-cursor"></i> Reserv</a>';	
			// }
			$aksi .= '<a href="'.base_url().'tpendaftaran_ranap/add_daftar_rencana/'.$r->assesmen_id.'" type="button" title="Daftar Rawat inap" class="btn btn-success btn-xs"><i class="si si-cursor"></i> Daftarkan</a>';	
			$result = array();
			$result[] =$no;
			$result[] =$aksi;
			$result[] =$r->nopermintaan.'<br>'.HumanDateLong($r->created_date);
			$result[] =GetTipePasienPiutang($r->tipe);
			$result[] =$r->nopendaftaran.'<br>'.HumanDateShort($r->tanggal);
			$result[] =$r->nomedrec_pasien.'<br>'.$r->nama_pasien;
			$result[] =$r->nama_dokter;
			$result[] =($r->st_ranap=='1'?text_primary('RAWAT INAP'):GetTipePasienPiutang($r->idtipe_poli).'<br>'.$r->nama_poli_asal);
			$result[] =$r->dengan_diagnosa;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
  }
  function getIndexHisRanap()	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$idpasien =$this->input->post('idpasien');
		
		$this->select = array();
		$from="
				(
					SELECT H.tanggaldaftar,H.idpasien,H.nopendaftaran,H.nopermintaan,H.diagnosa,H.iddokter_perujuk,H.iddokterpenanggungjawab 
					,dpjp.nama as nama_dpjp,MD.nama as nama_dokter
					FROM `trawatinap_pendaftaran` H
					LEFT JOIN mdokter dpjp ON dpjp.id=H.iddokterpenanggungjawab
					LEFT JOIN mdokter MD ON MD.id=H.iddokter_perujuk
					WHERE H.idpasien='$idpasien' AND H.`status` !='0'
					ORDER BY H.tanggaldaftar ASC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama_dpjp','nama_dokter','nopendaftaran','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			
			$result = array();
			$result[] =$no;
			$result[] =HumanDateShort($r->tanggaldaftar);
			$result[] =$r->nopendaftaran;
			$result[] =$r->nopermintaan;
			$result[] =$r->diagnosa;
			$result[] =$r->nama_dpjp;
			$result[] =$r->nama_dokter;
			
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
  }
  function getIndex_reservasi()	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// tipe_pemilik:tipe_pemilik,status:status,nama:nama
		$where='';
		$tab_detail =$this->input->post('tab_detail');
		if ($tab_detail=='2'){
			$where .=" AND (H.status_reservasi) = '1'";
		}
		if ($tab_detail=='3'){
			$where .=" AND (H.status_reservasi) = '2'";
		}
		if ($tab_detail=='4'){
			$where .=" AND (H.status_reservasi) = '0'";
		}
		$idbed =$this->input->post('idbed');
		$idkelas =$this->input->post('idkelas');
		$idruangan =$this->input->post('idruangan');
		$notransaksi =$this->input->post('notransaksi');
		$no_medrec =$this->input->post('no_medrec');
		$namapasien =$this->input->post('namapasien');
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$tanggal_input_1 =$this->input->post('tanggal_input_1');
		$tanggal_input_2 =$this->input->post('tanggal_input_2');
		if ($tanggal_1!=''){
			$where .=" AND H.rencana_masuk >= '".YMDFormat($tanggal_1)."' AND H.rencana_masuk <= '".YMDFormat($tanggal_2)."'";
		}
		if ($tanggal_input_1!=''){
			$where .=" AND DATE(H.created_date) >= '".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <= '".YMDFormat($tanggal_input_2)."'";
		}
		if ($idruangan!='#'){
			$where .=" AND (H.idruangan) = '$idruangan'";
		}
		if ($idkelas!='#'){
			$where .=" AND (H.idkelas) = '$idkelas'";
		}
		if ($idbed!='#'){
			$where .=" AND (H.id) = '$idbed'";
		}
		
		if ($no_medrec!=''){
			$where .=" AND (H.no_medrec) LIKE '%".$no_medrec."%'";
		}
		if ($namapasien!=''){
			$where .=" AND (H.namapasien) LIKE '%".$namapasien."%'";
		}
		// print_r($tab);exit;
		$this->select = array();
		$from="
				(
					SELECT 
					MR.nama as nama_ruangan,MK.nama as nama_kelas,MB.nama as nama_bed,MD.nama as nama_dokter
					,H.*
					FROM treservasi_bed H
					INNER JOIN mruangan MR ON MR.id=H.idruangan
					INNER JOIN mkelas MK ON MK.id=H.idkelas
					LEFT JOIN mbed MB ON MB.id=H.idbed
					LEFT JOIN mdokter MD ON MD.id=H.dpjp
					WHERE H.id IS NOT NULL ".$where."
					ORDER BY H.rencana_masuk ASC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array();
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			
			$aksi ='<div class="btn-group btn-group-sm" role="group">';
			if ($r->status_reservasi>0){
				if ($r->status_reservasi=='1'){
					
					if (UserAccesForm($user_acces_form,array('1914'))){
					// $aksi .= '<a href="'.base_url().'treservasi_bed/edit/'.$r->id.'" type="button" title="Proses" class="btn btn-warning btn-xs"><i class="si si-arrow-right"></i></a>';	
					$aksi .= '<a href="'.base_url().'tpendaftaran_ranap/add_daftar_reservasi/'.$r->id.'"  type="button" title="Proses" class="btn btn-primary btn-xs"><i class="si si-arrow-right"></i> Daftarkan</a>';	
					}
					
				}
				
			}
			$aksi .='</div>';
			$result = array();
			$result[] =$no;
			$result[] =HumanDateLong($r->created_date);
			$result[] =$r->no_medrec.'<br>'.$r->namapasien;
			$result[] =$r->nama_ruangan;
			$result[] =$r->nama_kelas;
			$result[] =($r->idbed==0?text_default('TIDAK DITENTUKAN'):$r->nama_bed);
			$result[] =HumanDateShort($r->rencana_masuk);
			$result[] =$r->nama_dokter;
			$result[] =($r->prioritas==1?text_danger('CITO'):text_info('NON CITO'));
			$result[] =Status_Reservasi_Bed($r->status_reservasi);
			$result[] =$aksi;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
  }
  function getIndexRencanaCari()	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$idpasien =$this->input->post('idpasien');
		$nopermintaan =$this->input->post('nopermintaan');
		$cari_rencana =$this->input->post('cari_rencana');
		$idpasien =$this->input->post('idpasien');
		
		if ($idpasien!=''){
			$where .=" AND (H.idpasien)='$idpasien'";
		}
		if ($nopermintaan!=''){
			$where .=" AND (H.nopermintaan) LIKE '%".$nopermintaan."%'";
		}
		if ($cari_rencana!=''){
			$where .=" AND (H.nomedrec_pasien LIKE '%".$cari_rencana."%' OR H.nama_pasien LIKE '%".$cari_rencana."%')";
		}
		if ($tanggal_1!=''){
			$where .=" AND DATE(H.created_date) >= '".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <= '".YMDFormat($tanggal_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT MP.nopendaftaran,MP.tanggal,MD.nama as nama_dokter,MP.idtipe as idtipe_poli,M.nama as nama_poli_asal,H.* 
					FROM tpoliklinik_ranap_perencanaan H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN mppa MD ON MD.id=H.iddokter_peminta
					LEFT JOIN mpoliklinik M oN M.id=MP.idpoliklinik
					WHERE H.idpasien IS NOT NULL AND H.status_assemen='2' ".$where."
					ORDER BY H.tanggal_input DESC 
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array();
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$aksi='';
			$aksi .= '<button onclick="get_data_perencanaan('.$r->assesmen_id.')" type="button" title="Pilih" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Pilih</button>';
			$result = array();
			$result[] =$no;
			$result[] =$aksi;
			$result[] =$r->nopermintaan.'<br>'.HumanDateLong($r->created_date);
			$result[] =GetTipePasienPiutang($r->tipe);
			$result[] =$r->nopendaftaran.'<br>'.HumanDateShort($r->tanggal);
			$result[] =$r->nomedrec_pasien.'<br>'.$r->nama_pasien;
			$result[] =$r->nama_dokter;
			$result[] =GetTipePasienPiutang($r->idtipe_poli).'<br>'.$r->nama_poli_asal;
			$result[] =strip_tags($r->dengan_diagnosa);
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
  }
	function getIndexPoliCari()	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$cari_poli =$this->input->post('cari_poli');
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$notransaksi =$this->input->post('notransaksi');
		$idpasien =$this->input->post('idpasien');
		$where_kunjungan_ranap =get_where_kunjungan_ranap();
		if ($tanggal_1!=''){
			$where .=" AND ((H.tanggal >= '".YMDFormat($tanggal_1)."' AND H.tanggal <= '".YMDFormat($tanggal_2)."') OR (DATE(H.tanggaldaftar) >= '".YMDFormat($tanggal_1)."' AND DATE(H.tanggaldaftar) <= '".YMDFormat($tanggal_2)."'))";
		}
		
		if ($notransaksi!=''){
			$where .=" AND (H.nopendaftaran) LIKE '%".$notransaksi."%'";
		}
		if ($idpasien!=''){
			$where .=" AND (H.idpasien)='$idpasien'";
		}
		if ($cari_poli!=''){
			$where .=" AND (H.no_medrec LIKE '%".$cari_poli."%' OR H.namapasien LIKE '%".$cari_poli."%' )";
		}
		
		// print_r($tab);exit;
		$this->select = array();
		$from="
				(
					SELECT H.*,MD.nama as nama_dokter,M.nama as nama_poli_asal 
					FROM tpoliklinik_pendaftaran H
					INNER JOIN mdokter MD ON MD.id=H.iddokter
					LEFT JOIN mpoliklinik M oN M.id=H.idpoliklinik
					WHERE H.idpasien IS NOT NULL ".$where."
					".$where_kunjungan_ranap."
					ORDER BY H.tanggal DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array();
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$aksi='';
			$aksi .= '<button onclick="get_data_pendaftaran('.$r->id.')" type="button" title="Pilih" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Pilih</button>';	
			$result = array();
			$result[] =$no;
			$result[] =$aksi;
			$result[] =$r->nopendaftaran;
			$result[] =$r->tanggaldaftar;
			$result[] =$r->no_medrec;
			$result[] =$r->namapasien;
			$result[] =$r->nama_dokter;
			$result[] =GetTipePasienPiutang($r->idtipe).'<br>'.$r->nama_poli_asal;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
  }
  function get_data_perencanaan(){
	  $perencanaan_id=$this->input->post('id');
		$q="
			SELECT MP.*,H.dengan_diagnosa as diagnosa,H.nopermintaan,H.assesmen_id as perencanaan_id,H.tanggal_masuk as rencana_masuk,H.tipe,H.dpjp as iddokterpenanggungjawab
			FROM tpoliklinik_ranap_perencanaan H 
			INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
			INNER JOIN mpoliklinik P ON MP.idpoliklinik=P.id
			INNER JOIN mdokter MD ON MD.id=MP.iddokter
				WHERE H.assesmen_id='$perencanaan_id' AND H.status_assemen='2'
			";
		$data=$this->db->query($q)->row_array();
		$data_umur_pasien=getUmurArray($data['tanggal_lahir']);
		$data['list_provinsi']=$this->Tpendaftaran_ranap_model->get_kota(0,$data['provinsi_id']);
		$data['list_kab']=$this->Tpendaftaran_ranap_model->get_kota($data['provinsi_id'],$data['kabupaten_id']);
		$data['list_kec']=$this->Tpendaftaran_ranap_model->get_kota($data['kabupaten_id'],$data['kecamatan_id']);
		$data['list_desa']=$this->Tpendaftaran_ranap_model->get_kota($data['kecamatan_id'],$data['kelurahan_id']);
		
		$data['diagnosa']=strip_tags($data['diagnosa']);
		$data['tipe']=($data['tipe']=="3"?1:2);
		$data['dirawat_ke']=$this->Tpendaftaran_ranap_model->get_jumlah_rawat($data['idpasien']);
		$this->output->set_output(json_encode($data));
  }
  function get_data_pendaftaran(){
	  $pendaftaran_id=$this->input->post('id');
		$q="
		SELECT MP.*
		,P.nama as nama_poli,MD.nama as nama_dokter
		
		FROM tpoliklinik_pendaftaran MP 
		INNER JOIN mpoliklinik P ON MP.idpoliklinik=P.id
		INNER JOIN mdokter MD ON MD.id=MP.iddokter
			WHERE MP.id='$pendaftaran_id'
		";
		$data=$this->db->query($q)->row_array();
		$data_umur_pasien=getUmurArray($data['tanggal_lahir']);
		$data['list_provinsi']=$this->Tpendaftaran_ranap_model->get_kota(0,$data['provinsi_id']);
		$data['list_kab']=$this->Tpendaftaran_ranap_model->get_kota($data['provinsi_id'],$data['kabupaten_id']);
		$data['list_kec']=$this->Tpendaftaran_ranap_model->get_kota($data['kabupaten_id'],$data['kecamatan_id']);
		$data['list_desa']=$this->Tpendaftaran_ranap_model->get_kota($data['kecamatan_id'],$data['kelurahan_id']);
		$data['dirawat_ke']=$this->Tpendaftaran_ranap_model->get_jumlah_rawat($data['idpasien']);
		$data=array_merge($data,$data_umur_pasien);
		$this->output->set_output(json_encode($data));
  }
  function add_daftar_rj($id){
	  $pendaftaran_id=$id;
		$data=$this->default_data_daftar();
		$q="
		SELECT MP.*
		,P.nama as nama_poli,MD.nama as nama_dokter
		,MP.nopendaftaran as nopendaftaran_poli,MP.idtipe as idtipe_asal,MP.id as pendaftaran_poli_id
		,MP.iddokter as iddokter_perujuk,MP.idpoliklinik as idpoliklinik_asal,MP.hubungan as hubunganpenanggungjawab
		,MP.provinsi_id as provinsi_idpenanggungjawab,MP.kabupaten_id as kabupaten_idpenanggungjawab,MP.kelurahan_id as kelurahan_idpenanggungjawab
		,MP.kodepos as kodepospenanggungjawab,MP.kecamatan_id as kecamatan_idpenanggungjawab
		FROM tpoliklinik_pendaftaran MP 
		INNER JOIN mpoliklinik P ON MP.idpoliklinik=P.id
		INNER JOIN mdokter MD ON MD.id=MP.iddokter
			WHERE MP.id='$pendaftaran_id'
		";
		$data_get=$this->db->query($q)->row_array();
		// print_r($data_get);exit;
		$tanggallahir=DMYFormat2($data_get['tanggal_lahir']);
		$data['tgl_hari']=substr($tanggallahir,0,2);
		$data['tgl_bulan']=substr($tanggallahir,3,2);
		$data['tgl_tahun']=substr($tanggallahir,6,4);
		
		$data_umur_pasien=getUmurArray($data_get['tanggal_lahir']);
		$data['list_provinsi']=$this->Tpendaftaran_ranap_model->get_kota(0,$data_get['provinsi_id']);
		$data['list_kab']=$this->Tpendaftaran_ranap_model->get_kota($data_get['provinsi_id'],$data_get['kabupaten_id']);
		$data['list_kec']=$this->Tpendaftaran_ranap_model->get_kota($data_get['kabupaten_id'],$data_get['kecamatan_id']);
		$data['list_desa']=$this->Tpendaftaran_ranap_model->get_kota($data_get['kecamatan_id'],$data_get['kelurahan_id']);
		$data=array_merge($data,$data_get,$data_umur_pasien);
		$data['tanggaldaftar'] 			= date('d-m-Y');
		$data['waktudaftar'] 			= date('H:i:s');
		
		
		
		$data['dirawat_ke']=$this->Tpendaftaran_ranap_model->get_jumlah_rawat($data_get['idpasien']);
		$data['id'] 			= '';
		$data['error'] 			= '';
		$data['title'] 			= 'Pendaftaran Rawat Inap';
		$data['content'] 		= 'Tpendaftaran_ranap/add';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Pendaftaran",'#'),
											  array("Rawat Inap",'tpendaftaran_ranap')
											);
		$setting_lm=$this->Tpendaftaran_ranap_model->setting_lm();
		$setting_sp=$this->Tpendaftaran_ranap_model->setting_sp();
		$setting_hk=$this->Tpendaftaran_ranap_model->setting_hk();
		$data = array_merge($data,$setting_lm,$setting_sp,$setting_hk, backend_info());
		$this->parser->parse('module_template', $data);
		
		
		
  }
  function add_daftar_rencana($id){
	  $pendaftaran_id=$id;
		$data=$this->default_data_daftar();
		$q="
		SELECT MP.*
		,P.nama as nama_poli,MD.nama as nama_dokter
		,MP.nopendaftaran as nopendaftaran_poli,MP.idtipe as idtipe_asal,MP.id as pendaftaran_poli_id
		,H.iddokter_peminta as iddokter_perujuk,MP.idpoliklinik as idpoliklinik_asal,MP.hubungan as hubunganpenanggungjawab
		,MP.provinsi_id as provinsi_idpenanggungjawab,MP.kabupaten_id as kabupaten_idpenanggungjawab,MP.kelurahan_id as kelurahan_idpenanggungjawab
		,MP.kodepos as kodepospenanggungjawab,MP.kecamatan_id as kecamatan_idpenanggungjawab,H.assesmen_id as perencanaan_id,H.nopermintaan
		,H.dengan_diagnosa as diagnosa,H.tipe as tipe_rencana,H.dpjp as iddokterpenanggungjawab
		FROM tpoliklinik_ranap_perencanaan H 
		INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
		INNER JOIN mpoliklinik P ON MP.idpoliklinik=P.id
		INNER JOIN mdokter MD ON MD.id=MP.iddokter
		WHERE H.assesmen_id='$id' AND H.status_assemen='2'
		";
		$data_get=$this->db->query($q)->row_array();
		// print_r($data_get);exit;
		$tanggallahir=DMYFormat2($data_get['tanggal_lahir']);
		$data['tgl_hari']=substr($tanggallahir,0,2);
		$data['tgl_bulan']=substr($tanggallahir,3,2);
		$data['tgl_tahun']=substr($tanggallahir,6,4);
		
		$data_umur_pasien=getUmurArray($data_get['tanggal_lahir']);
		$data['list_provinsi']=$this->Tpendaftaran_ranap_model->get_kota(0,$data_get['provinsi_id']);
		$data['list_kab']=$this->Tpendaftaran_ranap_model->get_kota($data_get['provinsi_id'],$data_get['kabupaten_id']);
		$data['list_kec']=$this->Tpendaftaran_ranap_model->get_kota($data_get['kabupaten_id'],$data_get['kecamatan_id']);
		$data['list_desa']=$this->Tpendaftaran_ranap_model->get_kota($data_get['kecamatan_id'],$data_get['kelurahan_id']);
		$data=array_merge($data,$data_get,$data_umur_pasien);
		$data['tanggaldaftar'] 			= date('d-m-Y');
		$data['waktudaftar'] 			= date('H:i:s');
		
		
		$data['dirawat_ke']=$this->Tpendaftaran_ranap_model->get_jumlah_rawat($data_get['idpasien']);
		$data['idtipe'] 			= ($data_get['tipe_rencana']=='3'?1:2);
		$data['id'] 			= '';
		$data['error'] 			= '';
		$data['title'] 			= 'Pendaftaran Rawat Inap';
		$data['content'] 		= 'Tpendaftaran_ranap/add';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Pendaftaran",'#'),
											  array("Rawat Inap",'tpendaftaran_ranap')
											);
		$setting_lm=$this->Tpendaftaran_ranap_model->setting_lm();
		$setting_sp=$this->Tpendaftaran_ranap_model->setting_sp();
		$setting_hk=$this->Tpendaftaran_ranap_model->setting_hk();
		$data = array_merge($data,$setting_lm,$setting_sp,$setting_hk, backend_info());
		$this->parser->parse('module_template', $data);
		
		
		
  }
  function add_daftar_reservasi($id){
	  $pendaftaran_id=$id;
		$data=$this->default_data_daftar();
		$q="
		SELECT MP.*
		,P.nama as nama_poli,MD.nama as nama_dokter
		,MP.nopendaftaran as nopendaftaran_poli,MP.id as pendaftaran_poli_id
		,H.iddokter_perujuk as iddokter_perujuk,MP.idpoliklinik as idpoliklinik_asal,MP.hubungan as hubunganpenanggungjawab
		,MP.provinsi_id as provinsi_idpenanggungjawab,MP.kabupaten_id as kabupaten_idpenanggungjawab,MP.kelurahan_id as kelurahan_idpenanggungjawab
		,MP.kodepos as kodepospenanggungjawab,MP.kecamatan_id as kecamatan_idpenanggungjawab,H.perencanaan_id as perencanaan_id,H.nopermintaan
		,H.diagnosa as diagnosa,H.dpjp as iddokterpenanggungjawab,H.idtipe as idtipe_asal
		,H.idruangan,H.idkelas,H.idbed,H.idruangan as idruangan_permintaan,H.idkelas as idkelas_permintaan,H.idbed as idbed_permintaan,H.id as treservasi_bed_id
		FROM treservasi_bed H 
		INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
		INNER JOIN mpoliklinik P ON MP.idpoliklinik=P.id
		INNER JOIN mdokter MD ON MD.id=MP.iddokter
		
		WHERE H.id='$id'
		";
		$data_get=$this->db->query($q)->row_array();
		// print_r($data_get);exit;
		$tanggallahir=DMYFormat2($data_get['tanggal_lahir']);
		$data['tgl_hari']=substr($tanggallahir,0,2);
		$data['tgl_bulan']=substr($tanggallahir,3,2);
		$data['tgl_tahun']=substr($tanggallahir,6,4);
		
		$data_umur_pasien=getUmurArray($data_get['tanggal_lahir']);
		$data['list_provinsi']=$this->Tpendaftaran_ranap_model->get_kota(0,$data_get['provinsi_id']);
		$data['list_kab']=$this->Tpendaftaran_ranap_model->get_kota($data_get['provinsi_id'],$data_get['kabupaten_id']);
		$data['list_kec']=$this->Tpendaftaran_ranap_model->get_kota($data_get['kabupaten_id'],$data_get['kecamatan_id']);
		$data['list_desa']=$this->Tpendaftaran_ranap_model->get_kota($data_get['kecamatan_id'],$data_get['kelurahan_id']);
		$data=array_merge($data,$data_get,$data_umur_pasien);
		$data['tanggaldaftar'] 			= date('d-m-Y');
		$data['waktudaftar'] 			= date('H:i:s');
		
		
		
		$data['dirawat_ke']=$this->Tpendaftaran_ranap_model->get_jumlah_rawat($data_get['idpasien']);
		$data['id'] 			= '';
		$data['error'] 			= '';
		$data['title'] 			= 'Pendaftaran Rawat Inap';
		$data['content'] 		= 'Tpendaftaran_ranap/add';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Pendaftaran",'#'),
											  array("Rawat Inap",'tpendaftaran_ranap')
											);
		$setting_lm=$this->Tpendaftaran_ranap_model->setting_lm();
		$setting_sp=$this->Tpendaftaran_ranap_model->setting_sp();
		$setting_hk=$this->Tpendaftaran_ranap_model->setting_hk();
		$data = array_merge($data,$setting_lm,$setting_sp,$setting_hk, backend_info());
		$this->parser->parse('module_template', $data);
		
		
		
  }
  function get_kota(){
	  $kode=$this->input->post('kode');
	  $id=($this->input->post('id')?$this->input->post('id'):'');
	  $data['list_data']=$this->Tpendaftaran_ranap_model->get_kota($kode,$id);
	  $this->output->set_output(json_encode($data));
  }
  function get_umur(){
	  $tahun=$this->input->post('tahun');
	  $bulan=$this->input->post('bulan');
	  $hari=$this->input->post('hari');
	  
	  $tanggal_lahir=$tahun.'-'.$bulan.'-'.$hari;
	  $data_umur_pasien=getUmurArray($tanggal_lahir);
	  $this->output->set_output(json_encode($data_umur_pasien));
  }
  public function getKodepos()
	{
		$kode=$this->input->post('kode');
		$row = get_all('mfwilayah', ['id' => $kode]);
		// print_r($row);exit;
		$data['kodepos']=$row[0]->kodepos;
		$this->output->set_output(json_encode($data));
	}
	function default_data_daftar(){
		$data=array(
				'idruangan_default' => '1',
				'idkelas_default' => '3',
				'idbed_default' => '0',
				'foto_kartu' => '',
				'idtipe' => '',
				'idtipe_asal' => '',
				'idpoliklinik_asal' => '',
				'idpoliklinik' => '',
				'tanggaldaftar' => '',
				'nopendaftaran' => '',
				'noantrian' => '',
				'idtipepasien' => '',
				'idjenispasien' => '',
				'idkelompokpasien' => '',
				'idrekanan' => '',
				'idtarifbpjskesehatan' => '',
				'idtarifbpjstenagakerja' => '',
				'idkelompokpasien2' => '',
				'idrekanan2' => '',
				'idtarifbpjskesehatan2' => '',
				'idtarifbpjstenagakerja2' => '',
				'iddokterpenanggungjawab' => '',
				'idruangan' => '',
				'idkelas' => '',
				'idbed' => '',
				'rujukfarmasi' => '',
				'rujuklaboratorium' => '',
				'rujukradiologi' => '',
				'rujukfisioterapi' => '',
				'iduserinput' => '',
				'tanggalperubahandokter' => '',
				'tanggalperubahanbed' => '',
				'tanggalcheckout' => '',
				'statuscheckout' => '',
				'statuskasir' => '',
				'statusvalidasi' => '',
				'statustransaksi' => '',
				'statuspembayaran' => '',
				'idpasien' => '',
				'title' => '',
				'namapasien' => '',
				'no_medrec' => '',
				'nohp' => '',
				'telepon' => '',
				'tempat_lahir' => '',
				'tanggal_lahir' => '',
				'alamatpasien' => '',
				'provinsi_id' => '',
				'kabupaten_id' => '',
				'kecamatan_id' => '',
				'kelurahan_id' => '',
				'kodepos' => '',
				'namapenanggungjawab' => '',
				'hubungan' => '',
				'umurhari' => '',
				'umurbulan' => '',
				'umurtahun' => '',
				'idalasan' => '',
				'jenis_kelamin' => '',
				'catatan' => '',
				'created_by' => '',
				'created_date' => '',
				'edited_by' => '',
				'edited_date' => '',
				'deleted_by' => '',
				'deleted_date' => '',
				'status' => '',
				'antrian_id' => '',
				'pendaftaran_poli_id' => '',
				'nopendaftaran_poli' => '',
				'perencanaan_id' => '',
				'nopermintaan' => '',
				'diagnosa' => '',
				'iddokter_perujuk' => '',
				'nik' => '',
				'nama_panggilan' => '',
				'bahasa' => '',
				'rw' => '',
				'rt' => '',
				'nama_ibu_kandung' => '',
				'golongan_darah' => '',
				'agama_id' => '',
				'warganegara' => '',
				'suku_id' => '',
				'pendidikan' => '',
				'pekerjaan' => '',
				'statuskawin' => '',
				'jenis_id' => '',
				'noidentitas' => '',
				'chk_st_domisili' => '',
				'alamat_jalan_ktp' => '',
				'rw_ktp' => '',
				'rt_ktp' => '',
				'provinsi_id_ktp' => '',
				'kabupaten_id_ktp' => '',
				'kecamatan_id_ktp' => '',
				'kelurahan_id_ktp' => '',
				'kodepos_ktp' => '',
				'hubunganpenanggungjawab' => '',
				'tgl_lahirpenanggungjawab' => '',
				'umur_tahunpenanggungjawab' => '',
				'umur_bulanpenanggungjawab' => '',
				'umur_haripenanggungjawab' => '',
				'pekerjaannpenanggungjawab' => '',
				'pendidikanpenanggungjawab' => '',
				'agama_idpenanggungjawab' => '',
				'alamatpenanggungjawab' => '',
				'provinsi_idpenanggungjawab' => '',
				'kabupaten_idpenanggungjawab' => '',
				'kecamatan_idpenanggungjawab' => '',
				'kelurahan_idpenanggungjawab' => '',
				'kodepospenanggungjawab' => '',
				'rwpenanggungjawab' => '',
				'rtpenanggungjawab' => '',
				'teleponpenanggungjawab' => '',
				'noidentitaspenanggungjawab' => '',
				'namapengantar' => '',
				'hubunganpengantar' => '',
				'tgl_lahirpengantar' => '',
				'umur_tahunpengantar' => '',
				'umur_bulanpengantar' => '',
				'umur_haripengantar' => '',
				'pekerjaannpengantar' => '',
				'pendidikanpengantar' => '',
				'agama_idpengantar' => '',
				'alamatpengantar' => '',
				'provinsi_idpengantar' => '',
				'kabupaten_idpengantar' => '',
				'kecamatan_idpengantar' => '',
				'kelurahan_idpengantar' => '',
				'kodepospengantar' => '',
				'rwpengantar' => '',
				'rtpengantar' => '',
				'teleponpengantar' => '',
				'noidentitaspengantar' => '',
				'nama_keluarga_td' => '',
				'hubungan_td' => '',
				'alamat_td' => '',
				'catatan_lain' => '',
				'email' => '',
				'chk_st_pengantar' => '1',
				'tgl_hari' => '',
				'tgl_hari' => '',
				'tgl_bulan' => '',
				'tgl_tahun' => '',
				'foto_kartu' => '',
				'kasus_kepolisian' => '2',
				'idasalpasien' => '',
				'dirawat_ke' => '',
				'idrujukan' => '',
				'kartu_id' => '',
				'nokartu' => '',
				'nama_kartu' => '',
				'noreference_kartu' => '',
				'pertemuan_id' => '',
				'idtipepasien' => '',
				'st_dpjp_pendamping' => '0',
				'idruangan_permintaan' => '',
				'idkelas_permintaan' => '',
				'idbed_permintaan' => '',
				'tgl_lahirpenanggungjawab' => '0',
				'bulan_lahirpenanggungjawab' => '0',
				'tahun_lahirpenanggungjawab' => '0',
				'tgl_lahirpengantar' => '0',
				'bulan_lahirpengantar' => '0',
				'tahun_lahirpengantar' => '0',
				'umur_tahunpenanggungjawab' => '0',
				'umur_bulanpenanggungjawab' => '0',
				'umur_haripenanggungjawab' => '0',
				'treservasi_bed_id' => '0',

			);
			$data['list_dokter_pendamping']=array();
			
			$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("-14 days"));
			$date2= date_format($date2,"d/m/Y");
			$data['tab'] 			= 1;
			$data['st_lm'] 			= 0;
			$data['st_lembar'] 			= 0;
			$data['st_sp'] 			= 0;
			$data['st_pernyataan'] 			= 0;
			$data['st_gc'] 			= 0;
			$data['st_general'] 			= 0;
			$data['st_hk'] 			= 0;
			$data['st_hak'] 			= 0;
			
			
			$data['tanggal_1'] 			= $date2;
			$data['tanggal_2'] 			= date('d/m/Y');
			$data['list_kontak'] 			= array();
			$data['list_asuransi'] 			= $this->Tpendaftaran_ranap_model->list_asuransi();
			$data['list_antrian'] 			= $this->Tcheckin_model->list_antrian();
			$data['list_cara_bayar'] 			= $this->Tpendaftaran_ranap_model->list_cara_bayar();
			
			$data['tanggaldaftar'] 			= date('d-m-Y');
			$data['waktudaftar'] 			= date('H:i:s');
			$data['tanggaldaftar2'] 			= date('d-m-Y');
			$data['waktudaftar2'] 			= date('H:i:s');
			$data['statuspasienbaru'] 			= '0';
			//DATA TEMBAHAKN
			// $data['pekerjaannpenanggungjawab']='10';
			// $data['pendidikanpenanggungjawab']='10';
			// $data['agama_idpenanggungjawab']='1';
			// $data['kabupaten_idpenanggungjawab']='193';
			// $data['kecamatan_idpenanggungjawab']='2457';
			// $data['kelurahan_idpenanggungjawab']='32516';
			// $data['kodepospenanggungjawab']='44165';
			// $data['rwpenanggungjawab']='44165';
			// $data['rtpenanggungjawab']='44165';
			// $data['tgl_lahirpenanggungjawab']='01';
			// $data['bulan_lahirpenanggungjawab']='01';
			// $data['tahun_lahirpenanggungjawab']='1986';
			// $data['id']='44165';
			return $data;
	}
	function add_daftar($antrian_id=''){
		// print_r(getUmurArray());exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1930'))){
			$data=$this->default_data_daftar();
			$data['tanggaldaftar'] 			= date('d-m-Y');
			$data['waktudaftar'] 			= date('H:i:s');
			$data['antrian_id'] 			= $antrian_id;
			$data['id'] 			= '';
			$data['error'] 			= '';
			$data['title'] 			= 'Pendaftaran Rawat Inap';
			$data['content'] 		= 'Tpendaftaran_ranap/add';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Pendaftaran",'#'),
												  array("Rawat Inap",'tpendaftaran_ranap')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	public function get_logic_tipe_pasien()
	{
		$asal_pasien = $this->input->post('asal_pasien');
		$idpoli = $this->input->post('idpoli');
		$iddokter = $this->input->post('iddokter');
		$idkelompokpasien = $this->input->post('idkelompokpasien');
		$tipe_rekanan = 0;
		$idrekanan = $this->input->post('idrekanan');
		$idtujuan = ($this->input->post('idtujuan')=='1'?3:4);
		
		if ($idkelompokpasien!='1'){//Bukan Asuransi
			$q="
				SELECT T.idtipepasien FROM setting_ranap_tipe T INNER JOIN (
					SELECT 
					compare_value_5(
					MAX(IF(idpoli='$idpoli' AND H.iddokter='$iddokter' AND H.idkelompokpasien='$idkelompokpasien' AND H.idtujuan='$idtujuan',H.id,NULL))
					,MAX(IF(idpoli='$idpoli' AND H.iddokter='$iddokter' AND H.idkelompokpasien='$idkelompokpasien' AND (H.idtujuan='0' OR H.idtujuan='$idtujuan'),H.id,NULL))
					,MAX(IF(idpoli='$idpoli' AND H.iddokter='$iddokter' AND H.idkelompokpasien!='1' AND (H.idtujuan='0' OR H.idtujuan='$idtujuan'),H.id,NULL))
					,MAX(IF(idpoli='$idpoli' AND (H.iddokter='$iddokter' OR H.iddokter='0') AND H.idkelompokpasien!='1' AND (H.idtujuan='0' OR H.idtujuan='$idtujuan'),H.id,NULL))
					,MAX(IF((idpoli='$idpoli' OR idpoli='0') AND (H.iddokter='$iddokter' OR H.iddokter='0') AND H.idkelompokpasien!='1' AND (H.idtujuan='0' OR H.idtujuan='$idtujuan'),H.id,NULL))
					) as id
					FROM setting_ranap_tipe H
					WHERE H.asal_pasien ='$asal_pasien'
				) S ON T.id=S.id

			";
		}else{
			if ($idrekanan){
				$q="SELECT tipe_rekanan FROM mrekanan WHERE id='$idrekanan'";
				$tipe_rekanan=$this->db->query($q)->row('tipe_rekanan');
			}
			$q="
				SELECT T.idtipepasien FROM setting_ranap_tipe T INNER JOIN (
					SELECT 
					compare_value_7(
					MAX(IF(idpoli='$idpoli' AND H.iddokter='$iddokter' AND H.idrekanan='$idrekanan' AND H.tipe_rekanan='$tipe_rekanan' AND H.idtujuan='$idtujuan',H.id,NULL))
					,MAX(IF(idpoli='$idpoli' AND H.iddokter='$iddokter' AND H.idrekanan='$idrekanan' AND H.tipe_rekanan='$tipe_rekanan' AND (H.idtujuan='0' OR H.idtujuan='$idtujuan'),H.id,NULL))
					,MAX(IF(idpoli='$idpoli' AND H.iddokter='$iddokter' AND H.idrekanan='$idrekanan' AND (H.tipe_rekanan='$tipe_rekanan' OR H.tipe_rekanan='0') AND (H.idtujuan='0' OR H.idtujuan='$idtujuan'),H.id,NULL))
					,MAX(IF(idpoli='$idpoli' AND H.iddokter='$iddokter' AND H.idrekanan='$idrekanan' AND (H.tipe_rekanan='$tipe_rekanan' OR H.tipe_rekanan='0') AND (H.idtujuan='0' OR H.idtujuan='$idtujuan'),H.id,NULL))
					,MAX(IF(idpoli='$idpoli' AND H.iddokter='$iddokter' AND H.idrekanan='0' AND (H.tipe_rekanan='$tipe_rekanan' OR H.tipe_rekanan='0') AND (H.idtujuan='0' OR H.idtujuan='$idtujuan'),H.id,NULL))
					,MAX(IF(idpoli='$idpoli' AND (H.iddokter='$iddokter' OR H.iddokter='0') AND (H.tipe_rekanan='$tipe_rekanan' OR H.tipe_rekanan='0') AND H.idrekanan='0' AND (H.idtujuan='0' OR H.idtujuan='$idtujuan'),H.id,NULL))
					,MAX(IF((idpoli='$idpoli' OR idpoli='0') AND (H.iddokter='$iddokter' OR H.iddokter='0') AND (H.idrekanan='0' OR H.idrekanan='$idrekanan') AND (H.tipe_rekanan='$tipe_rekanan' OR H.tipe_rekanan='0') AND (H.idtujuan='0' OR H.idtujuan='$idtujuan'),H.id,NULL))
					) as id
					FROM setting_ranap_tipe H
					WHERE H.asal_pasien ='$asal_pasien' AND H.idkelompokpasien='$idkelompokpasien'
				) S ON T.id=S.id
	
			";
			
		}
		// $q="
			// SELECT 
				// compare_value_12(
				// MAX(IF(H.asal_pasien = '$asal_pasien' AND idpoli='$idpoli' AND H.iddokter='$iddokter' AND H.idkelompokpasien='$idkelompokpasien' AND H.idtujuan='$idtujuan' AND H.idrekanan='$idrekanan' AND H.tipe_rekanan=M.tipe_rekanan,H.idtipepasien,NULL))
				// ,MAX(IF(H.asal_pasien = '$asal_pasien' AND idpoli='$idpoli' AND H.iddokter='$iddokter' AND H.idkelompokpasien='$idkelompokpasien' AND H.idtujuan='$idtujuan' AND H.idrekanan='0',H.idtipepasien,NULL))
				// ,MAX(IF(H.asal_pasien = '$asal_pasien' AND idpoli='$idpoli' AND H.iddokter='$iddokter' AND H.idkelompokpasien='$idkelompokpasien' AND H.idtujuan='0' AND H.idrekanan='0',H.idtipepasien,NULL))
				// ,MAX(IF(H.asal_pasien = '$asal_pasien' AND idpoli='$idpoli' AND H.iddokter='0' AND H.idkelompokpasien='0' AND H.idtujuan='0' AND H.idrekanan='0',H.idtipepasien,NULL))
				// ,MAX(IF(H.asal_pasien = '$asal_pasien' AND idpoli='0' AND H.iddokter='0' AND H.idkelompokpasien='0' AND H.idtujuan='0' AND H.idrekanan='0',H.idtipepasien,NULL))
				// ,MAX(IF(H.asal_pasien = '0' AND idpoli='0' AND H.iddokter='0' AND H.idkelompokpasien='0' AND H.idtujuan='0' AND H.idrekanan='0',H.idtipepasien,NULL))
				// ,MAX(IF(H.asal_pasien = '0' AND idpoli='0' AND H.iddokter='0' AND H.idkelompokpasien='0' AND H.idtujuan='$idtujuan' AND H.idrekanan='$idrekanan',H.idtipepasien,NULL))
				// ,MAX(IF(H.asal_pasien = '0' AND idpoli='0' AND H.iddokter='0' AND H.idkelompokpasien='0' AND H.idtujuan='0' AND H.idrekanan='$idrekanan',H.idtipepasien,NULL))
				// ,MAX(IF(H.asal_pasien = '0' AND idpoli='0' AND H.iddokter='0' AND H.idkelompokpasien='$idkelompokpasien' AND H.idtujuan='0',H.idtipepasien,NULL))
				// ,MAX(IF(H.asal_pasien = '0' AND idpoli='0' AND H.iddokter='$iddokter' AND H.idkelompokpasien='0' AND H.idtujuan='0',H.idtipepasien,NULL))
				// ,MAX(IF(H.asal_pasien = '0' AND idpoli='$idpoli' AND H.iddokter='0' AND H.idkelompokpasien='0' AND H.idtujuan='0',H.idtipepasien,NULL))
				// ,MAX(IF(H.asal_pasien = '$asal_pasien' AND idpoli='0' AND H.iddokter='0' AND H.idkelompokpasien='0' AND H.idtujuan='0',H.idtipepasien,NULL))


				// ) as idtipepasien


				// FROM setting_ranap_tipe H
				// LEFT JOIN mrekanan M ON M.id='$idrekanan'
		// ";
		// print_r($q);exit;
		$result = $this->db->query($q)->row_array();
		if ($result==null){
			$result['idtipepasien']=1;
		}else{
			if ($result['idtipepasien']==null){
				$result['idtipepasien']=1;
			}
		}
		$this->output->set_output(json_encode($result));
	}
	public function getBed()
	{
		$idruangan=$this->input->post('idruangan');
		$idkelas=$this->input->post('idkelas');
		$id=$this->input->post('id');
		
		$result = $this->Tpendaftaran_ranap_model->getBed($idruangan, $idkelas, $id);
		$this->output->set_output(json_encode($result));
	}
	public function getBedAll()
	{
		$idruangan=$this->input->post('idruangan');
		$idkelas=$this->input->post('idkelas');
		$id=$this->input->post('id');
		
		$result = $this->Tpendaftaran_ranap_model->getBedAll($idruangan, $idkelas, $id);
		$this->output->set_output(json_encode($result));
	}
	public function getDataPasien($id)
	{
		$data = $this->Tpendaftaran_ranap_model->get_pasien($id);
		// if ($row){
		// $data=$this->get_record_pasien($id,$row);
		// }else{

		// }
		// $data = get_all('mfpasien',array('id' => $id));
		$this->output->set_output(json_encode($data));
	}
	function edit_pendaftaran($id){
		insert_awal_history_bed();
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$this->Tpendaftaran_ranap_model->insert_lm($id);
		$this->Tpendaftaran_ranap_model->insert_gc($id);
		$this->Tpendaftaran_ranap_model->insert_hk($id);
		$data=$this->Tpendaftaran_ranap_model->get_edit_pendaftaran($id);
		// print_r($data);exit;
		$data['title_nama']=$data['title'];
		$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("-7 days"));
			$date2= date_format($date2,"d/m/Y");
		if ($data){
			$data['tanggal_1'] 			= $date2;
			$data['tanggal_2'] 			= date('d/m/Y');
			// $data['statuskawin']=$data['statuskawin'];
			$tanggallahir=DMYFormat2($data['tanggal_lahir']);
			$data['tgl_hari']=substr($tanggallahir,0,2);
			$data['tgl_bulan']=substr($tanggallahir,3,2);
			$data['tgl_tahun']=substr($tanggallahir,6,4);
			if ($data['foto_kartu_master']!=''){
				if ($data['foto_kartu']==''){
					$data['foto_kartu']=$data['foto_kartu_master'];
				}
			}
			$q="SELECT  H.*,MD.nama 
			FROM trawatinap_pendaftaran_dpjp H
			INNER JOIN mdokter MD ON MD.id=H.iddokter
			WHERE H.pendaftaran_id='$id'";
			$data['list_dokter_pendamping']=$this->db->query($q)->result();
			$tanggallahir=DMYFormat2($data['tanggal_lahirpenanggungjawab']);
			$data['tgl_lahirpenanggungjawab']=substr($tanggallahir,0,2);
			$data['bulan_lahirpenanggungjawab']=substr($tanggallahir,3,2);
			$data['tahun_lahirpenanggungjawab']=substr($tanggallahir,6,4);
			$data['login_ppa_id']=$login_ppa_id;
			
			$tanggallahir=DMYFormat2($data['tanggal_lahirpengantar']);
			$data['tgl_lahirpengantar']=substr($tanggallahir,0,2);
			$data['bulan_lahirpengantar']=substr($tanggallahir,3,2);
			$data['tahun_lahirpengantar']=substr($tanggallahir,6,4);
			
			$data['list_asuransi'] 			= $this->Tpendaftaran_ranap_model->list_asuransi();
			
			// print_r($tanggallahir.'-'.$data['tgl_bulan']);exit;
			$data['tanggaldaftar']=DMYFormat($data['tanggaldaftar']);
			$data['waktudaftar']=HISTimeFormat($data['tanggaldaftar']);
		}
		// print_r($data);exit;
		$data['list_kontak'] 			= $this->Tpendaftaran_ranap_model->list_kontak($data['idpasien']);
		// $data['list_asuransi'] 			= $this->Tpendaftaran_ranap_model->list_asuransi();
		$data['list_antrian'] 			= $this->Tcheckin_model->list_antrian($data['antrian_id']);
		// $data['list_dokter'] 			= $this->Tpendaftaran_ranap_model->list_dokter_poli($data['idpoli']);
		
		$data['idruangan_default'] 			= 1;
		$data['idkelas_default'] 			= 3;
		$data['idbed_default'] 			= 0;
		$data['list_cara_bayar'] 			= $this->Tpendaftaran_ranap_model->list_cara_bayar();
		// $data['tanggaldaftar'] 			= date('d-m-Y');
		// $data['waktudaftar'] 			= date('H:i:s');
		$setting_lm=$this->Tpendaftaran_ranap_model->setting_lm();
		$setting_sp=$this->Tpendaftaran_ranap_model->setting_sp();
		$setting_hk=$this->Tpendaftaran_ranap_model->setting_hk();
		// $setting_gc=$this->Tpendaftaran_ranap_model->setting_gc();
		
		// $data['statuspasienbaru'] 			= '0';
		$tab='1';
		$data['tab'] 			= $tab;
		if ($data['st_gc']){	
			
			$data_gc=$this->Tpendaftaran_ranap_model->get_gc($id);

			$data = array_merge($data, $data_gc);
		}
		
		
		if ($data['st_hk']=='1' && $data['st_hak']=='0'){	
			
			$data['tab']='6';
		}
		if ($data['st_gc']=='1' && $data['st_general']=='0'){	
			$data['tab']='5';
		}
		if ($data['st_sp']=='1' && $data['st_pernyataan']=='0'){	
			$data['tab']='4';
		}
		if ($data['st_lm']=='1' && $data['st_lembar']=='0'){	
			$data['tab']='3';
		}
		// $data['tab'] 			= $tab;
		$data['error'] 			= '';
		$data['title'] 			= 'Pendaftaran Rawat Inap';
		$data['content'] 		= 'Tpendaftaran_ranap/add';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Pendaftaran",'#'),
											  array("Edit Rawat Inap",'tpendaftaran_ranap')
											);
		$data = array_merge($data,$setting_lm,$setting_sp,$setting_hk, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function save_register_poli(){
		// print_r($this->input->post());exit;
		$id = $this->input->post('id');
		$btn_simpan = $this->input->post('btn_simpan');
		if ($btn_simpan=='1' || $btn_simpan=='2'){
			
			if ($id == '') {
				$id=$this->Tpendaftaran_ranap_model->saveData();
				// print_r($id);exit;
				if ($id) {
					$data = [];
					$data['idpendaftaran'] = $id;
					$data = array_merge($data, backend_info());
					 $this->session->set_flashdata('confirm', true);
					$this->session->set_flashdata('message_flash', 'Sudah Tersimpan !');
					if ($btn_simpan=='1'){
					redirect('tpendaftaran_ranap/edit_pendaftaran/'.$id,'location');	
					// redirect('tpendaftaran_ranap','location');	
						
					}else{
						
					redirect('tpendaftaran_ranap','location');	
					}
				}
			} else {
		// print_r($this->input->post());exit;
				if ($this->Tpendaftaran_ranap_model->updateData($id)) {
					$data = [];
					$data['idpendaftaran'] = $id;
					$data = array_merge($data, backend_info());
					 $this->session->set_flashdata('confirm', true);
					$this->session->set_flashdata('message_flash', 'Sudah Tersimpan !');
					if ($btn_simpan=='1'){
						redirect('tpendaftaran_ranap/edit_pendaftaran/'.$id,'location');	
						
					}else{
						
						redirect('tpendaftaran_ranap','location');	
					}
				}
			}
		}
		if ($btn_simpan=='3' || $btn_simpan=='31'){//Update LM
			$id = $this->input->post('id');
			$btn_simpan = $this->input->post('btn_simpan');
			$data=array(
				'st_lm' =>1,
				'st_lembar' =>1,
				'user_lm' =>$this->session->userdata('user_name'),
				'tanggal_lm' =>date('Y-m-d H:i:s'),
			);
			// if ($btn_simpan=='41'){//Selesai
				// // $data['status_reservasi']=3;
				// // $data['checkin_date']=date('Y-m-d H:i:s');
				// // $data['checkin_by']= $this->session->userdata('user_id');
			// }
			$this->db->where('id',$id);
			$hasil=$this->db->update('trawatinap_pendaftaran',$data);
			if ($hasil){			
				redirect('tpendaftaran_ranap/edit_pendaftaran/'.$id,'location');						
				// if ($btn_simpan=='2'){//Selesai
					// redirect('tcheckin','location');						
				// }else{
				// }
			}
		}
		if ($btn_simpan=='4' || $btn_simpan=='41'){//Update LP
			$id = $this->input->post('id');
			$btn_simpan = $this->input->post('btn_simpan');
			$data=array(
				'st_sp' =>1,
				'st_pernyataan' =>1,
				'user_sp' =>$this->session->userdata('user_name'),
				'tanggal_sp' =>date('Y-m-d H:i:s'),
			);
			// if ($btn_simpan=='41'){//Selesai
				// // $data['status_reservasi']=3;
				// // $data['checkin_date']=date('Y-m-d H:i:s');
				// // $data['checkin_by']= $this->session->userdata('user_id');
			// }
			$this->db->where('id',$id);
			$hasil=$this->db->update('trawatinap_pendaftaran',$data);
			if ($hasil){			
				redirect('tpendaftaran_ranap/edit_pendaftaran/'.$id,'location');						
				// if ($btn_simpan=='2'){//Selesai
					// redirect('tcheckin','location');						
				// }else{
				// }
			}
		}
		if ($btn_simpan=='5' || $btn_simpan=='51'){//Update GC
			$id = $this->input->post('id');
			$btn_simpan = $this->input->post('btn_simpan');
			$data=array(
				'st_gc' =>1,
				'st_general' =>1,
				'user_gc' =>$this->session->userdata('user_name'),
				'tanggal_gc' =>date('Y-m-d H:i:s'),
			);
			// if ($btn_simpan=='41'){//Selesai
				// // $data['status_reservasi']=3;
				// // $data['checkin_date']=date('Y-m-d H:i:s');
				// // $data['checkin_by']= $this->session->userdata('user_id');
			// }
			$this->db->where('id',$id);
			$hasil=$this->db->update('trawatinap_pendaftaran',$data);
			if ($hasil){			
				redirect('tpendaftaran_ranap/edit_pendaftaran/'.$id,'location');						
				// if ($btn_simpan=='2'){//Selesai
					// redirect('tcheckin','location');						
				// }else{
				// }
			}
		}
		
		if ($btn_simpan=='6' || $btn_simpan=='61'){//Update Covid
			$id = $this->input->post('id');
			$btn_simpan = $this->input->post('btn_simpan');
			$data=array(
				'st_hk' =>1,
				'st_hak' =>1,
				'user_hk' =>$this->session->userdata('user_name'),
				'tanggal_hk' =>date('Y-m-d H:i:s'),
			);
			// if ($btn_simpan=='2'){//Selesai
				// $data['status_reservasi']=3;
				// $data['checkin_date']=date('Y-m-d H:i:s');
				// $data['checkin_by']= $this->session->userdata('user_id');
			// }
			$this->db->where('id',$id);
			$hasil=$this->db->update('trawatinap_pendaftaran',$data);
			if ($hasil){	
				redirect('tpendaftaran_ranap','location');	
				// if ($btn_simpan=='2'){//Selesai
					// redirect('tcheckin','location');						
				// }else{
					// redirect('tcheckin/detail/'.$id,'location');						
				// }
			}
		}
	}
	
	public function save_kartu()
    {
		
        $id_kartu = $this->input->post('id_kartu');
        $idpasien = $this->input->post('idpasien');
        $kelompokpasien = $this->input->post('kelompokpasien');
        $idrekanan = $this->input->post('idrekanan');
        $nokartu = $this->input->post('nokartu');
        $nama_partisipant = $this->input->post('nama_partisipant');
        $data =array(
            'idpasien'=>$idpasien,
            'kelompokpasien'=>$kelompokpasien,
            'idrekanan'=>$idrekanan,
            'nokartu'=>$nokartu,
            'nama_partisipant'=>$nama_partisipant,
            'staktif'=>'1',
        );
		if ($id_kartu){
			$data['edited_by']=$this->session->userdata('user_id');
			$data['delted_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$id_kartu);
			$result=$this->db->update('mfpasien_kartu', $data);			
		}else{
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$result=$this->db->insert('mfpasien_kartu', $data);
		// print_r($this->db->_error_message());exit;
			
		}
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function hapus_kartu()
    {
		
        $id_kartu = $this->input->post('id');
        $data =array(
            'staktif'=>'0',
            'deleted_by'=>$this->session->userdata('user_id'),
            'delted_date'=>date('Y-m-d H:i:s'),
        );
		$this->db->where('id',$id_kartu);
		$result=$this->db->update('mfpasien_kartu', $data);	
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function hapus_record_daftar()
    {
		
        $id = $this->input->post('id');
        $keterangan_hapus = $this->input->post('keterangan_hapus');
        $alasan_hapus_id = $this->input->post('alasan_id');
        $data =array(
            'status'=>0,
            'keterangan_hapus'=>$keterangan_hapus,
            'alasan_hapus_id'=>$alasan_hapus_id,
            'deleted_by'=>$this->session->userdata('user_id'),
            'deleted_date'=>date('Y-m-d H:i:s'),
        );
		$this->db->where('id',$id);
		$result=$this->db->update('trawatinap_pendaftaran', $data);	
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function terima_ranap()
    {
		
        $id = $this->input->post('id');
        $tanggalterima = $this->input->post('tanggalterima');
        $waktuterima = $this->input->post('waktuterima');
		$tanggal_terima=YMDTimeFormat($tanggalterima.' '.$waktuterima);
		// print_r($tanggal_terima);exit;
        $data =array(
            'st_terima_ranap'=>1,
            'tanggal_terima'=>$tanggal_terima,
            'user_terima'=>$this->session->userdata('user_id'),
            'user_nama_terima'=>$this->session->userdata('user_name'),
        );
		$this->db->where('id',$id);
		$result=$this->db->update('trawatinap_pendaftaran', $data);	
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function aktifkan_kartu()
    {
		
        $id_kartu = $this->input->post('id');
        $data =array(
            'staktif'=>'1',
            // 'deleted_by'=>$this->session->userdata('user_id'),
            // 'delted_date'=>date('Y-m-d H:i:s'),
        );
		$this->db->where('id',$id_kartu);
		$result=$this->db->update('mfpasien_kartu', $data);	
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	function LoadKartu()
    {
		$idpasien     		= $this->input->post('idpasien');
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT H.id,H.kelompokpasien,MP.nama as nama_kelompok,MR.nama as nama_rekanan,H.nokartu,H.nama_partisipant,H.staktif,H.foto_kartu FROM `mfpasien_kartu` H
					LEFT JOIN mpasien_kelompok MP ON MP.id=H.kelompokpasien
					LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
					WHERE H.idpasien='$idpasien'
			) as tbl";
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('nama_partisipant','nokartu','nama_kelompok','nama_rekanan');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			$nama='';
			if ($r->kelompokpasien=='1'){
				$nama=$r->nama_kelompok.' ('.$r->nama_rekanan.')';
			}else{
				$nama=$r->nama_kelompok;
				
			}
            $row[] = $no;
            $row[] = $nama;
            // $row[] = $no;
            // $row[] = $r->kodebank;
            $row[] = $r->nokartu;
            $row[] = $r->nama_partisipant.' ';
			if ($r->staktif){
				$row[] = '<span class="label label-success">Active</span>';			
			}else{
				$row[] = '<span class="label label-danger">Not Active</span>';
				
			}
			$aksi = '<div class="btn-group">';
			if ($r->staktif=='0'){
				$aksi .= '<button data-toggle="tooltip" title="Edit" class="btn btn-success btn-xs" onclick="aktifkan_kartu('.$r->id.')"><i class="fa fa-pencil"></i> Aktifkan</button>';
				
			}else{
				
				$aksi .= '<button data-toggle="tooltip" title="Pilih Kartu" class="btn btn-success btn-xs" onclick="pilih_kartu('.$r->id.')"><i class="fa fa-check"></i> Pilih</button>';
				if ($r->foto_kartu){
				$aksi .= '<a href="'.base_url().'/assets/upload/foto_kartu/'.$r->foto_kartu.'" target="_blank" title="Lihat Kartu" class="btn btn-default btn-xs" onclick="libat_kartu('.$r->id.')"><i class="fa fa-file-image-o"></i></a>';
				$aksi .= '<button data-toggle="tooltip" title="Upload Kartu" class="btn btn-warning btn-xs" onclick="upload_kartu('.$r->id.')"><i class="fa fa-upload"></i></button>';
					
				}else{
				$aksi .= '<button data-toggle="tooltip" title="Upload Kartu" class="btn btn-danger btn-xs" onclick="upload_kartu('.$r->id.')"><i class="fa fa-upload"></i></button>';
					
				}
				$aksi .= '<button data-toggle="tooltip" title="Edit" class="btn btn-primary btn-xs" onclick="edit_kartu('.$r->id.')"><i class="fa fa-pencil"></i></button>';
				$aksi .= '<button data-toggle="tooltip" title="History" class="btn btn-warning btn-xs" onclick="lihat_history_kartu('.$idpasien.','.$r->id.')"><i class="si si-list"></i> </button>';
				$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-xs" onclick="hapus_kartu('.$r->id.')"><i class="fa fa-trash"></i></button>';
			}
			// $aksi .= '<a href="#btabs-animated-slideleft-add"><i class="fa fa-pencil"></i> Add / Edit Rekening</a>';
			$aksi .= '</div>';
            $row[] = $aksi;
            
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	public function upload_kartu() {
       $uploadDir = './assets/upload/foto_kartu';
		if (!empty($_FILES)) {
			 $id_foto_add = $this->input->post('id_foto_add');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['foto_kartu']		= $file_name;
			$this->db->where('id',$id_foto_add);
			$this->db->update('mfpasien_kartu', $detail);
		}
    }
	function get_logic_pendaftaran(){
		$idtujuan = ($this->input->post('idtujuan')=='1'?3:4);
		$iddokter=$this->input->post('iddokter');
		
		$q="SELECT T.st_lembar_masuk as st_lm,T.st_pernyataan as st_sp,T.st_general as st_gc,T.st_hak_kewajiban as st_hk FROM (
			SELECT compare_value_4( 
			MAX(IF(H.idtujuan = '$idtujuan' AND iddokter='$iddokter',H.id,NULL))
			,MAX(IF(H.idtujuan = '$idtujuan' AND iddokter='0',H.id,NULL))
			,MAX(IF(H.idtujuan = '0' AND iddokter='$iddokter',H.id,NULL))
			,MAX(IF(H.idtujuan = '0' AND iddokter='0',H.id,NULL))
			) as id
			FROM setting_ranap_form H
			) S 
			LEFT JOIN setting_ranap_form T ON T.id=S.id";
		// print_r($q);exit;
		$st_lm='0';$st_sp='0';$st_gc='0';$st_hk='0';
		$hasil=$this->db->query($q)->row();
		if ($hasil){
		  $st_lm=$hasil->st_lm;
		  $st_gc=$hasil->st_gc;
		  $st_sp=$hasil->st_sp;
		  $st_hk=$hasil->st_hk;
		}
		$text_skrining='';
		$text_skrining .=' '.($st_lm?'<span class="label label-success"><i class="fa fa-check"></i> Lembar Masuk</span>':'<span class="label label-danger"><i class="fa fa-times-circle"></i> Lembar Masuk</span>');
		$text_skrining .=' '.($st_sp?'<span class="label label-success"><i class="fa fa-check"></i> Lembar Pernyataan</span>':'<span class="label label-danger"><i class="fa fa-times-circle"></i> Lembar Pernyataan</span>');
		$text_skrining .=' '.($st_gc?'<span class="label label-success"><i class="fa fa-check"></i> General Consent</span>':'<span class="label label-danger"><i class="fa fa-times-circle"></i> General Consent</span>');
		$text_skrining .=' '.($st_hk?'<span class="label label-success"><i class="fa fa-check"></i> Hak & Kewajiban</span>':'<span class="label label-danger"><i class="fa fa-times-circle"></i> Hak & Kewajiban</span>');
		
		$arr['text_skrining']=$text_skrining;
		$arr['st_lm']=$st_lm;
		$arr['st_sp']=$st_sp;
		$arr['st_gc']=$st_gc;
		$arr['st_hk']=$st_hk;
		$this->output->set_output(json_encode($arr));
	}
	function get_poli(){
		$idtipe=$this->input->post('idtipe');
		$reservasi_tipe_id=$this->input->post('reservasi_tipe_id');
		if ($reservasi_tipe_id=='1'){//DOKTER
			$q="SELECT *FROM mpoliklinik M WHERE M.`status`='1' AND M.idtipe='$idtipe' AND M.id NOT IN (SELECT idpoli FROM app_reservasi_poli)";
		}else{
			$q="SELECT *FROM mpoliklinik M WHERE M.`status`='1' AND M.idtipe='$idtipe'  AND M.id IN (SELECT idpoli FROM app_reservasi_poli)";
			
		}
		// print_r($q);exit;
		$result= $this->db->query($q)->result();
		$this->output->set_output(json_encode($result));
	}
	public function get_dokter_poli()
	{
		$idpoli=$this->input->post('idpoli');
		$q="SELECT MD.id,MD.nama FROM mpoliklinik_dokter H
			INNER JOIN mdokter MD ON MD.id=H.iddokter
			WHERE H.idpoliklinik='$idpoli'";
		$data = $this->db->query($q)->result();
		$this->output->set_output(json_encode($data));
	}
	public function get_jadwal_dokter_poli()
	{
		$reservasi_tipe_id=$this->input->post('reservasi_tipe_id');
		$idpoli=$this->input->post('idpoli');
		$iddokter=$this->input->post('iddokter');
		if ($reservasi_tipe_id=='1'){
			
			// $q="SELECT H.tanggal,CASE WHEN ML.id IS NOT NULL THEN 'LIBUR NASIONAL' ELSE MC.sebab END sebab,CASE WHEN ML.id IS NOT NULL THEN ML.id ELSE MC.id END as cuti_id
				// ,CASE 
				// WHEN MC.id IS NOT NULL THEN 'disabled' 
				// WHEN ML.id IS NOT NULL THEN 'disabled' 

				// ELSE '' END st_disabel 
				// ,CONCAT(MR.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y') )as tanggal_nama,H.kodehari
				// FROM `app_reservasi_tanggal` H
				// LEFT JOIN mcuti_dokter MC ON MC.iddokter=H.iddokter AND H.tanggal BETWEEN MC.tanggal_dari AND MC.tanggal_sampai AND MC.`status`='1'
				// LEFT JOIN mholiday ML ON H.tanggal  BETWEEN ML.tgl_libur_awal AND ML.tgl_libur_akhir
				// LEFT JOIN merm_hari MR ON MR.kodehari=H.kodehari
				// WHERE H.reservasi_tipe_id='1' AND H.iddokter='$iddokter' AND H.idpoli='$idpoli' AND H.tanggal >= CURRENT_DATE AND H.tanggal<DATE_ADD(CURRENT_DATE, INTERVAL 7 DAY) 
				// GROUP BY H.tanggal";
					$q="SELECT 
						CONCAT(MR.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y') )as tanggal_nama
						,H.tanggal,D.saldo_kuota,D.jadwal_id,MJ.id,MJ.kuota
						,CASE 
								WHEN MC.id IS NOT NULL THEN CONCAT('CUTI' ,' ',MC.alasan)
								WHEN ML.id IS NOT NULL THEN CONCAT('Libur' ,' ',ML.nama_libur)
								WHEN H.hari='1' THEN 'Libur' 
							
								WHEN (D.saldo_kuota IS NULL OR D.saldo_kuota<=0) AND MJ.id IS NOT NULL THEN 'TIDAK ADA KUOTA' 
								WHEN MJ.id IS NULL THEN 'TIDAK ADA JADWAL' 
								WHEN MJ.id IS NOT NULL AND MJ.kuota<=0 THEN 'TIDAK ADA KUOTA'
								WHEN MJ.id IS NULL AND jadwal_antara(H.tanggal,11,1)=1 THEN 'TIDAK ANTARA JADWAL'
								END AS ket
						,CASE 
								WHEN MC.id IS NOT NULL THEN 'disabled' 
								WHEN ML.id IS NOT NULL THEN 'disabled' 
								WHEN H.hari='1' THEN 'disabled' 
								WHEN (D.saldo_kuota IS NULL OR D.saldo_kuota<=0) AND MJ.id IS NOT NULL AND  H.tanggal!=CURRENT_DATE() THEN 'disabled' 
								WHEN MJ.id IS NULL AND jadwal_antara(H.tanggal,11,1)=1 THEN ''
								WHEN MJ.id IS NOT NULL AND H.tanggal!=CURRENT_DATE() AND MJ.kuota<=0 THEN 'disabled' 
								WHEN MJ.id IS NULL AND H.tanggal!=CURRENT_DATE() THEN 'disabled' 
								
										
								ELSE '' END st_disabel
						FROM date_row H
					LEFT JOIN merm_hari MR ON MR.kodehari=H.hari
					LEFT JOIN app_reservasi_tanggal D ON D.iddokter='$iddokter' AND D.idpoli='$idpoli' AND D.tanggal=H.tanggal
					LEFT JOIN mjadwal_dokter  MJ ON MJ.iddokter='$iddokter' AND MJ.idpoliklinik='$idpoli' AND MJ.kodehari=H.hari
					LEFT JOIN mholiday ML ON H.tanggal  BETWEEN ML.tgl_libur_awal AND ML.tgl_libur_akhir
					LEFT JOIN mcuti_dokter MC ON MC.iddokter='$iddokter' AND H.tanggal BETWEEN MC.tanggal_dari AND MC.tanggal_sampai AND MC.`status`='1'
					WHERE H.tanggal BETWEEN DATE_ADD(CURRENT_DATE(),INTERVAL -2 DAY) AND DATE_ADD(CURRENT_DATE(),INTERVAL 7 DAY)
					GROUP BY H.tanggal";
		}else{
			$q="SELECT H.tanggal
				,CONCAT(MR.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y') )as tanggal_nama,H.kodehari
				,CASE WHEN ML.id IS NOT NULL THEN 'disabled' ELSE '' END as st_disabel
				FROM `app_reservasi_tanggal` H
				LEFT JOIN mholiday ML ON H.tanggal  BETWEEN ML.tgl_libur_awal AND ML.tgl_libur_akhir
				LEFT JOIN merm_hari MR ON MR.kodehari=H.kodehari
				WHERE H.reservasi_tipe_id='2' AND H.idpoli='$idpoli' AND H.tanggal >= CURRENT_DATE AND H.tanggal<DATE_ADD(CURRENT_DATE, INTERVAL 7 DAY) 
				GROUP BY H.tanggal";
		}
		// print_r($q);exit;
		$data = $this->db->query($q)->result();
		$this->output->set_output(json_encode($data));
	}
	public function get_jadwal_dokter_igd()
	{
		$reservasi_tipe_id=$this->input->post('reservasi_tipe_id');
		$idpoli=$this->input->post('idpoli');
		$iddokter=$this->input->post('iddokter');
		$id=$this->input->post('id');
		$tmp_tanggal=$this->input->post('tmp_tanggal');
		
		if ($id){
			$q2="UNION ALL

			SELECT H.tanggal, CONCAT(MH.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y') )as tanggal_nama,H.hari as kodehari FROM date_row H 
			LEFT JOIN merm_hari MH ON MH.kodehari=H.hari
			WHERE H.tanggal='$tmp_tanggal'";
			$q='';
		}else{
			$q2='';
		}
		$q="SELECT H.tanggal, CONCAT(MH.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y') )as tanggal_nama,H.hari as kodehari FROM date_row H 
			LEFT JOIN merm_hari MH ON MH.kodehari=H.hari
			WHERE H.tanggal=CURRENT_DATE()

			".$q2;
		// print_r($q);exit;
		$data = $this->db->query($q)->result();
		$this->output->set_output(json_encode($data));
	}
	public function get_jam_dokter_poli()
	{
		$reservasi_tipe_id=$this->input->post('reservasi_tipe_id');
		$idpoli=$this->input->post('idpoli');
		$iddokter=$this->input->post('iddokter');
		$tanggal=$this->input->post('tanggal');
		if ($reservasi_tipe_id=='1'){
			
			$q="SELECT H.jadwal_id,CONCAT(DATE_FORMAT(JD.jam_dari,'%H:%i') ,' - ',DATE_FORMAT(JD.jam_sampai,'%H:%i') ) jam_nama
			,H.saldo_kuota,CASE WHEN H.saldo_kuota > 0 THEN '' ELSE 'disabled' END st_disabel
			FROM `app_reservasi_tanggal` H
			LEFT JOIN mjadwal_dokter JD ON JD.id=H.jadwal_id
			WHERE H.reservasi_tipe_id='1' AND H.iddokter='$iddokter' AND H.idpoli='$idpoli' AND H.tanggal = '$tanggal'
			GROUP BY H.tanggal,H.jadwal_id";
		}else{
			$q="SELECT H.jam_id,CONCAT(JD.jam ,'.00 - ',JD.jam_akhir,'.00' ) jam_nama
			,H.saldo_kuota,CASE WHEN COALESCE(H.saldo_kuota)  > 0 THEN '' ELSE 'disabled' END st_disabel
			FROM `app_reservasi_tanggal` H
			LEFT JOIN merm_jam JD ON JD.jam_id=H.jam_id
			WHERE H.reservasi_tipe_id='2' AND H.idpoli='$idpoli' AND H.tanggal = '$tanggal' AND H.saldo_kuota IS NOT NULL
			GROUP BY H.tanggal,H.jam_id";
		}
		$data = $this->db->query($q)->result();
		$this->output->set_output(json_encode($data));
	}
	function get_last_kunjungan($idpasien,$id=''){
		$q="SELECT MP.idtipe,CONCAT(CASE WHEN MP.idtipe=1 THEN 'Poliklinik' ELSE 'IGD' END,' - ',MP.nama) as nama_poli,MD.nama as nama_dokter
			,DATE_FORMAT(H.tanggaldaftar,'%d-%m-%Y %H:%i') as tanggaldaftar FROM `tpoliklinik_pendaftaran` H
			LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
			LEFT JOIN mdokter MD ON MD.id=H.iddokter
			WHERE H.idpasien='$idpasien' AND H.id !='$id'
			ORDER BY H.id DESC
			LIMIT 1";
		$result=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($result));
	}
	function load_kontak($idpasien){
		$q="SELECT *FROM mfpasien_kontak WHERE idpasien='$idpasien' AND staktif='1'";
		
		$result=$this->db->query($q)->result();
		$tabel='';
		foreach($result as $r){
			$level_kontak_id=$r->level_kontak;
			$tabel .='<tr>
						<td>
								<select tabindex="23" name="level_kontak[]" class="js_select2  form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
									<option value="2" '.($level_kontak_id=='2'?'selected':'').'>Secondary</option>
									<option value="1" '.($level_kontak_id=='1'?'selected':'').'>Primary</option>
								</select>
						</td>
						<td>
								<select tabindex="23" name="jenis_kontak[]" class="js_select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
									<option value="">Pilih Opsi</option>
									'.$this->jenis_kontal_list($r->jenis_kontak).'
									
								</select>
						</td>
						<td>
								<input tabindex="29" type="text" class="form-control" placeholder="Kontak" name="nama_kontak[]" value="'.$r->nama_kontak.'"  >
								<input type="hidden" name="id_kontak_tambahan[]" value="'.$r->id.'">
						</td>
						<td>
							<div class="btn-group">
								<button class="btn btn-xs btn-danger" onclick="hapus_kontak_id(this,'.$r->id.')" type="button" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-trash"></i></button>
							</div>
						</td>
					</tr>';
			
		}
		$this->output->set_output(json_encode($tabel));
	}
	function load_bahasa($idpasien=''){
		$q="SELECT H.nilai as id,H.ref as nama
			,CASE WHEN B.bahasa_id IS NOT NULL THEN 'selected' ELSE '' END pilih

			FROM merm_referensi H
			LEFT JOIN mfpasien_bahasa B ON B.idpasien='$idpasien' AND H.nilai=B.bahasa_id
			WHERE H.ref_head_id='5'";
		$opsi='';
		// print_r($q);exit;
		$result=$this->db->query($q)->result();
		foreach($result as $r){
			$opsi .='<option value="'.$r->id.'" '.$r->pilih.'>'.$r->nama.'</option>';
		}
		$this->output->set_output(json_encode($opsi));
	}
	function jenis_kontal_list($id){
		$opsi='';
		foreach(list_variable_ref(20) as $row){
		$opsi.='<option value="'.$row->id.'" '.($id == $row->id ? 'selected="selected"' : '').'>'.$row->nama.'</option>';
		}
		return $opsi;
	}
	function load_kartu(){
		$id=$this->input->post('id');
		$q="SELECT *FROM mfpasien_kartu H WHERE H.id='$id'";
		$result=$this->db->query($q)->row_array();
		if ($result['foto_kartu']){
		$result['url_foto']=base_url().'assets/upload/foto_kartu/'.$result['foto_kartu'];
			
		}else{
			
		$result['url_foto']=base_url().'assets/upload/foto_kartu/default.png';
		}
		$this->output->set_output(json_encode($result));
	}
	function get_pasien($idpasien){
		$q="SELECT M.no_medrec,M.nama FROM mfpasien M
WHERE M.id='$idpasien'";
		$result=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($result));
	}
	function loadDataPasienHistory(){
		
			$idpasien =$this->input->post('idpasien');
			
			
			$this->select = array();
			$from="
					(
						SELECT H.id,H.tanggaldaftar,H.idtipe,MP.nama as nama_poli,MD.nama as nama_dokter,H.iduserinput,MU.`name` as nama_user,
						H.`status`,H.statusrencana,H.statustindakan,H.rencana

						FROM tpoliklinik_pendaftaran H
						INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
						LEFT JOIN mdokter MD ON MD.id=H.iddokter
						LEFT JOIN musers MU ON MU.id=H.iduserinput
						WHERE H.idpasien='$idpasien'
						ORDER BY id DESC
					) as tbl WHERE id IS NOT NULL 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','nama_poli');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		 
          $result[] = $no;
          $result[] = HumanDateLong($r->tanggaldaftar);
          $result[] = GetAsalPasienRujukan($r->idtipe);
          $result[] = $r->nama_poli;
          $result[] = $r->nama_dokter;
          $result[] = ($r->nama_user==''?text_primary('RESERVASI MANDIRI'):$r->nama_user);
          $result[] = ($r->statustindakan=='1'?text_primary('TELAH DITINDAK'):text_danger('BELUM DITINDAK'));
          $result[] = GetStatusRencana($r->rencana);
         
          $aksi = '<div class="btn-group">';
			$aksi .= '<a href="'.base_url().'tpoliklinik_pendaftaran/update/'.$r->id.'" type="button" data-toggle="tooltip" title="Lihat Detail" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';
			
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
	}
	
	
	function detail($id,$antrian_id=''){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($antrian_id!=''){
			$this->db->where('id',$id);
			$this->db->update('tpoliklinik_pendaftaran',array('antrian_id'=>$antrian_id));
		}
		$data=$this->Tpendaftaran_ranap_model->load_edit_poli($id);
		// print_r($data);exit;
		$tanggallahir=DMYFormat2($data['tanggal_lahir']);
		$data['tgl_hari']=substr($tanggallahir,0,2);
		$data['tgl_bulan']=substr($tanggallahir,3,2);
		// print_r($tanggallahir.'-'.$data['tgl_bulan']);exit;
		$data['tgl_tahun']=substr($tanggallahir,6,4);
		$data['tanggaldaftar']=DMYFormat($data['waktu_reservasi']);
		$data['waktudaftar']=HISTimeFormat($data['waktu_reservasi']);
		$data['ktp']=($data['noidentitas']);
		$data['kelompokpasien']=($data['idkelompokpasien']);
		if ($data['antrian_id']==''){
			$data['antrian_id']=$antrian_id;
		}
		$data['list_poli'] 			= $this->Tpendaftaran_ranap_model->list_poli();
		$data['list_cara_bayar'] 			= $this->Tpendaftaran_ranap_model->list_cara_bayar();
		$data['list_asuransi'] 			= $this->Tpendaftaran_ranap_model->list_asuransi();
		$data['list_dokter'] 			= $this->Tpendaftaran_ranap_model->list_dokter();
		$data['list_antrian'] 			= $this->Tpendaftaran_ranap_model->list_antrian();
		$data['content'] 		= 'Tpendaftaran_ranap/detail';
		$data['breadcrum'] 	= array(
							  array("RSKB Halmahera",'#'),
							  array("Checkin",'#'),
							  array("Poliklinik Reservasi",'tcheckin')
							);
								
		$data['tab']='3';
		// $data['tab']='1';
		// if ($data['st_sp']=='1'){			
			// $data['tab']='5';
		// }
		// if ($data['st_sc']=='1'){			
			// $data['tab']='6';
		// }
		if ($data['reservasi_tipe_id']=='1'){
			$hasil=get_logic_checkin_poli_petugas($data['pertemuan_id'],$data['idpoli'],$data['iddokter']);
		}else{
			$hasil=get_logic_checkin_rehab_petugas($data['pertemuan_id'],$data['idpoli'],$data['iddokter']);
			// print_r($hasil);exit;
		}
		if ($hasil){
			if ($hasil->st_gc=='1' && $data['st_gc']=='0'){
				$data_gc=$this->Tpendaftaran_ranap_model->get_gc($id);
				if (!$data_gc){
					// print_r('tidak _ada');exit;
					$q="INSERT INTO trawatinap_pendaftaran_gc_head (pendaftaran_id,logo,judul,sub_header,sub_header_side,footer_1,footer_2,ttd_1,ttd_2,footer_form_1,footer_form_2)
					SELECT '$id' as pendaftaran_id,logo,judul,sub_header,sub_header_side,footer_1,footer_2,ttd_1,ttd_2,footer_form_1,footer_form_2
					FROM merm_general WHERE id='1'";
					$this->db->query($q);
					$q="INSERT INTO trawatinap_pendaftaran_gc (pendaftaran_id,no,pertanyaan,group_jawaban_id,group_jawaban_nama,jawaban_id,jawaban_nama,jenis_isi)
						SELECT '$id',H.`no`,H.isi,GROUP_CONCAT(J.ref_id) as group_jawaban_id,GROUP_CONCAT(R.ref) as group_jawaban_nama,0 as jawaban_id,null as jawaban_nama,jenis_isi FROM merm_general_isi H
						LEFT JOIN merm_general_isi_jawaban J ON J.general_isi_id=H.id
						LEFT JOIN merm_referensi R ON R.id=J.ref_id
						WHERE H.`status`='1'

						GROUP BY H.id
						ORDER BY H.`no`";
					$this->db->query($q);
					
				}
				$data['st_gc']='1';
				if ($data['st_general']=='1'){
					$data['st_general']=0;
				}
			}
			if ($hasil->st_sp=='1' && $data['st_sp']=='0'){
				$q="SELECT *FROM tpoliklinik_pendaftaran_sp WHERE pendaftaran_id='$id'";
				$data_sp=$this->db->query($q)->row();
					// print_r($data_sp);exit;
				if (!$data_sp){
					$q="INSERT INTO tpoliklinik_pendaftaran_sp (pendaftaran_id,no,pertanyaan,group_jawaban_id,group_jawaban_nama,jawaban_id,jawaban_nama,jenis_isi)
					SELECT '$id',H.`no`,H.isi,GROUP_CONCAT(J.ref_id) as group_jawaban_id,GROUP_CONCAT(R.ref) as group_jawaban_nama,0 as jawaban_id,null as jawaban_nama,jenis_isi 
					FROM merm_skrining_pasien_isi H
					LEFT JOIN merm_skrining_pasien_isi_jawaban J ON J.general_isi_id=H.id
					LEFT JOIN merm_referensi R ON R.id=J.ref_id
					WHERE H.`status`='1'
					GROUP BY H.id
					ORDER BY H.`no`";
					$this->db->query($q);
					
				}
				$data['st_sp']='1';
				if ($data['st_skrining']=='1'){
					$data['st_skrining']=0;
				}
			}
			if ($hasil->st_sc=='1' && $data['st_sc']=='0'){
				$q="SELECT *FROM tpoliklinik_pendaftaran_sc WHERE pendaftaran_id='$id'";
				$data_sc=$this->db->query($q)->row();
				if (!$data_sc){
					// print_r('tidak _ada');exit;
					$q="INSERT INTO tpoliklinik_pendaftaran_sc (pendaftaran_id,no,pertanyaan,group_jawaban_id,group_jawaban_nama,jawaban_id,jawaban_nama,jenis_isi)
					SELECT '$id',H.`no`,H.isi,GROUP_CONCAT(J.ref_id) as group_jawaban_id,GROUP_CONCAT(R.ref) as group_jawaban_nama,0 as jawaban_id,null as jawaban_nama,jenis_isi 
					FROM merm_skrining_covid_isi H
					LEFT JOIN merm_skrining_covid_isi_jawaban J ON J.general_isi_id=H.id
					LEFT JOIN merm_referensi R ON R.id=J.ref_id
					WHERE H.`status`='1'
					GROUP BY H.id
					ORDER BY H.`no`";
					$this->db->query($q);
					
				}
				$data['st_sc']='1';
				if ($data['st_covid']=='1'){
					$data['st_covid']=0;
				}
			}
		}
		if ($data['st_sc']=='1'){	
			
			$data_sc=$this->Tbooking_model->get_sc($id);
			$data = array_merge($data, $data_sc);
		}
		if ($data['st_sp']=='1'){	
			
			$data_sp=$this->Tpendaftaran_ranap_model->get_sp($id);

			$data = array_merge($data, $data_sp);
		}
		if ($data['st_gc']){	
			
			$data_gc=$this->Tpendaftaran_ranap_model->get_gc($id);

			$data = array_merge($data, $data_gc);
		}
		
		if ($data['st_sc']=='1' && $data['st_covid']=='0' && $data['st_skrining']=='1'){	
			
			$data['tab']='6';
		}
		if ($data['st_sp']=='1' && $data['st_general']=='1' && $data['st_skrining']=='0'){	
			
			$data['tab']='5';
		}
		if ($data['st_gc']=='1' && $data['st_general']=='0'){	
			
			$data['tab']='4';
		}
		$data['tab']='3';
		// print_r($data);exit;
		$data['error']='';
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		
	}
	function update_antrian_id(){
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $antrian_id=$this->input->post('antrian_id');
	  $data=array(
		'antrian_id' =>$antrian_id
	  );
	  $this->db->where('id',$pendaftaran_id);
	  $result=$this->db->update('tpoliklinik_pendaftaran',$data);
	  $this->output->set_output(json_encode($result));
		}
	// function getIndex_all()
	// {
			// $data_user=get_acces();
			// $user_acces_form=$data_user['user_acces_form'];
			// // tipe_pemilik:tipe_pemilik,status:status,nama:nama
			// $where='';
			// $reservasi_cara =$this->input->post('reservasi_cara');
			// $statuspasienbaru =$this->input->post('statuspasienbaru');
			// $status_transaksi =$this->input->post('status_transaksi');
			// $status_kehadiran =$this->input->post('status_kehadiran');
			// $tanggal_1 =$this->input->post('tanggal_1');
			// $tanggal_2 =$this->input->post('tanggal_2');
			// $no_medrec =$this->input->post('no_medrec');
			// $namapasien =$this->input->post('namapasien');
			// $idpoli =$this->input->post('idpoli');
			// $iddokter =$this->input->post('iddokter');
			// $waktu_reservasi_1 =$this->input->post('waktu_reservasi_1');
			// $waktu_reservasi_2 =$this->input->post('waktu_reservasi_2');
			// $notransaksi =$this->input->post('notransaksi');
			// $kode_booking =$this->input->post('kode_booking');
			// $tab =$this->input->post('tab');
			// if ($tab=='4'){
				
				// $where .=" AND (H.status) = '0'";
			// }
			// if ($notransaksi !=''){
				// $where .=" AND (H.nopendaftaran) = '".($notransaksi)."'";
			// }
			// if ($kode_booking !=''){
				// $where .=" AND (H.kode_antrian) = '".($kode_booking)."'";
			// }
			// if ($namapasien !=''){
				// $where .=" AND (H.namapasien) LIKE '%".($namapasien)."%'";
			// }
			// if ($no_medrec !=''){
				// $where .=" AND (H.no_medrec) LIKE '%".($no_medrec)."%'";
			// }
			// if ($idpoli !='#'){
				// $where .=" AND (H.idpoliklinik) ='".($idpoli)."'";
			// }
			// if ($iddokter !='#'){
				// $where .=" AND (H.iddokter) ='".($iddokter)."'";
			// }
			// if ($reservasi_cara !='#'){
				// $where .=" AND (H.reservasi_cara) ='".($reservasi_cara)."'";
			// }
			// if ($statuspasienbaru !='#'){
				// $where .=" AND (H.statuspasienbaru) ='".($statuspasienbaru)."'";
			// }
			// if ($status_kehadiran !='#'){
				// // $where .=" AND (H.status_kehadiran) ='".($status_kehadiran)."'";
			// }
			// if ($status_transaksi !='1'){
				// if ($status_transaksi=='2'){
					// $where .=" AND (H.statustindakan) ='1'";
					
				// }
				// // if ($status_reservasi=='2'){
					
				// // }else{
					
				// // $where .=" AND (H.status_reservasi) ='".($status_reservasi)."'";
				// // }
			// }
			
			
			// if ($tanggal_1 !=''){
				// $where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_2)."'";
			// }else{
				// $where .=" AND DATE(H.tanggal) >='".date('Y-m-d')."'";
			// }
			// if ($waktu_reservasi_1 !=''){
				// $where .=" AND DATE(H.tanggaldaftar) >='".YMDFormat($waktu_reservasi_1)."' AND DATE(H.tanggaldaftar) <='".YMDFormat($waktu_reservasi_2)."'";
			// }
			// // print_r($tab);exit;
			// $this->select = array();
			// $from="
					// (
						// SELECT H.id,H.reservasi_tipe_id,H.nopendaftaran,H.tanggal,H.tanggaldaftar,H.idtipe
						// ,H.no_medrec,H.namapasien,H.idpasien
						// ,CASE WHEN H.idtipe=1 THEN 'POLIKLINIK' ELSE 'IGD' END as tipe_nama
						// ,H.reservasi_cara,H.statuspasienbaru,H.noantrian
						// ,H.idpoliklinik,MP.nama as nama_poli,H.iddokter,MD.nama as nama_dokter
						// ,CASE WHEN H.reservasi_tipe_id=1 
							// THEN CONCAT(DATE_FORMAT(MJ.jam_dari,'%H:%i') ,' - ',DATE_FORMAT(MJ.jam_sampai,'%H:%i')) 
							// ELSE  CONCAT(MJ2.jam ,'.00 - ',MJ2.jam_akhir,'.00') END as jadwal 
						// ,H.jadwal_id,CASE WHEN H.idkelompokpasien=1 THEN MR.nama ELSE MK.nama END as nama_kelompok
						// ,(H.st_general+H.st_skrining+H.st_covid) as jml_skrining,H.status_reservasi
						// ,H.status,H.st_gc,H.st_general,H.st_sp,H.st_skrining,H.st_sc,H.st_covid,H.kode_antrian
						// ,H.statustindakan	
						// FROM tpoliklinik_pendaftaran H
						// INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
						// LEFT JOIN mdokter MD ON MD.id=H.iddokter
						// LEFT JOIN mjadwal_dokter MJ ON H.reservasi_tipe_id='1' AND MJ.id=H.jadwal_id
						// LEFT JOIN merm_jam MJ2 ON MJ2.jam_id=H.jadwal_id
						// LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						// LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						// WHERE H.id IS NOT  NULL ".$where."
						
						// ORDER BY H.tanggal DESC
					// ) as tbl WHERE id IS NOT NULL 
				// ";
			// // print_r($from);exit();
			// $this->from   = $from;
			// $this->join 	= array();
			
			
			// $this->order  = array();
			// $this->group  = array();
			// $this->column_search = array('nama_dokter');
			// $this->column_order  = array();

      // $list = $this->datatable->get_datatables(true);
      // $data = array();
      // $no = $_POST['start'];
      // foreach ($list as $r) {
          // $no++;
          // $result = array();
		  // $label_skrining='';
		  // $disabel_verif='';
		  // // if ($r->jml_skrining<3){
			  // // $label_skrining='<br><br>'.'<a href="'.base_url().'tpendaftaran_ranap/edit_pendaftaran/'.$r->id.'" type="button" data-toggle="tooltip" title="BELUM SKRINING" class="btn btn-danger btn-xs"><i class="fa fa-eye"></i> BELUM SKRINING</a>';;
			  // // // $disabel_verif='disabled';
		  // // }
		  // $label_skrining='<br><br>'.label_skrining($r->st_gc,$r->st_general,$r->st_sp,$r->st_skrining,$r->st_sc,$r->st_covid);
          // $result[] = $no;
          // $result[] = $r->nopendaftaran;
          // $result[] = HumanDateLong($r->tanggaldaftar);
          // $result[] = GetTujuanBerkas($r->idtipe);
          // $result[] = jenis_reservasi($r->reservasi_cara);
          // $result[] = ($r->statuspasienbaru=='0'?text_info('Pasien Lama'):text_danger('Pasien Baru'));
          // $result[] = $r->kode_antrian;
          // $result[] ='<strong>'. $r->no_medrec.'</strong><br>'.($r->namapasien);
          // $result[] ='<strong>'. $r->nama_poli.'</strong><br>'.($r->nama_dokter);
          // $result[] = HumanDateShort($r->tanggal).'<br>'.$r->jadwal;
          // $result[] = $r->nama_kelompok;
          // $result[] = ($r->status!='0'?status_reservasi($r->status_reservasi).$label_skrining:text_danger('Dibatalkan'));
          // $aksi = '<div class="btn-group">';
			// if ($r->status!='0'){
				// if ($r->status_reservasi=='3'){//Telah Datang
					// if (UserAccesForm($user_acces_form,array('1580'))){
					// $aksi .= '<a href="'.base_url().'tpendaftaran_ranap/edit_pendaftaran/'.$r->id.'" type="button" data-toggle="tooltip" title="Edit" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
					// }
					// if (UserAccesForm($user_acces_form,array('1581'))){
					// $aksi .= '<a href="'.base_url().'tpendaftaran_ranap/tindakan/'.$r->id.'" type="button" data-toggle="tooltip" title="Tindak" class="btn btn-warning btn-xs"><i class="fa fa-info"></i></a>';
					// }
					// if (UserAccesForm($user_acces_form,array('1582'))){
					// $aksi .= '<a href="'.base_url().'tpendaftaran_ranap/cetak" type="button" data-toggle="tooltip" title="Print Bukti" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
					// }
					// if (UserAccesForm($user_acces_form,array('1583'))){
					// $aksi .= '<button type="button" data-toggle="tooltip" title="Hapus" onclick="myDelete('.$r->id.')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>';
					// }
				// }else{
					// $aksi .=text_danger('BELUM HADIR');
				// }
			// }
			
			
		  // $aksi .= '</div>';
          // $result[] = $aksi;

          // $data[] = $result;
      // }
      // $output = array(
	      // "draw" => $_POST['draw'],
	      // "recordsTotal" => $this->datatable->count_all(true),
	      // "recordsFiltered" => $this->datatable->count_all(true),
	      // "data" => $data
      // );
      // echo json_encode($output);
  // }
	
	function load_index_gc($id){
	  $q_nilai="SELECT *FROM merm_referensi WHERE ref_head_id='16' ORDER BY merm_referensi.id ASC";
	  $list_nilai=$this->db->query($q_nilai)->result();
	  $q="SELECT  H.*
		,CASE 
			WHEN H.jenis_isi=1 THEN H.jawaban_id
			WHEN H.jenis_isi=2 THEN H.jawaban_freetext
			WHEN H.jenis_isi=3 THEN H.jawaban_ttd
		 END as jawaban
		
	  
	  FROM trawatinap_pendaftaran_gc H WHERE H.pendaftaran_id='$id'";
	  $list_data=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_data as $row){
		  
		  $tabel .='<tr>';
		  $tabel .='<td width="3%" ><input type="hidden" class="approval_id" value="'.$row->id.'">'.$row->no.'</td>';
		  $tabel .='<td width="60%" >'.($row->pertanyaan).'</td>';
	  
		  $tabel .='<td width="37%" class="text-center">'.$this->opsi_nilai($list_nilai,$row->group_jawaban_id,$row->jawaban,$row->jenis_isi,$row->id).'</td>';
			  
		  
		  $tabel .='</tr>';
	  }
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function load_index_lm($id){
	  $q_nilai="SELECT *FROM merm_referensi WHERE ref_head_id='16' ORDER BY merm_referensi.id ASC";
	  $list_nilai=$this->db->query($q_nilai)->result();
	  $q="SELECT  H.*
		,CASE 
			WHEN H.jenis_isi=1 THEN H.jawaban_id
			WHEN H.jenis_isi=2 THEN H.jawaban_freetext
			WHEN H.jenis_isi=3 THEN H.jawaban_ttd
		 END as jawaban
		
	  
	  FROM trawatinap_pendaftaran_lm H WHERE H.pendaftaran_id='$id'
	  ORDER BY H.nourut ASC
	  ";
	  $list_data=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_data as $row){
		  
		  $tabel .='<tr>';
		  if ($row->header_id=='0'){
			  $tabel .='<td colspan="3" class="text-primary h5">'.($row->pertanyaan).'</td>';
		  }else{
		  $tabel .='<td width="3%" class="text-right"><input type="hidden" class="approval_id" value="'.$row->id.'">'.$row->no.'</td>';
			 $tabel .='<td width="64%" >'.($row->pertanyaan).'</td>';
			$tabel .='<td width="33%" class="text-center">'.$this->opsi_nilai_lm($list_nilai,$row->group_jawaban_id,$row->jawaban,$row->jenis_isi,$row->id).'</td>'; 
		  }
		  
		  $tabel .='</tr>';
	  }
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function opsi_nilai_lm($list_nilai,$group_nilai,$jawaban_id,$jenis_isi,$id){
		if ($jenis_isi=='1'){
			  $arr_group_nilai=explode(",",$group_nilai);
			  $opsi='
											   
											  
			  <select class="js-select2 full-width nilai_lm has-error2" style="width:100%;" data-placeholder="Pilih Opsi">';
			  $opsi .='<option value="" '.($jawaban_id==0?'selected':'').'>Opsi Jawaban</option>'; 
			  foreach($list_nilai as $row){
				  if (in_array($row->id,$arr_group_nilai)){
					 $opsi .='<option value="'.$row->id.'" '.($jawaban_id==$row->id?'selected':'').'>'.$row->ref.'</option>'; 
				  }
			  }
			  $opsi .="</select>";
			  return $opsi;
		}
		if ($jenis_isi=='2'){
			  $jawaban_free_text='';
			  $jawaban_free_text='
				<textarea class="form-control nilai_lm_free" placeholder="Isi" required>'.$jawaban_id.'</textarea>
			  ';
			  return $jawaban_free_text;
		}
		if ($jenis_isi=='3'){
			$btn_paraf='';
			$nama_tabel="'trawatinap_pendaftaran_lm'";
			  if ($jawaban_id==''){
				  $btn_paraf= '<button type="button" onclick="modal_faraf('.$id.','.$nama_tabel.')"  data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i> TANDA TANGAN</button>';
			  }else{
				   $btn_paraf='<div class="img-container fx-img-rotate-r text-center">
						<img class="" style="width:100px;height:100px; text-align: center;" src="'.$jawaban_id.'" alt="" title="">
						<div class="img-options">
							<div class="img-options-content">
								<div class="btn-group btn-group-sm">
									<a class="btn btn-default" onclick="modal_faraf('.$id.','.$nama_tabel.')"  href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
									<a class="btn btn-default btn-danger" onclick="hapus_paraf('.$id.','.$nama_tabel.')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
								</div>
							</div>
						</div>
					</div>';
					
			  }
			  return $btn_paraf;
		}
	}
	function load_index_hk($id){
	  $q_nilai="SELECT *FROM merm_referensi WHERE ref_head_id='16' ORDER BY merm_referensi.id ASC";
	  $list_nilai=$this->db->query($q_nilai)->result();
	  $q="SELECT  H.*
		,CASE 
			WHEN H.jenis_isi=1 THEN H.jawaban_id
			WHEN H.jenis_isi=2 THEN H.jawaban_freetext
			WHEN H.jenis_isi=3 THEN H.jawaban_ttd
		 END as jawaban
		
	  
	  FROM trawatinap_pendaftaran_hk H WHERE H.pendaftaran_id='$id'
	  ORDER BY H.nourut ASC
	  ";
	  $list_data=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_data as $row){
		  
		  $tabel .='<tr>';
		  if ($row->header_id=='0'){
			  $tabel .='<td colspan="2"  width="85%" class="text-white bg-info h5"><input type="hidden" class="approval_id" value="'.$row->id.'">'.($row->no.'. '.$row->pertanyaan).'</td>';
			  $tabel .='<td width="15%" class="text-center text-white bg-info">'.$this->opsi_nilai_hk($list_nilai,$row->group_jawaban_id,$row->jawaban,$row->jenis_isi,$row->id).'</td>'; 
		  }else{
		  $tabel .='<td width="3%" class="text-right"><input type="hidden" class="approval_id" value="'.$row->id.'">'.$row->no.'</td>';
			 $tabel .='<td width="64%" >'.($row->pertanyaan).'</td>';
			$tabel .='<td width="33%" class="text-center">'.$this->opsi_nilai_hk($list_nilai,$row->group_jawaban_id,$row->jawaban,$row->jenis_isi,$row->id).'</td>'; 
		  }
		  
		  $tabel .='</tr>';
	  }
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function opsi_nilai_hk($list_nilai,$group_nilai,$jawaban_id,$jenis_isi,$id){
		if ($jenis_isi=='1'){
			  $arr_group_nilai=explode(",",$group_nilai);
			  $opsi='
											   
											  
			  <select class="js-select2 full-width nilai_hk has-error2" style="width:100%;" data-placeholder="Pilih Opsi">';
			  $opsi .='<option value="" '.($jawaban_id==0?'selected':'').'>Opsi Jawaban</option>'; 
			  foreach($list_nilai as $row){
				  if (in_array($row->id,$arr_group_nilai)){
					 $opsi .='<option value="'.$row->id.'" '.($jawaban_id==$row->id?'selected':'').'>'.$row->ref.'</option>'; 
				  }
			  }
			  $opsi .="</select>";
			  return $opsi;
		}
		if ($jenis_isi=='2'){
			  $jawaban_free_text='';
			  $jawaban_free_text='
				<textarea class="form-control nilai_hk_free" style="width:100%" placeholder="Isi" required>'.$jawaban_id.'</textarea>
			  ';
			  return $jawaban_free_text;
		}
		if ($jenis_isi=='3'){
			$btn_paraf='';
			$nama_tabel="'trawatinap_pendaftaran_hk'";
			  if ($jawaban_id==''){
				  $btn_paraf= '<button type="button" onclick="modal_faraf('.$id.','.$nama_tabel.')"  data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i> TANDA TANGAN</button>';
			  }else{
				   $btn_paraf='<div class="img-container fx-img-rotate-r text-center">
						<img class="" style="width:100px;height:100px; text-align: center;" src="'.$jawaban_id.'" alt="" title="">
						<div class="img-options">
							<div class="img-options-content">
								<div class="btn-group btn-group-sm">
									<a class="btn btn-default" onclick="modal_faraf('.$id.','.$nama_tabel.')"  href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
									<a class="btn btn-default btn-danger" onclick="hapus_paraf('.$id.','.$nama_tabel.')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
								</div>
							</div>
						</div>
					</div>';
					
			  }
			  return $btn_paraf;
		}
	}
	function opsi_nilai($list_nilai,$group_nilai,$jawaban_id,$jenis_isi,$id){
		if ($jenis_isi=='1'){
			  $arr_group_nilai=explode(",",$group_nilai);
			  $opsi='
											   
											  
			  <select class="js-select2 full-width nilai_gc has-error2" style="width:100%;" data-placeholder="Pilih Opsi">';
			  $opsi .='<option value="" '.($jawaban_id==0?'selected':'').'>Opsi Jawaban</option>'; 
			  foreach($list_nilai as $row){
				  if (in_array($row->id,$arr_group_nilai)){
					 $opsi .='<option value="'.$row->id.'" '.($jawaban_id==$row->id?'selected':'').'>'.$row->ref.'</option>'; 
				  }
			  }
			  $opsi .="</select>";
			  return $opsi;
		}
		if ($jenis_isi=='2'){
			  $jawaban_free_text='';
			  $jawaban_free_text='
				<textarea class="form-control nilai_gc_free" placeholder="Isi" required>'.$jawaban_id.'</textarea>
			  ';
			  return $jawaban_free_text;
		}
		if ($jenis_isi=='3'){
			$btn_paraf='';
			$nama_tabel="'trawatinap_pendaftaran_gc'";
			  if ($jawaban_id==''){
				  $btn_paraf= '<button type="button" onclick="modal_faraf('.$id.','.$nama_tabel.')"  data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i> TANDA TANGAN</button>';
			  }else{
				   $btn_paraf='<div class="img-container fx-img-rotate-r text-center">
						<img class="" style="width:100px;height:100px; text-align: center;" src="'.$jawaban_id.'" alt="" title="">
						<div class="img-options">
							<div class="img-options-content">
								<div class="btn-group btn-group-sm">
									<a class="btn btn-default" onclick="modal_faraf('.$id.','.$nama_tabel.')"  href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
									<a class="btn btn-default btn-danger" onclick="hapus_paraf('.$id.','.$nama_tabel.')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
								</div>
							</div>
						</div>
					</div>';
					
			  }
			  return $btn_paraf;
		}
	}
	
	function update_nilai_hk(){
	  $approval_id=$this->input->post('approval_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_id' =>$jawaban_id
	  );
	  $this->db->where('id',$approval_id);
	  $result=$this->db->update('trawatinap_pendaftaran_hk',$data);
	  $this->output->set_output(json_encode($result));
	}
	function update_nilai_hk_free(){
	  $approval_id=$this->input->post('approval_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_freetext' =>$jawaban_id
	  );
	  $this->db->where('id',$approval_id);
	  $result=$this->db->update('trawatinap_pendaftaran_hk',$data);
	  $this->output->set_output(json_encode($result));
	}
	function update_nilai_lm(){
	  $approval_id=$this->input->post('approval_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_id' =>$jawaban_id
	  );
	  $this->db->where('id',$approval_id);
	  $result=$this->db->update('trawatinap_pendaftaran_lm',$data);
	  $this->output->set_output(json_encode($result));
	}
	function update_nilai_lm_free(){
	  $approval_id=$this->input->post('approval_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_freetext' =>$jawaban_id
	  );
	  $this->db->where('id',$approval_id);
	  $result=$this->db->update('trawatinap_pendaftaran_lm',$data);
	  $this->output->set_output(json_encode($result));
	}
	function update_nilai_gc(){
	  $approval_id=$this->input->post('approval_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_id' =>$jawaban_id
	  );
	  $this->db->where('id',$approval_id);
	  $result=$this->db->update('trawatinap_pendaftaran_gc',$data);
	  $this->output->set_output(json_encode($result));
	}
	function update_nilai_gc_free(){
	  $approval_id=$this->input->post('approval_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_freetext' =>$jawaban_id
	  );
	  $this->db->where('id',$approval_id);
	  $result=$this->db->update('trawatinap_pendaftaran_gc',$data);
	  $this->output->set_output(json_encode($result));
	}
	function update_persetujuan_gc(){
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $jawaban_perssetujuan=$this->input->post('jawaban_perssetujuan');
	  $data=array(
		'jawaban_perssetujuan' =>$jawaban_perssetujuan
	  );
	  $this->db->where('pendaftaran_id',$pendaftaran_id);
	  $result=$this->db->update('trawatinap_pendaftaran_gc_head',$data);
	  $this->output->set_output(json_encode($result));
	}
	
	function save_ttd(){
		
		$id = $this->input->post('pendaftaran_id');
		$nama_tabel = $this->input->post('nama_tabel');
		$jawaban_ttd = $this->input->post('signature64');
		$data=array(
			'jawaban_ttd' =>$jawaban_ttd,
		);
		// print_r($id);
		$this->db->where('id',$id);
		$hasil=$this->db->update($nama_tabel,$data);
		$this->output->set_output(json_encode($hasil));
	}
	function batal(){
		
		$id = $this->input->post('id');
		$nama_tabel = $this->input->post('nama_tabel');
		$jawaban_ttd = $this->input->post('signature64');
		$data=array(
			'jawaban_ttd' =>$jawaban_ttd,
		);
		// print_r($id);
		$this->db->where('id',$id);
		$hasil=$this->db->update($nama_tabel,$data);
		$this->output->set_output(json_encode($hasil));
	}
	function save_ttd_2(){
		
		$nama_id_ttd = $this->input->post('nama_id_ttd');
		$id = $this->input->post('pendaftaran_id');
		$nama_tabel = $this->input->post('nama_tabel');
		$nama_field = $this->input->post('nama_field');
		$jawaban_ttd = $this->input->post('signature64');
		$data=array(
			"$nama_field" =>$jawaban_ttd,
		);
		// print_r($id);
		$this->db->where("$nama_id_ttd",$id);
		$hasil=$this->db->update($nama_tabel,$data);
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_paraf(){
		
		$id = $this->input->post('pendaftaran_id');
		$nama_tabel = $this->input->post('nama_tabel');
		
		$data=array(
			'jawaban_ttd' =>'',
		);
		// print_r($id);
		$this->db->where('id',$id);
		$hasil=$this->db->update($nama_tabel,$data);
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_ttd(){
		
		$id = $this->input->post('pendaftaran_id');
		// $nama_tabel = $this->input->post('nama_tabel');
		
		$data=array(
			'jawaban_ttd' =>'',
		);
		// print_r($id);
		$this->db->where('pendaftaran_id',$id);
		$hasil=$this->db->update('trawatinap_pendaftaran_gc_head',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_ttd_lm(){
		
		$id = $this->input->post('pendaftaran_id');
		// $nama_tabel = $this->input->post('nama_tabel');
		
		$data=array(
			'ttd_lm' =>'',
			'st_lembar' =>'0',
		);
		// print_r($id);
		$this->db->where('id',$id);
		$hasil=$this->db->update('trawatinap_pendaftaran',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_ttd_sp(){
		
		$id = $this->input->post('pendaftaran_id');
		// $nama_tabel = $this->input->post('nama_tabel');
		
		$data=array(
			'ttd_sp' =>'',
			'st_pernyataan' =>'0',
		);
		// print_r($id);
		$this->db->where('id',$id);
		$hasil=$this->db->update('trawatinap_pendaftaran',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_ttd_gc(){
		
		$id = $this->input->post('pendaftaran_id');
		// $nama_tabel = $this->input->post('nama_tabel');
		
		$data=array(
			'ttd_gc' =>'',
			'st_general' =>'0',
		);
		// print_r($id);
		$this->db->where('id',$id);
		$hasil=$this->db->update('trawatinap_pendaftaran',$data);
		$this->output->set_output(json_encode($hasil));
	}
	
	//Skrining pasien
	function load_index_sp($id){
	  $q_nilai="SELECT *FROM merm_referensi WHERE ref_head_id='17' ORDER BY merm_referensi.id ASC";
	  $list_nilai=$this->db->query($q_nilai)->result();
	  $q="SELECT  H.*
			,CASE 
						WHEN H.jenis_isi=1 THEN H.jawaban_id
						WHEN H.jenis_isi=2 THEN H.jawaban_freetext
						WHEN H.jenis_isi=3 THEN H.jawaban_ttd
					 END as jawaban
		
	  FROM tpoliklinik_pendaftaran_sp H
	  
	  WHERE H.pendaftaran_id='$id'";
	  $list_data=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_data as $row){
		  
		  $tabel .='<tr>';
		  $tabel .='<td width="50%" ><input type="hidden" class="sp_id" value="'.$row->id.'">'.($row->pertanyaan).'</td>';
	  
		// $tabel .='<td width="50%" >'.$this->opsi_nilai_sp($list_nilai,$row->group_jawaban_id,$row->jawaban_id).'</td>';
			  $tabel .='<td width="37%" >'.$this->opsi_nilai_sp($list_nilai,$row->group_jawaban_id,$row->jawaban,$row->jenis_isi,$row->id).'</td>';  
		  
		  $tabel .='</tr>';
	  }
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function opsi_nilai_sp($list_nilai,$group_nilai,$jawaban_id,$jenis_isi,$id){
		if ($jenis_isi=='1'){
			  $arr_group_nilai=explode(",",$group_nilai);
			  $opsi='
											   
											  
			  <select class="js-select2 full-width nilai_sp has-error2" style="width:100%;" data-placeholder="Pilih Opsi">';
			  $opsi .='<option value="" '.($jawaban_id==0?'selected':'').'>Opsi Jawaban</option>'; 
			  foreach($list_nilai as $row){
				  if (in_array($row->id,$arr_group_nilai)){
					 $opsi .='<option value="'.$row->id.'" '.($jawaban_id==$row->id?'selected':'').'>'.$row->ref.'</option>'; 
				  }
			  }
			  $opsi .="</select>";
			  return $opsi;
		}
		if ($jenis_isi=='2'){
			  $jawaban_free_text='';
			  $jawaban_free_text='
				<textarea class="form-control nilai_sp_free" placeholder="Isi" required>'.$jawaban_id.'</textarea>
			  ';
			  return $jawaban_free_text;
		}
		if ($jenis_isi=='3'){
			$btn_paraf='';
			$nama_tabel="'tpoliklinik_pendaftaran_sp'";
			  if ($jawaban_id==''){
				  $btn_paraf= '<button onclick="modal_faraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd_sp"><i class="fa fa-paint-brush"></i></button>';
			  }else{
				  $btn_paraf="<img style='display:block; width:100px;height:100px;' id='base64image' src='".$jawaban_id."' />";
				  $btn_paraf .= '<br><div class="btn-group btn-group-sm" role="group"><button onclick="hapus_paraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Hapus Tanda tangan" class="btn btn-danger btn-xs btn_ttd_sp"><i class="fa fa-trash"></i></button>';
				  $btn_paraf .= '<button onclick="modal_faraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd_sp"><i class="fa fa-paint-brush"></i></button>';
				  $btn_paraf .= '</div>';
			  }
			  return $btn_paraf;
		}
	}
	function update_nilai_sp_free(){
	  $approval_id=$this->input->post('approval_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_freetext' =>$jawaban_id
	  );
	  // print_r($approval_id);exit;
	  $this->db->where('id',$approval_id);
	  $result=$this->db->update('tpoliklinik_pendaftaran_sp',$data);
	  $this->output->set_output(json_encode($result));
	}
	function update_nilai_sp(){
	  $sp_id=$this->input->post('sp_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_id' =>$jawaban_id
	  );
	  $this->db->where('id',$sp_id);
	  $result=$this->db->update('tpoliklinik_pendaftaran_sp',$data);
	  $this->output->set_output(json_encode($result));
	}
	function update_nilai_sc_free(){
	  $approval_id=$this->input->post('approval_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_freetext' =>$jawaban_id
	  );
	  // print_r($approval_id);exit;
	  $this->db->where('id',$approval_id);
	  $result=$this->db->update('tpoliklinik_pendaftaran_sc',$data);
	  $this->output->set_output(json_encode($result));
	}
	function hapus_kontak(){
	  $id=$this->input->post('id');
	  $data=array(
		'staktif' =>0,
		'deleted_by'=>$this->session->userdata('user_id'),
		'deleted_date'=>date('Y-m-d H:i:s'),
	  );
	  // print_r($approval_id);exit;
	  $this->db->where('id',$id);
	  $result=$this->db->update('mfpasien_kontak',$data);
	  $this->output->set_output(json_encode($result));
	}
	
	function load_index_sc($id){
	  $q_nilai="SELECT *FROM merm_referensi WHERE ref_head_id='17' ORDER BY merm_referensi.id ASC";
	  $list_nilai=$this->db->query($q_nilai)->result();
	  $q="SELECT  H.*			,CASE 
						WHEN H.jenis_isi=1 THEN H.jawaban_id
						WHEN H.jenis_isi=2 THEN H.jawaban_freetext
						WHEN H.jenis_isi=3 THEN H.jawaban_ttd
					 END as jawaban
 FROM tpoliklinik_pendaftaran_sc H WHERE H.pendaftaran_id='$id'";
	  $list_data=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_data as $row){
		  
		  $tabel .='<tr>';
		  $tabel .='<td width="50%" ><input type="hidden" class="sc_id" value="'.$row->id.'">'.($row->pertanyaan).'</td>';
	  
		$tabel .='<td width="50%" >'.$this->opsi_nilai_sc($list_nilai,$row->group_jawaban_id,$row->jawaban,$row->jenis_isi,$row->id).'</td>';
			  
		  
		  $tabel .='</tr>';
	  }
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	// function opsi_nilai_sc($list_nilai,$group_nilai,$jawaban_id){
	  // $arr_group_nilai=explode(",",$group_nilai);
	  // $opsi='<select class="js-select2 full-width nilai_sc" style="width:100%" data-placeholder="Pilih Opsi">';
	  // $opsi .='<option value="" '.($jawaban_id==0?'selected':'').' class="has-danger">Opsi Jawaban</option>'; 
	  // foreach($list_nilai as $row){
		  // if (in_array($row->id,$arr_group_nilai)){
			 // $opsi .='<option value="'.$row->id.'" '.($jawaban_id==$row->id?'selected':'').'>'.$row->ref.'</option>'; 
		  // }
	  // }
	  // $opsi .="</select>";
	  // return $opsi;
	// }
	function opsi_nilai_sc($list_nilai,$group_nilai,$jawaban_id,$jenis_isi,$id){
		if ($jenis_isi=='1'){
			  $arr_group_nilai=explode(",",$group_nilai);
			  $opsi='
											   
											  
			  <select class="js-select2 full-width nilai_sc has-error2" style="width:100%;" data-placeholder="Pilih Opsi">';
			  $opsi .='<option value="" '.($jawaban_id==0?'selected':'').'>Opsi Jawaban</option>'; 
			  foreach($list_nilai as $row){
				  if (in_array($row->id,$arr_group_nilai)){
					 $opsi .='<option value="'.$row->id.'" '.($jawaban_id==$row->id?'selected':'').'>'.$row->ref.'</option>'; 
				  }
			  }
			  $opsi .="</select>";
			  return $opsi;
		}
		if ($jenis_isi=='2'){
			  $jawaban_free_text='';
			  $jawaban_free_text='
				<textarea class="form-control nilai_sc_free" placeholder="Isi" required>'.$jawaban_id.'</textarea>
			  ';
			  return $jawaban_free_text;
		}
		if ($jenis_isi=='3'){
			$btn_paraf='';
			$nama_tabel="'tpoliklinik_pendaftaran_sc'";
			  if ($jawaban_id==''){
				  $btn_paraf= '<button onclick="modal_faraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd_sc"><i class="fa fa-paint-brush"></i></button>';
			  }else{
				  $btn_paraf="<img style='display:block; width:100px;height:100px;' id='base64image' src='".$jawaban_id."' />";
				  $btn_paraf .= '<br><div class="btn-group btn-group-sm" role="group"><button onclick="hapus_paraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Hapus Tanda tangan" class="btn btn-danger btn-xs btn_ttd_sc"><i class="fa fa-trash"></i></button>';
				  $btn_paraf .= '<button onclick="modal_faraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd_sc"><i class="fa fa-paint-brush"></i></button>';
				  $btn_paraf .= '</div>';
			  }
			  return $btn_paraf;
		}
	}
	function update_nilai_sc(){
	  $sc_id=$this->input->post('sc_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_id' =>$jawaban_id
	  );
	  $this->db->where('id',$sc_id);
	  $result=$this->db->update('tpoliklinik_pendaftaran_sc',$data);
	  $this->output->set_output(json_encode($result));
	}
	public function delete()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		$idalasan = $this->input->post('idalasan');
		$data = [];
		$data['idalasan'] = $idalasan;
		$data['status'] = 0;
		$data['deleted_by'] = $this->session->userdata('user_id');
		$data['deleted_date'] = date('Y-m-d H:i:s');
		$this->db->where('id', $idpendaftaran);
		if ($this->db->update('tpoliklinik_pendaftaran', $data)) {
			$this->output->set_output(json_encode(true));
		} else {
			$this->output->set_output(json_encode(false));
			$this->error_message = 'Penyimpanan Gagal';
			// return false;
		}
	}
	
}
