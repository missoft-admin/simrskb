<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thistory_tindakan extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->helper('path');
		
  }

	
	function list_index_template()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tpoliklinik_assesmen_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tpoliklinik_assesmen` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_assesmen_rj/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
          $result[] = $aksi;
          $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
          $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
          $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
          $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
         

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_alergi()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.*,R.ref  as jenis_alergi, MP.nama as user_created
						FROM tpoliklinik_assesmen_alergi H
						INNER JOIN merm_referensi R ON R.ref_head_id='24' AND R.nilai=H.input_jenis_alergi
						LEFT JOIN mppa MP ON MP.id=H.created_by
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('jenis_alergi','input_detail_alergi','input_reaksi_alergi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->jenis_alergi);
          $result[] = $r->input_detail_alergi;
          $result[] = $r->input_reaksi_alergi;
          $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  $aksi .= '<button onclick="edit_alergi('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  $aksi .= '<button onclick="hapus_alergi('.$r->id.')" type="button" title="Hapus Alergi" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_alergi_his()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$idpasien=$this->input->post('idpasien');
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.*,R.ref  as jenis_alergi, MP.nama as user_created
						FROM tpoliklinik_assesmen_alergi H
						INNER JOIN merm_referensi R ON R.ref_head_id='24' AND R.nilai=H.input_jenis_alergi
						LEFT JOIN mppa MP ON MP.id=H.created_by
						WHERE H.idpasien='$idpasien' AND H.staktif='1' AND H.assesmen_id!='$assesmen_id' AND H.status_assemen='2'
						ORDER BY H.id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('jenis_alergi','input_detail_alergi','input_reaksi_alergi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->jenis_alergi);
          $result[] = $r->input_detail_alergi;
          $result[] = $r->input_reaksi_alergi;
          $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
          // $aksi = '<div class="btn-group">';
		  // // if (UserAccesForm($user_acces_form,array('1607'))){
		  // $aksi .= '<button onclick="edit_alergi('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // // }
		   // // if (UserAccesForm($user_acces_form,array('1608'))){
		  // $aksi .= '<button onclick="hapus_alergi('.$r->id.')" type="button" title="Hapus Suhu" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // // }
		  // $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_alergi_his_header()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$idpasien=$this->input->post('idpasien');
			$this->select = array();
			$from="
					(
						SELECT H.*,R.ref  as jenis_alergi, MP.nama as user_created
						FROM tpoliklinik_assesmen_alergi H
						INNER JOIN merm_referensi R ON R.ref_head_id='24' AND R.nilai=H.input_jenis_alergi
						LEFT JOIN mppa MP ON MP.id=H.created_by
						WHERE H.idpasien='$idpasien' AND H.staktif='1' AND H.status_assemen='2'
						ORDER BY H.id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('jenis_alergi','input_detail_alergi','input_reaksi_alergi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->jenis_alergi);
          $result[] = $r->input_detail_alergi;
          $result[] = $r->input_reaksi_alergi;
          $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
         
          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function load_skrining_risiko_jatuh(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $versi_edit=$this->input->post('versi_edit');
	  $versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
			SELECT *FROM (
				SELECT D.id,D.versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai  
				FROM `tpoliklinik_assesmen_his_risiko_jatuh` D
				WHERE D.assesmen_id='$assesmen_id'
				UNION ALL
				SELECT D.id,H.jml_edit as versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai  
				FROM tpoliklinik_assesmen H
				LEFT JOIN `tpoliklinik_assesmen_risiko_jatuh` D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id'
			) H 
			WHERE H.versi_edit='$versi_edit_last'
			ORDER BY H.id
		";
		$data_last=$this->db->query($q)->result_array();
	  // $q="SELECT * FROM `tpoliklinik_assesmen_risiko_jatuh` H WHERE H.assesmen_id='$assesmen_id'";
	  $q="SELECT *FROM (
			SELECT D.id,D.versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai  
			FROM `tpoliklinik_assesmen_his_risiko_jatuh` D
			WHERE D.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT D.id,H.jml_edit as versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai  
			FROM tpoliklinik_assesmen H
			LEFT JOIN `tpoliklinik_assesmen_risiko_jatuh` D ON D.assesmen_id=H.assesmen_id
			WHERE H.assesmen_id='$assesmen_id'
			) H 
			WHERE H.versi_edit='$versi_edit'
			ORDER BY H.id";
	  $hasil=$this->db->query($q)->result();
	  $opsi='';
	  $no=0;
	  foreach($hasil as $r){
		  $no=$no+1;
		  $parameter_nama=$r->parameter_nama;
			$data_pilih = array_filter($data_last, function($var) use ($parameter_nama) { 
				return ($var['parameter_nama'] == $parameter_nama);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['param_nilai_id'];
			}else{
				$last_pilih='#';
			}
			if ($r->param_nilai_id!=$last_pilih){
				$nama_class="edited";
			}else{
				$nama_class="";
				
			}
			
		 $opsi.='<tr>">
					<td>'.$r->parameter_nama.'<input class="risiko_nilai_id" type="hidden"  value="'.$r->id.'" ></td>
					<td><select tabindex="8" class="form-control nilai '.$nama_class.'" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						'.$this->opsi_nilai($r->group_nilai,$r->param_nilai_id).'
					</select>
					</td>
					
				</tr>';
	  }
	  $data['opsi']=$opsi;
	  echo json_encode($data);
  }
  function opsi_nilai($param,$param_nilai_id){
	  $q="SELECT * FROM `mrisiko_jatuh_param_skor` H WHERE H.id IN ($param)";
	  $hasil='';
	  $row=$this->db->query($q)->result();
		  $hasil .='<option value="" '.($param_nilai_id==''?'selected':'').'></option>';
	  foreach($row as $r){
		  $hasil .='<option value="'.$r->id.'" '.($param_nilai_id==$r->id?'selected':'').'>'.$r->deskripsi_nama.'</option>';
	  }
	  return $hasil;
  }
 
  function get_skor_pengkajian(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $template_id=$this->input->post('template_id');
	 
	  
	  $skor=$this->db->query("SELECT SUM(H.nilai) as skor FROM `tpoliklinik_assesmen_risiko_jatuh` H WHERE H.assesmen_id='$assesmen_id'")->row('skor');
	  $q="SELECT H.ref_nilai,H.st_tindakan,H.nama_tindakan FROM mrisiko_jatuh_setting_nilai H
			WHERE H.mrisiko_id='$template_id' AND ('$skor' BETWEEN H.skor_1 AND H.skor_2)";
	  $hasil=$this->db->query($q)->row();
	  
	  $data['skor']=$skor;
	  $data['ref_nilai']=$hasil->ref_nilai;
	  $data['st_tindakan']=$hasil->st_tindakan;
	  $data['nama_tindakan']=$hasil->nama_tindakan;
	  $this->output->set_output(json_encode($data));
  }
  
	function load_diagnosa()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$versi_edit_last=$this->input->post('versi_edit');
		
			if ($versi_edit!='0'){
				$versi_edit_last=$versi_edit-1;
			}
			$q="
				SELECT  MP.nama as nama_user,CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa, H.*  
				FROM (
					SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mdiagnosa_id,D.created_by,D.created_date,D.prioritas FROM tpoliklinik_assesmen H
					LEFT JOIN tpoliklinik_assesmen_diagnosa D ON D.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

					UNION ALL

					SELECT H.assesmen_id,H.versi_edit,H.id,H.mdiagnosa_id,H.created_by,H.created_date,H.prioritas FROM tpoliklinik_assesmen_his_diagnosa H
					WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
					) H
				LEFT JOIN mppa MP ON MP.id=H.created_by
				LEFT JOIN mdiagnosa MD ON MD.id=H.mdiagnosa_id 
				WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit_last'
				ORDER BY H.prioritas ASC
			";
			$data_last=$this->db->query($q)->result_array();
			$this->select = array();
			$from="
					(
						SELECT  MP.nama as nama_user,CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa, H.*  
						FROM (
							SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mdiagnosa_id,D.created_by,D.created_date,D.prioritas FROM tpoliklinik_assesmen H
							LEFT JOIN tpoliklinik_assesmen_diagnosa D ON D.assesmen_id=H.assesmen_id
							WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

							UNION ALL

							SELECT H.assesmen_id,H.versi_edit,H.id,H.mdiagnosa_id,H.created_by,H.created_date,H.prioritas FROM tpoliklinik_assesmen_his_diagnosa H
							WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
							) H
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN mdiagnosa MD ON MD.id=H.mdiagnosa_id 
						WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit'
						ORDER BY H.prioritas ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		  $mdiagnosa_id=$r->mdiagnosa_id;
			$data_pilih = array_filter($data_last, function($var) use ($mdiagnosa_id) { 
				return ($var['mdiagnosa_id'] == $mdiagnosa_id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['prioritas'];
			}else{
				$last_pilih='#';
			}
			if ($r->prioritas!=$last_pilih){
				$nama_class=' &nbsp;&nbsp;'.text_danger('Changed');
			}else{
				$nama_class="";
				
			}
			
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->diagnosa).$nama_class;
          $result[] = $r->prioritas;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  // $aksi .= '<button onclick="edit_diagnosa('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  // $aksi .= '<button onclick="hapus_diagnosa('.$r->id.')" type="button" title="Hapus Diagnosa" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '<button onclick="set_rencana_asuhan('.$r->id.')" type="button" title="Rencana Asuhan" class="btn btn-success btn-xs btn_rencana"><i class="si si-book-open"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  
  function refresh_diagnosa(){
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $versi_edit=$this->input->post('versi_edit');
	  $hasil='';
	  $q="SELECT H.id, CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa  
			FROM (
				SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mdiagnosa_id,D.created_by,D.created_date,D.prioritas FROM tpoliklinik_assesmen H
				LEFT JOIN tpoliklinik_assesmen_diagnosa D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

				UNION ALL

				SELECT H.assesmen_id,H.versi_edit,H.id,H.mdiagnosa_id,H.created_by,H.created_date,H.prioritas FROM tpoliklinik_assesmen_his_diagnosa H
				WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
			) H
			
			LEFT JOIN mdiagnosa MD ON MD.id=H.mdiagnosa_id 
			WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit'
			ORDER BY H.prioritas ASC";
	  $row=$this->db->query($q)->result();
		  $hasil .='<option value="" selected>Pilih Opsi</option>';
	  foreach($row as $r){
		  $hasil .='<option value="'.$r->id.'" selected>'.$r->diagnosa.'</option>';
	  }
	  $data['list']=$hasil;
	  echo json_encode($hasil);
  }
  function load_data_rencana_asuhan(){
		$assesmen_id=$this->input->post('assesmen_id');
		$diagnosa_id=$this->input->post('diagnosa_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
			SELECT *FROM (
				SELECT H.assesmen_id,H.jml_edit as versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM `tpoliklinik_assesmen` H
				LEFT JOIN tpoliklinik_assesmen_diagnosa_data D ON D.assesmen_id=H.assesmen_id 
				WHERE H.assesmen_id='$assesmen_id'
				UNION ALL

				SELECT D.assesmen_id,D.versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM tpoliklinik_assesmen_his_diagnosa_data D
				WHERE D.assesmen_id='$assesmen_id' AND D.versi_edit='$versi_edit_last'
				) H 
				WHERE H.versi_edit='$versi_edit_last' AND H.diagnosa_id='$diagnosa_id'

				ORDER BY H.nourut ASC
		";
		$data_last=$this->db->query($q)->result_array();
		// print_r($data_last);exit;
		$q="
			SELECT *FROM (
				SELECT H.assesmen_id,H.jml_edit as versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM `tpoliklinik_assesmen` H
				LEFT JOIN tpoliklinik_assesmen_diagnosa_data D ON D.assesmen_id=H.assesmen_id 
				WHERE H.assesmen_id='$assesmen_id'
				UNION ALL

				SELECT D.assesmen_id,D.versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM tpoliklinik_assesmen_his_diagnosa_data D
				WHERE D.assesmen_id='$assesmen_id' AND D.versi_edit='$versi_edit'
				) H 
				WHERE H.versi_edit='$versi_edit' AND H.diagnosa_id='$diagnosa_id'

				ORDER BY H.nourut ASC
		";
		$row=$this->db->query($q)->result();
		$tabel='';
		$last_lev='';
		$jml_col='0';
		foreach($row as $r){
			if ($r->lev=='0'){//Section
				if ($last_lev =='2'){//Jika Record Baru
					if ($jml_col==1){
					$tabel .='<td></td>';
						
					}
					$tabel .='</tr>';
				}
				$last_lev=$r->lev;
				$tabel .='
					<tr>
						<td colspan="2" style="background-color:#d9efff; font-weight: bold;width:100%">'.$r->nama.'</td>
					</tr>				
				';
				$jml_col=0;
			}
			if ($r->lev=='1'){//Kategori
				if ($last_lev =='2'){//Jika Record Baru
					if ($jml_col==1){
					$tabel .='<td></td>';
						
					}
					$tabel .='</tr>';
				}
				$last_lev=$r->lev;
				$tabel .='
					<tr>
						<td colspan="2" style="background-color:#f7f7f7; font-weight: bold;width:100%" class="text-primary">'.$r->nama.'</td>
					</tr>				
				';
				$jml_col=0;
			}
			if ($r->lev=='2'){//Data
				if ($last_lev != $r->lev){//Jika Record Baru
					$tabel .='<tr>';
					$jml_col=$jml_col+1;
				}else{
					$jml_col=$jml_col+1;
				}
				if ($jml_col=='3'){
					$tabel .='</tr>';
					$tabel .='<tr>';
					$jml_col=1;
				}
				$data_id=$r->data_id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['data_id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['pilih'];
				}else{
					$last_pilih='0';
				}
				if ($r->pilih!=$last_pilih){
					$nama_class="edited2";
				}else{
					$nama_class="";
					
				}
				// print_r($data_pilih['pilih']);exit;
				$tabel .='
					<td style="width:50%">
						<input type="hidden" class="data_id" value="'.$r->id.'">
						<div class="checkbox">
							<label for="example-'.$r->id.'">
								<input id="example-'.$r->id.'" name="chk[]" class="data_asuhan "  type="checkbox" '.($r->pilih?'checked':'').'><span class="'.$nama_class.'"> '.$r->nama.'</span>
							</label>
						</div>
					</td>			
				';
				$last_lev=$r->lev;
			}
		}
		if ($jml_col==1){
		$tabel .='<td></td>';
			
		}
		$tabel .='</tr>';
		// $tabel .='<tr><td colspan="2">&nbsp;&nbsp;</td></tr>';
		// print_r($tabel);exit;
		echo json_encode($tabel);
  }
	function list_index_his_nyeri()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tpoliklinik_nyeri_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tpoliklinik_nyeri` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_nyeri_rj/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	
	function load_nrs(){
		$assesmen_id=$this->input->post('assesmen_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		
		  $q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tpoliklinik_nyeri_scale_number H 
				LEFT JOIN tpoliklinik_nyeri D ON D.assesmen_id=H.assesmen_id
				WHERE  H.assesmen_id='$assesmen_id' 
				
				UNION ALL
				
				SELECT H.* FROM tpoliklinik_nyeri_his_scale_number H WHERE assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit_last'
				ORDER BY H.versi_edit,H.nourut
		  ";
		  
		$data_last=$this->db->query($q)->result_array();
		$q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tpoliklinik_nyeri_scale_number H 
				LEFT JOIN tpoliklinik_nyeri D ON D.assesmen_id=H.assesmen_id
				WHERE  H.assesmen_id='$assesmen_id' 
				
				UNION ALL
				
				SELECT H.* FROM tpoliklinik_nyeri_his_scale_number H WHERE assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit'
				ORDER BY H.versi_edit,H.nourut
		  ";
		  
		  $row=$this->db->query($q)->result();
		  $detail='';
		  $detail .='<tr>';
		  $ket_awal='';
		  $warna_awal='';
		  $nama_tabel="'tpoliklinik_nyeri_scale_number'";
		  $index_arr=0;
		  $jml_kol=1;
		  $array_ket=array();;
		  $array_kol=array();;
		  $array_warna=array();;
		  foreach($row as $r){
			  $data_id=$r->id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['pilih'];
				}else{
					$last_pilih='0';
				}
				if ($r->pilih!=$last_pilih){
					$nama_class="edited2";
				}else{
					$nama_class="";
					
				}
			  $detail .='<td class="border-full '.$nama_class.'" style="background-color:bg-gray-lighter">
						<label class="css-input css-radio css-radio-primary push-10-r">
						<input type="radio" name="nrs_opsi" '.($r->pilih?'checked':'').' onclick="set_skala_nyeri('.$r->id.','.$r->nilai.','.$nama_tabel.')"><span></span> <strong>'.$r->nilai.'</strong>
						</label>';
			  $detail .='</td>';
			  if ($ket_awal!=$r->keterangan){
				  if ($ket_awal != ''){
						$array_ket[$index_arr]=$ket_awal;
						$array_kol[$index_arr]=$jml_kol;
						$array_warna[$index_arr]=$warna_awal;
						$ket_awal=$r->keterangan;
						$warna_awal=$r->warna;
						$index_arr=$index_arr+1;;
				  }else{
					  $ket_awal=$r->keterangan;
					  $warna_awal=$r->warna;
				  }
				  $jml_kol=1;
			  }else{
				  $jml_kol=$jml_kol+1;
			  }
		  }
		  $array_ket[$index_arr]=$ket_awal;
		  $array_kol[$index_arr]=$jml_kol;
		  $array_warna[$index_arr]=$warna_awal;
		  $detail .='</tr>';
		  $detail .='<tr>';
		  foreach($row as $r){
			  $detail .='<td class="border-full"  style="background-color:'.$r->warna.'">';
			  $detail .='</td>';
		  }
		  $detail .='</tr>';
		  $detail .='<tr>';
		  foreach($array_ket as $index=>$val){
			  $detail .='<td class="border-full" colspan="'.$array_kol[$index].'" style="background-color:'.$array_warna[$index].'"><strong>'.$val.'</strong></td>';
		  }
		  $detail .='</tr>';
		  
		   $tabel='
			<table class="block-table text-center " style="width:100%">
				<tbody>
					'.$detail.'
				</tbody>
			</table>
		  ';
		  //FACE
		  $q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tpoliklinik_nyeri_scale_face H 
				INNER JOIN tpoliklinik_nyeri D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id' 
				UNION ALL
				SELECT H.* 
				FROM tpoliklinik_nyeri_his_scale_face H 
				WHERE H.assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit_last'
				ORDER BY H.nourut ASC
		  ";
		  
		$data_last=$this->db->query($q)->result_array();
		  
		  $nama_tabel="'tpoliklinik_nyeri_scale_face'";
		  $q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tpoliklinik_nyeri_scale_face H 
				INNER JOIN tpoliklinik_nyeri D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id' 
				UNION ALL
				SELECT H.* 
				FROM tpoliklinik_nyeri_his_scale_face H 
				WHERE H.assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit'
				ORDER BY H.nourut ASC
		  ";
		  $row=$this->db->query($q)->result();
		  $detail ='';
		  $detail .='<tr>';
		   foreach($row as $r){
			  $detail .='<td >';
			  $detail .='<img src="'.site_url().'assets/upload/wbf/'.$r->gambar.'" style="width:100px;height:100px" alt="Avatar">';
			  $detail .='</td>';
		  }
		  $detail .='</tr>';
		  $detail .='<tr>';
		   foreach($row as $r){
			   $data_id=$r->id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['pilih'];
				}else{
					$last_pilih='0';
				}
				if ($r->pilih!=$last_pilih){
					$nama_class="edited2";
				}else{
					$nama_class="";
					
				}
			  $detail .='<td class="border-full '.$nama_class.'" >';
			  $detail .='<label class="css-input css-radio css-radio-primary push-10-r">
						<input type="radio"  '.($r->pilih?'checked':'').' onclick="set_skala_nyeri('.$r->id.','.$r->nilai.','.$nama_tabel.')" name="nrs_opsi" value="'.$r->nilai.'"><span></span> <strong>'.$r->nilai.'</strong>
						</label>';
			  $detail .='</td>';
		  }
		  $detail .='</tr>';
		  $tabel .='
			<table class="block-table text-center " style="width:100%">
				<tbody>
					'.$detail.'
				</tbody>
			</table>
		  ';
		  // print_r($array_warna);exit;
		  $this->output->set_output(json_encode($tabel));
	  }
	  function load_nrs_fisio(){
		$assesmen_id=$this->input->post('assesmen_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		
		  $q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tpoliklinik_asmed_fisio_scale_number H 
				LEFT JOIN tpoliklinik_asmed_fisio D ON D.assesmen_id=H.assesmen_id
				WHERE  H.assesmen_id='$assesmen_id' 
				
				UNION ALL
				
				SELECT H.* FROM tpoliklinik_asmed_fisio_scale_number_his H WHERE assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit_last'
				ORDER BY H.versi_edit,H.nourut
		  ";
		  
		$data_last=$this->db->query($q)->result_array();
		$q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tpoliklinik_asmed_fisio_scale_number H 
				LEFT JOIN tpoliklinik_asmed_fisio D ON D.assesmen_id=H.assesmen_id
				WHERE  H.assesmen_id='$assesmen_id' 
				
				UNION ALL
				
				SELECT H.* FROM tpoliklinik_asmed_fisio_scale_number_his H WHERE assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit'
				ORDER BY H.versi_edit,H.nourut
		  ";
		  
		  $row=$this->db->query($q)->result();
		  $detail='';
		  $detail .='<tr>';
		  $ket_awal='';
		  $warna_awal='';
		  $nama_tabel="'tpoliklinik_asmed_fisio_scale_number'";
		  $index_arr=0;
		  $jml_kol=1;
		  $array_ket=array();;
		  $array_kol=array();;
		  $array_warna=array();;
		  foreach($row as $r){
			  $data_id=$r->id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['pilih'];
				}else{
					$last_pilih='0';
				}
				if ($r->pilih!=$last_pilih){
					$nama_class="edited2";
				}else{
					$nama_class="";
					
				}
			  $detail .='<td class="border-full '.$nama_class.'" style="background-color:bg-gray-lighter">
						<label class="css-input css-radio css-radio-primary push-10-r">
						<input type="radio" name="nrs_opsi" '.($r->pilih?'checked':'').' onclick="set_skala_nyeri('.$r->id.','.$r->nilai.','.$nama_tabel.')"><span></span> <strong>'.$r->nilai.'</strong>
						</label>';
			  $detail .='</td>';
			  if ($ket_awal!=$r->keterangan){
				  if ($ket_awal != ''){
						$array_ket[$index_arr]=$ket_awal;
						$array_kol[$index_arr]=$jml_kol;
						$array_warna[$index_arr]=$warna_awal;
						$ket_awal=$r->keterangan;
						$warna_awal=$r->warna;
						$index_arr=$index_arr+1;;
				  }else{
					  $ket_awal=$r->keterangan;
					  $warna_awal=$r->warna;
				  }
				  $jml_kol=1;
			  }else{
				  $jml_kol=$jml_kol+1;
			  }
		  }
		  $array_ket[$index_arr]=$ket_awal;
		  $array_kol[$index_arr]=$jml_kol;
		  $array_warna[$index_arr]=$warna_awal;
		  $detail .='</tr>';
		  $detail .='<tr>';
		  foreach($row as $r){
			  $detail .='<td class="border-full"  style="background-color:'.$r->warna.'">';
			  $detail .='</td>';
		  }
		  $detail .='</tr>';
		  $detail .='<tr>';
		  foreach($array_ket as $index=>$val){
			  $detail .='<td class="border-full" colspan="'.$array_kol[$index].'" style="background-color:'.$array_warna[$index].'"><strong>'.$val.'</strong></td>';
		  }
		  $detail .='</tr>';
		  
		   $tabel='
			<table class="block-table text-center " style="width:100%">
				<tbody>
					'.$detail.'
				</tbody>
			</table>
		  ';
		  //FACE
		  $q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tpoliklinik_asmed_fisio_scale_face H 
				INNER JOIN tpoliklinik_asmed_fisio D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id' 
				UNION ALL
				SELECT H.* 
				FROM tpoliklinik_asmed_fisio_scale_face_his H 
				WHERE H.assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit_last'
				ORDER BY H.nourut ASC
		  ";
		  
		$data_last=$this->db->query($q)->result_array();
		  
		  $nama_tabel="'tpoliklinik_asmed_fisio_scale_face'";
		  $q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tpoliklinik_asmed_fisio_scale_face H 
				INNER JOIN tpoliklinik_asmed_fisio D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id' 
				UNION ALL
				SELECT H.* 
				FROM tpoliklinik_asmed_fisio_scale_face_his H 
				WHERE H.assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit'
				ORDER BY H.nourut ASC
		  ";
		  $row=$this->db->query($q)->result();
		  $detail ='';
		  $detail .='<tr>';
		   foreach($row as $r){
			  $detail .='<td >';
			  $detail .='<img src="'.site_url().'assets/upload/wbf/'.$r->gambar.'" style="width:100px;height:100px" alt="Avatar">';
			  $detail .='</td>';
		  }
		  $detail .='</tr>';
		  $detail .='<tr>';
		   foreach($row as $r){
			   $data_id=$r->id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['pilih'];
				}else{
					$last_pilih='0';
				}
				if ($r->pilih!=$last_pilih){
					$nama_class="edited2";
				}else{
					$nama_class="";
					
				}
			  $detail .='<td class="border-full '.$nama_class.'" >';
			  $detail .='<label class="css-input css-radio css-radio-primary push-10-r">
						<input type="radio"  '.($r->pilih?'checked':'').' onclick="set_skala_nyeri('.$r->id.','.$r->nilai.','.$nama_tabel.')" name="nrs_opsi" value="'.$r->nilai.'"><span></span> <strong>'.$r->nilai.'</strong>
						</label>';
			  $detail .='</td>';
		  }
		  $detail .='</tr>';
		  $tabel .='
			<table class="block-table text-center " style="width:100%">
				<tbody>
					'.$detail.'
				</tbody>
			</table>
		  ';
		  // print_r($array_warna);exit;
		  $this->output->set_output(json_encode($tabel));
	  }
	  function load_nilai_param_nyeri(){
		 
		 $assesmen_id=$this->input->post('assesmen_id');
		 $versi_edit=$this->input->post('versi_edit');
		 $versi_edit_last=$this->input->post('versi_edit');
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		 $q="
			SELECT *FROM (
			SELECT D.jml_edit as versi_edit, H.* FROM tpoliklinik_nyeri_param H
			INNER JOIN tpoliklinik_nyeri D ON D.assesmen_id=H.assesmen_id
			WHERE H.assesmen_id='$assesmen_id'

			UNION ALL

			SELECT H.* FROM tpoliklinik_nyeri_his_param H
			WHERE H.assesmen_id='$assesmen_id'

			) H 
			WHERE H.versi_edit='$versi_edit_last'
			ORDER BY H.id ASC
		  ";
		  
		$data_last=$this->db->query($q)->result_array();
		  $q="
			SELECT *FROM (
			SELECT D.jml_edit as versi_edit, H.* FROM tpoliklinik_nyeri_param H
			INNER JOIN tpoliklinik_nyeri D ON D.assesmen_id=H.assesmen_id
			WHERE H.assesmen_id='$assesmen_id'

			UNION ALL

			SELECT H.* FROM tpoliklinik_nyeri_his_param H
			WHERE H.assesmen_id='$assesmen_id'

			) H 
			WHERE H.versi_edit='$versi_edit'
			ORDER BY H.id ASC";
		  $list_data=$this->db->query($q)->result();
		  $tabel='';
		  $nourut=1;
		  $total_skor=0;
		  foreach ($list_data as $row){
			  $warna='#fff';
			   $data_id=$row->id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['jawaban_id'];
				}else{
					$last_pilih='0';
				}
				if ($row->jawaban_id!=$last_pilih){
					$nama_class="edited";
				}else{
					$nama_class="";
					
				}
				$tabel .='<tr style="background-color:'.$warna.'">';
				$tabel .='<td><input type="hidden" class="parameter_id" value="'.$row->id.'">'.$nourut.'</td>';
				$tabel .='<td class="text-bold"><strong>'.$row->parameter_nama.'</strong></td>';
				$tabel .='<td>'.$this->opsi_nilai_nyeri($row->id,$row->jawaban_id,$nama_class).'</td>';
				$tabel .='<td class="text-bold"><input readonly type="text" class="form-control parameter_skor '.$nama_class.'" value="'.$row->jawaban_skor.'"></td>';
				$nourut=$nourut+1;
				$total_skor=$total_skor+$row->jawaban_skor;
			  $tabel .='</tr>';
		  }
		  $tabel.='<tr><td colspan="3" class="text-primary text-right"><strong>TOTAL SKOR SKALA NYERI</strong></td><td><input readonly type="text" class="form-control total_skor" value="'.$total_skor.'"></td></tr>';
		  
		  $arr['tabel']=$tabel;
		  $arr['total_skor']=$total_skor;
		  $this->output->set_output(json_encode($arr));
	  }
	  function opsi_nilai_nyeri($parameter_id,$jawaban_id,$nama_class){
		  $q="SELECT *FROM tpoliklinik_nyeri_param_skor WHERE parameter_id='$parameter_id' ORDER BY id ASC";
		  $list_nilai=$this->db->query($q)->result();
		  $opsi='<select class="'.$nama_class.' form-control nilai_nyeri" disabled style="width: 100%;">';
		  $opsi .='<option value="0" '.($jawaban_id==''?'selected':'').'>Pilih Jawaban</option>'; 
		  foreach($list_nilai as $row){
				 $opsi .='<option  value="'.$row->id.'" '.($jawaban_id==$row->id?'selected':'').'>'.$row->deskripsi_nama.' ('.$row->skor.')</option>'; 
		  }
		  $opsi .="</select>";
		  return $opsi;
	  }
	  function list_index_his_asmed()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tpoliklinik_asmed_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tpoliklinik_asmed` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_asmed_rj/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	
	function load_anatomi_asmed()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$versi_edit_last=$this->input->post('versi_edit');
		
			if ($versi_edit!='0'){
				$versi_edit_last=$versi_edit-1;
			}
			$q="
				SELECT * FROM (
						SELECT D.jml_edit as versi_edit,MD.nourut,MD.id, MP.nama as nama_user,CONCAT(MD.nama,'<br>(',MD.nama_english,')') as anatomi, H.ket_anatomi,H.created_date
						FROM tpoliklinik_asmed_anatomi H
						LEFT JOIN tpoliklinik_asmed D ON H.assesmen_id=D.assesmen_id
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN manatomi MD ON MD.id=H.manatomi_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'


						UNION ALL

						SELECT H.versi_edit as versi_edit,MD.nourut,MD.id, MP.nama as nama_user,CONCAT(MD.nama,'<br>(',MD.nama_english,')') as anatomi,H.ket_anatomi,H.created_date
						FROM tpoliklinik_asmed_his_anatomi H

						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN manatomi MD ON MD.id=H.manatomi_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						) T WHERE T.versi_edit='$versi_edit_last'
						ORDER BY T.nourut ASC
			";
			$data_last=$this->db->query($q)->result_array();
			$this->select = array();
			$from="
					(
						SELECT * FROM (
						SELECT D.jml_edit as versi_edit,MD.nourut,MD.id, MP.nama as nama_user,CONCAT(MD.nama,'<br>(',MD.nama_english,')') as anatomi, H.ket_anatomi,H.created_date
						FROM tpoliklinik_asmed_anatomi H
						LEFT JOIN tpoliklinik_asmed D ON H.assesmen_id=D.assesmen_id
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN manatomi MD ON MD.id=H.manatomi_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'


						UNION ALL

						SELECT H.versi_edit as versi_edit,MD.nourut,MD.id, MP.nama as nama_user,CONCAT(MD.nama,'<br>(',MD.nama_english,')') as anatomi,H.ket_anatomi,H.created_date
						FROM tpoliklinik_asmed_his_anatomi H

						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN manatomi MD ON MD.id=H.manatomi_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						) T WHERE T.versi_edit='$versi_edit'
						ORDER BY T.nourut ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		   $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['ket_anatomi'];
			}else{
				$last_pilih='#';
			}
			$lbl_perubahan='';
			if ($r->ket_anatomi!=$last_pilih){
				$st_perubahan=' '.text_danger('EDITED');
				if ($last_pilih!='#'){
					$lbl_perubahan='<br><br><i>Data sebelumnya <label class="text-danger">('.$last_pilih.')</label></i>';
				}
			}else{
				$st_perubahan='';
				
			}
		  
          $no++;
          $result = array();
          $result[] = $no;
			 $aksi='';
          $result[] = ($r->anatomi);
          $result[] = $r->ket_anatomi.$lbl_perubahan;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $result[] = $st_perubahan;
         
          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_diagnosa_asmed()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$versi_edit_last=$this->input->post('versi_edit');
		
			if ($versi_edit!='0'){
				$versi_edit_last=$versi_edit-1;
			}
			$q="
				SELECT *FROM(
						SELECT D.jml_edit as versi_edit,MP.nama as nama_user,MD.nama as diagnosa, H.mdiagnosa_id,H.prioritas,H.created_date 
							FROM tpoliklinik_asmed_diagnosa H
							LEFT JOIN tpoliklinik_asmed D ON D.assesmen_id=H.assesmen_id
							LEFT JOIN mppa MP ON MP.id=H.created_by
							LEFT JOIN mdiagnosa_medis MD ON MD.id=H.mdiagnosa_id 
							WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'


							UNION ALL

							SELECT H.versi_edit as versi_edit,MP.nama as nama_user,MD.nama as diagnosa, H.mdiagnosa_id ,H.prioritas,H.created_date
							FROM tpoliklinik_asmed_his_diagnosa H
							LEFT JOIN mppa MP ON MP.id=H.created_by
							LEFT JOIN mdiagnosa_medis MD ON MD.id=H.mdiagnosa_id 
							WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						) T
						WHERE T.versi_edit='$versi_edit_last'
						ORDER BY T.prioritas ASC
			";
			$data_last=$this->db->query($q)->result_array();
			$this->select = array();
			$from="
					(
						SELECT *FROM(
						SELECT D.jml_edit as versi_edit,MP.nama as nama_user,MD.nama as diagnosa, H.mdiagnosa_id,H.prioritas,H.created_date 
							FROM tpoliklinik_asmed_diagnosa H
							LEFT JOIN tpoliklinik_asmed D ON D.assesmen_id=H.assesmen_id
							LEFT JOIN mppa MP ON MP.id=H.created_by
							LEFT JOIN mdiagnosa_medis MD ON MD.id=H.mdiagnosa_id 
							WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'


							UNION ALL

							SELECT H.versi_edit as versi_edit,MP.nama as nama_user,MD.nama as diagnosa, H.mdiagnosa_id ,H.prioritas,H.created_date
							FROM tpoliklinik_asmed_his_diagnosa H
							LEFT JOIN mppa MP ON MP.id=H.created_by
							LEFT JOIN mdiagnosa_medis MD ON MD.id=H.mdiagnosa_id 
							WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						) T
						WHERE T.versi_edit='$versi_edit'
						ORDER BY T.prioritas ASC

					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
		   $mdiagnosa_id=$r->mdiagnosa_id;
			$data_pilih = array_filter($data_last, function($var) use ($mdiagnosa_id) { 
				return ($var['mdiagnosa_id'] == $mdiagnosa_id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['mdiagnosa_id'];
			}else{
				$last_pilih='#';
			}
			if ($r->mdiagnosa_id!=$last_pilih){
				$st_perubahan=' '.text_danger('EDITED');
				
			}else{
				$st_perubahan='';
				
			}
		  
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->diagnosa);
          $result[] = $r->prioritas;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
         
          $result[] = $st_perubahan;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_gambar_asmed(){
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $gambar_id=$this->input->post('gambar_id');
	  $status_assemen=$this->input->post('status_assemen');
	  $versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
	
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
	 
	  $q="SELECT gambar_tubuh FROM `tpoliklinik_asmed_lokalis_gambar` WHERE id='$gambar_id' AND assesmen_id='$assesmen_id'";
	  $gambar_tubuh=$this->db->query($q)->row('gambar_tubuh');
	  $data['gambar_tubuh']=$gambar_tubuh;
	  
	  $q="
	  SELECT * FROM (
		SELECT D.jml_edit as versi_edit, H.* 
		FROM tpoliklinik_asmed_lokalis H 
		LEFT JOIN tpoliklinik_asmed D ON D.assesmen_id=H.assesmen_id
		WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'


		UNION ALL

		SELECT H.* 
		FROM tpoliklinik_asmed_his_lokalis H 
		WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'
		) H 
		WHERE H.versi_edit='$versi_edit'
		ORDER BY H.`index` ASC
	  ";
	  $data['list_lokalis']=$this->db->query($q)->result();
	  
	  echo json_encode($data);
	}
	//TRIAGE
	function list_index_his_triage()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tpoliklinik_triage_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tpoliklinik_triage` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_triage/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.($r->user_edited?HumanDateLong($r->edited_date):'');
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function load_pemeriksaan_triage(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $status_assemen=$this->input->post('status_assemen');
	   $versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
	
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
	  $jml_kolom=0;
	  $tabel ='<thead><tr>';
	  $tabel .='<th class="text-center" style="width:15%"><strong>PEMERIKSAAN</strong></th>';
		$q="
			SELECT *FROM (
			SELECT D.jml_edit as versi_edit, H.* FROM tpoliklinik_triage_kat H 
			LEFT JOIN tpoliklinik_triage D ON D.assesmen_id=H.assesmen_id
			WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT *FROM tpoliklinik_triage_his_kat H WHERE H.assesmen_id='$assesmen_id'
			) T WHERE T.versi_edit='$versi_edit_last'";
			
		$header_last=$this->db->query($q)->result_array();
		$q="
			SELECT *FROM (
			SELECT D.jml_edit as versi_edit, H.* FROM tpoliklinik_triage_kat H 
			LEFT JOIN tpoliklinik_triage D ON D.assesmen_id=H.assesmen_id
			WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT *FROM tpoliklinik_triage_his_kat H WHERE H.assesmen_id='$assesmen_id'
			) T WHERE T.versi_edit='$versi_edit'";
			
		$header=$this->db->query($q)->result();
		$q="SELECT H.mtriage_pemeriksaan_id,H.pemeriksaan FROM tpoliklinik_triage_kat_detail H
			WHERE H.assesmen_id='$assesmen_id'
			GROUP BY H.mtriage_pemeriksaan_id
			ORDER BY H.mtriage_pemeriksaan_id ASC";
		$pemeriksaan_list=$this->db->query($q)->result();
		
		$q="SELECT *FROM (
			SELECT D.jml_edit as versi_edit, H.* FROM tpoliklinik_triage_kat_detail H 
			LEFT JOIN tpoliklinik_triage D ON D.assesmen_id=H.assesmen_id
			WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT * FROM tpoliklinik_triage_his_kat_detail H 
			WHERE H.assesmen_id='$assesmen_id'
			) T 
			WHERE T.versi_edit='$versi_edit'";
		$pemeriksaan_detail=$this->db->query($q)->result_array();
		
		$q="SELECT *FROM (
			SELECT D.jml_edit as versi_edit, H.* FROM tpoliklinik_triage_kat_detail H 
			LEFT JOIN tpoliklinik_triage D ON D.assesmen_id=H.assesmen_id
			WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT * FROM tpoliklinik_triage_his_kat_detail H 
			WHERE H.assesmen_id='$assesmen_id'
			) T 
			WHERE T.versi_edit='$versi_edit_last'";
		$pemeriksaan_detail_last=$this->db->query($q)->result_array();
		
		$jml_kolom=count($header);
		$lebar_kolom=85/$jml_kolom;
		foreach($header as $r){
			 $mtriage_id = $r->mtriage_id;
				 $data_pilih_lama = array_filter($header_last, function($var) use ($mtriage_id) { 
					return ($var['mtriage_id'] == $mtriage_id);
				  });
				  $data_pilih_lama=reset($data_pilih_lama);
				if ($data_pilih_lama){
					$last_pilih=$data_pilih_lama['pemeriksaan'];
				}else{
					$last_pilih='0';
				}
				if ($r->pemeriksaan!=$last_pilih){
					$nama_class="edited";
				}else{
					$nama_class="";
					
				}
				
			$opsi='<label class="css-input css-radio css-radio-primary push-10-r">
					<input type="radio" class="input_kategori_'.$r->mtriage_id.' input_kategori_header item_all" value="1" name="kategori_name" onclick="set_all_opsi_kategori('.$r->mtriage_id.')" '.($r->st_pilih_all=='1'?"checked":"").'><span></span><strong> '.$r->nama.'</strong>
				</label>';
			$opsi.='<input onblur="ganti_blur('.$r->mtriage_id.')" '.($status_assemen=='2'?'readonly':'').' class="'.$nama_class.'  form-control  input_pemeriksaan_header_'.$r->mtriage_id.' input_kategori_'.$r->mtriage_id.' input_pemeriksaan_header_global item_all"  data-id="'.$r->mtriage_id.'"  type="text" value="'.$r->pemeriksaan.'" placeholder="Tanda / Gejala Spesifik" >';
			$tabel .='<th style="background-color:'.$r->warna.';width:'.$lebar_kolom.'% " class="text-left">'.$opsi.'</th>';
		}
	  $tabel .='</tr></thead>';
	  $tabel .='<tbody>';
			foreach($pemeriksaan_list as $row){
			$mtriage_pemeriksaan_id=$row->mtriage_pemeriksaan_id;
			$tabel .='<tr>';
				$tabel .='<td class="text-center "><strong>'.$row->pemeriksaan.'</strong></td>';
					foreach($header as $r){
						$kat_id=$r->mtriage_id;
						$hasil='';
						$data_pilih = array_filter($pemeriksaan_detail, function($var) use ($kat_id,$mtriage_pemeriksaan_id) { 
							return ($var['kat_id'] == $kat_id && $var['mtriage_pemeriksaan_id'] == $mtriage_pemeriksaan_id);
						  });
						if ($data_pilih){
							foreach($data_pilih as $baris){
								 $data_id = ' '.$baris['id'];
								 $data_pilih_lama = array_filter($pemeriksaan_detail_last, function($var) use ($data_id) { 
									return ($var['id'] == $data_id);
								  });
								  $data_pilih_lama=reset($data_pilih_lama);
								if ($data_pilih_lama){
									$last_pilih=$data_pilih_lama['pilih'];
								}else{
									$last_pilih='0';
								}
								if ($baris['pilih']!=$last_pilih){
									$nama_class="edited2";
								}else{
									$nama_class="";
									
								}
								$hasil .='
										<div class="checkbox">
											<label for="example-'.$baris['id'].'">
												<input id="example-'.$baris['id'].'" data-id="'.$baris['id'].'"  data-kat="'.$kat_id.'" name="chk[]" class="chk_kategori_'.$baris['kat_id'].' chck_all item_all"  type="checkbox" '.($baris['pilih']=='1'?'checked':'').'><span class="'.$nama_class.' "> '.$baris['penilaian'].'</span>
											</label>
										</div>';
							}
						}
						// $data_pilih=reset($data_pilih);
						// print_r($data_pilih);
						// exit;
						// foreach($data_pilih as $){
							
						// }
						$tabel .='<td class="text-left">'.$hasil.'</td>';
					}
			$tabel .='</tr>';
			}
			$tabel .='<tr>';
			$tabel .='<td class="text-center"><strong>RESPON TIME</strong></td>';
				foreach($header as $r){
					$tabel .='<td class="text-center"><strong>'.$r->respon_time.'</strong></td>';
				}
			$tabel .='</tr>';
	  $tabel .='</tbody>';
	  
	  $data['tabel']=$tabel;
	  $this->output->set_output(json_encode($data));
  }
  
  // ASMED IGD
  function list_index_his_asmed_igd()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tpoliklinik_asmed_igd_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tpoliklinik_asmed_igd` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_asmed_igd/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	
	function load_anatomi_asmed_igd()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$versi_edit_last=$this->input->post('versi_edit');
		
			if ($versi_edit!='0'){
				$versi_edit_last=$versi_edit-1;
			}
			$q="
				SELECT * FROM (
						SELECT D.jml_edit as versi_edit,MD.nourut,MD.id, MP.nama as nama_user,CONCAT(MD.nama,'<br>(',MD.nama_english,')') as anatomi, H.ket_anatomi,H.created_date
						FROM tpoliklinik_asmed_igd_anatomi H
						LEFT JOIN tpoliklinik_asmed_igd D ON H.assesmen_id=D.assesmen_id
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN manatomi MD ON MD.id=H.manatomi_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'


						UNION ALL

						SELECT H.versi_edit as versi_edit,MD.nourut,MD.id, MP.nama as nama_user,CONCAT(MD.nama,'<br>(',MD.nama_english,')') as anatomi,H.ket_anatomi,H.created_date
						FROM tpoliklinik_asmed_igd_his_anatomi H

						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN manatomi MD ON MD.id=H.manatomi_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						) T WHERE T.versi_edit='$versi_edit_last'
						ORDER BY T.nourut ASC
			";
			$data_last=$this->db->query($q)->result_array();
			$this->select = array();
			$from="
					(
						SELECT * FROM (
						SELECT D.jml_edit as versi_edit,MD.nourut,MD.id, MP.nama as nama_user,CONCAT(MD.nama,'<br>(',MD.nama_english,')') as anatomi, H.ket_anatomi,H.created_date
						FROM tpoliklinik_asmed_igd_anatomi H
						LEFT JOIN tpoliklinik_asmed_igd D ON H.assesmen_id=D.assesmen_id
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN manatomi MD ON MD.id=H.manatomi_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'


						UNION ALL

						SELECT H.versi_edit as versi_edit,MD.nourut,MD.id, MP.nama as nama_user,CONCAT(MD.nama,'<br>(',MD.nama_english,')') as anatomi,H.ket_anatomi,H.created_date
						FROM tpoliklinik_asmed_igd_his_anatomi H

						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN manatomi MD ON MD.id=H.manatomi_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						) T WHERE T.versi_edit='$versi_edit'
						ORDER BY T.nourut ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		   $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['ket_anatomi'];
			}else{
				$last_pilih='#';
			}
			$lbl_perubahan='';
			if ($r->ket_anatomi!=$last_pilih){
				$st_perubahan=' '.text_danger('EDITED');
				if ($last_pilih!='#'){
					$lbl_perubahan='<br><br><i>Data sebelumnya <label class="text-danger">('.$last_pilih.')</label></i>';
				}
			}else{
				$st_perubahan='';
				
			}
		  
          $no++;
          $result = array();
          $result[] = $no;
			 $aksi='';
          $result[] = ($r->anatomi);
          $result[] = $r->ket_anatomi.$lbl_perubahan;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $result[] = $st_perubahan;
         
          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function load_diagnosa_asmed_igd()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$versi_edit_last=$this->input->post('versi_edit');
		
			if ($versi_edit!='0'){
				$versi_edit_last=$versi_edit-1;
			}
			$q="
				SELECT *FROM(
						SELECT D.jml_edit as versi_edit,MP.nama as nama_user,MD.nama as diagnosa, H.mdiagnosa_id,H.prioritas,H.created_date 
							FROM tpoliklinik_asmed_igd_diagnosa H
							LEFT JOIN tpoliklinik_asmed_igd D ON D.assesmen_id=H.assesmen_id
							LEFT JOIN mppa MP ON MP.id=H.created_by
							LEFT JOIN mdiagnosa_medis MD ON MD.id=H.mdiagnosa_id 
							WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'


							UNION ALL

							SELECT H.versi_edit as versi_edit,MP.nama as nama_user,MD.nama as diagnosa, H.mdiagnosa_id ,H.prioritas,H.created_date
							FROM tpoliklinik_asmed_igd_his_diagnosa H
							LEFT JOIN mppa MP ON MP.id=H.created_by
							LEFT JOIN mdiagnosa_medis MD ON MD.id=H.mdiagnosa_id 
							WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						) T
						WHERE T.versi_edit='$versi_edit_last'
						ORDER BY T.prioritas ASC
			";
			$data_last=$this->db->query($q)->result_array();
			$this->select = array();
			$from="
					(
						SELECT *FROM(
						SELECT D.jml_edit as versi_edit,MP.nama as nama_user,MD.nama as diagnosa, H.mdiagnosa_id,H.prioritas,H.created_date 
							FROM tpoliklinik_asmed_igd_diagnosa H
							LEFT JOIN tpoliklinik_asmed_igd D ON D.assesmen_id=H.assesmen_id
							LEFT JOIN mppa MP ON MP.id=H.created_by
							LEFT JOIN mdiagnosa_medis MD ON MD.id=H.mdiagnosa_id 
							WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'


							UNION ALL

							SELECT H.versi_edit as versi_edit,MP.nama as nama_user,MD.nama as diagnosa, H.mdiagnosa_id ,H.prioritas,H.created_date
							FROM tpoliklinik_asmed_igd_his_diagnosa H
							LEFT JOIN mppa MP ON MP.id=H.created_by
							LEFT JOIN mdiagnosa_medis MD ON MD.id=H.mdiagnosa_id 
							WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						) T
						WHERE T.versi_edit='$versi_edit'
						ORDER BY T.prioritas ASC

					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
		   $mdiagnosa_id=$r->mdiagnosa_id;
			$data_pilih = array_filter($data_last, function($var) use ($mdiagnosa_id) { 
				return ($var['mdiagnosa_id'] == $mdiagnosa_id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['mdiagnosa_id'];
			}else{
				$last_pilih='#';
			}
			if ($r->mdiagnosa_id!=$last_pilih){
				$st_perubahan=' '.text_danger('EDITED');
				
			}else{
				$st_perubahan='';
				
			}
		  
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->diagnosa);
          $result[] = $r->prioritas;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
         
          $result[] = $st_perubahan;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_gambar_asmed_igd(){
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $gambar_id=$this->input->post('gambar_id');
	  $status_assemen=$this->input->post('status_assemen');
	  $versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
	
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
	 
	  $q="SELECT gambar_tubuh FROM `tpoliklinik_asmed_igd_lokalis_gambar` WHERE id='$gambar_id' AND assesmen_id='$assesmen_id'";
	  $gambar_tubuh=$this->db->query($q)->row('gambar_tubuh');
	  $data['gambar_tubuh']=$gambar_tubuh;
	  
	  $q="
	  SELECT * FROM (
		SELECT D.jml_edit as versi_edit, H.* 
		FROM tpoliklinik_asmed_igd_lokalis H 
		LEFT JOIN tpoliklinik_asmed_igd D ON D.assesmen_id=H.assesmen_id
		WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'


		UNION ALL

		SELECT H.* 
		FROM tpoliklinik_asmed_igd_his_lokalis H 
		WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'
		) H 
		WHERE H.versi_edit='$versi_edit'
		ORDER BY H.`index` ASC
	  ";
	  $data['list_lokalis']=$this->db->query($q)->result();
	  
	  echo json_encode($data);
	}
	function load_gambar_asmed_ri(){
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $gambar_id=$this->input->post('gambar_id');
	  $status_assemen=$this->input->post('status_assemen');
	  $versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
	
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
	 
	  $q="SELECT gambar_tubuh FROM `tranap_asmed_ri_lokalis_gambar` WHERE id='$gambar_id' AND assesmen_id='$assesmen_id'";
	  // print_r($q);exit;
	  $gambar_tubuh=$this->db->query($q)->row('gambar_tubuh');
	  $data['gambar_tubuh']=$gambar_tubuh;
	  
	  $q="
	  SELECT * FROM (
		SELECT D.jml_edit as versi_edit, H.* 
		FROM tranap_asmed_ri_lokalis H 
		LEFT JOIN tranap_asmed_ri D ON D.assesmen_id=H.assesmen_id
		WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'


		UNION ALL

		SELECT H.* 
		FROM tranap_asmed_ri_x_his_lokalis H 
		WHERE H.assesmen_id='$assesmen_id' AND H.gambar_id='$gambar_id'
		) H 
		WHERE H.versi_edit='$versi_edit'
		ORDER BY H.`index` ASC
	  ";
	  $data['list_lokalis']=$this->db->query($q)->result();
	  
	  echo json_encode($data);
	}
	function list_index_his_asmed_ri()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_asmed_ri_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_asmed_ri` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_asmed_ri/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function load_anatomi_asmed_ri()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$versi_edit_last=$this->input->post('versi_edit');
		
			if ($versi_edit!='0'){
				$versi_edit_last=$versi_edit-1;
			}
			$q="
				SELECT * FROM (
						SELECT D.jml_edit as versi_edit,MD.nourut,MD.id, MP.nama as nama_user,CONCAT(MD.nama,'<br>(',MD.nama_english,')') as anatomi, H.ket_anatomi,H.created_date
						FROM tranap_asmed_ri_anatomi H
						LEFT JOIN tranap_asmed_ri D ON H.assesmen_id=D.assesmen_id
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN manatomi MD ON MD.id=H.manatomi_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'


						UNION ALL

						SELECT H.versi_edit as versi_edit,MD.nourut,MD.id, MP.nama as nama_user,CONCAT(MD.nama,'<br>(',MD.nama_english,')') as anatomi,H.ket_anatomi,H.created_date
						FROM tranap_asmed_ri_x_his_anatomi H

						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN manatomi MD ON MD.id=H.manatomi_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						) T WHERE T.versi_edit='$versi_edit_last'
						ORDER BY T.nourut ASC
			";
			$data_last=$this->db->query($q)->result_array();
			$this->select = array();
			$from="
					(
						SELECT * FROM (
						SELECT D.jml_edit as versi_edit,MD.nourut,MD.id, MP.nama as nama_user,CONCAT(MD.nama,'<br>(',MD.nama_english,')') as anatomi, H.ket_anatomi,H.created_date
						FROM tranap_asmed_ri_anatomi H
						LEFT JOIN tranap_asmed_ri D ON H.assesmen_id=D.assesmen_id
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN manatomi MD ON MD.id=H.manatomi_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'


						UNION ALL

						SELECT H.versi_edit as versi_edit,MD.nourut,MD.id, MP.nama as nama_user,CONCAT(MD.nama,'<br>(',MD.nama_english,')') as anatomi,H.ket_anatomi,H.created_date
						FROM tranap_asmed_ri_x_his_anatomi H

						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN manatomi MD ON MD.id=H.manatomi_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						) T WHERE T.versi_edit='$versi_edit'
						ORDER BY T.nourut ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		   $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['ket_anatomi'];
			}else{
				$last_pilih='#';
			}
			$lbl_perubahan='';
			if ($r->ket_anatomi!=$last_pilih){
				$st_perubahan=' '.text_danger('EDITED');
				if ($last_pilih!='#'){
					$lbl_perubahan='<br><br><i>Data sebelumnya <label class="text-danger">('.$last_pilih.')</label></i>';
				}
			}else{
				$st_perubahan='';
				
			}
		  
          $no++;
          $result = array();
          $result[] = $no;
			 $aksi='';
          $result[] = ($r->anatomi);
          $result[] = $r->ket_anatomi.$lbl_perubahan;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $result[] = $st_perubahan;
         
          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	 function list_index_his_assesmen_igd()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tpoliklinik_assesmen_igd_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tpoliklinik_assesmen_igd` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_assesmen_igd/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = ($r->user_edited?$r->user_edited.'<br>'.HumanDateLong($r->edited_date):'');
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function load_data_rencana_asuhan_igd(){
		$assesmen_id=$this->input->post('assesmen_id');
		$diagnosa_id=$this->input->post('diagnosa_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
			SELECT *FROM (
				SELECT H.assesmen_id,H.jml_edit as versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM `tpoliklinik_assesmen_igd` H
				LEFT JOIN tpoliklinik_assesmen_igd_diagnosa_data D ON D.assesmen_id=H.assesmen_id 
				WHERE H.assesmen_id='$assesmen_id'
				UNION ALL

				SELECT D.assesmen_id,D.versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM tpoliklinik_assesmen_igd_his_diagnosa_data D
				WHERE D.assesmen_id='$assesmen_id' AND D.versi_edit='$versi_edit_last'
				) H 
				WHERE H.versi_edit='$versi_edit_last' AND H.diagnosa_id='$diagnosa_id'

				ORDER BY H.nourut ASC
		";
		$data_last=$this->db->query($q)->result_array();
		$q="
			SELECT *FROM (
				SELECT H.assesmen_id,H.jml_edit as versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM `tpoliklinik_assesmen_igd` H
				LEFT JOIN tpoliklinik_assesmen_igd_diagnosa_data D ON D.assesmen_id=H.assesmen_id 
				WHERE H.assesmen_id='$assesmen_id'
				UNION ALL

				SELECT D.assesmen_id,D.versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM tpoliklinik_assesmen_igd_his_diagnosa_data D
				WHERE D.assesmen_id='$assesmen_id' AND D.versi_edit='$versi_edit'
				) H 
				WHERE H.versi_edit='$versi_edit' AND H.diagnosa_id='$diagnosa_id'

				ORDER BY H.nourut ASC
		";
		// print_r($q);exit;
		$row=$this->db->query($q)->result();
		$tabel='';
		$last_lev='';
		$jml_col='0';
		foreach($row as $r){
			if ($r->lev=='0'){//Section
				if ($last_lev =='2'){//Jika Record Baru
					if ($jml_col==1){
					$tabel .='<td></td>';
						
					}
					$tabel .='</tr>';
				}
				$last_lev=$r->lev;
				$tabel .='
					<tr>
						<td colspan="2" style="background-color:#d9efff; font-weight: bold;width:100%">'.$r->nama.'</td>
					</tr>				
				';
				$jml_col=0;
			}
			if ($r->lev=='1'){//Kategori
				if ($last_lev =='2'){//Jika Record Baru
					if ($jml_col==1){
					$tabel .='<td></td>';
						
					}
					$tabel .='</tr>';
				}
				$last_lev=$r->lev;
				$tabel .='
					<tr>
						<td colspan="2" style="background-color:#f7f7f7; font-weight: bold;width:100%" class="text-primary">'.$r->nama.'</td>
					</tr>				
				';
				$jml_col=0;
			}
			if ($r->lev=='2'){//Data
				if ($last_lev != $r->lev){//Jika Record Baru
					$tabel .='<tr>';
					$jml_col=$jml_col+1;
				}else{
					$jml_col=$jml_col+1;
				}
				if ($jml_col=='3'){
					$tabel .='</tr>';
					$tabel .='<tr>';
					$jml_col=1;
				}
				$data_id=$r->data_id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['data_id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['pilih'];
				}else{
					$last_pilih='0';
				}
				if ($r->pilih!=$last_pilih){
					$nama_class="edited2";
				}else{
					$nama_class="";
					
				}
				// print_r($data_pilih['pilih']);exit;
				$tabel .='
					<td style="width:50%">
						<input type="hidden" class="data_id" value="'.$r->id.'">
						<div class="checkbox">
							<label for="example-'.$r->id.'">
								<input id="example-'.$r->id.'" name="chk[]" class="data_asuhan "  type="checkbox" '.($r->pilih?'checked':'').'><span class="'.$nama_class.'"> '.$r->nama.'</span>
							</label>
						</div>
					</td>			
				';
				$last_lev=$r->lev;
			}
		}
		if ($jml_col==1){
		$tabel .='<td></td>';
			
		}
		$tabel .='</tr>';
		// $tabel .='<tr><td colspan="2">&nbsp;&nbsp;</td></tr>';
		// print_r($tabel);exit;
		echo json_encode($tabel);
  }
 
   function refresh_diagnosa_igd(){
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $versi_edit=$this->input->post('versi_edit');
	  $hasil='';
	  $q="SELECT H.id, CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa  
			FROM (
				SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mdiagnosa_id,D.created_by,D.created_date,D.prioritas FROM tpoliklinik_assesmen_igd H
				LEFT JOIN tpoliklinik_assesmen_igd_diagnosa D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

				UNION ALL

				SELECT H.assesmen_id,H.versi_edit,H.id,H.mdiagnosa_id,H.created_by,H.created_date,H.prioritas FROM tpoliklinik_assesmen_igd_his_diagnosa H
				WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
			) H
			
			LEFT JOIN mdiagnosa MD ON MD.id=H.mdiagnosa_id 
			WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit'
			ORDER BY H.prioritas ASC";
	  $row=$this->db->query($q)->result();
		  $hasil .='<option value="" selected>Pilih Opsi</option>';
	  foreach($row as $r){
		  $hasil .='<option value="'.$r->id.'" selected>'.$r->diagnosa.'</option>';
	  }
	  $data['list']=$hasil;
	  echo json_encode($hasil);
  }
  function load_skrining_risiko_jatuh_igd(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $versi_edit=$this->input->post('versi_edit');
	  $versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
			SELECT *FROM (
				SELECT D.id,D.versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai  
				FROM `tpoliklinik_assesmen_igd_his_risiko_jatuh` D
				WHERE D.assesmen_id='$assesmen_id'
				UNION ALL
				SELECT D.id,H.jml_edit as versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai  
				FROM tpoliklinik_assesmen_igd H
				LEFT JOIN `tpoliklinik_assesmen_igd_risiko_jatuh` D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id'
			) H 
			WHERE H.versi_edit='$versi_edit_last'
			ORDER BY H.id
		";
		$data_last=$this->db->query($q)->result_array();
	  // $q="SELECT * FROM `tpoliklinik_assesmen_igd_risiko_jatuh` H WHERE H.assesmen_id='$assesmen_id'";
	  $q="SELECT *FROM (
			SELECT D.id,D.versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai  
			FROM `tpoliklinik_assesmen_igd_his_risiko_jatuh` D
			WHERE D.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT D.id,H.jml_edit as versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai  
			FROM tpoliklinik_assesmen_igd H
			LEFT JOIN `tpoliklinik_assesmen_igd_risiko_jatuh` D ON D.assesmen_id=H.assesmen_id
			WHERE H.assesmen_id='$assesmen_id'
			) H 
			WHERE H.versi_edit='$versi_edit'
			ORDER BY H.id";
	  $hasil=$this->db->query($q)->result();
	  $opsi='';
	  $no=0;
	  foreach($hasil as $r){
		  $no=$no+1;
		  $parameter_nama=$r->parameter_nama;
			$data_pilih = array_filter($data_last, function($var) use ($parameter_nama) { 
				return ($var['parameter_nama'] == $parameter_nama);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['param_nilai_id'];
			}else{
				$last_pilih='#';
			}
			if ($r->param_nilai_id!=$last_pilih){
				$nama_class="edited";
			}else{
				$nama_class="";
				
			}
			
		 $opsi.='<tr>">
					<td>'.$r->parameter_nama.'<input class="risiko_nilai_id" type="hidden"  value="'.$r->id.'" ></td>
					<td><select tabindex="8" class="form-control nilai '.$nama_class.'" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						'.$this->opsi_nilai_igd($r->group_nilai,$r->param_nilai_id).'
					</select>
					</td>
					
				</tr>';
	  }
	  $data['opsi']=$opsi;
	  echo json_encode($data);
  }
  function opsi_nilai_igd($param,$param_nilai_id){
	  $q="SELECT * FROM `mrisiko_jatuh_param_skor` H WHERE H.id IN ($param)";
	  $hasil='';
	  $row=$this->db->query($q)->result();
		  $hasil .='<option value="" '.($param_nilai_id==''?'selected':'').'></option>';
	  foreach($row as $r){
		  $hasil .='<option value="'.$r->id.'" '.($param_nilai_id==$r->id?'selected':'').'>'.$r->deskripsi_nama.'</option>';
	  }
	  return $hasil;
  }
  function load_obat_igd()
	{
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			  $versi_edit_last=$this->input->post('versi_edit');
				
				if ($versi_edit!='0'){
					$versi_edit_last=$versi_edit-1;
				}
				$q="
					SELECT *FROM (
						SELECT D.jml_edit as versi_edit,H.*
						FROM tpoliklinik_assesmen_igd_obat H
						LEFT JOIN tpoliklinik_assesmen_igd D ON D.assesmen_id=H.assesmen_id
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'

						UNION ALL

						SELECT H.*
						FROM tpoliklinik_assesmen_igd_his_obat H
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
						) T WHERE T.versi_edit='$versi_edit_last'
				";
				$data_last=$this->db->query($q)->result_array();
			$this->select = array();
			$from="
					(
						SELECT *FROM (
						SELECT D.jml_edit as versi_edit,H.*
						FROM tpoliklinik_assesmen_igd_obat H
						LEFT JOIN tpoliklinik_assesmen_igd D ON D.assesmen_id=H.assesmen_id
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'

						UNION ALL

						SELECT H.*
						FROM tpoliklinik_assesmen_igd_his_obat H
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
						) T WHERE T.versi_edit='$versi_edit'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_obat','dosis','waktu');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
			$id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['id'];
			}else{
				$last_pilih='#';
			}
			if ($r->id!=$last_pilih){
				$nama_class="edited";
				$aksi=' &nbsp;&nbsp;'.text_danger('New');
			}else{
				$nama_class="";
				$aksi="";
				
			}
			 // $aksi='';
          $result[] = ($r->nama_obat);
          $result[] = $r->dosis;
          $result[] = $r->waktu;
         
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_obat_infus_igd()
	{
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			  $versi_edit_last=$this->input->post('versi_edit');
				
				if ($versi_edit!='0'){
					$versi_edit_last=$versi_edit-1;
				}
				$q="
				SELECT *FROM(	
						SELECT D.jml_edit as versi_edit ,H.*,M.nama as nama_obat,MP.nama as pemberi,MPR.nama as pemeriksa,C.nama as pembuat,MR.ref as nama_rute
						FROM tpoliklinik_assesmen_igd_obat_infus H
						INNER JOIN tpoliklinik_assesmen_igd D ON D.assesmen_id=H.assesmen_id
						INNER JOIN view_barang_all M ON M.idtipe=H.idtipe AND M.id=H.obat_id
						INNER JOIN mppa MP ON MP.id=H.ppa_pemberi
						INNER JOIN mppa MPR ON MPR.id=H.ppa_periksa
						INNER JOIN mppa C ON C.id=H.ppa_created
						INNER JOIN merm_referensi MR ON MR.nilai=H.rute AND MR.ref_head_id='74'
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1' 
						GROUP BY H.id

						UNION ALL

						SELECT H.*,M.nama as nama_obat,MP.nama as pemberi,MPR.nama as pemeriksa,C.nama as pembuat,MR.ref as nama_rute
						FROM tpoliklinik_assesmen_igd_his_obat_infus H
						INNER JOIN view_barang_all M ON M.idtipe=H.idtipe AND M.id=H.obat_id
						INNER JOIN mppa MP ON MP.id=H.ppa_pemberi
						INNER JOIN mppa MPR ON MPR.id=H.ppa_periksa
						INNER JOIN mppa C ON C.id=H.ppa_created
						INNER JOIN merm_referensi MR ON MR.nilai=H.rute AND MR.ref_head_id='74'
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
						GROUP BY H.id
					) T WHERE versi_edit='$versi_edit_last'
				";
				$data_last=$this->db->query($q)->result_array();
			$q="
				SELECT *FROM(	
						SELECT D.jml_edit as versi_edit ,H.*,M.nama as nama_obat,MP.nama as pemberi,MPR.nama as pemeriksa,C.nama as pembuat,MR.ref as nama_rute
						FROM tpoliklinik_assesmen_igd_obat_infus H
						INNER JOIN tpoliklinik_assesmen_igd D ON D.assesmen_id=H.assesmen_id
						INNER JOIN view_barang_all M ON M.idtipe=H.idtipe AND M.id=H.obat_id
						INNER JOIN mppa MP ON MP.id=H.ppa_pemberi
						INNER JOIN mppa MPR ON MPR.id=H.ppa_periksa
						INNER JOIN mppa C ON C.id=H.ppa_created
						INNER JOIN merm_referensi MR ON MR.nilai=H.rute AND MR.ref_head_id='74'
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1' 
						GROUP BY H.id

						UNION ALL

						SELECT H.*,M.nama as nama_obat,MP.nama as pemberi,MPR.nama as pemeriksa,C.nama as pembuat,MR.ref as nama_rute
						FROM tpoliklinik_assesmen_igd_his_obat_infus H
						INNER JOIN view_barang_all M ON M.idtipe=H.idtipe AND M.id=H.obat_id
						INNER JOIN mppa MP ON MP.id=H.ppa_pemberi
						INNER JOIN mppa MPR ON MPR.id=H.ppa_periksa
						INNER JOIN mppa C ON C.id=H.ppa_created
						INNER JOIN merm_referensi MR ON MR.nilai=H.rute AND MR.ref_head_id='74'
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
						GROUP BY H.id
					) T WHERE versi_edit='$versi_edit'
					ORDER BY T.id ASC
				";
			$list=$this->db->query($q)->result();
			$tabel='';
      foreach ($list as $r) {
          $result = array();

			 $aksi='';
			 $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['id'];
			}else{
				$last_pilih='#';
			}
			if ($r->id!=$last_pilih){
				$nama_class=' text-danger';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
			 $tabel .='<tr>';
          $tabel .='<td class="text-center '.$nama_class.'"><i class="fa fa-calendar-plus-o"></i> '.HumanDateShort_exp($r->tanggal_infus).' </td>';
          $tabel .='<td class="text-center '.$nama_class.'"><i class="si si-clock"></i> '.HumanTime($r->tanggal_infus).' </td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.($r->nama_obat).'</td>';
          $tabel .='<td class="text-center '.$nama_class.'">'.$r->kuantitas_infus.'</td>';
          $tabel .='<td class="text-center '.$nama_class.'">'.$r->dosis_infus.'</td>';
          $tabel .='<td class="text-center '.$nama_class.'">'.$r->nama_rute.'</td>';
          $tabel .='<td class="text-center '.$nama_class.'">'.$r->pemberi.'</td>';
          $tabel .='<td class="text-center '.$nama_class.'">'. $r->pemeriksa.'</td>';
		  $aksi .= ($r->pembuat?'<br>'.$r->pembuat:'');
           $tabel .='<td class="text-center '.$nama_class.'">'.$aksi.'</td>';
		  $tabel .= '</tr>';

      }
	  $data['tabel']=$tabel;
      $this->output->set_output(json_encode($data));
  }
  function load_tindakan_assesmen_igd()
	{
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			  $versi_edit_last=$this->input->post('versi_edit');
				
				if ($versi_edit!='0'){
					$versi_edit_last=$versi_edit-1;
				}
				$q="
				SELECT *FROM (
					SELECT D.jml_edit as versi_edit,H.*,MP.nama as pelaksana,MC.nama as user_created 
									,MT.nama as nama_tindakan
									FROM tpoliklinik_assesmen_igd_tindakan H
									INNER JOIN tpoliklinik_assesmen_igd D ON D.assesmen_id=H.assesmen_id
									INNER JOIN mppa MP ON MP.id=H.ppa_pelaksana
									INNER JOIN mppa MC ON MC.id=H.created_ppa
									INNER JOIN mtindakan_igd MT ON MT.id=H.mtindakan_id
									WHERE H.assesmen_id='$assesmen_id'
									AND H.staktif='1'	
									
									
					UNION ALL

					SELECT H.*,MP.nama as pelaksana,MC.nama as user_created 
									,MT.nama as nama_tindakan
									FROM tpoliklinik_assesmen_igd_his_tindakan H
									INNER JOIN mppa MP ON MP.id=H.ppa_pelaksana
									INNER JOIN mppa MC ON MC.id=H.created_ppa
									INNER JOIN mtindakan_igd MT ON MT.id=H.mtindakan_id
									WHERE H.assesmen_id='$assesmen_id'
									AND H.staktif='1'	
				) T WHERE T.versi_edit='$versi_edit_last'
				";
				$data_last=$this->db->query($q)->result_array();
			$q="
					
				SELECT *FROM (
					SELECT D.jml_edit as versi_edit,H.*,MP.nama as pelaksana,MC.nama as user_created 
									,MT.nama as nama_tindakan
									FROM tpoliklinik_assesmen_igd_tindakan H
									INNER JOIN tpoliklinik_assesmen_igd D ON D.assesmen_id=H.assesmen_id
									INNER JOIN mppa MP ON MP.id=H.ppa_pelaksana
									INNER JOIN mppa MC ON MC.id=H.created_ppa
									INNER JOIN mtindakan_igd MT ON MT.id=H.mtindakan_id
									WHERE H.assesmen_id='$assesmen_id'
									AND H.staktif='1'	
									
									
					UNION ALL

					SELECT H.*,MP.nama as pelaksana,MC.nama as user_created 
									,MT.nama as nama_tindakan
									FROM tpoliklinik_assesmen_igd_his_tindakan H
									INNER JOIN mppa MP ON MP.id=H.ppa_pelaksana
									INNER JOIN mppa MC ON MC.id=H.created_ppa
									INNER JOIN mtindakan_igd MT ON MT.id=H.mtindakan_id
									WHERE H.assesmen_id='$assesmen_id'
									AND H.staktif='1'	
				) T WHERE T.versi_edit='$versi_edit'
				";
			$list=$this->db->query($q)->result();
			$tabel='';
      foreach ($list as $r) {
          $result = array();

			 $aksi='';
			  $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['id'];
			}else{
				$last_pilih='#';
			}
			if ($r->id!=$last_pilih){
				$nama_class=' text-danger';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
			 $tabel .='<tr>';
          $tabel .='<td class="text-center '.$nama_class.'"><i class="fa fa-calendar"></i> '.HumanDateShort_exp($r->tanggal_input).' </td>';
          $tabel .='<td class="text-center '.$nama_class.'"><i class="si si-clock"></i> '.HumanTime($r->tanggal_input).' </td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.($r->nama_tindakan).'</td>';
          $tabel .='<td class="text-center '.$nama_class.'">'.$r->pelaksana.'</td>';
          $tabel .='<td class="text-center '.$nama_class.'">'.HumanTime($r->waktu_mulai).'</td>';
          $tabel .='<td class="text-center '.$nama_class.'">'.HumanTime($r->waktu_selesai).'</td>';
          // $aksi = '<div class="btn-group">';
		  // $aksi .= '<button onclick="edit_tindakan_assesmen_igd('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // $aksi .= '<button onclick="hapus_tindakan_assesmen_igd('.$r->id.')" type="button" title="Hapus Obat" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // $aksi .= '</div>';
		  // $aksi .= ($r->user_created?'<br>'.$r->user_created.'<br>'.HumanDateLong($r->created_date):'');
           $tabel .='<td class="text-center '.$nama_class.'">'.$aksi.'</td>';
		  $tabel .= '</tr>';

      }
	  $data['tabel']=$tabel;
      $this->output->set_output(json_encode($data));
  }
  
  function load_alergi_igd()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			  $versi_edit_last=$this->input->post('versi_edit');
				
				if ($versi_edit!='0'){
					$versi_edit_last=$versi_edit-1;
				}
				$q="
				SELECT *FROM (
						SELECT D.jml_edit as versi_edit,H.*,R.ref  as jenis_alergi, MP.nama as user_created
						FROM tpoliklinik_assesmen_igd_alergi H
						INNER JOIN tpoliklinik_assesmen_igd D ON D.assesmen_id=H.assesmen_id
						INNER JOIN merm_referensi R ON R.ref_head_id='24' AND R.nilai=H.input_jenis_alergi
						LEFT JOIN mppa MP ON MP.id=H.created_by
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'

						UNION ALL

						SELECT H.*,R.ref  as jenis_alergi, MP.nama as user_created
						FROM tpoliklinik_assesmen_igd_his_alergi H

						INNER JOIN merm_referensi R ON R.ref_head_id='24' AND R.nilai=H.input_jenis_alergi
						LEFT JOIN mppa MP ON MP.id=H.created_by
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
						) T WHERE T.versi_edit='$versi_edit_last'
				";
				$data_last=$this->db->query($q)->result_array();
				
			$this->select = array();
			$from="
					(
						SELECT *FROM (
						SELECT D.jml_edit as versi_edit,H.*,R.ref  as jenis_alergi, MP.nama as user_created
						FROM tpoliklinik_assesmen_igd_alergi H
						INNER JOIN tpoliklinik_assesmen_igd D ON D.assesmen_id=H.assesmen_id
						INNER JOIN merm_referensi R ON R.ref_head_id='24' AND R.nilai=H.input_jenis_alergi
						LEFT JOIN mppa MP ON MP.id=H.created_by
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'

						UNION ALL

						SELECT H.*,R.ref  as jenis_alergi, MP.nama as user_created
						FROM tpoliklinik_assesmen_igd_his_alergi H

						INNER JOIN merm_referensi R ON R.ref_head_id='24' AND R.nilai=H.input_jenis_alergi
						LEFT JOIN mppa MP ON MP.id=H.created_by
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
						) T WHERE T.versi_edit='$versi_edit'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('jenis_alergi','input_detail_alergi','input_reaksi_alergi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
			  $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['id'];
			}else{
				$last_pilih='#';
			}
			if ($r->id!=$last_pilih){
				$nama_class=' text-danger';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
          $result[] = ($r->jenis_alergi);
          $result[] = $r->input_detail_alergi;
          $result[] = $r->input_reaksi_alergi;
          $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
        
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function list_index_his_cppt()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$login_profesi_id=$this->session->userdata('login_profesi_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (H.profesi_id) IN (".$filter_profesi_id.")";
			}
			if ($st_owned!='#'){
				if ($st_owned=='1'){
					$where .=" AND (H.created_ppa) = '$login_ppa_id'";
				}else{
					$where .=" AND (H.created_ppa) != '$login_ppa_id'";
					
			}			
			}
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT 		H.jml_edit as versi_edit,
							 H.created_ppa
							,H.assesmen_id,H.pendaftaran_id,MP.tanggaldaftar,MP.nopendaftaran,MPOL.nama as poli ,H.st_ranap,H.pendaftaran_id_ranap
							,MD.nama as dokter,H.tanggal_input as tanggal_pengkajian
							,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
							,MP.iddokter,MP.idpoliklinik,H.keterangan_edit,H.jml_edit,H.st_edited
							,H.ttd_nama,H.ttd,R.ref as soap_nama
							,H.subjectif,H.objectif,H.assemen,H.planing,H.intruksi,H.intervensi,H.evaluasi,H.reassesmen
							,H.st_verifikasi,H.dokter_pjb,H.profesi_id
							,ppa_verif.nama as nama_verif,H.verifikasi_date
							FROM `tpoliklinik_cppt` H
							INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
							INNER JOIN mpoliklinik MPOL ON MPOL.id=MP.idpoliklinik
							INNER JOIN mdokter MD ON MD.id=MP.iddokter
							INNER JOIN merm_referensi R ON R.nilai=H.profesi_id AND R.ref_head_id='21'
							LEFT JOIN mppa ON mppa.id=H.created_ppa
							LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
							LEFT JOIN mppa ppa_verif ON ppa_verif.id=H.verifikasi_ppa

							WHERE H.assesmen_id='$assesmen_id' ".$where."

							UNION ALL

							SELECT H.versi_edit,
							 H.created_ppa
							,H.assesmen_id,H.pendaftaran_id,MP.tanggaldaftar,MP.nopendaftaran,MPOL.nama as poli ,H.st_ranap,H.pendaftaran_id_ranap
							,MD.nama as dokter,H.tanggal_input as tanggal_pengkajian
							,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
							,MP.iddokter,MP.idpoliklinik,H.keterangan_edit,H.jml_edit,H.st_edited
							,H.ttd_nama,H.ttd,R.ref as soap_nama
							,H.subjectif,H.objectif,H.assemen,H.planing,H.intruksi,H.intervensi,H.evaluasi,H.reassesmen
							,H.st_verifikasi,H.dokter_pjb,H.profesi_id
							,ppa_verif.nama as nama_verif,H.verifikasi_date
							FROM `tpoliklinik_cppt_his` H
							INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
							INNER JOIN mpoliklinik MPOL ON MPOL.id=MP.idpoliklinik
							INNER JOIN mdokter MD ON MD.id=MP.iddokter
							INNER JOIN merm_referensi R ON R.nilai=H.profesi_id AND R.ref_head_id='21'
							LEFT JOIN mppa ON mppa.id=H.created_ppa
							LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
							LEFT JOIN mppa ppa_verif ON ppa_verif.id=H.verifikasi_ppa

							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						
						ORDER BY H.versi_edit ASC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  $subjectif_last='';$objectif_last='';$assemen_last='';$planing_last='';$intruksi_last='';$intervensi_last='';$evaluasi_last='';$reassesmen_last='';
	  $subjectif_class='';$objectif_class='';$assemen_class='';$planing_class='';$intruksi_class='';$intervensi_class='';$evaluasi_class='';$reassesmen_class='';
	  $setting_cppt_field=$this->Tpendaftaran_poli_ttv_model->setting_cppt_field($login_profesi_id);
	  foreach ($list as $r) {
		  $subjectif_class='';$objectif_class='';$assemen_class='';$planing_class='';$intruksi_class='';$intervensi_class='';$evaluasi_class='';$reassesmen_class='';
		  $no++;
		  $result = array();
			if ($versi_edit!=$r->versi_edit){
				if ($r->st_ranap=='1'){
			 $btn_last='&nbsp;&nbsp;<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_cppt_ri/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
					
				}else{
			 $btn_last='&nbsp;&nbsp;<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_cppt/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
					
				}
			  
		  }else{
			  $btn_last='&nbsp;&nbsp;&nbsp;'.text_default('SEDANG DILIHAT');
		  }
		
			$jml_kolom=0;
			$subjectif='';$objectif='';$assemen='';$planing='';$intruksi='';$intervensi='';$evaluasi='';$reassesmen='';
			if (trim($r->subjectif)!=trim($subjectif_last)){$subjectif_class='text-danger';}
			if (trim($r->objectif)!=trim($objectif_last)){$objectif_class='text-danger';}
			if (trim($r->assemen)!=trim($assemen_last)){$assemen_class='text-danger';}
			if (trim($r->planing)!=trim($planing_last)){$assemen_class='text-danger';}
			if (trim($r->intruksi)!=trim($intruksi_last)){$intruksi_class='text-danger';}
			if (trim($r->intervensi)!=trim($intervensi_last)){$intervensi_class='text-danger';}
			if (trim($r->evaluasi)!=trim($evaluasi_last)){$evaluasi_class='text-danger';}
			if (trim($r->reassesmen)!=trim($reassesmen_last)){$reassesmen_class='text-danger';}
			
			
			if ($setting_cppt_field['field_subjectif']=='1'){$jml_kolom=$jml_kolom+1;}
			if ($setting_cppt_field['field_objectif']=='1'){$jml_kolom=$jml_kolom+1;}
			if ($setting_cppt_field['field_assesmen']=='1'){$jml_kolom=$jml_kolom+1;}
			if ($setting_cppt_field['field_planing']=='1'){$jml_kolom=$jml_kolom+1;}
			if ($setting_cppt_field['field_intruction']=='1'){$jml_kolom=$jml_kolom+1;}
			if ($setting_cppt_field['field_intervensi']=='1'){$jml_kolom=$jml_kolom+1;}
			if ($setting_cppt_field['field_evaluasi']=='1'){$jml_kolom=$jml_kolom+1;}
			if ($setting_cppt_field['field_reasessmen']=='1'){$jml_kolom=$jml_kolom+1;}
			$lebar_kolom=100/$jml_kolom;
			if ($r->versi_edit=='0'){
				$subjectif_class='';$objectif_class='';$assemen_class='';$planing_class='';$intruksi_class='';$intervensi_class='';$evaluasi_class='';$reassesmen_class='';
			}
			if ($setting_cppt_field['field_subjectif']){
				$subjectif='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">SUBJECTIVE</div>
								<div class="push-5-t '.$subjectif_class.'"> '.$r->subjectif.'</div>
							</td>';
				
			}
			if ($setting_cppt_field['field_objectif']){
				$objectif='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">OBJECTIVE</div>
								<div class="push-5-t '.$objectif_class.'"> '.$r->objectif.'</div>
							</td>';
				
			}
			if ($setting_cppt_field['field_assesmen']){
				$assemen='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">ASSESMENT</div>
								<div class="push-5-t '.$assemen_class.'"> '.$r->assemen.'</div>
							</td>';
				
			}
			if ($setting_cppt_field['field_planing']){
				$planing='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">PLANING</div>
								<div class="push-5-t '.$planing_class.'"> '.$r->planing.'</div>
							</td>';
				
			}
			if ($setting_cppt_field['field_intruction']){
				$intruksi='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">INTRUCTIONS</div>
								<div class="push-5-t '.$intruksi_class.'"> '.$r->intruksi.'</div>
							</td>';
				
			}
			if ($setting_cppt_field['field_intervensi']){
				$intervensi='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">INTERVENTION</div>
								<div class="push-5-t '.$intervensi_class.'"> '.$r->intervensi.'</div>
							</td>';
				
			}
			if ($setting_cppt_field['field_evaluasi']){
				$evaluasi='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">EVALUATION</div>
								<div class="push-5-t '.$evaluasi_class.'"> '.$r->evaluasi.'</div>
							</td>';
				
			}
			if ($setting_cppt_field['field_reasessmen']){
				$reassesmen='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">RE-ASSESMENT</div>
								<div class="push-5-t '.$reassesmen_class.'"> '.$r->reassesmen.'</div>
							</td>';
				
			}
			$info_edit='';
			if ($r->nama_edit){
				$info_edit='<div class="push-5-t  text-success"> Edited On '.tanggal_indo_DMY($r->edited_date).' at '.HumanTimeShort($r->edited_date).' Oleh '.$r->nama_edit.'</div>';
			}
			$tabel='<table class="table table-condensed" style="border-spacing:0;border-collapse: collapse!important;">
						<tbody>
							<tr>
								<td colspan="'.$jml_kolom.'" class="bg-white" style="width: 100%;">
									<div class="h5"><strong>'.strtoupper($r->soap_nama).'&nbsp;&nbsp;&nbsp;'.($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last.'</strong></div>
									<div class="push-5-t">'.$r->nopendaftaran.' | Added On '.tanggal_indo_DMY($r->tanggal_pengkajian).' at '.HumanTimeShort($r->tanggal_pengkajian).' Oleh '.$r->nama_mppa.'</div>
									'.
									$info_edit
									.'
								</td>
							</tr>
							<tr>
								'.$subjectif.'								
								'.$objectif.'								
								'.$assemen.'								
								'.$planing.'								
								'.$intruksi.'								
								'.$intervensi.'								
								'.$evaluasi.'								
								'.$reassesmen.'								
							</tr>
						</tbody>
					</table>';
			$subjectif_last=$r->subjectif;$objectif_last=$r->objectif;;$assemen_last=$r->assemen;$planing_last=$r->planing;
			$intruksi_last=$r->intruksi;$intervensi_last=$r->intervensi;$evaluasi_last=$r->evaluasi;$reassesmen_last=$r->reassesmen;
			 $result[] =$tabel;
			$data[] = $result;
			
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	}
	function load_diagnosa_igd()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$versi_edit_last=$this->input->post('versi_edit');
		
			if ($versi_edit!='0'){
				$versi_edit_last=$versi_edit-1;
			}
			$q="
				SELECT  MP.nama as nama_user,CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa, H.*  
				FROM (
					SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mdiagnosa_id,D.created_by,D.created_date,D.prioritas FROM tpoliklinik_assesmen_igd H
					LEFT JOIN tpoliklinik_assesmen_igd_diagnosa D ON D.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

					UNION ALL

					SELECT H.assesmen_id,H.versi_edit,H.id,H.mdiagnosa_id,H.created_by,H.created_date,H.prioritas FROM tpoliklinik_assesmen_igd_his_diagnosa H
					WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
					) H
				LEFT JOIN mppa MP ON MP.id=H.created_by
				LEFT JOIN mdiagnosa MD ON MD.id=H.mdiagnosa_id 
				WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit_last'
				ORDER BY H.prioritas ASC
			";
			$data_last=$this->db->query($q)->result_array();
			$this->select = array();
			$from="
					(
						SELECT  MP.nama as nama_user,CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa, H.*  
						FROM (
							SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mdiagnosa_id,D.created_by,D.created_date,D.prioritas FROM tpoliklinik_assesmen_igd H
							LEFT JOIN tpoliklinik_assesmen_igd_diagnosa D ON D.assesmen_id=H.assesmen_id
							WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

							UNION ALL

							SELECT H.assesmen_id,H.versi_edit,H.id,H.mdiagnosa_id,H.created_by,H.created_date,H.prioritas FROM tpoliklinik_assesmen_igd_his_diagnosa H
							WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
							) H
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN mdiagnosa MD ON MD.id=H.mdiagnosa_id 
						WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit'
						ORDER BY H.prioritas ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		  $mdiagnosa_id=$r->mdiagnosa_id;
			$data_pilih = array_filter($data_last, function($var) use ($mdiagnosa_id) { 
				return ($var['mdiagnosa_id'] == $mdiagnosa_id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['prioritas'];
			}else{
				$last_pilih='#';
			}
			if ($r->prioritas!=$last_pilih){
				$nama_class=' &nbsp;&nbsp;'.text_danger('Changed');
			}else{
				$nama_class="";
				
			}
			
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->diagnosa).$nama_class;
          $result[] = $r->prioritas;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  // $aksi .= '<button onclick="edit_diagnosa('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  // $aksi .= '<button onclick="hapus_diagnosa('.$r->id.')" type="button" title="Hapus Diagnosa" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '<button onclick="set_rencana_asuhan('.$r->id.')" type="button" title="Rencana Asuhan" class="btn btn-success btn-xs btn_rencana"><i class="si si-book-open"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_index_informasi_ic(){
	  $status_assemen=$this->input->post('status_assemen');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $ttd_dokter_pelaksana=$this->input->post('ttd_dokter_pelaksana');
	  $ttd_penerima_info=$this->input->post('ttd_penerima_info');
	  $versi_edit=$this->input->post('versi_edit');
	  $versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
		SELECT *FROM (
				select H.jml_edit as versi_edit,D.*
				FROM tpoliklinik_ic_informasi D
				INNER JOIN tpoliklinik_ic H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'

				UNION ALL

				select D.*
				FROM tpoliklinik_ic_his_informasi D
				WHERE D.assesmen_id='$assesmen_id'

				) T WHERE T.versi_edit='$versi_edit_last'
		";
		$data_last=$this->db->query($q)->result_array();
		
	  $q=" SELECT 
		ttd_dokter_ina,ttd_dokter_eng,ket_ttd_dokter_ina,ket_ttd_dokter_eng,ttd_pasien_ina,ttd_pasien_eng,ket_ttd_pasien_ina,ket_ttd_pasien_eng FROM setting_ic_keterangan

	  ";
	  $row_ttd=$this->db->query($q)->row_array();
	   $q="
		SELECT *FROM (
				select H.jml_edit  as versi_edit,D.*
				FROM tpoliklinik_ic_informasi D
				INNER JOIN tpoliklinik_ic H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'

				UNION ALL

				select D.*
				FROM tpoliklinik_ic_his_informasi D
				WHERE D.assesmen_id='$assesmen_id'

				) T WHERE T.versi_edit='$versi_edit'
		";
	  $list_info=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_info as $r){
		   $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['isi_informasi'];
			}else{
				$last_pilih='#';
			}
			if ($r->isi_informasi!=$last_pilih){
				$nama_class=' edited';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
			
		  $btn_paraf='';
		  if ($r->paraf){
		  $btn_paraf='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$r->paraf.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_faraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_paraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
		  }else{
			  $btn_paraf .= '<button onclick="modal_faraf('.$r->id.')"  type="button" data-toggle="tooltip" title="Paraf" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i></button>';
		  }
		  
		  $tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="informasi_id" value="'.$r->id.'">'.$r->nourut.'</td>';
			$tabel .='<td class="text-center"><strong>'.$r->nama.'</strong><br><i>'.$r->nama_english.'</i></td>';
			$tabel .='<td>'.$r->isi_informasi.'</td>';
			$tabel .='<td class="text-center">'.$btn_paraf.'</td>';
		  $tabel .='</tr>';
	  } 
	  $btn_ttd_dokter='';
	  if ($ttd_dokter_pelaksana){
		  $btn_ttd_dokter='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_dokter_pelaksana.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
	  }else{
		  $btn_ttd_dokter='<button class="btn btn-sm btn-success" onclick="modal_ttd_dokter()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $btn_ttd_kel='';
	  if ($ttd_penerima_info){
		  $btn_ttd_kel='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_penerima_info.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_kel()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_kel()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
	  }else{
		  $btn_ttd_kel='<button class="btn btn-sm btn-success" onclick="modal_ttd_kel()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_dokter_ina'].'<br><i>'.$row_ttd['ket_ttd_dokter_eng'].'</i></td>';
	  $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_dokter_ina'].'/'.$row_ttd['ttd_dokter_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_dokter.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_pasien_ina'].'<br><i>'.$row_ttd['ket_ttd_pasien_eng'].'</i></td>';
	   $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_pasien_ina'].'/'.$row_ttd['ttd_pasien_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_kel.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function list_index_his_ic()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa,H.st_ranap
							FROM `tpoliklinik_ic_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa,H.st_ranap
							FROM `tpoliklinik_ic` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			  if ($r->st_ranap=='1'){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_ri/his_ic_ri/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }else{
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_ic/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = ($r->user_edited?$r->user_edited.'<br>'.HumanDateLong($r->edited_date):'');
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function list_index_his_edukasi()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			// $tipe_rj_ri=$this->input->post('tipe_rj_ri');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa,H.pendaftaran_id_ranap,H.tipe_rj_ri
							FROM `tpoliklinik_edukasi_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa,H.pendaftaran_id_ranap,H.tipe_rj_ri
							FROM `tpoliklinik_edukasi` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
		  if ($r->tipe_rj_ri==1){

			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_edukasi/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			}else{
				$aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_edukasi_ri/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				
		  }
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = ($r->user_edited?$r->user_edited.'<br>'.HumanDateLong($r->edited_date):'');
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function load_index_materi_edukasi(){
		$status_assemen=$this->input->post('status_assemen');
		$assesmen_id=$this->input->post('assesmen_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');

		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
			SELECT *FROM (
				select D.jml_edit as versi_edit,H.*,M.ref as nama_evaluasi 
				FROM tpoliklinik_edukasi_materi H
				LEFT JOIN tpoliklinik_edukasi D ON D.assesmen_id=H.assesmen_id
				LEFT JOIN merm_referensi M ON M.nilai=H.evaluasi AND M.ref_head_id='84'
				WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
				
				UNION ALL
				
				select H.*,M.ref as nama_evaluasi 
				FROM tpoliklinik_edukasi_his_materi H

				LEFT JOIN merm_referensi M ON M.nilai=H.evaluasi AND M.ref_head_id='84'
				WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
			) H WHERE H.versi_edit='$versi_edit_last'
		";
		$data_last=$this->db->query($q)->result_array();
		
	  $q="
			SELECT *FROM (
				select D.jml_edit as versi_edit,H.*,M.ref as nama_evaluasi 
				FROM tpoliklinik_edukasi_materi H
				LEFT JOIN tpoliklinik_edukasi D ON D.assesmen_id=H.assesmen_id
				LEFT JOIN merm_referensi M ON M.nilai=H.evaluasi AND M.ref_head_id='84'
				WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
				
				UNION ALL
				
				select H.*,M.ref as nama_evaluasi 
				FROM tpoliklinik_edukasi_his_materi H

				LEFT JOIN merm_referensi M ON M.nilai=H.evaluasi AND M.ref_head_id='84'
				WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
			) H WHERE H.versi_edit='$versi_edit'
	  
	  ";
	  $list_info=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_info as $r){
		  $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			 $class_materi='';$class_evaluasi='';
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				// $last_pilih=$data_pilih['param_nilai_id'];
				if ($r->materi_edukasi!=$data_pilih['materi_edukasi']){
					$class_materi="text-danger";
				}
				if ($r->evaluasi!=$data_pilih['evaluasi']){
					$class_evaluasi="text-danger";
				}
			}
			
			
		  
		  $btn_ttd_petugas='';
		  $img=base_url().'qrcode/qr_code_ppa/'.$r->created_by;
		  $btn_ttd_petugas='
			<div class="img-container fx-img-rotate-r ">
				<img class="" style="width:60px;height:60px; text-align: center;" src="'.$img.'" alt="">
				<div class="text-muted text-center">'.$r->ttd_petugas_nama.'</div>
			</div>
		  ';
		  
		  $btn_ttd_sasaran='';
		  if ($r->ttd_sasaran){
		  $btn_ttd_sasaran='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$r->ttd_sasaran.'" alt="">
				<div class="text-muted text-center">'.$r->ttd_sasaran_nama.'</div>
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default btn-xs" onclick="modal_ttd_sasaran('.$r->id.')" href="javascript:void(0)"><i class="fa fa-pencil"></i> </a>
							<a class="btn btn-default btn-danger  btn-xs" onclick="hapus_ttd_sasaran('.$r->id.')" href="javascript:void(0)"><i class="fa fa-times"></i> </a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
		  }else{
			  // $btn_ttd_sasaran .= '<button onclick="modal_ttd_sasaran('.$r->id.')"  type="button" data-toggle="tooltip" title="Tanda Tangan" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
		  }
		  $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_materi_edukasi('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_materi_edukasi('.$r->id.')" type="button" title="Hapus Diagnosa" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  $tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="edukasi_detail_id" value="'.$r->id.'">'.HumanDateLong($r->input_tanggal).'</td>';
			$tabel .='<td class="'.$class_materi.'"><span class="">'.str_replace(' style=', ' data-mso-style=',$r->materi_edukasi).'</span></td>';
			$tabel .='<td class="text-center">'.$btn_ttd_petugas.'</td>';
			$tabel .='<td class="text-center">'.$btn_ttd_sasaran.'</td>';
			$tabel .='<td class="text-center '.$class_evaluasi.'">'.$r->nama_evaluasi.'</td>';
			$tabel .='<td></td>';
		  $tabel .='</tr>';
	  } 
	  $btn_ttd_dokter='';
	 
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function list_index_his_asmed_fisio()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tpoliklinik_asmed_fisio_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tpoliklinik_asmed_fisio` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_asmed_fisio/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function list_index_his_rencana_ranap()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tpoliklinik_ranap_perencanaan_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tpoliklinik_ranap_perencanaan` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/erm_perencanaan/his_perencanaan_ranap/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function list_index_his_rencana_bedah()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tpoliklinik_ranap_perencanaan_bedah_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tpoliklinik_ranap_perencanaan_bedah` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/erm_perencanaan/his_perencanaan_ranap/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function list_index_his_estimasi_biaya()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tpoliklinik_ranap_rencana_biaya_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tpoliklinik_ranap_rencana_biaya` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/erm_perencanaan/his_estimasi_biaya/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function load_implant_estimasi_biaya()
	{
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$versi_edit_last=$this->input->post('versi_edit');
		
			if ($versi_edit!='0'){
				$versi_edit_last=$versi_edit-1;
			}
			$q="
			
				SELECT *FROM (
					SELECT H.*
					FROM tpoliklinik_ranap_rencana_biaya_implant_his H
					INNER JOIN tpoliklinik_ranap_rencana_biaya HH ON HH.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id'
					UNION ALL
					SELECT HH.jml_edit as versi_edit,H.*
					FROM tpoliklinik_ranap_rencana_biaya_implant H
					INNER JOIN tpoliklinik_ranap_rencana_biaya HH ON HH.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id'
				) T WHERE T.versi_edit='$versi_edit_last'
				ORDER BY T.id ASC
				";
			$data_last=$this->db->query($q)->result_array();
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.*
							FROM tpoliklinik_ranap_rencana_biaya_implant_his H
							INNER JOIN tpoliklinik_ranap_rencana_biaya HH ON HH.assesmen_id=H.assesmen_id
							WHERE H.assesmen_id='$assesmen_id'
							UNION ALL
							SELECT HH.jml_edit as versi_edit,H.*
							FROM tpoliklinik_ranap_rencana_biaya_implant H
							INNER JOIN tpoliklinik_ranap_rencana_biaya HH ON HH.assesmen_id=H.assesmen_id
							WHERE H.assesmen_id='$assesmen_id'
						) T WHERE T.versi_edit='$versi_edit'
						ORDER BY T.id ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_implant','ukuran_implant','merk_implant','catatan_implant');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih='1';
			}else{
				$last_pilih='0';
			}
			
          $result[] = $no;
		  $aksi='';
		  if ($data_pilih){
			  
			  $result[] = '<span class="'.($r->nama_implant <> $data_pilih['nama_implant']?'text-danger':'').'">'.($r->nama_implant).'</span>';
			  $result[] = '<span class="'.($r->ukuran_implant <> $data_pilih['ukuran_implant']?'text-danger':'').'">'.($r->ukuran_implant).'</span>';
			  $result[] = '<span class="'.($r->merk_implant <> $data_pilih['merk_implant']?'text-danger':'').'">'.($r->merk_implant).'</span>';
			  $result[] = '<span class="'.($r->catatan_implant <> $data_pilih['catatan_implant']?'text-danger':'').'">'.($r->catatan_implant).'</span>';
			  $result[] = '<span class="'.($r->jumlah_implant <> $data_pilih['jumlah_implant']?'text-danger':'').'">'.($r->jumlah_implant).'</span>';
			  $result[] = '<span class="'.($r->total_biaya_implant <> $data_pilih['total_biaya_implant']?'text-danger':'').'">'.($r->total_biaya_implant).'</span>';
		  }else{
			   $result[] = ($r->nama_implant);
				  $result[] = ($r->ukuran_implant);
				  $result[] = ($r->merk_implant);
				  $result[] = ($r->catatan_implant);
				  $result[] = ($r->jumlah_implant);
				  $result[] = ($r->total_biaya_implant);
		  }
         
          $aksi = '<div class="btn-group">';
		  if ($data_pilih=='0'){
		  $aksi .= '<button  type="button" title="Hapus Implant" class="btn btn-danger btn-xs  btn_kolom"><i class="fa fa-lock"></i> NEW RECORD</button>';	
			  
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function refresh_diagnosa_ri(){
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $versi_edit=$this->input->post('versi_edit');
	  $hasil='';
	  $q="SELECT H.id, CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa  
			FROM (
				SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mdiagnosa_id,D.created_by,D.created_date,D.prioritas FROM tranap_assesmen_ri H
				LEFT JOIN tranap_assesmen_diagnosa D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

				UNION ALL

				SELECT H.assesmen_id,H.versi_edit,H.id,H.mdiagnosa_id,H.created_by,H.created_date,H.prioritas FROM tranap_assesmen_diagnosa_his H
				WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
			) H
			
			LEFT JOIN mdiagnosa_ranap MD ON MD.id=H.mdiagnosa_id 
			WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit'
			ORDER BY H.prioritas ASC";
			// // // // print_r($q);exit;
	  $row=$this->db->query($q)->result();
		  $hasil .='<option value="" selected>Pilih Opsi</option>';
	  foreach($row as $r){
		  $hasil .='<option value="'.$r->id.'" selected>'.$r->diagnosa.'</option>';
	  }
	  $data['list']=$hasil;
	  echo json_encode($hasil);
  }
 function load_data_rencana_asuhan_ri(){
		$assesmen_id=$this->input->post('assesmen_id');
		$diagnosa_id=$this->input->post('diagnosa_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
			SELECT *FROM (
				SELECT H.assesmen_id,H.jml_edit as versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM `tranap_assesmen_ri` H
				LEFT JOIN tranap_assesmen_diagnosa_data D ON D.assesmen_id=H.assesmen_id 
				WHERE H.assesmen_id='$assesmen_id'
				UNION ALL

				SELECT D.assesmen_id,D.versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM tranap_assesmen_diagnosa_data_his D
				WHERE D.assesmen_id='$assesmen_id' AND D.versi_edit='$versi_edit_last'
				) H 
				WHERE H.versi_edit='$versi_edit_last' AND H.diagnosa_id='$diagnosa_id'

				ORDER BY H.nourut ASC
		";
		$data_last=$this->db->query($q)->result_array();
		$q="
			SELECT *FROM (
				SELECT H.assesmen_id,H.jml_edit as versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM `tranap_assesmen_ri` H
				LEFT JOIN tranap_assesmen_diagnosa_data D ON D.assesmen_id=H.assesmen_id 
				WHERE H.assesmen_id='$assesmen_id'
				UNION ALL

				SELECT D.assesmen_id,D.versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM tranap_assesmen_diagnosa_data_his D
				WHERE D.assesmen_id='$assesmen_id' AND D.versi_edit='$versi_edit'
				) H 
				WHERE H.versi_edit='$versi_edit' AND H.diagnosa_id='$diagnosa_id'

				ORDER BY H.nourut ASC
		";
		// print_r($q);exit;
		$row=$this->db->query($q)->result();
		$tabel='';
		$last_lev='';
		$jml_col='0';
		foreach($row as $r){
			if ($r->lev=='0'){//Section
				if ($last_lev =='2'){//Jika Record Baru
					if ($jml_col==1){
					$tabel .='<td></td>';
						
					}
					$tabel .='</tr>';
				}
				$last_lev=$r->lev;
				$tabel .='
					<tr>
						<td colspan="2" style="background-color:#d9efff; font-weight: bold;width:100%">'.$r->nama.'</td>
					</tr>				
				';
				$jml_col=0;
			}
			if ($r->lev=='1'){//Kategori
				if ($last_lev =='2'){//Jika Record Baru
					if ($jml_col==1){
					$tabel .='<td></td>';
						
					}
					$tabel .='</tr>';
				}
				$last_lev=$r->lev;
				$tabel .='
					<tr>
						<td colspan="2" style="background-color:#f7f7f7; font-weight: bold;width:100%" class="text-primary">'.$r->nama.'</td>
					</tr>				
				';
				$jml_col=0;
			}
			if ($r->lev=='2'){//Data
				if ($last_lev != $r->lev){//Jika Record Baru
					$tabel .='<tr>';
					$jml_col=$jml_col+1;
				}else{
					$jml_col=$jml_col+1;
				}
				if ($jml_col=='3'){
					$tabel .='</tr>';
					$tabel .='<tr>';
					$jml_col=1;
				}
				$data_id=$r->data_id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['data_id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['pilih'];
				}else{
					$last_pilih='0';
				}
				if ($r->pilih!=$last_pilih){
					$nama_class="edited2";
				}else{
					$nama_class="";
					
				}
				// print_r($data_pilih['pilih']);exit;
				$tabel .='
					<td style="width:50%">
						<input type="hidden" class="data_id" value="'.$r->id.'">
						<div class="checkbox">
							<label for="example-'.$r->id.'">
								<input id="example-'.$r->id.'" name="chk[]" class="data_asuhan "  type="checkbox" '.($r->pilih?'checked':'').'><span class="'.$nama_class.'"> '.$r->nama.'</span>
							</label>
						</div>
					</td>			
				';
				$last_lev=$r->lev;
			}
		}
		if ($jml_col==1){
		$tabel .='<td></td>';
			
		}
		$tabel .='</tr>';
		// $tabel .='<tr><td colspan="2">&nbsp;&nbsp;</td></tr>';
		// print_r($tabel);exit;
		echo json_encode($tabel);
  }
  function load_diagnosa_ri()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT MP.nama as nama_user,CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa, H.* FROM tranap_assesmen_diagnosa H
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN mdiagnosa_ranap MD ON MD.id=H.mdiagnosa_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						ORDER BY H.prioritas ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->diagnosa);
          $result[] = $r->prioritas;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="set_rencana_asuhan('.$r->id.')" type="button" title="Rencana Asuhan" class="btn btn-warning btn-xs btn_rencana"><i class="si si-book-open"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
 function list_index_his_assesmen_ri()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_assesmen_ri_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_assesmen_ri` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_assesmen_ri/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	
	function load_skrining_nutrisi(){
		$template_id=$this->input->post('template_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
			SELECT *FROM (
			SELECT D.jml_edit as versi_edit,H.* FROM tranap_assesmen_ri D
			INNER JOIN tranap_assesmen_ri_nutrisi H ON H.assesmen_id=D.assesmen_id
			WHERE D.assesmen_id='$assesmen_id' AND H.skrining_nutrisi_id='$template_id'
			UNION ALL
			SELECT H.* FROM tranap_assesmen_ri_nutrisi_his H
			WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit_last' AND H.skrining_nutrisi_id='$template_id'
			) T WHERE T.versi_edit='$versi_edit_last'";
		$data_last=$this->db->query($q)->result_array();
		
		$q="
			SELECT *FROM (
			SELECT D.jml_edit as versi_edit,H.* FROM tranap_assesmen_ri D
			INNER JOIN tranap_assesmen_ri_nutrisi H ON H.assesmen_id=D.assesmen_id
			WHERE D.assesmen_id='$assesmen_id' AND H.skrining_nutrisi_id='$template_id'
			UNION ALL
			SELECT H.* FROM tranap_assesmen_ri_nutrisi_his H
			WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit' AND H.skrining_nutrisi_id='$template_id'
			) T WHERE T.versi_edit='$versi_edit'";
		$hasil=$this->db->query($q)->result();
		$opsi='';
		$no=0;
		$total=0;
		foreach($hasil as $r){
		$nama_class="";
		$data_id=$r->id;
		$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
			return ($var['id'] == $data_id);
			});
		$data_pilih=reset($data_pilih);
		if ($data_pilih){
			if ($data_pilih['param_nilai_id']!=$r->param_nilai_id){
				$nama_class='edited';

			}
		}else{
			$nama_class='';
		}
		$no=$no+1;
		$opsi.='<tr>
				<td>'.$r->parameter_nama.'<input class="risiko_nilai_id" type="hidden"  value="'.$r->id.'" ></td>
				<td><select  class="form-control '.$nama_class.' " style="width: 100%;" data-placeholder="Pilih Opsi" required>
					'.$this->opsi_nilai_nutrisi($r->group_nilai,$r->param_nilai_id,$versi_edit).'
				</select>
				<td class="text-center">'.$r->nilai.'</td>
				</td>
				
			</tr>';
			$total=$total + $r->nilai;
		}
		$opsi.='<tr><td colspan="2" class="text-right"><strong>TOTAL</strong></td>';
		$opsi.='<td class="text-center"><strong><span class="label_total_nutrisi">'.$total.'</span></strong></td></tr>';
		$data['opsi']=$opsi;
		echo json_encode($data);
  }
  function opsi_nilai_nutrisi($param,$param_nilai_id){
	  $q="SELECT * FROM `mskrining_nutrisi_param_skor` H WHERE H.id IN ($param)";
	  $hasil='';
	  $row=$this->db->query($q)->result();
		  $hasil .='<option data-nilai="0" value="" '.($param_nilai_id==''?'selected':'').'>-Opsi-</option>';
	  foreach($row as $r){
		  $hasil .='<option data-nilai="'.$r->skor.'" value="'.$r->id.'" '.($param_nilai_id==$r->id?'selected':'').'>'.$r->deskripsi_nama.'</option>';
	  }
	  return $hasil;
  }
  function load_syaraf_mata_ri(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $pupil_kiri=$this->input->post('pupil_kiri');
	  $pupil_kiri_cahaya=$this->input->post('pupil_kiri_cahaya');
	  $pupil_kanan=$this->input->post('pupil_kanan');
	  $pupil_kanan_cahaya=$this->input->post('pupil_kanan_cahaya');
	  
	  $pupil_kiri_asal=$this->input->post('pupil_kiri_asal');
	  $pupil_kiri_cahaya_asal=$this->input->post('pupil_kiri_cahaya_asal');
	  $pupil_kanan_asal=$this->input->post('pupil_kanan_asal');
	  $pupil_kanan_cahaya_asal=$this->input->post('pupil_kanan_cahaya_asal');
	  $tabel='';
	  $nama_tabel='1';
	  $q="SELECT * FROM msyaraf WHERE id='$nama_tabel'";
	  $data_header=$this->db->query($q)->row();
	  
	  $q="SELECT * FROM msyaraf_scale_image WHERE syaraf_id='$nama_tabel' order by nourut";
	  $row=$this->db->query($q)->result();
	  $detail='';
	  $detail .='<tr>';
	  $jml_kolom=0;
	  $warna_awal='';
	  $array_warna=array();;
	  foreach($row as $r){
		  $detail .='<td class="border-full '.($r->nilai==$pupil_kiri_asal?'edited2':'').'" style="background-color:">
					<label class="css-input css-radio css-radio-primary push-10-r">
					<input type="radio" name="nrs_opsi_1" '.($r->nilai==$pupil_kiri?'checked':'').' onclick="set_syaraf('.$r->id.','.$r->nilai.','.$nama_tabel.')"><span></span> <strong>'.$r->nilai.'</strong>
					</label>';
		  $detail .='</td>';
		 $jml_kolom +=1;
	  }
	 
	  $detail .='</tr>';
	  $detail .='<tr>';
	  foreach($row as $r){
		  $detail .='<td class="border-full"  style="background-color:'.$r->warna.'">';
		  $detail .='</td>';
	  }
	  $detail .='</tr>';
	  $detail .='<tr>';
	  foreach($row as $r){
		  $detail .='<td class="border-full">';
		  $detail .='<img src="'.site_url().'assets/upload/wbf/'.$r->gambar.'" style="width:100px;height:100px" alt="Avatar">';
		  $detail .='</td>';
	  }
	  $detail .='</tr>';
	   $q="SELECT * FROM msyaraf_cahaya WHERE syaraf_id='$nama_tabel' order by id";
	  $row=$this->db->query($q)->result();
	  $opsi='<select  id="pupil_1"  class="form-control '.($pupil_kiri_cahaya!=$pupil_kiri_cahaya_asal?'edited':'').'" style="width: 100%;" data-placeholder="Pilih Opsi" required>';
	  $opsi .='<option value="" '.($pupil_kiri_cahaya == '' ? 'selected="selected"' : '').'>Pilih Opsi</option>';
		foreach($row as $r){
			$opsi .='<option value="'.$r->nilai.'"'.($pupil_kiri_cahaya == $r->nilai ? 'selected="selected"' : '').'>'.$r->keterangan.$r->nilai.'</option>';
		}
		$opsi .='</select>';
	
		$detail .='<tr>';
	  $detail .='<td colspan="2" class="text-left"><strong >Repleksi Cahaya '.$data_header->nama.'</strong>'.$opsi.'</td>';
	  $detail .='<td colspan="'.($jml_kolom-2).'"></td>';
	  $detail .='</tr>';
	   $tabel .='
		<table class="block-table text-center " style="width:100%">
			<tbody>
				<tr><td colspan="'.$jml_kolom.'" class="text-left">'.$data_header->isi_header.'</td></tr>
				<tr><td colspan="'.$jml_kolom.'" class="border-full text-bold"  style="background-color:#f5f5f5"><strong>'.$data_header->nama.'</strong></td></tr>
				'.$detail.'
				<tr><td colspan="'.$jml_kolom.'" class="text-left">'.$data_header->isi_footer.'</td></tr>
			</tbody>
		</table>
	  ';
	  
	  $nama_tabel='2';
	  $q="SELECT * FROM msyaraf WHERE id='$nama_tabel'";
	  $data_header=$this->db->query($q)->row();
	  
	  $q="SELECT * FROM msyaraf_scale_image WHERE syaraf_id='$nama_tabel' order by nourut";
	  $row=$this->db->query($q)->result();
	  $detail='';
	  $detail .='<tr>';
	  $jml_kolom=0;
	  $warna_awal='';
	  $array_warna=array();;
	  foreach($row as $r){
		  $detail .='<td class="border-full '.($r->nilai==$pupil_kanan_asal?'edited2':'').'" style="background-color:">
					<label class="css-input css-radio css-radio-primary push-10-r">
					<input type="radio" name="nrs_opsi_2" '.($r->nilai==$pupil_kanan?'checked':'').' onclick="set_syaraf('.$r->id.','.$r->nilai.','.$nama_tabel.')"><span></span> <strong>'.$r->nilai.'</strong>
					</label>';
		  $detail .='</td>';
		 $jml_kolom +=1;
	  }
	 
	  $detail .='</tr>';
	  $detail .='<tr>';
	  foreach($row as $r){
		  $detail .='<td class="border-full"  style="background-color:'.$r->warna.'">';
		  $detail .='</td>';
	  }
	  $detail .='</tr>';
	  $detail .='<tr>';
	  foreach($row as $r){
		  $detail .='<td class="border-full">';
		  $detail .='<img src="'.site_url().'assets/upload/wbf/'.$r->gambar.'" style="width:100px;height:100px" alt="Avatar">';
		  $detail .='</td>';
	  }
	  $detail .='</tr>';
	   $q="SELECT * FROM msyaraf_cahaya WHERE syaraf_id='$nama_tabel' order by id";
	  $row=$this->db->query($q)->result();
	  $opsi='<select  id="pupil_2"  class="form-control '.($pupil_kanan_cahaya==$pupil_kanan_cahaya_asal?'edited':'').'" style="width: 100%;" data-placeholder="Pilih Opsi" required>';
	  $opsi .='<option value="" '.($pupil_kanan_cahaya == '' ? 'selected="selected"' : '').'>Pilih Opsi</option>';
		foreach($row as $r){
			$opsi .='<option value="'.$r->nilai.'"'.($pupil_kanan_cahaya == $r->nilai ? 'selected="selected"' : '').'>'.$r->keterangan.'</option>';
		}
		$opsi .='</select>';
	
		$detail .='<tr>';
	  $detail .='<td colspan="2" class="text-left"><strong >Repleksi Cahaya '.$data_header->nama.'</strong>'.$opsi.'</td>';
	  $detail .='<td colspan="'.($jml_kolom-2).'"></td>';
	  $detail .='</tr>';
	   $tabel .='
		<table class="block-table text-center " style="width:100%">
			<tbody>
				<tr><td colspan="'.$jml_kolom.'" class="text-left"><strong>'.$data_header->isi_header.'</strong></td></tr>
				<tr><td colspan="'.$jml_kolom.'" class="border-full text-bold"  style="background-color:#f5f5f5"><strong>'.$data_header->nama.'</strong></td></tr>
				'.$detail.'
				<tr><td colspan="'.$jml_kolom.'" class="text-left">'.$data_header->isi_footer.'</td></tr>
			</tbody>
		</table>
	  ';
	  
	  $this->output->set_output(json_encode($tabel));
  }
  function get_uji_otot_ri(){
	  $uji_otot_id=$this->input->post('uji_otot_id');
	  $nilai_otot_1=$this->input->post('nilai_otot_1');
	  $nilai_otot_2=$this->input->post('nilai_otot_2');
	  $nilai_otot_3=$this->input->post('nilai_otot_3');
	  $nilai_otot_4=$this->input->post('nilai_otot_4');

	  $nilai_otot_1_asal=$this->input->post('nilai_otot_1_asal');
	  $nilai_otot_2_asal=$this->input->post('nilai_otot_2_asal');
	  $nilai_otot_3_asal=$this->input->post('nilai_otot_3_asal');
	  $nilai_otot_4_asal=$this->input->post('nilai_otot_4_asal');
	  $q="
		SELECT H.* FROM muji_otot H
			WHERE H.id='$uji_otot_id'
	  ";
	  $data_otot=$this->db->query($q)->row();
	  $q="
		SELECT H.nourut, D.* FROM muji_otot_param H
			LEFT JOIN muji_otot_param_detail D ON D.parameter_id=H.id
			WHERE H.uji_otot_id='$uji_otot_id' AND H.staktif='1' AND D.staktif='1'
			ORDER BY H.nourut,D.urutan ASC
	  ";
	  $data_detail=$this->db->query($q)->result_array();
	  $q="
		SELECT * FROM `muji_otot_param` H WHERE H.uji_otot_id='$uji_otot_id' AND H.staktif='1' ORDER BY H.nourut
	  ";
	  $list_data=$this->db->query($q)->result();
	  $detail='';
	  $nilai_otot=1;
	  foreach($list_data as $r){
		  $nilai_pembanding=$this->get_variable_ri($nilai_otot,$nilai_otot_1,$nilai_otot_2,$nilai_otot_3,$nilai_otot_4);
		  $nilai_pembanding_asal=$this->get_variable_ri_asal($nilai_otot,$nilai_otot_1_asal,$nilai_otot_2_asal,$nilai_otot_3_asal,$nilai_otot_4_asal);
		   $parameter_id=$r->id;
			$data_pilih = array_filter($data_detail, function($var) use ($parameter_id) { 
				return ($var['parameter_id'] == $parameter_id);
			  });
			  $jml_kolom=0;
			 // print_r($data_pilih);exit;
			$tabel='<tr>';
			foreach($data_pilih as $data){
				$nama_kelas='';
			
				$tabel .='<td class="border-full '.($data['nilai']==$nilai_pembanding_asal?'edited2':'').'" style="background-color:bg-gray-lighter">
					<label class="css-input css-radio css-radio-primary push-10-r">
					<input type="radio" name="opsi_uji_'.$parameter_id.'" '.($data['nilai']==$nilai_pembanding?'checked':'').'  onclick="set_uji('.$nilai_otot.','.$data['nilai'].')"><span></span> <strong>'.$data['deskripsi_nama'].'</strong>
					</label>';
				$tabel .='</td>';
				// print_r($data['warna']);
				 $jml_kolom +=1;
			}
			$tabel .='</tr>';
			$tabel .='<tr>';
			foreach($data_pilih as $data){
				$tabel .='<td class="border-full" style="background-color:'.$data['warna'].'">';
				$tabel .='</td>';
			}
			$tabel .='</tr>';
			
		  $detail .='<div class="col-md-6 " style="margin-top:15px;!importan">
						<table class="block-table text-center " style="width:100%">
							<tbody>
								<tr><td colspan="'.$jml_kolom.'" class="border-full text-bold"  style="background-color:#f5f5f5"><strong>'.$r->parameter_nama.'</strong></td></tr>
								'.$tabel.'
							</tbody>
						</table>
					</div>
					';
			$nilai_otot +=1;
	  }
	  $detail ='
		<div class="col-md-12" style="margin-bottom:-25px;!importan"><label>'.$data_otot->isi_header.'</label></div>	
			'.$detail.'
		<div class="col-md-12" style="margin-top:15px;!importan"><label>'.$data_otot->isi_footer.'</label></div>	  
	  ';
	  $this->output->set_output(json_encode($detail));
  }
  function get_variable_ri($nilai_otot,$var1,$var2,$var3,$var4){
	  $nilai=0;
	  if ($nilai_otot=='1'){
		  $nilai=$var1;
	  }
	  if ($nilai_otot=='2'){
		  $nilai=$var2;
	  }
	  if ($nilai_otot=='3'){
		  $nilai=$var3;
	  }
	  if ($nilai_otot=='4'){
		  $nilai=$var4;
	  }
	  return $nilai;
  }
  function get_variable_ri_asal($nilai_otot,$var1,$var2,$var3,$var4){
	  $nilai=0;
	  if ($nilai_otot=='1'){
		  $nilai=$var1;
	  }
	  if ($nilai_otot=='2'){
		  $nilai=$var2;
	  }
	  if ($nilai_otot=='3'){
		  $nilai=$var3;
	  }
	  if ($nilai_otot=='4'){
		  $nilai=$var4;
	  }
	  return $nilai;
  }
  function load_risiko_jatuh_ri(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $versi_edit=$this->input->post('versi_edit');
	  $versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
			SELECT *FROM (
				SELECT D.id,D.versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai,M.deskripsi_nama,D.param_id    
				FROM `tranap_risiko_jatuh_x_his_detail` D
				LEFT JOIN mrisiko_jatuh_ranap_param_skor M ON M.id=D.param_nilai_id
				WHERE D.assesmen_id='$assesmen_id'
				UNION ALL
				SELECT D.id,H.jml_edit as versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai,M.deskripsi_nama,D.param_id      
				FROM tranap_risiko_jatuh H
				LEFT JOIN `tranap_risiko_jatuh_detail` D ON D.assesmen_id=H.assesmen_id
				LEFT JOIN mrisiko_jatuh_ranap_param_skor M ON M.id=D.param_nilai_id
				WHERE H.assesmen_id='$assesmen_id'
			) H 
			WHERE H.versi_edit='$versi_edit_last'
			ORDER BY H.id
		";
		// print_r($q);exit;
		$data_last=$this->db->query($q)->result_array();
	  // $q="SELECT * FROM `tpoliklinik_assesmen_risiko_jatuh` H WHERE H.assesmen_id='$assesmen_id'";
	 $q="
			SELECT *FROM (
				SELECT D.id,D.versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai,M.deskripsi_nama,D.param_id  
				FROM `tranap_risiko_jatuh_x_his_detail` D
				LEFT JOIN mrisiko_jatuh_ranap_param_skor M ON M.id=D.param_nilai_id
				WHERE D.assesmen_id='$assesmen_id'
				UNION ALL
				SELECT D.id,H.jml_edit as versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai,M.deskripsi_nama,D.param_id    
				FROM tranap_risiko_jatuh H
				LEFT JOIN `tranap_risiko_jatuh_detail` D ON D.assesmen_id=H.assesmen_id
				LEFT JOIN mrisiko_jatuh_ranap_param_skor M ON M.id=D.param_nilai_id
				WHERE H.assesmen_id='$assesmen_id'
			) H 
			WHERE H.versi_edit='$versi_edit'
			ORDER BY H.id
		";
	  $hasil=$this->db->query($q)->result();
	  $opsi='';
	  $no=0;
	  foreach($hasil as $r){
		  $no=$no+1;
		  $param_id=$r->param_id;
			$data_pilih = array_filter($data_last, function($var) use ($param_id) { 
				return ($var['param_id'] == $param_id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['param_nilai_id'];
			}else{
				$last_pilih='#';
			}
			$nama_class="";
			if ($last_pilih !='#'){
				if ($r->param_nilai_id!=$last_pilih){
					$nama_class="text-danger";
				}else{
					$nama_class="";
					
				}
			}
			
		 $opsi.='<tr>
					<td>'.$r->parameter_nama.' &nbsp;&nbsp;&nbsp;&nbsp;'.($nama_class!=''?text_danger('PERUBAHAN'):'').'<input class="risiko_nilai_id" type="hidden"  value="'.$r->id.'" ></td>
					<td class="'.$nama_class.'"><strong>'.$r->deskripsi_nama.'</strong></td>
					<td class="'.$nama_class.' text-center"><strong>'.$r->nilai.'</strong></td>
					
				</tr>';
	  }
	  $data['opsi']=$opsi;
	  echo json_encode($data);
  }
  function list_index_his_assesmen_rj()//Risiko Jatuh
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_risiko_jatuh_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_risiko_jatuh` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_risiko_jatuh/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function list_index_his_lokasi_operasi()//Risiko Jatuh
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit,H.pendaftaran_id,H.st_ranap
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_lokasi_operasi_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit,H.pendaftaran_id,H.st_ranap
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_lokasi_operasi` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			  if ($r->st_ranap=='1'){
			 $aksi='<a class="btn btn-default menu_click btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_site/his_lokasi_operasi/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }else{
			 $aksi='<a class="btn btn-default menu_click btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_site/his_lokasi_operasi_rj/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function list_index_his_sga()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_sga_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_sga` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_sga/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function load_skrining_sga(){
		$footer_pengkajian=$this->input->post('footer_pengkajian');
		$template_id=$this->input->post('template_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
			SELECT *FROM (
			SELECT D.jml_edit as versi_edit,H.* FROM tranap_sga D
			INNER JOIN tranap_sga_pengkajian H ON H.assesmen_id=D.assesmen_id
			WHERE D.assesmen_id='$assesmen_id' 
			UNION ALL
			SELECT H.* FROM tranap_sga_x_his_pengkajian H
			WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit_last' 
			) T WHERE T.versi_edit='$versi_edit_last'";
		$data_last=$this->db->query($q)->result_array();
		
		$q="
			SELECT *FROM (
			SELECT D.jml_edit as versi_edit,H.* FROM tranap_sga D
			INNER JOIN tranap_sga_pengkajian H ON H.assesmen_id=D.assesmen_id
			WHERE D.assesmen_id='$assesmen_id' 
			UNION ALL
			SELECT H.* FROM tranap_sga_x_his_pengkajian H
			WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit' 
			) T WHERE T.versi_edit='$versi_edit'";
		$hasil=$this->db->query($q)->result();
		$opsi='';
		$no=0;
		$total=0;
		foreach($hasil as $r){
		$nama_class="";
		$data_id=$r->id;
		$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
			return ($var['id'] == $data_id);
			});
		$data_pilih=reset($data_pilih);
		if ($data_pilih){
			if ($data_pilih['nilai']!=$r->nilai){
				$nama_class='text-danger';

			}
		}else{
			$nama_class='';
		}
		$no=$no+1;
		$opsi.='<tr>
				<td class="'.$nama_class.' text-center">'.$no.'</td>
				<td class="'.$nama_class.'"><strong>'.$r->parameter_nama.'</strong></td>
				<td class="'.$nama_class.' text-center"><strong>'.$r->nilai_nama.'</strong></td>;
				
				</td>
				
			</tr>';
		}
		
		$data['opsi']=$opsi;
		echo json_encode($data);
  }
  function list_index_his_gizi_anak()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_gizi_anak_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_gizi_anak` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_gizi_anak/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	
	function load_skrining_gizi_anak(){
		$footer_pengkajian=$this->input->post('footer_pengkajian');
		$template_id=$this->input->post('template_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
			SELECT *FROM (
			SELECT D.jml_edit as versi_edit,H.* FROM tranap_gizi_anak D
			INNER JOIN tranap_gizi_anak_pengkajian H ON H.assesmen_id=D.assesmen_id
			WHERE D.assesmen_id='$assesmen_id' 
			UNION ALL
			SELECT H.* FROM tranap_gizi_anak_x_his_pengkajian H
			WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit_last' 
			) T WHERE T.versi_edit='$versi_edit_last'";
		$data_last=$this->db->query($q)->result_array();
		
		$q="
			SELECT *FROM (
			SELECT D.jml_edit as versi_edit,H.* FROM tranap_gizi_anak D
			INNER JOIN tranap_gizi_anak_pengkajian H ON H.assesmen_id=D.assesmen_id
			WHERE D.assesmen_id='$assesmen_id' 
			UNION ALL
			SELECT H.* FROM tranap_gizi_anak_x_his_pengkajian H
			WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit' 
			) T WHERE T.versi_edit='$versi_edit'";
		$hasil=$this->db->query($q)->result();
		$opsi='';
		$no=0;
		$total=0;
		foreach($hasil as $r){
		$nama_class="";
		$data_id=$r->id;
		$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
			return ($var['id'] == $data_id);
			});
		$data_pilih=reset($data_pilih);
		if ($data_pilih){
			if ($data_pilih['nilai']!=$r->nilai){
				$nama_class='text-danger';

			}
		}else{
			$nama_class='';
		}
		$no=$no+1;
		$opsi.='<tr>
				<td class="'.$nama_class.' text-center">'.$no.'</td>
				<td class="'.$nama_class.'"><strong>'.$r->parameter_nama.'</strong></td>
				<td class="'.$nama_class.' text-center"><strong>'.$r->nilai_nama.'</strong></td>;
				<td class="'.$nama_class.' text-center"><strong>'.$r->skor.'</strong></td>;
				
				</td>
				
			</tr>';
		}
		
		$data['opsi']=$opsi;
		echo json_encode($data);
  }
  function load_diagnosa_gizi_lanjutan()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$versi_edit_last=$this->input->post('versi_edit');
		
			if ($versi_edit!='0'){
				$versi_edit_last=$versi_edit-1;
			}
			$q="
				SELECT  MP.nama as nama_user,CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa, H.*  
				FROM (
					SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mdiagnosa_id,D.created_by,D.created_date,D.prioritas FROM tranap_gizi_lanjutan H
					LEFT JOIN tranap_gizi_lanjutan_diagnosa D ON D.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

					UNION ALL

					SELECT H.assesmen_id,H.versi_edit,H.id,H.mdiagnosa_id,H.created_by,H.created_date,H.prioritas FROM tranap_gizi_lanjutan_x_his_diagnosa H
					WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
					) H
				LEFT JOIN mppa MP ON MP.id=H.created_by
				LEFT JOIN mdiagnosa_gizi MD ON MD.id=H.mdiagnosa_id 
				WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit_last'
				ORDER BY H.prioritas ASC
			";
			$data_last=$this->db->query($q)->result_array();
			$this->select = array();
			$from="
					(
						SELECT  MP.nama as nama_user,CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa, H.*  
						FROM (
							SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mdiagnosa_id,D.created_by,D.created_date,D.prioritas FROM tranap_gizi_lanjutan H
							LEFT JOIN tranap_gizi_lanjutan_diagnosa D ON D.assesmen_id=H.assesmen_id
							WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

							UNION ALL

							SELECT H.assesmen_id,H.versi_edit,H.id,H.mdiagnosa_id,H.created_by,H.created_date,H.prioritas FROM tranap_gizi_lanjutan_x_his_diagnosa H
							WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
							) H
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN mdiagnosa_gizi MD ON MD.id=H.mdiagnosa_id 
						WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit'
						ORDER BY H.prioritas ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		  $mdiagnosa_id=$r->mdiagnosa_id;
			$data_pilih = array_filter($data_last, function($var) use ($mdiagnosa_id) { 
				return ($var['mdiagnosa_id'] == $mdiagnosa_id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['prioritas'];
			}else{
				$last_pilih='#';
			}
			if ($r->prioritas!=$last_pilih){
				$nama_class=' &nbsp;&nbsp;'.text_danger('Changed');
			}else{
				$nama_class="";
				
			}
			
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->diagnosa).$nama_class;
          $result[] = $r->prioritas;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  // $aksi .= '<button onclick="edit_diagnosa('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  // $aksi .= '<button onclick="hapus_diagnosa('.$r->id.')" type="button" title="Hapus Diagnosa" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '<button onclick="set_rencana_asuhan('.$r->id.')" type="button" title="Rencana Asuhan" class="btn btn-success btn-xs btn_rencana"><i class="si si-book-open"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_data_rencana_asuhan_gizi_lanjutan(){
		$assesmen_id=$this->input->post('assesmen_id');
		$diagnosa_id=$this->input->post('diagnosa_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
			SELECT *FROM (
				SELECT H.assesmen_id,H.jml_edit as versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM `tranap_gizi_lanjutan` H
				LEFT JOIN tranap_gizi_lanjutan_diagnosa_data D ON D.assesmen_id=H.assesmen_id 
				WHERE H.assesmen_id='$assesmen_id'
				UNION ALL

				SELECT D.assesmen_id,D.versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM tranap_gizi_lanjutan_x_his_diagnosa_data D
				WHERE D.assesmen_id='$assesmen_id' AND D.versi_edit='$versi_edit_last'
				) H 
				WHERE H.versi_edit='$versi_edit_last' AND H.diagnosa_id='$diagnosa_id'

				ORDER BY H.nourut ASC
		";
		$data_last=$this->db->query($q)->result_array();
		$q="
			SELECT *FROM (
				SELECT H.assesmen_id,H.jml_edit as versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM `tranap_gizi_lanjutan` H
				LEFT JOIN tranap_gizi_lanjutan_diagnosa_data D ON D.assesmen_id=H.assesmen_id 
				WHERE H.assesmen_id='$assesmen_id'
				UNION ALL

				SELECT D.assesmen_id,D.versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM tranap_gizi_lanjutan_x_his_diagnosa_data D
				WHERE D.assesmen_id='$assesmen_id' AND D.versi_edit='$versi_edit'
				) H 
				WHERE H.versi_edit='$versi_edit' AND H.diagnosa_id='$diagnosa_id'

				ORDER BY H.nourut ASC
		";
		// print_r($q);exit;
		$row=$this->db->query($q)->result();
		$tabel='';
		$last_lev='';
		$jml_col='0';
		foreach($row as $r){
			if ($r->lev=='0'){//Section
				if ($last_lev =='2'){//Jika Record Baru
					if ($jml_col==1){
					$tabel .='<td></td>';
						
					}
					$tabel .='</tr>';
				}
				$last_lev=$r->lev;
				$tabel .='
					<tr>
						<td colspan="2" style="background-color:#d9efff; font-weight: bold;width:100%">'.$r->nama.'</td>
					</tr>				
				';
				$jml_col=0;
			}
			if ($r->lev=='1'){//Kategori
				if ($last_lev =='2'){//Jika Record Baru
					if ($jml_col==1){
					$tabel .='<td></td>';
						
					}
					$tabel .='</tr>';
				}
				$last_lev=$r->lev;
				$tabel .='
					<tr>
						<td colspan="2" style="background-color:#f7f7f7; font-weight: bold;width:100%" class="text-primary">'.$r->nama.'</td>
					</tr>				
				';
				$jml_col=0;
			}
			if ($r->lev=='2'){//Data
				if ($last_lev != $r->lev){//Jika Record Baru
					$tabel .='<tr>';
					$jml_col=$jml_col+1;
				}else{
					$jml_col=$jml_col+1;
				}
				if ($jml_col=='3'){
					$tabel .='</tr>';
					$tabel .='<tr>';
					$jml_col=1;
				}
				$data_id=$r->data_id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['data_id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['pilih'];
				}else{
					$last_pilih='0';
				}
				if ($r->pilih!=$last_pilih){
					$nama_class="edited2";
				}else{
					$nama_class="";
					
				}
				// print_r($data_pilih['pilih']);exit;
				$tabel .='
					<td style="width:50%">
						<input type="hidden" class="data_id" value="'.$r->id.'">
						<div class="checkbox">
							<label for="example-'.$r->id.'">
								<input id="example-'.$r->id.'" name="chk[]" class="data_asuhan "  type="checkbox" '.($r->pilih?'checked':'').'><span class="'.$nama_class.'"> '.$r->nama.'</span>
							</label>
						</div>
					</td>			
				';
				$last_lev=$r->lev;
			}
		}
		if ($jml_col==1){
		$tabel .='<td></td>';
			
		}
		$tabel .='</tr>';
		// $tabel .='<tr><td colspan="2">&nbsp;&nbsp;</td></tr>';
		// print_r($tabel);exit;
		echo json_encode($tabel);
  }
  function list_index_his_gizi_lanjutan()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_gizi_lanjutan_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_gizi_lanjutan` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_gizi_lanjutan/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
 function list_index_his_discarge()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_discarge_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_discarge` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_discarge/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function list_index_his_rekon_obat()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_rekon_obat_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_rekon_obat` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_rekon/his_rekon_obat/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function list_index_his_intra_hospital()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa,H.pendaftaran_id
							FROM `tranap_intra_hospital_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa,H.pendaftaran_id
							FROM `tranap_intra_hospital` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			  if ($r->pendaftaran_id_ranap=='0'){
					$aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_tf/his_intra_hospital_rj/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }else{
					$aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_tf/his_intra_hospital/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function list_index_his_tf_hospital()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_tf_hospital_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_tf_hospital` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_tf/his_tf_hospital/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function list_index_his_timbang()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_timbang_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_timbang` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_timbang/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function list_index_his_tindakan_anestesi()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$tipe_rj_ri=$this->input->post('tipe_rj_ri');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa,H.pendaftaran_id_ranap
							FROM `tranap_tindakan_anestesi_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa,H.pendaftaran_id_ranap
							FROM `tranap_tindakan_anestesi` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			if ($tipe_rj_ri=='1'){
				$aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_per/his_tindakan_anestesi_rj/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				
			}else{
				$aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_per/his_tindakan_anestesi/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				
			}
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = ($r->user_edited?$r->user_edited.'<br>'.HumanDateLong($r->edited_date):'');
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function load_index_informasi_tindakan_anestesi(){
	  $status_assemen=$this->input->post('status_assemen');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $ttd_dokter_pelaksana=$this->input->post('ttd_dokter_pelaksana');
	  $ttd_penerima_info=$this->input->post('ttd_penerima_info');
	  $versi_edit=$this->input->post('versi_edit');
	  $versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
		SELECT *FROM (
				select H.jml_edit as versi_edit,D.*
				FROM tranap_tindakan_anestesi_informasi D
				INNER JOIN tranap_tindakan_anestesi H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'

				UNION ALL

				select D.*
				FROM tranap_tindakan_anestesi_his_informasi D
				WHERE D.assesmen_id='$assesmen_id'

				) T WHERE T.versi_edit='$versi_edit_last'
		";
		$data_last=$this->db->query($q)->result_array();
		
	  $q=" SELECT 
		ttd_dokter_ina,ttd_dokter_eng,ket_ttd_dokter_ina,ket_ttd_dokter_eng,ttd_pasien_ina,ttd_pasien_eng,ket_ttd_pasien_ina,ket_ttd_pasien_eng FROM setting_ic_keterangan

	  ";
	  $row_ttd=$this->db->query($q)->row_array();
	   $q="
		SELECT *FROM (
				select H.jml_edit  as versi_edit,D.*
				FROM tranap_tindakan_anestesi_informasi D
				INNER JOIN tranap_tindakan_anestesi H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'

				UNION ALL

				select D.*
				FROM tranap_tindakan_anestesi_his_informasi D
				WHERE D.assesmen_id='$assesmen_id'

				) T WHERE T.versi_edit='$versi_edit'
		";
	  $list_info=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_info as $r){
		   $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['isi_informasi'];
			}else{
				$last_pilih='#';
			}
			if ($r->isi_informasi!=$last_pilih){
				$nama_class=' edited';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
			
		  $btn_paraf='';
		  if ($r->paraf){
		  $btn_paraf='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$r->paraf.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_faraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_paraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
		  }else{
			  $btn_paraf .= '<button onclick="modal_faraf('.$r->id.')"  type="button" data-toggle="tooltip" title="Paraf" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i></button>';
		  }
		  
		  $tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="informasi_id" value="'.$r->id.'">'.$r->nourut.'</td>';
			$tabel .='<td class="text-center"><strong>'.$r->nama.'</strong><br><i>'.$r->nama_english.'</i></td>';
			$tabel .='<td>'.$r->isi_informasi.'</td>';
			$tabel .='<td class="text-center">'.$btn_paraf.'</td>';
		  $tabel .='</tr>';
	  } 
	  $btn_ttd_dokter='';
	  if ($ttd_dokter_pelaksana){
		  $btn_ttd_dokter='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_dokter_pelaksana.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
	  }else{
		  $btn_ttd_dokter='<button class="btn btn-sm btn-success" onclick="modal_ttd_dokter()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $btn_ttd_kel='';
	  if ($ttd_penerima_info){
		  $btn_ttd_kel='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_penerima_info.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_kel()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_kel()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
	  }else{
		  $btn_ttd_kel='<button class="btn btn-sm btn-success" onclick="modal_ttd_kel()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_dokter_ina'].'<br><i>'.$row_ttd['ket_ttd_dokter_eng'].'</i></td>';
	  $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_dokter_ina'].'/'.$row_ttd['ttd_dokter_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_dokter.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_pasien_ina'].'<br><i>'.$row_ttd['ket_ttd_pasien_eng'].'</i></td>';
	   $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_pasien_ina'].'/'.$row_ttd['ttd_pasien_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_kel.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function list_index_his_risiko_tinggi()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$tipe_rj_ri=$this->input->post('tipe_rj_ri');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa,H.pendaftaran_id_ranap
							FROM `tranap_risiko_tinggi_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa,H.pendaftaran_id_ranap
							FROM `tranap_risiko_tinggi` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			if ($tipe_rj_ri=='1'){
				$aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_per/his_risiko_tinggi_rj/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				
			}else{
				$aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_per/his_risiko_tinggi/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				
			}
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = ($r->user_edited?$r->user_edited.'<br>'.HumanDateLong($r->edited_date):'');
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function load_index_informasi_risiko_tinggi(){
	  $status_assemen=$this->input->post('status_assemen');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $ttd_dokter_pelaksana=$this->input->post('ttd_dokter_pelaksana');
	  $ttd_penerima_info=$this->input->post('ttd_penerima_info');
	  $versi_edit=$this->input->post('versi_edit');
	  $versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
		SELECT *FROM (
				select H.jml_edit as versi_edit,D.*
				FROM tranap_risiko_tinggi_informasi D
				INNER JOIN tranap_risiko_tinggi H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'

				UNION ALL

				select D.*
				FROM tranap_risiko_tinggi_his_informasi D
				WHERE D.assesmen_id='$assesmen_id'

				) T WHERE T.versi_edit='$versi_edit_last'
		";
		$data_last=$this->db->query($q)->result_array();
		
	  $q=" SELECT 
		ttd_dokter_ina,ttd_dokter_eng,ket_ttd_dokter_ina,ket_ttd_dokter_eng,ttd_pasien_ina,ttd_pasien_eng,ket_ttd_pasien_ina,ket_ttd_pasien_eng FROM setting_ic_keterangan

	  ";
	  $row_ttd=$this->db->query($q)->row_array();
	   $q="
		SELECT *FROM (
				select H.jml_edit  as versi_edit,D.*
				FROM tranap_risiko_tinggi_informasi D
				INNER JOIN tranap_risiko_tinggi H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'

				UNION ALL

				select D.*
				FROM tranap_risiko_tinggi_his_informasi D
				WHERE D.assesmen_id='$assesmen_id'

				) T WHERE T.versi_edit='$versi_edit'
		";
	  $list_info=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_info as $r){
		   $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['isi_informasi'];
			}else{
				$last_pilih='#';
			}
			if ($r->isi_informasi!=$last_pilih){
				$nama_class=' edited';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
			
		  $btn_paraf='';
		  if ($r->paraf){
		  $btn_paraf='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$r->paraf.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_faraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_paraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
		  }else{
			  $btn_paraf .= '<button onclick="modal_faraf('.$r->id.')"  type="button" data-toggle="tooltip" title="Paraf" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i></button>';
		  }
		  
		  $tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="informasi_id" value="'.$r->id.'">'.$r->nourut.'</td>';
			$tabel .='<td class="text-center"><strong>'.$r->nama.'</strong><br><i>'.$r->nama_english.'</i></td>';
			$tabel .='<td>'.$r->isi_informasi.'</td>';
			$tabel .='<td class="text-center">'.$btn_paraf.'</td>';
		  $tabel .='</tr>';
	  } 
	  $btn_ttd_dokter='';
	  if ($ttd_dokter_pelaksana){
		  $btn_ttd_dokter='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_dokter_pelaksana.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
	  }else{
		  $btn_ttd_dokter='<button class="btn btn-sm btn-success" onclick="modal_ttd_dokter()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $btn_ttd_kel='';
	  if ($ttd_penerima_info){
		  $btn_ttd_kel='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_penerima_info.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_kel()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_kel()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
	  }else{
		  $btn_ttd_kel='<button class="btn btn-sm btn-success" onclick="modal_ttd_kel()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_dokter_ina'].'<br><i>'.$row_ttd['ket_ttd_dokter_eng'].'</i></td>';
	  $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_dokter_ina'].'/'.$row_ttd['ttd_dokter_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_dokter.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_pasien_ina'].'<br><i>'.$row_ttd['ket_ttd_pasien_eng'].'</i></td>';
	   $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_pasien_ina'].'/'.$row_ttd['ttd_pasien_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_kel.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function load_nrs_ri(){
		$assesmen_id=$this->input->post('assesmen_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		
		  $q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tranap_assesmen_nyeri_scale_number H 
				LEFT JOIN tranap_assesmen_nyeri D ON D.assesmen_id=H.assesmen_id
				WHERE  H.assesmen_id='$assesmen_id' 
				
				UNION ALL
				
				SELECT H.* FROM tranap_assesmen_nyeri_his_scale_number H WHERE assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit_last'
				ORDER BY H.versi_edit,H.nourut
		  ";
		  
		$data_last=$this->db->query($q)->result_array();
		$q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tranap_assesmen_nyeri_scale_number H 
				LEFT JOIN tranap_assesmen_nyeri D ON D.assesmen_id=H.assesmen_id
				WHERE  H.assesmen_id='$assesmen_id' 
				
				UNION ALL
				
				SELECT H.* FROM tranap_assesmen_nyeri_his_scale_number H WHERE assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit'
				ORDER BY H.versi_edit,H.nourut
		  ";
		  
		  $row=$this->db->query($q)->result();
		  $detail='';
		  $detail .='<tr>';
		  $ket_awal='';
		  $warna_awal='';
		  $nama_tabel="'tranap_assesmen_nyeri_scale_number'";
		  $index_arr=0;
		  $jml_kol=1;
		  $array_ket=array();;
		  $array_kol=array();;
		  $array_warna=array();;
		  foreach($row as $r){
			  $data_id=$r->id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['pilih'];
				}else{
					$last_pilih='0';
				}
				if ($r->pilih!=$last_pilih){
					$nama_class="edited2";
				}else{
					$nama_class="";
					
				}
			  $detail .='<td class="border-full '.$nama_class.'" style="background-color:bg-gray-lighter">
						<label class="css-input css-radio css-radio-primary push-10-r">
						<input type="radio" name="nrs_opsi" '.($r->pilih?'checked':'').' onclick="set_skala_nyeri('.$r->id.','.$r->nilai.','.$nama_tabel.')"><span></span> <strong>'.$r->nilai.'</strong>
						</label>';
			  $detail .='</td>';
			  if ($ket_awal!=$r->keterangan){
				  if ($ket_awal != ''){
						$array_ket[$index_arr]=$ket_awal;
						$array_kol[$index_arr]=$jml_kol;
						$array_warna[$index_arr]=$warna_awal;
						$ket_awal=$r->keterangan;
						$warna_awal=$r->warna;
						$index_arr=$index_arr+1;;
				  }else{
					  $ket_awal=$r->keterangan;
					  $warna_awal=$r->warna;
				  }
				  $jml_kol=1;
			  }else{
				  $jml_kol=$jml_kol+1;
			  }
		  }
		  $array_ket[$index_arr]=$ket_awal;
		  $array_kol[$index_arr]=$jml_kol;
		  $array_warna[$index_arr]=$warna_awal;
		  $detail .='</tr>';
		  $detail .='<tr>';
		  foreach($row as $r){
			  $detail .='<td class="border-full"  style="background-color:'.$r->warna.'">';
			  $detail .='</td>';
		  }
		  $detail .='</tr>';
		  $detail .='<tr>';
		  foreach($array_ket as $index=>$val){
			  $detail .='<td class="border-full" colspan="'.$array_kol[$index].'" style="background-color:'.$array_warna[$index].'"><strong>'.$val.'</strong></td>';
		  }
		  $detail .='</tr>';
		  
		   $tabel='
			<table class="block-table text-center " style="width:100%">
				<tbody>
					'.$detail.'
				</tbody>
			</table>
		  ';
		  //FACE
		  $q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tranap_assesmen_nyeri_scale_face H 
				INNER JOIN tranap_assesmen_nyeri D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id' 
				UNION ALL
				SELECT H.* 
				FROM tranap_assesmen_nyeri_his_scale_face H 
				WHERE H.assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit_last'
				ORDER BY H.nourut ASC
		  ";
		  
		$data_last=$this->db->query($q)->result_array();
		  
		  $nama_tabel="'tranap_assesmen_nyeri_scale_face'";
		  $q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tranap_assesmen_nyeri_scale_face H 
				INNER JOIN tranap_assesmen_nyeri D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id' 
				UNION ALL
				SELECT H.* 
				FROM tranap_assesmen_nyeri_his_scale_face H 
				WHERE H.assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit'
				ORDER BY H.nourut ASC
		  ";
		  // print_r($q);exit;
		  $row=$this->db->query($q)->result();
		  $detail ='';
		  $detail .='<tr>';
		   foreach($row as $r){
			  $detail .='<td >';
			  $detail .='<img src="'.site_url().'assets/upload/wbf/'.$r->gambar.'" style="width:100px;height:100px" alt="Avatar">';
			  $detail .='</td>';
		  }
		  $detail .='</tr>';
		  $detail .='<tr>';
		   foreach($row as $r){
			   $data_id=$r->id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['pilih'];
				}else{
					$last_pilih='0';
				}
				if ($r->pilih!=$last_pilih){
					$nama_class="edited2";
				}else{
					$nama_class="";
					
				}
			  $detail .='<td class="border-full '.$nama_class.'" >';
			  $detail .='<label class="css-input css-radio css-radio-primary push-10-r">
						<input type="radio"  '.($r->pilih?'checked':'').' onclick="set_skala_nyeri('.$r->id.','.$r->nilai.','.$nama_tabel.')" name="nrs_opsi" value="'.$r->nilai.'"><span></span> <strong>'.$r->nilai.'</strong>
						</label>';
			  $detail .='</td>';
		  }
		  $detail .='</tr>';
		  $tabel .='
			<table class="block-table text-center " style="width:100%">
				<tbody>
					'.$detail.'
				</tbody>
			</table>
		  ';
		  // print_r($array_warna);exit;
		  $this->output->set_output(json_encode($tabel));
	  }
	  function load_nilai_param_nyeri_ri(){
		 
		 $assesmen_id=$this->input->post('assesmen_id');
		 $versi_edit=$this->input->post('versi_edit');
		 $versi_edit_last=$this->input->post('versi_edit');
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		 $q="
			SELECT *FROM (
			SELECT D.jml_edit as versi_edit, H.* FROM tranap_assesmen_nyeri_param H
			INNER JOIN tranap_assesmen_nyeri D ON D.assesmen_id=H.assesmen_id
			WHERE H.assesmen_id='$assesmen_id'

			UNION ALL

			SELECT H.* FROM tranap_assesmen_nyeri_his_param H
			WHERE H.assesmen_id='$assesmen_id'

			) H 
			WHERE H.versi_edit='$versi_edit_last'
			ORDER BY H.id ASC
		  ";
		  
		$data_last=$this->db->query($q)->result_array();
		  $q="
			SELECT *FROM (
			SELECT D.jml_edit as versi_edit, H.* FROM tranap_assesmen_nyeri_param H
			INNER JOIN tranap_assesmen_nyeri D ON D.assesmen_id=H.assesmen_id
			WHERE H.assesmen_id='$assesmen_id'

			UNION ALL

			SELECT H.* FROM tranap_assesmen_nyeri_his_param H
			WHERE H.assesmen_id='$assesmen_id'

			) H 
			WHERE H.versi_edit='$versi_edit'
			ORDER BY H.id ASC";
		  $list_data=$this->db->query($q)->result();
		  $tabel='';
		  $nourut=1;
		  $total_skor=0;
		  foreach ($list_data as $row){
			  $warna='#fff';
			   $data_id=$row->id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['jawaban_id'];
				}else{
					$last_pilih='0';
				}
				if ($row->jawaban_id!=$last_pilih){
					$nama_class="edited";
				}else{
					$nama_class="";
					
				}
				$tabel .='<tr style="background-color:'.$warna.'">';
				$tabel .='<td><input type="hidden" class="parameter_id" value="'.$row->id.'">'.$nourut.'</td>';
				$tabel .='<td class="text-bold"><strong>'.$row->parameter_nama.'</strong></td>';
				$tabel .='<td>'.$this->opsi_nilai_nyeri($row->id,$row->jawaban_id,$nama_class).'</td>';
				$tabel .='<td class="text-bold"><input readonly type="text" class="form-control parameter_skor '.$nama_class.'" value="'.$row->jawaban_skor.'"></td>';
				$nourut=$nourut+1;
				$total_skor=$total_skor+$row->jawaban_skor;
			  $tabel .='</tr>';
		  }
		  $tabel.='<tr><td colspan="3" class="text-primary text-right"><strong>TOTAL SKOR SKALA NYERI</strong></td><td><input readonly type="text" class="form-control total_skor" value="'.$total_skor.'"></td></tr>';
		  
		  $arr['tabel']=$tabel;
		  $arr['total_skor']=$total_skor;
		  $this->output->set_output(json_encode($arr));
	  }
	  function list_index_his_nyeri_ri()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_assesmen_nyeri_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_assesmen_nyeri` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_nyeri_ri/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	//FISIO
	function load_nrs_fisio_ri(){
		$assesmen_id=$this->input->post('assesmen_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		
		  $q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM trawatinap_fisio_scale_number H 
				LEFT JOIN trawatinap_fisio D ON D.assesmen_id=H.assesmen_id
				WHERE  H.assesmen_id='$assesmen_id' 
				
				UNION ALL
				
				SELECT H.* FROM trawatinap_fisio_scale_number_his H WHERE assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit_last'
				ORDER BY H.versi_edit,H.nourut
		  ";
		  
		$data_last=$this->db->query($q)->result_array();
		$q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM trawatinap_fisio_scale_number H 
				LEFT JOIN trawatinap_fisio D ON D.assesmen_id=H.assesmen_id
				WHERE  H.assesmen_id='$assesmen_id' 
				
				UNION ALL
				
				SELECT H.* FROM trawatinap_fisio_scale_number_his H WHERE assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit'
				ORDER BY H.versi_edit,H.nourut
		  ";
		  
		  $row=$this->db->query($q)->result();
		  $detail='';
		  $detail .='<tr>';
		  $ket_awal='';
		  $warna_awal='';
		  $nama_tabel="'trawatinap_fisio_scale_number'";
		  $index_arr=0;
		  $jml_kol=1;
		  $array_ket=array();;
		  $array_kol=array();;
		  $array_warna=array();;
		  foreach($row as $r){
			  $data_id=$r->id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['pilih'];
				}else{
					$last_pilih='0';
				}
				if ($r->pilih!=$last_pilih){
					$nama_class="edited2";
				}else{
					$nama_class="";
					
				}
			  $detail .='<td class="border-full '.$nama_class.'" style="background-color:bg-gray-lighter">
						<label class="css-input css-radio css-radio-primary push-10-r">
						<input type="radio" name="nrs_opsi" '.($r->pilih?'checked':'').' onclick="set_skala_nyeri('.$r->id.','.$r->nilai.','.$nama_tabel.')"><span></span> <strong>'.$r->nilai.'</strong>
						</label>';
			  $detail .='</td>';
			  if ($ket_awal!=$r->keterangan){
				  if ($ket_awal != ''){
						$array_ket[$index_arr]=$ket_awal;
						$array_kol[$index_arr]=$jml_kol;
						$array_warna[$index_arr]=$warna_awal;
						$ket_awal=$r->keterangan;
						$warna_awal=$r->warna;
						$index_arr=$index_arr+1;;
				  }else{
					  $ket_awal=$r->keterangan;
					  $warna_awal=$r->warna;
				  }
				  $jml_kol=1;
			  }else{
				  $jml_kol=$jml_kol+1;
			  }
		  }
		  $array_ket[$index_arr]=$ket_awal;
		  $array_kol[$index_arr]=$jml_kol;
		  $array_warna[$index_arr]=$warna_awal;
		  $detail .='</tr>';
		  $detail .='<tr>';
		  foreach($row as $r){
			  $detail .='<td class="border-full"  style="background-color:'.$r->warna.'">';
			  $detail .='</td>';
		  }
		  $detail .='</tr>';
		  $detail .='<tr>';
		  foreach($array_ket as $index=>$val){
			  $detail .='<td class="border-full" colspan="'.$array_kol[$index].'" style="background-color:'.$array_warna[$index].'"><strong>'.$val.'</strong></td>';
		  }
		  $detail .='</tr>';
		  
		   $tabel='
			<table class="block-table text-center " style="width:100%">
				<tbody>
					'.$detail.'
				</tbody>
			</table>
		  ';
		  //FACE
		  $q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM trawatinap_fisio_scale_face H 
				INNER JOIN trawatinap_fisio D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id' 
				UNION ALL
				SELECT H.* 
				FROM trawatinap_fisio_scale_face_his H 
				WHERE H.assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit_last'
				ORDER BY H.nourut ASC
		  ";
		  
		$data_last=$this->db->query($q)->result_array();
		  
		  $nama_tabel="'trawatinap_fisio_scale_face'";
		  $q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM trawatinap_fisio_scale_face H 
				INNER JOIN trawatinap_fisio D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id' 
				UNION ALL
				SELECT H.* 
				FROM trawatinap_fisio_scale_face_his H 
				WHERE H.assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit'
				ORDER BY H.nourut ASC
		  ";
		  $row=$this->db->query($q)->result();
		  $detail ='';
		  $detail .='<tr>';
		   foreach($row as $r){
			  $detail .='<td >';
			  $detail .='<img src="'.site_url().'assets/upload/wbf/'.$r->gambar.'" style="width:100px;height:100px" alt="Avatar">';
			  $detail .='</td>';
		  }
		  $detail .='</tr>';
		  $detail .='<tr>';
		   foreach($row as $r){
			   $data_id=$r->id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['pilih'];
				}else{
					$last_pilih='0';
				}
				if ($r->pilih!=$last_pilih){
					$nama_class="edited2";
				}else{
					$nama_class="";
					
				}
			  $detail .='<td class="border-full '.$nama_class.'" >';
			  $detail .='<label class="css-input css-radio css-radio-primary push-10-r">
						<input type="radio"  '.($r->pilih?'checked':'').' onclick="set_skala_nyeri('.$r->id.','.$r->nilai.','.$nama_tabel.')" name="nrs_opsi" value="'.$r->nilai.'"><span></span> <strong>'.$r->nilai.'</strong>
						</label>';
			  $detail .='</td>';
		  }
		  $detail .='</tr>';
		  $tabel .='
			<table class="block-table text-center " style="width:100%">
				<tbody>
					'.$detail.'
				</tbody>
			</table>
		  ';
		  // print_r($array_warna);exit;
		  $this->output->set_output(json_encode($tabel));
	  }
	  function list_index_his_fisio_ri()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `trawatinap_fisio_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `trawatinap_fisio` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_fisio_ri/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	// mppa
	function list_index_his_mpp()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa,H.pendaftaran_id,H.st_ranap
							FROM `tranap_mpp_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa,H.pendaftaran_id,H.st_ranap
							FROM `tranap_mpp` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			  if ($r->st_ranap=='1'){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_mpp/his_mpp/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }else{
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/0/erm_mpp/his_mpp/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	
	function load_skrining_mpp(){
		$footer_pengkajian=$this->input->post('footer_pengkajian');
		$template_id=$this->input->post('template_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
			SELECT *FROM (
			SELECT D.jml_edit as versi_edit,H.* FROM tranap_mpp D
			INNER JOIN tranap_mpp_pengkajian H ON H.assesmen_id=D.assesmen_id
			WHERE D.assesmen_id='$assesmen_id' 
			UNION ALL
			SELECT H.* FROM tranap_mpp_x_his_pengkajian H
			WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit_last' 
			) T WHERE T.versi_edit='$versi_edit_last'";
		$data_last=$this->db->query($q)->result_array();
		
		$q="
			SELECT *FROM (
			SELECT D.jml_edit as versi_edit,H.* FROM tranap_mpp D
			INNER JOIN tranap_mpp_pengkajian H ON H.assesmen_id=D.assesmen_id
			WHERE D.assesmen_id='$assesmen_id' 
			UNION ALL
			SELECT H.* FROM tranap_mpp_x_his_pengkajian H
			WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit' 
			) T WHERE T.versi_edit='$versi_edit'";
		$hasil=$this->db->query($q)->result();
		$opsi='';
		$no=0;
		$total=0;
		foreach($hasil as $r){
		$nama_class="";
		$data_id=$r->id;
		$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
			return ($var['id'] == $data_id);
			});
		$data_pilih=reset($data_pilih);
		if ($data_pilih){
			if ($data_pilih['nilai']!=$r->nilai){
				$nama_class='text-danger';

			}
		}else{
			$nama_class='';
		}
		$no=$no+1;
		$opsi.='<tr>
				<td class="'.$nama_class.' text-center">'.$no.'</td>
				<td class="'.$nama_class.'"><strong>'.$r->parameter_nama.'</strong></td>
				<td class="'.$nama_class.' text-center"><strong>'.$r->nilai_nama.'</strong></td>;
				<td class="'.$nama_class.' text-center"><strong>'.$r->skor.'</strong></td>;
				
				</td>
				
			</tr>';
		}
		
		$data['opsi']=$opsi;
		echo json_encode($data);
  }
  function list_index_his_evaluasi_mpp()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$st_ranap=$this->input->post('st_ranap');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit,H.st_ranap,H.pendaftaran_id
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_evaluasi_mpp_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit,H.st_ranap,H.pendaftaran_id
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_evaluasi_mpp` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			  if ($r->st_ranap=='1'){
				  
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_mpp/his_evaluasi_mpp/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  }else{
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/0/erm_mpp/his_evaluasi_mpp/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function list_index_his_imp_mpp()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$st_ranap=$this->input->post('st_ranap');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit,H.st_ranap,H.pendaftaran_id
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_imp_mpp_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit,H.st_ranap,H.pendaftaran_id
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_imp_mpp` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			  if ($r->st_ranap=='1'){
				  
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_mpp/his_imp_mpp/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  }else{
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpoliklinik_ranap/tindakan/'.$r->pendaftaran_id.'/0/erm_mpp/his_imp_mpp/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function list_index_his_pulang()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_assesmen_pulang_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_assesmen_pulang` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_ri/his_assesmen_pulang/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	
	function load_diagnosa_pulang()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$versi_edit_last=$this->input->post('versi_edit');
		
			if ($versi_edit!='0'){
				$versi_edit_last=$versi_edit-1;
			}
			$q="
				SELECT  MP.nama as nama_user,CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa, H.*  
				FROM (
					SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mdiagnosa_id,D.created_by,D.created_date,D.prioritas FROM tranap_assesmen_pulang H
					LEFT JOIN tranap_assesmen_pulang_diagnosa D ON D.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

					UNION ALL

					SELECT H.assesmen_id,H.versi_edit,H.id,H.mdiagnosa_id,H.created_by,H.created_date,H.prioritas FROM tranap_assesmen_pulang_x_diagnosa_his H
					WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
					) H
				LEFT JOIN mppa MP ON MP.id=H.created_by
				LEFT JOIN mdiagnosa MD ON MD.id=H.mdiagnosa_id 
				WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit_last'
				ORDER BY H.prioritas ASC
			";
			$data_last=$this->db->query($q)->result_array();
			$this->select = array();
			$from="
					(
						SELECT  MP.nama as nama_user,CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa, H.*  
						FROM (
							SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mdiagnosa_id,D.created_by,D.created_date,D.prioritas FROM tranap_assesmen_pulang H
							LEFT JOIN tranap_assesmen_pulang_diagnosa D ON D.assesmen_id=H.assesmen_id
							WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

							UNION ALL

							SELECT H.assesmen_id,H.versi_edit,H.id,H.mdiagnosa_id,H.created_by,H.created_date,H.prioritas FROM tranap_assesmen_pulang_x_diagnosa_his H
							WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
							) H
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN mdiagnosa MD ON MD.id=H.mdiagnosa_id 
						WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit'
						ORDER BY H.prioritas ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		  $mdiagnosa_id=$r->mdiagnosa_id;
			$data_pilih = array_filter($data_last, function($var) use ($mdiagnosa_id) { 
				return ($var['mdiagnosa_id'] == $mdiagnosa_id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['prioritas'];
			}else{
				$last_pilih='#';
			}
			if ($r->prioritas!=$last_pilih){
				$nama_class=' &nbsp;&nbsp;'.text_danger('Changed');
			}else{
				$nama_class="";
				
			}
			
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->diagnosa).$nama_class;
          $result[] = $r->prioritas;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  // $aksi .= '<button onclick="edit_diagnosa('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  // $aksi .= '<button onclick="hapus_diagnosa('.$r->id.')" type="button" title="Hapus Diagnosa" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '<button onclick="set_rencana_asuhan('.$r->id.')" type="button" title="Rencana Asuhan" class="btn btn-success btn-xs btn_rencana"><i class="si si-book-open"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_data_rencana_asuhan_pulang(){
		$assesmen_id=$this->input->post('assesmen_id');
		$diagnosa_id=$this->input->post('diagnosa_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
			SELECT *FROM (
				SELECT H.assesmen_id,H.jml_edit as versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM `tranap_assesmen_pulang` H
				LEFT JOIN tranap_assesmen_pulang_diagnosa_data D ON D.assesmen_id=H.assesmen_id 
				WHERE H.assesmen_id='$assesmen_id'
				UNION ALL

				SELECT D.assesmen_id,D.versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM tranap_assesmen_pulang_x_diagnosa_data_his D
				WHERE D.assesmen_id='$assesmen_id' AND D.versi_edit='$versi_edit_last'
				) H 
				WHERE H.versi_edit='$versi_edit_last' AND H.diagnosa_id='$diagnosa_id'

				ORDER BY H.nourut ASC
		";
		$data_last=$this->db->query($q)->result_array();
		$q="
			SELECT *FROM (
				SELECT H.assesmen_id,H.jml_edit as versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM `tranap_assesmen_pulang` H
				LEFT JOIN tranap_assesmen_pulang_diagnosa_data D ON D.assesmen_id=H.assesmen_id 
				WHERE H.assesmen_id='$assesmen_id'
				UNION ALL

				SELECT D.assesmen_id,D.versi_edit,D.id,D.diagnosa_id,D.nama,D.pilih,D.lev,D.nourut,D.data_id FROM tranap_assesmen_pulang_x_diagnosa_data_his D
				WHERE D.assesmen_id='$assesmen_id' AND D.versi_edit='$versi_edit'
				) H 
				WHERE H.versi_edit='$versi_edit' AND H.diagnosa_id='$diagnosa_id'

				ORDER BY H.nourut ASC
		";
		// print_r($q);exit;
		$row=$this->db->query($q)->result();
		$tabel='';
		$last_lev='';
		$jml_col='0';
		foreach($row as $r){
			if ($r->lev=='0'){//Section
				if ($last_lev =='2'){//Jika Record Baru
					if ($jml_col==1){
					$tabel .='<td></td>';
						
					}
					$tabel .='</tr>';
				}
				$last_lev=$r->lev;
				$tabel .='
					<tr>
						<td colspan="2" style="background-color:#d9efff; font-weight: bold;width:100%">'.$r->nama.'</td>
					</tr>				
				';
				$jml_col=0;
			}
			if ($r->lev=='1'){//Kategori
				if ($last_lev =='2'){//Jika Record Baru
					if ($jml_col==1){
					$tabel .='<td></td>';
						
					}
					$tabel .='</tr>';
				}
				$last_lev=$r->lev;
				$tabel .='
					<tr>
						<td colspan="2" style="background-color:#f7f7f7; font-weight: bold;width:100%" class="text-primary">'.$r->nama.'</td>
					</tr>				
				';
				$jml_col=0;
			}
			if ($r->lev=='2'){//Data
				if ($last_lev != $r->lev){//Jika Record Baru
					$tabel .='<tr>';
					$jml_col=$jml_col+1;
				}else{
					$jml_col=$jml_col+1;
				}
				if ($jml_col=='3'){
					$tabel .='</tr>';
					$tabel .='<tr>';
					$jml_col=1;
				}
				$data_id=$r->data_id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['data_id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['pilih'];
				}else{
					$last_pilih='0';
				}
				if ($r->pilih!=$last_pilih){
					$nama_class="edited2";
				}else{
					$nama_class="";
					
				}
				// print_r($data_pilih['pilih']);exit;
				$tabel .='
					<td style="width:50%">
						<input type="hidden" class="data_id" value="'.$r->id.'">
						<div class="checkbox">
							<label for="example-'.$r->id.'">
								<input id="example-'.$r->id.'" name="chk[]" class="data_asuhan "  type="checkbox" '.($r->pilih?'checked':'').'><span class="'.$nama_class.'"> '.$r->nama.'</span>
							</label>
						</div>
					</td>			
				';
				$last_lev=$r->lev;
			}
		}
		if ($jml_col==1){
		$tabel .='<td></td>';
			
		}
		$tabel .='</tr>';
		// $tabel .='<tr><td colspan="2">&nbsp;&nbsp;</td></tr>';
		// print_r($tabel);exit;
		echo json_encode($tabel);
  }
   function refresh_diagnosa_pulang(){
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $versi_edit=$this->input->post('versi_edit');
	  $hasil='';
	  $q="SELECT H.id, CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa  
			FROM (
				SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mdiagnosa_id,D.created_by,D.created_date,D.prioritas FROM tranap_assesmen_pulang H
				LEFT JOIN tranap_assesmen_pulang_diagnosa D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

				UNION ALL

				SELECT H.assesmen_id,H.versi_edit,H.id,H.mdiagnosa_id,H.created_by,H.created_date,H.prioritas FROM tranap_assesmen_pulang_x_diagnosa_his H
				WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
			) H
			
			LEFT JOIN mdiagnosa MD ON MD.id=H.mdiagnosa_id 
			WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit'
			ORDER BY H.prioritas ASC";
	  $row=$this->db->query($q)->result();
		  $hasil .='<option value="" selected>Pilih Opsi</option>';
	  foreach($row as $r){
		  $hasil .='<option value="'.$r->id.'" selected>'.$r->diagnosa.'</option>';
	  }
	  $data['list']=$hasil;
	  echo json_encode($hasil);
  }
  function load_tindakan_assesmen_pulang()
	{
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			  $versi_edit_last=$this->input->post('versi_edit');
				
				if ($versi_edit!='0'){
					$versi_edit_last=$versi_edit-1;
				}
				$q="
				SELECT *FROM (
					SELECT D.jml_edit as versi_edit
					,H.*
					FROM tranap_assesmen_pulang_tindakan H
					LEFT JOIN tranap_assesmen_pulang D ON D.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'	
					
									
					UNION ALL

					SELECT H.*
					FROM tranap_assesmen_pulang_x_tindakan_his H
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'	
					
				) T WHERE T.versi_edit='$versi_edit_last'
				";
				$data_last=$this->db->query($q)->result_array();
			$q="
					
				SELECT *FROM (
					SELECT D.jml_edit as versi_edit
					,H.*
					FROM tranap_assesmen_pulang_tindakan H
					LEFT JOIN tranap_assesmen_pulang D ON D.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'	
					
									
					UNION ALL

					SELECT H.*
					FROM tranap_assesmen_pulang_x_tindakan_his H
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'	
				) T WHERE T.versi_edit='$versi_edit'
				";
			$list=$this->db->query($q)->result();
			$tabel='';
			$no=0;
      foreach ($list as $r) {
          $result = array();

			 $aksi='';
			  $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['id'];
			}else{
				$last_pilih='#';
			}
			if ($r->id!=$last_pilih){
				$nama_class=' text-danger';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
			 $no++;
				 $aksi='';
				 $tabel .='<tr>';
			  $tabel .='<td class="text-left '.$nama_class.'">'.($no).'</td>';
			  $tabel .='<td class="text-left '.$nama_class.'">'.$r->nama.'</td>';
			  $tabel .='<td class="text-left '.$nama_class.'">'.HumanDateShort_exp($r->tanggal).' </td>';
			  $aksi = '<div class="btn-group '.$nama_class.'">';
			  $aksi .= '</div>';
			   $tabel .='<td class="text-center">'.$aksi.'</td>';
			  $tabel .= '</tr>';


      }
	  $data['tabel']=$tabel;
      $this->output->set_output(json_encode($data));
  }
  function load_hasil_assesmen_pulang()
	{
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			  $versi_edit_last=$this->input->post('versi_edit');
				
				if ($versi_edit!='0'){
					$versi_edit_last=$versi_edit-1;
				}
				$q="
				SELECT *FROM (
					SELECT D.jml_edit as versi_edit
					,H.*,M.ref as jenis_nama
					FROM tranap_assesmen_pulang_hasil H
					LEFT JOIN tranap_assesmen_pulang D ON D.assesmen_id=H.assesmen_id
					LEFT JOIN merm_referensi M ON M.nilai=H.jenis_pemeriksaan AND M.ref_head_id='322'
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'	
					
									
					UNION ALL

					SELECT H.*,M.ref as jenis_nama
					FROM tranap_assesmen_pulang_x_hasil_his H
					LEFT JOIN merm_referensi M ON M.nilai=H.jenis_pemeriksaan AND M.ref_head_id='322'
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'		
					
				) T WHERE T.versi_edit='$versi_edit_last'
				";
				$data_last=$this->db->query($q)->result_array();
			$q="
					
				SELECT *FROM (
					SELECT D.jml_edit as versi_edit
					,H.*,M.ref as jenis_nama
					FROM tranap_assesmen_pulang_hasil H
					LEFT JOIN tranap_assesmen_pulang D ON D.assesmen_id=H.assesmen_id
					LEFT JOIN merm_referensi M ON M.nilai=H.jenis_pemeriksaan AND M.ref_head_id='322'
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'	
					
									
					UNION ALL

					SELECT H.*,M.ref as jenis_nama
					FROM tranap_assesmen_pulang_x_hasil_his H
					LEFT JOIN merm_referensi M ON M.nilai=H.jenis_pemeriksaan AND M.ref_head_id='322'
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'		
				) T WHERE T.versi_edit='$versi_edit'
				";
			$list=$this->db->query($q)->result();
			$tabel='';
			$no=0;
      foreach ($list as $r) {
          $result = array();

			 $aksi='';
			  $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['id'];
			}else{
				$last_pilih='#';
			}
			if ($r->id!=$last_pilih){
				$nama_class=' text-danger';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
			 $no++;
				 $aksi='';
				 $tabel .='<tr>';
          $tabel .='<td class="text-left '.$nama_class.'">'.($no).'</td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.$r->jenis_nama.'</td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.$r->hasil.'</td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.HumanDateShort_exp($r->tanggal).' </td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.$r->jumlah_lembar.'</td>';
          $aksi = '<div class="btn-group '.$nama_class.'">';
		  $aksi .= '</div>';
           $tabel .='<td class="text-center">'.$aksi.'</td>';
		  $tabel .= '</tr>';


      }
	  $data['tabel']=$tabel;
      $this->output->set_output(json_encode($data));
  }
  function load_kontrol_assesmen_pulang()
	{
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			  $versi_edit_last=$this->input->post('versi_edit');
				
				if ($versi_edit!='0'){
					$versi_edit_last=$versi_edit-1;
				}
				$q="
				SELECT *FROM (
					SELECT D.jml_edit as versi_edit
					,H.*,MP.nama as nama_poli,MD.nama as nama_dokter,DR.hari,HR.namahari 
					FROM tranap_assesmen_pulang_kontrol H
					LEFT JOIN tranap_assesmen_pulang D ON D.assesmen_id=H.assesmen_id
					LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli
					LEFT JOIN mdokter MD ON MD.id=H.iddokter
					INNER JOIN date_row DR ON DR.tanggal=H.tanggal
					LEFT JOIN merm_hari HR ON HR.kodehari=DR.hari
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'			
					
									
					UNION ALL

					SELECT H.*,MP.nama as nama_poli,MD.nama as nama_dokter,DR.hari,HR.namahari 
					FROM tranap_assesmen_pulang_x_kontrol_his H
					LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli
					LEFT JOIN mdokter MD ON MD.id=H.iddokter
					INNER JOIN date_row DR ON DR.tanggal=H.tanggal
					LEFT JOIN merm_hari HR ON HR.kodehari=DR.hari
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'			
					
				) T WHERE T.versi_edit='$versi_edit_last'
				";
				$data_last=$this->db->query($q)->result_array();
			$q="
					
				SELECT *FROM (
					SELECT D.jml_edit as versi_edit
					,H.*,MP.nama as nama_poli,MD.nama as nama_dokter,DR.hari,HR.namahari 
					FROM tranap_assesmen_pulang_kontrol H
					LEFT JOIN tranap_assesmen_pulang D ON D.assesmen_id=H.assesmen_id
					LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli
					LEFT JOIN mdokter MD ON MD.id=H.iddokter
					INNER JOIN date_row DR ON DR.tanggal=H.tanggal
					LEFT JOIN merm_hari HR ON HR.kodehari=DR.hari
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'			
					
									
					UNION ALL

					SELECT H.*,MP.nama as nama_poli,MD.nama as nama_dokter,DR.hari,HR.namahari 
					FROM tranap_assesmen_pulang_x_kontrol_his H
					LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli
					LEFT JOIN mdokter MD ON MD.id=H.iddokter
					INNER JOIN date_row DR ON DR.tanggal=H.tanggal
					LEFT JOIN merm_hari HR ON HR.kodehari=DR.hari
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'	
				) T WHERE T.versi_edit='$versi_edit'
				";
			$list=$this->db->query($q)->result();
			$tabel='';
			$no=0;
      foreach ($list as $r) {
          $result = array();

			 $aksi='';
			  $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['id'];
			}else{
				$last_pilih='#';
			}
			if ($r->id!=$last_pilih){
				$nama_class=' text-danger';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
			 $no++;
				 $aksi='';
				 $tabel .='<tr>';
          $tabel .='<td class="text-left '.$nama_class.'">'.($no).'</td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.$r->namahari.', '.HumanDateShort_exp($r->tanggal).' </td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.$r->nama_poli.'</td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.$r->nama_dokter.'</td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.$r->jam.'</td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.$r->keterangan.'</td>';
          $aksi = '<div class="btn-group">';
		  $aksi .= '</div>';
           $tabel .='<td class="text-center">'.$aksi.'</td>';
		  $tabel .= '</tr>';


      }
	  $data['tabel']=$tabel;
      $this->output->set_output(json_encode($data));
  }
  function load_skrining_pulang(){
		$assesmen_id=$this->input->post('assesmen_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
			
		if ($versi_edit!='0'){
		$versi_edit_last=$versi_edit-1;
		}
	  $q="SELECT H.jenis as id,M.ref as nama 
			FROM `tranap_assesmen_pulang_pengkajian` H 
			INNER JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='325'
			WHERE H.assesmen_id='$assesmen_id'
			GROUP BY H.jenis
			ORDER BY H.jenis";
	  $list_header=$this->db->query($q)->result();
	   $q="
	   SELECT *FROM (	
		  SELECT D.jml_edit as versi_edit,H.*
		  FROM `tranap_assesmen_pulang_pengkajian` H 
		  LEFT JOIN tranap_assesmen_pulang D ON D.assesmen_id=H.assesmen_id
		  WHERE H.assesmen_id='$assesmen_id'
		  
		  UNION ALL
		  
		  SELECT H.*
		  FROM `tranap_assesmen_pulang_x_pengkajian_his` H 
		  
		  WHERE H.assesmen_id='$assesmen_id'
		  ) T WHERE T.versi_edit='$versi_edit'
	  ";
	  $isi=$this->db->query($q)->result_array();
	  $q="
	   SELECT *FROM (	
		  SELECT D.jml_edit as versi_edit,H.*
		  FROM `tranap_assesmen_pulang_pengkajian` H 
		  LEFT JOIN tranap_assesmen_pulang D ON D.assesmen_id=H.assesmen_id
		  WHERE H.assesmen_id='$assesmen_id'
		  
		  UNION ALL
		  
		  SELECT H.*
		  FROM `tranap_assesmen_pulang_x_pengkajian_his` H 
		  
		  WHERE H.assesmen_id='$assesmen_id'
		  ) T WHERE T.versi_edit='$versi_edit_last'
	  ";
	  $isi_last=$this->db->query($q)->result_array();
	  
	  $q="
		  
		  SELECT * 
		  FROM `tranap_assesmen_pulang_pengkajian` H 
		  WHERE H.assesmen_id='$assesmen_id'
		  GROUP BY H.param_id
	  ";
	  $hasil=$this->db->query($q)->result();
	  $opsi='';
	  $no=0;
	  $total=0;
	  $nilai_max=0;
	  $nilai_max_nama='';
	  foreach($hasil as $r){
		  $param_id=$r->param_id;
	      $trx_id=$r->id;
		  $no=$no+1;
		  $kolom_baru='';
		  foreach ($list_header as $row){
			  $jenis_id=$row->id;
			  $kolom_baru .='
				<td class="text-center">
					'.opsi_nilai_pulang_his($isi,$param_id,$jenis_id,$isi_last).'
				</td>
			  ';
		  }
		 $opsi.='
			<tr>
				<td class="text-center">'.$no.'<input class="edukasi_id" type="hidden"  value="'.$r->id.'" ></td>
				<td>'.$r->parameter_nama.'<input class="risiko_edukasi_id" type="hidden"  value="'.$r->id.'" ></td>
				'.$kolom_baru.'
				
			</tr>';
	  }
	  $data['opsi']=$opsi;
	  echo json_encode($data);
  }
  function list_index_his_askep()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_assesmen_askep_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_assesmen_askep` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_ri/his_assesmen_aksep/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function list_index_his_keselamatan()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa,H.st_ranap
							FROM `tranap_assesmen_keselamatan_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa,H.st_ranap
							FROM `tranap_assesmen_keselamatan` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			  if ($r->st_ranap=='1'){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_ri/his_assesmen_keselamatan/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }else{
				  
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_assesmen_keselamatan_rj/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  }
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	
	function load_skrining_keselamatan(){
		$assesmen_id=$this->input->post('assesmen_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');

		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$assesmen_id=$this->input->post('assesmen_id');
		$jenis_header=$this->input->post('jenis_header');
		$q_nilai="SELECT *FROM merm_referensi WHERE ref_head_id='388' ORDER BY merm_referensi.id ASC";
		$list_nilai=$this->db->query($q_nilai)->result();
		$q="
		SELECT *FROM (
			SELECT  D.jml_edit as versi_edit,H.*
			,CASE 
				WHEN H.jenis_isi=1 THEN H.jawaban_id
				WHEN H.jenis_isi=2 THEN H.jawaban_freetext
				WHEN H.jenis_isi=3 THEN H.jawaban_ttd
			 END as jawaban


			FROM tranap_assesmen_keselamatan_detail H 
			LEFT JOIN tranap_assesmen_keselamatan D ON D.assesmen_id=H.assesmen_id
			WHERE H.assesmen_id='$assesmen_id' AND H.jenis_header='$jenis_header'
			
			UNION ALL
			
			SELECT  H.*
			,CASE 
				WHEN H.jenis_isi=1 THEN H.jawaban_id
				WHEN H.jenis_isi=2 THEN H.jawaban_freetext
				WHEN H.jenis_isi=3 THEN H.jawaban_ttd
			 END as jawaban


			FROM tranap_assesmen_keselamatan_x_his_detail H 
			
			WHERE H.assesmen_id='$assesmen_id' AND H.jenis_header='$jenis_header'
			
		) T WHERE T.versi_edit='$versi_edit_last' ORDER BY T.nourut ASC
		";
		$data_last=$this->db->query($q)->result_array();
		
		$q="
		SELECT *FROM (
			SELECT  D.jml_edit as versi_edit,H.*
			,CASE 
				WHEN H.jenis_isi=1 THEN H.jawaban_id
				WHEN H.jenis_isi=2 THEN H.jawaban_freetext
				WHEN H.jenis_isi=3 THEN H.jawaban_ttd
			 END as jawaban
			,mppa.nama as nama_ttd

			FROM tranap_assesmen_keselamatan_detail H 
			LEFT JOIN mppa ON mppa.id=H.petugas_id
			LEFT JOIN tranap_assesmen_keselamatan D ON D.assesmen_id=H.assesmen_id
			WHERE H.assesmen_id='$assesmen_id' AND H.jenis_header='$jenis_header'
			
			UNION ALL
			
			SELECT  H.*
			,CASE 
				WHEN H.jenis_isi=1 THEN H.jawaban_id
				WHEN H.jenis_isi=2 THEN H.jawaban_freetext
				WHEN H.jenis_isi=3 THEN H.jawaban_ttd
			 END as jawaban
			,mppa.nama as nama_ttd

			FROM tranap_assesmen_keselamatan_x_his_detail H 
			LEFT JOIN mppa ON mppa.id=H.petugas_id
			WHERE H.assesmen_id='$assesmen_id' AND H.jenis_header='$jenis_header'
			
		) T WHERE T.versi_edit='$versi_edit' ORDER BY T.nourut ASC
		";
		$list_data=$this->db->query($q)->result();
		$tabel='';
		foreach ($list_data as $row){
			$id=$row->id;
			$jawaban_asli=$row->jawaban_id.$row->jawaban_freetext.$row->petugas_id;
		  $data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$jawaban_last=$data_pilih['jawaban_id'].$data_pilih['jawaban_freetext'].$data_pilih['petugas_id'];
				$last_pilih=$data_pilih['id'];
			}else{
				$last_pilih='#';
				$jawaban_last='';
			}
			if ($jawaban_asli!=$jawaban_last){
				$nama_class=' bg-edited';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
		  $tabel .='<tr class="'.$nama_class.'">';
		  if ($row->header_id=='0'){
			  $tabel .='<td colspan="2"  width="85%" class="text-white bg-info h5"><input type="hidden" class="approval_id" value="'.$row->id.'">'.($row->pertanyaan).'</td>';
			  $tabel .='<td width="15%" class="text-center text-white bg-info">'.$this->opsi_nilai_hk($list_nilai,$row->group_jawaban_id,$row->jawaban,$row->jenis_isi,$row->id,$row).'</td>'; 
		  }else{
		  $tabel .='<td width="3%" class="text-right"><input type="hidden" class="approval_id" value="'.$row->id.'">'.$row->no.'</td>';
			 $tabel .='<td width="64%" >'.($row->pertanyaan).'</td>';
			$tabel .='<td width="33%" class="text-center">'.$this->opsi_nilai_hk($list_nilai,$row->group_jawaban_id,$row->jawaban,$row->jenis_isi,$row->id,$row).'</td>'; 
		  }
		  
		  $tabel .='</tr>';
		}
		$arr['tabel']=$tabel;
		$this->output->set_output(json_encode($arr));
	}
	function opsi_nilai_hk($list_nilai,$group_nilai,$jawaban_id,$jenis_isi,$id,$row){
		if ($jenis_isi=='1'){
			  $arr_group_nilai=explode(", ",$group_nilai);
											  
			  $opsi =''; 
			  foreach($list_nilai as $row){
				  if (in_array($row->id,$arr_group_nilai)){
					  if ($jawaban_id==$row->id){
					 $opsi .=$row->ref; 
						  
					  }
				  }
			  }
			  return $opsi;
		}
		if ($jenis_isi=='2'){
			  $jawaban_free_text='';
			  $jawaban_free_text='
				'.$jawaban_id.'
			  ';
			  return $jawaban_free_text;
		}
		if ($jenis_isi=='3'){
			$btn_paraf='';
			$nama_tabel="'tranap_assesmen_keselamatan_detail'";
			  if ($jawaban_id==''){
				  $btn_paraf= '<button type="button" onclick="modal_faraf('.$id.','.$nama_tabel.')"  data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i> TANDA TANGAN</button>';
			  }else{
				   $btn_paraf='<div class="img-container fx-img-rotate-r text-center ">
						<img class="" style="width:100px;height:100px; text-align: center;" src="'.$jawaban_id.'" alt="" title="">
						<div class="img-options div_edit_ttd">
							<div class="img-options-content">
								<div class="btn-group btn-group-sm">
									<a class="btn btn-default" onclick="modal_faraf('.$id.','.$nama_tabel.')"  href="javascript:void(0)"><i class="fa fa-pencil"></i></a>
									<a class="btn btn-default btn-danger" onclick="hapus_paraf('.$id.','.$nama_tabel.')" href="javascript:void(0)"><i class="fa fa-times"></i></a>
								</div>
							</div>
						</div>
					</div><div class="text-centr">('.$row->nama_ttd.')<br>'.HumanDateLong($row->date_isi).'</div>';
					
			  }
			  return $btn_paraf;
		}
	}
	function list_index_his_lap_bedah()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa,H.st_ranap
							FROM `tranap_lap_bedah_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa,H.st_ranap
							FROM `tranap_lap_bedah` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			  if ($r->st_ranap=='1'){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_ri/his_lap_bedah/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }else{
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_lap_bedah_rj/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function list_index_his_pra_sedasi()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_pra_sedasi_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_pra_sedasi` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_ri/his_pra_sedasi/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function load_premedikasi_pra_sedasi()
	{
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			  $versi_edit_last=$this->input->post('versi_edit');
				
				if ($versi_edit!='0'){
					$versi_edit_last=$versi_edit-1;
				}
				$q="
				SELECT *FROM (
					SELECT D.jml_edit as versi_edit
					,H.*,M.ref 
					FROM tranap_pra_sedasi_premedikasi H
					LEFT JOIN tranap_pra_sedasi D ON D.assesmen_id=H.assesmen_id
					LEFT JOIN merm_referensi M ON M.nilai=H.hasil AND M.ref_head_id='408'
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'	
					
									
					UNION ALL

					SELECT H.*,M.ref 
					FROM tranap_pra_sedasi_x_his_premedikasi H
					LEFT JOIN merm_referensi M ON M.nilai=H.hasil AND M.ref_head_id='408'
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'		
					
				) T WHERE T.versi_edit='$versi_edit_last'
				";
				$data_last=$this->db->query($q)->result_array();
			$q="
					
				SELECT *FROM (
					SELECT D.jml_edit as versi_edit
					,H.*,M.ref 
					FROM tranap_pra_sedasi_premedikasi H
					LEFT JOIN tranap_pra_sedasi D ON D.assesmen_id=H.assesmen_id
					LEFT JOIN merm_referensi M ON M.nilai=H.hasil AND M.ref_head_id='408'
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'	
					
									
					UNION ALL

					SELECT H.*,M.ref 
					FROM tranap_pra_sedasi_x_his_premedikasi H
					LEFT JOIN merm_referensi M ON M.nilai=H.hasil AND M.ref_head_id='408'
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'		
				) T WHERE T.versi_edit='$versi_edit'
				";
			$list=$this->db->query($q)->result();
			$tabel='';
			$no=0;
      foreach ($list as $r) {
          $result = array();

			 $aksi='';
			  $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['id'];
			}else{
				$last_pilih='#';
			}
			if ($r->id!=$last_pilih){
				$nama_class=' text-danger';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
			 $no++;
				 $aksi='';
				 $tabel .='<tr>';
          $tabel .='<td class="text-left '.$nama_class.'">'.($no).'</td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.$r->obat.'</td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.$r->dosis.'</td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.HumanDateLong2($r->jam).' </td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.$r->ref.'</td>';
          $aksi = '<div class="btn-group '.$nama_class.'">';
		  $aksi .= '</div>';
           $tabel .='<td class="text-center">'.$aksi.'</td>';
		  $tabel .= '</tr>';


      }
	  $data['tabel']=$tabel;
      $this->output->set_output(json_encode($data));
  }
  function load_medikasi_pra_sedasi()
	{
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			  $versi_edit_last=$this->input->post('versi_edit');
				
				if ($versi_edit!='0'){
					$versi_edit_last=$versi_edit-1;
				}
				$q="
				SELECT *FROM (
					SELECT D.jml_edit as versi_edit
					,H.*,M.ref 
					FROM tranap_pra_sedasi_medikasi H
					LEFT JOIN tranap_pra_sedasi D ON D.assesmen_id=H.assesmen_id
					LEFT JOIN merm_referensi M ON M.nilai=H.satuan AND M.ref_head_id='403'
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'	
					
									
					UNION ALL

					SELECT H.*,M.ref 
					FROM tranap_pra_sedasi_x_his_medikasi H
					LEFT JOIN merm_referensi M ON M.nilai=H.satuan AND M.ref_head_id='403'
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'		
					
				) T WHERE T.versi_edit='$versi_edit_last'
				";
				$data_last=$this->db->query($q)->result_array();
			$q="
					
				SELECT *FROM (
					SELECT D.jml_edit as versi_edit
					,H.*,M.ref 
					FROM tranap_pra_sedasi_medikasi H
					LEFT JOIN tranap_pra_sedasi D ON D.assesmen_id=H.assesmen_id
					LEFT JOIN merm_referensi M ON M.nilai=H.satuan AND M.ref_head_id='403'
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'	
					
									
					UNION ALL

					SELECT H.*,M.ref 
					FROM tranap_pra_sedasi_x_his_medikasi H
					LEFT JOIN merm_referensi M ON M.nilai=H.satuan AND M.ref_head_id='403'
					WHERE H.assesmen_id='$assesmen_id'
					AND H.staktif='1'		
				) T WHERE T.versi_edit='$versi_edit'
				";
			$list=$this->db->query($q)->result();
			$tabel='';
			$no=0;
      foreach ($list as $r) {
          $result = array();

			 $aksi='';
			  $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['id'];
			}else{
				$last_pilih='#';
			}
			if ($r->id!=$last_pilih){
				$nama_class=' text-danger';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
			 $no++;
				 $aksi='';
				 $tabel .='<tr>';
          $tabel .='<td class="text-left '.$nama_class.'">'.($no).'</td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.$r->obat.'</td>';
          $tabel .='<td class="text-left '.$nama_class.'">'.$r->ref.'</td>';
          $aksi = '<div class="btn-group '.$nama_class.'">';
		  $aksi .= '</div>';
           $tabel .='<td class="text-center">'.$aksi.'</td>';
		  $tabel .= '</tr>';


      }
	  $data['tabel']=$tabel;
      $this->output->set_output(json_encode($data));
  }
  function load_nrs_fisio_pra_sedasi(){
		$assesmen_id=$this->input->post('assesmen_id');
		$versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		
		  $q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tranap_pra_sedasi_scale_number H 
				LEFT JOIN tranap_pra_sedasi D ON D.assesmen_id=H.assesmen_id
				WHERE  H.assesmen_id='$assesmen_id' 
				
				UNION ALL
				
				SELECT H.* FROM tranap_pra_sedasi_x_his_scale_number H WHERE assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit_last'
				ORDER BY H.versi_edit,H.nourut
		  ";
		  
		$data_last=$this->db->query($q)->result_array();
		$q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tranap_pra_sedasi_scale_number H 
				LEFT JOIN tranap_pra_sedasi D ON D.assesmen_id=H.assesmen_id
				WHERE  H.assesmen_id='$assesmen_id' 
				
				UNION ALL
				
				SELECT H.* FROM tranap_pra_sedasi_x_his_scale_number H WHERE assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit'
				ORDER BY H.versi_edit,H.nourut
		  ";
		  
		  $row=$this->db->query($q)->result();
		  $detail='';
		  $detail .='<tr>';
		  $ket_awal='';
		  $warna_awal='';
		  $nama_tabel="'tranap_pra_sedasi_scale_number'";
		  $index_arr=0;
		  $jml_kol=1;
		  $array_ket=array();;
		  $array_kol=array();;
		  $array_warna=array();;
		  foreach($row as $r){
			  $data_id=$r->id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['pilih'];
				}else{
					$last_pilih='0';
				}
				if ($r->pilih!=$last_pilih){
					$nama_class="edited2";
				}else{
					$nama_class="";
					
				}
			  $detail .='<td class="border-full '.$nama_class.'" style="background-color:bg-gray-lighter">
						<label class="css-input css-radio css-radio-primary push-10-r">
						<input type="radio" name="nrs_opsi" '.($r->pilih?'checked':'').' onclick="set_skala_nyeri('.$r->id.','.$r->nilai.','.$nama_tabel.')"><span></span> <strong>'.$r->nilai.'</strong>
						</label>';
			  $detail .='</td>';
			  if ($ket_awal!=$r->keterangan){
				  if ($ket_awal != ''){
						$array_ket[$index_arr]=$ket_awal;
						$array_kol[$index_arr]=$jml_kol;
						$array_warna[$index_arr]=$warna_awal;
						$ket_awal=$r->keterangan;
						$warna_awal=$r->warna;
						$index_arr=$index_arr+1;;
				  }else{
					  $ket_awal=$r->keterangan;
					  $warna_awal=$r->warna;
				  }
				  $jml_kol=1;
			  }else{
				  $jml_kol=$jml_kol+1;
			  }
		  }
		  $array_ket[$index_arr]=$ket_awal;
		  $array_kol[$index_arr]=$jml_kol;
		  $array_warna[$index_arr]=$warna_awal;
		  $detail .='</tr>';
		  $detail .='<tr>';
		  foreach($row as $r){
			  $detail .='<td class="border-full"  style="background-color:'.$r->warna.'">';
			  $detail .='</td>';
		  }
		  $detail .='</tr>';
		  $detail .='<tr>';
		  foreach($array_ket as $index=>$val){
			  $detail .='<td class="border-full" colspan="'.$array_kol[$index].'" style="background-color:'.$array_warna[$index].'"><strong>'.$val.'</strong></td>';
		  }
		  $detail .='</tr>';
		  
		   $tabel='
			<table class="block-table text-center " style="width:100%">
				<tbody>
					'.$detail.'
				</tbody>
			</table>
		  ';
		  //FACE
		  $q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tranap_pra_sedasi_scale_face H 
				INNER JOIN tranap_pra_sedasi D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id' 
				UNION ALL
				SELECT H.* 
				FROM tranap_pra_sedasi_x_his_scale_face H 
				WHERE H.assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit_last'
				ORDER BY H.nourut ASC
		  ";
		  
		$data_last=$this->db->query($q)->result_array();
		  
		  $nama_tabel="'tranap_pra_sedasi_scale_face'";
		  $q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit, H.* 
				FROM tranap_pra_sedasi_scale_face H 
				INNER JOIN tranap_pra_sedasi D ON D.assesmen_id=H.assesmen_id
				WHERE H.assesmen_id='$assesmen_id' 
				UNION ALL
				SELECT H.* 
				FROM tranap_pra_sedasi_x_his_scale_face H 
				WHERE H.assesmen_id='$assesmen_id' 
				) H 
				WHERE H.versi_edit='$versi_edit'
				ORDER BY H.nourut ASC
		  ";
		  $row=$this->db->query($q)->result();
		  $detail ='';
		  $detail .='<tr>';
		   foreach($row as $r){
			  $detail .='<td >';
			  $detail .='<img src="'.site_url().'assets/upload/wbf/'.$r->gambar.'" style="width:100px;height:100px" alt="Avatar">';
			  $detail .='</td>';
		  }
		  $detail .='</tr>';
		  $detail .='<tr>';
		   foreach($row as $r){
			   $data_id=$r->id;
				$data_pilih = array_filter($data_last, function($var) use ($data_id) { 
					return ($var['id'] == $data_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['pilih'];
				}else{
					$last_pilih='0';
				}
				if ($r->pilih!=$last_pilih){
					$nama_class="edited2";
				}else{
					$nama_class="";
					
				}
			  $detail .='<td class="border-full '.$nama_class.'" >';
			  $detail .='<label class="css-input css-radio css-radio-primary push-10-r">
						<input type="radio"  '.($r->pilih?'checked':'').' onclick="set_skala_nyeri('.$r->id.','.$r->nilai.','.$nama_tabel.')" name="nrs_opsi" value="'.$r->nilai.'"><span></span> <strong>'.$r->nilai.'</strong>
						</label>';
			  $detail .='</td>';
		  }
		  $detail .='</tr>';
		  $tabel .='
			<table class="block-table text-center " style="width:100%">
				<tbody>
					'.$detail.'
				</tbody>
			</table>
		  ';
		  // print_r($array_warna);exit;
		  $this->output->set_output(json_encode($tabel));
	  }
	  function load_skrining_pra_sedasi(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $mrisiko_id=$this->input->post('mrisiko_id');
	  $versi_edit=$this->input->post('versi_edit');
		$versi_edit_last=$this->input->post('versi_edit');
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
	  $q="SELECT H.nama,H.isi_header,H.isi_footer FROM mpasca_anestesi H WHERE H.id='1'";
	  $data_all=$this->db->query($q)->row_array();
	  $q="
		SELECT T.mrisiko_id,T.jenis,T.total_skor,S.ref_nilai FROM(
		SELECT H.mrisiko_id,H.jenis, SUM(COALESCE(H.jawaban_skor,0)) as total_skor 
		FROM tranap_pra_sedasi_skrining H
		WHERE H.mrisiko_id='$mrisiko_id' AND H.assesmen_id='$assesmen_id'
		GROUP BY H.mrisiko_id,H.jenis
		) T 
		LEFT JOIN mpasca_anestesi_setting_nilai S ON S.mrisiko_id=T.mrisiko_id AND T.total_skor BETWEEN S.skor_1 AND S.skor_2
	  ";
	  $hasil_penilaian=$this->db->query($q)->result_array();
	  $q="SELECT H.param_id, H.jenis as id,M.ref as nama 
			FROM `tranap_pra_sedasi_skrining` H 
			INNER JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='407'
			WHERE H.assesmen_id='$assesmen_id' AND H.mrisiko_id='$mrisiko_id'
			GROUP BY H.jenis
			ORDER BY H.jenis";
	  $list_header=$this->db->query($q)->result();
	  
	  $q="
		SELECT *FROM (
		SELECT  D.jml_edit as versi_edit,H.*
		  FROM `tranap_pra_sedasi_skrining` H 
			LEFT JOIN tranap_pra_sedasi D ON D.assesmen_id=H.assesmen_id
		  WHERE H.assesmen_id='$assesmen_id' AND H.mrisiko_id='$mrisiko_id'
			
			UNION ALL

			SELECT  H.*
			FROM `tranap_pra_sedasi_x_his_skrining` H 
		WHERE H.assesmen_id='$assesmen_id' AND H.mrisiko_id='$mrisiko_id'
		) H 
		WHERE H.versi_edit='$versi_edit'
		ORDER BY H.versi_edit,H.id
	  ";
	  $isi=$this->db->query($q)->result_array();
	  
	  $q="
		SELECT *FROM (
		SELECT  D.jml_edit as versi_edit,H.*
		  FROM `tranap_pra_sedasi_skrining` H 
			LEFT JOIN tranap_pra_sedasi D ON D.assesmen_id=H.assesmen_id
		  WHERE H.assesmen_id='$assesmen_id' AND H.mrisiko_id='$mrisiko_id'
			
			UNION ALL

			SELECT  H.*
			FROM `tranap_pra_sedasi_x_his_skrining` H 
		WHERE H.assesmen_id='$assesmen_id' AND H.mrisiko_id='$mrisiko_id'
		) H 
		WHERE H.versi_edit='$versi_edit_last'
		ORDER BY H.versi_edit,H.id
	  ";
	  $isi_last=$this->db->query($q)->result_array();
	  
	  $q="SELECT * 
	  FROM `tranap_pra_sedasi_skrining` H 
	  WHERE H.assesmen_id='$assesmen_id' AND H.mrisiko_id='$mrisiko_id'
	  GROUP BY H.param_id";
	  $hasil=$this->db->query($q)->result();
	  $opsi='';
	  $no=0;
	  $total=0;
	  $nilai_max=0;
	  $nilai_max_nama='';
	  foreach($hasil as $r){
		  $param_id=$r->param_id;
	      $trx_id=$r->id;
		  $no=$no+1;
		  $kolom_baru='';
		  foreach ($list_header as $row){
			  $jenis_id=$row->id;
			  $kolom_baru .='
				<td class="text-center">
					'.$this->opsi_nilai_pra_sedasi($isi,$param_id,$jenis_id,$isi_last).'
				</td>
			  ';
		  }
		 $opsi.='
			<tr>
				<td class="text-center">'.$no.'</td>
				<td><strong>'.$r->parameter_nama.'</strong></td>
				'.$kolom_baru.'
			</tr>';
	  }
	  $kolom_baru='';
	  foreach ($list_header as $row){
		  $jenis_id=$row->id;
		  $data = array_filter($hasil_penilaian, function($var) use ($mrisiko_id,$jenis_id) { 
			return ($var['mrisiko_id']==$mrisiko_id && $var['jenis']==$jenis_id);
		  });	
		  $data = reset($data);
		if ($data){
			$total_skor=$data['total_skor'];
			$ref_nilai=$data['ref_nilai'];
		}else{
			$total_skor=0;
			$ref_nilai='';
		}
		  $kolom_baru .='
			<td class="text-center">
				<input class="text-center text-bold text-primary footer_'.$jenis_id.' form-control " id="footer_'.$jenis_id.'" disabled type="hidden"   value="'.$total_skor.'">
				<span class="h4 font-w700 text-primary" id="ket_skor_'.$jenis_id.'">'.$total_skor.'</span><br>
				<span class="h5 font-w600 text-muted" id="ket_'.$jenis_id.'">'.$ref_nilai.'</span>
			</td>
		  ';
	  }
	  $opsi.='
			<tr>
				<td colspan="2" class="text-center"><strong>TOTAL SKOR</strong></td>
				'.$kolom_baru.'
			</tr>';
	  $data_all['opsi']=$opsi;
	  echo json_encode($data_all);
  }
  
  function opsi_nilai_pra_sedasi($arr_isi,$param_id,$jenis_id,$isi_last){
		$data = array_filter($arr_isi, function($var) use ($param_id,$jenis_id) { 
			return ($var['param_id']==$param_id && $var['jenis']==$jenis_id);
		});	
		// print_r($arr_jadwal);exit;
		$data = reset($data);
		$data_last = array_filter($isi_last, function($var) use ($param_id,$jenis_id) { 
			return ($var['param_id']==$param_id && $var['jenis']==$jenis_id);
		});	
		// print_r($arr_jadwal);exit;
		$data_last = reset($data_last);
		if ($data){
			$group_id=explode('#',$data['id_group']);
			$group_nama=explode('#',$data['deskripsi_group']);
			$skor_group=explode('#',$data['skor_group']);
			// $group_nama=$data['group_nama'];
			$jawaban_id=$data['jawaban_id'];
			$jawaban_id_last=$data_last['jawaban_id'];
			if ($jawaban_id!=$jawaban_id_last){
				$namaclass="edited";
			}else{
				$namaclass="";
				
			}
			$jawaban_skor=$data['jawaban_skor'];
			$row_id=$data['id'];
			$isi_opsi='<option value="" '.($jawaban_id==''?'selected':'').'>-Pilih Opsi-</option>';
			foreach ($group_id as $index=>$val){
				$isi_opsi .='<option data-row_id="'.$row_id.'" data-skor="'.$skor_group[$index].'" data-jenis="'.$jenis_id.'" data-nama="'.$group_nama[$index].'" value="'.$val.'" '.($jawaban_id==$val?'selected':'').'>'.$group_nama[$index].'</option>';
			}
			// $tabel.='<td class="text-center text-primary">'.get_nama_ppa($data['created_ppa']).' |  '.HumanDateLong($data['tanggal_input']).'<br>'.$aksi.'</td>';
			 $opsi_data='
				<select class="form-control nilai '.$namaclass.'" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						'.$isi_opsi.'		
								
				</select>
				<input class="text-center detail_'.$param_id.' form-control nilai_skor'.$row_id.' '.$namaclass.'"  disabled type="text"   value="'.$jawaban_skor.'">
			  ';
		}else{
			$opsi_data='';
		}
		return $opsi_data;
	 
  }
  function list_index_his_ringkasan_pulang()
  {
		$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tranap_ringkasan_pulang_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tranap_ringkasan_pulang` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			 $aksi='<a class="btn btn-default menu_click btn-xs" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_ringkasan_pulang/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function load_diagnosa_ringkasan_pulang()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$versi_edit_last=$this->input->post('versi_edit');
		
			if ($versi_edit!='0'){
				$versi_edit_last=$versi_edit-1;
			}
			$q="
				SELECT  MP.nama as nama_user,MD.nama as diagnosa, H.*  
				FROM (
					SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mdiagnosa_id,D.created_by,D.created_date,D.prioritas FROM tranap_ringkasan_pulang H
					LEFT JOIN tranap_ringkasan_pulang_diagnosa D ON D.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

					UNION ALL

					SELECT H.assesmen_id,H.versi_edit,H.id,H.mdiagnosa_id,H.created_by,H.created_date,H.prioritas FROM tranap_ringkasan_pulang_x_his_diagnosa H
					WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
					) H
				LEFT JOIN mppa MP ON MP.id=H.created_by
				LEFT JOIN mdiagnosa_medis MD ON MD.id=H.mdiagnosa_id 
				WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit_last'
				ORDER BY H.prioritas ASC
			";
			$data_last=$this->db->query($q)->result_array();
			$this->select = array();
			$from="
					(
						SELECT  MP.nama as nama_user,MD.nama as diagnosa, H.*  
						FROM (
							SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mdiagnosa_id,D.created_by,D.created_date,D.prioritas FROM tranap_ringkasan_pulang H
							LEFT JOIN tranap_ringkasan_pulang_diagnosa D ON D.assesmen_id=H.assesmen_id
							WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

							UNION ALL

							SELECT H.assesmen_id,H.versi_edit,H.id,H.mdiagnosa_id,H.created_by,H.created_date,H.prioritas FROM tranap_ringkasan_pulang_x_his_diagnosa H
							WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
							) H
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN mdiagnosa_medis MD ON MD.id=H.mdiagnosa_id 
						WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit'
						ORDER BY H.prioritas ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		  $mdiagnosa_id=$r->mdiagnosa_id;
			$data_pilih = array_filter($data_last, function($var) use ($mdiagnosa_id) { 
				return ($var['mdiagnosa_id'] == $mdiagnosa_id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['prioritas'];
			}else{
				$last_pilih='#';
			}
			if ($r->prioritas!=$last_pilih){
				$nama_class=' &nbsp;&nbsp;'.text_danger('Changed');
			}else{
				$nama_class="";
				
			}
			
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->diagnosa).$nama_class;
          $result[] = $r->prioritas;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  // $aksi .= '<button onclick="edit_diagnosa('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  // $aksi .= '<button onclick="hapus_diagnosa('.$r->id.')" type="button" title="Hapus Diagnosa" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '<button onclick="set_rencana_asuhan('.$r->id.')" type="button" title="Rencana Asuhan" class="btn btn-success btn-xs btn_rencana"><i class="si si-book-open"></i></button>';	
		  // }
		  $aksi .= '</div>';
          // $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_tindakan_ringkasan_pulang()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$versi_edit_last=$this->input->post('versi_edit');
		
			if ($versi_edit!='0'){
				$versi_edit_last=$versi_edit-1;
			}
			$q="
				SELECT  MP.nama as nama_user,MD.nama as tindakan, H.*  
				FROM (
					SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mtindakan_id,D.created_by,D.created_date,D.prioritas FROM tranap_ringkasan_pulang H
					LEFT JOIN tranap_ringkasan_pulang_tindakan D ON D.assesmen_id=H.assesmen_id
					WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

					UNION ALL

					SELECT H.assesmen_id,H.versi_edit,H.id,H.mtindakan_id,H.created_by,H.created_date,H.prioritas FROM tranap_ringkasan_pulang_x_his_tindakan H
					WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
					) H
				LEFT JOIN mppa MP ON MP.id=H.created_by
				LEFT JOIN mtindakan_medis MD ON MD.id=H.mtindakan_id 
				WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit_last'
				ORDER BY H.prioritas ASC
			";
			$data_last=$this->db->query($q)->result_array();
			$this->select = array();
			$from="
					(
						SELECT  MP.nama as nama_user,MD.nama as tindakan, H.*  
						FROM (
							SELECT D.assesmen_id,H.jml_edit as versi_edit,D.id,D.mtindakan_id,D.created_by,D.created_date,D.prioritas FROM tranap_ringkasan_pulang H
							LEFT JOIN tranap_ringkasan_pulang_tindakan D ON D.assesmen_id=H.assesmen_id
							WHERE H.assesmen_id='$assesmen_id' AND  D.staktif='1'

							UNION ALL

							SELECT H.assesmen_id,H.versi_edit,H.id,H.mtindakan_id,H.created_by,H.created_date,H.prioritas FROM tranap_ringkasan_pulang_x_his_tindakan H
							WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
							) H
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN mtindakan_medis MD ON MD.id=H.mtindakan_id 
						WHERE H.assesmen_id='$assesmen_id' AND H.versi_edit='$versi_edit'
						ORDER BY H.prioritas ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','tindakan','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		  $mtindakan_id=$r->mtindakan_id;
			$data_pilih = array_filter($data_last, function($var) use ($mtindakan_id) { 
				return ($var['mtindakan_id'] == $mtindakan_id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['prioritas'];
			}else{
				$last_pilih='#';
			}
			if ($r->prioritas!=$last_pilih){
				$nama_class=' &nbsp;&nbsp;'.text_danger('Changed');
			}else{
				$nama_class="";
				
			}
			
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->tindakan).$nama_class;
          $result[] = $r->prioritas;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  // $aksi .= '<button onclick="edit_tindakan('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  // $aksi .= '<button onclick="hapus_tindakan('.$r->id.')" type="button" title="Hapus Diagnosa" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '<button onclick="set_rencana_asuhan('.$r->id.')" type="button" title="Rencana Asuhan" class="btn btn-success btn-xs btn_rencana"><i class="si si-book-open"></i></button>';	
		  // }
		  $aksi .= '</div>';
          // $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function list_index_his_pra_bedah()
  {
		$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa,H.st_ranap,H.pendaftaran_id
							FROM `tranap_pra_bedah_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa,H.st_ranap,H.pendaftaran_id
							FROM `tranap_pra_bedah` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			  if ($r->st_ranap=='1'){
			 $aksi='<a class="btn btn-default menu_click btn-xs" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_pra_bedah/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }else{
				  
			 $aksi='<a class="btn btn-default menu_click btn-xs" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_pra_bedah_rj/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  }
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function list_index_his_survey()
  {
		$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,H.pendaftaran_id,H.st_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa
							FROM `tpoliklinik_survey_kepuasan_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,H.pendaftaran_id,H.st_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa
							FROM `tpoliklinik_survey_kepuasan` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			if ($r->st_ranap=='1'){
			 $aksi='<a class="btn btn-default menu_click btn-xs" href="'.site_url().'tsurvey/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_survey/his_survey/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			}else{
			 $aksi='<a class="btn btn-default menu_click btn-xs" href="'.site_url().'tsurvey/tindakan/'.$r->pendaftaran_id.'/0/erm_survey/his_survey/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			}
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function load_nilai_param_survey_kepuasan(){
		 
		 $assesmen_id=$this->input->post('assesmen_id');
		 $versi_edit=$this->input->post('versi_edit');
			$versi_edit_last=$this->input->post('versi_edit');
		
			if ($versi_edit!='0'){
				$versi_edit_last=$versi_edit-1;
			}
			$q="
			SELECT *FROM (
			SELECT H.jml_edit as versi_edit,D.* FROM `tpoliklinik_survey_kepuasan` H
			LEFT JOIN tpoliklinik_survey_kepuasan_param D ON D.assesmen_id=H.assesmen_id
			WHERE H.assesmen_id='$assesmen_id'
			UNION ALL

			SELECT * FROM tpoliklinik_survey_kepuasan_param_his D
			WHERE D.assesmen_id='$assesmen_id'
			) H WHERE H.versi_edit='$versi_edit_last'";
		$data_last=$this->db->query($q)->result_array();
		
		  $q="
			SELECT *FROM (
			SELECT H.jml_edit as versi_edit,D.* FROM `tpoliklinik_survey_kepuasan` H
			LEFT JOIN tpoliklinik_survey_kepuasan_param D ON D.assesmen_id=H.assesmen_id
			WHERE H.assesmen_id='$assesmen_id'
			UNION ALL

			SELECT * FROM tpoliklinik_survey_kepuasan_param_his D
			WHERE D.assesmen_id='$assesmen_id'
			) H WHERE H.versi_edit='$versi_edit'";
		  $list_data=$this->db->query($q)->result();
		  $tabel='';
		  $nourut=1;
		  $total_skor=0;
		  foreach ($list_data as $row){
			  $warna='#fff';
			    $jawaban_id=$row->jawaban_id;
			    $param_id=$row->param_id;
				$data_pilih = array_filter($data_last, function($var) use ($param_id,$jawaban_id) { 
					return ($var['param_id'] == $param_id && $var['jawaban_id'] == $jawaban_id);
				  });
				$data_pilih=reset($data_pilih);
				if ($data_pilih){
					$last_pilih=$data_pilih['jawaban_id'];
				}else{
					$last_pilih='';
				}
				if ($row->jawaban_id!=$last_pilih){
					$nama_class=' edited';
					$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
				}else{
					$nama_class='';
					$aksi="";
					
				}
				$tabel .='<tr style="background-color:'.$warna.'">';
				$tabel .='<td><input type="hidden" class="parameter_id" value="'.$row->id.'">'.$nourut.'</td>';
				$tabel .='<td class="text-bold"><strong>'.$row->parameter_nama.$aksi.'</strong></td>';
				$tabel .='<td >'.$this->opsi_nilai_survey_kepuasan($row->id,$row->jawaban_id,$nama_class).'</td>';
				$tabel .='<td class="text-bold" hidden><input readonly type="text" class="form-control parameter_skor" value="'.$row->jawaban_skor.'"></td>';
				$nourut=$nourut+1;
				$total_skor=$total_skor+$row->jawaban_skor;
			  $tabel .='</tr>';
		  }
		  $tabel.='<tr><td colspan="2" class="text-primary text-right"><strong>TOTAL PENILAIAN</strong></td><td><input readonly type="text" class="form-control total_skor" value="'.$total_skor.'"></td></tr>';
		  
		  $arr['tabel']=$tabel;
		  $arr['total_skor']=$total_skor;
		  $this->output->set_output(json_encode($arr));
	  }
	  function opsi_nilai_survey_kepuasan($parameter_id,$master_id,$nama_class){
		  $q="SELECT *FROM tpoliklinik_survey_kepuasan_param_skor WHERE parameter_id='$parameter_id' ORDER BY id ASC";
		  $list_nilai=$this->db->query($q)->result();
		  $opsi='<select class="'.$nama_class.' form-control nilai_survey_kepuasan"  style="width: 100%;" data-placeholder="Belum Mengisi">';
		  $opsi .='<option value="" '.($master_id==''?'selected':'').'>- Pilih Jawaban -</option>'; 
		  foreach($list_nilai as $row){
				 $opsi .='<option value="'.$row->master_id.'" data-skor="'.$row->skor.'" '.($master_id==$row->master_id?'selected':'').'>'.$row->deskripsi_nama.'</option>'; 
		  }
		  $opsi .="</select>";
		  return $opsi;
	  }
	  function list_index_his_pews()//Risiko Jatuh
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa,H.pendaftaran_id,H.st_ranap
							FROM `tranap_pews_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id_ranap,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa,H.pendaftaran_id,H.st_ranap
							FROM `tranap_pews` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			  if ($r->st_ranap=='1'){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tews/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_ews/his_pews/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }else{
				  
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tews/tindakan/'.$r->pendaftaran_id.'/0/erm_ews/his_pews/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			  }
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function load_pews(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $versi_edit=$this->input->post('versi_edit');
	  $versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
			SELECT *FROM (
				SELECT D.id,D.versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai,M.deskripsi_nama,D.param_id    
				FROM `tranap_pews_x_his_detail` D
				LEFT JOIN mpews_param_skor M ON M.id=D.param_nilai_id
				WHERE D.assesmen_id='$assesmen_id'
				UNION ALL
				SELECT D.id,H.jml_edit as versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai,M.deskripsi_nama,D.param_id      
				FROM tranap_pews H
				LEFT JOIN `tranap_pews_detail` D ON D.assesmen_id=H.assesmen_id
				LEFT JOIN mpews_param_skor M ON M.id=D.param_nilai_id
				WHERE H.assesmen_id='$assesmen_id'
			) H 
			WHERE H.versi_edit='$versi_edit_last'
			ORDER BY H.id
		";
		// print_r($q);exit;
		$data_last=$this->db->query($q)->result_array();
	  // $q="SELECT * FROM `tpoliklinik_assesmen_pews` H WHERE H.assesmen_id='$assesmen_id'";
	 $q="
			SELECT *FROM (
				SELECT D.id,D.versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai,M.deskripsi_nama,D.param_id  
				FROM `tranap_pews_x_his_detail` D
				LEFT JOIN mpews_param_skor M ON M.id=D.param_nilai_id
				WHERE D.assesmen_id='$assesmen_id'
				UNION ALL
				SELECT D.id,H.jml_edit as versi_edit,D.assesmen_id,D.parameter_nama,D.group_nilai,D.param_nilai_id,D.nilai,M.deskripsi_nama,D.param_id    
				FROM tranap_pews H
				LEFT JOIN `tranap_pews_detail` D ON D.assesmen_id=H.assesmen_id
				LEFT JOIN mpews_param_skor M ON M.id=D.param_nilai_id
				WHERE H.assesmen_id='$assesmen_id'
			) H 
			WHERE H.versi_edit='$versi_edit'
			ORDER BY H.id
		";
	  $hasil=$this->db->query($q)->result();
	  $opsi='';
	  $no=0;
	  foreach($hasil as $r){
		  $no=$no+1;
		  $param_id=$r->param_id;
			$data_pilih = array_filter($data_last, function($var) use ($param_id) { 
				return ($var['param_id'] == $param_id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['param_nilai_id'];
			}else{
				$last_pilih='#';
			}
			$nama_class="";
			if ($last_pilih !='#'){
				if ($r->param_nilai_id!=$last_pilih){
					$nama_class="text-danger";
				}else{
					$nama_class="";
					
				}
			}
			
		 $opsi.='<tr>
					<td>'.$r->parameter_nama.' &nbsp;&nbsp;&nbsp;&nbsp;'.($nama_class!=''?text_danger('PERUBAHAN'):'').'<input class="risiko_nilai_id" type="hidden"  value="'.$r->id.'" ></td>
					<td class="'.$nama_class.'"><strong>'.$r->deskripsi_nama.'</strong></td>
					<td class="'.$nama_class.' text-center"><strong>'.$r->nilai.'</strong></td>
					
				</tr>';
	  }
	  $data['opsi']=$opsi;
	  echo json_encode($data);
  }
   function list_index_his_info_ods()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa,H.st_ranap
							FROM `tranap_info_ods_x_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa,H.st_ranap
							FROM `tranap_info_ods` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			if ($r->st_ranap=='1'){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_per/his_info_ods/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			}else{
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_per/his_info_ods_rj/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
			}
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = $r->user_edited.'<br>'.HumanDateLong($r->edited_date);
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	// function load_dok_info_ods()
	// {
			// $data_user=get_acces();
			// $user_acces_form=$data_user['user_acces_form'];
			// $assesmen_id=$this->input->post('assesmen_id');
			// $status_assemen=$this->input->post('status_assemen');
			// $this->select = array();
			// $from="
					// (
						// SELECT H.* 
						// ,J.ref as nama_dok,S.ref as st_dok
						// FROM tranap_info_ods_dok H
						// LEFT JOIN merm_referensi J ON J.nilai=H.nama_dok_id AND J.ref_head_id='430'
						// LEFT JOIN merm_referensi S ON S.nilai=H.status_dok AND S.ref_head_id='429'
						// WHERE H.assesmen_id='$assesmen_id' AND H.st_aktif='1'
					// ) as tbl
				// ";
			// // print_r($from);exit();
			// $this->from   = $from;
			// $this->join 	= array();
			
			
			// $this->order  = array();
			// $this->group  = array();
			// $this->column_search = array('nama_dok','st_dok');
			// $this->column_order  = array();

      // $list = $this->datatable->get_datatables(true);
      // $data = array();
      // $no = $_POST['start'];
      // foreach ($list as $r) {
          // $no++;
          // $result = array();

          // $result[] = $no;
	      // $aksi='';
          // $result[] = ($r->nama_dok);
          // $result[] = $r->st_dok;
          // $result[] = $r->ket_dok;
         

          // $data[] = $result;
      // }
      // $output = array(
	      // "draw" => $_POST['draw'],
	      // "recordsTotal" => $this->datatable->count_all(true),
	      // "recordsFiltered" => $this->datatable->count_all(true),
	      // "data" => $data
      // );
      // echo json_encode($output);
  // }
  function load_dok_info_ods()
	{
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			  $versi_edit_last=$this->input->post('versi_edit');
				
				if ($versi_edit!='0'){
					$versi_edit_last=$versi_edit-1;
				}
				$q="
				SELECT *FROM (
				SELECT D.jml_edit as versi_edit,H.* 
				,J.ref as nama_dok,S.ref as st_dok
				FROM tranap_info_ods_dok H
				LEFT JOIN tranap_info_ods D ON D.assesmen_id=H.assesmen_id
				LEFT JOIN merm_referensi J ON J.nilai=H.nama_dok_id AND J.ref_head_id='430'
				LEFT JOIN merm_referensi S ON S.nilai=H.status_dok AND S.ref_head_id='429'
				WHERE H.assesmen_id='1' AND H.st_aktif='1'

				UNION ALL

				SELECT H.* 
				,J.ref as nama_dok,S.ref as st_dok
				FROM tranap_info_ods_x_his_dok H

				LEFT JOIN merm_referensi J ON J.nilai=H.nama_dok_id AND J.ref_head_id='430'
				LEFT JOIN merm_referensi S ON S.nilai=H.status_dok AND S.ref_head_id='429'
				WHERE H.assesmen_id='1' AND H.st_aktif='1'
				) H WHERE H.versi_edit='$versi_edit_last'
				ORDER BY H.versi_edit,H.id
				";
				$data_last=$this->db->query($q)->result_array();
					$q="
			SELECT *FROM (
				SELECT D.jml_edit as versi_edit,H.* 
				,J.ref as nama_dok,S.ref as st_dok
				FROM tranap_info_ods_dok H
				LEFT JOIN tranap_info_ods D ON D.assesmen_id=H.assesmen_id
				LEFT JOIN merm_referensi J ON J.nilai=H.nama_dok_id AND J.ref_head_id='430'
				LEFT JOIN merm_referensi S ON S.nilai=H.status_dok AND S.ref_head_id='429'
				WHERE H.assesmen_id='1' AND H.st_aktif='1'

				UNION ALL

				SELECT H.* 
				,J.ref as nama_dok,S.ref as st_dok
				FROM tranap_info_ods_x_his_dok H

				LEFT JOIN merm_referensi J ON J.nilai=H.nama_dok_id AND J.ref_head_id='430'
				LEFT JOIN merm_referensi S ON S.nilai=H.status_dok AND S.ref_head_id='429'
				WHERE H.assesmen_id='1' AND H.st_aktif='1'
				) H WHERE H.versi_edit='$versi_edit'
				ORDER BY H.versi_edit,H.id
				";
			$list=$this->db->query($q)->result();
			$tabel='';
			$no=0;
      foreach ($list as $r) {
          $result = array();

			 $aksi='';
			  $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['id'];
			}else{
				$last_pilih='#';
			}
			if ($r->id!=$last_pilih){
				$nama_class=' text-danger';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
			 $no++;
				 $aksi='';
				 $tabel .='<tr>';
			  $tabel .='<td class="text-left '.$nama_class.'">'.($no).'</td>';
			  $tabel .='<td class="text-left '.$nama_class.'">'.$r->nama_dok.'</td>';
			  $tabel .='<td class="text-left '.$nama_class.'">'.$r->st_dok.'</td>';
			  $tabel .='<td class="text-left '.$nama_class.'">'.$r->ket_dok.'</td>';
			  
			  $tabel .= '</tr>';


      }
	  $data['tabel']=$tabel;
      $this->output->set_output(json_encode($data));
  }
  //TF DARAH
  function list_index_his_tf_darah()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$tipe_rj_ri=$this->input->post('tipe_rj_ri');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa,H.pendaftaran_id_ranap
							FROM `tranap_tf_darah_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa,H.pendaftaran_id_ranap
							FROM `tranap_tf_darah` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			if ($tipe_rj_ri=='1'){
				$aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_per/his_tf_darah_rj/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				
			}else{
				$aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_per/his_tf_darah/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				
			}
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = ($r->user_edited?$r->user_edited.'<br>'.HumanDateLong($r->edited_date):'');
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	function load_index_informasi_tf_darah(){
	  $status_assemen=$this->input->post('status_assemen');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $ttd_dokter_pelaksana=$this->input->post('ttd_dokter_pelaksana');
	  $ttd_penerima_info=$this->input->post('ttd_penerima_info');
	  $versi_edit=$this->input->post('versi_edit');
	  $versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
		SELECT *FROM (
				select H.jml_edit as versi_edit,D.*
				FROM tranap_tf_darah_informasi D
				INNER JOIN tranap_tf_darah H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'

				UNION ALL

				select D.*
				FROM tranap_tf_darah_his_informasi D
				WHERE D.assesmen_id='$assesmen_id'

				) T WHERE T.versi_edit='$versi_edit_last'
		";
		$data_last=$this->db->query($q)->result_array();
		
	  $q=" SELECT 
		ttd_dokter_ina,ttd_dokter_eng,ket_ttd_dokter_ina,ket_ttd_dokter_eng,ttd_pasien_ina,ttd_pasien_eng,ket_ttd_pasien_ina,ket_ttd_pasien_eng FROM setting_ic_keterangan

	  ";
	  $row_ttd=$this->db->query($q)->row_array();
	   $q="
		SELECT *FROM (
				select H.jml_edit  as versi_edit,D.*
				FROM tranap_tf_darah_informasi D
				INNER JOIN tranap_tf_darah H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'

				UNION ALL

				select D.*
				FROM tranap_tf_darah_his_informasi D
				WHERE D.assesmen_id='$assesmen_id'

				) T WHERE T.versi_edit='$versi_edit'
		";
	  $list_info=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_info as $r){
		   $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['isi_informasi'];
			}else{
				$last_pilih='#';
			}
			if ($r->isi_informasi!=$last_pilih){
				$nama_class=' edited';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
			
		  $btn_paraf='';
		  if ($r->paraf){
		  $btn_paraf='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$r->paraf.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_faraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_paraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
		  }else{
			  $btn_paraf .= '<button onclick="modal_faraf('.$r->id.')"  type="button" data-toggle="tooltip" title="Paraf" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i></button>';
		  }
		  
		  $tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="informasi_id" value="'.$r->id.'">'.$r->nourut.'</td>';
			$tabel .='<td class="text-center"><strong>'.$r->nama.'</strong><br><i>'.$r->nama_english.'</i></td>';
			$tabel .='<td>'.$r->isi_informasi.'</td>';
			$tabel .='<td class="text-center">'.$btn_paraf.'</td>';
		  $tabel .='</tr>';
	  } 
	  $btn_ttd_dokter='';
	  if ($ttd_dokter_pelaksana){
		  $btn_ttd_dokter='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_dokter_pelaksana.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
	  }else{
		  $btn_ttd_dokter='<button class="btn btn-sm btn-success" onclick="modal_ttd_dokter()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $btn_ttd_kel='';
	  if ($ttd_penerima_info){
		  $btn_ttd_kel='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_penerima_info.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_kel()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_kel()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
	  }else{
		  $btn_ttd_kel='<button class="btn btn-sm btn-success" onclick="modal_ttd_kel()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_dokter_ina'].'<br><i>'.$row_ttd['ket_ttd_dokter_eng'].'</i></td>';
	  $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_dokter_ina'].'/'.$row_ttd['ttd_dokter_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_dokter.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_pasien_ina'].'<br><i>'.$row_ttd['ket_ttd_pasien_eng'].'</i></td>';
	   $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_pasien_ina'].'/'.$row_ttd['ttd_pasien_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_kel.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	 function load_index_informasi_icu(){
	  $status_assemen=$this->input->post('status_assemen');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $ttd_dokter_pelaksana=$this->input->post('ttd_dokter_pelaksana');
	  $ttd_penerima_info=$this->input->post('ttd_penerima_info');
	  $versi_edit=$this->input->post('versi_edit');
	  $versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
		SELECT *FROM (
				select H.jml_edit as versi_edit,D.*
				FROM tpoliklinik_icu_informasi D
				INNER JOIN tpoliklinik_icu H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'

				UNION ALL

				select D.*
				FROM tpoliklinik_icu_his_informasi D
				WHERE D.assesmen_id='$assesmen_id'

				) T WHERE T.versi_edit='$versi_edit_last'
		";
		$data_last=$this->db->query($q)->result_array();
		
	  $q=" SELECT 
		ttd_dokter_ina,ttd_dokter_eng,ket_ttd_dokter_ina,ket_ttd_dokter_eng,ttd_pasien_ina,ttd_pasien_eng,ket_ttd_pasien_ina,ket_ttd_pasien_eng FROM setting_icu_keterangan

	  ";
	  $row_ttd=$this->db->query($q)->row_array();
	   $q="
		SELECT *FROM (
				select H.jml_edit  as versi_edit,D.*
				FROM tpoliklinik_icu_informasi D
				INNER JOIN tpoliklinik_icu H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'

				UNION ALL

				select D.*
				FROM tpoliklinik_icu_his_informasi D
				WHERE D.assesmen_id='$assesmen_id'

				) T WHERE T.versi_edit='$versi_edit'
		";
	  $list_info=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_info as $r){
		   $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['isi_informasi'];
			}else{
				$last_pilih='#';
			}
			if ($r->isi_informasi!=$last_pilih){
				$nama_class=' edited';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
			
		  $btn_paraf='';
		  if ($r->paraf){
		  $btn_paraf='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$r->paraf.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_faraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_paraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
		  }else{
			  $btn_paraf .= '<button onclick="modal_faraf('.$r->id.')"  type="button" data-toggle="tooltip" title="Paraf" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i></button>';
		  }
		  
		  $tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="informasi_id" value="'.$r->id.'">'.$r->nourut.'</td>';
			$tabel .='<td class="text-center"><strong>'.$r->nama.'</strong><br><i>'.$r->nama_english.'</i></td>';
			$tabel .='<td>'.$r->isi_informasi.'</td>';
			$tabel .='<td class="text-center">'.$btn_paraf.'</td>';
		  $tabel .='</tr>';
	  } 
	  $btn_ttd_dokter='';
	  if ($ttd_dokter_pelaksana){
		  $btn_ttd_dokter='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_dokter_pelaksana.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
	  }else{
		  $btn_ttd_dokter='<button class="btn btn-sm btn-success" onclick="modal_ttd_dokter()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $btn_ttd_kel='';
	  if ($ttd_penerima_info){
		  $btn_ttd_kel='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_penerima_info.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_kel()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_kel()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
	  }else{
		  $btn_ttd_kel='<button class="btn btn-sm btn-success" onclick="modal_ttd_kel()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_dokter_ina'].'<br><i>'.$row_ttd['ket_ttd_dokter_eng'].'</i></td>';
	  $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_dokter_ina'].'/'.$row_ttd['ttd_dokter_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_dokter.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_pasien_ina'].'<br><i>'.$row_ttd['ket_ttd_pasien_eng'].'</i></td>';
	   $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_pasien_ina'].'/'.$row_ttd['ttd_pasien_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_kel.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function list_index_his_icu()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa,H.st_ranap
							FROM `tpoliklinik_icu_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa,H.st_ranap
							FROM `tpoliklinik_icu` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			  if ($r->st_ranap=='1'){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_ri/his_icu_ri/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }else{
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_icu/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = ($r->user_edited?$r->user_edited.'<br>'.HumanDateLong($r->edited_date):'');
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	
	//ISOLASI
	function load_index_informasi_isolasi(){
	  $status_assemen=$this->input->post('status_assemen');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $ttd_dokter_pelaksana=$this->input->post('ttd_dokter_pelaksana');
	  $ttd_penerima_info=$this->input->post('ttd_penerima_info');
	  $versi_edit=$this->input->post('versi_edit');
	  $versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
		SELECT *FROM (
				select H.jml_edit as versi_edit,D.*
				FROM tpoliklinik_isolasi_informasi D
				INNER JOIN tpoliklinik_isolasi H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'

				UNION ALL

				select D.*
				FROM tpoliklinik_isolasi_his_informasi D
				WHERE D.assesmen_id='$assesmen_id'

				) T WHERE T.versi_edit='$versi_edit_last'
		";
		$data_last=$this->db->query($q)->result_array();
		
	  $q=" SELECT 
		ttd_dokter_ina,ttd_dokter_eng,ket_ttd_dokter_ina,ket_ttd_dokter_eng,ttd_pasien_ina,ttd_pasien_eng,ket_ttd_pasien_ina,ket_ttd_pasien_eng FROM setting_isolasi_keterangan

	  ";
	  $row_ttd=$this->db->query($q)->row_array();
	   $q="
		SELECT *FROM (
				select H.jml_edit  as versi_edit,D.*
				FROM tpoliklinik_isolasi_informasi D
				INNER JOIN tpoliklinik_isolasi H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'

				UNION ALL

				select D.*
				FROM tpoliklinik_isolasi_his_informasi D
				WHERE D.assesmen_id='$assesmen_id'

				) T WHERE T.versi_edit='$versi_edit'
		";
	  $list_info=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_info as $r){
		   $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['isi_informasi'];
			}else{
				$last_pilih='#';
			}
			if ($r->isi_informasi!=$last_pilih){
				$nama_class=' edited';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
			
		  $btn_paraf='';
		  if ($r->paraf){
		  $btn_paraf='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$r->paraf.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_faraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_paraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
		  }else{
			  $btn_paraf .= '<button onclick="modal_faraf('.$r->id.')"  type="button" data-toggle="tooltip" title="Paraf" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i></button>';
		  }
		  
		  $tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="informasi_id" value="'.$r->id.'">'.$r->nourut.'</td>';
			$tabel .='<td class="text-center"><strong>'.$r->nama.'</strong><br><i>'.$r->nama_english.'</i></td>';
			$tabel .='<td>'.$r->isi_informasi.'</td>';
			$tabel .='<td class="text-center">'.$btn_paraf.'</td>';
		  $tabel .='</tr>';
	  } 
	  $btn_ttd_dokter='';
	  if ($ttd_dokter_pelaksana){
		  $btn_ttd_dokter='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_dokter_pelaksana.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
	  }else{
		  $btn_ttd_dokter='<button class="btn btn-sm btn-success" onclick="modal_ttd_dokter()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $btn_ttd_kel='';
	  if ($ttd_penerima_info){
		  $btn_ttd_kel='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_penerima_info.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_kel()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_kel()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
	  }else{
		  $btn_ttd_kel='<button class="btn btn-sm btn-success" onclick="modal_ttd_kel()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_dokter_ina'].'<br><i>'.$row_ttd['ket_ttd_dokter_eng'].'</i></td>';
	  $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_dokter_ina'].'/'.$row_ttd['ttd_dokter_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_dokter.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_pasien_ina'].'<br><i>'.$row_ttd['ket_ttd_pasien_eng'].'</i></td>';
	   $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_pasien_ina'].'/'.$row_ttd['ttd_pasien_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_kel.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function list_index_his_isolasi()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa,H.st_ranap
							FROM `tpoliklinik_isolasi_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa,H.st_ranap
							FROM `tpoliklinik_isolasi` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			  if ($r->st_ranap=='1'){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_ri/his_isolasi_ri/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }else{
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_isolasi/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = ($r->user_edited?$r->user_edited.'<br>'.HumanDateLong($r->edited_date):'');
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
	
	//ISOLASI
	function load_index_informasi_dnr(){
	  $status_assemen=$this->input->post('status_assemen');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $ttd_dokter_pelaksana=$this->input->post('ttd_dokter_pelaksana');
	  $ttd_penerima_info=$this->input->post('ttd_penerima_info');
	  $versi_edit=$this->input->post('versi_edit');
	  $versi_edit_last=$this->input->post('versi_edit');
		
		if ($versi_edit!='0'){
			$versi_edit_last=$versi_edit-1;
		}
		$q="
		SELECT *FROM (
				select H.jml_edit as versi_edit,D.*
				FROM tpoliklinik_dnr_informasi D
				INNER JOIN tpoliklinik_dnr H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'

				UNION ALL

				select D.*
				FROM tpoliklinik_dnr_his_informasi D
				WHERE D.assesmen_id='$assesmen_id'

				) T WHERE T.versi_edit='$versi_edit_last'
		";
		$data_last=$this->db->query($q)->result_array();
		
	  $q=" SELECT 
		ttd_dokter_ina,ttd_dokter_eng,ket_ttd_dokter_ina,ket_ttd_dokter_eng,ttd_pasien_ina,ttd_pasien_eng,ket_ttd_pasien_ina,ket_ttd_pasien_eng FROM setting_dnr_keterangan

	  ";
	  $row_ttd=$this->db->query($q)->row_array();
	   $q="
		SELECT *FROM (
				select H.jml_edit  as versi_edit,D.*
				FROM tpoliklinik_dnr_informasi D
				INNER JOIN tpoliklinik_dnr H ON H.assesmen_id=D.assesmen_id
				WHERE D.assesmen_id='$assesmen_id'

				UNION ALL

				select D.*
				FROM tpoliklinik_dnr_his_informasi D
				WHERE D.assesmen_id='$assesmen_id'

				) T WHERE T.versi_edit='$versi_edit'
		";
	  $list_info=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_info as $r){
		   $id=$r->id;
			$data_pilih = array_filter($data_last, function($var) use ($id) { 
				return ($var['id'] == $id);
			  });
			$data_pilih=reset($data_pilih);
			if ($data_pilih){
				$last_pilih=$data_pilih['isi_informasi'];
			}else{
				$last_pilih='#';
			}
			if ($r->isi_informasi!=$last_pilih){
				$nama_class=' edited';
				$aksi=' &nbsp;&nbsp;'.text_danger('Edited');
			}else{
				$nama_class='';
				$aksi="";
				
			}
			
		  $btn_paraf='';
		  if ($r->paraf){
		  $btn_paraf='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$r->paraf.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_faraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_paraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
		  }else{
			  $btn_paraf .= '<button onclick="modal_faraf('.$r->id.')"  type="button" data-toggle="tooltip" title="Paraf" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i></button>';
		  }
		  
		  $tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="informasi_id" value="'.$r->id.'">'.$r->nourut.'</td>';
			$tabel .='<td class="text-center"><strong>'.$r->nama.'</strong><br><i>'.$r->nama_english.'</i></td>';
			$tabel .='<td>'.$r->isi_informasi.'</td>';
			$tabel .='<td class="text-center">'.$btn_paraf.'</td>';
		  $tabel .='</tr>';
	  } 
	  $btn_ttd_dokter='';
	  if ($ttd_dokter_pelaksana){
		  $btn_ttd_dokter='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_dokter_pelaksana.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
	  }else{
		  $btn_ttd_dokter='<button class="btn btn-sm btn-success" onclick="modal_ttd_dokter()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $btn_ttd_kel='';
	  if ($ttd_penerima_info){
		  $btn_ttd_kel='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_penerima_info.'" alt="">
				'.($status_assemen!='2'?'
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_kel()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_kel()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
				':'').'
			</div>
		  ';
	  }else{
		  $btn_ttd_kel='<button class="btn btn-sm btn-success" onclick="modal_ttd_kel()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_dokter_ina'].'<br><i>'.$row_ttd['ket_ttd_dokter_eng'].'</i></td>';
	  $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_dokter_ina'].'/'.$row_ttd['ttd_dokter_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_dokter.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_pasien_ina'].'<br><i>'.$row_ttd['ket_ttd_pasien_eng'].'</i></td>';
	   $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_pasien_ina'].'/'.$row_ttd['ttd_pasien_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_kel.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function list_index_his_dnr()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$assesmen_id=$this->input->post('assesmen_id');
			$versi_edit=$this->input->post('versi_edit');
			$filter_ppa_id=$this->input->post('filter_ppa_id');
			$filter_profesi_id=$this->input->post('filter_profesi_id');
			$st_owned=$this->input->post('st_owned');
			$tanggal_1=$this->input->post('tanggal_1');
			$tanggal_2=$this->input->post('tanggal_2');
			$where='';
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($filter_ppa_id){
				$filter_ppa_id=implode(", ", $filter_ppa_id);
				$where .=" AND (H.edited_ppa) IN (".$filter_ppa_id.")";
			}
			if ($filter_profesi_id){
				$filter_profesi_id=implode(", ", $filter_profesi_id);
				$where .=" AND (M2.jenis_profesi_id) IN (".$filter_profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT H.versi_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,'#' as jml_edit,H.edited_ppa,H.st_ranap
							FROM `tpoliklinik_dnr_his` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
							UNION ALL
							
							SELECT H.jml_edit,H.assesmen_id,H.pendaftaran_id,M1.nama as user_created,M2.nama as user_edited 
							,H.created_date,H.edited_date,MA.keterangan,H.keterangan_edit
							,created_ppa,H.jml_edit,H.edited_ppa,H.st_ranap
							FROM `tpoliklinik_dnr` H
							LEFT JOIN mppa M1 ON M1.id=H.created_ppa
							LEFT JOIN mppa M2 ON M2.id=H.edited_ppa
							LEFT JOIN malasan_batal MA ON MA.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id' ".$where."
						) H
						WHERE H.assesmen_id='$assesmen_id' 
						ORDER BY H.versi_edit DESC
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('user_created','user_edited','keterangan_edit','keterangan');
			$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();

		  $result[] = $no;
			 $aksi='';
		  if ($versi_edit!=$r->versi_edit){
			  if ($r->st_ranap=='1'){
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_ri/his_dnr_ri/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }else{
			 $aksi='<a class="btn btn-default  btn-xs" onclick="set_loading()" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_dnr/'.$r->assesmen_id.'/'.$r->versi_edit.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat</a>';
				  
			  }
			  
		  }else{
			  $aksi=text_default('SEDANG DILIHAT');
		  }
		  if ($r->jml_edit!='#'){
			  $btn_last=' &nbsp;'.text_success('DATA TERAKHIR');
		  }else{
			  $btn_last='';
			  
		  }
		  $result[] = $aksi;
		  $result[] = ($r->versi_edit=='0'?text_primary('ORIGINAL'):text_danger('PERUBAHAN KE-'.$r->versi_edit)).$btn_last;
		  $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
		  $result[] = ($r->user_edited?$r->user_edited.'<br>'.HumanDateLong($r->edited_date):'');
		  $result[] = $r->keterangan.'<br>'.text_default($r->keterangan_edit);
		 

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
	}
}
