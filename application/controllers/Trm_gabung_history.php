<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_gabung_history extends CI_Controller
{

    /**
     * History Gabung Rekam Medis controller.
     * Developer @GunaliRezqiMauludi
     */

    function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Trm_gabung_history_model', 'model');
    }

    function index()
    {
        $data = array(
          'nomedrec' => '',
          'namapasien' => '',
          'tanggallahir' => '',
          'jeniskelamin' => '',
          'alamatpasien' => '',
          'nomedrec_new' => '',
          'namapasien_new' => '',
          'tanggallahir_new' => '',
          'jeniskelamin_new' => '',
          'alamatpasien_new' => '',
          'tanggaldari' => date("Y-m-d"),
          'tanggalsampai' => date("Y-m-d"),
          'iduser' => '',
        );

        $data['error']     = '';
        $data['title']     = 'History Gabung Rekam Medis';
        $data['content']   = 'Trm_gabung_history/manage';
        $data['breadcrum'] = array(
					array("RSKB Halmahera",'#'),
          array("Gabung Rekam Medis",'#'),
          array("History",'trm_gabung')
        );

        $data['list_user'] = $this->model->getUser();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function filter(){
      $data = array(
        'nomedrec' => $this->input->post('nomedrec'),
        'namapasien' => $this->input->post('namapasien'),
        'tanggallahir' => $this->input->post('tanggallahir'),
        'jeniskelamin' => $this->input->post('jeniskelamin'),
        'alamatpasien' => $this->input->post('alamatpasien'),
        'nomedrec_new' => $this->input->post('nomedrec_new'),
        'namapasien_new' => $this->input->post('namapasien_new'),
        'tanggallahir_new' => $this->input->post('tanggallahir_new'),
        'jeniskelamin_new' => $this->input->post('jeniskelamin_new'),
        'alamatpasien_new' => $this->input->post('alamatpasien_new'),
        'tanggaldari' => $this->input->post('tanggaldari'),
        'tanggalsampai' => $this->input->post('tanggalsampai'),
        'iduser' => $this->input->post('iduser'),
      );

  		$this->session->set_userdata($data);

      $data['error']     = '';
      $data['title']     = 'History Gabung Rekam Medis';
      $data['content']   = 'Trm_gabung_history/manage';
      $data['breadcrum'] = array(
        array("RSKB Halmahera",'#'),
        array("Gabung Rekam Medis",'#'),
        array("Filter",'trm_gabung')
      );

      $data['list_user'] = $this->model->getUser();

  		$data = array_merge($data, backend_info());
  		$this->parser->parse('module_template', $data);
  	}

    function getIndex($uri='index')
  	{
  			$this->select = array(
  				'trm_gabung.id',
  				'trm_gabung.created_at AS tanggal',
  				'mfpasien1.no_medrec AS nomedrec',
  				'mfpasien1.nama AS namapasien',
  				'mfpasien1.tanggal_lahir AS tanggallahir',
  				'mfpasien1.alamat_jalan AS alamatpasien',
          'mfpasien2.no_medrec AS nomedrec_new',
  				'mfpasien2.nama AS namapasien_new',
  				'mfpasien2.tanggal_lahir AS tanggallahir_new',
  				'mfpasien2.alamat_jalan AS alamatpasien_new',
  				'trm_gabung.alasan',
  				'musers.name AS user_created',
  			);

  			$this->from   = 'trm_gabung';

  			$this->join 	= array(
  				array('mfpasien mfpasien1', 'mfpasien1.id = trm_gabung.idpasien_lama', 'LEFT'),
  				array('mfpasien mfpasien2', 'mfpasien2.id = trm_gabung.idpasien_baru', 'LEFT'),
  				array('musers', 'musers.id = trm_gabung.created_by', 'LEFT'),
  			);

  			// FILTER
  			$this->where  = array();

        if($uri == 'filter'){
          if ($this->session->userdata('nomedrec') != null) {
            $this->where = array_merge($this->where, array('mfpasien1.no_medrec LIKE' => '%'.$this->session->userdata('nomedrec').'%'));
          }
      		if ($this->session->userdata('namapasien') != null) {
            $this->where = array_merge($this->where, array('mfpasien1.nama LIKE' => '%'.$this->session->userdata('namapasien').'%'));
          }
      		if ($this->session->userdata('tanggallahir') != null) {
            $this->where = array_merge($this->where, array('DATE(mfpasien1.tanggal_lahir)' => $this->session->userdata('tanggallahir')));
          }
      		if ($this->session->userdata('alamatpasien') != null) {
            $this->where = array_merge($this->where, array('DATE(mfpasien1.alamat_jalan)' => $this->session->userdata('alamatpasien')));
          }

          if ($this->session->userdata('nomedrec_new') != null) {
            $this->where = array_merge($this->where, array('mfpasien2.no_medrec LIKE' => '%'.$this->session->userdata('nomedrec_new').'%'));
          }
      		if ($this->session->userdata('namapasien_new') != null) {
            $this->where = array_merge($this->where, array('mfpasien2.nama LIKE' => '%'.$this->session->userdata('namapasien_new').'%'));
          }
      		if ($this->session->userdata('tanggallahir_new') != null) {
            $this->where = array_merge($this->where, array('DATE(mfpasien2.tanggal_lahir)' => $this->session->userdata('tanggallahir_new')));
          }
      		if ($this->session->userdata('alamatpasien_new') != null) {
            $this->where = array_merge($this->where, array('DATE(mfpasien2.alamat_jalan)' => $this->session->userdata('alamatpasien_new')));
          }

      		if ($this->session->userdata('tanggaltransaksi') != null) {
            $this->where = array_merge($this->where, array('DATE(trm_gabung.created_at)' => $this->session->userdata('tanggaltransaksi')));
          }

      		if ($this->session->userdata('tanggaldari') != null) {
            $this->where = array_merge($this->where, array('DATE(trm_gabung.created_at) >=' => $this->session->userdata('tanggaldari')));
          }
      		if ($this->session->userdata('tanggalsampai') != null) {
            $this->where = array_merge($this->where, array('DATE(trm_gabung.created_at) <=' => $this->session->userdata('tanggalsampai')));
          }


      		if ($this->session->userdata('iduser') != "#") {
            $this->where = array_merge($this->where, array('musers.id' => $this->session->userdata('iduser')));
          }
        }else {
  				$this->where = array_merge($this->where, array('DATE(trm_gabung.created_at)' => date("Y-m-d")));
        }

  			$this->order  = array(
  				'trm_gabung.id' => 'DESC',
  			);

  			$this->group  = array();

  			$this->column_search   = array('mfpasien1.nomedrec');
  			$this->column_order    = array('mfpasien1.nomedrec');

  			$list = $this->datatable->get_datatables();
  			$data = array();
  			$no = $_POST['start'];
  			foreach ($list as $r) {
  					$no++;
  					$row = array();

  					$row[] = $r->tanggal;
  					$row[] = $r->nomedrec;
  					$row[] = $r->namapasien;
  					$row[] = $r->tanggallahir;
  					$row[] = $r->alamatpasien;

  					$row[] = $r->nomedrec_new;
  					$row[] = $r->namapasien_new;
  					$row[] = $r->tanggallahir_new;
  					$row[] = $r->alamatpasien_new;

  					$row[] = $r->alasan;
  					$row[] = $r->user_created;

						$row[] = '<a href="'.site_url().'trm_gabung/print_document/'.$r->id.'/3" class="btn btn-warning btn-sm" target="_blank"><i class="fa fa-print"></i></a>';

  					$data[] = $row;
  			}
  			$output = array(
  				"draw" => $_POST['draw'],
  				"recordsTotal" => $this->datatable->count_all(),
  				"recordsFiltered" => $this->datatable->count_all(),
  				"data" => $data
  			);
  			echo json_encode($output);
  	}
}
