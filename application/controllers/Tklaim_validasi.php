<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tklaim_validasi extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tklaim_validasi_model','model');
	}

	function index() {
		$hari_ini = date("Y-m-d");
		$date=date_create($hari_ini);
		date_add($date,date_interval_create_from_date_string("-30 days"));
		$tgl_pertama = date_format($date,"d-m-Y");
		
		$date=date_create($hari_ini);
		date_add($date,date_interval_create_from_date_string("30 days"));
		$tgl_terakhir = date_format($date,"d-m-Y");
		
		// $tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		// $tgl_terakhir = date('Y-m-d', strtotime($hari_ini));
		$data=array(
			'tipe'=>'#',
			'cara_bayar'=>'#',
			'no_po'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'#',
			'tanggal_tagihan'=>$tgl_pertama,
			'tanggal_tagihan2'=>$tgl_terakhir,
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['title'] 			= 'Validasi Pendapatan';
		$data['content'] 		= 'Tklaim_validasi/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Rincian Tagihan",'tklaim_validasi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
	
	
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$metode=$this->input->post('metode');
		$idrekanan=$this->input->post('idrekanan');
		$tipe=$this->input->post('tipe');
		$st_validasi=$this->input->post('st_validasi');
		$tanggal_tagihan=$this->input->post('tanggal_tagihan');
		$tanggal_tagihan2=$this->input->post('tanggal_tagihan2');
		$no_klaim=$this->input->post('no_klaim');
		
		$where1='';
		$where2='';
		$where='';
		
		if ($no_klaim !=''){
			$where1 .=" AND (H.no_klaim LIKE '%".$no_klaim."%') ";
		}
		if ($no_medrec !=''){
			$where1 .=" AND (TP.no_medrec LIKE '%".$no_medrec."%' OR RI.no_medrec LIKE '%".$no_medrec."%') ";
		}
		if ($nama_pasien !=''){
			$where1 .=" AND (TP.namapasien LIKE '%".$nama_pasien."%' OR RI.namapasien LIKE '%".$nama_pasien."%') ";
		}
		if ($st_validasi !='#'){
			$where1 .=" AND B.st_validasi='$st_validasi' ";
		}
		if ($idrekanan !='#'){
			$where1 .=" AND H.idrekanan='$idrekanan' ";
		}
		if ($tipe !='#'){
			$where1 .=" AND D.tipe='$tipe' ";
		}
		if ($metode !='#'){
			$where1 .=" AND B.metode_bayar='$metode' ";
		}
		
		
		if ('' != $tanggal_tagihan) {
            $where1 .= " AND DATE(B.tanggal_pembayaran) >='".YMDFormat($tanggal_tagihan)."' AND DATE(B.tanggal_pembayaran) <='".YMDFormat($tanggal_tagihan2)."'";
        }
		
        $from = "(
					SELECT COALESCE(TP.no_medrec,RI.no_medrec) as no_medrec,COALESCE(TP.namapasien,RI.namapasien) as namapasien,
					B.id as idpembayaran,D.id as detail_id,D.klaim_id, B.tanggal_pembayaran,H.no_klaim
					,CASE WHEN H.idkelompokpasien='1' THEN MR.nama ELSE KP.nama END as rekanan
					,D.tipe,B.nominal_bayar as nominal,B.metode_bayar,CASE WHEN B.metode_bayar='1' THEN 'TRANSFER' ELSE 'TUNAI' END as metode ,SK.nama as sumberkas
					,B.bankid,MB.nama as bank,B.st_validasi,TBL.jml,TBL.jml_validasi,B.st_posting
					,D.pendaftaran_id,D.kasir_id,COALESCE(KRI.total,KRJ.total) as total_trx
					from tklaim_pembayaran B
					
					INNER JOIN tklaim_detail D ON D.id=B.klaim_detail_id
					INNER JOIN tklaim H ON H.id=D.klaim_id
					LEFT JOIN trawatinap_tindakan_pembayaran KRI ON KRI.id=D.kasir_id AND D.tipe='2'
					LEFT JOIN tkasir KRJ ON KRJ.id=D.kasir_id AND D.tipe='1'
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=D.pendaftaran_id AND D.tipe='1'
					LEFT JOIN trawatinap_pendaftaran RI ON RI.id=D.pendaftaran_id AND D.tipe='2'
					
					LEFT JOIN (
						SELECT B.klaim_id,B.tanggal_pembayaran, COUNT(*) jml,SUM(B.st_validasi) as jml_validasi FROM tklaim_pembayaran B
						WHERE B.`status`='1'
						GROUP BY B.klaim_id,B.tanggal_pembayaran
					) TBL ON TBL.klaim_id=B.klaim_id AND TBL.tanggal_pembayaran=B.tanggal_pembayaran
					LEFT JOIN mrekanan MR ON MR.id=H.idrekanan AND H.idkelompokpasien='1'
					LEFT JOIN mpasien_kelompok KP ON KP.id=H.idkelompokpasien
					LEFT JOIN mbank MB ON MB.id=B.bankid AND B.metode_bayar='1'
					LEFT JOIN msumber_kas SK ON SK.bank_id=MB.id
					WHERE B.`status`='1' ".$where1."
					ORDER BY B.tanggal_pembayaran DESC,H.id ASC,D.id DESC
				) as tbl";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('rekanan');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tklaim_monitoring');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			$url_dok_rajal       = site_url('tverifikasi_transaksi/upload_document/tkasir/');
            $url_ranap        = site_url('trawatinap_tindakan/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->idpembayaran;
            $row[] = $r->metode_bayar;
            $row[] = $r->st_validasi;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggal_pembayaran).'<br>'.$r->no_klaim;
            $row[] = $r->no_medrec.'<br>'.$r->namapasien;
            $row[] = $r->rekanan.' '.$r->kasir_id;
            $row[] = ($r->tipe=='1' ? '<span class="label label-primary">RJ</span>':'<span class="label label-success">RI/ODS</span>');
            $row[] = number_format($r->total_trx,2);
            $row[] = number_format($r->nominal,2);
            $row[] = ($r->metode_bayar=='1' ? '<span class="label label-primary">TRANSFER</span>':'<span class="label label-default">TUNAI</span>').'<br>'.$r->bank;
            $row[] = $r->sumberkas;
			if ($r->st_validasi=='1'){
				if ($r->st_posting=='0'){
					$row[] = '<span class="label label-success">TELAH DIPROSES</span>';
					
				}else{
					$row[] = '<span class="label label-warning">TELAH POSTING</span>';
				}
			}elseif ($r->st_validasi=='0'){
				$row[] = '<span class="label label-default">BELUM DIPROSES</span>';			
			}
           
			
			// $aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_tagihan/'.$r->id.'/'.$r->tipe.'/'.$r->status_kirim.'" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></a>';
			// $aksi .= '<button title="List Bayar" class="btn btn-xs btn-default list_bayar"><i class="fa fa-money"></i></button>';
			$aksi .= '<a title="Lihat Detail" href="'.$url.'/koreksi_tagihan/'.$r->klaim_id.'/'.$r->tipe.'/1" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-th-list"></i></a>';
			
			if ($r->tipe=='1'){
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'print_transaksi/'.$r->pendaftaran_id.'/'.$r->kasir_id.'" target="_blank"  type="button"  title="Rincian"><i class="fa fa-print"></i></a>';
			}else{
				$aksi .= '<div class="btn-group" role="group">
						<button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-print"></i>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li class="dropdown-header">Print Ranap</li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->pendaftaran_id.'/0" target="_blank"  > Rincian Biaya (All)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->pendaftaran_id.'/1" target="_blank"  > Rincian Biaya (Verified)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->pendaftaran_id.'/0" target="_blank"  > Rincian Global (All)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->pendaftaran_id.'/1" target="_blank"  > Rincian Global (Verified)</a></li>
							
						</ul>
					</div>';
			}
			// $aksi .= '<button class="view btn btn-xs btn-success" type="button"  title="Detail"><i class="fa fa-print"></i></button>';
			if ($r->st_validasi=='0'){
				// if ($r->metode_bayar=='1'){
					$aksi .= '<button class="btn btn-xs btn-primary validasi" type="button"  title="Validasi"><i class="fa fa-check"></i> Validasi</button>';				
				// }
			}
			if ($r->st_posting=='0'){
				if ($r->jml==$r->jml_validasi){
					$aksi .= '<button class="btn btn-xs btn-warning" type="button" onclick="posting(\''.$r->klaim_id.'\',\''.$r->tanggal_pembayaran.'\')"  title="Posting Jurnal"><i class="fa fa-send"></i> Posting</button>';				
					
				}
			}
			
			$aksi.='</div>';
            $row[] = $aksi;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(true),
            "recordsFiltered" => $this->datatable->count_all(true),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	public function verif_semua()
    {
		$arr_id = $this->input->post('arr_id');
		
		foreach ($arr_id as $x => $val){
			$id = $arr_id[$x];
		
			$result=$this->db->query("UPDATE tklaim_pembayaran set st_validasi='1' where id='$id'");

		}
		$this->output->set_output(json_encode($result));
    }
	
	public function validasi()
    {
		$id=$this->input->post('id');
		

       $result=$this->db->query("UPDATE tklaim_pembayaran set st_validasi='1' where id='$id'");
        if ($result) {
            $this->output->set_output(json_encode($data_info));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function posting()
    {
		$id=$this->input->post('id');
		$tanggal=YMDFormat($this->input->post('tanggal'));		
		// print_r($tanggal);exit();
       $result=$this->model->insert_jurnal_pelunasan_piutang($id,$tanggal);
        if ($result) {
			$this->db->update('tklaim_pembayaran',array('st_posting'=>1),array('klaim_id'=>$id,'tanggal_pembayaran'=>$tanggal));
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	
	
	
}
