<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpiutang extends CI_Controller {

	/**
	 * Pengaturan Piutang controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('mpiutang_model');
  }

	function index(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '#',
			'status' 				=> '1'
		);
		$data['error'] 			= '';
		$data['title'] 			= 'Pengaturan Piutang';
		$data['content'] 		= 'Mpiutang/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Pengaturan Piutang",'#'),
									    			array("List",'Mpiutang')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'kelompok_operasi_id' 			=> '#',
			'status' 				=> '1'
		);

		$data['lokasi_tubuh_list'] = $this->mpiutang_model->lokasi_tubuh_list();
		$data['kelompok_operasi_list'] = $this->mpiutang_model->kelompok_operasi_list();
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Pengaturan Piutang';
		$data['content'] 		= 'Mpiutang/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Pengaturan Piutang",'#'),
									    			array("Tambah",'Mpiutang')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function js_icd()
	{
		$arr = $this->mpiutang_model->js_icd();
		echo json_encode($arr);
	}
	function jd_pegawai_dokter()
	{
		$arr = $this->mpiutang_model->jd_pegawai_dokter();
		echo json_encode($arr);
	}
	function simpan_add()
	{
		$data=array(
			'idpengaturan'=>$this->input->post('id'),
			'tanggal_hari'=>$this->input->post('tanggal_hari'),
			'deskripsi'=>$this->input->post('deskripsi'),
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
			'created_date'=>date('Y-m-d H:i:s'),
			
		);
		
       // print_r($data);
		$result = $this->db->insert('mpiutang_setting',$data);
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function simpan_edit()
	{
		$data=array(
			'deskripsi'=>$this->input->post('deskripsi'),
			'tanggal_hari'=>$this->input->post('tanggal_hari'),
			
		);
		$this->db->where('id',$this->input->post('tedit'));
		
       // print_r($data);
		$result = $this->db->update('mpiutang_setting',$data);
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function hapus_tanggal()
	{
		$this->db->where('id',$this->input->post('tedit'));		
       // print_r($data);
		$result = $this->db->delete('mpiutang_setting');
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function hapus_peg()
	{
		$tipe=$this->input->post('tipe');
		$this->db->where('id',$this->input->post('id'));		
       // print_r($data);
	   if ($tipe=='1'){		   
			$result = $this->db->delete('mpiutang_pegawai');
	   }else{
		   $result = $this->db->delete('mpiutang_dokter');
	   }
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function cek_duplicate()
	{
		// tipe: tipe,	peg_id:peg_id,pengaturan_id:id
		$tipe=$this->input->post('tipe');
		
		$peg_id=$this->input->post('peg_id');
		$pengaturan_id=$this->input->post('pengaturan_id');
		if ($tipe=='1'){
			$q="select *from mpiutang_pegawai where idpegawai='$peg_id'";
			
		}else{
			$q="select *from mpiutang_dokter where iddokter='$peg_id'";
		}
		$query=$this->db->query($q);
		$result=$query->row_array();
		if ($result){
			$result=false;
			echo json_encode($result);
		}else{
			if ($tipe=='1'){
				$data=array(
					'idpengaturan'=>$this->input->post('pengaturan_id'),
					'idpegawai'=>$this->input->post('peg_id'),
					'created_by'=>$this->session->userdata('user_id'),
					'created_nama'=>$this->session->userdata('user_name'),
					'created_date'=>date('Y-m-d H:i:s'),
					
					);
				$result = $this->db->insert('mpiutang_pegawai',$data);
			}else{
				$data=array(
					'idpengaturan'=>$this->input->post('pengaturan_id'),
					'iddokter'=>$this->input->post('peg_id'),
					'created_by'=>$this->session->userdata('user_id'),
					'created_nama'=>$this->session->userdata('user_name'),
					'created_date'=>date('Y-m-d H:i:s'),
					
					);
				$result = $this->db->insert('mpiutang_dokter',$data);
			}
			
			
		   // print_r($data);
			
			if ($result) {
				echo json_encode($result);
			}else{
				echo json_encode($result);
			}
		}
	}
	function load_tanggal()
    {
		
		$id     = $this->input->post('id');
		
		$iduser=$this->session->userdata('user_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT D.id,H.nama,D.tanggal_hari,D.deskripsi from mpiutang H
				INNER JOIN mpiutang_setting D ON D.idpengaturan=H.id
				WHERE H.id='$id'
				ORDER BY D.tanggal_hari ASC
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('tanggal_hari','deskripsi','nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->deskripsi;
            $row[] = sprintf("%02s", $r->tanggal_hari);
            $aksi       = '<div class="btn-group">';	
			
			$aksi 		.= '<button type="button" class="btn btn-xs btn-primary edit" title="Hapus"><i class="fa fa-pencil"></i></button>';
			$aksi 		.= '<button class="btn btn-xs btn-danger hapus" title="Hapus"><i class="fa fa-close"></i></button>';
				
			
			$aksi.='</div>';
			
			$row[] = $aksi;			
            $row[] = $r->id;//3
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	function load_peg_dok()
    {
		
		$id     = $this->input->post('id');
		
		$iduser=$this->session->userdata('user_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT 'Pegawai' as tipe,'1' as tipe_id, P.id,M.nama from mpiutang_pegawai P
					LEFT JOIN mpegawai M ON M.id=P.idpegawai

					WHERE P.idpengaturan='$id'

					UNION ALL

					SELECT 'Dokter' as tipe,'2' as tipe_id, P.id,M.nama from mpiutang_dokter P
					LEFT JOIN mdokter M ON M.id=P.iddokter

					WHERE P.idpengaturan='$id'

				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('nama','tipe');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->tipe;
            $row[] = $r->nama;
            $aksi       = '<div class="btn-group">';	
			
			$aksi 		.= '<button class="btn btn-xs btn-danger hapus_peg" title="Hapus"><i class="fa fa-close"></i></button>';				
			
			$aksi.='</div>';			
			$row[] = $aksi;			
            $row[] = $r->id;
            $row[] = $r->tipe_id;
            $row[] = '';
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	function update($id){
		
		if($id != ''){
			$row = $this->mpiutang_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Pengaturan Piutang';
				$data['content']	 	= 'Mpiutang/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Pengaturan Piutang",'#'),
											    			array("Ubah",'mpiutang')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mpiutang','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpiutang');
		}
	}

	function delete($id){
		// print_r($id);exit();
		$this->mpiutang_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mpiutang','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->mpiutang_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mpiutang/update/'.$id,'location');
				}
			} else {
				if($this->mpiutang_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mpiutang/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 		= validation_errors();
		$data['content'] 	= 'Mpiutang/manage';

		if($id==''){
			$data['title'] = 'Tambah Pengaturan Piutang';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Pengaturan Piutang",'#'),
															array("Tambah",'Mpiutang')
													);
		}else{
			$data['title'] = 'Ubah Pengaturan Piutang';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Pengaturan Piutang",'#'),
															array("Ubah",'Mpiutang')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
		$status_aktif     = $this->input->post('status_aktif');
		$kelompok_operasi_id     = $this->input->post('kelompok_operasi_id');
		
		$iduser=$this->session->userdata('user_id');
		
		$where='';
		if ($status_aktif !='#'){
			if ($status_aktif=='1') {
				$where .=" AND M.status=='1'";				
			}else{
				$where .=" AND M.status=='0'";					
			}
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		$from="(SELECT M.id,M.nama,GROUP_CONCAT(DISTINCT(S.tanggal_hari) ORDER BY tanggal_hari ASC) as tanggal
			,COUNT(DISTINCT(P.id)) as jml_pegawai,COUNT(DISTINCT(D.id)) as jml_dokter,M.`status` 
			FROM mpiutang M
			LEFT JOIN mpiutang_setting S ON S.idpengaturan=M.id
			LEFT JOIN mpiutang_pegawai P ON P.idpengaturan=M.id
			LEFT JOIN mpiutang_dokter D ON D.idpengaturan=M.id
			WHERE M.status='1'
			GROUP BY M.id
					) as tbl";
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $no;
          $row[] = $r->nama;
          $row[] ='<span class="label label-success">'.$r->tanggal.'</span>';;
          $row[] = $r->jml_pegawai;
          $row[] = $r->jml_dokter;
		  $status='';
		  if ($r->status=='1'){
			  $status='<span class="label label-success">AKTIF</span>';
		  }else{
			$status='<span class="label label-danger">TIDAK AKTIF</span>';
		  }
          $row[] = $status;
          $aksi = '<div class="btn-group">';
          
		  if (UserAccesForm($user_acces_form,array('315'))){
          	$aksi .= '<a href="'.site_url().'mpiutang/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
          }
          
		  if (UserAccesForm($user_acces_form,array('316'))){
          	$aksi .= '<a href="#" data-urlindex="'.site_url().'mpiutang" data-urlremove="'.site_url().'mpiutang/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
          }
          $aksi .= '</div>';
          $row[] = $aksi;
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
				  
		
  }
	function ajaxSave(){
		if ($this->mpiutang_model->saveData()) {
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			$data['response']="data telah di simpan";
			$data['code']=200;
			$data['id']=$this->db->insert_id();
			$data['nama']=$this->input->post("nama");
			echo json_encode($data);
		}
	}
}
