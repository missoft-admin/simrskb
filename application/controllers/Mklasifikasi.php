<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mklasifikasi extends CI_Controller {

	/**
	 * Klasifikasi Pembelian controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mklasifikasi_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Klasifikasi Pembelian';
		$data['content'] 		= 'Mklasifikasi/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Klasifikasi Pembelian",'mklasifikasi/index'),
														array("List",'#')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 							=> '',
			'nama' 						=> '',
			'idakun' 						=> '',
			'idakun_diskon' 						=> '',
			'idakun_ppn' 						=> '',
			'status' 					=> '',
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Klasifikasi Pembelian';
		$data['content'] 		= 'Mklasifikasi/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Klasifikasi Pembelian",'#'),
									    			array("Tambah",'mklasifikasi')
													);

		$data['list_akun'] 	= $this->Mklasifikasi_model->list_akun();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mklasifikasi_model->getSpecified($id);
			// print_r($row);exit();
			if(isset($row->id)){
				$data = array(
					'id' 							 => $row->id,
					'nama' 						 => $row->nama,
					'idakun' 						 => $row->idakun,
					'idakun_diskon' 						 => $row->idakun_diskon,
					'idakun_ppn' 						 => $row->idakun_ppn,
					'status' 					 => $row->status,
				);
				// print_r($data);exit();

				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Klasifikasi Pembelian';
				$data['content']	 	= 'Mklasifikasi/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Klasifikasi Pembelian",'#'),
											    			array("Ubah",'mklasifikasi')
															);
				$data['list_akun'] 	= $this->Mklasifikasi_model->list_akun();

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mklasifikasi','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mklasifikasi','location');
		}
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mklasifikasi_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mklasifikasi/index/'.$this->input->post('idtipe'),'location');
				}
			} else {
				if($this->Mklasifikasi_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mklasifikasi/index/'.$this->input->post('idtipe'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 			 = validation_errors();
		$data['content'] 		 = 'Mklasifikasi/manage';

		if($id==''){
			$data['title'] = 'Tambah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Tambah",'mklasifikasi')
													);
		}else{
			$data['title'] = 'Ubah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Ubah",'mklasifikasi')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function delete($id){
		$this->Mklasifikasi_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mklasifikasi','location');
	}

	function getIndex()
	{
			$from="(
					SELECT H.id, H.nama,H.`status`
					,CONCAT(A.noakun,' - ',A.namaakun,' (',A.id,')') as akun
					,CONCAT(AD.noakun,' - ',AD.namaakun,' (',AD.id,')') as akun_diskon
					,CONCAT(AP.noakun,' - ',AP.namaakun,' (',AP.id,')') as akun_ppn
					,H.idakun,H.idakun_diskon,H.idakun_ppn 
					FROM mklasifikasi H
					LEFT JOIN makun_nomor A ON A.id=H.idakun
					LEFT JOIN makun_nomor AD ON AD.id=H.idakun_diskon
					LEFT JOIN makun_nomor AP ON AP.id=H.idakun_ppn
					WHERE H.`status`='1'
					GROUP BY H.id
			) as tbl
			";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

			$this->column_search   = array();
			$this->column_order    = array();

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$aksi = '<div class="btn-group">
						<a href="'.site_url().'mklasifikasi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
						<a href="#" data-urlindex="'.site_url().'mklasifikasi" data-urlremove="'.site_url().'mklasifikasi/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>
					</div>';

					$row[] = $no;
					$row[] = $r->nama;
					$row[] = $r->akun;
					$row[] = $r->akun_diskon;
					$row[] = $r->akun_ppn;
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
}
