<?php defined('BASEPATH') or exit('No direct script access allowed');

class Thonor_approval extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Thonor_approval_model', 'model');
	}

	public function create()
	{
		$data = [
			'disabel' => '',
			'id' => '',
			'list_trx' => '',
			'no_terima' => '',
			'tanggal_pengajuan1' => '',
			'tanggal_pengajuan2' => '',
			'iddokter' => '',
		];
		$data['error'] = '';
		// $data['list_trx'] 	= array();
		$data['list_kategori'] = $this->model->list_kategori();
		$data['title'] = 'Create Approval Honor Dokter';
		$data['content'] = 'Thonor_approval/create';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Honor Dokter Transaksi', 'thonor_approval/index'],
			['List', '#']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function load_trx()
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		$userid = $this->session->userdata('user_id');

		$id = $this->input->post('id');
		$idkategori = $this->input->post('idkategori');
		$notransaksi = $this->input->post('notransaksi');
		$iddokter = $this->input->post('iddokter');
		$tanggalpembayaran_dari = $this->input->post('tanggalpembayaran_dari');
		$tanggalpembayaran_sampai = $this->input->post('tanggalpembayaran_sampai');
		$list_trx = $this->input->post('list_trx');

		$where = '';
		// if ($id==''){
		// $where .=" AND H.`status`='2'";
		// }else{
		// $where .=" AND H.`status`IN ('2','3')";
		// }
		if ($list_trx != '') {
			$where .= ' AND H.id NOT IN (' . $list_trx . ')';
		}
		if ($iddokter != '') {
			$arr_dokter = implode("','",$iddokter);
			$where .= " AND H.iddokter IN ('" . $arr_dokter . "')";
		}
		if ($notransaksi != '') {
			$where .= " AND H.notransaksi LIKE '%" . $notransaksi . "%'";
		}
		if ($tanggalpembayaran_dari != '' && $tanggalpembayaran_sampai) {
			$where .= " AND (H.tanggal_pembayaran >= '" . YMDFormat($tanggalpembayaran_dari) . "' AND H.tanggal_pembayaran <= '" . YMDFormat($tanggalpembayaran_sampai) . "')";
		}
		if ($idkategori != '#') {
			$where .= " AND MD.idkategori='$idkategori'";
		}

		$from = "(
					SELECT H.id,H.notransaksi,H.iddokter,H.namadokter,MD.idkategori,MK.nama as kategori 
					,H.nominal,H.tanggal_pembayaran,H.tanggal_jatuhtempo,H.`status`
					,GROUP_CONCAT(S.id ORDER BY S.id asc) as list_logic
					FROM thonor_dokter H
					LEFT JOIN mdokter MD ON MD.id=H.iddokter
					LEFT JOIN mdokter_kategori MK ON MK.id=MD.idkategori
					LEFT JOIN mlogic_honor S ON MD.idkategori=S.idtipe AND S.status='1' AND calculate_logic(S.operand, H.nominal, S.nominal)='1'
					WHERE H.`status`='1' AND H.status_stop=1 AND H.status_approval=0 " . $where . '
					GROUP BY H.id
				) as tbl';

		//WHERE H.`status`='2' AND H.mbagi_hasil_id='$mbagi_hasil_id' ".$where."
		// print_r($from);exit();
		$this->from = $from;
		$this->join = [];
		$this->where = [];
		$this->where_in = [];
		$this->order = [];
		$this->group = [];
		$this->column_search = [];
		$this->column_order = [];
		$this->select = [];
		$this->load->library('datatables');
		$list = $this->datatable->get_datatables();
		$data = [];
		$no = null;
		if (isset($_POST['start'])) {
			$no = $_POST['start'];
		}
		foreach ($list as $r) {
			$no++;
			$row = [];
			$aksi = '<div class="btn-group">';
			$btn_disabled = ($r->list_logic ? '' : 'disabled');
			$row[] = $r->id;
			$row[] = $no;
			// $row[] = $r->list_logic;
			$row[] = $r->notransaksi . '<br>' . $r->list_logic;
			$row[] = $r->namadokter;
			$row[] = $r->kategori;
			$row[] = HumanDateShort($r->tanggal_pembayaran);
			$row[] = number_format($r->nominal, 0);
			$row[] = HumanDateShort($r->tanggal_jatuhtempo);
			$row[] = ($btn_disabled == '' ? text_success('SUDAH DISETTING') : text_danger('BELUM DISETTING'));
			$aksi .= '<button data-toggle="tooltip" title="Pilih" ' . $btn_disabled . ' class="btn btn-primary btn-xs pilih"><i class="fa fa-check"></i> Pilih</button>';
			$aksi .= '</div>';
			$row[] = $aksi; //8
			$data[] = $row;
		}

		$draw = null;
		if (isset($_POST['draw'])) {
			$draw = $_POST['draw'];
		}

		$output = [
			'draw' => $draw,
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($output, JSON_PRETTY_PRINT));
	}

	public function load_hasil()
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		$userid = $this->session->userdata('user_id');

		$list_trx = $this->input->post('list_trx');
		$disabel = $this->input->post('disabel');
		$where = '';
		if ($list_trx != '') {
			$where .= ' AND H.id IN (' . $list_trx . ')';
		} else {
			$where .= ' AND H.id=0';
		}

		$from = "(
					SELECT H.id,H.notransaksi,H.iddokter,H.namadokter,MD.idkategori,MK.nama as kategori 
					,H.nominal,H.tanggal_pembayaran,H.tanggal_jatuhtempo,H.`status`
					,GROUP_CONCAT(S.id ORDER BY S.id asc) as list_logic
					FROM thonor_dokter H
					LEFT JOIN mdokter MD ON MD.id=H.iddokter
					LEFT JOIN mdokter_kategori MK ON MK.id=MD.idkategori
					LEFT JOIN mlogic_honor S ON MD.idkategori=S.idtipe AND S.status='1' AND calculate_logic(S.operand, H.nominal, S.nominal)='1'
					WHERE H.`status`='1' AND H.status_approval=0 " . $where . '
					GROUP BY H.id
				) as tbl';

		//
		// print_r($from);exit();
		$this->from = $from;
		$this->join = [];
		$this->where = [];
		$this->where_in = [];
		$this->order = [];
		$this->group = [];
		$this->column_search = [];
		$this->column_order = [];
		$this->select = [];
		$this->load->library('datatables');
		$list = $this->datatable->get_datatables();
		$data = [];
		$no = null;
		if (isset($_POST['start'])) {
			$no = $_POST['start'];
		}
		foreach ($list as $r) {
			$no++;
			$row = [];
			$aksi = '<div class="btn-group">';
			$row[] = $r->id;
			$row[] = $no;
			$row[] = $r->notransaksi . '<br>' . $r->list_logic;
			$row[] = $r->namadokter;
			$row[] = $r->kategori;
			$row[] = HumanDateShort($r->tanggal_pembayaran);
			$row[] = number_format($r->nominal, 0);
			$row[] = HumanDateShort($r->tanggal_jatuhtempo);
			$row[] = '<button type="button" class="btn btn-success btn-xs">Selesai Diproses</button>';
			$aksi .= '<button type="button" data-toggle="tooltip" ' . $disabel . ' title="Pilih" class="btn btn-danger btn-xs hapus"><i class="fa fa-trash-o"></i></button>';
			$aksi .= '</div>';
			$row[] = $aksi; //8
			$data[] = $row;
		}

		$draw = null;
		if (isset($_POST['draw'])) {
			$draw = $_POST['draw'];
		}

		$output = [
			'draw' => $draw,
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($output, JSON_PRETTY_PRINT));
	}

	public function save()
	{
		// print_r($this->input->post());exit();
		$list_trx = $this->input->post('list_trx');
		if ($this->model->saveData($list_trx)) {
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah disimpan.');
			// redirect('thonor_approval','location');
			redirect('thonor_approval/index', 'location');
		}
	}

	public function simpan_proses_peretujuan($id)
	{
		$result = $this->model->saveData($id);
		echo json_encode($result);
	}

	public function index()
	{
		$data = [
			'status' => '1',
			'mbagi_hasil_id' => '#',
			'deskripsi' => '',
			'no_terima' => '',
			'tanggal_pengajuan1' => '',
			'tanggal_pengajuan2' => '',
		];
		$data['error'] = '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		// $data['list_mbagi_hasil'] 	= $this->model->list_mbagi_hasil();
		$data['title'] = 'Approval Honor Dokter';
		$data['content'] = 'Thonor_approval/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Honor Dokter Transaksi', 'thonor_approval/index'],
			['List', '#']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function detail($id)
	{
		$data = $this->model->get_header($id);
		$data['error'] = '';

		$data['title'] = 'Honor Dokter Detail';
		$data['content'] = 'Thonor_approval/detail';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Honor Dokter Transaksi', 'thonor_approval/index'],
			['List', '#']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function get_index()
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		$userid = $this->session->userdata('user_id');
		$iduser = $this->session->userdata('user_id');

		$notransaksi = $this->input->post('notransaksi');
		$status = $this->input->post('status');

		$tanggal_pengajuan1 = $this->input->post('tanggal_pengajuan1');
		$tanggal_pengajuan2 = $this->input->post('tanggal_pengajuan2');

		$where = '';
		if ($notransaksi != '') {
			$where .= " AND H.noreg='$notransaksi' ";
		}

		if ('' != $tanggal_pengajuan1) {
			$where .= " AND DATE(H.tanggal_pengajuan) >='" . YMDFormat($tanggal_pengajuan1) . "' AND DATE(H.tanggal_pengajuan) <='" . YMDFormat($tanggal_pengajuan2) . "'";
		}

		if ($status != '#') {
			$where .= " AND H.status_approval='$status' ";
		}

		$from = "(
					SELECT H.id,H.noreg,H.tanggal_pengajuan,H.nominal,H.status_approval 
					,(SELECT MAX(AP.step) FROM thonor_approval AP WHERE AP.st_aktif='1' AND AP.idhead=H.id) as step
					FROM thonor_approval_head H
					LEFT JOIN thonor_approval A ON A.idhead=H.id
					WHERE H.created_by='$iduser' OR A.iduser IN(" . $iduser . ') ' . $where . '
					GROUP BY H.id
					ORDER BY H.id DESC
				) as tbl ';

		// print_r($from);exit();
		$this->from = $from;
		$this->join = [];
		$this->where = [];
		$this->where_in = [];
		$this->order = [];
		$this->group = [];
		$this->column_search = ['noreg'];
		$this->column_order = [];
		$this->select = [];
		$this->load->library('datatables');
		$list = $this->datatable->get_datatables();
		$data = [];
		$no = null;
		if (isset($_POST['start'])) {
			$no = $_POST['start'];
		}

		foreach ($list as $r) {
			$no++;
			$row = [];
			$aksi = '<div class="btn-group">';
			if ($r->status_approval != '3') {
				$aksi .= '<a href="' . site_url('thonor_approval/') . 'detail/' . $r->id . '" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
			}
			$status = '';
			$respon = '';
			$status_setting = '';
			$url = site_url('tbagi_hasil/');
			// if ($r->status=='2'){
			$status_setting = '<button title="Lihat User" class="btn btn-danger btn-xs user" onclick="load_user(' . $r->id . ')"><i class="si si-user"></i></button>';
			// }
			$query = $this->get_respon($r->id, $r->step);
			if ($query) {
				foreach ($query as $res) {
					if ($res->iduser == $iduser && $res->step == $r->step) {
						if ($res->approve == '0') {
							$aksi .= '<button title="Setuju" class="btn btn-success btn-xs setuju" onclick="setuju(' . $res->id . ',' . $r->id . ')"><i class="fa fa-check"></i> SETUJUI</button>';
							$aksi .= '<button title="Tolak" class="btn btn-danger btn-xs tolak" onclick="tolak(' . $res->id . ',' . $r->id . ')"><i class="si si-ban"></i> TOLAK</button>';
						} else {
							if ($r->status_approval == '3') {
								$aksi .= '<span class="label label-danger">DITOLAK</span>';
							}
						}
					}
					if ($res->iduser == $iduser) {
						$respon .= '<span class="label label-warning" data-toggle="tooltip">STEP ' . $res->step . '</span> : ' . status_approval($res->approve) . '<br>';
					}
				}
			} else {
				$respon = '';
			}

			$aksi .= '<a href="' . site_url() . 'thonor_approval/upload_document/' . $r->id . '" title="Upload Dokumen" class="btn btn-danger btn-xs"><i class="fa fa-upload"></i></a>';

			$row[] = $r->id;
			$row[] = $no;
			$row[] = $r->noreg;
			$row[] = HumanDateShort($r->tanggal_pengajuan);
			$row[] = number_format($r->nominal, 2);
			$row[] = status_approval_HD($r->status_approval, $r->step) . ' ' . $status_setting;
			$row[] = $respon; //8

			$aksi .= '</div>';
			$row[] = $aksi; //8
			$data[] = $row;
		}

		$draw = null;
		if (isset($_POST['draw'])) {
			$draw = $_POST['draw'];
		}

		$output = [
			'draw' => $draw,
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($output, JSON_PRETTY_PRINT));
	}

	public function get_detail()
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		$userid = $this->session->userdata('user_id');
		$iduser = $this->session->userdata('user_id');

		$id = $this->input->post('id');

		$from = "(
					SELECT HD.tanggal_pembayaran,HD.notransaksi,H.namadokter,H.kategori,H.nominal 
					,HD.id
					FROM `thonor_approval_detail` H
					LEFT JOIN thonor_dokter HD ON HD.id=H.idhonor
					WHERE H.idhead='$id'
				) as tbl ";

		// print_r($from);exit();
		$this->from = $from;
		$this->join = [];
		$this->where = [];
		$this->where_in = [];
		$this->order = [];
		$this->group = [];
		$this->column_search = ['namadokter', 'notransaksi', 'kategori'];
		$this->column_order = [];
		$this->select = [];
		$this->load->library('datatables');
		$list = $this->datatable->get_datatables();
		$data = [];
		$no = null;
		if (isset($_POST['start'])) {
			$no = $_POST['start'];
		}

		foreach ($list as $r) {
			$no++;
			$row = [];
			$aksi = '<div class="btn-group">';
			$aksi .= '<a href="' . site_url('thonor_dokter/') . 'detail_honor/' . $r->id . '/disabled" target="_blank" title="Detail" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
			$status = '';
			$respon = '';
			$status_setting = '';

			$row[] = $r->id;
			$row[] = $no;
			$row[] = $r->notransaksi;
			$row[] = $r->namadokter;
			$row[] = HumanDateShort($r->tanggal_pembayaran);
			$row[] = number_format($r->nominal, 2);

			$aksi .= '</div>';
			$row[] = $aksi; //8
			$data[] = $row;
		}

		$draw = null;
		if (isset($_POST['draw'])) {
			$draw = $_POST['draw'];
		}

		$output = [
			'draw' => $draw,
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($output, JSON_PRETTY_PRINT));
	}

	public function get_respon($id, $step)
	{
		$iduser = $this->session->userdata('user_id');
		$q = "SELECT *from thonor_approval AP
			WHERE AP.st_aktif='1' AND AP.iduser='$iduser' AND AP.idhead='$id'";
		return $this->db->query($q)->result();
	}

	public function list_user($id)
	{
		$q = "SELECT *FROM thonor_approval H WHERE H.idhead='$id'";
		$row = $this->db->query($q)->result();
		$content = '';
		$no = 1;
		$no_step = '1';
		foreach ($row as $r) {
			$st_user_lain = '';
			$content .= '<tr class="' . ($r->st_aktif == '1' && $r->approve == '0' ? 'success' : 'active') . '">';
			if ($no_step != $r->step) {
				$no_step = $r->step;
			}
			if ($no_step % 2) {
				$content .= '<td> <span class="label label-default" data-toggle="tooltip">STEP ' . $r->step . '</span> <br><br>' . ($r->proses_setuju == '1' ? '<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>' : '') . ' ' . ($r->proses_tolak == '1' ? '<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>' : '') . '</td>';
			} else {
				$content .= '<td> <span class="label label-primary" data-toggle="tooltip">STEP ' . $r->step . '</span> <br><br>' . ($r->proses_setuju == '1' ? '<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>' : '') . ' ' . ($r->proses_tolak == '1' ? '<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>' : '') . '</td>';
				// $content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span></td>';
			}
			if ($r->approve != '0') {
				if ($r->iduser != $r->user_eksekusi) {
					$st_user_lain = ' <span class="label label-warning" data-toggle="tooltip">Oleh User Lain</span>';
				}
			}
			$content .= '<td><i class="si si-users fa-2x text-black-op"></i>  ' . $r->user_nama . '</td>';
			$content .= '<td>' . ($r->st_aktif == '1' ? status_approval($r->approve) : '') . ' ' . $st_user_lain . '</td>';
			$content .= '</tr>';
		}
		$arr['detail'] = $content;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($content));
	}

	public function setuju_batal($id, $status, $idrka = '')
	{
		// $arr=array();
		$q = "call update_thonor_approval('$id', $status) ";
		$result = $this->db->query($q);
		// // $arr['st_insert']=$result;
		// $arr=$this->db->query("SELECT H.st_bendahara FROM rka_pengajuan H  WHERE H.id='$idrka' AND H.status='2'")->row('st_bendahara');
		$this->output->set_output(json_encode($result));
	}

	public function tolak()
	{
		$id = $this->input->post('id_approval');
		$alasan_tolak = $this->input->post('alasan_tolak');
		$q = "call update_thonor_approval('$id', 2) ";
		$this->db->query($q);
		$result = $this->db->update('thonor_approval', ['alasan_tolak' => $alasan_tolak], ['id' => $id]);
		echo json_encode($result);
	}

	// Upload Dokumen
	public function upload_document($idapproval)
	{
		$row = $this->model->getHeadDokumenUpload($idapproval);

		$data = [
			'idapproval' => $row->id,
			'notransaksi' => $row->notransaksi,
			'tanggal_pengajuan' => $row->tanggal_pengajuan,
		];

		$data['error'] = '';
		$data['title'] = 'Upload Dokumen';
		$data['content'] = 'Thonor_approval/upload_dokumen';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Approval Honor Dokter', 'Thonor_approval/index'],
			['Upload Dokumen', '#']
		];

		$data['listFiles'] = $this->model->getListUploadedDocument($idapproval);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function upload_files()
	{
		if (!file_exists('assets/upload/honor_approval')) {
			mkdir('assets/upload/honor_approval', 0755, true);
		}

		$files = [];
		foreach ($_FILES as $file) {
			if ($file['name'] != '') {
				$config['upload_path'] = './assets/upload/honor_approval/';
				$config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';

				$files[] = $file['name'];

				$this->upload->initialize($config);

				if ($this->upload->do_upload('file')) {
					$file_upload = $this->upload->data();

					$data = [];
					$data['idapproval'] = $this->input->post('idapproval');
					$data['filename'] = $file_upload['file_name'];
					$data['size'] = formatSizeUnits($file['size']);

					$this->db->insert('thonor_approval_dokumen', $data);

					return true;
				} else {
					print_r($this->upload->display_errors());
					exit();

					return false;
				}
			} else {
				return true;
			}
		}
	}

	public function delete_file($idfile)
	{
		$this->db->where('id', $idfile);
		$query = $this->db->get('thonor_approval_dokumen');
		$row = $query->row();

		if ($query->num_rows() > 0) {
			$this->db->where('id', $idfile);
			if ($this->db->delete('thonor_approval_dokumen')) {
				if (file_exists('./assets/upload/honor_approval/' . $row->filename) && $row->filename != '') {
					unlink('./assets/upload/honor_approval/' . $row->filename);
				}
			}
		}

		return true;
	}
}
