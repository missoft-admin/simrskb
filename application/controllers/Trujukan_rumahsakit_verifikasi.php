<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Trujukan_rumahsakit_verifikasi extends CI_Controller
{
	/**
	 * Rujukan RS Verifikasi controller.
	 * Developer @gunalirezqimauludi & @muhamadalinurdin
	 */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Trujukan_rumahsakit_verifikasi_model', 'model');
		$this->load->model('Tpoliklinik_verifikasi_model');
		$this->load->model('Tkasir_model');
	}

	public function index()
	{
		$data = [
			'uri' => $this->uri->segment(1),
			'tipetransaksi' => 'index_refund',
			'tanggal_pendaftaran_awal' => date('d/m/Y'),
			'tanggal_pendaftaran_akhir' => date('d/m/Y'),
			'nomedrec' => '',
			'namapasien' => '',
			'idlayanan' => '',
			'idasalpasien' => '',
			'idrujukan' => '',
			'tanggal_pembayaran_awal' => '',
			'tanggal_pembayaran_akhir' => '',
		];

		$data['error'] = '';
		$data['title'] = 'Rujukan RS Verifikasi';
		$data['content'] = 'Trujukan_rumahsakit_verifikasi/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rujukan RS Verifikasi', 'trujukan_rumahsakit_verifikasi/index'],
			['List', '#']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter()
	{
		$data = [
			'uri' => $this->uri->segment(1),
			'tipetransaksi' => 'filter',
			'tanggal_pendaftaran_awal' => $this->input->post('tanggal_pendaftaran_awal'),
			'tanggal_pendaftaran_akhir' => $this->input->post('tanggal_pendaftaran_akhir'),
			'nomedrec' => $this->input->post('nomedrec'),
			'namapasien' => $this->input->post('namapasien'),
			'idlayanan' => $this->input->post('idlayanan'),
			'idasalpasien' => $this->input->post('idasalpasien'),
			'idrujukan' => $this->input->post('idrujukan'),
			'tanggal_pembayaran_awal' => $this->input->post('tanggal_pembayaran_awal'),
			'tanggal_pembayaran_akhir' => $this->input->post('tanggal_pembayaran_akhir'),
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Rujukan RS Verifikasi';
		$data['content'] = 'Trujukan_rumahsakit_verifikasi/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rujukan RS Verifikasi', 'trujukan_rumahsakit_verifikasi/index'],
			['List', '#']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function detail($idpendaftaran)
	{
		$data = [
			'id' => $idpendaftaran,
		];

		$dataPendaftaran = $this->Tkasir_model->get_header_poli($idpendaftaran);
		if ($dataPendaftaran) {
			$data['nopendaftaran'] = $dataPendaftaran->nopendaftaran;
			$data['nomedrec'] = $dataPendaftaran->no_medrec;
			$data['namapasien'] = $dataPendaftaran->nama;
			$data['idtipepasien'] = $dataPendaftaran->idtipepasien;
			$data['tipependaftaran'] = ($dataPendaftaran->idtipe == '1' ? 'Poliklinik' : 'Instalasi Gawat Darurat');
			$data['poliklinik'] = $dataPendaftaran->poli;
			$data['idkelompokpasien'] = $dataPendaftaran->idkelompokpasien;
			$data['kelompokpasien'] = $dataPendaftaran->kelompok;
			$data['bpjs_kesehatan'] = $dataPendaftaran->tarif_bpjs_kes;
			$data['bpjs_ketenagakerjaan'] = $dataPendaftaran->tarif_bpjs_ket;
			$data['namarekanan'] = $dataPendaftaran->rekanan;
			$data['usertransaksi'] = $dataPendaftaran->nama;
			$data['tanggaltransaksi'] = $dataPendaftaran->tanggaldaftar;
			$data['namadokter'] = $dataPendaftaran->dokter;
		}

		$dataTindakan = $this->model->getTindakan($idpendaftaran);
		if ($dataTindakan) {
			$data['idpendaftaran'] = $dataTindakan->idpendaftaran;
			$data['idkategori'] = $dataTindakan->idkategori;
			$data['jenis_berekanan'] = $dataTindakan->jenis_berekanan;
			$data['jumlah'] = $dataTindakan->jumlah;
			$data['persentase'] = $dataTindakan->persentase;
			$data['ratetarif'] = $dataTindakan->ratetarif;
			$data['tentukantarif'] = $dataTindakan->tentukantarif;
			$data['alkes_poli'] = $dataTindakan->alkes_poli;
			$data['obat_poli'] = $dataTindakan->obat_poli;
			$data['alkes_farmasi'] = $dataTindakan->alkes_farmasi;
			$data['obat_farmasi'] = $dataTindakan->obat_farmasi;
		}

		$data['error'] = '';
		$data['title'] = 'Detail Fee Rujukan';
		$data['content'] = 'Trujukan_rumahsakit_verifikasi/detail';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rujukan RS Verifikasi', 'trujukan_rumahsakit_verifikasi/index'],
			['Detail Transaksi', '#']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function getIndex($uri)
	{
		$this->select = ['*'];
		$this->from = 'view_index_verifikasi_transaksi_rujukan';
		$this->join = [];

		$this->order = [
			'tanggal' => 'DESC'
		];
		$this->group = [];

		// FILTER
		$this->where = [];
		if ($uri == 'filter_rujukan') {
			if ($this->session->userdata('tanggal_pendaftaran_awal') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggalpendaftaran) >=' => YMDFormat($this->session->userdata('tanggal_pendaftaran_awal'))]);
			}
			if ($this->session->userdata('tanggal_pendaftaran_akhir') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggalpendaftaran) <=' => YMDFormat($this->session->userdata('tanggal_pendaftaran_akhir'))]);
			}
			if ($this->session->userdata('tanggal_pembayaran_awal') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal) >=' => YMDFormat($this->session->userdata('tanggal_pembayaran_awal'))]);
			}
			if ($this->session->userdata('tanggal_pembayaran_akhir') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal) <=' => YMDFormat($this->session->userdata('tanggal_pembayaran_akhir'))]);
			}
			if ($this->session->userdata('nomedrec') != '') {
				$this->where = array_merge($this->where, ['nomedrec' => $this->session->userdata('nomedrec')]);
			}
			if ($this->session->userdata('namapasien') != '') {
				$this->where = array_merge($this->where, ['namapasien LIKE' => '%' . $this->session->userdata('namapasien') . '%']);
			}
			if ($this->session->userdata('idlayanan') != 0) {
				$this->where = array_merge($this->where, ['idtipe' => $this->session->userdata('idlayanan')]);
			}
			if ($this->session->userdata('idasalpasien') != 0) {
				$this->where = array_merge($this->where, ['idasalpasien' => $this->session->userdata('idasalpasien')]);
			}
			if ($this->session->userdata('idrujukan') != 0) {
				$this->where = array_merge($this->where, ['idrujukan' => $this->session->userdata('idrujukan')]);
			}
		} else {
			if ($this->session->userdata('tanggal_pendaftaran_awal') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggalpendaftaran) >=' => YMDFormat($this->session->userdata('tanggal_pendaftaran_awal'))]);
			} elseif ($this->session->userdata('tanggal_pendaftaran_akhir') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggalpendaftaran) <=' => YMDFormat($this->session->userdata('tanggal_pendaftaran_akhir'))]);
			} elseif ($this->session->userdata('tanggal_pembayaran_awal') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal) >=' => YMDFormat($this->session->userdata('tanggal_pembayaran_awal'))]);
			} elseif ($this->session->userdata('tanggal_pembayaran_akhir') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal) <=' => YMDFormat($this->session->userdata('tanggal_pembayaran_akhir'))]);
			} else {
				$this->where = array_merge($this->where, ['DATE(tanggal)' => date('Y-m-d')]);
			}
		}

		$this->column_search = ['nopendaftaran', 'tipe', 'nomedrec', 'namapasien'];

		$this->column_order = ['nopendaftaran', 'tipe', 'nomedrec', 'namapasien'];

		$list = $this->datatable->get_datatables();

		$data = [];
		foreach ($list as $row) {

			$nominalRujukan = $this->model->GetNominalRujukanRS($row);

			if ($row->idasalpasien == 2) {
				$idtiperujukan = 1;
			} else if ($row->idasalpasien == 3) {
				$idtiperujukan = 2;
			} else {
				$idtiperujukan = 0;
			}

			$tanggal_pembayaran = ($row->periode_pembayaran != '' ? $row->periode_pembayaran : getPembayaranFeeRujukanRS($idtiperujukan, $row->idrujukan));
			$tanggal_jatuhtempo = ($row->periode_jatuhtempo != '' ? $row->periode_jatuhtempo : getJatuhTempoFeeRujukanRS($idtiperujukan, $row->idrujukan));
			$tanggal_pembayaran_label = ($tanggal_pembayaran != 'null' && $tanggal_pembayaran != '-' ? $tanggal_pembayaran : '<span class="label label-xs label-warning" style="font-size: 11px;">Belum di Setting</span>');

			$action = '<div class="btn-group">';
			if ($row->status_verifikasi_feers == 0) {
				$action .= '<a href="#" 
							data-idkasir="' . $row->id . '" 
							data-idpendaftaran="' . $row->idpendaftaran . '" 
							data-idasalpasien="' . $row->idasalpasien . '" 
							data-nomedrec="' . ($row->nomedrec ? $row->nomedrec : '-') . '" 
							data-namapasien="' . $row->namapasien . '" 
							data-idrujukan="' . $row->idrujukan . '" 
							data-tanggalpembayaran="' . $tanggal_pembayaran . '" 
							title="Edit Tanggal Pembayaran" data-toggle="modal" data-target="#EditTanggalPembayaranModal" class="btn btn-primary btn-sm editTanggalPembayaran"><i class="fa fa-pencil"></i></a>';
			}
			
			$action .= '<a href="' . site_url() . 'trujukan_rumahsakit_verifikasi/detail/' . $row->idpendaftaran . '" target="_blank" title="Detail" class="btn btn-danger btn-sm"><i class="fa fa-list"></i></a>';
			
			if ($row->status_verifikasi_feers == 0) {
				if ($tanggal_pembayaran != 'null' && $tanggal_pembayaran != '-') {
					$action .= '<a href="' . site_url() . 'trujukan_rumahsakit_verifikasi/updateStatus/' . $row->id . '" title="Rujukan RS Verifikasi" class="btn btn-success btn-sm"><i class="fa fa-check"></i></a>';
				}
			}

			$action .= '<a href="' . site_url() . 'trujukan_rumahsakit_verifikasi/print/' . $row->id . '" target="_blank" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
			$action .= '</div>';

			$rows = [];
			$rows[] = '<label class="css-input css-checkbox css-checkbox-success"><input type="checkbox" class="checkboxTrx" name="check_status[]" value="' . $row->id . '"><span></span></label>';
			$rows[] = $row->tanggal;
			$rows[] = ($row->nomedrec ? $row->nomedrec : '-');
			$rows[] = $row->namapasien;
			$rows[] = GetJenisLayananBerkas($row->idlayanan);
			$rows[] = $row->asalpasien;
			$rows[] = $row->namarujukan;
			$rows[] = StatusRekanan($row->statusrekanan);
			$rows[] = $tanggal_pembayaran_label;
			$rows[] = number_format($row->jumlah);
			$rows[] = number_format($nominalRujukan);
			$rows[] = StatusKasirVerifikasi($row->status_verifikasi_feers);
			$rows[] = $action;

			$data[] = $rows;
		}

		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data,
		];

		echo json_encode($output);
	}

	public function GetPeriodePembayaranFeeRujukanRS($idasalpasien, $idrujukan, $tanggal = '', $bulan = '', $tahun = '')
	{
		if ($tanggal && $bulan && $tahun) {
			$tanggalPembayaranSelected = $tanggal . '/' . $bulan . '/' . $tahun;
		} else {
			$tanggalPembayaranSelected = '';
		}

		if ($idasalpasien == 2) {
			$idtiperujukan = 1;
		} elseif ($idasalpasien == 3) {
			$idtiperujukan = 2;
		} else {
			$idtiperujukan = 0;
		}

		$query = $this->db->query("SELECT DATE_FORMAT(result.xtanggal_next_pembayaran,'%d/%m/%Y') AS tanggal_pembayaran,DATE_FORMAT(result.xtanggal_next_jatuhtempo,'%d/%m/%Y') AS tanggal_jatuhtempo FROM (
		SELECT mfeerujukan_setting.*,tfeerujukan_rumahsakit.tanggal_pembayaran,tfeerujukan_rumahsakit.tanggal_jatuhtempo FROM (
		SELECT CONCAT(YEAR (CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE (),'%m'),'-',LPAD(msetting_pembayaran_rumahsakit_jatuhtempo.tanggal,2,'0')) AS xtanggal_pembayaran,(CASE WHEN (
		SELECT xtanggal_pembayaran)< CURRENT_DATE THEN DATE_ADD((
		SELECT xtanggal_pembayaran),INTERVAL 1 MONTH) ELSE (
		SELECT xtanggal_pembayaran) END) AS xtanggal_next_pembayaran,CONCAT(YEAR (CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE (),'%m'),'-',LPAD(msetting_pembayaran_rumahsakit_jatuhtempo.jatuhtempo,2,'0')) AS xtanggal_jatuh_tempo,(CASE WHEN (
		SELECT xtanggal_jatuh_tempo)< CURRENT_DATE THEN DATE_ADD((
		SELECT xtanggal_jatuh_tempo),INTERVAL 1 MONTH) ELSE (
		SELECT xtanggal_jatuh_tempo) END) AS xtanggal_next_jatuhtempo FROM msetting_pembayaran_rumahsakit_jatuhtempo LEFT JOIN msetting_pembayaran_rumahsakit_rujukan ON msetting_pembayaran_rumahsakit_rujukan.idsetting=msetting_pembayaran_rumahsakit_jatuhtempo.idsetting WHERE msetting_pembayaran_rumahsakit_rujukan.tiperujukan=$idtiperujukan AND msetting_pembayaran_rumahsakit_rujukan.idrujukan=$idrujukan AND msetting_pembayaran_rumahsakit_jatuhtempo.STATUS='1') AS mfeerujukan_setting LEFT JOIN tfeerujukan_rumahsakit ON mfeerujukan_setting.xtanggal_next_pembayaran=tfeerujukan_rumahsakit.tanggal_pembayaran AND tfeerujukan_rumahsakit.idtipe=$idtiperujukan AND tfeerujukan_rumahsakit.idrujukan=$idrujukan AND tfeerujukan_rumahsakit.status_stop=1 ORDER BY mfeerujukan_setting.xtanggal_next_pembayaran ASC) result WHERE result.tanggal_pembayaran IS NULL");

		$result = $query->result();

		$option = '';
		foreach ($result as $row) {
			$option .= '<option value="' . $row->tanggal_pembayaran . '" data-jatuhtempo="' . $row->tanggal_jatuhtempo . '" ' . ($row->tanggal_pembayaran == $tanggalPembayaranSelected ? 'selected' : '') . '>' . $row->tanggal_pembayaran . '</option>';
		}

		$arr['list_option'] = $option;
		$this->output->set_output(json_encode($arr));
	}

	public function updateData()
	{
		$idkasir = $_POST['idkasir'];
		$submitWithVerifikasi = $_POST['submitWithVerifikasi'];

		if ($this->model->updateData()) {
			if ($submitWithVerifikasi) {
				$this->updateStatus($idkasir);
			} else {
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'Data berhasil dirubah.');
				redirect('trujukan_rumahsakit_verifikasi', 'location');
			}
		}
	}

	public function updateStatus($idkasir)
	{
		$this->db->select("tpoliklinik_pendaftaran.id AS idpendaftaran,
			tpoliklinik_pendaftaran.no_medrec AS nomedrec,
			tpoliklinik_pendaftaran.namapasien,
			tpoliklinik_pendaftaran.idtipe AS idlayanan,
			(
				CASE 
					WHEN tpoliklinik_pendaftaran.idtipe = '1' THEN 'Rawat Jalan'
					WHEN tpoliklinik_pendaftaran.idtipe = '2' THEN 'Instalasi Gawat Darurat (IGD)'
				END
			) AS layanan,
			tpoliklinik_pendaftaran.idasalpasien,
			mpasien_asal.nama AS asalpasien,
			tpoliklinik_pendaftaran.idrujukan,
			mrumahsakit.nama AS namarujukan,
			tkasir.bayar AS jumlah,
			tpoliklinik_tindakan.id AS idtindakan,
			tkasir.id AS idkasir,
			tpoliklinik_pendaftaran.tanggaldaftar AS tanggal_pemeriksaan,
			tpoliklinik_pendaftaran.periode_pembayaran_feers AS periode_pembayaran,
			tpoliklinik_pendaftaran.periode_jatuhtempo_feers AS periode_jatuhtempo,
			mrumahsakit.idkategori,
			mrumahsakit.jenis_berekanan,
			mrumahsakit.persentase,
			mrumahsakit.ratetarif,
			mrumahsakit.tentukantarif,
			mrumahsakit.alkes_poli,
			mrumahsakit.obat_poli,
			mrumahsakit.alkes_farmasi,
			mrumahsakit.obat_farmasi
		");

		$this->db->join('tpoliklinik_tindakan', 'tpoliklinik_tindakan.id = tkasir.idtindakan AND idtipe IN (1,2)');
		$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran');
		$this->db->join('mpasien_asal', 'mpasien_asal.id = tpoliklinik_pendaftaran.idasalpasien', 'LEFT');
		$this->db->join('mrumahsakit', 'mrumahsakit.id = tpoliklinik_pendaftaran.idrujukan', 'LEFT');
		$this->db->where('tkasir.id', $idkasir);
		$this->db->where('tkasir.status', 2);
		$this->db->limit(1);
		$query = $this->db->get('tkasir');
		$row = $query->row();

		if ($row->idasalpasien == 2) {
			$idtiperujukan = 1;
		} else if ($row->idasalpasien == 3) {
			$idtiperujukan = 2;
		} else {
			$idtiperujukan = 0;
		}
		
		$dataPendaftaran = [
			'idtransaksi' => $row->idpendaftaran,
			'nomedrec' => $row->nomedrec,
			'namapasien' => $row->namapasien,
			'idlayanan' => $row->idlayanan,
			'layanan' => $row->layanan,
			'idasalpasien' => $row->idasalpasien,
			'asalpasien' => $row->asalpasien,
			'idrujukan' => $row->idrujukan,
			'idtiperujukan' => $idtiperujukan,
			'namarujukan' => $row->namarujukan,
			'totaltransaksi' => $row->jumlah,
			'nominalrujukan' => $this->model->GetNominalRujukanRS($row),
			'tanggal_pemeriksaan' => $row->tanggal_pemeriksaan,
			'periode_pembayaran' => $row->periode_pembayaran,
			'periode_jatuhtempo' => $row->periode_jatuhtempo
		];

		if ($row->idpendaftaran) {
			$this->model->generateTransaksiRujukanRS($dataPendaftaran);
		}

		$this->model->updateStatusVerifikasi($row->idpendaftaran);

		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'Status Verifikasi berhasil dirubah.');
		redirect('trujukan_rumahsakit_verifikasi', 'location');
	}
}
