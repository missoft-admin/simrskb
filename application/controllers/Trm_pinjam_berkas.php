<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_pinjam_berkas extends CI_Controller {

    /**
     * Pinjam Berkas Rekam Medis controller
     * Developer Acep Kursina
     */

    public function __construct() {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('trm_pinjam_berkas_model','model');
    }
    
    public function index() {
        // print_r(date('d'));exit();
		
			$data=array();
			
			$data['error'] = '';
			$data['title'] = 'Pinjam Berkas Rekam Medis';
			$data['content'] = 'Trm_pinjam_berkas/index';
			$data['tombol'] = 'index';
			$data['breadcrum'] = [
				["RSKB Halmahera",'#'],
				["Pinjam Berkas Rekam Medis",'trm_pinjam_berkas/index'],
				["List",'#']
			];

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		
    }
	function simpan(){
		// print_r($this->input->post());exit();
		$idpasien=$this->input->post('idpasien_detail');
		foreach ($idpasien as $key => $value){
			$row=$this->model->get_pasien_detail($value);
			$data=array(
				'id_trx'=>'',
				'tanggal_trx'=>date('Y-m-d H:i:s'),
				'jenis_pelayanan'=>'2',
				'tujuan'=>'5',
				'idpasien'=>$value,
				'no_medrec'=>$row->no_medrec,
				'title'=>$row->title,
				'namapasien'=>$row->nama,
				'tanggal_lahir'=>$row->tanggal_lahir,
				'alamat'=>$row->alamat_jalan,
				'statuspasienbaru'=>'0',
				'idpoliklinik'=>'0',
				'iddokter'=>'0',
				'kelas'=>'0',
				'kelas'=>'0',
				'bed'=>'0',
				'status_kirim'=>'0',
				'user_trx'=>$this->session->userdata('user_id'),
				'user_peminjam_id'=>$this->input->post('iddokter'),
				'tipe_user_peminjam'=>$this->input->post('idtipe'),
				'catatan'=>$this->input->post('tcatatan'),
				'status'=>'1',
			);	
			// print_r($data);exit();
			$this->db->insert('trm_layanan_berkas',$data);
		}
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'Data Berhasil Tersimpan !');
		redirect('trm_pinjam_berkas', 'location');
			
	}
	function js_pasien()
	{
		$arr = $this->model->js_pasien();
		echo json_encode($arr);
	}
	function get_pasien($id)
	{
		$arr = $this->model->get_pasien($id);
		echo json_encode($arr);
	}
	function get_dokter_pegawai()
	{
		$arr = $this->model->get_dokter_pegawai();
		echo json_encode($arr);
	}
	//HAPUS
	
	
}
