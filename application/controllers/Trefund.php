<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use Dompdf\Dompdf;

class Trefund extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Trefund_model', 'model');
		$this->load->model('Tpoliklinik_tindakan_model');
		$this->load->model('Tpoliklinik_verifikasi_model');
		$this->load->model('Trawatinap_verifikasi_model');
	}

	public function index()
	{
		$data = [
			'nomedrec' => '',
			'namapasien' => '',
			'tiperefund' => '',
			'tanggalawal' => date('d/m/Y'),
			'tanggalakhir' => date('d/m/Y')
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Refund';
		$data['content'] = 'Trefund/index';
		$data['breadcrum'] = [['RSKB Halmahera', '#'], ['Refund', '#']];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function manage()
	{
		$data['error'] = '';
		$data['title'] = 'Tambah Refund';
		$data['content'] = 'Trefund/manage';
		$data['breadcrum'] = [['RSKB Halmahera', '#'], ['Refund', '#'], ['Tambah', '#']];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter()
	{
		$data = [
			'nomedrec' => $this->input->post('nomedrec'),
			'namapasien' => $this->input->post('namapasien'),
			'tiperefund' => $this->input->post('tiperefund'),
			'tanggalawal' => $this->input->post('tanggal_refund_awal'),
			'tanggalakhir' => $this->input->post('tanggal_refund_akhir')
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Refund';
		$data['content'] = 'Trefund/index';
		$data['breadcrum'] = [['RSKB Halmahera', '#'], ['Refund', '#']];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function showAllIndex()
	{
		$nomedrec = $this->session->userdata('nomedrec');
		$namapasien = $this->session->userdata('namapasien');
		$tiperefund = $this->session->userdata('tiperefund');
		$tanggalawal = $this->session->userdata('tanggalawal');
		$tanggalakhir = $this->session->userdata('tanggalakhir');

		$result = $this->model->getAllIndex($nomedrec, $namapasien, $tiperefund, $tanggalawal, $tanggalakhir);

		$data = [];
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		$no = 1;
		foreach ($result as $row) {
			$rows = [];
			$aksi = '';
			if (UserAccesForm($user_acces_form, ['1097'])) {
				if ($row->st_transaksi == '0') {
					if ($row->st_verifikasi == '0') {
						$aksi = '<button class="btn btn-xs btn-primary edit-refund" data-idtransaksi="' . $row->id . '" data-norefund="' . $row->norefund . '" data-notransaksi="' . $row->notransaksi . '" data-nominalrefund="' . number_format($row->totalrefund) . '"><i class="fa fa-pencil"></i></button>&nbsp;';
						$aksi .= '<button class="btn btn-xs btn-danger show-refund" data-idtransaksi="' . $row->id . '"><i class="fa fa-trash"></i></button>&nbsp;';
					}else{
						$aksi .= '<label class="label label-success text-uppercase">SUDAH DIVERIFIKASI</label> <br>';
						
					}
				} else {
					$aksi .= '<label class="label label-success text-uppercase">SUDAH DI PROSES</label> <br>';
				}
			}

			switch ($row->tipe) {
				case 0:
					$tipe = '<label class="label label-primary text-uppercase">Deposit</label>';
					if (UserAccesForm($user_acces_form, ['1098'])) {
						$aksi .= '<a href="' . base_url() . 'Trefund/fakturRefundDeposit/' . $row->id . '" class="btn btn-xs btn-primary print-refund" target="_blank"><i class="fa fa-print"></i></a>';
					}
					break;
				case 1:
					$tipe = '<label class="label label-primary text-uppercase">Retur Obat</label>';
					if (UserAccesForm($user_acces_form, ['1098'])) {
						$aksi .= '<a href="' . base_url() . 'Trefund/fakturRefundObat/' . $row->id . '" class="btn btn-xs btn-primary print-refund" target="_blank"><i class="fa fa-print"></i></a>';
					}
					break;
				case 2:
					$tipe = '<label class="label label-primary text-uppercase">Transaksi</label>';
					if (UserAccesForm($user_acces_form, ['1098'])) {
						$aksi .= '<a href="' . base_url() . 'Trefund/fakturRefundTransaksi/' . $row->id . '" class="btn btn-xs btn-primary print-refund" target="_blank"><i class="fa fa-print"></i></a>';
					}
					break;
			}
			$rows[] = $no++;
			$rows[] = date('d/m/Y h:i', strtotime($row->tanggal));
			$rows[] = $row->norefund;
			$rows[] = $row->notransaksi;
			$rows[] = $tipe;
			$rows[] = $row->nomedrec;
			$rows[] = $row->namapasien;
			$rows[] = number_format($row->totalrefund);
			$rows[] = $aksi;

			$data[] = $rows;
		}

		$output = [
			'recordsTotal' => count($result),
			'recordsFiltered' => count($result),
			'data' => $data
		];

		echo json_encode($output);
	}

	public function saveRefund()
	{
		$data = [
			'tanggal' => date('Y-m-d h:i:s', strtotime(YMDFormat($this->input->post('tanggal')) . ' ' . $this->input->post('jam'))),
			'tipe' => $this->input->post('tiperefund'),
			'tipetransaksi' => $this->input->post('tipetransaksi'),
			'idtransaksi' => $this->input->post('idtransaksi'),
			'notransaksi' => $this->input->post('notransaksi'),
			'idpasien' => $this->input->post('idpasien'),
			'idkelompokpasien' => $this->input->post('idkelompokpasien'),
			'metode' => $this->input->post('metode'),
			'totaltransaksi' => RemoveComma($this->input->post('totaltransaksi')),
			'totalpembayaran' => RemoveComma($this->input->post('totalpembayaran')),
			'totaldeposit' => RemoveComma($this->input->post('totaldeposit')),
			'totalrefund' => RemoveComma($this->input->post('totalrefund')),
			'bank' => $this->input->post('bank'),
			'norekening' => $this->input->post('norekening'),
			'nominal' => RemoveComma($this->input->post('nominal')),
			'alasan' => $this->input->post('alasan'),
			'created_by' => $this->session->userdata('user_id'),
			'transaksi_by' => $this->session->userdata('user_id'),
			'transaksi_nama' => $this->session->userdata('user_name'),
			'transaksi_date' => date('Y-m-d H:i:s'),
		];

		if ($this->db->insert('trefund', $data)) {
			$detail = json_decode($this->input->post('detail'));
			$idrefund = $this->db->insert_id();

			foreach ($detail as $row) {
				$data = [];
				$data['idrefund'] = $idrefund;
				if ($this->input->post('tiperefund') != '2') {
					$data['reference'] = $row[0];
					$data['idtransaksi'] = $row[1];
				} else {
					$data['reference'] = $row->reference;
					$data['idtransaksi'] = $row->iddetail;
					$data['tanggal'] = $row->tanggal;
					$data['tipe'] = $row->tipe;
					
					$data['idtarif'] = $row->idtarif;
					$data['tindakan'] = $row->tindakan;

					$data['jasasarana'] = $row->jasasarana;
					$data['jasasarana_disc'] = $row->jasasarana_disc;

					$data['jasapelayanan'] = $row->jasapelayanan;
					$data['jasapelayanan_disc'] = $row->jasapelayanan_disc;

					$data['bhp'] = $row->bhp;
					$data['bhp_disc'] = $row->bhp_disc;

					$data['biayaperawatan'] = $row->biayaperawatan;
					$data['biayaperawatan_disc'] = $row->biayaperawatan_disc;

					$data['total'] = $row->total;
					$data['kuantitas'] = $row->kuantitas;
					$data['diskon'] = $row->diskon;
					$data['totalkeseluruhan'] = $row->totalkeseluruhan;
				}

				$this->db->insert('trefund_detail', $data);
			}

			$status = ['status' => 200];
			$this->output->set_output(json_encode($status));
		}
	}

	// public function saveRefundTransaksi()
	// {
	// 	$detail = $this->input->post('details');

	// 	$data = [
	// 		'idtransaksi' => $this->input->post('idtransaksi'),
	// 		'tanggal' => date('Y-m-d', strtotime($this->input->post('tanggal_refund'))),
	// 		'noregistrasi' => $this->input->post('no_registrasi'),
	// 		'nomedrec' => $this->input->post('no_medrec'),
	// 		'tipe_refund' => $this->input->post('tipe_refund'),
	// 		'metode_refund' => $this->input->post('metode_refund'),
	// 		'bank_refund' => $this->input->post('bank_refund'),
	// 		'norek_refund' => $this->input->post('norek_refund'),
	// 		'nominal_refund' => $this->input->post('nominal_refund'),
	// 		'total' => 0,
	// 		'total_transaksi' => 0,
	// 		'alasan' => $this->input->post('alasan'),
	// 		'user_created' => $this->session->userdata('user_id'),
	// 		'status' => 1
	// 	];

	// 	$result = $this->model->saveRefundTransaksi($data, $detail);

	// 	if ($result) {
	// 		$status = ['status' => 200];
	// 		$this->output->set_output(json_encode($status));
	// 	}
	// }

	public function removeRefund($id)
	{
		$alasan = $this->input->post('alasan');
		$result = $this->model->removeRefund($id, $alasan);

		if ($result) {
			$status = ['status' => 200];
			$this->output->set_output(json_encode($status));
		}
	}

	public function updatePembayaranRefund()
	{
		$this->metode = $this->input->post('metode');
		$this->bank = $this->input->post('bank');
		$this->norekening = $this->input->post('norekening');
		$this->alasan = $this->input->post('alasan');

		if ($this->db->update('trefund', $this, ['id' => $this->input->post('idtransaksi')])) {
			$status = ['status' => 200];
			$this->output->set_output(json_encode($status));
		}
	}

	public function searchList()
	{
		$tiperefund = $this->input->post('tiperefund');
		$tanggalawal = YMDFormat($this->input->post('tanggalawal'));
		$tanggalakhir = YMDFormat($this->input->post('tanggalakhir'));
		$idtipetransaksi = $this->input->post('idtipetransaksi');

		$data = [];

		if ($tiperefund == '0') {
			$result = $this->model->getRefundDeposit($tanggalawal, $tanggalakhir, $idtipetransaksi);
		} elseif ($tiperefund == '1') {
			$result = $this->model->getRefundObat($tanggalawal, $tanggalakhir, $idtipetransaksi);
		} elseif ($tiperefund == '2') {
			$result = $this->model->getRefundTransaksi($tanggalawal, $tanggalakhir, $idtipetransaksi);
		}

		foreach ($result as $row) {
			$rows = [];

			$rows[] = '<label class="label label-primary text-uppercase">' . $row->tipetransaksi . '</label>';
			;
			$rows[] = $row->notransaksi;
			$rows[] = $row->nomedrec;
			$rows[] = $row->namapasien;
			$rows[] = $row->namakelompokpasien;
			$rows[] = $row->namadokter;

			if ($tiperefund == '2') {
				$rows[] = '<button class="btn btn-success btn-xs text-uppercase selectTransaksi" style="font-size:10px;" data-reference="' . $row->reference . '" data-idtransaksi="' . $row->id . '">Pilih Transaksi</button>';
			} else {
				$rows[] = '<button class="btn btn-success btn-xs text-uppercase selectTransaksi" style="font-size:10px;" data-idtransaksi="' . $row->id . '">Pilih Transaksi</button>';
			}

			$data[] = $rows;
		}

		$output = [
			'recordsTotal' => count($result),
			'recordsFiltered' => count($result),
			'data' => $data
		];

		echo json_encode($output);
	}

	public function getInfoRefund($id)
	{
		$row = $this->model->getInfoRefund($id);

		$this->output->set_output(json_encode($row));
	}

	public function fakturRefundDeposit($id)
	{
		$dompdf = new Dompdf();
		$row = $this->model->getInfoRefund($id);

		$data = [
			'notransaksi' => $row->notransaksi,
			'tanggaltransaksi' => $row->tanggaltransaksi,
			'norefund' => $row->norefund,
			'tanggalrefund' => $row->tanggalrefund,
			'tiperefund' => $row->tiperefund,
			'nomedrec' => $row->nomedrec,
			'namapasien' => $row->namapasien,
			'kelompokpasien' => $row->kelompokpasien,
			'alasan' => $row->alasan,
			'idmetode' => $row->idmetode,
			'metode' => $row->metode,
			'bank' => $row->bank,
			'norekening' => $row->norekening,
			'totaltransaksi' => $row->totaltransaksi,
			'totaldeposit' => $row->totaldeposit,
			'totalrefund' => $row->totalrefund,
			'created_by' => $row->created_by,
			'namauser' => $row->namauser,
		];

		$data['list_deposit'] = $this->model->getFakturRefundDeposit($id);

		$html = $this->load->view('Trefund/print/faktur_refund_deposit', $data, true);

		$dompdf->loadHtml($html);
		$customPaper = [0, 0, 480, 600];
		$dompdf->setPaper($customPaper);
		$dompdf->render();
		$dompdf->stream('Faktur Refund Deposit.pdf', ['Attachment' => 0]);
	}

	public function fakturRefundObat($id)
	{
		$dompdf = new Dompdf();
		$row = $this->model->getInfoRefund($id);

		$data = [
			'notransaksi' => $row->notransaksi,
			'tanggaltransaksi' => $row->tanggaltransaksi,
			'norefund' => $row->norefund,
			'tanggalrefund' => $row->tanggalrefund,
			'tiperefund' => $row->tiperefund,
			'nomedrec' => $row->nomedrec,
			'namapasien' => $row->namapasien,
			'kelompokpasien' => $row->kelompokpasien,
			'alasan' => $row->alasan,
			'idmetode' => $row->idmetode,
			'metode' => $row->metode,
			'bank' => $row->bank,
			'norekening' => $row->norekening,
			'totaltransaksi' => $row->totaltransaksi,
			'totaldeposit' => $row->totaldeposit,
			'totalrefund' => $row->totalrefund,
		];

		$data['list_obat'] = $this->model->getFakturRefundObat($id);

		$html = $this->load->view('Trefund/print/faktur_refund_obat', $data, true);

		$dompdf->loadHtml($html);
		$customPaper = [0, 0, 480, 600];
		$dompdf->setPaper($customPaper);
		$dompdf->render();
		$dompdf->stream('Faktur Refund Deposit.pdf', ['Attachment' => 0]);
	}

	public function fakturRefundTransaksi($id)
	{
		$dompdf = new Dompdf();
		$row = $this->model->getInfoRefund($id);

		$data = [
			'notransaksi' => $row->notransaksi,
			'tanggaltransaksi' => $row->tanggaltransaksi,
			'norefund' => $row->norefund,
			'tanggalrefund' => $row->tanggalrefund,
			'tiperefund' => $row->tiperefund,
			'nomedrec' => $row->nomedrec,
			'namapasien' => $row->namapasien,
			'kelompokpasien' => $row->kelompokpasien,
			'alasan' => $row->alasan,
			'idmetode' => $row->idmetode,
			'metode' => $row->metode,
			'bank' => $row->bank,
			'norekening' => $row->norekening,
			'totaltransaksi' => $row->totaltransaksi,
			'totaldeposit' => $row->totaldeposit,
			'totalrefund' => $row->totalrefund,
		];

		$data['list_transaksi'] = $this->model->getFakturRefundTransaksi($id);

		$html = $this->load->view('Trefund/print/faktur_refund_transaksi', $data, true);

		$dompdf->loadHtml($html);
		$customPaper = [0, 0, 480, 600];
		$dompdf->setPaper($customPaper);
		$dompdf->render();
		$dompdf->stream('Faktur Refund Deposit.pdf', ['Attachment' => 0]);
	}

	// Refund Deposit
	public function getRefundDeposit()
	{
		$empty = (object)['id' => '', 'text' => ''];
		$result = $this->model->getRefundDeposit();
		array_unshift($result, $empty);

		$this->output->set_output(json_encode($result));
	}

	public function getInfoRefundDeposit($id)
	{
		$row = $this->model->getInfoRefundDeposit($id);

		$this->output->set_output(json_encode($row));
	}

	// Refund Retur Obat Rawat Jalan
	public function getRefundObat()
	{
		$empty = (object)['id' => '', 'text' => ''];
		$result = $this->model->getRefundObat();
		array_unshift($result, $empty);

		$this->output->set_output(json_encode($result));
	}

	public function getInfoRefundObat($id)
	{
		$row = $this->model->getInfoRefundObat($id);

		$this->output->set_output(json_encode($row));
	}

	public function getDetailRefundObat($id)
	{
		$row = $this->model->getDetailRefundObat($id);

		$this->output->set_output(json_encode($row));
	}

	// Refund Transaksi Rawat Jalan & Rawat Inap
	public function getRefundTransaksi()
	{
		$empty = (object)['id' => '', 'text' => ''];
		$result = $this->model->getRefundTransaksi();
		array_unshift($result, $empty);

		$this->output->set_output(json_encode($result));
	}

	public function getInfoRefundTransaksi($reference, $id)
	{
		$row = $this->model->getInfoRefundTransaksi($reference, $id);

		$this->output->set_output(json_encode($row));
	}

	public function getDetailRefundTransaksiRawatJalan($id)
	{
		$data = [];
		$data['idpendaftaran'] = $id;
		$data['listPelayanan'] = $this->Tpoliklinik_verifikasi_model->viewRincianRajalTindakan($id);
		$data['listObat'] = $this->Tpoliklinik_verifikasi_model->viewRincianRajalObat($id);
		$data['listAlkes'] = $this->Tpoliklinik_verifikasi_model->viewRincianRajalAlkes($id);
		$data['listLaboratorium'] = $this->Tpoliklinik_verifikasi_model->viewRincianLaboratorium($id);
		$data['listRadiologi'] = $this->Tpoliklinik_verifikasi_model->viewRincianRadiologi($id);
		$data['listFisioterapi'] = $this->Tpoliklinik_verifikasi_model->viewRincianFisioterapi($id);
		$data['listAdministrasi'] = $this->Tpoliklinik_verifikasi_model->viewRincianAdministrasi($id);
		$data['listFarmasi'] = $this->model->getListFarmasi($id);

		$this->load->view('Trefund/transaksi_rawatjalan', $data);
	}

	public function getDetailRefundTransaksiRawatInap($id)
	{
		$data = [];
		$data['idpendaftaran'] = $id;

		$this->load->view('Trefund/transaksi_rawatinap', $data);
	}
}
