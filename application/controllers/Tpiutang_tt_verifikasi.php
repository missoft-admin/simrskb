<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tpiutang_tt_verifikasi extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpiutang_tt_verifikasi_model','model');
		$this->load->model('Tvalidasi_model');
		$this->load->model('Tpoliklinik_verifikasi_model');
		$this->load->model('Trawatinap_tindakan_model');
		$this->load->model('Trawatinap_verifikasi_model');
	}

	function index() {
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		$tgl_terakhir = date('Y-m-d', strtotime($hari_ini));
		$data=array(
			'tipe'=>'#',
			'cara_bayar'=>'#',
			'no_po'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'#',
			// 'tgl_trx1'=>$tgl_pertama,
			// 'tgl_trx2'=>date('d-m-Y'),
			
			'tgl_trx1'=>'',
			'tgl_trx2'=>'',
			'tanggaldaftar1'=>'',
			'tanggaldaftar2'=>'',
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['title'] 			= 'Verifikasi Piutang Tidak Tertagih';
		$data['content'] 		= 'Tpiutang_tt_verifikasi/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Verifikasi Transaksi",'tverifikasi_transaksi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');


		$no_reg=$this->input->post('no_reg');
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$tipe=$this->input->post('tipe');
		$status=$this->input->post('status');
		$tgl_trx1=$this->input->post('tgl_trx1');
		$tgl_trx2=$this->input->post('tgl_trx2');
		$tanggaldaftar1=$this->input->post('tanggaldaftar1');
		$tanggaldaftar2=$this->input->post('tanggaldaftar2');

		$where1='';
		$where2='';
		$where='';
		if ($no_reg !=''){
			$where1 .=" AND TP.nopendaftaran='$no_reg' ";
			$where2 .=" AND RI.nopendaftaran='$no_reg' ";
		}
		if ($no_medrec !=''){
			$where1 .=" AND TP.no_medrec='$no_medrec' ";
			$where2 .=" AND RI.no_medrec='$no_medrec' ";
		}
		if ($nama_pasien !=''){
			$where1 .=" AND TP.namapasien='$nama_pasien' ";
			$where2 .=" AND RI.namapasien='$nama_pasien' ";
		}

		if ($idkelompokpasien !='#'){
			$where1 .=" AND TK.tipekontraktor='$idkelompokpasien' ";
			$where2 .=" AND PD.tipekontraktor='$idkelompokpasien' ";
		}
		if ($idrekanan !='#'){
			$where1 .=" AND TK.idkontraktor='$idrekanan' ";
			$where2 .=" AND PD.idkontraktor='$idrekanan' ";
		}
		if ($tipe !='#'){
			$where .=" AND idtipe='$tipe' ";
		}
		if ($status !='#'){
			$where1 .=" AND TK.st_verifikasi_piutang='$status' ";
			$where2 .=" AND PD.st_verifikasi_piutang='$status' ";
		}


		if ('' != $tgl_trx1) {
            $where1 .= " AND DATE(K.tanggal) >='".YMDFormat($tgl_trx1)."' AND DATE(K.tanggal) <='".YMDFormat($tgl_trx2)."'";
            $where2 .= " AND DATE(PDH.tanggal) >='".YMDFormat($tgl_trx1)."' AND DATE(PDH.tanggal) <='".YMDFormat($tgl_trx2)."'";
        }
		if ('' != $tanggaldaftar1) {
            $where1 .= " AND DATE(TP.tanggaldaftar) >='".YMDFormat($tanggaldaftar1)."' AND DATE(TP.tanggaldaftar) <='".YMDFormat($tanggaldaftar2)."'";
            $where2 .= " AND DATE(RI.tanggaldaftar) >='".YMDFormat($tanggaldaftar1)."' AND DATE(RI.tanggaldaftar) <='".YMDFormat($tanggaldaftar2)."'";
        }
        $from = "(
					SELECT TP.id as pendaftaran_id,K.id as kasir_id, TK.id as pembayaran_id
					, TP.idtipe as idtipe,TP.nopendaftaran
					,TP.tanggaldaftar as tanggal_kunjungan,K.tanggal as tanggal_transaksi, TP.idpasien,TP.no_medrec,TP.title,TP.namapasien
					,TP.idkelompokpasien,KP.nama as kel_pasien,TP.idrekanan
					,K.total as total_transaksi,TK.nominal as tidak_tertagih,(K.total - TK.nominal) as nominal_lain
					,TK.jaminan,TK.`status`,TK.tanggal_jatuh_tempo
					,TK.st_verifikasi_piutang,S.id as setting_id,S.jatuh_tempo,DATE_ADD(CURRENT_DATE,INTERVAL S.jatuh_tempo DAY) as tgl_setting					
					from tkasir K
					INNER JOIN tkasir_pembayaran TK  ON TK.idkasir=K.id
					LEFT JOIN tpoliklinik_tindakan TD ON TD.id=K.idtindakan
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TD.idpendaftaran
					LEFT JOIN mpasien_kelompok KP ON KP.id=TP.idkelompokpasien
					LEFT JOIN msetting_piutang S ON S.idtipe=TP.idtipe
					WHERE TK.idmetode='6' AND K.`status` !='0' AND K.idtipe IN (1,2) ".$where1."
					GROUP BY TK.id
					
					UNION ALL
					
					SELECT RI.id as pendaftaran_id,PDH.id as kasir_id,PD.id as pembayaran_id,(RI.idtipe + 2) as idtipe,RI.nopendaftaran					
					,RI.tanggaldaftar as tanggal_kunjungan,PDH.tanggal as tanggal_transaksi,RI.idpasien,RI.no_medrec,RI.title,RI.namapasien
					,RI.idkelompokpasien,KP.nama as kel_pasien,RI.idrekanan
					,PDH.totalharusdibayar as total_transaksi,PD.nominal as tidak_tertagih,(PDH.totalharusdibayar-PD.nominal) as nominal_lain
					,PD.jaminan,'1' as status,PD.tanggal_jatuh_tempo,PD.st_verifikasi_piutang,S.id as setting_id,S.jatuh_tempo,DATE_ADD(CURRENT_DATE,INTERVAL S.jatuh_tempo DAY) as tgl_setting							
					FROM trawatinap_tindakan_pembayaran PDH
					LEFT JOIN trawatinap_tindakan_pembayaran_detail PD ON PDH.id=PD.idtindakan
					LEFT JOIN trawatinap_pendaftaran RI ON RI.id=PDH.idtindakan
					LEFT JOIN mpasien_kelompok KP ON KP.id=RI.idkelompokpasien
					LEFT JOIN msetting_piutang S ON S.idtipe=(RI.idtipe + 2)
					WHERE PD.idmetode='7' AND PDH.statusbatal='0' ".$where2."
					GROUP BY PD.id
				) as tbl WHERE idtipe IS NOT NULL".$where;


		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('no_medrec','namapasien','nopendaftaran');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tpiutang_tt_monitoring/');
            $url_kasir        = site_url('tkasir/');
            $url_ranap        = site_url('trawatinap_tindakan/');
            $url_batal       = site_url('tgudang_pengembalian/');
            $url_klaim       = site_url('tklaim_rincian/');
            $url_dok_rajal       = site_url('tverifikasi_transaksi/upload_document/tkasir/');
            $url_dok_ranap       = site_url('tverifikasi_transaksi/upload_document/trawatinap_tindakan_pembayaran/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
			// if ($r->no_klaim){
				// $no_klaim='<br><br><span class="label label-warning">'.$r->no_klaim.'</span>';			
			// }else{
				// $no_klaim='';				
			// }
            $row[] = $r->idtipe;
            $row[] = $r->pembayaran_id;
            $row[] = $r->pendaftaran_id;
            $row[] = $r->kasir_id;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggal_kunjungan).'<br>'.$r->nopendaftaran;
            $row[] = HumanDateShort($r->tanggal_transaksi);
            $row[] = GetTipePasienPiutang($r->idtipe);
            // $row[] = $r->nopendaftaran;
            $row[] = $r->no_medrec;
            $row[] = $r->no_medrec.'<br>'.($r->title?$r->title.'. ':'').$r->namapasien;
            $row[] = $r->kel_pasien;
            $row[] = number_format($r->total_transaksi,0);
            $row[] = number_format($r->tidak_tertagih,0);
            $row[] = number_format($r->nominal_lain,0);
            $row[] = $r->jaminan;
			$row[] = ($r->st_verifikasi_piutang=='1'?text_success('TELAH DIVERIFIKASI'):text_default('MENUGGU DIPROSES'));
			$row[] = HumanDateShort(($r->st_verifikasi_piutang?$r->tanggal_jatuh_tempo:$r->tgl_setting));
            if($r->st_verifikasi_piutang=='0'){
				$aksi .= '<button title="Verifikasi" class="btn btn-xs btn-danger verifikasi"><i class="fa fa-check"></i></button>';
			}else{
				$aksi .= '<a class="view btn btn-xs btn-warning" href="'.$url.'rincian_tagihan/'.$r->tanggal_jatuh_tempo.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-external-link-square"></i></a>';
				
			}
			
			if ($r->idtipe < 3){
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'transaksi/'.$r->pendaftaran_id.'/'.$r->kasir_id.'/1/1" target="_blank"  type="button"  title="Detail"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'kwitansi_verifikasi/'.$r->pembayaran_id.'" target="_blank"  type="button"  title="Kwitansi"><i class="fa fa-list-alt "></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'print_transaksi/'.$r->pendaftaran_id.'/'.$r->kasir_id.'" target="_blank"  type="button"  title="Rincian"><i class="fa fa-print"></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-danger" href="'.$url_dok_rajal.$r->kasir_id.'" target="_blank"  type="button"  title="Upload"><i class="fa fa-upload"></i></a>';
			}else{
				$aksi .= '<a  class="view btn btn-xs btn-success" href="'.$url_ranap.'verifikasi/'.$r->pendaftaran_id.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<div class="btn-group" role="group">
						<button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-print"></i>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li class="dropdown-header">Print Ranap</li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->pendaftaran_id.'/0" target="_blank"  > Rincian Biaya (All)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->pendaftaran_id.'/1" target="_blank"  > Rincian Biaya (Verified)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->pendaftaran_id.'/0" target="_blank"  > Rincian Global (All)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->pendaftaran_id.'/1" target="_blank"  > Rincian Global (Verified)</a></li>
							
						</ul>
					</div>';
				$aksi .= '<a class="view btn btn-xs btn-danger" href="'.$url_dok_ranap.$r->kasir_id.'" target="_blank"  type="button"  title="Upload"><i class="fa fa-upload"></i></a>';
			}
			
			

			$aksi.='</div>';
            
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	
	public function verifikasi()
    {

		$where='';
        $id = $this->input->post('id');//idpembayaran
        $idtipe = $this->input->post('idtipe');
        $idpendaftaran = $this->input->post('idpendaftaran');
        $tanggal_jt =YMDFormat($this->input->post('tanggal_jt'));
       
		if ($idtipe < 3){//Poli
			$q="SELECT TP.id as pendaftaran_id,K.id as kasir_id, TK.id as pembayaran_id
					, TP.idtipe as idtipe,TP.nopendaftaran
					,TP.tanggaldaftar as tanggal_kunjungan,K.tanggal as tanggal_transaksi, TP.idpasien,TP.no_medrec,TP.title,TP.namapasien
					,TP.idkelompokpasien,KP.nama as kel_pasien,TP.idrekanan
					,K.total as total_transaksi,TK.nominal as tidak_tertagih,(K.total - TK.nominal) as nominal_lain
					,TK.jaminan,TK.`status`,TK.tanggal_jatuh_tempo
					,TK.st_verifikasi_piutang,S.id as setting_id,S.jatuh_tempo,DATE_ADD(CURRENT_DATE,INTERVAL S.jatuh_tempo DAY) as tgl_setting					
					from tkasir K
					INNER JOIN tkasir_pembayaran TK  ON TK.idkasir=K.id
					LEFT JOIN tpoliklinik_tindakan TD ON TD.id=K.idtindakan
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TD.idpendaftaran
					LEFT JOIN mpasien_kelompok KP ON KP.id=TP.idkelompokpasien
					LEFT JOIN msetting_piutang S ON S.idtipe=TP.idtipe
					WHERE TK.idmetode='6' AND K.`status` !='0' AND K.idtipe IN (1,2) AND TK.id='$id'
					GROUP BY TK.id";
		}else{
			$q="SELECT RI.id as pendaftaran_id,PDH.id as kasir_id,PD.id as pembayaran_id,(RI.idtipe + 2) as idtipe,RI.nopendaftaran					
					,RI.tanggaldaftar as tanggal_kunjungan,PDH.tanggal as tanggal_transaksi,RI.idpasien,RI.no_medrec,RI.title,RI.namapasien
					,RI.idkelompokpasien,KP.nama as kel_pasien,RI.idrekanan
					,PDH.totalharusdibayar as total_transaksi,PD.nominal as tidak_tertagih,(PDH.totalharusdibayar-PD.nominal) as nominal_lain
					,PD.jaminan,'1' as status,PD.tanggal_jatuh_tempo,PD.st_verifikasi_piutang,S.id as setting_id,S.jatuh_tempo,DATE_ADD(CURRENT_DATE,INTERVAL S.jatuh_tempo DAY) as tgl_setting							
					FROM trawatinap_tindakan_pembayaran PDH
					LEFT JOIN trawatinap_tindakan_pembayaran_detail PD ON PDH.id=PD.idtindakan
					LEFT JOIN trawatinap_pendaftaran RI ON RI.id=PDH.idtindakan
					LEFT JOIN mpasien_kelompok KP ON KP.id=RI.idkelompokpasien
					LEFT JOIN msetting_piutang S ON S.idtipe=(RI.idtipe + 2)
					WHERE PD.idmetode='7' AND PDH.statusbatal='0' AND PD.id='$id'
					GROUP BY PD.id";
		}
		$row=$this->db->query($q)->row();
		if ($idtipe < 3){//Poli
			$this->Tvalidasi_model->GenerateValidasiPendapatanRajal($row->pendaftaran_id,$row->kasir_id,'4',date('Y-m-d'));	
		}else{
			$this->Tvalidasi_model->GenerateValidasiPendapatanRanap($row->pendaftaran_id,$row->kasir_id,'4',date('Y-m-d'));	
		}
		$data_info=array(
			'pendaftaran_id'=>$row->pendaftaran_id,
			'kasir_id'=>$row->kasir_id,
			'pembayaran_id'=>$row->pembayaran_id,
			'idtipe'=>$row->idtipe,
			'nopendaftaran'=>$row->nopendaftaran,
			'tanggal_kunjungan'=>$row->tanggal_kunjungan,
			'tanggal_transaksi'=>$row->tanggal_transaksi,
			'idpasien'=>$row->idpasien,
			'no_medrec'=>$row->no_medrec,
			'title'=>$row->title,
			'namapasien'=>$row->namapasien,
			'idkelompokpasien'=>$row->idkelompokpasien,
			'idrekanan'=>$row->idrekanan,
			'total_transaksi'=>$row->total_transaksi,
			'tidak_tertagih'=>$row->tidak_tertagih,
			'nominal_tagihan'=>$row->tidak_tertagih,
			'nominal_bayar'=>0,
			'nominal_lain'=>$row->nominal_lain,
			'jaminan'=>$row->jaminan,
			'status'=>$row->status,
			'tanggal_jatuh_tempo'=>$row->tgl_setting,
			'created_by'=> $this->session->userdata('user_id'),
			'created_nama'=> $this->session->userdata('user_name'),
			'created_date'=>date('Y-m-d H:i:s'),
		);
		// print_r($data_info);
		$this->db->insert('tpiutang_tt_verifikasi', $data_info);
		// $klaim_id=$this->db->insert_id();
        $data =array(
            'st_verifikasi_piutang'=>'1',
			'user_verifikasi'=>$this->session->userdata('user_name'),
            'tanggal_verifikasi'=>date('Y-m-d H:i:s'),
            'tanggal_jatuh_tempo'=>$tanggal_jt
        );
        $this->db->where('id', $id);
		if ($idtipe < 3){
			$result=$this->db->update('tkasir_pembayaran', $data);
		}else{
			$result=$this->db->update('trawatinap_tindakan_pembayaran_detail', $data);
		}
        if ($result) {
            $this->output->set_output(json_encode($data_info));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }


}
