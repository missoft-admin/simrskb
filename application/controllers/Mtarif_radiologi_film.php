<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtarif_radiologi_film extends CI_Controller {

	/**
	 * Film Radiologi controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtarif_radiologi_film_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Film Radiologi';
		$data['content'] 		= 'Mtarif_radiologi_film/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Film Radiologi",'#'),
									    			array("List",'mtarif_radiologi_film')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Film Radiologi';
		$data['content'] 		= 'Mtarif_radiologi_film/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Film Radiologi",'#'),
									    			array("Tambah",'mtarif_radiologi_film')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mtarif_radiologi_film_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> htmlspecialchars($row->nama),
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Film Radiologi';
				$data['content']	 	= 'Mtarif_radiologi_film/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Film Radiologi",'#'),
											    			array("Ubah",'mtarif_radiologi_film')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtarif_radiologi_film','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtarif_radiologi_film');
		}
	}

	function delete($id){
		$this->Mtarif_radiologi_film_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mtarif_radiologi_film','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mtarif_radiologi_film_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_radiologi_film','location');
				}
			} else {
				if($this->Mtarif_radiologi_film_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_radiologi_film','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mtarif_radiologi_film/manage';

		if($id==''){
			$data['title'] = 'Tambah Film Radiologi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Film Radiologi",'#'),
															array("Tambah",'mtarif_radiologi_film')
													);
		}else{
			$data['title'] = 'Ubah Film Radiologi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Film Radiologi",'#'),
															array("Ubah",'mtarif_radiologi_film')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
			$this->from   = 'mtarif_radiologi_film';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'id' => 'DESC'
			);
			$this->group  = array();
			if (UserAccesForm($user_acces_form,array('240'))){
				$this->column_search   = array('nama');
			}else{
				$this->column_search   = array();
			}
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->nama;

					$aksi = '<div class="btn-group">';
		            if (UserAccesForm($user_acces_form,array('242'))){
		                $aksi .= '<a href="'.site_url().'mtarif_radiologi_film/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
		            }
		            if (UserAccesForm($user_acces_form,array('243'))){
		                $aksi .= '<a href="#" data-urlindex="'.site_url().'mtarif_radiologi_film" data-urlremove="'.site_url().'mtarif_radiologi_film/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
		            }
		            $aksi .= '</div>';            
  					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
}
