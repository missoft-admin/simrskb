<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mjenis_pendapatan extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('mjenis_pendapatan_model');
		$this->load->model('msetting_jurnal_pembelian_model');
		$this->load->helper('path');
		
  }


	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
				$tab='1';
		
		if (UserAccesForm($user_acces_form,array('2566'))){
			$data['list_akun'] = $this->msetting_jurnal_pembelian_model->list_akun();
			$data['idakun_pendapatan'] 			= '';
			$data['idakun_pendapatan_kredit'] 			= '';
			$data['idakun_loss'] 			= '';
			$data['idakun_loss_kredit'] 			= '';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Pendapatan & Loss';
			$data['content'] 		= 'Mjenis_pendapatan/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Billing & Pendapatan",'#'),
												  array("Setting Pendapatan & Losss",'mjenis_pendapatan/index')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function load_pendapatan()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*,A.namaakun,A.noakun ,AK.namaakun as namaakun_kredit,AK.noakun as noakun_kredit
						FROM mjenis_pendapatan H
						LEFT JOIN makun_nomor A ON A.id=H.idakun
						LEFT JOIN makun_nomor AK ON AK.id=H.idakun_kredit
						WHERE H.staktif='1'
						ORDER BY H.id asc
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('namaakun','nama_jenis','noakun');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->nama_jenis);
          $result[] = ($r->noakun).' - '.$r->namaakun;
          $result[] = ($r->noakun_kredit).' - '.$r->namaakun_kredit;
         
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('2568'))){
			$aksi .= '<button onclick="edit_pendapatan('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		  if (UserAccesForm($user_acces_form,array('2569'))){
			$aksi .= '<button onclick="hapus_pendapatan('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	

  function simpan_pendapatan(){
	$pendapatan_id=$this->input->post('pendapatan_id');
	$nama_jenis=$this->input->post('nama_jenis');
	$idakun=$this->input->post('idakun');
	$idakun_kredit=$this->input->post('idakun_kredit');
		$data=array(
			'nama_jenis'=>$nama_jenis,
			'idakun'=>$idakun,
			'idakun_kredit'=>$idakun_kredit,
		);

		if ($pendapatan_id){
			$data['edited_by'] = $this->session->userdata('user_id');
			$data['edited_date'] = date('Y-m-d H:i:s');
			$this->db->where('id',$pendapatan_id);
			$hasil=$this->db->update('mjenis_pendapatan',$data);
		}else{
			$data['created_by'] = $this->session->userdata('user_id');
			$data['created_date'] = date('Y-m-d H:i:s');
			$hasil=$this->db->insert('mjenis_pendapatan',$data);
		}
		$this->output->set_output(json_encode($hasil));
	}
  function edit_pendapatan(){
	  $id=$this->input->post('id');
	  $q="SELECT *FROM mjenis_pendapatan H WHERE H.id='$id'";
	  $hasil=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($hasil));
  }
  function hapus_pendapatan(){
		$id=$this->input->post('id');
		$data['deleted_by'] = $this->session->userdata('user_id');
		$data['deleted_date'] = date('Y-m-d H:i:s');
		$data['staktif'] = '0';
		$this->db->where('id',$id);
		$hasil=$this->db->update('mjenis_pendapatan',$data);

		$this->output->set_output(json_encode($hasil));

  }
function load_loss()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*,A.namaakun,A.noakun ,AK.namaakun as namaakun_kredit,AK.noakun as noakun_kredit
						FROM mjenis_loss H
						LEFT JOIN makun_nomor A ON A.id=H.idakun
						LEFT JOIN makun_nomor AK ON AK.id=H.idakun_kredit
						WHERE H.staktif='1'
						ORDER BY H.id asc
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('namaakun','nama_jenis','noakun');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->nama_jenis);
          $result[] = ($r->noakun).' - '.$r->namaakun;
          $result[] = ($r->noakun_kredit).' - '.$r->namaakun_kredit;
         
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('2568'))){
			$aksi .= '<button onclick="edit_loss('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		  if (UserAccesForm($user_acces_form,array('2569'))){
			$aksi .= '<button onclick="hapus_loss('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	

  function simpan_loss(){
	$loss_id=$this->input->post('loss_id');
	$nama_jenis=$this->input->post('nama_jenis');
	$idakun=$this->input->post('idakun');
	$idakun_kredit=$this->input->post('idakun_kredit');
		$data=array(
			'nama_jenis'=>$nama_jenis,
			'idakun'=>$idakun,
			'idakun_kredit'=>$idakun_kredit,
		);

		if ($loss_id){
			$data['edited_by'] = $this->session->userdata('user_id');
			$data['edited_date'] = date('Y-m-d H:i:s');
			$this->db->where('id',$loss_id);
			$hasil=$this->db->update('mjenis_loss',$data);
		}else{
			$data['created_by'] = $this->session->userdata('user_id');
			$data['created_date'] = date('Y-m-d H:i:s');
			$hasil=$this->db->insert('mjenis_loss',$data);
		}
		$this->output->set_output(json_encode($hasil));
	}
  function edit_loss(){
	  $id=$this->input->post('id');
	  $q="SELECT *FROM mjenis_loss H WHERE H.id='$id'";
	  $hasil=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($hasil));
  }
  function hapus_loss(){
		$id=$this->input->post('id');
		$data['deleted_by'] = $this->session->userdata('user_id');
		$data['deleted_date'] = date('Y-m-d H:i:s');
		$data['staktif'] = '0';
		$this->db->where('id',$id);
		$hasil=$this->db->update('mjenis_loss',$data);

		$this->output->set_output(json_encode($hasil));

  }

  
}
