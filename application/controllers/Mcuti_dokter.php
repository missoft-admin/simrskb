<?php

class Mcuti_dokter extends CI_Controller
{

    /**
     * Cuti Dokter controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mcuti_dokter_model', 'model');
    }

    public function index()
    {
        $data['error']      = '';
        $data['title']      = 'Cuti Dokter';
        $data['content']    = 'Mcuti_dokter/index';
        $data['breadcrum']  = array(
            array("RSKB Halmahera",'dashboard'),
            array("Bendahara",'#'),
            array("Cuti Dokter",'Mcuti_dokter'),
            array("List",'')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create()
    {
        $data = array(
            'id' => '',
            'iddokter' => '',
            'sebab' => '',
            'tanggal_dari' => '',
            'tanggal_sampai' => '',
            'alasan' => '',
            'status' => '',
        );

        $data['error']      = '';
        $data['title']      = 'Tambah Cuti Dokter';
        $data['content']    = 'Mcuti_dokter/manage';
        $data['breadcrum']  = array(
            array("RSKB Halmahera",'dashboard'),
            array("Bendahara",'#'),
            array("Cuti Dokter",'Mcuti_dokter'),
            array("Create",'')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id)
    {
        if ($id != '') {
            $row = $this->model->getSpecified($id);
            if (isset($row->id)) {
                $data = array(
                    'id' => $row->id,
                    'iddokter' => $row->iddokter,
                    'sebab' => $row->sebab,
                    'tanggal_dari' => DMYFormat($row->tanggal_dari),
                    'tanggal_sampai' => DMYFormat($row->tanggal_sampai),
                    'alasan' => $row->alasan,
                    'status' => $row->status,
                );

                $data['error']      = '';
                $data['title']      = 'Ubah Data';
                $data['content']    = 'Mcuti_dokter/manage';
                $data['breadcrum']  = array(
                    array("RSKB Halmahera",'dashboard'),
                    array("Bendahara",'#'),
                    array("Cuti Dokter",'Mcuti_dokter'),
                    array("Edit",'')
                );

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mcuti_dokter', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mcuti_dokter');
        }
    }

    public function delete($id)
    {
        $this->model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mcuti_dokter', 'location');
    }

    public function save()
    {
        $this->form_validation->set_rules('iddokter', 'Dokter', 'trim|required');
        $this->form_validation->set_rules('sebab', 'Sebab', 'trim|required');

        if ($this->form_validation->run() == true) {
            if ($this->input->post('id') == '') {
                if ($this->model->saveData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mcuti_dokter', 'location');
                }
            } else {
                if ($this->model->updateData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mcuti_dokter', 'location');
                }
            }
        } else {
            $this->failed_save($this->input->post('id'));
        }
    }

    public function failed_save($id)
    {
        $data = $this->input->post();
        $data['error']   = validation_errors();
        $data['content'] = 'Mcuti_dokter/manage';

        if ($id=='') {
            $data['title']      = 'Tambah Cuti Dokter';
            $data['breadcrum']  = array(
                array("RSKB Halmahera",'dashboard'),
                array("Bendahara",'#'),
                array("Cuti Dokter",'Mcuti_dokter'),
                array("Tambah",'')
            );
        } else {
            $data['title']      = 'Ubah Cuti Dokter';
            $data['breadcrum']  = array(
                array("RSKB Halmahera",'dashboard'),
                array("Bendahara",'#'),
                array("Cuti Dokter",'Mcuti_dokter'),
                array("Ubah",'')
            );
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function getIndex()
    {
        $this->select = array('mcuti_dokter.*', 'mdokter.nama AS namadokter');
        $this->from = 'mcuti_dokter';
        $this->join = array(
            array('mdokter', 'mdokter.id = mcuti_dokter.iddokter', ''),
        );

        $this->where  = array(
            'mcuti_dokter.status' => '1'
        );
        $this->order  = array(
            'mcuti_dokter.id' => 'DESC'
        );

        $this->group  = array();

        $this->column_search   = array('mdokter.nama');

        $this->column_order    = array('mdokter.nama');

        $list = $this->datatable->get_datatables();

        $no = $_POST['start'];

        $data = array();
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->namadokter;
            $row[] = $r->sebab;
            $row[] = '<label class="label label-primary">' . DMYFormat($r->tanggal_dari) . ' - '. DMYFormat($r->tanggal_sampai). '</label>';
            $row[] = $r->alasan;
            $row[] = '<div class="btn-group">
                <a href="'.site_url().'mcuti_dokter/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                <a href="#" data-urlindex="'.site_url().'mrak" data-urlremove="'.site_url().'mcuti_dokter/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>
            </div>';

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        echo json_encode($output);
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
