<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
  # Transaksi C-Arm #
  @Muhamad Ali Nurdin
*/

class Tc_arm extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Tc_arm_model', 'model');
  }

  // @common:function &start
    public function getJenis($id)
    {
      $result = $this->model->getJenis($id);
      return $result[0]->nama;
    }

    public function getKelas($kelas) {
      // 1 : I | 2 : II | 3 : III
      if ($kelas == 0) {
        $romawi = "POLIKLINIK / IGD";
      } else if ($kelas == 1) {
        $romawi = "I";
      } else if ($kelas == 2) {
        $romawi = "II";
      } else if ($kelas == 3) {
        $romawi = "III";
      } else if ($kelas == 4) {
        $romawi = "UTAMA";
      }
      return $romawi;
    }

  // @common:function &end

  public function index()
  {
    $data['error']      = '';
    $data['title']      = 'C - Arm';
    $data['content']    = 'Tc_arm/index';
    $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Bendahara",'#'), array("Transaksi",'#'), array("C - Arm",'#'));
    $data = array_merge($data, backend_info());
    $this->parser->parse('module_template', $data);
  }

  public function payments($id)
  {
    $data['error']      = '';
    $data['title']      = 'Transaksi Bagi Hasil Usaha';
    $data['content']    = 'Tc_arm/payments';
    $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Bendahara",'#'), array("Transaksi",'#'), array("Bagi Hasil Usaha",'#'));
    $data = array_merge($data, backend_info());
    $this->parser->parse('module_template', $data);
  }

  public function paymentsDetail($id)
  {
    $data['error']      = '';
    $data['title']      = 'Transaksi Bagi Hasil Usaha';
    $data['content']    = 'Tc_arm/paymentsdetail';
    $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Bendahara",'#'), array("Transaksi",'#'), array("Bagi Hasil Usaha",'#'));
    $data = array_merge($data, backend_info());
    $this->parser->parse('module_template', $data);
  }

  public function periodYears($id)
  {
    $data['error']      = '';
    $data['title']      = 'Periode C - Arm Pertahun';
    $data['content']    = 'Tc_arm/periodyears';
    $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Bendahara",'#'), array("Transaksi",'#'), array("C - Arm",'#'), array("Periode Pertahun",'#'));
    $data = array_merge($data, backend_info());
    $this->parser->parse('module_template', $data);
  }

  public function periodMonths($id)
  {
    $data['error']      = '';
    $data['title']      = 'Periode C - Arm Perbulan';
    $data['content']    = 'Tc_arm/periodmonths';
    $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Bendahara",'#'), array("Transaksi",'#'), array("C - Arm",'#'), array("Periode Pertahun",'#'), array("Periode Perbulan",'#'));
    $data = array_merge($data, backend_info());
    $this->parser->parse('module_template', $data);
  }

  public function periodMonthsDetail($id)
  {
    $data['error']      = '';
    $data['title']      = 'Transaksi C - ARM';
    $data['content']    = 'Tc_arm/manage';
    $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Bendahara",'#'), array("Transaksi",'#'), array("C - Arm",'#'), array("Periode Pertahun",'#'), array("Periode Perbulan",'#'), array("Transaksi C - ARM",'#'));
    $data = array_merge($data, backend_info());
    $this->parser->parse('module_template', $data);
  }

  public function savePayments($id, $year)
  {
    $months = $this->input->post('months[]');
    $state  = $this->input->post('state');

    $data = [
      'nama_transaksi' => $this->input->post('name-payments'),
      'bulan'          => implode(',', $months),
      'tahun'          => $this->input->post('periodyears'),
      'status'         => 0,
      'catatan'        => $this->input->post('note'),
      'id_carm'        => $this->input->post('id-carm')
    ];

    $result = $this->model->savePayments($data, $id, $year, $months, $state);

    if ($result) {
      $this->session->set_flashdata('confirm', true);
      $this->session->set_flashdata('message_flash', 'Sudah Tersimpan !');
      redirect("Tc_arm/payments/$id/$year", 'location');
    }
  }

  public function saveTransaction()
  {
    date_default_timezone_set("Asia/Bangkok");
    $action = $this->input->post('action');

    $id      = $this->input->post('id-transaksi');
    $idtarif = $this->input->post('id-tarif');
    $period  = $this->input->post('name-period');
    $de_period = explode(" ", $period);

    $data = [
      'id_carm'            => $idtarif,
      'periode'            => $period,
      'periodebulan'       => $de_period[0],
      'periodetahun'       => $de_period[1],
      'total_pemiliksaham' => $this->input->post('total-pemilik-saham'),
      'catatan'            => $this->input->post('note-carm'),
      'total_pendapatan'   => $this->input->post('total-pendapatan'),
      'total_operasional'  => $this->input->post('total-operasional'),
      'total_potongan'     => $this->input->post('total-potongan'),
      'total_deviden'      => $this->input->post('total-deviden'),
      'total_perlembar'    => $this->input->post('total-perlembar'),
      'status'             => $action,
      'statuspayment'      => 0
    ];

    $detail   = $this->input->post('detail-biaya-operasional');
    $detail_s = $this->input->post('id-sewaalat');

    $result = $this->model->saveTransaction($data, $detail, $id, $detail_s, $action);

    if ($result) {
      $this->session->set_flashdata('confirm', true);
      $this->session->set_flashdata('message_flash', 'Sudah Tersimpan !');
      redirect("Tc_arm/periodMonths/$idtarif", 'location');
    }
  }

  public function showCarm()
  {
    $result = $this->model->getCarmView();

    $data = array();
    foreach ($result as $row) {
      $rows = array();

      $id = $row->id;
      $btn = '<a href="'.site_url().'Tc_arm/periodYears/'.$id.'" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></a>&nbsp;';

      if ($row->status == 1) {
        $status = '<button class="btn btn-sm btn-primary status-carm" data-value="'.$row->status.'"><i class="fa fa-thumbs-up"></i></button>';
      } else {
        $status = '<button class="btn btn-sm btn-danger status-carm" data-value="'.$row->status.'"><i class="fa fa-thumbs-down"></i></button>';
      }

      $rows[] = $row->id;
      $rows[] = $row->nama_carm;
      $rows[] = $row->totallembaran;
      $rows[] = $status;
      $rows[] = $btn;

      $data[] = $rows;
    }

    $output = array(
        "recordsTotal" => count($result),
        "recordsFiltered" => count($result),
        "data" => $data
      );

    echo json_encode($output);
  }

  public function showCarmDetail($id)
  {
    $result = $this->model->getCarmView($id);
    $this->output->set_output(json_encode($result));
  }

  public function showDetailInMonths($id, $year, $month, $idt = null)
  {
    $result = $this->model->getDetailInMonths($id, $year, $month, $idt);

    $data = array();
    foreach ($result as $row) {
      $rows = array();

      if ($row->statusverifikasi == 0) {
        $status = '<label class="label label-warning text-uppercase"><i class="fa fa-history"></i> Belum Diverifikasi</label>';
      } else {
        $status = '<label class="label label-primary text-uppercase"><i class="fa fa-check-circle"></i> Sudah Diverifikasi</label>';
      }

      $kelas = '<label class="label label-primary text-uppercase"><i class="fa fa-bed"></i> Kelas&nbsp;'.$this->getKelas($row->kelas).'</label>';
      $jenis = '<label class="label label-primary text-uppercase"><i class="fa fa-heartbeat"></i> '.$this->getJenis($row->jenis).'</label>';

      $nominal = '<label class="label label-success text-uppercase">'.number_format($row->nominal).'</label>';

      $rows[] = $row->noreg;
      $rows[] = $row->nomedrec;
      $rows[] = $row->nama;
      $rows[] = $row->namakelompokpasien;
      $rows[] = $row->namarekanan;
      $rows[] = $kelas;
      $rows[] = $jenis;
      // @CHANGE TANGGAL TO KONDISI
      $rows[] = $row->tanggal;
      $rows[] = $status;
      $rows[] = $nominal;
      $rows[] = $row->nominal;
      $rows[] = $row->id;
      $rows[] = $row->statuscarm;
      $rows[] = $row->tanggal_tutup;

      $data[] = $rows;
    }


    $output = array(
        "recordsTotal" => count($result),
        "recordsFiltered" => count($result),
        "data" => $data
      );

    echo json_encode($output);
  }

  // @--------------------------------------------------------------------------
  public function showPayments()
  {
    $result = $this->model->getPayments();

    $data = array();
    foreach ($result as $row) {
      $rows = array();

      if ($row->status == 0) {
        $el = '<label class="label label-danger"><i class="fa fa-history"></i> Sedang Diproses</label>';
      } else {
        $el = '<label class="label label-success"><i class="fa fa-check-circle"></i> Selesai Diproses</label>';
      }

      $btn = '<button class="btn btn-xs btn-info text-uppercase edit-payments" type="button" data-value="'.$row->id.'" data-toggle="modal" data-target="#add-payments" style="font-size:10px;"><i class="fa fa-pencil"></i>&nbsp;Edit</button>&nbsp;';
      $btn .= '<a href="'.site_url().'Tc_arm/paymentsDetail/'.$row->id.'/'.$row->tahun.'" class="btn btn-xs btn-primary" style="font-size:10px;"><i class="fa fa-check"></i>&nbsp;Transaksi</a>';

      $rows[] = $row->no_transaksi;
      $rows[] = $row->nama_transaksi;
      $rows[] = '<label class="label label-success">'.$row->bulan.'</label>';
      $rows[] = $row->tahun;
      $rows[] = $el;
      $rows[] = $btn;


      $data[] = $rows;
    }

    $output = array(
        "recordsTotal" => count($result),
        "recordsFiltered" => count($result),
        "data" => $data
      );

    echo json_encode($output);
  }

  public function showPaymentsB($id)
  {
    $result = $this->model->getPaymentsB($id);
    $this->output->set_output(json_encode($result));
  }

  public function showPaymentsD($id) {
    $result = $this->model->getPayments($id);
    $this->output->set_output(json_encode($result));
  }

  public function showPaymentsDetail($year, $months)
  {
    $result = $this->model->getPaymentsDetail($year, $months);

    $data = array();
    foreach ($result as $row) {
      $rows = array();

      $rows[] = $row->periode;
      $rows[] = number_format($row->total_pendapatan);
      $rows[] = $row->potongan_rs;
      $rows[] = number_format($row->total_operasional);
      $rows[] = number_format($row->total_deviden);
      $rows[] = number_format($row->total_perlembar);

      $data[] = $rows;
    }

    $output = array(
        "recordsTotal" => count($result),
        "recordsFiltered" => count($result),
        "data" => $data
      );

    echo json_encode($output);
  }

  public function showPaymentsPS($id)
  {
    $result = $this->model->getPaymentsPS($id);

    // print_r($result);
    //
    // die();
    $data = array();
    foreach ($result as $row) {

      if ($row->statuskirim == 1) {
        $option  = '<option value="0">Cash</option>';
        $option .= '<option value="1" selected>Transfer</option>';
      } else {
        $option  = '<option value="0" selected>Cash</option>';
        $option .= '<option value="1">Transfer</option>';
      }

      $rows = array();

      $rows[] = $row->nama_pemilik;
      $rows[] = $row->lembaran;
      $rows[] = number_format(20000);
      $rows[] = '<select class="form-control js-select2 method-transfer" type="text" disabled>'.$option.'</select>';
      if ($row->statuskirim == 1) {
        $rows[] = '<input class="form-control bank" type="text" value="'.$row->norekening.'" disabled>';
        $rows[] = '<input class="form-control number_rek" type="text" value="'.$row->namabank.'" disabled>';
      } else {
        $rows[] = '<input class="form-control bank" type="text" value="-" disabled>';
        $rows[] = '<input class="form-control number_rek" type="text" value="-" disabled>';
      }

    if ($row->statuskirim != null) {
        $rows[] = '<input class="form-control datepayments" type="text" value="'.$row->tanggal.'" disabled>';
      } else {
        $rows[] = '<input class="form-control datepayments" type="text" value="'.date('Y-m-d').'" disabled>';
      }

      $rows[] = '<button class="btn btn-xs btn-success check-method" disabled><i class="fa fa-check"></i></button>
                <button class="btn btn-xs btn-primary edit-method"><i class="fa fa-pencil"></i></button>
                <button class="btn btn-xs btn-info delete-method"><i class="fa fa-print"></i></button>';

      if ($row->statuskirim == 1) {
        $rows[] = "1";
        $rows[] = $row->norekening;
        $rows[] = $row->namabank;
      } else {
        $rows[] = "0";
        $rows[] = "-";
        $rows[] = "-";
      }

      $rows[] = $row->id;
      $rows[] = '-';
      $rows[] = '-';
      $rows[] = $row->namabank;
      $rows[] = $row->norekening;

      if ($row->statuskirim != null) {
        $rows[] = (string)$row->tanggal;
      } else {
        $rows[] = date("Y-m-d");
      }
      $rows[] = "";

      $data[] = $rows;
    }

    $output = array(
        "recordsTotal" => count($result),
        "recordsFiltered" => count($result),
        "data" => $data
      );

    echo json_encode($output);
  }

  public function showPaymentsMonths($id ,$year, $status = null)
  {
    $result = $this->model->getPaymentsMonths($id, $year, $status);
    $this->output->set_output(json_encode($result));
  }

  // @--------------------------------------------------------------------------

  public function showPeriodInYears($id)
  {
    $result = $this->model->getPeriodInYears($id);

    $data = array();
    foreach ($result as $row) {
      if ($row->id_carm == $id || $row->idpemilik == $id) {

        $rows = array();

        $btn = '<a href="'.site_url().'Tc_arm/periodMonths/'.$id.'" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></a>&nbsp;';
        $btn .= '<a href="'.site_url().'Tc_arm/payments/'.$id.'/'.$row->periode.'" class="btn btn-sm btn-primary"><i class="fa fa-shopping-cart"></i></a>&nbsp;';

        if ($row->status == 0) {
          $el = '<label class="label label-danger"><i class="fa fa-history"></i> Sedang Diproses</label>';
        } else {
          $el = '<label class="label label-success"><i class="fa fa-check-circle"></i> Selesai Diproses</label>';
        }

        $rows[] = $row->periode;
        $rows[] = number_format($row->totalpendapatan);
        $rows[] = $el;
        $rows[] = $btn;

        $data[] = $rows;

      }
    }

    $output = array(
        "recordsTotal" => count($result),
        "recordsFiltered" => count($result),
        "data" => $data
      );

    echo json_encode($output);
  }

  public function showPeriodInMonths($id)
  {
    $no     = 1;
    $result = $this->model->getPeriodInMonths($id);

    $data = array();
    foreach ($result as $row) {
      if ($row->id_carm == $id || $row->idpemilik == $id) {
        $rows = array();

          if ($no == count($result) && $row->tanggal_tutup == null) {
            $a = $row->periode;
            $periode = $a." ".$row->tahun;
          } else if ($row->tanggal_tutup == null) {
            $a = $this->convertMonthsName($this->convertMonthsNumber($row->periode)+1);
            $periode = $a." ".$row->tahun;
          } else {
            $a = $row->periodebulan;
            $periode = $row->periodebulan." ".$row->periodetahun;
          }

          $transaction = $this->model->getTransaction($id, $periode);

          if (empty($transaction)) {
            $btn = '<a href="'.site_url().'Tc_arm/periodMonthsDetail/'.$id.'/'.$a.'%'.$row->tahun.'" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></a>&nbsp;';
            $total_pendapatan = number_format($row->totalpendapatan);
          } else {
            $btn = '<a href="'.site_url().'Tc_arm/periodMonthsDetail/'.$id.'/'.$a.'%'.$row->tahun.'/'.$transaction[0]->id.'" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></a>&nbsp;';
            $total_pendapatan = number_format($row->totalpendapatan);
          }

          if ($row->status == 0) {
            $el = '<label class="label label-warning"><i class="fa fa-history"></i> Sedang Diproses</label>';
          } else if ($row->status == 1) {
            $el = '<label class="label label-success"><i class="fa fa-check-circle"></i> Selesai Diproses</label>';
          } else {
            $el = '<label class="label label-danger"><i class="fa fa-check-circle"></i> Diberhentikan</label>';
          }

          $rows[] = $periode;
          $rows[] = $total_pendapatan;
          $rows[] = $el;
          $rows[] = $btn;

          $data[] = $rows;
      }

      $no++;
    }

    $output = array(
        "recordsTotal" => count($result),
        "recordsFiltered" => count($result),
        "data" => $data
      );

    echo json_encode($output);
  }

  public function showTransactionDetail($id)
  {
    $result = $this->model->getTransactionDetail($id);
    $this->output->set_output(json_encode($result));
  }

  public function showTransactionId($id)
  {
    $result = $this->model->getTransactionId($id);
    $this->output->set_output(json_encode($result));
  }

  public function savePaymentsDetail() {
    $detail = $this->input->post('detailps');
    $id     = $this->input->post('id');

    $this->model->savePaymentsDetail($detail, $id);
  }

  // @----
  public function convertMonthsNumber($month) {
    $months = [
      "January"   => 1,
      "February"  => 2,
      "March"     => 3,
      "April"     => 4,
      "May"       => 5,
      "June"      => 6,
      "July"      => 7,
      "August"    => 8,
      "September" => 9,
      "October"   => 10,
      "November"  => 11,
      "December"  => 12
    ];

    return (int)$months[$month];
  }

  public function convertMonthsName($month) {
    $months = [
      1  => "January",
      2  => "February",
      3  => "March",
      4  => "April",
      5  => "May",
      6  => "June",
      7  => "July",
      8  => "August",
      9  => "September",
      10 => "October",
      11 => "November" ,
      12 => "December"
    ];

    return $months[$month];
  }
  // @----
}
