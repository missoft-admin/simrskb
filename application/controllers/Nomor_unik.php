<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use Hashids\Hashids;


class Nomor_unik extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
    {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		
    }

	function index(){
		$hashids = new Hashids('RSKBHALMAHERA', 0, 'ABCEFGHJKLMNPRSTUVWXYZ123456789');

        $id[] = $hashids->encode(23060001);
        $id[] = $hashids->encode(23060002);
        $id[] = $hashids->encode(23060003);
        $id[] = $hashids->encode(23060004);
        $id[] = $hashids->encode(23060005);
        
        echo "<pre>";
        echo "ENCODE <br/><br/>";
        print_r($id);
        
        echo "<br/>DECODE <br/>";
        $dec[] = $hashids->decode('PWVZ3W7');
        $dec[] = $hashids->decode('JWV5ZW2');
        $dec[] = $hashids->decode('7W986W5');
        $dec[] = $hashids->decode('8R6ZMRJ');
        $dec[] = $hashids->decode('Z37MN3K');
        
        print_r($dec);
        echo "</pre>";
    }
}
