<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpasien_asal extends CI_Controller {

	/**
	 * Asal Pasien controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpasien_asal_model');
		$this->load->helper('path');
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('24'))){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Asal Pasien';
		$data['content'] 		= 'Mpasien_asal/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Asal Pasien",'#'),
									    			array("List",'mpasien_asal')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function getIndex()
  {
			$this->select = array();
			$this->from   = 'mpasien_asal';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'id' => 'ASC'
			);
			$this->group  = array();

      $this->column_search   = array('nama');
      $this->column_order    = array('nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
}
