<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_honor_dokter extends CI_Controller {

	/**
	 * Setting Honor Dokter controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Msetting_honor_dokter_model');
  }

	function index() {
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Honor Dokter';
		$data['content'] 		= 'Msetting_honor_dokter/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Honor Dokter",'#'),
									    			array("List",'msetting_honor_dokter')
													);

		$data['setting_akun']	= $this->Msetting_honor_dokter_model->getSettingAkun();
		$data['setting_penyerahan_asuransi']	= $this->Msetting_honor_dokter_model->getSettingPenyerahan(1); // Asuransi
		$data['setting_penyerahan_hold']	= $this->Msetting_honor_dokter_model->getSettingPenyerahan(2); // Hold

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update() {
		return $this->Msetting_honor_dokter_model->updateData();
	}
}
