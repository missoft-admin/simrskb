<?php
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

class Lpenerimaan_gudang extends CI_Controller {

	/**
	 * Laporan Penerimaan Gudang controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Lpenerimaan_gudang_model', 'model');
		$this->load->model('Tsurvey_model');
	}

	function index() {
		$filter = [
			'tipe_gudang' => '',
			'periode_tanggal' => '',
			'tanggal_dari' => '',
			'tanggal_sampai' => '',
			'distributor_id' => '',
			'tipe_barang' => '',
			'order_by' => '',
		];

		$data = [];
		$data['error'] = '';
		$data['title'] = 'Laporan Penerimaan Gudang';
		$data['content'] = 'Lpenerimaan_gudang/index';
		$data['breadcrum'] = array(
			array("RSKB Halmahera",'#'),
			array("Laporan Penerimaan Gudang",'#'),
			array("List",'msatuan')
		);

		$data['list_report'] = $this->model->getAll($filter);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function export_report() {
		$tipe_gudang = $this->input->post('tipe_gudang');
		$periode_tanggal = $this->input->post('periode_tanggal');
		$tanggal_dari = $this->input->post('tanggal_dari');
		$tanggal_sampai = $this->input->post('tanggal_sampai');
		$distributor_id = $this->input->post('distributor_id');
		$tipe_barang = $this->input->post('tipe_barang');
		$order_by = $this->input->post('order_by');
		$btn_export = $this->input->post('btn_export');

		$data['filter'] = [
			'tipe_gudang' => $tipe_gudang,
			'periode_tanggal' => $periode_tanggal,
			'tanggal_dari' => $tanggal_dari,
			'tanggal_sampai' => $tanggal_sampai,
			'distributor_id' => $distributor_id,
			'tipe_barang' => $tipe_barang,
			'order_by' => $order_by,
		];

		$data['title'] = 'Laporan Penerimaan Gudang';
		$data['report'] = $this->model->getAll($data['filter']);

		$setting = $this->Tsurvey_model->setting_survey_label();
		$data = array_merge($data, $setting, backend_info());

		if ($btn_export == 'pdf'){
			$this->export_pdf($data);
		}else{
			$this->export_excel($data);
		}
	}

    public function export_pdf($data)
    {
		$options = new Options();
		$options->set('is_remote_enabled',true);
		$dompdf = new Dompdf($options);

        $html = $this->load->view('Lpenerimaan_gudang/export_pdf', $data, true);
        $dompdf->loadHtml($html);
		$dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
		$dompdf->stream($data['title'].'.pdf', array("Attachment" => 0));
    }
    
	public function export_excel($data)
    {
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Kartu Stok');

        // Set Logo
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $logo = FCPATH.'/assets/upload/logo/logoreport.jpg';
        $objDrawing->setPath($logo);
        $objDrawing->setCoordinates('B1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setHeight(70);
        $objDrawing->setWidth(50);
        $objDrawing->setWorksheet($activeSheet);

        // Set Header
        $activeSheet->setCellValue('B1', "                                      RUMAH SAKIT KHUSUS BEDAH HALMAHERA SIAGA");
        $activeSheet->setCellValue('B2', "                                      Jl.LLRE.Martadinata no.28, Telp: +6222-4206061");
        $activeSheet->setCellValue('B3', "                                      Bandung, 40115");
        $activeSheet->mergeCells('B1:AA1');
        $activeSheet->mergeCells('B2:AA2');
        $activeSheet->mergeCells('B3:AA3');
        $activeSheet->getStyle('B1:AA1')->getFont()->setSize(11);
        $activeSheet->getStyle('B2:AA2')->getFont()->setSize(11);
        $activeSheet->getStyle('B3:AA3')->getFont()->setSize(11);

		// Set Periode
		$activeSheet->setCellValue('B6', "Periode");
		$activeSheet->setCellValue('C6', $data['filter']['tanggal_dari'] . ' s/d ' . $data['filter']['tanggal_sampai']);

		// Set Header Column
		$activeSheet->setCellValue('B8', "NO");
		$activeSheet->setCellValue('C8', "TIPE GUDANG");
		$activeSheet->setCellValue('D8', "NAMA DISTRIBUTOR");
		$activeSheet->setCellValue('E8', "TANGGAL TERIMA");
		$activeSheet->setCellValue('F8', "NOMOR PENERIMAAN");
		$activeSheet->setCellValue('G8', "NOMOR FAKTUR");
		$activeSheet->setCellValue('H8', "TANGGAL JATUH TEMPO");
		$activeSheet->setCellValue('I8', "KODE");
		$activeSheet->setCellValue('J8', "NAMA BARANG");
		$activeSheet->setCellValue('K8', "NAMA SATUAN");
		$activeSheet->setCellValue('L8', "HARGA SATUAN");
		$activeSheet->setCellValue('M8', "QTY");
		$activeSheet->setCellValue('N8', "JUMLAH HARGA");
		$activeSheet->setCellValue('O8', "DISC");
		$activeSheet->setCellValue('P8', "JUMLAH DISC");
		$activeSheet->setCellValue('Q8', "PPN");
		$activeSheet->setCellValue('R8', "NOMINAL PPN");
		$activeSheet->setCellValue('S8', "NILAI PERSEDIAAN");
		$activeSheet->setCellValue('T8', "NILAI FAKTUR");
		$activeSheet->setCellValue('U8', "TANGGAL KADALUARSA");
		$activeSheet->setCellValue('V8', "BAYAR");
		$activeSheet->setCellValue('W8', "KETERANGAN");
		$activeSheet->setCellValue('X8', "TANGGAL TRANSAKSI");
		$activeSheet->setCellValue('Y8', "USER");
		$activeSheet->setCellValue('Z8', "NOMOR PEMESANAN");
		$activeSheet->setCellValue('AA8', "TANGGAL PEMESANAN");

		$activeSheet->getStyle('B8:AA8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle('B8:AA8')->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK,
					)
				),
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Calibri'
				)
			)
		);

		$activeSheet->setCellValue('B5', 'LAPORAN PENERIMAAN GUDANG');
		$activeSheet->mergeCells('B5:AA5');

		$activeSheet->getStyle('B5')->getFont()->setBold(true)->setSize(12);
		$activeSheet->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$x = 8;
		$no = 1;
		$list_data = $data['report'];

		foreach ($list_data as $r) {
			$x = $x + 1;
			
			$activeSheet->setCellValue('B'.$x, $no);
			$activeSheet->setCellValue('C'.$x, $r->tipe_gudang);
			$activeSheet->setCellValue('D'.$x, $r->nama_distributor);
			$activeSheet->setCellValue('E'.$x, $r->tanggal_terima);
			$activeSheet->setCellValue('F'.$x, $r->nomor_penerimaan);
			$activeSheet->setCellValue('G'.$x, $r->nomor_faktur);
			$activeSheet->setCellValue('H'.$x, $r->tanggal_jatuh_tempo);
			$activeSheet->setCellValue('I'.$x, $r->kode_barang);
			$activeSheet->setCellValue('J'.$x, $r->nama_barang);
			$activeSheet->setCellValue('K'.$x, $r->nama_satuan);
			$activeSheet->setCellValue('L'.$x, $r->harga_satuan);
			$activeSheet->setCellValue('M'.$x, $r->qty);
			$activeSheet->setCellValue('N'.$x, $r->jumlah_harga);
			$activeSheet->setCellValue('O'.$x, $r->disc);
			$activeSheet->setCellValue('P'.$x, $r->jumlah_disc);
			$activeSheet->setCellValue('Q'.$x, $r->ppn);
			$activeSheet->setCellValue('R'.$x, $r->nominal_ppn);
			$activeSheet->setCellValue('S'.$x, $r->nilai_persediaan);
			$activeSheet->setCellValue('T'.$x, $r->nilai_faktur);
			$activeSheet->setCellValue('U'.$x, $r->tanggal_kadaluarsa);
			$activeSheet->setCellValue('V'.$x, $r->bayar);
			$activeSheet->setCellValue('W'.$x, $r->keterangan);
			$activeSheet->setCellValue('X'.$x, $r->tanggal_transaksi);
			$activeSheet->setCellValue('Y'.$x, $r->user_terima);
			$activeSheet->setCellValue('Z'.$x, $r->nomor_pemesanan);
			$activeSheet->setCellValue('AA'.$x, $r->tanggal_pemesanan);
			
			$no = $no + 1;
		}

		$activeSheet->getStyle('L9:T'.$x)->getNumberFormat()->setFormatCode('#,##0');
		$activeSheet->getStyle("B9:AA$x")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					)
				)
			)
		);

		$x = $x + 1;
		
		$activeSheet->setCellValue('B'.$x, 'TOTAL');
		$activeSheet->setCellValue('L'.$x, '=SUM(L9:L'.($x-1).')');
		$activeSheet->setCellValue('M'.$x, '=SUM(M9:M'.($x-1).')');
		$activeSheet->setCellValue('N'.$x, '=SUM(N9:N'.($x-1).')');
		$activeSheet->setCellValue('O'.$x, '=SUM(O9:O'.($x-1).')');
		$activeSheet->setCellValue('P'.$x, '=SUM(P9:P'.($x-1).')');
		$activeSheet->setCellValue('Q'.$x, '=SUM(Q9:Q'.($x-1).')');
		$activeSheet->setCellValue('R'.$x, '=SUM(R9:R'.($x-1).')');
		$activeSheet->setCellValue('S'.$x, '=SUM(S9:S'.($x-1).')');
		$activeSheet->setCellValue('T'.$x, '=SUM(T9:T'.($x-1).')');

		
        $activeSheet->mergeCells("B$x:K$x");
        $activeSheet->mergeCells("U$x:AA$x");

		// Set alignment ke kanan untuk total
		$activeSheet->getStyle('B'.$x.':T'.$x)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

		// Set format angka
		$activeSheet->getStyle('L'.$x.':T'.$x)->getNumberFormat()->setFormatCode('#,##0');

		$activeSheet->getStyle("B$x:AA$x")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					)
				)
			)
		);

		foreach (range('A', 'AA') as $column){
			$activeSheet->getColumnDimension($column)->setAutoSize(true);
		}

    	header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    	header('Content-Disposition: attachment;filename="LAPORAN PENERIMAAN GUDANG.xls"');
    	header('Cache-Control: max-age=0');

    	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    	$objWriter->save('php://output');
    }

	function get_index() {
		$tipe_gudang = $this->input->post('tipe_gudang');
		$periode_tanggal = $this->input->post('periode_tanggal');
		$tanggal_dari = $this->input->post('tanggal_dari');
		$tanggal_sampai = $this->input->post('tanggal_sampai');
		$distributor_id = $this->input->post('distributor_id');
		$tipe_barang = $this->input->post('tipe_barang');
		$order_by = $this->input->post('order_by');

		$filter = [
			'tipe_gudang' => $tipe_gudang,
			'periode_tanggal' => $periode_tanggal,
			'tanggal_dari' => $tanggal_dari,
			'tanggal_sampai' => $tanggal_sampai,
			'distributor_id' => $distributor_id,
			'tipe_barang' => $tipe_barang,
			'order_by' => $order_by,
		];

		$data = $this->model->getAll($filter);

		$this->output->set_output(json_encode($data));
	}
}
