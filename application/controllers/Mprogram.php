<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mprogram extends CI_Controller {

	/**
	 * Master Program controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mprogram_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Program';
		$data['content'] 		= 'Mprogram/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master Program",'#'),
									    			array("List",'mprogram')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'kode' 					=> '',
			'nama' 					=> '',
			'idprespektif' 					=> '',
			'urutan' 					=> '',
			'deskripsi'			=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Program';
		$data['content'] 		= 'Mprogram/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master Program",'#'),
									    			array("Tambah",'mprogram')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mprogram_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'kode' 					=> $row->kode,
					'nama' 					=> $row->nama,
					'idprespektif' 					=> $row->idprespektif,
					'urutan' 					=> $row->urutan,
					'deskripsi' 		=> $row->deskripsi,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Program';
				$data['content']	 	= 'Mprogram/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Program",'#'),
											    			array("Ubah",'mprogram')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mprogram','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mprogram');
		}
	}

	function delete($id){
		$this->Mprogram_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mprogram','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mprogram_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mprogram','location');
				}
			} else {
				if($this->Mprogram_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mprogram','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}
	
	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mprogram/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Program';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Program",'#'),
															array("Tambah",'mprogram')
													);
		}else{
			$data['title'] = 'Ubah Master Program';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Program",'#'),
															array("Ubah",'mprogram')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  $idprespektif=$this->input->post('idprespektif');
	  $where='';
	  if ($idprespektif !='#'){
		  $where .=" AND M.idprespektif='$idprespektif'";
	  }
		
	  $from="(SELECT M.id,M.kode,M.nama,M.urutan,M.deskripsi,P.nama as presprektif from mprogram_rka M
				LEFT JOIN mprespektif_rka P ON P.id=M.idprespektif
				WHERE M.`status`='1' ".$where."
				ORDER BY M.urutan ASC
				) as tbl";
	// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array('urutan','nama','presprektif','deskripsi','kode');
			$this->group  = array();

      $this->column_search   = array('urutan','nama','presprektif','deskripsi','kode');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					// if (UserAccesForm($user_acces_form,array('131'))){
						$aksi .= '<a href="'.site_url().'mprogram/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
					// }
					// if (UserAccesForm($user_acces_form,array('132'))){
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mprogram" data-urlremove="'.site_url().'mprogram/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
					// }
					
			        $aksi .= '</div>';

          $row[] = $no;
          $row[] = $r->kode;
          $row[] = $r->urutan;
          $row[] = $r->nama;
          $row[] = $r->presprektif;
          $row[] = $r->deskripsi;
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }

}
