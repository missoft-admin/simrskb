<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtarif_rawatinap extends CI_Controller {

	/**
	 * Tarif Rawat Inap controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtarif_rawatinap_model');
		$this->load->model('Mtarif_administrasi_model');
  }

	function index($idtipe=1, $idruangan=1){
		$data = array();
		$data['error'] 			= '';
		$data['idtipe']  		= $idtipe;
		$data['idruangan']  = $idruangan;
		$data['title'] 			= 'Tarif Rawat Inap';
		$data['content'] 		= 'Mtarif_rawatinap/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif Rawat Inap",'mtarif_rawatinap/index'),
														array("List",'#')
													);

		$data['list_ruangan'] = $this->Mtarif_rawatinap_model->getAllRuangan();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function filter(){
		if($this->input->post('idtipe') != '' && $this->input->post('idruangan') != ''){
			$idtipe = $this->input->post('idtipe');
			$idruangan = $this->input->post('idruangan');
			redirect("mtarif_rawatinap/index/$idtipe/$idruangan",'location');
		}else{
			redirect('mtarif_rawatinap/index/1/1','location');
		}
	}

	function create(){
		$data = array(
			'id' 							=> '',
			'idruangan' 			=> '',
			'idtipe' 					=> '',
			'nama' 						=> '',
			'idkelompok' 			=> '1',
			'headerpath' 			=> '0',
			'old_headerpath' 	=> '',
			'path' 						=> '',
			'level' 					=> '',
			'status' 					=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Tarif Rawat Inap';
		$data['content'] 		= 'Mtarif_rawatinap/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif Rawat Inap",'#'),
									    			array("Tambah",'mtarif_rawatinap')
													);

		$data['list_ruangan'] = $this->Mtarif_rawatinap_model->getAllRuangan();
		$data['list_parent'] = $this->Mtarif_rawatinap_model->getAllParent();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($idtipe, $idruangan, $id){
		if($id != ''){
			$row = $this->Mtarif_rawatinap_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 							=> $row->id,
					'idruangan' 			=> $row->idruangan,
					'idtipe' 					=> $row->idtipe,
					'nama' 						=> $row->nama,
					'idkelompok' 			=> $row->idkelompok,
					'headerpath' 			=> $row->headerpath,
					'old_headerpath' 	=> $row->headerpath,
					'path' 						=> $row->path,
					'level' 					=> $row->level,
					'status' 					=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Tarif Rawat Inap';
				$data['content']	 	= 'Mtarif_rawatinap/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Tarif Rawat Inap",'#'),
											    			array("Ubah",'mtarif_rawatinap')
															);

				$data['list_ruangan'] = $this->Mtarif_rawatinap_model->getAllRuangan();
				$data['list_parent'] = $this->Mtarif_rawatinap_model->getAllParent();
				$data['list_tarif'] = $this->Mtarif_rawatinap_model->getAllTarif($row->id);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtarif_rawatinap/index/'.$idtipe.'/'.$idruangan, 'location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtarif_rawatinap/index/'.$idtipe.'/'.$idruangan, 'location');
		}
	}

	function delete($idtipe, $idruangan, $id){
		$this->Mtarif_rawatinap_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mtarif_rawatinap/index/'.$idtipe.'/'.$idruangan, 'location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mtarif_rawatinap_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_rawatinap/index/'.$this->input->post('idtipe').'/'.$this->input->post('idruangan'), 'location');
				}
			} else {
				if($this->Mtarif_rawatinap_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_rawatinap/index/'.$this->input->post('idtipe').'/'.$this->input->post('idruangan'), 'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 			 = validation_errors();
		$data['content'] 		 = 'Mtarif_rawatinap/manage';

		$data['list_parent'] = $this->Mtarif_rawatinap_model->getAllParent();

		if($id==''){
			$data['title'] 			= 'Tambah Tarif Rawat Inap';
			$data['breadcrum']	= array(
															array("RSKB Halmahera",'#'),
															array("Tarif Rawat Inap",'#'),
										    			array("Tambah",'mtarif_rawatinap')
														);
		}else{
			$data['title'] 			= 'Ubah Tarif Rawat Inap';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Tarif Rawat Inap",'#'),
															array("Ubah",'mtarif_rawatinap')
														);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function get_parent($idtipe, $idruangan) {
		$arr = $this->Mtarif_rawatinap_model->getParent($idtipe, $idruangan);
		$this->output->set_output(json_encode($arr));
	}

	function get_child_level($headerpath) {
		$arr = $this->Mtarif_rawatinap_model->getPathLevel($headerpath);
		$this->output->set_output(json_encode($arr));
	}

	function getIndex($idtipe, $idruangan)
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];

			$this->select = array();
			$this->from   = 'mtarif_rawatinap';
			$this->join 	= array();
			$this->where  = array(
				'idtipe' => $idtipe,
				'idruangan' => $idruangan,
				'status' => '1'
			);
			$this->order  = array(
				'path' => 'ASC'
			);
			$this->group  = array();

			$this->column_search   = array('nama');
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = TreeView($r->level, $r->nama);

					$aksi = '<div class="btn-group">';
          if (UserAccesForm($user_acces_form,array('232'))){
              $aksi .= '<a href="'.site_url().'mtarif_rawatinap/update/'.$idtipe.'/'.$idruangan.'/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
          }
          if (UserAccesForm($user_acces_form,array('233'))){
              $aksi .= '<a href="'.site_url().'mtarif_rawatinap/delete/'.$idtipe.'/'.$idruangan.'/'.$r->id.'" data-urlindex="'.site_url().'mtarif_rawatinap" data-urlremove="'.site_url().'mtarif_rawatinap/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
          }
					if ($r->idkelompok == 0) {
						$aksi .= '<a href="'.site_url().'mtarif_rawatinap/setting/'.$idtipe.'/'.$idruangan.'/'.$r->id.'" data-toggle="tooltip" title="Setting" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>';
					}
          $aksi .= '</div>';

					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}

	function setting($idtipe, $idruangan, $id){
		if($id != ''){
			$row = $this->Mtarif_rawatinap_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 							=> $row->id,
					'idruangan' 			=> $row->idruangan,
					'idtipe' 					=> $row->idtipe,
					'nama' 						=> $row->nama,
					'idkelompok' 			=> $row->idkelompok,
					'headerpath' 			=> $row->headerpath,
					'old_headerpath' 	=> $row->headerpath,
					'path' 						=> $row->path,
					'level' 					=> $row->level,
					'group_diskon_all' 					=> $row->group_diskon_all,
					'status' 					=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Setting Group Pembayaran Tarif Rawat Inap';
				$data['content']	 	= 'Mtarif_rawatinap/setting';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Tarif Rawat Inap",'#'),
											    			array("Setting",'mtarif_rawatinap')
															);

				$data['list_ruangan'] = $this->Mtarif_rawatinap_model->getAllRuangan();
				$data['list_parent'] = $this->Mtarif_rawatinap_model->getAllParent();
				$data['list_tarif'] = $this->Mtarif_rawatinap_model->getAllTarif($row->id);
				$data['list_group_pembayaran'] = $this->Mtarif_administrasi_model->getGroupPembayaran();

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtarif_rawatinap/index/'.$idtipe.'/'.$idruangan, 'location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtarif_rawatinap/index/'.$idtipe.'/'.$idruangan, 'location');
		}
	}

	function save_setting() {
		if($this->Mtarif_rawatinap_model->updateSetting()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mtarif_rawatinap/index/'.$this->input->post('idtipe'), 'location');
		}
	}
}
