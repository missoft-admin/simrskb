<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_pengelolaan_hold extends CI_Controller
{

    /**
     * Pengelolaan Hold controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Trm_pengelolaan_hold_model', 'model');
    }

    public function index()
    {
        $data = array(
            'tipe' => '',
            'dokter' => '',
            'jam_dari' => '',
            'jam_sampai' => '',
        );

        $data['error'] = '';
        $data['title'] = 'Pengelolaan Hold';
        $data['content'] = 'Trm_pengelolaan_hold/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Pengelolaan Hold",'#'),
            array("List",'trm_pengelolaan_hold')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filter()
    {
        $data = array(
            'tipe' => $this->input->post('tipe'),
            'dokter' => $this->input->post('dokter'),
            'jam_dari' => $this->input->post('jam_dari'),
            'jam_sampai' => $this->input->post('jam_sampai'),
        );

        $this->session->set_userdata($data);

        $data['error'] = '';
        $data['title'] = 'Pengelolaan Hold';
        $data['content'] = 'Trm_pengelolaan_hold/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Pengelolaan Hold",'#'),
            array("Filter",'trm_pengelolaan_hold')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function detail($iddokter, $idruangan)
    {
        $dokter = $this->model->getDokter($iddokter);
        $ruangan = $this->model->getRuangan($idruangan);

        if ($dokter->id && $ruangan->id) {
            $data = array(
                'iddokter' => $iddokter,
                'idruangan' => $idruangan,
                'namapasien' => '',
                'nomedrec' => '',
                'tipe' => '',
                'tanggal_dari' => '',
                'tanggal_sampai' => '',
                'status' => '',
            );

            $data['error'] = '';
            $data['title'] = 'Pengelolaan Hold ' . $dokter->nama . ' / R.' . $ruangan->nama;
            $data['content'] = 'Trm_pengelolaan_hold/detail';
            $data['breadcrum'] 	= array(
                array("RSKB Halmahera",'#'),
                array("Pengelolaan Hold",'#'),
                array("List",'trm_pengelolaan_hold')
            );

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        }
    }

    public function filter_detail($iddokter, $idruangan)
    {
        $dokter = $this->model->getDokter($iddokter);
        $ruangan = $this->model->getRuangan($idruangan);

        if ($dokter->id && $ruangan->id) {
            $data = array(
                'iddokter' => $iddokter,
                'idruangan' => $idruangan,
                'namapasien' => $this->input->post('namapasien'),
                'nomedrec' => $this->input->post('nomedrec'),
                'tipe' => $this->input->post('tipe'),
                'tanggal_dari' => $this->input->post('tanggal_dari'),
                'tanggal_sampai' => $this->input->post('tanggal_sampai'),
                'status' => $this->input->post('status'),
            );
            
            $this->session->set_userdata($data);

            $data['error'] = '';
            $data['title'] = 'Pengelolaan Hold ' . $dokter->nama . ' / R.' . $ruangan->nama;
            $data['content'] = 'Trm_pengelolaan_hold/detail';
            $data['breadcrum'] 	= array(
                array("RSKB Halmahera",'#'),
                array("Pengelolaan Hold",'#'),
                array("List",'trm_pengelolaan_hold')
            );

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        }
    }

    public function getIndex($uri = 'index')
    {
        $prefix = '<label class="label label-primary">';
        $suffix = '</label>';

        $filterTime = '';
        $filterTipe = '';
        $filterDokter = '';

        if ($uri == 'filter') {
            $jamDari = $this->session->userdata('jam_dari');
            $jamSampai = $this->session->userdata('jam_sampai');
            if ($jamDari != null && $jamSampai != null) {
                $filterTime = "AND mjadwal_dokter.jam_dari >= '$jamDari' AND ( mjadwal_dokter.jam_dari <= '$jamDari' OR mjadwal_dokter.jam_sampai <= '$jamSampai' )";
            }
            
            $idtipe = $this->session->userdata('dokter');
            if ($idtipe != '#') {
                $filterTipe = "AND trm_berkas_analisa_dokter.asalpendaftaran = $idtipe";
            }
            
            $iddokter = $this->session->userdata('dokter');
            if ($iddokter != '#') {
                $filterDokter = "AND trm_berkas_analisa_dokter.iddokter = $iddokter";
            }
        }

        $this->select = array();

        $this->from = "
        (
            SELECT
                result.iddokter,
                result.namadokter,
                result.jumlah_lembaran_hold,
                result.jumlah_lembar_minus,
                GROUP_CONCAT( DISTINCT CONCAT('$prefix', result.jam_dari, ' - ', result.jam_sampai, '$suffix') SEPARATOR '<br><br>' ) AS jadwal,
                result.idruangan,
                result.ruangan
            FROM
            (	
                SELECT
                    trm_berkas_analisa_dokter.iddokter AS iddokter,
                    trm_berkas_analisa_dokter.namadokter,
                    SUM( IF ( trm_berkas_analisa.hold = '1', 1, 0 ) ) AS jumlah_lembaran_hold,
                    SUM( IF ( trm_berkas_analisa.kriteria_nilai = '-' AND trm_berkas_analisa.hold = '0', 1, 0 ) ) AS jumlah_lembar_minus,
                    mjadwal_dokter.jam_dari,
                    mjadwal_dokter.jam_sampai,
                    mjadwal_dokter.idruangan,
                    mruangan.nama AS ruangan
                FROM
                    trm_berkas_analisa
                    JOIN trm_layanan_berkas ON trm_layanan_berkas.id = trm_berkas_analisa.idberkas
                    JOIN trm_berkas_analisa_dokter ON trm_berkas_analisa_dokter.idberkas = trm_berkas_analisa.idberkas 
                    AND trm_berkas_analisa_dokter.idanalisa = trm_berkas_analisa.idanalisa 
                    AND trm_berkas_analisa_dokter.idlembaran = trm_berkas_analisa.idlembaran
                    JOIN mjadwal_dokter ON mjadwal_dokter.iddokter = trm_berkas_analisa_dokter.iddokter
                    JOIN mruangan ON mruangan.id = mjadwal_dokter.idruangan 
                    AND mjadwal_dokter.hari = ( CASE DAYOFWEEK( NOW() ) WHEN 1 THEN 'Minggu' WHEN 2 THEN 'Senin' WHEN 3 THEN 'Selasa' WHEN 4 THEN 'Rabu' WHEN 5 THEN 'Kamis' WHEN 6 THEN 'Jumat' WHEN 7 THEN 'Sabtu' END ) 
                    WHERE
                        trm_berkas_analisa.status = '1' $filterTipe $filterDokter $filterTime 
                    GROUP BY
                        trm_berkas_analisa_dokter.iddokter,
                        mjadwal_dokter.idpoliklinik,
                        mjadwal_dokter.idruangan 
                    ORDER BY
                    trm_berkas_analisa.created_at DESC 
                    LIMIT 10
            ) AS result
            GROUP BY 
                result.iddokter,
                result.idruangan 
        ) AS result
        ";

        $this->join = array();

        $this->where = array();

        $this->order = array();

        $this->group = array();

        $this->column_search = array('result.namadokter');
        $this->column_order = array('result.namadokter');

        $list = $this->datatable->get_datatables();

        $data = array();
        foreach ($list as $index => $r) {
            $row = array();

            $row[] = $r->namadokter;
            $row[] = $r->jumlah_lembaran_hold;
            $row[] = $r->jumlah_lembar_minus;
            $row[] = $r->jadwal;
            $row[] = $r->ruangan;
            $row[] = '<div class="btn-group">
				<a href="'.site_url().'trm_pengelolaan_hold/detail/' . $r->iddokter . '/' . $r->idruangan . '" data-toggle="tooltip" title="Lihat Data" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Lihat Data</a>
				<a href="'.site_url().'trm_pengelolaan_hold/print/' . $r->iddokter . '/' . $r->idruangan . '" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i> Print</a>
			</div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );

        echo json_encode($output);
    }

    public function getDetail($uri = 'index', $iddokter, $idurangan)
    {
        $filterNama = '';
        $filterNoMedrec = '';
        $filterTipe = '';
        $filterTanggal = '';
        $filterStatus = array();

        if ($uri == 'filter_detail') {
            $namapasien = $this->session->userdata('namapasien');
            if ($iddokter != null) {
                $filterNama = "AND trm_layanan_berkas.namapasien LIKE '%$namapasien%'";
            }

            $nomedrec = $this->session->userdata('nomedrec');
            if ($nomedrec != null) {
                $filterNoMedrec = "AND trm_layanan_berkas.no_medrec = '$nomedrec'";
            }
            
            $tipe = $this->session->userdata('tipe');
            if ($tipe != '#') {
                $filterTipe = "AND trm_layanan_berkas.tujuan = '$tipe'";
            }

            $tanggalDari = $this->session->userdata('tanggal_dari');
            $tanggalSampai = $this->session->userdata('tanggal_sampai');
            if ($tanggalDari != null && $tanggalSampai != null) {
                $filterTanggal = "AND DATE(trm_layanan_berkas.tanggal_trx) >= '" .YMDFormat($tanggalDari) . "' AND DATE(trm_layanan_berkas.tanggal_trx) <= '" . YMDFormat($tanggalSampai) . "'";
            }

            $status = $this->session->userdata('status');
            if ($status == 'hold') {
                $filterStatus = array('result.status_hold' => '1');
            } elseif ($status == 'non-hold') {
                $filterStatus = array('result.status_hold' => '0');
            } elseif ($status == 'minus') {
                $filterStatus = array('result.status_berkas' => '1');
            } elseif ($status == 'non-minus') {
                $filterStatus = array('result.status_berkas' => '0');
            }
        }

        $this->select = array();

        $this->from = "
        (
            SELECT
                trm_layanan_berkas.idpasien,
                trm_layanan_berkas.id AS idberkas,
                trm_layanan_berkas.id_trx AS idpendaftaran,
                trm_layanan_berkas.tanggal_trx AS tanggal,
                trm_layanan_berkas.no_medrec AS nomedrec,
                trm_layanan_berkas.namapasien,
                (CASE
                    WHEN trm_layanan_berkas.tujuan = '1' THEN 'Rawat Jalan'
                    WHEN trm_layanan_berkas.tujuan = '2' THEN 'Instalasi Gawat Darurat (IGD)'
                    WHEN trm_layanan_berkas.tujuan = '3' THEN 'Rawat Inap'
                    WHEN trm_layanan_berkas.tujuan = '4' THEN 'One Day Surgery (ODS)'
                END) AS tipe,
                (CASE
                    WHEN trm_layanan_berkas.tujuan IN ( 1, 2 ) THEN 'rawatjalan'
                    WHEN trm_layanan_berkas.tujuan IN ( 3, 4 ) THEN 'rawatinap'
                END) AS asalpendaftaran,
                mdokter.id AS iddokter,
                mdokter.nama AS namadokter,
                IF ( trm_berkas_analisa.hold = '1' AND trm_berkas_analisa.kriteria_nilai = '-', 1, 0 ) AS status_hold,
                IF ( SUM( CASE WHEN trm_berkas_analisa.kriteria_nilai = '-' THEN 1 ELSE 0 END) > 0, 1, 0 ) AS status_berkas
            FROM trm_layanan_berkas
            JOIN mdokter ON mdokter.id = trm_layanan_berkas.iddokter
            JOIN trm_berkas_analisa_dokter ON trm_berkas_analisa_dokter.idberkas = trm_layanan_berkas.id AND trm_berkas_analisa_dokter.iddokter = trm_layanan_berkas.iddokter
            JOIN trm_berkas_analisa ON trm_berkas_analisa.idberkas = trm_berkas_analisa_dokter.idberkas AND trm_berkas_analisa.idanalisa = trm_berkas_analisa_dokter.idanalisa AND trm_berkas_analisa.idlembaran = trm_berkas_analisa_dokter.idlembaran
            WHERE trm_layanan_berkas.iddokter = $iddokter $filterNama $filterNoMedrec $filterTipe $filterTanggal
            GROUP BY trm_layanan_berkas.id
        ) AS result
        ";

        $this->join = array();

        $this->where = $filterStatus;

        $this->order = array();

        $this->group = array();

        $this->column_search = array('result.namapasien');
        $this->column_order = array('result.namapasien');

        $list = $this->datatable->get_datatables();


        $data = array();
        foreach ($list as $index => $r) {
            $row = array();

            $row[] = DMYFormat($r->tanggal);
            $row[] = $r->nomedrec;
            $row[] = $r->namapasien;
            $row[] = $r->tipe;
            $row[] = $r->namadokter;
            $row[] = StatusPengelolaanHold($r->status_hold);
            $row[] = StatusPengelolaanHold($r->status_berkas);
            $row[] = '<div class="btn-group">
				<a href="'.site_url().'trm_analisa/proses/' . $r->idpasien . '?tipe=verifikasi&idberkas=' . $r->idberkas . '&idpendaftaran=' . $r->idpendaftaran . '&asalpendaftaran=' . $r->asalpendaftaran . '" target="_blank" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
				<a href="'.site_url().'trm_pengelolaan_hold/print/' . $r->iddokter . '" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
			</div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );

        echo json_encode($output);
    }
}
