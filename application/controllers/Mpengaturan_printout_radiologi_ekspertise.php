<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpengaturan_printout_radiologi_ekspertise extends CI_Controller {

	/**
	 * Pengaturan Printout Radiologi Ekspertise controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpengaturan_printout_radiologi_ekspertise_model');
		$this->load->helper('path');
  }

	function index($tipe = 'FORMAT_1')
	{
		$row = $this->Mpengaturan_printout_radiologi_ekspertise_model->getSpecified($tipe);
		if(isset($row->id)){
			$data = array(
				'id' => $row->id,
				'tipe' => $row->tipe,
				'nama_file' => $row->nama_file,
				'logo' => $row->logo,
				'logo_header' => $row->logo_header,
				'logo_footer' => $row->logo_footer,
				'alamat' => $row->alamat,
				'phone' => $row->phone,
				'website' => $row->website,
				'label_header' => $row->label_header,
				'label_header_eng' => $row->label_header_eng,
				'tampilkan_noregister' => $row->tampilkan_noregister,
				'label_noregister' => $row->label_noregister,
				'label_noregister_eng' => $row->label_noregister_eng,
				'tampilkan_nomedrec' => $row->tampilkan_nomedrec,
				'label_nomedrec' => $row->label_nomedrec,
				'label_nomedrec_eng' => $row->label_nomedrec_eng,
				'tampilkan_nama' => $row->tampilkan_nama,
				'label_nama' => $row->label_nama,
				'label_nama_eng' => $row->label_nama_eng,
				'tampilkan_kelompok_pasien' => $row->tampilkan_kelompok_pasien,
				'label_kelompok_pasien' => $row->label_kelompok_pasien,
				'label_kelompok_pasien_eng' => $row->label_kelompok_pasien_eng,
				'tampilkan_nama_asuransi' => $row->tampilkan_nama_asuransi,
				'label_nama_asuransi' => $row->label_nama_asuransi,
				'label_nama_asuransi_eng' => $row->label_nama_asuransi_eng,
				'tampilkan_alamat' => $row->tampilkan_alamat,
				'label_alamat' => $row->label_alamat,
				'label_alamat_eng' => $row->label_alamat_eng,
				'tampilkan_dokter_perujuk' => $row->tampilkan_dokter_perujuk,
				'label_dokter_perujuk' => $row->label_dokter_perujuk,
				'label_dokter_perujuk_eng' => $row->label_dokter_perujuk_eng,
				'tampilkan_nomor_radiologi' => $row->tampilkan_nomor_radiologi,
				'label_nomor_radiologi' => $row->label_nomor_radiologi,
				'label_nomor_radiologi_eng' => $row->label_nomor_radiologi_eng,
				'tampilkan_tanggal_lahir' => $row->tampilkan_tanggal_lahir,
				'label_tanggal_lahir' => $row->label_tanggal_lahir,
				'label_tanggal_lahir_eng' => $row->label_tanggal_lahir_eng,
				'tampilkan_umur' => $row->tampilkan_umur,
				'label_umur' => $row->label_umur,
				'label_umur_eng' => $row->label_umur_eng,
				'tampilkan_rujukan' => $row->tampilkan_rujukan,
				'label_rujukan' => $row->label_rujukan,
				'label_rujukan_eng' => $row->label_rujukan_eng,
				'tampilkan_barcode' => $row->tampilkan_barcode,
				'label_barcode' => $row->label_barcode,
				'label_barcode_eng' => $row->label_barcode_eng,
				'tampilkan_nama_pemeriksaan' => $row->tampilkan_nama_pemeriksaan,
				'label_nama_pemeriksaan' => $row->label_nama_pemeriksaan,
				'label_nama_pemeriksaan_eng' => $row->label_nama_pemeriksaan_eng,
				'tampilkan_klinis' => $row->tampilkan_klinis,
				'label_klinis' => $row->label_klinis,
				'label_klinis_eng' => $row->label_klinis_eng,
				'tampilkan_kesan' => $row->tampilkan_kesan,
				'label_kesan' => $row->label_kesan,
				'label_kesan_eng' => $row->label_kesan_eng,
				'tampilkan_usul' => $row->tampilkan_usul,
				'label_usul' => $row->label_usul,
				'label_usul_eng' => $row->label_usul_eng,
				'tampilkan_hasil' => $row->tampilkan_hasil,
				'label_hasil' => $row->label_hasil,
				'label_hasil_eng' => $row->label_hasil_eng,
				'tampilkan_flag_kritis' => $row->tampilkan_flag_kritis,
				'label_flag_kritis' => $row->label_flag_kritis,
				'label_flag_kritis_eng' => $row->label_flag_kritis_eng,
				'tampilkan_penanggung_jawab' => $row->tampilkan_penanggung_jawab,
				'label_penanggung_jawab' => $row->label_penanggung_jawab,
				'label_penanggung_jawab_eng' => $row->label_penanggung_jawab_eng,
				'tampilkan_tanda_tangan' => $row->tampilkan_tanda_tangan,
				'label_tanda_tangan' => $row->label_tanda_tangan,
				'label_tanda_tangan_eng' => $row->label_tanda_tangan_eng,
				'tampilkan_waktu_input_order' => $row->tampilkan_waktu_input_order,
				'label_waktu_input_order' => $row->label_waktu_input_order,
				'label_waktu_input_order_eng' => $row->label_waktu_input_order_eng,
				'tampilkan_waktu_pemeriksaan' => $row->tampilkan_waktu_pemeriksaan,
				'label_waktu_pemeriksaan' => $row->label_waktu_pemeriksaan,
				'label_waktu_pemeriksaan_eng' => $row->label_waktu_pemeriksaan_eng,
				'tampilkan_waktu_expertise' => $row->tampilkan_waktu_expertise,
				'label_waktu_expertise' => $row->label_waktu_expertise,
				'label_waktu_expertise_eng' => $row->label_waktu_expertise_eng,
				'tampilkan_waktu_cetak' => $row->tampilkan_waktu_cetak,
				'label_waktu_cetak' => $row->label_waktu_cetak,
				'label_waktu_cetak_eng' => $row->label_waktu_cetak_eng,
				'tampilkan_jumlah_cetak' => $row->tampilkan_jumlah_cetak,
				'label_jumlah_cetak' => $row->label_jumlah_cetak,
				'label_jumlah_cetak_eng' => $row->label_jumlah_cetak_eng,
				'tampilkan_user_input_order' => $row->tampilkan_user_input_order,
				'label_user_input_order' => $row->label_user_input_order,
				'label_user_input_order_eng' => $row->label_user_input_order_eng,
				'tampilkan_user_pemeriksaan' => $row->tampilkan_user_pemeriksaan,
				'label_user_pemeriksaan' => $row->label_user_pemeriksaan,
				'label_user_pemeriksaan_eng' => $row->label_user_pemeriksaan_eng,
				'tampilkan_user_expertise' => $row->tampilkan_user_expertise,
				'label_user_expertise' => $row->label_user_expertise,
				'label_user_expertise_eng' => $row->label_user_expertise_eng,
				'tampilkan_user_cetak' => $row->tampilkan_user_cetak,
				'label_user_cetak' => $row->label_user_cetak,
				'label_user_cetak_eng' => $row->label_user_cetak_eng,
				'footer_notes' => $row->footer_notes,
				'footer_notes_eng' => $row->footer_notes_eng,
			);

			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Pengaturan Printout Radiologi Ekspertise';
			$data['content']	 	= 'Mpengaturan_printout_radiologi_ekspertise/index';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Pengaturan Printout Radiologi Ekspertise",'#'),
															array("Ubah",'Mpengaturan_printout_radiologi_ekspertise')
														);
			
			$data['tab'] = $tipe;
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('Mpengaturan_printout_radiologi_ekspertise','location');
		}
	}

	function save()
	{
		if($this->Mpengaturan_printout_radiologi_ekspertise_model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('Mpengaturan_printout_radiologi_ekspertise/index/' . $this->input->post('tipe'),'location');
		}
	}

	public function updateDataLabel() {
			// Ambil data dari permintaan AJAX
			$tipe = $this->input->post('tipe');
			$labelId = $this->input->post('labelId');
			$labelEngId = $this->input->post('labelEngId');
			$textareaContent = $this->input->post('textareaContent');
			$textareaEngContent = $this->input->post('textareaEngContent');

			// Menyusun data untuk diupdate
			$data = array(
					$labelId => $textareaContent,
					$labelEngId => $textareaEngContent,
			);

			// Panggil method model untuk melakukan update
			$result = $this->Mpengaturan_printout_radiologi_ekspertise_model->updateDataLabel($tipe, $data);

			// Berikan respons ke klien (browser)
			if ($result) {
					echo json_encode(array('status' => 'success', 'message' => 'Data berhasil diupdate.'));
			} else {
					echo json_encode(array('status' => 'error', 'message' => 'Gagal mengupdate data.'));
			}
	}
}
