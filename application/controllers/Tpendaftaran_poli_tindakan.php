<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use Hashids\Hashids;
class Tpendaftaran_poli_tindakan extends CI_Controller {

	/**
	 * Checkin controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbooking_rehab_model');
		$this->load->model('Tbooking_model');
		$this->load->model('Tpendaftaran_poli_tindakan_model');
		$this->load->model('Tpoliklinik_pendaftaran_model');
		$this->load->model('Tcheckin_model');
		$this->load->helper('path');
		
  }
	function index($tab='1'){
			$log['path_tindakan']='tpendaftaran_poli_tindakan';
			$this->session->set_userdata($log);
		    $this->db->query("UPDATE tpoliklinik_pendaftaran INNER JOIN (
			SELECT H.id,H.tanggal,H.kode_antrian,K.kodeantrian_poli
			,CONCAT(COALESCE(K.kodeantrian_poli,''),COALESCE(KD.kodeantrian_dokter,''),LPAD(H.noantrian,3,'0')) as kode_baru
			FROM tpoliklinik_pendaftaran H
			LEFT JOIN antrian_poli_kode K ON K.idpoli=H.idpoliklinik
			LEFT JOIN antrian_poli_kode_dokter KD ON KD.idpoli=H.idpoliklinik AND KD.iddokter=H.iddokter
			WHERE H.kode_antrian IS NULL AND H.reservasi_tipe_id > 0
			) T ON T.id=tpoliklinik_pendaftaran.id
			SET tpoliklinik_pendaftaran.kode_antrian=T.kode_baru");

		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// if ($tab=='0'){			
			// if (UserAccesForm($user_acces_form,array('1566'))){
				// $tab='1';
			// }elseif (UserAccesForm($user_acces_form,array('1567'))){
				// $tab='2';
			// }elseif (UserAccesForm($user_acces_form,array('1568'))){
				// $tab='3';
			// }elseif (UserAccesForm($user_acces_form,array('1569'))){
				// $tab='4';
			// }
		// }
		if (UserAccesForm($user_acces_form,array('1584'))){
			$data = array(
				
			);
			$DL =$this->Tpendaftaran_poli_tindakan_model->get_data_login();
			if ($DL){
				$data=$DL;
			}else{
				$data['st_login']='0';
				$data['tanggal_login']='';
				$data['iddokter']='#';
				$data['idpoli']='#';
				$data['ruangan_id']='#';
			}
			$data['list_ruangan'] 			= $this->Tpendaftaran_poli_tindakan_model->list_ruangan();
			$data['list_poli'] 			= $this->Tpendaftaran_poli_tindakan_model->list_poli();
			$data['list_dokter'] 			= $this->Tpendaftaran_poli_tindakan_model->list_dokter();
			
			$data['no_medrec'] 			= '';
			$data['namapasien'] 			= '';
			$data['tab'] 			= $tab;
			
			$data['tanggal'] 			= DMYFormat(date('Y-m-d'));
			$data['error'] 			= '';
			$data['title'] 			= 'Antrian Layanan Poliklinik';
			$data['content'] 		= 'Tpendaftaran_poli_tindakan/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Tindakan Poliklinik",'#'),
												  array("Panggil Antrian",'tpendaftaran_poli_tindakan')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function login(){
		$ruangan_id=$this->input->post('ruangan_id');
		$idpoli=$this->input->post('idpoli');
		$iddokter=$this->input->post('iddokter');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		$data['iddokter']=$iddokter;
		$data['idpoli']=$idpoli;
		$data['user_login']=$user_id;
		$data['user_nama_login']=$user_nama_login;
		$data['tanggal_login']=date('Y-m-d H:i:s');
		$data['st_login']=1;
		$this->db->where('id',$ruangan_id);
		$hasil=$this->db->update('mtujuan',$data);
		// print_r($ruangan_id);
		$this->output->set_output(json_encode($hasil));
		
	}
		function logout(){
		$ruangan_id=$this->input->post('ruangan_id');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		$data['user_login']=null;
		$data['user_nama_login']=null;
		$data['tanggal_login']='';
		$data['st_login']=0;
		// print_r($ruangan_id);
		$this->db->where('id',$ruangan_id);
		$hasil=$this->db->update('mtujuan',$data);
		$this->output->set_output(json_encode($hasil));
		
	}
	function getIndex_all()
	{
			$this->db->query("UPDATE tpoliklinik_pendaftaran INNER JOIN (
			SELECT H.id,H.tanggal,H.kode_antrian,K.kodeantrian_poli
			,CONCAT(COALESCE(K.kodeantrian_poli,''),COALESCE(KD.kodeantrian_dokter,''),LPAD(H.noantrian,3,'0')) as kode_baru
			FROM tpoliklinik_pendaftaran H
			LEFT JOIN antrian_poli_kode K ON K.idpoli=H.idpoliklinik
			LEFT JOIN antrian_poli_kode_dokter KD ON KD.idpoli=H.idpoliklinik AND KD.iddokter=H.iddokter
			WHERE H.kode_antrian IS NULL AND H.reservasi_tipe_id > 0
			) T ON T.id=tpoliklinik_pendaftaran.id
			SET tpoliklinik_pendaftaran.kode_antrian=T.kode_baru");


			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$no_medrec =$this->input->post('no_medrec');
			$st_login =$this->input->post('st_login');
			$namapasien =$this->input->post('namapasien');
			$idpoli =$this->input->post('idpoli');
			$iddokter =$this->input->post('iddokter');
			$ruangan_id =$this->input->post('ruangan_id');
			$tanggal =YMDFormat($this->input->post('tanggal'));
			$tab =$this->input->post('tab');
			if ($no_medrec!=''){
				$where .=" AND (H.no_medrec) = '$no_medrec'";
			}
			if ($namapasien!=''){
				$where .=" AND (H.namapasien) LIKE '%".$namapasien."%'";
			}
			if ($tab=='2'){
				$where .=" AND (H.st_panggil) = '0'";
			}
			if ($tab=='3'){
				$where .=" AND (H.st_panggil) = '1'";
			}
			if ($tab=='4'){
				$where .=" AND (H.st_panggil) = '2'";
			}
			
			// print_r($tab);exit;
			$this->select = array();
			$from="
					(
						SELECT H.id,H.reservasi_tipe_id,H.nopendaftaran,H.tanggal,H.tanggaldaftar,H.idtipe,H.alamatpasien
						,H.no_medrec,H.namapasien,H.idpasien
						,CASE WHEN H.idtipe=1 THEN 'POLIKLINIK' ELSE 'IGD' END as tipe_nama
						,H.reservasi_cara,H.statuspasienbaru,H.noantrian
						,H.idpoliklinik,MP.nama as nama_poli,H.iddokter,MD.nama as nama_dokter
						,CASE WHEN H.reservasi_tipe_id=1 
							THEN CONCAT(DATE_FORMAT(MJ.jam_dari,'%H:%i') ,' - ',DATE_FORMAT(MJ.jam_sampai,'%H:%i')) 
							ELSE  CONCAT(MJ2.jam ,'.00 - ',MJ2.jam_akhir,'.00') END as jadwal 
						,H.jadwal_id,CASE WHEN H.idkelompokpasien=1 THEN MR.nama ELSE MK.nama END as nama_kelompok
						,(H.st_general+H.st_skrining+H.st_covid) as jml_skrining,H.status_reservasi
						,H.status,H.st_gc,H.st_general,H.st_sp,H.st_skrining,H.st_sc,H.st_covid,H.kode_antrian
						,H.statustindakan,H.st_panggil	
						FROM tpoliklinik_pendaftaran H
						INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
						LEFT JOIN mdokter MD ON MD.id=H.iddokter
						LEFT JOIN mjadwal_dokter MJ ON H.reservasi_tipe_id='1' AND MJ.id=H.jadwal_id
						LEFT JOIN merm_jam MJ2 ON MJ2.jam_id=H.jadwal_id
						LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						WHERE H.idpoliklinik='$idpoli' ANd H.iddokter='$iddokter'
						AND H.tanggal='$tanggal' ".$where."
						
						ORDER BY H.tanggal DESC
					) as tbl WHERE id IS NOT NULL 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $label_skrining='';
		  $disabel_verif='';
		  // if ($r->jml_skrining<3){
			  // $label_skrining='<br><br>'.'<a href="'.base_url().'tpendaftaran_poli_tindakan/edit_pendaftaran/'.$r->id.'" type="button" data-toggle="tooltip" title="BELUM SKRINING" class="btn btn-danger btn-xs"><i class="fa fa-eye"></i> BELUM SKRINING</a>';;
			  // // $disabel_verif='disabled';
		  // }
		  $label_skrining='<br><br>'.label_skrining($r->st_gc,$r->st_general,$r->st_sp,$r->st_skrining,$r->st_sc,$r->st_covid);
          $result[] = $no;
          $result[] = $r->kode_antrian.'<br>'.$r->nopendaftaran;
          $result[] = HumanDateShort($r->tanggal).'<br>'.$r->jadwal;
          $result[] ='<strong>'. $r->no_medrec.'</strong><br>'.($r->namapasien);
          $result[] = ($r->alamatpasien);
          $result[] ='<strong>'. $r->nama_poli.'</strong><br>'.($r->nama_dokter);
          $result[] = $r->nama_kelompok;
          $result[] = '';
          $result[] = StatusPanggil($r->st_panggil);
          $result[] = ($r->status!='0'?status_reservasi($r->status_reservasi).$label_skrining:text_danger('Dibatalkan'));
          $aksi = '<div class="btn-group">';
			$aksi .= '<a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->id.'/erm_rj/input_cppt" data-toggle="tooltip" title="Tindak" class="btn btn-primary btn-xs"><i class="fa fa-user-md"></i> Layani</a>';
			$aksi .= '<button type="button" data-toggle="tooltip" title="Call" onclick="call_antrian_manual('.$r->id.')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-volume-up"></i></button>';
			$aksi .=div_panel_kendali($user_acces_form,$r->id) ;
			
		  $aksi .= '</div>';
		  if ($st_login=='0'){
			  $aksi='';
		  }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  public function get_dokter_poli()
	{
		$idpoli=$this->input->post('idpoli');
		$q="SELECT MD.id,MD.nama,CASE WHEN MT.id IS NOT NULL THEN 'disabled' ELSE '' END as disabel,MT.user_nama_login,MT.nama_tujuan FROM mpoliklinik_dokter H
			INNER JOIN mdokter MD ON MD.id=H.iddokter AND MD.`status`='1'
			LEFT JOIN mtujuan MT ON MT.st_login='1' AND MT.iddokter=H.iddokter AND MT.idpoli=H.idpoliklinik
			WHERE H.`status`='1' AND H.idpoliklinik='$idpoli'
			";
		$hasil = $this->db->query($q)->result();
		$opsi='<option value="#" selected>-Pilih Dokter-</option>';
		foreach($hasil as $r){
			$opsi .='<option value="'.$r->id.'" '.$r->disabel.'>'.$r->nama.($r->disabel?' ('.$r->nama_tujuan.' - '.$r->user_nama_login.')':'').'</option>';
		}
		$this->output->set_output(json_encode($opsi));
	}
	function refres_div_counter(){
		$idpoli =$this->input->post('idpoli');
		$iddokter =$this->input->post('iddokter');
		$ruangan_id =$this->input->post('ruangan_id');
		$st_login =$this->input->post('st_login');
		$tanggal =YMDFormat($this->input->post('tanggal'));
		$id_next='0';
		$id_layani='0';
		$div_tabel='';
		$q="SELECT H.id,H.namapasien,H.no_medrec,H.kode_antrian,H.noantrian FROM `mtujuan` M
			INNER JOIN tpoliklinik_pendaftaran H ON H.id=M.pendaftaran_dilayani
			WHERE M.id='$ruangan_id' AND antrian_tanggal='$tanggal'";
		$data_next=$this->db->query($q)->row_array();
		if ($data_next){
			$data_next['button_disabel']='';
			$id_layani=$data_next['id'];
		}else{
			$data_next=array(
				'noantrian'=>'',
				'id'=>'',
				'kode_antrian'=>'-',
				'no_medrec'=>'-',
				'namapasien'=>'-',
				'button_disabel'=>'disabled',
			);
		}
		if ($st_login=='0'){
			$data_next=array(
				'noantrian'=>'',
				'id'=>'',
				'kode_antrian'=>'-',
				'no_medrec'=>'-',
				'namapasien'=>'-',
				'button_disabel'=>'disabled',
			);
		}
		$div_tabel.='
			<div class="col-sm-3">
				<div class="block block-themed">
					<div class="block-header bg-danger" style="border-bottom: 2px solid white;padding:10">
						<h3 class="block-title text-center" >ANTRIAN DILAYANI</h3>
					</div>
					<div class="content-mini content-mini-full bg-danger">
						<div>
							<div class="h2 text-center text-white">'.$data_next['kode_antrian'].'</div>
							<div class="h5 text-center text-white">'.$data_next['no_medrec'].'</div>
							<div class="h5 text-center text-white push-10">'.$data_next['namapasien'].'</div>
							<div class="text-center ">
							</div>
						</div>
						<div class="text-center ">
								<button class="btn btn-block btn-success" '.$data_next['button_disabel'].' type="button" onclick="recall_manual('.$data_next['id'].')" title="Panggil Ulang"><i class="si si-refresh pull-left"></i> PANGGIL ULANG</button>
						</div>
					</div>
				</div>
			</div>
		';
		$q="SELECT H.id,H.namapasien,H.no_medrec,H.kode_antrian,H.noantrian FROM tpoliklinik_pendaftaran H

			WHERE H.tanggal='$tanggal' 
			AND H.iddokter='$iddokter' AND H.idpoliklinik='$idpoli'
			AND H.st_panggil='0'

			ORDER BY H.noantrian ASC
			LIMIT 1
			";
		$data_next=$this->db->query($q)->row_array();
		if ($data_next){
			$data_next['button_disabel']='';
			$id_next=$data_next['id'];
		}else{
			$data_next=array(
				'noantrian'=>'',
				'id'=>'',
				'kode_antrian'=>'-',
				'no_medrec'=>'-',
				'namapasien'=>'-',
				'button_disabel'=>'disabled',
			);
		}
		if ($st_login=='0'){
			$data_next=array(
				'noantrian'=>'',
				'id'=>'',
				'kode_antrian'=>'-',
				'no_medrec'=>'-',
				'namapasien'=>'-',
				'button_disabel'=>'disabled',
			);
		}
		$div_tabel.='<div class="col-sm-3">
							<div class="block block-themed">
								<div class="block-header bg-success" style="border-bottom: 2px solid white;padding:10">
									<h3 class="block-title text-center" >ANTRIAN SELANJUTNYA</h3>
								</div>
								<div class="content-mini content-mini-full bg-success">
									<div>
										<div class="h2 text-center text-white">'.$data_next['kode_antrian'].'</div>
										<div class="h5 text-center text-white">'.$data_next['no_medrec'].'</div>
										<div class="h5 text-center text-white push-10">'.$data_next['namapasien'].'</div>
									</div>
									<div class="text-center ">
										<button class="btn btn-block btn-primary " onclick="call_antrian_manual('.$data_next['id'].')" '.$data_next['button_disabel'].' title="Lanjut Panggil" type="button"><i class="glyphicon glyphicon-volume-up pull-left"></i> '.($id_layani=='0'?'PANGGIL':'SELANJUTNYA').'</button>
									</div>
								</div>
								
							</div>
							
						</div>';
		$q="SELECT SUM(1) as total_antrian 
			,SUM(CASE WHEN H.st_panggil='0' THEN 1 ELSE 0 END) as sisa_antrian
			FROM tpoliklinik_pendaftaran H

			WHERE H.tanggal='$tanggal' 
			AND H.iddokter='$iddokter' AND H.idpoliklinik='$idpoli'

			ORDER BY H.noantrian ASC

			";
		$data_next=$this->db->query($q)->row_array();
		if ($id_layani=='0'){
			$button_disabel='disabled';
		}else{
			$button_disabel='';
			
		}
		if ($data_next['sisa_antrian']=='0'){
			$button_disabel='disabled';
			
		}
		if ($st_login=='0'){
			$button_disabel='disabled';
		}
		$div_tabel.='
			<div class="col-sm-3">
				<div class="block block-themed">
					<div class="block-header bg-success" style="border-bottom: 2px solid white;padding:10">
						<h3 class="block-title text-center" >SISA ANTRIAN</h3>
					</div>
					<div class="content-mini content-mini-full bg-success">
						<div>
							<div class="h2 text-center text-white">'.$data_next['sisa_antrian'].'</div>
							<div class="h5 text-center text-white">&nbsp;</div>
							<div class="h5 text-center text-white push-10">&nbsp;</div>
						</div>
						<div class="text-center ">
							<button class="btn btn-block btn-warning" type="button" '.$button_disabel.' onclick="call_lewati('.$id_layani.','.$id_next.')"  title="LEWATI"><i class="fa fa-mail-forward pull-left"></i> LEWATI</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-sm-3">
				<div class="block block-themed">
					<div class="block-header bg-success" style="border-bottom: 2px solid white;padding:10">
						<h3 class="block-title text-center" >TOTAL ANTRIAN</h3>
					</div>
					<div class="content-mini content-mini-full bg-success">
						<div>
							<div class="h1 font-w700 text-center text-white">'.$data_next['total_antrian'].'</div>
						</div>
					</div>
				</div>
			</div>
		';
		$this->output->set_output(json_encode($div_tabel));
	}
	function call_antrian(){
		$antrian_id=$this->input->post('antrian_id');
		
		$data=$this->db->query("SELECT H.id,H.noantrian,H.kode_antrian,H.idpoliklinik,H.iddokter,H.tanggal FROM tpoliklinik_pendaftaran H WHERE H.id='$antrian_id'")->row();
		$idpoli=$data->idpoliklinik;
		$iddokter=$data->iddokter;
		$noantrian=$data->noantrian;
		$tanggal=$data->tanggal;
		$ruangan_id=$this->input->post('ruangan_id');
		// $noantrian=$this->input->post('noantrian');
		// $tanggal='$tanggal';
		$q2="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM mtujuan_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.tujuan_id='$ruangan_id' AND H.`status`='1'
			ORDER BY H.nourut ASC";
		$file_3=$this->db->query($q2)->row('file');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		
		if ($data){
			$kode_number=str_pad($noantrian,3,"0",STR_PAD_LEFT);
			// $this->db->where('id',$data->id);
			// $this->db->update('antrian_harian',array('st_panggil'=>1));
			$sound_play='';
			// $pelayanan_id=$data->antrian_id;
			
			
			$q1="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_poli_kode_dokter H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_dokter_id
			WHERE H.idpoli='$idpoli' AND H.iddokter='$iddokter'";
			$file_dokter=$this->db->query($q1)->row('file');
			
			$q1="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_poli_kode_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.idpoli='$idpoli' AND H.`status`='1'
			ORDER BY H.nourut ASC";
			$file_1=$this->db->query($q1)->row('file');
			if ($file_dokter){
				$file_1=$file_1.':'.$file_dokter;
			}
			
			$file_2=terbilang_sound($kode_number);
			$sound_play=$file_1.':'.$file_2.':'.$file_3;
			$data_insert=array(
				'pendaftaran_id' => $data->id,
				'idpoli' => $idpoli,
				'iddokter' => $iddokter,
				'ruangan_id' => $ruangan_id,
				'kode_antrian' => $data->kode_antrian,
				'noantrian' => $data->noantrian,
				// 'kode_number' => $data->kode_number,
				'sound_play' => $sound_play,
				'tanggal' => $data->tanggal,
				'waktu_panggil' => date('Y-m-d H:i:s'),
				'user_call' => $user_id,
				'user_call_nama' => $user_nama_login,
			);
			$this->db->insert('antrian_harian_poli_call',$data_insert);
			$hasil=$data_insert;
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
		
	}
	function call_lewati(){
		$antrian_id=$this->input->post('antrian_id_next');
		$antrian_id_skip=$this->input->post('antrian_id');
		$data_update=array(
			'st_panggil'=>2,
			'waktu_dilayani'=>date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$antrian_id_skip);
		$this->db->update('tpoliklinik_pendaftaran',$data_update);
		$data=$this->db->query("SELECT H.id,H.noantrian,H.kode_antrian,H.idpoliklinik,H.iddokter,H.tanggal FROM tpoliklinik_pendaftaran H WHERE H.id='$antrian_id'")->row();
		$idpoli=$data->idpoliklinik;
		$iddokter=$data->iddokter;
		$noantrian=$data->noantrian;
		$tanggal=$data->tanggal;
		$ruangan_id=$this->input->post('ruangan_id');
		// $noantrian=$this->input->post('noantrian');
		// $tanggal='$tanggal';
		$q2="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM mtujuan_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.tujuan_id='$ruangan_id' AND H.`status`='1'
			ORDER BY H.nourut ASC";
		$file_3=$this->db->query($q2)->row('file');
		$user_id=$this->session->userdata('user_id');
		$user_nama_login=$this->session->userdata('user_name');
		
		if ($data){
			$kode_number=str_pad($noantrian,3,"0",STR_PAD_LEFT);
			// $this->db->where('id',$data->id);
			// $this->db->update('antrian_harian',array('st_panggil'=>1));
			$sound_play='';
			// $pelayanan_id=$data->antrian_id;
			
			
			$q1="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_poli_kode_dokter H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_dokter_id
			WHERE H.idpoli='$idpoli' AND H.iddokter='$iddokter'";
			$file_dokter=$this->db->query($q1)->row('file');
			
			$q1="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') as file
			FROM antrian_poli_kode_sound H
			INNER JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.idpoli='$idpoli' AND H.`status`='1'
			ORDER BY H.nourut ASC";
			$file_1=$this->db->query($q1)->row('file');
			if ($file_dokter){
				$file_1=$file_1.':'.$file_dokter;
			}
			
			$file_2=terbilang_sound($kode_number);
			$sound_play=$file_1.':'.$file_2.':'.$file_3;
			$data_insert=array(
				'pendaftaran_id' => $data->id,
				'idpoli' => $idpoli,
				'iddokter' => $iddokter,
				'ruangan_id' => $ruangan_id,
				'kode_antrian' => $data->kode_antrian,
				'noantrian' => $data->noantrian,
				// 'kode_number' => $data->kode_number,
				'sound_play' => $sound_play,
				'tanggal' => $data->tanggal,
				'waktu_panggil' => date('Y-m-d H:i:s'),
				'user_call' => $user_id,
				'user_call_nama' => $user_nama_login,
			);
			$this->db->insert('antrian_harian_poli_call',$data_insert);
			$hasil=$data_insert;
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
		
	}
	//HAPUS
	
}
