<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/
require 'vendor/autoload.php';
use Dompdf\Dompdf;
use Dompdf\Options;
class Tunit_pengembalian extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('tunit_pengembalian_model','model');
        $this->load->model('munitpelayanan_model', 'unitpelayanan');
        $this->load->model('tunit_permintaan_model', 'tunit_permintaan_model');
        
	}

	function _list($data)
    {
        $this->session->set_userdata($data);
        $data['error'] = '';
        $data['title'] = 'Pengembalian Obat';
        $data['content'] = 'Tunit_pengembalian/index';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Pengembalian Obat", '#'),
            array("List", 'tunit_pengembalian')
        );
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
    public function index() {
        $data = array(
            'iddariunit' => '#',
            'nmdariunit' => '',
            'statuspenerimaan' => '#',
            'tanggaldari' => '',
            'tanggalsampai' => '',
        );
        // $this->_list($data);
		// print_r($data);exit();
		$data['list_dari']=$this->tunit_permintaan_model->selectdariunit_index();
		$this->session->set_userdata($data);
        $data['error'] = '';
        $data['title'] = 'Pengembalian Obat';
        $data['content'] = 'Tunit_pengembalian/index';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Pengembalian Obat", '#'),
            array("List", 'tunit_pengembalian')
        );
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filter() {
        $data = array(
            'iddariunit' => $this->input->post('iddariunit'),
            'nmdariunit' => "",
            'statuspenerimaan' => $this->input->post('statuspenerimaan'),
            'tanggaldari' => $this->input->post('tanggaldari'),
            'tanggalsampai' => $this->input->post('tanggalsampai'),
        );
		$data['list_dari']=$this->tunit_permintaan_model->selectdariunit_index();
		$this->session->set_userdata($data);
        $data['error'] = '';
        $data['title'] = 'Filter Pengembalian Obat';
        $data['content'] = 'Tunit_pengembalian/index';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Pengembalian Obat", '#'),
            array("List", 'tunit_pengembalian')
        );
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function ajax_list(){
        $iddariunit = $this->session->userdata('iddariunit');
        $status= $this->session->userdata('statuspenerimaan');
        $tanggaldari = $this->session->userdata('tanggaldari');
        $tanggalsampai = $this->session->userdata('tanggalsampai');
        
        $user_login=$this->session->userdata('user_id');
		
		$str_where="WHERE CONCAT(T.D_user,T.K_user)<>'00' ";
		 if ($iddariunit!="#") {
            $str_where.=" AND id_unit_dari=".$iddariunit;
         }
        if ($status!="#" && $status!="") {
			if ($status=="00"){
				$str_where.=" AND status='0'";
			}else{
				$str_where.=" AND status='".$status."'";
			}
         }
		 if ('' != $tanggaldari) {
            $str_where .= " AND DATE(tanggal) >='".YMDFormat($tanggaldari)."'";
        }
        if ('' != $tanggalsampai) {
            $str_where .= " AND DATE(tanggal) <='".YMDFormat($tanggalsampai)."'";
        }
        $from = "(SELECT *FROM (SELECT H.id,H.nopengembalian,H.tanggal,H.alasan,H.idunitpelayanan as id_unit_dari,UD.nama as unit_dari,H.idpermintaan as id_unit_ke,
				UK.nama as unit_ke,SUM(D.kuantitas) as tot_kembali,H.status,
				SUM(D.kuantitas_terima) as tot_terima,
				COALESCE(USD.userid,0) as D_user,COALESCE(USK.userid,0) as K_user
				FROM tunit_pengembalian H
				LEFT JOIN tunit_pengembalian_detail D ON H.id=D.idpengembalian AND D.status='1'
				LEFT JOIN munitpelayanan UD ON UD.id=H.idunitpelayanan
				LEFT JOIN munitpelayanan UK ON UK.id=H.idpermintaan
				LEFT JOIN munitpelayanan_user USD ON  USD.idunitpelayanan=H.idunitpelayanan AND USD.userid='$user_login'
				LEFT JOIN munitpelayanan_user USK ON USK.idunitpelayanan=H.idpermintaan AND USK.userid='$user_login'
				GROUP BY H.id) T ".$str_where;
        
        $from .=" ) as tbl ";
		// print_r($from);exit();
        $this->select           = array();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array('id' => 'desc');
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $status     = '';
            $url        = site_url('tunit_pengembalian/');
			
			// $st_terima=$this->cek_user($r->user_dariunit);
			// $st_kirim=$this->cek_user($r->user_keunit);
			if ($r->status=='0'){
				$aksi .='<a href="'.$url.'view/'.$r->id.'" class="btn btn-xs btn-info" title="Lihat Detail"><i class="fa fa-eye"></i></a>';
				
				$status='<label class="label label-danger">Dibatalkan</label>';
			}elseif($r->status=='1'){
				$aksi .='<a href="'.$url.'view/'.$r->id.'" class="btn btn-xs btn-info" title="Lihat Detail"><i class="fa fa-eye"></i></a>';
				if (UserAccesForm($user_acces_form,array('1114'))){
				$aksi .='<a href="'.$url.'cetak_pengembalian/'.$r->id.'" class="btn btn-xs btn-success" title="Cetak Pengembalian" target="blank"><i class="fa fa-print"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1111'))){
					if ($r->D_user != '0'){
						$aksi .='<a class="btn btn-success btn-xs btn-edit-data" title="Edit" href="'.$url.'edit/'.$r->id.'"><i class="glyphicon glyphicon-pencil"></i></a>';
					}
				}
				if ($r->K_user != '0'){
					if (UserAccesForm($user_acces_form,array('1113'))){
					$aksi .='<a class="btn btn-info btn-xs btn-terima" title="Proses Penerimaan" href="'.$url.'terima/'.$r->id.'"><i class="fa fa-sign-out"></i> Terima</a>';
					}
				}
				if (UserAccesForm($user_acces_form,array('1112'))){
				$aksi .='<a class="btn btn-danger btn-xs"  title="Batalkan" href="'.$url.'cancel/'.$r->id.'"  onClick="return confirm(\'Yakin Akan Membatalkan?\')"><i class="glyphicon glyphicon-remove"></i></a>';
				}
				$status='<label class="label label-info">Proses Pengembalian</label>';
			}else{
				$aksi .='<a href="'.$url.'view/'.$r->id.'" class="btn btn-xs btn-info" title="Lihat Detail"><i class="fa fa-eye"></i></a>';
				if (UserAccesForm($user_acces_form,array('1114'))){
				$aksi .='<a href="'.$url.'cetak_pengembalian/'.$r->id.'" class="btn btn-xs btn-success" title="Cetak Pengembalian" target="blank"><i class="fa fa-print"></i></a>';
				}
				$status='<label class="label label-success">Selesai</label>';
			}

            $row[] = $no;
            $row[] = $r->tanggal;
            $row[] = $r->nopengembalian;
            $row[] = $r->unit_dari;
            $row[] = $r->unit_ke;
            $row[] = $r->tot_kembali;
            $row[] = $r->alasan;
            $row[] = $status;
            
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
    function ajax_list_detail($id,$iddetail=null){
        $sql="SELECT a.*,v.kode,v.nama,v.hargabeli,r.idpermintaan,r.tanggal,r.idunitpelayanan,r.alasan,v.namatipe,p.nama as namaunitpelayanan,p2.nama as namaunitpermintaan";
        $sql.=" FROM tunit_pengembalian_detail a";
        $sql.=" JOIN view_barang v on a.idbarang=v.id and v.idtipe=a.idtipe";
        $sql.=" JOIN tunit_pengembalian r ON r.id=a.idpengembalian";
        $sql.=" JOIN munitpelayanan p ON p.id=r.idunitpelayanan";
        $sql.=" JOIN munitpelayanan p2 ON p2.id=r.idpermintaan";
        $sql.=" WHERE a.id IS NOT NULL";
        if($id!=null)
            $sql.=" AND a.idpengembalian= $id";
        if($iddetail!=null)
            $sql.=" AND a.id = $iddetail";
        $this->load->library('datatables');
        $this->datatables->from('('.$sql.') t');
        return print_r($this->datatables->generate());
    }

    function create() {
        $data = getFieldTable('tunit_pengembalian');
        $data['error']          = '';
        $data['title']          = 'Tambah Pengembalian Unit';
        $data['content']        = 'Tunit_pengembalian/create';
        $data['breadcrum']      = array(array("RSKB Halmahera",'#'),
                                    array("Pengembalian Unit",'#'),
                                    array("Create",''));
                                    
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
    function edit($id) {
        if($id) {
            $data = $this->model->edit($id);
            $data['error'] = '';
            $data['title'] = 'Edit Pengembalian Obat';
            $data['content'] = 'Tunit_pengembalian/edit';
            $data['List_barang_edit'] = $this->model->List_barang_edit($id);
			// print_r($data['List_barang_edit']);exit();
            $data['breadcrum'] = array(
                array("RSKB Halmahera", '#'),
                array("Pengembalian Obat", 'tunit_pengembalian'),
                array("Sunting", '#')
            );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            show_404();
        }
    }
    function terima($id) {
        if($id) {
            $data = $this->model->edit($id);
            $data['error'] = '';
            $data['title'] = 'Terima Pengembalian Obat Oleh Gudang';
            $data['content'] = 'Tunit_pengembalian/terima';
            $data['List_barang_edit'] = $this->model->List_barang_edit($id);
			// print_r($data['List_barang_edit']);exit();
            $data['breadcrum'] = array(
                array("RSKB Halmahera", '#'),
                array("Pengembalian Obat", 'tunit_pengembalian'),
                array("Sunting", '#')
            );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            show_404();
        }
    }
    function view($id) {
        if($id) {
            $data = $this->model->edit($id);
            $data['error'] = '';
            $data['title'] = 'Terima Pengembalian Obat Oleh Gudang';
            $data['content'] = 'Tunit_pengembalian/view';
            $data['List_barang_edit'] = $this->model->List_barang_edit($id);
			// print_r($data['List_barang_edit']);exit();
            $data['breadcrum'] = array(
                array("RSKB Halmahera", '#'),
                array("Pengembalian Obat", 'tunit_pengembalian'),
                array("Sunting", '#')
            );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            show_404();
        }
    }
	function cetak_pengembalian($id,$jenis='2'){
		$data=array();
		$data = $this->model->edit($id);
		$data['detail']  = $this->model->List_barang_edit($id);
		// print_r($data);exit();
		$options = new Options();
        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);
		$dompdf->set_option('isRemoteEnabled', TRUE);
		 $html = $this->parser->parse('Tunit_pengembalian/cetak_pengembalian',array_merge($data,backend_info()),TRUE);
        $html = $this->parser->parse_string($html,$data);
        // print_r($html);exit();
		$dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
		$dompdf->output();
        $dompdf->render();
        $dompdf->stream('Cetak Permintaan.pdf', array("Attachment"=>0));

	}
	function save(){
        $this->model->save();
        redirect("tunit_pengembalian","location");
    }
	public function save_edit(){
		if ($this->model->save_edit()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
            redirect('tunit_pengembalian/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tunit_pengembalian/index', 'refresh');
        }
		// print_r($this->input->post());exit();
	}
    public function save_terima(){
		if ($this->model->save_terima()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
            redirect('tunit_pengembalian/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tunit_pengembalian/index', 'refresh');
        }
		// print_r($this->input->post());exit();
	}
    function generate_no(){
        print_r($this->model->generate_no());exit();
    }
    function update($id){
        $this->model->update($id);
        redirect("tunit_pengembalian","location");
	}

    function getListBarang($id){
        if($id) {
            $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');

            $this->load->library('datatables');
            $this->datatables->select('
                tblbarang.id, 
                tblbarang.kode,
                tblbarang.nama,
                tblbarang.hargadasar as hargabarang,
                tblbarang.marginumum as marginbarang,
                IF(tblbarang.hargabeli IS NULL,0,tblbarang.hargabeli) as hargabeli,
                gs.stok
            ');
            $this->datatables->join($table[$id] . ' as tblbarang','tblbarang.id = gs.idbarang');
            $this->datatables->where('gs.idunitpelayanan',$this->input->post('idunit'));
            $this->datatables->from('mgudang_stok gs');
            return print_r($this->datatables->generate());
        }
    }

    function getIdPermintaan() {
        $idbarang = $this->input->post('idbarang');
        $idtipe = $this->input->post('idtipe');
        if($idbarang && $idtipe) {
            $result = $this->model->getIdPermintaan($idbarang, $idtipe);
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode(array('status' => '400')));
        }
    }

	function getNoPermintaan() {
		$result = $this->model->getNoPermintaan();
		$this->output->set_output(json_encode($result));
	}

    function acc($id) {
        if($id) {
            $this->db->update('tunit_pengembalian', array('status' => 2), array('id' => $id) );
            $this->session->set_flashdata('confirm',true);
            $this->session->set_flashdata('message_flash','data telah dikonfirmasi.');
            redirect('tunit_pengembalian','location');
        } else {
            $this->session->set_flashdata('error',true);
            $this->session->set_flashdata('message_flash','data gagal dikonfirmasi.');
            redirect('tunit_pengembalian','location');            
        }
    }

    public function get_tipe() {
        $this->db->select("
            idtipe AS id,
            CASE WHEN idtipe = 1 THEN 'Alkes'
            WHEN idtipe = 2 THEN 'Implant'
            WHEN idtipe = 3 THEN 'Obat'
            ELSE 'Logistik'
            END text            
        ");
        $this->db->from('munitpelayanan_tipebarang');
        $this->db->where('idunitpelayanan', $this->input->post('idunit'));
        $res = $this->db->get()->result();
        $this->output->set_output(json_encode($res));
    }

    public function get_gudang() {
        // $gudangtipe = $this->input->post('gudangtipe');
        // $this->db->select('id, nama as text');
        // if($gudangtipe == 4) $this->db->where_in('id', array(49) );
        // else $this->db->where_in('id', array(0) );
        // $get = $this->db->get('munitpelayanan');
        // $this->output->set_output(json_encode($get->result()));
		
        $idunit = $this->input->post('idunit');
		$q="SELECT M.id,M.nama as text from munitpelayanan_permintaan H
		LEFT JOIN munitpelayanan M ON M.id=H.idunitpenerima
		WHERE idunitpeminta='$idunit'";
		$this->output->set_output(json_encode($this->db->query($q)->result()));
    }

    function get_unit() {
        $id = $this->uri->segment(3);
        $res = array();
        if($id) {
            $this->db->select('munitpelayanan.id, munitpelayanan.nama as text');
            $this->db->where('munitpelayanan_user.userid', $this->session->userdata('user_id'));
            $this->db->join('munitpelayanan', 'munitpelayanan_user.idunitpelayanan = munitpelayanan.id', 'left');
            $this->db->where('munitpelayanan.id', $id);
            $res = $this->db->get('munitpelayanan_user')->row();
        } else {
            $this->db->select('munitpelayanan.id, munitpelayanan.nama as text');
            $this->db->where('munitpelayanan_user.userid', $this->session->userdata('user_id'));
            $this->db->join('munitpelayanan', 'munitpelayanan_user.idunitpelayanan = munitpelayanan.id', 'left');
            if ($this->input->get('q')) {
                $this->db->like('munitpelayanan.nama', $this->input->get('q'), 'BOTH');
            }
            $res = $this->db->get('munitpelayanan_user')->result();
        }
        $this->output->set_output(json_encode($res));
    }

    function cancel($id) {
        if($id) {
            $this->db->update('tunit_pengembalian', array('status' => 0,'tgl_deleted' => date('Y-m-d H:i:s'),'iduser_deleted' => $this->session->userdata("user_id"),'namauser_deleted' => $this->session->userdata("user_name")), array('id' => $id) );
            $this->session->set_flashdata('confirm',true);
            $this->session->set_flashdata('message_flash','data telah dibatalkan.');
            redirect('tunit_pengembalian','location');            
        } else {
            $this->session->set_flashdata('error',true);
            $this->session->set_flashdata('message_flash','data gagal dibatalkan.');
            redirect('tunit_pengembalian','location');
        }
    }
    function delete($id) {
        if($id) {
            $this->model->delete($id);
            $this->session->set_flashdata('confirm',true);
            $this->session->set_flashdata('message_flash','data telah dihapus.');
            redirect('tunit_pengembalian','location');            
        } else {
            $this->session->set_flashdata('error',true);
            $this->session->set_flashdata('message_flash','data gagal dihapus.');
            redirect('tunit_pengembalian','location');
        }
    }


    function get_list_barang($id){
        if($id) {
            $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');

            $this->load->library('datatables');
            $this->datatables->select('
                tblbarang.id, 
                tblbarang.kode,
                tblbarang.nama,
                IF(tblbarang.hargabeli IS NULL,0,tblbarang.hargabeli) as hargabeli,
                gs.stok
            ');
            $this->datatables->join($table[$id] . ' as tblbarang','tblbarang.id = gs.idbarang');
            $this->datatables->where('gs.idunitpelayanan',1);
            $this->datatables->from('mgudang_stok gs');
            return print_r($this->datatables->generate());
        }
    }

    function get_distributor() {
        $query = get_all('mdistributor', array('status' => 1) );
        $data = "<option value=''>Pilih Opsi</option>";
        if($query) {
            foreach ($query as $r) {
                $data .= "<option value='".$r->id."'>".$r->nama."</option>";
            }
        }
        echo $data;
    }

    function getPermintaanDetail() {
        $id = $this->uri->segment(3);
        if($id) {
            $result = $this->model->getPermintaanDetail($id);
            $data = array();
            foreach ($result as $r) {
                $row = array();
                $row['tipebarang']              = GetTipeKategoriBarang($r->idtipe);
                $row['namabarang']              = get_detail_barang($r->idtipe,$r->idbarang)->nama;
                $row['kuantitas']               = 0;
                $row['kuantitasSisa']           = $this->model->getKuantitasSisa($id, $r->idbarang, $r->idtipe);
                $row['idbarang']                = $r->idbarang;
                $row['idtipe']                  = $r->idtipe;
                $row['edit']                    = "<a href='#' class='edit'><i class='fa fa-pencil'></i></a>";

                $data[] = $row;
            }
            $output = array('data' => $data);
            $this->output->set_output(json_encode($output));
        } else {
            show_404();
        }
    }

    function get_barang_detail() {
    	$kode 		= $this->input->post('kodebarang');
    	$tipe 		= $this->input->post('idtipe');
		$table 		= array(null, 'mdata_alkes', 'mdata_implan', 'mdata_obat', 'mdata_logistik');
		$barang 	= get_by_field('kode', $kode, $table[ $tipe ] );
		$data 		= array();

		$data['id']				= $barang->id;
		if( $barang->hargabeli == null) {
			$data['hargabeli']		= 0;
		} else {
			$data['hargabeli']		= $barang->hargabeli;
		}

		$this->output->set_output(json_encode($data));

    }
}
