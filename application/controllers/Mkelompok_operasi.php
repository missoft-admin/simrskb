<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mkelompok_operasi extends CI_Controller {

	/**
	 * Kelompok Operasi controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mkelompok_operasi_model');
  }

	function index(){
		
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Kelompok Operasi';
		$data['content'] 		= 'Mkelompok_operasi/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Kelompok Operasi",'#'),
									    			array("List",'mkelompok_operasi')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'status' 				=> '1'
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Kelompok Operasi';
		$data['content'] 		= 'Mkelompok_operasi/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Kelompok Operasi",'#'),
									    			array("Tambah",'mkelompok_operasi')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Mkelompok_operasi_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Kelompok Operasi';
				$data['content']	 	= 'Mkelompok_operasi/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Kelompok Operasi",'#'),
											    			array("Ubah",'mkelompok_operasi')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mkelompok_operasi','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mkelompok_operasi');
		}
	}

	function delete($id){
		// print_r($id);exit();
		$this->Mkelompok_operasi_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mkelompok_operasi','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mkelompok_operasi_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mkelompok_operasi','location');
				}
			} else {
				if($this->Mkelompok_operasi_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mkelompok_operasi','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 		= validation_errors();
		$data['content'] 	= 'Mkelompok_operasi/manage';

		if($id==''){
			$data['title'] = 'Tambah Kelompok Operasi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Kelompok Operasi",'#'),
															array("Tambah",'mkelompok_operasi')
													);
		}else{
			$data['title'] = 'Ubah Kelompok Operasi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Kelompok Operasi",'#'),
															array("Ubah",'mkelompok_operasi')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	  
$data_user=get_acces();
$user_acces_form=$data_user['user_acces_form'];

			$this->select = array();
			$this->from   = 'mkelompok_operasi';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'id' => 'ASC'
			);
			$this->group  = array();

      $this->column_search   = array('nama');
      $this->column_order    = array('nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama;
          $aksi = '<div class="btn-group">';
          
		  if (UserAccesForm($user_acces_form,array('285'))){
          	$aksi .= '<a href="'.site_url().'mkelompok_operasi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
          }
          
		  if (UserAccesForm($user_acces_form,array('286'))){
          	$aksi .= '<a href="#" data-urlindex="'.site_url().'mkelompok_operasi" data-urlremove="'.site_url().'mkelompok_operasi/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
          }
          $aksi .= '</div>';
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function ajaxSave(){
		if ($this->Mkelompok_operasi_model->saveData()) {
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			$data['response']="data telah di simpan";
			$data['code']=200;
			$data['id']=$this->db->insert_id();
			$data['nama']=$this->input->post("nama");
			echo json_encode($data);
		}
	}
}
