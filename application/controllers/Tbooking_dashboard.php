<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tbooking_dashboard extends CI_Controller {

	/**
	 * Booking controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbooking_dashboard_model');
		$this->load->model('Tpoliklinik_pendaftaran_model');
		$this->load->helper('path');
		
  }
	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$date2=date_create(date('Y-m-d'));
		date_add($date2,date_interval_create_from_date_string("1 days"));
		$date2= date_format($date2,"d/m/Y");
		if (UserAccesForm($user_acces_form,array('1505'))){
			$data = array();
			
			$data['tanggal_1'] 			= date('d/m/Y');
			// $data['tanggal_2'] 			= $date2;
			$data['tanggal_2'] 			= date('d/m/Y');
			
			$data['error'] 			= '';
			$data['title'] 			= 'Dashboard Reservasi';
			$data['content'] 		= 'Tbooking_dashboard/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Booking",'#'),
												  array("Reservasi Dashboard",'tbooking_dashboard')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function load_index_poliklinik()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			
			
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(H.tanggal) ='".date('Y-m-d')."'";
			}
			
			$this->select = array();
			$from="
					(
						SELECT H.id,H.reservasi_tipe_id,H.reservasi_cara,H.statuspasienbaru,H.waktu_reservasi,H.tanggal 
						,H.no_medrec,H.namapasien,P.nama as nama_poli,MD.nama as nama_dokter,H.status_reservasi,H.status_kehadiran
						,CONCAT(DATE_FORMAT(MJ.jam_dari,'%H:%i'),' - ',DATE_FORMAT(MJ.jam_sampai,'%H:%i')) as jam,MH.namahari,H.kodehari,H.noantrian
						,(H.st_general+H.st_skrining+H.st_covid) as jml_skrining
						,CASE WHEN H.idkelompokpasien=1 THEN MR.nama ELSE MK.nama END as asuransi,H.idpoliklinik,H.idkelompokpasien
						FROM app_reservasi_tanggal_pasien H
						INNER JOIN mpoliklinik P ON P.id=H.idpoli
						INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						INNER JOIN mdokter MD ON MD.id=H.iddokter
						INNER JOIN mjadwal_dokter MJ ON MJ.id=H.jadwal_id
						LEFT JOIN merm_hari MH ON MH.kodehari=H.kodehari
						WHERE H.reservasi_tipe_id='1' ".$where."
						ORDER BY H.tanggal ASC,H.id ASC
					) as tbl  
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec','nama_poli','asuransi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $label_skrining='';
		  $disabel_verif='';
		  if ($r->jml_skrining<3){
			  $label_skrining='<br><br>'.'<a href="'.base_url().'tbooking/edit_poli/'.$r->id.'" type="button" data-toggle="tooltip" title="BELUM SKRINING" class="btn btn-danger btn-xs"><i class="fa fa-eye"></i> BELUM SKRINING</a>';;
			  $disabel_verif='disabled';
		  }
          $result[] = $no;
          $result[] = jenis_reservasi($r->reservasi_cara);
          $result[] = $r->no_medrec.'<br>'.$r->namapasien;
          $result[] = '<strong>'.$r->nama_poli.'</strong><br>'.$r->nama_dokter;
          $result[] ='<strong>'. $r->namahari.', '.HumanDateShort_exp($r->tanggal).'</strong><br>'.text_primary($r->jam);
          $result[] = $r->asuransi;
          $result[] = ($r->statuspasienbaru=='0'?text_primary('Pasien Lama'):text_danger('Pasien Baru'));
          $result[] = HumanDateLong($r->waktu_reservasi);
          $aksi = '<div class="btn-group">';
				
				if (UserAccesForm($user_acces_form,array('1501'))){
					if ($r->status_reservasi<='2'){
					$aksi .= '<a href="'.base_url().'tbooking/edit_poli/'.$r->id.'" target="_blank" type="button" data-toggle="tooltip" title="Lihat Detail" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
					}
				}
				$aksi .= '<a href="'.base_url().'tbooking/print_poli" type="button" target="_blank"  data-toggle="tooltip" title="Print Bukti" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';

		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_index_rehabilitas()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			
			
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(H.tanggal) ='".date('Y-m-d')."'";
			}
			
			$this->select = array();
			$from="
					(
						SELECT H.id,H.reservasi_tipe_id,H.reservasi_cara,H.statuspasienbaru,H.waktu_reservasi,H.tanggal 
						,H.no_medrec,H.namapasien,P.nama as nama_poli,MD.nama as nama_dokter,H.status_reservasi,H.status_kehadiran
						,CONCAT(MJ.jam,'.00',' - ',MJ.jam_akhir,'.00') as jam,MH.namahari,H.kodehari,H.noantrian
						,(H.st_general+H.st_skrining+H.st_covid) as jml_skrining
						,CASE WHEN H.idkelompokpasien=1 THEN MR.nama ELSE MK.nama END as asuransi,H.idpoliklinik,H.idkelompokpasien
						FROM app_reservasi_tanggal_pasien H
						INNER JOIN mpoliklinik P ON P.id=H.idpoli
						INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						LEFT JOIN mdokter MD ON MD.id=H.iddokter
						LEFT JOIN merm_jam MJ ON MJ.jam_id=H.jam_id
						LEFT JOIN merm_hari MH ON MH.kodehari=H.kodehari
						WHERE H.reservasi_tipe_id='2' ".$where."
						ORDER BY H.tanggal ASC,H.id ASC
					) as tbl  
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec','nama_poli','asuransi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $label_skrining='';
		  $disabel_verif='';
		  if ($r->jml_skrining<3){
			  $label_skrining='<br><br>'.'<a href="'.base_url().'tbooking_rehab/edit_poli/'.$r->id.'" type="button" data-toggle="tooltip" title="BELUM SKRINING" class="btn btn-danger btn-xs"><i class="fa fa-eye"></i> BELUM SKRINING</a>';;
			  $disabel_verif='disabled';
		  }
          $result[] = $no;
          $result[] = jenis_reservasi($r->reservasi_cara);
          $result[] = $r->no_medrec.'<br>'.$r->namapasien;
          $result[] = '<strong>'.$r->nama_poli.'</strong><br>'.$r->nama_dokter;
          $result[] ='<strong>'. $r->namahari.', '.HumanDateShort_exp($r->tanggal).'</strong><br>'.text_primary($r->jam);
          $result[] = $r->asuransi;
          $result[] = ($r->statuspasienbaru=='0'?text_primary('Pasien Lama'):text_danger('Pasien Baru'));
          $result[] = HumanDateLong($r->waktu_reservasi);
          $aksi = '<div class="btn-group">';
				
				if (UserAccesForm($user_acces_form,array('1501'))){
					if ($r->status_reservasi<='2'){
					$aksi .= '<a href="'.base_url().'tbooking_rehab/edit_poli/'.$r->id.'" target="_blank" type="button" data-toggle="tooltip" title="Lihat Detail" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
					}
				}
				$aksi .= '<a href="'.base_url().'tbooking_rehab/print_poli" type="button" target="_blank"  data-toggle="tooltip" title="Print Bukti" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';

		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_jumlah_reservasi(){
	  $tanggal_1 =$this->input->post('tanggal_1');
	  $tanggal_2 =$this->input->post('tanggal_2');
	  $where='';
	  if ($tanggal_1 !=''){
			$where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_2)."'";
		}else{
			$where .=" AND DATE(H.tanggal) ='".date('Y-m-d')."'";
		}
	  $q="SELECT count(H.id) as jumlah_reservasi_dokter FROM app_reservasi_tanggal_pasien H
			WHERE H.reservasi_tipe_id='1'".$where;
	  
	  $arr['jumlah_reservasi_dokter']=$this->db->query($q)->row('jumlah_reservasi_dokter');
	  $this->output->set_output(json_encode($arr));
  }
  function load_jumlah_reservasi_rehab(){
	  $tanggal_1 =$this->input->post('tanggal_1');
	  $tanggal_2 =$this->input->post('tanggal_2');
	  $where='';
	  if ($tanggal_1 !=''){
			$where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_2)."'";
		}else{
			$where .=" AND DATE(H.tanggal) ='".date('Y-m-d')."'";
		}
	  $q="SELECT count(H.id) as jumlah_reservasi_rehab FROM app_reservasi_tanggal_pasien H
			WHERE H.reservasi_tipe_id='2'".$where;
	  
	  $arr['jumlah_reservasi_rehab']=$this->db->query($q)->row('jumlah_reservasi_rehab');
	  $this->output->set_output(json_encode($arr));
  }
}
