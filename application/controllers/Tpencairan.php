<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Tpencairan extends CI_Controller
{

    /**
   * Pencairan Controller.
   * Developer @GunaliRezqiMauludi
   */

    function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Tpencairan_model');
    }

    function index($status='#')
    {
        $data = array(
          'tanggaldari' => '',
          'tanggalsampai' => '',
          'deskripsi' => '',
          'status' => $status,
        );

        $data['error']      = '';
        $data['title']      = 'Transaksi Pencairan';
        $data['content']    = 'Tpencairan/index';
        $data['breadcrum']  = array(
                                array("RSKB Halmahera",'#'),
                                array("Transaksi Pencairan",'#'),
                                array("List",'Tpencairan')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$tanggaldari=$this->input->post('tanggaldari');
		$tanggalsampai=$this->input->post('tanggalsampai');
		$status=$this->input->post('status');
		$where='';
		
		
		if ('' != $tanggaldari) {
            $where .= " AND DATE(H.tanggal_pencairan) >='".YMDFormat($tanggaldari)."' AND DATE(H.tanggal_pencairan) <='".YMDFormat($tanggalsampai)."'";
        }
		if ($status!='#'){
			if ($status=='1'){//Belum Di proses
				$where .=" AND (H.status='1' AND H.total=0)";
			}
			if ($status=='2'){//Sudah Diproses
				$where .=" AND (H.status='1' AND H.total>0  AND H.st_verifikasi='0')";
			}
			if ($status=='3'){//Sudah Diproses
				$where .=" AND (H.status='1' AND H.total>0  AND H.st_verifikasi='1')";
			}
			if ($status=='4'){//Sudah Diproses
				$where .=" AND (H.status='2')";
			}	
			if ($status=='5'){//Sudah Diproses
				$where .=" AND (H.status='3')";
			}		
		}
        $from = "(
					SELECT * FROM tpencairan H WHERE H.status > 0 ".$where." 
					ORDER BY tanggal_pencairan DESC
				) as tbl  ";
				
		
		// print_r($status);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $status       = '';
            if ($r->status=='1'){
				if ($r->total=='0'){
					$status       = '<span class="label label-default">BELUM DIPROSES</span>';
				}else{
					$status       = '<span class="label label-success">TELAH DIPROSES</span>';				
				}
			}
			if ($r->st_verifikasi=='1' && $r->status=='1'){
				$status       = '<span class="label label-primary">TELAH DIVERIFIKASI</span>';		
			}
			if ($r->status=='2'){
				$status       = '<span class="label label-danger">PROSES PENCAIRAN</span>';		
			}
			if ($r->status=='3'){
				$status       = '<span class="label label-success">SELESAI</span>';		
			}
			
            $row[] = $r->id;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggal_trx);
            $row[] = HumanDateShort($r->tanggal_pencairan);
           
			$aksi .= '<a href="'.site_url().'tpencairan/review/'.$r->id.'/disabled" data-toggle="tooltip" title="Lihat" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
			if ($r->status=='1'){
				$aksi .= '<a href="'.site_url().'tpencairan/review/'.$r->id.'" data-toggle="tooltip" title="Edit" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>';
			}
			if ($r->st_verifikasi=='0' && $r->status=='1' && $r->total >0){
				$aksi .= '<button title="Verifikasi" class="btn btn-primary btn-xs verifikasi"><i class="fa fa-check"></i> Verifikasi</button>';
			}
			if ($r->st_verifikasi=='1' && $r->status=='1'){
				$aksi .= '<button title="Kirim" class="btn btn-info btn-xs kirim"><i class="fa fa-send"></i></button>';
			}
			if ($r->st_verifikasi=='0' && $r->status=='1' && $r->total=='0'){
				$aksi .= '<button title="Hapus" class="btn btn-danger btn-xs hapus"><i class="fa fa-trash"></i></button>';
			}
			if ($r->status=='2'){
				$aksi .= '<button title="Selesaikan" class="btn btn-danger btn-xs finish"><i class="fa fa-flag-o"></i> Selesaikan Pencairan</button>';
			}
			$aksi.='</div>';
            $row[] = $status;           
            $row[] = $aksi;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function verif($id){
		// $id=$this->input->post('id');
		$result=$this->db->query("UPDATE tpencairan set st_verifikasi='1' WHERE id='$id'");
		echo json_encode($result);
	}
	function hapus($id){
		// $id=$this->input->post('idtsetoran_kas
		$result=$this->db->query("UPDATE tpencairan set status='0' WHERE id='$id'");
		echo json_encode($result);
	}
	function kirim($id){
		// $id=$this->input->post('idtsetoran_kas
		$result=$this->db->query("UPDATE tpencairan set status='2' WHERE id='$id'");
		echo json_encode($result);
	}
	function finish($id){
		// $id=$this->input->post('idtsetoran_kas
		$result=$this->db->query("UPDATE tpencairan set status='3' WHERE id='$id'");
		echo json_encode($result);
	}
    function review($id,$disabel='')
    {
        $data =$this->Tpencairan_model->get_detail($id);
        $query_pengajuan =$this->Tpencairan_model->get_pengajuan($data['tanggal_pencairan']);
        $query_kasbon =$this->Tpencairan_model->get_kasbon($data['tanggal_pencairan']);
        $query_refund =$this->Tpencairan_model->get_refund($data['tanggal_pencairan']);
        $query_mutasi =$this->Tpencairan_model->get_mutasi($data['tanggal_pencairan']);
        $query_kbo =$this->Tpencairan_model->get_kbo($data['tanggal_pencairan']);
        $query_gaji =$this->Tpencairan_model->get_gaji($data['tanggal_pencairan']);
        $query_honor =$this->Tpencairan_model->get_honor($data['tanggal_pencairan']);
        $query_bagi_hasil =$this->Tpencairan_model->get_bagi_hasil($data['tanggal_pencairan']);
        $query_retur =$this->Tpencairan_model->get_retur($data['tanggal_pencairan']);
		// print_r($query_honor);exit();
		// print_r($data);exit();
        $data['rekap_penggajian'] = $this->Tpencairan_model->rekap_penggajian($data['tanggal_pencairan']);
        $data['disabel']      = $disabel;
        $data['error']      = '';
        $data['title']      = 'Transaksi Pencairan';
        $data['content']    = 'Tpencairan/review';
        $data['breadcrum']  = array(
                                array("RSKB Halmahera",'#'),
                                array("Transaksi Pencairan",'#'),
                                array("Rekapan",'Tpencairan')
                              );

        // $data = array_merge($data, backend_info());
		$data = array_merge($data,$query_pengajuan,$query_kasbon,$query_refund,$query_mutasi,$query_kbo,$query_gaji,$query_honor,$query_bagi_hasil,$query_retur,backend_info());
        $this->parser->parse('module_template', $data);
    }

    function create(){
		$data = array(
  			'id' 				=> '',
  			'tanggal_pencairan'   => '',
  			'deskripsi' => '',
  			'catatan' 	=> '',
  			'status' 		=> ''
  		);
		// $tanggal_pencairan=$this->input->post('tanggal_pencairan');
		// if ($tanggal_pencairan){
			// $data['tanggal_pencairan']      = HumanDateShort($tanggal_pencairan);
		// }else{
			// $data['tanggal_pencairan']      = '';
			
		// }
  		

      $data['error']      = '';
      $data['title']      = 'Transaksi Pencairan';
      $data['content']    = 'Tpencairan/manage';
      $data['breadcrum']  = array(
                              array("RSKB Halmahera",'#'),
                              array("Transaksi Pencairan",'#'),
                              array("Tambah",'Tpencairan')
                            );

  		$data = array_merge($data, backend_info());
  		$this->parser->parse('module_template', $data);
  	}

    
    function save(){
		
  		$this->form_validation->set_rules('tanggal_pencairan','Validasi', 'callback_check_duplicate_code');
		$this->form_validation->set_message('check_duplicate_code','Tanggal Duplicat');
		
  		if ($this->form_validation->run() == TRUE){
  			if($this->input->post('id') == '' ) {
  				if($this->Tpencairan_model->saveData()){
  					$this->session->set_flashdata('confirm',true);
  					$this->session->set_flashdata('message_flash','data telah disimpan.');
  					redirect('tpencairan','location');
  				}  			
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}
	function save_verifikasi(){ 	
		// print_r($this->input->post());exit();
		if($this->Tpencairan_model->save_verifikasi()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tpencairan','location');
		}  			
	}
	public function check_duplicate_code() {
		// print_r($this->input->post('tanggal_pencairan'));exit();
		$q= $this->Tpencairan_model->check_duplicate($this->input->post('tanggal_pencairan'));
		// print_r($q);exit();
		if ($q==FALSE){
			
			return FALSE;
		}else{
			return TRUE;
		}
		return $q;
	}

  	function failed_save($id){
		// print_r($this->input->post('tanggal_pencairan'));exit();
  		$data = $this->input->post();
  		$data['error'] 					= validation_errors();
      $data['content']    = 'Tpencairan/manage';

  		if($id==''){
        $data['title']      = 'Transaksi Pencairan';
        $data['breadcrum']  = array(
                                array("RSKB Halmahera",'#'),
                                array("Transaksi Pencairan",'#'),
                                array("Tambah",'Tpencairan')
                              );
  		}else{
        $data['title']      = 'Transaksi Pencairan';
        $data['breadcrum']  = array(
                                array("RSKB Halmahera",'#'),
                                array("Transaksi Pencairan",'#'),
                                array("Ubah",'Tpencairan')
                              );
  		}

  		$data = array_merge($data, backend_info());
  		$this->parser->parse('module_template',$data);
  	}

   
}

/* End of file Tpencairan.php */
/* Location: ./application/controllers/Tpencairan.php */
