<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_refund extends CI_Controller {

	/**
	 * Setting Jurnal Pembelian controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_refund_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_refund_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_refund_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Pembelian';
		$data['content'] 		= 'Msetting_jurnal_refund/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Pembelian",'#'),
									    			array("List",'mlogic_kasbon')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function list_kategori($idtipe){
		// $idtipe=$this->input->post('idtipe');
		$q="SELECT * FROM mdata_kategori M
			WHERE M.`status`='1' AND M.idtipe='$idtipe'
			ORDER BY M.nama ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	public function list_barang()
    {
        $cari 	= $this->input->post('search');
        $idtipe 	= $this->input->post('idtipe');
        $idkategori 	= $this->input->post('idkategori');
		$where='';
		if ($idkategori!='#' && $idkategori!='0'){
			$where .=" AND M.idkategori='$idkategori' ";
		}
		if ($cari){
			$where .=" AND (M.nama LIKE '%".$cari."%' OR M.kode LIKE '%".$cari."%')";
		}
		$q="SELECT *FROM view_barang M WHERE M.idtipe='$idtipe' ".$where." LIMIT 100";
		// print_r($q);exit();
        $data_obat = $this->db->query($q)->result_array();
        $this->output->set_output(json_encode($data_obat));
    }
	function update(){
		$id_edit=1;
		$data=array(
			'st_auto_posting'=>$this->input->post('st_auto_posting'),
			'batas_batal'=>$this->input->post('batas_batal'),
		);
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_refund',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	//Pembelian
	function load_deposit()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT S.id,S.idmetode,S.idakun,CONCAT(A.noakun,' - ',A.namaakun) as akun 
				FROM `msetting_jurnal_refund_deposit` S
				LEFT JOIN makun_nomor A ON A.id=S.idakun 
				ORDER BY S.idmetode,S.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('barang','kategori','nama_tipe');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = metode_pembayaran($r->idmetode);
            $row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_deposit('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_deposit(){
		$id_edit=$this->input->post('id_edit');
		$data=array(
			'setting_id'=>1,
			'idmetode'=>$this->input->post('idmetode'),
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_deposit($data['idmetode'])){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_refund_deposit',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_deposit($idmetode){
		
		
		$q="SELECT *FROM msetting_jurnal_refund_deposit S
			WHERE S.idmetode='$idmetode'";
		
		// print_r($q);
		return $this->db->query($q)->row('id');
	}	
	function hapus_deposit($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_refund_deposit');
		echo json_encode($result);
	}
	
	//PPN
	function load_obat()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT S.id,S.idtipe,MT.nama_tipe,S.idkategori,S.idmetode
				,CASE WHEN S.idkategori='0' THEN 'Semua Kategori' ELSE MK.nama END as kategori 
				,CASE WHEN S.idbarang=0 THEN 'Semua Barang' ELSE CONCAT(B.kode,' - ',B.nama) END as barang,S.idbarang,S.idakun,CONCAT(A.noakun,' - ',A.namaakun) as akun
				,CONCAT(A2.noakun,' - ',A2.namaakun) as akun_diskon,S.idakun_diskon 
				FROM `msetting_jurnal_refund_obat` S
				LEFT JOIN mdata_tipebarang MT ON MT.id=S.idtipe
				LEFT JOIN mdata_kategori MK ON MK.id=S.idkategori AND S.idkategori !=0
				LEFT JOIN view_barang B ON S.idbarang!=0 AND B.idtipe=S.idtipe AND S.idbarang=B.id
				LEFT JOIN makun_nomor A ON A.id=S.idakun 
				LEFT JOIN makun_nomor A2 ON A2.id=S.idakun_diskon 
				ORDER BY S.idmetode,S.idtipe,S.idkategori,S.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('barang','kategori','nama_tipe');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = metode_pembayaran($r->idmetode);
            $row[] = $r->nama_tipe;
            $row[] = ($r->idkategori=='0')?text_default($r->kategori):$r->kategori;
            $row[] = ($r->idbarang=='0')?text_default($r->barang):$r->barang;
            // $row[] = $r->akun;
			$row[] = $r->akun.' ('.$r->idakun.')';
			$row[] = $r->akun_diskon.' ('.$r->idakun_diskon.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_obat('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_obat(){
		$id_edit=$this->input->post('id_edit');
		$idtipe=($this->input->post('idtipe')=='#'?0:$this->input->post('idtipe'));
		$idmetode=($this->input->post('idmetode')=='#'?0:$this->input->post('idmetode'));
		$idkategori=($this->input->post('idkategori')=='#'?0:$this->input->post('idkategori'));
		$idbarang=($this->input->post('idbarang')=='#'?0:$this->input->post('idbarang'));
		$data=array(
			'setting_id'=>1,
			'idmetode'=>$idmetode,
			'idtipe'=>$idtipe,
			'idkategori'=>$idkategori,
			'idbarang'=>$idbarang,
			'idakun'=>$this->input->post('idakun'),			
			'idakun_diskon'=>$this->input->post('idakun_diskon')			
		);
		
		if ($this->cek_duplicate_obat($idmetode,$idtipe,$idkategori,$idbarang)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_refund_obat',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	function cek_duplicate_obat($idmetode,$idtipe,$idkategori='0',$idbarang='0'){
		$gabung=$idmetode.'-'.$idtipe.'-'.$idkategori.'-'.$idbarang;
		
		$q="SELECT *FROM msetting_jurnal_refund_obat S
			WHERE CONCAT(S.idmetode,'-',S.idtipe,'-',S.idkategori,'-',S.idbarang)='$gabung'";
		return $this->db->query($q)->row('id');
	}	
	function hapus_obat($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_refund_obat');
		echo json_encode($result);
	}
	

}
