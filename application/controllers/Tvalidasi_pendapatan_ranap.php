<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tvalidasi_pendapatan_ranap extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('tvalidasi_pendapatan_ranap_model','model');
		$this->load->model('Tkasir_model');
		$this->load->model('Tpoliklinik_verifikasi_model');
		$this->load->model('Tverifikasi_transaksi_model');
		$this->load->model('Tpasien_penjualan_model');
		$this->load->model('Trawatinap_tindakan_model');
	}

	function index() {
		// $this->count_null(8);
		$data=array(
			'st_posting'=>'#',
			'tipe_trx'=>'#',
			'nojurnal'=>'',
			'tipe_trx'=>'',
			'tanggal_trx1'=>date('d-m-Y'),
			'tanggal_trx2'=>date('d-m-Y'),
			// 'tanggal_trx2'=>'',
		);
		$data['error'] 			= '';
		// $data['list_rekanan'] 	= $this->model->list_rekanan();
		// print_r($data['list_rekanan']);exit();
		// $data['list_kp'] 	= $this->model->list_kp();
		$data['title'] 			= 'Validasi Jurnal Pendapatan Rawat Inap';
		$data['content'] 		= 'Tvalidasi_pendapatan_ranap/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Pendapatan Rawat Inap",'tvalidasi_pendapatan_ranap/index'),
									array("List",'#')
								);

		// print_r($data);exit();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		
		// $data_user=get_acces();
		// $user_acces_form=$data_user['user_acces_form'];
		
        $userid = $this->session->userdata('user_id');
		$batas_batal=$this->db->query("SELECT batas_batal_ranap FROM msetting_jurnal_pendapatan_general WHERE id='1'")->row('batas_batal_ranap');
		
		$nojurnal=$this->input->post('nojurnal');
		$tipe_trx=$this->input->post('tipe_trx');
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		$st_posting=$this->input->post('st_posting');
		$where='';
		
		
		if ($st_posting !='#'){
			$where .=" AND H.st_posting='$st_posting' ";
		}
		
		if ($nojurnal !=''){
			$where .=" AND H.nojurnal LIKE '%".trim($nojurnal)."%'";
		}
		if ($tipe_trx !='#'){
			$where .=" AND H.tipe_trx='$tipe_trx' ";
		}
		
		
		
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(H.tanggal_transaksi) >='".YMDFormat($tanggal_trx1)."' AND DATE(H.tanggal_transaksi) <='".YMDFormat($tanggal_trx2)."'";
        }
		
        $from = "(
					SELECT H.id,H.id as idvalidasi,H.tanggal_transaksi,H.nojurnal,H.tipe_trx,H.idtipe,IFNULL(DATEDIFF(NOW(),
					H.posting_date),0) as umur_posting,H.no_medrec,H.namapasien,H.nominal,H.st_posting,H.pendaftaran_id,H.kasir_id 
					,0 as jml_sisa
					FROM tvalidasi_pendapatan_ranap H
					WHERE H.`status`='1' ".$where."
					GROUP BY H.id
				) as tbl";
				
		// print_r($this->db->last_query());exit();
		
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nojurnal');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
		// print_r($this->db->last_query());exit();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

			
        foreach ($list as $r) {
            $no++;
            $row = array();
			$disabel_btn='';
			
			if ($r->umur_posting>$batas_batal){
				$disabel_btn='disabled';
			}
            $aksi       = '<div class="btn-group">';
            $jml_null     = $this->count_null($r->id);
            // $jml_null     = 0;
            $status     = '0';
            $status_retur     = '';
            $row[] = $r->id;
            $row[] = $r->st_posting;
            $row[] = $no;
            $row[] = $r->nojurnal;
            $row[] = HumanDateShort($r->tanggal_transaksi);
            // $row[] = '';
            // $row[] = '';
            $row[] = get_tipe_trx($r->tipe_trx);
            $row[] = GetTipeRujukanRawatInap($r->idtipe);
            $row[] = $r->no_medrec;
            $row[] = $r->namapasien;
            $row[] = number_format($r->nominal,2);
            $row[] = ($r->st_posting=='1'?text_success('TELAH DIPOSTING'):text_default('MENUGGU DIPOSTING')).' <br> <br> '.text_primary($r->jml_sisa.' TRX BELUM DIVERIFIKASI');
					$aksi .= '<a href="'.base_url().'tverifikasi_transaksi/edit_transaksi_ranap/'.$r->pendaftaran_id.'/0" target="_blank" title="Detail" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
					$aksi .= '<a href="'.base_url().'tvalidasi_pendapatan_ranap/detail/'.$r->id.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
					if ($r->st_posting=='1'){
						$aksi .= '<button title="Batal Posting" '.$disabel_btn.' class="btn btn-xs btn-danger batalkan"><i class="fa fa-close"></i></button>';						
					}else{
						if ($r->jml_sisa==0){
							if ($jml_null > 0){
								$aksi .= '<button title="Null" class="btn btn-xs btn-danger">Ada Akun Null</button>';								
							}else{
								$aksi .= '<button title="Posting" class="btn btn-xs btn-success posting"><i class="si si-arrow-right"></i></button>';
								$status='1';
							}
						}						
					}
					// $aksi .= '<a href="'.$url_validasi.'print_kwitansi_deposit/'.$r->idtransaksi.'" target="_blank" title="Print Kwitansi" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';
					// $aksi .= '<button title="Print" class="btn btn-xs btn-success"><i class="fa fa-print"></i></button>';
			$aksi.='</div>';
            $row[] = $aksi;
            $row[] = $status;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
		
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function detail($id,$disabel='') {
		$data=$this->model->getHeader($id);
		$data['list_group'] = $this->model->list_group();
		// print_r($data);exit();
		if ($data['st_posting']=='1'){
			$data['statusEdit'] 			= '0';
		}else{
			$data['statusEdit'] 			= '1';
		}
		if ($data['tipe_trx']=='5'){
			$data['st_gabungan']='1';
		}
		$data['list_null'] 			= $this->model->list_group_null($id);
		// print_r($data['list_null']);exit();
		$data['error'] 			= '';
		$data['disabel'] 			= $disabel;
		$data['title'] 			= 'Detail Transaksi';
		$data['content'] 		= 'Tvalidasi_pendapatan_ranap/detail';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Pendapatan Rawat Inap",'tvalidasi_pendapatan_ranap/index'),
									array("List",'#')
								);
		 $data['detail_pembayaran'] = $this->model->list_pembayaran($id);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function edit_tindakan($idvalidasi,$idetail,$disabel='') {
		$data = $this->model->get_edit_detail_tindakan($idetail);
		// print_r($data);exit();
		$data['list_group'] = $this->model->list_group();
        $data['disabel']    = ($disabel==''?$this->model->GetStatusPosting($idvalidasi):$disabel);
        // $data['idvalidasi']    = $idval;
        $data['error']    = '';
        $data['st_posting']    = '';
        $data['title'] = 'Detail Transaksi';
        $data['content'] = 'Tvalidasi_pendapatan_ranap/edit_tindakan';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_ranap/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,backend_info());
        $this->parser->parse('module_template', $data);
	}
	function save_edit_tindakan(){
		// print_r(($this->input->post()));exit();
		$id=$this->input->post('id');
		$idvalidasi=$this->input->post('idvalidasi');
		$data_detail=array(
			'group_jasasarana' =>$this->input->post('group_jasasarana'),
			'group_jasapelayanan' =>$this->input->post('group_jasapelayanan'),
			'group_bhp' =>$this->input->post('group_bhp'),
			'group_biayaperawatan' =>$this->input->post('group_biayaperawatan'),			
			'group_jasasarana_diskon' =>$this->input->post('group_jasasarana_diskon'),
			'group_jasapelayanan_diskon' =>$this->input->post('group_jasapelayanan_diskon'),
			'group_bhp_diskon' =>$this->input->post('group_bhp_diskon'),
			'group_biayaperawatan_diskon' =>$this->input->post('group_biayaperawatan_diskon'),
			'group_diskon_all' =>$this->input->post('group_diskon_all'),
			'idakun_jasasarana' =>$this->GetNoAkunGroup($this->input->post('group_jasasarana')),
			'idakun_jasapelayanan' =>$this->GetNoAkunGroup($this->input->post('group_jasapelayanan')),
			'idakun_bhp' =>$this->GetNoAkunGroup($this->input->post('group_bhp')),
			'idakun_biayaperawatan' =>$this->GetNoAkunGroup($this->input->post('group_biayaperawatan')),		
			'idakun_jasasarana_diskon' =>$this->GetNoAkunGroup($this->input->post('group_jasasarana_diskon')),
			'idakun_jasapelayanan_diskon' =>$this->GetNoAkunGroup($this->input->post('group_jasapelayanan_diskon')),
			'idakun_bhp_diskon' =>$this->GetNoAkunGroup($this->input->post('group_bhp_diskon')),
			'idakun_biayaperawatan_diskon' =>$this->GetNoAkunGroup($this->input->post('group_biayaperawatan_diskon')),
			'idakun_diskon_all' =>$this->GetNoAkunGroup($this->input->post('group_diskon_all')),
		);
		// print_r($data_detail);exit();
		$this->db->where('id',$id);
		if($this->db->update('tvalidasi_pendapatan_ranap_tindakan',$data_detail)){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tvalidasi_pendapatan_ranap/edit_tindakan/'.$idvalidasi.'/'.$id,'location');
		}
	
	}
	function edit_obat_ranap($idvalidasi,$idtipe,$asal='',$disabel='') {
		$data = $this->model->getHeader($idvalidasi);
        $data['asal']    = $asal;
		$asal=str_replace("_",",",$asal);
		// print_r($asal);exit();
		$data['list_group'] = $this->model->list_group();
		$data['list_obat'] = $this->model->list_obat_ranap($idvalidasi,$idtipe,$asal);
		$data['list_obat_racikan'] = $this->model->list_obat_ranap_racikan($idvalidasi,$idtipe,$asal);
		$data['disabel']    = ($disabel==''?$this->model->GetStatusPosting($idvalidasi):$disabel);
        $data['error']    = '';
        $data['idtipe']    = $idtipe;
        $data['st_posting']    = '';
        // $data['nama_tipe']    = GetTipeKategoriBarang($idtipe);
        $data['title'] = 'Detail Transaksi '.GetTipeKategoriBarang($idtipe);
        $data['content'] = 'Tvalidasi_pendapatan_ranap/edit_obat_ranap';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_ranap/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,backend_info());
		// print_r($data);
        $this->parser->parse('module_template', $data);
	}
	function save_edit_obat_ranap(){
		// print_r($this->GetNoAkunGroup(17));exit();
		$idvalidasi=$this->input->post('idvalidasi');
		$asal=$this->input->post('asal');
		$idtipe=$this->input->post('idtipe');
		$iddet_obat=$this->input->post('iddet_obat');
		$idgroup_tuslah=$this->input->post('idgroup_tuslah');
		$idgroup_penjualan=$this->input->post('idgroup_penjualan');
		$idgroup_diskon=$this->input->post('idgroup_diskon');
		foreach($iddet_obat as $index => $val){
			$data_detail=array(
				'idgroup_tuslah' =>$idgroup_tuslah[$index],
				'idgroup_penjualan' =>$idgroup_penjualan[$index],
				'idgroup_diskon' =>$idgroup_diskon[$index],
				'idakun_tuslah' =>$this->GetNoAkunGroup($idgroup_tuslah[$index]),
				'idakun_penjualan' =>$this->GetNoAkunGroup($idgroup_penjualan[$index]),
				'idakun_diskon' =>$this->GetNoAkunGroup($idgroup_diskon[$index]),				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_ranap_obat_alkes',$data_detail);
		}
		
		$iddet_obat_racikan=$this->input->post('iddet_obat_racikan');
		$idgroup_penjualan=$this->input->post('idgroup_penjualan_racikan');
		$idgroup_diskon=$this->input->post('idgroup_diskon_racikan');
		foreach($iddet_obat_racikan as $index => $val){
			$data_detail=array(
				'idgroup_penjualan' =>$idgroup_penjualan[$index],
				'idgroup_diskon' =>$idgroup_diskon[$index],
				'idakun_penjualan' =>$this->GetNoAkunGroup($idgroup_penjualan[$index]),
				'idakun_diskon' =>$this->GetNoAkunGroup($idgroup_diskon[$index]),				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_ranap_obat_alkes_racikan',$data_detail);
		}
		
	
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah disimpan.');
		redirect('tvalidasi_pendapatan_ranap/edit_obat_ranap/'.$idvalidasi.'/'.$idtipe.'/'.$asal,'location');
		
	
	}
	function edit_obat_ko($idvalidasi,$idtipe,$jenis='',$disabel='') {
		$data = $this->model->getHeader($idvalidasi);
        $data['jenis']    = $jenis;
		$data['list_group'] = $this->model->list_group();
		$data['list_obat'] = $this->model->list_obat_ko($idvalidasi,$idtipe,$jenis);
		$data['disabel']    = ($disabel==''?$this->model->GetStatusPosting($idvalidasi):$disabel);
        $data['error']    = '';
        $data['idtipe']    = $idtipe;
        $data['st_posting']    = '';
        // $data['nama_tipe']    = GetTipeKategoriBarang($idtipe);
        $data['title'] = 'Detail Transaksi '.GetTipeKategoriBarang($idtipe).' KAMAR OPERASI';
        $data['content'] = 'Tvalidasi_pendapatan_ranap/edit_obat_ko';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_ranap/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,backend_info());
		// print_r($data);
        $this->parser->parse('module_template', $data);
	}
	
	function save_edit_obat_ko(){
		// print_r($this->GetNoAkunGroup(17));exit();
		$idvalidasi=$this->input->post('idvalidasi');
		$jenis=$this->input->post('jenis');
		$idtipe=$this->input->post('idtipe');
		$iddet_obat=$this->input->post('iddet_obat');
		$idgroup_penjualan=$this->input->post('idgroup_penjualan');
		$idgroup_diskon=$this->input->post('idgroup_diskon');
		foreach($iddet_obat as $index => $val){
			$data_detail=array(
				'idgroup_penjualan' =>$idgroup_penjualan[$index],
				'idgroup_diskon' =>$idgroup_diskon[$index],
				'idakun_penjualan' =>$this->GetNoAkunGroup($idgroup_penjualan[$index]),
				'idakun_diskon' =>$this->GetNoAkunGroup($idgroup_diskon[$index]),				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_ranap_obat_alkes_ko',$data_detail);
		}
		
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah disimpan.');
		redirect('tvalidasi_pendapatan_ranap/edit_obat_ko/'.$idvalidasi.'/'.$idtipe.'/'.$jenis,'location');
		
	
	}
	function edit_obat_igd($idvalidasi,$idtipe,$asal='',$disabel='') {
		$data = $this->model->getHeader($idvalidasi);
        $data['asal']    = $asal;
		$asal=str_replace("_",",",$asal);
		// print_r($asal);exit();
		$data['list_group'] = $this->model->list_group();
		$data['list_obat'] = $this->model->list_obat_igd($idvalidasi,$idtipe,$asal);
		$data['list_obat_racikan'] = $this->model->list_obat_igd_racikan($idvalidasi,$idtipe,$asal);
		$data['disabel']    = ($disabel==''?$this->model->GetStatusPosting($idvalidasi):$disabel);
        $data['error']    = '';
        $data['idtipe']    = $idtipe;
        $data['st_posting']    = '';
        // $data['nama_tipe']    = GetTipeKategoriBarang($idtipe);
        $data['title'] = 'Detail Transaksi '.GetTipeKategoriBarang($idtipe).' IGD';
        $data['content'] = 'Tvalidasi_pendapatan_ranap/edit_obat_igd';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_ranap/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,backend_info());
		// print_r($data);
        $this->parser->parse('module_template', $data);
	}
	function save_edit_obat_igd(){
		// print_r($this->GetNoAkunGroup(17));exit();
		$idvalidasi=$this->input->post('idvalidasi');
		$asal=$this->input->post('asal');
		$idtipe=$this->input->post('idtipe');
		$iddet_obat=$this->input->post('iddet_obat');
		$idgroup_tuslah=$this->input->post('idgroup_tuslah');
		$idgroup_penjualan=$this->input->post('idgroup_penjualan');
		$idgroup_diskon=$this->input->post('idgroup_diskon');
		foreach($iddet_obat as $index => $val){
			$data_detail=array(
				'idgroup_tuslah' =>$idgroup_tuslah[$index],
				'idgroup_penjualan' =>$idgroup_penjualan[$index],
				'idgroup_diskon' =>$idgroup_diskon[$index],
				'idakun_tuslah' =>$this->GetNoAkunGroup($idgroup_tuslah[$index]),
				'idakun_penjualan' =>$this->GetNoAkunGroup($idgroup_penjualan[$index]),
				'idakun_diskon' =>$this->GetNoAkunGroup($idgroup_diskon[$index]),				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_ranap_obat_alkes_igd',$data_detail);
		}
		
		$iddet_obat_racikan=$this->input->post('iddet_obat_racikan');
		$idgroup_penjualan=$this->input->post('idgroup_penjualan_racikan');
		$idgroup_diskon=$this->input->post('idgroup_diskon_racikan');
		foreach($iddet_obat_racikan as $index => $val){
			$data_detail=array(
				'idgroup_penjualan' =>$idgroup_penjualan[$index],
				'idgroup_diskon' =>$idgroup_diskon[$index],
				'idakun_penjualan' =>$this->GetNoAkunGroup($idgroup_penjualan[$index]),
				'idakun_diskon' =>$this->GetNoAkunGroup($idgroup_diskon[$index]),				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_ranap_obat_alkes_igd_racikan',$data_detail);
		}
		
	
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah disimpan.');
		redirect('tvalidasi_pendapatan_ranap/edit_obat_igd/'.$idvalidasi.'/'.$idtipe.'/'.$asal,'location');
		
	
	}
	
	function edit_dokter_anesthesi($idvalidasi,$disabel='') {
		$data = $this->model->getHeader($idvalidasi);
		$data['list_group'] = $this->model->list_group();
		$data['list_detail'] = $this->model->list_dokter_anesthesi($idvalidasi);
		$data['disabel']    = ($disabel==''?$this->model->GetStatusPosting($idvalidasi):$disabel);
        $data['error']    = '';
        $data['st_posting']    = '';
        $data['title'] = 'Detail Dokter Anesthesi';
        $data['content'] = 'Tvalidasi_pendapatan_ranap/edit_dokter_anesthesi';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_ranap/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,backend_info());
		// print_r($data);
        $this->parser->parse('module_template', $data);
	}
	function save_edit_dokter_anesthesi(){
		$idvalidasi=$this->input->post('idvalidasi');
		$iddet=$this->input->post('iddet');
		$group_tarif=$this->input->post('group_tarif');
		$group_diskon=$this->input->post('group_diskon');
		foreach($iddet as $index => $val){
			$data_detail=array(
				'group_tarif' =>$group_tarif[$index],
				'group_diskon' =>$group_diskon[$index],
				'idakun_tarif' =>$this->GetNoAkunGroup($group_tarif[$index]),
				'idakun_diskon' =>$this->GetNoAkunGroup($group_diskon[$index]),				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_ranap_dokter_ko',$data_detail);
		}
		
		
	
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah disimpan.');
		redirect('tvalidasi_pendapatan_ranap/edit_dokter_anesthesi/'.$idvalidasi,'location');
		
	
	}
	function edit_dokter_asisten($idvalidasi,$disabel='') {
		$data = $this->model->getHeader($idvalidasi);
		$data['list_group'] = $this->model->list_group();
		$data['list_detail'] = $this->model->list_asisten($idvalidasi);
		$data['disabel']    = ($disabel==''?$this->model->GetStatusPosting($idvalidasi):$disabel);
        $data['error']    = '';
        $data['st_posting']    = '';
        $data['title'] = 'Detail Asisten';
        $data['content'] = 'Tvalidasi_pendapatan_ranap/edit_dokter_asisten';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_ranap/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,backend_info());
		// print_r($data);
        $this->parser->parse('module_template', $data);
	}
	
	function save_edit_dokter_asisten(){
		$idvalidasi=$this->input->post('idvalidasi');
		$iddet=$this->input->post('iddet');
		$group_tarif=$this->input->post('group_tarif');
		$group_diskon=$this->input->post('group_diskon');
		foreach($iddet as $index => $val){
			$data_detail=array(
				'group_tarif' =>$group_tarif[$index],
				'group_diskon' =>$group_diskon[$index],
				'idakun_tarif' =>$this->GetNoAkunGroup($group_tarif[$index]),
				'idakun_diskon' =>$this->GetNoAkunGroup($group_diskon[$index]),				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_ranap_dokter_ko_asisten',$data_detail);
		}
		
		
	
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah disimpan.');
		redirect('tvalidasi_pendapatan_ranap/edit_dokter_asisten/'.$idvalidasi,'location');
		
	
	}
	function edit_adm($idvalidasi,$disabel='') {
		$data = $this->model->getHeader($idvalidasi);
		$data['list_group'] = $this->model->list_group();
		$data['list_detail'] = $this->model->list_adm($idvalidasi);
		$data['disabel']    = ($disabel==''?$this->model->GetStatusPosting($idvalidasi):$disabel);
        $data['error']    = '';
        $data['st_posting']    = '';
        $data['title'] = 'Detail Adminstrasi Rawat Inap';
        $data['content'] = 'Tvalidasi_pendapatan_ranap/edit_adm';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_ranap/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,backend_info());
		// print_r($data);
        $this->parser->parse('module_template', $data);
	}
	function save_edit_adm(){
		$idvalidasi=$this->input->post('idvalidasi');
		$iddet=$this->input->post('iddet');
		$group_tarif=$this->input->post('group_tarif');
		$group_diskon=$this->input->post('group_diskon');
		foreach($iddet as $index => $val){
			$data_detail=array(
				'group_tarif' =>$group_tarif[$index],
				'group_diskon' =>$group_diskon[$index],
				'idakun_tarif' =>$this->GetNoAkunGroup($group_tarif[$index]),
				'idakun_diskon' =>$this->GetNoAkunGroup($group_diskon[$index]),				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_ranap_adm',$data_detail);
		}
		
		
	
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah disimpan.');
		redirect('tvalidasi_pendapatan_ranap/edit_adm/'.$idvalidasi,'location');
		
	
	}
	function save_edit_pembayaran(){
		// print_r($this->input->post());exit();
		$idvalidasi=$this->input->post('idvalidasi');
		$iddet_tunai=$this->input->post('iddet_tunai');
		$idakun=$this->input->post('idakun');
		$iddet_non_tunai=$this->input->post('iddet_non_tunai');
		$group_akun=$this->input->post('group_akun');
		$form=$this->input->post('form');
		
		if ($iddet_tunai){
			foreach($iddet_tunai as $index => $val){
				$data_detail=array(
					'idakun' =>$idakun[$index],
				);
				$this->db->where('id',$val);
				$this->db->update('tvalidasi_pendapatan_ranap_bayar',$data_detail);
			}
		}
		if ($iddet_non_tunai){
			foreach($iddet_non_tunai as $index => $val){
				$data_detail=array(
					'group_akun' =>$group_akun[$index],
					'idakun' =>$this->GetNoAkunGroup($group_akun[$index]),	
				);
				$this->db->where('id',$val);
				$this->db->update('tvalidasi_pendapatan_ranap_bayar_non_tunai',$data_detail);
			}
		}
		
	
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah disimpan.');
		redirect('tvalidasi_pendapatan_ranap/edit_pembayaran/'.$idvalidasi,'location');
		
	
	}
	function edit_pembayaran($idvalidasi,$disabel='') {
		$data = $this->model->getHeader($idvalidasi);
		// print_r($data);exit();
		$data['list_group'] = $this->model->list_group();
		$data['list_akun'] = $this->model->list_akun();
		$data['list_tunai'] = $this->model->list_tunai($idvalidasi);
		$data['list_non_tunai'] = $this->model->list_non_tunai($idvalidasi);
		$data['disabel']    = ($disabel==''?$this->model->GetStatusPosting($idvalidasi):$disabel);
        $data['error']    = '';
        $data['st_posting']    = '';
        $data['title'] = 'Detail Transaksi';
        $data['content'] = 'Tvalidasi_pendapatan_ranap/edit_pembayaran';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_rajal/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,backend_info());
		// print_r($data);
        $this->parser->parse('module_template', $data);
	}
	function load_ringkasan(){
		$where='';
		$id=$this->input->post('id');
		$query=$this->model->query_ringkasan($id);
		$disabel=$this->input->post('disabel');
		
        $q = "
				SELECT T.idvalidasi,T.idakun,A.noakun,A.namaakun
				,SUM(CASE WHEN T.posisi_akun='D' THEN IFNULL(T.nominal,0) ELSE 0 END) as debet 
				,SUM(CASE WHEN T.posisi_akun='K' THEN IFNULL(T.nominal,0) ELSE 0 END) as kredit
				,GROUP_CONCAT(DISTINCT T.ket ORDER BY T.ket ASC SEPARATOR ', ') as keterangan
				FROM (
						".$query."
				) T 
				LEFT JOIN makun_nomor A ON A.id=T.idakun
				WHERE T.nominal !='0' ".$where."
				GROUP BY T.posisi_akun,T.idakun

				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total_D=0;
		$total_K=0;
		foreach($rows as $r){
			$no++;
			$total_D=$total_D + $r->debet;
			$total_K=$total_K + $r->kredit;
			
			
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td class="text-right">'.$r->noakun.'</td>';
			$tbl .='<td>'.$r->namaakun.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->debet,2).'</td>';
			$tbl .='<td class="text-right">'.number_format($r->kredit,2).'</td>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<td colspan="3" class="text-right"><strong>TOTAL</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_D,2).'</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_K,2).'</strong></td>';
		
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	public function updateDetail()
	{
		$idvalidasi=$this->input->post('idvalidasi');
		$idgroup_gabungan=($this->input->post('idgroup_gabungan')=='#'?'0':$this->input->post('idgroup_gabungan'));
		$idgroup_diskon=($this->input->post('idgroup_diskon')=='#'?'0':$this->input->post('idgroup_diskon'));
		$idgroup_round=($this->input->post('idgroup_round')=='#'?'0':$this->input->post('idgroup_round'));
		// $idgroup_diskon=$this->input->post('idgroup_diskon');
		$data=array(
			'idgroup_diskon' => $idgroup_diskon,
			'idakun_diskon' => $this->GetNoAkunGroup($idgroup_diskon),	
			'idgroup_gabungan' => $idgroup_gabungan,
			'idakun_gabungan' => $this->GetNoAkunGroup($idgroup_gabungan),	
			'idgroup_round' => $idgroup_round,
			'idakun_round' => $this->GetNoAkunGroup($idgroup_round),	
		);
		$this->db->where('id',$idvalidasi);
		// $this->db->update('id',$idhead);
		if ($this->db->update('tvalidasi_pendapatan_ranap',$data)) {
			$output = [
				'status' => true
			];
		} else {
			$output = [
				'status' => false
			];
		}

		echo json_encode($output);
	}
	//BATAS HAPUS
	
	

	function count_null($id){
		$q="SELECT COUNT(*) as jml_null FROM (".$this->model->query_ringkasan($id).") T 
		WHERE T.nominal > 0 AND T.idakun IS NULL
		";
		return $this->db->query($q)->row('jml_null');
	}
	function count_detail_null($id,$idhead){
		$q="SELECT COUNT(*) as jml_null FROM ".$this->model->query_ringkasan($id)." T 
		WHERE T.nominal > 0 AND T.idakun IS NULL AND T.idhead='$idhead'
		";
		return $this->db->query($q)->row('jml_null');
	}
	
	public function posting()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'1',
			'posting_by'=>$this->session->userdata('user_id'),
			'posting_nama'=>$this->session->userdata('user_name'),
            'posting_date'=>date('Y-m-d H:i:s')
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_pendapatan_ranap', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function posting_all()
    {
        $id = $this->input->post('id');
		foreach($id as $index=>$val){
			 $data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $val);
			$result=$this->db->update('tvalidasi_pendapatan_ranap', $data);
		}
       
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	
	public function batalkan()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'0',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_pendapatan_ranap', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function get_transaksi_detail($iddet)
    {
        $q="SELECT B.nama as nama_barang,D.* FROM `tgudang_penerimaan_detail` D
LEFT JOIN view_barang_all B ON B.id=D.idbarang AND B.idkelompokpasien=D.idkelompokpasien
WHERE D.id='$iddet'";
       
        $result=$this->db->query($q)->row_array();
        $this->output->set_output(json_encode($result));
    }
	
	
	
	function GetNoAkunGroup($id){
		return $this->db->query("SELECT GetNoAkunGroup($id) as idakun")->row('idakun');
	}
	function load_bayar(){
		
		$id=$this->input->post('id');
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		
        // $q = "SELECT H.id,H.metode_nama,SUM(H.nominal_bayar) as nominal_bayar FROM tvalidasi_pendapatan_ranap_bayar H
				// WHERE H.idvalidasi='$id'
				// GROUP BY H.idvalidasi,H.idmetode
				// ";
			$q="SELECT H.id,H.metode_nama,SUM(H.nominal_bayar) as nominal_bayar
				,'tvalidasi_pendapatan_ranap_bayar' as reff
				FROM tvalidasi_pendapatan_ranap_bayar H
				WHERE H.idvalidasi='$id'
				GROUP BY H.idvalidasi,H.idmetode

				UNION 

				SELECT H.id,H.metode_nama,SUM(H.nominal_bayar) as nominal_bayar
				,'tvalidasi_pendapatan_ranap_bayar_non_tunai' as reff
				FROM tvalidasi_pendapatan_ranap_bayar_non_tunai H
				WHERE H.idvalidasi='$id'
				GROUP BY H.idvalidasi,H.idmetode";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			$total = $total + $r->nominal_bayar;			
			$aksi       = '<div class="btn-group">';
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td><input type="hidden" name="iddet_bayar[]" value="'.$r->id.'">';
			$tbl .='<td class="text-center">'.$r->metode_nama.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_bayar,2).'</td>';
			$tbl .='<td></td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<tr>';
		$tbl .='<td colspan="2" class="text-right"><strong>TOTAL</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total,2).'</strong></td>';
		$tbl .='<td class="text-right"></td>';
		
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	
	function list_akun($list,$idakun){
		$hasil='';
		foreach($list as $row){
			$hasil .='<option value="'.$row->id.'" '.($row->id==$idakun?"selected":"").'>'.$row->noakun.' - '.$row->namaakun.'</option>';
		}
		$hasil .='<option value="0" '.($idakun=="0"?"selected":"").'>TIDAK ADA</option>';
		return $hasil;
	}
}
