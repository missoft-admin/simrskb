<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrak extends CI_Controller {

	/**
	 * Rak controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mrak_model');
  }

	function index(){
		
		$data = array(
			'idtipe' => '',
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Rak';
		$data['content'] 		= 'Mrak/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Rak",'#'),
									    			array("List",'mrak')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function filter($idtipe=''){
		$data = array(
			'idtipe' => $this->input->post('idtipe'),
		);

		$this->session->set_userdata($data);

		$data['error'] 			= '';
		$data['title'] 			= 'Rak';
		$data['content'] 		= 'Mrak/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Rak",'#'),
									    			array("List",'mrak')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'idtipe' 				=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Rak';
		$data['content'] 		= 'Mrak/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Rak",'#'),
									    			array("Tambah",'mrak')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Mrak_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'idtipe' 				=> $row->idtipe,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Rak';
				$data['content']	 	= 'Mrak/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Rak",'#'),
											    			array("Ubah",'mrak')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mrak','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mrak');
		}
	}

	function delete($id){
		
		$this->Mrak_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mrak/index','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('idtipe', 'Tipe', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mrak_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mrak/index','location');
				}
			} else {
				if($this->Mrak_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mrak/index','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mrak/manage';

		if($id==''){
			$data['title'] = 'Tambah Rak';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Rak",'#'),
															array("Tambah",'mrak')
													);
		}else{
			$data['title'] = 'Ubah Rak';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Rak",'#'),
															array("Ubah",'mrak')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex($uri)
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
			$this->from   = 'mrak';
			$this->join 	= array();

			// FILTER
			if($uri == 'filter'){
				$this->where  = array(
					'status' => '1'
				);
				// $this->where  = array();
				if ($this->session->userdata('idtipe') != "#") {
					$this->where = array_merge($this->where, array('idtipe' => $this->session->userdata('idtipe')));
				}
			}else{
				$this->where  = array(
					'status' => '1'
				);
			}

			$this->order  = array(
				'id' => 'DESC'
			);
			$this->group  = array();
			if (UserAccesForm($user_acces_form,array('255'))){
				$this->column_search   = array('nama');
			}else{
				$this->column_search   = array();
			}
			
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->nama;
					$row[] = GetTipeRak($r->idtipe);
					$aksi = '<div class="btn-group">';
		            if (UserAccesForm($user_acces_form,array('197'))){
		                $aksi .= '<a href="'.site_url().'mrak/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
		            }
		            if (UserAccesForm($user_acces_form,array('198'))){
		                $aksi .= '<a href="#" data-urlindex="'.site_url().'mrak/index/" data-urlremove="'.site_url().'mrak/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
		            }
		            $aksi .= '</div>';
  					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
}
