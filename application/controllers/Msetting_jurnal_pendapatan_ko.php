<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_pendapatan_ko extends CI_Controller {

	/**
	 * Setting Jurnal Penjualan Kamar Operasi controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_pendapatan_ko_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_pendapatan_ko_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_pendapatan_ko_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Transaksi Kamar Operasi';
		$data['content'] 		= 'Msetting_jurnal_pendapatan_ko/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Penjualan Kamar Operasi",'#'),
									    			array("List",'mlogic_narcose')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	public function list_dokter_pegawai_anestesi(){
		$cari=$this->input->post('search');
		$jenis_asisten_daa=$this->input->post('jenis_asisten_daa');
		//jenis_asisten_da : 1 Dokter 2 ; pegawai
		if ($jenis_asisten_daa=='1'){
			$q="SELECT D.id,D.nama from mdokter D WHERE D.idkategori='4' AND nama like '%".$cari."%'";
		}
		if ($jenis_asisten_daa=='2'){
			$q="SELECT D.id,D.nama from mpegawai D WHERE D.idkategori='4' AND nama like '%".$cari."%'";
		}
		// print_r($q);exit();
		$query=$this->db->query($q);
		$result= $query->result();
		 echo json_encode($result);
		// return $result;

	}
	public function list_dokter_pegawai_operator(){
		$cari=$this->input->post('search');
		$jenis_asisten_dao=$this->input->post('jenis_asisten_dao');
		//jenis_asisten_da : 1 Dokter 2 ; pegawai
		if ($jenis_asisten_dao=='1'){
			$q="SELECT D.id,D.nama from mdokter D WHERE  nama like '%".$cari."%'";
		}
		if ($jenis_asisten_dao=='2'){
			$q="SELECT D.id,D.nama from mpegawai D WHERE  nama like '%".$cari."%'";
		}
		// print_r($q);exit();
		$query=$this->db->query($q);
		$result= $query->result();
		 echo json_encode($result);
		// return $result;

	}
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	
	
	
	
	function load_da()
    {
		$jenis=$this->input->post('jenis');
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.*
				,MK.nama as kelas
				,MD.nama as dokter
					 ,GH1.nama as nama_group_pembelian
					 ,GH2.nama as nama_group_diskon					 
					FROM msetting_jurnal_pendapatan_ko_da H
					
					LEFT JOIN mkelas MK ON MK.id=H.idkelas 
					LEFT JOIN mdokter MD ON MD.id=H.iddokter
					LEFT JOIN mgroup_pembayaran GH1 ON GH1.id=H.idgroup_penjualan
					LEFT JOIN mgroup_pembayaran GH2 ON GH2.id=H.idgroup_diskon
					
					GROUP BY H.id
					ORDER BY H.idtipe,H.idkelas,H.iddokter

				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = ($r->idtipe=='0'?text_default('All Tipe'):GetTipeRujukanRawatInap($r->idtipe));
            $row[] = ($r->idkelas=='0'?text_default('All kelas'):$r->kelas);
            $row[] = ($r->iddokter=='0'?text_default('All Dokter'):$r->dokter);
            $row[] = $r->nama_group_pembelian;
            $row[] = $r->nama_group_diskon;
			$aksi 		= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_da('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_da(){
		$id_edit=$this->input->post('id_edit');
		$idtipe=($this->input->post('idtipe')=='#'?0:$this->input->post('idtipe'));
		$idkelas=($this->input->post('idkelas')=='#'?0:$this->input->post('idkelas'));
		$iddokter=($this->input->post('iddokter')=='#'?0:$this->input->post('iddokter'));
		$idgroup_penjualan=($this->input->post('idgroup_penjualan')=='#'?0:$this->input->post('idgroup_penjualan'));
		$idgroup_diskon=($this->input->post('idgroup_diskon')=='#'?0:$this->input->post('idgroup_diskon'));
		$data=array(
			'setting_id'=>1,
			'idtipe'=>$idtipe,
			'idkelas'=>$idkelas,
			'iddokter'=>$iddokter,
			'idgroup_penjualan'=>$idgroup_penjualan,
			'idgroup_diskon'=>$idgroup_diskon,
		);
		
		if ($this->cek_duplicate_da($idtipe,$idkelas,$iddokter)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pendapatan_ko_da',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	function cek_duplicate_da($idtipe,$idkelas,$iddokter){
		$gabung=$idtipe.'-'.$idkelas.'-'.$iddokter;
		$q="SELECT *FROM msetting_jurnal_pendapatan_ko_da S
			WHERE CONCAT(S.idtipe,'-',S.idkelas,'-',S.iddokter)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_da($id)
	{		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_pendapatan_ko_da');
		echo json_encode($result);
	}
	
	function load_daa()
    {
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.*
				,MK.nama as kelas
					 ,GH1.nama as nama_group_pembelian
					 ,GH2.nama as nama_group_diskon		
					,CASE WHEN H.jenis_asisten='1' THEN MD.nama ELSE MP.nama END as dokter			 
					FROM msetting_jurnal_pendapatan_ko_daa H
					
					LEFT JOIN mkelas MK ON MK.id=H.idkelas 
					LEFT JOIN mdokter MD ON MD.id=H.iddokter AND H.jenis_asisten='1'
					LEFT JOIN mpegawai MP ON MP.id=H.iddokter AND H.jenis_asisten='2'
					LEFT JOIN mgroup_pembayaran GH1 ON GH1.id=H.idgroup_penjualan
					LEFT JOIN mgroup_pembayaran GH2 ON GH2.id=H.idgroup_diskon
					
					GROUP BY H.id
					ORDER BY H.idtipe,H.idkelas,H.iddokter

				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = ($r->idtipe=='0'?text_default('All Tipe'):GetTipeRujukanRawatInap($r->idtipe));
            $row[] = ($r->idkelas=='0'?text_default('All kelas'):$r->kelas);
            $row[] = ($r->jenis_asisten=='0'?text_default('All Dokter'):($r->jenis_asisten=='1'?'Dokter':'Pegawai'));
            $row[] = ($r->iddokter=='0'?text_default('All Dokter'):$r->dokter);
            $row[] = $r->nama_group_pembelian;
            $row[] = $r->nama_group_diskon;
			$aksi 		= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_daa('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_daa(){
		$id_edit=$this->input->post('id_edit');
		$idtipe=($this->input->post('idtipe')=='#'?0:$this->input->post('idtipe'));
		$idkelas=($this->input->post('idkelas')=='#'?0:$this->input->post('idkelas'));
		$jenis_asisten=($this->input->post('jenis_asisten')=='#'?0:$this->input->post('jenis_asisten'));
		$iddokter=($this->input->post('iddokter')=='#'?0:$this->input->post('iddokter'));
		$idgroup_penjualan=($this->input->post('idgroup_penjualan')=='#'?0:$this->input->post('idgroup_penjualan'));
		$idgroup_diskon=($this->input->post('idgroup_diskon')=='#'?0:$this->input->post('idgroup_diskon'));
		$data=array(
			'setting_id'=>1,
			'idtipe'=>$idtipe,
			'idkelas'=>$idkelas,
			'jenis_asisten'=>$jenis_asisten,
			'iddokter'=>$iddokter,
			'idgroup_penjualan'=>$idgroup_penjualan,
			'idgroup_diskon'=>$idgroup_diskon,
		);
		
		if ($this->cek_duplicate_daa($idtipe,$idkelas,$jenis_asisten,$iddokter)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pendapatan_ko_daa',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	function cek_duplicate_daa($idtipe,$idkelas,$jenis_asisten,$iddokter){
		$gabung=$idtipe.'-'.$idkelas.'-'.$jenis_asisten.'-'.$iddokter;
		$q="SELECT *FROM msetting_jurnal_pendapatan_ko_daa S
			WHERE CONCAT(S.idtipe,'-',S.idkelas,'-',S.jenis_asisten,'-',S.iddokter)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_daa($id)
	{		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_pendapatan_ko_daa');
		echo json_encode($result);
	}
	
	function load_dao()
    {
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.*
				,MK.nama as kelas
					 ,GH1.nama as nama_group_pembelian
					 ,GH2.nama as nama_group_diskon		
					,CASE WHEN H.jenis_asisten='1' THEN MD.nama ELSE MP.nama END as dokter			 
					FROM msetting_jurnal_pendapatan_ko_dao H
					
					LEFT JOIN mkelas MK ON MK.id=H.idkelas 
					LEFT JOIN mdokter MD ON MD.id=H.iddokter AND H.jenis_asisten='1'
					LEFT JOIN mpegawai MP ON MP.id=H.iddokter AND H.jenis_asisten='2'
					LEFT JOIN mgroup_pembayaran GH1 ON GH1.id=H.idgroup_penjualan
					LEFT JOIN mgroup_pembayaran GH2 ON GH2.id=H.idgroup_diskon
					
					GROUP BY H.id
					ORDER BY H.idtipe,H.idkelas,H.iddokter

				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = ($r->idtipe=='0'?text_default('All Tipe'):GetTipeRujukanRawatInap($r->idtipe));
            $row[] = ($r->idkelas=='0'?text_default('All kelas'):$r->kelas);
            $row[] = ($r->jenis_asisten=='0'?text_default('All Dokter'):($r->jenis_asisten=='1'?'Dokter':'Pegawai'));
            $row[] = ($r->iddokter=='0'?text_default('All Dokter'):$r->dokter);
            $row[] = $r->nama_group_pembelian;
            $row[] = $r->nama_group_diskon;
			$aksi 		= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_dao('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_dao(){
		$id_edit=$this->input->post('id_edit');
		$idtipe=($this->input->post('idtipe')=='#'?0:$this->input->post('idtipe'));
		$idkelas=($this->input->post('idkelas')=='#'?0:$this->input->post('idkelas'));
		$jenis_asisten=($this->input->post('jenis_asisten')=='#'?0:$this->input->post('jenis_asisten'));
		$iddokter=($this->input->post('iddokter')=='#'?0:$this->input->post('iddokter'));
		$idgroup_penjualan=($this->input->post('idgroup_penjualan')=='#'?0:$this->input->post('idgroup_penjualan'));
		$idgroup_diskon=($this->input->post('idgroup_diskon')=='#'?0:$this->input->post('idgroup_diskon'));
		$data=array(
			'setting_id'=>1,
			'idtipe'=>$idtipe,
			'idkelas'=>$idkelas,
			'jenis_asisten'=>$jenis_asisten,
			'iddokter'=>$iddokter,
			'idgroup_penjualan'=>$idgroup_penjualan,
			'idgroup_diskon'=>$idgroup_diskon,
		);
		
		if ($this->cek_duplicate_dao($idtipe,$idkelas,$jenis_asisten,$iddokter)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pendapatan_ko_dao',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	function cek_duplicate_dao($idtipe,$idkelas,$jenis_asisten,$iddokter){
		$gabung=$idtipe.'-'.$idkelas.'-'.$jenis_asisten.'-'.$iddokter;
		$q="SELECT *FROM msetting_jurnal_pendapatan_ko_dao S
			WHERE CONCAT(S.idtipe,'-',S.idkelas,'-',S.jenis_asisten,'-',S.iddokter)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_dao($id)
	{		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_pendapatan_ko_dao');
		echo json_encode($result);
	}
}
