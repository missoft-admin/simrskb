<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mvariable_hold extends CI_Controller {

	/**
	 * Variable Hold controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mvariable_hold_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Variable Hold';
		$data['content'] 		= 'Mvariable_hold/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Variable Hold",'#'),
									    			array("List",'mvariable_hold')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'deskripsi' 		=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Variable Hold';
		$data['content'] 		= 'Mvariable_hold/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Variable Hold",'#'),
									    			array("Tambah",'mvariable_hold')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mvariable_hold_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'deskripsi' 		=> $row->deskripsi,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Variable Hold';
				$data['content']	 	= 'Mvariable_hold/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Variable Hold",'#'),
											    			array("Ubah",'mvariable_hold')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mvariable_hold','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mvariable_hold');
		}
	}

	function delete($id){
		$this->Mvariable_hold_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mvariable_hold','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('deskripsi', 'Singkatan', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mvariable_hold_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mvariable_hold','location');
				}
			} else {
				if($this->Mvariable_hold_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mvariable_hold','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mvariable_hold/manage';

		if($id==''){
			$data['title'] = 'Tambah Variable Hold';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Variable Hold",'#'),
															array("Tambah",'mvariable_hold')
													);
		}else{
			$data['title'] = 'Ubah Variable Hold';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Variable Hold",'#'),
															array("Ubah",'mvariable_hold')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
			$this->select = array();
			$this->from   = 'mvariable_hold';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'id' => 'ASC'
			);
			$this->group  = array();

      $this->column_search   = array('nama','deskripsi');
      $this->column_order    = array('nama','deskripsi');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
			$aksi = '<div class="btn-group">';
			// if(button_roles('mvariable_hold/update')) {
				$aksi .= '<a href="'.site_url().'mvariable_hold/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
			// }
			// if(button_roles('mvariable_hold/delete')) {
				$aksi .= '<a href="#" data-urlindex="'.site_url().'mvariable_hold" data-urlremove="'.site_url().'mvariable_hold/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
			// }
			$aksi .= '</div>';

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = $r->deskripsi;
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
}
