<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_pengelolaan_dokter extends CI_Controller
{

    /**
     * Pengelolaan Dokter controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Trm_pengelolaan_dokter_model', 'model');
    }

    public function index()
    {
        $data = array(
            'iddokter' => '',
            'jam' => '',
        );

        $data['error'] = '';
        $data['title'] = 'Pengelolaan Dokter';
        $data['content'] = 'Trm_pengelolaan_dokter/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Pengelolaan Dokter",'#'),
            array("List",'trm_pengelolaan_dokter')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filter()
    {
        $data = array(
            'iddokter' => $this->input->post('iddokter'),
            'jam' => $this->input->post('jam'),
        );

        $this->session->set_userdata($data);

        $data['error'] = '';
        $data['title'] = 'Pengelolaan Dokter';
        $data['content'] = 'Trm_pengelolaan_dokter/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Pengelolaan Dokter",'#'),
            array("Filter",'trm_pengelolaan_dokter')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function detail($iddokter='')
    {
        $row = $this->model->getDokter($iddokter);
        if ($row) {
            $data = array(
                'iddokter' => $iddokter,
                'no_pengajuan' => '',
                'no_medrec' => '',
                'nama_pasien' => '',
                'jenis_kunjungan' => '',
                'estimasi_selesai' => '',
                'jenis_pengajuan' => '',
                'status_rawat' => '',
                'status_waktu' => '',
                'status_proses' => '',
            );

            $data['error'] = '';
            $data['title'] = "Pengelolaan Dokter - " . $row->nama;
            $data['content'] = 'Trm_pengelolaan_dokter/detail';
            $data['breadcrum'] = array(
                array("RSKB Halmahera",'#'),
                array("Pengelolaan Dokter",'#'),
                array('Proses Pengambilan','Trm_pengelolaan_dokter')
            );

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        }
    }

    public function detail_filter($iddokter='')
    {
        $data = array(
            'iddokter' => $iddokter,
            'no_pengajuan' => $this->input->post('no_pengajuan'),
            'no_medrec' => $this->input->post('no_medrec'),
            'nama_pasien' => $this->input->post('nama_pasien'),
            'jenis_kunjungan' => $this->input->post('jenis_kunjungan'),
            'estimasi_selesai' => $this->input->post('estimasi_selesai'),
            'jenis_pengajuan' => $this->input->post('jenis_pengajuan'),
            'status_rawat' => $this->input->post('status_rawat'),
            'status_waktu' => $this->input->post('status_waktu'),
            'status_proses' => $this->input->post('status_proses'),
        );

        $this->session->set_userdata($data);


        $data['error'] = '';
        $data['title'] = "Pengelolaan Dokter";
        $data['content'] = 'Trm_pengelolaan_dokter/detail';
        $data['breadcrum'] = array(
            array("RSKB Halmahera",'#'),
            array("Pengelolaan Dokter",'#'),
            array('Proses Pengambilan','Trm_pengelolaan_dokter')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function getIndex($uri='index')
    {
        $prefix = '<label class="label label-primary">';
        $suffix = '</label>';

        $where = '';

        $iddokter = $this->session->userdata('iddokter');
        $jam = $this->session->userdata('jam');

        if ($uri == 'filter') {
			if ($jam != null && ($iddokter != null || $iddokter != '#')) {
				$where = 'WHERE ';
			}
            
            if ($jam != null) {
                $where .= "mjadwal_dokter.jam_dari >= '$jam' AND mjadwal_dokter.jam_sampai <= '$jam'";
            }

            if ($jam != null && $iddokter != null) {
                $where .= " AND ";
            }
            
            if ($iddokter != '#') {
                $where .= "trm_berkas_analisa_dokter.iddokter = $iddokter";
            }
        }

        $this->select = array();

        $this->from = "
        (
            SELECT
                trm_pengajuan_informasi_detail.id,
                mdokter.id AS id_dokter,
                mdokter.nama AS nama_dokter,
                SUM(CASE WHEN trawatinap_pendaftaran.id IS NOT NULL AND trawatinap_pendaftaran.statuscheckout = 0 THEN 1 ELSE 0 END) AS belum_selesai,
                SUM(CASE WHEN trawatinap_pendaftaran.id IS NULL || trawatinap_pendaftaran.statuscheckout = 1 THEN 1 ELSE 0 END) AS sudah_selesai,
                GROUP_CONCAT( DISTINCT CONCAT('$prefix', mjadwal_dokter.jam_dari, ' - ', mjadwal_dokter.jam_sampai, '$suffix') SEPARATOR '<br><br>' ) AS jadwal,
                mruangan.nama AS nama_ruangan
            FROM
                trm_pengajuan_informasi_detail
            JOIN trm_layanan_berkas ON trm_layanan_berkas.id = trm_pengajuan_informasi_detail.idberkas
            JOIN mdokter ON mdokter.id = trm_layanan_berkas.iddokter
            JOIN mjadwal_dokter ON mjadwal_dokter.iddokter = mdokter.id
            JOIN mruangan ON mruangan.id = mjadwal_dokter.idruangan AND mjadwal_dokter.hari = ( CASE DAYOFWEEK( NOW() ) WHEN 1 THEN 'Minggu' WHEN 2 THEN 'Senin' WHEN 3 THEN 'Selasa' WHEN 4 THEN 'Rabu' WHEN 5 THEN 'Kamis' WHEN 6 THEN 'Jumat' WHEN 7 THEN 'Sabtu' END ) 
            LEFT JOIN trawatinap_pendaftaran ON trawatinap_pendaftaran.id = trm_layanan_berkas.id_trx AND trm_layanan_berkas.tujuan = 3
            GROUP BY 
                mjadwal_dokter.iddokter, 
                mjadwal_dokter.idpoliklinik
            $where
        ) AS result
        ";

        $this->join = array();

        $this->where = array();

        $this->order = array();

        $this->group = array();

        $this->column_search = array('result.nama_dokter');
        $this->column_order = array('result.nama_dokter');

        $list = $this->datatable->get_datatables();

        $data = array();
        foreach ($list as $r) {
            $row = array();
            
            $row[] = $r->nama_dokter;
            $row[] = number_format($r->belum_selesai);
            $row[] = number_format($r->sudah_selesai);
            $row[] = $r->jadwal;
            $row[] = $r->nama_ruangan;
            $row[] = '<div class="btn-group">
                <a href="'.site_url().'trm_pengelolaan_dokter/detail/'.$r->id_dokter.'" data-toggle="tooltip" title="Rincian Data" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                <a href="'.site_url().'trm_pengelolaan_dokter/print/'.$r->id_dokter.'" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
            </div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        echo json_encode($output);
    }

    public function getDetail($uri='detail', $iddokter)
    {
        $this->select = array(
            'trm_pengajuan_informasi_detail.id',
            'trm_pengajuan_informasi.notransaksi AS no_pengajuan',
            'trm_pengajuan_informasi.tanggal AS tanggal_pengajuan',
            'trm_layanan_berkas.no_medrec',
            'trm_layanan_berkas.namapasien AS nama_pasien',
            'trm_layanan_berkas.tujuan AS jenis_kunjungan',
            'mdokter.nama AS nama_dokter',
            'mpengajuan_skd.nama AS jenis_pengajuan',
            'trm_pengajuan_informasi.nama_pemohon',
            'trm_pengajuan_informasi.keterangan',
            'trm_pengajuan_informasi_detail.estimasi_selesai',
            'trm_pengajuan_informasi_detail.status_verifikasi',
            'trm_pengajuan_informasi_detail.status_pengajuan',
            'COALESCE(trawatinap_pendaftaran.statuscheckout, 1) AS status_checkout',
        );

        $this->from = 'trm_pengajuan_informasi_detail';

        $this->join = array(
            array('trm_pengajuan_informasi', 'trm_pengajuan_informasi.id = trm_pengajuan_informasi_detail.idtransaksi', 'LEFT'),
            array('trm_layanan_berkas', 'trm_layanan_berkas.id = trm_pengajuan_informasi_detail.idberkas', 'LEFT'),
            array('mdokter', 'mdokter.id = trm_layanan_berkas.iddokter', 'LEFT'),
            array('mpengajuan_skd', 'mpengajuan_skd.id = trm_pengajuan_informasi_detail.jenis_pengajuan', 'LEFT'),
            array('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = trm_layanan_berkas.id_trx AND trm_layanan_berkas.tujuan IN (3, 4)', 'LEFT'),
        );

        // FILTER
        $this->where  = array(
            'trm_layanan_berkas.iddokter = ' => $iddokter,
            'trm_pengajuan_informasi_detail.status = ' => '1',
        );

        if ($uri == 'detail_filter') {
            if ($this->session->userdata('no_pengajuan') != null) {
                $this->where = array_merge($this->where, array('trm_pengajuan_informasi.notransaksi' => $this->session->userdata('no_pengajuan')));
            }
            if ($this->session->userdata('no_medrec') != null) {
                $this->where = array_merge($this->where, array('trm_layanan_berkas.no_medrec' => $this->session->userdata('no_medrec')));
            }
            if ($this->session->userdata('nama_pasien') != null) {
                $this->where = array_merge($this->where, array('trm_layanan_berkas.namapasien LIKE' => '%'.$this->session->userdata('nama_pasien').'%'));
            }
            if ($this->session->userdata('jenis_kunjungan') != '#') {
                $this->where = array_merge($this->where, array('trm_layanan_berkas.tujuan' => $this->session->userdata('jenis_kunjungan')));
            }
            if ($this->session->userdata('estimasi_selesai') != null) {
                $this->where = array_merge($this->where, array('DATE(trm_pengajuan_informasi_detail.estimasi_selesai) >=' => YMDFormat($this->session->userdata('estimasi_selesai'))));
            }
            if ($this->session->userdata('estimasi_selesai') != null) {
                $this->where = array_merge($this->where, array('DATE(trm_pengajuan_informasi_detail.estimasi_selesai) <=' => YMDFormat($this->session->userdata('estimasi_selesai'))));
            }
            if ($this->session->userdata('jenis_pengajuan') != '#') {
                $this->where = array_merge($this->where, array('trm_pengajuan_informasi_detail.jenis_pengajuan' => $this->session->userdata('jenis_pengajuan')));
            }
            if ($this->session->userdata('status_rawat') != '#') {
                $this->where = array_merge($this->where, array('COALESCE(trawatinap_pendaftaran.statuscheckout, 1) = ' => $this->session->userdata('status_rawat')));
            }
            if ($this->session->userdata('status_waktu') != '#') {
                if ($this->session->userdata('status_waktu') == 'merah') {
                    $this->where = array_merge($this->where, array('DATEDIFF(trm_pengajuan_informasi_detail.estimasi_selesai, NOW()) <' => '5'));
                } else if ($this->session->userdata('status_waktu') == 'kuning') {
                    $this->where = array_merge($this->where, array('DATEDIFF(trm_pengajuan_informasi_detail.estimasi_selesai, NOW()) >=' => '5'));
                    $this->where = array_merge($this->where, array('DATEDIFF(trm_pengajuan_informasi_detail.estimasi_selesai, NOW()) <' => '10'));
                } else {
                    $this->where = array_merge($this->where, array('DATEDIFF(trm_pengajuan_informasi_detail.estimasi_selesai, NOW()) >=' => '10'));
                }
            }
        } else {
            $this->where = array_merge($this->where, array('DATE(trm_pengajuan_informasi.tanggal)' => date("Y-m-d")));
        }

        $this->order = array();

        $this->group = array();

        $this->column_search = array('trm_pengajuan_informasi.notransaksi','trm_pengajuan_informasi.nama_pemohon');
        $this->column_order = array('trm_pengajuan_informasi.notransaksi','trm_pengajuan_informasi.nama_pemohon');

        $list = $this->datatable->get_datatables();

        $data = array();
        foreach ($list as $r) {
            $row = array();
            
            if ($r->status_verifikasi == 0) {
                $action = '<a href="'.site_url().'trm_pengelolaan_informasi/proses_verifikasi/'.$r->id.'" data-toggle="tooltip" title="Menyelesaikan Pengajuan" class="btn btn-primary btn-sm"><i class="fa fa-check-circle"></i></a>';
            } elseif ($r->status_verifikasi == 1 && $r->status_pengajuan == 0) {
                $action = '<a href="'.site_url().'trm_pengelolaan_informasi/proses_pengambilan/'.$r->id.'" data-toggle="tooltip" title="Pengambilan Informasi" class="btn btn-danger btn-sm"><i class="fa fa-arrow-circle-right"></i></a>';
            } elseif ($r->status_pengajuan == 1) {
                $action = '<a href="'.site_url().'trm_pengelolaan_informasi/view_data/'.$r->id.'" data-toggle="tooltip" title="Rincian Pengambilan Informasi" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
            }

            $row[] = $r->no_pengajuan;
            $row[] = DMYFormat($r->tanggal_pengajuan);
            $row[] = $r->no_medrec;
            $row[] = $r->nama_pasien;
            $row[] = GetJenisLayananBerkas($r->jenis_kunjungan);
            $row[] = $r->nama_dokter;
            $row[] = $r->jenis_pengajuan;
            $row[] = $r->nama_pemohon;
            $row[] = $r->keterangan;
            $row[] = DMYFormat($r->estimasi_selesai);
            $row[] = StatusCheckoutRanap($r->status_checkout);
            $row[] = getStatusEstimasiPengajuanInformasi($r->tanggal_pengajuan, $r->estimasi_selesai);
            $row[] = '<div class="btn-group">
                ' . $action . '
                <a href="'.site_url().'trm_pengelolaan_informasi/print/'.$r->id.'" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
            </div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        echo json_encode($output);
    }
}
