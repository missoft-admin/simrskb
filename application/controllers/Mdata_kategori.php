<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mdata_kategori extends CI_Controller
{

    /**
     * Kategori Barang controller.
     * Developer @gunalirezqimauludi
     */

    function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mdata_kategori_model');
		$this->load->helper('path');
    }

    function index()
    {
        
        $data = array(
          'idtipe' => '',
        );

        $data['error']      = '';
        $data['title']     = 'Kategori Barang';
        $data['content']   = 'Mdata_kategori/index';
        $data['breadcrum'] = array(
										            array("RSKB Halmahera",'#'),
										            array("Kategori Barang",'#'),
										            array("List",'mdata_kategori')
											       );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function filter(){
    		$data = array(
    			'idtipe' => $this->input->post('idtipe'),
    		);

    		$this->session->set_userdata($data);

        $data['error']      = '';
        $data['title']     = 'Kategori Barang';
        $data['content']   = 'Mdata_kategori/index';
        $data['breadcrum'] = array(
                                array("RSKB Halmahera",'#'),
                                array("Kategori Barang",'#'),
                                array("List",'mdata_kategori')
                             );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
  	}

    function create()
    {
        
        $data = array(
            'id'       => '',
            'idparent'   => '0',
            'idtipe'   => '0',
            'nama'     => '',
            'margin'   => '',
            'status'   => ''
        );

        $data['error']      = '';
        $data['title']      = 'Tambah Kategori Barang';
        $data['content']    = 'Mdata_kategori/manage';
        $data['breadcrum']  = array(
                                array("RSKB Halmahera",'#'),
                                array("Kategori Barang",'#'),
                              	array("Tambah",'mdata_kategori')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function update($id)
    {
        
        if ($id != '') {
            $row = $this->Mdata_kategori_model->getSpecified($id);
            if (isset($row->id)) {
                $data = array(
                    'id'       => $row->id,
                    'idparent'   => $row->idparent,
                    'idtipe'   => $row->idtipe,
                    'nama'     => $row->nama,
                    'margin'   => $row->margin,
                    'status'   => $row->status
                );
				$data['list_parent'] = $this->Mdata_kategori_model->list_parent($row->idtipe);
				// print_r($data);exit();
                $data['error']      = '';
                $data['title']      = 'Ubah Kategori Barang';
                $data['content']    = 'Mdata_kategori/manage';
                $data['breadcrum']  = array(
                                        array("RSKB Halmahera",'#'),
                                        array("Kategori Barang",'#'),
                                      	array("Ubah",'mdata_kategori')
                                      );

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mdata_kategori/index', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mdata_kategori/index', 'location');
        }
    }

    function delete($id)
    {
        
        $this->Mdata_kategori_model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mdata_kategori/index', 'location');
    }

    function save()
    {
        $this->form_validation->set_rules('idtipe', 'Tipe', 'trim|required');
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');

        if ($this->form_validation->run() == true) {
            if ($this->input->post('id') == '') {
                if ($this->Mdata_kategori_model->saveData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mdata_kategori/index/'.$this->input->post('idtipe'), 'location');
                }
            } else {
                if ($this->Mdata_kategori_model->updateData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mdata_kategori/index/'.$this->input->post('idtipe'), 'location');
                }
            }
        } else {
            $this->failed_save($this->input->post('id'));
        }
    }

    function failed_save($id)
    {
        $data = $this->input->post();
        $data['error']   = validation_errors();
        $data['content'] = 'Mdata_kategori/manage';

        if ($id=='') {
            $data['title'] = 'Tambah Kategori Barang';
            $data['breadcrum'] = array(
                                    array("RSKB Halmahera",'#'),
                                    array("Kategori Barang",'#'),
                                    array("Tambah",'mdata_kategori')
                                 );
        } else {
            $data['title'] = 'Ubah Kategori Barang';
            $data['breadcrum'] = array(
	                                  array("RSKB Halmahera",'#'),
	                                  array("Kategori Barang",'#'),
	                                  array("Ubah",'mdata_kategori')
	                               );
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

  	function getIndex($uri)
    {
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

        $this->select = array('mdata_kategori.*','S.nama as nama_parent');
        $this->from   = 'mdata_kategori';
        $this->join 	=array(
			array('mdata_kategori S', 'S.id=mdata_kategori.idparent','LEFT')
			);

		
		// $this->select = array('mdata_alkes.*', 'mdata_kategori.nama AS namakategori');
        // $this->from   = 'mdata_alkes';
        // $this->join 	= array(
          // array("mdata_kategori", "mdata_kategori.id = mdata_alkes.idkategori", "")
        // );
        // FILTER
  			if($uri == 'filter'){
  				$this->where  = array();
  				if ($this->session->userdata('idtipe') != "#") {
  					$this->where = array_merge($this->where, array('mdata_kategori.idtipe' => $this->session->userdata('idtipe')));
  				}
  			}else{
  				$this->where  = array(
  					'mdata_kategori.status' => '1'
  				);
  			}

        $this->order  = array(
          'mdata_kategori.statuslock' => 'DESC',
          'mdata_kategori.id' => 'DESC'
        );
        $this->group  = array();

        $this->column_search   = array('mdata_kategori.nama');
        $this->column_order    = array('mdata_kategori.nama');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            // PROCESS
            $margin = ($r->idtipe == '2' ? $r->margin.'%' : '-');
            $aksi = '<div class="btn-group">';
            if($r->statuslock == 0){
                if (UserAccesForm($user_acces_form,array('51'))){
                    $aksi .= '<a href="'.site_url().'mdata_kategori/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
                }
                if (UserAccesForm($user_acces_form,array('52'))){
                    $aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_kategori/index" data-urlremove="'.site_url().'mdata_kategori/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
                }
            }else{
              $aksi .= '<a href="#" data-toggle="tooltip" title="Terkunci" class="btn btn-danger btn-sm"><i class="fa fa-lock"></i></a>';
            }
            $aksi .= '</div>';

            $row[] = $no;
            $row[] = $r->nama;
            $row[] = GetTipeKategoriBarang($r->idtipe);
            $row[] = $r->nama_parent;
            $row[] = $margin;
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = array(
  	      "draw" => $_POST['draw'],
  	      "recordsTotal" => $this->datatable->count_all(),
  	      "recordsFiltered" => $this->datatable->count_all(),
  	      "data" => $data
        );
        echo json_encode($output);
    }
	function find_parent($idtipe){
		$arr['detail'] = $this->Mdata_kategori_model->find_parent($idtipe);
		$this->output->set_output(json_encode($arr));

	}
}
