<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tperubahan extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tperubahan_model');
		$this->load->model('Tpendaftaran_poli_pasien_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Tpoliklinik_resep_model');
		$this->load->helper('path');
		
  }
	function index($tab='2'){
		get_ppa_login();
		$log['path_tindakan']='tperubahan';
		$this->session->set_userdata($log);
		$user_id=$this->session->userdata('user_id');
		// print_r($this->session->userdata('path_tindakan'));exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$q="SELECT H.id as login_mppa_id,H.pegawai_id as login_iddokter,H.pegawai_id as iddokter FROM mppa H WHERE H.user_id='$user_id' AND H.tipepegawai='2' AND H.staktif='1'";
		// $login_iddokter=$this->db->query($q)->row('login_iddokter');	
		if (UserAccesForm($user_acces_form,array('1597'))){
			$data = $this->db->query($q)->row_array();	
			if ($data){
				
				$data['list_poli'] 			= $this->Tperubahan_model->list_poliklinik();
				$data['list_dokter'] 			= $this->Tpendaftaran_poli_pasien_model->list_dokter();
				
				$data['idpoli'] 			= '#';
				$data['namapasien'] 			= '';
				$data['tab'] 			= $tab;
				
				$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
				$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
				$data['error'] 			= '';
				$data['title'] 			= 'My Pasien';
				$data['content'] 		= 'Tperubahan/index';
				$data['breadcrum'] 	= array(
													  array("RSKB Halmahera",'#'),
													  array("Tindakan Poliklinik",'#'),
													  array("My Pasien",'tpendaftaran_poli_pasien')
													);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				redirect('page404');
			}
		}else{
			redirect('page404');
		}
	}
	
	function getIndex_all()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// tipe_pemilik:tipe_pemilik,status:status,nama:nama
		$where='';
			$login_ppa_id=$this->session->userdata('login_ppa_id');		
			// print_r($this->session->userdata());exit;
		$idpoli =$this->input->post('idpoli');
		$iddokter =$this->input->post('iddokter');
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$pencarian =$this->input->post('pencarian');
		$tab =$this->input->post('tab');
		if ($tanggal_1 !=''){
			$where .=" AND DATE(TK.tanggal_permintaan) >='".YMDFormat($tanggal_1)."' AND DATE(TK.tanggal_permintaan) <='".YMDFormat($tanggal_2)."'";
		}else{
			$where .=" AND DATE(TK.tanggal_permintaan) >='".date('Y-m-d')."'";
		}
		if ($pencarian!=''){
			$where .=" AND (H.namapasien LIKE '%".$pencarian."%')";
			$where .=" OR (H.no_medrec LIKE '%".$pencarian."%')";
		}
		if ($tab=='2'){
			$where .=" AND (TK.jawab_perubahan) = '0'";
		}
		if ($tab=='3'){
			$where .=" AND (TK.jawab_perubahan = TK.st_perubahan)";
		}
		if ($tab=='4'){
			$where .=" AND (TK.jawab_perubahan>0 AND TK.jawab_perubahan<TK.st_perubahan)";
		}
		if ($idpoli!='#'){
			$where .=" AND (H.idpoliklinik) = '$idpoli'";
		}
		// $where='';
		// print_r($tab);exit;
		$this->select = array();
		$from="
				(
					SELECT H.id,H.umurhari,H.umurbulan,H.umurtahun,H.tanggal_lahir,H.jenis_kelamin,H.title,H.tanggaldaftar,H.idtipe,H.tanggal
					
					,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header
					,MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
					,MK.nama as nama_kelompok,MR.nama as nama_rekanan,
					TK.* ,H.idkelompokpasien
					,CASE WHEN jawab_perubahan=0 THEN 0 WHEN st_perubahan=jawab_perubahan THEN 1 ELSE 2 END as st_jawaban
					FROM tpoliklinik_e_resep TK
					INNER JOIN tpoliklinik_pendaftaran H ON H.id=TK.pendaftaran_id
					LEFT JOIN (
												".get_alergi_sql()."
											) A ON A.idpasien=H.idpasien 
					INNER JOIN mdokter MD ON MD.id=H.iddokter
					INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
					INNER JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
					INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
					LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
					WHERE TK.st_perubahan !='0' AND TK.iddokter_resep='$login_ppa_id' ".$where."

					ORDER BY TK.tanggal_input DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama_dokter','namapasien','no_medrec');
		$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  $btn_setuju='<div class="push-5-t"><button class="btn btn-xs btn-success" onclick="setuju_all('.$r->assesmen_id.')"><i class="si si-check"></i> SETUJUI PERUBAHAN</button></div>';
		  if ($r->jawab_perubahan>0){
			  $btn_setuju='';
		  }
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="push-5-t"><a href="'.base_url().'tperubahan/tindakan/'.$r->id.'/erm_e_resep/lihat_perubahan/'.$r->assesmen_id.'" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-primary btn-xs menu_click"><i class="fa fa-user-md"></i> '.($r->jawab_perubahan=='0'?'JAWAB':'LIHAT JAWABAN').'</a> '.$btn_setuju.'</div>
									'.div_panel_kendali($user_acces_form,$r->id).'
									
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->namapasien.' - '.$r->nomedrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
										<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
									</div>
									
									</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  
		  
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO </div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class="text-muted text-uppercase">'.GetAsalRujukanR($r->idtipe).' - '.$r->nama_poli.'</div>
									<div class="text-muted"><i class="fa fa-user-md"></i> '.($r->nama_dokter).'</div>
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->idkelompokpasien=='1'?'ASURANSI : '.$r->nama_rekanan:$r->nama_kelompok).'</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.status_jawab_perubahan($r->st_jawaban).'</div>
									<div class="h4 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggal).'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function setuju_all(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $q="SELECT *FROM tpoliklinik_e_resep_obat WHERE st_perubahan>0";
	  $list_data=$this->db->query($q)->result();
	  foreach($list_data as $row){
		$data=array(
			'st_setuju' => 1,
			'user_setuju' => $login_ppa_id,
			'tanggal_setuju' => date('Y-m-d H:i:s'),
			
		);
		$this->db->where('id',$row->id);
		$hasil=$this->db->update('tpoliklinik_e_resep_obat',$data);
	  }
	  $q="SELECT *FROM tpoliklinik_e_resep_obat_racikan_obat WHERE st_perubahan>0";
	  $list_data=$this->db->query($q)->result();
	  foreach($list_data as $row){
		$data=array(
			'st_setuju' => 1,
			'user_setuju' => $login_ppa_id,
			'tanggal_setuju' => date('Y-m-d H:i:s'),
			
		);
		$this->db->where('id',$row->id);
		$hasil=$this->db->update('tpoliklinik_e_resep_obat_racikan_obat',$data);
	  }
	  $this->output->set_output(json_encode($hasil));
  }
	function tindakan($pendaftaran_id='',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		get_ppa_login();
		$data_login_ppa=get_ppa_login();
		$str='["1","2"]';
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// $data_satuan_ttv=array();
		$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan,makral_satuan,mcahaya_satuan,mgcs_eye_satuan,mgcs_motion_satuan,mgcs_voice_satuan,mpupil_satuan,mspo2_satuan,mgds_satuan")->row_array();
		
		$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id);
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tperubahan/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		// $data['array_menu_kiri'] 	= $this->Tperubahan_model->list_menu_kiri($menu_atas);
		// $data['array_menu_kiri'] 	= array('input_ttv','input_data');
		
		// print_r(json_encode($data['array_menu_kiri']));exit;
		$data['idpoli'] 			= '#';
		
		$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
		$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
		$data['konsul_id'] 			= '';
		$data['status_konsul'] 			= '';
		$data['st_edited'] 			= '0';
		$data['error'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'Konsultasi';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Konsultasi ",'#'),
											  array("Dokter",'tperubahan')
											);
		$data_assemen=array('idpoliklinik'=>'','iddokter_dari'=>'');
		// print_r($data_assemen);exit;
		//Input Baru
		if ($menu_kiri=='lihat_perubahan'){
				$data_label=$this->Tpoliklinik_resep_model->setting_label();
				$data = array_merge($data,$data_label);
				$data_assemen=$this->Tpoliklinik_resep_model->get_data_assesmen_trx($trx_id);
				$data['list_ppa'] = $this->Tpoliklinik_resep_model->list_ppa(1);
				$data['list_tujuan_eresep'] = $this->Tpoliklinik_resep_model->list_tujuan_eresep_all();
				$data['list_telaah']=$this->Tpoliklinik_resep_model->get_telaah($data_assemen['assesmen_id']);
				$data['list_telaah_obat']=$this->Tpoliklinik_resep_model->get_telaah_obat($data_assemen['assesmen_id']);
				
				$q="SELECT racikan_id,nama_racikan,jumlah_racikan,jenis_racikan,interval_racikan,rute_racikan,aturan_tambahan_racikan,iter_racikan,status_racikan
					FROM tpoliklinik_e_resep_obat_racikan H WHERE H.assesmen_id='".$data_assemen['assesmen_id']."' AND H.status_racikan='1'";	
				$data_racikan=$this->db->query($q)->row_array();
				if ($data_racikan){}else{
					$data_racikan['racikan_id']='';
					$data_racikan['nama_racikan']='';
					$data_racikan['jumlah_racikan']='';
					$data_racikan['jenis_racikan']='';
					$data_racikan['interval_racikan']='';
					$data_racikan['rute_racikan']='';
					$data_racikan['aturan_tambahan_racikan']='';
					$data_racikan['iter_racikan']='';
					$data_racikan['status_racikan']='';
				}
				$setting_assesmen=$this->Tpoliklinik_resep_model->setting_eresep();
				// print_r($setting_assesmen);exit;
				$data = array_merge($data,$data_assemen,$data_racikan,$setting_assesmen);
			// $data = array_merge($data,$data_assemen);
		}
		
		
		$data['trx_id']=$trx_id;
		$data['pendaftaran_id']=$pendaftaran_id;
		$data['list_dokter']=$this->Tperubahan_model->list_dokter($data['idpoliklinik']);
		$data['list_dokter_all']=$this->Tperubahan_model->list_dokter_all();
		$data['list_poli']=$this->Tperubahan_model->list_poliklinik();
		// print_r('Langnsung');exit;
		$data = array_merge($data, backend_info());
		// $data = array_merge($data,$data_login_ppa,$data_satuan_ttv,$data_header_ttv,$data_assemen, backend_info());
		// print_r($data);exit;
		$this->parser->parse('module_template', $data);
		
	}
	
}	
