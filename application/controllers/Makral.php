<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Makral extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Makral_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data=$this->Makral_model->get_akral();
		// print_r($data);exit;
		if (UserAccesForm($user_acces_form,array('1734'))){
			
			$data['tab'] 			= $tab;
			$data['error'] 			= '';
			$data['title'] 			= 'Referensi Akral';
			$data['content'] 		= 'Makral/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Referensi Akral",'makral')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	

  function simpan_akral(){
		$akral_id=$this->input->post('akral_id');
		$data=array(
			'akral_1'=>$this->input->post('akral_1'),
			'akral_2'=>$this->input->post('akral_2'),
			'kategori_akral'=>$this->input->post('kategori_akral'),
			'warna'=>$this->input->post('warna'),
			'staktif'=>1,
		);
		if ($akral_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('makral',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$akral_id);
		    $hasil=$this->db->update('makral',$data);
		}
		  
		  json_encode($hasil);
	}
	function update_satuan(){
		$satuan_akral=$this->input->post('satuan_akral');
		$data=array(
			'satuan_akral'=>$this->input->post('satuan_akral'),
			
		);
		$hasil=$this->db->update('makral_satuan',$data);
		
		json_encode($hasil);
	}
	
	function load_akral()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
							FROM `makral` H
							where H.staktif='1'
							ORDER BY H.akral_1


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('akral_1','akral_1','kategori_akral');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->akral_1.' - '.$r->akral_2);
          $result[] = $r->kategori_akral;
          $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'" type="button" >'.$r->warna.'</button>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('1736'))){
		  $aksi .= '<button onclick="edit_akral('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		   if (UserAccesForm($user_acces_form,array('1737'))){
		  $aksi .= '<button onclick="hapus_akral('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_akral(){
	  $akral_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$akral_id);
		$hasil=$this->db->update('makral',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_akral(){
	  $akral_id=$this->input->post('id');
	  $q="SELECT *FROM makral H WHERE H.id='$akral_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
  
}
