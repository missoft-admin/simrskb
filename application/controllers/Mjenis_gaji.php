<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mjenis_gaji extends CI_Controller {

	/**
	 * Master Jenis controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mjenis_gaji_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Jenis';
		$data['content'] 		= 'Mjenis_gaji/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master Jenis",'#'),
									    			array("List",'mjenis_gaji')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'deskripsi'			=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Jenis';
		$data['content'] 		= 'Mjenis_gaji/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master Jenis",'#'),
									    			array("Tambah",'mjenis_gaji')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mjenis_gaji_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'deskripsi' 		=> $row->deskripsi,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Jenis';
				$data['content']	 	= 'Mjenis_gaji/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Jenis",'#'),
											    			array("Ubah",'mjenis_gaji')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mjenis_gaji','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mjenis_gaji');
		}
	}
	function setting($id){
		if($id != ''){
			$row = $this->Mjenis_gaji_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'deskripsi' 		=> $row->deskripsi,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Jenis';
				$data['content']	 	= 'Mjenis_gaji/variable';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Jenis",'#'),
											    			array("Ubah",'mjenis_gaji')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mjenis_gaji','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mjenis_gaji');
		}
	}

	function delete($id){
		$this->Mjenis_gaji_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mjenis_gaji','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		// print_r($this->input->post());exit();
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mjenis_gaji_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mjenis_gaji','location');
				}
			} else {
				if($this->Mjenis_gaji_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mjenis_gaji','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}
	
	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mjenis_gaji/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Jenis';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Jenis",'#'),
															array("Tambah",'mjenis_gaji')
													);
		}else{
			$data['title'] = 'Ubah Master Jenis';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Jenis",'#'),
															array("Ubah",'mjenis_gaji')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$this->from   = 'mjenis_gaji';
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama');
      $this->column_order    = array('nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					if ($r->status=='1'){
					// if (UserAccesForm($user_acces_form,array('131'))){
						$aksi .= '<a href="'.site_url().'mjenis_gaji/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
						$aksi .= '<a href="'.site_url().'mjenis_gaji/setting/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-success btn-sm"><i class="fa fa-list"></i> Add Variable</a>';
					// }
					// if (UserAccesForm($user_acces_form,array('132'))){
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mjenis_gaji" data-urlremove="'.site_url().'mjenis_gaji/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
					// }
					}else{
					
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mjenis_gaji" data-urlremove="'.site_url().'mjenis_gaji/aktifkan/'.$r->id.'" data-toggle="tooltip" title="Aktifkan" class="btn btn-success btn-sm aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
					}
					
					
			        $aksi .= '</div>';

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = $r->deskripsi;
          $row[] = StatusBarang($r->status);
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_akun()
  {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$this->from   = "(SELECT *from makun_nomor M
							WHERE M.`status`='1'
							ORDER BY M.noakun ASC) as tbl";
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('namaakun','noakun');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					// if ($r->stheader !='1'){
					$aksi .= "<button type='button' title='Pilih' class='btn btn-xs btn-primary pilih'><i class='fa fa-check'></i> Pilih</button>";	
					// }					
			        $aksi .= '</div>';

          $row[] = $r->id;
          $row[] = TreeView($r->level,$r->noakun.' - '.$r->namaakun);
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
   function getVariableRekapan($idjenis='1',$idtipevariable)
	{
		$html = '';
		$where = '';
		if ($idtipevariable!='0'){
			$where="AND M.idtipe='$idtipevariable'";
		}
		$q="SELECT M.*,COUNT(MD.id) as sub from mvariable M
LEFT JOIN mvariable MD ON MD.idheader=M.id
			WHERE M.id NOT IN (SELECT idvariable FROM mjenis_gaji_setting S WHERE S.idjenis='$idjenis' AND S.status='1') AND M.`status`='1' AND M.idsub != '2'
			".$where."
			GROUP BY M.id ";
		$result = $this->db->query($q)->result();

		foreach ($result as $row) {
			$html .= '<tr>';
			$html .= '  <td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" class="checkboxVariable"><span></span></label></td>';
			$html .= '  <td style="display:none">'.$row->id.'</td>';
			$html .= '  <td>'.$row->nama.'</td>';
			$html .= '  <td>'.($row->idtipe == 1 ? "Pendapatan" : "Potongan").($row->sub>0?' <span class="label label-danger">'.$row->sub.' Sub</span>':'').'</td>';
			$html .= '</tr>';
		}

		echo $html;
	}
	function get_list_variable($idjenis='1')
	{
		$html = '';
		
		$q="SELECT *FROM (
				SELECT S.id,S.idvariable,M.nama,S.noakun,CONCAT(A.noakun,'-',A.namaakun) as namaakun
				,CASE WHEN M.idheader='0' THEN CONCAT(M.nourut,'.',M.id) ELSE CONCAT(MH.nourut,'.', M.idheader,'-',M.nourut) END as urutan
				,M.idsub,M.idheader,S.idakun
				FROM mjenis_gaji_setting S
				LEFT JOIN mvariable M ON M.id=S.idvariable
				LEFT JOIN mvariable MH ON MH.id=M.idheader AND MH.`status`!='0'
				LEFT JOIN makun_nomor A ON A.id=S.idakun
				WHERE S.idjenis='$idjenis' AND S.status='1'
			) T ORDER BY T.urutan ASC ";
			$result = $this->db->query($q)->result();

		foreach ($result as $row) {
			$btn_add='';
			if ($row->idakun){
				if ($row->idsub !='1'){
					$btn_add =" <div class='btn-group'>";
					$btn_add .="&nbsp;&nbsp;&nbsp;&nbsp;<button type='button' title='Edit' class='btn btn-xs btn-success add_akun'><i class='fa fa-pencil'></i></button>";
					$btn_add .="<button type='button' title='Hapus Akun' class='btn btn-xs btn-danger hapus_akun'><i class='fa fa-trash-o'></i></button>";
					$btn_add .="</div>";
				}
			}else{
				if ($row->idsub !='1'){
					$btn_add="<button type='button' title='Add No. Akun' class='btn btn-xs btn-primary add_akun'><i class='fa fa-plus'></i> Add No.Akun</button>";
				}
				
			}
			$html .= '<tr>';
			$html .= '  <td style="display:none">'.$row->id.'</td>';
			$html .= '  <td>'.($row->idsub=='2'?' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   └─ '.$row->nama:'<srong>'.$row->nama.'</strong>').'</td>';
			$html .= '  <td>'. $row->namaakun.' '.$btn_add.'</td>';
			if ($row->idsub !='2'){
			$html .= "  <td><button type='button' title='Hapus Variable' class='btn btn-xs btn-danger hapus_var'><i class='fa fa-trash-o'></i></button></td>";
				
			}else{
				$html .= '<td></td>';				
			}
			$html .= '</tr>';
		}

		echo $html;
	}
	function addVariable(){
		$id=$this->input->post('id');
		$idjenis=$this->input->post('idjenis');
		//mjenis_gaji_setting
		foreach($id as $index=>$val){
			$q="SELECT *from mvariable M WHERE (M.id='$val' OR M.idheader='$val') AND M.status='1'";
			$rows=$this->db->query($q)->result();
			foreach($rows as $r){
				$data_det=array(
					'idjenis'=>$idjenis,
					'idvariable'=>$r->id,
					'idsub'=>$r->idsub,
					'idheader'=>$r->idheader,
					'noakun'=>'',
				);
				$result=$this->db->insert('mjenis_gaji_setting',$data_det);
			}
		}
		$this->output->set_output(json_encode($result));
	}
	function update_akun(){
		$idsetting=$this->input->post('idsetting');
		$idakun=$this->input->post('idakun');
		//mjenis_gaji_setting
		$this->db->where('id',$idsetting);
		$result=	$this->db->update('mjenis_gaji_setting',array('idakun'=>$idakun,'edited_by'=>$this->session->userdata('user_id'),'edited_date'=>date('Y-m-d H:i:s')));
		$this->output->set_output(json_encode($result));
	}
	function hapus_akun(){
		$idsetting=$this->input->post('idsetting');
		$noakun=null;
		//mjenis_gaji_setting
		$this->db->where('id',$idsetting);
		$result=	$this->db->update('mjenis_gaji_setting',array('idakun'=>$idakun));
		$this->output->set_output(json_encode($result));
	}
	function hapus_var(){
		$idsetting=$this->input->post('idsetting');
		$idjenis=$this->input->post('idjenis');
		$idvariable=$this->db->query("select idvariable FROM mjenis_gaji_setting WHERE id='$idsetting'")->row('idvariable');
		$iduser=$this->session->userdata('user_id');
		$tgl=date('Y-m-d H:i:s');
		$result=$this->db->query("UPDATE mjenis_gaji_setting SET `status`='0',deleted_by='$iduser',deleted_date='$tgl'  WHERE idjenis='$idjenis' AND (idvariable='$idvariable' OR idheader='$idvariable')");
		// $result="UPDATE mjenis_gaji_setting SET `status`='0',deleted_by='$iduser',deleted_date='$tgl'  WHERE idjenis='$idjenis' AND (idvariable='$idvariable' OR idheader='$idvariable')";
		// print_r($result);exit();
		$this->output->set_output(json_encode($result));
	}
	function aktifkan($id){
		
		$this->Mjenis_gaji_model->aktifkan($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah aktif kembali.');
		redirect('mjenis_gaji','location');
	}
}
