<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mgroup_test extends CI_Controller
{
    /**
     * Group Test controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mgroup_test_model');
        $this->load->model('Mtarif_laboratorium_model');
    }

    public function index(): void
    {
        $data = [];
        $data['error'] = '';
        $data['title'] = 'Group Test';
        $data['content'] = 'Mgroup_test/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Group Test', '#'],
            ['List', 'mgroup_test'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create(): void
    {
        $data = [
            'id' => '',
            'nama' => '',
        ];

        $data['error'] = '';
        $data['title'] = 'Tambah Group Test';
        $data['content'] = 'Mgroup_test/manage';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Group Test', '#'],
            ['Tambah', 'mgroup_test'],
        ];

        $data['list_user'] = $this->Mgroup_test_model->getUser();
        $data['list_user_selected'] = [];
        $data['list_tariflab'] = $this->Mgroup_test_model->getTarifLab();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id): void
    {
        if ('' !== $id) {
            $row = $this->Mgroup_test_model->getSpecified($id);
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'nama' => $row->nama,
                    'status' => $row->status,
                ];
                $data['error'] = '';
                $data['title'] = 'Ubah Group Test';
                $data['content'] = 'Mgroup_test/manage';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Group Test', '#'],
                    ['Ubah', 'mgroup_test'],
                ];

                $data['list_user'] = $this->Mgroup_test_model->getUser();
                $data['list_user_selected'] = $this->Mgroup_test_model->getUserSelected($id);
                $data['list_tariflab'] = $this->Mgroup_test_model->getTarifLab();
                $data['list_pemeriksaan'] = $this->Mgroup_test_model->getListPemeriksaan($id);

                // Modal Pemeriksaan
                $idtipe = 1;
                $idparent = 0;
                $data['list_parent'] = $this->Mtarif_laboratorium_model->find_index_parent($idtipe);
                $data['list_subparent'] = $this->Mtarif_laboratorium_model->find_index_subparent($idparent);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mkelas', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mgroup_test');
        }
    }

    public function delete($id): void
    {
        $this->Mgroup_test_model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mgroup_test', 'location');
    }

    public function save(): void
    {
        if ('' === $this->input->post('id')) {
            if ($this->Mgroup_test_model->saveData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                redirect('mgroup_test', 'location');
            }
        } else {
            if ($this->Mgroup_test_model->updateData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                redirect('mgroup_test', 'location');
            }
        }
    }

    public function getIndex(): void
    {
        $data_user = get_acces();
        $user_acces_form = $data_user['user_acces_form'];
        $this->select = ['merm_group_test.*', 'COUNT(merm_group_test_pemeriksaan.id) AS jumlah_pemeriksaan'];
        $this->from = 'merm_group_test';
        $this->join = [
            ['merm_group_test_pemeriksaan', 'merm_group_test_pemeriksaan.idgroup_test = merm_group_test.id', ''],
        ];
        $this->order = [
            'merm_group_test.id' => 'DESC',
        ];
        $this->group = [
            'merm_group_test.id',
        ];

        $this->column_search = ['merm_group_test.nama'];
        $this->column_order = ['merm_group_test.nama'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = $r->nama;
            $row[] = $r->jumlah_pemeriksaan;
            $row[] = StatusRow($r->status);
            $aksi = '<div class="btn-group">';
            if (UserAccesForm($user_acces_form, ['205'])) {
                $aksi .= '<a href="'.site_url().'mgroup_test/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
            }
            if (UserAccesForm($user_acces_form, ['206'])) {
                $aksi .= '<a href="#" data-urlindex="'.site_url().'mrak" data-urlremove="'.site_url().'mgroup_test/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
            }
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function getDaftarPemeriksaan(): void
    {
        $idtipe = $this->input->post('idtipe');
        $idparent = $this->input->post('idparent');
        $idsubparent = $this->input->post('idsubparent');
        $status_paket = $this->input->post('status_paket');
        $tarif_selected = $this->input->post('tarif_selected') ? $this->input->post('tarif_selected') : [];

        $data_user = get_acces();
        $user_acces_form = $data_user['user_acces_form'];

        $this->select = ['mtarif_laboratorium.*,
            (CASE
                WHEN mtarif_laboratorium.idpaket = 1 THEN 0
                WHEN mtarif_laboratorium.idpaket = 0 AND merm_group_test_pemeriksaan.idtariflaboratorium IS NOT NULL THEN 1
                WHEN mtarif_laboratorium.idpaket = 0 AND merm_group_test_pemeriksaan.idtariflaboratorium IS NULL THEN 2
            END) AS status_group_test'];
        $this->from = 'mtarif_laboratorium';
        $this->join = [
            ['merm_group_test_pemeriksaan', 'merm_group_test_pemeriksaan.idtariflaboratorium = mtarif_laboratorium.id', 'LEFT'],
        ];
        
        $whereArray = [
            'mtarif_laboratorium.status' => '1',
        ];
        $orWhereArray = [];

        if ($idtipe != '0') {
            $whereArray = array_merge($whereArray, [
                'mtarif_laboratorium.idtipe' => $idtipe,
            ]);
        }

        if ($status_paket != '9') {
            $whereArray = array_merge($whereArray, [
                'mtarif_laboratorium.idpaket' => $status_paket,
            ]);
        }

        if ($idsubparent == '0') {
            if ($idparent != '0') {
                $whereArray = array_merge($whereArray, [
                    'left(mtarif_laboratorium.path,' . strlen($idparent) . ')' => $idparent,
                ]);
            }
        } else {
            if ($idsubparent) {
                $idsubparentArray = explode("_", $idsubparent);
                foreach ($idsubparentArray as $val) {
                    $orWhereArray[] = array("LEFT(mtarif_laboratorium.path, " . strlen($val . '.') . ")" => $val . '.');
                }
            }
        }

        $this->where = $whereArray;
        $this->or_where = $orWhereArray;
				
        $this->order = [
            'path' => 'ASC',
        ];

        $this->group = [];

        if (UserAccesForm($user_acces_form, ['154'])) {
            $this->column_search = ['nama'];
        } else {
            $this->column_search = [];
        }
        $this->column_order = ['nama'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $status_group_test = 0;
            if (count($tarif_selected) && in_array($r->id, $tarif_selected)) {
                $status_group_test = 1;
            } else {
                $status_group_test = $r->status_group_test;    
            }

            if ($status_group_test != 1) {
                $row[] = '<label class="css-input css-checkbox css-checkbox-primary">
                        <input type="checkbox" class="checkbox_pemeriksaan" data-idtarif="'.$r->id.'" data-namatarif="'.$r->nama.'" value="'.$r->id.'"><span></span>
                    </label>';
            } else {
                $row[] = '';
            }
            $row[] = $no;
            $row[] = TreeView($r->level, $r->nama);
            $row[] = StatusTarifGroupTest($status_group_test);

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }
}

// End of file welcome.php
// Location: ./system/application/controllers/welcome.php
