<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Tpencairan extends CI_Controller
{

    /**
   * Pencairan Controller.
   * Developer @GunaliRezqiMauludi
   */

    function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Tpencairan_model');
    }

    function index()
    {
        $data = array(
          'tanggaldari' => date('d/m/Y'),
          'tanggalsampai' => date('d/m/Y'),
          'deskripsi' => '',
          'status' => '',
        );

        $data['error']      = '';
        $data['title']      = 'Transaksi Pencairan';
        $data['content']    = 'Tpencairan/index';
        $data['breadcrum']  = array(
                                array("RSKB Halmahera",'#'),
                                array("Transaksi Pencairan",'#'),
                                array("List",'Tpencairan')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function filter()
    {
        $data = array(
          'tanggaldari' => $this->input->post('tanggaldari'),
          'tanggalsampai' => $this->input->post('tanggalsampai'),
          'deskripsi' => $this->input->post('deskripsi'),
          'status' => $this->input->post('status'),
        );

        $this->session->set_userdata($data);

        $data['error']      = '';
        $data['title']      = 'Transaksi Pencairan';
        $data['content']    = 'Tpencairan/index';
        $data['breadcrum']  = array(
                                array("RSKB Halmahera",'#'),
                                array("Transaksi Pencairan",'#'),
                                array("List",'Tpencairan')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function review($id)
    {
        $data = array(
          'idpencairan' => $id
        );

        $data['error']      = '';
        $data['title']      = 'Transaksi Pencairan';
        $data['content']    = 'Tpencairan/review';
        $data['breadcrum']  = array(
                                array("RSKB Halmahera",'#'),
                                array("Transaksi Pencairan",'#'),
                                array("Rekapan",'Tpencairan')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function create(){
  		$data = array(
  			'id' 				=> '',
  			'tanggal'   => date('d/m/Y'),
  			'deskripsi' => '',
  			'catatan' 	=> '',
  			'status' 		=> ''
  		);

      $data['error']      = '';
      $data['title']      = 'Transaksi Pencairan';
      $data['content']    = 'Tpencairan/manage';
      $data['breadcrum']  = array(
                              array("RSKB Halmahera",'#'),
                              array("Transaksi Pencairan",'#'),
                              array("Tambah",'Tpencairan')
                            );

  		$data = array_merge($data, backend_info());
  		$this->parser->parse('module_template', $data);
  	}

    function update($id){
  		if($id != ''){
  			$row = $this->Tpencairan_model->getSpecified($id);
  			if(isset($row->id)){
  				$data = array(
  					'id' 				=> $row->id,
  					'tanggal'	  => DMYFormat($row->tanggal),
  					'deskripsi'	=> $row->deskripsi,
  					'catatan' 	=> $row->catatan,
  					'status' 		=> $row->status
  				);

          $data['error']      = '';
          $data['title']      = 'Transaksi Pencairan';
          $data['content']    = 'Tpencairan/manage';
          $data['breadcrum']  = array(
                                  array("RSKB Halmahera",'#'),
                                  array("Transaksi Pencairan",'#'),
                                  array("Ubah",'Tpencairan')
                                );

  				$data = array_merge($data, backend_info());
  				$this->parser->parse('module_template', $data);
  			}else{
  				$this->session->set_flashdata('error',true);
  				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
  				redirect('tpencairan','location');
  			}
  		}else{
  			$this->session->set_flashdata('error',true);
  			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
  			redirect('tpencairan');
  		}
  	}

    function save(){
  		$this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
  		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
  		$this->form_validation->set_rules('catatan', 'Catatan', 'trim|required');

  		if ($this->form_validation->run() == TRUE){
  			if($this->input->post('id') == '' ) {
  				if($this->Tpencairan_model->saveData()){
  					$this->session->set_flashdata('confirm',true);
  					$this->session->set_flashdata('message_flash','data telah disimpan.');
  					redirect('tpencairan','location');
  				}
  			} else {
  				if($this->Tpencairan_model->updateData()){
  					$this->session->set_flashdata('confirm',true);
  					$this->session->set_flashdata('message_flash','data telah disimpan.');
  					redirect('tpencairan','location');
  				}
  			}
  		}else{
  			$this->failed_save($this->input->post('id'));
  		}
  	}

  	function failed_save($id){
  		$data = $this->input->post();
  		$data['error'] 					= validation_errors();
      $data['content']    = 'Tpencairan/manage';

  		if($id==''){
        $data['title']      = 'Transaksi Pencairan';
        $data['breadcrum']  = array(
                                array("RSKB Halmahera",'#'),
                                array("Transaksi Pencairan",'#'),
                                array("Tambah",'Tpencairan')
                              );
  		}else{
        $data['title']      = 'Transaksi Pencairan';
        $data['breadcrum']  = array(
                                array("RSKB Halmahera",'#'),
                                array("Transaksi Pencairan",'#'),
                                array("Ubah",'Tpencairan')
                              );
  		}

  		$data = array_merge($data, backend_info());
  		$this->parser->parse('module_template',$data);
  	}

    function getDataPencairan($idpencairan)
    {
        $query = $this->Tpencairan_model->getDataPencairan($idpencairan);
        $this->output->set_output(json_encode($query));
    }

    // Start Aksi
    // Pengajuan, Kontrabon, C-Arm, Honor Dokter, Kasbon, Gaji Karyawan, Fee Klinik

    // Header Group : History
    function getHeaderGroupKontrabon($tanggal, $idcarabayar)
    {
        $query = $this->Tpencairan_model->getHeaderGroupKontrabon($tanggal, $idcarabayar);
        $this->output->set_output(json_encode($query));
    }

    function getHeaderGroupCarm($idtransaksi)
    {
        $query = $this->Tpencairan_model->getHeaderGroupCarm($idtransaksi);
        $this->output->set_output(json_encode($query));
    }

    function getHeaderGroupGajiKaryawan($idtransaksi)
    {
        $query = $this->Tpencairan_model->getHeaderGroupGajiKaryawan($idtransaksi);
        $this->output->set_output(json_encode($query));
    }

    // History
    function getHistoryPengajuan($idpencairan)
    {
        $query = $this->Tpencairan_model->getHistoryPengajuan($idpencairan);
        $this->output->set_output(json_encode($query));
    }

    function getHistoryKontrabon($idpencairan)
    {
        $query = $this->Tpencairan_model->getHistoryKontrabon($idpencairan);
        $this->output->set_output(json_encode($query));
    }

    function getHistoryCarm($idpencairan)
    {
        $query = $this->Tpencairan_model->getHistoryCarm($idpencairan);
        $this->output->set_output(json_encode($query));
    }

    function getHistoryHonorDokter($idpencairan)
    {
        $query = $this->Tpencairan_model->getHistoryHonorDokter($idpencairan);
        $this->output->set_output(json_encode($query));
    }

    function getHistoryKasbon($idpencairan)
    {
        $query = $this->Tpencairan_model->getHistoryKasbon($idpencairan);
        $this->output->set_output(json_encode($query));
    }

    function getHistoryGajiKaryawan($idpencairan)
    {
        $query = $this->Tpencairan_model->getHistoryGajiKaryawan($idpencairan);
        $this->output->set_output(json_encode($query));
    }

    function getHistoryFeeKlinik($idpencairan)
    {
        $query = $this->Tpencairan_model->getHistoryFeeKlinik($idpencairan);
        $this->output->set_output(json_encode($query));
    }

    // Data
    function getPengajuan() {
      $search = $this->input->post('search');
      $query = $this->Tpencairan_model->getPengajuan($search);
      $this->output->set_output(json_encode($query));
  	}

    function getKontrabon() {
      $search = $this->input->post('search');
      $query = $this->Tpencairan_model->getKontrabon($search);
      $this->output->set_output(json_encode($query));
  	}

    function getCarm() {
      $search = $this->input->post('search');
      $query = $this->Tpencairan_model->getCarm($search);
      $this->output->set_output(json_encode($query));
  	}

    function getHonorDokter() {
      $search = $this->input->post('search');
      $query = $this->Tpencairan_model->getHonorDokter($search);
      $this->output->set_output(json_encode($query));
  	}

    function getKasbon() {
      $search = $this->input->post('search');
      $query = $this->Tpencairan_model->getKasbon($search);
      $this->output->set_output(json_encode($query));
  	}

    function getGajiKaryawan() {
      $search = $this->input->post('search');
      $query = $this->Tpencairan_model->getGajiKaryawan($search);
      $this->output->set_output(json_encode($query));
  	}

    function getFeeKlinik() {
      $search = $this->input->post('search');
      $query = $this->Tpencairan_model->getFeeKlinik($search);
      $this->output->set_output(json_encode($query));
  	}

    // Listing
    function getListPengajuan($idtipepengajuan)
  	{
        $this->select = array('tpengajuan.*, musers.name AS namapemohon, munitpelayanan.nama AS namaunit,
        (CASE
          WHEN tpengajuan.jenispembayaran = 1 THEN
            "Tunai"
          WHEN tpengajuan.jenispembayaran = 2 THEN
            "Kontrabon"
          WHEN tpengajuan.jenispembayaran = 3 THEN
            "Transfer"
          WHEN tpengajuan.jenispembayaran = 4 THEN
            "Termin By Progress"
          WHEN tpengajuan.jenispembayaran = 5 THEN
            "Termin By Fix (Cicilan)"
        END) AS tipe,
        "-" AS termin');

  			$this->from   = 'tpengajuan';

  			$this->join 	= array(
          array('musers', 'musers.id = tpengajuan.idpemohon',''),
          array('munitpelayanan', 'munitpelayanan.id = tpengajuan.untukbagian','')
  			);

        $this->where = array();
        $this->where = array_merge($this->where, array('tpengajuan.status' => 1));

        if($idtipepengajuan){
          $this->where = array_merge($this->where, array('tpengajuan.jenispembayaran' => $idtipepengajuan));
        }

  			$this->order  = array(
  				'tpengajuan.nopengajuan' => 'DESC'
  			);
  			$this->group  = array();

  			$this->column_search = array('tpengajuan.nopengajuan');
  			$this->column_order  = array('tpengajuan.nopengajuan');

  			$list = $this->datatable->get_datatables();
  			$data = array();
  			$no = $_POST['start'];
  			foreach ($list as $r) {
  					$no++;
  					$row = array();

            if($r->stpencairan){
              $row[] = '<td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" disabled><span></span></label></td>';
              $row[] = $r->nopengajuan;
            }else{
              $row[] = '<td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" value="'.$r->id.'"><span></span></label></td>';
              $row[] = "<a href='#' class='selectContentTable' data-idtransaksi='".$r->id."' data-notransaksi='".$r->nopengajuan."' data-idtipe='pengajuan' data-dismiss='modal'>".$r->nopengajuan."</td>";
            }

  					$row[] = $r->subjek;
  					$row[] = $r->tanggal;
  					$row[] = $r->tanggaldibutuhkan;
  					$row[] = $r->namapemohon;
  					$row[] = $r->namaunit;
  					$row[] = StatusPencairan($r->stpencairan);

  					$data[] = $row;
  			}
  			$output = array(
  				"draw" => $_POST['draw'],
  				"recordsTotal" => $this->datatable->count_all(),
  				"recordsFiltered" => $this->datatable->count_all(),
  				"data" => $data
  			);
  			echo json_encode($output);
  	}

    function getListKontrabon($idtipekontrabon)
  	{
        $this->select = array('tkontrabon.*,
        (CASE
          WHEN tkontrabon.carabayar = 1 THEN
            "Cheq"
          WHEN tkontrabon.carabayar = 2 THEN
            "Cash"
        END) AS tipe');

  			$this->from   = 'tkontrabon';

  			$this->join 	= array();

        $this->where = array();
        if($idtipekontrabon){
          $this->where = array_merge($this->where, array('tkontrabon.carabayar' => $idtipekontrabon));
        }

  			$this->order  = array(
  				'tkontrabon.nokontrabon' => 'DESC'
  			);

  			$this->group  = array();

  			$this->column_search = array('tkontrabon.nokontrabon');
  			$this->column_order  = array('tkontrabon.nokontrabon');

  			$list = $this->datatable->get_datatables();
  			$data = array();
  			$no = $_POST['start'];
  			foreach ($list as $r) {
  					$no++;
  					$row = array();

            if($r->stpencairan){
              $row[] = '<td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" disabled><span></span></label></td>';
              $row[] = $r->nokontrabon;
            }else{
              $row[] = '<td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" value="'.$r->id.'"><span></span></label></td>';
              $row[] = "<a href='#' class='selectContentTable' data-idtransaksi='".$r->id."' data-notransaksi='".$r->nokontrabon."' data-idtipe='kontrabon' data-dismiss='modal'>".$r->nokontrabon."</td>";
            }

  					$row[] = $r->tanggalkontrabon;
  					$row[] = $r->tipe;
  					$row[] = number_format($r->grandtotalnominal);
  					$row[] = StatusPencairan($r->stpencairan);

  					$data[] = $row;
  			}
  			$output = array(
  				"draw" => $_POST['draw'],
  				"recordsTotal" => $this->datatable->count_all(),
  				"recordsFiltered" => $this->datatable->count_all(),
  				"data" => $data
  			);
  			echo json_encode($output);
  	}

    function getListCarm()
  	{
        $this->select = array('tcarm_payments.id,
        tcarm_payments.no_transaksi AS notransaksi,
        tcarm_payments.stpencairan,
        CONCAT(tcarm_payments.bulan, tcarm_payments.tahun) AS periode,
        SUM(tcarm_payments_detail.total) AS total,
        tcarm_payments.status');

        $this->from   = 'tcarm_payments';

        $this->join 	= array(
          array('tcarm_payments_detail', 'tcarm_payments_detail.id_transaksi = tcarm_payments.id','')
        );

        $this->where = array();

        $this->order  = array(
  				'tcarm_payments.id' => 'DESC'
  			);

        $this->group  = array('tcarm_payments.id');

        $this->column_search = array('tcarm_payments.no_transaksi');
  			$this->column_order  = array('tcarm_payments.no_transaksi');

  			$list = $this->datatable->get_datatables();
  			$data = array();
  			$no = $_POST['start'];
  			foreach ($list as $r) {
  					$no++;
  					$row = array();

            if($r->stpencairan){
              $row[] = '<td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" disabled><span></span></label></td>';
              $row[] = $r->notransaksi;
            }else{
              $row[] = '<td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" value="'.$r->id.'"><span></span></label></td>';
              $row[] = "<a href='#' class='selectContentTable' data-idtransaksi='".$r->id."' data-notransaksi='".$r->notransaksi."' data-idtipe='carm' data-dismiss='modal'>".$r->notransaksi."</td>";
            }

  					$row[] = $r->periode;
  					$row[] = number_format($r->total);
  					$row[] = StatusPencairan($r->stpencairan);

  					$data[] = $row;
  			}
  			$output = array(
  				"draw" => $_POST['draw'],
  				"recordsTotal" => $this->datatable->count_all(),
  				"recordsFiltered" => $this->datatable->count_all(),
  				"data" => $data
  			);
  			echo json_encode($output);
  	}

    function getListHonorDokter($tahun, $bulan, $iddokter)
  	{
        $this->select = array('thonor_dokter.*', 'mdokter.nama AS namadokter');

        $this->from   = 'thonor_dokter';

        $this->join 	= array(
          array('mdokter','mdokter.id = thonor_dokter.iddokter','')
        );

        $this->where = array();
        if($tahun){
          $this->where = array_merge($this->where, array('thonor_dokter.tahun' => $tahun));
        }
        if($bulan){
          $this->where = array_merge($this->where, array('thonor_dokter.bulan' => $bulan));
        }
        if($iddokter){
          $this->where = array_merge($this->where, array('thonor_dokter.iddokter' => $iddokter));
        }

  			$this->order  = array(
  				'mdokter.nama' => 'DESC'
  			);

  			$this->group  = array();

  			$this->column_search = array('mdokter.nama');
  			$this->column_order  = array('mdokter.nama');

  			$list = $this->datatable->get_datatables();
  			$data = array();
  			$no = $_POST['start'];
  			foreach ($list as $r) {
  					$no++;
  					$row = array();

            $label = $r->namadokter.', Periode '.MONTHFormat($r->bulan).' '.$r->tahun;
            if($r->stpencairan){
              $row[] = '<td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" disabled><span></span></label></td>';
              $row[] = $r->namadokter;
            }else{
              $row[] = '<td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" value="'.$r->id.'"><span></span></label></td>';
              $row[] = "<a href='#' class='selectContentTable' data-idtransaksi='".$r->id."' data-notransaksi='".$label."' data-idtipe='honordokter' data-dismiss='modal'>".$r->namadokter."</td>";
            }

  					$row[] = MONTHFormat($r->bulan);
  					$row[] = $r->tahun;
  					$row[] = number_format($r->nominal);
  					$row[] = StatusPencairan($r->stpencairan);

  					$data[] = $row;
  			}
  			$output = array(
  				"draw" => $_POST['draw'],
  				"recordsTotal" => $this->datatable->count_all(),
  				"recordsFiltered" => $this->datatable->count_all(),
  				"data" => $data
  			);
  			echo json_encode($output);
  	}

    function getListKasbon()
  	{
        $this->select = array('tkasbon.*', '(CASE WHEN tkasbon.idtipe = 1 THEN mdokter.nama ELSE mpegawai.nama END) AS namapegawai');
        $this->from   = 'tkasbon';
        $this->join 	= array(
          array('mdokter', 'mdokter.id = tkasbon.idpegawai', 'LEFT'),
          array('mpegawai', 'mpegawai.id = tkasbon.idpegawai', 'LEFT'),
        );
        $this->where  = array(
          'tkasbon.status' => '1'
        );
        $this->order  = array(
          'tkasbon.id' => 'DESC'
        );
        $this->group  = array();

        $this->column_search   = array('tkasbon.tanggal');
        $this->column_order    = array('tkasbon.tanggal');

  			$list = $this->datatable->get_datatables();
  			$data = array();
  			$no = $_POST['start'];
  			foreach ($list as $r) {
  					$no++;
  					$row = array();

            $label = DMYFormat($r->tanggal).' - '.$r->namapegawai.' - '.$r->catatan;
            if($r->stpencairan){
              $row[] = '<td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" disabled><span></span></label></td>';
              $row[] = DMYFormat($r->tanggal);
            }else{
              $row[] = '<td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" value="'.$r->id.'"><span></span></label></td>';
              $row[] = "<a href='#' class='selectContentTable' data-idtransaksi='".$r->id."' data-notransaksi='".$label."' data-idtipe='kasbon' data-dismiss='modal'>".DMYFormat($r->tanggal)."</td>";
            }

            $row[] = ($r->idtipe == 1 ? 'Dokter' : 'Pegawai');
            $row[] = $r->namapegawai;
            $row[] = $r->catatan;
            $row[] = number_format($r->nominal);
  					$row[] = StatusPencairan($r->stpencairan);

  					$data[] = $row;
  			}
  			$output = array(
  				"draw" => $_POST['draw'],
  				"recordsTotal" => $this->datatable->count_all(),
  				"recordsFiltered" => $this->datatable->count_all(),
  				"data" => $data
  			);
  			echo json_encode($output);
  	}

    function getListGajiKaryawan()
  	{
        $this->select = array('trekap_penggajian.*, SUM(trekap_penggajian_detail.nominal) AS total');

        $this->from   = 'trekap_penggajian';

        $this->join 	= array(
          array('trekap_penggajian_detail', 'trekap_penggajian_detail.idrekap = trekap_penggajian.id', '')
        );

        $this->where  = array();

        $this->order  = array(
          'trekap_penggajian.id' => 'DESC'
        );

        $this->group  = array('trekap_penggajian.id');

        $this->column_search   = array('trekap_penggajian.notransaksi');
        $this->column_order    = array('trekap_penggajian.notransaksi');

  			$list = $this->datatable->get_datatables();
  			$data = array();
  			$no = $_POST['start'];
  			foreach ($list as $r) {
  					$no++;
  					$row = array();

            if($r->stpencairan){
              $row[] = '<td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" disabled><span></span></label></td>';
              $row[] = $r->notransaksi;
            }else{
              $row[] = '<td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" value="'.$r->id.'"><span></span></label></td>';
              $row[] = "<a href='#' class='selectContentTable' data-idtransaksi='".$r->id."' data-notransaksi='".$r->notransaksi."' data-idtipe='gajikaryawan' data-dismiss='modal'>".$r->notransaksi."</td>";
            }

            $row[] = $r->notransaksi;
            $row[] = $r->subyek;
            $row[] = number_format($r->total);
  					$row[] = StatusPencairan($r->stpencairan);

  					$data[] = $row;
  			}
  			$output = array(
  				"draw" => $_POST['draw'],
  				"recordsTotal" => $this->datatable->count_all(),
  				"recordsFiltered" => $this->datatable->count_all(),
  				"data" => $data
  			);
  			echo json_encode($output);
  	}

    function getListFeeKlinik($tahun, $bulan, $idrumahsakit)
  	{
        $this->select = array('tverifikasi_rujukan_klinik_fee.*', 'mrumahsakit.nama AS namarumahsakit');

        $this->from   = 'tverifikasi_rujukan_klinik_fee';

        $this->join 	= array(
          array('mrumahsakit', 'mrumahsakit.id = tverifikasi_rujukan_klinik_fee.idrumahsakit', '')
        );

        $this->where = array();
        if($tahun){
          $this->where = array_merge($this->where, array('tverifikasi_rujukan_klinik_fee.tahun' => $tahun));
        }
        if($bulan){
          $this->where = array_merge($this->where, array('tverifikasi_rujukan_klinik_fee.bulan' => $bulan));
        }
        if($idrumahsakit){
          $this->where = array_merge($this->where, array('tverifikasi_rujukan_klinik_fee.idrumahsakit' => $idrumahsakit));
        }

        $this->order  = array(
          'tverifikasi_rujukan_klinik_fee.id' => 'DESC'
        );

        $this->group  = array('tverifikasi_rujukan_klinik_fee.id');

        $this->column_search   = array('mrumahsakit.nama');
        $this->column_order    = array('mrumahsakit.nama');

  			$list = $this->datatable->get_datatables();
  			$data = array();
  			$no = $_POST['start'];
  			foreach ($list as $r) {
  					$no++;
  					$row = array();

            $label = $r->namarumahsakit.', Periode '.MONTHFormat($r->bulan).' '.$r->tahun;
            if($r->stpencairan){
              $row[] = '<td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" disabled><span></span></label></td>';
              $row[] = $r->namarumahsakit;
            }else{
              $row[] = '<td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" value="'.$r->id.'"><span></span></label></td>';
              $row[] = "<a href='#' class='selectContentTable' data-idtransaksi='".$r->id."' data-notransaksi='".$label."' data-idtipe='feeklinik' data-dismiss='modal'>".$r->namarumahsakit."</td>";
            }

            $row[] = MONTHFormat($r->bulan).' '.$r->tahun;
            $row[] = number_format($r->total);
  					$row[] = StatusPencairan($r->stpencairan);

  					$data[] = $row;
  			}
  			$output = array(
  				"draw" => $_POST['draw'],
  				"recordsTotal" => $this->datatable->count_all(),
  				"recordsFiltered" => $this->datatable->count_all(),
  				"data" => $data
  			);
  			echo json_encode($output);
  	}
    // EOF Listing

    // Function Save & Remove
    function saveData()
    {
        $idrow        = $this->input->post('idrow');
        $idpencairan  = $this->input->post('idpencairan');
        $idtipe       = $this->input->post('idtipe');
        $idtransaksi  = $this->input->post('idtransaksi');

        if($idtipe == 'carm'){
          $this->db->where('id_transaksi', $idtransaksi);
          $query = $this->db->get('tcarm_payments_detail')->result();

          foreach ($query as $row) {
            $this->db->where('id', $row->id);
            $this->db->delete('tpencairan_detail');

            $data = array();
            $data['idpencairan']      = $idpencairan;
            $data['idtipe']           = $idtipe;
            $data['idtransaksi']      = $row->id;

            $this->db->insert('tpencairan_detail', $data);
          }
          return true;
        }else if($idtipe == 'gajikaryawan'){
          $this->db->where('idrekap', $idtransaksi);
          $query = $this->db->get('trekap_penggajian_detail')->result();

          foreach ($query as $row) {
            $this->db->where('id', $row->id);
            $this->db->delete('tpencairan_detail');

            $data = array();
            $data['idpencairan']      = $idpencairan;
            $data['idtipe']           = $idtipe;
            $data['idtransaksi']      = $row->id;

            $this->db->insert('tpencairan_detail', $data);
          }
          return true;
        }else{
          if($idrow != ''){
            $this->db->where('id', $idrow);
            $this->db->delete('tpencairan_detail');
          }

          $data = array();
          $data['idpencairan']      = $idpencairan;
          $data['idtipe']           = $idtipe;
          $data['idtransaksi']      = $idtransaksi;

          if($this->db->insert('tpencairan_detail', $data)){
            return true;
          }else{
            return false;
          }
        }
    }

    function saveBatch()
    {
        $idpencairan  = $this->input->post('idpencairan');
        $idtipe       = $this->input->post('idtipe');
        $multipleTransaction = json_decode($_POST['idtransaksi']);

        foreach ($multipleTransaction as $idtransaksi) {
          if($idtipe == 'carm'){
            $this->db->where('id_transaksi', $idtransaksi);
            $query = $this->db->get('tcarm_payments_detail')->result();

            foreach ($query as $row) {
              $this->db->where('id', $row->id);
              $this->db->delete('tpencairan_detail');

              $data = array();
              $data['idpencairan']      = $idpencairan;
              $data['idtipe']           = $idtipe;
              $data['idtransaksi']      = $row->id;

              $this->db->insert('tpencairan_detail', $data);
            }

          }else if($idtipe == 'gajikaryawan'){
            $this->db->where('idrekap', $idtransaksi);
            $query = $this->db->get('trekap_penggajian_detail')->result();

            foreach ($query as $row) {
              $this->db->where('id', $row->id);
              $this->db->delete('tpencairan_detail');

              $data = array();
              $data['idpencairan']      = $idpencairan;
              $data['idtipe']           = $idtipe;
              $data['idtransaksi']      = $row->id;

              $this->db->insert('tpencairan_detail', $data);
            }

          }else{
            $data = array();
            $data['idpencairan']      = $idpencairan;
            $data['idtipe']           = $idtipe;
            $data['idtransaksi']      = $idtransaksi;

            $this->db->insert('tpencairan_detail', $data);
          }
        }
        return true;
    }

    function removeData()
    {
        $idrow  = $this->input->post('idrow');

        $this->db->where('id', $idrow);
        if($this->db->delete('tpencairan_detail')){
          return true;
        }else{
          return false;
        }
    }

    function removeDataGroup()
    {
        $idrow  = $this->input->post('idrow');
        $idtipe  = $this->input->post('idtipe');

        if($idtipe == 'carm'){
          $this->db->where('id_transaksi', $idrow);
          $result = $this->db->get('tcarm_payments_detail')->result();
        }else if($idtipe == 'gajikaryawan'){
          $this->db->where('idrekap', $idrow);
          $result = $this->db->get('trekap_penggajian_detail')->result();
        }

        foreach ($result as $row) {
          $this->db->where('idtipe', $idtipe);
          $this->db->where('idtransaksi', $row->id);
          $this->db->delete('tpencairan_detail');
        }
        return true;
    }
    // EOF Function Save & Remove

    function getIndex($uri='index')
  	{
  			$this->select   = array('*');

  			$this->from   = 'tpencairan';

  			$this->join 	= array();

        // FILTER
        $this->where  = array();
        if($uri == 'filter'){
          if ($this->session->userdata('tanggaldari') != null) {
            $this->where = array_merge($this->where, array('DATE(tanggal) >=' => YMDFormat($this->session->userdata('tanggaldari'))));
          }
      		if ($this->session->userdata('tanggalsampai') != null) {
            $this->where = array_merge($this->where, array('DATE(tanggal) <=' => YMDFormat($this->session->userdata('tanggalsampai'))));
          }
      		if ($this->session->userdata('deskripsi') != null) {
            $this->where = array_merge($this->where, array('deskripsi LIKE' => '%'.$this->session->userdata('deskripsi').'%'));
          }
      		if ($this->session->userdata('status') != "#") {
            $this->where = array_merge($this->where, array('status' => $this->session->userdata('status')));
          }
        }else{
          $this->where = array_merge($this->where, array('DATE(tanggal)' => date("Y-m-d")));
        }

  			$this->order  = array(
  				'tanggal' => 'DESC'
  			);
  			$this->group  = array();

  			$this->column_search  = array('tanggal','deskripsi');
  			$this->column_order   = array('tanggal','deskripsi');

  			$list = $this->datatable->get_datatables();
  			$data = array();
  			$no = $_POST['start'];
  			foreach ($list as $r) {
  					$no++;
  					$row = array();

            $action = '';
            if(button_roles('tpencairan/update')) {
              $action .= '<a href="'.site_url().'tpencairan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Ubah</a>';
            }
           $action .= '<a href="'.site_url().'tpencairan/print/'.$r->id.'" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i> Print</a>';

  					$row[] = "<span data-idpencairan='".$r->id."'>".$no."</span>";
  					$row[] = $r->tanggal;
  					$row[] = $r->deskripsi;
  					$row[] = StatusTransaksiPencairan($r->status);
  					$row[] = $action;

  					$data[] = $row;
  			}
  			$output = array(
  				"draw" => $_POST['draw'],
  				"recordsTotal" => $this->datatable->count_all(),
  				"recordsFiltered" => $this->datatable->count_all(),
  				"data" => $data
  			);
  			echo json_encode($output);
  	}

    function success(){
  			$this->session->set_flashdata('confirm',true);
  			$this->session->set_flashdata('message_flash','data telah disimpan.');
  			redirect('tpencairan/index','location');
  	}
}

/* End of file Tpencairan.php */
/* Location: ./application/controllers/Tpencairan.php */
