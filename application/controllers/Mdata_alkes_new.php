<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdata_alkes_new extends CI_Controller {

	/**
	 * Alat Kesehatan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mdata_alkes_new_model');
		$this->load->model('Mdata_obat_new_model');
		$this->load->helper('path');
  }

	function index($idkategori='0'){
		// $row=$this->Mdata_alkes_new_model->get_array_kategori('00024-00025');
		// print_r($this->input->post());exit();
		$data = array();
		$data['idkategori'] 			= $idkategori;
		$data['error'] 			= '';
		$data['title'] 			= 'Alat Kesehatan';
		$data['content'] 		= 'Mdata_alkes_new/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Alat Kesehatan",'#'),
									    			array("List",'mdata_alkes')
													);
		$data['list_kategori'] = $this->Mdata_alkes_new_model->getKategori();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data_default=$this->Mdata_obat_new_model->getDefault();
		$data = array(
			'id' 											=> '',
			'kode' 										=> '',
			'idkategori' 							=> '',
			'nama' 										=> '',
			'komposisi' 							=> '',
			'indikasi' 								=> '',
			'idtipe' 									=> '',
			'idsatuanbesar' 					=> '',
			'hargasatuanbesar' 				=> '',
			'jumlahsatuanbesar' 			=> '',
			'idsatuankecil' 					=> '',
			'ppn'            		      => '',
			'hargabeli'            		=> '',
			'hargadasar'            	=> '',
			'idrakgudang' 						=> '',
			'idrakfarmasi'=> '',
			
			'catatan' => '',
			'status' 									=> '',
			'nama_generik' => '',
			'dosis' => '',
			'merk' => '',
			'high_alert' => '0',
			'nama_industri' => '0',
			'bentuk_sediaan' => '',
			'gol_obat' => '',
			'jenis_generik' => '',
			'formularium' => '',
			'idsatuanlabel' => '',
			'idsatuandosis' => '',
		);
		$data['tab'] 			= '1';
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Alat Kesehatan';
		$data['content'] 		= 'Mdata_alkes_new/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Alat Kesehatan",'#'),
									    			array("Tambah",'mdata_alkes')
													);

		$data['list_kategori'] = $this->Mdata_alkes_new_model->getKategori();
		$data['list_satuan'] = $this->Mdata_alkes_new_model->getSatuan();
		$data['list_rakgudang'] = $this->Mdata_alkes_new_model->getRak(1);
		$data['list_rakfarmasi'] = $this->Mdata_alkes_new_model->getRak(2);
		$data['list_data_history'] = array();

		$data=array_merge($data,$data_default);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Mdata_alkes_new_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 											=> $row->id,
					'kode' 										=> $row->kode,
					'idkategori' 							=> $row->idkategori,
					'nama' 										=> $row->nama,
					'komposisi' 							=> $row->komposisi,
					'indikasi' 								=> $row->indikasi,
					'idtipe' 									=> $row->idtipe,
					'idsatuanbesar' 					=> $row->idsatuanbesar,
					'hargasatuanbesar' 				=> $row->hargasatuanbesar,
					'jumlahsatuanbesar' 			=> $row->jumlahsatuanbesar,
					'idsatuankecil' 					=> $row->idsatuankecil,
					'ppn'                     => $row->ppn,
					'hargabeli'               => $row->hargabeli,
					'hargadasar'              => $row->hargadasar,
					'idrakgudang' 						=> $row->idrakgudang,
					'idrakfarmasi' 						=> $row->idrakfarmasi,
					'alokasiumum' 						=> $row->alokasiumum,
					'alokasiasuransi' 				=> $row->alokasiasuransi,
					'alokasijasaraharja' 			=> $row->alokasijasaraharja,
					'alokasibpjskesehatan' 		=> $row->alokasibpjskesehatan,
					'alokasibpjstenagakerja' 	=> $row->alokasibpjstenagakerja,
					'marginumum' 							=> $row->marginumum,
					'marginasuransi' 					=> $row->marginasuransi,
					'marginjasaraharja' 			=> $row->marginjasaraharja,
					'marginbpjskesehatan' 		=> $row->marginbpjskesehatan,
					'marginbpjstenagakerja' 	=> $row->marginbpjstenagakerja,
					'stokreorder' 						=> $row->stokreorder,
					'stokminimum' 						=> $row->stokminimum,
					'catatan' 								=> $row->catatan,
					'status' 									=> $row->status,
					'nama_generik' => $row->nama_generik,
					'dosis' => $row->dosis,
					'merk' => $row->merk,
					'high_alert' => $row->high_alert,
					'nama_industri' => $row->nama_industri,
					'bentuk_sediaan' => $row->bentuk_sediaan,
					'gol_obat' => $row->gol_obat,
					'formularium' => $row->formularium,
					'jenis_generik' => $row->jenis_generik,
					'idsatuanlabel' => $row->idsatuanlabel,
					'idsatuandosis' => $row->idsatuandosis,

				);
				// print_r($data);exit;
				$data['tab'] 			= '1';
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Alat Kesehatan';
				$data['content']	 	= 'Mdata_alkes_new/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Alat Kesehatan",'#'),
											    			array("Ubah",'mdata_alkes')
															);

				$data['list_kategori'] = $this->Mdata_alkes_new_model->getKategori();
				$data['list_satuan'] = $this->Mdata_alkes_new_model->getSatuan();
				$data['list_rakgudang'] = $this->Mdata_alkes_new_model->getRak(1);
				$data['list_rakfarmasi'] = $this->Mdata_alkes_new_model->getRak(2);
				$data['list_data_history'] = $this->Mdata_alkes_new_model->list_data_history($id);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mdata_alkes','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mdata_alkes');
		}
	}

	function delete($id){
		
		$this->Mdata_alkes_new_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mdata_alkes','location');
	}
	function aktifkan($id){
		
		$this->Mdata_alkes_new_model->aktifkan($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah aktif kembali.');
		redirect('mdata_alkes','location');
	}

	function save(){
		// print_r($this->input->post());exit;
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mdata_alkes_new_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mdata_alkes_new','location');
				}
			} else {
				if($this->Mdata_alkes_new_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mdata_alkes_new','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		print_r($data);exit();
		$data['error'] 					= validation_errors();
		$data['content'] 				= 'Mdata_alkes_new/manage';

		$data['list_kategori'] = $this->Mdata_alkes_new_model->getKategori();
		$data['list_satuan'] = $this->Mdata_alkes_new_model->getSatuan();
		$data['list_rakgudang'] = $this->Mdata_alkes_new_model->getRak(1);
		$data['list_rakfarmasi'] = $this->Mdata_alkes_new_model->getRak(2);

		if($id==''){
			$data['title'] = 'Tambah Alat Kesehatan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Alat Kesehatan",'#'),
															array("Tambah",'mdata_alkes')
													);
		}else{
			$data['title'] = 'Ubah Alat Kesehatan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Alat Kesehatan",'#'),
															array("Ubah",'mdata_alkes')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	function getIndex()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		$status=$this->input->post('status');
		$idkategori=$this->input->post('idkategori');
		$bentuk_sediaan=$this->input->post('bentuk_sediaan');
		$idtipe=$this->input->post('idtipe');
		$nama=$this->input->post('nama');
		$merk=$this->input->post('merk');
		$where='';
		if ($nama!=''){
			$where .=" AND H.nama LIKE '%".ReplaceKutip($nama)."%'";
		}
		if ($idkategori!='0'){
			$row=$this->Mdata_alkes_new_model->get_array_kategori($idkategori);
			$row=implode(", ", $row);
			$where .=' AND H.idkategori IN ('.$row.')';
		}
		if ($bentuk_sediaan){
			
			$bentuk_sediaan=implode(", ", $bentuk_sediaan);
			$where .=' AND H.bentuk_sediaan IN ('.$bentuk_sediaan.')';
		}
		if ($idtipe){
			$idtipe=implode(", ", $idtipe);
			$where .=' AND H.idtipe IN ('.$idtipe.')';
		}
		if ($merk){
			$merk=implode(", ", $merk);
			$where .=' AND H.merk IN ('.$merk.')';
		}
		if ($status!='#'){
			
			$where .=' AND H.status='.$status;
		}
		
		$from="(
			SELECT K.nama as namakategori,H.* FROM mdata_alkes H
				INNER JOIN mdata_kategori K ON K.id=H.idkategori
				WHERE H.id IS NOT NULL ".$where."
				ORDER BY H.kode ASC
			) tbl
		";
			$this->select = array();
			$this->from   =  $from;
			$this->join 	= array();
			$this->wher = array();
			
			
			$this->order  = array();
			$this->group  = array();

			$this->column_search   = array('kode','nama','namakategori');
			$this->column_order    = array();
			
			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->kode;
					$row[] = $r->nama;
					$row[] = $r->namakategori;
					$row[] = number_format($r->hargasatuanbesar,0);
					$row[] = number_format($r->hargadasar,0);
					$row[] = StatusBarang($r->status);
					$aksi = '<div class="btn-group">';
					if ($r->status=='1'){
						if (UserAccesForm($user_acces_form,array('113','114','115','116'))){
							$aksi .= '<a href="'.site_url().'mdata_alkes_new/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
						}
						if (UserAccesForm($user_acces_form,array('117'))){
							$aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_alkes_new" data-urlremove="'.site_url().'mdata_alkes_new/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
						}
					}else{
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_alkes_new" data-urlremove="'.site_url().'mdata_alkes_new/aktifkan/'.$r->id.'" data-toggle="tooltip" title="Aktifkan" class="btn btn-success btn-sm aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
					}
		            $aksi .= '</div>';
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
	function refresh_select2(){
		$ref_head_id=$this->input->post('ref_head_id');
		
		$q="SELECT *FROM merm_referensi where status='1' AND ref_head_id='$ref_head_id'";
		$list=$this->db->query($q)->result();
		$opsi='<option value="" selected>Pilih Opsi</option>';
		foreach($list as $r){
			$opsi .='<option value="'.$r->nilai.'">'.$r->ref.'</option>';
		}
		$this->output->set_output(json_encode($opsi));
	}
	function list_referensi()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		$ref_head_id=$this->input->post('ref_head_id');
			$this->select = array('merm_referensi.*');
			$this->from   = 'merm_referensi';
			$this->order  = array(
				'merm_referensi.nilai' => 'ASC'
			);
			$this->where_in = array(
					'merm_referensi.ref_head_id' => $ref_head_id,
					'merm_referensi.status' => 1,
				);
			$this->group  = array();

			$this->column_search   = array('merm_referensi.ref');
			$this->column_order    = array('merm_referensi.ref');
			
			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $r->nilai;
					$row[] = $r->ref;
					$nama="'".$r->ref."'";
					$aksi = '<div class="btn-group">';
					if ($r->status=='1'){
							$aksi .= '<button data-toggle="tooltip"  type="button" onclick="edit_ref('.$r->id.','.$nama.')" title="Edit" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button>';
							$aksi .= '<button onclick="hapus_ref('.$r->id.')" type="button" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>';
					}else{
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_alkes" data-urlremove="'.site_url().'mdata_alkes/aktifkan/'.$r->id.'" data-toggle="tooltip" title="Aktifkan" class="btn btn-success btn-sm aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
					}
		            $aksi .= '</div>';
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
	function load_data_ref(){
		$ref_head_id=$this->input->post('ref_head_id');
		$q="SELECT H.referensi_nama,COUNT(D.id)+1 as nilai FROM merm_referensi_head H
		LEFT JOIN merm_referensi D ON D.ref_head_id=H.id
		WHERE H.id='$ref_head_id'";
		$data=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($data));
	}
	function simpan_referensi(){
		$nilai_ref=$this->input->post('nilai_ref');
		$ref_head_id=$this->input->post('ref_head_id');
		$ref_id=$this->input->post('ref_id');
		$nama_ref=$this->input->post('nama_ref');
		$data=array(
			'ref_head_id'=>$ref_head_id,
			'ref'=>$nama_ref,
			'nilai'=>$nilai_ref,
		);
		if ($ref_id){
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$ref_id);
			$hasil=$this->db->update('merm_referensi',$data);
		}else{
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('merm_referensi',$data);
		}
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_ref(){
		$ref_id=$this->input->post('ref_id');
		$data=array(
			'status'=>0,
		);
		if ($ref_id){
			$data['deleted_by']=$this->session->userdata('user_id');
			$data['deleted_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$ref_id);
			$hasil=$this->db->update('merm_referensi',$data);
		
		}
		$this->output->set_output(json_encode($hasil));
	}
	function load_pembelian(){
		$id=$this->input->post('id');
		$nopemesanan=$this->input->post('nopemesanan');
		$nopenerimaan=$this->input->post('nopenerimaan');
		$tanggalpenerimaan_1=$this->input->post('tanggalpenerimaan_1');
		$tanggalpenerimaan_2=$this->input->post('tanggalpenerimaan_2');
		$iddis=$this->input->post('iddis');
		$nofakturexternal=$this->input->post('nofakturexternal');
		$nobatch=$this->input->post('nobatch');
		$tanggalkadaluarsa_1=$this->input->post('tanggalkadaluarsa_1');
		$tanggalkadaluarsa_2=$this->input->post('tanggalkadaluarsa_2');
		$where='';
		if ($nopemesanan!=''){
			$where .=" AND P.nopemesanan LIKE '%".$nopemesanan."%'";
		}
		if ($nopenerimaan!=''){
			$where .=" AND H.nopenerimaan LIKE '%".$nopenerimaan."%'";
		}
		
		if ($iddis) {
			$iddis=implode(", ", $iddis);
            $where .= " AND H.iddistributor IN (".$iddis.")";
        }
		if ($nofakturexternal) {
            $where .=" AND H.nofakturexternal LIKE '%".$nofakturexternal."%'";
        }
		if ($nobatch) {
            $where .=" AND D.nobatch LIKE '%".$nobatch."%'";
        }
		if ($tanggalkadaluarsa_1 !='') {
            $where .= " AND DATE(D.tanggalkadaluarsa) >='".YMDFormat($tanggalkadaluarsa_1)."' AND DATE(D.tanggalkadaluarsa) <='".YMDFormat($tanggalkadaluarsa_2)."'";
        }
		if ($tanggalpenerimaan_1 !='') {
            $where .= " AND DATE(H.tanggalpenerimaan) >='".YMDFormat($tanggalpenerimaan_1)."' AND DATE(H.tanggalpenerimaan) <='".YMDFormat($tanggalpenerimaan_2)."'";
        }
		$q="
			SELECT P.nopemesanan,P.tanggal,H.nopenerimaan,H.nofakturexternal,D.totalharga,H.tanggalpenerimaan,MD.nama as nama_distributor,D.harga_after_diskon,D.nominaldiskon,D.nominalppn,D.harga_after_ppn 
			,D.kuantitas,D.namasatuan,D.nobatch,D.tanggalkadaluarsa
			,H.iddistributor
			FROM tgudang_penerimaan H
			INNER JOIN tgudang_penerimaan_detail D ON D.idpenerimaan=H.id
			INNER JOIN tgudang_pemesanan P ON P.id=H.idpemesanan
			INNER JOIN mdistributor MD ON MD.id=H.iddistributor
			WHERE D.idbarang='$id' AND D.idtipe='1' ".$where."
			ORDER BY H.tanggalpenerimaan ASC
		";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_pembelian='0';
		$no=0;
		foreach ($list as $r) {	
			$no++;
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left">'.$no.'</td>';
			$tabel .='<td class="text-center">'.($r->nopemesanan).'<br>'.HumanDatelong($r->tanggal).'</td>';
			$tabel .='<td class="text-center">'.($r->nopenerimaan).'<br>'.HumanDatelong($r->tanggalpenerimaan).'</td>';
			$tabel .='<td class="text-center">'.($r->nama_distributor).'<br>Faktur : '.($r->nofakturexternal).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->harga_after_diskon,0).'<br>Dic :'.number_format($r->nominaldiskon,0).'<br>PPN : '.number_format($r->nominalppn,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->harga_after_ppn,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).' '.$r->namasatuan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalharga,0).'</td>';
			$tabel .='<td class="text-right">'.($r->nobatch).'</td>';
			$tabel .='<td class="text-right">'.HumanDateShort($r->tanggalkadaluarsa).'</td>';
			
			$tabel .='</tr>';
			$total_pembelian=$total_pembelian+$r->totalharga;
			
		  }
			
		  $arr['tabel']=$tabel;
		  $arr['total_pembelian']=number_format($total_pembelian,0);
		  $this->output->set_output(json_encode($arr));
	}
	function load_history_harga(){
		$id=$this->input->post('id');
		$tanggalupdate_1=$this->input->post('tanggalupdate_1');
		$tanggalupdate_2=$this->input->post('tanggalupdate_2');
		$where='';
		
		if ($tanggalupdate_1 !='') {
            $where .= " AND DATE(H.edited_date) >='".YMDFormat($tanggalupdate_1)."' AND DATE(H.edited_date) <='".YMDFormat($tanggalupdate_2)."'";
        }
		$q="
			SELECT SB.nama as satuan_besar,SK.nama as satuan_kecil,M.`name` as nama_user
				,H.* 
				FROM `mdata_barang_harga` H
				LEFT JOIN msatuan SB ON SB.id=H.idsatuanbesar
				LEFT JOIN msatuan SK ON SK.id=H.idsatuankecil
				LEFT JOIN musers M ON M.id=H.edited_by
			WHERE H.idbarang='$id' AND H.idtipe='1' ".$where."
			ORDER BY H.id 
		";
		$list=$this->db->query($q)->result();
		$tabel='';
		$no=0;
		foreach ($list as $r) {	
			$no++;
			$tabel .='<tr>';
			$tabel .='<td class="text-right">'.$no.'</td>';
			$tabel .='<td class="text-center">'.($r->st_pembelian=='1'?text_warning('PEMBELIAN'):text_primary('MANUAL')).'</td>';
			$tabel .='<td class="text-center">'.($r->satuan_besar).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->hargasatuanbesar,0).'</td>';
			$tabel .='<td class="text-center">'.number_format($r->jumlahsatuanbesar,0).' '.$r->satuan_kecil.'</td>';
			$tabel .='<td class="text-center">'.($r->satuan_kecil).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->hargadasar,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->ppn,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->hargabeli,0).'</td>';
			$tabel .='<td class="text-center">'.($r->nama_user).'<br>'.($r->edited_date?HumanDatelong($r->edited_date):'').'</td>';
			
			$tabel .='</tr>';
			
		  }
			
		  $arr['tabel']=$tabel;
		  $this->output->set_output(json_encode($arr));
	}
}
