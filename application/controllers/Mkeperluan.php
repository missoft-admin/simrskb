<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mkeperluan extends CI_Controller {

	/**
	 * Keperluan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mkeperluan_model');
  }

	function index(){

		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Keperluan';
		$data['content'] 		= 'Mkeperluan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Keperluan",'#'),
									    			array("List",'mkeperluan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){

		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Keperluan';
		$data['content'] 		= 'Mkeperluan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Keperluan",'#'),
									    			array("Tambah",'mkeperluan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){

		if($id != ''){
			$row = $this->Mkeperluan_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Keperluan';
				$data['content']	 	= 'Mkeperluan/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Keperluan",'#'),
											    			array("Ubah",'mkeperluan')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mkeperluan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mkeperluan');
		}
	}

	function delete($id){

		$this->Mkeperluan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mkeperluan','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mkeperluan_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mkeperluan','location');
				}
			} else {
				if($this->Mkeperluan_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mkeperluan','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mkeperluan/manage';

		if($id==''){
			$data['title'] = 'Tambah Keperluan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Keperluan",'#'),
															array("Tambah",'mkeperluan')
													);
		}else{
			$data['title'] = 'Ubah Keperluan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Keperluan",'#'),
															array("Ubah",'mkeperluan')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
			$this->select = array();
			$this->from   = 'mjenis_keperluan';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'nama' => 'ASC'
			);
			$this->group  = array();

      $this->column_search   = array('nama');
      $this->column_order    = array('nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama;
	      	// if(button_roles('mkeperluan/update')) {
	          $aksi = '';
	          $aksi .= '<a href="'.site_url().'mkeperluan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
	        // }
	        // if(button_roles('mkeperluan/delete')) {
	          $aksi .= '<a href="#" data-urlindex="'.site_url().'mkeperluan" data-urlremove="'.site_url().'mkeperluan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
	        // }
	        $aksi .= '</div>';
	        $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
}
