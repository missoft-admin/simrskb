<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tintra_hospital extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tintra_hospital_model');
		$this->load->model('Tpendaftaran_poli_pasien_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->helper('path');
		
  }
	function index($tab='1'){
		$log['path_tindakan']='tintra_hospital';
		$this->session->set_userdata($log);
		$user_id=$this->session->userdata('user_id');
		// print_r($this->session->userdata('path_tindakan'));exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('2046'))){
	
				$date2=date_create(date('Y-m-d'));
				date_add($date2,date_interval_create_from_date_string("-30 days"));
				$date2= date_format($date2,"d/m/Y");
				
				$data['idunit_user'] 			= '#';
				$data['idunit_asal'] 			= '#';
				$data['idunit_tujuan'] 			= '#';
				$data['list_unit_user'] 			= $this->Tintra_hospital_model->list_unit_user();
				$data['list_unit'] 			= $this->Tintra_hospital_model->list_unit();
				$data['tab'] 			= $tab;
				
				$data['tanggal_1'] 			= $date2;
				$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
				$data['error'] 			= '';
				$data['title'] 			= 'My Pasien';
				$data['content'] 		= 'Tintra_hospital/index';
				$data['breadcrum'] 	= array(
													  array("RSKB Halmahera",'#'),
													  array("Tindakan Poliklinik",'#'),
													  array("My Pasien",'tpendaftaran_poli_pasien')
													);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$user_id=$this->session->userdata('user_id');
			$q="
				SELECT GROUP_CONCAT(M.id) as idunit_user  FROM munitpelayanan_user H
				INNER JOIN munitpelayanan M ON M.id=H.idunitpelayanan
				INNER JOIN munitpelayanan_tujuan T ON T.idunit=M.id
				WHERE H.userid='$user_id' AND M.`status`='1'
			";
			$idunit_array=$this->db->query($q)->row('idunit_user');
			$arr=explode(',',$idunit_array);
			$idunit_user =$this->input->post('idunit_user');
			$idunit_asal =$this->input->post('idunit_asal');
			$idunit_tujuan =$this->input->post('idunit_tujuan');
			
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$pencarian =$this->input->post('pencarian');
			$tab =$this->input->post('tab');
			if ($tanggal_1 !=''){
				$where .=" AND DATE(TK.tanggal_input) >='".YMDFormat($tanggal_1)."' AND DATE(TK.tanggal_input) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(TK.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($pencarian!=''){
				$where .=" AND ((H.namapasien LIKE '%".$pencarian."%')";
				$where .=" OR (H.no_medrec LIKE '%".$pencarian."%'))";
			}
			if ($tab=='2'){
				$where .=" AND (TK.idunit_asal = (".$idunit_user."))";
			}
			
			if ($tab=='3'){
				$where .=" AND (TK.idunit_tujuan = (".$idunit_user."))";
			}
			if ($idunit_asal!='#'){
				$where .=" AND (TK.idunit_asal = (".$idunit_asal."))";
			}
			if ($idunit_tujuan!='#'){
				$where .=" AND (TK.idunit_tujuan = (".$idunit_tujuan."))";
			}
			// print_r($tab);exit;
			$this->select = array();
			$q_alergi=get_alergi_sql();
			$from="
				(
					SELECT TK.assesmen_id,TK.pendaftaran_id_ranap,TK.pendaftaran_id, TK.idunit_asal,TK.idunit_tujuan,TK.alasan_transfer,MU1.nama as unit_asal,MU2.nama as unit_tujuan,TK.st_terima,
					MP.nama as nama_poli,TK.idpasien
					,MK.nama as nama_kelompok,MR.nama as nama_rekanan,MDP.nama as nama_dokter_poli
					,CASE WHEN TK.pendaftaran_id_ranap='0' THEN RJ.nopendaftaran ELSE H.nopendaftaran END as nopendaftaran
					,CASE WHEN TK.pendaftaran_id_ranap='0' THEN RJ.tanggaldaftar ELSE H.tanggaldaftar END as tanggaldaftar
					,CASE WHEN TK.pendaftaran_id_ranap='0' THEN RJ.jenis_kelamin ELSE H.jenis_kelamin END as jenis_kelamin
					,CASE WHEN TK.pendaftaran_id_ranap='0' THEN RJ.namapasien ELSE H.namapasien END as namapasien
					,CASE WHEN TK.pendaftaran_id_ranap='0' THEN RJ.no_medrec ELSE H.no_medrec END as no_medrec
					,CASE WHEN TK.pendaftaran_id_ranap='0' THEN RJ.tanggal_lahir ELSE H.tanggal_lahir END as tanggal_lahir
					,CASE WHEN TK.pendaftaran_id_ranap='0' THEN RJ.umurtahun ELSE H.umurtahun END as umurtahun
					,CASE WHEN TK.pendaftaran_id_ranap='0' THEN RJ.umurbulan ELSE H.umurbulan END as umurbulan
					,CASE WHEN TK.pendaftaran_id_ranap='0' THEN RJ.umurhari ELSE H.umurhari END as umurhari
					,CASE WHEN TK.pendaftaran_id_ranap='0' THEN RJ.title ELSE H.title END as title
					,CASE WHEN TK.pendaftaran_id_ranap='0' THEN JK2.ref ELSE JK.ref END as jk
					,CASE WHEN TK.pendaftaran_id_ranap='0' THEN MDP.nama ELSE MD.nama END as nama_dokter
					,MKL.nama as nama_kelas
					,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header
					,MB.nama as nama_bed,TP.tanggal,MRU.nama as nama_ruangan,AH.kodeantrian as kode_antrian
					,UH.name as nama_hapus,GROUP_CONCAT(MDHP.nama SEPARATOR ', ') as nama_dokter_pendamping,PT.nama as ppa_penerima_nama
					,TP.idtipe as idtipe_asal
					FROM tranap_intra_hospital TK
					LEFT JOIN trawatinap_pendaftaran H ON H.id=TK.pendaftaran_id_ranap
					LEFT JOIN tpoliklinik_pendaftaran RJ ON RJ.id=TK.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran_dpjp HP ON HP.pendaftaran_id=H.id
					LEFT JOIN mdokter MDHP ON MDHP.id=HP.iddokter
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TK.pendaftaran_id
					LEFT JOIN mruangan MRU ON MRU.id=H.idruangan
					LEFT JOIN mkelas MKL ON MKL.id=H.idkelas
					LEFT JOIN antrian_harian AH ON AH.id=H.antrian_id
					LEFT JOIN mdokter MDP ON MDP.id=TP.iddokter
					LEFT JOIN mdokter MD ON MD.id=H.iddokterpenanggungjawab
					LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik_asal
					LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
					LEFT JOIN merm_referensi JK2 ON JK2.ref_head_id='1' AND JK2.nilai=RJ.jenis_kelamin
					LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
					LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
					LEFT JOIN mbed MB on MB.id=H.idbed						
					LEFT JOIN musers UH on UH.id=H.deleted_by	
					LEFT JOIN mppa PT on PT.id=TK.ppa_penerima	
					LEFT JOIN munitpelayanan MU1 ON MU1.id=TK.idunit_asal
					LEFT JOIN munitpelayanan MU2 ON MU2.id=TK.idunit_tujuan
					LEFT JOIN (
						".$q_alergi."
					) A ON A.idpasien=H.idpasien			
					WHERE TK.status_assemen='2' AND (TK.idunit_asal IN (".$idunit_array.") OR TK.idunit_tujuan IN (".$idunit_array.")) ".$where."		
					GROUP BY TK.assesmen_id

					ORDER BY TK.tanggal_input DESC
				) as tbl
			";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		  $pedamping='';
		  $btn_jawaban='';
		  if ($r->nama_dokter_pendamping){
			  $pedamping='<div class="text-wrap text-danger"><i class="fa fa-user-md"></i> PENDAMPING : '.($r->nama_dokter_pendamping).'</div>';
		  }
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  $btn_setuju='';
		  $st_terima_pindah=0;
		  if ($r->idunit_tujuan==$idunit_user){//Pindah 
			  if ($r->st_terima=='0'){
				  if (UserAccesForm($user_acces_form,array('2047'))){
				  // $btn_setuju ='<button class="btn btn-xs btn-success btn-block" onclick="set_terima('.$r->assesmen_id.')" type="button"><i class="fa fa-check"></i> TERIMA</button>';
				  $btn_setuju ='<a href="'.base_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_tf/terima_intra_hospital/'.$r->assesmen_id.'" type="button" data-toggle="tooltip" title="Terima"  class="btn btn-block btn-success btn-xs menu_click"><i class="fa fa-check"></i> TERIMA</a>';
				  }
			  }
		  }
		 // $arr
		  if (in_array($r->idunit_asal, $arr)){//MENYERAHKAN 
			 $btn_jawaban .='<button class="btn btn-xs btn-info btn-block" type="button">MENYERAHKAN</button>';
		  }
		  
		  if (in_array($r->idunit_tujuan, $arr)){//MENERIMA 
			 $btn_jawaban .='<button class="btn btn-xs btn-danger btn-block" type="button">MENERIMA</button>';
		  }
		  
		  if ($r->st_terima=='0'){
			  $btn_jawaban .='<button class="btn btn-xs btn-warning btn-block" type="button">BELUM MENERIMA</button>';
		  }else{
			  $btn_jawaban .='<button class="btn btn-xs btn-success btn-block" type="button">SUDAH DITERIMA</button>';
		  }
		  
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
								'.($r->pendaftaran_id_ranap=='0'?'<div class="push-5-t"><a href="'.base_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id.'/erm_tf/input_intra_hospital_rj/'.$r->assesmen_id.'" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block btn-primary btn-xs menu_click"><i class="si si-doc"></i> '.($r->st_terima=='0'?'LIHAT DOKUMENT':'LIHAT DOKUMENT').'</a> </div>':'<div class="push-5-t"><a href="'.base_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_tf/input_intra_hospital/'.$r->assesmen_id.'" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block btn-primary btn-xs menu_click"><i class="si si-doc"></i> '.($r->st_terima=='0'?'LIHAT DOKUMENT':'LIHAT DOKUMENT').'</a> </div>').'
									
									<div class="push-5-t">'.$btn_setuju.'</div>
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
										<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
									</div>
									<div class="text-primary font-s13 push-5-t">'.$r->nopendaftaran.' | '.HumanDateShort($r->tanggal).' | '.GetAsalRujukanR($r->idtipe_asal).' | '.$r->nama_poli.' | '.$r->nama_dokter_poli.'</div>
									<div class="text-primary font-s13 push-5-t"></div>
									
									</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_5='<table class="block-table text-left">
						<tbody style="width:100%">
							<tr>
								<td class="bg-white" style="width: 50%;">
									<div class="text-primary"><strong>Ruangan Asal</strong></div>
									<div class="">'.($r->unit_asal).'</div>
								</td>
								<td class="bg-white" style="width: 50%;">
									<div class="text-primary"><strong>Ruangan Tujuan</strong></div>
									<div class="">'.($r->unit_tujuan).'</div>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="bg-white" style="width: 100%;">
									<div class="text-primary"><strong>Alasan Transfer</strong></div>
									<div class="">'.($r->alasan_transfer).'</div>
								</td>
								
							</tr>
						</tbody>
					</table>';
		 
          $result[] = $btn_5;
		  $div_terima='';
		  $div_hapus='';
		 
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class=" text-uppercase">'.($r->nama_ruangan).' - '.$r->nama_kelas.' - '.$r->nama_bed.'</div>
									<div class=""><i class="fa fa-user-md"></i> DPJP : '.($r->nama_dokter).'</div>
									'.$pedamping.'
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->nama_rekanan?$r->nama_rekanan:$r->nama_kelompok).'</div>
									'.$div_terima.'
									'.$div_hapus.'
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.($btn_jawaban).'</div>
									<div class="h4 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggal).'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function status_jawaban($jawaban,$st_terima_pindah){
	  $btn_jawaban='';
	  if ($jawaban=='0'){
		  $btn_jawaban='<button class="btn btn-xs btn-warning btn-block" type="button">BELUM DIJAWAB</button>';
	  }
	  if ($jawaban=='1'){
		  $btn_jawaban='<button class="btn btn-xs btn-success btn-block" type="button">SETUJU</button>';
	  }
	  if ($jawaban=='2'){
		  $btn_jawaban='<button class="btn btn-xs btn-danger btn-block" type="button">MENOLAK</button>';
	  }
	  if ($st_terima_pindah=='1'){
		  $btn_jawaban .='<button class="btn btn-xs btn-danger btn-block" type="button">PINDAH DPJP</button>';
	  }
	  if ($st_terima_pindah=='2'){
		  $btn_jawaban .='<button class="btn btn-xs btn-info btn-block" type="button">DPJP BARU</button>';
	  }
	  return $btn_jawaban;
  }
  function setuju_pindah(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'st_terima' => 1,
		'jawaban' => 1,
		'tanggal_jawab' => date('Y-m-d H:i:s'),
	  );
	  // print_r($data);exit;
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_intra_hospital',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function tolak_pindah(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'st_terima' => 1,
		'jawaban' => 2,
		'tanggal_jawab' => date('Y-m-d H:i:s'),
	  );
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_intra_hospital',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function setuju_terima(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'st_terima_baru' => 1,
		'jawaban_baru' => 1,
		'tanggal_jawab_baru' => date('Y-m-d H:i:s'),
	  );
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_intra_hospital',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function tolak_terima(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'st_terima_baru' => 1,
		'jawaban_baru' => 2,
		'tanggal_jawab_baru' => date('Y-m-d H:i:s'),
	  );
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_intra_hospital',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function kirim_jawaban(){
		get_ppa_login();
		$intra_hospital_id=$this->input->post('intra_hospital_id');
		$data=array(
			'intra_hospital_jawaban' => $this->input->post('intra_hospital_jawaban'),
			'intra_hospital_anjuran' => $this->input->post('intra_hospital_anjuran'),

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$data['jawaban_ppa']=$login_ppa_id;
		$data['jawaban_date']=date('Y-m-d H:i:s');
		
		$data['st_terima']=1;
		// exit;
		$this->db->where('intra_hospital_id',$intra_hospital_id);
		$result=$this->db->update('tranap_intra_hospital',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
}	
