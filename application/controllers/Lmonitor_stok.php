<?php
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

defined('BASEPATH') or exit('No direct script access allowed');

class Lmonitor_stok extends CI_Controller
{

    /**
     * Monitoring  Stok controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Lmonitor_stok_model');
        $this->load->model('munitpelayanan_model', 'unitpelayanan');
        $this->load->model('Tstockopname_model', 'Tstockopname_model');
    }

    public function index()
    {
        $data = array(
            'idunitpelayanan' => '#',
            'idtipe' => '#',
            'idkategori' => '#',
            'statusstok' => '#',
        );
		// print_r();exit();
        $data['error'] 			= '';
        $data['title'] 			= 'Monitoring  Stok';
        $data['content'] 		= 'Lmonitor_stok/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Monitoring  Stok",'lgudang_stok/index'),
            array("List",'#')
        );

		$data['list_unitpelayanan'] = $this->unitpelayanan->getDefaultUnitPelayananUser();
		$data['idunitpelayanan'] = $this->Lmonitor_stok_model->get_default();
		$data['list_tipe'] = $this->Tstockopname_model->list_tipe($data['idunitpelayanan']);
		// print_r($data['list_tipe']);exit();
		
		// $array_idtipe=$this->Tstockopname_model->list_tipe2('1');
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
   
    public function getBarang()
    {
        $iddariunit     = $this->input->post('idunit');
        // $id     		= $this->input->post('id');
        // $idtipe     	= $this->input->post('idtipe');
        // $idkategori    = $this->input->post('idkategori');
        // $statusstok    = $this->input->post('statusstok');
        // $order   = $this->input->post('order_by');
        // $iduser=$this->session->userdata('user_id');
		$iduser=$this->session->userdata('user_id');
		$array_idtipe=$this->Lmonitor_stok_model->get_tipe_barang_user();
		
        $this->select = array();
        $this->join 	= array();
        $this->where  = array();
       
        $from="(SELECT B.id as idbarang,B.idtipe,
				B.kode,B.nama as namabarang,B.namatipe,K.nama as kategori,msatuan.nama as satuan
				from view_barang B
				LEFT JOIN mdata_kategori K ON K.id=B.idkategori
				inner JOIN msatuan ON msatuan.id=B.idsatuan WHERE B.idtipe IN (".$array_idtipe.")) as tbl ";
		// print_r($from);exit();
        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        $this->column_search   = array('namabarang','namatipe','kode');
        $this->column_order    = array('namabarang','namatipe','kode');
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->kode;
            $row[] = $r->namabarang;
            $row[] = $r->namatipe;
            $row[] = $r->satuan;
			foreach($iddariunit as $x => $val) {
			  $row[] = detail_stok($r->idtipe,$r->idbarang,$val);
			}
            
            // $row[] = detail_stok($r->idtipe,$r->idbarang,4);
			
            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }

   
  
}
