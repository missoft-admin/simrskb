<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tpendaftaran_poli_ttv extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->helper('path');
		
  }
	function get_logic_formulir($pendaftaran_id){
	  $q="SELECT H.*
		, (SELECT  H.tanggal FROM tpoliklinik_pendaftaran D WHERE D.idpasien=H.idpasien AND H.id!=D.id AND D.idpoliklinik=H.idpoliklinik ORDER BY D.tanggal DESC LIMIT 1) as tgl_akhir
		FROM tpoliklinik_pendaftaran H
		WHERE H.id='74342'
		GROUP BY H.id
		";
	  $data_pasien=$this->db->query($q)->row();
	  $hasil='#';
	  if ($data_pasien){
		  
	  }
	  echo $hasil;
  }
	function index($tab='1'){
		get_ppa_login();
		// print_r($this->session->userdata());exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// $q="SELECT H.id as login_mppa_id,H.pegawai_id as login_iddokter,H.pegawai_id as iddokter FROM mppa H WHERE H.user_id='".$this->session->userdata('user_id')."' AND H.tipepegawai='2' AND H.staktif='1'";
		// // $login_iddokter=$this->db->query($q)->row('login_iddokter');	
		// $data = $this->db->query($q)->row_array();	
		// if (){}
		if (UserAccesForm($user_acces_form,array('1597'))){
			// print_r($data);exit;	
			// $data['list_ruangan'] 			= $this->Tpendaftaran_poli_ttv_model->list_ruangan();
			$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
			$data['list_dokter'] 			= $this->Tpendaftaran_poli_ttv_model->list_dokter();
			$data['list_ruang'] 			= $this->Tpendaftaran_poli_ttv_model->list_ruang();
			
			$data['iddokter'] 			= '#';
			$data['ruangan_id'] 			= '#';
			$data['idpoli'] 			= '#';
			$data['namapasien'] 			= '';
			$data['tab'] 			= $tab;
			
			$data['tanggal_1'] 			= DMYFormat(date('2023-09-01'));
			$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
			$data['error'] 			= '';
			$data['title'] 			= 'My Pasien';
			$data['content'] 		= 'Tpendaftaran_poli_ttv/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Tindakan Poliklinik",'#'),
												  array("My Pasien",'tpendaftaran_poli_ttv')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function getIndex_all()	{
			$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
						
			$idpoli =$this->input->post('idpoli');
			$iddokter =$this->input->post('iddokter');
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$pencarian =$this->input->post('pencarian');
			$ruangan_id =$this->input->post('ruangan_id');
			$tab =$this->input->post('tab');
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(H.tanggal) >='".date('Y-m-d')."'";
			}
			if ($pencarian!=''){
				$where .=" AND (H.namapasien LIKE '%".$pencarian."%')";
				$where .=" OR (H.no_medrec LIKE '%".$pencarian."%')";
			}
			if ($tab=='2'){
				$where .=" AND (H.status_input_ttv) = '0'";
			}
			if ($tab=='3'){
				$where .=" AND (H.status_input_ttv) = '1'";
			}
			if ($idpoli!='#'){
				$where .=" AND (H.idpoliklinik) = '$idpoli'";
			}
			if ($iddokter!='#'){
				$where .=" AND (H.iddokter) = '$iddokter'";
			}
			if ($ruangan_id!='#'){
				$where .=" AND (H.ruangan_id) = '$ruangan_id'";
			}
		
			// print_r($tab);exit;
			$this->select = array();
			$from="
					(
						SELECT 
							TP.suhu,TP.nadi,TP.nafas,TP.td_sistole,TP.td_diastole,TP.tinggi_badan,TP.berat_badan
							,mnadi.warna as warna_nadi
							,mnafas.warna as warna_nafas
							,mtd_sistole.warna as warna_sistole
							,mtd_diastole.warna as warna_diastole
							,msuhu.warna as warna_suhu
							,TP.berat_badan/((TP.tinggi_badan/100)*2) as masa_tubuh,mberat.warna as warna_berat
							,MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
							,MK.nama as nama_kelompok,MR.nama as nama_rekanan
							,H.*


							FROM tpoliklinik_pendaftaran H
							INNER JOIN mdokter MD ON MD.id=H.iddokter
							INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
							INNER JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
							INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
							LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
							LEFT JOIN (SELECT * FROM (SELECT * FROM tpoliklinik_ttv WHERE status_ttv > 1 ORDER BY tanggal_input DESC) XX GROUP BY pendaftaran_id) TP ON TP.pendaftaran_id = H.id
							LEFT JOIN mnadi ON TP.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
							LEFT JOIN mnafas ON TP.nafas BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
							LEFT JOIN mtd_sistole ON TP.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
							LEFT JOIN mtd_diastole ON TP.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
							LEFT JOIN msuhu ON TP.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
							LEFT JOIN mberat ON TP.berat_badan/((TP.tinggi_badan/100)*2) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'

							WHERE H.tanggal IS NOT NULL AND H.idtipe = 1 ".$where."
							GROUP BY H.id
						ORDER BY H.tanggal ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  // tpendaftaran_poli_ttv/tindakan/74329/erm_rj/input_ttv
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="push-5-t"><a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->id.'/erm_rj/input_ttv" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block btn-primary btn-xs menu_click"><i class="fa fa-user-md"></i> LAYANI</a> </div>
									<div class="push-5-t">
										<div class="btn-group btn-block">
											<div class="btn-group">
												<button class="btn btn-warning btn-block btn-xs dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
													<span class="fa fa-caret-down"></span> PANEL KENDALI
												</button>
												<ul class="dropdown-menu">
													<li class="dropdown-header">ACTION</li>
													<li>
														<a tabindex="-1" href="javascript:void(0)"> OPTION 1</a>
													</li>
													<li>
														<a tabindex="-1" href="javascript:void(0)"> OPTION 2</a>
													</li>
													<li class="divider"></li>
													<li class="dropdown-header">More</li>
													<li>
														<a tabindex="-1" href="javascript:void(0)"> Lihat Profile..</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Call"  class="btn btn-block btn-success btn-xs"><i class="fa fa-save"></i> FINAL TRANSAKSI</button> </div>
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button"> Tidak Ada ALergi</button>
										<button class="btn btn-danger  btn-sm" type="button"><i class="fa fa-info"></i></button>
									</div>
									
									</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_5='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Suhu Tubuh :</div>
									<div class=" text-muted " style="color:'.$r->warna_suhu.'"> '.($r->suhu).' '.$data_satuan_ttv['satuan_suhu'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frek Nadi :</div>
									<div class=" text-muted " style="color:'.$r->warna_nadi.'"> '.($r->nadi).' '.$data_satuan_ttv['satuan_nadi'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frek Nafas :</div>
									<div class=" text-muted " style="color:'.$r->warna_nafas.'"> '.($r->nafas).' '.$data_satuan_ttv['satuan_nafas'].'</div>
								</td>
								
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Tekanan Darah :</div>
									<div class=" text-muted "> <label  class="text-muted" style="color:'.$r->warna_sistole.'">'.($r->td_sistole.' '.$data_satuan_ttv['satuan_td'].'</label> / <label class="text-muted " style="color:'.$r->warna_diastole.'">'.$r->td_diastole).' '.$data_satuan_ttv['satuan_td'].'</label></div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Berat Badan :</div>
									<div class=" text-muted " style="color:'.$r->warna_berat.'"> '.($r->berat_badan).' Kg</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Tinggi Badan :</div>
									<div class=" text-muted style="color:'.$r->warna_berat.'""> '.($r->tinggi_badan).' Cm</div>
								</td>
								
							</tr>
						</tbody>
					</table>';
		  
          $result[] = $btn_5;
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class="text-muted text-uppercase">'.GetAsalRujukanR($r->idtipe).' - '.$r->nama_poli.'</div>
									<div class="text-muted"><i class="fa fa-user-md"></i> '.($r->nama_dokter).'</div>
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->nama_rekanan?$r->nama_rekanan:$r->nama_kelompok).'</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.($r->status_input_ttv?text_success('TELAH DIPERIKSA'):text_warning('BELUM DIPERIKSA')).'</div>
									<div class="h4 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggal).'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function tindakan($pendaftaran_id='',$menu_atas='',$menu_kiri=''){
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data_satuan_ttv=array();
		$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
		// $data=array();
		$q="SELECT MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
			,MK.nama as nama_kelompok,MR.nama as nama_rekanan
			,H.*,H.title as title_nama,H.id as pendaftaran_id FROM tpoliklinik_pendaftaran H
			INNER JOIN mdokter MD ON MD.id=H.iddokter
			INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
			INNER JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
			INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
			WHERE H.id='$pendaftaran_id'";
		$data=$this->db->query($q)->row_array();
		$data['st_cetak']=0;
		$data_header_ttv=$this->Tpendaftaran_poli_ttv_model->get_header_ttv($pendaftaran_id);
		
		$data_login_ppa=get_ppa_login();
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		$data['array_menu_kiri'] 	= $this->Tpendaftaran_poli_ttv_model->list_menu_kiri($menu_atas);
		$data['array_menu_kiri'] 	= array('input_ttv','input_data');
		
		// print_r(json_encode($data['array_menu_kiri']));exit;
		$data['idpoli'] 			= '#';
		
		$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
		$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
		$data['assesmen_id'] 			= '';
		$data['status_assesmen'] 			= '1';
		$data['error'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'My Pasien';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Tindakan Poliklinik",'#'),
											  array("My Pasien",'tpendaftaran_poli_ttv')
											);
		$logic_akses_ttv=$this->Tpendaftaran_poli_ttv_model->logic_akses_ttv($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_assemen_rj=$this->Tpendaftaran_poli_ttv_model->logic_akses_assemen_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		
		if ($menu_kiri=='input_ttv'){
			$data_ttv=$this->Tpendaftaran_poli_ttv_model->get_data_ttv($pendaftaran_id);
			$setting_ttv=$this->Tpendaftaran_poli_ttv_model->setting_ttv();
			// print_r($logic_akses_ttv);exit;
			$data = array_merge($data_ttv,$data,$setting_ttv);
		}
		if ($menu_kiri=='input_assesmen_rj'){
			$data_ttv=$this->Tpendaftaran_poli_ttv_model->get_data_ttv_all($pendaftaran_id);
			$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_assesmen($pendaftaran_id);
			if ($data_assemen){
				
			}else{
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['template_id']='2';
			}
			$list_edukasi=$this->Tpendaftaran_poli_ttv_model->list_edukasi($data_assemen['assesmen_id']);
				$data_assemen['list_edukasi']=$list_edukasi;
			// print_r($list_edukasi);exit;
			
			$setting_assesmen=$this->Tpendaftaran_poli_ttv_model->setting_assesmen();
			$data = array_merge($data,$setting_assesmen,$data_ttv,$data_assemen);
		}
		
		$data = array_merge($data,$data_login_ppa,$data_satuan_ttv,$data_header_ttv,$logic_akses_ttv,$logic_akses_assemen_rj, backend_info());
		// print_r($data);exit;
		$this->parser->parse('module_template', $data);
		
	}
	function save_ttv(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tanggal_input')). ' ' .$this->input->post('waktudaftar');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$ttv_id=$this->input->post('ttv_id');
		$data=array(
			'pendaftaran_id'=> $this->input->post('pendaftaran_id'),
			'tanggal_input'=> $tanggal_input,
			'tingkat_kesadaran'=> $this->input->post('tingkat_kesadaran'),
			'nadi'=> $this->input->post('nadi'),
			'nafas'=> $this->input->post('nafas'),
			'td_sistole'=> $this->input->post('td_sistole'),
			'td_diastole'=> $this->input->post('td_diastole'),
			'suhu'=> $this->input->post('suhu'),
			'tinggi_badan'=> $this->input->post('tinggi_badan'),
			'berat_badan'=> $this->input->post('berat_badan'),
			'status_ttv'=> $this->input->post('status_ttv'),
		);
		if ($ttv_id==''){
			$data['created_ppa']=$this->input->post('login_ppa_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$result=$this->db->insert('tpoliklinik_ttv',$data);
			$ttv_id=$this->db->insert_id();
			
			if ($result){
				$data['ttv_id']=$ttv_id;
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}else{
			$data['edited_ppa']=$this->input->post('login_ppa_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$ttv_id);
			$result=$this->db->update('tpoliklinik_ttv',$data);
			if ($result){
				$data['ttv_id']=$ttv_id;
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_ttv(){
		$id=$this->input->post('id');
		$data=array(
			
			'status_ttv'=> 0,
		);
			$data['deleted_ppa']=$this->input->post('login_ppa_id');
			$data['deleted_date']=date('Y-m-d H:i:s');
			
			$this->db->where('id',$id);
			$result=$this->db->update('tpoliklinik_ttv',$data);
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_record_ttv(){
		$id=$this->input->post('id');
		$data=array(
			'status_ttv'=> 0,
			'alasan_id'=> $this->input->post('alasan_id'),
			'keterangan_hapus'=> $this->input->post('keterangan_hapus'),
		);
			$data['deleted_ppa']=$this->input->post('login_ppa_id');
			$data['deleted_date']=date('Y-m-d H:i:s');
			
			$this->db->where('id',$id);
			$result=$this->db->update('tpoliklinik_ttv',$data);
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		$this->output->set_output(json_encode($hasil));
	}
	function copy_ttv(){
		$id=$this->input->post('id');
		$q="SELECT CONCAT(MP.nik,' - ',MP.nama) as ppa,H.* FROM tpoliklinik_ttv H
				INNER JOIN mppa MP ON MP.id=H.created_ppa
				WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$hasil['tglpendaftaran']=HumanDateShort(date('Y-m-d'));
		$hasil['waktupendaftaran']=HumanTime(date('H:i:s'));
		$this->output->set_output(json_encode($hasil));
	}
	function edit_ttv(){
		$id=$this->input->post('id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_edit=$this->input->post('keterangan_edit');
		if ($alasan_id){
			$data_edit=array(
				'alasan_edit_id' =>$alasan_id,
				'keterangan_edit' =>$keterangan_edit,
			);
			$this->db->where('id',$id);
			$this->db->update('tpoliklinik_ttv',$data_edit);
		}
		$q="SELECT CONCAT(MP.nik,' - ',MP.nama) as ppa,H.* FROM tpoliklinik_ttv H
				INNER JOIN mppa MP ON MP.id=H.created_ppa
				WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$hasil['tglpendaftaran']=HumanDateShort($hasil['tanggal_input']);
		$hasil['waktupendaftaran']=HumanTime($hasil['tanggal_input']);
		$this->output->set_output(json_encode($hasil));
	}
	function index_TTV_history(){
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
			$login_profesi_id=$this->session->userdata('login_profesi_id');
			
			$logic_akse_ttv=$this->Tpendaftaran_poli_ttv_model->logic_akses_ttv($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
			
			
			$akses_general=$this->db->query("SELECT *FROM setting_ttv")->row_array();
			$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
						
			$pendaftaran_id =$this->input->post('pendaftaran_id');
			$st_owned =$this->input->post('st_owned');
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$profesi_id =$this->input->post('profesi_id');
			$ppa_id =$this->input->post('ppa_id');
			$st_sedang_edit =$this->input->post('st_sedang_edit');
			// print_r($st_sedang_edit);exit;
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($ppa_id){
				$ppa_id=implode(", ", $ppa_id);
				$where .=" AND (H.created_ppa) IN (".$ppa_id.")";
			}
			if ($profesi_id){
				$profesi_id=implode(", ", $profesi_id);
				$where .=" AND (UC.jenis_profesi_id) IN (".$profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}
			
			$this->select = array();
			$from="
					(
						SELECT DATEDIFF(NOW(), H.tanggal_input) as selisih,
						mnadi.warna as warna_nadi
						,mnafas.warna as warna_nafas
						,mtd_sistole.warna as warna_sistole
						,mtd_diastole.warna as warna_diastole
						,msuhu.warna as warna_suhu
						,H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) as masa_tubuh,mberat.warna as warna_berat
						,TK.ref as kesadaran,H.* 
						,UC.nama as nama_created,UE.nama as nama_edited,UC.jenis_profesi_id,malasan_batal.keterangan as ket_edit
						FROM tpoliklinik_ttv H
						LEFT JOIN merm_referensi TK ON TK.nilai=H.tingkat_kesadaran AND TK.ref_head_id='23'
						LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
						LEFT JOIN mnafas ON H.nafas BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
						LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
						LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
						LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
						LEFT JOIN mberat ON H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
						LEFT JOIN mppa UC ON UC.id=H.created_ppa
						LEFT JOIN mppa UE ON UE.id=H.edited_ppa
						LEFT JOIN malasan_batal ON malasan_batal.id=H.alasan_edit_id
						WHERE H.pendaftaran_id='$pendaftaran_id' AND H.status_ttv > 1 ".$where."
						GROUP BY H.id
						ORDER BY H.tanggal_input DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array();
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $btn_disabel_edit='';
		  $btn_disabel_hapus='';
		  if ($st_sedang_edit=='1'){
			$btn_disabel_edit='disabled';			  
		  }
		  
		  $btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_ttv('.$r->id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		  $btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_ttv('.$r->id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
		  $btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_ttv('.$r->id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		  if ($akses_general['st_edit_catatan']=='0'){
			  $btn_edit='';
		  }else{
			  if ($akses_general['lama_edit']>0){
				  if ($r->selisih>$akses_general['lama_edit']){
					  $btn_edit='';
				  }else{
					  
				  }
				  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_edit='';
					  }
				  }
			  }
		  }
		  if ($akses_general['st_duplikasi_catatan']=='0'){
			  $btn_duplikasi='';
		  }else{
			  if ($akses_general['lama_duplikasi']>0){
				  if ($r->selisih>$akses_general['lama_duplikasi']){
					  $btn_duplikasi='';
				  }else{
					 
				  }
			  }
			   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id!=$r->created_ppa){
						$btn_duplikasi='';
				  }
			   }
		  }
		  if ($akses_general['st_hapus_catatan']=='0'){
			  $btn_hapus='';
		  }else{
			  if ($akses_general['lama_hapus']>0){
				  if ($r->selisih>$akses_general['lama_hapus']){
					  $btn_hapus='';
				  }else{
					 
				  }
				   
			  }
			  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id <> $r->created_ppa){
						$btn_hapus='';
				  }else{
					  
				  }
						// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
			  }
		  }
				if ($logic_akse_ttv['st_edit_ttv']=='0'){
				  $btn_edit='';
			  }
			  if ($logic_akse_ttv['st_hapus_ttv']=='0'){
				  $btn_hapus='';
			  }
		 $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 85%;">
								
									
									<div class="h2 font-w700 '.($r->alasan_edit_id?'text-danger':'text-primary').' text-center">'.DFormat($r->tanggal_input).'</div>
									<div class="h5 text-muted text-center"> '.tanggal_indo_MY($r->tanggal_input).'</div>
									<div class="h5 text-muted  text-center"> '.HumanTime($r->tanggal_input).'</div>
									<div class="h5 text-muted text-uppercase push-5-t text-center"> 
										<div class="btn-group" role="group">
											'.$btn_edit.'
											'.$btn_duplikasi.'
											'.$btn_hapus.'
											
											
										</div>
									
									</div>
									'.($r->alasan_edit_id?'<div class="text-center text-success push-5-t"><a href="javascript:void(0)" onclick="lihat_perubahan('.$r->id.')"><i class="fa fa-pencil text-center"></i> '.$r->ket_edit.'</a></div>':'').'
								</td>
							</tr>
						</tbody>
					</table>';
		
          $result[] = $btn_1;
		  $btn_2='
			<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white" colspan="2">
									<div class="h5 "> Tingkat Kesadaran :</div>
									<div class="h5 text-muted  push-5-t "> '.($r->kesadaran).'</div>
									
								</td>
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Frekuensi Nadi :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$r->warna_nadi.'" > '.($r->nadi).' '.$data_satuan_ttv['satuan_nadi'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frekuensi Nafas :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$r->warna_nafas.'"> '.($r->nafas).' '.$data_satuan_ttv['satuan_nafas'].'</div>
								</td>
							</tr>
						</tbody>
					</table>
		  ';
		 
          $result[] = $btn_2;
		  $btn_3 ='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Tekanan Darah :</div>
									<div class="h5 text-muted  push-5-t "> <label  class="h5 text-muted  push-5-t" style="color:'.$r->warna_sistole.'">'.($r->td_sistole.' '.$data_satuan_ttv['satuan_td'].'</label> / <label class="h5 text-muted  push-5-t" style="color:'.$r->warna_diastole.'">'.$r->td_diastole).' '.$data_satuan_ttv['satuan_td'].'</label></div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Suhu Tubuh :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$r->warna_suhu.'"> '.($r->suhu).' '.$data_satuan_ttv['satuan_suhu'].'</div>
								</td>
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Tinggi Badan :</div>
									<div class="h5 text-muted  push-5-t style="color:'.$r->warna_berat.'""> '.($r->tinggi_badan).' Cm</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Berat Badan :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$r->warna_berat.'"> '.($r->berat_badan).' Kg</div>
								</td>
							</tr>
						</tbody>
					</table>
				';
		  
          $result[] = $btn_3;
		  $btn_4 ='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Created</div>
									<div class=" text-muted  push-5-t "><i class="fa fa-user-md"></i> '.$r->nama_created.' <br>('.HumanDateLong($r->created_date).')</div>
								</td>
								
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Edited</div>
									<div class=" text-muted  push-5-t "><i class="fa fa-user-md"></i> '.($r->nama_edited?$r->nama_edited.' <br>'.HumanDateLong($r->edited_date):'').'</div>
								</td>
								
							</tr>
							
						</tbody>
					</table>
				';
		  
          $result[] = $btn_4;
		  
          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function index_ttv_perubahan()	{
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
			$login_profesi_id=$this->session->userdata('login_profesi_id');
			
			$logic_akse_ttv=$this->Tpendaftaran_poli_ttv_model->logic_akses_ttv($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
			
			
			$akses_general=$this->db->query("SELECT *FROM setting_ttv")->row_array();
			$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
						
			$id =$this->input->post('id');
			
			
			$this->select = array();
			$from="
					(
						SELECT DATEDIFF(NOW(), H.tanggal_input) as selisih,
						mnadi.warna as warna_nadi
						,mnafas.warna as warna_nafas
						,mtd_sistole.warna as warna_sistole
						,mtd_diastole.warna as warna_diastole
						,msuhu.warna as warna_suhu
						,H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) as masa_tubuh,mberat.warna as warna_berat
						,TK.ref as kesadaran,H.* 
						,UC.nama as nama_created,UE.nama as nama_edited,UC.jenis_profesi_id,malasan_batal.keterangan as ket_edit
						FROM tpoliklinik_ttv_his H
						LEFT JOIN merm_referensi TK ON TK.nilai=H.tingkat_kesadaran AND TK.ref_head_id='23'
						LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
						LEFT JOIN mnafas ON H.nafas BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
						LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
						LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
						LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
						LEFT JOIN mberat ON H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
						LEFT JOIN mppa UC ON UC.id=H.created_ppa
						LEFT JOIN mppa UE ON UE.id=H.edited_ppa
						LEFT JOIN malasan_batal ON malasan_batal.id=H.alasan_edit_id
						WHERE H.ttv_id='$id'
						GROUP BY H.id
						ORDER BY H.id ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array();
			$this->column_order  = array();
	  
	  $tingkat_kesadaran_awal='';
	  $nadi_awal='';
	  $nafas_awal='';
	  $td_sistole_awal='';
	  $td_diastole_awal='';
	  $tinggi_badan_awal='';
	  $berat_badan_awal='';
	  $suhu_awal='';
	  $warna_sadar='';
	  $warna_normal='';$warna_nadi='';$warna_nafas='';$warna_sistole='';$warna_diastole=''; $warna_suhu='';$warna_tinggi='';$warna_berat='';
	  $warna_tidak_normal='red';
	  
      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		 if ($r->jml_edit>0){
			 $warna_sadar=($tingkat_kesadaran_awal!=$r->tingkat_kesadaran?$warna_tidak_normal:$warna_normal);
			 $warna_nadi=($nadi_awal!=$r->nadi?$warna_tidak_normal:$warna_normal);
			 $warna_nafas=($nafas_awal!=$r->nafas?$warna_tidak_normal:$warna_normal);
			 $warna_sistole=($td_sistole_awal!=$r->td_sistole?$warna_tidak_normal:$warna_normal);
			 $warna_diastole=($td_diastole_awal!=$r->td_diastole?$warna_tidak_normal:$warna_normal);
			 $warna_suhu=($suhu_awal!=$r->suhu?$warna_tidak_normal:$warna_normal);
			 $warna_tinggi=($tinggi_badan_awal!=$r->tinggi_badan?$warna_tidak_normal:$warna_normal);
			 $warna_berat=($berat_badan_awal!=$r->berat_badan?$warna_tidak_normal:$warna_normal);
		 }
		 $tingkat_kesadaran_awal=$r->tingkat_kesadaran;
		 $nadi_awal=$r->nadi;
		 $nafas_awal=$r->nafas;
		 $td_sistole_awal=$r->td_sistole;
		 $td_diastole_awal=$r->td_diastole;
		 $suhu_awal=$r->suhu;
		 $tinggi_badan_awal=$r->tinggi_badan;
		 $berat_badan_awal=$r->berat_badan;
		 $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 85%;">
								
									
									<div class="h2 font-w700 '.($r->alasan_edit_id?'text-danger':'text-primary').' text-center">'.DFormat($r->tanggal_input).'</div>
									<div class="h5 text-muted text-center"> '.tanggal_indo_MY($r->tanggal_input).'</div>
									<div class="h5 text-muted  text-center"> '.HumanTime($r->tanggal_input).'</div>
									
									'.($r->jml_edit>0?'<div class="text-center text-danger push-5-t"><i class="fa fa-pencil text-center"></i> '.$r->ket_edit.' ('.$r->keterangan_edit.')</div>':'<div class="text-center text-primary push-5-t"><i class="si si-anchor text-center"></i> ORIGINAL</div>').'
								</td>
							</tr>
						</tbody>
					</table>';
		
          $result[] = $btn_1;
		  $btn_2='
			<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white" colspan="2">
									<div class="h5 "> Tingkat Kesadaran :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_sadar.'" > '.($r->kesadaran).'</div>
									
								</td>
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Frekuensi Nadi :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_nadi.'" > '.($r->nadi).' '.$data_satuan_ttv['satuan_nadi'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frekuensi Nafas :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_nafas.'"> '.($r->nafas).' '.$data_satuan_ttv['satuan_nafas'].'</div>
								</td>
							</tr>
						</tbody>
					</table>
		  ';
		 
          $result[] = $btn_2;
		  $btn_3 ='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Tekanan Darah :</div>
									<div class="h5 text-muted  push-5-t "> <label  class="h5 text-muted  push-5-t" style="color:'.$warna_sistole.'">'.($r->td_sistole.' '.$data_satuan_ttv['satuan_td'].'</label> / <label class="h5 text-muted  push-5-t" style="color:'.$warna_diastole.'">'.$r->td_diastole).' '.$data_satuan_ttv['satuan_td'].'</label></div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Suhu Tubuh :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_suhu.'"> '.($r->suhu).' '.$data_satuan_ttv['satuan_suhu'].'</div>
								</td>
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Tinggi Badan :</div>
									<div class="h5 text-muted  push-5-t style="color:'.$warna_tinggi.'""> '.($r->tinggi_badan).' Cm</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Berat Badan :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_berat.'"> '.($r->berat_badan).' Kg</div>
								</td>
							</tr>
						</tbody>
					</table>
				';
		  
          $result[] = $btn_3;
		  $btn_4 ='<table class="block-table text-left">
						<tbody>'.($r->jml_edit==0?'
							<tr>
								<td class="bg-white">
									<div class="h5 "> Created</div>
									<div class=" text-muted  push-5-t "><i class="fa fa-user-md"></i> '.$r->nama_created.' <br>('.HumanDateLong($r->created_date).')</div>
								</td>
								
							</tr>':'
							<tr>
								<td class="bg-white">
									<div class="h5 "> Edited</div>
									<div class=" text-muted  push-5-t "><i class="fa fa-user-md"></i> '.($r->nama_edited?$r->nama_edited.' <br>'.HumanDateLong($r->edited_date):'').'</div>
								</td>
								
							</tr>
							').'
						</tbody>
					</table>
				';
		  
          $result[] = $btn_4;
		  
          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  //ASSSESMENT
	function create_assesmen(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_id=$this->input->post('template_id');
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'template_id' => $template_id,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_assesmen',$data);
	  $this->output->set_output(json_encode($hasil));
  }
	function batal_assesmen(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 0,
		
	  );
	  $this->db->where('id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_assesmen',$data);
	  $this->output->set_output(json_encode($hasil));
  }
	function save_assesmen(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'status_assemen' => $this->input->post('status_assemen'),
			'st_anamnesa' => $this->input->post('st_anamnesa'),
			'keluhan_utama' => $this->input->post('keluhan_utama'),
			'nama_anamnesa' => $this->input->post('nama_anamnesa'),
			'hubungan_anamnesa' => $this->input->post('hubungan_anamnesa'),
			'st_riwayat_penyakit' => $this->input->post('st_riwayat_penyakit'),
			'riwayat_penyakit_lainnya' => $this->input->post('riwayat_penyakit_lainnya'),
			'riwayat_alergi' => $this->input->post('riwayat_alergi'),
			'riwayat_pengobatan' => $this->input->post('riwayat_pengobatan'),
			'hubungan_anggota_keluarga' => $this->input->post('hubungan_anggota_keluarga'),
			'st_psikologis' => $this->input->post('st_psikologis'),
			'st_sosial_ekonomi' => $this->input->post('st_sosial_ekonomi'),
			'st_spiritual' => $this->input->post('st_spiritual'),
			'pendidikan' => $this->input->post('pendidikan'),
			'pekerjaan' => $this->input->post('pekerjaan'),
			'st_nafsu_makan' => $this->input->post('st_nafsu_makan'),
			'st_turun_bb' => $this->input->post('st_turun_bb'),
			'st_mual' => $this->input->post('st_mual'),
			'st_muntah' => $this->input->post('st_muntah'),
			'st_fungsional' => $this->input->post('st_fungsional'),
			'st_alat_bantu' => $this->input->post('st_alat_bantu'),
			'st_cacat' => $this->input->post('st_cacat'),
			'cacat_lain' => $this->input->post('cacat_lain'),
			'header_risiko_jatuh' => $this->input->post('header_risiko_jatuh'),
			'footer_risiko_jatuh' => $this->input->post('footer_risiko_jatuh'),
			'st_tindakan' => $this->input->post('st_tindakan'),
			'st_tindakan_action' => $this->input->post('st_tindakan_action'),
			'st_edukasi' => $this->input->post('st_edukasi'),
			'st_menerima_info' => $this->input->post('st_menerima_info'),
			'st_hambatan_edukasi' => $this->input->post('st_hambatan_edukasi'),
			'st_penerjemaah' => $this->input->post('st_penerjemaah'),
			'penerjemaah' => $this->input->post('penerjemaah'),
			'catatan_edukasi' => $this->input->post('catatan_edukasi'),

		);
		// print_r($data);
		// exit;
		$data['created_ppa']=$this->input->post('login_ppa_id');
		$data['created_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_assesmen',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_edukasi(){
		$assesmen_id=$this->input->post('assesmen_id');
		$medukasi_id=$this->input->post('medukasi_id');
		// print_r($medukasi_id);exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->delete('tpoliklinik_assesmen_edukasi');
		if ($medukasi_id){
			foreach($medukasi_id as $index=>$val){
				$data=array(
					'assesmen_id' =>$assesmen_id,
					'medukasi_id' =>$val,
				);
				$hasil=$this->db->insert('tpoliklinik_assesmen_edukasi',$data);
			}
		}
		
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_alergi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$alergi_id=$this->input->post('alergi_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$input_jenis_alergi=$this->input->post('input_jenis_alergi');
		$input_detail_alergi=$this->input->post('input_detail_alergi');
		$input_reaksi_alergi=$this->input->post('input_reaksi_alergi');
		$data=array(
				'idpasien' =>$idpasien,
				'assesmen_id' =>$assesmen_id,
				'pendaftaran_id' =>$pendaftaran_id,
				'input_jenis_alergi' =>$input_jenis_alergi,
				'input_detail_alergi' =>$input_detail_alergi,
				'input_reaksi_alergi' =>$input_reaksi_alergi,
				'staktif' =>1,
			);
		if ($alergi_id){
			$data['edited_by']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$alergi_id);
			$hasil=$this->db->update('tpoliklinik_assesmen_alergi',$data);
			
		}else{
			$data['created_by']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('tpoliklinik_assesmen_alergi',$data);
		}
	
		$this->output->set_output(json_encode($hasil));
	}
	function load_alergi()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.*,R.ref  as jenis_alergi, MP.nama as user_created
						FROM tpoliklinik_assesmen_alergi H
						INNER JOIN merm_referensi R ON R.ref_head_id='24' AND R.nilai=H.input_jenis_alergi
						LEFT JOIN mppa MP ON MP.id=H.created_by
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('jenis_alergi','input_detail_alergi','input_reaksi_alergi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->jenis_alergi);
          $result[] = $r->input_detail_alergi;
          $result[] = $r->input_reaksi_alergi;
          $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  $aksi .= '<button onclick="edit_alergi('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  $aksi .= '<button onclick="hapus_alergi('.$r->id.')" type="button" title="Hapus Suhu" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_alergi_his()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$idpasien=$this->input->post('idpasien');
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.*,R.ref  as jenis_alergi, MP.nama as user_created
						FROM tpoliklinik_assesmen_alergi H
						INNER JOIN merm_referensi R ON R.ref_head_id='24' AND R.nilai=H.input_jenis_alergi
						LEFT JOIN mppa MP ON MP.id=H.created_by
						WHERE H.idpasien='$idpasien' AND H.staktif='1' AND H.assesmen_id!='$assesmen_id'
						ORDER BY H.id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('jenis_alergi','input_detail_alergi','input_reaksi_alergi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->jenis_alergi);
          $result[] = $r->input_detail_alergi;
          $result[] = $r->input_reaksi_alergi;
          $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
          // $aksi = '<div class="btn-group">';
		  // // if (UserAccesForm($user_acces_form,array('1607'))){
		  // $aksi .= '<button onclick="edit_alergi('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // // }
		   // // if (UserAccesForm($user_acces_form,array('1608'))){
		  // $aksi .= '<button onclick="hapus_alergi('.$r->id.')" type="button" title="Hapus Suhu" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // // }
		  // $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_alergi(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $id=$this->input->post('id');
	  $data['deleted_by']=$login_ppa_id;
	  $data['deleted_date']=date('Y-m-d H:i:s');
	  $data['staktif']=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_assesmen_alergi',$data);
	  echo json_encode($hasil);
  }
  function edit_alergi(){
	  $id=$this->input->post('id');
	  $q="SELECT * FROM `tpoliklinik_assesmen_alergi` H WHERE H.id='$id'";
	  $output=$this->db->query($q)->row_array();
	  echo json_encode($output);
  }
}