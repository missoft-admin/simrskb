<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tpiutang_tt_monitoring extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpiutang_tt_monitoring_model','model');
	}

	function index() {
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
		$data=array(
			'tipe'=>'#',
			'cara_bayar'=>'#',
			'no_po'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'#',
			'jatuh_tempo_bayar'=>'',
			'jatuh_tempo_bayar2'=>'',
			'tgl_kirim'=>'',
			'tgl_kirim2'=>'',
			// 'tgl_kirim'=>$tgl_pertama,
			// 'tgl_kirim2'=>date('d-m-Y'),
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['list_bank'] 	= $this->model->list_bank();
		$data['title'] 			= 'Monitoring Tagihan';
		$data['content'] 		= 'Tpiutang_tt_monitoring/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Monitoring Tagihan",'tklaim_monitoring/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$no_tagihan=$this->input->post('no_tagihan');
		$jatuh_tempo_bayar2=$this->input->post('jatuh_tempo_bayar2');
		$jatuh_tempo_bayar=$this->input->post('jatuh_tempo_bayar');
		$where1='';
		$where2='';
		$where='';
		
		if ($no_tagihan !=''){
			$where1 .=" AND CONCAT('PTT-',DATE_FORMAT(H.tanggal_jatuh_tempo,'%Y%m%d')) LIKE '%".$no_tagihan."%' ";
		}
		
		
		if ('' != $jatuh_tempo_bayar) {
            $where1 .= " AND DATE(H.tanggal_jatuh_tempo) >='".YMDFormat($jatuh_tempo_bayar)."' AND DATE(H.tanggal_jatuh_tempo) <='".YMDFormat($jatuh_tempo_bayar2)."'";
        }
		
        $from = "(
					SELECT CONCAT('PTT-',DATE_FORMAT(H.tanggal_jatuh_tempo,'%Y%m%d')) as no_tagihan,COUNT(H.id) as jml_faktur,SUM(H.nominal_tagihan) as tot_tagihan
					,SUM(H.nominal_tagihan-H.nominal_bayar-H.nominal_dihapus) as tot_sisa,H.tanggal_jatuh_tempo,SUM(CASE WHEN H.st_lunas='1' THEN 1 ELSE 0 END) as jml_lunas 
					FROM `tpiutang_tt_verifikasi` H
					WHERE H.`status`='1' ".$where1."
					GROUP BY H.tanggal_jatuh_tempo
				) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tpiutang_tt_monitoring/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->tanggal_jatuh_tempo;
            $row[] = $no;
            $row[] = $r->no_tagihan;
            $row[] = number_format($r->jml_faktur,0);
            $row[] = number_format($r->tot_tagihan,2);
            $row[] = number_format($r->tot_sisa,2);
            $row[] = HumanDateShort($r->tanggal_jatuh_tempo);
			if ($r->jml_lunas=='0'){
				$row[] = text_danger('BELUM ADA PEMBAYARAN');
			}else{			
				if ($r->jml_lunas==$r->jml_faktur ){
					$row[] = text_success('SELESAI');					
				}else{					
					$row[] = text_primary($r->jml_lunas.'/'.$r->jml_faktur.' TELAH DIBAYAR');
				}
			}

			$aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_tagihan/'.$r->tanggal_jatuh_tempo.'"  class="btn btn-xs btn-primary"><i class="fa fa-th-list"></i></a>';
			if ($r->jml_lunas=='0'){				
				$aksi .= '<button title="Edit Pembayaran & Jatuh Tempo"  class="btn btn-xs btn-warning ganti"><i class="fa fa-calendar"></i></button>';
			}
			$aksi .= '<button disabled class="view btn btn-xs btn-success" type="button"  title="Detail"><i class="fa fa-print"></i></button>';
			
			$aksi.='</div>';
            $row[] = $aksi;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(true),
            "recordsFiltered" => $this->datatable->count_all(true),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_rincian(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		
		$disabel=$this->input->post('disabel');
		$tanggal_jatuh_tempo=YMDFormat($this->input->post('tanggal_jatuh_tempo'));
		$no_reg=$this->input->post('no_reg');
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$tgl_trx=($this->input->post('tgl_trx'));
		$tgl_trx2=($this->input->post('tgl_trx2'));
		
		$where1='';
		
		if ($no_medrec !=''){
			$where1 .=" AND H.no_medrec='$no_medrec' ";
		}
		if ($nama_pasien !=''){
			$where1 .=" AND H.namapasien LIKE '%".$nama_pasien."%'";
		}
		if ($no_reg !=''){
			$where1 .=" AND H.nopendaftaran='$no_reg' ";
		}
				
		if ($tgl_trx!='') {
            $where1 .= " AND DATE(H.tanggal_kunjungan) >='".YMDFormat($tgl_trx)."' AND DATE(H.tanggal_kunjungan) <='".YMDFormat($tgl_trx2)."'";
        }
		
        $from = "(
					SELECT H.*,K.nama as kel_pasien,'1' as st_verifikasi_piutang from tpiutang_tt_verifikasi H
					LEFT JOIN mpasien_kelompok K ON K.id=H.idkelompokpasien
					WHERE H.tanggal_jatuh_tempo='$tanggal_jatuh_tempo' ".$where1."
				) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


         foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tkontrabon_verifikasi/');
            $url_kasir        = site_url('tkasir/');
            $url_ranap        = site_url('trawatinap_tindakan/');
            $url_batal       = site_url('tgudang_pengembalian/');
            $url_klaim       = site_url('tklaim_rincian/');
            $url_dok_rajal       = site_url('tverifikasi_transaksi/upload_document/tkasir/');
            $url_dok_ranap       = site_url('tverifikasi_transaksi/upload_document/trawatinap_tindakan_pembayaran/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
			// if ($r->no_klaim){
				// $no_klaim='<br><br><span class="label label-warning">'.$r->no_klaim.'</span>';			
			// }else{
				// $no_klaim='';				
			// }
            $row[] = $r->id;
            $row[] = $r->pembayaran_id;
            $row[] = $r->pendaftaran_id;
            $row[] = $r->idtipe;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggal_kunjungan).'<br>'.$r->nopendaftaran;
            $row[] = HumanDateShort($r->tanggal_transaksi);
            $row[] = GetTipePasienPiutang($r->idtipe);
            // $row[] = $r->nopendaftaran;
            $row[] = $r->no_medrec;
            $row[] = $r->no_medrec.'<br>'.($r->title?$r->title.'. ':'').$r->namapasien;
            $row[] = $r->kel_pasien;
            $row[] = number_format($r->total_transaksi,2);
            $row[] = number_format($r->tidak_tertagih,2);
            $row[] = number_format($r->nominal_lain,2);
            $row[] = $r->jaminan;
			$row[] = ($r->st_verifikasi_piutang=='1'?text_success('TELAH DIVERIFIKASI'):text_default('MENUGGU DIPROSES'));
			$row[] = HumanDateShort(($r->st_verifikasi_piutang?$r->tanggal_jatuh_tempo:$r->tgl_setting));
			if ($r->status=='1'){
				if (($r->nominal_bayar + $r->nominal_dihapus)=='0'){
				$aksi .= '<button title="Jatuh Tempo"  class="btn btn-xs btn-warning ganti"><i class="fa fa-calendar"></i></button>';
				}
				if ($r->cara_bayar=='1'){
					if (($r->nominal_bayar + $r->nominal_dihapus)=='0'){
						$aksi .= '<button title="Edit Pembayaran"  class="btn btn-xs btn-danger cara_tagihan"><i class="si si-arrow-right"></i></button>';
						
					}
				}
				$aksi .= '<button title="Follow Up"  class="btn btn-xs btn-info followup"><i class="fa fa-pencil"></i></button>';
				if ($r->idtipe < 3){
					$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'print_transaksi/'.$r->pendaftaran_id.'/'.$r->kasir_id.'" target="_blank"  type="button"  title="Rincian"><i class="fa fa-print"></i></a>';
				}else{
					$aksi .= '<div class="btn-group" role="group">
							<button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-print"></i>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right">
								<li class="dropdown-header">Print Ranap</li>
								<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->pendaftaran_id.'/0" target="_blank"  > Rincian Biaya (All)</a></li>
								<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->pendaftaran_id.'/1" target="_blank"  > Rincian Biaya (Verified)</a></li>
								<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->pendaftaran_id.'/0" target="_blank"  > Rincian Global (All)</a></li>
								<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->pendaftaran_id.'/1" target="_blank"  > Rincian Global (Verified)</a></li>
								
							</ul>
						</div>';
				}
				
			}else{
				$aksi .= '<button title="Jatuh Tempo"  class="btn btn-xs btn-danger">SUDAH DIGANTI MENJADI CICILAN</button>';
			}

			$aksi.='</div>';
            
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function rincian_tagihan($tanggal_jatuh_tempo,$disabel=''){
		
		$data=$this->model->detail($tanggal_jatuh_tempo);
		// print_r($data);exit();
		$data['grandtotalnominal'] 				= '0';
		$data['totalnominal'] 				= '0';
		$data['tgl_trx'] 				= '';
		$data['tgl_trx2'] 				= '';
		$data['st_kbo'] 				= '';
		if ($disabel!='0'){
			$data['disabel'] 				= 'disabled';
			
		}else{
			$data['disabel'] 				= '';
		}
		$data['error'] 				= '';
		$data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		
			$data['title'] 				= 'Rincian Detail Piutang ';
			$data['content'] 			= 'Tpiutang_tt_monitoring/detail_rincian';
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'), 
										array("Piutang Rincian",'tpiutang_tt_monitoring/index'), 
										array("List",'#') 
									);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		// print_r($id);
	}
	function edit_tanggal_satu(){
		
		
		$id=$this->input->post('id');
		$alasan=$this->input->post('alasan');
		$tanggal_jatuh_tempo=YMDFormat($this->input->post('tanggal_jatuh_tempo'));
		$tgl_asal=YMDFormat($this->input->post('tgl_asal'));
		

		$data_koreksi=array(
			'user_edit' =>$this->session->userdata('user_name'),
			'tanggal_edit' =>date('Y-m-d H:i:s'),
			'tanggal_jatuh_tempo' =>$tanggal_jatuh_tempo,
			'alasan' =>$alasan,
			'tgl_asal' =>$tgl_asal,
		);
		$this->db->where('id', $id);
		$result=$this->db->update('tpiutang_tt_verifikasi', $data_koreksi);	
		
		$this->output->set_output(json_encode($result));
	}
	function edit_tanggal_all(){
		
		
		$id=$this->input->post('id');
		$alasan=$this->input->post('alasan');
		$tanggal_jatuh_tempo=YMDFormat($this->input->post('tanggal_jatuh_tempo'));
		$tgl_asal=YMDFormat($this->input->post('tgl_asal'));
		

		$data_koreksi=array(
			'user_edit' =>$this->session->userdata('user_name'),
			'tanggal_edit' =>date('Y-m-d H:i:s'),
			'tanggal_jatuh_tempo' =>$tanggal_jatuh_tempo,
			'alasan' =>$alasan,
			'tgl_asal' =>$tgl_asal,
		);
		$this->db->where('tanggal_jatuh_tempo', $tgl_asal);
		$result=$this->db->update('tpiutang_tt_verifikasi', $data_koreksi);	
		
		$this->output->set_output(json_encode($result));
	}
	
	
	
	function load_informasi(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$piutang_id=$this->input->post('piutang_id');
		
        $from = "(
					select *from tpiutang_tt_fu where piutang_id='$piutang_id' ORDER BY id DESC
				) as tbl ";
				
		
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('keterangan');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $row[] = $r->id;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggal_info);
            $row[] = $r->keterangan;
            $row[] = $r->created_user;
            $row[] = HumanDateShort($r->tanggal_reminder);
			$aksi .= '<button title="Hapus" class="btn btn-xs btn-danger fu_hapus"><i class="fa fa-trash"></i></button>';
           
			
			$aksi.='</div>';
            $row[] = $aksi;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_upload(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$piutang_id=$this->input->post('piutang_id');
		
        $from = "(
					select *from tpiutang_tt_upload where piutang_id='$piutang_id' ORDER BY id DESC
				) as tbl ";
				
		
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('file_name');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $row[] = $r->id;
            $row[] = $no;
            $row[] = '<a href="'.base_url().'assets/upload/piutang/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a>';
            $row[] = $r->size;
			$aksi .= '<button title="Hapus" class="btn btn-xs btn-danger hapus_file"><i class="fa fa-trash"></i></button>';     
			
			$aksi.='</div>';
            $row[] = $aksi;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	
	function tambah_fu(){
		
		$piutang_id=$this->input->post('piutang_id');
		$tanggal_info=YMDFormat($this->input->post('tanggal_info'));
		$tanggal_reminder=YMDFormat($this->input->post('tanggal_reminder'));
		$keterangan=$this->input->post('keterangan');
		$data=array(
			'piutang_id' =>$piutang_id,
			'tanggal_info' =>$tanggal_info,
			'tanggal_reminder' =>$tanggal_reminder,
			'keterangan' =>$keterangan,			
			'created_date' =>date('Y-m-d H:i:s'),
			'created_user' =>$this->session->userdata('user_name'),
			'created_user_id' =>$this->session->userdata('user_ide'),
		);
		// $this->db->where('id', $id);
		$result=$this->db->insert('tpiutang_tt_fu', $data);		
		$this->output->set_output(json_encode($result));
	}
	public function upload_bukti() {
      $uploadDir = './assets/upload/piutang';
		if (!empty($_FILES)) {
			 $piutang_id = $this->input->post('upload_piutang_id');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['piutang_id'] 	= $piutang_id;
			$detail['file_name']		= $file_name;
			$detail['size']		= formatSizeUnits($size);
			$detail['tanggal_upload']	= date('Y-m-d H:i:s');
			$detail['user_upload']	= $this->session->userdata('user_name');
			$this->db->insert('tpiutang_tt_upload', $detail);
		}
		
    }
	function hapus_file(){
		$id=$this->input->post('id');
		$row =$this->db->query("select file_name from tpiutang_tt_upload where id='$id'")->row();
		if(file_exists('./assets/upload/piutang/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/piutang/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from tpiutang_tt_upload WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	function hapus_informasi(){
		$id=$this->input->post('id');
		
		$result=$this->db->query("delete from tpiutang_tt_fu WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	function get_data_kirim($id,$tipe){
		// if ($tipe=='1' || $tipe=='3'){//RJ
			$arr['detail']=$this->db->query("SELECT *FROM tpiutang_tt_verifikasi WHERE id='$id'")->row_array();
			$arr['detail']['tanggal_jatuh_tempo2']=HumanDateShort($arr['detail']['tanggal_jatuh_tempo']);
		// }else{
			// $arr['detail']=$this->db->query("SELECT *FROM trawatinap_tindakan_pembayaran_detail WHERE id='$id'")->row_array();
		// }
		
		$this->output->set_output(json_encode($arr));
	}
	function simulasi(){
		
		$jml_cicilan=$this->input->post('jml');
		$tgl_start=YMDFormat($this->input->post('tgl'));
		
		
		$opsi='';
		
		// print_r('hasil');exit();
			for ($i=0;$i<$jml_cicilan;$i++){				
				$date=date_create($tgl_start);
				date_add($date,date_interval_create_from_date_string("$i month"));
				$tgl_tagihan = date_format($date,"d-m-Y");
				$opsi .='<option value="'. $tgl_tagihan.'" selected>'.'Ke-'.($i+1).' : '.$tgl_tagihan.'</option>';
			}
		
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
		// print_r($arr);exit();
	}
	function save_cicilan(){
		$idpembayaran=$this->input->post('id_detail');
		$cara_bayar=$this->input->post('cara_bayar');
		$jml_cicilan=RemoveComma($this->input->post('jml'));
		$nomial_per_bulan=RemoveComma($this->input->post('xperbulan'));
		// $xperbulan=RemoveComma($this->input->post('xperbulan'));
		$xsetiap_bulan=$this->input->post('xsetiap_bulan');
		$row=$this->db->query("SELECT *FROM tpiutang_tt_verifikasi WHERE id='$idpembayaran'")->row();
		$tgl_tagihan='';
		$tagihan_ke=1;
		foreach ($xsetiap_bulan as $val){
			$tanggal_jt =YMDFormat($val);
			$data_header=array(
				'pendaftaran_id' => $row->pendaftaran_id,
				'kasir_id' => $row->kasir_id,
				'pembayaran_id' => $row->pembayaran_id,
				'idtipe' => $row->idtipe,
				'nopendaftaran' => $row->nopendaftaran,
				'tanggal_kunjungan' => $row->tanggal_kunjungan,
				'tanggal_transaksi' => $row->tanggal_transaksi,
				'idpasien' => $row->idpasien,
				'no_medrec' => $row->no_medrec,
				'title' => $row->title,
				'namapasien' => $row->namapasien,
				'idkelompokpasien' => $row->idkelompokpasien,
				'idrekanan' => $row->idrekanan,
				'total_transaksi' => $row->total_transaksi,
				'tidak_tertagih' => $row->tidak_tertagih,
				'nominal_lain' => $row->nominal_lain,
				'nominal_tagihan' => $nomial_per_bulan,
				'nominal_bayar' => 0,
				'cara_bayar' => $cara_bayar,
				'jml_cicilan' => $jml_cicilan,
				'cicilan_ke' => $tagihan_ke,
				'jaminan' => $row->jaminan,
				'status' => 1,
				'st_lunas' => 0,
				'status_dihapus' => 0,
				'nominal_dihapus' => 0,
				'tanggal_jatuh_tempo' => $tanggal_jt,
				'alasan' => $row->alasan,
				'tgl_asal' => $row->tgl_asal,
				'created_by' => $row->created_by,
				'created_nama' => $row->created_nama,
				'created_date' => $row->created_date,
				'user_edit' => $row->user_edit,
				'tanggal_edit' => $row->tanggal_edit,
				'tanggal_reminder' => $row->tanggal_reminder,
				'last_fu' => $row->last_fu,
				'user_fu' => $row->user_fu,
				'tanggal_informasi' => $row->tanggal_informasi,
			);
			$this->db->insert('tpiutang_tt_verifikasi',$data_header);
			$tagihan_ke=$tagihan_ke+1;
				
		}
		$this->db->where('id',$idpembayaran);
		$result=$this->db->update('tpiutang_tt_verifikasi',array('status'=>0));
		$this->output->set_output(json_encode($result));
	}
	//BATAS HAPUS
	
}
