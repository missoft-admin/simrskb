<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdata_hutang extends CI_Controller {

	/**
	 * Pegawai controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mdata_hutang_model');
		$this->load->helper('path');
		
  }

	function index($idkategori='0',$idakun='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('2'))){
			$data = array();
			$data['nama'] 			= '#';
			$data['status'] 			= '#';
			$data['error'] 			= '';
			$data['title'] 			= 'Master Hutang';
			$data['content'] 		= 'Mdata_hutang/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master Hutang",'#'),
												  array("List",'mdata_hutang')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create(){
		
		$data = array(
			'id'            => '',
			'nama' 					=> '',
			'idakun' 	=> '#',
			'nominal_default' 	=> '0',
		);

		$data['error'] 			= '';
		$data['list_akun'] 			= $this->Mdata_hutang_model->list_akun();
		$data['title'] 			= 'Tambah Master Hutang';
		$data['content'] 		= 'Mdata_hutang/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Master Hutang",'#'),
								            array("Tambah",'mdata_hutang')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$data= $this->Mdata_hutang_model->getSpecified($id);
			$data['list_akun'] 			= $this->Mdata_hutang_model->list_akun();
			
			
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Master Hutang';
			$data['content']    = 'Mdata_hutang/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Master Hutang",'#'),
										array("Ubah",'mdata_hutang')
										);

			// $data['statusAvailableApoteker'] = $this->Mdata_hutang_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mdata_hutang');
		}
	}

	function delete($id){
		
		$result=$this->Mdata_hutang_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('mdata_hutang','location');
	}
	function aktifkan($id){
		
		$result=$this->Mdata_hutang_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){
	
		if($this->input->post('id') == '' ) {
			if($this->Mdata_hutang_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mdata_hutang','location');
			}
		} else {
			if($this->Mdata_hutang_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mdata_hutang','location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mdata_hutang/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Hutang';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Master Hutang",'#'),
							               array("Tambah",'mdata_hutang')
								           );
		}else{
			$data['title'] = 'Ubah Master Hutang';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Master Hutang",'#'),
							               array("Ubah",'mdata_hutang')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	function getIndex($idkategori='0',$idakun='0')
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// idakun:idakun,status:status,nama:nama
			$where='';
			$status = $this->input->post('status');
			$nama = $this->input->post('nama');
			
			if ($status !='#'){
				$where .=" AND M.status='$status'";
			}
			if ($nama !=''){
				$where .=" AND M.nama LIKE '%".$nama."%'";
			}
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama,M.idakun,M.nominal_default,A.noakun,A.namaakun,M.`status`,M.saldo from mdata_hutang M
						LEFT JOIN makun_nomor A ON A.id=M.idakun 
						WHERE M.id IS NOT NULL ".$where."
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama','nominal_default','namaakun','noakun');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $r->id;
          $result[] = $no;
          $result[] = $r->nama;
          $result[] = $r->noakun.' - '.$r->namaakun;
         $result[] = StatusBarang($r->status);
          // $result[] = GetKategoriPegawai($r->idkategori);
          $aksi = '<div class="btn-group">';
			if ($r->status=='1'){
				$aksi .= '<a href="'.site_url().'mdata_hutang/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<button title="Hapus" class="btn btn-danger btn-xs removeData"><i class="fa fa-trash-o"></i></button>';
			}else{
				$aksi .= '<button title="Aktifkan" class="btn btn-success btn-xs aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
			}
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
}
