<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrka_kegiatan extends CI_Controller {

	/**
	 * Create RKA controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mrka_kegiatan_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'List RKA';
		$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
		$data['content'] 		= 'Mrka_kegiatan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Create RKA",'#'),
									    			array("List",'mrka')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create($disabel=''){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'periode' 					=> date('Y'),
			'status' 				=> ''
		);

		$data['disabel'] 			= $disabel;
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Create RKA';
		$data['content'] 		= 'Mrka_kegiatan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Create RKA",'#'),
									    			array("Tambah",'mrka')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function add($id,$disabel=''){
		if($id != ''){
			$row = $this->Mrka_kegiatan_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'periode' 					=> $row->periode,
					'status' 				=> $row->status
				);
				// print_r($row);exit();
				$data['disabel'] 			= $disabel;
				$data['error'] 			= '';
				$data['title'] 			= 'Input Kegiatan';
				$data['content']	 	= 'Mrka_kegiatan/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Create RKA",'#'),
											    			array("Ubah",'mrka')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mrka','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mrka');
		}
	}
	function add_kegiatan($idrka,$idprespektif,$idprogram,$disabel=''){

		$data = array(
			'id' 						=> '',
			'idrka' 						=> $idrka,
			'idprespektif' 					=> $idprespektif,
			'idprogram' 					=> $idprogram,			
			'idkegiatan' 					=> '',			
			'disabel' 					=> $disabel,			
			'kpi' 					=> '',			
			'bobot' 					=> '',			
			'target' 					=> '',			
			'initiative' 					=> '',			
			'idunit' 					=> '#',			
			'idunit_kolab' 					=> '#',			
			'keterangan' 					=> '',			
			'total_anggaran' 					=> '',			
			'b1' 					=> '0',			
			'b2' 					=> '0',			
			'b3' 					=> '0',			
			'b4' 					=> '0',			
			'b5' 					=> '0',			
			'b6' 					=> '0',			
			'b7' 					=> '0',			
			'b8' 					=> '0',			
			'b9' 					=> '0',			
			'b10' 					=> '0',			
			'b11' 					=> '0',			
			'b12' 					=> '0',			
		);
		$data['nominal'][1]=$data['b1'];
		$data['nominal'][2]=$data['b2'];
		$data['nominal'][3]=$data['b3'];
		$data['nominal'][4]=$data['b4'];
		$data['nominal'][5]=$data['b5'];
		$data['nominal'][6]=$data['b6'];
		$data['nominal'][7]=$data['b7'];
		$data['nominal'][8]=$data['b8'];
		$data['nominal'][9]=$data['b9'];
		$data['nominal'][10]=$data['b10'];
		$data['nominal'][11]=$data['b11'];
		$data['nominal'][12]=$data['b12'];
		// $data['disabel'] 			= $disabel;
		$data['list_prespektif'] 	= $this->Mrka_kegiatan_model->list_prespektif($idrka);
		$data['list_program'] 	= $this->Mrka_kegiatan_model->list_program($idrka);
		$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
		$data['list_unit_kolab'] 		= $this->Mrka_kegiatan_model->list_unit_kolab();
		$data['list_kegiatan'] 		= $this->Mrka_kegiatan_model->list_kegiatan();
		$data['error'] 			= '';
		$data['title'] 			= 'Input Kegiatan';
		$data['content']	 	= 'Mrka_kegiatan/add_kegiatan';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Input Kegiatan",'#'),
													array("Add",'mrka_kegiatan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
			
	}
	function edit_add_kegiatan($id,$disabel=''){
		$data=$this->Mrka_kegiatan_model->get_edit_kegiatan($id);
		// print_r($data);exit();
		$data['nominal'][1]=$data['b1'];
		$data['nominal'][2]=$data['b2'];
		$data['nominal'][3]=$data['b3'];
		$data['nominal'][4]=$data['b4'];
		$data['nominal'][5]=$data['b5'];
		$data['nominal'][6]=$data['b6'];
		$data['nominal'][7]=$data['b7'];
		$data['nominal'][8]=$data['b8'];
		$data['nominal'][9]=$data['b9'];
		$data['nominal'][10]=$data['b10'];
		$data['nominal'][11]=$data['b11'];
		$data['nominal'][12]=$data['b12'];
		// print_r($data);exit();
		$data['list_prespektif'] 	= $this->Mrka_kegiatan_model->list_prespektif($data['idrka']);
		$data['list_program'] 	= $this->Mrka_kegiatan_model->list_program($data['idrka']);
		$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
		$data['list_unit_kolab'] 		= $this->Mrka_kegiatan_model->list_unit_kolab($id);
		$data['list_kegiatan'] 		= $this->Mrka_kegiatan_model->list_kegiatan();
		$data['disabel'] 			= $disabel;
		$data['error'] 			= '';
		$data['title'] 			= 'Input Kegiatan';
		$data['content']	 	= 'Mrka_kegiatan/add_kegiatan';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Input Kegiatan",'#'),
													array("Add",'mrka_kegiatan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
			
	}

	function delete($id){
		$this->Mrka_kegiatan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mrka','location');
	}
	function update_status($id,$status){
		$result=$this->Mrka_kegiatan_model->update_status($id,$status);
		echo json_encode($result);
	}
	function hapus_kegiatan($id){
		$result=$this->Mrka_kegiatan_model->update_status($id,0);
		echo json_encode($result);
	}

	
	function save_kegiatan(){
		// print_r($this->input->post());exit();

			if($this->input->post('id') == '' ) {
				$id=$this->Mrka_kegiatan_model->save_kegiatan();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mrka_kegiatan/add/'.$this->input->post('idrka'),'location');
				}
			} else {
				if($this->Mrka_kegiatan_model->update_kegiatan()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mrka_kegiatan/add/'.$this->input->post('idrka'),'location');
				}
			}
	}
	
	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mrka_kegiatan/manage';

		if($id==''){
			$data['title'] = 'Tambah Create RKA';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Create RKA",'#'),
															array("Tambah",'mrka')
													);
		}else{
			$data['title'] = 'Ubah Create RKA';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Create RKA",'#'),
															array("Ubah",'mrka')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  $periode=$this->input->post('periode');
	  $where='';
	  if ($periode !='#'){
		  $where .=" AND M.periode='$periode'";
	  }
		
	  $from="(SELECT * from mrka M
				WHERE M.status > 1 ".$where."
				) as tbl";
	// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

			$aksi = '<div class="btn-group">';
			if (UserAccesForm($user_acces_form,array('1385'))){
				$aksi .= '<a href="'.site_url().'mrka_kegiatan/add/'.$r->id.'/disabled" data-toggle="tooltip" title="Ubah" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>';

			}
			if ($r->status !='0'){
				if ($r->status =='2'){
					if (UserAccesForm($user_acces_form,array('1386'))){
						$aksi .= '<a href="'.site_url().'mrka_kegiatan/add/'.$r->id.'" data-toggle="tooltip" title="Tambah Kegiatan" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>';
					}
				}
					if (UserAccesForm($user_acces_form,array('1387'))){
				$aksi .= '<a href="#" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
					}
			}
			$aksi .= '</div>';

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = $r->periode;
          $row[] = status_rka($r->status);
          $row[] = $r->created_nama.' '.HumanDateLong($r->created_date);
          $row[] = $aksi;
          $row[] = $r->id;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_kegiatan()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  $idrka=$this->input->post('idrka');
	  $idprespektif=$this->input->post('idprespektif');
	  $idprogram=$this->input->post('idprogram');
	  $idkegiatan=$this->input->post('idkegiatan');
	  $disabel=$this->input->post('disabel');
	  $where='';
	  if ($idprespektif !='#'){
		  $where .=" AND MPG.idprespektif='$idprespektif'";
	  }
	  if ($idprogram !='#'){
		  $where .=" AND H.idprogram='$idprogram'";
	  }
	  if ($idkegiatan !='#'){
		  $where .=" AND MK.idkegiatan='$idkegiatan'";
	  }
		
	  $from="(
				SELECT mrka.`status` as status_rka,MK.id, mrka.periode,mrka.nama as rka_nama,H.idrka, MPG.idprespektif,H.idprogram 
				,MP.nama as prespektif_nama,MPG.nama as program_nama
				,MK.idkegiatan,K.nama as kegiatan_nama
				,U.nama as pic,MK.kpi,MK.bobot,MPG.kode
				,b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,UK.nama as unit_kol,MK.keterangan,MK.target,MK.initiative
				from mrka_program H
				LEFT JOIN mrka ON mrka.id=H.idrka
				LEFT JOIN mprogram_rka MPG ON H.idprogram=MPG.id
				LEFT JOIN mprespektif_rka MP ON MP.id=MPG.idprespektif
				LEFT JOIN mrka_kegiatan MK ON MK.idrka=H.idrka AND MK.idprespektif=H.idprespektif AND MK.idprogram=H.idprogram  AND MK.status='1'
				LEFT JOIN mkegiatan_rka K ON K.id=MK.idkegiatan 
				LEFT JOIN munitpelayanan U ON U.id=MK.idunit
				LEFT JOIN munitpelayanan UK ON UK.id=MK.idunit_kolab
				WHERE H.idrka='$idrka' AND H.`status`='1' ".$where." 
				ORDER BY MP.urutan ASC,MPG.urutan ASC
				) as tbl";
	// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('program_nama','pic');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $nama_presprektif='';
	  $nama_gabungan='';
      foreach ($list as $r) {
          $no++;
		  $idgabungan=$r->idprespektif.'-'.$r->idprogram;
		  if ($nama_presprektif !=$r->idprespektif){
			  $row = array();
			  $row[] = $r->idrka;
			  $row[] = $r->idprespektif;
			  $row[] = $r->idprogram;
			  $row[] = $r->idkegiatan;
			  $row[] = $r->idkegiatan;
			  $row[] = '<span class="label label-success">'.$r->prespektif_nama.'</span>';
			  $nama_presprektif=$r->idprespektif;
			  for ($i=1;$i<=22;$i++){
			  $row[] = '';
			  }
			  $data[] = $row;
		  }
		  if ($idgabungan != $nama_gabungan){
			  $row = array();
			  $row[] = $r->idrka;
			  $row[] = $r->idprespektif;
			  $row[] = $r->idprogram;
			  $row[] = $r->idkegiatan;
			  $row[] = $r->idkegiatan;
			  if ($r->status_rka=='2'){//Publish
				$button_add='<a '.$disabel.' href="'.site_url().'mrka_kegiatan/add_kegiatan/'.$r->idrka.'/'.$r->idprespektif.'/'.$r->idprogram.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i></a>';
				  
			  }else{
				  $button_add='';
			  }
			  $row[] = '<span class="label label-default">'.$r->program_nama.'</span> '.$button_add;
			  for ($i=1;$i<=22;$i++){
			  $row[] = '';
			  }
			  $nama_gabungan=$idgabungan;
			  $data[] = $row;
		  }
		  if ($r->idkegiatan){
		  $row = array();
		  $row[] = $r->idrka;//0
		  $row[] = $r->idprespektif;//1
		  $row[] = $r->idprogram;//2
		  $row[] = $r->idkegiatan;//3
		  $row[] = $r->id;//4
		  $row[] = $r->kegiatan_nama;//5
		  $row[] = $r->pic;
		  $row[] = $r->kode;
		  $row[] = $r->kpi;
		  $row[] = $r->target;
		  $row[] = number_format($r->bobot,0).'%';//11
		  $row[] = $r->initiative;//8
		  $sub=$r->b1 +$r->b2 +$r->b3 +$r->b4 +$r->b5 +$r->b6 +$r->b7 +$r->b8 +$r->b9 +$r->b10 +$r->b11 +$r->b12; 
		  $row[] = number_format($r->b1,0);//12
		  $row[] = number_format($r->b2,0);//13
		  $row[] = number_format($r->b3,0);//14
		  $row[] = number_format($r->b4,0);
		  $row[] = number_format($r->b5,0);
		  $row[] = number_format($r->b6,0);
		  $row[] = number_format($r->b7,0);
		  $row[] = number_format($r->b8,0);
		  $row[] = number_format($r->b9,0);
		  $row[] = number_format($r->b10,0);
		  $row[] = number_format($r->b11,0);
		  $row[] = number_format($r->b12,0);//23
		  $row[] = number_format($sub,0);//24
		  $row[] = $r->unit_kol;//25
		  $row[] = $r->keterangan;//26
		  $aksi = '<div class="btn-group">';
			if ($r->status_rka=='2'){
				$aksi .= '<a href="'.site_url().'mrka_kegiatan/edit_add_kegiatan/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<button title="Hapus" class="btn btn-danger btn-xs hapus_kegiatan"><i class="fa fa-trash"></i></button>';
			}
			$aksi .= '<button title="Direct Transaksi" class="btn btn-primary btn-xs "><i class="fa fa-upload"></i></button>';
			$aksi .= '<button title="Penilaian" class="btn btn-warning btn-xs "><i class="si si-docs"></i></button>';
		  $aksi .= '</div>';
		  $row[] = $aksi;//27
		  
		  $data[] = $row;
		  }
		 
		  // print_r($row);exit();
          
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function list_kegiatan(){
		
		$q="SELECT M.id,M.nama,M.urutan from mkegiatan_rka M
			WHERE M.`status`='1'
			ORDER BY M.urutan ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->urutan.'. '.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
}
