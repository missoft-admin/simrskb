<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdata_pengelolaan extends CI_Controller {

	/**
	 * Master Pengelolaan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mdata_pengelolaan_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array();
		$data['status'] 			= '#';
		$data['error'] 			= '';
		$data['title'] 			= 'Master Pengelolaan';
		$data['content'] 		= 'Mdata_pengelolaan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master Pengelolaan",'#'),
									    			array("List",'mdata_pengelolaan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'st_harga_jual'			=> '#',
			'status' 				=> '',
		);

		$data['list_akun'] 			= $this->Mdata_pengelolaan_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Pengelolaan';
		$data['content'] 		= 'Mdata_pengelolaan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master Pengelolaan",'#'),
									    			array("Tambah",'mdata_pengelolaan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mdata_pengelolaan_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'st_harga_jual' 		=> $row->st_harga_jual,
					'status' 				=> $row->status
				);
				$data['list_akun'] 			= $this->Mdata_pengelolaan_model->list_akun();
				$data['list_bank'] 			= $this->Mdata_pengelolaan_model->list_bank();
				$data['list_hutang'] 			= $this->Mdata_pengelolaan_model->list_hutang();
				$data['list_piutang'] 			= $this->Mdata_pengelolaan_model->list_piutang();
				// $data['list_jenis'] 			= $this->Mdata_pengelolaan_model->list_jenis();
				// $data['list_tanggal'] 			= $this->Mdata_pengelolaan_model->list_tanggal();
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Pengelolaan';
				$data['content']	 	= 'Mdata_pengelolaan/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Pengelolaan",'#'),
											    			array("Ubah",'mdata_pengelolaan')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mdata_pengelolaan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mdata_pengelolaan');
		}
	}
	function simpan_detail()
	{
		$id_edit=$this->input->post('id_edit');
		$data=array(
				'iduser'=>$this->input->post('iduser'),
				'step'=>$this->input->post('step'),
				'proses_setuju'=>$this->input->post('proses_setuju'),
				'proses_tolak'=>$this->input->post('proses_tolak'),
				'operand'=>$this->input->post('operand'),
				'idpengelolaan'=>$this->input->post('idpengelolaan'),
				'nominal'=>RemoveComma($this->input->post('nominal')),
				
				
			);
		// if ($data['operand']=='>='){
			// $data['nominal']=0;
		// }else{
			// $data['nominal']=RemoveComma($this->input->post('nominal'));
		// }
		if ($id_edit==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');			
			$result = $this->db->insert('mdata_pengelolaan_approval',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');		
			$this->db->where('id',$id_edit);
			$result = $this->db->update('mdata_pengelolaan_approval',$data);
		}
       
		
		$this->output->set_output(json_encode($result));
	}
	function load_detail()
    {
		
		$idpengelolaan     = $this->input->post('idpengelolaan');
		
		$iduser=$this->session->userdata('user_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT D.*,U.`name` as user_nama
				FROM `mdata_pengelolaan_approval` D
								LEFT JOIN musers U ON U.id=D.iduser
								WHERE  D.status='1' AND D.idpengelolaan='$idpengelolaan'
								ORDER BY D.step
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();            
            $row[] = $r->id;
            $row[] = $r->step;
            $row[] = (($r->operand=='>=' && $r->nominal=='0')?'BEBAS':$r->operand.' '.number_format($r->nominal,0));
            $aksi       = '<div class="btn-group">';	
			
			$aksi 		.= '<button  type="button" class="btn btn-xs btn-primary edit" title="Edit"><i class="fa fa-pencil"></i></button>';				
			$aksi 		.= '<button type="button" class="btn btn-xs btn-danger hapus_det" title="Hapus"><i class="fa fa-close"></i></button>';				
			
			$aksi.='</div>';			
            $row[] = $r->user_nama;
			$row[] = ($r->proses_setuju=='1'?'Langsung':'Menunggu User');
            $row[] = ($r->proses_tolak=='1'?'Langsung':'Menunggu User');
			$row[] = $aksi;			
            $row[] = $r->id;
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	function get_edit(){
		$id     = $this->input->post('id');
		$q="SELECT D.*
			,U.`name` as user_nama FROM `mdata_pengelolaan_approval` D
				LEFT JOIN musers U ON U.id=D.iduser
				WHERE D.id='$id'";
		$arr['detail'] =$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($arr['detail']));
	}
	function hapus_det()
	{
		$id=$this->input->post('id');
		$this->db->where('id',$this->input->post('id'));	
		$data=array(
			'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_date'=>date('Y-m-d H:i:s'),
		);
       // print_r($data);
	   
		$result = $this->db->update('mdata_pengelolaan_approval',$data);
		echo json_encode($result);
	}
	function list_user_approval($step,$idpengelolaan)
	{
		$arr['detail'] = $this->Mdata_pengelolaan_model->list_user_approval($step,$idpengelolaan);
		$this->output->set_output(json_encode($arr));
	}
	function setting($id){
		if($id != ''){
			$row = $this->Mdata_pengelolaan_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'st_harga_jual' 		=> $row->st_harga_jual,
					'status' 				=> $row->status
				);
				// print_r($data);exit();
				$data['list_akun'] 			= $this->Mdata_pengelolaan_model->list_akun();
				$data['list_bank'] 			= $this->Mdata_pengelolaan_model->list_bank();
				$data['list_hutang'] 			= $this->Mdata_pengelolaan_model->list_hutang();
				$data['list_piutang'] 			= $this->Mdata_pengelolaan_model->list_piutang();
				// $data['list_jenis'] 			= $this->Mdata_pengelolaan_model->list_jenis();
				// $data['list_tanggal'] 			= $this->Mdata_pengelolaan_model->list_tanggal();
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Pengelolaan';
				$data['content']	 	= 'Mdata_pengelolaan/manage_logic';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Pengelolaan",'#'),
											    			array("Ubah",'mdata_pengelolaan')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mdata_pengelolaan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mdata_pengelolaan');
		}
	}
	
	function delete($id){
		$this->Mdata_pengelolaan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mdata_pengelolaan','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		// print_r($this->input->post());exit();
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('idpengelolaan') == '' ) {
				$id=$this->Mdata_pengelolaan_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mdata_pengelolaan/update/'.$id,'location');
				}
			} else {
				if($this->Mdata_pengelolaan_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mdata_pengelolaan','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}
	
	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mdata_pengelolaan/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Pengelolaan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Pengelolaan",'#'),
															array("Tambah",'mdata_pengelolaan')
													);
		}else{
			$data['title'] = 'Ubah Master Pengelolaan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Pengelolaan",'#'),
															array("Ubah",'mdata_pengelolaan')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	function aktifkan($id){
		
		$result=$this->Mdata_pengelolaan_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	function softDelete($id){
		
		$result=$this->Mdata_pengelolaan_model->softDelete($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	function getIndex()
    {
		$where='';
			$status = $this->input->post('status');
			$nama = $this->input->post('nama');
			if ($status !='#'){
				$where .=" AND M.status='$status'";
			}
			if ($nama !=''){
				$where .=" AND M.nama LIKE '%".$nama."%'";
			}
		  $data_user=get_acces();
		  $user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.id,H.nama,GROUP_CONCAT(D.tanggal) as tanggal,H.`status`,H.st_harga_jual from mdata_pengelolaan H
LEFT JOIN mdata_pengelolaan_tanggal D ON D.idpengelolaan=H.id
						WHERE H.id IS NOT NULL ".$where."
GROUP BY H.id
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama');
      $this->column_order    = array('nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					if ($r->status=='1'){
					// if (UserAccesForm($user_acces_form,array('131'))){
						$aksi .= '<a href="'.site_url().'mdata_pengelolaan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>';
						$aksi .= '<a href="'.site_url().'mdata_pengelolaan/setting/'.$r->id.'" data-toggle="tooltip" title="Approval" class="btn btn-primary btn-xs"><i class="si si-settings"></i></a>';
					// }
					// if (UserAccesForm($user_acces_form,array('132'))){
						$aksi .= '<button title="Aktifkan" class="btn btn-danger btn-xs aktifData" onclick="softDelete('.$r->id.')"><i class="fa fa-trash-o"></i></a>';
					// }
					}else{
					   // if (UserAccesForm($user_acces_form,array('132'))){
						$aksi .= '<button title="Aktifkan" class="btn btn-success btn-xs aktifData" onclick="aktifkan('.$r->id.')"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
						// }
					}
					
			        $aksi .= '</div>';

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = ($r->st_harga_jual=='1'?'<span class="label label-primary">Sebelum Margin</span>':'<span class="label label-default">Setelah Margin</span>');
          $row[] = $r->tanggal;
          $row[] = ($r->status=='1'?'<span class="label label-primary">Active</span>':'<span class="label label-danger">Not Active</span>');
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function load_tanggal()
    {
	 $idpengelolaan=$this->input->post('idpengelolaan');
	$this->select = array();
	$this->from   = "(SELECT H.id,H.tanggal,H.deskripsi,H.edited_nama,H.edited_date FROM mdata_pengelolaan_tanggal H WHERE H.`status`='1' AND H.idpengelolaan='$idpengelolaan')as tbl";
	$this->join 	= array();
	$this->where  = array();
	$this->order  = array();
	$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
			$aksi = '<div class="btn-group">';					
			$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-success btn-xs edit_tanggal"><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-xs hapus_tanggal"><i class="fa fa-trash"></i></button>';
			$aksi .= '</div>';

          $row[] = $r->id;
          $row[] = $no;
          $row[] = $r->tanggal;
          $row[] = $r->deskripsi;
          $row[] = $r->edited_nama.'  '.HumanDateLong($r->edited_date);
          $row[] = $aksi;
		  $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
	
	function simpan_tanggal(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$tedit=$this->input->post('tedit_tanggal');
		$idpengelolaan=$this->input->post('idpengelolaan');
		$tanggal=$this->input->post('tanggal');
		$deskripsi=$this->input->post('deskripsi');
		$idpengelolaan=$this->input->post('idpengelolaan');
		if ($tedit==''){
			$data_det=array(
					'idpengelolaan'=>$idpengelolaan,
					'tanggal'=>$tanggal,
					'deskripsi'=>$deskripsi,
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
					
			$result=$this->db->insert('mdata_pengelolaan_tanggal',$data_det);
		}else{
			$data_det=array(
					'idpengelolaan'=>$idpengelolaan,
					'tanggal'=>$tanggal,
					'deskripsi'=>$deskripsi,
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->where('id',$tedit);
			$result=$this->db->update('mdata_pengelolaan_tanggal',$data_det);
		}
		$this->output->set_output(json_encode($result));
	}
	
	function hapus_tanggal(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$result=$this->db->delete('mdata_pengelolaan_tanggal');
		$this->output->set_output(json_encode($result));
	}
	//Rekening
	function load_rekening()
    {
	 $idpengelolaan=$this->input->post('idpengelolaan');
	$this->select = array();
	$this->from   = "(SELECT H.*,M.bank FROM mdata_pengelolaan_rekening H 
						LEFT JOIN ref_bank M ON M.id=H.idbank
						WHERE H.`status`='1' AND H.idpengelolaan='$idpengelolaan')as tbl";
	$this->join 	= array();
	$this->where  = array();
	$this->order  = array();
	$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
			$aksi = '<div class="btn-group">';					
			$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-success btn-xs edit_rekening"><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-xs hapus_rekening"><i class="fa fa-trash"></i></button>';
			$aksi .= '</div>';

          $row[] = $r->id;
          $row[] = $r->idbank;
          $row[] = $r->bank;
          $row[] = $r->norekening;
          $row[] = $r->atasnama;
          $row[] = ($r->st_primary=='1'?text_primary('Primary'):text_default('Secondary'));
          $row[] = $r->keterangan;
          $row[] = $aksi;
          $row[] = $r->st_primary;
		  $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
	
	function simpan_rekening(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$tedit=$this->input->post('tedit_rekening');
		$idpengelolaan=$this->input->post('idpengelolaan');
		$idbank=$this->input->post('idbank');
		$norekening=$this->input->post('norekening');
		$atasnama=$this->input->post('atasnama');
		$st_primary=$this->input->post('st_primary');
		$keterangan=$this->input->post('keterangan');
		if ($tedit==''){
			$data_det=array(
					'idpengelolaan'=>$idpengelolaan,
					'idbank'=>$idbank,
					'norekening'=>$norekening,
					'atasnama'=>$atasnama,
					'st_primary'=>$st_primary,
					'keterangan'=>$keterangan,
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
					
			$result=$this->db->insert('mdata_pengelolaan_rekening',$data_det);
		}else{
			$data_det=array(
					'idpengelolaan'=>$idpengelolaan,
					'idbank'=>$idbank,
					'norekening'=>$norekening,
					'atasnama'=>$atasnama,
					'st_primary'=>$st_primary,
					'keterangan'=>$keterangan,
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->where('id',$tedit);
			$result=$this->db->update('mdata_pengelolaan_rekening',$data_det);
		}
		$this->output->set_output(json_encode($result));
	}
	
	function hapus_rekening(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$result=$this->db->delete('mdata_pengelolaan_rekening');
		$this->output->set_output(json_encode($result));
	}
	
	//Hutang
	function load_hutang()
    {
	 $idpengelolaan=$this->input->post('idpengelolaan');
	$this->select = array();
	$this->from   = "(SELECT H.id,MH.nama,H.st_rutin,CONCAT(A.noakun,' - ',A.namaakun) as akun,H.idakun,H.mdata_hutang_id 
					,H.nominal_default 
					FROM `mdata_pengelolaan_hutang` H
					LEFT JOIN mdata_hutang MH ON MH.id=H.mdata_hutang_id
					LEFT JOIN makun_nomor A ON A.id=H.idakun
					WHERE H.idpengelolaan='$idpengelolaan')as tbl";
	$this->join 	= array();
	$this->where  = array();
	$this->order  = array();
	$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
			$aksi = '<div class="btn-group">';					
			$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-success btn-xs edit_hutang"><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-xs hapus_hutang"><i class="fa fa-trash"></i></button>';
			$aksi .= '</div>';

          $row[] = $r->id;
          $row[] = $r->mdata_hutang_id;
          $row[] = $r->idakun;
          $row[] = $r->st_rutin;
          $row[] = $r->nama;
          $row[] = ($r->st_rutin=='1'?text_primary('Rutin'):text_default('Tidak Rutin'));
          $row[] = number_format($r->nominal_default,0);
          $row[] = $r->akun;
          $row[] = $aksi;
		  $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
	
	function simpan_hutang(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$tedit=$this->input->post('tedit_hutang');
		$idpengelolaan=$this->input->post('idpengelolaan');
		$idakun=$this->input->post('idakun');
		$st_rutin=$this->input->post('st_rutin');
		$nominal_default=$this->input->post('nominal_default');
		$mdata_hutang_id=$this->input->post('mdata_hutang_id');
		if ($tedit==''){
			$data_det=array(
					'idpengelolaan'=>$idpengelolaan,
					'idakun'=>$idakun,
					'mdata_hutang_id'=>$mdata_hutang_id,
					'st_rutin'=>$st_rutin,
					'nominal_default'=>$nominal_default,
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
					
			$result=$this->db->insert('mdata_pengelolaan_hutang',$data_det);
		}else{
			$data_det=array(
					'idpengelolaan'=>$idpengelolaan,
					'idakun'=>$idakun,
					'mdata_hutang_id'=>$mdata_hutang_id,
					'st_rutin'=>$st_rutin,
					'nominal_default'=>$nominal_default,
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->where('id',$tedit);
			$result=$this->db->update('mdata_pengelolaan_hutang',$data_det);
		}
		$this->output->set_output(json_encode($result));
	}
	
	function get_data_hutang($id){
		$q="SELECT *FROM mdata_hutang H WHERE H.id='$id'";
		$data['detail']=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($data));
	}
	function hapus_hutang(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$result=$this->db->delete('mdata_pengelolaan_hutang');
		$this->output->set_output(json_encode($result));
	}
	//Hutang
	function load_piutang()
    {
	 $idpengelolaan=$this->input->post('idpengelolaan');
	$this->select = array();
	$this->from   = "(SELECT H.id,MH.nama,H.st_rutin,CONCAT(A.noakun,' - ',A.namaakun) as akun,H.idakun,H.mdata_piutang_id 
					,H.nominal_default 
					FROM `mdata_pengelolaan_piutang` H
					LEFT JOIN mdata_piutang MH ON MH.id=H.mdata_piutang_id
					LEFT JOIN makun_nomor A ON A.id=H.idakun
					WHERE H.idpengelolaan='$idpengelolaan')as tbl";
	$this->join 	= array();
	$this->where  = array();
	$this->order  = array();
	$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
			$aksi = '<div class="btn-group">';					
			$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-success btn-xs edit_piutang"><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-xs hapus_piutang"><i class="fa fa-trash"></i></button>';
			$aksi .= '</div>';

          $row[] = $r->id;
          $row[] = $r->mdata_piutang_id;
          $row[] = $r->idakun;
          $row[] = $r->st_rutin;
          $row[] = $r->nama;
          $row[] = ($r->st_rutin=='1'?text_primary('Rutin'):text_default('Tidak Rutin'));
          $row[] = number_format($r->nominal_default,0);
          $row[] = $r->akun;
          $row[] = $aksi;
		  $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
	
	function simpan_piutang(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$tedit=$this->input->post('tedit_piutang');
		$idpengelolaan=$this->input->post('idpengelolaan');
		$idakun=$this->input->post('idakun');
		$st_rutin=$this->input->post('st_rutin');
		$nominal_default=$this->input->post('nominal_default');
		$mdata_piutang_id=$this->input->post('mdata_piutang_id');
		if ($tedit==''){
			$data_det=array(
					'idpengelolaan'=>$idpengelolaan,
					'idakun'=>$idakun,
					'mdata_piutang_id'=>$mdata_piutang_id,
					'st_rutin'=>$st_rutin,
					'nominal_default'=>$nominal_default,
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
					
			$result=$this->db->insert('mdata_pengelolaan_piutang',$data_det);
		}else{
			$data_det=array(
					'idpengelolaan'=>$idpengelolaan,
					'idakun'=>$idakun,
					'mdata_piutang_id'=>$mdata_piutang_id,
					'st_rutin'=>$st_rutin,
					'nominal_default'=>$nominal_default,
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->where('id',$tedit);
			$result=$this->db->update('mdata_pengelolaan_piutang',$data_det);
		}
		$this->output->set_output(json_encode($result));
	}
	
	function get_data_piutang($id){
		$q="SELECT *FROM mdata_piutang H WHERE H.id='$id'";
		$data['detail']=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($data));
	}
	function hapus_piutang(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$result=$this->db->delete('mdata_pengelolaan_piutang');
		$this->output->set_output(json_encode($result));
	}
	
}
