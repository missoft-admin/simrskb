<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tsetoran_rj extends CI_Controller {

	/**
	 * Setoran Kas controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tsetoran_rj_model');
		$this->load->model('Mrka_bendahara_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array(
			'tanggal_setoran2'=>'',
			'tanggal_setoran1'=>'',
			'tanggal_trx1'=>'',
			'tanggal_trx2'=>'',
			'status'=>'#',
			'notransaksi'=>'',
		);
		$this->update_detail(date('Y-m-d'));
		// $data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
		// $data['list_jenis'] 		= $this->Tsetoran_rj_model->list_jenis_all();
		$data['error'] 			= '';
		$data['title'] 			= 'Setoran Kas Rawat Jalan';
		$data['content'] 		= 'Tsetoran_rj/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("SETORAN KAS",'#'),
									    			array("List",'Tsetoran_rj')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function update_detail($tanggal){
		$q="
			SELECT H.tanggal_trx as tanggal,H.id as idtransaksi,D.id,T.user_id,MU.`name` as nama_user
			,SUM(T.tunai) as total_tunai
			,SUM(T.debit+T.kredit+T.tf+T.kontraktor) as total_non_tunai
			,SUM(T.debit) as total_debit
			,SUM(T.kredit) as total_kredit
			,SUM(T.tf) as total_tf
			,SUM(T.kontraktor) as total_kontraktor
			,T.total_trx as jumlah_trx
			,SUM(T.pengeluaran) as total_pengeluaran,(T.idbayar_id)
			FROM (
							SELECT 
										K.edited_by as user_id
										,SUM(CASE WHEN KB.idmetode='1' THEN KB.nominal ELSE 0 END)-SUM(K.kembalian) as tunai
										,SUM(CASE WHEN KB.idmetode='2' THEN KB.nominal ELSE 0 END) as debit
										,SUM(CASE WHEN KB.idmetode='3' THEN KB.nominal ELSE 0 END) as kredit
										,SUM(CASE WHEN KB.idmetode='4' THEN KB.nominal ELSE 0 END) as tf
										,SUM(CASE WHEN KB.idmetode='5' THEN KB.nominal ELSE 0 END) as karyawan
										,SUM(CASE WHEN KB.idmetode='6' THEN KB.nominal ELSE 0 END) as tidak_tertagih
										,SUM(CASE WHEN KB.idmetode='7' THEN KB.nominal ELSE 0 END) as kontraktor
										,SUM(1) as total_trx,0 as pengeluaran,GROUP_CONCAT(KB.id) as idbayar_id
										FROM tkasir K
										LEFT JOIN tkasir_pembayaran KB ON KB.idkasir=K.id
										WHERE K.`status`='2' AND DATE(K.tanggal)='$tanggal' AND K.total > 0
										GROUP BY  DATE(K.tanggal),K.edited_by		
							UNION ALL
							SELECT 
							H.created_by as user_id
							,SUM(CASE WHEN KB.idmetode='1' THEN KB.nominal ELSE 0 END) as tunai
										,SUM(CASE WHEN KB.idmetode='2' THEN KB.nominal ELSE 0 END) as debit
										,SUM(CASE WHEN KB.idmetode='3' THEN KB.nominal ELSE 0 END) as kredit
										,SUM(CASE WHEN KB.idmetode='4' THEN KB.nominal ELSE 0 END) as tf
										,SUM(CASE WHEN KB.idmetode='5' THEN KB.nominal ELSE 0 END) as karyawan
										,SUM(CASE WHEN KB.idmetode='6' THEN KB.nominal ELSE 0 END) as tidak_tertagih
										,SUM(CASE WHEN KB.idmetode='7' THEN KB.nominal ELSE 0 END) as kontraktor
										,SUM(1) as total_trx,0 as pengeluaran,GROUP_CONCAT(KB.id) as idbayar_id
							FROM trm_pengajuan_informasi H
							LEFT JOIN trm_pembayaran_informasi_detail KB ON KB.idpengajuan=H.id
							WHERE H.tanggal='$tanggal' AND H.status_pembayaran='2'
							GROUP BY H.tanggal,H.created_by
							
							UNION ALL
							
							SELECT P.user_id,0 as tunai,0 as debit,0 as kredit,0 as tf,0 as karyawan,0 as tidak_tertagih,0 as kontraktor,SUM(1) as total_trx,SUM(P.totalrefund) as pengeluaran,GROUP_CONCAT(idbayar_id) FROM (
								SELECT DATE(T.tanggal) as tanggal, T.created_by as user_id,T.totalrefund,T.id as idbayar_id
								 FROM trefund T
								 LEFT JOIN mfpasien M ON M.id=T.idpasien
								WHERE T.`status`='1' AND T.tipe='2' AND T.tipetransaksi='rawatjalan' AND DATE(T.tanggal)='$tanggal' AND T.metode='1'

								UNION ALL

								SELECT DATE(T.tanggal) as tanggal,T.created_by as user_id,T.totalrefund,T.id as idbayar_id
								 FROM trefund T
								 LEFT JOIN mfpasien M ON M.id=T.idpasien
								WHERE T.`status`='1' AND T.tipe='1' AND DATE(T.tanggal)='$tanggal' AND T.metode='1'
							) P
							GROUP BY P.tanggal,P.user_id
			) T
			LEFT JOIN musers MU ON MU.id=T.user_id
			LEFT JOIN tsetoran_rj H ON H.tanggal_trx='$tanggal'
			LEFT JOIN tsetoran_rj_detail D ON D.idtransaksi=H.id AND D.user_id=T.user_id
			GROUP BY T.user_id
		";
		$list_data=$this->db->query($q)->result();
		foreach($list_data as $r){
			$data=array(
				'idtransaksi' => $r->idtransaksi,
				'tanggal' => $r->tanggal,
				'user_id' => $r->user_id,
				'jumlah_trx' => $r->jumlah_trx,
				'total_trx' => $r->total_non_tunai + $r->total_tunai,
				'total_non_tunai' => $r->total_non_tunai,
				'total_debit' => $r->total_debit,
				'total_kredit' => $r->total_kredit,
				'total_tf' => $r->total_tf,
				'total_kontraktor' => $r->total_kontraktor,
				'total_tunai' => $r->total_tunai,
				'idbayar_id' => $r->idbayar_id,
				'total_pengeluaran' => $r->total_pengeluaran,
				'final_total' => $r->total_tunai-$r->total_pengeluaran,
			);
			if ($r->id){//Update
				$this->db->where('id',$r->id);
				$this->db->update('tsetoran_rj_detail',$data);
			}else{//INSERT
				$this->db->insert('tsetoran_rj_detail',$data);
			}
		}
		
	}
	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	
	  $notransaksi=$this->input->post('notransaksi');
	  $tanggal_setoran2=$this->input->post('tanggal_setoran2');
	  $tanggal_setoran1=$this->input->post('tanggal_setoran1');
	  $tanggal_trx1=$this->input->post('tanggal_trx1');
	  $tanggal_trx2=$this->input->post('tanggal_trx2');
	  $status=$this->input->post('status');
	 
	  $iduser=$this->session->userdata('user_id');
	  $where='';
	  if ($notransaksi !=''){
		  $where .=" AND H.notransaksi='$notransaksi'";
	  }
	  if ($status !='#'){
		  if ($status=='1'){
			  $where .=" AND H.st_verifikasi='1'";
		  }
		  if ($status=='2'){
			  $where .=" AND H.st_verifikasi='0'";
		  }
		  if ($status=='3'){
			  $where .=" AND H.status='0'";
		  }
		 
	  }
	   if ($tanggal_trx1 !='' || $tanggal_trx2 !=''){
		  $where .=" AND (H.tanggal_trx >='".YMDFormat($tanggal_trx1)."' AND H.tanggal_trx <='".YMDFormat($tanggal_trx2)."' )";
	  }
	  if ($tanggal_setoran1 !='' || $tanggal_setoran2 !=''){
		  $where .=" AND (H.tanggal_setoran >='".YMDFormat($tanggal_setoran1)."' AND H.tanggal_setoran <='".YMDFormat($tanggal_setoran2)."' )";
	  }
	 
	  $from="(
				SELECT
					T.id as idvalidasi,H.*
				FROM tsetoran_rj H
				LEFT JOIN tvalidasi_pendapatan_rajal T ON H.tanggal_trx=DATE(T.tanggal_transaksi) AND T.tipe_trx='1' AND T.st_posting='0'
			WHERE
				H.`status` != '0' ".$where."
			GROUP BY
				H.id
				ORDER BY H.tanggal_trx DESC
				) as tbl";
	// print_r($tanggal_setoran1);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama_kegiatan','no_pengajuan');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
            $no++;
            $row = array();

            $action = '';
            $status = '';
			if ($r->status!='0'){
				if (UserAccesForm($user_acces_form,array('2575'))){
					$action .= '<a href="'.site_url().'tsetoran_rj/update/'.$r->id.'/disabled" target="_blank" data-toggle="tooltip" title="Lihat" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>';
				}
				if ($r->status=='1'){
					if (UserAccesForm($user_acces_form,array('2576'))){
						$action .= '<a href="'.site_url().'tsetoran_rj/update/'.$r->id.'" data-toggle="tooltip" title="Proses" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>';
					}
				}
				if (UserAccesForm($user_acces_form,array('2580'))){
					if ($r->status=='2'){
						$action .= '<button onclick="update_periksa('.$r->id.')" data-toggle="tooltip" title="Periksa" class="btn btn-warning btn-sm"><i class="si si-doc"></i></button>';
					}elseif($r->status=='3'){
						$action .= '<a href="'.site_url().'tsetoran_rj/update/'.$r->id.'" data-toggle="tooltip" title="Periksa" class="btn btn-warning btn-sm"><i class="si si-doc"></i></a>';
						
					}
				}
				if ($r->idvalidasi){
					$disabel_verif='';
				}else{
					$disabel_verif='disabled';
					
				}
				if ($r->status=='4'){
					if ($r->st_verifikasi=='0'){
						if ($r->hasil_pemeriksaan=='1'){//Kelebihan
							if (UserAccesForm($user_acces_form,array('2577'))){
							$action .= '<a href="'.site_url().'tsetoran_rj/update_kekurangan/'.$r->id.'" data-toggle="tooltip" title="Proses Kelebihan" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a>';
							}
							if (UserAccesForm($user_acces_form,array('2581'))){
								if ($r->st_bayar=='1'){
									$action .= '<button class="btn btn-primary btn-sm " onclick="verif('.$r->id.')" '.$disabel_verif.' title="Verifikasi"><i class="fa fa-check"></i></button>';
								}
							}
						}
						if ($r->hasil_pemeriksaan=='2'){//Kekurangan
							if (UserAccesForm($user_acces_form,array('2578'))){
								$action .= '<a href="'.site_url().'tsetoran_rj/update_kekurangan/'.$r->id.'" data-toggle="tooltip" title="Proses Kekurangan" class="btn btn-warning btn-sm"><i class="fa fa-minus"></i></a>';
							}
							if (UserAccesForm($user_acces_form,array('2581'))){
								if ($r->st_bayar=='1'){
									$action .= '<button class="btn btn-primary btn-sm " onclick="verif('.$r->id.')" '.$disabel_verif.' title="Verifikasi"><i class="fa fa-check"></i></button>';
								}
							}
						}
					}
				}
				if ($r->st_verifikasi=='0'){
					if ($r->status=='4'){
						if ($r->selisih==0){
							if (UserAccesForm($user_acces_form,array('2578'))){
							$action .= '<button class="btn btn-primary btn-sm " onclick="verif('.$r->id.')" title="Verifikasi"><i class="fa fa-check"></i></button>';
							}
						}
					}
					$action .= '<button class="btn btn-danger btn-sm hapus"><i class="fa fa-trash"></i></button>';
				}else{
					// if ($r->st_bayar=='1'){
						// $action .= '<a href="'.site_url().'tsetoran_rj/update_kekurangan/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Lihat Pembayaran" class="btn btn-default btn-sm"><i class="fa fa-money"></i></a>';
					// }
				}
			}else{
				$action='<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">DIBATALKAN</span>';
			}
  					
            $row[] = $no;
  			$row[] = HumanDateShort($r->tanggal_trx);
            $row[] = number_format($r->total_transaksi);
            $row[] = number_format($r->total_tunai);
            $row[] = number_format($r->real_cash);
            $row[] = number_format($r->real_pemeriksa);
  			$row[] = get_status_setoran_rj($r->status);
  			$row[] = $this->hasil_pemeriksaan($r->selisih);
  			$row[] = ($r->st_verifikasi=='1'?text_primary('VERIFIED'):text_default('UNVERIFIED'));
            $row[] = '<div class="btn-group">'.$action.'</div>';
            $data[] = $row;
        }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hasil_pemeriksaan($selisih){
	  if ($selisih=='0'){
		  return text_primary('SESUAI');
	  }
	  if ($selisih < 0){
		  return text_danger('KURANG PENDAPATAN');
	  }
	  if ($selisih > 0){
		  return text_success('LEBIH PENDAPATAN');
	  }
  }
  function update_last_update($idtransaksi,$keterangan_update){
	  $user_id=$this->session->userdata('user_id');
	  $data=array(
			'tanggal' => date('Y-m-d H:i:s'),
			'user_id' => $user_id,
			'idtransaksi' => $idtransaksi,
			'keterangan_update' => $keterangan_update,
		);
		$this->db->insert('tsetoran_rj_update_his',$data);
  }
  function getIndex_RJ()
    {
	  $user_id=$this->session->userdata('user_id');
	  // $user_id='101';
	  
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	
	  $id=$this->input->post('id');
	 
	 
	
      $no =0;
	  
	  $q="
		SELECT T.`status` as status_setoran,MU.`name` as nama_user,H.*
				FROM tsetoran_rj_detail H
				LEFT JOIN tsetoran_rj T ON T.id=H.idtransaksi
				LEFT JOIN musers MU ON MU.id=H.user_id
				WHERE H.idtransaksi='$id' AND H.status !='0'
	  ";
	  $list_data=$this->db->query($q)->result();
	  $tabel='';
	  $gt_final_total='0';
	  $gt_jumlah_trx='0';
	  $gt_total_trx='0';
	  $gt_total_non_tunai='0';
	  $gt_total_debit='0';
	  $gt_total_kredit='0';
	  $gt_total_tf='0';
	  $gt_total_kontraktor='0';
	  $gt_total_tunai='0';
	  $gt_total_pengeluaran='0';
	  $gt_real_cash_user='0';
	  $gt_real_cash_pemeriksa='0';
	  $status_user_terkecil='99';
	  $status_pemeriksa_terkecil='99';
      foreach ($list_data as $r) {
            $no++;
            $action = '';
            $disabel_user = '';
            $disabel_pemeriksa = '';
			if ($r->status_setoran=='1'){
				if ($r->status < $status_user_terkecil){
					$status_user_terkecil=$r->status;
				}
				if ($user_id !=$r->user_id){
					$disabel_user = 'disabled';
				}
				if ($r->status=='1'){
					if ($user_id !=$r->user_id){
						$disabel_user = 'disabled';
					}
					$action = '<button class="btn btn-xs btn-primary btn_selesai_input" '.$disabel_user.' title="Selesai" type="button"><i class="si si-arrow-right"></i> SELESAI</button>';
				}elseif ($r->status=='2'){
					$disabel_user = 'disabled';
					if ($user_id !=$r->user_id){
						$action = text_success('SUDAH SELESAI');
					}else{
						
					$action = '<button class="btn btn-xs btn-danger" onclick="batalkan_selesai('.$r->id.')"  type="button"><i class="fa fa-pencil"></i></button>';
					}
				}

			}else{
				$disabel_user = 'disabled';
			}
			if ($r->status_setoran=='3'){
				if ($r->status < $status_pemeriksa_terkecil){
					$status_pemeriksa_terkecil=$r->status;
				}
				if ($r->status=='2'){
					if (UserAccesForm($user_acces_form,array('2580'))){
						$action = '<button class="btn btn-xs btn-warning btn_selesai_input_periksa"  title="Periksa" type="button"><i class="si si-arrow-right"></i> SELESAI</button>';
					}
				}elseif ($r->status=='3'){
					$disabel_pemeriksa='disabled';
					$action = text_success('SUDAH SELESAI');
					$action .= '<br><button class="btn btn-xs btn-danger" onclick="batalkan_selesai_periksa('.$r->id.')"  type="button"><i class="fa fa-pencil"></i></button>';
				}
				
				// if ($user_id !=$r->user_id){
					// $disabel_pemeriksa = 'disabled';
				// }
			}else{
				$disabel_pemeriksa = 'disabled';
			}
			$tanggal="'".$r->tanggal."'";
			$tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="id_detail" value="'.$r->id.'" >'.$no.'</td>';
			$tabel .='<td class="text-center">'. ($r->nama_user).'</td>';
			$tabel .='<td class="text-right">'. number_format($r->jumlah_trx).'</td>';
			$tabel .='<td class="text-right">'. number_format($r->total_trx).'</td>';
			$tabel .='<td class="text-right">'. number_format($r->total_non_tunai).'</td>';
			$tabel .='<td class="text-right">'. number_format($r->total_debit).'</td>';
			$tabel .='<td class="text-right">'. number_format($r->total_kredit).'</td>';
			$tabel .='<td class="text-right">'. number_format($r->total_tf).'</td>';
			$tabel .='<td class="text-right">'. number_format($r->total_kontraktor).'</td>';
			$tabel .='<td class="text-right">'. number_format($r->total_tunai).' <button class="btn btn-xs btn-success" type="button" onclick="getIndex_detail_trx('.$tanggal.','.$r->user_id.')"><i class="fa fa-list-ul"></i></button></td>';
			$tabel .='<td class="text-right">'. number_format($r->total_pengeluaran).'</td>';
			$tabel .='<td class="text-right">
						<input type="hidden" class="form-control text-right input-sm form_sm number final_total" style="width:100%" value="'.$r->final_total.'" >
						'. number_format($r->final_total).'</td>';
			$tabel .='<td class="text-right"><input type="text" class="form-control text-right input-sm form_sm number real_cash_user" '.$disabel_user.' style="width:100%" value="'.$r->real_cash_user.'" ></td>';
			$tabel .='<td class="text-right"><input type="text" class="form-control text-right input-sm form_sm number real_cash_pemeriksa" '.($disabel_pemeriksa).' style="width:100%" value="'.$r->real_cash_pemeriksa.'" ></td>';
			$tabel .='<td class="text-center"><div class="btn-group">'.$action.'</div></td>';
			$tabel .='</tr>';
			
			$gt_final_total=$gt_final_total + $r->final_total;
			$gt_jumlah_trx=$gt_jumlah_trx + $r->jumlah_trx;
			$gt_total_trx=$gt_total_trx + $r->total_trx;
			$gt_total_non_tunai=$gt_total_non_tunai + $r->total_non_tunai;
			$gt_total_debit=$gt_total_debit + $r->total_debit;
			$gt_total_kredit=$gt_total_kredit + $r->total_kredit;
			$gt_total_tf=$gt_total_tf + $r->total_tf;
			$gt_total_kontraktor=$gt_total_kontraktor + $r->total_kontraktor;
			$gt_total_tunai=$gt_total_tunai + $r->total_tunai;
			$gt_total_pengeluaran=$gt_total_pengeluaran + $r->total_pengeluaran;
			$gt_real_cash_user=$gt_real_cash_user + $r->real_cash_user;
			$gt_real_cash_pemeriksa=$gt_real_cash_pemeriksa + $r->real_cash_pemeriksa;
			
			  
        }
        $tabel .='<tr style="background-color:#98ffd8">';
		$tabel .='<td class="text-center" colspan="2"><strong>TOTAL</strong></td>';
		$tabel .='<td class="text-right"><strong><label id="gt_jumlah_trx">'. number_format($gt_jumlah_trx).'</label></strong></td>';
		$tabel .='<td class="text-right"><strong><label id="gt_total_trx">'. number_format($gt_total_trx).'</label></strong></td>';
		$tabel .='<td class="text-right"><strong><label id="gt_total_non_tunai">'. number_format($gt_total_non_tunai).'</label></strong></td>';
		$tabel .='<td class="text-right"><strong><label id="gt_total_debit">'. number_format($gt_total_debit).'</label></strong></td>';
		$tabel .='<td class="text-right"><strong><label id="gt_total_kredit">'. number_format($gt_total_kredit).'</label></strong></td>';
		$tabel .='<td class="text-right"><strong><label id="gt_total_tf">'. number_format($gt_total_tf).'</label></strong></td>';
		$tabel .='<td class="text-right"><strong><label id="gt_total_kontraktor">'. number_format($gt_total_kontraktor).'</label></strong></td>';
		$tabel .='<td class="text-right"><strong><label id="gt_total_tunai">'. number_format($gt_total_tunai).'</label></strong></td>';
		$tabel .='<td class="text-right"><strong><label id="gt_total_pengeluaran">'. number_format($gt_total_pengeluaran).'</label></strong></td>';
		$tabel .='<td class="text-right"><strong><label id="gt_final_total">'. number_format($gt_final_total).'</label></strong></td>';
		$tabel .='<td class="text-right"><strong><label id="gt_real_cash_user">'. number_format($gt_real_cash_user).'</label></strong></td>';
		$tabel .='<td class="text-right"><strong><label id="gt_real_cash_pemeriksa">'. number_format($gt_real_cash_pemeriksa).'</label></strong></td>';
		$tabel .='<td class="text-right"></td>';
        $tabel .='</tr>';
	   $data['tabel']=$tabel;
	   $selisih=$gt_real_cash_pemeriksa-$gt_final_total;
	   $data['gt_real_cash_pemeriksa']=$gt_real_cash_pemeriksa;
	   $data['gt_final_total']=$gt_final_total;
	   $data['selisih']=$selisih;
	   $data['hasil']=$this->hasil_pemeriksaan($selisih);
	   $data['status_user_terkecil']=$status_user_terkecil;
	   $data['status_pemeriksa_terkecil']=$status_pemeriksa_terkecil;
	   $q="SELECT MU.`name` as nama_user,H.tanggal FROM `tsetoran_rj_update_his` H 
			LEFT JOIN musers MU ON MU.id=H.user_id
			WHERE H.idtransaksi='$id' ORDER BY H.tanggal DESC LIMIT 1 ";
		$data_update=$this->db->query($q)->row();
		if ($data_update){
	   $data['user_update']=$data_update-> nama_user.' - '.HumanDateLong($data_update->tanggal);
			
		}else{
			
	   $data['user_update']='';
		}
		
      echo json_encode($data);
  }
  function getIndex_user()
    {
	  
	  $id=$this->input->post('id');
	 
	 
	  $from="(
			SELECT MU.`name` as nama_user,H.* FROM `tsetoran_rj_update_his` H
		LEFT JOIN musers MU ON MU.id=H.user_id
		WHERE H.idtransaksi='1' ORDER BY H.tanggal DESC
				) as tbl";
	// print_r($tanggal_setoran1);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama_user','keterangan_update');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
            $no++;
          	$row = array();
  			$row[] = HumanDateLong($r->tanggal);
            $row[] = ($r->nama_user);
            $row[] = ($r->keterangan_update);
          
            $data[] = $row;
        }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function getIndex_detail_trx()
    {
	  
	  $user_id=$this->input->post('user_id');
	  $tanggal=YMDFormat($this->input->post('tanggal'));
	 $where='';
	  if ($user_id !=''){
		  $where ="WHERE H.user_id='".$user_id."'";
	  }
	  $from="(
					SELECT MU.`name` as naama_user,H.* FROM (
						SELECT 
						K.edited_by as user_id,KB.nominal,K.tanggal,CASE WHEN TP.idtipe=1 THEN 'POLI' ELSE 'IGD' END as tujuan
						,TP.nopendaftaran,TP.no_medrec,TP.namapasien,TP.idtipe
						FROM tkasir K
						LEFT JOIN tkasir_pembayaran KB ON KB.idkasir=K.id
						LEFT JOIN tpoliklinik_tindakan T ON T.id=K.idtindakan
						LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=T.idpendaftaran
						WHERE K.`status`='2' AND DATE(K.tanggal)='$tanggal' AND K.total > 0 AND KB.idmetode='1'

						UNION ALL

						SELECT 
						H.created_by as user_id,KB.nominal
						,H.tanggal,'Informasi Medis' as tujuan,H.notransaksi as nopendaftaran,B.no_medrec,B.namapasien,'0' as idtipe
						FROM trm_pengajuan_informasi H
						LEFT JOIN trm_pengajuan_informasi_detail D ON H.id=idtransaksi AND D.nominal_akhir > 0
						LEFT JOIN trm_pembayaran_informasi_detail KB ON KB.idpengajuan=H.id
						LEFT JOIN trm_layanan_berkas B ON B.id=D.idberkas
						WHERE H.tanggal='$tanggal' AND H.status_pembayaran='2' AND KB.idmetode='1' 
					) H 
					LEFT JOIN musers MU ON MU.id=H.user_id
					".$where."
				) as tbl";
	// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama_user','keterangan_update');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
            $no++;
          	$row = array();
  			$row[] = HumanDateShort($r->tanggal);
            $row[] = ($r->nopendaftaran);
            $row[] = ($r->no_medrec);
            $row[] = ($r->namapasien);
            $row[] = ($r->tujuan);
            $row[] = number_format($r->nominal);
            $row[] = 'Tunai';
          
            $data[] = $row;
        }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function getIndex_user_asal()
    {
	
	  $id=$this->input->post('id');
	 $tabel='';
	 
	
      $no =0;
	  
	  $q="
		SELECT MU.`name` as nama_user,H.* FROM `tsetoran_rj_update_his` H
		LEFT JOIN musers MU ON MU.id=H.user_id
		WHERE H.idtransaksi='1' ORDER BY H.tanggal DESC
	  ";
	  $list_data=$this->db->query($q)->result();
	 
      foreach ($list_data as $r) {
            $no++;
           
			$tabel .='<tr>';
			$tabel .='<td class="text-center">'. ($r->nama_user).'</td>';
			$tabel .='<td class="text-center">'. HumanDateLong($r->tanggal).'</td>';
			$tabel .='<td class="text-left">'. ($r->keterangan_update).'</td>';
			$tabel .='</tr>';
			
			  
        }
       
	   $data['tabel']=$tabel;
	 
      echo json_encode($data);
  }
  
	function create_setoran(){
		$tanggal_trx=YMDFormat($this->input->post('tanggal_trx'));
		$notransaksi=$this->Tsetoran_rj_model->getNoTransaksi();
		
		$data=array(
			'notransaksi' => $notransaksi,
			'tanggal_trx' => $tanggal_trx,
			'tanggal_setoran' => $tanggal_trx,
			'created_at' => date('Y-m-d H:i:s'),
			'user_created' => $this->session->userdata('user_id'),
		);
		$this->db->insert('tsetoran_rj',$data);
		$idtransaksi=$this->db->insert_id();
		$q="
			SELECT T.user_id,MU.`name` as nama_user,SUM(T.tunai) as tunai,SUM(T.debit+T.kredit+T.tf+T.kontraktor) as non_tunai
			,SUM(T.debit) as debit ,SUM(T.kredit) as kredit,SUM(T.tf) as tf,SUM(T.kontraktor) as kontraktor,T.total_trx,SUM(T.pengeluaran) as pengeluaran 
			FROM (
							SELECT 
										K.edited_by as user_id
										,SUM(CASE WHEN KB.idmetode='1' THEN KB.nominal ELSE 0 END)-SUM(K.kembalian) as tunai
										,SUM(CASE WHEN KB.idmetode='2' THEN KB.nominal ELSE 0 END) as debit
										,SUM(CASE WHEN KB.idmetode='3' THEN KB.nominal ELSE 0 END) as kredit
										,SUM(CASE WHEN KB.idmetode='4' THEN KB.nominal ELSE 0 END) as tf
										,SUM(CASE WHEN KB.idmetode='5' THEN KB.nominal ELSE 0 END) as karyawan
										,SUM(CASE WHEN KB.idmetode='6' THEN KB.nominal ELSE 0 END) as tidak_tertagih
										,SUM(CASE WHEN KB.idmetode='7' THEN KB.nominal ELSE 0 END) as kontraktor
										,SUM(1) as total_trx,0 as pengeluaran
										FROM tkasir K
										LEFT JOIN tkasir_pembayaran KB ON KB.idkasir=K.id
										WHERE K.`status`='2' AND DATE(K.tanggal)='$tanggal_trx' AND K.total > 0
										GROUP BY  DATE(K.tanggal),K.edited_by		
							UNION ALL
							SELECT 
							H.created_by as user_id
							,SUM(CASE WHEN KB.idmetode='1' THEN KB.nominal ELSE 0 END) as tunai
										,SUM(CASE WHEN KB.idmetode='2' THEN KB.nominal ELSE 0 END) as debit
										,SUM(CASE WHEN KB.idmetode='3' THEN KB.nominal ELSE 0 END) as kredit
										,SUM(CASE WHEN KB.idmetode='4' THEN KB.nominal ELSE 0 END) as tf
										,SUM(CASE WHEN KB.idmetode='5' THEN KB.nominal ELSE 0 END) as karyawan
										,SUM(CASE WHEN KB.idmetode='6' THEN KB.nominal ELSE 0 END) as tidak_tertagih
										,SUM(CASE WHEN KB.idmetode='7' THEN KB.nominal ELSE 0 END) as kontraktor
										,SUM(1) as total_trx,0 as pengeluaran
							FROM trm_pengajuan_informasi H
							LEFT JOIN trm_pembayaran_informasi_detail KB ON KB.idpengajuan=H.id
							WHERE H.tanggal='$tanggal_trx' AND H.status_pembayaran='2'
							GROUP BY H.tanggal,H.created_by
							
							UNION ALL
							
							SELECT P.user_id,0 as tunai,0 as debit,0 as kredit,0 as tf,0 as karyawan,0 as tidak_tertagih,0 as kontraktor,SUM(1) as total_trx,SUM(P.totalrefund) as pengeluaran FROM (
								SELECT DATE(T.tanggal) as tanggal, T.created_by as user_id,T.totalrefund
								 FROM trefund T
								 LEFT JOIN mfpasien M ON M.id=T.idpasien
								WHERE T.`status`='1' AND T.tipe='2' AND T.tipetransaksi='rawatjalan' AND DATE(T.tanggal)='$tanggal_trx' AND T.metode='1'

								UNION ALL

								SELECT DATE(T.tanggal) as tanggal,T.created_by as user_id,T.totalrefund
								 FROM trefund T
								 LEFT JOIN mfpasien M ON M.id=T.idpasien
								WHERE T.`status`='1' AND T.tipe='1' AND DATE(T.tanggal)='$tanggal_trx' AND T.metode='1'
							) P
							GROUP BY P.tanggal,P.user_id
			) T
			LEFT JOIN musers MU ON MU.id=T.user_id
			GROUP BY T.user_id
		";
	   $list_row=$this->db->query($q)->result();
	   foreach ($list_row as $r){
			$data=array(
				'idtransaksi' => $idtransaksi,
				'tanggal' => $tanggal_trx,
				'created_date' => date('Y-m-d H:i:s'),
				'created_by' => $this->session->userdata('user_id'),
				'user_id' => $r->user_id,
				'jumlah_trx' => $r->total_trx,
				'total_trx' => $r->tunai + $r->non_tunai,
				'total_non_tunai' => $r->non_tunai,
				'total_debit' => $r->debit,
				'total_kredit' => $r->kredit,
				'total_tf' => $r->tf,
				'total_kontraktor' => $r->kontraktor,
				'total_tunai' => $r->tunai,
				'total_pengeluaran' => $r->pengeluaran,
				'final_total' => $r->tunai - $r->pengeluaran,
			);
			$hasil=$this->db->insert('tsetoran_rj_detail',$data);
	   }
	   echo json_encode($hasil);
	}
	function update_real_user(){
		$user_id=$this->session->userdata('user_id');
		$id=($this->input->post('id'));
		$idtransaksi=($this->input->post('idtransaksi'));
		$real_cash_user=($this->input->post('real_cash_user'));
		$data=array(
			'real_cash_user'=>$real_cash_user,
			'status'=>2,
			'created_by'=>$user_id,
			'created_date'=>date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('tsetoran_rj_detail',$data);
		$this->update_last_update($idtransaksi,'UPDATE REAL CASH USER');
	   echo json_encode($hasil);
	}
	function simpan_verifikasi(){
		$user_id=$this->session->userdata('user_id');
		$id=($this->input->post('id'));
		$idtransaksi=($this->input->post('idtransaksi'));
		$tanggal_setoran=YMDFormat($this->input->post('tanggal_setoran'));
		$jenis_id=($this->input->post('jenis_id'));
		$hasil_pemeriksaan=($this->input->post('hasil_pemeriksaan'));
		$keterangan=($this->input->post('keterangan'));
		$st_verifikasi=($this->input->post('st_verifikasi'));
		$nominal=($this->input->post('nominal'));
		$data=array(
			'tanggal_setoran'=>$tanggal_setoran,
			'jenis_id'=>$jenis_id,
			'keterangan'=>$keterangan,
			'nominal'=>$nominal,
			'st_bayar'=>1,
		);
		 if ($hasil_pemeriksaan=='1'){//Pendapatan
			$q="SELECT H.idakun,A.noakun,H.idakun_kredit,AK.noakun as noakun_kredit FROM `mjenis_pendapatan` H 
			LEFT JOIN makun_nomor A ON A.id=H.idakun
			LEFT JOIN makun_nomor AK ON AK.id=H.idakun_kredit
			WHERE H.id='$jenis_id'";
		}else{
			$q="SELECT H.idakun,A.noakun,H.idakun_kredit,AK.noakun as noakun_kredit FROM `mjenis_loss` H 
			LEFT JOIN makun_nomor A ON A.id=H.idakun
			LEFT JOIN makun_nomor AK ON AK.id=H.idakun_kredit
			WHERE H.id='$jenis_id'";
		}
		$data_akun=$this->db->query($q)->row();
		// print_r($q);exit;
		
		$data['idakun']=$data_akun->idakun;
		$data['noakun']=$data_akun->noakun;
		$data['idakun_kredit']=$data_akun->idakun_kredit;
		$data['noakun_kredit']=$data_akun->noakun_kredit;
		
		if ($st_verifikasi=='1'){
			$data['st_verifikasi']=$st_verifikasi;
			$data['verifikasi_by']=$user_id;
			$data['verifikasi_date']=date('Y-m-d H:i:s');
			$this->update_last_update($idtransaksi,'SIMPAN & VERIFIKASI PEMBAYARAN');
		}else{
			$this->update_last_update($idtransaksi,'UPDATE PEMBAYARAN');
		}
		// print_r($idtransaksi);exit;
		$this->db->where('id',$idtransaksi);
		$hasil=$this->db->update('tsetoran_rj',$data);
		if ($st_verifikasi=='1'){
			$this->Tsetoran_rj_model->insert_jurnal_setoran($idtransaksi);
		}
	   echo json_encode($hasil);
	}
	function update_real_pemeriksa(){
		$user_id=$this->session->userdata('user_id');
		$id=($this->input->post('id'));
		$idtransaksi=($this->input->post('idtransaksi'));
		$real_cash_pemeriksa=($this->input->post('real_cash_pemeriksa'));
		$data=array(
			'real_cash_pemeriksa'=>$real_cash_pemeriksa,
			'status'=>3,
			'created_by'=>$user_id,
			'created_date'=>date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('tsetoran_rj_detail',$data);
		$this->update_last_update($idtransaksi,'UPDATE REAL CASH PEMERIKSA');
	   echo json_encode($hasil);
	}
	function update_periksa(){
		$user_id=$this->session->userdata('user_id');
		$id=($this->input->post('id'));
		
		$data=array(
			'status'=>3,
			'edited_by'=>$user_id,
			'edited_date'=>date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('tsetoran_rj',$data);
		$this->update_last_update($id,'START PEMERIKSAAN');
	   echo json_encode($hasil);
	}
	function update_selesai(){
		$user_id=$this->session->userdata('user_id');
		$id=($this->input->post('id'));
		
		$data=array(
			'status'=>2,
			'edited_by'=>$user_id,
			'edited_date'=>date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('tsetoran_rj',$data);
		$this->update_last_update($id,'UPDATE SELESAI PENDAPATAN');
	   echo json_encode($hasil);
	}
	function update_selesai_periksa(){
		$user_id=$this->session->userdata('user_id');
		$id=($this->input->post('id'));
		
		$data=array(
			'status'=>4,
			'edited_by'=>$user_id,
			'edited_date'=>date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('tsetoran_rj',$data);
		$this->update_last_update($id,'UPDATE SELESAI PERIKSA');
	   echo json_encode($hasil);
	}
	function batalkan_selesai(){
		$user_id=$this->session->userdata('user_id');
		$id=($this->input->post('id'));
		$idtransaksi=($this->input->post('idtransaksi'));
		$this->update_last_update($idtransaksi,'EDIT REAL CASH USER');
		$data=array(
			'status'=>1,
			'created_by'=>$user_id,
			'created_date'=>date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('tsetoran_rj_detail',$data);
	   echo json_encode($hasil);
	}
	function batalkan_selesai_periksa(){
		$user_id=$this->session->userdata('user_id');
		$id=($this->input->post('id'));
		$idtransaksi=($this->input->post('idtransaksi'));
		$this->update_last_update($idtransaksi,'EDIT REAL CASH PEMERIKSA');
		$data=array(
			'status'=>2,
			'created_by'=>$user_id,
			'created_date'=>date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('tsetoran_rj_detail',$data);
	   echo json_encode($hasil);
	}
	function create($disabel=''){
		// $tanggal_trx=$this->input->post('tgl_trx');
		// $tanggal_setoran=$this->input->post('tanggal_setoran');
		$data = array(
			'id' 						=> '',			
			'nominal' 						=> '0',			
			'cash_piutang' 						=> '0',			
			'cash_retur' 						=> '0',			
			'cash_rajal' 						=> '0',			
			'cash_ranap' 						=> '0',			
			'cash_pendapatan' 						=> '0',			
			'tanggal_trx' 	=> date('d-m-Y'),
			// 'tanggal_trx' 	=> HumanDateShort('2021-01-12'),
			'tanggal_setoran' 	=> date('d-m-Y'),
			
		);


		$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
		$data['list_pembayaran'] 			= array();
		$data['disabel'] 			= $disabel;
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Pengajuan Kegiatan';
		$data['content'] 		= 'Tsetoran_rj/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Setoran Kas",'#'),
									    			array("Tambah",'Tsetoran_rj')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function update($id,$disabel=''){
		
			$data = $this->Tsetoran_rj_model->getSpecified($id);
			if ($data['hasil_pemeriksaan']=='1'){
				$q="SELECT * FROM `mjenis_pendapatan` H WHERE H.staktif='1'";
				$list_pendapatan=$this->db->query($q)->result();
			
				$data['list_jenis']=$list_pendapatan;
			}
			if ($data['hasil_pemeriksaan']=='2'){
				$q="SELECT * FROM `mjenis_loss` H WHERE H.staktif='1'";
				$list_loss=$this->db->query($q)->result();
				$data['list_jenis']=$list_loss;
			// print_r($data['list_jenis']);exit;
			}
			$this->update_detail($data['tanggal_trx']);
			$data['tanggal_trx']=HumanDateShort($data['tanggal_trx']);
			
			$data['disabel'] 			= $disabel;
			$data['error'] 			= '';
			$data['title'] 			= 'Tambah Pengajuan Kegiatan';
			$data['content'] 		= 'Tsetoran_rj/manage_proses';
			$data['breadcrum']	= array(
															array("RSKB Halmahera",'#'),
															array("Setoran Kas Rawat Jalan",'#'),
														array("Proses",'tsetoran_rj')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		
	}
	// function update_kekurangan($id,$disabel=''){
		
			// $data = $this->Tsetoran_rj_model->getSpecified($id);
			// $data['hasil_pemeriksaan_label'] 			= $this->hasil_pemeriksaan($data['selisih']);
			// $q="SELECT MU.`name` as nama_user,H.tanggal FROM `tsetoran_rj_update_his` H 
			// LEFT JOIN musers MU ON MU.id=H.user_id
			// WHERE H.idtransaksi='$id' ORDER BY H.tanggal DESC LIMIT 1 ";
			// $data_update=$this->db->query($q)->row();
			// if ($data_update){
		   // $data['user_update']=$data_update-> nama_user.' - '.HumanDateLong($data_update->tanggal);
				
			// }else{
				
		   // $data['user_update']='';
			// }
			// // print_r($data);exit;
			// $data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
			
			// if ($data['hasil_pemeriksaan']=='1'){
				// $q="SELECT * FROM `mjenis_pendapatan` H WHERE H.staktif='1'";
				// $list_pendapatan=$this->db->query($q)->result();
			
				// $data['list_jenis']=$list_pendapatan;
			// }
			// if ($data['hasil_pemeriksaan']=='2'){
				// $q="SELECT * FROM `mjenis_loss` H WHERE H.staktif='1'";
				// $list_loss=$this->db->query($q)->result();
				// $data['list_jenis']=$list_loss;
			// // print_r($data['list_jenis']);exit;
			// }
			// $data['list_pembayaran'] 		= $this->Tsetoran_rj_model->list_pembayaran($id);
			// $data['disabel'] 			= $disabel;
			// $data['error'] 			= '';
			// $data['title'] 			= 'Proses Setoran Rawat Jalan';
			// $data['content'] 		= 'Tsetoran_rj/manage_kekurangan';
			// $data['breadcrum']	= array(
															// array("RSKB Halmahera",'#'),
															// array("Setoran Kas Rawat Jalan",'#'),
														// array("Proses",'tsetoran_rj')
														// );

			// $data = array_merge($data, backend_info());
			// $this->parser->parse('module_template', $data);
		
	// }
	function update_kekurangan($id,$disabel=''){
		
			$data = $this->Tsetoran_rj_model->getSpecified($id);
			if ($data['hasil_pemeriksaan']=='1'){
				$q="SELECT * FROM `mjenis_pendapatan` H WHERE H.staktif='1'";
				$list_pendapatan=$this->db->query($q)->result();
			
				$data['list_jenis']=$list_pendapatan;
			}
			if ($data['hasil_pemeriksaan']=='2'){
				$q="SELECT * FROM `mjenis_loss` H WHERE H.staktif='1'";
				$list_loss=$this->db->query($q)->result();
				$data['list_jenis']=$list_loss;
			// print_r($data['list_jenis']);exit;
			}
			$data['disabel'] 			= $disabel;
			$data['error'] 			= '';
			$data['title'] 			= 'Proses Setoran Rawat Jalan';
			$data['content'] 		= 'Tsetoran_rj/manage_proses';
			$data['breadcrum']	= array(
															array("RSKB Halmahera",'#'),
															array("Setoran Kas Rawat Jalan",'#'),
														array("Proses",'tsetoran_rj')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		
	}
	function verif($id){
		// $id=$this->input->post('id');
		$user_id=$this->session->userdata('user_id');
		$result=$this->db->query("UPDATE tsetoran_rj set st_verifikasi='1',verifikasi_by='".$user_id."',verifikasi_date='".date('Y-m-d H:i:s')."' WHERE id='$id'");
		$this->update_last_update($id,'VERIFIKASI TRANSAKSI SETORAN RAWAT JALAN');
		$this->Tsetoran_rj_model->insert_jurnal_setoran($id);
		echo json_encode($result);
	}
	function hapus($id){
		// $id=$this->input->post('idtsetoran_rj
		$result=$this->db->query("UPDATE tsetoran_rj set status='0' WHERE id='$id'");
		$this->update_last_update($id,'HAPUS TRANSAKSI SETORAN RAWAT JALAN');
		echo json_encode($result);
	}
	
	function delete($id){
		$this->Tsetoran_rj_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('Tsetoran_rj','location');
	}
	function update_status($id,$status){
		$result=$this->Tsetoran_rj_model->update_status($id,$status);
		echo json_encode($result);
	}
	function cek_sudah_ada(){
		$tanggal_trx=YMDFormat($this->input->post('tanggal_trx'));
		$q="SELECT id from tsetoran_rj H WHERE H.tanggal_trx='$tanggal_trx' AND H.`status` != '0'";
		$id=$this->db->query($q)->row('id');
		if ($id==null){
			$id=0;
		}
		$result=$id;
		echo json_encode($result);
	}
	
	function save(){
		// print_r($this->input->post());exit();
		if($this->input->post('idtransaksi') == '' ) {
			$id=$this->Tsetoran_rj_model->saveData();
			if($id){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tsetoran_rj','location');
			}
		} else {
			if($this->Tsetoran_rj_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tsetoran_rj','location');
			}
		}

	}
	
	

}
