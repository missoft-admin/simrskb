<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merm_referensi extends CI_Controller {

	/**
	 * Pegawai controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Merm_referensi_model');
		$this->load->helper('path');
		
  }

	function index($ref_head_id='#'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1464'))){
			$data = array();
			$data['list_head']=$this->Merm_referensi_model->list_head();
			$data['ref_head_id'] 			= $ref_head_id;
			$data['status'] 			= 1;
			$data['error'] 			= '';
			$data['title'] 			= 'Data Referensi';
			$data['content'] 		= 'Merm_referensi/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Data Referensi",'#'),
												  array("List",'merm_referensi')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function LoadHead()
    {
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT *FROM merm_referensi_head H
ORDER BY H.id ASC
			) as tbl";
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('referensi_nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;
            $row[] = $r->referensi_nama.' ';
			if ($r->status){
				$row[] = '<span class="label label-success">Active</span>';			
			}else{
				$row[] = '<span class="label label-danger">Not Active</span>';
				
			}
			$aksi = '<div class="btn-group">';
			if ($r->status=='0'){
				$aksi .= '<button data-toggle="tooltip" title="Edit" class="btn btn-success btn-xs" onclick="aktifkan_head('.$r->id.')"><i class="fa fa-pencil"></i> Aktifkan</button>';
				
			}else{
				
				$aksi .= '<button data-toggle="tooltip" title="Edit" class="btn btn-primary btn-xs" onclick="edit_head('.$r->id.')"><i class="fa fa-pencil"></i></button>';
				$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-xs" onclick="hapus_head('.$r->id.')"><i class="fa fa-trash"></i></button>';
			}
			// $aksi .= '<a href="#btabs-animated-slideleft-add"><i class="fa fa-pencil"></i> Add / Edit Rekening</a>';
			$aksi .= '</div>';
            $row[] = $aksi;
            
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	public function save_head()
    {
		
        $id_head = $this->input->post('id_head');
        $referensi_nama = $this->input->post('referensi_nama');
        $data =array(
            'referensi_nama'=>$referensi_nama,
        );
		if ($id_head){
			$this->db->where('id',$id_head);
			$result=$this->db->update('merm_referensi_head', $data);			
		}else{
			$result=$this->db->insert('merm_referensi_head', $data);
			
		}
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function hapus_head()
    {
		
        $id_head = $this->input->post('id');
        $data =array(
            'status'=>'0',
        );
		$this->db->where('id',$id_head);
		$result=$this->db->update('merm_referensi_head', $data);	
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function aktifkan_head()
    {
		
        $id_head = $this->input->post('id');
        $data =array(
            'status'=>'1',
        );
		$this->db->where('id',$id_head);
		$result=$this->db->update('merm_referensi_head', $data);	
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	function refresh_head(){
		$q="SELECT *FROM merm_referensi_head H ORDER BY H.id ASC";
		$hasil=$this->db->query($q)->result();
		$opsi='';
			$opsi .='<option value="#" selected>-Silahkan Pilih-</option>';
		foreach($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->referensi_nama.'</option>';
		}
		$this->output->set_output(json_encode($opsi));
	}
	
	function load_head(){
		$id=$this->input->post('id');
		$q="SELECT *FROM merm_referensi_head H WHERE H.id='$id'";
		$result=$this->db->query($q)->row_array();
		
		$this->output->set_output(json_encode($result));
	}
	function create($ref_head_id='#'){
		
		$data = array(
			'id'            => '',
			'ref' 					=> '',
			'ref_head_id' 	=>$ref_head_id,
			'nilai' 	=> '',
			'st_default' 	=> '0',
		);

		$data['error'] 			= '';
		$data['list_head'] 			= $this->Merm_referensi_model->list_head();
		$data['title'] 			= 'Tambah Data Referensi';
		$data['content'] 		= 'Merm_referensi/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Data Referensi",'#'),
								            array("Tambah",'merm_referensi')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$data= $this->Merm_referensi_model->getSpecified($id);
			$data['list_head'] 			= $this->Merm_referensi_model->list_head();
			
			
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Data Referensi';
			$data['content']    = 'Merm_referensi/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Data Referensi",'#'),
										array("Ubah",'merm_referensi')
										);

			// $data['statusAvailableApoteker'] = $this->Merm_referensi_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('merm_referensi');
		}
	}
	
	function delete($id){
		
		$result=$this->Merm_referensi_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('merm_referensi','location');
	}
	function aktifkan($id){
		
		$result=$this->Merm_referensi_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){
		$ref_head_id=$this->input->post('ref_head_id');
		if($this->input->post('id') == '' ) {
			if($this->Merm_referensi_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('merm_referensi/create/'.$ref_head_id,'location');
			}
		} else {
			if($this->Merm_referensi_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('merm_referensi/index/'.$ref_head_id,'location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Merm_referensi/manage';

		if($id==''){
			$data['title'] = 'Tambah Data Referensi';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Data Referensi",'#'),
							               array("Tambah",'merm_referensi')
								           );
		}else{
			$data['title'] = 'Ubah Data Referensi';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Data Referensi",'#'),
							               array("Ubah",'merm_referensi')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	function getIndex($idkategori='0',$idakun='0')
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$where='';
			$status = $this->input->post('status');
			$ref_head_id = $this->input->post('ref_head_id');
			
			if ($status !='#'){
				$where .=" AND D.status='$status'";
			}
			if ($ref_head_id !='#'){
				$where .=" AND D.ref_head_id = '".$ref_head_id."'";
			}
			$this->select = array();
			$from="
					(
						SELECT D.id,H.referensi_nama, D.ref_head_id,D.ref,D.nilai,D.status,D.st_default FROM `merm_referensi` D
						INNER JOIN merm_referensi_head H ON H.id=D.ref_head_id
						WHERE D.id IS NOT NULL ".$where."
						ORDER BY D.ref_head_id,D.id ASC
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('ref_head_id','referensi_nama','ref','nilai');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
          $result[] = $r->referensi_nama.' ('.$r->ref_head_id.')';
          $result[] = $r->ref;
          $result[] = $r->nilai;
          $result[] = ($r->st_default?text_default('YA'):('TIDAK'));
         $result[] = StatusBarang($r->status);
          $aksi = '<div class="btn-group">';
			if ($r->status=='1'){
				if (UserAccesForm($user_acces_form,array('1466'))){
				$aksi .= '<a href="'.site_url().'merm_referensi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1467'))){
				$aksi .= '<button title="Hapus" type="button" onclick="removeData('.$r->id.')" class="btn btn-danger btn-xs "><i class="fa fa-trash-o"></i></button>';
				}
			}else{
				if (UserAccesForm($user_acces_form,array('1468'))){
				$aksi .= '<button title="Aktifkan" onclick="aktifkan('.$r->id.')" type="button" class="btn btn-success btn-xs aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
				}
			}
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_referensi_list()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$where='';
			$status = $this->input->post('status');
			$ref_head_id = $this->input->post('ref_head_id');
			
			
			if ($ref_head_id !='#'){
				$where .=" AND D.ref_head_id = '".$ref_head_id."'";
			}
			$this->select = array();
			$from="
					(
						SELECT D.id,H.referensi_nama, D.ref_head_id,D.ref,D.nilai,D.status,D.st_default FROM `merm_referensi` D
						INNER JOIN merm_referensi_head H ON H.id=D.ref_head_id
						WHERE D.id IS NOT NULL ".$where."
						ORDER BY D.ref_head_id,D.id ASC
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('ref_head_id','referensi_nama','ref','nilai');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $r->nilai;
          $result[] = $r->ref;
         $result[] = StatusBarang($r->status);
          // $result[] = ($r->st_default?text_default('YA'):('TIDAK'));
          $aksi = '<div class="btn-group">';
			if ($r->status=='1'){
				if (UserAccesForm($user_acces_form,array('1466'))){
				$aksi .= '<a href="'.site_url().'merm_referensi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1467'))){
				$aksi .= '<button title="Hapus" type="button" onclick="removeData('.$r->id.')" class="btn btn-danger btn-xs "><i class="fa fa-trash-o"></i></button>';
				}
			}else{
				if (UserAccesForm($user_acces_form,array('1468'))){
				$aksi .= '<button title="Aktifkan" onclick="aktifkan('.$r->id.')" type="button" class="btn btn-success btn-xs aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
				}
			}
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_tabel_referensi()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$where='';
			$status = $this->input->post('status');
			$ref_head_id = $this->input->post('ref_head_id');
			
			
			if ($ref_head_id !='#'){
				$where .=" AND D.ref_head_id = '".$ref_head_id."'";
			}
			
			$q="
				SELECT D.id,H.referensi_nama, D.ref_head_id,D.ref,D.nilai,D.status,D.st_default 
				,D.created_by
				FROM `merm_referensi` D
				INNER JOIN merm_referensi_head H ON H.id=D.ref_head_id
				WHERE D.status='1' ".$where."
				ORDER BY D.ref_head_id,D.id ASC
			
				";
			$list_data=$this->db->query($q)->result();
	  $tabel='';	
      foreach ($list_data as $r) {
		$aksi = '<div class="btn-group">';
		if ($r->created_by==$this->session->userdata('user_id')){
			$aksi .= '<button title="Edit" type="button" onclick="editDataRef('.$r->id.')" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button title="Hapus" type="button" onclick="removeDataRef('.$r->id.')" class="btn btn-danger btn-xs "><i class="fa fa-trash-o"></i></button>';
		}
		$aksi .= '</div>';
		$tabel .='<tr>';
		$tabel .='<td>'.$r->nilai.'</td>';
		$tabel .='<td>'.$r->ref.'</td>';
		$tabel .='<td>'.StatusBarang($r->status).'</td>';
		$tabel .='<td>'.$aksi.'</td>';
		$tabel .='</tr>';
         
        
      }
	  $data['tabel']=$tabel;
      $this->output->set_output(json_encode($data));
  }
  function editDataRef(){
		$id=$this->input->post('id');
		$data= $this->Merm_referensi_model->getSpecified($id);
		$this->output->set_output(json_encode($data));
	}
	function removeDataRef(){
		$id=$this->input->post('id');
		$result=$this->Merm_referensi_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
   function saveRef(){
		$id=$this->input->post('id');
		$st_edited_ref=$this->input->post('st_edited_ref');
		$ref_head_id=$this->input->post('ref_head_id');
		$ref=$this->input->post('ref');
		$nilai=$this->input->post('nilai');
		
		$data=array(
			'ref_head_id'=>$ref_head_id,
			'ref'=>$ref,
			'nilai'=>$nilai,
		);
		if ($st_edited_ref=='1'){
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$id);
			$data=$this->db->update('merm_referensi',$data);
		}else{
			// $q="SELECT COALESCE(MAX(H.nilai),0)+1 as nilai FROM `merm_referensi` H WHERE H.ref_head_id='$ref_head_id'";
			$q="SELECT COALESCE(MAX(LPAD(H.nilai,3,'0') ),0)+1 as nilai FROM `merm_referensi` H WHERE H.ref_head_id='$ref_head_id' AND H.`status`='1'";
			$data['nilai']=$this->db->query($q)->row('nilai');
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$data=$this->db->insert('merm_referensi',$data);
		}		
		$this->output->set_output(json_encode($data));
	}
	function refresh_ref_isian(){
		$ref_head_id=$this->input->post('ref_head_id');
		$q="SELECT *FROM merm_referensi H WHERE H.ref_head_id='$ref_head_id' AND H.status='1' ORDER BY H.nilai ASC";
		$hasil=$this->db->query($q)->result();
		$opsi='';
			$opsi .='<option value="#" selected>-Silahkan Pilih-</option>';
		foreach($hasil as $r){
			$opsi .='<option value="'.$r->nilai.'">'.$r->ref.'</option>';
		}
		$this->output->set_output(json_encode($opsi));
	}
}
