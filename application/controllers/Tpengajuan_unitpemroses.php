<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpengajuan_unitpemroses extends CI_Controller
{
    /*
     * Pengajuan Unit Pemroses
     * Developer muhamadali1504@gmail.com
     * */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->load->model('Tpengajuan_unitpemroses_model', 'model');
    }

    public function index()
    {
        $data = array();
        $data['error'] = '';
        $data['title'] = 'Pengajuan';
        $data['content'] = 'Tpengajuan_unitpemroses/index';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Pengajuan Unit Proses", '#'),
            array("List", 'tpengajuan_unitpemroses')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function getIndex($uri = 'index')
    {
        $sessionUserId = $this->session->userdata('user_id');
        // $sessionUserRole = $this->session->userdata('user_idpermission');

        $result = $this->model->getIndex($sessionUserId);

        $data = array();
        $no = $_POST['start'];
        foreach ($result as $row) {
            $no++;
            $rows = array();

            $rows[] = $no;
            $rows[] = $row->nopengajuan;
            $rows[] = DMYFormat($row->tanggal);
            $rows[] = DMYFormat($row->tanggaldibutuhkan);
            $rows[] = $row->namapemohon;
            $rows[] = $row->subjek;
            $rows[] = $row->namaunitpelayanan;
            $rows[] = $row->namakeperluan;
            $rows[] = $this->statusBendahara($row->statusbendahara);
            $rows[] = $this->statusUnit($row->statusunitproses, $row->deskripsi);
            $rows[] = $this->actionUnit($row->statusunitproses, $row->idunit);

            $data[] = $rows;
        }

        $output = array(
            "recordsTotal" => count($result),
            "recordsFiltered" => count($result),
            "data" => $data
        );

        echo json_encode($output);
    }

    public function statusBendahara($status)
    {
        switch ($status) {
            case 1:
                $info = '<label class="label label-danger text-uppercase"></i>Belum Diproses</label>';
                break;
            case 2:
                $info = '<label class="label label-success text-uppercase">Sudah Diproses</label>';
                break;
        }

        return $info;
    }

    public function statusUnit($status, $deskripsi = null)
    {
        switch ($status) {
            case 1:
                if ($deskripsi !== "Tunai/Kontrabon/Transfer") {
                    $info = '<label class="label label-danger text-uppercase">'.$deskripsi.'</label>';
                } else {
                    $info = '<label class="label label-danger text-uppercase">Belum Diproses</label>';
                }
                break;
            case 2:
                $info = '<label class="label label-success text-uppercase">Sudah Diproses</label>';
                break;
        }

        return $info;
    }

    public function actionUnit($status, $id)
    {
        switch ($status) {
            case 1:
                $action = '<a href="'.site_url().'/tpengajuan_unitpemroses/create/'.$id.'" class="btn btn-xs btn-primary text-uppercase" style="font-size: 10px;"><i class="fa fa-chevron-circle-right"></i> Proses Tugas</a>';
                break;
            case 2:
                $action = '<a href="'.site_url().'/tpengajuan_unitpemroses/create/'.$id.'" class="btn btn-xs btn-primary text-uppercase" style="font-size: 10px;"><i class="fa fa-chevron-circle-right"></i> Proses Tugas</a>';
                $action .= '<hr style="margin-top: 1px;margin-bottom: 1px;"><button class="btn btn-xs btn-success text-uppercase" type="button" data-toggle="modal" data-target="#modal-end-proses" style="font-size: 10px;"><i class="fa fa-thumbs-up"></i> Proses Selesai</button>';
                break;
        }

        return $action;
    }

    # -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function create($id)
    {
        $data = array();
        $data['error'] = '';
        $data['title'] = 'Pengajuan';
        $data['content'] = 'Tpengajuan_unitpemroses/manage';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Unit Proses Tugas", '#'),
            array("List", 'tpengajuan_unitpemroses')
        );

        $result = $this->model->getDetailUnitPengajuan($id);
        $data['statusbendahara'] = $this->statusBendahara($result[0]->statusbendahara);
        $data['statusunit'] = $this->statusUnit($result[0]->statusunitproses, $result[0]->deskripsi);
        $data['detail'] = $result;

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
}

?>