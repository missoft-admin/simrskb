<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';
use Dompdf\Dompdf;
use Dompdf\Options;

class Trm_pengajuan_informasi extends CI_Controller
{

    /**
     * Pengajuan Informasi Medis controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Trm_pengajuan_informasi_model', 'model');
    }

    public function index()
    {
        $data = array(
            'notransaksi' => '',
            'nama_pemohon' => '',
            'tanggal_dari' => '',
            'tanggal_sampai' => '',
        );

        $data['error'] = '';
        $data['title'] = 'Pengajuan Informasi Medis';
        $data['content'] = 'Trm_pengajuan_informasi/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Pengajuan Informasi Medis",'#'),
            array("List",'trm_pengajuan_informasi')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filter()
    {
        $data = array(
            'notransaksi' => $this->input->post('notransaksi'),
            'nama_pemohon' => $this->input->post('nama_pemohon'),
            'tanggal_dari' => $this->input->post('tanggal_dari'),
            'tanggal_sampai' => $this->input->post('tanggal_sampai'),
        );

        $this->session->set_userdata($data);

        $data['error'] = '';
        $data['title'] = 'Pengajuan Informasi Medis';
        $data['content'] = 'Trm_pengajuan_informasi/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Pengajuan Informasi Medis",'#'),
            array("Filter",'trm_pengajuan_informasi')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create()
    {
        $isReadonly = isset($_GET['isReadonly']) ? $_GET['isReadonly'] : false;
        $data = array(
            'id' => '',
            'idtemp' => hash('ripemd160', $this->session->userdata('user_id') . '' . date("Y-m-d")),
            'tanggal' => date("d/m/Y"),
            'nama_pemohon' => '',
            'keterangan' => '',
            'status' => '',
            'isReadonly' => $isReadonly,
        );

        $data['error'] = '';
        $data['title'] = 'Tambah Pengajuan Informasi Medis';
        $data['content'] = 'Trm_pengajuan_informasi/manage';
        $data['breadcrum'] = array(
            array("RSKB Halmahera",'#'),
            array("Pengajuan Informasi Medis",'#'),
            array("Tambah",'Trm_pengajuan_informasi')
        );

        $data['list_berkas'] = $this->model->getOptionBerkasMedis();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id)
    {
        $isReadonly = isset($_GET['isReadonly']) ? $_GET['isReadonly'] : false;
        $labelSection = $isReadonly ? "Rincian" : "Ubah";

        if ($id != '') {
            $row = $this->model->getSpecified($id);
            if (isset($row->id)) {
                $data = array(
                    'id' => $row->id,
                    'idtemp' => $row->id,
                    'tanggal' => $row->tanggal,
                    'nama_pemohon' => $row->nama_pemohon,
                    'keterangan' => $row->keterangan,
                    'status' => $row->status,
                    'isReadonly' => $isReadonly,
                );

                $data['error'] = '';
                $data['title'] = "$labelSection Pengajuan Informasi Medis";
                $data['content'] = 'Trm_pengajuan_informasi/manage';
                $data['breadcrum'] = array(
                    array("RSKB Halmahera",'#'),
                    array("Pengajuan Informasi Medis",'#'),
                    array($labelSection,'Trm_pengajuan_informasi')
                );

                $data['list_berkas'] = $this->model->getOptionBerkasMedis();

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('Trm_pengajuan_informasi', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('Trm_pengajuan_informasi');
        }
    }

    public function delete($id)
    {
        $this->model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('Trm_pengajuan_informasi', 'location');
    }

    public function save()
    {
        if ($this->input->post('id') == '') {
            if ($this->model->saveData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                redirect('trm_pengajuan_informasi', 'location');
            }
        } else {
            if ($this->model->updateData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                redirect('trm_pengajuan_informasi', 'location');
            }
        }
    }
    
    public function failed_save($id)
    {
        $data = $this->input->post();
        $data['error'] 	 = validation_errors();
        $data['content'] = 'Trm_pengajuan_informasi/manage';

        if ($id=='') {
            $data['title'] = 'Tambah Pengajuan Informasi Medis';
            $data['breadcrum'] = array(
                array("RSKB Halmahera",'#'),
                array("Pengajuan Informasi Medis",'#'),
                array("Tambah",'Trm_pengajuan_informasi')
            );
        } else {
            $data['title'] = 'Ubah Pengajuan Informasi Medis';
            $data['breadcrum'] = array(
                array("RSKB Halmahera",'#'),
                array("Pengajuan Informasi Medis",'#'),
                array("Ubah",'Trm_pengajuan_informasi')
            );
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function print_faktur($id)
    {
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);

        $row = $this->model->getSpecified($id);
        $data = array(
            'id' => $row->id,
            'tanggal' => $row->tanggal,
            'nama_pemohon' => $row->nama_pemohon,
            'keterangan' => $row->keterangan,
            'petugas' => $row->petugas,
        );

        $data['list_kunjungan'] = $this->model->getDataKunjungan($id);

        $html = $this->parser->parse('Trm_pengajuan_informasi/print', array_merge($data, backend_info()), true);
        $html = $this->parser->parse_string($html, $data);
        $dompdf->loadHtml($html);

        // $customPaper = array(0,0,226,4000);
        // $customPaper = array(0,0,226,4000);
        // $dompdf->set_paper($customPaper);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream('Faktur .pdf', array("Attachment"=>0));
    }

    public function getIndex($uri='index')
	{
        $this->select = array(
            'trm_pengajuan_informasi.id',
            'trm_pengajuan_informasi.notransaksi',
            'trm_pengajuan_informasi.tanggal',
            'trm_pengajuan_informasi.nama_pemohon',
            'trm_pengajuan_informasi.keterangan',
            'COUNT(trm_pengajuan_informasi_detail.id) AS jumlah_pengajuan',
            'SUM(trm_pengajuan_informasi_detail.nominal) AS nominal',
            'musers.name AS user_created'
        );

        $this->from = 'trm_pengajuan_informasi';

        $this->join = array(
            array('musers', 'musers.id = trm_pengajuan_informasi.created_by', 'LEFT'),
            array('trm_pengajuan_informasi_detail', 'trm_pengajuan_informasi_detail.idtransaksi = trm_pengajuan_informasi.id', 'LEFT'),
        );

        // FILTER
        $this->where  = array(
            'trm_pengajuan_informasi.status = ' => 1,
        );

        if($uri == 'filter'){
            if ($this->session->userdata('notransaksi') != null) {
                $this->where = array_merge($this->where, array('trm_pengajuan_informasi.notransaksi' => $this->session->userdata('notransaksi')));
            }
            if ($this->session->userdata('nama_pemohon') != null) {
                $this->where = array_merge($this->where, array('trm_pengajuan_informasi.nama_pemohon LIKE' => '%'.$this->session->userdata('nama_pemohon').'%'));
            }
            if ($this->session->userdata('tanggal_dari') != null) {
                $this->where = array_merge($this->where, array('DATE(trm_pengajuan_informasi.tanggal) >=' => YMDFormat($this->session->userdata('tanggal_dari'))));
            }
            if ($this->session->userdata('tanggal_sampai') != null) {
                $this->where = array_merge($this->where, array('DATE(trm_pengajuan_informasi.tanggal) <=' => YMDFormat($this->session->userdata('tanggal_sampai'))));
            }
        }else {
            $this->where = array_merge($this->where, array('DATE(trm_pengajuan_informasi.tanggal)' => date("Y-m-d")));
        }

        $this->order = array();

        $this->group = array('trm_pengajuan_informasi.id');

        $this->column_search = array('trm_pengajuan_informasi.notransaksi','trm_pengajuan_informasi.nama_pemohon');
        $this->column_order = array('trm_pengajuan_informasi.notransaksi','trm_pengajuan_informasi.nama_pemohon');

        $list = $this->datatable->get_datatables();

        $data = array();
        foreach ($list as $r) {
            $row = array();
            
            $row[] = $r->notransaksi;
            $row[] = DMYFormat($r->tanggal);
            $row[] = $r->nama_pemohon;
            $row[] = $r->keterangan;
            $row[] = number_format($r->jumlah_pengajuan);
            $row[] = number_format($r->nominal);
            $row[] = $r->user_created;
            $row[] = '<div class="btn-group">
                <a href="'.site_url().'trm_pengajuan_informasi/update/'.$r->id.'?isReadonly=true" data-toggle="tooltip" title="Rincian" class="btn btn-warning btn-sm"><i class="fa fa-eye"></i></a>
                <a href="'.site_url().'trm_pengajuan_informasi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                <a href="'.site_url().'trm_pengajuan_informasi/print_faktur/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
            </div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        echo json_encode($output);
	}

    function getBerkasPasien()
    {
        $this->select = array(
            'trm_layanan_berkas.id',
            'trm_layanan_berkas.no_medrec',
            'trm_layanan_berkas.namapasien AS nama_pasien',
            'trm_layanan_berkas.tanggal_lahir',
            'trm_layanan_berkas.tanggal_trx AS tanggal_kunjungan',
            'trm_layanan_berkas.tujuan AS layanan',
            '(CASE 
                WHEN trm_layanan_berkas.tujuan IN (1, 2) THEN
                    mpoliklinik.nama
                WHEN trm_layanan_berkas.tujuan IN (3, 4) THEN
                    mkelas.nama
                ELSE ""
            END) AS poliklinik_kelas',
            'mdokter.id AS id_dokter',
            'mdokter.nama AS nama_dokter',
        );
        $this->join = array(
            array('mdokter', 'mdokter.id = trm_layanan_berkas.iddokter', 'LEFT'),
            array('mpoliklinik', 'mpoliklinik.id = trm_layanan_berkas.idpoliklinik AND trm_layanan_berkas.tujuan IN (1, 2)', 'LEFT'),
            array('mkelas', 'mkelas.id = trm_layanan_berkas.kelas AND trm_layanan_berkas.tujuan IN (3, 4)', 'LEFT'),
        );
        $this->where = array();

        $this->where = array_merge($this->where, array('trm_layanan_berkas.tujuan !=' => 5));
        
        if(isset($_POST['nomedrec']) && $_POST['nomedrec'] != ''){
            $this->where = array_merge($this->where, array('trm_layanan_berkas.no_medrec' => $_POST['nomedrec']));
        }

        if(isset($_POST['namapasien']) && $_POST['namapasien'] != ''){
            $this->where = array_merge($this->where, array('trm_layanan_berkas.namapasien LIKE ' => '%'.$_POST['namapasien'].'%'));
        }

        if(isset($_POST['tanggallahir']) && $_POST['tanggallahir'] != ''){
            $this->where = array_merge($this->where, array('trm_layanan_berkas.tanggal_lahir' => $_POST['tanggallahir']));
        }

        if(isset($_POST['dpjp']) && $_POST['dpjp'] != ''){
            $this->where = array_merge($this->where, array('trm_layanan_berkas.iddokter' => $_POST['dpjp']));
        }

        if(isset($_POST['tanggalkunjungan']) && $_POST['tanggalkunjungan'] != ''){
            $this->where = array_merge($this->where, array('trm_layanan_berkas.tanggal_trx' => $_POST['tanggalkunjungan']));
        }

        if(isset($_POST['jeniskunjungan']) && $_POST['jeniskunjungan'] != ''){
            $this->where = array_merge($this->where, array('trm_layanan_berkas.layanan' => $_POST['jeniskunjungan']));
        }

        $this->order = array(
            'trm_layanan_berkas.id' => 'DESC'
        );

        $this->group = array();

        $this->from = 'trm_layanan_berkas';

        $this->column_search = array('trm_layanan_berkas.no_medrec','trm_layanan_berkas.namapasien');
        $this->column_order = array('trm_layanan_berkas.no_medrec','trm_layanan_berkas.namapasien');

        $list = $this->datatable->get_datatables();

        $data = array();
        foreach ($list as $r) {
            $row = array();

            $row[] = "<a href='#' class='selectBerkas' 
                data-idberkas='".$r->id."'
                data-nomedrec='".$r->no_medrec."'
                data-namapasien='".$r->nama_pasien."'
                data-tanggalkunjungan='".$r->tanggal_kunjungan."'
                data-layanan='".GetJenisLayananBerkas($r->layanan)."'
                data-poliklinikkelas='".$r->poliklinik_kelas."'
                data-namadokter='".$r->nama_dokter."'
                data-dismiss='modal'>".$r->no_medrec."</span></a>";
            $row[] = $r->nama_pasien;
            $row[] = $r->tanggal_lahir;
            $row[] = $r->tanggal_kunjungan;
            $row[] = GetJenisLayananBerkas($r->layanan);
            $row[] = $r->nama_dokter;

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        echo json_encode($output);
    }

    function getSettingPengajuan()
    {
        $idBerkas = $_POST['idberkas'];
        $jenisPengajuan = $_POST['jenis_pengajuan'];
		$tanggalKunjungan = YMDFormat($_POST['tanggal_kunjungan']);
		$tanggalPengajuan = YMDFormat($_POST['tanggal_pengajuan']);

        $waktuEstimasi = $this->model->getWaktuEstimasi($jenisPengajuan);
        $tanggalEstimasiSelesai = DMYFormat(addDayToDate($tanggalPengajuan, $waktuEstimasi));

        $nomorUrutPengajuan = $this->model->getUrutanPengajuan($idBerkas, $jenisPengajuan);

        $settingTarif = $this->model->getSettingTarifPengajuan($jenisPengajuan, $nomorUrutPengajuan);

        if (isset($settingTarif)) {
            $idsetting = $settingTarif->idsetting;
			$group_diskon_all = $settingTarif->group_diskon_all;
			$tarif_rs = $settingTarif->tarif_rs;
			$tarif_dokter = $settingTarif->tarif_dokter;
			$total_tarif = $settingTarif->total_tarif;
            $statusSetting = 'AVALIABLE';
        } else {
            $idsetting = 0;
			$group_diskon_all = 0;
			$tarif_rs = 0;
			$tarif_dokter = 0;
			$total_tarif = 0;
            $statusSetting = 'NOT_FOUND';
        }

        $result = array(
            'status_setting' => $statusSetting,
            'tanggal_estimasi_selesai' => $tanggalEstimasiSelesai,
            'nomor_urut_pengajuan' => $nomorUrutPengajuan,
            'idsetting' => $idsetting, 
            'group_diskon_all' => $group_diskon_all, 
            'tarif_rs' => $tarif_rs, 
            'tarif_dokter' => $tarif_dokter, 
            'total_tarif' => $total_tarif,
        );
        
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($result));
    }

    function setDataKunjungan()
    {
        $idtransaksiTemp = $_POST['idtransaksi'];

        $data = array(
            'idtransaksi' => $idtransaksiTemp,
            'idberkas' => $_POST['idberkas'],
            'tanggal_kunjungan' => YMDFormat($_POST['tanggal_kunjungan']),
            'tanggal_pengajuan' => YMDFormat($_POST['tanggal_pengajuan']),
            'jenis_pengajuan' => $_POST['jenis_pengajuan'],
            'pengajuan_ke' => $_POST['pengajuan_ke'],
            'estimasi_selesai' => YMDFormat($_POST['estimasi_selesai']),
            'idsetting' => $_POST['idsetting'],
            'group_diskon_all' => $_POST['group_diskon_all'],
            'tarif_rs' => $_POST['tarif_rs'],
            'tarif_dokter' => $_POST['tarif_dokter'],
            'nominal' => RemoveComma($_POST['nominal_tarif']),
        );

        if ($this->db->insert('trm_pengajuan_informasi_detail', $data)) {
            $response = array(
                'status' => 'success',
                'idtransaksi' => $idtransaksiTemp,
            );
        } else {
            $response = array(
                'status' => 'error',
            );
        }

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response));
    }

    function getDataKunjungan($idtransaksi)
    {
        $response = $this->model->getDataKunjungan($idtransaksi);

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response));
    }

    function removeDataKunjungan($id)
    {
        $response = $this->model->removeDataKunjungan($id);

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response));
    }

    function updateDataKunjungan($id)
    {
        $diskonRupiah = $this->input->post('diskonRupiah');
        $nominalAkhir = $this->input->post('nominalAkhir');
        $response = $this->model->updateDataKunjungan($id, $diskonRupiah, $nominalAkhir);

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response));
    }
}
