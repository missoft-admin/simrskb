<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtujuan_farmasi extends CI_Controller {

	
	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtujuan_farmasi_model');
		$this->load->model('Antrian_layanan_model');
		$this->load->helper('path');
		
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1817'))){
			$data = array();
			$data['error'] 			= '';
			$data['title'] 			= 'Pengaturan Tujuan Farmasi';
			$data['content'] 		= 'Mtujuan_farmasi/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Pengaturan Tujuan Farmasi",'#'),
												  array("List",'mtujuan_farmasi')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create(){
		
		$data = array(
			'id'            => '',
			'nama_tujuan' 					=> '',
			'idunit' 					=> '#',
			'mtujuan_antrian_id' 					=> '#',
			'apoteker_id' 					=> '#',
		);

		$data['error'] 			= '';
		// $data['list_layanan'] 			= $this->Mtujuan_farmasi_model->list_layanan('');
		// $data['list_sound'] 			= $this->Antrian_layanan_model->list_sound();
		$data['list_user'] 			= $this->Mtujuan_farmasi_model->list_user('');
		$data['title'] 			= 'Tambah Pengaturan Tujuan Farmasi';
		$data['content'] 		= 'Mtujuan_farmasi/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Pengaturan Tujuan Farmasi",'#'),
								            array("Tambah",'mtujuan_farmasi')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$data= $this->Mtujuan_farmasi_model->getSpecified($id);
			$data['list_user'] 			= $this->Mtujuan_farmasi_model->list_user($id);
			$data['list_sound'] 			= $this->Antrian_layanan_model->list_sound();
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Pengaturan Tujuan Farmasi';
			$data['content']    = 'Mtujuan_farmasi/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Pengaturan Tujuan Farmasi",'#'),
										array("Ubah",'mtujuan_farmasi')
										);

			// $data['statusAvailableApoteker'] = $this->Mtujuan_farmasi_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtujuan_farmasi');
		}
	}

	function delete($id){
		
		$result=$this->Mtujuan_farmasi_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('mtujuan_farmasi','location');
	}
	function aktifkan($id){
		
		$result=$this->Mtujuan_farmasi_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){
		if($this->input->post('id') == '' ) {
			$id=$this->Mtujuan_farmasi_model->saveData();
			if($id){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mtujuan_farmasi/update/'.$id,'location');
			}
		} else {
			if($this->Mtujuan_farmasi_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mtujuan_farmasi/index','location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mtujuan_farmasi/manage';

		if($id==''){
			$data['title'] = 'Tambah Pengaturan Tujuan Farmasi';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Pengaturan Tujuan Farmasi",'#'),
							               array("Tambah",'mtujuan_farmasi')
								           );
		}else{
			$data['title'] = 'Ubah Pengaturan Tujuan Farmasi';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Pengaturan Tujuan Farmasi",'#'),
							               array("Ubah",'mtujuan_farmasi')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$where='';
			
			$this->select = array();
			$from="
					(
						select H.*
						,CASE WHEN H.idunit=1 THEN 'STATIC' ELSE 'DINAMIS' END as idunit_nama,MT.nama_tujuan as nama_tujuan_antrian
						,UM.nama_user
						FROM mtujuan_farmasi H 
						LEFT JOIN (
							SELECT UU.tujuan_id, GROUP_CONCAT(MU.`name` SEPARATOR ';') as nama_user FROM mtujuan_farmasi_user UU 
							LEFT JOIN musers MU ON MU.id=UU.userid
							GROUP BY UU.tujuan_id
						) UM ON UM.tujuan_id=H.id
						INNER JOIN mtujuan MT ON MT.id=H.mtujuan_antrian_id
						GROUP BY H.id
						ORDER BY H.id
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_tujuan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
          $result[] = $r->nama_tujuan;
          $result[] = $r->nama_tujuan_antrian;

		  $nama_user='';
		  $arr_user=explode(';',$r->nama_user);
		  foreach($arr_user as $index=>$val){
			  $nama_user .=text_success($val).' ';
		  }
          $result[] = $nama_user;
          $result[] = ($r->status?text_primary('AKTIF'):text_danger('TIDAK AKTIF'));
          $aksi = '<div class="btn-group">';
			if ($r->status=='1'){
				if (UserAccesForm($user_acces_form,array('1819'))){
				$aksi .= '<a href="'.site_url().'mtujuan_farmasi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1820'))){
				$aksi .= '<button title="Hapus" type="button" onclick="removeData('.$r->id.')" class="btn btn-danger btn-xs "><i class="fa fa-trash-o"></i></button>';
				}
			}else{
				if (UserAccesForm($user_acces_form,array('1821'))){
				$aksi .= '<button title="Aktifkan" onclick="aktifkan('.$r->id.')" type="button" class="btn btn-success btn-xs aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
				}
			}
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function simpan_sound(){
		$tujuan_id=$this->input->post('tujuan_id');
		$idsound=$this->input->post('idsound');
		$sound_id=$this->input->post('sound_id');
		$nourut=$this->input->post('nourut');
		$data['tujuan_id']=$tujuan_id;
		$data['sound_id']=$sound_id;
		$data['nourut']=$nourut;
		if ($idsound==''){
			// print_r($data);exit;
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('mtujuan_farmasi_sound',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$idsound);
			$hasil=$this->db->update('mtujuan_farmasi_sound',$data);
		}
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		
		  
		  json_encode($hasil);
	}
	function simpan_logic(){
		$idlogic=$this->input->post('idlogic');
		$tujuan_id=$this->input->post('tujuan_id');
		$jenis_order=$this->input->post('jenis_order');
		$asal_pasien=$this->input->post('asal_pasien');
		$idpoli_kelas=$this->input->post('idpoli_kelas');
		$iddokter_ppa=$this->input->post('iddokter_ppa');
		$data['tujuan_id']=$tujuan_id;
		$data['jenis_order']=$jenis_order;
		$data['asal_pasien']=$asal_pasien;
		$data['idpoli_kelas']=$idpoli_kelas;
		$data['iddokter_ppa']=$iddokter_ppa;
		if ($idlogic==''){
			// print_r($data);exit;
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('mtujuan_farmasi_logic',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$idlogic);
			$hasil=$this->db->update('mtujuan_farmasi_logic',$data);
		}
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		
		  
		  json_encode($hasil);
	}
	function load_sound(){
		$tujuan_id=$this->input->post('tujuan_id');
		$this->select = array();
	  $from="(
		SELECT H.id, H.nourut,H.sound_id,M.nama_asset,M.file_sound 
		
			FROM mtujuan_farmasi_sound H
			LEFT JOIN antrian_asset_sound M ON H.sound_id=M.id
			 WHERE H.status='1' AND H.tujuan_id='$tujuan_id' ORDER BY nourut
		) as tbl";
		// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('sound_id');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		$no++;
		$result = array();

		$result[] = $no;
		$result[] = $r->nourut;
		$result[] = $r->nama_asset.' ('.$r->file_sound.')';

		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_sound('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="si si-pencil"></i></button>';	

		$aksi .= '<button onclick="hapus_sound('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		 $aksi .= '</div>';
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
	}
	function load_logic(){
		$tujuan_id=$this->input->post('tujuan_id');
		$this->select = array();
	  $from="(
		SELECT H.*,M.ref as nama_jenis
			,MD.nama as nama_dokter
			,CASE WHEN H.asal_pasien IN (1,2) THEN MP.nama ELSE MK.nama END nama_poli
			FROM mtujuan_farmasi_logic H
			LEFT JOIN merm_referensi M ON H.jenis_order=M.nilai AND M.ref_head_id='100'
			LEFT JOIN mppa MD ON H.iddokter_ppa=MD.id
			LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli_kelas AND H.asal_pasien IN (1,2)
			LEFT JOIN mkelas MK ON MK.id=H.idpoli_kelas AND H.asal_pasien IN (2,3)
			 WHERE H.tujuan_id='$tujuan_id' ORDER BY id
		) as tbl";
		// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('sound_id');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		$no++;
		$result = array();

		$result[] = $no;
		$result[] = $r->nama_jenis;
		$result[] = ($r->asal_pasien=='0'?text_default('ALL'):GetTipePasienPiutang($r->asal_pasien));
		$result[] = ($r->idpoli_kelas=='0'?text_default('SEMUA'):$r->nama_poli);
		$result[] = ($r->iddokter_ppa=='0'?text_default('ALL DOKTER'):$r->nama_dokter);

		$aksi = '<div class="btn-group">';
		// $aksi .= '<button onclick="edit_logic('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="si si-pencil"></i></button>';	

		$aksi .= '<button onclick="hapus_logic('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		 $aksi .= '</div>';
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
	}
	function edit_sound(){
		$id=$this->input->post('id');
		$q="select *FROM mtujuan_farmasi_sound H WHERE H.id='$id'";
		$data=$this->db->query($q)->row_array();
		echo json_encode($data);
	}
	function load_data_sound(){
		$tujuan_id=$this->input->post('tujuan_id');
		$q="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') file
			FROM `mtujuan_farmasi_sound` H
			LEFT JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.tujuan_id='$tujuan_id' AND H.status='1'
			ORDER BY H.nourut ASC";
		$data=$this->db->query($q)->row_array();
		echo json_encode($data);
	}
	function hapus_sound(){
		$id=$this->input->post('id');
		$data['status']=0;
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		// print_r($data);exit;
		$this->db->where('id',$id);
		$hasil=$this->db->update('mtujuan_farmasi_sound',$data);
		json_encode($hasil);
	}
	function hapus_logic(){
		$id=$this->input->post('id');
		
		$this->db->where('id',$id);
		$hasil=$this->db->delete('mtujuan_farmasi_logic');
		json_encode($hasil);
	}
}
