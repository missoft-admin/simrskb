<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpengajuan_bendahara extends CI_Controller
{

    /**
     * Pengajuan controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Tpengajuan_model');
        $this->load->model('Tpengajuan_bendahara_model');
    }

    public function index()
    {
        $data = array(
            'namapemohon' => '',
            'untukbagian' => '#',
            'jenispembayaran' => '#',
            'stproses' => '#',
            'tanggaldari' => date('Y-m-d'),
            'tanggalsampai' => date('Y-m-d')
        );

        $data['error'] = '';
        $data['title'] = 'Pengajuan';
        $data['content'] = 'Tpengajuan_bendahara/index';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Pengajuan", '#'),
            array("List", 'tpengajuan_bendahara')
        );


        $data['list_unitpelayanan'] = $this->Tpengajuan_model->getUnitPelayanan();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function filter()
    {
        $data = array(
            'namapemohon' => $this->input->post('namapemohon'),
            'untukbagian' => $this->input->post('untukbagian'),
            'jenispembayaran' => $this->input->post('jenispembayaran'),
            'stproses' => $this->input->post('stproses'),
            'tanggaldari' => $this->input->post('tanggaldari'),
            'tanggalsampai' => $this->input->post('tanggalsampai')
        );

        $this->session->set_userdata($data);

        $data['error'] = '';
        $data['title'] = 'Pengajuan';
        $data['content'] = 'Tpengajuan_bendahara/index';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Pengajuan", '#'),
            array("List", 'tpengajuan_bendahara')
        );


        $data['list_unitpelayanan'] = $this->Tpengajuan_model->getUnitPelayanan();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }


    public function review($id)
    {
        if ($id != '') {
            $row = $this->Tpengajuan_bendahara_model->getSpecified($id);
            if (isset($row->id)) {
                $data = array(
                    'id' => $row->id,
                    'nopengajuan' => $row->nopengajuan,
                    'tanggaldibutuhkan' => $row->tanggaldibutuhkan,
                    'idvendor' => $row->idvendor,
                    'namavendor' => $row->namavendor,
                    'jenispembayaran' => $row->jenispembayaran,
                    'norekening' => $row->norekening,
                    'bank' => $row->bank,
                    'tanggalkontrabon' => $row->tanggalkontrabon,
                    'stdownpayment' => ($row->downpayment ? 'Aktif' : 'Tidak Aktif'),
                    'downpayment' => number_format($row->downpayment),
                    'cicilan' => number_format($row->cicilan),
                    'subjek' => $row->subjek,
                    'stproses' => $row->stproses,
                );
                $data['error'] = '';
                $data['title'] = 'Proses Bendahara';
                $data['content'] = 'Tpengajuan_bendahara/review';
                $data['breadcrum'] = array(
                    array("RSKB Halmahera", '#'),
                    array("Pengajuan", '#'),
                    array("Proses Bendahara", 'tpengajuan_bendahara')
                );

                $data['list_satuan'] = $this->Tpengajuan_bendahara_model->getSatuan();
                $data['list_detail'] = $this->Tpengajuan_bendahara_model->getDetailBarang($row->id);
                $data['list_termin_progress'] = $this->Tpengajuan_bendahara_model->getDetailTerminProgress($row->id);
                $data['list_termin_fix'] = $this->Tpengajuan_bendahara_model->getDetailTerminFix($row->id);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('tpengajuan_bendahara', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('tpengajuan_bendahara');
        }
    }

    public function saveProses()
    {
        if ($this->Tpengajuan_bendahara_model->saveProses()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('tpengajuan_bendahara', 'location');
        }
    }

    public function getIndex($uri = 'index')
    {
        $sessionUserId = $this->session->userdata('user_id');
//        $sessionUserRole = $this->session->userdata('user_idpermission');

        $result = $this->Tpengajuan_bendahara_model->getIndexPengajuanBendahara($sessionUserId);

        $jumlah_cicilan = null;
        $cicilan_terbayar = null;

        $data = array();
        $no = $_POST['start'];
        foreach ($result as $row) {
            $no++;
            $rows = array();

            switch ($row->stproses) {
                case 4:
                    $action = '<a href="'.site_url().'/tpengajuan_bendahara/review/'.$row->id.'" class="btn btn-sm btn-default text-uppercase"><span style="font-size: 10px;">Proses</span>&nbsp;<i class="fa fa-chevron-circle-right"></i></a>';
                    break;
                case 5:
                    $action = '<a href="" class="btn btn-sm btn-default text-uppercase" style="font-size:10px;"><i class="fa fa-clock-o"></i>&nbsp;History</a>';
                    break;
                case 7:
                    $jumlah_cicilan = count($this->Tpengajuan_bendahara_model->checkTotalCicilan($row->id));
                    $cicilan_terbayar = count($this->Tpengajuan_bendahara_model->checkTotalCicilan($row->id, 2));

                    $action = '<a href="" class="btn btn-sm btn-default text-uppercase" style="font-size:10px;"><i class="fa fa-clock-o"></i>&nbsp;History</a>';
                    $action .= '<hr style="margin-top: 6px;margin-bottom: 6px;"><a href="" class="btn btn-sm btn-link text-uppercase" style="font-size:10px;padding-bottom: 4px;border-bottom: 1px solid blue;">Konfirmasi&nbsp;<i class="fa fa-chevron-circle-right"></i></a>';
                    break;
                default:
                    $action = "-";
            }

            $rows[] = $no;
            $rows[] = $row->nopengajuan;
            $rows[] = DMYFormat($row->tanggal);
            $rows[] = DMYFormat($row->tanggaldibutuhkan);
            $rows[] = $row->namapemohon;
            $rows[] = $row->namaunitpelayanan;
            $rows[] = $row->subjek;
            $rows[] = number_format($row->totalnominal);
            $rows[] = $this->getPenyetuju($row->id);
            $rows[] = $this->GetStatusPengajuan($row->stproses, $jumlah_cicilan, $cicilan_terbayar);
            $rows[] = $this->getUserPerintah($row->idpemrosesan);
            $rows[] = GetJenisPembayaranPengajuan($row->jenispembayaran);
            $rows[] = $action;

            $data[] = $rows;
        }

        $output = array(
            "recordsTotal" => count($result),
            "recordsFiltered" => count($result),
            "data" => $data
        );

        echo json_encode($output);
    }

    public function GetStatusPengajuan($id, $jumlah_cicilan = null, $cicilan_terbayar = null)
    {
        if ($id != '') {
            $data = array(
                '0' => '<span class="label label-danger text-uppercase">Draft</span>',
                '1' => '<span class="label label-warning text-uppercase">Telah Diajukan</span>',
                '2' => '<span class="label label-success text-uppercase">Disetujui</span>',
                '3' => '<span class="label label-danger text-uppercase">Ditolak</span>',
                '4' => '<span class="label label-success text-uppercase">Aktivasi Bendahara</span>',
                '5' => '<span class="label label-success text-uppercase">Selesai</span>',
                '6' => '<span class="label label-danger text-uppercase">Ditolak</span>',
                '7' => '<span class="label label-info text-uppercase">Aktivasi&nbsp;'.$cicilan_terbayar.' / '.$jumlah_cicilan.'</span>'
            );

            return $data[$id];
        } else {
            return '-';
        }
    }

    public function getPenyetuju($id)
    {
        $results = $this->Tpengajuan_model->getPersetujuanDetail($id);

        if (empty($results)) {
            $assigment = "<p class='text-center'>-</p>";
        } else {
            $assigment = '<div class="row items-push text-center">';
            foreach ($results as $row) {
                $status = $row->status_persetujuan_ == 0 ? $row->status_persetujuan : $row->status_persetujuan_;
                $assigment .= '<div class="">';
                $assigment .= '<div class="push-5">' . GetStatusAssigment($status) . '</div>';
                $assigment .= '<div class="h6">' . $row->nama . '</div>';
                if ($row->alasan !== null || $row->alasan_ !== null) {
                    $assigment .= '<div style="padding-top: 10px;"><a href="' . site_url() . 'tpengajuan/penolakan/' . $row->idpengajuan . '/' . $row->id . '" class="btn btn-danger text-uppercase" style="padding: 1px;font-size: 9px;">alasan penolakan</a></div>';
                }
                $assigment .= '</div>';

            }
            $assigment .= '</div>';
        }

        return $assigment;
    }

    public function getUserPerintah($id)
    {
        $results = $this->Tpengajuan_bendahara_model->getUserPerintah($id);

        if (empty($results)) {
            $assigment = "<p class='text-center'>-</p>";
        } else {
            $assigment = '<p class="text-center">';
            $no = 0;
            foreach ($results as $row) {
                if ($no == 0) {
                    $assigment .= $row->nama;
                } else {
                    $assigment .= ', '.$row->nama;
                }

                $no++;
            }
            $assigment .= '</p>';
        }

        return $assigment;
    }

    public function test()
    {
        print_r($this->Tpengajuan_bendahara_model->getUserPerintah());
    }

}
