<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

require 'vendor/autoload.php';

use Dompdf\Dompdf;
use Dompdf\Options;

class Term_hasil_penunjang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');

        $this->load->model('Term_hasil_penunjang_model', 'model');
        $this->load->model('Tpendaftaran_poli_ttv_model', 'poli_ttv_model');
    }

    public function tindakan($asal_rujukan = '', $pendaftaran_id = '', $menu_atas = '', $menu_kiri = '', $transaksi_id = '#', $status_form = ''): void
    {
        $ref = isset($_GET['ref']) ? $_GET['ref'] : '';
        $ref_pendaftaran_id = isset($_GET['ref_pendaftaran_id']) ? $_GET['ref_pendaftaran_id'] : '';
        $ref_asal_rujukan = isset($_GET['ref_asal_rujukan']) ? $_GET['ref_asal_rujukan'] : '';
        $pendaftaran_id = $ref_pendaftaran_id ? $ref_pendaftaran_id : $pendaftaran_id;
        $asal_rujukan = $ref_asal_rujukan ? ($ref_asal_rujukan == '1' || $ref_asal_rujukan == '2' ? 'rawat_jalan' : 'rawat_inap')  : $asal_rujukan;
        $data = $this->_getRequired($asal_rujukan, $pendaftaran_id, $menu_atas, $menu_kiri);

        $data['ref_asal_rujukan'] = $ref_asal_rujukan;
        $data['asal_rujukan_status'] = $asal_rujukan;
        if ($asal_rujukan == 'rawat_jalan') {
            $data['pendaftaran_id'] = $data['pendaftaran_id'];
            $data['pasien_id'] = get_by_field('id', $pendaftaran_id, 'tpoliklinik_pendaftaran')->idpasien;
        } else {
            // $data['pendaftaran_id'] = $data['pendaftaran_id'];
            $data['pendaftaran_id'] = $data['pendaftaran_id_ranap'];
            $data['pasien_id'] = get_by_field('id', $pendaftaran_id, 'trawatinap_pendaftaran')->idpasien;
        }
        
        $data['error'] = '';
        $data['title'] = 'My Pasien';
        $data['content'] = 'Tpendaftaran_poli_ttv/menu/_app_header';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Tindakan Poliklinik', '#'],
            ['My Pasien', 'tpendaftaran_poli_ttv'],
        ];

        $data = array_merge($data, backend_info());
        $data['trx_id'] = $transaksi_id;
        
        $this->parser->parse('module_template', $data);
    }

    public function _getRequired($asal_rujukan, $pendaftaran_id, $menu_atas, $menu_kiri)
    {
        if ($asal_rujukan == 'rawat_jalan') {
            $logicForm = $this->poli_ttv_model->logic_akses_form_erm($pendaftaran_id);
        } else {
            $logicForm = $this->poli_ttv_model->logic_akses_form_erm($pendaftaran_id, 1);
        }

        $defImage = '';
        if (isset($logicForm['jenis_kelamin'])) {
            $defImage = ('1' == $logicForm['jenis_kelamin']) ? 'def_1.png' : 'def_2.png';
        }

        $data = array_merge(
            $logicForm,
            [
                'url_back' => site_url().'tpendaftaran_poli_ttv',
                'url_utama' => site_url().'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id,
                'menu_atas' => $menu_atas,
                'menu_kiri' => $menu_kiri,
                'def_image' => $defImage,
            ]
        );

        return array_merge($data, get_ppa_login());
    }
}
