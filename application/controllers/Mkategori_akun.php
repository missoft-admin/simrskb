<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mkategori_akun extends CI_Controller
{
	/**
	 * Kategori Akun controller.
	 * Developer @gunalirezqimauludi
	 */

	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mkategori_akun_model');
		$this->load->model('Mnomorakuntansi_model');
	}

	public function index()
	{
		$data = [];
		$data['error'] = '';
		$data['title'] = 'Kategori Akun';
		$data['content'] = 'Mkategori_akun/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Kategori Akun', '#'],
			['List', 'mkategori_akun']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function create()
	{
		$data = [
			'id' => '',
			'nama' => '',
			'jikabertambah' => 'DB',
			'jikaberkurang' => 'CR',
			'possaldo' => '',
			'poslaporan' => '',
			'status' => ''
		];

		$data['error'] = '';
		$data['title'] = 'Tambah Kategori Akun';
		$data['content'] = 'Mkategori_akun/manage';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Kategori Akun', '#'],
			['Tambah', 'mkategori_akun']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	public function detail($idkategori)
	{
		$row = $this->Mkategori_akun_model->getSpecified($idkategori);
        $data = [
			'idkategori' => $idkategori,
			'kategoriakun' => $row->nama,
			'headerakun' => '',
			'nomorakun' => '',
			'status' => '',
			'jikabertambah' => '',
			'jikaberkurang' => '',
        ];

		$data['error'] = '';
		$data['list_header'] = $this->Mkategori_akun_model->list_header($idkategori);
		$data['list_akun'] = $this->Mkategori_akun_model->list_akun($idkategori);
		$data['title'] = 'Daftar  Akun Kategori ('.$row->nama.')';
		$data['content'] = 'Mkategori_akun/index_akun';
		$data['breadcrum'] = [
			['RSKB Halmahera', 'dashboard'],
			['Akuntansi', '#'],
			['No. Akuntansi', 'Mnomorakuntansi'],
			['List', '']
		];

		// $data['list_index'] = $this->Mnomorakuntansi_model->getAll();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function load_detail()
    {
		
		$idkategori     		= $this->input->post('idkategori');
		$headerakun     	= $this->input->post('headerakun');
		if ($headerakun){
			$headerakun=implode(',', $headerakun);			
		}
		$noakun     	= $this->input->post('noakun');
		if ($noakun){
			$noakun=implode(',', $noakun);			
		}
		$iduser=$this->session->userdata('user_id');
				
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		$where='';
		$where .=" AND makun_nomor.idkategori='$idkategori'";
        if ($headerakun){
			$where .=" AND makun_nomor.noheader IN (".$headerakun.")";
		}
		if ($noakun){
			$where .=" AND makun_nomor.id IN (".$noakun.")";
		}
		$from="(SELECT
                makun_nomor.id,
                CONCAT( akun_header.noakun, ' - ', akun_header.namaakun ) AS noheader,
                makun_nomor.noakun,
                makun_nomor.namaakun,
                makun_nomor.level,
                mkategori_akun.nama AS namakategori,
                makun_nomor.status,
                makun_nomor.saldo
            FROM
                makun_nomor
                LEFT JOIN makun_nomor akun_header ON akun_header.noakun = makun_nomor.noheader 
                LEFT JOIN mkategori_akun mkategori_akun ON mkategori_akun.id = makun_nomor.idkategori 
            WHERE
                makun_nomor.status = '1' ".$where."
            
            ORDER BY
                makun_nomor.noakun ASC
				) as tbl";
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('noakun','namaakun','namakategori');
        $this->column_order    = array();
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
			
            $row[] = $r->noheader;
            $row[] = $r->noakun;
            $row[] = TreeView($r->level, $r->namaakun);
            $row[] = $r->namakategori;
            $row[] =  StatusRow($r->status);
            $row[] = number_format($r->saldo,2);
			$aksi   = '<div class="btn-group">';
			$aksi .= '<a href="'.base_url().'mnomorakuntansi/view/'.$r->id.'" type="button" title="View" class="btn btn-xs btn-default" target="_blank"><i class="fa fa-eye"></i></a>';
			$aksi .= '<a href="'.base_url().'mnomorakuntansi/view/'.$r->id.'" type="button" title="Lihat Detail Transaksi" class="btn btn-xs btn-primary" target="_blank"><i class="fa fa-list"></i></a>';
			$aksi .= '<button title="Print" class="btn btn-xs btn-success"><i class="fa fa-print"></i></button>';
			$aksi.='</div>';
			$row[] = $aksi;
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	public function update($id)
	{
		if ($id != '') {
			$row = $this->Mkategori_akun_model->getSpecified($id);
			if (isset($row->id)) {
				$data = [
					'id' => $row->id,
					'nama' => $row->nama,
					'jikabertambah' => $row->jikabertambah,
					'jikaberkurang' => $row->jikaberkurang,
					'possaldo' => $row->possaldo,
					'poslaporan' => $row->poslaporan,
					'status' => $row->status
				];

				$data['error'] = '';
				$data['title'] = 'Ubah Kategori Akun';
				$data['content'] = 'Mkategori_akun/manage';
				$data['breadcrum'] = [
					['RSKB Halmahera', '#'],
					['Kategori Akun', '#'],
					['Ubah', 'mkategori_akun']
				];

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			} else {
				$this->session->set_flashdata('error', true);
				$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
				redirect('mkategori_akun', 'location');
			}
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('mkategori_akun');
		}
	}

	public function delete($id)
	{
		$this->Mkategori_akun_model->softDelete($id);
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'data telah terhapus.');
		redirect('mkategori_akun', 'location');
	}

	public function save()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == true) {
			if ($this->input->post('id') == '') {
				if ($this->Mkategori_akun_model->saveData()) {
					$this->session->set_flashdata('confirm', true);
					$this->session->set_flashdata('message_flash', 'data telah disimpan.');
					redirect('mkategori_akun', 'location');
				}
			} else {
				if ($this->Mkategori_akun_model->updateData()) {
					$this->session->set_flashdata('confirm', true);
					$this->session->set_flashdata('message_flash', 'data telah disimpan.');
					redirect('mkategori_akun', 'location');
				}
			}
		} else {
			$this->failed_save($this->input->post('id'));
		}
	}

	public function failed_save($id)
	{
		$data = $this->input->post();
		$data['error'] = validation_errors();
		$data['content'] = 'Mkategori_akun/manage';

		if ($id == '') {
			$data['title'] = 'Tambah Kategori Akun';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['Kategori Akun', '#'],
				['Tambah', 'mkategori_akun']
			];
		} else {
			$data['title'] = 'Ubah Kategori Akun';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['Kategori Akun', '#'],
				['Ubah', 'mkategori_akun']
			];
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function getIndex()
	{
		$this->select = [];

		$this->from = 'mkategori_akun';

		$this->join = [];

		$this->where = [];

		$this->order = [
			'nama' => 'ASC'
		];

		$this->group = [];

		$this->column_search = ['nama'];
		$this->column_order = ['nama'];

		$list = $this->datatable->get_datatables();
		
		$data = [];
		$no = $_POST['start'];

		foreach ($list as $r) {
			$no++;

			if ($r->status == 1) {
				$action = '<a href="' . site_url() . 'mkategori_akun/update/' . $r->id . '" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
						<a href="' . site_url() . 'mkategori_akun/delete/' . $r->id . '" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
			} else {
				$action = '';
			}

			$row = [];
			$row[] = $no;
			$row[] = $r->nama;
			$row[] = StatusRow($r->status);
			$row[] = '<div class="btn-group">' . $action . '
				<a href="' . site_url() . 'mkategori_akun/detail/' . $r->id . '" data-toggle="tooltip" title="Daftar Akun" class="btn btn-success btn-sm"><i class="fa fa-list"></i></a>
			</div>';

			$data[] = $row;
		}

		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];

		echo json_encode($output);
	}
}
