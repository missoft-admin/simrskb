<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mkontrabon extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('mkontrabon_model','model');
	}

	public function index() {
		$data=array();
		$data=$this->model->get_data();
		// print_r($data);exit();
		$data['list_his'] 			= $this->model->list_his();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Kontrabon';
		$data['content'] 		= 'Mkontrabon/manage';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Master Kontrabon",'mkontrabon/index'),
									array("List",'#')
								);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_jatuh_tempo(){
		// $tanggal  = '2020-12-22';
		$tanggal  = $this->input->post('tgl_terima');
		$data=$this->model->get_data();
		// print_r($data['jml_hari']);exit();
		$hari=$data['harikbo'];
		$jml_hari=$data['jml_hari']-1;
		// $week = date('w',strtotime($tanggal)) + 1;
		$week = date('w',strtotime($tanggal. " + $jml_hari days")) + 1;
		// $selisih = $jml_hari + (($week > $hari) * (8 - $week)) +  (($week < $hari) * ($hari - $week)) + (($week > $hari) * 3);
		$selisih = $jml_hari + (($week > $hari) * (7 - $week)) +  (($week < $hari) * ($hari - $week)) + (($week > $hari) * $hari);
		$this->output->set_output(json_encode(date('d-m-Y', strtotime($tanggal. " + $selisih days"))));
		// echo date('d-m-Y', strtotime($tanggal. " + $selisih days"));
	}
	function test_jatuh_tempo(){
		$tanggal  = '2021-02-10';
		$hari=1;
		$jml_hari=7;
		echo "Tanggal Invoice : ";
		echo $tanggal;
		echo "<br/>";
		echo "Setiap Hari : ";
		echo getNamaHari($hari);
		echo "<br/>";
		$week = date('w',strtotime($tanggal. " + $jml_hari days")) + 1;
		$selisih = $jml_hari + (($week > $hari) * (8 - $week)) +  (($week < $hari) * ($hari - $week)) + (($week > $hari) * 3);
		echo $selisih." Hari";
		echo "<br/>";
		echo "Tanggal Jatuh Tempo : ";
		echo date('d-m-Y', strtotime($tanggal. " + $selisih days"));
        echo "<br/>";
        echo $week;
        echo "<br/>";
        echo $hari;
	}
	public function get_user_validasi() {
		$this->db->select('id, name as text');
		$res = $this->db->get('musers')->result_array();
		$this->output->set_output(json_encode($res));
  }
	public function get_user_validasi_selected() {
		$id = $this->uri->segment(3);
		$this->db->select('mkontrabon_user_validasi.user_id as id, musers.name as text');
		$this->db->join('musers', 'musers.id = mkontrabon_user_validasi.user_id', 'left');
		$res = $this->db->get('mkontrabon_user_validasi')->result_array();
		$this->output->set_output(json_encode($res));
	  }
	  public function get_user_aktivasi_selected() {
		$id = $this->uri->segment(3);
		$this->db->select('mkontrabon_user_aktivasi.user_id as id, musers.name as text');
		$this->db->join('musers', 'musers.id = mkontrabon_user_aktivasi.user_id', 'left');
		$res = $this->db->get('mkontrabon_user_aktivasi')->result_array();
		$this->output->set_output(json_encode($res));
	  }

	public function index_table() {
		$this->model->index_table();
	}

	public function save() {		
			
		if($this->model->save()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mkontrabon/index','location');
		}
			
	}

	public function aktifasi() {
		$this->model->aktifasi();
	}
	
	public function non_aktifasi() {
		$this->model->non_aktifasi();
	}

	public function hapus() {
		$this->model->hapus();
	}

}

/* End of file Mkontrabon.php */
/* Location: ./application/controllers/Mkontrabon.php */
