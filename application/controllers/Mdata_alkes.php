<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mdata_alkes extends CI_Controller
{

    /**
     * Alat Kesehatan controller.
     * Developer @gunalirezqimauludi
     */

    function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mdata_alkes_model');
		$this->load->helper('path');
    }

    function index($idkategori='0')
    {
        
        $data = array();
		// print_r($idkategori);exit();
        $data['idkategori']      = $idkategori;
        $data['error']      = '';
        $data['title']      = 'Alat Kesehatan';
        $data['content']    = 'Mdata_alkes/index';
        $data['breadcrum']  = array(
										            array("RSKB Halmahera",'#'),
										            array("Alat Kesehatan",'#'),
										            array("List",'mdata_alkes')
											        );
		$data['list_kategori']  = $this->Mdata_alkes_model->getKategori();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	function filter(){
		// if($this->input->post('idkategori') != '0'){
			// $idkategori = $this->input->post('idkategori');
			// redirect("mdata_alkes/index/$idkategori",'location');
		// }else{
			// redirect('mdata_alkes/index/0','location');
		// }
		$data = array();
		// $idkategori=$this->input->post('idkategori');
		// $row=$this->Mdata_alkes_model->get_array_kategori($idkategori);
		
		// print_r($row);exit();
        $data['idkategori']      = $this->input->post('idkategori');
        $data['error']      = '';
        $data['title']      = 'Alat Kesehatan';
        $data['content']    = 'Mdata_alkes/index';
        $data['breadcrum']  = array(
										            array("RSKB Halmahera",'#'),
										            array("Alat Kesehatan",'#'),
										            array("List",'mdata_alkes')
											        );
		$data['list_kategori']  = $this->Mdata_alkes_model->getKategori();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
	}
    function create()
    {
        
        $data = array(
            'id'                    	=> '',
            'kode'                    => '',
            'idkategori'            	=> '',
            'nama'                    => '',
            'komposisi'            		=> '',
            'indikasi'                => '',
            'idsatuanbesar'        		=> '',
            'hargasatuanbesar'        => '',
            'jumlahsatuanbesar'    		=> '',
            'idsatuankecil'        		=> '',
            'ppn'            		      => '',
            'hargabeli'            		=> '',
            'hargadasar'            	=> '',
            'idrakgudang'        	    => '',
            'idrakfarmasi'            => '',
            'alokasiumum'            	=> '',
            'alokasiasuransi'        	=> '',
            'alokasijasaraharja'    	=> '',
            'alokasibpjskesehatan'    => '',
            'alokasibpjstenagakerja'	=> '',
            'marginumum'            	=> '',
            'marginasuransi'        	=> '',
            'marginjasaraharja'    		=> '',
            'marginbpjskesehatan'    	=> '',
            'marginbpjstenagakerja' 	=> '',
            'stokreorder'            	=> '',
            'stokminimum'            	=> '',
            'catatan'               	=> '',
            'status'                	=> ''
        );

        $data['error']      = '';
        $data['title']      = 'Tambah Alat Kesehatan';
        $data['content']    = 'Mdata_alkes/manage';
        $data['breadcrum']  = array(
										            array("RSKB Halmahera",'#'),
										            array("Alat Kesehatan",'#'),
										            array("Tambah",'mdata_alkes')
											        );

        $data['list_kategori']  = $this->Mdata_alkes_model->getKategori();
        $data['list_satuan'] 		= $this->Mdata_alkes_model->getSatuan();
        $data['list_rakgudang'] 		= $this->Mdata_alkes_model->getRak(1);
        $data['list_rakfarmasi'] 		= $this->Mdata_alkes_model->getRak(2);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
    function update($id)
    {
        
        if ($id != '') {
            $row = $this->Mdata_alkes_model->getSpecified($id);
            if (isset($row->id)) {
                $data = array(
                    'id'                       => $row->id,
                    'kode'                     => $row->kode,
                    'idkategori'               => $row->idkategori,
                    'nama'                     => $row->nama,
                    'komposisi'                => $row->komposisi,
                    'indikasi'                 => $row->indikasi,
                    'idsatuanbesar'            => $row->idsatuanbesar,
                    'hargasatuanbesar'         => $row->hargasatuanbesar,
                    'jumlahsatuanbesar'        => $row->jumlahsatuanbesar,
                    'idsatuankecil'            => $row->idsatuankecil,
                    'ppn'                      => $row->ppn,
                    'hargabeli'                => $row->hargabeli,
                    'hargadasar'               => $row->hargadasar,
                    'idrakgudang'              => $row->idrakgudang,
                    'idrakfarmasi'             => $row->idrakfarmasi,
                    'alokasiumum'              => $row->alokasiumum,
                    'alokasiasuransi'          => $row->alokasiasuransi,
                    'alokasijasaraharja'       => $row->alokasijasaraharja,
                    'alokasibpjskesehatan'     => $row->alokasibpjskesehatan,
                    'alokasibpjstenagakerja'   => $row->alokasibpjstenagakerja,
                    'marginumum'               => $row->marginumum,
                    'marginasuransi'           => $row->marginasuransi,
                    'marginjasaraharja'        => $row->marginjasaraharja,
                    'marginbpjskesehatan'      => $row->marginbpjskesehatan,
                    'marginbpjstenagakerja'    => $row->marginbpjstenagakerja,
                    'stokreorder'              => $row->stokreorder,
                    'stokminimum'              => $row->stokminimum,
                    'catatan'                  => $row->catatan,
                    'status'                   => $row->status
                );
                $data['error']       = '';
                $data['title']       = 'Ubah Alat Kesehatan';
                $data['content']     = 'Mdata_alkes/manage';
                $data['breadcrum']   = array(
												                  array("RSKB Halmahera",'#'),
												                  array("Alat Kesehatan",'#'),
												                  array("Ubah",'mdata_alkes')
													             );

                $data['list_kategori'] = $this->Mdata_alkes_model->getKategori();
                $data['list_satuan'] = $this->Mdata_alkes_model->getSatuan();
                $data['list_rakgudang'] 		= $this->Mdata_alkes_model->getRak(1);
                $data['list_rakfarmasi'] 		= $this->Mdata_alkes_model->getRak(2);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mdata_alkes', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mdata_alkes');
        }
    }

    function delete($id)
    {
        
        $this->Mdata_alkes_model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mdata_alkes', 'location');
    }

    function save()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');

        if ($this->form_validation->run() == true) {
            if ($this->input->post('id') == '') {
                if ($this->Mdata_alkes_model->saveData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mdata_alkes', 'location');
                }
            } else {
                if ($this->Mdata_alkes_model->updateData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mdata_alkes', 'location');
                }
            }
        } else {
            $this->failed_save($this->input->post('id'));
        }
    }

    function failed_save($id)
    {
        $data = $this->input->post();
        $data['error']          = validation_errors();
        $data['content']        = 'Mdata_alkes/manage';
        $data['list_kategori']  = $this->Mdata_alkes_model->getKategori();
        $data['list_satuan']    = $this->Mdata_alkes_model->getSatuan();
        $data['list_rakgudang'] 		= $this->Mdata_alkes_model->getRak(1);
        $data['list_rakfarmasi'] 		= $this->Mdata_alkes_model->getRak(2);

        if ($id=='') {
            $data['title'] 			= 'Tambah Alat Kesehatan';
            $data['breadcrum']  = array(
										                array("RSKB Halmahera",'#'),
										                array("Alat Kesehatan",'#'),
										                array("Tambah",'mdata_alkes')
										              );
        } else {
            $data['title'] 		 = 'Ubah Alat Kesehatan';
            $data['breadcrum'] = array(
										                array("RSKB Halmahera",'#'),
										                array("Alat Kesehatan",'#'),
										                array("Ubah",'mdata_alkes')
											           );
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

  	function getIndex($idkategori='0')
    {
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

		// print_r($idkategori);exit();
		$idkategori=$this->input->post('idkategori');
		$row=$this->Mdata_alkes_model->get_array_kategori($idkategori);
		
        $this->select = array('mdata_alkes.*', 'mdata_kategori.nama AS namakategori');
        $this->from   = 'mdata_alkes';
        $this->join 	= array(
          array("mdata_kategori", "mdata_kategori.id = mdata_alkes.idkategori", "")
        );
		
		// $this->where  = array(
		// 'mdata_alkes.status' => '1'
		// );
		// $row=array('31');
		$this->where_in = array(
					'mdata_alkes.idkategori' => $row
				);
        
        $this->order  = array(
          'mdata_alkes.kode' => 'DESC'
        );
        $this->group  = array();

        $this->column_search   = array('mdata_alkes.kode','mdata_kategori.nama','mdata_alkes.nama');
        $this->column_order    = array('mdata_alkes.kode','mdata_kategori.nama','mdata_alkes.nama');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->kode;
            $row[] = $r->nama;
            $row[] = $r->namakategori;
            $row[] = number_format($r->hargasatuanbesar,0);
            $row[] = number_format($r->hargadasar,0);
			$row[] = StatusBarang($r->status);
            $aksi = '<div class="btn-group">';
			if ($r->status=='1'){
				if (UserAccesForm($user_acces_form,array('91','92','93','94'))){
					$aksi .= '<a href="'.site_url().'mdata_alkes/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('95'))){
					$aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_alkes" data-urlremove="'.site_url().'mdata_alkes/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
				}
			}else{
				$aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_alkes" data-urlremove="'.site_url().'mdata_alkes/aktifkan/'.$r->id.'" data-toggle="tooltip" title="Aktifkan" class="btn btn-success btn-sm aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
			}
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = array(
  	      "draw" => $_POST['draw'],
  	      "recordsTotal" => $this->datatable->count_all(),
  	      "recordsFiltered" => $this->datatable->count_all(),
  	      "data" => $data
        );
        echo json_encode($output);
    }
	function aktifkan($id){
		
		$this->Mdata_alkes_model->aktifkan($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah aktif kembali.');
		redirect('mdata_alkes','location');
	}
}
