<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trm_analisa_hold extends CI_Controller
{

    /**
     * Rincian Analisa Hold controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Trm_analisa_hold_model');
    }

    public function index()
    {
        $data = array(
            'tanggal_dari' => '',
            'tanggal_sampai' => '',
            'nomedrec' => '',
            'namapasien' => '',
            'layanan' => '',
            'iddokter' => '',
            'status' => '',
        );

        $data['error'] 			= '';
        $data['title'] 			= 'Rincian Analisa Hold';
        $data['content'] 		= 'Trm_analisa_hold/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Rincian Analisa Hold",'#'),
            array("List",'trm_analisa_hold')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filter()
    {
        $data = array(
            'tanggal_dari' => $this->input->post('tanggal_dari'),
            'tanggal_sampai' => $this->input->post('tanggal_sampai'),
            'nomedrec' => $this->input->post('nomedrec'),
            'namapasien' => $this->input->post('namapasien'),
            'layanan' => $this->input->post('layanan'),
            'iddokter' => $this->input->post('iddokter'),
            'status' => $this->input->post('status'),
        );

        $this->session->set_userdata($data);

        $data['error'] 			= '';
        $data['title'] 			= 'Rincian Analisa Hold';
        $data['content'] 		= 'Trm_analisa_hold/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Rincian Analisa Hold",'#'),
            array("Filter",'trm_analisa_hold')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function getIndex($uri = 'index')
    {
        $this->select = array(
            'trm_layanan_berkas.idpasien',
            'trm_berkas_analisa.idberkas',
            'trm_berkas_analisa.idpendaftaran',
            'trm_berkas_analisa.asalpendaftaran',
            'trm_berkas_analisa.created_at AS tanggal',
            'trm_layanan_berkas.no_medrec AS nomedrec',
            'trm_layanan_berkas.namapasien',
            '(
				CASE
					WHEN trm_layanan_berkas.tujuan IN (1 ,2) THEN "Rawat Jalan"
					WHEN trm_layanan_berkas.tujuan IN (3 ,4) THEN "Rawat Inap"
				END
			) AS layanan',
            'SUM(
				CASE
					WHEN trm_berkas_analisa.kriteria_nilai = "+" THEN 1
					WHEN trm_berkas_analisa.kriteria_nilai = "-" THEN -1
					WHEN trm_berkas_analisa.kriteria_nilai = "=" THEN 0
				END
			) AS jumlah_nilai_kriteria',
            'trm_berkas_analisa_dokter.iddokter AS iddokter',
            'trm_berkas_analisa_dokter.namadokter AS namadokter',
            'COUNT(trm_berkas_analisa.idlembaran) AS jumlah_lembar_hold',
        );

        $this->from = 'trm_berkas_analisa';

        $this->join = array(
            array('trm_layanan_berkas', 'trm_layanan_berkas.id = trm_berkas_analisa.idberkas', ''),
            array('trm_berkas_analisa_dokter', 'trm_berkas_analisa_dokter.idberkas = trm_berkas_analisa.idberkas AND trm_berkas_analisa_dokter.idanalisa = trm_berkas_analisa.idanalisa AND trm_berkas_analisa_dokter.idlembaran = trm_berkas_analisa.idlembaran', 'LEFT')
        );

        $this->where  = array();
        $this->where = array_merge($this->where, array('trm_berkas_analisa.status' => 1));
        $this->where = array_merge($this->where, array('trm_berkas_analisa.hold' => 1));

        // FILTER
        if ($uri == 'filter') {
            if ($this->session->userdata('tanggal_dari') != null) {
                $this->where = array_merge($this->where, array('DATE(trm_berkas_analisa.created_at) >=' => YMDFormat($this->session->userdata('tanggal_dari'))));
            }
            if ($this->session->userdata('tanggal_sampai') != null) {
                $this->where = array_merge($this->where, array('DATE(trm_berkas_analisa.created_at) <=' => YMDFormat($this->session->userdata('tanggal_sampai'))));
            }
            if ($this->session->userdata('nomedrec') != "") {
                $this->where = array_merge($this->where, array('trm_layanan_berkas.no_medrec' => $this->session->userdata('nomedrec')));
            }
            if ($this->session->userdata('namapasien') != "") {
                $this->where = array_merge($this->where, array('trm_layanan_berkas.namapasien LIKE' => '%' . $this->session->userdata('namapasien') . '%'));
            }
            if ($this->session->userdata('layanan') != "#") {
                $this->where = array_merge($this->where, array('IF(trm_layanan_berkas.tujuan IN (1 ,2), "Rawat Jalan", "Rawat Inap") = ' => $this->session->userdata('layanan')));
            }
            if ($this->session->userdata('iddokter') != "#") {
                $this->where = array_merge($this->where, array('trm_berkas_analisa_dokter.iddokter' => $this->session->userdata('iddokter')));
            }
            if ($this->session->userdata('status') != "#") {
                $this->having = array('IF(jumlah_nilai_kriteria <> jumlah_lembar_hold, 0, 1) = ' => $this->session->userdata('status'));
            }
        }

        $this->order = array(
            'trm_berkas_analisa.created_at' => 'DESC'
        );

        $this->group = array('trm_berkas_analisa.idberkas', 'trm_berkas_analisa_dokter.iddokter');

        $this->column_search = array('trm_layanan_berkas.no_medrec', 'trm_layanan_berkas.namapasien');
        $this->column_order = array('trm_layanan_berkas.no_medrec', 'trm_layanan_berkas.namapasien');

        $list = $this->datatable->get_datatables();

        $number = $_POST['start'];

        $data = array();
        foreach ($list as $index => $r) {
            $number++;
            $row = array();

            if ($r->jumlah_nilai_kriteria != $r->jumlah_lembar_hold) {
                $status = '<span class="label label-danger">Belum Selesai</span>';
            } else {
                $status = '<span class="label label-success">Sudah Selesai</span>';
            }

            $row[] = $number;
            $row[] = $r->tanggal;
            $row[] = $r->nomedrec;
            $row[] = $r->namapasien;
            $row[] = $r->layanan;
            $row[] = $r->namadokter;
            $row[] = $r->jumlah_lembar_hold;
            $row[] = $status;
            $row[] = '<div class="btn-group">
				<a href="'.site_url().'trm_analisa/proses/' . $r->idpasien . '?tipe=verifikasi&idberkas=' . $r->idberkas . '&idpendaftaran=' . $r->idpendaftaran . '&asalpendaftaran=' . $r->asalpendaftaran . '" target="_blank" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
				<a href="'.site_url().'trm_analisa_hold/print/' . $r->idpasien . '?tipe=verifikasi&idberkas=' . $r->idberkas . '&idpendaftaran=' . $r->idpendaftaran . '&asalpendaftaran=' . $r->asalpendaftaran . '" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
			</div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );

        echo json_encode($output);
    }
}
