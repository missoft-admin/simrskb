<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdata_obat_alkes_default extends CI_Controller {

	/**
	 * Obat controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mdata_obat_alkes_default_model');
		$this->load->helper('path');
  }

	function index(){
		// print_r($this->input->post());exit();
		$data = array();
		$data=$this->Mdata_obat_alkes_default_model->getSpecified(1);
		$data['error'] 			= '';
		$data['title'] 			= 'Obat';
		$data['content'] 		= 'Mdata_obat_alkes_default/manage';
		$data['breadcrum'] 	= array(
									array("RSKB Halmahera",'#'),
									array("Obat",'#'),
								array("List",'mdata_obat_alkes_default')
								);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function save(){
		if($this->Mdata_obat_alkes_default_model->updateData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mdata_obat_alkes_default','location');
		}
	}

}
