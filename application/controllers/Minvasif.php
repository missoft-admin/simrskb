<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Minvasif extends CI_Controller {

	/**
	 * Master invasif controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Minvasif_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master invasif';
		$data['content'] 		= 'Minvasif/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master invasif",'#'),
									    			array("List",'minvasif')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'isi_footer' 		=> '',
			'isi_header' 		=> '',
			'nilai_tertimbang' 		=> '0',
			'nilai_satuan' 		=> '0',
			'st_default' 		=> '0',
			'staktif' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master invasif';
		$data['content'] 		= 'Minvasif/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master invasif",'#'),
									    			array("Tambah",'minvasif')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Minvasif_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'template_id' 					=> $row->template_id,
					'staktif' 				=> $row->staktif
				);
				$data['inisial'] 			= '';
				$data['st_nilai'] 			= '1';
				$data['st_default'] 			= '0';
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master invasif';
				$data['content']	 	= 'Minvasif/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master invasif",'#'),
											    			array("Ubah",'minvasif')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('minvasif','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('minvasif');
		}
	}
	function create_with_template(){
		$minvasif_id=$this->input->post('minvasif_id');
		$template_id=$this->input->post('template_id');
		$data=array(
			'template_id'=>$template_id,
		);
		$this->db->where('id',$minvasif_id);
		$this->db->update('minvasif',$data);
		$q="DELETE FROM minvasif_informasi
				WHERE minvasif_id='$minvasif_id'
				";
		$this->db->query($q);
		$q="INSERT INTO minvasif_informasi(minvasif_id,tindakan_id)
		SELECT '$minvasif_id' as minvasif_id,H.id as tindakan_id FROM mtemplate_invasif_detail H
		WHERE H.template_id='$template_id'
		ORDER BY H.nourut ASC";
		$arr=$this->db->query($q);
		$this->output->set_output(json_encode($arr));
	}

	function delete($id){
		$this->Minvasif_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('minvasif','location');
	}
	function load_index_informasi_invasif(){
	  $id=$this->input->post('id');
	 
	  $q="SELECT H.id,H.tindakan_id,D.nourut,D.info_ina,D.info_eng,H.isi_informasi
		FROM minvasif_informasi H
		LEFT JOIN mtemplate_invasif_detail D ON D.id=H.tindakan_id
		WHERE H.minvasif_id='$id'
		ORDER BY D.nourut";
	  $list_info=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_info as $r){
		  // style="text-align: center;"
		  $tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="informasi_id" value="'.$r->id.'">'.$r->nourut.'</td>';
			$tabel .='<td class="text-center"><strong>'.$r->info_ina.'</strong><br><i>'.$r->info_eng.'</i></td>';
			$tabel .='<td><textarea class="js-summernote form-control auto_blur_tabel " name="story" rows="3" style="width:100%">'.$r->isi_informasi.'</textarea></td>';
		  $tabel .='</tr>';
	  } 
	 
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function update_isi_informasi_invasif(){
		$id=$this->input->post('informasi_id');
		$isi_informasi=$this->input->post('isi');
		
		
		$data=array(
			'isi_informasi' => $isi_informasi,
			);
		$this->db->where('id',$id);
		$result=$this->db->update('minvasif_informasi',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
	}
	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->Minvasif_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('minvasif/update/'.$id,'location');
				}
			} else {
				if($this->Minvasif_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('minvasif/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Minvasif/manage';

		if($id==''){
			$data['title'] = 'Tambah Master invasif';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master invasif",'#'),
															array("Tambah",'minvasif')
													);
		}else{
			$data['title'] = 'Ubah Master invasif';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master invasif",'#'),
															array("Ubah",'minvasif')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
			$this->from   = 'minvasif';
			$this->join 	= array();
			$this->where  = array(
				'staktif' => '1'
			);
			$this->order  = array(
				'nama' => 'ASC'
			);
			$this->group  = array();
		
		$this->column_search   = array('nama');
      $this->column_order    = array('nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama;
		$aksi = '<div class="btn-group">';
       
		if (UserAccesForm($user_acces_form,array('2464'))){
            $aksi .= '<a href="'.site_url().'minvasif/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
        }
        if (UserAccesForm($user_acces_form,array('2465'))){
            $aksi .= '<a href="#" data-urlindex="'.site_url().'minvasif" data-urlremove="'.site_url().'minvasif/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
        }
        $aksi .= '</div>';
		$row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
 
}
