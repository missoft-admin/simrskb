<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
  # Master C-Arm #
  @Muhamad Ali Nurdin
*/

class Mc_arm extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Tc_arm_model', 'model');
  }

  public function create($id = null)
  {
    $data['id']         = $id;
    $data['error']      = '';
    $data['title']      = 'Tambah C - Arm';
    $data['content']    = 'Mc_arm/manage';
    $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Bendahara",'#'), array("Master Data",'#'), array("C - Arm",'#'));
    $data = array_merge($data, backend_info());
    $this->parser->parse('module_template', $data);
  }

  public function changeStatusCarm()
  {
    $id     = $this->input->post('id');
    $status = $this->input->post('status');

    $this->model->changeStatusCarm($id, $status);
  }

  public function deleteCarm()
  {

    $id = $this->input->post('id');
    if ($this->model->deleteCarm($id)) {
      return true;
    }
  }

  public function getCarmId()
  {
    $result = $this->model->getCarmId();
    $this->output->set_output(json_encode($result));
  }

  public function getPemilikSaham()
  {
    $result = $this->model->getPemilikSaham();
    $this->output->set_output(json_encode($result));
  }

  public function index()
  {

    $data['error']      = '';
    $data['title']      = 'C - Arm';
    $data['content']    = 'Mc_arm/index';
    $data['breadcrum']  = array(array("RSKB Halmahera",'#'), array("Bendahara",'#'), array("Master Data",'#'), array("C - Arm",'#'));
    $data = array_merge($data, backend_info());
    $this->parser->parse('module_template', $data);
  }

  public function saveCarm() {
    $id    = $this->input->post('mcarm-id');
    $time  = date("Y-m-d H:i:s");
    $tarif = implode(',' ,$this->input->post('name-carm[]'));

    $data = [
      'nama_carm'         => $this->input->post('name-tarif'),
      'tarif_carm'        => $tarif,
      'bagian_pemilik'    => $this->input->post('pemilik-tarif'),
      'potongan_rs'       => $this->input->post('potongan-tarif'),
      'tanggal_berlaku'   => $this->input->post('mulai-tarif'),
      'tanggal_awal'      => $this->input->post('awal-tarif'),
      'tanggal_akhir'     => $this->input->post('akhir-tarif'),
      'catatan'           => $this->input->post('note-carm'),
      'status'            => 1,
      'history_carm_time' => $time
    ];

    $detail = $this->input->post('mcarm-detail');
    $result = $this->model->saveCarm($data, $detail, $id, $tarif);

    // Attachment File
    //Configure upload.
    $this->upload->initialize(array(
      "upload_path"	=> "./assets/upload/carm",
      "allowed_types"	=> '*',
      "encrypt_name"	=> true
    ));

    //Perform upload.
    if($this->upload->do_multi_upload("attachment")) {
      $result = $this->upload->get_multi_data();
      foreach ($result as $row) {
        $data = array();
        $data['id_carm']  = $id;
        $data['lampiran'] = $row['file_name'];

        $this->db->insert('mcarm_lampiran', $data);
      }
    }

    if($result) {
      $this->session->set_flashdata('confirm',true);
      $this->session->set_flashdata('message_flash', 'Sudah Tersimpan !');
      redirect('Mc_arm', 'location');
    };
  }

  public function showCarm()
  {
    $result = $this->model->getCarmView();

    $data = array();
    foreach ($result as $row) {
      $rows = array();

      $id = $row->id;
      $btn = '';

      if(button_roles('Mc_arm/create')) {
        $btn .= '<a href="'.site_url().'Mc_arm/create/'.$id.'" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>&nbsp;';
      }
      if(button_roles('Mc_arm/deleteCarm')) {
        $btn .= '<button class="btn btn-sm btn-danger del-mcarm" data-value="'.$id.'"><i class="fa fa-close"></i></button>';
      }

      if ($row->status == 1) {
        $status = '<button class="btn btn-sm btn-primary status-carm" data-value="'.$row->status.'"><i class="fa fa-thumbs-up"></i></button>';
      } else {
        $status = '<button class="btn btn-sm btn-danger status-carm" data-value="'.$row->status.'"><i class="fa fa-thumbs-down"></i></button>';
      }

      $rows[] = $row->noregistrasi;
      $rows[] = $row->nama_carm;
      $rows[] = $row->totallembaran;
      $rows[] = $status;
      $rows[] = $btn;

      $data[] = $rows;
    }

    $output = array(
        "recordsTotal" => count($result),
        "recordsFiltered" => count($result),
        "data" => $data
      );

    echo json_encode($output);
  }

  public function showCarmIn($id)
  {
    $result = $this->model->getCarm($id);
    $this->output->set_output(json_encode($result));
  }

  public function showCarmDetail($id)
  {
    $result = $this->model->getCarmDetail($id);
    $this->output->set_output(json_encode($result));
  }

}
