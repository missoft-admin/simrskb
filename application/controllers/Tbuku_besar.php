<?php
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
class Tbuku_besar extends CI_Controller
{
	/**
	 * No. Akuntansi controller.
	 * Developer @gunalirezqimauludi
	 */

	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbuku_besar_model', 'model');
		$this->load->model('Mkategori_akun_model');
	}
	function index($idakun='',$tanggal_trx1='',$tanggal_trx2=''){
		
		$row=$this->model->getSpecified($idakun);
		if ($tanggal_trx2==''){
				$tanggal_trx2=date('d-m-Y');
			}
		$awal='';
		if ($row){
			$data=array(
				'idakun' =>$row->id,
				'noakun' =>$row->noakun,
				'namaakun' =>$row->namaakun,
				'tanggal_trx1' =>$tanggal_trx1,
				'tanggal_trx2' =>$tanggal_trx2,
				'notransaksi' =>'',
				'nojurnal' =>'',
				'posisi' =>'',
				'saldoawal' =>'',
			);
			
		}else{
			if ($tanggal_trx1==''){
				$tanggal_trx1=date('d-m-Y');
			}
			$data=array(
				'idakun' =>'',
				'noakun' =>'',
				'namaakun' =>'',
				'tanggal_trx1' =>$tanggal_trx1,
				'tanggal_trx2' =>$tanggal_trx2,
				'notransaksi' =>'',
				'nojurnal' =>'',
				'posisi' =>'',
				'saldoawal' =>'',
			);
		}
		// print_r($data);exit();
		$data['list_akun'] = $this->Mkategori_akun_model->list_akun();
		$data['list_ref'] = $this->model->list_ref();
		$data['disabel'] = '';
		$data['idakun_arr'] = '';
		$data['error'] = '';
		$data['title'] = 'Buku Besar';
		$data['content'] = 'Tbuku_besar/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', 'dashboard'],
			['Akuntansi', '#'],
			['Buku Besar', 'Tbuku_besar'],
			['List', '']
		];

		$data = array_merge($data, backend_info());
		// print_r($data['idperiode_akuntansi']);exit();
		$this->parser->parse('module_template', $data);
	}
	function load_index()
    {
		
		$idakun_arr     		= $this->input->post('idakun_arr');
		$ref_validasi     		= $this->input->post('ref_validasi');		
		$tanggal_trx1     		= $this->input->post('tanggal_trx1');		
		$tanggal_trx2     		= $this->input->post('tanggal_trx2');		
		$notransaksi     		= $this->input->post('notransaksi');		
		$nojurnal     		= $this->input->post('nojurnal');		
		if ($ref_validasi){
			$ref_validasi=implode(',',$ref_validasi);			
		}
		if ($idakun_arr){
			$idakun_arr=implode(',',$idakun_arr);			
		}
		
		$iduser=$this->session->userdata('user_id');
				
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		$where='';
       
		if ($notransaksi !=''){
			$where .=" AND H.notransaksi LIKE '%$notransaksi%'";
		}
		if ($nojurnal !=''){
			$where .=" AND H.nojurnal LIKE '%$nojurnal%'";
		}
        if ($idakun_arr){
			$where .=" AND H.idakun IN (".$idakun_arr.")";
		}
		if ($ref_validasi){
			$where .=" AND H.ref_tabel_validasi IN (".$ref_validasi.")";
		}
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(H.tanggal) >='".YMDFormat($tanggal_trx1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_trx2)."'";
        }
		// if ($noakun){
			// $where .=" AND H.id IN (".$noakun.")";
		// }
		$from="(
					SELECT H.id,H.tanggal,ref_tabel_validasi,H.idakun,H.idvalidasi,H.notransaksi,H.nojurnal 
					,A.noakun,A.namaakun,H.debet,H.kredit,H.keterangan,J.nama as asal,H.link_transaksi,H.link_validasi,H.link_rekap

					FROM `jurnal_umum` H
					LEFT JOIN makun_nomor A ON A.id=H.idakun
					LEFT JOIN ref_jenis_jurnal J ON J.ref_validasi=H.ref_tabel_validasi
					WHERE H.`status`='1' ".$where."
					ORDER BY H.idakun,H.tanggal
				) as tbl";
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('noakun','namaakun','namakategori');
        $this->column_order    = array();
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;
            $row[] = HumanDateShort($r->tanggal);
            $row[] = $r->nojurnal;
            $row[] = $r->notransaksi;
            $row[] = $r->noakun.' - '.$r->namaakun;
            $row[] = number_format($r->debet,2, ".", ",");
            $row[] = number_format($r->kredit,2, ".", ",");
            $row[] = number_format(0,2, ".", ",");
            $row[] = $r->keterangan;
            $row[] = $r->asal;
				$aksi   = '<div class="btn-group">';
				if ($r->link_transaksi){
				$aksi .= '<a href="'.base_url().$r->link_transaksi.'" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';					
				}
				if ($r->link_validasi){
				$aksi .= '<a href="'.base_url().$r->link_validasi.'/disabled" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
				}
				if ($r->link_rekap){
				$aksi .= '<a href="'.base_url().$r->link_rekap.'/disabled" target="_blank" title="Detail" class="btn btn-xs btn-danger"><i class="si si-doc"></i></a>';
				}
				$aksi.='</div>';
			$row[] = $aksi;
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	function load_jurnal(){
		$jml_page=10;
		$where='';
		$limit='';
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$jml_page     		= $this->input->post('jml_page');
		$idakun_arr     		= $this->input->post('idakun_arr');
		$ref_validasi     		= $this->input->post('ref_validasi');		
		$tanggal_trx1     		= $this->input->post('tanggal_trx1');		
		$tanggal_trx2     		= $this->input->post('tanggal_trx2');		
		$notransaksi     		= $this->input->post('notransaksi');
		$no     		= $this->input->post('tmp_no');
		$jml_all     		= $this->input->post('jml_all');
		$tmp_idakun     		= $this->input->post('tmp_idakun');
		
		$offset     		= $this->input->post('offset');
		$tmp_saldo     		= $this->input->post('tmp_saldo');
		
		
		$nojurnal     		= $this->input->post('nojurnal');		
		if ($ref_validasi){
			$ref_validasi=implode(',',$ref_validasi);			
		}
		if ($idakun_arr){
			$idakun_arr=implode(',',$idakun_arr);			
		}
		
		$iduser=$this->session->userdata('user_id');
		
		if ($notransaksi !=''){
			$where .=" AND H.notransaksi LIKE '%$notransaksi%'";
		}
		if ($nojurnal !=''){
			$where .=" AND H.nojurnal LIKE '%$nojurnal%'";
		}
        if ($idakun_arr){
			$where .=" AND H.idakun IN (".$idakun_arr.")";
		}
		if ($ref_validasi){
			$where .=" AND H.ref_tabel_validasi IN (".$ref_validasi.")";
		}
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(H.tanggal) >='".YMDFormat($tanggal_trx1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_trx2)."'";
        }
			$limit=" LIMIT ".$offset.",".$jml_page;
		if ($jml_all=='0'){
			$q="SELECT COUNT(*) jml_all
					FROM `jurnal_umum` H
					LEFT JOIN makun_nomor A ON A.id=H.idakun
					LEFT JOIN ref_jenis_jurnal J ON J.ref_validasi=H.ref_tabel_validasi
					WHERE H.`status`='1' ".$where;
			$jml_all=$this->db->query($q)->row('jml_all');
					
		}
		$q = "
				SELECT H.id,H.tanggal,ref_tabel_validasi,H.idakun,H.idvalidasi,H.notransaksi,H.nojurnal 
					,A.noakun,A.namaakun,H.debet,H.kredit,H.keterangan,J.nama as asal,H.link_transaksi,H.link_validasi,H.link_rekap
					,A.possaldo
					FROM `jurnal_umum` H
					LEFT JOIN makun_nomor A ON A.id=H.idakun
					LEFT JOIN ref_jenis_jurnal J ON J.ref_validasi=H.ref_tabel_validasi
					WHERE H.`status`='1' ".$where."
					ORDER BY H.idakun,H.tanggal ASC ".$limit."
			";
		
			// print_r($q);exit();
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		// $no=0;
		$offset=$offset + $jml_page;
		$saldo=0;
		
		foreach($rows as $r){
			if ($tmp_idakun!=$r->idakun){
				$tmp_idakun=$r->idakun;
				$q="SELECT get_saldo_awal_akun(".$r->idakun.",'".YMDFormat($tanggal_trx1)."',0) as saldo";
				$tmp_saldo=$this->db->query($q)->row('saldo');
			}
			if ($r->possaldo=='DB'){
				$tmp_saldo=$tmp_saldo + $r->debet - $r->kredit;
			}else{
				$tmp_saldo=$tmp_saldo + $r->kredit - $r->debet;
				
			}
			
			$no++;
			$tbl .='<tr>';
			$tbl .='<td  class="text-right">'.$no.'</td>';
			$tbl .='<td>'.HumanDateShort($r->tanggal).'</td>';
			$tbl .='<td>'.$r->nojurnal.'</td>';
			$tbl .='<td>'.$r->notransaksi.'</td>';
			$tbl .='<td >'.$r->noakun.' - '.$r->namaakun.' ('.$r->idakun.')</td>';
			$tbl .='<td class="text-right">'.number_format($r->debet,2, ".", ",").'</td>';
			$tbl .='<td class="text-right">'.number_format($r->kredit,2, ".", ",").'</td>';
			$tbl .='<td class="text-right">'.number_format($tmp_saldo,2, ".", ",").'</td>';
			$tbl .='<td >'.$r->keterangan.'</td>';
			$tbl .='<td >'.$r->asal.'</td>';
				$aksi   = '<div class="btn-group">';
				if ($r->link_transaksi){
				$aksi .= '<a href="'.base_url().$r->link_transaksi.'" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';					
				}
				if ($r->link_validasi){
				$aksi .= '<a href="'.base_url().$r->link_validasi.'/disabled" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
				}
				if ($r->link_rekap){
				$aksi .= '<a href="'.base_url().$r->link_rekap.'/disabled" target="_blank" title="Detail" class="btn btn-xs btn-danger"><i class="si si-doc"></i></a>';
				}
				$aksi.='</div>';
			$tbl .='<td>'.$aksi.'</td>';
			$tbl .='</tr>';
		}
		
		$arr['tabel']=$tbl;
		$arr['offset']=$offset;
		$arr['jml_all']=$jml_all;
		$arr['tmp_idakun']=$tmp_idakun;
		$arr['tmp_saldo']=$tmp_saldo;
		$arr['no']=$no;
		$this->output->set_output(json_encode($arr));
    
	}
	function load_info_akun()
	{
		$tanggal=YMDFormat($this->input->post('tanggal'));
		$idakun=$this->input->post('idakun');
		$q="SELECT H.possaldo,H.noakun,H.namaakun,get_saldo_awal_akun(H.id,'".$tanggal."',0) as saldo FROM makun_nomor H WHERE H.id=$idakun";
			// print_r($q);exit();
		$rows=$this->db->query($q)->row();
		if ($rows){
			$arr['saldo']=$rows->saldo;
			$arr['possaldo']=$rows->possaldo;
			$arr['noakun']=$rows->noakun;
			$arr['namaakun']=$rows->namaakun;
		}else{
			$arr['saldo']=0;
			$arr['possaldo']='';
			$arr['noakun']='';
			$arr['namaakun']='';
		}
		$this->output->set_output(json_encode($arr));
	}

	
	public function export()
    {
		// print_r($this->input->post());exit();
		// $str='200000.98';
		// $str2='2.01';
		
		// print_r((float)$str+(float)$str2);exit();
		
        ini_set('memory_limit', '-1');
        set_time_limit(1000);
		$tanggal_trx1     		= $this->input->post('tanggal_trx1');		
		$tanggal_trx2     		= $this->input->post('tanggal_trx2');	
		$data=array(
			'tanggal' =>HumanDateShort($tanggal_trx1).' s/d '.HumanDateShort($tanggal_trx2),
			'judul' =>'BUKU BESAR',
			'akun' =>'Semua',
			'no_bukti' =>'Semua',
			'no_validasi' =>'Semua',
			'jenis_jurnal' =>'Semua',
			'tanggal_trx1' =>$tanggal_trx1,
		);
		$where='';
		$idakun_arr     		= $this->input->post('idakun_arr');
		$ref_validasi     		= $this->input->post('ref_validasi');		
			
		$notransaksi     		= $this->input->post('notransaksi');
		$no     		= $this->input->post('tmp_no');
		$jml_all     		= $this->input->post('jml_all');
		$tmp_idakun     		= $this->input->post('tmp_idakun');
		
		$offset     		= $this->input->post('offset');
		$tmp_saldo     		= $this->input->post('tmp_saldo');
		
		
		$nojurnal     		= $this->input->post('nojurnal');		
		if ($ref_validasi){
			$ref_validasi=implode(',',$ref_validasi);		
			$data['jenis_jurnal']=$this->db->query("SELECT GROUP_CONCAT(H.nama) as hasil FROM ref_jenis_jurnal H WHERE H.ref_validasi IN (".$ref_validasi.") ORDER BY H.nama")->row('hasil');
		}
		if ($idakun_arr){
			$idakun_arr=implode(',',$idakun_arr);	
			$data['akun']=$this->db->query("SELECT GROUP_CONCAT(CONCAT(A.noakun,'-',A.namaakun) )as hasil FROM makun_nomor A WHERE A.id IN (".$idakun_arr.")")->row('hasil');
		}
			// print_r($this->input->post());exit();
		
		$iduser=$this->session->userdata('user_id');
		
		if ($notransaksi !=''){
			$where .=" AND H.notransaksi LIKE '%$notransaksi%'";
			$data['no_bukti']=$notransaksi;
		}
		if ($nojurnal !=''){
			$where .=" AND H.nojurnal LIKE '%$nojurnal%'";
			$data['no_validasi']=$nojurnal;
		}
        if ($idakun_arr){
			$where .=" AND H.idakun IN (".$idakun_arr.")";
		}
		if ($ref_validasi){
			$where .=" AND H.ref_tabel_validasi IN (".$ref_validasi.")";
		}
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(H.tanggal) >='".YMDFormat($tanggal_trx1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_trx2)."'";
        }
		
		// print_r($data);exit();
		$q = "
				SELECT H.id,H.tanggal,ref_tabel_validasi,H.idakun,H.idvalidasi,H.notransaksi,H.nojurnal 
					,A.noakun,A.namaakun,H.debet,H.kredit,H.keterangan,J.nama as asal,H.link_transaksi,H.link_validasi,H.link_rekap
					,A.possaldo
					FROM `jurnal_umum` H
					LEFT JOIN makun_nomor A ON A.id=H.idakun
					LEFT JOIN ref_jenis_jurnal J ON J.ref_validasi=H.ref_tabel_validasi
					WHERE H.`status`='1' ".$where."
					ORDER BY H.idakun,H.tanggal ASC 
			";
		
			// print_r($q);exit();
		$tbl='';
		
		// $rows=$this->db->query($q)->result();
        $btn    = $this->input->post('btn_export');
        $row_detail=$this->db->query($q)->result();
        
		if ($btn=='1'){
			$this->excel($row_detail,$data);
		}
		if ($btn=='2'){
			// print_r($data);exit();
			$this->pdf($row_detail,$data);
		}
		if ($btn=='3'){
			$this->pdf($row_detail,$data);
		}
    }
	public function pdf($row_detail,$row){
		// print_r($row_detail);exit();
		$dompdf = new Dompdf();
		$data=array();
		$data=array_merge($data,$row);
        
        $data['detail'] = $row_detail;
		// print_r($data);exit();
		$data = array_merge($data, backend_info());

        $html = $this->load->view('Tbuku_besar/pdf', $data, true);
		// print_r($html	);exit();
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('No. Akuntansi.pdf', array("Attachment"=>0));
	}
	function excel($row_detail,$data){
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle($data['judul']);

        // Set Logo
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $logo = FCPATH.'/assets/upload/logo/logoreport.jpg';
        $objDrawing->setPath($logo);
        $objDrawing->setCoordinates('B1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setHeight(70);
        $objDrawing->setWidth(40);
        $objDrawing->setWorksheet($activeSheet);

        // Set Header
        $activeSheet->setCellValue('B1', "                                      RUMAH SAKIT KHUSUS BEDAH HALMAHERA SIAGA");
        $activeSheet->setCellValue('B2', "                                      Jl.LLRE.Martadinata no.28, Telp: +6222-4206061");
        $activeSheet->setCellValue('B3', "                                      Bandung, 40115");
        $activeSheet->mergeCells('B1:AA1');
        $activeSheet->mergeCells('B2:AA2');
        $activeSheet->mergeCells('B3:AA3');
        $activeSheet->getStyle('B1:AA1')->getFont()->setSize(11);
        $activeSheet->getStyle('B2:AA2')->getFont()->setSize(11);
        $activeSheet->getStyle('B3:AA3')->getFont()->setSize(11);
		
		$activeSheet->setCellValue('B5', "TANGGAL ");
		$activeSheet->setCellValue('C5', ": ".$data['tanggal']);
        $activeSheet->setCellValue('B6', "NO. AKUN  ");
		$activeSheet->setCellValue('C6', ": ".$data['akun']);
        $activeSheet->setCellValue('B7', "NO. BUKTI ");
		$activeSheet->setCellValue('C7', ": ".$data['no_bukti']);
		$activeSheet->setCellValue('D7', "NO. VALIDASI ");
		$activeSheet->setCellValue('E7', ": ".$data['no_validasi']);
        $activeSheet->setCellValue('B8', "JENIS JURNAL ");
		$activeSheet->setCellValue('C8', ": ".$data['jenis_jurnal']);
		
		// Set Title
		$activeSheet->setCellValue('B9', $data['judul']);
		$activeSheet->mergeCells('B9:H9');
		$activeSheet->getStyle('B9')->getFont()->setBold(true)->setSize(14);
		$activeSheet->getStyle('B9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$activeSheet->setCellValue('B10', "NO");
		$activeSheet->setCellValue('C10', "TANGGAL");
		$activeSheet->setCellValue('D10', "NO VALIDASI");
		$activeSheet->setCellValue('E10', "NO BUKTI");
		$activeSheet->setCellValue('F10', "NO. AKUN");
		$activeSheet->setCellValue('G10', "DEBIT");
		$activeSheet->setCellValue('H10', "KREDIT");
		$activeSheet->setCellValue('I10', "SALDO");
		$activeSheet->setCellValue('J10', "KETERANGAN");
		$activeSheet->setCellValue('K10', "ASAL JURNAL");
		$activeSheet->getStyle('B10:K10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("B10:K10")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
	  'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK,
					)
				),
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Calibri'
				)
			)
		);

		$x = 10;
		$debet = 0;
		$kredit= 0;
		$no=0;
		$tmp_idakun     		='';
		$tmp_saldo     		= 0;
		$saldo_akhir=0;
        if (COUNT($row_detail)) {
            foreach ($row_detail as $r) {
				$x = $x+1;
				$no =$no+1;
				if ($tmp_idakun!=$r->idakun){
					$tmp_idakun=$r->idakun;
					$q="SELECT get_saldo_awal_akun(".$r->idakun.",'".YMDFormat($data['tanggal_trx1'])."',1) as saldo";
					$tmp_saldo=$this->db->query($q)->row('saldo');
				}
				// if ($r->possaldo=='DB'){
					// $tmp_saldo=(float)$tmp_saldo + (float)$r->debet - (float)$r->kredit;
				// }else{
					// $tmp_saldo=(int)$tmp_saldo + (int)$r->kredit - (int)$r->debet;					
				// }
				if ($r->possaldo=='DB'){
					$tmp_saldo=floatval($tmp_saldo) + (float)$r->debet - (float)$r->kredit;
				}else{
					$tmp_saldo=floatval($tmp_saldo) + floatval($r->kredit) - floatval($r->debet);					
				}
				$saldo_akhir=(string)$tmp_saldo;	
	            $activeSheet->setCellValue("B$x", number_format($no));
	            $activeSheet->setCellValue("C$x", HumanDateShort($r->tanggal));
	            $activeSheet->setCellValue("D$x", $r->nojurnal);
	            $activeSheet->setCellValue("E$x", $r->notransaksi);
	            $activeSheet->setCellValue("F$x", $r->noakun.' - '.$r->namaakun);
	            $activeSheet->setCellValue("G$x", $r->debet);
	            $activeSheet->setCellValue("H$x", $r->kredit);
	            $activeSheet->setCellValue("I$x", $saldo_akhir);
	            $activeSheet->setCellValue("J$x", $r->keterangan);
	            $activeSheet->setCellValue("K$x", $r->asal);
				
	           
				
				
			}
		}
		// $x = $x+1;
		$activeSheet->getStyle("G10:I$x")->getNumberFormat()->applyFromArray(
        array('code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1));
		
		$activeSheet->getStyle("C11:E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("B11:B$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		// $activeSheet->getStyle("E11:E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$activeSheet->getStyle("B10:K$x")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
					)
				)
			)
		);
		
    	// Set Auto Width Column
    	$activeSheet->getColumnDimension("A")->setWidth(5);
    	$activeSheet->getColumnDimension("C")->setWidth(20);
    	$activeSheet->getColumnDimension("B")->setAutoSize(true);
    	// $activeSheet->getColumnDimension("C")->setAutoSize(true);
    	$activeSheet->getColumnDimension("D")->setAutoSize(true);
    	$activeSheet->getColumnDimension("E")->setAutoSize(true);
    	$activeSheet->getColumnDimension("F")->setAutoSize(true);
    	$activeSheet->getColumnDimension("G")->setAutoSize(true);
    	$activeSheet->getColumnDimension("H")->setAutoSize(true);
    	$activeSheet->getColumnDimension("I")->setAutoSize(true);
    	$activeSheet->getColumnDimension("J")->setAutoSize(true);
    	$activeSheet->getColumnDimension("K")->setAutoSize(true);
    	// $activeSheet->getColumnDimension("H")->setWidth(45);
    	// $activeSheet->getColumnDimension("H")->setAutoSize(true);

    	header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    	header('Content-Disposition: attachment;filename="'.$data['judul'].' '.date('Y-m-d-h-i-s').'.xls"');
    	header('Cache-Control: max-age=0');

    	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    	$objWriter->save('php://output');
	}
	
}
?>