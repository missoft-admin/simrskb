<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tbagi_hasil extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbagi_hasil_model','model');
		$this->load->model('Mbagi_hasil_model');
	}

	function index() {
		$this->model->generate_ausansi_pending();//GENERARATE PENDING
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		$tgl_terakhir = date('Y-m-d', strtotime($hari_ini));
		$data=array(
			'status'=>'#',
			'mbagi_hasil_id'=>'#',
			'deskripsi'=>'',
			'no_terima'=>'',
			'tanggal_tagihan1'=>'',
			'tanggal_tagihan2'=>'',
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_mbagi_hasil'] 	= $this->model->list_mbagi_hasil();
		$data['title'] 			= 'Bagi Hasil';
		$data['content'] 		= 'Tbagi_hasil/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Bagi Hasil Transaksi",'tBagi Hasil_transaksi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function detail($id,$disabel=''){
		
		$data=$this->model->detail($id);
		// print_r($data);exit();
		if ($data['status']=='1'){
			$data['disabel_edit'] 				= '';
			
		}else{
			$data['disabel_edit'] 				= 'disabled';
		}
		if ($data['status']=='3'){//SUDAH PEMBAYARAN
			$disabel='disabled';
		}
		$data['disabel'] 				= $disabel;
		$data['error'] 				= '';
		$data['list_biaya'] 	= $this->model->list_biaya();
		
		$data['title'] 				= '<h3><span class="label label-primary">Informasi Bagi Hasil</span></h3>';
		$data['content'] 			= 'Tbagi_hasil/detail_rincian';
			
		
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'), 
										array("Bagi Hasil Transaksi",'tbagi_hasil/index'), 
										array("List",'#') 
									);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		// print_r($id);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
	

		$notransaksi=$this->input->post('notransaksi');
		$deskripsi=$this->input->post('deskripsi');
		$mbagi_hasil_id=$this->input->post('mbagi_hasil_id');
		$status=$this->input->post('status');
		
		$tanggal_tagihan1=$this->input->post('tanggal_tagihan1');
		$tanggal_tagihan2=$this->input->post('tanggal_tagihan2');
		
		$where='';
		if ($notransaksi !=''){
			$where .=" AND H.notransaksi='$notransaksi' ";
		}
		if ($deskripsi !=''){
			$where .=" AND H.deskripsi LIKE '%".$deskripsi."%' ";
		}
		if ($mbagi_hasil_id !='#'){
			$where .=" AND H.mbagi_hasil_id='$mbagi_hasil_id' ";
		}
		if ('' != $tanggal_tagihan1) {
            $where .= " AND DATE(H.tanggal) >='".YMDFormat($tanggal_tagihan1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_tagihan2)."'";
        }
		
		if ($status !='#'){
			$where .=" AND H.status='$status' ";
		}


        $from = "(
					SELECT H.id,H.notransaksi,H.mbagi_hasil_id,H.nama_bagi_hasil,H.deskripsi,H.tanggal_tagihan,H.total_pendapatan,H.`status`,H.status_stop 
					FROM tbagi_hasil H
					WHERE H.`status` !='0' ".$where."
				) as tbl ";


		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nama_bagi_hasil','deskripsi','notransaksi');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $url        = site_url('tbagi_hasil/');
            
            $row[] = $r->id;
            $row[] = $no;
            $row[] = $r->notransaksi;
            $row[] = $r->nama_bagi_hasil;
            $row[] = $r->deskripsi;
            $row[] = HumanDateShort($r->tanggal_tagihan);
            $row[] = number_format($r->total_pendapatan,0);
				$status_stop="";
			if ($r->status_stop=='1' && $r->status=='1'){
				$status_stop='<br><br><span class="label label-danger" data-toggle="tooltip">Transaksi distop</span>';
			}	
            $row[] = status_bagi_hasil($r->status).$status_stop;
				if ($r->status !='0'){
					$aksi .= '<a href="'.site_url().'tbagi_hasil/detail/'.$r->id.'"  target="_blank" data-toggle="tooltip" title="Detail" class="btn btn-primary btn-xs"><i class="fa fa-list-ul"></i></a>';
					
					$aksi .= '<a href="'.site_url().'mbiaya_operasional/create" target="_blank" data-toggle="tooltip" title="Tambah Biaya Operasional" class="btn btn-warning btn-xs"><i class="fa fa-plus-square"></i></a>';
					if ($r->status =='1'){
						if ($r->status_stop=='1'){
							$aksi .= '<button title="Klik Untuk start" class="btn btn-primary btn-xs play" onclick="start('.$r->id.',0)"><i class="si si-control-play"></i></button>';
						}else{
							$aksi .= '<button title="Klik untuk stop" class="btn btn-danger btn-xs stop" onclick="stop('.$r->id.',1)"><i class="fa fa-stop"></i></button>';
						}
					}
				}
					$aksi .= '<button title="Print" class="btn btn-success btn-xs"><i class="fa fa-print"></i></button>';
			$aksi.='</div>';
            $row[] = $aksi;//8            
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_detail(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
	

		$id=$this->input->post('id');
		
				
		$no_reg=$this->input->post('no_reg');
		$jenis_transaksi_id=$this->input->post('jenis_transaksi_id');
		$namatarif=$this->input->post('namatarif');
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$tgl_trx=$this->input->post('tgl_trx');
		$tgl_trx2=$this->input->post('tgl_trx2');
		$disabel=$this->input->post('disabel');
		$where='';
		$where1='';
		if ('' != $tgl_trx) {
            $where .= " AND DATE(H.tanggal) >='".YMDFormat($tgl_trx)."' AND DATE(H.tanggal) <='".YMDFormat($tgl_trx)."'";
        }
		
		if ($jenis_transaksi_id !='#'){
			$where .=" AND H.jenis_transaksi_id='$jenis_transaksi_id' ";
		}
		if ($no_reg !=''){
			$where1 .=" AND no_reg='$no_reg' ";
		}
		if ($no_medrec !=''){
			$where1 .=" AND no_medrec='$no_medrec' ";
		}
		if ($nama_pasien !=''){
			$where1 .=" AND namapasien LIKE '%".$nama_pasien."%' ";
		}
		if ($namatarif !=''){
			$where .=" AND H.namatarif  LIKE '%".$namatarif."%' ";
		}
		// if ($deskripsi !=''){
			// $where .=" AND H.deskripsi LIKE '%".$deskripsi."%' ";
		// }
		// if ($mbagi_hasil_id !='#'){
			// $where .=" AND H.mbagi_hasil_id='$mbagi_hasil_id' ";
		// }
		
		
		// if ($status !='#'){
			// $where .=" AND H.status='$status' ";
		// }


        $from = "(
					SELECT  H.id,
						CASE WHEN H.jenis_transaksi_id='1' THEN RJ.nopendaftaran ELSE RI.nopendaftaran END as no_reg,H.jenis_transaksi_id
						,CASE WHEN H.jenis_transaksi_id='1' THEN RJ.no_medrec ELSE RI.no_medrec END as no_medrec
						,CASE WHEN H.jenis_transaksi_id='1' THEN RJ.namapasien ELSE RI.namapasien END as namapasien
						,H.tanggal,H.namatarif,H.total_pendapatan,H.namakelompok,I.tanggal_tagihan
						,CASE WHEN H.idkelompok='5' THEN H.namakelompok ELSE H.namarekanan END as nama_rekanan,H.status,I.status as status_proses
						,K.no_klaim,H.kuantitas
						FROM tbagi_hasil_detail H
						LEFT JOIN tbagi_hasil I ON I.id=H.tbagi_hasil_id
						LEFT JOIN tpoliklinik_pendaftaran RJ ON RJ.id=H.idtransaksi AND H.jenis_transaksi_id='1'
						LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.idtransaksi AND H.jenis_transaksi_id='2'
						LEFT JOIN tklaim K ON K.id=H.tklaim_id
						WHERE H.tbagi_hasil_id='$id' ".$where."
				) as tbl WHERE id is not null  ".$where1." ORDER BY tanggal ASC";


		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('namapasien','no_medrec','namatarif','nama_rekanan');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $url        = site_url('tbagi_hasil/');
            $status_pending="";
			if ($r->status=='2'){
				$status_pending='<br><br><span class="label label-danger" data-toggle="tooltip">PENDING</span>';
			}	
			$no_klaim='';
			if ($r->no_klaim){
				$no_klaim='<span class="label label-danger">'.$r->no_klaim.'</span>';
			}
            $row[] = $r->id;
            $row[] = $no;
            $row[] = $r->no_reg;
            $row[] = get_tipe_layanan($r->jenis_transaksi_id);
            $row[] = HumanDateShort($r->tanggal);
            $row[] = $r->no_medrec.' - '.$r->namapasien;
            $row[] = $r->namatarif;
            $row[] = number_format($r->kuantitas,0);
            $row[] = number_format($r->total_pendapatan,0);
            $row[] = $r->nama_rekanan.' '.$no_klaim;
            $row[] = HumanDateShort($r->tanggal_tagihan).$status_pending;
			if ($r->status_proses=='1'){
				$aksi .= '<button target="_blank" data-toggle="tooltip" title="Pindah Tanggal" class="btn btn-success btn-xs" onclick="edit_tanggal('.$r->id.')"><i class="fa fa-calendar-check-o"></i> Pindah Tanggal</button>';
				
			}else{
				$aksi .= '<button class="btn btn-danger btn-xs">Sudah Diproses</button>';
			}
            
			$aksi.='</div>';
            $row[] = $aksi;//8            
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_biaya(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
	

		$id=$this->input->post('id');
		$disabel=$this->input->post('disabel');
		
        $from = "(
					SELECT T.id,T.idbiaya,T.tanggal_beli,M.nama as nama_biaya,T.nominal,T.keterangan,T.`status` FROM `tbagi_hasil_biaya` T
					LEFT JOIN mbiaya_operasional M ON M.id=T.idbiaya
					WHERE T.tbagi_hasil_id='$id' AND T.status='1'
				) as tbl  ORDER BY id ASC";


		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('namapasien','no_medrec','namatarif','nama_rekanan');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }
		
		$total_biaya=0;

        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $url        = site_url('tbagi_hasil/');
            $total_biaya=$total_biaya + $r->nominal;
            $row[] = $r->id;
            $row[] = $r->idbiaya;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggal_beli);
            $row[] = $r->nama_biaya;
            $row[] = number_format($r->nominal,0);
            $row[] = $r->keterangan;
            
			$aksi .= '<button type="button" data-toggle="tooltip" '.$disabel.' title="Edit" class="btn btn-success btn-xs edit_biaya"><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button type="button" data-toggle="tooltip" '.$disabel.' title="Edit" class="btn btn-danger btn-xs" onclick="hapus_biaya('.$r->id.')"><i class="fa fa-trash-o"></i></button>';
            
			$aksi.='</div>';
            $row[] = $aksi;//8            
            $data[] = $row;
        }
			$row = array();
			$row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '<strong>TOTAL BIAYA OPERASIONAL</strong>';
            $row[] ='<strong>'. number_format($total_biaya,0).'</strong>';
            $row[] = '';
            $row[] = '';         
			 
            $data[] = $row;
        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	public function update_start_stop()
    {
		$id=$this->input->post('id');
		$val=$this->input->post('val');
		$this->db->where('id',$id);
		$result=$this->db->update('tbagi_hasil',array('status_stop'=>$val));
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	function refresh_biaya(){
		
		$q="SELECT * FROM mbiaya_operasional M
			WHERE M.`status`='1'
			ORDER BY M.nama ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function get_estimasi(){
		$idbiaya=$this->input->post('idbiaya');
		$q="SELECT M.estimasi FROM mbiaya_operasional M
			WHERE M.`id`='$idbiaya'";
		$row=$this->db->query($q)->row('estimasi');
		
		$arr['detail']=$row;
		$this->output->set_output(json_encode($arr));
	}
	function get_info(){
		$id=$this->input->post('id');
		$q="SELECT H.total_pendapatan,H.total_biaya,H.bagian_rs_persen,H.bagian_ps_persen,H.nominal_rs,H.nominal_ps 
			,SUM(B.nominal) total_biaya_detail,H.tanggungan_biaya,M.jumlah_lembar,H.pendapatan_per_lembar
			From tbagi_hasil H
			LEFT JOIN tbagi_hasil_biaya B ON B.tbagi_hasil_id=H.id AND B.`status`='1'
			LEFT JOIN (SELECT P.mbagi_hasil_id,SUM(P.jumlah_lembar) as jumlah_lembar FROM mbagi_hasil_pemilik_saham P WHERE P.`status`='1' GROUP BY P.mbagi_hasil_id) M ON M.mbagi_hasil_id=H.mbagi_hasil_id 
			WHERE H.id='$id'
			GROUP BY H.id";
		$row=$this->db->query($q)->row_array();
		
		// $arr['detail']=$row;
		$this->output->set_output(json_encode($row));
	}
	function simpan_biaya(){
		// data: {tbagi_hasil_id: tbagi_hasil_id,tanggal_beli: tanggal_beli,idbiaya: idbiaya,nominal: nominal,keterangan: keterangan,id_edit: id_edit},
		$tbagi_hasil_id=$this->input->post('tbagi_hasil_id');
		$tanggal_beli=YMDFormat($this->input->post('tanggal_beli'));
		$idbiaya=$this->input->post('idbiaya');
		$keterangan=$this->input->post('keterangan');
		$id_edit=$this->input->post('id_edit');
		$nominal=RemoveComma($this->input->post('nominal'));
		$data=array(
			'tbagi_hasil_id' =>$tbagi_hasil_id,
			'tanggal_beli' =>$tanggal_beli,
			'idbiaya' =>$idbiaya,
			'keterangan' =>$keterangan,			
			'nominal' =>$nominal,
			
			'created_by' =>$this->session->userdata('user_id'),
			'created_date' =>date('Y-m-d H:i:s'),
		);
		if ($id_edit==''){
			$result=$this->db->insert('tbagi_hasil_biaya', $data);	
		}else{
			$this->db->where('id', $id_edit);
			$result=$this->db->update('tbagi_hasil_biaya', $data);		
		}
		
		$this->output->set_output(json_encode($result));
	}
	function simpan_rekap(){
		// ,bagian_ps_persen: bagian_ps_persen,nominal_ps: nominal_ps,nominal_rs: nominal_rs,total_biaya: total_biaya,bagian_rs_persen: 
					// ,tanggungan_biaya: tanggungan_biaya
					// ,pembagian_bersih: pembagian_bersih
					// ,pendapatan_bersih_rs: pendapatan_bersih_rs
					// ,pendapatan_bersih_ps: pendapatan_bersih_ps
					// ,jumlah_lembar: jumlah_lembar
					// ,pendapatan_per_lembar: pendapatan_per_lembar				
		$tbagi_hasil_id=$this->input->post('tbagi_hasil_id');
		$bagian_ps_persen=RemoveComma($this->input->post('bagian_ps_persen'));
		$bagian_rs_persen=RemoveComma($this->input->post('bagian_rs_persen'));
		$nominal_rs=RemoveComma($this->input->post('nominal_rs'));
		$nominal_ps=RemoveComma($this->input->post('nominal_ps'));
		$total_biaya=RemoveComma($this->input->post('total_biaya'));
		$tanggungan_biaya=RemoveComma($this->input->post('tanggungan_biaya'));
		$pembagian_bersih=RemoveComma($this->input->post('pembagian_bersih'));
		$pendapatan_bersih_rs=RemoveComma($this->input->post('pendapatan_bersih_rs'));
		$pendapatan_bersih_ps=RemoveComma($this->input->post('pendapatan_bersih_ps'));
		$jumlah_lembar=RemoveComma($this->input->post('jumlah_lembar'));
		$pendapatan_per_lembar=RemoveComma($this->input->post('pendapatan_per_lembar'));
		
		$data=array(
			'bagian_ps_persen' =>$bagian_ps_persen,
			'bagian_rs_persen' =>$bagian_rs_persen,
			'nominal_rs' =>$nominal_rs,
			'nominal_ps' =>$nominal_ps,
			'total_biaya' =>$total_biaya,
			'tanggungan_biaya' =>$tanggungan_biaya,
			'pembagian_bersih' =>$pembagian_bersih,
			'pendapatan_bersih_rs' =>$pendapatan_bersih_rs,
			'pendapatan_bersih_ps' =>$pendapatan_bersih_ps,
			'jumlah_lembar' =>$jumlah_lembar,
			'pendapatan_per_lembar' =>$pendapatan_per_lembar,
			'status' =>2,
			'status_stop' =>1,
			
		);
	
		$this->db->where('id', $tbagi_hasil_id);
		$result=$this->db->update('tbagi_hasil', $data);		
		
		$q="SELECT M.id_pemilik_saham,M.jumlah_lembar
				,CASE WHEN P.tipe_pemilik='1' THEN MP.nama WHEN P.tipe_pemilik='2' THEN MD.nama ELSE P.nama END as nama_pemilik,P.tipe_pemilik 
				FROM tbagi_hasil T
				LEFT JOIN mbagi_hasil_pemilik_saham M ON M.mbagi_hasil_id=T.mbagi_hasil_id
				LEFT JOIN mpemilik_saham P ON P.id=M.id_pemilik_saham 
				LEFT JOIN mpegawai MP ON MP.id=P.idpeg_dok
				LEFT JOIN mdokter MD ON MD.id=P.idpeg_dok
				WHERE T.id='$tbagi_hasil_id'";
		$row=$this->db->query($q)->result();
		$this->db->where('tbagi_hasil_id',$tbagi_hasil_id);
		$this->db->delete('tbagi_hasil_pemilik_saham');
		foreach($row as $r){
			$data=array(
				'tbagi_hasil_id' =>$tbagi_hasil_id,
				'id_pemilik_saham' =>$r->id_pemilik_saham,
				'tipe_pemilik' =>$r->tipe_pemilik,
				'jumlah_lembar' =>$r->jumlah_lembar,
				'nama_pemilik' =>$r->nama_pemilik,
				'pendapatan_per_lembar' =>$pendapatan_per_lembar,
				'total_pendapatan' =>$pendapatan_per_lembar *$r->jumlah_lembar ,
			
			);
			$result=$this->db->insert('tbagi_hasil_pemilik_saham', $data);	
		}
		
		$this->output->set_output(json_encode($result));
	}
	public function hapus_biaya()
    {
        $id = $this->input->post('id');

		$data =array(
            'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_date'=>date('Y-m-d H:i:s'),


        );
		$this->db->where('id',$id);;
		$result=$this->db->update('tbagi_hasil_biaya',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	function list_tanggal_by_tangggal_id($id){

		$q="SELECT * FROM (
			SELECT DATE(DT.tanggal) as tanggal_next,T.tanggal_tagihan 
			FROM date_row DT 
			LEFT JOIN tbagi_hasil T ON T.tanggal_tagihan = DT.tanggal AND  T.mbagi_hasil_id=1 AND (T.`status` > 1 OR T.status_stop='1')
			WHERE DT.tanggal>=CURRENT_DATE 
			AND DAY(DT.tanggal) IN (SELECT S.tanggal_hari FROM mbagi_hasil_tanggal S WHERE S.`status`='1' AND S.mbagi_hasil_id=$id) 
			AND DT.tanggal <= DATE_ADD(CURRENT_DATE,INTERVAL 6 MONTH)
			) H WHERE H.tanggal_tagihan IS NULL";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->tanggal_next.'">'.HumanDateShort($r->tanggal_next).'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function pindah_tanggal(){
		$id=$this->input->post('id');
		$tanggal=$this->input->post('tanggal');
		// $tanggal='2021-08-14';
		$q="SELECT H.mbagi_hasil_id,M.nama as nama_bagi_hasil,M.keterangan as deskripsi,M.bagian_rs,M.bagian_ps,T.id as tbagi_hasil_id  from tbagi_hasil_detail D
			LEFT JOIN tbagi_hasil H ON H.id=D.tbagi_hasil_id
			LEFT JOIN mbagi_hasil M ON M.id=H.mbagi_hasil_id
			LEFT JOIN tbagi_hasil T ON T.mbagi_hasil_id=H.mbagi_hasil_id AND T.tanggal_tagihan='".YMDFormat($tanggal)."' AND T.`status`=1 AND T.status_stop='0'
			WHERE D.id='$id'
			LIMIT 1
			";
		 $result_setting=$this->db->query($q)->row();
		 $tbagi_hasil_id='';
		 if ($result_setting->tbagi_hasil_id == null){//Jika Belum Ada di tbagi_hasil
			$data_header=array(
				'mbagi_hasil_id'=>$result_setting->mbagi_hasil_id,
				'nama_bagi_hasil'=>$result_setting->nama_bagi_hasil,
				'deskripsi'=>$result_setting->deskripsi,
				'tanggal_tagihan'=>YMDFormat($tanggal),
				'bagian_rs_persen'=>$result_setting->bagian_rs,
				'bagian_ps_persen'=>$result_setting->bagian_ps,
				'created_at' => date('Y-m-d H:i:s'),
				'created_by' => $this->session->userdata('user_id'),
				'status' => 1
			);
			$this->db->insert('tbagi_hasil',$data_header);
			$tbagi_hasil_id= $this->db->insert_id();
			
		}else{
			$tbagi_hasil_id=$result_setting->tbagi_hasil_id;				
		}
		$qdet="SELECT *From tbagi_hasil_detail D WHERE D.id='$id'";
		$rows=$this->db->query($qdet)->result();
		foreach ($rows as $r){
			// print_r($r);exit();
			$dataDetailBagiHasil = array(
			  'tbagi_hasil_id' => $tbagi_hasil_id,
			  'mbagi_hasil_pelayanan_id' => $r->mbagi_hasil_pelayanan_id,
			  'idtransaksi' => $r->idtransaksi,
			  'jenis_transaksi' => $r->jenis_transaksi,
			  'jenis_transaksi_id' =>$r->jenis_transaksi_id,
			  'jenis_tindakan' => $r->jenis_tindakan,
			  'reference_table' => $r->reference_table,
			  'tanggal' => YMDFormat($r->tanggal),
			  'jenis_pasien' => $r->jenis_pasien,
			  'idkelompok' => $r->idkelompok,
			  'namakelompok' => $r->namakelompok,
			  'idrekanan' => $r->idrekanan,
			  'namarekanan' => $r->namarekanan,
			  'iddetail' => $r->iddetail,
			  'idtarif' => $r->idtarif,
			  'namatarif' => $r->namatarif,
			  'kuantitas' => $r->kuantitas,
			  'status_asuransi' => $r->status_asuransi,
			  'jasasarana' => $r->jasasarana,
			  'jasapelayanan' => $r->jasapelayanan,
			  'bhp' => $r->bhp,
			  'biayaperawatan' => $r->biayaperawatan,
			  'biayaperawatan' => $r->biayaperawatan,
			  'nominal_jasasarana' => $r->nominal_jasasarana,
			  'nominal_jasapelayanan' => $r->nominal_jasapelayanan,
			  'nominal_bhp' => $r->nominal_bhp,
			  'nominal_biayaperawatan' => $r->nominal_biayaperawatan,				  
			  'status' => $r->status,//Jika 1=Langsung;2:Hold Asuransi
			  'total_pendapatan' => $r->total_pendapatan,//Jika 1=Langsung;2:Hold Asuransi
			);
			$resul=$this->db->insert('tbagi_hasil_detail', $dataDetailBagiHasil);
		}
		if ($resul){
			$this->db->where('id',$id);
			$result=$this->db->delete('tbagi_hasil_detail');
		}
		$this->output->set_output(json_encode($result));
		 // print_r($tbagi_hasil_id);exit();
	}
}
