<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_pendapatan_jual_ranap extends CI_Controller {

	/**
	 * Setting Jurnal Penjualan Obat Rawat Inap controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_pendapatan_jual_ranap_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_pendapatan_jual_ranap_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_pendapatan_jual_ranap_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Penjualan Obat Rawat Inap';
		$data['content'] 		= 'Msetting_jurnal_pendapatan_jual_ranap/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Penjualan Obat Rawat Inap",'#'),
									    			array("List",'mlogic_ranap')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function list_pegawai(){
		$idtipe=$this->input->post('idtipe');
		$idkategori=$this->input->post('idkategori');
		$where='';
		if ($idkategori !='#'){
			$where .=" AND M.idkategori='$idkategori'";
		}
		if ($idtipe=='1'){
			$q="SELECT * FROM mdokter M WHERE M.status='1' ".$where." ORDER BY M.nama ASC";
		}else{
			$q="SELECT * FROM mpegawai M  M WHERE M.status='1' ".$where." ORDER BY M.nama ASC";
		}
		
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function list_poli_kelas($idtipe){
		if ($idtipe=='1'){//Poli
			$q="SELECT * FROM mpoliklinik M WHERE M.id !='22' ORDER BY M.nama ASC";
		}elseif ($idtipe=='2'){ // RANAP
			$q="SELECT * FROM mpoliklinik M WHERE M.id ='22' ORDER BY M.nama ASC";
			// $q="SELECT * FROM mkelas M ORDER BY M.nama ASC";			
		}
		$opsi='';
		if ($idtipe !='#'){
				
			$row=$this->db->query($q)->result();
			foreach($row as $r){
				$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
			}
			
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	
	
	//Penjualan Obat Rawat Inap
	function load_ranap()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.*
					,MR.nama as ruangan,MK.nama as kelas
					 ,K.nama as kategori,T.nama_tipe,B.nama as namabarang
					 ,GH1.nama as nama_group_pembelian
					 ,GH2.nama as nama_group_diskon
					FROM msetting_jurnal_pendapatan_jual_ranap H
					
					LEFT JOIN mkelas MK ON MK.id=H.idkelas 
					LEFT JOIN mruangan MR ON MR.id=H.idruangan 
					LEFT JOIN mdata_kategori K ON K.id=H.idkategori
					LEFT JOIN mdata_tipebarang T ON T.id=H.idtipe
					LEFT JOIN view_barang_all B ON B.id=H.idbarang AND B.idtipe=H.idtipe
					LEFT JOIN mgroup_pembayaran GH1 ON GH1.id=H.idgroup_penjualan
					LEFT JOIN mgroup_pembayaran GH2 ON GH2.id=H.idgroup_diskon
					
					GROUP BY H.id
					ORDER BY H.idruangan,H.idtipe,H.idkategori,H.idbarang

				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = ($r->ruangan);
            $row[] = ($r->idkelas=='0'?text_default('All kelas'):$r->kelas);
            $row[] = ($r->idtipe=='0'?text_default('All Tipe'):$r->nama_tipe);
            $row[] = ($r->idkategori=='0'?text_default('All kategori'):$r->kategori);
            $row[] = ($r->idbarang=='0'?text_default('All Barang'):$r->namabarang);
            $row[] = $r->nama_group_pembelian;
            $row[] = $r->nama_group_diskon;
			$aksi 		= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_ranap('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_ranap(){
		$id_edit=$this->input->post('id_edit');
		$idruangan=($this->input->post('idruangan')=='#'?0:$this->input->post('idruangan'));
		$idkelas=($this->input->post('idkelas')=='#'?0:$this->input->post('idkelas'));
		$idtipe=($this->input->post('idtipe')=='#'?0:$this->input->post('idtipe'));
		$idkategori=($this->input->post('idkategori')=='#'?0:$this->input->post('idkategori'));
		$idbarang=($this->input->post('idbarang')=='#'?0:$this->input->post('idbarang'));
		$idgroup_penjualan=($this->input->post('idgroup_penjualan')=='#'?0:$this->input->post('idgroup_penjualan'));
		$idgroup_diskon=($this->input->post('idgroup_diskon')=='#'?0:$this->input->post('idgroup_diskon'));
		$data=array(
			'setting_id'=>1,
			'idruangan'=>$idruangan,
			'idkelas'=>$idkelas,
			'idtipe'=>$idtipe,
			'idkategori'=>$idkategori,
			'idbarang'=>$idbarang,
			'idgroup_penjualan'=>$idgroup_penjualan,
			'idgroup_diskon'=>$idgroup_diskon,
		);
		
		if ($this->cek_duplicate_ranap($idruangan,$idkelas,$idtipe,$idkategori,$idbarang)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pendapatan_jual_ranap',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_ranap($idruangan,$idkelas,$idtipe,$idkategori,$idbarang){
		$gabung=$idruangan.'-'.$idkelas.'-'.$idtipe.'-'.$idkategori.'-'.$idbarang;
		$q="SELECT *FROM msetting_jurnal_pendapatan_jual_ranap S
			WHERE CONCAT(S.idruangan,'-',S.idkelas,'-',S.idtipe,'-',S.idkategori,'-',S.idbarang)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_ranap($id)
	{		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_pendapatan_jual_ranap');
		echo json_encode($result);
	}
	
}
