<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tunitpermintaan extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('tunitpermintaan_model', 'model');
    }

    public function index() {
        $data['error'] = '';
        $data['title'] = 'Permintaan Unit';
        $data['content'] = 'Tunitpermintaan/index';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Permintaan Unit", '#'),
            array("List", 'tunitpermintaan')
        );
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function add() {
        $data['error']      = '';
        $data['title']      = 'Tambah Permintaan';
        $data['content'] 	= 'Tunitpermintaan/manage';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Permintaan Unit", '#'),
            array("List", 'tunitpermintaan')
        );
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function acc_pengiriman() {
        $data['error']      = '';
        $data['title']      = 'Konfirmassi Pengiriman';
        $data['content'] 	= 'Tunitpermintaan/acc_pengiriman';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Permintaan Unit", '#'),
            array("List", 'tunitpermintaan')
        );
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function unit_penerimaan() {
        $data['error']      = '';
        $data['title']      = 'Unit Penerimaan';
        $data['content']    = 'Tunitpermintaan/unit_penerimaan';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Permintaan Unit", '#'),
            array("List", 'tunitpermintaan')
        );
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function view_detail() {
        $data['error']      = '';
        $data['title']      = 'Lihat Detail';
        $data['content']    = 'Tunitpermintaan/view_detail';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Permintaan Unit", '#'),
            array("List", 'tunitpermintaan')
        );
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function save(){
        $this->form_validation->set_rules('detailvalue', 'Detail', 'trim|required|min_length[10]');
        if($this->form_validation->run() == true) {
            if($this->model->save()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
                redirect('tunitpermintaan/index','refresh');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tunitpermintaan/index','refresh');
        }
    }

    public function save_acc_pengiriman() {
        $this->form_validation->set_rules('id[]', 'ID', 'trim|required');
        $this->form_validation->set_rules('kuantitaskirim[]', 'Kuantitas Kirim', 'trim|required');
        if($this->form_validation->run() == true) {
            if($this->model->save_acc_pengiriman()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
                redirect('tunitpermintaan/index','refresh');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tunitpermintaan/index','refresh');
        }
    }

    public function save_pengalihan_permintaan() {
        if($this->model->save_pengalihan_permintaan()) {
            $data['status'] = '200';
        } else {
            $data['status'] = '200';
        }
        $this->output->set_output(json_encode($data));
    }

    public function save_penerimaan() {
        $this->form_validation->set_rules('idbarang[]', 'Barang', 'trim|required');
        $this->form_validation->set_rules('idtipe[]', 'Tipe', 'trim|required');
        $this->form_validation->set_rules('kuantitasterima[]', 'Kuantitas Kirim', 'trim|required');
        if($this->form_validation->run() == true) {
            if($this->model->save_penerimaan()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
                redirect('tunitpermintaan/index','refresh');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tunitpermintaan/index','refresh');
        }
    }


    // additional
    public function dtpermintaan() {
        $this->model->dtpermintaan();
    }
    public function dtpermintaan_detail() {
        $this->model->dtpermintaan_detail();
    }
    public function dtpermintaan_detail_penerimaan() {
        $this->model->dtpermintaan_detail_penerimaan();
    }
    public function dtbarang() {
        $this->model->dtbarang();
    }
    public function ajax_stokunit() {
        $this->model->ajax_stokunit();
    }
    public function ajax_namabarang() {
        $this->model->ajax_namabarang();
    }
    public function ajax_pernah_terima() {
        $this->model->ajax_pernah_terima();
    }
    public function selectdariunit() {
    	$this->model->selectdariunit();
    }
    public function selectkeunit() {
        $this->model->selectkeunit();
    }
    public function selectdariunit_alihan() {
        $this->model->selectdariunit_alihan();
    }
}

/* End of file Tunitpermintaan.php */
/* Location: ./application/controllers/Tunitpermintaan.php */
