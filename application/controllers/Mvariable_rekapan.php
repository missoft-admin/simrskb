<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mvariable_rekapan extends CI_Controller {

	/**
	 * Variable Rekapan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mvariable_rekapan_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Variable Rekapan';
		$data['content'] 		= 'Mvariable_rekapan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Variable Rekapan",'mvariable_rekapan/index'),
														array("List",'#')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 							=> '',
			'nama' 						=> '',
			'idtipe' 					=> '1',
			'idsub' 					=> '0',
			'status' 					=> '',
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Variable Rekapan';
		$data['content'] 		= 'Mvariable_rekapan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Variable Rekapan",'#'),
									    			array("Tambah",'mvariable_rekapan')
													);

		$data['list_sub'] 	= array();
		$data['list_akun_pengeluaran_debit'] 	= array();
		$data['list_akun_pengeluaran_kredit'] = array();
		$data['list_akun_pendapatan_debit'] 	= array();
		$data['list_akun_pendapatan_kredit'] 	= array();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mvariable_rekapan_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 							 => $row->id,
					'nama' 						 => $row->nama,
					'idtipe' 					 => $row->idtipe,
					'idsub' 					 => $row->idsub,
					'status' 					 => $row->status,
				);

				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Variable Rekapan';
				$data['content']	 	= 'Mvariable_rekapan/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Variable Rekapan",'#'),
											    			array("Ubah",'mvariable_rekapan')
															);

				$data['list_sub'] 	= $this->Mvariable_rekapan_model->getSubData($id);
				$data['list_akun_pengeluaran_debit'] 	= $this->Mvariable_rekapan_model->getAkunData($id, 'D', '1');
				$data['list_akun_pengeluaran_kredit'] = $this->Mvariable_rekapan_model->getAkunData($id, 'K', '1');
				$data['list_akun_pendapatan_debit'] 	= $this->Mvariable_rekapan_model->getAkunData($id, 'D', '2');
				$data['list_akun_pendapatan_kredit'] 	= $this->Mvariable_rekapan_model->getAkunData($id, 'K', '2');

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mvariable_rekapan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mvariable_rekapan','location');
		}
	}

	function delete($id){
		$this->Mvariable_rekapan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mvariable_rekapan','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mvariable_rekapan_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mvariable_rekapan/index/'.$this->input->post('idtipe'),'location');
				}
			} else {
				if($this->Mvariable_rekapan_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mvariable_rekapan/index/'.$this->input->post('idtipe'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 			 = validation_errors();
		$data['content'] 		 = 'Mvariable_rekapan/manage';

		if($id==''){
			$data['title'] = 'Tambah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Tambah",'mvariable_rekapan')
													);
		}else{
			$data['title'] = 'Ubah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Ubah",'mvariable_rekapan')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
	{
			$this->select = array();
			$this->from   = 'mvariable_rekapan';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'id' => 'ASC'
			);
			$this->group  = array();

			$this->column_search   = array('nama');
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->nama;
					$row[] = ($r->idtipe == 1 ? 'Pengeluaran' : 'Pengeluaran + Pendapatan');
					$row[] = ($r->idsub == 1 ? 'Sub' : 'Non Sub');
					$aksi = '<div class="btn-group">';
					// if(button_roles('mvariable_rekapan/update')) {
						$aksi .= '<a href="'.site_url().'mvariable_rekapan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
					// }
					// if(button_roles('mvariable_rekapan/delete')) {
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mvariable_rekapan" data-urlremove="'.site_url().'mvariable_rekapan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
					// }
					$aksi .= '</div>';
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
}
