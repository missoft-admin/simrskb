<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpoliklinik_trx extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpoliklinik_trx_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Tpendaftaran_poli_model');
		$this->load->helper('path');
		
  }
	function get_head_parent($path){
		// $path='14.1.11';
		$arr=explode(".",$path);
		return $arr[0];
	}
	function tindakan($pendaftaran_id='',$st_ranap='0',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		$data_login_ppa=get_ppa_login();
		
		$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id,$st_ranap);
		
		$data['tittle']='Penggunaan BMHP';
		
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tkonsul/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		
		$data['idpoli'] 			= '#';
		
		$data['error'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'Tindakan';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Poliklinik Tindakan ",'#'),
											  array("Input BMHP",'tpoliklinik_trx')
											);
		
		$data['tanggal_transaksi']=date('d-m-Y H:i:s');
		// $data['pendaftaran_id']=$pendaftaran_id;
			// print_r($data);exit;
		if ($menu_kiri=='input_layanan_ri'){
			if ($data_login_ppa['login_profesi_id']=='1'){//Jika Profesi Dokter
				$profesi_id=$data_login_ppa['login_profesi_id'];
				$iddokter_ppa=$data['iddokter'];
				$mppa_id=$data_login_ppa['login_ppa_id'];
				// $mppa_id=
			}else{
				$profesi_id=1;	
				$q="SELECT *FROM mppa H WHERE  H.tipepegawai='2' AND pegawai_id='".$data['iddokter']."'";
			// print_r($data_login_ppa);exit;
				$data_ppa=$this->db->query($q)->row();
				if ($data_ppa){
					$mppa_id=$data_ppa->id;
					$iddokter_ppa=$data_ppa->pegawai_id;
				}else{
					$mppa_id='';
					$iddokter_ppa='';
				}
			}
			$data['jenis_tindakan'] = 0;
			$data['mppa_id'] = $mppa_id;
			$data['iddokter_ppa'] = $iddokter_ppa;
			$data['profesi_id'] = $profesi_id;
			$data['list_ppa'] = $this->Tpoliklinik_trx_model->list_ppa($profesi_id);
			$data['list_ppa_all'] = $this->Tpoliklinik_trx_model->list_ppa_all();
			// print_r($data['idtipe_poli'].$data['idkelompokpasien'].$data['idrekanan']);exit;
			// $data['list_header_path'] = $this->Tpoliklinik_trx_model->list_header_path($data['idtipe_poli'],$data['idkelompokpasien'],$data['idrekanan']);
			$data['list_header_path'] = array();
			// print_r($data['list_header_path']);exit;
			$data_paket=$this->Tpoliklinik_trx_model->data_paket_layanan();
			if ($data_paket){}else{
				$data_paket=array(
					'paket_id' =>'',
					'nama_paket' =>'',
					'status_paket' =>'0',
					'st_edited' =>'0',
				);
			}
			$data_setting=$this->db->query("SELECT H.st_duplikate as st_duplikate_visit
											,H.st_ruangan as st_ruangan_visit,H.st_kelas as st_kelas_visit,H.st_diskon as st_diskon_visit,H.st_kuantitas as st_kuantitas
											FROM setting_pelayanan_ri H WHERE H.id='1'")->row_array();
			$data = array_merge($data,$data_setting,$data_paket);
		}
		if ($menu_kiri=='input_bmhp_ri'){
			$data['idtipe']=3;
			$data['idunit_default'] = $this->Tpoliklinik_trx_model->getDefaultUnitPelayananUser();
			$data['list_unitpelayanan'] = $this->Tpoliklinik_trx_model->getListUnitPelayanan();
			$data['list_tipe'] = $this->Tpoliklinik_trx_model->getListTipeAll();
			$data_paket=$this->Tpoliklinik_trx_model->data_paket();
			if ($data_paket){}else{
				$data_paket=array(
					'paket_id' =>'',
					'nama_paket' =>'',
					'status_paket' =>'0',
					'st_edited' =>'0',
				);
			}
			$data = array_merge($data,$data_paket);
		}
		if ($menu_kiri=='input_visit'){
			
			if ($data_login_ppa['login_profesi_id']=='1'){//Jika Profesi Dokter
				$profesi_id=$data_login_ppa['login_profesi_id'];
				$iddokter_ppa=$data['iddokter'];
				$mppa_id=$data_login_ppa['login_ppa_id'];
				// $mppa_id=
			}else{
				$profesi_id=1;	
				$q="SELECT *FROM mppa H WHERE  H.tipepegawai='2' AND pegawai_id='".$data['iddokter']."'";
			// print_r($data_login_ppa);exit;
				$data_ppa=$this->db->query($q)->row();
				if ($data_ppa){
					$mppa_id=$data_ppa->id;
					$iddokter_ppa=$data_ppa->pegawai_id;
				}else{
					$mppa_id='';
					$iddokter_ppa='';
				}
			}
			$data['mppa_id'] = $mppa_id;
			$data['iddokter_ppa'] = $iddokter_ppa;
			$data['idpelayanan'] = '';
			$data['profesi_id'] = $profesi_id;
			$data['list_ppa'] = $this->Tpoliklinik_trx_model->list_ppa($profesi_id);
			$data['list_ppa_all'] = $this->Tpoliklinik_trx_model->list_ppa_all();
			// print_r($data['idtipe_poli'].$data['idkelompokpasien'].$data['idrekanan']);exit;
			$data['list_header_path'] = $this->Tpoliklinik_trx_model->list_header_path($data['idtipe_poli'],$data['idkelompokpasien'],$data['idrekanan']);
			// print_r($data['list_header_path']);exit;
			$data_paket=$this->Tpoliklinik_trx_model->data_paket_layanan();
			if ($data_paket){}else{
				$data_paket=array(
					'paket_id' =>'',
					'nama_paket' =>'',
					'status_paket' =>'0',
					'st_edited' =>'0',
				);
			}
			$data_setting=$this->db->query("SELECT H.st_duplikate as st_duplikate_visit 
											,H.st_ruangan as st_ruangan_visit,H.st_kelas as st_kelas_visit,H.st_diskon as st_diskon_visit,H.st_kuantitas as st_kuantitas_visit
											FROM setting_visite H WHERE H.id='1'")->row_array();
			$data = array_merge($data,$data_setting,$data_paket);
		}
		
		if ($menu_kiri=='input_bmhp'){
			$data['idtipe']=3;
			$data['idunit_default'] = $this->Tpoliklinik_trx_model->getDefaultUnitPelayananUser();
			$data['list_unitpelayanan'] = $this->Tpoliklinik_trx_model->getListUnitPelayanan();
			$data['list_tipe'] = $this->Tpoliklinik_trx_model->getListTipe();
			$data_paket=$this->Tpoliklinik_trx_model->data_paket();
			if ($data_paket){}else{
				$data_paket=array(
					'paket_id' =>'',
					'nama_paket' =>'',
					'status_paket' =>'0',
					'st_edited' =>'0',
				);
			}
			$data = array_merge($data,$data_paket);
		}
		if ($menu_kiri=='input_layanan'){
			if ($data_login_ppa['login_profesi_id']=='1'){//Jika Profesi Dokter
				$profesi_id=$data_login_ppa['login_profesi_id'];
				$iddokter_ppa=$data['iddokter'];
				$mppa_id=$data_login_ppa['login_ppa_id'];
				// $mppa_id=
			}else{
				$profesi_id=1;	
				$q="SELECT *FROM mppa H WHERE  H.tipepegawai='2' AND pegawai_id='".$data['iddokter']."'";
			// print_r($data_login_ppa);exit;
				$data_ppa=$this->db->query($q)->row();
				if ($data_ppa){
					$mppa_id=$data_ppa->id;
					$iddokter_ppa=$data_ppa->pegawai_id;
				}else{
					$mppa_id='';
					$iddokter_ppa='';
				}
			}
			$data['mppa_id'] = $mppa_id;
			$data['iddokter_ppa'] = $iddokter_ppa;
			$data['profesi_id'] = $profesi_id;
			$data['list_ppa'] = $this->Tpoliklinik_trx_model->list_ppa($profesi_id);
			$data['list_ppa_all'] = $this->Tpoliklinik_trx_model->list_ppa_all();
			// print_r($data['idtipe_poli'].$data['idkelompokpasien'].$data['idrekanan']);exit;
			$data['list_header_path'] = $this->Tpoliklinik_trx_model->list_header_path($data['idtipe_poli'],$data['idkelompokpasien'],$data['idrekanan']);
			// print_r($data['list_header_path']);exit;
			$data_paket=$this->Tpoliklinik_trx_model->data_paket_layanan();
			if ($data_paket){}else{
				$data_paket=array(
					'paket_id' =>'',
					'nama_paket' =>'',
					'status_paket' =>'0',
					'st_edited' =>'0',
				);
			}
			$data = array_merge($data,$data_paket);
		}
		
			// print_r($menu_kiri);exit;
		if ($menu_kiri=='input_layanan_fisio'){
			// print_r($data['list_header_path']);exit;
			$q="SELECT H.id as idtindakan,D.id as idrujukan,D.norujukan,D.tanggal FROM tpoliklinik_tindakan H
				LEFT JOIN trujukan_fisioterapi D ON D.idtindakan=H.id
				WHERE H.idpendaftaran='$pendaftaran_id'";
			
			$data_rujukan=$this->db->query($q)->row_array();
			if ($data_rujukan){
				if ($data_rujukan['idrujukan']==''){
					$iduser=$this->session->userdata('user_id');
					$idtindakan=$data_rujukan['idtindakan'];
					$iddokter_=$data['iddokter'];
					$idasalrujukan_=$data['idtipe_poli'];
					$tanggaldaftar_=$data['tanggal'];
					$q="INSERT INTO trujukan_fisioterapi ( norujukan, tanggal, idtindakan, iddokterperujuk, asalrujukan, statusredirect, iduser_rujukan, tanggal_rujukan) 
					VALUES ( GetNoRujukan('FT', 3), NOW(), '$idtindakan', '$iddokter_', '$idasalrujukan_', 0, '$iduser', '$tanggaldaftar_')";
					$this->db->query($q);
					$q="SELECT H.id as idtindakan,D.id as idrujukan,D.norujukan FROM tpoliklinik_tindakan H
						LEFT JOIN trujukan_fisioterapi D ON D.idtindakan=H.id
						WHERE H.idpendaftaran='$pendaftaran_id'";
					
					$data_rujukan=$this->db->query($q)->row_array();
				}else{
					$idpasien=$data['idpasien'];
					$idrujukan=$data_rujukan['idrujukan'];
					$data_update=array(
						'pendaftaran_id'=>$pendaftaran_id,
						'idpasien'=>$idpasien,
					);
					
					$this->db->where('id',$idrujukan);
					$this->db->update('trujukan_fisioterapi',$data_update);
				}
			}else{
				$data_rujukan=array();
			}
			if ($data_login_ppa['login_profesi_id']=='1'){//Jika Profesi Dokter
				$profesi_id=$data_login_ppa['login_profesi_id'];
				$iddokter_ppa=$data['iddokter'];
				$mppa_id=$data_login_ppa['login_ppa_id'];
				// $mppa_id=
			}else{
				$profesi_id=1;	
				$q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='1' AND H.tipepegawai='2' AND pegawai_id='".$data['iddokter']."'";
				$data_ppa=$this->db->query($q)->row();
				if ($data_ppa){
					$mppa_id=$data_ppa->id;
					$iddokter_ppa=$data_ppa->pegawai_id;
				}else{
					$mppa_id='';
					$iddokter_ppa='';
				}
			}
			$data['mppa_id'] = $mppa_id;
			$data['iddokter_ppa'] = $iddokter_ppa;
			$data['profesi_id'] = $profesi_id;
			$data['list_ppa'] = $this->Tpoliklinik_trx_model->list_ppa($profesi_id);
			$data['list_ppa_all'] = $this->Tpoliklinik_trx_model->list_ppa_all();
			// print_r($data['idtipe_poli'].$data['idkelompokpasien'].$data['idrekanan']);exit;
			$data['list_header_path'] = $this->Tpoliklinik_trx_model->list_header_path_fisio($data['idtipe_poli'],$data['idkelompokpasien'],$data['idrekanan']);
			$data_paket=$this->Tpoliklinik_trx_model->data_paket_layanan_fisio();
			if ($data_paket){}else{
				$data_paket=array(
					'paket_id' =>'',
					'nama_paket' =>'',
					'status_paket' =>'0',
					'st_edited' =>'0',
				);
			}
			$data = array_merge($data,$data_rujukan,$data_paket);
		
		}
		if ($menu_kiri=='input_layanan_fisio_ri'){
			$q="SELECT H.id as idrujukan,H.norujukan,H.tanggal FROM `trujukan_fisioterapi` H
				WHERE H.idtindakan='$pendaftaran_id' AND H.`status`='1' AND H.asalrujukan='3' ";
			
			$data_rujukan=$this->db->query($q)->row_array();
			// print_r($data_rujukan);exit;
			if ($data_rujukan){
				
			}else{
				$data_rujukan=array(
					'idrujukan'=>'',
					'norujukan'=>'',
				);
			}
			if ($data_login_ppa['login_profesi_id']=='1'){//Jika Profesi Dokter
				$profesi_id=$data_login_ppa['login_profesi_id'];
				$iddokter_ppa=$data['iddokter'];
				$mppa_id=$data_login_ppa['login_ppa_id'];
				// $mppa_id=
			}else{
				$profesi_id=1;	
				$q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='1' AND H.tipepegawai='2' AND pegawai_id='".$data['iddokter']."'";
				$data_ppa=$this->db->query($q)->row();
				if ($data_ppa){
					$mppa_id=$data_ppa->id;
					$iddokter_ppa=$data_ppa->pegawai_id;
				}else{
					$mppa_id='';
					$iddokter_ppa='';
				}
			}
			$data['mppa_id'] = $mppa_id;
			$data['iddokter_ppa'] = $iddokter_ppa;
			$data['profesi_id'] = $profesi_id;
			$data['list_ppa'] = $this->Tpoliklinik_trx_model->list_ppa($profesi_id);
			$data['list_ppa_all'] = $this->Tpoliklinik_trx_model->list_ppa_all();
			// print_r($data['idtipe_poli'].$data['idkelompokpasien'].$data['idrekanan']);exit;
			$data['list_header_path'] = $this->Tpoliklinik_trx_model->list_header_path_fisio($data['idtipe_poli'],$data['idkelompokpasien'],$data['idrekanan']);
			$data_paket=$this->Tpoliklinik_trx_model->data_paket_layanan_fisio();
			if ($data_paket){}else{
				$data_paket=array(
					'paket_id' =>'',
					'nama_paket' =>'',
					'status_paket' =>'0',
					'st_edited' =>'0',
				);
			}
			// print_r($data_rujukan);exit;
			$data = array_merge($data,$data_rujukan,$data_paket);
		}
		// print_r($data);exit;
		$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
		$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
		$data = array_merge($data, backend_info());
		// print_r($data);exit;
		$data['st_ranap']=$st_ranap;
		$data['trx_id']=$trx_id;
		$this->parser->parse('module_template', $data);
	}
	public function get_obat()
    {
        $cari 	= $this->input->post('search');
        $idtipe 	= $this->input->post('idtipe');
        $idunit 	= $this->input->post('idunit');
        $q="SELECT H.*,S.stok FROM (
				SELECT '1' as idtipe,id as idbarang,kode,idkategori,nama FROM mdata_alkes  WHERE status='1'
				UNION ALL
				SELECT '2' as idtipe,id as idbarang,kode,idkategori,nama FROM mdata_implan  WHERE status='1'
				UNION ALL
				SELECT '3' as idtipe,id as idbarang,kode,idkategori,nama FROM mdata_obat WHERE status='1'
				) H 
				INNER JOIN mgudang_stok S ON S.idbarang=H.idbarang AND S.idtipe=H.idtipe AND S.idunitpelayanan='$idunit'
				
				WHERE H.idtipe='$idtipe' AND H.nama LIKE '%".$cari."%'
				ORDER BY nama
				";
		$data_obat=$this->db->query($q)->result_array();
        $this->output->set_output(json_encode($data_obat));
    }
	
	public function get_obat_detail()
    {
		$id=$this->input->post('idbarang');
		$idtipe=$this->input->post('idtipe');
		// print_r($idtipe);exit;
		$kelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		if ($kelompokpasien!='1'){
			$idrekanan=0;
		}
		$idunit=$this->input->post('idunit');
		$pertemuan_id=$this->input->post('pertemuan_id');
		$statuspasienbaru=$this->input->post('statuspasienbaru');
		$idpoli=$this->input->post('idpoliklinik');
		$idtipe_poli=$this->input->post('idtipe_poli');
		// $idtipe=$this->input->post('idtipe');
        $data_obat = $this->Tpoliklinik_trx_model->get_obat_detail($id, $kelompokpasien, $idtipe,$idunit);
		// print_r($data_obat);exit;
		$harga_before_diskon=ceiling(($data_obat->margin * $data_obat->hargadasar / 100) + ($data_obat->hargadasar),100);
		// $harga_before_diskon->hargajual=$hargajual;
		$data_obat->harga_before_diskon=$harga_before_diskon;
		$idbarang=$id;
		$q_diskon="
			SELECT M.* FROM (
			SELECT compare_value_10(
			MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
			,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
			,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.pertemuan_id='0' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
			,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='2' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
			,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
			,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
			,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
			,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='0' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
			,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='0' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='0' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
			,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='0' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='0' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
			) as id FROM merm_pengaturan_diskon_rajal_bmhp H
			
			) T INNER JOIN merm_pengaturan_diskon_rajal_bmhp M
			WHERE M.id=T.id

		";
		// print_r($q_diskon);exit;
		$data_diskon=$this->db->query($q_diskon)->row();
		$diskon_rp=0;
		$diskon_persen=0;
		if ($data_diskon){
			
			if ($data_diskon->operand!=0){
				if ($data_diskon->operand=1){
					$diskon_persen=$data_diskon->diskon;
					$diskon_rp=ceiling($harga_before_diskon * $diskon_persen / 100,100);
				}else{
					$diskon_persen=0;
					$diskon_rp=ceiling($data_diskon->diskon,100);
				}
			}
		}
		$hargajual_before=$harga_before_diskon-$diskon_rp;
		$hargajual=ceiling($hargajual_before,100);
		$pembulatan=$hargajual-$hargajual_before;
		$data_obat->hargajual=$hargajual;
		$data_obat->diskon_rp=$diskon_rp;
		$data_obat->diskon_persen=$diskon_persen;
		$data_obat->pembulatan=$pembulatan;
        $this->output->set_output(json_encode($data_obat));
    }
	
	public function get_obat_detail_paket()
    {
		$id=$this->input->post('idbarang');
		$idtipe=$this->input->post('idtipe');
		// print_r($idtipe);exit;
		$kelompokpasien=5;
		$idrekanan=$this->input->post('idrekanan');
		if ($kelompokpasien!='1'){
			$idrekanan=0;
		}
		$idunit=$this->input->post('idunit');
		// print_r($idunit);exit;
        $data_obat = $this->Tpoliklinik_trx_model->get_obat_detail($id, $kelompokpasien, $idtipe,$idunit);
		$harga_before_diskon=ceiling(($data_obat->margin * $data_obat->hargadasar / 100) + ($data_obat->hargadasar),100);
		$idbarang=$id;
		
		$hargajual=ceiling($harga_before_diskon,100);
		$data_obat->hargajual=$hargajual;
        $this->output->set_output(json_encode($data_obat));
    }
	
	function simpan_bmhp(){
		$user_id=$this->session->userdata('user_id');
		$tanggal_transaksi= YMDFormat($this->input->post('tgltransaksi')). ' ' .$this->input->post('waktutransaksi');
		$transaksi_id=$this->input->post('transaksi_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$idtipe=$this->input->post('idtipe');
		if ($transaksi_id){
			$this->db->where('id',$transaksi_id);
			if ($idtipe=='3'){
			$this->db->delete('tpoliklinik_obat');
				
			}else{
			$this->db->delete('tpoliklinik_alkes');
				
			}
		}
		$q="SELECT *FROM tpoliklinik_tindakan H WHERE H.idpendaftaran='$pendaftaran_id'";
		$data_tindakan=$this->db->query($q)->row();
		if ($data_tindakan){
			$idtindakan=$data_tindakan->id;
		}else{
			$q="INSERT INTO tpoliklinik_tindakan (idpendaftaran,iddokter1)
			VALUES ('$pendaftaran_id',null)";
			$this->db->query($q);
			$idtindakan=$this->db->insert_id;
		}
		$data=array(
			'tanggal_transaksi'=> $tanggal_transaksi,
			'created_date' => date('Y-m-d H:i:s'),
			'created_by' => $user_id,
			'idtindakan' => $idtindakan,
			'idunit' => $this->input->post('idunit'),
			'idtipe' => $this->input->post('idtipe'),
			'hargadasar' => RemoveComma($this->input->post('hargadasar')),
			'margin' => RemoveComma($this->input->post('margin')),
			'hargajual' => RemoveComma($this->input->post('hargajual')),
			'kuantitas' => RemoveComma($this->input->post('kuantitas')),
			'diskon' => RemoveComma($this->input->post('diskon')),
			'totalkeseluruhan' => RemoveComma($this->input->post('totalkeseluruhan')),
			'status' => 1,
			'iduser_transaksi' => $user_id,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
			'idpoliklinik' => $this->input->post('idpoliklinik'),
			'harga_before_diskon' => RemoveComma($this->input->post('harga_before_diskon')),
			'diskon_persen' => RemoveComma($this->input->post('diskon_persen')),
			'pembulatan' => RemoveComma($this->input->post('pembulatan')),


		);
		if ($idtipe=='3'){
			$data['statusstok'] = $this->input->post('statusstok');
			$data['idobat'] = $this->input->post('idobat');
			$result=$this->db->insert('tpoliklinik_obat',$data);
		}else{
			$data['idalkes'] = $this->input->post('idobat');
			$result=$this->db->insert('tpoliklinik_alkes',$data);
		}
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	
	function load_bmhp_list(){
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tabel_h="
			SELECT  
			id,idtindakan,idunit,idtipe,idalkes as idobat,hargadasar,margin,hargajual,kuantitas,diskon,totalkeseluruhan,statusverifikasi,status,tanggal_transaksi,iduser_transaksi,tanggal_verifikasi,iduser_verifikasi,pendaftaran_id,idpasien,idpoliklinik,harga_before_diskon,diskon_persen,pembulatan,created_date,created_by
			FROM tpoliklinik_alkes WHERE pendaftaran_id='$pendaftaran_id'
			UNION ALL
			SELECT 
			id,idtindakan,idunit,idtipe,idobat,hargadasar,margin,hargajual,kuantitas,diskon,totalkeseluruhan,statusverifikasi,status,tanggal_transaksi,iduser_transaksi,tanggal_verifikasi,iduser_verifikasi,pendaftaran_id,idpasien,idpoliklinik,harga_before_diskon,diskon_persen,pembulatan,created_date,created_by
			FROM tpoliklinik_obat WHERE pendaftaran_id='$pendaftaran_id'

		";
		$q="SELECT MU.nama as nama_unit,B.nama,B.idsatuan,B.namatipe,MS.nama as nama_satuan 
			,H.*,U.`name` as nama_user
			FROM (
				".$tabel_h."
			) H
			INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
			INNER JOIN munitpelayanan MU ON MU.id=H.idunit
			LEFT JOIN msatuan MS ON MS.id=B.idsatuan
			INNER JOIN musers U ON U.id=H.created_by 
			WHERE H.pendaftaran_id='$pendaftaran_id' 
			ORDER BY H.tanggal_transaksi ASC";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		foreach ($list as $r) {	
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_bmhp('.$r->id.','.$r->idtipe.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_bmhp('.$r->id.','.$r->idtipe.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			
			// $result[] = $no;
			$aksi='';
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			$col_tipe='<div class="text-primary">['.strtoupper($r->namatipe).']</div><div class="">'.$r->nama.'</div>';
			$col_harga='<div class="h5 text-primary">Harga : Rp. '.number_format($r->hargajual,0).'</div>';
			$col_harga .='<div class="text-muted">Harga : Rp. '.number_format($r->harga_before_diskon,0).'</div>';
			$col_harga .='<div class="text-muted">Disc : Rp. ('.number_format($r->diskon,0).')</div>';
			$aksi .= '</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-center">'.$r->nama_unit.'</td>';
			$tabel .='<td class="text-center">'.HumanDateLong($r->tanggal_transaksi).'</td>';
			$tabel .='<td class="text-left">'.$col_tipe.'</td>';
			$tabel .='<td class="text-right">'.$col_harga.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).' '.$r->nama_satuan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			$tabel .='<td class="text-center">'.($r->nama_user).'<br>'.HumanDateLong($r->created_date).'</td>';
			$tabel .='<td class="text-center">'.($aksi).'<input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idtipe" value="'.$r->idtipe.'"><input type="hidden" class="cls_idobat" value="'.$r->idobat.'"></td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			// $tabel .='<tr>';
				// $tabel .='<td colspan="4" width="60%"  class="text-right"><label id="total_kuantitas">TOTAL</label></td>';
				// $tabel .='<td class="text-right" width="5%"><label id="total_penjualan">2 QTY</label></td>';
				// $tabel .='<td width="12%">2333</td>';
				// $tabel .='<td width="23%"></td>';
			// $tabel .='</tr>';
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	
	
	
	
	function hapus_bmhp(){
		$id=$this->input->post('id');
		$idtipe=$this->input->post('idtipe');
		
		$this->db->where('id',$id);
		if ($idtipe=='3'){
		$hasil=$this->db->delete('tpoliklinik_obat');
			
		}else{
			
		$hasil=$this->db->delete('tpoliklinik_alkes');
		}
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_paket_detail_bmhp(){
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$hasil=$this->db->delete('mpaket_detail');
		$this->output->set_output(json_encode($hasil));
	}
	
	function hapus_bmhp_paket(){
		$id=$this->input->post('paket_id');
		$user_id=$this->session->userdata('user_id');
		$data=array(
			'deleted_date'=>date('Y-m-d H:i:s'),
			'deleted_by'=>$user_id,
			'staktif'=>0,
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('mpaket',$data);
		$this->output->set_output(json_encode($hasil));
	}
	
	function get_edit_bmhp(){
		$id=$this->input->post('id');
		$idtipe=$this->input->post('idtipe');
		if ($idtipe=='3'){
			$q="SELECT H.*,B.nama as nama_barang,MS.singkatan FROM `tpoliklinik_obat` H
			INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
			LEFT JOIN msatuan MS ON MS.id=B.idsatuan
			WHERE H.id='$id'";
		}else{
			$q="SELECT H.*,H.idalkes as idobat,B.nama as nama_barang,MS.singkatan FROM `tpoliklinik_alkes` H
			INNER JOIN view_barang_all B ON B.id=H.idalkes AND B.idtipe=H.idtipe
			LEFT JOIN msatuan MS ON MS.id=B.idsatuan
			WHERE H.id='$id'";
		}
		
		$hasil=$this->db->query($q)->row_array();
		$hasil['tgltransaksi']=HumanDateShort($hasil['tanggal_transaksi']);
		$hasil['waktutransaksi']=HumanTime($hasil['tanggal_transaksi']);
		$this->output->set_output(json_encode($hasil));
	}
	
	 public function get_obat_filter()
    {
        $nama_barang     = $this->input->post('nama_barang');
        $idunit     = $this->input->post('idunit');
        $idtipe     	= $this->input->post('idtipe');
        $idkategori    = $this->input->post('idkategori');
        $iduser=$this->session->userdata('user_id');

        $where='';
		if ($nama_barang!=''){
			 $where .=" AND B.nama LIKE '%".$nama_barang."%'";
		}
        if ($idtipe  != '#') {
            $where .=" AND S.idtipe='$idtipe'";
        } else {
            $where .=" AND S.idtipe IN( SELECT H.idtipe from munitpelayanan_tipebarang H
										LEFT JOIN mdata_tipebarang T ON H.idtipe=T.id
										WHERE H.idunitpelayanan='$idunit'
					)";
        }
        if ($idkategori  != '#') {
            $where .=" AND B.idkategori IN (
							SELECT id FROM (
									SELECT *FROM view_kategori_alkes
									UNION
									SELECT *FROM view_kategori_implan
									UNION
									SELECT *FROM view_kategori_obat
									UNION
									SELECT *FROM view_kategori_logistik
							) T1
							where T1.path like '$idkategori%'
			)";
        }
        $from="(SELECT S.idunitpelayanan,S.idtipe,S.idbarang as id,B.nama,B.idkategori,B.kode,B.namatipe,B.catatan,B.stokminimum,B.stokreorder,S.stok as stok_asli,msatuan.nama as satuan from mgudang_stok S
		INNER JOIN view_barang B ON B.idtipe=S.idtipe AND B.id=S.idbarang
		INNER JOIN msatuan ON msatuan.id=B.idsatuan
		WHERE S.idunitpelayanan='$idunit' ".$where." ORDER BY B.idtipe,B.nama) as tbl";


        $this->select = array();
        $this->join 	= array();
        $this->where  = array();

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        // print_r($from);exit();
        $this->column_search   = array('kode','nama','satuan','stok_asli','catatan');
        $this->column_order    = array('kode','nama','satuan','stok_asli','catatan');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        // print_r($list);exit();
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = "<a href='#' class='selectObat' data-idobat='".$r->id."' data-nama='".$r->nama."' data-idtipe='".$r->idtipe."' data-dismiss='modal'>".$r->kode."</span></a>";
            $row[] = "<a href='#' class='selectObat' data-idobat='".$r->id."' data-nama='".$r->nama."' data-idtipe='".$r->idtipe."' data-dismiss='modal'>".$r->nama."</span></a>";
            $row[] = $r->satuan;

            $row[] = WarningStok($r->stok_asli, $r->stokreorder, $r->stokminimum);
            $row[] = $r->catatan;
            $aksi       = '<div class="btn-group">';
            $aksi 		.= '<a target="_blank" href="'.site_url().'lgudang_stok/kartustok/'.$r->idtipe.'/'.$r->id.'/'.$r->idunitpelayanan.'" class="btn btn-xs btn-danger ks" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';
            $aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }
        $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->datatable->count_all(),
      "recordsFiltered" => $this->datatable->count_all(),
      "data" => $data
    );
        echo json_encode($output);
    }
	public function get_obat_paket_filter()
    {
        $nama_barang     = $this->input->post('nama_barang');
        $idunit     = $this->input->post('idunit');
        $idtipe     	= $this->input->post('idtipe');
        $idkategori    = $this->input->post('idkategori');
        $iduser=$this->session->userdata('user_id');

        $where='';
		if ($nama_barang!=''){
			 $where .=" AND B.nama LIKE '%".$nama_barang."%'";
		}
        if ($idtipe  != '#') {
            $where .=" AND S.idtipe='$idtipe'";
        } else {
            $where .=" AND S.idtipe IN( SELECT H.idtipe from munitpelayanan_tipebarang H
										LEFT JOIN mdata_tipebarang T ON H.idtipe=T.id
										WHERE H.idunitpelayanan='$idunit'
					)";
        }
        if ($idkategori  != '#') {
            $where .=" AND B.idkategori IN (
							SELECT id FROM (
									SELECT *FROM view_kategori_alkes
									UNION
									SELECT *FROM view_kategori_implan
									UNION
									SELECT *FROM view_kategori_obat
									UNION
									SELECT *FROM view_kategori_logistik
							) T1
							where T1.path like '$idkategori%'
			)";
        }
        $from="(SELECT S.idunitpelayanan,S.idtipe,S.idbarang as id,B.nama,B.idkategori,B.kode,B.namatipe,B.catatan,B.stokminimum,B.stokreorder,S.stok as stok_asli,msatuan.nama as satuan from mgudang_stok S
		INNER JOIN view_barang B ON B.idtipe=S.idtipe AND B.id=S.idbarang
		INNER JOIN msatuan ON msatuan.id=B.idsatuan
		WHERE S.idunitpelayanan='$idunit' ".$where." ORDER BY B.idtipe,B.nama) as tbl";


        $this->select = array();
        $this->join 	= array();
        $this->where  = array();

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        // print_r($from);exit();
        $this->column_search   = array('kode','nama','satuan','stok_asli','catatan');
        $this->column_order    = array('kode','nama','satuan','stok_asli','catatan');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        // print_r($list);exit();
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = "<a href='#' class='selectObatPaket' data-idobat='".$r->id."' data-nama='".$r->nama."' data-idtipe='".$r->idtipe."' data-dismiss='modal'>".$r->kode."</span></a>";
            $row[] = "<a href='#' class='selectObatPaket' data-idobat='".$r->id."' data-nama='".$r->nama."' data-idtipe='".$r->idtipe."' data-dismiss='modal'>".$r->nama."</span></a>";
            $row[] = $r->satuan;

            $row[] = WarningStok($r->stok_asli, $r->stokreorder, $r->stokminimum);
            $row[] = $r->catatan;
            $aksi       = '<div class="btn-group">';
            $aksi 		.= '<a target="_blank" href="'.site_url().'lgudang_stok/kartustok/'.$r->idtipe.'/'.$r->id.'/'.$r->idunitpelayanan.'" class="btn btn-xs btn-danger ks" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';
            $aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }
        $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->datatable->count_all(),
      "recordsFiltered" => $this->datatable->count_all(),
      "data" => $data
    );
        echo json_encode($output);
    }
	
	function list_history_bmhp(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_transaksi) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_transaksi) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT H.pendaftaran_id,MP.nopendaftaran,MP.tanggal,CASE WHEN MP.idtipe=1 THEN 'POLIKLLINIK' ELSE 'IGD' END as tipe_poli,P.nama as nama_poli
					,MD.nama as nama_dokter,H.tanggal_transaksi,MP.tanggaldaftar
					,SUM(H.totalkeseluruhan) as total_trx FROM tpoliklinik_obat H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					INNER JOIN mpoliklinik P ON P.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					WHERE H.idpasien='$idpasien' AND H.pendaftaran_id != '$pendaftaran_id'
					GROUP BY H.pendaftaran_id
					ORDER BY MP.id DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$btn_lihat='<button class="btn btn-primary  btn-xs" onclick="lihat_data_bmhp('.$r->pendaftaran_id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat Detail</button>';
		$result[] = $no;
		$aksi='';
		$aksi_print='<button class="btn btn-success  btn-xs"  type="button" title="Print" type="button"><i class="fa fa-print"></i></button>';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_lihat;	
		$aksi .= $aksi_print;	
		$aksi .= '</div>';
		$result[] = ($r->nopendaftaran);
		$result[] = HumanDateLong($r->tanggaldaftar);
		$result[] = ($r->tipe_poli);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dokter);
		$result[] = number_format($r->total_trx,0);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  function lihat_data_bmhp(){
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$q="SELECT MU.nama as nama_unit,B.nama,B.idsatuan,B.namatipe,MS.nama as nama_satuan 
			,H.*,U.`name` as nama_user
			FROM tpoliklinik_obat H
			INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
			INNER JOIN munitpelayanan MU ON MU.id=H.idunit
			LEFT JOIN msatuan MS ON MS.id=B.idsatuan
			INNER JOIN musers U ON U.id=H.created_by 
			WHERE H.pendaftaran_id='$pendaftaran_id' 
			ORDER BY H.tanggal_transaksi ASC";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		foreach ($list as $r) {	
			$btn_edit='<button class="btn btn-warning  btn-xs" onclick="copy_bmhp('.$r->id.')" type="button" title="Copy Data" type="button"><i class="fa fa-copy"></i></button>';
			// $result[] = $no;
			$aksi='';
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= '</div>';
			$col_tipe='<div class="text-primary">['.strtoupper($r->namatipe).']</div><div class="">'.$r->nama.'</div>';
			$col_harga='<div class="h5 text-primary">Harga : Rp. '.number_format($r->hargajual,0).'</div>';
			$col_harga .='<div class="text-muted">Harga : Rp. '.number_format($r->harga_before_diskon,0).'</div>';
			$col_harga .='<div class="text-muted">Disc : Rp. ('.number_format($r->diskon,0).')</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-center">'.$r->nama_unit.'</td>';
			$tabel .='<td class="text-center">'.HumanDateLong($r->tanggal_transaksi).'</td>';
			$tabel .='<td class="text-left">'.$col_tipe.'</td>';
			$tabel .='<td class="text-right">'.$col_harga.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).' '.$r->nama_satuan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			$tabel .='<td class="text-center">'.($r->nama_user).'<br>'.HumanDateLong($r->created_date).'</td>';
			$tabel .='<td class="text-center">'.($aksi).'</td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	
	
	function copy_bmhp(){
		// $user_id=$this->session->userdata('user_id');
		// $id=$this->input->post('id');
		// $pendaftaran_id=$this->input->post('pendaftaran_id');
		// $idpasien=$this->input->post('idpasien');
		// $idpoliklinik=$this->input->post('idpoliklinik');
		
		
		// $q="
			// SELECT 
			// idunit,idtipe,idobat,hargadasar,margin,hargajual,kuantitas,diskon,totalkeseluruhan,statusverifikasi,status,statusstok,NOW(),iduser_transaksi,tanggal_verifikasi,iduser_verifikasi,'$pendaftaran_id','$idpasien','$idpoliklinik',harga_before_diskon,diskon_persen,pembulatan,NOW(),'$user_id' 
			// FROM `tpoliklinik_obat` 
			// WHERE id='$id'
		// ";
		// $hasil=$this->db->query($q)->row();
		$user_id=$this->session->userdata('user_id');
		$idunit=$this->input->post('idunit');
		$kelompok_pasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$idbarang=$this->input->post('id');
		$pertemuan_id=$this->input->post('pertemuan_id');
		// print_r($idbarang);exit;
		$statuspasienbaru=$this->input->post('statuspasienbaru');
		$idpoli=$this->input->post('idpoliklinik');
		$idtipe_poli=$this->input->post('idtipe_poli');
		if ($kelompok_pasien!='1'){
			$idrekanan=0;
		}
		  $q="
			SELECT S.stok as stok_asli,MS.nama as nama_satuan 
					,B.*,H.kuantitas,H.idunit
					,(CASE
					WHEN $kelompok_pasien = 5 THEN
						marginumum
					WHEN $kelompok_pasien = 0 THEN
						marginumum
					WHEN $kelompok_pasien = 1 THEN
						marginasuransi
					WHEN $kelompok_pasien = 2 THEN
						marginjasaraharja
					WHEN $kelompok_pasien = 3 THEN
						marginbpjskesehatan
					WHEN $kelompok_pasien = 4 THEN
						marginbpjstenagakerja
				END) AS margin
					FROM `tpoliklinik_obat` H
					LEFT JOIN mgudang_stok S ON S.idtipe=H.idtipe AND S.idbarang=H.idobat AND S.idunitpelayanan=H.idunit
					INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
					LEFT JOIN msatuan MS ON MS.id=B.idsatuan
					WHERE H.id IN (".$idbarang.")
		  ";
		  // print_r($q);exit;
		$list_barang=$this->db->query($q)->result();
		$jml_insert=0;
		foreach($list_barang as $r){
			$idunit=$r->idunit;
			if ($r->kuantitas<=$r->stok_asli){
				$jml_insert=$jml_insert+1;
				$kelompokpasien=$kelompok_pasien;
				$idbarang=$r->id;
				$idtipe=$r->idtipe;
				$kuantitas=$r->kuantitas;
				$hargadasar=$r->hargadasar;
				$harga_before_diskon=(ceiling($r->hargadasar + ($r->hargadasar*$r->margin/100),100));
				$q_diskon="
					SELECT M.* FROM (
					SELECT compare_value_10(
					MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.pertemuan_id='0' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='2' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='0' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='0' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='0' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='0' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='0' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
					) as id FROM merm_pengaturan_diskon_rajal_bmhp H

					) T INNER JOIN merm_pengaturan_diskon_rajal_bmhp M
					WHERE M.id=T.id

					";
					$data_diskon=$this->db->query($q_diskon)->row();
					$diskon_rp=0;
					$diskon_persen=0;
					if ($data_diskon){
						if ($data_diskon->operand!=0){
							if ($data_diskon->operand=1){
								$diskon_persen=$data_diskon->diskon;
								$diskon_rp=ceiling($harga_before_diskon * $diskon_persen / 100,100);
							}else{
								$diskon_persen=0;
								$diskon_rp=ceiling($data_diskon->diskon,100);
							}
						}
					}else{
						
					}
					// print_r($harga_before_diskon);exit;
				$hargajual_before=$harga_before_diskon-$diskon_rp;
				$hargajual=ceiling($hargajual_before,100);	
				$data=array(
					'tanggal_transaksi'=> date('Y-m-d H:i:s'),
					'created_date' => date('Y-m-d H:i:s'),
					'created_by' => $user_id,
					'idunit' => $idunit,
					'idtipe' => $r->idtipe,
					'idobat' => $r->id,
					'hargadasar' => RemoveComma($hargadasar),
					'margin' => ($r->margin),
					'hargajual' => $hargajual,
					'kuantitas' => ($kuantitas),
					'diskon' => $diskon_rp,
					'totalkeseluruhan' => $hargajual * $kuantitas,
					'status' => 1,
					'statusstok' => $r->stok_asli,
					'iduser_transaksi' => $user_id,
					'pendaftaran_id' => $this->input->post('pendaftaran_id'),
					'idpasien' => $this->input->post('idpasien'),
					'idpoliklinik' => $this->input->post('idpoliklinik'),
					'harga_before_diskon' => ($harga_before_diskon),
					'diskon_persen' => $diskon_persen,
					'pembulatan' => 0,
				);
				
				$result=$this->db->insert('tpoliklinik_obat',$data);
			}
		}
		$this->output->set_output(json_encode($jml_insert));
		// $this->output->set_output(json_encode($hasil));
	}
	
	function create_paket_bmhp(){
		$user_id=$this->session->userdata('user_id');
		$data=array(
			'created_date' =>date('Y-m-d H:i:s'),
			'created_by' =>$user_id,
			'nama_paket' =>'',
			'status_paket' =>'1',
			'total' =>'0',
			'st_edited' =>'0',

		);
		$this->db->insert('mpaket',$data);
		$this->output->set_output(json_encode($data));
	}
	
	function simpan_paket_bmhp(){
		$user_id=$this->session->userdata('user_id');
		$paket_detail_id=$this->input->post('paket_detail_id');
		
		$data=array(
			'paket_id' => $this->input->post('paket_id'),
			'idtipe' => $this->input->post('idtipe'),
			'idobat' => $this->input->post('idobat'),
			'nama_barang' => $this->input->post('nama_barang'),
			'nama_satuan' => $this->input->post('nama_satuan'),
			'idsatuan' => $this->input->post('idsatuan'),
			'harga' => RemoveComma($this->input->post('harga')),
			'kuantitas' => RemoveComma($this->input->post('kuantitas')),
			'total_harga' => RemoveComma($this->input->post('total_harga')),
			'created_by' => $user_id,
			'created_date' => date('Y-m-d H:i:s'),

		);
		if ($paket_detail_id){
			$this->db->where('id',$paket_detail_id);
			$result=$this->db->update('mpaket_detail',$data);
		}else{
			$result=$this->db->insert('mpaket_detail',$data);
			
		}
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function load_bmhp_paket(){
		$paket_id=$this->input->post('paket_id');
		$q="
			SELECT M.nama_tipe,H.* FROM mpaket_detail H
			INNER JOIN mdata_tipebarang M ON M.id=H.idtipe
			WHERE H.paket_id='$paket_id'
		";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		foreach ($list as $r) {	
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_paket_detail_bmhp('.$r->id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			// $result[] = $no;
			$aksi='';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_paket_detail_bmhp('.$r->id.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			$aksi .= '</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idtipe" value="'.$r->idtipe.'"><input type="hidden" class="cls_idobat" value="'.$r->idobat.'"> '.$r->nama_tipe.'</td>';
			$tabel .='<td class="text-left">'.$r->nama_barang.'</td>';
			$tabel .='<td class="text-center">'.$r->nama_satuan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->harga,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total_harga,0).'</td>';
			
			$tabel .='<td class="text-center">'.($aksi).'</td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->total_harga;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	
	function lihat_data_bmhp_paket(){
		$paket_id=$this->input->post('paket_id');
		$q="
			SELECT M.nama_tipe,H.* FROM mpaket_detail H
			INNER JOIN mdata_tipebarang M ON M.id=H.idtipe
			WHERE H.paket_id='$paket_id'
		";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		foreach ($list as $r) {	
			// $result[] = $no;
			$aksi='';
			$btn_copy='<button class="btn btn-warning  btn-xs" onclick="copy_bmhp_paket_detail('.$r->id.')" type="button" title="Terapkan Paket" type="button"><i class="fa fa-copy"></i> Apply</button>';
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_copy;	
			$aksi .= '</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="cls_idtipe" value="'.$r->idtipe.'"><input type="hidden" class="cls_idobat" value="'.$r->idobat.'"> '.$r->nama_tipe.'</td>';
			$tabel .='<td class="text-left">'.$r->nama_barang.'</td>';
			$tabel .='<td class="text-center">'.$r->nama_satuan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->harga,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total_harga,0).'</td>';
			
			$tabel .='<td class="text-center">'.($aksi).'</td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->total_harga;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	
	function get_edit_paket_bmhp(){
		$id=$this->input->post('id');
		$q="SELECT H.* FROM `mpaket_detail` H
			WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_paket(){
		$paket_id=$this->input->post('paket_id');
		$nama_paket=$this->input->post('nama_paket');
		$status_paket=$this->input->post('status_paket');
		$st_edited=$this->input->post('st_edited');
		$data=array(
			'nama_paket' => $nama_paket,
			'status_paket' => $status_paket,
		);
		if ($status_paket=='2'){
			$data['st_edited']=0;
		}
		// if ($st_edited=='0'){
			// $data['status_paket']=0;
			// $data['staktif']=0;
		// }else{
			
			// $data['status_paket']=2;
		// }
		$this->db->where('id',$paket_id);
		$hasil=$this->db->update('mpaket',$data);
		$this->output->set_output(json_encode($data));
		
	}
	
	function batal_paket_bmhp(){
		$paket_id=$this->input->post('paket_id');
		
		$st_edited=$this->input->post('st_edited');
		
		if ($st_edited=='0'){
			$data['status_paket']=0;
			$data['staktif']=0;
		}else{
			
			$data['status_paket']=2;
			$data['st_edited']=0;
		}
		
		$this->db->where('id',$paket_id);
		$hasil=$this->db->update('mpaket',$data);
		$this->output->set_output(json_encode($data));
		
	}
	
	function simpan_paket_edit(){
		$paket_id=$this->input->post('paket_id');
		$status_paket=$this->input->post('status_paket');
		$data=array(
			'st_edited' => 1,
			'status_paket' => 1,
		);
		
		$this->db->where('id',$paket_id);
		$hasil=$this->db->update('mpaket',$data);
		$this->output->set_output(json_encode($data));
		
	}
	
	function list_history_bmhp_paket(){
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tgl_input_1=$this->input->post('tgl_input_1');
		$tgl_input_1=$this->input->post('tgl_input_1');
		$nama_paket=$this->input->post('nama_paket');
		
		if ($nama_paket!=''){
			$where .=" AND H.nama_paket LIKE '%".$nama_paket."%'";
		}
		
		if ($tgl_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tgl_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tgl_input_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT H.*,SUM(D.total_harga) as total_harga,MU.`name` as nama_user 
					,GROUP_CONCAT(D.id) as detail_id
					FROM mpaket H
					INNER JOIN mpaket_detail D ON D.paket_id=H.id
					INNER JOIN musers MU ON MU.id=H.created_by
					WHERE H.status_paket='2' AND H.staktif='1'
					GROUP BY H.id
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama_paket');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		$detail_id="'".$r->detail_id."'";
		$result = array();
		$btn_lihat='<button class="btn btn-primary  btn-xs" onclick="lihat_data_bmhp_paket('.$r->id.')" type="button" title="Detail" type="button"><i class="fa fa-eye"></i> Detail</button>';
		$btn_edit='<button class="btn btn-success  btn-xs" onclick="edit_bmhp_paket('.$r->id.')" type="button" title="Edit Paket" type="button"><i class="fa fa-pencil"></i> Edit</button>';
		$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_bmhp_paket('.$r->id.')" type="button" title="Hapus Paket" type="button"><i class="fa fa-trash"></i> Hapus</button>';
		$btn_copy='<button class="btn btn-warning  btn-xs" onclick="copy_bmhp_paket('.$detail_id.')" type="button" title="Terapkan Paket" type="button"><i class="fa fa-copy"></i> Apply</button>';
		$result[] = $no;
		$aksi='';
		$aksi_print='<button class="btn btn-success  btn-xs"  type="button" title="Print" type="button"><i class="fa fa-print"></i></button>';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_lihat;	
		$aksi .= $btn_edit;	
		$aksi .= $btn_hapus;	
		$aksi .= $btn_copy;	
		$aksi .= '</div>';
		$result[] = ($r->nama_paket);
		$result[] = number_format($r->total_harga,0);
		$result[] = ($r->nama_user).'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  public function loadBarangCopy()
    {
        $kelompok_pasien     = $this->input->post('idkelompokpasien');
        $idunit     = $this->input->post('idunit');
        $array_paket_id     	= $this->input->post('array_paket_id');
        
        $from="(
			SELECT S.stok as stok_asli,MS.nama as nama_satuan 
				,B.*,H.kuantitas
				,(CASE
				WHEN $kelompok_pasien = 5 THEN
					marginumum
				WHEN $kelompok_pasien = 0 THEN
					marginumum
				WHEN $kelompok_pasien = 1 THEN
					marginasuransi
				WHEN $kelompok_pasien = 2 THEN
					marginjasaraharja
				WHEN $kelompok_pasien = 3 THEN
					marginbpjskesehatan
				WHEN $kelompok_pasien = 4 THEN
					marginbpjstenagakerja
			END) AS margin
				FROM `mpaket_detail` H
				LEFT JOIN mgudang_stok S ON S.idtipe=H.idtipe AND S.idbarang=H.idobat AND S.idunitpelayanan='$idunit'
				INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
				LEFT JOIN msatuan MS ON MS.id=B.idsatuan
				WHERE H.id IN (".$array_paket_id.")
		) as tbl";
        // print_r($from);exit();


        $this->select = array();
        $this->join 	= array();
        $this->where  = array();

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        // print_r($from);exit();
        $this->column_search   = array('kode','nama');
        $this->column_order    = array('kode','nama');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->kode;
            $row[] = $r->nama;
            $row[] = $r->nama_satuan;
            $row[] = number_format(ceiling($r->hargadasar + ($r->hargadasar*$r->margin/100),100),0);

            $row[] = WarningStok($r->stok_asli, $r->stokreorder, $r->stokminimum);
            $row[] = $r->catatan;
            $aksi       = ($r->kuantitas>$r->stok_asli ? text_danger('TIDAK TERSEDIA'):text_primary('TERSEDIA'));
            $row[] = $aksi;
            $data[] = $row;
        }
        $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->datatable->count_all(),
      "recordsFiltered" => $this->datatable->count_all(),
      "data" => $data
    );
        echo json_encode($output);
    }
	
  function terapkan_copy(){
	  $user_id=$this->session->userdata('user_id');
	$idunit=$this->input->post('idunit');
	$kelompok_pasien=$this->input->post('idkelompokpasien');
	$idrekanan=$this->input->post('idrekanan');
	$idbarang=$this->input->post('idbarang');
	$pertemuan_id=$this->input->post('pertemuan_id');
	// print_r($idbarang);exit;
	$statuspasienbaru=$this->input->post('statuspasienbaru');
	$idpoli=$this->input->post('idpoliklinik');
	$idtipe_poli=$this->input->post('idtipe_poli');
	if ($kelompok_pasien!='1'){
		$idrekanan=0;
	}
	  $q="
		SELECT S.stok as stok_asli,MS.nama as nama_satuan 
				,B.*,H.kuantitas
				,(CASE
				WHEN $kelompok_pasien = 5 THEN
					marginumum
				WHEN $kelompok_pasien = 0 THEN
					marginumum
				WHEN $kelompok_pasien = 1 THEN
					marginasuransi
				WHEN $kelompok_pasien = 2 THEN
					marginjasaraharja
				WHEN $kelompok_pasien = 3 THEN
					marginbpjskesehatan
				WHEN $kelompok_pasien = 4 THEN
					marginbpjstenagakerja
			END) AS margin
				FROM `mpaket_detail` H
				LEFT JOIN mgudang_stok S ON S.idtipe=H.idtipe AND S.idbarang=H.idobat AND S.idunitpelayanan='$idunit'
				INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
				LEFT JOIN msatuan MS ON MS.id=B.idsatuan
				WHERE H.id IN (".$idbarang.")
	  ";
	$list_barang=$this->db->query($q)->result();
	  // print_r($list_barang);exit;
	$jml_insert=0;
	foreach($list_barang as $r){
		if ($r->kuantitas<=$r->stok_asli){
			$jml_insert=$jml_insert+1;
			$kelompokpasien=$kelompok_pasien;
			$idbarang=$r->id;
			$idtipe=$r->idtipe;
			$kuantitas=$r->kuantitas;
			$hargadasar=$r->hargadasar;
			$harga_before_diskon=(ceiling($r->hargadasar + ($r->hargadasar*$r->margin/100),100));
			$q_diskon="
				SELECT M.* FROM (
				SELECT compare_value_10(
				MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
				,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
				,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.pertemuan_id='0' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
				,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe'  AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='2' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
				,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='$pertemuan_id' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
				,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='$statuspasienbaru' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
				,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='$idpoli' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
				,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='0' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='$idrekanan' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
				,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='0' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='0' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
				,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.pertemuan_id='0' AND H.statuspasienbaru='2' AND H.idpoli='0' AND H.idtipe_poli='$idtipe_poli' AND H.idrekanan='0' AND H.idkelompokpasien='$kelompokpasien',H.id,NULL))
				) as id FROM merm_pengaturan_diskon_rajal_bmhp H

				) T INNER JOIN merm_pengaturan_diskon_rajal_bmhp M
				WHERE M.id=T.id

				";
				$data_diskon=$this->db->query($q_diskon)->row();
				$diskon_rp=0;
				$diskon_persen=0;
				if ($data_diskon){
					if ($data_diskon->operand!=0){
						if ($data_diskon->operand=1){
							$diskon_persen=$data_diskon->diskon;
							$diskon_rp=ceiling($harga_before_diskon * $diskon_persen / 100,100);
						}else{
							$diskon_persen=0;
							$diskon_rp=ceiling($data_diskon->diskon,100);
						}
					}
				}else{
					
				}
				// print_r($harga_before_diskon);exit;
			$hargajual_before=$harga_before_diskon-$diskon_rp;
			$hargajual=ceiling($hargajual_before,100);	
			$data=array(
				'tanggal_transaksi'=> date('Y-m-d H:i:s'),
				'created_date' => date('Y-m-d H:i:s'),
				'created_by' => $user_id,
				'idunit' => $idunit,
				'idtipe' => $r->idtipe,
				'idobat' => $r->id,
				'hargadasar' => RemoveComma($hargadasar),
				'margin' => ($r->margin),
				'hargajual' => $hargajual,
				'kuantitas' => ($kuantitas),
				'diskon' => $diskon_rp,
				'totalkeseluruhan' => $hargajual * $kuantitas,
				'status' => 1,
				'statusstok' => $r->stok_asli,
				'iduser_transaksi' => $user_id,
				'pendaftaran_id' => $this->input->post('pendaftaran_id'),
				'idpasien' => $this->input->post('idpasien'),
				'idpoliklinik' => $this->input->post('idpoliklinik'),
				'harga_before_diskon' => ($harga_before_diskon),
				'diskon_persen' => $diskon_persen,
				'pembulatan' => 0,
			);
			
			$result=$this->db->insert('tpoliklinik_obat',$data);
		}
	}
	$this->output->set_output(json_encode($jml_insert));
	  
  }
  //AKHIR OBAT
  
  //AWAL tindakan
  public function getTindakanRawatJalan()
	{
		
		$form_akses=$this->input->post('form_akses');
		$rowpath=$this->input->post('rowpath');
		$sub_parent=$this->input->post('sub_parent');
		$nama_tarif_filter=$this->input->post('nama_tarif_filter');
		$where='';
		if ($sub_parent!='#'){
			 $where .=" AND path LIKE '".$sub_parent."%'";
		}
		if ($nama_tarif_filter!=''){
			 $where .=" AND nama LIKE '%".$nama_tarif_filter."%'";
		}
        
       $from="
			(
				SELECT *FROM mtarif_rawatjalan WHERE path LIKE '".$rowpath."%' AND status='1' 
				".$where."
				ORDER BY path
			) as tbl
		";
        $this->select = array();
        $this->join 	= array();
        $this->where  = array();

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        // print_r($from);exit();
        $this->column_search   = array('nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];

		
		
		foreach ($list as $r) {
			$no++;
			$row = [];

			if ($r->idkelompok == 1) {
				$namatarif = TreeView($r->level, $r->nama);
			} else {
				if ($form_akses=='paket'){
				$namatarif = '<a href="#" class="selectLayananPaket" data-nama="' . TreeView($r->level, $r->nama) . '" data-idpelayanan="' . $r->id . '">' . TreeView($r->level, $r->nama) . '</a>';
					
				}else{
				$namatarif = '<a href="#" class="selectLayanan" data-nama="' . TreeView($r->level, $r->nama) . '" data-idpelayanan="' . $r->id . '">' . TreeView($r->level, $r->nama) . '</a>';
					
				}
			}

			$row[] = $no;
			$row[] = $namatarif;
			$row[] = number_format($r->jasasarana);
			$row[] = number_format($r->jasapelayanan);
			$row[] = number_format($r->bhp);
			$row[] = number_format($r->biayaperawatan);
			$row[] = number_format($r->total);

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}
	
	
	function hapus_layanan_paket(){
		$id=$this->input->post('paket_id');
		$user_id=$this->session->userdata('user_id');
		$data=array(
			'deleted_date'=>date('Y-m-d H:i:s'),
			'deleted_by'=>$user_id,
			'staktif'=>0,
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('mpaket_layanan',$data);
		$this->output->set_output(json_encode($hasil));
	}
	
	public function get_layanan_detail()
    {
		$id=$this->input->post('idpelayanan');
		$idtipe=$this->input->post('idtipe_poli');
		// print_r($idtipe);exit;
		$kelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		if ($kelompokpasien!='1'){
			$idrekanan=0;
		}
		$idunit=$this->input->post('idunit');
		$pertemuan_id=$this->input->post('pertemuan_id');
		$statuspasienbaru=$this->input->post('statuspasienbaru');
		$idpoli=$this->input->post('idpoliklinik');
		$idtipe_poli=$this->input->post('idtipe_poli');
		// $idtipe=$this->input->post('idtipe');
		$q="SELECT * FROM mtarif_rawatjalan WHERE id='$id'";
        $data_layanan = $this->db->query($q)->row();
		$headparent=$this->get_head_parent($data_layanan->path);
		$data_diskon=$this->Tpoliklinik_trx_model->get_logic_diskon_pelayanan($kelompokpasien,$idrekanan,$idtipe,$idpoli,$statuspasienbaru,$pertemuan_id,$headparent,$id);
		// print_r($kelompokpasien.'-'.$idrekanan.'-'.$idtipe.'-'.$idpoli.'-'.$statuspasienbaru.'-'.$pertemuan_id.'-'.$headparent.'-'.$id);exit;
		$diskon_rp=0;
		$diskon_persen=0;
		$st_diskon=0;
		if ($data_diskon){
			if ($data_diskon->status_diskon_jasapelayanan=='1'){
				$st_diskon='1';
				if ($data_diskon->tipe_diskon_jasapelayanan=='1'){//PErsen
					$diskon_persen=$data_diskon->diskon_jasapelayanan;
					$diskon_rp=$diskon_persen/100 * $data_layanan->jasapelayanan;
				}else{
					$diskon_rp=$data_diskon->diskon_jasapelayanan;
					$diskon_persen =($diskon_rp * 100) / $data_layanan->jasapelayanan;
				}
			}
		}
		
		$data_layanan->st_diskon=$st_diskon;
		$data_layanan->diskon_rp=$diskon_rp;
		$data_layanan->diskon_persen=$diskon_persen;
        $this->output->set_output(json_encode($data_layanan));
    }
	
	
	public function select2_layanan()
    {
        $rowpath 	= $this->input->post('rowpath');
        $q="
			SELECT *FROM mtarif_rawatjalan H
			WHERE H.headerpath LIKE '".$rowpath."%' 
			
			ORDER BY H.path ASC
				";
		$opsi='<option value="" selected>-Pilih Tindakan-</option>';
	  $hasil=$this->db->query($q)->result();
	  foreach ($hasil as $r){
		$opsi .='<option value="'.$r->id.'" '.($r->level!='2'?'disabled':'').'>'.TreeView($r->level,$r->nama).'</option>';
		  
	  }
	  $this->output->set_output(json_encode($opsi));
    }
	public function select2_subparent()
    {
        $rowpath 	= $this->input->post('rowpath');
        $q="
			SELECT *FROM mtarif_rawatjalan H
			WHERE H.headerpath LIKE '".$rowpath."%'  AND H.level !='2'
			
			ORDER BY H.path ASC
				";
		$opsi='<option value="#" selected>- All -</option>';
	  $hasil=$this->db->query($q)->result();
	  foreach ($hasil as $r){
		$opsi .='<option value="'.$r->path.'">'.TreeView($r->level,$r->nama).'</option>';
		  
	  }
	  $this->output->set_output(json_encode($opsi));
    }
	function find_mppa($profesi_id,$mppa_id){
	  $q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='$profesi_id' AND H.staktif='1'";
	  $opsi='<option value="" selected>Silahkan Pilih PPA</option>';
	  if ($profesi_id!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'" '.($mppa_id==$r->id && $profesi_id=='1'?'selected':'').'>'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
	}
	function get_iddokter_ppa($mppa_id){
		$q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='1' AND H.tipepegawai='2' AND id='$mppa_id'";
		$data=$this->db->query($q)->row('pegawai_id');
		if ($data){
			
		}else{
			$data='';
		}
		$this->output->set_output(json_encode($data));
	}
	function simpan_layanan(){
		$user_id=$this->session->userdata('user_id');
		$tanggal_transaksi= YMDFormat($this->input->post('tgltransaksi')). ' ' .$this->input->post('waktutransaksi');
		$transaksi_id=$this->input->post('transaksi_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$iddokter=$this->input->post('iddokter');
		if ($transaksi_id){
			$this->db->where('id',$transaksi_id);
			$this->db->delete('tpoliklinik_pelayanan');
		}
		$q="SELECT *FROM tpoliklinik_tindakan H WHERE H.idpendaftaran='$pendaftaran_id'";
		$data_tindakan=$this->db->query($q)->row();
		if ($data_tindakan){
			$idtindakan=$data_tindakan->id;
		}else{
			$q="INSERT INTO tpoliklinik_tindakan (idpendaftaran,iddokter1)
			VALUES ('$pendaftaran_id',$iddokter)";
			$this->db->query($q);
			$idtindakan=$this->db->insert_id;
		}
		$data=array(
			'idtindakan'=> $idtindakan,
			'tanggal_transaksi'=> $tanggal_transaksi,
			'created_date' => date('Y-m-d H:i:s'),
			'created_by' => $user_id,
			'iddokter' => $this->input->post('iddokter'),
			'idpelayanan' => $this->input->post('idpelayanan'),
			'jasasarana' => RemoveComma($this->input->post('jasasarana')),
			'jasasarana_disc' => RemoveComma($this->input->post('jasasarana_disc')),
			'jasapelayanan' => RemoveComma($this->input->post('jasapelayanan')),
			'jasapelayanan_disc' => RemoveComma($this->input->post('jasapelayanan_disc')),
			'bhp' => RemoveComma($this->input->post('bhp')),
			'bhp_disc' => RemoveComma($this->input->post('bhp_disc')),
			'biayaperawatan' => RemoveComma($this->input->post('biayaperawatan')),
			'biayaperawatan_disc' => RemoveComma($this->input->post('biayaperawatan_disc')),
			'total' => RemoveComma($this->input->post('total')),
			'kuantitas' => RemoveComma($this->input->post('kuantitas')),
			'diskon' => RemoveComma($this->input->post('diskon')),
			'totalkeseluruhan' => RemoveComma($this->input->post('totalkeseluruhan')),
			'status' => 1,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
			'profesi_id' => $this->input->post('profesi_id'),
			'mppa_id' => $this->input->post('mppa_id'),

		);
		
		$result=$this->db->insert('tpoliklinik_pelayanan',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	
	function load_transaksi_layanan_list(){
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$q="SELECT M.nama,MR.ref as jenis_tenaga,D.nama as nama_tindakan, H.* 
			FROM `tpoliklinik_pelayanan` H
			INNER JOIN mtarif_rawatjalan D ON D.id=H.idpelayanan
			INNER JOIN mppa M ON M.id=mppa_id
			INNER JOIN merm_referensi MR ON MR.ref_head_id='21' AND MR.nilai=H.profesi_id
			WHERE H.pendaftaran_id='$pendaftaran_id' 
			ORDER BY H.tanggal_transaksi ASC";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		$no=0;
		foreach ($list as $r) {	
		$no++;
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_layanan('.$r->id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			// $result[] = $no;
			$aksi='';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_layanan('.$r->id.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			$col_tindakan='<div class=""><strong>'.$r->jenis_tenaga.'</strong></div><div class="">'.$r->nama.'</div><div class="text-primary">['.HumanDateLong($r->tanggal_transaksi).']</div>';
			$col_harga='<div class="text-primary">'.number_format($r->jasapelayanan,0).'</div>';
			$col_harga .='<div class="text-muted">Disc : ('.number_format($r->jasapelayanan_disc,0).')</div>';
			$aksi .= '</div>';
			$tabel .='<tr>';
			// $tabel .='<td class="text-center">'.$no.'</td>';
			$tabel .='<td class="text-center">'.$col_tindakan.'</td>';
			$tabel .='<td class="text-left">'.$r->nama_tindakan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
			$tabel .='<td class="text-right">'.$col_harga.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			$tabel .='<td class="text-center">'.($aksi).'<input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idpelayanan.'"></td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			// $tabel .='<tr>';
				// $tabel .='<td colspan="4" width="60%"  class="text-right"><label id="total_kuantitas">TOTAL</label></td>';
				// $tabel .='<td class="text-right" width="5%"><label id="total_penjualan">2 QTY</label></td>';
				// $tabel .='<td width="12%">2333</td>';
				// $tabel .='<td width="23%"></td>';
			// $tabel .='</tr>';
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	
	function hapus_layanan(){
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$hasil=$this->db->delete('tpoliklinik_pelayanan');
		$this->output->set_output(json_encode($hasil));
	}
	
	
	
	
	function get_edit_layanan(){
		$id=$this->input->post('id');
		$q="SELECT H.*
			FROM `tpoliklinik_pelayanan` H
			
			WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$hasil['tgltransaksi']=HumanDateShort($hasil['tanggal_transaksi']);
		$hasil['waktutransaksi']=HumanTime($hasil['tanggal_transaksi']);
		$this->output->set_output(json_encode($hasil));
	}
	
	
	function list_history_layanan(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_transaksi) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_transaksi) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT H.pendaftaran_id,MP.nopendaftaran,MP.tanggal,CASE WHEN MP.idtipe=1 THEN 'POLIKLLINIK' ELSE 'IGD' END as tipe_poli,P.nama as nama_poli
					,MD.nama as nama_dokter,H.tanggal_transaksi,MP.tanggaldaftar
					,SUM(H.totalkeseluruhan) as total_trx FROM tpoliklinik_pelayanan H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					INNER JOIN mpoliklinik P ON P.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					WHERE H.idpasien='$idpasien' AND H.pendaftaran_id != '$pendaftaran_id'
					GROUP BY H.pendaftaran_id
					ORDER BY MP.id DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$btn_lihat='<button class="btn btn-primary  btn-xs" onclick="lihat_data_layanan('.$r->pendaftaran_id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat Detail</button>';
		$result[] = $no;
		$aksi='';
		$aksi_print='<button class="btn btn-success  btn-xs"  type="button" title="Print" type="button"><i class="fa fa-print"></i></button>';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_lihat;	
		$aksi .= $aksi_print;	
		$aksi .= '</div>';
		$result[] = ($r->nopendaftaran);
		$result[] = HumanDateLong($r->tanggaldaftar);
		$result[] = ($r->tipe_poli);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dokter);
		$result[] = number_format($r->total_trx,0);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  
  function lihat_data_layanan(){
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$q="SELECT M.nama,MR.ref as jenis_tenaga,D.nama as nama_tindakan, H.* 
			FROM `tpoliklinik_pelayanan` H
			INNER JOIN mtarif_rawatjalan D ON D.id=H.idpelayanan
			INNER JOIN mppa M ON M.id=mppa_id
			INNER JOIN merm_referensi MR ON MR.ref_head_id='21' AND MR.nilai=H.profesi_id
			WHERE H.pendaftaran_id='$pendaftaran_id' 
			ORDER BY H.tanggal_transaksi ASC";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		$no=0;
		foreach ($list as $r) {	
		$no++;
		$btn_edit='<button class="btn btn-warning  btn-xs" onclick="copy_layanan('.$r->id.')" type="button" title="Copy Layanan" type="button"><i class="fa fa-copy"></i></button>';
		// $result[] = $no;
		$aksi='';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_edit;	
		$col_tindakan='<div class=""><strong>'.$r->jenis_tenaga.'</strong></div><div class="">'.$r->nama.'</div><div class="text-primary">['.HumanDateLong($r->tanggal_transaksi).']</div>';
		$col_harga='<div class="text-primary">'.number_format($r->jasapelayanan,0).'</div>';
		$col_harga .='<div class="text-muted">Disc : ('.number_format($r->jasapelayanan_disc,0).')</div>';
		$aksi .= '</div>';
		$tabel .='<tr>';
		// $tabel .='<td class="text-center">'.$no.'</td>';
		$tabel .='<td class="text-center">'.$col_tindakan.'</td>';
		$tabel .='<td class="text-left">'.$r->nama_tindakan.'</td>';
		$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
		$tabel .='<td class="text-right">'.$col_harga.'</td>';
		$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
		$tabel .='<td class="text-center">'.($aksi).'<input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idpelayanan.'"></td>';
		$tabel .='</tr>';
		$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
		$total_kuantitas=$total_kuantitas+$r->kuantitas;
	  }
		// $tabel .='<tr>';
			// $tabel .='<td colspan="4" width="60%"  class="text-right"><label id="total_kuantitas">TOTAL</label></td>';
			// $tabel .='<td class="text-right" width="5%"><label id="total_penjualan">2 QTY</label></td>';
			// $tabel .='<td width="12%">2333</td>';
			// $tabel .='<td width="23%"></td>';
		// $tabel .='</tr>';
	  $arr['tabel']=$tabel;
	  $arr['total_kuantitas']=number_format($total_kuantitas,0);
	  $arr['total_penjualan']=number_format($total_penjualan,0);
	  $this->output->set_output(json_encode($arr));
  }
  
  
  
  function copy_layanan(){
		$user_id=$this->session->userdata('user_id');
		$id=$this->input->post('id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$idpasien=$this->input->post('idpasien');
		$iddokter=$this->input->post('iddokter');
		$mppa_id=$this->input->post('mppa_id');
		$profesi_id=$this->input->post('profesi_id');
		
		
		$q="
			INSERT INTO tpoliklinik_pelayanan (iddokter,idpelayanan,jasasarana,jasasarana_disc,jasapelayanan,jasapelayanan_disc,bhp,bhp_disc,biayaperawatan,biayaperawatan_disc,total,kuantitas,diskon,totalkeseluruhan,statusverifikasi,status,tanggal_transaksi,iduser_transaksi,tanggal_verifikasi,iduser_verifikasi,pajak_dokter,pot_rs,periode_pembayaran,periode_jatuhtempo,status_jasamedis,pendaftaran_id,idpasien,profesi_id,mppa_id,created_date,created_by)
			SELECT 
			'$iddokter',idpelayanan,jasasarana,jasasarana_disc,jasapelayanan,jasapelayanan_disc,bhp,bhp_disc,biayaperawatan,biayaperawatan_disc,total,kuantitas,diskon,totalkeseluruhan,statusverifikasi,status,NOW(),iduser_transaksi,tanggal_verifikasi,iduser_verifikasi,pajak_dokter,pot_rs,periode_pembayaran,periode_jatuhtempo,status_jasamedis,'$pendaftaran_id','$idpasien','$profesi_id',mppa_id,NOW(),'$user_id'
			FROM `tpoliklinik_pelayanan` 
			WHERE id='$id'
		";
		$hasil=$this->db->query($q);
		
		$this->output->set_output(json_encode($hasil));
		// $this->output->set_output(json_encode($hasil));
	}
	
	function create_paket_layanan(){
		$user_id=$this->session->userdata('user_id');
		$data=array(
			'created_date' =>date('Y-m-d H:i:s'),
			'created_by' =>$user_id,
			'nama_paket' =>'',
			'status_paket' =>'1',
			'total' =>'0',
			'st_edited' =>'0',

		);
		$this->db->insert('mpaket_layanan',$data);
		$this->output->set_output(json_encode($data));
	}
	function list_history_layanan_paket(){
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tgl_input_1=$this->input->post('tgl_input_1');
		$tgl_input_1=$this->input->post('tgl_input_1');
		$nama_paket=$this->input->post('nama_paket');
		
		if ($nama_paket!=''){
			$where .=" AND H.nama_paket LIKE '%".$nama_paket."%'";
		}
		
		if ($tgl_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tgl_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tgl_input_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT H.*,SUM(MT.total * D.kuantitas) as total_harga,MU.`name` as nama_user 
					,GROUP_CONCAT(D.id) as detail_id
					FROM mpaket_layanan H
					INNER JOIN mpaket_layanan_detail D ON D.paket_id=H.id
					INNER JOIN mtarif_rawatjalan MT ON MT.id=D.idpelayanan
					INNER JOIN musers MU ON MU.id=H.created_by
					WHERE H.status_paket='2' AND H.staktif='1' ".$where."
					GROUP BY H.id
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama_paket');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		$detail_id="'".$r->detail_id."'";
		$result = array();
		$btn_lihat='<button class="btn btn-primary  btn-xs" onclick="lihat_data_layanan_paket('.$r->id.')" type="button" title="Detail" type="button"><i class="fa fa-eye"></i> Detail</button>';
		$btn_edit='<button class="btn btn-success  btn-xs" onclick="edit_layanan_paket('.$r->id.')" type="button" title="Edit Paket" type="button"><i class="fa fa-pencil"></i> Edit</button>';
		$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_layanan_paket('.$r->id.')" type="button" title="Hapus Paket" type="button"><i class="fa fa-trash"></i> Hapus</button>';
		$btn_copy='<button class="btn btn-warning  btn-xs" onclick="copy_layanan_paket('.$detail_id.')" type="button" title="Terapkan Paket" type="button"><i class="fa fa-copy"></i> Apply</button>';
		$result[] = $no;
		$aksi='';
		$aksi_print='<button class="btn btn-success  btn-xs"  type="button" title="Print" type="button"><i class="fa fa-print"></i></button>';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_lihat;	
		$aksi .= $btn_edit;	
		$aksi .= $btn_hapus;	
		$aksi .= $btn_copy;	
		$aksi .= '</div>';
		$result[] = ($r->nama_paket);
		$result[] = number_format($r->total_harga,0);
		$result[] = ($r->nama_user).'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  function simpan_paket_head(){
		$paket_id=$this->input->post('paket_id');
		$nama_paket=$this->input->post('nama_paket');
		$status_paket=$this->input->post('status_paket');
		$st_edited=$this->input->post('st_edited');
		$data=array(
			'nama_paket' => $nama_paket,
			'status_paket' => $status_paket,
		);
		if ($status_paket=='2'){
			$data['st_edited']=0;
		}
		// if ($st_edited=='0'){
			// $data['status_paket']=0;
			// $data['staktif']=0;
		// }else{
			
			// $data['status_paket']=2;
		// }
		$this->db->where('id',$paket_id);
		$hasil=$this->db->update('mpaket_layanan',$data);
		$this->output->set_output(json_encode($data));
		
	}
	
	function simpan_paket_layanan(){
		$user_id=$this->session->userdata('user_id');
		$paket_detail_id=$this->input->post('paket_detail_id');
		
		$data=array(
			'paket_id' => $this->input->post('paket_id'),
			'idpelayanan' => $this->input->post('idpelayanan'),
			'kuantitas' => $this->input->post('kuantitas'),
			
			'created_by' => $user_id,
			'created_date' => date('Y-m-d H:i:s'),

		);
		if ($paket_detail_id){
			$this->db->where('id',$paket_detail_id);
			$result=$this->db->update('mpaket_layanan_detail',$data);
		}else{
			$result=$this->db->insert('mpaket_layanan_detail',$data);
			
		}
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	public function get_layanan_detail_paket()
    {
		$id=$this->input->post('idpelayanan');
		$q="SELECT * FROM mtarif_rawatjalan WHERE id='$id'";
        $data_layanan = $this->db->query($q)->row();
        $this->output->set_output(json_encode($data_layanan));
    }
	function load_layanan_paket(){
		$paket_id=$this->input->post('paket_id');
		$q="
			SELECT H.paket_id,H.id,D.nama,H.idpelayanan,D.jasasarana,D.jasapelayanan,D.bhp,D.biayaperawatan,D.total,H.kuantitas,(D.total * H.kuantitas) as totalkeseluruhan FROM `mpaket_layanan_detail` H
			INNER JOIN mtarif_rawatjalan D ON D.id=H.idpelayanan
			WHERE H.paket_id='$paket_id'
		";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		$no=0;
		foreach ($list as $r) {	
			$no++;
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_paket_detail_layanan('.$r->id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			// $result[] = $no;
			$aksi='';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_paket_detail_layanan('.$r->id.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			$aksi .= '</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idpelayanan.'">'.$no.'. '.$r->nama.'</td>';
			// $tabel .='<td class="text-left">'.$r->nama.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasapelayanan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			
			$tabel .='<td class="text-center">'.($aksi).'</td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	
	function lihat_data_layanan_paket(){
		$paket_id=$this->input->post('paket_id');
		$q="
			SELECT H.paket_id,H.id,D.nama,H.idpelayanan,D.jasasarana,D.jasapelayanan,D.bhp,D.biayaperawatan,D.total,H.kuantitas,(D.total * H.kuantitas) as totalkeseluruhan FROM `mpaket_layanan_detail` H
			INNER JOIN mtarif_rawatjalan D ON D.id=H.idpelayanan
			WHERE H.paket_id='$paket_id'
		";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		foreach ($list as $r) {	
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_paket_detail_layanan('.$r->id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			// $result[] = $no;
			$aksi='';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_paket_detail_layanan('.$r->id.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			$aksi .= '</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idpelayanan.'">'.$r->nama.'</td>';
			// $tabel .='<td class="text-left">'.$r->nama.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasapelayanan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			
			// $tabel .='<td class="text-center">'.($aksi).'</td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	
	function get_edit_paket_layanan(){
		$id=$this->input->post('id');
		$q="SELECT H.paket_id,H.id,D.nama,H.idpelayanan,D.jasasarana,D.jasapelayanan,D.bhp,D.biayaperawatan,D.total,H.kuantitas,(D.total * H.kuantitas) as totalkeseluruhan FROM `mpaket_layanan_detail` H
			INNER JOIN mtarif_rawatjalan D ON D.id=H.idpelayanan
			WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	
	function hapus_paket_detail_layanan(){
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$hasil=$this->db->delete('mpaket_layanan_detail');
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_paket_edit_layanan(){
		$paket_id=$this->input->post('paket_id');
		$status_paket=$this->input->post('status_paket');
		$data=array(
			'st_edited' => 1,
			'status_paket' => 1,
		);
		
		$this->db->where('id',$paket_id);
		$hasil=$this->db->update('mpaket_layanan',$data);
		$this->output->set_output(json_encode($data));
		
	}
	
	// public function LoadLayananCopy()
    // {
        // $kelompok_pasien     = $this->input->post('idkelompokpasien');
        // $idunit     = $this->input->post('idunit');
        // $array_paket_id     	= $this->input->post('array_paket_id');
        
        // $from="(
				// SELECT D.id,M.nama,M.jasasarana,M.jasapelayanan,M.bhp,M.biayaperawatan,M.total,D.kuantitas,(D.kuantitas*M.total) as totalkeseluruhan 
				// FROM `mpaket_layanan_detail` D
				// INNER JOIN mtarif_rawatjalan M ON M.id=D.idpelayanan

				// WHERE D.id IN (".$array_paket_id.")
		// ) as tbl";
        // // print_r($from);exit();


        // $this->select = array();
        // $this->join 	= array();
        // $this->where  = array();

        // $this->order  = array();
        // $this->group  = array();
        // $this->from   = $from;
        // // print_r($from);exit();
        // $this->column_search   = array('kode','nama');
        // $this->column_order    = array('kode','nama');

        // $list = $this->datatable->get_datatables();
        // $data = array();
        // $no = $_POST['start'];
        // foreach ($list as $r) {
            // $no++;
            // $row = array();

            // $row[] = $no;
            // $row[] = $r->nama;
            // $row[] = $r->nama_satuan;
            // $row[] = number_format(ceiling($r->hargadasar + ($r->hargadasar*$r->margin/100),100),0);

            // $row[] = WarningStok($r->stok_asli, $r->stokreorder, $r->stokminimum);
            // $row[] = $r->catatan;
            // $aksi       = ($r->kuantitas>$r->stok_asli ? text_danger('TIDAK TERSEDIA'):text_primary('TERSEDIA'));
            // $row[] = $aksi;
            // $data[] = $row;
        // }
        // $output = array(
      // "draw" => $_POST['draw'],
      // "recordsTotal" => $this->datatable->count_all(),
      // "recordsFiltered" => $this->datatable->count_all(),
      // "data" => $data
    // );
        // echo json_encode($output);
  // }
  function LoadLayananCopy(){
	  $list_ppa_all = $this->Tpoliklinik_trx_model->list_ppa_all();
		$mppa_id_pelaksana_copy     	= $this->input->post('mppa_id_pelaksana_copy');
	  $opsi='<select tabindex="3" class="js-select2 form-control mppa_id" style="width: 100%;" data-placeholder="Pilih PPA Pelaksana" required>';
	  $opsi .='<option value="#" selected>Pilih Opsi</option>';
			foreach($list_ppa_all as $r){
			$opsi .='<option value="'.$r->id.'" '.($mppa_id_pelaksana_copy==$r->id?'selected':'').'>'.$r->nama.'</option>';
			}
			
		$opsi .='</select>';
		$array_paket_id     	= $this->input->post('array_paket_id');
		$q="
			SELECT D.id,D.idpelayanan,M.nama,M.jasasarana,M.jasapelayanan,M.bhp,M.biayaperawatan,M.total,D.kuantitas,(D.kuantitas*M.total) as totalkeseluruhan 
				FROM `mpaket_layanan_detail` D
				INNER JOIN mtarif_rawatjalan M ON M.id=D.idpelayanan

				WHERE D.id IN (".$array_paket_id.")
		";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		$no=0;
		foreach ($list as $r) {	
			$no++;
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_paket_detail_layanan('.$r->id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			// $result[] = $no;
			$aksi='';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_paket_detail_layanan('.$r->id.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			$aksi .= '</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-left">'.$no.'</td>';
			$tabel .='<td class="text-left">'.$opsi.'</td>';
			$tabel .='<td class="text-left"><input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idpelayanan.'"><input type="hidden" class="cls_kuantitas" value="'.$r->kuantitas.'">'.$r->nama.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasapelayanan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			
			// $tabel .='<td class="text-center">'.($aksi).'</td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	
	function terapkan_copy_paket(){
		$user_id=$this->session->userdata('user_id');
		$pendaftaran_id = $this->input->post('pendaftaran_id');
		$idpasien = $this->input->post('idpasien');
		$profesi_id = $this->input->post('profesi_id');
		$idpelayanan = $this->input->post('idpelayanan');
		$idpoliklinik = $this->input->post('idpoliklinik');
		$pertemuan_id = $this->input->post('pertemuan_id');
		$statuspasienbaru = $this->input->post('statuspasienbaru');
		$idtipe_poli = $this->input->post('idtipe_poli');
		$idkelompokpasien = $this->input->post('idkelompokpasien');
		$idrekanan = $this->input->post('idrekanan');
		$arr_idpelayanan = $this->input->post('arr_idpelayanan');
		$arr_idtrx = $this->input->post('arr_idtrx');
		$arr_kuantitas = $this->input->post('arr_kuantitas');
		$arr_mppa_id = $this->input->post('arr_mppa_id');
		$iddokter_ppa = $this->input->post('iddokter');
			// print_r($kelompokpasien.'-'.$idrekanan.'-'.$idtipe.'-'.$idpoli.'-'.$statuspasienbaru.'-'.$pertemuan_id.'-'.$headparent.'-'.$id);exit;
		$data_variable=array(
			'kelompokpasien' =>$idkelompokpasien,
			'idrekanan' =>$idrekanan,
			'idtipe' =>$idtipe_poli,
			'idpoli' =>$idpoliklinik,
			'statuspasienbaru' =>$statuspasienbaru,
			'pertemuan_id' =>$pertemuan_id,
		);
		foreach ($arr_mppa_id as $index=>$val){
			$data_harga=$this->get_harga_layanan($arr_idpelayanan[$index],$arr_kuantitas[$index],$data_variable);
			$data_ppa=$this->get_mppa($val,$iddokter_ppa);
			$data=array(
				'tanggal_transaksi'=> date('Y-m-d H:i:s'),
				'created_date' => date('Y-m-d H:i:s'),
				'created_by' => $user_id,
				'iddokter' => $data_ppa['iddokter'],
				'idpelayanan' => $arr_idpelayanan[$index],
				'status' => 1,
				'pendaftaran_id' => $this->input->post('pendaftaran_id'),
				'idpasien' => $this->input->post('idpasien'),
				'profesi_id' => $data_ppa['profesi_id'],
				'mppa_id' => $arr_mppa_id[$index],
			);
			$data=array_merge($data,$data_harga);
			$hasil=$this->db->insert('tpoliklinik_pelayanan',$data);
		
		}
		$this->output->set_output(json_encode($hasil));
	}
	function get_harga_layanan($idpelayanan,$kuantitas,$data_variable){
		$kelompokpasien=$data_variable['kelompokpasien'];
		$idrekanan=$data_variable['idrekanan'];
		$idtipe=$data_variable['idtipe'];
		$idpoli=$data_variable['idpoli'];
		$statuspasienbaru=$data_variable['statuspasienbaru'];
		$pertemuan_id=$data_variable['pertemuan_id'];
		
		$q="SELECT *FROM mtarif_rawatjalan WHERE id='$idpelayanan'";
		$row= $this->db->query($q)->row();
		if ($row){
			$headparent=$this->get_head_parent($row->path);
			$data_diskon=$this->Tpoliklinik_trx_model->get_logic_diskon_pelayanan($kelompokpasien,$idrekanan,$idtipe,$idpoli,$statuspasienbaru,$pertemuan_id,$headparent,$idpelayanan);
			// print_r($kelompokpasien.'-'.$idrekanan.'-'.$idtipe.'-'.$idpoli.'-'.$statuspasienbaru.'-'.$pertemuan_id.'-'.$headparent.'-'.$id);exit;
			$diskon_rp=0;
			$diskon_persen=0;
			$st_diskon=0;
			if ($data_diskon){
				if ($data_diskon->status_diskon_jasapelayanan=='1'){
					$st_diskon='1';
					if ($data_diskon->tipe_diskon_jasapelayanan=='1'){//PErsen
						$diskon_persen=$data_diskon->diskon_jasapelayanan;
						$diskon_rp=$diskon_persen/100 * $row->jasapelayanan;
					}else{
						$diskon_rp=$data_diskon->diskon_jasapelayanan;
						$diskon_persen =($diskon_rp * 100) / $row->jasapelayanan;
					}
				}
			}
			$jasapelayanan_disc=$diskon_rp;
			$total=$row->jasasarana+$row->jasapelayanan+$row->bhp+$row->biayaperawatan-$jasapelayanan_disc;
			$data_pelayanan=array(
				'jasasarana' => $row->jasasarana,
				'jasapelayanan' => $row->jasapelayanan,
				'jasapelayanan_disc' => $jasapelayanan_disc,
				'bhp' => $row->bhp,
				'biayaperawatan' => $row->biayaperawatan,
				'total' => $total,
				'kuantitas' => $kuantitas,
				'diskon' => 0,
				'totalkeseluruhan' => $kuantitas *  $total,
			);
		}
		return $data_pelayanan;
	}
	function get_mppa($id,$iddokter){
		$q="SELECT *FROM mppa WHERE id='$id'";
		$row= $this->db->query($q)->row();
		if ($row){
			$data_ppa=array();
			if ($row->tipepegawai=='2'){
				$data_ppa['iddokter']=$row->pegawai_id;
			}else{
				$data_ppa['iddokter']=$iddokter;
				
			}
				$data_ppa['profesi_id']=$row->jenis_profesi_id;
		}
		return $data_ppa;
	}
	
	//FISIO
	public function select2_layanan_fisio()
    {
        $rowpath 	= $this->input->post('rowpath');
        $q="
			SELECT *FROM mtarif_fisioterapi H
			WHERE H.headerpath LIKE '".$rowpath."%' 
			
			ORDER BY H.path ASC
				";
		$opsi='<option value="" selected>-Pilih Tindakan-</option>';
	  $hasil=$this->db->query($q)->result();
	  foreach ($hasil as $r){
		$opsi .='<option value="'.$r->id.'" '.($r->level=='0'?'disabled':'').'>'.TreeView($r->level,$r->nama).'</option>';
		  
	  }
	  $this->output->set_output(json_encode($opsi));
    }
	public function get_layanan_fisio_detail()
    {
		$id=$this->input->post('idpelayanan');
		$idtipe=$this->input->post('idtipe_poli');
		$idkelas=$this->input->post('idkelas');
		// print_r($idtipe);exit;
		$kelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		if ($kelompokpasien!='1'){
			$idrekanan=0;
		}
		$idunit=$this->input->post('idunit');
		$pertemuan_id=$this->input->post('pertemuan_id');
		$statuspasienbaru=$this->input->post('statuspasienbaru');
		$idpoli=$this->input->post('idpoliklinik');
		$idtipe_poli=$this->input->post('idtipe_poli');
		// $idtipe=$this->input->post('idtipe');
		$q="SELECT P.id as headerparent,H.* FROM mtarif_fisioterapi_detail H 
			INNER JOIN mtarif_fisioterapi D ON D.id=H.idtarif
			INNER JOIN mtarif_fisioterapi P ON P.path=D.headerpath
			WHERE H.idtarif='$id' AND H.kelas='$idkelas'
			";
        $data_layanan = $this->db->query($q)->row();
		$headparent=($data_layanan->headerparent);
		$data_diskon=$this->Tpoliklinik_trx_model->get_logic_diskon_pelayanan_fisio($kelompokpasien,$idrekanan,$idtipe,$idpoli,$statuspasienbaru,$pertemuan_id,$headparent,$id);
		// print_r($kelompokpasien.'-'.$idrekanan.'-'.$idtipe.'-'.$idpoli.'-'.$statuspasienbaru.'-'.$pertemuan_id.'-'.$headparent.'-'.$id);exit;
		$diskon_rp=0;
		$diskon_persen=0;
		$st_diskon=0;
		if ($data_diskon){
			if ($data_diskon->status_diskon_jasapelayanan=='1'){
				$st_diskon='1';
				if ($data_diskon->tipe_diskon_jasapelayanan=='1'){//PErsen
					$diskon_persen=$data_diskon->diskon_jasapelayanan;
					$diskon_rp=$diskon_persen/100 * $data_layanan->jasapelayanan;
				// print_r('sini');exit;
				}else{
					$diskon_rp=$data_diskon->diskon_jasapelayanan;
					$diskon_persen =($diskon_rp * 100) / $data_layanan->jasapelayanan;
				}
			}
		}
		
		$data_layanan->st_diskon=$st_diskon;
		$data_layanan->diskon_rp=$diskon_rp;
		$data_layanan->diskon_persen=$diskon_persen;
        $this->output->set_output(json_encode($data_layanan));
    }
	public function get_layanan_fisio_detail_paket()
    {
		$id=$this->input->post('idpelayanan');
		$idtipe=$this->input->post('idtipe_poli');
		// print_r($idtipe);exit;
		$kelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		if ($kelompokpasien!='1'){
			$idrekanan=0;
		}
		$idunit=$this->input->post('idunit');
		$pertemuan_id=$this->input->post('pertemuan_id');
		$statuspasienbaru=$this->input->post('statuspasienbaru');
		$idpoli=$this->input->post('idpoliklinik');
		$idtipe_poli=$this->input->post('idtipe_poli');
		// $idtipe=$this->input->post('idtipe');
		$q="SELECT P.id as headerparent,H.* FROM mtarif_fisioterapi_detail H 
			INNER JOIN mtarif_fisioterapi D ON D.id=H.idtarif
			INNER JOIN mtarif_fisioterapi P ON P.path=D.headerpath
			WHERE H.idtarif='$id' AND H.kelas=0
			";
        $data_layanan = $this->db->query($q)->row();
		$headparent=($data_layanan->headerparent);
		$data_diskon=$this->Tpoliklinik_trx_model->get_logic_diskon_pelayanan_fisio($kelompokpasien,$idrekanan,$idtipe,$idpoli,$statuspasienbaru,$pertemuan_id,$headparent,$id);
		// print_r($kelompokpasien.'-'.$idrekanan.'-'.$idtipe.'-'.$idpoli.'-'.$statuspasienbaru.'-'.$pertemuan_id.'-'.$headparent.'-'.$id);exit;
		$diskon_rp=0;
		$diskon_persen=0;
		$st_diskon=0;
		if ($data_diskon){
			if ($data_diskon->status_diskon_jasapelayanan=='1'){
				$st_diskon='1';
				if ($data_diskon->tipe_diskon_jasapelayanan=='1'){//PErsen
					$diskon_persen=$data_diskon->diskon_jasapelayanan;
					$diskon_rp=$diskon_persen/100 * $data_layanan->jasapelayanan;
				// print_r('sini');exit;
				}else{
					$diskon_rp=$data_diskon->diskon_jasapelayanan;
					$diskon_persen =($diskon_rp * 100) / $data_layanan->jasapelayanan;
				}
			}
		}
		
		$data_layanan->st_diskon=$st_diskon;
		$data_layanan->diskon_rp=$diskon_rp;
		$data_layanan->diskon_persen=$diskon_persen;
        $this->output->set_output(json_encode($data_layanan));
    }
	function simpan_layanan_fisio(){
		$user_id=$this->session->userdata('user_id');
		$tanggal_transaksi= YMDFormat($this->input->post('tgltransaksi')). ' ' .$this->input->post('waktutransaksi');
		$transaksi_id=$this->input->post('transaksi_id');
		$idrujukan=$this->input->post('idrujukan');
		$data=array(
			'pendaftaran_id'=>$this->input->post('pendaftaran_id'),
			'idpasien'=>$this->input->post('idpasien'),
		);
		
		$this->db->where('id',$idrujukan);
		$this->db->update('trujukan_fisioterapi',$data);
		if ($transaksi_id){
			$this->db->where('id',$transaksi_id);
			$this->db->delete('trujukan_fisioterapi_detail');
		}
		$data=array(
			'idrujukan'=> $idrujukan,
			'tanggal_transaksi'=> $tanggal_transaksi,
			'created_date' => date('Y-m-d H:i:s'),
			'created_by' => $user_id,
			'iddokter' => $this->input->post('iddokter'),
			'idfisioterapi' => $this->input->post('idpelayanan'),
			'jasasarana' => RemoveComma($this->input->post('jasasarana')),
			'jasasarana_disc' => RemoveComma($this->input->post('jasasarana_disc')),
			'jasapelayanan' => RemoveComma($this->input->post('jasapelayanan')),
			'jasapelayanan_disc' => RemoveComma($this->input->post('jasapelayanan_disc')),
			'bhp' => RemoveComma($this->input->post('bhp')),
			'bhp_disc' => RemoveComma($this->input->post('bhp_disc')),
			'biayaperawatan' => RemoveComma($this->input->post('biayaperawatan')),
			'biayaperawatan_disc' => RemoveComma($this->input->post('biayaperawatan_disc')),
			'total' => RemoveComma($this->input->post('total')),
			'kuantitas' => RemoveComma($this->input->post('kuantitas')),
			'diskon' => RemoveComma($this->input->post('diskon')),
			'totalkeseluruhan' => RemoveComma($this->input->post('totalkeseluruhan')),
			'status' => 1,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
			'profesi_id' => $this->input->post('profesi_id'),
			'mppa_id' => $this->input->post('mppa_id'),

		);
		
		$result=$this->db->insert('trujukan_fisioterapi_detail',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function load_transaksi_layanan_fisio_list(){
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$idrujukan=$this->input->post('idrujukan');
		$q="SELECT M.nama,MR.ref as jenis_tenaga,D.nama as nama_tindakan, H.* 
			FROM `trujukan_fisioterapi_detail` H
			INNER JOIN mtarif_fisioterapi D ON D.id=H.idfisioterapi
			
			LEFT  JOIN mppa M ON M.id=mppa_id
			LEFT JOIN merm_referensi MR ON MR.ref_head_id='21' AND MR.nilai=H.profesi_id
			WHERE H.idrujukan='$idrujukan' 
			";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		$no=0;
		foreach ($list as $r) {	
		$no++;
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_layanan_fisio('.$r->id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			// $result[] = $no;
			$aksi='';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_layanan_fisio('.$r->id.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			$col_tindakan='<div class=""><strong>'.$r->jenis_tenaga.'</strong></div><div class="">'.$r->nama.'</div><div class="text-primary">['.HumanDateLong($r->tanggal_transaksi).']</div>';
			$col_harga='<div class="text-primary">'.number_format($r->jasapelayanan,0).'</div>';
			$col_harga .='<div class="text-muted">Disc : ('.number_format($r->jasapelayanan_disc,0).')</div>';
			$aksi .= '</div>';
			$tabel .='<tr>';
			// $tabel .='<td class="text-center">'.$no.'</td>';
			$tabel .='<td class="text-center">'.$col_tindakan.'</td>';
			$tabel .='<td class="text-left">'.$r->nama_tindakan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
			$tabel .='<td class="text-right">'.$col_harga.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			$tabel .='<td class="text-center">'.($aksi).'<input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idfisioterapi.'"></td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			// $tabel .='<tr>';
				// $tabel .='<td colspan="4" width="60%"  class="text-right"><label id="total_kuantitas">TOTAL</label></td>';
				// $tabel .='<td class="text-right" width="5%"><label id="total_penjualan">2 QTY</label></td>';
				// $tabel .='<td width="12%">2333</td>';
				// $tabel .='<td width="23%"></td>';
			// $tabel .='</tr>';
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	function get_edit_layanan_fisio(){
		$id=$this->input->post('id');
		$q="SELECT H.*
			FROM `trujukan_fisioterapi_detail` H
			
			WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$hasil['tgltransaksi']=HumanDateShort($hasil['tanggal_transaksi']);
		$hasil['waktutransaksi']=HumanTime($hasil['tanggal_transaksi']);
		$this->output->set_output(json_encode($hasil));
	}
	function list_history_layanan_fisio(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_transaksi) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_transaksi) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT H.pendaftaran_id,MP.nopendaftaran,MP.tanggal,CASE WHEN MP.idtipe=1 THEN 'POLIKLLINIK' ELSE 'IGD' END as tipe_poli,P.nama as nama_poli
					,MD.nama as nama_dokter,H.tanggal as tanggal_transaksi,MP.tanggaldaftar
					,SUM(D.totalkeseluruhan) as total_trx 
					FROM trujukan_fisioterapi H
					INNER JOIN trujukan_fisioterapi_detail D ON D.idrujukan=H.id
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					INNER JOIN mpoliklinik P ON P.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					WHERE H.idpasien='$idpasien' AND H.pendaftaran_id !='$pendaftaran_id'
					GROUP BY H.id
					ORDER BY MP.id DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$btn_lihat='<button class="btn btn-primary  btn-xs" onclick="lihat_data_layanan_fisio('.$r->pendaftaran_id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat Detail</button>';
		$result[] = $no;
		$aksi='';
		$aksi_print='<button class="btn btn-success  btn-xs"  type="button" title="Print" type="button"><i class="fa fa-print"></i></button>';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_lihat;	
		$aksi .= $aksi_print;	
		$aksi .= '</div>';
		$result[] = ($r->nopendaftaran);
		$result[] = HumanDateLong($r->tanggaldaftar);
		$result[] = ($r->tipe_poli);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dokter);
		$result[] = number_format($r->total_trx,0);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  function lihat_data_layanan_fisio(){
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$q="SELECT M.nama,MR.ref as jenis_tenaga,D.nama as nama_tindakan, H.* 
			FROM `trujukan_fisioterapi_detail` H
			INNER JOIN mtarif_fisioterapi D ON D.id=H.idfisioterapi
			INNER JOIN mppa M ON M.id=mppa_id
			INNER JOIN merm_referensi MR ON MR.ref_head_id='21' AND MR.nilai=H.profesi_id
			WHERE H.pendaftaran_id='$pendaftaran_id' 
			ORDER BY H.tanggal_transaksi ASC";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		$no=0;
		foreach ($list as $r) {	
		$no++;
		$btn_edit='<button class="btn btn-warning  btn-xs" onclick="copy_layanan_fisio('.$r->id.')" type="button" title="Copy Layanan" type="button"><i class="fa fa-copy"></i></button>';
		// $result[] = $no;
		$aksi='';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_edit;	
		$col_tindakan='<div class=""><strong>'.$r->jenis_tenaga.'</strong></div><div class="">'.$r->nama.'</div><div class="text-primary">['.HumanDateLong($r->tanggal_transaksi).']</div>';
		$col_harga='<div class="text-primary">'.number_format($r->jasapelayanan,0).'</div>';
		$col_harga .='<div class="text-muted">Disc : ('.number_format($r->jasapelayanan_disc,0).')</div>';
		$aksi .= '</div>';
		$tabel .='<tr>';
		// $tabel .='<td class="text-center">'.$no.'</td>';
		$tabel .='<td class="text-center">'.$col_tindakan.'</td>';
		$tabel .='<td class="text-left">'.$r->nama_tindakan.'</td>';
		$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
		$tabel .='<td class="text-right">'.$col_harga.'</td>';
		$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
		$tabel .='<td class="text-center">'.($aksi).'<input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idfisioterapi.'"></td>';
		$tabel .='</tr>';
		$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
		$total_kuantitas=$total_kuantitas+$r->kuantitas;
	  }
		// $tabel .='<tr>';
			// $tabel .='<td colspan="4" width="60%"  class="text-right"><label id="total_kuantitas">TOTAL</label></td>';
			// $tabel .='<td class="text-right" width="5%"><label id="total_penjualan">2 QTY</label></td>';
			// $tabel .='<td width="12%">2333</td>';
			// $tabel .='<td width="23%"></td>';
		// $tabel .='</tr>';
	  $arr['tabel']=$tabel;
	  $arr['total_kuantitas']=number_format($total_kuantitas,0);
	  $arr['total_penjualan']=number_format($total_penjualan,0);
	  $this->output->set_output(json_encode($arr));
  }
  
  function copy_layanan_fisio(){
		$user_id=$this->session->userdata('user_id');
		$id=$this->input->post('id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$idpasien=$this->input->post('idpasien');
		$iddokter=$this->input->post('iddokter');
		$mppa_id=$this->input->post('mppa_id');
		$profesi_id=$this->input->post('profesi_id');
		$idrujukan=$this->input->post('idrujukan');
		
		
		$q="
			INSERT INTO trujukan_fisioterapi_detail (idrujukan,iddokter,idfisioterapi,jasasarana,jasasarana_disc,jasapelayanan,jasapelayanan_disc,bhp,bhp_disc,biayaperawatan,biayaperawatan_disc,total,kuantitas,diskon,totalkeseluruhan,pajak_dokter,potongan_rs,periode_pembayaran,periode_jatuhtempo,status_jasamedis,pendaftaran_id,idpasien,profesi_id,mppa_id,created_date,created_by)
			SELECT 
			'$idrujukan','$iddokter',idfisioterapi,jasasarana,jasasarana_disc,jasapelayanan,jasapelayanan_disc,bhp,bhp_disc,biayaperawatan,biayaperawatan_disc,total,kuantitas,diskon,totalkeseluruhan,pajak_dokter,potongan_rs,periode_pembayaran,periode_jatuhtempo,status_jasamedis,'$pendaftaran_id','$idpasien','$profesi_id',mppa_id,NOW(),'$user_id'
			FROM `trujukan_fisioterapi_detail` 
			WHERE id='$id'
		";
		$hasil=$this->db->query($q);
		
		$this->output->set_output(json_encode($hasil));
		// $this->output->set_output(json_encode($hasil));
	}
	
	function create_paket_layanan_fisio(){
		$user_id=$this->session->userdata('user_id');
		$data=array(
			'created_date' =>date('Y-m-d H:i:s'),
			'created_by' =>$user_id,
			'nama_paket' =>'',
			'status_paket' =>'1',
			'total' =>'0',
			'st_edited' =>'0',

		);
		$this->db->insert('mpaket_fisio',$data);
		$this->output->set_output(json_encode($data));
	}
	
	function simpan_paket_head_fisio(){
		$paket_id=$this->input->post('paket_id');
		$nama_paket=$this->input->post('nama_paket');
		$status_paket=$this->input->post('status_paket');
		$st_edited=$this->input->post('st_edited');
		$data=array(
			'nama_paket' => $nama_paket,
			'status_paket' => $status_paket,
		);
		if ($status_paket=='2'){
			$data['st_edited']=0;
		}
		// if ($st_edited=='0'){
			// $data['status_paket']=0;
			// $data['staktif']=0;
		// }else{
			
			// $data['status_paket']=2;
		// }
		$this->db->where('id',$paket_id);
		$hasil=$this->db->update('mpaket_fisio',$data);
		$this->output->set_output(json_encode($data));
		
	}
	function simpan_paket_layanan_fisio(){
		$user_id=$this->session->userdata('user_id');
		$paket_detail_id=$this->input->post('paket_detail_id');
		
		$data=array(
			'paket_id' => $this->input->post('paket_id'),
			'idtarif' => $this->input->post('idpelayanan'),
			'kuantitas' => $this->input->post('kuantitas'),
			
			'created_by' => $user_id,
			'created_date' => date('Y-m-d H:i:s'),

		);
		if ($paket_detail_id){
			$this->db->where('id',$paket_detail_id);
			$result=$this->db->update('mpaket_fisio_detail',$data);
		}else{
			$result=$this->db->insert('mpaket_fisio_detail',$data);
			
		}
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function load_layanan_fisio_paket(){
		$paket_id=$this->input->post('paket_id');
		$q="
			SELECT H.paket_id,H.id,T.nama,H.idtarif idpelayanan,D.jasasarana,D.jasapelayanan,D.bhp,D.biayaperawatan,D.total,H.kuantitas,(D.total * H.kuantitas) as totalkeseluruhan 
			FROM `mpaket_fisio_detail` H
			INNER JOIN mtarif_fisioterapi_detail D ON D.idtarif=H.idtarif AND D.kelas=0
			INNER JOIN mtarif_fisioterapi T ON T.id=D.idtarif
			WHERE H.paket_id='$paket_id'
		";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		$no=0;
		foreach ($list as $r) {	
			$no++;
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_paket_detail_layanan_fisio('.$r->id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			// $result[] = $no;
			$aksi='';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_paket_detail_layanan_fisio('.$r->id.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			$aksi .= '</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idpelayanan.'">'.$no.'. '.$r->nama.'</td>';
			// $tabel .='<td class="text-left">'.$r->nama.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasapelayanan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			
			$tabel .='<td class="text-center">'.($aksi).'</td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	function list_history_layanan_fisio_paket(){
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tgl_input_1=$this->input->post('tgl_input_1');
		$tgl_input_1=$this->input->post('tgl_input_1');
		$nama_paket=$this->input->post('nama_paket');
		
		if ($nama_paket!=''){
			$where .=" AND H.nama_paket LIKE '%".$nama_paket."%'";
		}
		
		if ($tgl_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tgl_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tgl_input_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT H.*,SUM(MD.total * D.kuantitas) as total_harga,MU.`name` as nama_user 
					,GROUP_CONCAT(D.id) as detail_id
					FROM mpaket_fisio H
					INNER JOIN mpaket_fisio_detail D ON D.paket_id=H.id
					INNER JOIN mtarif_fisioterapi_detail MD ON MD.idtarif=D.idtarif AND MD.kelas=0
					
					INNER JOIN musers MU ON MU.id=H.created_by
					WHERE H.status_paket='2' AND H.staktif='1' ".$where."
					GROUP BY H.id
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama_paket');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		$detail_id="'".$r->detail_id."'";
		$result = array();
		$btn_lihat='<button class="btn btn-primary  btn-xs" onclick="lihat_data_layanan_fisio_paket('.$r->id.')" type="button" title="Detail" type="button"><i class="fa fa-eye"></i> Detail</button>';
		$btn_edit='<button class="btn btn-success  btn-xs" onclick="edit_layanan_fisio_paket('.$r->id.')" type="button" title="Edit Paket" type="button"><i class="fa fa-pencil"></i> Edit</button>';
		$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_layanan_fisio_paket('.$r->id.')" type="button" title="Hapus Paket" type="button"><i class="fa fa-trash"></i> Hapus</button>';
		$btn_copy='<button class="btn btn-warning  btn-xs" onclick="copy_layanan_fisio_paket('.$detail_id.')" type="button" title="Terapkan Paket" type="button"><i class="fa fa-copy"></i> Apply</button>';
		$result[] = $no;
		$aksi='';
		$aksi_print='<button class="btn btn-success  btn-xs"  type="button" title="Print" type="button"><i class="fa fa-print"></i></button>';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_lihat;	
		$aksi .= $btn_edit;	
		$aksi .= $btn_hapus;	
		$aksi .= $btn_copy;	
		$aksi .= '</div>';
		$result[] = ($r->nama_paket);
		$result[] = number_format($r->total_harga,0);
		$result[] = ($r->nama_user).'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function lihat_data_layanan_fisio_paket(){
		$paket_id=$this->input->post('paket_id');
		$q="
			SELECT H.paket_id,H.id,T.nama,H.idtarif idpelayanan,D.jasasarana,D.jasapelayanan,D.bhp,D.biayaperawatan,D.total,H.kuantitas,(D.total * H.kuantitas) as totalkeseluruhan 
			FROM `mpaket_fisio_detail` H
			INNER JOIN mtarif_fisioterapi_detail D ON D.idtarif=H.idtarif AND D.kelas=0
			INNER JOIN mtarif_fisioterapi T ON T.id=D.idtarif
			WHERE H.paket_id='$paket_id'
		";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		foreach ($list as $r) {	
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_paket_detail_layanan_fisio('.$r->id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			// $result[] = $no;
			$aksi='';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_paket_detail_layanan_fisio('.$r->id.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			$aksi .= '</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idpelayanan.'">'.$r->nama.'</td>';
			// $tabel .='<td class="text-left">'.$r->nama.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasapelayanan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			
			// $tabel .='<td class="text-center">'.($aksi).'</td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	function simpan_paket_edit_layanan_fisio(){
		$paket_id=$this->input->post('paket_id');
		$status_paket=$this->input->post('status_paket');
		$data=array(
			'st_edited' => 1,
			'status_paket' => 1,
		);
		
		$this->db->where('id',$paket_id);
		$hasil=$this->db->update('mpaket_fisio',$data);
		$this->output->set_output(json_encode($data));
		
	}
	function hapus_paket_detail_layanan_fisio(){
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$hasil=$this->db->delete('mpaket_fisio_detail');
		$this->output->set_output(json_encode($hasil));
	}
	function get_edit_paket_layanan_fisio(){
		$id=$this->input->post('id');
		// $q="SELECT H.paket_id,H.id,D.nama,H.idpelayanan,D.jasasarana,D.jasapelayanan,D.bhp,D.biayaperawatan,D.total,H.kuantitas,(D.total * H.kuantitas) as totalkeseluruhan 
			// FROM `mpaket_fisio_detail` H
			// INNER JOIN mtarif_rawatjalan D ON D.id=H.idpelayanan
			// WHERE H.id='$id'";
		$q="SELECT H.paket_id,H.id,T.nama,H.idtarif idpelayanan,D.jasasarana,D.jasapelayanan,D.bhp,D.biayaperawatan,D.total,H.kuantitas,(D.total * H.kuantitas) as totalkeseluruhan 
			FROM `mpaket_fisio_detail` H
			INNER JOIN mtarif_fisioterapi_detail D ON D.idtarif=H.idtarif AND D.kelas=0
			INNER JOIN mtarif_fisioterapi T ON T.id=D.idtarif
			WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	
	function batal_paket_layanan_fisio(){
		$paket_id=$this->input->post('paket_id');
		
		$st_edited=$this->input->post('st_edited');
		
		if ($st_edited=='0'){
			$data['status_paket']=0;
			$data['staktif']=0;
		}else{
			
			$data['status_paket']=2;
			$data['st_edited']=0;
		}
		
		$this->db->where('id',$paket_id);
		$hasil=$this->db->update('mpaket_fisio',$data);
		$this->output->set_output(json_encode($data));
		
	}
	function hapus_layanan_fisio_paket(){
		$id=$this->input->post('paket_id');
		$user_id=$this->session->userdata('user_id');
		$data=array(
			'deleted_date'=>date('Y-m-d H:i:s'),
			'deleted_by'=>$user_id,
			'staktif'=>0,
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('mpaket_fisio',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_layanan_fisio(){
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$hasil=$this->db->delete('trujukan_fisioterapi_detail');
		$this->output->set_output(json_encode($hasil));
	}
	function LoadLayananCopy_fisio(){
	  $list_ppa_all = $this->Tpoliklinik_trx_model->list_ppa_all();
		$mppa_id_pelaksana_copy     	= $this->input->post('mppa_id_pelaksana_copy');
	  $opsi='<select tabindex="3" class="js-select2 form-control mppa_id" style="width: 100%;" data-placeholder="Pilih PPA Pelaksana" required>';
	  $opsi .='<option value="#" selected>Pilih Opsi</option>';
			foreach($list_ppa_all as $r){
			$opsi .='<option value="'.$r->id.'" '.($mppa_id_pelaksana_copy==$r->id?'selected':'').'>'.$r->nama.'</option>';
			}
			
		$opsi .='</select>';
		$array_paket_id     	= $this->input->post('array_paket_id');
		$q="
			SELECT D.id,D.idtarif as idpelayanan,MT.nama,M.jasasarana,M.jasapelayanan,M.bhp,M.biayaperawatan,M.total,D.kuantitas,(D.kuantitas*M.total) as totalkeseluruhan 
				FROM `mpaket_fisio_detail` D
				INNER JOIN mtarif_fisioterapi_detail M ON M.idtarif=D.idtarif AND M.kelas=0
				INNER JOIN mtarif_fisioterapi MT ON MT.id=D.idtarif
				WHERE D.id IN (".$array_paket_id.")
		";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		$no=0;
		foreach ($list as $r) {	
			$no++;
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_paket_detail_layanan_fisio('.$r->id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			// $result[] = $no;
			$aksi='';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_paket_detail_layanan_fisio('.$r->id.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			$aksi .= '</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-left">'.$no.'</td>';
			$tabel .='<td class="text-left">'.$opsi.'</td>';
			$tabel .='<td class="text-left"><input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idpelayanan.'"><input type="hidden" class="cls_kuantitas" value="'.$r->kuantitas.'">'.$r->nama.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasapelayanan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			
			// $tabel .='<td class="text-center">'.($aksi).'</td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	function terapkan_copy_paket_fisio(){
		$user_id=$this->session->userdata('user_id');
		$idrujukan = $this->input->post('idrujukan');
		$pendaftaran_id = $this->input->post('pendaftaran_id');
		$idpasien = $this->input->post('idpasien');
		$profesi_id = $this->input->post('profesi_id');
		$idpelayanan = $this->input->post('idpelayanan');
		$idpoliklinik = $this->input->post('idpoliklinik');
		$pertemuan_id = $this->input->post('pertemuan_id');
		$statuspasienbaru = $this->input->post('statuspasienbaru');
		$idtipe_poli = $this->input->post('idtipe_poli');
		$idkelompokpasien = $this->input->post('idkelompokpasien');
		$idrekanan = $this->input->post('idrekanan');
		$arr_idpelayanan = $this->input->post('arr_idpelayanan');
		$arr_idtrx = $this->input->post('arr_idtrx');
		$arr_kuantitas = $this->input->post('arr_kuantitas');
		$arr_mppa_id = $this->input->post('arr_mppa_id');
		$iddokter_ppa = $this->input->post('iddokter');
			// print_r($kelompokpasien.'-'.$idrekanan.'-'.$idtipe.'-'.$idpoli.'-'.$statuspasienbaru.'-'.$pertemuan_id.'-'.$headparent.'-'.$id);exit;
		$data_variable=array(
			'kelompokpasien' =>$idkelompokpasien,
			'idrekanan' =>$idrekanan,
			'idtipe' =>$idtipe_poli,
			'idpoli' =>$idpoliklinik,
			'statuspasienbaru' =>$statuspasienbaru,
			'pertemuan_id' =>$pertemuan_id,
		);
		foreach ($arr_mppa_id as $index=>$val){
			$data_harga=$this->get_harga_layanan_fisio($arr_idpelayanan[$index],$arr_kuantitas[$index],$data_variable);
			$data_ppa=$this->get_mppa($val,$iddokter_ppa);
			$data=array(
				'idrujukan' => $idrujukan,
				'tanggal_transaksi'=> date('Y-m-d H:i:s'),
				'created_date' => date('Y-m-d H:i:s'),
				'created_by' => $user_id,
				'iddokter' => $data_ppa['iddokter'],
				'idfisioterapi' => $arr_idpelayanan[$index],
				'status' => 1,
				'pendaftaran_id' => $this->input->post('pendaftaran_id'),
				'idpasien' => $this->input->post('idpasien'),
				'profesi_id' => $data_ppa['profesi_id'],
				'mppa_id' => $arr_mppa_id[$index],
			);
			$data=array_merge($data,$data_harga);
			$hasil=$this->db->insert('trujukan_fisioterapi_detail',$data);
		
		}
		$this->output->set_output(json_encode($hasil));
	}
	function get_harga_layanan_fisio($idpelayanan,$kuantitas,$data_variable){
		$kelompokpasien=$data_variable['kelompokpasien'];
		$idrekanan=$data_variable['idrekanan'];
		$idtipe=$data_variable['idtipe'];
		$idpoli=$data_variable['idpoli'];
		$statuspasienbaru=$data_variable['statuspasienbaru'];
		$pertemuan_id=$data_variable['pertemuan_id'];
		
		$q="SELECT P.id as headerparent,H.* FROM mtarif_fisioterapi_detail H 
			INNER JOIN mtarif_fisioterapi D ON D.id=H.idtarif
			INNER JOIN mtarif_fisioterapi P ON P.path=D.headerpath
			WHERE H.idtarif='$idpelayanan' AND H.kelas=0
			";
		$row= $this->db->query($q)->row();
		if ($row){
			$headparent=$row->headerparent;
			$data_diskon=$this->Tpoliklinik_trx_model->get_logic_diskon_pelayanan_fisio($kelompokpasien,$idrekanan,$idtipe,$idpoli,$statuspasienbaru,$pertemuan_id,$headparent,$idpelayanan);
			// print_r($kelompokpasien.'-'.$idrekanan.'-'.$idtipe.'-'.$idpoli.'-'.$statuspasienbaru.'-'.$pertemuan_id.'-'.$headparent.'-'.$id);exit;
			$diskon_rp=0;
			$diskon_persen=0;
			$st_diskon=0;
			if ($data_diskon){
				if ($data_diskon->status_diskon_jasapelayanan=='1'){
					$st_diskon='1';
					if ($data_diskon->tipe_diskon_jasapelayanan=='1'){//PErsen
						$diskon_persen=$data_diskon->diskon_jasapelayanan;
						$diskon_rp=$diskon_persen/100 * $row->jasapelayanan;
					}else{
						$diskon_rp=$data_diskon->diskon_jasapelayanan;
						$diskon_persen =($diskon_rp * 100) / $row->jasapelayanan;
					}
				}
			}
			$jasapelayanan_disc=$diskon_rp;
			$total=$row->jasasarana+$row->jasapelayanan+$row->bhp+$row->biayaperawatan-$jasapelayanan_disc;
			$data_pelayanan=array(
				'jasasarana' => $row->jasasarana,
				'jasapelayanan' => $row->jasapelayanan,
				'jasapelayanan_disc' => $jasapelayanan_disc,
				'bhp' => $row->bhp,
				'biayaperawatan' => $row->biayaperawatan,
				'total' => $total,
				'kuantitas' => $kuantitas,
				'diskon' => 0,
				'totalkeseluruhan' => $kuantitas *  $total,
			);
		}
		return $data_pelayanan;
	}
	public function getTindakanRawatJalan_fisio()
	{
		
		$form_akses=$this->input->post('form_akses');
		$rowpath=$this->input->post('rowpath');
		$sub_parent=$this->input->post('sub_parent');
		$nama_tarif_filter=$this->input->post('nama_tarif_filter');
		$where='';
		if ($sub_parent!='#'){
			 $where .=" AND path LIKE '".$sub_parent."%'";
		}
		if ($nama_tarif_filter!=''){
			 $where .=" AND nama LIKE '%".$nama_tarif_filter."%'";
		}
        
       $from="
			(
				
			SELECT H.nama,H.level,H.idkelompok,D.* FROM mtarif_fisioterapi H
			INNER JOIN mtarif_fisioterapi_detail D ON D.idtarif=H.id AND D.kelas=0
			WHERE H.headerpath LIKE '".$rowpath."%' 
			
			ORDER BY H.path ASC
				
			) as tbl
		";
        $this->select = array();
        $this->join 	= array();
        $this->where  = array();

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        // print_r($from);exit();
        $this->column_search   = array('nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];

		
		
		foreach ($list as $r) {
			$no++;
			$row = [];

			if ($r->idkelompok == 1) {
				$namatarif = TreeView($r->level, $r->nama);
			} else {
				if ($form_akses=='paket'){
				$namatarif = '<a href="#" class="selectLayananPaket" data-nama="' . TreeView($r->level, $r->nama) . '" data-idpelayanan="' . $r->idtarif . '">' . TreeView($r->level, $r->nama) . '</a>';
					
				}else{
				$namatarif = '<a href="#" class="selectLayanan" data-nama="' . TreeView($r->level, $r->nama) . '" data-idpelayanan="' . $r->idtarif . '">' . TreeView($r->level, $r->nama) . '</a>';
					
				}
			}

			$row[] = $no;
			$row[] = $namatarif;
			$row[] = number_format($r->jasasarana);
			$row[] = number_format($r->jasapelayanan);
			$row[] = number_format($r->bhp);
			$row[] = number_format($r->biayaperawatan);
			$row[] = number_format($r->total);

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}
	function load_transaksi_layanan_fisio_ri_list(){
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$idrujukan=$this->input->post('idrujukan');
		$q="SELECT M.nama,MR.ref as jenis_tenaga,D.nama as nama_tindakan, H.* 
			FROM `trujukan_fisioterapi_detail` H
			INNER JOIN mtarif_fisioterapi D ON D.id=H.idfisioterapi
			
			LEFT  JOIN mppa M ON M.id=mppa_id
			LEFT JOIN merm_referensi MR ON MR.ref_head_id='21' AND MR.nilai=H.profesi_id
			WHERE H.idrujukan='$idrujukan' 
			";
			// print_r($q);exit;
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		$no=0;
		foreach ($list as $r) {	
		$no++;
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_layanan_fisio('.$r->id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			// $result[] = $no;
			$aksi='';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_layanan_fisio('.$r->id.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			$col_tindakan='<div class=""><strong>'.$r->jenis_tenaga.'</strong></div><div class="">'.$r->nama.'</div><div class="text-primary">['.HumanDateLong($r->tanggal_transaksi).']</div>';
			$col_harga='<div class="text-primary">'.number_format($r->jasapelayanan,0).'</div>';
			$col_harga .='<div class="text-muted">Disc : ('.number_format($r->jasapelayanan_disc,0).')</div>';
			$aksi .= '</div>';
			$tabel .='<tr>';
			// $tabel .='<td class="text-center">'.$no.'</td>';
			$tabel .='<td class="text-center">'.$col_tindakan.'</td>';
			$tabel .='<td class="text-left">'.$r->nama_tindakan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
			$tabel .='<td class="text-right">'.$col_harga.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			$tabel .='<td class="text-center">'.($aksi).'<input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idfisioterapi.'"></td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			// $tabel .='<tr>';
				// $tabel .='<td colspan="4" width="60%"  class="text-right"><label id="total_kuantitas">TOTAL</label></td>';
				// $tabel .='<td class="text-right" width="5%"><label id="total_penjualan">2 QTY</label></td>';
				// $tabel .='<td width="12%">2333</td>';
				// $tabel .='<td width="23%"></td>';
			// $tabel .='</tr>';
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	function create_assesmen_fisio_ri(){
		$user_id=$this->session->userdata('user_id');
		$norujukan=$this->db->query("SELECT  GetNoRujukan('FT',3) as norujukan")->row('norujukan');
		$pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$iddokterperujuk=$this->input->post('iddokter');
		$idpasien=$this->input->post('idpasien');
		$tanggal= YMDFormat($this->input->post('tanggalheader')). ' ' .$this->input->post('waktuheader');
		$user_id=$this->session->userdata('user_id');
		$data=array(
			'norujukan' =>$norujukan,
			'tanggal' =>$tanggal,
			'idtindakan' =>$pendaftaran_id_ranap,
			'iddokterperujuk' =>$iddokterperujuk,
			'idpegawai' =>'0',
			'asalrujukan' =>'3',
			'status' =>'1',
			'statusredirect' =>'0',
			'iduser_rujukan' =>$user_id,
			'tanggal_rujukan' =>date('Y-m-d H:i:s'),
			'pendaftaran_id' =>$pendaftaran_id_ranap,
			'idpasien' =>$idpasien,

		);
		$this->db->insert('trujukan_fisioterapi',$data);
		$this->output->set_output(json_encode($data));
	}
	function edit_rujukan_layanan_fisio(){
		$id=$this->input->post('id');
		$data['status']=1;
		
		$this->db->where('id',$id);
		$hasil=$this->db->update('trujukan_fisioterapi',$data);
		$this->output->set_output(json_encode($data));
		
	}
	function close_assesmen_fisio_ri(){
		$user_id=$this->session->userdata('user_id');
		$idrujukan=$this->input->post('idrujukan');
		$tanggal= YMDFormat($this->input->post('tanggalheader')). ' ' .$this->input->post('waktuheader');
		$data=array(
			
			'status' =>'3',
			'tanggal' =>$tanggal,
			

		);
		$this->db->where('id',$idrujukan);
		$this->db->update('trujukan_fisioterapi',$data);
		$this->output->set_output(json_encode($data));
	}
	function batal_assesmen_fisio_ri(){
		$user_id=$this->session->userdata('user_id');
		$idrujukan=$this->input->post('idrujukan');
		$data=array(
			
			'status' =>'0',
			

		);
		$this->db->where('id',$idrujukan);
		$this->db->update('trujukan_fisioterapi',$data);
		$this->output->set_output(json_encode($data));
	}
	function simpan_layanan_fisio_ri(){
		$user_id=$this->session->userdata('user_id');
		$tanggal_transaksi= YMDFormat($this->input->post('tgltransaksi')). ' ' .$this->input->post('waktutransaksi');
		$transaksi_id=$this->input->post('transaksi_id');
		$idrujukan=$this->input->post('idrujukan');
		$data=array(
			'pendaftaran_id'=>$this->input->post('pendaftaran_id_ranap'),
			'idpasien'=>$this->input->post('idpasien'),
		);
		
		$this->db->where('id',$idrujukan);
		$this->db->update('trujukan_fisioterapi',$data);
		if ($transaksi_id){
			$this->db->where('id',$transaksi_id);
			$this->db->delete('trujukan_fisioterapi_detail');
		}
		$data=array(
			'idrujukan'=> $idrujukan,
			'tanggal_transaksi'=> $tanggal_transaksi,
			'created_date' => date('Y-m-d H:i:s'),
			'created_by' => $user_id,
			'iddokter' => $this->input->post('iddokter'),
			'idfisioterapi' => $this->input->post('idpelayanan'),
			'jasasarana' => RemoveComma($this->input->post('jasasarana')),
			'jasasarana_disc' => RemoveComma($this->input->post('jasasarana_disc')),
			'jasapelayanan' => RemoveComma($this->input->post('jasapelayanan')),
			'jasapelayanan_disc' => RemoveComma($this->input->post('jasapelayanan_disc')),
			'bhp' => RemoveComma($this->input->post('bhp')),
			'bhp_disc' => RemoveComma($this->input->post('bhp_disc')),
			'biayaperawatan' => RemoveComma($this->input->post('biayaperawatan')),
			'biayaperawatan_disc' => RemoveComma($this->input->post('biayaperawatan_disc')),
			'total' => RemoveComma($this->input->post('total')),
			'kuantitas' => RemoveComma($this->input->post('kuantitas')),
			'diskon' => RemoveComma($this->input->post('diskon')),
			'totalkeseluruhan' => RemoveComma($this->input->post('totalkeseluruhan')),
			'status' => 1,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
			'profesi_id' => $this->input->post('profesi_id'),
			'mppa_id' => $this->input->post('mppa_id'),

		);
		
		$result=$this->db->insert('trujukan_fisioterapi_detail',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function list_history_layanan_fisio_ri(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		$statuskasir=$this->input->post('statuskasir');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%'";
			$where .=" OR MJ.nopendaftaran LIKE '%".$notransaksi."%')";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MJ.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MJ.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_transaksi) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_transaksi) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MJ.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MJ.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT H.pendaftaran_id,H.id
					,CASE WHEN H.asalrujukan='3' THEN MP.nopendaftaran ELSE MJ.nopendaftaran END as nopendaftaran
					,CASE WHEN H.asalrujukan='3' THEN MP.tanggaldaftar ELSE MJ.tanggaldaftar END as tanggaldaftar
					,CASE WHEN H.asalrujukan='3' THEN CONCAT(mruangan.nama,' - ',mkelas.nama,' - ',mbed.nama) ELSE P.nama END as nama_poli
					,CASE WHEN H.asalrujukan='3' THEN MD2.nama ELSE MD.nama END as nama_dokter
					,H.tanggal as tanggal_transaksi
					,SUM(D.totalkeseluruhan) as total_trx ,H.asalrujukan
					FROM trujukan_fisioterapi H
					INNER JOIN trujukan_fisioterapi_detail D ON D.idrujukan=H.id
					LEFT JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id AND asalrujukan='3'
					LEFT JOIN tpoliklinik_pendaftaran MJ ON MJ.id=H.pendaftaran_id AND asalrujukan!='3'
					LEFT JOIN mpoliklinik P ON P.id=MJ.idpoliklinik
					LEFT JOIN mdokter MD ON MD.id=MJ.iddokter
					LEFT JOIN mdokter MD2 ON MD2.id=H.iddokterperujuk
					LEFT JOIN mruangan  ON mruangan.id=MP.idruangan
					LEFT JOIN mkelas  ON mkelas.id=MP.idkelas
					LEFT JOIN mbed  ON mbed.id=MP.idbed
					WHERE H.idpasien='$idpasien' AND H.status='3' ".$where."
					GROUP BY H.id
					ORDER BY H.id DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$btn_lihat='<button class="btn btn-primary  btn-xs" onclick="lihat_data_layanan_fisio('.$r->id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat Detail</button>';
		$result[] = $no;
		$aksi='';
		$aksi_edit='';
		$aksi_print='<button class="btn btn-success  btn-xs"  type="button" title="Print" type="button"><i class="fa fa-print"></i></button>';
		if ($statuskasir!='2'){
		$aksi_edit='<button class="btn btn-success   btn-xs" onclick="edit_rujukan_layanan_fisio('.$r->id.')" type="button" title="Edit" type="button"><i class="fa fa-pencil"></i></button>';
			
		}
		
		$aksi = '<div class="btn-group">';
		$aksi .= $aksi_edit;	
		$aksi .= $btn_lihat;	
		$aksi .= $aksi_print;	
		$aksi .= '</div>';
		$result[] = ($r->nopendaftaran).($pendaftaran_id_ranap==$r->pendaftaran_id?'<br>'.text_primary('PENDAFTARAN INI'):'');
		$result[] = HumanDateLong($r->tanggaldaftar);
		$result[] = GetTipePasienPiutang($r->asalrujukan);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dokter);
		$result[] = number_format($r->total_trx,0);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function lihat_data_layanan_fisio_ri(){
		$id=$this->input->post('id');
		$idrujukan=$this->input->post('idrujukan');
		$q="SELECT M.nama,MR.ref as jenis_tenaga,D.nama as nama_tindakan, H.* 
			FROM `trujukan_fisioterapi_detail` H
			LEFT JOIN mtarif_fisioterapi D ON D.id=H.idfisioterapi
			LEFT JOIN mppa M ON M.id=mppa_id
			LEFT JOIN merm_referensi MR ON MR.ref_head_id='21' AND MR.nilai=H.profesi_id
			WHERE H.idrujukan='$id' 
			ORDER BY H.id ASC";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		$no=0;
		foreach ($list as $r) {	
		$no++;
		$btn_edit='<button class="btn btn-warning  btn-xs" '.($idrujukan==''?'disabled':'').' onclick="copy_layanan_fisio('.$r->id.')" type="button" title="Copy Layanan" type="button"><i class="fa fa-copy"></i></button>';
		// $result[] = $no;
		$aksi='';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_edit;	
		$col_tindakan='<div class=""><strong>'.$r->jenis_tenaga.'</strong></div><div class="">'.$r->nama.'</div><div class="text-primary">['.HumanDateLong($r->tanggal_transaksi).']</div>';
		$col_harga='<div class="text-primary">'.number_format($r->jasapelayanan,0).'</div>';
		$col_harga .='<div class="text-muted">Disc : ('.number_format($r->jasapelayanan_disc,0).')</div>';
		$aksi .= '</div>';
		$tabel .='<tr>';
		// $tabel .='<td class="text-center">'.$no.'</td>';
		$tabel .='<td class="text-center">'.$col_tindakan.'</td>';
		$tabel .='<td class="text-left">'.$r->nama_tindakan.'</td>';
		$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
		$tabel .='<td class="text-right">'.$col_harga.'</td>';
		$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
		$tabel .='<td class="text-center">'.($aksi).'<input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idfisioterapi.'"></td>';
		$tabel .='</tr>';
		$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
		$total_kuantitas=$total_kuantitas+$r->kuantitas;
	  }
		// $tabel .='<tr>';
			// $tabel .='<td colspan="4" width="60%"  class="text-right"><label id="total_kuantitas">TOTAL</label></td>';
			// $tabel .='<td class="text-right" width="5%"><label id="total_penjualan">2 QTY</label></td>';
			// $tabel .='<td width="12%">2333</td>';
			// $tabel .='<td width="23%"></td>';
		// $tabel .='</tr>';
	  $arr['tabel']=$tabel;
	  $arr['total_kuantitas']=number_format($total_kuantitas,0);
	  $arr['total_penjualan']=number_format($total_penjualan,0);
	  $this->output->set_output(json_encode($arr));
  }
  function copy_layanan_fisio_ri(){
		$user_id=$this->session->userdata('user_id');
		$id=$this->input->post('id');
		$pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$idpasien=$this->input->post('idpasien');
		$iddokter=$this->input->post('iddokter');
		$mppa_id=$this->input->post('mppa_id');
		$profesi_id=$this->input->post('profesi_id');
		$idrujukan=$this->input->post('idrujukan');
		
		
		$q="
			INSERT INTO trujukan_fisioterapi_detail (idrujukan,iddokter,idfisioterapi,jasasarana,jasasarana_disc,jasapelayanan,jasapelayanan_disc,bhp,bhp_disc,biayaperawatan,biayaperawatan_disc,total,kuantitas,diskon,totalkeseluruhan,pajak_dokter,potongan_rs,periode_pembayaran,periode_jatuhtempo,status_jasamedis,pendaftaran_id,idpasien,profesi_id,mppa_id,created_date,created_by)
			SELECT 
			'$idrujukan','$iddokter',idfisioterapi,jasasarana,jasasarana_disc,jasapelayanan,jasapelayanan_disc,bhp,bhp_disc,biayaperawatan,biayaperawatan_disc,total,kuantitas,diskon,totalkeseluruhan,pajak_dokter,potongan_rs,periode_pembayaran,periode_jatuhtempo,status_jasamedis,'$pendaftaran_id','$idpasien','$profesi_id',mppa_id,NOW(),'$user_id'
			FROM `trujukan_fisioterapi_detail` 
			WHERE id='$id'
		";
		$hasil=$this->db->query($q);
		
		$this->output->set_output(json_encode($hasil));
		// $this->output->set_output(json_encode($hasil));
	}
	function list_history_layanan_fisio_paket_ri(){
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tgl_input_1=$this->input->post('tgl_input_1');
		$tgl_input_1=$this->input->post('tgl_input_1');
		$nama_paket=$this->input->post('nama_paket');
		$idrujukan=$this->input->post('idrujukan');
		
		if ($nama_paket!=''){
			$where .=" AND H.nama_paket LIKE '%".$nama_paket."%'";
		}
		
		if ($tgl_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tgl_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tgl_input_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT H.*,SUM(MD.total * D.kuantitas) as total_harga,MU.`name` as nama_user 
					,GROUP_CONCAT(D.id) as detail_id
					FROM mpaket_fisio H
					INNER JOIN mpaket_fisio_detail D ON D.paket_id=H.id
					INNER JOIN mtarif_fisioterapi_detail MD ON MD.idtarif=D.idtarif AND MD.kelas=0
					
					INNER JOIN musers MU ON MU.id=H.created_by
					WHERE H.status_paket='2' AND H.staktif='1' ".$where."
					GROUP BY H.id
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama_paket');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		$detail_id="'".$r->detail_id."'";
		$result = array();
		$btn_lihat='<button class="btn btn-primary  btn-xs" onclick="lihat_data_layanan_fisio_paket('.$r->id.')" type="button" title="Detail" type="button"><i class="fa fa-eye"></i> Detail</button>';
		$btn_edit='<button class="btn btn-success  btn-xs" onclick="edit_layanan_fisio_paket('.$r->id.')" type="button" title="Edit Paket" type="button"><i class="fa fa-pencil"></i> Edit</button>';
		$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_layanan_fisio_paket('.$r->id.')" type="button" title="Hapus Paket" type="button"><i class="fa fa-trash"></i> Hapus</button>';
		$btn_copy='<button class="btn btn-warning  btn-xs" '.($idrujukan?'':'disabled').' onclick="copy_layanan_fisio_paket('.$detail_id.')" type="button" title="Terapkan Paket" type="button"><i class="fa fa-copy"></i> Apply</button>';
		$result[] = $no;
		$aksi='';
		$aksi_print='<button class="btn btn-success  btn-xs"  type="button" title="Print" type="button"><i class="fa fa-print"></i></button>';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_lihat;	
		$aksi .= $btn_edit;	
		$aksi .= $btn_hapus;	
		$aksi .= $btn_copy;	
		$aksi .= '</div>';
		$result[] = ($r->nama_paket);
		$result[] = number_format($r->total_harga,0);
		$result[] = ($r->nama_user).'<br>'.HumanDateLong($r->created_date);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function get_tarif_visite(){
	  $idtipe=$this->input->post('idtipe');
	  $idkelompokpasien=$this->input->post('idkelompokpasien');
	  $idrekanan=$this->input->post('idrekanan');
	  $idruangan=$this->input->post('idruangan');
	  $kategori_dokter=$this->input->post('idkategori_dokter');
	  $iddokter=$this->input->post('iddokter');
	  $q="SELECT H.idkategori FROM mdokter H WHERE H.id='$iddokter'";
	  $kategori_dokter=$this->db->query($q)->row('idkategori');
	  $idkelas=$this->input->post('idkelas');
	  if ($idkelompokpasien!='1'){
		  $q="
			SELECT D.* FROM setting_visite_tarif H
			INNER JOIN (
				SELECT 
			compare_value_4(
			MAX(IF(H.iddokter = '$iddokter' AND H.kategori_dokter='$kategori_dokter',H.id,NULL))
			,MAX(IF(H.iddokter = '$iddokter' AND H.kategori_dokter='0',H.id,NULL))
			,MAX(IF(H.iddokter = '0' AND H.kategori_dokter='$kategori_dokter',H.id,NULL))
			,MAX(IF(H.iddokter = '0' AND H.kategori_dokter='0',H.id,NULL))
			) as id

			FROM setting_visite_tarif H 
			WHERE H.idkelompokpasien='$idkelompokpasien' AND H.idruangan='$idruangan'
			) S ON S.id=H.id
			INNER JOIN mtarif_visitedokter_detail D ON D.idtarif=H.idtarif AND D.kelas='$idkelas'
		  ";
		  // print_r($q);exit;
	  }else{
		  $q="
			SELECT D.* FROM setting_visite_tarif H
			INNER JOIN (
				SELECT 
			compare_value_7(
			MAX(IF(H.iddokter = '$iddokter' AND H.kategori_dokter='$kategori_dokter' AND H.idrekanan='$idrekanan',H.id,NULL))
			,MAX(IF(H.iddokter = '$iddokter' AND H.kategori_dokter='0' AND H.idrekanan='$idrekanan',H.id,NULL))
			,MAX(IF(H.iddokter = '$iddokter' AND H.kategori_dokter='0' AND H.idrekanan='0',H.id,NULL))
			,MAX(IF(H.iddokter = '0' AND H.kategori_dokter='$kategori_dokter' AND H.idrekanan='$idrekanan',H.id,NULL))
			,MAX(IF(H.iddokter = '0' AND H.kategori_dokter='$kategori_dokter' AND H.idrekanan='0',H.id,NULL))
			,MAX(IF(H.iddokter = '0' AND H.kategori_dokter='0' AND H.idrekanan='$idrekanan',H.id,NULL))
			,MAX(IF(H.iddokter = '0' AND H.kategori_dokter='0' AND H.idrekanan='0',H.id,NULL))
			) as id

			FROM setting_visite_tarif H 
			WHERE H.idkelompokpasien='$idkelompokpasien' AND H.idruangan='$idruangan'
			) S ON S.id=H.id
			INNER JOIN mtarif_visitedokter_detail D ON D.idtarif=H.idtarif AND D.kelas='$idkelas'
		  ";
	  }
	  // print_r($q);exit;
      $data=$this->db->query($q)->row_array();
	  if ($data){
			$data_diskon=$this->Tpoliklinik_trx_model->get_logic_diskon_visite($idkelompokpasien,$idrekanan,$idtipe,$idruangan,$idkelas,$data['idtarif']);
			$diskon_rp=0;
			$diskon_persen=0;
			$st_diskon=0;
			if ($data_diskon){
				if ($data_diskon->status_diskon_jasapelayanan=='1'){
					$st_diskon='1';
					if ($data_diskon->tipe_diskon_jasapelayanan=='1'){//PErsen
						$diskon_persen=$data_diskon->diskon_jasapelayanan;
						$diskon_rp=$diskon_persen/100 * $data['jasapelayanan'];
					// print_r('sini');exit;
					}else{
						$diskon_rp=$data_diskon->diskon_jasapelayanan;
						$diskon_persen =($diskon_rp * 100) / $data['jasapelayanan'];
					}
				}
			}
			$data['diskon_rp']=$diskon_rp;
			$data['diskon_persen']=$diskon_persen;
	  }
	 
	  $this->output->set_output(json_encode($data));
  }
  
  function simpan_layanan_visite(){
		$user_id=$this->session->userdata('user_id');
		$tanggal_transaksi= YMDFormat($this->input->post('tgltransaksi'));
		$transaksi_id=$this->input->post('transaksi_id');
		$iddokter=$this->input->post('iddokter');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$idruangan=$this->input->post('idruangan');
		$idpelayanan=$this->input->post('idpelayanan');
		$idtipepasien=$this->input->post('idtipepasien');
		$idtipe=$this->input->post('idtipe');
		$idkelas=$this->input->post('idkelas');
		if ($transaksi_id){
			$this->db->where('id',$transaksi_id);
			$this->db->delete('trawatinap_visite');
		}
		$pajak_dokter=0;
		$potongan_rs=0;
		$q="SELECT * FROM mdokter H WHERE H.id='$iddokter'";
		$data_pajak=$this->db->query($q)->row();
		if ($idtipe=='1'){//RANAP
			
			$pajak_dokter=$data_pajak->pajakranap;
			if ($idtipepasien=='1'){//RS
				$potongan_rs=$data_pajak->potonganrsranap;
			}else{//Pasien Pribadi
				$potongan_rs=$data_pajak->potonganrsranapsiang;
			}
		}
		if ($idtipe=='2'){//RANAP
			
			$pajak_dokter=$data_pajak->pajakods;
			if ($idtipepasien=='1'){//RS
				$potongan_rs=$data_pajak->potonganrsods;
			}else{//Pasien Pribadi
				$potongan_rs=$data_pajak->potonganrsodssiang;
			}
		}
		$data=array(
			'idrawatinap'=> $pendaftaran_id,
			'tanggal'=> $tanggal_transaksi,
			'idruangan'=> $idruangan,
			'iddokter' => $this->input->post('iddokter'),
			'created_date' => date('Y-m-d H:i:s'),
			'created_by' => $user_id,
			'idpelayanan' => $this->input->post('idpelayanan'),
			'jasasarana' => RemoveComma($this->input->post('jasasarana')),
			'jasasarana_disc' => RemoveComma($this->input->post('jasasarana_disc')),
			'jasapelayanan' => RemoveComma($this->input->post('jasapelayanan')),
			'jasapelayanan_disc' => RemoveComma($this->input->post('jasapelayanan_disc')),
			'bhp' => RemoveComma($this->input->post('bhp')),
			'bhp_disc' => RemoveComma($this->input->post('bhp_disc')),
			'biayaperawatan' => RemoveComma($this->input->post('biayaperawatan')),
			'biayaperawatan_disc' => RemoveComma($this->input->post('biayaperawatan_disc')),
			'total' => RemoveComma($this->input->post('total')),
			'kuantitas' => RemoveComma($this->input->post('kuantitas')),
			'diskon' => RemoveComma($this->input->post('diskon')),
			'totalkeseluruhan' => RemoveComma($this->input->post('totalkeseluruhan')),
			'status' => 1,
			'statusverifikasi' => 0,
			'pajak_dokter' => $pajak_dokter,
			'potongan_rs' => $potongan_rs,
			'idkelas' => $idkelas,

		);
		
		$result=$this->db->insert('trawatinap_visite',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function load_transaksi_layanan_visite_list(){
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$q="SELECT MD.nama as nama_dokter,MR.nama as nama_ruangan,MK.nama as nama_kelas,MT.nama as nama_tarif,COUNT(MF.id) as jml_file
			,H.* FROM `trawatinap_visite` H
			INNER JOIN mdokter MD ON MD.id=H.iddokter
			LEFT JOIN  mruangan MR ON MR.id=H.idruangan
			LEFT JOIN mkelas MK ON MK.id=H.idkelas
			LEFT JOIN mtarif_visitedokter MT ON MT.id=H.idpelayanan
			LEFT JOIN trawatinap_visite_bukti MF ON MF.iddetail_visite=H.id
			WHERE H.idrawatinap='$pendaftaran_id'
			GROUP BY H.id
			ORDER BY H.tanggal DESC,H.id DESC";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		$no=0;
		foreach ($list as $r) {	
		$no++;
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_layanan('.$r->id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			// $result[] = $no;
			$aksi='';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_layanan('.$r->id.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			$btn_ttd='<button class="btn btn-'.($r->ttd_dokter?'default':'success').'  btn-xs" onclick="ttd_dokter('.$r->id.')" type="button" title="Tanda Tangan" type="button"><i class="fa fa-pencil-square-o"></i></button>';
			$btn_upload='<button class="btn btn-'.($r->jml_file>0?'default':'warning').'  btn-xs" onclick="upload_bukti('.$r->id.')" type="button" title="Upload" type="button"><i class="fa fa-upload"></i></button>';
			
			$aksi = '<div class="btn-group">';
			if ($r->statusverifikasi=='0'){
				$aksi .= $btn_edit;	
				$aksi .= $btn_hapus;	
				
			}
			$aksi .= $btn_ttd;	
			$aksi .= $btn_upload;	
			
			$col_harga='<div class="text-primary">'.number_format($r->jasapelayanan,0).'</div>';
			$col_harga .='<div class="text-muted">Disc : ('.number_format($r->jasapelayanan_disc,0).')</div>';
			$aksi .= '</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-center">'.$no.'</td>';
			$tabel .='<td class="text-left"><strong>'.HumanDateShort($r->tanggal).'</strong><br>'.$r->nama_dokter.'</td>';
			$tabel .='<td class="text-center">'.$r->nama_ruangan.'<br>'.$r->nama_kelas.'</td>';
			$tabel .='<td class="text-left">'.$r->nama_tarif.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
			$tabel .='<td class="text-right">'.$col_harga.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			$tabel .='<td class="text-center">'.($aksi).'<input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idpelayanan.'"><input type="hidden" class="cls_tanggal" value="'.HumanDateShort($r->tanggal).'"><input type="hidden" class="cls_iddokter" value="'.$r->iddokter.'"><input type="hidden" class="cls_idruangan" value="'.$r->idruangan.'"></td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			// $tabel .='<tr>';
				// $tabel .='<td colspan="4" width="60%"  class="text-right"><label id="total_kuantitas">TOTAL</label></td>';
				// $tabel .='<td class="text-right" width="5%"><label id="total_penjualan">2 QTY</label></td>';
				// $tabel .='<td width="12%">2333</td>';
				// $tabel .='<td width="23%"></td>';
			// $tabel .='</tr>';
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	function get_edit_layanan_visite(){
		$id=$this->input->post('id');
		$q="SELECT H.*
			FROM `trawatinap_visite` H
			
			WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$hasil['tgltransaksi']=HumanDateShort($hasil['tanggal']);
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_layanan_visite(){
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$hasil=$this->db->delete('trawatinap_visite');
		$this->output->set_output(json_encode($hasil));
	}
	public function upload_bukti() {
       $uploadDir = './assets/upload/upload_bukti_visite/';
	   if (!file_exists($uploadDir)) {
			mkdir($uploadDir, 0755, true);
		}
		if (!empty($_FILES)) {
			 $iddetail_visite = $this->input->post('iddetail_visite');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['iddetail_visite'] 	= $iddetail_visite;
			$detail['file_name']		= $file_name;
			$detail['size']		= formatSizeUnits($size);
			$detail['tanggal_upload']	= date('Y-m-d H:i:s');
			$detail['user_upload']	= $this->session->userdata('user_name');
			$this->db->insert('trawatinap_visite_bukti', $detail);
		}
    }
	function refresh_image_bukti_data($id){
		$q="SELECT  H.* from trawatinap_visite_bukti H

			WHERE H.iddetail_visite='$id'";
		$row= $this->db->query($q)->result();
		$tabel='';
		foreach ($row as $r){
			
			$tabel .='<tr>';
			$tabel .='<td class="text-left"><a href="'.base_url().'assets/upload/upload_bukti_visite/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a></td>';
			$tabel .='<td class="text-left">'.$r->size.'</td>';
			$tabel .='<td class="text-left">'.$r->user_upload.'-'.HumanDateLong($r->tanggal_upload).'</td>';
			$tabel .='<td class="text-left"><button class="btn btn-xs btn-danger hapus_file_bukti" title="Hapus"><li class="fa fa-trash-o"></li></button></td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
		}
		return $tabel;
	}
	function refresh_image_bukti($id){		
		$arr['detail'] = $this->refresh_image_bukti_data($id);
		$this->output->set_output(json_encode($arr));
	}
	function get_ttd_visit($id){		
		$q="SELECT *FROM trawatinap_visite WHERE id='$id'";
		$arr=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($arr));
	}
	function simpan_ttd_visite(){	
		$ttd_id=$this->input->post('ttd_id');
		$ttd_dokter=$this->input->post('ttd_dokter');
		
		$this->db->where('id',$ttd_id);
		$hasil=$this->db->update('trawatinap_visite',array('ttd_dokter'=>$ttd_dokter));
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_file_bukti(){
		$id=$this->input->post('id');
		
		$row = $this->db->query("SELECT *FROM trawatinap_visite_bukti WHERE id='$id'")->row();
		if(file_exists('./assets/upload/upload_bukti_visite/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/upload_bukti_visite/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from trawatinap_visite_bukti WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	function list_history_layanan_visite(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokterpenanggungjawab = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idkelas = '".$idpoli."'";
		}
		
		// if ($tanggal_input_1 !=''){
			// $where .=" AND DATE(H.tanggal_transaksi) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_transaksi) <='".YMDFormat($tanggal_input_2)."'";
		// }
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT H.idrawatinap as pendaftaran_id,MP.nopendaftaran,MP.tanggaldaftar,CASE WHEN MP.idtipe=1 THEN 'Rawat Inap' ELSE 'ODS' END as tujuan
					,CONCAT(MR.nama,' - ',MK.nama,' - ',MB.nama) as nama_poli
					,MD.nama as nama_dokter
					,SUM(H.totalkeseluruhan) as total_trx 
					FROM trawatinap_pendaftaran MP 
					INNER JOIN trawatinap_visite H ON MP.id=H.idrawatinap
					
					INNER JOIN mruangan MR ON MR.id=MP.idruangan
					INNER JOIN mkelas MK ON MK.id=MP.idkelas
					INNER JOIN mbed MB ON MB.id=MP.idbed
					INNER JOIN mdokter MD ON MD.id=MP.iddokterpenanggungjawab
					WHERE MP.idpasien='$idpasien' AND MP.id !='$pendaftaran_id' ".$where."
					GROUP BY H.idrawatinap
					ORDER BY MP.id DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$btn_lihat='<button class="btn btn-primary  btn-xs" onclick="lihat_data_layanan('.$r->pendaftaran_id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat Detail</button>';
		$result[] = $no;
		$aksi='';
		$aksi_print='<button class="btn btn-success  btn-xs"  type="button" title="Print" type="button"><i class="fa fa-print"></i></button>';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_lihat;	
		$aksi .= $aksi_print;	
		$aksi .= '</div>';
		$result[] = ($r->nopendaftaran);
		$result[] = HumanDateLong($r->tanggaldaftar);
		$result[] = ($r->tujuan);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dokter);
		$result[] = number_format($r->total_trx,0);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function lihat_data_layanan_visite(){
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$q="SELECT MD.nama as nama_dokter,MR.nama as nama_ruangan,MK.nama as nama_kelas,MT.nama as nama_tarif,COUNT(MF.id) as jml_file
			,H.* FROM `trawatinap_visite` H
			INNER JOIN mdokter MD ON MD.id=H.iddokter
			LEFT JOIN  mruangan MR ON MR.id=H.idruangan
			LEFT JOIN mkelas MK ON MK.id=H.idkelas
			LEFT JOIN mtarif_visitedokter MT ON MT.id=H.idpelayanan
			LEFT JOIN trawatinap_visite_bukti MF ON MF.iddetail_visite=H.id
			WHERE H.idrawatinap='$pendaftaran_id'
			GROUP BY H.id
			ORDER BY H.tanggal DESC,H.id DESC";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		$no=0;
		foreach ($list as $r) {	
		$no++;
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_layanan('.$r->id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			// $result[] = $no;
			$aksi='';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_layanan('.$r->id.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			$btn_ttd='<button class="btn btn-success  btn-xs" onclick="ttd_dokter('.$r->id.')" type="button" title="Tanda Tangan" type="button"><i class="fa fa-pencil-square-o"></i></button>';
			$btn_upload='<button class="btn btn-'.($r->jml_file>0?'default':'warning').'  btn-xs" onclick="upload_bukti('.$r->id.')" type="button" title="Upload" type="button"><i class="fa fa-upload"></i></button>';
			
			$aksi = '<div class="btn-group">';
			if ($r->statusverifikasi=='0'){
				$aksi .= $btn_edit;	
				$aksi .= $btn_hapus;	
				
			}
			$aksi .= $btn_ttd;	
			$aksi .= $btn_upload;	
			
			$col_harga='<div class="text-primary">'.number_format($r->jasapelayanan,0).'</div>';
			$col_harga .='<div class="text-muted">Disc : ('.number_format($r->jasapelayanan_disc,0).')</div>';
			$aksi .= '</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-center">'.$no.'</td>';
			$tabel .='<td class="text-left"><strong>'.HumanDateShort($r->tanggal).'</strong><br>'.$r->nama_dokter.'</td>';
			$tabel .='<td class="text-center">'.$r->nama_ruangan.'<br>'.$r->nama_kelas.'</td>';
			$tabel .='<td class="text-left">'.$r->nama_tarif.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
			$tabel .='<td class="text-right">'.$col_harga.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			// $tabel .='<td class="text-center">'.($aksi).'<input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idpelayanan.'"><input type="hidden" class="cls_tanggal" value="'.HumanDateShort($r->tanggal).'"><input type="hidden" class="cls_iddokter" value="'.$r->iddokter.'"><input type="hidden" class="cls_idruangan" value="'.$r->idruangan.'"></td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			// $tabel .='<tr>';
				// $tabel .='<td colspan="4" width="60%"  class="text-right"><label id="total_kuantitas">TOTAL</label></td>';
				// $tabel .='<td class="text-right" width="5%"><label id="total_penjualan">2 QTY</label></td>';
				// $tabel .='<td width="12%">2333</td>';
				// $tabel .='<td width="23%"></td>';
			// $tabel .='</tr>';
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
  }
  //BMHP right
  function simpan_bmhp_ri(){
		$user_id=$this->session->userdata('user_id');
		$tanggal_transaksi= YMDFormat($this->input->post('tgltransaksi'));
		$transaksi_id=$this->input->post('transaksi_id');
		$idrawatinap=$this->input->post('pendaftaran_id_ranap');
		$idtipe=$this->input->post('idtipe');
		if ($transaksi_id){
			$this->db->where('id',$transaksi_id);
			if ($idtipe=='3'){
				$this->db->delete('trawatinap_obat');
				
			}else{
				$this->db->delete('trawatinap_alkes');
				
			}
		}
		
		$data=array(
			'idrawatinap' => $idrawatinap,
			'tanggal'=> $tanggal_transaksi,
			'created_date' => date('Y-m-d H:i:s'),
			'idunit' => $this->input->post('idunit'),
			'idtipe' => $this->input->post('idtipe'),
			'created_by' => $user_id,
			'hargadasar' => RemoveComma($this->input->post('hargadasar')),
			'margin' => RemoveComma($this->input->post('margin')),
			'hargajual' => RemoveComma($this->input->post('hargajual')),
			'kuantitas' => RemoveComma($this->input->post('kuantitas')),
			'diskon' => RemoveComma($this->input->post('diskon')),
			'totalkeseluruhan' => RemoveComma($this->input->post('totalkeseluruhan')),
			'status'  => 1,
			'statusverifikasi'  => 0,
			// 'iduser_transaksi' => $user_id,
			'idpasien' => $this->input->post('idpasien'),
			// 'idpoliklinik' => $this->input->post('idpoliklinik'),
			'harga_before_diskon' => RemoveComma($this->input->post('harga_before_diskon')),
			'diskon_persen' => RemoveComma($this->input->post('diskon_persen')),
			'pembulatan' => RemoveComma($this->input->post('pembulatan')),


		);
		if ($idtipe=='3'){
			// $data['statusstok'] = $this->input->post('statusstok');
			$data['idobat'] = $this->input->post('idobat');
			$result=$this->db->insert('trawatinap_obat',$data);
		}else{
			$data['idalkes'] = $this->input->post('idobat');
			$result=$this->db->insert('trawatinap_alkes',$data);
		}
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=true;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function load_bmhp_list_ri(){
		$idrawatinap=$this->input->post('idrawatinap');
		$tabel_h="
			SELECT  
			id,idunit,idtipe,idalkes as idobat,hargadasar,margin,hargajual,kuantitas,diskon,totalkeseluruhan,statusverifikasi,status,tanggal,idrawatinap,idpasien,harga_before_diskon,diskon_persen,pembulatan,created_date,created_by
			FROM trawatinap_alkes WHERE idrawatinap='$idrawatinap'
			UNION ALL
			SELECT 
			id,idunit,idtipe,idobat,hargadasar,margin,hargajual,kuantitas,diskon,totalkeseluruhan,statusverifikasi,status,tanggal,idrawatinap,idpasien,harga_before_diskon,diskon_persen,pembulatan,created_date,created_by
			FROM trawatinap_obat WHERE idrawatinap='$idrawatinap'

		";
		$q="SELECT MU.nama as nama_unit,B.nama,B.idsatuan,B.namatipe,MS.nama as nama_satuan 
			,H.*,U.`name` as nama_user
			FROM (
				".$tabel_h."
			) H
			INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
			INNER JOIN munitpelayanan MU ON MU.id=H.idunit
			LEFT JOIN msatuan MS ON MS.id=B.idsatuan
			INNER JOIN musers U ON U.id=H.created_by 
			WHERE H.idrawatinap='$idrawatinap' 
			ORDER BY H.tanggal ASC";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		foreach ($list as $r) {	
		
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_bmhp('.$r->id.','.$r->idtipe.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_bmhp('.$r->id.','.$r->idtipe.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			
			// $result[] = $no;
			$aksi='';
			$aksi = '<div class="btn-group">';
			if ($r->statusverifikasi=='0'){
				$aksi .= $btn_edit;	
				$aksi .= $btn_hapus;	
			}
			$col_tipe='<div class="text-primary">['.strtoupper($r->namatipe).']</div><div class="">'.$r->nama.'</div>';
			$col_harga='<div class="h5 text-primary">Harga : Rp. '.number_format($r->hargajual,0).'</div>';
			$col_harga .='<div class="text-muted">Harga : Rp. '.number_format($r->harga_before_diskon,0).'</div>';
			$col_harga .='<div class="text-muted">Disc : Rp. ('.number_format($r->diskon,0).')</div>';
			$aksi .= '</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-center">'.$r->nama_unit.'</td>';
			$tabel .='<td class="text-center">'.HumanDateShort($r->tanggal).'</td>';
			$tabel .='<td class="text-left">'.$col_tipe.'</td>';
			$tabel .='<td class="text-right">'.$col_harga.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).' '.$r->nama_satuan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			$tabel .='<td class="text-center">'.($r->nama_user).'<br>'.HumanDateLong($r->created_date).'</td>';
			$tabel .='<td class="text-center">'.($aksi).'<input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idtipe" value="'.$r->idtipe.'"><input type="hidden" class="cls_idobat" value="'.$r->idobat.'"></td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			// $tabel .='<tr>';
				// $tabel .='<td colspan="4" width="60%"  class="text-right"><label id="total_kuantitas">TOTAL</label></td>';
				// $tabel .='<td class="text-right" width="5%"><label id="total_penjualan">2 QTY</label></td>';
				// $tabel .='<td width="12%">2333</td>';
				// $tabel .='<td width="23%"></td>';
			// $tabel .='</tr>';
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	public function get_obat_detail_ri()
    {
		$id=$this->input->post('idbarang');
		$idtipe=$this->input->post('idtipe');
		// print_r($idtipe);exit;
		$kelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		if ($kelompokpasien!='1'){
			$idrekanan=0;
		}
		$idunit=$this->input->post('idunit');
		// $statuspasienbaru=$this->input->post('statuspasienbaru');
		// $idpoli=$this->input->post('idpoliklinik');
		$tujuan_pasien=$this->input->post('asal_rujukan');
		$idruangan=$this->input->post('idruangan');
		$idkelas=$this->input->post('idkelas');
		
        $data_obat = $this->Tpoliklinik_trx_model->get_obat_detail($id, $kelompokpasien, $idtipe,$idunit);
		// print_r($data_obat);exit;
		$harga_before_diskon=ceiling(($data_obat->margin * $data_obat->hargadasar / 100) + ($data_obat->hargadasar),100);
		// $harga_before_diskon->hargajual=$hargajual;
		$data_obat->harga_before_diskon=$harga_before_diskon;
		$idbarang=$id;
		$q_diskon="
			SELECT M.* FROM (
			SELECT compare_value_9(
			MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND  H.idruangan='$idruangan' AND H.idkelas='$idkelas',H.id,NULL))
			,MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND  H.idruangan='$idruangan' AND H.idkelas='0',H.id,NULL))
			,MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND H.idruangan='0' AND H.idkelas='0',H.id,NULL))
			,MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='0' AND H.idruangan='0' AND H.idkelas='0',H.id,NULL))
			,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe' AND H.idruangan='$idruangan' AND H.idkelas='$idkelas',H.id,NULL))
			,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe' AND H.idruangan='0' AND H.idkelas='$idkelas',H.id,NULL))
			,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe' AND H.idruangan='$idruangan' AND H.idkelas='0',H.id,NULL))
			,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe' AND H.idruangan='0' AND H.idkelas='0',H.id,NULL))
			,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.idruangan='0' AND H.idkelas='0',H.id,NULL))
			
			) as id FROM merm_pengaturan_diskon_ranap_bmhp H WHERE H.tujuan_pasien='$tujuan_pasien' 
				AND H.idkelompokpasien='$kelompokpasien' AND (H.idrekanan='$idrekanan' OR H.idrekanan='0')
			
			) T INNER JOIN merm_pengaturan_diskon_ranap_bmhp M
			WHERE M.id=T.id

		";
		// print_r($q_diskon);exit;
		$data_diskon=$this->db->query($q_diskon)->row();
		$diskon_rp=0;
		$diskon_persen=0;
		if ($data_diskon){
			
			if ($data_diskon->operand!=0){
				if ($data_diskon->operand=1){
					$diskon_persen=$data_diskon->diskon;
					$diskon_rp=ceiling($harga_before_diskon * $diskon_persen / 100,100);
				}else{
					$diskon_persen=0;
					$diskon_rp=ceiling($data_diskon->diskon,100);
				}
			}
		}
		$hargajual_before=$harga_before_diskon-$diskon_rp;
		$hargajual=ceiling($hargajual_before,100);
		$pembulatan=$hargajual-$hargajual_before;
		$data_obat->hargajual=$hargajual;
		$data_obat->diskon_rp=$diskon_rp;
		$data_obat->diskon_persen=$diskon_persen;
		$data_obat->pembulatan=$pembulatan;
        $this->output->set_output(json_encode($data_obat));
    }
	function hapus_bmhp_ri(){
		$id=$this->input->post('id');
		$idtipe=$this->input->post('idtipe');
		
		$this->db->where('id',$id);
		if ($idtipe=='3'){
		$hasil=$this->db->delete('trawatinap_obat');
			
		}else{
			
		$hasil=$this->db->delete('trawatinap_alkes');
		}
		$this->output->set_output(json_encode($hasil));
	}
	function terapkan_copy_ri(){
	  $user_id=$this->session->userdata('user_id');
		$idunit=$this->input->post('idunit');
		$kelompok_pasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$idbarang=$this->input->post('idbarang');
		$tujuan_pasien=$this->input->post('asal_rujukan');
		$idruangan=$this->input->post('idruangan');
		$idkelas=$this->input->post('idkelas');
		$idrawatinap=$this->input->post('pendaftaran_id');
		$idpasien=$this->input->post('idpasien');
		if ($kelompok_pasien!='1'){
			$idrekanan=0;
		}
		  $q="
			SELECT S.stok as stok_asli,MS.nama as nama_satuan 
					,B.*,H.kuantitas
					,(CASE
					WHEN $kelompok_pasien = 5 THEN
						marginumum
					WHEN $kelompok_pasien = 0 THEN
						marginumum
					WHEN $kelompok_pasien = 1 THEN
						marginasuransi
					WHEN $kelompok_pasien = 2 THEN
						marginjasaraharja
					WHEN $kelompok_pasien = 3 THEN
						marginbpjskesehatan
					WHEN $kelompok_pasien = 4 THEN
						marginbpjstenagakerja
				END) AS margin
					FROM `mpaket_detail` H
					LEFT JOIN mgudang_stok S ON S.idtipe=H.idtipe AND S.idbarang=H.idobat AND S.idunitpelayanan='$idunit'
					INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
					LEFT JOIN msatuan MS ON MS.id=B.idsatuan
					WHERE H.id IN (".$idbarang.")
		  ";
		$list_barang=$this->db->query($q)->result();
		  // print_r($list_barang);exit;
		$jml_insert=0;
		foreach($list_barang as $r){
			if ($r->kuantitas<=$r->stok_asli){
				$jml_insert=$jml_insert+1;
				$kelompokpasien=$kelompok_pasien;
				$idbarang=$r->id;
				$idtipe=$r->idtipe;
				$kuantitas=$r->kuantitas;
				$hargadasar=$r->hargadasar;
				$harga_before_diskon=(ceiling($r->hargadasar + ($r->hargadasar*$r->margin/100),100));
				$q_diskon="
					SELECT M.* FROM (
					SELECT compare_value_9(
					MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND  H.idruangan='$idruangan' AND H.idkelas='$idkelas',H.id,NULL))
					,MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND  H.idruangan='$idruangan' AND H.idkelas='0',H.id,NULL))
					,MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND H.idruangan='0' AND H.idkelas='0',H.id,NULL))
					,MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='0' AND H.idruangan='0' AND H.idkelas='0',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe' AND H.idruangan='$idruangan' AND H.idkelas='$idkelas',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe' AND H.idruangan='0' AND H.idkelas='$idkelas',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe' AND H.idruangan='$idruangan' AND H.idkelas='0',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe' AND H.idruangan='0' AND H.idkelas='0',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.idruangan='0' AND H.idkelas='0',H.id,NULL))
					
					) as id FROM merm_pengaturan_diskon_ranap_bmhp H WHERE H.tujuan_pasien='$tujuan_pasien' 
						AND H.idkelompokpasien='$kelompokpasien' AND (H.idrekanan='$idrekanan' OR H.idrekanan='0')
					
					) T INNER JOIN merm_pengaturan_diskon_ranap_bmhp M
					WHERE M.id=T.id

				";
					$data_diskon=$this->db->query($q_diskon)->row();
					$diskon_rp=0;
					$diskon_persen=0;
					if ($data_diskon){
						if ($data_diskon->operand!=0){
							if ($data_diskon->operand=1){
								$diskon_persen=$data_diskon->diskon;
								$diskon_rp=ceiling($harga_before_diskon * $diskon_persen / 100,100);
							}else{
								$diskon_persen=0;
								$diskon_rp=ceiling($data_diskon->diskon,100);
							}
						}
					}else{
						
					}
					// print_r($harga_before_diskon);exit;
				$hargajual_before=$harga_before_diskon-$diskon_rp;
				$hargajual=ceiling($hargajual_before,100);	
				
				$data=array(
					'idrawatinap' => $idrawatinap,
					'tanggal'=>  date('Y-m-d'),
					'created_date' => date('Y-m-d H:i:s'),
					'idunit' => $this->input->post('idunit'),
					'idtipe' => $idtipe,
					'created_by' => $user_id,
					'hargadasar' => RemoveComma($hargadasar),
					'margin' => ($r->margin),
					'hargajual' => $hargajual,
					'kuantitas' => ($kuantitas),
					'diskon' => $diskon_rp,
					'totalkeseluruhan' => $hargajual * $kuantitas,
					'status'  => 1,
					'statusverifikasi'  => 0,
					// 'iduser_transaksi' => $user_id,
					'idpasien' => $this->input->post('idpasien'),
					// 'idpoliklinik' => $this->input->post('idpoliklinik'),
					'harga_before_diskon' => ($harga_before_diskon),
					'diskon_persen' => $diskon_persen,
					'pembulatan' => 0,


				);
				if ($idtipe=='3'){
					// $data['statusstok'] = $this->input->post('statusstok');
					$data['idobat'] = $r->id;
					$result=$this->db->insert('trawatinap_obat',$data);
				}else{
					$data['idalkes'] = $r->id;
					$result=$this->db->insert('trawatinap_alkes',$data);
				}
			}
		}
		$this->output->set_output(json_encode($jml_insert));
	  
  }
  function list_history_bmhp_ri(){
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokterpenanggungjawab = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idkelas = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT H.idrawatinap as pendaftaran_id,MP.nopendaftaran,MP.tanggaldaftar,CASE WHEN MP.idtipe=1 THEN 'Rawat Inap' ELSE 'ODS' END as tujuan
					,CONCAT(MR.nama,' - ',MK.nama,' - ',MB.nama) as nama_poli
					,MD.nama as nama_dokter
					,SUM(H.totalkeseluruhan) as total_trx 
					FROM trawatinap_pendaftaran MP 
					INNER JOIN (
						SELECT idrawatinap,tanggal, totalkeseluruhan FROM `trawatinap_obat` 
						UNION ALL
						SELECT idrawatinap,tanggal, totalkeseluruhan FROM `trawatinap_alkes` 
					) H ON MP.id=H.idrawatinap
					INNER JOIN mruangan MR ON MR.id=MP.idruangan
					INNER JOIN mkelas MK ON MK.id=MP.idkelas
					INNER JOIN mbed MB ON MB.id=MP.idbed
					INNER JOIN mdokter MD ON MD.id=MP.iddokterpenanggungjawab
					WHERE MP.idpasien='$idpasien' AND MP.id !='$pendaftaran_id' ".$where."
					GROUP BY H.idrawatinap
					ORDER BY MP.id DESC
				) as tbl
			";
			// 
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$btn_lihat='<button class="btn btn-primary  btn-xs" onclick="lihat_data_bmhp('.$r->pendaftaran_id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat Detail</button>';
		$result[] = $no;
		$aksi='';
		$aksi_print='<button class="btn btn-success  btn-xs"  type="button" title="Print" type="button"><i class="fa fa-print"></i></button>';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_lihat;	
		$aksi .= $aksi_print;	
		$aksi .= '</div>';
		$result[] = ($r->nopendaftaran);
		$result[] = HumanDateLong($r->tanggaldaftar);
		$result[] = ($r->tujuan);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dokter);
		$result[] = number_format($r->total_trx,0);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function lihat_data_bmhp_ri(){
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$q="SELECT MU.nama as nama_unit,B.nama,B.idsatuan,B.namatipe,MS.nama as nama_satuan 
			,H.*,U.`name` as nama_user
			FROM (
				SELECT id,idrawatinap,tanggal as tanggal_transaksi,idtipe,idunit,idobat,hargajual,kuantitas,harga_before_diskon,diskon, totalkeseluruhan,created_date,created_by FROM `trawatinap_obat` 
				UNION ALL
				SELECT id,idrawatinap,tanggal as tanggal_transaksi,idtipe,idunit,idalkes as idobat,hargajual,kuantitas,harga_before_diskon,diskon, totalkeseluruhan,created_date,created_by FROM `trawatinap_alkes` 
			) H
			INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
			INNER JOIN munitpelayanan MU ON MU.id=H.idunit
			LEFT JOIN msatuan MS ON MS.id=B.idsatuan
			INNER JOIN musers U ON U.id=H.created_by 
			WHERE H.idrawatinap='$pendaftaran_id' 
			ORDER BY H.tanggal_transaksi ASC";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		foreach ($list as $r) {	
			$btn_edit='<button class="btn btn-warning  btn-xs" onclick="copy_bmhp('.$r->id.','.$r->idtipe.')" type="button" title="Copy Data" type="button"><i class="fa fa-copy"></i></button>';
			// $result[] = $no;
			$aksi='';
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= '</div>';
			$col_tipe='<div class="text-primary">['.strtoupper($r->namatipe).']</div><div class="">'.$r->nama.'</div>';
			$col_harga='<div class="h5 text-primary">Harga : Rp. '.number_format($r->hargajual,0).'</div>';
			$col_harga .='<div class="text-muted">Harga : Rp. '.number_format($r->harga_before_diskon,0).'</div>';
			$col_harga .='<div class="text-muted">Disc : Rp. ('.number_format($r->diskon,0).')</div>';
			$tabel .='<tr>';
			$tabel .='<td class="text-center">'.$r->nama_unit.'</td>';
			$tabel .='<td class="text-center">'.HumanDateLong($r->tanggal_transaksi).'</td>';
			$tabel .='<td class="text-left">'.$col_tipe.'</td>';
			$tabel .='<td class="text-right">'.$col_harga.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).' '.$r->nama_satuan.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			$tabel .='<td class="text-center">'.($r->nama_user).'<br>'.HumanDateLong($r->created_date).'</td>';
			$tabel .='<td class="text-center">'.($aksi).'</td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	function copy_bmhp_ri(){
		
		$user_id=$this->session->userdata('user_id');
		$idtipe=$this->input->post('idtipe');
		$idunit=$this->input->post('idunit');
		$kelompok_pasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$idbarang=$this->input->post('id');
		$tujuan_pasien=$this->input->post('asal_rujukan');
		$idruangan=$this->input->post('idruangan');
		$idkelas=$this->input->post('idkelas');
		$idrawatinap=$this->input->post('pendaftaran_id');
		if ($kelompok_pasien!='1'){
			$idrekanan=0;
		}
		  $q="
			SELECT S.stok as stok_asli,MS.nama as nama_satuan 
					,B.*,H.kuantitas,H.idunit
					,(CASE
					WHEN $kelompok_pasien = 5 THEN
						marginumum
					WHEN $kelompok_pasien = 0 THEN
						marginumum
					WHEN $kelompok_pasien = 1 THEN
						marginasuransi
					WHEN $kelompok_pasien = 2 THEN
						marginjasaraharja
					WHEN $kelompok_pasien = 3 THEN
						marginbpjskesehatan
					WHEN $kelompok_pasien = 4 THEN
						marginbpjstenagakerja
				END) AS margin
					FROM 
					(
					SELECT id,idrawatinap,tanggal as tanggal_transaksi,idtipe,idunit,idobat,hargajual,kuantitas,harga_before_diskon,diskon, totalkeseluruhan,created_date,created_by FROM `trawatinap_obat` 
					UNION ALL
					SELECT id,idrawatinap,tanggal as tanggal_transaksi,idtipe,idunit,idalkes as idobat,hargajual,kuantitas,harga_before_diskon,diskon, totalkeseluruhan,created_date,created_by FROM `trawatinap_alkes` 
					)
					H
					LEFT JOIN mgudang_stok S ON S.idtipe=H.idtipe AND S.idbarang=H.idobat AND S.idunitpelayanan=H.idunit
					INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
					LEFT JOIN msatuan MS ON MS.id=B.idsatuan
					WHERE H.id IN (".$idbarang.") AND H.idtipe='$idtipe'
		  ";
		  // print_r($q);exit;
		$list_barang=$this->db->query($q)->result();
		$jml_insert=0;
		foreach($list_barang as $r){
			$idunit=$r->idunit;
			if ($r->kuantitas<=$r->stok_asli){
				$jml_insert=$jml_insert+1;
				$kelompokpasien=$kelompok_pasien;
				$idbarang=$r->id;
				$idtipe=$r->idtipe;
				$kuantitas=$r->kuantitas;
				$hargadasar=$r->hargadasar;
				$harga_before_diskon=(ceiling($r->hargadasar + ($r->hargadasar*$r->margin/100),100));
				$q_diskon="
					SELECT M.* FROM (
					SELECT compare_value_9(
					MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND  H.idruangan='$idruangan' AND H.idkelas='$idkelas',H.id,NULL))
					,MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND  H.idruangan='$idruangan' AND H.idkelas='0',H.id,NULL))
					,MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='$idtipe' AND H.idruangan='0' AND H.idkelas='0',H.id,NULL))
					,MAX(IF(H.idbarang = '$idbarang' AND H.idtipe='0' AND H.idruangan='0' AND H.idkelas='0',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe' AND H.idruangan='$idruangan' AND H.idkelas='$idkelas',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe' AND H.idruangan='0' AND H.idkelas='$idkelas',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe' AND H.idruangan='$idruangan' AND H.idkelas='0',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='$idtipe' AND H.idruangan='0' AND H.idkelas='0',H.id,NULL))
					,MAX(IF(H.idbarang = '0' AND H.idtipe='0' AND H.idruangan='0' AND H.idkelas='0',H.id,NULL))
					
					) as id FROM merm_pengaturan_diskon_ranap_bmhp H WHERE H.tujuan_pasien='$tujuan_pasien' 
						AND H.idkelompokpasien='$kelompokpasien' AND (H.idrekanan='$idrekanan' OR H.idrekanan='0')
					
					) T INNER JOIN merm_pengaturan_diskon_ranap_bmhp M
					WHERE M.id=T.id

					";
					$data_diskon=$this->db->query($q_diskon)->row();
					$diskon_rp=0;
					$diskon_persen=0;
					if ($data_diskon){
						if ($data_diskon->operand!=0){
							if ($data_diskon->operand=1){
								$diskon_persen=$data_diskon->diskon;
								$diskon_rp=ceiling($harga_before_diskon * $diskon_persen / 100,100);
							}else{
								$diskon_persen=0;
								$diskon_rp=ceiling($data_diskon->diskon,100);
							}
						}
					}else{
						
					}
					// print_r($harga_before_diskon);exit;
				$hargajual_before=$harga_before_diskon-$diskon_rp;
				$hargajual=ceiling($hargajual_before,100);	
				$data=array(
					'idrawatinap' => $idrawatinap,
					'tanggal'=>  date('Y-m-d'),
					'created_date' => date('Y-m-d H:i:s'),
					'idunit' => $this->input->post('idunit'),
					'idtipe' => $idtipe,
					'created_by' => $user_id,
					'hargadasar' => RemoveComma($hargadasar),
					'margin' => ($r->margin),
					'hargajual' => $hargajual,
					'kuantitas' => ($kuantitas),
					'diskon' => $diskon_rp,
					'totalkeseluruhan' => $hargajual * $kuantitas,
					'status'  => 1,
					'statusverifikasi'  => 0,
					// 'iduser_transaksi' => $user_id,
					'idpasien' => $this->input->post('idpasien'),
					// 'idpoliklinik' => $this->input->post('idpoliklinik'),
					'harga_before_diskon' => ($harga_before_diskon),
					'diskon_persen' => $diskon_persen,
					'pembulatan' => 0,


				);
				if ($idtipe=='3'){
					// $data['statusstok'] = $this->input->post('statusstok');
					$data['idobat'] = $r->id;
					$result=$this->db->insert('trawatinap_obat',$data);
				}else{
					$data['idalkes'] = $r->id;
					$result=$this->db->insert('trawatinap_alkes',$data);
				}
				
				// $result=$this->db->insert('tpoliklinik_obat',$data);
			}
		}
		$this->output->set_output(json_encode($jml_insert));
		// $this->output->set_output(json_encode($hasil));
	}
	function get_edit_bmhp_ri(){
		$id=$this->input->post('id');
		$idtipe=$this->input->post('idtipe');
			$q="SELECT H.*,B.nama as nama_barang,MS.singkatan 
			FROM (
				SELECT id,idrawatinap,tanggal as tanggal_transaksi,idtipe,idunit,idobat,hargajual,kuantitas,harga_before_diskon,diskon, totalkeseluruhan,created_date,created_by FROM `trawatinap_obat` WHERE id='$id' AND idtipe='$idtipe'
				UNION ALL
				SELECT id,idrawatinap,tanggal as tanggal_transaksi,idtipe,idunit,idalkes as idobat,hargajual,kuantitas,harga_before_diskon,diskon, totalkeseluruhan,created_date,created_by FROM `trawatinap_alkes` WHERE id='$id' AND idtipe='$idtipe'
			) H
			INNER JOIN view_barang_all B ON B.id=H.idobat AND B.idtipe=H.idtipe
			LEFT JOIN msatuan MS ON MS.id=B.idsatuan
			WHERE H.id='$id'";
		
		
		$hasil=$this->db->query($q)->row_array();
		$hasil['tgltransaksi']=HumanDateShort($hasil['tanggal_transaksi']);
		// $hasil['waktutransaksi']=HumanTime($hasil['tanggal_transaksi']);
		$this->output->set_output(json_encode($hasil));
	}
	function get_tarif_list_pelayanan_ri(){
	  $idkelompokpasien=$this->input->post('idkelompokpasien');
	  $idrekanan=$this->input->post('idrekanan');
	  $idruangan=$this->input->post('idruangan');
	  $idkelas=$this->input->post('idkelas');
	  $iddokter=$this->input->post('iddokter');
	  $jenis_tindakan=$this->input->post('jenis_tindakan');
	  $idtarif_tmp=$this->input->post('idtarif_tmp');
	  $q="SELECT H.idkategori FROM mdokter H WHERE H.id='$iddokter'";
	  $kategori_dokter=$this->db->query($q)->row('idkategori');
	  
	  if ($idkelompokpasien!='1'){
		  $q="
			SELECT T.* FROM mtarif_rawatinap H
			INNER JOIN (
			SELECT 
			compare_value_4(
						MAX(IF(H.iddokter = '$iddokter' AND H.kategori_dokter='$kategori_dokter',H.idtarif,NULL))
						,MAX(IF(H.iddokter = '$iddokter' AND H.kategori_dokter='0',H.idtarif,NULL))
						,MAX(IF(H.iddokter = '0' AND H.kategori_dokter='$kategori_dokter',H.idtarif,NULL))
						,MAX(IF(H.iddokter = '0' AND H.kategori_dokter='0',H.idtarif,NULL))
						) as idtarif
			FROM setting_pelayanan_ri_tarif H
			WHERE H.idkelompokpasien='$idkelompokpasien' AND H.idruangan='$idruangan' AND H.idtipe='$jenis_tindakan'
			) S ON S.idtarif=H.id
			INNER JOIN mtarif_rawatinap T ON T.headerpath=H.headerpath
			ORDER BY T.path ASC
		  ";
		  // print_r($q);exit;
	  }else{
		   $q="
			SELECT T.* FROM mtarif_rawatinap H
			INNER JOIN (
			SELECT 
			compare_value_4(
						MAX(IF(H.iddokter = '$iddokter' AND H.kategori_dokter='$kategori_dokter',H.idtarif,NULL))
						,MAX(IF(H.iddokter = '$iddokter' AND H.kategori_dokter='0',H.idtarif,NULL))
						,MAX(IF(H.iddokter = '0' AND H.kategori_dokter='$kategori_dokter',H.idtarif,NULL))
						,MAX(IF(H.iddokter = '0' AND H.kategori_dokter='0',H.idtarif,NULL))
						) as idtarif
			FROM setting_pelayanan_ri_tarif H
			WHERE H.idkelompokpasien='1' AND (H.idrekanan='$idrekanan' OR H.idrekanan='0')  AND H.idruangan='$idruangan' AND H.idtipe='$jenis_tindakan'
			) S ON S.idtarif=H.id
			INNER JOIN mtarif_rawatinap T ON T.headerpath=H.headerpath
			ORDER BY T.path ASC
		  ";
	  }
	  // print_r($q);exit;
      $data=$this->db->query($q)->result();
	  $opsi='';
	  $headerpath='';
	  $opsi='<option value="0" selected>- Pilih Tarif -</option>';
	  if ($data){
			  $hasil=$this->db->query($q)->result();
			  foreach ($hasil as $r){
				  $opsi .='<option value="'.$r->id.'" '.($r->level=='0'?'disabled':'').' '.($idtarif_tmp==$r->id?'selected':'').'>'.TreeView($r->level,$r->nama).'</option>';
				  $headerpath=$r->headerpath;
			  }
	  }
	  $data['opsi']=$opsi;
	  $data['headerpath']=$headerpath;
	  $this->output->set_output(json_encode($data));
	 
  }
	public function get_layanan_detail_ri()
    {
		$id=$this->input->post('idpelayanan');
		$kelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$idruangan=$this->input->post('idruangan');
		$idkelas=$this->input->post('idkelas');
		$iddokter=$this->input->post('iddokter');
		$jenis_tindakan=$this->input->post('jenis_tindakan');
		$headerpath=$this->input->post('headerpath');
		$asal_rujukan=$this->input->post('asal_rujukan');
		if ($kelompokpasien!='1'){
			$idrekanan=0;
		}
		$idunit=$this->input->post('idunit');
		$pertemuan_id=$this->input->post('pertemuan_id');
		$statuspasienbaru=$this->input->post('statuspasienbaru');
		$idpoli=$this->input->post('idpoliklinik');
		$idtipe_poli=$this->input->post('idtipe_poli');
		// $idtipe=$this->input->post('idtipe');
		$q="SELECT * FROM mtarif_rawatinap_detail WHERE idtarif='$id' AND kelas='$idkelas'";
        $data_layanan = $this->db->query($q)->row();
		$headparent=$headerpath;
		$data_diskon=$this->Tpoliklinik_trx_model->get_logic_diskon_pelayanan_ri($kelompokpasien,$idrekanan,$asal_rujukan,$idruangan,$idkelas,$headparent,$id);
		// print_r($kelompokpasien.'-'.$idrekanan.'-'.$idtipe.'-'.$idpoli.'-'.$statuspasienbaru.'-'.$pertemuan_id.'-'.$headparent.'-'.$id);exit;
		$diskon_rp=0;
		$diskon_persen=0;
		$st_diskon=0;
		if ($data_diskon){
			if ($data_diskon->status_diskon_jasapelayanan=='1'){
				$st_diskon='1';
				if ($data_diskon->tipe_diskon_jasapelayanan=='1'){//PErsen
					$diskon_persen=$data_diskon->diskon_jasapelayanan;
					$diskon_rp=$diskon_persen/100 * $data_layanan->jasapelayanan;
				}else{
					$diskon_rp=$data_diskon->diskon_jasapelayanan;
					$diskon_persen =($diskon_rp * 100) / $data_layanan->jasapelayanan;
				}
			}
		}
		
		$data_layanan->st_diskon=$st_diskon;
		$data_layanan->diskon_rp=$diskon_rp;
		$data_layanan->diskon_persen=$diskon_persen;
        $this->output->set_output(json_encode($data_layanan));
    }
	public function getTindakanRawatInap()
	{
		
		$form_akses=$this->input->post('form_akses');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$idruangan=$this->input->post('idruangan');
		$idkelas=$this->input->post('idkelas');
		$iddokter=$this->input->post('iddokter');
		$jenis_tindakan=$this->input->post('jenis_tindakan');
		$q="SELECT H.idkategori FROM mdokter H WHERE H.id='$iddokter'";
		$kategori_dokter=$this->db->query($q)->row('idkategori');

		if ($idkelompokpasien!='1'){
		  $q="
			SELECT T.*,D.jasasarana,D.jasapelayanan,D.bhp,D.biayaperawatan,D.total FROM mtarif_rawatinap H
			INNER JOIN (
			SELECT 
			compare_value_4(
						MAX(IF(H.iddokter = '$iddokter' AND H.kategori_dokter='$kategori_dokter',H.idtarif,NULL))
						,MAX(IF(H.iddokter = '$iddokter' AND H.kategori_dokter='0',H.idtarif,NULL))
						,MAX(IF(H.iddokter = '0' AND H.kategori_dokter='$kategori_dokter',H.idtarif,NULL))
						,MAX(IF(H.iddokter = '0' AND H.kategori_dokter='0',H.idtarif,NULL))
						) as idtarif
			FROM setting_pelayanan_ri_tarif H
			WHERE H.idkelompokpasien='$idkelompokpasien' AND H.idruangan='$idruangan' AND H.idtipe='$jenis_tindakan'
			) S ON S.idtarif=H.id
			LEFT JOIN mtarif_rawatinap T ON T.headerpath=H.headerpath
			LEFT JOIN mtarif_rawatinap_detail D ON D.idtarif=T.id
			WHERE D.kelas='$idkelas'
			ORDER BY T.path ASC
		  ";
		  // print_r($q);exit;
		}else{
		   $q="
			SELECT T.*,D.jasasarana,D.jasapelayanan,D.bhp,D.biayaperawatan,D.total FROM mtarif_rawatinap H
			INNER JOIN (
			SELECT 
			compare_value_4(
						MAX(IF(H.iddokter = '$iddokter' AND H.kategori_dokter='$kategori_dokter',H.idtarif,NULL))
						,MAX(IF(H.iddokter = '$iddokter' AND H.kategori_dokter='0',H.idtarif,NULL))
						,MAX(IF(H.iddokter = '0' AND H.kategori_dokter='$kategori_dokter',H.idtarif,NULL))
						,MAX(IF(H.iddokter = '0' AND H.kategori_dokter='0',H.idtarif,NULL))
						) as idtarif
			FROM setting_pelayanan_ri_tarif H
			WHERE H.idkelompokpasien='1' AND (H.idrekanan='$idrekanan' OR H.idrekanan='0') AND H.idruangan='$idruangan' AND H.idtipe='$jenis_tindakan'
			) S ON S.idtarif=H.id
			LEFT JOIN mtarif_rawatinap T ON T.headerpath=H.headerpath
			LEFT JOIN mtarif_rawatinap_detail D ON D.idtarif=T.id
			WHERE D.kelas='$idkelas'
			ORDER BY T.path ASC
		  ";
		}
		// print_r($q);exit;
		$data=$this->db->query($q)->result();
        
       $from="
			(
				".$q."
			) as tbl
		";
        $this->select = array();
        $this->join 	= array();
        $this->where  = array();

        $this->order  = array();
        $this->group  = array();
        $this->from   = $from;
        // print_r($from);exit();
        $this->column_search   = array('nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];

		
		
		foreach ($list as $r) {
			$no++;
			$row = [];

			if ($r->idkelompok == 1) {
				$namatarif = TreeView($r->level, $r->nama);
			} else {
				if ($form_akses=='paket'){
				$namatarif = '<a href="#" class="selectLayananPaket" data-nama="' . TreeView($r->level, $r->nama) . '" data-idpelayanan="' . $r->id . '">' . TreeView($r->level, $r->nama) . '</a>';
					
				}else{
				$namatarif = '<a href="#" class="selectLayanan" data-nama="' . TreeView($r->level, $r->nama) . '" data-idpelayanan="' . $r->id . '">' . TreeView($r->level, $r->nama) . '</a>';
					
				}
			}

			$row[] = $no;
			$row[] = $namatarif;
			$row[] = number_format($r->jasasarana);
			$row[] = number_format($r->jasapelayanan);
			$row[] = number_format($r->bhp);
			$row[] = number_format($r->biayaperawatan);
			$row[] = number_format($r->total);

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}
	function simpan_layanan_ri(){
		$user_id=$this->session->userdata('user_id');
		$tanggal= YMDFormat($this->input->post('tanggal'));
		$transaksi_id=$this->input->post('transaksi_id');
		$idrawatinap=$this->input->post('idrawatinap');
		$iddokter=$this->input->post('iddokter');
		$asal_rujukan=$this->input->post('asal_rujukan');
		$idtipepasien=$this->input->post('idtipepasien');
		if ($transaksi_id){
			$this->db->where('id',$transaksi_id);
			$this->db->delete('trawatinap_tindakan');
		}
		$pajak_dokter=0;
		$potongan_rs=0;
		$q="SELECT * FROM mdokter H WHERE H.id='$iddokter'";
		$data_pajak=$this->db->query($q)->row();
		if ($data_pajak){
			
		if ($asal_rujukan=='1'){//RANAP
			
			$pajak_dokter=$data_pajak->pajakranap;
			if ($idtipepasien=='1'){//RS
				$potongan_rs=$data_pajak->potonganrsranap;
			}else{//Pasien Pribadi
				$potongan_rs=$data_pajak->potonganrsranapsiang;
			}
		}
		if ($asal_rujukan=='2'){//ODS
			
			$pajak_dokter=$data_pajak->pajakods;
			if ($idtipepasien=='1'){//RS
				$potongan_rs=$data_pajak->potonganrsods;
			}else{//Pasien Pribadi
				$potongan_rs=$data_pajak->potonganrsodssiang;
			}
		}
		}
		$data=array(
			'idrawatinap'=> $idrawatinap,
			'tanggal'=> $tanggal,
			'created_date' => date('Y-m-d H:i:s'),
			'created_by' => $user_id,
			'iddokter' => $this->input->post('iddokter'),
			'idpelayanan' => $this->input->post('idpelayanan'),
			'jasasarana' => RemoveComma($this->input->post('jasasarana')),
			'jasasarana_disc' => RemoveComma($this->input->post('jasasarana_disc')),
			'jasapelayanan' => RemoveComma($this->input->post('jasapelayanan')),
			'jasapelayanan_disc' => RemoveComma($this->input->post('jasapelayanan_disc')),
			'bhp' => RemoveComma($this->input->post('bhp')),
			'bhp_disc' => RemoveComma($this->input->post('bhp_disc')),
			'biayaperawatan' => RemoveComma($this->input->post('biayaperawatan')),
			'biayaperawatan_disc' => RemoveComma($this->input->post('biayaperawatan_disc')),
			'total' => RemoveComma($this->input->post('total')),
			'kuantitas' => RemoveComma($this->input->post('kuantitas')),
			'diskon' => RemoveComma($this->input->post('diskon')),
			'totalkeseluruhan' => RemoveComma($this->input->post('totalkeseluruhan')),
			'status' => 1,
			// 'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
			'jenis_tindakan' => $this->input->post('jenis_tindakan'),
			'idruangan' => $this->input->post('idruangan'),
			'idkelas' => $this->input->post('idkelas'),
			'pajak_dokter' => $pajak_dokter,
			'potongan_rs' => $potongan_rs,
			// 'profesi_id' => $this->input->post('profesi_id'),
			// 'mppa_id' => $this->input->post('mppa_id'),

		);
		
		$result=$this->db->insert('trawatinap_tindakan',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function load_transaksi_layanan_ri_list(){
		$idrawatinap=$this->input->post('idrawatinap');
		$q="SELECT M.nama,D.nama as nama_tindakan
			,MR.nama as nama_ruangan,MK.nama as nama_kelas
			, H.* 
			FROM `trawatinap_tindakan` H
			INNER JOIN trawatinap_pendaftaran MP ON MP.id=H.idrawatinap
			INNER JOIN mtarif_rawatinap D ON D.id=H.idpelayanan
			LEFT JOIN mdokter M ON M.id=H.iddokter
			LEFT JOIN mruangan MR ON MR.id=H.idruangan
			LEFT JOIN mkelas MK ON MK.id=H.idkelas
			WHERE H.idrawatinap='$idrawatinap' 
			ORDER BY H.tanggal ASC,H.id ASC";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		$no=0;
		foreach ($list as $r) {	
		$no++;
			$btn_edit='<button class="btn btn-primary  btn-xs" onclick="edit_layanan('.$r->id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></button>';
			// $result[] = $no;
			$aksi='';
			$btn_hapus='<button class="btn btn-danger  btn-xs" onclick="hapus_layanan('.$r->id.')" type="button" title="Hapus" type="button"><i class="fa fa-trash"></i></button>';
			
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			$col_tindakan='<div class=""><strong>'.TipeTarifRanap($r->jenis_tindakan).'</strong></div><div class="push-5-t">'.$r->nama_ruangan.' - '.$r->nama_kelas.'</div><div class="text-primary">['.HumanDateShort($r->tanggal).']</div>';
			$col_harga='<div class="text-primary">'.number_format($r->jasapelayanan,0).'</div>';
			$col_harga .='<div class="text-muted">Disc : ('.number_format($r->jasapelayanan_disc,0).')</div>';
			$aksi .= '</div>';
			$tabel .='<tr>';
			// $tabel .='<td class="text-center">'.$no.'</td>';
			$tabel .='<td class="text-left">'.$col_tindakan.'</td>';
			$tabel .='<td class="text-left"><strong>'.$r->nama_tindakan.'</strong><div class="push-5-t"><i class="fa fa-user-md"></i> '.$r->nama.'</div></td>';
			$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
			$tabel .='<td class="text-right">'.$col_harga.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
			$field_hidden ='<input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_tanggal" value="'.HumanDateShort($r->tanggal).'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idpelayanan.'">';
			$field_hidden .='<input type="hidden" class="cls_iddokter" value="'.$r->iddokter.'"><input type="hidden" class="cls_idruangan" value="'.($r->idruangan).'"><input type="hidden" class="cls_jenis_tindakan" value="'.($r->jenis_tindakan).'">';
			$tabel .='<td class="text-center">'.($aksi).$field_hidden.'</td>';
			$tabel .='</tr>';
			$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
			$total_kuantitas=$total_kuantitas+$r->kuantitas;
		  }
			// $tabel .='<tr>';
				// $tabel .='<td colspan="4" width="60%"  class="text-right"><label id="total_kuantitas">TOTAL</label></td>';
				// $tabel .='<td class="text-right" width="5%"><label id="total_penjualan">2 QTY</label></td>';
				// $tabel .='<td width="12%">2333</td>';
				// $tabel .='<td width="23%"></td>';
			// $tabel .='</tr>';
		  $arr['tabel']=$tabel;
		  $arr['total_kuantitas']=number_format($total_kuantitas,0);
		  $arr['total_penjualan']=number_format($total_penjualan,0);
		  $this->output->set_output(json_encode($arr));
	}
	function hapus_layanan_ri(){
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$hasil=$this->db->delete('trawatinap_tindakan');
		$this->output->set_output(json_encode($hasil));
	}
	function get_edit_layanan_ri(){
		$id=$this->input->post('id');
		$q="SELECT H.*
			FROM `trawatinap_tindakan` H
			
			WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$hasil['tgltransaksi']=HumanDateShort($hasil['tanggal']);
		$this->output->set_output(json_encode($hasil));
	}
	function list_history_layanan_ri(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$idrawatinap=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokterpenanggungjawab = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idruangan = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT H.idrawatinap as pendaftaran_id,MP.nopendaftaran,H.tanggal,CASE WHEN MP.idtipe=1 THEN 'RAWAT INAP' ELSE 'ODS' END as tipe_poli
					,CONCAT(MR.nama,' - ',MK.nama) as nama_poli
					,MD.nama as nama_dokter,MP.tanggaldaftar
					,SUM(H.totalkeseluruhan) as total_trx 
					FROM trawatinap_tindakan H
					INNER JOIN trawatinap_pendaftaran MP ON MP.id=H.idrawatinap
					INNER JOIN mruangan MR ON MR.id=MP.idruangan
					INNER JOIN mkelas MK ON MK.id=MP.idkelas
					INNER JOIN mdokter MD ON MD.id=MP.iddokterpenanggungjawab
					WHERE H.idpasien='$idpasien' AND H.idrawatinap != '$idrawatinap' ".$where."
					GROUP BY H.idrawatinap
					ORDER BY MP.id DESC
				) as tbl
			";
			// 
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$btn_lihat='<button class="btn btn-primary  btn-xs" onclick="lihat_data_layanan('.$r->pendaftaran_id.')" type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat Detail</button>';
		$result[] = $no;
		$aksi='';
		$aksi_print='<button class="btn btn-success  btn-xs"  type="button" title="Print" type="button"><i class="fa fa-print"></i></button>';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_lihat;	
		$aksi .= $aksi_print;	
		$aksi .= '</div>';
		$result[] = ($r->nopendaftaran);
		$result[] = HumanDateLong($r->tanggaldaftar);
		$result[] = ($r->tipe_poli);
		$result[] = ($r->nama_poli);
		$result[] = ($r->nama_dokter);
		$result[] = number_format($r->total_trx,0);
		$result[] = $aksi;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function lihat_data_layanan_ri(){
		$idrawatinap=$this->input->post('pendaftaran_id');
		$q="SELECT M.nama,D.nama as nama_tindakan
			,MR.nama as nama_ruangan,MK.nama as nama_kelas
			, H.* 
			FROM `trawatinap_tindakan` H
			INNER JOIN trawatinap_pendaftaran MP ON MP.id=H.idrawatinap
			INNER JOIN mtarif_rawatinap D ON D.id=H.idpelayanan
			LEFT JOIN mdokter M ON M.id=H.iddokter
			LEFT JOIN mruangan MR ON MR.id=H.idruangan
			LEFT JOIN mkelas MK ON MK.id=H.idkelas
			WHERE H.idrawatinap='$idrawatinap' 
			ORDER BY H.tanggal ASC,H.id ASC";
		$list=$this->db->query($q)->result();
		$tabel='';
		$total_kuantitas='0';
		$total_penjualan='0';
		$no=0;
		foreach ($list as $r) {	
		$no++;
		$btn_edit='<button class="btn btn-warning  btn-xs" onclick="copy_layanan('.$r->id.')" type="button" title="Copy Layanan" type="button"><i class="fa fa-copy"></i></button>';
		// $result[] = $no;
		$aksi='';
		
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_edit;	
		$col_tindakan='<div class=""><strong>'.TipeTarifRanap($r->jenis_tindakan).'</strong></div><div class="push-5-t">'.$r->nama_ruangan.' - '.$r->nama_kelas.'</div><div class="text-primary">['.HumanDateShort($r->tanggal).']</div>';
		$col_harga='<div class="text-primary">'.number_format($r->jasapelayanan,0).'</div>';
		$col_harga .='<div class="text-muted">Disc : ('.number_format($r->jasapelayanan_disc,0).')</div>';
		$aksi .= '</div>';
		$tabel .='<tr>';
		// $tabel .='<td class="text-center">'.$no.'</td>';
		$tabel .='<td class="text-left">'.$col_tindakan.'</td>';
		$tabel .='<td class="text-left"><strong>'.$r->nama_tindakan.'</strong><div class="push-5-t"><i class="fa fa-user-md"></i> '.$r->nama.'</div></td>';
		$tabel .='<td class="text-right">'.number_format($r->jasasarana,0).'</td>';
		$tabel .='<td class="text-right">'.$col_harga.'</td>';
		$tabel .='<td class="text-right">'.number_format($r->bhp,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->biayaperawatan,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->total,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->kuantitas,0).'</td>';
		$tabel .='<td class="text-right">'.number_format($r->totalkeseluruhan,0).'</td>';
		$tabel .='<td class="text-center">'.($aksi).'<input type="hidden" class="cls_idtrx" value="'.$r->id.'"><input type="hidden" class="cls_idpelayanan" value="'.$r->idpelayanan.'"></td>';
		$tabel .='</tr>';
		$total_penjualan=$total_penjualan+$r->totalkeseluruhan;
		$total_kuantitas=$total_kuantitas+$r->kuantitas;
	  }
		// $tabel .='<tr>';
			// $tabel .='<td colspan="4" width="60%"  class="text-right"><label id="total_kuantitas">TOTAL</label></td>';
			// $tabel .='<td class="text-right" width="5%"><label id="total_penjualan">2 QTY</label></td>';
			// $tabel .='<td width="12%">2333</td>';
			// $tabel .='<td width="23%"></td>';
		// $tabel .='</tr>';
	  $arr['tabel']=$tabel;
	  $arr['total_kuantitas']=number_format($total_kuantitas,0);
	  $arr['total_penjualan']=number_format($total_penjualan,0);
	  $this->output->set_output(json_encode($arr));
  }
  function copy_layanan_ri(){
		$user_id=$this->session->userdata('user_id');
		$id=$this->input->post('id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$idpasien=$this->input->post('idpasien');
		$iddokter=$this->input->post('iddokter');
		$mppa_id=$this->input->post('mppa_id');
		$profesi_id=$this->input->post('profesi_id');
		$tanggal=date('Y-m-d');
		
		$q="
			INSERT INTO trawatinap_tindakan (
			idrawatinap,tanggal,iddokter,idpelayanan,jasasarana,jasasarana_disc,jasapelayanan,jasapelayanan_disc,bhp,bhp_disc,biayaperawatan,biayaperawatan_disc,total,kuantitas,diskon,totalkeseluruhan,status,statusverifikasi,created_by,created_date,pajak_dokter,potongan_rs,periode_pembayaran,periode_jatuhtempo,status_jasamedis,jenis_tindakan,idpasien,idruangan,idkelas
			)
			SELECT 
			'$pendaftaran_id','$tanggal','$iddokter',idpelayanan,jasasarana,jasasarana_disc,jasapelayanan,jasapelayanan_disc,bhp,bhp_disc,biayaperawatan,biayaperawatan_disc,total,kuantitas,diskon,totalkeseluruhan,status,0,'$user_id',NOW(),pajak_dokter,potongan_rs,periode_pembayaran,periode_jatuhtempo,status_jasamedis,jenis_tindakan,idpasien,idruangan,idkelas
			FROM `trawatinap_tindakan` 
			WHERE id='$id'
		";
		$hasil=$this->db->query($q);
		
		$this->output->set_output(json_encode($hasil));
		// $this->output->set_output(json_encode($hasil));
	}
  
}	
