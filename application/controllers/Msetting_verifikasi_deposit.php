<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_verifikasi_deposit extends CI_Controller {

	/**
	 * Setting Verifikasi Deposit controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Msetting_verifikasi_deposit_model');
  }

	function index($id='') {
		if ($id) {
			$row = $this->Msetting_verifikasi_deposit_model->getSpecified($id);
			$data = array(
				'id' => $row->id,
				'idmetode' => $row->idmetode,
				'noakun_debit' => $row->noakun_debit,
				'noakun_kredit' => $row->noakun_kredit,
				'status' => $row->status
			);
		} else {
			$data = array(
				'id' => '',
				'idmetode' => '',
				'noakun_debit' => '',
				'noakun_kredit' => '',
				'status' => ''
			);
		}

		$data['error'] 			= '';
		$data['title'] 			= 'Setting Verifikasi Deposit';
		$data['content'] 		= 'Msetting_verifikasi_deposit/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Verifikasi Deposit",'#'),
									    			array("List",'msetting_verifikasi_deposit')
													);

		$data['list_akun_submited'] = $this->Msetting_verifikasi_deposit_model->getAkunSubmited();
		$data['list_setting'] = $this->Msetting_verifikasi_deposit_model->getAll();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function delete($id) {
		$this->Msetting_verifikasi_deposit_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('msetting_verifikasi_deposit','location');
	}

	function save() {
		if($this->input->post('id') == '') {
			if($this->Msetting_verifikasi_deposit_model->saveData()) {
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('msetting_verifikasi_deposit','location');
			}
		} else {
			if($this->Msetting_verifikasi_deposit_model->updateData()) {
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('msetting_verifikasi_deposit','location');
			}
		}
	}
}
