<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpengaturan_form_order_laboratorium_patalogi_anatomi extends CI_Controller {

	/**
	 * Pengaturan Form Permintaan Laboratorium Patalogi Anatomi controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpengaturan_form_order_laboratorium_patalogi_anatomi_model');
		$this->load->helper('path');
  }

	function index()
	{
		$row = $this->Mpengaturan_form_order_laboratorium_patalogi_anatomi_model->getSpecified(1);
		if(isset($row->id)){
			$data = array(
				'id' => $row->id,
				'pesan_informasi_berhasil' => $row->pesan_informasi_berhasil,
				'label_judul' => $row->label_judul,
				'label_judul_eng' => $row->label_judul_eng,
				'label_nomor_spesimen' => $row->label_nomor_spesimen,
				'label_nomor_spesimen_eng' => $row->label_nomor_spesimen_eng,
				'required_nomor_spesimen' => $row->required_nomor_spesimen,
				'label_sumber_spesimen_klinis' => $row->label_sumber_spesimen_klinis,
				'label_sumber_spesimen_klinis_eng' => $row->label_sumber_spesimen_klinis_eng,
				'required_sumber_spesimen_klinis' => $row->required_sumber_spesimen_klinis,
				'label_lokasi_pengambilan_spesimen_klinis' => $row->label_lokasi_pengambilan_spesimen_klinis,
				'label_lokasi_pengambilan_spesimen_klinis_eng' => $row->label_lokasi_pengambilan_spesimen_klinis_eng,
				'required_lokasi_pengambilan_spesimen_klinis' => $row->required_lokasi_pengambilan_spesimen_klinis,
				'label_jumlah_spesimen' => $row->label_jumlah_spesimen,
				'label_jumlah_spesimen_eng' => $row->label_jumlah_spesimen_eng,
				'required_jumlah_spesimen' => $row->required_jumlah_spesimen,
				'label_volume_spesimen' => $row->label_volume_spesimen,
				'label_volume_spesimen_eng' => $row->label_volume_spesimen_eng,
				'required_volume_spesimen' => $row->required_volume_spesimen,
				'label_cara_pengambilan_spesimen' => $row->label_cara_pengambilan_spesimen,
				'label_cara_pengambilan_spesimen_eng' => $row->label_cara_pengambilan_spesimen_eng,
				'required_cara_pengambilan_spesimen' => $row->required_cara_pengambilan_spesimen,
				'label_waktu_pengambilan' => $row->label_waktu_pengambilan,
				'label_waktu_pengambilan_eng' => $row->label_waktu_pengambilan_eng,
				'required_waktu_pengambilan' => $row->required_waktu_pengambilan,
				'label_kondisi_spesimen' => $row->label_kondisi_spesimen,
				'label_kondisi_spesimen_eng' => $row->label_kondisi_spesimen_eng,
				'required_kondisi_spesimen' => $row->required_kondisi_spesimen,
				'label_waktu_fiksasi_spesimen_klinis' => $row->label_waktu_fiksasi_spesimen_klinis,
				'label_waktu_fiksasi_spesimen_klinis_eng' => $row->label_waktu_fiksasi_spesimen_klinis_eng,
				'required_waktu_fiksasi_spesimen_klinis' => $row->required_waktu_fiksasi_spesimen_klinis,
				'label_cairan_fiksasi' => $row->label_cairan_fiksasi,
				'label_cairan_fiksasi_eng' => $row->label_cairan_fiksasi_eng,
				'required_cairan_fiksasi' => $row->required_cairan_fiksasi,
				'label_volume_cairan_fiksasi' => $row->label_volume_cairan_fiksasi,
				'label_volume_cairan_fiksasi_eng' => $row->label_volume_cairan_fiksasi_eng,
				'required_volume_cairan_fiksasi' => $row->required_volume_cairan_fiksasi,
				'label_nama_pengambil_spesimen' => $row->label_nama_pengambil_spesimen,
				'label_nama_pengambil_spesimen_eng' => $row->label_nama_pengambil_spesimen_eng,
				'required_nama_pengambil_spesimen' => $row->required_nama_pengambil_spesimen,
				'label_nama_pengantar_spesimen' => $row->label_nama_pengantar_spesimen,
				'label_nama_pengantar_spesimen_eng' => $row->label_nama_pengantar_spesimen_eng,
				'required_nama_pengantar_spesimen' => $row->required_nama_pengantar_spesimen,
				'label_catatan' => $row->label_catatan,
				'label_catatan_eng' => $row->label_catatan_eng,
				'required_catatan' => $row->required_catatan,
			);
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Pengaturan Form Permintaan Laboratorium Patalogi Anatomi';
			$data['content']	 	= 'Mpengaturan_form_order_laboratorium_patalogi_anatomi/index';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Pengaturan Form Permintaan Laboratorium Patalogi Anatomi",'#'),
															array("Ubah",'mpengaturan_form_order_laboratorium_pa')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpengaturan_form_order_laboratorium_pa/index','location');
		}
	}

	function save()
	{
		if($this->Mpengaturan_form_order_laboratorium_patalogi_anatomi_model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mpengaturan_form_order_laboratorium_pa/index','location');
		}
	}
}
