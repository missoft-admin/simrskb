<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Antrian_poli_tv extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // PermissionUserLoggedIn($this->session);

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Antrian_poli_tv_model');
        $this->load->helper('path');
    }

    public function index($tv_id = '1'): void
    {
        redirect('antrian_tv/display/1', 'location');
    }

    public function display($tv_id = '1'): void
    {
        $data = $this->Antrian_poli_tv_model->get_index_setting($tv_id);
        $data['list_running'] = $this->Antrian_poli_tv_model->list_running($tv_id);
        $data['title'] = 'APM Setting Index';
        $data['title_atas'] = $data['nama_display'];
        $data['content'] = 'Antrian_poli_tv/index';
        $data['tv_id'] = $tv_id;
        $data['error'] = '';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['APM Setting', '#'],
            ['Index Setting', 'antrian_tv'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_apm', $data);
    }

    public function display_khusus($tv_id = '1'): void
    {		
				$data = $this->Antrian_poli_tv_model->get_index_setting($tv_id);

        $data['title'] = 'APM Setting Index';
        $data['content'] = 'Antrian_poli_tv/index_khusus';
        $data['error'] = '';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['APM Setting', '#'],
            ['Index Setting', 'antrian_tv'],
        ];
				
        $data['tv_id'] = $tv_id;
        $data['title_atas'] = $data['nama_display'];
        $data['list_running'] = $this->Antrian_poli_tv_model->list_running($tv_id);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_apm', $data);
    }

    public function get_video(): void
    {
        $display_id = $this->input->post('display_id');
        $q = "SELECT GROUP_CONCAT(H.file_name) as nama_file FROM antrian_poli_display_video H

			WHERE H.display_id='{$display_id}'
			ORDER BY H.id ASC";

        $hasil = $this->db->query($q)->row('nama_file');

        $this->output->set_output(json_encode($hasil));
    }

    public function get_antrian(): void
    {
        $display_id = $this->input->post('display_id');
        $q = "SELECT GROUP_CONCAT(H.tujuan_id) as tujuan_id FROM antrian_poli_display_tujuan H WHERE H.display_id='{$display_id}' AND H.`status`='1'";
        print_r($q);exit;
        $list_ruang = $this->db->query($q)->row('tujuan_id');
        $q = "SELECT H.id,H.kode_antrian,MD.nama as nama_dokter,MT.nama_tujuan,H.st_panggil_suara,H.sound_play,H.pendaftaran_id 
		FROM antrian_harian_poli_call H
		INNER JOIN mdokter MD ON MD.id=H.iddokter
		INNER JOIN mtujuan MT ON MT.id=H.ruangan_id
		WHERE H.tanggal=CURRENT_DATE() AND H.st_panggil_suara='0' AND H.ruangan_id IN (".$list_ruang.')

		ORDER BY H.id ASC
		LIMIT 1';
        // AND H.st_panggil_suara='0'
        $hasil = $this->db->query($q)->row();
        $data['antrian_atas'] = $hasil;
        $tabel = '';
        $q = "SELECT M.id,M.kode_antrian,M.nama_tujuan,MD.nama as nama_dokter,M.antrian_tanggal
		FROM antrian_poli_display_tujuan H
		INNER JOIN mtujuan M ON M.id=H.tujuan_id
		LEFT JOIN mdokter MD ON MD.id=M.iddokter

		WHERE H.display_id='{$display_id}' AND H.`status`='1'
		ORDER BY H.nourut ASC";
        $list_antrian = $this->db->query($q)->result();
        foreach ($list_antrian as $r) {
            if ($r->antrian_tanggal !== date('Y-m-d')) {
                $tabel .= '<div class="col-sm-2">
						<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
							<div class="block-content block-content-full bg-primary-dark" style="border: 1px solid white;">
								<div class="h3 font-w400 text-white" >'.$r->nama_tujuan.'</div>
								<div class="font-s13 text-white" style="border-bottom: 1px solid white;padding:0;height: 40px"></div>
								<div class="h1 font-w700 text-white push-5-t">&nbsp;</div>
							</div>

						</a>
				</div>';
            } else {
                $tabel .= '<div class="col-sm-2">
						<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
							<div class="block-content block-content-full bg-primary-dark" style="border: 1px solid white;">
								<div class="h3 font-w400 text-white" >'.$r->nama_tujuan.'</div>
								<div class="font-s13 text-white" style="border-bottom: 1px solid white;padding:0;height: 40px">'.($r->nama_dokter ?: '&nbsp;').'</div>
								<div class="h1 font-w700 text-white push-5-t">'.($r->kode_antrian ?: '&nbsp;').'</div>
							</div>

						</a>
				</div>';
            }
        }
        $data['list_antrian'] = $tabel;
        $this->output->set_output(json_encode($data));
    }

    public function get_antrian_awal(): void
    {
        $display_id = $this->input->post('display_id');
        $q = "SELECT GROUP_CONCAT(H.tujuan_id) as tujuan_id FROM antrian_poli_display_tujuan H WHERE H.display_id='{$display_id}' AND H.`status`='1'";
        $list_ruang = $this->db->query($q)->row('tujuan_id');
        $q = "SELECT H.id,H.kode_antrian,MD.nama as nama_dokter,MT.nama_tujuan,H.st_panggil_suara,H.sound_play,H.pendaftaran_id FROM antrian_harian_poli_call H
		INNER JOIN mdokter MD ON MD.id=H.iddokter
		INNER JOIN mtujuan MT ON MT.id=H.ruangan_id
		WHERE H.tanggal=CURRENT_DATE() AND H.st_panggil_suara='1' AND H.ruangan_id IN (".$list_ruang.')

		ORDER BY H.id DESC
		LIMIT 1';
        // AND H.st_panggil_suara='0'
        // print_r($q);exit;
        $hasil = $this->db->query($q)->row();
        $data['antrian_atas'] = $hasil;
        $tabel = '';
        $q = "SELECT M.id,M.kode_antrian,M.nama_tujuan,MD.nama as nama_dokter,M.antrian_tanggal
		FROM antrian_poli_display_tujuan H
		INNER JOIN mtujuan M ON M.id=H.tujuan_id
		LEFT JOIN mdokter MD ON MD.id=M.iddokter

		WHERE H.display_id='{$display_id}' AND H.`status`='1'
		ORDER BY H.nourut ASC";
        $list_antrian = $this->db->query($q)->result();
        foreach ($list_antrian as $r) {
            if ($r->antrian_tanggal !== date('Y-m-d')) {
                $tabel .= '<div class="col-sm-2">
						<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
							<div class="block-content block-content-full bg-primary-dark" style="border: 1px solid white;">
								<div class="h3 font-w400 text-white" >'.$r->nama_tujuan.'</div>
								<div class="font-s13 text-white" style="border-bottom: 1px solid white;padding:0;height: 40px"></div>
								<div class="h1 font-w700 text-white push-5-t">&nbsp;</div>
							</div>

						</a>
				</div>';
            } else {
                $tabel .= '<div class="col-sm-2">
						<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
							<div class="block-content block-content-full bg-primary-dark" style="border: 1px solid white;">
								<div class="h3 font-w400 text-white" >'.$r->nama_tujuan.'</div>
								<div class="font-s13 text-white" style="border-bottom: 1px solid white;padding:0;height: 40px">'.($r->nama_dokter ?: '&nbsp;').'</div>
								<div class="h1 font-w700 text-white push-5-t">'.($r->kode_antrian ?: '&nbsp;').'</div>
							</div>

						</a>
				</div>';
            }
        }
        $data['list_antrian'] = $tabel;
        $this->output->set_output(json_encode($data));
    }

    public function call_update_panggil(): void
    {
        $id = $this->input->post('id');
        $data = [
            'st_panggil_suara' => 1,
        ];
        $this->db->where('id', $id);
        $hasil = $this->db->update('antrian_harian_poli_call', $data);
        $this->output->set_output(json_encode($hasil));
    }
	public function call_update_panggil_khusus(): void
    {
        $id = $this->input->post('id');
        $data = [
            'st_panggil_suara' => 1,
        ];
        $this->db->where('id', $id);
        $hasil = $this->db->update('antrian_harian_khusus_call', $data);
        $this->output->set_output(json_encode($hasil));
    }

		public function get_antrian_khusus(): void
    {
        $display_id = $this->input->post('display_id');
        $q = "SELECT GROUP_CONCAT(H.tujuan_id) as tujuan_id FROM antrian_poli_display_tujuan_khusus H WHERE H.display_id='{$display_id}' AND H.`status`='1'";
		$list_ruang = $this->db->query($q)->row('tujuan_id');

        $q = "SELECT H.*,M.nama_tujuan FROM antrian_harian_khusus_call H
				INNER JOIN mtujuan M ON M.id=H.tujuan_antrian_id
				WHERE H.st_panggil_suara='0' AND H.tujuan_antrian_id IN ({$list_ruang})
				ORDER BY H.id ASC
				LIMIT 1";

        $hasil = $this->db->query($q)->row();
        $data['antrian_atas'] = $hasil;
		$q_data="
				SELECT * FROM
                (
                    SELECT H.assesmen_id,H.nomedrec,H.namapasien,H.nopendaftaran,H.kode_antrian,H.tujuan_id,H.tujuan_farmasi_id, H.tanggal_kirim FROM tpoliklinik_e_resep H
                    WHERE H.status_assemen='2' AND H.st_panggil='0' AND DATE(H.tanggal_kirim)=CURRENT_DATE()
                    LIMIT 10
                
                    UNION ALL

                    SELECT H.id AS assesmen_id,T.no_medrec AS nomedrec,T.namapasien,T.nopendaftaran,T.kode_antrian,H.tujuan_pelayanan AS tujuan_id,H.tujuan_laboratorium AS tujuan_farmasi_id, H.rencana_pemeriksaan AS tanggal_kirim FROM term_laboratorium_umum H
                    JOIN tpoliklinik_pendaftaran T ON T.id = H.pendaftaran_id AND H.asal_rujukan IN (1, 2)
                    WHERE H.status_pemeriksaan='2' AND H.status_panggil='0' AND DATE(H.rencana_pemeriksaan)=CURRENT_DATE()
                    LIMIT 10
                ) AS result
				ORDER BY result.tanggal_kirim ASC
                ";
		$data_detail=$this->db->query($q_data)->result();
        $html = '';
        $q = "SELECT M.nama_tujuan,H.* FROM antrian_poli_display_tujuan_khusus H
				INNER JOIN mtujuan M ON M.id=H.tujuan_id
				WHERE H.display_id='{$display_id}' AND H.`status`='1'
				ORDER BY H.nourut ASC";

        $list_antrian = $this->db->query($q)->result();
		$lebar=12/floor(count($list_antrian));
        foreach ($list_antrian as $r) {
            
                $html .= '<div class="col-md-'.$lebar.'">
					<div class="block-content block-content-full bg-gray-lighter text-center " style="min-height: 760px">
					<div class="h1 font-w600 push-5 text-uppercase">'.$r->nama_tujuan.'</div>
					
				';
					foreach($data_detail as $row){
						if ($row->tujuan_id==$r->tujuan_id){
					 $html .= '
								<table class="block-table text-left bg-flat border-b text-white">
									<tbody>
										<tr>
											<td class="border-r" style="width: 40%;">
											'.($r->tampil_nomor_antrian=='1'?'<div class="h1 font-w700 push-5-l">'.$row->kode_antrian.'</div>':'').'
											'.($r->tampil_nomor_medrec=='1'?'<div class="h2 text-uppercase push-5-l">'.$row->nomedrec.'</div>':'').'
												
											</td>
											<td>
												'.($r->tampil_nama_pasien=='1'?'<div class="h2">'.$row->namapasien.'</div>':'').'
												'.($r->tampil_nomor_daftar=='1'?'<div class="h3">'.$row->nopendaftaran.'</div>':'').'
												
												
											</td>
										</tr>
									</tbody>
								</table>
					 ';
					}
					}
				$html .= '
						</div>
						</div>';
        }
        
				$data['list_antrian'] = $html;

        $this->output->set_output(json_encode($data));
    }
	public function get_antrian_awal_khusus(): void
    {
        $display_id = $this->input->post('display_id');
        $q = "SELECT GROUP_CONCAT(H.tujuan_id) as tujuan_id FROM antrian_poli_display_tujuan_khusus H WHERE H.display_id='{$display_id}' AND H.`status`='1'";
		$list_ruang = $this->db->query($q)->row('tujuan_id');

        $q = "SELECT H.*,M.nama_tujuan FROM antrian_harian_khusus_call H
				INNER JOIN mtujuan M ON M.id=H.tujuan_antrian_id
				WHERE H.st_panggil_suara='1' AND H.tujuan_antrian_id IN ({$list_ruang}) AND tanggal=CURRENT_DATE
				ORDER BY H.id DESC
				LIMIT 1";

        $hasil = $this->db->query($q)->row();
        $data['antrian_atas'] = $hasil;
		$q_data="
				SELECT H.assesmen_id,H.nomedrec,H.namapasien,H.nopendaftaran,H.kode_antrian,H.tujuan_id,H.tujuan_farmasi_id FROM tpoliklinik_e_resep H
				WHERE H.status_assemen='2' AND H.st_panggil='0'
				ORDER BY H.tanggal_kirim ASC LIMIT 10";
		$data_detail=$this->db->query($q_data)->result();
        $html = '';
        $q = "SELECT M.nama_tujuan,H.* FROM antrian_poli_display_tujuan_khusus H
				INNER JOIN mtujuan M ON M.id=H.tujuan_id
				WHERE H.display_id='{$display_id}' AND H.`status`='1'
				ORDER BY H.nourut ASC";

        $list_antrian = $this->db->query($q)->result();
		$lebar=12/floor(count($list_antrian));
        foreach ($list_antrian as $r) {
            
                $html .= '<div class="col-md-'.$lebar.'">
					<div class="block-content block-content-full bg-gray-lighter text-center " style="min-height: 950px">
					<div class="h1 font-w600 push-5 text-uppercase">'.$r->nama_tujuan.'</div>
					
				';
					foreach($data_detail as $row){
						if ($row->tujuan_id==$r->tujuan_id){
					 $html .= '
								<table class="block-table text-left bg-flat border-b text-white">
									<tbody>
										<tr>
											<td class="border-r" style="width: 40%;">
												<div class="h1 font-w700 push-5-l">'.$row->kode_antrian.'</div>
												<div class="h2 text-uppercase push-5-l">'.$row->nomedrec.'</div>
											</td>
											<td>
												<div class="h2">'.$row->namapasien.'</div>
												<div class="h3">'.$row->nopendaftaran.'</div>
											</td>
										</tr>
									</tbody>
								</table>
					 ';
					}
					}
				$html .= '
						</div>
						</div>';
        }
        
				$data['list_antrian'] = $html;

        $this->output->set_output(json_encode($data));
    }
}
