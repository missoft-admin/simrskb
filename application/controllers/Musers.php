<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Musers extends CI_Controller {

	/**
	 * User controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Musers_model');
  }

	function index(){
		PermissionUserLoggedIn($this->session);

		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Users';
		$data['content'] 		= 'Musers/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Users",'#'),
									    			array("List",'musers')
													);

		$data['list_index'] = $this->Musers_model->getAll();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		PermissionUserLoggedIn($this->session);

		$data = array(
			'id' 						=> '',
			'avatar' 				=> '',
			'name' 					=> '',
			'username' 			=> '',
			'password' 			=> '',
			'idpermission' 	=> '',
			'status' 				=> '',
			'email' 				=> '',
			'st_kasir' 				=> '',
			'unitpelayananiddefault' 				=> null,
			'roleid' 				=> null,
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Users';
		$data['content'] 		= 'Musers/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Users",'#'),
									    			array("Add",'musers')
													);
		$data['list_dokter_radiologi'] = $this->Musers_model->get_dokter_rad('','2');
		$data['list_dokter_radiologi_lain'] = $this->Musers_model->get_dokter_rad('','2','2');
		$data['list_dokter_lab'] = $this->Musers_model->get_dokter_lab('');
		$data['list_dokter_anesthesi'] = $this->Musers_model->get_dokter_anesthesi('','1');
		$data['list_dokter_anesthesi_lain'] = $this->Musers_model->get_dokter_anesthesi('','2');
		$data['list_permission'] = $this->Musers_model->getAllPermission();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		PermissionUserLoggedIn($this->session);

		if($id != ''){
			$row = $this->Musers_model->getSpecified($id);
			// print_r($row);exit();
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'avatar' 				=> $row->avatar,
					'name' 					=> $row->name,
					'username' 			=> $row->username,
					'password' 			=> $row->password,
					'idpermission' 		=> $row->idpermission,
					'status' 				=> $row->status,
					'st_kasir' 				=> $row->st_kasir,
					'email' 				=> $row->email,
					'unitpelayananiddefault' 				=> $row->unitpelayananiddefault,
					'roleid' 				=> $row->roleid,
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Users';
				$data['content']	 	= 'Musers/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Users",'#'),
											    			array("Edit",'musers')
															);

				$data['list_permission'] = $this->Musers_model->getAllPermission();
				$data['list_dokter_radiologi'] = $this->Musers_model->get_dokter_rad($id,'2');
				$data['list_dokter_radiologi_lain'] = $this->Musers_model->get_dokter_rad($id,'2','2');
				$data['list_dokter_lab'] = $this->Musers_model->get_dokter_lab($id);
				$data['list_dokter_anesthesi'] = $this->Musers_model->get_dokter_anesthesi($id,'1');
				$data['list_dokter_anesthesi_lain'] = $this->Musers_model->get_dokter_anesthesi($id,'2');
				// print_r($data['list_dokter_radiologi']);exit();

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('musers','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('musers');
		}
	}

	function delete($id){
		PermissionUserLoggedIn($this->session);

		$this->Musers_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('musers','location');
	}

	function save(){
		// print_r($this->input->post());exit();
		PermissionUserLoggedIn($this->session);

		$this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('username', 'username', 'trim|required');
		if($this->input->post('id') == '' ) {
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
			$this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'trim|required|matches[password]');
		
		}
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Musers_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('musers','location');
				}
			} else {
				if($this->Musers_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('musers','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id, $image=false){
		PermissionUserLoggedIn($this->session);

		$data = $this->input->post();
		$data['avatar'] 						= '';
		$data['error'] 							= validation_errors();
		$data['content'] 						= 'Musers/manage';
		$data['list_permission'] 		= $this->Musers_model->getAllPermission();

		if($image) $data['error']  .= $this->Musers_model->error_message;

		if($id==''){
			$data['title'] = 'Tambah Users';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Users",'#'),
															array("Tambah",'musers')
													);
		}else{
			$data['title'] = 'Ubah Users';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Users",'#'),
															array("Ubah",'musers')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function profile(){
		PermissionUserLoggedIn($this->session);
		// print_r(md5('123456'));exit();
		if($this->session->userdata('user_id') != ''){
			$row = $this->Musers_model->getSpecified($this->session->userdata('user_id'));
			if(isset($row->id)){
				$data = array(
						'id' 						=> $row->id,
						'avatar' 				=> $row->avatar,
						'name' 					=> $row->name,
						'username' 			=> $row->username,
						'password' 			=> $row->password,
						'idpermission' 	=> $row->idpermission,
						'idpermission' 	=> $row->idpermission,
						'unitpelayananiddefault' 	=> $row->unitpelayananiddefault,
						'status' 				=> $row->status
						);
				$data['unit_pelayanan'] = $this->Musers_model->munit_pelayanan($row->id);
				$data['error'] = '';
				$data['title'] = 'Edit Profile';
				$data['content'] = 'Musers/profile';
				$data['breadcrum'] = array(
															array("RSKB Halmahera",'dashboard'),
															array("Users",'musers'),
															array("Profile",'#')
														);

				$data['permission'] = $this->Musers_model->getAllPermission();
				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template',$data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('dashboard','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('dashboard');
		}
	}

	function profile_save(){
		PermissionUserLoggedIn($this->session);

		$this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('username', 'username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'trim|required|matches[password]');

		if ($this->form_validation->run() == TRUE){
			if($this->Musers_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('musers/profile','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak berhasil disimpan.');
			redirect('musers/profile','location');
		}
	}
	function change_profile(){
		// print_r($this->input->post());exit();
		PermissionUserLoggedIn($this->session);
		$data['name']=$this->input->post('name');
		$data['unitpelayananiddefault']     = $this->input->post('unitpelayananiddefault');
		$data['last_edit']=date('Y-m-d H:i:s');
		$data['edit_by']=$this->session->userdata('user_id');
		$data['username']=$this->input->post('username');
		if ($this->input->post('password')!=''){
			
			$data['password']=md5($this->input->post('password'));
		}
		$avatar=$this->upload_profile(true);
		if ($avatar){
			$data['avatar']=$avatar;
		}
		$this->db->where('id',$this->input->post('id'));
		$hasil=$this->db->update('musers',$data);
		// print_r($data);exit();
		
		if ($hasil){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('musers/profile','location');
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak berhasil disimpan.');
			redirect('musers/profile','location');
		}
	}
	public function upload_profile($update = false)
    {
		if (!file_exists('assets/upload/avatars')) {
			mkdir('assets/upload/avatars', 0755, true);
		}

        if (isset($_FILES['avatar'])) {
            if ($_FILES['avatar']['name'] != '') {
                $config['upload_path'] = './assets/upload/avatars';
                $config['allowed_types'] = 'jpg|bmp';
                $config['encrypt_name']  = true;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);
                if ($this->upload->do_upload('avatar')) {
                    $image_upload = $this->upload->data();
                    $hasil= $image_upload['file_name'];
					
                    if ($update == true) {
                        $this->Musers_model->removeImage($_POST['id']);
                    }

                    return $hasil;
                } else {
                    $this->error_message = $this->upload->display_errors();
					// print_r($this->error_message);exit();
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
	function get_md5(){
		$pass=$this->input->post('password');
		$hasil=md5($pass);
		$data['pass']=$pass;
		$data['pass_hash']=$hasil;
		$this->output->set_output(json_encode($hasil));
	}
	function sign_in(){
		PermissionUserLogin($this->session);

		$data = array(
			'username' 	=> '',
			'remember' 	=> FALSE,
			'error' 		=> ''
		);

		$data = array_merge($data, backend_info());
		$this->load->view('Musers/sign_in',$data);
	}

	function dosign_in(){
		PermissionUserLogin($this->session);

		$username	= $this->input->post('username');
	 	$password = $this->input->post('password');
		
		if($this->Musers_model->signIn($username, $password)){
			get_ppa_login();
			// redirect('dashboard','location');
			if ($this->session->userdata('referer') != '')
            {
                redirect($this->session->userdata('referer'),'location');
            }else{
                redirect('dashboard','location');
            }
		}else{
			redirect('sign_in','location');
		}
	}

	function sign_out(){
		// PermissionUserLoggedIn($this->session);
		
		$this->Musers_model->signOut();
		redirect('musers/sign_in','location');
	}

	function select_roleid() {

		$this->db->select('id, nama as text');
		$this->db->where('status', '1');
		$this->db->order_by('id', 'desc');
		$data = $this->db->get('mroles')->result_array();
		$this->output->set_output(json_encode($data));		
	}

	function select_roleid_select() {
		$id = $this->uri->segment(3);
		$this->db->select('id, nama as text');
		$this->db->where('id', $id);
		$data = $this->db->get('mroles')->row();
		$this->output->set_output(json_encode($data));
	}	

	function select_unitpelayananid() {
		$this->db->select('id, nama as text');
		$this->db->where('status', '1');
		$this->db->order_by('id', 'desc');
		$data = $this->db->get('munitpelayanan')->result_array();
		$this->output->set_output(json_encode($data));		
	}

	function select_unitpelayananiddefault() {
		$this->db->select('id, nama as text');
		$this->db->where('status', '1');
		$this->db->order_by('id', 'desc');
		$data = $this->db->get('munitpelayanan')->result_array();
		$this->output->set_output(json_encode($data));		
	}
	function select_unitpelayananiddefault_acep($id='') {
			$q="SELECT H.idunitpelayanan as id,M.nama as text from munitpelayanan_user H
			INNER JOIN munitpelayanan M ON M.id=H.idunitpelayanan
			WHERE H.userid='$id'";
			$query=$this->db->query($q);
			$data=$query->result_array();
		
		$this->output->set_output(json_encode($data));		
	}

	function select_unitpelayananiddefault_select() {
		$id = $this->uri->segment(3);
		$this->db->select('id, nama as text');
		$this->db->where('id', $id);
		$data = $this->db->get('munitpelayanan')->row();
		$this->output->set_output(json_encode($data));
	}

	public function get_unitpelayanan_tipebarang() {
		$id = $this->uri->segment(3);
		$this->db->select("
			idtipe as id, 
			CASE WHEN idtipe = 1  THEN 'Alkes'
			WHEN idtipe = 2 THEN 'Implan'
			WHEN idtipe = 1  THEN 'Obat'
			ELSE 'Logistik' END AS text 
		");
		$this->db->where('musers_tipebarang.iduser', $id);
		$this->db->join('musers', 'musers_tipebarang.iduser = musers.id', 'left');
		$res = $this->db->get('musers_tipebarang')->result_array();
		// print_r($res);
		$this->output->set_output(json_encode($res));
	}	

	public function get_tipegudang() {
		$id = $this->uri->segment(3);
		$this->db->select("
			gudangtipe as id,
			CASE WHEN gudangtipe = 1 THEN 'Logistik'
			ELSE 'Non Logistik'
			END as text
		");
		$this->db->where('iduser', $id);
		$query = $this->db->get('musers_tipegudang')->result_array();
		$this->output->set_output(json_encode($query));
	}
}
