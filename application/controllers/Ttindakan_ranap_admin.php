<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use Hashids\Hashids;
class Ttindakan_ranap_admin extends CI_Controller {

	/**
	 * Checkin controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbooking_rehab_model');
		$this->load->model('Tbooking_model');
		$this->load->model('Ttindakan_ranap_admin_model');
		$this->load->model('Tpoliklinik_pendaftaran_model');
		$this->load->model('Tcheckin_model');
		$this->load->helper('path');
		// print_r('sini');exit;
		
  }
	function index(){
		$log['path_tindakan']='ttindakan_ranap_admin';
		$this->session->set_userdata($log);
		 get_ppa_login();
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1939'))){
			$data = array();
			$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("-30 days"));
			$date2= date_format($date2,"d/m/Y");
			
			$data['list_reservasi_cara'] 			= list_reservasi_cara();
			// print_r($data['list_reservasi_cara']);exit;
			
			$data['list_poli'] 			= $this->Ttindakan_ranap_admin_model->list_poli();
			$data['list_dokter'] 			= $this->Ttindakan_ranap_admin_model->list_dokter();
			$data['tanggal_1'] 			= $date2;
			$data['tanggal_2'] 			= date('d/m/Y');
			$data['tab_utama'] 			= 3;
			$data['idalasan'] 			= '#';
			$data['waktu_reservasi_1'] 			= '';
			$data['waktu_reservasi_2'] 			= '';
			$data['namapasien'] 			= '';
			$data['no_medrec'] 			= '';
			$data['tab_detail'] 			=1;
			$tahun=date('Y');
			$bulan=date('m');
			$q="SELECT D.tanggal,DATE_ADD(D.tanggal,INTERVAL 6 DAY) as tanggal_next   FROM date_row D WHERE D.tanggal BETWEEN DATE_ADD(CURRENT_DATE(),INTERVAL -6 DAY) AND CURRENT_DATE()";
			$tanggal=$this->db->query($q)->row();
			$data['tanggaldari'] 			= DMYFormat($tanggal->tanggal);
			$data['tanggalsampai'] 			= DMYFormat($tanggal->tanggal_next);
			$data['error'] 			= '';
			$data['title'] 			= 'Pendaftaran Rawat Inap';
			$data['content'] 		= 'Ttindakan_ranap_admin/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Rawat Inap & ODS",'#'),
												  array("ADM Rawat Inap",'ttindakan_ranap_admin')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$iddokter =$this->input->post('iddokter');
			$idtipe =$this->input->post('idtipe');
			$idtipe_asal =$this->input->post('idtipe_asal');
			$cari_pasien =$this->input->post('cari_pasien');
			$nopendaftaran=$this->input->post('nopendaftaran');
			$idruangan=$this->input->post('idruangan');
			$idkelas=$this->input->post('idkelas');
			$idbed=$this->input->post('idbed');
			$norujukan=$this->input->post('norujukan');
			$iddokter_perujuk=$this->input->post('iddokter_perujuk');
			$idkelompokpasien=$this->input->post('idkelompokpasien');
			$idrekanan=$this->input->post('idrekanan');
			$status_tindakan_index=$this->input->post('status_tindakan_index');
			
			$idpoliklinik_array=$this->input->post('idpoliklinik_array');
			if ($idpoliklinik_array){
				$idpoliklinik_array=implode(", ", $idpoliklinik_array);
				$where .=" AND (H.idpoliklinik_asal IN (".$idpoliklinik_array.")) ";
			}
			$iddokter_perujuk_array=$this->input->post('iddokter_perujuk_array');
			if ($iddokter_perujuk_array){
				$iddokter_perujuk_array=implode(", ", $iddokter_perujuk_array);
				$where .=" AND (H.iddokter_perujuk IN (".$iddokter_perujuk_array.")) ";
			}
			$iddokter_dpjp=$this->input->post('iddokter_dpjp');
			if ($iddokter_dpjp){
				$iddokter_dpjp=implode(", ", $iddokter_dpjp);
				$where .=" AND (H.iddokterpenanggungjawab IN (".$iddokter_dpjp.")) ";
			}
			$dpjp_pendukung_array=$this->input->post('dpjp_pendukung_array');
			if ($dpjp_pendukung_array){
				$dpjp_pendukung_array=implode(", ", $dpjp_pendukung_array);
				$where .=" AND (HP.iddokter IN (".$dpjp_pendukung_array.")) ";
			}
			$idkelompokpasien_array=$this->input->post('idkelompokpasien_array');
			if ($idkelompokpasien_array){
				$idkelompokpasien_array=implode(", ", $idkelompokpasien_array);
				$where .=" AND (H.idkelompokpasien IN (".$idkelompokpasien_array.")) ";
			}
			$idrekanan_array=$this->input->post('idrekanan_array');
			if ($idrekanan_array){
				$idrekanan_array=implode(", ", $idrekanan_array);
				$where .=" AND (H.idrekanan IN (".$idrekanan_array.")) ";
			}
			$idruangan_array=$this->input->post('idruangan_array');
			if ($idruangan_array){
				$idruangan_array=implode(", ", $idruangan_array);
				$where .=" AND (H.idruangan IN (".$idruangan_array.")) ";
			}
			$idkelas_array=$this->input->post('idkelas_array');
			if ($idkelas_array){
				$idkelas_array=implode(", ", $idkelas_array);
				$where .=" AND (H.idkelas IN (".$idkelas_array.")) ";
			}
			$idbed_array=$this->input->post('idbed_array');
			if ($idbed_array){
				$idbed_array=implode(", ", $idbed_array);
				$where .=" AND (H.idkelas IN (".$idbed_array.")) ";
			}
			$status_tindakan_array=$this->input->post('status_tindakan_array');
			if ($status_tindakan_array){
				$where .=" AND (";
				$where_tindakan='';
				
				if (in_array('2', $status_tindakan_array)){
					// if ($statuskasir=='1' && $statuspembayaran=='0'){
					$where_tindakan .=($where_tindakan==''?'':' OR ')."(H.statuskasir='1' AND H.statuspembayaran='0') ";
				}
				if (in_array('3', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.st_terima_ranap='1' AND H.status='1') ";
				}
				if (in_array('4', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.statuspembayaran='1') ";
				}
				if (in_array('5', $status_tindakan_array)){
					$where_tindakan .=($where_tindakan==''?'  ':' OR ')."(H.statuscheckout='1') ";
				}
				$where .=$where_tindakan.")";
			}
			$no_medrec=$this->input->post('no_medrec');
			if ($no_medrec !=''){
				$where .=" AND (H.no_medrec LIKE '%".$no_medrec."%') ";
			}
			$namapasien=$this->input->post('namapasien');
			if ($namapasien !=''){
				$where .=" AND (H.namapasien LIKE '%".$namapasien."%') ";
			}
			
			
			if ($iddokter!='#'){
				$where .=" AND (H.iddokterpenanggungjawab='$iddokter') ";
			}
			if ($idrekanan!='#'){
				$where .=" AND (H.idrekanan='$idrekanan') ";
			}
			if ($idkelompokpasien!='#'){
				$where .=" AND (H.idkelompokpasien='$idkelompokpasien') ";
			}
			if ($iddokter_perujuk!='#'){
				$where .=" AND (H.iddokter_perujuk='$iddokter_perujuk') ";
			}
			if ($idbed!='#'){
				$where .=" AND (H.idbed='$idbed') ";
			}
			if ($idruangan!='#'){
				$where .=" AND (H.idruangan='$idruangan') ";
			}
			if ($idkelas!='#'){
				$where .=" AND (H.idkelas='$idkelas') ";
			}
			if ($idtipe!='#'){
				$where .=" AND (H.idtipe='$idtipe') ";
			}
			if ($idtipe_asal!='#'){
				$where .=" AND (H.idtipe_asal='$idtipe_asal') ";
			}
			if ($norujukan !=''){
				$where .=" AND (H.nopermintaan LIKE '%".$norujukan."%') ";
			}
			if ($nopendaftaran !=''){
				$where .=" AND (H.nopendaftaran LIKE '%".$nopendaftaran."%' OR H.nopendaftaran_poli LIKE '%".$nopendaftaran."%') ";
			}
			
			if ($cari_pasien !=''){
				$where .=" AND (H.namapasien LIKE '%".$cari_pasien."%' OR H.no_medrec LIKE '%".$cari_pasien."%') ";
			}
			// print_r($where);exit;
			$tab =$this->input->post('tab');
			if ($tab=='2'){
				$where .=" AND (H.statuskasir='1'  AND H.statuspembayaran='0') ";
			}
			
			if ($tab=='3'){
				$where .=" AND (H.st_terima_ranap='1' AND H.status='1' AND H.statuscheckout='0') ";
			}
			if ($tab=='4'){
				$where .=" AND (H.statuspembayaran='1') ";
			}
			if ($tab=='5'){
				$where .=" AND (H.statuscheckout='1') ";
			}
			if ($status_tindakan_index=='2'){
				$where .=" AND (H.statuskasir='1'  AND H.statuspembayaran='0') ";
			}
			
			if ($status_tindakan_index=='3'){
				$where .=" AND (H.st_terima_ranap='1' AND H.status='1') ";
			}
			if ($status_tindakan_index=='4'){
				$where .=" AND (H.statuspembayaran='1') ";
			}
			if ($status_tindakan_index=='5'){
				$where .=" AND (H.statuscheckout='1') ";
				if ($tanggal_1 !=''){
					$where .=" AND DATE(H.tanggaldaftar) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggaldaftar) <='".YMDFormat($tanggal_2)."'";
				}else{
					$where .=" AND DATE(H.tanggaldaftar) >='".date('Y-m-d')."'";
				}
			}
			
			
			$tanggal_1_trx =$this->input->post('tanggal_1_trx');
			if ($tanggal_1_trx !=''){
				$where .=" AND DATE(H.tanggaldaftar) >='".YMDFormat($tanggal_1_trx)."' AND DATE(H.tanggaldaftar) <='".YMDFormat($tanggal_2_trx)."'";
			}
			$tanggal_1_pulang =$this->input->post('tanggal_1_pulang');
			if ($tanggal_1_pulang !=''){
				$where .=" AND DATE(H.tanggalcheckout) >='".YMDFormat($tanggal_1_pulang)."' AND DATE(H.tanggalcheckout) <='".YMDFormat($tanggal_2_pulang)."'";
			}
			$this->select = array();
			$get_alergi_sql=get_alergi_sql();
			$from="
					(
						SELECT 
						PB.status_verifikasi_bayar,PB.st_verifikasi_piutang
						,MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
						,MK.nama as nama_kelompok,MR.nama as nama_rekanan,MDP.nama as nama_dokter_poli
						,H.*,MKL.nama as nama_kelas 
						,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header
						,MB.nama as nama_bed,TP.tanggal,MRU.nama as nama_ruangan,AH.kodeantrian as kode_antrian
						,UH.name as nama_hapus,GROUP_CONCAT(MDHP.nama SEPARATOR ', ') as nama_dokter_pendamping,TT.nama_tindakan_setuju		
						FROM trawatinap_pendaftaran H
						LEFT JOIN trawatinap_pendaftaran_dpjp HP ON HP.pendaftaran_id=H.id
						LEFT JOIN mdokter MDHP ON MDHP.id=HP.iddokter
						LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpoliklinik
						LEFT JOIN mruangan MRU ON MRU.id=H.idruangan
						LEFT JOIN mkelas MKL ON MKL.id=H.idkelas
						LEFT JOIN antrian_harian AH ON AH.id=H.antrian_id
						LEFT JOIN mdokter MDP ON MDP.id=TP.iddokter
						LEFT JOIN mdokter MD ON MD.id=H.iddokterpenanggungjawab
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik_asal
						LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
						LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
						LEFT JOIN mbed MB on MB.id=H.idbed						
						LEFT JOIN musers UH on UH.id=H.deleted_by	
						LEFT JOIN (
							".$get_alergi_sql."
						) A ON A.idpasien=H.idpasien 
						LEFT JOIN (
							SELECT H.idtindakan as pendaftaran_id,H.status_verifikasi as status_verifikasi_bayar,D.st_verifikasi_piutang FROM trawatinap_tindakan_pembayaran H
							LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON D.idtindakan=H.id AND D.st_verifikasi_piutang=1 AND D.idmetode IN (6,8)
							WHERE H.statusbatal='0'
							GROUP BY H.id

						) PB ON PB.pendaftaran_id=H.id
						LEFT JOIN (
							".get_tindakan_persetujuan()."
						) TT ON TT.idrawatinap=H.id
						WHERE H.idtipe IS NOT NULL ".$where."
						GROUP BY H.id
						
						ORDER BY H.tanggaldaftar DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $def_image='';
		  $btn_persetujuan_pulang=panel_persetujuan_pulang($r->status_proses_card_pass,$r->nopendaftaran);
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  if ($r->nama_tindakan_setuju){
			  
			  $btn_tindakan_setujua='
				<div class="text-danger font-s13 push-5-t text-bold">
				<button class="btn btn-success  btn-xs" type="button" onclick="show_list_tindakan('.$r->id.')">'.$r->nama_tindakan_setuju.'</button>
				</div>
			  ';
		  }else{
			  $btn_tindakan_setujua='';
		  }
		  if ($r->idtipe=='1'){
			  $info_bed='<div class="h4 push-10-t text-primary"><button class="btn btn-xs btn-block btn-primary push-10" type="button"><i class="fa fa-bed pull-left"></i> '.($r->nama_bed).'</button></strong></div>';
		  }else{
			  $info_bed='';
		  }
		  $div_terima='';
		  $div_hapus='';
		  if ($r->st_terima_ranap=='1'){
			  $div_terima='<div class="text-primary text-uppercase"><i class="fa fa-bed"></i> Tanggal Terima : '.HumanDateLong($r->tanggal_terima).'</div>';
			  $div_terima.='<div class="text-primary text-uppercase"><i class="fa fa-user"></i> User Terima : '.($r->user_nama_terima).'</div>';
		  }
		  if ($r->status=='0'){
			  $div_hapus='<div class="text-danger text-uppercase"><i class="fa fa-trash"></i> Tanggal Batal : '.HumanDateLong($r->deleted_date).'</div>';
			  $div_hapus.='<div class="text-danger text-uppercase"><i class="fa fa-user"></i> User  : '.($r->nama_hapus).'</div>';
		  }
		  $btn_checkout='';
		  $btn_deposit='';
		  $btn_terima='';
		  $btn_layani='';
		  $btn_trx='';
		  $btn_panel='';
		  $pedamping='';
		  $btn_lock='';
		  $btn_verifikasi='';
		  $btn_verifikasi=($r->status_verifikasi_bayar?text_success('VERIFIED'):'');
		  $btn_verifikasi_piutang='';
		  $btn_verifikasi_piutang='&nbsp;&nbsp;'.($r->st_verifikasi_piutang?text_danger('VERIFIED PIUTANG'):'');
		  if ($r->statuskasir=='0'){//LOCK
			  $btn_lock='<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Lock" onclick="show_lock('.$r->id.',1)" class="btn btn-danger btn-xs"><i class="fa fa-lock"></i> &nbsp;&nbsp;LOCK&nbsp;&nbsp;</a> </div>';
		  }else{
			  $btn_lock='<div class="push-5-t"><button type="button" data-toggle="tooltip" '.($r->statustransaksi=='1' && $r->statuspembayaran=='1'?'disabled':'').' title="Unlock" onclick="show_lock('.$r->id.',0)" class="btn btn-success btn-xs"><i class="fa fa-unlock"></i>&nbsp;&nbsp; UNLOCK&nbsp;&nbsp;</a> </div>';
			  
		  }
		  // $label_total='Total Keseluruhan : Rp. 1.780.000';
		  if ($r->nama_dokter_pendamping){
			  $pedamping='<div class="text-wrap text-danger"><i class="fa fa-user-md"></i> PENDAMPING : '.($r->nama_dokter_pendamping).'</div>';
		  }
			  if ($r->status!='0'){
					$btn_layani='<div class="push-5-t"><a href="'.base_url().'tpendaftaran_ranap_erm/tindakan/'.$r->id.'/erm_ri" target="_blank" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block btn-primary btn-xs"><i class="fa fa-user-md"></i> LAYANI</a> </div>';
					$btn_trx='<div class="push-5-t"><a href="'.base_url().'trawatinap_tindakan/verifikasi/'.$r->id.'" target="_blank" type="button" data-toggle="tooltip" title="Transaksi"  class="btn btn-block btn-success btn-xs"><i class="fa fa-credit-card"></i> TRANSAKSI</a> </div>';
					$btn_deposit='<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Deposit" onclick="show_modal_deposit('.$r->id.')"  class="btn btn-block btn-info btn-xs"><i class="fa fa-credit-card"></i> DEPOSIT</button> </div>';
				  // if ($r->st_terima_ranap=='1' && $r->statuspembayaran=='1'){
					  // if (UserAccesForm($user_acces_form,array('1931'))){
						// $btn_checkout='<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Checkout" onclick="show_modal_checkout('.$r->id.')" class="btn btn-block btn-danger btn-xs"><i class="si si-logout"></i> DATA CHECKOUT</a> </div>';
					  // }
					 
				  // }
					  $btn_panel=div_panel_kendali_ri_admin($user_acces_form,$r->id);
			  }
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									'.$btn_layani.'
									'.$btn_panel.'
									'.$btn_lock.'
									'.$btn_trx.'
									'.$btn_deposit.'
									'.$btn_terima.'
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_edit_tipe='<button class="btn btn-xs btn-default" onclick="modal_edit_tipe('.$r->id.','.$r->idtipepasien.')" type="button" data-toggle="tooltip" title="Edit Tipe Pasien" data-original-title="Edit Tipe Pasien"><i class="fa fa-pencil"></i></button>';
		  $btn_edit_catatan='<button class="btn btn-xs btn-default" onclick="modal_edit_catatan('.$r->id.')" type="button" data-toggle="tooltip" title="Edit Catatan" data-original-title="Edit Catatan Pasien"><i class="fa fa-pencil"></i></button>';
		  $catatan_pasien='Tipe Pasien : '.($r->idtipepasien=='1'?'Pasien RS':'Pasien Pribadi ').' '.$btn_edit_tipe.' | Catatan : '.($r->catatan?$r->catatan:' - ').$btn_edit_catatan;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
										<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
									</div>
									<div class="text-primary font-s13 push-5-t">'.$r->nopendaftaran_poli.' | '.HumanDateShort($r->tanggal).' | '.GetAsalRujukanR($r->idtipe_asal).' | '.$r->nama_poli.' | '.$r->nama_dokter_poli.'</div>
									<div class="font-s13 push-5-t">'.$catatan_pasien.'</div>
									<div class="push-5-t">'.$btn_tindakan_setujua.$btn_persetujuan_pulang.'</div>
									<div class="text-danger font-s13 push-5-t text-bold">'.$btn_verifikasi.$btn_verifikasi_piutang.'</div>
									
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_5='';
		  
          $result[] = $btn_5;
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class=" text-uppercase">'.($r->nama_ruangan).' - '.$r->nama_kelas.' - '.$r->nama_bed.'</div>
									<div class=""><i class="fa fa-user-md"></i> DPJP : '.($r->nama_dokter).'</div>
									'.$pedamping.'
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->nama_rekanan?$r->nama_rekanan:$r->nama_kelompok).'</div>
									'.$div_terima.'
									'.$div_hapus.'
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.$this->status_ranap($r->status,$r->st_terima_ranap,$r->statuscheckout,$r->statuspembayaran,$r->statuskasir).'</div>
									'.$info_bed.'
									<div class="h5 push-10-t text-primary"><strong>'.GetTipeRujukanRawatInap($r->idtipe).'</strong></div>
									<div class="h5 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggaldaftar).'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function get_data_deposit(){
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $q="SELECT H.no_medrec,CONCAT(H.title,' ',H.namapasien) as nama_pasien,H.nohp,H.namapenanggungjawab,H.teleponpenanggungjawab,
KP.nama as nama_kp,CASE WHEN H.idkelompokpasien='1' THEN  MR.nama ELSE '' END as nama_rekanan,MK.nama as nama_kelas,MB.nama as nama_bed,R.nama as nama_ruangan
,H.* FROM `trawatinap_pendaftaran` H
LEFT JOIN mpasien_kelompok KP ON KP.id=H.idkelompokpasien
LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
LEFT JOIN mkelas MK ON MK.id=H.idkelas
LEFT JOIN mbed MB ON MB.id=H.idbed
LEFT JOIN mruangan R ON R.id=H.idruangan
		WHERE H.id='$pendaftaran_id'";
		
		$result=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($result));
		
  }
  function status_ranap($status,$st_terima_ranap,$statuscheckout,$statuspembayaran,$statuskasir){
	  $label='<button class="btn btn-xs btn-block btn-primary push-10" type="button"><i class="fa fa-facebook pull-left "></i> Login with Facebook</button>';
	  if ($status=='0'){
		  $label='<button class="btn btn-xs btn-block btn-danger push-10" type="button"><i class="fa fa-times pull-left "></i>DIBATALKAN</button>';
	  }else{
		  if ($st_terima_ranap=='0'){
			  $label='<button class="btn btn-xs btn-block btn-info push-10" type="button"><i class="fa fa-wrench pull-left "></i>MENUNGGU DIRAWAT</button>';
		  }
		  if ($st_terima_ranap=='1' && $statuscheckout=='0'){
			  $label='<button class="btn btn-xs btn-block btn-warning push-10" type="button"><i class="fa fa-bed pull-left"></i>DIRAWAT</button>';
		  }
		 
		  if ($statuskasir=='1' && $statuspembayaran=='0'){
			  $label='<button class="btn btn-xs btn-block btn-danger push-10" type="button">MENUNGGU TRANSAKSI</button>';
		  }elseif ($statuskasir=='1' && $statuspembayaran=='1'){
			  $label='<button class="btn btn-xs btn-block btn-primary push-10" type="button">SELESAI</button>';
		  }
		   
		  if ($statuscheckout=='1'){
			  $label='<button class="btn btn-xs btn-block btn-success push-10" type="button">PULANG</button>';
		  }
	  }
	  
	  return $label;
  }
  function update_kunjungan(){
	  $pendaftaran_id_lama=$this->input->post('pendaftaran_id_lama');
	  $pendaftaran_id_baru=$this->input->post('pendaftaran_id_baru');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$q="UPDATE tpoliklinik_pendaftaran SET statusrencana='0' WHERE id='$pendaftaran_id_lama'";
		$this->db->query($q);
		$q="UPDATE tpoliklinik_pendaftaran SET statusrencana='1' WHERE id='$pendaftaran_id_baru'";
		$this->db->query($q);
		$q="UPDATE trawatinap_pendaftaran SET idpoliklinik='$pendaftaran_id_baru' WHERE id='$pendaftaran_id_ranap'";
		$hasil=$this->db->query($q);
		
		$this->output->set_output(json_encode($hasil));
  }
  function update_tipe_pasein(){
	  $id=$this->input->post('id');
	  $idtipepasien=$this->input->post('idtipepasien');
	 
		$q="UPDATE trawatinap_pendaftaran SET idtipepasien='$idtipepasien' WHERE id='$id'";
		$hasil=$this->db->query($q);
		
		$this->output->set_output(json_encode($hasil));
  }
  function update_catatan(){
	  $id=$this->input->post('id');
	  $catatan=$this->input->post('catatan');
	 
		$q="UPDATE trawatinap_pendaftaran SET catatan='$catatan' WHERE id='$id'";
		$hasil=$this->db->query($q);
		
		$this->output->set_output(json_encode($hasil));
  }
  function get_catatan(){
	  $id=$this->input->post('id');
	 
		$q="SELECT catatan FROM trawatinap_pendaftaran WHERE id='$id'";
		$hasil=$this->db->query($q)->row_array();
		
		$this->output->set_output(json_encode($hasil));
  }
	function set_lock(){
	  $id=$this->input->post('id');
		$statuskasir=$this->input->post('statuskasir');
		$data=array(
			'statuskasir' =>$statuskasir,

		);
		$this->db->where('id',$id);
		$result=$this->db->update('trawatinap_pendaftaran',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
  }
  public function getIndex_kasir($uri = 'index')
	{
		$this->select = [];
		$this->join = [];
		$this->where = [];

		$idpasien=$this->input->post('idpasien');
		$where = " WHERE P.idpasien='$idpasien'";
		if ($this->input->post('tanggaldari') == '') {
			$where .= " AND DATE(K.tanggal)='" . date('Y-m-d') . "'";
		} else {
			$where .= " AND DATE(K.tanggal)>='" . YMDFormat($this->input->post('tanggaldari')) . "' AND DATE(K.tanggal)<='" . YMDFormat($this->input->post('tanggalsampai')) . "'";
		}
		

		$from = "(SELECT K.id,K.idtindakan
        ,CASE WHEN K.idtipe='3' THEN K.tanggal ELSE K.tanggal END as tanggal,K.idtipe,K.nokasir,P.iddokter,P.idpoliklinik
				,CASE WHEN K.idtipe='3' THEN 'N' ELSE P.id END as idpendaftaran
				,CASE WHEN K.idtipe='3' THEN OB.nopenjualan ELSE P.nopendaftaran END as notrx
				,CASE WHEN K.idtipe='3' THEN OB.nomedrec ELSE P.no_medrec END as no_medrec
				,CASE WHEN K.idtipe='3' THEN '' ELSE P.title END as title
				,CASE WHEN K.idtipe='3' THEN OB.nama ELSE P.namapasien END as nama
				,CASE WHEN K.idtipe='3' THEN '' ELSE mp.nama END as namapoli
				,CASE WHEN K.idtipe='3' THEN pk_ob.id ELSE pk.id END as idkelompokpasien
				,CASE WHEN K.idtipe='3' THEN pk_ob.nama ELSE pk.nama END as namakelompok
				,CASE WHEN K.idtipe='3' THEN '0' ELSE T.rujukfarmasi END as rujukfarmasi
				,CASE WHEN K.idtipe='3' THEN '0' ELSE T.rujuklaboratorium END as rujuklaboratorium
				,CASE WHEN K.idtipe='3' THEN '0' ELSE T.rujukradiologi END as rujukradiologi
				,CASE WHEN K.idtipe='3' THEN '0' ELSE IF(T.rujukfisioterapi='',0,T.rujukfisioterapi)  END as rujukfisioterapi
				,RFa.`status` as far_status
				,RR.`status` as rad_status
				,RL.`status` as lab_status
				,RF.`status` as fis_status
				,K.`status`
				,K.`status_verifikasi`
				,GROUP_CONCAT(B.idmetode) as idmetode
				,GROUP_CONCAT(B.id) as idbayar
				,P.statusrencana,RI.id as ri_id
				,CASE WHEN K.idtipe = '3' THEN OB.status ELSE T.status END AS status_tindakan
				from tkasir K
				LEFT JOIN tpoliklinik_tindakan T ON T.id=K.idtindakan AND K.idtipe IN (1,2)
				LEFT JOIN tpoliklinik_pendaftaran P ON T.idpendaftaran=P.id
				LEFT JOIN mpoliklinik mp ON mp.id=P.idpoliklinik
				LEFT JOIN mpasien_kelompok pk ON pk.id=P.idkelompokpasien
				LEFT JOIN tpasien_penjualan OB ON OB.id=K.idtindakan AND K.idtipe='3'
				LEFT JOIN mpasien_kelompok pk_ob ON pk_ob.id=OB.idkelompokpasien
				LEFT JOIN trujukan_radiologi RR ON RR.idtindakan=T.id AND T.rujukradiologi='1'
				LEFT JOIN trujukan_laboratorium RL ON RL.idtindakan=T.id AND T.rujuklaboratorium='1'
				LEFT JOIN trujukan_fisioterapi RF ON RF.idtindakan=T.id AND T.rujukfisioterapi='1'
				LEFT JOIN tpasien_penjualan RFa ON RFa.idtindakan=T.id AND T.rujukfarmasi='1'
				LEFT JOIN tkasir_pembayaran B ON B.idkasir=K.id
				LEFT JOIN trawatinap_pendaftaran RI ON RI.idpoliklinik=P.id AND RI.`status` != 0 

				" . $where . '
				GROUP BY K.id

				ORDER BY K.id DESC) as tbl
        WHERE tbl.status_tindakan != 0';
		// print_r($from);exit;
		$this->order = [];
		$this->group = [];
		$this->from = $from;

		$this->column_search = [];
		$this->column_order = [];

		$list = $this->datatable->get_datatables();

		$data = [];
		$no = $_POST['start'];

		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		$url_dok_rajal = site_url('tverifikasi_transaksi/upload_document/tkasir/');

		foreach ($list as $r) {
			$no++;
			$row = [];
			$action = '';
			if ($r->status == 0) {
				//Dibatalkan
				$tipeAction = '';
				if ($r->idtipe == '3') {
					if (button_roles('tkasir/transaksi_ob')) {
						$tipeAction = '<a href="' . site_url() . 'tkasir/transaksi_ob/' . $r->idtindakan . '/' . $r->id . '/1' . '" target="_blank"  class="btn btn-primary btn-sm" data-toggle="tooltip" title="Edit Transaksi"><i class="fa fa-pencil"></i></a>';
					}
				} else {
					if (UserAccesForm($user_acces_form, ['1072'])) {
						$tipeAction = '<a href="' . site_url() . 'tkasir/transaksi/' . $r->idpendaftaran . '/' . $r->id . '/1' . '" target="_blank" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Transaksi"><i class="fa fa-pencil"></i></a>';
					}
				}

				if (UserAccesForm($user_acces_form, ['1089'])) {
					$action .= $tipeAction . '<div class="btn-group">
                     <div class="btn-group dropright">
                       <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                         <span class="fa fa-print"></span>
                       </button>
                     <ul class="dropdown-menu">
                       <li>
                         <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_transaksi/' . $r->idpendaftaran . '/' . $r->id . '/1' . '">Rincian Pembayaran</a>
                         <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_rincian_biaya/' . $r->idpendaftaran . '">Rincian Biaya</a>
                       </li>
                     </ul>
                     </div>
                   </div>';
				}
			} elseif ($r->status == 1) {
				//Menunggu Di Proses

				$tipeAction = '';
				if ($r->idtipe == '3') {
					if (UserAccesForm($user_acces_form, ['1072'])) {
						$tipeAction = '<a href="' . site_url() . 'tkasir/transaksi_ob/' . $r->idtindakan . '/' . $r->id . '/0' . '" target="_blank"  class="btn btn-primary btn-sm" data-toggle="tooltip" title="Transaksi"><i class="fa fa-pencil"></i></a>';
					}
				} else {
					if (UserAccesForm($user_acces_form, ['1072'])) {
						if ($r->ri_id == null) {
							$tipeAction = '<a href="' . site_url() . 'tkasir/transaksi/' . $r->idpendaftaran . '/' . $r->id . '/0' . '" target="_blank"  class="btn btn-primary btn-sm" data-toggle="tooltip" title="Transaksi"><i class="fa fa-pencil"></i></a>';
						}
					}
				}
				$action = '<div class="btn-group">' . $tipeAction;
				if (UserAccesForm($user_acces_form, ['1089'])) {
					$action .= '<div class="btn-group">
                      <div class="btn-group dropright">
                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                          <span class="fa fa-print"></span>
                        </button>
                      <ul class="dropdown-menu">
                        <li>
                          <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_transaksi/' . $r->idpendaftaran . '/' . $r->id . '">Rincian Pembayaran</a>
                          <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_rincian_biaya/' . $r->idpendaftaran . '">Rincian Biaya</a>
                        </li>
                      </ul>
                      </div>
                    </div>';
				}
				if (UserAccesForm($user_acces_form, ['1088'])) {
					// $action .= '';
				}
				$action .= '</div>';
			} elseif ($r->status == 2) {
				//SELESAI
				//Telah di proses
				$tipeAction = '';
				$action = '<div class="btn-group">' . $tipeAction;
				if (UserAccesForm($user_acces_form, ['1089'])) {
					$action .= '<div class="btn-group">
                      <div class="btn-group dropright">
                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                          <span class="fa fa-print"></span>
                        </button>
                      <ul class="dropdown-menu">
                        <li>
                          <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_transaksi/' . $r->idpendaftaran . '/' . $r->id . '">Rincian Pembayaran</a>
                          <a tabindex="-1" target="_blank" href="' . site_url() . 'tkasir/print_rincian_biaya/' . $r->idpendaftaran . '">Rincian Biaya</a>
                        </li>
                      </ul>
                      </div>
                    </div>';

					$action .= '<div class="btn-group" role="group">
								<button class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-expanded="false"  data-toggle="tooltip" title="Kwitansi">
									<span class="fa fa-list-alt "></span>
								</button>
								<ul class="dropdown-menu dropdown-menu-right">';
					$action .= '<li><a tabindex="-1" data-toggle="modal" href="#modalKwitansi" onclick="ShowOld(' . $r->id . ',0,0);">Kwitansi All</a></li><li class="divider"></li>';
					$list_bayar = $r->idmetode;
					$list_bayar_id = $r->idbayar;
					$arr_bayar = explode(',', $list_bayar);
					$arr_bayar_id = explode(',', $list_bayar_id);
					foreach ($arr_bayar as $x => $x_value) {
						if (metode_pembayaran_kasir($arr_bayar[$x]) != '') {
							$action .= '<li><a tabindex="-1" data-toggle="modal" href="#modalKwitansi" onclick="ShowOld(' . $r->id . ',' . $arr_bayar_id[$x] . ',' . $arr_bayar[$x] . ');">Print Out Kwitansi ' . metode_pembayaran_kasir($arr_bayar[$x]) . '</a></li>';
						}
					}

					$action .= '</ul></div>';
				}
				if (UserAccesForm($user_acces_form, ['1088'])) {
					if ($r->status_verifikasi == 0) {
						$action .= '<button data-idkasir="' . $r->id . '" class="btn btn-danger btn-sm selectedBatal" id="btn_pembayaran" data-toggle="modal"  data-target="#modalPembatalan"  type="button"><i class="fa fa-trash"></i></button>';
					}
				}
				$action .= '<a class="view btn btn-sm btn-danger" href="' . $url_dok_rajal . $r->id . '" target="_blank"  type="button"  title="Upload"><i class="fa fa-upload"></i></a>';
				$action .= '</div>';
			} else {
				$action = '';
			}

			$row[] = $no;
			$row[] = $r->tanggal;
			$row[] = $r->notrx;
			$row[] = $r->no_medrec;

			if ($r->title <> '') {
				$row[] = $r->title . '. ' . $r->nama;
			} else {
				$row[] = $r->nama;
			}

			$row[] = $r->namapoli;
			$row[] = $r->namakelompok;
			$row[] = '<p class="nice-copy">' .
											StatusIndexRujukan2($r->rujukfarmasi, 'FR', $r->far_status) . '&nbsp;' .
											StatusIndexRujukan2($r->rujuklaboratorium, 'LB', $r->lab_status) . '&nbsp;' .
											StatusIndexRujukan2($r->rujukradiologi, 'RD', $r->rad_status) . '&nbsp;' .
											StatusIndexRujukan2($r->rujukfisioterapi, 'FI', $r->fis_status) . '&nbsp;' .
										'</p>';
			$label_rencana = '';
			if ($r->statusrencana > 0) {
				$label_rencana = GetStatusRencana($r->statusrencana);
			}

			$row[] = $action;
			if ($r->ri_id != null && $r->status == '1') {
				$row[] = $label_rencana;
			} else {
				$row[] = StatusKasir($r->status) . '<br>' . $label_rencana;
			}

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}
	
}
