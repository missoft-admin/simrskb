<?php defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
class Lvalidasi_hutang extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Lvalidasi_hutang_model','model');
	}

	function index() {
		$date1=date_create(date('Y-m-d'));
		date_add($date1,date_interval_create_from_date_string("-30 days"));
		$date1= date_format($date1,"Y-m-d");
		$date2= date("Y-m-d");


		$data=array(
			'nojurnal'=>'',
			'idakun'=>'#',
			// 'tanggal_trx1'=>'',
			// 'tanggal_trx2'=>'',
			'tanggal_trx1'=>HumanDateShort($date1),
			'tanggal_trx2'=>HumanDateShort($date2),
		);
		$data['error'] 			= '';
		$data['list_akun'] 	= $this->model->list_akun();
		$data['title'] 			= 'Laporan Jurnal Hutang';
		$data['content'] 		= 'Lvalidasi_hutang/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Hutang",'tvalidasi_hutang/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$idakun=$this->input->post('idakun');
		$nojurnal=$this->input->post('nojurnal');
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		$where='';
		
		if ($idakun !='#'){
			$where .=" AND H.idakun='$idakun' ";
		}
		if ($nojurnal !=''){
			$where .=" AND H.notransaksi='$nojurnal' ";
		}
		
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(V.tanggal_transaksi) >='".YMDFormat($tanggal_trx1)."' AND DATE(V.tanggal_transaksi) <='".YMDFormat($tanggal_trx2)."'";
        }
        $from = "(
					SELECT H.id,V.tanggal_transaksi,H.notransaksi  as nojurnal,H.idakun,H.noakun,H.namaakun,H.kode_bantu 
					,SUM(H.debet) as debet,SUM(H.kredit) as kredit,H.keterangan,M.metode_bayar as cara_bayar
					,V.idtransaksi,H.idvalidasi,V.st_cara_bayar as tipe_bayar
					FROM `jurnal_umum` H
					LEFT JOIN tvalidasi_hutang V ON V.id=H.idvalidasi 
					LEFT JOIN ref_metode M ON M.id=V.st_cara_bayar
					WHERE H.ref_tabel_validasi='tvalidasi_hutang' AND H.`status`='1' ".$where."
					GROUP BY H.idvalidasi,H.idakun
					ORDER BY H.id ASC
				) as tbl";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nojurnal','namaakun','noakun','keterangan');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('lvalidasi_hutang/');
             $url_kbo        = site_url('tkontrabon/');
            $url_validasi        = site_url('tvalidasi_hutang/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->id;
            $row[] = $r->idtransaksi;
            $row[] = HumanDateShort($r->tanggal_transaksi);
            $row[] = $r->nojurnal;
            $row[] = $r->kode_bantu;
            $row[] = $r->noakun;
            $row[] = $r->namaakun;
            $row[] = ($r->debet > 0?number_format($r->debet,2):'');
            $row[] = ($r->kredit>0?number_format($r->kredit,2):'');
            $row[] = $r->keterangan;          
				$aksi .= '<a href="'.$url_kbo.'detail_kontrabon/'.$r->idtransaksi.'" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
				$aksi .= '<a href="'.$url_validasi.'detail/'.$r->idvalidasi.'/disabled" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
				$aksi .= '<a href="'.$url.'detail/'.$r->idvalidasi.'/disabled" target="_blank" title="Detail" class="btn btn-xs btn-danger"><i class="si si-doc"></i></a>';
				// if ($r->tipe_bayar=='1'){
					$aksi .= '<a href="'.$url_kbo.'kontrabon_kas/'.$r->idtransaksi.'" target="_blank" title="Pembayaran" class="btn btn-xs btn-primary"><i class="fa fa-credit-card"></i></a>';
					
				// }
				$aksi .= '<button disabled title="History Transaksi" class="btn btn-xs btn-success"><i class="fa fa-print"></i></button>';
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	
	function detail($id) {
		$data=$this->model->getHeader($id);
		// print_r($data);exit();
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['title'] 			= 'Kategori Akuntansi';
		$data['content'] 		= 'Lvalidasi_hutang/detail';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("ValidasiHutang",'tvalidasi_hutang/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	
	function load_ringkasan(){
		
		$id=$this->input->post('id');
		
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT *FROM tvalidasi_hutang D
				WHERE D.id='$id'
				";
		$tbl='';		
		$rows=$this->db->query($q)->row();	
		$q="SELECT SUM(nominal) as nominal from tvalidasi_hutang_bayar A WHERE A.idvalidasi='$id' AND A.st_biaya_tf='1'";
		
		$biaya_tf=$this->db->query($q)->row('nominal');
		
		$tbl .='<tr>';
		$tbl .='<td class="text-right">1</td><td class="text-center"><strong>HUTANG</strong></td><td class="text-right"><strong>'.number_format($rows->nominal_bayar,2).'</strong></td>';
		$tbl .='</tr>';
		$tbl .='<tr>';
		$tbl .='<td class="text-right">2</td><td class="text-center"><strong>BIAYA CHEQ</strong></td><td class="text-right"><strong>'.number_format($rows->nominal_cheq,2).'</strong></td>';
		$tbl .='</tr>';
		$tbl .='<tr>';
		$tbl .='<td class="text-right">3</td><td class="text-center"><strong>BIAYA MATERAI</strong></td><td class="text-right"><strong>'.number_format($rows->nominal_materai,2).'</strong></td>';
		$tbl .='</tr>';
		$tbl .='<tr>';
		$tbl .='<td class="text-right">3</td><td class="text-center"><strong>BIAYA TRANSFER</strong></td><td class="text-right"><strong>'.number_format($biaya_tf,2).'</strong></td>';
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	public function pdf($row_detail,$header){
		// print_r($header);exit();
		$dompdf = new Dompdf();
		$data=array();
		$data=array_merge($data,$header);
        
        $data['detail'] = $row_detail;

		$data = array_merge($data, backend_info());

        $html = $this->load->view('Lvalidasi_hutang/pdf', $data, true);
		// print_r($html	);exit();
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('JurnalHutang.pdf', array("Attachment"=>0));
	}
	public function export()
    {
		// print_r($this->input->post());exit();
        ini_set('memory_limit', '-1');
        set_time_limit(1000);
        $idakun=$this->input->post('idakun');
		$nojurnal=$this->input->post('nojurnal');
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		$where='';
		
		if ($idakun !='#'){
			$where .=" AND H.idakun='$idakun' ";
		}
		if ($nojurnal !=''){
			$where .=" AND H.notransaksi='$nojurnal' ";
		}
		
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(V.tanggal_transaksi) >='".YMDFormat($tanggal_trx1)."' AND DATE(V.tanggal_transaksi) <='".YMDFormat($tanggal_trx2)."'";
        }
        $from = "
					SELECT H.id,V.tanggal_transaksi,H.notransaksi  as nojurnal,H.idakun,H.noakun,H.namaakun,H.kode_bantu 
					,SUM(H.debet) as debet,SUM(H.kredit) as kredit,H.keterangan,M.metode_bayar as cara_bayar
					,V.idtransaksi,H.idvalidasi,V.st_cara_bayar as tipe_bayar
					FROM `jurnal_umum` H
					LEFT JOIN tvalidasi_hutang V ON V.id=H.idvalidasi 
					LEFT JOIN ref_metode M ON M.id=V.st_cara_bayar
					WHERE H.ref_tabel_validasi='tvalidasi_hutang' AND H.`status`='1' ".$where."
					GROUP BY H.idvalidasi,H.idakun
					ORDER BY H.id ASC
				";
				
        $btn    = $this->input->post('btn_export');
        $row_detail=$this->db->query($from)->result();
        
		if ($btn=='1'){
			$this->excel($row_detail);
		}
		if ($btn=='2'){
			$this->pdf($row_detail,$this->input->post());
		}
		if ($btn=='3'){
			$this->pdf($row_detail,$this->input->post());
		}
		
		
		
    }
	function excel($row_detail){
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Jurnal Hutang');

        // Set Logo
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $logo = FCPATH.'/assets/upload/logo/logoreport.jpg';
        $objDrawing->setPath($logo);
        $objDrawing->setCoordinates('B1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setHeight(70);
        $objDrawing->setWidth(40);
        $objDrawing->setWorksheet($activeSheet);

        // Set Header
        $activeSheet->setCellValue('B1', "                                      RUMAH SAKIT KHUSUS BEDAH HALMAHERA SIAGA");
        $activeSheet->setCellValue('B2', "                                      Jl.LLRE.Martadinata no.28, Telp: +6222-4206061");
        $activeSheet->setCellValue('B3', "                                      Bandung, 40115");
        $activeSheet->mergeCells('B1:AA1');
        $activeSheet->mergeCells('B2:AA2');
        $activeSheet->mergeCells('B3:AA3');
        $activeSheet->getStyle('B1:AA1')->getFont()->setSize(11);
        $activeSheet->getStyle('B2:AA2')->getFont()->setSize(11);
        $activeSheet->getStyle('B3:AA3')->getFont()->setSize(11);

		// Set Title
		$activeSheet->setCellValue('B5', "JURNAL HUTANG ");
		$activeSheet->mergeCells('B5:H5');
		$activeSheet->getStyle('B5')->getFont()->setBold(true)->setSize(14);
		$activeSheet->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$activeSheet->setCellValue('B7', "TANGGAL");
		$activeSheet->setCellValue('C7', "NO BUKTI");
		$activeSheet->setCellValue('D7', "KODE AKUN");
		$activeSheet->setCellValue('E7', "NAMA AKUN");
		$activeSheet->setCellValue('F7', "DEBIT");
		$activeSheet->setCellValue('G7', "KREDIT");
		$activeSheet->setCellValue('H7', "KETERANGAN");
		$activeSheet->getStyle('B7:H7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("B7:H7")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
	  'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK,
					)
				),
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Calibri'
				)
			)
		);

		$x = 7;
		$debet = 0;
		$kredit= 0;
		
        if (COUNT($row_detail)) {
            foreach ($row_detail as $row) {
				$x = $x+1;
				$debet=(int)$debet+(int)$row->debet;
				$kredit=(int)$kredit+(int)$row->kredit;
				// $debet = $debet+$row->debet;
				// $kredit = $kredit+$row->kredit;
				
	            $activeSheet->setCellValue("B$x", HumanDateShort($row->tanggal_transaksi));
	            $activeSheet->setCellValue("C$x", $row->nojurnal);
	            $activeSheet->setCellValue("D$x", $row->noakun);
	            $activeSheet->setCellValue("E$x", $row->namaakun);
	            $activeSheet->setCellValue("F$x", $row->debet);
	            $activeSheet->setCellValue("G$x", $row->kredit);
	            $activeSheet->setCellValue("H$x", $row->keterangan);
				
	           
				
				
			}
		}
		$x = $x+1;
		$activeSheet->setCellValue("E$x", "TOTAL");
		// $activeSheet->setCellValue("B$x", "");
		// $activeSheet->setCellValue("C$x", "");
		// $activeSheet->setCellValue("D$x", "");
		$activeSheet->setCellValue("F$x", $debet);
		$activeSheet->setCellValue("G$x", $kredit);
		// $activeSheet->setCellValue("H$x", "");
		$activeSheet->getStyle("D7:D$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("H7:H$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$activeSheet->getStyle("E7:E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$activeSheet->getStyle("F7:G$x")->getNumberFormat()->setFormatCode('#,##0');
		$activeSheet->getStyle("B7:H$x")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
					)
				)
			)
		);
		
    	// Set Auto Width Column
    	$activeSheet->getColumnDimension("A")->setWidth(5);
    	$activeSheet->getColumnDimension("B")->setAutoSize(true);
    	$activeSheet->getColumnDimension("C")->setAutoSize(true);
    	$activeSheet->getColumnDimension("D")->setAutoSize(true);
    	$activeSheet->getColumnDimension("E")->setAutoSize(true);
    	$activeSheet->getColumnDimension("F")->setAutoSize(true);
    	$activeSheet->getColumnDimension("G")->setAutoSize(true);
    	$activeSheet->getColumnDimension("H")->setWidth(100);
    	// $activeSheet->getColumnDimension("H")->setAutoSize(true);

    	header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    	header('Content-Disposition: attachment;filename="Jurnal Hutang '.date('Y-m-d-h-i-s').'.xls"');
    	header('Cache-Control: max-age=0');

    	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    	$objWriter->save('php://output');
	}
	
}
