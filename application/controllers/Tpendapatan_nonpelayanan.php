<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpendapatan_nonpelayanan extends CI_Controller
{

    /**
     * Pendapatan Non Pelayanan controller.
     * Developer @GunaliRezqiMauludi
     */

    function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Tpendapatan_nonpelayanan_model');
        $this->load->model('Thonor_dokter_model');
    }

    function index()
    {
        $data = array(
          'iddokter' => '',
          'status_pendapatan' => '',
          'status_periode' => '',
        );

        $data['uri']        = 'index';
        $data['error']      = '';
        $data['title']      = 'Pendapatan Non Pelayanan';
        $data['content']    = 'Tpendapatan_nonpelayanan/index';
        $data['breadcrum']  = array(
																array("RSKB Halmahera",'#'),
	                              array("Pendapatan Non Pelayanan",'#'),
	                              array("List",'tpendapatan_nonpelayanan')
															);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function filter()
    {
        $data = array(
          'iddokter' => $this->input->post('iddokter'),
          'status_pendapatan' => $this->input->post('status_pendapatan'),
          'status_periode' => $this->input->post('status_periode'),
        );

        $this->session->set_userdata($data);

        $data['uri']        = 'filter';
        $data['error']      = '';
        $data['title']      = 'Pendapatan Non Pelayanan';
        $data['content']    = 'Tpendapatan_nonpelayanan/index';
        $data['breadcrum']  = array(
                                array("RSKB Halmahera",'#'),
                                array("Pendapatan Non Pelayanan",'#'),
                                array("List",'tpendapatan_nonpelayanan')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function save()
    {
        $namatarif = $this->input->post('deskripsi');
        $iddokter = $this->input->post('iddokter');
        $namadokter = $this->input->post('namadokter');
        $idkategori = $this->input->post('idkategori');
        $namakategori = $this->input->post('namakategori');
        $jasamedis = RemoveComma($this->input->post('jasamedis'));
        $nominal_potongan_rs = RemoveComma($this->input->post('nominal_potongan_rs'));
        $nominal_pajak_dokter = RemoveComma($this->input->post('nominal_pajak_dokter'));
        $jasamedis_netto = RemoveComma($this->input->post('jasamedis_netto'));
        $tanggal_pemeriksaan = date("Y-m-d H:i:s");
        $tanggal_pembayaran = YMDFormat($this->input->post('tanggal_pembayaran'));
        $tanggal_jatuhtempo = YMDFormat($this->input->post('tanggal_jatuhtempo'));
        $status_pendapatan = $this->input->post('status_pendapatan');
        $status_periode = $this->input->post('status_periode');

        $data = array();
        $data['tanggal']              = $tanggal_pemeriksaan;
        $data['iddokter']             = $iddokter;
        $data['deskripsi']            = $namatarif;
        $data['jasamedis']            = $jasamedis;
        $data['nominal_potongan_rs']  = $nominal_potongan_rs;
        $data['nominal_pajak_dokter'] = $nominal_pajak_dokter;
        $data['jasamedis_netto']      = $jasamedis_netto;
        $data['status_pendapatan']    = $status_pendapatan;

        if ($status_pendapatan == '2') {
          $data['tanggal_pembayaran']   = $tanggal_pembayaran;
          $data['tanggal_jatuhtempo']   = $tanggal_jatuhtempo;
        } else {
          $data['status_periode']       = $status_periode;
        }

        if ($this->db->insert('tpendapatan_nonpelayanan', $data)) {
            $idtransaksi = $this->db->insert_id();

            if ($status_pendapatan == '2') {
              $isStopPeriode = $this->Thonor_dokter_model->checkStatusHonorDokterStopped($iddokter, $tanggal_pembayaran);
              if ($isStopPeriode == null) {
                $idhonor = $this->Thonor_dokter_model->checkStatusHonorDokter($iddokter, $tanggal_pembayaran);

                if ($idhonor == null) {
                    $dataHonorDokter = array(
                      'iddokter' => $iddokter,
                      'namadokter' => $namadokter,
                      'tanggal_pembayaran' => $tanggal_pembayaran,
                      'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
                      'nominal' => 0,
                      'created_at' => date('Y-m-d H:i:s'),
                      'created_by' => $this->session->userdata('user_id')
                    );

                    if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
                        $idhonor = $this->db->insert_id();
                    }
                }
              }

              $dokter = $this->db->query("SELECT mdokter.id,
                  mdokter.nama,
                  mdokter_kategori.id AS idkategori,
                  mdokter_kategori.nama AS namakategori
                FROM
                  mdokter
                JOIN mdokter_kategori ON mdokter_kategori.id=mdokter.idkategori
                WHERE
                  mdokter.id = $iddokter AND
                  mdokter.status = 1")->row();

              $dataDetailHonorDokter = array(
                'idhonor' => $idhonor,
                'idtransaksi' => $idtransaksi,
                'jenis_transaksi' => 'pendapatan',
                'jenis_tindakan' => 'PENDAPATAN NON PELAYANAN',
                'reference_table' => 'tpendapatan_nonpelayanan',
                'namatarif' => $namatarif,
                'idkategori' => $dokter->idkategori,
                'namakategori' => $dokter->namakategori,
                'iddokter' => $iddokter,
                'namadokter' => $namadokter,
                'jasamedis' => $jasamedis,
                'potongan_rs' => 0,
                'nominal_potongan_rs' => $nominal_potongan_rs,
                'pajak_dokter' => 0,
                'nominal_pajak_dokter' => $nominal_pajak_dokter,
                'jasamedis_netto' => $jasamedis_netto,
                'tanggal_pemeriksaan' => $tanggal_pemeriksaan,
                'tanggal_pembayaran' => $tanggal_pembayaran,
                'tanggal_jatuhtempo' => $tanggal_jatuhtempo
              );

              $this->db->insert('thonor_dokter_detail', $dataDetailHonorDokter);
            }
        }
    }

    function delete()
    {
        $idtransaksi = $this->input->post('idrow');
        $this->db->set('status', '0');
        $this->db->where('id', $idtransaksi);
        if ($this->db->update('tpendapatan_nonpelayanan')) {
            $this->db->where('jenis_transaksi', 'pendapatan');
            $this->db->where('jenis_tindakan', 'PENDAPATAN NON PELAYANAN');
            $this->db->where('idtransaksi', $idtransaksi);
            $this->db->delete('thonor_dokter_detail');
            return true;
        } else {
            return false;
        }
    }

  	function getIndex($uri)
  	{
    		$data_user=get_acces();
    		$user_acces_form=$data_user['user_acces_form'];

        $this->select = array(
          'tpendapatan_nonpelayanan.*',
          'mdokter.nama AS namadokter'
        );

  			$this->from   = 'tpendapatan_nonpelayanan';

  			$this->join 	= array(
  				array('mdokter', 'mdokter.id = tpendapatan_nonpelayanan.iddokter', ''),
  			);

        $this->where  = array(
          'tpendapatan_nonpelayanan.status' => '1',
        );

        // FILTER
        if($uri == 'filter'){
      		if ($this->session->userdata('iddokter') != "#") {
            $this->where = array_merge($this->where, array('iddokter.id' => $this->session->userdata('iddokter')));
          }
      		if ($this->session->userdata('status_pendapatan') != "#") {
            $this->where = array_merge($this->where, array('status_pendapatan.id' => $this->session->userdata('status_pendapatan')));
          }
      		if ($this->session->userdata('status_periode') != "#") {
            $this->where = array_merge($this->where, array('status_periode.id' => $this->session->userdata('status_periode')));
          }
        }

  			$this->order  = array(
          'tpendapatan_nonpelayanan.id' => 'DESC',
  			);

  			$this->group  = array('tpendapatan_nonpelayanan.id');

  			$this->column_search   = array('tpendapatan_nonpelayanan.tanggal', 'mdokter.nama', 'tpendapatan_nonpelayanan.deskripsi');
  			$this->column_order    = array('tpendapatan_nonpelayanan.tanggal', 'mdokter.nama', 'tpendapatan_nonpelayanan.deskripsi');
  			$list = $this->datatable->get_datatables();

        $data = array();
  			$no = $_POST['start'];
  			foreach ($list as $r) {
  					$no++;
  					$row = array();

            if ($r->status_pendapatan == 1) {
              $status_pendapatan = '<label class="label label-md label-success">Rutin</label>';
              if ($r->status_periode == 1) {
                $status_periode = 'Setiap Tanggal';
              } else {
                $status_periode = 'Setiap Bulan';
              }
            } else {
              $status_pendapatan = '<label class="label label-md label-warning">Non Rutin</label><br>';
              $status_periode = DMYFormat($r->tanggal_pembayaran);
            }

  					$row[] = $no;
  					$row[] = $r->tanggal;
  					$row[] = $r->namadokter;
  					$row[] = $r->deskripsi;
  					$row[] = number_format($r->jasamedis);
  					$row[] = number_format($r->nominal_potongan_rs);
  					$row[] = number_format($r->nominal_pajak_dokter);
  					$row[] = number_format($r->jasamedis_netto);
  					$row[] = $status_pendapatan;
  					$row[] = $status_periode;

  					$row[] = '<div class="btn-group">
              <button type="button" class="btn btn-sm btn-danger deletePendapatanNonPelayanan" data-idrow="'.$r->id.'" title="Hapus"><i class="fa fa-trash"></i></a>
              <button type="button" class="btn btn-sm btn-success" title="Print"><i class="fa fa-print"></i></a>
            </div>';

  					$data[] = $row;
  			}
  			$output = array(
  				"draw" => $_POST['draw'],
  				"recordsTotal" => $this->datatable->count_all(),
  				"recordsFiltered" => $this->datatable->count_all(),
  				"data" => $data
  			);
  			echo json_encode($output);
  	}
}
