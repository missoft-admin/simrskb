<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtarif_radiologi_expose extends CI_Controller {

	/**
	 * Expose Radiologi controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtarif_radiologi_expose_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Expose Radiologi';
		$data['content'] 		= 'Mtarif_radiologi_expose/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Expose Radiologi",'#'),
									    			array("List",'mtarif_radiologi_expose')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Expose Radiologi';
		$data['content'] 		= 'Mtarif_radiologi_expose/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Expose Radiologi",'#'),
									    			array("Tambah",'mtarif_radiologi_expose')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mtarif_radiologi_expose_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Expose Radiologi';
				$data['content']	 	= 'Mtarif_radiologi_expose/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Expose Radiologi",'#'),
											    			array("Ubah",'mtarif_radiologi_expose')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtarif_radiologi_expose','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtarif_radiologi_expose');
		}
	}

	function delete($id){
		$this->Mtarif_radiologi_expose_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mtarif_radiologi_expose','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mtarif_radiologi_expose_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_radiologi_expose','location');
				}
			} else {
				if($this->Mtarif_radiologi_expose_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_radiologi_expose','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mtarif_radiologi_expose/manage';

		if($id==''){
			$data['title'] = 'Tambah Expose Radiologi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Expose Radiologi",'#'),
															array("Tambah",'mtarif_radiologi_expose')
													);
		}else{
			$data['title'] = 'Ubah Expose Radiologi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Expose Radiologi",'#'),
															array("Ubah",'mtarif_radiologi_expose')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$this->from   = 'mtarif_radiologi_expose';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'id' => 'DESC'
			);
			$this->group  = array();
			if (UserAccesForm($user_acces_form,array('235'))){
			$this->column_search   = array('nama');
			}else{
				$this->column_search   = array();
			}
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->nama;
					$aksi = '<div class="btn-group">';
		            if (UserAccesForm($user_acces_form,array('237'))){
		                $aksi .= '<a href="'.site_url().'mtarif_radiologi_expose/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
		            }
		            if (UserAccesForm($user_acces_form,array('238'))){
		                $aksi .= '<a href="#" data-urlindex="'.site_url().'mtarif_radiologi_expose" data-urlremove="'.site_url().'mtarif_radiologi_expose/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
		            }
		            $aksi .= '</div>';            
  					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
}
