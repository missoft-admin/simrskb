<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trekon_obat extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Trekon_obat_model');
		$this->load->model('Tpendaftaran_poli_pasien_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->helper('path');
		
  }
	function index($tab='1'){
		$log['path_tindakan']='trekon_obat';
		$this->session->set_userdata($log);
		$user_id=$this->session->userdata('user_id');
		// print_r($this->session->userdata('path_tindakan'));exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$q="SELECT H.id as login_mppa_id,H.pegawai_id as login_iddokter,H.pegawai_id as iddokter FROM mppa H WHERE H.user_id='$user_id' AND H.tipepegawai='2' AND H.staktif='1'";
		// $login_iddokter=$this->db->query($q)->row('login_iddokter');	
		if (UserAccesForm($user_acces_form,array('2038'))){
			$data = $this->db->query($q)->row_array();	
			if ($data){
				
				$date2=date_create(date('Y-m-d'));
				date_add($date2,date_interval_create_from_date_string("-30 days"));
				$date2= date_format($date2,"d/m/Y");
				
				$data['idpoli'] 			= '#';
				$data['namapasien'] 			= '';
				$data['tab'] 			= $tab;
				
				$data['tanggal_1'] 			= $date2;
				$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
				$data['error'] 			= '';
				$data['title'] 			= 'My Pasien';
				$data['content'] 		= 'Trekon_obat/index';
				$data['breadcrum'] 	= array(
													  array("RSKB Halmahera",'#'),
													  array("Tindakan Poliklinik",'#'),
													  array("My Pasien",'tpendaftaran_poli_pasien')
													);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				redirect('page404');
			}
		}else{
			redirect('page404');
		}
	}
	
	function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
						
			$idpoli =$this->input->post('idpoli');
			$iddokter =$this->input->post('iddokter');
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$pencarian =$this->input->post('pencarian');
			$tab =$this->input->post('tab');
			if ($tanggal_1 !=''){
				$where .=" AND DATE(TR.tanggal_input) >='".YMDFormat($tanggal_1)."' AND DATE(TR.tanggal_input) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(TR.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($pencarian!=''){
				$where .=" AND ((H.namapasien LIKE '%".$pencarian."%')";
				$where .=" OR (H.no_medrec LIKE '%".$pencarian."%'))";
			}
			if ($tab=='2'){
				$where .=" AND ((O.st_jawab = O.st_total))";
			}
			if ($tab=='3'){
				$where .=" AND ((O.st_jawab <> O.st_total))";
			}
			
			// print_r($tab);exit;
			$this->select = array();
			
			$from="
				(
					SELECT TR.assesmen_id,TR.pendaftaran_id_ranap,O.st_jawab,O.st_setuju,O.st_total
					,MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
					,MK.nama as nama_kelompok,MR.nama as nama_rekanan,MDP.nama as nama_dokter_poli
					,H.*,MKL.nama as nama_kelas 
					,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header
					,MB.nama as nama_bed,TP.tanggal,MRU.nama as nama_ruangan,AH.kodeantrian as kode_antrian
					,UH.name as nama_hapus,GROUP_CONCAT(MDHP.nama SEPARATOR ', ') as nama_dokter_pendamping
					,TP.suhu,TP.nadi,TP.nafas,TP.td_sistole,TP.td_diastole,TP.tinggi_badan,TP.berat_badan
						,mnadi.warna as warna_nadi
						,mnafas.warna as warna_nafas
						,mtd_sistole.warna as warna_sistole
						,mtd_diastole.warna as warna_diastole
						,msuhu.warna as warna_suhu
						,TP.berat_badan/((TP.tinggi_badan/100)*2) as masa_tubuh,mberat.warna as warna_berat
					FROM tranap_rekon_obat TR
					INNER JOIN trawatinap_history_dpjp D ON D.idrawatinap=TR.pendaftaran_id_ranap AND D.st_aktif='1' AND D.`status`='1'
					INNER JOIN 
					(
						SELECT H.assesmen_id,SUM(CASE WHEN H.st_setuju=0 THEN 1 ELSE 0 END) as st_jawab,SUM(CASE WHEN H.st_setuju=1 THEN 1 ELSE 0 END) as st_setuju, SUM(1) as st_total FROM tranap_rekon_obat_konsumsi H
						WHERE H.`status`='1'
						GROUP BY H.assesmen_id
					) O ON O.assesmen_id=TR.assesmen_id 
					INNER JOIN trawatinap_pendaftaran H ON H.id=TR.pendaftaran_id_ranap
					LEFT JOIN trawatinap_pendaftaran_dpjp HP ON HP.pendaftaran_id=H.id
					LEFT JOIN mdokter MDHP ON MDHP.id=HP.iddokter
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=H.idruangan
					LEFT JOIN mkelas MKL ON MKL.id=H.idkelas
					LEFT JOIN antrian_harian AH ON AH.id=H.antrian_id
					LEFT JOIN mdokter MDP ON MDP.id=TP.iddokter
					LEFT JOIN mdokter MD ON MD.id=H.iddokterpenanggungjawab
					LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik_asal
					LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
					LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
					LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
					LEFT JOIN mbed MB on MB.id=H.idbed						
					LEFT JOIN musers UH on UH.id=H.deleted_by	
					LEFT JOIN (
							".get_alergi_sql()."
						) A ON A.idpasien=H.idpasien	
					LEFT JOIN (SELECT * FROM (SELECT * FROM tranap_ttv WHERE status_ttv > 1 ORDER BY tanggal_input DESC) XX GROUP BY pendaftaran_id_ranap) TP ON TP.pendaftaran_id_ranap = H.id
					LEFT JOIN mnadi ON TP.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
					LEFT JOIN mnafas ON TP.nafas BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
					LEFT JOIN mtd_sistole ON TP.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
					LEFT JOIN mtd_diastole ON TP.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
					LEFT JOIN msuhu ON TP.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
					LEFT JOIN mberat ON TP.berat_badan/((TP.tinggi_badan/100)*2) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
					WHERE TR.status_assemen IN (1,2) AND TR.obat_sebelum_masuk='1' AND D.iddokter ='$iddokter' ".$where."
					GROUP BY TR.assesmen_id
				) as tbl
			";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
	  $data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  if ($r->idtipe=='1'){
			  $info_bed='<div class="h4 push-10-t text-primary"><button class="btn btn-xs btn-block btn-primary push-10" type="button"><i class="fa fa-bed pull-left"></i> '.($r->nama_bed).'</button></strong></div>';
		  }else{
			  $info_bed='';
		  }
		  
		  $div_terima='';
		  $div_hapus='';
		  if ($r->st_terima_ranap=='1'){
			  $div_terima='<div class="text-primary text-uppercase"><i class="fa fa-bed"></i> Tanggal Terima : '.HumanDateLong($r->tanggal_terima).'</div>';
			  $div_terima.='<div class="text-primary text-uppercase"><i class="fa fa-user"></i> User Terima : '.($r->user_nama_terima).'</div>';
		  }
		  if ($r->status=='0'){
			  $div_hapus='<div class="text-danger text-uppercase"><i class="fa fa-trash"></i> Tanggal Batal : '.HumanDateLong($r->deleted_date).'</div>';
			  $div_hapus.='<div class="text-danger text-uppercase"><i class="fa fa-user"></i> User  : '.($r->nama_hapus).'</div>';
		  }
		  $btn_setuju='';
		  $btn_layani='';
		  $btn_panel='';
		  $pedamping='';
		  if ($r->nama_dokter_pendamping){
			  $pedamping='<div class="text-wrap text-danger"><i class="fa fa-user-md"></i> PENDAMPING : '.($r->nama_dokter_pendamping).'</div>';
		  }
			  if ($r->status!='0'){
					  $btn_layani='<div class="push-5-t"><a href="'.base_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_rekon/setuju_rekon_obat/'.$r->assesmen_id.'" target="_blank" type="button" data-toggle="tooltip" title="Lihat Data"  class="btn btn-block btn-primary btn-xs"><i class="fa fa-eye"></i> LIHAT DATA</a> </div>';
					  if ($r->st_jawab>0){
						  
						$btn_setuju='<div class="push-5-t"><button type="button" onclick="setujui_all('.$r->assesmen_id.')" data-toggle="tooltip" title="Setuju"  class="btn btn-block btn-success btn-xs "><i class="fa fa-check"></i> SETUJUI</button> </div>';
					  }
				 
					  $btn_panel=div_panel_kendali_ri($user_acces_form,$r->id);
			  }
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									'.$btn_layani.'
									'.$btn_setuju.'
									'.$btn_panel.'
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
										<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
									</div>
									<div class="text-primary font-s13 push-5-t">'.$r->nopendaftaran_poli.' | '.HumanDateShort($r->tanggal).' | '.GetAsalRujukanR($r->idtipe_asal).' | '.$r->nama_poli.' | '.$r->nama_dokter_poli.'</div>
									
									
									</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_5='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Suhu Tubuh :</div>
									<div class=" text-muted " style="color:'.$r->warna_suhu.'"> '.($r->suhu).' '.$data_satuan_ttv['satuan_suhu'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frek Nadi :</div>
									<div class=" text-muted " style="color:'.$r->warna_nadi.'"> '.($r->nadi).' '.$data_satuan_ttv['satuan_nadi'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frek Nafas :</div>
									<div class=" text-muted " style="color:'.$r->warna_nafas.'"> '.($r->nafas).' '.$data_satuan_ttv['satuan_nafas'].'</div>
								</td>
								
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Tekanan Darah :</div>
									<div class=" text-muted "> <label  class="text-muted" style="color:'.$r->warna_sistole.'">'.($r->td_sistole.' '.$data_satuan_ttv['satuan_td'].'</label> / <label class="text-muted " style="color:'.$r->warna_diastole.'">'.$r->td_diastole).' '.$data_satuan_ttv['satuan_td'].'</label></div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Berat Badan :</div>
									<div class=" text-muted " style="color:'.$r->warna_berat.'"> '.($r->berat_badan).' Kg</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Tinggi Badan :</div>
									<div class=" text-muted style="color:'.$r->warna_berat.'""> '.($r->tinggi_badan).' Cm</div>
								</td>
								
							</tr>
						</tbody>
					</table>';
		  
          $result[] = $btn_5;
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class=" text-uppercase">'.($r->nama_ruangan).' - '.$r->nama_kelas.' - '.$r->nama_bed.'</div>
									<div class=""><i class="fa fa-user-md"></i> DPJP : '.($r->nama_dokter).'</div>
									'.$pedamping.'
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->nama_rekanan?$r->nama_rekanan:$r->nama_kelompok).'</div>
									'.$div_terima.'
									'.$div_hapus.'
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.$this->status_jawaban($r->st_jawab,$r->st_setuju,$r->st_total).'</div>
									'.$info_bed.'
									<div class="h5 push-10-t text-primary"><strong>'.GetTipeRujukanRawatInap($r->idtipe).'</strong></div>
									<div class="h5 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggaldaftar).'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function status_jawaban($st_jawab,$st_setuju,$st_total){
	  $btn_jawaban='';
	  if ($st_setuju=='0'){
		  $btn_jawaban='<button class="btn btn-xs btn-default btn-block" type="button">MENUNGGU JAWABAN</button>';
	  }else{
		  if ($st_setuju!=$st_total){
			  $btn_jawaban='<button class="btn btn-xs btn-warning btn-block" type="button">DISETUJUI SEBAGIAN</button>';
		  }else{
			  $btn_jawaban='<button class="btn btn-xs btn-success btn-block" type="button">DISETUJUI </button>';
		  }
	  }
	 
	  return $btn_jawaban;
  }
  function setujui_all(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'st_setuju' => 1,
		'approved_ppa' => $login_ppa_id,
		'approved_date' => date('Y-m-d H:i:s'),
	  );
	  // print_r($data);exit;
	  $this->db->where('st_setuju',0);
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_rekon_obat_konsumsi',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function setuju_satu(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $id=$this->input->post('id');
	  $data=array(
		'st_setuju' => 1,
		'approved_ppa' => $login_ppa_id,
		'approved_date' => date('Y-m-d H:i:s'),
	  );
	  // print_r($data);exit;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tranap_rekon_obat_konsumsi',$data);
	  $this->output->set_output(json_encode($hasil));
  }
 
}	
