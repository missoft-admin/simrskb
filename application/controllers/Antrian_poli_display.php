<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Antrian_poli_display extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Antrian_poli_display_model');
        $this->load->helper('path');
    }

    public function index(): void
    {
        $data_user = get_acces();
        $user_acces_form = $data_user['user_acces_form'];

        if (UserAccesForm($user_acces_form, ['1557'])) {
            $data['error'] = '';
            $data['title'] = 'Setting Display Antrian';
            $data['content'] = 'Antrian_poli_display/index';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Antrian Setting Display', '#'],
                ['Index Setting', 'antrian_poli_display'],
            ];

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            redirect('page404');
        }
    }

    public function getIndex(): void
    {
        $data_user = get_acces();
        $user_acces_form = $data_user['user_acces_form'];

        $this->select = [];
        $this->from = '(SELECT * FROM antrian_poli_display H ORDER BY H.id) as tbl';
        $this->join = [];

        $this->order = [];
        $this->group = [];
        $this->column_search = ['nama_display'];
        $this->column_order = [];

        $list = $this->datatable->get_datatables(true);
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $result = [];

            $result[] = $no;
            $result[] = $r->nama_display;
            $result[] = $r->judul_header;
            $result[] = $r->alamat;
            $result[] = $r->telepone;
            $result[] = ($r->status ? text_primary('AKTIF') : text_danger('TIDAK AKTIF'));
            $aksi = '<div class="btn-group">';
            if ('1' === $r->status) {
                if (UserAccesForm($user_acces_form, ['1539'])) {
                    $aksi .= '<a href="'.site_url().'antrian_poli_display/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
                }
                if (UserAccesForm($user_acces_form, ['1540'])) {
                    $aksi .= '<button title="Hapus" type="button" onclick="removeData('.$r->id.')" class="btn btn-danger btn-xs "><i class="fa fa-trash-o"></i></button>';
                }
                $aksi .= '<a href="'.site_url().'antrian_poli_tv/display/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Lihat" class="btn btn-info btn-xs"><i class="fa fa-tv"></i></a>';
            } else {
                if (UserAccesForm($user_acces_form, ['1539'])) {
                    $aksi .= '<button title="Aktifkan" onclick="aktifkan('.$r->id.')" type="button" class="btn btn-success btn-xs aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
                }
            }
            $aksi .= '</div>';

            if ('1' === $r->status) {
                $aksi .= '&nbsp;<div class="btn-group">';
                if (UserAccesForm($user_acces_form, ['1539'])) {
                    $aksi .= '<a href="'.site_url().'antrian_poli_display/setting_khusus/'.$r->id.'" data-toggle="tooltip" title="Setting Display Khusus" class="btn btn-success btn-xs"><i class="fa fa-gear"></i></a>';
                }
                $aksi .= '<a href="'.site_url().'antrian_poli_tv/display_khusus/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Show Display Khusus" class="btn btn-primary btn-xs"><i class="fa fa-tv"></i></a>';
                $aksi .= '</div>';
            }

            $result[] = $aksi;

            $data[] = $result;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(true),
            'recordsFiltered' => $this->datatable->count_all(true),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function create(): void
    {
        $data = [
            'id' => '',
            'nama_display' => '',
            'bg_color' => '#14adc4',
            'header_logo' => '',
            'judul_header' => '',
            'alamat' => '',
            'telepone' => '',
            'email' => '',
            'website' => '',
            'judul_sub_header' => '',
        ];
        $data['list_sound'] = [];
        $data['error'] = '';
        $data['title'] = 'Create Display Antrian';
        $data['content'] = 'Antrian_poli_display/manage';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Antrian Setting', '#'],
            ['Index Display', 'antrian_poli_display'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id): void
    {
        $data = $this->Antrian_poli_display_model->get_index_setting($id);
        $data['list_tujuan'] = $this->Antrian_poli_display_model->list_tujuan();
        $data['error'] = '';
        $data['title'] = 'Edit Display Antrian';
        $data['content'] = 'Antrian_poli_display/manage';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Antrian Setting', '#'],
            ['Index Display', 'antrian_poli_display'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function delete($id): void
    {
        $result = $this->Antrian_poli_display_model->softDelete($id);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }

    public function aktifkan($id): void
    {
        $result = $this->Antrian_poli_display_model->aktifkan($id);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }

    public function save_general(): void
    {
        if (true === $this->Antrian_poli_display_model->save_general()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('antrian_poli_display', 'location');
        }
    }

    public function simpan_running(): void
    {
        $display_id = $this->input->post('display_id');
        $idrunning = $this->input->post('idrunning');
        $isi = $this->input->post('isi');
        $nourut = $this->input->post('nourut');
        $data['display_id'] = $display_id;
        $data['isi'] = $isi;
        $data['nourut'] = $nourut;
        $data['nourut'] = $nourut;

        if ('' === $idrunning) {
            $data['created_by'] = $this->session->userdata('user_id');
            $data['created_date'] = date('Y-m-d H:i:s');
            $hasil = $this->db->insert('antrian_poli_display_running_text', $data);
        } else {
            $data['edited_by'] = $this->session->userdata('user_id');
            $data['edited_date'] = date('Y-m-d H:i:s');
            $this->db->where('id', $idrunning);
            $hasil = $this->db->update('antrian_poli_display_running_text', $data);
        }

        json_encode($hasil);
    }

    public function hapus_running(): void
    {
        $id = $this->input->post('id');
        $data['status'] = 0;
        $data['deleted_by'] = $this->session->userdata('user_id');
        $data['deleted_date'] = date('Y-m-d H:i:s');

        $this->db->where('id', $id);
        $hasil = $this->db->update('antrian_poli_display_running_text', $data);

        json_encode($hasil);
    }

    public function load_running_text(): void
    {
        $display_id = $this->input->post('display_id');
        $from = "(SELECT H.id, H.nourut,H.isi FROM antrian_poli_display_running_text H WHERE H.display_id='{$display_id}' AND H.status='1' ORDER BY nourut) as tbl";

        $this->select = [];
        $this->from = $from;
        $this->join = [];
        $this->order = [];
        $this->group = [];
        $this->column_search = ['isi'];
        $this->column_order = [];

        $list = $this->datatable->get_datatables(true);
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $result = [];

            $result[] = $no;
            $result[] = $r->nourut;
            $result[] = $r->isi;

            $aksi = '<div class="btn-group">';
            $aksi .= '<button onclick="edit_running('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="si si-pencil"></i></button>';
            $aksi .= '<button onclick="hapus_running('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';
            $aksi .= '</div>';

            $result[] = $aksi;

            $data[] = $result;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(true),
            'recordsFiltered' => $this->datatable->count_all(true),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function edit_running(): void
    {
        $id = $this->input->post('id');
        $q = "SELECT * FROM antrian_poli_display_running_text H WHERE H.id='{$id}'";
        $data = $this->db->query($q)->row_array();
        echo json_encode($data);
    }

    public function save_sound(): void
    {
        if ($this->Antrian_poli_display_model->save_sound()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'sound telah disimpan.');
            redirect('antrian_poli_display/update/'.$this->input->post('display_id'), 'location');
        }
    }

    public function hapus_sound($id, $display_id): void
    {
        if ($this->Antrian_poli_display_model->hapus_sound($id)) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'sound telah disimpan.');
            redirect('antrian_poli_display/update/'.$display_id, 'location');
        }
    }

    public function upload_video(): void
    {
        $uploadDir = './assets/upload/video';
        if (!empty($_FILES)) {
            $display_id = $this->input->post('display_id');
            $tmpFile = $_FILES['file']['tmp_name'];
            $size = $_FILES['file']['size'];
            $filename = $uploadDir.'/'.time().'-'.$_FILES['file']['name'];
            $file_name = time().'-'.$_FILES['file']['name'];
            move_uploaded_file($tmpFile, $filename);

            $detail = [];
            $detail['display_id'] = $display_id;
            $detail['file_name'] = $file_name;
            $detail['size'] = formatSizeUnits($size);
            $detail['tanggal_upload'] = date('Y-m-d H:i:s');
            $detail['user_upload'] = $this->session->userdata('user_name');
            $this->db->insert('antrian_poli_display_video', $detail);
        }
    }

    public function refresh_video($id): void
    {
        $arr['detail'] = $this->Antrian_poli_display_model->refresh_video($id);
        $this->output->set_output(json_encode($arr));
    }

    public function hapus_file(): void
    {
        $id = $this->input->post('id');
        $row = $this->Antrian_poli_display_model->get_file_name($id);
        if (file_exists('./assets/upload/video/'.$row->file_name) && '' !== $row->file_name) {
            unlink('./assets/upload/video/'.$row->file_name);
        }

        $result = $this->db->query("delete from antrian_poli_display_video WHERE id='{$id}'");
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }

    public function load_tujuan(): void
    {
        $display_id = $this->input->post('display_id');
        $from = "(
					SELECT H.id, H.nourut,H.tujuan_id,M.nama_tujuan
					FROM antrian_poli_display_tujuan H
					LEFT JOIN mtujuan M ON M.id=H.tujuan_id
					WHERE H.display_id='{$display_id}' AND H.status='1' ORDER BY nourut
				) as tbl";

        $this->select = [];
        $this->from = $from;
        $this->join = [];
        $this->order = [];
        $this->group = [];
        $this->column_search = ['tujuan_id'];
        $this->column_order = [];

        $list = $this->datatable->get_datatables(true);
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $result = [];

            $result[] = $no;
            $result[] = $r->nourut;
            $result[] = $r->nama_tujuan;

            $aksi = '<div class="btn-group">';
            $aksi .= '<button onclick="edit_tujuan('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="si si-pencil"></i></button>';
            $aksi .= '<button onclick="hapus_tujuan('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';
            $aksi .= '</div>';

            $result[] = $aksi;

            $data[] = $result;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(true),
            'recordsFiltered' => $this->datatable->count_all(true),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function simpan_tujuan(): void
    {
        $display_id = $this->input->post('display_id');
        $idtujuan = $this->input->post('idtujuan');
        $tujuan_id = $this->input->post('tujuan_id');
        $nourut = $this->input->post('nourut_tujuan');
        $data['display_id'] = $display_id;
        $data['tujuan_id'] = $tujuan_id;
        $data['nourut'] = $nourut;
        if ('' === $idtujuan) {
            $data['created_by'] = $this->session->userdata('user_id');
            $data['created_date'] = date('Y-m-d H:i:s');
            $hasil = $this->db->insert('antrian_poli_display_tujuan', $data);
        } else {
            $data['edited_by'] = $this->session->userdata('user_id');
            $data['edited_date'] = date('Y-m-d H:i:s');
            $this->db->where('id', $idtujuan);
            $hasil = $this->db->update('antrian_poli_display_tujuan', $data);
        }

        json_encode($hasil);
    }

    public function hapus_tujuan(): void
    {
        $id = $this->input->post('id');
        $data['status'] = 0;
        $data['deleted_by'] = $this->session->userdata('user_id');
        $data['deleted_date'] = date('Y-m-d H:i:s');

        $this->db->where('id', $id);
        $hasil = $this->db->update('antrian_poli_display_tujuan', $data);
        json_encode($hasil);
    }

    public function edit_tujuan(): void
    {
        $id = $this->input->post('id');
        $q = "SELECT * FROM antrian_poli_display_tujuan H WHERE H.id='{$id}'";
        $data = $this->db->query($q)->row_array();
        echo json_encode($data);
    }

    public function setting_khusus($id): void
    {
        $data = $this->Antrian_poli_display_model->get_index_setting($id);

        $data['error'] = '';
        $data['title'] = 'Setting Display TV (Khusus)';
        $data['content'] = 'Antrian_poli_display/manage_khusus';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Antrian Setting', '#'],
            ['Index Display', 'antrian_poli_display'],
        ];

        $data['list_tujuan'] = $this->Antrian_poli_display_model->list_tujuan_khusus();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function load_tujuan_khusus(): void
    {
        $display_id = $this->input->post('display_id');
        $from = "(
            SELECT
                antrian_poli_display_tujuan_khusus.*, mtujuan.nama_tujuan
            FROM
                antrian_poli_display_tujuan_khusus
            LEFT JOIN mtujuan ON mtujuan.id = antrian_poli_display_tujuan_khusus.tujuan_id
            WHERE
                antrian_poli_display_tujuan_khusus.display_id='{$display_id}' AND
                antrian_poli_display_tujuan_khusus.status='1'
            ORDER BY nourut
        ) AS tbl";

        $this->select = [];
        $this->from = $from;
        $this->join = [];
        $this->order = [];
        $this->group = [];
        $this->column_search = ['tujuan_id'];
        $this->column_order = [];

        $list = $this->datatable->get_datatables(true);
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $result = [];

            $result[] = $no;
            $result[] = $r->nourut;
            $result[] = $r->nama_tujuan;

            $aksi = '<div class="btn-group">';
            $aksi .= '<button onclick="editTujuanKhusus('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="si si-pencil"></i></button>';
            $aksi .= '<button onclick="hapusTujuanKhusus('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';
            $aksi .= '<button data-toggle="modal" data-target="#modalSetting" data-id="'.$r->id.'" data-tujuan="'.$r->nama_tujuan.'" data-nomor-antrian="'.$r->tampil_nomor_antrian.'" data-nomor-medrec="'.$r->tampil_nomor_medrec.'" data-nama-pasien="'.$r->tampil_nama_pasien.'" data-nomor-daftar="'.$r->tampil_nomor_daftar.'" type="button" title="Setting" class="btn btn-success btn-xs setting"><i class="fa fa-gear"></i></button>';
            $aksi .= '</div>';

            $result[] = $aksi;

            $data[] = $result;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(true),
            'recordsFiltered' => $this->datatable->count_all(true),
            'data' => $data,
        ];

        echo json_encode($output);
    }

    public function simpan_display_khusus(): void
    {
        $display_id = $this->input->post('display_id');
        $bg_color = $this->input->post('bg_color');

        $data['bg_color_khusus'] = $bg_color;

        $this->db->where('id', $display_id);
        $hasil = $this->db->update('antrian_poli_display', $data);

        json_encode($hasil);
    }

    public function simpan_tujuan_khusus(): void
    {
        $id = $this->input->post('id');
        $display_id = $this->input->post('display_id');
        $nomor_urut = $this->input->post('nomor_urut');
        $tujuan_id = $this->input->post('tujuan_id');

        $data['display_id'] = $display_id;
        $data['tujuan_id'] = $tujuan_id;
        $data['nourut'] = $nomor_urut;

        if ('' === $id) {
            $data['created_by'] = $this->session->userdata('user_id');
            $data['created_date'] = date('Y-m-d H:i:s');
            $hasil = $this->db->insert('antrian_poli_display_tujuan_khusus', $data);
        } else {
            $data['edited_by'] = $this->session->userdata('user_id');
            $data['edited_date'] = date('Y-m-d H:i:s');
            $this->db->where('id', $id);
            $hasil = $this->db->update('antrian_poli_display_tujuan_khusus', $data);
        }

        json_encode($hasil);
    }

    public function simpan_tujuan_khusus_setting(): void
    {
        $id = $this->input->post('id');

        $tampil_nomor_antrian = $this->input->post('tampil_nomor_antrian');
        $tampil_nomor_medrec = $this->input->post('tampil_nomor_medrec');
        $tampil_nama_pasien = $this->input->post('tampil_nama_pasien');
        $tampil_nomor_daftar = $this->input->post('tampil_nomor_daftar');

        $data['tampil_nomor_antrian'] = $tampil_nomor_antrian;
        $data['tampil_nomor_medrec'] = $tampil_nomor_medrec;
        $data['tampil_nama_pasien'] = $tampil_nama_pasien;
        $data['tampil_nomor_daftar'] = $tampil_nomor_daftar;
        $data['edited_by'] = $this->session->userdata('user_id');
        $data['edited_date'] = date('Y-m-d H:i:s');

        $this->db->where('id', $id);
        $hasil = $this->db->update('antrian_poli_display_tujuan_khusus', $data);

        json_encode($hasil);
    }

    public function hapus_tujuan_khusus(): void
    {
        $id = $this->input->post('id');

        $data['status'] = 0;
        $data['deleted_by'] = $this->session->userdata('user_id');
        $data['deleted_date'] = date('Y-m-d H:i:s');

        $this->db->where('id', $id);
        $hasil = $this->db->update('antrian_poli_display_tujuan_khusus', $data);

        json_encode($hasil);
    }

    public function edit_tujuan_khusus(): void
    {
        $id = $this->input->post('id');
        
        $q = "SELECT *
            FROM antrian_poli_display_tujuan_khusus
            WHERE
                antrian_poli_display_tujuan_khusus.id = '{$id}'";
        $data = $this->db->query($q)->row_array();

        echo json_encode($data);
    }
}
