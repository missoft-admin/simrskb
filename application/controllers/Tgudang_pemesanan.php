
<?php defined('BASEPATH') OR exit('No direct script access allowed');
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tgudang_pemesanan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('tgudang_pemesanan_model','model');
	}

	function index() {
		$data = array(
            'filter_distributor' => '#',
            'statuspesan' => '#',
            'tanggaldari' => date('d-m-Y'),
            'tanggalsampai' => date('d-m-Y'),
            'no_trx' => '',
        );
		// $userid='1';
			// $tipe_gudang=$this->model->get_user_tipe($userid);
			// print_r($tipe_gudang);exit();
		$this->session->set_userdata($data);
		$data['error'] 			= '';
		$data['list_distributor'] 			= $this->model->list_distributor();
		$data['title'] 			= 'Pemesanan Gudang';
		$data['content'] 		= 'Tgudang_pemesanan/index';
		// $data['tanggaldari'] 		= date('d-m-Y');
		// $data['tanggalsampai'] 		= date('d-m-Y');
		$data['breadcrum'] 	= array(array("RSKB Halmahera",'#'),
							  array("tgudang_pemesanan",'#'),
							  array("List",'tgudang_pemesanan'));
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	public function add() {
        $data['error']      = '';
        $data['statusstok']      = '#';
        $data['title']      = 'Tambah Pemesanan';
        $data['content']    = 'Tgudang_pemesanan/manage_pesan';
        $data['list_tipe']    = $this->model->list_tipe();
		
        // $data['unitpelayanandef']    = $this->model->getDefaultUnitPelayananUser();
        // $data['list_unit_pelayanan_user']    = $this->model->list_unit_pelayanan_user();
		// $data['list_tipe_barang']    = $this->model->list_tipe_barang($data['unitpelayanandef']);
		// $data['list_unit_ke']    = $this->model->list_unit_ke($data['unitpelayanandef']);
        // $data['list_user']    = $this->model->get_user();
		// print_r( $data['list_unit_pelayanan_user'] );exit();
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Pemesanan Barang", '#'),
            array("List", 'Tgudang_pemesanan')
        );
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	// public function selectbarang() {
        // $this->model->selectbarang();
    // }
	public function filter() {
		// print_r($this->input->post('tanggaldari'));exit();
        $data = array(
            'filter_distributor' => $this->input->post('filter_distributor'),
            'statuspesan' => $this->input->post('statuspesan'),
            'tanggaldari' => $this->input->post('tanggaldari'),
            'tanggalsampai' => $this->input->post('tanggalsampai'),
            'no_trx' => $this->input->post('no_trx'),
        );
		$this->session->set_userdata($data);
		$data['list_distributor'] 			= $this->model->list_distributor();
        
        $data['error'] = '';
        $data['title'] = 'Pemesanan Gudang';
        $data['content'] = 'Tgudang_pemesanan/index';
        $data['breadcrum'] 	= array(array("RSKB Halmahera",'#'),
							  array("tgudang_pemesanan",'#'),
							  array("Filter",'tgudang_pemesanan'));
		// print_r($data);exit();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	public function getsorted_distributor_2($idtipe='3',$idbarang='1'){
		$this->model->getsorted_distributor_2($idtipe,$idbarang);
	}
	public function cek_distibutor(){
		$this->model->cek_distibutor();
	}
	function view() {
		$id = $this->uri->segment(3);
		if($id) {
			$data 					= $this->model->viewHead($id);
			$data['error'] 			= '';
			$data['title'] 			= 'Detail Pemesanan Gudang';
			$data['content'] 		= 'Tgudang_pemesanan/view';
			$data['breadcrum'] 	= array(array("RSKB Halmahera",'#'),
								  array("Pemesanan Gudang",'tgudang_pemesanan'),
								  array("View",'#'));
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);			
		} else {
			show_404();
		}
	}
	public function selected_barang_harga() {
        $this->model->selected_barang_harga();
    }
	public function selectbarang_array_tipe() {
        $this->model->selectbarang_array_tipe();
    }
	public function view_barang() {
        $this->model->view_barang();
    }
	public function selectbarang_all() {
        $this->model->selectbarang_all();
    }
	public function get_harga() {
        $this->model->get_harga();
    }
	public function selectbarang() {
        $this->model->selectbarang();
    }
	function create() {
		$data = array(
			'id' 					=> '',
			'nopemesanan' 			=> '',
			'iddistributor' 		=> '',
			'totalbarang' 			=> '',
			'totalharga' 			=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Pemesanan Gudang';
		$data['content'] 		= 'Tgudang_pemesanan/manage';
		$data['breadcrum']		= array(array("RSKB Halmahera",'#'),
							  		array("Pemesanan Gudang",'#'),
							  		array("Create",''));
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function save(){
		$this->model->save();
	}
	function save_pesan(){
		if ($this->model->save_pesan()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tgudang_pemesanan');
			
		}else{
			$_SESSION['status']='Toastr("Data gagal disimpan","Info")';
			redirect('tgudang_pemesanan');
		}
	}
	// public function getBarang_cari() {
        // $this->model->getBarang_cari();
    // }
	function getBarang_cari()
    {
		$idtipe     = $this->input->post('idtipe');
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		     
		$from="(SELECT B.id,B.idtipe,B.kode,B.namatipe,B.nama,CASE WHEN B.idtipe='4' THEN SL.stok ELSE SO.stok END as stok,B.hargabeli as  harga,M.nama as satuan from view_barang B
				LEFT JOIN mgudang_stok SO ON SO.idbarang=B.id AND SO.idtipe=B.idtipe AND SO.idunitpelayanan='0'
				LEFT JOIN mgudang_stok SL ON SL.idbarang=B.id AND SL.idtipe=B.idtipe AND SL.idunitpelayanan='49'
				LEFT JOIN msatuan M ON M.id=B.idsatuan
				WHERE B.idtipe IN (1,2,3,4)) as tbl";
			// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('namatipe','kode','nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
			$row[] = $r->id;			
			$row[] = $r->idtipe;			
            $row[] = $r->namatipe;
            $row[] = $r->kode;
            $row[] = $r->nama;
            $row[] = ($r->stok?$r->stok:0);
            $row[] = $r->satuan;
            
			$status="";
			$aksi       = '<div class="btn-group">';
			$aksi 		.= '<button class="btn btn-xs btn-success pilih" title="Pilih"><i class="fa fa-check"></i> Pilih</button>';			
			$aksi.='</div>';
			$row[] = $aksi;			
						
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	public function save_edit(){
		if ($this->model->save_edit()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
            redirect('tgudang_pemesanan/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_pemesanan/index', 'refresh');
        }
		// print_r($this->input->post());exit();
	}
	public function save_edit_pemesanan(){
		if ($this->model->save_edit_pemesanan()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
            redirect('tgudang_penerimaan/create2', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_penerimaan/create2', 'refresh');
        }
		// print_r($this->input->post());exit();
	}
	public function save_edit_po(){
		if ($this->model->save_edit_po()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
            redirect('tgudang_penerimaan/create2', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_penerimaan/create2', 'refresh');
        }
		// print_r($this->input->post());exit();
	}
	public function konfirmasi($id){
		if ($this->model->konfirmasi($id)) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
            redirect('tgudang_pemesanan/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_pemesanan/index', 'refresh');
        }
		// print_r($this->input->post());exit();
	}
	function update() {
		$this->model->update();
	}

    function ajax_list(){
        $userid = $this->session->userdata('user_id');
        $from = "(SELECT
        tgudang_pemesanan.*,
        mdistributor.nama
        FROM tgudang_pemesanan
        LEFT JOIN mdistributor ON tgudang_pemesanan.iddistributor = mdistributor.id
        WHERE tgudang_pemesanan.stdraft != 2
        AND tipepemesanan IN (SELECT gudangtipe FROM musers_tipegudang WHERE iduser = ".$userid.") ) as tbl";
        $this->load->library('datatables');
        $this->datatables->add_column('action', '', 'id');
        $this->datatables->from($from);
        return print_r($this->datatables->generate());
    }
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		$filter_distributor = $this->session->userdata('filter_distributor');
        $statuspesan = $this->session->userdata('statuspesan');
        $tanggaldari = $this->session->userdata('tanggaldari');
        $tanggalsampai = $this->session->userdata('tanggalsampai');
        $no_trx = $this->session->userdata('no_trx');
		$tipe_gudang=$this->model->get_user_tipe($userid);
		$where='';
		if ($statuspesan!="#") {
			if ($statuspesan=='10'){				
				$where.=" AND H.stdraft='1' AND H.status='1'";	
			}elseif($statuspesan=='1'){
				$where.=" AND H.stdraft='0' AND H.status='1'";		
			}else{
				$where.=" AND H.status='".$statuspesan."'";
			}
        }
		if ($no_trx!="") {					
			$where.=" AND H.nopemesanan LIKE '%".$no_trx."%'";			
        }
		if ($filter_distributor!="#") {					
			$where.=" AND H.iddistributor='$filter_distributor'";			
        }
		if ('' != $tanggaldari) {
            $where .= " AND DATE(H.tanggal) >='".YMDFormat($tanggaldari)."'";
        }
        if ('' != $tanggalsampai) {
            $where .= " AND DATE(H.tanggal) <='".YMDFormat($tanggalsampai)."'";
        }
        $from = "(SELECT H.id,H.tanggal,H.st_batal_by,CASE WHEN H.tipepemesanan='1' THEN 'NON LOGISTIK' ELSE 'LOGISTIK' END as tipe_pemesanan,H.tipepemesanan,
				H.nopemesanan,H2.nopemesanan as nopemesanan_asal,H.stdraft,H.`status`,COUNT(D.id) as item,H.iddistributor,M.nama as nama_distributor,H.tipe_bayar,H.st_approval
				FROM tgudang_pemesanan H 
				LEFT JOIN tgudang_pemesanan H2 ON H2.id=H.id_asal
				LEFT JOIN tgudang_pemesanan_detail D ON H.id=D.idpemesanan AND D.status='1'
				LEFT JOIN mdistributor M ON M.id=H.iddistributor
				WHERE H.tipepemesanan IN (".$tipe_gudang.") ".$where."
				GROUP BY H.id
				ORDER BY H.id DESC ) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array('id' => 'desc');
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_setting     = '';
            $url        = site_url('tgudang_pemesanan/');
			$cara_bayar='';
			if ($r->tipe_bayar=='1'){
				$cara_bayar=text_success('TUNAI');
				
			}else{
				$cara_bayar=text_primary('KREDIT');
			}
			if ($r->status=='2'){
				$status_setting='<button title="Lihat User" class="btn btn-danger btn-xs user" onclick="load_user('.$r->id.')"><i class="si si-user"></i></button>';
			}
			if ($r->status=='1' && $r->st_approval=='2'){
				$status_setting='<button title="Lihat User" class="btn btn-danger btn-xs user" onclick="load_user('.$r->id.')"><i class="si si-user"></i> DITOLAK</button>';
			}

            $row[] = $r->id;
            $row[] = $no;
            $row[] = ($r->tipepemesanan=='1'?'<label class="label label-default">'.$r->nopemesanan.'</label>':'<label class="label label-default">'.$r->nopemesanan.'</label>').' '.($r->nopemesanan_asal?'<label class="label label-danger">'.$r->nopemesanan_asal.'</label>':'');
            $row[] = $r->tanggal;
            $row[] = $r->tipe_pemesanan;
            $row[] = $r->nama_distributor.' <br>'.$cara_bayar;
            $row[] = $r->item;
			if ($r->stdraft=='1'){
				
				// $aksi .= '<a class="view btn btn-xs btn-info" type="button" data-target="#modalDetailPemesanan" data-toggle="modal" title="Detail"><i class="fa fa-eye"></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-info" href="'.$url.'detail/'.$r->id.'" type="button"  title="Detail"><i class="fa fa-eye"></i></a>';
				if ($r->status=='0'){
					$status='<span class="label label-danger">DIBATALKAN</span>';
				}else{
					$status='<span class="label label-info">DRAFT</span>';
					if (UserAccesForm($user_acces_form,array('1370'))){//Edit
					$aksi .= '<a href="'.$url.'edit_draft/'.$r->id.'" type="button" title="Edit Draft" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>';
					}
					if (UserAccesForm($user_acces_form,array('1119'))){
					$aksi .= '<a href="'.$url.'konfirmasi_pemesanan_draft/'.$r->id.'" type="button" title="Konversi" class="btn btn-xs btn-warning"><i class="fa fa-hand-o-left"></i></a>';
					}
					if (UserAccesForm($user_acces_form,array('1123'))){
					$aksi .= '<a href="'.$url.'print_data/'.$r->id.'" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-print"></i></a>';
					}
					if (UserAccesForm($user_acces_form,array('1124'))){
					$aksi .= '<a class="confirmCancel btn btn-xs btn-danger" type="button" title="Batalkan"><i class="fa fa-times"></i></a>';
					}
				}
			}else{
				$aksi .= '<a class="view btn btn-xs btn-info" href="'.$url.'detail/'.$r->id.'" type="button"  title="Detail"><i class="fa fa-eye"></i></a>';
				if ($r->status=='0'){
					if ($r->st_batal_by=='3'){
						$status='<span class="label label-danger">DIBATALKAN KEUANGAN</span>';
					}else{
						$status='<span class="label label-danger">DIBATALKAN</span>';
						
					}
				}elseif($r->status=='1'){
					$status=status_pesan_gudang($r->status);
					if (UserAccesForm($user_acces_form,array('1121'))){
					$aksi .= '<a href="'.$url.'edit/'.$r->id.'" type="button" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>';
					}
					if (UserAccesForm($user_acces_form,array('1122'))){
					// $aksi .= '<a href="'.$url.'konfirmasi/'.$r->id.'" type="button" title="Konfirmasi Kepala Installasi" class="btn btn-xs btn-success"><i class="fa fa-check"></i></a>';
					$aksi .= '<button type="button" title="Konfirmasi Kepala Installasi" class="btn btn-xs btn-success" onclick="kirim('.$r->id.')"><i class="fa fa-check"></i></button>';
					}
					if (UserAccesForm($user_acces_form,array('1123'))){
					$aksi .= '<a href="'.$url.'print_data/'.$r->id.'" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-print"></i></a>';
					}
					if (UserAccesForm($user_acces_form,array('1124'))){
					$aksi .= '<a class="confirmCancel btn btn-xs btn-danger" type="button" title="Batalkan"><i class="fa fa-times"></i></a>';
					}
				}elseif($r->status=='2'){
					if (UserAccesForm($user_acces_form,array('1123'))){
					$aksi .= '<a href="'.$url.'print_data/'.$r->id.'" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-print"></i></a>';
					}
					$status=status_pesan_gudang($r->status);
				
				}else{
					if (UserAccesForm($user_acces_form,array('1123'))){
					$aksi .= '<a href="'.$url.'print_data/'.$r->id.'" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-print"></i></a>';
					}
					$status=status_pesan_gudang($r->status);
				}
			}
            $row[] = $status.' '.$status_setting;
            // $row[] = $r->status;
			// $row[] = $st_kirim;
            // $row[] = $r->user_dariunit;
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }

    function acc($id) {
        if($id) $this->db->update('tgudang_pemesanan', array('status' => 2), array('id' => $id) );
    }

    function cancel($id) {
        if($id) $this->db->update('tgudang_pemesanan', array('status' => 0,'iduser_batal' => $this->session->userdata("user_id"),'tgl_batal' => date('Y-m-d H:i:s')), array('id' => $id) );
    }   


    function get_list_barang($id){
        if($id) {
            $table  = array(null,'mdata_alkes','mdata_implan','mdata_obat','mdata_logistik');

            $this->load->library('datatables');
            if($id == 2 || $id == 4) {
            	$this->datatables->select('id,kode,nama,IF(hargabeli IS NULL,0,hargabeli) as hargabeli,catatan');
	            $this->datatables->add_column('satuan','-');
            } else {            	
	            $this->datatables->select('a.id,a.kode,a.nama,IF(a.hargabeli IS NULL,0,a.hargabeli) as hargabeli,b.nama as satuan,catatan');
	            $this->datatables->join('msatuan b', 'a.idsatuanbesar = b.id', 'left');
            }
            $this->datatables->from($table[$id] . ' as a');
            return print_r($this->datatables->generate());
        }
    }

    function get_distributor() {
        $query = get_all('mdistributor', array('status' => 1) );
        $data = "<option value=''>Pilih Opsi</option>";
        if($query) {
            foreach ($query as $r) {
                $data .= "<option value='".$r->id."'>".$r->nama."</option>";
            }
        }
        echo $data;
    }

    function get_barang_detail() {
    	$kode 		= $this->input->post('kodebarang');
    	$tipe 		= $this->input->post('idtipe');
		$table 		= array(null, 'mdata_alkes', 'mdata_implan', 'mdata_obat', 'mdata_logistik');
		$barang 	= get_by_field('kode', $kode, $table[ $tipe ] );
		$data 		= array();

		$data['id']				= $barang->id;
		if( $barang->hargabeli == null) {
			$data['hargabeli']		= 0;
		} else {
			$data['hargabeli']		= $barang->hargabeli;
		}

		$this->output->set_output(json_encode($data));
    }

    function viewDetail() {
    	$idPemesanan = $this->input->post('idpemesanan');
    	if($idPemesanan) {
    		if($this->model->viewDetail($idPemesanan)) {
	    		$result = $this->model->viewDetail($idPemesanan);
    		} else {
	    		$result = array();
    		}
    	} else {
    		$result = array();
    	}
    	$data = array('data' => $result);
    	$this->output->set_output(json_encode($data));
    }

    function getSatuanBarang() {
    	$idtipe = $this->input->post('idtipe');
    	$idbarang = $this->input->post('idbarang');
    	if($idtipe && $idbarang) {
    		$result = $this->model->getSatuanBarang($idtipe,$idbarang);
    	} else {
    		$result = '-';
    	}
    	echo $result;
    }

    function getAnyQty() {
    	$idpemesanan = $this->input->post('idpemesanan');
    	$idtipe = $this->input->post('idtipe');
    	$idbarang = $this->input->post('idbarang');
    	if($idtipe && $idbarang && $idpemesanan) {
    		$result = $this->model->getAnyQty($idtipe,$idbarang,$idpemesanan);
    	} else {
    		$result = array();
    	}
    	$this->output->set_output(json_encode($result));
    }

    function getJmlPesanTerima() {
    	$idpemesanan = $this->input->post('idpemesanan');
    	if($idpemesanan) {
    		if($this->model->getJmlPesanTerima($idpemesanan)) {
	    		$result = $this->model->getJmlPesanTerima($idpemesanan);
    		} else {
    			$result = array();
    		}
    	} else {
    		$result = array();
    	}
    	$this->output->set_output(json_encode($result));
    }

    function editPemesanan() {
    	$idPemesanan = $this->input->post('idpemesanan');
    	if($idPemesanan) {
    		$result = $this->model->editPemesanan($idPemesanan);
    	} else {
    		$result = array();
    	}
    	$data = array('data' => $result);
    	$this->output->set_output(json_encode($data));
    }

    function getNamaBarang() {
    	$idtipe = $this->input->post('idtipe');
    	$idbarang = $this->input->post('idbarang');
    	if($idtipe && $idbarang) {
    		$result = $this->model->getNamaBarang($idtipe, $idbarang);
    	} else {
    		$result = '-';
    	}
    	echo $result;
    }

    function getMaxDistributor() {
        $idtipe = $this->input->post('idtipe');
        $idbarang = $this->input->post('idbarang');
        if($idtipe && $idbarang) {
            $result = $this->model->getMaxDistributor($idtipe, $idbarang);
        } else {
            $result = '';
        }
        echo $result;
    }

    function getStokGudang() {
        $idtipe = $this->input->post('idtipe');
        $idbarang = $this->input->post('idbarang');
        if($idtipe && $idbarang) {
            $result = $this->model->getStokGudang($idtipe, $idbarang);
        } else {
            $result = '0';
        }
        echo $result;
    }

    public function get_satuan_barang() {
        $this->model->get_satuan_barang();
    }
    public function get_harga_beli() {
        $opsisatuan = $this->input->post('opsisatuan');
        $idbarang = $this->input->post('idbarang');
        $idtipe = $this->input->post('idtipe');
        if($idbarang && $idtipe) {
            $row = $this->model->get_detailbarang($idtipe,$idbarang);
            if($opsisatuan == 2) {
                $harga = $row->hargasatuanbesar;
            } else {
                $harga = $row->hargabeli;
            }
            $this->output->set_output(json_encode($harga));
        }
    }

    public function print_po() {
        $id = $this->uri->segment(3);
        if($id) {
            $data                   = $this->model->print_po_head($id);
            $data['detail']         = $this->model->print_po_detail($data['id']);
            $data['error']          = '';
            $data['title']          = 'Print Pemesanan';
            $data = array_merge($data, backend_info());
            $this->parser->parse('Tgudang_pemesanan/print_po', $data);        
        }
    }
	public function print_data($id,$tipe='0')
    {
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();

        $data = $this->db->where('id',$id)->get('tgudang_pemesanan')->row_array();
		$q="
			SELECT mppa.id as mppa_id FROM `tgudang_pemesanan` H
			LEFT JOIN musers M ON M.`name`=H.userpemesanan
			LEFT JOIN mppa ON mppa.user_id=M.id 
			WHERE H.id='".$data['id']."'
		";
		$data['mppa_id']=$this->db->query($q)->row('mppa_id');
        $data['namadistributor'] = get_by_field('id',$data['iddistributor'],'mdistributor')->nama;
        $data['tipe']          = $tipe;
        $data['error']          = '';
        $data['list_detail']    = $this->model->list_pesanan_edit($id);
		$data=array_merge($data,backend_info());
        $html = $this->load->view('Tgudang_pemesanan/cetak_nota', $data, true);
        $html = $this->parser->parse_string($html,$data);
		// print_r($data);exit();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Rincian Global.pdf', array("Attachment"=>0));
    }
	public function print_data_bukti_pemesanan($id,$idterima)
    {
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();

        $data = $this->db->where('id',$id)->get('tgudang_pemesanan')->row_array();
		$q="
			SELECT mppa.id as mppa_id FROM `tgudang_pemesanan` H
			LEFT JOIN musers M ON M.`name`=H.userpemesanan
			LEFT JOIN mppa ON mppa.user_id=M.id 
			WHERE H.id='".$data['id']."'
		";
		$data['mppa_id']=$this->db->query($q)->row('mppa_id');
        $data['namadistributor'] = get_by_field('id',$data['iddistributor'],'mdistributor')->nama;
        $data['error']          = '';
        $data['list_detail']    = $this->model->list_pesanan_edit($id);
		$data=array_merge($data,backend_info());
        $html = $this->load->view('Tgudang_pemesanan/cetak_nota_bukti', $data, true);
        $html = $this->parser->parse_string($html,$data);
		// print_r($data);exit();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Rincian Global.pdf', array("Attachment"=>0));
    }
    public function konfirmasi_pemesanan_draft($id) {
        // $id = $this->uri->segment(3);
        $data = $this->db->where('id',$id)->get('tgudang_pemesanan')->row_array();
		
        $data['namadistributor'] = get_by_field('id',$data['iddistributor'],'mdistributor')->nama;
        $data['error']          = '';
        $data['list_detail']    = $this->model->list_pesanan_draft($id);
        $data['title']          = 'Konfirmasi Pemesanan Draft';
        $data['content']        = 'Tgudang_pemesanan/konfirmasi_pemesanan_draft';
        $data['breadcrum']      = array(array("RSKB Halmahera",'#'),
                                    array("Pemesanan Gudang",'#'),
                                    array("Konfirmasi Pemesanan Draft",''));
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);        
    }
	public function edit($id) {
        // $id = $this->uri->segment(3);
        $data = $this->db->where('id',$id)->get('tgudang_pemesanan')->row_array();
		// print_r($data);exit();
		// $xidunit
        $data['namadistributor'] = get_by_field('id',$data['iddistributor'],'mdistributor')->nama;
        $data['error']          = '';
		if ($data['tipepemesanan']=='1'){
			$xidunit="0";
		}else{
			$xidunit="49";
			
		}
        $data['xidunit']    = $xidunit;
        $data['list_detail']    = $this->model->list_pesanan_edit($id);
        $data['get_list_tipe']    = $this->model->get_list_tipe($xidunit);
		// print_r($data['get_list_tipe']);exit();
		// print_r($data['list_detail']);exit();
        $data['title']          = 'Edit Pemesanan';
        $data['content']        = 'Tgudang_pemesanan/edit';
        $data['breadcrum']      = array(array("RSKB Halmahera",'#'),
                                    array("Pemesanan Gudang",'#'),
                                    array("Edit Pemesanan",''));
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);        
    }
	public function edit_penerimaan($id) {
        // $id = $this->uri->segment(3);
        $data = $this->db->where('id',$id)->get('tgudang_pemesanan')->row_array();
		// print_r($data);exit();
		// $xidunit
        $data['namadistributor'] = get_by_field('id',$data['iddistributor'],'mdistributor')->nama;
        $data['error']          = '';
		if ($data['tipepemesanan']=='1'){
			$xidunit="0";
		}else{
			$xidunit="49";
			
		}
        $data['xidunit']    = $xidunit;
        $data['list_detail']    = $this->model->list_pesanan_edit_penerimaan($id);
        $data['get_list_tipe']    = $this->model->get_list_tipe($xidunit);
		// print_r($data['get_list_tipe']);exit();
		// print_r($data['list_detail']);exit();
        $data['title']          = 'Edit Pemesanan dari Penerimaan';
        $data['content']        = 'Tgudang_pemesanan/edit_penerimaan';
        $data['breadcrum']      = array(array("RSKB Halmahera",'#'),
                                    array("Pemesanan Gudang",'#'),
                                    array("Edit Pemesanan",''));
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);        
    }
	public function edit_po($id) {
        // $id = $this->uri->segment(3);
        $data = $this->db->where('id',$id)->get('tgudang_pemesanan')->row_array();
		// print_r($data);exit();
		// $xidunit
		$nama=get_by_field('id',$data['iddistributor'],'mdistributor')->nama;
        $data['namadistributor'] = ($nama?$nama:'');
        $data['error']          = '';
		if ($data['tipepemesanan']=='1'){
			$xidunit="0";
		}else{
			$xidunit="49";
			
		}
        $data['xidunit']    = $xidunit;
        $data['list_detail']    = $this->model->list_pesanan_edit_po($id);
        $data['get_list_tipe']    = $this->model->get_list_tipe($xidunit);
		// print_r($data['get_list_tipe']);exit();
		// print_r($data['list_detail']);exit();
        $data['title']          = 'Gabung Ke Pemesanan [po] Lain';
        $data['content']        = 'Tgudang_pemesanan/edit_po';
        $data['breadcrum']      = array(array("RSKB Halmahera",'#'),
                                    array("Pemesanan Gudang",'#'),
                                    array("Edit Pemesanan",''));
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);        
    }
	public function edit_draft($id) {
         // $id = $this->uri->segment(3);
        $data = $this->db->where('id',$id)->get('tgudang_pemesanan')->row_array();
		// print_r($data);exit();
		// $xidunit
        $data['namadistributor'] = get_by_field('id',$data['iddistributor'],'mdistributor')->nama;
        $data['error']          = '';
		if ($data['tipepemesanan']=='1'){
			$xidunit="0";
		}else{
			$xidunit="49";
			
		}
        $data['xidunit']    = $xidunit;
        $data['list_detail']    = $this->model->list_pesanan_edit($id);
        $data['get_list_tipe']    = $this->model->get_list_tipe($xidunit);
		// print_r($data['get_list_tipe']);exit();
		// print_r($data['list_detail']);exit();
        $data['title']          = 'Edit Pemesanan [DRAFT]';
        $data['content']        = 'Tgudang_pemesanan/edit';
        $data['breadcrum']      = array(array("RSKB Halmahera",'#'),
                                    array("Pemesanan Gudang",'#'),
                                    array("Edit Pemesanan",''));
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);    
    }
	public function detail($id) {
        $data = $this->db->where('id',$id)->get('tgudang_pemesanan')->row_array();
        $data['namadistributor'] = get_by_field('id',$data['iddistributor'],'mdistributor')->nama;
        $data['error']          = '';
        $data['list_detail']    = $this->model->list_pesanan_edit($id);
        $data['title']          = 'Detail Pemesanan';
        $data['content']        = 'Tgudang_pemesanan/detail';
        $data['breadcrum']      = array(array("RSKB Halmahera",'#'),
                                    array("Pemesanan Gudang",'#'),
                                    array("Detail Pemesanan",''));
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);        
    }

    public function save_konfirmasi_pemesanan_draft2() {
        
		if($this->model->save_konfirmasi_pemesanan_draft2()) {
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
			redirect('tgudang_pemesanan/index','refresh');
		}else{
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
			redirect('tgudang_pemesanan/index','refresh');
		}
       
    }
	public function save_konfirmasi_pemesanan_draft() {
        $this->form_validation->set_rules('detailvalue', 'DataTable', 'trim|required|min_length[10]');
        if($this->form_validation->run() == true) {
            if($this->model->save_konfirmasi_pemesanan_draft()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
                redirect('tgudang_pemesanan/index','refresh');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_pemesanan/index','refresh');
        }
    }

    public function detailbarangdraft() {
        $this->model->detailbarangdraft();
    }

    public function testing() {
        $var1 = 25/100;

        $qty = 0;
        if($var1 < 1) {
            $qty = 1;
        } else {
            $qty = ceil($var1);
        }        
    }

    public function getdistributor() {
        $this->model->getdistributor();
    }
	public function view_unit() {
        $this->model->view_unit();
    }
    public function select_tipe() {
        $data = array();
        $this->db->select('gudangtipe');
        $this->db->where('iduser', $this->session->userdata('user_id'));
        $get = $this->db->get('musers_tipegudang');
        $row = $get->row_array();
        if($get->num_rows() == 1) {
            if($row['gudangtipe'] == 1) {
                $data[1] = 'Alkes';
                $data[2] = 'Implant';
                $data[3] = 'Obat';
            } else {
                $data[4] = 'Logistik';
            }
        } else {
            $data[1] = 'Alkes';
            $data[2] = 'Implant';
            $data[3] = 'Obat';            
            $data[4] = 'Logistik';            
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }
	function list_user($id){
		$q="SELECT *FROM tgudang_pemesanan_approval H WHERE H.idPemesanan='$id'";
		$row=$this->db->query($q)->result();
		$content='';
		$no=1;
		$no_step="1";
		foreach($row as $r){
			$st_user_lain='';
			$content .='<tr class="'.($r->st_aktif=='1' && $r->approve=='0'?'success':'active').'">';
			if ($no_step != $r->step){
				$no_step=$r->step;
			}
			if ($no_step % 2){
				$content .='<td> <span class="label label-default" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
			}else{
				$content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
				// $content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span></td>';
			}
			if ($r->approve !='0'){
				if ($r->iduser !=$r->user_eksekusi){
					$st_user_lain=' <span class="label label-warning" data-toggle="tooltip">Oleh User Lain</span>';
				}
			}
			$content .='<td><i class="si si-users fa-2x text-black-op"></i>  '.$r->user_nama.'</td>';
			$content .='<td>'.($r->st_aktif=='1'?status_approval($r->approve):'').' '.$st_user_lain.'</td>';			
			$content .='</tr>';
			
		}
		$arr['detail']=$content;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($content));
	}
	
}	

/* End of file tgudang_pemesanan.php */
/* Location: ./application/controllers/tgudang_pemesanan.php */
/* End of file Tgudang_pemesanan.php */
/* Location: ./application/controllers/Tgudang_pemesanan.php */
