<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mpengaturan_penginputan_ekpertise_radiologi extends CI_Controller
{
    /**
     * Pengaturan Penginputan Expertise Radiologi controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mpengaturan_penginputan_ekpertise_radiologi_model', 'model');
    }

    public function index(): void
    {
        $data = [];

        $data['error'] = '';
        $data['title'] = 'Tambah Pengaturan Penginputan Expertise Radiologi';
        $data['content'] = 'Mpengaturan_penginputan_ekpertise_radiologi/manage';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Pengaturan Penginputan Expertise Radiologi', '#'],
            ['Tambah', 'mpengaturan_penginputan_ekpertise_radiologi'],
        ];

        $data['list_setting'] = $this->model->getListSetting();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function delete($id): void
    {
        $this->model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mpengaturan_penginputan_ekpertise_radiologi', 'location');
    }

    public function save(): void
    {
        if ($this->model->saveData()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah ditambahkan.');
            redirect('mpengaturan_penginputan_ekpertise_radiologi', 'location');
        }
    }
}

// End of file welcome.php
// Location: ./system/application/controllers/welcome.php
