<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mpengaturan_tujuan_radiologi extends CI_Controller
{
    /**
     * Pengaturan Tujuan Radiologi controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mpengaturan_tujuan_radiologi_model');
    }

    public function index(): void
    {
        $data = [];
        $data['error'] = '';
        $data['title'] = 'Pengaturan Tujuan Radiologi';
        $data['content'] = 'Mpengaturan_tujuan_radiologi/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Pengaturan Tujuan Radiologi', '#'],
            ['List', 'mpengaturan_tujuan_radiologi'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create(): void
    {
        $data = [
            'id' => '',
            'nama' => '',
            'tipe_layanan' => '',
            'tujuan' => '',
            'status_file_audio' => '',
            'dokter_penanggung_jawab' => '',
        ];

        $data['error'] = '';
        $data['title'] = 'Tambah Pengaturan Tujuan Radiologi';
        $data['content'] = 'Mpengaturan_tujuan_radiologi/manage';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Pengaturan Tujuan Radiologi', '#'],
            ['Tambah', 'mpengaturan_tujuan_radiologi'],
        ];

        $data['list_audio'] = [];
        $data['list_dokter_radiologi'] = [];
        $data['list_akses_unit_lab'] = [];
        $data['user_radiologi_akses'] = [];
        $data['tipe_layanan_akses'] = [];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id): void
    {
        if ('' !== $id) {
            $row = $this->Mpengaturan_tujuan_radiologi_model->getSpecified($id);
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'nama' => $row->nama,
                    'tipe_layanan' => $row->tipe_layanan,
                    'tujuan' => $row->tujuan,
                    'status_file_audio' => $row->status_file_audio,
                    'dokter_penanggung_jawab' => $row->dokter_penanggung_jawab,
                ];
                $data['error'] = '';
                $data['title'] = 'Ubah Pengaturan Tujuan Radiologi';
                $data['content'] = 'Mpengaturan_tujuan_radiologi/manage';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Pengaturan Tujuan Radiologi', '#'],
                    ['Ubah', 'mpengaturan_tujuan_radiologi'],
                ];

                $data['list_audio'] = $this->Mpengaturan_tujuan_radiologi_model->getListAudio($id);
                $data['list_dokter_radiologi'] = $this->Mpengaturan_tujuan_radiologi_model->getListDokterRadiologi($id);
                $data['list_akses_unit_lab'] = $this->Mpengaturan_tujuan_radiologi_model->getListAksesUnit($id);
                $data['user_radiologi_akses'] = $this->Mpengaturan_tujuan_radiologi_model->getUserRadiologiAkses($id);
                $data['tipe_layanan_akses'] = $this->Mpengaturan_tujuan_radiologi_model->getTipeLayananAkses($id);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mkelas', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mpengaturan_tujuan_radiologi');
        }
    }

    public function delete($id): void
    {
        $this->Mpengaturan_tujuan_radiologi_model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mpengaturan_tujuan_radiologi', 'location');
    }

    public function save(): void
    {
        if ('' === $this->input->post('id')) {
            if ($this->Mpengaturan_tujuan_radiologi_model->saveData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data berhasil disimpan.');
                redirect('mpengaturan_tujuan_radiologi', 'location');
            }
        } else {
            if ($this->Mpengaturan_tujuan_radiologi_model->updateData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data berhasil diubah.');
                redirect('mpengaturan_tujuan_radiologi', 'location');
            }
        }
    }

    public function getIndex(): void
    {
        $this->select = ['merm_pengaturan_tujuan_radiologi.*', 'mtujuan.nama_tujuan AS tujuan_antrian'];
        $this->from = 'merm_pengaturan_tujuan_radiologi';
        $this->join = [
            ['mtujuan', 'mtujuan.id = merm_pengaturan_tujuan_radiologi.tujuan', '']
        ];
        $this->where = [
            'merm_pengaturan_tujuan_radiologi.status' => '1',
        ];
        $this->order = [
            'merm_pengaturan_tujuan_radiologi.id' => 'DESC',
        ];
        $this->group = [];

        $this->column_search = ['merm_pengaturan_tujuan_radiologi.nama'];
        $this->column_order = ['merm_pengaturan_tujuan_radiologi.nama'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = $r->nama;
            $row[] = GetTipeLayananRadiologi($r->tipe_layanan);
            $row[] = $r->tujuan_antrian;
            $row[] = StatusRow($r->status);
            $aksi = '<div class="btn-group">';
            $aksi .= '<a href="'.site_url().'mpengaturan_tujuan_radiologi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
            $aksi .= '<a href="#" data-urlindex="'.site_url().'mrak" data-urlremove="'.site_url().'mpengaturan_tujuan_radiologi/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function getPoliklinikData($asalPasien) {
        $data = array();

        if ($asalPasien == '1' || $asalPasien == '2') {
            // Fetch data for RAWAT_JALAN
            $data = get_all('mpoliklinik', ['status' => 1]);
        } elseif ($asalPasien == '3') {
            // Fetch data for RAWAT_INAP
            $data = get_all('mkelas', ['status' => 1]);
        }

        // Return JSON response
        echo json_encode($data);
    }
}

// End of file welcome.php
// Location: ./system/application/controllers/welcome.php
