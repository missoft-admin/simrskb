<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tpiutang_tt_reminder extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpiutang_tt_reminder_model','model');
	}

	function index() {
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		$tgl_terakhir = date('Y-m-d', strtotime($hari_ini));
		$data=array(
			'tipe'=>'#',
			'cara_bayar'=>'#',
			'no_po'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'#',
			// 'tgl_trx1'=>$tgl_pertama,
			// 'tgl_trx2'=>date('d-m-Y'),
			
			'tgl_trx1'=>'',
			'tgl_trx2'=>'',
			'tanggaldaftar1'=>'',
			'tanggaldaftar2'=>'',
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['title'] 			= 'Penghapusan Piutang Tidak Tertagih';
		$data['content'] 		= 'Tpiutang_tt_reminder/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Penghapusan Transaksi",'tverifikasi_transaksi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');

		$no_reg=$this->input->post('no_reg');
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$tipe=$this->input->post('tipe');
		$status=$this->input->post('status');
		$tgl_trx1=$this->input->post('tgl_trx1');
		$tgl_trx2=$this->input->post('tgl_trx2');
		$tanggaldaftar1=$this->input->post('tanggaldaftar1');
		$tanggaldaftar2=$this->input->post('tanggaldaftar2');
		
		// print_r($tipe);exit();
		$where1='';
		$where2='';
		$where='';
		if ($no_reg !=''){
			$where1 .=" AND H.nopendaftaran='$no_reg' ";
		}
		if ($no_medrec !=''){
			$where1 .=" AND H.no_medrec='$no_medrec' ";
		}
		if ($nama_pasien !=''){
			$where1 .=" AND H.namapasien LIKE '%".$nama_pasien."%' ";
		}

		if ($idkelompokpasien !='#'){
			$where1 .=" AND H.idkelompokpasien='$idkelompokpasien' ";
		}
		if ($idrekanan !='#'){
			$where1 .=" AND TK.idrekanan='$idrekanan' ";
		}
		if ($tipe !='#'){
			$where1 .=" AND H.idtipe='$tipe' ";
		}
		if ($status !='#'){
			if ($status=='0'){
				$where1 .=" AND ceil(DATEDIFF(CURRENT_DATE,H.tanggal_jatuh_tempo)) <= 0";
			}else{
				$where1 .=" AND ceil(DATEDIFF(CURRENT_DATE,H.tanggal_jatuh_tempo)) > 0";				
			}
			// $where1 .=" AND H.st_lunas='$status' ";
		}


		if ('' != $tgl_trx1) {
            $where1 .= " AND DATE(H.tanggal_transaksi) >='".YMDFormat($tgl_trx1)."' AND DATE(H.tanggal_transaksi) <='".YMDFormat($tgl_trx2)."'";
        }
		if ('' != $tanggaldaftar1) {
            $where1 .= " AND DATE(H.tanggal_kunjungan) >='".YMDFormat($tanggaldaftar1)."' AND DATE(H.tanggal_kunjungan) <='".YMDFormat($tanggaldaftar2)."'";
        }
        $from = "(SELECT ceil(DATEDIFF(CURRENT_DATE,H.tanggal_jatuh_tempo) / 30) as umur,ceil(DATEDIFF(CURRENT_DATE,H.tanggal_jatuh_tempo)) as umur_hari,H.*,'1' as st_verifikasi_piutang,K.nama as kel_nama
					FROM `tpiutang_tt_verifikasi` H
					LEFT JOIN mpasien_kelompok K ON K.id=H.idkelompokpasien
					WHERE H.`status`='1' AND H.st_lunas='0' ".$where1."
					
				) as tbl WHERE idtipe IS NOT NULL";


		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('no_medrec','namapasien','nopendaftaran');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url_rincian        = site_url('tpiutang_tt_monitoring/');
            $url        = site_url('tpiutang_tt_pembayaran/');
            $url_kasir        = site_url('tkasir/');
            $url_ranap        = site_url('trawatinap_tindakan/');
            $url_batal       = site_url('tgudang_pengembalian/');
            $url_klaim       = site_url('tklaim_rincian/');
            $url_dok_rajal       = site_url('tverifikasi_transaksi/upload_document/tkasir/');
            $url_dok_ranap       = site_url('tverifikasi_transaksi/upload_document/trawatinap_tindakan_pembayaran/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
			// if ($r->no_klaim){
				// $no_klaim='<br><br><span class="label label-warning">'.$r->no_klaim.'</span>';			
			// }else{
				// $no_klaim='';				
			// }
            $row[] = $r->idtipe;
            $row[] = $r->pembayaran_id;
            $row[] = $r->pendaftaran_id;
            $row[] = $r->id;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggal_kunjungan).'<br>'.$r->nopendaftaran;
            $row[] = GetTipePasienPiutang($r->idtipe);
            // $row[] = $r->nopendaftaran;
            $row[] = $r->no_medrec.'<br>'.($r->title?$r->title.'. ':'').$r->namapasien;//7
            $row[] = $r->no_medrec;
            $row[] = $r->kel_nama;//9
            $row[] = number_format($r->tidak_tertagih,2);//10
            $row[] = number_format($r->nominal_tagihan - $r->nominal_bayar - $r->nominal_dihapus,2);//11
            $row[] = number_format($r->nominal_lain,2);//12
            $row[] = HumanDateShort($r->tanggal_jatuh_tempo);//13
			$row[] = ($r->cara_bayar=='1'?text_success('NON CICILAN'):text_danger('CICILAN '.$r->cicilan_ke.' / '.$r->jml_cicilan));			//14	
            $row[] = ($r->umur);//15
			$row[] = (date('Y-m-d') <= $r->tanggal_jatuh_tempo?text_info('BELUM JATUH TEMPO'):text_danger('JATUH TEMPO'));//16
			$row[] = ($r->umur_hari > 0 ?text_danger('LEBIH DARI '.$r->umur_hari.' HARI'):text_info(($r->umur_hari*-1).' HARI JATUH TEMPO'));//17
			// if ($r->st_lunas=='0'){
				// $aksi .= '<a class="view btn btn-xs btn-success" href="'.$url.'bayar/'.$r->id.'" target="_blank"  type="button"  title="Add Penghapusan"><i class="fa fa-credit-card"></i></a>';
				
			// }else{
				
			// $aksi .= '<a class="view btn btn-xs btn-default" href="'.$url.'bayar/'.$r->id.'/disabled" target="_blank"  type="button"  title="Add Penghapusan"><i class="fa fa-credit-card"></i></a>';
			// }
			$aksi .= '<a class="view btn btn-xs btn-warning" href="'.$url_rincian.'rincian_tagihan/'.$r->tanggal_jatuh_tempo.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-external-link-square"></i></a>';
			if ($r->idtipe < 3){
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'transaksi/'.$r->pendaftaran_id.'/'.$r->kasir_id.'/1/1" target="_blank"  type="button"  title="Detail"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'kwitansi_verifikasi/'.$r->pembayaran_id.'" target="_blank"  type="button"  title="Kwitansi"><i class="fa fa-list-alt "></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'print_transaksi/'.$r->pendaftaran_id.'/'.$r->kasir_id.'" target="_blank"  type="button"  title="Rincian"><i class="fa fa-print"></i></a>';
			}else{
				$aksi .= '<a  class="view btn btn-xs btn-success" href="'.$url_ranap.'verifikasi/'.$r->pendaftaran_id.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<div class="btn-group" role="group">
						<button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-print"></i>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li class="dropdown-header">Print Ranap</li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->pendaftaran_id.'/0" target="_blank"  > Rincian Biaya (All)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->pendaftaran_id.'/1" target="_blank"  > Rincian Biaya (Verified)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->pendaftaran_id.'/0" target="_blank"  > Rincian Global (All)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->pendaftaran_id.'/1" target="_blank"  > Rincian Global (Verified)</a></li>
							
						</ul>
					</div>';
			}
			$aksi .= '<button title="Edit  Jatuh Tempo"  class="btn btn-xs btn-danger ganti"><i class="fa fa-calendar"></i></button>';
			
			

			$aksi.='</div>';
			$aksi .= ' <button title="Hapus piutang"  class="btn btn-xs btn-danger hapus"><i class="fa fa-trash"></i></button>';
            
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_upload(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$piutang_id=$this->input->post('piutang_id');
		
        $from = "(
					select *from tpiutang_tt_upload_hapus where piutang_id='$piutang_id' ORDER BY id DESC
				) as tbl ";
				
		
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('file_name');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $row[] = $r->id;
            $row[] = $no;
            $row[] = '<a href="'.base_url().'assets/upload/piutang_hapus/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a>';
            $row[] = $r->size;
			$aksi .= '<button title="Hapus" class="btn btn-xs btn-danger hapus_file"><i class="fa fa-trash"></i></button>';     
			
			$aksi.='</div>';
            $row[] = $aksi;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function tambah_fu_hapus(){
		
		$nominal_dihapus=RemoveComma($this->input->post('nominal_dihapus'));
		$piutang_id=$this->input->post('piutang_id');
		$tanggal_hapus=YMDFormat($this->input->post('tanggal_info'));
		// $tanggal_reminder=YMDFormat($this->input->post('tanggal_reminder'));
		$keterangan=$this->input->post('keterangan');
		$data=array(
			// 'piutang_id' =>$piutang_id,
			'st_lunas' =>1,
			'status_dihapus' =>1,
			'nominal_dihapus' =>$nominal_dihapus,
			'tanggal_hapus' =>$tanggal_hapus,
			'alasan_hapus' =>$keterangan,			
			'user_hapus' =>$this->session->userdata('user_name'),
		);
		$this->db->where('id', $piutang_id);
		$result=$this->db->update('tpiutang_tt_verifikasi', $data);		
		$this->output->set_output(json_encode($result));
	}
	public function upload_bukti_hapus() {
      $uploadDir = './assets/upload/piutang_hapus';
		if (!empty($_FILES)) {
			 $piutang_id = $this->input->post('upload_piutang_id');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['piutang_id'] 	= $piutang_id;
			$detail['file_name']		= $file_name;
			$detail['size']		= formatSizeUnits($size);
			$detail['tanggal_upload']	= date('Y-m-d H:i:s');
			$detail['user_upload']	= $this->session->userdata('user_name');
			$this->db->insert('tpiutang_tt_upload_hapus', $detail);
		}
		
    }
	function hapus_file(){
		$id=$this->input->post('id');
		$row =$this->db->query("select file_name from tpiutang_tt_upload_hapus where id='$id'")->row();
		if(file_exists('./assets/upload/piutang_hapus/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/piutang_hapus/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from tpiutang_tt_upload_hapus WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

}
