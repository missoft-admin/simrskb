<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdiagnosa_gizi extends CI_Controller {

	
	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mdiagnosa_gizi_model');
		$this->load->model('Antrian_layanan_model');
		$this->load->helper('path');
		
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('2010'))){
			$data = array();
			$data['error'] 			= '';
			$data['title'] 			= 'Diagnosa Gizi';
			$data['content'] 		= 'Mdiagnosa_gizi/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Diagnosa",'#'),
												  array("List",'mdiagnosa_gizi')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create(){
		
		$data = array(
			'id'            => '',
			'template_id' 					=> '0',
			'kode_diagnosa' 					=> '',
			'deskripsi' 					=> '',
			'nama' 					=> '',
		);

		$data['error'] 			= '';
		// $data['list_layanan'] 			= $this->Mdiagnosa_gizi_model->list_layanan('');
		// $data['list_sound'] 			= $this->Antrian_layanan_model->list_sound();
		// $data['list_user'] 			= $this->Mdiagnosa_gizi_model->list_user('');
		$data['title'] 			= 'Tambah Diagnosa Gizi';
		$data['content'] 		= 'Mdiagnosa_gizi/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Diagnosa",'#'),
								            array("Tambah",'mdiagnosa_gizi')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$data= $this->Mdiagnosa_gizi_model->getSpecified($id);
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Diagnosa Gizi';
			$data['content']    = 'Mdiagnosa_gizi/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Diagnosa Gizi",'#'),
										array("Ubah",'mdiagnosa_gizi')
										);

			// $data['statusAvailableApoteker'] = $this->Mdiagnosa_gizi_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mdiagnosa_gizi');
		}
	}

	function delete($id){
		
		$result=$this->Mdiagnosa_gizi_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('mdiagnosa_gizi','location');
	}
	function aktifkan($id){
		
		$result=$this->Mdiagnosa_gizi_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){
		if($this->input->post('id') == '' ) {
			$id=$this->Mdiagnosa_gizi_model->saveData();
			if($id){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mdiagnosa_gizi/update/'.$id,'location');
			}
		} else {
			if($this->Mdiagnosa_gizi_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mdiagnosa_gizi/index','location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mdiagnosa_gizi/manage';

		if($id==''){
			$data['title'] = 'Tambah Diagnosa Gizi';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Diagnosa Gizi",'#'),
							               array("Tambah",'mdiagnosa_gizi')
								           );
		}else{
			$data['title'] = 'Ubah Diagnosa Gizi';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Diagnosa Gizi",'#'),
							               array("Ubah",'mdiagnosa_gizi')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$where='';
			
			$this->select = array();
			$from="
					(
						select H.*,M.nama as nama_template
						FROM mdiagnosa_gizi H 
						LEFT JOIN mtemplate_gizi M ON M.id=H.template_id
						WHERE H.staktif='1'
						ORDER BY H.id
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
          $result[] = $r->nama.' ( '.$r->kode_diagnosa.' )';
          $result[] = $r->nama_template;
         
          $aksi = '<div class="btn-group">';
			if (UserAccesForm($user_acces_form,array('2012'))){
			$aksi .= '<a href="'.site_url().'mdiagnosa_gizi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
			}
			if (UserAccesForm($user_acces_form,array('2013'))){
			$aksi .= '<button title="Hapus" type="button" onclick="removeData('.$r->id.')" class="btn btn-danger btn-xs "><i class="fa fa-trash-o"></i></button>';
			}
		
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
	
}
