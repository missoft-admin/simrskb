<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tklaim_verifikasi extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tklaim_verifikasi_model','model');
	}

	function index() {
		$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d'));
		// $date2=(date('Y-m-d'));
		date_add($date1,date_interval_create_from_date_string("-1 days"));
		date_add($date2,date_interval_create_from_date_string("0 days"));
		$date1= date_format($date1,"d-m-Y");
		$date2= date_format($date2,"d-m-Y");
		$data=array(
			'tipe'=>'#',
			'cara_bayar'=>'#',
			'no_po'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'0',
			'tgl_trx1'=>$date1,
			'tgl_trx2'=>$date2,
			'tanggaldaftar1'=>'',
			'tanggaldaftar2'=>'',
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['title'] 			= 'Verifikasi Piutang';
		$data['content'] 		= 'Tklaim_verifikasi/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Verifikasi Transaksi",'tverifikasi_transaksi/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');


		$no_reg=$this->input->post('no_reg');
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$tipe=$this->input->post('tipe');
		$status=$this->input->post('status');
		$tgl_trx1=$this->input->post('tgl_trx1');
		$tgl_trx2=$this->input->post('tgl_trx2');
		$tanggaldaftar1=$this->input->post('tanggaldaftar1');
		$tanggaldaftar2=$this->input->post('tanggaldaftar2');

		$where1='';
		$where2='';
		$where='';
		if ($no_reg !=''){
			$where1 .=" AND TP.nopendaftaran='$no_reg' ";
			$where2 .=" AND RI.nopendaftaran='$no_reg' ";
		}
		if ($no_medrec !=''){
			$where1 .=" AND TP.no_medrec='$no_medrec' ";
			$where2 .=" AND RI.no_medrec='$no_medrec' ";
		}
		if ($nama_pasien !=''){
			$where1 .=" AND TP.namapasien='$nama_pasien' ";
			$where2 .=" AND RI.namapasien='$nama_pasien' ";
		}

		if ($idkelompokpasien !='#'){
			$where1 .=" AND TK.tipekontraktor='$idkelompokpasien' ";
			$where2 .=" AND PD.tipekontraktor='$idkelompokpasien' ";
		}
		if ($idrekanan !='#'){
			$where1 .=" AND TK.idkontraktor='$idrekanan' ";
			$where2 .=" AND PD.idkontraktor='$idrekanan' ";
		}
		if ($tipe !='#'){
			$where .=" AND tipe='$tipe' ";
		}
		if ($status !='#'){
			$where1 .=" AND TK.st_verifikasi_piutang='$status' ";
			$where2 .=" AND PD.st_verifikasi_piutang='$status' ";
		}


		if ('' != $tgl_trx1) {
            $where1 .= " AND DATE(K.tanggal) >='".YMDFormat($tgl_trx1)."' AND DATE(K.tanggal) <='".YMDFormat($tgl_trx2)."'";
            $where2 .= " AND DATE(PDH.tanggal) >='".YMDFormat($tgl_trx1)."' AND DATE(PDH.tanggal) <='".YMDFormat($tgl_trx2)."'";
        }
		if ('' != $tanggaldaftar1) {
            $where1 .= " AND DATE(TP.tanggaldaftar) >='".YMDFormat($tanggaldaftar1)."' AND DATE(TP.tanggaldaftar) <='".YMDFormat($tanggaldaftar2)."'";
            $where2 .= " AND DATE(RI.tanggaldaftar) >='".YMDFormat($tanggaldaftar1)."' AND DATE(RI.tanggaldaftar) <='".YMDFormat($tanggaldaftar2)."'";
        }
        $from = "(
					SELECT CASE WHEN S.id IS NOT NULL THEN 1 ELSE 2 END as st_setting,TK.id as idpembayaran

					, '1' as tipe,'RJ' as tipe_nama,TP.id as idpendaftaran
					,TP.tanggaldaftar,K.tanggal as tanggal_kasir,TP.nopendaftaran, TP.idpasien,TP.no_medrec,TP.namapasien,TP.idkelompokpasien,Kel.nama as kel_pasien
					,TK.tipekontraktor,R.nama as asuransi_nama,TK.idkontraktor,TK.nominal,TK.`status`,TK.st_verifikasi_piutang
					,TK.tanggal_jatuh_tempo
					,cek_jatuh_tempo_bayar ( TK.tipekontraktor, TK.idkontraktor, 1, S.id,Kel.st_multiple,TP.idpasien) AS next_date
					,K.id as idkasir,K.total as tot_trx,tklaim.`status` as status_klaim,KD.id as klaim_detail_id,tklaim.id as klaim_id,tklaim.no_klaim
					,Kel.st_multiple
					from tkasir K
					INNER JOIN tkasir_pembayaran TK  ON TK.idkasir=K.id
					LEFT JOIN tpoliklinik_tindakan TD ON TD.id=K.idtindakan
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TD.idpendaftaran
					LEFT JOIN mpasien_kelompok Kel ON Kel.id=TK.tipekontraktor
					LEFT JOIN mrekanan R ON R.id=TK.idkontraktor AND TK.tipekontraktor='1'
					LEFT JOIN mrekanan_setting S ON S.idrekanan=TK.idkontraktor AND S.`status`='1' AND TK.tipekontraktor='1' AND S.tipe='1'
					LEFT JOIN tklaim_detail KD ON KD.pembayaran_id=TK.id AND KD.tipe='1'
					LEFT JOIN tklaim ON tklaim.id=KD.klaim_id
					WHERE TK.idmetode='7' AND K.`status` !='0' AND K.idtipe IN (1,2) ".$where1."
					GROUP BY TK.id
					UNION ALL

					SELECT  CASE WHEN S.id IS NOT NULL THEN 1 ELSE 2 END as st_setting,PD.id as idpembayaran
					, '2' as tipe,'RI/ODS' as tipe_nama,RI.id as idpendaftaran
					,RI.tanggaldaftar,PDH.tanggal as tanggal_kasir,RI.nopendaftaran,RI.idpasien,RI.no_medrec,RI.namapasien,RI.idkelompokpasien,Kel.nama as kel_pasien,PD.tipekontraktor
					,R.nama as asuransi_nama,PD.idkontraktor,PD.nominal,RI.`status`,PD.st_verifikasi_piutang
					,PD.tanggal_jatuh_tempo
					,cek_jatuh_tempo_bayar ( PD.tipekontraktor, PD.idkontraktor, 2, S.id,Kel.st_multiple,RI.idpasien ) AS next_date
					,PDH.id as idkasir,PDH.totalharusdibayar as tot_trx,tklaim.`status` as status_klaim,KD.id as klaim_detail_id,tklaim.id as klaim_id,tklaim.no_klaim
					,Kel.st_multiple
					FROM trawatinap_tindakan_pembayaran PDH
					LEFT JOIN trawatinap_tindakan_pembayaran_detail PD ON PDH.id=PD.idtindakan
					LEFT JOIN trawatinap_pendaftaran RI ON RI.id=PDH.idtindakan
					LEFT JOIN mpasien_kelompok Kel ON Kel.id=PD.tipekontraktor
					LEFT JOIN mrekanan R ON R.id=PD.idkontraktor AND PD.tipekontraktor='1'
					LEFT JOIN mrekanan_setting S ON S.idrekanan=PD.idkontraktor AND S.`status`='1' AND PD.tipekontraktor='1' AND S.tipe='2'
					LEFT JOIN tklaim_detail KD ON KD.pembayaran_id=PD.id AND KD.tipe='2'
					LEFT JOIN tklaim ON tklaim.id=KD.klaim_id
					WHERE PD.idmetode='8' AND PDH.statusbatal='0' ".$where2."
					GROUP BY PD.id
				) as tbl WHERE tipe is not null ".$where." ORDER BY tanggaldaftar ASC";


		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tkontrabon_verifikasi/');
            $url_kasir        = site_url('tkasir/');
            $url_ranap        = site_url('trawatinap_tindakan/');
            $url_batal       = site_url('tgudang_pengembalian/');
            $url_klaim       = site_url('tklaim_rincian/');
            $url_dok_rajal       = site_url('tverifikasi_transaksi/upload_document/tkasir/');
            $url_dok_ranap       = site_url('tverifikasi_transaksi/upload_document/trawatinap_tindakan_pembayaran/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
			if ($r->no_klaim){
				$no_klaim='<br><br><span class="label label-warning">'.$r->no_klaim.'</span>';			
			}else{
				$no_klaim='';				
			}
            $row[] = $r->tipe;
            $row[] = $r->idpembayaran;
            $row[] = $no.' '.$r->status_klaim;
            $row[] = HumanDateShort($r->tanggaldaftar);
            $row[] = HumanDateShort($r->tanggal_kasir);
            $row[] = ($r->tipe=='1' ? '<span class="label label-primary">'.$r->tipe_nama.'</span>':'<span class="label label-success">'.$r->tipe_nama.'</span>');
            $row[] = $r->nopendaftaran;
            $row[] = $r->no_medrec;
            $row[] = $r->namapasien;
            $row[] = $r->kel_pasien;
            $row[] = ($r->idkelompokpasien!='1'?$r->kel_pasien:$r->asuransi_nama);
            $row[] = status_piutang($r->st_verifikasi_piutang).$no_klaim;
            $row[] = number_format($r->tot_trx,0);
            $row[] = '<span class="label label-primary">'.number_format($r->nominal,0).'</span>';
            $row[] = number_format($r->tot_trx-$r->nominal,0);
			if ($r->st_verifikasi_piutang=='0'){
				// if ($r->tanggal_jatuh_tempo==null || ($r->tanggal_jatuh_tempo < date('Y-m-d'))){
					if ($r->next_date){
						$row[] = HumanDateShort($r->next_date);
					}else{
						$row[] = '<span class="label label-danger">BELUM DISETTING</span>';
						$date_setting='0';
					}

				// }else{
					// $row[] = HumanDateShort($r->tanggal_jatuh_tempo);
				// }
			}else{
				$row[] = HumanDateShort($r->tanggal_jatuh_tempo);//15
			}

			if ($r->status_klaim){
			$aksi .= '<a class="view btn btn-xs btn-warning" href="'.$url_klaim.'rincian_tagihan/'.$r->klaim_id.'/'.$r->tipe.'/1" target="_blank"  type="button"  title="Rincian Tagihan"><i class="fa fa-external-link"></i></a>';
			}
			if($r->st_verifikasi_piutang=='0'){
				$aksi .= '<button title="Verifikasi" '.($date_setting=='0'?"disabled":"").' class="btn btn-xs btn-danger verifikasi"><i class="fa fa-check"></i></button>';
			}
			if ($r->st_verifikasi_piutang=='1'){
				if ($r->status_klaim=='1'){
					$aksi .= '<button title="Pindahkan Tanggal"  class="btn btn-xs btn-primary ganti"><i class="si si-login"></i></button>';
				}
			}
			if ($r->tipe=='1'){
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'transaksi/'.$r->idpendaftaran.'/'.$r->idkasir.'/1/1" target="_blank"  type="button"  title="Detail"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'kwitansi_verifikasi/'.$r->idpembayaran.'" target="_blank"  type="button"  title="Kwitansi"><i class="fa fa-list-alt "></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url_kasir.'print_transaksi/'.$r->idpendaftaran.'/'.$r->idkasir.'" target="_blank"  type="button"  title="Rincian"><i class="fa fa-print"></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-danger" href="'.$url_dok_rajal.$r->idkasir.'" target="_blank"  type="button"  title="Upload"><i class="fa fa-upload"></i></a>';
			}else{
				$aksi .= '<a  class="view btn btn-xs btn-success" href="'.$url_ranap.'verifikasi/'.$r->idpendaftaran.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<div class="btn-group" role="group">
						<button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-print"></i>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li class="dropdown-header">Print Ranap</li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->idpendaftaran.'/0" target="_blank"  > Rincian Biaya (All)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_biaya/'.$r->idpendaftaran.'/1" target="_blank"  > Rincian Biaya (Verified)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->idpendaftaran.'/0" target="_blank"  > Rincian Global (All)</a></li>
							<li><a tabindex="-1" href="'.$url_ranap.'print_rincian_global/'.$r->idpendaftaran.'/1" target="_blank"  > Rincian Global (Verified)</a></li>
							
						</ul>
					</div>';
				// $aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_ranap.'print_rincian_global/'.$r->idpendaftaran.'/1" target="_blank"  type="button"  title="Print Rincian Global"><i class="fa fa-list-alt "></i></a>';
				// $aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_ranap.'print_rincian_biaya/'.$r->idpendaftaran.'/1" target="_blank"  type="button"  title="Print Rincian Biaya"><i class="fa fa-print"></i></a>';
				$aksi .= '<a class="view btn btn-xs btn-danger" href="'.$url_dok_ranap.$r->idkasir.'" target="_blank"  type="button"  title="Upload"><i class="fa fa-upload"></i></a>';
			}
			
			if ($r->st_verifikasi_piutang=='0'){//UPDATE TANGGAL
				// if ($r->tanggal_jatuh_tempo==null || ($r->tanggal_jatuh_tempo < date('Y-m-d'))){
				if ($r->tanggal_jatuh_tempo != $r->next_date){
					if ($r->tipe=='1'){//RAJAL
						if ($r->next_date){
							$q="UPDATE tkasir_pembayaran SET tanggal_jatuh_tempo='".YMDFormat($r->next_date)."' WHERE id='".$r->idpembayaran."'";
							$this->db->query($q);
						}
					}else{//RANAP
						if ($r->next_date){
							$q="UPDATE trawatinap_tindakan_pembayaran_detail SET tanggal_jatuh_tempo='".YMDFormat($r->next_date)."' WHERE id='".$r->idpembayaran."'";
							$this->db->query($q);
						}
					}
				}
			}

			$aksi.='</div>';
            $row[] = $aksi;//16
            $row[] = $r->klaim_detail_id;//17
            $row[] = '';//18
            $row[] = '';//19
            $row[] = $r->tipekontraktor;//20
            $row[] = $r->idkontraktor;//21
            $row[] = $r->st_multiple;//22
            $row[] = $r->idpasien;//23
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function cek_date(){
		$tgl='2011-20-01';
		$tgl_asli=(int)substr($tgl,-2);
		print_r($tgl_asli);
	}

	public function verifikasi()
    {

		$where='';
        $id = $this->input->post('id');
        $tipe = $this->input->post('tipe');
        $tipekontraktor = $this->input->post('tipekontraktor');
        $idkontraktor = $this->input->post('idkontraktor');
        $idpasien = $this->input->post('idpasien');
        $st_multiple = $this->input->post('st_multiple');
        $tanggal_jt =YMDFormat($this->input->post('tanggal_jt'));
		$tgl_angka=(int)substr($tanggal_jt,-2);
		if ($this->cek_duplicate($tipe,$id)==false){
					
			// if ($tipe='1'){
				if ($tipekontraktor=='1'){
					$where .=" AND  K.idrekanan='$idkontraktor'";
				}
				if ($st_multiple=='2'){
					$where .=" AND  K.st_multiple='$st_multiple' AND  K.idpasien='$idpasien'";
				}else{
					$where .=" AND  K.st_multiple='$st_multiple' ";
					$idpasien=null;
				}
				$q_ada="SELECT K.id from tklaim K
					WHERE K.tipe='$tipe' AND K.idkelompokpasien='$tipekontraktor'  AND K.tanggal_tagihan='$tanggal_jt' AND status>1 ".$where;
				$klaim_ada=$this->db->query($q_ada)->row('id');
				if ($klaim_ada==null){
				$q="SELECT K.id from tklaim K
					WHERE K.tipe='$tipe' AND K.idkelompokpasien='$tipekontraktor'  AND K.tanggal_tagihan='$tanggal_jt' AND status=1 ".$where;
				$klaim_id=$this->db->query($q)->row('id');
				if ($klaim_id==''){
					if ($tipekontraktor=='1'){
					$q_set="SELECT * FROM (
							SELECT '1' tipe,M.tanggal_hari,M.batas_kirim,M.jml_hari FROM mrekanan_setting M
							WHERE M.idrekanan='$idkontraktor' AND M.`status`='1' AND M.tanggal_hari='$tgl_angka'

							UNION ALL

							SELECT '2' tipe,M.tanggal_hari,M.batas_kirim,M.jml_hari FROM mpasien_kelompok_setting M
							WHERE M.idkelompokpasien='$tipekontraktor' AND M.`status`='1' AND M.tanggal_hari='$tgl_angka'

							) T ORDER BY T.tipe ASC LIMIT 1";
					}else{
						$q_set="SELECT * FROM (
							SELECT '2' tipe,M.tanggal_hari,M.batas_kirim,M.jml_hari FROM mpasien_kelompok_setting M
							WHERE M.idkelompokpasien='$tipekontraktor' AND M.`status`='1' AND M.tanggal_hari='$tgl_angka'

							) T ORDER BY T.tipe ASC LIMIT 1";
					}
					$row=$this->db->query($q_set)->row();
					if ($row){
						$var_batas_kirim=$row->batas_kirim;
						$var_jml_hari=$row->jml_hari;

						$date=date_create($tanggal_jt);
						date_add($date,date_interval_create_from_date_string("$var_batas_kirim days"));
						$batas_kirim = date_format($date,"Y-m-d");

						$date2=date_create($tanggal_jt);
						date_add($date2,date_interval_create_from_date_string("$var_jml_hari days"));
						$jatuh_tempo_bayar = date_format($date2,"Y-m-d");


						// $batas_kirim
					}else{
						$batas_kirim='';
						$jatuh_tempo_bayar='';
					}

					$data_info=array(
						'tipe' =>$tipe,
						'idkelompokpasien' =>$tipekontraktor,
						'idrekanan' =>$idkontraktor,
						'st_multiple' =>$st_multiple,
						'idpasien' =>$idpasien,
						'tanggal_tagihan' =>$tanggal_jt,
						'tanggal_tagihan' =>$tanggal_jt,
						'batas_kirim' =>$batas_kirim,
						'jatuh_tempo_bayar' =>$jatuh_tempo_bayar,
						'created_date' =>date('Y-m-d H:i:s'),
						'created_user_id' =>$this->session->userdata('user_id'),
						'created_user' =>$this->session->userdata('user_name'),
						'status' =>'1',
					);
					// print_r($data_info);
					$this->db->insert('tklaim', $data_info);
					$klaim_id=$this->db->insert_id();
				}
				$data_det=array(
						'klaim_id' =>$klaim_id,
						'tipe' =>$tipe,
						'pembayaran_id' =>$id,
						'user_verifikasi'=>$this->session->userdata('user_name'),
						'tanggal_verifikasi'=>date('Y-m-d H:i:s'),
						'tanggal_tagihan' =>$tanggal_jt,
					);
				$result=$this->db->insert('tklaim_detail', $data_det);

			}else{
				$result=false;
			}



			$data =array(
				'st_verifikasi_piutang'=>'1',
				'user_verifikasi'=>$this->session->userdata('user_name'),
				'tanggal_verifikasi'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $id);
			if ($tipe=='1'){
				$result=$this->db->update('tkasir_pembayaran', $data);
			}else{
				$result=$this->db->update('trawatinap_tindakan_pembayaran_detail', $data);
			}
			if ($result) {
				$this->output->set_output(json_encode($result));
			} else {
				$this->output->set_output(json_encode($result));
			}
		}else{
			
            $this->output->set_output(json_encode(false));
		}
    }
	function cek_duplicate($tipe,$id){
		$q="SELECT D.id FROM `tklaim_detail` D WHERE D.pembayaran_id='$id' AND D.tipe='$tipe'";
		$result=$this->db->query($q)->row('id');
		if ($result){
			return true;
		}else{
			return false;
		}
	}


}
