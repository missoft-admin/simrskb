<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrian_poli_tujuan extends CI_Controller {

	
	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Antrian_poli_tujuan_model');
		$this->load->model('Antrian_layanan_model');
		$this->load->helper('path');
		
  }

	function index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1552'))){
			$data = array();
			$data['error'] 			= '';
			$data['title'] 			= 'Pengaturan Tujuan';
			$data['content'] 		= 'Antrian_poli_tujuan/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Pengaturan Tujuan",'#'),
												  array("List",'antrian_poli_tujuan')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create(){
		
		$data = array(
			'id'            => '',
			'nama_tujuan' 					=> '',
			'jenis' 					=> '',
		);

		$data['error'] 			= '';
		// $data['list_layanan'] 			= $this->Antrian_poli_tujuan_model->list_layanan('');
		// $data['list_sound'] 			= $this->Antrian_layanan_model->list_sound();
		$data['list_user'] 			= $this->Antrian_poli_tujuan_model->list_user('');
		$data['title'] 			= 'Tambah Pengaturan Tujuan';
		$data['content'] 		= 'Antrian_poli_tujuan/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Pengaturan Tujuan",'#'),
								            array("Tambah",'antrian_poli_tujuan')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$data= $this->Antrian_poli_tujuan_model->getSpecified($id);
			$data['list_user'] 			= $this->Antrian_poli_tujuan_model->list_user($id);
			$data['list_sound'] 			= $this->Antrian_layanan_model->list_sound();
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Pengaturan Tujuan';
			$data['content']    = 'Antrian_poli_tujuan/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Pengaturan Tujuan",'#'),
										array("Ubah",'antrian_poli_tujuan')
										);

			// $data['statusAvailableApoteker'] = $this->Antrian_poli_tujuan_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('antrian_poli_tujuan');
		}
	}

	function delete($id){
		
		$result=$this->Antrian_poli_tujuan_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('antrian_poli_tujuan','location');
	}
	function aktifkan($id){
		
		$result=$this->Antrian_poli_tujuan_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){
		if($this->input->post('id') == '' ) {
			if($this->Antrian_poli_tujuan_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('antrian_poli_tujuan/create','location');
			}
		} else {
			if($this->Antrian_poli_tujuan_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('antrian_poli_tujuan/index','location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Antrian_poli_tujuan/manage';

		if($id==''){
			$data['title'] = 'Tambah Pengaturan Tujuan';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Pengaturan Tujuan",'#'),
							               array("Tambah",'antrian_poli_tujuan')
								           );
		}else{
			$data['title'] = 'Ubah Pengaturan Tujuan';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Pengaturan Tujuan",'#'),
							               array("Ubah",'antrian_poli_tujuan')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$where='';
			
			$this->select = array();
			$from="
					(
						select H.*
						,CASE WHEN H.jenis=1 THEN 'STATIC' ELSE 'DINAMIS' END as jenis_nama
						,UM.nama_user
						FROM mtujuan H 
						LEFT JOIN (
							SELECT UU.tujuan_id, GROUP_CONCAT(MU.`name` SEPARATOR ';') as nama_user FROM mtujuan_user UU 
							LEFT JOIN musers MU ON MU.id=UU.userid
							GROUP BY UU.tujuan_id
						) UM ON UM.tujuan_id=H.id
						GROUP BY H.id
						ORDER BY H.id
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_tujuan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
          $result[] = $r->nama_tujuan;
          $result[] = $r->jenis_nama;

		  $nama_user='';
		  $arr_user=explode(';',$r->nama_user);
		  foreach($arr_user as $index=>$val){
			  $nama_user .=text_success($val).' ';
		  }
          $result[] = $nama_user;
          $result[] = ($r->status?text_primary('AKTIF'):text_danger('TIDAK AKTIF'));
          $aksi = '<div class="btn-group">';
			if ($r->status=='1'){
				if (UserAccesForm($user_acces_form,array('1554'))){
				$aksi .= '<a href="'.site_url().'antrian_poli_tujuan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1555'))){
				$aksi .= '<button title="Hapus" type="button" onclick="removeData('.$r->id.')" class="btn btn-danger btn-xs "><i class="fa fa-trash-o"></i></button>';
				}
			}else{
				if (UserAccesForm($user_acces_form,array('1556'))){
				$aksi .= '<button title="Aktifkan" onclick="aktifkan('.$r->id.')" type="button" class="btn btn-success btn-xs aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
				}
			}
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function simpan_sound(){
		$tujuan_id=$this->input->post('tujuan_id');
		$idsound=$this->input->post('idsound');
		$sound_id=$this->input->post('sound_id');
		$nourut=$this->input->post('nourut');
		$data['tujuan_id']=$tujuan_id;
		$data['sound_id']=$sound_id;
		$data['nourut']=$nourut;
		if ($idsound==''){
			// print_r($data);exit;
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('mtujuan_sound',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$idsound);
			$hasil=$this->db->update('mtujuan_sound',$data);
		}
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		
		  
		  json_encode($hasil);
	}
	function load_sound(){
		$tujuan_id=$this->input->post('tujuan_id');
		$this->select = array();
	  $from="(
		SELECT H.id, H.nourut,H.sound_id,M.nama_asset,M.file_sound 
		
			FROM mtujuan_sound H
			LEFT JOIN antrian_asset_sound M ON H.sound_id=M.id
			 WHERE H.status='1' AND H.tujuan_id='$tujuan_id' ORDER BY nourut
		) as tbl";
		// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('sound_id');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		$no++;
		$result = array();

		$result[] = $no;
		$result[] = $r->nourut;
		$result[] = $r->nama_asset.' ('.$r->file_sound.')';

		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_sound('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="si si-pencil"></i></button>';	

		$aksi .= '<button onclick="hapus_sound('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		 $aksi .= '</div>';
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
	}
	function edit_sound(){
		$id=$this->input->post('id');
		$q="select *FROM mtujuan_sound H WHERE H.id='$id'";
		$data=$this->db->query($q)->row_array();
		echo json_encode($data);
	}
	function load_data_sound(){
		$tujuan_id=$this->input->post('tujuan_id');
		$q="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') file
			FROM `mtujuan_sound` H
			LEFT JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.tujuan_id='$tujuan_id' AND H.status='1'
			ORDER BY H.nourut ASC";
		$data=$this->db->query($q)->row_array();
		echo json_encode($data);
	}
	function hapus_sound(){
		$id=$this->input->post('id');
		$data['status']=0;
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		// print_r($data);exit;
		$this->db->where('id',$id);
		$hasil=$this->db->update('mtujuan_sound',$data);
		json_encode($hasil);
	}
}
