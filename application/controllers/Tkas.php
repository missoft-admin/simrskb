<?php

class Tkas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Tkas_model');
    }

    public function create($idtipe, $idtransaksi)
    {
      if($idtipe != '' && $idtransaksi != ''){
        $row = $this->Tkas_model->getDataPencairan($idtipe, $idtransaksi);
        if (isset($row->id)) {
          $data = array(
            'iddetailpencairan' => $row->id,
            'notransaksi'       => $row->notransaksi,
            'namatransaksi'     => $row->namatransaksi,
            'nominaltransaksi'  => $row->nominaltransaksi,
            'tanggaltransaksi'  => date("d/m/Y"),
          );

          $data['error']      = '';
          $data['title']      = 'Transaksi Kas';
          $data['content']    = 'Tkas/manage';
          $data['breadcrum']  = array(
                                 array("RSKB Halmahera",'dashboard'),
                                 array("Bendahara",'#'),
                                 array("Transaksi Kas",'tkas'),
                                 array("Tambah",'')
                              );

          $data['list_akundebit'] = $this->Tkas_model->getListAkunDebit();
          $data['list_akunkredit'] = $this->Tkas_model->getListAkunKredit();

          $data = array_merge($data,backend_info());
          $this->parser->parse('module_template',$data);
        }else{
          $this->session->set_flashdata('error',true);
          $this->session->set_flashdata('message_flash','data tidak ditemukan.');
          redirect('tpencairan','location');
        }
      }else{
        $this->session->set_flashdata('error',true);
        $this->session->set_flashdata('message_flash','data tidak ditemukan.');
        redirect('tpencairan','location');
      }
    }

    public function save()
    {
        if ($this->Tkas_model->saveData()) {
          $this->session->set_flashdata('success',true);
          $this->session->set_flashdata('message_flash','Transaksi Kas berhasil.');
          redirect('tpencairan','location');
        }
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
