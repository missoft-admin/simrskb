<?php

class Mpendapatan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mpendapatan_model');
    }

    public function index()
    {

        $data['error']      = '';
        $data['title']      = 'Pendapatan';
        $data['content']    = 'Mpendapatan/index';
        $data['breadcrum']  = array(
                                 array("RSKB Halmahera",'dashboard'),
                                 array("Bendahara",'#'),
                                 array("Pendapatan",'mpendapatan'),
                                 array("List",'')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create()
    {

        $data = array(
            'id'          => '',
            'keterangan'  => '',
            'idakun'      => '#',
            'noakun'      => '',
            'status'      => '',
        );

        $data['error']      = '';
        $data['title']      = 'Tambah Pendapatan';
        $data['content']    = 'Mpendapatan/manage';
        $data['breadcrum']  = array(
                                 array("RSKB Halmahera",'dashboard'),
                                 array("Bendahara",'#'),
                                 array("Pendapatan",'mpendapatan'),
                                 array("Tambah",'')
                              );

        $data['list_noakun'] = $this->Mpendapatan_model->getNoAkun();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id)
    {

        if ($id != '') {
            $row = $this->Mpendapatan_model->getSpecified($id);
            if (isset($row->id)) {
                $data = array(
                  'id'          => $row->id,
                  'idakun'  => $row->idakun,
                  'keterangan'  => $row->keterangan,
                  'noakun'      => $row->noakun,
                  'status'      => $row->status,
                );

                $data['error']      = '';
                $data['title']      = 'Ubah Pendapatan';
                $data['content']    = 'Mpendapatan/manage';
                $data['breadcrum']  = array(
                                         array("RSKB Halmahera",'dashboard'),
                                         array("Bendahara",'#'),
                                         array("Pendapatan",'mpendapatan'),
                                         array("Ubah",'')
                                      );

                $data['list_noakun'] = $this->Mpendapatan_model->getNoAkun();

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mpendapatan', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mpendapatan');
        }
    }

    public function delete($id)
    {

        $this->Mpendapatan_model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mpendapatan', 'location');
    }

    public function save()
    {
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
        $this->form_validation->set_rules('noakun', 'No. Akun', 'trim|required');

        if ($this->form_validation->run() == true) {
            if ($this->input->post('id') == '') {
                if ($this->Mpendapatan_model->saveData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mpendapatan', 'location');
                }
            } else {
                if ($this->Mpendapatan_model->updateData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mpendapatan', 'location');
                }
            }
        } else {
            $this->failed_save($this->input->post('id'));
        }
    }

    public function failed_save($id)
    {
        $data = $this->input->post();
        $data['error']   = validation_errors();
        $data['content'] = 'Mpendapatan/manage';

        if ($id=='') {
            $data['title']      = 'Create Pendapatan';
            $data['breadcrum']  = array(
                                     array("RSKB Halmahera",'dashboard'),
                                     array("Bendahara",'#'),
                                     array("Pendapatan",'mpendapatan'),
                                     array("Create",'')
                                  );
        } else {
            $data['title']      = 'Edit Data';
            $data['breadcrum']  = array(
                                     array("RSKB Halmahera",'dashboard'),
                                     array("Bendahara",'#'),
                                     array("Pendapatan",'mpendapatan'),
                                     array("Edit",'')
                                  );
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function getIndex()
    {
        $this->select = array('mpendapatan.*', 'makun_nomor.noakun', 'makun_nomor.namaakun');
        $this->from   = 'mpendapatan';
        $this->join 	= array(
          array('makun_nomor', 'makun_nomor.noakun = mpendapatan.noakun','')
        );
        $this->where  = array(
          'mpendapatan.status' => '1'
        );
        $this->order  = array(
          'mpendapatan.keterangan' => 'ASC'
        );
        $this->group  = array();

        $this->column_search   = array('mpendapatan.keterangan');
        $this->column_order    = array('mpendapatan.keterangan');

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->keterangan;
            $row[] = '<i>'.$r->noakun.' '.$r->namaakun.'<i>';
            $aksi = '<div class="btn-group">';
            // if(button_roles('mpendapatan/update')) {
              $aksi .= '<a href="'.site_url().'mpendapatan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
            // }
            // if(button_roles('mpendapatan/delete')) {
              $aksi .= '<a href="#" data-urlindex="'.site_url().'mpendapatan" data-urlremove="'.site_url().'mpendapatan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
            // }
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = array(
  	      "draw" => $_POST['draw'],
  	      "recordsTotal" => $this->datatable->count_all(),
  	      "recordsFiltered" => $this->datatable->count_all(),
  	      "data" => $data
        );
        echo json_encode($output);
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
