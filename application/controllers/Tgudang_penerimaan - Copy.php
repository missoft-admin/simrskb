<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tgudang_penerimaan extends CI_Controller {
    public function __construct() {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('tgudang_penerimaan_model', 'model');
        $this->load->model('Mdistributor_model', 'distributor_model');
    }

    public function _list($data) {
        $this->session->set_userdata($data);
        $data['error'] = '';
        $data['title'] = 'Penerimaan Gudang';
        $data['content'] = 'Tgudang_penerimaan/index';
        $data['breadcrum'] = array(array('RSKB Halmahera', '#'),
            array('Penerimaan Gudang', '#'),
            array('List', 'tgudang_penerimaan'), );
        $data['list_distributor'] = $this->distributor_model->load_all();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function index() {
        $data = array(
            'iddistributor' => '',
            'statuspenerimaan' => '',
            'tanggaldari' => '',
            'tanggalsampai' => '',
        );
        $this->_list($data);
    }

    public function filter() {
        $data = array(
            'iddistributor' => $this->input->post('iddistributor'),
            'statuspenerimaan' => $this->input->post('statuspenerimaan'),
            'tanggaldari' => $this->input->post('tanggaldari'),
            'tanggalsampai' => $this->input->post('tanggalsampai'),
        );
        $this->_list($data);
    }

    public function create() {
        $data = array(
            'id' => '',
            'nopenerimaan' => '',
            'idpemesanan' => '',
            'totalbarang' => '',
            'tanggalpenerimaan' => date('d-m-Y'),
            'tanggaljatuhtempo' => '',
            'totalharga' => '',
        );
		// print_r($data);exit();
        $data['error'] = '';
        $data['title'] = 'Tambah Penerimaan Gudang';
        $data['content'] = 'Tgudang_penerimaan/add';
        $data['breadcrum'] = array(array('RSKB Halmahera', '#'),
            array('Penerimaan Gudang', '#'),
            array('Create', '#'), );
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function save() {
        
		if ($this->model->save()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
            redirect('tgudang_penerimaan/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_penerimaan/index', 'refresh');
        }
    }

    public function ajax_list() {
        $iddistributor = $this->session->userdata('iddistributor');
        $statuspenerimaan = $this->session->userdata('statuspenerimaan');
        $tanggaldari = $this->session->userdata('tanggaldari');
        $tanggalsampai = $this->session->userdata('tanggalsampai');
        $userid = $this->session->userdata('user_id');
        
        $from = "(SELECT
	H.id,H.tipepenerimaan,
	H.tanggalpenerimaan,
	H.tgl_terima,P.nopemesanan,DS.nama as nama_distributor,H.iddistributor,
	H.nopenerimaan,H.tipe_bayar,(CASE WHEN H.tipe_bayar='1' THEN 'CASH' ELSE 'KREDIT' END) as cara_bayar,
	(CASE WHEN H.tipe_bayar='1' THEN '' ELSE H.tanggaljatuhtempo END) as tgl_jatuh_tempo,SUM(D.totalharga) as tagihan
FROM
	tgudang_penerimaan H
	LEFT JOIN tgudang_penerimaan_detail D ON H.id=D.idpenerimaan
	LEFT JOIN tgudang_pemesanan P ON H.idpemesanan = P.id
	LEFT JOIN mdistributor DS ON DS.id=H.iddistributor
	WHERE H.tipepenerimaan IN (SELECT gudangtipe FROM musers_tipegudang WHERE iduser='$userid')
	GROUP BY H.id
	ORDER BY H.id DESC ";
        // $from .= ' v_index_tgudang_penerimaan.*';
        // $from .= ' FROM v_index_tgudang_penerimaan';
        // $from .= ' WHERE tipepemesanan IN (SELECT gudangtipe FROM musers_tipegudang WHERE iduser = '.$userid.')';
        // if ($iddistributor!='' && $iddistributor!='0') {
            // $from .= ' AND iddistributor="'.$iddistributor.'"';
        // }
        // if ($statuspenerimaan!='' && $statuspenerimaan!='0') {
            // $from .= ' AND status_penerimaan = "'.$statuspenerimaan.'"';
        // }
        // if ('' != $tanggaldari) {
            // $from .= ' AND tanggalpenerimaan >= "'.date("Y-m-d",strtotime($tanggaldari)).'"';
        // }
        // if ('' != $tanggalsampai) {
            // $from .= ' AND tanggalpenerimaan <= "'.date("Y-m-d",strtotime($tanggalsampai)).'"';
        // }
        $from .= ') tbl';
        $this->load->library('datatables');
        $this->datatables->select("
            nopenerimaan,
            tanggalpenerimaan,
            nofakturexternal,
            distributor,
            totalpsn,
            totalpernahtrm,
            totaltrm,
            totalsisa,
            totalharga,
            id,
            id
        ");
        $this->datatables->from($from);
        $this->datatables->add_column('option', '', 'id');

        return print_r($this->datatables->generate());
    }

    public function detailPenerimaan() {
        $idpenerimaan = $this->input->post('idpenerimaan');
        if ($idpenerimaan) {
            $this->load->library('datatables');
            $this->datatables->select('trmdet.*');
            $this->datatables->where('trmdet.idpenerimaan', $idpenerimaan);
            $this->datatables->from('tgudang_penerimaan_detail trmdet');

            return print_r($this->datatables->generate());
        }
    }

    public function ajaxDetail() {
        $idtipe = $this->input->post('idtipe');
        $idbarang = $this->input->post('idbarang');
        $idpenerimaan = $this->input->post('idpenerimaan');
        if ($idtipe && $idbarang && $idpenerimaan) {
            $r = $this->model->ajaxDetail($idpenerimaan, $idtipe, $idbarang);
            $this->output->set_output(json_encode($r));
        }
    }

    public function get_nopemesanan() {
        $data = $this->model->get_nopemesanan();
        $this->output->set_output(json_encode($data));
    }

    public function get_nopemesanan_detail($id) {
        if ($id) {
            $result = $this->model->get_pemesanan_detail($id);
            $data = array();
            foreach ($result as $r) {
                $row = array();
                $row['tipebarang'] = GetTipeKategoriBarang($r->idtipe);
                $row['namabarang'] = get_detail_barang($r->idtipe, $r->idbarang)->nama;
                $row['distributor'] = $r->nama;
                $row['namasatuan'] = $this->_get_namasatuan_barang($r->idtipe, $r->idbarang, $r->opsisatuan);
                $row['harga'] = $r->harga;
                $row['totalharga'] = 0;
                $row['kuantitas_sisa'] = $this->model->get_kuantitas_sisa($id, $r->idbarang, $r->idtipe);
                $row['kuantitas'] = 0;
                $row['idbarang'] = $r->idbarang;
                $row['idtipe'] = $r->idtipe;
                $row['iddistributor'] = $r->iddistributor;
                $row['opsisatuan'] = $r->opsisatuan;
                $row['nobatch'] = 0;
                $row['edit'] = "<a href='#edit' class='edit-detail'><i class='fa fa-pencil'></i></a>";
                $row['idpemesanandetail'] = $r->id;
                $data[] = $row;
            }
            $output = array('data' => $data);
            $this->output->set_output(json_encode($output));
        }
    }

    private function _get_namasatuan_barang($idtipe = 1, $idbarang = 1, $opsisatuan = 1) {
        $from = array(null, 'mdata_alkes', 'mdata_implan', 'mdata_obat', 'mdata_logistik');

        $this->db->select('satuan.nama');
        if (1 == $idtipe || 3 == $idtipe) {
            if (1 == $opsisatuan) {
                $this->db->join('msatuan satuan', 'barang.idsatuankecil = satuan.id', 'left');
            } else {
                $this->db->join('msatuan satuan', 'barang.idsatuanbesar = satuan.id', 'left');
            }
        } else {
            $this->db->join('msatuan satuan', 'barang.idsatuan = satuan.id', 'left');
        }
        $this->db->where('barang.id', $idbarang);
        $this->db->from($from[$idtipe].' barang');
        $res = $this->db->get()->row_array()['nama'];

        return $res;
    }

    public function get_satuan_barang() {
        $this->model->get_satuan_barang();
    }

    public function update() {
        $this->model->update();
    }

    public function edit_no_faktur() {
        $this->model->edit_no_faktur();
    }

    public function print_penerimaan() {
        $id = $this->uri->segment(3);
        if ($id) {
            $data = $this->model->viewHead($id);
            $data['detail'] = $this->model->viewDetail($data['id']);
            $data['title'] = 'Print Penerimaan';
            $data = array_merge($data, backend_info());
            $this->parser->parse('Tgudang_penerimaan/print_penerimaan', $data);
        }
    }

    public function view() {
        $id = $this->uri->segment(3);
        if ($id) {
            $data = $this->model->viewHead($id);
            $data['error'] = '';
            $data['title'] = 'Detail Penerimaan Gudang';
            $data['content'] = 'Tgudang_penerimaan/view';
            $data['breadcrum'] = array(array('RSKB Halmahera', '#'),
                array('Penerimaan Gudang', '#'),
                array('List', 'tgudang_penerimaan'), );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            show_404();
        }
    }

    public function edit($id) {
        if ($id) {
            $data = array(
                'id' => '',
                'nopenerimaan' => '',
                'idpemesanan' => '',
                'totalbarang' => '',
                'totalharga' => '',
            );

            $data = $this->model->viewHead($id);
            $data['detail'] = $this->model->viewDetail($data['id']);
            $data['error'] = '';
            $data['title'] = 'Edit Penerimaan Gudang';
            $data['content'] = 'Tgudang_penerimaan/manage';
            $data['breadcrum'] = array(
                array('RSKB Halmahera', '#'),
                array('Penerimaan Gudang', '#'),
                array('Edit', '#'),
            );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            show_404();
        }
    }
	function find_detail($id)
	{
		$arr = $this->model->find_detail($id);
		$this->output->set_output(json_encode($arr));
	}
	function find_detail_user($id)
	{
		$arr = $this->model->find_detail_user($id);
		$this->output->set_output(json_encode($arr));
	}
	function get_distributor()
	{
		$this->model->get_distributor();
		// $this->output->set_output(json_encode($arr));
	}

}

/* End of file Tgudang_penerimaan.php */
/* Location: ./application/controllers/Tgudang_penerimaan.php */
