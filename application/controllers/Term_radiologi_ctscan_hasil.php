<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Term_radiologi_ctscan_hasil extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Term_radiologi_ctscan_model', 'erm_radiologi_ctscan_model');
        $this->load->model('Term_radiologi_ctscan_hasil_model', 'model');
    }

    public function index($status_pemeriksaan = '#'): void
    {
        // $data_user = get_acces();
        // $user_acces_form = $data_user['user_acces_form'];

        // if (UserAccesForm($user_acces_form, ['1584'])) {
            $data = [
                'tanggal' => date("d/m/Y"),
                'status_pemeriksaan' => $status_pemeriksaan,
            ];

            $data['error'] = '';
            $data['title'] = 'Radiologi';
            $data['content'] = 'Term_radiologi_ctscan_hasil/index';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Radiologi', '#'],
                ['Hasil Pemeriksaan', 'term_radiologi_ctscan_hasil'],
            ];

            $data['tujuan_radiologi'] = $this->model->get_tujuan_radiologi();

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        // } else {
        //     redirect('page404');
        // }
    }

    public function proses($id): void
    {
        // $data_user = get_acces();
        // $user_acces_form = $data_user['user_acces_form'];

        // if (UserAccesForm($user_acces_form, ['1584'])) {
            $row = $this->erm_radiologi_ctscan_model->get_data_transaksi($id);
            $data = [
                'id' => $row->id,
                'tujuan_radiologi' => $row->tujuan_radiologi,
                'tujuan_radiologi_label' => $row->tujuan_radiologi_nama,
                'dokter_peminta_id' => $row->dokter_peminta_id,
                'diagnosa' => $row->diagnosa,
                'catatan_permintaan' => $row->catatan_permintaan,
                'tanggal_permintaan' => DMYFormat($row->waktu_permintaan),
                'waktu_permintaan' => HISTimeFormat($row->waktu_permintaan),
                'prioritas' => $row->prioritas,
                'pasien_puasa' => $row->pasien_puasa,
                'pengiriman_hasil' => $row->pengiriman_hasil,
                'alergi_bahan_kontras' => $row->alergi_bahan_kontras,
                'pasien_hamil' => $row->pasien_hamil,
                'tanggal_pemeriksaan' => DMYFormat($row->waktu_pemeriksaan),
                'waktu_pemeriksaan' => HISTimeFormat($row->waktu_pemeriksaan),
                'petugas_pemeriksaan' => $row->petugas_pemeriksaan,
                'nomor_foto' => $row->nomor_foto,
                'dokter_radiologi' => $row->dokter_radiologi,
                'jumlah_expose' => $row->jumlah_expose,
                'jumlah_film' => $row->jumlah_film,
                'qp' => $row->qp,
                'mas' => $row->mas,
                'posisi' => $row->posisi,
                'nomor_medrec' => $row->nomedrec,
                'nama_pasien' => $row->nama_pasien,
                'nomor_ktp' => $row->nomor_ktp,
                'jenis_kelamin' => $row->jenis_kelamin_label,
                'nomor_registrasi' => $row->nomor_registrasi,
                'alamat_pasien' => $row->alamat_jalan,
                'tanggal_lahir' => DMYFormat($row->tanggal_lahir),
                'umur_tahun' => $row->umur_tahun,
                'umur_bulan' => $row->umur_bulan,
                'umur_hari' => $row->umur_hari,
                'kelompok_pasien' => $row->kelompok_pasien,
                'tipe_kunjungan' => $row->tujuan_pendaftaran,
                'nama_asuransi' => $row->nama_asuransi,
                'nama_poliklinik' => $row->poliklinik,
            ];

            $data['error'] = '';
            $data['title'] = 'Upload Hasil';
            $data['content'] = 'Term_radiologi_ctscan_hasil/manage';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Radiologi', '#'],
                ['Upload Hasil', 'term_radiologi_ctscan_hasil'],
            ];

            $data['pengaturan_form'] = (array) get_row('merm_pengaturan_form_order_radiologi_ctscan');
            
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        // } else {
        //     redirect('page404');
        // }
    }

    public function save(): void
    {
		if ($this->model->update()) {
			$this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data telah disimpan.');
            redirect("term_radiologi_ctscan_hasil", 'location');
		}
    }

    public function getIndex(): void
    {
        $tujuan_radiologi = $this->input->post('tujuan_radiologi');
        $dokter_peminta = $this->input->post('dokter_peminta');
        $tanggal_dari = YMDFormat($this->input->post('tanggal_dari'));
        $tanggal_sampai = YMDFormat($this->input->post('tanggal_sampai'));
        $dokter_radiologi = $this->input->post('dokter_radiologi');
        $nomor_medrec = $this->input->post('nomor_medrec');
        $nama_pasien = $this->input->post('nama_pasien');
        $asal_pasien = $this->input->post('asal_pasien');
        $kelompok_pasien = $this->input->post('kelompok_pasien');
        $nomor_pendaftaran = $this->input->post('nomor_pendaftaran');
        $nomor_permintaan = $this->input->post('nomor_permintaan');
        $nomor_radiologi = $this->input->post('nomor_radiologi');
        $status_pemeriksaan = $this->input->post('status_pemeriksaan');
        $status_cito_expertise = $this->input->post('status_cito_expertise');
        $status_expertise = $this->input->post('status_expertise');

        $where = '';
        if ('0' != $tujuan_radiologi) {
            $where .= " AND (term_radiologi_ctscan.tujuan_radiologi) = '{$tujuan_radiologi}'";
        }
        if ('0' != $dokter_peminta) {
            $where .= " AND (term_radiologi_ctscan.dokter_peminta_id) = '{$dokter_peminta}'";
        }
        if ('' != $tanggal_dari) {
            $where .= " AND DATE(term_radiologi_ctscan.rencana_pemeriksaan) >= '{$tanggal_dari}'";
        }
        if ('' != $tanggal_sampai) {
            $where .= " AND DATE(term_radiologi_ctscan.rencana_pemeriksaan) <= '{$tanggal_sampai}'";
        }
        if ('0' != $dokter_radiologi) {
            $where .= " AND (term_radiologi_ctscan.dokter_radiologi) = '{$dokter_radiologi}'";
        }
        if ('' != $nomor_medrec) {
            $where .= " AND (mfpasien.no_medrec) = '{$nomor_medrec}'";
        }
        if ('' != $nama_pasien) {
            $where .= " AND (mfpasien.nama) LIKE '%".$nama_pasien."%'";
        }
        if ('0' != $asal_pasien) {
            $where .= " AND (tpoliklinik_pendaftaran.idtipe) = '{$asal_pasien}'";
        }
        if ('0' != $kelompok_pasien) {
            $where .= " AND (tpoliklinik_pendaftaran.idkelompokpasien) = '{$kelompok_pasien}'";
        }
        if ('' != $nomor_pendaftaran) {
            $where .= " AND (tpoliklinik_pendaftaran.nopendaftaran) = '{$nomor_pendaftaran}'";
        }
        if ('' != $nomor_permintaan) {
            $where .= " AND (term_radiologi_ctscan.nomor_permintaan) = '{$nomor_permintaan}'";
        }
        if ('' != $nomor_radiologi) {
            $where .= " AND (term_radiologi_ctscan.nomor_foto) = '{$nomor_radiologi}'";
        }
        if ('#' != $status_pemeriksaan) {
            $where .= " AND (term_radiologi_ctscan.status_pemeriksaan) = '{$status_pemeriksaan}'";
        }
        if ('#' != $status_cito_expertise) {
            $where .= " AND (term_radiologi_ctscan.prioritas) = '{$status_cito_expertise}'";
        }
        if ('#' != $status_expertise) {
            $where .= " AND (term_radiologi_ctscan.status_expertise) = '{$status_expertise}'";
        }

        $this->select = [];
        $from = "
            (
                SELECT
                    result.*,
                    hasil_pemeriksaan.hasil_pemeriksaan_id
                FROM (
                    SELECT
                        term_radiologi_ctscan.id,
                        term_radiologi_ctscan.asal_rujukan,
                        (CASE
                            WHEN term_radiologi_ctscan.asal_rujukan = 1 THEN 'Poliklinik'
                            WHEN term_radiologi_ctscan.asal_rujukan = 2 THEN 'Instalasi Gawat Darurat (IGD)'
                            WHEN term_radiologi_ctscan.asal_rujukan = 3 THEN 'Rawat Inap'
                            WHEN term_radiologi_ctscan.asal_rujukan = 4 THEN 'One Day Surgery (ODS)'
                        END) AS asal_pasien,
                        (CASE
                            WHEN term_radiologi_ctscan.asal_rujukan IN (1, 2) THEN
                                tpoliklinik_pendaftaran.nopendaftaran
                            WHEN term_radiologi_ctscan.asal_rujukan IN (3, 4) THEN
                                poliklinik_ranap.nopendaftaran
                        END) AS nomor_pendaftaran,
                        term_radiologi_ctscan.nomor_permintaan,
                        term_radiologi_ctscan.nomor_foto,
                        term_radiologi_ctscan.rencana_pemeriksaan,
                        term_radiologi_ctscan.nomor_antrian,
                        COALESCE ( tpoliklinik_pendaftaran.kode_antrian, poliklinik_ranap.kode_antrian ) AS kode_antrian,
                        COALESCE ( tpoliklinik_pendaftaran.no_medrec, poliklinik_ranap.nomor_medrec ) AS nomor_medrec,
                        COALESCE ( tpoliklinik_pendaftaran.idpasien, poliklinik_ranap.pasien_id ) AS pasien_id,
                        COALESCE ( tpoliklinik_pendaftaran.namapasien, poliklinik_ranap.nama_pasien ) AS nama_pasien,
                        COALESCE ( mpasien_kelompok.nama, poliklinik_ranap.kelompok_pasien ) AS kelompok_pasien,
                        term_radiologi_ctscan.status_pemeriksaan,
                        (CASE
                            WHEN term_radiologi_ctscan.asal_rujukan IN ( 1, 2 ) THEN
                            tpoliklinik_pendaftaran.id 
                            WHEN term_radiologi_ctscan.asal_rujukan IN ( 3, 4 ) THEN
                            poliklinik_ranap.id 
                        END) AS pendaftaran_id,
                        term_radiologi_ctscan.tujuan_radiologi,
                        term_radiologi_ctscan.jumlah_edit,
                        SUM(term_radiologi_ctscan_pemeriksaan.totalkeseluruhan) AS total_tarif,
                        referensi_prioritas.ref AS prioritas_label,
                        mdokter.nama AS dokter_radiologi,
                        term_radiologi_ctscan.prioritas,
                        term_radiologi_ctscan.status_expertise,
                        tkasir.status AS statuskasir,
                        poliklinik_ranap.id AS idpendaftaran_ranap,
                        COALESCE ( tpoliklinik_pendaftaran.id, poliklinik_ranap.idpendaftaran_rajal ) AS idpendaftaran_rajal
                    FROM
                        term_radiologi_ctscan
                        LEFT JOIN (
                        SELECT
                            trawatinap_pendaftaran.id,
                            trawatinap_pendaftaran.nopendaftaran,
                            tpoliklinik_pendaftaran.kode_antrian,
                            tpoliklinik_pendaftaran.no_medrec AS nomor_medrec,
                            tpoliklinik_pendaftaran.idpasien AS pasien_id,
                            tpoliklinik_pendaftaran.namapasien AS nama_pasien,
                            mpasien_kelompok.nama AS kelompok_pasien,
                            tpoliklinik_pendaftaran.id AS idpendaftaran_rajal
                        FROM
                            trawatinap_pendaftaran
                            INNER JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik
                            INNER JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien 
                        ) AS poliklinik_ranap ON term_radiologi_ctscan.pendaftaran_id = poliklinik_ranap.id 
                        AND term_radiologi_ctscan.asal_rujukan IN ( 3, 4 )
                        LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = term_radiologi_ctscan.pendaftaran_id AND term_radiologi_ctscan.asal_rujukan IN (1, 2)
                        LEFT JOIN mfpasien ON mfpasien.id = tpoliklinik_pendaftaran.idpasien
                        LEFT JOIN mpasien_kelompok ON mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien
                        LEFT JOIN merm_pengaturan_tujuan_radiologi ON merm_pengaturan_tujuan_radiologi.id = term_radiologi_ctscan.tujuan_radiologi 
                        LEFT JOIN term_radiologi_ctscan_pemeriksaan ON term_radiologi_ctscan_pemeriksaan.transaksi_id = term_radiologi_ctscan.id 
                        LEFT JOIN merm_referensi referensi_prioritas ON referensi_prioritas.ref_head_id = 85 AND referensi_prioritas.nilai = term_radiologi_ctscan.prioritas
                        LEFT JOIN mdokter ON mdokter.id = term_radiologi_ctscan.dokter_radiologi
                        LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
			            LEFT JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN (1,2)
                    WHERE
                        term_radiologi_ctscan.status_pemeriksaan NOT IN (1, 2, 3) AND
                        merm_pengaturan_tujuan_radiologi.tipe_layanan = '3' ".$where.'
                    GROUP BY
                        term_radiologi_ctscan.id
                ) AS result
                LEFT JOIN (
                    SELECT
                        transaksi_id,
                        MAX(id) AS hasil_pemeriksaan_id
                    FROM
                        term_radiologi_ctscan_hasil_pemeriksaan
                    GROUP BY
                        transaksi_id
                ) AS hasil_pemeriksaan ON result.id = hasil_pemeriksaan.transaksi_id
            ) AS tbl WHERE tbl.id IS NOT NULL
        ';
        
        $this->from = $from;
        $this->join = [];

        $this->order = [];
        $this->group = [];
        $this->column_search = ['nomor_permintaan'];
        $this->column_order = [];

        $list = $this->datatable->get_datatables(true);
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $result = [];

            $asal_rujukan = (in_array($r->asal_rujukan, [1, 2]) ? 'rawat_jalan' : 'rawat_inap');
            
            $result[] = $no;
            $result[] = $r->asal_pasien;
            $result[] = $r->nomor_pendaftaran;
            $result[] = $r->nomor_permintaan;
            $result[] = $r->nomor_foto;
            $result[] = HumanDateShort($r->rencana_pemeriksaan);
            $result[] = $r->nomor_antrian;
            $result[] = $r->kode_antrian;
            $result[] = $r->nomor_medrec;
            $result[] = $r->nama_pasien;
            $result[] = $r->kelompok_pasien;
            $result[] = $r->dokter_radiologi;
            $result[] = StatusERMTindakanRadiologi($r->status_pemeriksaan);
            $result[] = '<span class="label label-danger" style="width: 100%; display: block;">'.$r->prioritas_label.'</span>' . '' .StatusExpertiseRadiologi($r->status_expertise);
            $result[] = '<div class="btn-group">
                            ' . ($r->status_pemeriksaan < 6 ? '<a href="#" data-toggle="tooltip" title="Upload Hasil" class="btn btn-success btn-xs" onclick="uploadFotoPemeriksaan(' . $r->id . ')"><i class="fa fa-upload"></i></a>' : '') .'
                            ' . ($r->status_pemeriksaan < 6 ? '<a href="#" data-toggle="tooltip" title="Ubah Transaksi" class="btn btn-primary btn-xs" onclick="editTarifPemeriksaan(\'' . $asal_rujukan . '\', ' . $r->pendaftaran_id . ', ' . $r->id . ')"><i class="fa fa-pencil"></i></a>' : '') .'
                            ' . ($r->status_pemeriksaan < 5 ? '<a href="#" data-toggle="tooltip" title="Batal Transaksi" class="btn btn-danger btn-xs" onclick="batalTransaksi(' . $r->id . ')"><i class="fa fa-trash"></i></></a>' : '') .'
                            ' . ($r->status_pemeriksaan == 5 && $r->status_expertise == 0 ? '<a href="#" data-toggle="tooltip" title="Langsung Expertise" class="btn btn-success btn-xs" onclick="merubahStatusExpertise('.$r->id.', 1)"><i class="fa fa-check"></i></a>' : '') .'
                            ' . ($r->status_pemeriksaan == 5 && $r->status_expertise == 0 ? '<a href="#" data-toggle="tooltip" title="Menolak Expertise" class="btn btn-warning btn-xs" onclick="merubahStatusExpertise('.$r->id.', 2)"><i class="fa fa-ban"></i></a>' : '') .'
                            ' . ($r->status_pemeriksaan == 5 && $r->status_expertise == 2  ? '<a href="#" data-toggle="tooltip" title="Kembali Expertise" class="btn btn-info btn-xs" onclick="merubahStatusExpertise('.$r->id.', 3)"><i class="fa fa-reply"></i></a>' : '') .'
                            ' . ($r->status_pemeriksaan == 5 && $r->prioritas == 2 ? '<a href="#" data-toggle="tooltip" title="Urgent" class="btn btn-danger btn-xs" onclick="merubahPrioritasUrgent('.$r->id.')"><i class="fa fa-warning"></i></a>' : '') .'
                            ' . ($r->status_pemeriksaan >= 5 ? '<a href="#" data-toggle="modal" data-target="#modal-kirim-hasil-upload" class="btn btn-danger btn-xs" onclick="kirimEmailHasilUpload('.$r->id.', '.$r->pasien_id.', '.$r->tujuan_radiologi.')"><i class="fa fa-share-square"></i></a>' : '') .'
                            ' . ($r->status_pemeriksaan == 6 ? '<a href="#" data-toggle="modal" data-target="#modal-kirim-hasil" class="btn btn-primary btn-xs" onclick="kirimEmailHasilPemeriksaan('.$r->id.', '.$r->pasien_id.', '.$r->tujuan_radiologi.')"><i class="fa fa-send"></i></a>' : '') .'
                            ' . ($r->status_pemeriksaan == 7 ? '<a href="#" data-toggle="tooltip" title="Lihat Hasil" class="btn btn-default btn-xs" onclick="lihatFotoPemeriksaan('.$r->id.')"><i class="fa fa-eye"></i></a>' : '') .'
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-success btn-xs btn-block dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-print"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                '. ($r->status_pemeriksaan >= 2 ? '<li class=""><a href="#" onclick="cetakPemeriksaan(' . $r->id . ')">Pemeriksaan</a></li>' : '') .'
                                '. ($r->status_pemeriksaan >= 2 ? '<li class=""><a href="#" onclick="cetakBuktiPemeriksaan(' . $r->id . ')">Bukti Pemeriksaan</a></li>' : '') .'
                                '. ($r->status_pemeriksaan >= 2 ? '<li class=""><a href="#" onclick="cetakBuktiPemeriksaanSmall(' . $r->id . ')">Bukti Pemeriksaan (Small)</a></li>' : '') .'
                                '. ($r->status_pemeriksaan >= 2 ? '<li class=""><a href="#" onclick="cetakBuktiPengambilanPemeriksaan(' . $r->id . ')">Bukti Pengambilan Pemeriksaan</a></li>' : '') .'
                                '. ($r->status_pemeriksaan >= 2 ? '<li class=""><a href="#" onclick="cetakBuktiPengambilanPemeriksaanSmall(' . $r->id . ')">Bukti Pengambilan Pemeriksaan (Small)</a></li>' : '') .'
                                '. ($r->status_pemeriksaan >= 6 ? '<li class=""><a href="#" onclick="cetakHasilPemeriksaan(' . $r->hasil_pemeriksaan_id . ', \'FORMAT_1\')">Hasil Pemeriksaan - Format 1</a></li>' : '') .'
                                '. ($r->status_pemeriksaan >= 6 ? '<li class=""><a href="#" onclick="cetakHasilPemeriksaan(' . $r->hasil_pemeriksaan_id . ', \'FORMAT_2\')">Hasil Pemeriksaan - Format 2</a></li>' : '') .'
                            </ul>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-warning btn-block btn-xs dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                                <span class="fa fa-ellipsis-v"></span>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                '. ($r->status_pemeriksaan >= 7 ? '<li class=""><a href="#" onclick="kirimUlangEmailHasilPemeriksaan('.$r->id.')" data-toggle="modal" data-target="#modal-kirim-ulang-hasil">Kirim Email Ulang</a></li>' : '') .'
                                '. ($r->status_pemeriksaan >= 7 ? '<li class=""><a href="#" onclick="riwayatPengirimanEmailHasilPemeriksaan(' . $r->id . ')">Riwayat Pengiriman</a></li>' : '') .'
                                '. ($r->status_pemeriksaan >= 6 ? '<li class=""><a href="#" onclick="riwayatHasilPemeriksaan(' . $r->id . ')">Riwayat Hasil</a></li>' : '') .'
                                '. ($r->status_pemeriksaan >= 6 ? '<li class=""><a href="#" onclick="editFotoPemeriksaan(' . $r->id . ')">Edit Hasil</a></li>' : '') .'
                                <li class=""><a href="#" onclick="riwayatRadiologi(' . $r->pendaftaran_id . ', ' . $r->pasien_id . ')">Riwayat Radiologi</a></li>
                                <li class=""><a href="#" onclick="dataRadiologi(\'' . $asal_rujukan . '\', ' . $r->pendaftaran_id . ', ' . $r->id . ')">Data Radiologi</a></li>
                                <li class=""><a href="#" onclick="stikerIdentitas(' . $r->id . ')">Stiker Identitas</a></li>
                                <li class=""><a href="#" onclick="labelTabungDarah(' . $r->id . ')">Label Tabung Darah</a></li>
                                <li class=""><a href="#" onclick="labelRadiologi(' . $r->id .')">Label Radiologi</a></li>
                                '.($r->statuskasir != 2 ? '<li class=""><a href="#" data-toggle="modal" data-target="#modal-ubah-kelas-tarif" data-transaksi-id="' . $r->id . '" data-total-tarif="' . $r->total_tarif . '" id="ubah-kelas-tarif">Ubah Kelas Tarif</a></li>' : '').'
                                <li class=""><a href="#" onclick="inputBMHPTagihan(' . $r->id . ')">Input BMHP Tagihan</a></li>
                                <li class=""><a href="#" onclick="inputBMHPNonTagihan(' . $r->id . ')">Input BMHP Non Tagihan</a></li>
                                '.($r->idpendaftaran_rajal ? '<li class=""><a href="' . site_url().'tpendaftaran_poli_ttv/tindakan/' . $r->idpendaftaran_rajal . '/erm_rj" target="_blank">ERM Rawat Jalan</a></li>' : ''). '
                                '.($r->idpendaftaran_ranap ? '<li class=""><a href="' . site_url().'tpendaftaran_ranap_erm/tindakan/' . $r->idpendaftaran_ranap . '/erm_ri" target="_blank">ERM Rawat Inap</a></li>' : ''). '
                            </ul>
                        </div>';

            $data[] = $result;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(true),
            'recordsFiltered' => $this->datatable->count_all(true),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function merubah_status_expertise($id, $status_expertise): void
    {
        $this->db->set('status_expertise', $status_expertise);
        $this->db->where('id', $id);
        if ($this->db->update('term_radiologi_ctscan')) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function merubah_prioritas_urgent($id): void
    {
        $this->db->set('prioritas', 2);
        $this->db->where('id', $id);
        if ($this->db->update('term_radiologi_ctscan')) {
            $response = [
                'status' => true,
                'message' => 'success',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'error',
            ];
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display()
        ;

        exit;
    }

    public function get_data_pemeriksaan($transaksi_id) {
        $daftar_pemeriksaan = $this->model->get_daftar_pemeriksaan($transaksi_id);
        
        $html = '';
        foreach ($daftar_pemeriksaan as $index => $row) {
            $html .= '<tr>';
            $html .= '<td class="text-center">' . ($index + 1) . '</td>';
            $html .= '<td>' . $row->namapemeriksaan . '</td>';
            $html .= '<td>' . StatusUploadFotoRadiologi($row->status_upload_foto) . '</td>';
            $html .= '<td class="text-center">';
            $html .= '<button type="button" class="btn btn-xs btn-primary btn-open" data-pemeriksaan-id="' . $row->id . '" data-pemeriksaan-nama="' . $row->namatarif . '" style="width: 100%;"><i class="fa fa-arrow-circle-o-right"></i></button>';
            $html .= '<button type="button" class="btn btn-xs btn-success" style="width: 100%;"><i class="fa fa-print"></i></button>';
            $html .= '</td>';
            $html .= '</tr>';
        }

        header('Content-Type: text/html');
        echo $html;
    }

    public function upload_foto_radiologi_pemeriksaan()
    {
        $folderPath = "assets/upload/foto_radiologi_ctscan";
        if (!file_exists($folderPath)) {
            mkdir($folderPath, 0755, true);
        }

        $files = array();
        foreach ($_FILES as $file) {
            if ($file['name'] != '') {
                $config['upload_path'] = './assets/upload/foto_radiologi_ctscan/';
                $config['allowed_types'] = '*';

                $files[] = $file['name'];

                $this->upload->initialize($config);

                if ($this->upload->do_upload('file')) {
                    $file_upload = $this->upload->data();
                    $table = 'term_radiologi_ctscan_pemeriksaan_file';

                    $data = array();
                    $data['pemeriksaan_id']  = $this->input->post('pemeriksaan_id');
                    $data['filename'] = $file_upload['file_name'];
                    $data['size'] = formatSizeUnits($file['size']);
                    $data['upload_at'] = date('Y-m-d H:i:s');
                    $data['upload_by'] = $this->session->userdata('user_id');
                    $data['upload_name'] = $this->session->userdata('user_name');

                    $this->db->insert($table, $data);

                    // Check if there are any files related to the pemeriksaan_id
                    $pemeriksaan_id = $this->input->post('pemeriksaan_id');
                    $file_count = $this->db->where('pemeriksaan_id', $pemeriksaan_id)->count_all_results('term_radiologi_ctscan_pemeriksaan_file');

                    // Update status_upload_foto based on the file count
                    $status_upload_foto = ($file_count > 0) ? 1 : 0;
                    $this->db->where('id', $pemeriksaan_id);
                    $this->db->update('term_radiologi_ctscan_pemeriksaan', array('status_upload_foto' => $status_upload_foto));

                    // Update status_pemeriksaan based on the file count
                    $this->updateStatusPemeriksaan($pemeriksaan_id);
                    
                    return true;
                } else {
                    print_r($this->upload->display_errors());
                    exit();

                    return false;
                }
            } else {
                return true;
            }
        }
    }

	public function get_foto_radiologi_pemeriksaan($pemeriksaanId)
    {		
		$arr['detail'] = $this->model->get_foto_radiologi_pemeriksaan($pemeriksaanId);
		$this->output->set_output(json_encode($arr));
	}

    public function remove_foto_radiologi_pemeriksaan($fileId, $pemeriksaanId)
    {
        $this->db->where('id', $fileId);
        $query = $this->db->get('term_radiologi_ctscan_pemeriksaan_file');
        $row = $query->row();

        if ($query->num_rows() > 0) {
            $this->db->where('id', $fileId);
            if ($this->db->delete('term_radiologi_ctscan_pemeriksaan_file')) {
                if (file_exists('./assets/upload/foto_radiologi_ctscan/'.$row->filename) && $row->filename !='') {
                    unlink('./assets/upload/foto_radiologi_ctscan/'.$row->filename);
                }
            }
        }

        // Check if there are any files related to the pemeriksaan_id
        $file_count = $this->db->where('pemeriksaan_id', $pemeriksaanId)->count_all_results('term_radiologi_ctscan_pemeriksaan_file');

        // Update status_upload_foto based on the file count
        $status_upload_foto = ($file_count > 0) ? 1 : 0;
        $this->db->where('id', $pemeriksaanId);
        $this->db->update('term_radiologi_ctscan_pemeriksaan', array('status_upload_foto' => $status_upload_foto));

        // Update status_pemeriksaan based on the file count
        $this->updateStatusPemeriksaan($pemeriksaanId);

        return true;
    }

    public function updateStatusPemeriksaan($pemeriksaan_id)
    {
        $query = "
            SELECT
                term_radiologi_ctscan.id AS transaksi_id,
                term_radiologi_ctscan_pemeriksaan.id AS pemeriksaan_id,
                COUNT(term_radiologi_ctscan_pemeriksaan_file.id) AS count
            FROM
                term_radiologi_ctscan_pemeriksaan_file
            RIGHT JOIN term_radiologi_ctscan_pemeriksaan ON term_radiologi_ctscan_pemeriksaan.id = term_radiologi_ctscan_pemeriksaan_file.pemeriksaan_id
            LEFT JOIN term_radiologi_ctscan ON term_radiologi_ctscan.id = term_radiologi_ctscan_pemeriksaan.transaksi_id
            WHERE term_radiologi_ctscan_pemeriksaan.id = ?
        ";

        $result = $this->db->query($query, $pemeriksaan_id)->row();

        if ($result) {
            $transaksi_id = $result->transaksi_id;
            $count = $result->count;

            $currentStatus = $this->db->get_where('term_radiologi_ctscan', array('id' => $transaksi_id))->row()->status_pemeriksaan;

            if ($count > 0 && $currentStatus <= 5) {
                $this->db->set('status_pemeriksaan', '5');
                $this->db->where('id', $transaksi_id);
                $this->db->update('term_radiologi_ctscan');
            } elseif ($count == 0 && $currentStatus == 5) {
                $this->db->set('status_pemeriksaan', '4');
                $this->db->where('id', $transaksi_id);
                $this->db->update('term_radiologi_ctscan');
            }
        }
    }
}
