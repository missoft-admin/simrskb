<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mruangan extends CI_Controller {

	/**
	 * Ruangan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mruangan_model');
  }

	function index(){
		
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Ruangan';
		$data['content'] 		= 'Mruangan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Ruangan",'#'),
									    			array("List",'mruangan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'idtipe' 				=> '',
			'status' 				=> '',
			'st_tampil' 				=> '1',
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Ruangan';
		$data['content'] 		= 'Mruangan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Ruangan",'#'),
									    			array("Tambah",'mruangan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Mruangan_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'idtipe' 				=> $row->idtipe,
					'status' 				=> $row->status,
					'st_tampil' 				=> $row->st_tampil
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Ruangan';
				$data['content']	 	= 'Mruangan/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Ruangan",'#'),
											    			array("Ubah",'mruangan')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mruangan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mruangan');
		}
	}

	function delete($id){
		
		$this->Mruangan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mruangan','location');
	}
	function update_tampil(){
		$id=$this->input->post('id');
		$st_tampil=$this->input->post('st_tampil');
		$this->db->where('id',$id);
		
		$hasil=$this->db->update('mruangan',array('st_tampil'=>$st_tampil));
		
		$this->output->set_output(json_encode($hasil));
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('idtipe', 'Tipe', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mruangan_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mruangan','location');
				}
			} else {
				if($this->Mruangan_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mruangan','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mruangan/manage';

		if($id==''){
			$data['title'] = 'Tambah Ruangan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Ruangan",'#'),
															array("Tambah",'mruangan')
													);
		}else{
			$data['title'] = 'Ubah Ruangan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Ruangan",'#'),
															array("Ubah",'mruangan')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$this->from   = 'mruangan';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'id' => 'DESC'
			);
			$this->group  = array();

			$this->column_search   = array('nama');
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->nama.' &nbsp;&nbsp;'.($r->st_tampil=='1'?text_success('<i class="fa fa-eye"></i> TAMPIL'):text_danger('<i class="fa fa-eye-slash"></i> TIDAK TAMPIL'));
					$row[] = GetTipeRuangan($r->idtipe);
					$aksi = '<div class="btn-group">';
		            if (UserAccesForm($user_acces_form,array('185'))){
		                $aksi .= '<a href="'.site_url().'mruangan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
		            }
		            if (UserAccesForm($user_acces_form,array('186'))){
		                $aksi .= '<a href="#" data-urlindex="'.site_url().'mruangan" data-urlremove="'.site_url().'mruangan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
		            }
					if ($r->st_tampil=='1'){
		                $aksi .= '<button data-toggle="tooltip" title="Jangan Tampilkan" onclick="set_tampil('.$r->id.',0)" class="btn btn-warning btn-sm"><i class="fa fa-eye-slash"></i></button>';
		            }else{
		                $aksi .= '<button data-toggle="tooltip" title="Tampilkan" onclick="set_tampil('.$r->id.',1)" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>';
						
					}
		            $aksi .= '</div>';
  					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
}
