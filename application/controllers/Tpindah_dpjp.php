<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpindah_dpjp extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpindah_dpjp_model');
		$this->load->model('Tpendaftaran_poli_pasien_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->helper('path');
		
  }
	function index($tab='1'){
		$log['path_tindakan']='tpindah_dpjp';
		$this->session->set_userdata($log);
		$user_id=$this->session->userdata('user_id');
		// print_r($this->session->userdata('path_tindakan'));exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$q="SELECT H.id as login_mppa_id,H.pegawai_id as login_iddokter,H.pegawai_id as iddokter FROM mppa H WHERE H.user_id='$user_id' AND H.tipepegawai='2' AND H.staktif='1'";
		// $login_iddokter=$this->db->query($q)->row('login_iddokter');	
		if (UserAccesForm($user_acces_form,array('1990'))){
			$data = $this->db->query($q)->row_array();	
			if ($data){
				
				$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("-30 days"));
			$date2= date_format($date2,"d/m/Y");
				
				$data['idpoli'] 			= '#';
				$data['namapasien'] 			= '';
				$data['tab'] 			= $tab;
				
				$data['tanggal_1'] 			= $date2;
				$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
				$data['error'] 			= '';
				$data['title'] 			= 'My Pasien';
				$data['content'] 		= 'Tpindah_dpjp/index';
				$data['breadcrum'] 	= array(
													  array("RSKB Halmahera",'#'),
													  array("Tindakan Poliklinik",'#'),
													  array("My Pasien",'tpendaftaran_poli_pasien')
													);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				redirect('page404');
			}
		}else{
			redirect('page404');
		}
	}
	
	function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
						
			$idpoli =$this->input->post('idpoli');
			$iddokter =$this->input->post('iddokter');
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$pencarian =$this->input->post('pencarian');
			$tab =$this->input->post('tab');
			if ($tanggal_1 !=''){
				$where .=" AND DATE(TK.tanggal_input) >='".YMDFormat($tanggal_1)."' AND DATE(TK.tanggal_input) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(TK.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($pencarian!=''){
				$where .=" AND ((H.namapasien LIKE '%".$pencarian."%')";
				$where .=" OR (H.no_medrec LIKE '%".$pencarian."%'))";
			}
			if ($tab=='2'){
				$where .=" AND ((TK.st_jawab = '0' AND TK.iddokter_awal='$iddokter') OR (TK.st_jawab_baru = '0' AND TK.iddokter_baru='$iddokter') )";
			}
			if ($tab=='3'){
				$where .=" AND ((TK.st_jawab = '1' AND TK.iddokter_awal='$iddokter') OR (TK.st_jawab_baru = '1' AND TK.iddokter_baru='$iddokter') )";
			}
			
			// print_r($tab);exit;
			$this->select = array();
			
			$from="
				(
					SELECT TK.assesmen_id,TK.pendaftaran_id_ranap, TK.iddokter_awal,TK.iddokter_baru,TK.st_jawab,TK.jawaban,TK.tanggal_jawab,TK.tandatangan_pembuat,TK.st_jawab_baru,TK.jawaban_baru,TK.tanggal_jawab_baru, 
					MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
					,MK.nama as nama_kelompok,MR.nama as nama_rekanan,MDP.nama as nama_dokter_poli
					,H.*,MKL.nama as nama_kelas 
					,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header
					,MB.nama as nama_bed,TP.tanggal,MRU.nama as nama_ruangan,AH.kodeantrian as kode_antrian
					,UH.name as nama_hapus,GROUP_CONCAT(MDHP.nama SEPARATOR ', ') as nama_dokter_pendamping
					FROM tranap_pindah_dpjp TK
					INNER JOIN trawatinap_pendaftaran H ON H.id=TK.pendaftaran_id_ranap
					LEFT JOIN trawatinap_pendaftaran_dpjp HP ON HP.pendaftaran_id=H.id
					LEFT JOIN mdokter MDHP ON MDHP.id=HP.iddokter
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=H.idpoliklinik
					LEFT JOIN mruangan MRU ON MRU.id=H.idruangan
					LEFT JOIN mkelas MKL ON MKL.id=H.idkelas
					LEFT JOIN antrian_harian AH ON AH.id=H.antrian_id
					LEFT JOIN mdokter MDP ON MDP.id=TP.iddokter
					LEFT JOIN mdokter MD ON MD.id=H.iddokterpenanggungjawab
					LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik_asal
					LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
					LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
					LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
					LEFT JOIN mbed MB on MB.id=H.idbed						
					LEFT JOIN musers UH on UH.id=H.deleted_by	
					LEFT JOIN (
							".get_alergi_sql()."
						) A ON A.idpasien=H.idpasien				
					WHERE TK.status_assemen='2' AND (TK.iddokter_awal='$iddokter' OR TK.iddokter_baru='$iddokter')	".$where."		
					GROUP BY TK.assesmen_id

					ORDER BY H.tanggaldaftar DESC
				) as tbl
			";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		  $pedamping='';
		  if ($r->nama_dokter_pendamping){
			  $pedamping='<div class="text-wrap text-danger"><i class="fa fa-user-md"></i> PENDAMPING : '.($r->nama_dokter_pendamping).'</div>';
		  }
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  $btn_setuju='';
		  $st_terima_pindah=0;
		  if ($r->iddokter_awal==$iddokter){//Pindah Ke Dokter Lain
			  $st_terima_pindah='1';
			  if ($r->st_jawab=='0'){
				  if (UserAccesForm($user_acces_form,array('1991'))){
				  $btn_setuju .='<button class="btn btn-xs btn-success btn-block" onclick="setuju_pindah('.$r->assesmen_id.')" type="button"><i class="fa fa-check"></i> SETUJU</button>';
				  }
				  if (UserAccesForm($user_acces_form,array('1992'))){
				  $btn_setuju .='<button class="btn btn-xs btn-danger btn-block" onclick="tolak_pindah('.$r->assesmen_id.')" type="button"><i class="fa fa-times"></i> TOLAK</button>';
				  }
			  }
		  }
		  if ($r->iddokter_baru==$iddokter){//Pindah Ke Pribadi
			  $st_terima_pindah='2';
			  if ($r->st_jawab_baru=='0'){
				  if (UserAccesForm($user_acces_form,array('1991'))){
				  $btn_setuju .='<button class="btn btn-xs btn-success btn-block" onclick="setuju_terima('.$r->assesmen_id.')" type="button"><i class="fa fa-check"></i> SETUJU TERIMA</button>';
				  }
				  if (UserAccesForm($user_acces_form,array('1992'))){
				  $btn_setuju .='<button class="btn btn-xs btn-danger btn-block" onclick="tolak_terima('.$r->assesmen_id.')" type="button"><i class="fa fa-times"></i> TOLAK</button>';
				  }
			  }
		  }
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="push-5-t"><a href="'.base_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_dpjp/input_pindah_dpjp/'.$r->assesmen_id.'" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block btn-primary btn-xs menu_click"><i class="si si-doc"></i> '.($r->st_jawab=='0'?'LIHAT DOKUMENT':'LIHAT DOKUMENT').'</a> </div>
									<div class="push-5-t">'.$btn_setuju.'</div>
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
										<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
									</div>
									<div class="text-primary font-s13 push-5-t">'.$r->nopendaftaran_poli.' | '.HumanDateShort($r->tanggal).' | '.GetAsalRujukanR($r->idtipe_asal).' | '.$r->nama_poli.' | '.$r->nama_dokter_poli.'</div>
									<div class="text-primary font-s13 push-5-t"></div>
									
									</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_5='';
		 
          $result[] = $btn_5;
		  $div_terima='';
		  $div_hapus='';
		  if ($r->st_terima_ranap=='1'){
			  $div_terima='<div class="text-primary text-uppercase"><i class="fa fa-bed"></i> Tanggal Terima : '.HumanDateLong($r->tanggal_terima).'</div>';
			  $div_terima.='<div class="text-primary text-uppercase"><i class="fa fa-user"></i> User Terima : '.($r->user_nama_terima).'</div>';
		  }
		  if ($r->status=='0'){
			  $div_hapus='<div class="text-danger text-uppercase"><i class="fa fa-trash"></i> Tanggal Batal : '.HumanDateLong($r->deleted_date).'</div>';
			  $div_hapus.='<div class="text-danger text-uppercase"><i class="fa fa-user"></i> User  : '.($r->nama_hapus).'</div>';
		  }
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class=" text-uppercase">'.($r->nama_ruangan).' - '.$r->nama_kelas.' - '.$r->nama_bed.'</div>
									<div class=""><i class="fa fa-user-md"></i> DPJP : '.($r->nama_dokter).'</div>
									'.$pedamping.'
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->nama_rekanan?$r->nama_rekanan:$r->nama_kelompok).'</div>
									'.$div_terima.'
									'.$div_hapus.'
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_status_jawab='';
		  if ($st_terima_pindah=='1'){//PINDAH
			  $btn_status_jawab=$this->status_jawaban($r->jawaban,$st_terima_pindah);
		  }else{
			  $btn_status_jawab=$this->status_jawaban($r->jawaban_baru,$st_terima_pindah);
		  }
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.($btn_status_jawab).'</div>
									<div class="h4 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggal).'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function status_jawaban($jawaban,$st_terima_pindah){
	  $btn_jawaban='';
	  if ($jawaban=='0'){
		  $btn_jawaban='<button class="btn btn-xs btn-warning btn-block" type="button">BELUM DIJAWAB</button>';
	  }
	  if ($jawaban=='1'){
		  $btn_jawaban='<button class="btn btn-xs btn-success btn-block" type="button">SETUJU</button>';
	  }
	  if ($jawaban=='2'){
		  $btn_jawaban='<button class="btn btn-xs btn-danger btn-block" type="button">MENOLAK</button>';
	  }
	  if ($st_terima_pindah=='1'){
		  $btn_jawaban .='<button class="btn btn-xs btn-danger btn-block" type="button">PINDAH DPJP</button>';
	  }
	  if ($st_terima_pindah=='2'){
		  $btn_jawaban .='<button class="btn btn-xs btn-info btn-block" type="button">DPJP BARU</button>';
	  }
	  return $btn_jawaban;
  }
  function setuju_pindah(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'st_jawab' => 1,
		'jawaban' => 1,
		'tanggal_jawab' => date('Y-m-d H:i:s'),
	  );
	  // print_r($data);exit;
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_pindah_dpjp',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function tolak_pindah(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'st_jawab' => 1,
		'jawaban' => 2,
		'tanggal_jawab' => date('Y-m-d H:i:s'),
	  );
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_pindah_dpjp',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function setuju_terima(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'st_jawab_baru' => 1,
		'jawaban_baru' => 1,
		'tanggal_jawab_baru' => date('Y-m-d H:i:s'),
	  );
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_pindah_dpjp',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function tolak_terima(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'st_jawab_baru' => 1,
		'jawaban_baru' => 2,
		'tanggal_jawab_baru' => date('Y-m-d H:i:s'),
	  );
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_pindah_dpjp',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function kirim_jawaban(){
		get_ppa_login();
		$pindah_dpjp_id=$this->input->post('pindah_dpjp_id');
		$data=array(
			'pindah_dpjp_jawaban' => $this->input->post('pindah_dpjp_jawaban'),
			'pindah_dpjp_anjuran' => $this->input->post('pindah_dpjp_anjuran'),

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$data['jawaban_ppa']=$login_ppa_id;
		$data['jawaban_date']=date('Y-m-d H:i:s');
		
		$data['st_jawab']=1;
		// exit;
		$this->db->where('pindah_dpjp_id',$pindah_dpjp_id);
		$result=$this->db->update('tranap_pindah_dpjp',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
}	
