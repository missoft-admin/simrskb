<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tmonitoring_bed extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tmonitoring_bed_model');
		$this->load->model('Tpendaftaran_poli_ttv_model','ttv_model');
		$this->load->helper('path');
		
  }

  function index($tab='1'){
	    $log['path_tindakan']='tmonitoring_bed';
		$this->session->set_userdata($log);
		get_ppa_login();
		// print_r($this->session->userdata());exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1598'))){
			// print_r($data);exit;	
			// $data['list_ruangan'] 			= $this->ttv_model->list_ruangan();
			$data['list_kelas'] 			= $this->Tmonitoring_bed_model->list_kelas();
			$data['list_dokter'] 			= $this->ttv_model->list_dokter();
			$data['list_ruang'] 			= $this->Tmonitoring_bed_model->list_ruang();
			
			$data['idkelas'] 			= '#';
			$data['ruangan_id'] 			= '#';
			$data['idbed'] 			= '#';
			$data['namapasien'] 			= '';
			$data['tab'] 			= $tab;
			
			$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
			$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
			$data['error'] 			= '';
			$data['title'] 			= 'Perawat';
			$data['content'] 		= 'Tmonitoring_bed/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Tindakan Poliklinik",'#'),
												  array("Perawat",'tmonitoring_bed')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function getIndex_all()	{
		$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// tipe_pemilik:tipe_pemilik,status:status,nama:nama
		$where='';
		$idbed =$this->input->post('idbed');
		$idkelas =$this->input->post('idkelas');
		$idruangan =$this->input->post('idruangan');


		if ($idruangan!='#'){
			$where .=" AND (M.idruangan) = '$idruangan'";
		}
		if ($idkelas!='#'){
			$where .=" AND (M.idkelas) = '$idkelas'";
		}
		if ($idbed!='#'){
			$where .=" AND (M.id) = '$idbed'";
		}

		// print_r($tab);exit;
		$this->select = array();
		$from="
				(
					SELECT MR.nama as nama_ruang,MR.tarif,MT.total,MK.nama as nama_kelas,MBD.mfasilitas_id,F.jml_foto,MB.nama as nama_fasilitas
					,M.* 
					FROM mbed M
					INNER JOIN (
						SELECT M.id,M.nama 
						,CASE 
						WHEN M.id=1 THEN S.truang_perawatan
						WHEN M.id=2 THEN S.truang_hcu
						WHEN M.id=3 THEN S.truang_icu
						WHEN M.id=4 THEN S.truang_isolasi
						END as tarif
						FROM mruangan M,mtarif_harga_kamar S

						WHERE M.`status`='1' AND M.idtipe='1'
					) MR ON MR.id=M.idruangan
					LEFT JOIN mkelas MK ON MK.id=M.idkelas
					LEFT JOIN mtarif_ruangperawatan_detail MT ON MT.idtarif=MR.tarif AND MT.kelas=M.idkelas
					LEFT JOIN mfasilitas_bed_detail MBD ON MBD.mbed_id=M.id
					LEFT JOIN (
						SELECT mfasilitas_id, COUNT(id) as jml_foto FROM mfasilitas_bed_foto GROUP BY mfasilitas_id
					) F ON F.mfasilitas_id=MBD.mfasilitas_id
					LEFT JOIN mfasilitas_bed MB ON MB.id=MBD.mfasilitas_id
					WHERE M.`status`='1' ".$where."
					ORDER BY M.idruangan,M.idkelas ASC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array();
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$btn_foto='';
			$btn_daftar='';
			$btn_res='';
			$btn_ref='';
			if ($r->jml_foto>0){
			  $btn_foto .='<button class="btn btn-xs btn-warning" onclick="set_modal_fasilitas('.$r->mfasilitas_id.')" type="button" title="Foto Fasilitas"><i class="fa fa-image"></i></button>';
			}
			  $btn_daftar .='<button class="btn btn-xs btn-primary" title="Daftarkan Rawat Inap"><i class="fa fa-pencil"></i></button>';
			  $btn_res .='<button class="btn btn-xs btn-info" title="Reservasi Rawat Inap"><i class="fa fa-address-book-o"></i></button>';
			  $btn_ref .='<button class="btn btn-xs btn-danger" title="Set Tidak Bisa Digunakan"><i class="fa fa-chain-broken"></i></button>';
			$aksi='
			<div class="btn-group btn-group-xs" role="group">
				'.$btn_foto.'
				'.$btn_daftar.'
				'.$btn_res.'
				'.$btn_ref.'
			</div>
			';
			$result = array();
			$result[] =$aksi;
			$result[] =$r->nama_ruang;
			$result[] =$r->nama_kelas;
			$result[] ='<button class="btn btn-xs btn-success btn-block "><i class="fa fa-bed pull-left"></i> '.$r->nama.'</button>';
			$result[] =number_format($r->total,0);
			$result[] ='';
			$result[] ='';

			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
  }
	function get_bed(){
		$where='';
		$idruangan=$this->input->post('idruangan');
		$idkelas=$this->input->post('idkelas');
		if ($idruangan!='#'){
			$where .=" AND H.idruangan='$idruangan'";
		}
		if ($idkelas!='#'){
			$where .=" AND H.idkelas='$idkelas'";
		}
		$q="SELECT H.*,MK.nama as nama_kelas 
		FROM mbed H
		LEFT JOIN mkelas MK ON MK.id=H.idkelas 
		WHERE H.`status`='1' ".$where." ORDER BY H.id ASC";
		$list_data=$this->db->query($q)->result();
		$opsi='';
		if ($list_data){
			foreach ($list_data as $r){
				$opsi .='<option value='.$r->id.'>'.$r->nama_kelas.' - '.$r->nama.'</option>';
			}
		}
		$data['detail']=$opsi;
		$this->output->set_output(json_encode($data));
	}
	function get_fasilitas(){
		$where='';
		$mfasilitas_id=$this->input->post('mfasilitas_id');
		
		$q="SELECT H.*,M.nama as nama_fasilitas FROM `mfasilitas_bed_foto` H
			INNER JOIN mfasilitas_bed M ON M.id=H.mfasilitas_id
			WHERE H.mfasilitas_id='$mfasilitas_id'";
		$list_data=$this->db->query($q)->result();
		$opsi='';
		$uploadDir = './assets/upload/foto_fasilitas/';
		if ($list_data){
			foreach ($list_data as $r){
				$file_gambar = base_url().'assets/upload/foto_fasilitas/'.$r->filename;
				$opsi .='<div class="col-lg-6 animated fadeIn">
								<div class="img-container">
									<img class="img-responsive" src="'.$uploadDir.$r->filename.'" alt="">
									<div class="img-options">
										<div class="img-options-content">
											<h3 class="font-w400 text-white push-5">'.$r->nama_fasilitas.'</h3>
											<h4 class="h6 font-w400 text-white-op push-15">'.$r->size.'</h4>
											<a class="btn btn-sm btn-default" href="'.$file_gambar.'" target="_blank"><i class="fa fa-eye"></i> Lihat</a>
										</div>
									</div>
								</div>
							</div>';
			}
		}
		$data['detail']=$opsi;
		$this->output->set_output(json_encode($data));
	}
}	
