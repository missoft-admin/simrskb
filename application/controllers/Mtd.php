<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtd extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtd_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data=$this->Mtd_model->get_td();
		// print_r($data);exit;
		if (UserAccesForm($user_acces_form,array('1617'))){
			
			$data['tab'] 			= $tab;
			$data['error'] 			= '';
			$data['title'] 			= 'Referensi Tekanan Darah';
			$data['content'] 		= 'Mtd/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Referensi Tekanan Darah",'mtd')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function update_satuan(){
		$satuan_td=$this->input->post('satuan_td');
		$data=array(
			'satuan_td'=>$this->input->post('satuan_td'),
			
		);
		$hasil=$this->db->update('mtd_satuan',$data);
		
		json_encode($hasil);
	}

  function simpan_td(){
		$td_id=$this->input->post('td_id');
		$data=array(
			'td_1'=>$this->input->post('td_1'),
			'td_2'=>$this->input->post('td_2'),
			'kategori_td'=>$this->input->post('kategori_td'),
			'warna'=>$this->input->post('warna'),
			'staktif'=>1,
		);
		if ($td_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mtd_sistole',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$td_id);
		    $hasil=$this->db->update('mtd_sistole',$data);
		}
		  
		  json_encode($hasil);
	}
	
	
	function load_td()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
							FROM `mtd_sistole` H
							where H.staktif='1'
							ORDER BY H.td_1


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('td_1','td_1','kategori_td');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->td_1.' - '.$r->td_2);
          $result[] = $r->kategori_td;
          $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'" type="button" >'.$r->warna.'</button>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('1619'))){
		  $aksi .= '<button onclick="edit_td('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		   if (UserAccesForm($user_acces_form,array('1620'))){
		  $aksi .= '<button onclick="hapus_td('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_td(){
	  $td_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$td_id);
		$hasil=$this->db->update('mtd_sistole',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_td(){
	  $td_id=$this->input->post('id');
	  $q="SELECT *FROM mtd_sistole H WHERE H.id='$td_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
  //DIASTOLE
  function simpan_td_diastole(){
		$td_diastole_id=$this->input->post('td_diastole_id');
		$data=array(
			'td_1'=>$this->input->post('td_diastole_1'),
			'td_2'=>$this->input->post('td_diastole_2'),
			'kategori_td'=>$this->input->post('kategori_td_diastole'),
			'warna'=>$this->input->post('warna'),
			'staktif'=>1,
		);
		if ($td_diastole_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mtd_diastole',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$td_diastole_id);
		    $hasil=$this->db->update('mtd_diastole',$data);
		}
		  
		  json_encode($hasil);
	}
	
	
	function load_td_diastole()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
							FROM `mtd_diastole` H
							where H.staktif='1'
							ORDER BY H.td_1


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('td_1','td_2','kategori_td');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->td_1.' - '.$r->td_2);
          $result[] = $r->kategori_td;
          $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'" type="button" >'.$r->warna.'</button>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('1619'))){
		  $aksi .= '<button onclick="edit_td_diastole('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		   if (UserAccesForm($user_acces_form,array('1620'))){
		  $aksi .= '<button onclick="hapus_td_diastole('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_td_diastole(){
	  $td_diastole_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$td_diastole_id);
		$hasil=$this->db->update('mtd_diastole',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_td_diastole(){
	  $td_diastole_id=$this->input->post('id');
	  $q="SELECT *FROM mtd_diastole H WHERE H.id='$td_diastole_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
  
}
