<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdata_implan extends CI_Controller {

	/**
	 * Implan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mdata_implan_model');
		$this->load->helper('path');
  }

	function index($idkategori='0'){
		
		$data = array();
		$data['idkategori']      = $idkategori;
		$data['error'] 			= '';
		$data['title'] 			= 'Implan';
		$data['content'] 		= 'Mdata_implan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Implan",'#'),
									    			array("List",'mdata_implan')
													);
		$data['list_kategori'] = $this->Mdata_implan_model->getKategori();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 					=> '',
			'kode' 				=> '',
			'idkategori' 	=> '',
			'nama' 				=> '',
			'idsatuan' 		=> '',
			'ppn'         => '',
			'hargabeli'  	=> '',
			'hargadasar'  => '',
			'catatan' 		=> '',
			'status' 			=> '',
			'idsatuanbesar' 					=> '',
			'hargasatuanbesar' 				=> '',
			'jumlahsatuanbesar' 			=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Implan';
		$data['content'] 		= 'Mdata_implan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Implan",'#'),
									    			array("Tambah",'mdata_implan')
													);

		$data['list_kategori'] = $this->Mdata_implan_model->getKategori();
		$data['list_satuan'] = $this->Mdata_implan_model->getSatuan();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Mdata_implan_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 					=> $row->id,
					'kode' 				=> $row->kode,
					'idkategori' 	=> $row->idkategori,
					'nama' 				=> $row->nama,
					'idsatuan' 		=> $row->idsatuan,
					'ppn'         => $row->ppn,
					'hargabeli'  	=> $row->hargabeli,
					'hargadasar'  => $row->hargadasar,
					'catatan' 		=> $row->catatan,
					'status' 			=> $row->status,
					'idsatuanbesar' 					=> $row->idsatuanbesar,
					'hargasatuanbesar' 				=> $row->hargasatuanbesar,
					'jumlahsatuanbesar' 			=> $row->jumlahsatuanbesar
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Implan';
				$data['content']	 	= 'Mdata_implan/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Implan",'#'),
											    			array("Ubah",'mdata_implan')
															);

				$data['list_kategori'] = $this->Mdata_implan_model->getKategori();
				$data['list_satuan'] = $this->Mdata_implan_model->getSatuan();

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mdata_implan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mdata_implan');
		}
	}

	function delete($id){
		
		$this->Mdata_implan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mdata_implan','location');
	}

	function save(){
		$this->form_validation->set_rules('idkategori', 'Kategori', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mdata_implan_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mdata_implan','location');
				}
			} else {
				if($this->Mdata_implan_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mdata_implan','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 					= validation_errors();
		$data['content'] 				= 'Mdata_implan/manage';

		$data['list_kategori'] 	= $this->Mdata_implan_model->getKategori();
		$data['list_satuan'] = $this->Mdata_implan_model->getSatuan();

		if($id==''){
			$data['title'] = 'Tambah Implan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Implan",'#'),
															array("Tambah",'mdata_implan')
													);
		}else{
			$data['title'] = 'Ubah Implan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Implan",'#'),
															array("Ubah",'mdata_implan')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	function filter(){
		// if($this->input->post('idkategori') != '0'){
			// $idkategori = $this->input->post('idkategori');
			// redirect("mdata_implan/index/$idkategori",'location');
		// }else{
			// redirect('mdata_implan/index/0','location');
		// }
		$data = array();
		$data['idkategori']      = $this->input->post('idkategori');
		$data['error'] 			= '';
		$data['title'] 			= 'Implan';
		$data['content'] 		= 'Mdata_implan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Implan",'#'),
									    			array("List",'mdata_implan')
													);
		$data['list_kategori'] = $this->Mdata_implan_model->getKategori();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function getIndex($idkategori='0')
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$idkategori=$this->input->post('idkategori');
			$row=$this->Mdata_implan_model->get_array_kategori($idkategori);
		
			
			$this->select = array('mdata_implan.*', 'mdata_kategori.nama AS namakategori');
			$this->from   = 'mdata_implan';
			$this->join 	= array(
				array("mdata_kategori", "mdata_kategori.id = mdata_implan.idkategori", "")
			);
			
			// $this->where  = array(
				// 'mdata_implan.status' => '1'
			// );
			$this->where_in = array(
					'mdata_implan.idkategori' => $row
				);
			$this->order  = array(
				'mdata_implan.kode' => 'DESC'
			);
			$this->group  = array();

			$this->column_search   = array('mdata_implan.kode','mdata_kategori.nama','mdata_implan.nama');
			$this->column_order    = array('mdata_implan.kode','mdata_kategori.nama','mdata_implan.nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->kode;
					$row[] = $r->nama;
					$row[] = $r->namakategori;
					$row[] = number_format($r->hargasatuanbesar,0);
					$row[] = number_format($r->hargadasar,0);
					$row[] = StatusBarang($r->status);
					$aksi = '<div class="btn-group">';
					if ($r->status=='1'){
						if (UserAccesForm($user_acces_form,array('102','105'))){
							$aksi .= '<a href="'.site_url().'mdata_implan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
						}
						 if (UserAccesForm($user_acces_form,array('106'))){
							$aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_implan" data-urlremove="'.site_url().'mdata_implan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
						}
					}else{
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mdata_obat" data-urlremove="'.site_url().'mdata_implan/aktifkan/'.$r->id.'" data-toggle="tooltip" title="Aktifkan" class="btn btn-success btn-sm aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
					}
		            $aksi .= '</div>';
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
	function aktifkan($id){
		
		$this->Mdata_implan_model->aktifkan($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah aktif kembali.');
		redirect('mdata_implan','location');
	}
}
