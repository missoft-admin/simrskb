<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mnafas extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mnafas_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data=$this->Mnafas_model->get_nafas();
		// print_r($data);exit;
		if (UserAccesForm($user_acces_form,array('1613'))){
			
			$data['tab'] 			= $tab;
			$data['error'] 			= '';
			$data['title'] 			= 'Referensi Nafas';
			$data['content'] 		= 'Mnafas/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Referensi Nafas",'mnafas')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	

  function simpan_nafas(){
		$nafas_id=$this->input->post('nafas_id');
		$data=array(
			'nafas_1'=>$this->input->post('nafas_1'),
			'nafas_2'=>$this->input->post('nafas_2'),
			'kategori_nafas'=>$this->input->post('kategori_nafas'),
			'warna'=>$this->input->post('warna'),
			'staktif'=>1,
		);
		if ($nafas_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mnafas',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$nafas_id);
		    $hasil=$this->db->update('mnafas',$data);
		}
		  
		  json_encode($hasil);
	}
	function update_satuan(){
		$satuan_nafas=$this->input->post('satuan_nafas');
		$data=array(
			'satuan_nafas'=>$this->input->post('satuan_nafas'),
			
		);
		$hasil=$this->db->update('mnafas_satuan',$data);
		
		json_encode($hasil);
	}
	
	function load_nafas()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
							FROM `mnafas` H
							where H.staktif='1'
							ORDER BY H.nafas_1


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nafas_1','nafas_1','kategori_nafas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->nafas_1.' - '.$r->nafas_2);
          $result[] = $r->kategori_nafas;
          $result[] ='<button class="btn btn-block btn-sm text-white" style="background-color: '.$r->warna.'" type="button" >'.$r->warna.'</button>';
          $aksi = '<div class="btn-group">';
		  if (UserAccesForm($user_acces_form,array('1615'))){
		  $aksi .= '<button onclick="edit_nafas('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		   if (UserAccesForm($user_acces_form,array('1616'))){
		  $aksi .= '<button onclick="hapus_nafas('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_nafas(){
	  $nafas_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$nafas_id);
		$hasil=$this->db->update('mnafas',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_nafas(){
	  $nafas_id=$this->input->post('id');
	  $q="SELECT *FROM mnafas H WHERE H.id='$nafas_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
  
}
