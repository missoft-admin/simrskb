<?php defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
class Tpenyesuaian_kas extends CI_Controller {

	/**
	 * Penyesuaian Kas controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpenyesuaian_kas_model');
		$this->load->model('Mrka_kegiatan_model');
		$this->load->model('Mrka_bendahara_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array(
			'tanggal_setoran2'=>'',
			'tanggal_setoran1'=>'',
			'tanggal_trx1'=>'',
			'tanggal_trx2'=>'',
			'status'=>'#',
			'notransaksi'=>'',
			'dari'=>'#',
			'ke'=>'#',
		);
		$data['list_sumber_kas'] 	= $this->Tpenyesuaian_kas_model->list_sumber_kas();
		$data['list_jenis_kas'] 	= $this->Tpenyesuaian_kas_model->list_jenis_kas();
		$data['list_bank'] 	= $this->Tpenyesuaian_kas_model->list_bank();
		$data['error'] 			= '';
		$data['title'] 			= 'Monitoring & Penyesuaian Kas';
		$data['content'] 		= 'Tpenyesuaian_kas/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("SETORAN KAS",'#'),
									    			array("List",'Tpenyesuaian_kas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function log(){
		$data = array(
			'tanggal_setoran2'=>'',
			'tanggal_setoran1'=>'',
			'tanggal_trx1'=>'',
			'tanggal_trx2'=>'',
			'status'=>'#',
			'notransaksi'=>'',
			'dari'=>'#',
			'ke'=>'#',
		);
		$data['list_sumber_kas'] 	= $this->Tpenyesuaian_kas_model->list_sumber_kas();
		$data['list_jenis_kas'] 	= $this->Tpenyesuaian_kas_model->list_jenis_kas();
		$data['list_bank'] 	= $this->Tpenyesuaian_kas_model->list_bank();
		$data['list_user'] 	= $this->Tpenyesuaian_kas_model->list_user();
		$data['error'] 			= '';
		$data['title'] 			= 'Log Penyesuaian';
		$data['content'] 		= 'Tpenyesuaian_kas/log';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("SETORAN KAS",'#'),
									    			array("List",'Tpenyesuaian_kas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	public function save()
    {
		
		if($this->Tpenyesuaian_kas_model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tpenyesuaian_kas','location');
		}	
    
    }
	function create($id,$disabel=''){
		$data = $this->Tpenyesuaian_kas_model->getSpecified($id);
		$data['tanggal_trx'] 	= date('d-m-Y');
		$data['list_sumber_kas'] 	= $this->Tpenyesuaian_kas_model->list_sumber_kas();
		$data['list_jenis_kas'] 	= $this->Tpenyesuaian_kas_model->list_jenis_kas();
		$data['list_bank'] 	= $this->Tpenyesuaian_kas_model->list_bank();
		
		$data['deskripsi'] 			= '';
		$data['disabel'] 			= $disabel;
		$data['error'] 			= '';
		$data['title'] 			= 'Penyesuaian Kas';
		$data['content'] 		= 'Tpenyesuaian_kas/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Penyesuaian Kas",'#'),
									    			array("Tambah",'Tpenyesuaian_kas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function edit($id,$disabel=''){
		$data = $this->Tpenyesuaian_kas_model->get_header($id);
		$data['tanggal_trx'] 	= date('d-m-Y');
		$data['list_sumber_kas'] 	= $this->Tpenyesuaian_kas_model->list_sumber_kas();
		$data['list_jenis_kas'] 	= $this->Tpenyesuaian_kas_model->list_jenis_kas();
		$data['list_bank'] 	= $this->Tpenyesuaian_kas_model->list_bank();
		
		$data['disabel'] 			= $disabel;
		$data['error'] 			= '';
		$data['title'] 			= 'Penyesuaian Kas';
		$data['content'] 		= 'Tpenyesuaian_kas/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Penyesuaian Kas",'#'),
									    			array("Tambah",'Tpenyesuaian_kas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function detail($id,$disabel=''){
		$data = $this->Tpenyesuaian_kas_model->getSpecified($id);
		$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d'));
		// date_add($date1,date_interval_create_from_date_string("-30 days"));
		date_add($date2,date_interval_create_from_date_string("-15 days"));
		$date1= date_format($date1,"Y-m-d");
		$date2= date_format($date2,"Y-m-d");


		$data['tanggal1'] 	= $date2;
		$data['tanggal2'] 	= $date1;
		$data['list_sumber_kas'] 	= $this->Tpenyesuaian_kas_model->list_sumber_kas();
		$data['list_jenis_kas'] 	= $this->Tpenyesuaian_kas_model->list_jenis_kas();
		$data['list_bank'] 	= $this->Tpenyesuaian_kas_model->list_bank();
		
		$data['disabel'] 			= $disabel;
		$data['error'] 			= '';
		$data['title'] 			= 'Penyesuaian Kas';
		$data['content'] 		= 'Tpenyesuaian_kas/monitor';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Penyesuaian Kas",'#'),
									    			array("Tambah",'Tpenyesuaian_kas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function refresh_koneksi($id){
		$q="SELECT H.sumber_kas_koneksi as id,M.nama 
			FROM msumber_kas_koneksi H
			LEFT JOIN msumber_kas M ON M.id=H.sumber_kas_koneksi
			WHERE H.sumber_kas_id='$id'";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		
		$q="SELECT M.saldo FROM msumber_kas M
			WHERE M.id='$id'";
		$arr['saldo_dari']=$this->db->query($q)->row('saldo');
		$this->output->set_output(json_encode($arr));
		
	}
	function get_saldo_ke($id){
		
		$q="SELECT M.saldo FROM msumber_kas M
			WHERE M.id='$id'";
		$arr['saldo_ke']=$this->db->query($q)->row('saldo');
		$this->output->set_output(json_encode($arr));
		
	}
	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
		
	  $jenis_kas_id=$this->input->post('jenis_kas_id');
	  $sumber_kas_id=$this->input->post('sumber_kas_id');
	  $bank_id=$this->input->post('bank_id');
	 
	  $iduser=$this->session->userdata('user_id');
	  $where='';
	  if ($jenis_kas_id !='#'){
		  $where .=" AND M.jenis_kas_id='$jenis_kas_id'";
	  }
	  if ($sumber_kas_id !='#'){
		  $where .=" AND M.id='$sumber_kas_id'";
	  }
	  if ($bank_id !='#'){
		  $where .=" AND M.bank_id='$bank_id'";
	  }
	  
	  $from="(
				SELECT M.id,J.nama as jenis_kas,M.nama,B.nama as bank,M.saldo
				 FROM msumber_kas M
				LEFT JOIN mjenis_kas J ON J.id=M.jenis_kas_id
				LEFT JOIN mbank B ON B.id=M.bank_id
				WHERE M.`status`='1' ".$where."
				) as tbl";
	// print_r($tanggal_setoran1);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama','jenis_kas','bank');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
            $no++;
            $row = array();

            $action = '';
            $status = '';
				$action .= '<a href="'.site_url().'tpenyesuaian_kas/detail/'.$r->id.'/disabled" target="_blank" data-toggle="tooltip" title="Lihat" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
				$action .= '<a href="'.site_url().'tpenyesuaian_kas/create/'.$r->id.'" data-toggle="tooltip" title="Create" class="btn btn-danger btn-sm"><i class="fa fa-check"></i></a>';
				$action .= '<a href="'.site_url().'tpenyesuaian_kas/kwitansi/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Cetak Kwitansi" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
			
            $row[] = $r->id;
            $row[] = $no;
  			$row[] = $r->jenis_kas;
  			$row[] = $r->nama;
  			$row[] = $r->bank;
            $row[] = number_format($r->saldo);  			
            $row[] = '<div class="btn-group">'.$action.'</div>';
            $data[] = $row;
        }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function getLog()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
		
	  $jenis_kas_id=$this->input->post('jenis_kas_id');
	  $sumber_kas_id=$this->input->post('sumber_kas_id');
	  $bank_id=$this->input->post('bank_id');
	  $user_id=$this->input->post('user_id');
	  $tanggal1=$this->input->post('tanggal1');
	  $tanggal2=$this->input->post('tanggal2');
	 
	  $iduser=$this->session->userdata('user_id');
	  $where='';
	  if ($jenis_kas_id !='#'){
		  $where .=" AND H.jenis_kas_id='$jenis_kas_id'";
	  }
	  if ($sumber_kas_id !='#'){
		  $where .=" AND H.id='$sumber_kas_id'";
	  }
	  if ($bank_id !='#'){
		  $where .=" AND H.bank_id='$bank_id'";
	  }
	  if ($user_id !='#'){
		  $where .=" AND H.user_created='$user_id'";
	  }
	  if ($tanggal1 !=''){
		  $where .=" AND DATE(H.created_at)>='".YMDFormat($tanggal1)."' AND DATE(H.created_at)<='".YMDFormat($tanggal2)."'";
	  }
	  $from="(
				SELECT H.id,H.jenis_kas_id,H.sumber_kas_id,H.created_at
				,H.tanggal_trx
				,J.nama as jenis_nama,S.nama as sumber_nama,H.saldo_awal,H.saldo_akhir,H.deskripsi
				,U.`name` as nama_user,B.nama as bank_nama
				FROM tpenyesuaian_kas H
				LEFT JOIN mjenis_kas J ON J.id=H.jenis_kas_id
				LEFT JOIN msumber_kas S ON S.id=H.sumber_kas_id
				LEFT JOIN musers U ON U.id=H.user_created
				LEFT JOIN mbank B ON B.id=H.bank_id
				WHERE H.`status`='1' ".$where."
				ORDER BY H.id DESC
				) as tbl";
	// print_r($tanggal_setoran1);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('sumber_nama','jenis_nama','bank_nama','nama_user');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $r->id;
            $row[] = $no;
  			$row[] = HumanDateLong($r->created_at);
  			$row[] = $r->jenis_nama;
  			$row[] = $r->sumber_nama;
  			$row[] = $r->bank_nama;
            $row[] = number_format($r->saldo_awal);  			
            $row[] = number_format($r->saldo_akhir);  			
            $row[] = ($r->nama_user);  			
            $row[] = ($r->deskripsi);  			
            $data[] = $row;
        }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function getJurnal()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
		
	  $tanggal1=$this->input->post('tanggal1');
	  $tanggal2=$this->input->post('tanggal2');
	  $sumber_kas_id=$this->input->post('sumber_kas_id');
	  $bank_id=$this->input->post('bank_id');
	 
	  $iduser=$this->session->userdata('user_id');
	  $where='';
	  if ($tanggal1 !=''){
		  $where .=" AND DATE(H.tanggal)>='".YMDFormat($tanggal1)."' AND DATE(H.tanggal)<='".YMDFormat($tanggal2)."'";
	  }
	  
	  $from="(
				SELECT M.nama as sumber,H.* from tjurnal_kas H
				LEFT JOIN msumber_kas M ON M.id=H.sumber_kas_id
					WHERE H.sumber_kas_id='$sumber_kas_id' ".$where." ORDER BY H.id DESC
				) as tbl";
	// print_r($from);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama','jenis_kas','bank');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
            $no++;
            $row = array();

            $action = '';
            $status = '';
				// $action .= '<a href="'.site_url().'tpenyesuaian_kas/kwitansi/'.$r->id.'" target="_blank" data-toggle="tooltip" titleKwitansi" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
				if ($r->link!='#'){
				$action .= '<a href="'.site_url().$r->link.'" target="_blank" data-toggle="tooltip" " class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>';
					
				}
				if ($r->link_report){
				$action .= '<a href="'.site_url().$r->link_report.'" target="_blank" data-toggle="tooltip" titleKwitansi" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
				}
			
            $row[] = $no;
  			$row[] =HumanDateLong($r->tanggal);
  			$row[] = $r->sumber;
  			$row[] = nama_tabel($r->nama_tabel);
  			$row[] = $r->keterangan;
            $row[] = number_format($r->debet);  			
            $row[] = number_format($r->kredit);  			
            $row[] = number_format($r->saldo_akhir);  			
            $row[] = '<div class="btn-group">'.$action.'</div>';
            $data[] = $row;
        }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function verif($id){
		// $id=$this->input->post('id');
		$result=$this->db->query("UPDATE tpenyesuaian_kas set st_verifikasi='1' WHERE id='$id'");
		echo json_encode($result);
	}
	function hapus($id){
		// $id=$this->input->post('idtsetoran_kas
		$result=$this->db->query("UPDATE tpenyesuaian_kas set status='0' WHERE id='$id'");
		echo json_encode($result);
	}
  public function update($id,$disabel='')
    {
      if($id != ''){
        $data = $this->Tpenyesuaian_kas_model->getSpecified($id);
		$data['list_sumber_kas'] 	= $this->Tpenyesuaian_kas_model->list_sumber_kas();
          $data['disabel']      = $disabel;
          $data['error']      = '';
          $data['title']      = 'Ubah Transaksi Pendapatan Lain-lain';
          $data['content']    = 'Tpenyesuaian_kas/manage';
          $data['breadcrum']  = array(
                                 array("RSKB Halmahera",'dashboard'),
                                 array("Bendahara",'#'),
                                 array("Transaksi Pendapatan Lain-lain",'tpendapatan'),
                                 array("Ubah",'')
                              );
        $data = array_merge($data,backend_info());
        $this->parser->parse('module_template',$data);
       
      }else{
        $this->session->set_flashdata('error',true);
        $this->session->set_flashdata('message_flash','data tidak ditemukan.');
        redirect('Tpenyesuaian_kas');
      }
    }
	public function pdf($row_detail,$header){
		// print_r($header);exit();
		$dompdf = new Dompdf();
		$data=array();
		$data=array_merge($data,$header);
        
        $data['detail'] = $row_detail;

		$data = array_merge($data, backend_info());

        $html = $this->load->view('Tpenyesuaian_kas/pdf', $data, true);
		// print_r($html	);exit();
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Jurnal Kas.pdf', array("Attachment"=>0));
	}
	public function export()
    {
		// print_r($this->input->post());exit();
        ini_set('memory_limit', '-1');
        set_time_limit(1000);
        $jenis_kas_id=$this->input->post('xjenis_kas_id');
		$sumber_kas_id=$this->input->post('xsumber_kas_id');
		$tanggal_trx1=$this->input->post('tanggal1');
		$tanggal_trx2=$this->input->post('tanggal2');
		$where='';
		
		// if ($jenis_kas_id !='#'){
			// $where .=" AND H.jenis_kas_id='$jenis_kas_id' ";
		// }
		$data=$this->Tpenyesuaian_kas_model->get_header_report($sumber_kas_id);
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(H.tanggal) >='".YMDFormat($tanggal_trx1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_trx2)."'";
			$data['tanggal1']=($tanggal_trx1);
			$data['tanggal2']=($tanggal_trx2);
		// print_r($data['tanggal2']);exit();
        }else{
			$data['tanggal1']='';
			$data['tanggal2']='';
		}
		// print_r($data);exit();
       
	  $from="
				SELECT M.nama as sumber,H.* from tjurnal_kas H
				LEFT JOIN msumber_kas M ON M.id=H.sumber_kas_id
					WHERE H.sumber_kas_id='$sumber_kas_id' ".$where." ORDER BY H.id ASC
			";
				
        $btn    = $this->input->post('btn_export');
        $row_detail=$this->db->query($from)->result();
        
		if ($btn=='1'){
			$this->excel($row_detail,$data);
		}
		if ($btn=='2'){
			$this->pdf($row_detail,$data);
		}
		if ($btn=='3'){
			$this->pdf($row_detail,$data);
		}
		
		
		
    }
	function excel($row_detail,$data){
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Jurnal Kas');

        // Set Logo
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $logo = FCPATH.'/assets/upload/logo/logoreport.jpg';
        $objDrawing->setPath($logo);
        $objDrawing->setCoordinates('B1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setHeight(70);
        $objDrawing->setWidth(40);
        $objDrawing->setWorksheet($activeSheet);

        // Set Header
        $activeSheet->setCellValue('B1', "                                      RUMAH SAKIT KHUSUS BEDAH HALMAHERA SIAGA");
        $activeSheet->setCellValue('B2', "                                      Jl.LLRE.Martadinata no.28, Telp: +6222-4206061");
        $activeSheet->setCellValue('B3', "                                      Bandung, 40115");
        $activeSheet->mergeCells('B1:AA1');
        $activeSheet->mergeCells('B2:AA2');
        $activeSheet->mergeCells('B3:AA3');
        $activeSheet->getStyle('B1:AA1')->getFont()->setSize(11);
        $activeSheet->getStyle('B2:AA2')->getFont()->setSize(11);
        $activeSheet->getStyle('B3:AA3')->getFont()->setSize(11);

		$activeSheet->setCellValue('B5', "SUMBER KAS");
		$activeSheet->setCellValue('C5', $data['sumber_kas']);
		$activeSheet->setCellValue('B6', "JENIS KAS");
		$activeSheet->setCellValue('C6', $data['jenis_kas']);
		$activeSheet->setCellValue('B7', "TANGGAL");
		$activeSheet->setCellValue('C7', $data['tanggal1'].' s/d'. $data['tanggal2']);
		
		// Set Title
		$activeSheet->setCellValue('B9', "JURNAL SUMBER KAS ");
		$activeSheet->mergeCells('B9:H9');
		$activeSheet->getStyle('B9')->getFont()->setBold(true)->setSize(14);
		$activeSheet->getStyle('B9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$x = 11;
		$activeSheet->setCellValue("B$x", "TANGGAL");
		$activeSheet->setCellValue("C$x", "SUMBER KAS");
		$activeSheet->setCellValue("D$x", "TIPE");
		$activeSheet->setCellValue("E$x", "KETERANGAN");
		$activeSheet->setCellValue("F$x", "DEBIT");
		$activeSheet->setCellValue("G$x", "KREDIT");
		$activeSheet->setCellValue("H$x", "SALDO");
		$activeSheet->getStyle("B$x:H$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("B$x:H$x")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
	  'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK,
					)
				),
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Calibri'
				)
			)
		);

		
		$debet = 0;
		$kredit= 0;
		
        if (COUNT($row_detail)) {
            foreach ($row_detail as $row) {
				$x = $x+1;
				$debet=(int)$debet+(int)$row->debet;
				$kredit=(int)$kredit+(int)$row->kredit;
				// $debet = $debet+$row->debet;
				// $kredit = $kredit+$row->kredit;
				
	            $activeSheet->setCellValue("B$x", HumanDateShort($row->tanggal));
	            $activeSheet->setCellValue("C$x", $row->sumber);
	            $activeSheet->setCellValue("D$x", nama_tabel_biasa($row->nama_tabel));
	            $activeSheet->setCellValue("E$x", $row->keterangan);
	            $activeSheet->setCellValue("F$x", $row->debet);
	            $activeSheet->setCellValue("G$x", $row->kredit);
	            $activeSheet->setCellValue("H$x", $row->saldo_akhir);
				
	           
				
				
			}
		}
		$x = $x+1;
		$activeSheet->setCellValue("E$x", "TOTAL");
		// $activeSheet->setCellValue("B$x", "");
		// $activeSheet->setCellValue("C$x", "");
		// $activeSheet->setCellValue("D$x", "");
		$activeSheet->setCellValue("F$x", $debet);
		$activeSheet->setCellValue("G$x", $kredit);
		// $activeSheet->setCellValue("H$x", "");
		$activeSheet->getStyle("B11:D$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("H11:H$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$activeSheet->getStyle("E11:E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$activeSheet->getStyle("F11:H$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$activeSheet->getStyle("F11:H$x")->getNumberFormat()->setFormatCode('#,##0');
		$activeSheet->getStyle("B11:H$x")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
					)
				)
			)
		);
		
    	// Set Auto Width Column
    	$activeSheet->getColumnDimension("A")->setWidth(5);
    	$activeSheet->getColumnDimension("B")->setAutoSize(true);
    	$activeSheet->getColumnDimension("C")->setAutoSize(true);
    	$activeSheet->getColumnDimension("D")->setAutoSize(true);
    	$activeSheet->getColumnDimension("E")->setAutoSize(true);
    	$activeSheet->getColumnDimension("F")->setAutoSize(true);
    	$activeSheet->getColumnDimension("G")->setAutoSize(true);
    	$activeSheet->getColumnDimension("H")->setAutoSize(true);
    	// $activeSheet->getColumnDimension("H")->setWidth(45);
    	// $activeSheet->getColumnDimension("H")->setAutoSize(true);

    	header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    	header('Content-Disposition: attachment;filename="Jurnal Kas '.date('Y-m-d-h-i-s').'.xls"');
    	header('Cache-Control: max-age=0');

    	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    	$objWriter->save('php://output');
	}
}
