<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tdeposit_batal extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tdeposit_batal_model');
		$this->load->model('Tpendaftaran_poli_pasien_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->helper('path');
		
  }
	function index($tab='1'){
		$log['path_tindakan']='tdeposit_batal';
		$this->session->set_userdata($log);
		$user_id=$this->session->userdata('user_id');
		// print_r($this->session->userdata('path_tindakan'));exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$q="SELECT H.id as login_mppa_id,H.pegawai_id as login_iddokter,H.pegawai_id as iddokter FROM mppa H WHERE H.user_id='$user_id' AND H.tipepegawai='2' AND H.staktif='1'";
		// $login_iddokter=$this->db->query($q)->row('login_iddokter');	
		if (UserAccesForm($user_acces_form,array('2615'))){
			$data = $this->db->query($q)->row_array();	
			if ($data){
				
				$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("-30 days"));
			$date2= date_format($date2,"d/m/Y");
				
				$data['idpoli'] 			= '#';
				$data['namapasien'] 			= '';
				$data['tab'] 			= $tab;
				
				$data['tanggal_1'] 			= $date2;
				$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
				$data['error'] 			= '';
				$data['title'] 			= 'Deposit Yang Dibatalkan';
				$data['content'] 		= 'Tdeposit_batal/index';
				$data['breadcrum'] 	= array(
													  array("RSKB Halmahera",'#'),
													  array("Rawan Inap & ODS Deposit",'#'),
													  array("Pembatalan Deposit",'tdeposit_batal')
													);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				redirect('page404');
			}
		}else{
			redirect('page404');
		}
	}
	
	function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$iduser=$this->session->userdata('user_id');		
			
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$no_medrec =$this->input->post('no_medrec');
			$nama_pasien =$this->input->post('nama_pasien');
			$tab =$this->input->post('tab');
			
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.deleted_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.deleted_date) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(H.deleted_date) >='".date('Y-m-d')."'";
			}
			if ($no_medrec!=''){
				$where .=" AND (MH.no_medrec LIKE '%".$no_medrec."%')";
			}
			if ($nama_pasien!=''){
				$where .=" AND (MH.namapasien LIKE '%".$nama_pasien."%')";
			}
			// if ($tab=='2'){
				// // $where .=" AND ((TK.st_jawab = '0' AND TK.iddokter_awal='$iddokter') OR (TK.st_jawab_baru = '0' AND TK.iddokter_baru='$iddokter') )";
			// }
			// if ($tab=='3'){
				// // $where .=" AND ((TK.st_jawab = '1' AND TK.iddokter_awal='$iddokter') OR (TK.st_jawab_baru = '1' AND TK.iddokter_baru='$iddokter') )";
			// }
			
			// print_r($tab);exit;
			$this->select = array();
			
				$from="
					(
						SELECT DH.tipe_deposit,DH.deskripsi,MJ.nama as jenis_deposit,MH.no_medrec,MH.namapasien
						,MU.`name` as nama_user,MC.`name` as nama_user_created,H.* 
						,(CASE
							WHEN H.idmetodepembayaran = 1 THEN 'Tunai'
							WHEN H.idmetodepembayaran = 2 THEN 'Debit'
							WHEN H.idmetodepembayaran = 3 THEN 'Kredit'
							WHEN H.idmetodepembayaran = 4 THEN 'Transfer'
						END) AS metodepembayaran,MB.nama as nama_bank
						FROM `trawatinap_deposit` H
						LEFT JOIN musers MU ON MU.id=H.iduserdelete
						LEFT JOIN musers MC ON MC.id=H.iduserinput
						INNER JOIN trawatinap_deposit_head DH ON DH.id=H.head_id
						LEFT JOIN mdeposit_jenis MJ ON MJ.id=H.jenis_deposit_id
						INNER JOIN trawatinap_pendaftaran MH ON MH.id=H.idrawatinap
						
						LEFT JOIN mbank MB ON MB.id=H.idbank
						WHERE H.`status`='0' AND H.hapus_proses='1' ".$where."
						GROUP BY H.id
						ORDER BY H.created_date DESC
					) as tbl
				";
			
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('deskripsi','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
		  $respon='';
          $result = array();
		  
		  $result[]=$r->no_medrec.'<br>'.($r->namapasien);
		  $result[]=$r->nama_user.'<br>'.HumanDateLong($r->deleted_date);
		  $result[]=$r->jenis_deposit;
		  $result[]=HumanDateShort($r->tanggal);
		  $result[]=$r->metodepembayaran.($r->nama_bank?'<br>'.$r->nama_bank:'');
		  $result[]=number_format($r->nominal,0);
		  $result[]=$r->terimadari;
		  $result[]=$r->alasanhapus;
		  $btn_pdf='<a href="'.base_url().'assets/upload/deposit/'.$r->nodeposit.'.pdf" title="Cetak PDF" target="_blank" class="btn btn-xs btn-danger" ><i class="fa fa-file-pdf-o"></i> '.$r->nodeposit.'.pdf</a>';
		  $result[]=$btn_pdf;
		

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	
}	
