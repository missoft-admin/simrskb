<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qrcode extends CI_Controller {

	/**
	 * Booking controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		
  }


	function get_barcode($id)
    {
        $this->load->library('Barcode_generator');
        $this->barcode_generator->output_image('png', 'qr-h',$id,array());
    }
	function qr_code_ttd($id)
    {
		$q="SELECT *FROM mppa WHERE id='$id'";
		$data=$this->db->query($q)->row();
		$nama_lengkap ="Nama Pemberi Asuhan : ".$data->nama;
		$nama_lengkap .="
						NIP : ".$data->nip;
        $this->load->library('Barcode_generator');
        $this->barcode_generator->output_image('png', 'qr-h',$nama_lengkap,array());
    }
	function qr_code_ppa($id)
    {
		$q="SELECT *FROM mppa WHERE id='$id'";
		$data=$this->db->query($q)->row();
		$nama_lengkap ="Nama Pemberi Asuhan : ".$data->nama;
		$nama_lengkap .="
						NIP : ".$data->nip;
        $this->load->library('Barcode_generator');
        $this->barcode_generator->output_image('png', 'qr-h',$nama_lengkap,array());
    }
	function qr_code_ttd_dokter($id)
    {
		
		$q="SELECT *FROM mdokter WHERE id='$id'";
		$data=$this->db->query($q)->row();
		$nama_lengkap ="Nama Pemberi Asuhan : ".$data->nama;
		$nama_lengkap .="
						NIP : ".$data->nip;
        $this->load->library('Barcode_generator');
        $this->barcode_generator->output_image('jpg', 'qr-h',$nama_lengkap,array());
    }
	function qr_code_ttd_user_name($id)
    {
		// print_r($id);exit;
		$q="SELECT *FROM musers WHERE id='$id'";
		$data=$this->db->query($q)->row();
		$nama_lengkap ="Nama User : ".$data->name;
		$nama_lengkap .="
						NIP : - ";
        $this->load->library('Barcode_generator');
        $this->barcode_generator->output_image('jpg', 'qr-h',$nama_lengkap,array());
    }
}
