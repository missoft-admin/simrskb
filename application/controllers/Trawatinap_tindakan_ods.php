<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use Dompdf\Dompdf;

class Trawatinap_tindakan_ods extends CI_Controller
{
	/**
   * Rawat Inap Controller.
   * Developer @RendyIchtiarSaputra & @GunaliRezqiMauludi
   */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Trawatinap_tindakan_ods_model');
	}

	public function index()
	{
		$data = [
			'nomedrec' => '',
			'namapasien' => '',
			'iddokterpenanggungjawab' => '',
			'tanggaldari_transaksi' => date('d/m/Y'),
			'tanggalsampai_transaksi' => date('d/m/Y'),
			'status' => '',
		];

		$data['error'] = '';
		$data['title'] = 'Tindakan One Day Surgery (ODS)';
		$data['content'] = 'Trawatinap_tindakan_ods/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Tindakan One Day Surgery (ODS)', '#'],
			['List', 'Trawatinap_tindakan_ods']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter()
	{
		$data = [
			'nomedrec' => $this->input->post('nomedrec'),
			'namapasien' => $this->input->post('namapasien'),
			'iddokterpenanggungjawab' => $this->input->post('iddokterpenanggungjawab'),
			'tanggaldari_transaksi' => $this->input->post('tanggaldari_transaksi'),
			'tanggalsampai_transaksi' => $this->input->post('tanggalsampai_transaksi'),
			'status' => $this->input->post('status'),
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Tindakan One Day Surgery (ODS)';
		$data['content'] = 'Trawatinap_tindakan_ods/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Tindakan One Day Surgery (ODS)', '#'],
			['List', 'Trawatinap_tindakan_ods']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function getIndex($uri = 'index')
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];

		$this->select = [
			'trawatinap_pendaftaran.id AS idpendaftaran',
			'trawatinap_pendaftaran.tanggaldaftar',
			'trawatinap_pendaftaran.statuscheckout',
			'trawatinap_pendaftaran.statuspembayaran',
			'trawatinap_pendaftaran.nopendaftaran',
			'trawatinap_pendaftaran.iddokterpenanggungjawab',
			'trawatinap_pendaftaran.status',
			'mfpasien.no_medrec AS nomedrec',
			'mfpasien.nama AS namapasien',
			'mfpasien.jenis_kelamin',
			'mfpasien.umur_tahun',
			'mfpasien.umur_bulan',
			'mfpasien.umur_hari',
			'tpoliklinik_pendaftaran.iddokter',
			'mdokterperujuk.nama AS namadokterperujuk',
			'mdokterpenanggungjawab.nama AS namadokterpenanggungjawab',
			'mbed.nama AS namabed',
		];

		$this->from = 'trawatinap_pendaftaran';

		$this->join = [
			['mbed', 'mbed.id = trawatinap_pendaftaran.idbed', 'LEFT'],
			['tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT'],
			['mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT'],
			['mdokter mdokterperujuk', 'mdokterperujuk.id = tpoliklinik_pendaftaran.iddokter', 'LEFT'],
			['mdokter mdokterpenanggungjawab', 'mdokterpenanggungjawab.id = trawatinap_pendaftaran.iddokterpenanggungjawab', 'LEFT'],
		];

		// FILTER
		if ($uri == 'filter') {
			$this->where = [];
			if ($this->session->userdata('nomedrec') != '') {
				$this->where = array_merge($this->where, ['mfpasien.no_medrec' => $this->session->userdata('nomedrec')]);
			}
			if ($this->session->userdata('namapasien') != '') {
				$this->where = array_merge($this->where, ['mfpasien.nama LIKE' => '%' . $this->session->userdata('namapasien') . '%']);
			}
			if ($this->session->userdata('iddokterpenanggungjawab') != '#') {
				$this->where = array_merge($this->where, ['mdokterpenanggungjawab.id' => $this->session->userdata('iddokterpenanggungjawab')]);
			}

			if ($this->session->userdata('tanggaldari_transaksi') != '') {
				$this->where = array_merge($this->where, ['DATE(trawatinap_pendaftaran.tanggaldaftar) >=' => YMDFormat($this->session->userdata('tanggaldari_transaksi'))]);
			}
			if ($this->session->userdata('tanggalsampai_transaksi') != '') {
				$this->where = array_merge($this->where, ['DATE(trawatinap_pendaftaran.tanggaldaftar) <=' => YMDFormat($this->session->userdata('tanggalsampai_transaksi'))]);
			}

			if ($this->session->userdata('status') != '#') {
				$this->where = array_merge($this->where, ['trawatinap_pendaftaran.status' => $this->session->userdata('status')]);
			}
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idtipe' => '2']);
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.statuscheckout' => '0']);
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.status' => '1']);
		} else {
			$this->where = ['trawatinap_pendaftaran.idtipe' => '2'];
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.statuscheckout' => '0']);
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.status' => '1']);
		}

		$this->order = [
			'trawatinap_pendaftaran.id' => 'DESC'
		];
		$this->group = [];

		$this->column_search = ['trawatinap_pendaftaran.tanggaldaftar', 'mfpasien.no_medrec', 'mfpasien.nama', 'mfpasien.jenis_kelamin', 'mfpasien.umur_tahun', 'mdokterperujuk.nama', 'mdokterpenanggungjawab.nama'];
		$this->column_order = ['trawatinap_pendaftaran.tanggaldaftar', 'mfpasien.no_medrec', 'mfpasien.nama', 'mfpasien.jenis_kelamin', 'mfpasien.umur_tahun', 'mdokterperujuk.nama', 'mdokterpenanggungjawab.nama'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			$pembayaran = $this->db->query('SELECT
              trawatinap_tindakan_pembayaran_detail.tipekontraktor,
              trawatinap_tindakan_pembayaran_detail.idkontraktor,
            	(CASE
            		WHEN trawatinap_tindakan_pembayaran_detail.tipekontraktor = 1 THEN mrekanan.nama
            		ELSE mpasien_kelompok.nama
            	END) AS namakontraktor
            FROM trawatinap_tindakan_pembayaran_detail
            JOIN trawatinap_tindakan_pembayaran ON trawatinap_tindakan_pembayaran.id = trawatinap_tindakan_pembayaran_detail.idtindakan
            LEFT JOIN mrekanan ON mrekanan.id = trawatinap_tindakan_pembayaran_detail.idkontraktor AND trawatinap_tindakan_pembayaran_detail.tipekontraktor = 1
            LEFT JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_tindakan_pembayaran_detail.tipekontraktor
            WHERE trawatinap_tindakan_pembayaran_detail.idmetode = 8 AND trawatinap_tindakan_pembayaran.idtindakan = "' . $r->idpendaftaran . '"
            GROUP BY tipekontraktor, idkontraktor');

			if ($pembayaran->num_rows() > 0) {
				$action = '<div class="btn-group"><div class="btn-group"><button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown"><span class="fa fa-print"></span></button><ul class="dropdown-menu dropdown-menu-left"><li>';
				foreach ($pembayaran->result() as $pb) {
					$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trawatinap_tindakan/print_rincian_tagihan/' . $r->idpendaftaran . '/' . $pb->tipekontraktor . '/' . $pb->idkontraktor . '">Tagihan Rekanan (' . $pb->namakontraktor . ')</a>';
				}
				$action .= '</li></ul></div></div>';
			} else {
				$action = '';
			}

			$row[] = "<span data-idpendaftaran='" . $r->idpendaftaran . "'>" . $no . '</span>';
			$row[] = DMYFormat($r->tanggaldaftar);
			$row[] = $r->nopendaftaran;
			$row[] = $r->nomedrec;
			$row[] = $r->namapasien;
			$row[] = GetJenisKelamin($r->jenis_kelamin);
			$row[] = $r->umur_tahun . ' Th ' . $r->umur_bulan . ' Bln ' . $r->umur_hari . ' Hr';
			$row[] = $r->namadokterpenanggungjawab;
			$row[] = StatusTransaksiKasir($r->statuspembayaran);
			$row[] = $action;

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}
}

/* End of file Trawatinap_tindakan_ods.php */
/* Location: ./application/controllers/Trawatinap_tindakan_ods.php */
