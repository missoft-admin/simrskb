<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mracikan_des extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mracikan_des_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1621'))){
			
			$data['jenis_racik_id'] 			= '';
			$data['error'] 			= '';
			$data['title'] 			= 'Jenis Racikan Deskripsi';
			$data['content'] 		= 'Mracikan_des/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Jenis Racikan Deskripsi",'mracikan_des')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	

	function simpan_racikan_des(){
		$jenis_racik_id=$this->input->post('jenis_racik_id');
		if ($this->cek_duplicate($jenis_racik_id)==false){
			$data=array(
				'jenis_racik_id'=>$this->input->post('jenis_racik_id'),
				'deskripsi'=>$this->input->post('deskripsi'),
			);
			$hasil=$this->db->insert('mracikan_des',$data);
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate($jenis_racik_id){
		$q="SELECT *FROM mracikan_des WHERE jenis_racik_id='$jenis_racik_id'";
		$hasil=$this->db->query($q)->row();
		if ($hasil){
			return true;
		}else{
			return false;
		}
	}
	function load_racikan_des()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*,M.ref as jenis FROM mracikan_des H
						INNER JOIN merm_referensi M ON M.nilai=H.jenis_racik_id AND M.ref_head_id='105'
						ORDER BY M.nilai ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('deskripsi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->jenis;
          $result[] = $r->deskripsi;
          $aksi = '<div class="btn-group">';
		  
		   if (UserAccesForm($user_acces_form,array('2100'))){
		  $aksi .= '<button onclick="hapus_racikan_des('.$r->jenis_racik_id.')" type="button" title="Hapus Suhu" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_racikan_des(){
	  $jenis_racik_id=$this->input->post('id');
	 
		$this->db->where('jenis_racik_id',$jenis_racik_id);
		$hasil=$this->db->delete('mracikan_des');
	  
	  json_encode($hasil);
	  
  }

  function find_racikan_des(){
	  $racikan_des_id=$this->input->post('id');
	  $q="SELECT *FROM mracikan_des H WHERE H.id='$racikan_des_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  
  
}
