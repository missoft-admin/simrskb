<?php

class Mbiaya extends CI_Controller
{

	/**
	 * Biaya controller.
	 * Developer @gunalirezqimauludi
	 */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mbiaya_model');
    }

    public function index()
    {
        $data['error']      = '';
        $data['title']      = 'Biaya';
        $data['content']    = 'Mbiaya/index';
        $data['breadcrum']  = array(
                                 array("RSKB Halmahera",'dashboard'),
                                 array("Bendahara",'#'),
                                 array("Biaya",'Mbiaya'),
                                 array("List",'')
                              );

        $data['list_index'] = $this->Mbiaya_model->getAll();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create()
    {
        $data = array(
            'id'          => '',
            'keterangan'  => '',
            'noakun'      => '',
            'status'      => '',
        );

        $data['error']      = '';
        $data['title']      = 'Create Biaya';
        $data['content']    = 'Mbiaya/manage';
        $data['breadcrum']  = array(
                                 array("RSKB Halmahera",'dashboard'),
                                 array("Bendahara",'#'),
                                 array("Biaya",'Mbiaya'),
                                 array("Create",'')
                              );

        $data['list_noakun'] = $this->Mbiaya_model->getNoAkun();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function edit($id)
    {
        if ($id != '') {
            $row = $this->Mbiaya_model->getSpecified($id);
            if (isset($row->id)) {
                $data = array(
                          'id'          => $row->id,
                          'keterangan'  => $row->keterangan,
                          'noakun'      => $row->noakun,
                          'status'      => $row->status,
                        );
                $data['error']      = '';
                $data['title']      = 'Edit Data';
                $data['content']    = 'Mbiaya/manage';
                $data['breadcrum']  = array(
                                         array("RSKB Halmahera",'dashboard'),
                                         array("Bendahara",'#'),
                                         array("Biaya",'Mbiaya'),
                                         array("Edit",'')
                                      );

                $data['list_noakun'] = $this->Mbiaya_model->getNoAkun();

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mbiaya', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mbiaya');
        }
    }

    public function delete($id)
    {
        $this->Mbiaya_model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mbiaya', 'location');
    }

    public function save()
    {
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
        $this->form_validation->set_rules('noakun', 'No. Akun', 'trim|required');

        if ($this->form_validation->run() == true) {
            if ($this->input->post('id') == '') {
                if ($this->Mbiaya_model->saveData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mbiaya', 'location');
                }
            } else {
                if ($this->Mbiaya_model->updateData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mbiaya', 'location');
                }
            }
        } else {
            $this->failed_save($this->input->post('id'));
        }
    }

    public function failed_save($id)
    {
        $data = $this->input->post();
        $data['error']   = validation_errors();
        $data['content'] = 'Mbiaya/manage';

        if ($id=='') {
            $data['title']      = 'Create Biaya';
            $data['breadcrum']  = array(
                                     array("RSKB Halmahera",'dashboard'),
                                     array("Bendahara",'#'),
                                     array("Biaya",'Mbiaya'),
                                     array("Create",'')
                                  );
        } else {
            $data['title']      = 'Edit Data';
            $data['breadcrum']  = array(
                                     array("RSKB Halmahera",'dashboard'),
                                     array("Bendahara",'#'),
                                     array("Biaya",'Mbiaya'),
                                     array("Edit",'')
                                  );
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
