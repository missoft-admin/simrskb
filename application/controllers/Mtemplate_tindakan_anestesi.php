<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtemplate_tindakan_anestesi extends CI_Controller {

	/**
	 * Master Template Tindakan Anestesi controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtemplate_tindakan_anestesi_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Template Tindakan Anestesi';
		$data['content'] 		= 'Mtemplate_tindakan_anestesi/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Template Tindakan Anestesi",'#'),
									    			array("List",'mtemplate_tindakan_anestesi')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'isi_footer' 		=> '',
			'isi_header' 		=> '',
			'nilai_tertimbang' 		=> '0',
			'nilai_satuan' 		=> '0',
			'st_default' 		=> '0',
			'staktif' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Template Tindakan Anestesi';
		$data['content'] 		= 'Mtemplate_tindakan_anestesi/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master ERM",'#'),
														array("Master Template Tindakan Anestesi",'#'),
									    			array("Tambah",'mtemplate_tindakan_anestesi')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mtemplate_tindakan_anestesi_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'isi_header' 		=> $row->isi_header,
					'nilai_tertimbang' 		=> $row->nilai_tertimbang,
					'nilai_satuan' 		=> $row->nilai_satuan,
					'isi_footer' 		=> $row->isi_footer,
					'staktif' 				=> $row->staktif
				);
				$data['inisial'] 			= '';
				$data['st_nilai'] 			= '1';
				$data['st_default'] 			= '0';
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Template Tindakan Anestesi';
				$data['content']	 	= 'Mtemplate_tindakan_anestesi/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Template Tindakan Anestesi",'#'),
											    			array("Ubah",'mtemplate_tindakan_anestesi')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtemplate_tindakan_anestesi','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtemplate_tindakan_anestesi');
		}
	}
	function setting_range_nilai($id){
		if($id != ''){
			$row = $this->Mtemplate_tindakan_anestesi_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					
					'staktif' 				=> $row->staktif
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Template Tindakan Anestesi';
				$data['content']	 	= 'Mtemplate_tindakan_anestesi/manage_setting';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Template Tindakan Anestesi",'#'),
											    			array("Ubah",'mtemplate_tindakan_anestesi')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtemplate_tindakan_anestesi','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtemplate_tindakan_anestesi');
		}
	}

	function delete($id){
		$this->Mtemplate_tindakan_anestesi_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mtemplate_tindakan_anestesi','location');
	}
	function load_index_informasi_tindakan_anestesi(){
	  $id=$this->input->post('id');
	 
	  $q="SELECT H.*,M.nama,M.nama_english,M.nourut FROM mtemplate_tindakan_anestesi_informasi H
			LEFT JOIN mjenis_info_tindakan M ON M.id=H.tindakan_id
			WHERE H.template_id='$id'

			ORDER BY M.nourut";
	  $list_info=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_info as $r){
		  // style="text-align: center;"
		  $tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="informasi_id" value="'.$r->id.'">'.$r->nourut.'</td>';
			$tabel .='<td class="text-center"><strong>'.$r->nama.'</strong><br><i>'.$r->nama_english.'</i></td>';
			$tabel .='<td><textarea class="js-summernote form-control auto_blur_tabel " name="story" rows="3" style="width:100%">'.$r->isi_informasi.'</textarea></td>';
		  $tabel .='</tr>';
	  } 
	 
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function update_isi_informasi_tindakan_anestesi(){
		$assesmen_id=$this->input->post('assesmen_id');
		$id=$this->input->post('informasi_id');
		$isi_informasi=$this->input->post('isi');
		
		
		$data=array(
			'isi_informasi' => $isi_informasi,
			);
		$this->db->where('id',$id);
		$result=$this->db->update('mtemplate_tindakan_anestesi_informasi',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
	}
	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->Mtemplate_tindakan_anestesi_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtemplate_tindakan_anestesi/update/'.$id,'location');
				}
			} else {
				if($this->Mtemplate_tindakan_anestesi_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtemplate_tindakan_anestesi/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mtemplate_tindakan_anestesi/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Template Tindakan Anestesi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Template Tindakan Anestesi",'#'),
															array("Tambah",'mtemplate_tindakan_anestesi')
													);
		}else{
			$data['title'] = 'Ubah Master Template Tindakan Anestesi';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master ERM",'#'),
															array("Master Template Tindakan Anestesi",'#'),
															array("Ubah",'mtemplate_tindakan_anestesi')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
			$this->from   = 'mtemplate_tindakan_anestesi';
			$this->join 	= array();
			$this->where  = array(
				'staktif' => '1'
			);
			$this->order  = array(
				'nama' => 'ASC'
			);
			$this->group  = array();
		
		$this->column_search   = array('nama','isi_header');
      $this->column_order    = array('nama','isi_header');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama;
		$aksi = '<div class="btn-group">';
       
		if (UserAccesForm($user_acces_form,array('2415'))){
            $aksi .= '<a href="'.site_url().'mtemplate_tindakan_anestesi/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
        }
        if (UserAccesForm($user_acces_form,array('2416'))){
            $aksi .= '<a href="#" data-urlindex="'.site_url().'mtemplate_tindakan_anestesi" data-urlremove="'.site_url().'mtemplate_tindakan_anestesi/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
        }
        $aksi .= '</div>';
		$row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_parameter()
	{
			$mtemplate_tindakan_anestesi_id=$this->input->post('mtemplate_tindakan_anestesi_id');
		
			$this->select = array();
			$from="
					(
						SELECT H.*,
						GROUP_CONCAT(CONCAT(M.deskripsi_nama,' (',M.skor,')') SEPARATOR '#') as jawaban
						FROM `mtemplate_tindakan_anestesi_param` H
						LEFT JOIN mtemplate_tindakan_anestesi_param_skor M ON M.parameter_id=H.id AND M.staktif='1'
						where H.staktif='1' AND H.mtemplate_tindakan_anestesi_id='$mtemplate_tindakan_anestesi_id'
						GROUP BY H.id
						ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('parameter_nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->inisial;
          $result[] = $r->parameter_nama.($r->jawaban?'&nbsp;&nbsp;'.$this->render_jawaban($r->jawaban):'');
          $result[] = ($r->st_nilai=='1'?'YA':'TIDAK');
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="add_jawaban('.$r->id.')" type="button" title="Add Value" class="btn btn-warning btn-xs "><i class="fa fa-plus"></i></button>';	
		  $aksi .= '<button onclick="edit_parameter('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_parameter('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function render_jawaban($jawaban){
	  $hasil='';
	  $arr=explode('#',$jawaban);
	  foreach($arr as $index=>$val){
		  $hasil .='<span class="badge ">'.$val.'</span> ';
	  }
	  return $hasil;
  }
  function load_nilai()
	{
			$mtemplate_tindakan_anestesi_id=$this->input->post('mtemplate_tindakan_anestesi_id');
		
			$this->select = array();
			$from="
					(
						SELECT H.*
						
						FROM `mtemplate_tindakan_anestesi_setting_nilai` H
						where H.staktif='1' AND H.mtemplate_tindakan_anestesi_id='$mtemplate_tindakan_anestesi_id'
						GROUP BY H.id
						ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('ref_nilai');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->skor_1.' - '.$r->skor_2;
          $result[] = $r->ref_nilai;
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_nilai('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_nilai('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function simpan_nilai(){
		$parameter_id=$this->input->post('parameter_id');
		$mtemplate_tindakan_anestesi_id=$this->input->post('mtemplate_tindakan_anestesi_id');
		$skor_1=$this->input->post('skor_1');
		$skor_2=$this->input->post('skor_2');
		$ref_nilai=$this->input->post('ref_nilai');
		$ref_nilai_id=$this->input->post('ref_nilai_id');
		$data=array(
			'mtemplate_tindakan_anestesi_id'=>$this->input->post('mtemplate_tindakan_anestesi_id'),
			'skor_1'=>$this->input->post('skor_1'),
			'skor_2'=>$this->input->post('skor_2'),
			'ref_nilai'=>$this->input->post('ref_nilai'),
			'ref_nilai_id'=>$this->input->post('ref_nilai_id'),
			'staktif'=>1,
		);
		if ($parameter_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mtemplate_tindakan_anestesi_setting_nilai',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$parameter_id);
		    $hasil=$this->db->update('mtemplate_tindakan_anestesi_setting_nilai',$data);
		}
		  
		  json_encode($hasil);
	}
	function simpan_parameter(){
		$mtemplate_tindakan_anestesi_id=$this->input->post('mtemplate_tindakan_anestesi_id');
		$parameter_id=$this->input->post('parameter_id');
		$data=array(
			'mtemplate_tindakan_anestesi_id'=>$this->input->post('mtemplate_tindakan_anestesi_id'),
			'parameter_nama'=>$this->input->post('parameter_nama'),
			'staktif'=>1,
			'inisial'=>$this->input->post('inisial'),
			'st_nilai'=>$this->input->post('st_nilai'),
		);
		if ($parameter_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mtemplate_tindakan_anestesi_param',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$parameter_id);
		    $hasil=$this->db->update('mtemplate_tindakan_anestesi_param',$data);
		}
		  
		  json_encode($hasil);
	}
	function simpan_jawaban(){
		$skor_id=$this->input->post('skor_id');
		$parameter_id=$this->input->post('parameter_id');
		$data=array(
			'parameter_id'=>$this->input->post('parameter_id'),
			'skor'=>$this->input->post('skor'),
			'deskripsi_nama'=>$this->input->post('deskripsi_nama'),
			'st_default'=>$this->input->post('st_default'),
			'staktif'=>1,
		);
		if ($skor_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mtemplate_tindakan_anestesi_param_skor',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$skor_id);
		    $hasil=$this->db->update('mtemplate_tindakan_anestesi_param_skor',$data);
		}
		  
		  json_encode($hasil);
	}
	function hapus_nilai(){
	  $parameter_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$parameter_id);
		$hasil=$this->db->update('mtemplate_tindakan_anestesi_setting_nilai',$data);
	  
	  json_encode($hasil);
	  
  }
  function hapus_parameter(){
	  $parameter_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$parameter_id);
		$hasil=$this->db->update('mtemplate_tindakan_anestesi_param',$data);
	  
	  json_encode($hasil);
	  
  }
  function hapus_jawaban(){
	  $skor_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$skor_id);
		$hasil=$this->db->update('mtemplate_tindakan_anestesi_param_skor',$data);
	  
	  json_encode($hasil);
	  
  }

  function find_parameter(){
	  $parameter_id=$this->input->post('id');
	  $q="SELECT *FROM mtemplate_tindakan_anestesi_param H WHERE H.id='$parameter_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function find_nilai(){
	  $parameter_id=$this->input->post('id');
	  $q="SELECT *FROM mtemplate_tindakan_anestesi_setting_nilai H WHERE H.id='$parameter_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function find_nilai_mas(){
	  $parameter_id=$this->input->post('id');
	  $q="SELECT *FROM mtemplate_tindakan_anestesi_setting_nilai_mas H WHERE H.id='$parameter_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function find_jawaban(){
	  $skor_id=$this->input->post('id');
	  $q="SELECT *FROM mtemplate_tindakan_anestesi_param_skor H WHERE H.id='$skor_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function load_jawaban()
	{
		$parameter_id=$this->input->post('parameter_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
							FROM `mtemplate_tindakan_anestesi_param_skor` H
							where H.staktif='1' AND H.parameter_id='$parameter_id'
							ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('deskripsi_nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->skor;
          $result[] = $r->deskripsi_nama;
          $result[] = ($r->st_default=='1'?text_danger('DEFAULT'):'');
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_jawaban('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_jawaban('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function load_nilai_mas()
	{
			$mtemplate_tindakan_anestesi_id=$this->input->post('mtemplate_tindakan_anestesi_id');
		
			$this->select = array();
			$from="
					(
						SELECT H.*
						
						FROM `mtemplate_tindakan_anestesi_setting_nilai_mas` H
						where H.staktif='1' AND H.mtemplate_tindakan_anestesi_id='$mtemplate_tindakan_anestesi_id'
						GROUP BY H.id
						ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('ref_nilai');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->skor_1.' - '.$r->skor_2;
          $result[] = $r->flag_nilai;
          $result[] = $r->ref_nilai;
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_nilai_mas('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_nilai_mas('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function simpan_nilai_mas(){
		$parameter_id=$this->input->post('parameter_id');
		$mtemplate_tindakan_anestesi_id=$this->input->post('mtemplate_tindakan_anestesi_id');
		$skor_1=$this->input->post('skor_1');
		$skor_2=$this->input->post('skor_2');
		$ref_nilai=$this->input->post('ref_nilai');
		$ref_nilai_id=$this->input->post('ref_nilai_id');
		$flag_nilai_id=$this->input->post('flag_nilai_id');
		$flag_nilai=$this->input->post('flag_nilai');
		$data=array(
			'mtemplate_tindakan_anestesi_id'=>$this->input->post('mtemplate_tindakan_anestesi_id'),
			'skor_1'=>$this->input->post('skor_1'),
			'skor_2'=>$this->input->post('skor_2'),
			'ref_nilai'=>$this->input->post('ref_nilai'),
			'ref_nilai_id'=>$this->input->post('ref_nilai_id'),
			'flag_nilai_id'=>$this->input->post('flag_nilai_id'),
			'flag_nilai'=>$this->input->post('flag_nilai'),
			'staktif'=>1,
		);
		if ($parameter_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('mtemplate_tindakan_anestesi_setting_nilai_mas',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$parameter_id);
		    $hasil=$this->db->update('mtemplate_tindakan_anestesi_setting_nilai_mas',$data);
		}
		  
		  json_encode($hasil);
	}
	function hapus_nilai_mas(){
	  $parameter_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$parameter_id);
		$hasil=$this->db->update('mtemplate_tindakan_anestesi_setting_nilai_mas',$data);
	  
	  json_encode($hasil);
	  
  }
}
