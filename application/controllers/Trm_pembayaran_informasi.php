<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';
// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

class Trm_pembayaran_informasi extends CI_Controller
{

    /**
     * Pembayaran Double Claim / Informasi Medis controller.
     * Developer @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Trm_pembayaran_informasi_model', 'model');
        $this->load->model('Thonor_dokter_model');
        $this->load->model('Tvalidasi_model');
    }
	function insert_validasi($id){
		$this->Tvalidasi_model->GenerateValidasiPendapatanRajalInfoMedis($id);
	}
    public function index()
    {
        $data = array(
            'no_pengajuan' => '',
            'nama_pemohon' => '',
            'tanggal_dari' => '',
            'tanggal_sampai' => '',
            'status_pembayaran' => '0',
        );

        $data['error'] = '';
        $data['title'] = 'Pembayaran Double Claim / Informasi Medis';
        $data['content'] = 'Trm_pembayaran_informasi/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Pembayaran Double Claim / Informasi Medis",'#'),
            array("List",'trm_pembayaran_informasi')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filter()
    {
        $data = array(
            'no_pengajuan' => $this->input->post('no_pengajuan'),
            'nama_pemohon' => $this->input->post('nama_pemohon'),
            'tanggal_dari' => $this->input->post('tanggal_dari'),
            'tanggal_sampai' => $this->input->post('tanggal_sampai'),
            'status_pembayaran' => $this->input->post('status_pembayaran'),
        );

        $this->session->set_userdata($data);

        $data['error'] = '';
        $data['title'] = 'Pembayaran Double Claim / Informasi Medis';
        $data['content'] = 'Trm_pembayaran_informasi/index';
        $data['breadcrum'] 	= array(
            array("RSKB Halmahera",'#'),
            array("Pembayaran Double Claim / Informasi Medis",'#'),
            array("Filter",'trm_pembayaran_informasi')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function proses_transaction($id)
    {
        if ($id != '') {
            $row = $this->model->getDetailTransaction($id);
            if (isset($row->id)) {
                $data = array(
                    'idpengajuan' => $row->id,
                    'no_pengajuan' => $row->no_pengajuan,
                    'tanggal_pembayaran' => ($row->tanggal_pembayaran ? $row->tanggal_pembayaran : date("d/m/Y")),
                    'tanggal_pengajuan' => $row->tanggal_pengajuan,
                    'nama_pemohon' => $row->nama_pemohon,
                    'keterangan' => $row->keterangan,
                    'jumlah_pengajuan' => number_format($row->jumlah_pengajuan),
                    'sub_total' => number_format($row->sub_total),
                    'diskon_rp' => number_format($row->diskon_rp),
                    'grand_total' => number_format($row->grand_total),
                );

                $data['error'] = '';
                $data['title'] = "Pembayaran Double Claim / Informasi Medis";
                $data['content'] = 'Trm_pembayaran_informasi/transaction';
                $data['breadcrum'] = array(
                    array("RSKB Halmahera",'#'),
                    array("Pembayaran Double Claim / Informasi Medis",'#'),
                    array("Transaksi",'Trm_pembayaran_informasi')
                );

                
                $data['nominal_pembayaran'] = 0;
                $data['sisa_pembayaran'] = 0;

                $pembayaran = $this->model->getPembayaran($id);

                if ($pembayaran) {
                    $data['detail_pembayaran'] = $this->model->getDetailPembayaran($id);
                    $data['nominal_pembayaran'] = 0;
                    $data['sisa_pembayaran'] = 0;
                } else {
                    $data['detail_pembayaran'] = array();
                    $data['nominal_pembayaran'] = 0;
                    $data['sisa_pembayaran'] = 0;
                }
                
                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('trm_pembayaran_informasi', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('trm_pembayaran_informasi');
        }
    }

    public function reject_transaction($id)
    {
        $this->model->rejectTransaction($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'transaksi telah dibatalkan.');
        redirect('trm_pembayaran_informasi', 'location');
    }

    public function approve_transaction($id)
    {
		$this->Tvalidasi_model->GenerateValidasiPendapatanRajalInfoMedis($id);
		// redirect('trm_pembayaran_informasi', 'location');
		// exit();
        if ($this->model->approveTransaction($id)) {
            $this->generateHonorDokterPengajuanInformasi($id);

            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'transaksi telah disetujui.');
            redirect('trm_pembayaran_informasi', 'location');
        }
    }

    public function print_transaction($id)
    {
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $options->set('enable-javascript', true);
        $options->set('javascript-delay', 13500);
        $options->set('enable-smart-shrinking', true);
        $options->set('no-stop-slow-scripts', true);
        $dompdf = new Dompdf($options);

        $row = $this->model->getPengajuan($id);
        if ($row) {
            $data = array();
            $data['idtransaksi'] = $row->id;
            $data['no_pengajuan'] = $row->no_pengajuan;
            $data['tanggal_pengajuan'] = DMYFormat($row->tanggal_pengajuan);
            $data['nama_pemohon'] = $row->nama_pemohon;
            $data['sub_total'] = $row->sub_total;
            $data['discount'] = $row->diskon_rp;
            $data['grand_total'] = $row->grand_total;
            $data['selisih_pembayaran'] = $row->selisih_pembayaran < 0 ? $row->selisih_pembayaran * -1 : $row->selisih_pembayaran;
            $data['user_created'] = $row->user_created;
            $data['print_version'] = 1;
            $data['print_user'] = $this->session->userdata('user_name');
        }

        $data['list_detail_pengajuan'] = $this->model->getDetailPengajuan($id);
        
        $html = $this->load->view('Trm_pembayaran_informasi/print', $data, true);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream('Faktur Transaksi Pembayaran Informasi Medis.pdf', array("Attachment"=>0));

    }

    public function save_transaction()
    {
        if ($this->model->saveTransaction()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'transaksi pembayaran telah disimpan.');
            redirect('trm_pembayaran_informasi', 'location');
        }
    }

    public function getIndex($uri='index')
    {
        $this->select = array(
            'trm_pengajuan_informasi.id',
            'trm_pengajuan_informasi.notransaksi AS no_pengajuan',
            'trm_pengajuan_informasi.tanggal AS tanggal_pengajuan',
            'trm_pengajuan_informasi.nama_pemohon',
            'trm_pengajuan_informasi.keterangan',
            'COUNT(trm_pengajuan_informasi_detail.id) AS jumlah_pengajuan',
            'SUM(trm_pengajuan_informasi_detail.nominal) AS nominal',
            'trm_pengajuan_informasi.status_pembayaran',
        );

        $this->from = 'trm_pengajuan_informasi';

        $this->join = array(
            array('trm_pengajuan_informasi_detail', 'trm_pengajuan_informasi_detail.idtransaksi = trm_pengajuan_informasi.id', 'LEFT'),
        );

        // FILTER
        $this->where  = array(
            'trm_pengajuan_informasi.status = ' => '1',
        );

        if ($uri == 'filter') {
            if ($this->session->userdata('no_pengajuan') != null) {
                $this->where = array_merge($this->where, array('trm_pengajuan_informasi.notransaksi' => $this->session->userdata('no_pengajuan')));
            }
            if ($this->session->userdata('nama_pemohon') != null) {
                $this->where = array_merge($this->where, array('trm_pengajuan_informasi.nama_pemohon LIKE' => '%'.$this->session->userdata('nama_pemohon').'%'));
        }
            if ($this->session->userdata('tanggal_dari') != null) {
                $this->where = array_merge($this->where, array('DATE(trm_pengajuan_informasi.tanggal) >=' => YMDFormat($this->session->userdata('tanggal_dari'))));
            }
            if ($this->session->userdata('tanggal_sampai') != null) {
                $this->where = array_merge($this->where, array('DATE(trm_pengajuan_informasi.tanggal) <=' => YMDFormat($this->session->userdata('tanggal_sampai'))));
            }
            if ($this->session->userdata('status_pembayaran') != '#') {
                $this->where = array_merge($this->where, array('trm_pengajuan_informasi.status_pembayaran' => $this->session->userdata('status_pembayaran')));
            } else {
                $this->where = array_merge($this->where, array('trm_pengajuan_informasi.status_pembayaran' => 0));
            }
        } else {
            $this->where = array_merge($this->where, array('DATE(trm_pengajuan_informasi.tanggal)' => date("Y-m-d")));
        }

        $this->order = array(
            'trm_pengajuan_informasi.tanggal' => 'DESC'
        );

        $this->group = array('trm_pengajuan_informasi.id');
        
        $this->having = array('nominal > ' => 0);

        $this->column_search = array('trm_pengajuan_informasi.notransaksi','trm_pengajuan_informasi.nama_pemohon');
        $this->column_order = array('trm_pengajuan_informasi.notransaksi','trm_pengajuan_informasi.nama_pemohon');

        $list = $this->datatable->get_datatables();

        $data = array();
        foreach ($list as $r) {
            $row = array();
        
            $actionDetail = '';
            if ($r->status_pembayaran == 0) {
                $actionDetail = '
                    <a href="'.site_url().'trm_pembayaran_informasi/proses_transaction/'.$r->id.'" data-toggle="tooltip" title="Proses Transaksi Pembayaran" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                ';
            } else if ($r->status_pembayaran == 1) {
                $actionDetail = '
                    <a href="'.site_url().'trm_pembayaran_informasi/reject_transaction/'.$r->id.'" data-toggle="tooltip" title="Batalkan Transaksi Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                    <a href="'.site_url().'trm_pembayaran_informasi/approve_transaction/'.$r->id.'" data-toggle="tooltip" title="Setujui Transaksi Pembayaran" class="btn btn-success btn-sm"><i class="fa fa-check"></i></a>
                    <a href="'.site_url().'trm_pembayaran_informasi/print_transaction/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Print Transaksi Pembayaran" class="btn btn-primary btn-sm"><i class="fa fa-print"></i></a>
                ';
            } else if ($r->status_pembayaran == 2) {
                $actionDetail = '
                    <a href="'.site_url().'trm_pembayaran_informasi/print_transaction/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Print Transaksi Pembayaran" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                ';
            }

            $row[] = $r->no_pengajuan;
            $row[] = DMYFormat($r->tanggal_pengajuan);
            $row[] = $r->nama_pemohon;
            $row[] = $r->keterangan;
            $row[] = number_format($r->jumlah_pengajuan);
            $row[] = number_format($r->nominal);
            $row[] = StatusPembayaranInformasi($r->status_pembayaran);
            $row[] = '<div class="btn-group">' . $actionDetail . '</div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        echo json_encode($output);
    }

    public function generateHonorDokterPengajuanInformasi($idtransaksi) {
        foreach ($this->model->getDetailPengajuan($idtransaksi) as $row) {
        
            $dokter = $this->db->query("SELECT mdokter.id,
                mdokter.id,
                mdokter.nama,
                mdokter_kategori.id AS idkategori,
                mdokter_kategori.nama AS namakategori,
                mdokter.pajak
            FROM
                mdokter
            JOIN mdokter_kategori ON mdokter_kategori.id=mdokter.idkategori
            WHERE
                mdokter.id = $row->iddokter AND
                mdokter.status = 1")->row();

            $tanggal_pembayaran = YMDFormat(getPembayaranHonorDokter($dokter->id));
            $tanggal_jatuhtempo = YMDFormat(getJatuhTempoHonorDokter($dokter->id));
            $nominal_pajak_dokter = $row->tarif_dokter * ($dokter->pajak / 100);
            $jasamedis_netto = $row->tarif_dokter - $nominal_pajak_dokter;

            $this->db->set('periode_pembayaran', $tanggal_pembayaran);
            $this->db->set('periode_jatuhtempo', $tanggal_jatuhtempo);
            $this->db->where('id', $row->iddetail);
            $this->db->update('trm_pengajuan_informasi_detail');
            
            $isStopPeriode = $this->Thonor_dokter_model->checkStatusHonorDokterStopped($dokter->id, $tanggal_pembayaran);
            if ($isStopPeriode == null) {
                $idhonor = $this->Thonor_dokter_model->checkStatusHonorDokter($dokter->id, $tanggal_pembayaran);

                if ($idhonor == null) {
                    $dataHonorDokter = array(
                        'iddokter' => $dokter->id,
                        'namadokter' => $dokter->nama,
                        'tanggal_pembayaran' => $tanggal_pembayaran,
                        'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
                        'nominal' => 0,
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => $this->session->userdata('user_id')
                    );

                    if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
                        $idhonor = $this->db->insert_id();
                    }
                }
            }

            $dataDetailHonorDokter = array(
                'idhonor' => $idhonor,
                'idtransaksi' => $idtransaksi,
                'jenis_transaksi' => 'pendapatan',
                'jenis_tindakan' => 'PENDAPATAN INFORMASI MEDIS',
                'reference_table' => 'trm_pengajuan_informasi_detail',
                'jenis_pasien' => $row->jenis_pasien,
                'idkelompok' => $row->idkelompok,
                'namakelompok' => $row->namakelompok,
                'idrekanan' => $row->idrekanan,
                'namarekanan' => $row->namarekanan,
                'iddetail' => $row->iddetail,
                'namatarif' => 'Pengajuan Informasi Medis ( ' . $row->no_medrec . ' - ' . $row->nama_pasien . ' ) - Penjuan Ke ' . $row->pengajuan_ke,
                'idkategori' => $dokter->idkategori,
                'namakategori' => $dokter->namakategori,
                'iddokter' => $dokter->id,
                'namadokter' => $dokter->nama,
                'jasamedis' => $row->tarif_dokter,
                'potongan_rs' => 0,
                'nominal_potongan_rs' => 0,
                'pajak_dokter' => $dokter->pajak,
                'nominal_pajak_dokter' => $nominal_pajak_dokter,
                'jasamedis_netto' => $jasamedis_netto,
                'tanggal_pemeriksaan' => $row->tanggal_pengajuan,
                'tanggal_pembayaran' => $tanggal_pembayaran,
                'tanggal_jatuhtempo' => $tanggal_jatuhtempo
            );

            $this->db->insert('thonor_dokter_detail', $dataDetailHonorDokter);
        }
    }
}
