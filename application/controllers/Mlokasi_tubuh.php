<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlokasi_tubuh extends CI_Controller {

	/**
	 * Lokasi Tubuh controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mlokasi_tubuh_model');
  }

	function index(){
		
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Lokasi Tubuh';
		$data['content'] 		= 'Mlokasi_tubuh/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Lokasi Tubuh",'#'),
									    			array("List",'Mlokasi_tubuh')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'deskripsi' 			=> '',
			'status' 				=> '1'
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Lokasi Tubuh';
		$data['content'] 		= 'Mlokasi_tubuh/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Lokasi Tubuh",'#'),
									    			array("Tambah",'Mlokasi_tubuh')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Mlokasi_tubuh_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'deskripsi' 					=> $row->deskripsi,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Lokasi Tubuh';
				$data['content']	 	= 'Mlokasi_tubuh/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Lokasi Tubuh",'#'),
											    			array("Ubah",'mlokasi_tubuh')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mlokasi_tubuh','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mlokasi_tubuh');
		}
	}

	function delete($id){
		// print_r($id);exit();
		$this->Mlokasi_tubuh_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mlokasi_tubuh','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mlokasi_tubuh_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mlokasi_tubuh','location');
				}
			} else {
				if($this->Mlokasi_tubuh_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mlokasi_tubuh','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 		= validation_errors();
		$data['content'] 	= 'Mlokasi_tubuh/manage';

		if($id==''){
			$data['title'] = 'Tambah Lokasi Tubuh';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Lokasi Tubuh",'#'),
															array("Tambah",'Mlokasi_tubuh')
													);
		}else{
			$data['title'] = 'Ubah Lokasi Tubuh';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Lokasi Tubuh",'#'),
															array("Ubah",'Mlokasi_tubuh')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	  
$data_user=get_acces();
$user_acces_form=$data_user['user_acces_form'];

			$this->select = array();
			$this->from   = 'mlokasi_tubuh';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'id' => 'ASC'
			);
			$this->group  = array();

      $this->column_search   = array('nama','deskripsi');
      $this->column_order    = array('nama','deskripsi');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = $r->deskripsi;
          $aksi = '<div class="btn-group">';
          
		  if (UserAccesForm($user_acces_form,array('300'))){
          	$aksi .= '<a href="'.site_url().'mlokasi_tubuh/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
          }
          
		  if (UserAccesForm($user_acces_form,array('301'))){
          	$aksi .= '<a href="#" data-urlindex="'.site_url().'mlokasi_tubuh" data-urlremove="'.site_url().'mlokasi_tubuh/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
          }
          $aksi .= '</div>';
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function ajaxSave(){
		if ($this->Mlokasi_tubuh_model->saveData()) {
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			$data['response']="data telah di simpan";
			$data['code']=200;
			$data['id']=$this->db->insert_id();
			$data['nama']=$this->input->post("nama");
			echo json_encode($data);
		}
	}
}
