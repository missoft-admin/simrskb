<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
class Tmutasi_kas extends CI_Controller {

	/**
	 * Mutasi Kas controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tmutasi_kas_model');
		$this->load->model('Mrka_kegiatan_model');
		$this->load->model('Mrka_bendahara_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function insert_validasi_mutasi($id){
		 $this->Tmutasi_kas_model->insert_validasi_mutasi($id);		
	}
	function index(){
		$data = array(
			'tanggal_setoran2'=>'',
			'tanggal_setoran1'=>'',
			'tanggal_trx1'=>'',
			'tanggal_trx2'=>'',
			'status'=>'#',
			'notransaksi'=>'',
			'deskripsi'=>'',
			'dari'=>'#',
			'ke'=>'#',
		);
		$data['list_sumber_kas'] 	= $this->Tmutasi_kas_model->list_sumber_kas();
		$data['error'] 			= '';
		$data['title'] 			= 'Mutasi Kas';
		$data['content'] 		= 'Tmutasi_kas/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("SETORAN KAS",'#'),
									    			array("List",'Tmutasi_kas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	
	  $notransaksi=$this->input->post('notransaksi');
	  $dari=$this->input->post('dari');
	  $ke=$this->input->post('ke');
	  $tanggal_trx1=$this->input->post('tanggal_trx1');
	  $tanggal_trx2=$this->input->post('tanggal_trx2');
	  $status=$this->input->post('status');
	 
	  $iduser=$this->session->userdata('user_id');
	  $where='';
	  if ($notransaksi !=''){
		  $where .=" AND H.notransaksi='$notransaksi'";
	  }
	  if ($status !='#'){
		  if ($status=='1'){
			  $where .=" AND H.st_verifikasi='1'";
		  }
		  if ($status=='2'){
			  $where .=" AND H.st_verifikasi='0'";
		  }
		  if ($status=='3'){
			  $where .=" AND H.status='0'";
		  }
		 
	  }
	   if ($tanggal_trx1 !='' || $tanggal_trx2 !=''){
		  $where .=" AND (H.tanggal_trx >='".YMDFormat($tanggal_trx1)."' AND H.tanggal_trx <='".YMDFormat($tanggal_trx2)."' )";
	  }
	  if ($dari !='#'){
			$where .=" AND H.dari='$dari'";
	  }
	  if ($ke !='#'){
			$where .=" AND H.ke='$ke'";
	  }
	 
	  $from="(
				SELECT MD.nama as nama_dari,MK.nama as nama_ke,U.`name` as nama_user, H.* ,COUNT(DISTINCT Doc.id) as doc
					FROM tmutasi_kas H  
					LEFT JOIN msumber_kas MD ON MD.id=H.dari
					LEFT JOIN msumber_kas MK ON MK.id=H.ke
					LEFT JOIN musers U ON U.id=H.user_created
					LEFT JOIN tmutasi_kas_dokumen Doc ON Doc.idtransaksi=H.id
					WHERE H.id is not null ".$where." 
					GROUP BY H.id	
					ORDER BY H.id DESC
				) as tbl";
	// print_r($tanggal_setoran1);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama_ke','nama_dari','nama_user','notransaksi');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
            $no++;
            $row = array();

            $action = '';
            $status = '';
			if ($r->status=='1'){
					$action .= '<a href="'.site_url().'tmutasi_kas/update/'.$r->id.'/disabled" data-toggle="tooltip" title="Lihat" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>';
				if ($r->st_verifikasi=='0'){
					if ($r->st_approval=='0'){
						if (UserAccesForm($user_acces_form,array('1434'))){
							$action .= '<a href="'.site_url().'tmutasi_kas/update/'.$r->id.'" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>';
						}
					}
					
					if ($r->status_approval=='1'){
						$action .= '<button class="btn btn-primary btn-sm verif"><i class="fa fa-check"></i></button>';
					}else{
						if ($r->st_approval=='0'){
							$action .= '<button class="btn btn-primary btn-sm" onclick="load_user_approval('.$r->id.','.$r->dari.','.$r->ke.',\''.$r->nominal.'\')"><i class="fa fa-send"></i></button>';						
						}
					}
					if ($r->st_approval=='1' && $r->status_approval=='0'){
						$status='<span class="label label-primary" data-toggle="tooltip" title="MENUNGGU">Menunggu Persetujuan</span>';
						$status .='<br><br><button class="btn btn-danger btn-xs" onclick="lihat_user('.$r->id.')"><i class="si si-user-following"></i></button>';
					}elseif ($r->st_approval=='1' && $r->status_approval=='1'){
						$status='<span class="label label-warning" data-toggle="tooltip" title="Disetujuain">BELUM DIVERIFIKASI</span>';
						$status .='<br><br>'.text_success('DISETUJUI').'&nbsp;<button class="btn btn-danger btn-xs" onclick="lihat_user('.$r->id.')"><i class="si si-user-following"></i></button>';
					}elseif ($r->st_approval=='1' && $r->status_approval=='2'){
						$status='<span class="label label-danger" data-toggle="tooltip" title="Disetujuain">DITOLAK</span>';
						$status .='<br><br><button class="btn btn-danger btn-xs" onclick="lihat_user('.$r->id.')"><i class="si si-user-following"></i></button>';
					}else{
						$status='<span class="label label-warning" data-toggle="tooltip" title="DIBATALKAN">BELUM DIVERIFIKASI</span>';
						
					}
				}else{
					$status='<span class="label label-success" data-toggle="tooltip" title="DIBATALKAN">SUDAH DIVERIFIKASI</span>';
				}
				if ($r->status_approval!='2'){
				$action .= '<a href="'.site_url().'tmutasi_kas/kwitansi/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Cetak Kwitansi" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
				if ($r->doc){
					$action .= '<a href="'.base_url().'tmutasi_kas/upload_document/'.$r->id.'" title="Upload Document" class="btn btn-sm btn-warning" ><i class="fa fa-file-image-o"></i></a>';
				  }else{
					$action .= '<a href="'.base_url().'tmutasi_kas/upload_document/'.$r->id.'" title="Upload Document" class="btn btn-sm btn-danger" ><i class="fa fa-upload"></i></a>';
					  
				  }
				}
			}else{
				$action='<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">DIBATALKAN</span>';
				$status='<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">DIBATALKAN</span>';
			}
  					
            $row[] = $r->id;
            $row[] = $no;
  			$row[] = $r->notransaksi;
  			$row[] = HumanDateShort($r->tanggal_trx);
  			$row[] = ($r->nama_dari);
  			$row[] = ($r->nama_ke);
  			$row[] = ($r->deskripsi);
            $row[] = number_format($r->nominal);
  			$row[] = ($r->nama_user);
            $row[] = $status;
            $row[] = '<div class="btn-group">'.$action.'</div>';
            $data[] = $row;
        }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
	public function save()
    {
		if($this->input->post('id') == '' ) {
			if($this->Tmutasi_kas_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tmutasi_kas','location');
			}
		} else {
			if($this->Tmutasi_kas_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tmutasi_kas','location');
			}
		}
    
    }
	function create($disabel=''){
		$data = array(
			'id' 						=> '',			
			'idpegawai' 						=> '#',			
			'nama_pegawai' 						=> '',			
			'idmetode' 						=> '#',			
			'dari' 						=> '#',			
			'nominal' 						=> '0',			
			'st_verifikasi' 						=> '0',			
			'deskripsi' 						=> '',			
			'cash_rajal' 						=> '0',			
			'cash_ranap' 						=> '0',			
			'cash_pendapatan' 						=> '0',			
			'tanggal_trx' 	=> date('d-m-Y'),
			'tanggal_setoran' 	=> date('d-m-Y'),
			
		);
		$data['list_sumber_kas'] 	= $this->Tmutasi_kas_model->list_sumber_kas();
		// $data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
		// $data['list_pembayaran'] 			= array();
		$data['disabel'] 			= $disabel;
		$data['error'] 			= '';
		$data['title'] 			= 'Mutasi Kas';
		$data['content'] 		= 'Tmutasi_kas/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Mutasi Kas",'#'),
									    			array("Tambah",'Tmutasi_kas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function refresh_koneksi($id){
		$q="SELECT H.sumber_kas_koneksi as id,M.nama 
			FROM msumber_kas_koneksi H
			LEFT JOIN msumber_kas M ON M.id=H.sumber_kas_koneksi
			WHERE H.sumber_kas_id='$id'";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		
		$q="SELECT M.saldo FROM msumber_kas M
			WHERE M.id='$id'";
		$arr['saldo_dari']=$this->db->query($q)->row('saldo');
		$this->output->set_output(json_encode($arr));
		
	}
	function get_saldo_ke($id){
		
		$q="SELECT M.saldo FROM msumber_kas M
			WHERE M.id='$id'";
		$arr['saldo_ke']=$this->db->query($q)->row('saldo');
		$this->output->set_output(json_encode($arr));
		
	}
	
  function verif($id){
		// $id=$this->input->post('id');
		$query="UPDATE tmutasi_kas INNER JOIN (
			SELECT H.id,H.dari,MD.saldo as saldoawal_dari,H.ke,MK.saldo as saldoawal_ke
			,MD.saldo - H.nominal as saldoakhir_dari,MK.saldo+H.nominal as saldoakhir_ke
			from tmutasi_kas H 
			LEFT JOIN msumber_kas MD ON MD.id=H.dari
			LEFT JOIN msumber_kas MK ON MK.id=H.ke
			WHERE H.id='$id') T ON T.id=tmutasi_kas.id
			SET 
				tmutasi_kas.saldoawal_dari=T.saldoawal_dari
				,tmutasi_kas.saldoawal_ke=T.saldoawal_ke
				,tmutasi_kas.saldoakhir_dari=T.saldoakhir_dari
				,tmutasi_kas.saldoakhir_ke=T.saldoakhir_ke
				WHERE tmutasi_kas.id=T.id";
		$result=$this->db->query($query);
		$result=$this->db->query("UPDATE tmutasi_kas set st_verifikasi='1' WHERE id='$id'");
		$this->Tmutasi_kas_model->insert_validasi_mutasi($id);		
		echo json_encode($result);
	}
	function hapus($id){
		// $id=$this->input->post('idtsetoran_kas
		$result=$this->db->query("UPDATE tmutasi_kas set status='0' WHERE id='$id'");
		echo json_encode($result);
	}
  public function update($id,$disabel='')
    {
      if($id != ''){
        $data = $this->Tmutasi_kas_model->getSpecified($id);
		// print_r($data);exit();
		$data['list_sumber_kas'] 	= $this->Tmutasi_kas_model->list_sumber_kas();
          $data['disabel']      = $disabel;
          $data['error']      = '';
          $data['title']      = 'Ubah Transaksi Mutasi Kas';
          $data['content']    = 'Tmutasi_kas/manage';
          $data['breadcrum']  = array(
                                 array("RSKB Halmahera",'dashboard'),
                                 array("Bendahara",'#'),
                                 array("Transaksi Mutasi Kas",'tmutasi_kas'),
                                 array("Ubah",'')
                              );
        $data = array_merge($data,backend_info());
        $this->parser->parse('module_template',$data);
       
      }else{
        $this->session->set_flashdata('error',true);
        $this->session->set_flashdata('message_flash','data tidak ditemukan.');
        redirect('Tmutasi_kas');
      }
    }
	public function upload_document($idtransaksi)
    {
       
        $data = $this->Tmutasi_kas_model->getSpecifiedHeader($idtransaksi);
		// print_r($data);exit();
       

        $data['error']      = '';
        $data['title'] = 'Upload Dokumen Mutasi Kas';
        $data['content'] = 'Tmutasi_kas/upload_dokumen';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Mutasi Kas", 'Tmutasi_kas/index'),
            array("Upload Dokumen", '#')
        );

        // $data['listFiles'] = array();
        $data['listFiles'] = $this->Tmutasi_kas_model->getListUploadedDocument($idtransaksi);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	public function upload_files()
    {
        $files = array();
        foreach ($_FILES as $file) {
            if ($file['name'] != '') {
                $config['upload_path'] = './assets/upload/mutasi_kas/';
                $config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';

                $files[] = $file['name'];

                $this->upload->initialize($config);

                if ($this->upload->do_upload('file')) {
                    $image_upload = $this->upload->data();
                    $table = 'tmutasi_kas_dokumen';

                    $data = array();
                    $data['idtransaksi']  = $this->input->post('idtransaksi');
                    $data['keterangan']  = $this->input->post('keterangan');
                    $data['upload_by']  = $this->session->userdata('user_id');
                    $data['upload_by_nama']  = $this->session->userdata('user_name');
                    $data['upload_date']  = date('Y-m-d H:i:s');
                    $data['filename'] = $image_upload['file_name'];
                    $data['size'] = formatSizeUnits($file['size']);

                    $this->db->insert($table, $data);

                    return true;
                } else {
                    print_r($this->upload->display_errors());
                    exit();

                    return false;
                }
            } else {
                return true;
            }
        }
    }
	function refresh_image($id){		
		$arr['detail'] = $this->Tmutasi_kas_model->refresh_image($id);
		$this->output->set_output(json_encode($arr));
	}
	 public function delete_file($idfile)
    {
        $this->db->where('id', $idfile);
        $query = $this->db->get('tmutasi_kas_dokumen');
        $row = $query->row();
        if ($query->num_rows() > 0) {
            $this->db->where('id', $idfile);
            if ($this->db->delete('tmutasi_kas_dokumen')) {
                if (file_exists('./assets/upload/mutasi_kas/'.$row->filename) && $row->filename !='') {
                    unlink('./assets/upload/mutasi_kas/'.$row->filename);
                }
            }
        }

        return true;
    }
	public function kwitansi($id)
    {
      // instantiate and use the dompdf class
      $dompdf = new Dompdf();

      if($id != ''){
        $data = $this->Tmutasi_kas_model->getSpecifiedHeader($id);
		// print_r($data);exit();
          $data['error']      = '';
          $data['title']      = 'Bukti Mutasi Kas';
          $data['content']    = 'Tmutasi_kas/kwitansi';
          $data['breadcrum']  = array(
                                 array("RSKB Halmahera",'dashboard'),
                                 array("Bendahara",'#'),
                                 array("Mutasi",'tmutasi_kas'),
                                 array("Invoice",'')
                              );

          // $data['list_biaya'] = $this->Tmutasi_kas_model->getListBiaya();
          // $data['list_kasbesar'] = $this->Tmutasi_kas_model->getListKasBesar();
          // $data['list_pembayaran'] = $this->Tmutasi_kas_model->getListPembayaran($id);

          $html = $this->load->view('Tmutasi_kas/kwitansi', $data, true);

          $dompdf->loadHtml($html);

          // (Optional) Setup the paper size and orientation
          $dompdf->setPaper('A4', 'portrait');

          // Render the HTML as PDF
          $dompdf->render();

          // // Output the generated PDF to Browser
          $dompdf->stream('Bukti Mutasi.pdf', array("Attachment"=>false));
          exit(0);
        }else{
          $this->session->set_flashdata('error',true);
          $this->session->set_flashdata('message_flash','data tidak ditemukan.');
          redirect('Tpendapatan','location');
        }
     
    }
	function load_user_approval()
    {
	  $dari=$this->input->post('dari');
	  $ke=$this->input->post('ke');
	  $nominal=$this->input->post('nominal');
	  $where='';
	  $from="(
				SELECT S.step,U.`name` as user_nama,S.proses_setuju,S.proses_tolak 
				FROM mlogic_mutasi S
				LEFT JOIN musers U ON U.id=S.iduser
				WHERE S.dari='$dari' AND S.ke='$ke' AND calculate_logic(S.operand, '$nominal', S.nominal)='1' AND S.status='1'
				ORDER BY S.step,S.id
				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('user_nama');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $no_step='';
      foreach ($list as $r) {
          $no++;
          $row = array();
		  if ($no_step != $r->step){
				$no_step=$r->step;
		  }
		  if ($no_step % 2){
			$row[] = '<span class="label label-default">STEP '.$r->step.'</span>';
		  }else{
			   $row[] = '<span class="label label-primary">STEP '.$r->step.'</span>';
		  }
          $row[] = $r->user_nama;
          $row[] = ($r->proses_setuju=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
          $row[] = ($r->proses_tolak=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function simpan_proses_peretujuan($id){

		$result=$this->Tmutasi_kas_model->simpan_proses_peretujuan($id);
		echo json_encode($result);
  }
  function list_user($id){
		$q="SELECT *FROM tmutasi_kas_approval H WHERE H.idtransaksi='$id'";
		$row=$this->db->query($q)->result();
		$content='';
		$no=1;
		$no_step="1";
		foreach($row as $r){
			$st_user_lain='';
			$content .='<tr class="'.($r->st_aktif=='1' && $r->approve=='0'?'success':'active').'">';
			if ($no_step != $r->step){
				$no_step=$r->step;
			}
			if ($no_step % 2){
				$content .='<td> <span class="label label-default" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
			}else{
				$content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
				// $content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span></td>';
			}
			if ($r->approve !='0'){
				if ($r->iduser !=$r->user_eksekusi){
					$st_user_lain=' <span class="label label-warning" data-toggle="tooltip">Oleh User Lain</span>';
				}
			}
			$content .='<td><i class="si si-users fa-2x text-black-op"></i>  '.$r->user_nama.'</td>';
			$content .='<td>'.($r->st_aktif=='1'?status_approval($r->approve):'').' '.$st_user_lain.'</td>';
			$content .='</tr>';

		}
		$arr['detail']=$content;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($content));
	}
}
