<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tpengelolaan_approval extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpengelolaan_approval_model','model');
	}
	function create(){
		$data=array(
			'disabel'=>'',
			'id'=>'',
			'list_trx'=>'',
			'no_terima'=>'',
			'tanggal_hitung1'=>'',
			'tanggal_hitung2'=>'',
		);
		$data['error'] 			= '';
		// $data['list_trx'] 	= array();
		$data['list_kategori'] 	= $this->model->list_kategori();
		$data['title'] 			= 'Create Approval Pengelolaan Hutang & Piutang';
		$data['content'] 		= 'Tpengelolaan_approval/create';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Pengelolaan Hutang & Piutang Transaksi",'tpengelolaan_approval/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function save(){
		// print_r($this->input->post());exit();
		$list_trx=$this->input->post('list_trx');
		if($this->model->saveData($list_trx)){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			// redirect('tpengelolaan_approval','location');
			 redirect('tpengelolaan_approval/index', 'location');
		}
	
	}
	
	function index() {
		
		$data=array(
			'status'=>'1',
			'mbagi_hasil_id'=>'#',
			'deskripsi'=>'',
			'no_terima'=>'',
			'tanggal_hitung1'=>'',
			'tanggal_hitung2'=>'',
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		// $data['list_mbagi_hasil'] 	= $this->model->list_mbagi_hasil();
		$data['title'] 			= 'Approval Pengelolaan Hutang & Piutang';
		$data['content'] 		= 'Tpengelolaan_approval/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Pengelolaan Hutang & Piutang Transaksi",'tpengelolaan_approval/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		 $iduser=$this->session->userdata('user_id');	

		$notransaksi=$this->input->post('notransaksi');
		$nama_pengelolaan=$this->input->post('nama_pengelolaan');
		$status=$this->input->post('status');
		
		$tanggal_hitung1=$this->input->post('tanggal_hitung1');
		$tanggal_hitung2=$this->input->post('tanggal_hitung2');
		
		$where='';
		if ($notransaksi !=''){
			$where .=" AND H.notransaksi LIKE '%".$notransaksi."%' ";
		}
		if ($nama_pengelolaan !=''){
			$where .=" AND H.nama_pengelolaan LIKE '%".$nama_pengelolaan."%' ";
		}
		
		
		if ('' != $tanggal_hitung1) {
            $where .= " AND DATE(H.tanggal_hitung) >='".YMDFormat($tanggal_hitung1)."' AND DATE(H.tanggal_hitung) <='".YMDFormat($tanggal_hitung2)."'";
        }
		
		if ($status !='#'){
			$where .=" AND H.st_approval='$status' ";
		}


        $from = "(
					SELECT H.id,H.notransaksi,H.idpengelolaan,H.tanggal_hitung,H.nama_pengelolaan,H.deskripsi_pengelolaan,H.nominal,H.`status`,H.st_proses,H.st_approval 
					,(SELECT MAX(HH.step) step FROM tpengelolaan_approval HH WHERE HH.tpengelolaan_id=H.id AND HH.st_aktif='1') as step
					from tpengelolaan H
					LEFT JOIN tpengelolaan_approval A ON A.tpengelolaan_id=H.id
					WHERE H.st_proses='2' AND A.iduser IN(".$iduser.") ".$where."
					GROUP BY H.id
					ORDER BY H.id DESC
				) as tbl ";


		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('noreg');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables');
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
			
            $status     = '';
			$respon='';
            $status_setting     = '';
            // if ($r->status=='2'){
				$status_setting='<button title="Lihat User" class="btn btn-danger btn-xs user" onclick="load_user('.$r->id.')"><i class="si si-user"></i></button>';
			// }
			$query=$this->get_respon($r->id);
			if ($query){
			foreach ($query as $res){
				if ($res->iduser==$iduser && $res->step==$r->step){
					if ($res->approve=='0'){
					  $aksi .= '<button title="Setuju" class="btn btn-success btn-xs setuju" onclick="setuju('.$res->id.','.$r->id.')"><i class="fa fa-check"></i> SETUJUI</button>';
					  $aksi .= '<button title="Tolak" class="btn btn-danger btn-xs tolak" onclick="tolak('.$res->id.','.$r->id.')"><i class="si si-ban"></i> TOLAK</button>';
					}else{
					  $aksi .= '<button title="Reset" class="btn btn-warning btn-xs" onclick="batal('.$res->id.','.$r->id.')"><i class="fa fa-refresh"></i></button>';
					  if ($r->st_approval=='3'){
						$aksi .= '<span class="label label-danger">DITOLAK</span>';						  
					  }
					}
				}
				if ($res->iduser==$iduser){
					$respon .='<span class="label label-warning" data-toggle="tooltip">STEP '.$res->step.'</span> : '.status_approval($res->approve).'<br>';
				}
			}
			  
			  
		  }else{
			   $respon='';
		  }
            $row[] = $r->id;
            $row[] = $no;
            $row[] = $r->notransaksi;
            $row[] = $r->nama_pengelolaan;
            $row[] = HumanDateShort($r->tanggal_hitung);
            $row[] = $r->deskripsi_pengelolaan;
            $row[] = number_format($r->nominal,2);
            $row[] = status_approval_HD($r->st_approval,$r->step).' '.$status_setting;			
            $row[] = $respon;//8            
				
			$aksi.='</div>';
            $row[] = $aksi;//8            
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	
	function get_respon($id){
	  $iduser=$this->session->userdata('user_id');
	  $q="SELECT *from tpengelolaan_approval AP
			WHERE AP.st_aktif='1' AND AP.iduser='$iduser' AND AP.tpengelolaan_id='$id'";
	  return $this->db->query($q)->result();
  }
	function list_user($id){
		$q="SELECT *FROM tpengelolaan_approval H WHERE H.tpengelolaan_id='$id'";
		$row=$this->db->query($q)->result();
		$content='';
		$no=1;
		$no_step="1";
		foreach($row as $r){
			$st_user_lain='';
			$content .='<tr class="'.($r->st_aktif=='1' && $r->approve=='0'?'success':'active').'">';
			if ($no_step != $r->step){
				$no_step=$r->step;
			}
			if ($no_step % 2){
				$content .='<td> <span class="label label-default" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
			}else{
				$content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span> <br><br>'.($r->proses_setuju=='1'?'<span class="label label-success" data-toggle="tooltip">SETUJU LANGSUNG</span>':'').' '.($r->proses_tolak=='1'?'<span class="label label-danger" data-toggle="tooltip">TOLAK LANGSUNG</span>':'').'</td>';
				// $content .='<td> <span class="label label-primary" data-toggle="tooltip">STEP '.$r->step.'</span></td>';
			}
			if ($r->approve !='0'){
				if ($r->iduser !=$r->user_eksekusi){
					$st_user_lain=' <span class="label label-warning" data-toggle="tooltip">Oleh User Lain</span>';
				}
			}
			$content .='<td><i class="si si-users fa-2x text-black-op"></i>  '.$r->user_nama.'</td>';
			$content .='<td>'.($r->st_aktif=='1'?status_approval_HD($r->approve):'').' '.$st_user_lain.'</td>';			
			$content .='</tr>';
			
		}
		$arr['detail']=$content;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($content));
	}
	function setuju_batal($id,$status,$idrka=''){
		// $arr=array();
		$q="call update_tpengelolaan_approval('$id', $status) ";
		$result=$this->db->query($q);
		// // $arr['st_insert']=$result;
		// $arr=$this->db->query("SELECT H.st_bendahara FROM rka_pengajuan H  WHERE H.id='$idrka' AND H.status='2'")->row('st_bendahara');
		$this->output->set_output(json_encode($result));
	}
	function tolak(){
		$id=$this->input->post('id_approval');
		$alasan_tolak=$this->input->post('alasan_tolak');
		$q="call update_tpengelolaan_approval('$id', 2) ";
		$this->db->query($q);
		$result=$this->db->update('tpengelolaan_approval',array('alasan_tolak'=>$alasan_tolak),array('id'=>$id));
		echo json_encode($result);
	}
}
