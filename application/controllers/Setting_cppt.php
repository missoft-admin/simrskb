<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_cppt extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Setting_cppt_model');
		$this->load->helper('path');
		
  }


	function index($tab='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('1752'))){
				$tab='1';
			}elseif (UserAccesForm($user_acces_form,array('1753'))){
				$tab='2';
			}
		}
		if (UserAccesForm($user_acces_form,array('1752','1753'))){
			$data = $this->Setting_cppt_model->get_assesmen_setting();
			// print_r($data_spesifik);exit;
			$data['tab'] 			= $tab;
			$data['spesialisasi_id'] 			= '#';
			$data['profesi_id'] 			= '#';
			$data['profesi_id_field'] 			= '#';
			$data['mppa_id'] 			= '#';
			$data['st_lihat'] 			= '0';
			$data['st_input'] 			= '0';
			$data['st_edit'] 			= '0';
			$data['st_hapus'] 			= '0';
			$data['st_cetak'] 			= '0';
			$data['field_subjectif']	='0';
			$data['field_objectif']	='0';
			$data['field_assesmen']	='0';
			$data['field_planing']	='0';
			$data['field_intruction']	='0';
			$data['field_intervensi']	='0';
			$data['field_evaluasi']	='0';
			$data['field_reasessmen']	='0';
			$data['field_assesmen_a']	='0';
			$data['field_diagnosis_a']	='0';
			$data['field_intervensi_a']	='0';
			$data['field_monitoring_a']	='0';

			$data['error'] 			= '';
			$data['title'] 			= 'Setting CPPT';
			$data['content'] 		= 'Setting_cppt/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Pengkajian CPPT Setting",'setting_cppt/index')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function save_assesmen(){
		if ($this->Setting_cppt_model->save_assesmen()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_cppt/index/1','location');
		}
	}
	function save_default(){
		if ($this->Setting_cppt_model->save_defaultsave_default()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_cppt/index/3','location');
		}
	}

  function simpan_hak_akses(){
		$hasil=$this->cek_duplicate_hak_akses($this->input->post('profesi_id'),$this->input->post('spesialisasi_id'),$this->input->post('mppa_id'));
		if ($hasil==null){	
			$this->profesi_id=$this->input->post('profesi_id');
			$this->spesialisasi_id=$this->input->post('spesialisasi_id');
			$this->mppa_id=$this->input->post('mppa_id');
			$this->st_lihat=$this->input->post('st_lihat');
			$this->st_input=$this->input->post('st_input');
			$this->st_edit=$this->input->post('st_edit');
			$this->st_hapus=$this->input->post('st_hapus');
			$this->st_cetak=$this->input->post('st_cetak');
			
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
		
			$hasil=$this->db->insert('setting_cppt_akses',$this);
		}else{
			$hasil=null;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_hak_akses($profesi_id,$spesialisasi_id,$mppa_id){
		$q="SELECT *FROM setting_cppt_akses WHERE profesi_id='$profesi_id' AND spesialisasi_id='$spesialisasi_id' AND mppa_id='$mppa_id'";
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	function simpan_field(){
		$hasil=$this->cek_duplicate_field($this->input->post('profesi_id'));
		if ($hasil==null){	
			$this->profesi_id=$this->input->post('profesi_id');
			$this->field_subjectif=$this->input->post('field_subjectif');
			$this->field_objectif=$this->input->post('field_objectif');
			$this->field_assesmen=$this->input->post('field_assesmen');
			$this->field_planing=$this->input->post('field_planing');
			$this->field_intruction=$this->input->post('field_intruction');
			$this->field_intervensi=$this->input->post('field_intervensi');
			$this->field_evaluasi=$this->input->post('field_evaluasi');
			$this->field_reasessmen=$this->input->post('field_reasessmen');
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
			$this->field_assesmen_a=$this->input->post('field_assesmen_a');
			$this->field_diagnosis_a=$this->input->post('field_diagnosis_a');
			$this->field_intervensi_a=$this->input->post('field_intervensi_a');
			$this->field_monitoring_a=$this->input->post('field_monitoring_a');
		
			$hasil=$this->db->insert('setting_cppt_filed',$this);
		}else{
			$hasil=null;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_field($profesi_id){
		$q="SELECT *FROM setting_cppt_filed WHERE profesi_id='$profesi_id'";
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	function load_field()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*,P.ref as profesi
							
							FROM `setting_cppt_filed` H
							LEFT JOIN merm_referensi P ON P.nilai=H.profesi_id AND P.ref_head_id='21' AND P.`status`='1'
							ORDER BY H.profesi_id



					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_ppa','spesialisasi','profesi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->profesi);
          $result[] = $this->status_cppt($r->field_subjectif);
          $result[] = $this->status_cppt($r->field_objectif);
          $result[] = $this->status_cppt($r->field_assesmen);
          $result[] = $this->status_cppt($r->field_planing);
          $result[] = $this->status_cppt($r->field_intruction);
          $result[] = $this->status_cppt($r->field_intervensi);
          $result[] = $this->status_cppt($r->field_evaluasi);
          $result[] = $this->status_cppt($r->field_reasessmen);
		  
		  $result[] = $this->status_cppt($r->field_assesmen_a);
          $result[] = $this->status_cppt($r->field_diagnosis_a);
          $result[] = $this->status_cppt($r->field_intervensi_a);
          $result[] = $this->status_cppt($r->field_monitoring_a);
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_field('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function status_cppt($id){
		$label='';
		if ($id=='1'){
			$label=text_success('INPUT');
		}
		if ($id=='2'){
			$label=text_primary('MELIHAT');
		}
		if ($id=='0'){
			$label=text_danger('TIDAK');
		}
		return $label;
  }
	function simpan_formulir(){
		// print_r();exit;
		$hasil=$this->get_duplicat_logic_formulir($this->input->post('idtipe'),$this->input->post('idpoli'),$this->input->post('statuspasienbaru'),$this->input->post('pertemuan_id'),$this->input->post('st_tujuan_terakhir'),$this->input->post('operand'),$this->input->post('lama_terakhir_tujan'));
		if ($hasil==null){
			
			$this->idtipe=$this->input->post('idtipe');
			$this->idpoli=$this->input->post('idpoli');
			$this->statuspasienbaru=$this->input->post('statuspasienbaru');
			$this->pertemuan_id=$this->input->post('pertemuan_id');
			$this->st_tujuan_terakhir=$this->input->post('st_tujuan_terakhir');
			$this->operand=$this->input->post('operand');
			$this->lama_terakhir_tujan=$this->input->post('lama_terakhir_tujan');
			$this->st_formulir=$this->input->post('st_formulir');
			
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
			
		  $hasil=$this->db->insert('setting_cppt_tampil_formulir',$this);
		}else{
			$hasil=null;
		}
		  
		  $this->output->set_output(json_encode($hasil));
	}
	
	function get_duplicat_logic_formulir($idtipe,$idpoli,$statuspasienbaru,$pertemuan_id,$st_tujuan_terakhir,$operand,$lama_terakhir_tujan){
		$$idpoli=($idpoli=='#'?0:$idpoli);
		$$pertemuan_id=($pertemuan_id=='#'?0:$pertemuan_id);
		if ($operand!='0'){
			
		$where=" AND lama_terakhir_tujan='$lama_terakhir_tujan'";
		}else{
		$where=" ";
			
		}
		$q="SELECT *FROM setting_cppt_tampil_formulir WHERE idtipe='$idtipe' AND idpoli='$idpoli' AND statuspasienbaru='$statuspasienbaru'  AND st_tujuan_terakhir='$st_tujuan_terakhir' 
				AND pertemuan_id='$pertemuan_id' AND operand='$operand' ".$where;
		// print_r($q);exit;
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	function load_hak_akses()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.id,H.profesi_id,P.ref as profesi
							,H.spesialisasi_id,CASE WHEN H.spesialisasi_id=0 THEN 'SEMUA' ELSE S.ref END as spesialisasi 
							,H.mppa_id,M.nama as nama_ppa,H.st_lihat,H.st_input,H.st_edit,H.st_hapus,H.st_cetak
							FROM `setting_cppt_akses` H
							LEFT JOIN merm_referensi P ON P.nilai=H.profesi_id AND P.ref_head_id='21' AND P.`status`='1'
							LEFT JOIN merm_referensi S ON S.nilai=H.spesialisasi_id AND S.ref_head_id='22'  AND S.`status`='1'
							LEFT JOIN mppa M ON M.id=H.mppa_id AND M.staktif='1'
							ORDER BY H.profesi_id,H.spesialisasi_id



					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_ppa','spesialisasi','profesi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->profesi);
          $result[] = ($r->spesialisasi_id=='0'?text_default($r->spesialisasi):$r->spesialisasi);
          $result[] = ($r->mppa_id=='0'?text_default('SEMUA'):$r->nama_ppa);
          $result[] = ($r->st_lihat?text_success('IZINKAN'):text_danger('TIDAK'));
          $result[] = ($r->st_input?text_success('IZINKAN'):text_danger('TIDAK'));
          $result[] = ($r->st_edit?text_success('IZINKAN'):text_danger('TIDAK'));
          $result[] = ($r->st_hapus?text_success('IZINKAN'):text_danger('TIDAK'));
          $result[] = ($r->st_cetak?text_success('IZINKAN'):text_danger('TIDAK'));
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_hak_akses('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_hak_akses(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_cppt_akses',$this);
	  
	  json_encode($hasil);
	  
  }
  function hapus_field(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_cppt_filed',$this);
	  
	  json_encode($hasil);
	  
  }

  function find_mppa($profesi_id){
	  $q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='$profesi_id' AND H.staktif='1'";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($profesi_id!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
  function find_poli($idtipe){
	  $q="SELECT *FROM mpoliklinik H WHERE H.idtipe='$idtipe' AND H.`status`='1'";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($idtipe!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
	function load_formulir()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT 
						CASE WHEN H.idpoli !='0' THEN MP.nama ELSE 'ALL' END as nama_poli
						,CASE WHEN H.pertemuan_id !='0' THEN K.ref ELSE 'ALL' END as kasus
						,CASE WHEN H.idtipe ='1' THEN 'IGD' ELSE 'POLIKLINIK' END as nama_tipe
						,H.* FROM setting_cppt_tampil_formulir H
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli
						LEFT JOIN merm_referensi K ON K.nilai=H.pertemuan_id AND K.ref_head_id='15'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_poli','kasus','nama_tipe');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = GetTipePasienPiutang($r->idtipe);
          $result[] = ($r->idpoli=='0'?text_default($r->nama_poli):$r->nama_poli);
          $result[] = GetPertemuan($r->statuspasienbaru);
          $result[] = ($r->pertemuan_id=='0'?text_default($r->kasus):$r->kasus);
          $result[] = ($r->st_tujuan_terakhir=='1'?'YA':text_danger('TIDAK'));
          $result[] = ($r->operand=='0'?text_default('TIDAK DITENTUKAN'):$r->operand.' '.$r->lama_terakhir_tujan);
          $result[] = GetFormulir($r->st_formulir);
		  if (UserAccesForm($user_acces_form,array('1604'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_formulir('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_default()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT *FROM (SELECT 
						CASE WHEN H.idpoli !='0' THEN MP.nama ELSE 'ALL' END as nama_poli
						,CASE WHEN H.pertemuan_id !='0' THEN K.ref ELSE 'ALL' END as kasus
						,CASE WHEN H.idtipe ='1' THEN 'IGD' ELSE 'POLIKLINIK' END as nama_tipe
						,MN.singkatan
						,H.* FROM msetting_cppt_default_detail H
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli
						LEFT JOIN mCPPT MN ON MN.id=H.mCPPT_id
						LEFT JOIN merm_referensi K ON K.nilai=H.pertemuan_id AND K.ref_head_id='15'
						) T ORDER BY T.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_poli','kasus','nama_tipe');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = GetTipePasienPiutang($r->idtipe);
          $result[] = ($r->idpoli=='0'?text_default($r->nama_poli):$r->nama_poli);
          $result[] = GetPertemuan($r->statuspasienbaru);
          $result[] = ($r->pertemuan_id=='0'?text_default($r->kasus):$r->kasus);
          $result[] = ($r->operand_tahun=='0'?text_default('TIDAK DITENTUKAN'):$r->operand_tahun.' '.$r->umur_tahun);
          $result[] = ($r->operand_bulan=='0'?text_default('TIDAK DITENTUKAN'):$r->operand_bulan.' '.$r->umur_bulan);
          $result[] = ($r->operand_hari=='0'?text_default('TIDAK DITENTUKAN'):$r->operand_hari.' '.$r->umur_hari);
          $result[] = ($r->singkatan);
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_default('.$r->id.')" type="button" title="Hapus Default" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
    function hapus_formulir(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_cppt_tampil_formulir',$this);
	  
	  json_encode($hasil);
	  
  }
  
  
}
