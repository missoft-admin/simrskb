<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpoliklinik extends CI_Controller {

	/**
	 * Poliklinik controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpoliklinik_model');
		$this->load->helper('path');
  }

	function index()
	{
		
		$data = array(
			'idtipe' => '',
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Poliklinik';
		$data['content'] 		= 'Mpoliklinik/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Poliklinik",'#'),
									    			array("List",'mpoliklinik')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
  }

	function filter(){
		$data = array(
			'idtipe' => $this->input->post('idtipe'),
		);

		$this->session->set_userdata($data);

		$data['error'] 			= '';
		$data['title'] 			= 'Poliklinik';
		$data['content'] 		= 'Mpoliklinik/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Poliklinik",'#'),
									    			array("List",'mpoliklinik')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create()
	{
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'idtipe' 				=> '',
			'statuslock' 		=> '',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Poliklinik';
		$data['content'] 		= 'Mpoliklinik/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Poliklinik",'#'),
									    			array("Tambah",'mpoliklinik')
													);

		$data['list_dokter'] = $this->Mpoliklinik_model->getAllDokter();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id)
	{
		
		if($id != ''){
			$row = $this->Mpoliklinik_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'idtipe' 				=> $row->idtipe,
					'statuslock' 		=> $row->statuslock,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Poliklinik';
				$data['content']	 	= 'Mpoliklinik/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Poliklinik",'#'),
											    			array("Ubah",'mpoliklinik')
															);

				$data['list_dokter'] = $this->Mpoliklinik_model->getAllDokter();
				$data['list_detail'] = $this->Mpoliklinik_model->getSpecifiedDetail($id);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mpoliklinik/index','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpoliklinik/index');
		}
	}

	function delete($id)
	{
		
		$this->Mpoliklinik_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mpoliklinik/index','location');
	}

	function save()
	{
		if($this->Mpoliklinik_model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mpoliklinik/index','location');
		}
	}

	function getIndex($uri)
  {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  
			$this->select = array();
			$this->from   = 'mpoliklinik';
			$this->join 	= array();

			// FILTER
			if($uri == 'filter'){
				$this->where  = array();
				if ($this->session->userdata('idtipe') != "#") {
					$this->where = array_merge($this->where, array('idtipe' => $this->session->userdata('idtipe')));
				}
				$this->where = array_merge($this->where, array('status' => '1'));
			}else{
				$this->where  = array(
					'status' => '1'
				);
			}

			$this->order  = array(
				'id' => 'DESC'
			);
			$this->group  = array();

      $this->column_search   = array('nama');
      $this->column_order    = array('nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					if($r->statuslock == 0){
						if (UserAccesForm($user_acces_form,array('42'))){
							$aksi .= '<a href="'.site_url().'mpoliklinik/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';	
						}
						if (UserAccesForm($user_acces_form,array('43'))){
							$aksi .= '<a href="#" data-urlindex="'.site_url().'mpoliklinik" data-urlremove="'.site_url().'mpoliklinik/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
						}

					}else{
						if (UserAccesForm($user_acces_form,array('42'))){
							$aksi .= '<a href="'.site_url().'mpoliklinik/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
						}
					}
					$aksi .= '</div>';

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = GetAsalRujukan($r->idtipe);
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
	}
}
