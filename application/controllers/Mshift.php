<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mshift extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mshift_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// print_r($data);exit;
		if (UserAccesForm($user_acces_form,array('1792'))){
			
			$data['tab'] 			= $tab;
			$data['shift_id'] 			= '#';
			$data['idshift'] 			= '#';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Shift';
			$data['content'] 		= 'Mshift/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Setting Shift",'mshift')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	

  function simpan_shift(){
		$id=$this->input->post('id');
		$data=array(
			'shift_id'=>$this->input->post('shift_id'),
			'waktu_1'=>$this->input->post('waktu_1'),
			'waktu_2'=>$this->input->post('waktu_2'),
		);
		$hasil=$this->cek_duplicate($id,$this->input->post('shift_id'));
		if ($hasil==true){
			if ($id){
				$this->db->where('id',$id);
				$hasil=$this->db->update('mshift',$data);
			}else{
				$hasil=$this->db->insert('mshift',$data);
				
			}
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function cek_duplicate($id='',$shift_id){
		$q="SELECT * FROM mshift H WHERE H.`id` !='$id' AND H.shift_id='$shift_id'";
		$hasil=$this->db->query($q)->row('id');
		if ($hasil){
			return false;
		}else{
			return true;
		}
	}
	function load_shift()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*,M.ref as nama_shif FROM mshift H
						LEFT JOIN merm_referensi M ON M.nilai=H.shift_id AND M.ref_head_id='295'
						ORDER BY H.shift_id ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_shif');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
          $result[] = $no;
          $result[] = $r->nama_shif;
          $result[] = HumanTimeShort($r->waktu_1).' s/d '.HumanTimeShort($r->waktu_2);
          $aksi = '<div class="btn-group">';
		   if (UserAccesForm($user_acces_form,array('2451'))){
		  $aksi .= '<button onclick="edit_shift('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  }
		   if (UserAccesForm($user_acces_form,array('2452'))){
		  $aksi .= '<button onclick="hapus_shift('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_shift(){
	  $id=$this->input->post('id');
	 
		$this->db->where('id',$id);
		$hasil=$this->db->delete('mshift');
	  
	  json_encode($hasil);
	  
  }


  function find_shift($id){
	  $q="SELECT * FROM mshift H
			WHERE H.`id`='$id' ";
	  $opsi=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($opsi));
  }
  function load_dokter()
	{
			$id=$this->input->post('id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama,CASE WHEN H.iddokter IS NOT NULL THEN '1' ELSE '0' END as pilih 
						FROM mshiftklinik_dokter MS 
						INNER JOIN mdokter M ON MS.iddokter=M.id
						LEFT JOIN mshift_dokter H ON H.iddokter=M.id AND H.idshiftklinik=MS.idshiftklinik
						WHERE M.`status`='1' AND MS.idshiftklinik='$id' AND MS.`status`='1'
						ORDER BY M.id
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
         
          $result[] = $r->nama;
		  $aksi='';
		  if ($r->pilih){
			$aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save_dokter('.$id.','.$r->id.','.$r->pilih.')" type="checkbox" checked><span></span>
					</label>';	
		  }else{
			  $aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input onclick="check_save_dokter('.$id.','.$r->id.','.$r->pilih.')" type="checkbox"><span></span>
					</label>';	
		  }
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
    }
	function check_save_dokter(){
		$pilih=$this->input->post('pilih');
		$idshift=$this->input->post('idshift');
		$iddokter=$this->input->post('iddokter');
		if ($pilih=='1'){//Asalnya Terpilih
			$this->db->where('idshiftklinik',$idshift);
			$this->db->where('iddokter',$iddokter);
			$hasil=$this->db->delete('mshift_dokter');
		}else{
			$this->idshiftklinik=$idshift;
			$this->iddokter=$iddokter;
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
			
			  $hasil=$this->db->insert('mshift_dokter',$this);
		}
		
		  
		  json_encode($hasil);
	}
}
