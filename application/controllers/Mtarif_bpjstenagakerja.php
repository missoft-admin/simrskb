<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtarif_bpjstenagakerja extends CI_Controller {

	/**
	 * Tarif BPJS Tenagakerja controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtarif_bpjstenagakerja_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Tarif BPJS Tenagakerja';
		$data['content'] 		= 'Mtarif_bpjstenagakerja/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif BPJS Tenagakerja",'#'),
									    			array("List",'mtarif_bpjstenagakerja')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 								=> '',
			'nama' 							=> '',
			'tarifrawatjalan' 	=> '',
			'tarifrawatinap' 		=> '',
			'tariftotal' 				=> '',
			'status' 						=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Tarif BPJS Tenagakerja';
		$data['content'] 		= 'Mtarif_bpjstenagakerja/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif BPJS Tenagakerja",'#'),
									    			array("Tambah",'mtarif_bpjstenagakerja')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mtarif_bpjstenagakerja_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 								=> $row->id,
					'nama' 							=> $row->nama,
					'tarifrawatjalan' 	=> $row->tarifrawatjalan,
					'tarifrawatinap' 		=> $row->tarifrawatinap,
					'tariftotal' 				=> $row->tariftotal,
					'status' 						=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Tarif BPJS Tenagakerja';
				$data['content']	 	= 'Mtarif_bpjstenagakerja/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Tarif BPJS Tenagakerja",'#'),
											    			array("Ubah",'mtarif_bpjstenagakerja')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtarif_bpjstenagakerja','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtarif_bpjstenagakerja');
		}
	}

	function delete($id){
		$this->Mtarif_bpjstenagakerja_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mtarif_bpjstenagakerja','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('tarifrawatjalan', 'Tarif Rawat Jalan', 'trim|required');
		$this->form_validation->set_rules('tarifrawatinap', 'Tarif Rawat Inap', 'trim|required');
		$this->form_validation->set_rules('tariftotal', 'Tarif Total', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mtarif_bpjstenagakerja_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_bpjstenagakerja','location');
				}
			} else {
				if($this->Mtarif_bpjstenagakerja_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_bpjstenagakerja','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mtarif_bpjstenagakerja/manage';

		if($id==''){
			$data['title'] = 'Tambah Tarif BPJS Tenagakerja';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Tarif BPJS Tenagakerja",'#'),
															array("Tambah",'mtarif_bpjstenagakerja')
													);
		}else{
			$data['title'] = 'Ubah Tarif BPJS Tenagakerja';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Tarif BPJS Tenagakerja",'#'),
															array("Ubah",'mtarif_bpjstenagakerja')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
			$this->from   = 'mtarif_bpjstenagakerja';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'id' => 'DESC'
			);
			$this->group  = array();
			
			if (UserAccesForm($user_acces_form,array('169'))){
				$this->column_search   = array('nama');
			}else{
				$this->column_search   = array();
			}
			
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->nama;
					$row[] = number_format($r->tarifrawatjalan);
					$row[] = number_format($r->tarifrawatinap);
					$row[] = number_format($r->tariftotal);
					$aksi = '<div class="btn-group">';
		            if (UserAccesForm($user_acces_form,array('171'))){
		                $aksi .= '<a href="'.site_url().'mtarif_bpjstenagakerja/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
		            }
		            if (UserAccesForm($user_acces_form,array('172'))){
		                $aksi .= '<a href="#" data-urlindex="'.site_url().'mtarif_bpjstenagakerja" data-urlremove="'.site_url().'mtarif_bpjstenagakerja/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
		            }
		            $aksi .= '</div>';
  					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
}
