<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpemilik_saham extends CI_Controller {

	/**
	 * Pegawai controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpemilik_saham_model');
		$this->load->helper('path');
		
  }

	function index($idkategori='0',$tipe_pemilik='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('2'))){
			$data = array();
			$data['idkategori'] 			= $idkategori;
			$data['tipe_pemilik'] 			= $tipe_pemilik;
			$data['tipe_pemilik'] 			= '#';
			$data['status'] 			= '#';
			$data['error'] 			= '';
			$data['title'] 			= 'Pemilik Saham';
			$data['content'] 		= 'Mpemilik_saham/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Pemilik Saham",'#'),
												  array("List",'mpemilik_saham')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create(){
		
		$data = array(
			'id'            => '',
			'nama' 					=> '',
			'tipe_pemilik' 	=> '',
			'nama' 					=> '',
			'idpegawai' 				=> '',
			'iddokter' 				=> '',
			'npwp' 	=> '',
			'keterangan' 	    => '',
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Pemilik Saham';
		$data['content'] 		= 'Mpemilik_saham/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Pemilik Saham",'#'),
								            array("Tambah",'mpemilik_saham')
								        	);

		$data['list_pegawai'] = $this->Mpemilik_saham_model->list_pegawai();
		$data['list_dokter'] = $this->Mpemilik_saham_model->list_dokter();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$data= $this->Mpemilik_saham_model->getSpecified($id);
			
			if ($data['tipe_pemilik']=='1'){
				$data['idpegawai']=$data['idpeg_dok'];
				$data['iddokter']='#';
			}
			if ($data['tipe_pemilik']=='2'){
				$data['iddokter']=$data['idpeg_dok'];
				$data['idpegawai']='#';
			}
			if ($data['tipe_pemilik']=='3'){
				$data['iddokter']='#';
				$data['idpegawai']='#';
			}	
			// print_r($data);exit();
			$data['list_pegawai'] = $this->Mpemilik_saham_model->list_pegawai();
			$data['list_dokter'] = $this->Mpemilik_saham_model->list_dokter();
			$data['list_bank'] = $this->Mpemilik_saham_model->list_bank();
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Pemilik Saham';
			$data['content']    = 'Mpemilik_saham/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Pemilik Saham",'#'),
										array("Ubah",'mpemilik_saham')
										);

			// $data['statusAvailableApoteker'] = $this->Mpemilik_saham_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpemilik_saham');
		}
	}

	function delete($id){
		
		$result=$this->Mpemilik_saham_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('mpemilik_saham','location');
	}
	function aktifkan($id){
		
		$result=$this->Mpemilik_saham_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){
	
		if($this->input->post('id') == '' ) {
			if($this->Mpemilik_saham_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mpemilik_saham','location');
			}
		} else {
			if($this->Mpemilik_saham_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mpemilik_saham','location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mpemilik_saham/manage';

		if($id==''){
			$data['title'] = 'Tambah Pemilik Saham';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Pemilik Saham",'#'),
							               array("Tambah",'mpemilik_saham')
								           );
		}else{
			$data['title'] = 'Ubah Pemilik Saham';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Pemilik Saham",'#'),
							               array("Ubah",'mpemilik_saham')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	function filter(){
			$idkategori = $this->input->post('idkategori');
			$tipe_pemilik = $this->input->post('tipe_pemilik');
			redirect("mpemilik_saham/index/$idkategori/$tipe_pemilik",'location');
	}
	function getIndex($idkategori='0',$tipe_pemilik='0')
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$tipe_pemilik = $this->input->post('tipe_pemilik');
			$status = $this->input->post('status');
			$nama = $this->input->post('nama');
			if ($tipe_pemilik !='#'){
				$where .=" AND tipe_pemilik='$tipe_pemilik'";
			}
			if ($status !='#'){
				$where .=" AND status='$status'";
			}
			if ($nama !=''){
				$where .=" AND nama LIKE '%".$nama."%'";
			}
			$this->select = array();
			$from="
					(
						SELECT 
						M.id,M.tipe_pemilik
						,CASE WHEN M.tipe_pemilik='1' THEN MP.nama WHEN M.tipe_pemilik='2' THEN MD.nama ELSE M.nama END as nama
						,CASE WHEN M.tipe_pemilik='1' THEN 'PEGAWAI' WHEN M.tipe_pemilik='2' THEN 'DOKTER' ELSE 'NON PEGAWAI' END as tipe_nama
						,M.npwp,M.keterangan,M.`status`

						from mpemilik_saham M

						LEFT JOIN mpegawai MP ON MP.id=M.idpeg_dok AND M.tipe_pemilik='1'
						LEFT JOIN mdokter MD ON MD.id=M.idpeg_dok AND M.tipe_pemilik='2'

					) as tbl WHERE id IS NOT NULL ".$where."
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama','npwp');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $r->id;
          $result[] = $no;
          $result[] = $r->nama;
          $result[] = $r->tipe_nama;
         $result[] = StatusBarang($r->status);
          // $result[] = GetKategoriPegawai($r->idkategori);
          $aksi = '<div class="btn-group">';
			if ($r->status=='1'){
				$aksi .= '<a href="'.site_url().'mpemilik_saham/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<a href="'.site_url().'mpemilik_saham/update/'.$r->id.'" data-toggle="tooltip" title="Rekening" class="btn btn-success btn-xs"><i class="fa fa-address-card-o"></i></a>';
				$aksi .= '<button title="Hapus" class="btn btn-danger btn-xs removeData"><i class="fa fa-trash-o"></i></button>';
			}else{
				$aksi .= '<button title="Aktifkan" class="btn btn-success btn-xs aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
			}
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  public function save_rekening()
    {
		// data: {idbank: idbank,norek: norek,idpemilik:idpemilik,atas_nama:atas_nama},
        $idpemilik = $this->input->post('idpemilik');
        $idbank = $this->input->post('idbank');
        $norek = $this->input->post('norek');
        $atas_nama = $this->input->post('atas_nama');
		$data =array(
            'idpemilik'=>$idpemilik,
            'idbank'=>$idbank,
            'norek'=>$norek,
            'atas_nama'=>$atas_nama,           
			'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
			'created_date'=>date('Y-m-d H:i:s'),
			'updated_by'=>$this->session->userdata('user_id'),
			'updated_nama'=>$this->session->userdata('user_name'),
			'updated_date'=>date('Y-m-d H:i:s'),


        );
		$result=$this->db->insert('mpemilik_saham_rek',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	function LoadRekening()
    {
		$idpemilik     		= $this->input->post('idpemilik');


		$this->select = array();
		$this->join 	= array();
		$this->where  = array();


		$from="(SELECT M.*,B.bank from mpemilik_saham_rek M
				LEFT JOIN ref_bank B ON B.id=M.idbank
				WHERE M.idpemilik='$idpemilik' AND M.status='1'
				ORDER BY M.id ASC
			) as tbl  ";

		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();
		$url        = site_url('mpasien_kelompok/');
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

           $row[] = $r->id;//0
			$row[] = $r->idbank;//1
            $row[] = $r->bank;
            $row[] = $r->norek;
			$row[] = $r->atas_nama;
            $row[] =$r->updated_nama.'<br>'.HumanDateLong($r->updated_date);

			$aksi       = '<div class="btn-group">';
			$aksi .= '<button class="btn btn-xs btn-primary edit" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button class="btn btn-xs btn-danger hapus" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';

			$aksi.='</div>';
			$row[] = $aksi;
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	public function update_rekening()
    {
        $rek_id = $this->input->post('rek_id');
        $idpemilik = $this->input->post('idpemilik');
        $idbank = $this->input->post('idbank');
        $norek = $this->input->post('norek');
        $atas_nama = $this->input->post('atas_nama');
		$data =array(
            'idpemilik'=>$idpemilik,
            'idbank'=>$idbank,
            'norek'=>$norek,
            'atas_nama'=>$atas_nama,           
			'updated_by'=>$this->session->userdata('user_id'),
			'updated_nama'=>$this->session->userdata('user_name'),
			'updated_date'=>date('Y-m-d H:i:s'),


        );
		
		$this->db->where('id',$rek_id);;
		$result=$this->db->update('mpemilik_saham_rek',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function hapus_data()
    {
        $rek_id = $this->input->post('rek_id');

		$data =array(
            'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_nama'=>$this->session->userdata('user_name'),
			'deleted_date'=>date('Y-m-d H:i:s'),


        );
		$this->db->where('id',$rek_id);;
		$result=$this->db->update('mpemilik_saham_rek',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
}
