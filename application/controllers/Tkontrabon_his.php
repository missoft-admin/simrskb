<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tkontrabon_his extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tkontrabon_his_model','model');
	}

	function index() {
		$data=array(
			'iddistributor'=>'#',
			
		);
		$data['error'] 			= '';
		$data['list_distributor'] 	= $this->model->list_distributor();
		$data['title'] 			= 'History Kontrabon';
		$data['content'] 		= 'Tkontrabon_his/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("History Kontrabon",'tverifikasi_his/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function detail($iddistributor) {
		$data=$this->model->get_distributor($iddistributor);
		
		$data['error'] 			= '';
		$data['list_distributor'] 	= $this->model->list_distributor();
		$data['title'] 			= 'History Kontrabon';
		$data['content'] 		= 'Tkontrabon_his/detail';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("History Kontrabon Detail",'tverifikasi_his/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$iddistributor=$this->input->post('iddistributor');
		
		$where1='';
		$where='';
		$where2='';
		if ($iddistributor !='#'){
			$where .=" AND H.iddistributor='$iddistributor' ";
		}
		
        $from = "(
					SELECT H.iddistributor,M.nama,M.alamat,M.telepon,M.kode from tkontrabon H
					LEFT JOIN tkontrabon_info I ON I.id=H.kontrabon_info_id 
					LEFT JOIN mdistributor M ON M.id=H.iddistributor
					WHERE I.`status` > 3 ".$where."
					GROUP BY H.iddistributor 
				) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nama','alamat','kode','telepon');
        $this->column_order     = array('nama','alamat');
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tkontrabon_his/');
            $row[] = $no;
            $row[] = $r->kode;
            $row[] = $r->nama;
            $row[] = $r->alamat;
            $row[] = $r->telepon;
				$aksi .= '<a class="view btn btn-xs btn-primary" href="'.$url.'detail/'.$r->iddistributor.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-eye"></i> Detail</a>';
				$aksi .= '<a href="'.$url.'print_data/'.$r->iddistributor.'" type="button" target="_blank" title="Edit" class="btn btn-xs btn-success"><i class="fa fa-print"></i> Print</a>';				
			
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function get_index_detail(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$iddistributor=$this->input->post('iddistributor');
		$ym=$this->input->post('ym');
		
		
        $from = "(
					SELECT H.id,H.nokontrabon,H.st_cara_bayar,H.grandtotalnominal as totalnominal,I.user_finish_verifikasi as user_verifikasi,I.user_finish_validasi as user_validasi
					,GROUP_CONCAT(M.`name`) as user_konfirmasi
					,H.user_penyerahan,H.tanggal_penyerahan,H.nama_penerima
					FROM tkontrabon H
					LEFT JOIN tkontrabon_info I ON I.id=H.kontrabon_info_id
					LEFT JOIN tkontrabon_user U ON U.kontrabon_info_id=I.id
					LEFT JOIN musers M ON M.id=U.user_id
					WHERE H.iddistributor='$iddistributor' AND DATE_FORMAT(H.tanggalkontrabon,'%Y%m')='$ym'
					GROUP BY H.kontrabon_info_id
				) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

		$url        = site_url('tkontrabon_his/');
		$url2        = site_url('tkontrabon/');
        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            
            $row[] = $r->id;
            $row[] = $no;
            $row[] = $r->nokontrabon;
            $row[] = cara_bayar($r->st_cara_bayar);
            $row[] = number_format($r->totalnominal,2);
            $row[] = '<span class="label label-success">'.$r->user_verifikasi.'</span>';
            $row[] = '<span class="label label-success">'.$r->user_validasi.'</span>';
            $row[] = '<span class="label label-success">'.$r->user_konfirmasi.'</span>';
            $row[] ='<span class="label label-warning">'. HumanDateShort($r->tanggal_penyerahan).' - '.$r->nama_penerima.'</span>';
				$aksi .= '<a class="view btn btn-xs btn-success" href="'.$url2.'detail_kontrabon/'.$r->id.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<button  title="Edit" class="btn btn-xs btn-primary serahkan"><i class="fa fa-photo"></i></button>';				
				$aksi .= '<a href="#" type="button" title="Edit" class="btn btn-xs btn-primary"><i class="fa fa-print"></i></a>';				
			
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_left()
	{
		$tahun 	= $this->input->post('tahun');
		$iddistributor 	= $this->input->post('iddistributor');
		$arr = $this->model->load_left($iddistributor,$tahun);
		$this->output->set_output(json_encode($arr));
	}


}
