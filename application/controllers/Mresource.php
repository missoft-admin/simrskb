<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mresource extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mresource_model', 'model'); 
	}

	function index(){
		
		PermissionUserLoggedIn($this->session);

		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Resource';
		$data['content'] 		= 'Mresource/index';
		$data['breadcrum'] 		= array(
				array("RSKB Halmahera",'#'),
				array("Resource",'#'),
				array("List",'mresource')
			);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function list_resource() {
		$this->load->library('datatables');
		$this->datatables->from('mresource');
		return print_r($this->datatables->generate());
	}

	public function select_path() {
		$this->db->select('path as id, concat( repeat("--",tipe), resource ) as text');
		$this->db->where('tipe !=', '3');
		$this->db->order_by('path', 'asc');
		$data = $this->db->get('mresource')->result_array();
		$this->output->set_output(json_encode($data));
	}

	public function save_resource() {
		$this->model->save_resource();
	}
}
