<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrka_pengajuan_monitoring extends CI_Controller {

	/**
	 * Create RKA controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mrka_pengajuan_model');
		$this->load->model('Mrka_kegiatan_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$date1=date_create(date('Y-m-d'));
		date_add($date1,date_interval_create_from_date_string("-30 days"));
		$date1= date_format($date1,"d-m-Y");
		// print_r($date1);exit;
		$data = array(
			'tanggal_pengajuan2'=>date('d-m-Y'),
			'tanggal_pengajuan1'=>($date1),
			'tanggal_dibutuhkan1'=>'',
			'tanggal_dibutuhkan2'=>'',
			'idjenis'=>'#',
			'status'=>'#',
		);
		$data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
		$data['list_jenis'] 		= $this->Mrka_pengajuan_model->list_jenis_all();
		$data['error'] 			= '';
		$data['title'] 			= 'Monitoring Pengajuan';
		$data['content'] 		= 'Mrka_pengajuan_monitoring/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("List RKA",'#'),
									    			array("List",'mrka_pengajuan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	  		
	  $no_pengajuan=$this->input->post('no_pengajuan');
	  $nama_kegiatan=$this->input->post('nama_kegiatan');
	  $tipe_rka=$this->input->post('tipe_rka');
	  $idunit_pengaju=$this->input->post('idunit_pengaju');
	  $idjenis=$this->input->post('idjenis');
	  $tanggal_pengajuan1= ($this->input->post('tanggal_pengajuan1'));
	  $tanggal_pengajuan2= ($this->input->post('tanggal_pengajuan2'));
	  $tanggal_dibutuhkan1= ($this->input->post('tanggal_dibutuhkan1'));
	  $tanggal_dibutuhkan2= ($this->input->post('tanggal_dibutuhkan2'));
	  
	  $iduser=$this->session->userdata('user_id');
	  $where='';
	  $status= ($this->input->post('status'));
	  if ($status !='#'){
		  $where .=" AND H.status='$status'";
	  }
	  if ($no_pengajuan !=''){
		  $where .=" AND H.no_pengajuan='$no_pengajuan'";
	  }
	  if ($nama_kegiatan !=''){
		  $where .=" AND H.nama_kegiatan='$nama_kegiatan'";
	  }
	  if ($tipe_rka !='#'){
		  $where .=" AND H.tipe_rka='$tipe_rka'";
	  }
	  if ($idunit_pengaju !='#'){
		  $where .=" AND H.idunit_pengaju='$idunit_pengaju'";
	  }
	  if ($idjenis !='#'){
		  $where .=" AND H.idjenis='$idjenis'";
	  }
	  if ($tanggal_pengajuan1 !='' || $tanggal_pengajuan2 !=''){
		  $where .=" AND (H.tanggal_pengajuan >='".YMDFormat($tanggal_pengajuan1)."' AND H.tanggal_pengajuan <='".YMDFormat($tanggal_pengajuan2)."' )";
	  }
	  if ($tanggal_dibutuhkan1 !='' || $tanggal_dibutuhkan2 !=''){
		  $where .=" AND (H.tanggal_dibutuhkan >='".YMDFormat($tanggal_dibutuhkan1)."' AND H.tanggal_dibutuhkan <='".YMDFormat($tanggal_dibutuhkan2)."' )";
	  }
	  
	  // $where .="AND (H.idunit_pengaju IN (SELECT idunit from munitpelayanan_user_setting US WHERE US.userid='$iduser'))";
	  $from="(
				SELECT H.idpengajuan_asal,H.id,H.no_pengajuan,H.created_date, H.tanggal_pengajuan,H.tipe_rka,H.nama_kegiatan,H.catatan 
				,U.nama as unit_pengaju,H.idunit_pengaju,H.grand_total,H.`status`,H.idjenis
				,H.tanggal_dibutuhkan,H.idunit,cek_logic(H.id) as nama_logic
				,J.nama as jenis,GROUP_CONCAT(AP.user_nama) as nama_user,GROUP_CONCAT(AP.approve) as st_approve,MAX(AP.step) as step
				,H.st_validasi,H.nominal_bayar_verifikasi,H.nominal_pencairan,H.st_berjenjang,H.total_sisa
				,H.st_approval_lanjutan,H.status_approval_lanjutan
				from rka_pengajuan H
				LEFT JOIN rka_pengajuan_approval AP ON AP.idrka=H.id AND AP.st_aktif='1'
				LEFT JOIN munitpelayanan U ON U.id=H.idunit_pengaju
				LEFT JOIN mjenis_pengajuan_rka J ON J.id=H.idjenis
				WHERE H.status != '99' ".$where."
				GROUP BY H.id
				ORDER BY H.tanggal_dibutuhkan,H.id DESC
				) as tbl";
	// print_r($tanggal_dibutuhkan1);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama_kegiatan','no_pengajuan','catatan');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
		  $button_asal='';
		  if ($r->idpengajuan_asal){
			  $button_asal='<br><a href="'.site_url().'mrka_pengajuan/update/'.$r->idpengajuan_asal.'/disabled" target="_blank" title="Lihat Pengajau Asal"><i class="fa fa-folder-open-o"></i></a>';
		  }
		  $nama_logic=$r->nama_logic;//$r->unit_pengaju;;

					$aksi = '<div class="btn-group">';
					if ($r->status !='0'){
						
							if ($r->status <3){
								$aksi .= '<a href="'.site_url().'mrka_pengajuan/update/'.$r->id.'/disabled" data-toggle="tooltip" title="Detail" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>';
								
							}else{
								$aksi .= '<a href="'.site_url().'mrka_bendahara/proses_bendahara/'.$r->id.'/disabled" data-toggle="tooltip" title="Detail" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>';
								
							}
							
							
							$aksi .= '<button title="Upload Dokument" class="btn btn-warning btn-sm" onclick="upload_image('.$r->id.')"><i class="fa fa-file-image-o"></i></button>';
						if ($r->status=='1'){//Draft
							$aksi .= '<a href="'.site_url().'mrka_pengajuan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>';
							if ($nama_logic==''){
								$aksi .= '<button disabled title="Actived" class="btn btn-primary btn-sm persetujuan"><i class="fa fa-check"></i></button>';
								$nama_logic='<span class="label label-danger" data-toggle="tooltip" title="BELUM DISETTING">LOGIC BELUM DISETTING</span>';
							}else{
								$aksi .= '<button title="Actived" class="btn btn-primary btn-sm persetujuan"><i class="fa fa-check"></i></button>';
								$nama_logic='<span class="label label-success" data-toggle="tooltip" title="LOGI">'.$nama_logic.'</span>';
							}
							$aksi .= '<button title="Hapus" class="btn btn-danger btn-sm hapus"><i class="fa fa-trash-o"></i></button>';
						}
							
					
						
					}
					if ($r->st_berjenjang=='1' && $r->nominal_bayar_verifikasi>0){
						$aksi .= '<a href="'.site_url().'mrka_bendahara/proses_pencairan/'.$r->id.'" data-toggle="tooltip" title="Pengambilan Pencairan" class="btn btn-primary btn-sm"><i class="fa fa-handshake-o"></i></a>';
					}
					if ($r->st_berjenjang=='1' && $r->nominal_pencairan>0){
						if (($r->nominal_bayar_verifikasi - $r->total_sisa) != $r->grand_total){
							$aksi .= '<button data-toggle="tooltip" title="Realisasi Anggaran" disabled class="btn btn-danger btn-sm"><i class="fa fa-book"></i></button>';
						}else{
							$aksi .= '<a href="'.site_url().'mrka_bendahara/proses_realisasi/'.$r->id.'/disabled" data-toggle="tooltip" title="Realisasi Anggaran" class="btn btn-danger btn-sm"><i class="fa fa-book"></i></a>';
						 
						}
					 
					}
					if ($r->st_approval_lanjutan=='1'){
						$aksi .= '<button data-toggle="tooltip" title="Approval Lanjutan" onclick="lihat_approval_lanjutan('.$r->id.')" class="btn btn-danger btn-sm"><i class="si si-user-following"></i></button>';
					}
					if (UserAccesForm($user_acces_form,array('1430'))){
						$aksi .= '<a href="#" data-toggle="tooltip" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
					}
					
			        $aksi .= '</div>';
		  $btn_bendahara='';
		  $btn_proses='';
		  if ($r->status!='1'){
				$nama_logic='<br><br><button title="Lihat User" class="btn btn-danger btn-xs user"><i class="si si-user"></i></button>';
		  }
		  if ($r->status>'2'){
				$btn_bendahara='&nbsp;<button onclick="lihat_user_bendahara('.$r->id.')" title="Lihat Bendahara" class="btn btn-primary btn-xs"><i class="si si-user"></i></button>';
				$btn_proses='&nbsp;<button onclick="lihat_user_belanja('.$r->id.')" title="User Mengerjakan" class="btn btn-success btn-xs"><i class="si si-user"></i></button>';
		  }
		  $catatan='';
		  if ($r->catatan){
			  $catatan='<br>'.'<p class="text-primary"><i class="fa fa-sticky-note-o"></i> '.$r->catatan.'</p>';
		  }
          $row[] = $r->id;
          $row[] = $r->id;
          $row[] = $r->id;
          $row[] = $no;//3
          $row[] = $r->no_pengajuan.'<br>'.HumanDateLong($r->created_date);
          $row[] = HumanDateShort($r->tanggal_pengajuan).$button_asal;
          $row[] = tipe_rka($r->tipe_rka);
          $row[] = $r->nama_kegiatan.$catatan;
          $row[] = $r->unit_pengaju;
          $row[] = HumanDateShort($r->tanggal_dibutuhkan);
          $row[] = number_format($r->grand_total,0);
          $row[] = $r->jenis;
          $row[] = status_pengajuan($r->status,$r->step).' '.$nama_logic.$btn_bendahara.$btn_proses;//.($status=='1'?$nama_logic:'');
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function lihat_user_bendahara($id){
		$q="SELECT * FROM rka_pengajuan_bendahara H WHERE H.idrka='$id' ORDER BY H.id ASC";
		$row=$this->db->query($q)->result();
		$content='';
		$no=1;
		$no_step="1";
		foreach($row as $r){
			$content .='<tr>';
			$content .='<td>'.$no.'</td>';
			$content .='<td><i class="si si-users fa-2x text-black-op"></i> &nbsp;&nbsp;&nbsp; '.$r->nama_user.'</td>';
			$content .='</tr>';
			$no=$no+1;
		}
		$arr['detail']=$content;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($content));
	}
	function lihat_user_belanja($id){
		$q="SELECT * FROM rka_pengajuan_user_proses H WHERE H.idrka='$id' 
		GROUP BY H.iduser
		ORDER BY H.id ASC";
		$row=$this->db->query($q)->result();
		$content='';
		$no=1;
		$no_step="1";
		foreach($row as $r){
			$content .='<tr>';
			$content .='<td>'.$no.'</td>';
			$content .='<td><i class="si si-users fa-2x text-black-op"></i> &nbsp;&nbsp;&nbsp; '.$r->nama_user.'</td>';
			$content .='</tr>';
			$no=$no+1;
		}
		$arr['detail']=$content;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($content));
	}
  

}
