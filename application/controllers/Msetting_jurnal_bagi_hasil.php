<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_bagi_hasil extends CI_Controller {

	/**
	 * Setting Jurnal Bagi Hasil controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_bagi_hasil_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_bagi_hasil_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_bagi_hasil_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Bagi Hasil';
		$data['content'] 		= 'Msetting_jurnal_bagi_hasil/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Bagi Hasil",'#'),
									    			array("List",'mlogic_bagi_hasil')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function list_pegawai(){
		$idtipe=$this->input->post('idtipe');
		$idkategori=$this->input->post('idkategori');
		$where='';
		if ($idkategori !='#'){
			$where .=" AND M.idkategori='$idkategori'";
		}
		if ($idtipe=='1'){
			$q="SELECT * FROM mdokter M WHERE M.status='1' ".$where." ORDER BY M.nama ASC";
		}else{
			$q="SELECT * FROM mpegawai M  M WHERE M.status='1' ".$where." ORDER BY M.nama ASC";
		}
		
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function list_kategori($idtipe){
		if ($idtipe=='1'){
			$q="SELECT * FROM mdokter_kategori M ORDER BY M.nama ASC";
		}else{
			$q="SELECT * FROM mpegawai_kategori M ORDER BY M.nama ASC";
			
		}
		$opsi='';
		if ($idtipe !='#'){
			
			$row=$this->db->query($q)->result();
			foreach($row as $r){
				$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
			}
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	
	function update(){
		$id_edit=1;
		$data=array(
			'st_auto_posting'=>$this->input->post('st_auto_posting'),
			// 'idakun_kredit'=>$this->input->post('idakun_kredit'),
			'batas_batal'=>$this->input->post('batas_batal'),
		);
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_bagi_hasil',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	//Bagi Hasil
	function load_bagi_hasil()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT
				H.id,H.mbagi_hasil_id,H.idakun,M.nama as bagi_hasil,CONCAT(A.noakun,' - ',A.namaakun) as akun
				FROM msetting_jurnal_bagi_hasil_detail H
				LEFT JOIN mbagi_hasil M ON M.id=H.mbagi_hasil_id
				LEFT JOIN makun_nomor A ON A.id=H.idakun
				ORDER BY H.mbagi_hasil_id,H.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','pegawai');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = ($r->bagi_hasil);
            $row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_bagi_hasil('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_bagi_hasil(){
		$id_edit=$this->input->post('id_edit');
		$mbagi_hasil_id=($this->input->post('mbagi_hasil_id')=='#'?0:$this->input->post('mbagi_hasil_id'));
		$data=array(
			'setting_id'=>1,
			'mbagi_hasil_id'=>$mbagi_hasil_id,
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_bagi_hasil($mbagi_hasil_id)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_bagi_hasil_detail',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_bagi_hasil($mbagi_hasil_id){
		
		$q="SELECT *FROM msetting_jurnal_bagi_hasil_detail S
			WHERE S.mbagi_hasil_id='$mbagi_hasil_id'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_bagi_hasil($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_bagi_hasil_detail');
		echo json_encode($result);
	}
	
}
