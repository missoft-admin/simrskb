<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_pembelian extends CI_Controller {

	/**
	 * Setting Jurnal Pembelian controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_pembelian_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_pembelian_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_pembelian_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Pembelian';
		$data['content'] 		= 'Msetting_jurnal_pembelian/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Pembelian",'#'),
									    			array("List",'mlogic_kasbon')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function list_kategori($idtipe){
		// $idtipe=$this->input->post('idtipe');
		$q="SELECT * FROM mdata_kategori M
			WHERE M.`status`='1' AND M.idtipe='$idtipe'
			ORDER BY M.nama ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	public function list_barang()
    {
        $cari 	= $this->input->post('search');
        $idtipe 	= $this->input->post('idtipe');
        $idkategori 	= $this->input->post('idkategori');
		$where='';
		if ($idkategori!='#' && $idkategori!='0'){
			$where .=" AND M.idkategori='$idkategori' ";
		}
		if ($cari){
			$where .=" AND (M.nama LIKE '%".$cari."%' OR M.kode LIKE '%".$cari."%')";
		}
		$q="SELECT *FROM view_barang M WHERE M.idtipe='$idtipe' ".$where." LIMIT 100";
		// print_r($q);exit();
        $data_obat = $this->db->query($q)->result_array();
        $this->output->set_output(json_encode($data_obat));
    }
	function update(){
		$id_edit=1;
		$data=array(
			'st_auto_posting'=>$this->input->post('st_auto_posting'),
			'idakun_kredit'=>$this->input->post('idakun_kredit'),
			'batas_batal'=>$this->input->post('batas_batal'),
		);
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_pembelian',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	//Pembelian
	function load_beli()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT S.id,S.idtipe,MT.nama_tipe,S.idkategori
				,CASE WHEN S.idkategori='0' THEN 'Semua Kategori' ELSE MK.nama END as kategori 
				,CASE WHEN S.idbarang=0 THEN 'Semua Barang' ELSE CONCAT(B.kode,' - ',B.nama) END as barang,S.idbarang,S.idakun,CONCAT(A.noakun,' - ',A.namaakun) as akun 
				FROM `msetting_jurnal_pembelian_beli` S
				LEFT JOIN mdata_tipebarang MT ON MT.id=S.idtipe
				LEFT JOIN mdata_kategori MK ON MK.id=S.idkategori AND S.idkategori !=0
				LEFT JOIN view_barang B ON S.idbarang!=0 AND B.idtipe=S.idtipe AND S.idbarang=B.id
				LEFT JOIN makun_nomor A ON A.id=S.idakun 

				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('barang','kategori','nama_tipe');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = $r->nama_tipe;
            $row[] = ($r->idkategori=='0')?text_default($r->kategori):$r->kategori;
            $row[] = ($r->idbarang=='0')?text_default($r->barang):$r->barang;
            $row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_beli('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_beli(){
		$id_edit=$this->input->post('id_edit');
		$data=array(
			'setting_id'=>1,
			'idtipe'=>$this->input->post('idtipe'),
			'idkategori'=>$this->input->post('idkategori'),
			'idbarang'=>$this->input->post('idbarang'),
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_beli($data['idtipe'],$data['idkategori'],$data['idbarang'])){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pembelian_beli',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_beli($idtipe,$idkategori='0',$idbarang='0'){
		
		$gabung=$idtipe.'-'.$idkategori.'-'.$idbarang;
		
		$q="SELECT *FROM msetting_jurnal_pembelian_beli S
			WHERE CONCAT(S.idtipe,'-',S.idkategori,'-',S.idbarang)='$gabung'";
		
		// print_r($q);
		return $this->db->query($q)->row('id');
	}	
	function hapus_beli($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_pembelian_beli');
		echo json_encode($result);
	}
	
	//PPN
	function load_ppn()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT S.id,S.idtipe,MT.nama_tipe,S.idkategori
				,CASE WHEN S.idkategori='0' THEN 'Semua Kategori' ELSE MK.nama END as kategori 
				,CASE WHEN S.idbarang=0 THEN 'Semua Barang' ELSE CONCAT(B.kode,' - ',B.nama) END as barang,S.idbarang,S.idakun,CONCAT(A.noakun,' - ',A.namaakun) as akun 
				FROM `msetting_jurnal_pembelian_ppn` S
				LEFT JOIN mdata_tipebarang MT ON MT.id=S.idtipe
				LEFT JOIN mdata_kategori MK ON MK.id=S.idkategori AND S.idkategori !=0
				LEFT JOIN view_barang B ON S.idbarang!=0 AND B.idtipe=S.idtipe AND S.idbarang=B.id
				LEFT JOIN makun_nomor A ON A.id=S.idakun 

				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('barang','kategori','nama_tipe');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = $r->nama_tipe;
            $row[] = ($r->idkategori=='0')?text_default($r->kategori):$r->kategori;
            $row[] = ($r->idbarang=='0')?text_default($r->barang):$r->barang;
            // $row[] = $r->akun;
			$row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_ppn('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_ppn(){
		$id_edit=$this->input->post('id_edit');
		$data=array(
			'setting_id'=>1,
			'idtipe'=>$this->input->post('idtipe'),
			'idkategori'=>$this->input->post('idkategori'),
			'idbarang'=>$this->input->post('idbarang'),
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_ppn($data['idtipe'],$data['idkategori'],$data['idbarang'])){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pembelian_ppn',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	function cek_duplicate_ppn($idtipe,$idkategori='0',$idbarang='0'){
		$gabung=$idtipe.'-'.$idkategori.'-'.$idbarang;
		
		$q="SELECT *FROM msetting_jurnal_pembelian_ppn S
			WHERE CONCAT(S.idtipe,'-',S.idkategori,'-',S.idbarang)='$gabung'";
		// $q="SELECT *FROM msetting_jurnal_pembelian_ppn S
			// WHERE S.idtipe='$idtipe' ".$where;
		// print_r($q);
		return $this->db->query($q)->row('id');
	}	
	function hapus_ppn($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_pembelian_ppn');
		echo json_encode($result);
	}
	
	//DISKON
	
	function load_diskon()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT S.id,S.idtipe,MT.nama_tipe,S.idkategori
				,CASE WHEN S.idkategori='0' THEN 'Semua Kategori' ELSE MK.nama END as kategori 
				,CASE WHEN S.idbarang=0 THEN 'Semua Barang' ELSE CONCAT(B.kode,' - ',B.nama) END as barang,S.idbarang,S.idakun,CONCAT(A.noakun,' - ',A.namaakun) as akun 
				FROM `msetting_jurnal_pembelian_diskon` S
				LEFT JOIN mdata_tipebarang MT ON MT.id=S.idtipe
				LEFT JOIN mdata_kategori MK ON MK.id=S.idkategori AND S.idkategori !=0
				LEFT JOIN view_barang B ON S.idbarang!=0 AND B.idtipe=S.idtipe AND S.idbarang=B.id
				LEFT JOIN makun_nomor A ON A.id=S.idakun 

				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('barang','kategori','nama_tipe');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = $r->nama_tipe;
            $row[] = ($r->idkategori=='0')?text_default($r->kategori):$r->kategori;
            $row[] = ($r->idbarang=='0')?text_default($r->barang):$r->barang;
            // $row[] = $r->akun;
			$row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_diskon('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_diskon(){
		$id_edit=$this->input->post('id_edit');
		$data=array(
			'setting_id'=>1,
			'idtipe'=>$this->input->post('idtipe'),
			'idkategori'=>$this->input->post('idkategori'),
			'idbarang'=>$this->input->post('idbarang'),
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_diskon($data['idtipe'],$data['idkategori'],$data['idbarang'])){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pembelian_diskon',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	function cek_duplicate_diskon($idtipe,$idkategori='0',$idbarang='0'){
		$gabung=$idtipe.'-'.$idkategori.'-'.$idbarang;
		
		$q="SELECT *FROM msetting_jurnal_pembelian_diskon S
			WHERE CONCAT(S.idtipe,'-',S.idkategori,'-',S.idbarang)='$gabung'";
		// print_r($q);
		return $this->db->query($q)->row('id');
	}	
	function hapus_diskon($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_pembelian_diskon');
		echo json_encode($result);
	}
}
