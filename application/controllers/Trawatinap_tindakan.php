<?php
ini_set('max_execution_time', 1000);
ini_set('memory_limit', '512M');

defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Trawatinap_tindakan extends CI_Controller
{
	/**
   * Rawat Inap Controller.
   * Developer @GunaliRezqiMauludi
   */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Trawatinap_tindakan_model');
		$this->load->model('Trawatinap_verifikasi_model');
	}

	public function index()
	{
		$data = [
			'tabActive' => 1,
			'nomedrec' => '',
			'namapasien' => '',
			'tanggalkeluar' => '',
			'iddokterperujuk' => '',
			'iddokterpenanggungjawab' => '',
			'idbed' => '',
			'tanggaldari_transaksi' => date('d/m/Y'),
			'tanggalsampai_transaksi' => date('d/m/Y'),
			'tanggaldari_checkout' => date('d/m/Y'),
			'tanggalsampai_checkout' => date('d/m/Y'),
			'status' => '',
		];

		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form, ['1230'])) {
			$data['idruangan'] = '4';
		}
		if (UserAccesForm($user_acces_form, ['1229'])) {
			$data['idruangan'] = '3';
		}
		if (UserAccesForm($user_acces_form, ['1228'])) {
			$data['idruangan'] = '2';
		}
		if (UserAccesForm($user_acces_form, ['1227'])) {
			$data['idruangan'] = '1';
		}
		if (UserAccesForm($user_acces_form, ['1336'])) {
			$data['idruangan'] = '#';
		}

		if (UserAccesForm($user_acces_form, ['1234'])) {
			$data['idkelas'] = '4';
		}
		if (UserAccesForm($user_acces_form, ['1237'])) {
			$data['idkelas'] = '3';
		}
		if (UserAccesForm($user_acces_form, ['1236'])) {
			$data['idkelas'] = '2';
		}
		if (UserAccesForm($user_acces_form, ['1235'])) {
			$data['idkelas'] = '1';
		}
		if (UserAccesForm($user_acces_form, ['1233'])) {
			$data['idkelas'] = '#';
		}
		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Tindakan Rawat Inap';
		$data['content'] = 'Trawatinap_tindakan/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Tindakan Rawat Inap', '#'],
			['List', 'Trawatinap_tindakan']
		];

		$data['list_sebab_luar'] = $this->Trawatinap_tindakan_model->getSebabLuar();
		$data['list_kelompok_diagnosa'] = $this->Trawatinap_tindakan_model->getKelompokDiagnosa();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function verifikasi($idpendaftaran, $page = 'rawatinap')
	{
		$totaldeposit = $this->Trawatinap_tindakan_model->getTotalDeposit($idpendaftaran)->totaldeposit;
		$row = $this->Trawatinap_tindakan_model->getDataRawatInap($idpendaftaran);
		$pembayaran = $this->Trawatinap_tindakan_model->pembayaran($idpendaftaran);
		$kwitansi = $this->Trawatinap_tindakan_model->getLastKwitansi($idpendaftaran);
		$kwitansiImplant = $this->Trawatinap_tindakan_model->getLastKwitansiImplant($idpendaftaran);

		if ($pembayaran) {
			$idPembayaran = $pembayaran->id;
		} else {
			$idPembayaran = 0;
		}

		if ($kwitansi) {
			$kwitansi_terima_dari = $kwitansi->sudahterimadari;
		} else {
			$kwitansi_terima_dari = '';
		}
		$kwitansi_deskripsi = 'Tindakan dan Rawat Inap Pasien a/n ' . $row->namapasien;

		if ($kwitansiImplant) {
			$kwitansi_implant_terima_dari = $kwitansiImplant->sudahterimadari;
		} else {
			$kwitansi_implant_terima_dari = '';
		}
		$kwitansi_implant_deskripsi = 'Implant Pasien a/n ' . $row->namapasien;

		$rowOperasi = $this->Trawatinap_tindakan_model->getDataOperasi($idpendaftaran);

		$data = [
			'idpendaftaran' => $row->id,
			'nomedrec' => $row->nomedrec,
			'idpasien' => $row->idpasien,
			'idtipepasien' => $row->idtipepasien,
			'namapasien' => $row->namapasien,
			'alamatpasien' => $row->alamatpasien,
			'nohp' => ($row->hp ? $row->hp : $row->telepon),
			'namapenanggungjawab' => $row->nama_keluarga,
			'telppenanggungjawab' => $row->telepon_keluarga,
			'idkelompokpasien' => $row->idkelompokpasien,
			'namakelompok' => $row->namakelompok,
			'namadokterpenanggungjawab' => $row->namadokterpenanggungjawab,
			'idruangan' => $row->idruangan,
			'idkelas' => $row->idkelas,
			'namaruangan' => $row->namaruangan,
			'namakelas' => $row->namakelas,
			'namaperusahaan' => ($row->idkelompokpasien == 1 && $row->namarekanan ? $row->namarekanan : '-'),
			'namabed' => $row->namabed,
			'statuscheckout' => $row->statuscheckout,
			'statuskasir' => $row->statuskasir,
			'statusvalidasi' => $row->statusvalidasi,
			'statustransaksi' => $row->statustransaksi,
			'statuspembayaran' => $row->statuspembayaran,
			'catatan' => $row->catatan,
			'totaldeposit' => $totaldeposit,
			'last_sudah_terima_dari' => $kwitansi_terima_dari,
			'last_deskripsi' => $kwitansi_deskripsi,
			'implant_last_sudah_terima_dari' => $kwitansi_implant_terima_dari,
			'implant_last_deskripsi' => $kwitansi_implant_deskripsi,
			'page' => $page,
			'diagnosa' => ($rowOperasi ? $rowOperasi->diagnosa : ''),
			'operasi' => ($rowOperasi ? $rowOperasi->operasi : ''),
			'tanggaloperasi' => ($rowOperasi ? $rowOperasi->tanggaloperasi : ''),

			'pembayaran_tunai' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(1, $idPembayaran),
			'pembayaran_debit' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(2, $idPembayaran),
			'pembayaran_kredit' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(3, $idPembayaran),
			'pembayaran_transfer' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(4, $idPembayaran),
			'pembayaran_karyawan_pegawai' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(5, $idPembayaran),
			'pembayaran_karyawan_dokter' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(6, $idPembayaran),
			'pembayaran_tidak_tertagihkan' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(7, $idPembayaran),
			'pembayaran_kontraktor' => $this->Trawatinap_tindakan_model->count_metode_pembayaran(8, $idPembayaran),
			'pembayaran_bpjstk' => $this->Trawatinap_tindakan_model->count_metode_pembayaran_bpjstk($idPembayaran),

			'deposit_tunai' => $this->Trawatinap_tindakan_model->count_metode_pembayaran_deposit(1, $idpendaftaran),
			'deposit_debit' => $this->Trawatinap_tindakan_model->count_metode_pembayaran_deposit(2, $idpendaftaran),
			'deposit_kredit' => $this->Trawatinap_tindakan_model->count_metode_pembayaran_deposit(3, $idpendaftaran),
			'deposit_transfer' => $this->Trawatinap_tindakan_model->count_metode_pembayaran_deposit(4, $idpendaftaran),
			'deposit_all' => $this->Trawatinap_tindakan_model->count_metode_pembayaran_deposit_all($idpendaftaran),

			'list_kontraktor' => $this->Trawatinap_tindakan_model->list_kontraktor($idpendaftaran),
		];

		$data['pembayaran_non_kontraktor'] = $this->Trawatinap_tindakan_model->count_metode_pembayaran_non_kontraktor($idPembayaran);
		$data['pembayaran_excess'] = ($data['pembayaran_kontraktor'] && ($data['pembayaran_non_kontraktor'] || $data['deposit_all']) ? 1 : 0);

		$data['error'] = '';
		$data['title'] = 'Verifikasi Tindakan Rawat Inap';
		$data['content'] = 'Trawatinap_tindakan/verifikasi';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Tindakan Rawat Inap', '#'],
			['Verifikasi', 'Trawatinap_tindakan']
		];

		$data['list_bank'] = $this->Trawatinap_tindakan_model->getBank();

		if ($pembayaran) {
			$data['status_verifikasi'] = DMYFormat($pembayaran->status_verifikasi);
			$data['tanggal_pembayaran'] = DMYFormat($pembayaran->tanggal);
			$data['waktu_pembayaran'] = HISTimeFormat($pembayaran->tanggal);
			$data['total_pembayaran'] = $this->Trawatinap_tindakan_model->sum_pembayaran($pembayaran->idtindakan);
			$data['detail_pembayaran'] = $this->Trawatinap_tindakan_model->detail_pembayaran($pembayaran->id);
			$data['diskon_rp'] = $pembayaran->diskonrp;
		} else {
			$data['status_verifikasi'] = 0;
			$data['tanggal_pembayaran'] = date('Y-m-d');
			$data['waktu_pembayaran'] = date('H:i:s');
			$data['total_pembayaran'] = 0;
			$data['detail_pembayaran'] = [];
			$data['diskon_rp'] = 0;
		}

		$data['bataltransaksi'] = $this->Trawatinap_tindakan_model->getCountBatalTransaksi($idpendaftaran);
		$data['historyBatal'] = $this->Trawatinap_tindakan_model->getHistoryBatal($idpendaftaran);
		$data['historyProsesKwitansi'] = $this->Trawatinap_tindakan_model->getHistoryProsesKwitansi($idpendaftaran);
		$data['historyProsesKwitansiImplant'] = $this->Trawatinap_tindakan_model->getHistoryProsesKwitansiImplant($idpendaftaran);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function print_kwitansi_deposit($iddeposit)
	{
		// instantiate and use the dompdf class
		$dompdf = new Dompdf();

		$row = $this->Trawatinap_tindakan_model->getDeposit($iddeposit);

		if ($row->idmetodepembayaran == 1) {
			$keterangan = 'Deposit - ' . $row->metodepembayaran . ' - Kelas ' . $row->namakelas . ' ( Kwitansi Sementara )';
		} else {
			$keterangan = 'Deposit - ' . $row->metodepembayaran . ' ( ' . $row->namabank . ' )' . ' - Kelas ' . $row->namakelas . ' ( Kwitansi Sementara )';
		}

		if ($row->terimadari != '') {
			$keterangan = $keterangan . ' atas nama ' . $row->namapasien . ' (' . $row->nomedrec . ')';
		}

		$terimadari = ($row->terimadari != '' ? $row->terimadari : $row->namapasien . ' (' . $row->nomedrec . ')');

		$data = [
			'id' => $row->id,
			'tanggal' => $row->tanggal,
			'nodeposit' => $row->nodeposit,
			'namakelas' => $row->namakelas,
			'idmetodepembayaran' => $row->idmetodepembayaran,
			'metodepembayaran' => $row->metodepembayaran,
			'namabank' => $row->namabank,
			'nominal' => $row->nominal,
			'terimadari' => $terimadari,
			'keterangan' => $keterangan,
			'namapetugas' => $row->namapetugas
		];

		$html = $this->load->view('Trawatinap_tindakan/kwitansi_deposit', $data, true);

		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();

		// // Output the generated PDF to Browser
		$dompdf->stream('Kwitansi Deposit.pdf', ['Attachment' => 0]);
	}

	public function print_rincian_deposit($idrawatinap)
	{
		// instantiate and use the dompdf class
		$dompdf = new Dompdf();

		$row = $this->Trawatinap_tindakan_model->getDataRawatInap($idrawatinap);

		$data = [
			'nomedrec' => $row->nomedrec,
			'namapasien' => $row->namapasien,
		];

		$data['rincian'] = $this->Trawatinap_tindakan_model->getHistoryDeposit($idrawatinap);

		$html = $this->load->view('Trawatinap_tindakan/print/rincian_deposit', $data, true);

		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();

		// Output the generated PDF to Browser
		$dompdf->stream('Rincian Deposit.pdf', ['Attachment' => 0]);
	}

	public function print_rincian_biaya($idrawatinap, $status, $tipe = '')
	{
		$this->output->set_content_type('application/pdf');
		ini_set('memory_limit', '2048M');

		// instantiate and use the dompdf class
		$dompdf = new Dompdf();

		$row = $this->Trawatinap_tindakan_model->getDataRawatInap($idrawatinap);
		$rowCheckout = $this->Trawatinap_tindakan_model->getDataCheckout($idrawatinap);
		$rowOperasi = $this->Trawatinap_tindakan_model->getDataOperasi($idrawatinap);
		if ($row->idtipe == 1) {
			if ($rowCheckout) {
				$tanggalkeluar = $rowCheckout->tanggalkeluar;
			} else {
				$tanggalkeluar = date('d-m-Y');
			}
		} else {
			$tanggalkeluar = $row->tanggaldaftar;
		}

		$totaldeposit = $this->Trawatinap_tindakan_model->getTotalDeposit($idrawatinap)->totaldeposit;

		$data = [
			'idpendaftaran' => $row->id,
			'tanggaldaftar' => $row->tanggaldaftar,
			'tanggalkeluar' => $tanggalkeluar,
			'nopendaftaran' => $row->nopendaftaran,
			'idtipe' => $row->idtipe,
			'nomedrec' => $row->nomedrec,
			'namapasien' => $row->namapasien,
			'alamatpasien' => $row->alamatpasien,
			'namakelas' => $row->namakelas,
			'namabed' => $row->namabed,
			'catatan' => $row->catatan,
			'totaldeposit' => $totaldeposit,
			'diagnosa' => ($rowOperasi ? $rowOperasi->diagnosa : ''),
			'operasi' => ($rowOperasi ? $rowOperasi->operasi : ''),
			'pelayanan' => ($row->idtipe == 1 ? 'Rawat Inap' : 'One Day Surgery (ODS)'),
			'kelas' => $row->namakelas,
			'bed' => $row->namabed,
			'ruangan' => $row->namaruangan,
			'kelompokpasien' => $row->namakelompok,
			'rekanan' => ($row->idkelompokpasien == 1 ? $row->namarekanan : '-'),
			'statusvalidasi' => $row->statusvalidasi,
			'status' => $status,
			'tipe' => $tipe,
		];

		$data['tanggal_pembayaran'] = $this->Trawatinap_tindakan_model->getTanggalPembayaran($idrawatinap);

		$data['history_bed'] = $this->Trawatinap_tindakan_model->getHistoryPindahBed($idrawatinap);

		$pembayaran = $this->Trawatinap_tindakan_model->getInfoPembayaran($idrawatinap);
		if ($pembayaran) {
			$data['diskon_pembayaran'] = $pembayaran->diskonrp;
			$data['user_input_pembayaran'] = $pembayaran->user_input;
			$data['detail_pembayaran'] = $this->Trawatinap_tindakan_model->detail_pembayaran($pembayaran->id);
		} else {
			$data['diskon_pembayaran'] = 0;
			$data['detail_pembayaran'] = [];
			$data['user_input_pembayaran'] = '';
		}

		$data['detail_deposit'] = $this->Trawatinap_tindakan_model->getHistoryDeposit($idrawatinap);

		// $this->load->view('Trawatinap_tindakan/print/rincian_biaya', $data);
		$html = $this->load->view('Trawatinap_tindakan/print/rincian_biaya', $data, true);

		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();

		// Output the generated PDF to Browser
		$dompdf->stream('Rincian Biaya.pdf', ['Attachment' => 0]);
	}

	public function print_rincian_global($idrawatinap, $status, $tipe = '')
	{
		// instantiate and use the dompdf class
		$dompdf = new Dompdf();

		$row = $this->Trawatinap_tindakan_model->getDataRawatInap($idrawatinap);
		$rowCheckout = $this->Trawatinap_tindakan_model->getDataCheckout($idrawatinap);
		$rowOperasi = $this->Trawatinap_tindakan_model->getDataOperasi($idrawatinap);
		if ($row->idtipe == 1) {
			if ($rowCheckout) {
				$tanggalkeluar = $rowCheckout->tanggalkeluar;
			} else {
				$tanggalkeluar = date('d-m-Y');
			}
		} else {
			$tanggalkeluar = $row->tanggaldaftar;
		}

		$totaldeposit = $this->Trawatinap_tindakan_model->getTotalDeposit($idrawatinap)->totaldeposit;

		$data = [
			'idpendaftaran' => $row->id,
			'tanggaldaftar' => $row->tanggaldaftar,
			'tanggalkeluar' => $tanggalkeluar,
			'nopendaftaran' => $row->nopendaftaran,
			'nomedrec' => $row->nomedrec,
			'namapasien' => $row->namapasien,
			'alamatpasien' => $row->alamatpasien,
			'namakelas' => $row->namakelas,
			'namabed' => $row->namabed,
			'catatan' => $row->catatan,
			'totaldeposit' => $totaldeposit,
			'diagnosa' => ($rowOperasi ? $rowOperasi->diagnosa : ''),
			'operasi' => ($rowOperasi ? $rowOperasi->operasi : ''),
			'statusvalidasi' => $row->statusvalidasi,
			'status' => $status,
			'tipe' => $tipe,
		];

		$data['history_bed'] = $this->Trawatinap_tindakan_model->getHistoryPindahBed($idrawatinap);

		$data['tanggal_pembayaran'] = $this->Trawatinap_tindakan_model->getTanggalPembayaran($idrawatinap);

		$pembayaran = $this->Trawatinap_tindakan_model->getInfoPembayaran($idrawatinap);
		if ($pembayaran) {
			$data['diskon_pembayaran'] = $pembayaran->diskonrp;
			$data['user_input_pembayaran'] = $pembayaran->user_input;
		} else {
			$data['diskon_pembayaran'] = 0;
			$data['user_input_pembayaran'] = '';
		}

		$html = $this->load->view('Trawatinap_tindakan/print/rincian_global', $data, true);

		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();

		// // Output the generated PDF to Browser
		$dompdf->stream('Rincian Global.pdf', ['Attachment' => 0]);
	}

	public function print_rincian_tagihan($idrawatinap, $tipe, $idrekanan)
	{
		// instantiate and use the dompdf class
		$dompdf = new Dompdf();

		$row = $this->Trawatinap_tindakan_model->getDataRawatInap($idrawatinap);
		$kontraktor = $this->Trawatinap_tindakan_model->getDataKontraktor($tipe, $idrekanan);
		$pembayaran = $this->Trawatinap_tindakan_model->getPembayaranKontraktor($idrawatinap, $tipe, $idrekanan);
		$nosurat = $this->Trawatinap_tindakan_model->getNoSuratTagihan($idrawatinap, $tipe, $idrekanan);

		$data = [
			'nosurat' => $nosurat,
			'nokwitansi' => "KWR-$row->nopendaftaran",
			'namapasien' => $row->namapasien,
			'titlepasien' => $row->titlepasien,
			'namakontraktor' => $kontraktor->nama,
			'tagihanrekanan' => $pembayaran->tagihanrekanan
		];

		$html = $this->load->view('Trawatinap_tindakan/print/rincian_tagihan', $data, true);

		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();

		// // Output the generated PDF to Browser
		$dompdf->stream('Rincian Tagihan.pdf', ['Attachment' => 0]);
	}

	public function export_rincian_biaya($idrawatinap, $status)
	{
		// Excel Generate

		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Rincian Biaya');

		$row = $this->Trawatinap_tindakan_model->getDataRawatInap($idrawatinap);
		$rowCheckout = $this->Trawatinap_tindakan_model->getDataCheckout($idrawatinap);
		$rowOperasi = $this->Trawatinap_tindakan_model->getDataOperasi($idrawatinap);
		if ($row->idtipe == 1) {
			if ($rowCheckout) {
				$tanggalkeluar = $rowCheckout->tanggalkeluar;
			} else {
				$tanggalkeluar = date('d-m-Y');
			}
		} else {
			$tanggalkeluar = $row->tanggaldaftar;
		}

		$totaldeposit = $this->Trawatinap_tindakan_model->getTotalDeposit($idrawatinap)->totaldeposit;

		$idpendaftaran = $row->id;
		$tanggaldaftar = $row->tanggaldaftar;
		$tanggalkeluar = $tanggalkeluar;
		$nopendaftaran = $row->nopendaftaran;
		$idtipe = $row->idtipe;
		$nomedrec = $row->nomedrec;
		$namapasien = $row->namapasien;
		$alamatpasien = $row->alamatpasien;
		$namakelas = $row->namakelas;
		$namabed = $row->namabed;
		$catatan = $row->catatan;
		$totaldeposit = $totaldeposit;
		$diagnosa = ($rowOperasi ? $rowOperasi->diagnosa : '');
		$operasi = ($rowOperasi ? $rowOperasi->operasi : '');
		$statusvalidasi = $row->statusvalidasi;
		$pelayanan = ($row->idtipe == 1 ? 'Rawat Inap' : 'One Day Surgery (ODS)');
		$kelas = $row->namakelas;
		$bed = $row->namabed;
		$ruangan = $row->namaruangan;
		$kelompokpasien = $row->namakelompok;
		$rekanan = ($row->idkelompokpasien == 1 ? $row->namarekanan : '-');
		$jumlahhari = 1;
		$tarifbed = 450000;
		$status = $status;

		$tanggal_pembayaran = $this->Trawatinap_tindakan_model->getTanggalPembayaran($idrawatinap);

		// Set Logo
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$logo = FCPATH . '/assets/upload/logo/logoreport.jpg';
		$objDrawing->setPath($logo);
		$objDrawing->setCoordinates('B3');
		$objDrawing->setResizeProportional(true);
		$objDrawing->setHeight(60);
		$objDrawing->setWorksheet($activeSheet);
		$objDrawing->setOffsetX(20);

		$activeSheet->setCellValue('B2', 'NOTA RINCIAN PEMBAYARAN');
		$activeSheet->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$activeSheet->setCellValue('C3', 'Nomor Register');
		$activeSheet->setCellValue('D3', $nopendaftaran);

		$activeSheet->setCellValue('E3', 'Pelayanan');
		$activeSheet->setCellValue('F3', $pelayanan);

		$activeSheet->setCellValue('C4', 'No. Rekam Medis');
		$activeSheet->setCellValue('D4', $nomedrec);

		$activeSheet->setCellValue('E4', 'R. Perawatan');
		$activeSheet->setCellValue('F4', $kelas . ' - ' . $bed);

		$activeSheet->setCellValue('C5', 'Nama Pasien');
		$activeSheet->setCellValue('D5', $namapasien);

		$activeSheet->setCellValue('E5', 'Alamat');
		$activeSheet->setCellValue('F5', $alamatpasien);

		$activeSheet->setCellValue('C6', 'Kelompok Pasien');
		$activeSheet->setCellValue('D6', $kelompokpasien);

		$activeSheet->setCellValue('E6', 'Penjamin');
		$activeSheet->setCellValue('F6', $rekanan);

		$activeSheet->setCellValue('C7', 'Mulai Dirawat');
		$activeSheet->setCellValue('D7', $tanggaldaftar);

		$activeSheet->setCellValue('E7', 'Akhir Dirawat');
		$activeSheet->setCellValue('F7', $tanggalkeluar);

		$activeSheet->mergeCells('B2:G2');
		$activeSheet->getStyle('B2')->applyFromArray(['font' => ['bold' => true]]);
		$activeSheet->getStyle('C3:C7')->applyFromArray(['font' => ['bold' => true]]);
		$activeSheet->getStyle('E3:E7')->applyFromArray(['font' => ['bold' => true]]);

		$activeSheet->getStyle('B2:G7')->applyFromArray(
			[
				'borders' => [
					'top' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'right' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'bottom' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'left' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
				]
			]
		);

		$x = 8;

		$show = 0;
		$totalRanapRuangan = 0;
		if ($idtipe == 1) {
			if ($statusvalidasi == 1) {
				$dataRuanganValidasi = $this->Trawatinap_verifikasi_model->viewRincianRanapRuanganValidasi($idpendaftaran);
				$jumlahhistory = COUNT($dataRuanganValidasi);
				foreach ($dataRuanganValidasi as $row) {
					$totalKeseluruhan = $row->total * $row->jumlahhari;

					if ($show == 0) {
						$count = $x + $jumlahhistory - 1;
						$activeSheet->setCellValue("B$x", 'Kelas Perawatan');
						$activeSheet->mergeCells("B$x:C$count");
					}

					$activeSheet->setCellValue("D$x", "$row->namatarif	:	" . number_format($row->jumlahhari) . '	hari	@		Rp.	' . number_format($row->total));
					$activeSheet->setCellValue("G$x", $totalKeseluruhan);
					$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
					$activeSheet->mergeCells("D$x:F$x");

					$x = $x + 1;

					$show = 1;
					$totalRanapRuangan = $totalRanapRuangan + $totalKeseluruhan;
				}
			} else {
				$dataRuanganGroup = $this->Trawatinap_verifikasi_model->viewRincianRanapRuanganGroup($idpendaftaran);
				$jumlahhistory = COUNT($dataRuanganGroup);
				foreach ($dataRuanganGroup as $row) {
					$tarifRanapRuangan = $this->Trawatinap_tindakan_model->getTarifRuangan($row->idkelompokpasien, $row->idruangan, $row->idkelas);
					$totalKeseluruhan = $tarifRanapRuangan['total'] * $row->jumlahhari;

					if ($show == 0) {
						$count = $x + $jumlahhistory - 1;
						$activeSheet->setCellValue("B$x", 'Kelas Perawatan');
						$activeSheet->mergeCells("B$x:C$count");
					}

					$activeSheet->setCellValue("D$x", "Ruang $row->namaruangan, Kelas $row->namakelas	:	" . number_format($row->jumlahhari) . '	hari	@		Rp.	' . number_format($tarifRanapRuangan['total']));
					$activeSheet->setCellValue("G$x", $totalKeseluruhan);
					$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
					$activeSheet->mergeCells("D$x:F$x");

					$x = $x + 1;

					$show = 1;
					$totalRanapRuangan = $totalRanapRuangan + $totalKeseluruhan;
				}
			}
		}

		$activeSheet->setCellValue("B$x", 'RAWAT INAP [FULL CARE]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$totalRanapFullCare = 0;
		$dataRanapFullCare = $this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 1);
		if (COUNT($dataRanapFullCare) == 0) {
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
			$x = $x + 1;
		}

		foreach ($dataRanapFullCare as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$x = $x + 1;
				$totalRanapFullCare = $totalRanapFullCare + $row->totalkeseluruhan;
			}
		}

		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL RAWAT INAP [FULL CARE]');
		$activeSheet->setCellValue("G$x", $totalRanapFullCare);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'RAWAT INAP [ECG]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalRanapECG = 0;
		$dataRanapECG = $this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 2);
		if (COUNT($dataRanapECG) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataRanapECG as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalRanapECG = $totalRanapECG + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL RAWAT INAP [ECG]');
		$activeSheet->setCellValue("G$x", $totalRanapECG);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'RAWAT INAP [VISITE DOKTER]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalRanapVisite = 0;
		$dataRanapVisite = $this->Trawatinap_verifikasi_model->viewRincianRanapVisite($idpendaftaran);
		if (COUNT($dataRanapVisite) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataRanapVisite as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namadokter . ' (' . $row->namaruangan . ')');
				$activeSheet->setCellValue("E$x", 1);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalRanapVisite = $totalRanapVisite + $row->totalkeseluruhan;
			}
		}

		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL RAWAT INAP [VISITE DOKTER]');
		$activeSheet->setCellValue("G$x", $totalRanapVisite);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'RAWAT INAP [SEWA ALAT]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalRanapSewaAlat = 0;
		$dataRanapSewaAlat = $this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 4);
		if (COUNT($dataRanapSewaAlat) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataRanapSewaAlat as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalRanapSewaAlat = $totalRanapSewaAlat + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL RAWAT INAP [SEWA ALAT]');
		$activeSheet->setCellValue("G$x", $totalRanapVisite);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'RAWAT INAP [AMBULANCE]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalRanapAmbulance = 0;
		$dataRanapAmbulance = $this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 5);
		if (COUNT($dataRanapAmbulance) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataRanapAmbulance as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalRanapAmbulance = $totalRanapAmbulance + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL RAWAT INAP [AMBULANCE]');
		$activeSheet->setCellValue("G$x", $totalRanapAmbulance);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'RAWAT INAP [OBAT]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$dataRanapObat = $this->Trawatinap_verifikasi_model->viewRincianRanapObat($idpendaftaran);
		$dataFarmasiObat = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapObat($idpendaftaran);
		$dataFarmasiReturObat = $this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapObat($idpendaftaran);
		if (COUNT($dataFarmasiObat) == 0 and COUNT($dataFarmasiReturObat) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		$totalRanapObat = 0;
		foreach ($dataRanapObat as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->hargajual);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalRanapObat = $totalRanapObat + $row->totalkeseluruhan;
			}
		}

		$totalFarmasiObat = 0;
		foreach ($dataFarmasiObat as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopenjualan);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->hargajual);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalFarmasiObat = $totalFarmasiObat + $row->totalkeseluruhan;
			}
		}

		$totalFarmasiReturObat = 0;
		foreach ($dataFarmasiReturObat as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopengembalian);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->hargajual);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalFarmasiReturObat = $totalFarmasiReturObat + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL RAWAT INAP [OBAT]');
		$activeSheet->setCellValue("G$x", $totalRanapObat + $totalFarmasiObat + $totalFarmasiReturObat);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'RAWAT INAP [ALAT KESEHATAN]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$dataRanapAlkes = $this->Trawatinap_verifikasi_model->viewRincianRanapAlkes($idpendaftaran);
		$dataFarmasiAlkes = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapAlkes($idpendaftaran);
		$dataFarmasiReturAlkes = $this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapAlkes($idpendaftaran);

		if (COUNT($dataRanapAlkes) == 0 and COUNT($dataFarmasiAlkes) == 0 and COUNT($dataFarmasiReturAlkes) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		$totalRanapAlkes = 0;
		foreach ($dataRanapAlkes as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->hargajual);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalRanapAlkes = $totalRanapAlkes + $row->totalkeseluruhan;
			}
		}

		$totalFarmasiAlkes = 0;
		foreach ($dataFarmasiAlkes as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopenjualan);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->hargajual);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalFarmasiAlkes = $totalFarmasiAlkes + $row->totalkeseluruhan;
			}
		}

		$totalFarmasiReturAlkes = 0;
		foreach ($dataFarmasiReturAlkes as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopengembalian);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->hargajual);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalFarmasiReturAlkes = $totalFarmasiReturAlkes + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL RAWAT INAP [ALAT KESEHATAN]');
		$activeSheet->setCellValue("G$x", $totalRanapAlkes + $totalFarmasiAlkes + $totalFarmasiReturAlkes);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'RAWAT INAP [ALAT KESEHATAN] [ALAT BANTU]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$dataRanapAlkesBantu = $this->Trawatinap_verifikasi_model->viewRincianRanapAlkesBantu($idpendaftaran);
		$dataFarmasiAlkesBantu = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapAlkesBantu($idpendaftaran);
		$dataFarmasiReturAlkesBantu = $this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapAlkesBantu($idpendaftaran);

		if (COUNT($dataRanapAlkesBantu) == 0 and COUNT($dataFarmasiAlkesBantu) == 0 and COUNT($dataFarmasiReturAlkesBantu) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		$totalRanapAlkesBantu = 0;
		foreach ($dataRanapAlkesBantu as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->hargajual);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalRanapAlkes = $totalRanapAlkes + $row->totalkeseluruhan;
			}
		}

		$totalFarmasiAlkesBantu = 0;
		foreach ($dataFarmasiAlkesBantu as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopenjualan);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->harga);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalFarmasiAlkesBantu = $totalFarmasiAlkesBantu + $row->totalkeseluruhan;
			}
		}

		$totalFarmasiReturAlkesBantu = 0;
		foreach ($dataFarmasiReturAlkesBantu as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopengembalian);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->harga);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalFarmasiReturAlkesBantu = $totalFarmasiReturAlkesBantu + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL RAWAT INAP [ALAT KESEHATAN] [ALAT BANTU]');
		$activeSheet->setCellValue("G$x", $totalRanapAlkesBantu + $totalFarmasiAlkesBantu + $totalFarmasiReturAlkesBantu);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'RAWAT INAP [LAIN-LAIN]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalRanapLainLain = 0;
		$dataRanapLainLain = $this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 6);
		if (COUNT($dataRanapLainLain) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataRanapLainLain as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalRanapLainLain = $totalRanapLainLain + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL RAWAT INAP [LAIN-LAIN]');
		$activeSheet->setCellValue("G$x", $totalRanapLainLain);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'IGD');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalPoli = 0;
		$dataPoli = $this->Trawatinap_verifikasi_model->viewRincianRajalTindakanFilteredIGD($idpendaftaran);
		if (COUNT($dataPoli) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataPoli as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif . '' . $row->jasapelayanan > 0 ? ' - ' . $row->namadokter : '');
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalPoli = $totalPoli + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL IGD');
		$activeSheet->setCellValue("G$x", $totalPoli);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'IGD [OBAT]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalPoliObat = 0;
		$totalFarmasiObatIGD = 0;
		$dataPoliObat = $this->Trawatinap_verifikasi_model->viewRincianRajalObatFilteredIGD($idpendaftaran, 2);
		$dataFarmasiObatIGD = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran, 3);
		if (COUNT($dataPoliObat) == 0 && COUNT($dataFarmasiObatIGD) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataPoliObat as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->hargajual);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalPoliObat = $totalPoliObat + $row->totalkeseluruhan;
			}
		}

		foreach ($dataFarmasiObatIGD as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopenjualan);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->hargajual);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalFarmasiObatIGD = $totalFarmasiObatIGD + $row->totalkeseluruhan;
			}
		}

		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL IGD [OBAT]');
		$activeSheet->setCellValue("G$x", $totalPoliObat + $totalFarmasiObatIGD);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'IGD [ALAT KESEHATAN]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalPoliAlkes = 0;
		$totalFarmasiAlkesIGD = 0;
		$dataPoliAlkes = $this->Trawatinap_verifikasi_model->viewRincianRajalAlkesFilteredIGD($idpendaftaran);
		$dataFarmasiAlkesIGD = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran, 1);
		if (COUNT($dataPoliAlkes) == 0 && COUNT($dataFarmasiAlkesIGD) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataPoliAlkes as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->hargajual);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalPoliAlkes = $totalPoliAlkes + $row->totalkeseluruhan;
			}
		}

		foreach ($dataFarmasiAlkesIGD as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopenjualan);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->hargajual);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalFarmasiAlkesIGD = $totalFarmasiAlkesIGD + $row->totalkeseluruhan;
			}
		}

		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL IGD [ALAT KESEHATAN]');
		$activeSheet->setCellValue("G$x", $totalPoliAlkes + $totalFarmasiAlkesIGD);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'LABORATORIUM [UMUM]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalLab = 0;
		$dataLab = $this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 1);
		if (COUNT($dataLab) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataLab as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->norujukan);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalLab = $totalLab + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL LABORATORIUM [UMUM]');
		$activeSheet->setCellValue("G$x", $totalLab);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'LABORATORIUM [PATHOLOGI ANATOMI]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalLabPA = 0;
		$dataLabPA = $this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 2);
		if (COUNT($dataLabPA) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataLabPA as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->norujukan);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalLabPA = $totalLabPA + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL LABORATORIUM [PATHOLOGI ANATOMI]');
		$activeSheet->setCellValue("G$x", $totalLabPA);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'LABORATORIUM [PMI]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalLabPMI = 0;
		$dataLabPMI = $this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 3);
		if (COUNT($dataLabPMI) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataLabPMI as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->norujukan);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalLabPMI = $totalLabPMI + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL LABORATORIUM [PMI]');
		$activeSheet->setCellValue("G$x", $totalLabPMI);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'RADIOLOGI [X-RAY]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalRadXray = 0;
		$dataRadXray = $this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 1);
		if (COUNT($dataRadXray) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataRadXray as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->norujukan);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalRadXray = $totalRadXray + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL RADIOLOGI [X-RAY]');
		$activeSheet->setCellValue("G$x", $totalRadXray);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'RADIOLOGI [USG]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalRadUSG = 0;
		$dataRadUSG = $this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 2);
		if (COUNT($dataRadUSG) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataRadUSG as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->norujukan);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalRadUSG = $totalRadUSG + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL RADIOLOGI [USG]');
		$activeSheet->setCellValue("G$x", $totalRadUSG);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'RADIOLOGI [CT SCAN]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalRadCTScan = 0;
		$dataRadCTScan = $this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 3);
		if (COUNT($dataRadCTScan) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataRadCTScan as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->norujukan);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalRadCTScan = $totalRadCTScan + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL RADIOLOGI [CT SCAN]');
		$activeSheet->setCellValue("G$x", $totalRadCTScan);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'RADIOLOGI [MRI]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalRadMRI = 0;
		$dataRadMRI = $this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 4);
		if (COUNT($dataRadMRI) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataRadMRI as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->norujukan);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalRadMRI = $totalRadMRI + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL RADIOLOGI [MRI]');
		$activeSheet->setCellValue("G$x", $totalRadMRI);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'RADIOLOGI [BMD]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalRadBMD = 0;
		$dataRadBMD = $this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 5);
		if (COUNT($dataRadBMD) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataRadBMD as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->norujukan);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalRadBMD = $totalRadBMD + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL RADIOLOGI [BMD]');
		$activeSheet->setCellValue("G$x", $totalRadBMD);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'FISIOTERAPI');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalFisio = 0;
		$dataFisio = $this->Trawatinap_verifikasi_model->viewRincianFisioterapi($idpendaftaran);
		if (COUNT($dataFisio) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataFisio as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->norujukan);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalFisio = $totalFisio + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL FISIOTERAPI');
		$activeSheet->setCellValue("G$x", $totalFisio);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'KAMAR OPERASI [SEWA ALAT]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalOKSewaAlat = 0;
		$dataOKSewaAlat = $this->Trawatinap_verifikasi_model->viewRincianOperasiSewaAlat($idpendaftaran);
		if (COUNT($dataOKSewaAlat) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataOKSewaAlat as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggaloperasi));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->total);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalOKSewaAlat = $totalOKSewaAlat + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL KAMAR OPERASI [SEWA ALAT]');
		$activeSheet->setCellValue("G$x", $totalOKSewaAlat);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'KAMAR OPERASI [ALAT KESEHATAN]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalOKAlkes = 0;
		$dataOKAlkes = $this->Trawatinap_verifikasi_model->viewRincianOperasiAlkes($idpendaftaran);
		if (COUNT($dataOKAlkes) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataOKAlkes as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggaloperasi));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->hargajual);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalOKAlkes = $totalOKAlkes + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL KAMAR OPERASI [ALAT KESEHATAN]');
		$activeSheet->setCellValue("G$x", $totalOKAlkes);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'KAMAR OPERASI [OBAT]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalOKObat = 0;
		$dataOKObat = $this->Trawatinap_verifikasi_model->viewRincianOperasiObat($idpendaftaran);
		if (COUNT($dataOKObat) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}
		foreach ($dataOKObat as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggaloperasi));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->hargajual);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalOKObat = $totalOKObat + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL KAMAR OPERASI [OBAT]');
		$activeSheet->setCellValue("G$x", $totalOKObat);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'KAMAR OPERASI [OBAT NARCOSE]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalOKObatNarcose = 0;
		$dataOKObatNarcose = $this->Trawatinap_verifikasi_model->viewRincianOperasiNarcose($idpendaftaran);
		if (COUNT($dataOKObatNarcose) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataOKObatNarcose as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggaloperasi));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->hargajual);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalOKObatNarcose = $totalOKObatNarcose + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL KAMAR OPERASI [OBAT NARCOSE]');
		$activeSheet->setCellValue("G$x", $totalOKObatNarcose);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'KAMAR OPERASI [IMPLAN]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalOKImplan = 0;
		$dataOKImplan = $this->Trawatinap_verifikasi_model->viewRincianOperasiImplan($idpendaftaran);
		if (COUNT($dataOKImplan) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataOKImplan as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggaloperasi));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", $row->kuantitas);
				$activeSheet->setCellValue("F$x", $row->hargajual);
				$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalOKImplan = $totalOKImplan + $row->totalkeseluruhan;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL KAMAR OPERASI [IMPLAN]');
		$activeSheet->setCellValue("G$x", $totalOKImplan);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'KAMAR OPERASI [SEWA KAMAR]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalOKSewaKamar = 0;
		$dataOKSewaKamar = $this->Trawatinap_verifikasi_model->viewRincianOperasiKamar($idpendaftaran);
		if (COUNT($dataOKSewaKamar) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataOKSewaKamar as $row) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", $row->nopendaftaran);
			$activeSheet->setCellValue("C$x", DMYFormat($row->tanggaloperasi));
			$activeSheet->setCellValue("D$x", $row->namatarif);
			$activeSheet->setCellValue("E$x", $row->kuantitas);
			$activeSheet->setCellValue("F$x", $row->total);
			$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

			$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

			$totalOKSewaKamar = $totalOKSewaKamar + $row->totalkeseluruhan;
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL KAMAR OPERASI [SEWA KAMAR]');
		$activeSheet->setCellValue("G$x", $totalOKSewaKamar);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'KAMAR OPERASI [JASA MEDIS]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalOKJasaMedis = 0;
		$dataOKJasaMedis = $this->Trawatinap_verifikasi_model->viewRincianOperasiJasaMedisPrint($idpendaftaran);
		if (COUNT($dataOKJasaMedis) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataOKJasaMedis as $row) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", $row->nopendaftaran);
			$activeSheet->setCellValue("C$x", DMYFormat($row->tanggaloperasi));
			$activeSheet->setCellValue("D$x", $row->namatarif);
			$activeSheet->setCellValue("E$x", $row->kuantitas);
			$activeSheet->setCellValue("F$x", $row->total);
			$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

			$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

			$totalOKJasaMedis = $totalOKJasaMedis + $row->totalkeseluruhan;
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL KAMAR OPERASI [JASA MEDIS]');
		$activeSheet->setCellValue("G$x", $totalOKJasaMedis);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'KAMAR OPERASI [JASA ASISTEN]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalOKJasaAsisten = 0;
		$dataOKJasaAsisten = $this->Trawatinap_verifikasi_model->viewRincianOperasiJasaAsisten($idpendaftaran);
		if (COUNT($dataOKJasaAsisten) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($dataOKJasaAsisten as $row) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", $row->nopendaftaran);
			$activeSheet->setCellValue("C$x", DMYFormat($row->tanggaloperasi));
			$activeSheet->setCellValue("D$x", $row->namatarif);
			$activeSheet->setCellValue("E$x", $row->kuantitas);
			$activeSheet->setCellValue("F$x", $row->total);
			$activeSheet->setCellValue("F$x", $row->grandtotal);

			$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

			$totalOKJasaAsisten = $totalOKJasaAsisten + $row->grandtotal;
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL KAMAR OPERASI [JASA ASISTEN]');
		$activeSheet->setCellValue("G$x", $totalOKJasaAsisten);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'ADMINISTRASI [RAWAT JALAN]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalAdmRajal = 0;
		$datAdmRajal = $this->Trawatinap_verifikasi_model->viewRincianAdministrasiRawatJalan($idpendaftaran);
		if (COUNT($datAdmRajal) == 0) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", '-');
			$activeSheet->mergeCells("B$x:G$x");
		}

		foreach ($datAdmRajal as $row) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", $row->nopendaftaran);
			$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
			$activeSheet->setCellValue("D$x", $row->namatarif);
			$activeSheet->setCellValue("E$x", $row->kuantitas);
			$activeSheet->setCellValue("F$x", $row->total);
			$activeSheet->setCellValue("G$x", $row->totalkeseluruhan);

			$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

			$totalAdmRajal = $totalAdmRajal + $row->totalkeseluruhan;
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL ADMINISTRASI [RAWAT JALAN]');
		$activeSheet->setCellValue("G$x", $totalAdmRajal);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalSebelumAdm = $totalRanapFullCare + $totalRanapECG + $totalRanapVisite + $totalRanapSewaAlat +
		$totalRanapAmbulance + $totalRanapAlkes + $totalRanapAlkesBantu + $totalRanapLainLain + $totalPoli +
		$totalPoliObat + $totalFarmasiObatIGD + $totalPoliAlkes + $totalFarmasiAlkesIGD + $totalRanapObat + $totalFarmasiObat + $totalFarmasiReturObat + $totalFarmasiAlkes +
		$totalFarmasiReturAlkes + $totalFarmasiAlkesBantu + $totalFarmasiReturAlkesBantu + $totalLab + $totalLabPA + $totalLabPMI +
		$totalRadXray + $totalRadUSG + $totalRadCTScan + $totalRadMRI + $totalRadBMD +
		$totalFisio + $totalOKSewaAlat + $totalOKAlkes + $totalOKObat + $totalOKObatNarcose +
		$totalOKImplan + $totalOKSewaKamar + $totalOKJasaMedis + $totalOKJasaAsisten +
		$totalRanapRuangan + $totalAdmRajal;

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'ADMINISTRASI [RAWAT INAP]');
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalAdmRanap = 0;
		if ($statusvalidasi == 1) {
			$dataAdminRanap = get_all('trawatinap_administrasi', ['idpendaftaran' => $idrawatinap]);
			if (COUNT($dataAdminRanap) == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", '-');
				$activeSheet->mergeCells("B$x:G$x");
			}

			foreach ($dataAdminRanap as $row) {
				$x = $x + 1;
				$ranap = get_by_field('id', $row->idpendaftaran, 'trawatinap_pendaftaran');

				$activeSheet->setCellValue("B$x", $ranap->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($ranap->tanggaldaftar));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", 1);
				$activeSheet->setCellValue("F$x", $row->tarifsetelahdiskon);
				$activeSheet->setCellValue("F$x", $row->tarifsetelahdiskon);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalAdmRanap = $totalAdmRanap + $row->tarifsetelahdiskon;
			}
		} else {
			$dataAdminRanap = $this->db->query("CALL spAdministrasiRanap($idpendaftaran, $totalSebelumAdm)")->result();
			if (COUNT($dataAdminRanap) == 0) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", '-');
				$activeSheet->mergeCells("B$x:G$x");
			}

			foreach ($dataAdminRanap as $row) {
				$x = $x + 1;
				$activeSheet->setCellValue("B$x", $row->nopendaftaran);
				$activeSheet->setCellValue("C$x", DMYFormat($row->tanggal));
				$activeSheet->setCellValue("D$x", $row->namatarif);
				$activeSheet->setCellValue("E$x", 1);
				$activeSheet->setCellValue("F$x", $row->tarif);
				$activeSheet->setCellValue("F$x", $row->tarif);

				$activeSheet->getStyle("E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("F$x:G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

				$totalAdmRanap = $totalAdmRanap + $row->tarif;
			}
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'SUB TOTAL ADMINISTRASI [RAWAT INAP]');
		$activeSheet->setCellValue("G$x", $totalAdmRanap);
		$activeSheet->mergeCells("D$x:F$x");
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);

		$totalKeseluruhan = ceil(($totalSebelumAdm + $totalAdmRanap) / 100) * 100;

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", '');
		$activeSheet->mergeCells("B$x:G$x");

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'TOTAL BIAYA PERAWATAN + DOKTER');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);
		$activeSheet->mergeCells("D$x:F$x");

		$activeSheet->setCellValue("G$x", $totalKeseluruhan);
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'UANG MUKA/TELAH DIBAYAR');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);
		$activeSheet->mergeCells("D$x:F$x");

		$activeSheet->setCellValue("G$x", $totaldeposit);
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'KURANG/LEBIH/JUMLAH');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);
		$activeSheet->mergeCells("D$x:F$x");

		$activeSheet->setCellValue("G$x", ($totalKeseluruhan >= $totaldeposit ? ($totalKeseluruhan - $totaldeposit) : ($totaldeposit - $totalKeseluruhan)));
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'DETAIL PEMBAYARAN');
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);
		$activeSheet->mergeCells("B$x:G$x");
		$activeSheet->getStyle("B$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'NO PEMBAYARAN');
		$activeSheet->setCellValue("D$x", 'TIPE PEMBAYARAN');
		$activeSheet->setCellValue("E$x", 'KETERANGAN');
		$activeSheet->setCellValue("G$x", 'NOMINAL');
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->mergeCells("E$x:F$x");
		$activeSheet->getStyle("B$x:G$x")->applyFromArray(['font' => ['bold' => true]]);

		$detail_deposit = $this->Trawatinap_tindakan_model->getHistoryDeposit($idrawatinap);

		$total_deposit = 0;
		foreach ($detail_deposit as $row) {
			if ($row->idmetodepembayaran == 1) {
				$metode = $row->metodepembayaran;
			} else {
				$metode = $row->metodepembayaran . ' : ' . $row->namabank;
			}

			$x = $x + 1;
			$activeSheet->setCellValue("B$x", $nopendaftaran);
			$activeSheet->setCellValue("D$x", 'DEPOSIT');
			$activeSheet->setCellValue("E$x", $metode);
			$activeSheet->setCellValue("G$x", $row->nominal);

			$activeSheet->mergeCells("B$x:C$x");
			$activeSheet->mergeCells("E$x:F$x");
			$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

			$total_deposit = $total_deposit + $row->nominal;
		}

		$pembayaran = $this->Trawatinap_tindakan_model->getInfoPembayaran($idrawatinap);
		if ($pembayaran) {
			$user_input_pembayaran = $pembayaran->user_input;
			$detail_pembayaran = $this->Trawatinap_tindakan_model->detail_pembayaran($pembayaran->id);
		} else {
			$detail_pembayaran = [];
			$user_input_pembayaran = '';
		}

		$total_pembayaran = 0;
		foreach ($detail_pembayaran as $row) {
			$x = $x + 1;
			$activeSheet->setCellValue("B$x", $nopendaftaran);
			$activeSheet->setCellValue("D$x", 'PEMBAYARAN');
			$activeSheet->setCellValue("E$x", $row->keterangan);
			$activeSheet->setCellValue("G$x", $row->nominal);

			$activeSheet->mergeCells("B$x:C$x");
			$activeSheet->mergeCells("E$x:F$x");
			$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

			$total_pembayaran = $total_pembayaran + $row->nominal;
		}

		$x = $x + 1;
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->setCellValue("D$x", 'TOTAL TERIMA PEMBAYARAN');
		$activeSheet->getStyle("D$x")->applyFromArray(['font' => ['bold' => true]]);
		$activeSheet->mergeCells("D$x:F$x");

		$activeSheet->setCellValue("G$x", $total_pembayaran + $total_deposit);
		$activeSheet->getStyle("G$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'Diagnosa');
		$activeSheet->setCellValue("D$x", 'Operasi');
		$activeSheet->setCellValue("F$x", 'Catatan');
		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->mergeCells("D$x:E$x");
		$activeSheet->mergeCells("F$x:G$x");
		$activeSheet->getStyle("D$x:G$x")->applyFromArray(['font' => ['bold' => true]]);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", $diagnosa);
		$activeSheet->setCellValue("D$x", $operasi);
		$activeSheet->setCellValue("F$x", $catatan);

		$activeSheet->mergeCells("B$x:C$x");
		$activeSheet->mergeCells("D$x:E$x");
		$activeSheet->mergeCells("F$x:G$x");

		$activeSheet->getStyle("B8:G$x")->applyFromArray(
			[
				'borders' => [
					'allborders' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
				]
			]
		);

		$x = $x + 1;
		$activeSheet->setCellValue("G$x", 'Bandung, ' . HumanDate($tanggal_pembayaran));

		$x = $x + 5;
		$activeSheet->setCellValue("G$x", $this->session->userdata('user_name'));

		// Set Auto Width Column
		$activeSheet->getColumnDimension('B')->setWidth(15);
		$activeSheet->getColumnDimension('C')->setWidth(15);
		$activeSheet->getColumnDimension('D')->setWidth(60);
		$activeSheet->getColumnDimension('E')->setWidth(10);
		$activeSheet->getColumnDimension('F')->setWidth(20);
		$activeSheet->getColumnDimension('G')->setWidth(20);

		header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Rincian Biaya.xls"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		$objWriter->save('php://output');
	}

	public function export_rincian_global($idrawatinap, $status)
	{
		$row = $this->Trawatinap_tindakan_model->getDataRawatInap($idrawatinap);
		$rowCheckout = $this->Trawatinap_tindakan_model->getDataCheckout($idrawatinap);
		$rowOperasi = $this->Trawatinap_tindakan_model->getDataOperasi($idrawatinap);

		if ($row->idtipe == 1) {
			if ($rowCheckout) {
				$tanggalkeluar = $rowCheckout->tanggalkeluar;
			} else {
				$tanggalkeluar = date('d-m-Y');
			}
		} else {
			$tanggalkeluar = $row->tanggaldaftar;
		}

		$totaldeposit = $this->Trawatinap_tindakan_model->getTotalDeposit($idrawatinap)->totaldeposit;
		$idpendaftaran = $row->id;
		$tanggaldaftar = $row->tanggaldaftar;
		$nopendaftaran = $row->nopendaftaran;
		$nomedrec = $row->nomedrec;
		$namapasien = $row->namapasien;
		$alamatpasien = $row->alamatpasien;
		$namakelas = $row->namakelas;
		$namabed = $row->namabed;
		$catatan = $row->catatan;
		$totaldeposit = $totaldeposit;
		$diagnosa = ($rowOperasi ? $rowOperasi->diagnosa : '');
		$operasi = ($rowOperasi ? $rowOperasi->operasi : '');
		$statusvalidasi = $row->statusvalidasi;
		$status = $status;

		$history_bed = $this->Trawatinap_tindakan_model->getHistoryPindahBed($idrawatinap);

		$pembayaran = $this->Trawatinap_tindakan_model->getInfoPembayaran($idrawatinap);
		$tanggal_pembayaran = $this->Trawatinap_tindakan_model->getTanggalPembayaran($idrawatinap);
		$user_input_pembayaran = ($pembayaran ? $pembayaran->user_input : '');

		// Excel Generate

		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle('Rincian Global');

		// // Set Logo
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$logo = FCPATH . '/assets/upload/logo/logoreport.jpg';
		$objDrawing->setPath($logo);
		$objDrawing->setCoordinates('B2');
		$objDrawing->setResizeProportional(true);
		$objDrawing->setHeight(30);
		$objDrawing->setWorksheet($activeSheet);
		$objDrawing->setOffsetX(50);

		$activeSheet->getRowDimension('2')->setRowHeight(40);

		$activeSheet->setCellValue('B2', '');
		$activeSheet->setCellValue('B3', 'RSKB HALMAHERA SIAGA');
		$activeSheet->setCellValue('B4', 'Jl.LLRE.Martadinata No.28');
		$activeSheet->setCellValue('B5', 'Bandung');
		$activeSheet->setCellValue('B6', 'T. (022) 4206717');
		$activeSheet->setCellValue('B7', 'F. (022) 4216436');
		$activeSheet->getStyle('B2:B7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle('B2:B7')->applyFromArray(
			[
				'borders' => [
					'top' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'right' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'bottom' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'left' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
				]
			]
		);

		$activeSheet->getStyle('B3')->applyFromArray(['font' => ['bold' => true]]);
		$activeSheet->getStyle('C2')->applyFromArray(['font' => ['bold' => true]]);
		$activeSheet->getStyle('D2')->applyFromArray(['font' => ['bold' => true]]);

		$activeSheet->setCellValue('C2', 'NOTA RINCIAN');
		$activeSheet->setCellValue('C3', 'BIAYA PERAWATAN');
		$activeSheet->mergeCells('C2:D2');
		$activeSheet->mergeCells('C3:D3');
		$activeSheet->getStyle('C2:D3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle('C2:D3')->applyFromArray(
			[
				'borders' => [
					'top' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'right' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'bottom' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'left' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
				]
			]
		);

		$activeSheet->setCellValue('E2', $nopendaftaran);
		$activeSheet->setCellValue('E3', 'Tanggal ' . date('d/m/Y'));
		$activeSheet->mergeCells('E2:F2');
		$activeSheet->mergeCells('E3:F3');
		$activeSheet->getStyle('E2:F3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle('E2:F3')->applyFromArray(
			[
				'borders' => [
					'top' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'right' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'bottom' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'left' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
				]
			]
		);

		$activeSheet->getStyle('E2')->applyFromArray(['font' => ['bold' => true]]);

		$activeSheet->setCellValue('C4', 'Nomor Register / Medrec');
		$activeSheet->setCellValue('D4', "$nopendaftaran / $nomedrec");

		$activeSheet->setCellValue('C5', 'Nama Pasien');
		$activeSheet->setCellValue('D5', $namapasien);

		$activeSheet->setCellValue('C6', 'Alamat');
		$activeSheet->setCellValue('D6', $alamatpasien);

		$activeSheet->setCellValue('C7', 'Bagian Kamar');
		$activeSheet->setCellValue('D7', $namabed);

		$activeSheet->getStyle('C4:F7')->applyFromArray(
			[
				'borders' => [
					'top' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'right' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'bottom' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'left' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
				]
			]
		);

		$activeSheet->setCellValue('B8', 'Mulai Dirawat Tanggal :');
		$activeSheet->setCellValue('C8', $tanggaldaftar);
		$activeSheet->getStyle('C8')->applyFromArray(['font' => ['bold' => true]]);

		$activeSheet->setCellValue('E8', 'Sampai Dengan Tanggal :');
		$activeSheet->setCellValue('F8', $tanggalkeluar);
		$activeSheet->getStyle('C8')->applyFromArray(['font' => ['bold' => true]]);
		$activeSheet->getStyle('F8')->applyFromArray(['font' => ['bold' => true]]);
		$activeSheet->getStyle('B8:F8')->applyFromArray(
			[
				'borders' => [
					'top' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'right' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'bottom' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
					'left' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
				]
			]
		);

		$x = 9;

		$totalRanapRuangan = 0;
		if ($statusvalidasi == 1) {
			$dataRuanganValidasi = $this->Trawatinap_verifikasi_model->viewRincianRanapRuanganValidasi($idpendaftaran);
			$jumlahhistory = COUNT($dataRuanganValidasi);
			foreach ($dataRuanganValidasi as $row) {
				$totalKeseluruhan = $row->total * $row->jumlahhari;
				$activeSheet->setCellValue("B$x", 'Kelas Perawatan');
				$activeSheet->getStyle("B$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("B$x")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$activeSheet->setCellValue("C$x", "$row->namatarif : " . number_format($row->jumlahhari) . ' hari @ ' . number_format($row->total));
				$activeSheet->setCellValue("E$x", $totalKeseluruhan);
				$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
				$activeSheet->mergeCells("C$x:D$x");
				$activeSheet->mergeCells("E$x:F$x");
				$activeSheet->getStyle("B$x:F$x")->applyFromArray(
					[
						'borders' => [
							'allborders' => [
								'style' => PHPExcel_Style_Border::BORDER_THIN,
							],
						]
					]
				);

				$totalRanapRuangan = $totalRanapRuangan + $totalKeseluruhan;
				$x++;
			}
		} else {
			$dataRuanganGroup = $this->Trawatinap_verifikasi_model->viewRincianRanapRuanganGroup($idpendaftaran);
			$jumlahhistory = COUNT($dataRuanganGroup);
			foreach ($dataRuanganGroup as $row) {
				$tarifRanapRuangan = $this->Trawatinap_tindakan_model->getTarifRuangan($row->idkelompokpasien, $row->idruangan, $row->idkelas);
				$totalKeseluruhan = $tarifRanapRuangan['total'] * $row->jumlahhari;

				$activeSheet->setCellValue("B$x", 'Kelas Perawatan');
				$activeSheet->getStyle("B$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$activeSheet->getStyle("B$x")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$activeSheet->setCellValue("C$x", "Ruang $row->namaruangan, Kelas $row->namakelas : " . number_format($row->jumlahhari) . ' hari	@	' . number_format($tarifRanapRuangan['total']));
				$activeSheet->setCellValue("E$x", $totalKeseluruhan);
				$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
				$activeSheet->mergeCells("C$x:D$x");
				$activeSheet->mergeCells("E$x:F$x");
				$activeSheet->getStyle("B$x:F$x")->applyFromArray(
					[
						'borders' => [
							'allborders' => [
								'style' => PHPExcel_Style_Border::BORDER_THIN,
							],
						]
					]
				);

				$totalRanapRuangan = $totalRanapRuangan + $totalKeseluruhan;
				$x++;
			}
		}

		$totalPoli = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRajalTindakanFilteredIGD($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalPoli = $totalPoli + $row->totalkeseluruhan;
			}
		}

		$totalPoliObat = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRajalObatFilteredIGD($idpendaftaran, 2) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalPoliObat = $totalPoliObat + $row->totalkeseluruhan;
			}
		}

		$totalPoliAlkes = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRajalAlkesFilteredIGD($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalPoliAlkes = $totalPoliAlkes + $row->totalkeseluruhan;
			}
		}

		$totalFarmasiObatIGD = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran, 3) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalFarmasiObatIGD = $totalFarmasiObatIGD + $row->totalkeseluruhan;
			}
		}

		$totalFarmasiAlkesIGD = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran, 1) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalFarmasiAlkesIGD = $totalFarmasiAlkesIGD + $row->totalkeseluruhan;
			}
		}

		$totalRanapObat = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapObat($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalRanapObat = $totalRanapObat + $row->totalkeseluruhan;
			}
		}

		$totalFarmasiObat = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapObat($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalFarmasiObat = $totalFarmasiObat + $row->totalkeseluruhan;
			}
		}

		$totalFarmasiReturObat = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapObat($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalFarmasiReturObat = $totalFarmasiReturObat + $row->totalkeseluruhan;
			}
		}

		$totalRanapSewaAlat = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 4) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalRanapSewaAlat = $totalRanapSewaAlat + $row->totalkeseluruhan;
			}
		}

		$totalOKSewaAlat = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiSewaAlat($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalOKSewaAlat = $totalOKSewaAlat + $row->totalkeseluruhan;
			}
		}

		$totalOKSewaKamar = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiKamar($idpendaftaran) as $row) {
			$totalOKSewaKamar = $totalOKSewaKamar + $row->totalkeseluruhan;
		}

		$totalOKObatNarcose = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiNarcose($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalOKObatNarcose = $totalOKObatNarcose + $row->totalkeseluruhan;
			}
		}

		$totalOKObat = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiObat($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalOKObat = $totalOKObat + $row->totalkeseluruhan;
			}
		}

		$totalOKAlkes = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiAlkes($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalOKAlkes = $totalOKAlkes + $row->totalkeseluruhan;
			}
		}

		$totalOKJasaAsisten = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaAsisten($idpendaftaran) as $row) {
			$totalOKJasaAsisten = $totalOKJasaAsisten + $row->grandtotal;
		}

		$totalRanapAlkes = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapAlkes($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalRanapAlkes = $totalRanapAlkes + $row->totalkeseluruhan;
			}
		}

		$totalFarmasiReturAlkes = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapAlkes($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalFarmasiReturAlkes = $totalFarmasiReturAlkes + $row->totalkeseluruhan;
			}
		}

		$totalFarmasiAlkes = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapAlkes($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalFarmasiAlkes = $totalFarmasiAlkes + $row->totalkeseluruhan;
			}
		}

		$totalRadXray = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 1) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalRadXray = $totalRadXray + $row->totalkeseluruhan;
			}
		}

		$totalRadUSG = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 2) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalRadUSG = $totalRadUSG + $row->totalkeseluruhan;
			}
		}

		$totalRadCTScan = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 3) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalRadCTScan = $totalRadCTScan + $row->totalkeseluruhan;
			}
		}

		$totalRadMRI = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 4) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalRadMRI = $totalRadMRI + $row->totalkeseluruhan;
			}
		}

		$totalRadBMD = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 5) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalRadBMD = $totalRadBMD + $row->totalkeseluruhan;
			}
		}

		$totalFisio = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianFisioterapi($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalFisio = $totalFisio + $row->totalkeseluruhan;
			}
		}

		$totalLab = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 1) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalLab = $totalLab + $row->totalkeseluruhan;
			}
		}

		$totalLabPA = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 2) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalLabPA = $totalLabPA + $row->totalkeseluruhan;
			}
		}

		$totalRanapECG = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 2) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalRanapECG = $totalRanapECG + $row->totalkeseluruhan;
			}
		}

		$totalRanapFullCare = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 1) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalRanapFullCare = $totalRanapFullCare + $row->totalkeseluruhan;
			}
		}

		$totalOKImplan = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiImplan($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalOKImplan = $totalOKImplan + $row->totalkeseluruhan;
			}
		}

		$totalRanapAlkesBantu = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapAlkesBantu($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalRanapAlkesBantu = $totalRanapAlkesBantu + $row->totalkeseluruhan;
			}
		}

		$totalFarmasiAlkesBantu = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapAlkesBantu($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalFarmasiAlkesBantu = $totalFarmasiAlkesBantu + $row->totalkeseluruhan;
			}
		}

		$totalFarmasiReturAlkesBantu = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapAlkesBantu($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalFarmasiReturAlkesBantu = $totalFarmasiReturAlkesBantu + $row->totalkeseluruhan;
			}
		}

		$totalRanapAmbulance = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 5) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalRanapAmbulance = $totalRanapAmbulance + $row->totalkeseluruhan;
			}
		}

		$totalLabPMI = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 3) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalLabPMI = $totalLabPMI + $row->totalkeseluruhan;
			}
		}

		$totalRanapLainLain = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 6) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalRanapLainLain = $totalRanapLainLain + $row->totalkeseluruhan;
			}
		}

		$totalRanapVisite = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapVisite($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalRanapVisite = $totalRanapVisite + $row->totalkeseluruhan;
			}
		}

		$totalOKJasaDokterOperator = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterOpertor($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalOKJasaDokterOperator = $totalOKJasaDokterOperator + $row->totalkeseluruhan;
			}
		}

		$totalOKJasaDokterAnesthesi = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterAnesthesi($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalOKJasaDokterAnesthesi = $totalOKJasaDokterAnesthesi + $row->grandtotal;
			}
		}

		$totalAdmRajal = 0;
		foreach ($this->Trawatinap_verifikasi_model->viewRincianAdministrasiRawatJalan($idpendaftaran) as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$totalAdmRajal = $totalAdmRajal + $row->totalkeseluruhan;
			}
		}

		$totalSebelumAdm = $totalRanapFullCare + $totalRanapECG + $totalRanapVisite + $totalRanapSewaAlat +
		$totalRanapAmbulance + $totalRanapAlkes + $totalRanapAlkesBantu + $totalRanapLainLain + $totalPoli +
		$totalPoliObat + $totalFarmasiObatIGD + $totalFarmasiAlkesIGD + $totalPoliAlkes + $totalRanapObat + $totalFarmasiObat + $totalFarmasiReturObat + $totalFarmasiAlkes +
		$totalFarmasiReturAlkes + $totalFarmasiAlkesBantu + $totalFarmasiReturAlkesBantu + $totalLab + $totalLabPA + $totalLabPMI +
		$totalRadXray + $totalRadUSG + $totalRadCTScan + $totalRadMRI + $totalRadBMD +
		$totalFisio + $totalOKSewaAlat + $totalOKAlkes + $totalOKObat + $totalOKObatNarcose +
		$totalOKImplan + $totalOKSewaKamar + $totalOKJasaDokterOperator + $totalOKJasaDokterAnesthesi + $totalOKJasaAsisten +
		$totalRanapRuangan + $totalAdmRajal;

		$totalAdmRanap = 0;
		if ($statusvalidasi == 1) {
			$dataAdminRanap = get_all('trawatinap_administrasi', ['idpendaftaran' => $idpendaftaran]);
			foreach ($dataAdminRanap as $row) {
				$totalAdmRanap = $totalAdmRanap + $row->tarifsetelahdiskon;
			}
		} else {
			$dataAdminRanap = $this->db->query("CALL spAdministrasiRanap($idpendaftaran, $totalSebelumAdm)")->result();
			foreach ($dataAdminRanap as $row) {
				$totalAdmRanap = $totalAdmRanap + $row->tarif;
			}
		}

		$xStart = $x;
		$xEnd = $x + 14;

		$activeSheet->setCellValue("B$x", 'Pertolongan dan Pemeriksaan');
		$activeSheet->mergeCells("B$xStart:B$xEnd");

		$activeSheet->getStyle("B$x:B$xEnd")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("B$x:B$xEnd")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$x = $x;

		$activeSheet->setCellValue("C$x", 'Pertolongan di Poli Gawat Darurat');
		$activeSheet->setCellValue("E$x", $totalPoli + $totalPoliObat + $totalPoliAlkes + $totalFarmasiObatIGD + $totalFarmasiAlkesIGD);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Sewa Alat');
		$activeSheet->setCellValue("E$x", $totalRanapSewaAlat + $totalOKSewaAlat);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Kamar Operasi');
		$activeSheet->setCellValue("E$x", $totalOKSewaKamar);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Obat Narcose');
		$activeSheet->setCellValue("E$x", $totalOKObatNarcose);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Obat Dikamar Operasi');
		$activeSheet->setCellValue("E$x", $totalOKObat);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Alat Kesehatan di Kamar Operasi');
		$activeSheet->setCellValue("E$x", $totalOKAlkes);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Ass. Dikamar Operasi');
		$activeSheet->setCellValue("E$x", $totalOKJasaAsisten);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Obat di Perawatan');
		$activeSheet->setCellValue("E$x", $totalRanapObat + $totalFarmasiObat + $totalFarmasiReturObat);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Alat Kesehatan di perawatan');
		$activeSheet->setCellValue("E$x", $totalRanapAlkes + $totalFarmasiAlkes + $totalFarmasiReturAlkes);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Radiologi');
		$activeSheet->setCellValue("E$x", $totalRadXray + $totalRadUSG + $totalRadCTScan + $totalRadMRI + $totalRadBMD);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Fisioterapi');
		$activeSheet->setCellValue("E$x", $totalFisio);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Pemeriksaan Laboratorium');
		$activeSheet->setCellValue("E$x", $totalLab);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Pemeriksaan Pathologi Anatomi');
		$activeSheet->setCellValue("E$x", $totalLabPA);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'ECG');
		$activeSheet->setCellValue("E$x", $totalRanapECG);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Tindakan Keperawatan');
		$activeSheet->setCellValue("E$x", $totalRanapFullCare);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$activeSheet->getStyle("B$xStart:F$xEnd")->applyFromArray(
			[
				'borders' => [
					'allborders' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					]
				],
			]
		);

		$activeSheet->getStyle("B$xStart:B$xEnd")->applyFromArray(
			[
				'font' => [
					'bold' => true,
					'size' => 11,
					'name' => 'Calibri'
				]
			]
		);

		$x = $xEnd + 1;
		$xStart = $x;
		$xEnd = $xStart + 5;

		$activeSheet->setCellValue("B$x", 'Lain-lain');

		$activeSheet->setCellValue("C$x", 'Alat Implant / Orthopaedi');
		$activeSheet->setCellValue("E$x", $totalOKImplan);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Alat Bantu');
		$activeSheet->setCellValue("E$x", $totalRanapAlkesBantu + $totalFarmasiAlkesBantu + $totalFarmasiReturAlkesBantu);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Ambulance');
		$activeSheet->setCellValue("E$x", $totalRanapAmbulance);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'PMI');
		$activeSheet->setCellValue("E$x", $totalLabPMI);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Administrasi');
		$activeSheet->setCellValue("E$x", $totalAdmRajal + $totalAdmRanap);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;

		$activeSheet->setCellValue("C$x", 'Lain-lain');
		$activeSheet->setCellValue("E$x", $totalRanapLainLain);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$activeSheet->mergeCells("B$xStart:B$xEnd");
		$activeSheet->getStyle("B$xStart:B$xEnd")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("B$xStart:B$xEnd")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$activeSheet->getStyle("B$xStart:F$xEnd")->applyFromArray(
			[
				'borders' => [
					'allborders' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
				]
			]
		);

		$activeSheet->getStyle("B$xStart:B$xEnd")->applyFromArray(
			[
				'font' => [
					'bold' => true,
					'size' => 11,
					'name' => 'Calibri'
				]
			]
		);

		if (mysqli_more_results($this->db->conn_id)) {
			mysqli_next_result($this->db->conn_id);
		}

		$x = $x + 1;

		$activeSheet->setCellValue("B$x", 'Visite :');
		$activeSheet->mergeCells("B$x:F$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$dataRanapVisite = $this->Trawatinap_verifikasi_model->viewRincianRanapVisiteGlobal($idpendaftaran);
		$jumlahvisite = COUNT($dataRanapVisite);

		$xEndVisite = $x + $jumlahvisite;
		$x = $x + 1;

		$activeSheet->setCellValue("B$x", 'Visite Dokter Spesialis');
		$activeSheet->mergeCells("B$x:B$xEndVisite");
		$activeSheet->getStyle("B$x:B$xEndVisite")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		foreach ($dataRanapVisite as $row) {
			if ($row->statusverifikasi == 1 || $status == 0) {
				$activeSheet->setCellValue("C$x", $row->namadokter);
				$activeSheet->setCellValue("E$x", $row->totalkeseluruhan);
				$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
				$activeSheet->mergeCells("C$x:D$x");
				$activeSheet->mergeCells("E$x:F$x");

				$x = $x + 1;
			}
		}

		$activeSheet->setCellValue("B$x", 'Jasa Dokter Operator :');
		$activeSheet->mergeCells("B$x:F$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$dataDokterOperator = $this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterOpertor($idpendaftaran);
		$jumlahdokter = COUNT($dataDokterOperator);

		$xEndJasaDokterOperator = $x + ($jumlahdokter > 0 ? $jumlahdokter : 1);
		$x = $x + 1;

		$activeSheet->setCellValue("B$x", 'Jasa Dokter Operator');
		$activeSheet->mergeCells("B$x:B$xEndJasaDokterOperator");
		$activeSheet->getStyle("B$x:B$xEndJasaDokterOperator")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		foreach ($dataDokterOperator as $row) {
			$activeSheet->setCellValue("C$x", $row->namadokter);
			$activeSheet->setCellValue("E$x", $row->totalkeseluruhan);
			$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
			$activeSheet->mergeCells("C$x:D$x");
			$activeSheet->mergeCells("E$x:F$x");

			$x = $x + 1;
		}

		$activeSheet->setCellValue("B$x", 'Jasa Dokter Anasthesi :');
		$activeSheet->mergeCells("B$x:F$x");
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$dataDokterAnesthesi = $this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterAnesthesi($idpendaftaran);
		$jumlahdokter = COUNT($dataDokterAnesthesi);

		$xEndJasaDokterAnasthesi = $x + ($jumlahdokter > 0 ? $jumlahdokter : 1);
		$x = $x + 1;

		$activeSheet->setCellValue("B$x", 'Jasa Dokter Anasthesi');
		$activeSheet->mergeCells("B$x:B$xEndJasaDokterAnasthesi");
		$activeSheet->getStyle("B$x:B$xEndJasaDokterAnasthesi")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		foreach ($dataDokterAnesthesi as $row) {
			$activeSheet->setCellValue("C$x", $row->namadokter);
			$activeSheet->setCellValue("E$x", $row->grandtotal);
			$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
			$activeSheet->mergeCells("C$x:D$x");
			$activeSheet->mergeCells("E$x:F$x");

			$x = $x + 1;
		}

		$activeSheet->mergeCells("B$x:F$x");

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'Diagnosa : ');
		$activeSheet->mergeCells("B$x:D$x");
		$activeSheet->getStyle("B$x:D$x")->applyFromArray(
			[
				'borders' => [
					'bottom' => [
						'style' => PHPExcel_Style_Border::BORDER_NONE,
					],
				]
			]
		);

		$activeSheet->setCellValue("E$x", 'Operasi : ');
		$activeSheet->mergeCells("E$x:F$x");
		$activeSheet->getStyle("E$x:F$x")->applyFromArray(
			[
				'borders' => [
					'bottom' => [
						'style' => PHPExcel_Style_Border::BORDER_NONE,
					],
				]
			]
		);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", $diagnosa);
		$activeSheet->mergeCells("B$x:D$x");
		$activeSheet->getStyle("B$x:D$x")->applyFromArray(
			[
				'borders' => [
					'top' => [
						'style' => PHPExcel_Style_Border::BORDER_NONE,
					],
				]
			]
		);

		$activeSheet->setCellValue("F$x", $operasi);
		$activeSheet->mergeCells("E$x:F$x");
		$activeSheet->getStyle("B$x:D$x")->applyFromArray(
			[
				'borders' => [
					'top' => [
						'style' => PHPExcel_Style_Border::BORDER_NONE,
					],
				]
			]
		);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'Catatan : ');
		$activeSheet->getStyle("B$x")->applyFromArray(
			[
				'borders' => [
					'bottom' => [
						'style' => PHPExcel_Style_Border::BORDER_NONE,
					],
				]
			]
		);

		$totalKeseluruhan = ceil(($totalSebelumAdm + $totalAdmRanap) / 100) * 100;
		$activeSheet->setCellValue("C$x", 'TOTAL BIAYA PERAWATAN + DOKTER');
		$activeSheet->setCellValue("E$x", $totalKeseluruhan);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$x = $x + 1;
		$activeSheet->setCellValue("C$x", 'UANG MUKA/TELAH DIBAYAR');
		$activeSheet->setCellValue("E$x", $totaldeposit);
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D$x");
		$activeSheet->mergeCells("E$x:F$x");

		$activeSheet->setCellValue("B$x", $catatan);
		$activeSheet->getStyle("B$x")->applyFromArray(
			[
				'borders' => [
					'top' => [
						'style' => PHPExcel_Style_Border::BORDER_NONE,
					],
				]
			]
		);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'Paraf');
		$activeSheet->getStyle("B$x:F$x")->applyFromArray(
			[
				'borders' => [
					'bottom' => [
						'style' => PHPExcel_Style_Border::BORDER_NONE,
					],
				]
			]
		);

		$activeSheet->setCellValue("C$x", 'KURANG/LEBIH/JUMLAH');
		$activeSheet->setCellValue("E$x", ($totalKeseluruhan >= $totaldeposit ? ($totalKeseluruhan - $totaldeposit) : ($totaldeposit - $totalKeseluruhan) * -1));
		$activeSheet->getStyle("E$x")->getNumberFormat()->setFormatCode('_("RP"* #,##0_);_("RP"* \(#,##0\);_("RP"* "-"??_);_(@_)');
		$activeSheet->mergeCells("C$x:D" . ($x + 4));
		$activeSheet->mergeCells("E$x:F" . ($x + 4));
		$activeSheet->getStyle("C$x:D" . ($x + 4))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("C$x:D" . ($x + 4))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$activeSheet->mergeCells("E$x:F" . ($x + 4));
		$activeSheet->getStyle("E$x:F" . ($x + 4))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("E$x:F" . ($x + 4))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$x = $x + 1;
		$activeSheet->setCellValue("B$x", 'Administrator');
		$activeSheet->mergeCells("B$x:B" . ($x + 3));
		$activeSheet->getStyle("B$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("B$x:D$x")->applyFromArray(
			[
				'borders' => [
					'top' => [
						'style' => PHPExcel_Style_Border::BORDER_NONE,
					],
				]
			]
		);

		$x = $x + 4;
		$activeSheet->setCellValue("B$x", 'NOTA PERINCIAN INI BUKAN MERUPAKAN BUKTI PEMBAYARAN');
		$activeSheet->mergeCells("B$x:F$x");
		$activeSheet->getStyle("B$x:F$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("B$x")->applyFromArray(['font' => ['bold' => true]]);

		$activeSheet->getStyle("B9:F$x")->applyFromArray(
			[
				'borders' => [
					'allborders' => [
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					],
				]
			]
		);

		$x = $x + 2;

		// Set Info Print
		$activeSheet->setCellValue("B$x", 'Tanggal Cetak : ' . date('d/m/Y - h:i:s') . ', User Pembayaran : ' . $user_input_pembayaran . ', User Cetak : ' . $this->session->userdata('user_name') . ', Cetak Ke : 1');
		$activeSheet->getStyle("B$x")->getFont()->setSize(11);
		$activeSheet->mergeCells("B$x:F$x");

		// Set Auto Width Column
		$activeSheet->getColumnDimension('B')->setWidth(30);
		$activeSheet->getColumnDimension('C')->setAutoSize(true);
		$activeSheet->getColumnDimension('D')->setAutoSize(true);
		$activeSheet->getColumnDimension('E')->setWidth(20);
		$activeSheet->getColumnDimension('F')->setWidth(38);

		header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Rincian Global.xls"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		$objWriter->save('php://output');
	}

	public function filter()
	{
		$data = [
			'tabActive' => 1,
			'nomedrec' => $this->input->post('nomedrec'),
			'namapasien' => $this->input->post('namapasien'),
			'tanggalkeluar' => $this->input->post('tanggalkeluar'),
			'iddokterperujuk' => $this->input->post('iddokterperujuk'),
			'iddokterpenanggungjawab' => $this->input->post('iddokterpenanggungjawab'),
			'idruangan' => $this->input->post('idruangan'),
			'idkelas' => $this->input->post('idkelas'),
			'idbed' => $this->input->post('idbed'),
			'status' => $this->input->post('status'),
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Tindakan Rawat Inap';
		$data['content'] = 'Trawatinap_tindakan/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Tindakan Rawat Inap', '#'],
			['List', 'Trawatinap_tindakan']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filterCheckout()
	{
		$data = [
			'tabActive' => 2,
			'nomedrec' => $this->input->post('nomedrec'),
			'namapasien' => $this->input->post('namapasien'),
			'tanggalkeluar' => $this->input->post('tanggalkeluar'),
			'iddokterperujuk' => $this->input->post('iddokterperujuk'),
			'iddokterpenanggungjawab' => $this->input->post('iddokterpenanggungjawab'),
			'idruangan' => $this->input->post('idruangan'),
			'idkelas' => $this->input->post('idkelas'),
			'idbed' => $this->input->post('idbed'),
			'tanggaldari_transaksi' => $this->input->post('tanggaldari_transaksi'),
			'tanggalsampai_transaksi' => $this->input->post('tanggalsampai_transaksi'),
			'tanggaldari_checkout' => $this->input->post('tanggaldari_checkout'),
			'tanggalsampai_checkout' => $this->input->post('tanggalsampai_checkout'),
			'status' => $this->input->post('status'),
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Tindakan Rawat Inap';
		$data['content'] = 'Trawatinap_tindakan/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Tindakan Rawat Inap', '#'],
			['List', 'Trawatinap_tindakan']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function statusCheckout($idpendaftaran)
	{
		$row = $this->Trawatinap_tindakan_model->getDataRawatInap($idpendaftaran);
		$data = [
			'status' => $row->statuscheckout
		];
		$this->output->set_output(json_encode($data));
	}

	public function lockKasir($idpendaftaran)
	{
		$this->db->where('id', $idpendaftaran);
		$this->db->set('statuskasir', 1);
		if ($this->db->update('trawatinap_pendaftaran')) {
			return true;
		} else {
			return false;
		}
	}

	public function unlockKasir($idpendaftaran)
	{
		$this->db->where('id', $idpendaftaran);
		$this->db->set('statuskasir', 0);
		if ($this->db->update('trawatinap_pendaftaran')) {
			return true;
		} else {
			return false;
		}
	}

	public function prosesValidasi()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		$ruangranap = json_decode($this->input->post('ruangranap'));
		$admranap = json_decode($this->input->post('admranap'));

		$this->db->where('id', $idpendaftaran);
		$this->db->set('statusvalidasi', 1);
		if ($this->db->update('trawatinap_pendaftaran')) {
			foreach ($ruangranap as $row) {
				$data = [];
				$data['idrawatinap'] = $row[9];
				$data['idtarif'] = $row[10];
				$data['namatarif'] = $row[11];
				$data['idruangan'] = $row[12];
				$data['idkelas'] = $row[13];
				$data['tanggaldari'] = $row[14];
				$data['tanggalsampai'] = $row[15];
				$data['jumlahhari'] = RemoveComma($row[16]);
				$data['jasasarana'] = RemoveComma($row[17]);
				$data['jasapelayanan'] = RemoveComma($row[18]);
				$data['bhp'] = RemoveComma($row[19]);
				$data['biayaperawatan'] = RemoveComma($row[20]);
				$data['total'] = RemoveComma($row[21]);
				$data['totalkeseluruhan'] = RemoveComma($row[7]);

				$this->db->insert('trawatinap_ruangan', $data);
			}

			foreach ($admranap as $row) {
				$data = [];
				$data['idpendaftaran'] = $row[7];
				$data['idadministrasi'] = $row[8];
				$data['namatarif'] = $row[9];
				$data['jasasarana'] = RemoveComma($row[10]);
				$data['jasapelayanan'] = RemoveComma($row[11]);
				$data['bhp'] = RemoveComma($row[12]);
				$data['biayaperawatan'] = RemoveComma($row[13]);
				$data['total'] = RemoveComma($row[14]);
				$data['mintarif'] = RemoveComma($row[15]);
				$data['maxtarif'] = RemoveComma($row[16]);
				$data['persentasetarif'] = RemoveComma($row[17]);
				$data['tarif'] = RemoveComma($row[18]);
				$data['tarifsetelahdiskon'] = RemoveComma($row[18]);

				$this->db->insert('trawatinap_administrasi', $data);
			}

			return true;
		} else {
			return false;
		}
	}
	public function prosesBatalValidasi()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		$ruangranap = json_decode($this->input->post('ruangranap'));
		$admranap = json_decode($this->input->post('admranap'));

		$this->db->where('id', $idpendaftaran);
		$this->db->set('statusvalidasi', 0);
		if ($this->db->update('trawatinap_pendaftaran')) {
			$this->db->where('idrawatinap',$idpendaftaran);
			$this->db->delete('trawatinap_ruangan');
			
			$this->db->where('idrawatinap',$idpendaftaran);
			$this->db->delete('trawatinap_administrasi');
			
			return true;
		} else {
			return false;
		}
	}

	public function prosesTransaksi()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');

		$this->db->where('id', $idpendaftaran);
		$this->db->set('statuskasir', 1);
		$this->db->set('statustransaksi', 1);
		if ($this->db->update('trawatinap_pendaftaran')) {
			return true;
		} else {
			return false;
		}
	}
	public function updateProsesKwitansi()
	{
		$idpendaftaran = $this->input->post('id');

		$this->db->where('id', $idpendaftaran);
		$this->db->set('status_proses_kwitansi', 1);
		if ($this->db->update('trawatinap_pendaftaran')) {
			return true;
		} else {
			return false;
		}
	}

	public function prosesBatalTransaksi()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		$alasanbatal = $this->input->post('alasanbatal');

		$data = [
			'statusbatal' => 1,
			'tanggalbatal' => date('Y-m-d H:i:s'),
			'alasanbatal' => $alasanbatal,
			'iduser_batal' => $this->session->userdata('user_id'),
		];

		$this->db->where('idtindakan', $idpendaftaran);
		$this->db->where('statusbatal', 0);
		if ($this->db->update('trawatinap_tindakan_pembayaran', $data)) {
			$data = [
				'statuskasir' => 1,
				'statustransaksi' => 0,
				'statuspembayaran' => 0,
			];
			$this->db->where('id', $idpendaftaran);
			$this->db->update('trawatinap_pendaftaran', $data);
			return true;
		} else {
			return false;
		}
	}
	public function prosesBatalTransaksiPembayaran()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		// $alasanbatal = $this->input->post('alasanbatal');

			$data = [
				'statuskasir' => 1,
				'statustransaksi' => 0,
				'statuspembayaran' => 0,
			];
			$this->db->where('id', $idpendaftaran);
			$this->db->update('trawatinap_pendaftaran', $data);
			return true;
		
	}

	public function prosesCetakKwitansi()
	{
		$idrawatinap = $this->input->post('idrawatinap');
		$tipe_proses = $this->input->post('tipe_proses');
		$sudahterimadari = $this->input->post('sudahterimadari');
		$deskripsi = $this->input->post('deskripsi');

		$this->idrawatinap = $idrawatinap;
		$this->tipe_proses = $tipe_proses;
		$this->tanggal = date('Y-m-d H:i:s');
		$this->sudahterimadari = $sudahterimadari;
		$this->deskripsi = $deskripsi;
		$this->iduser_input = $this->session->userdata('user_id');

		if ($this->db->insert('trawatinap_kwitansi', $this)) {
			echo $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function prosesCetakKwitansiImplant()
	{
		$idrawatinap = $this->input->post('idrawatinap');
		$sudahterimadari = $this->input->post('sudahterimadari');
		$deskripsi = $this->input->post('deskripsi');

		$this->idrawatinap = $idrawatinap;
		$this->tanggal = date('Y-m-d H:i:s');
		$this->sudahterimadari = $sudahterimadari;
		$this->deskripsi = $deskripsi;
		$this->iduser_input = $this->session->userdata('user_id');

		if ($this->db->insert('trawatinap_kwitansi_implant', $this)) {
			echo $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function prosesCetakFakturImplant()
	{
		$this->idrawatinap = $this->input->post('idrawatinap');
		$this->nomedrec = $this->input->post('nomedrec');
		$this->namapasien = $this->input->post('namapasien');
		$this->namadokter = $this->input->post('namadokter');
		$this->diagnosa = $this->input->post('diagnosa');
		$this->tanggal_operasi = YMDFormat($this->input->post('tanggaloperasi'));
		$this->operasi = $this->input->post('operasi');
		$this->created_at = date("Y-m-d H:i:s");
		$this->created_by = $this->session->userdata('user_id');

		if ($this->db->insert('trawatinap_faktur_implant', $this)) {
			$idfaktur = $this->db->insert_id();
			$iddistributor = $this->input->post('iddistributor');
			foreach ($iddistributor as $key => $value) {
				$data = [
					'idfaktur' => $idfaktur,
					'idrawatinap' => $this->input->post('idrawatinap'),
					'iddistributor' => $value,
				];
				$this->db->insert('trawatinap_faktur_implant_distributor', $data);
			}

			echo $idfaktur;
		} else {
			return false;
		}
	}

	public function getDataRawatInap($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getDataRawatInap($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	public function getDataPoliTindakan($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getDataPoliTindakan($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	// Start Checkout
	public function getDataCheckout($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getDataCheckout($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	public function saveCheckout()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');

		$tanggalkeluar = $this->input->post('tanggalkeluar');
		$tanggalkontrol = $this->input->post('tanggalkontrol');
		$sebabluar = $this->input->post('sebabluar');
		$keadaanpasienkeluar = $this->input->post('keadaanpasienkeluar');
		$kondisipasienkeluar = $this->input->post('kondisipasienkeluar');
		$kondisipasiensaatini = $this->input->post('kondisipasiensaatini');
		$carapasienkeluar = $this->input->post('carapasienkeluar');
		$diagnosa = $this->input->post('diagnosa');
		$checkoutCaraPasienKeluarRsKlinik = $this->input->post('checkoutCaraPasienKeluarRsKlinik');
		$checkoutCaraPasienKeluarAlasanBatal = $this->input->post('checkoutCaraPasienKeluarAlasanBatal');
		$checkoutCaraPasienKeluarKeterangan = $this->input->post('checkoutCaraPasienKeluarKeterangan');

		$this->db->where('id', $idpendaftaran);
		$this->db->set('statuscheckout', 1);
		if ($this->db->update('trawatinap_pendaftaran')) {
			$this->db->where('idrawatinap', $idpendaftaran);
			if ($this->db->delete('trawatinap_checkout')) {
				$data = [
					'idrawatinap' => $idpendaftaran,
					'tanggalkeluar' => YMDFormat($tanggalkeluar),
					'tanggalkontrol' => YMDFormat($tanggalkontrol),
					'sebabluar' => $sebabluar,
					'keadaanpasienkeluar' => $keadaanpasienkeluar,
					'kondisipasienkeluar' => $kondisipasienkeluar,
					'kondisipasiensaatini' => $kondisipasiensaatini,
					'carapasienkeluar' => $carapasienkeluar,
					'diagnosa' => $diagnosa,
					'idrumahsakit' => $checkoutCaraPasienKeluarRsKlinik,
					'idalasanbatal' => $checkoutCaraPasienKeluarAlasanBatal,
					'keterangancarapasienkeluar' => $checkoutCaraPasienKeluarKeterangan,
					'iduser' => $this->session->userdata('user_id'),
				];
				if ($this->db->insert('trawatinap_checkout', $data)) {
					$this->db->where('idpendaftaran', $idpendaftaran);
					if ($this->db->delete('trawatinap_diagnosa')) {
						$kelompokDiagnosa = $this->input->post('kelompokdiagnosa');
						foreach ($kelompokDiagnosa as $idkelompok) {
							$data = [];
							$data['idpendaftaran'] = $idpendaftaran;
							$data['idkelompok_diagnosa'] = $idkelompok;

							$this->db->insert('trawatinap_diagnosa', $data);
						}
					}

					return true;
				} else {
					return false;
				}
			}
		} else {
			return false;
		}
	}

	// Start Dokter Penanggung Jawab
	public function getDokterPJ()
	{
		$query = $this->Trawatinap_tindakan_model->getDokterPJ();
		$this->output->set_output(json_encode($query));
	}

	public function getCurrentDokterPJ($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getCurrentDokterPJ($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	public function getHistoryDokterPJ($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getHistoryDokterPJ($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	public function saveDokterPJ()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		$iddokterpj = $this->input->post('iddokterpj');
		$tanggalperubahan = date('Y-m-d H:i:s');

		$row = $this->Trawatinap_tindakan_model->getDataRawatInap($idpendaftaran);
		$tanggaldari = ($row->tanggalperubahandokter == '' ? $row->tanggalpendaftaran : $row->tanggalperubahandokter);

		$data = [
			'idrawatinap' => $idpendaftaran,
			'tanggaldari' => YMDTimeFormat($tanggaldari),
			'tanggalsampai' => date('Y-m-d H:i:s'),
			'iddokter' => $row->iddokterpenanggungjawab,
			'created_by' => $this->session->userdata('user_id'),
			'created_date' => date('Y-m-d H:i:s')
		];

		if ($this->db->insert('trawatinap_history_dokter', $data)) {
			if ($this->Trawatinap_tindakan_model->updateDokterPJ($idpendaftaran, $tanggalperubahan, $iddokterpj)) {
				return true;
			} else {
				return false;
			}
		}
	}

	// EOF Dokter Penanggung Jawab

	// Start Pindah Bed
	public function getPegawaiRawatInap()
	{
		$query = $this->Trawatinap_tindakan_model->getPegawaiRawatInap();
		$this->output->set_output(json_encode($query));
	}

	public function getRuangan()
	{
		$query = $this->Trawatinap_tindakan_model->getRuangan();
		$this->output->set_output(json_encode($query));
	}

	public function getKelas()
	{
		$query = $this->Trawatinap_tindakan_model->getKelas();
		$this->output->set_output(json_encode($query));
	}

	public function getBed($idruangan, $idkelas)
	{
		$query = $this->Trawatinap_tindakan_model->getBed($idruangan, $idkelas);
		$this->output->set_output(json_encode($query));
	}

	public function getHistoryPindahBed($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getHistoryPindahBed($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	public function savePindahBed()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		$tanggal = $this->input->post('tanggal');
		$idpegawai = $this->input->post('idpegawai');
		$idruangan = $this->input->post('idruangan');
		$idkelas = $this->input->post('idkelas');
		$idbed = $this->input->post('idbed');

		$row = $this->Trawatinap_tindakan_model->getDataRawatInap($idpendaftaran);
		$oldtanggal = ($row->tanggalperubahanbed != '' ? $row->tanggalperubahanbed : $row->tanggaldaftar);
		$oldidruangan = $row->idruangan;
		$oldidkelas = $row->idkelas;
		$oldidbed = $row->idbed;

		if ($this->Trawatinap_tindakan_model->updateBed($idpendaftaran, $tanggal, $idruangan, $idkelas, $idbed)) {
			$data = [];
			$data['idrawatinap'] = $idpendaftaran;
			$data['tanggaldari'] = YMDFormat($oldtanggal);
			$data['tanggalsampai'] = YMDFormat($tanggal);
			$data['idpegawai'] = $idpegawai;
			$data['idruangan'] = $oldidruangan;
			$data['idkelas'] = $oldidkelas;
			$data['idbed'] = $oldidbed;
			$data['status'] = 1;
			$data['created_by'] = $this->session->userdata('user_id');
			$data['created_date'] = date('Y-m-d H:i:s');
			if ($this->db->insert('trawatinap_history_bed', $data)) {
				return true;
			} else {
				return false;
			}
		}
	}

	// EOF Pindah Bed

	public function saveCatatan()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		$catatan = $this->input->post('catatan');

		$this->db->where('id', $idpendaftaran);
		$this->db->set('catatan', $catatan);
		if ($this->db->update('trawatinap_pendaftaran')) {
			return true;
		} else {
			return false;
		}
	}
	public function saveCatatanNew()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		$catatan = $this->input->post('catatan');
		$catatan_internal = $this->input->post('catatan_internal');
		// print_r($catatan_internal);exit;
		$this->db->where('id', $idpendaftaran);
		$this->db->set('catatan', $catatan);
		$this->db->set('catatan_internal', $catatan_internal);
		if ($this->db->update('trawatinap_pendaftaran')) {
			return true;
		} else {
			return false;
		}
	}
	// Start Estimasi Biaya
	public function getHistoryEstimasiBiaya($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getHistoryEstimasiBiaya($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	public function getDataEstimasiBiaya($idestimasi)
	{
		$query = $this->Trawatinap_tindakan_model->getDataEstimasiBiaya($idestimasi);
		$this->output->set_output(json_encode($query));
	}

	public function getDataEstimasiBiayaImplant($idestimasi)
	{
		$query = $this->Trawatinap_tindakan_model->getDataEstimasiBiayaImplant($idestimasi);
		$this->output->set_output(json_encode($query));
	}

	// EOF Estimasi Biaya

	// Start Aksi Tindakan
	public function getHistoryTindakan($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getHistoryTindakan($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	// Alat Kesehatan
	public function getHistoryAlkes($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getHistoryAlkes($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	public function getListAlkesUnit($idpendaftaran, $idunitpelayanan)
	{
		$idkelompokpasien = get_by_field('id', $idpendaftaran, 'trawatinap_pendaftaran')->idkelompokpasien;

		$this->select = ['mgudang_stok.*',
			'mdata_alkes.id AS idalkes',
			'mdata_alkes.kode AS kodealkes',
			'mdata_alkes.nama AS namaalkes',
			'mdata_alkes.stokreorder',
			'mdata_alkes.stokminimum',
			'msatuan.nama AS namasatuan',
			'mdata_alkes.hargadasar',
			'(CASE
    			#UMUM
    			WHEN "' . $idkelompokpasien . '" = 0 THEN
  			       mdata_alkes.marginumum
    			#PERUSAHAAN ASURANSI
    			WHEN "' . $idkelompokpasien . '" = 1 THEN
  			     mdata_alkes.marginasuransi
    			#JASA RAHARJA
    			WHEN "' . $idkelompokpasien . '" = 3 THEN
  			     mdata_alkes.marginjasaraharja
    			#BPJS KESEHATAN
    			WHEN "' . $idkelompokpasien . '" = 4 THEN
  			     mdata_alkes.marginbpjskesehatan
    			#BPJS KETENAGAKERJAAN
    			WHEN "' . $idkelompokpasien . '" = 5 THEN
  			     mdata_alkes.marginbpjstenagakerja
  			  END
  			) as margin'];

		$this->from = 'mgudang_stok';

		$this->join = [
			['mdata_alkes', 'mdata_alkes.id = mgudang_stok.idbarang AND mgudang_stok.idtipe = 1', ''],
			['msatuan', 'msatuan.id = mdata_alkes.idsatuankecil', 'LEFT'],
		];

		$this->where = [
			'mdata_alkes.status' => 1,
			'mgudang_stok.idunitpelayanan' => $idunitpelayanan,
		];

		$this->order = [
			'mdata_alkes.nama' => 'ASC'
		];

		$this->group = [];

		$this->column_search = ['mdata_alkes.kode', 'mdata_alkes.nama'];
		$this->column_order = ['mdata_alkes.kode', 'mdata_alkes.nama'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			$hargajual = ($r->hargadasar + ($r->hargadasar * ($r->margin / 100)));

			$row[] = "<a href='#' class='selectAlkesTable' data-idalkes='" . $r->idalkes . "' data-hargadasar='" . $r->hargadasar . "' data-margin='" . $r->margin . "' data-dismiss='modal'>" . $r->kodealkes . '';
			$row[] = $r->namaalkes;
			$row[] = $r->namasatuan;
			$row[] = number_format(ceiling($hargajual, 100));
			$row[] = WarningStok($r->stok, $r->stokreorder, $r->stokminimum);
			$aksi = '<div class="btn-group">';
			$aksi .= '<a target="_blank" href="' . site_url() . 'lgudang_stok/kartustok/' . $r->idtipe . '/' . $r->idbarang . '/' . $r->idunitpelayanan . '" class="btn btn-xs btn-danger ks" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';
			$aksi .= '</div>';
			$row[] = $aksi;
			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function saveDataAlkes()
	{
		if ($this->input->post('idrow') != '') {
			$this->db->set('idrawatinap', $this->input->post('idpendaftaran'));
			$this->db->set('tanggal', YMDFormat($this->input->post('tanggal')));
			$this->db->set('idunit', $this->input->post('idunitpelayanan'));
			$this->db->set('idalkes', $this->input->post('idalkes'));
			$this->db->set('hargadasar', RemoveComma($this->input->post('hargadasar')));
			$this->db->set('margin', RemoveComma($this->input->post('margin')));
			$this->db->set('hargajual', RemoveComma($this->input->post('hargajual')));
			$this->db->set('kuantitas', RemoveComma($this->input->post('kuantitas')));
			$this->db->set('totalkeseluruhan', RemoveComma($this->input->post('totalkeseluruhan')));

			$this->db->set('modified_by', $this->session->userdata('user_id'));
			$this->db->set('modified_date', date('Y-m-d H:i:s'));
			$this->db->set('version', 'version+1', false);

			$this->db->where('id', $this->input->post('idrow'));
			$this->db->update('trawatinap_alkes');
		} else {
			$data = [];
			$data['idrawatinap'] = $this->input->post('idpendaftaran');
			$data['tanggal'] = YMDFormat($this->input->post('tanggal'));
			$data['idunit'] = $this->input->post('idunitpelayanan');
			$data['idalkes'] = $this->input->post('idalkes');
			$data['hargadasar'] = RemoveComma($this->input->post('hargadasar'));
			$data['margin'] = RemoveComma($this->input->post('margin'));
			$data['hargajual'] = RemoveComma($this->input->post('hargajual'));
			$data['kuantitas'] = RemoveComma($this->input->post('kuantitas'));
			$data['totalkeseluruhan'] = RemoveComma($this->input->post('totalkeseluruhan'));

			$data['created_by'] = $this->session->userdata('user_id');
			$data['created_date'] = date('Y-m-d H:i:s');

			$this->db->insert('trawatinap_alkes', $data);
		}
	}

	public function removeDataAlkes()
	{
		$this->db->where('id', $this->input->post('idrow'));
		if ($this->db->delete('trawatinap_alkes')) {
			return true;
		} else {
			return false;
		}
	}

	// Obat
	public function getHistoryObat($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getHistoryObat($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	public function getListObatUnit($idpendaftaran, $idunitpelayanan)
	{
		$idkelompokpasien = get_by_field('id', $idpendaftaran, 'trawatinap_pendaftaran')->idkelompokpasien;

		$this->select = ['mgudang_stok.*',
			'mdata_obat.id AS idobat',
			'mdata_obat.kode AS kodeobat',
			'mdata_obat.nama AS namaobat',
			'mdata_obat.stokreorder',
			'mdata_obat.stokminimum',
			'msatuan.nama AS namasatuan',
			'mdata_obat.hargadasar',
			'(CASE
    			#UMUM
    			WHEN "' . $idkelompokpasien . '" = 0 THEN
  			       mdata_obat.marginumum
    			#PERUSAHAAN ASURANSI
    			WHEN "' . $idkelompokpasien . '" = 1 THEN
  			     mdata_obat.marginasuransi
    			#JASA RAHARJA
    			WHEN "' . $idkelompokpasien . '" = 3 THEN
  			     mdata_obat.marginjasaraharja
    			#BPJS KESEHATAN
    			WHEN "' . $idkelompokpasien . '" = 4 THEN
  			     mdata_obat.marginbpjskesehatan
    			#BPJS KETENAGAKERJAAN
    			WHEN "' . $idkelompokpasien . '" = 5 THEN
  			     mdata_obat.marginbpjstenagakerja
  			  END
  			) as margin'];

		$this->from = 'mgudang_stok';

		$this->join = [
			['mdata_obat', 'mdata_obat.id = mgudang_stok.idbarang AND mgudang_stok.idtipe = 3', ''],
			['msatuan', 'msatuan.id = mdata_obat.idsatuankecil', 'LEFT'],
		];

		$this->where = [
			'mdata_obat.status' => 1,
			'mgudang_stok.idunitpelayanan' => $idunitpelayanan,
		];

		$this->order = [
			'mdata_obat.nama' => 'ASC'
		];
		$this->group = [];

		$this->column_search = ['mdata_obat.kode', 'mdata_obat.nama'];
		$this->column_order = ['mdata_obat.kode', 'mdata_obat.nama'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			$hargajual = ($r->hargadasar + ($r->hargadasar * ($r->margin / 100)));

			$row[] = "<a href='#' class='selectObatTable' data-idobat='" . $r->idobat . "' data-hargadasar='" . $r->hargadasar . "' data-margin='" . $r->margin . "' data-dismiss='modal'>" . $r->kodeobat . '';
			$row[] = $r->namaobat;
			$row[] = $r->namasatuan;
			$row[] = number_format(ceiling($hargajual, 100));
			$row[] = WarningStok($r->stok, $r->stokreorder, $r->stokminimum);
			$aksi = '<div class="btn-group">';
			$aksi .= '<a target="_blank" href="' . site_url() . 'lgudang_stok/kartustok/' . $r->idtipe . '/' . $r->idbarang . '/' . $r->idunitpelayanan . '" class="btn btn-xs btn-danger ks" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';
			$aksi .= '</div>';
			$row[] = $aksi;
			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function saveDataObat()
	{
		if ($this->input->post('idrow') != '') {
			$this->db->set('idrawatinap', $this->input->post('idpendaftaran'));
			$this->db->set('tanggal', YMDFormat($this->input->post('tanggal')));
			$this->db->set('idunit', $this->input->post('idunitpelayanan'));
			$this->db->set('idobat', $this->input->post('idobat'));
			$this->db->set('hargadasar', RemoveComma($this->input->post('hargadasar')));
			$this->db->set('margin', RemoveComma($this->input->post('margin')));
			$this->db->set('hargajual', RemoveComma($this->input->post('hargajual')));
			$this->db->set('kuantitas', RemoveComma($this->input->post('kuantitas')));
			$this->db->set('totalkeseluruhan', RemoveComma($this->input->post('totalkeseluruhan')));

			$this->db->set('modified_by', $this->session->userdata('user_id'));
			$this->db->set('modified_date', date('Y-m-d H:i:s'));
			$this->db->set('version', 'version+1', false);

			$this->db->where('id', $this->input->post('idrow'));
			$this->db->update('trawatinap_obat');
		} else {
			$data = [];
			$data['idrawatinap'] = $this->input->post('idpendaftaran');
			$data['tanggal'] = YMDFormat($this->input->post('tanggal'));
			$data['idunit'] = $this->input->post('idunitpelayanan');
			$data['idobat'] = $this->input->post('idobat');
			$data['hargadasar'] = RemoveComma($this->input->post('hargadasar'));
			$data['margin'] = RemoveComma($this->input->post('margin'));
			$data['hargajual'] = RemoveComma($this->input->post('hargajual'));
			$data['kuantitas'] = RemoveComma($this->input->post('kuantitas'));
			$data['totalkeseluruhan'] = RemoveComma($this->input->post('totalkeseluruhan'));

			$data['created_by'] = $this->session->userdata('user_id');
			$data['created_date'] = date('Y-m-d H:i:s');

			$this->db->insert('trawatinap_obat', $data);
		}
	}

	public function removeDataObat()
	{
		$this->db->where('id', $this->input->post('idrow'));
		if ($this->db->delete('trawatinap_obat')) {
			return true;
		} else {
			return false;
		}
	}

	// Tindakan
	public function getTindakanRawatInap()
	{
		$search = $this->input->post('search');
		$query = $this->Trawatinap_tindakan_model->getTindakanRawatInap($search);
		$this->output->set_output(json_encode($query));
	}

	public function getTarifKelompokPasienRanapFullcare($idruangan, $idkelompokpasien)
	{
		// Get Path Full Care
		$this->db->select('mtarif_rawatinap.path');
		if ($idruangan == 1) {
			// PERAWATAN
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_fullcare1 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 1');
		} elseif ($idruangan == 2) {
			// HCU
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_fullcare2 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 2');
		} elseif ($idruangan == 3) {
			// ICU
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_fullcare3 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 3');
		} elseif ($idruangan == 4) {
			// ISOLASI
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_fullcare4 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 4');
		} elseif ($idruangan == 6) {
			// OK 1
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_fullcare5 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 6');
		} elseif ($idruangan == 7) {
			// OK 2
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_fullcare6 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 7');
		}

		$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
		$query = $this->db->get('mtarif_rawatinap');
		$row = $query->row();
		$rowpath = '';
		if ($row) {
			$rowpath = $row->path;
		}

		return $rowpath;
	}

	public function getTarifKelompokPasienRanapECG($idruangan, $idkelompokpasien)
	{
		// Get Path ECG
		$this->db->select('mtarif_rawatinap.path');
		if ($idruangan == 1) {
			// PERAWATAN
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_ecg1 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 1');
		} elseif ($idruangan == 2) {
			// HCU
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_ecg2 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 2');
		} elseif ($idruangan == 3) {
			// ICU
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_ecg3 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 3');
		} elseif ($idruangan == 4) {
			// ISOLASI
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_ecg4 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 4');
		} elseif ($idruangan == 6) {
			// OK 1
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_ecg5 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 6');
		} elseif ($idruangan == 7) {
			// OK 2
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_ecg6 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 7');
		}

		$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
		$query = $this->db->get('mtarif_rawatinap');
		$row = $query->row();
		$rowpath = '';
		if ($row) {
			$rowpath = $row->path;
		}

		return $rowpath;
	}

	public function getTarifKelompokPasienRanapSewaAlat($idruangan, $idkelompokpasien)
	{
		// Get Path Sewa Alat
		$this->db->select('mtarif_rawatinap.path');
		if ($idruangan == 1) {
			// PERAWATAN
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_sewaalat1 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 1');
		} elseif ($idruangan == 2) {
			// HCU
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_sewaalat2 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 2');
		} elseif ($idruangan == 3) {
			// ICU
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_sewaalat3 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 3');
		} elseif ($idruangan == 4) {
			// ISOLASI
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_sewaalat4 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 4');
		} elseif ($idruangan == 6) {
			// OK 1
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_sewaalat5 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 6');
		} elseif ($idruangan == 7) {
			// OK 2
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_sewaalat6 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 7');
		}

		$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
		$query = $this->db->get('mtarif_rawatinap');
		$row = $query->row();
		$rowpath = '';
		if ($row) {
			$rowpath = $row->path;
		}

		return $rowpath;
	}

	public function getTarifKelompokPasienRanapSewaAmbulance($idruangan, $idkelompokpasien)
	{
		// Get Path Ambulance
		$this->db->select('mtarif_rawatinap.path');
		if ($idruangan == 1) {
			// PERAWATAN
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_ambulance1 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 1');
		} elseif ($idruangan == 2) {
			// HCU
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_ambulance2 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 2');
		} elseif ($idruangan == 3) {
			// ICU
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_ambulance3 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 3');
		} elseif ($idruangan == 4) {
			// ISOLASI
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_ambulance4 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 4');
		} elseif ($idruangan == 6) {
			// OK 1
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_ambulance5 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 6');
		} elseif ($idruangan == 7) {
			// OK 2
			$this->db->join('mpasien_kelompok', 'mpasien_kelompok.trawatinap_ambulance6 = mtarif_rawatinap.id AND mtarif_rawatinap.idruangan = 7');
		}

		$this->db->where('mpasien_kelompok.id', $idkelompokpasien);
		$query = $this->db->get('mtarif_rawatinap');
		$row = $query->row();
		$rowpath = '';
		if ($row) {
			$rowpath = $row->path;
		}

		return $rowpath;
	}

	public function getListTindakanRawatInap($idpendaftaran, $idtipe)
	{
		$dataSelected = get_by_field('id', $idpendaftaran, 'trawatinap_pendaftaran');

		$pathFullcare = $this->getTarifKelompokPasienRanapFullcare($dataSelected->idruangan, $dataSelected->idkelompokpasien);
		$pathECG = $this->getTarifKelompokPasienRanapECG($dataSelected->idruangan, $dataSelected->idkelompokpasien);
		$pathSewaAlat = $this->getTarifKelompokPasienRanapSewaAlat($dataSelected->idruangan, $dataSelected->idkelompokpasien);
		$pathSewaAmbulance = $this->getTarifKelompokPasienRanapSewaAmbulance($dataSelected->idruangan, $dataSelected->idkelompokpasien);

		$this->select = [
			'mtarif_rawatinap.*',
			'mtarif_rawatinap_detail.idtarif',
			'mtarif_rawatinap_detail.kelas',
			'mtarif_rawatinap_detail.jasasarana',
			'mtarif_rawatinap_detail.jasapelayanan',
			'mtarif_rawatinap_detail.bhp',
			'mtarif_rawatinap_detail.biayaperawatan',
			'mtarif_rawatinap_detail.total'
		];

		$this->from = 'mtarif_rawatinap_detail';

		$this->join = [
			['mtarif_rawatinap', '((mtarif_rawatinap.id = mtarif_rawatinap_detail.idtarif AND mtarif_rawatinap.idruangan = "' . $dataSelected->idruangan . '" AND mtarif_rawatinap_detail.kelas = "' . $dataSelected->idkelas . '") OR (mtarif_rawatinap.idruangan = "' . $dataSelected->idruangan . '" AND mtarif_rawatinap.level = 0)) AND (mtarif_rawatinap.headerpath LIKE "' . $pathFullcare . '%" OR mtarif_rawatinap.headerpath LIKE "' . $pathECG . '%" OR mtarif_rawatinap.headerpath LIKE "' . $pathSewaAlat . '%" OR mtarif_rawatinap.headerpath LIKE "' . $pathSewaAmbulance . '%")', '']
		];

		$this->where = [
			'mtarif_rawatinap.idtipe' => $idtipe,
			'mtarif_rawatinap.status' => 1
		];
		// $this->where = array('mtarif_rawatinap.idruangan' => $dataSelected->idruangan);
		// $this->where = array('mtarif_rawatinap_detail.kelas' => $dataSelected->idkelas);

		$this->order = [
			'mtarif_rawatinap.path' => 'ASC'
		];
		$this->group = ['mtarif_rawatinap.id'];

		$this->column_search = ['mtarif_rawatinap.nama'];
		$this->column_order = ['mtarif_rawatinap.nama'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			if ($r->idkelompok == 1) {
				$namatarif = TreeView($r->level, $r->nama);
			} else {
				$namatarif = "<a href='#' class='selectTindakanTable' data-idtindakan='" . $r->idtarif . "' data-dismiss='modal'>" . TreeView($r->level, $r->nama) . '';
			}

			$row[] = $namatarif;
			if ($r->level == 0) {
				$row[] = 0;
				$row[] = 0;
				$row[] = 0;
				$row[] = 0;
				$row[] = 0;
			} else {
				$row[] = number_format($r->jasasarana);
				$row[] = number_format($r->jasapelayanan);
				$row[] = number_format($r->bhp);
				$row[] = number_format($r->biayaperawatan);
				$row[] = number_format($r->total);
			}

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function getDetailTindakanRawatInap($idpendaftaran, $idtindakan)
	{
		$idkelas = get_by_field('id', $idpendaftaran, 'trawatinap_pendaftaran')->idkelas;
		$query = $this->Trawatinap_tindakan_model->getDetailTindakanRawatInap($idtindakan, $idkelas);
		$this->output->set_output(json_encode($query));
	}

	public function saveDataTindakan()
	{
		$idrawatinap = $this->input->post('idpendaftaran');
		$iddokter = $this->input->post('iddokter');
		if ($iddokter) {
			$dokter = GetPotonganDokterRanap($idrawatinap, $iddokter);
			$pajak_dokter = $dokter->pajak_dokter;
			$potongan_rs = $dokter->potongan_rs;
		} else {
			$pajak_dokter = 0;
			$potongan_rs = 0;
		}

		if ($this->input->post('idrow') != '') {
			$this->db->set('idrawatinap', $this->input->post('idpendaftaran'));
			$this->db->set('tanggal', YMDFormat($this->input->post('tanggal')));
			$this->db->set('iddokter', $this->input->post('iddokter'));
			$this->db->set('idpelayanan', $this->input->post('idpelayanan'));
			$this->db->set('jasasarana', RemoveComma($this->input->post('jasasarana')));
			$this->db->set('jasasarana_disc', RemoveComma($this->input->post('jasasarana_disc')));
			$this->db->set('jasapelayanan', RemoveComma($this->input->post('jasapelayanan')));
			$this->db->set('jasapelayanan_disc', RemoveComma($this->input->post('jasapelayanan_disc')));
			$this->db->set('bhp', RemoveComma($this->input->post('bhp')));
			$this->db->set('bhp_disc', RemoveComma($this->input->post('bhp_disc')));
			$this->db->set('biayaperawatan', RemoveComma($this->input->post('biayaperawatan')));
			$this->db->set('biayaperawatan_disc', RemoveComma($this->input->post('biayaperawatan_disc')));
			$this->db->set('total', RemoveComma($this->input->post('subtotal')));
			$this->db->set('kuantitas', RemoveComma($this->input->post('kuantitas')));
			$this->db->set('diskon', RemoveComma($this->input->post('diskon')));
			$this->db->set('totalkeseluruhan', RemoveComma($this->input->post('totalkeseluruhan')));
			$this->db->set('pajak_dokter', $pajak_dokter);
			$this->db->set('potongan_rs', $potongan_rs);

			$this->db->set('modified_by', $this->session->userdata('user_id'));
			$this->db->set('modified_date', date('Y-m-d H:i:s'));

			$this->db->where('id', $this->input->post('idrow'));
			$this->db->update('trawatinap_tindakan');
		} else {
			$data = [];
			$data['idrawatinap'] = $this->input->post('idpendaftaran');
			$data['tanggal'] = YMDFormat($this->input->post('tanggal'));
			$data['iddokter'] = $this->input->post('iddokter');
			$data['idpelayanan'] = $this->input->post('idpelayanan');
			$data['jasasarana'] = RemoveComma($this->input->post('jasasarana'));
			$data['jasasarana_disc'] = RemoveComma($this->input->post('jasasarana_disc'));
			$data['jasapelayanan'] = RemoveComma($this->input->post('jasapelayanan'));
			$data['jasapelayanan_disc'] = RemoveComma($this->input->post('jasapelayanan_disc'));
			$data['bhp'] = RemoveComma($this->input->post('bhp'));
			$data['bhp_disc'] = RemoveComma($this->input->post('bhp_disc'));
			$data['biayaperawatan'] = RemoveComma($this->input->post('biayaperawatan'));
			$data['biayaperawatan_disc'] = RemoveComma($this->input->post('biayaperawatan_disc'));
			$data['total'] = RemoveComma($this->input->post('subtotal'));
			$data['kuantitas'] = RemoveComma($this->input->post('kuantitas'));
			$data['diskon'] = RemoveComma($this->input->post('diskon'));
			$data['totalkeseluruhan'] = RemoveComma($this->input->post('totalkeseluruhan'));
			$data['pajak_dokter'] = $pajak_dokter;
			$data['potongan_rs'] = $potongan_rs;

			$data['created_by'] = $this->session->userdata('user_id');
			$data['created_date'] = date('Y-m-d H:i:s');

			$this->db->insert('trawatinap_tindakan', $data);
		}
	}

	public function updateDataRuanganRanap()
	{
		$table = $this->input->post('table');
		$idrow = $this->input->post('idrow');

		// $date = strtotime($tanggaldari);
		// $date = strtotime("+$jumlahhari day", $date);
		// $tanggalsampai = date('Y-m-d', $date);

		$this->db->set('idruangan', $this->input->post('idruangan'));
		$this->db->set('idkelas', $this->input->post('idkelas'));
		$this->db->set('tanggaldari', $this->input->post('tanggal_dari'));
		$this->db->set('tanggalsampai', $this->input->post('tanggal_sampai'));
		$this->db->set('idtarif', $this->input->post('idtarif'));
		$this->db->set('jasasarana', RemoveComma($this->input->post('jasasarana')));
		$this->db->set('jasasarana_disc', RemoveComma($this->input->post('jasasarana_disc')));
		$this->db->set('jasapelayanan', RemoveComma($this->input->post('jasapelayanan')));
		$this->db->set('jasapelayanan_disc', RemoveComma($this->input->post('jasapelayanan_disc')));
		$this->db->set('bhp', RemoveComma($this->input->post('bhp')));
		$this->db->set('bhp_disc', RemoveComma($this->input->post('bhp_disc')));
		$this->db->set('biayaperawatan', RemoveComma($this->input->post('biayaperawatan')));
		$this->db->set('biayaperawatan_disc', RemoveComma($this->input->post('biayaperawatan_disc')));
		$this->db->set('total', RemoveComma($this->input->post('subtotal')));
		$this->db->set('jumlahhari', RemoveComma($this->input->post('kuantitas')));
		$this->db->set('diskon', RemoveComma($this->input->post('diskon')));
		$this->db->set('totalkeseluruhan', RemoveComma($this->input->post('totalkeseluruhan')));

		$this->db->where('id', $idrow);
		if ($this->db->update($table)) {
			return true;
		} else {
			return false;
		}
	}

	public function updateDataTindakanGlobal()
	{
		$table = $this->input->post('table');
		$idrow = $this->input->post('idrow');

		if ($table == 'trujukan_laboratorium_detail') {
			$this->db->set('iddokter', $this->input->post('iddokter'));
			$this->db->set('idlaboratorium', $this->input->post('idpelayanan'));
		} elseif ($table == 'trujukan_radiologi_detail') {
			$this->db->set('iddokter', $this->input->post('iddokter'));
			$this->db->set('idradiologi', $this->input->post('idpelayanan'));
		} elseif ($table == 'trujukan_fisioterapi_detail') {
			$this->db->set('idfisioterapi', $this->input->post('idpelayanan'));
		} elseif ($table == 'tkamaroperasi_sewaalat') {
			$this->db->set('idalat', $this->input->post('idpelayanan'));
		} else {
			$this->db->set('iddokter', $this->input->post('iddokter'));
			$this->db->set('idpelayanan', $this->input->post('idpelayanan'));
		}

		$this->db->set('jasasarana', RemoveComma($this->input->post('jasasarana')));
		$this->db->set('jasasarana_disc', RemoveComma($this->input->post('jasasarana_disc')));
		$this->db->set('jasapelayanan', RemoveComma($this->input->post('jasapelayanan')));
		$this->db->set('jasapelayanan_disc', RemoveComma($this->input->post('jasapelayanan_disc')));
		$this->db->set('bhp', RemoveComma($this->input->post('bhp')));
		$this->db->set('bhp_disc', RemoveComma($this->input->post('bhp_disc')));
		$this->db->set('biayaperawatan', RemoveComma($this->input->post('biayaperawatan')));
		$this->db->set('biayaperawatan_disc', RemoveComma($this->input->post('biayaperawatan_disc')));
		$this->db->set('total', RemoveComma($this->input->post('subtotal')));
		$this->db->set('kuantitas', RemoveComma($this->input->post('kuantitas')));
		$this->db->set('diskon', RemoveComma($this->input->post('diskon')));
		$this->db->set('totalkeseluruhan', RemoveComma($this->input->post('totalkeseluruhan')));

		$this->db->where('id', $idrow);
		if ($this->db->update($table)) {
			return true;
		} else {
			return false;
		}
	}

	public function updateDataTindakanFarmasi()
	{
		$table = $this->input->post('table');
		$idrow = $this->input->post('idrow');

		$hargadasar = RemoveComma($this->input->post('hargadasar'));
		$margin = RemoveComma($this->input->post('margin'));
		$hargajual = RemoveComma($this->input->post('hargajual'));
		$kuantitas = RemoveComma($this->input->post('kuantitas'));
		$diskon = RemoveComma($this->input->post('diskon'));
		$diskon_rp = RemoveComma($this->input->post('diskon_rp'));
		$total = RemoveComma($this->input->post('total'));

		$this->db->set('margin', $margin);
		$this->db->set('kuantitas', $kuantitas);

		if ($table == 'tpasien_penjualan_nonracikan' || $table == 'tpasien_pengembalian_detail') {
			$this->db->set('harga_dasar', $hargadasar);
			$this->db->set('harga', $hargajual);
			$this->db->set('diskon', $diskon);
			$this->db->set('diskon_rp', $diskon_rp);
			$this->db->set('totalharga', $total);
		} else {
			$this->db->set('hargadasar', $hargadasar);
			$this->db->set('hargajual', $hargajual);
			$this->db->set('diskon', $diskon_rp);
			$this->db->set('totalkeseluruhan', $total);
		}

		$this->db->where('id', $idrow);
		if ($this->db->update($table)) {
			return true;
		} else {
			return false;
		}
	}

	public function removeDataTindakan()
	{
		$this->db->where('id', $this->input->post('idrow'));
		if ($this->db->delete('trawatinap_tindakan')) {
			return true;
		} else {
			return false;
		}
	}

	// Visite Dokter
	public function getHistoryVisiteDokter($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getHistoryVisiteDokter($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	public function getRuanganVisite($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getCurrentRuangan($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	public function saveDataVisiteDokter()
	{
		$idrawatinap = $this->input->post('idpendaftaran');
		$iddokter = $this->input->post('iddokter');
		$dokter = GetPotonganDokterRanap($idrawatinap, $iddokter);

		if ($this->input->post('idrow') != '') {
			$this->db->set('idrawatinap', $this->input->post('idpendaftaran'));
			$this->db->set('tanggal', YMDFormat($this->input->post('tanggal')));
			$this->db->set('idruangan', $this->input->post('idruangan'));
			$this->db->set('iddokter', $this->input->post('iddokter'));
			$this->db->set('idpelayanan', $this->input->post('idpelayanan'));
			$this->db->set('jasasarana', RemoveComma($this->input->post('jasasarana')));
			$this->db->set('jasasarana_disc', RemoveComma($this->input->post('jasasarana_disc')));
			$this->db->set('jasapelayanan', RemoveComma($this->input->post('jasapelayanan')));
			$this->db->set('jasapelayanan_disc', RemoveComma($this->input->post('jasapelayanan_disc')));
			$this->db->set('bhp', RemoveComma($this->input->post('bhp')));
			$this->db->set('bhp_disc', RemoveComma($this->input->post('bhp_disc')));
			$this->db->set('biayaperawatan', RemoveComma($this->input->post('biayaperawatan')));
			$this->db->set('biayaperawatan_disc', RemoveComma($this->input->post('biayaperawatan_disc')));
			$this->db->set('total', RemoveComma($this->input->post('total')));
			$this->db->set('kuantitas', RemoveComma($this->input->post('kuantitas')));
			$this->db->set('diskon', RemoveComma($this->input->post('diskon')));
			$this->db->set('totalkeseluruhan', RemoveComma($this->input->post('totalkeseluruhan')));

			$this->db->set('modified_by', $this->session->userdata('user_id'));
			$this->db->set('modified_date', date('Y-m-d H:i:s'));
			$this->db->set('version', 'version+1', false);

			$this->db->set('pajak_dokter', $dokter->pajak_dokter);
			$this->db->set('potongan_rs', $dokter->potongan_rs);

			$this->db->where('id', $this->input->post('idrow'));
			$this->db->update('trawatinap_visite');
		} else {
			$data = [];
			$data['idrawatinap'] = $this->input->post('idpendaftaran');
			$data['tanggal'] = YMDFormat($this->input->post('tanggalvisite'));
			$data['idruangan'] = $this->input->post('idruangan');
			$data['iddokter'] = $this->input->post('iddokter');
			$data['idpelayanan'] = $this->input->post('idpelayanan');
			$data['jasasarana'] = RemoveComma($this->input->post('jasasarana'));
			$data['jasapelayanan'] = RemoveComma($this->input->post('jasapelayanan'));
			$data['bhp'] = RemoveComma($this->input->post('bhp'));
			$data['biayaperawatan'] = RemoveComma($this->input->post('biayaperawatan'));
			$data['total'] = RemoveComma($this->input->post('tarif'));
			$data['kuantitas'] = 1;
			$data['totalkeseluruhan'] = RemoveComma($this->input->post('tarif'));

			$data['created_by'] = $this->session->userdata('user_id');
			$data['created_date'] = date('Y-m-d H:i:s');

			if ($this->db->insert('trawatinap_visite', $data)) {
				$id = $this->db->insert_id();
				$this->db->set('pajak_dokter', $dokter->pajak_dokter);
				$this->db->set('potongan_rs', $dokter->potongan_rs);

				$this->db->where('id', $id);
				$this->db->update('trawatinap_visite');
			}
		}
	}

	public function removeDataVisiteDokter()
	{
		$this->db->where('id', $this->input->post('idrow'));
		if ($this->db->delete('trawatinap_visite')) {
			return true;
		} else {
			return false;
		}
	}

	// EOF Aksi Tindakan

	// Start Daftar Operasi
	public function getDataOperasi($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getDataOperasi($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	public function getHistoryOperasi($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getHistoryOperasi($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	public function saveDaftarOperasi()
	{
		$data = [];
		$data['tipe'] = $this->input->post('tipe');
		$data['tanggaloperasi'] = YMDFormat($this->input->post('tanggaloperasi'));
		$data['diagnosa'] = $this->input->post('diagnosaoperasi');
		$data['operasi'] = $this->input->post('operasi');
		$data['waktumulaioperasi'] = $this->input->post('jamoperasi');
		$data['namapetugas'] = $this->input->post('petugasoperasi');
		$data['catatan'] = $this->input->post('catatanoperasi');
		$data['idpendaftaran'] = $this->input->post('idpendaftaran');
		$data['idpasien'] = $this->input->post('idpasien');
		$data['status'] = '1';
		$data['statusdatang'] = '1';
		$data['statussetuju'] = '0';
		$data['idasalpendaftaran'] = 2;
		$data['tanggal_diajukan'] = date('Y-m-d H:i:s');

		if ($this->db->insert('tkamaroperasi_pendaftaran', $data)) {
			return true;
		} else {
			return false;
		}
	}

	// EOF Daftar Operasi

	// Start Deposit
	public function getBank()
	{
		$query = $this->Trawatinap_tindakan_model->getBank();
		$this->output->set_output(json_encode($query));
	}

	public function getHistoryDeposit($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getHistoryDeposit($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	public function getTotalDeposit($idpendaftaran)
	{
		$query = $this->Trawatinap_tindakan_model->getTotalDeposit($idpendaftaran);
		$this->output->set_output(json_encode($query));
	}

	public function saveDataDeposit()
	{
		$data = [];
		$data['idrawatinap'] = $this->input->post('idrawatinap');
		$data['tanggal'] = YMDFormat($this->input->post('tanggal'));
		$data['idmetodepembayaran'] = $this->input->post('idmetodepembayaran');
		$data['idbank'] = $this->input->post('idbank');
		$data['nominal'] = RemoveComma($this->input->post('nominal'));
		$data['terimadari'] = $this->input->post('terimadari');
		$data['idtipe'] = $this->input->post('idtipe');
		$data['iduserinput'] = $this->session->userdata('user_id');
		$data['created_date'] = date('Y-m-d H:i:s');

		if ($this->db->insert('trawatinap_deposit', $data)) {
			return true;
		} else {
			return false;
		}
	}

	public function removeDataDeposit()
	{
		$this->db->set('iduserdelete', $this->session->userdata('user_id'));
		$this->db->set('alasanhapus', $this->input->post('alasanhapus'));
		$this->db->set('deleted_date', date("Y-m-d H:i:s"));
		$this->db->set('status', 0);
		$this->db->where('id', $this->input->post('idrow'));
		if ($this->db->update('trawatinap_deposit')) {
			return true;
		} else {
			return false;
		}
	}

	// EOF Deposit

	// Start Estimasi
	public function getImplan()
	{
		$search = $this->input->post('search');
		$query = $this->Trawatinap_tindakan_model->getImplan($search);
		$this->output->set_output(json_encode($query));
	}

	public function getDetailImplan($idimplan)
	{
		$query = $this->Trawatinap_tindakan_model->getDetailImplan($idimplan);
		$this->output->set_output(json_encode($query));
	}

	public function saveDataEstimasi()
	{
		$data = [];
		$data['idrawatinap'] = $this->input->post('idpendaftaran');
		$data['tanggal'] = date('Y-m-d');
		$data['diagnosa'] = $this->input->post('diagnosa');
		$data['operasi'] = $this->input->post('operasi');
		$data['jenisoperasi'] = $this->input->post('jenisoperasi');
		$data['sewakamar'] = RemoveComma($this->input->post('sewakamar'));
		$data['jasadokteroperator'] = RemoveComma($this->input->post('jasadokteroperator'));
		$data['jasadokteranaesthesi'] = RemoveComma($this->input->post('jasadokteranaesthesi'));
		$data['jasaassisten'] = RemoveComma($this->input->post('jasaassisten'));
		$data['obatbhp'] = RemoveComma($this->input->post('obatbhp'));
		$data['ruangperawatan'] = RemoveComma($this->input->post('ruangperawatan'));
		$data['kuantitasruangperawatan'] = RemoveComma($this->input->post('kuantitasruangperawatan'));
		$data['ruanghcu'] = RemoveComma($this->input->post('ruanghcu'));
		$data['kuantitasruanghcu'] = RemoveComma($this->input->post('kuantitasruanghcu'));
		$data['obatperawatan'] = RemoveComma($this->input->post('obatperawatan'));
		$data['penunjanglain'] = RemoveComma($this->input->post('penunjanglain'));
		$data['created_date'] = date('Y-m-d H:i:s');

		if ($this->db->insert('testimasi', $data)) {
			$idestimasi = $this->db->insert_id();
			$implanList = json_decode($this->input->post('dataImplan'));
			foreach ($implanList as $row) {
				$dataImplant = [];
				$dataImplant['idtestimasi'] = $idestimasi;
				$dataImplant['idimplan'] = $row[0];
				$dataImplant['hargadasar'] = RemoveComma($row[2]);
				$dataImplant['margin'] = RemoveComma($row[3]);
				$dataImplant['hargajual'] = RemoveComma($row[4]);
				$dataImplant['kuantitas'] = RemoveComma($row[5]);
				$dataImplant['statusimplan'] = $row[6];

				$this->db->insert('testimasi_implan', $dataImplant);
			}

			return true;
		} else {
			return false;
		}
	}

	public function approveDataEstimasi()
	{
		$this->db->where('id', $this->input->post('idrow'));
		$this->db->set('status', 2);
		if ($this->db->update('testimasi')) {
			return true;
		} else {
			return false;
		}
	}

	public function uploadFotoScan($idestimasi)
	{
		if (isset($_FILES['fotoscan'])) {
			if ($_FILES['fotoscan']['name'] != '') {
				$config['upload_path'] = './assets/upload/estimasi/';
				$config['allowed_types'] = 'jpg|jpeg|bmp|tiff|png';
				$config['encrypt_name'] = true;

				$this->load->library('upload', $config);

				$this->upload->initialize($config);

				if ($this->upload->do_upload('fotoscan')) {
					$image_upload = $this->upload->data();

					$this->db->where('id', $idestimasi);
					$this->db->set('fotoscan', $image_upload['file_name']);
					$this->db->update('testimasi');
				} else {
					$data = ['error' => $this->upload->display_errors()];
					print_r($this->upload->display_errors());
					exit();
					return false;
				}
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	// EOF Estimasi

	// RUJUKAN
	public function rujukFarmasi()
	{
		$idtindakan = $this->input->post('idrawatinap');

		$this->db->where('id', $idtindakan);
		$this->db->set('rujukfarmasi', 1);
		if ($this->db->update('trawatinap_pendaftaran')) {
			return true;
		} else {
			return false;
		}
	}

	public function rujukLaboratorium()
	{
		$idtindakan = $this->input->post('idrawatinap');

		$this->db->where('id', $idtindakan);
		$this->db->set('rujuklaboratorium', 1);
		if ($this->db->update('trawatinap_pendaftaran')) {
			return true;
		} else {
			return false;
		}
	}

	public function rujukRadiologi()
	{
		$idtindakan = $this->input->post('idrawatinap');

		$this->db->where('id', $idtindakan);
		$this->db->set('rujukradiologi', 1);
		if ($this->db->update('trawatinap_pendaftaran')) {
			return true;
		} else {
			return false;
		}
	}

	public function rujukFisioterapi()
	{
		$idtindakan = $this->input->post('idrawatinap');

		$this->db->where('id', $idtindakan);
		$this->db->set('rujukfisioterapi', 1);
		if ($this->db->update('trawatinap_pendaftaran')) {
			return true;
		} else {
			return false;
		}
	}

	public function updateStatusRuanganRanap()
	{
		$idrow = $this->input->post('idrow');
		$table = $this->input->post('table');
		$statusverifikasi = $this->input->post('statusverifikasi');

		$this->db->where('id', $idrow);
		$this->db->set('statusverifikasi', $statusverifikasi);

		if ($this->db->update($table)) {
			return true;
		} else {
			return false;
		}
	}
	public function updateStatusDeleteTindakan()
	{
		$idrow = $this->input->post('idrow');
		$table = $this->input->post('table');
		$status = $this->input->post('status');

		$this->db->where('id', $idrow);
		$this->db->set('status', $status);

		if ($this->db->update($table)) {
			return true;
		} else {
			return false;
		}
	}
	public function updateStatusDeleteRuanganRanap()
	{
		$idrow = $this->input->post('idrow');
		$table = $this->input->post('table');
		$statusverifikasi = $this->input->post('statusverifikasi');

		$this->db->where('id', $idrow);
		$this->db->set('status', $statusverifikasi);

		if ($this->db->update($table)) {
			return true;
		} else {
			return false;
		}
	}

	// RINCIAN VERIFIKASI
	public function updateStatusTindakanDetail()
	{
		$idrow = $this->input->post('idrow');
		$table = $this->input->post('table');
		$statusracikan = $this->input->post('statusracikan');
		$statusverifikasi = $this->input->post('statusverifikasi');

		$this->db->where('id', $idrow);
		if ($table == 'tkamaroperasi_jasado' || $table == 'tkamaroperasi_jasada' || $table == 'tkamaroperasi_jasaao' || $table == 'tkamaroperasi_jasadaa') {
			$this->db->set('statusverif', $statusverifikasi);
		} else {
			$this->db->set('statusverifikasi', $statusverifikasi);
		}

		if ($table == 'farmasi') {
			if ($statusracikan == 1) {
				if ($this->db->update('tpasien_penjualan_racikan')) {
					return true;
				} else {
					return false;
				}
			} else {
				if ($this->db->update('tpasien_penjualan_nonracikan')) {
					return true;
				} else {
					return false;
				}
			}
		} else {
			if ($this->db->update($table)) {
				return true;
			} else {
				return false;
			}
		}
	}

	// ADMINISTRASI
	public function updateAdministrasi()
	{
		$table = $this->input->post('table');
		$idrow = $this->input->post('idrow');

		if ($table == 'trawatinap_administrasi') {
			$this->db->set('tarif', RemoveComma($this->input->post('tarif')));
			$this->db->set('diskon', RemoveComma($this->input->post('diskon')));
			$this->db->set('tarifsetelahdiskon', RemoveComma($this->input->post('totalkeseluruhan')));
		} else {
			$this->db->set('idadministrasi', $this->input->post('idadministrasi'));
			$this->db->set('jasasarana', RemoveComma($this->input->post('jasasarana')));
			$this->db->set('jasasarana_disc', RemoveComma($this->input->post('jasasarana_disc')));
			$this->db->set('jasapelayanan', RemoveComma($this->input->post('jasapelayanan')));
			$this->db->set('jasapelayanan_disc', RemoveComma($this->input->post('jasapelayanan_disc')));
			$this->db->set('bhp', RemoveComma($this->input->post('bhp')));
			$this->db->set('bhp_disc', RemoveComma($this->input->post('bhp_disc')));
			$this->db->set('biayaperawatan', RemoveComma($this->input->post('biayaperawatan')));
			$this->db->set('biayaperawatan_disc', RemoveComma($this->input->post('biayaperawatan_disc')));
			$this->db->set('total', RemoveComma($this->input->post('subtotal')));
			$this->db->set('kuantitas', RemoveComma($this->input->post('kuantitas')));
			$this->db->set('diskon', RemoveComma($this->input->post('diskon')));
			$this->db->set('totalkeseluruhan', RemoveComma($this->input->post('totalkeseluruhan')));
		}

		$this->db->where('id', $idrow);
		if ($this->db->update($table)) {
			return true;
		} else {
			return false;
		}
	}

	public function updateStatusAdministrasi()
	{
		$idrow = $this->input->post('idrow');
		$table = $this->input->post('table');
		$statusverifikasi = $this->input->post('statusverifikasi');

		$this->db->where('id', $idrow);
		$this->db->set('statusverifikasi', $statusverifikasi);

		if ($this->db->update($table)) {
			return true;
		} else {
			return false;
		}
	}

	// SEWA KAMAR OK
	public function updateSewaKamarOK()
	{
		$table = $this->input->post('table');
		$idrow = $this->input->post('idrow');

		$this->db->set('jasasarana', RemoveComma($this->input->post('jasasarana')));
		$this->db->set('jasasarana_disc', RemoveComma($this->input->post('jasasarana_disc')));
		$this->db->set('jasapelayanan', RemoveComma($this->input->post('jasapelayanan')));
		$this->db->set('jasapelayanan_disc', RemoveComma($this->input->post('jasapelayanan_disc')));
		$this->db->set('bhp', RemoveComma($this->input->post('bhp')));
		$this->db->set('bhp_disc', RemoveComma($this->input->post('bhp_disc')));
		$this->db->set('biayaperawatan', RemoveComma($this->input->post('biayaperawatan')));
		$this->db->set('biayaperawatan_disc', RemoveComma($this->input->post('biayaperawatan_disc')));
		$this->db->set('total', RemoveComma($this->input->post('subtotal')));
		$this->db->set('diskon', RemoveComma($this->input->post('diskon')));
		$this->db->set('totalkeseluruhan', RemoveComma($this->input->post('totalkeseluruhan')));

		$this->db->where('id', $idrow);

		if ($this->db->update($table)) {
			return true;
		} else {
			return false;
		}
	}

	public function updateStatusSewaKamarOK()
	{
		$idrow = $this->input->post('idrow');
		$table = $this->input->post('table');
		$statusverifikasi = $this->input->post('statusverifikasi');

		$this->db->where('id', $idrow);
		$this->db->set('statusverif', $statusverifikasi);

		if ($this->db->update($table)) {
			return true;
		} else {
			return false;
		}
	}

	public function updateDataAsistenOperasi()
	{
		$table = $this->input->post('table');
		$idrow = $this->input->post('idrow');

		$this->db->set('persen', RemoveComma($this->input->post('persentase')));
		$this->db->set('totalkeseluruhan', RemoveComma($this->input->post('tarif')));
		$this->db->set('diskon', RemoveComma($this->input->post('diskon')));

		$this->db->where('id', $idrow);

		if ($this->db->update($table)) {
			return true;
		} else {
			return false;
		}
	}

	// Start Pembayaran
	public function updateTanggalPembayaran()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		$tanggalPembayaran = YMDFormat($this->input->post('tanggal_pembayaran'));
		$waktuPembayaran = $this->input->post('waktu_pembayaran');

		$this->db->set('tanggal', $tanggalPembayaran . ' ' . $waktuPembayaran);
		$this->db->where('idtindakan', $idpendaftaran);
		$this->db->where('statusbatal', 0);

		if ($this->db->update('trawatinap_tindakan_pembayaran')) {
			return true;
		} else {
			return false;
		}
	}

	public function getIndex($uri)
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];

		$this->select = [
			'trawatinap_pendaftaran.id AS idpendaftaran',
			'trawatinap_pendaftaran.tanggaldaftar',
			'trawatinap_pendaftaran.statuscheckout',
			'tpoliklinik_pendaftaran.nopendaftaran',
			'trawatinap_pendaftaran.iddokterpenanggungjawab',
			'trawatinap_pendaftaran.idkelompokpasien',
			'trawatinap_pendaftaran.status',
			'mfpasien.no_medrec AS nomedrec',
			'mfpasien.id AS idpasien',
			'mfpasien.nama AS namapasien',
			'mfpasien.jenis_kelamin',
			'mfpasien.umur_tahun',
			'mfpasien.umur_bulan',
			'mfpasien.umur_hari',
			'tpoliklinik_pendaftaran.iddokter',
			'mdokterperujuk.nama AS namadokterperujuk',
			'mdokterpenanggungjawab.nama AS namadokterpenanggungjawab',
			'mbed.nama AS namabed',
		];

		$this->from = 'trawatinap_pendaftaran';

		$this->join = [
			['mbed', 'mbed.id = trawatinap_pendaftaran.idbed', 'LEFT'],
			['tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT'],
			['mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT'],
			['mdokter mdokterperujuk', 'mdokterperujuk.id = tpoliklinik_pendaftaran.iddokter', 'LEFT'],
			['mdokter mdokterpenanggungjawab', 'mdokterpenanggungjawab.id = trawatinap_pendaftaran.iddokterpenanggungjawab', 'LEFT'],
		];

		// FILTER
		if ($uri == 'filter') {
			$this->where = [];
			if ($this->session->userdata('idruangan') != '#') {
				$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idruangan' => $this->session->userdata('idruangan')]);
			}
			if ($this->session->userdata('idkelas') != '#') {
				$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idkelas' => $this->session->userdata('idkelas')]);
			}
			if ($this->session->userdata('idbed') != '#') {
				$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idbed' => $this->session->userdata('idbed')]);
			}
			if ($this->session->userdata('namapasien') != '') {
				$this->where = array_merge($this->where, ['mfpasien.nama LIKE' => '%' . $this->session->userdata('namapasien') . '%']);
			}
			if ($this->session->userdata('iddokterpenanggungjawab') != '#') {
				$this->where = array_merge($this->where, ['mdokterpenanggungjawab.id' => $this->session->userdata('iddokterpenanggungjawab')]);
			}
			if ($this->session->userdata('iddokterperujuk') != '#') {
				$this->where = array_merge($this->where, ['mdokterperujuk.id' => $this->session->userdata('iddokterperujuk')]);
			}
			if ($this->session->userdata('status') != '#') {
				$this->where = array_merge($this->where, ['trawatinap_pendaftaran.status' => $this->session->userdata('status')]);
			}
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idtipe' => '1']);
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.statuscheckout' => '0']);
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.status' => '1']);
		} else {
			$this->where = ['trawatinap_pendaftaran.idtipe' => '1'];
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.statuscheckout' => '0']);
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.status' => '1']);

			if ($this->session->userdata('idruangan') != '#') {
				$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idruangan' => $this->session->userdata('idruangan')]);
			}
			if ($this->session->userdata('idkelas') != '#') {
				$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idkelas' => $this->session->userdata('idkelas')]);
			}
		}

		$this->order = [
			'trawatinap_pendaftaran.id' => 'DESC'
		];

		$this->group = [];
		if (UserAccesForm($user_acces_form, ['370'])) {
			$this->column_search = ['trawatinap_pendaftaran.tanggaldaftar', 'mfpasien.no_medrec', 'mfpasien.nama', 'mfpasien.jenis_kelamin', 'mfpasien.umur_tahun', 'mdokterperujuk.nama', 'mdokterpenanggungjawab.nama'];
		} else {
			$this->column_search = [];
		}
		$this->column_order = ['trawatinap_pendaftaran.tanggaldaftar', 'mfpasien.no_medrec', 'mfpasien.nama', 'mfpasien.jenis_kelamin', 'mfpasien.umur_tahun', 'mdokterperujuk.nama', 'mdokterpenanggungjawab.nama'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			if (UserAccesForm($user_acces_form, ['1337'])) {
				$action = '<div class="btn-group">
                <div class="btn-group dropup">
                  <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                    <span class="fa fa-print"></span>
                  </button>
                <ul class="dropdown-menu">
                  <li>
                    <a tabindex="-1" target="_blank" href="' . site_url() . 'trawatinap_pendaftaran/printForm/' . $r->idpendaftaran . '/1">Sticker ID</a>
                  </li>
                </ul>
                </div>
                <a href="' . site_url() . 'trm_berkas/profile/' . $r->idpasien . '" target="_blank" class="btn btn-sm btn-primary gabung" title="Berkas Pasien"><i class="fa fa-external-link-square"></i></a>
              </div>';
			} else {
				$action = '';
			}

			$row[] = "<span data-idpendaftaran='" . $r->idpendaftaran . "'>" . $no . '</span>';
			$row[] = "<span class='label label-success text-center'><i class='fa fa-hotel'></i> '" . $r->namabed . '</span>';
			$row[] = $r->nomedrec;
			$row[] = $r->namapasien;
			$row[] = GetJenisKelamin($r->jenis_kelamin);
			$row[] = $r->umur_tahun . ' Th ' . $r->umur_bulan . ' Bln ' . $r->umur_hari . ' Hr';
			$row[] = $r->namadokterpenanggungjawab;
			$row[] = $action;

			$data[] = $row;
		}

		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function getIndexCheckout($uri)
	{
		$this->select = [
			'trawatinap_pendaftaran.id AS idpendaftaran',
			'trawatinap_pendaftaran.idkelompokpasien',
			'tpoliklinik_pendaftaran.nopendaftaran',
			'trawatinap_checkout.tanggalkeluar',
			'mfpasien.no_medrec AS nomedrec',
			'mfpasien.nama AS namapasien',
			'mdokterpenanggungjawab.nama AS namadokterpenanggungjawab',
			'mbed.nama AS namabed',
			'mkelas.nama AS namakelas',
			'mpasien_kelompok.nama AS namakelompokpasien',
			'trawatinap_pendaftaran.statuskasir',
			'trawatinap_pendaftaran.statustransaksi',
			'trawatinap_pendaftaran.statuspembayaran',
		];

		$this->from = 'trawatinap_pendaftaran';

		$this->join = [
			['mbed', 'mbed.id = trawatinap_pendaftaran.idbed', 'LEFT'],
			['mkelas', 'mkelas.id = trawatinap_pendaftaran.idkelas', 'LEFT'],
			['tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT'],
			['mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT'],
			['mdokter mdokterpenanggungjawab', 'mdokterpenanggungjawab.id = trawatinap_pendaftaran.iddokterpenanggungjawab', 'LEFT'],
			['mdokter mdokterperujuk', 'mdokterperujuk.id = tpoliklinik_pendaftaran.iddokter', 'LEFT'],
			['mpasien_kelompok', 'mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien', 'LEFT'],
			['trawatinap_checkout', 'trawatinap_checkout.idrawatinap = trawatinap_pendaftaran.id', 'LEFT'],
		];

		// FILTER
		if ($uri == 'filterCheckout') {
			$this->where = [];
			if ($this->session->userdata('idruangan') != '#') {
				$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idruangan' => $this->session->userdata('idruangan')]);
			}
			if ($this->session->userdata('idkelas') != '#') {
				$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idkelas' => $this->session->userdata('idkelas')]);
			}
			if ($this->session->userdata('idbed') != '#') {
				$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idbed' => $this->session->userdata('idbed')]);
			}
			if ($this->session->userdata('namapasien') != '') {
				$this->where = array_merge($this->where, ['mfpasien.nama LIKE' => '%' . $this->session->userdata('namapasien') . '%']);
			}
			if ($this->session->userdata('iddokterpenanggungjawab') != '#') {
				$this->where = array_merge($this->where, ['mdokterpenanggungjawab.id' => $this->session->userdata('iddokterpenanggungjawab')]);
			}
			if ($this->session->userdata('iddokterperujuk') != '#') {
				$this->where = array_merge($this->where, ['mdokterperujuk.id' => $this->session->userdata('iddokterperujuk')]);
			}
			if ($this->session->userdata('status') != '#') {
				$this->where = array_merge($this->where, ['trawatinap_pendaftaran.statuskasir' => $this->session->userdata('status')]);
			}

			if ($this->session->userdata('tanggaldari_transaksi') != null) {
				$this->where = array_merge($this->where, ['DATE(trawatinap_pendaftaran.tanggaldaftar) >=' => YMDFormat($this->session->userdata('tanggaldari_transaksi'))]);
			}
			if ($this->session->userdata('tanggalsampai_transaksi') != null) {
				$this->where = array_merge($this->where, ['DATE(trawatinap_pendaftaran.tanggaldaftar) <=' => YMDFormat($this->session->userdata('tanggalsampai_transaksi'))]);
			}

			if ($this->session->userdata('tanggaldari_checkout') != null) {
				$this->where = array_merge($this->where, ['DATE(trawatinap_checkout.tanggalkeluar) >=' => YMDFormat($this->session->userdata('tanggaldari_checkout'))]);
			}
			if ($this->session->userdata('tanggalsampai_checkout') != null) {
				$this->where = array_merge($this->where, ['DATE(trawatinap_checkout.tanggalkeluar) <=' => YMDFormat($this->session->userdata('tanggalsampai_checkout'))]);
			}

			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idtipe' => '1']);
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.statuscheckout' => '1']);
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.status' => '1']);
		} else {
			// $this->where = array('trawatinap_pendaftaran.id' => null);
			$this->where = ['trawatinap_pendaftaran.idtipe' => '1'];
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.id' => null]);
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.statuscheckout' => '1']);
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.status' => '1']);
			if ($this->session->userdata('idruangan') != '#') {
				$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idruangan' => $this->session->userdata('idruangan')]);
			}
			if ($this->session->userdata('idkelas') != '#') {
				$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idkelas' => $this->session->userdata('idkelas')]);
			}
		}

		$this->order = [
			'trawatinap_checkout.tanggalkeluar' => 'DESC'
		];
		$this->group = [];

		$this->column_search = ['trawatinap_pendaftaran.tanggaldaftar', 'mfpasien.no_medrec', 'mfpasien.nama', 'mfpasien.jenis_kelamin', 'mfpasien.umur_tahun', 'mdokterperujuk.nama', 'mdokterpenanggungjawab.nama'];
		$this->column_order = ['trawatinap_pendaftaran.tanggaldaftar', 'mfpasien.no_medrec', 'mfpasien.nama', 'mfpasien.jenis_kelamin', 'mfpasien.umur_tahun', 'mdokterperujuk.nama', 'mdokterpenanggungjawab.nama'];
		// print_r($this->where);exit();
		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];

		foreach ($list as $r) {
			$no++;
			$row = [];

			$action = '<div class="btn-group">
              <div class="btn-group">
                <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                  <span class="fa fa-print"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-left">
                  <li>';
			if (UserAccesForm($user_acces_form, ['1282'])) {
				$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trawatinap_tindakan/print_rincian_biaya/' . $r->idpendaftaran . '/0">Rincian Biaya (All)</a>';
			}
			if (UserAccesForm($user_acces_form, ['1283'])) {
				$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trawatinap_tindakan/print_rincian_biaya/' . $r->idpendaftaran . '/1">Rincian Biaya (Verified)</a>';
			}
			if (UserAccesForm($user_acces_form, ['1284'])) {
				$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trawatinap_tindakan/print_rincian_global/' . $r->idpendaftaran . '/0">Rincian Global (All)</a>';
			}
			if (UserAccesForm($user_acces_form, ['1285'])) {
				$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trawatinap_tindakan/print_rincian_global/' . $r->idpendaftaran . '/1">Rincian Global (Verified)</a>';
			}
			if (UserAccesForm($user_acces_form, ['1286'])) {
				$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trawatinap_pendaftaran/printForm/' . $r->idpendaftaran . '/1">Sticker ID</a>';
			}

			$pembayaran = $this->db->query('SELECT
              trawatinap_tindakan_pembayaran_detail.tipekontraktor,
              trawatinap_tindakan_pembayaran_detail.idkontraktor,
            	(CASE
            		WHEN trawatinap_tindakan_pembayaran_detail.tipekontraktor = 1 THEN mrekanan.nama
            		ELSE mpasien_kelompok.nama
            	END) AS namakontraktor
            FROM trawatinap_tindakan_pembayaran_detail
            JOIN trawatinap_tindakan_pembayaran ON trawatinap_tindakan_pembayaran.id = trawatinap_tindakan_pembayaran_detail.idtindakan
            LEFT JOIN mrekanan ON mrekanan.id = trawatinap_tindakan_pembayaran_detail.idkontraktor AND trawatinap_tindakan_pembayaran_detail.tipekontraktor = 1
            LEFT JOIN mpasien_kelompok ON mpasien_kelompok.id = trawatinap_tindakan_pembayaran_detail.tipekontraktor
            WHERE trawatinap_tindakan_pembayaran_detail.idmetode = 8 AND trawatinap_tindakan_pembayaran.idtindakan = "' . $r->idpendaftaran . '"
            GROUP BY tipekontraktor, idkontraktor');

			foreach ($pembayaran->result() as $pb) {
				$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trawatinap_tindakan/print_rincian_tagihan/' . $r->idpendaftaran . '/' . $pb->tipekontraktor . '/' . $pb->idkontraktor . '">Tagihan Rekanan (' . $pb->namakontraktor . ')</a>';
			}

			$action .= '</li>
                </ul>
              </div>
            </div>';

			$row[] = "<span data-idpendaftaran='" . $r->idpendaftaran . "'>" . $no . '</span>';
			// $row[] = "<a href='#' id='openModalCheckoutExisting' data-toggle='modal' data-target='#CheckoutModal'>".$r->tanggalkeluar."</a>";
			$row[] = $r->tanggalkeluar;
			$row[] = $r->nopendaftaran;
			$row[] = $r->nomedrec;
			$row[] = $r->namapasien;
			$row[] = $r->namakelas;
			$row[] = $r->namabed;
			$row[] = $r->namadokterpenanggungjawab;
			$row[] = $r->namakelompokpasien;
			$row[] = StatusTransaksiKasir($r->statuspembayaran);
			$row[] = $action;

			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}

	public function simpan_transaksi()
	{
		return $this->Trawatinap_tindakan_model->simpan_transaksi();
	}

	public function success($id)
	{
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'pembayaran telah berhasil disimpan.');
		redirect('trawatinap_tindakan/verifikasi/' . $id, 'location');
	}

	public function getTarifVisiteDokter($idkategori, $idkelompokpasien, $idruangan, $idkelas)
	{
		$result = $this->Trawatinap_tindakan_model->getTarifVisiteDokter($idkategori, $idkelompokpasien, $idruangan, $idkelas);
		$this->output->set_content_type('application/json')->set_output(json_encode($result, JSON_PRETTY_PRINT));
	}

	public function getRsKlinik()
	{
		$term = $this->input->get('term');
		$this->db->select('id, nama');
		if ($term) {
			$this->db->like('nama', $term, 'BOTH');
		}
		$this->db->where('status', '1');
		$result = $this->db->get('mrumahsakit')->result_array();
		$data['results'] = $result;
		$this->output->set_content_type('application/json')->set_output(json_encode($data, JSON_PRETTY_PRINT));
	}

	public function getAlasanBatal()
	{
		$term = $this->input->get('term');
		$this->db->select('id, keterangan');
		if ($term) {
			$this->db->like('keterangan', $term, 'BOTH');
		}
		$this->db->where('status', '1');
		$result = $this->db->get('malasan_batal')->result_array();
		$data['results'] = $result;
		$this->output->set_content_type('application/json')->set_output(json_encode($data, JSON_PRETTY_PRINT));
	}

	public function getDiagnosa()
	{
		$id = $this->uri->segment(3);
		if ($id) {
			$this->db->where('idpendaftaran', $id);
			$get = $this->db->get('tkamaroperasi_pendaftaran', 1)->row_array();
			if ($get) {
				$data['diagnosa'] = $get['diagnosa'];
			} else {
				$data['diagnosa'] = '';
			}
			$this->output->set_content_type('application/json')->set_output(json_encode($data));
		}
	}

	public function print_kwitansi($id, $tipekontraktor, $idkontraktor)
	{
		$dompdf = new Dompdf();

		$row = $this->Trawatinap_tindakan_model->getDetailKwitansi($id);
		if ($row->tipe_proses == 9) {
			$nominal = $this->Trawatinap_tindakan_model->getTotalExcess($row->idrawatinap);
		} elseif ($row->tipe_proses == 10) {
			$nominal = $this->Trawatinap_tindakan_model->getTotalBPJSTK($row->idrawatinap);
		} else {
			$nominal = $this->Trawatinap_tindakan_model->getTotalPembayaran($row->idrawatinap, $row->tipe_proses, $tipekontraktor, $idkontraktor);
		}
		$ranap = $this->Trawatinap_tindakan_model->getDataRawatInap($row->idrawatinap);

		$data = [
			'id' => $row->id,
			'tanggal' => $row->tanggal,
			'no_kwitansi' => "KWR-$ranap->nopendaftaran",
			'sudahterimadari' => $row->sudahterimadari,
			'deskripsi' => $row->deskripsi,
			'nominal' => $nominal,
			'userinput' => $row->namauser,
			'status' => $row->status,
		];

		$html = $this->load->view('Trawatinap_tindakan/kwitansi', $data, true);

		$dompdf->loadHtml($html);
		$dompdf->setPaper('A4', 'portrait');
		$dompdf->render();
		$dompdf->stream('Kwitansi.pdf', ['Attachment' => 0]);
	}

	public function print_kwitansi_implant($id)
	{
		$dompdf = new Dompdf();

		$row = $this->Trawatinap_tindakan_model->getDetailKwitansiImplant($id);
		$ranap = $this->Trawatinap_tindakan_model->getDataRawatInap($row->idrawatinap);
		$nominal = $this->Trawatinap_tindakan_model->getTotalKwitansiImplant($row->idrawatinap);

		$data = [
			'id' => $row->id,
			'tanggal' => $row->tanggal,
			'no_kwitansi' => "KWR-$ranap->nopendaftaran",
			'sudahterimadari' => $row->sudahterimadari,
			'deskripsi' => $row->deskripsi,
			'nominal' => $nominal,
			'userinput' => $row->namauser,
			'status' => $row->status,
		];

		$html = $this->load->view('Trawatinap_tindakan/print/kwitansi_implant', $data, true);

		$dompdf->loadHtml($html);
		$dompdf->setPaper('A4', 'portrait');
		$dompdf->render();
		$dompdf->stream('Kwitansi.pdf', ['Attachment' => 0]);
	}

	public function print_faktur_implant($id)
	{
		$dompdf = new Dompdf();

		$row = $this->Trawatinap_tindakan_model->getDetailFakturImplant($id);
		$data = [
			'id' => $row->id,
			'nomedrec' => $row->nomedrec,
			'namapasien' => $row->namapasien,
			'namadokter' => $row->namadokter,
			'diagnosa' => $row->diagnosa,
			'operasi' => $row->operasi,
			'namadistributor' => $row->namadistributor,
			'tanggaloperasi' => $row->tanggal_operasi,
			'print_date' => date("Y-m-d H:i:s"),
			'print_user' => $this->session->userdata('user_name'),
		];

		$data['list_tindakan'] = $this->Trawatinap_verifikasi_model->viewRincianOperasiImplan($row->idrawatinap);

		$html = $this->load->view('Trawatinap_tindakan/print/faktur_implant', $data, true);

		$dompdf->loadHtml($html);
		$dompdf->setPaper('A4', 'portrait');
		$dompdf->render();
		$dompdf->stream('Kwitansi.pdf', ['Attachment' => 0]);
	}

	public function find_kontraktor($jenis)
	{
		$arr['detail'] = $this->Trawatinap_tindakan_model->get_nama_kontraktor($jenis);
		$this->output->set_output(json_encode($arr));
	}
}

/* End of file Trawatinap_tindakan.php */
/* Location: ./application/controllers/Trawatinap_tindakan.php */
