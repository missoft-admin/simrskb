<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tvalidasi_jurnal_umum extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('tvalidasi_jurnal_umum_model','model');
	}

	function index() {
		$data=array(
			'st_posting'=>'#',
			'nojurnal'=>'',
			'tipe'=>'#',
			'status'=>'#',
			'userid'=>'#',
			'tanggal_trx1'=>'',
			'tanggal_trx2'=>'',
		);
		$data['error'] 			= '';
		$data['list_tipe'] 	= $this->model->list_tipe();
		$data['list_user'] 	= $this->model->list_user();
		$data['title'] 			= 'Validasi Jurnal Jurnal Umum';
		$data['content'] 		= 'Tvalidasi_jurnal_umum/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Jurnal Umum",'tvalidasi_jurnal_umum/index'),
									array("List",'#')
								);

		// print_r($data);exit();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function create($id='',$disabel=''){
		if ($id){
			$data=$this->model->getHeader($id);
		}else{
			$data = array(
				'id' 						=> $id,			
				'nojurnal' 						=> '',			
				'tipe' 						=> '#',			
				'jenis' 					=> '#',			
				'debet' 					=> '0',			
				'kredit' 					=> '0',			
				'keterangan' 					=> '',			
				'tanggal_transaksi' 	=> date('d-m-Y'),
				
			);
		}


		$data['list_tipe'] 	= $this->model->list_tipe();
		$data['list_jenis'] 	= $this->model->list_jenis();
		$data['list_akun'] 	= $this->model->list_akun();
		$data['disabel'] 			= $disabel;
		$data['error'] 			= '';
		$data['title'] 			= 'Jurnal Umum';
		$data['content'] 		= 'Tvalidasi_jurnal_umum/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Setoran Kas",'#'),
									    			array("Tambah",'Tsetoran_kas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function simpan_jurnal(){
		$idvalidasi=$this->input->post('id');
		$iddet=$this->input->post('iddet');
		$debet=RemoveComma($this->input->post('debet'));
		$kredit=RemoveComma($this->input->post('kredit'));
		$ket=$this->input->post('ket');
		$idakun=$this->input->post('idakun');
		$posisi=($debet > 0 ?'D':'K');
		$q="SELECT M.noakun,M.namaakun FROM makun_nomor M WHERE M.id='$idakun'";
		$r=$this->db->query($q)->row();
		$noakun=$r->noakun;
		$namaakun=$r->namaakun;
		$data=array(
			'idvalidasi'=>$idvalidasi,
			'idakun'=>$idakun,
			'noakun'=>$noakun,
			'namaakun'=>$namaakun,
			'kredit'=>$kredit,
			'debet'=>$debet,
			'ket'=>$ket,
			'posisi'=>$posisi,
		);
		if ($iddet){
			$this->db->where('id',$iddet);
			$result = $this->db->update('tvalidasi_jurnal_umum_detail',$data);			
		}else{
			$result = $this->db->insert('tvalidasi_jurnal_umum_detail',$data);			
		}
		$this->output->set_output(json_encode($result));
	}
	function hapus_jurnal($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('tvalidasi_jurnal_umum_detail');
		echo json_encode($result);
	}
	function hapus_index()
	{
		$id=$this->input->post('id');
		$data=array(
			'deleted_by' => $this->session->userdata('user_id'), 
			'deleted_nama' => $this->session->userdata('user_name'), 
			'deleted_date' => date("Y-m-d H:i:s"), 
			'status' =>0,
		);
		$this->db->where('id',$id);		
		$result =$this->db->update('tvalidasi_jurnal_umum',$data);
		echo json_encode($result);
	}
	function load_jurnal(){
		
		$idvalidasi=$this->input->post('id');
		$disabel=$this->input->post('disabel');
		$q="
				SELECT  
				H.id,H.idvalidasi,H.idakun,H.namaakun,H.noakun,H.debet,H.kredit,H.ket
				FROM tvalidasi_jurnal_umum_detail H
				WHERE H.idvalidasi='$idvalidasi'
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total_D=0;
		$total_K=0;
		foreach($rows as $r){
			$no++;
			$total_D=$total_D + $r->debet;
			$total_K=$total_K + $r->kredit;
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'<input class="form-control idtrx" type="hidden" name="iddet[]" value="'.$r->id.'"></td>';
			$tbl .='<td class="text-left">'.$r->noakun.' - '.$r->namaakun.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->debet,2).'</td>';
			$tbl .='<td class="text-right">'.number_format($r->kredit,2).'</td>';
			$tbl .='<td class="text-center">'.$r->ket.'</td>';
			$aksi       = '<div class="btn-group">';
			$aksi 		.= '<button type="button" '.$disabel.' class="btn btn-xs btn-success edit"><i class="fa fa-pencil"></i></button>';				
			$aksi 		.= '<button type="button" '.$disabel.' class="btn btn-xs btn-danger" onclick="hapus_jurnal('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';
			$aksi .='</div>';
			$tbl .='<td>'.$aksi.'</td>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<td colspan="2" class="text-right"><strong>TOTAL</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_D,2).'</strong><input class="form-control" type="hidden" id="total_debet" name="total_debet" value="'.$total_D.'"></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_K,2).'</strong><input class="form-control" type="hidden" id="total_kredit" name="total_kredit" value="'.$total_K.'"></td>';
		$tbl .='<td class="text-right"></td>';
		
		$tbl .='</tr>';
		
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function save(){
		// print_r($this->input->post());exit();
		if($this->input->post('id') == '' ) {
			$id=$this->model->saveData();
			if($id){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tvalidasi_jurnal_umum/create/'.$id,'location');
			}
		} else {
			if($this->model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tvalidasi_jurnal_umum','location');
			}
		}

	}
	public function upload_files()
    {
        $files = array();
        foreach ($_FILES as $file) {
            if ($file['name'] != '') {
                $config['upload_path'] = './assets/upload/tvalidasi_jurnal_umum/';
                $config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';

                $files[] = $file['name'];
				$new_name = time().'-'.$file['name'];
				$config['file_name'] = $new_name;
                $this->upload->initialize($config);

                if ($this->upload->do_upload('file')) {
                    $image_upload = $this->upload->data();
                    $table = 'tvalidasi_jurnal_umum_dokumen';

                    $data = array();
                    $data['idvalidasi']  = $this->input->post('idvalidasi');
                    // $data['keterangan']  = $this->input->post('keterangan');
                    $data['upload_by']  = $this->session->userdata('user_id');
                    $data['upload_by_nama']  = $this->session->userdata('user_name');
                    $data['upload_date']  = date('Y-m-d H:i:s');
                    $data['filename'] = $image_upload['file_name'];
                    $data['size'] = formatSizeUnits($file['size']);

                    $this->db->insert($table, $data);

                    return true;
                } else {
                    print_r($this->upload->display_errors());
                    exit();

                    return false;
                }
            } else {
                return true;
            }
        }
    }
	function refresh_image($id){		
		$arr['detail'] = $this->model->refresh_image($id);
		$this->output->set_output(json_encode($arr));
	}
	function removeFile(){
		$id=$this->input->post('id');
		$this->db->where('id', $id);
        $query = $this->db->get('tvalidasi_jurnal_umum_dokumen');
        $row = $query->row();
		if(file_exists('./assets/upload/tvalidasi_jurnal_umum/'.$row->filename) && $row->filename !='') {
			unlink('./assets/upload/tvalidasi_jurnal_umum/'.$row->filename);
		}else{
			
		}
		$result=$this->db->query("delete from tvalidasi_jurnal_umum_dokumen WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	public function get_transaksi_detail($iddet)
    {
        $q="SELECT B.nama as nama_barang,D.* FROM `tgudang_penerimaan_detail` D
			LEFT JOIN view_barang_all B ON B.id=D.idbarang AND B.idkelompokpasien=D.idkelompokpasien
			WHERE D.id='$iddet'";
       
        $result=$this->db->query($q)->row_array();
        $this->output->set_output(json_encode($result));
    }
	function detail($id,$disabel='') {
		$data=$this->model->getHeader($id);
		$data['error'] 			= '';
		$data['disabel'] 			= $disabel;
		// $data['list_rekanan'] 	= $this->model->list_rekanan();
		// print_r($data['list_rekanan']);exit();
		$data['list_kp'] 	= $this->model->list_kp();
		$data['title'] 			= 'Detail Validasi Jurnal  Umum';
		$data['content'] 		= 'Tvalidasi_jurnal_umum/detail';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Jurnal Umum",'tvalidasi_jurnal_umum/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        $userid = $this->session->userdata('user_id');
		// $batas_batal=$this->db->query("SELECT batas_batal FROM msetting_jurnal_jurnal_umum WHERE id='1'")->row('batas_batal');
		
		$nojurnal=$this->input->post('nojurnal');
		$userid=$this->input->post('userid');
		$tipe=$this->input->post('tipe');
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		$st_posting=$this->input->post('st_posting');
		$where='';
		
		
		
		if ($st_posting !='#'){
			$where .=" AND H.st_posting='$st_posting' ";
		}
		
		if ($nojurnal !=''){
			$where .=" AND H.nojurnal='$nojurnal' ";
		}
		if ($tipe !='#'){
			$where .=" AND H.tipe='$tipe' ";
		}
		if ($userid !='#'){
			$where .=" AND H.created_by='$userid' ";
		}
				
		
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(H.tanggal_transaksi) >='".YMDFormat($tanggal_trx1)."' AND DATE(H.tanggal_transaksi) <='".YMDFormat($tanggal_trx2)."'";
        }
        $from = "(
					SELECT H.id,H.nojurnal,H.tanggal_transaksi,H.tipe,H.jenis,IFNULL(H.debet,0) as debet,IFNULL(H.kredit,0) as kredit,H.keterangan
					,H.created_by,H.created_nama,R.nama as tipe_nama,H.st_posting,CASE WHEN H.tipe='3' THEN J.nama ELSE '' END as jenis_nama,H.posting_date
					FROM tvalidasi_jurnal_umum H
					LEFT JOIN ref_trx_jurnal_umum R ON R.id=H.tipe
					LEFT JOIN msetting_jurnal_umum_rekon J ON J.id=H.jenis
					WHERE H.status='1' ".$where."
					ORDER BY H.tanggal_transaksi DESC,H.id DESC
				) as tbl";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nojurnal','tipe_nama');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

			
        foreach ($list as $r) {
            $no++;
            $row = array();
			$siap_posting='0';
			if (($r->debet + $r->kredit) > 0){
				if ($r->debet==$r->kredit){
					$siap_posting=1;
				}
			}
			$disabel_btn=$this->get_batas($r->posting_date,$r->tipe,$r->jenis);
			$disabel_posting=($siap_posting!='1'?'disabled':'');
			
			// if ($r->umur_posting>$batas_batal){
				// $disabel_btn='disabled';
			// }
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url_klaim        = site_url('tpendapatan/');
            $url       = site_url('tsetoran_kas/');
            $url_validasi        = site_url('tvalidasi_jurnal_umum/');
            $url_batal       = site_url('tgudang_pengembalian/');
            $row[] = $r->id;
            $row[] = $r->st_posting;
            $row[] = $no;
            $row[] = $r->nojurnal;
            $row[] = HumanDateShort($r->tanggal_transaksi);
            $row[] = tipe_jurnal($r->tipe,$r->tipe_nama).($r->tipe=='3'?'<br>'.$r->jenis_nama:'');
            $row[] = $r->keterangan;
            $row[] = number_format($r->debet,2);
            $row[] = number_format($r->kredit,2);
            $row[] = $r->created_nama;
           
            $row[] = ($r->st_posting=='1'?text_success('TELAH DIPOSTING'):text_default('MENUGGU DIPOSTING'));
					$aksi .= '<a href="'.$url_validasi.'create/'.$r->id.'/disabled" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
					if ($r->st_posting=='0'){
					$aksi .= '<a href="'.$url_validasi.'create/'.$r->id.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>';
					}
					if ($r->st_posting=='1'){
						$aksi .= '<button title="Batal Posting" '.$disabel_btn.' class="btn btn-xs btn-danger batalkan"><i class="fa fa-close"></i></button>';						
					}else{
						$aksi .= '<button title="Posting" '.$disabel_posting.' class="btn btn-xs btn-success posting"><i class="si si-arrow-right"></i></button>';						
					}
					if ($r->st_posting=='0'){
						$aksi .= '<button title="Detail" class="btn btn-xs btn-danger" onclick="hapus_index('.$r->id.')"><i class="fa fa-trash-o"></i></button>';
					}
					// $aksi .= '<a href="'.$url_validasi.'print_kwitansi_deposit/'.$r->id.'" target="_blank" title="Print Kwitansi" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';
					$aksi .= '<button title="Print" class="btn btn-xs btn-success"><i class="fa fa-print"></i></button>';
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	public function get_batas($posting_date,$tipe,$jenis){
		$hasil='';
		if ($tipe=='3'){
			$hasil=$this->db->query("SELECT IF(S.batas < DATEDIFF(NOW(),'$posting_date'),'disabled','') as hasil FROM msetting_jurnal_umum_rekon S WHERE S.id='$jenis'")->row('hasil');
		}else{
			$row=$this->db->query("SELECT S.batas_batal_dep,S.batas_batal_pen,DATEDIFF(NOW(),'$posting_date') lama FROM msetting_jurnal_umum S")->row();
			if ($tipe=='1'){
				if ($row->batas_batal_dep < $row->lama){
					$hasil='disabled';
				}
			}
			if ($tipe=='2'){
				if ($row->batas_batal_pen < $row->lama){
					$hasil='disabled';
				}
			}
		}
		return $hasil;
	}
	
	public function posting()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'1',
			'posting_by'=>$this->session->userdata('user_id'),
			'posting_nama'=>$this->session->userdata('user_name'),
            'posting_date'=>date('Y-m-d H:i:s')
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_jurnal_umum', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function posting_all()
    {
        $id = $this->input->post('id');
		foreach($id as $index=>$val){
			 $data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $val);
			$result=$this->db->update('tvalidasi_jurnal_umum', $data);
		}
       
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	
	public function batalkan()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'0',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_jurnal_umum', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	
	function save_detail(){
		// print_r($this->input->post());exit();
		// $id=$this->model->saveData();
		if($this->model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tvalidasi_jurnal_umum','location');
		}
	
	}
	function load_detail(){
		
		$id=$this->input->post('id');
		$q="SELECT *from makun_nomor A ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT *FROM tvalidasi_jurnal_umum H
				WHERE H.id='$id'
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			
			
			$aksi       = '<div class="btn-group">';
			$select_setor='<select name="idakun_tunai[]" '.$disabel.'  class="js-select2 form-control opsi" style="width: 100%;">
							'.$this->list_akun($list,$r->idakun_tunai).'
						</select>';
			
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td><input type="hidden" name="iddet[]" value="'.$r->id.'">';
			$tbl .='<td class="text-right">'.number_format($r->nominal,2).'</td>';
			$tbl .='<td>'.$select_setor.'</td>';
			$url       = site_url('tsetoran_kas/');
			$aksi .= '<a href="'.$url.'update/'.$r->idtransaksi.'/disabled" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
				// $aksi .= '<button type="button" title="Lihat Detail" class="btn btn-default btn-sm " onclick="get_transaksi_detail('.$r->id.')"><i class="fa fa-eye"></i></button>';
			$tbl .='<td class="text-center">'.$aksi.'</td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function get_edit(){
		
		$id=$this->input->post('id');
		
        $q = "SELECT *FROM tvalidasi_jurnal_umum_detail H
				WHERE H.id='$id'
				";
		$rows=$this->db->query($q)->row_array();
		
		$this->output->set_output(json_encode($rows));
    }
	
	function load_ringkasan(){
		
		$id=$this->input->post('id');
		
		$disabel=$this->input->post('disabel');
		
        $q = "SELECT T.noakun,T.namaakun,SUM(T.debet) debet,SUM(T.kredit) kredit FROM(
					SELECT 
					H.tanggal_transaksi as tanggal,H.idtransaksi as ref_tabel_id,'tsetoran_kas' as ref_tabel
					,'tvalidasi_jurnal_umum' as ref_tabel_validasi,H.id as idvalidasi,H.notransaksi,H.idakun_tunai as idakun
					,A.noakun,A.namaakun
					,SUM(CASE WHEN H.posisi_akun='D' THEN H.nominal ELSE 0 END) debet
					,SUM(CASE WHEN H.posisi_akun='K' THEN H.nominal ELSE 0 END) kredit
					,CONCAT('SETORAN KAS ',H.notransaksi,' | ',DATE_FORMAT(H.periode_transaksi,'%d-%m-%Y')) as keterangan,CASE WHEN H.st_auto_posting='1' THEN 'AUTO POSTING' ELSE 'MANUAL' END as keterangan_code
					,H.created_by,H.created_date,H.created_nama,H.posisi_akun
					FROM tvalidasi_jurnal_umum H
					LEFT JOIN makun_nomor A ON A.id=H.idakun_tunai
					WHERE H.id='$id' AND H.nominal > 0
					GROUP BY H.id,H.idakun_tunai,H.posisi_akun

					UNION ALL

					SELECT 
					H.tanggal_transaksi as tanggal,H.idtransaksi as ref_tabel_id,'tsetoran_kas' as ref_tabel
					,'tvalidasi_jurnal_umum' as ref_tabel_validasi,H.id as idvalidasi,H.notransaksi,D.idakun as idakun
					,A.noakun,A.namaakun
					,SUM(CASE WHEN D.posisi_akun='D' THEN D.nominal_bayar ELSE 0 END) debet
					,SUM(CASE WHEN D.posisi_akun='K' THEN D.nominal_bayar ELSE 0 END) kredit
					,CONCAT('SETORAN KAS ',H.notransaksi,' | ',DATE_FORMAT(H.periode_transaksi,'%d-%m-%Y')) as keterangan,CASE WHEN H.st_auto_posting='1' THEN 'AUTO POSTING' ELSE 'MANUAL' END as keterangan_code
					,H.created_by,H.created_date,H.created_nama,D.posisi_akun
					FROM tvalidasi_jurnal_umum H
					LEFT JOIN tvalidasi_jurnal_umum_bayar D ON D.idvalidasi=H.id
					LEFT JOIN makun_nomor A ON A.id=D.idakun
					WHERE H.id='$id' AND D.nominal_bayar > 0
					GROUP BY H.id,D.idakun,D.posisi_akun
					) T WHERE (T.debet + T.kredit) > 0
					
					GROUP BY T.idakun,T.posisi_akun
					ORDER BY T.posisi_akun
					
				";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total_D=0;
		$total_K=0;
		foreach($rows as $r){
			$no++;
			$total_D=$total_D + $r->debet;
			$total_K=$total_K + $r->kredit;
			
			
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td class="text-right">'.$r->noakun.'</td>';
			$tbl .='<td>'.$r->namaakun.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->debet,2).'</td>';
			$tbl .='<td class="text-right">'.number_format($r->kredit,2).'</td>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<td colspan="3" class="text-right"><strong>TOTAL</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_D,2).'</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_K,2).'</strong></td>';
		
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function list_akun($list,$idakun){
		$hasil='';
		foreach($list as $row){
			$hasil .='<option value="'.$row->id.'" '.($row->id==$idakun?"selected":"").'>'.$row->noakun.' - '.$row->namaakun.'</option>';
		}
		return $hasil;
	}
}
