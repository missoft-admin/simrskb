<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpendaftaran_poli_ttv extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Tpendaftaran_poli_model');
		$this->load->helper('path');
		
  }

  function get_logic_formulir($pendaftaran_id){
	  $q="SELECT H.*
		, (SELECT  H.tanggal FROM tpoliklinik_pendaftaran D WHERE D.idpasien=H.idpasien AND H.id!=D.id AND D.idpoliklinik=H.idpoliklinik ORDER BY D.tanggal DESC LIMIT 1) as tgl_akhir
		FROM tpoliklinik_pendaftaran H
		WHERE H.id='74342'
		GROUP BY H.id
		";
	  $data_pasien=$this->db->query($q)->row();
	  $hasil='#';
	  if ($data_pasien){
		  
	  }
	  echo $hasil;
  }
  function index($tab='1'){
		get_ppa_login();
		// print_r($this->session->userdata());exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// $q="SELECT H.id as login_mppa_id,H.pegawai_id as login_iddokter,H.pegawai_id as iddokter FROM mppa H WHERE H.user_id='".$this->session->userdata('user_id')."' AND H.tipepegawai='2' AND H.staktif='1'";
		// // $login_iddokter=$this->db->query($q)->row('login_iddokter');	
		// $data = $this->db->query($q)->row_array();	
		// if (){}
		if (UserAccesForm($user_acces_form,array('1597'))){
			// print_r($data);exit;	
			// $data['list_ruangan'] 			= $this->Tpendaftaran_poli_ttv_model->list_ruangan();
			$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
			$data['list_dokter'] 			= $this->Tpendaftaran_poli_ttv_model->list_dokter();
			$data['list_ruang'] 			= $this->Tpendaftaran_poli_ttv_model->list_ruang();
			
			$data['iddokter'] 			= '#';
			$data['ruangan_id'] 			= '#';
			$data['idpoli'] 			= '#';
			$data['namapasien'] 			= '';
			$data['tab'] 			= $tab;
			
			$data['tanggal_1'] 			= DMYFormat(date('2023-09-01'));
			$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
			$data['error'] 			= '';
			$data['title'] 			= 'My Pasien';
			$data['content'] 		= 'Tpendaftaran_poli_ttv/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Tindakan Poliklinik",'#'),
												  array("My Pasien",'tpendaftaran_poli_ttv')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function getIndex_all()	{
			$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
						
			$idpoli =$this->input->post('idpoli');
			$iddokter =$this->input->post('iddokter');
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$pencarian =$this->input->post('pencarian');
			$ruangan_id =$this->input->post('ruangan_id');
			$tab =$this->input->post('tab');
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(H.tanggal) >='".date('Y-m-d')."'";
			}
			if ($pencarian!=''){
				$where .=" AND (H.namapasien LIKE '%".$pencarian."%')";
				$where .=" OR (H.no_medrec LIKE '%".$pencarian."%')";
			}
			if ($tab=='2'){
				$where .=" AND (H.status_input_ttv) = '0'";
			}
			if ($tab=='3'){
				$where .=" AND (H.status_input_ttv) = '1'";
			}
			if ($idpoli!='#'){
				$where .=" AND (H.idpoliklinik) = '$idpoli'";
			}
			if ($iddokter!='#'){
				$where .=" AND (H.iddokter) = '$iddokter'";
			}
			if ($ruangan_id!='#'){
				$where .=" AND (H.ruangan_id) = '$ruangan_id'";
			}
		
			// print_r($tab);exit;
			$this->select = array();
			$from="
					(
						SELECT 
							TP.suhu,TP.nadi,TP.nafas,TP.td_sistole,TP.td_diastole,TP.tinggi_badan,TP.berat_badan
							,mnadi.warna as warna_nadi
							,mnafas.warna as warna_nafas
							,mtd_sistole.warna as warna_sistole
							,mtd_diastole.warna as warna_diastole
							,msuhu.warna as warna_suhu
							,TP.berat_badan/((TP.tinggi_badan/100)*2) as masa_tubuh,mberat.warna as warna_berat
							,MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
							,MK.nama as nama_kelompok,MR.nama as nama_rekanan
							,H.*
							,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header 
							FROM tpoliklinik_pendaftaran H
							LEFT JOIN mdokter MD ON MD.id=H.iddokter
							LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
							LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
							LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
							LEFT JOIN (
								".get_alergi_sql()."
							) A ON A.idpasien=H.idpasien
							LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
							LEFT JOIN (SELECT * FROM (SELECT * FROM tpoliklinik_ttv WHERE status_ttv > 1 ORDER BY tanggal_input DESC) XX GROUP BY pendaftaran_id) TP ON TP.pendaftaran_id = H.id
							LEFT JOIN mnadi ON TP.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
							LEFT JOIN mnafas ON TP.nafas BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
							LEFT JOIN mtd_sistole ON TP.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
							LEFT JOIN mtd_diastole ON TP.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
							LEFT JOIN msuhu ON TP.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
							LEFT JOIN mberat ON TP.berat_badan/((TP.tinggi_badan/100)*2) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'

							WHERE H.tanggal IS NOT NULL  ".$where."
							GROUP BY H.id
						ORDER BY H.tanggal ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter','namapasien','no_medrec');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  // tpendaftaran_poli_ttv/tindakan/74329/erm_rj/input_ttv
		  $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="push-5-t"><a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->id.'/erm_rj/input_ttv" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-block btn-primary btn-xs menu_click"><i class="fa fa-user-md"></i> LAYANI</a> </div>
									<div class="push-5-t">
										<div class="btn-group btn-block">
											<div class="btn-group">
												<button class="btn btn-warning btn-block btn-xs dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
													<span class="fa fa-caret-down"></span> PANEL KENDALI
												</button>
												<ul class="dropdown-menu">
													<li class="dropdown-header">Rawat Jalan</li>
													<li>
														<a tabindex="-1" href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->id.'/erm_rj/input_assesmen_rj"> Input Assesmen Perawatan RJ</a>
													</li>
													<li>
														<a tabindex="-1" href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->id.'/erm_rj/input_asmed_rj"> Input Assesmen Medis RJ</a>
													</li>
													<li>
														<a tabindex="-1" href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->id.'/erm_rj/input_nyeri_rj"> Input Pengkajian Nyeri RJ</a>
													</li>
													<li>
														<a tabindex="-1" href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->id.'/erm_rj/input_triage"> Input Triage</a>
													</li>
													<li class="divider"></li>
													<li class="dropdown-header">IGD</li>
													<li>
														<a tabindex="-1" href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->id.'/erm_rj/input_asmed_igd"> Input Assesmen Medis IGD</a>
													</li>
													<li>
														<a tabindex="-1" href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->id.'/erm_rj/input_assesmen_igd"> Input Assesmen IGD</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="push-5-t"><button type="button" data-toggle="tooltip" title="Call"  class="btn btn-block btn-success btn-xs"><i class="fa fa-save"></i> FINAL TRANSAKSI</button> </div>
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->title.'. '.$r->namapasien.' - '.$r->no_medrec.'</div>
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
										<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
									</div>
									
									</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  $btn_5='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Suhu Tubuh :</div>
									<div class=" text-muted " style="color:'.$r->warna_suhu.'"> '.($r->suhu).' '.$data_satuan_ttv['satuan_suhu'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frek Nadi :</div>
									<div class=" text-muted " style="color:'.$r->warna_nadi.'"> '.($r->nadi).' '.$data_satuan_ttv['satuan_nadi'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frek Nafas :</div>
									<div class=" text-muted " style="color:'.$r->warna_nafas.'"> '.($r->nafas).' '.$data_satuan_ttv['satuan_nafas'].'</div>
								</td>
								
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Tekanan Darah :</div>
									<div class=" text-muted "> <label  class="text-muted" style="color:'.$r->warna_sistole.'">'.($r->td_sistole.' '.$data_satuan_ttv['satuan_td'].'</label> / <label class="text-muted " style="color:'.$r->warna_diastole.'">'.$r->td_diastole).' '.$data_satuan_ttv['satuan_td'].'</label></div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Berat Badan :</div>
									<div class=" text-muted " style="color:'.$r->warna_berat.'"> '.($r->berat_badan).' Kg</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Tinggi Badan :</div>
									<div class=" text-muted style="color:'.$r->warna_berat.'""> '.($r->tinggi_badan).' Cm</div>
								</td>
								
							</tr>
						</tbody>
					</table>';
		  
          $result[] = $btn_5;
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO</div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class="text-muted text-uppercase">'.GetAsalRujukanR($r->idtipe).' - '.$r->nama_poli.'</div>
									<div class="text-muted"><i class="fa fa-user-md"></i> '.($r->nama_dokter).'</div>
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->nama_rekanan?$r->nama_rekanan:$r->nama_kelompok).'</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> '.($r->status_input_ttv?text_success('TELAH DIPERIKSA'):text_warning('BELUM DIPERIKSA')).'</div>
									<div class="h4 push-10-t text-primary"><strong><i class="fa fa-stethoscope"></i>  '.HumanDateShort($r->tanggal).'</strong></div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function tindakan($pendaftaran_id='',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		get_ppa_login();
		// print_r($this->session->userdata());exit;
		// $setting_cppt_field=$this->Tpendaftaran_poli_ttv_model->setting_cppt_field();
		// print_r($setting_cppt_field);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data_satuan_ttv=array();
		$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan,makral_satuan,mcahaya_satuan,mgcs_eye_satuan,mgcs_motion_satuan,mgcs_voice_satuan,mpupil_satuan,mspo2_satuan,mgds_satuan")->row_array();
		// $data=array();
		$data=$this->Tpendaftaran_poli_ttv_model->get_data_header($pendaftaran_id);
		
		
		// $data=$this->db->query($q)->row_array();
		// print_r($data);exit;
		$data['st_cetak']=0;
		$data['ttd']='';
		if ($data['idtipe']=='1'){
			$data_header_ttv=$this->Tpendaftaran_poli_ttv_model->get_header_ttv($pendaftaran_id);
			
		}else{
			$data_header_ttv=$this->Tpendaftaran_poli_ttv_model->get_header_ttv_igd($data['assesmen_id']);
			
		}
		
		$data_login_ppa=get_ppa_login();
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		// $data['array_menu_kiri'] 	= $this->Tpendaftaran_poli_ttv_model->list_menu_kiri($menu_atas);
		// $data['array_menu_kiri'] 	= array('input_ttv','input_data');
		
		// print_r(json_encode($data['array_menu_kiri']));exit;
		$data['idpoli'] 			= '#';
		
		$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
		$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
		$data['assesmen_id'] 			= '';
		$data['status_assesmen'] 			= '1';
		$data['error'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'My Pasien';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Tindakan Poliklinik",'#'),
											  array("My Pasien",'tpendaftaran_poli_ttv')
											);
		$logic_akses_ttv=$this->Tpendaftaran_poli_ttv_model->logic_akses_ttv($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_assemen_rj=$this->Tpendaftaran_poli_ttv_model->logic_akses_assemen_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_asmed=$this->Tpendaftaran_poli_ttv_model->logic_akses_asmed_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_asmed_igd=$this->Tpendaftaran_poli_ttv_model->logic_akses_asmed_igd($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_nyeri=$this->Tpendaftaran_poli_ttv_model->logic_akses_nyeri_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_triage=$this->Tpendaftaran_poli_ttv_model->logic_akses_triage_rj($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_assesmen_igd=$this->Tpendaftaran_poli_ttv_model->logic_akses_assesmen_igd($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_cppt=$this->Tpendaftaran_poli_ttv_model->logic_akses_cppt($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		$logic_akses_ic=$this->Tpendaftaran_poli_ttv_model->logic_akses_ic($data_login_ppa['login_ppa_id'],$data_login_ppa['login_spesialisasi_id'],$data_login_ppa['login_profesi_id']);
		// print_r($logic_akses_assemen_rj);exit;
		
		if ($menu_kiri=='input_ttv'){
			$data_ttv=$this->Tpendaftaran_poli_ttv_model->get_data_ttv($pendaftaran_id);
			$setting_ttv=$this->Tpendaftaran_poli_ttv_model->setting_ttv();
			$data = array_merge($data_ttv,$data,$setting_ttv);
		}
		
		if ($menu_kiri=='his_assesmen_rj'){
			$data_assemen['riwayat_alergi']=$data['riwayat_alergi_header'];
			// print_r($data);exit;
		}
		
		if ($menu_kiri=='input_assesmen_rj' || $menu_kiri=='his_assesmen_rj'){
			
			if ($trx_id!='#'){
				if ($menu_kiri=='his_assesmen_rj'){
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_assesmen_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpendaftaran_poli_ttv_model->get_data_assesmen_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_assesmen_trx($trx_id);
				// print_r($data_assemen);exit;
				}
				
			}else{
				$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_assesmen($pendaftaran_id);
				
				
			}
			if ($data_assemen){
				
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['ttd_nama']='';
				$data_assemen['ttd_hubungan']='';
				$data_assemen['jenis_ttd']='1';
				
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			if ($menu_kiri=='his_assesmen_rj'){
				$data_ttv=$this->Tpendaftaran_poli_ttv_model->get_data_ttv_assesmen_trx($data_assemen['assesmen_id'],$versi_edit);
				$data_ttv_2=$this->Tpendaftaran_poli_ttv_model->get_data_ttv_assesmen_trx_last($data_assemen['assesmen_id'],$versi_edit);
				// $list_edukasi=$this->Tpendaftaran_poli_ttv_model->list_edukasi_his($data_assemen['assesmen_id'],$versi_edit);
				$data_edukasi=$this->Tpendaftaran_poli_ttv_model->perubahan_edukasi($data_assemen['assesmen_id'],$versi_edit);
				$data_ttv = array_merge($data_ttv,$data_ttv_2,$data_edukasi);
				
				
			}else{
				$data_ttv=$this->Tpendaftaran_poli_ttv_model->get_data_ttv_assesmen($data_assemen['assesmen_id']);
				$data_assemen['riwayat_alergi']=$data['riwayat_alergi_header'];
				$list_edukasi=$this->Tpendaftaran_poli_ttv_model->list_edukasi($data_assemen['assesmen_id']);
				$data_assemen['list_edukasi']=$list_edukasi;
				$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
				$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
				$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			}
			$data_assemen['template_id']=$this->db->query("SELECT id FROM `mrisiko_jatuh` WHERE idtipe='1' AND staktif='1' LIMIT 1")->row('id');
			$list_riwayat_penyakit=$this->Tpendaftaran_poli_ttv_model->list_riwayat_penyakit($data_assemen['assesmen_id']);
			$list_template_assement=$this->Tpendaftaran_poli_ttv_model->list_template_assement();
			$data_assemen['list_template_assement']=$list_template_assement;
			$data_assemen['list_riwayat_penyakit']=$list_riwayat_penyakit;
			// print_r($list_edukasi);exit;
			
			$setting_assesmen=$this->Tpendaftaran_poli_ttv_model->setting_assesmen();
			$data = array_merge($data,$setting_assesmen,$data_ttv,$data_assemen);
			$data['pendaftaran_id']=$pendaftaran_id;
		}
		//ASMED
		if ($menu_kiri=='input_asmed_rj' || $menu_kiri=='his_asmed_rj'){
			$data_warna=$this->Tpendaftaran_poli_ttv_model->get_default_warna_asmed();
			
			if ($trx_id!='#'){
				if ($menu_kiri=='his_asmed_rj'){
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_asmed_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpendaftaran_poli_ttv_model->get_data_asmed_trx_asal($trx_id,$versi_edit);
					// print_r($data_assemen_asal);exit;
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
				}else{
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_asmed_trx($trx_id);
				}
				
			}else{
				$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_asmed($pendaftaran_id);
				
				
			}
				// print_r($data_assemen);exit;
			if ($data_assemen){
				
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['ttd_nama']='';
				$data_assemen['ttd_hubungan']='';
				$data_assemen['jenis_ttd']='1';
				$data_assemen['tanggal_kontrol']='';
				$data_assemen['gambar_id']='';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			if ($menu_kiri=='his_asmed_rj'){
				$list_edukasi=$this->Tpendaftaran_poli_ttv_model->list_edukasi_asmed_his($data_assemen['assesmen_id'],$versi_edit);
				$data_assemen['list_edukasi']=$list_edukasi;
				
				$list_riwayat_penyakit=$this->Tpendaftaran_poli_ttv_model->list_riwayat_penyakit_asmed($data_assemen['assesmen_id'],1,$versi_edit);
				$list_riwayat_penyakit_dahulu=$this->Tpendaftaran_poli_ttv_model->list_riwayat_penyakit_asmed($data_assemen['assesmen_id'],2,$versi_edit);
				$list_riwayat_penyakit_keluarga=$this->Tpendaftaran_poli_ttv_model->list_riwayat_penyakit_asmed($data_assemen['assesmen_id'],3,$versi_edit);
				$get_lokalis=$this->Tpendaftaran_poli_ttv_model->get_lokalis($data_assemen['assesmen_id'],$data_assemen['gambar_id'],$versi_edit);
			}else{
				// $data_ttv=$this->Tpendaftaran_poli_ttv_model->get_data_ttv_assesmen($data_assemen['assesmen_id']);
				$data_assemen['riwayat_alergi']=$data['riwayat_alergi_header'];
				$list_edukasi=$this->Tpendaftaran_poli_ttv_model->list_edukasi_asmed($data_assemen['assesmen_id']);
				$data_assemen['list_edukasi']=$list_edukasi;
				
				
				$list_riwayat_penyakit=$this->Tpendaftaran_poli_ttv_model->list_riwayat_penyakit_asmed($data_assemen['assesmen_id'],1,'#');
				$list_riwayat_penyakit_dahulu=$this->Tpendaftaran_poli_ttv_model->list_riwayat_penyakit_asmed($data_assemen['assesmen_id'],2,'#');
				$list_riwayat_penyakit_keluarga=$this->Tpendaftaran_poli_ttv_model->list_riwayat_penyakit_asmed($data_assemen['assesmen_id'],3,'#');
				$get_lokalis=$this->Tpendaftaran_poli_ttv_model->get_lokalis($data_assemen['assesmen_id'],$data_assemen['gambar_id'],'#');
			}
			$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
			$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
			$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			// print_r($get_lokalis);exit;
			$data_assemen['template_id']='';
			
			$list_template_assement=$this->Tpendaftaran_poli_ttv_model->list_template_asmed();
			
			$data_assemen['list_template_assement']=$list_template_assement;
			$data_assemen['list_riwayat_penyakit']=$list_riwayat_penyakit;
			$data_assemen['list_riwayat_penyakit_dahulu']=$list_riwayat_penyakit_dahulu;
			$data_assemen['list_riwayat_penyakit_keluarga']=$list_riwayat_penyakit_keluarga;
			$data_assemen['list_lokalis']=$get_lokalis;
			// print_r($get_lokalis);exit;
			
			$setting_assesmen=$this->Tpendaftaran_poli_ttv_model->setting_asmed();
			$data = array_merge($data,$setting_assesmen,$data_assemen,$data_warna);
			$data['pendaftaran_id']=$pendaftaran_id;
		}
		//NYERI
		if ($menu_kiri=='input_nyeri_rj' || $menu_kiri=='his_nyeri_rj'){
			$data['template_assesmen_id']='';
			if ($trx_id!='#'){
				if ($menu_kiri=='his_nyeri_rj'){
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_nyeri_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpendaftaran_poli_ttv_model->get_data_nyeri_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_nyeri_trx($trx_id);
				}
				
				// print_r($data_assemen);exit;
				
			}else{
				$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_nyeri($pendaftaran_id);
				
			}
			if ($data_assemen){
				if ($data_assemen['mnyeri_id']!='1'){
					// $q="SELECT H.id,D.skor,J.id as jawaban_id FROM `tpoliklinik_nyeri_param` H
					// LEFT JOIN mnyeri_param_skor D ON D.parameter_id=H.param_id AND D.st_default='1' AND D.staktif='1'
					// LEFT JOIN tpoliklinik_nyeri_param_skor J ON J.parameter_id=H.id AND J.skor=D.skor
					// WHERE H.assesmen_id='".$data_assemen['assesmen_id']."' AND H.jawaban_id IS NULL";
					$q="UPDATE tpoliklinik_nyeri_param H INNER JOIN (
					SELECT H.id,D.skor,J.id as jawaban_id FROM `tpoliklinik_nyeri_param` H
					LEFT JOIN mnyeri_param_skor D ON D.parameter_id=H.param_id AND D.st_default='1' AND D.staktif='1'
					LEFT JOIN tpoliklinik_nyeri_param_skor J ON J.parameter_id=H.id AND J.skor=D.skor
					WHERE H.assesmen_id='".$data_assemen['assesmen_id']."'
					) T ON T.id=H.id
					SET H.jawaban_id=T.jawaban_id
					WHERE H.jawaban_id IS NULL";
					// print_r($q);exit;
					$this->db->query($q);
				}
				
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['ttd_nama']='';
				$data_assemen['ttd_hubungan']='';
				$data_assemen['jenis_ttd']='1';
				$data_assemen['total_skor_nyeri']='0';
				$mnyeri_id=$this->Tpendaftaran_poli_ttv_model->default_logic_nyeri($data);
				$data_assemen['mnyeri_id']=$mnyeri_id;
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			if ($menu_kiri=='his_nyeri_rj'){
				
				
				
			}else{
				$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
				$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
				$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			}
			
			$setting_nyeri=$this->Tpendaftaran_poli_ttv_model->setting_nyeri();
			$setting_default_nyeri=$this->Tpendaftaran_poli_ttv_model->setting_default_nyeri();
			$data = array_merge($data,$data_assemen,$setting_nyeri,$setting_default_nyeri);
			$data['pendaftaran_id']=$pendaftaran_id;
		}
		
		//TRIAGE
		if ($menu_kiri=='input_triage' || $menu_kiri=='his_triage'){
			
			if ($trx_id!='#'){
				if ($menu_kiri=='his_triage'){
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_triage_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpendaftaran_poli_ttv_model->get_data_triage_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_triage_trx($trx_id);
				}
				
				// print_r($data_assemen);exit;
				
			}else{
				$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_triage($pendaftaran_id);
				
			}
			if ($data_assemen){
				
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['ttd_nama']='';
				$data_assemen['ttd_hubungan']='';
				$data_assemen['jenis_ttd']='1';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			if ($menu_kiri=='his_triage'){
				
				
				
			}else{
				// $data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
				// $data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
				// $data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			}
			$list_template_assement=$this->Tpendaftaran_poli_ttv_model->list_template_triage();
			
			$data['list_template_assement']=$list_template_assement;
			$setting_triage=$this->Tpendaftaran_poli_ttv_model->setting_triage();
			$data = array_merge($data,$data_assemen,$setting_triage);
			$data['pendaftaran_id']=$pendaftaran_id;
		}
		//ASMED IGD
		if ($menu_kiri=='input_asmed_igd' || $menu_kiri=='his_asmed_igd'){
			$data_warna=$this->Tpendaftaran_poli_ttv_model->get_default_warna_asmed();
			
			if ($trx_id!='#'){
				if ($menu_kiri=='his_asmed_igd'){
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_asmed_igd_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpendaftaran_poli_ttv_model->get_data_asmed_igd_trx_asal($trx_id,$versi_edit);
					// print_r($data_assemen_asal);exit;
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
				}else{
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_asmed_igd_trx($trx_id);
				// print_r($data_assemen);exit;
				}
				
			}else{
				$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_asmed_igd($pendaftaran_id);
				
				
			}
			if ($data_assemen){
				
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['ttd_nama']='';
				$data_assemen['ttd_hubungan']='';
				$data_assemen['jenis_ttd']='1';
				$data_assemen['tanggal_kontrol']='';
				$data_assemen['gambar_id']='';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			if ($menu_kiri=='his_asmed_igd'){
				// $data_ttv=$this->Tpendaftaran_poli_ttv_model->get_data_ttv_assesmen_trx($data_assemen['assesmen_id'],$versi_edit);
				// $data_ttv_2=$this->Tpendaftaran_poli_ttv_model->get_data_ttv_assesmen_trx_last($data_assemen['assesmen_id'],$versi_edit);
				$list_edukasi=$this->Tpendaftaran_poli_ttv_model->list_edukasi_asmed_igd_his($data_assemen['assesmen_id'],$versi_edit);
				// $data_edukasi=$this->Tpendaftaran_poli_ttv_model->perubahan_edukasi($data_assemen['assesmen_id'],$versi_edit);
				// $data_ttv = array_merge($data_ttv,$data_ttv_2,$data_edukasi);
				$data_assemen['list_edukasi']=$list_edukasi;
				$list_riwayat_penyakit=$this->Tpendaftaran_poli_ttv_model->list_riwayat_penyakit_asmed_igd($data_assemen['assesmen_id'],1,$versi_edit);
				$list_riwayat_penyakit_dahulu=$this->Tpendaftaran_poli_ttv_model->list_riwayat_penyakit_asmed_igd($data_assemen['assesmen_id'],2,$versi_edit);
				$list_riwayat_penyakit_keluarga=$this->Tpendaftaran_poli_ttv_model->list_riwayat_penyakit_asmed_igd($data_assemen['assesmen_id'],3,$versi_edit);
				$get_lokalis=$this->Tpendaftaran_poli_ttv_model->get_lokalis_igd($data_assemen['assesmen_id'],$data_assemen['gambar_id'],$versi_edit);
			}else{
				// $data_ttv=$this->Tpendaftaran_poli_ttv_model->get_data_ttv_assesmen($data_assemen['assesmen_id']);
				$data_assemen['riwayat_alergi']=$data['riwayat_alergi_header'];
				$list_edukasi=$this->Tpendaftaran_poli_ttv_model->list_edukasi_asmed_igd($data_assemen['assesmen_id']);
				$data_assemen['list_edukasi']=$list_edukasi;
				
				
				$list_riwayat_penyakit=$this->Tpendaftaran_poli_ttv_model->list_riwayat_penyakit_asmed_igd($data_assemen['assesmen_id'],1,'#');
				$list_riwayat_penyakit_dahulu=$this->Tpendaftaran_poli_ttv_model->list_riwayat_penyakit_asmed_igd($data_assemen['assesmen_id'],2,'#');
				$list_riwayat_penyakit_keluarga=$this->Tpendaftaran_poli_ttv_model->list_riwayat_penyakit_asmed_igd($data_assemen['assesmen_id'],3,'#');
				$get_lokalis=$this->Tpendaftaran_poli_ttv_model->get_lokalis_igd($data_assemen['assesmen_id'],$data_assemen['gambar_id'],'#');
			}
			$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
			$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
			$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			// print_r($get_lokalis);exit;
			// $data_assemen['template_id']=$this->db->query("SELECT id FROM `mrisiko_jatuh` WHERE idtipe='1' AND staktif='1' LIMIT 1")->row('id');
			
			$list_template_assement=$this->Tpendaftaran_poli_ttv_model->list_template_asmed_igd();
			
			$data_assemen['list_template_assement']=$list_template_assement;
			$data_assemen['list_riwayat_penyakit']=$list_riwayat_penyakit;
			$data_assemen['list_riwayat_penyakit_dahulu']=$list_riwayat_penyakit_dahulu;
			$data_assemen['list_riwayat_penyakit_keluarga']=$list_riwayat_penyakit_keluarga;
			$data_assemen['list_lokalis']=$get_lokalis;
			// print_r($get_lokalis);exit;
			
			$setting_assesmen=$this->Tpendaftaran_poli_ttv_model->setting_asmed_igd();
			$data = array_merge($data,$setting_assesmen,$data_assemen,$data_warna);
			$data['pendaftaran_id']=$pendaftaran_id;
		}
		
		//ASSESMEN IGD
		if ($menu_kiri=='input_assesmen_igd' || $menu_kiri=='his_assesmen_igd'){
			
			if ($trx_id!='#'){
				if ($menu_kiri=='his_assesmen_igd'){
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_assesmen_igd_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpendaftaran_poli_ttv_model->get_data_assesmen_igd_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_assesmen_igd_trx($trx_id);
				}
				
			}else{
				$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_assesmen_igd($pendaftaran_id);
				
				
			}
				// print_r($data_assemen);exit;
			if ($data_assemen){
				
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['ttd_nama']='';
				$data_assemen['ttd_hubungan']='';
				$data_assemen['jenis_ttd']='1';
				
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			if ($menu_kiri=='his_assesmen_igd'){
				$data_ttv=$this->Tpendaftaran_poli_ttv_model->get_data_ttv_assesmen_trx($data_assemen['assesmen_id'],$versi_edit);
				$data_ttv_2=$this->Tpendaftaran_poli_ttv_model->get_data_ttv_assesmen_trx_last($data_assemen['assesmen_id'],$versi_edit);
				$data_edukasi=$this->Tpendaftaran_poli_ttv_model->perubahan_edukasi_assesmen_igd($data_assemen['assesmen_id'],$versi_edit);
				$data_ttv = array_merge($data_ttv,$data_ttv_2,$data_edukasi);
				$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
				$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
				$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
				
			}else{
				$data_ttv=array();;
				$data_assemen['riwayat_alergi']=$data['riwayat_alergi_header'];
				$list_edukasi=$this->Tpendaftaran_poli_ttv_model->list_edukasi_assesmen_igd($data_assemen['assesmen_id']);
				$data_assemen['list_edukasi']=$list_edukasi;
				$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
				$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
				$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			}
			// $data_assemen['template_id']=$this->db->query("SELECT id FROM `mrisiko_jatuh` WHERE idtipe='1' AND staktif='1' LIMIT 1")->row('id');
			$list_riwayat_penyakit=$this->Tpendaftaran_poli_ttv_model->list_riwayat_penyakit_assesmen_igd($data_assemen['assesmen_id']);
			$list_template_assement=$this->Tpendaftaran_poli_ttv_model->list_template_assement_igd();
			$data_assemen['list_template_assement']=$list_template_assement;
			$data_assemen['list_riwayat_penyakit']=$list_riwayat_penyakit;
			// print_r($list_edukasi);exit;
			
			$setting_assesmen=$this->Tpendaftaran_poli_ttv_model->setting_assesmen_igd();
			$data = array_merge($data,$setting_assesmen,$data_ttv,$data_assemen);
			$data['pendaftaran_id']=$pendaftaran_id;
		}
		
		//CPPT
		if ($menu_kiri=='input_cppt' || $menu_kiri=='his_cppt'){
			$data['template_assesmen_id']='';
			if ($trx_id!='#'){
				if ($menu_kiri=='his_cppt'){
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_cppt_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpendaftaran_poli_ttv_model->get_data_cppt_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_cppt_trx($trx_id);
				}
				
				// print_r($data_assemen);exit;
				
			}else{
				$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_cppt($pendaftaran_id);
			// print_r($data_assemen);exit;
				
			}
			if ($data_assemen){
				
				
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['ttd_nama']='';
				$data_assemen['ttd_hubungan']='';
				$data_assemen['jenis_ttd']='1';
				$data_assemen['dokter_pjb']='';
				$data_assemen['profesi_id']='';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			if ($menu_kiri=='his_cppt'){
				
				
				
			}else{
				$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
				$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
				$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			}
			$list_template_assement=$this->Tpendaftaran_poli_ttv_model->list_template_cppt();
			$data_assemen['list_template_assement']=$list_template_assement;
			
			$setting_cppt=$this->Tpendaftaran_poli_ttv_model->setting_cppt();
			$setting_cppt_field=$this->Tpendaftaran_poli_ttv_model->setting_cppt_field($data_login_ppa['login_profesi_id']);
			// print_r($setting_cppt_field);exit;
			$jml_verif=0;
			$login_pegawai_id=$this->session->userdata('login_pegawai_id');
			if ($login_profesi_id='1'){				
				$q="SELECT COUNT(H.assesmen_id) as jml_verif FROM tpoliklinik_cppt H
					WHERE H.dokter_pjb='$login_pegawai_id' AND H.st_verifikasi='0' AND H.status_assemen='2'";
				$jml_verif=$this->db->query($q)->row('jml_verif');
			}
			$data['jml_verif']=$jml_verif;
			$data = array_merge($data,$data_assemen,$setting_cppt,$setting_cppt_field);
			$data['pendaftaran_id']=$pendaftaran_id;
		}

		//IC
		if ($menu_kiri=='input_ic' || $menu_kiri=='his_ic'){
			$data['template_assesmen_id']='';
			if ($trx_id!='#'){
				if ($menu_kiri=='his_ic'){
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_ic_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tpendaftaran_poli_ttv_model->get_data_ic_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_ic_trx($trx_id);
				}
				
				// print_r($data_assemen);exit;
				
			}else{
				$data_assemen=$this->Tpendaftaran_poli_ttv_model->get_data_ic($pendaftaran_id);
			// print_r($data_assemen);exit;
				
			}
			if ($data_assemen){
				
				
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['ttd_nama']='';
				$data_assemen['ttd_hubungan']='';
				$data_assemen['jenis_ttd']='1';
				$data_assemen['dokter_pjb']='';
				$data_assemen['profesi_id']='';
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			if ($menu_kiri=='his_ic'){
				
				
				
			}else{
				$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
				$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
				$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			}
			$list_template_assement=$this->Tpendaftaran_poli_ttv_model->list_template_ic();
			$data_assemen['list_template_assement']=$list_template_assement;
			
			$setting_ic=$this->Tpendaftaran_poli_ttv_model->setting_ic();
			$get_user_pelaksana_logic_ic=$this->Tpendaftaran_poli_ttv_model->get_user_pelaksana_logic_ic();
			// print_r($get_user_pelaksana_logic_ic);exit;
			$data = array_merge($data,$data_assemen,$setting_ic,$get_user_pelaksana_logic_ic);
			$data['pendaftaran_id']=$pendaftaran_id;
		}
		
		// print_r($data);exit;
		$data = array_merge($data,$data_login_ppa,$data_satuan_ttv,$data_header_ttv,$logic_akses_ttv,$logic_akses_assemen_rj,$logic_akses_asmed,$logic_akses_asmed_igd,$logic_akses_nyeri,$logic_akses_triage,$logic_akses_assesmen_igd,$logic_akses_ic,$logic_akses_cppt, backend_info());
			// print_r($list_template_assement);exit;
		$this->parser->parse('module_template', $data);
		
	}
	
	function history_assesmen($pendaftaran_id,$assesmen_id,$lev='0'){
		$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
		// $data=array();
		$q="SELECT MD.nama as nama_dokter,MP.nama as nama_poli,JK.ref as jk
			,MK.nama as nama_kelompok,MR.nama as nama_rekanan
			,H.*,H.title as title_nama,H.id as pendaftaran_id,TPA.risiko_jatuh as var_header_risiko_jatuh
			,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header 
			FROM tpoliklinik_pendaftaran H
			INNER JOIN mdokter MD ON MD.id=H.iddokter
			INNER JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
			INNER JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
			INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
			LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
			LEFT JOIN tpoliklinik_assesmen TPA ON TPA.assesmen_id=H.assesmen_id
			LEFT JOIN tpoliklinik_assesmen_alergi A ON A.idpasien=H.idpasien AND A.status_assemen='2'
			WHERE H.id='$pendaftaran_id'
			GROUP BY H.id";
		$data=$this->db->query($q)->row_array();
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$this->parser->parse('module_template', $data);
	}
	function save_ttv(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tanggal_input')). ' ' .$this->input->post('waktudaftar');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$ttv_id=$this->input->post('ttv_id');
		$data=array(
			'pendaftaran_id'=> $this->input->post('pendaftaran_id'),
			'tanggal_input'=> $tanggal_input,
			'tingkat_kesadaran'=> $this->input->post('tingkat_kesadaran'),
			'nadi'=> $this->input->post('nadi'),
			'nafas'=> $this->input->post('nafas'),
			'td_sistole'=> $this->input->post('td_sistole'),
			'td_diastole'=> $this->input->post('td_diastole'),
			'suhu'=> $this->input->post('suhu'),
			'tinggi_badan'=> $this->input->post('tinggi_badan'),
			'berat_badan'=> $this->input->post('berat_badan'),
			'status_ttv'=> $this->input->post('status_ttv'),
		);
		if ($ttv_id==''){
			$data['created_ppa']=$this->input->post('login_ppa_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$result=$this->db->insert('tpoliklinik_ttv',$data);
			$ttv_id=$this->db->insert_id();
			
			if ($result){
				$data['ttv_id']=$ttv_id;
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}else{
			$data['edited_ppa']=$this->input->post('login_ppa_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$ttv_id);
			$result=$this->db->update('tpoliklinik_ttv',$data);
			if ($result){
				$data['ttv_id']=$ttv_id;
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function hapus_ttv(){
		$id=$this->input->post('id');
		$data=array(
			
			'status_ttv'=> 0,
		);
			$data['deleted_ppa']=$this->input->post('login_ppa_id');
			$data['deleted_date']=date('Y-m-d H:i:s');
			
			$this->db->where('id',$id);
			$result=$this->db->update('tpoliklinik_ttv',$data);
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_record_ttv(){
		$id=$this->input->post('id');
		$data=array(
			'status_ttv'=> 0,
			'alasan_id'=> $this->input->post('alasan_id'),
			'keterangan_hapus'=> $this->input->post('keterangan_hapus'),
		);
			$data['deleted_ppa']=$this->input->post('login_ppa_id');
			$data['deleted_date']=date('Y-m-d H:i:s');
			
			$this->db->where('id',$id);
			$result=$this->db->update('tpoliklinik_ttv',$data);
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		$this->output->set_output(json_encode($hasil));
	}
	function copy_ttv(){
		$id=$this->input->post('id');
		$q="SELECT CONCAT(MP.nik,' - ',MP.nama) as ppa,H.* FROM tpoliklinik_ttv H
				INNER JOIN mppa MP ON MP.id=H.created_ppa
				WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$hasil['tglpendaftaran']=HumanDateShort(date('Y-m-d'));
		$hasil['waktupendaftaran']=HumanTime(date('H:i:s'));
		$this->output->set_output(json_encode($hasil));
	}
	function edit_ttv(){
		$id=$this->input->post('id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_edit=$this->input->post('keterangan_edit');
		if ($alasan_id){
			$data_edit=array(
				'alasan_edit_id' =>$alasan_id,
				'keterangan_edit' =>$keterangan_edit,
			);
			$this->db->where('id',$id);
			$this->db->update('tpoliklinik_ttv',$data_edit);
		}
		$q="SELECT CONCAT(MP.nik,' - ',MP.nama) as ppa,H.* FROM tpoliklinik_ttv H
				INNER JOIN mppa MP ON MP.id=H.created_ppa
				WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$hasil['tglpendaftaran']=HumanDateShort($hasil['tanggal_input']);
		$hasil['waktupendaftaran']=HumanTime($hasil['tanggal_input']);
		$this->output->set_output(json_encode($hasil));
	}
	function index_TTV_history(){
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
			$login_profesi_id=$this->session->userdata('login_profesi_id');
			
			$logic_akse_ttv=$this->Tpendaftaran_poli_ttv_model->logic_akses_ttv($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
			
			
			$akses_general=$this->db->query("SELECT *FROM setting_ttv")->row_array();
			$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
						
			$pendaftaran_id =$this->input->post('pendaftaran_id');
			$st_owned =$this->input->post('st_owned');
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$profesi_id =$this->input->post('profesi_id');
			$ppa_id =$this->input->post('ppa_id');
			$st_sedang_edit =$this->input->post('st_sedang_edit');
			// print_r($st_sedang_edit);exit;
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($ppa_id){
				$ppa_id=implode(", ", $ppa_id);
				$where .=" AND (H.created_ppa) IN (".$ppa_id.")";
			}
			if ($profesi_id){
				$profesi_id=implode(", ", $profesi_id);
				$where .=" AND (UC.jenis_profesi_id) IN (".$profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}
			
			$this->select = array();
			$from="
					(
						SELECT DATEDIFF(NOW(), H.tanggal_input) as selisih,
						mnadi.warna as warna_nadi
						,mnafas.warna as warna_nafas
						,mtd_sistole.warna as warna_sistole
						,mtd_diastole.warna as warna_diastole
						,msuhu.warna as warna_suhu
						,H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) as masa_tubuh,mberat.warna as warna_berat
						,TK.ref as kesadaran,H.* 
						,UC.nama as nama_created,UE.nama as nama_edited,UC.jenis_profesi_id,malasan_batal.keterangan as ket_edit
						FROM tpoliklinik_ttv H
						LEFT JOIN merm_referensi TK ON TK.nilai=H.tingkat_kesadaran AND TK.ref_head_id='23'
						LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
						LEFT JOIN mnafas ON H.nafas BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
						LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
						LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
						LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
						LEFT JOIN mberat ON H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
						LEFT JOIN mppa UC ON UC.id=H.created_ppa
						LEFT JOIN mppa UE ON UE.id=H.edited_ppa
						LEFT JOIN malasan_batal ON malasan_batal.id=H.alasan_edit_id
						WHERE H.pendaftaran_id='$pendaftaran_id' AND H.status_ttv > 1 ".$where."
						GROUP BY H.id
						ORDER BY H.tanggal_input DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array();
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $btn_disabel_edit='';
		  $btn_disabel_hapus='';
		  if ($st_sedang_edit=='1'){
			$btn_disabel_edit='disabled';			  
		  }
		  
		  $btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_ttv('.$r->id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		  $btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_ttv('.$r->id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
		  $btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_ttv('.$r->id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		  if ($akses_general['st_edit_catatan']=='0'){
			  $btn_edit='';
		  }else{
			  if ($akses_general['lama_edit']>0){
				  if ($r->selisih>$akses_general['lama_edit']){
					  $btn_edit='';
				  }else{
					  
				  }
				  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_edit='';
					  }
				  }
			  }
		  }
		  if ($akses_general['st_duplikasi_catatan']=='0'){
			  $btn_duplikasi='';
		  }else{
			  if ($akses_general['lama_duplikasi']>0){
				  if ($r->selisih>$akses_general['lama_duplikasi']){
					  $btn_duplikasi='';
				  }else{
					 
				  }
			  }
			   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id!=$r->created_ppa){
						$btn_duplikasi='';
				  }
			   }
		  }
		  if ($akses_general['st_hapus_catatan']=='0'){
			  $btn_hapus='';
		  }else{
			  if ($akses_general['lama_hapus']>0){
				  if ($r->selisih>$akses_general['lama_hapus']){
					  $btn_hapus='';
				  }else{
					 
				  }
				   
			  }
			  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id <> $r->created_ppa){
						$btn_hapus='';
				  }else{
					  
				  }
						// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
			  }
		  }
				if ($logic_akse_ttv['st_edit_ttv']=='0'){
				  $btn_edit='';
			  }
			  if ($logic_akse_ttv['st_hapus_ttv']=='0'){
				  $btn_hapus='';
			  }
		 $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 85%;">
								
									
									<div class="h2 font-w700 '.($r->alasan_edit_id?'text-danger':'text-primary').' text-center">'.DFormat($r->tanggal_input).'</div>
									<div class="h5 text-muted text-center"> '.tanggal_indo_MY($r->tanggal_input).'</div>
									<div class="h5 text-muted  text-center"> '.HumanTime($r->tanggal_input).'</div>
									<div class="h5 text-muted text-uppercase push-5-t text-center"> 
										<div class="btn-group" role="group">
											'.$btn_edit.'
											'.$btn_duplikasi.'
											'.$btn_hapus.'
											
											
										</div>
									
									</div>
									'.($r->alasan_edit_id?'<div class="text-center text-success push-5-t"><a href="javascript:void(0)" onclick="lihat_perubahan('.$r->id.')"><i class="fa fa-pencil text-center"></i> '.$r->ket_edit.'</a></div>':'').'
								</td>
							</tr>
						</tbody>
					</table>';
		
          $result[] = $btn_1;
		  $btn_2='
			<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white" colspan="2">
									<div class="h5 "> Tingkat Kesadaran :</div>
									<div class="h5 text-muted  push-5-t "> '.($r->kesadaran).'</div>
									
								</td>
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Frekuensi Nadi :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$r->warna_nadi.'" > '.($r->nadi).' '.$data_satuan_ttv['satuan_nadi'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frekuensi Nafas :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$r->warna_nafas.'"> '.($r->nafas).' '.$data_satuan_ttv['satuan_nafas'].'</div>
								</td>
							</tr>
						</tbody>
					</table>
		  ';
		 
          $result[] = $btn_2;
		  $btn_3 ='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Tekanan Darah :</div>
									<div class="h5 text-muted  push-5-t "> <label  class="h5 text-muted  push-5-t" style="color:'.$r->warna_sistole.'">'.($r->td_sistole.' '.$data_satuan_ttv['satuan_td'].'</label> / <label class="h5 text-muted  push-5-t" style="color:'.$r->warna_diastole.'">'.$r->td_diastole).' '.$data_satuan_ttv['satuan_td'].'</label></div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Suhu Tubuh :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$r->warna_suhu.'"> '.($r->suhu).' '.$data_satuan_ttv['satuan_suhu'].'</div>
								</td>
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Tinggi Badan :</div>
									<div class="h5 text-muted  push-5-t style="color:'.$r->warna_berat.'""> '.($r->tinggi_badan).' Cm</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Berat Badan :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$r->warna_berat.'"> '.($r->berat_badan).' Kg</div>
								</td>
							</tr>
						</tbody>
					</table>
				';
		  
          $result[] = $btn_3;
		 							
		  if ($r->alasan_edit_id){
			   $kolom_edit='<div class="h5 "> Edited</div>
									<div class=" text-muted  push-5-t "><i class="fa fa-user-md"></i> '.($r->alasan_edit_id?$r->nama_edited.' <br>'.HumanDateLong($r->edited_date):'').'</div>';
		
		  }else{
			  $kolom_edit='';
		  }
		  $btn_4 ='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Created</div>
									<div class=" text-muted  push-5-t "><i class="fa fa-user-md"></i> '.$r->nama_created.' <br>('.HumanDateLong($r->created_date).')</div>
								</td>
								
							</tr>
							<tr>
								<td class="bg-white">
									'.$kolom_edit.'
																	
								</td>
								
							</tr>
							
						</tbody>
					</table>
				';
		  
          $result[] = $btn_4;
		  
          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function index_ttv_perubahan()	{
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
			$login_profesi_id=$this->session->userdata('login_profesi_id');
			
			$logic_akse_ttv=$this->Tpendaftaran_poli_ttv_model->logic_akses_ttv($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
			
			
			$akses_general=$this->db->query("SELECT *FROM setting_ttv")->row_array();
			$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
						
			$id =$this->input->post('id');
			
			
			$this->select = array();
			$from="
					(
						SELECT DATEDIFF(NOW(), H.tanggal_input) as selisih,
						mnadi.warna as warna_nadi
						,mnafas.warna as warna_nafas
						,mtd_sistole.warna as warna_sistole
						,mtd_diastole.warna as warna_diastole
						,msuhu.warna as warna_suhu
						,H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) as masa_tubuh,mberat.warna as warna_berat
						,TK.ref as kesadaran,H.* 
						,UC.nama as nama_created,UE.nama as nama_edited,UC.jenis_profesi_id,malasan_batal.keterangan as ket_edit
						FROM tpoliklinik_ttv_his H
						LEFT JOIN merm_referensi TK ON TK.nilai=H.tingkat_kesadaran AND TK.ref_head_id='23'
						LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
						LEFT JOIN mnafas ON H.nafas BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
						LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
						LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
						LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
						LEFT JOIN mberat ON H.berat_badan/((H.tinggi_badan/100)*(H.tinggi_badan/100)) BETWEEN mberat.berat_1 AND mberat.berat_2 AND mberat.staktif='1'
						LEFT JOIN mppa UC ON UC.id=H.created_ppa
						LEFT JOIN mppa UE ON UE.id=H.edited_ppa
						LEFT JOIN malasan_batal ON malasan_batal.id=H.alasan_edit_id
						WHERE H.ttv_id='$id'
						GROUP BY H.id
						ORDER BY H.id ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array();
			$this->column_order  = array();
	  
	  $tingkat_kesadaran_awal='';
	  $nadi_awal='';
	  $nafas_awal='';
	  $td_sistole_awal='';
	  $td_diastole_awal='';
	  $tinggi_badan_awal='';
	  $berat_badan_awal='';
	  $suhu_awal='';
	  $warna_sadar='';
	  $warna_normal='';$warna_nadi='';$warna_nafas='';$warna_sistole='';$warna_diastole=''; $warna_suhu='';$warna_tinggi='';$warna_berat='';
	  $warna_tidak_normal='red';
	  
      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		 if ($r->jml_edit>0){
			 $warna_sadar=($tingkat_kesadaran_awal!=$r->tingkat_kesadaran?$warna_tidak_normal:$warna_normal);
			 $warna_nadi=($nadi_awal!=$r->nadi?$warna_tidak_normal:$warna_normal);
			 $warna_nafas=($nafas_awal!=$r->nafas?$warna_tidak_normal:$warna_normal);
			 $warna_sistole=($td_sistole_awal!=$r->td_sistole?$warna_tidak_normal:$warna_normal);
			 $warna_diastole=($td_diastole_awal!=$r->td_diastole?$warna_tidak_normal:$warna_normal);
			 $warna_suhu=($suhu_awal!=$r->suhu?$warna_tidak_normal:$warna_normal);
			 $warna_tinggi=($tinggi_badan_awal!=$r->tinggi_badan?$warna_tidak_normal:$warna_normal);
			 $warna_berat=($berat_badan_awal!=$r->berat_badan?$warna_tidak_normal:$warna_normal);
		 }
		 $tingkat_kesadaran_awal=$r->tingkat_kesadaran;
		 $nadi_awal=$r->nadi;
		 $nafas_awal=$r->nafas;
		 $td_sistole_awal=$r->td_sistole;
		 $td_diastole_awal=$r->td_diastole;
		 $suhu_awal=$r->suhu;
		 $tinggi_badan_awal=$r->tinggi_badan;
		 $berat_badan_awal=$r->berat_badan;
		 $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 85%;">
								
									
									<div class="h2 font-w700 '.($r->alasan_edit_id?'text-danger':'text-primary').' text-center">'.DFormat($r->tanggal_input).'</div>
									<div class="h5 text-muted text-center"> '.tanggal_indo_MY($r->tanggal_input).'</div>
									<div class="h5 text-muted  text-center"> '.HumanTime($r->tanggal_input).'</div>
									
									'.($r->jml_edit>0?'<div class="text-center text-danger push-5-t"><i class="fa fa-pencil text-center"></i> '.$r->ket_edit.' ('.$r->keterangan_edit.')</div>':'<div class="text-center text-primary push-5-t"><i class="si si-anchor text-center"></i> ORIGINAL</div>').'
								</td>
							</tr>
						</tbody>
					</table>';
		
          $result[] = $btn_1;
		  $btn_2='
			<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white" colspan="2">
									<div class="h5 "> Tingkat Kesadaran :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_sadar.'" > '.($r->kesadaran).'</div>
									
								</td>
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Frekuensi Nadi :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_nadi.'" > '.($r->nadi).' '.$data_satuan_ttv['satuan_nadi'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frekuensi Nafas :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_nafas.'"> '.($r->nafas).' '.$data_satuan_ttv['satuan_nafas'].'</div>
								</td>
							</tr>
						</tbody>
					</table>
		  ';
		 
          $result[] = $btn_2;
		  $btn_3 ='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Tekanan Darah :</div>
									<div class="h5 text-muted  push-5-t "> <label  class="h5 text-muted  push-5-t" style="color:'.$warna_sistole.'">'.($r->td_sistole.' '.$data_satuan_ttv['satuan_td'].'</label> / <label class="h5 text-muted  push-5-t" style="color:'.$warna_diastole.'">'.$r->td_diastole).' '.$data_satuan_ttv['satuan_td'].'</label></div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Suhu Tubuh :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_suhu.'"> '.($r->suhu).' '.$data_satuan_ttv['satuan_suhu'].'</div>
								</td>
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Tinggi Badan :</div>
									<div class="h5 text-muted  push-5-t style="color:'.$warna_tinggi.'""> '.($r->tinggi_badan).' Cm</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Berat Badan :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_berat.'"> '.($r->berat_badan).' Kg</div>
								</td>
							</tr>
						</tbody>
					</table>
				';
		  
          $result[] = $btn_3;
		  $btn_4 ='<table class="block-table text-left">
						<tbody>'.($r->jml_edit==0?'
							<tr>
								<td class="bg-white">
									<div class="h5 "> Created</div>
									<div class=" text-muted  push-5-t "><i class="fa fa-user-md"></i> '.$r->nama_created.' <br>('.HumanDateLong($r->created_date).')</div>
								</td>
								
							</tr>':'
							<tr>
								<td class="bg-white">
									<div class="h5 "> Edited</div>
									<div class=" text-muted  push-5-t "><i class="fa fa-user-md"></i> '.($r->nama_edited?$r->nama_edited.' <br>'.HumanDateLong($r->edited_date):'').'</div>
								</td>
								
							</tr>
							').'
						</tbody>
					</table>
				';
		  
          $result[] = $btn_4;
		  
          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  //ASSSESMENT
	function create_assesmen(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_id=$this->input->post('template_id');
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'template_id' => $template_id,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_assesmen',$data);
	  $assesmen_id=$this->db->insert_id();
	  $q="INSERT INTO tpoliklinik_assesmen_risiko_jatuh (assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai)
			SELECT '$assesmen_id',H.parameter_nama,GROUP_CONCAT(M.id) as group_nilai,null as param_nilai_id,0 as nilai FROM `mrisiko_jatuh_param` H
			LEFT JOIN mrisiko_jatuh_param_skor M ON M.parameter_id=H.id AND M.staktif='1'
			WHERE H.mrisiko_id='$template_id' AND H.staktif='1'
			GROUP BY H.id";
	  $this->db->query($q);
	  
	  $q="INSERT INTO tpoliklinik_assesmen_ttv (
			 assesmen_id,pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv,created_ppa,created_date
			)
			SELECT '$assesmen_id',pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv 
			,'$login_ppa_id',NOW()
			FROM (
				
			SELECT 1 as lev, pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv FROM tpoliklinik_assesmen_ttv
			WHERE pendaftaran_id='$pendaftaran_id'
			UNION ALL
			
			SELECT 2 as lev, pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv FROM tpoliklinik_ttv
			WHERE pendaftaran_id='$pendaftaran_id' AND status_ttv='2'
			
			UNION ALL
			
			SELECT 3 as lev, '$pendaftaran_id',NOW(),null,null,null,null,null,null,null,null,2 FROM tpoliklinik_ttv

		
			) T 
			ORDER BY T.lev ASC
			LIMIT 1";
		$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
    }
	
	function create_template(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_id=$this->input->post('template_id');
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'template_id' => $template_id,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 3,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_assesmen',$data);
	  $assesmen_id=$this->db->insert_id();
	  $q="INSERT INTO tpoliklinik_assesmen_risiko_jatuh (assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai)
			SELECT '$assesmen_id',H.parameter_nama,GROUP_CONCAT(M.id) as group_nilai,null as param_nilai_id,0 as nilai FROM `mrisiko_jatuh_param` H
			LEFT JOIN mrisiko_jatuh_param_skor M ON M.parameter_id=H.id AND M.staktif='1'
			WHERE H.mrisiko_id='$template_id' AND H.staktif='1'
			GROUP BY H.id";
	  $this->db->query($q);
	  $q="INSERT INTO tpoliklinik_assesmen_ttv (assesmen_id,pendaftaran_id,tanggal_input,created_ppa,created_date)
			VALUES (
		'$assesmen_id','$pendaftaran_id','$tanggal_input','$login_ppa_id','$tanggal_input')
	  ";
	  $this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
    }
	
	function batal_assesmen(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $st_edited=$this->input->post('st_edited');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	   if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
		  
		  
	  }
	  if ($st_edited=='1'){
		  $q="DELETE FROM tpoliklinik_assesmen_alergi WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  $q="INSERT INTO tpoliklinik_assesmen_alergi 
			(assesmen_id,pendaftaran_id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,staktif,status_assemen,created_date,created_by,edited_date,edited_by,deleted_date,deleted_by)
			SELECT assesmen_id,pendaftaran_id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,staktif,status_assemen,created_date,created_by,edited_date,edited_by,deleted_date,deleted_by FROM tpoliklinik_assesmen_his_alergi WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_assesmen_diagnosa WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
			$q="INSERT INTO tpoliklinik_assesmen_diagnosa (assesmen_id,template_assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,template_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif)
SELECT assesmen_id,template_assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,template_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif FROM tpoliklinik_assesmen_his_diagnosa WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
		$this->db->query($q);
		
			 $q="SELECT mdiagnosa_id,data_id,pilih  FROM tpoliklinik_assesmen_his_diagnosa_data WHERE  pilih='1' AND assesmen_id='$assesmen_id' AND versi_edit='$jml_edit' ";//Update ISI diagnosa
			  $row=$this->db->query($q)->result();
			  foreach ($row as $r){
				  $this->db->where('assesmen_id',$assesmen_id);
				  $this->db->where('mdiagnosa_id',$r->mdiagnosa_id);
				  $this->db->where('data_id',$r->data_id);
				  $this->db->update('tpoliklinik_assesmen_diagnosa_data',array('pilih'=>$r->pilih));
			  }
			  
			  $q="DELETE FROM tpoliklinik_assesmen_risiko_jatuh WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
			  $q="INSERT INTO  tpoliklinik_assesmen_risiko_jatuh(assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai)
			SELECT assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai
			FROM tpoliklinik_assesmen_his_risiko_jatuh WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_assesmen_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			$q="DELETE FROM tpoliklinik_assesmen_his_alergi WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			$q="DELETE FROM tpoliklinik_assesmen_his_diagnosa WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			$q="DELETE FROM tpoliklinik_assesmen_his_diagnosa_data WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			$q="DELETE FROM tpoliklinik_assesmen_his_edukasi WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			$q="DELETE FROM tpoliklinik_assesmen_his_risiko_jatuh WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			$q="DELETE FROM tpoliklinik_assesmen_his_riwayat_penyakit WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			$q="DELETE FROM tpoliklinik_assesmen_his_ttv WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
		  $data['status_assemen']='2';
	  }else{
		  $data['status_assemen']='0';
		  
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_assesmen',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
  function batal_template(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nama_template=$this->input->post('nama_template');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	  if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
	  }
	  if ($nama_template){
		  $data['nama_template']=$nama_template;
		  $data['status_assemen']='4';
	  }else{
		  
		  $data['status_assemen']='0';
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_assesmen',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
	function save_assesmen(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$jml_edit=$this->input->post('jml_edit');
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'status_assemen' => $this->input->post('status_assemen'),
			'st_anamnesa' => $this->input->post('st_anamnesa'),
			'keluhan_utama' => $this->input->post('keluhan_utama'),
			'nama_template' => $this->input->post('nama_template'),
			'nama_anamnesa' => $this->input->post('nama_anamnesa'),
			'hubungan_anamnesa' => $this->input->post('hubungan_anamnesa'),
			'st_riwayat_penyakit' => $this->input->post('st_riwayat_penyakit'),
			'riwayat_penyakit_lainnya' => $this->input->post('riwayat_penyakit_lainnya'),
			'riwayat_alergi' => $this->input->post('riwayat_alergi'),
			'riwayat_pengobatan' => $this->input->post('riwayat_pengobatan'),
			'hubungan_anggota_keluarga' => $this->input->post('hubungan_anggota_keluarga'),
			'st_psikologis' => $this->input->post('st_psikologis'),
			'st_sosial_ekonomi' => $this->input->post('st_sosial_ekonomi'),
			'st_spiritual' => $this->input->post('st_spiritual'),
			'pendidikan' => $this->input->post('pendidikan'),
			'pekerjaan' => $this->input->post('pekerjaan'),
			'st_nafsu_makan' => $this->input->post('st_nafsu_makan'),
			'st_turun_bb' => $this->input->post('st_turun_bb'),
			'st_mual' => $this->input->post('st_mual'),
			'st_muntah' => $this->input->post('st_muntah'),
			'st_fungsional' => $this->input->post('st_fungsional'),
			'st_alat_bantu' => $this->input->post('st_alat_bantu'),
			'st_cacat' => $this->input->post('st_cacat'),
			'cacat_lain' => $this->input->post('cacat_lain'),
			'risiko_jatuh' => $this->input->post('risiko_jatuh'),
			'skor_pengkajian' => $this->input->post('skor_pengkajian'),
			'st_tindakan' => $this->input->post('st_tindakan'),
			'nama_tindakan' => $this->input->post('nama_tindakan'),
			'st_tindakan_action' => $this->input->post('st_tindakan_action'),
			'st_edukasi' => $this->input->post('st_edukasi'),
			'st_menerima_info' => $this->input->post('st_menerima_info'),
			'st_hambatan_edukasi' => $this->input->post('st_hambatan_edukasi'),
			'st_penerjemaah' => $this->input->post('st_penerjemaah'),
			'penerjemaah' => $this->input->post('penerjemaah'),
			'catatan_edukasi' => $this->input->post('catatan_edukasi'),

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tpoliklinik_assesmen_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_assesmen',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_edukasi(){
		$hasil=false;
		$assesmen_id=$this->input->post('assesmen_id');
		$medukasi_id=$this->input->post('medukasi_id');
		// print_r($medukasi_id);exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->delete('tpoliklinik_assesmen_edukasi');
		if ($medukasi_id){
			foreach($medukasi_id as $index=>$val){
				$data=array(
					'assesmen_id' =>$assesmen_id,
					'medukasi_id' =>$val,
				);
				$hasil=$this->db->insert('tpoliklinik_assesmen_edukasi',$data);
			}
		}
		
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_riwayat_penyakit(){
		$hasil=false;
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$riwayat_penyakit=$this->input->post('riwayat_penyakit');
		// print_r($medukasi_id);exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->delete('tpoliklinik_assesmen_riwayat_penyakit');
		if ($riwayat_penyakit){
			foreach($riwayat_penyakit as $index=>$val){
				$data=array(
					'assesmen_id' =>$assesmen_id,
					'idpasien' =>$idpasien,
					'pendaftaran_id' =>$pendaftaran_id,
					'penyakit_id' =>$val,
				);
				$hasil=$this->db->insert('tpoliklinik_assesmen_riwayat_penyakit',$data);
			}
		}
		
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_alergi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$alergi_id=$this->input->post('alergi_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$input_jenis_alergi=$this->input->post('input_jenis_alergi');
		$input_detail_alergi=$this->input->post('input_detail_alergi');
		$input_reaksi_alergi=$this->input->post('input_reaksi_alergi');
		$data=array(
				'idpasien' =>$idpasien,
				'assesmen_id' =>$assesmen_id,
				'pendaftaran_id' =>$pendaftaran_id,
				'input_jenis_alergi' =>$input_jenis_alergi,
				'input_detail_alergi' =>$input_detail_alergi,
				'input_reaksi_alergi' =>$input_reaksi_alergi,
				'staktif' =>1,
			);
		if ($alergi_id){
			$data['edited_by']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$alergi_id);
			$hasil=$this->db->update('tpoliklinik_assesmen_alergi',$data);
			
		}else{
			$data['created_by']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('tpoliklinik_assesmen_alergi',$data);
		}
	
		$this->output->set_output(json_encode($hasil));
	}
	
	function load_alergi()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.*,R.ref  as jenis_alergi, MP.nama as user_created
						FROM tpoliklinik_assesmen_alergi H
						INNER JOIN merm_referensi R ON R.ref_head_id='24' AND R.nilai=H.input_jenis_alergi
						LEFT JOIN mppa MP ON MP.id=H.created_by
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('jenis_alergi','input_detail_alergi','input_reaksi_alergi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->jenis_alergi);
          $result[] = $r->input_detail_alergi;
          $result[] = $r->input_reaksi_alergi;
          $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  $aksi .= '<button onclick="edit_alergi('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  $aksi .= '<button onclick="hapus_alergi('.$r->id.')" type="button" title="Hapus Alergi" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_alergi_his()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$idpasien=$this->input->post('idpasien');
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.*,R.ref  as jenis_alergi, MP.nama as user_created
						FROM (
							SELECT * FROM `tpoliklinik_assesmen_igd_alergi`
							WHERE idpasien='$idpasien' AND status_assemen='2'
							UNION ALL
							SELECT * FROM `tpoliklinik_assesmen_alergi`
							WHERE idpasien='$idpasien'  AND status_assemen='2'
						) H
						INNER JOIN merm_referensi R ON R.ref_head_id='24' AND R.nilai=H.input_jenis_alergi
						LEFT JOIN mppa MP ON MP.id=H.created_by
						WHERE H.idpasien='$idpasien' AND H.staktif='1'  AND H.status_assemen='2'
						ORDER BY H.id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('jenis_alergi','input_detail_alergi','input_reaksi_alergi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->jenis_alergi);
          $result[] = $r->input_detail_alergi;
          $result[] = $r->input_reaksi_alergi;
          $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
          // $aksi = '<div class="btn-group">';
		  // // if (UserAccesForm($user_acces_form,array('1607'))){
		  // $aksi .= '<button onclick="edit_alergi('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // // }
		   // // if (UserAccesForm($user_acces_form,array('1608'))){
		  // $aksi .= '<button onclick="hapus_alergi('.$r->id.')" type="button" title="Hapus Suhu" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // // }
		  // $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_alergi_his_header()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$idpasien=$this->input->post('idpasien');
			$this->select = array();
			$from="
					(
						SELECT H.*,R.ref  as jenis_alergi, MP.nama as user_created
						FROM (
							SELECT * FROM `tpoliklinik_assesmen_igd_alergi`
							WHERE idpasien='$idpasien' AND status_assemen='2'
							UNION ALL
							SELECT * FROM `tpoliklinik_assesmen_alergi`
							WHERE idpasien='$idpasien'  AND status_assemen='2'
						) H
						INNER JOIN merm_referensi R ON R.ref_head_id='24' AND R.nilai=H.input_jenis_alergi
						LEFT JOIN mppa MP ON MP.id=H.created_by
						WHERE H.idpasien='$idpasien' AND H.staktif='1' AND H.status_assemen='2'
						ORDER BY H.id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('jenis_alergi','input_detail_alergi','input_reaksi_alergi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->jenis_alergi);
          $result[] = $r->input_detail_alergi;
          $result[] = $r->input_reaksi_alergi;
          $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
         
          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_alergi(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $id=$this->input->post('id');
	  $data['deleted_by']=$login_ppa_id;
	  $data['deleted_date']=date('Y-m-d H:i:s');
	  $data['staktif']=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_assesmen_alergi',$data);
	  echo json_encode($hasil);
  }
  
  function edit_alergi(){
	  $id=$this->input->post('id');
	  $q="SELECT * FROM `tpoliklinik_assesmen_alergi` H WHERE H.id='$id'";
	  $output=$this->db->query($q)->row_array();
	  echo json_encode($output);
  }
 
  function load_skrining_risiko_jatuh(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $q="SELECT * FROM `tpoliklinik_assesmen_risiko_jatuh` H WHERE H.assesmen_id='$assesmen_id'";
	  $hasil=$this->db->query($q)->result();
	  $opsi='';
	  $no=0;
	  foreach($hasil as $r){
		  $no=$no+1;
		 $opsi.='<tr>">
					<td>'.$r->parameter_nama.'<input class="risiko_nilai_id" type="hidden"  value="'.$r->id.'" ></td>
					<td><select tabindex="8" class="form-control nilai " style="width: 100%;" data-placeholder="Pilih Opsi" required>
						'.$this->opsi_nilai($r->group_nilai,$r->param_nilai_id).'
					</select>
					</td>
					
				</tr>';
	  }
	  $data['opsi']=$opsi;
	  echo json_encode($data);
  }
  function opsi_nilai($param,$param_nilai_id){
	  $q="SELECT * FROM `mrisiko_jatuh_param_skor` H WHERE H.id IN ($param)";
	  $hasil='';
	  $row=$this->db->query($q)->result();
		  $hasil .='<option value="" '.($param_nilai_id==''?'selected':'').'>-Opsi-</option>';
	  foreach($row as $r){
		  $hasil .='<option value="'.$r->id.'" '.($param_nilai_id==$r->id?'selected':'').'>'.$r->deskripsi_nama.'</option>';
	  }
	  return $hasil;
  }
  function update_nilai_risiko_jatuh(){
	  $id=$this->input->post('risiko_nilai_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $template_id=$this->input->post('template_id');
	  $nilai_id=$this->input->post('nilai_id');
	  $data=array(
		'param_nilai_id' =>$nilai_id
	  );
	  $this->db->where('id',$id);
	  $result=$this->db->update('tpoliklinik_assesmen_risiko_jatuh',$data);
	  
	  $skor=$this->db->query("SELECT SUM(H.nilai) as skor FROM `tpoliklinik_assesmen_risiko_jatuh` H WHERE H.assesmen_id='$assesmen_id'")->row('skor');
	  $q="SELECT H.ref_nilai,H.st_tindakan,H.nama_tindakan FROM mrisiko_jatuh_setting_nilai H
			WHERE H.mrisiko_id='$template_id' AND ('$skor' BETWEEN H.skor_1 AND H.skor_2)";
	  $hasil=$this->db->query($q)->row();
	  
	  $data['skor']=$skor;
	  $data['ref_nilai']=$hasil->ref_nilai;
	  $data['st_tindakan']=$hasil->st_tindakan;
	  $data['nama_tindakan']=$hasil->nama_tindakan;
	  $this->output->set_output(json_encode($data));
  }
  function get_skor_pengkajian(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $template_id=$this->input->post('template_id');
	 
	  
	  $skor=$this->db->query("SELECT SUM(H.nilai) as skor FROM `tpoliklinik_assesmen_risiko_jatuh` H WHERE H.assesmen_id='$assesmen_id'")->row('skor');
	  $q="SELECT H.ref_nilai,H.st_tindakan,H.nama_tindakan FROM mrisiko_jatuh_setting_nilai H
			WHERE H.mrisiko_id='$template_id' AND ('$skor' BETWEEN H.skor_1 AND H.skor_2)";
	  $hasil=$this->db->query($q)->row();
	  
	  $data['skor']=$skor;
	  $data['ref_nilai']=$hasil->ref_nilai;
	  $data['st_tindakan']=$hasil->st_tindakan;
	  $data['nama_tindakan']=$hasil->nama_tindakan;
	  $this->output->set_output(json_encode($data));
  }
  function simpan_diagnosa(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$mdiagnosa_id=$this->input->post('mdiagnosa_id');
		$prioritas=$this->input->post('prioritas');
		$diagnosa_id=$this->input->post('diagnosa_id');
		if ($this->cek_duplicate_diagnosa($assesmen_id,$mdiagnosa_id,$diagnosa_id)){
			$data=array(
					'idpasien' =>$idpasien,
					'assesmen_id' =>$assesmen_id,
					'pendaftaran_id' =>$pendaftaran_id,
					'mdiagnosa_id' =>$mdiagnosa_id,
					'prioritas' =>$prioritas,
					'staktif' =>1,
				);
			if ($diagnosa_id){
				$data['edited_by']=$login_ppa_id;
				$data['edited_date']=date('Y-m-d H:i:s');
				$this->db->where('id',$diagnosa_id);
				$hasil=$this->db->update('tpoliklinik_assesmen_diagnosa',$data);
				
			}else{
				$data['template_assesmen_id']=0;
				$data['created_by']=$login_ppa_id;
				$data['created_date']=date('Y-m-d H:i:s');
				$hasil=$this->db->insert('tpoliklinik_assesmen_diagnosa',$data);
			}
		}else{
			$hasil=false;
		}
	
		$this->output->set_output(json_encode($hasil));
	}
	
	function cek_duplicate_diagnosa($assesmen_id,$mdiagnosa_id,$diagnosa_id){
		$q="SELECT * FROM `tpoliklinik_assesmen_diagnosa` WHERE assesmen_id='$assesmen_id' AND mdiagnosa_id='$mdiagnosa_id' AND id !='$diagnosa_id' AND staktif='1'";
		$hasil=$this->db->query($q)->row('id');
		if ($hasil){
			return false;
		}else{
			return true;
		}
	}
	function load_diagnosa()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT MP.nama as nama_user,CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa, H.* FROM tpoliklinik_assesmen_diagnosa H
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN mdiagnosa MD ON MD.id=H.mdiagnosa_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						ORDER BY H.prioritas ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->diagnosa);
          $result[] = $r->prioritas;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  $aksi .= '<button onclick="edit_diagnosa('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  $aksi .= '<button onclick="hapus_diagnosa('.$r->id.')" type="button" title="Hapus Diagnosa" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '<button onclick="set_rencana_asuhan('.$r->id.')" type="button" title="Rencana Asuhan" class="btn btn-warning btn-xs btn_rencana"><i class="si si-book-open"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  
   function edit_diagnosa(){
	  $id=$this->input->post('id');
	  $q="SELECT * FROM `tpoliklinik_assesmen_diagnosa` H WHERE H.id='$id'";
	  $output=$this->db->query($q)->row_array();
	  echo json_encode($output);
  }
  
  
  function hapus_diagnosa(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $id=$this->input->post('id');
	  $data['deleted_by']=$login_ppa_id;
	  $data['deleted_date']=date('Y-m-d H:i:s');
	  $data['staktif']=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_assesmen_diagnosa',$data);
	  echo json_encode($hasil);
  }
  
  function hapus_anatomi(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $id=$this->input->post('id');
	  $data['deleted_by']=$login_ppa_id;
	  $data['deleted_date']=date('Y-m-d H:i:s');
	  $data['staktif']=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_asmed_anatomi',$data);
	  echo json_encode($hasil);
  }
  function update_data(){
	  $id=$this->input->post('id');
	  $pilih=$this->input->post('pilih');
	  $data['pilih']=$pilih;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_assesmen_diagnosa_data',$data);
	  echo json_encode($hasil);
  }
  
  function refresh_diagnosa(){
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $hasil='';
	  $q="SELECT H.id,CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa FROM tpoliklinik_assesmen_diagnosa H
			LEFT JOIN mdiagnosa MD ON MD.id=H.mdiagnosa_id
			WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1' ORDER BY H.prioritas";
	  $row=$this->db->query($q)->result();
		  $hasil .='<option value="" selected>Pilih Opsi</option>';
	  foreach($row as $r){
		  $hasil .='<option value="'.$r->id.'">'.$r->diagnosa.'</option>';
	  }
	  $data['list']=$hasil;
	  echo json_encode($hasil);
  }
  
  function load_data_rencana_asuhan(){
		$assesmen_id=$this->input->post('assesmen_id');
		$diagnosa_id=$this->input->post('diagnosa_id');
		$q="SELECT *FROM tpoliklinik_assesmen_diagnosa_data H
			WHERE H.assesmen_id='$assesmen_id' AND H.diagnosa_id='$diagnosa_id'
			ORDER BY H.nourut ASC";
		$row=$this->db->query($q)->result();
		$tabel='';
		$last_lev='';
		$jml_col='0';
		foreach($row as $r){
			if ($r->lev=='0'){//Section
				if ($last_lev =='2'){//Jika Record Baru
					if ($jml_col==1){
					$tabel .='<td></td>';
						
					}
					$tabel .='</tr>';
				}
				$last_lev=$r->lev;
				$tabel .='
					<tr>
						<td colspan="2" style="background-color:#d9efff; font-weight: bold;width:100%">'.$r->nama.'</td>
					</tr>				
				';
				$jml_col=0;
			}
			if ($r->lev=='1'){//Kategori
				if ($last_lev =='2'){//Jika Record Baru
					if ($jml_col==1){
					$tabel .='<td></td>';
						
					}
					$tabel .='</tr>';
				}
				$last_lev=$r->lev;
				$tabel .='
					<tr>
						<td colspan="2" style="background-color:#f7f7f7; font-weight: bold;width:100%" class="text-primary">'.$r->nama.'</td>
					</tr>				
				';
				$jml_col=0;
			}
			if ($r->lev=='2'){//Data
				if ($last_lev != $r->lev){//Jika Record Baru
					$tabel .='<tr>';
					$jml_col=$jml_col+1;
				}else{
					$jml_col=$jml_col+1;
				}
				if ($jml_col=='3'){
					$tabel .='</tr>';
					$tabel .='<tr>';
					$jml_col=1;
				}
				$tabel .='
					<td style="width:50%">
						<input type="hidden" class="data_id" value="'.$r->id.'">
						<div class="checkbox">
							<label for="example-'.$r->id.'">
								<input id="example-'.$r->id.'" name="chk[]" class="data_asuhan"  type="checkbox" '.($r->pilih?'checked':'').'> '.$r->nama.'
							</label>
						</div>
					</td>			
				';
				$last_lev=$r->lev;
			}
		}
		if ($jml_col==1){
		$tabel .='<td></td>';
			
		}
		$tabel .='</tr>';
		// $tabel .='<tr><td colspan="2">&nbsp;&nbsp;</td></tr>';
		// print_r($tabel);exit;
		echo json_encode($tabel);
  }
  
  function list_index_template()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
						 FROM tpoliklinik_assesmen H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.template_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		$result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nama_template);
		$result[] = ($r->jumlah_template);
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  
  
  function edit_template_assesmen(){
	  $assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'status_assemen' => 3,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_assesmen',$data);
		
		$data_detail=array(
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
		);
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_assesmen_alergi',$data_detail);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_assesmen_diagnosa',$data_detail);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_assesmen_riwayat_penyakit',$data_detail);
		
		
		$this->output->set_output(json_encode($result));
  }
  function list_history_pengkajian()
  {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_assesmen($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$where='';
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		if ($mppa_id!='#'){
			$where .=" AND H.created_ppa = '".$mppa_id."'";
		}
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_assesmen")->row_array();
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa
					,H.assesmen_id,H.pendaftaran_id,MP.tanggaldaftar,MP.nopendaftaran,MPOL.nama as poli 
					,MD.nama as dokter,H.tanggal_input as tanggal_pengkajian
					,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,MP.iddokter,MP.idpoliklinik,H.keterangan_edit,H.jml_edit,H.st_edited
					,H.ttd_nama,H.ttd
					FROM `tpoliklinik_assesmen` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					INNER JOIN mpoliklinik MPOL ON MPOL.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
					ORDER BY H.tanggal_input DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
		$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/input_assesmen_rj/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
		$btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
		
		 if ($akses_general['st_edit_catatan']=='0'){
			  $btn_edit='';
		  }else{
			  if ($akses_general['lama_edit']>0){
				  if ($r->selisih>$akses_general['lama_edit']){
					  $btn_edit='';
				  }else{
					  
				  }
				  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_edit='';
					  }
				  }
			  }
		  }
		  if ($akses_general['st_duplikasi_catatan']=='0'){
			  $btn_duplikasi='';
		  }else{
			  if ($akses_general['lama_duplikasi']>0){
				  if ($r->selisih>$akses_general['lama_duplikasi']){
					  $btn_duplikasi='';
				  }else{
					 
				  }
			  }
			   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id!=$r->created_ppa){
						$btn_duplikasi='';
				  }
			   }
		  }
		  if ($akses_general['st_hapus_catatan']=='0'){
			  $btn_hapus='';
		  }else{
			  if ($akses_general['lama_hapus']>0){
				  if ($r->selisih>$akses_general['lama_hapus']){
					  $btn_hapus='';
				  }else{
					 
				  }
				   
			  }
			  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id <> $r->created_ppa){
						$btn_hapus='';
				  }else{
					  
				  }
						// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
			  }
		  }
		if ($logic_akses_assesmen['st_edit_ttv']=='0'){
		  $btn_edit='';
		}
		if ($logic_akses_assesmen['st_hapus_ttv']=='0'){
		  $btn_hapus='';
		}
		
		$result[] = $no;
		$aksi='';
		$aksi_edit='';
		
		if ($r->jml_edit>0){
			$aksi_edit='<a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_assesmen_rj/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></a>';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_edit;	
		$aksi .= $btn_duplikasi;	
		$aksi .= $btn_hapus;	
		$aksi .= $btn_lihat;	
		$aksi .= $aksi_edit;	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = HumanDateLong($r->tanggaldaftar).' - '.$r->st_edited;
		$result[] = ($r->nopendaftaran);
		$result[] = ('<strong>'.$r->poli.'</strong><br>'.$r->dokter);
		$result[] = HumanDateLong($r->tanggal_pengkajian);
		$result[] = ($r->nama_mppa).$btn_ttd;
		$result[] = ($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function create_with_template(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $q="INSERT INTO tpoliklinik_assesmen (
			template_assesmen_id,tanggal_input,pendaftaran_id,created_date,created_ppa,template_id,status_assemen
			,st_anamnesa,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,riwayat_alergi,riwayat_pengobatan,hubungan_anggota_keluarga,st_psikologis,st_sosial_ekonomi,st_spiritual,pendidikan,pekerjaan,st_nafsu_makan,st_turun_bb,st_mual,st_muntah,st_fungsional,st_alat_bantu,st_cacat,cacat_lain,header_risiko_jatuh,footer_risiko_jatuh,skor_pengkajian,risiko_jatuh,nama_tindakan,st_tindakan,st_tindakan_action,st_edukasi,st_menerima_info,st_hambatan_edukasi,st_penerjemaah,penerjemaah,catatan_edukasi
			)
			SELECT '$template_assesmen_id',NOW() as tanggal_input,'$pendaftaran_id',NOW() as created_date,'$login_ppa_id',template_id,'1' as status_assemen
			,st_anamnesa,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,riwayat_alergi,riwayat_pengobatan,hubungan_anggota_keluarga,st_psikologis,st_sosial_ekonomi,st_spiritual,pendidikan,pekerjaan,st_nafsu_makan,st_turun_bb,st_mual,st_muntah,st_fungsional,st_alat_bantu,st_cacat,cacat_lain,header_risiko_jatuh,footer_risiko_jatuh,skor_pengkajian,risiko_jatuh,nama_tindakan,st_tindakan,st_tindakan_action,st_edukasi,st_menerima_info,st_hambatan_edukasi,st_penerjemaah,penerjemaah,catatan_edukasi
			FROM `tpoliklinik_assesmen`

			WHERE assesmen_id='$template_assesmen_id'";
	  $hasil=$this->db->query($q);
	  $assesmen_id=$this->db->insert_id();
	  $data=$this->db->query("SELECT template_id,idpasien from tpoliklinik_assesmen where assesmen_id='$assesmen_id'")->row();
	  $idpasien=$data->idpasien;
	  $template_id=$data->template_id;
	  $q="INSERT INTO tpoliklinik_assesmen_risiko_jatuh (assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai)
			SELECT '$assesmen_id',parameter_nama,group_nilai,param_nilai_id,nilai
			FROM tpoliklinik_assesmen_risiko_jatuh WHERE assesmen_id='$template_assesmen_id'";
	  $this->db->query($q);
	  //RIWAYAT PENYAKIT
	  $q="INSERT INTO tpoliklinik_assesmen_riwayat_penyakit (assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
			SELECT 
			'$assesmen_id','$pendaftaran_id',penyakit_id,'$idpasien',status_assemen
			FROM tpoliklinik_assesmen_riwayat_penyakit
			WHERE assesmen_id='$template_assesmen_id'";
	  $this->db->query($q);

	  //RIWAYAT EDUKASI
	  $q="INSERT INTO tpoliklinik_assesmen_edukasi(assesmen_id,medukasi_id)
			SELECT '$assesmen_id',medukasi_id FROM tpoliklinik_assesmen_edukasi
			WHERE assesmen_id='$template_assesmen_id'";
	  $this->db->query($q);

	  //RIWAYAT ALERGI
	  $q="INSERT INTO tpoliklinik_assesmen_alergi (assesmen_id,pendaftaran_id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,status_assemen,created_date,created_by )
		SELECT '$assesmen_id','$pendaftaran_id','$idpasien',input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,status_assemen,NOW(),'$login_ppa_id'
		 FROM tpoliklinik_assesmen_alergi
		 WHERE assesmen_id='$template_assesmen_id' AND staktif='1'";
	  $this->db->query($q);
	  //INSERT ttv_id
	  $q="INSERT INTO tpoliklinik_assesmen_ttv (assesmen_id, pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv,created_ppa,created_date)
			SELECT '$assesmen_id' as assesmen_id, pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv,'$login_ppa_id',NOW() FROM tpoliklinik_assesmen_ttv
			WHERE assesmen_id='$template_assesmen_id'";
	  $this->db->query($q);
	  
	  $q="SELECT *FROM tpoliklinik_assesmen_diagnosa WHERE assesmen_id='$template_assesmen_id'";
	  $row=$this->db->query($q)->result();
	 
				
	  foreach($row as $r){
		  $data_input=array(
				'idpasien' =>$idpasien,
				'assesmen_id' =>$assesmen_id,
				'template_assesmen_id' =>$template_assesmen_id,
				'pendaftaran_id' =>$pendaftaran_id,
				'mdiagnosa_id' =>$r->mdiagnosa_id,
				'prioritas' =>$r->prioritas,
				'created_by' =>$login_ppa_id,
				'created_date' =>date('Y-m-d H:i:s'),
				'staktif' =>1,
		  );
		  $this->db->insert('tpoliklinik_assesmen_diagnosa',$data_input);
	  }
	  $q="SELECT mdiagnosa_id,data_id,pilih  FROM tpoliklinik_assesmen_diagnosa_data WHERE  pilih='1' AND assesmen_id='$template_assesmen_id'";//Update ISI diagnosa
	  $row=$this->db->query($q)->result();
	  foreach ($row as $r){
		  $this->db->where('assesmen_id',$assesmen_id);
		  $this->db->where('mdiagnosa_id',$r->mdiagnosa_id);
		  $this->db->where('data_id',$r->data_id);
		  $this->db->update('tpoliklinik_assesmen_diagnosa_data',array('pilih'=>$r->pilih));
	  }
	  
	  //Update jumlah_template
	  $q="UPDATE tpoliklinik_assesmen 
			SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
			$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
    }
	
	function save_edit_assesmen(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_edit_id=$this->input->post('alasan_id');
		$keterangan_edit=$this->input->post('keterangan_edit');
		$jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_assesmen WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
		// $jml_edit=$this->input->post('jml_edit');
		$res=$this->simpan_history_assesmen($assesmen_id,$jml_edit);
		// print_r($res);exit;
		if ($res){
			
			$data=array(
				'status_assemen' => 1,
				'st_edited' => 1,
				'alasan_edit_id' =>$alasan_edit_id,
				'keterangan_edit' =>$keterangan_edit,

			);
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tpoliklinik_assesmen',$data);
			// $ttv_id=$this->db->insert_id();
			
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}else{
			$hasil=$res;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_history_assesmen($assesmen_id,$jml_edit){
		$hasil=false;
		
		// $jml_edit=$jml_edit+1;
		$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tpoliklinik_assesmen_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
		// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
		if ($baris==0){
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$q="INSERT IGNORE INTO tpoliklinik_assesmen_his (versi_edit,assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_anamnesa,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,riwayat_alergi,riwayat_pengobatan,hubungan_anggota_keluarga,st_psikologis,st_sosial_ekonomi,st_spiritual,pendidikan,pekerjaan,st_nafsu_makan,st_turun_bb,st_mual,st_muntah,st_fungsional,st_alat_bantu,st_cacat,cacat_lain,header_risiko_jatuh,footer_risiko_jatuh,skor_pengkajian,risiko_jatuh,nama_tindakan,st_tindakan,st_tindakan_action,st_edukasi,st_menerima_info,st_hambatan_edukasi,st_penerjemaah,penerjemaah,catatan_edukasi,risiko_jatuh_header,risiko_jatuh_footer,st_edited,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,edited_ppa,edited_date )
				SELECT '$jml_edit',assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_anamnesa,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,riwayat_alergi,riwayat_pengobatan,hubungan_anggota_keluarga,st_psikologis,st_sosial_ekonomi,st_spiritual,pendidikan,pekerjaan,st_nafsu_makan,st_turun_bb,st_mual,st_muntah,st_fungsional,st_alat_bantu,st_cacat,cacat_lain,header_risiko_jatuh,footer_risiko_jatuh,skor_pengkajian,risiko_jatuh,nama_tindakan,st_tindakan,st_tindakan_action,st_edukasi,st_menerima_info,st_hambatan_edukasi,st_penerjemaah,penerjemaah,catatan_edukasi,risiko_jatuh_header,risiko_jatuh_footer,st_edited,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,'$login_ppa_id',NOW() FROM tpoliklinik_assesmen 
				WHERE assesmen_id='$assesmen_id'";
			// print_r($q);exit;
			$hasil=$this->db->query($q);
			//ALERGI
			$q="INSERT IGNORE INTO tpoliklinik_assesmen_his_alergi 
				(versi_edit,assesmen_id,pendaftaran_id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,staktif,status_assemen,created_date,created_by,edited_date,edited_by,deleted_date,deleted_by)
				SELECT '$jml_edit',assesmen_id,pendaftaran_id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,staktif,status_assemen,created_date,created_by,edited_date,edited_by,deleted_date,deleted_by FROM tpoliklinik_assesmen_alergi WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			//DIAGNOSA
			$q="INSERT IGNORE INTO tpoliklinik_assesmen_his_diagnosa (id,versi_edit,assesmen_id,template_assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,template_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif)
	SELECT id,'$jml_edit',assesmen_id,template_assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,template_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif FROM tpoliklinik_assesmen_diagnosa WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			
			$q="INSERT IGNORE INTO tpoliklinik_assesmen_his_diagnosa_data (versi_edit,assesmen_id,diagnosa_id,mdiagnosa_id,lev,template_id,data_id,nourut,no_urut,nama,parent_id,pilih)
	SELECT '$jml_edit',assesmen_id,diagnosa_id,mdiagnosa_id,lev,template_id,data_id,nourut,no_urut,nama,parent_id,pilih FROM tpoliklinik_assesmen_diagnosa_data WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			//EDUKASI
			 $q="INSERT IGNORE INTO tpoliklinik_assesmen_his_edukasi(versi_edit,assesmen_id,medukasi_id)
				SELECT '$jml_edit',assesmen_id,medukasi_id FROM tpoliklinik_assesmen_edukasi
				WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
			//RISIKO JATUH
			$q="INSERT IGNORE INTO tpoliklinik_assesmen_his_risiko_jatuh (versi_edit,assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai)
				SELECT '$jml_edit',assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai
				FROM tpoliklinik_assesmen_risiko_jatuh WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		  //RIWAYAT PENYAKIT
		   $q="INSERT IGNORE  INTO tpoliklinik_assesmen_his_riwayat_penyakit (versi_edit,assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$jml_edit',assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen
				FROM tpoliklinik_assesmen_riwayat_penyakit
				WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		  //INSERT ttv_id
		  $q="INSERT IGNORE INTO tpoliklinik_assesmen_his_ttv (versi_edit,assesmen_id, pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv,created_ppa,created_date)
				SELECT '$jml_edit',assesmen_id, pendaftaran_id,tanggal_input,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,status_ttv,'$login_ppa_id',NOW() FROM tpoliklinik_assesmen_ttv
				WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		}
	  return $hasil;
	}
	
	function hapus_record_assesmen(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_assesmen',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	
	function simpan_assesmen_ttv(){
		
		$assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			
			'tingkat_kesadaran'=> $this->input->post('tingkat_kesadaran'),
			'nadi'=> $this->input->post('nadi'),
			'nafas'=> $this->input->post('nafas'),
			'td_sistole'=> $this->input->post('td_sistole'),
			'td_diastole'=> $this->input->post('td_diastole'),
			'suhu'=> $this->input->post('suhu'),
			'tinggi_badan'=> $this->input->post('tinggi_badan'),
			'berat_badan'=> $this->input->post('berat_badan'),
			'status_ttv'=> 1,
		);
		
		// $data['edited_ppa']=$this->input->post('login_ppa_id');
		// $data['edited_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_assesmen_ttv',$data);
		
		$this->output->set_output(json_encode($result));
	}
	function simpan_risiko_jatuh_template_igd(){
		
		$assesmen_id=$this->input->post('assesmen_id');
		$template_id=$this->input->post('template_id');
		$q="UPDATE tpoliklinik_assesmen_igd H INNER JOIN
			(
			SELECT '$assesmen_id' AS assesmen_id,isi_header,isi_footer FROM mrisiko_jatuh WHERE id='$template_id'
			) T ON H.assesmen_id=T.assesmen_id
			SET H.risiko_jatuh_header=T.isi_header,H.risiko_jatuh_footer=T.isi_footer
			";
		$this->db->query($q);
		$data=array(
			
			'template_id'=> $this->input->post('template_id'),
			
		);
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_assesmen_igd',$data);
		
		$this->output->set_output(json_encode($result));
	}
	function simpan_ttd(){
		
		$assesmen_id = $this->input->post('assesmen_id');
		$ttd_hubungan = $this->input->post('ttd_hubungan');
		$ttd_nama = $this->input->post('ttd_nama');
		$jenis_ttd = $this->input->post('jenis_ttd');
		$nama_tabel ='tpoliklinik_assesmen';
		$ttd = $this->input->post('signature64');
		$data=array(
			'jenis_ttd' =>$jenis_ttd,
			'ttd_nama' =>$ttd_nama,
			'ttd_hubungan' =>$ttd_hubungan,
			'ttd' =>$ttd,
			'ttd_date' =>date('Y-m-d H:i:s'),
		);
		// print_r($id);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update($nama_tabel,$data);
		$this->output->set_output(json_encode($hasil));
	}
	
	function load_ttd(){
		$assesmen_id = $this->input->post('assesmen_id');
		$q="SELECT H.ttd,H.ttd_date,H.ttd_nama,H.ttd_hubungan,H.jenis_ttd
			FROM tpoliklinik_assesmen H WHERE H.assesmen_id='$assesmen_id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	
	
	//ASMED
	function create_asmed(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_id=$this->input->post('template_id');
	  $idpasien=$this->input->post('idpasien');
	  $q="SELECT H.nama_template_lokasi,H.gambar_tubuh FROM `manatomi_template` H WHERE H.staktif='1' AND H.st_default='1'";
	  $gambar=$this->db->query($q)->row();
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'template_gambar' => $gambar->nama_template_lokasi,
		'gambar_tubuh' => $gambar->gambar_tubuh,
		'idpasien' => $idpasien,
		'template_id' => $template_id,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_asmed',$data);
	  $assesmen_id=$this->db->insert_id();
	  $q="INSERT INTO tpoliklinik_asmed_lokalis_gambar (id,assesmen_id,nama_template_lokasi,gambar_tubuh,st_default)
		SELECT H.id,'$assesmen_id' as assesmen_id, H.nama_template_lokasi,H.gambar_tubuh,H.st_default FROM manatomi_template H
		WHERE H.staktif='1'";
	  $hasil=$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
    }
	function simpan_anatomi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$manatomi_id=$this->input->post('manatomi_id');
		$ket_anatomi=$this->input->post('ket_anatomi');
		$anatomi_id=$this->input->post('anatomi_id');
		if ($this->cek_duplicate_anatomi($assesmen_id,$manatomi_id,$anatomi_id)){
			$data=array(
					'idpasien' =>$idpasien,
					'assesmen_id' =>$assesmen_id,
					'pendaftaran_id' =>$pendaftaran_id,
					'manatomi_id' =>$manatomi_id,
					'ket_anatomi' =>$ket_anatomi,
					'staktif' =>1,
				);
			if ($anatomi_id){
				$data['edited_by']=$login_ppa_id;
				$data['edited_date']=date('Y-m-d H:i:s');
				$this->db->where('id',$anatomi_id);
				$hasil=$this->db->update('tpoliklinik_asmed_anatomi',$data);
				
			}else{
				// $data['template_assesmen_id']=0;
				$data['created_by']=$login_ppa_id;
				$data['created_date']=date('Y-m-d H:i:s');
				$hasil=$this->db->insert('tpoliklinik_asmed_anatomi',$data);
			}
		}else{
			$hasil=false;
		}
	
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_anatomi($assesmen_id,$manatomi_id,$anatomi_id){
		$q="SELECT * FROM `tpoliklinik_asmed_anatomi` WHERE assesmen_id='$assesmen_id' AND manatomi_id='$manatomi_id' AND id !='$anatomi_id'";
		$hasil=$this->db->query($q)->row('id');
		if ($hasil){
			return false;
		}else{
			return true;
		}
	}
	function load_anatomi()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT MP.nama as nama_user,CONCAT(MD.nama,'<br>(',MD.nama_english,')') as anatomi, H.* 
						FROM tpoliklinik_asmed_anatomi H
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN manatomi MD ON MD.id=H.manatomi_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						ORDER BY MD.nourut ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->anatomi);
          $result[] = $r->ket_anatomi;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  $aksi .= '<button onclick="edit_anatomi('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  $aksi .= '<button onclick="hapus_anatomi('.$r->id.')" type="button" title="Hapus Diagnosa" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function edit_anatomi(){
	  $id=$this->input->post('id');
	  $q="SELECT * FROM `tpoliklinik_asmed_anatomi` H WHERE H.id='$id'";
	  $output=$this->db->query($q)->row_array();
	  echo json_encode($output);
  }
  function batal_asmed(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $st_edited=$this->input->post('st_edited');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	   if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
		  
	  }
	  if ($st_edited=='1'){
		
			$q="DELETE FROM tpoliklinik_asmed_diagnosa WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		  $q="DELETE FROM tpoliklinik_asmed_lokalis WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		  $q="DELETE FROM tpoliklinik_asmed_lokalis_gambar WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
			$q="INSERT INTO tpoliklinik_asmed_diagnosa (assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif)
SELECT assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif FROM tpoliklinik_asmed_his_diagnosa WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
		$this->db->query($q);
		
		  $q="INSERT IGNORE  INTO tpoliklinik_asmed_his_riwayat_penyakit_keluarga (versi_edit,assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$jml_edit',assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen
				FROM tpoliklinik_asmed_riwayat_penyakit_keluarga
				WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		  
			$q="INSERT IGNORE INTO tpoliklinik_asmed_lokalis (assesmen_id, gambar_id, `index`, XPos, YPos, `key`, content, warna, keterangan, class_name, path ) SELECT
				assesmen_id,gambar_id,`index`,XPos,YPos,`key`,content,warna,keterangan,class_name,path 
				FROM tpoliklinik_asmed_his_lokalis WHERE assesmen_id = '$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="INSERT IGNORE INTO tpoliklinik_asmed_lokalis_gambar 
					(id,assesmen_id,nama_template_lokasi,gambar_tubuh,st_default,gambar_final)
					SELECT id,assesmen_id,nama_template_lokasi,gambar_tubuh,st_default,gambar_final
					FROM tpoliklinik_asmed_his_lokalis_gambar  WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);  
			  //anatomi
			  
		    $q="DELETE FROM tpoliklinik_asmed_anatomi WHERE assesmen_id='$assesmen_id'  ";
		    $this->db->query($q);
			
			$q="INSERT IGNORE INTO tpoliklinik_asmed_anatomi 
				(assesmen_id,pendaftaran_id,idpasien,manatomi_id,ket_anatomi,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif)
				SELECT assesmen_id,pendaftaran_id,idpasien,manatomi_id,ket_anatomi,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif 
				FROM tpoliklinik_asmed_his_anatomi WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			
			$q="DELETE FROM tpoliklinik_asmed_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_his_anatomi WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_his_edukasi WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_his_diagnosa WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_his_riwayat_penyakit WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_his_riwayat_penyakit_dahulu WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_his_riwayat_penyakit_keluarga WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_his_lokalis WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_his_lokalis_gambar WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
		  $data['status_assemen']='2';
	  }else{
		  $data['status_assemen']='0';
		  
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_asmed',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
  
  function simpan_riwayat_penyakit_asmed(){
		$hasil=false;
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$riwayat_penyakit=$this->input->post('riwayat_penyakit');
		$riwayat_penyakit_dahulu=$this->input->post('riwayat_penyakit_dahulu');
		$riwayat_penyakit_keluarga=$this->input->post('riwayat_penyakit_keluarga');
		$jenis=$this->input->post('jenis');
		// print_r($riwayat_penyakit_keluarga);exit;
		if ($jenis=='1'){
			$this->db->where('assesmen_id',$assesmen_id);
			$this->db->delete('tpoliklinik_asmed_riwayat_penyakit');
			if ($riwayat_penyakit){
				foreach($riwayat_penyakit as $index=>$val){
					$data=array(
						'assesmen_id' =>$assesmen_id,
						'idpasien' =>$idpasien,
						'pendaftaran_id' =>$pendaftaran_id,
						'penyakit_id' =>$val,
					);
					$hasil=$this->db->insert('tpoliklinik_asmed_riwayat_penyakit',$data);
				}
			}
			
		}
		if ($jenis=='2'){
			$this->db->where('assesmen_id',$assesmen_id);
			$this->db->delete('tpoliklinik_asmed_riwayat_penyakit_dahulu');
			if ($riwayat_penyakit_dahulu){
				foreach($riwayat_penyakit_dahulu as $index=>$val){
					$data=array(
						'assesmen_id' =>$assesmen_id,
						'idpasien' =>$idpasien,
						'pendaftaran_id' =>$pendaftaran_id,
						'penyakit_id' =>$val,
					);
					$hasil=$this->db->insert('tpoliklinik_asmed_riwayat_penyakit_dahulu',$data);
				}
			}
			
		}
		if ($jenis=='3'){
			$this->db->where('assesmen_id',$assesmen_id);
			$this->db->delete('tpoliklinik_asmed_riwayat_penyakit_keluarga');
			if ($riwayat_penyakit_keluarga){
				foreach($riwayat_penyakit_keluarga as $index=>$val){
					// print_r($$val);exit;
					$data=array(
						'assesmen_id' =>$assesmen_id,
						'idpasien' =>$idpasien,
						'pendaftaran_id' =>$pendaftaran_id,
						'penyakit_id' =>$val,
					);
					$hasil=$this->db->insert('tpoliklinik_asmed_riwayat_penyakit_keluarga',$data);
				}
			}
			
		}
		$this->output->set_output(json_encode($hasil));
	}
	function save_asmed(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$jml_edit=$this->input->post('jml_edit');
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'status_assemen' => $this->input->post('status_assemen'),
			'st_anamnesa' => $this->input->post('st_anamnesa'),
			'keluhan_utama' => $this->input->post('keluhan_utama'),
			'nama_template' => $this->input->post('nama_template'),
			'nama_anamnesa' => $this->input->post('nama_anamnesa'),
			'hubungan_anamnesa' => $this->input->post('hubungan_anamnesa'),
			'st_riwayat_penyakit' => $this->input->post('st_riwayat_penyakit'),
			'riwayat_penyakit_lainnya' => $this->input->post('riwayat_penyakit_lainnya'),
			'st_riwayat_penyakit_dahulu' => $this->input->post('st_riwayat_penyakit_dahulu'),
			'riwayat_penyakit_dahulu' => $this->input->post('riwayat_penyakit_dahulu'),
			'st_riwayat_penyakit_keluarga' => $this->input->post('st_riwayat_penyakit_keluarga'),
			'riwayat_penyakit_keluarga' => $this->input->post('riwayat_penyakit_keluarga'),
			'catatan_edukasi' => $this->input->post('catatan_edukasi'),
			'pemeriksaan_penunjang' => $this->input->post('pemeriksaan_penunjang'),
			'rencana_asuhan' => $this->input->post('rencana_asuhan'),
			'intruksi' => $this->input->post('intruksi'),
			'rencana_kontrol' => $this->input->post('rencana_kontrol'),
			'tanggal_kontrol' =>($this->input->post('rencana_kontrol')>0?YMDFormat($this->input->post('tanggal_kontrol')):null),
			'idpoli_kontrol' => $this->input->post('idpoli_kontrol'),
			'iddokter_kontrol' => $this->input->post('iddokter_kontrol'),
			'diagnosa_utama' => $this->input->post('diagnosa_utama'),
			'txt_keterangan' => $this->input->post('txt_keterangan'),
		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tpoliklinik_asmed_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_asmed',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_edukasi_asmed(){
		$hasil=false;
		$assesmen_id=$this->input->post('assesmen_id');
		$medukasi_id=$this->input->post('medukasi_id');
		// print_r($medukasi_id);exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->delete('tpoliklinik_asmed_edukasi');
		if ($medukasi_id){
			foreach($medukasi_id as $index=>$val){
				$data=array(
					'assesmen_id' =>$assesmen_id,
					'medukasi_id' =>$val,
				);
				$hasil=$this->db->insert('tpoliklinik_asmed_edukasi',$data);
			}
		}
		
		$this->output->set_output(json_encode($hasil));
	}
	function set_rencana_kontrol(){
		$rencana_kontrol=$this->input->post('rencana_kontrol');
		$tanggal=$this->input->post('tanggal');
		if ($rencana_kontrol>0){
			$date1=date_create($tanggal);
			date_add($date1,date_interval_create_from_date_string("$rencana_kontrol days"));
			$tanggal_kontrol= date_format($date1,"d-m-Y");
		}else{
			$tanggal_kontrol='';
			
		}
		$this->output->set_output(json_encode($tanggal_kontrol));
	}
	function simpan_diagnosa_asmed(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$mdiagnosa_id=$this->input->post('mdiagnosa_id');
		$mdiagnosa_nama=$this->input->post('mdiagnosa_nama');
		if ($mdiagnosa_nama==$mdiagnosa_id){
			$mdiagnosa_id=$this->cek_duplikate_nama_diagonosa($mdiagnosa_nama,$mdiagnosa_id);
		}
		$prioritas=$this->input->post('prioritas');
		$diagnosa_id=$this->input->post('diagnosa_id');
		if ($this->cek_duplicate_diagnosa_asmed($assesmen_id,$mdiagnosa_id,$diagnosa_id,$prioritas)){
			$data=array(
					'idpasien' =>$idpasien,
					'assesmen_id' =>$assesmen_id,
					'pendaftaran_id' =>$pendaftaran_id,
					'mdiagnosa_id' =>$mdiagnosa_id,
					'prioritas' =>$prioritas,
					'staktif' =>1,
				);
			if ($diagnosa_id){
				$data['edited_by']=$login_ppa_id;
				$data['edited_date']=date('Y-m-d H:i:s');
				$this->db->where('id',$diagnosa_id);
				$hasil=$this->db->update('tpoliklinik_asmed_diagnosa',$data);
				
			}else{
				$data['created_by']=$login_ppa_id;
				$data['created_date']=date('Y-m-d H:i:s');
				$hasil=$this->db->insert('tpoliklinik_asmed_diagnosa',$data);
			}
		}else{
			$hasil=false;
		}
	
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplikate_nama_diagonosa($mdiagnosa_nama,$mdiagnosa_id){
		$login_ppa_id=$this->session->userdata('user_id');
		$q="SELECT H.id FROM `mdiagnosa_medis` H WHERE H.nama='$mdiagnosa_nama'";
		$id=$this->db->query($q)->row('id');
		if ($id){
			return $id;
		}else{
			$q="INSERT INTO mdiagnosa_medis(nama,created_by,created_date) VALUES('$mdiagnosa_nama','$login_ppa_id',NOW())";
			$this->db->query($q);
			$id=$this->db->insert_id();
			return $id;
		}
	}
	function cek_duplicate_diagnosa_asmed($assesmen_id,$mdiagnosa_id,$diagnosa_id,$prioritas){
		$x_hasil=true;
		$q="SELECT * FROM `tpoliklinik_asmed_diagnosa` WHERE assesmen_id='$assesmen_id' AND mdiagnosa_id='$mdiagnosa_id' AND id !='$diagnosa_id' AND staktif='1'";
		// print_r($q);exit;
		$hasil=$this->db->query($q)->row('id');
		if ($hasil){
			$x_hasil=false;
		}
		$q="SELECT * FROM `tpoliklinik_asmed_diagnosa` WHERE assesmen_id='$assesmen_id' AND prioritas='$prioritas' AND id !='$diagnosa_id' AND staktif='1'";
		$hasil=$this->db->query($q)->row('id');
		if ($hasil){
			$x_hasil=false;
		}
		
		return $x_hasil;
	}
	function get_last_prioritas_asmed(){
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $q="SELECT COALESCE(MAX(prioritas),0) +1 as prioritas FROM `tpoliklinik_asmed_diagnosa` WHERE assesmen_id='$assesmen_id'";
	  $prioritas=$this->db->query($q)->row('prioritas');
	  $data['prioritas']=$prioritas;
	  echo json_encode($data);
	}
	function load_gambar_asmed(){
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $gambar_id=$this->input->post('gambar_id');
	  $status_assemen=$this->input->post('status_assemen');
	  if ($status_assemen!='2'){
		  $q="UPDATE tpoliklinik_asmed_lokalis_gambar SET st_default=0 WHERE assesmen_id='$assesmen_id' AND st_default='1'";
		  $this->db->query($q);
		  $q="UPDATE tpoliklinik_asmed_lokalis_gambar SET st_default=1 WHERE assesmen_id='$assesmen_id' AND id='$gambar_id'";
		  $this->db->query($q);
	  }
	  $q="SELECT gambar_tubuh FROM `tpoliklinik_asmed_lokalis_gambar` WHERE id='$gambar_id' AND assesmen_id='$assesmen_id'";
	  $gambar_tubuh=$this->db->query($q)->row('gambar_tubuh');
	  $data['gambar_tubuh']=$gambar_tubuh;
	  
	  $q="SELECT *FROM tpoliklinik_asmed_lokalis WHERE assesmen_id='$assesmen_id' AND gambar_id='$gambar_id'";
	  $data['list_lokalis']=$this->db->query($q)->result();
	  
	  echo json_encode($data);
	}
	function load_diagnosa_asmed()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT MP.nama as nama_user,MD.nama as diagnosa, H.* FROM tpoliklinik_asmed_diagnosa H
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN mdiagnosa_medis MD ON MD.id=H.mdiagnosa_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						ORDER BY H.prioritas ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->diagnosa);
          $result[] = $r->prioritas;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  $aksi .= '<button onclick="edit_diagnosa('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  $aksi .= '<button onclick="hapus_diagnosa('.$r->id.')" type="button" title="Hapus Diagnosa" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function get_diagnosa_medis(){
	  $cari 	= $this->input->post('search');
	  $q="SELECT *FROM mdiagnosa_medis WHERE staktif='1' AND nama LIKE '%".$cari."%'";
	  $hasil=$this->db->query($q)->result_array();
	  $this->output->set_output(json_encode($hasil));
  }
  
  function hapus_diagnosa_asmed(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $id=$this->input->post('id');
	  $data['deleted_by']=$login_ppa_id;
	  $data['deleted_date']=date('Y-m-d H:i:s');
	  $data['staktif']=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_asmed_diagnosa',$data);
	  echo json_encode($hasil);
  }
  function edit_diagnosa_asmed(){
	  $id=$this->input->post('id');
	  $q="SELECT H.*,MD.nama  FROM tpoliklinik_asmed_diagnosa H 
		LEFT JOIN mdiagnosa_medis MD ON MD.id=H.mdiagnosa_id
		WHERE H.id='$id'";
	  $output=$this->db->query($q)->row_array();
	  echo json_encode($output);
  }
  function list_history_pengkajian_asmed()
  {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_asmed($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		// print_r($logic_akses_assesmen);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$where='';
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		if ($mppa_id!='#'){
			$where .=" AND H.created_ppa = '".$mppa_id."'";
		}
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_asmed")->row_array();
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa
					,H.assesmen_id,H.pendaftaran_id,MP.tanggaldaftar,MP.nopendaftaran,MPOL.nama as poli 
					,MD.nama as dokter,H.tanggal_input as tanggal_pengkajian
					,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,MP.iddokter,MP.idpoliklinik,H.keterangan_edit,H.jml_edit,H.st_edited
					,H.ttd_nama,H.ttd
					FROM `tpoliklinik_asmed` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					INNER JOIN mpoliklinik MPOL ON MPOL.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
					ORDER BY H.tanggal_input DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
		$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash	text-danger"></i></button>';
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/input_asmed_rj/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
		$btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
		
		 if ($akses_general['st_edit_catatan']=='0'){
			  $btn_edit='';
		  }else{
			  if ($akses_general['lama_edit']>0){
				  if ($r->selisih>$akses_general['lama_edit']){
					  $btn_edit='';
				  }else{
					  
				  }
				  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_edit='';
					  }
				  }
			  }
		  }
		  if ($akses_general['st_duplikasi_catatan']=='0'){
			  $btn_duplikasi='';
		  }else{
			  if ($akses_general['lama_duplikasi']>0){
				  if ($r->selisih>$akses_general['lama_duplikasi']){
					  $btn_duplikasi='';
				  }else{
					 
				  }
			  }
			   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id!=$r->created_ppa){
						$btn_duplikasi='';
				  }
			   }
		  }
		  if ($akses_general['st_hapus_catatan']=='0'){
			  $btn_hapus='';
		  }else{
			  if ($akses_general['lama_hapus']>0){
				  if ($r->selisih>$akses_general['lama_hapus']){
					  $btn_hapus='';
				  }else{
					 
				  }
				   
			  }
			  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id <> $r->created_ppa){
						$btn_hapus='';
				  }else{
					  
				  }
						// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
			  }
		  }
		if ($logic_akses_assesmen['st_edit_ttv']=='0'){
		  $btn_edit='';
		}
		if ($logic_akses_assesmen['st_hapus_ttv']=='0'){
		  $btn_hapus='';
		}
		
		$result[] = $no;
		$aksi='';
		$aksi_edit='';
		
		if ($r->jml_edit>0){
			$aksi_edit='<a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_asmed_rj/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></a>';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_edit;	
		$aksi .= $btn_duplikasi;	
		$aksi .= $btn_hapus;	
		if ($logic_akses_assesmen['st_lihat_ttv']=='1'){
		$aksi .= $btn_lihat;	
		}
		$aksi .= $aksi_edit;	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = HumanDateLong($r->tanggaldaftar).' - '.$r->st_edited;
		$result[] = ($r->nopendaftaran);
		$result[] = ('<strong>'.$r->poli.'</strong><br>'.$r->dokter);
		$result[] = HumanDateLong($r->tanggal_pengkajian);
		$result[] = ($r->nama_mppa).$btn_ttd;
		$result[] = ($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function simpan_ttd_asmed(){
		
		$assesmen_id = $this->input->post('assesmen_id');
		$ttd_hubungan = $this->input->post('ttd_hubungan');
		$ttd_nama = $this->input->post('ttd_nama');
		$jenis_ttd = $this->input->post('jenis_ttd');
		$nama_tabel ='tpoliklinik_asmed';
		$ttd = $this->input->post('signature64');
		$data=array(
			'jenis_ttd' =>$jenis_ttd,
			'ttd_nama' =>$ttd_nama,
			'ttd_hubungan' =>$ttd_hubungan,
			'ttd' =>$ttd,
			'ttd_date' =>date('Y-m-d H:i:s'),
		);
		// print_r($id);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update($nama_tabel,$data);
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_default_warna(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$warna = $this->input->post('warna');
		$jenis_drawing = $this->input->post('jenis_drawing');
		$q="INSERT INTO tpoliklinik_asmed_default(mppa_id,warna,jenis_drawing) VALUES(14,'$warna','$jenis_drawing')
		ON DUPLICATE KEY UPDATE
		warna='$warna',jenis_drawing='$jenis_drawing'";
		
		$hasil=$this->db->query($q);
		$this->output->set_output(json_encode($hasil));
	}
	function load_ttd_asmed(){
		$assesmen_id = $this->input->post('assesmen_id');
		$q="SELECT H.ttd,H.ttd_date,H.ttd_nama,H.ttd_hubungan,H.jenis_ttd
			FROM tpoliklinik_asmed H WHERE H.assesmen_id='$assesmen_id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function create_template_asmed(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_id=$this->input->post('template_id');
	  $idpasien=$this->input->post('idpasien');
	  $q="SELECT H.nama_template_lokasi,H.gambar_tubuh FROM `manatomi_template` H WHERE H.staktif='1' AND H.st_default='1'";
	  $gambar=$this->db->query($q)->row();
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'template_gambar' => $gambar->nama_template_lokasi,
		'gambar_tubuh' => $gambar->gambar_tubuh,
		'idpasien' => $idpasien,
		'template_id' => $template_id,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 3,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_asmed',$data);
	  $assesmen_id=$this->db->insert_id();
	  $q="INSERT INTO tpoliklinik_asmed_lokalis_gambar (id,assesmen_id,nama_template_lokasi,gambar_tubuh,st_default)
		SELECT H.id,'$assesmen_id' as assesmen_id, H.nama_template_lokasi,H.gambar_tubuh,H.st_default FROM manatomi_template H
		WHERE H.staktif='1'";
	  $hasil=$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
    }
	function batal_template_asmed(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nama_template=$this->input->post('nama_template');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	  if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
	  }
	  if ($nama_template){
		  $data['status_assemen']='4';
		  $data['nama_template']=$nama_template;
	  }else{
		  
		  $data['status_assemen']='0';
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_asmed',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
  function list_index_template_asmed()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
						 FROM tpoliklinik_asmed H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.template_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		$result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nama_template);
		$result[] = ($r->jumlah_template);
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_record_asmed(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_asmed',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	  function edit_template_asmed(){
	  $assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'status_assemen' => 3,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_asmed',$data);
		
		$data_detail=array(
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_asmed_diagnosa',$data_detail);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_asmed_riwayat_penyakit',$data_detail);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_asmed_riwayat_penyakit_dahulu',$data_detail);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_asmed_riwayat_penyakit_keluarga',$data_detail);
		
		
		$this->output->set_output(json_encode($result));
  }
  function create_with_template_asmed(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $idpasien=$this->input->post('idpasien');
	  $q="INSERT INTO tpoliklinik_asmed (
			template_assesmen_id,tanggal_input,pendaftaran_id,created_date,created_ppa,template_id,status_assemen,idpasien
			,st_anamnesa,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,st_riwayat_penyakit_dahulu,riwayat_penyakit_dahulu,st_riwayat_penyakit_keluarga,riwayat_penyakit_keluarga,template_gambar,gambar_tubuh,pemeriksaan_penunjang,rencana_asuhan,intruksi,rencana_kontrol,tanggal_kontrol,idpoli_kontrol,iddokter_kontrol,st_edukasi,catatan_edukasi,diagnosa_utama
			,txt_keterangan,gambar_id)
			SELECT '$template_assesmen_id',NOW() as tanggal_input,'$pendaftaran_id',NOW() as created_date,'$login_ppa_id',template_id,'1' as status_assemen,'$idpasien'
			,st_anamnesa,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,st_riwayat_penyakit_dahulu,riwayat_penyakit_dahulu,st_riwayat_penyakit_keluarga,riwayat_penyakit_keluarga,template_gambar,gambar_tubuh,pemeriksaan_penunjang,rencana_asuhan,intruksi,rencana_kontrol,tanggal_kontrol,idpoli_kontrol,iddokter_kontrol,st_edukasi,catatan_edukasi,diagnosa_utama
			,txt_keterangan,gambar_id
			FROM `tpoliklinik_asmed`

			WHERE assesmen_id='$template_assesmen_id'";
	  // print_r($q);exit;
	  $hasil=$this->db->query($q);
	  $assesmen_id=$this->db->insert_id();
	  $data=$this->db->query("SELECT template_id,idpasien from tpoliklinik_asmed where assesmen_id='$assesmen_id'")->row();
	  if ($data){
		  $idpasien=$data->idpasien;
		  $template_id=$data->template_id;
		  // $q="INSERT INTO tpoliklinik_assesmen_risiko_jatuh (assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai)
				// SELECT '$assesmen_id',parameter_nama,group_nilai,param_nilai_id,nilai
				// FROM tpoliklinik_assesmen_risiko_jatuh WHERE assesmen_id='$template_assesmen_id'";
		  // $this->db->query($q);
		  //RIWAYAT PENYAKIT
		  
		  $q="INSERT INTO tpoliklinik_asmed_lokalis (assesmen_id, gambar_id, `index`, XPos, YPos, `key`, content, warna, keterangan, class_name, path ) SELECT
				'$assesmen_id',gambar_id,`index`,XPos,YPos,`key`,content,warna,keterangan,class_name,path 
				FROM tpoliklinik_asmed_lokalis WHERE assesmen_id = '$template_assesmen_id'";
			$this->db->query($q);
			
			$q="INSERT IGNORE INTO tpoliklinik_asmed_lokalis_gambar 
					(id,assesmen_id,nama_template_lokasi,gambar_tubuh,st_default,gambar_final)
					SELECT id,'$assesmen_id',nama_template_lokasi,gambar_tubuh,st_default,gambar_final
					FROM tpoliklinik_asmed_lokalis_gambar  WHERE assesmen_id='$template_assesmen_id'";
			$this->db->query($q);  
			
			
		  $q="INSERT INTO tpoliklinik_asmed_riwayat_penyakit (assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$assesmen_id','$pendaftaran_id',penyakit_id,'$idpasien',status_assemen
				FROM tpoliklinik_asmed_riwayat_penyakit
				WHERE assesmen_id='$template_assesmen_id'";
		  $this->db->query($q);
		  $q="INSERT INTO tpoliklinik_asmed_riwayat_penyakit_dahulu (assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$assesmen_id','$pendaftaran_id',penyakit_id,'$idpasien',status_assemen
				FROM tpoliklinik_asmed_riwayat_penyakit_dahulu
				WHERE assesmen_id='$template_assesmen_id'";
		  $this->db->query($q);
		  $q="INSERT INTO tpoliklinik_asmed_riwayat_penyakit_keluarga (assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$assesmen_id','$pendaftaran_id',penyakit_id,'$idpasien',status_assemen
				FROM tpoliklinik_asmed_riwayat_penyakit_keluarga
				WHERE assesmen_id='$template_assesmen_id'";
		  $this->db->query($q);

		  //RIWAYAT EDUKASI
		  $q="INSERT INTO tpoliklinik_asmed_edukasi(assesmen_id,medukasi_id)
				SELECT '$assesmen_id',medukasi_id FROM tpoliklinik_asmed_edukasi
				WHERE assesmen_id='$template_assesmen_id'";
		  $this->db->query($q);

		  //anatomi
		  $q="INSERT INTO tpoliklinik_asmed_anatomi(assesmen_id,pendaftaran_id,idpasien,manatomi_id,ket_anatomi,created_by,created_date)
				SELECT '$assesmen_id','$pendaftaran_id','$idpasien',manatomi_id,ket_anatomi,'$login_ppa_id',NOW() FROM tpoliklinik_asmed_anatomi
				WHERE assesmen_id='$template_assesmen_id'";
		  $this->db->query($q);
		  
		  $q="SELECT *FROM tpoliklinik_asmed_diagnosa WHERE assesmen_id='$template_assesmen_id'";
		  $row=$this->db->query($q)->result();
		 
					
		  foreach($row as $r){
			  $data_input=array(
					'idpasien' =>$idpasien,
					'assesmen_id' =>$assesmen_id,
					'pendaftaran_id' =>$pendaftaran_id,
					'mdiagnosa_id' =>$r->mdiagnosa_id,
					'prioritas' =>$r->prioritas,
					'created_by' =>$login_ppa_id,
					'created_date' =>date('Y-m-d H:i:s'),
					'staktif' =>1,
			  );
			  $this->db->insert('tpoliklinik_asmed_diagnosa',$data_input);
		  }
		
		  //Update jumlah_template
		  $q="UPDATE tpoliklinik_asmed 
				SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
				$this->db->query($q);
	  }
	  $this->output->set_output(json_encode($hasil));
    }
	function save_edit_asmed(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_edit_id=$this->input->post('alasan_id');
		$keterangan_edit=$this->input->post('keterangan_edit');
		$jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_asmed WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
		// $jml_edit=$this->input->post('jml_edit');
		$res=$this->simpan_history_asmed($assesmen_id,$jml_edit);
		// print_r($res);exit;
		if ($res){
			
			$data=array(
				'status_assemen' => 1,
				'st_edited' => 1,
				'alasan_edit_id' =>$alasan_edit_id,
				'keterangan_edit' =>$keterangan_edit,

			);
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tpoliklinik_asmed',$data);
			// $ttv_id=$this->db->insert_id();
			
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}else{
			$hasil=$res;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_status_lokalis(){
		$gambar_id=$this->input->post('gambar_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$gambar_final=$this->input->post('gambar_final');
		$jsonString=($this->input->post('jsonString'));
		// print_r($jsonString);exit;
		$i=0;
		$this->db->where('gambar_id',$gambar_id);
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->delete('tpoliklinik_asmed_lokalis');
		foreach($jsonString as $r){
			$data=array(
				'assesmen_id'=>$assesmen_id,
				'gambar_id'=>$gambar_id,
				'index'=>$i,
				'XPos'=>$r['XPos'],
				'YPos'=>$r['YPos'],
				'key'=>$r['key'],
				// 'content'=>$r['content'],
				'path'=>json_encode($r['path']),
				'warna'=>$r['warna'],
				'keterangan'=>$r['keterangan'],
				'class_name'=>$r['class_name'],
				'path'=>json_encode($r['path']),
			);
			$hasil=$this->db->insert('tpoliklinik_asmed_lokalis',$data);
			$i=$i+1;
			// print_r($r['keterangan']);exit;
		}
		$q="UPDATE tpoliklinik_asmed_lokalis_gambar SET gambar_final='$gambar_final' where assesmen_id='$assesmen_id' AND id='$gambar_id'";
		$hasil=$this->db->query($q);
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_history_asmed($assesmen_id,$jml_edit){
		$hasil=false;
		
		// $jml_edit=$jml_edit+1;
		$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tpoliklinik_asmed_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
		// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
		if ($baris==0){
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$q="INSERT IGNORE INTO tpoliklinik_asmed_his 
						(versi_edit,assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_anamnesa
						,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,st_riwayat_penyakit_dahulu,riwayat_penyakit_dahulu,st_riwayat_penyakit_keluarga,riwayat_penyakit_keluarga,template_gambar,gambar_tubuh,txt_keterangan,gambar_id,pemeriksaan_penunjang,rencana_asuhan,intruksi,rencana_kontrol,tanggal_kontrol,idpoli_kontrol,iddokter_kontrol,st_edukasi,catatan_edukasi,diagnosa_utama
						,edited_ppa,edited_date )
				SELECT '$jml_edit',assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_anamnesa
				,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,st_riwayat_penyakit_dahulu,riwayat_penyakit_dahulu,st_riwayat_penyakit_keluarga,riwayat_penyakit_keluarga,template_gambar,gambar_tubuh,txt_keterangan,gambar_id,pemeriksaan_penunjang,rencana_asuhan,intruksi,rencana_kontrol,tanggal_kontrol,idpoli_kontrol,iddokter_kontrol,st_edukasi,catatan_edukasi,diagnosa_utama
				,'$login_ppa_id',NOW() FROM tpoliklinik_asmed 
				WHERE assesmen_id='$assesmen_id'";
			// print_r($q);exit;
			$hasil=$this->db->query($q);
			
			//ALERGI
			$q="INSERT IGNORE INTO tpoliklinik_asmed_his_anatomi 
				(versi_edit,assesmen_id,pendaftaran_id,idpasien,manatomi_id,ket_anatomi,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif)
				SELECT '$jml_edit',assesmen_id,pendaftaran_id,idpasien,manatomi_id,ket_anatomi,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif FROM tpoliklinik_asmed_anatomi WHERE assesmen_id='$assesmen_id' AND staktif='1'";
			$this->db->query($q);
			
			 $q="INSERT IGNORE INTO tpoliklinik_asmed_his_edukasi(versi_edit,assesmen_id,medukasi_id)
				SELECT '$jml_edit',assesmen_id,medukasi_id FROM tpoliklinik_asmed_edukasi
				WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
			
			//DIAGNOSA
			$q="INSERT IGNORE INTO tpoliklinik_asmed_his_diagnosa (id,versi_edit,assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif)
	SELECT id,'$jml_edit',assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif FROM tpoliklinik_asmed_diagnosa WHERE assesmen_id='$assesmen_id' AND staktif='1'";
			$this->db->query($q);
			
			 // //RIWAYAT PENYAKIT
		   $q="INSERT IGNORE  INTO tpoliklinik_asmed_his_riwayat_penyakit (versi_edit,assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$jml_edit',assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen
				FROM tpoliklinik_asmed_riwayat_penyakit
				WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		  $q="INSERT IGNORE  INTO tpoliklinik_asmed_his_riwayat_penyakit_dahulu (versi_edit,assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$jml_edit',assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen
				FROM tpoliklinik_asmed_riwayat_penyakit_dahulu
				WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  $q="INSERT IGNORE  INTO tpoliklinik_asmed_his_riwayat_penyakit_keluarga (versi_edit,assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$jml_edit',assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen
				FROM tpoliklinik_asmed_riwayat_penyakit_keluarga
				WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		  
			$q="INSERT IGNORE INTO tpoliklinik_asmed_his_lokalis ( versi_edit, assesmen_id, gambar_id, `index`, XPos, YPos, `key`, content, warna, keterangan, class_name, path ) SELECT
				'$jml_edit',assesmen_id,gambar_id,`index`,XPos,YPos,`key`,content,warna,keterangan,class_name,path 
				FROM tpoliklinik_asmed_lokalis WHERE assesmen_id = '$assesmen_id'";
			$this->db->query($q);
			
			$q="INSERT IGNORE INTO tpoliklinik_asmed_his_lokalis_gambar 
					(versi_edit,id,assesmen_id,nama_template_lokasi,gambar_tubuh,st_default,gambar_final)
					SELECT '$jml_edit',id,assesmen_id,nama_template_lokasi,gambar_tubuh,st_default,gambar_final
					FROM tpoliklinik_asmed_lokalis_gambar WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			
		}
	  return $hasil;
	}
	//NYERI
	function batal_template_nyeri(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nama_template=$this->input->post('nama_template');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	  if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
	  }
	  if ($nama_template){
		  $data['status_assemen']='4';
		  $data['nama_template']=$nama_template;
	  }else{
		  
		  $data['status_assemen']='0';
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_nyeri',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
	function create_nyeri(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $mnyeri_id=$this->input->post('mnyeri_id');
	  $idpasien=$this->input->post('idpasien');
	  $q="SELECT H.nama_template_lokasi,H.gambar_tubuh FROM `manatomi_template` H WHERE H.staktif='1' AND H.st_default='1'";
	  $gambar=$this->db->query($q)->row();
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'idpasien' => $idpasien,
		'mnyeri_id' => $mnyeri_id,
		'total_skor_nyeri' => 0,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_nyeri',$data);
	  $assesmen_id=$this->db->insert_id();
	  
	  
	  $this->output->set_output(json_encode($hasil));
    }
	
	
  function load_nrs(){
	$assesmen_id=$this->input->post('assesmen_id');
	  $q="SELECT * FROM tpoliklinik_nyeri_scale_number WHERE assesmen_id='$assesmen_id' order by nourut";
	  $row=$this->db->query($q)->result();
	  $detail='';
	  $detail .='<tr>';
	  $ket_awal='';
	  $warna_awal='';
	  $nama_tabel="'tpoliklinik_nyeri_scale_number'";
	  $index_arr=0;
	  $jml_kol=1;
	  $array_ket=array();;
	  $array_kol=array();;
	  $array_warna=array();;
	  foreach($row as $r){
		  $detail .='<td class="border-full" style="background-color:bg-gray-lighter">
					<label class="css-input css-radio css-radio-primary push-10-r">
					<input type="radio" name="nrs_opsi" '.($r->pilih?'checked':'').' onclick="set_skala_nyeri('.$r->id.','.$r->nilai.','.$nama_tabel.')"><span></span> <strong>'.$r->nilai.'</strong>
					</label>';
		  $detail .='</td>';
		  if ($ket_awal!=$r->keterangan){
			  if ($ket_awal != ''){
					$array_ket[$index_arr]=$ket_awal;
					$array_kol[$index_arr]=$jml_kol;
					$array_warna[$index_arr]=$warna_awal;
					$ket_awal=$r->keterangan;
					$warna_awal=$r->warna;
					$index_arr=$index_arr+1;;
			  }else{
				  $ket_awal=$r->keterangan;
				  $warna_awal=$r->warna;
			  }
			  $jml_kol=1;
		  }else{
			  $jml_kol=$jml_kol+1;
		  }
	  }
	  $array_ket[$index_arr]=$ket_awal;
	  $array_kol[$index_arr]=$jml_kol;
	  $array_warna[$index_arr]=$warna_awal;
	  $detail .='</tr>';
	  $detail .='<tr>';
	  foreach($row as $r){
		  $detail .='<td class="border-full"  style="background-color:'.$r->warna.'">';
		  $detail .='</td>';
	  }
	  $detail .='</tr>';
	  $detail .='<tr>';
	  foreach($array_ket as $index=>$val){
		  $detail .='<td class="border-full" colspan="'.$array_kol[$index].'" style="background-color:'.$array_warna[$index].'"><strong>'.$val.'</strong></td>';
	  }
	  $detail .='</tr>';
	  
	   $tabel='
		<table class="block-table text-center " style="width:100%">
			<tbody>
				'.$detail.'
			</tbody>
		</table>
	  ';
	  
	  $nama_tabel="'tpoliklinik_nyeri_scale_face'";
	  $q="SELECT * FROM tpoliklinik_nyeri_scale_face WHERE assesmen_id='$assesmen_id' order by nourut";
	  $row=$this->db->query($q)->result();
	  $detail ='';
	  $detail .='<tr>';
	   foreach($row as $r){
		  $detail .='<td >';
		  $detail .='<img src="'.site_url().'assets/upload/wbf/'.$r->gambar.'" style="width:100px;height:100px" alt="Avatar">';
		  $detail .='</td>';
	  }
	  $detail .='</tr>';
	  $detail .='<tr>';
	   foreach($row as $r){
		  $detail .='<td class="border-full" >';
		  $detail .='<label class="css-input css-radio css-radio-primary push-10-r">
					<input type="radio"  '.($r->pilih?'checked':'').' onclick="set_skala_nyeri('.$r->id.','.$r->nilai.','.$nama_tabel.')" name="nrs_opsi" value="'.$r->nilai.'"><span></span> <strong>'.$r->nilai.'</strong>
					</label>';
		  $detail .='</td>';
	  }
	  $detail .='</tr>';
	  $tabel .='
		<table class="block-table text-center " style="width:100%">
			<tbody>
				'.$detail.'
			</tbody>
		</table>
	  ';
	  // print_r($array_warna);exit;
	  $this->output->set_output(json_encode($tabel));
  }
   function set_skala_nyeri(){
		$assesmen_id=$this->input->post('assesmen_id');
		$nama_tabel=$this->input->post('nama_tabel');
		$trx_id=$this->input->post('trx_id');
		$trx_nilai=$this->input->post('trx_nilai');
		$q="UPDATE tpoliklinik_nyeri_scale_face SET pilih=0 WHERE assesmen_id='$assesmen_id' AND pilih='1'";
		$this->db->query($q);
		$q="UPDATE tpoliklinik_nyeri_scale_number SET pilih=0 WHERE assesmen_id='$assesmen_id' AND pilih='1'";
		$this->db->query($q);
		$q="UPDATE tpoliklinik_nyeri SET total_skor_nyeri='".$trx_nilai."' WHERE assesmen_id='$assesmen_id'";
		$this->db->query($q);
		
		$data=array(
			'pilih' => 1,
			);
		$this->db->where('id',$trx_id);
		$result=$this->db->update($nama_tabel,$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
	
		$this->output->set_output(json_encode($hasil));
	}
	
	function save_nyeri(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$jml_edit=$this->input->post('jml_edit');
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'nama_template' => $this->input->post('nama_template'),
			'status_assemen' => $this->input->post('status_assemen'),
			'lokasi_nyeri' => $this->input->post('lokasi_nyeri'),
			'penyebab_nyeri' => $this->input->post('penyebab_nyeri'),
			'durasi_nyeri' => $this->input->post('durasi_nyeri'),
			'frek_nyeri' => $this->input->post('frek_nyeri'),
			'jenis_nyeri' => $this->input->post('jenis_nyeri'),
			'skala_nyeri' => $this->input->post('skala_nyeri'),
			'karakteristik_nyeri' => $this->input->post('karakteristik_nyeri'),
			'nyeri_hilang' => $this->input->post('nyeri_hilang'),
			'total_skor_nyeri' => $this->input->post('total_skor_nyeri'),
			
		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tpoliklinik_nyeri_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_nyeri',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	 function load_nilai_param_nyeri(){
		 
		 $assesmen_id=$this->input->post('assesmen_id');
		 
		  $q="SELECT *FROM tpoliklinik_nyeri_param WHERE assesmen_id='$assesmen_id' ORDER BY id ASC";
		  $list_data=$this->db->query($q)->result();
		  $tabel='';
		  $nourut=1;
		  $total_skor=0;
		  foreach ($list_data as $row){
			  $warna='#fff';
			  // if ($row->st_header){
				// $warna='#e5eeff';  
			  // }
				$tabel .='<tr style="background-color:'.$warna.'">';
				$tabel .='<td><input type="hidden" class="parameter_id" value="'.$row->id.'">'.$nourut.'</td>';
				$tabel .='<td class="text-bold"><strong>'.$row->parameter_nama.'</strong></td>';
				$tabel .='<td>'.$this->opsi_nilai_nyeri($row->id,$row->jawaban_id).'</td>';
				$tabel .='<td class="text-bold"><input readonly type="text" class="form-control parameter_skor" value="'.$row->jawaban_skor.'"></td>';
				$nourut=$nourut+1;
				$total_skor=$total_skor+$row->jawaban_skor;
			  $tabel .='</tr>';
		  }
		  $tabel.='<tr><td colspan="3" class="text-primary text-right"><strong>TOTAL SKOR SKALA NYERI</strong></td><td><input readonly type="text" class="form-control total_skor" value="'.$total_skor.'"></td></tr>';
		  
		  $arr['tabel']=$tabel;
		  $arr['total_skor']=$total_skor;
		  $this->output->set_output(json_encode($arr));
	  }
	  function opsi_nilai_nyeri($parameter_id,$jawaban_id){
		  $q="SELECT *FROM tpoliklinik_nyeri_param_skor WHERE parameter_id='$parameter_id' ORDER BY id ASC";
		  $list_nilai=$this->db->query($q)->result();
		  $opsi='<select class="js-select2 form-control nilai_nyeri" style="width: 100%;">';
		  $opsi .='<option value="0" '.($jawaban_id==''?'selected':'').'>Pilih Jawaban</option>'; 
		  foreach($list_nilai as $row){
				 $opsi .='<option value="'.$row->id.'" '.($jawaban_id==$row->id?'selected':'').'>'.$row->deskripsi_nama.' ('.$row->skor.')</option>'; 
		  }
		  $opsi .="</select>";
		  return $opsi;
	  }
	  function update_nilai_nyeri(){
		$assesmen_id=$this->input->post('assesmen_id');
		$parameter_id=$this->input->post('parameter_id');
		$nilai_id=$this->input->post('nilai_id');
		
		
		$data=array(
			'jawaban_id' => $nilai_id,
			);
		$this->db->where('id',$parameter_id);
		$result=$this->db->update('tpoliklinik_nyeri_param',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
	}
	
	function list_history_pengkajian_nyeri()
    {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_nyeri($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		// print_r($logic_akses_assesmen);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$where='';
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		if ($mppa_id!='#'){
			$where .=" AND H.created_ppa = '".$mppa_id."'";
		}
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_nyeri")->row_array();
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa
					,H.assesmen_id,H.pendaftaran_id,MP.tanggaldaftar,MP.nopendaftaran,MPOL.nama as poli 
					,MD.nama as dokter,H.tanggal_input as tanggal_pengkajian
					,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,MP.iddokter,MP.idpoliklinik,H.keterangan_edit,H.jml_edit,H.st_edited
					,H.ttd_nama,H.ttd,H.singkatan_kajian,H.total_skor_nyeri
					FROM `tpoliklinik_nyeri` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					INNER JOIN mpoliklinik MPOL ON MPOL.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
					ORDER BY H.tanggal_input DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','singkatan_kajian');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
			$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
			$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/input_nyeri_rj/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			// $btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
			$btn_ttd='';
			 if ($akses_general['st_edit_catatan']=='0'){
				  $btn_edit='';
			  }else{
				  if ($akses_general['lama_edit']>0){
					  if ($r->selisih>$akses_general['lama_edit']){
						  $btn_edit='';
					  }else{
						  
					  }
					  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id!=$r->created_ppa){
								$btn_edit='';
						  }
					  }
				  }
			  }
			  if ($akses_general['st_duplikasi_catatan']=='0'){
				  $btn_duplikasi='';
			  }else{
				  if ($akses_general['lama_duplikasi']>0){
					  if ($r->selisih>$akses_general['lama_duplikasi']){
						  $btn_duplikasi='';
					  }else{
						 
					  }
				  }
				   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_duplikasi='';
					  }
				   }
			  }
			  if ($akses_general['st_hapus_catatan']=='0'){
				  $btn_hapus='';
			  }else{
				  if ($akses_general['lama_hapus']>0){
					  if ($r->selisih>$akses_general['lama_hapus']){
						  $btn_hapus='';
					  }else{
						 
					  }
					   
				  }
				  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id <> $r->created_ppa){
							$btn_hapus='';
					  }else{
						  
					  }
							// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
				  }
			  }
			if ($logic_akses_assesmen['st_edit_nyeri']=='0'){
			  $btn_edit='';
			}
			if ($logic_akses_assesmen['st_hapus_nyeri']=='0'){
			  $btn_hapus='';
			}
			
			$result[] = $no;
			$aksi='';
			$aksi_edit='';
			
			if ($r->jml_edit>0){
				$aksi_edit='<a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_nyeri_rj/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></a>';
			}
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_duplikasi;	
			$aksi .= $btn_hapus;	
			if ($logic_akses_assesmen['st_lihat_nyeri']=='1'){
			$aksi .= $btn_lihat;	
			}
			$aksi .= $aksi_edit;	
			$aksi .= '</div>';
			$result[] = $aksi;
			$result[] = HumanDateLong($r->tanggaldaftar);
			$result[] = ($r->nopendaftaran);
			$result[] = ('<strong>'.$r->poli.'</strong><br>'.$r->dokter);
			$result[] = HumanDateLong($r->tanggal_pengkajian).'<br>'.text_success($r->singkatan_kajian).' '.text_danger($r->total_skor_nyeri);
			$result[] = ($r->nama_mppa).$btn_ttd;
			$result[] = ($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
			$data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
		}
		
		function save_edit_nyeri(){
			$assesmen_id=$this->input->post('assesmen_id');
			$alasan_edit_id=$this->input->post('alasan_id');
			$keterangan_edit=$this->input->post('keterangan_edit');
			$jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_nyeri WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// $jml_edit=$this->input->post('jml_edit');
			$res=$this->simpan_history_nyeri($assesmen_id,$jml_edit);
			// print_r($res);exit;
			if ($res){
				
				$data=array(
					'status_assemen' => 1,
					'st_edited' => 1,
					'alasan_edit_id' =>$alasan_edit_id,
					'keterangan_edit' =>$keterangan_edit,

				);
				$login_ppa_id=$this->session->userdata('login_ppa_id');
				$data['edited_ppa']=$login_ppa_id;
				$data['edited_date']=date('Y-m-d H:i:s');
				$this->db->where('assesmen_id',$assesmen_id);
				$result=$this->db->update('tpoliklinik_nyeri',$data);
				// $ttv_id=$this->db->insert_id();
				
				if ($result){
					$hasil=$data;
				}else{
					$hasil=false;
				}
			}else{
				$hasil=$res;
			}
			$this->output->set_output(json_encode($hasil));
		}
		function simpan_history_nyeri($assesmen_id,$jml_edit){
			$hasil=false;
			// $jml_edit=$jml_edit+1;
			$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tpoliklinik_nyeri_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
			// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
			if ($baris==0){
				$login_ppa_id=$this->session->userdata('login_ppa_id');
				$q="INSERT IGNORE INTO tpoliklinik_nyeri_his 
							(versi_edit,assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,mnyeri_id,status_assemen,nilai_nyeri_number,nilai_nyeri_face,lokasi_nyeri,penyebab_nyeri,durasi_nyeri,frek_nyeri,jenis_nyeri,skala_nyeri,karakteristik_nyeri,nyeri_hilang,total_skor_nyeri,nama_kajian,singkatan_kajian,isi_header,isi_footer,edited_ppa,edited_date,alasan_edit_id,keterangan_edit)
					SELECT '$jml_edit'versi_edit,assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,mnyeri_id,status_assemen,nilai_nyeri_number,nilai_nyeri_face,lokasi_nyeri,penyebab_nyeri,durasi_nyeri,frek_nyeri,jenis_nyeri,skala_nyeri,karakteristik_nyeri,nyeri_hilang,total_skor_nyeri,nama_kajian,singkatan_kajian,isi_header,isi_footer
					,'$login_ppa_id',NOW(),alasan_edit_id,keterangan_edit FROM tpoliklinik_nyeri 
					WHERE assesmen_id='$assesmen_id'";
				// print_r($q);exit;
				$hasil=$this->db->query($q);
				
			  
			}
		  return $hasil;
		}
		
		function batal_nyeri(){
		  $login_ppa_id=$this->session->userdata('login_ppa_id');
		  $pendaftaran_id=$this->input->post('pendaftaran_id');
		  $assesmen_id=$this->input->post('assesmen_id');
		  $st_edited=$this->input->post('st_edited');
		  $jml_edit=$this->input->post('jml_edit');
		  $data=array(
			'deleted_ppa' => $login_ppa_id,
			'deleted_date' => date('Y-m-d H:i:s'),
			
		  );
		   if ($jml_edit=='0'){
			  $data['edited_ppa']=null;
			  $data['edited_date']=null;
			  $data['alasan_edit_id']='';
			  $data['keterangan_edit']='';
			  
		  }
		  if ($st_edited=='1'){
				$q="SELECT * FROM tpoliklinik_nyeri_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				$data_his_edit=$this->db->query($q)->row();
				if ($data_his_edit){
					$data['edited_ppa']=$data_his_edit->edited_ppa;
					$data['edited_date']=$data_his_edit->edited_date;
					$data['alasan_edit_id']=$data_his_edit->alasan_edit_id;
					$data['keterangan_edit']=$data_his_edit->keterangan_edit;
				}
				
				
				$q="DELETE FROM tpoliklinik_nyeri_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				$this->db->query($q);
				
				
			  $data['status_assemen']='2';
		  }else{
			  $data['status_assemen']='0';
			  
		  }
		  $this->db->where('assesmen_id',$assesmen_id);
		  $hasil=$this->db->update('tpoliklinik_nyeri',$data);
		  $this->output->set_output(json_encode($hasil));
	  }
  function create_with_template_nyeri(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $q="INSERT INTO tpoliklinik_nyeri (
			template_assesmen_id,nama_template,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,mnyeri_id,status_assemen,nilai_nyeri_number,nilai_nyeri_face,lokasi_nyeri,penyebab_nyeri,durasi_nyeri,frek_nyeri,jenis_nyeri,skala_nyeri,karakteristik_nyeri,nyeri_hilang,total_skor_nyeri,nama_kajian,singkatan_kajian,isi_header,isi_footer
			)
			SELECT '$template_assesmen_id',nama_template,NOW(),pendaftaran_id,idpasien,NOW(),'$login_ppa_id',mnyeri_id,1,nilai_nyeri_number,nilai_nyeri_face,lokasi_nyeri,penyebab_nyeri,durasi_nyeri,frek_nyeri,jenis_nyeri,skala_nyeri,karakteristik_nyeri,nyeri_hilang,total_skor_nyeri,nama_kajian,singkatan_kajian,isi_header,isi_footer
			FROM `tpoliklinik_nyeri`

			WHERE assesmen_id='$template_assesmen_id'";
	  $hasil=$this->db->query($q);
	 
	  $assesmen_id=$this->db->insert_id();
	  $q="UPDATE tpoliklinik_nyeri_param H INNER JOIN(
			SELECT H.id,D.jawaban_skor,J.id as jawaban_id FROM tpoliklinik_nyeri_param H
			LEFT JOIN tpoliklinik_nyeri_param D ON D.assesmen_id='$template_assesmen_id' AND D.param_id=H.param_id
			LEFT JOIN tpoliklinik_nyeri_param_skor J ON J.parameter_id=H.id AND J.skor=D.jawaban_skor
			WHERE H.assesmen_id='$assesmen_id'
			) T  ON T.id=H.id
			SET H.jawaban_id=T.jawaban_id";
			$this->db->query($q);
	  //Update jumlah_template
	  $q="UPDATE tpoliklinik_nyeri 
			SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
			$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
   }
   function hapus_record_nyeri(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_nyeri',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function list_index_template_nyeri()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template,H.singkatan_kajian
						 FROM tpoliklinik_nyeri H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.assesmen_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		$result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nama_template) .' ('.$r->singkatan_kajian.')';
		$result[] = ($r->jumlah_template);
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }	
  function create_template_nyeri(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $mnyeri_id=$this->input->post('mnyeri_id');
	  $idpasien=$this->input->post('idpasien');
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'idpasien' => $idpasien,
		'mnyeri_id' => $mnyeri_id,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 3,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_nyeri',$data);
	  $this->output->set_output(json_encode($hasil));
    }
	
	function edit_template_nyeri(){
	  $assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'status_assemen' => 3,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_nyeri',$data);
		
		$this->output->set_output(json_encode($result));
  }
  
  //TRIAGE
  function create_triage(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
	  
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'tanggal_datang' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'idpasien' => $idpasien,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_triage',$data);
	  $assesmen_id=$this->db->insert_id();
	  $q="INSERT IGNORE INTO tpoliklinik_triage_kat (assesmen_id,mtriage_id,nama,nama_lain,warna,respon_time,jml_detail)
		SELECT '$assesmen_id' as assesmen_id,H.id as kat_id,H.nama,H.nama_lain,H.warna,H.respon_time,COUNT(D.mtriage_id) as jml_detail
		FROM `mtriage` H
		LEFT JOIN mtriage_setting D ON D.mtriage_id=H.id AND D.staktif='1'
		WHERE H.staktif='1'
		GROUP BY H.id";
	  $this->db->query($q);
	  $q="INSERT INTO tpoliklinik_triage_kat_detail (assesmen_id,kat_id,mtriage_pemeriksaan_id,pemeriksaan,nilai,penilaian,pilih)
		SELECT '$assesmen_id' as assesmen_id,H.mtriage_id as kat_id,H.mtriage_pemeriksaan_id
		,M.nama as pemeriksaan,H.nilai,H.penilaian,0 as pilih
		FROM mtriage_setting H
		LEFT JOIN mtriage_pemeriksaan M ON M.id=H.mtriage_pemeriksaan_id
		WHERE H.staktif='1'
		ORDER BY H.mtriage_id,H.mtriage_pemeriksaan_id ";
	  $this->db->query($q);
	  
	  $this->output->set_output(json_encode($hasil));
    }
	function create_template_triage(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'tanggal_datang' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'idpasien' => $idpasien,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 3,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_triage',$data);
	  $assesmen_id=$this->db->insert_id();
	  $q="INSERT IGNORE INTO tpoliklinik_triage_kat (assesmen_id,mtriage_id,nama,nama_lain,warna,respon_time,jml_detail)
		SELECT '$assesmen_id' as assesmen_id,H.id as kat_id,H.nama,H.nama_lain,H.warna,H.respon_time,COUNT(D.mtriage_id) as jml_detail
		FROM `mtriage` H
		LEFT JOIN mtriage_setting D ON D.mtriage_id=H.id AND D.staktif='1'
		WHERE H.staktif='1'
		GROUP BY H.id";
	  $this->db->query($q);
	  $q="INSERT INTO tpoliklinik_triage_kat_detail (assesmen_id,kat_id,mtriage_pemeriksaan_id,pemeriksaan,nilai,penilaian,pilih)
		SELECT '$assesmen_id' as assesmen_id,H.mtriage_id as kat_id,H.mtriage_pemeriksaan_id
		,M.nama as pemeriksaan,H.nilai,H.penilaian,0 as pilih
		FROM mtriage_setting H
		LEFT JOIN mtriage_pemeriksaan M ON M.id=H.mtriage_pemeriksaan_id
		WHERE H.staktif='1'
		ORDER BY H.mtriage_id,H.mtriage_pemeriksaan_id ";
	  $this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
    }
	function batal_triage(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $st_edited=$this->input->post('st_edited');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	   if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
		  
	  }
	  if ($st_edited=='1'){
			$q="SELECT * FROM tpoliklinik_triage_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$data_his_edit=$this->db->query($q)->row();
			if ($data_his_edit){
				$data['edited_ppa']=$data_his_edit->edited_ppa;
				$data['edited_date']=$data_his_edit->edited_date;
				$data['alasan_edit_id']=$data_his_edit->alasan_edit_id;
				$data['keterangan_edit']=$data_his_edit->keterangan_edit;
			}
			$q="DELETE FROM tpoliklinik_triage_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_triage_kat WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_triage_kat_detail WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			
			$q="INSERT IGNORE INTO  tpoliklinik_triage_kat
						(assesmen_id,mtriage_id,nama,nama_lain,warna,respon_time,jml_detail,st_pilih_all,pemeriksaan)
				SELECT assesmen_id,mtriage_id,nama,nama_lain,warna,respon_time,jml_detail,st_pilih_all,pemeriksaan
				FROM  tpoliklinik_triage_his_kat
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			// print_r($q);exit;
			$hasil=$this->db->query($q);
			
			$q="INSERT IGNORE INTO tpoliklinik_triage_kat_detail
						(id,assesmen_id,kat_id,mtriage_pemeriksaan_id,pemeriksaan,nilai,penilaian,pilih)
				SELECT id,assesmen_id,kat_id,mtriage_pemeriksaan_id,pemeriksaan,nilai,penilaian,pilih
				FROM  tpoliklinik_triage_his_kat_detail
				WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			// print_r($q);exit;
					
			$hasil=$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_triage_his_kat WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_triage_his_kat_detail WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			
		  $data['status_assemen']='2';
	  }else{
		  $data['status_assemen']='0';
		  
	  }
	  $jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tpoliklinik_triage_his WHERE assesmen_id='$assesmen_id'")->row('baris');
	  $data['jml_edit']=$jml_edit;
	  $data['st_edited']='0';
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_triage',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
	
  function load_pemeriksaan_triage(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $status_assemen=$this->input->post('status_assemen');
	  $jml_kolom=0;
	  $tabel ='<thead><tr>';
	  $tabel .='<th class="text-center" style="width:15%"><strong>PEMERIKSAAN</strong></th>';
		$q="SELECT *FROM tpoliklinik_triage_kat H WHERE H.assesmen_id='$assesmen_id' ";
		$header=$this->db->query($q)->result();
		$q="SELECT H.mtriage_pemeriksaan_id,H.pemeriksaan FROM tpoliklinik_triage_kat_detail H
			WHERE H.assesmen_id='$assesmen_id'
			GROUP BY H.mtriage_pemeriksaan_id
			ORDER BY H.mtriage_pemeriksaan_id ASC";
		$pemeriksaan_list=$this->db->query($q)->result();
		
		$q="SELECT *FROM tpoliklinik_triage_kat_detail H WHERE H.assesmen_id='$assesmen_id'";
		$pemeriksaan_detail=$this->db->query($q)->result_array();
		
		$jml_kolom=count($header);
		$lebar_kolom=85/$jml_kolom;
		foreach($header as $r){
			$opsi='<label class="css-input css-radio css-radio-primary push-10-r">
					<input type="radio" class="opsi_kategori_'.$r->mtriage_id.' input_kategori_'.$r->mtriage_id.' input_kategori_header item_all" value="1" name="kategori_name" onclick="set_all_opsi_kategori('.$r->mtriage_id.')" '.($r->st_pilih_all=='1'?"checked":"").'><span></span><strong> '.$r->nama.'</strong>
				</label>';
			$opsi.='<input onblur="ganti_blur('.$r->mtriage_id.')" '.($status_assemen=='2'?'readonly':'').' class="form-control input_pemeriksaan_header_'.$r->mtriage_id.' input_kategori_'.$r->mtriage_id.' input_pemeriksaan_header_global item_all"  data-id="'.$r->mtriage_id.'"  type="text" value="'.$r->pemeriksaan.'" placeholder="Tanda / Gejala Spesifik" >';
			$tabel .='<th style="background-color:'.$r->warna.';width:'.$lebar_kolom.'% " class="text-left">'.$opsi.'</th>';
		}
	  $tabel .='</tr></thead>';
	  $tabel .='<tbody>';
			foreach($pemeriksaan_list as $row){
			$mtriage_pemeriksaan_id=$row->mtriage_pemeriksaan_id;
			$tabel .='<tr>';
				$tabel .='<td class="text-center"><strong>'.$row->pemeriksaan.'</strong></td>';
					foreach($header as $r){
						$kat_id=$r->mtriage_id;
						$hasil='';
						$data_pilih = array_filter($pemeriksaan_detail, function($var) use ($kat_id,$mtriage_pemeriksaan_id) { 
							return ($var['kat_id'] == $kat_id && $var['mtriage_pemeriksaan_id'] == $mtriage_pemeriksaan_id);
						  });
						if ($data_pilih){
							foreach($data_pilih as $baris){
								// $hasil .= ' '.$baris['id'];
								$hasil .='
										<div class="checkbox">
											<label for="example-'.$baris['id'].'">
												<input id="example-'.$baris['id'].'" data-id="'.$baris['id'].'"  data-kat="'.$kat_id.'" name="chk[]" class="chk_kategori_'.$baris['kat_id'].' chck_all item_all"  type="checkbox" '.($baris['pilih']=='1'?'checked':'').'> '.$baris['penilaian'].'
											</label>
										</div>';
							}
						}
						$tabel .='<td class="text-left">'.$hasil.'</td>';
					}
			$tabel .='</tr>';
			}
			$tabel .='<tr>';
			$tabel .='<td class="text-center"><strong>RESPON TIME</strong></td>';
				foreach($header as $r){
					$tabel .='<td class="text-center"><strong>'.$r->respon_time.'</strong></td>';
				}
			$tabel .='</tr>';
	  $tabel .='</tbody>';
	  
	  $data['tabel']=$tabel;
	  $this->output->set_output(json_encode($data));
  }
  function save_triage(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$tanggal_datang= YMDFormat($this->input->post('tanggaldatang')). ' ' .$this->input->post('waktudatang');
		
		// $waktudaftar= $this->input->post('waktudaftar'),
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$jml_edit=$this->input->post('jml_edit');
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'tanggal_datang'=> $tanggal_datang,
			'nama_template' => $this->input->post('nama_template'),
			'status_assemen' => $this->input->post('status_assemen'),
			'sarana_transport' => $this->input->post('sarana_transport'),
			'surat_pengantar' => $this->input->post('surat_pengantar'),
			'asal_rujukan' => $this->input->post('asal_rujukan'),
			'cara_masuk' => $this->input->post('cara_masuk'),
			'macam_kasus' => $this->input->post('macam_kasus'),
			'gcs_eye' => $this->input->post('gcs_eye'),
			'gcs_voice' => $this->input->post('gcs_voice'),
			'gcs_motion' => $this->input->post('gcs_motion'),
			'pupil_1' => $this->input->post('pupil_1'),
			'pupil_2' => $this->input->post('pupil_2'),
			'reflex_cahaya_1' => $this->input->post('reflex_cahaya_1'),
			'reflex_cahaya_2' => $this->input->post('reflex_cahaya_2'),
			'spo2' => $this->input->post('spo2'),
			'akral' => $this->input->post('akral'),
			'nadi' => $this->input->post('nadi'),
			'ket_nadi' => $this->input->post('ket_nadi'),			
			'idasalpasien' => $this->input->post('idasalpasien'),
			'idrujukan' => $this->input->post('idrujukan'),
			'namapengantar' => $this->input->post('namapengantar'),
			'teleponpengantar' => $this->input->post('teleponpengantar'),
			'nafas' => $this->input->post('nafas'),
			'suhu' => $this->input->post('suhu'),
			'td_sistole' => $this->input->post('td_sistole'),
			'td_diastole' => $this->input->post('td_diastole'),
			'tinggi_badan' => $this->input->post('tinggi_badan'),
			'berat_badan' => $this->input->post('berat_badan'),
			'kasus_alergi' => $this->input->post('kasus_alergi'),
			'alergi' => $this->input->post('alergi'),
			'st_psikologi' => $this->input->post('st_psikologi'),
			'risiko_jatuh_morse' => $this->input->post('risiko_jatuh_morse'),
			'risiko_pasien_anak' => $this->input->post('risiko_pasien_anak'),
			'kategori_id' => $this->input->post('kategori_id'),
		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tpoliklinik_triage_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_triage',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function update_data_triage(){
	  $id=$this->input->post('id');
	  $pilih=$this->input->post('pilih');
	  $mtriage_id=$this->input->post('data_kat');
	  $pemeriksaan=$this->input->post('pemeriksaan');
	  $assesmen_id=$this->input->post('assesmen_id');
	  
	  $data['pilih']=$pilih;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_triage_kat_detail',$data);
	  
	  $data=array(
		'pemeriksaan'=>$pemeriksaan
	  );
	  // if ($pilih=='0'){
		 // $data['st_pilih_all'] =0;
	  // }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $this->db->where('mtriage_id',$mtriage_id);
	  $hasil=$this->db->update('tpoliklinik_triage_kat',$data);
	  echo json_encode($hasil);
  }
  function simpan_triage_header(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $pemeriksaan=$this->input->post('pemeriksaan');
	  $mtriage_id=$this->input->post('kategori');
	  $st_pilih_all=1;
	  $data=array(
		'pemeriksaan'=>'',
		'st_pilih_all'=>0,
	  );
	  $this->db->where('assesmen_id',$assesmen_id);
	  $this->db->where('mtriage_id <>',$mtriage_id);
	  $hasil=$this->db->update('tpoliklinik_triage_kat',$data);
	  
	  $data=array(
		'pemeriksaan'=>$pemeriksaan,
		'st_pilih_all'=>$st_pilih_all,
	  );
	  $this->db->where('assesmen_id',$assesmen_id);
	  $this->db->where('mtriage_id',$mtriage_id);
	  $hasil=$this->db->update('tpoliklinik_triage_kat',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function save_all_detail_triage(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $kategori_id=$this->input->post('kategori_id');
	  // print_r($kategori_id);exit;
	  $data=array(
		
		'st_pilih_all'=>1,
	  );
	  $this->db->where('assesmen_id',$assesmen_id);
	  $this->db->where('mtriage_id',$kategori_id);
	  $hasil=$this->db->update('tpoliklinik_triage_kat',$data);
	  
	  $data=array(
		'pilih'=>1,
	  );
	  $this->db->where('assesmen_id',$assesmen_id);
	  $this->db->where('kat_id',$kategori_id);
	  $hasil=$this->db->update('tpoliklinik_triage_kat_detail',$data);
	  
	  $data=array(
		'pilih'=>0,
	  );
	  $this->db->where('assesmen_id',$assesmen_id);
	  $this->db->where('kat_id <>',$kategori_id);
	  $hasil=$this->db->update('tpoliklinik_triage_kat_detail',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function simpan_triage_pemeriksaan(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $pemeriksaan=$this->input->post('pemeriksaan');
	  $mtriage_id=$this->input->post('mtriage_id');
	 
	  $data=array(
		'pemeriksaan'=>$pemeriksaan,
	  );
	  $this->db->where('assesmen_id',$assesmen_id);
	  $this->db->where('mtriage_id',$mtriage_id);
	  $hasil=$this->db->update('tpoliklinik_triage_kat',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function list_history_pengkajian_triage()
		{
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_triage($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		// print_r($logic_akses_assesmen);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$where='';
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		if ($mppa_id!='#'){
			$where .=" AND H.created_ppa = '".$mppa_id."'";
		}
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_triage")->row_array();
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa
					,H.assesmen_id,H.pendaftaran_id,MP.tanggaldaftar,MP.nopendaftaran,MPOL.nama as poli 
					,MD.nama as dokter,H.tanggal_input as tanggal_pengkajian
					,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,MP.iddokter,MP.idpoliklinik,H.keterangan_edit,H.jml_edit,H.st_edited
					,H.ttd_nama,H.ttd
					FROM `tpoliklinik_triage` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					INNER JOIN mpoliklinik MPOL ON MPOL.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
					ORDER BY H.tanggal_input DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','singkatan_kajian');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
			$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_triage('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
			$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_triage('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/input_triage/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			// $btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
			$btn_ttd='';
			 if ($akses_general['st_edit_catatan']=='0'){
				  $btn_edit='';
			  }else{
				  if ($akses_general['lama_edit']>0){
					  if ($r->selisih>$akses_general['lama_edit']){
						  $btn_edit='';
					  }else{
						  
					  }
					  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id!=$r->created_ppa){
								$btn_edit='';
						  }
					  }
				  }
			  }
			  if ($akses_general['st_duplikasi_catatan']=='0'){
				  $btn_duplikasi='';
			  }else{
				  if ($akses_general['lama_duplikasi']>0){
					  if ($r->selisih>$akses_general['lama_duplikasi']){
						  $btn_duplikasi='';
					  }else{
						 
					  }
				  }
				   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_duplikasi='';
					  }
				   }
			  }
			  if ($akses_general['st_hapus_catatan']=='0'){
				  $btn_hapus='';
			  }else{
				  if ($akses_general['lama_hapus']>0){
					  if ($r->selisih>$akses_general['lama_hapus']){
						  $btn_hapus='';
					  }else{
						 
					  }
					   
				  }
				  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id <> $r->created_ppa){
							$btn_hapus='';
					  }else{
						  
					  }
							// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
				  }
			  }
			if ($logic_akses_assesmen['st_edit_triage']=='0'){
			  $btn_edit='';
			}
			if ($logic_akses_assesmen['st_hapus_triage']=='0'){
			  $btn_hapus='';
			}
			
			$result[] = $no;
			$aksi='';
			$aksi_edit='';
			
			if ($r->jml_edit>0){
				$aksi_edit='<a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_triage/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></a>';
			}
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_duplikasi;	
			$aksi .= $btn_hapus;	
			if ($logic_akses_assesmen['st_lihat_triage']=='1'){
			$aksi .= $btn_lihat;	
			}
			$aksi .= $aksi_edit;	
			$aksi .= '</div>';
			$result[] = $aksi;
			$result[] = HumanDateLong($r->tanggaldaftar);
			$result[] = ($r->nopendaftaran);
			$result[] = ('<strong>'.$r->poli.'</strong><br>'.$r->dokter);
			$result[] = HumanDateLong($r->tanggal_pengkajian);
			$result[] = ($r->nama_mppa).$btn_ttd;
			$result[] = ($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
			$data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
		}
		function save_edit_triage(){
			$assesmen_id=$this->input->post('assesmen_id');
			$alasan_edit_id=$this->input->post('alasan_id');
			$keterangan_edit=$this->input->post('keterangan_edit');
			$jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_triage WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// $jml_edit=$this->input->post('jml_edit');
			$res=$this->simpan_history_triage($assesmen_id,$jml_edit);
			// print_r($res);exit;
			if ($res){
				
				$data=array(
					'status_assemen' => 1,
					'st_edited' => 1,
					'alasan_edit_id' =>$alasan_edit_id,
					'keterangan_edit' =>$keterangan_edit,

				);
				$login_ppa_id=$this->session->userdata('login_ppa_id');
				$data['edited_ppa']=$login_ppa_id;
				$data['edited_date']=date('Y-m-d H:i:s');
				$this->db->where('assesmen_id',$assesmen_id);
				$result=$this->db->update('tpoliklinik_triage',$data);
				// $ttv_id=$this->db->insert_id();
				
				if ($result){
					$hasil=$data;
				}else{
					$hasil=false;
				}
			}else{
				$hasil=$res;
			}
			$this->output->set_output(json_encode($hasil));
		}
		function simpan_history_triage($assesmen_id,$jml_edit){
			$hasil=false;
			
			// $jml_edit=$jml_edit+1;
			$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tpoliklinik_triage_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
			// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
			if ($baris==0){
				$login_ppa_id=$this->session->userdata('login_ppa_id');
				$q="INSERT IGNORE INTO tpoliklinik_triage_his 
							(versi_edit,assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,tanggal_datang,sarana_transport,surat_pengantar,asal_rujukan,cara_masuk,macam_kasus,gcs_eye,gcs_voice,gcs_motion,pupil_1,pupil_2,reflex_cahaya_1,reflex_cahaya_2,spo2,akral,nadi,ket_nadi,nafas,suhu,td_sistole,td_diastole,tinggi_badan,berat_badan,kasus_alergi,alergi,st_psikologi,risiko_jatuh_morse,risiko_pasien_anak,kategori_id,pemeriksaan,respon_time,idasalpasien,idrujukan,namapengantar,teleponpengantar,st_edited,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,edited_ppa,edited_date,ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd)
					SELECT '$jml_edit',assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,tanggal_datang,sarana_transport,surat_pengantar,asal_rujukan,cara_masuk,macam_kasus,gcs_eye,gcs_voice,gcs_motion,pupil_1,pupil_2,reflex_cahaya_1,reflex_cahaya_2,spo2,akral,nadi,ket_nadi,nafas,suhu,td_sistole,td_diastole,tinggi_badan,berat_badan,kasus_alergi,alergi,st_psikologi,risiko_jatuh_morse,risiko_pasien_anak,kategori_id,pemeriksaan,respon_time,idasalpasien,idrujukan,namapengantar,teleponpengantar,st_edited,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,edited_ppa,edited_date,ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd
					FROM tpoliklinik_triage
					WHERE assesmen_id='$assesmen_id'";
				// print_r($q);exit;
				$hasil=$this->db->query($q);
				
				$q="INSERT IGNORE INTO tpoliklinik_triage_his_kat 
							(versi_edit,assesmen_id,mtriage_id,nama,nama_lain,warna,respon_time,jml_detail,st_pilih_all,pemeriksaan)
					SELECT '$jml_edit',assesmen_id,mtriage_id,nama,nama_lain,warna,respon_time,jml_detail,st_pilih_all,pemeriksaan
					FROM  tpoliklinik_triage_kat
					WHERE assesmen_id='$assesmen_id'";
				// print_r($q);exit;
				$hasil=$this->db->query($q);
				
				$q="INSERT IGNORE INTO tpoliklinik_triage_his_kat_detail 
							(versi_edit,id,assesmen_id,kat_id,mtriage_pemeriksaan_id,pemeriksaan,nilai,penilaian,pilih)
					SELECT '$jml_edit',id,assesmen_id,kat_id,mtriage_pemeriksaan_id,pemeriksaan,nilai,penilaian,pilih
					FROM  tpoliklinik_triage_kat_detail
					WHERE assesmen_id='$assesmen_id'";
				// print_r($q);exit;
				$hasil=$this->db->query($q);
				
			  
			}
		  return $hasil;
		}
		function hapus_record_triage(){
			$assesmen_id=$this->input->post('assesmen_id');
			$alasan_id=$this->input->post('alasan_id');
			$keterangan_hapus=$this->input->post('keterangan_hapus');
			$data=array(
				'status_assemen' => 0,
				'st_edited' => 0,
				'alasan_id' =>$alasan_id,
				'keterangan_hapus' =>$keterangan_hapus,

			);
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['deleted_ppa']=$login_ppa_id;
			$data['deleted_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tpoliklinik_triage',$data);
			// $ttv_id=$this->db->insert_id();
			
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
			$this->output->set_output(json_encode($hasil));
		}
		function list_index_template_triage()
		{
			$assesmen_id=$this->input->post('assesmen_id');
			$login_ppa_id=$this->session->userdata('login_ppa_id');
				$data_user=get_acces();
				$user_acces_form=$data_user['user_acces_form'];
				$assesmen_id=$this->input->post('assesmen_id');
				$this->select = array();
				$from="
						(
							SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
							 FROM tpoliklinik_triage H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
							ORDER BY H.template_id DESC
						) as tbl
					";
				// print_r($from);exit();
				$this->from   = $from;
				$this->join 	= array();
				
				
				$this->order  = array();
				$this->group  = array();
				$this->column_search = array('nama_template');
				$this->column_order  = array();

			$list = $this->datatable->get_datatables(true);
			$data = array();
			$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;
			$result = array();

			$result[] = $no;
			 $aksi='';
			 $btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$aksi = '<div class="btn-group">';
			$aksi .= '<button onclick="edit_template_triage('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
			$aksi .= '<button onclick="gunakan_template_triage('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
			$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
			$aksi .= '<button  onclick="hapus_template_triage('.$r->id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			$aksi .= '</div>';
			$result[] = $aksi;
			$result[] = ($r->nama_template);
			$result[] = ($r->jumlah_template);
			$data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	  }
	  function edit_template_triage(){
		  $assesmen_id=$this->input->post('assesmen_id');
			$data=array(
				'status_assemen' => 3,
				'pendaftaran_id' => $this->input->post('pendaftaran_id'),
				'idpasien' => $this->input->post('idpasien'),
			);
			
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tpoliklinik_triage',$data);
			
			$this->output->set_output(json_encode($result));
	  }
	  function create_with_template_triage(){
		  $login_ppa_id=$this->session->userdata('login_ppa_id');
		  $pendaftaran_id=$this->input->post('pendaftaran_id');
		  $template_assesmen_id=$this->input->post('template_assesmen_id');
		 $q="INSERT IGNORE INTO tpoliklinik_triage 
							(tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,tanggal_datang,sarana_transport,surat_pengantar,asal_rujukan,cara_masuk,macam_kasus,gcs_eye,gcs_voice,gcs_motion,pupil_1,pupil_2,reflex_cahaya_1,reflex_cahaya_2,spo2,akral,nadi,nafas,suhu,td_sistole,td_diastole,tinggi_badan,berat_badan,kasus_alergi,alergi,st_psikologi,risiko_jatuh_morse,risiko_pasien_anak,kategori_id,pemeriksaan,respon_time,ket_nadi,idasalpasien,idrujukan,namapengantar,teleponpengantar)
					SELECT NOW(),pendaftaran_id,idpasien,NOW(),created_ppa,template_id,'1',deleted_ppa,deleted_date,NOW(),sarana_transport,surat_pengantar,asal_rujukan,cara_masuk,macam_kasus,gcs_eye,gcs_voice,gcs_motion,pupil_1,pupil_2,reflex_cahaya_1,reflex_cahaya_2,spo2,akral,nadi,nafas,suhu,td_sistole,td_diastole,tinggi_badan,berat_badan,kasus_alergi,alergi,st_psikologi,risiko_jatuh_morse,risiko_pasien_anak,kategori_id,pemeriksaan,respon_time,ket_nadi,idasalpasien,idrujukan,namapengantar,teleponpengantar
					FROM tpoliklinik_triage
					WHERE assesmen_id='$template_assesmen_id'";
		  $hasil=$this->db->query($q);
		 
		  $assesmen_id=$this->db->insert_id();
		 $q="INSERT IGNORE INTO tpoliklinik_triage_kat 
					(assesmen_id,mtriage_id,nama,nama_lain,warna,respon_time,jml_detail,st_pilih_all,pemeriksaan)
			SELECT '$assesmen_id',mtriage_id,nama,nama_lain,warna,respon_time,jml_detail,st_pilih_all,pemeriksaan
			FROM  tpoliklinik_triage_kat
			WHERE assesmen_id='$template_assesmen_id'";
		// print_r($q);exit;
		$hasil=$this->db->query($q);
		
		$q="INSERT IGNORE INTO tpoliklinik_triage_kat_detail 
					(assesmen_id,kat_id,mtriage_pemeriksaan_id,pemeriksaan,nilai,penilaian,pilih)
			SELECT '$assesmen_id',kat_id,mtriage_pemeriksaan_id,pemeriksaan,nilai,penilaian,pilih
			FROM  tpoliklinik_triage_kat_detail
			WHERE assesmen_id='$template_assesmen_id'";
		// print_r($q);exit;
		$hasil=$this->db->query($q);
		
		  $q="UPDATE tpoliklinik_triage 
				SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
				$this->db->query($q);
		  $this->output->set_output(json_encode($hasil));
	   }
	  
	  function get_ket_nadi_triage(){
		  $nadi=$this->input->post('nadi');
		  $q="SELECT kategori_nadi FROM mnadi WHERE '$nadi' BETWEEN nadi_1 AND nadi_2 AND staktif='1'";
		  $hasil=$this->db->query($q)->row('kategori_nadi');
		  $this->output->set_output(json_encode($hasil));
	  }
	  function batal_template_triage(){
		  $login_ppa_id=$this->session->userdata('login_ppa_id');
		  $pendaftaran_id=$this->input->post('pendaftaran_id');
		  $assesmen_id=$this->input->post('assesmen_id');
		  $nama_template=$this->input->post('nama_template');
		  $jml_edit=$this->input->post('jml_edit');
		  $data=array(
			'deleted_ppa' => $login_ppa_id,
			'deleted_date' => date('Y-m-d H:i:s'),
			
		  );
		  if ($jml_edit=='0'){
			  $data['edited_ppa']=null;
			  $data['edited_date']=null;
			  $data['alasan_edit_id']='';
			  $data['keterangan_edit']='';
		  }
		  if ($nama_template){
			  $data['status_assemen']='4';
			  $data['nama_template']=$nama_template;
		  }else{
			  
			  $data['status_assemen']='0';
		  }
		  $this->db->where('assesmen_id',$assesmen_id);
		  $hasil=$this->db->update('tpoliklinik_triage',$data);
		  $this->output->set_output(json_encode($hasil));
	  }
	  
	  //ASMED IGD
	function create_asmed_igd(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_id=$this->input->post('template_id');
	  $idpasien=$this->input->post('idpasien');
	  $q="SELECT H.nama_template_lokasi,H.gambar_tubuh FROM `manatomi_template_igd` H WHERE H.staktif='1' AND H.st_default='1'";
	  $gambar=$this->db->query($q)->row();
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'template_gambar' => $gambar->nama_template_lokasi,
		'gambar_tubuh' => $gambar->gambar_tubuh,
		'idpasien' => $idpasien,
		'template_id' => $template_id,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_asmed_igd',$data);
	  $assesmen_id=$this->db->insert_id();
	  $q="INSERT INTO tpoliklinik_asmed_igd_lokalis_gambar (id,assesmen_id,nama_template_lokasi,gambar_tubuh,st_default)
		SELECT H.id,'$assesmen_id' as assesmen_id, H.nama_template_lokasi,H.gambar_tubuh,H.st_default FROM manatomi_template_igd H
		WHERE H.staktif='1'";
	  $hasil=$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
    }
	function simpan_anatomi_igd(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$manatomi_id=$this->input->post('manatomi_id');
		$ket_anatomi=$this->input->post('ket_anatomi');
		$anatomi_id=$this->input->post('anatomi_id');
		if ($this->cek_duplicate_anatomi_igd($assesmen_id,$manatomi_id,$anatomi_id)){
			$data=array(
					'idpasien' =>$idpasien,
					'assesmen_id' =>$assesmen_id,
					'pendaftaran_id' =>$pendaftaran_id,
					'manatomi_id' =>$manatomi_id,
					'ket_anatomi' =>$ket_anatomi,
					'staktif' =>1,
				);
			if ($anatomi_id){
				$data['edited_by']=$login_ppa_id;
				$data['edited_date']=date('Y-m-d H:i:s');
				$this->db->where('id',$anatomi_id);
				$hasil=$this->db->update('tpoliklinik_asmed_igd_anatomi',$data);
				
			}else{
				// $data['template_assesmen_id']=0;
				$data['created_by']=$login_ppa_id;
				$data['created_date']=date('Y-m-d H:i:s');
				$hasil=$this->db->insert('tpoliklinik_asmed_igd_anatomi',$data);
			}
		}else{
			$hasil=false;
		}
	
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_anatomi_igd($assesmen_id,$manatomi_id,$anatomi_id){
		$q="SELECT * FROM `tpoliklinik_asmed_igd_anatomi` WHERE assesmen_id='$assesmen_id' AND manatomi_id='$manatomi_id' AND id !='$anatomi_id'";
		$hasil=$this->db->query($q)->row('id');
		if ($hasil){
			return false;
		}else{
			return true;
		}
	}
	function load_anatomi_igd()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT MP.nama as nama_user,CONCAT(MD.nama,'<br>(',MD.nama_english,')') as anatomi, H.* 
						FROM tpoliklinik_asmed_igd_anatomi H
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN manatomi MD ON MD.id=H.manatomi_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						ORDER BY MD.nourut ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->anatomi);
          $result[] = $r->ket_anatomi;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  $aksi .= '<button onclick="edit_anatomi('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  $aksi .= '<button onclick="hapus_anatomi('.$r->id.')" type="button" title="Hapus Diagnosa" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function edit_anatomi_igd(){
	  $id=$this->input->post('id');
	  $q="SELECT * FROM `tpoliklinik_asmed_igd_anatomi` H WHERE H.id='$id'";
	  $output=$this->db->query($q)->row_array();
	  echo json_encode($output);
  }
  function batal_asmed_igd(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $st_edited=$this->input->post('st_edited');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	   if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
		  
	  }
	  if ($st_edited=='1'){
		
			$q="DELETE FROM tpoliklinik_asmed_igd_diagnosa WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		  $q="DELETE FROM tpoliklinik_asmed_igd_lokalis WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		  $q="DELETE FROM tpoliklinik_asmed_igd_lokalis_gambar WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
			$q="INSERT INTO tpoliklinik_asmed_igd_diagnosa (assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif)
SELECT assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif FROM tpoliklinik_asmed_igd_his_diagnosa WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
		$this->db->query($q);
		
		  $q="INSERT IGNORE  INTO tpoliklinik_asmed_igd_his_riwayat_penyakit_keluarga (versi_edit,assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$jml_edit',assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen
				FROM tpoliklinik_asmed_igd_riwayat_penyakit_keluarga
				WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		  
			$q="INSERT IGNORE INTO tpoliklinik_asmed_igd_lokalis (assesmen_id, gambar_id, `index`, XPos, YPos, `key`, content, warna, keterangan, class_name, path ) SELECT
				assesmen_id,gambar_id,`index`,XPos,YPos,`key`,content,warna,keterangan,class_name,path 
				FROM tpoliklinik_asmed_igd_his_lokalis WHERE assesmen_id = '$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="INSERT IGNORE INTO tpoliklinik_asmed_igd_lokalis_gambar 
					(id,assesmen_id,nama_template_lokasi,gambar_tubuh,st_default,gambar_final)
					SELECT id,assesmen_id,nama_template_lokasi,gambar_tubuh,st_default,gambar_final
					FROM tpoliklinik_asmed_igd_his_lokalis_gambar  WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);  
			  //anatomi
			  
		    $q="DELETE FROM tpoliklinik_asmed_igd_anatomi WHERE assesmen_id='$assesmen_id'  ";
		    $this->db->query($q);
			
			$q="INSERT IGNORE INTO tpoliklinik_asmed_igd_anatomi 
				(assesmen_id,pendaftaran_id,idpasien,manatomi_id,ket_anatomi,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif)
				SELECT assesmen_id,pendaftaran_id,idpasien,manatomi_id,ket_anatomi,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif 
				FROM tpoliklinik_asmed_igd_his_anatomi WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			
			$q="DELETE FROM tpoliklinik_asmed_igd_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_igd_his_anatomi WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_igd_his_edukasi WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_igd_his_diagnosa WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_igd_his_riwayat_penyakit WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_igd_his_riwayat_penyakit_dahulu WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_igd_his_riwayat_penyakit_keluarga WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_igd_his_lokalis WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_asmed_igd_his_lokalis_gambar WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
		  $data['status_assemen']='2';
	  }else{
		  $data['status_assemen']='0';
		  
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_asmed_igd',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
  
  function simpan_riwayat_penyakit_asmed_igd(){
		$hasil=false;
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$riwayat_penyakit=$this->input->post('riwayat_penyakit');
		$riwayat_penyakit_dahulu=$this->input->post('riwayat_penyakit_dahulu');
		$riwayat_penyakit_keluarga=$this->input->post('riwayat_penyakit_keluarga');
		$jenis=$this->input->post('jenis');
		// print_r($riwayat_penyakit_keluarga);exit;
		if ($jenis=='1'){
			$this->db->where('assesmen_id',$assesmen_id);
			$this->db->delete('tpoliklinik_asmed_igd_riwayat_penyakit');
			if ($riwayat_penyakit){
				foreach($riwayat_penyakit as $index=>$val){
					$data=array(
						'assesmen_id' =>$assesmen_id,
						'idpasien' =>$idpasien,
						'pendaftaran_id' =>$pendaftaran_id,
						'penyakit_id' =>$val,
					);
					$hasil=$this->db->insert('tpoliklinik_asmed_igd_riwayat_penyakit',$data);
				}
			}
			
		}
		if ($jenis=='2'){
			$this->db->where('assesmen_id',$assesmen_id);
			$this->db->delete('tpoliklinik_asmed_igd_riwayat_penyakit_dahulu');
			if ($riwayat_penyakit_dahulu){
				foreach($riwayat_penyakit_dahulu as $index=>$val){
					$data=array(
						'assesmen_id' =>$assesmen_id,
						'idpasien' =>$idpasien,
						'pendaftaran_id' =>$pendaftaran_id,
						'penyakit_id' =>$val,
					);
					$hasil=$this->db->insert('tpoliklinik_asmed_igd_riwayat_penyakit_dahulu',$data);
				}
			}
			
		}
		if ($jenis=='3'){
			$this->db->where('assesmen_id',$assesmen_id);
			$this->db->delete('tpoliklinik_asmed_igd_riwayat_penyakit_keluarga');
			if ($riwayat_penyakit_keluarga){
				foreach($riwayat_penyakit_keluarga as $index=>$val){
					// print_r($$val);exit;
					$data=array(
						'assesmen_id' =>$assesmen_id,
						'idpasien' =>$idpasien,
						'pendaftaran_id' =>$pendaftaran_id,
						'penyakit_id' =>$val,
					);
					$hasil=$this->db->insert('tpoliklinik_asmed_igd_riwayat_penyakit_keluarga',$data);
				}
			}
			
		}
		$this->output->set_output(json_encode($hasil));
	}
	function save_asmed_igd(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$jml_edit=$this->input->post('jml_edit');
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'status_assemen' => $this->input->post('status_assemen'),
			'st_anamnesa' => $this->input->post('st_anamnesa'),
			'keluhan_utama' => $this->input->post('keluhan_utama'),
			'nama_template' => $this->input->post('nama_template'),
			'nama_anamnesa' => $this->input->post('nama_anamnesa'),
			'hubungan_anamnesa' => $this->input->post('hubungan_anamnesa'),
			'st_riwayat_penyakit' => $this->input->post('st_riwayat_penyakit'),
			'riwayat_penyakit_lainnya' => $this->input->post('riwayat_penyakit_lainnya'),
			'st_riwayat_penyakit_dahulu' => $this->input->post('st_riwayat_penyakit_dahulu'),
			'riwayat_penyakit_dahulu' => $this->input->post('riwayat_penyakit_dahulu'),
			'st_riwayat_penyakit_keluarga' => $this->input->post('st_riwayat_penyakit_keluarga'),
			'riwayat_penyakit_keluarga' => $this->input->post('riwayat_penyakit_keluarga'),
			'catatan_edukasi' => $this->input->post('catatan_edukasi'),
			'pemeriksaan_penunjang' => $this->input->post('pemeriksaan_penunjang'),
			'rencana_asuhan' => $this->input->post('rencana_asuhan'),
			'intruksi' => $this->input->post('intruksi'),
			'rencana_kontrol' => $this->input->post('rencana_kontrol'),
			'tanggal_kontrol' =>($this->input->post('rencana_kontrol')>0?YMDFormat($this->input->post('tanggal_kontrol')):null),
			'idpoli_kontrol' => $this->input->post('idpoli_kontrol'),
			'iddokter_kontrol' => $this->input->post('iddokter_kontrol'),
			'diagnosa_utama' => $this->input->post('diagnosa_utama'),
			'txt_keterangan' => $this->input->post('txt_keterangan'),
			'txt_keterangan_diagnosa' => $this->input->post('txt_keterangan_diagnosa'),
			'keadaan_umum' => $this->input->post('keadaan_umum'),
			'tingkat_kesadaran' => $this->input->post('tingkat_kesadaran'),
			'td_sistole' => $this->input->post('td_sistole'),
			'td_diastole' => $this->input->post('td_diastole'),
			'nafas' => $this->input->post('nafas'),
			'gds' => $this->input->post('gds'),
			'st_pulang' => $this->input->post('st_pulang'),
			'tanggal_keluar' =>($this->input->post('tanggal_keluar')?YMDFormat($this->input->post('tanggal_keluar')):null),
		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tpoliklinik_asmed_igd_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_asmed_igd',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_edukasi_asmed_igd(){
		$hasil=false;
		$assesmen_id=$this->input->post('assesmen_id');
		$medukasi_id=$this->input->post('medukasi_id');
		// print_r($medukasi_id);exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->delete('tpoliklinik_asmed_igd_edukasi');
		if ($medukasi_id){
			foreach($medukasi_id as $index=>$val){
				$data=array(
					'assesmen_id' =>$assesmen_id,
					'medukasi_id' =>$val,
				);
				$hasil=$this->db->insert('tpoliklinik_asmed_igd_edukasi',$data);
			}
		}
		
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_diagnosa_asmed_igd(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$mdiagnosa_id=$this->input->post('mdiagnosa_id');
		$mdiagnosa_nama=$this->input->post('mdiagnosa_nama');
		if ($mdiagnosa_nama==$mdiagnosa_id){
			$mdiagnosa_id=$this->cek_duplikate_nama_diagonosa($mdiagnosa_nama,$mdiagnosa_id);
		}
		$prioritas=$this->input->post('prioritas');
		$diagnosa_id=$this->input->post('diagnosa_id');
		if ($this->cek_duplicate_diagnosa_asmed_igd($assesmen_id,$mdiagnosa_id,$diagnosa_id,$prioritas)){
			$data=array(
					'idpasien' =>$idpasien,
					'assesmen_id' =>$assesmen_id,
					'pendaftaran_id' =>$pendaftaran_id,
					'mdiagnosa_id' =>$mdiagnosa_id,
					'prioritas' =>$prioritas,
					'staktif' =>1,
				);
			if ($diagnosa_id){
				$data['edited_by']=$login_ppa_id;
				$data['edited_date']=date('Y-m-d H:i:s');
				$this->db->where('id',$diagnosa_id);
				$hasil=$this->db->update('tpoliklinik_asmed_igd_diagnosa',$data);
				
			}else{
				$data['created_by']=$login_ppa_id;
				$data['created_date']=date('Y-m-d H:i:s');
				$hasil=$this->db->insert('tpoliklinik_asmed_igd_diagnosa',$data);
			}
		}else{
			$hasil=false;
		}
	
		$this->output->set_output(json_encode($hasil));
	}
	
	function cek_duplicate_diagnosa_asmed_igd($assesmen_id,$mdiagnosa_id,$diagnosa_id,$prioritas){
		$x_hasil=true;
		$q="SELECT * FROM `tpoliklinik_asmed_igd_diagnosa` WHERE assesmen_id='$assesmen_id' AND mdiagnosa_id='$mdiagnosa_id' AND id !='$diagnosa_id' AND staktif='1'";
		// print_r($q);exit;
		$hasil=$this->db->query($q)->row('id');
		if ($hasil){
			$x_hasil=false;
		}
		$q="SELECT * FROM `tpoliklinik_asmed_igd_diagnosa` WHERE assesmen_id='$assesmen_id' AND prioritas='$prioritas' AND id !='$diagnosa_id' AND staktif='1'";
		$hasil=$this->db->query($q)->row('id');
		if ($hasil){
			$x_hasil=false;
		}
		
		return $x_hasil;
	}
	function get_last_prioritas_asmed_igd(){
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $q="SELECT COALESCE(MAX(prioritas),0) +1 as prioritas FROM `tpoliklinik_asmed_igd_diagnosa` WHERE assesmen_id='$assesmen_id'";
	  $prioritas=$this->db->query($q)->row('prioritas');
	  $data['prioritas']=$prioritas;
	  echo json_encode($data);
	}
	function load_gambar_asmed_igd(){
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $gambar_id=$this->input->post('gambar_id');
	  $status_assemen=$this->input->post('status_assemen');
	  if ($status_assemen!='2'){
		  $q="UPDATE tpoliklinik_asmed_igd_lokalis_gambar SET st_default=0 WHERE assesmen_id='$assesmen_id' AND st_default='1'";
		  $this->db->query($q);
		  $q="UPDATE tpoliklinik_asmed_igd_lokalis_gambar SET st_default=1 WHERE assesmen_id='$assesmen_id' AND id='$gambar_id'";
		  $this->db->query($q);
	  }
	  $q="SELECT gambar_tubuh FROM `tpoliklinik_asmed_igd_lokalis_gambar` WHERE id='$gambar_id' AND assesmen_id='$assesmen_id'";
	  $gambar_tubuh=$this->db->query($q)->row('gambar_tubuh');
	  $data['gambar_tubuh']=$gambar_tubuh;
	  
	  $q="SELECT *FROM tpoliklinik_asmed_igd_lokalis WHERE assesmen_id='$assesmen_id' AND gambar_id='$gambar_id'";
	  $data['list_lokalis']=$this->db->query($q)->result();
	  
	  echo json_encode($data);
	}
	function load_diagnosa_asmed_igd()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT MP.nama as nama_user,MD.nama as diagnosa, H.* FROM tpoliklinik_asmed_igd_diagnosa H
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN mdiagnosa_medis MD ON MD.id=H.mdiagnosa_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						ORDER BY H.prioritas ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->diagnosa);
          $result[] = $r->prioritas;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  $aksi .= '<button onclick="edit_diagnosa_igd('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  $aksi .= '<button onclick="hapus_diagnosa_igd('.$r->id.')" type="button" title="Hapus Diagnosa" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
 
  function hapus_diagnosa_asmed_igd(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $id=$this->input->post('id');
	  $data['deleted_by']=$login_ppa_id;
	  $data['deleted_date']=date('Y-m-d H:i:s');
	  $data['staktif']=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_asmed_igd_diagnosa',$data);
	  echo json_encode($hasil);
  }
  function edit_diagnosa_asmed_igd(){
	  $id=$this->input->post('id');
	  $q="SELECT H.*,MD.nama  FROM tpoliklinik_asmed_igd_diagnosa H 
		LEFT JOIN mdiagnosa_medis MD ON MD.id=H.mdiagnosa_id
		WHERE H.id='$id'";
	  $output=$this->db->query($q)->row_array();
	  echo json_encode($output);
  }
  function list_history_pengkajian_asmed_igd()
  {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_asmed_igd($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		// print_r($logic_akses_assesmen);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$where='';
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		if ($mppa_id!='#'){
			$where .=" AND H.created_ppa = '".$mppa_id."'";
		}
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_asmed_igd")->row_array();
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa
					,H.assesmen_id,H.pendaftaran_id,MP.tanggaldaftar,MP.nopendaftaran,MPOL.nama as poli 
					,MD.nama as dokter,H.tanggal_input as tanggal_pengkajian
					,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,MP.iddokter,MP.idpoliklinik,H.keterangan_edit,H.jml_edit,H.st_edited
					,H.ttd_nama,H.ttd
					FROM `tpoliklinik_asmed_igd` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					INNER JOIN mpoliklinik MPOL ON MPOL.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
					ORDER BY H.tanggal_input DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
		$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash	text-danger"></i></button>';
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/input_asmed_igd/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
		$btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
		
		 if ($akses_general['st_edit_catatan']=='0'){
			  $btn_edit='';
		  }else{
			  if ($akses_general['lama_edit']>0){
				  if ($r->selisih>$akses_general['lama_edit']){
					  $btn_edit='';
				  }else{
					  
				  }
				  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_edit='';
					  }
				  }
			  }
		  }
		  if ($akses_general['st_duplikasi_catatan']=='0'){
			  $btn_duplikasi='';
		  }else{
			  if ($akses_general['lama_duplikasi']>0){
				  if ($r->selisih>$akses_general['lama_duplikasi']){
					  $btn_duplikasi='';
				  }else{
					 
				  }
			  }
			   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id!=$r->created_ppa){
						$btn_duplikasi='';
				  }
			   }
		  }
		  if ($akses_general['st_hapus_catatan']=='0'){
			  $btn_hapus='';
		  }else{
			  if ($akses_general['lama_hapus']>0){
				  if ($r->selisih>$akses_general['lama_hapus']){
					  $btn_hapus='';
				  }else{
					 
				  }
				   
			  }
			  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id <> $r->created_ppa){
						$btn_hapus='';
				  }else{
					  
				  }
						// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
			  }
		  }
		if ($logic_akses_assesmen['st_edit_asmed_igd']=='0'){
		  $btn_edit='';
		}
		if ($logic_akses_assesmen['st_hapus_asmed_igd']=='0'){
		  $btn_hapus='';
		}
		
		$result[] = $no;
		$aksi='';
		$aksi_edit='';
		
		if ($r->jml_edit>0){
			$aksi_edit='<a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_asmed_igd/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></a>';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_edit;	
		$aksi .= $btn_duplikasi;	
		$aksi .= $btn_hapus;	
		if ($logic_akses_assesmen['st_lihat_asmed_igd']=='1'){
		$aksi .= $btn_lihat;	
		}
		$aksi .= $aksi_edit;	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = HumanDateLong($r->tanggaldaftar).' - '.$r->st_edited;
		$result[] = ($r->nopendaftaran);
		$result[] = ('<strong>'.$r->poli.'</strong><br>'.$r->dokter);
		$result[] = HumanDateLong($r->tanggal_pengkajian);
		$result[] = ($r->nama_mppa).$btn_ttd;
		$result[] = ($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function simpan_ttd_asmed_igd(){
		
		$assesmen_id = $this->input->post('assesmen_id');
		$ttd_hubungan = $this->input->post('ttd_hubungan');
		$ttd_nama = $this->input->post('ttd_nama');
		$jenis_ttd = $this->input->post('jenis_ttd');
		$nama_tabel ='tpoliklinik_asmed_igd';
		$ttd = $this->input->post('signature64');
		$data=array(
			'jenis_ttd' =>$jenis_ttd,
			'ttd_nama' =>$ttd_nama,
			'ttd_hubungan' =>$ttd_hubungan,
			'ttd' =>$ttd,
			'ttd_date' =>date('Y-m-d H:i:s'),
		);
		// print_r($id);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update($nama_tabel,$data);
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_default_warna_igd(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$warna = $this->input->post('warna');
		$jenis_drawing = $this->input->post('jenis_drawing');
		$q="INSERT INTO tpoliklinik_asmed_igd_default(mppa_id,warna,jenis_drawing) VALUES(14,'$warna','$jenis_drawing')
		ON DUPLICATE KEY UPDATE
		warna='$warna',jenis_drawing='$jenis_drawing'";
		
		$hasil=$this->db->query($q);
		$this->output->set_output(json_encode($hasil));
	}
	function load_ttd_asmed_igd(){
		$assesmen_id = $this->input->post('assesmen_id');
		$q="SELECT H.ttd,H.ttd_date,H.ttd_nama,H.ttd_hubungan,H.jenis_ttd
			FROM tpoliklinik_asmed_igd H WHERE H.assesmen_id='$assesmen_id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function create_template_asmed_igd(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_id=$this->input->post('template_id');
	  $idpasien=$this->input->post('idpasien');
	  $q="SELECT H.nama_template_lokasi,H.gambar_tubuh FROM `manatomi_template_igd` H WHERE H.staktif='1' AND H.st_default='1'";
	  $gambar=$this->db->query($q)->row();
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'template_gambar' => $gambar->nama_template_lokasi,
		'gambar_tubuh' => $gambar->gambar_tubuh,
		'idpasien' => $idpasien,
		'template_id' => $template_id,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 3,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_asmed_igd',$data);
	  $assesmen_id=$this->db->insert_id();
	  $q="INSERT INTO tpoliklinik_asmed_igd_lokalis_gambar (id,assesmen_id,nama_template_lokasi,gambar_tubuh,st_default)
		SELECT H.id,'$assesmen_id' as assesmen_id, H.nama_template_lokasi,H.gambar_tubuh,H.st_default FROM manatomi_template_igd H
		WHERE H.staktif='1'";
	  $hasil=$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
    }
	function batal_template_asmed_igd(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nama_template=$this->input->post('nama_template');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	  if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
	  }
	  if ($nama_template){
		  $data['status_assemen']='4';
		  $data['nama_template']=$nama_template;
	  }else{
		  
		  $data['status_assemen']='0';
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_asmed_igd',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
  function list_index_template_asmed_igd()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
						 FROM tpoliklinik_asmed_igd H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.template_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		$result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nama_template);
		$result[] = ($r->jumlah_template);
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_record_asmed_igd(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_asmed_igd',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	  function edit_template_asmed_igd(){
	  $assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'status_assemen' => 3,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_asmed_igd',$data);
		
		$data_detail=array(
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_asmed_igd_diagnosa',$data_detail);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_asmed_igd_riwayat_penyakit',$data_detail);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_asmed_igd_riwayat_penyakit_dahulu',$data_detail);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_asmed_igd_riwayat_penyakit_keluarga',$data_detail);
		
		
		$this->output->set_output(json_encode($result));
  }
  function create_with_template_asmed_igd(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $idpasien=$this->input->post('idpasien');
	  $q="INSERT INTO tpoliklinik_asmed_igd (
			template_assesmen_id,tanggal_input,pendaftaran_id,created_date,created_ppa,template_id,status_assemen,idpasien
			,st_anamnesa,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,st_riwayat_penyakit_dahulu,riwayat_penyakit_dahulu,st_riwayat_penyakit_keluarga,riwayat_penyakit_keluarga,template_gambar,gambar_tubuh,pemeriksaan_penunjang,rencana_asuhan,intruksi,rencana_kontrol,tanggal_kontrol,idpoli_kontrol,iddokter_kontrol,st_edukasi,catatan_edukasi,diagnosa_utama
			,txt_keterangan,gambar_id
			,alasan_edit_id,keterangan_edit,txt_keterangan_diagnosa,keadaan_umum,tingkat_kesadaran,td_sistole,td_diastole,nafas,gds,st_pulang,tanggal_keluar)
			SELECT '$template_assesmen_id',NOW() as tanggal_input,'$pendaftaran_id',NOW() as created_date,'$login_ppa_id',template_id,'1' as status_assemen,'$idpasien'
			,st_anamnesa,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,st_riwayat_penyakit_dahulu,riwayat_penyakit_dahulu,st_riwayat_penyakit_keluarga,riwayat_penyakit_keluarga,template_gambar,gambar_tubuh,pemeriksaan_penunjang,rencana_asuhan,intruksi,rencana_kontrol,tanggal_kontrol,idpoli_kontrol,iddokter_kontrol,st_edukasi,catatan_edukasi,diagnosa_utama
			,txt_keterangan,gambar_id
			,alasan_edit_id,keterangan_edit,txt_keterangan_diagnosa,keadaan_umum,tingkat_kesadaran,td_sistole,td_diastole,nafas,gds,st_pulang,tanggal_keluar
			FROM `tpoliklinik_asmed_igd`

			WHERE assesmen_id='$template_assesmen_id'";
	  // print_r($q);exit;
	  $hasil=$this->db->query($q);
	  $assesmen_id=$this->db->insert_id();
	  $data=$this->db->query("SELECT template_id,idpasien from tpoliklinik_asmed_igd where assesmen_id='$assesmen_id'")->row();
	  if ($data){
		  $idpasien=$data->idpasien;
		  $template_id=$data->template_id;
		  // $q="INSERT INTO tpoliklinik_assesmen_risiko_jatuh (assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai)
				// SELECT '$assesmen_id',parameter_nama,group_nilai,param_nilai_id,nilai
				// FROM tpoliklinik_assesmen_risiko_jatuh WHERE assesmen_id='$template_assesmen_id'";
		  // $this->db->query($q);
		  //RIWAYAT PENYAKIT
		  
		  $q="INSERT INTO tpoliklinik_asmed_igd_lokalis (assesmen_id, gambar_id, `index`, XPos, YPos, `key`, content, warna, keterangan, class_name, path ) SELECT
				'$assesmen_id',gambar_id,`index`,XPos,YPos,`key`,content,warna,keterangan,class_name,path 
				FROM tpoliklinik_asmed_igd_lokalis WHERE assesmen_id = '$template_assesmen_id'";
			$this->db->query($q);
			
			$q="INSERT IGNORE INTO tpoliklinik_asmed_igd_lokalis_gambar 
					(id,assesmen_id,nama_template_lokasi,gambar_tubuh,st_default,gambar_final)
					SELECT id,'$assesmen_id',nama_template_lokasi,gambar_tubuh,st_default,gambar_final
					FROM tpoliklinik_asmed_igd_lokalis_gambar  WHERE assesmen_id='$template_assesmen_id'";
			$this->db->query($q);  
			
			
		  $q="INSERT INTO tpoliklinik_asmed_igd_riwayat_penyakit (assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$assesmen_id','$pendaftaran_id',penyakit_id,'$idpasien',status_assemen
				FROM tpoliklinik_asmed_igd_riwayat_penyakit
				WHERE assesmen_id='$template_assesmen_id'";
		  $this->db->query($q);
		  $q="INSERT INTO tpoliklinik_asmed_igd_riwayat_penyakit_dahulu (assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$assesmen_id','$pendaftaran_id',penyakit_id,'$idpasien',status_assemen
				FROM tpoliklinik_asmed_igd_riwayat_penyakit_dahulu
				WHERE assesmen_id='$template_assesmen_id'";
		  $this->db->query($q);
		  $q="INSERT INTO tpoliklinik_asmed_igd_riwayat_penyakit_keluarga (assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$assesmen_id','$pendaftaran_id',penyakit_id,'$idpasien',status_assemen
				FROM tpoliklinik_asmed_igd_riwayat_penyakit_keluarga
				WHERE assesmen_id='$template_assesmen_id'";
		  $this->db->query($q);

		  //RIWAYAT EDUKASI
		  $q="INSERT INTO tpoliklinik_asmed_igd_edukasi(assesmen_id,medukasi_id)
				SELECT '$assesmen_id',medukasi_id FROM tpoliklinik_asmed_igd_edukasi
				WHERE assesmen_id='$template_assesmen_id'";
		  $this->db->query($q);

		  //anatomi
		  $q="INSERT INTO tpoliklinik_asmed_igd_anatomi(assesmen_id,pendaftaran_id,idpasien,manatomi_id,ket_anatomi,created_by,created_date)
				SELECT '$assesmen_id','$pendaftaran_id','$idpasien',manatomi_id,ket_anatomi,'$login_ppa_id',NOW() FROM tpoliklinik_asmed_igd_anatomi
				WHERE assesmen_id='$template_assesmen_id'";
		  $this->db->query($q);
		  
		  $q="SELECT *FROM tpoliklinik_asmed_igd_diagnosa WHERE assesmen_id='$template_assesmen_id'";
		  $row=$this->db->query($q)->result();
		 
					
		  foreach($row as $r){
			  $data_input=array(
					'idpasien' =>$idpasien,
					'assesmen_id' =>$assesmen_id,
					'pendaftaran_id' =>$pendaftaran_id,
					'mdiagnosa_id' =>$r->mdiagnosa_id,
					'prioritas' =>$r->prioritas,
					'created_by' =>$login_ppa_id,
					'created_date' =>date('Y-m-d H:i:s'),
					'staktif' =>1,
			  );
			  $this->db->insert('tpoliklinik_asmed_igd_diagnosa',$data_input);
		  }
		
		  //Update jumlah_template
		  $q="UPDATE tpoliklinik_asmed_igd 
				SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
				$this->db->query($q);
	  }
	  $this->output->set_output(json_encode($hasil));
    }
	function save_edit_asmed_igd(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_edit_id=$this->input->post('alasan_id');
		$keterangan_edit=$this->input->post('keterangan_edit');
		$jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_asmed_igd WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
		// $jml_edit=$this->input->post('jml_edit');
		$res=$this->simpan_history_asmed_igd($assesmen_id,$jml_edit);
		// print_r($res);exit;
		if ($res){
			
			$data=array(
				'status_assemen' => 1,
				'st_edited' => 1,
				'alasan_edit_id' =>$alasan_edit_id,
				'keterangan_edit' =>$keterangan_edit,

			);
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tpoliklinik_asmed_igd',$data);
			// $ttv_id=$this->db->insert_id();
			
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}else{
			$hasil=$res;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_history_asmed_igd($assesmen_id,$jml_edit){
		$hasil=false;
		
		// $jml_edit=$jml_edit+1;
		$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tpoliklinik_asmed_igd_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
		// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
		if ($baris==0){
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$q="INSERT IGNORE INTO tpoliklinik_asmed_igd_his 
						(versi_edit,assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_anamnesa
						,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,st_riwayat_penyakit_dahulu,riwayat_penyakit_dahulu,st_riwayat_penyakit_keluarga,riwayat_penyakit_keluarga,template_gambar,gambar_tubuh,txt_keterangan,gambar_id,pemeriksaan_penunjang,rencana_asuhan,intruksi,rencana_kontrol,tanggal_kontrol,idpoli_kontrol,iddokter_kontrol,st_edukasi,catatan_edukasi,diagnosa_utama
						,edited_ppa,edited_date
						,alasan_edit_id,keterangan_edit,txt_keterangan_diagnosa,keadaan_umum,tingkat_kesadaran,td_sistole,td_diastole,nafas,gds,st_pulang,tanggal_keluar)
				SELECT '$jml_edit',assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date,st_anamnesa
				,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,st_riwayat_penyakit_dahulu,riwayat_penyakit_dahulu,st_riwayat_penyakit_keluarga,riwayat_penyakit_keluarga,template_gambar,gambar_tubuh,txt_keterangan,gambar_id,pemeriksaan_penunjang,rencana_asuhan,intruksi,rencana_kontrol,tanggal_kontrol,idpoli_kontrol,iddokter_kontrol,st_edukasi,catatan_edukasi,diagnosa_utama
				,'$login_ppa_id',NOW()
				,alasan_edit_id,keterangan_edit,txt_keterangan_diagnosa,keadaan_umum,tingkat_kesadaran,td_sistole,td_diastole,nafas,gds,st_pulang,tanggal_keluar
				FROM tpoliklinik_asmed_igd 
				WHERE assesmen_id='$assesmen_id'";
			// print_r($q);exit;
			$hasil=$this->db->query($q);
			
			//ALERGI
			$q="INSERT IGNORE INTO tpoliklinik_asmed_igd_his_anatomi 
				(versi_edit,assesmen_id,pendaftaran_id,idpasien,manatomi_id,ket_anatomi,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif)
				SELECT '$jml_edit',assesmen_id,pendaftaran_id,idpasien,manatomi_id,ket_anatomi,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif FROM tpoliklinik_asmed_igd_anatomi WHERE assesmen_id='$assesmen_id' AND staktif='1'";
			$this->db->query($q);
			
			 $q="INSERT IGNORE INTO tpoliklinik_asmed_igd_his_edukasi(versi_edit,assesmen_id,medukasi_id)
				SELECT '$jml_edit',assesmen_id,medukasi_id FROM tpoliklinik_asmed_igd_edukasi
				WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
			
			//DIAGNOSA
			$q="INSERT IGNORE INTO tpoliklinik_asmed_igd_his_diagnosa (id,versi_edit,assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif)
	SELECT id,'$jml_edit',assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif FROM tpoliklinik_asmed_igd_diagnosa WHERE assesmen_id='$assesmen_id' AND staktif='1'";
			$this->db->query($q);
			
			 // //RIWAYAT PENYAKIT
		   $q="INSERT IGNORE  INTO tpoliklinik_asmed_igd_his_riwayat_penyakit (versi_edit,assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$jml_edit',assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen
				FROM tpoliklinik_asmed_igd_riwayat_penyakit
				WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		  $q="INSERT IGNORE  INTO tpoliklinik_asmed_igd_his_riwayat_penyakit_dahulu (versi_edit,assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$jml_edit',assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen
				FROM tpoliklinik_asmed_igd_riwayat_penyakit_dahulu
				WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  $q="INSERT IGNORE  INTO tpoliklinik_asmed_igd_his_riwayat_penyakit_keluarga (versi_edit,assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$jml_edit',assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen
				FROM tpoliklinik_asmed_igd_riwayat_penyakit_keluarga
				WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		  
			$q="INSERT IGNORE INTO tpoliklinik_asmed_igd_his_lokalis ( versi_edit, assesmen_id, gambar_id, `index`, XPos, YPos, `key`, content, warna, keterangan, class_name, path ) SELECT
				'$jml_edit',assesmen_id,gambar_id,`index`,XPos,YPos,`key`,content,warna,keterangan,class_name,path 
				FROM tpoliklinik_asmed_igd_lokalis WHERE assesmen_id = '$assesmen_id'";
			$this->db->query($q);
			
			$q="INSERT IGNORE INTO tpoliklinik_asmed_igd_his_lokalis_gambar 
					(versi_edit,id,assesmen_id,nama_template_lokasi,gambar_tubuh,st_default,gambar_final)
					SELECT '$jml_edit',id,assesmen_id,nama_template_lokasi,gambar_tubuh,st_default,gambar_final
					FROM tpoliklinik_asmed_igd_lokalis_gambar WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			
		}
	  return $hasil;
	}
	function simpan_status_lokalis_igd(){
		$gambar_id=$this->input->post('gambar_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$gambar_final=$this->input->post('gambar_final');
		$jsonString=($this->input->post('jsonString'));
		// print_r($jsonString);exit;
		$i=0;
		$this->db->where('gambar_id',$gambar_id);
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->delete('tpoliklinik_asmed_igd_lokalis');
		foreach($jsonString as $r){
			$data=array(
				'assesmen_id'=>$assesmen_id,
				'gambar_id'=>$gambar_id,
				'index'=>$i,
				'XPos'=>$r['XPos'],
				'YPos'=>$r['YPos'],
				'key'=>$r['key'],
				// 'content'=>$r['content'],
				'path'=>json_encode($r['path']),
				'warna'=>$r['warna'],
				'keterangan'=>$r['keterangan'],
				'class_name'=>$r['class_name'],
				'path'=>json_encode($r['path']),
			);
			$hasil=$this->db->insert('tpoliklinik_asmed_igd_lokalis',$data);
			$i=$i+1;
			// print_r($r['keterangan']);exit;
		}
		$q="UPDATE tpoliklinik_asmed_igd_lokalis_gambar SET gambar_final='$gambar_final' where assesmen_id='$assesmen_id' AND id='$gambar_id'";
		$hasil=$this->db->query($q);
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_anatomi_igd(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $id=$this->input->post('id');
	  $data['deleted_by']=$login_ppa_id;
	  $data['deleted_date']=date('Y-m-d H:i:s');
	  $data['staktif']=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_asmed_igd_anatomi',$data);
	  echo json_encode($hasil);
  }
  //ASSESMEN IGD
  function create_assesmen_igd(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_id=0;
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'template_id' => $template_id,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,
		
	  );
	  $data_ttv=$this->get_history_ttv_igd($pendaftaran_id);
	  $data = array_merge($data_ttv,$data);
	  $hasil=$this->db->insert('tpoliklinik_assesmen_igd',$data);
	  $assesmen_id=$this->db->insert_id();
	  // $q="INSERT INTO tpoliklinik_assesmen_igd_risiko_jatuh (assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai)
			// SELECT '$assesmen_id',H.parameter_nama,GROUP_CONCAT(M.id) as group_nilai,null as param_nilai_id,0 as nilai FROM `mrisiko_jatuh_param` H
			// LEFT JOIN mrisiko_jatuh_param_skor M ON M.parameter_id=H.id AND M.staktif='1'
			// WHERE H.mrisiko_id='$template_id' AND H.staktif='1'
			// GROUP BY H.id";
	  // $this->db->query($q);
	
	  $this->output->set_output(json_encode($hasil));
    }
	function get_history_ttv_igd($pendaftaran_id){
		$q="SELECT keadaan_umum,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan FROM tpoliklinik_assesmen_igd 
			WHERE pendaftaran_id='$pendaftaran_id' AND status_assemen='2' 
			ORDER BY assesmen_id DESC
			LIMIT 1";
		$row=$this->db->query($q)->row_array();
		if ($row){
			$data=$row;
		}else{
			$data=array(
				'keadaan_umum' => '',
				'tingkat_kesadaran' => '',
				'nadi' => '',
				'nafas' => '',
				'td_sistole' => '',
				'td_diastole' => '',
				'suhu' => '',
				'tinggi_badan' => '',
				'berat_badan' => '',

			);
		}
		return $data;
	}
	function create_template_igd(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $template_id=$this->input->post('template_id');
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'template_id' => $template_id,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 3,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_assesmen_igd',$data);
	  $assesmen_id=$this->db->insert_id();
	  $q="INSERT INTO tpoliklinik_assesmen_igd_risiko_jatuh (assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai)
			SELECT '$assesmen_id',H.parameter_nama,GROUP_CONCAT(M.id) as group_nilai,null as param_nilai_id,0 as nilai FROM `mrisiko_jatuh_param` H
			LEFT JOIN mrisiko_jatuh_param_skor M ON M.parameter_id=H.id AND M.staktif='1'
			WHERE H.mrisiko_id='$template_id' AND H.staktif='1'
			GROUP BY H.id";
	  $this->db->query($q);
	 
	  $this->output->set_output(json_encode($hasil));
    }
	
	function batal_assesmen_igd(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $st_edited=$this->input->post('st_edited');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	   if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
		  
		  
	  }
	  if ($st_edited=='1'){
		  $q="DELETE FROM tpoliklinik_assesmen_igd_alergi WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  $q="INSERT INTO tpoliklinik_assesmen_igd_alergi 
			(assesmen_id,pendaftaran_id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,staktif,status_assemen,created_date,created_by,edited_date,edited_by,deleted_date,deleted_by)
			SELECT assesmen_id,pendaftaran_id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,staktif,status_assemen,created_date,created_by,edited_date,edited_by,deleted_date,deleted_by FROM tpoliklinik_assesmen_igd_his_alergi WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			$q="DELETE FROM tpoliklinik_assesmen_igd_diagnosa WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
			$q="INSERT INTO tpoliklinik_assesmen_igd_diagnosa (assesmen_id,template_assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,template_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif)
SELECT assesmen_id,template_assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,template_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif FROM tpoliklinik_assesmen_igd_his_diagnosa WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
		$this->db->query($q);
		
			 $q="SELECT mdiagnosa_id,data_id,pilih  FROM tpoliklinik_assesmen_igd_his_diagnosa_data WHERE  pilih='1' AND assesmen_id='$assesmen_id' AND versi_edit='$jml_edit' ";//Update ISI diagnosa
			  $row=$this->db->query($q)->result();
			  foreach ($row as $r){
				  $this->db->where('assesmen_id',$assesmen_id);
				  $this->db->where('mdiagnosa_id',$r->mdiagnosa_id);
				  $this->db->where('data_id',$r->data_id);
				  $this->db->update('tpoliklinik_assesmen_igd_diagnosa_data',array('pilih'=>$r->pilih));
			  }
			  
			  $q="DELETE FROM tpoliklinik_assesmen_igd_risiko_jatuh WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
			  $q="INSERT INTO  tpoliklinik_assesmen_igd_risiko_jatuh(id,assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai)
			SELECT id,assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai
			FROM tpoliklinik_assesmen_igd_his_risiko_jatuh WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			// NEW
			$q="DELETE FROM tpoliklinik_assesmen_igd_obat WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			
			 $q="INSERT INTO tpoliklinik_assesmen_igd_obat (id,assesmen_id,pendaftaran_id,idpasien,nama_obat,dosis,waktu,staktif)
			SELECT id,assesmen_id,pendaftaran_id,idpasien,nama_obat,dosis,waktu,staktif
			 FROM tpoliklinik_assesmen_igd_his_obat
			 WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
		  $this->db->query($q);
		  
		  $q="DELETE FROM tpoliklinik_assesmen_igd_his_obat WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
			
			$q="DELETE FROM tpoliklinik_assesmen_igd_obat_infus WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			
		  $q="INSERT INTO tpoliklinik_assesmen_igd_obat_infus (id,assesmen_id,pendaftaran_id,idpasien,tanggal_infus,obat_id,idtipe,kuantitas_infus,rute,dosis_infus,ppa_pemberi,ppa_periksa,ppa_created,staktif)
			SELECT id,assesmen_id,pendaftaran_id,idpasien,tanggal_infus,obat_id,idtipe,kuantitas_infus,rute,dosis_infus,ppa_pemberi,ppa_periksa,ppa_created,staktif
			 FROM tpoliklinik_assesmen_igd_his_obat_infus
			 WHERE assesmen_id='$assesmen_id' AND staktif='1'";
		  $this->db->query($q);
		  
		   $q="DELETE FROM tpoliklinik_assesmen_igd_his_obat_infus WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
		  $q="DELETE FROM tpoliklinik_assesmen_igd_tindakan WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			
		  $q="INSERT INTO  tpoliklinik_assesmen_igd_tindakan(id,assesmen_id,pendaftaran_id,idpasien,tanggal_input,mtindakan_id,ppa_pelaksana,waktu_mulai,waktu_selesai,created_ppa,created_date,edited_ppa,edited_date,staktif)
			SELECT id,assesmen_id,pendaftaran_id,idpasien,tanggal_input,mtindakan_id,ppa_pelaksana,waktu_mulai,waktu_selesai,created_ppa,created_date,edited_ppa,edited_date,staktif
			 FROM tpoliklinik_assesmen_igd_his_tindakan
			 WHERE assesmen_id='$assesmen_id' AND staktif='1'";
		  $this->db->query($q);
		  
		   $q="DELETE FROM tpoliklinik_assesmen_igd_his_tindakan WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
		  
		  //NEW END
			
			$q="DELETE FROM tpoliklinik_assesmen_igd_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			$q="DELETE FROM tpoliklinik_assesmen_igd_his_alergi WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			$q="DELETE FROM tpoliklinik_assesmen_igd_his_diagnosa WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			$q="DELETE FROM tpoliklinik_assesmen_igd_his_diagnosa_data WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			$q="DELETE FROM tpoliklinik_assesmen_igd_his_edukasi WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			$q="DELETE FROM tpoliklinik_assesmen_igd_his_risiko_jatuh WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			$q="DELETE FROM tpoliklinik_assesmen_igd_his_riwayat_penyakit WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
			$this->db->query($q);
			
		  $data['status_assemen']='2';
	  }else{
		  $data['status_assemen']='0';
		  
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_assesmen_igd',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
  function batal_template_igd(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nama_template=$this->input->post('nama_template');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	  if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
	  }
	  if ($nama_template){
		  $data['status_assemen']='4';
		  $data['nama_template']=$nama_template;
		  
	  }else{
		  
		  $data['status_assemen']='0';
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_assesmen_igd',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
	function save_assesmen_igd(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$tanggal_datang= YMDFormat($this->input->post('tanggaldatang')). ' ' .$this->input->post('waktudatang');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$jml_edit=$this->input->post('jml_edit');
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'status_assemen' => $this->input->post('status_assemen'),
			'tanggal_datang' => $tanggal_datang,
			'sarana_transport' => $this->input->post('sarana_transport'),
			'surat_pengantar' => $this->input->post('surat_pengantar'),
			'asal_rujukan' => $this->input->post('asal_rujukan'),
			'cara_masuk' => $this->input->post('cara_masuk'),
			'macam_kasus' => $this->input->post('macam_kasus'),
			'idasalpasien' => $this->input->post('idasalpasien'),
			'idrujukan' => $this->input->post('idrujukan'),
			'namapengantar' => $this->input->post('namapengantar'),
			'teleponpengantar' => $this->input->post('teleponpengantar'),
			'st_kecelakaan' => $this->input->post('st_kecelakaan'),
			'jenis_kecelakaan' => $this->input->post('jenis_kecelakaan'),
			'jenis_kendaraan_pasien' => $this->input->post('jenis_kendaraan_pasien'),
			'jenis_kendaraan_lawan' => $this->input->post('jenis_kendaraan_lawan'),
			'lokasi_kecelakaan' => $this->input->post('lokasi_kecelakaan'),
			'lokasi_lainnya' => $this->input->post('lokasi_lainnya'),
			'st_hamil' => $this->input->post('st_hamil'),
			'hamil_g' => $this->input->post('hamil_g'),
			'hamil_p' => $this->input->post('hamil_p'),
			'hamil_a' => $this->input->post('hamil_a'),
			'st_menyusui' => $this->input->post('st_menyusui'),
			'info_menyusui' => $this->input->post('info_menyusui'),
			'keadaan_umum' => $this->input->post('keadaan_umum'),
			'tingkat_kesadaran' => $this->input->post('tingkat_kesadaran'),
			'nadi' => $this->input->post('nadi'),
			'nafas' => $this->input->post('nafas'),
			'td_sistole' => $this->input->post('td_sistole'),
			'td_diastole' => $this->input->post('td_diastole'),
			'suhu' => $this->input->post('suhu'),
			'tinggi_badan' => $this->input->post('tinggi_badan'),
			'berat_badan' => $this->input->post('berat_badan'),
			'agama' => $this->input->post('agama'),
			'faktor_komorbid' => $this->input->post('faktor_komorbid'),
			'penurunan_fungsi' => $this->input->post('penurunan_fungsi'),
			'st_decubitus' => $this->input->post('st_decubitus'),
			'st_p3' => $this->input->post('st_p3'),
			'st_riwayat_demam' => $this->input->post('st_riwayat_demam'),
			'st_berkeringat_malam' => $this->input->post('st_berkeringat_malam'),
			'st_pergi_area_wabah' => $this->input->post('st_pergi_area_wabah'),
			'st_pemakaian_obat_panjang' => $this->input->post('st_pemakaian_obat_panjang'),
			'st_turun_bb_tanpa_sebab' => $this->input->post('st_turun_bb_tanpa_sebab'),
			'intruksi_keperawatan' => $this->input->post('intruksi_keperawatan'),
			'keadaan_umum_keluar' => $this->input->post('keadaan_umum_keluar'),
			'tingkat_kesadaran_keluar' => $this->input->post('tingkat_kesadaran_keluar'),
			'td_sistole_keluar' => $this->input->post('td_sistole_keluar'),
			'td_diastole_keluar' => $this->input->post('td_diastole_keluar'),
			'nafas_keluar' => $this->input->post('nafas_keluar'),
			'gds_keluar' => $this->input->post('gds_keluar'),
			'st_pulang' => $this->input->post('st_pulang'),
			'tanggal_keluar' => ($this->input->post('tanggal_keluar')?YMDFormat($this->input->post('tanggal_keluar')):null),
			'rencana_kontrol' => $this->input->post('rencana_kontrol'),
			'tanggal_kontrol' => ($this->input->post('rencana_kontrol')>0?YMDFormat($this->input->post('tanggal_kontrol')):null),
			'idpoli_kontrol' => $this->input->post('idpoli_kontrol'),
			'iddokter_kontrol' => $this->input->post('iddokter_kontrol'),

			'st_anamnesa' => $this->input->post('st_anamnesa'),
			'keluhan_utama' => $this->input->post('keluhan_utama'),
			'nama_template' => $this->input->post('nama_template'),
			'nama_anamnesa' => $this->input->post('nama_anamnesa'),
			'hubungan_anamnesa' => $this->input->post('hubungan_anamnesa'),
			'st_riwayat_penyakit' => $this->input->post('st_riwayat_penyakit'),
			'riwayat_penyakit_lainnya' => $this->input->post('riwayat_penyakit_lainnya'),
			'riwayat_alergi' => $this->input->post('riwayat_alergi'),
			'riwayat_pengobatan' => $this->input->post('riwayat_pengobatan'),
			'hubungan_anggota_keluarga' => $this->input->post('hubungan_anggota_keluarga'),
			'st_psikologis' => $this->input->post('st_psikologis'),
			'st_sosial_ekonomi' => $this->input->post('st_sosial_ekonomi'),
			'st_spiritual' => $this->input->post('st_spiritual'),
			'pendidikan' => $this->input->post('pendidikan'),
			'pekerjaan' => $this->input->post('pekerjaan'),
			'st_nafsu_makan' => $this->input->post('st_nafsu_makan'),
			'st_turun_bb' => $this->input->post('st_turun_bb'),
			'st_mual' => $this->input->post('st_mual'),
			'st_muntah' => $this->input->post('st_muntah'),
			'st_fungsional' => $this->input->post('st_fungsional'),
			'st_alat_bantu' => $this->input->post('st_alat_bantu'),
			'st_cacat' => $this->input->post('st_cacat'),
			'cacat_lain' => $this->input->post('cacat_lain'),
			'risiko_jatuh' => $this->input->post('risiko_jatuh'),
			'skor_pengkajian' => $this->input->post('skor_pengkajian'),
			'st_tindakan' => $this->input->post('st_tindakan'),
			'nama_tindakan' => $this->input->post('nama_tindakan'),
			'st_tindakan_action' => $this->input->post('st_tindakan_action'),
			'st_edukasi' => $this->input->post('st_edukasi'),
			'st_menerima_info' => $this->input->post('st_menerima_info'),
			'st_hambatan_edukasi' => $this->input->post('st_hambatan_edukasi'),
			'st_penerjemaah' => $this->input->post('st_penerjemaah'),
			'penerjemaah' => $this->input->post('penerjemaah'),
			'catatan_edukasi' => $this->input->post('catatan_edukasi'),

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tpoliklinik_assesmen_igd_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			
		}
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_assesmen_igd',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_edukasi_igd(){
		$hasil=false;
		$assesmen_id=$this->input->post('assesmen_id');
		$medukasi_id=$this->input->post('medukasi_id');
		// print_r($medukasi_id);exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->delete('tpoliklinik_assesmen_igd_edukasi');
		if ($medukasi_id){
			foreach($medukasi_id as $index=>$val){
				$data=array(
					'assesmen_id' =>$assesmen_id,
					'medukasi_id' =>$val,
				);
				$hasil=$this->db->insert('tpoliklinik_assesmen_igd_edukasi',$data);
			}
		}
		
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_riwayat_penyakit_igd(){
		$hasil=false;
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$riwayat_penyakit=$this->input->post('riwayat_penyakit');
		// print_r($medukasi_id);exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->delete('tpoliklinik_assesmen_igd_riwayat_penyakit');
		if ($riwayat_penyakit){
			foreach($riwayat_penyakit as $index=>$val){
				$data=array(
					'assesmen_id' =>$assesmen_id,
					'idpasien' =>$idpasien,
					'pendaftaran_id' =>$pendaftaran_id,
					'penyakit_id' =>$val,
				);
				$hasil=$this->db->insert('tpoliklinik_assesmen_igd_riwayat_penyakit',$data);
			}
		}
		
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_alergi_igd(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$alergi_id=$this->input->post('alergi_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$input_jenis_alergi=$this->input->post('input_jenis_alergi');
		$input_detail_alergi=$this->input->post('input_detail_alergi');
		$input_reaksi_alergi=$this->input->post('input_reaksi_alergi');
		$data=array(
				'idpasien' =>$idpasien,
				'assesmen_id' =>$assesmen_id,
				'pendaftaran_id' =>$pendaftaran_id,
				'input_jenis_alergi' =>$input_jenis_alergi,
				'input_detail_alergi' =>$input_detail_alergi,
				'input_reaksi_alergi' =>$input_reaksi_alergi,
				'staktif' =>1,
			);
		if ($alergi_id){
			$data['edited_by']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$alergi_id);
			$hasil=$this->db->update('tpoliklinik_assesmen_igd_alergi',$data);
			
		}else{
			$data['created_by']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('tpoliklinik_assesmen_igd_alergi',$data);
		}
	
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_obat_igd(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$obat_id=$this->input->post('obat_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$nama_obat=$this->input->post('nama_obat');
		$dosis=$this->input->post('dosis');
		$waktu=$this->input->post('waktu');
		$data=array(
				'idpasien' =>$idpasien,
				'assesmen_id' =>$assesmen_id,
				'pendaftaran_id' =>$pendaftaran_id,
				'nama_obat' =>$nama_obat,
				'dosis' =>$dosis,
				'waktu' =>$waktu,
				'staktif' =>1,
			);
		if ($obat_id){
			// $data['edited_by']=$login_ppa_id;
			// $data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$obat_id);
			$hasil=$this->db->update('tpoliklinik_assesmen_igd_obat',$data);
			
		}else{
			// $data['created_by']=$login_ppa_id;
			// $data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('tpoliklinik_assesmen_igd_obat',$data);
		}
	
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_obat_infus_igd(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$obat_infus_id=$this->input->post('obat_infus_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_infus= YMDFormat($this->input->post('tanggalinfus')). ' ' .$this->input->post('waktuinfus');
		$obat_id=$this->input->post('obat_id_select2');
		$kuantitas_infus=$this->input->post('kuantitas_infus');
		$dosis_infus=$this->input->post('dosis_infus');
		$rute=$this->input->post('rute');
		$ppa_periksa=$this->input->post('ppa_periksa');
		$ppa_pemberi=$this->input->post('ppa_pemberi');
		$idtipe=$this->input->post('idtipe');
		$data=array(
				'idpasien' =>$idpasien,
				'assesmen_id' =>$assesmen_id,
				'pendaftaran_id' =>$pendaftaran_id,
				'obat_id' =>$obat_id,
				'tanggal_infus' =>$tanggal_infus,
				'kuantitas_infus' =>$kuantitas_infus,
				'dosis_infus' =>$dosis_infus,
				'rute' =>$rute,
				'ppa_periksa' =>$ppa_periksa,
				'ppa_pemberi' =>$ppa_pemberi,
				'idtipe' =>$idtipe,
				'ppa_created' =>$login_ppa_id,
				'staktif' =>1,
			);
		if ($obat_infus_id){
			// $data['edited_by']=$login_ppa_id;
			// $data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$obat_infus_id);
			$hasil=$this->db->update('tpoliklinik_assesmen_igd_obat_infus',$data);
			
		}else{
			// $data['created_by']=$login_ppa_id;
			// $data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('tpoliklinik_assesmen_igd_obat_infus',$data);
		}
	
		$this->output->set_output(json_encode($hasil));
	}
	function load_obat_igd()
	{
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.*
						FROM tpoliklinik_assesmen_igd_obat H
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_obat','dosis','waktu');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

			 $aksi='';
          $result[] = ($r->nama_obat);
          $result[] = $r->dosis;
          $result[] = $r->waktu;
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  $aksi .= '<button onclick="edit_obat_igd('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  $aksi .= '<button onclick="hapus_obat_igd('.$r->id.')" type="button" title="Hapus Alergi" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  // function load_obat_infus_igd()
	// {
			// $assesmen_id=$this->input->post('assesmen_id');
			// $this->select = array();
			// $from="
					// (
						// SELECT H.*,M.nama as nama_obat,MP.nama as pemberi,MPR.nama as pemeriksa,C.nama as pembuat,MR.ref as nama_rute
						// FROM tpoliklinik_assesmen_igd_obat_infus H
						// INNER JOIN view_barang_all M ON M.idtipe=H.idtipe AND M.id=H.obat_id
						// INNER JOIN mppa MP ON MP.id=H.ppa_pemberi
						// INNER JOIN mppa MPR ON MPR.id=H.ppa_periksa
						// INNER JOIN mppa C ON C.id=H.ppa_created
						// INNER JOIN merm_referensi MR ON MR.nilai AND MR.ref_head_id='74'
						// WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
						// GROUP BY H.id
					// ) as tbl
				// ";
			// // print_r($from);exit();
			// $this->from   = $from;
			// $this->join 	= array();
			
			
			// $this->order  = array();
			// $this->group  = array();
			// $this->column_search = array('dosis_infus','pemeriksa','pemberi','nama_obat');
			// $this->column_order  = array();

      // $list = $this->datatable->get_datatables(true);
      // $data = array();
      // $no = $_POST['start'];
      // foreach ($list as $r) {
          // $no++;
          // $result = array();

			 // $aksi='';
          // $result[] = HumanDateShort_exp($r->tanggal_infus);
          // $result[] = HumanTime($r->tanggal_infus);
          // $result[] = ($r->nama_obat);
          // $result[] = $r->kuantitas_infus;
          // $result[] = $r->dosis_infus;
          // $result[] = $r->nama_rute;
          // $result[] = $r->pemberi;
          // $result[] = $r->pemeriksa;
          // $aksi = '<div class="btn-group">';
		  // $aksi .= '<button onclick="edit_obat_infus_igd('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // $aksi .= '<button onclick="hapus_obat_infus_igd('.$r->id.')" type="button" title="Hapus Obat" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // $aksi .= '</div>';
		  // $aksi .= ($r->pembuat?'<br>'.$r->pembuat:'');
          // $result[] = $aksi;

          // $data[] = $result;
      // }
      // $output = array(
	      // "draw" => $_POST['draw'],
	      // "recordsTotal" => $this->datatable->count_all(true),
	      // "recordsFiltered" => $this->datatable->count_all(true),
	      // "data" => $data
      // );
      // echo json_encode($output);
  // }
  function load_obat_infus_igd()
	{
			$assesmen_id=$this->input->post('assesmen_id');
			$q="
					
						SELECT H.*,M.nama as nama_obat,MP.nama as pemberi,MPR.nama as pemeriksa,C.nama as pembuat,MR.ref as nama_rute
						FROM tpoliklinik_assesmen_igd_obat_infus H
						INNER JOIN view_barang_all M ON M.idtipe=H.idtipe AND M.id=H.obat_id
						INNER JOIN mppa MP ON MP.id=H.ppa_pemberi
						INNER JOIN mppa MPR ON MPR.id=H.ppa_periksa
						INNER JOIN mppa C ON C.id=H.ppa_created
						INNER JOIN merm_referensi MR ON MR.nilai=H.rute AND MR.ref_head_id='74'
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
						GROUP BY H.id
					
				";
			$list=$this->db->query($q)->result();
			$tabel='';
      foreach ($list as $r) {
          $result = array();

			 $aksi='';
			 $tabel .='<tr>';
          $tabel .='<td class="text-center"><i class="fa fa-calendar-plus-o"></i> '.HumanDateShort_exp($r->tanggal_infus).' </td>';
          $tabel .='<td class="text-center"><i class="si si-clock"></i> '.HumanTime($r->tanggal_infus).' </td>';
          $tabel .='<td class="text-left">'.($r->nama_obat).'</td>';
          $tabel .='<td class="text-center">'.$r->kuantitas_infus.'</td>';
          $tabel .='<td class="text-center">'.$r->dosis_infus.'</td>';
          $tabel .='<td class="text-center">'.$r->nama_rute.'</td>';
          $tabel .='<td class="text-center">'.$r->pemberi.'</td>';
          $tabel .='<td class="text-center">'. $r->pemeriksa.'</td>';
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_obat_infus_igd('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_obat_infus_igd('.$r->id.')" type="button" title="Hapus Obat" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  $aksi .= ($r->pembuat?'<br>'.$r->pembuat:'');
           $tabel .='<td class="text-center">'.$aksi.'</td>';
		  $tabel .= '</tr>';

      }
	  $data['tabel']=$tabel;
      $this->output->set_output(json_encode($data));
  }
  
	function load_alergi_igd()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.*,R.ref  as jenis_alergi, MP.nama as user_created
						FROM tpoliklinik_assesmen_igd_alergi H
						INNER JOIN merm_referensi R ON R.ref_head_id='24' AND R.nilai=H.input_jenis_alergi
						LEFT JOIN mppa MP ON MP.id=H.created_by
						WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('jenis_alergi','input_detail_alergi','input_reaksi_alergi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->jenis_alergi);
          $result[] = $r->input_detail_alergi;
          $result[] = $r->input_reaksi_alergi;
          $result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  $aksi .= '<button onclick="edit_alergi_igd('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  $aksi .= '<button onclick="hapus_alergi_igd('.$r->id.')" type="button" title="Hapus Alergi" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_alergi_igd(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $id=$this->input->post('id');
	  $data['deleted_by']=$login_ppa_id;
	  $data['deleted_date']=date('Y-m-d H:i:s');
	  $data['staktif']=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_assesmen_igd_alergi',$data);
	  echo json_encode($hasil);
  }
  function hapus_obat_igd(){
	  $id=$this->input->post('id');
	  $data['staktif']=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_assesmen_igd_obat',$data);
	  echo json_encode($hasil);
  }
  function hapus_obat_infus_igd(){
	  $id=$this->input->post('id');
	  $data['staktif']=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_assesmen_igd_obat_infus',$data);
	  echo json_encode($hasil);
  }
  
  
  function edit_alergi_igd(){
	  $id=$this->input->post('id');
	  $q="SELECT * FROM `tpoliklinik_assesmen_igd_alergi` H WHERE H.id='$id'";
	  $output=$this->db->query($q)->row_array();
	  echo json_encode($output);
  }
  function edit_obat_igd(){
	  $id=$this->input->post('id');
	  $q="SELECT * FROM `tpoliklinik_assesmen_igd_obat` H WHERE H.id='$id'";
	  $output=$this->db->query($q)->row_array();
	  echo json_encode($output);
  }
 function edit_obat_infus_igd(){
	  $id=$this->input->post('id');
	  $q="SELECT H.*,M.nama as nama_obat,MP.nama as pemberi,MPR.nama as pemeriksa,MR.ref as nama_rute
		
		FROM tpoliklinik_assesmen_igd_obat_infus H
		INNER JOIN view_barang_all M ON M.idtipe=H.idtipe AND M.id=H.obat_id
		INNER JOIN mppa MP ON MP.id=H.ppa_pemberi
		INNER JOIN mppa MPR ON MPR.id=H.ppa_periksa
		INNER JOIN merm_referensi MR ON MR.nilai AND MR.ref_head_id='74'
		WHERE H.id='$id' AND H.staktif='1'
		GROUP BY H.id";
	  $output=$this->db->query($q)->row_array();
	  $output['waktu_infus']=HumanTime($output['tanggal_infus']);
	  $output['tanggal_infus']=HumanDateShort($output['tanggal_infus']);
	  echo json_encode($output);
  }
  function edit_tindakan_assesmen_igd(){
	  $id=$this->input->post('id');
	  $q="SELECT H.*,MP.nama as pelaksana,MC.nama as user_created 
			,MT.nama as nama_tindakan
			FROM tpoliklinik_assesmen_igd_tindakan H
			INNER JOIN mppa MP ON MP.id=H.ppa_pelaksana
			INNER JOIN mppa MC ON MC.id=H.created_ppa
			INNER JOIN mtindakan_igd MT ON MT.id=H.mtindakan_id
			WHERE H.id='$id'
			ORDER BY H.tanggal_input ASC";
		// print_r($q);exit;
	  $output=$this->db->query($q)->row_array();
	  // $output['waktu_mulai']=HumanTime($output['waktu_mulai']);
	  // $output['waktu_selesai']=HumanTime($output['waktu_selesai']);
	  $output['waktutindakan']=HumanTime($output['tanggal_input']);
	  $output['tanggaltindakan']=HumanDateShort($output['tanggal_input']);
	  echo json_encode($output);
  }
 
  function load_skrining_risiko_jatuh_igd(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $q="SELECT * FROM `tpoliklinik_assesmen_igd_risiko_jatuh` H WHERE H.assesmen_id='$assesmen_id'";
	  // print_r($q);exit;
	  $hasil=$this->db->query($q)->result();
	  $opsi='';
	  $no=0;
	  foreach($hasil as $r){
		  $no=$no+1;
		 $opsi.='<tr>">
					<td>'.$r->parameter_nama.'<input class="risiko_nilai_id" type="hidden"  value="'.$r->id.'" ></td>
					<td><select tabindex="8" class="form-control nilai " style="width: 100%;" data-placeholder="Pilih Opsi" required>
						'.$this->opsi_nilai_igd($r->group_nilai,$r->param_nilai_id).'
					</select>
					</td>
					
				</tr>';
	  }
	  $data['opsi']=$opsi;
	  echo json_encode($data);
  }
  function opsi_nilai_igd($param,$param_nilai_id){
	  $q="SELECT * FROM `mrisiko_jatuh_param_skor` H WHERE H.id IN ($param)";
	  $hasil='';
	  if ($param){
		  
		  $row=$this->db->query($q)->result();
			  $hasil .='<option value="" '.($param_nilai_id==''?'selected':'').'>-Opsi-</option>';
		  foreach($row as $r){
			  $hasil .='<option value="'.$r->id.'" '.($param_nilai_id==$r->id?'selected':'').'>'.$r->deskripsi_nama.'</option>';
		  }
	  }
	  return $hasil;
  }
  function update_nilai_risiko_jatuh_igd(){
	  $id=$this->input->post('risiko_nilai_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $template_id=$this->input->post('template_id');
	  $nilai_id=$this->input->post('nilai_id');
	  $data=array(
		'param_nilai_id' =>$nilai_id
	  );
	  $this->db->where('id',$id);
	  $result=$this->db->update('tpoliklinik_assesmen_igd_risiko_jatuh',$data);
	  
	  $skor=$this->db->query("SELECT SUM(H.nilai) as skor FROM `tpoliklinik_assesmen_igd_risiko_jatuh` H WHERE H.assesmen_id='$assesmen_id'")->row('skor');
	  $q="SELECT H.ref_nilai,H.st_tindakan,H.nama_tindakan FROM mrisiko_jatuh_setting_nilai H
			WHERE H.mrisiko_id='$template_id' AND ('$skor' BETWEEN H.skor_1 AND H.skor_2)";
	  $hasil=$this->db->query($q)->row();
	  
	  $data['skor']=$skor;
	  $data['ref_nilai']=$hasil->ref_nilai;
	  $data['st_tindakan']=$hasil->st_tindakan;
	  $data['nama_tindakan']=$hasil->nama_tindakan;
	  $this->output->set_output(json_encode($data));
  }
  function get_skor_pengkajian_igd(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $template_id=$this->input->post('template_id');
	 
	  
	  $skor=$this->db->query("SELECT SUM(H.nilai) as skor FROM `tpoliklinik_assesmen_igd_risiko_jatuh` H WHERE H.assesmen_id='$assesmen_id'")->row('skor');
	  $q="SELECT H.ref_nilai,H.st_tindakan,H.nama_tindakan FROM mrisiko_jatuh_setting_nilai H
			WHERE H.mrisiko_id='$template_id' AND ('$skor' BETWEEN H.skor_1 AND H.skor_2)";
	  $hasil=$this->db->query($q)->row();
	  
	  $data['skor']=$skor;
	  $data['ref_nilai']=$hasil->ref_nilai;
	  $data['st_tindakan']=$hasil->st_tindakan;
	  $data['nama_tindakan']=$hasil->nama_tindakan;
	  $this->output->set_output(json_encode($data));
  }
  function simpan_diagnosa_igd(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$mdiagnosa_id=$this->input->post('mdiagnosa_id');
		$prioritas=$this->input->post('prioritas');
		$diagnosa_id=$this->input->post('diagnosa_id');
		if ($this->cek_duplicate_diagnosa_igd($assesmen_id,$mdiagnosa_id,$diagnosa_id)){
			$data=array(
					'idpasien' =>$idpasien,
					'assesmen_id' =>$assesmen_id,
					'pendaftaran_id' =>$pendaftaran_id,
					'mdiagnosa_id' =>$mdiagnosa_id,
					'prioritas' =>$prioritas,
					'staktif' =>1,
				);
			if ($diagnosa_id){
				$data['edited_by']=$login_ppa_id;
				$data['edited_date']=date('Y-m-d H:i:s');
				$this->db->where('id',$diagnosa_id);
				$hasil=$this->db->update('tpoliklinik_assesmen_igd_diagnosa',$data);
				
			}else{
				$data['template_assesmen_id']=0;
				$data['created_by']=$login_ppa_id;
				$data['created_date']=date('Y-m-d H:i:s');
				$hasil=$this->db->insert('tpoliklinik_assesmen_igd_diagnosa',$data);
			}
		}else{
			$hasil=false;
		}
	
		$this->output->set_output(json_encode($hasil));
	}
	
	function cek_duplicate_diagnosa_igd($assesmen_id,$mdiagnosa_id,$diagnosa_id){
		$q="SELECT * FROM `tpoliklinik_assesmen_igd_diagnosa` WHERE assesmen_id='$assesmen_id' AND mdiagnosa_id='$mdiagnosa_id' AND id !='$diagnosa_id' AND staktif='1'";
		$hasil=$this->db->query($q)->row('id');
		if ($hasil){
			return false;
		}else{
			return true;
		}
	}
	function load_diagnosa_igd()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT MP.nama as nama_user,CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa, H.* FROM tpoliklinik_assesmen_igd_diagnosa H
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN mdiagnosa MD ON MD.id=H.mdiagnosa_id 
						WHERE H.staktif='1' AND H.assesmen_id='$assesmen_id'
						ORDER BY H.prioritas ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_user','diagnosa','prioritas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->diagnosa);
          $result[] = $r->prioritas;
          $result[] = $r->nama_user.'<br>'.HumanDateLong($r->created_date);
          $aksi = '<div class="btn-group">';
		  // if (UserAccesForm($user_acces_form,array('1607'))){
		  $aksi .= '<button onclick="edit_diagnosa('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  // }
		   // if (UserAccesForm($user_acces_form,array('1608'))){
		  $aksi .= '<button onclick="hapus_diagnosa_igd('.$r->id.')" type="button" title="Hapus Diagnosa" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '<button onclick="set_rencana_asuhan('.$r->id.')" type="button" title="Rencana Asuhan" class="btn btn-warning btn-xs btn_rencana"><i class="si si-book-open"></i></button>';	
		  // }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  
   function edit_diagnosa_igd(){
	  $id=$this->input->post('id');
	  $q="SELECT * FROM `tpoliklinik_assesmen_igd_diagnosa` H WHERE H.id='$id'";
	  $output=$this->db->query($q)->row_array();
	  echo json_encode($output);
  }
  
  
  function hapus_diagnosa_igd(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $id=$this->input->post('id');
	  $data['deleted_by']=$login_ppa_id;
	  $data['deleted_date']=date('Y-m-d H:i:s');
	  $data['staktif']=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_assesmen_igd_diagnosa',$data);
	  echo json_encode($hasil);
  }
 
  function update_data_igd(){
	  $id=$this->input->post('id');
	  $pilih=$this->input->post('pilih');
	  $data['pilih']=$pilih;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_assesmen_igd_diagnosa_data',$data);
	  echo json_encode($hasil);
  }
  
  function refresh_diagnosa_igd(){
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $hasil='';
	  $q="SELECT H.id,CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa FROM tpoliklinik_assesmen_igd_diagnosa H
			LEFT JOIN mdiagnosa MD ON MD.id=H.mdiagnosa_id
			WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1' ORDER BY H.prioritas";
	  $row=$this->db->query($q)->result();
		  $hasil .='<option value="" selected>Pilih Opsi</option>';
	  foreach($row as $r){
		  $hasil .='<option value="'.$r->id.'" selected>'.$r->diagnosa.'</option>';
	  }
	  $data['list']=$hasil;
	  echo json_encode($hasil);
  }
  
  function load_data_rencana_asuhan_igd(){
		$assesmen_id=$this->input->post('assesmen_id');
		$diagnosa_id=$this->input->post('diagnosa_id');
		$q="SELECT *FROM tpoliklinik_assesmen_igd_diagnosa_data H
			WHERE H.assesmen_id='$assesmen_id' AND H.diagnosa_id='$diagnosa_id'
			ORDER BY H.nourut ASC";
		$row=$this->db->query($q)->result();
		$tabel='';
		$last_lev='';
		$jml_col='0';
		foreach($row as $r){
			if ($r->lev=='0'){//Section
				if ($last_lev =='2'){//Jika Record Baru
					if ($jml_col==1){
					$tabel .='<td></td>';
						
					}
					$tabel .='</tr>';
				}
				$last_lev=$r->lev;
				$tabel .='
					<tr>
						<td colspan="2" style="background-color:#d9efff; font-weight: bold;width:100%">'.$r->nama.'</td>
					</tr>				
				';
				$jml_col=0;
			}
			if ($r->lev=='1'){//Kategori
				if ($last_lev =='2'){//Jika Record Baru
					if ($jml_col==1){
					$tabel .='<td></td>';
						
					}
					$tabel .='</tr>';
				}
				$last_lev=$r->lev;
				$tabel .='
					<tr>
						<td colspan="2" style="background-color:#f7f7f7; font-weight: bold;width:100%" class="text-primary">'.$r->nama.'</td>
					</tr>				
				';
				$jml_col=0;
			}
			if ($r->lev=='2'){//Data
				if ($last_lev != $r->lev){//Jika Record Baru
					$tabel .='<tr>';
					$jml_col=$jml_col+1;
				}else{
					$jml_col=$jml_col+1;
				}
				if ($jml_col=='3'){
					$tabel .='</tr>';
					$tabel .='<tr>';
					$jml_col=1;
				}
				$tabel .='
					<td style="width:50%">
						<input type="hidden" class="data_id" value="'.$r->id.'">
						<div class="checkbox">
							<label for="example-'.$r->id.'">
								<input id="example-'.$r->id.'" name="chk[]" class="data_asuhan"  type="checkbox" '.($r->pilih?'checked':'').'> '.$r->nama.'
							</label>
						</div>
					</td>			
				';
				$last_lev=$r->lev;
			}
		}
		if ($jml_col==1){
		$tabel .='<td></td>';
			
		}
		$tabel .='</tr>';
		// $tabel .='<tr><td colspan="2">&nbsp;&nbsp;</td></tr>';
		// print_r($tabel);exit;
		echo json_encode($tabel);
  }
  
  function list_index_template_igd()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
						 FROM tpoliklinik_assesmen_igd H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.template_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		$result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen_igd('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nama_template);
		$result[] = ($r->jumlah_template);
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  
  
  function edit_template_assesmen_igd(){
	  $assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'status_assemen' => 3,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_assesmen_igd',$data);
		
		$data_detail=array(
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
		);
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_assesmen_igd_alergi',$data_detail);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_assesmen_igd_diagnosa',$data_detail);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_assesmen_igd_riwayat_penyakit',$data_detail);
		
		
		$this->output->set_output(json_encode($result));
  }
  function list_history_pengkajian_igd()
  {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_assesmen_igd($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$where='';
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		if ($mppa_id!='#'){
			$where .=" AND H.created_ppa = '".$mppa_id."'";
		}
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_assesmen_igd")->row_array();
		
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa
					,H.assesmen_id,H.pendaftaran_id,MP.tanggaldaftar,MP.nopendaftaran,MPOL.nama as poli 
					,MD.nama as dokter,H.tanggal_input as tanggal_pengkajian
					,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,MP.iddokter,MP.idpoliklinik,H.keterangan_edit,H.jml_edit,H.st_edited
					,H.ttd_nama,H.ttd
					FROM `tpoliklinik_assesmen_igd` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					INNER JOIN mpoliklinik MPOL ON MPOL.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
					ORDER BY H.tanggal_input DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
		$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/input_assesmen_igd/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
		$btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
		
		 if ($akses_general['st_edit_catatan']=='0'){
			  $btn_edit='';
		  }else{
			  if ($akses_general['lama_edit']>0){
				  if ($r->selisih>$akses_general['lama_edit']){
					  $btn_edit='';
				  }else{
					  
				  }
				  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_edit='';
					  }
				  }
			  }
		  }
		  if ($akses_general['st_duplikasi_catatan']=='0'){
			  $btn_duplikasi='';
		  }else{
			  if ($akses_general['lama_duplikasi']>0){
				  if ($r->selisih>$akses_general['lama_duplikasi']){
					  $btn_duplikasi='';
				  }else{
					 
				  }
			  }
			   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id!=$r->created_ppa){
						$btn_duplikasi='';
				  }
			   }
		  }
		  if ($akses_general['st_hapus_catatan']=='0'){
			  $btn_hapus='';
		  }else{
			  if ($akses_general['lama_hapus']>0){
				  if ($r->selisih>$akses_general['lama_hapus']){
					  $btn_hapus='';
				  }else{
					 
				  }
				   
			  }
			  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id <> $r->created_ppa){
						$btn_hapus='';
				  }else{
					  
				  }
						// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
			  }
		  }
		if ($logic_akses_assesmen['st_edit_assesmen_igd']=='0'){
		  $btn_edit='';
		}
		if ($logic_akses_assesmen['st_hapus_assesmen_igd']=='0'){
		  $btn_hapus='';
		}
		
		$result[] = $no;
		$aksi='';
		$aksi_edit='';
		
		if ($r->jml_edit>0){
			$aksi_edit='<a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_assesmen_igd/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></a>';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_edit;	
		$aksi .= $btn_duplikasi;	
		$aksi .= $btn_hapus;	
		$aksi .= $btn_lihat;	
		$aksi .= $aksi_edit;	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = HumanDateLong($r->tanggaldaftar);
		$result[] = ($r->nopendaftaran);
		$result[] = ('<strong>'.$r->poli.'</strong><br>'.$r->dokter);
		$result[] = HumanDateLong($r->tanggal_pengkajian);
		$result[] = ($r->nama_mppa).$btn_ttd;
		$result[] = ($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function create_with_template_igd(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $q="INSERT INTO tpoliklinik_assesmen_igd (
			template_assesmen_id,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,template_id,status_assemen
			,tanggal_datang,sarana_transport,surat_pengantar,asal_rujukan,cara_masuk,macam_kasus,idasalpasien,idrujukan,namapengantar,teleponpengantar,st_kecelakaan,jenis_kecelakaan,jenis_kendaraan_pasien,jenis_kendaraan_lawan,lokasi_kecelakaan,lokasi_lainnya,st_hamil,hamil_g,hamil_p,hamil_a,st_menyusui,info_menyusui,keadaan_umum,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,agama,faktor_komorbid,penurunan_fungsi,st_decubitus,st_p3,st_riwayat_demam,st_berkeringat_malam,st_pergi_area_wabah,st_pemakaian_obat_panjang,st_turun_bb_tanpa_sebab,intruksi_keperawatan,keadaan_umum_keluar,tingkat_kesadaran_keluar,td_sistole_keluar,td_diastole_keluar,nafas_keluar,gds_keluar,st_pulang,tanggal_keluar,rencana_kontrol,tanggal_kontrol,idpoli_kontrol,iddokter_kontrol,st_anamnesa,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,riwayat_alergi,riwayat_pengobatan,hubungan_anggota_keluarga,st_psikologis,st_sosial_ekonomi,st_spiritual,pendidikan,pekerjaan,st_nafsu_makan,st_turun_bb,st_mual,st_muntah,st_fungsional,st_alat_bantu,st_cacat,cacat_lain,header_risiko_jatuh,footer_risiko_jatuh,skor_pengkajian,risiko_jatuh,nama_tindakan,st_tindakan,st_tindakan_action,st_edukasi,st_menerima_info,st_hambatan_edukasi,st_penerjemaah,penerjemaah,catatan_edukasi,risiko_jatuh_header,risiko_jatuh_footer
			)
			SELECT '$template_assesmen_id',NOW() as tanggal_input,'$pendaftaran_id','$idpasien',NOW() as created_date,'$login_ppa_id',template_id,'1' as status_assemen
			,tanggal_datang,sarana_transport,surat_pengantar,asal_rujukan,cara_masuk,macam_kasus,idasalpasien,idrujukan,namapengantar,teleponpengantar,st_kecelakaan,jenis_kecelakaan,jenis_kendaraan_pasien,jenis_kendaraan_lawan,lokasi_kecelakaan,lokasi_lainnya,st_hamil,hamil_g,hamil_p,hamil_a,st_menyusui,info_menyusui,keadaan_umum,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,agama,faktor_komorbid,penurunan_fungsi,st_decubitus,st_p3,st_riwayat_demam,st_berkeringat_malam,st_pergi_area_wabah,st_pemakaian_obat_panjang,st_turun_bb_tanpa_sebab,intruksi_keperawatan,keadaan_umum_keluar,tingkat_kesadaran_keluar,td_sistole_keluar,td_diastole_keluar,nafas_keluar,gds_keluar,st_pulang,tanggal_keluar,rencana_kontrol,tanggal_kontrol,idpoli_kontrol,iddokter_kontrol,st_anamnesa,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,riwayat_alergi,riwayat_pengobatan,hubungan_anggota_keluarga,st_psikologis,st_sosial_ekonomi,st_spiritual,pendidikan,pekerjaan,st_nafsu_makan,st_turun_bb,st_mual,st_muntah,st_fungsional,st_alat_bantu,st_cacat,cacat_lain,header_risiko_jatuh,footer_risiko_jatuh,skor_pengkajian,risiko_jatuh,nama_tindakan,st_tindakan,st_tindakan_action,st_edukasi,st_menerima_info,st_hambatan_edukasi,st_penerjemaah,penerjemaah,catatan_edukasi,risiko_jatuh_header,risiko_jatuh_footer\
			FROM `tpoliklinik_assesmen_igd`

			WHERE assesmen_id='$template_assesmen_id'";
	  $hasil=$this->db->query($q);
	  $assesmen_id=$this->db->insert_id();
	  $data=$this->db->query("SELECT template_id,idpasien from tpoliklinik_assesmen_igd where assesmen_id='$assesmen_id'")->row();
	  $idpasien=$data->idpasien;
	  $template_id=$data->template_id;
	  
	  //RISIKO JATUH
	  $q="INSERT INTO tpoliklinik_assesmen_igd_risiko_jatuh (assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai)
			SELECT '$assesmen_id',parameter_nama,group_nilai,param_nilai_id,nilai
			FROM tpoliklinik_assesmen_igd_risiko_jatuh WHERE assesmen_id='$template_assesmen_id'";
	  $this->db->query($q);
	  //RIWAYAT PENYAKIT
	  $q="INSERT INTO tpoliklinik_assesmen_igd_riwayat_penyakit (assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
			SELECT 
			'$assesmen_id','$pendaftaran_id',penyakit_id,'$idpasien',status_assemen
			FROM tpoliklinik_assesmen_igd_riwayat_penyakit
			WHERE assesmen_id='$template_assesmen_id'";
	  $this->db->query($q);

	  //RIWAYAT EDUKASI
	  $q="INSERT INTO tpoliklinik_assesmen_igd_edukasi(assesmen_id,medukasi_id)
			SELECT '$assesmen_id',medukasi_id FROM tpoliklinik_assesmen_igd_edukasi
			WHERE assesmen_id='$template_assesmen_id'";
	  $this->db->query($q);

	  //RIWAYAT ALERGI
	  $q="INSERT INTO tpoliklinik_assesmen_igd_alergi (assesmen_id,pendaftaran_id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,status_assemen,created_date,created_by )
		SELECT '$assesmen_id','$pendaftaran_id','$idpasien',input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,status_assemen,NOW(),'$login_ppa_id'
		 FROM tpoliklinik_assesmen_igd_alergi
		 WHERE assesmen_id='$template_assesmen_id' AND staktif='1'";
	  $this->db->query($q);
	  
	  $q="INSERT INTO tpoliklinik_assesmen_igd_obat (assesmen_id,pendaftaran_id,idpasien,nama_obat,dosis,waktu,staktif)
		SELECT '$assesmen_id','$pendaftaran_id','$idpasien',nama_obat,dosis,waktu,staktif
		 FROM tpoliklinik_assesmen_igd_obat
		 WHERE assesmen_id='$template_assesmen_id' AND staktif='1'";
	  $this->db->query($q);
	  $q="INSERT INTO tpoliklinik_assesmen_igd_obat_infus (assesmen_id,pendaftaran_id,idpasien,tanggal_infus,obat_id,idtipe,kuantitas_infus,rute,dosis_infus,ppa_pemberi,ppa_periksa,ppa_created,staktif)
		SELECT '$assesmen_id','$pendaftaran_id','$idpasien',tanggal_infus,obat_id,idtipe,kuantitas_infus,rute,dosis_infus,ppa_pemberi,ppa_periksa,ppa_created,staktif
		 FROM tpoliklinik_assesmen_igd_obat_infus
		 WHERE assesmen_id='$template_assesmen_id' AND staktif='1'";
	  $this->db->query($q);
	  
	  $q="INSERT INTO tpoliklinik_assesmen_igd_tindakan (assesmen_id,pendaftaran_id,idpasien,tanggal_input,mtindakan_id,ppa_pelaksana,waktu_mulai,waktu_selesai,created_ppa,created_date,edited_ppa,edited_date,staktif)
		SELECT '$assesmen_id','$pendaftaran_id','$idpasien',tanggal_input,mtindakan_id,ppa_pelaksana,waktu_mulai,waktu_selesai,created_ppa,created_date,edited_ppa,edited_date,staktif
		 FROM tpoliklinik_assesmen_igd_tindakan
		 WHERE assesmen_id='$template_assesmen_id' AND staktif='1'";
	  $this->db->query($q);
	
	  
	  $q="SELECT *FROM tpoliklinik_assesmen_igd_diagnosa WHERE assesmen_id='$template_assesmen_id'";
	  $row=$this->db->query($q)->result();
	 
				
	  foreach($row as $r){
		  $data_input=array(
				'idpasien' =>$idpasien,
				'assesmen_id' =>$assesmen_id,
				'template_assesmen_id' =>$template_assesmen_id,
				'pendaftaran_id' =>$pendaftaran_id,
				'mdiagnosa_id' =>$r->mdiagnosa_id,
				'prioritas' =>$r->prioritas,
				'created_by' =>$login_ppa_id,
				'created_date' =>date('Y-m-d H:i:s'),
				'staktif' =>1,
		  );
		  $this->db->insert('tpoliklinik_assesmen_igd_diagnosa',$data_input);
	  }
	  $q="SELECT mdiagnosa_id,data_id,pilih  FROM tpoliklinik_assesmen_igd_diagnosa_data WHERE  pilih='1' AND assesmen_id='$template_assesmen_id'";//Update ISI diagnosa
	  $row=$this->db->query($q)->result();
	  foreach ($row as $r){
		  $this->db->where('assesmen_id',$assesmen_id);
		  $this->db->where('mdiagnosa_id',$r->mdiagnosa_id);
		  $this->db->where('data_id',$r->data_id);
		  $this->db->update('tpoliklinik_assesmen_igd_diagnosa_data',array('pilih'=>$r->pilih));
	  }
	  
	  //Update jumlah_template
	  $q="UPDATE tpoliklinik_assesmen_igd 
			SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
			$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
    }
	
	function save_edit_assesmen_igd(){
		$assesmen_id=$this->input->post('assesmen_id');
		// print_r($assesmen_id);exit;
		$alasan_edit_id=$this->input->post('alasan_id');
		$keterangan_edit=$this->input->post('keterangan_edit');
		$jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_assesmen_igd WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
		// $jml_edit=$this->input->post('jml_edit');
		$res=$this->simpan_history_assesmen_igd($assesmen_id,$jml_edit);
		// print_r($res);exit;
		if ($res){
			
			$data=array(
				'status_assemen' => 1,
				'st_edited' => 1,
				'alasan_edit_id' =>$alasan_edit_id,
				'keterangan_edit' =>$keterangan_edit,

			);
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tpoliklinik_assesmen_igd',$data);
			// $ttv_id=$this->db->insert_id();
			
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}else{
			$hasil=$res;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_history_assesmen_igd($assesmen_id,$jml_edit){
		$hasil=false;
		
		// $jml_edit=$jml_edit+1;
		$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tpoliklinik_assesmen_igd_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
		// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
		if ($baris==0){
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$q="INSERT IGNORE INTO tpoliklinik_assesmen_igd_his (versi_edit,assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date
				,tanggal_datang,sarana_transport,surat_pengantar,asal_rujukan,cara_masuk,macam_kasus,idasalpasien,idrujukan,namapengantar,teleponpengantar,st_kecelakaan,jenis_kecelakaan,jenis_kendaraan_pasien,jenis_kendaraan_lawan,lokasi_kecelakaan,lokasi_lainnya,st_hamil,hamil_g,hamil_p,hamil_a,st_menyusui,info_menyusui,keadaan_umum,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,agama,faktor_komorbid,penurunan_fungsi,st_decubitus,st_p3,st_riwayat_demam,st_berkeringat_malam,st_pergi_area_wabah,st_pemakaian_obat_panjang,st_turun_bb_tanpa_sebab,intruksi_keperawatan,keadaan_umum_keluar,tingkat_kesadaran_keluar,td_sistole_keluar,td_diastole_keluar,nafas_keluar,gds_keluar,st_pulang,tanggal_keluar,rencana_kontrol,tanggal_kontrol,idpoli_kontrol,iddokter_kontrol,st_anamnesa,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,riwayat_alergi,riwayat_pengobatan,hubungan_anggota_keluarga,st_psikologis,st_sosial_ekonomi,st_spiritual,pendidikan,pekerjaan,st_nafsu_makan,st_turun_bb,st_mual,st_muntah,st_fungsional,st_alat_bantu,st_cacat,cacat_lain,header_risiko_jatuh,footer_risiko_jatuh,skor_pengkajian,risiko_jatuh,nama_tindakan,st_tindakan,st_tindakan_action,st_edukasi,st_menerima_info,st_hambatan_edukasi,st_penerjemaah,penerjemaah,catatan_edukasi,risiko_jatuh_header,risiko_jatuh_footer,st_edited,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,edited_ppa,edited_date,ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd)
				SELECT '$jml_edit',assesmen_id,template_assesmen_id,nama_template,jumlah_template,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,template_id,status_assemen,deleted_ppa,deleted_date
				,tanggal_datang,sarana_transport,surat_pengantar,asal_rujukan,cara_masuk,macam_kasus,idasalpasien,idrujukan,namapengantar,teleponpengantar,st_kecelakaan,jenis_kecelakaan,jenis_kendaraan_pasien,jenis_kendaraan_lawan,lokasi_kecelakaan,lokasi_lainnya,st_hamil,hamil_g,hamil_p,hamil_a,st_menyusui,info_menyusui,keadaan_umum,tingkat_kesadaran,nadi,nafas,td_sistole,td_diastole,suhu,tinggi_badan,berat_badan,agama,faktor_komorbid,penurunan_fungsi,st_decubitus,st_p3,st_riwayat_demam,st_berkeringat_malam,st_pergi_area_wabah,st_pemakaian_obat_panjang,st_turun_bb_tanpa_sebab,intruksi_keperawatan,keadaan_umum_keluar,tingkat_kesadaran_keluar,td_sistole_keluar,td_diastole_keluar,nafas_keluar,gds_keluar,st_pulang,tanggal_keluar,rencana_kontrol,tanggal_kontrol,idpoli_kontrol,iddokter_kontrol,st_anamnesa,keluhan_utama,nama_anamnesa,hubungan_anamnesa,st_riwayat_penyakit,riwayat_penyakit_lainnya,riwayat_alergi,riwayat_pengobatan,hubungan_anggota_keluarga,st_psikologis,st_sosial_ekonomi,st_spiritual,pendidikan,pekerjaan,st_nafsu_makan,st_turun_bb,st_mual,st_muntah,st_fungsional,st_alat_bantu,st_cacat,cacat_lain,header_risiko_jatuh,footer_risiko_jatuh,skor_pengkajian,risiko_jatuh,nama_tindakan,st_tindakan,st_tindakan_action,st_edukasi,st_menerima_info,st_hambatan_edukasi,st_penerjemaah,penerjemaah,catatan_edukasi,risiko_jatuh_header,risiko_jatuh_footer,st_edited,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,edited_ppa,edited_date,ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd
				FROM tpoliklinik_assesmen_igd
				WHERE assesmen_id='$assesmen_id'";
			// print_r($q);exit;
			$hasil=$this->db->query($q);
			//ALERGI
			$q="INSERT IGNORE INTO tpoliklinik_assesmen_igd_his_alergi 
				(versi_edit,id,assesmen_id,pendaftaran_id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,staktif,status_assemen,created_date,created_by,edited_date,edited_by,deleted_date,deleted_by)
				SELECT '$jml_edit',id,assesmen_id,pendaftaran_id,idpasien,input_jenis_alergi,input_detail_alergi,input_reaksi_alergi,staktif,status_assemen,created_date,created_by,edited_date,edited_by,deleted_date,deleted_by FROM tpoliklinik_assesmen_igd_alergi WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			//DIAGNOSA
			$q="INSERT IGNORE INTO tpoliklinik_assesmen_igd_his_diagnosa (id,versi_edit,assesmen_id,template_assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,template_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif)
	SELECT id,'$jml_edit',assesmen_id,template_assesmen_id,pendaftaran_id,idpasien,mdiagnosa_id,template_id,prioritas,created_by,created_date,edited_date,edited_by,deleted_by,deleted_date,staktif FROM tpoliklinik_assesmen_igd_diagnosa WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			
			$q="INSERT IGNORE INTO tpoliklinik_assesmen_igd_his_diagnosa_data (versi_edit,id,assesmen_id,diagnosa_id,mdiagnosa_id,lev,template_id,data_id,nourut,no_urut,nama,parent_id,pilih)
	SELECT '$jml_edit',id,assesmen_id,diagnosa_id,mdiagnosa_id,lev,template_id,data_id,nourut,no_urut,nama,parent_id,pilih FROM tpoliklinik_assesmen_igd_diagnosa_data WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			//EDUKASI
			 $q="INSERT IGNORE INTO tpoliklinik_assesmen_igd_his_edukasi(versi_edit,assesmen_id,medukasi_id)
				SELECT '$jml_edit',assesmen_id,medukasi_id FROM tpoliklinik_assesmen_igd_edukasi
				WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
			//RISIKO JATUH
			$q="INSERT IGNORE INTO tpoliklinik_assesmen_igd_his_risiko_jatuh (versi_edit,id,assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai)
				SELECT '$jml_edit',id,assesmen_id,parameter_nama,group_nilai,param_nilai_id,nilai
				FROM tpoliklinik_assesmen_igd_risiko_jatuh WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		  //RIWAYAT PENYAKIT
		   $q="INSERT IGNORE  INTO tpoliklinik_assesmen_igd_his_riwayat_penyakit (versi_edit,assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen)
				SELECT 
				'$jml_edit',assesmen_id,pendaftaran_id,penyakit_id,idpasien,status_assemen
				FROM tpoliklinik_assesmen_igd_riwayat_penyakit
				WHERE assesmen_id='$assesmen_id'";
		  $this->db->query($q);
		  
		   $q="INSERT INTO tpoliklinik_assesmen_igd_his_obat (versi_edit,id,assesmen_id,pendaftaran_id,idpasien,nama_obat,dosis,waktu,staktif)
			SELECT '$jml_edit',id,assesmen_id,pendaftaran_id,idpasien,nama_obat,dosis,waktu,staktif
			 FROM tpoliklinik_assesmen_igd_obat
			 WHERE assesmen_id='$assesmen_id' AND staktif='1'";
		  $this->db->query($q);
		  $q="INSERT INTO tpoliklinik_assesmen_igd_his_obat_infus (versi_edit,id,assesmen_id,pendaftaran_id,idpasien,tanggal_infus,obat_id,idtipe,kuantitas_infus,rute,dosis_infus,ppa_pemberi,ppa_periksa,ppa_created,staktif)
			SELECT '$jml_edit',id,assesmen_id,pendaftaran_id,idpasien,tanggal_infus,obat_id,idtipe,kuantitas_infus,rute,dosis_infus,ppa_pemberi,ppa_periksa,ppa_created,staktif
			 FROM tpoliklinik_assesmen_igd_obat_infus
			 WHERE assesmen_id='$assesmen_id' AND staktif='1'";
		  $this->db->query($q);
		  
		  $q="INSERT INTO tpoliklinik_assesmen_igd_his_tindakan (versi_edit,id,assesmen_id,pendaftaran_id,idpasien,tanggal_input,mtindakan_id,ppa_pelaksana,waktu_mulai,waktu_selesai,created_ppa,created_date,edited_ppa,edited_date,staktif)
			SELECT '$jml_edit',id,assesmen_id,pendaftaran_id,idpasien,tanggal_input,mtindakan_id,ppa_pelaksana,waktu_mulai,waktu_selesai,created_ppa,created_date,edited_ppa,edited_date,staktif
			 FROM tpoliklinik_assesmen_igd_tindakan
			 WHERE assesmen_id='$assesmen_id' AND staktif='1'";
		  $this->db->query($q);
		  
		 
		}
	  return $hasil;
	}
	
	function hapus_record_assesmen_igd(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_assesmen_igd',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	
	function simpan_ttd_igd(){
		
		$assesmen_id = $this->input->post('assesmen_id');
		$ttd_hubungan = $this->input->post('ttd_hubungan');
		$ttd_nama = $this->input->post('ttd_nama');
		$jenis_ttd = $this->input->post('jenis_ttd');
		$nama_tabel ='tpoliklinik_assesmen_igd';
		$ttd = $this->input->post('signature64');
		$data=array(
			'jenis_ttd' =>$jenis_ttd,
			'ttd_nama' =>$ttd_nama,
			'ttd_hubungan' =>$ttd_hubungan,
			'ttd' =>$ttd,
			'ttd_date' =>date('Y-m-d H:i:s'),
		);
		// print_r($id);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update($nama_tabel,$data);
		$this->output->set_output(json_encode($hasil));
	}
	
	
	function load_ttd_igd(){
		$assesmen_id = $this->input->post('assesmen_id');
		$q="SELECT H.ttd,H.ttd_date,H.ttd_nama,H.ttd_hubungan,H.jenis_ttd
			FROM tpoliklinik_assesmen_igd H WHERE H.assesmen_id='$assesmen_id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_tindakan_assesmen_igd(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tindakan_id=$this->input->post('tindakan_id');
		$mtindakan_id=$this->input->post('mtindakan_id');
		$mtindakan_nama=$this->input->post('mtindakan_nama');
		$ppa_pelaksana=$this->input->post('ppa_pelaksana');
		$waktu_mulai=$this->input->post('waktu_mulai');
		$waktu_selesai=$this->input->post('waktu_selesai');
		if ($mtindakan_nama==$mtindakan_id){
			$mtindakan_id=$this->cek_duplikate_nama_tindakan_igd($mtindakan_nama,$mtindakan_id);
		}
		$tanggal_input= YMDFormat($this->input->post('tanggaltindakan')). ' ' .$this->input->post('waktutindakan');
		// $diagnosa_id=$this->input->post('diagnosa_id');
		// if ($this->cek_duplicate_diagnosa_asmed($assesmen_id,$mtindakan_id,$diagnosa_id,$prioritas)){
		$data=array(
				'idpasien' =>$idpasien,
				'assesmen_id' =>$assesmen_id,
				'pendaftaran_id' =>$pendaftaran_id,
				'tanggal_input' =>$tanggal_input,
				'mtindakan_id' =>$mtindakan_id,
				'ppa_pelaksana' =>$ppa_pelaksana,
				'waktu_mulai' =>$waktu_mulai,
				'waktu_selesai' =>$waktu_selesai,
				'staktif' =>1,
			);
		if ($tindakan_id){
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$tindakan_id);
			$hasil=$this->db->update('tpoliklinik_assesmen_igd_tindakan',$data);
			
		}else{
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('tpoliklinik_assesmen_igd_tindakan',$data);
		}
		// }else{
			// $hasil=false;
		// }
	
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplikate_nama_tindakan_igd($mtindakan_nama,$mtindakan_id){
		$login_ppa_id=$this->session->userdata('user_id');
		$q="SELECT H.id FROM `mtindakan_igd` H WHERE H.nama='$mtindakan_nama'";
		$id=$this->db->query($q)->row('id');
		if ($id){
			return $id;
		}else{
			$q="INSERT INTO mtindakan_igd(nama,created_by,created_date) VALUES('$mtindakan_nama','$login_ppa_id',NOW())";
			$this->db->query($q);
			$id=$this->db->insert_id();
			return $id;
		}
	}
	function load_tindakan_assesmen_igd()
	{
			$assesmen_id=$this->input->post('assesmen_id');
			$q="
					
				SELECT H.*,MP.nama as pelaksana,MC.nama as user_created 
				,MT.nama as nama_tindakan
				FROM tpoliklinik_assesmen_igd_tindakan H
				INNER JOIN mppa MP ON MP.id=H.ppa_pelaksana
				INNER JOIN mppa MC ON MC.id=H.created_ppa
				INNER JOIN mtindakan_igd MT ON MT.id=H.mtindakan_id
				WHERE H.assesmen_id='$assesmen_id'
				AND H.staktif='1'	
				ORDER BY H.tanggal_input ASC
				";
			$list=$this->db->query($q)->result();
			$tabel='';
      foreach ($list as $r) {
          $result = array();

			 $aksi='';
			 $tabel .='<tr>';
          $tabel .='<td class="text-center"><i class="fa fa-calendar"></i> '.HumanDateShort_exp($r->tanggal_input).' </td>';
          $tabel .='<td class="text-center"><i class="si si-clock"></i> '.HumanTime($r->tanggal_input).' </td>';
          $tabel .='<td class="text-left">'.($r->nama_tindakan).'</td>';
          $tabel .='<td class="text-center">'.$r->pelaksana.'</td>';
          $tabel .='<td class="text-center">'.HumanTime($r->waktu_mulai).'</td>';
          $tabel .='<td class="text-center">'.HumanTime($r->waktu_selesai).'</td>';
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_tindakan_assesmen_igd('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_tindakan_assesmen_igd('.$r->id.')" type="button" title="Hapus Obat" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  $aksi .= ($r->user_created?'<br>'.$r->user_created.'<br>'.HumanDateLong($r->created_date):'');
           $tabel .='<td class="text-center">'.$aksi.'</td>';
		  $tabel .= '</tr>';

      }
	  $data['tabel']=$tabel;
      $this->output->set_output(json_encode($data));
  }
  function get_tindakan_assesmen_igd(){
	  $cari 	= $this->input->post('search');
	  $q="SELECT *FROM mtindakan_igd WHERE staktif='1' AND nama LIKE '%".$cari."%'";
	  $hasil=$this->db->query($q)->result_array();
	  $this->output->set_output(json_encode($hasil));
  }
  function hapus_tindakan_assesmen_igd(){
	  $id=$this->input->post('id');
	  $data['staktif']=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_assesmen_igd_tindakan',$data);
	  echo json_encode($hasil);
  }
  
	public function get_obat_rujukan()
	{
		$iddariunit     = $this->input->post('idunit');
		$idtipe     	= $this->input->post('idtipe');
		$idkategori    = $this->input->post('idkategori');
		$iduser=$this->session->userdata('user_id');

		$where='';
		if ($idtipe  != '#') {
			$where .=" AND S.idtipe='$idtipe'";
		} else {
			$where .=" AND S.idtipe IN( SELECT H.idtipe from munitpelayanan_tipebarang H
										LEFT JOIN mdata_tipebarang T ON H.idtipe=T.id
										WHERE H.idunitpelayanan='1'
					)";
		}
		if ($idkategori  != '#') {
			$where .=" AND B.idkategori IN (
							SELECT id FROM (
									SELECT *FROM view_kategori_alkes
									UNION
									SELECT *FROM view_kategori_implan
									UNION
									SELECT *FROM view_kategori_obat
									UNION
									SELECT *FROM view_kategori_logistik
							) T1
							where T1.path like '$idkategori%'
			)";
		}
		$from="(SELECT S.idunitpelayanan,S.idtipe,S.idbarang as id,B.nama,B.idkategori,B.kode,B.namatipe,B.catatan,B.stokminimum,B.stokreorder,S.stok as stok_asli,msatuan.nama as satuan from mgudang_stok S
		INNER JOIN view_barang B ON B.idtipe=S.idtipe AND B.id=S.idbarang
		INNER JOIN msatuan ON msatuan.id=B.idsatuan
		WHERE S.idunitpelayanan='1' ".$where." ORDER BY B.idtipe,B.nama) as tbl";


		$this->select = array();
		$this->join 	= array();
		$this->where  = array();

		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;
		// print_r($from);exit();
		$this->column_search   = array('kode','nama','satuan','stok_asli','catatan');
		$this->column_order    = array('kode','nama','satuan','stok_asli','catatan');

		$list = $this->datatable->get_datatables();
		$data = array();
		$no = $_POST['start'];
		// print_r($list);exit();
		foreach ($list as $r) {
			$no++;
			$row = array();
			$nama_obat='"'.$r->nama.'"';
			$row[] = $no;
			$row[] = $r->kode;
			$row[] = "<a href='#' class='selectObat'  data-idobat='".$r->id."' data-idtipe='".$r->idtipe."' data-dismiss='modal'>".$r->nama."</span></a>";
			$row[] = $r->satuan;

			// $row[] = WarningStok($r->stok_asli, $r->stokreorder, $r->stokminimum);
			// $row[] = $r->catatan;
			// $aksi       = '<div class="btn-group">';
			// $aksi 		.= '<a target="_blank" href="'.site_url().'lgudang_stok/kartustok/'.$r->idtipe.'/'.$r->id.'/'.$r->idunitpelayanan.'" class="btn btn-xs btn-danger ks" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';
			// $aksi.='</div>';
			// $row[] = $aksi;
			$data[] = $row;
		}
		$output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(),
		  "recordsFiltered" => $this->datatable->count_all(),
		  "data" => $data
		);
		echo json_encode($output);
	}
	//CPPT
	function batal_template_cppt(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nama_template=$this->input->post('nama_template');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	  if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
	  }
	  if ($nama_template){
		  $data['status_assemen']='4';
		  $data['nama_template']=$nama_template;
	  }else{
		  
		  $data['status_assemen']='0';
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_cppt',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
	function create_cppt(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $login_profesi_id=$this->session->userdata('login_profesi_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
		
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'profesi_id' => $login_profesi_id,
		'pendaftaran_id' => $pendaftaran_id,
		'idpasien' => $idpasien,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_cppt',$data);
	  $assesmen_id=$this->db->insert_id();
	  
	  
	  $this->output->set_output(json_encode($hasil));
    }
	
	function save_cppt(){
		get_ppa_login();
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$jml_edit=$this->input->post('jml_edit');
		$dokter_pjb=$this->input->post('dokter_pjb');
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'nama_template' => $this->input->post('nama_template'),
			'status_assemen' => $this->input->post('status_assemen'),
			'subjectif' => $this->input->post('subjectif'),
			'objectif' => $this->input->post('objectif'),
			'assemen' => $this->input->post('assemen'),
			'planing' => $this->input->post('planing'),
			'intruksi' => $this->input->post('intruksi'),
			'intervensi' => $this->input->post('intervensi'),
			'evaluasi' => $this->input->post('evaluasi'),
			'reassesmen' => $this->input->post('reassesmen'),

			
		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tpoliklinik_cppt_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			if ($login_pegawai_id=='1'){//Jika Dokter
				if ($login_pegawai_id==$dokter_pjb){
					$data['verifikasi_ppa']=$login_ppa_id;
					$data['st_verifikasi']=1;
					$data['verifikasi_date']=date('Y-m-d H:i:s');
				}
			}
		}
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_cppt',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function list_history_pengkajian_cppt()
    {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_cppt($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		// print_r($logic_akses_assesmen);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$profesi_id=$this->input->post('profesi_id');
		$st_owned=$this->input->post('st_owned');
		$mppa_id=$this->input->post('mppa_id');
		$st_verifikasi=$this->input->post('st_verifikasi');
		// $iddokter=$this->input->post('iddokter');
		// $idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$where='';
		if ($st_owned!='#'){
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			
		}			
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		if ($profesi_id){
			$profesi_id=implode(", ", $profesi_id);
			$where .=" AND (H.profesi_id) IN (".$profesi_id.")";
		}
		if ($mppa_id){
			$mppa_id=implode(", ", $mppa_id);
			$where .=" AND (H.created_ppa) IN (".$mppa_id.")";
		}
		// if ($iddokter!='#'){
			// $where .=" AND MP.iddokter = '".$iddokter."'";
		// }
		// if ($idpoli!='#'){
			// $where .=" AND MP.idpoliklinik = '".$idpoli."'";
		// }
		if ($st_verifikasi!='#'){
			$where .=" AND H.st_verifikasi = '".$st_verifikasi."'";
		}
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_cppt")->row_array();
		$setting_cppt_field=$this->Tpendaftaran_poli_ttv_model->setting_cppt_field($login_profesi_id);
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa
					,H.assesmen_id,H.pendaftaran_id,MP.tanggaldaftar,MP.nopendaftaran,MPOL.nama as poli 
					,MD.nama as dokter,H.tanggal_input as tanggal_pengkajian
					,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,MP.iddokter,MP.idpoliklinik,H.keterangan_edit,H.jml_edit,H.st_edited
					,H.ttd_nama,H.ttd,R.ref as soap_nama
					,H.subjectif,H.objectif,H.assemen,H.planing,H.intruksi,H.intervensi,H.evaluasi,H.reassesmen
					,H.st_verifikasi,H.dokter_pjb,H.profesi_id
					,ppa_verif.nama as nama_verif,H.verifikasi_date
					FROM `tpoliklinik_cppt` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					INNER JOIN mpoliklinik MPOL ON MPOL.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					INNER JOIN merm_referensi R ON R.nilai=H.profesi_id AND R.ref_head_id='21'
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					LEFT JOIN mppa ppa_verif ON ppa_verif.id=H.verifikasi_ppa

					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
					ORDER BY H.tanggal_input DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','singkatan_kajian');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
			$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
			$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/input_cppt/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			// $btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
			$btn_ttd='';
			 if ($akses_general['st_edit_catatan']=='0'){
				  $btn_edit='';
			  }else{
				  if ($akses_general['lama_edit']>0){
					  if ($r->selisih>$akses_general['lama_edit']){
						  $btn_edit='';
					  }else{
						  
					  }
					  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id!=$r->created_ppa){
								$btn_edit='';
						  }
					  }
				  }
			  }
			  if ($akses_general['st_duplikasi_catatan']=='0'){
				  $btn_duplikasi='';
			  }else{
				  if ($akses_general['lama_duplikasi']>0){
					  if ($r->selisih>$akses_general['lama_duplikasi']){
						  $btn_duplikasi='';
					  }else{
						 
					  }
				  }
				   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_duplikasi='';
					  }
				   }
			  }
			  if ($akses_general['st_hapus_catatan']=='0'){
				  $btn_hapus='';
			  }else{
				  if ($akses_general['lama_hapus']>0){
					  if ($r->selisih>$akses_general['lama_hapus']){
						  $btn_hapus='';
					  }else{
						 
					  }
					   
				  }
				  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id <> $r->created_ppa){
							$btn_hapus='';
					  }else{
						  
					  }
							// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
				  }
			  }
			if ($logic_akses_assesmen['st_edit_cppt']=='0'){
			  $btn_edit='';
			}
			if ($logic_akses_assesmen['st_hapus_cppt']=='0'){
			  $btn_hapus='';
			}
			if ($r->profesi_id!=$login_profesi_id){
				$btn_edit='';
				$btn_hapus='';
				$btn_duplikasi='';
			}
			// $result[] = $no;
			$aksi='';
			$aksi_edit='';
			$btn_verifikasi='';
			
			if ($r->jml_edit>0){
				$aksi_edit='<a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_cppt/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></i> '.$r->keterangan_edit.'</a>';
			}
			$status_verifikasi='';
			if ($r->st_verifikasi){
				$btn_edit='';
				$btn_hapus='';
				$status_verifikasi='&nbsp;&nbsp;&nbsp;'.text_success('SUDAH DIVERIFIKASI');
				$status_verifikasi .='&nbsp;&nbsp;'.text_default(HumanDateLong($r->verifikasi_date).' ('.$r->nama_verif.')');
			}else{
				$status_verifikasi='&nbsp;&nbsp;'.text_danger('MENUNGGU DIVERIFIKASI');
				$btn_verifikasi='<button class="btn btn-primary  btn-xs" '.$btn_disabel_edit.' type="button" title="Verifikasi" onclick="verifikasi_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-check"></i> Verifikasi</button>';
				if ($login_profesi_id=='1'){
					if ($r->dokter_pjb!=$login_pegawai_id){//Jika Create yang bersangkutan
						$btn_verifikasi='';
					}
					
				}else{
					$btn_verifikasi='';
				}
			}
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_duplikasi;	
			$aksi .= $btn_hapus;	
			if ($logic_akses_assesmen['st_lihat_cppt']=='1'){
			$aksi .= $btn_lihat;	
			}
			$aksi .= $aksi_edit;	
			
			$aksi .= $btn_verifikasi;	
			$aksi .= '</div>';
			$jml_kolom=0;
			$subjectif='';$objectif='';$assemen='';$planing='';$intruksi='';$intervensi='';$evaluasi='';$reassesmen='';
			if ($setting_cppt_field['field_subjectif']=='1'){$jml_kolom=$jml_kolom+1;}
			if ($setting_cppt_field['field_objectif']=='1'){$jml_kolom=$jml_kolom+1;}
			if ($setting_cppt_field['field_assesmen']=='1'){$jml_kolom=$jml_kolom+1;}
			if ($setting_cppt_field['field_planing']=='1'){$jml_kolom=$jml_kolom+1;}
			if ($setting_cppt_field['field_intruction']=='1'){$jml_kolom=$jml_kolom+1;}
			if ($setting_cppt_field['field_intervensi']=='1'){$jml_kolom=$jml_kolom+1;}
			if ($setting_cppt_field['field_evaluasi']=='1'){$jml_kolom=$jml_kolom+1;}
			if ($setting_cppt_field['field_reasessmen']=='1'){$jml_kolom=$jml_kolom+1;}
			$lebar_kolom=100/$jml_kolom;
			if ($setting_cppt_field['field_subjectif']){
				$subjectif='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">SUBJECTIVE</div>
								<div class="push-5-t"> '.$r->subjectif.'</div>
							</td>';
				
			}
			if ($setting_cppt_field['field_objectif']){
				$objectif='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">OBJECTIVE</div>
								<div class="push-5-t"> '.$r->objectif.'</div>
							</td>';
				
			}
			if ($setting_cppt_field['field_assesmen']){
				$assemen='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">ASSESMENT</div>
								<div class="push-5-t"> '.$r->assemen.'</div>
							</td>';
				
			}
			if ($setting_cppt_field['field_planing']){
				$planing='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">PLANING</div>
								<div class="push-5-t"> '.$r->planing.'</div>
							</td>';
				
			}
			if ($setting_cppt_field['field_intruction']){
				$intruksi='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">INTRUCTIONS</div>
								<div class="push-5-t"> '.$r->intruksi.'</div>
							</td>';
				
			}
			if ($setting_cppt_field['field_intervensi']){
				$intervensi='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">INTERVENTION</div>
								<div class="push-5-t"> '.$r->intervensi.'</div>
							</td>';
				
			}
			if ($setting_cppt_field['field_evaluasi']){
				$evaluasi='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">EVALUATION</div>
								<div class="push-5-t"> '.$r->evaluasi.'</div>
							</td>';
				
			}
			if ($setting_cppt_field['field_reasessmen']){
				$reassesmen='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">RE-ASSESMENT</div>
								<div class="push-5-t"> '.$r->reassesmen.'</div>
							</td>';
				
			}
			
			$tabel='<table class="table table-condensed" style="border-spacing:0;border-collapse: collapse!important;">
						<tbody>
							<tr>
								<td colspan="'.$jml_kolom.'" class="bg-white" style="width: 100%;">
									<div class="h5"><strong>'.strtoupper($r->soap_nama).$status_verifikasi.'</strong></div>
									<div class="push-5-t">'.$r->nopendaftaran.' | Add On '.tanggal_indo_DMY($r->tanggal_pengkajian).' at '.HumanTimeShort($r->tanggal_pengkajian).' Oleh '.$r->nama_mppa.' &nbsp;&nbsp;&nbsp;'.$aksi.'</div>
								</td>
							</tr>
							<tr>
								'.$subjectif.'								
								'.$objectif.'								
								'.$assemen.'								
								'.$planing.'								
								'.$intruksi.'								
								'.$intervensi.'								
								'.$evaluasi.'								
								'.$reassesmen.'								
							</tr>
						</tbody>
					</table>';
			// $result[] = HumanDateLong($r->tanggaldaftar);
			// $result[] = ($r->nopendaftaran);
			// $result[] = ('<strong>'.$r->poli.'</strong><br>'.$r->dokter);
			// $result[] = HumanDateLong($r->tanggal_pengkajian);
			// $result[] = ($r->nama_mppa).$btn_ttd;
			// $result[] = ($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
			 $result[] =$tabel;
			$data[] = $result;
			
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
		}
		
		function save_edit_cppt(){
			$assesmen_id=$this->input->post('assesmen_id');
			$alasan_edit_id=$this->input->post('alasan_id');
			$keterangan_edit=$this->input->post('keterangan_edit');
			$jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_cppt WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// $jml_edit=$this->input->post('jml_edit');
			$res=$this->simpan_history_cppt($assesmen_id,$jml_edit);
			// print_r($res);exit;
			if ($res){
				
				$data=array(
					'status_assemen' => 1,
					'st_edited' => 1,
					'alasan_edit_id' =>$alasan_edit_id,
					'keterangan_edit' =>$keterangan_edit,

				);
				$login_ppa_id=$this->session->userdata('login_ppa_id');
				$data['edited_ppa']=$login_ppa_id;
				$data['edited_date']=date('Y-m-d H:i:s');
				$this->db->where('assesmen_id',$assesmen_id);
				$result=$this->db->update('tpoliklinik_cppt',$data);
				// $ttv_id=$this->db->insert_id();
				
				if ($result){
					$hasil=$data;
				}else{
					$hasil=false;
				}
			}else{
				$hasil=$res;
			}
			$this->output->set_output(json_encode($hasil));
		}
		function simpan_history_cppt($assesmen_id,$jml_edit){
			$hasil=false;
			// $jml_edit=$jml_edit+1;
			$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tpoliklinik_cppt_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
			// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
			if ($baris==0){
				$login_ppa_id=$this->session->userdata('login_ppa_id');
				$q="INSERT IGNORE INTO tpoliklinik_cppt_his 
							(versi_edit,
							assesmen_id,template_assesmen_id,nama_template,jumlah_template,pendaftaran_id,idpasien,dokter_pjb,profesi_id,tanggal_input,status_assemen,subjectif,objectif,assemen,planing,intruksi,intervensi,evaluasi,reassesmen,st_verifikasi,created_ppa,created_date,edited_ppa,edited_date,deleted_ppa,deleted_date,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_edited,ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd,verifikasi_ppa,verifikasi_date)
					SELECT '$jml_edit',
					'$assesmen_id',template_assesmen_id,nama_template,jumlah_template,pendaftaran_id,idpasien,dokter_pjb,profesi_id,tanggal_input,status_assemen,subjectif,objectif,assemen,planing,intruksi,intervensi,evaluasi,reassesmen,st_verifikasi,created_ppa,created_date,edited_ppa,edited_date,deleted_ppa,deleted_date,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_edited,ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd,verifikasi_ppa,verifikasi_date 
					FROM tpoliklinik_cppt 
					WHERE assesmen_id='$assesmen_id'";
				// print_r($q);exit;
				$hasil=$this->db->query($q);
				
			  
			}
		  return $hasil;
		}
		
		function batal_cppt(){
		  $login_ppa_id=$this->session->userdata('login_ppa_id');
		  $pendaftaran_id=$this->input->post('pendaftaran_id');
		  $assesmen_id=$this->input->post('assesmen_id');
		  $st_edited=$this->input->post('st_edited');
		  $jml_edit=$this->input->post('jml_edit');
		  $data=array(
			'deleted_ppa' => $login_ppa_id,
			'deleted_date' => date('Y-m-d H:i:s'),
			
		  );
		   if ($jml_edit=='0'){
			  $data['edited_ppa']=null;
			  $data['edited_date']=null;
			  $data['alasan_edit_id']='';
			  $data['keterangan_edit']='';
			  
		  }
		  if ($st_edited=='1'){
				$q="SELECT * FROM tpoliklinik_cppt_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				$data_his_edit=$this->db->query($q)->row();
				if ($data_his_edit){
					$data['edited_ppa']=$data_his_edit->edited_ppa;
					$data['edited_date']=$data_his_edit->edited_date;
					$data['alasan_edit_id']=$data_his_edit->alasan_edit_id;
					$data['keterangan_edit']=$data_his_edit->keterangan_edit;
					$data['tanggal_input']=$data_his_edit->tanggal_input;
					$data['subjectif']=$data_his_edit->subjectif;
					$data['objectif']=$data_his_edit->objectif;
					$data['assemen']=$data_his_edit->assemen;
					$data['planing']=$data_his_edit->planing;
					$data['intruksi']=$data_his_edit->intruksi;
					$data['intervensi']=$data_his_edit->intervensi;
					$data['evaluasi']=$data_his_edit->evaluasi;
					$data['reassesmen']=$data_his_edit->reassesmen;

				}
				
				
				$q="DELETE FROM tpoliklinik_cppt_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				$this->db->query($q);
				
				
			  $data['status_assemen']='2';
		  }else{
			  $data['status_assemen']='0';
			  
		  }
		  $this->db->where('assesmen_id',$assesmen_id);
		  $hasil=$this->db->update('tpoliklinik_cppt',$data);
		  $this->output->set_output(json_encode($hasil));
	  }
  function create_with_template_cppt(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $q="INSERT INTO tpoliklinik_cppt (
			template_assesmen_id,
			nama_template,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,status_assemen,profesi_id,
			subjectif,objectif,assemen,planing,intruksi,intervensi,evaluasi,reassesmen,st_verifikasi
			)
			SELECT '$template_assesmen_id',nama_template,NOW(),'$pendaftaran_id','$idpasien',NOW(),'$login_ppa_id',1,profesi_id,
			subjectif,objectif,assemen,planing,intruksi,intervensi,evaluasi,reassesmen,st_verifikasi
			FROM `tpoliklinik_cppt`

			WHERE assesmen_id='$template_assesmen_id'";
		// print_r($q);exit;
	  $hasil=$this->db->query($q);
	  $assesmen_id=$this->db->insert_id();
	 
	  $q="UPDATE tpoliklinik_cppt 
			SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
			$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
   }
   function hapus_record_cppt(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_cppt',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function verifikasi_cppt(){
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data=array(
			'st_verifikasi' => 1,
			'verifikasi_ppa' => $login_ppa_id,
			'verifikasi_date' =>date('Y-m-d H:i:s'),

		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_cppt',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function list_index_template_cppt()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.*,M.nama as nama_mppa
						 FROM tpoliklinik_cppt H 
						 LEFT JOIN mppa M ON M.id=H.created_ppa
						 WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.assesmen_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		// $result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen('.$r->assesmen_id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->assesmen_id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->assesmen_id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		// $result[] = $aksi;
		// $result[] = ($r->nama_template);
		// $result[] = ($r->jumlah_template);
		// $data[] = $result;
		$jml_kolom=0;
			$subjectif='';$objectif='';$assemen='';$planing='';$intruksi='';$intervensi='';$evaluasi='';$reassesmen='';
			if ($r->subjectif){$jml_kolom=$jml_kolom+1;}
			if ($r->objectif){$jml_kolom=$jml_kolom+1;}
			if ($r->assemen){$jml_kolom=$jml_kolom+1;}
			if ($r->planing){$jml_kolom=$jml_kolom+1;}
			if ($r->intruksi){$jml_kolom=$jml_kolom+1;}
			if ($r->intervensi){$jml_kolom=$jml_kolom+1;}
			if ($r->evaluasi){$jml_kolom=$jml_kolom+1;}
			if ($r->reassesmen){$jml_kolom=$jml_kolom+1;}
			$lebar_kolom=100/$jml_kolom;
			if ($r->subjectif){
				$subjectif='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">SUBJECTIVE</div>
								<div class="push-5-t"> '.$r->subjectif.'</div>
							</td>';
				
			}
			if ($r->objectif){
				$objectif='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">OBJECTIVE</div>
								<div class="push-5-t"> '.$r->objectif.'</div>
							</td>';
				
			}
			if ($r->assemen){
				$assemen='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">SUBJECTIVE</div>
								<div class="push-5-t"> '.$r->assemen.'</div>
							</td>';
				
			}
			if ($r->planing){
				$planing='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">PLANING</div>
								<div class="push-5-t"> '.$r->planing.'</div>
							</td>';
				
			}
			if ($r->intruksi){
				$intruksi='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">INTRUCTIONS</div>
								<div class="push-5-t"> '.$r->intruksi.'</div>
							</td>';
				
			}
			if ($r->intervensi){
				$intervensi='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">INTERVENTION</div>
								<div class="push-5-t"> '.$r->intervensi.'</div>
							</td>';
				
			}
			if ($r->evaluasi){
				$evaluasi='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">EVALUATION</div>
								<div class="push-5-t"> '.$r->evaluasi.'</div>
							</td>';
				
			}
			if ($r->reassesmen){
				$reassesmen='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">RE-ASSESMENT</div>
								<div class="push-5-t"> '.$r->reassesmen.'</div>
							</td>';
				
			}
			
			$tabel='<table class="table table-condensed" style="border-spacing:0;border-collapse: collapse!important;">
						<tbody>
							<tr>
								<td colspan="'.$jml_kolom.'" class="bg-white" style="width: 100%;">
									<div class="h5"><strong>'.strtoupper($r->nama_template).'</strong></div>
									<div class="push-5-t"> | Add On '.tanggal_indo_DMY($r->tanggal_input).' at '.HumanTimeShort($r->tanggal_input).' Oleh '.$r->nama_mppa.' &nbsp;&nbsp;&nbsp;'.$aksi.'</div>
								</td>
							</tr>
							<tr>
								'.$subjectif.'								
								'.$objectif.'								
								'.$assemen.'								
								'.$planing.'								
								'.$intruksi.'								
								'.$intervensi.'								
								'.$evaluasi.'								
								'.$reassesmen.'								
							</tr>
						</tbody>
					</table>';
			 $result[] =$tabel;
			$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }	
  function create_template_cppt(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $login_profesi_id=$this->session->userdata('login_profesi_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'profesi_id' => $login_profesi_id,
		'pendaftaran_id' => $pendaftaran_id,
		'idpasien' => $idpasien,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 3,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_cppt',$data);
	  $this->output->set_output(json_encode($hasil));
    }
	
	function edit_template_cppt(){
	  $assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'status_assemen' => 3,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_cppt',$data);
		
		$this->output->set_output(json_encode($result));
  }
  function verifikasi_cppt_all(){
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$q="SELECT H.assesmen_id FROM tpoliklinik_cppt H
					WHERE H.dokter_pjb='$login_pegawai_id' AND H.st_verifikasi='0' AND H.status_assemen='2'";
		$row=$this->db->query($q)->result();
		foreach($row as $r){
			$data=array(
				'st_verifikasi' => 1,
				'verifikasi_ppa' => $login_ppa_id,
				'verifikasi_date' =>date('Y-m-d H:i:s'),

			);
			$this->db->where('assesmen_id',$r->assesmen_id);
			$result=$this->db->update('tpoliklinik_cppt',$data);
		}
		
		
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
  }
  
  //IC
  function batal_template_ic(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nama_template=$this->input->post('nama_template');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	  if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
	  }
	  if ($nama_template){
		  $data['status_assemen']='4';
		  $data['nama_template']=$nama_template;
	  }else{
		  
		  $data['status_assemen']='0';
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_ic',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  
	function create_ic(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
		
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'pendaftaran_id' => $pendaftaran_id,
		'tanggal_informasi' => date('Y-m-d H:i:s'),
		'tanggal_pernyataan' => date('Y-m-d H:i:s'),
		'idpasien' => $idpasien,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_ic',$data);
	  $assesmen_id=$this->db->insert_id();
	  
	  $q="INSERT INTO tpoliklinik_ic_informasi (assesmen_id,nourut,nama,nama_english,isi_informasi)
			SELECT '$assesmen_id',H.nourut,H.nama,H.nama_english,'' FROM mjenis_info H
			WHERE H.staktif='1'";
	  $this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
    }
	
	function save_ic(){
		get_ppa_login();
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		$tanggal_pernyataan= YMDFormat($this->input->post('tanggalpernyataan')). ' ' .$this->input->post('waktupernyataan');
		$tanggal_informasi= YMDFormat($this->input->post('tanggalinformasi')). ' ' .$this->input->post('waktuinformasi');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$jml_edit=$this->input->post('jml_edit');
		// print_r($tanggal_pernyataan);exit;
		$data=array(
			'tanggal_input'=> $tanggal_input,
			'nama_template' => $this->input->post('nama_template'),
			'status_assemen' => $this->input->post('status_assemen'),
			'dokter_pelaksana' => $this->input->post('dokter_pelaksana'),
			'tanggal_informasi' => $tanggal_informasi,
			'pemberi_info' => $this->input->post('pemberi_info'),
			'pendamping' => $this->input->post('pendamping'),
			'penerima_info' => $this->input->post('penerima_info'),
			'hubungan_id' => $this->input->post('hubungan_id'),
			'nama' => $this->input->post('nama'),
			'yang_menyatakan' => $this->input->post('nama'),
			'umur' => $this->input->post('umur'),
			'jenis_kelamin' => $this->input->post('jenis_kelamin'),
			'alamat' => $this->input->post('alamat'),
			'no_ktp' => $this->input->post('no_ktp'),
			'hereby' => $this->input->post('hereby'),
			'untuk_tindakan' => $this->input->post('untuk_tindakan'),
			'terhadap' => $this->input->post('terhadap'),
			'nama_pasien' => $this->input->post('nama_pasien'),
			'tanggal_lahir_pasien' => YMDFormat($this->input->post('tanggal_lahir_pasien')),
			'umur_pasien' => $this->input->post('umur_pasien'),
			'jenis_kelamin_pasien' => $this->input->post('jenis_kelamin_pasien'),
			'no_medrec' => $this->input->post('no_medrec'),
			'tanggal_pernyataan' => $tanggal_pernyataan,
			// 'yang_menyatakan' => $this->input->post('yang_menyatakan'),
			// 'saksi_rs' => $this->input->post('saksi_rs'),
			// 'saksi_keluarga' => $this->input->post('saksi_keluarga'),

			
		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tpoliklinik_ic_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			if ($login_pegawai_id=='1'){//Jika Dokter
				if ($login_pegawai_id==$dokter_pjb){
					$data['verifikasi_ppa']=$login_ppa_id;
					$data['st_verifikasi']=1;
					$data['verifikasi_date']=date('Y-m-d H:i:s');
				}
			}
		}
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_ic',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function list_history_pengkajian_ic()
    {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_ic($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		// print_r($logic_akses_assesmen);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$profesi_id=$this->input->post('profesi_id');
		$st_owned=$this->input->post('st_owned');
		$mppa_id=$this->input->post('mppa_id');
		$st_verifikasi=$this->input->post('st_verifikasi');
		// $iddokter=$this->input->post('iddokter');
		// $idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$where='';
		if ($st_owned!='#'){
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}else{
				$where .=" AND (H.created_ppa) != '$login_ppa_id'";
				
			}			
			
		}			
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		if ($profesi_id){
			$profesi_id=implode(", ", $profesi_id);
			$where .=" AND (H.profesi_id) IN (".$profesi_id.")";
		}
		if ($mppa_id){
			$mppa_id=implode(", ", $mppa_id);
			$where .=" AND (H.created_ppa) IN (".$mppa_id.")";
		}
		// if ($iddokter!='#'){
			// $where .=" AND MP.iddokter = '".$iddokter."'";
		// }
		// if ($idpoli!='#'){
			// $where .=" AND MP.idpoliklinik = '".$idpoli."'";
		// }
		if ($st_verifikasi!='#'){
			$where .=" AND H.st_verifikasi = '".$st_verifikasi."'";
		}
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_ic")->row_array();
		$this->select = array();
		$from="
				(
					SELECT DP.nama as dokter_pelaksana_nama
					,DPI.nama as pemberi_info_nama,DPP.nama as pendamping_nama,M.ref as persetujuan,P.nopendaftaran
					,UC.nama as created_nama
					,UE.nama as edited_nama
					,H.* 
					FROM `tpoliklinik_ic` H
					INNER JOIN tpoliklinik_pendaftaran P ON P.id=H.pendaftaran_id
					LEFT JOIN mppa DP ON DP.id=H.dokter_pelaksana
					LEFT JOIN mppa DPI ON DPI.id=H.pemberi_info
					LEFT JOIN mppa DPP ON DPP.id=H.pendamping
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN mppa UE ON UE.id=H.edited_ppa
					LEFT JOIN merm_referensi M ON M.nilai=H.hereby AND M.ref_head_id='78'

					WHERE H.status_assemen='2' ORDER BY H.tanggal_input DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('pemberi_info_nama');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
			$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
			$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
			$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/input_ic/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			// $btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
			$btn_ttd='';
			 if ($akses_general['st_edit_catatan']=='0'){
				  $btn_edit='';
			  }else{
				  if ($akses_general['lama_edit']>0){
					  if ($r->selisih>$akses_general['lama_edit']){
						  $btn_edit='';
					  }else{
						  
					  }
					  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
						  if ($login_ppa_id!=$r->created_ppa){
								$btn_edit='';
						  }
					  }
				  }
			  }
			  if ($akses_general['st_duplikasi_catatan']=='0'){
				  $btn_duplikasi='';
			  }else{
				  if ($akses_general['lama_duplikasi']>0){
					  if ($r->selisih>$akses_general['lama_duplikasi']){
						  $btn_duplikasi='';
					  }else{
						 
					  }
				  }
				   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_duplikasi='';
					  }
				   }
			  }
			  if ($akses_general['st_hapus_catatan']=='0'){
				  $btn_hapus='';
			  }else{
				  if ($akses_general['lama_hapus']>0){
					  if ($r->selisih>$akses_general['lama_hapus']){
						  $btn_hapus='';
					  }else{
						 
					  }
					   
				  }
				  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id <> $r->created_ppa){
							$btn_hapus='';
					  }else{
						  
					  }
							// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
				  }
			  }
			if ($logic_akses_assesmen['st_edit_ic']=='0'){
			  $btn_edit='';
			}
			if ($logic_akses_assesmen['st_hapus_ic']=='0'){
			  $btn_hapus='';
			}
			
			// $result[] = $no;
			$aksi='';
			$aksi_edit='';
			$btn_verifikasi='';
			
			if ($r->jml_edit>0){
				$aksi_edit='<a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->pendaftaran_id.'/erm_rj/his_ic/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></i> '.$r->keterangan_edit.'</a>';
			}
			
			$aksi = '<div class="btn-group">';
			$aksi .= $btn_edit;	
			$aksi .= $btn_duplikasi;	
			$aksi .= $btn_hapus;	
			if ($logic_akses_assesmen['st_lihat_ic']=='0'){
			$aksi_edit = '';	
			}
			
			
			$aksi .= '</div>';
			$jml_kolom=0;
			$btn_1='';
			$btn_2='
				<td class="bg-white" style="width: 15%;">
					<div class="h5  text-left text-primary">Dokter Pelaksana Tindakan</div>
					<div class="h5   text-left"> '.($r->dokter_pelaksana_nama).' </div>
					<div class="h5  text-left text-primary push-10-t">Nama Penerima Informasi</div>
					<div class="h5   text-left"> '.($r->penerima_info).'</div>
				</td>';
			$btn_3='
				<td class="bg-white" style="width: 15%;">
					<div class="h5  text-left text-primary">Nama Pemberi Informasi</div>
					<div class="h5   text-left"> '.($r->pemberi_info_nama).' </div>
					<div class="h5  text-left text-primary push-10-t">Waktu Pemberian Informasi</div>
					<div class="h5   text-left"> '.HumanDateLong($r->tanggal_informasi).'</div>
				</td>';
			$btn_4='
				<td class="bg-white" style="width: 15%;">
					<div class="h5  text-left text-primary">Nama Petugas Yang Mendampingi</div>
					<div class="h5   text-left"> '.($r->pendamping_nama).' </div>
					<div class="h5  text-left text-primary push-10-t">Yang Memberikan Pernyataan</div>
					<div class="h5   text-left"> '.($r->yang_menyatakan).'</div>
				</td>';
			$btn_5='
				<td class="bg-white" style="width: 15%;">
					<div class="h5  text-left text-primary">Waktu Pernyataan Tindakan</div>
					<div class="h5   text-left"> '.HumanDateLong($r->tanggal_pernyataan).' </div>
					
				</td>';
			$btn_6='
				<td class="bg-white" style="width: 15%;">
					<div class="h5  text-left text-primary">'.$r->nopendaftaran.'</div>
					<div class="h5   text-left"> '.($r->hereby=='1'?text_success($r->persetujuan):text_danger($r->persetujuan)).' </div>
					
				</td>';
			   if ($r->jml_edit>0){
			  $info_edit='  | <i class="fa fa-pencil text-danger"></i> Edit On '.tanggal_indo_DMY($r->edited_date).' at '.HumanTimeShort($r->edited_date).' Oleh '.$r->edited_nama;
				   
			   }else{
				   $info_edit='';
			   }
			  $btn_1 .='<table class="block-table text-left">
							<tbody>
								<tr>
									<td colspan="6">
										<div class="h5"><strong>'.strtoupper($r->nopendaftaran).'</strong></div>
										<div class="push-5-t"><i class="fa fa-plus text-muted"></i> Add On '.tanggal_indo_DMY($r->created_date).' at '.HumanTimeShort($r->created_date).' Oleh '.$r->created_nama.'</div>
									</td>
								</tr>
								<tr>
									<td class="bg-white" style="width: 10%;">
										<div class="h2 font-w700 '.($r->alasan_edit_id?'text-danger':'text-primary').' text-center">'.DFormat($r->tanggal_input).'</div>
										<div class="h5  text-center"> '.tanggal_indo_MY($r->tanggal_input).'</div>
										<div class="h5   text-center"> '.HumanTime($r->tanggal_input).'</div>
										<div class="h5 text-muted text-uppercase push-5-t text-center"> 
											<div class="btn-group" role="group">
												'.$btn_edit.'
												'.$btn_duplikasi.'
												'.$btn_hapus.'
												'.$btn_lihat.'
												'.$aksi_edit.'
												
												
											</div>
										
										</div>
										'.($r->alasan_edit_id?'<div class="text-center text-success push-5-t"><a href="javascript:void(0)" onclick="lihat_perubahan('.$r->id.')"><i class="fa fa-pencil text-center"></i> '.$r->ket_edit.'</a></div>':'').'
									</td>
									'.$btn_2.'
									'.$btn_3.'
									'.$btn_4.'
									'.$btn_5.'
									'.$btn_6.'
								</tr>
							</tbody>
						</table>';
			
			  $result[] = $btn_1;
			  
			
			$data[] = $result;
			
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
		}
		
		function save_edit_ic(){
			$assesmen_id=$this->input->post('assesmen_id');
			$alasan_edit_id=$this->input->post('alasan_id');
			$keterangan_edit=$this->input->post('keterangan_edit');
			$jml_edit=$this->db->query("SELECT jml_edit FROM tpoliklinik_ic WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// $jml_edit=$this->input->post('jml_edit');
			$res=$this->simpan_history_ic($assesmen_id,$jml_edit);
			// print_r($res);exit;
			if ($res){
				
				$data=array(
					'status_assemen' => 1,
					'st_edited' => 1,
					'alasan_edit_id' =>$alasan_edit_id,
					'keterangan_edit' =>$keterangan_edit,

				);
				$login_ppa_id=$this->session->userdata('login_ppa_id');
				$data['edited_ppa']=$login_ppa_id;
				$data['edited_date']=date('Y-m-d H:i:s');
				$this->db->where('assesmen_id',$assesmen_id);
				$result=$this->db->update('tpoliklinik_ic',$data);
				// $ttv_id=$this->db->insert_id();
				
				if ($result){
					$hasil=$data;
				}else{
					$hasil=false;
				}
			}else{
				$hasil=$res;
			}
			$this->output->set_output(json_encode($hasil));
		}
		function simpan_history_ic($assesmen_id,$jml_edit){
			$hasil=false;
			// $jml_edit=$jml_edit+1;
			$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tpoliklinik_ic_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
			// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
			if ($baris==0){
				$login_ppa_id=$this->session->userdata('login_ppa_id');
				$q="INSERT IGNORE INTO tpoliklinik_ic_his 
							(versi_edit,
							assesmen_id,template_assesmen_id,nama_template,jumlah_template,pendaftaran_id,idpasien,dokter_pjb,profesi_id,tanggal_input,status_assemen,subjectif,objectif,assemen,planing,intruksi,intervensi,evaluasi,reassesmen,st_verifikasi,created_ppa,created_date,edited_ppa,edited_date,deleted_ppa,deleted_date,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_edited,ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd,verifikasi_ppa,verifikasi_date)
					SELECT '$jml_edit',
					'$assesmen_id',template_assesmen_id,nama_template,jumlah_template,pendaftaran_id,idpasien,dokter_pjb,profesi_id,tanggal_input,status_assemen,subjectif,objectif,assemen,planing,intruksi,intervensi,evaluasi,reassesmen,st_verifikasi,created_ppa,created_date,edited_ppa,edited_date,deleted_ppa,deleted_date,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_edited,ttd,ttd_date,ttd_nama,ttd_hubungan,jenis_ttd,verifikasi_ppa,verifikasi_date 
					FROM tpoliklinik_ic 
					WHERE assesmen_id='$assesmen_id'";
				// print_r($q);exit;
				$hasil=$this->db->query($q);
				
			  
			}
		  return $hasil;
		}
		
		function batal_ic(){
		  $login_ppa_id=$this->session->userdata('login_ppa_id');
		  $pendaftaran_id=$this->input->post('pendaftaran_id');
		  $assesmen_id=$this->input->post('assesmen_id');
		  $st_edited=$this->input->post('st_edited');
		  $jml_edit=$this->input->post('jml_edit');
		  $data=array(
			'deleted_ppa' => $login_ppa_id,
			'deleted_date' => date('Y-m-d H:i:s'),
			
		  );
		   if ($jml_edit=='0'){
			  $data['edited_ppa']=null;
			  $data['edited_date']=null;
			  $data['alasan_edit_id']='';
			  $data['keterangan_edit']='';
			  
		  }
		  if ($st_edited=='1'){
				$q="SELECT * FROM tpoliklinik_ic_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				$data_his_edit=$this->db->query($q)->row();
				if ($data_his_edit){
					$data['edited_ppa']=$data_his_edit->edited_ppa;
					$data['edited_date']=$data_his_edit->edited_date;
					$data['alasan_edit_id']=$data_his_edit->alasan_edit_id;
					$data['keterangan_edit']=$data_his_edit->keterangan_edit;
					$data['tanggal_input']=$data_his_edit->tanggal_input;
					$data['subjectif']=$data_his_edit->subjectif;
					$data['objectif']=$data_his_edit->objectif;
					$data['assemen']=$data_his_edit->assemen;
					$data['planing']=$data_his_edit->planing;
					$data['intruksi']=$data_his_edit->intruksi;
					$data['intervensi']=$data_his_edit->intervensi;
					$data['evaluasi']=$data_his_edit->evaluasi;
					$data['reassesmen']=$data_his_edit->reassesmen;

				}
				
				
				$q="DELETE FROM tpoliklinik_ic_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'";
				$this->db->query($q);
				
				
			  $data['status_assemen']='2';
		  }else{
			  $data['status_assemen']='0';
			  
		  }
		  $this->db->where('assesmen_id',$assesmen_id);
		  $hasil=$this->db->update('tpoliklinik_ic',$data);
		  $this->output->set_output(json_encode($hasil));
	  }
  function create_with_template_ic(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $q="INSERT INTO tpoliklinik_ic (
			template_assesmen_id,
			nama_template,tanggal_input,pendaftaran_id,idpasien,created_date,created_ppa,status_assemen,profesi_id,
			subjectif,objectif,assemen,planing,intruksi,intervensi,evaluasi,reassesmen,st_verifikasi
			)
			SELECT '$template_assesmen_id',nama_template,NOW(),'$pendaftaran_id','$idpasien',NOW(),'$login_ppa_id',1,profesi_id,
			subjectif,objectif,assemen,planing,intruksi,intervensi,evaluasi,reassesmen,st_verifikasi
			FROM `tpoliklinik_ic`

			WHERE assesmen_id='$template_assesmen_id'";
		// print_r($q);exit;
	  $hasil=$this->db->query($q);
	  $assesmen_id=$this->db->insert_id();
	 
	  $q="UPDATE tpoliklinik_ic 
			SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
			$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
   }
   function hapus_record_ic(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_hapus=$this->input->post('keterangan_hapus');
		$data=array(
			'status_assemen' => 0,
			'st_edited' => 0,
			'alasan_id' =>$alasan_id,
			'keterangan_hapus' =>$keterangan_hapus,

		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data['deleted_ppa']=$login_ppa_id;
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_ic',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function verifikasi_ic(){
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$data=array(
			'st_verifikasi' => 1,
			'verifikasi_ppa' => $login_ppa_id,
			'verifikasi_date' =>date('Y-m-d H:i:s'),

		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_ic',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function list_index_template_ic()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.*,M.nama as nama_mppa
						 FROM tpoliklinik_ic H 
						 LEFT JOIN mppa M ON M.id=H.created_ppa
						 WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.assesmen_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		// $result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen('.$r->assesmen_id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->assesmen_id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  type="button" title="Print Template" '.$btn_disabel_edit.' class="btn btn-success btn-xs "><i class="fa fa-print"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->assesmen_id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		// $result[] = $aksi;
		// $result[] = ($r->nama_template);
		// $result[] = ($r->jumlah_template);
		// $data[] = $result;
		$jml_kolom=0;
			$subjectif='';$objectif='';$assemen='';$planing='';$intruksi='';$intervensi='';$evaluasi='';$reassesmen='';
			if ($r->subjectif){$jml_kolom=$jml_kolom+1;}
			if ($r->objectif){$jml_kolom=$jml_kolom+1;}
			if ($r->assemen){$jml_kolom=$jml_kolom+1;}
			if ($r->planing){$jml_kolom=$jml_kolom+1;}
			if ($r->intruksi){$jml_kolom=$jml_kolom+1;}
			if ($r->intervensi){$jml_kolom=$jml_kolom+1;}
			if ($r->evaluasi){$jml_kolom=$jml_kolom+1;}
			if ($r->reassesmen){$jml_kolom=$jml_kolom+1;}
			$lebar_kolom=100/$jml_kolom;
			if ($r->subjectif){
				$subjectif='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">SUBJECTIVE</div>
								<div class="push-5-t"> '.$r->subjectif.'</div>
							</td>';
				
			}
			if ($r->objectif){
				$objectif='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">OBJECTIVE</div>
								<div class="push-5-t"> '.$r->objectif.'</div>
							</td>';
				
			}
			if ($r->assemen){
				$assemen='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">SUBJECTIVE</div>
								<div class="push-5-t"> '.$r->assemen.'</div>
							</td>';
				
			}
			if ($r->planing){
				$planing='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">PLANING</div>
								<div class="push-5-t"> '.$r->planing.'</div>
							</td>';
				
			}
			if ($r->intruksi){
				$intruksi='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">INTRUCTIONS</div>
								<div class="push-5-t"> '.$r->intruksi.'</div>
							</td>';
				
			}
			if ($r->intervensi){
				$intervensi='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">INTERVENTION</div>
								<div class="push-5-t"> '.$r->intervensi.'</div>
							</td>';
				
			}
			if ($r->evaluasi){
				$evaluasi='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">EVALUATION</div>
								<div class="push-5-t"> '.$r->evaluasi.'</div>
							</td>';
				
			}
			if ($r->reassesmen){
				$reassesmen='<td class="bg-white" style="width: '.$lebar_kolom.'%;">
								<div class="h5 text-primary">RE-ASSESMENT</div>
								<div class="push-5-t"> '.$r->reassesmen.'</div>
							</td>';
				
			}
			
			$tabel='<table class="table table-condensed" style="border-spacing:0;border-collapse: collapse!important;">
						<tbody>
							<tr>
								<td colspan="'.$jml_kolom.'" class="bg-white" style="width: 100%;">
									<div class="h5"><strong>'.strtoupper($r->nama_template).'</strong></div>
									<div class="push-5-t"> | Add On '.tanggal_indo_DMY($r->tanggal_input).' at '.HumanTimeShort($r->tanggal_input).' Oleh '.$r->nama_mppa.' &nbsp;&nbsp;&nbsp;'.$aksi.'</div>
								</td>
							</tr>
							<tr>
								'.$subjectif.'								
								'.$objectif.'								
								'.$assemen.'								
								'.$planing.'								
								'.$intruksi.'								
								'.$intervensi.'								
								'.$evaluasi.'								
								'.$reassesmen.'								
							</tr>
						</tbody>
					</table>';
			 $result[] =$tabel;
			$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }	
  function create_template_ic(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $login_profesi_id=$this->session->userdata('login_profesi_id');
	  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
	  $data=array(
		'tanggal_input' => $tanggal_input,
		'profesi_id' => $login_profesi_id,
		'pendaftaran_id' => $pendaftaran_id,
		'idpasien' => $idpasien,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 3,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_ic',$data);
	  $this->output->set_output(json_encode($hasil));
    }
	
	function edit_template_ic(){
	  $assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'status_assemen' => 3,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'idpasien' => $this->input->post('idpasien'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_ic',$data);
		
		$this->output->set_output(json_encode($result));
  }
  function load_index_informasi_ic(){
	  $assesmen_id=$this->input->post('assesmen_id');
	  $ttd_dokter_pelaksana=$this->input->post('ttd_dokter_pelaksana');
	  $ttd_penerima_info=$this->input->post('ttd_penerima_info');
	  $q=" SELECT 
		ttd_dokter_ina,ttd_dokter_eng,ket_ttd_dokter_ina,ket_ttd_dokter_eng,ttd_pasien_ina,ttd_pasien_eng,ket_ttd_pasien_ina,ket_ttd_pasien_eng FROM setting_ic_keterangan

	  ";
	  $row_ttd=$this->db->query($q)->row_array();
	  $q="select *FROM tpoliklinik_ic_informasi WHERE assesmen_id='$assesmen_id'";
	  $list_info=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_info as $r){
		  // style="text-align: center;"
		  $btn_paraf='';
		  if ($r->paraf){
		  $btn_paraf='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$r->paraf.'" alt="">
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_faraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_paraf('.$r->id.')" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
			</div>
		  ';
		  }else{
			  $btn_paraf .= '<button onclick="modal_faraf('.$r->id.')"  type="button" data-toggle="tooltip" title="Paraf" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i></button>';
		  }
		  // if ($r->paraf){
		   // $btn_paraf="<img style='display:block; width:100px;height:100px;text-align: center;' class='text-center' src='".$r->paraf."' />";
		  // $btn_paraf .= '<br><div class="btn-group btn-group-sm " role="group"><button  type="button" onclick="hapus_paraf('.$r->id.')"  type="button" data-toggle="tooltip" title="Hapus Tanda tangan" class="btn btn-danger btn-xs btn_ttd"><i class="fa fa-trash"></i></button>';
			  
		  // }
		  // $btn_paraf .= '<button onclick="modal_faraf('.$r->id.')"  type="button" data-toggle="tooltip" title="Paraf" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i></button>';
		  // $btn_paraf .= '</div>';
		  $tabel .='<tr>';
			$tabel .='<td class="text-center"><input type="hidden" class="informasi_id" value="'.$r->id.'">'.$r->nourut.'</td>';
			$tabel .='<td class="text-center"><strong>'.$r->nama.'</strong><br><i>'.$r->nama_english.'</i></td>';
			$tabel .='<td><textarea class="form-control auto_blur_tabel " name="story" rows="3" style="width:100%">'.$r->isi_informasi.'</textarea></td>';
			$tabel .='<td class="text-center">'.$btn_paraf.'</td>';
		  $tabel .='</tr>';
	  } 
	  $btn_ttd_dokter='';
	  if ($ttd_dokter_pelaksana){
		  $btn_ttd_dokter='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_dokter_pelaksana.'" alt="">
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_dokter()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
			</div>
		  ';
	  }else{
		  $btn_ttd_dokter='<button class="btn btn-sm btn-success" onclick="modal_ttd_dokter()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $btn_ttd_kel='';
	  if ($ttd_penerima_info){
		  $btn_ttd_kel='
			<div class="img-container fx-img-rotate-r ">
				<img class="img-responsive" src="'.$ttd_penerima_info.'" alt="">
				<div class="img-options">
					<div class="img-options-content">
						<div class="btn-group btn-group-sm">
							<a class="btn btn-default" onclick="modal_ttd_kel()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
							<a class="btn btn-default btn-danger" onclick="hapus_ttd_kel()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
						</div>
					</div>
				</div>
			</div>
		  ';
	  }else{
		  $btn_ttd_kel='<button class="btn btn-sm btn-success" onclick="modal_ttd_kel()" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>';
	  }
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_dokter_ina'].'<br><i>'.$row_ttd['ket_ttd_dokter_eng'].'</i></td>';
	  $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_dokter_ina'].'/'.$row_ttd['ttd_dokter_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_dokter.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $tabel .='<tr>';
	  $tabel .='<td colspan="3">'.$row_ttd['ket_ttd_pasien_ina'].'<br><i>'.$row_ttd['ket_ttd_pasien_eng'].'</i></td>';
	   $tabel .='<td>
				<div class=" text-center"><strong>'.$row_ttd['ttd_pasien_ina'].'/'.$row_ttd['ttd_pasien_eng'].'</strong></div>
				<div class=" text-muted text-uppercase push-5-t text-center">'.$btn_ttd_kel.'</div>
	  </td>';
	  $tabel .='</tr>';
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function simpan_paraf_info_ic(){
		
		$id = $this->input->post('informasi_id');
		$paraf = $this->input->post('signature64');
		$data=array(
			'paraf' =>$paraf,
		);
		// print_r($id);
		$this->db->where('id',$id);
		$hasil=$this->db->update('tpoliklinik_ic_informasi',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_ttd_dokter_ic(){
		
		$assesmen_id = $this->input->post('assesmen_id');
		$ttd_dokter_pelaksana = $this->input->post('signature64');
		$data=array(
			'ttd_dokter_pelaksana' =>$ttd_dokter_pelaksana,
		);
		// print_r($id);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update('tpoliklinik_ic',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_ttd_kel_ic(){
		
		$assesmen_id = $this->input->post('assesmen_id');
		$ttd_penerima_info = $this->input->post('signature64');
		$data=array(
			'ttd_penerima_info' =>$ttd_penerima_info,
		);
		// print_r($id);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update('tpoliklinik_ic',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_ttd_pernyataan_ic(){
		
		$assesmen_id = $this->input->post('assesmen_id');
		$ttd_menyatakan = $this->input->post('signature64');
		$data=array(
			'ttd_menyatakan' =>$ttd_menyatakan,
		);
		// print_r($id);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update('tpoliklinik_ic',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_ttd_saksi_kel_ic(){
		
		$assesmen_id = $this->input->post('assesmen_id');
		$ttd_saksi = $this->input->post('signature64');
		$saksi_keluarga = $this->input->post('saksi_keluarga');
		$data=array(
			'ttd_saksi' =>$ttd_saksi,
			'saksi_keluarga' =>$saksi_keluarga,
		);
		// print_r($id);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update('tpoliklinik_ic',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_ttd_saksi_rs_ic(){
		
		$assesmen_id = $this->input->post('assesmen_id');
		$ttd_saksi_rs = $this->input->post('signature64');
		$saksi_rs = $this->input->post('saksi_rs');
		$data=array(
			'ttd_saksi_rs' =>$ttd_saksi_rs,
			'saksi_rs' =>$saksi_rs,
		);
		// print_r($id);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update('tpoliklinik_ic',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function update_isi_informasi_ic(){
		$assesmen_id=$this->input->post('assesmen_id');
		$id=$this->input->post('informasi_id');
		$isi_informasi=$this->input->post('isi');
		
		
		$data=array(
			'isi_informasi' => $isi_informasi,
			);
		$this->db->where('id',$id);
		$result=$this->db->update('tpoliklinik_ic_informasi',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		
		$this->output->set_output(json_encode($hasil));
	}
}	
