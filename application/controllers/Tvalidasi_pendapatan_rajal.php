<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tvalidasi_pendapatan_rajal extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('tvalidasi_pendapatan_rajal_model','model');
		$this->load->model('Tkasir_model');
		$this->load->model('Tpoliklinik_verifikasi_model');
		$this->load->model('Tverifikasi_transaksi_model');
		$this->load->model('Tpasien_penjualan_model');
	}

	function index() {
		// $this->count_null(8);
		$data=array(
			'st_posting'=>'#',
			'tipe_trx'=>'#',
			'nojurnal'=>'',
			'tipe_trx'=>'',
			'tanggal_trx1'=>date('d-m-Y'),
			'tanggal_trx2'=>date('d-m-Y'),
		);
		$data['error'] 			= '';
		// $data['list_rekanan'] 	= $this->model->list_rekanan();
		// print_r($data['list_rekanan']);exit();
		// $data['list_kp'] 	= $this->model->list_kp();
		$data['title'] 			= 'Validasi Jurnal Pendapatan Rawat Jalan';
		$data['content'] 		= 'Tvalidasi_pendapatan_rajal/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Pendapatan Rawat Jalan",'tvalidasi_pendapatan_rajal/index'),
									array("List",'#')
								);

		// print_r($data);exit();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function detail($id,$disabel='') {
		$data=$this->model->getHeader($id);
		$tanggal=YMDFormat($data['tanggal_transaksi']);
		$data['jml_sisa_verifikasi'] 			= $this->model->jml_sisa_verifikasi($tanggal);
		// print_r($data);exit();
		$data['error'] 			= '';
		$data['disabel'] 			= $disabel;
		$data['title'] 			= 'Detail Transaksi';
		$data['content'] 		= 'Tvalidasi_pendapatan_rajal/detail';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Pendapatan Rawat Jalan",'tvalidasi_pendapatan_rajal/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function detail_info($id,$disabel='') {
		$data=$this->model->getHeader($id);
		$tanggal=YMDFormat($data['tanggal_transaksi']);
		// $data['jml_sisa_verifikasi'] 			= $this->model->jml_sisa_verifikasi($tanggal);
		// print_r($data);exit();
		$data['error'] 			= '';
		$data['disabel'] 			= $disabel;
		$data['list_null'] = $this->list_null_info($id);
		// print_r($data['list_null']);exit();
		$data['list_group'] = $this->model->list_group();
		$data['list_akun'] = $this->model->list_akun();
		$data['title'] 			= 'Detail Transaksi';
		$data['content'] 		= 'Tvalidasi_pendapatan_rajal/detail_info';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Validasi Pendapatan Rawat Jalan",'tvalidasi_pendapatan_rajal/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	public function updateDetail()
	{
		$idhead=$this->input->post('idhead');
		$idgroup_gabungan=($this->input->post('idgroup_gabungan')=='#'?'0':$this->input->post('idgroup_gabungan'));
		$idgroup_diskon=($this->input->post('idgroup_diskon')=='#'?'0':$this->input->post('idgroup_diskon'));
		$idgroup_round=($this->input->post('idgroup_round')=='#'?'0':$this->input->post('idgroup_round'));
		// $idgroup_diskon=$this->input->post('idgroup_diskon');
		$data=array(
			'idgroup_diskon' => $idgroup_diskon,
			'idakun_diskon' => $this->GetNoAkunGroup($idgroup_diskon),	
			'idgroup_gabungan' => $idgroup_gabungan,
			'idakun_gabungan' => $this->GetNoAkunGroup($idgroup_gabungan),	
			'idgroup_round' => $idgroup_round,
			'idakun_round' => $this->GetNoAkunGroup($idgroup_round),	
		);
		$this->db->where('id',$idhead);
		// $this->db->update('id',$idhead);
		if ($this->db->update('tvalidasi_pendapatan_rajal_detail',$data)) {
			$output = [
				'status' => true
			];
		} else {
			$output = [
				'status' => false
			];
		}

		echo json_encode($output);
	}

	function detail_rajal($idvalidasi,$idhead,$disabel='') {
		
		$head = $this->model->get_head($idhead);
		// print_r($head);exit();
		$idpendaftaran=$head->pendaftaran_id;
		$idkasir=$head->kasir_id;
		$data = array(
			'idgroup_diskon' => null,
			'id' => $idpendaftaran,
			'idkasir' => $idkasir,
			'disabel' => '',
			'idvalidasi' => $idvalidasi,
			'idhead' => $idhead,
			'diskon_rp' => 0,
			'diskon_persen' => 0,
			'bayar_rp' => 0,
			'sisa_rp' => 0,
		  );
		
		// print_r($head);exit();
		$data['nominal_round']=$head->nominal_round;
		$data['nominal_diskon']=$head->nominal_diskon;
		$data['nominal_gabungan']=$head->nominal_gabungan;
		$data['idgroup_round']=$head->idgroup_round;
		$data['idgroup_diskon']=$head->idgroup_diskon;
		$data['idgroup_gabungan']=$head->idgroup_gabungan;
		$data['st_gabungan']=$head->st_gabungan;
		$data['list_group'] = $this->model->list_group();
        $data_header = $this->Tkasir_model->get_header_poli($idpendaftaran);
        $data['list_group_null'] = $this->model->list_group_null($idvalidasi,$idhead);
		
        if ($data_header) {
            $data['nopendaftaran'] = $data_header->nopendaftaran;
            $data['nomedrec'] = $data_header->no_medrec;
            $data['namapasien'] = $data_header->nama;
            $data['idtipepasien'] = $data_header->idtipepasien;
            $data['tipependaftaran'] = ($data_header->idtipe == '1' ? 'Poliklinik' : 'Instalasi Gawat Darurat');
            $data['poliklinik'] = $data_header->poli;
            $data['idkelompokpasien'] = $data_header->idkelompokpasien;
            $data['kelompokpasien'] = $data_header->kelompok;
            $data['bpjs_kesehatan'] = $data_header->tarif_bpjs_kes;
            $data['bpjs_ketenagakerjaan'] = $data_header->tarif_bpjs_ket;
            $data['namarekanan'] = $data_header->rekanan;
            $data['usertransaksi'] =$head->nama_user;
            $data['tanggaltransaksi'] = $data_header->tanggaldaftar;
            $data['namadokter'] = $data_header->dokter;
        }

        $data_header = $this->Tkasir_model->get_kasir($idkasir);
        if ($data_header) {
            $data['diskon_rp'] = $data_header->diskon;
            $data['bayar_rp'] = $data_header->total_bayar;
        }

        $data['error']    = '';
        $data['title'] = 'Detail Transaksi';
        $data['content'] = 'Tvalidasi_pendapatan_rajal/detail_rajal';
        $data['breadcrum'] = array(
          array("RSKB Halmahera", '#'),
          array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_rajal/index'),
          array("Detail Transaksi", '#')
      );
	  $data['detail_pembayaran'] = $this->model->list_pembayaran($idhead);

      
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
	}
	function detail_refund_rajal($idhead,$idvalidasi,$disabel='') {
		$data = $this->model->get_head_refund($idhead);
		// print_r($data);exit();
		// $data = array(
			
			// 'idhead' => $idvalidasi,
			
		  // );
		
		$data['idhead'] = $idhead;
		$data['list_group'] = $this->model->list_group();
        // $data_header = $this->Tkasir_model->get_header_poli($idpendaftaran);
        $data['list_group_null'] = $this->model->list_group_null($idvalidasi,$idhead);
		
      

        $data['error']    = '';
        $data['title'] = 'Detail Transaksi Refund';
        $data['content'] = 'Tvalidasi_pendapatan_rajal/detail_refund';
        $data['breadcrum'] = array(
          array("RSKB Halmahera", '#'),
          array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_rajal/index'),
          array("Detail Transaksi", '#')
      );
	  $data['detail_pembayaran'] = $this->model->list_pembayaran($idhead);

      
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
	}
	public function detail_obat_bebas($idpendaftaran,$idkasir,$idvalidasi,$idhead,$disabel='')
	{
		$data = [
			'id' => $idpendaftaran,
			'idkasir' => $idkasir,
			'idhead' => $idhead,
			'idvalidasi' => $idvalidasi,
			'stedit' => 1,
			'diskon_rp' => 0,
			'diskon_persen' => 0,
			'bayar_rp' => 0,
			'sisa_rp' => 0,
		];

		$data_header = $this->Tpasien_penjualan_model->get_dataPenjualan($idpendaftaran);
		if ($data_header) {
			$data['tanggaltransaksi'] = $data_header->tanggal;
			$data['notransaksi'] = $data_header->nopenjualan;
			$data['nomedrec'] = ($data_header->nomedrec ? $data_header->nomedrec : '-');
			$data['namapasien'] = $data_header->nama;

			if ($data_header->idkategori == 1) {
				$data['kategori'] = 'Pasien RS';
			} elseif ($data_header->idkategori == 2) {
				$data['kategori'] = 'Pegawai';
			} elseif ($data_header->idkategori == 3) {
				$data['kategori'] = 'Dokter';
			}

			if ($data_header->statusresep == 0) {
				$data['statusresep'] = 'Tidak Ada';
			} elseif ($data_header->statusresep == 1) {
				$data['statusresep'] = 'Ada';
			}

			$data_header = $this->Tkasir_model->get_kasir($idkasir);
			if ($data_header) {
				$data['diskon_rp'] = $data_header->diskon;
				$data['bayar_rp'] = $data_header->total_bayar;
			}
		}

		$data['error'] = '';
		$data['title'] = 'Detail Transaksi';
		$data['content'] = 'Tvalidasi_pendapatan_rajal/detail_obat_bebas';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Verifikasi Transaksi', 'tvalidasi_pendapatan_rajal/index'],
			['Detail Transaksi', '#']
		];

		$data['list_bank'] = $this->Tkasir_model->getBank();
		 $data['detail_pembayaran'] = $this->model->list_pembayaran($idhead);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function edit_tindakan($idhead,$idetail) {
		$data = $this->model->get_edit_detail($idhead);
		$data2 = $this->model->get_edit_detail2($idetail);
		// print_r($data2);exit();
		// print_r($data2);exit();
		$data['list_group'] = $this->model->list_group();
        $data['disabel']    = $this->model->GetStatusPosting($data['idvalidasi']);
        $data['idjenis']    = '';
        $data['error']    = '';
        $data['st_posting']    = '';
        $data['title'] = 'Detail Transaksi';
        $data['content'] = 'Tvalidasi_pendapatan_rajal/edit_rajal_tindakan';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_rajal/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,$data2,backend_info());
        $this->parser->parse('module_template', $data);
	}
	function edit_obat($idhead,$idtipe,$disabel='') {
		$data = $this->model->get_edit_detail_obat($idhead);
		// print_r($data);exit();
		$data['list_group'] = $this->model->list_group();
		$data['list_obat'] = $this->model->list_obat($idhead,$idtipe);
        $data['idhead']    = $idhead;
        // $data['disabel']    = $disabel;
		 $data['disabel']    = $this->model->GetStatusPosting($data['idvalidasi']);
        $data['error']    = '';
        $data['idtipe']    = $idtipe;
        $data['st_posting']    = '';
        $data['title'] = 'Detail Transaksi';
        $data['content'] = 'Tvalidasi_pendapatan_rajal/edit_rajal_obat';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_rajal/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,backend_info());
		// print_r($data);
        $this->parser->parse('module_template', $data);
	}
	function edit_farmasi_nr($idhead,$idtipe,$form='poli',$disabel='') {
		$data = $this->model->get_edit_detail_obat($idhead);
		// print_r($data);exit();
		$data['list_group'] = $this->model->list_group();
		$data['list_obat'] = $this->model->list_obat_farmasi($idhead,$idtipe);
        $data['idhead']    = $idhead;
        // $data['disabel']    = $disabel;
		 $data['disabel']    = $this->model->GetStatusPosting($data['idvalidasi']);
        $data['form']    = $form;
        $data['error']    = '';
        $data['idtipe']    = $idtipe;
        $data['st_posting']    = '';
        $data['title'] = 'Detail Transaksi Farmasi';
        $data['content'] = 'Tvalidasi_pendapatan_rajal/edit_rajal_farmasi_nr';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_rajal/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,backend_info());
		// print_r($data);
        $this->parser->parse('module_template', $data);
	}
	function edit_farmasi_refund($idhead,$idtipe,$disabel='') {
		$data = $this->model->get_head_refund($idhead);
		// print_r($data);exit();
		$data['list_group'] = $this->model->list_group();
		$data['list_obat'] = $this->model->list_obat_farmasi($idhead,$idtipe);
        $data['idhead']    = $idhead;
        // $data['disabel']    = $disabel;
		 $data['disabel']    = $this->model->GetStatusPosting($data['idvalidasi']);
        $data['error']    = '';
        $data['idtipe']    = $idtipe;
        $data['st_posting']    = '';
        $data['title'] = 'Detail Transaksi Farmasi';
        $data['content'] = 'Tvalidasi_pendapatan_rajal/edit_rajal_farmasi_refund';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_rajal/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,backend_info());
		// print_r($data);
        $this->parser->parse('module_template', $data);
	}
	function edit_rajal_pembayaran($idhead,$ref_id,$form='poli',$disabel='') {
		$data = $this->model->get_edit_detail_obat($idhead);
		// print_r($data);exit();
		$data['list_group'] = $this->model->list_group();
		$data['list_akun'] = $this->model->list_akun();
		// $data['list_akun'] = $this->model->list_akun();
		$data['list_tunai'] = $this->model->list_tunai($idhead);
		$data['list_non_tunai'] = $this->model->list_non_tunai($idhead);
        $data['list_obat']    = array();
        $data['idhead']    = $idhead;
        // $data['disabel']    = $disabel;
		 $data['disabel']    = $this->model->GetStatusPosting($data['idvalidasi']);
        $data['form']    = $form;
        $data['error']    = '';
        $data['ref_id']    = $ref_id;
        $data['st_posting']    = '';
        $data['title'] = 'Detail Transaksi';
        $data['content'] = 'Tvalidasi_pendapatan_rajal/edit_rajal_pembayaran';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_rajal/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,backend_info());
		// print_r($data);
        $this->parser->parse('module_template', $data);
	}
	function edit_other($id) {
	
		$data['list_group'] = $this->model->list_group();
		$data['list_akun'] = $this->model->list_akun();
		// $data['list_akun'] = $this->model->list_akun();
		$data['list_tunai'] = $this->model->list_tunai($idhead);
		$data['list_non_tunai'] = $this->model->list_non_tunai($idhead);
        $data['list_obat']    = array();
        $data['idhead']    = $idhead;
        // $data['disabel']    = $disabel;
		 $data['disabel']    = $this->model->GetStatusPosting($id);
        $data['form']    = $form;
        $data['error']    = '';
        $data['ref_id']    = $ref_id;
        $data['st_posting']    = '';
        $data['title'] = ' Transaksi Other';
        $data['content'] = 'Tvalidasi_pendapatan_rajal/edit_rajal_other';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_rajal/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,backend_info());
		// print_r($data);
        $this->parser->parse('module_template', $data);
	}
	function edit_rajal_pembayaran_refund($idhead,$ref_id,$disabel='') {
		$data = $this->model->get_edit_detail_obat($idhead);
		// print_r($data);exit();
		$data['list_akun'] = $this->model->list_akun();
		$data['list_tunai'] = $this->model->list_tunai($idhead);
        $data['list_obat']    = array();
        $data['idhead']    = $idhead;
        // $data['disabel']    = $disabel;
		 $data['disabel']    = $this->model->GetStatusPosting($data['idvalidasi']);
        $data['error']    = '';
        $data['ref_id']    = $ref_id;
        $data['st_posting']    = '';
        $data['title'] = 'Detail Transaksi';
        $data['content'] = 'Tvalidasi_pendapatan_rajal/edit_rajal_pembayaran_refund';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_rajal/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,backend_info());
		// print_r($data);
        $this->parser->parse('module_template', $data);
	}
	function edit_farmasi_racikan($idhead,$iddetail,$form='poli',$disabel='') {
		
		$head=$this->model->get_head_racikan($idhead,$iddetail);
		$data = $this->model->get_edit_detail_obat($idhead);
		// print_r($data);exit();
		$data['idracikan'] = $head->idracikan;
		$data['nama_racikan'] = $head->namabarang;
		$data['totalkeseluruhan'] = $head->totalkeseluruhan;
		$data['tuslah'] = $head->tuslah;
		$data['idgroup_tuslah'] = $head->idgroup_tuslah;
		$data['list_obat'] = $this->model->list_obat_farmasi_racikan($idhead,$data['idracikan']);
		// print_r($data['list_obat']);exit();
		$data['list_group'] = $this->model->list_group();
        $data['idhead']    = $idhead;
        $data['iddetail']    = $iddetail;
        $data['form']    = $form;
        // $data['disabel']    = $disabel;
		 $data['disabel']    = $this->model->GetStatusPosting($data['idvalidasi']);
        $data['error']    = '';
        $data['st_posting']    = '';
        $data['title'] = 'Detail Transaksi Farmasi Racikan';
        $data['content'] = 'Tvalidasi_pendapatan_rajal/edit_rajal_farmasi_racikan';
        $data['breadcrum'] = array(
		array("RSKB Halmahera", '#'),
		array("Verifikasi Transaksi", 'Tvalidasi_pendapatan_rajal/index'),
		array("Detail Transaksi", '#'));
      


        $data = array_merge($data,backend_info());
		// print_r($data);
        $this->parser->parse('module_template', $data);
	}
	function load_detail(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        $userid = $this->session->userdata('user_id');
		// $batas_batal=$this->db->query("SELECT batas_batal FROM msetting_jurnal_klaim WHERE id='1'")->row('batas_batal');
		
		$id=$this->input->post('id');
		$disabel=$this->input->post('disabel');

		
		$tanggal_pendaftaran_awal=$this->input->post('tanggal_pendaftaran_awal');
		$tanggal_pendaftaran_akhir=$this->input->post('tanggal_pendaftaran_akhir');
		$idtipe=$this->input->post('idtipe');
		$idpoliklinik=$this->input->post('idpoliklinik');
		$nomedrec=$this->input->post('nomedrec');
		$namapasien=$this->input->post('namapasien');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$iduser=$this->input->post('iduser');
		$nopendaftaran=$this->input->post('nopendaftaran');
		$nokasir=$this->input->post('nokasir');
		// $st_posting=$this->input->post('st_posting');
		$where='';
		
		if ($nopendaftaran !=''){
			$where .=" AND H.notransaksi='$nopendaftaran' ";
		}
		if ($idtipe !='#'){
			$where .=" AND H.idtipe='$idtipe' ";
		}
		if ($idrekanan !='#'){
			$where .=" AND H.idrekanan='$idrekanan' ";
		}
		if ($iduser !='#'){
			$where .=" AND H.userid_trx='$iduser' ";
		}
		
		if ($idpoliklinik !='#'){
			$where .=" AND H.idpoliklinik='$idpoliklinik' ";
		}
		if ($nomedrec !=''){
			$where .=" AND H.no_medrec LIKE '%".$nomedrec."%' ";
		}
		if ($namapasien !=''){
			$where .=" AND H.namapasien LIKE '%".$namapasien."%' ";
		}
		
		
		if ('' != $tanggal_pendaftaran_awal) {
            $where .= " AND DATE(H.tanggal_pendaftaran) >='".YMDFormat($tanggal_pendaftaran_awal)."' AND DATE(H.tanggal_pendaftaran) <='".YMDFormat($tanggal_pendaftaran_akhir)."'";
        }
        $from = "(
					SELECT H.*,U.`name` as nama_user_trx,K.edited_nama,K.edited_date FROM tvalidasi_pendapatan_rajal_detail H
					LEFT JOIN tkasir K ON K.id=H.kasir_id
					LEFT JOIN musers U ON U.id=H.userid_trx
					WHERE H.idvalidasi='$id' ".$where."
				) as tbl";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('notransaksi','namapasien','no_medrec');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

			
        foreach ($list as $r) {
            $no++;
            $row = array();
			$disabel_btn='';
			
            $aksi       = '<div class="btn-group">';
            
           
            $row[] = HumanDateShort($r->tanggal_pendaftaran);
            $row[] = $r->notransaksi;
            $row[] = GetTipeTransaksiDetail($r->idtipe,$r->tipe_refund);
            $row[] = $r->no_medrec;
            $row[] = $r->namapasien;
            $row[] = number_format($r->nominal,2);
            $row[] = $r->edited_nama.'<br>'.$r->edited_date;
            $row[] = $r->user_verif.'<br>'.$r->date_verif;
           
			if ($r->idtipe=='3'){//OB
				$aksi .='<a href="'.site_url().'tverifikasi_transaksi/edit_transaksi_obatbebas/'.$r->pendaftaran_id.'/'.$r->kasir_id.'/0" target="_blank" title="Transaksi Obat Bebas" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
				$aksi .='<a href="'.site_url().'tkasir/print_transaksi/N/'.$r->kasir_id.'/0" target="_blank" title="Transaksi Obat Bebas" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
				$aksi .= '<a href="'.base_url().'tvalidasi_pendapatan_rajal/detail_obat_bebas/'.$r->pendaftaran_id.'/'.$r->kasir_id.'/'.$r->idvalidasi.'/'.$r->id.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
			}
			if ($r->idtipe=='1' || $r->idtipe=='2'){//POLI
				$aksi = '<a href="'.site_url().'tverifikasi_transaksi/edit_transaksi_rajal/'.$r->pendaftaran_id.'/'.$r->id.'/0" target="_blank" title="Transaksi Rawat Jalan" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
				$aksi .='<a href="'.site_url().'tkasir/kwitansi_verifikasi/'.$r->pendaftaran_id.'" target="_blank" title="Transaksi Obat Bebas" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
				$aksi .= '<a href="'.base_url().'tvalidasi_pendapatan_rajal/detail_rajal/'.$r->idvalidasi.'/'.$r->id.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
			}
			if ($r->idtipe=='4'){//Refund
				$aksi = '<a href="'.site_url().'trefund_kas/pembayaran/'.$r->pendaftaran_id.'/disabled" target="_blank" title="Transaksi Rawat Jalan" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
				if ($r->tipe_refund=='1'){						
				$aksi .='<a href="'.site_url().'Trefund/fakturRefundObat/'.$r->pendaftaran_id.'" target="_blank" title="Transaksi Obat Bebas" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
				}
				if ($r->tipe_refund=='2'){						
				$aksi .='<a href="'.site_url().'Trefund/fakturRefundTransaksi/'.$r->pendaftaran_id.'" target="_blank" title="Transaksi Obat Bebas" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
				}
				$aksi .= '<a href="'.base_url().'tvalidasi_pendapatan_rajal/detail_refund_rajal/'.$r->id.'/'.$r->idvalidasi.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
			}
			$jml_null=$this->count_detail_null($r->idvalidasi,$r->id);
			if ($jml_null > 0){
				$aksi .= '<button title="Null" class="btn btn-xs btn-danger" type="button">Ada Akun Null</button>';			
			}
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function get_index(){
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        $userid = $this->session->userdata('user_id');
		$batas_batal=$this->db->query("SELECT batas_batal_rajal FROM msetting_jurnal_pendapatan_general WHERE id='1'")->row('batas_batal_rajal');
		
		$nojurnal=$this->input->post('nojurnal');
		$tipe_trx=$this->input->post('tipe_trx');
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		$st_posting=$this->input->post('st_posting');
		$where='';
		
		
		
		if ($st_posting !='#'){
			$where .=" AND H.st_posting='$st_posting' ";
		}
		
		if ($nojurnal !=''){
			$where .=" AND H.nojurnal='$nojurnal' ";
		}
		if ($tipe_trx !='#'){
			$where .=" AND H.tipe_trx='$tipe_trx' ";
		}
		
		
		
		if ('' != $tanggal_trx1) {
            $where .= " AND DATE(H.tanggal_transaksi) >='".YMDFormat($tanggal_trx1)."' AND DATE(H.tanggal_transaksi) <='".YMDFormat($tanggal_trx2)."'";
        }else{
			
            $where .= " AND DATE(H.tanggal_transaksi) >='".date('Y-m-d')."' ";
		}
		
        // $from = "(
					// SELECT H.id,H.nojurnal,H.tipe_trx,H.tanggal_transaksi,H.total_transaksi,H.total_poli,H.total_igd,H.total_ob,H.total_refund
					// ,H.jml_trx,H.st_posting,IFNULL(DATEDIFF(NOW(), H.posting_date),0) as umur_posting
					// ,CASE WHEN tipe_trx='6' THEN 0 ELSE (SELECT COUNT(K.id) as jml FROM tkasir K
						// WHERE K.`status`='2' AND DATE(K.tanggal)=DATE(H.tanggal_transaksi) AND K.status_verifikasi='0') END as jml_sisa
					// FROM tvalidasi_pendapatan_rajal H 							
					// WHERE H.`status`='1'  ".$where."
					// GROUP BY H.id
				// ) as tbl";
				
		 $from = "(
					SELECT H.id,H.nojurnal,H.tipe_trx,H.tanggal_transaksi,H.total_transaksi,H.total_poli,H.total_igd,H.total_ob,H.total_refund
					,H.jml_trx,H.st_posting,IFNULL(DATEDIFF(NOW(), H.posting_date),0) as umur_posting
					,SUM(CASE WHEN H.tipe_trx != '6' AND K.status_verifikasi='0' THEN 1 ELSE 0 END) as jml_sisa
					
					FROM tvalidasi_pendapatan_rajal H 		
					LEFT JOIN tkasir K ON  K.`status`='2' AND DATE(K.tanggal)=DATE(H.tanggal_transaksi) 
					WHERE H.`status`='1'  ".$where."
					GROUP BY H.id
				) as tbl";
				
		
		// print_r($from);exit();
        $this->from   = $from;
		$this->select = array();
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nojurnal');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

			
        foreach ($list as $r) {
            $no++;
            $row = array();
			$disabel_btn='';
			$str_verifikasi='';
			
			if ($r->umur_posting>$batas_batal){
				$disabel_btn='disabled';
			}
            $aksi       = '<div class="btn-group">';
            $jml_null     = $this->count_null($r->id);
            $status     = '0';
            $status_retur     = '';
            $url_klaim        = site_url('tpendapatan/');
            $url       = site_url('tklaim_monitoring/');
            $url_validasi        = site_url('tvalidasi_pendapatan_rajal/');
            $url_batal       = site_url('tgudang_pengembalian/');
            $row[] = $r->id;
            $row[] = $r->st_posting;
            $row[] = $no;
            $row[] = $r->nojurnal;
            $row[] = HumanDateShort($r->tanggal_transaksi);
            $row[] = get_tipe_trx($r->tipe_trx);
            $row[] = $r->jml_trx;
            $row[] = number_format($r->total_igd,2);
            $row[] = number_format($r->total_poli,2);
            $row[] = number_format($r->total_ob,2);
            $row[] = number_format($r->total_refund,2);
            $row[] = number_format($r->total_transaksi,2);
			if ($r->jml_sisa > 0){
				$str_verifikasi=text_primary($r->jml_sisa.' TRX BELUM DIVERIFIKASI');
			}
            $row[] = ($r->st_posting=='1'?text_success('TELAH DIPOSTING'):text_default('MENUGGU DIPOSTING')).' <br> <br> '.$str_verifikasi;
					if ($r->tipe_trx=='6'){//Informasi Medis
						$aksi .= '<a href="'.base_url().'tvalidasi_pendapatan_rajal/detail_info/'.$r->id.'/disabled" target="_blank" title="Detail" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
						$aksi .= '<a href="'.base_url().'tvalidasi_pendapatan_rajal/detail_info/'.$r->id.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
					}else{
						$aksi .= '<a href="'.base_url().'tvalidasi_pendapatan_rajal/detail/'.$r->id.'/disabled" target="_blank" title="Detail" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
						$aksi .= '<a href="'.base_url().'tvalidasi_pendapatan_rajal/detail/'.$r->id.'" target="_blank" title="Detail" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a>';
					}
					
					if ($r->st_posting=='1'){
						$aksi .= '<button title="Batal Posting" '.$disabel_btn.' class="btn btn-xs btn-danger batalkan"><i class="fa fa-close"></i></button>';						
					}else{
						if ($r->jml_sisa==0){
							if ($jml_null > 0){
								$aksi .= '<button title="Null" class="btn btn-xs btn-danger">Ada Akun Null</button>';								
							}else{
								$aksi .= '<button title="Posting" class="btn btn-xs btn-success posting"><i class="si si-arrow-right"></i></button>';
								$status='1';
							}
						}						
					}
					// $aksi .= '<a href="'.$url_validasi.'print_kwitansi_deposit/'.$r->idtransaksi.'" target="_blank" title="Print Kwitansi" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';
					// $aksi .= '<button title="Print" class="btn btn-xs btn-success"><i class="fa fa-print"></i></button>';
			$aksi.='</div>';
            $row[] = $aksi;
            $row[] = $status;
            $data[] = $row;
        }
		$output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
		  echo json_encode($output);
        // $draw = null;
        // if (isset($_POST['draw'])) {
            // $draw = $_POST['draw'];
        // }

        // $output = array(
            // "draw" => $draw,
            // "recordsTotal" => $this->datatable->count_all(),
            // "recordsFiltered" => $this->datatable->count_all(),
            // "data" => $data
        // );
        // $this->output
        // ->set_content_type('application/json')
        // ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function count_null($id){
		$q="SELECT COUNT(*) as jml_null FROM ".$this->model->query_ringkasan($id)." T 
		WHERE T.nominal > 0 AND T.idakun IS NULL
		";
		return $this->db->query($q)->row('jml_null');
	}
	function count_detail_null($id,$idhead){
		$q="SELECT COUNT(*) as jml_null FROM ".$this->model->query_ringkasan($id)." T 
		WHERE T.nominal > 0 AND T.idakun IS NULL AND T.idhead='$idhead'
		";
		return $this->db->query($q)->row('jml_null');
	}
	function list_null_info($id){
		$q="SELECT * FROM ".$this->model->query_ringkasan($id)." T 
		WHERE T.nominal > 0 AND T.idakun IS NULL 
		";
		return $this->db->query($q)->result();
	}
	
	public function posting()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'1',
			'posting_by'=>$this->session->userdata('user_id'),
			'posting_nama'=>$this->session->userdata('user_name'),
            'posting_date'=>date('Y-m-d H:i:s')
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_pendapatan_rajal', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function update_other()
    {
        $id = $this->input->post('id');
        $idakun_debet = $this->input->post('idakun_debet');
        $idakun_kredit = $this->input->post('idakun_kredit');
        $data =array(
            'idakun_debet'=>$idakun_debet,
            'idakun_kredit'=>$idakun_kredit,
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_pendapatan_rajal_other', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function posting_all()
    {
        $id = $this->input->post('id');
		foreach($id as $index=>$val){
			 $data =array(
				'st_posting'=>'1',
				'posting_by'=>$this->session->userdata('user_id'),
				'posting_nama'=>$this->session->userdata('user_name'),
				'posting_date'=>date('Y-m-d H:i:s')
			);
			$this->db->where('id', $val);
			$result=$this->db->update('tvalidasi_pendapatan_rajal', $data);
		}
       
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	
	public function batalkan()
    {
        $id = $this->input->post('id');
        $data =array(
            'st_posting'=>'0',
        );
        $this->db->where('id', $id);
        $result=$this->db->update('tvalidasi_pendapatan_rajal', $data);
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function get_transaksi_detail($iddet)
    {
        $q="SELECT B.nama as nama_barang,D.* FROM `tgudang_penerimaan_detail` D
LEFT JOIN view_barang_all B ON B.id=D.idbarang AND B.idkelompokpasien=D.idkelompokpasien
WHERE D.id='$iddet'";
       
        $result=$this->db->query($q)->row_array();
        $this->output->set_output(json_encode($result));
    }
	function save_edit_rajal_tindakan(){
		$idhead=$this->input->post('idhead');
		$iddetail=$this->input->post('iddetail');
		$idjenis=$this->input->post('idjenis');
		$idvalidasi=$this->input->post('idvalidasi');
		$id=$this->input->post('id');
		// print_r(($this->input->post()));exit();
		// $id=$this->model->saveData();
		$data_detail=array(
			'group_jasasarana' =>$this->input->post('group_jasasarana'),
			'group_jasapelayanan' =>$this->input->post('group_jasapelayanan'),
			'group_bhp' =>$this->input->post('group_bhp'),
			'group_biayaperawatan' =>$this->input->post('group_biayaperawatan'),			
			'group_jasasarana_diskon' =>$this->input->post('group_jasasarana_diskon'),
			'group_jasapelayanan_diskon' =>$this->input->post('group_jasapelayanan_diskon'),
			'group_bhp_diskon' =>$this->input->post('group_bhp_diskon'),
			'group_biayaperawatan_diskon' =>$this->input->post('group_biayaperawatan_diskon'),
			'group_diskon_all' =>$this->input->post('group_diskon_all'),
			'idakun_jasasarana' =>$this->GetNoAkunGroup($this->input->post('group_jasasarana')),
			'idakun_jasapelayanan' =>$this->GetNoAkunGroup($this->input->post('group_jasapelayanan')),
			'idakun_bhp' =>$this->GetNoAkunGroup($this->input->post('group_bhp')),
			'idakun_biayaperawatan' =>$this->GetNoAkunGroup($this->input->post('group_biayaperawatan')),		
			'idakun_jasasarana_diskon' =>$this->GetNoAkunGroup($this->input->post('group_jasasarana_diskon')),
			'idakun_jasapelayanan_diskon' =>$this->GetNoAkunGroup($this->input->post('group_jasapelayanan_diskon')),
			'idakun_bhp_diskon' =>$this->GetNoAkunGroup($this->input->post('group_bhp_diskon')),
			'idakun_biayaperawatan_diskon' =>$this->GetNoAkunGroup($this->input->post('group_biayaperawatan_diskon')),
			'idakun_diskon_all' =>$this->GetNoAkunGroup($this->input->post('group_diskon_all')),
		);
		$this->db->where('id',$this->input->post('id'));
		if($this->db->update('tvalidasi_pendapatan_rajal_detail_tindakan',$data_detail)){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tvalidasi_pendapatan_rajal/edit_tindakan/'.$idhead.'/'.$id,'location');
		}
	
	}
	function save_edit_rajal_obat(){
		// print_r($this->GetNoAkunGroup(17));exit();
		$iddet_obat=$this->input->post('iddet_obat');
		$idgroup_penjualan=$this->input->post('idgroup_penjualan');
		$idgroup_diskon=$this->input->post('idgroup_diskon');
		$idhead=$this->input->post('idhead');
		$iddetail=$this->input->post('iddetail');
		$idjenis=$this->input->post('idjenis');
		// print_r(($this->input->post()));exit();
		// $id=$this->model->saveData();
		foreach($iddet_obat as $index => $val){
			$data_detail=array(
				'idgroup_penjualan' =>$idgroup_penjualan[$index],
				'idgroup_diskon' =>$idgroup_diskon[$index],
				'idakun_penjualan' =>$this->GetNoAkunGroup($idgroup_penjualan[$index]),
				'idakun_diskon' =>$this->GetNoAkunGroup($idgroup_diskon[$index]),				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_rajal_detail_tindakan_obat',$data_detail);
		}
	
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah disimpan.');
		redirect('tvalidasi_pendapatan_rajal/edit_obat/'.$idhead.'/'.$idjenis,'location');
		
	
	}
	function save_detail_info(){
		$idvalidasi=$this->input->post('id');
		
		$iddet_header=$this->input->post('iddet_header');
		$idgroup_diskon_all=$this->input->post('idgroup_diskon_all');
		$idgroup_round=$this->input->post('idgroup_round');
		foreach($iddet_header as $index => $val){
			$data_detail=array(
				'idgroup_diskon_all' =>$idgroup_diskon_all[$index],
				'idgroup_round' =>$idgroup_round[$index],
				'idakun_diskon_all' =>$this->GetNoAkunGroup($idgroup_diskon_all[$index]),
				'idakun_round' =>$this->GetNoAkunGroup($idgroup_round[$index]),				
			);
				// print_r($this->GetNoAkunGroup($idgroup_diskon_all[$index]));exit();
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_rajal_info_medis',$data_detail);
		}
		
		$iddet_detail=$this->input->post('iddet_detail');
		$idgroup_dokter=$this->input->post('idgroup_dokter');
		$idgroup_rs=$this->input->post('idgroup_rs');
		$idgroup_diskon=$this->input->post('idgroup_diskon');
		foreach($iddet_detail as $index => $val){
			$data_detail=array(
				'idgroup_dokter' =>$idgroup_dokter[$index],
				'idakun_dokter' =>$this->GetNoAkunGroup($idgroup_dokter[$index]),	
				'idgroup_rs' =>$idgroup_rs[$index],
				'idakun_rs' =>$this->GetNoAkunGroup($idgroup_rs[$index]),
				'idgroup_diskon' =>$idgroup_diskon[$index],
				'idakun_diskon' =>$this->GetNoAkunGroup($idgroup_diskon[$index]),				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_rajal_info_medis_detail',$data_detail);
		}
		$iddet_bayar=$this->input->post('iddet_bayar');
		$idakun=$this->input->post('idakun');
		
		foreach($iddet_bayar as $index => $val){
			$data_detail=array(
				'idakun' =>$idakun[$index],
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_rajal_info_medis_bayar',$data_detail);
		}
		
	
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah disimpan.');
		redirect('tvalidasi_pendapatan_rajal/detail_info/'.$idvalidasi,'location');
		
	
	}
	function save_edit_rajal_pembayaran(){
		// print_r($this->input->post());exit();
		$idhead=$this->input->post('idhead');
		$iddet_tunai=$this->input->post('iddet_tunai');
		$idakun=$this->input->post('idakun');
		$iddet_non_tunai=$this->input->post('iddet_non_tunai');
		$group_akun=$this->input->post('group_akun');
		$form=$this->input->post('form');
		
		if ($iddet_tunai){
			foreach($iddet_tunai as $index => $val){
				$data_detail=array(
					'idakun' =>$idakun[$index],
				);
				$this->db->where('id',$val);
				$this->db->update('tvalidasi_pendapatan_rajal_bayar',$data_detail);
			}
		}
		if ($iddet_non_tunai){
			foreach($iddet_non_tunai as $index => $val){
				$data_detail=array(
					'group_akun' =>$group_akun[$index],
					'idakun' =>$this->GetNoAkunGroup($group_akun[$index]),	
				);
				$this->db->where('id',$val);
				$this->db->update('tvalidasi_pendapatan_rajal_bayar_non_tunai',$data_detail);
			}
		}
		
	
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah disimpan.');
		redirect('tvalidasi_pendapatan_rajal/edit_rajal_pembayaran/'.$idhead.'/1/'.$form,'location');
		
	
	}
	function save_edit_rajal_pembayaran_refund(){
		// print_r($this->input->post());exit();
		$idhead=$this->input->post('idhead');
		$iddet_tunai=$this->input->post('iddet_tunai');
		$idakun=$this->input->post('idakun');
		
		if ($iddet_tunai){
			foreach($iddet_tunai as $index => $val){
				$data_detail=array(
					'idakun' =>$idakun[$index],
				);
				$this->db->where('id',$val);
				$this->db->update('tvalidasi_pendapatan_rajal_bayar',$data_detail);
			}
		}
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah disimpan.');
		redirect('tvalidasi_pendapatan_rajal/edit_rajal_pembayaran_refund/'.$idhead.'/1','location');
		
	
	}
	function save_edit_rajal_farmasi_nr(){
		// print_r($this->GetNoAkunGroup(17));exit();
		$iddet_obat=$this->input->post('iddet_obat');
		$idgroup_penjualan=$this->input->post('idgroup_penjualan');
		$idgroup_diskon=$this->input->post('idgroup_diskon');
		$idgroup_tuslah=$this->input->post('idgroup_tuslah');
		$idhead=$this->input->post('idhead');
		$iddetail=$this->input->post('iddetail');
		$idjenis=$this->input->post('idjenis');
		$form=$this->input->post('form');
		// print_r(($this->input->post()));exit();
		// $id=$this->model->saveData();
		foreach($iddet_obat as $index => $val){
			$data_detail=array(
				'idgroup_penjualan' =>$idgroup_penjualan[$index],
				'idgroup_diskon' =>$idgroup_diskon[$index],
				'idgroup_tuslah' =>$idgroup_tuslah[$index],
				'idakun_penjualan' =>$this->GetNoAkunGroup($idgroup_penjualan[$index]),
				'idakun_diskon' =>$this->GetNoAkunGroup($idgroup_diskon[$index]),				
				'idakun_tuslah' =>$this->GetNoAkunGroup($idgroup_tuslah[$index]),				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan',$data_detail);
		}
	
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah disimpan.');
		redirect('tvalidasi_pendapatan_rajal/edit_farmasi_nr/'.$idhead.'/'.$idjenis.'/'.$form,'location');			
		
	
	}
	function save_edit_rajal_farmasi_refund(){
		// print_r($this->GetNoAkunGroup(17));exit();
		$iddet_obat=$this->input->post('iddet_obat');
		$idgroup_penjualan=$this->input->post('idgroup_penjualan');
		$idgroup_diskon=$this->input->post('idgroup_diskon');
		$idgroup_tuslah=$this->input->post('idgroup_tuslah');
		$idhead=$this->input->post('idhead');
		$iddetail=$this->input->post('iddetail');
		$idtipe=$this->input->post('idtipe');
		// print_r(($this->input->post()));exit();
		// $id=$this->model->saveData();
		foreach($iddet_obat as $index => $val){
			$data_detail=array(
				'idgroup_penjualan' =>$idgroup_penjualan[$index],
				'idgroup_diskon' =>$idgroup_diskon[$index],
				'idgroup_tuslah' =>$idgroup_tuslah[$index],
				'idakun_penjualan' =>$this->GetNoAkunGroup($idgroup_penjualan[$index]),
				'idakun_diskon' =>$this->GetNoAkunGroup($idgroup_diskon[$index]),				
				'idakun_tuslah' =>$this->GetNoAkunGroup($idgroup_tuslah[$index]),				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan',$data_detail);
		}
	
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah disimpan.');
		redirect('tvalidasi_pendapatan_rajal/edit_farmasi_refund/'.$idhead.'/'.$idtipe,'location');
		
	
	}
	function save_edit_rajal_farmasi_racikan(){
		// print_r($this->GetNoAkunGroup(17));exit();
		$iddet_obat=$this->input->post('iddet_obat');
		$idgroup_penjualan=$this->input->post('idgroup_penjualan');
		$idgroup_diskon=$this->input->post('idgroup_diskon');
		$idhead=$this->input->post('idhead');
		$iddetail=$this->input->post('iddetail');
		$idracikan=$this->input->post('idracikan');
		$idgroup_tuslah=$this->input->post('idgroup_tuslah');
		$form=$this->input->post('form');
		
		$data_head=array(
			'idgroup_tuslah' => $idgroup_tuslah,
			'idakun_tuslah' => $this->GetNoAkunGroup($idgroup_tuslah),
		);
		$this->db->where('id',$idracikan);
		$this->db->update('tvalidasi_pendapatan_rajal_detail_farmasi_racikan',$data_head);
		
		foreach($iddet_obat as $index => $val){
			$data_detail=array(
				'idgroup_penjualan' =>$idgroup_penjualan[$index],
				'idgroup_diskon' =>$idgroup_diskon[$index],
				'idakun_penjualan' =>$this->GetNoAkunGroup($idgroup_penjualan[$index]),
				'idakun_diskon' =>$this->GetNoAkunGroup($idgroup_diskon[$index]),				
			);
			$this->db->where('id',$val);
			$this->db->update('tvalidasi_pendapatan_rajal_detail_farmasi_racikan_detail',$data_detail);
		}
	
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah disimpan.');
		redirect('tvalidasi_pendapatan_rajal/edit_farmasi_racikan/'.$idhead.'/'.$iddetail.'/'.$form,'location');
		
	
	}
	function GetNoAkunGroup($id){
		return $this->db->query("SELECT GetNoAkunGroup($id) as idakun")->row('idakun');
	}
	function load_bayar(){
		
		$id=$this->input->post('id');
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		
        // $q = "SELECT H.id,H.metode_nama,SUM(H.nominal_bayar) as nominal_bayar FROM tvalidasi_pendapatan_rajal_bayar H
				// WHERE H.idvalidasi='$id'
				// GROUP BY H.idvalidasi,H.idmetode
				// ";
			$q="SELECT H.id,H.metode_nama,SUM(H.nominal_bayar) as nominal_bayar
				,'tvalidasi_pendapatan_rajal_bayar' as reff
				FROM tvalidasi_pendapatan_rajal_bayar H
				WHERE H.idvalidasi='$id'
				GROUP BY H.idvalidasi,H.idmetode

				UNION 

				SELECT H.id,H.metode_nama,SUM(H.nominal_bayar) as nominal_bayar
				,'tvalidasi_pendapatan_rajal_bayar_non_tunai' as reff
				FROM tvalidasi_pendapatan_rajal_bayar_non_tunai H
				WHERE H.idvalidasi='$id'
				GROUP BY H.idvalidasi,H.idmetode,H.tipekontraktor,H.idkontraktor";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			$total = $total + $r->nominal_bayar;			
			$aksi       = '<div class="btn-group">';
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td><input type="hidden" name="iddet_bayar[]" value="'.$r->id.'">';
			$tbl .='<td class="text-center">'.$r->metode_nama.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_bayar,2).'</td>';
			$tbl .='<td></td>';
			$aksi .='</div>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<tr>';
		$tbl .='<td colspan="2" class="text-right"><strong>TOTAL</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total,2).'</strong></td>';
		$tbl .='<td class="text-right"></td>';
		
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function load_other(){
		
		$id=$this->input->post('id');
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		$disabel=$this->input->post('disabel');
		
        // $q = "SELECT H.id,H.metode_nama,SUM(H.nominal_bayar) as nominal_bayar FROM tvalidasi_pendapatan_rajal_bayar H
				// WHERE H.idvalidasi='$id'
				// GROUP BY H.idvalidasi,H.idmetode
				// ";
			$q="SELECT H.id,H.idakun_debet,H.idakun_kredit,H.hasil_pemeriksaan
				,CONCAT(AD.noakun,' - ',AD.namaakun) as akun_debet
				,CONCAT(AK.noakun,' - ',AK.namaakun) as akun_kredit
				,H.nominal_bayar
				FROM tvalidasi_pendapatan_rajal_other H
				LEFT JOIN makun_nomor AD ON AD.id=H.idakun_debet
				LEFT JOIN makun_nomor AK ON AK.id=H.idakun_kredit
				WHERE H.idvalidasi='$id'";
		$tbl='';
		
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total=0;
		foreach($rows as $r){
			$no++;
			$total = $total + $r->nominal_bayar;	
			$aksi='';
			
			$aksi = '<div class="btn-group">';
				$aksi .='<button type="button" '.$disabel.' title="Edit Other" class="btn btn-primary btn-xs btn_simpan_other"><i class="fa fa-save"></i></button>';
			$aksi .='</div>';
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td><input type="hidden" class="iddet_other" value="'.$r->id.'">';
			$tbl .='<td class="text-center">'.($r->hasil_pemeriksaan=='1'?text_primary('INCOME'):text_danger('LOSS')).'</td>';
			$tbl .='<td class="text-right">'.number_format($r->nominal_bayar,2).'</td>';
			$tbl .='<td class="text-left">'.$this->opsi_akun($list,'idakun_debet',$r->idakun_debet,$disabel).'</td>';
			$tbl .='<td class="text-left">'.$this->opsi_akun($list,'idakun_kredit',$r->idakun_kredit,$disabel).'</td>';
			$tbl .='<td>'.$aksi.'</td>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<tr>';
		$tbl .='<td colspan="2" class="text-right"><strong>TOTAL</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total,2).'</strong></td>';
		$tbl .='<td></td>';
		$tbl .='<td></td>';
		$tbl .='<td class="text-right"></td>';
		
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function opsi_akun($list,$nama_var,$idakun,$disabel){
		$hasil='<select class="js-select2 form-control '.$nama_var.'" '.$disabel.' style="width: 100%;" data-placeholder="Choose one..">';
		foreach($list as $row){
			$hasil .='<option value="'.$row->id.'" '.($row->id==$idakun?"selected":"").'>'.$row->noakun.' - '.$row->namaakun.'</option>';
		}
		$hasil .='<option value="0" '.($idakun=="0"?"selected":"").'>TIDAK ADA</option>';
		$hasil .='</select>';
		return $hasil;
	}
	function load_ringkasan(){
		$where='';
		$id=$this->input->post('id');
		$idhead=$this->input->post('idhead');
		$disabel=$this->input->post('disabel');
		if ($idhead){
			$where=" AND T.idhead='$idhead'";
		}
        $q = "
				SELECT T.idvalidasi,T.idakun,A.noakun,A.namaakun
				,SUM(CASE WHEN T.posisi_akun='D' THEN IFNULL(T.nominal,0) ELSE 0 END) as debet 
				,SUM(CASE WHEN T.posisi_akun='K' THEN IFNULL(T.nominal,0) ELSE 0 END) as kredit
				,GROUP_CONCAT(DISTINCT T.ket ORDER BY T.ket ASC SEPARATOR ', ') as keterangan
				FROM (
						SELECT H.idvalidasi,H.idakun_round as idakun,H.posisi_round as posisi_akun,H.nominal_round as nominal,H.id as idhead,CONCAT('ROUND UP KASIR RAWAT JALAN') as ket
						FROM tvalidasi_pendapatan_rajal_detail H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,H.nominal_diskon as nominal,H.id as idhead,CONCAT('DISKON RAWAT JALAN') as ket
						FROM tvalidasi_pendapatan_rajal_detail H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_gabungan as idakun,H.posisi_gabungan as posisi_akun,H.nominal_gabungan as nominal,H.id as idhead,CONCAT('GABUNGAN RAWAT JALAN') as ket
						FROM tvalidasi_pendapatan_rajal_detail H WHERE H.idvalidasi='$id' AND H.st_gabungan='1'
						UNION ALL
						
						SELECT H.idvalidasi,H.idakun_jasasarana as idakun,CASE WHEN D.idtipe='4' THEN 'D' ELSE 'K' END as posisi_akun,H.jasasarana*H.kuantitas as nominal,H.idhead,CONCAT('JASA SARANA ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
						LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
						WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_jasapelayanan as idakun,CASE WHEN D.idtipe='4' THEN 'D' ELSE 'K' END  as posisi_akun,H.jasapelayanan*H.kuantitas as nominal,H.idhead,CONCAT('JASA PELAYANAN ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
						LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
						WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_bhp as idakun,CASE WHEN D.idtipe='4' THEN 'D' ELSE 'K' END as posisi_akun,H.bhp*H.kuantitas as nominal,H.idhead,CONCAT('BHP ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
						LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
						WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_biayaperawatan as idakun,CASE WHEN D.idtipe='4' THEN 'D' ELSE 'K' END  as posisi_akun,H.biayaperawatan*H.kuantitas as nominal,H.idhead,CONCAT('BIAYA PERAWATAN ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
						LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
						WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_jasasarana_diskon as idakun,CASE WHEN D.idtipe='4' THEN 'K' ELSE 'D' END as posisi_akun,H.jasasarana_diskon*H.kuantitas as nominal,H.idhead,CONCAT('DISKON JASA SARANA ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
						LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
						WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_jasapelayanan_diskon as idakun,CASE WHEN D.idtipe='4' THEN 'K' ELSE 'D' END as posisi_akun,H.jasapelayanan_diskon*H.kuantitas as nominal,H.idhead,CONCAT('DISKON JASA PELAYANAN  ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
						LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
						WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_bhp_diskon as idakun,CASE WHEN D.idtipe='4' THEN 'K' ELSE 'D' END as posisi_akun,H.bhp_diskon*H.kuantitas as nominal,H.idhead,CONCAT('DISKON BHP ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
						LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
						WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_biayaperawatan_diskon as idakun,CASE WHEN D.idtipe='4' THEN 'K' ELSE 'D' END as posisi_akun,H.biayaperawatan_diskon*H.kuantitas as nominal,H.idhead,CONCAT('DISKON BIAYA PERAWATAN ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
						LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
						WHERE H.idvalidasi='$id' 
						UNION ALL
						SELECT H.idvalidasi,H.idakun_diskon_all as idakun,CASE WHEN D.idtipe='4' THEN 'K' ELSE 'D' END as posisi_akun,H.diskon as nominal,H.idhead,CONCAT('DISKON LAYANAN ',H.namatarif) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan H 
						LEFT JOIN tvalidasi_pendapatan_rajal_detail D ON D.id=H.idhead
						WHERE H.idvalidasi='$id' 


						UNION ALL
						SELECT H.idvalidasi,H.idakun_penjualan as idakun,H.posisi_penjualan as posisi_akun,H.hargajual * H.kuantitas as nominal,H.idhead,CONCAT('PENJUALAN ',CASE WHEN H.idtipe='1' THEN 'ALKES' ELSE 'OBAT' END) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan_obat H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,H.diskon as nominal,H.idhead,CONCAT('DISKON ',CASE WHEN H.idtipe='1' THEN 'ALKES' ELSE 'OBAT' END) as ket
						FROM tvalidasi_pendapatan_rajal_detail_tindakan_obat H WHERE H.idvalidasi='$id'

						UNION ALL

						SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,H.total_diskon as nominal,H.idhead,CONCAT('DISKON ',CASE WHEN H.idtipe='1' THEN 'ALKES' ELSE 'OBAT' END) as ket
						FROM tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_penjualan as idakun,H.posisi_penjualan as posisi_akun,H.total_jual as nominal,H.idhead,CONCAT('FARMASI ',CASE WHEN H.idtipe='1' THEN 'ALKES' ELSE 'OBAT' END) as ket
						FROM tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_tuslah as idakun,H.posisi_tuslah as posisi_akun,H.tuslah as nominal,H.idhead,CONCAT('TUSLAH ',CASE WHEN H.idtipe='1' THEN 'ALKES' ELSE 'OBAT' END) as ket
						FROM tvalidasi_pendapatan_rajal_detail_farmasi_non_racikan H WHERE H.idvalidasi='$id'
						
						UNION ALL
						SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,H.total_diskon as nominal,H.idhead,CONCAT('DISKON RACIKAN ',CASE WHEN H.idtipe='1' THEN 'ALKES' ELSE 'OBAT' END) as ket
						FROM tvalidasi_pendapatan_rajal_detail_farmasi_racikan_detail H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_penjualan as idakun,H.posisi_penjualan as posisi_akun,H.total_jual as nominal,H.idhead,CONCAT('FARMASI RACIKAN ',CASE WHEN H.idtipe='1' THEN 'ALKES' ELSE 'OBAT' END) as ket
						FROM tvalidasi_pendapatan_rajal_detail_farmasi_racikan_detail H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_tuslah as idakun,H.posisi_tuslah as posisi_akun,H.tuslah as nominal,H.idhead,CONCAT('TUSLAH RACIKAN') as ket
						FROM tvalidasi_pendapatan_rajal_detail_farmasi_racikan H WHERE H.idvalidasi='$id'

						UNION ALL
						SELECT H.idvalidasi,H.idakun as idakun,H.posisi_akun as posisi_akun,H.nominal_bayar as nominal,H.idhead,CONCAT('PEMBAYARAN ',H.metode_nama) as ket
						FROM tvalidasi_pendapatan_rajal_bayar H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun as idakun,H.posisi_akun as posisi_akun,H.nominal_bayar as nominal,H.idhead,CONCAT('PEMBAYARAN NON TUNAI',H.metode_nama) as ket
						FROM tvalidasi_pendapatan_rajal_bayar_non_tunai H WHERE H.idvalidasi='$id'
						
						UNION ALL
						SELECT H.idvalidasi,H.idakun as idakun,H.posisi_akun as posisi_akun,H.nominal_bayar as nominal,H.idhead,CONCAT('PEMBAYARAN INFORMASI MEDIS ',H.metode_nama,' ',IFNULL(H.bank,'')) as ket
						FROM tvalidasi_pendapatan_rajal_info_medis_bayar H WHERE H.idvalidasi='$id'

						UNION ALL
						SELECT H.idvalidasi,H.idakun_diskon_all as idakun,H.posisi_diskon_all as posisi_akun,H.nominal_diskon_all as nominal,H.id as  idhead,CONCAT('INFORMASI MEDIS DISKON ALL - ',H.notransaksi) as ket
						FROM tvalidasi_pendapatan_rajal_info_medis H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_round as idakun,H.posisi_round as posisi_akun,H.nominal_round as nominal,H.id as  idhead,CONCAT('INFORMASI MEDIS ROUND - ',H.notransaksi) as ket
						FROM tvalidasi_pendapatan_rajal_info_medis H WHERE H.idvalidasi='$id'

						UNION ALL
						SELECT H.idvalidasi,H.idakun_dokter as idakun,H.posisi_dokter as posisi_akun,H.nominal_dokter as nominal,H.idhead,CONCAT('TARIF DOKTER ',H.namapasien,' - ',IFNULL(H.no_medrec,'')) as ket
						FROM tvalidasi_pendapatan_rajal_info_medis_detail H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_rs as idakun,H.posisi_rs as posisi_akun,H.nominal_rs as nominal,H.idhead,CONCAT('TARIF RS ',H.namapasien,' - ',IFNULL(H.no_medrec,'')) as ket
						FROM tvalidasi_pendapatan_rajal_info_medis_detail H WHERE H.idvalidasi='$id'
						UNION ALL
						SELECT H.idvalidasi,H.idakun_diskon as idakun,H.posisi_diskon as posisi_akun,H.diskon_rp as nominal,H.idhead,CONCAT('DISKON ',H.namapasien,' - ',IFNULL(H.no_medrec,'')) as ket
						FROM tvalidasi_pendapatan_rajal_info_medis_detail H WHERE H.idvalidasi='$id'
						
						UNION ALL
						SELECT
						H.idvalidasi,H.idakun_debet AS idakun,'D' AS posisi_akun,H.nominal_bayar AS nominal,'' as idhead,
						CASE WHEN H.hasil_pemeriksaan='1' THEN 'OTHER INCOME' ELSE 'OTHER LOSS' END AS ket 
						FROM tvalidasi_pendapatan_rajal_other H  WHERE H.idvalidasi = '$id' AND H.idakun_debet IS NOT NULL
						
						UNION ALL
						
						SELECT H.idvalidasi,H.idakun_kredit AS idakun,'K' AS posisi_akun,H.nominal_bayar AS nominal,'' as idhead,
						CASE WHEN H.hasil_pemeriksaan='1' THEN 'OTHER INCOME' ELSE 'OTHER LOSS' END AS ket 
						FROM tvalidasi_pendapatan_rajal_other H WHERE H.idvalidasi = '$id' AND H.idakun_kredit IS NOT NULL

				) T 
				LEFT JOIN makun_nomor A ON A.id=T.idakun
				WHERE T.nominal > 0 ".$where."
				GROUP BY T.posisi_akun,T.idakun

				";
		$tbl='';
		// print_r($q);exit;
		$rows=$this->db->query($q)->result();
		
		$no=0;
		$total_D=0;
		$total_K=0;
		foreach($rows as $r){
			$no++;
			$total_D=$total_D + $r->debet;
			$total_K=$total_K + $r->kredit;
			
			
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td class="text-right">'.$r->noakun.'</td>';
			$tbl .='<td>'.$r->namaakun.'</td>';
			$tbl .='<td class="text-right">'.number_format($r->debet,2).'</td>';
			$tbl .='<td class="text-right">'.number_format($r->kredit,2).'</td>';
			$tbl .='</tr>';
		}
		$tbl .='<tr>';
		$tbl .='<td colspan="3" class="text-right"><strong>TOTAL</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_D,2).'</strong></td>';
		$tbl .='<td class="text-right"><strong>'.number_format($total_K,2).'</strong></td>';
		
		$tbl .='</tr>';
		$arr['tabel']=$tbl;
		$this->output->set_output(json_encode($arr));
    }
	function list_akun($list,$idakun){
		$hasil='';
		foreach($list as $row){
			$hasil .='<option value="'.$row->id.'" '.($row->id==$idakun?"selected":"").'>'.$row->noakun.' - '.$row->namaakun.'</option>';
		}
		$hasil .='<option value="0" '.($idakun=="0"?"selected":"").'>TIDAK ADA</option>';
		return $hasil;
	}
	
}
