<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mtarif_laboratorium extends CI_Controller
{
    /**
     * Tarif Laboratorium controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mtarif_laboratorium_model');
        $this->load->model('Mtarif_administrasi_model');
    }

    public function index($idtipe = '1', $idparent = '0', $idsubparent = '0', $status_paket = '9'): void
    {
        $data = [
            'idtipe' => $idtipe,
            'idparent' => $idparent,
            'idsubparent' => $idsubparent,
            'status_paket' => $status_paket,
        ];

        $data['error'] = '';
        $data['title'] = 'Tarif Laboratorium';
        $data['content'] = 'Mtarif_laboratorium/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Tarif Laboratorium', 'mtarif_laboratorium/index'],
            ['List', '#'],
        ];

        $data['list_parent'] = $this->Mtarif_laboratorium_model->find_index_parent($idtipe);
        $data['list_subparent'] = $this->Mtarif_laboratorium_model->find_index_subparent($idparent);
        
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function filter(): void
    {
        if ('' != $this->input->post('idtipe')) {
            $idtipe = $this->input->post('idtipe');
            $idparent = $this->input->post('idparent');
            $idsubparent = count($this->input->post('idsubparent')) ? implode('_', $this->input->post('idsubparent')) : '0';
            $status_paket = $this->input->post('status_paket');
            redirect("mtarif_laboratorium/index/$idtipe/$idparent/$idsubparent/$status_paket", 'location');
        } else {
            redirect('mtarif_laboratorium', 'location');
        }
    }

    public function create($idtipe = '0', $level0 = '0', $headerpath = '0'): void
    {
        $data = [
            'id' => '',
            'kode' => '',
            'nama' => '',
            'nama_english' => '',
            'metode' => '',
            'sumber_spesimen' => '',
            'group_test' => '',
            'idtipe' => $idtipe,
            'idkelompok' => '1',
            'idpaket' => '0',
            'level0' => $level0,
            'old_level0' => '',
            'headerpath' => $headerpath,
            'old_headerpath' => '',
            'path' => '',
            'level' => '',
            'status' => '',
            'idkelompokparent' => '',
            'idpaketparent' => '',
        ];

        $data['error'] = '';
        $data['title'] = 'Tambah Tarif Laboratorium';
        $data['content'] = 'Mtarif_laboratorium/manage';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Tarif Laboratorium', '#'],
            ['Tambah', 'mtarif_laboratorium'],
        ];

        $data['list_level0'] = $this->Mtarif_laboratorium_model->find_update_parent($idtipe);
        $data['list_parent'] = $this->Mtarif_laboratorium_model->find_update_subparent($level0);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id): void
    {
        if ('' != $id) {
            $row = $this->Mtarif_laboratorium_model->getSpecified($id);
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'kode' => $row->kode,
                    'nama' => $row->nama,
                    'nama_english' => $row->nama_english,
                    'metode' => $row->metode,
                    'sumber_spesimen' => $row->sumber_spesimen,
                    'group_test' => $row->group_test,
                    'idtipe' => $row->idtipe,
                    'idkelompok' => $row->idkelompok,
                    'idpaket' => $row->idpaket,
                    'level0' => $row->level0,
                    'old_level0' => $row->level0,
                    'headerpath' => $row->headerpath,
                    'old_headerpath' => $row->headerpath,
                    'path' => $row->path,
                    'level' => $row->level,
                    'status' => $row->status,
                    'idkelompokparent' => $row->idkelompokparent,
                    'idpaketparent' => $row->idpaketparent,
                ];

                $data['error'] = '';
                $data['title'] = 'Ubah Tarif Laboratorium';
                $data['content'] = 'Mtarif_laboratorium/manage';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Tarif Laboratorium', '#'],
                    ['Ubah', 'mtarif_laboratorium'],
                ];

                $level0 = $row->level0 ? $row->level0 : getLevelZero($row->headerpath);
                $data['list_level0'] = $this->Mtarif_laboratorium_model->find_update_parent($row->idtipe);
                $data['list_parent'] = $this->Mtarif_laboratorium_model->find_update_subparent($level0);
                $data['list_tarif'] = $this->Mtarif_laboratorium_model->getAllTarif($row->id);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mtarif_laboratorium', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mtarif_laboratorium', 'location');
        }
    }

    public function delete($id): void
    {
        $this->Mtarif_laboratorium_model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mtarif_laboratorium', 'location');
    }

    public function save(): void
    {
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');

        if (true == $this->form_validation->run()) {
            if ('' == $this->input->post('id')) {
                if ($this->Mtarif_laboratorium_model->saveData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mtarif_laboratorium/index/'.$this->input->post('idtipe'), 'location');
                }
            } else {
                if ($this->Mtarif_laboratorium_model->updateData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mtarif_laboratorium/index/'.$this->input->post('idtipe'), 'location');
                }
            }
        } else {
            $this->failed_save($this->input->post('id'));
        }
    }

    public function failed_save($id): void
    {
        $data = $this->input->post();
        $data['error'] = validation_errors();
        $data['content'] = 'Mtarif_laboratorium/manage';

        $data['list_parent'] = $this->Mtarif_laboratorium_model->getAllParent($this->input->post('idtipe'));

        if ('' == $id) {
            $data['title'] = 'Tambah Satuan';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Satuan', '#'],
                ['Tambah', 'mtarif_laboratorium'],
            ];
        } else {
            $data['title'] = 'Ubah Satuan';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Satuan', '#'],
                ['Ubah', 'mtarif_laboratorium'],
            ];
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function get_parent($idtipe): void
    {
        $arr = $this->Mtarif_laboratorium_model->getParent($idtipe);
        $this->output->set_output(json_encode($arr));
    }

    public function get_child_level($headerpath): void
    {
        $arr = $this->Mtarif_laboratorium_model->getPathLevel($headerpath);
        $this->output->set_output(json_encode($arr));
    }

    public function getIndex($idtipe, $idparent, $idsubparent, $status_paket): void
    {
        $data_user = get_acces();
        $user_acces_form = $data_user['user_acces_form'];

        $this->select = [];
        $this->from = 'mtarif_laboratorium';
        $this->join = [
            ['mnilainormal_pelayanan', 'mnilainormal_pelayanan.idtariflaboratorium = mtarif_laboratorium.id', 'LEFT'],
        ];
        
        $whereArray = [
            'mtarif_laboratorium.status' => '1',
        ];
        $orWhereArray = [];

        if ($idtipe != '0') {
            $whereArray = array_merge($whereArray, [
                'mtarif_laboratorium.idtipe' => $idtipe,
            ]);
        }

        if ($status_paket != '9') {
            $whereArray = array_merge($whereArray, [
                'mtarif_laboratorium.idpaket' => $status_paket,
            ]);
        }

        if ($idsubparent == '0') {
            if ($idparent != '0') {
                $whereArray = array_merge($whereArray, [
                    'left(mtarif_laboratorium.path,' . strlen($idparent) . ')' => $idparent,
                ]);
            }
        } else {
            $idsubparentArray = explode("_", $idsubparent);
            foreach ($idsubparentArray as $val) {
                $orWhereArray[] = array("LEFT(mtarif_laboratorium.path, " . strlen($val . '.') . ")" => $val . '.');
            }
        }

        $this->where = $whereArray;
        $this->or_where = $orWhereArray;
				
        $this->order = [
            'path' => 'ASC',
        ];

        $this->group = [];

        if (UserAccesForm($user_acces_form, ['154'])) {
            $this->column_search = ['nama'];
        } else {
            $this->column_search = [];
        }
        $this->column_order = ['nama'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = TreeView($r->level, $r->nama);
            $aksi = '<div class="btn-group">';
            if (UserAccesForm($user_acces_form, ['156'])) {
                $aksi .= '<a href="'.site_url().'mtarif_laboratorium/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
            }
            if (UserAccesForm($user_acces_form, ['157'])) {
                $aksi .= '<a href="#" data-urlindex="'.site_url().'mtarif_laboratorium" data-urlremove="'.site_url().'mtarif_laboratorium/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
            }
            if (0 == $r->idkelompok) {
                $aksi .= '<a href="'.site_url().'mtarif_laboratorium/setting/'.$r->id.'" data-toggle="tooltip" title="Setting" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>';
            } else {
                if (1 == $r->idkelompok && '1' == $r->idpaket) {
                    $aksi .= '<a href="'.site_url().'mtarif_laboratorium/setting/'.$r->id.'" data-toggle="tooltip" title="Setting" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>';
                }
            }
            $aksi .= '<a href="'.site_url().'mtarif_laboratorium/create/'.($r->idtipe ? $r->idtipe : 0).'/'.($r->level0 ? $r->level0 : 0).'/'.($r->headerpath ? $r->headerpath : 0).'" data-toggle="tooltip" title="Tambah Tarif Child" class="btn btn-warning btn-sm"><i class="fa fa-plus"></i></a>';
            if ($r->idnilainormal) {
                $aksi .= '<a href="'.site_url().'mnilainormal/update/'.$r->idnilainormal.'" data-toggle="tooltip" title="Nilai Normal" class="btn btn-primary btn-sm"><i class="fa fa-cloud-upload"></i></a>';
            }
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function setting($id): void
    {
        if ('' != $id) {
            $row = $this->Mtarif_laboratorium_model->getSpecified($id);
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'kode' => $row->kode,
                    'nama' => $row->nama,
                    'nama_english' => $row->nama_english,
                    'metode' => $row->metode,
                    'sumber_spesimen' => $row->sumber_spesimen,
                    'group_test' => $row->group_test,
                    'idtipe' => $row->idtipe,
                    'idkelompok' => $row->idkelompok,
                    'idpaket' => $row->idpaket,
                    'level0' => $row->level0,
                    'headerpath' => $row->headerpath,
                    'old_headerpath' => $row->headerpath,
                    'path' => $row->path,
                    'level' => $row->level,
                    'status' => $row->status,
                    'group_diskon_all' => $row->group_diskon_all,
                ];

                $data['error'] = '';
                $data['title'] = 'Setting Group Pembayaran Tarif Laboratorium';
                $data['content'] = 'Mtarif_laboratorium/setting';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Tarif Laboratorium', 'mtarif_laboratorium/index'],
                    ['Setting', 'mtarif_laboratorium'],
                ];

                $data['list_level0'] = $this->Mtarif_laboratorium_model->find_update_parent($row->idtipe);
                $data['list_tarif'] = $this->Mtarif_laboratorium_model->getAllTarif($row->id);
                $data['list_group_pembayaran'] = $this->Mtarif_administrasi_model->getGroupPembayaran();

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mtarif_laboratorium/index/'.$id, 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mtarif_laboratorium/index/'.$id, 'location');
        }
    }

    public function save_setting(): void
    {
        if ($this->Mtarif_laboratorium_model->updateSetting()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('mtarif_laboratorium/index/'.$this->input->post('idtipe'), 'location');
        }
    }

    public function find_headparent($idtipe, $idkelompokpasien, $idrekanan): void
    {
        $arr['detail'] = $this->Mtarif_laboratorium_model->find_headparent($idtipe, $idkelompokpasien, $idrekanan);
        $this->output->set_output(json_encode($arr));
    }

    public function find_headparent_all($idtipe): void
    {
        $arr['detail'] = $this->Mtarif_laboratorium_model->find_headparent_all($idtipe);
        $this->output->set_output(json_encode($arr));
    }

    // # Update Document ERM
    public function find_index_parent($idtipe): void
    {
        $data = $this->Mtarif_laboratorium_model->find_index_parent($idtipe);

        $arr = '';
        foreach ($data as $row) {
            $arr = $arr.'<option value="'.$row->path.'">'.TreeView($row->level, $row->nama).'</option>';
        }

        $this->output->set_output(json_encode($arr));
    }

    public function find_index_subparent($idparent): void
    {
        $data = $this->Mtarif_laboratorium_model->find_index_subparent($idparent);

        $arr = '';
        foreach ($data as $row) {
            $arr = $arr.'<option value="'.$row->path.'">'.TreeView($row->level, $row->nama).'</option>';
        }

        $this->output->set_output(json_encode($arr));
    }

    public function find_manage_parent($idtipe): void
    {
        $arr = $this->Mtarif_laboratorium_model->find_manage_parent($idtipe);
        $this->output->set_output(json_encode($arr));
    }

    public function find_manage_subparent($idparent): void
    {
        $arr = $this->Mtarif_laboratorium_model->find_manage_subparent($idparent);
        $this->output->set_output(json_encode($arr));
    }
}
