<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tobservasi extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Tpendaftaran_poli_model');
		$this->load->model('Tobservasi_model');
		$this->load->helper('path');
		
  }
	function find_shift($id){
		$q="SELECT * FROM mshift H
			WHERE H.`shift_id`='$id' ";
		$opsi=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($opsi));
	}
	function tindakan($pendaftaran_id='',$st_ranap='0',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		get_ppa_login();
	
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data_satuan_ttv=array();
		
		$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id,$st_ranap);
		// print_r($data);exit;
		$data_login_ppa=get_ppa_login();
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tpendaftaran_poli_ttv/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		// $data['array_menu_kiri'] 	= $this->Tpendaftaran_poli_ttv_model->list_menu_kiri($menu_atas);
		// $data['array_menu_kiri'] 	= array('input_ttv','input_data');
		
		// print_r(json_encode($data['array_menu_kiri']));exit;
		$data['st_ranap'] 			= $st_ranap;
		$data['idpoli'] 			= '#';
		
		$data['tanggal_1'] 			= DMYFormat(date('Y-m-d'));
		$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
		$data['assesmen_id'] 			= '';
		$data['status_assesmen'] 			= '1';
		$data['error'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'Assesmen Observasi';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Tindakan  ",'#'),
											  array("Assesmen Observasi",'tpendaftaran_poli_ttv')
											);
		
		
		
		if ($menu_kiri=='input_observasi' || $menu_kiri=='his_observasi'){
			$data['template_assesmen_id']='';
			if ($trx_id!='#'){
				if ($menu_kiri=='his_observasi'){
					$data_assemen=$this->Tobservasi_model->get_data_observasi_trx_his($trx_id,$versi_edit);
					$data_assemen_asal=$this->Tobservasi_model->get_data_observasi_trx_asal($trx_id,$versi_edit);
					$data_assemen = array_merge($data_assemen,$data_assemen_asal);
					// print_r($data_assemen);exit;
				}else{
					$data_assemen=$this->Tobservasi_model->get_data_observasi_trx($trx_id);
				}
				
				// print_r($data_assemen);exit;
				
			}else{
				$data_assemen=$this->Tobservasi_model->get_data_observasi($pendaftaran_id,$st_ranap);
			// print_r($data_assemen);exit;
				
			}
			if ($data_assemen){
				$data_assemen['tab_utama']='1';
			}else{
				$data_assemen['tab_utama']='2';
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				$data_assemen['total_skor_observasi']='0';
				$data_assemen['tanggal_pengkajian']=date('d-m-Y');
			}
			$data_assemen['trx_id']=$trx_id;
			$data_assemen['versi_edit']=$versi_edit;
			if ($menu_kiri=='his_observasi'){
				
			}else{
				$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
				$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
				$data['list_ppa'] 			= $this->Tpendaftaran_poli_ttv_model->list_ppa();
			}
			$list_template_assement=$this->Tobservasi_model->list_template_observasi();
			$data_assemen['list_template_assement']=$list_template_assement;
			
			$setting_observasi=$this->Tobservasi_model->setting_observasi();
			
			$data = array_merge($data,$data_assemen,$setting_observasi);
		}
		
		$data['trx_id']=$trx_id;
		// print_r($data);exit;
		$data = array_merge($data, backend_info());
			// print_r($list_template_assement);exit;
		$this->parser->parse('module_template', $data);
		
	}
	function create_observasi(){
		  $login_ppa_id=$this->session->userdata('login_ppa_id');
		  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		  $pendaftaran_id=$this->input->post('pendaftaran_id');
		  $template_id=$this->input->post('template_id');
		  $idpasien=$this->input->post('idpasien');
		  $st_ranap=$this->input->post('st_ranap');
		  if ($st_ranap=='1'){
		  $q="SELECT H.nadi,H.nafas,H.td_sistole,H.td_diastole,H.suhu,H.tingkat_kesadaran,H.spo2 FROM `tranap_ttv` H WHERE H.pendaftaran_id_ranap='$pendaftaran_id_ranap' ORDER BY H.created_date DESC LIMIT 1";
			  
		  }else{
		  $q="SELECT H.nadi,H.nafas,H.td_sistole,H.td_diastole,H.suhu,H.tingkat_kesadaran,H.spo2 FROM `tpoliklinik_ttv` H WHERE H.pendaftaran_id='$pendaftaran_id' ORDER BY H.created_date DESC LIMIT 1";
			  
		  }
		  $data_ttv=$this->db->query($q)->row_array();
		  
		  $data=array(
			'tanggal_input' => $tanggal_input,
			'pendaftaran_id' => $pendaftaran_id,
			'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
			'idpasien' => $idpasien,
			'st_ranap' => $st_ranap,
			// 'template_id' => $template_id,
			'created_ppa' => $login_ppa_id,
			'created_date' => date('Y-m-d H:i:s'),
			'status_assemen' => 1,
			
		  );
		  if ($data_ttv){
			  $data=array_merge($data,$data_ttv);
		  }
		  $hasil=$this->db->insert('tranap_observasi',$data);
		 
		  
		  $this->output->set_output(json_encode($hasil));
    }
	function create_template_observasi(){
		  $login_ppa_id=$this->session->userdata('login_ppa_id');
		  $tanggal_input= YMDFormat($this->input->post('tglpendaftaran')). ' ' .$this->input->post('waktupendaftaran');
		  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		  $pendaftaran_id=$this->input->post('pendaftaran_id');
		  $template_id=$this->input->post('template_id');
		  $idpasien=$this->input->post('idpasien');
		  $st_ranap=$this->input->post('st_ranap');
		  if ($st_ranap=='1'){
		  $q="SELECT H.nadi,H.nafas,H.td_sistole,H.td_diastole,H.suhu,H.tingkat_kesadaran,H.spo2 FROM `tranap_ttv` H WHERE H.pendaftaran_id_ranap='$pendaftaran_id_ranap' ORDER BY H.created_date DESC LIMIT 1";
			  
		  }else{
		  $q="SELECT H.nadi,H.nafas,H.td_sistole,H.td_diastole,H.suhu,H.tingkat_kesadaran,H.spo2 FROM `tpoliklinik_ttv` H WHERE H.pendaftaran_id='$pendaftaran_id' ORDER BY H.created_date DESC LIMIT 1";
			  
		  }
		  $data_ttv=$this->db->query($q)->row_array();
		  
		  $data=array(
			'tanggal_input' => $tanggal_input,
			'pendaftaran_id' => $pendaftaran_id,
			'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
			'idpasien' => $idpasien,
			'st_ranap' => $st_ranap,
			// 'template_id' => $template_id,
			'created_ppa' => $login_ppa_id,
			'created_date' => date('Y-m-d H:i:s'),
			'status_assemen' => 3,
			
		  );
		  if ($data_ttv){
			  $data=array_merge($data,$data_ttv);
		  }
		  $hasil=$this->db->insert('tranap_observasi',$data);
		 
		  
		  $this->output->set_output(json_encode($hasil));
    }
	function save_observasi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_input= YMDFormat($this->input->post('tanggal_input')). ' ' .$this->input->post('waktudaftar');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$st_edited=$this->input->post('st_edited');
		$assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'pendaftaran_id_ranap'=> $this->input->post('pendaftaran_id_ranap'),
			'tanggal_input'=> $tanggal_input,
			'tingkat_kesadaran'=> $this->input->post('tingkat_kesadaran'),
			'nadi'=> $this->input->post('nadi'),
			'nafas'=> $this->input->post('nafas'),
			'spo2'=> $this->input->post('spo2'),
			'td_sistole'=> $this->input->post('td_sistole'),
			'td_diastole'=> $this->input->post('td_diastole'),
			'suhu'=> $this->input->post('suhu'),
			'intek_jenis'=> $this->input->post('intek_jenis'),
			'intek'=> $this->input->post('intek'),
			'status_assemen'=> $this->input->post('status_assemen'),
			'satuan_intek'=> $this->input->post('satuan_intek'),
			'output' => $this->input->post('output'),
			'jumlah_output' => $this->input->post('jumlah_output'),
			'satuan_output' => $this->input->post('satuan_output'),
			'keterangan' => $this->input->post('keterangan'),
			'st_edited' => $this->input->post('st_edited'),
			'nama_template' => $this->input->post('nama_template'),

		);
		// print_r($st_edited);exit;
		if ($st_edited=='0'){
			
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tranap_observasi_x_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update('tranap_observasi',$data);
		$this->output->set_output(json_encode($data));
	}
	
	function hapus_observasi_ri(){
		$id=$this->input->post('id');
		$data=array(
			
			'status_assemen'=> 0,
		);
			$data['deleted_ppa']=$this->input->post('login_ppa_id');
			$data['deleted_date']=date('Y-m-d H:i:s');
			
			$this->db->where('id',$id);
			$result=$this->db->update('tranap_observasi',$data);
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		$this->output->set_output(json_encode($hasil));
	}
	
	function hapus_record_observasi(){
		$id=$this->input->post('id');
		$data=array(
			'status_assemen'=> 0,
			'alasan_id'=> $this->input->post('alasan_id'),
			'keterangan_hapus'=> $this->input->post('keterangan_hapus'),
		);
			$data['deleted_ppa']=$this->input->post('login_ppa_id');
			$data['deleted_date']=date('Y-m-d H:i:s');
			
			$this->db->where('assesmen_id',$id);
			$result=$this->db->update('tranap_observasi',$data);
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		$this->output->set_output(json_encode($hasil));
	}
	function copy_observasi_ri(){
		$id=$this->input->post('id');
		$q="SELECT CONCAT(MP.nik,' - ',MP.nama) as ppa,H.* FROM tranap_observasi H
				INNER JOIN mppa MP ON MP.id=H.created_ppa
				WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$hasil['tglpendaftaran']=HumanDateShort(date('Y-m-d'));
		$hasil['waktupendaftaran']=HumanTime(date('H:i:s'));
		$this->output->set_output(json_encode($hasil));
	}
	function edit_observasi_ri(){
		$id=$this->input->post('id');
		$alasan_id=$this->input->post('alasan_id');
		$keterangan_edit=$this->input->post('keterangan_edit');
		if ($alasan_id){
			$data_edit=array(
				'alasan_edit_id' =>$alasan_id,
				'keterangan_edit' =>$keterangan_edit,
			);
			$this->db->where('id',$id);
			$this->db->update('tranap_observasi',$data_edit);
		}
		$q="SELECT CONCAT(MP.nik,' - ',MP.nama) as ppa,H.* FROM tranap_observasi H
				INNER JOIN mppa MP ON MP.id=H.created_ppa
				WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$hasil['tglpendaftaran']=HumanDateShort($hasil['tanggal_input']);
		$hasil['waktupendaftaran']=HumanTime($hasil['tanggal_input']);
		$this->output->set_output(json_encode($hasil));
	}
	function index_observasi_history(){
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
			$login_profesi_id=$this->session->userdata('login_profesi_id');
			
			$logic_akse_observasi=$this->Tpendaftaran_poli_ttv_model->logic_akses_observasi($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
			
			
			$akses_general=$this->db->query("SELECT *FROM setting_observasi")->row_array();
			$data_satuan_observasi=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
						
			$pendaftaran_id_ranap =$this->input->post('pendaftaran_id_ranap');
			$pendaftaran_id =$this->input->post('pendaftaran_id');
			$st_owned =$this->input->post('st_owned');
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$profesi_id =$this->input->post('profesi_id');
			$ppa_id =$this->input->post('ppa_id');
			$st_sedang_edit =$this->input->post('st_sedang_edit');
			$st_ranap =$this->input->post('st_ranap');
			if ($st_ranap=='1'){
				$where .=" AND H.pendaftaran_id_ranap='$pendaftaran_id_ranap' AND H.st_ranap='$st_ranap'";
			}else{
				$where .=" AND H.pendaftaran_id='$pendaftaran_id'  AND H.st_ranap='$st_ranap'";
				
			}
			// print_r($st_sedang_edit);exit;
			if ($tanggal_1  != ''){
				$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_2)."'";
			}else{
				// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
			}
			if ($ppa_id){
				$ppa_id=implode(", ", $ppa_id);
				$where .=" AND (H.created_ppa) IN (".$ppa_id.")";
			}
			if ($profesi_id){
				$profesi_id=implode(", ", $profesi_id);
				$where .=" AND (UC.jenis_profesi_id) IN (".$profesi_id.")";
			}
			
			if ($st_owned=='1'){
				$where .=" AND (H.created_ppa) = '$login_ppa_id'";
			}
			
			$this->select = array();
			$from="
					(
						SELECT DATEDIFF(NOW(), H.tanggal_input) as selisih,
						mnadi.warna as warna_nadi
						,mnafas.warna as warna_nafas
						,mtd_sistole.warna as warna_sistole
						,mtd_diastole.warna as warna_diastole
						,msuhu.warna as warna_suhu
						,TK.ref as kesadaran,H.* 
						,UC.nama as nama_created,UE.nama as nama_edited,UC.jenis_profesi_id,malasan_batal.keterangan as ket_edit
						,IJ.ref as jenis_intek_nama,ISA.ref as satuan_intek_nama
						,OJ.ref as jenis_output,OS.ref as satuan_output_nama
						FROM tranap_observasi H
						LEFT JOIN merm_referensi OJ ON OJ.nilai=H.output AND OJ.ref_head_id='424'
						LEFT JOIN merm_referensi OS ON OS.nilai=H.satuan_output AND OS.ref_head_id='425'
						LEFT JOIN merm_referensi IJ ON IJ.nilai=H.intek_jenis AND IJ.ref_head_id='423'
						LEFT JOIN merm_referensi ISA ON ISA.nilai=H.satuan_intek AND ISA.ref_head_id='425'
						LEFT JOIN merm_referensi TK ON TK.nilai=H.tingkat_kesadaran AND TK.ref_head_id='23'
						LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
						LEFT JOIN mnafas ON H.nafas BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
						LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
						LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
						LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
						LEFT JOIN mppa UC ON UC.id=H.created_ppa
						LEFT JOIN mppa UE ON UE.id=H.edited_ppa
						LEFT JOIN malasan_batal ON malasan_batal.id=H.alasan_edit_id
						WHERE  H.status_assemen > 1 ".$where."
						GROUP BY H.assesmen_id
						ORDER BY H.tanggal_input DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array();
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $btn_disabel_edit='';
		  $btn_disabel_hapus='';
		  if ($st_sedang_edit=='1'){
			$btn_disabel_edit='disabled';			  
		  }
		  
		  $btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_observasi('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		  $btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_observasi('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
		  $btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_observasi('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		  if ($akses_general['st_edit_catatan']=='0'){
			  $btn_edit='';
		  }else{
			  if ($akses_general['lama_edit']>0){
				  if ($r->selisih>$akses_general['lama_edit']){
					  $btn_edit='';
				  }else{
					  
				  }
				  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id != $r->created_ppa){
							$btn_edit='';
					  }
				  }
			  }
		  }
		  if ($akses_general['st_duplikasi_catatan']=='0'){
			  $btn_duplikasi='';
		  }else{
			  if ($akses_general['lama_duplikasi']>0){
				  if ($r->selisih>$akses_general['lama_duplikasi']){
					  $btn_duplikasi='';
				  }else{
					 
				  }
			  }
			   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id != $r->created_ppa){
						$btn_duplikasi='';
				  }
			   }
		  }
		  if ($akses_general['st_hapus_catatan']=='0'){
			  $btn_hapus='';
		  }else{
			  if ($akses_general['lama_hapus']>0){
				  if ($r->selisih>$akses_general['lama_hapus']){
					  $btn_hapus='';
				  }else{
					 
				  }
				   
			  }
			  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id <> $r->created_ppa){
						$btn_hapus='';
				  }else{
					  
				  }
						// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
			  }
		  }
				if ($logic_akse_observasi['st_edit_observasi']=='0'){
				  $btn_edit='';
			  }
			  if ($logic_akse_observasi['st_hapus_observasi']=='0'){
				  $btn_hapus='';
			  }
		 $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 85%;">
								
									
									<div class="h2 font-w700 '.($r->alasan_edit_id?'text-danger':'text-primary').' text-center">'.DFormat($r->tanggal_input).'</div>
									<div class="h5 text-muted text-center"> '.tanggal_indo_MY($r->tanggal_input).'</div>
									<div class="h5 text-muted  text-center"> '.HumanTime($r->tanggal_input).'</div>
									<div class="h5 text-muted text-uppercase push-5-t text-center"> 
										<div class="btn-group" role="group">
											'.$btn_edit.'
											'.$btn_duplikasi.'
											'.$btn_hapus.'
											
											
										</div>
									
									</div>
									'.($r->alasan_edit_id?'<div class="text-center text-success push-5-t"><a href="javascript:void(0)" onclick="lihat_perubahan('.$r->assesmen_id.')"><i class="fa fa-pencil text-center"></i> '.$r->ket_edit.'</a></div>':'').'
								</td>
							</tr>
						</tbody>
					</table>';
		
          $result[] = $btn_1;
		  $btn_2='
			<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white" colspan="2">
									<div class="h5 "> Tingkat Kesadaran :</div>
									<div class="h5 text-muted  push-5-t "> '.($r->kesadaran).'</div>
									
								</td>
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Frekuensi Nadi :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$r->warna_nadi.'" > '.($r->nadi).' '.$data_satuan_observasi['satuan_nadi'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frekuensi Nafas :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$r->warna_nafas.'"> '.($r->nafas).' '.$data_satuan_observasi['satuan_nafas'].'</div>
								</td>
							</tr>
							<tr>
								<td class="bg-white" colspan="2">
									<div class="h5 "> Keterangan :</div>
									<div class="h5 text-muted  push-5-t "> '.($r->keterangan).'</div>
								</td>
								
							</tr>
						</tbody>
					</table>
		  ';
		 
          $result[] = $btn_2;
		  $btn_3 ='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Tekanan Darah :</div>
									<div class="h5 text-muted  push-5-t "> <label  class="h5 text-muted  push-5-t" style="color:'.$r->warna_sistole.'">'.($r->td_sistole.' '.$data_satuan_observasi['satuan_td'].'</label> / <label class="h5 text-muted  push-5-t" style="color:'.$r->warna_diastole.'">'.$r->td_diastole).' '.$data_satuan_observasi['satuan_td'].'</label></div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Suhu Tubuh :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$r->warna_suhu.'"> '.($r->suhu).' '.$data_satuan_observasi['satuan_suhu'].'</div>
								</td>
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Intek :</div>
									<div class="h5 text-muted  push-5-t "> '.($r->jenis_intek_nama.' | '.$r->intek.' '.$r->satuan_intek_nama).'</div>
								</td>
								<td class="bg-white">
									<div class="h5 ">Output  :</div>
									<div class="h5 text-muted  push-5-t "> '.($r->jenis_output.' | '.$r->jumlah_output.' '.$r->satuan_output_nama).'</div>
								</td>
							</tr>
							
						</tbody>
					</table>
				';
		  
          $result[] = $btn_3;
		 							
		  if ($r->alasan_edit_id){
			   $kolom_edit='<div class="h5 "> Edited</div>
									<div class=" text-muted  push-5-t "><i class="fa fa-user-md"></i> '.($r->alasan_edit_id?$r->nama_edited.' <br>'.HumanDateLong($r->edited_date):'').'</div>';
		
		  }else{
			  $kolom_edit='';
		  }
		  $btn_4 ='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Created</div>
									<div class=" text-muted  push-5-t "><i class="fa fa-user-md"></i> '.$r->nama_created.' <br>('.HumanDateLong($r->created_date).')</div>
								</td>
								
							</tr>
							<tr>
								<td class="bg-white">
									'.$kolom_edit.'
																	
								</td>
								
							</tr>
							
						</tbody>
					</table>
				';
		  
          $result[] = $btn_4;
		  
          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function create_with_template_obsesvasi(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $template_assesmen_id=$this->input->post('template_assesmen_id');
	  $idpasien=$this->input->post('idpasien');
	  $st_ranap=$this->input->post('st_ranap');
	  $q="INSERT INTO tranap_observasi (
				idpasien,pendaftaran_id,pendaftaran_id_ranap,tanggal_input,tingkat_kesadaran,nadi,nafas,spo2,td_sistole,td_diastole,suhu,intek_jenis,intek,satuan_intek,output,jumlah_output,satuan_output,keterangan,status_assemen,created_ppa,created_date,edited_ppa,edited_date,deleted_ppa,deleted_date,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_ranap,template_assesmen_id,nama_template,jumlah_template,st_edited
			)
			SELECT
				'$idpasien','$pendaftaran_id','$pendaftaran_id_ranap',NOW(),tingkat_kesadaran,nadi,nafas,spo2,td_sistole,td_diastole,suhu,intek_jenis,intek,satuan_intek,output,jumlah_output,satuan_output,keterangan,1,'$login_ppa_id',NOW(),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'$st_ranap','$template_assesmen_id',nama_template,jumlah_template,st_edited
			FROM `tranap_observasi`

			WHERE assesmen_id='$template_assesmen_id'";
	  // print_r($q);exit;
	  $hasil=$this->db->query($q);
	  $assesmen_id=$this->db->insert_id();
	   $q="UPDATE tranap_observasi 
				SET jumlah_template=jumlah_template+1 WHERE assesmen_id='$template_assesmen_id'";
				$this->db->query($q);
	  $this->output->set_output(json_encode($hasil));
  }
  function save_edit_observasi(){
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_edit_id=$this->input->post('alasan_id');
		$keterangan_edit=$this->input->post('keterangan_edit');
		$jml_edit=$this->db->query("SELECT jml_edit FROM tranap_observasi WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
		// $jml_edit=$this->input->post('jml_edit');
		$res=$this->simpan_history_pews($assesmen_id,$jml_edit);
		// print_r($res);exit;
		if ($res){
			
			$data=array(
				'status_assemen' => 1,
				'st_edited' => 1,
				'alasan_edit_id' =>$alasan_edit_id,
				'keterangan_edit' =>$keterangan_edit,

			);
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('assesmen_id',$assesmen_id);
			$result=$this->db->update('tranap_observasi',$data);
			// $ttv_id=$this->db->insert_id();
			$q="SELECT * from tranap_observasi WHERE assesmen_id='$assesmen_id'";
			$data=$this->db->query($q)->row_array();
			if ($result){
				$hasil=$data;
			}else{
				$hasil=false;
			}
		}else{
			$hasil=$res;
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	function simpan_history_pews($assesmen_id,$jml_edit){
		$hasil=false;
		$this->db->query("DELETE FROM tranap_observasi_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'");
		// $jml_edit=$jml_edit+1;
		$baris=$this->db->query("SELECT COUNT(assesmen_id) as baris FROM tranap_observasi_x_his WHERE assesmen_id='$assesmen_id' AND versi_edit='$jml_edit'")->row('baris');
		// print_r('Jumlah Baris '.$baris.' Edit '.$jml_edit);exit;
		if ($baris==0){
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$q="INSERT IGNORE INTO tranap_observasi_x_his (versi_edit,
				assesmen_id,
				idpasien,pendaftaran_id,pendaftaran_id_ranap,tanggal_input,tingkat_kesadaran,nadi,nafas,spo2,td_sistole,td_diastole,suhu,intek_jenis,intek,satuan_intek,output,jumlah_output,satuan_output,keterangan,status_assemen,created_ppa,created_date,edited_ppa,edited_date,deleted_ppa,deleted_date,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_ranap,template_assesmen_id,nama_template,jumlah_template,st_edited
				)
				SELECT '$jml_edit','$assesmen_id',
				idpasien,pendaftaran_id,pendaftaran_id_ranap,tanggal_input,tingkat_kesadaran,nadi,nafas,spo2,td_sistole,td_diastole,suhu,intek_jenis,intek,satuan_intek,output,jumlah_output,satuan_output,keterangan,status_assemen,created_ppa,created_date,edited_ppa,edited_date,deleted_ppa,deleted_date,alasan_id,keterangan_hapus,alasan_edit_id,keterangan_edit,jml_edit,st_ranap,template_assesmen_id,nama_template,jumlah_template,st_edited
				FROM tranap_observasi
				WHERE assesmen_id='$assesmen_id'";
			// print_r($q);exit;
			$hasil=$this->db->query($q);
		
			
		}
	  return $hasil;
	}
	function index_observasi_perubahan()	{
			$login_ppa_id=$this->session->userdata('login_ppa_id');
			$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
			$login_profesi_id=$this->session->userdata('login_profesi_id');
			
			$logic_akse_observasi=$this->Tpendaftaran_poli_ttv_model->logic_akses_observasi($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
			
			
			$akses_general=$this->db->query("SELECT *FROM setting_observasi")->row_array();
			$data_satuan_observasi=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
						
			$assesmen_id =$this->input->post('id');
			
			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
							SELECT 
							H.versi_edit,H.assesmen_id,tingkat_kesadaran,H.nadi,H.nafas,H.spo2,H.td_sistole,H.td_diastole,H.suhu,H.intek_jenis,H.intek,H.satuan_intek,H.output,H.jumlah_output,H.satuan_output,H.status_assemen,H.keterangan
							,H.alasan_edit_id,H.tanggal_input,H.created_date,H.keterangan_edit,H.edited_date
							,DATEDIFF(NOW(), H.tanggal_input) as selisih,
							mnadi.warna as warna_nadi
							,mnafas.warna as warna_nafas
							,mtd_sistole.warna as warna_sistole
							,mtd_diastole.warna as warna_diastole
							,msuhu.warna as warna_suhu
							,TK.ref as kesadaran
							,UC.nama as nama_created,UE.nama as nama_edited,UC.jenis_profesi_id,malasan_batal.keterangan as ket_edit
							,IJ.ref as jenis_intek_nama,ISA.ref as satuan_intek_nama
							,OJ.ref as jenis_output,OS.ref as satuan_output_nama
							,H.keterangan as ket_observasi
							FROM tranap_observasi_x_his H
							LEFT JOIN merm_referensi OJ ON OJ.nilai=H.output AND OJ.ref_head_id='424'
							LEFT JOIN merm_referensi OS ON OS.nilai=H.satuan_output AND OS.ref_head_id='425'
							LEFT JOIN merm_referensi IJ ON IJ.nilai=H.intek_jenis AND IJ.ref_head_id='423'
							LEFT JOIN merm_referensi ISA ON ISA.nilai=H.satuan_intek AND ISA.ref_head_id='425'
							LEFT JOIN merm_referensi TK ON TK.nilai=H.tingkat_kesadaran AND TK.ref_head_id='23'
							LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
							LEFT JOIN mnafas ON H.nafas BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
							LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
							LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
							LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
							LEFT JOIN mppa UC ON UC.id=H.created_ppa
							LEFT JOIN mppa UE ON UE.id=H.edited_ppa
							LEFT JOIN malasan_batal ON malasan_batal.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id'
							
							
							UNION ALL
							
							SELECT 
							H.jml_edit as versi_edit,H.assesmen_id,tingkat_kesadaran,H.nadi,H.nafas,H.spo2,H.td_sistole,H.td_diastole,H.suhu,H.intek_jenis,H.intek,H.satuan_intek,H.output,H.jumlah_output,H.satuan_output,H.status_assemen,H.keterangan
							,H.alasan_edit_id,H.tanggal_input,H.created_date,H.keterangan_edit,H.edited_date
							,DATEDIFF(NOW(), H.tanggal_input) as selisih,
							mnadi.warna as warna_nadi
							,mnafas.warna as warna_nafas
							,mtd_sistole.warna as warna_sistole
							,mtd_diastole.warna as warna_diastole
							,msuhu.warna as warna_suhu
							,TK.ref as kesadaran
							,UC.nama as nama_created,UE.nama as nama_edited,UC.jenis_profesi_id,malasan_batal.keterangan as ket_edit
							,IJ.ref as jenis_intek_nama,ISA.ref as satuan_intek_nama
							,OJ.ref as jenis_output,OS.ref as satuan_output_nama
							,H.keterangan as ket_observasi
							FROM tranap_observasi H
							LEFT JOIN merm_referensi OJ ON OJ.nilai=H.output AND OJ.ref_head_id='424'
							LEFT JOIN merm_referensi OS ON OS.nilai=H.satuan_output AND OS.ref_head_id='425'
							LEFT JOIN merm_referensi IJ ON IJ.nilai=H.intek_jenis AND IJ.ref_head_id='423'
							LEFT JOIN merm_referensi ISA ON ISA.nilai=H.satuan_intek AND ISA.ref_head_id='425'
							LEFT JOIN merm_referensi TK ON TK.nilai=H.tingkat_kesadaran AND TK.ref_head_id='23'
							LEFT JOIN mnadi ON H.nadi BETWEEN mnadi.nadi_1 AND mnadi.nadi_2 AND mnadi.staktif='1'
							LEFT JOIN mnafas ON H.nafas BETWEEN mnafas.nafas_1 AND mnafas.nafas_2  AND mnafas.staktif='1'
							LEFT JOIN mtd_sistole ON H.td_sistole BETWEEN mtd_sistole.td_1 AND mtd_sistole.td_2  AND mtd_sistole.staktif='1'
							LEFT JOIN mtd_diastole ON H.td_diastole BETWEEN mtd_diastole.td_1 AND mtd_diastole.td_2 AND mtd_diastole.staktif='1'
							LEFT JOIN msuhu ON H.suhu BETWEEN msuhu.suhu_1 AND msuhu.suhu_2  AND msuhu.staktif='1'
							LEFT JOIN mppa UC ON UC.id=H.created_ppa
							LEFT JOIN mppa UE ON UE.id=H.edited_ppa
							LEFT JOIN malasan_batal ON malasan_batal.id=H.alasan_edit_id
							WHERE H.assesmen_id='$assesmen_id'
						) H ORDER BY H.versi_edit ASC
						
						
						
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array();
			$this->column_order  = array();
	  
	  $tingkat_kesadaran_awal='';
	  $nadi_awal='';
	  $nafas_awal='';
	  $td_sistole_awal='';
	  $td_diastole_awal='';
	  $intek_awal='';
	  $jumlah_output_awal='';
	  $suhu_awal='';
	  $warna_sadar='';
	  $warna_normal='';$warna_nadi='';$warna_nafas='';$warna_sistole='';$warna_diastole=''; $warna_suhu='';$warna_tinggi='';$warna_berat='';$warna_ket='';$warna_intek='';$warna_output='';
	  $warna_ket_observasi='';
	  $warna_tidak_normal='red';
	  
      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		 if ($r->versi_edit>0){
			 $warna_sadar=($tingkat_kesadaran_awal != $r->tingkat_kesadaran?$warna_tidak_normal:$warna_normal);
			 $warna_nadi=($nadi_awal != $r->nadi?$warna_tidak_normal:$warna_normal);
			 $warna_nafas=($nafas_awal != $r->nafas?$warna_tidak_normal:$warna_normal);
			 $warna_sistole=($td_sistole_awal != $r->td_sistole?$warna_tidak_normal:$warna_normal);
			 $warna_diastole=($td_diastole_awal != $r->td_diastole?$warna_tidak_normal:$warna_normal);
			 $warna_suhu=($suhu_awal != $r->suhu?$warna_tidak_normal:$warna_normal);
			 if ($intek_awal != $r->intek || $intek_jenis_awal != $r->intek_jenis || $satuan_intek_awal != $r->satuan_intek){
				 $warna_intek=$warna_tidak_normal;
			 }else{
				 $warna_intek=$warna_normal;
				 
			 }
			 if ($output_awal != $r->output || $satuan_output_awal != $r->satuan_output || $jumlah_output_awal != $r->jumlah_output){
				 $warna_output=$warna_tidak_normal;
			 }else{
				 $warna_output=$warna_normal;
				 
			 }
			 
			 $warna_ket_observasi=($ket_observasi_awal != $r->ket_observasi?$warna_tidak_normal:$warna_normal);
			 // $warna_output=($jumlah_output_awal != $r->jumlah_output?$warna_tidak_normal:$warna_normal);
		 }
		 $tingkat_kesadaran_awal=$r->tingkat_kesadaran;
		 $nadi_awal=$r->nadi;
		 $nafas_awal=$r->nafas;
		 $td_sistole_awal=$r->td_sistole;
		 $td_diastole_awal=$r->td_diastole;
		 $suhu_awal=$r->suhu;
		 $intek_awal=$r->intek;
		 $intek_jenis_awal=$r->intek_jenis;
		 $satuan_intek_awal=$r->satuan_intek;
		 
		 $output_awal=$r->output;
		 $satuan_output_awal=$r->satuan_output;
		 $jumlah_output_awal=$r->jumlah_output;
		 $ket_observasi_awal=$r->ket_observasi;
		 $btn_1='';
		  $btn_1 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 85%;">
								
									
									<div class="h2 font-w700 '.($r->alasan_edit_id?'text-danger':'text-primary').' text-center">'.DFormat($r->tanggal_input).'</div>
									<div class="h5 text-muted text-center"> '.tanggal_indo_MY($r->tanggal_input).'</div>
									<div class="h5 text-muted  text-center"> '.HumanTime($r->tanggal_input).'</div>
									
									'.($r->versi_edit>0?'<div class="text-center text-danger push-5-t"><i class="fa fa-pencil text-center"></i> '.$r->ket_edit.' ('.$r->keterangan_edit.')</div>':'<div class="text-center text-primary push-5-t"><i class="si si-anchor text-center"></i> ORIGINAL</div>').'
								</td>
							</tr>
						</tbody>
					</table>';
		
          $result[] = $btn_1;
		  $btn_2='
			<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white" colspan="2">
									<div class="h5 "> Tingkat Kesadaran :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_sadar.'" > '.($r->kesadaran).'</div>
									
								</td>
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Frekuensi Nadi :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_nadi.'" > '.($r->nadi).' '.$data_satuan_observasi['satuan_nadi'].'</div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Frekuensi Nafas :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_nafas.'"> '.($r->nafas).' '.$data_satuan_observasi['satuan_nafas'].'</div>
								</td>
							</tr>
							<tr>
								<td class="bg-white" colspan="2">
									<div class="h5 "> Keterangan :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_ket_observasi.'" > '.($r->ket_observasi).'</div>
								</td>
							</tr>
						</tbody>
					</table>
		  ';
		 
          $result[] = $btn_2;
		  $btn_3 ='<table class="block-table text-left">
						<tbody>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Tekanan Darah :</div>
									<div class="h5 text-muted  push-5-t "> <label  class="h5 text-muted  push-5-t" style="color:'.$warna_sistole.'">'.($r->td_sistole.' '.$data_satuan_observasi['satuan_td'].'</label> / <label class="h5 text-muted  push-5-t" style="color:'.$warna_diastole.'">'.$r->td_diastole).' '.$data_satuan_observasi['satuan_td'].'</label></div>
								</td>
								<td class="bg-white">
									<div class="h5 "> Suhu Tubuh :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_suhu.'"> '.($r->suhu).' '.$data_satuan_observasi['satuan_suhu'].'</div>
								</td>
							</tr>
							<tr>
								<td class="bg-white">
									<div class="h5 "> Intek :</div>
									<div class="h5 text-muted  push-5-t " style="color:'.$warna_intek.'"> '.($r->jenis_intek_nama.' | '.$r->intek.' '.$r->satuan_intek_nama).'</div>
								</td>
								<td class="bg-white">
									<div class="h5 ">Output  :</div>
									<div class="h5 text-muted  push-5-t "> '.($r->jenis_output.' | '.$r->jumlah_output.' '.$r->satuan_output_nama).'</div>
								</td>
							</tr>
						</tbody>
					</table>
				';
		  
          $result[] = $btn_3;
		  $btn_4 ='<table class="block-table text-left">
						<tbody>'.($r->versi_edit==0?'
							<tr>
								<td class="bg-white">
									<div class="h5 "> Created</div>
									<div class=" text-muted  push-5-t "><i class="fa fa-user-md"></i> '.$r->nama_created.' <br>('.HumanDateLong($r->created_date).')</div>
								</td>
								
							</tr>':'
							<tr>
								<td class="bg-white">
									<div class="h5 "> Edited</div>
									<div class=" text-muted  push-5-t "><i class="fa fa-user-md"></i> '.($r->nama_edited?$r->nama_edited.' <br>'.HumanDateLong($r->edited_date):'').'</div>
								</td>
								
							</tr>
							').'
						</tbody>
					</table>
				';
		  
          $result[] = $btn_4;
		  
          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function list_index_template_observasi()
	{
		$assesmen_id=$this->input->post('assesmen_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$assesmen_id=$this->input->post('assesmen_id');
			$this->select = array();
			$from="
					(
						SELECT H.assesmen_id as id,H.nama_template,H.jumlah_template
						 FROM tranap_observasi H WHERE H.created_ppa='$login_ppa_id' AND H.status_assemen='4'
						ORDER BY H.template_id DESC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_template');
			$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$result = array();

		$result[] = $no;
		 $aksi='';
		 $btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		$aksi .= '<button onclick="gunakan_template_assesmen('.$r->id.')" '.$btn_disabel_edit.' type="button" title="Gunakan Template" class="btn btn-warning btn-xs "><i class="fa fa-paste"></i></button>';	
		$aksi .= '<button  onclick="hapus_template_assesmen('.$r->id.')" type="button" title="Hapus Template" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nama_template);
		$result[] = ($r->jumlah_template);
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function edit_template_observasi(){
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$data=array(
			'status_assemen' => 3,
			'pendaftaran_id' => $this->input->post('pendaftaran_id'),
			'pendaftaran_id_ranap' => $this->input->post('pendaftaran_id_ranap'),
			'idpasien' => $this->input->post('idpasien'),
		);

		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tranap_observasi',$data);

		$this->output->set_output(json_encode($result));
	}
	function batal_template_observasi(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $nama_template=$this->input->post('nama_template');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	  if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
	  }
	  if ($nama_template){
		  $data['status_assemen']='4';
		  $data['nama_template']=$nama_template;
	  }else{
		  
		  $data['status_assemen']='0';
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_observasi',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function batal_observasi(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $st_edited=$this->input->post('st_edited');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	   if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
		  
		  
	  }
	  if ($st_edited=='1'){
		  $data['status_assemen']='2';
	  }else{
		  $data['status_assemen']='0';
		  
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tranap_observasi',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function list_history_pengkajian_observasi()
  {
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_observasi($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
	
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$notransaksi=$this->input->post('notransaksi');
		$assesmen_id=$this->input->post('assesmen_id');
		$idrawat_ranap=$this->input->post('idrawat_ranap');
		$idtipe=$this->input->post('idtipe');
		$idruang=$this->input->post('idruang');
		$idbed=$this->input->post('idbed');
		$idkelas=$this->input->post('idkelas');
		$iddokter=$this->input->post('iddokter');
		$idpasien=$this->input->post('idpasien');
		$st_ranap=$this->input->post('st_ranap');
		$where='';
		
		if ($idrawat_ranap != '#'){
			if ($st_ranap=='1'){
				$where .=" AND H.pendaftaran_id_ranap = '".$idrawat_ranap."'";
			}else{
				$where .=" AND H.pendaftaran_id = '".$idrawat_ranap."'";
			}
		}
		if ($idruang != '#'){
			$where .=" AND MP.idruangan = '".$idruang."'";
		}
		if ($idkelas != '#'){
			$where .=" AND MP.idkelas = '".$idkelas."'";
		}
		if ($idbed != '#'){
			$where .=" AND MP.idbed = '".$idbed."'";
		}
		if ($notransaksi != ''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		if ($iddokter != '#'){
			$where .=" AND MP.iddokterpenanggungjawab = '".$iddokter."'";
		}
		
		if ($mppa_id != '#'){
			$where .=" AND H.created_ppa = '".$mppa_id."'";
		}
		if ($tanggal_input_1  != ''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1  != ''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		$akses_general=$this->db->query("SELECT *FROM setting_observasi")->row_array();
		// print_r($akses_general);exit;
		if ($st_ranap=='1'){
			$group_by=' GROUP BY H.pendaftaran_id_ranap,H.st_ranap';
		}else{
			$group_by=' GROUP BY H.pendaftaran_id,H.st_ranap';
			
		}
		$this->select = array();
		$from="
				(
					SELECT 
					DATEDIFF(NOW(), H.tanggal_input) as selisih,H.created_ppa,H.created_date,mppa.nama as nama_mppa,ppa_edit.nama as nama_edit,H.edited_date
					,H.keterangan_edit,H.jml_edit,H.st_edited,H.assesmen_id
					,CASE WHEN H.st_ranap='1' THEN MP.nopendaftaran ELSE RJ.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='1' THEN MP.tanggaldaftar ELSE RJ.tanggaldaftar END as tanggaldaftar
					,CASE WHEN H.st_ranap='1' THEN MP.idtipe ELSE RJ.idtipe END as idtipe
					,CASE WHEN MP.idtipe = 1 THEN 'Rawat Inap' ELSE 'ODS' END as idtipe_nama
					,MR.nama as nama_ruangan,MK.nama as nama_kelas,MB.nama as nama_bed
					,H.pendaftaran_id,H.pendaftaran_id_ranap,H.tanggal_input as tanggal_pengkajian
					,COUNT(H.assesmen_id) as jumlah_pengkajian,H.st_ranap
					,MPOL.nama as nama_poli
					FROM `tranap_observasi` H
					LEFT JOIN trawatinap_pendaftaran MP ON MP.id=H.pendaftaran_id_ranap  AND H.st_ranap='1'
					LEFT JOIN tpoliklinik_pendaftaran RJ ON RJ.id=H.pendaftaran_id AND H.st_ranap='0'
					LEFT JOIN mpoliklinik MPOL ON RJ.idpoliklinik=MPOL.id 
					LEFT JOIN mruangan MR ON MR.id=MP.idruangan
					LEFT JOIN mkelas MK ON MK.id=MP.idkelas
					LEFT JOIN mbed MB ON MB.id=MP.idbed
					LEFT JOIN mppa ON mppa.id=H.created_ppa
					LEFT JOIN mppa ppa_edit ON ppa_edit.id=H.edited_ppa
					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
					".$group_by."
					ORDER BY H.created_date DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		// $btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_history_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		if ($r->st_ranap=='1'){
		$btn_edit='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tobservasi/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_ri/input_observasi"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></a>';
		}else{
		$btn_edit='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tobservasi/tindakan/'.$r->pendaftaran_id.'/0/erm_ri/input_observasi"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-pencil"></i></a>';
			
		}
		$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_assesmen_all('.$r->pendaftaran_id_ranap.')" type="button"><i class="fa fa-copy text-success"></i></button>';
		$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_history_assesmen_all('.$r->pendaftaran_id_ranap.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		if ($r->st_ranap=='1'){
		$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tobservasi/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_ews/input_observasi/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}else{
		$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tobservasi/tindakan/'.$r->pendaftaran_id.'/0/erm_ews/input_observasi/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}
		
		 if ($akses_general['st_edit_catatan']=='0'){
			  $btn_edit='';
		  }else{
			  if ($akses_general['lama_edit']>0){
				  if ($r->selisih>$akses_general['lama_edit']){
					  $btn_edit='';
				  }else{
					  
				  }
				  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id != $r->created_ppa){
							$btn_edit='';
					  }
				  }
			  }
		  }
		  if ($akses_general['st_duplikasi_catatan']=='0'){
			  $btn_duplikasi='';
		  }else{
			  if ($akses_general['lama_duplikasi']>0){
				  if ($r->selisih>$akses_general['lama_duplikasi']){
					  $btn_duplikasi='';
				  }else{
					 
				  }
			  }
			   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id != $r->created_ppa){
						$btn_duplikasi='';
				  }
			   }
		  }
		  if ($akses_general['st_hapus_catatan']=='0'){
			  $btn_hapus='';
		  }else{
			  if ($akses_general['lama_hapus']>0){
				  if ($r->selisih>$akses_general['lama_hapus']){
					  $btn_hapus='';
				  }else{
					 
				  }
				   
			  }
			  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id <> $r->created_ppa){
						$btn_hapus='';
				  }else{
					  
				  }
						// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
			  }
		  }
		if ($logic_akses_assesmen['st_edit_observasi']=='0'){
		  $btn_edit='';
		}
		if ($logic_akses_assesmen['st_hapus_observasi']=='0'){
		  $btn_hapus='';
		}
		
		$result[] = $no;
		$aksi='';
		$aksi_edit='';
		
		if ($r->jml_edit>0){
			// $aksi_edit='<a href="'.base_url().'tpendaftaran_ranap_erm/tindakan/'.$r->pendaftaran_id_ranap.'/erm_ri/his_observasi/'.$r->assesmen_id.'/'.$r->jml_edit.'" class="btn btn-xs btn-warning"  title="Info Edit"><i class="fa fa-info"></a>';
		}
		$aksi = '<div class="btn-group">';
		$aksi .= $btn_edit;	
		// $aksi .= $btn_duplikasi;	
		// $aksi .= $btn_hapus;	
		$aksi .= $btn_lihat;	
		$aksi .= $aksi_edit;	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = HumanDateLong($r->tanggaldaftar);
		$result[] = ($r->nopendaftaran).' '.($r->jumlah_pengkajian>0?'<br>'.text_danger($r->jumlah_pengkajian.' Pengkajian'):'');
		if ($r->st_ranap=='1'){
			$result[] = ('<strong>'.$r->nama_ruangan.'</strong>'.($r->nama_kelas?' - '.$r->nama_kelas:'').($r->nama_bed?' - '.$r->nama_bed:''));
		}else{
			$result[] = ($idtipe=='1'?'Poliklinik':'IGD').' - '.'<strong>'.$r->nama_poli.'</strong>';
		}
		$result[] = HumanDateLong($r->tanggal_pengkajian);
		$result[] = ($r->nama_mppa);
		$result[] = ($r->nama_edit?$r->nama_edit.'<br>'.HumanDateLong($r->edited_date).'<br>'.text_default($r->keterangan_edit):'');
		$data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function list_balance(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$data_satuan_observasi=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan")->row_array();
		$where='';
					
		$pendaftaran_id_ranap =$this->input->post('pendaftaran_id_ranap');
		$pendaftaran_id =$this->input->post('pendaftaran_id');
		$st_waktu =$this->input->post('st_waktu');
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$shift_id =$this->input->post('shift_id');
		$waktu_1 =$this->input->post('waktu_1');
		$waktu_2 =$this->input->post('waktu_2');
		$st_ranap =$this->input->post('st_ranap');
		if ($st_ranap=='1'){
			$where .=" AND H.pendaftaran_id_ranap='$pendaftaran_id_ranap' AND H.st_ranap='$st_ranap'";
		}else{
			$where .=" AND H.pendaftaran_id='$pendaftaran_id'  AND H.st_ranap='$st_ranap'";
			
		}
		if ($tanggal_1  != ''){
			$where .=" AND DATE(H.tanggal_input) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal_input) <='".YMDFormat($tanggal_2)."'";
		}else{
			// $where .=" AND DATE(H.tanggal_input) >='".date('Y-m-d')."'";
		}
		if ($st_waktu=='1'){
			if ($waktu_1 !=''){
				$where .=" AND (TIME(H.tanggal_input) BETWEEN '$waktu_1' AND '$waktu_2')";
				// print_r($where);exit;
			}
		}
		$q="
			SELECT H.assesmen_id,H.tanggal_input,mhari.nama_hari,H.intek as jumlah_intek
			,IJ.ref as jenis_intek_nama,ISA.ref as satuan_intek_nama
			,OJ.ref as jenis_output_nama,OS.ref as satuan_output_nama,H.jumlah_output
			,H.keterangan,COALESCE(H.intek,0)-COALESCE(H.jumlah_output,0) as balance
			,UC.nama as nama_created,H.created_date
			FROM tranap_observasi H
			LEFT JOIN merm_referensi OJ ON OJ.nilai=H.output AND OJ.ref_head_id='424'
			LEFT JOIN merm_referensi OS ON OS.nilai=H.satuan_output AND OS.ref_head_id='425'
			LEFT JOIN merm_referensi IJ ON IJ.nilai=H.intek_jenis AND IJ.ref_head_id='423'
			LEFT JOIN merm_referensi ISA ON ISA.nilai=H.satuan_intek AND ISA.ref_head_id='425'
			LEFT JOIN date_row ON date_row.tanggal=DATE(H.tanggal_input)
			LEFT JOIN mhari ON mhari.id=date_row.hari
			LEFT JOIN mppa UC ON UC.id=H.created_ppa
			WHERE H.status_assemen='2' ".$where."
			ORDER BY H.tanggal_input ASC
		";
		// print_r($q);exit;
		 $list_param=$this->db->query($q)->result();
		 $total_intek=0;$total_output=0;$total_balance=0;
		 if ($list_param){
				$tabel='<table class="table table table-bordered table-condensed" id="index_his">';
				$tabel .='<thead>';
				$header='<tr>';
				$header .='<th class="text-center" style="width:5%;vertical-align: middle;"><strong>NO</strong></th>';
				$header .='<th class="text-center" style="width:250px;vertical-align: middle;"><strong>PARAMETER</strong></th>';
				$jml_kolom=0;
				foreach($list_param as $r){
				  $header .='<th class="text-center" style="width:150px;vertical-align: middle;"><strong>';
					$header .='<span class="text-center">'.$r->nama_hari.' '.HumanDateLong($r->tanggal_input).'</span></strong> ';
					$header .='<br><span class="text-center text-primary text-nama">ADD BY : '.$r->nama_created.'<br>'.HumanDateLong($r->created_date).'</span>';
					$header .='</th>';
					$jml_kolom =$jml_kolom+1;
				}
				  
				$tabel .=$header;
				$tabel .='</tr>';
				$tabel .='</thead>';
				$tabel .='<tbody>';
				$row_intex='
					<td class="text-center">1.</td>
					<td class="text-left"><strong>INTEK</strong></td>
					
				';
				foreach($list_param as $r){
					$row_intex .='<td>
									<div class="input-group">
										<span class="input-group-addon">'.$r->jenis_intek_nama.'</span>
										<input class="form-control decimal " readonly type="text" value="'.$r->jumlah_intek.'">
										<span class="input-group-addon">'.$r->satuan_intek_nama.'</span>
									</div>
								</td>';
				}
				$row_intex ='<tr>'.$row_intex.'</tr>';
				$tabel.=$row_intex;
				
				$row_output='
					<td class="text-center">2.</td>
					<td class="text-left"><strong>OUTPUT</strong></td>
					
				';
				foreach($list_param as $r){
					$row_output .='<td>
									<div class="input-group">
										<span class="input-group-addon">'.$r->jenis_output_nama.'</span>
										<input class="form-control decimal " readonly type="text" value="'.$r->jumlah_output.'">
										<span class="input-group-addon">'.$r->satuan_output_nama.'</span>
									</div>
								</td>';
				}
				$row_output ='<tr>'.$row_output.'</tr>';
				$tabel.=$row_output;
				
				$row_balance='
					<td class="text-center">3.</td>
					<td class="text-left"><strong>BALANCE</strong></td>
					
				';
				foreach($list_param as $r){
					$row_balance .='<td>
									<div class="input-group">
										<span class="input-group-addon"><strong>'.$this->label_plus($r->jumlah_intek,$r->jumlah_output).'</strong></span>
										<input class="form-control decimal " readonly type="text" value="'.($r->balance<0?$r->balance*-1:$r->balance).'">
										
									</div>
								</td>';
				}
				$row_balance ='<tr>'.$row_balance.'</tr>';
				$tabel.=$row_balance;
				
				$row_ket='
					<td class="text-center">4.</td>
					<td class="text-left"><strong>KETERANGAN</strong></td>
					
				';
				foreach($list_param as $r){
					$row_ket .='<td class="text-center">
									<strong>'.$r->keterangan.'</strong>
								</td>';
				}
				$row_ket ='<tr>'.$row_ket.'</tr>';
				$tabel.=$row_ket;
				
				
				$tabel.='</tbody>';
				$tabel.='</table>';
				
				foreach($list_param as $r){
					$total_intek =$total_intek + $r->jumlah_intek;
					$total_output =$total_output + $r->jumlah_output;
					$total_balance =$total_balance + $r->balance;
				}
				
		 }else{
			 $tabel='
				<div class="form-group">
					<div class="col-md-12 ">
						<div class="col-md-12 ">
							<div class="alert alert-warning alert-dismissable" id="peringatan_assesmen" style="display:block">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<p>Info <a class="alert-link" href="javascript:void(0)">BELUM ADA DATA OBSERVASI</a>!</p>
							</div>
						</div>
						
					</div>
				</div>
			 ';
			 
		 }
		 $arr['total_intek']=$total_intek;
		 $arr['total_output']=$total_output;
		 $arr['total_balance']=$total_balance;
		 $arr['tabel']=$tabel;
		$this->output->set_output(json_encode($arr));
  }
  function label_plus($intek,$output){
	  if ($intek > $output){
		  return '+';
	  }
	  if ($intek < $output){
		  return '-';
	  }
	  if ($intek == $output){
		  return '';
	  }
  }
}	
