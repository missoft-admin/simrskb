<?php
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
class Mperiode_akun extends CI_Controller
{
	/**
	 * Periode Akuntansi controller.
	 * Developer @gunalirezqimauludi
	 */

	public function __construct()
	{
		// print_r('sini');exit();
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mperiode_akun_model', 'model');
		$this->load->model('Mkategori_akun_model');
	}
	public function create_saldo()
    {		
        $tanggal_saldo =YMDFormat($this->input->post('tanggal_saldo'));
        $periode =YMFormat($this->input->post('tanggal_saldo'));
		
		$data =array(
            'tanggal_saldo'=>$tanggal_saldo,
            'st_awal'=>1,            
            'periode'=>$periode,            
            'created_userid'=>$this->session->userdata('user_id'),
            'created_username'=>$this->session->userdata('user_name'),
            'created_date'=>date('Y-m-d H:i:s'),
        );
		$this->db->insert('mperiode_akun',$data);
		$idperiode=$this->db->insert_id();
		$rows=$this->db->query("SELECT * FROM `makun_nomor` H WHERE H.`status`='1'")->result();
		foreach($rows as $row){
			$data=array(
				'idperiode' => $idperiode,
				'idakun' =>$row->id,
				'idkategori' => $row->idkategori,
				'idtipe' => $row->idtipe,
				'noheader' => $row->noheader,
				'noakun' => $row->noakun,
				'level' => $row->level,
				'namaakun' => $row->namaakun,
				'debet_awal' => 0,
				'kredit_awal' => 0,
				'debet_trx' => 0,
				'kredit_trx' => 0,
				'debet_akhir' => 0,
				'kredit_akhir' => 0,
				'possaldo' => $row->possaldo,
				'poslaporan' => $row->poslaporan,
				'bertambah' => $row->bertambah,
				'berkurang' => $row->berkurang,
				'tanggal_saldo' => $tanggal_saldo,
			);
			$result=$this->db->insert('mperiode_akun_saldo',$data);
		}
		
		
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function index()
	{
		$q="SELECT COUNT(H.id) as jml FROM mperiode_akun H WHERE H.st_aktif='1'";
        $data = [
			
        ];

		$data['error'] = '';
		$data['title'] = 'Periode Akuntansi';
		$data['content'] = 'Mperiode_akun/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', 'dashboard'],
			['Akuntansi', '#'],
			['Periode Akuntansi', 'Mperiode_akun'],
			['List', '']
		];

		$data['jml'] = $this->db->query($q)->row('jml');
		$data['list_index'] = $this->model->getAll();
		$data['list_header'] = $this->Mkategori_akun_model->list_header();
		$data['list_akun'] = $this->Mkategori_akun_model->list_akun();
		$data['list_kategori'] = $this->Mkategori_akun_model->list_kategori();
		// print_r($data['list_akun']);exit();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function load_index()
    {
		$iduser=$this->session->userdata('user_id');
				
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		$where='';
      
		$from="(SELECT *FROM mperiode_akun ORDER BY tanggal_saldo DESC
				) as tbl";
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
			
            $row[] = get_periode($r->periode);
            $row[] = HumanDateShort($r->tanggal_saldo);
            $row[] = ($r->st_aktif=='1'?text_success('ACTIVE'):text_danger('NOT ACTIVE'));
            $row[] = ($r->st_tutup=='0'?text_success('OPENED'):text_danger('CLOSED'));
           
			$aksi   = '<div class="btn-group">';
			$aksi .= '<a href="'.base_url().'mperiode_akun/view/'.$r->id.'" class="btn btn-xs btn-default" data-toggle="tooltip" title="Lihat Akun"><i class="fa fa-eye"></i></a>';
			if ($r->st_tutup=='0'){
				$aksi .= '<a href="'.base_url().'mperiode_akun/detail/'.$r->id.'" class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit Saldo Akun"><i class="fa fa-list"></i></a>';				
				$aksi .= '<a href="'.base_url().'mperiode_akun/tutup_buku/'.$r->id.'" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Edit Saldo Akun"><i class="si si-logout"></i> Tutup Buku</a>';				
			}
			
			$aksi.='</div>';
			$row[] = $aksi;
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	public function detail($id,$disabel='')
	{
        $row=$this->model->getSpecified($id);
		$data['idperiode'] = $id;
		$data['periode_nama'] = get_periode($row->periode);
		if ($row->st_tutup=='1'){
			$data['st_aktif'] = 1;
		}else{
			if ($row->st_awal=='1'){
				$data['st_aktif'] = 0;
			}else{
				$data['st_aktif'] = $row->st_aktif;
			}
		}
		
		$data['error'] = '';
		$data['title'] = 'Periode Akuntansi [Atur Saldo]';
		$data['content'] = 'Mperiode_akun/index_saldo';
		$data['breadcrum'] = [
			['RSKB Halmahera', 'dashboard'],
			['Akuntansi', '#'],
			['Periode Akuntansi', 'Mperiode_akun'],
			['List', '']
		];

		$data['list_index'] = $this->model->getAll();
		$data['list_header'] = $this->Mkategori_akun_model->list_header();
		$data['list_akun'] = $this->Mkategori_akun_model->list_akun();
		$data['list_kategori'] = $this->Mkategori_akun_model->list_kategori();
		// print_r($data['list_akun']);exit();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	public function view($id)
	{
        $row=$this->model->getSpecified($id);
		$data['idperiode'] = $id;
		$data['periode_nama'] = get_periode($row->periode);
		$data['st_aktif'] = 1;
		$data['error'] = '';
		$data['title'] = 'Periode Akuntansi [Atur Saldo]';
		$data['content'] = 'Mperiode_akun/index_saldo';
		$data['breadcrum'] = [
			['RSKB Halmahera', 'dashboard'],
			['Akuntansi', '#'],
			['Periode Akuntansi', 'Mperiode_akun'],
			['List', '']
		];

		$data['list_index'] = $this->model->getAll();
		$data['list_header'] = $this->Mkategori_akun_model->list_header();
		$data['list_akun'] = $this->Mkategori_akun_model->list_akun();
		$data['list_kategori'] = $this->Mkategori_akun_model->list_kategori();
		// print_r($data['list_akun']);exit();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function load_detail()
    {
		
		$idperiode     		= $this->input->post('idperiode');
		$idkategori     		= $this->input->post('idkategori');		
		$headerakun     	= $this->input->post('headerakun');
		if ($headerakun){
			$headerakun=implode(',', $headerakun);			
		}
		$noakun     	= $this->input->post('noakun');
		if ($noakun){
			$noakun=implode(',', $noakun);			
		}
		$iduser=$this->session->userdata('user_id');
				
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		$where='';
       
		if ($idkategori !='#'){
			$where .=" AND H.idkategori='$idkategori'";
		}
        if ($headerakun){
			$where .=" AND H.noheader IN (".$headerakun.")";
		}
		if ($noakun){
			$where .=" AND H.id IN (".$noakun.")";
		}
		$from="(
			SELECT
                H.id,
                CONCAT( akun_header.noakun, ' - ', akun_header.namaakun ) AS noheader,
                H.noakun,
                H.namaakun,
                H.level,
                mkategori_akun.nama AS namakategori
                ,H.debet_awal as debet,H.kredit_awal as kredit
            FROM
                mperiode_akun_saldo H
                LEFT JOIN mperiode_akun_saldo akun_header ON akun_header.noakun = H.noheader 
                LEFT JOIN mkategori_akun mkategori_akun ON mkategori_akun.id = H.idkategori 
            WHERE
                H.idperiode='$idperiode' ".$where."
            GROUP BY H.id
            ORDER BY
                H.noakun ASC
				) as tbl";
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('noakun','namaakun','namakategori');
        $this->column_order    = array();
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $r->id;
            $row[] = $no;
			
            $row[] = $r->noheader;
            $row[] = $r->noakun;
            $row[] = TreeView($r->level, $r->namaakun);
            $row[] = $r->namakategori;
            $row[] = number_format($r->debet,2, ".", ",");
            $row[] = number_format($r->kredit,2, ".", ",");
			
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function get_data_saldo($id){
		$q="SELECT H.id,H.noakun,H.namaakun,H.debet_awal,H.kredit_awal			
			FROM mperiode_akun_saldo H 
			WHERE H.id='$id'";
		$arr=$this->db->query($q)->row_array();		
		$this->output->set_output(json_encode($arr));

	}
	public function simpan_debet()
    {		
        $idperiode = $this->input->post('idperiode');
        $idedit = $this->input->post('idedit');
        $nominal =RemoveComma($this->input->post('nominal'));
		$data =array(
            'debet_awal'=>$nominal,
        );
		$this->db->where('id',$idedit);
		$arr=$this->db->update('mperiode_akun_saldo',$data);
		// $arr=$this->get_total_DK($idperiode);
		$this->output->set_output(json_encode($arr));
    }
	function get_total_DK($idperiode){
		
		$q="SELECT SUM(H.debet_awal) as tot_debet,SUM(H.kredit_awal) as tot_kredit FROM mperiode_akun_saldo H WHERE H.idperiode='$idperiode'";
		$result= $this->db->query($q)->row();
		$arr['tot_debet']=$result->tot_debet;
		$arr['tot_kredit']=$result->tot_kredit;
		$this->output->set_output(json_encode($arr));
	}
	public function simpan_kredit()
    {		
        $idperiode = $this->input->post('idperiode');
        $idedit = $this->input->post('idedit');
        $nominal =RemoveComma($this->input->post('nominal'));
        
		$data =array(
            'kredit_awal'=>$nominal,
        );
		$this->db->where('id',$idedit);
		$arr=$this->db->update('mperiode_akun_saldo',$data);
		// $arr=$this->get_total_DK($idperiode);
		$this->output->set_output(json_encode($arr));
        // if ($result) {
            // $this->output->set_output(json_encode($result));
        // } else {
            // $this->output->set_output(json_encode($result));
        // }
    }
	public function simpan_posting()
    {		
        $idperiode = $this->input->post('idperiode');
        
        
		$data =array(
            'st_aktif'=>'1',
            'posted_date'=> date('Y-m-d H:i:s'),
            'posted_userid'=> $this->session->userdata('user_id'),
            'posted_username'=>$this->session->userdata('user_name'),
        );
		$this->db->where('id',$idperiode);
		$arr=$this->db->update('mperiode_akun',$data);
		$this->output->set_output(json_encode($arr));
    }
}
?>