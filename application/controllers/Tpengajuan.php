<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpengajuan extends CI_Controller
{

    /**
     * Pengajuan controller.
     * Developer @gunalirezqimauludi & @muhamadali
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Tpengajuan_model', 'model');
    }

    /* it's right function &start */
    public function index()
    {
        $data = array();
        $data['error'] = '';
        $data['title'] = 'Pengajuan';
        $data['content'] = 'Tpengajuan/index';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Pengajuan", '#'),
            array("List", 'tpengajuan')
        );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
    /* it's right function &end */

    /* it's right function &start */
    public function create()
    {
        $data = array(
            'id' => '',
            'tanggal' => date("d/m/Y"),
            'idpemohon' => $this->session->userdata('user_id'),
            'namapemohon' => $this->session->userdata('user_name'),
            'subjek' => '',
            'untukbagian' => '',
            'diperlukanuntuk' => '',
            'tanggaldibutuhkan' => '',
            'jenispembayaran' => '',
            'idvendor' => '',
            'tanggalkontrabon' => date("d/m/Y"),
            'norekening' => '',
            'bank' => '',
            'downpayment' => '',
            'tanggaldownpayment' => date("d/m/Y"),
            'cicilan' => '',
            'nominalcicilan' => '',
            'fakturpajak' => '',
            'totalnominal' => 0,
            'stproses' => '',
            'status' => ''
        );

        $data['error'] = '';
        $data['title'] = 'Tambah Pengajuan';
        $data['content'] = 'Tpengajuan/manage';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Pengajuan", '#'),
            array("Tambah", 'tpengajuan')
        );

        $data['list_pegawai'] = $this->model->getPegawai();
        $data['list_unitpelayanan'] = $this->model->getUnitPelayanan();
        $data['list_keperluan'] = $this->model->getJenisKeperluan();
        $data['list_vendor'] = $this->model->getVendor();
        $data['list_satuan'] = $this->model->getSatuan();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
    /* it's right function &end */

    # check
    public function update($id)
    {
        if ($id !== '') {
            $pengajuan = $this->model->getDetailPengajuan($id);

            $data = array(
                'id' => $pengajuan->id,
                'tanggal' => date_format(date_create($pengajuan->tanggal), "d/m/Y"),
                'idpemohon' => $pengajuan->idpemohon,
                'namapemohon' => $pengajuan->namapemohon,
                'subjek' => $pengajuan->subjek,
                'untukbagian' => $pengajuan->untukbagian,
                'diperlukanuntuk' => $pengajuan->diperlukanuntuk,
                'tanggaldibutuhkan' => $pengajuan->tanggaldibutuhkan,
                'jenispembayaran' => $pengajuan->jenispembayaran,
                'idvendor' => $pengajuan->idvendor,
                'tanggalkontrabon' => $pengajuan->tanggalkontrabon,
                'norekening' => $pengajuan->norekening,
                'bank' => $pengajuan->bank,
                'fakturpajak' => $pengajuan->fakturpajak,
                'tanggaldownpayment' => date_format(date_create($pengajuan->tanggaldownpayment), "d/m/Y"),
                'downpayment' => $pengajuan->downpayment,
                'cicilan' => $pengajuan->cicilan,
                'nominalcicilan' => $pengajuan->nominalcicilan,
                'stproses' => $pengajuan->stproses,
                'status' => $pengajuan->status,
                'memo' => $pengajuan->memo,
                'idpersetujuan' => $pengajuan->idpersetujuan
            );

            $data['error'] = '';
            $data['title'] = 'Ubah Pengajuan';
            $data['content'] = 'Tpengajuan/manage';
            $data['breadcrum'] = array(
                array("RSKB Halmahera", '#'),
                array("Pengajuan", '#'),
                array("Ubah", 'tpengajuan')
            );

            $data['list_pegawai'] = $this->model->getPegawai();
            $data['list_unitpelayanan'] = $this->model->getUnitPelayanan();
            $data['list_keperluan'] = $this->model->getJenisKeperluan();
            $data['list_vendor'] = $this->model->getVendor();
            $data['list_satuan'] = $this->model->getSatuan();
            $data['list_detail'] = $this->model->getDetailBarang($pengajuan->id);
            $data['list_termin_progress'] = $this->model->getDetailTerminProgress($pengajuan->id);
            $data['list_termin_fix'] = $this->model->getDetailTerminFix($pengajuan->id);
            $data['list_lampiran'] = $this->model->getDetailLampiran($pengajuan->id);

            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('tpengajuan', 'location');
        }
    }

    # check
    public function delete($id)
    {
        if ($this->model->softDelete($id)) {
            $status = array('status' => 200);
            $this->output->set_output(json_encode($status));
        } else {
            $status = array('status' => 500);
            $this->output->set_output(json_encode($status));
        }
    }

    # check
    public function save()
    {
        if ($this->input->post('id') == '') {
            if ($this->model->saveData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                redirect('tpengajuan', 'location');
            }
        } else {
            if ($this->model->updateData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                redirect('tpengajuan', 'location');
            }
        }
    }

    # check
    public function aktivasibendahara($id)
    {
        if ($id != '') {
            $row = $this->model->getPengajuanDetail($id);

            if (isset($row->id)) {
                $data = array(
                    'id' => $row->id,
                    'nopengajuan' => $row->nopengajuan,
                    'tanggaldibutuhkan' => $row->tanggaldibutuhkan,
                    'idvendor' => $row->idvendor,
                    'namavendor' => $row->namavendor,
                    'jenispembayaran' => $row->jenispembayaran,
                    'norekening' => $row->norekening,
                    'bank' => $row->bank,
                    'tanggalkontrabon' => $row->tanggalkontrabon,
                    'stdownpayment' => ($row->downpayment ? 'Aktif' : 'Tidak Aktif'),
                    'downpayment' => number_format($row->downpayment),
                    'cicilan' => number_format($row->cicilan),
                    'subjek' => $row->subjek,
                    'stproses' => $row->stproses,
                );

                $data['error'] = '';
                $data['title'] = 'Persetujuan Pengajuan';
                $data['content'] = 'Tpengajuan/aktivasi';
                $data['breadcrum'] = array(
                    array("RSKB Halmahera", '#'),
                    array("Pengajuan", '#'),
                    array("Persetujuan", 'tpengajuan')
                );

                $data['stepPersetujuan'] = 3;
                $data['labelPersetujuan'] = 'Unit Yang Memproses';

                $data['list_user'] = $this->model->getUsers($id);
                $data['list_user_bendahara'] = $this->model->getUsersBendahara();
                $data['list_satuan'] = $this->model->getSatuan();
                $data['list_detail'] = $this->model->getDetailBarang($row->id);
                $data['list_termin_progress'] = $this->model->getDetailTerminProgress($row->id);
                $data['list_termin_fix'] = $this->model->getDetailTerminFix($row->id);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('tpengajuan', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('tpengajuan');
        }
    }

    public function ulasan($id)
    {
        $pengajuan = $this->model->getDetailPengajuan($id);

        $data = array(
            'id' => $pengajuan->id,
            'tanggal' => date_format(date_create($pengajuan->tanggal), "d/m/Y"),
            'idpemohon' => $pengajuan->idpemohon,
            'namapemohon' => $pengajuan->namapemohon,
            'subjek' => $pengajuan->subjek,
            'untukbagian' => $pengajuan->untukbagian,
            'diperlukanuntuk' => $pengajuan->diperlukanuntuk,
            'tanggaldibutuhkan' => $pengajuan->tanggaldibutuhkan,
            'jenispembayaran' => $pengajuan->jenispembayaran,
            'idvendor' => $pengajuan->idvendor,
            'tanggalkontrabon' => $pengajuan->tanggalkontrabon,
            'norekening' => $pengajuan->norekening,
            'bank' => $pengajuan->bank,
            'fakturpajak' => $pengajuan->fakturpajak,
            'tanggaldownpayment' => date_format(date_create($pengajuan->tanggaldownpayment), "d/m/Y"),
            'downpayment' => $pengajuan->downpayment,
            'cicilan' => $pengajuan->cicilan,
            'nominalcicilan' => $pengajuan->nominalcicilan,
            'stproses' => $pengajuan->stproses,
            'status' => $pengajuan->status,
            'memo' => $pengajuan->memo,
            'idpersetujuan' => $pengajuan->idpersetujuan
        );

        $data['error'] = '';
        $data['title'] = 'Review Pengajuan';
        $data['content'] = 'Tpengajuan/ulasan';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Pengajuan", '#'),
            array("Review", 'tpengajuan')
        );

        $data['list_jenispembayaran'] = array('Tunai', 'Kontrabon', 'Transfer', 'Termin By Progress', 'Termin Fix (Cicilan)');
        $data['list_unitpelayanan'] = $this->model->getUnitPelayanan();
        $data['list_keperluan'] = $this->model->getJenisKeperluan();
        $data['list_vendor'] = $this->model->getVendor();
        $data['list_detail'] = $this->model->getDetailBarang($id);
        $data['list_termin_progress'] = $this->model->getDetailTerminProgress($id);
        $data['list_termin_fix'] = $this->model->getDetailTerminFix($id);
        $data['list_lampiran'] = $this->model->getDetailLampiran($id);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function penolakan($id, $idpersetujuan_detail = null)
    {
        if ($id != '') {
            $row = $this->model->getPengajuanDetail($id);
            $persetujuan = $this->model->getPersetujuanDetail($id, $idpersetujuan_detail);
            $alasan = $persetujuan[0]->alasan_ == "" ? $persetujuan[0]->alasan : $persetujuan[0]->alasan_;

            if (isset($row->id)) {
                $data = array(
                    'id' => $row->id,
                    'nopengajuan' => $row->nopengajuan,
                    'subjek' => $row->subjek,
                    'stproses' => $row->stproses,
                    'alasan' => $alasan
                );

                $data['error'] = '';
                $data['title'] = 'Alasan Penolakan';
                $data['content'] = 'Tpengajuan/penolakan';
                $data['breadcrum'] = array(
                    array("RSKB Halmahera", '#'),
                    array("Pengajuan", '#'),
                    array("Alasan Penolakan", 'tpengajuan')
                );

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('tpengajuan', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('tpengajuan');
        }
    }

    public function saveUlasan()
    {
        if ($this->model->saveUlasan()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('tpengajuan', 'location');
        }
    }

    public function saveAktivasiBendahara()
    {
        if ($this->model->saveAktivasiBendahara()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('tpengajuan', 'location');
        }
    }

    public function savePenolakan()
    {
        $this->model->savePenolakan();
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah disimpan.');
        redirect('tpengajuan', 'location');
    }

    // Shortcut Master Data
    public function getJenisKeperluan()
    {
        $data = get_all('mjenis_keperluan', array('status' => 1));
        $this->output->set_output(json_encode($data));
    }

    public function saveDataJenisKeperluan()
    {
        $data = array();
        $data['nama'] = $this->input->post('namakeperluan');

        if ($this->db->insert('mjenis_keperluan', $data)) {
            return true;
        }
    }

    public function getVendor()
    {
        $data = get_all('mvendor', array('status' => 1));
        $this->output->set_output(json_encode($data));
    }

    public function saveDataVendor()
    {
        $data = array();
        $data['nama'] = $this->input->post('nama');
        $data['alamat'] = $this->input->post('alamat');
        $data['telepon'] = $this->input->post('telepon');
        $data['norekening'] = $this->input->post('norekening');
        $data['atasnama'] = $this->input->post('atasnama');
        $data['bank'] = $this->input->post('bank');

        if ($this->db->insert('mvendor', $data)) {
            return true;
        }
    }

    public function getIndex()
    {
        $sessionUserId = $this->session->userdata('user_id');
        $sessionUserRole = $this->session->userdata('user_idpermission');

        $result = $this->model->getIndexPengajuan($sessionUserId);

        $data = array();
        $no = $_POST['start'];
        foreach ($result as $row) {
            $action = "";

            $no++;
            $rows = array();


            $assigment = $this->getPenyetuju($row->id);
            $check = $this->model->checkStatusPersetujuan($sessionUserId, $row->id);

            if (!empty($check)) {
                $stpersetujuan = $check->status_persetujuan_ == 0 ? $check->status_persetujuan : $check->status_persetujuan_;
            }

            $total = $this->model->getTotalPersetujuan($row->id);
            $disetujui = $this->model->getTotalPersetujuan($row->id, 1);
            $ditolak = $this->model->getTotalPersetujuan($row->id, 2);

            $disetujui2 = $this->model->getTotalPersetujuan2($row->id, 1);
            $ditolak2 = $this->model->getTotalPersetujuan2($row->id, 2);

            $jumlah_cicilan = null;
            $cicilan_terbayar = null;

            switch ($row->stprosesheader) {
                case 0:
                    /* @condition draft &start */
                    #   @bagain - &first
                    $action .= '<a href="' . site_url() . 'tpengajuan/persetujuan/' . $row->id . '/1/' . $row->idpemohon . '" class="btn btn-sm btn-default text-uppercase"><span style="font-size: 10px;">Meminta Persetujuan</span>&nbsp;<i class="fa fa-chevron-circle-right"></i></a>';

                    $action .= '<hr>';

                    #   @second - &second
                    $action .= '<a href="' . site_url() . 'tpengajuan/update/' . $row->id . '" class="btn btn-sm btn-primary text-uppercase" style="border-radius: 100%;"><i class="fa fa-pencil"></i></a>&nbsp;';
                    $action .= '<button class="delete-pengajuan btn btn-sm btn-danger text-uppercase" style="border-radius: 100%;" data-value="' . $row->id . '"><i class="fa fa-trash"></i></button>';

                    /* @condition draft &end */
                    break;
                case 1:
                    /* @action diajukan &start */
                    if ($row->idpemohon == $sessionUserId) {
                        $action = "<p class='text-center'>-</p>";
                    } else {
                        if ($stpersetujuan == 0) {
                            $action = '<a href="' . site_url() . 'tpengajuan/ulasan/' . $row->id . '" class="btn btn-sm btn-default text-uppercase"><span style="font-size: 10px;">Ulasan</span>&nbsp;<i class="fa fa-chevron-circle-right"></i></a>';
                        } else {
                            $action = "<p class='text-center'>-</p>";
                        }
                    }
                    /* @action diajukan &end */
                    break;
                case 2:
                    /* @action disetujui &start */
                    if ($sessionUserRole == 5 && $row->status_persetujuan == 1 && $row->kategori == 2) {
                        $action = '<a href="' . site_url() . 'tpengajuan/aktivasibendahara/' . $row->id . '" class="btn btn-primary btn-xs text-uppercase" style="font-size: 10px;"><i class="fa fa-arrow-right"></i>&nbsp;Aktivasi Bendahara</a>';
                    } else {
                        if ($row->status_persetujuan == 1 && $row->kategori == 2) {
                          $action = '<p class="text-center">-</p>';
                        } else {
                            $action = '<a href="' . site_url() . 'tpengajuan/persetujuan/' . $row->id . '/2/' . $row->idpemohon . '" class="btn btn-primary btn-xs text-uppercase" style="font-size: 10px;"><i class="fa fa-arrow-right"></i>&nbsp;Persetujuan Lanjutan</a>';
                        }
                    }
                    /* @action disetujui &end */
                    break;
                case 3:
                    /* @action ditolak &start */
                    $action = "<p class='text-center'>-</p>";
                    /* @action ditolak &end */
                    break;
                case 4:
                    $action = "<p class='text-center'>-</p>";
                    break;
                case 5:
                    $action = '<a href="" class="btn btn-sm btn-default text-uppercase" style="font-size:10px;"><i class="fa fa-clock-o"></i>&nbsp;History</a>';
                    break;
                case 6:
                    /* @action ulasbalik &start */
                    if ($row->idpemohon == $sessionUserId) {
                        $action = "<p class='text-center'>-</p>";
                    } else {
                        if ($stpersetujuan == 2 && ($check->alasan == "" && $check->alasan_ == "")) {
                            $action = '<a href="' . site_url() . 'tpengajuan/penolakan/' . $row->id . '" class="btn btn-sm btn-default text-uppercase"><span style="font-size: 10px;">Alasan Penolakan</span>&nbsp;<i class="fa fa-chevron-circle-right"></i></a>';
                        } else {
                            if ($total == ($disetujui + $ditolak)) {
                                if ($check->alasan !== "") {
                                    # --------------------------------------------------------------
                                    if ($total == ($disetujui2 + $ditolak2)) {
                                        if ($check->alasan_ == "") {
                                            $action = '<a href="' . site_url() . 'tpengajuan/penolakan/' . $row->id . '" class="btn btn-sm btn-default text-uppercase"><span style="font-size: 10px;">Alasan Penolakan</span>&nbsp;<i class="fa fa-chevron-circle-right"></i></a>';
                                        } else {
                                            $action = '<p class="text-center">-</p>';
                                        }
                                    } else {
                                        if ($check->status_persetujuan_ == 2 && $check->alasan_ == "") {
                                            $action = '<a href="' . site_url() . 'tpengajuan/penolakan/' . $row->id . '" class="btn btn-sm btn-default text-uppercase"><span style="font-size: 10px;">Alasan Penolakan</span>&nbsp;<i class="fa fa-chevron-circle-right"></i></a>';
                                        } else if ($check->alasan_ == "") {
                                            $action = '<a href="' . site_url() . 'tpengajuan/ulasan/' . $row->id . '" class="btn btn-sm btn-default text-uppercase"><span style="font-size: 10px;">Ulasan</span>&nbsp;<i class="fa fa-chevron-circle-right"></i></a>';
                                        } else {
                                            $action = '<p class="text-center">-</p>';
                                        }
                                    }
                                    # --------------------------------------------------------------
                                } else {
                                    $action = '<a href="' . site_url() . 'tpengajuan/ulasan/' . $row->id . '" class="btn btn-sm btn-default text-uppercase"><span style="font-size: 10px;">Ulasan</span>&nbsp;<i class="fa fa-chevron-circle-right"></i></a>';
                                }
                            } else {
                                $action = "<p class='text-center'>-</p>";
                            }
                        }
                    }
                    /* @action ulasbalik &end */
                    break;
                case 7:
                    $jumlah_cicilan = $this->checkTotalCicilan($row->id);
                    $cicilan_terbayar = $this->checkTotalCicilan($row->id, 2);

                    $action = '<a href="" class="btn btn-sm btn-default text-uppercase" style="font-size:10px;"><i class="fa fa-clock-o"></i>&nbsp;History</a>';
                    $action .= '<hr style="margin-top: 6px;margin-bottom: 6px;"><a href="' . site_url() . 'tpengajuan/aktivasibendahara/' . $row->id . '" class="btn btn-sm btn-link text-uppercase" style="font-size:10px;padding-bottom: 4px;border-bottom: 1px solid blue;">Aktivasi Lanjutan&nbsp;<i class="fa fa-chevron-circle-right"></i></a>';
                    break;
            }

            $rows[] = $no;
            $rows[] = $row->nopengajuan;
            $rows[] = DMYFormat($row->tanggal);
            $rows[] = DMYFormat($row->tanggaldibutuhkan);
            $rows[] = $row->namapemohon;
            $rows[] = $row->subjek;
            $rows[] = $row->namaunitpelayanan;
            $rows[] = $row->namakeperluan;
            $rows[] = number_format($row->totalnominal);
            $rows[] = $assigment;
            $rows[] = $this->GetStatusPengajuan($row->stprosesheader, $jumlah_cicilan, $cicilan_terbayar);
            $rows[] = $action;

            $data[] = $rows;
        }

        $output = array(
            "recordsTotal" => count($result),
            "recordsFiltered" => count($result),
            "data" => $data
        );

        echo json_encode($output);
    }

    /* --------------------------------------------------------------------------------------*/
    public function persetujuan($id)
    {
        $kategori = $this->uri->segment(4);
        $idpemohon = $this->uri->segment(5);

        $data = array();
        $data['error'] = '';
        $data['title'] = 'Persetujuan Pengajuan';
        $data['content'] = 'Tpengajuan/persetujuan';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Pengajuan", '#'),
            array("Persetujuan", 'tpengajuan')
        );

        $pengajuan = $this->model->getPengajuanDetail($id);
        $data['nopengajuan'] = $pengajuan->nopengajuan;
        $data['subjek'] = $pengajuan->subjek;
        $data['id'] = $pengajuan->id;
        $data['kategori'] = $kategori;

        $data['datapegawai'] = $this->model->getPegawai($idpemohon);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function savePersetujuan()
    {
        if ($this->model->savePersetujuan()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('tpengajuan', 'location');
        }
    }

    public function checkTotalCicilan($id, $status = null)
    {
        $result = $this->model->checkTotalCicilan($id, $status);
        return count($result);
    }

    public function getPenyetuju($id)
    {
        $results = $this->model->getPersetujuanDetail($id);

        if (empty($results)) {
            $assigment = "<p class='text-center'>-</p>";
        } else {
            $assigment = '<div class="row items-push text-center">';
            foreach ($results as $row) {
                $status = $row->status_persetujuan_ == 0 ? $row->status_persetujuan : $row->status_persetujuan_;
                $assigment .= '<div class="">';
                $assigment .= '<div class="push-5">' . GetStatusAssigment($status) . '</div>';
                $assigment .= '<div class="h6">' . $row->nama . '</div>';
                if ($row->alasan !== null || $row->alasan_ !== null) {
                    $assigment .= '<div style="padding-top: 10px;"><a href="' . site_url() . 'tpengajuan/penolakan/' . $row->idpengajuan . '/' . $row->id . '" class="btn btn-danger text-uppercase" style="padding: 1px;font-size: 9px;">alasan penolakan</a></div>';
                }
                $assigment .= '</div>';

            }
            $assigment .= '</div>';
        }

        return $assigment;
    }

    public function getAlasanPenolakan($id)
    {
        $query = $this->model->getPersetujuanDetail($id);
        $result = '<div>';
        foreach ($query as $rows) {
            if ($rows->stdetail == 2) {
                $result .= '<a href="' . site_url() . 'tpengajuan/ulasan/' . $rows->idpersetujuan . '" class="label label-success text-uppercase">alasan (' . $rows->namapenyetuju . ')</a>';
            }
        }

        $result .= '</div>';
        return $result;
    }

    public function showVendorRekening($id)
    {
        $result = $this->model->getVendorRekening($id);
        $this->output->set_output(json_encode($result));
    }

    public function getPengajuan()
    {
        $this->db->where('id', $id);
        $query = $this->db->get('tpengajuan');
        return $query->result();
    }

    public function GetStatusPengajuan($id, $jumlah_cicilan = null, $cicilan_terbayar = null)
    {
        if ($id != '') {
            $data = array(
                '0' => '<span class="label label-danger text-uppercase">Draft</span>',
                '1' => '<span class="label label-warning text-uppercase">Telah Diajukan</span>',
                '2' => '<span class="label label-success text-uppercase">Disetujui</span>',
                '3' => '<span class="label label-danger text-uppercase">Ditolak</span>',
                '4' => '<span class="label label-success text-uppercase">Aktivasi Bendahara</span>',
                '5' => '<span class="label label-success text-uppercase">Selesai</span>',
                '6' => '<span class="label label-danger text-uppercase">Ditolak</span>',
                '7' => '<span class="label label-info text-uppercase">Aktivasi&nbsp;'.$cicilan_terbayar.' / '.$jumlah_cicilan.'</span>'
            );

            return $data[$id];
        } else {
            return '-';
        }
    }


    /* @function (testing) &start */
    public function testingGood()
    {
        $sessionUserId = $this->session->userdata('user_id');
        $query = $this->model->getIndexPengajuan($sessionUserId);

        print_r($query);
    }

    public function test()
    {
        $result = $this->model->getStatusTerminFix(2);
        print_r($result->statusaktivasi1);
    }
    /* @function (testing) &end */
}
