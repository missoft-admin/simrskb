<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page404 extends CI_Controller {

	/**
	 * Dashboard controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
  }

	function index()
	{
		$data = array();
    $data['title']      = 'Page 404';
		$data['content'] 		= 'page404';
		$data['breadcrum'] 	= array(
												    array("Page 404",'#'),
												    array("Index",'')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
}
