<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mpengaturan_tujuan_laboratorium extends CI_Controller
{
    /**
     * Pengaturan Tujuan Laboratorium controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mpengaturan_tujuan_laboratorium_model');
    }

    public function index(): void
    {
        $data = [];
        $data['error'] = '';
        $data['title'] = 'Pengaturan Tujuan Laboratorium';
        $data['content'] = 'Mpengaturan_tujuan_laboratorium/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Pengaturan Tujuan Laboratorium', '#'],
            ['List', 'mpengaturan_tujuan_laboratorium'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create(): void
    {
        $data = [
            'id' => '',
            'nama' => '',
            'tipe_layanan' => '',
            'tujuan' => '',
            'status_file_audio' => '',
            'dokter_penanggung_jawab' => '',
        ];

        $data['error'] = '';
        $data['title'] = 'Tambah Pengaturan Tujuan Laboratorium';
        $data['content'] = 'Mpengaturan_tujuan_laboratorium/manage';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Pengaturan Tujuan Laboratorium', '#'],
            ['Tambah', 'mpengaturan_tujuan_laboratorium'],
        ];

        $data['list_audio'] = [];
        $data['list_dokter_laboratorium'] = [];
        $data['list_akses_unit_lab'] = [];
        $data['user_laboratorium_akses'] = [];
        $data['tipe_layanan_akses'] = [];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id): void
    {
        if ('' !== $id) {
            $row = $this->Mpengaturan_tujuan_laboratorium_model->getSpecified($id);
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'nama' => $row->nama,
                    'tipe_layanan' => $row->tipe_layanan,
                    'tujuan' => $row->tujuan,
                    'status_file_audio' => $row->status_file_audio,
                    'dokter_penanggung_jawab' => $row->dokter_penanggung_jawab,
                ];
                $data['error'] = '';
                $data['title'] = 'Ubah Pengaturan Tujuan Laboratorium';
                $data['content'] = 'Mpengaturan_tujuan_laboratorium/manage';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Pengaturan Tujuan Laboratorium', '#'],
                    ['Ubah', 'mpengaturan_tujuan_laboratorium'],
                ];

                $data['list_audio'] = $this->Mpengaturan_tujuan_laboratorium_model->getListAudio($id);
                $data['list_dokter_laboratorium'] = $this->Mpengaturan_tujuan_laboratorium_model->getListDokterLaboratorium($id);
                $data['list_akses_unit_lab'] = $this->Mpengaturan_tujuan_laboratorium_model->getListAksesUnit($id);
                $data['user_laboratorium_akses'] = $this->Mpengaturan_tujuan_laboratorium_model->getUserLaboratoriumAkses($id);
                $data['tipe_layanan_akses'] = $this->Mpengaturan_tujuan_laboratorium_model->getTipeLayananAkses($id);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mkelas', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mpengaturan_tujuan_laboratorium');
        }
    }

    public function delete($id): void
    {
        $this->Mpengaturan_tujuan_laboratorium_model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mpengaturan_tujuan_laboratorium', 'location');
    }

    public function save(): void
    {
        if ('' === $this->input->post('id')) {
            if ($this->Mpengaturan_tujuan_laboratorium_model->saveData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data berhasil disimpan.');
                redirect('mpengaturan_tujuan_laboratorium', 'location');
            }
        } else {
            if ($this->Mpengaturan_tujuan_laboratorium_model->updateData()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'data berhasil diubah.');
                redirect('mpengaturan_tujuan_laboratorium', 'location');
            }
        }
    }

    public function getIndex(): void
    {
        $this->select = ['merm_pengaturan_tujuan_laboratorium.*', 'mtujuan.nama_tujuan AS tujuan_antrian'];
        $this->from = 'merm_pengaturan_tujuan_laboratorium';
        $this->join = [
            ['mtujuan', 'mtujuan.id = merm_pengaturan_tujuan_laboratorium.tujuan', '']
        ];
        $this->where = [
            'merm_pengaturan_tujuan_laboratorium.status' => '1',
        ];
        $this->order = [
            'merm_pengaturan_tujuan_laboratorium.id' => 'DESC',
        ];
        $this->group = [];

        $this->column_search = ['merm_pengaturan_tujuan_laboratorium.nama'];
        $this->column_order = ['merm_pengaturan_tujuan_laboratorium.nama'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = $r->nama;
            $row[] = GetTipeLayananLaboratorium($r->tipe_layanan);
            $row[] = $r->tujuan_antrian;
            $row[] = StatusRow($r->status);
            $aksi = '<div class="btn-group">';
            $aksi .= '<a href="'.site_url().'mpengaturan_tujuan_laboratorium/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
            $aksi .= '<a href="#" data-urlindex="'.site_url().'mrak" data-urlremove="'.site_url().'mpengaturan_tujuan_laboratorium/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }

    public function getPoliklinikData($asalPasien) {
        $data = array();

        if ($asalPasien == '1' || $asalPasien == '2') {
            // Fetch data for RAWAT_JALAN
            $data = get_all('mpoliklinik', ['status' => 1]);
        } elseif ($asalPasien == '3') {
            // Fetch data for RAWAT_INAP
            $data = get_all('mkelas', ['status' => 1]);
        }

        // Return JSON response
        echo json_encode($data);
    }
}

// End of file welcome.php
// Location: ./system/application/controllers/welcome.php
