<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tlaba_rugi_setting extends CI_Controller {

	/**
	 * Pegawai controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tlaba_rugi_setting_model','model');
		$this->load->model('Mtarif_rawatjalan_model');
		$this->load->model('Mtarif_rawatinap_model');
		$this->load->model('Mtarif_fisioterapi_model');
		$this->load->model('Tpoliklinik_verifikasi_model');
		$this->load->model('Trawatinap_tindakan_model');
		$this->load->model('Trawatinap_verifikasi_model');
		$this->load->helper('path');
		
  }

	function index($idkategori='0',$tipe_pemilik='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('2'))){
			$data = array();
			$data['idkategori'] 			= $idkategori;
			$data['tipe_pemilik'] 			= $tipe_pemilik;
			$data['tipe_pemilik'] 			= '#';
			$data['status'] 			= '#';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Laba Rugi Template';
			$data['content'] 		= 'Tlaba_rugi_setting/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Setting Laba Rugi Template",'#'),
												  array("List",'tlaba_rugi_setting')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function getIndex($idkategori='0',$tipe_pemilik='0')
  {
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$status = $this->input->post('status');
			$nama = $this->input->post('nama');
			
			if ($status !='#'){
				$where .=" AND status='$status'";
			}
			if ($nama !=''){
				$where .=" AND nama LIKE '%".$nama."%'";
			}
			$this->select = array();
			$from="
					(
						SELECT *
						from tlaba_rugi_setting M
						
						GROUP BY M.id
					) as tbl WHERE id IS NOT NULL ".$where."
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $r->id;
          $result[] = $no;
          $result[] = $r->nama;
         $result[] = StatusBarang($r->status);
         
          $aksi = '<div class="btn-group">';
				$aksi .= '<a href="'.site_url().'tlaba_rugi_setting/update/'.$r->id.'/disabled" data-toggle="tooltip" title="Lihat" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
			if ($r->status=='1'){
				$aksi .= '<a href="'.site_url().'tlaba_rugi_setting/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<a href="'.site_url().'tlaba_rugi_setting/setting/'.$r->id.'" data-toggle="tooltip" title="Management Anggaran" class="btn btn-success btn-xs"><i class="si si-settings"></i></a>';
				$aksi .= '<a href="'.site_url().'tlaba_rugi_setting/setting_tahun/'.$r->id.'" data-toggle="tooltip" title="Adjustment" class="btn btn-warning btn-xs"><i class="si si-docs"></i></a>';
				$aksi .= '<button title="Non Aktifkan" class="btn btn-danger btn-xs removeData"><i class="si si-ban"></i></button>';
				$aksi .= '<button data-toggle="tooltip" title="Print" class="btn btn-success btn-xs"><i class="fa fa-print"></i></button>';
			}else{
				$aksi .= '<button title="Aktifkan" class="btn btn-success btn-xs aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
			}
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function create($disabel=''){
		
		$data = array(
			'id'            => '',
			'nama' 					=> '',			
		);

		$data['list_user_akses'] 			= $this->model->list_user_akses('');
		$data['disabel'] 			= $disabel;
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Setting Laba Rugi Template';
		$data['content'] 		= 'Tlaba_rugi_setting/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Setting Laba Rugi Template",'#'),
								            array("Tambah",'tlaba_rugi_setting')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id,$disabel=''){
		
		if($id != ''){
			$data= $this->model->getSpecified($id);
			
			$data['list_user_akses'] 			= $this->model->list_user_akses($id);
			$data['disabel'] 		= $disabel;
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Setting Laba Rugi Template';
			$data['content']    = 'Tlaba_rugi_setting/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Setting Laba Rugi Template",'#'),
										array("Ubah",'tlaba_rugi_setting')
										);

			// $data['statusAvailableApoteker'] = $this->model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('tlaba_rugi_setting');
		}
	}
	 public function save_section()
    {
		
        $id_edit_section = $this->input->post('id_edit_section');
        $template_id = $this->input->post('template_id');
        $nama = $this->input->post('nama');
        $urutan = $this->input->post('urutan');
        $st_jumlah = $this->input->post('st_jumlah');
      
		$data =array(
            'template_id'=>$template_id,
            'header_id'=>0,
            'lev'=>0,
            'nama'=>$nama,
            'urutan'=>$urutan,
            'st_jumlah'=>$st_jumlah,
            'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
			'created_date'=>date('Y-m-d H:i:s'),
			'updated_by'=>$this->session->userdata('user_id'),
			'updated_nama'=>$this->session->userdata('user_name'),
			'updated_date'=>date('Y-m-d H:i:s'),

        );
		if ($id_edit_section){
			$this->db->where('id',$id_edit_section);
			$result=$this->db->update('tlaba_rugi_setting_section',$data);
		}else{
			$result=$this->db->insert('tlaba_rugi_setting_section',$data);
			$id=$this->db->insert_id();
			$var_nama="var_section_".$id;
			$this->db->where('id',$id);
			$this->db->update('tlaba_rugi_setting_section',array('var_nama'=>$var_nama));
			
		}        
        $this->output->set_output(json_encode($result));
    }
	public function save_perhitungan()
    {
		
        $id_edit_perhitungan = $this->input->post('id_edit_perhitungan');
        $template_id = $this->input->post('template_id');
        $nama = $this->input->post('nama');
        $urutan = $this->input->post('urutan');
        $lev = $this->input->post('lev');
        $header_id = $this->input->post('header_id');
        $formula = $this->input->post('formula');
      
		$data =array(
            'template_id'=>$template_id,
            'header_id'=>$header_id,
            'lev'=>$lev,
            'tipe'=>2,
            'nama'=>$nama,
            'formula'=>$formula,
            'urutan'=>$urutan,
            'st_jumlah'=>0,
            'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
			'created_date'=>date('Y-m-d H:i:s'),
			'updated_by'=>$this->session->userdata('user_id'),
			'updated_nama'=>$this->session->userdata('user_name'),
			'updated_date'=>date('Y-m-d H:i:s'),

        );
		if ($id_edit_perhitungan){
			$this->db->where('id',$id_edit_perhitungan);
			$result=$this->db->update('tlaba_rugi_setting_section',$data);
		}else{
			$result=$this->db->insert('tlaba_rugi_setting_section',$data);
			$id=$this->db->insert_id();
			$var_nama="var_section_".$id;
			$this->db->where('id',$id);
			$this->db->update('tlaba_rugi_setting_section',array('var_nama'=>$var_nama));
			
		}        
        $this->output->set_output(json_encode($result));
    }
	public function save_kategori()
    {
		
        $id_edit_kategori = $this->input->post('id_edit_kategori');
        $template_id = $this->input->post('template_id');
        $nama = $this->input->post('nama');
        $urutan = $this->input->post('urutan');
        $st_jumlah = $this->input->post('st_jumlah');
        $header_id = $this->input->post('header_id');
      
		$data =array(
            'template_id'=>$template_id,
            'header_id'=>$header_id,
            'lev'=>1,
            'nama'=>$nama,
            'urutan'=>$urutan,
            'st_jumlah'=>$st_jumlah,
            'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
			'created_date'=>date('Y-m-d H:i:s'),
			'updated_by'=>$this->session->userdata('user_id'),
			'updated_nama'=>$this->session->userdata('user_name'),
			'updated_date'=>date('Y-m-d H:i:s'),

        );
		if ($id_edit_kategori){
			$this->db->where('id',$id_edit_kategori);
			$result=$this->db->update('tlaba_rugi_setting_section',$data);
		}else{
			$result=$this->db->insert('tlaba_rugi_setting_section',$data);
			$id=$this->db->insert_id();
			$var_nama="var_section_".$id;
			$this->db->where('id',$id);
			$this->db->update('tlaba_rugi_setting_section',array('var_nama'=>$var_nama));
			
		}        
        $this->output->set_output(json_encode($result));
    }
	public function save_akun()
    {
		
        $id_edit_akun = $this->input->post('id_edit_akun');
        $template_id = $this->input->post('template_id');
        $idakun = $this->input->post('idakun');
        $urutan = $this->input->post('urutan');
        $section_id = $this->input->post('section_id');
		$level=$this->db->query("SELECT lev FROM tlaba_rugi_setting_section WHERE id='$section_id'")->row('lev');
		$data =array(
            'template_id'=>$template_id,
            'section_id'=>$section_id,
            'lev'=>($level+1),
            'idakun'=>$idakun,
            'urutan'=>$urutan,
            'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
			'created_date'=>date('Y-m-d H:i:s'),
			'updated_by'=>$this->session->userdata('user_id'),
			'updated_nama'=>$this->session->userdata('user_name'),
			'updated_date'=>date('Y-m-d H:i:s'),

        );
		if ($id_edit_akun){
			$this->db->where('id',$id_edit_akun);
			$result=$this->db->update('tlaba_rugi_setting_section_akun',$data);
		}else{
			$result=$this->db->insert('tlaba_rugi_setting_section_akun',$data);
			$id=$this->db->insert_id();
			$var_nama="var_akun_".$id;
			$this->db->where('id',$id);
			$this->db->update('tlaba_rugi_setting_section_akun',array('var_nama'=>$var_nama));
			
		}        
        $this->output->set_output(json_encode($result));
    }
	function load_variable(){
		$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$template_id = $this->input->post('template_id');
			$q="SELECT H.var_nama,H.nama FROM tlaba_rugi_setting_section H WHERE H.template_id='$template_id'
				UNION ALL
				SELECT H.var_nama,CONCAT(A.noakun,' - ',A.namaakun) as akun FROM tlaba_rugi_setting_section_akun H
				LEFT JOIN makun_nomor A ON A.id=H.idakun
				WHERE H.template_id='$template_id'
				";
			$arr_akun=$this->db->query($q)->result();
			
			$disabel = $this->input->post('disabel');
			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
						SELECT H.id,H.nama,H.urutan,H.lev,CONCAT('0',LPAD(H.urutan,3,'0')) as v_urutan,H.header_id,'' as idakun,H.tipe,H.var_nama
						,H.formula
						FROM tlaba_rugi_setting_section H
						WHERE H.template_id='$template_id' AND H.lev='0'


						UNION ALL

						SELECT H.id,H.nama,H.urutan,H.lev,CONCAT('0',LPAD(H2.urutan,3,'0'),'-1',LPAD(H.urutan,3,'0'))  as v_urutan,H.header_id,'' as idakun,H.tipe,H.var_nama
						,H.formula
						FROM tlaba_rugi_setting_section H
						LEFT JOIN tlaba_rugi_setting_section H2 ON H2.id=H.header_id
						WHERE H.template_id='$template_id' AND H.lev='1'

						UNION ALL

						SELECT H.id,CONCAT(A.noakun,'  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     ',A.namaakun) as nama,H.urutan,2 as lev
						,IF(S0.id IS NOT NULL,CONCAT('0',LPAD(S0.urutan,3,'0'),'-',S.lev,LPAD(S.urutan,3,'0'),'-',H.lev,LPAD(H.urutan,3,'0')),CONCAT(S.lev,LPAD(S.urutan,3,'0'),'-',H.lev,LPAD(H.urutan,3,'0'))) as v_urutan,H.section_id as header_id
						,H.idakun as idakun,'' tipe,H.var_nama
						,'' as formula
						FROM tlaba_rugi_setting_section_akun H
						LEFT JOIN makun_nomor A ON A.id=H.idakun
						LEFT JOIN tlaba_rugi_setting_section S ON S.id=H.section_id
						LEFT JOIN tlaba_rugi_setting_section S0 ON S0.id=S.header_id AND S.lev='1'
						WHERE H.template_id='$template_id'
						) H 
						ORDER BY H.v_urutan ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();$this->order  = array();$this->group  = array();
			$this->column_search = array('nama');$this->column_order  = array();

			$list = $this->datatable->get_datatables();
			$data = array();$no = $_POST['start'];
			
			$q="SELECT H.var_nama,H.nama FROM tlaba_rugi_setting_section H WHERE H.template_id='1'
			UNION ALL
			SELECT H.var_nama,CONCAT(A.noakun,' - ',A.namaakun) as akun FROM tlaba_rugi_setting_section_akun H
			LEFT JOIN makun_nomor A ON A.id=H.idakun
			WHERE H.template_id='1'
			";
		
		
		  foreach ($list as $r) {
			  $no++;
			  if ($r->lev=='0'){
				  $jml_akun=$this->db->query("SELECT IFNULL(COUNT(*),0) as jml_akun FROM tlaba_rugi_setting_section_akun H WHERE H.section_id='".$r->id."'")->row('jml_akun');
				  $jml_kategori=$this->db->query("SELECT IFNULL(COUNT(*),0) as jml_akun FROM tlaba_rugi_setting_section H WHERE H.header_id='".$r->id."'")->row('jml_akun');
			  }else{
				  $jml_akun=0;
				  $jml_kategori=0;
			  }
			  
			  $btn_perhitungan='';
			  if ($r->lev=='0'){
				  $btn_perhitungan='<button class="btn btn-danger btn-xs" onclick="add_perhitugan_kategori('.$r->id.')" type="button"><i class="fa fa-plus"></i> Perhitungan Kategori</button>';
			  }
			  $btn_add_kategori='';
			  $btn_add_akun='';
			  $result = array();
			  if ($r->idakun ==''){
				  if ($jml_akun=='0'){
				   $btn_add_kategori='<button '.$disabel.' class="btn btn-info btn-xs" onclick="add_kategori('.$r->id.')" type="button"><i class="fa fa-plus"></i> Kategori</button>';
					  
				  }
				  if ($jml_kategori=='0'){
					$btn_add_akun='<button  '.$disabel.' class="btn btn-success btn-xs" onclick="add_akun('.$r->id.')" type="button"><i class="fa fa-plus"></i> No.Akun</button>';
				  }
			  }
			  if ($disabel!='' || $r->tipe=='2'){
				   $btn_add_kategori='';
					$btn_add_akun='';
					$btn_perhitungan='';
			  }
			  $var_nama='&nbsp;&nbsp;&nbsp;'.'<code>{'.$r->var_nama.'}</code>';
			  $result[] = $r->lev;
			  $result[] = $r->tipe;
			  $result[] = $r->urutan;
			  $result[] =($r->tipe=='2'?'<strong>':''). TreeView2($r->lev,$r->nama).($r->tipe=='2'?'</strong>':'').$var_nama.' &nbsp;&nbsp;&nbsp; '.($r->lev=='0'?$btn_add_kategori:'').' '.$btn_add_akun.' '.$btn_perhitungan;
			  // $result[] = TreeView($r->lev,$r->nama).' &nbsp;&nbsp;&nbsp; '.($r->lev=='0'?$btn_add_kategori:'').' '.$btn_add_akun;
			  $result[] = ($r->tipe=='2'?'<i class="fa fa-calculator"></i>  &nbsp;&nbsp;&nbsp;':'').$this->convert_formula($arr_akun,$r->formula);
			 
			  $aksi = '<div class="btn-group">';
			  if ($disabel==''){
				  
			  if ($r->lev=='0'){
				  if ($r->tipe=='2'){
				  $aksi .= '<button class="btn btn-warning btn-xs"  '.$disabel.' onclick="edit_perhitungan('.$r->id.','.$r->header_id.')" type="button"><i class="fa fa-pencil"></i></button>';
					  
				  }else{
				  $aksi .= '<button class="btn btn-info btn-xs"  '.$disabel.' onclick="edit_section('.$r->id.')" type="button"><i class="fa fa-pencil"></i></button>';
					  
				  }
				  $aksi .= '<button class="btn btn-danger btn-xs"  '.$disabel.' onclick="hapus_section('.$r->id.')" type="button"><i class="fa fa-trash-o"></i></button>';
			  }
			  if ($r->lev=='1'){
				  if ($r->tipe=='2'){
					$aksi .= '<button class="btn btn-warning btn-xs"  '.$disabel.' onclick="edit_perhitungan('.$r->id.','.$r->header_id.')" type="button"><i class="fa fa-pencil"></i></button>';					  
				  }else{
					$aksi .= '<button class="btn btn-info btn-xs"  '.$disabel.' onclick="edit_kategori('.$r->id.','.$r->header_id.')" type="button"><i class="fa fa-pencil"></i></button>';
				  }
				  $aksi .= '<button class="btn btn-danger btn-xs"  '.$disabel.' onclick="hapus_kategori('.$r->id.')" type="button"><i class="fa fa-trash-o"></i></button>';
			  }
			  if ($r->lev=='2'){
				  $aksi .= '<button class="btn btn-info btn-xs"  '.$disabel.' onclick="edit_akun('.$r->id.','.$r->idakun.','.$r->header_id.')" type="button"><i class="fa fa-pencil"></i></button>';
				  $aksi .= '<button class="btn btn-danger btn-xs"  '.$disabel.' onclick="hapus_akun('.$r->id.')" type="button"><i class="fa fa-trash-o"></i></button>';
			  }
			  }
			  
				
			  $aksi .= '</div>';
			  $result[] = $aksi;

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(),
			  "recordsFiltered" => $this->datatable->count_all(),
			  "data" => $data
		  );
		  echo json_encode($output);
	  
		
	}
	function convert_formula($arr_akun,$formula){
		$data=array();
		foreach($arr_akun as $row){
			$data[$row->var_nama]=' ('.$row->nama.') ';
		}
		$text =$this->parser->parse_string($formula,$data,true);
		return $text;
	}
	
	function check_convert_formula(){
		$formula=$this->input->post('formula');
		$template_id=$this->input->post('template_id');
		$q="SELECT H.var_nama,H.nama FROM tlaba_rugi_setting_section H WHERE H.template_id='$template_id'
			UNION ALL
			SELECT H.var_nama,CONCAT(A.noakun,' - ',A.namaakun) as akun FROM tlaba_rugi_setting_section_akun H
			LEFT JOIN makun_nomor A ON A.id=H.idakun
			WHERE H.template_id='$template_id'
			";
		$arr_akun=$this->db->query($q)->result();
		
		$data=array();
		foreach($arr_akun as $row){
			$data[$row->var_nama]=' ('.$row->nama.') ';
		}
		$arr['formula'] =$this->parser->parse_string($formula,$data,true);
		$this->output->set_output(json_encode($arr));
	}
	function list_variable(){
		$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$template_id = $this->input->post('template_id');
			$disabel = $this->input->post('disabel');
			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
						SELECT H.id,H.nama,H.urutan,H.lev,CONCAT('0',LPAD(H.urutan,3,'0')) as v_urutan,H.header_id,'' as idakun,H.tipe,H.var_nama
						,H.formula
						FROM tlaba_rugi_setting_section H
						WHERE H.template_id='$template_id' AND H.lev='0'


						UNION ALL

						SELECT H.id,H.nama,H.urutan,H.lev,CONCAT('0',LPAD(H2.urutan,3,'0'),'-1',LPAD(H.urutan,3,'0'))  as v_urutan,H.header_id,'' as idakun,H.tipe,H.var_nama
						,H.formula
						FROM tlaba_rugi_setting_section H
						LEFT JOIN tlaba_rugi_setting_section H2 ON H2.id=H.header_id
						WHERE H.template_id='$template_id' AND H.lev='1'

						UNION ALL

						SELECT H.id,CONCAT(A.noakun,'  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     ',A.namaakun) as nama,H.urutan,2 as lev
						,IF(S0.id IS NOT NULL,CONCAT('0',LPAD(S0.urutan,3,'0'),'-',S.lev,LPAD(S.urutan,3,'0'),'-',H.lev,LPAD(H.urutan,3,'0')),CONCAT(S.lev,LPAD(S.urutan,3,'0'),'-',H.lev,LPAD(H.urutan,3,'0'))) as v_urutan,H.section_id as header_id
						,H.idakun as idakun,'' tipe,H.var_nama
						,'' as formula
						FROM tlaba_rugi_setting_section_akun H
						LEFT JOIN makun_nomor A ON A.id=H.idakun
						LEFT JOIN tlaba_rugi_setting_section S ON S.id=H.section_id
						LEFT JOIN tlaba_rugi_setting_section S0 ON S0.id=S.header_id AND S.lev='1'
						WHERE H.template_id='$template_id'
						) H 
						ORDER BY H.v_urutan ASC


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();$this->order  = array();$this->group  = array();
			$this->column_search = array('nama');$this->column_order  = array();

			$list2 = $this->datatable->get_datatables();
			$data = array();$no = $_POST['start'];
		  foreach ($list2 as $r) {
			  $result = array();
			  $var_nama='&nbsp;&nbsp;&nbsp;'.'<code>{'.$r->var_nama.'}</code>';
			  $var_pilih='{'.$r->var_nama.'}';
			  $result[] = $r->lev;
			  $result[] = $r->tipe;
			  // $result[] =($r->tipe=='2'?'<strong>':'').$r->nama.($r->tipe=='2'?'</strong>':'').$var_nama;
			  $result[] ='<a href="#" onclick="pilih_variable(\''.$var_pilih.'\')" >'.($r->tipe=='2'?'<strong>':''). TreeView2($r->lev,$r->nama).($r->tipe=='2'?'</strong>':'').$var_nama.'</a>';
			  // $result[] =$r->nama;
			  $result[] =$var_pilih;
			  
			  $data[] = $result;
		  }
			// print_r($list2);exit();
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(),
			  "recordsFiltered" => $this->datatable->count_all(),
			  "data" => $data
		  );
		  echo json_encode($output);
	  
		
	}
	function get_info_section(){
		$id=$this->input->post('id');
		$q="SELECT H.nama,H.st_jumlah,H.urutan FROM `tlaba_rugi_setting_section` H WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function get_info_perhitungan(){
		$id=$this->input->post('id');
		$q="SELECT H.nama,H.st_jumlah,H.lev,H.urutan,H.formula FROM `tlaba_rugi_setting_section` H WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function max_section(){
		$template_id=$this->input->post('template_id');
		$q="SELECT IFNULL(MAX(H.urutan),0)+1 as urutan FROM tlaba_rugi_setting_section H WHERE H.template_id='$template_id' AND H.lev='0'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function max_kategori(){
		$template_id=$this->input->post('template_id');
		$header_id=$this->input->post('header_id');
		$q="SELECT IFNULL(MAX(H.urutan),0)+1 as urutan FROM tlaba_rugi_setting_section H WHERE H.template_id='$template_id' AND H.header_id='$header_id'";
		// print_r($q);exit();
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function max_perhitungan(){
		$template_id=$this->input->post('template_id');
		$header_id=$this->input->post('header_id');
		$q="SELECT IFNULL(MAX(T.urutan),0)+1 as  urutan FROM (
			SELECT H.urutan FROM tlaba_rugi_setting_section H WHERE H.template_id='$template_id' AND H.header_id='$header_id'
			UNION ALL
			SELECT H.urutan FROM tlaba_rugi_setting_section_akun H WHERE H.template_id='$template_id' AND H.section_id='$header_id'
		) T";
		// print_r($q);exit();
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function max_akun(){
		$template_id=$this->input->post('template_id');
		$header_id=$this->input->post('header_id');
		$q="SELECT IFNULL(MAX(H.urutan),0)+1 as urutan FROM tlaba_rugi_setting_section_akun H WHERE H.template_id='$template_id' AND H.section_id='$header_id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function get_info_akun(){
		$id=$this->input->post('id');
		$q="SELECT H.section_id,H.idakun,H.urutan FROM `tlaba_rugi_setting_section_akun` H WHERE H.id='$id'";
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_section(){
		$id=$this->input->post('id');
		$this->db->delete('tlaba_rugi_setting_section_akun',array('section_id'=>$id));
		$this->db->delete('tlaba_rugi_setting_section',array('header_id'=>$id));
		$hasil=$this->db->delete('tlaba_rugi_setting_section',array('id'=>$id));
		
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_akun(){
		$id=$this->input->post('id');
		$hasil=$this->db->delete('tlaba_rugi_setting_section_akun',array('id'=>$id));
		
		$this->output->set_output(json_encode($hasil));
	}
	function load_akun(){
		$template_id=$this->input->post('template_id');
		$idakun=$this->input->post('idakun');
		$q="SELECT H.id,CONCAT(H.noakun,'-',H.namaakun) as akun FROM `makun_nomor` H
WHERE H.poslaporan='LR' AND H.id NOT IN (SELECT R.idakun from tlaba_rugi_setting_section_akun R WHERE R.template_id='$template_id' AND R.idakun !='$idakun')";
// print_r($q);exit();
		$row=$this->db->query($q)->result();
		$opsi='';
			$opsi .='<option value="#" '.($idakun==''?'selected':'').'>-Pilih Akun-</option>';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'" '.($r->id==$idakun?'selected':'').'>'.$r->akun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	

	}
	
	function delete($id){
		
		$result=$this->model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('tlaba_rugi_setting','location');
	}
	function aktifkan($id){
		
		$result=$this->model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	function save(){
	
		if($this->input->post('id') == '' ) {
			$id=$this->model->saveData();
			if($id){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tlaba_rugi_setting/update/'.$id,'location');
			}
		} else {
			if($this->model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tlaba_rugi_setting','location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Tlaba_rugi_setting/manage';

		if($id==''){
			$data['title'] = 'Tambah Setting Laba Rugi Template';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Setting Laba Rugi Template",'#'),
							               array("Tambah",'tlaba_rugi_setting')
								           );
		}else{
			$data['title'] = 'Ubah Setting Laba Rugi Template';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Setting Laba Rugi Template",'#'),
							               array("Ubah",'tlaba_rugi_setting')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	//Setting
	function setting($id,$disabel=''){
		
		if($id != ''){
			$data= $this->model->getSpecified($id);
			
			$data['list_user_akses'] 			= $this->model->list_user_akses($id);
			$data['disabel'] 		= $disabel;
			$data['error'] 			= '';
			$data['title'] 			= 'PENGATURAN ANGGARAN';
			$data['content']    = 'Tlaba_rugi_setting/manage_setting';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Setting Laba Rugi Template",'#'),
										array("Ubah",'tlaba_rugi_setting')
										);

			// $data['statusAvailableApoteker'] = $this->model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('tlaba_rugi_setting');
		}
	}
	function setting_tahun($id,$disabel=''){
		
		if($id != ''){
			$data= $this->model->getSpecified($id);
			
			$data['list_user_akses'] 			= $this->model->list_user_akses($id);
			$data['disabel'] 		= $disabel;
			$data['error'] 			= '';
			$data['title'] 			= 'SETTING ADJUSTMENT';
			$data['content']    = 'Tlaba_rugi_setting/manage_tahun';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Setting Laba Rugi Template",'#'),
										array("Ubah",'tlaba_rugi_setting')
										);

			// $data['statusAvailableApoteker'] = $this->model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('tlaba_rugi_setting');
		}
	}
	function refresh_periode($tahun){
		$opsi='';
		for($i=1;$i<=12;$i++){
			$bulan=str_pad($i,2,"0", STR_PAD_LEFT);
			$opsi .='<option value="'. $tahun.$bulan.'" selected>'.get_periode_2($tahun.$bulan).'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));

	}
	function load_periode($tahun,$template_id){
		$q="SELECT DISTINCT(H.periode) FROM tlaba_rugi_setting_anggaran_detail H WHERE H.tahun='$tahun' AND H.template_id='$template_id' ORDER BY H.periode";
		$rows=$this->db->query($q)->result();
		$opsi='';
		foreach($rows as $row){
			// $opsi .='<option value="'. $row->periode.'" '.($row->periode>=date('Ym')?'selected':'').'>'.get_periode_2($row->periode).'</option>';
			$opsi .='<option value="'. $row->periode.'" selected>'.get_periode_2($row->periode).'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));

	}
	
	function simpan_periode(){
		$xperiode=$this->input->post('periode');
		$template_id=$this->input->post('template_id');
		if ($xperiode){
			foreach($xperiode as $index=>$val){
				$bulan=substr($val,-2);
				$tahun=substr($val,0,4);
				$periode=$val;
				if ($this->cek_duplicate($periode,$template_id)=='0'){
					$q="INSERT INTO tlaba_rugi_setting_anggaran_detail (template_id,periode,tahun,bulan,section_akun_id,nominal)
						SELECT $template_id as template_id,'$periode' as periode,'$tahun' as tahun,'$bulan' as bulan,H.id as section_akun_id,'0' as nominal FROM tlaba_rugi_setting_section_akun H
						WHERE H.template_id='$template_id'";
					$result=$this->db->query($q);
				}
				// print_r($val);exit();
			}
		}
		$this->output->set_output(json_encode($result));
	}
	
	function update_nominal(){
		$periode=$this->input->post('periode');
		$template_id=$this->input->post('template_id');
		$section_akun_id=$this->input->post('section_akun_id');
		$nominal=RemoveComma($this->input->post('nominal'));
		$this->db->where('periode',$periode);
		$this->db->where('template_id',$template_id);
		$this->db->where('section_akun_id',$section_akun_id);
		$result=$this->db->update('tlaba_rugi_setting_anggaran_detail',array('nominal'=>$nominal));
		$this->output->set_output(json_encode($result));
	}
	function update_nominal_tahun(){
		$tahun=$this->input->post('tahun');
		$template_id=$this->input->post('template_id');
		$section_akun_id=$this->input->post('section_akun_id');
		$nominal=RemoveComma($this->input->post('nominal'));
		$this->db->where('tahun',$tahun);
		$this->db->where('template_id',$template_id);
		$this->db->where('section_akun_id',$section_akun_id);
		$result=$this->db->update('tlaba_rugi_setting_anggaran_tahun',array('nominal'=>$nominal));
		$this->output->set_output(json_encode($result));
	}
	
	function cek_duplicate($periode,$template_id){
		$q="SELECT COUNT(*) jml  FROM tlaba_rugi_setting_anggaran_detail H WHERE H.periode='$periode' AND H.template_id='$template_id'";
		$jml=$this->db->query($q)->row('jml');
		return $jml;
	}
	function load_anggaran(){
		$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$template_id = $this->input->post('template_id');			
			$periode = $this->input->post('periode');	
			foreach($periode as $index=>$val){
				$q="SELECT H.id FROM tlaba_rugi_setting_section_akun H
					WHERE H.id NOT IN (
					SELECT D.section_akun_id FROM tlaba_rugi_setting_anggaran_detail D
					WHERE D.template_id='$template_id' AND D.periode ='$val'
					) AND H.template_id='$template_id'";
				$rows=$this->db->query($q)->result();
				foreach($rows as $row){
					$bulan=substr($val,-2);
					$tahun=substr($val,0,4);
					$data=array(
						'template_id' =>$template_id,
						'periode' =>$val,
						'tahun' =>$tahun,
						'bulan' =>$bulan,
						'section_akun_id' =>$row->id,
						'nominal' =>0,
					);
					$this->db->insert('tlaba_rugi_setting_anggaran_detail',$data);
				}				
			}
			$disabel = $this->input->post('disabel');
			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
						SELECT H.id,H.nama,H.urutan,H.lev,CONCAT('0',LPAD(H.urutan,3,'0')) as v_urutan,H.header_id,'' as idakun,H.tipe,H.var_nama
						,H.formula
						FROM tlaba_rugi_setting_section H
						WHERE H.template_id='$template_id' AND H.lev='0' AND H.tipe!='2'


						UNION ALL

						SELECT H.id,H.nama,H.urutan,H.lev,CONCAT('0',LPAD(H2.urutan,3,'0'),'-1',LPAD(H.urutan,3,'0'))  as v_urutan,H.header_id,'' as idakun,H.tipe,H.var_nama
						,H.formula
						FROM tlaba_rugi_setting_section H
						LEFT JOIN tlaba_rugi_setting_section H2 ON H2.id=H.header_id
						WHERE H.template_id='$template_id' AND H.lev='1' AND H.tipe!='2'

						UNION ALL

						SELECT H.id,CONCAT(A.noakun,'  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     ',A.namaakun) as nama,H.urutan,2 as lev
						,IF(S0.id IS NOT NULL,CONCAT('0',LPAD(S0.urutan,3,'0'),'-',S.lev,LPAD(S.urutan,3,'0'),'-',H.lev,LPAD(H.urutan,3,'0')),CONCAT(S.lev,LPAD(S.urutan,3,'0'),'-',H.lev,LPAD(H.urutan,3,'0'))) as v_urutan,H.section_id as header_id
						,H.idakun as idakun,'' tipe,H.var_nama
						,'' as formula
						FROM tlaba_rugi_setting_section_akun H
						LEFT JOIN makun_nomor A ON A.id=H.idakun
						LEFT JOIN tlaba_rugi_setting_section S ON S.id=H.section_id
						LEFT JOIN tlaba_rugi_setting_section S0 ON S0.id=S.header_id AND S.lev='1'
						WHERE H.template_id='$template_id'
						) H 
						ORDER BY H.v_urutan ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();$this->order  = array();$this->group  = array();
			$this->column_search = array('nama');$this->column_order  = array();

			$list = $this->datatable->get_datatables();
			$data = array();$no = $_POST['start'];
			// print_r($periode);exit();
		  foreach ($list as $r) {
			  $no++;
			   $result = array();
			    $result[] = $r->lev;
			  $result[] = $r->tipe;
			  $result[] =($r->tipe!==''?'<strong>':'').TreeView2($r->lev,$r->nama).($r->tipe!==''?'<strong>':'');
			  
			 foreach($periode as $x => $val) {
				 if ($r->tipe==''){
					$result[] = $this->detail_periode($template_id,$r->id,$val);					 
				 }else{
					$result[] = ''; 
				 }
		     }

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(),
			  "recordsFiltered" => $this->datatable->count_all(),
			  "data" => $data
		  );
		  echo json_encode($output);
	  
		
	}
	
                                                   
	function detail_periode($template_id, $id,$periode){	
		$hasil='';
		$nominal=$this->db->query("SELECT H.nominal FROM `tlaba_rugi_setting_anggaran_detail` H WHERE H.template_id='$template_id' AND H.section_akun_id='$id' AND H.periode='$periode'")->row('nominal');
		$nominal=number_format($nominal,0, ".", ",");
		// $hasil='<div class="input-group" style="width:100%;z-index:0">';
		$hasil.='	<input class="form-control decimal input-sm inputan" style="width:140px;text-align:right;" onfocus="focusFunction(this)" onblur="blurFunction(this,'.$periode.','.$id.')"  type="text" placeholder="0" value="'.$nominal.'"> ';
		// $hasil.='	<span class="input-group-btn" style="z-index:0!important">';
		$hasil.='		<button class="btn btn-primary btn-xs" data-toggle="tooltip" style="width:140px" data-placement="top" onclick="copy_ke_periode('.$periode.','.$id.')" title="Terapkan Ke Periode Selanjutnya" type="button"><i class="fa fa-paper-plane"></i></button>';
		// $hasil.='	</span>';
		$hasil.='</div>';
		// $hasil.='</div>';

		// $hasil='t.nixon@datatables.net';
		return $hasil;
	}
	function date_akun_copy(){
		$template_id=$this->input->post('template_id');
		$section_akun_id=$this->input->post('section_akun_id');
		$periode=$this->input->post('periode');
		$q_periode="SELECT DISTINCT(H.periode) FROM tlaba_rugi_setting_anggaran_detail H 
					WHERE H.template_id='$template_id' AND H.periode >'$periode'
					ORDER BY H.periode";
		$rows=$this->db->query($q_periode)->result();
		$opsi='';
		foreach($rows as $row){
			$opsi .='<option value="'. $row->periode.'">'.get_periode_2($row->periode).'</option>';
		}
		$arr['periode']=$opsi;
		$q="SELECT H.nominal FROM `tlaba_rugi_setting_anggaran_detail` H WHERE H.template_id='$template_id' AND H.section_akun_id='$section_akun_id' AND H.periode='$periode'";
		$arr['nominal']=$this->db->query($q)->row('nominal');
		$this->output->set_output(json_encode($arr));
	}
	
	function date_akun_copy_all(){
		$template_id=$this->input->post('template_id');
		$periode=$this->input->post('periode');
		$q_periode="SELECT DISTINCT(H.periode) FROM tlaba_rugi_setting_anggaran_detail H 
					WHERE H.template_id='$template_id' AND H.periode >'$periode'
					ORDER BY H.periode";
		$rows=$this->db->query($q_periode)->result();
		$opsi='';
		foreach($rows as $row){
			$opsi .='<option value="'. $row->periode.'">'.get_periode_2($row->periode).'</option>';
		}
		$arr['periode']=$opsi;
		$arr['nominal']='0';
		$this->output->set_output(json_encode($arr));
	}
	function update_copy_periode(){
		
		$periode=$this->input->post('periode');
		$template_id=$this->input->post('template_id');
		$section_akun_id=$this->input->post('section_akun_id');
		$nominal=RemoveComma($this->input->post('nominal'));
		$nominal_kenaikan=RemoveComma($this->input->post('nominal_kenaikan'));
		$persentase_kenaikan=RemoveComma($this->input->post('persentase_kenaikan'));
		$decision=$this->input->post('decision');
		$periode_awal=$this->input->post('periode_awal');
		if ($section_akun_id=='all'){
			$nominal=0;
			foreach($periode as $index=>$val){
				$var_set="";
				if ($decision=='1'){
					$var_set="H.nominal=D.nominal+".$nominal;					
				}
				if ($decision=='2'){
					$nominal=$nominal + $nominal_kenaikan;
					$var_set="H.nominal=D.nominal+".$nominal;
				}
				if ($decision=='3'){
					$var_set="H.nominal=D.nominal+(D.nominal*".$persentase_kenaikan."/100)";					
				}
				if ($decision=='4'){
					$var_set="H.nominal=D.nominal*0";		
				}
				$q="UPDATE tlaba_rugi_setting_anggaran_detail H INNER JOIN (
					SELECT H.template_id,H.section_akun_id,H.nominal 
					FROM tlaba_rugi_setting_anggaran_detail H
					WHERE H.periode='$periode_awal' AND H.template_id='$template_id'
					) D ON D.section_akun_id=H.section_akun_id AND H.template_id=D.template_id AND H.periode='$val'
					SET ".$var_set."

					";
					// print_r($q);exit();
				$result=$this->db->query($q);
				if ($decision=='3'){
					$periode_awal=$val;				
				}
			}
		}else{
			foreach($periode as $index=>$val){
				$data=array();			
				if ($decision=='1'){
					$data['nominal']=$nominal;
				}
				if ($decision=='2'){
					$data['nominal']=$nominal + $nominal_kenaikan;
					$nominal=$data['nominal'];
				}
				if ($decision=='3'){
					$nominal=$nominal+ ($nominal*$persentase_kenaikan/100);
					$data['nominal']=$nominal;
					$nominal=$data['nominal'];
				}
				if ($decision=='4'){
					$data['nominal']=0;
				}
				$this->db->where('template_id',$template_id);
				$this->db->where('section_akun_id',$section_akun_id);
				$this->db->where('periode',$val);
				$result=$this->db->update('tlaba_rugi_setting_anggaran_detail',$data);
			}
		}
		
		
		$this->output->set_output(json_encode($result));
	}
	
	function load_tahun($template_id){
		$q="SELECT DISTINCT(H.tahun) tahun FROM tlaba_rugi_setting_anggaran_tahun H WHERE H.template_id='$template_id' ORDER BY H.tahun";
		$rows=$this->db->query($q)->result();
		$opsi='';
		foreach($rows as $row){
			// $opsi .='<option value="'. $row->periode.'" '.($row->periode>=date('Ym')?'selected':'').'>'.get_periode_2($row->periode).'</option>';
			$opsi .='<option value="'. $row->tahun.'" selected>'.($row->tahun).'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));

	}
	function simpan_tahun(){
		$tahun=$this->input->post('tahun');
		$template_id=$this->input->post('template_id');
		
		if ($this->cek_duplicate($periode,$template_id)=='0'){
			$q="INSERT INTO tlaba_rugi_setting_anggaran_tahun (template_id,tahun,section_akun_id,nominal,tipe_nominal,plus_minus)
				SELECT $template_id as template_id,'$tahun' as tahun,H.id as section_akun_id,'0' as nominal,'1' as tipe_nominal,'-' plus_minus FROM tlaba_rugi_setting_section_akun H
				WHERE H.template_id='$template_id'";
			$result=$this->db->query($q);
		}
				// print_r($val);exit();
		
		$this->output->set_output(json_encode($result));
	}
	function load_anggaran_tahun(){
		$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$template_id = $this->input->post('template_id');			
			$tahun = $this->input->post('tahun');	
			foreach($tahun as $index=>$val){
				$q="SELECT H.id FROM tlaba_rugi_setting_section_akun H
					WHERE H.id NOT IN (
					SELECT D.section_akun_id FROM tlaba_rugi_setting_anggaran_tahun D
					WHERE D.template_id='$template_id' AND D.tahun ='$val'
					) AND H.template_id='$template_id'";
				$rows=$this->db->query($q)->result();
				foreach($rows as $row){
					
					$data=array(
						'template_id' =>$template_id,
						'tahun' =>$val,
						'tipe_nominal' =>1,
						'plus_minus' =>'-',
						'section_akun_id' =>$row->id,
						'nominal' =>0,
					);
					$this->db->insert('tlaba_rugi_setting_anggaran_detail',$data);
				}				
			}
			$disabel = $this->input->post('disabel');
			
			$this->select = array();
			$from="
					(
						SELECT *FROM (
						SELECT H.id,H.nama,H.urutan,H.lev,CONCAT('0',LPAD(H.urutan,3,'0')) as v_urutan,H.header_id,'' as idakun,H.tipe,H.var_nama
						,H.formula
						FROM tlaba_rugi_setting_section H
						WHERE H.template_id='$template_id' AND H.lev='0' AND H.tipe!='2'


						UNION ALL

						SELECT H.id,H.nama,H.urutan,H.lev,CONCAT('0',LPAD(H2.urutan,3,'0'),'-1',LPAD(H.urutan,3,'0'))  as v_urutan,H.header_id,'' as idakun,H.tipe,H.var_nama
						,H.formula
						FROM tlaba_rugi_setting_section H
						LEFT JOIN tlaba_rugi_setting_section H2 ON H2.id=H.header_id
						WHERE H.template_id='$template_id' AND H.lev='1' AND H.tipe!='2'

						UNION ALL

						SELECT H.id,CONCAT(A.noakun,'  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     ',A.namaakun) as nama,H.urutan,2 as lev
						,IF(S0.id IS NOT NULL,CONCAT('0',LPAD(S0.urutan,3,'0'),'-',S.lev,LPAD(S.urutan,3,'0'),'-',H.lev,LPAD(H.urutan,3,'0')),CONCAT(S.lev,LPAD(S.urutan,3,'0'),'-',H.lev,LPAD(H.urutan,3,'0'))) as v_urutan,H.section_id as header_id
						,H.idakun as idakun,'' tipe,H.var_nama
						,'' as formula
						FROM tlaba_rugi_setting_section_akun H
						LEFT JOIN makun_nomor A ON A.id=H.idakun
						LEFT JOIN tlaba_rugi_setting_section S ON S.id=H.section_id
						LEFT JOIN tlaba_rugi_setting_section S0 ON S0.id=S.header_id AND S.lev='1'
						WHERE H.template_id='$template_id'
						) H 
						ORDER BY H.v_urutan ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();$this->order  = array();$this->group  = array();
			$this->column_search = array('nama');$this->column_order  = array();

			$list = $this->datatable->get_datatables();
			$data = array();$no = $_POST['start'];
			// print_r($tahun);exit();
		  foreach ($list as $r) {
			  $no++;
			   $result = array();
			   $btn_copy='';
			    $result[] = $r->lev;
			  $result[] = $r->tipe;
			  $result[] =$r->id.($r->tipe!==''?'<strong>':'').TreeView2($r->lev,$r->nama).($r->tipe!==''?'<strong>':'').$r->tipe;
			  
			 foreach($tahun as $x => $val) {
				 if ($r->tipe==''){
					$result[] = $this->detail_tahun($template_id,$r->id,$val);					 
				 }else{
					 if ($r->lev=='0'){
						 $btn_copy='<button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top"  onclick="copy_section('.$val.','.$r->id.')" title="Penyesuan Section" type="button"><i class="fa fa-paper-plane"></i></button>';
					 }else{
						 $btn_copy='<button class="btn btn-warning btn-sm" data-toggle="tooltip"  data-placement="top" onclick="copy_kategori('.$val.','.$r->id.')" title="Penyesuan Kategori" type="button"><i class="fa fa-paper-plane"></i></button>';
						 
					 }
					$result[] = $btn_copy; 
				 }
		     }

			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(),
			  "recordsFiltered" => $this->datatable->count_all(),
			  "data" => $data
		  );
		  echo json_encode($output);
	  
		
	}
	
                                                   
	function detail_tahun($template_id, $id,$tahun){	
		$hasil='';
		$rows=$this->db->query("SELECT H.nominal,H.tipe_nominal,H.plus_minus FROM `tlaba_rugi_setting_anggaran_tahun` H WHERE H.template_id='$template_id' AND H.section_akun_id='$id' AND H.tahun='$tahun'")->row();
		$tipe_nominal=$rows->tipe_nominal;
		$plus_minus=$rows->plus_minus;
		$nominal=number_format($rows->nominal,0, ".", ",");
		$hasil='<div class="input-group" style="width:240px;">';
		$hasil.='	<span class="input-group-btn">';
		$hasil.='		<button class="btn btn-default btn-sm" data-toggle="tooltip"  data-placement="top" onclick="update_tipe('.$tahun.','.$id.','.$tipe_nominal.')" title="Update Rp. atau %" type="button">'.($tipe_nominal=='1'?'Rp':'%').'</i></button>';
		// $hasil.='		<button class="btn btn-default btn-sm" data-toggle="tooltip"  data-placement="top" onclick="update_plus_minus(\''.$tahun.'\',\''.$id.'\',\''.$plus_minus.'\')" title="Jadikan Plus / Minus" type="button">'.$plus_minus.'</button>';
		$hasil.='	</span>';
		$hasil.='	<input class="form-control decimal input-sm inputan" style="width:100%;text-align:right;" onfocus="focusFunction(this)" onblur="blurFunction(this,'.$tahun.','.$id.')"  type="text" placeholder="0" value="'.$nominal.'"> ';
		$hasil.='	<span class="input-group-btn">';
		$hasil.='		<button class="btn btn-primary btn-sm" data-toggle="tooltip"  data-placement="top" onclick="copy_ke_periode('.$tahun.','.$id.')" title="Terapkan Ke Tahun Selanjutnya" type="button"><i class="fa fa-paper-plane"></i></button>';
		$hasil.='	</span>';
		$hasil.='</div>';

		// $hasil=$nominal;
		return $hasil;
	}
	function update_tipe(){
		$tahun=$this->input->post('tahun');
		$template_id=$this->input->post('template_id');
		$section_akun_id=$this->input->post('section_akun_id');
		$tipe_nominal=($this->input->post('tipe_nominal')=='1'?'2':'1');
		$this->db->where('tahun',$tahun);
		$this->db->where('template_id',$template_id);
		$this->db->where('section_akun_id',$section_akun_id);
		$result=$this->db->update('tlaba_rugi_setting_anggaran_tahun',array('tipe_nominal'=>$tipe_nominal));
		$this->output->set_output(json_encode($result));
	}
	function update_plus_minus(){
		$tahun=$this->input->post('tahun');
		$template_id=$this->input->post('template_id');
		$section_akun_id=$this->input->post('section_akun_id');
		$plus_minus=($this->input->post('plus_minus')=='+'?'-':'+');
		$this->db->where('tahun',$tahun);
		$this->db->where('template_id',$template_id);
		$this->db->where('section_akun_id',$section_akun_id);
		$result=$this->db->update('tlaba_rugi_setting_anggaran_tahun',array('plus_minus'=>$plus_minus));
		$this->output->set_output(json_encode($result));
	}
	function date_akun_copy_tahun(){
		$template_id=$this->input->post('template_id');
		$section_akun_id=$this->input->post('section_akun_id');
		$periode=$this->input->post('periode');
		$q_periode="SELECT DISTINCT(H.tahun) FROM tlaba_rugi_setting_anggaran_tahun H 
					WHERE H.template_id='$template_id' AND H.tahun >'$periode'
					ORDER BY H.tahun";
		$rows=$this->db->query($q_periode)->result();
		$opsi='';
		foreach($rows as $row){
			$opsi .='<option value="'. $row->tahun.'">'.($row->tahun).'</option>';
		}
		$arr['periode']=$opsi;
		$q="SELECT H.nominal,H.tipe_nominal,H.plus_minus FROM `tlaba_rugi_setting_anggaran_tahun` H WHERE H.template_id='$template_id' AND H.section_akun_id='$section_akun_id' AND H.tahun='$periode'";
		$rows=$this->db->query($q)->row();
		// $arr['nominal']=$this->db->query($q)->row('nominal');
		$arr['tipe_nominal']=$rows->tipe_nominal;
		$arr['nominal']=$rows->nominal;
		$arr['plus_minus']=$rows->plus_minus;
		$this->output->set_output(json_encode($arr));
	}
	function date_akun_copy_tahun_kategori(){
		$template_id=$this->input->post('template_id');
		$section_akun_id=$this->input->post('section_akun_id');
		$periode=$this->input->post('periode');
		$q_periode="SELECT DISTINCT(H.tahun) FROM tlaba_rugi_setting_anggaran_tahun H 
					WHERE H.template_id='$template_id' AND H.tahun >'$periode'
					ORDER BY H.tahun";
		$rows=$this->db->query($q_periode)->result();
		$opsi='';
		foreach($rows as $row){
			$opsi .='<option value="'. $row->tahun.'">'.($row->tahun).'</option>';
		}
		$arr['periode']=$opsi;
		
		$this->output->set_output(json_encode($arr));
	}
	function update_copy_periode_tahun(){
		
		$header_id=$this->input->post('header_id');
		$periode=$this->input->post('periode');
		$template_id=$this->input->post('template_id');
		$section_akun_id=$this->input->post('section_akun_id');
		$nominal=RemoveComma($this->input->post('nominal'));
		$nominal_kenaikan=RemoveComma($this->input->post('nominal_kenaikan'));
		$persentase_kenaikan=RemoveComma($this->input->post('persentase_kenaikan'));
		$tipe_nominal=RemoveComma($this->input->post('tipe_nominal'));
		$plus_minus=RemoveComma($this->input->post('plus_minus'));
		$decision=$this->input->post('decision');
		$periode_awal=$this->input->post('periode_awal');
		if ($header_id!=''){
			$q="SELECT GROUP_CONCAT(A.id) as section_akun_id FROM (SELECT H.id FROM tlaba_rugi_setting_section H WHERE H.id='$header_id'
				UNION ALL
				SELECT H2.id FROM tlaba_rugi_setting_section H
				INNER JOIN tlaba_rugi_setting_section H2 ON H2.header_id=H.id AND H2.tipe='1'
				WHERE H.id='$header_id') H
				INNER JOIN tlaba_rugi_setting_section_akun A ON A.section_id=H.id
				";
			$section_akun_id=$this->db->query($q)->row('section_akun_id');
			if ($section_akun_id){
				
				$nominal=0;
				foreach($periode as $index=>$val){
					$var_set="";
					if ($decision=='1'){
						$var_set="H.nominal=D.nominal+".$nominal;					
					}
					if ($decision=='2'){
						$nominal=$nominal + $nominal_kenaikan;
						$var_set="H.nominal=D.nominal+".$nominal;
					}
					if ($decision=='3'){
						$var_set="H.nominal=D.nominal+(D.nominal*".$persentase_kenaikan."/100)";					
					}
					if ($decision=='4'){
						$var_set="H.nominal=D.nominal*0";		
					}
					$var_set=$var_set.",H.tipe_nominal='$tipe_nominal',H.plus_minus='$plus_minus' ";
					$q="UPDATE tlaba_rugi_setting_anggaran_tahun H INNER JOIN (
						SELECT H.template_id,H.section_akun_id,H.nominal,H.tipe_nominal,H.plus_minus 
						FROM tlaba_rugi_setting_anggaran_tahun H
						WHERE H.tahun='$periode_awal' AND H.template_id='$template_id'
						) D ON D.section_akun_id=H.section_akun_id AND H.template_id=D.template_id AND H.tahun='$val' AND H.section_akun_id IN (".$section_akun_id.")
						SET ".$var_set."

						";
						// print_r($q);exit();
					$result=$this->db->query($q);
					if ($decision=='3'){
						$periode_awal=$val;				
					}
				}
			}
		}else{
			foreach($periode as $index=>$val){
				$data=array();			
				if ($decision=='1'){
					$data['nominal']=$nominal;
				}
				if ($decision=='2'){
					$data['nominal']=$nominal + $nominal_kenaikan;
					$nominal=$data['nominal'];
				}
				if ($decision=='3'){
					$nominal=$nominal+ ($nominal*$persentase_kenaikan/100);
					$data['nominal']=$nominal;
					$nominal=$data['nominal'];
				}
				if ($decision=='4'){
					$data['nominal']=0;
				}
				$data['tipe_nominal']=$tipe_nominal;
				$data['plus_minus']=$plus_minus;
				$this->db->where('template_id',$template_id);
				$this->db->where('section_akun_id',$section_akun_id);
				$this->db->where('tahun',$val);
				$result=$this->db->update('tlaba_rugi_setting_anggaran_tahun',$data);
			}
		}
		
		
		$this->output->set_output(json_encode($result));
	}
}
