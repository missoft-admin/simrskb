<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_honor extends CI_Controller {

	/**
	 * Setting Jurnal Honor Dokter controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_honor_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_honor_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_honor_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Honor Dokter';
		$data['content'] 		= 'Msetting_jurnal_honor/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Honor Dokter",'#'),
									    			array("List",'mlogic_honor')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function list_pegawai(){
		$idtipe=$this->input->post('idtipe');
		$idkategori=$this->input->post('idkategori');
		$where='';
		if ($idkategori !='#'){
			$where .=" AND M.idkategori='$idkategori'";
		}
		if ($idtipe=='1'){
			$q="SELECT * FROM mdokter M WHERE M.status='1' ".$where." ORDER BY M.nama ASC";
		}else{
			$q="SELECT * FROM mpegawai M  M WHERE M.status='1' ".$where." ORDER BY M.nama ASC";
		}
		
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function list_kategori($idtipe){
		if ($idtipe=='1'){
			$q="SELECT * FROM mdokter_kategori M ORDER BY M.nama ASC";
		}else{
			$q="SELECT * FROM mpegawai_kategori M ORDER BY M.nama ASC";
			
		}
		$opsi='';
		if ($idtipe !='#'){
			
			$row=$this->db->query($q)->result();
			foreach($row as $r){
				$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
			}
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	
	function update(){
		$id_edit=1;
		$data=array(
			'st_auto_posting'=>$this->input->post('st_auto_posting'),
			// 'idakun_kredit'=>$this->input->post('idakun_kredit'),
			'batas_batal'=>$this->input->post('batas_batal'),
		);
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_honor',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	//Honor Dokter
	function load_honor()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.*,D.nama as dokter,MD.nama as kategori
				,CONCAT(A1.noakun,'-',A1.namaakun) as beban
				,CONCAT(A2.noakun,'-',A2.namaakun) as pajak
				,CONCAT(A3.noakun,'-',A3.namaakun) as pembelian
				,CONCAT(A4.noakun,'-',A4.namaakun) as berobat
				,CONCAT(A5.noakun,'-',A5.namaakun) as kasbon
				,CONCAT(A6.noakun,'-',A6.namaakun) as potongan
				FROM `msetting_jurnal_honor_detail` H
				LEFT JOIN mdokter D ON D.id=H.iddokter
				LEFT JOIN mdokter_kategori MD ON MD.id=H.idkategori
				LEFT JOIN makun_nomor A1 ON A1.id=H.idakun_beban
				LEFT JOIN makun_nomor A2 ON A2.id=H.idakun_pajak
				LEFT JOIN makun_nomor A3 ON A3.id=H.idakun_pembelian
				LEFT JOIN makun_nomor A4 ON A4.id=H.idakun_berobat
				LEFT JOIN makun_nomor A5 ON A5.id=H.idakun_kasbon
				LEFT JOIN makun_nomor A6 ON A6.id=H.idakun_potongan

				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','pegawai');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = ($r->idkategori=='0'?text_default('All Kategori'):$r->kategori);
            $row[] = ($r->iddokter=='0'?text_default('All Dokter'):$r->dokter);
            $row[] = $r->beban.' ('.$r->idakun_beban.')';
            $row[] = $r->pajak.' ('.$r->idakun_pajak.')';
            $row[] = $r->pembelian.' ('.$r->idakun_pembelian.')';
            $row[] = $r->berobat.' ('.$r->idakun_berobat.')';
            $row[] = $r->kasbon.' ('.$r->idakun_kasbon.')';
            $row[] = $r->potongan.' ('.$r->idakun_potongan.')';
			$aksi 		= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_honor('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_honor(){
		$id_edit=$this->input->post('id_edit');
		$idkategori=($this->input->post('idkategori')=='#'?0:$this->input->post('idkategori'));
		$iddokter=($this->input->post('iddokter')=='#'?0:$this->input->post('iddokter'));
		$data=array(
			'setting_id'=>1,
			'idkategori'=>$idkategori,
			'iddokter'=>$iddokter,
			'idakun_beban'=>$this->input->post('idakun_beban'),			
			'idakun_pajak'=>$this->input->post('idakun_pajak'),		
			'idakun_pembelian'=>$this->input->post('idakun_pembelian'),			
			'idakun_berobat'=>$this->input->post('idakun_berobat'),			
			'idakun_kasbon'=>$this->input->post('idakun_kasbon'),			
			'idakun_potongan'=>$this->input->post('idakun_potongan'),			
		);
		
		if ($this->cek_duplicate_honor($idkategori,$iddokter)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_honor_detail',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_honor($idkategori,$iddokter){
		$gabung=$idkategori.'-'.$iddokter;
		$q="SELECT *FROM msetting_jurnal_honor_detail S
			WHERE CONCAT(S.idkategori,'-',S.iddokter)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_honor($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_honor_detail');
		echo json_encode($result);
	}
	function list_dokter($idtipe="#"){
		$where='';
		if ($idtipe!='#'){
			$where =" AND M.idkategori='$idtipe'";
		}
		
		$q="SELECT * FROM mdokter M
			WHERE M.`status`='1' ".$where."
			ORDER BY M.nama ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}	
}
