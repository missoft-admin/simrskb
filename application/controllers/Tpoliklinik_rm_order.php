<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

class Tpoliklinik_rm_order extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpoliklinik_rm_order_model');
		$this->load->model('Tpendaftaran_poli_pasien_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Tpoliklinik_rm_order_model');
		$this->load->helper('path');
		
  }
	function index($tab='1'){
		get_ppa_login();
		$log['path_tindakan']='tpoliklinik_rm_order';
		$this->session->set_userdata($log);
		$user_id=$this->session->userdata('user_id');
		// print_r($this->session->userdata('path_tindakan'));exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$q="SELECT H.id as login_mppa_id,H.pegawai_id as login_iddokter,H.pegawai_id as iddokter FROM mppa H WHERE H.user_id='$user_id' AND H.tipepegawai='2' AND H.staktif='1'";
		// $login_iddokter=$this->db->query($q)->row('login_iddokter');	
		if (UserAccesForm($user_acces_form,array('1849'))){
			$data = $this->db->query($q)->row_array();	
			if ($data){
				$data['tab_utama']=11;
				$data['list_poli_array'] 			= $this->Tpoliklinik_rm_order_model->list_tujuan_fisio_array();
				// print_r($data['list_poli_array']);exit;
				$data['list_poli'] 			= $this->Tpoliklinik_rm_order_model->list_tujuan_fisio();
				// $data['list_poli'] 			= $this->Tpoliklinik_rm_order_model->list_poliklinik();
				$data['list_dokter'] 			= $this->Tpoliklinik_rm_order_model->list_ppa_dokter();
				
				$data['idpoli'] 			= '#';
				$data['namapasien'] 			= '';
				$data['tab'] 			= $tab;
				
				$data['tanggal_1'] 			= DMYFormat(date('2023-09-01'));
				$data['tanggal_2'] 			= DMYFormat(date('Y-m-d'));
				$data['error'] 			= '';
				$data['title'] 			= 'My Pasien';
				$data['content'] 		= 'Tpoliklinik_rm_order/index';
				$data['breadcrum'] 	= array(
													  array("RSKB Halmahera",'#'),
													  array("Tindakan Poliklinik",'#'),
													  array("My Pasien",'tpendaftaran_poli_pasien')
													);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				redirect('page404');
			}
		}else{
			redirect('page404');
		}
	}
	public function cetak_rencana_rehab($assesmen_id,$st_create='0'){
		$this->Tpoliklinik_rm_order_model->cetak_rencana_rehab($assesmen_id,0);
	}
	function getIndex_all()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// tipe_pemilik:tipe_pemilik,status:status,nama:nama
		$where='';
		$where_2='';
		$where_tipe_1='';
		$where_tipe_2='';
			$login_ppa_id=$this->session->userdata('login_ppa_id');		
			// print_r($this->session->userdata());exit;
		$idpoli =$this->input->post('idpoli');
		$iddokter =$this->input->post('iddokter');
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$pencarian =$this->input->post('pencarian');
		$tab =$this->input->post('tab');
		$tab_utama =$this->input->post('tab_utama');
		if ($tanggal_1 !=''){
			$where_tipe_1 .=" AND DATE(H.tanggal_permintaan) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal_permintaan) <='".YMDFormat($tanggal_2)."'";
			$where_tipe_2 .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_2)."'";
		}else{
			$where_tipe_1 .=" AND DATE(H.tanggal_permintaan) >='".date('Y-m-d')."'";
			$where_tipe_2 .=" AND DATE(H.created_date) >='".date('Y-m-d')."'";
		}
		if ($pencarian!=''){
			$where_2 .=" AND (H.namapasien LIKE '%".$pencarian."%')";
			$where_2 .=" OR (H.no_medrec LIKE '%".$pencarian."%')";
		}
		if ($tab=='2'){
			$where .=" AND (H.jumlah_kunjungan > 0)";
		}
		if ($tab=='3'){
			$where .=" AND (H.jumlah_kunjungan = 0)";
		}
		if ($tab_utama=='22'){
			$where_2 .=" AND (TK.tipe_rm = '1')";
		}
		if ($tab_utama=='33'){
			$where_2 .=" AND (TK.tipe_rm = '2')";
		}
		$q_alergi=get_alergi_sql();
		// $where='';
		// print_r($tab);exit;
		$this->select = array();
		$from="
				(
					SELECT H.id,H.umurhari,H.umurbulan,H.umurtahun,H.tanggal_lahir,H.jenis_kelamin
					,H.title
					,CASE WHEN TK.st_ranap='1' THEN H2.idtipe ELSE H.idtipe END as idtipe
					,CASE WHEN TK.st_ranap='1' THEN H2.tanggaldaftar ELSE H.tanggaldaftar END as tanggaldaftar
					,H.tanggal,H.namapasien,H.no_medrec as nomedrec
					,H.kode_antrian
					,CASE WHEN TK.st_ranap='1' THEN H2.nopendaftaran ELSE H.nopendaftaran END as nopendaftaran
					
					,CASE WHEN A.id IS NOT NULL THEN 2 ELSE 0 END as riwayat_alergi_header
					,CASE WHEN TK.st_ranap='1' THEN MD2.nama ELSE MD.nama END as nama_dokter
					,CASE WHEN TK.st_ranap='1' THEN CONCAT(MRU.nama,' - ',MKL.nama,' ',MB.nama) ELSE MP.nama END as nama_poli
					,JK.ref as jk
					,CASE WHEN TK.st_ranap='1' THEN MK2.nama ELSE MK.nama END as nama_kelompok
					,MR.nama as nama_rekanan,
					TK.* 
					
					FROM (
						SELECT H.pendaftaran_id,H.tanggal_permintaan,H.jumlah_kunjungan,H.assesmen_id,H.idpasien,1 as tipe_rm,H.iddokter_peminta,H.st_ranap,H.pendaftaran_id_ranap  
						FROM tpoliklinik_rm_order H WHERE H.status_assemen='2' ".$where.$where_tipe_1."
						UNION ALL
						SELECT H.pendaftaran_id,H.created_date as tanggal_permintaan,H.jumlah_kunjungan,H.assesmen_id,H.idpasien,2 as tipe_rm,H.iddokter_peminta,H.st_ranap,H.pendaftaran_id_ranap 
						FROM tpoliklinik_rm_perencanaan H WHERE H.status_assemen='2' ".$where.$where_tipe_2."
					) TK
					INNER JOIN tpoliklinik_pendaftaran H ON H.id=TK.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran H2 ON H2.id=TK.pendaftaran_id_ranap
					LEFT JOIN mruangan MRU ON MRU.id=H2.idruangan
					LEFT JOIN mkelas MKL ON MKL.id=H2.idkelas
					LEFT JOIN mbed MB ON MB.id=H2.idbed
					LEFT JOIN (
								".$q_alergi."				
							  ) A ON A.idpasien=H.idpasien
					LEFT JOIN mdokter MD ON MD.id=H.iddokter
					LEFT JOIN mdokter MD2 ON MD2.id=H2.iddokterpenanggungjawab
					LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
					LEFT JOIN merm_referensi JK ON JK.ref_head_id='1' AND JK.nilai=H.jenis_kelamin
					LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
					LEFT JOIN mpasien_kelompok MK2 ON MK2.id=H2.idkelompokpasien
					LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
					WHERE H.id IS NOT NULL ".$where_2."
					GROUP BY TK.assesmen_id
					ORDER BY TK.tanggal_permintaan DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama_dokter','namapasien','no_medrec');
		$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $def_image='';
		  if ($r->jenis_kelamin=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		  $btn_lihat='';
		  if ($r->tipe_rm=='1'){
			if (UserAccesForm($user_acces_form,array('1852'))){
			  if ($r->st_ranap){
				$btn_lihat='<a class="btn btn-success btn-xs push-5-t pull-left" href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_rm/input_order/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> LIHAT PROGRAM</a>';
			  }else{
				$btn_lihat='<a class="btn btn-success btn-xs push-5-t pull-left" href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$r->pendaftaran_id.'/0/erm_rm/input_order/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> LIHAT PROGRAM</a>';
			  }
			}
		  }else{
			if (UserAccesForm($user_acces_form,array('1861'))){
				if ($r->st_ranap){
					$btn_lihat='<a class="btn btn-success btn-xs push-5-t pull-left" href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_rm/input_perencanaan/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> LIHAT PROGRAM</a>';
				}else{
					$btn_lihat='<a class="btn btn-success btn-xs push-5-t pull-left" href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$r->pendaftaran_id.'/0/erm_rm/input_perencanaan/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> LIHAT PROGRAM</a>';
				}
			}
		  }
		  $btn_setuju='<div class="push-5-t"><button class="btn btn-xs btn-success" onclick="setuju_all('.$r->assesmen_id.')"><i class="si si-check"></i> SETUJUI PERUBAHAN</button></div>';
			  $btn_setuju='';
		  $btn_1='';
		  $btn_rj='';
		  if (UserAccesForm($user_acces_form,array('1625'))){
			  if ($r->st_ranap=='1'){
		  $btn_rj='<a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->id .'/erm_ri" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-primary btn-xs menu_click"><i class="fa fa-fw fa-folder-open-o"></i> ERM RI</a>';
				  
			  }else{
				  
		  $btn_rj='<a href="'.base_url().'tpendaftaran_poli_ttv/tindakan/'.$r->id.'/erm_rj" type="button" data-toggle="tooltip" title="Layani"  class="btn btn-primary btn-xs menu_click"><i class="fa fa-fw fa-folder-open-o"></i> ERM RAJAL</a>';
			  }
		  }
		  $btn_1 .='<table class="block-table text-left" width="10%">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="push-5-t"> '.$btn_rj.'</div>
									'.div_panel_kendali($user_acces_form,$r->id).'
									'.$btn_lihat.'
								</td>
							</tr>
						</tbody>
					</table>';
					
		
          $result[] = $btn_1;
		  $btn_2='';
		  $btn_2 .='<table class="block-table text-left">
						<tbody>
							<tr>
								<td style="width: 15%;">
									<div class="text-center">
										<img class="img-avatar" src="./assets/upload/avatars/'.$def_image.'" alt="">
									</div>
									<div class="text-center bg-warning h5 push-5-t"><strong>
										'.$r->kode_antrian.'</strong>
									</div>
								</td>
								<td class="bg-white" style="width: 85%;">
									<div class="h5 font-w700 text-primary">'.$r->namapasien.' - '.$r->nomedrec.'</div>'.($r->st_ranap=='1'?text_primary('RAWAT INAP'):text_success('RAWAT JALAN')).'
									<div class="h5 text-muted  push-5-t"> '.$r->jk.', '.HumanDateShort($r->tanggal_lahir).' | '.$r->umurtahun.' Tahun '.$r->umurbulan.' Bulan '.$r->umurhari.' Hari </div>
									<div class="h5 text-muted text-uppercase push-5-t"> 
									<div class="btn-group" role="group">
										<button class="btn btn-default btn-sm" type="button">'.text_alergi($r->riwayat_alergi_header).'</button>
										<button class="btn btn-danger  btn-sm" type="button" onclick="lihat_his_alergi_pasien('.$r->idpasien.')"><i class="fa fa-info"></i></button>
									</div>
									
									</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_2;
		  
		  
		  $btn_3='';
		  $btn_3 .='<table class="block-table text-left">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary"> REGISTRATION INFO </div>
									<div class="push-5-t"> '.$r->nopendaftaran.', '.HumanDateLong($r->tanggaldaftar).'</div>
									<div class="text-muted text-uppercase">'.($r->st_ranap=='1'?GetAsalRujukanRI($r->idtipe):GetAsalRujukanR($r->idtipe)).' - '.$r->nama_poli.'</div>
									<div class="text-muted"><i class="fa fa-user-md"></i> '.($r->nama_dokter).'</div>
									<div class="text-uppercase"><i class="fa fa-street-view"></i> '.($r->nama_rekanan?$r->nama_rekanan:$r->nama_kelompok).'</div>
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] = $btn_3;
		  $btn_4='';
		  $btn_4 .='<table class="block-table text-center">
						<tbody>
							<tr>
								
								<td class="bg-white" style="width: 100%;">
									<div class="h5 text-primary">'.$this->status_kunjungan($r->tipe_rm,$r->jumlah_kunjungan).'</div>
									
								</td>
							</tr>
						</tbody>
					</table>';
          $result[] =$btn_4;
          

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function status_kunjungan($tipe_rm,$jumlah_kunjungan){
	  $btn='';
	  if ($tipe_rm=='1'){
		  $btn .='<button class="btn btn-block btn-sm btn-warning push-5" type="button"><i class="fa fa-reorder pull-left"></i>ORDER</button>';
	  }
	  if ($tipe_rm=='2'){
		  $btn .='<button class="btn btn-block btn-sm btn-success push-5" type="button"><i class="si si-plane pull-left"></i>PERENCANAAN</button>';
	  }
	  
	  if ($jumlah_kunjungan >0){
		  $btn .='<button class="btn btn-block btn-sm btn-primary push-5" type="button"><i class="si si-check pull-left"></i>'.$jumlah_kunjungan.' KUNJUNGAN</button>';
	  }else{
		  $btn .='<button class="btn btn-block btn-sm btn-danger push-5" type="button"><i class="si si-close pull-left"></i>BELUM ADA</button>';
	  }
	  return $btn;
  }
  function load_master(){
	$idtarif_header=$this->input->post('idtarif_header');
	$assesmen_id=$this->input->post('assesmen_id');
	$data_user=get_acces();
	$user_acces_form=$data_user['user_acces_form'];

	$this->select = array();
	$from="
			(
				SELECT D.id as idtarif,D.nama,D.path,D.headerpath,D.`level`,M.total,M.kelas,T.id FROM `mtarif_fisioterapi` H 
				INNER JOIN mtarif_fisioterapi D ON D.path LIKE CONCAT(H.path,'%')
				LEFT JOIN mtarif_fisioterapi_detail M ON M.idtarif=D.id AND M.kelas=0
				LEFT JOIN tpoliklinik_rm_order_tarif T ON T.idtarif=D.id AND T.assesmen_id='$assesmen_id'
				WHERE H.id='$idtarif_header' AND D.`status`='1'
				ORDER BY D.path ASC
			) as tbl
		";
	// print_r($from);exit();
	$this->from   = $from;
	$this->join 	= array();


	$this->order  = array();
	$this->group  = array();
	$this->column_search = array('nama');
	$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $aksi='';
		  // if ($r->pilih){
			// $aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						// <input onclick="check_save_master('.$id.','.$r->id.','.$r->pilih.')" type="checkbox" checked><span></span>
					// </label>';	
		  // }else{
			  $aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						<input data-id='.($r->id?$r->id:0).' data-idtarif='.$r->idtarif.'  data-kelas='.($r->kelas?$r->kelas:0).' data-path='.$r->path.' data-level='.$r->level.' type="checkbox" '.($r->id?'checked':'').' class="chck_tarif"><span></span>
					</label>';	
		  // }
          $result[] = $aksi;
          $result[] = $no;
		  if ($r->level==0){
          $result[] = text_primary(TreeView($r->level,$r->nama));
			  
		  }else{
          $result[] = TreeView($r->level,$r->nama);
			  
		  }
          $result[] = number_format($r->total,0).'&nbsp;&nbsp;&nbsp;';
		  

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
    }
	function load_tarif(){
		$assesmen_id=$this->input->post('assesmen_id');
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

		$this->select = array();
		$from="
				(
					SELECT H.* FROM `tpoliklinik_rm_order_tarif` H 
					
					WHERE H.assesmen_id='$assesmen_id'
					ORDER BY H.path
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();


		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama_tarif');
		$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
	  
	  $total=0;
      foreach ($list as $r) {
          $no++;
          $result = array();

         
		  $aksi='';
		  // if ($r->pilih){
			// $aksi .= ' <label class="css-input css-checkbox css-checkbox-primary">
						// <input onclick="check_save_master('.$id.','.$r->id.','.$r->pilih.')" type="checkbox" checked><span></span>
					// </label>';	
		  // }else{
			  $aksi .= '<button type="button" class="btn btn-danger btn-sm" onclick="hapus_tarif('.$r->id.','.$r->level.','.$r->path.')"><i class="fa fa-trash"></button>';	
		  // }
          $result[] = $no;
          $result[] = TreeView($r->level,$r->nama_tarif);
          $result[] = number_format($r->total,0);
          $result[] = $aksi;
		  $total=$total + $r->total;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data,
	      "total" => $total,
      );
      echo json_encode($output);
    }
	function tindakan($pendaftaran_id='',$st_ranap='0',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		get_ppa_login();
		$data_login_ppa=get_ppa_login();
		// print_r($trx_id);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// $data_satuan_ttv=array();
		$data_satuan_ttv=$this->db->query("SELECT *FROM mnadi_satuan,mnafas_satuan,mtd_satuan,msuhu_satuan,makral_satuan,mcahaya_satuan,mgcs_eye_satuan,mgcs_motion_satuan,mgcs_voice_satuan,mpupil_satuan,mspo2_satuan,mgds_satuan")->row_array();
		
		$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id,$st_ranap);
		// print_r($data);exit;
		 $def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tpoliklinik_rm_order/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		$data['error'] 			= '';
		$data['status_assemen'] 			= '0';
		$data['assesmen_id'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'Konsultasi';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
							  array("RSKB Halmahera",'#'),
							  array("Konsultasi ",'#'),
							  array("Dokter",'tpoliklinik_rm_order')
							);
		$data_assemen=array('idpoli'=>'','iddokter_peminta'=>'');
		// print_r($data_assemen);exit;
		//Input Baru
		if ($menu_kiri=='input_order'){
			// print_r($data['idtarif_header']);exit;
			$data_label=$this->Tpoliklinik_rm_order_model->setting_label_rm();
			if ($trx_id=='#'){
				$data_assemen=$this->Tpoliklinik_rm_order_model->get_data_assesmen($pendaftaran_id,$st_ranap);
				
			}else{
			// print_r($menu_kiri);exit;
				$data_assemen=$this->Tpoliklinik_rm_order_model->get_data_assesmen_trx($trx_id);
			}
			if ($data_assemen){
			}else{
				// print_r($data);exit;
				$data['idtarif_header']=$this->Tpoliklinik_rm_order_model->list_header_path($data['idtipe_poli'],$data['idkelompokpasien'],$data['idrekanan']);
				$data_assemen=array('iddokter_peminta'=>'');
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
			}
			// print_r($data_assemen);exit;
			$data = array_merge($data, $data_label,$data_assemen);
		}
		//PERENCANAAN
		if ($menu_kiri=='input_perencanaan'){
			$data_label=$this->Tpoliklinik_rm_order_model->setting_label_rm_perencanaan();
			if ($trx_id=='#'){
				$data_assemen=$this->Tpoliklinik_rm_order_model->get_data_assesmen_per($pendaftaran_id,$st_ranap);
				
			}else{
			// print_r($menu_kiri);exit;
				$data_assemen=$this->Tpoliklinik_rm_order_model->get_data_assesmen_trx_per($trx_id);
			}
			if ($data_assemen){
			}else{
				$data_assemen=array('iddokter_peminta'=>'');
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
			}
				
			$data = array_merge($data, $data_label,$data_assemen);
		}
		//PILLIH PROGRAM
		if ($menu_kiri=='pilih_program'){
			// print_r($data);exit;
			$data_label=$this->Tpoliklinik_rm_order_model->setting_label_rm_perencanaan();
			if ($trx_id=='#'){
				$data_assemen=$this->Tpoliklinik_rm_order_model->get_data_assesmen_per($pendaftaran_id);
				
			}else{
			// print_r($menu_kiri);exit;
				$data_assemen=$this->Tpoliklinik_rm_order_model->get_data_assesmen_trx_per($trx_id);
			}
			if ($data_assemen){
			}else{
				$data_assemen=array('iddokter_peminta'=>'');
				$data_assemen['assesmen_id']='';
				$data_assemen['status_assemen']='0';
			}
				
			$data = array_merge($data);
		}
		
		
		$data['st_ranap']=$st_ranap;
		$data['trx_id']=$trx_id;
		// $data['pendaftaran_id']=$pendaftaran_id;
		$data['list_ppa_dokter']=$this->Tpoliklinik_rm_order_model->list_ppa_dokter();
		$data['list_tujuan']=$this->Tpoliklinik_rm_order_model->list_tujuan();
		$data['list_dokter']=$this->Tpoliklinik_rm_order_model->list_dokter_all();
		$data['list_dokter_all']=$this->Tpoliklinik_rm_order_model->list_dokter_all();
		// print_r($data['list_dokter_all']);exit;
		$data['list_poli']=$this->Tpoliklinik_rm_order_model->list_poliklinik();
		$data = array_merge($data, backend_info());
		// $data = array_merge($data,$data_login_ppa,$data_satuan_ttv,$data_header_ttv,$data_assemen, backend_info());
		$this->parser->parse('module_template', $data);
		
	}
	function simpan_assesmen(){
		$assesmen_id=$this->input->post('assesmen_id');
		$tanggal_permintaan=YMDFormat($this->input->post('tanggalpermintaan')). ' ' .$this->input->post('waktupermintaan');
		$data=array(
			'tanggal_permintaan' => $tanggal_permintaan,
			'tujuan_id' => $this->input->post('tujuan_id'),
			'iddokter_peminta' => $this->input->post('iddokter_peminta'),
			'diagnosa' => $this->input->post('diagnosa'),
			'catatan_permintaan' => $this->input->post('catatan_permintaan'),
			'prioritas_cito' => $this->input->post('prioritas_cito'),
			'sebanyak' => $this->input->post('sebanyak'),
			'selama' => $this->input->post('selama'),
			'kontrol_kembali' => $this->input->post('kontrol_kembali'),
			'detail_tambahan' => $this->input->post('detail_tambahan'),
			'status_assemen' =>  $this->input->post('status_assemen'),

		);
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_rm_order',$data);
		
		$this->output->set_output(json_encode($data));
	}
	
	function create_assesmen(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');
	  $idtarif_header=$this->input->post('idtarif_header');
	  
	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
	  }
	  $data=array(
		'tanggal_permintaan' => date('Y-m-d H:i:s'),
		'tanggal_input' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'idtipe' => $idtipe,
		'idpoliklinik' => $idpoliklinik,
		'iddokter_peminta' => $iddokter_peminta,
		'idtarif_header' => $idtarif_header,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_rm_order',$data);
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	
	function batal_assesmen(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	 
	 
	  $data['status_assemen']='0';
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_rm_order',$data);
	  $this->output->set_output(json_encode($hasil));
	}
	
	function simpan_tarif(){
		$idtarif=$this->input->post('idtarif');
		$kelas=$this->input->post('kelas');
		$assesmen_id=$this->input->post('assesmen_id');
		$idpasien=$this->input->post('idpasien');
		$level=$this->input->post('level');
		$path=$this->input->post('path');
		if ($level > 0){
			$q="
				INSERT INTO tpoliklinik_rm_order_tarif (assesmen_id,idpasien,path,level,nama_tarif,idtarif,kelas,jasasarana,group_jasasarana,jasapelayanan,group_jasapelayanan,bhp,group_bhp,biayaperawatan,group_biayaperawatan,total,group_jasasarana_diskon,group_jasapelayanan_diskon,group_bhp_diskon,group_biayaperawatan_diskon,jasasarana_diskon,jasapelayanan_diskon,bhp_diskon,biayaperawatan_dikson)
				SELECT '$assesmen_id','$idpasien','$path','$level',M.nama,idtarif,kelas,jasasarana,group_jasasarana,jasapelayanan,group_jasapelayanan,bhp,group_bhp,biayaperawatan,group_biayaperawatan,total,group_jasasarana_diskon,group_jasapelayanan_diskon,group_bhp_diskon,group_biayaperawatan_diskon,jasasarana_diskon,jasapelayanan_diskon,bhp_diskon,biayaperawatan_dikson
				FROM mtarif_fisioterapi_detail H
				LEFT JOIN mtarif_fisioterapi M ON M.id=H.idtarif
				WHERE H.idtarif='$idtarif' AND H.kelas='$kelas'
			";
			// print_r($q);exit;
			$hasil=$this->db->query($q);
		}else{
			$q="SELECT D.id,D.path,D.level,D.nama, M.* FROM `mtarif_fisioterapi` H 
				INNER JOIN mtarif_fisioterapi D ON D.path LIKE CONCAT(H.path,'%')
				LEFT JOIN mtarif_fisioterapi_detail M ON M.idtarif=D.id AND M.kelas=0
				LEFT JOIN tpoliklinik_rm_order_tarif T ON T.idtarif=D.id AND T.assesmen_id='$assesmen_id'
				WHERE H.id='$idtarif' AND D.`status`='1' AND T.id IS NULL
				ORDER BY D.path ASC";
			$list_data=$this->db->query($q)->result();
			foreach($list_data as $row){
				$data=array(
					'assesmen_id' => $assesmen_id,
					'idpasien' => $idpasien,
					'nama_tarif' => $row->nama,
					'idtarif' => $row->id,
					'path' => $row->path,
					'level' => $row->level,
					'kelas' => $row->kelas,
					'jasasarana' => $row->jasasarana,
					'group_jasasarana' => $row->group_jasasarana,
					'jasapelayanan' => $row->jasapelayanan,
					'group_jasapelayanan' => $row->group_jasapelayanan,
					'bhp' => $row->bhp,
					'group_bhp' => $row->group_bhp,
					'biayaperawatan' => $row->biayaperawatan,
					'group_biayaperawatan' => $row->group_biayaperawatan,
					'total' => $row->total,
					'group_jasasarana_diskon' => $row->group_jasasarana_diskon,
					'group_jasapelayanan_diskon' => $row->group_jasapelayanan_diskon,
					'group_bhp_diskon' => $row->group_bhp_diskon,
					'group_biayaperawatan_diskon' => $row->group_biayaperawatan_diskon,
					'jasasarana_diskon' => $row->jasasarana_diskon,
					'jasapelayanan_diskon' => $row->jasapelayanan_diskon,
					'bhp_diskon' => $row->bhp_diskon,
					'biayaperawatan_dikson' => $row->biayaperawatan_dikson,
				);
				$hasil=$this->db->insert('tpoliklinik_rm_order_tarif',$data);
			}
		}
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_tarif(){
		$id=$this->input->post('id');
		$level=$this->input->post('level');
		$path=$this->input->post('path');
		$assesmen_id=$this->input->post('assesmen_id');
		if ($level>0){
			
			$q="DELETE FROM tpoliklinik_rm_order_tarif WHERE id='$id'";
			$hasil=$this->db->query($q);
		}else{
			$q="DELETE FROM tpoliklinik_rm_order_tarif WHERE assesmen_id='$assesmen_id' AND path LIKE '$path".'%'."'";
			$hasil=$this->db->query($q);
		}
		$this->output->set_output(json_encode($hasil));
	}
	function list_my_order(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$st_ranap=$this->input->post('st_ranap');
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR MR.nopendaftaran LIKE '%".$notransaksi."%')";
		}
		
		if ($iddokter!='#'){
			$where .=" AND (MP.iddokter = '".$iddokter."'";
			$where .=" OR MR.iddokterpenanggungjawab = '".$iddokter."')";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_permintaan) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_permintaan) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND ((DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."')";
			$where .=" OR (DATE(MR.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MR.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'))";
		}
		
		$this->select = array();
		$from="
				(
					SELECT  
					DATEDIFF(NOW(), H.tanggal_permintaan) as selisih
					,CASE WHEN H.st_ranap='0' THEN MP.nopendaftaran ELSE MR.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='0' THEN MP.tanggaldaftar ELSE MR.tanggaldaftar END as tanggaldaftar
					,MP.tanggal
					,CASE WHEN MP.idtipe=1 THEN CASE WHEN H.st_ranap=0 THEN 'POLIKLLINIK' ELSE 'RAWAT INAP' END ELSE CASE WHEN H.st_ranap='0' THEN 'IGD' ELSE 'ODS' END  END as tipe_poli
					,CASE WHEN H.st_ranap='1' THEN CONCAT(MRU.nama,' - ',MK.nama,' ',MB.nama) ELSE P.nama END as nama_poli
					,MD.nama as nama_dokter,mppa.nama as nama_pemberi_resep,MT.nama as nama_tujuan,UC.nama as user_created,SB.ref sebanyak_nama
					,PR.ref as nama_prioritas,SL.ref as selama_nama
					,H.* FROM `tpoliklinik_rm_order` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mkelas MK ON MK.id=MR.idkelas
					LEFT JOIN mbed MB ON MB.id=MR.idbed
					INNER JOIN mpoliklinik P ON P.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					INNER JOIN mppa ON mppa.id=H.iddokter_peminta
					INNER JOIN mpoliklinik MT ON MT.id=H.tujuan_id
					INNER JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN merm_referensi PR ON PR.nilai=H.prioritas_cito AND PR.ref_head_id='113'
					LEFT JOIN merm_referensi SB ON SB.nilai=H.sebanyak AND SB.ref_head_id='112'
					LEFT JOIN merm_referensi SL ON SL.nilai=H.selama AND SL.ref_head_id='111'
					WHERE H.status_assemen='2' AND H.created_ppa='$login_ppa_id' AND H.idpasien='$idpasien' ".$where."
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		if ($r->st_ranap=='1'){
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_rm/input_order/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}else{
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$r->pendaftaran_id.'/0/erm_rm/input_order/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}
		// $btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_assesmen('.$r->assesmen_id.','.$r->pendaftaran_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		$btn_duplikasi='<button class="btn btn-warning  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_order('.$r->assesmen_id.')" type="button"><i class="fa fa-copy"></i></button>';
		// $btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		$btn_print='<button class="btn btn-success  btn-xs" type="button" title="Cetak" type="button"><i class="fa fa-print"></i></button>';
		$btn_ttd='';
		
		
		$aksi = '<div class="btn-group">';
		if (UserAccesForm($user_acces_form,array('1852'))){
			$aksi .= $btn_lihat;	
		}
		if (UserAccesForm($user_acces_form,array('1853'))){
			if ($st_ranap==$r->st_ranap){
			$aksi .= $btn_duplikasi;	
				
			}
		}
		if (UserAccesForm($user_acces_form,array('1854'))){
			$aksi .= $btn_print;	
		}
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nopendaftaran.'<br>'.HumanDateLong($r->tanggaldaftar));
		$result[] = '<span>'.($r->nopermintaan).'</span><br><span class="text-primary">'.HumanDateLong($r->tanggal_permintaan).'</span>';
		$result[] = '<span>'.($r->tipe_poli.' - '.$r->nama_poli).'</span><br><span class="text-success">'.($r->nama_dokter).'</span>';
		$result[] = ($r->nama_pemberi_resep);
		$result[] = ($r->diagnosa);
		$result[] = ($r->nama_tujuan).'<br>'.($r->prioritas_cito=='1'?text_danger($r->nama_prioritas):text_info($r->nama_prioritas));
		$result[] =$r->sebanyak_nama;
		$result[] =$r->selama_nama;
		$result[] = ($r->jumlah_kunjungan>0?text_primary($r->jumlah_kunjungan.' Kunjungan'):text_danger('BELUM ADA KUNJUNGAN'));
		$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date);;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function list_order_rm(){
	  
		$tipe_rm=$this->input->post('tipe_rm');
		$program_rm_id=$this->input->post('program_rm_id');
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR MR.nopendaftaran LIKE '%".$notransaksi."%')";
		}
		
		
		if ($iddokter!='#'){
			$where .=" AND (MP.iddokter = '".$iddokter."'";
			$where .=" OR MR.iddokterpenanggungjawab = '".$iddokter."')";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_permintaan) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_permintaan) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND ((DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."')";
			$where .=" OR (DATE(MR.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MR.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'))";
		}
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_permintaan) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_permintaan) <='".YMDFormat($tanggal_input_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT  
					DATEDIFF(NOW(), H.tanggal_permintaan) as selisih
					,CASE WHEN H.st_ranap='0' THEN MP.nopendaftaran ELSE MR.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='0' THEN MP.tanggaldaftar ELSE MR.tanggaldaftar END as tanggaldaftar
					,MP.tanggal
					,CASE WHEN MP.idtipe=1 THEN CASE WHEN H.st_ranap=0 THEN 'POLIKLLINIK' ELSE 'RAWAT INAP' END ELSE CASE WHEN H.st_ranap='0' THEN 'IGD' ELSE 'ODS' END  END as tipe_poli
					,CASE WHEN H.st_ranap='1' THEN CONCAT(MRU.nama,' - ',MK.nama,' ',MB.nama) ELSE P.nama END as nama_poli
					
					,MD.nama as nama_dokter,mppa.nama as nama_pemberi_resep,MT.nama as nama_tujuan,UC.nama as user_created,SB.ref sebanyak_nama
					,PR.ref as nama_prioritas,SL.ref as selama_nama
					,H.* FROM `tpoliklinik_rm_order` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mkelas MK ON MK.id=MR.idkelas
					LEFT JOIN mbed MB ON MB.id=MR.idbed
					INNER JOIN mpoliklinik P ON P.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					INNER JOIN mppa ON mppa.id=H.iddokter_peminta
					INNER JOIN mpoliklinik MT ON MT.id=H.tujuan_id
					INNER JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN merm_referensi PR ON PR.nilai=H.prioritas_cito AND PR.ref_head_id='113'
					LEFT JOIN merm_referensi SB ON SB.nilai=H.sebanyak AND SB.ref_head_id='112'
					LEFT JOIN merm_referensi SL ON SL.nilai=H.selama AND SL.ref_head_id='111'
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
					ORDER BY H.created_date DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;$this->join 	= array();$this->order  = array();$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli','nopermintaan');$this->column_order  = array();
		$list = $this->datatable->get_datatables(true);$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$btn_disabel_edit='';
		
		$result = array();
		
		if ($r->st_ranap=='1'){
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_rm/input_order/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
		$btn_duplikasi='<button class="btn btn-warning  btn-xs"  type="button" title="Gunakan Program" onclick="apply_order('.$r->assesmen_id.',1,'.$r->pendaftaran_id_ranap.',1)" type="button"><i class="fa fa-level-down"></i> Apply</button>';
			
		}else{
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$r->pendaftaran_id.'/0/erm_rm/input_order/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
		$btn_duplikasi='<button class="btn btn-warning  btn-xs"  type="button" title="Gunakan Program" onclick="apply_order('.$r->assesmen_id.',1,'.$r->pendaftaran_id.',0)" type="button"><i class="fa fa-level-down"></i> Apply</button>';
		}
		// $btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_assesmen('.$r->assesmen_id.','.$r->pendaftaran_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		// $btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		$btn_print='<button class="btn btn-success  btn-xs" type="button" title="Cetak" type="button"><i class="fa fa-print"></i></button>';
		$aksi = '<div class="btn-group">';
			$aksi .= $btn_lihat;	
			$aksi .= $btn_duplikasi;	
			$aksi .= $btn_print;	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($tipe_rm=='1' && $r->assesmen_id==$program_rm_id?text_primary('Sedang Digunakan'):text_default('Tidak Digunakan'));
		$result[] = ($r->nopendaftaran.'<br>'.HumanDateLong($r->tanggaldaftar));
		$result[] = '<span>'.($r->nopermintaan).'</span><br><span class="text-primary">'.HumanDateLong($r->tanggal_permintaan).'</span>';
		$result[] = '<span>'.($r->tipe_poli.' - '.$r->nama_poli).'</span><br><span class="text-success">'.($r->nama_dokter).'</span>';
		$result[] = ($r->nama_pemberi_resep);
		$result[] = ($r->diagnosa);
		$result[] = ($r->nama_tujuan).'<br>'.($r->prioritas_cito=='1'?text_danger($r->nama_prioritas):text_info($r->nama_prioritas));
		$result[] =$r->sebanyak_nama;
		$result[] =$r->selama_nama;
		$result[] = ($r->jumlah_kunjungan>0?text_primary($r->jumlah_kunjungan.' Kunjungan'):text_danger('BELUM ADA KUNJUNGAN'));
		$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date);;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function list_perencanaan(){
	  
		$tipe_rm=$this->input->post('tipe_rm');
		$program_rm_id=$this->input->post('program_rm_id');
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND (MP.nopendaftaran LIKE '%".$notransaksi."%' OR MR.nopendaftaran LIKE '%".$notransaksi."%')";
		}
		
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_permintaan) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_permintaan) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT  
					DATEDIFF(NOW(), H.tanggal_permintaan) as selisih
					,CASE WHEN H.st_ranap='0' THEN MP.nopendaftaran ELSE MR.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='0' THEN MP.tanggaldaftar ELSE MR.tanggaldaftar END as tanggaldaftar
					,MP.tanggal
					,CASE WHEN MP.idtipe=1 THEN CASE WHEN H.st_ranap=0 THEN 'POLIKLLINIK' ELSE 'RAWAT INAP' END ELSE CASE WHEN H.st_ranap='0' THEN 'IGD' ELSE 'ODS' END  END as tipe_poli
					,CASE WHEN H.st_ranap='1' THEN CONCAT(MRU.nama,' - ',MK.nama,' ',MB.nama) ELSE P.nama END as nama_poli
					
					,MD.nama as nama_dokter,mppa.nama as nama_pemberi_resep,MT.nama as nama_tujuan,UC.nama as user_created,SB.ref sebanyak_nama
					,SL.ref as selama_nama
					,H.* FROM `tpoliklinik_rm_perencanaan` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mkelas MK ON MK.id=MR.idkelas
					LEFT JOIN mbed MB ON MB.id=MR.idbed
					INNER JOIN mpoliklinik P ON P.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					INNER JOIN mppa ON mppa.id=H.iddokter_peminta
					INNER JOIN mpoliklinik MT ON MT.id=H.tujuan_id
					INNER JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN merm_referensi SB ON SB.nilai=H.sebanyak AND SB.ref_head_id='112'
					LEFT JOIN merm_referensi SL ON SL.nilai=H.selama AND SL.ref_head_id='111'
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
					ORDER BY H.created_date DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;$this->join 	= array();$this->order  = array();$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli','nopermintaan');$this->column_order  = array();
		$list = $this->datatable->get_datatables(true);$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$btn_disabel_edit='';
		
		$result = array();
		if ($r->st_ranap=='1'){
		$btn_lihat='<a class="btn btn-primary  btn-xs" href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_rm/input_perencanaan/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat Detail</a>';
		$btn_duplikasi='<button class="btn btn-warning  btn-xs"  type="button" title="Gunakan Program" onclick="apply_order('.$r->assesmen_id.',2,'.$r->pendaftaran_id_ranap.',1)" type="button"><i class="fa fa-level-down"></i> Apply</button>';
			
		}else{
			
		$btn_duplikasi='<button class="btn btn-warning  btn-xs"  type="button" title="Gunakan Program" onclick="apply_order('.$r->assesmen_id.',2,'.$r->pendaftaran_id.',0)" type="button"><i class="fa fa-level-down"></i> Apply</button>';
		$btn_lihat='<a class="btn btn-primary  btn-xs" href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$r->pendaftaran_id.'/0/erm_rm/input_perencanaan/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat Detail</a>';
		}
		// $btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_assesmen('.$r->assesmen_id.','.$r->pendaftaran_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		// $btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		$btn_print='<button class="btn btn-success  btn-xs" type="button" title="Cetak" type="button"><i class="fa fa-print"></i></button>';
		
		
		$aksi = '<div class="btn-group">';
			$aksi .= $btn_lihat;	
			$aksi .= $btn_duplikasi;	
			$aksi .= $btn_print;	
			// $aksi .= $btn_email;	
			// $aksi .= $btn_email_his;	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($tipe_rm=='2' && $r->assesmen_id==$program_rm_id?text_primary('Sedang Digunakan'):text_default('Tidak Digunakan'));
		$result[] = ($r->nopendaftaran.'<br>'.HumanDateLong($r->tanggaldaftar));
		$result[] = '<span>'.($r->nopermintaan).'</span><br><span class="text-primary">'.HumanDateLong($r->tanggal_permintaan).'</span>';
		$result[] = '<span>'.($r->tipe_poli.' - '.$r->nama_poli).'</span><br><span class="text-success">'.($r->nama_dokter).'</span>';
		$result[] = ($r->nama_pemberi_resep);
		$result[] = ($r->dengan_diagnosa);
		$result[] = ($r->nama_tujuan);
		$result[] =$r->sebanyak_nama;
		$result[] =$r->selama_nama;
		$result[] = ($r->jumlah_kunjungan>0?text_primary($r->jumlah_kunjungan.' Kunjungan'):text_danger('BELUM ADA KUNJUNGAN'));
		$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date);;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function apply_order(){
		$st_ranap=$this->input->post('st_ranap');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tipe_rm=$this->input->post('tipe_rm');
		$program_rm_id=$this->input->post('program_rm_id');
		$data=array(
			'tipe_rm' => $tipe_rm,
			'program_rm_id' => $program_rm_id,
		);
		$this->db->where('id',$pendaftaran_id);
		if ($st_ranap=='1'){
		$hasil=$this->db->update('trawatinap_pendaftaran',$data);
			
		}else{
			
		$hasil=$this->db->update('tpoliklinik_pendaftaran',$data);
		}
		$this->output->set_output(json_encode($hasil));
		
	}
  function list_history_order(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_ranap=$this->input->post('st_ranap');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_permintaan) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_permintaan) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT  
					DATEDIFF(NOW(), H.tanggal_permintaan) as selisih
					,CASE WHEN H.st_ranap='0' THEN MP.nopendaftaran ELSE MR.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='0' THEN MP.tanggaldaftar ELSE MR.tanggaldaftar END as tanggaldaftar
					,MP.tanggal
					,CASE WHEN MP.idtipe=1 THEN CASE WHEN H.st_ranap=0 THEN 'POLIKLLINIK' ELSE 'RAWAT INAP' END ELSE CASE WHEN H.st_ranap='0' THEN 'IGD' ELSE 'ODS' END  END as tipe_poli
					,CASE WHEN H.st_ranap='1' THEN CONCAT(MRU.nama,' - ',MK.nama,' ',MB.nama) ELSE P.nama END as nama_poli
					,MD.nama as nama_dokter,mppa.nama as nama_pemberi_resep,MT.nama as nama_tujuan,UC.nama as user_created,SB.ref sebanyak_nama
					,PR.ref as nama_prioritas,SL.ref as selama_nama
					,H.* FROM `tpoliklinik_rm_order` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mkelas MK ON MK.id=MR.idkelas
					LEFT JOIN mbed MB ON MB.id=MR.idbed
					INNER JOIN mpoliklinik P ON P.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					INNER JOIN mppa ON mppa.id=H.iddokter_peminta
					INNER JOIN mpoliklinik MT ON MT.id=H.tujuan_id
					INNER JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN merm_referensi PR ON PR.nilai=H.prioritas_cito AND PR.ref_head_id='113'
					LEFT JOIN merm_referensi SB ON SB.nilai=H.sebanyak AND SB.ref_head_id='112'
					LEFT JOIN merm_referensi SL ON SL.nilai=H.selama AND SL.ref_head_id='111'
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$r->pendaftaran_id.'/erm_rm/input_order/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
		// $btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_assesmen('.$r->assesmen_id.','.$r->pendaftaran_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		$btn_duplikasi='<button class="btn btn-warning  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_order('.$r->assesmen_id.')" type="button"><i class="fa fa-copy"></i></button>';
		// $btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		$btn_print='<button class="btn btn-success  btn-xs" type="button" title="Cetak" type="button"><i class="fa fa-print"></i></button>';
		$btn_ttd='';
		
		
		$aksi = '<div class="btn-group">';
		if (UserAccesForm($user_acces_form,array('1856'))){
			$aksi .= $btn_lihat;	
		}
		if (UserAccesForm($user_acces_form,array('1857'))){
			if ($st_ranap==$r->st_ranap){
			$aksi .= $btn_duplikasi;	
				
			}	
		}
		if (UserAccesForm($user_acces_form,array('1858'))){
			$aksi .= $btn_print;	
		}
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nopendaftaran.'<br>'.HumanDateLong($r->tanggaldaftar));
		$result[] = '<span>'.($r->nopermintaan).'</span><br><span class="text-primary">'.HumanDateLong($r->tanggal_permintaan).'</span>';
		$result[] = '<span>'.($r->tipe_poli.' - '.$r->nama_poli).'</span><br><span class="text-success">'.($r->nama_dokter).'</span>';
		$result[] = ($r->nama_pemberi_resep);
		$result[] = ($r->diagnosa);
		$result[] = ($r->nama_tujuan).'<br>'.($r->prioritas_cito=='1'?text_danger($r->nama_prioritas):text_info($r->nama_prioritas));
		$result[] =$r->sebanyak_nama;
		$result[] =$r->selama_nama;
		$result[] = ($r->jumlah_kunjungan>0?text_primary($r->jumlah_kunjungan.' Kunjungan'):text_danger('BELUM ADA KUNJUNGAN'));
		$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date);;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function copy_order(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	   $login_ppa_id=$this->session->userdata('login_ppa_id');
		$template_assesmen_id=$this->input->post('template_assesmen_id');
		$pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
		$st_ranap=$this->input->post('st_ranap');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$idpasien=$this->input->post('idpasien');
		$idpoliklinik=$this->input->post('idpoliklinik');
		$idtipe=$this->input->post('idtipe');
		$idtarif_header=$this->input->post('idtarif_header');
		$q="INSERT INTO tpoliklinik_rm_order
			(idtipe,idpoliklinik,tanggal_input,pendaftaran_id,pendaftaran_id_ranap,st_ranap,idpasien,created_date,created_ppa,pendaftaran_id_kunjungan,st_kunjungan,status_assemen,deleted_ppa,deleted_date,idtarif_header,tanggal_permintaan,tujuan_id,iddokter_peminta,diagnosa,catatan_permintaan,prioritas_cito,sebanyak,selama,kontrol_kembali,detail_tambahan)
			SELECT 
			'$idtipe','$idpoliklinik',NOW(),'$pendaftaran_id','$pendaftaran_id_ranap','$st_ranap','$idpasien',NOW(),'$login_ppa_id',null,0,1,deleted_ppa,deleted_date,idtarif_header,tanggal_permintaan,tujuan_id,iddokter_peminta,diagnosa,catatan_permintaan,prioritas_cito,sebanyak,selama,kontrol_kembali,detail_tambahan
			FROM tpoliklinik_rm_order 
			WHERE assesmen_id='$template_assesmen_id'";
			// print_r($q);exit;
		$this->db->query($q);
		$assesmen_id=$this->db->insert_id();
		
		$q="SELECT H.* FROM `tpoliklinik_rm_order_tarif` H  WHERE H.assesmen_id='$template_assesmen_id'";
		// print_r($q);exit;
		$list_barang=$this->db->query($q)->result();
		$hasil=false;
		foreach($list_barang as $r){
			$data=array(
				'assesmen_id' => $assesmen_id,
				'idpasien' => $idpasien,
				'nama_tarif' => $r->nama_tarif,
				'idtarif' => $r->idtarif,
				'path' => $r->path,
				'level' => $r->level,
				'kelas' => $r->kelas,
				'jasasarana' => $r->jasasarana,
				'group_jasasarana' => $r->group_jasasarana,
				'jasapelayanan' => $r->jasapelayanan,
				'group_jasapelayanan' => $r->group_jasapelayanan,
				'bhp' => $r->bhp,
				'group_bhp' => $r->group_bhp,
				'biayaperawatan' => $r->biayaperawatan,
				'group_biayaperawatan' => $r->group_biayaperawatan,
				'total' => $r->total,
				'group_jasasarana_diskon' => $r->group_jasasarana_diskon,
				'group_jasapelayanan_diskon' => $r->group_jasapelayanan_diskon,
				'group_bhp_diskon' => $r->group_bhp_diskon,
				'group_biayaperawatan_diskon' => $r->group_biayaperawatan_diskon,
				'jasasarana_diskon' => $r->jasasarana_diskon,
				'jasapelayanan_diskon' => $r->jasapelayanan_diskon,
				'bhp_diskon' => $r->bhp_diskon,
				'biayaperawatan_dikson' => $r->biayaperawatan_dikson,
			);

			$hasil=$this->db->insert('tpoliklinik_rm_order_tarif',$data);
		}
		
		$this->output->set_output(json_encode($hasil));
  }
  function create_assesmen_perencanaan(){
	  $login_tipepegawai=$this->session->userdata('login_tipepegawai');
	  $login_pegawai_id=$this->session->userdata('login_pegawai_id');
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $pendaftaran_id_ranap=$this->input->post('pendaftaran_id_ranap');
	  $st_ranap=$this->input->post('st_ranap');
	  $idpasien=$this->input->post('idpasien');
	  $idtipe=$this->input->post('idtipe');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');
	  
	  if ($login_tipepegawai=='2' && $iddokter==$login_pegawai_id){
			$iddokter_peminta=$login_ppa_id;
	  }else{
		  $q="SELECT id FROM mppa WHERE pegawai_id='$iddokter' AND tipepegawai='2'";
		  $iddokter_peminta=$this->db->query($q)->row('id');
	  }
	  $data=array(
		'tanggal_permintaan' => date('Y-m-d H:i:s'),
		'tanggal_input' => date('Y-m-d H:i:s'),
		'pendaftaran_id' => $pendaftaran_id,
		'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
		'st_ranap' => $st_ranap,
		'idpasien' => $idpasien,
		'tujuan_id' => '14',
		'idtipe' => $idtipe,
		'idpoliklinik' => $idpoliklinik,
		'iddokter_peminta' => $iddokter_peminta,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 1,
		
	  );
	  $hasil=$this->db->insert('tpoliklinik_rm_perencanaan',$data);
	  
	  
	  $this->output->set_output(json_encode($hasil));
	}
	function simpan_assesmen_per(){
		$assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			'dengan_diagnosa' => $this->input->post('dengan_diagnosa'),
			'fisio' => $this->input->post('fisio'),
			'sebanyak' => $this->input->post('sebanyak'),
			'selama' => $this->input->post('selama'),
			'kontrol_kembali' => $this->input->post('kontrol_kembali'),
			'status_assemen' =>  $this->input->post('status_assemen'),

		);
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_rm_perencanaan',$data);
		
		$this->output->set_output(json_encode($data));
	}
	function batal_assesmen_per(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	 
	 
	  $data['status_assemen']='0';
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_rm_perencanaan',$data);
	  $this->output->set_output(json_encode($hasil));
	}
	function list_my_order_per(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT  
					DATEDIFF(NOW(), H.tanggal_permintaan) as selisih
					,CASE WHEN H.st_ranap='0' THEN MP.nopendaftaran ELSE MR.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='0' THEN MP.tanggaldaftar ELSE MR.tanggaldaftar END as tanggaldaftar
					,MP.tanggal
					,CASE WHEN MP.idtipe=1 THEN CASE WHEN H.st_ranap=0 THEN 'POLIKLLINIK' ELSE 'RAWAT INAP' END ELSE CASE WHEN H.st_ranap='0' THEN 'IGD' ELSE 'ODS' END  END as tipe_poli
					,CASE WHEN H.st_ranap='1' THEN CONCAT(MRU.nama,' - ',MK.nama,' ',MB.nama) ELSE P.nama END as nama_poli
					
					,MD.nama as nama_dokter,mppa.nama as nama_pemberi_resep,MT.nama as nama_tujuan,UC.nama as user_created,SB.ref sebanyak_nama
					,SL.ref as selama_nama
					,H.* FROM `tpoliklinik_rm_perencanaan` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mkelas MK ON MK.id=MR.idkelas
					LEFT JOIN mbed MB ON MB.id=MR.idbed
					INNER JOIN mpoliklinik P ON P.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					INNER JOIN mppa ON mppa.id=H.iddokter_peminta
					INNER JOIN mpoliklinik MT ON MT.id=H.tujuan_id
					INNER JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN merm_referensi SB ON SB.nilai=H.sebanyak AND SB.ref_head_id='112'
					LEFT JOIN merm_referensi SL ON SL.nilai=H.selama AND SL.ref_head_id='111'
					WHERE H.status_assemen='2' AND H.created_ppa='$login_ppa_id' AND H.idpasien='$idpasien' ".$where."
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		if ($r->st_ranap=='1'){
		$btn_lihat='<a class="btn btn-primary  btn-xs" href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$r->pendaftaran_id_ranap.'/1/erm_rm/input_perencanaan/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat Detail</a>';
			
		}else{
			
		$btn_lihat='<a class="btn btn-primary  btn-xs" href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$r->pendaftaran_id.'/0/erm_rm/input_perencanaan/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat Detail</a>';
		}
		// $btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_assesmen('.$r->assesmen_id.','.$r->pendaftaran_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		$btn_duplikasi='<button class="btn btn-warning  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_order('.$r->assesmen_id.')" type="button"><i class="fa fa-copy"></i></button>';
		// $btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		$btn_print='<a href="'.site_url().'tpoliklinik_rm_order/cetak_rencana_rehab/'.$r->assesmen_id.'" class="btn btn-success  btn-xs" type="button" target="_blank" title="Cetak" type="button"><i class="fa fa-print"></i></a>';
		$btn_ttd='';
		$jenis_surat=2;
			$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
			$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';

		
		$aksi = '<div class="btn-group">';
		if (UserAccesForm($user_acces_form,array('1861'))){
			$aksi .= $btn_lihat;	
		}
		
		if (UserAccesForm($user_acces_form,array('1862'))){
			$aksi .= $btn_print;	
		}
			$aksi .= $btn_email;	
			$aksi .= $btn_email_his;	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nopendaftaran.'<br>'.HumanDateLong($r->tanggaldaftar));
		$result[] = '<span>'.($r->nopermintaan).'</span>';
		$result[] = '<span>'.HumanDateLong($r->created_date).'</span>';
		$result[] = ($r->nama_pemberi_resep);
		$result[] = ($r->dengan_diagnosa);
		$result[] = ($r->fisio);
		$result[] =$r->sebanyak_nama;
		$result[] =$r->selama_nama;
		$result[] = ($r->jumlah_kunjungan>0?text_primary($r->jumlah_kunjungan.' Kunjungan'):text_danger('BELUM ADA KUNJUNGAN'));
		$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date);;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function list_history_order_per(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.created_date) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.created_date) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT  
					DATEDIFF(NOW(), H.tanggal_permintaan) as selisih
					,CASE WHEN H.st_ranap='0' THEN MP.nopendaftaran ELSE MR.nopendaftaran END as nopendaftaran
					,CASE WHEN H.st_ranap='0' THEN MP.tanggaldaftar ELSE MR.tanggaldaftar END as tanggaldaftar
					,MP.tanggal
					,CASE WHEN MP.idtipe=1 THEN CASE WHEN H.st_ranap=0 THEN 'POLIKLLINIK' ELSE 'RAWAT INAP' END ELSE CASE WHEN H.st_ranap='0' THEN 'IGD' ELSE 'ODS' END  END as tipe_poli
					,CASE WHEN H.st_ranap='1' THEN CONCAT(MRU.nama,' - ',MK.nama,' ',MB.nama) ELSE P.nama END as nama_poli
					
					,MD.nama as nama_dokter,mppa.nama as nama_pemberi_resep,MT.nama as nama_tujuan,UC.nama as user_created,SB.ref sebanyak_nama
					,SL.ref as selama_nama
					,H.* FROM `tpoliklinik_rm_perencanaan` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					LEFT JOIN trawatinap_pendaftaran MR ON MR.id=H.pendaftaran_id_ranap
					LEFT JOIN mruangan MRU ON MRU.id=MR.idruangan
					LEFT JOIN mkelas MK ON MK.id=MR.idkelas
					LEFT JOIN mbed MB ON MB.id=MR.idbed
					INNER JOIN mpoliklinik P ON P.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					INNER JOIN mppa ON mppa.id=H.iddokter_peminta
					INNER JOIN mpoliklinik MT ON MT.id=H.tujuan_id
					INNER JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN merm_referensi SB ON SB.nilai=H.sebanyak AND SB.ref_head_id='112'
					LEFT JOIN merm_referensi SL ON SL.nilai=H.selama AND SL.ref_head_id='111'
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli','nopermintaan');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		$btn_lihat='<a class="btn btn-primary  btn-xs" href="'.site_url().'tpoliklinik_rm_order/tindakan/'.$r->pendaftaran_id.'/erm_rm/input_perencanaan/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i> Lihat Detail</a>';
		// $btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_assesmen('.$r->assesmen_id.','.$r->pendaftaran_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		$btn_duplikasi='<button class="btn btn-warning  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_order('.$r->assesmen_id.')" type="button"><i class="fa fa-copy"></i></button>';
		// $btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		$btn_print='<a href="'.site_url().'tpoliklinik_rm_order/cetak_rencana_rehab/'.$r->assesmen_id.'" target="_blank" class="btn btn-success  btn-xs" type="button" title="Cetak" type="button"><i class="fa fa-print"></i></a>';
		$btn_ttd='';
		
		$jenis_surat=2;
			$btn_email='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Kirim Email" onclick="show_email('.$r->assesmen_id.','.$jenis_surat.','.$r->idpasien.')" type="button"><i class="si si-envelope text-success"></i></button>';
			$btn_email_his='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="History Kirim Email" onclick="show_email_history('.$r->assesmen_id.','.$jenis_surat.')" type="button"><i class="fa fa-history text-danger"></i></button>';


		$aksi = '<div class="btn-group">';
		if (UserAccesForm($user_acces_form,array('1861'))){
			$aksi .= $btn_lihat;	
		}
		
		if (UserAccesForm($user_acces_form,array('1862'))){
			$aksi .= $btn_print;	
		}
			$aksi .= $btn_email;	
			$aksi .= $btn_email_his;	
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nopendaftaran.'<br>'.HumanDateLong($r->tanggaldaftar));
		$result[] = '<span>'.($r->nopermintaan).'</span>';
		$result[] = '<span>'.HumanDateLong($r->created_date).'</span>';
		$result[] = ($r->nama_pemberi_resep);
		$result[] = ($r->dengan_diagnosa);
		$result[] = ($r->fisio);
		$result[] =$r->sebanyak_nama;
		$result[] =$r->selama_nama;
		$result[] = ($r->jumlah_kunjungan>0?text_primary($r->jumlah_kunjungan.' Kunjungan'):text_danger('BELUM ADA KUNJUNGAN'));
		$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date);;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
}	
