<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trekap_penggajian extends CI_Controller {

	/**
	 * Rekap Penggajian controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Trekap_penggajian_model');
  }

	function index(){
		$data = array(
			'tahun' => date('Y'),
		);
		$data['error'] 			= '';
		$data['title'] 			= 'Rekap Penggajian';
		$data['content'] 		= 'Trekap_penggajian/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Rekap Penggajian",'trekap_penggajian/index'),
														array("List",'#')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function filter(){
		$data = array(
			'tahun' => $this->input->post('tahun')
		);

    $this->session->set_userdata($data);

		$data['error'] 			= '';
		$data['title'] 			= 'Rekap Penggajian';
		$data['content'] 		= 'Trekap_penggajian/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Rekap Penggajian",'trekap_penggajian/index'),
														array("List",'#')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 			=> '',
			'bulan' 	=> date("m"),
			'tahun' 	=> date("Y"),
			'subyek' 	=> '',
			'status' 	=> '',
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Rekap Penggajian';
		$data['content'] 		= 'Trekap_penggajian/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Rekap Penggajian",'#'),
									    			array("Tambah",'trekap_penggajian')
													);

		$data['list_detail'] = array();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Trekap_penggajian_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 			=> $row->id,
					'bulan' 	=> $row->bulan,
					'tahun' 	=> $row->tahun,
					'subyek' 	=> $row->subyek,
					'status' 	=> $row->status,
				);

				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Rekap Penggajian';
				$data['content']	 	= 'Trekap_penggajian/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Rekap Penggajian",'#'),
											    			array("Ubah",'trekap_penggajian')
															);

				$data['list_detail'] 	= $this->Trekap_penggajian_model->getDetailData($id);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('trekap_penggajian','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('trekap_penggajian','location');
		}
	}

	function review($id){
		if($id != ''){
			$row = $this->Trekap_penggajian_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 			=> $row->id,
					'bulan' 	=> $row->bulan,
					'tahun' 	=> $row->tahun,
					'subyek' 	=> $row->subyek,
					'status' 	=> $row->status,
				);

				$data['error'] 			= '';
				$data['title'] 			= 'Input Rekap Penggajian';
				$data['content']	 	= 'Trekap_penggajian/review';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Rekap Penggajian",'#'),
											    			array("Input",'trekap_penggajian')
															);

				$data['list_detail'] 	= $this->Trekap_penggajian_model->getReviewData($id);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('trekap_penggajian','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('trekap_penggajian','location');
		}
	}

	function delete($id){
		$this->Trekap_penggajian_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('trekap_penggajian','location');
	}

	function save(){
		if($this->input->post('id') == '' ) {
			if($this->Trekap_penggajian_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('trekap_penggajian/index/'.$this->input->post('idtipe'),'location');
			}
		} else {
			if($this->Trekap_penggajian_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('trekap_penggajian/index/'.$this->input->post('idtipe'),'location');
			}
		}
	}

	function updateReview(){
		if($this->Trekap_penggajian_model->updateReview()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('trekap_penggajian/index/'.$this->input->post('idtipe'),'location');
		}
	}

	function getVariableRekapan($idtipevariable)
	{
		$html = '';
		$result = $this->Trekap_penggajian_model->getVariableRekapan($idtipevariable);

		foreach ($result as $row) {
			$html .= '<tr>';
			$html .= '  <td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" class="checkboxVariable"><span></span></label></td>';
			$html .= '  <td style="display:none">'.$row->id.'</td>';
			$html .= '  <td>'.$row->nama.'</td>';
			$html .= '  <td>'.($row->idtipe == 1 ? "Pengeluaran" : "Pengeluaran + Pendapatan").'</td>';
			$html .= '</tr>';
		}

		echo $html;
	}

	function getIndex($uri = '')
	{
			$this->select = array();
			$this->from   = 'trekap_penggajian';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);

			if($uri == 'filter'){
				if ($this->session->userdata('tahun') != "#") {
					$this->where = array_merge($this->where, array('tahun' => $this->session->userdata('tahun')));
				}
			} else {
				$this->where = array_merge($this->where, array('tahun' => date('Y')));
			}

			$this->order  = array(
				'id' => 'ASC'
			);
			$this->group  = array();

			$this->column_search   = array('notransaksi', 'tahun', 'bulan', 'subyek');
			$this->column_order    = array('notransaksi', 'tahun', 'bulan', 'subyek');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->notransaksi;
					$row[] = MONTHFormat($r->bulan);
					$row[] = $r->tahun;
					$row[] = $r->subyek;
					$aksi = '';
					if(button_roles('trekap_penggajian/review')) {
						$aksi .= '<a href="'.site_url().'trekap_penggajian/review/'.$r->id.'" data-toggle="tooltip" title="Input Rekap" class="btn btn-danger btn-sm"><i class="fa fa-list"></i> Input Rekap</a>';
					}
					if ($r->stpencairan != 1) {
						$aksi .= '<div class="btn-group">';
						if(button_roles('trekap_penggajian/update')) {
							$aksi .= '<a href="'.site_url().'trekap_penggajian/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
						}
						if(button_roles('trekap_penggajian/delete')) {
							$aksi .= '<a href="#" data-urlindex="'.site_url().'trekap_penggajian" data-urlremove="'.site_url().'trekap_penggajian/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
						}
					}
					$aksi .= '</div>';
					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
}
