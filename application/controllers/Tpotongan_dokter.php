<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpotongan_dokter extends CI_Controller
{

    /**
     * Potongan Dokter controller.
     * Developer @GunaliRezqiMauludi
     */

    function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Tpotongan_dokter_model');
        $this->load->model('Thonor_dokter_model');
    }

    function index()
    {
        $data = array(
          'tanggal_dari' => date("d/m/Y"),
          'tanggal_sampai' => date("d/m/Y"),
          'iddokter' => '',
          'idpotongan' => '',
          'status_potongan' => '',
          'status_periode' => '',
        );

        $data['uri']        = 'index';
        $data['error']      = '';
        $data['title']      = 'Potongan Dokter';
        $data['content']    = 'Tpotongan_dokter/index';
        $data['breadcrum']  = array(
																array("RSKB Halmahera",'#'),
	                              array("Potongan Dokter",'#'),
	                              array("List",'tpotongan_dokter')
															);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function filter()
    {
        $data = array(
          'tanggal_dari' => $this->input->post('tanggal_dari'),
          'tanggal_sampai' => $this->input->post('tanggal_sampai'),
          'iddokter' => $this->input->post('iddokter'),
          'idpotongan' => $this->input->post('idpotongan'),
          'status_potongan' => $this->input->post('status_potongan'),
          'status_periode' => $this->input->post('status_periode'),
        );

        $this->session->set_userdata($data);

        $data['uri']        = 'filter';
        $data['error']      = '';
        $data['title']      = 'Potongan Dokter';
        $data['content']    = 'Tpotongan_dokter/index';
        $data['breadcrum']  = array(
                                array("RSKB Halmahera",'#'),
                                array("Potongan Dokter",'#'),
                                array("List",'tpotongan_dokter')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function save()
    {
        $tanggal = date("Y-m-d H:i:s");
        $iddokter = $this->input->post('iddokter');
        $namadokter = $this->input->post('namadokter');
        $idpotongan = $this->input->post('idpotongan');
        $catatan = $this->input->post('catatan');
        $nominal = RemoveComma($this->input->post('nominal'));

        $tanggal_pembayaran = YMDFormat($this->input->post('tanggal_pembayaran'));
        $tanggal_jatuhtempo = YMDFormat($this->input->post('tanggal_jatuhtempo'));
        $status_potongan = $this->input->post('status_potongan');
        $status_periode = $this->input->post('status_periode');

        $data = array();
        $data['tanggal']              = $tanggal;
        $data['iddokter']             = $iddokter;
        $data['idpotongan']           = $idpotongan;
        $data['catatan']              = $catatan;
        $data['nominal']              = $nominal;
        $data['status_potongan']      = $status_potongan;

        if ($status_potongan == '2') {
          $data['tanggal_pembayaran']   = $tanggal_pembayaran;
          $data['tanggal_jatuhtempo']   = $tanggal_jatuhtempo;
        } else {
          $data['status_periode']       = $status_periode;
        }

        if ($this->db->insert('tpotongan_dokter', $data)) {
            $idtransaksi = $this->db->insert_id();

            if ($status_potongan == '2') {
              $isStopPeriode = $this->Thonor_dokter_model->checkStatusHonorDokterStopped($iddokter, $tanggal_pembayaran);
              if ($isStopPeriode == null) {
                $idhonor = $this->Thonor_dokter_model->checkStatusHonorDokter($iddokter, $tanggal_pembayaran);

                if ($idhonor == null) {
                    $dataHonorDokter = array(
                      'iddokter' => $iddokter,
                      'namadokter' => $namadokter,
                      'tanggal_pembayaran' => $tanggal_pembayaran,
                      'tanggal_jatuhtempo' => $tanggal_jatuhtempo,
                      'nominal' => 0,
                      'created_at' => date('Y-m-d H:i:s'),
                      'created_by' => $this->session->userdata('user_id')
                    );

                    if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
                        $idhonor = $this->db->insert_id();
                    }
                }
              }

              $dokter = $this->db->query("SELECT mdokter.id,
                  mdokter.nama,
                  mdokter_kategori.id AS idkategori,
                  mdokter_kategori.nama AS namakategori
                FROM
                  mdokter
                JOIN mdokter_kategori ON mdokter_kategori.id=mdokter.idkategori
                WHERE
                  mdokter.id = $iddokter AND
                  mdokter.status = 1")->row();

              $dataDetailHonorDokter = array(
                'idhonor' => $idhonor,
                'idtransaksi' => $idtransaksi,
                'jenis_transaksi' => 'pengeluaran',
                'jenis_tindakan' => 'POTONGAN LAIN LAIN',
                'reference_table' => 'tpotongan_dokter',
                'namatarif' => $catatan,
                'idkategori' => $dokter->idkategori,
                'namakategori' => $dokter->namakategori,
                'iddokter' => $iddokter,
                'namadokter' => $namadokter,
                'jasamedis' => $nominal,
                'potongan_rs' => 0,
                'nominal_potongan_rs' => 0,
                'pajak_dokter' => 0,
                'nominal_pajak_dokter' => 0,
                'jasamedis_netto' => $nominal,
                'tanggal_pemeriksaan' => $tanggal,
                'tanggal_pembayaran' => $tanggal_pembayaran,
                'tanggal_jatuhtempo' => $tanggal_jatuhtempo
              );

              $this->db->insert('thonor_dokter_detail', $dataDetailHonorDokter);
            }
        }
    }

    function delete()
    {
        $idtransaksi = $this->input->post('idrow');
        $this->db->set('status', '0');
        $this->db->where('id', $idtransaksi);
        if ($this->db->update('tpotongan_dokter')) {
            $this->db->where('jenis_transaksi', 'pengeluaran');
            $this->db->where('jenis_tindakan', 'POTONGAN LAIN LAIN');
            $this->db->where('idtransaksi', $idtransaksi);
            $this->db->delete('thonor_dokter_detail');
            return true;
        } else {
            return false;
        }
    }

  	function getIndex($uri)
  	{
    		$data_user = get_acces();
    		$user_acces_form = $data_user['user_acces_form'];

        $this->select = array(
          'tpotongan_dokter.*',
          'mdokter.nama AS namadokter',
          'mpotongan_dokter.nama AS namapotongan'
        );

  			$this->from   = 'tpotongan_dokter';

  			$this->join 	= array(
  				array('mdokter', 'mdokter.id = tpotongan_dokter.iddokter', ''),
  				array('mpotongan_dokter', 'mpotongan_dokter.id = tpotongan_dokter.idpotongan', ''),
  			);

        $this->where  = array(
          'tpotongan_dokter.status' => '1',
        );

        // FILTER
        if($uri == 'filter'){
          if ($this->session->userdata('tanggal_dari') != null && $this->session->userdata('tanggal_sampai') != null) {
  					$this->where = array_merge($this->where, array('DATE(tpotongan_dokter.tanggal) >=' => YMDFormat($this->session->userdata('tanggal_dari'))));
  					$this->where = array_merge($this->where, array('DATE(tpotongan_dokter.tanggal) <=' => YMDFormat($this->session->userdata('tanggal_sampai'))));
  				}
      		if ($this->session->userdata('iddokter') != "#") {
            $this->where = array_merge($this->where, array('mdokter.id' => $this->session->userdata('iddokter')));
          }
      		if ($this->session->userdata('idpotongan') != "#") {
            $this->where = array_merge($this->where, array('mpotongan_dokter.id' => $this->session->userdata('idpotongan')));
          }
      		if ($this->session->userdata('status_potongan') != "#") {
            $this->where = array_merge($this->where, array('status_potongan.id' => $this->session->userdata('status_potongan')));
          }
      		if ($this->session->userdata('status_periode') != "#") {
            $this->where = array_merge($this->where, array('status_periode.id' => $this->session->userdata('status_periode')));
          }
        }

  			$this->order  = array(
          'tpotongan_dokter.id' => 'DESC',
  			);

  			$this->group  = array('tpotongan_dokter.id');

  			$this->column_search   = array('tpotongan_dokter.tanggal', 'mdokter.nama', 'tpotongan_dokter.catatan');
  			$this->column_order    = array('tpotongan_dokter.tanggal', 'mdokter.nama', 'tpotongan_dokter.catatan');
  			$list = $this->datatable->get_datatables();

        $data = array();
  			$no = $_POST['start'];
  			foreach ($list as $r) {
  					$no++;
  					$row = array();

            if ($r->status_potongan == 1) {
              $status_potongan = '<label class="label label-md label-success">Rutin</label>';
              if ($r->status_periode == 1) {
                $status_periode = 'Setiap Tanggal';
              } else {
                $status_periode = 'Setiap Bulan';
              }
            } else {
              $status_potongan = '<label class="label label-md label-warning">Non Rutin</label><br>';
              $status_periode = DMYFormat($r->tanggal_pembayaran);
            }

  					$row[] = $no;
  					$row[] = $r->tanggal;
  					$row[] = $r->namadokter;
  					$row[] = $r->namapotongan;
  					$row[] = $r->catatan;
  					$row[] = number_format($r->nominal);
  					$row[] = $status_potongan;
  					$row[] = $status_periode;

  					$row[] = '<div class="btn-group">
              <button type="button" class="btn btn-sm btn-danger deletePotonganDokter" data-idrow="'.$r->id.'" title="Hapus"><i class="fa fa-trash"></i></a>
              <button type="button" class="btn btn-sm btn-success" title="Print"><i class="fa fa-print"></i></a>
            </div>';

  					$data[] = $row;
  			}
  			$output = array(
  				"draw" => $_POST['draw'],
  				"recordsTotal" => $this->datatable->count_all(),
  				"recordsFiltered" => $this->datatable->count_all(),
  				"data" => $data
  			);
  			echo json_encode($output);
  	}
}
