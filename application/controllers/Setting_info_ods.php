<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_info_ods extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Setting_info_ods_model');
		$this->load->helper('path');
		
  }


	function index($tab='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('2409'))){
				$tab='1';
			}elseif (UserAccesForm($user_acces_form,array('2410'))){
				$tab='2';
			}elseif (UserAccesForm($user_acces_form,array('2411'))){
				$tab='3';
			}elseif (UserAccesForm($user_acces_form,array('2412'))){
				$tab='4';
			}
		}
		if (UserAccesForm($user_acces_form,array('2409','2406','2410'))){
			$data = $this->Setting_info_ods_model->get_assesmen_setting();
			$data_label = $this->Setting_info_ods_model->get_assesmen_label();
			// $data_user = $this->Setting_info_ods_model->get_assesmen_user();
			// print_r($data_spesifik);exit;
			$data['tab'] 			= $tab;
			$data['spesialisasi_id'] 			= '#';
			$data['profesi_id'] 			= '#';
			$data['mppa_id'] 			= '#';
			$data['st_lihat'] 			= '0';
			$data['st_input'] 			= '0';
			$data['st_edit'] 			= '0';
			$data['st_hapus'] 			= '0';
			$data['st_cetak'] 			= '0';
			$data['jenis_pasien'] 			= '#';
			$data['informasi'] 			= '';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Information ODS';
			$data['content'] 		= 'Setting_info_ods/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Setting Information ODS Setting",'setting_info_ods/index')
												);

			$data = array_merge($data,$data_label, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function save_assesmen(){
		if ($this->Setting_info_ods_model->save_assesmen()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_info_ods/index/1','location');
		}
	}
	function save_info_ods_label(){
		if ($this->Setting_info_ods_model->save_label()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_info_ods/index/3','location');
		}
	}
  function simpan_hak_akses(){
		$hasil=$this->cek_duplicate_hak_akses($this->input->post('profesi_id'),$this->input->post('spesialisasi_id'),$this->input->post('mppa_id'));
		if ($hasil==null){	
			$this->profesi_id=$this->input->post('profesi_id');
			$this->spesialisasi_id=$this->input->post('spesialisasi_id');
			$this->mppa_id=$this->input->post('mppa_id');
			$this->st_lihat=$this->input->post('st_lihat');
			$this->st_input=$this->input->post('st_input');
			$this->st_edit=$this->input->post('st_edit');
			$this->st_hapus=$this->input->post('st_hapus');
			$this->st_cetak=$this->input->post('st_cetak');
			
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
		
			$hasil=$this->db->insert('setting_info_ods_akses',$this);
		}else{
			$hasil=null;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_hak_akses($profesi_id,$spesialisasi_id,$mppa_id){
		$q="SELECT *FROM setting_info_ods_akses WHERE  profesi_id='$profesi_id' AND spesialisasi_id='$spesialisasi_id' AND mppa_id='$mppa_id'";
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	function load_hak_akses()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.id,H.profesi_id,P.ref as profesi
							,H.spesialisasi_id,CASE WHEN H.spesialisasi_id=0 THEN 'SEMUA' ELSE S.ref END as spesialisasi 
							,H.mppa_id,M.nama as nama_ppa,H.st_lihat,H.st_input,H.st_edit,H.st_hapus,H.st_cetak
							FROM `setting_info_ods_akses` H
							LEFT JOIN merm_referensi P ON P.nilai=H.profesi_id AND P.ref_head_id='21' AND P.`status`='1'
							LEFT JOIN merm_referensi S ON S.nilai=H.spesialisasi_id AND S.ref_head_id='22'  AND S.`status`='1'
							LEFT JOIN mppa M ON M.id=H.mppa_id AND M.staktif='1'
							ORDER BY H.profesi_id,H.spesialisasi_id



					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_ppa','spesialisasi','profesi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->profesi);
          $result[] = ($r->spesialisasi_id=='0'?text_default($r->spesialisasi):$r->spesialisasi);
          $result[] = ($r->mppa_id=='0'?text_default('SEMUA'):$r->nama_ppa);
          $result[] = ($r->st_lihat?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_input?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_edit?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_hapus?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_cetak?'IZINKAN':text_danger('TIDAK'));
		  // if (UserAccesForm($user_acces_form,array('1603'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_hak_akses('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  // }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_hak_akses(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_info_ods_akses',$this);
	  
	  json_encode($hasil);
	  
  }
 

  function find_mppa($profesi_id){
	  $q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='$profesi_id' AND H.staktif='1'";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($profesi_id!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
  function find_poli($idtipe){
	  $q="SELECT *FROM mpoliklinik H WHERE H.idtipe='$idtipe' AND H.`status`='1'";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($idtipe!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
  function load_info()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*,P.ref as jenis
							FROM `setting_info_ods_informasi` H
							LEFT JOIN merm_referensi P ON P.nilai=H.jenis_pasien AND P.ref_head_id='428' AND P.`status`='1'
							ORDER BY H.id



					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('jenis','informasi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->jenis);
          $result[] = ($r->informasi);
		  // if (UserAccesForm($user_acces_form,array('1603'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_info('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  // }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
   function hapus_info(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_info_ods_informasi',$this);
	  
	  json_encode($hasil);
	  
  }
   function simpan_info(){
		$hasil=$this->cek_duplicate_info($this->input->post('jenis_pasien'));
		if ($hasil==null){	
			$this->jenis_pasien=$this->input->post('jenis_pasien');
			$this->informasi=$this->input->post('informasi');
			
			
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
		
			$hasil=$this->db->insert('setting_info_ods_informasi',$this);
		}else{
			$hasil=null;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_info($jenis_pasien){
		$q="SELECT *FROM setting_info_ods_informasi WHERE  jenis_pasien='$jenis_pasien'";
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
}
