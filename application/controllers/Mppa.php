<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use Hashids\Hashids;
class Mppa extends CI_Controller {

	/**
	 * Checkin controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mppa_model');
		$this->load->helper('path');
		
  }
	function index($tab='1'){
		

		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
		if (UserAccesForm($user_acces_form,array('1589'))){
			$data = array();
			
			$data['tipepegawai'] 			= '0';
			$data['error'] 			= '';
			$data['title'] 			= 'Master PPA';
			$data['content'] 		= 'Mppa/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master",'#'),
												  array("PPA",'mppa')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function add(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1590'))){
			$data=array(
				'tipepegawai' =>'1',
				'tgl_hari'=>'',
				'tgl_bulan'=>'',
				'tgl_tahun'=>'',
				'pegawai_id' =>'',
				'nik' =>'',
				'nama' =>'',
				'jenis_kelamin' =>'',
				'tanggal_lahir' =>'',
				'tempat_lahir' =>'',
				'pendidikan_id' =>'',
				'jenis_profesi_id' =>'',
				'alamat' =>'',
				'provinsi_id' =>'12',
				'kabupaten_id' =>'213',
				'kecamatan_id' =>'',
				'kelurahan_id' =>'',
				'kodepos' =>'',
				'rt' =>'',
				'rw' =>'',
				'alamat_ktp' =>'',
				'provinsi_id_ktp' =>'12',
				'kabupaten_id_ktp' =>'213',
				'kecamatan_id_ktp' =>'',
				'kelurahan_id_ktp' =>'',
				'kodepos_ktp' =>'',
				'rt_ktp' =>'',
				'rw_ktp' =>'',
				'gelar_depan' =>'',
				'gelar_belakang' =>'',
				'nip' =>'',
				'umur_tahun' =>'',
				'umur_bulan' =>'',
				'umur_hari' =>'',
				'agama_id' =>'',
				'spesialisasi_id' =>'',
				'chk_st_domisili' =>'1',
				'warganegara_id' =>'1',
				'warganegara_id_ktp' =>'1',

			);
			$data['list_kontak'] 			= array();
			$data['list_identitas'] 			= array();
			
			$data['statuspasienbaru'] 			= '0';
			$data['id'] 			= '';
			$data['error'] 			= '';
			$data['title'] 			= 'Tambah PPA';
			$data['content'] 		= 'Mppa/add';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master",'#'),
												  array("PPA",'mppa')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function getPegawai()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$where2='';
		$where3='';
		$nip =$this->input->post('snip');
		$namapasien =$this->input->post('snamapegawai');
		$staktif =$this->input->post('sstaktif');
		$tipepegawai =$this->input->post('stipepegawai');
		
		if ($namapasien !=''){
			$where .=" AND (H.nama) LIKE '%".($namapasien)."%'";
			$where2 .=" AND (H.nama) LIKE '%".($namapasien)."%'";
		}
		if ($nip !=''){
			$where .=" AND (H.nip) LIKE '%".($nip)."%'";
			$where2 .=" AND (H.nip) LIKE '%".($nip)."%'";
		}
		
		if ($staktif !='#'){
			$where3 .=" AND (st_terdaftar) ='".($staktif)."'";
		}
		if ($tipepegawai !='#'){
			$where3 .=" AND (tipepegawai) ='".($tipepegawai)."'";
		}
		
		
		$this->select = array();
		$from="
				(
					SELECT *FROM (SELECT '1' as tipepegawai, H.id,H.nip,H.nama 
					,CASE WHEN M.id IS NULL THEN 0 ELSE 1 END as st_terdaftar
					FROM mpegawai H
					LEFT JOIN mppa M ON M.pegawai_id=H.id AND M.tipepegawai='1' AND M.staktif='1'
					WHERE H.`status`='1' ".$where."

					UNION ALL

					SELECT '2' as tipepegawai, H.id,H.nip,H.nama 
					,CASE WHEN M.id IS NULL THEN 0 ELSE 1 END as st_terdaftar
					FROM mdokter H
					LEFT JOIN mppa M ON M.pegawai_id=H.id AND M.tipepegawai='2' AND M.staktif='1'
					WHERE H.`status`='1' ".$where2."
					) T where T.tipepegawai IS NOT NULL ".$where3."
				) as tbl 
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama','nip');
		$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();
		  $result[] = $no;
		  $result[] = ($r->tipepegawai=='1'?('PEGAWAI'):('DOKTER'));
		  $result[] = $r->nip;
		  $result[] = $r->nama;
		  $result[] = ($r->st_terdaftar=='0'?text_default('BELUM TERDAFTAR'):text_success('TERDAFTAR'));
		 
		  $aksi = '<div class="btn-group">';
				$aksi .= '<button onclick="pilih('.$r->tipepegawai.','.$r->id.')" type="button" data-toggle="tooltip" title="Pililh" class="btn btn-primary btn-xs"><i class="fa fa-check"></i> Pilih</button>';
				
		  $aksi .= '</div>';
		  if ($r->st_terdaftar=='1'){
			  $aksi='';
		  }
		  $result[] = $aksi;

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function LoadUser()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$where2='';
		$where3='';
		$nip =$this->input->post('snip');
		$namapasien =$this->input->post('snamapegawai');
		$staktif =$this->input->post('sstaktif');
		$tipepegawai =$this->input->post('stipepegawai');
		
		if ($namapasien !=''){
			$where .=" AND (H.nama) LIKE '%".($namapasien)."%'";
			$where2 .=" AND (H.nama) LIKE '%".($namapasien)."%'";
		}
		if ($nip !=''){
			$where .=" AND (H.nip) LIKE '%".($nip)."%'";
			$where2 .=" AND (H.nip) LIKE '%".($nip)."%'";
		}
		
		if ($staktif !='#'){
			$where3 .=" AND (st_terdaftar) ='".($staktif)."'";
		}
		if ($tipepegawai !='#'){
			$where3 .=" AND (tipepegawai) ='".($tipepegawai)."'";
		}
		
		
		$this->select = array();
		$from="
				(
					SELECT H.id,H.`name` as nama_user,H.username,H.roleid,MR.nama as nama_role 
					,CASE WHEN M.id IS NOT NULL THEN 1 ELSE 0 END as st_terpilih
					FROM musers H
					LEFT JOIN mroles MR ON MR.id=H.roleid
					LEFT JOIN mppa M ON M.user_id=H.id AND M.staktif='1'
					WHERE H.`status`='1'
				) as tbl 
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama_user','username','nama_role');
		$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();
		  $result[] = $no;
		  $result[] = $r->nama_user;
		  $result[] = $r->username;
		  $result[] = $r->nama_role;
		  $result[] = ($r->st_terpilih=='0'?text_default('BELUM TERPILIH'):text_success('TERPILIH'));
		 
		  $aksi = '<div class="btn-group">';
				$aksi .= '<button onclick="pilih_user('.$r->id.')" type="button" data-toggle="tooltip" title="Pililh" class="btn btn-primary btn-xs"><i class="fa fa-check"></i> Pilih</button>';
				
		  $aksi .= '</div>';
		  if ($r->st_terpilih=='1'){
			  $aksi='';
		  }
		  $result[] = $aksi;

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  
  function get_detail_pegwai(){
	  $tipe=$this->input->post('tipe');
	  $id=$this->input->post('id');
	  
	  if ($tipe=='1'){//pegawai
		  $q="SELECT H.id as pegawai_id,H.nip,H.nama,H.tanggallahir,H.alamat,H.jeniskelamin as jenis_kelamin FROM `mpegawai` H WHERE H.id='$id'";
	  }else{
		  $q="SELECT H.id as pegawai_id,H.nip,H.nama,H.tanggallahir,H.alamat,H.jeniskelamin as jenis_kelamin FROM `mdokter` H WHERE H.id='$id'";
	  }
	  $hasil=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($hasil));
  }
  function save(){
		$id = $this->input->post('id');
		$btn_simpan = $this->input->post('btn_simpan');
				// print_r($id);exit;
			
	  // print_r($this->input->post());exit;
			if ($id == '') {
				$id=$this->Mppa_model->saveData();
				if ($id) {
					$data = [];
					$data = array_merge($data, backend_info());
					 $this->session->set_flashdata('confirm', true);
					$this->session->set_flashdata('message_flash', 'Sudah Tersimpan !');
					if ($btn_simpan=='1'){
					redirect('mppa/edit/'.$id,'location');	
						
					}else{
						
					redirect('mppa','location');	
					}
				}
			} else {
		// print_r($this->input->post());exit;
				if ($this->Mppa_model->updateData($id)) {
					$data = [];
					$data = array_merge($data, backend_info());
					 $this->session->set_flashdata('confirm', true);
					$this->session->set_flashdata('message_flash', 'Sudah Tersimpan !');
					// if ($btn_simpan=='1'){
						redirect('mppa/edit/'.$id,'location');	
						
					// }else{
						
						// redirect('mppa','location');	
					// }
				}
			}
		
	}
	function getIndex_all()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			
			$tipepegawai =$this->input->post('tipepegawai');
			$nama =$this->input->post('nama');
			$nip =$this->input->post('nip');
			$jenis_profesi_id =$this->input->post('jenis_profesi_id');
			$spesialisasi_id =$this->input->post('spesialisasi_id');
			$staktif =$this->input->post('staktif');
			if ($nama !=''){
				$where .=" AND (H.nama) LIKE '%".($nama)."%'";
			}
			if ($nip !=''){
				$where .=" AND (H.nip) LIKE '%".($nip)."%'";
			}
			
			if ($tipepegawai !='#'){
				$where .=" AND (H.tipepegawai) ='".($tipepegawai)."'";
			}
			if ($jenis_profesi_id !='#'){
				$where .=" AND (H.jenis_profesi_id) ='".($jenis_profesi_id)."'";
			}
			if ($spesialisasi_id !='#'){
				$where .=" AND (H.spesialisasi_id) ='".($spesialisasi_id)."'";
			}
			if ($staktif !='#'){
				$where .=" AND (H.staktif) ='".($staktif)."'";
			}
			
			$this->select = array();
			$from="
					(
						SELECT H.id,H.tipepegawai,H.nama,H.nip 
						,H.jenis_profesi_id,JP.ref as jenis_profesi,H.spesialisasi_id,JS.ref as spesialisasi,H.staktif
						,H.pegawai_id,MU.name as nama_user
						FROM `mppa` H
						INNER JOIN merm_referensi JP ON JP.nilai=H.jenis_profesi_id AND JP.ref_head_id='21'
						INNER JOIN merm_referensi JS ON JS.nilai=H.spesialisasi_id AND JS.ref_head_id='22'
						LEFT JOIN musers MU ON MU.id=H.user_id
						WHERE H.tipepegawai IS NOT NULL ".$where."
						ORDER BY H.id DESC
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array(); 
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nip','nama','spesialisasi','jenis_profesi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
          $result[] = $no;
          $result[] = ($r->tipepegawai=='1'?('PEGAWAI'):('DOKTER'));
          $result[] = $r->nip;
          $result[] = $r->nama;
          $result[] = $r->jenis_profesi;
          $result[] = $r->spesialisasi;
		  $result[] = ($r->nama_user?$r->nama_user:text_default('BELUM TERKONEKSI'));
		  $result[] = ($r->staktif=='0'?text_default('TIDAK AKTIF'):text_success('AKTIF'));
         
          $aksi = '<div class="btn-group">';
			if ($r->staktif=='1'){
				if (UserAccesForm($user_acces_form,array('1591'))){
				$aksi .= '<a href="'.base_url().'mppa/edit/'.$r->id.'" type="button" data-toggle="tooltip" title="Edit" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1593'))){
				if ($r->nama_user){
					$aksi .= '<button onclick="setting_user('.$r->id.')" type="button" data-toggle="tooltip" title="Atur User Akses" class="btn btn-default btn-xs"><i class="fa fa-lock"></i></button>';
					
				}else{
					$aksi .= '<button onclick="setting_user('.$r->id.')" type="button" data-toggle="tooltip" title="Atur User Akses" class="btn btn-primary btn-xs"><i class="fa fa-lock"></i></button>';
					
				}
				}
				
				if (UserAccesForm($user_acces_form,array('1592'))){
				$aksi .= '<button type="button" data-toggle="tooltip" title="Hapus" onclick="myDelete('.$r->id.')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>';
				}
				
				
				
			}
			
			
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function edit($id){
		$data=$this->Mppa_model->get_edit($id);
		$tanggallahir=DMYFormat2($data['tanggal_lahir']);
		$data['tgl_hari']=substr($tanggallahir,0,2);
		$data['tgl_bulan']=substr($tanggallahir,3,2);
		// print_r($tanggallahir.'-'.$data['tgl_bulan']);exit;
		$data['tgl_tahun']=substr($tanggallahir,6,4);
		$data['list_kontak'] 			= $this->Mppa_model->list_kontak($data['id']);
		$data['list_identitas'] 			= $this->Mppa_model->list_identitas($data['id']);

		// print_r($data['list_identitas']);exit;
		$data['error'] 			= '';
		$data['title'] 			= 'Master PPA';
		$data['content'] 		= 'Mppa/add';
		$data['breadcrum'] 	= array(
											  array("RSKB Halmahera",'#'),
											  array("Master",'#'),
											  array("Edit PPA",'mppa')
											);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function hapus_kontak(){
	  $id=$this->input->post('id');
	  $data=array(
		'staktif' =>0,
		'deleted_by'=>$this->session->userdata('user_id'),
		'deleted_date'=>date('Y-m-d H:i:s'),
	  );
	  // print_r($approval_id);exit;
	  $this->db->where('id',$id);
	  $result=$this->db->update('mppa_kontak',$data);
	  $this->output->set_output(json_encode($result));
	}
	function load_kontak($idpasien){
		$q="SELECT *FROM mppa_kontak WHERE mppa_id='$idpasien' AND staktif='1'";
		
		$result=$this->db->query($q)->result();
		$tabel='';
		foreach($result as $r){
			$tabel .='<tr>
						<td>
								<select tabindex="23" name="level_kontak[]" class="js_select2  form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
									<option value="2" '.($r->level_kontak=='2'?'selected':'').'>Secondary</option>
									<option value="1" '.($r->level_kontak=='1'?'selected':'').'>Primary</option>
								</select>
						</td>
						<td>
								<select tabindex="23" name="jenis_kontak[]" class="js_select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
									<option value="">Pilih Opsi</option>
									'.$this->jenis_kontal_list($r->jenis_kontak).'
									
								</select>
						</td>
						<td>
								<input tabindex="29" type="text" class="form-control" placeholder="Kontak" name="nama_kontak[]" value="'.$r->nama_kontak.'"  >
								<input type="hidden" name="id_kontak_tambahan[]" value="'.$r->id.'">
						</td>
						<td>
							<div class="btn-group">
								<button class="btn btn-xs btn-danger" onclick="hapus_kontak_id(this,'.$r->id.')" type="button" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-trash"></i></button>
							</div>
						</td>
					</tr>';
			
		}
		$this->output->set_output(json_encode($tabel));
	}
	function jenis_kontal_list($id){
		$opsi='';
		foreach(list_variable_ref(20) as $row){
		$opsi.='<option value="'.$row->id.'" '.($id == $row->id ? 'selected="selected"' : '').'>'.$row->nama.'</option>';
		}
		return $opsi;
	}
	
	function hapus_identitas(){
	  $id=$this->input->post('id');
	  $data=array(
		'staktif' =>0,
		'deleted_by'=>$this->session->userdata('user_id'),
		'deleted_date'=>date('Y-m-d H:i:s'),
	  );
	  // print_r($approval_id);exit;
	  $this->db->where('id',$id);
	  $result=$this->db->update('mppa_identitas',$data);
	  $this->output->set_output(json_encode($result));
	}
	function hapus_ppa(){
	  $id=$this->input->post('id');
	  $data=array(
		'staktif' =>0,
		'deleted_by'=>$this->session->userdata('user_id'),
		'deleted_date'=>date('Y-m-d H:i:s'),
	  );
	  // print_r($approval_id);exit;
	  $this->db->where('id',$id);
	  $result=$this->db->update('mppa',$data);
	  $this->output->set_output(json_encode($result));
	}
	function pilih_user(){
		  $user_id=$this->input->post('user_id');
		  $mppa_id=$this->input->post('mppa_id');
		  $data=array(
			'user_id' =>$user_id,
		  );
		  // print_r($approval_id);exit;
		  $this->db->where('id',$mppa_id);
		  $result=$this->db->update('mppa',$data);
		  $this->output->set_output(json_encode($result));
    }
	function load_identitas($idpasien){
		$q="SELECT *FROM mppa_identitas WHERE mppa_id='$idpasien' AND staktif='1'";
		
		$result=$this->db->query($q)->result();
		$tabel='';
		foreach($result as $r){
			$tabel .='<tr>
						
						<td>
								<select tabindex="23" name="jenis_id[]" class="js_select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
									<option value="">Pilih Opsi</option>
									'.$this->jenis_id_list($r->jenis_id).'
									
								</select>
						</td>
						<td>
								<input tabindex="29" type="text" class="form-control" placeholder="Kontak" name="nama_identitas[]" value="'.$r->nama_identitas.'"  >
								<input type="hidden" name="id_identitas_tambahan[]" value="'.$r->id.'">
						</td>
						<td>
							<div class="btn-group">
								<button class="btn btn-xs btn-danger" onclick="hapus_identitas_id(this,'.$r->id.')" type="button" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-trash"></i></button>
							</div>
						</td>
					</tr>';
			
		}
		$this->output->set_output(json_encode($tabel));
	}
	function jenis_id_list($id){
		$opsi='';
		foreach(list_variable_ref(10) as $row){
		$opsi.='<option value="'.$row->id.'" '.($id == $row->id ? 'selected="selected"' : '').'>'.$row->nama.'</option>';
		}
		return $opsi;
	}
}
