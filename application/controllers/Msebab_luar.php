<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msebab_luar extends CI_Controller {

	/**
	 * Master External cause controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msebab_luar_model');
  }

	function index(){
		
		$data = array(
			'id' 						=> '',
			'status_aktif' 			=> '#',
			'status' 				=> '1'
		);
		// $data['lokasi_tubuh_list'] = $this->msebab_luar_model->lokasi_tubuh_list();
		// $data['kelompok_operasi_list'] = $this->msebab_luar_model->kelompok_operasi_list();
		$data['poli_list'] = $this->msebab_luar_model->poli_list();
		$data['error'] 			= '';
		$data['title'] 			= 'Master External cause';
		$data['content'] 		= 'Msebab_luar/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master External cause",'#'),
									    			array("List",'Msebab_luar')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'status' 				=> '1'
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master External cause';
		$data['content'] 		= 'Msebab_luar/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master External cause",'#'),
									    			array("Tambah",'Msebab_luar')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function js_icd()
	{
		$arr = $this->msebab_luar_model->js_icd();
		echo json_encode($arr);
	}
	function simpan_add()
	{
		$data=array(
			'msebab_luar_id'=>$this->input->post('id'),
			'icd_id'=>$this->input->post('icd_id'),
			'jenis_id'=>$this->input->post('jenis_id')
			
		);
		
       // print_r($data);
		$result = $this->db->insert('msebab_luar_icd',$data);
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function simpan_edit()
	{
		$data=array(
			'msebab_luar_id'=>$this->input->post('id'),
			'icd_id'=>$this->input->post('icd_id'),
			'jenis_id'=>$this->input->post('jenis_id')
			
		);
		$this->db->where('id',$this->input->post('tedit'));
		
       // print_r($data);
		$result = $this->db->update('msebab_luar_icd',$data);
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function hapus_icd()
	{
		$this->db->where('id',$this->input->post('tedit'));		
       // print_r($data);
		$result = $this->db->delete('msebab_luar_icd');
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function load_icd()
    {
		
		$id     = $this->input->post('id');
		
		$iduser=$this->session->userdata('user_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT *FROM (
				SELECT H.id,I.deskripsi,I.kode,H.icd_id,H.jenis_id,CASE WHEN H.jenis_id='1' THEN 'Primary' ELSE 'Secondary' END as jenis 
				from msebab_luar_icd H
				INNER JOIN icd_10 I ON I.id=H.icd_id 
				WHERE H.msebab_luar_id='$id' ORDER BY H.jenis_id,H.id ASC
				) as T1) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('jenis','deskripsi','kode');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->kode.'-'.$r->deskripsi;
            $row[] = $r->jenis;
            $aksi       = '<div class="btn-group">';	
			
			$aksi 		.= '<button class="btn btn-xs btn-primary edit" title="Edit"><i class="fa fa-pencil"></i></button>';			
			$aksi 		.= '<button class="btn btn-xs btn-danger hapus" title="Hapus"><i class="fa fa-close"></i></button>';
				
			
			$aksi.='</div>';
			
			$row[] = $aksi;			
            $row[] = $r->id;
            $row[] = $r->icd_id;
            $row[] = $r->jenis_id;
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	function update($id){
		
		if($id != ''){
			$row = $this->msebab_luar_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master External cause';
				$data['content']	 	= 'Msebab_luar/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master External cause",'#'),
											    			array("Ubah",'msebab_luar')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('msebab_luar','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('msebab_luar');
		}
	}

	function delete($id){
		// print_r($id);exit();
		$this->msebab_luar_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('msebab_luar','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->msebab_luar_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('msebab_luar/update/'.$id,'location');
				}
			} else {
				if($this->msebab_luar_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('msebab_luar/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 		= validation_errors();
		$data['content'] 	= 'Msebab_luar/manage';

		if($id==''){
			$data['title'] = 'Tambah Master External cause';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master External cause",'#'),
															array("Tambah",'Msebab_luar')
													);
		}else{
			$data['title'] = 'Ubah Master External cause';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master External cause",'#'),
															array("Ubah",'Msebab_luar')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
		$status_aktif     = $this->input->post('status_aktif');
		$tipe_id     = $this->input->post('tipe_id');
		$idpoliklinik     = $this->input->post('idpoliklinik');
		
		$iduser=$this->session->userdata('user_id');
		
		$where='';
		if ($status_aktif !='#'){
			if ($status_aktif=='1') {
				$where .=" AND detail IS NOT NULL";				
			}else{
				$where .=" AND detail IS NULL";					
			}
		}
		
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		$from="(SELECT *FROM (SELECT H.id,H.status, H.nama,
				D.id as detail
				FROM msebab_luar H
				LEFT JOIN msebab_luar_icd D ON D.msebab_luar_id=H.id
				GROUP BY H.id) as T1
					WHERE status <> '0' ".$where."
					 ) as tbl";
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $no;
          $row[] = $r->nama;
		  $status='';
		  if ($r->detail){
			  $status='<span class="label label-success">AKTIF</span>';
		  }else{
			$status='<span class="label label-danger">TIDAK AKTIF</span>';
		  }
          $row[] = $status;
          $aksi = '<div class="btn-group">';
          
		  if (UserAccesForm($user_acces_form,array('310'))){
          	$aksi .= '<a href="'.site_url().'msebab_luar/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
          }
          
		  if (UserAccesForm($user_acces_form,array('311'))){
          	$aksi .= '<a href="#" data-urlindex="'.site_url().'msebab_luar" data-urlremove="'.site_url().'msebab_luar/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
          }
          $aksi .= '</div>';
          $row[] = $aksi;
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
				  
		
  }
	function ajaxSave(){
		if ($this->msebab_luar_model->saveData()) {
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			$data['response']="data telah di simpan";
			$data['code']=200;
			$data['id']=$this->db->insert_id();
			$data['nama']=$this->input->post("nama");
			echo json_encode($data);
		}
	}
}
