<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
class Tbuku_besar_persediaan extends CI_Controller {

	/**
	 * Buku Besar Pembantu Persediaan controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbuku_besar_hutang_setting_model');
		$this->load->model('Tbuku_besar_persediaan_model','model');
  }

	function index(){
		$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d'));
		date_add($date1,date_interval_create_from_date_string("-130 days"));
		date_add($date2,date_interval_create_from_date_string("0 days"));
		$date1= date_format($date1,"Y-m-d");
		$date2= date_format($date2,"Y-m-d");
		
		$data = array();
		$data['tanggal_trx1'] 			= HumanDateShort($date1);
		$data['tanggal_trx2'] 			= HumanDateShort($date2);

		$data['error'] 			= '';
		$data['title'] 			= 'Buku Besar Pembantu Persediaan';
		$data['content'] 		= 'Tbuku_besar_persediaan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Buku Besar Pembantu Persediaan",'#'),
									    			array("List",'mdistributor')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function create($id=''){
		if ($id==''){
			$data = array();	
			$data['id'] 			='';
		}else{
			$data=$this->model->get_spesifik($id);
		}
		// $data['id'] 			= $id;
		$data['error'] 			= '';
		$data['title'] 			= 'Buku Besar Pembantu Persediaan';
		$data['content'] 		= 'Tbuku_besar_persediaan/index_create';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Buku Besar Pembantu Persediaan",'#'),
									    			array("List",'mdistributor')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function getIndex()
	{
		// GetTipeDistributor
		$where='';
		$iduser=$this->input->post('iduser');
		
		$tanggal_trx1=$this->input->post('tanggal_trx1');
		$tanggal_trx2=$this->input->post('tanggal_trx2');
		
		if ($tanggal_trx1 !=''){
			$where .=" AND H.tanggal >='".YMDFormat($tanggal_trx1)."' AND H.tanggal <='".YMDFormat($tanggal_trx2)."'";
		}
		if ($iduser !='#'){
		$where .=" AND H.created_by ='".$iduser."'";
			
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

		$this->select = array();
		$from="
			(
				SELECT * FROM `tbuku_besar_persediaan` H 
				WHERE H.status='1' ".$where."
				ORDER BY H.tanggal DESC
			)tbl
		";
		$this->from   = $from;
		$this->join 	= array();
		$this->where  = array();
		$this->order  = array();
		$this->group  = array();
		$saldo_awal=0;
		$this->column_search   = array();
		$this->column_order    = array();

		$list = $this->datatable->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$str_distributor='';
		foreach ($list as $r) {
		  $no++;
		  $row = array();
		  
		  $row[] = $no;
		  $row[] = ($r->id);
		  $row[] = HumanDateShort($r->tanggal);
		  $row[] = number_format($r->nilai_persediaan,2);
		  $row[] = ($r->created_nama).' '.HumanDateShort($r->created_date);
			   $aksi = '<div class="btn-group">';          
					$aksi .= '<a class="view btn btn-xs btn-primary" href="'.site_url().'tbuku_besar_persediaan/create/'.$r->id.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-list-ul"></i></a>';
					$aksi .= '<button title="Hapus" target="_blank" class="btn btn-xs btn-danger" onclick="hapus('.$r->id.')"><i class="fa fa-trash"></i> Hapus</button>';				
			
			  $aksi .= '</div>';
		
		  $row[] = $aksi;

		  $data[] = $row;
		}
		$output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(),
		  "recordsFiltered" => $this->datatable->count_all(),
		  "data" => $data
		);
		echo json_encode($output);
	}
	function getIndex_create()
	{
		// GetTipeDistributor
		$where='';
		$where2='';
		$idtipe=$this->input->post('idtipe');
		$id=$this->input->post('id');
		$idkategori=$this->input->post('idkategori');
		
		if ($idtipe){
			$idtipe=implode(',',$idtipe);			
		}
		if ($idkategori){
			$idkategori=implode(',',$idkategori);			
		}
		
	
		
		if ($idtipe){
			if ($id==''){
				$where .=" AND m.idtipe IN (".$idtipe.")";
				
			}else{
				$where .=" AND H.idtipe IN (".$idtipe.")";
				
			}
			
		}
		if ($idkategori){
			if ($id==''){
				$where .=" AND COALESCE(A.idkategori,B.idkategori,C.idkategori,D.idkategori) IN (".$idkategori.")";
			}else{
				$where .=" AND H.idkategori IN (".$idkategori.")";
			}
			
		}
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

		$this->select = array();
		if ($id){
			$from="
				(
					SELECT H.id,H.nama_tipe,H.idtipe,SUM(H.nilai) as nilai FROM tbuku_besar_persediaan_detail H
					WHERE H.tbuku_besar_id='$id' ".$where."
					GROUP BY H.idtipe
					ORDER BY H.idtipe
				) tbl
			";
		}else{
			
		$from="
			(
				SELECT H.idtipe,H.nama_tipe,SUM(nilai) as nilai FROM (
				SELECT m.idunitpelayanan as idunit,m.idtipe,m.idbarang,COALESCE(A.idkategori,B.idkategori,C.idkategori,D.idkategori) as idkategori,T.nama_tipe,k.nama as namakategori,COALESCE(A.nama,B.nama,C.nama,D.nama) as nama_barang,COALESCE(A.hargadasar,B.hargadasar,C.hargadasar,D.hargadasar) as hpp,m.stok,(COALESCE(A.hargadasar,B.hargadasar,C.hargadasar,D.hargadasar) * m.stok) as nilai,COALESCE(A.status,B.status,C.status,D.status) as status
				,COALESCE(A.idsatuankecil,B.idsatuan,C.idsatuankecil,D.idsatuan) as idsatuan,msatuan.nama as namasatuan
				,COALESCE(A.kode,B.kode,C.kode,D.kode) as kode
				FROM mgudang_stok m
				LEFT JOIN mdata_alkes A ON A.id=m.idbarang AND m.idtipe = 1
				LEFT JOIN mdata_implan B ON B.id=m.idbarang AND m.idtipe = 2
				LEFT JOIN mdata_obat C ON C.id=m.idbarang AND m.idtipe = 3
				LEFT JOIN mdata_logistik D ON D.id=m.idbarang AND m.idtipe = 4

				INNER JOIN tbuku_besar_persediaan_setting s ON m.idunitpelayanan = s.idunit AND m.idtipe = s.idtipe 
				INNER JOIN mdata_kategori k on k.id=COALESCE(A.idkategori,B.idkategori,C.idkategori,D.idkategori)
				INNER JOIN mdata_tipebarang T ON T.id=m.idtipe
				LEFT JOIN msatuan ON msatuan.id=COALESCE(A.idsatuankecil,B.idsatuan,C.idsatuankecil,D.idsatuan)
				AND check_value(s.idkategori,COALESCE(A.idkategori,B.idkategori,C.idkategori,D.idkategori)) = 1 AND check_value(s.idbarang,m.idbarang) = 1
				WHERE m.stok != 0 ".$where."
				) H

				GROUP BY H.idtipe
			)tbl";
		}
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		$this->where  = array();
		$this->order  = array();
		$this->group  = array();
		$saldo_awal=0;
		$this->column_search   = array();
		$this->column_order    = array();

		$list = $this->datatable->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$total=0;
		
		foreach ($list as $r) {
		  $no++;
		  $row = array();
		  
		  $total=$total + $r->nilai;
		  $row[] = $r->idtipe;
		  $row[] = 'Tipe :';
		  $row[] = $r->nama_tipe;
		  $row[] = '';
		  $row[] = '';
		  $row[] = '';
		  $row[] = number_format($r->nilai,2);
			   $aksi = '<div class="btn-group">';
					$aksi .= '<button data-toggle="tooltip" title="Detail" class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-menu-down"></i></button>';          
			   $aksi .= '</div>';
		
		  $row[] = $aksi;

		  $data[] = $row;
		}
		$row = array();
		$row[] = '';
		$row[] = '';
		$row[] = '<strong>TOTAL</strong>';
		$row[] = '';
		$row[] = '';
		$row[] = '';
		$row[] = '<strong>'.number_format($total,2).'</strong>';
		
		$row[] = '';

		  $data[] = $row;
		$output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(),
		  "recordsFiltered" => $this->datatable->count_all(),
		  "data" => $data
		);
		echo json_encode($output);
	}
	function getIndex_detail()
	{
		// GetTipeDistributor
		$where='';
		$where2='';
		$id=$this->input->post('id');
		$idkategori=$this->input->post('idkategori');
		$idtipe=$this->input->post('idtipe');
		
		// if ($idkategori){
			// $idkategori=implode(',',$idkategori);			
		// }
		if ($idkategori){
			
			$where .=" AND (H.idkategori) ='".$idkategori."'";			
		}
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

		$this->select = array();
		if ($id==''){			
			$from="
				(
					SELECT U.nama as nama_unit,H.idunit,H.idtipe,H.idbarang,H.nama_tipe,H.kode,H.idkategori,H.namakategori,H.nama_barang,H.namasatuan,H.stok,H.hpp,SUM(nilai) as nilai FROM (
					SELECT m.idunitpelayanan as idunit,m.idtipe,m.idbarang,COALESCE(A.idkategori,B.idkategori,C.idkategori,D.idkategori) as idkategori,T.nama_tipe,k.nama as namakategori,COALESCE(A.nama,B.nama,C.nama,D.nama) as nama_barang,COALESCE(A.hargadasar,B.hargadasar,C.hargadasar,D.hargadasar) as hpp,m.stok,(COALESCE(A.hargadasar,B.hargadasar,C.hargadasar,D.hargadasar) * m.stok) as nilai,COALESCE(A.status,B.status,C.status,D.status) as status
					,COALESCE(A.idsatuankecil,B.idsatuan,C.idsatuankecil,D.idsatuan) as idsatuan,msatuan.nama as namasatuan
					,COALESCE(A.kode,B.kode,C.kode,D.kode) as kode
					FROM mgudang_stok m
					LEFT JOIN mdata_alkes A ON A.id=m.idbarang AND m.idtipe = 1
					LEFT JOIN mdata_implan B ON B.id=m.idbarang AND m.idtipe = 2
					LEFT JOIN mdata_obat C ON C.id=m.idbarang AND m.idtipe = 3
					LEFT JOIN mdata_logistik D ON D.id=m.idbarang AND m.idtipe = 4

					INNER JOIN tbuku_besar_persediaan_setting s ON m.idunitpelayanan = s.idunit AND m.idtipe = s.idtipe 
					INNER JOIN mdata_kategori k on k.id=COALESCE(A.idkategori,B.idkategori,C.idkategori,D.idkategori)
					INNER JOIN mdata_tipebarang T ON T.id=m.idtipe
					LEFT JOIN msatuan ON msatuan.id=COALESCE(A.idsatuankecil,B.idsatuan,C.idsatuankecil,D.idsatuan)
					AND check_value(s.idkategori,COALESCE(A.idkategori,B.idkategori,C.idkategori,D.idkategori)) = 1 AND check_value(s.idbarang,m.idbarang) = 1
					WHERE m.stok != 0
					) H
					LEFT JOIN munitpelayanan U ON U.id=H.idunit
					WHERE H.idtipe='$idtipe'  ".$where."
					GROUP BY H.idunit,H.idtipe,H.idbarang
					ORDER BY H.idkategori,H.nama_barang
				)tbl
			";
		}else{
			$from="
				(
				SELECT H.id,H.namakategori,H.idtipe,H.nama_tipe,H.idbarang,H.idunit
				,U.nama as nama_unit,H.kode,H.nama_barang,H.namasatuan,H.stok,H.hpp,H.nilai
				FROM tbuku_besar_persediaan_detail H
				LEFT JOIN munitpelayanan U ON U.id=H.idunit
				WHERE H.tbuku_besar_id='$id' AND H.idtipe='$idtipe' ".$where."
				GROUP BY H.idunit,H.idtipe,H.idbarang
				ORDER BY H.idkategori,H.nama_barang
				)tbl
			";
		}
		$this->from   = $from;
		$this->join 	= array();
		$this->where  = array();
		$this->order  = array();
		$this->group  = array();
		$saldo_awal=0;
		$this->column_search   = array('nama_barang','kode');
		$this->column_order    = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		$str_distributor='';
		foreach ($list as $r) {
		  $no++;
		  $row = array();
		  
		  $row[] = $r->idtipe;
		  $row[] = $r->nama_unit;
		  $row[] = ($r->kode?$r->kode.' ':'').$r->nama_barang;
		  $row[] = $r->namasatuan;
		  $row[] = number_format($r->stok,0);
		  $row[] = number_format($r->hpp,2);
		  $row[] = number_format($r->nilai,2);
			   $aksi = '<div class="btn-group">';
					$aksi 		.= '<a target="_blank" href="'.site_url().'lgudang_stok/kartustok/'.$r->idtipe.'/'.$r->idbarang.'/'.$r->idunit.'" class="btn btn-xs btn-success ks" title="Kartu Stok"><i class="fa fa-database"></i> Kartu Stok</a>';     
			  $aksi .= '</div>';
		
		  $row[] = $aksi;

		  $data[] = $row;
		}
		$output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
		);
		echo json_encode($output);
	}

	
	function tipe_pemesanan2($tipe){
		if ($tipe=='1'){
			$tipe='<span class="label label-primary">PEMBELIAN</span>';
		}
		if ($tipe=='2'){
			$tipe='<span class="label label-danger">RETUR</span>';
			
		}
		if ($tipe=='3'){
			$tipe='<span class="label label-warning">PENGAJUAN</span>';
		}
		if ($tipe=='4'){
			$tipe='<span class="label label-warning">PEMBAYARAN</span>';
		}
		return $tipe;
	}
	public function export()
    {
		// print_r($this->input->post());exit();
        ini_set('memory_limit', '-1');
        set_time_limit(1000);
        $idtipe=$this->input->post('idtipe');
		$id=$this->input->post('id');
		$idkategori=$this->input->post('idkategori');
		$where='';
		
		$r=$this->model->get_spesifik($id);
		$data=array(
			'judul'=>'BUKU BESAR PEMBANTU PERSEDIAAN',
			'nama_tipe'=>'-All Tipe-',
			'nama_kategori'=>'-All Kategori-',
			// 'tanggal'=>'',
			'tanggal'=>$r['tanggal'],
		);
		
		
		if ($idtipe){
			$idtipe=implode(',',$idtipe);		
			$where .=" AND H.idtipe IN (".$idtipe.")";
			$data['nama_tipe']=$this->model->getTipe($idtipe);
			// print_r($data['distributor']);exit();
		}
		if ($idkategori){
			$idkategori=implode(',',$idkategori);		
			$where .=" AND H.idkategori IN (".$idkategori.")";
			$data['nama_kategori']=$this->model->getKategori($idkategori);
			// print_r($data['distributor']);exit();
		}
		// print_r($data);exit();
		// if ($tipe_transaksi){
			// $tipe_transaksi=implode(',',$tipe_transaksi);	
			// $where .=" AND (H.tipe_transaksi) IN (".$tipe_transaksi.")";
		// }
		
		$from="
				SELECT H.id,H.namakategori,H.idtipe,H.nama_tipe,H.idbarang,H.idunit
				,U.nama as nama_unit,H.kode,H.nama_barang,H.namasatuan,H.stok,H.hpp,H.nilai
				FROM tbuku_besar_persediaan_detail H
				LEFT JOIN munitpelayanan U ON U.id=H.idunit
				WHERE H.tbuku_besar_id='$id'  ".$where."
				GROUP BY H.idunit,H.idtipe,H.idbarang
				ORDER BY H.idkategori,H.nama_barang
			";
				// print_r($from);exit();
        $btn    = $this->input->post('btn_export');
        $row_detail=$this->db->query($from)->result();
        
		if ($btn=='1'){
			$this->excel($row_detail,$data);
		}
		if ($btn=='2'){
			$this->pdf($row_detail,$data);
		}
		if ($btn=='3'){
			$this->pdf($row_detail,$data);
		}
		
		
		
    }
	public function pdf($row_detail,$header){
		// print_r($header);exit();
		$dompdf = new Dompdf();
		$data=array();
		$data=array_merge($data,$header);
        
        $data['detail'] = $row_detail;

		$data = array_merge($data, backend_info());

        $html = $this->load->view('Tbuku_besar_persediaan/pdf', $data, true);
		// print_r($html	);exit();
        $dompdf->loadHtml($html);
		
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Jurnal Kas.pdf', array("Attachment"=>0));
	}
	
	function excel($row_detail,$data){
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$activeSheet = $this->excel->getActiveSheet();
		$activeSheet->setTitle($data['judul']);

        // Set Logo
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $logo = FCPATH.'/assets/upload/logo/logoreport.jpg';
        $objDrawing->setPath($logo);
        $objDrawing->setCoordinates('B1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setHeight(70);
        $objDrawing->setWidth(40);
        $objDrawing->setWorksheet($activeSheet);

        // Set Header
        $activeSheet->setCellValue('B1', "                                      RUMAH SAKIT KHUSUS BEDAH HALMAHERA SIAGA");
        $activeSheet->setCellValue('B2', "                                      Jl.LLRE.Martadinata no.28, Telp: +6222-4206061");
        $activeSheet->setCellValue('B3', "                                      Bandung, 40115");
        $activeSheet->mergeCells('B1:AA1');
        $activeSheet->mergeCells('B2:AA2');
        $activeSheet->mergeCells('B3:AA3');
        $activeSheet->getStyle('B1:AA1')->getFont()->setSize(11);
        $activeSheet->getStyle('B2:AA2')->getFont()->setSize(11);
        $activeSheet->getStyle('B3:AA3')->getFont()->setSize(11);

		$activeSheet->setCellValue('B5', "TIPE");
		$activeSheet->setCellValue('C5', $data['nama_tipe']);
		$activeSheet->setCellValue('B6', "KATEGORI");
		$activeSheet->setCellValue('C6', $data['nama_kategori']);
		$activeSheet->setCellValue('B7', "TANGGAL");
		$activeSheet->setCellValue('C7', $data['tanggal']);
		// $activeSheet->setCellValue('B7', "TANGGAL");
		// $activeSheet->setCellValue('C7', $data['tanggal1'].' s/d'. $data['tanggal2']);
		
		// Set Title
		$activeSheet->setCellValue('B9', $data['judul']);
		$activeSheet->mergeCells('B9:H9');
		$activeSheet->getStyle('B9')->getFont()->setBold(true)->setSize(14);
		$activeSheet->getStyle('B9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$x = 11;
		$activeSheet->setCellValue("B$x", "UNIT");
		$activeSheet->setCellValue("C$x", "TIPE");
		$activeSheet->setCellValue("D$x", "KATEGORI");
		$activeSheet->setCellValue("E$x", "BARANG");
		$activeSheet->setCellValue("F$x", "SATUAN");
		$activeSheet->setCellValue("G$x", "STOK");
		$activeSheet->setCellValue("H$x", "HPP");
		$activeSheet->setCellValue("I$x", "NILAI PERSEDIAAN");
		$activeSheet->getStyle("B$x:I$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("B$x:I$x")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
	  'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THICK,
					)
				),
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Calibri'
				)
			)
		);

		
        if (COUNT($row_detail)) {
            foreach ($row_detail as $row) {
				$x = $x+1;
				
				
	            $activeSheet->setCellValue("B$x", ($row->nama_unit));
	            $activeSheet->setCellValue("C$x", $row->nama_tipe);
	            $activeSheet->setCellValue("D$x", ($row->namakategori));
	            $activeSheet->setCellValue("E$x", $row->nama_barang);
	            $activeSheet->setCellValue("F$x", $row->namasatuan);
	            $activeSheet->setCellValue("G$x", $row->stok);
	            $activeSheet->setCellValue("H$x", $row->hpp);
	            $activeSheet->setCellValue("I$x", $row->nilai);
				
	           
				
				
			}
		}
		// $x = $x+1;
		
		$activeSheet->getStyle("B11:D$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$activeSheet->getStyle("H11:I$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$activeSheet->getStyle("E11:E$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$activeSheet->getStyle("F11:I$x")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$activeSheet->getStyle("F11:I$x")->getNumberFormat()->setFormatCode('#,##0');
		$activeSheet->getStyle("B11:I$x")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
					)
				)
			)
		);
		
    	// Set Auto Width Column
    	$activeSheet->getColumnDimension("A")->setWidth(5);
    	$activeSheet->getColumnDimension("B")->setAutoSize(true);
    	$activeSheet->getColumnDimension("C")->setAutoSize(true);
    	$activeSheet->getColumnDimension("D")->setAutoSize(true);
    	$activeSheet->getColumnDimension("E")->setAutoSize(true);
    	$activeSheet->getColumnDimension("F")->setAutoSize(true);
    	$activeSheet->getColumnDimension("G")->setAutoSize(true);
    	$activeSheet->getColumnDimension("H")->setAutoSize(true);
    	$activeSheet->getColumnDimension("i")->setAutoSize(true);
    	// $activeSheet->getColumnDimension("H")->setWidth(45);
    	// $activeSheet->getColumnDimension("H")->setAutoSize(true);

    	header('Content-Type:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    	header('Content-Disposition: attachment;filename="'.$data['judul'].' '.date('Y-m-d-h-i-s').'.xls"');
    	header('Cache-Control: max-age=0');

    	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    	$objWriter->save('php://output');
	}
	function load_detail_kategori($idtipe,$id=''){
		
		if ($id==''){
			
		$q="SELECT H.idunit,H.idtipe,H.idbarang,H.nama_tipe,H.kode,H.idkategori,H.namakategori,H.nama_barang,H.namasatuan,H.stok,H.hpp,SUM(nilai) as nilai FROM (
				SELECT m.idunitpelayanan as idunit,m.idtipe,m.idbarang,COALESCE(A.idkategori,B.idkategori,C.idkategori,D.idkategori) as idkategori,T.nama_tipe,k.nama as namakategori,COALESCE(A.nama,B.nama,C.nama,D.nama) as nama_barang,COALESCE(A.hargadasar,B.hargadasar,C.hargadasar,D.hargadasar) as hpp,m.stok,(COALESCE(A.hargadasar,B.hargadasar,C.hargadasar,D.hargadasar) * m.stok) as nilai,COALESCE(A.status,B.status,C.status,D.status) as status
				,COALESCE(A.idsatuankecil,B.idsatuan,C.idsatuankecil,D.idsatuan) as idsatuan,msatuan.nama as namasatuan
				,COALESCE(A.kode,B.kode,C.kode,D.kode) as kode
				FROM mgudang_stok m
				LEFT JOIN mdata_alkes A ON A.id=m.idbarang AND m.idtipe = 1
				LEFT JOIN mdata_implan B ON B.id=m.idbarang AND m.idtipe = 2
				LEFT JOIN mdata_obat C ON C.id=m.idbarang AND m.idtipe = 3
				LEFT JOIN mdata_logistik D ON D.id=m.idbarang AND m.idtipe = 4

				INNER JOIN tbuku_besar_persediaan_setting s ON m.idunitpelayanan = s.idunit AND m.idtipe = s.idtipe 
				INNER JOIN mdata_kategori k on k.id=COALESCE(A.idkategori,B.idkategori,C.idkategori,D.idkategori)
				INNER JOIN mdata_tipebarang T ON T.id=m.idtipe
				LEFT JOIN msatuan ON msatuan.id=COALESCE(A.idsatuankecil,B.idsatuan,C.idsatuankecil,D.idsatuan)
				AND check_value(s.idkategori,COALESCE(A.idkategori,B.idkategori,C.idkategori,D.idkategori)) = 1 AND check_value(s.idbarang,m.idbarang) = 1
				WHERE m.stok != 0
				) H
				WHERE H.idtipe='$idtipe' 
				GROUP BY H.idkategori
				ORDER BY H.idkategori,H.nama_barang";
		}else{
			$q="SELECT H.id,H.namakategori,H.idtipe,H.nama_tipe,H.idtipe,SUM(H.nilai) as nilai,H.idkategori FROM tbuku_besar_persediaan_detail H
			WHERE H.tbuku_besar_id='$id' AND H.idtipe='$idtipe'

			GROUP BY H.idkategori
			ORDER BY H.idkategori,H.nama_barang";
		}
		// print_r($q);exit();
		$row=$this->db->query($q)->result();
		$no=1;
		$tbl='<table class="table table-striped table-borderless table-header-bg tabel-kategori">
				<thead>
				<tr>
				<th>NO</th>
				<th>KATEGORI</th>
				<th>NILAI PERSEDIAAN</th>
				<th>AKSI</th>
				</tr>
				</thead><tbody>
		';
		foreach($row as $r){
			$tbl .='<tr>';
			$tbl .='<td>'.$no.'</td>';
			$tbl .='<td>'.$r->namakategori.'</td>';
			$tbl .='<td>'.number_format($r->nilai,2).'</td>';
			$aksi = '<div class="btn-group">';
				$aksi .= '<button data-toggle="tooltip" title="Detail" class="btn btn-primary btn-xs" onclick="get_detail(\''.$r->idtipe.'\',\''.$r->nama_tipe.'\',\''.$r->idkategori.'\',\''.$r->namakategori.'\')" ><i class="fa fa-eye"></i> Tampilkan Barang</button>';          
		  $aksi .= '</div>';
			$tbl .='<td>'.$aksi.'</td>';
			$tbl .='</tr>';
			$no++;
		}
		$tbl .='</tbody></table>';
		$arr['detail']=$tbl;
		$this->output->set_output(json_encode($arr));

	}
	function simpan(){
		$data_header=array(
			'tanggal' =>date('Y-m-d'),
			'nilai_persediaan' =>0,
			'created_by' =>$this->session->userdata('user_id'),
			'created_nama' =>$this->session->userdata('user_name'),
			'created_date' =>date('Y-m-d H:i:s'),
		);
		
		$this->db->insert('tbuku_besar_persediaan',$data_header);
		$tbuku_besar_id=$this->db->insert_id();
		$q="
			INSERT INTO tbuku_besar_persediaan_detail (tbuku_besar_id,idunit,idtipe,idbarang,nama_tipe,kode,idkategori,namakategori,nama_barang,idsatuan,namasatuan,stok,hpp,nilai)
				SELECT $tbuku_besar_id as tbuku_besar_id,H.idunit,H.idtipe,H.idbarang,H.nama_tipe,H.kode,H.idkategori,H.namakategori,H.nama_barang,H.idsatuan,IFNULL(H.namasatuan,'') as namasatuan,H.stok,H.hpp,(nilai) as nilai FROM (
				SELECT m.idunitpelayanan as idunit,m.idtipe,m.idbarang,COALESCE(A.idkategori,B.idkategori,C.idkategori,D.idkategori) as idkategori,T.nama_tipe,k.nama as namakategori,COALESCE(A.nama,B.nama,C.nama,D.nama) as nama_barang,COALESCE(A.hargadasar,B.hargadasar,C.hargadasar,D.hargadasar) as hpp,m.stok,(COALESCE(A.hargadasar,B.hargadasar,C.hargadasar,D.hargadasar) * m.stok) as nilai,COALESCE(A.status,B.status,C.status,D.status) as status
				,COALESCE(A.idsatuankecil,B.idsatuan,C.idsatuankecil,D.idsatuan) as idsatuan,msatuan.nama as namasatuan
				,COALESCE(A.kode,B.kode,C.kode,D.kode) as kode
				FROM mgudang_stok m
				LEFT JOIN mdata_alkes A ON A.id=m.idbarang AND m.idtipe = 1
				LEFT JOIN mdata_implan B ON B.id=m.idbarang AND m.idtipe = 2
				LEFT JOIN mdata_obat C ON C.id=m.idbarang AND m.idtipe = 3
				LEFT JOIN mdata_logistik D ON D.id=m.idbarang AND m.idtipe = 4

				INNER JOIN tbuku_besar_persediaan_setting s ON m.idunitpelayanan = s.idunit AND m.idtipe = s.idtipe 
				INNER JOIN mdata_kategori k on k.id=COALESCE(A.idkategori,B.idkategori,C.idkategori,D.idkategori)
				INNER JOIN mdata_tipebarang T ON T.id=m.idtipe
				LEFT JOIN msatuan ON msatuan.id=COALESCE(A.idsatuankecil,B.idsatuan,C.idsatuankecil,D.idsatuan)
				AND check_value(s.idkategori,COALESCE(A.idkategori,B.idkategori,C.idkategori,D.idkategori)) = 1 AND check_value(s.idbarang,m.idbarang) = 1
				WHERE m.stok != 0
				) H
				
				
		";
		$result=$this->db->query($q);
		$q="UPDATE tbuku_besar_persediaan H INNER JOIN (
			SELECT D.tbuku_besar_id,SUM(D.nilai) as nilai FROM tbuku_besar_persediaan_detail D WHERE D.tbuku_besar_id='$tbuku_besar_id'
			) T ON T.tbuku_besar_id=H.id
			SET H.nilai_persediaan=T.nilai";
		$result=$this->db->query($q);
		
		$this->output->set_output(json_encode($result));
		
	}
	function hapus($id){
		$data_header=array(
			'status' =>0,
			'deleted_by' =>$this->session->userdata('user_id'),
			'deleted_nama' =>$this->session->userdata('user_name'),
			'deleted_date' =>date('Y-m-d H:i:s'),
			
		);
		$this->db->where('id',$id);
		$result=$this->db->update('tbuku_besar_persediaan',$data_header);
		$this->output->set_output(json_encode($result));
		
	}
}
