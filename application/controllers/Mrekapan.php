<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrekapan extends CI_Controller {

	/**
	 * Master Rekapan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mrekapan_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Rekapan';
		$data['content'] 		= 'Mrekapan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master Rekapan",'#'),
									    			array("List",'mrekapan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'st_pendapatan'			=> '0',
			'status' 				=> '',
			'idakun' 				=> '#'
		);

		$data['list_akun'] 			= $this->Mrekapan_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Rekapan';
		$data['content'] 		= 'Mrekapan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master Rekapan",'#'),
									    			array("Tambah",'mrekapan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mrekapan_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'st_pendapatan' 		=> $row->st_pendapatan,
					'idakun' 		=> $row->idakun,
					'status' 				=> $row->status
				);
				$data['list_akun'] 			= $this->Mrekapan_model->list_akun();
				$data['list_jenis'] 			= $this->Mrekapan_model->list_jenis();
				$data['list_rekening'] 			= $this->Mrekapan_model->list_rekening();
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Master Rekapan';
				$data['content']	 	= 'Mrekapan/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Rekapan",'#'),
											    			array("Ubah",'mrekapan')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mrekapan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mrekapan');
		}
	}
	// function getVariableRekapan($idjenis='1')
	// {
		// $html = '';
		// $where = '';
		// // if ($idtipevariable!='0'){
			// // $where="AND M.idtipe='$idtipevariable'";
		// // }
		// $q="SELECT *FROM makun_nomor";
		// $result = $this->db->query($q)->result();

		// foreach ($result as $row) {
			// $html .= '<tr>';
			// $html .= '  <td class="text-center"><label class="css-input css-checkbox css-checkbox-danger"><input type="checkbox" class="checkboxVariable"><span></span></label></td>';
			// $html .= '  <td style="display:none">'.$row->id.'</td>';
			// $html .= '  <td>'.$row->namaakun.'</td>';
			// $html .= '  <td></td>';
			// $html .= '</tr>';
		// }

		// echo $html;
	// }
	function delete($id){
		$this->Mrekapan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mrekapan','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		// print_r($this->input->post());exit();
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->Mrekapan_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mrekapan/update/'.$id,'location');
				}
			} else {
				if($this->Mrekapan_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mrekapan','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}
	
	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mrekapan/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Rekapan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Rekapan",'#'),
															array("Tambah",'mrekapan')
													);
		}else{
			$data['title'] = 'Ubah Master Rekapan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Rekapan",'#'),
															array("Ubah",'mrekapan')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$this->from   = 'mrekap';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama');
      $this->column_order    = array('nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					// if (UserAccesForm($user_acces_form,array('131'))){
						$aksi .= '<a href="'.site_url().'mrekapan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
					// }
					// if (UserAccesForm($user_acces_form,array('132'))){
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mrekapan" data-urlremove="'.site_url().'mrekapan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
					// }
					
			        $aksi .= '</div>';

          $row[] = $no;
          $row[] = $r->nama;
          $row[] = ($r->st_pendapatan?'<span class="label label-primary">Dijadikan Pendapatan</span>':'<span class="label label-danger">TIDAK</span>');
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function getVariable()
    {
	$where='';
	 $idjenis=$this->input->post('idjenis');
	 if ($idjenis !='0'){
		 $where=" AND S.idjenis='$idjenis'";
	 }
	 $idrekap=$this->input->post('idrekap');
			$this->select = array();
			$this->from   = "(
				SELECT S.id,S.idvariable,S.idjenis,M.nama,J.nama as jenis 
				FROM `mjenis_gaji_setting` S
				LEFT JOIN mvariable M ON M.id=S.idvariable
				LEFT JOIN mjenis_gaji J ON J.id=S.idjenis
				WHERE S.idsub !='1' ".$where." AND S.id NOT IN (select idsetting FROM mrekap_var)
			)as tbl";
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					
					$aksi .= '<button data-toggle="tooltip" title="Pilih" class="btn btn-primary btn-xs pilih"><i class="fa fa-check"></i> Pilih</button>';
					
			        $aksi .= '</div>';

          $row[] = $r->id;
          $row[] = $r->idvariable;
          $row[] = $r->jenis;
          $row[] = $r->nama;
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function Loadvariable()
    {
	 $idrekap=$this->input->post('idrekap');
			$this->select = array();
			$this->from   = "(
				select D.id,D.nama_variable,M.nama as jenis FROM mrekap_var D
				LEFT JOIN mjenis_gaji M ON M.id=D.idjenis
				WHERE idrekap='$idrekap'
			)as tbl";
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama_variable');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					
					$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-xs hapus_pilih"><i class="fa fa-trash"></i> Hapus</button>';
					
			        $aksi .= '</div>';

          $row[] = $r->id;
          $row[] = $r->jenis.' - '.$r->nama_variable;
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function LoadRekening()
    {
	 $idrekap=$this->input->post('idrekap');
	$this->select = array();
	$this->from   = "(
		SELECT H.*,M.bank FROM `mrekap_rekening` H
		LEFT JOIN ref_bank M ON M.id=H.idbank
		WHERE H.idrekap='$idrekap'
	)as tbl";
	$this->join 	= array();
	$this->where  = array();
	$this->order  = array();
	$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					
					$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-success btn-xs edit_rek"><i class="fa fa-pencil"></i></button>';
					$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-xs hapus_rek"><i class="fa fa-trash"></i></button>';
					
			        $aksi .= '</div>';

          $row[] = $r->id;
          $row[] = $r->norek;
          $row[] = $r->atas_nama;
          $row[] = $r->bank;
          $row[] = $aksi;
          $row[] = $r->idbank;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function pilih_variable(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$idsetting=$this->input->post('idsetting');
		$idrekap=$this->input->post('idrekap');
		$q="SELECT S.id as idsetting,S.idjenis,S.idvariable,M.nama as nama_variable,S.idakun 
				FROM `mjenis_gaji_setting` S
				LEFT JOIN mvariable M ON M.id=S.idvariable
				LEFT JOIN mjenis_gaji J ON J.id=S.idjenis
				WHERE S.id='$idsetting'";
		$rows=$this->db->query($q)->result();
		foreach ($rows as $r){
			$data_det=array(
					'idrekap'=>$idrekap,
					'idsetting'=>$r->idsetting,
					'idjenis'=>$r->idjenis,
					'idvariable'=>$r->idvariable,
					'nama_variable'=>$r->nama_variable,
					'idakun'=>$r->idakun,
					// 'noakun'=>'',
				);
				$result=$this->db->insert('mrekap_var',$data_det);
		}
		$this->output->set_output(json_encode($result));
	}
	function simpan_rekening(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$tedit=$this->input->post('tedit');
		$idbank=$this->input->post('idbank');
		$norek=$this->input->post('norek');
		$atas_nama=$this->input->post('atas_nama');
		$idrekap=$this->input->post('idrekap');
		if ($tedit==''){
			$data_det=array(
					'idrekap'=>$idrekap,
					'idbank'=>$idbank,
					'norek'=>$norek,
					'atas_nama'=>$atas_nama,				
				);
			$result=$this->db->insert('mrekap_rekening',$data_det);
		}else{
			$data_det=array(
					'idbank'=>$idbank,
					'norek'=>$norek,
					'atas_nama'=>$atas_nama,				
				);
				$this->db->where('id',$tedit);
			$result=$this->db->update('mrekap_rekening',$data_det);
		}
		$this->output->set_output(json_encode($result));
	}
	function hapus_variable(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$result=$this->db->delete('mrekap_var');
		$this->output->set_output(json_encode($result));
	}
	function hapus_rek(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$result=$this->db->delete('mrekap_rekening');
		$this->output->set_output(json_encode($result));
	}
}
