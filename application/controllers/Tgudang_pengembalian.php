<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO + ACENG DJUHADI
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tgudang_pengembalian extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('tgudang_pengembalian_model','tgudang_pengembalian');
		$this->load->model('Mdistributor_model', 'distributor_model');
	}

	function _list($data) {
		$this->session->set_userdata($data);
		$data['error'] 			= '';
		$data['title'] 			= 'Pengembalian Gudang';
		$data['content'] 		= 'Tgudang_pengembalian/index';
		$data['breadcrum'] 	= array(array("RSKB Halmahera",'#'),
							  array("Pengembalian Gudang",'#'),
							  array("List",'Tgudang_pengembalian'));
		$data['list_distributor'] = $this->distributor_model->load_all();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	public function index() {
        $data = array(
            'iddistributor' => '#',
            'statuspenerimaan' => '#',
            'tanggaldari' => '',
            'tanggalsampai' => '',
            'jenis' => '#',
            'jenis_retur' => '#',
            'no_trx_pengembalian' => '',
        );
        $this->_list($data);
    }

    public function filter() {
        $data = array(
            'iddistributor' => $this->input->post('iddistributor'),
            'statuspenerimaan' => $this->input->post('statuspenerimaan'),
            'tanggaldari' => $this->input->post('tanggaldari'),
            'tanggalsampai' => $this->input->post('tanggalsampai'),
            'jenis' => $this->input->post('jenis'),
            'jenis_retur' => $this->input->post('jenis_retur'),
            'no_trx_pengembalian' => $this->input->post('no_trx_pengembalian'),
        );
        $this->_list($data);
    }

    function ajax_list(){
		$iddistributor = $this->session->userdata('iddistributor');
        $statuspenerimaan = $this->session->userdata('statuspenerimaan');
        $tanggaldari = $this->session->userdata('tanggaldari');
		$tanggalsampai = $this->session->userdata('tanggalsampai');
		
		$from ="(SELECT";
		$from.=" a.*,d.nama";
		$from.=" FROM tgudang_pengembalian a";
		$from.=" JOIN mdistributor d ON a.iddistributor=d.id";
		if ($iddistributor!='' && $iddistributor!='0') {
            $from .= ' AND iddistributor="'.$iddistributor.'"';
        }
        if ($statuspenerimaan!='' && $statuspenerimaan!='0') {
            $from .= ' AND a.status="'.$statuspenerimaan.'"';
        }
        if ('' != $tanggaldari) {
            $from .= ' AND tanggal >= "'.date("Y-m-d",strtotime($tanggaldari)).'"';
        }
        if ('' != $tanggalsampai) {
            $from .= ' AND tanggal <= "'.date("Y-m-d",strtotime($tanggalsampai)).'"';
		}
		$from .=") tbl";
        $this->load->library('datatables');
		$this->datatables->add_column('option', '', 'id');
		$this->datatables->from($from);
        return print_r($this->datatables->generate());
    }
	function get_index(){
		$iddistributor = $this->session->userdata('iddistributor');
        $statuspenerimaan = $this->session->userdata('statuspenerimaan');
        $tanggaldari = $this->session->userdata('tanggaldari');
		$tanggalsampai = $this->session->userdata('tanggalsampai');
		$jenis = $this->session->userdata('jenis');
		$jenis_retur = $this->session->userdata('jenis_retur');
		$no_trx_pengembalian = $this->session->userdata('no_trx_pengembalian');
		
		$from ="(SELECT  
					H.id,H.tanggal,H.nopengembalian,H.iddistributor,M.nama as nama_distributor,H.jenis,H.jenis_retur,
					CASE WHEN H.jenis='1' THEN 'RETUR' ELSE 'PEMUSNAHAN' END as jenis_nama,
					CASE WHEN H.jenis_retur='1' THEN 'GANTI UANG' WHEN H.jenis_retur='2' THEN 'GANTI BARANG SAMA' ELSE 'GANTI BARANG BERBEDA' END as retur_nama,H.`status`,H.alasan,
					H.nama_user_created,H.totalharga,H.totalpengganti
					from tgudang_pengembalian H 
					LEFT JOIN mdistributor M ON M.id=H.iddistributor 
					WHERE H.id !=''  ";
		
		if ($iddistributor!='#') {
            $from .= ' AND H.iddistributor="'.$iddistributor.'"';
        }
        if ($no_trx_pengembalian!='') {
            $from .= " AND H.nopengembalian LIKE '%".$no_trx_pengembalian."%'";
        }
        if ($jenis!='#') {
            $from .= ' AND H.jenis="'.$jenis.'"';
        }
        if ($jenis_retur!='#') {
            $from .= ' AND H.jenis_retur="'.$jenis_retur.'"';
        }
        if ($statuspenerimaan!='#') {
            $from .= ' AND H.status="'.$statuspenerimaan.'"';
        }
        if ('' != $tanggaldari) {
            $from .= ' AND H.tanggal >= "'.date("Y-m-d",strtotime($tanggaldari)).'"';
        }
        if ('' != $tanggalsampai) {
            $from .= ' AND H.tanggal <= "'.date("Y-m-d",strtotime($tanggalsampai)).'"';
		}
		$from .=" ORDER BY H.id DESC) tbl";
		$this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		
        $this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $status     = '';
            $url        = site_url('tgudang_pengembalian/');

			

            $row[] = $r->id;
            $row[] = $no;
            $row[] = $r->nopengembalian;
            $row[] = $r->tanggal;
            $row[] = $r->nama_distributor;
            $row[] = $r->jenis_nama;
			if ($r->jenis=='1'){
				$row[] = $r->retur_nama;
			}else{				
				$row[] = '';
			}
            $row[] = $r->alasan;
            $row[] = number_format($r->totalharga,2);
			$status=status_retur_gudang($r->status);
            $row[] = $r->nama_user_created;
            $row[] = $status;
			
			$aksi .= '<a class="view btn btn-xs btn-info" href="'.$url.'detail/'.$r->id.'" type="button"  title="Detail"><i class="fa fa-eye"></i></a>';
			if ($r->status=='1'){
				if (UserAccesForm($user_acces_form,array('1151'))){
				$aksi .= '<a href="'.$url.'edit/'.$r->id.'" type="button" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1152'))){
				$aksi .= '<a href="'.$url.'verifikasi_detail/'.$r->id.'" class="btn btn-xs btn-success" title="Verifikasi Proses / Pemesanan"><i class="fa fa-check"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1153'))){
				$aksi .= '<a href="'.$url.'cancel/'.$r->id.'" onclick="return confirm(\'Anda Yakin AKan Membatalkan Retur?\')" class="btn btn-xs btn-danger" title="Batalkan"><i class="fa fa-times"></i></a>';
				}
			}
			if ($r->status!='0'){
				if (UserAccesForm($user_acces_form,array('1154'))){
				$aksi .= '<a href="'.$url.'print_pengembalin/'.$r->id.'" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-print"></i></a>';
				}
			}
           
			$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }

    // function view() {
    	// $id = $this->uri->segment(3);
    	// if($id) {
    		// $data = $this->tgudang_pengembalian->viewHead($id);
			// $data['error'] 			= '';
			// $data['title'] 			= 'Detail Pengembalian Gudang';
			// $data['content'] 		= 'Tgudang_pengembalian/view';
			// $data['breadcrum'] 	= array(array("RSKB Halmahera",'#'),
								  // array("Pengembalian Gudang",'#'),
								  // array("View",'Tgudang_pengembalian'));
			// $data = array_merge($data, backend_info());
			// $this->parser->parse('module_template', $data);
    	// } else {
    		// show_404();
    	// }
    // }

	function create() {
		$data = array(
			'id' 					=> '',
			'nopengembalian' 		=> '',
			'idpemesanan' 			=> '',
			'totalbarang' 			=> '',
			'totalharga' 			=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Pengembalian Gudang';
		$data['content'] 		= 'Tgudang_pengembalian/manage';
		$data['breadcrum']		= array(array("RSKB Halmahera",'#'),
							  		array("Pengembalian Gudang",'#'),
							  		array("Create",''));
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);		
	}

	function save(){
        // print_r($this->input->post());exit();
		if($status = $this->tgudang_pengembalian->save()){
            // print_r($status);exit();    
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
            
            redirect('tgudang_pengembalian','location');
		}
	}	
	function save_edit(){
        // print_r($this->input->post());exit();
		if($status = $this->tgudang_pengembalian->save_edit()){
            // print_r($status);exit();    
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
            
            redirect('tgudang_pengembalian','location');
		}
	}	
	function verifikasi($id){
		
		if($this->tgudang_pengembalian->verifikasi($id)){
            // print_r($status);exit();    
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');            
            redirect('tgudang_pengembalian','location');
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data Gagal diverifikasi.');            
            redirect('tgudang_pengembalian','location');
		}
	}	
	public function edit($id) {
        // print_r(get_detail_pengganti('4'));exit();
        $data   = $this->tgudang_pengembalian->head($id);
        $data['error']          = '';
        $data['detail']    = $this->tgudang_pengembalian->detail($id);
		// print_r($data['detail']);exit();
        $data['title']          = 'Edit Pengembalian';
        $data['content']        = 'Tgudang_pengembalian/edit';
        $data['breadcrum']      = array(array("RSKB Halmahera",'#'),
                                    array("Pengembalian Gudang",'#'),
                                    array("Edit Pengembalian",''));
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);        
    }
	public function verifikasi_detail($id) {
        $data   = $this->tgudang_pengembalian->head($id);
        $data['error']          = '';
        $data['detail']    = $this->tgudang_pengembalian->detail($id);
        $data['title']          = 'Verifikasi Pengembalian';
        $data['content']        = 'Tgudang_pengembalian/verifikasi';
        $data['breadcrum']      = array(array("RSKB Halmahera",'#'),
                                    array("Pengembalian Gudang",'#'),
                                    array("Verifikasi Pengembalian",''));
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);        
    }
	public function detail($id) {
        $data   = $this->tgudang_pengembalian->head($id);
        $data['error']          = '';
        $data['detail']    = $this->tgudang_pengembalian->detail($id);
        $data['title']          = 'View Pengembalian';
        $data['content']        = 'Tgudang_pengembalian/view';
        $data['breadcrum']      = array(array("RSKB Halmahera",'#'),
                                    array("Pengembalian Gudang",'#'),
                                    array("View Pengembalian",''));
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);        
    }

	// additional
	function get_nobatch() {
		$idtipe 	= $this->input->post('idtipe');
		$idbarang 	= $this->input->post('idbarang');

		if($idbarang && $idtipe) {
			$nobatch = $this->tgudang_pengembalian->get_nobatch($idtipe,$idbarang);
			$this->output->set_output(json_encode($nobatch));
		}
	}

	function get_idpemesanan($nobatch) {
		if($nobatch) {
			$idpemesanan = $this->tgudang_pengembalian->get_idpemesanan($nobatch);
			$this->output->set_output(json_encode($idpemesanan));
		}
	}

	function get_kuantitas_barang() {
		$nobatch 		= $this->input->post('nobatch');
		$idtipe 		= $this->input->post('idtipe');
		$idbarang 		= $this->input->post('idbarang');

		if($idbarang && $idtipe && $nobatch) {
			$r = $this->tgudang_pengembalian->get_kuantitas_barang($nobatch,$idtipe,$idbarang);
			echo $r->kuantitas;
		}		
	}

	function getIdDistributor($id) {
		$iddistributor = $this->tgudang_pengembalian->getIdDistributor($id);
		echo $iddistributor;
	}

	function acc($id) {
        if($id) $this->db->update('tgudang_pengembalian', array('status' => 2), array('id' => $id) );
    }

    function cancel($id) {
		
        if ($this->db->update('tgudang_pengembalian', array('status' => 0,'id_user_hapus'=>$this->session->userdata("user_id"),'nama_user_hapus'=>$this->session->userdata("user_name")), array('id' => $id))) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil Dibatalkan.');
            redirect('tgudang_pengembalian/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_pengembalian/index', 'refresh');
        }
		
		
    }
    
    public function getNoBatchProduk()
    {
        $q = $this->input->get('search');
        $query =   $this->tgudang_pengembalian->getNoBatch($q);
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($query));
        
    }

    public function getProductList()
    {
        $q = $this->input->get('search');
        $query =   $this->tgudang_pengembalian->getProductList($q);
        $this->output->set_output(json_encode($query));
        
    }
    
    public function getPenerimaan()
    {
        
        $query =   $this->tgudang_pengembalian->getPenerimaan();
        $this->output->set_output(json_encode($query));
        
    }
    public function get_data_barang($id)
    {
        // $q = $this->input->post('search');
        $query =   $this->tgudang_pengembalian->getPenerimaan_detail($id);
        $this->output->set_output(json_encode($query));
        
    }
    public function get_mbarang($id,$idtipe)
    {
        // $q = $this->input->post('search');
        $query =   $this->tgudang_pengembalian->get_mbarang($id,$idtipe);
        $this->output->set_output(json_encode($query));
        
    }
    
    public function getPenerimaanDetail()
    {
        $id         = $this->input->post('id');
        // $nobatch    = $this->input->post('nobatch');
        $query =   $this->tgudang_pengembalian->getPenerimaanDetail($id);
        $this->output->set_output(json_encode($query));
        
    }
    public function Get_barang_penerimaan_detail($id)
    {
        
        $query =   $this->tgudang_pengembalian->Get_barang_penerimaan_detail($id);
        $this->output->set_output(json_encode($query));
        
    }
    
    public function print_pengembalin() {
    	$id = $this->uri->segment(3);
    	if($id) {
			$data 					= $this->tgudang_pengembalian->viewHead($id);
			$data['detail'] 		= $this->tgudang_pengembalian->viewDetail($data['id']);
			$data['title'] 			= 'Print Pengembalian';
			$data = array_merge($data, backend_info());
			$this->parser->parse('Tgudang_pengembalian/print_pengembalian', $data);
    	}
    }

}

/* End of file Tgudang_pengembalian.php */
/* Location: ./application/controllers/Tgudang_pengembalian.php */