<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Minterval extends CI_Controller {

	/**
	 * Interval controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Minterval_model');
  }

	function index(){
		
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Interval';
		$data['content'] 		= 'Minterval/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Interval",'#'),
									    			array("List",'minterval')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'st_default' 					=> '0',
			'label_interval' 					=> '',
			'jumlah_setiap_pemberian' 					=> '0',
			'nama' 					=> '',
			'jumlah_pemberian' 				=> '0',
			'interval_waktu' 			=> '0',
			'status' 				=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Interval';
		$data['content'] 		= 'Minterval/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Interval",'#'),
									    			array("Tambah",'minterval')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		
		if($id != ''){
			$row = $this->Minterval_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'label_interval' 					=> $row->label_interval,
					'st_default' 					=> $row->st_default,
					'jumlah_setiap_pemberian' 					=> $row->jumlah_setiap_pemberian,
					'nama' 					=> $row->nama,
					'jumlah_pemberian' 				=> $row->jumlah_pemberian,
					'interval_waktu' 			=> $row->interval_waktu,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Interval';
				$data['content']	 	= 'Minterval/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Interval",'#'),
											    			array("Ubah",'minterval')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('minterval','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('minterval');
		}
	}

	function delete($id){
		
		$this->Minterval_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('minterval','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('jumlah_pemberian', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('interval_waktu', 'Telepon', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Minterval_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('minterval','location');
				}
			} else {
				if($this->Minterval_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('minterval','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 		= validation_errors();
		$data['content'] 	= 'Minterval/manage';

		if($id==''){
			$data['title'] = 'Tambah Interval';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Interval",'#'),
															array("Tambah",'minterval')
													);
		}else{
			$data['title'] = 'Ubah Interval';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Interval",'#'),
															array("Ubah",'minterval')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	  
$data_user=get_acces();
$user_acces_form=$data_user['user_acces_form'];

			$this->select = array();
			$this->from   = 'minterval';
			$this->join 	= array();
			$this->where  = array(
				'status' => '1'
			);
			$this->order  = array(
				'nama' => 'ASC'
			);
			$this->group  = array();

      $this->column_search   = array('nama','jumlah_pemberian','interval_waktu');
      $this->column_order    = array('nama','jumlah_pemberian','interval_waktu');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

          $row[] = $no;
          $row[] = $r->nama.($r->st_default=='1'?'&nbsp;&nbsp;&nbsp;'.text_success('Default'):'');
          $row[] = $r->label_interval;
          $row[] = ($r->status=='1'?text_primary('AKTIF'):text_danger('TIDAK AKTIF'));
          $aksi = '<div class="btn-group">';
          
		  if (UserAccesForm($user_acces_form,array('1825'))){
          	$aksi .= '<a href="'.site_url().'minterval/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
          }
          
		  if (UserAccesForm($user_acces_form,array('1826'))){
          	$aksi .= '<a href="#" data-urlindex="'.site_url().'minterval" data-urlremove="'.site_url().'minterval/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
          }
          $aksi .= '</div>';
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function ajaxSave(){
		if ($this->Minterval_model->saveData()) {
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			$data['response']="data telah di simpan";
			$data['code']=200;
			$data['id']=$this->db->insert_id();
			$data['nama']=$this->input->post("nama");
			echo json_encode($data);
		}
	}
}
