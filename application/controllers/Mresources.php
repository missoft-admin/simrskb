<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mresources extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    PermissionUserLoggedIn($this->session);
    $this->load->model('Mresources_model', 'model');
  }

  function index(){
    
    $data = array(
            'id' => '#',
            'id_menu_sub' => '#',
            'action' => '',
            'status' => '#',            
        );
	$this->session->set_userdata($data);
    $data['list_menu'] 			= $this->model->get_utama();
    $data['list_sub'] 			= array();
    $data['error'] 			= '';
    $data['title'] 			= 'Resource';
    $data['content'] 		= 'Mresources/index';
    $data['breadcrum'] 		= array(array("RSKB Halmahera",'#'), array("Resource",'#'));

    $data['list_parent'] = $this->model->getAllParent();

    $data = array_merge($data, backend_info());
    $this->parser->parse('module_template', $data);
  }
  public function filter() {
		
        $data = array(
            'id' => $this->input->post('id'),
            'id_menu_sub' => $this->input->post('id_menu_sub'),
            'action' => $this->input->post('action'),
            'status' => $this->input->post('status'),
        );
		$this->session->set_userdata($data);
       $data['list_menu'] 			= $this->model->get_utama();
		$data['list_sub'] 			= $this->model->get_sub($this->input->post('id'));
		$data['error'] 			= '';
		$data['title'] 			= 'Resource';
		$data['content'] 		= 'Mresources/index';
		$data['breadcrum'] 		= array(array("RSKB Halmahera",'#'), array("Resource",'#'));

		$data['list_parent'] = $this->model->getAllParent();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
    }
  function add(){
    
    $data = array();
    $data['error'] 			= '';
    $data['title'] 			= 'Resource';
    $data['content'] 		= 'Mresources/add';
    $data['breadcrum'] 		= array(array("RSKB Halmahera",'#'), array("Resource",'#'));

    $data['list_parent'] = $this->model->getAllParent();

    $data = array_merge($data, backend_info());
    $this->parser->parse('module_template', $data);
  }
function get_index(){
	$where='';
	$id = $this->session->userdata('id');
	$id_menu_sub = $this->session->userdata('id_menu_sub');
	$action = $this->session->userdata('action');
	$status = $this->session->userdata('status');
	if ($id !='#' && $id !=''){
		$where .=" AND L1.id='$id' ";
	}
	if ($id_menu_sub !='#' && $id_menu_sub !=''){
		$where .=" AND L2.id2='$id_menu_sub' ";
	}
	if ($action !=''){
		$where .=" AND L3.action LIKE '%".$action."%' ";
	}
	if ($status !='#' && $status !=''){
		if ($status=='1'){
			$where .=" AND L3.id3 is null";			
		}else{
			$where .=" AND L3.id3 is not null";
			
		}
	}
	$from = "(SELECT *from mmenu L1 
			LEFT JOIN mmenu_sub L2 ON L1.id=L2.id_menu
			LEFT JOIN mmenu_action L3 ON L2.id2=L3.id_menu_sub  AND L3.`status` != '0' 
			WHERE L1.id !='' ".$where."
			ORDER BY L1.`index`,L2.index2,L3.index3 ";	
			
        $from .= ') tbl';
		// print_r($from);exit();
		$this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		
        $this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $url        = site_url('mresources/add');

			

            $row[] = $r->id;
            $row[] = $no;
            $row[] = $r->menu;
            $row[] = $r->menu2;
            $row[] = $r->action;
			if ($r->id3){
            $row[] = $r->id3;
            }else{
				
            $row[] = '<span class="label label-danger">BELUM ADA AKSI</span>';
			}
			// $status=status_pesan_gudang($r->status);
			
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
}
  function get_utama(){
		
		$arr = $this->model->get_utama();
		$this->output->set_output(json_encode($arr));
  }
	function get_sub($id){
		$arr = $this->model->get_sub($id);
		$this->output->set_output(json_encode($arr));

	}
	function get_action($id){
		$arr = $this->model->get_action($id);
		$this->output->set_output(json_encode($arr));

	}

  function get_child_level($headerpath) {
    $arr = $this->model->getPathLevel($headerpath);
    $this->output->set_output(json_encode($arr));
  }

  // @
  function save() {
    $data = [
      'nama'       => $this->input->post('nama-resource'),
      'tipe'       => $this->input->post('tipe-resource'),
      'idkelompok' => $this->input->post('jenis-resource'),
      'headerpath' => $this->input->post('parent-id'),
      'path'       => $this->input->post('path'),
      'level'      => $this->input->post('level'),
      'status'     => 1,
    ];

    $result = $this->model->saveData($data);

    if ($result) {
      $this->session->set_flashdata('confirm', true);
      $this->session->set_flashdata('message_flash', 'data telah disimpan.');
      redirect('mresources/index', 'location');
    }
  }
function simpan_menu() {
    $data = [
      'index'       => $this->input->post('index'),
      'menu' => $this->input->post('menu'),
      
    ];

    $result = $this->model->simpan_menu($data);
	$this->output->set_output(json_encode($result));
   // return $result;
  }
function simpan_sub() {
    $data = [
      'id_menu'       => $this->input->post('id_menu'),
      'index2'       => $this->input->post('index2'),
      'menu2' => $this->input->post('menu2'),
      
    ];

    $result = $this->model->simpan_sub($data);
	$this->output->set_output(json_encode($result));
   // return $result;
  }
  function simpan_action() {
    $data = [
      'id_menu_sub'       => $this->input->post('id_menu_sub'),
      'index3'       => $this->input->post('index3'),
      'action' => $this->input->post('action'),
      'status' => '1',
      
    ];

    $result = $this->model->simpan_action($data);
	$this->output->set_output(json_encode($result));
   // return $result;
  }
function edit_menu() {
    $data = [
      'index'       => $this->input->post('index'),
      'menu' => $this->input->post('menu'),
      
    ];

    $result = $this->model->edit_menu($data,$this->input->post('id'));
	$this->output->set_output(json_encode($result));
   // return $result;
  }
function edit_sub() {
    $data = [
      'index2'       => $this->input->post('index2'),
      'menu2' => $this->input->post('menu2'),
      
    ];

    $result = $this->model->edit_sub($data,$this->input->post('id2'));
	$this->output->set_output(json_encode($result));
   // return $result;
  }
function edit_action() {
    $data = [
      'index3'       => $this->input->post('index3'),
      'action' => $this->input->post('action'),
      
    ];
    $result = $this->model->edit_action($data,$this->input->post('id3'));
	$this->output->set_output(json_encode($result));
   // return $result;
  }
function delete_action() {
    $data = [
      'status'       => '0',
      
    ];
    $result = $this->model->delete_action($data,$this->input->post('id3'));
	$this->output->set_output(json_encode($result));
   // return $result;
  }

  function tipeResource($tipe) {
    switch ($tipe) {
      case 0:
        $result = "Action";
        break;
        case 1:
          $result = "Controller";
          break;
          case 2:
            $result = "Module";
            break;
            case 3:
              $result = "Other";
              break;
    }
    return $result;
  }

  public function showResource() {
    $result = $this->model->getAllResource();

    $data = array();

    $no = 1;
    foreach ($result as $row) {
      $rows = array();

      $button = '';
      if(button_roles('mresources/deleteResource')) {
        $button += '<button class="btn btn-xs btn-danger delete-resource" data-value="'.$row->id.'"><i class="fa fa-trash"></i>&nbsp; Hapus</button>';
      }

      $rows[] = $no++;
      $rows[] = TreeView($row->level, $row->nama);
      $rows[] = '<label class="label label-primary text-uppercase">'.$this->tipeResource($row->tipe).'</label>';
      $rows[] = $button;

      $data[] = $rows;
    }

    $output = array(
      "recordsTotal"    => count($result),
      "recordsFiltered" => count($result),
      "data"            => $data
    );

    echo json_encode($output);
  }

  function deleteResource() {
    
    $id = $this->input->post('id');

    $result = $this->model->deleteResource($id);
    if ($result) {
      return true;
    }
  }

}
