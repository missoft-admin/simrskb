<?php

defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
require 'vendor/autoload.php';
// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

// reference the Dompdf namespace
/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
| EMAIL			: POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tgudang_penerimaan extends CI_Controller {
    public function __construct() {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('tgudang_penerimaan_model', 'model');
        $this->load->model('Mdistributor_model', 'distributor_model');
        $this->load->model('Mrka_bendahara_model');
    }

    public function _list($data) {
        $this->session->set_userdata($data);
        $data['error'] = '';
        $data['title'] = 'Penerimaan Gudang';
        $data['content'] = 'Tgudang_penerimaan/index';
        $data['breadcrum'] = array(array('RSKB Halmahera', '#'),
            array('Penerimaan Gudang', '#'),
            array('List', 'tgudang_penerimaan'), );
        // $data['list_distributor'] = $this->distributor_model->load_all();
        $data['list_distributor'] = $this->model->list_distributor();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function index() {
		// tanggaldari
		$hari_ini = date("Y-m-d");
		$date=date_create($hari_ini);
		date_add($date,date_interval_create_from_date_string("-30 days"));
			// echo 
		$tgl_pertama = date_format($date,"d-m-Y");
		$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
		
        $data = array(
            'iddistributor' => '#',
            'statuspenerimaan' => '#',
            'tanggaldari' => $tgl_pertama,
            'tanggalsampai' => date('d-m-Y'),
            'tipe_bayar' => '#',
            'no_trx' => '',
            'no_po' => '',
            'no_faktur' => '',
        );
        $this->_list($data);
    }

    public function filter() {
		
        $data = array(
            'iddistributor' => $this->input->post('iddistributor'),
            'statuspenerimaan' => $this->input->post('statuspenerimaan'),
            'tanggaldari' => $this->input->post('tanggaldari'),
            'tanggalsampai' => $this->input->post('tanggalsampai'),
            'tipe_bayar' => $this->input->post('tipe_bayar'),
            'no_trx' => $this->input->post('no_trx'),
            'no_po' => $this->input->post('no_po'),
            'no_faktur' => $this->input->post('no_faktur'),
        );
        $this->_list($data);
    }

    // public function create() {
        // $data = array(
            // 'id' => '',
            // 'nopenerimaan' => '',
            // 'idpemesanan' => '',
            // 'totalbarang' => '',
            // 'tanggalpenerimaan' => date('d-m-Y'),
            // 'tanggaljatuhtempo' => '',
            // 'totalharga' => '',
        // );
		// // print_r($data);exit();
        // $data['error'] = '';
        // $data['title'] = 'Tambah Penerimaan Gudang';
        // $data['content'] = 'Tgudang_penerimaan/add';
        // $data['breadcrum'] = array(array('RSKB Halmahera', '#'),
            // array('Penerimaan Gudang', '#'),
            // array('Create', '#'), );
        // $data = array_merge($data, backend_info());
        // $this->parser->parse('module_template', $data);
    // }
	public function create2() {
        $data = array(
            'id' => '',
            'nopenerimaan' => '',
            'idpemesanan' => '',
            'totalbarang' => '',
            'tanggalpenerimaan' => date('d-m-Y'),
            'tanggaljatuhtempo' => '',
            'totalharga' => '',
        );
		// print_r($data);exit();
        $data['error'] = '';
        $data['title'] = 'Tambah Penerimaan Gudang';
        $data['content'] = 'Tgudang_penerimaan/add2';
        $data['breadcrum'] = array(array('RSKB Halmahera', '#'),
            array('Penerimaan Gudang', '#'),
            array('Create', '#'), );
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	public function manage_print_label($id) {
        $data = array(
            'id' => $id
        );
		$data['start_awal'] 	= 1;
		$data['list_data'] 				= $this->model->list_data($id);
        $data['error'] = '';
        $data['title'] = 'Cetak Label Barang';
        $data['content'] = 'Tgudang_penerimaan/manage_print';
        $data['breadcrum'] = array(array('RSKB Halmahera', '#'),
            array('Penerimaan Gudang', '#'),
            array('Cetak Label', '#'), );
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function save() {
        
		if ($this->model->save()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
            redirect('tgudang_penerimaan/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_penerimaan/index', 'refresh');
        }
    }
	public function save2() {
        
		if ($this->model->save2()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
            redirect('tgudang_penerimaan/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_penerimaan/index', 'refresh');
        }
    }
	public function save_edit() {
        
		if ($this->model->save_edit()) {
			
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
			if ($this->input->post('btn_simpan')>1){
				redirect('tkontrabon_verifikasi/index', 'refresh');
				
			}else{
				redirect('tgudang_penerimaan/index', 'refresh');
			}
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_penerimaan/index', 'refresh');
        }
    }
	public function save_batal() {
        
		if ($this->model->save_batal()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil dibatalkan.');
            redirect('tgudang_penerimaan/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tgudang_penerimaan/index', 'refresh');
        }
    }

    public function ajax_list() {
        $iddistributor = $this->session->userdata('iddistributor');
        $statuspenerimaan = $this->session->userdata('statuspenerimaan');
        $tanggaldari = $this->session->userdata('tanggaldari');
        $tanggalsampai = $this->session->userdata('tanggalsampai');
        $userid = $this->session->userdata('user_id');
        
        $from = "(SELECT
	H.id,H.tipepenerimaan,
	H.tanggalpenerimaan,
	H.tgl_terima,P.nopemesanan,DS.nama as nama_distributor,H.iddistributor,
	H.nopenerimaan,H.tipe_bayar,(CASE WHEN H.tipe_bayar='1' THEN 'TUNAI' ELSE 'KREDIT' END) as cara_bayar,
	(CASE WHEN H.tipe_bayar='1' THEN '' ELSE H.tanggaljatuhtempo END) as tgl_jatuh_tempo,SUM(D.totalharga) as tagihan
FROM
	tgudang_penerimaan H
	LEFT JOIN tgudang_penerimaan_detail D ON H.id=D.idpenerimaan
	LEFT JOIN tgudang_pemesanan P ON H.idpemesanan = P.id
	LEFT JOIN mdistributor DS ON DS.id=H.iddistributor
	WHERE H.tipepenerimaan IN (SELECT gudangtipe FROM musers_tipegudang WHERE iduser='$userid')  AND D.`status`='1'
	GROUP BY H.id
	ORDER BY H.id DESC ";
        // $from .= ' v_index_tgudang_penerimaan.*';
        // $from .= ' FROM v_index_tgudang_penerimaan';
        // $from .= ' WHERE tipepemesanan IN (SELECT gudangtipe FROM musers_tipegudang WHERE iduser = '.$userid.')';
        // if ($iddistributor!='' && $iddistributor!='0') {
            // $from .= ' AND iddistributor="'.$iddistributor.'"';
        // }
        // if ($statuspenerimaan!='' && $statuspenerimaan!='0') {
            // $from .= ' AND status_penerimaan = "'.$statuspenerimaan.'"';
        // }
        // if ('' != $tanggaldari) {
            // $from .= ' AND tanggalpenerimaan >= "'.date("Y-m-d",strtotime($tanggaldari)).'"';
        // }
        // if ('' != $tanggalsampai) {
            // $from .= ' AND tanggalpenerimaan <= "'.date("Y-m-d",strtotime($tanggalsampai)).'"';
        // }
        $from .= ') tbl';
        $this->load->library('datatables');
        $this->datatables->select("
            nopenerimaan,
            tanggalpenerimaan,
            nofakturexternal,
            distributor,
            totalpsn,
            totalpernahtrm,
            totaltrm,
            totalsisa,
            totalharga,
            id,
            id
        ");
        $this->datatables->from($from);
        $this->datatables->add_column('option', '', 'id');

        return print_r($this->datatables->generate());
    }
	public function get_index() {
		
        $iddistributor = $this->session->userdata('iddistributor');
        $statuspenerimaan = $this->session->userdata('statuspenerimaan');
        $tanggaldari = $this->session->userdata('tanggaldari');
        $tanggalsampai = $this->session->userdata('tanggalsampai');
        $tipe_bayar = $this->session->userdata('tipe_bayar');
        $no_trx = $this->session->userdata('no_trx');
        $no_po = $this->session->userdata('no_po');
        $no_faktur = $this->session->userdata('no_faktur');
		
		$where='';
		if ($iddistributor !='#'){
			$where .=" AND H.iddistributor='$iddistributor'";
		}
        if ($tipe_bayar !='#'){
			$where .=" AND H.tipe_bayar='$tipe_bayar'";
		}
        if ($no_trx !=''){
			$where .=" AND H.nopenerimaan LIKE '%".$no_trx."%'";
		}
		if ($no_po !=''){
			$where .=" AND P.nopemesanan LIKE '%".$no_po."%'";
		}
        if ($no_faktur !=''){
			$where .=" AND H.nofakturexternal LIKE '%".$no_faktur."%'";
		}
		if ('' != $tanggaldari) {
            $where .= " AND DATE(H.tanggalpenerimaan) >='".YMDFormat($tanggaldari)."'";
        }
        if ('' != $tanggalsampai) {
            $where .= " AND DATE(H.tanggalpenerimaan) <='".YMDFormat($tanggalsampai)."'";
        }
        $userid = $this->session->userdata('user_id');
        
        $from = "(SELECT
				H.id,H.tipepenerimaan,H.nofakturexternal,H.stverif_kbo,H.status,H.st_trx,
				DATE_FORMAT(H.tanggalpenerimaan,'%d-%m-%Y %H:%i:%s') as tanggalpenerimaan,
				DATE_FORMAT(H.tgl_terima,'%d-%m-%Y') as tgl_terima,P.nopemesanan,DS.nama as nama_distributor,H.iddistributor,
				H.nopenerimaan,H.tipe_bayar,(CASE WHEN H.tipe_bayar='1' THEN 'TUNAI' ELSE 'KREDIT' END) as cara_bayar,
				(CASE WHEN H.tipe_bayar='1' THEN '' ELSE DATE_FORMAT(H.tanggaljatuhtempo,'%d-%m-%Y') END) as tgl_jatuh_tempo,SUM(D.totalharga) as tagihan,H.total_pembayaran
				,H.st_retur,GK.nopengembalian,H.idpemesanan
			FROM
				tgudang_penerimaan H
				LEFT JOIN tgudang_penerimaan_detail D ON H.id=D.idpenerimaan AND D.status='1'
				LEFT JOIN tgudang_pemesanan P ON H.idpemesanan = P.id
				LEFT JOIN mdistributor DS ON DS.id=H.iddistributor
				LEFT JOIN tgudang_pengembalian GK ON GK.id=H.retur_id
				WHERE H.tipepenerimaan IN (SELECT gudangtipe FROM musers_tipegudang WHERE iduser='$userid')  ".$where."
				GROUP BY H.id
				ORDER BY H.id DESC ";
				  
        $from .= ') tbl';
		$this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array('id' => 'desc');
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		
        $this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        foreach ($list as $r) {
            $no++;
            $row = array();
			$status_retur='';
			if ($r->st_retur=='1'){
				$status_retur='<br>'.text_danger($r->nopengembalian);
			}
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $url        = site_url('tgudang_penerimaan/');
            $url_pemesanan        = site_url('tgudang_pemesanan/');
			$st_bayar='';
			if ($r->tipe_bayar=='1'){
				if ($r->total_pembayaran==$r->tagihan){
					$st_bayar='<span class="label label-success"><i class="fa fa-check"></i> TRANSAKSI KAS</span>';
					
				}else{
					$st_bayar='<span class="label label-danger"><i class="fa fa-close"></i> TRANSAKSI KAS</span>';
				}
			}
			

            $row[] = $r->id;
            $row[] = $no;
            $row[] = $r->nopenerimaan.$status_retur;
            $row[] = $r->tanggalpenerimaan;
            $row[] = $r->nopemesanan;
            $row[] = $r->tgl_terima;
            $row[] = $r->nofakturexternal;
            $row[] = $r->nama_distributor;
			if ($r->tipe_bayar=='1'){
				$row[] ='<span class="label label-success">'. $r->cara_bayar.'</span>';
			}else{
				$row[] ='<span class="label label-danger">'. $r->cara_bayar.'</span>';				
			}
            $row[] = $r->tgl_jatuh_tempo.$st_bayar;
            $row[] = number_format($r->tagihan,2);
			
			// $status=status_pesan_gudang($r->status);
			$aksi .= '<a class="view btn btn-xs btn-info" href="'.$url.'detail/'.$r->id.'" type="button"  title="Detail"><i class="fa fa-eye"></i></a>';
			if (UserAccesForm($user_acces_form,array('1142'))){
				if ($r->stverif_kbo =='0'){
					if ( $r->status=='1'){
					$aksi .= '<a href="'.$url.'edit/'.$r->id.'" type="button" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>';
					if ($r->tipe_bayar=='1'){
					$aksi .= '<a href="'.$url.'bayar/'.$r->id.'" target="_blank"  data-toggle="tooltip" title="Pembayaran" class="btn btn-primary btn-xs "><i class="fa fa-credit-card"></i></a>';
					}
					}
				}
			}
			if ( $r->status=='1'){
			if (UserAccesForm($user_acces_form,array('1143'))){
			$aksi .= '<div class="btn-group" role="group">
								<button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<span class="fa fa-print"></span>
								</button>
								<ul class="dropdown-menu dropdown-menu-right">
									<li class="dropdown-header">Cetak</li>
									<li>
										<a tabindex="-1" href="'.$url.'print_data/'.$r->id.'/0"  target="_blank">Bukti Penerimaan</a>
										<a tabindex="-1" href="'.$url.'print_data_bukti_pemesanan/'.$r->idpemesanan.'/'.$r->id.'"  target="_blank">Bukti Pemesanan</a>
										<a tabindex="-1" href="'.$url.'manage_print_label/'.$r->id.'"  target="_blank">Cetak Label</a>
									</li>
								</ul>
							</div>';
		
			}
			}
			if (UserAccesForm($user_acces_form,array('1373'))){
				if ($r->stverif_kbo =='0'){
					if ( $r->status=='1'){
					$aksi .= '<a href="'.$url.'batalkan/'.$r->id.'" type="button" title="Batalkan" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i></a>';
					}
				}
			}
			$status_batal='';
			if ( $r->status=='0'){
				$status_batal='<span class="label label-danger">DIBATALKAN</span>';
			}
			$aksi.='</div>';
            $row[] = $aksi .' '. $status_batal;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	public function print_data($id)
    {
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();

        $data = $this->model->viewHead($id);
		// $data['ttd']=base_url().'qrcode/qr_code_ttd_user_name/'.$data['userpenerimaan'];
		// print_r($data);exit();
        // $data['tipe'] = $tipe;
        $data['list_detail'] = $this->model->detail_penerimaan($id);
		$data=array_merge($data,backend_info());
        $html = $this->load->view('Tgudang_penerimaan/cetak_nota', $data, true);
        $html = $this->parser->parse_string($html,$data);
		// print_r($html);exit();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Rincian Global.pdf', array("Attachment"=>0));
    }
	public function print_data_bukti_pemesanan($id,$idterima)
    {
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();

        $data = $this->db->where('id',$id)->get('tgudang_pemesanan')->row_array();
		$q="
			SELECT H.nopenerimaan,H.tanggalpenerimaan,H.userpenerimaan,H.nofakturexternal,mppa.id as mppa_id_terima FROM `tgudang_penerimaan` H
LEFT JOIN musers MU ON MU.`name`=H.userpenerimaan
LEFT JOIN mppa ON mppa.user_id=MU.id
 WHERE H.id='$idterima'
		";
		$data_terima=$this->db->query($q)->row_array();
		// print_r($data);exit();
		$q="
			SELECT mppa.id as mppa_id FROM `tgudang_pemesanan` H
			LEFT JOIN musers M ON M.`name`=H.userpemesanan
			LEFT JOIN mppa ON mppa.user_id=M.id 
			WHERE H.id='".$data['id']."'
		";
		$data['mppa_id']=$this->db->query($q)->row('mppa_id');
        $data['namadistributor'] = get_by_field('id',$data['iddistributor'],'mdistributor')->nama;
        $data['error']          = '';
        $data['list_detail']    = $this->model->list_pesanan_edit($id,$idterima);
		$data=array_merge($data,backend_info(),$data_terima);
        $html = $this->load->view('Tgudang_penerimaan/cetak_nota_bukti', $data, true);
        $html = $this->parser->parse_string($html,$data);
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Rincian Global.pdf', array("Attachment"=>0));
    }
    public function detailPenerimaan() {
        $idpenerimaan = $this->input->post('idpenerimaan');
        if ($idpenerimaan) {
            $this->load->library('datatables');
            $this->datatables->select('trmdet.*');
            $this->datatables->where('trmdet.idpenerimaan', $idpenerimaan);
            $this->datatables->from('tgudang_penerimaan_detail trmdet');

            return print_r($this->datatables->generate());
        }
    }

    public function ajaxDetail() {
        $idtipe = $this->input->post('idtipe');
        $idbarang = $this->input->post('idbarang');
        $idpenerimaan = $this->input->post('idpenerimaan');
        if ($idtipe && $idbarang && $idpenerimaan) {
            $r = $this->model->ajaxDetail($idpenerimaan, $idtipe, $idbarang);
            $this->output->set_output(json_encode($r));
        }
    }

    public function get_nopemesanan() {
        $data = $this->model->get_nopemesanan();
        $this->output->set_output(json_encode($data));
    }
	public function get_nopemesanan2($id) {
        $data = $this->model->get_nopemesanan2($id);
        $this->output->set_output(json_encode($data));
    }

    public function get_nopemesanan_detail($id) {
        if ($id) {
            $result = $this->model->get_pemesanan_detail($id);
            $data = array();
            foreach ($result as $r) {
                $row = array();
                $row['tipebarang'] = GetTipeKategoriBarang($r->idtipe);
                $row['namabarang'] = get_detail_barang($r->idtipe, $r->idbarang)->nama;
                $row['distributor'] = $r->nama;
                $row['namasatuan'] = $this->_get_namasatuan_barang($r->idtipe, $r->idbarang, $r->opsisatuan);
                $row['harga'] = $r->harga;
                $row['totalharga'] = 0;
                $row['kuantitas_sisa'] = $this->model->get_kuantitas_sisa($id, $r->idbarang, $r->idtipe);
                $row['kuantitas'] = 0;
                $row['idbarang'] = $r->idbarang;
                $row['idtipe'] = $r->idtipe;
                $row['iddistributor'] = $r->iddistributor;
                $row['opsisatuan'] = $r->opsisatuan;
                $row['nobatch'] = 0;
                $row['edit'] = "<a href='#edit' class='edit-detail'><i class='fa fa-pencil'></i></a>";
                $row['idpemesanandetail'] = $r->id;
                $data[] = $row;
            }
            $output = array('data' => $data);
            $this->output->set_output(json_encode($output));
        }
    }

    private function _get_namasatuan_barang($idtipe = 1, $idbarang = 1, $opsisatuan = 1) {
        $from = array(null, 'mdata_alkes', 'mdata_implan', 'mdata_obat', 'mdata_logistik');

        $this->db->select('satuan.nama');
        if (1 == $idtipe || 3 == $idtipe) {
            if (1 == $opsisatuan) {
                $this->db->join('msatuan satuan', 'barang.idsatuankecil = satuan.id', 'left');
            } else {
                $this->db->join('msatuan satuan', 'barang.idsatuanbesar = satuan.id', 'left');
            }
        } else {
            $this->db->join('msatuan satuan', 'barang.idsatuan = satuan.id', 'left');
        }
        $this->db->where('barang.id', $idbarang);
        $this->db->from($from[$idtipe].' barang');
        $res = $this->db->get()->row_array()['nama'];

        return $res;
    }

    public function get_satuan_barang() {
        $this->model->get_satuan_barang();
    }

    public function update() {
        $this->model->update();
    }
	public function upload_lampiran() {
       $uploadDir = './assets/upload/penerimaan';
		if (!empty($_FILES)) {
			 $idpenerimaan = $this->input->post('idpenerimaan');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['idpenerimaan'] 	= $idpenerimaan;
			$detail['file_name']		= $file_name;
			$detail['size']		= formatSizeUnits($size);
			$detail['tanggal_upload']	= date('Y-m-d H:i:s');
			$detail['user_upload']	= $this->session->userdata('user_name');
			$this->db->insert('tgudang_penerimaan_lampiran', $detail);
		}
    }
	function hapus_file(){
		$id=$this->input->post('id');
		$row = $this->model->get_file_name($id);
		if(file_exists('./assets/upload/penerimaan/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/penerimaan/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from tgudang_penerimaan_lampiran WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	function hapus_bayar_head($id){		
		$this->db->query("UPDATE tgudang_penerimaan_pembayaran set status='0',deleted_by='".$this->session->userdata('user_id')."',deleted_date='".date('Y-m-d H:i:s')."' WHERE head_bayar_id='$id'");
		$result=$this->db->query("UPDATE tgudang_penerimaan_pembayaran_head set status='0' WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	public function hapus_lampiran($id,$idpenerimaan){
		$row = $this->model->get_file_name($id);
		if ($row){
			if(file_exists('./assets/upload/penerimaan/'.$row->file_name) && $row->file_name !='') {
				unlink('./assets/upload/penerimaan/'.$row->file_name);
				$this->db->query("delete from tgudang_penerimaan_lampiran WHERE id='$id'");
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'Lampiran berhasil dihapus.');
				redirect('tgudang_penerimaan/edit/'.$idpenerimaan.'/2', 'refresh');
			}else{
				$this->db->query("delete from tgudang_penerimaan_lampiran WHERE id='$id'");
				redirect('tgudang_penerimaan/edit/'.$idpenerimaan.'/2', 'refresh');
			}
		}else{
			redirect('tgudang_penerimaan/edit/'.$idpenerimaan.'/2', 'refresh');
		}
	}
	function refresh_image($id){
		
		
		$arr['detail'] = $this->model->get_file_lampiran($id);
		$this->output->set_output(json_encode($arr));

	}
	public function edit_no_faktur() {
        $this->model->edit_no_faktur();
    }

    public function print_penerimaan() {
        $id = $this->uri->segment(3);
        if ($id) {
            $data = $this->model->viewHead($id);
            $data['detail'] = $this->model->viewDetail($data['id']);
            $data['title'] = 'Print Penerimaan';
            $data = array_merge($data, backend_info());
            $this->parser->parse('Tgudang_penerimaan/print_penerimaan', $data);
        }
    }

    public function view() {
        $id = $this->uri->segment(3);
        if ($id) {
            $data = $this->model->viewHead($id);
            $data['error'] = '';
            $data['title'] = 'Detail Penerimaan Gudang';
            $data['content'] = 'Tgudang_penerimaan/view';
            $data['breadcrum'] = array(array('RSKB Halmahera', '#'),
                array('Penerimaan Gudang', '#'),
                array('List', 'tgudang_penerimaan'), );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            show_404();
        }
    }

    public function edit($id,$tkontrabon_id='',$id_header_kontrabon='') {
        if ($id) {
            $data = array(
                'id' => '',
                'nopenerimaan' => '',
                'idpemesanan' => '',
                'totalbarang' => '',
                'totalharga' => '',
            );

            $data = $this->model->viewHead($id);
			// print_r($data);exit();
			
            $data['tkontrabon_id'] = $tkontrabon_id;
            $data['id_header_kontrabon'] = $id_header_kontrabon;
            $data['detail'] = $this->model->detail_penerimaan($id);
            $data['list_lampiran'] = $this->model->list_lampiran($id);
            $data['detail_user'] = $this->model->find_detail_user($data['idpemesanan']);
            $data['error'] = '';
            $data['title'] = 'Edit Penerimaan Gudang';
            $data['content'] = 'Tgudang_penerimaan/edit';
            $data['breadcrum'] = array(
                array('RSKB Halmahera', '#'),
                array('Penerimaan Gudang', '#'),
                array('Edit', '#'),
            );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            show_404();
        }
    }
	public function detail_bayar($id,$head_bayar_id='',$disabel='') {
        if ($id) {
            
            $data = $this->model->viewHead($id);
			// print_r($data);exit();
			
            $data['head_bayar_id'] = $head_bayar_id;
            $data['disabel'] = $disabel;
            $data['idmetode'] = '#';
            // $data['id_header_kontrabon'] = $id_header_kontrabon;
			$data['list_barang']      = $this->model->list_barang_detail($id,$head_bayar_id);
			$data['list_pembayaran']      = $this->model->list_pembayaran($id,$head_bayar_id);
			$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
            // $data['detail_user'] = $this->model->find_detail_user($data['idpemesanan']);
            $data['error'] = '';
            $data['title'] = 'Pembayaran Penerimaan Gudang';
            $data['content'] = 'Tgudang_penerimaan/manage_bayar';
            $data['breadcrum'] = array(
                array('RSKB Halmahera', '#'),
                array('Penerimaan Gudang', '#'),
                array('Transaksi Kas', '#'),
            );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            show_404();
        }
    }
	public function bayar($id,$disabel='') {
        if ($id) {
            
            $data = $this->model->viewHead($id);
			// print_r($data);exit();
			
            $data['disabel'] = $disabel;
            $data['idmetode'] = '#';
            // $data['id_header_kontrabon'] = $id_header_kontrabon;
			$data['list_barang']      = $this->model->list_barang_all($id);
			// print_r(count($data['list_barang']));exit();
			$data['list_pembayaran']      = $this->model->list_pembayaran_head($id);
			$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
            // $data['detail_user'] = $this->model->find_detail_user($data['idpemesanan']);
            $data['jml_row'] = count($data['list_barang']);
			if ($data['jml_row']>0){
				$data['error'] = '';
				
			}else{
				$data['error'] = 'Anda Tidak Memiliki Akses Ke pembayaran ini !';
			}
            $data['title'] = 'Pembayaran Penerimaan Gudang';
            $data['content'] = 'Tgudang_penerimaan/manage_bayar_head';
            $data['breadcrum'] = array(
                array('RSKB Halmahera', '#'),
                array('Penerimaan Gudang', '#'),
                array('Transaksi Kas', '#'),
            );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            show_404();
        }
    }
	public function detail($id) {
        if ($id) {
            $data = array(
                'id' => '',
                'nopenerimaan' => '',
                'idpemesanan' => '',
                'totalbarang' => '',
                'totalharga' => '',
            );

            $data = $this->model->viewHead($id);
			// print_r($data);exit();
            $data['detail'] = $this->model->detail_penerimaan($id);
			$data['list_lampiran'] = $this->model->list_lampiran($id);
            $data['detail_user'] = $this->model->find_detail_user($data['idpemesanan']);
            $data['detail_user_batal'] = $this->model->find_detail_user_batal($id);
            $data['error'] = '';
            $data['title'] = 'View Penerimaan Gudang';
            $data['content'] = 'Tgudang_penerimaan/detail';
            $data['breadcrum'] = array(
                array('RSKB Halmahera', '#'),
                array('Penerimaan Gudang', '#'),
                array('View', '#'),
            );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            show_404();
        }
    }
	public function batalkan($id) {
        if ($id) {
            $data = array(
                'id' => '',
                'nopenerimaan' => '',
                'idpemesanan' => '',
                'totalbarang' => '',
                'totalharga' => '',
            );

            $data = $this->model->viewHead($id);
            $data['detail'] = $this->model->detail_penerimaan($id);
            $data['detail_user'] = $this->model->find_detail_user($data['idpemesanan']);
			// print_r($data['detail_user'] );exit();
            $data['error'] = '';
            $data['title'] = 'Form Pembatalan';
            $data['content'] = 'Tgudang_penerimaan/view_batal';
            $data['breadcrum'] = array(
                array('RSKB Halmahera', '#'),
                array('Penerimaan Gudang', '#'),
                array('View', '#'),
            );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        } else {
            show_404();
        }
    }
	function find_detail($id)
	{
		$arr = $this->model->find_detail($id);
		$this->output->set_output(json_encode($arr));
	}
	function find_detail_user($id)
	{
		$arr = $this->model->find_detail_user($id);
		$this->output->set_output(json_encode($arr));
	}
	function find_cara_bayar($id)
	{
		$q="SELECT H.tipe_bayar FROM tgudang_pemesanan H WHERE H.id='$id'";
		$arr = $this->db->query($q)->row('tipe_bayar');
		$this->output->set_output(json_encode($arr));
	}
	function get_distributor()
	{
		$this->model->get_distributor();
		// $this->output->set_output(json_encode($arr));
	}
	public function print_label()
    {
        $data = array();
		// $hasil=62 % 5;
		// print_r($hasil);exit();
        // $idpenjualan=$this->input->post('idpenjualan');
        $jenis=$this->input->post('jenis');
        $chck_obat=$this->input->post('checked');
        $idetail=$this->input->post('idetail');
        $namabarang=$this->input->post('namabarang');
        $nobatch=$this->input->post('nobatch');
        $tanggalkadaluarsa=$this->input->post('tanggalkadaluarsa');
        $tgl_terima=$this->input->post('tgl_terima');
        $barcode=$this->input->post('barcode');
        $jml=$this->input->post('jml');
        $counter=$this->input->post('counter');
		$start_awal=$this->input->post('start_awal');
        $akhir=$counter + $start_awal - 1;
        $index_obat=array();
        $i=0;
        if ($chck_obat) {
            foreach ($chck_obat as $index => $value) {
                if ($value==1) {
                    $index_obat[$i]=$index;
                    $i=$i+1;
                }
            }
        }
		$final=array();
		
		// do {
			// $final[$i]=$i;
			// // echo $i;
			// $i=$i+1;
		// } while ($i > $akhir);
		$i = 1;
		do {
			if ($i>=$start_awal){
				
				foreach ($index_obat as $index => $value) {
					for ($x = 1; $x <= $jml[$value]; $x++) {
						// echo "INDEX ".$i." :".$namabarang[$value]."<br/>";
						$final[$i]=array(
							'namabarang'=>$namabarang[$value],
							'nobatch'=>$nobatch[$value],
							'tgl_terima'=>$tgl_terima[$value],
							'tanggalkadaluarsa'=>$tanggalkadaluarsa[$value],
							'barcode'=>$barcode[$value],
						);
						++$i;
					}
				}
			}else{
				// echo ' <br/>';
				$final[$i]=array(
							'namabarang'=>'',
							'nobatch'=>'',
							'tgl_terima'=>'',
							'tanggalkadaluarsa'=>'',
							'barcode'=>'',
						);
				++$i;
			}
			
		} while ($i <= $akhir);
		// print_r($jenis);exit();
		if ($jenis=='1'){			
			$hasil=$akhir % 2;
			if ($hasil=='1'){
				$final[$akhir+1]=array(
								'namabarang'=>'',
								'nobatch'=>'',
								'tgl_terima'=>'',
								'tanggalkadaluarsa'=>'',
								'barcode'=>'',
							);
				$akhir=$akhir+1;
			}
		}else{
			
			$hasil=$akhir % 9;
			// print_r($hasil);exit();
			if ($hasil !='0'){
			while($hasil < 9) {
				$akhir++;
			  // echo "The number is: $x <br>";
			  $final[$akhir]=array(
								'namabarang'=>'',
								'nobatch'=>'',
								'tgl_terima'=>'',
								'tanggalkadaluarsa'=>'',
								'barcode'=>'',
							);
			  
			  $hasil++;
			}
			}

			

		}
		// print_r($final);exit();
        
        

        // print_r("<pre>");
        // print_r($data['list_data_ticket_obat']);
        // print_r("<br/>");

        // print_r($data['list_data_ticket_racikan']);
        // print_r("</pre>");
        // exit();
        $data['akhir']=$akhir;
        $data['final']=$final;
		// print_r($data);
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);
		if ($jenis=='1'){
			$html = $this->parser->parse('Tgudang_penerimaan/print_sticker', array_merge($data, backend_info()), true);
		}else{
			$html = $this->parser->parse('Tgudang_penerimaan/print_sticker109', array_merge($data, backend_info()), true);
		}
        // print_r($html);exit();
        $html = $this->parser->parse_string($html, $data);

        $dompdf->loadHtml($html);
        // (Optional) Setup the paper size and orientation

        // $customPaper = array(0,0,462,600);
        // $dompdf->set_paper($customPaper, 'portrait');
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('Faktur .pdf', array("Attachment"=>0));
    }
	public function edit_tanggal()
    {
		
        $id = $this->input->post('id');
        $tkontrabon_id = $this->input->post('tkontrabon_id');
        $tgl_edit_kbo = YMDFormat($this->input->post('tgl_baru'));
		
		
		$sql="UPDATE tgudang_penerimaan SET tanggaljatuhtempo='$tgl_edit_kbo' WHERE id='".$id."'";
		$this->db->query($sql);
	
		$sql="DELETE FROM tkontrabon_detail  WHERE id='$tkontrabon_id'";
		$result=$this->db->query($sql);
		
		// $result=true;
		
        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function save_pembayaran()
    {
		$id=$this->input->post('id');
		// $id=$this->model->save_pembayaran();
		if($this->model->save_pembayaran()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tgudang_penerimaan/bayar/'.$id,'location');
		}
	}

}

/* End of file Tgudang_penerimaan.php */
/* Location: ./application/controllers/Tgudang_penerimaan.php */
