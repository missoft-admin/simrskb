<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpengelolaan extends CI_Controller {

	/**
	 * Pegawai controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpengelolaan_model');
		$this->load->model('Mdata_pengelolaan_model');
		$this->load->model('Mrka_bendahara_model');
		
  }
	function insert_validasi_pengelolaan($id){
		$this->Tpengelolaan_model->insert_validasi_pengelolaan($id);
	}
	function index($idkategori='0',$idakun='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->auto_generate();
		if (UserAccesForm($user_acces_form,array('2'))){
			$data = array();
			$data['tanggal_trx1'] 			= '';
			$data['tanggal_trx2'] 			= '';
			$data['status'] 			= '#';
			$data['error'] 			= '';
			$data['title'] 			= 'Transaksi Pengelolaan';
			$data['content'] 		= 'Tpengelolaan/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Transaksi Pengelolaan",'#'),
												  array("List",'tpengelolaan')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function getIndex($idkategori='0',$idakun='0')
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// idakun:idakun,status:status,nama:nama
			$where='';
			$status = $this->input->post('status');
			$nama = $this->input->post('nama');
			
			if ($status !='#'){
				$where .=" AND M.status='$status'";
			}
			if ($nama !=''){
				$where .=" AND M.nama LIKE '%".$nama."%'";
			}
			$this->select = array();
			$from="
					(
						SELECT H.id,H.notransaksi,H.idpengelolaan,H.tanggal_hitung,H.nama_pengelolaan,H.deskripsi_pengelolaan,H.nominal
						,H.`status`,H.status_hapus,H.st_proses,H.st_approval,H.total_piutang,H.total_hutang,H.total_bayar,H.total_terima 
						,SUM(B.nominal_bayar) as bayar 
						FROM tpengelolaan H
						LEFT JOIN (
							SELECT H.tpengelolaan_id,H.nominal_bayar from tpengelolaan_bayar_terima H
							WHERE H.`status`='1'
							UNION ALL
							SELECT H.tpengelolaan_id,H.nominal_bayar from tpengelolaan_bayar H
							WHERE H.`status`='1'
						) B ON B.tpengelolaan_id=H.id
						WHERE H.status_hapus='0' ".$where."
						GROUP BY H.id
					) as tbl 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('notransaksi','nama_pengelolaan','tanggal_hitung','deskripsi_pengelolaan');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $r->id;
          $result[] = $no;
          $result[] = $r->notransaksi;
          $result[] = $r->nama_pengelolaan;
          $result[] = HumanDateShort($r->tanggal_hitung);
          $result[] = $r->deskripsi_pengelolaan;
          $result[] = number_format($r->nominal,0);
		  if ($r->st_proses=='0'){
			   $result[] = text_default('BELUM DIPROSES');
		  }else{
			  if ($r->st_proses=='1'){
				$result[] = text_success('SUDAH DIPROSES');
			  }
			  if ($r->st_proses=='2'){					
				$result[] = status_approval_HD($r->st_approval);					
			  }
			  if ($r->st_proses=='3'){					
				$result[] = text_primary('PEMBAYARAN');					
			  }
			  if ($r->st_proses=='4'){					
				$result[] = text_success('SELESAI');					
			  }
		  }
        
          // $result[] = GetKategoriPegawai($r->idkategori);
          $aksi = '<div class="btn-group">';
			if ($r->status=='1'){
					$aksi .= '<a href="'.site_url().'tpengelolaan/update/'.$r->id.'/disabled" data-toggle="tooltip" title="Lihat" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
				if ($r->st_proses=='0'){
					$aksi .= '<a href="'.site_url().'tpengelolaan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
					// $aksi .= '<button title="Hapus" class="btn btn-danger btn-xs removeData"><i class="fa fa-trash-o"></i></button>';
				}else{
					if ($r->st_proses=='1'){
						$aksi .= '<button data-toggle="tooltip" title="Proses Approval" onclick="kirim('.$r->id.')" class="btn btn-danger btn-xs kirim"><i class="fa fa-send"></i></button>';
					}
					if ($r->st_proses=='2' && $r->st_approval=='3'){
						$aksi .= '<button data-toggle="tooltip" title="Proses Approval" onclick="kirim('.$r->id.')" class="btn btn-danger btn-xs kirim"><i class="fa fa-send"></i></button>';
					}
					if ($r->st_proses=='3'){	
						
						if ($r->total_terima > 0){
							$aksi .= '<a href="'.site_url().'tpengelolaan/proses_terima/'.$r->id.'" data-toggle="tooltip" title="Terima Pembayaran" class="btn btn-primary btn-xs"><i class="fa fa-credit-card"></i></a>';
							
						}
						if ($r->total_bayar > 0){
							$aksi .= '<a href="'.site_url().'tpengelolaan/proses_bayar/'.$r->id.'" data-toggle="tooltip" title="Pembayaran" class="btn btn-danger btn-xs"><i class="fa fa-credit-card"></i></a>';
							
						}
						if ($r->nominal==$r->bayar){
							$aksi .= '<button data-toggle="tooltip" title="Pembayaran" class="btn btn-primary btn-xs verifikasi" onclick="verifikasi('.$r->id.')"><i class="fa fa-check"></i> Verifikasi</button>';
							
						}
					}
					if ($r->st_proses=='4'){	
						
						if ($r->total_terima > 0){
							$aksi .= '<a href="'.site_url().'tpengelolaan/proses_terima/'.$r->id.'/disabled" data-toggle="tooltip" title="Terima Pembayaran" class="btn btn-default btn-xs"><i class="fa fa-credit-card"></i></a>';
							
						}
						if ($r->total_bayar > 0){
							$aksi .= '<a href="'.site_url().'tpengelolaan/proses_bayar/'.$r->id.'/disabled" data-toggle="tooltip" title="Pembayaran" class="btn btn-default btn-xs"><i class="fa fa-credit-card"></i></a>';
							
						}
						
					}
				}
				
			}else{
				$aksi .= '<button title="Aktifkan" class="btn btn-success btn-xs aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
			}
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function auto_generate(){
	  $q="SELECT H.*,T.id as idpengelolaan FROM (
			SELECT H.id,H.nama,H.`status`,CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-',LPAD(D.tanggal,2,'0')) as tanggal,D.deskripsi,H.st_harga_jual FROM mdata_pengelolaan H
			LEFT JOIN mdata_pengelolaan_tanggal D ON D.idpengelolaan=H.id
			WHERE H.`status`='1' AND D.tanggal IS NOT NULL
			) H 
			LEFT JOIN tpengelolaan T ON T.idpengelolaan=H.id AND T.tanggal_hitung=H.tanggal
			WHERE H.tanggal >=CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-01') AND T.id IS NULL";
	  $rows=$this->db->query($q)->result();
	  foreach ($rows as $r){
		$data_header=array(
			'idpengelolaan'=>$r->id,
			'tanggal_hitung'=>$r->tanggal,
			'nama_pengelolaan'=>$r->nama,
			'st_harga_jual'=>$r->st_harga_jual,
			'deskripsi_pengelolaan'=>$r->deskripsi,
			'created_by'=>$this->session->userdata('user_id'),
			'created_date'=>date('Y-m-d H:i:s'),
		);
		$this->db->insert('tpengelolaan',$data_header);
		// $tpengelolaan_id=$this->db->insert_id();
		$tpengelolaan_id=$this->db->insert_id();
		$q_hutang="SELECT H.mdata_hutang_id,M.nama as nama_transaksi,H.nominal_default as nominal,1 as kuantitas,H.nominal_default as totalkeseluruhan,1 as st_insert 
					FROM `mdata_pengelolaan_hutang` H
					LEFT JOIN mdata_hutang M ON M.id=H.mdata_hutang_id
					WHERE H.idpengelolaan='".$r->id."' AND H.st_rutin='1' AND H.`status`='1'";
		 $rows_hutang=$this->db->query($q_hutang)->result();
		 // print_r($rows_hutang);exit();
		 foreach($rows_hutang as $row){
			 $data_detail=array(
				'tpengelolaan_id' => $tpengelolaan_id,
				'mdata_hutang_id' => $row->mdata_hutang_id,
				'nama_transaksi' => $row->nama_transaksi,
				'tanggal_transaksi' => $r->tanggal,
				'nominal' => $row->nominal,
				'kuantitas' => $row->kuantitas,
				'totalkeseluruhan' => $row->totalkeseluruhan,
				'st_insert' => $row->st_insert,
				'keterangan' => '',
				'created_by'=>$this->session->userdata('user_id'),
				'created_date'=>date('Y-m-d H:i:s'),
			 );
			 $this->db->insert('tpengelolaan_hutang',$data_detail);
		 }
		 $q_hutang="SELECT H.mdata_piutang_id,M.nama as nama_transaksi,H.nominal_default as nominal,1 as kuantitas,H.nominal_default as totalkeseluruhan,1 as st_insert 
					FROM `mdata_pengelolaan_piutang` H
					LEFT JOIN mdata_piutang M ON M.id=H.mdata_piutang_id
					WHERE H.idpengelolaan='".$r->id."' AND H.st_rutin='1' AND H.`status`='1'";
		 $rows_piutang=$this->db->query($q_hutang)->result();
		 foreach($rows_piutang as $row){
			 $data_detail=array(
				'tpengelolaan_id' => $tpengelolaan_id,
				'mdata_piutang_id' => $row->mdata_piutang_id,
				'nama_transaksi' => $row->nama_transaksi,
				'tanggal_transaksi' => $r->tanggal,
				'nominal' => $row->nominal,
				'kuantitas' => $row->kuantitas,
				'totalkeseluruhan' => $row->totalkeseluruhan,
				'st_insert' => $row->st_insert,
				'keterangan' => '',
				'created_by'=>$this->session->userdata('user_id'),
				'created_date'=>date('Y-m-d H:i:s'),
			 );
			 $this->db->insert('tpengelolaan_piutang',$data_detail);
		 }
	  }
  }
	function create(){
		
		$data = array(
			'id'            => '',
			'nama' 					=> '',
			'idakun' 	=> '#',
			'nominal_default' 	=> '0',
		);

		$data['error'] 			= '';
		$data['list_akun'] 			= $this->Tpengelolaan_model->list_akun();
		$data['title'] 			= 'Tambah Transaksi Pengelolaan';
		$data['content'] 		= 'Tpengelolaan/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Transaksi Pengelolaan",'#'),
								            array("Tambah",'tpengelolaan')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id,$disabel=''){
		
		$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d'));
		// date_add($date1,date_interval_create_from_date_string("-30 days"));
		date_add($date2,date_interval_create_from_date_string("-30 days"));
		$date1= date_format($date1,"Y-m-d");
		$date2= date_format($date2,"Y-m-d");


		if($id != ''){
			$data= $this->Tpengelolaan_model->getSpecified($id);
			// print_r($data);exit();
			// $data['list_akun'] 			= $this->Tpengelolaan_model->list_akun();
			
			
			$data['list_hutang'] 			= $this->Mdata_pengelolaan_model->list_hutang();
			$data['list_piutang'] 			= $this->Mdata_pengelolaan_model->list_piutang();
			$data['xtgl1'] 			= HumanDateShort($date2);
			$data['xtgl2'] 			= HumanDateShort($date1);
			// print_r($data);exit();
			$data['error'] 			= '';
			$data['disabel'] 			= $disabel;
			$data['title'] 			= 'Ubah Transaksi Pengelolaan';
			$data['content']    = 'Tpengelolaan/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Transaksi Pengelolaan",'#'),
										array("Ubah",'tpengelolaan')
										);

			// $data['statusAvailableApoteker'] = $this->Tpengelolaan_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('tpengelolaan');
		}
	}
	function proses_terima($id,$disabel=''){
		
		if($id != ''){
			$data= $this->Tpengelolaan_model->getSpecified($id);
			// print_r($data);exit();
			// $data['list_akun'] 			= $this->Tpengelolaan_model->list_akun();
			
			$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
			$data['list_pembayaran'] 			= $this->Tpengelolaan_model->list_pembayaran($id);
			$data['list_hutang'] 			= $this->Mdata_pengelolaan_model->list_hutang();
			$data['list_piutang'] 			= $this->Mdata_pengelolaan_model->list_piutang();
			$data['error'] 			= '';
			$data['disabel'] 			= $disabel;
			$data['title'] 			= 'Terima Pembayaran';
			$data['content']    = 'Tpengelolaan/manage_terima';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Transaksi Pengelolaan",'#'),
										array("Ubah",'tpengelolaan')
										);

			// $data['statusAvailableApoteker'] = $this->Tpengelolaan_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('tpengelolaan');
		}
	}
	function proses_bayar($id,$disabel=''){
		
		if($id != ''){
			$data= $this->Tpengelolaan_model->getSpecified($id);
			// print_r($data);exit();
			// $data['list_akun'] 			= $this->Tpengelolaan_model->list_akun();
			
			$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
			$data['list_pembayaran'] 			= $this->Tpengelolaan_model->list_pembayaran($id);
			$data['list_hutang'] 			= $this->Mdata_pengelolaan_model->list_hutang();
			$data['list_piutang'] 			= $this->Mdata_pengelolaan_model->list_piutang();
			$data['error'] 			= '';
			$data['disabel'] 			= $disabel;
			$data['title'] 			= 'Proses Pembayaran';
			$data['content']    = 'Tpengelolaan/manage_bayar';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Transaksi Pengelolaan",'#'),
										array("Ubah",'tpengelolaan')
										);

			// $data['statusAvailableApoteker'] = $this->Tpengelolaan_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('tpengelolaan');
		}
	}
	
	function delete($id){
		
		$result=$this->Tpengelolaan_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('tpengelolaan','location');
	}
	function aktifkan($id){
		
		$result=$this->Tpengelolaan_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	function verifikasi($id){
		
		$result=$this->Tpengelolaan_model->verifikasi($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){	
		// print_r($this->input->post());exit();
		if($this->input->post('tpengelolaan_id') == '' ) {
			if($this->Tpengelolaan_model->saveData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tpengelolaan','location');
			}
		} else {
			if($this->Tpengelolaan_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tpengelolaan','location');
			}
		}
		
	}
	function get_info($id){
		$q="SELECT SUM(IFNULL(H.hutang,0)) as hutang,SUM(IFNULL(H.piutang,0)) as piutang FROM (
			SELECT H.tpengelolaan_id,H.nominal as hutang,0 as piutang FROM tpengelolaan_hutang H WHERE H.tpengelolaan_id='$id'
			UNION ALL
			SELECT H.tpengelolaan_id,H.nominal as hutang,0 as piutang FROM tpengelolaan_pendapatan H WHERE H.tpengelolaan_id='$id'
			UNION ALL
			SELECT H.tpengelolaan_id,0 as hutang,H.nominal as piutang FROM tpengelolaan_piutang H WHERE H.tpengelolaan_id='$id'
			UNION ALL
			SELECT H.tpengelolaan_id,0 as hutang,H.nominal as piutang FROM tpengelolaan_permintaan H WHERE H.tpengelolaan_id='$id'
			) H 
			GROUP BY H.tpengelolaan_id";
		$row=$this->db->query($q)->row();
		$total_hutang=$row->hutang;
		$total_piutang=$row->piutang;
		$total_bayar=0;$total_terima=0;
		$keterangan='TIDAK ADA TRANSAKSI PEMBAYARAN';
		if ($total_hutang > $total_piutang){
			$total_bayar=$total_hutang - $total_piutang;
			$nominal=$total_bayar;
			$keterangan='RS HARUS BAYAR';
		}
		if ($total_hutang < $total_piutang){
			$total_terima=$total_piutang- $total_hutang;
			$nominal=$total_terima;
			$keterangan='RS MENERIMA PEMBAYARAN';
		}
		$arr=array(
			'nominal'=> $nominal, 
			'total_hutang'=> $total_hutang, 
			'total_piutang'=> $total_piutang, 
			'total_bayar'=> $total_bayar, 
			'total_terima'=> $total_terima, 
			'keterangan'=> $keterangan, 
		);
		$this->db->where('id',$id);
		$this->db->update('tpengelolaan',$arr);
		$this->output->set_output(json_encode($arr));
	}
	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Tpengelolaan/manage';

		if($id==''){
			$data['title'] = 'Tambah Transaksi Pengelolaan';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Transaksi Pengelolaan",'#'),
							               array("Tambah",'tpengelolaan')
								           );
		}else{
			$data['title'] = 'Ubah Transaksi Pengelolaan';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Transaksi Pengelolaan",'#'),
							               array("Ubah",'tpengelolaan')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	//Hutang
	function load_hutang()
    {
	 $tpengelolaan_id=$this->input->post('tpengelolaan_id');
	 $disabel=$this->input->post('disabel');
	$this->select = array();
	$this->from   = "(SELECT * FROM tpengelolaan_hutang H
					WHERE H.tpengelolaan_id='$tpengelolaan_id')as tbl";
	$this->join 	= array();
	$this->where  = array();
	$this->order  = array();
	$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
			$aksi = '<div class="btn-group">';					
			$aksi .= '<button data-toggle="tooltip" title="Edit" class="btn btn-success btn-xs edit_hutang" '.$disabel.'><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-xs hapus_hutang" '.$disabel.'><i class="fa fa-trash"></i></button>';
			$aksi .= '</div>';

          $row[] = $r->id;
          $row[] = $no;
          $row[] = $r->nama_transaksi;
          $row[] = number_format($r->nominal,0);
          $row[] = number_format($r->kuantitas,0);
          $row[] = number_format($r->totalkeseluruhan,0);
          $row[] = $r->keterangan;
          $row[] = $aksi;
		  $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function get_data_hutang($id){
		$q="SELECT H.id,H.mdata_hutang_id,H.nominal,H.kuantitas,H.totalkeseluruhan,H.keterangan 
			FROM `tpengelolaan_hutang` H
			WHERE H.id='$id'";
		$data['detail']=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($data));
	}
	function simpan_hutang(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$tedit=$this->input->post('tedit_hutang');
		$tpengelolaan_id=$this->input->post('tpengelolaan_id');
		$mdata_hutang_id=$this->input->post('mdata_hutang_id');
		$nama_transaksi=$this->input->post('nama_transaksi');
		$nominal=$this->input->post('nominal');
		$kuantitas=$this->input->post('kuantitas');
		$totalkeseluruhan=$this->input->post('totalkeseluruhan');
		$keterangan=$this->input->post('keterangan');
		if ($tedit==''){
			$data_det=array(
					'tpengelolaan_id'=>$tpengelolaan_id,
					'mdata_hutang_id'=>$mdata_hutang_id,
					'nama_transaksi'=>$nama_transaksi,
					'nominal'=>$nominal,
					'kuantitas'=>$kuantitas,
					'totalkeseluruhan'=>$totalkeseluruhan,
					'keterangan'=>$keterangan,
					'tanggal_transaksi'=>date('Y-m-d H:i:s'),
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
					
			$result=$this->db->insert('tpengelolaan_hutang',$data_det);
		}else{
			$data_det=array(
					'tpengelolaan_id'=>$tpengelolaan_id,
					'mdata_hutang_id'=>$mdata_hutang_id,
					'nama_transaksi'=>$nama_transaksi,
					'nominal'=>$nominal,
					'kuantitas'=>$kuantitas,
					'totalkeseluruhan'=>$totalkeseluruhan,
					'keterangan'=>$keterangan,
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->where('id',$tedit);
			$result=$this->db->update('tpengelolaan_hutang',$data_det);
		}
		$this->output->set_output(json_encode($result));
	}
	
	function hapus_hutang(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$result=$this->db->delete('tpengelolaan_hutang');
		$this->output->set_output(json_encode($result));
	}
	//Piutang
	function load_piutang()
    {
	 $tpengelolaan_id=$this->input->post('tpengelolaan_id');
	 $disabel=$this->input->post('disabel');
	$this->select = array();
	$this->from   = "(SELECT * FROM tpengelolaan_piutang H
					WHERE H.tpengelolaan_id='$tpengelolaan_id')as tbl";
	$this->join 	= array();
	$this->where  = array();
	$this->order  = array();
	$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
			$aksi = '<div class="btn-group">';					
			$aksi .= '<button data-toggle="tooltip" title="Edit" class="btn btn-success btn-xs edit_piutang" '.$disabel.'><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-xs hapus_piutang" '.$disabel.'><i class="fa fa-trash"></i></button>';
			$aksi .= '</div>';

          $row[] = $r->id;
          $row[] = $no;
          $row[] = $r->nama_transaksi;
          $row[] = number_format($r->nominal,0);
          $row[] = number_format($r->kuantitas,0);
          $row[] = number_format($r->totalkeseluruhan,0);
          $row[] = $r->keterangan;
          $row[] = $aksi;
		  $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
	function get_data_piutang($id){
		$q="SELECT H.id,H.tpengelolaan_id,H.nominal,H.kuantitas,H.totalkeseluruhan,H.keterangan,H.mdata_piutang_id
			FROM `tpengelolaan_piutang` H
			WHERE H.id='$id'";
		$data['detail']=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($data));
	}
	
	
	function simpan_piutang(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$tedit=$this->input->post('tedit_piutang');
		$tpengelolaan_id=$this->input->post('tpengelolaan_id');
		$nama_transaksi=$this->input->post('nama_transaksi');
		$mdata_piutang_id=$this->input->post('mdata_piutang_id');
		$nominal=$this->input->post('nominal');
		$kuantitas=$this->input->post('kuantitas');
		$totalkeseluruhan=$this->input->post('totalkeseluruhan');
		$keterangan=$this->input->post('keterangan');
		if ($tedit==''){
			$data_det=array(
					'tpengelolaan_id'=>$tpengelolaan_id,
					'mdata_piutang_id'=>$mdata_piutang_id,
					'nama_transaksi'=>$nama_transaksi,
					'nominal'=>$nominal,
					'kuantitas'=>$kuantitas,
					'totalkeseluruhan'=>$totalkeseluruhan,
					'keterangan'=>$keterangan,
					'tanggal_transaksi'=>date('Y-m-d H:i:s'),
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
					
			$result=$this->db->insert('tpengelolaan_piutang',$data_det);
		}else{
			$data_det=array(
					'tpengelolaan_id'=>$tpengelolaan_id,
					'mdata_piutang_id'=>$mdata_piutang_id,
					'nama_transaksi'=>$nama_transaksi,
					'nominal'=>$nominal,
					'kuantitas'=>$kuantitas,
					'totalkeseluruhan'=>$totalkeseluruhan,
					'keterangan'=>$keterangan,
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->where('id',$tedit);
			$result=$this->db->update('tpengelolaan_piutang',$data_det);
		}
		$this->output->set_output(json_encode($result));
	}
	
	
	function hapus_piutang(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$result=$this->db->delete('tpengelolaan_piutang');
		$this->output->set_output(json_encode($result));
	}
	
	//PENDPATAN
	function load_cari_pendapatan()
    {
	 $idpendapatan=$this->input->post('xidpendapatan');
	 $terima_dari=$this->input->post('xterima_dari');
	 $notransaksi=$this->input->post('xnotransaksi');
	 $tgl1=$this->input->post('xtgl1');
	 $tgl2=$this->input->post('xtgl2');
	 $where='';
	if ($idpendapatan !='#'){
		$where .=" AND H.idpendapatan='$idpendapatan'";
	}
	if ($terima_dari !='#'){
		$where .=" AND H.terimadari='$terima_dari'";
	}
	if ($notransaksi !=''){
		$where .=" AND H.notransaksi LIKE '%".$notransaksi."%'";
	}
	if ($tgl1 !=''){
		$where .=" AND (H.tanggal >= '".YMDFormat($tgl1)."' AND H.tanggal <= '".YMDFormat($tgl2)."')";
	}
	$this->select = array();
	$this->from   = "(
						SELECT H.id,H.notransaksi,DATE(H.tanggal) as tanggal,H.idpendapatan,MP.keterangan as tipe
						,H.terimadari,MD.nama as terimadari_nama,H.keterangan,H.nominal,TH.id as id_pilih
						FROM tbendahara_pendapatan H
						LEFT JOIN mpendapatan MP ON MP.id=H.idpendapatan
						LEFT JOIN mdari MD ON MD.id=H.terimadari
						LEFT JOIN tpengelolaan_pendapatan TH ON TH.tbendahara_pendapatan_id=H.id
						WHERE H.`status`='1' ".$where."
					)as tbl";
	$this->join 	= array();
	$this->where  = array();
	$this->order  = array();
	$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
		  if ($r->id_pilih){
			  $aksi = text_danger('SUDAH DIPILIH');
		  }else{
				$aksi = '<div class="btn-group">';					
				$aksi .= '<button data-toggle="tooltip" title="Pilih" class="btn btn-primary btn-xs" onclick="pilih_pendapatan('.$r->id.')"><i class="fa fa-check"></i> Pilih</button>';
				$aksi .= '<a href="'.base_url().'tpendapatan/kwitansi/'.$r->id.'" data-toggle="tooltip" title="Pilih" target="_blank" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
				$aksi .= '</div>';
		  }
			

          $row[] = $r->id;
          $row[] = $no;
          $row[] = $r->notransaksi;
          $row[] = HumanDateShort($r->tanggal);
          $row[] = $r->tipe;
          $row[] = $r->keterangan;
          $row[] = $r->terimadari_nama;
          $row[] = number_format($r->nominal,2);
          $row[] = '';
          $row[] = $aksi;
		  $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function get_data_pendapatan_lain($id){
		$q="SELECT H.id,H.notransaksi,DATE(H.tanggal) as tanggal,H.idpendapatan,MP.keterangan as tipe
			,H.terimadari,MD.nama as terimadari_nama,H.keterangan,H.nominal
			FROM tbendahara_pendapatan H
			LEFT JOIN mpendapatan MP ON MP.id=H.idpendapatan
			LEFT JOIN mdari MD ON MD.id=H.terimadari
			WHERE H.`id`='$id'";
		$data['detail']=$this->db->query($q)->row_array();
		$data['detail']['ket']=strip_tags($data['detail']['keterangan']);
		$this->output->set_output(json_encode($data));
	}
	function get_edit_pendapatan_lain($id){
		$q="SELECT *
			FROM tpengelolaan_pendapatan H
			WHERE H.`id`='$id'";
		$data['detail']=$this->db->query($q)->row_array();
		$data['detail']['ket']=strip_tags($data['detail']['keterangan']);
		$this->output->set_output(json_encode($data));
	}
	function simpan_pendapatan(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$tpengelolaan_id=$this->input->post('tpengelolaan_id');
		$tedit=$this->input->post('tedit_pendapatan');
		$tbendahara_pendapatan_id=$this->input->post('tbendahara_pendapatan_id');
		$nama_transaksi=$this->input->post('nama_transaksi');
		$keterangan=$this->input->post('keterangan');
		
		if ($tedit==''){
				$row=$this->db->query("SELECT H.id,H.notransaksi,DATE(H.tanggal) as tanggal,H.idpendapatan,MP.keterangan as tipe
					,H.terimadari,MD.nama as terimadari_nama,H.keterangan,H.nominal
					FROM tbendahara_pendapatan H
					LEFT JOIN mpendapatan MP ON MP.id=H.idpendapatan
					LEFT JOIN mdari MD ON MD.id=H.terimadari
					WHERE H.`id`='$tbendahara_pendapatan_id'")->row();
				$data_det=array(
					'tpengelolaan_id'=>$tpengelolaan_id,
					'tbendahara_pendapatan_id'=>$tbendahara_pendapatan_id,
					'nama_transaksi'=>$nama_transaksi,
					'notransaksi'=>$row->notransaksi,
					'tanggal'=>$row->tanggal,
					'idpendapatan'=>$row->idpendapatan,
					'terimadari'=>$row->terimadari,
					'terimadari_nama'=>$row->terimadari_nama,
					'nominal'=>$row->nominal,
					'keterangan'=>$keterangan,
					// 'tanggal_transaksi'=>date('Y-m-d H:i:s'),
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
					
			$result=$this->db->insert('tpengelolaan_pendapatan',$data_det);
		}else{
			$data_det=array(					
					'keterangan'=>$keterangan,
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->where('id',$tedit);
			$result=$this->db->update('tpengelolaan_pendapatan',$data_det);
		}
		$this->output->set_output(json_encode($result));
	}
	//Piutang
	function load_pendapatan()
    {
	 $tpengelolaan_id=$this->input->post('tpengelolaan_id');
	 $disabel=$this->input->post('disabel');
	$this->select = array();
	$this->from   = "(SELECT * FROM tpengelolaan_pendapatan H
					WHERE H.tpengelolaan_id='$tpengelolaan_id')as tbl";
	$this->join 	= array();
	$this->where  = array();
	$this->order  = array();
	$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
			$aksi = '<div class="btn-group">';					
			$aksi .= '<button data-toggle="tooltip" title="Edit" class="btn btn-success btn-xs edit_pendapatan" '.$disabel.'><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-xs hapus_pendapatan" '.$disabel.'><i class="fa fa-trash"></i></button>';
			$aksi .= '</div>';

          $row[] = $r->id;
          $row[] = $no;
          $row[] = $r->nama_transaksi;
          $row[] = number_format($r->nominal,0);
          $row[] = $r->terimadari_nama;
          $row[] = $r->keterangan;
          $row[] = $aksi;
		  $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_pendapatan(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$result=$this->db->delete('tpengelolaan_pendapatan');
		$this->output->set_output(json_encode($result));
	}
	//PERMINTAAN
	function load_cari_permintaan()
    {
		// xnopermintaan:xnopermintaan
		// ,xdari_id:xdari_id
		// ,xke_id:xke_id
		// ,xtgl1:xtgl1
		// ,xtgl2:xtgl2
		
	 $st_harga_jual=$this->input->post('st_harga_jual');
	 $xdari_id=$this->input->post('xdari_id');
	 $xnopermintaan=$this->input->post('xnopermintaan');
	 $xke_id=$this->input->post('xke_id');
	 $xtgl1=$this->input->post('xtgl1');
	 $xtgl2=$this->input->post('xtgl2');
	 $where='';
	 if ($xnopermintaan !=''){
		$where .=" AND H.nopermintaan LIKE '%".$xnopermintaan."%'";
	}
	if ($xdari_id !='#'){
		$where .=" AND H.idunitpenerima='".$xdari_id."'";
	}
	if ($xke_id !='#'){
		$where .=" AND H.idunitpeminta='".$xke_id."'";
	}
	// if ($xke_id !='#'){
		// $where .=" AND H.idunitpeminta='".$xke_id."'";
	// }
	
	// if ($idpendapatan !='#'){
		// $where .=" AND H.idpendapatan='$idpendapatan'";
	// }
	// if ($terima_dari !='#'){
		// $where .=" AND H.terimadari='$terima_dari'";
	// }
	
	if ($xtgl1 !=''){
		$where .=" AND (DATE(H.datetime) >= '".YMDFormat($xtgl1)."' AND DATE(H.datetime) <= '".YMDFormat($xtgl2)."')";
	}
	$from= "(
				SELECT H.id,H.nopermintaan,H.datetime as tanggal,H.idunitpeminta as ke_id,H.idunitpenerima as dari_id
				,UD.nama as dari_nama,UK.nama as ke_nama,D.idbarang,D.idtipe,B.nama,B.namatipe,D.kuantitas,B.marginumum,SUM(B.hargadasar * D.kuantitaskirim) total_dasar,SUM(D.kuantitaskirim *(B.hargadasar + ((B.marginumum * B.hargadasar)/100))) as total_margin
				,PP.id as id_pilih
				FROM tunit_permintaan H
				INNER JOIN tunit_permintaan_detail D ON D.idpermintaan=H.id
				INNER JOIN view_barang_all B ON B.id=D.idbarang AND B.idtipe=D.idtipe
				INNER JOIN munitpelayanan UD ON UD.id=H.idunitpenerima
				INNER JOIN munitpelayanan UK ON UK.id=H.idunitpeminta
				LEFT JOIN tpengelolaan_permintaan PP ON PP.tpermintaan_id=H.id
				WHERE H.`status`!='0' ".$where."
				GROUP BY H.id
			)as tbl";
			
			// print_r($from);exit();
	$this->select = array();
	$this->from   =$from;
	$this->join 	= array();
	$this->where  = array();
	$this->order  = array();
	$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
		  if ($r->id_pilih){
			  $aksi = text_danger('SUDAH DIPILIH');
		  }else{
				$aksi = '<div class="btn-group">';					
				$aksi .= '<button data-toggle="tooltip" title="Pilih" class="btn btn-primary btn-xs" onclick="pilih_permintaan('.$r->id.')"><i class="fa fa-check"></i> Pilih</button>';
				$aksi .= '<a href="'.base_url().'tunit_permintaan/cetak_permintaan/'.$r->id.'" data-toggle="tooltip" title="Pilih" target="_blank" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
				$aksi .= '</div>';
		  }
			

          $row[] = $r->id;
          $row[] = $no;
          $row[] = $r->nopermintaan;
          $row[] = HumanDateShort($r->tanggal);
          $row[] = $r->dari_nama;
          $row[] = $r->ke_nama;
		  if ($st_harga_jual=='1'){
			$row[] = number_format($r->total_dasar,2);			  
		  }else{			  
			$row[] = number_format($r->total_margin,2);
		  }
          $row[] = $aksi;
		  $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function get_data_permintaan($id){
		$q="SELECT H.id,H.nopermintaan,H.datetime as tanggal,H.idunitpeminta as ke_id,H.idunitpenerima as dari_id
						,UD.nama as dari_nama,UK.nama as ke_nama,D.idbarang,D.idtipe,B.nama,B.namatipe,D.kuantitas,B.marginumum,SUM(B.hargadasar * D.kuantitaskirim) total_dasar
						,SUM(D.kuantitaskirim *(B.hargadasar + ((B.marginumum * B.hargadasar)/100))) as total_margin
						,'0' as id_pilih
						FROM tunit_permintaan H
						LEFT JOIN tunit_permintaan_detail D ON D.idpermintaan=H.id
						LEFT JOIN view_barang_all B ON B.id=D.idbarang AND B.idtipe=D.idtipe
						LEFT JOIN munitpelayanan UD ON UD.id=H.idunitpenerima
						LEFT JOIN munitpelayanan UK ON UK.id=H.idunitpeminta
						WHERE H.`id`='$id'
						GROUP BY H.id";
		$data['detail']=$this->db->query($q)->row_array();
		// $data['detail']['ket']=strip_tags($data['detail']['keterangan']);
		$this->output->set_output(json_encode($data));
	}
	
	function get_edit_permintaan($id){
		$q="SELECT *
			FROM tpengelolaan_permintaan H
			WHERE H.`id`='$id'";
		$data['detail']=$this->db->query($q)->row_array();
		$data['detail']['ket']=strip_tags($data['detail']['keterangan']);
		$this->output->set_output(json_encode($data));
	}
	function simpan_permintaan(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$tpengelolaan_id=$this->input->post('tpengelolaan_id');
		$tedit=$this->input->post('tedit_permintaan');
		$tpermintaan_id=$this->input->post('tpermintaan_id');
		$nama_transaksi=$this->input->post('nama_transaksi');
		$keterangan=$this->input->post('keterangan');
		$st_harga_jual=$this->input->post('st_harga_jual');
		
		if ($tedit==''){
				$row=$this->db->query("SELECT H.id,H.nopermintaan,H.datetime as tanggal,H.idunitpeminta as ke_id,H.idunitpenerima as dari_id
						,UD.nama as dari_nama,UK.nama as ke_nama,D.idbarang,D.idtipe,B.nama,B.namatipe,D.kuantitas,B.marginumum,SUM(B.hargadasar * D.kuantitaskirim) total_dasar
						,SUM(D.kuantitaskirim *(B.hargadasar + ((B.marginumum * B.hargadasar)/100))) as total_margin
						,'0' as id_pilih
						FROM tunit_permintaan H
						LEFT JOIN tunit_permintaan_detail D ON D.idpermintaan=H.id
						LEFT JOIN view_barang_all B ON B.id=D.idbarang AND B.idtipe=D.idtipe
						LEFT JOIN munitpelayanan UD ON UD.id=H.idunitpenerima
						LEFT JOIN munitpelayanan UK ON UK.id=H.idunitpeminta
						WHERE H.`id`='$tpermintaan_id'
						GROUP BY H.id")->row();
				if ($st_harga_jual=='1'){
					$nominal=$row->total_dasar;
				}else{
					$nominal=$row->total_margin;					
				}
				$data_det=array(
					'tpengelolaan_id'=>$tpengelolaan_id,
					'tpermintaan_id'=>$tpermintaan_id,
					'nama_transaksi'=>$nama_transaksi,
					'notransaksi'=>$row->nopermintaan,
					'tanggal'=>$row->tanggal,
					'dari_id'=>$row->dari_id,
					'ke_id'=>$row->ke_id,
					'dari_nama'=>$row->dari_nama,
					'ke_nama'=>$row->ke_nama,
					'nominal'=>$nominal,
					'keterangan'=>$keterangan,
					// 'tanggal_transaksi'=>date('Y-m-d H:i:s'),
					'created_by'=>$this->session->userdata('user_id'),
					'created_date'=>date('Y-m-d H:i:s'),
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
					
			$result=$this->db->insert('tpengelolaan_permintaan',$data_det);
		}else{
			$data_det=array(					
					'keterangan'=>$keterangan,
					'edited_by'=>$this->session->userdata('user_id'),
					'edited_nama'=>$this->session->userdata('user_name'),
					'edited_date'=>date('Y-m-d H:i:s'),
				);
				$this->db->where('id',$tedit);
			$result=$this->db->update('tpengelolaan_permintaan',$data_det);
		}
		$this->output->set_output(json_encode($result));
	}
	//Piutang
	function load_permintaan()
    {
	 $tpengelolaan_id=$this->input->post('tpengelolaan_id');
	 $disabel=$this->input->post('disabel');
	$this->select = array();
	$this->from   = "(SELECT * FROM tpengelolaan_permintaan H
					WHERE H.tpengelolaan_id='$tpengelolaan_id')as tbl";
	$this->join 	= array();
	$this->where  = array();
	$this->order  = array();
	$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();
			$aksi = '<div class="btn-group">';					
			$aksi .= '<button data-toggle="tooltip" title="Edit" class="btn btn-success btn-xs edit_permintaan" '.$disabel.'><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-xs hapus_permintaan" '.$disabel.'><i class="fa fa-trash"></i></button>';
			$aksi .= '</div>';

          $row[] = $r->id;
          $row[] = $no;
          $row[] = $r->nama_transaksi;
          $row[] = number_format($r->nominal,0);
          $row[] = $r->dari_nama;
          $row[] = $r->keterangan;
          $row[] = $aksi;
		  $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function hapus_permintaan(){
		// data: {idsetting: idsetting,idrekap:idrekap},
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$result=$this->db->delete('tpengelolaan_permintaan');
		$this->output->set_output(json_encode($result));
	}
	public function load_user_approval()
    {
        $id = $this->input->post('id');
        $where = '';
        $from = "(
          SELECT S.id,S.step,S.proses_setuju,S.proses_tolak,U.`name` as user_nama
          FROM tpengelolaan H          
          LEFT JOIN mdata_pengelolaan_approval S ON S.idpengelolaan=H.idpengelolaan
          LEFT JOIN musers U ON U.id=S.iduser
          WHERE H.id='$id'  AND calculate_logic(S.operand,H.nominal,S.nominal) = 1
          ORDER BY S.step ASC,S.id ASC
				) as tbl";

        $this->select = array();
        $this->from   = $from;
        $this->join 	= array();
        $this->where  = array();
        $this->order  = array();
        $this->group  = array();

        $this->column_search   = array('user_nama');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $no_step='';
        foreach ($list as $r) {
            $no++;
            $row = array();

            if ($no_step != $r->step) {
                $no_step=$r->step;
            }

            if ($no_step % 2) {
                $row[] = '<span class="label label-default">STEP '.$r->step.'</span>';
            } else {
                $row[] = '<span class="label label-primary">STEP '.$r->step.'</span>';
            }

            $row[] = $r->user_nama;
            $row[] = ($r->proses_setuju=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');
            $row[] = ($r->proses_tolak=='1'?'<span class="label label-success">LANGSUNG</span>':'<span class="label label-danger">MENUNGGU USER LAIN</span>');

            $data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
      );
        echo json_encode($output);
    }
	function simpan_proses_peretujuan($id){
	
		$result=$this->Tpengelolaan_model->simpan_proses_peretujuan($id);
		echo json_encode($result);
	}
	
	public function save_terima()
    {
		$id=$this->Tpengelolaan_model->save_terima();
		if($id){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tpengelolaan','location');
		}
	}
	public function save_bayar()
    {
		$id=$this->Tpengelolaan_model->save_bayar();
		if($id){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('tpengelolaan','location');
		}
	}
}
