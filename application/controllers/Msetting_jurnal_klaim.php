<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_klaim extends CI_Controller {

	/**
	 * Setting Jurnal Hutang controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_klaim_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_klaim_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_klaim_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Hutang';
		$data['content'] 		= 'Msetting_jurnal_klaim/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Hutang",'#'),
									    			array("List",'mlogic_kasbon')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function list_kategori($idrekanan){
		// $idrekanan=$this->input->post('idrekanan');
		$q="SELECT * FROM mdata_kategori M
			WHERE M.`status`='1' AND M.idrekanan='$idrekanan'
			ORDER BY M.nama ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}	
	function list_akun(){
		$q="SELECT * FROM makun_nomor M
			WHERE M.`status`='1' 
			ORDER BY M.noakun ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->noakun.' - '.$r->namaakun.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function list_rekanan($tipe){
			$opsi='';
		if ($tipe=='1'){
			$q="SELECT * FROM mrekanan M WHERE M.`status`='1' ORDER BY M.nama ASC";		
			$row=$this->db->query($q)->result();
			foreach($row as $r){
				$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
			}
		}else{
			// $opsi .='<option value="#">- All Rekanan -</option>';
		}
		
		
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	public function list_kelompok()
    {
       $q="SELECT * FROM mpasien_kelompok M
			WHERE M.`status`='1' AND M.id !='5' ORDER BY M.id ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
    }
	function update(){
		$id_edit=1;
		$data=array(
			'st_auto_posting'=>$this->input->post('st_auto_posting'),
			'idakun_tunai'=>$this->input->post('idakun_tunai'),
			'batas_batal'=>$this->input->post('batas_batal'),
		);
		// print_r($data);exit();
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_klaim',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	//Hutang
	function load_piutang()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.id,H.idkelompokpasien,H.idrekanan,K.nama as kelompokpasien,R.nama as rekanan 
				,A.noakun,A.namaakun,CONCAT(A.noakun,' - ',A.namaakun) as akun,H.idakun
				FROM `msetting_jurnal_klaim_piutang` H
				LEFT JOIN mpasien_kelompok K ON K.id=H.idkelompokpasien
				LEFT JOIN mrekanan R ON R.id=H.idrekanan
				LEFT JOIN makun_nomor A ON A.id=H.idakun
				ORDER BY H.idkelompokpasien,H.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','rekanan','kelompokpasien');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = $r->kelompokpasien;
            $row[] = ($r->idrekanan=='0'?text_default('All Rekanan'):$r->rekanan);
            $row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_piutang('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_piutang(){
		$id_edit=$this->input->post('id_edit');
		$idrekanan=($this->input->post('idrekanan')=='#'?0:$this->input->post('idrekanan'));
		$data=array(
			'setting_id'=>1,
			'idrekanan'=>$idrekanan,
			'idkelompokpasien'=>$this->input->post('idkelompokpasien'),
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_piutang($data['idkelompokpasien'],$idrekanan)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_klaim_piutang',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_piutang($idkelompokpasien,$idrekanan){
		
		$gabung=$idkelompokpasien.'-'.$idrekanan;
		$q="SELECT *FROM msetting_jurnal_klaim_piutang S
			WHERE CONCAT(S.idkelompokpasien,'-',S.idrekanan)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_piutang($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_klaim_piutang');
		echo json_encode($result);
	}
	
	//PPN
	function load_loss()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(				
				SELECT H.id,H.idkelompokpasien,H.idrekanan,K.nama as kelompokpasien,R.nama as rekanan 
				,A.noakun,A.namaakun,CONCAT(A.noakun,' - ',A.namaakun) as akun,H.idakun
				FROM `msetting_jurnal_klaim_loss` H
				LEFT JOIN mpasien_kelompok K ON K.id=H.idkelompokpasien
				LEFT JOIN mrekanan R ON R.id=H.idrekanan
				LEFT JOIN makun_nomor A ON A.id=H.idakun
				ORDER BY H.idkelompokpasien,H.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','rekanan','kelompokpasien');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = $r->kelompokpasien;
            $row[] = ($r->idrekanan=='0'?text_default('All Rekanan'):$r->rekanan);
            $row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_loss('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_loss(){
		$id_edit=$this->input->post('id_edit');
		$idrekanan=($this->input->post('idrekanan')=='#'?0:$this->input->post('idrekanan'));
		$data=array(
			'setting_id'=>1,
			'idrekanan'=>$idrekanan,
			'idkelompokpasien'=>$this->input->post('idkelompokpasien'),
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_loss($data['idkelompokpasien'],$idrekanan)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_klaim_loss',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_loss($idkelompokpasien,$idrekanan){
		$gabung=$idkelompokpasien.'-'.$idrekanan;
		$q="SELECT *FROM msetting_jurnal_klaim_loss S
			WHERE CONCAT(S.idkelompokpasien,'-',S.idrekanan)='$gabung'";
		// print_r($q);
		return $this->db->query($q)->row('id');
	}	
	function hapus_loss($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_klaim_loss');
		echo json_encode($result);
	}
	
	//DISKON
	
	function load_income()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(				
				SELECT H.id,H.idkelompokpasien,H.idrekanan,K.nama as kelompokpasien,R.nama as rekanan 
				,A.noakun,A.namaakun,CONCAT(A.noakun,' - ',A.namaakun) as akun,H.idakun
				FROM `msetting_jurnal_klaim_income` H
				LEFT JOIN mpasien_kelompok K ON K.id=H.idkelompokpasien
				LEFT JOIN mrekanan R ON R.id=H.idrekanan
				LEFT JOIN makun_nomor A ON A.id=H.idakun
				ORDER BY H.idkelompokpasien,H.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','rekanan','kelompokpasien');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = $r->kelompokpasien;
            $row[] = ($r->idrekanan=='0'?text_default('All Rekanan'):$r->rekanan);
            $row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_income('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_income(){
		$id_edit=$this->input->post('id_edit');
		$idrekanan=($this->input->post('idrekanan')=='#'?0:$this->input->post('idrekanan'));
		$data=array(
			'setting_id'=>1,
			'idrekanan'=>$idrekanan,
			'idkelompokpasien'=>$this->input->post('idkelompokpasien'),
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_income($data['idkelompokpasien'],$idrekanan)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_klaim_income',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_income($idkelompokpasien,$idrekanan){
		$gabung=$idkelompokpasien.'-'.$idrekanan;
		$q="SELECT *FROM msetting_jurnal_klaim_income S
				WHERE CONCAT(S.idkelompokpasien,'-',S.idrekanan)='$gabung'";
		
		return $this->db->query($q)->row('id');
	}	
	function hapus_income($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_klaim_income');
		echo json_encode($result);
	}
	
	//KAS
	function load_kas()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.id,H.sumber_kas_id,M.nama as nama_kas,H.idakun,CONCAT(A.noakun,' - ',A.namaakun) as akun FROM `msetting_jurnal_klaim_kas` H
				LEFT JOIN msumber_kas M ON M.id=H.sumber_kas_id
				LEFT JOIN makun_nomor A ON A.id=H.idakun

				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','nama_kas');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = ($r->sumber_kas_id=='0'?text_default('All Sumber Kas'):$r->nama_kas);
            $row[] = $r->akun.' ('.$r->idakun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_kas('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_kas(){
		$id_edit=$this->input->post('id_edit');
		$data=array(
			'setting_id'=>1,
			'sumber_kas_id'=>$this->input->post('sumber_kas_id'),
			'idakun'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_kas($data['sumber_kas_id'])){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_klaim_kas',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	function cek_duplicate_kas($sumber_kas_id){
		$q="SELECT *FROM msetting_jurnal_klaim_kas S
			WHERE S.sumber_kas_id='$sumber_kas_id'";
		
		return $this->db->query($q)->row('id');
	}	
	function hapus_kas($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_klaim_kas');
		echo json_encode($result);
	}
}
