<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_ranap_kunjungan extends CI_Controller {

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Setting_ranap_kunjungan_model');
		$this->load->model('Antrian_poli_kode_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='1'){		
			
			if (UserAccesForm($user_acces_form,array('1919'))){
				$tab='1';
			}
			if (UserAccesForm($user_acces_form,array('1920'))){
				$tab='2';
			}
			if (UserAccesForm($user_acces_form,array('1921'))){
				$tab='3';
			}
			if (UserAccesForm($user_acces_form,array('1922'))){
				$tab='4';
			}
				// print_r(UserAccesForm($user_acces_form,array('1923')));exit;
			if (UserAccesForm($user_acces_form,array('1923'))){
				$tab='5';
			}
			if (UserAccesForm($user_acces_form,array('1932'))){
				$tab='6';
			}
			
		}
		// print_r($tab);exit;
		if (UserAccesForm($user_acces_form,array('1919','1920','1921','1922','1923','1932'))){
			$q="SELECT st_tipe_pasien_edit,st_play FROM setting_ranap";
			$data=$this->db->query($q)->row_array();
			if ($tab=='4'){
				$q="SELECT * FROM setting_ranap_lembar_masuk";
				$data_lembar=$this->db->query($q)->row_array();
				$data = array_merge($data,$data_lembar);
			}
			if ($tab=='5'){
				$q="SELECT * FROM setting_ranap_pernyataan";
				$data_lembar=$this->db->query($q)->row_array();
				$data = array_merge($data,$data_lembar);
			}
			$data['list_sound'] 			= $this->Antrian_poli_kode_model->list_sound();
			$data['tab'] 			= $tab;
			
			$data['jenis_isi'] 			= '0';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Rawat Inap';
			$data['content'] 		= 'Setting_ranap_kunjungan/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Pengaturan Rawat Inap",'#'),
												  array("Setting Akses Setting",'setting_ranap_kunjungan/index')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	
	function get_poli(){
		$asal_pasien=$this->input->post('asal_pasien');
		// print_r($asal_pasien);exit;
		$where='';
		 if ($asal_pasien!='0'){
			$where .="WHERE idtipe='$asal_pasien'";
		}
		$q="
				SELECT *FROM mpoliklinik ".$where."
			";
		$hasil=$this->db->query($q)->result();
		$opsi='<option value="0" selected>SEMUA</option>';
		foreach($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.($r->nama).'</option>';
		}
		$this->output->set_output(json_encode($opsi));
	}
	
	function simpan_kunjungan(){
		$user_id=$this->session->userdata('user_id');
		$asal_pasien = $this->input->post('asal_pasien');
		$idpoli = $this->input->post('idpoli');
		$iddokter = $this->input->post('iddokter');
		$kunjungan_id = $this->input->post('kunjungan_id');
		
		$q="SELECT * FROM setting_ranap_kunjungan 
		WHERE 
		asal_pasien='".$asal_pasien."' AND 
		idpoli='".$idpoli."' AND 
		iddokter='".$iddokter."' AND id !='$kunjungan_id'";
		$row=$this->db->query($q)->row();
		if ($row){
			$hasil=false;
		}else{
			$data=array(
				'asal_pasien' => $asal_pasien,
				'idpoli' => $idpoli,
				'iddokter' => $iddokter,
				'created_by' => $user_id,
				'created_date' => date('Y-m-d H:i:s'),
			);
			if ($kunjungan_id==''){
				$hasil=$this->db->insert('setting_ranap_kunjungan',$data);
				
			}else{
				$this->db->where('id',$kunjungan_id);
				$hasil=$this->db->update('setting_ranap_kunjungan',$data);
			}
		}
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_general(){
		$st_tipe_pasien_edit = $this->input->post('st_tipe_pasien_edit');
		$st_play = $this->input->post('st_play');
		
			$data=array(
				'st_tipe_pasien_edit' => $st_tipe_pasien_edit,
				'st_play' => $st_play,
				
			);
			$this->db->where('id',1);
			$hasil=$this->db->update('setting_ranap',$data);
			$this->output->set_output(json_encode($hasil));
	}
	function save_label_lembar(){
		// print_r($this->input->post());exit;
			$data=array(
				'alamat_form' => $this->input->post('alamat_form'),
				'telepone_form' => $this->input->post('telepone_form'),
				'email_form' => $this->input->post('email_form'),
				'judul_ina' => $this->input->post('judul_ina'),
				'judul_eng' => $this->input->post('judul_eng'),
				'header_ina' => $this->input->post('header_ina'),
				'header_eng' => $this->input->post('header_eng'),
				'no_register_ina' => $this->input->post('no_register_ina'),
				'no_register_eng' => $this->input->post('no_register_eng'),
				'no_rekam_medis_ina' => $this->input->post('no_rekam_medis_ina'),
				'no_rekam_medis_eng' => $this->input->post('no_rekam_medis_eng'),
				'nama_pasien_ina' => $this->input->post('nama_pasien_ina'),
				'nama_pasien_eng' => $this->input->post('nama_pasien_eng'),
				'umur_ina' => $this->input->post('umur_ina'),
				'umur_eng' => $this->input->post('umur_eng'),
				'ttl_ina' => $this->input->post('ttl_ina'),
				'ttl_eng' => $this->input->post('ttl_eng'),
				'jk_ina' => $this->input->post('jk_ina'),
				'jk_eng' => $this->input->post('jk_eng'),
				'pekerjaan_ina' => $this->input->post('pekerjaan_ina'),
				'pekerjaan_eng' => $this->input->post('pekerjaan_eng'),
				'pendidikan_ina' => $this->input->post('pendidikan_ina'),
				'pendidikan_eng' => $this->input->post('pendidikan_eng'),
				'agama_ina' => $this->input->post('agama_ina'),
				'agama_eng' => $this->input->post('agama_eng'),
				'alamat_ina' => $this->input->post('alamat_ina'),
				'alamat_eng' => $this->input->post('alamat_eng'),
				'provinsi_ina' => $this->input->post('provinsi_ina'),
				'provinsi_eng' => $this->input->post('provinsi_eng'),
				'kab_ina' => $this->input->post('kab_ina'),
				'kab_eng' => $this->input->post('kab_eng'),
				'kec_ina' => $this->input->post('kec_ina'),
				'kec_eng' => $this->input->post('kec_eng'),
				'kel_ina' => $this->input->post('kel_ina'),
				'kel_eng' => $this->input->post('kel_eng'),
				'kode_pos_ina' => $this->input->post('kode_pos_ina'),
				'kode_pos_eng' => $this->input->post('kode_pos_eng'),
				'rt_ina' => $this->input->post('rt_ina'),
				'rt_eng' => $this->input->post('rt_eng'),
				'rw_ina' => $this->input->post('rw_ina'),
				'rw_eng' => $this->input->post('rw_eng'),
				'wn_ina' => $this->input->post('wn_ina'),
				'wn_eng' => $this->input->post('wn_eng'),
				'telephone_ina' => $this->input->post('telephone_ina'),
				'telephone_eng' => $this->input->post('telephone_eng'),
				'nohp_ina' => $this->input->post('nohp_ina'),
				'nohp_eng' => $this->input->post('nohp_eng'),
				'nik_ina' => $this->input->post('nik_ina'),
				'nik_eng' => $this->input->post('nik_eng'),
				'label_header_pj_ina' => $this->input->post('label_header_pj_ina'),
				'label_header_pj_eng' => $this->input->post('label_header_pj_eng'),
				'nama_pj_ina' => $this->input->post('nama_pj_ina'),
				'nama_pj_eng' => $this->input->post('nama_pj_eng'),
				'ttl_pj_ina' => $this->input->post('ttl_pj_ina'),
				'ttl_pj_eng' => $this->input->post('ttl_pj_eng'),
				'umur_pj_ina' => $this->input->post('umur_pj_ina'),
				'umur_pj_eng' => $this->input->post('umur_pj_eng'),
				'jk_pj_ina' => $this->input->post('jk_pj_ina'),
				'jk_pj_eng' => $this->input->post('jk_pj_eng'),
				'pekerjaan_pj_ina' => $this->input->post('pekerjaan_pj_ina'),
				'pekerjaan_pj_eng' => $this->input->post('pekerjaan_pj_eng'),
				'pendidikan_pj_ina' => $this->input->post('pendidikan_pj_ina'),
				'pendidikan_pj_eng' => $this->input->post('pendidikan_pj_eng'),
				'agama_pj_ina' => $this->input->post('agama_pj_ina'),
				'agama_pj_eng' => $this->input->post('agama_pj_eng'),
				'alamat_pj_ina' => $this->input->post('alamat_pj_ina'),
				'alamat_pj_eng' => $this->input->post('alamat_pj_eng'),
				'provinsi_pj_ina' => $this->input->post('provinsi_pj_ina'),
				'provinsi_pj_eng' => $this->input->post('provinsi_pj_eng'),
				'kab_pj_ina' => $this->input->post('kab_pj_ina'),
				'kab_pj_eng' => $this->input->post('kab_pj_eng'),
				'kec_pj_ina' => $this->input->post('kec_pj_ina'),
				'kec_pj_eng' => $this->input->post('kec_pj_eng'),
				'kel_pj_ina' => $this->input->post('kel_pj_ina'),
				'kel_pj_eng' => $this->input->post('kel_pj_eng'),
				'kode_pos_pj_ina' => $this->input->post('kode_pos_pj_ina'),
				'kode_pos_pj_eng' => $this->input->post('kode_pos_pj_eng'),
				'rt_pj_ina' => $this->input->post('rt_pj_ina'),
				'rt_pj_eng' => $this->input->post('rt_pj_eng'),
				'rw_pj_ina' => $this->input->post('rw_pj_ina'),
				'rw_pj_eng' => $this->input->post('rw_pj_eng'),
				'wn_pj_ina' => $this->input->post('wn_pj_ina'),
				'wn_pj_eng' => $this->input->post('wn_pj_eng'),
				'telephone_pj_ina' => $this->input->post('telephone_pj_ina'),
				'telephone_pj_eng' => $this->input->post('telephone_pj_eng'),
				'nohp_pj_ina' => $this->input->post('nohp_pj_ina'),
				'nohp_pj_eng' => $this->input->post('nohp_pj_eng'),
				'nik_pj_ina' => $this->input->post('nik_pj_ina'),
				'nik_pj_eng' => $this->input->post('nik_pj_eng'),
				'hubungan_pj_ina' => $this->input->post('hubungan_pj_ina'),
				'hubungan_pj_eng' => $this->input->post('hubungan_pj_eng'),
				'detail_pendaftaran_ina' => $this->input->post('detail_pendaftaran_ina'),
				'detail_pendaftaran_eng' => $this->input->post('detail_pendaftaran_eng'),
				'tipe_pasien_ina' => $this->input->post('tipe_pasien_ina'),
				'tipe_pasien_eng' => $this->input->post('tipe_pasien_eng'),
				'asal_pasien_ina' => $this->input->post('asal_pasien_ina'),
				'asal_pasien_eng' => $this->input->post('asal_pasien_eng'),
				'poliklinik_ina' => $this->input->post('poliklinik_ina'),
				'poliklinik_eng' => $this->input->post('poliklinik_eng'),
				'tgl_pendaftaran_ina' => $this->input->post('tgl_pendaftaran_ina'),
				'tgl_pendaftaran_eng' => $this->input->post('tgl_pendaftaran_eng'),
				'kasus_ina' => $this->input->post('kasus_ina'),
				'kasus_eng' => $this->input->post('kasus_eng'),
				'dirawat_ke_ina' => $this->input->post('dirawat_ke_ina'),
				'dirawat_ke_eng' => $this->input->post('dirawat_ke_eng'),
				'cara_masuk_ina' => $this->input->post('cara_masuk_ina'),
				'cara_masuk_eng' => $this->input->post('cara_masuk_eng'),
				'kel_pasien_ina' => $this->input->post('kel_pasien_ina'),
				'kel_pasien_eng' => $this->input->post('kel_pasien_eng'),
				'dpjp_utama_ina' => $this->input->post('dpjp_utama_ina'),
				'dpjp_utama_eng' => $this->input->post('dpjp_utama_eng'),
				'ruangan_ina' => $this->input->post('ruangan_ina'),
				'ruangan_eng' => $this->input->post('ruangan_eng'),
				'kelas_ina' => $this->input->post('kelas_ina'),
				'kelas_eng' => $this->input->post('kelas_eng'),
				'bed_ina' => $this->input->post('bed_ina'),
				'bed_eng' => $this->input->post('bed_eng'),
				'pernyataan_persetujuan_ina' => $this->input->post('pernyataan_persetujuan_ina'),
				'pernyataan_persetujuan_eng' => $this->input->post('pernyataan_persetujuan_eng'),
				'identifikasi_privasi_ina' => $this->input->post('identifikasi_privasi_ina'),
				'identifikasi_privasi_eng' => $this->input->post('identifikasi_privasi_eng'),
				'khusus_asuransi_ina' => $this->input->post('khusus_asuransi_ina'),
				'khusus_asuransi_eng' => $this->input->post('khusus_asuransi_eng'),
				'lain_lain_ina' => $this->input->post('lain_lain_ina'),
				'lain_lain_eng' => $this->input->post('lain_lain_eng'),
				'ttd_pendaftaran_ina' => $this->input->post('ttd_pendaftaran_ina'),
				'ttd_pendaftaran_eng' => $this->input->post('ttd_pendaftaran_eng'),
				'ttd_pasien_ina' => $this->input->post('ttd_pasien_ina'),
				'ttd_pasien_eng' => $this->input->post('ttd_pasien_eng'),

			);
			if (isset($_FILES['logo_form'])) {
				// print_r('sini');exit;
				$config['upload_path'] = './assets/upload/logo_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('logo_form')) {
                    $image_upload = $this->upload->data();
                    $data['logo_form'] = $image_upload['file_name'];
					
                } 
			}
			$hasil=$this->db->update('setting_ranap_lembar_masuk',$data);
			if ($hasil){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data tandatangan telah disimpan.');
				redirect('setting_ranap_kunjungan/index/4','location');
			}
	}
	function save_pernyataan(){
		// print_r($this->input->post());exit;
			$data=array(
				'alamat_form' => $this->input->post('alamat_form'),
				'telepone_form' => $this->input->post('telepone_form'),
				'email_form' => $this->input->post('email_form'),
				'judul_ina' => $this->input->post('judul_ina'),
				'judul_eng' => $this->input->post('judul_eng'),
				'header_ina' => $this->input->post('header_ina'),
				'header_eng' => $this->input->post('header_eng'),
				'no_register_ina' => $this->input->post('no_register_ina'),
				'no_register_eng' => $this->input->post('no_register_eng'),
				'no_rekam_medis_ina' => $this->input->post('no_rekam_medis_ina'),
				'no_rekam_medis_eng' => $this->input->post('no_rekam_medis_eng'),
				'nama_pasien_ina' => $this->input->post('nama_pasien_ina'),
				'nama_pasien_eng' => $this->input->post('nama_pasien_eng'),
				'umur_ina' => $this->input->post('umur_ina'),
				'umur_eng' => $this->input->post('umur_eng'),
				'ttl_ina' => $this->input->post('ttl_ina'),
				'ttl_eng' => $this->input->post('ttl_eng'),
				'jk_ina' => $this->input->post('jk_ina'),
				'jk_eng' => $this->input->post('jk_eng'),
				'pekerjaan_ina' => $this->input->post('pekerjaan_ina'),
				'pekerjaan_eng' => $this->input->post('pekerjaan_eng'),
				'pendidikan_ina' => $this->input->post('pendidikan_ina'),
				'pendidikan_eng' => $this->input->post('pendidikan_eng'),
				'agama_ina' => $this->input->post('agama_ina'),
				'agama_eng' => $this->input->post('agama_eng'),
				'alamat_ina' => $this->input->post('alamat_ina'),
				'alamat_eng' => $this->input->post('alamat_eng'),
				'provinsi_ina' => $this->input->post('provinsi_ina'),
				'provinsi_eng' => $this->input->post('provinsi_eng'),
				'kab_ina' => $this->input->post('kab_ina'),
				'kab_eng' => $this->input->post('kab_eng'),
				'kec_ina' => $this->input->post('kec_ina'),
				'kec_eng' => $this->input->post('kec_eng'),
				'kel_ina' => $this->input->post('kel_ina'),
				'kel_eng' => $this->input->post('kel_eng'),
				'kode_pos_ina' => $this->input->post('kode_pos_ina'),
				'kode_pos_eng' => $this->input->post('kode_pos_eng'),
				'rt_ina' => $this->input->post('rt_ina'),
				'rt_eng' => $this->input->post('rt_eng'),
				'rw_ina' => $this->input->post('rw_ina'),
				'rw_eng' => $this->input->post('rw_eng'),
				'wn_ina' => $this->input->post('wn_ina'),
				'wn_eng' => $this->input->post('wn_eng'),
				'telephone_ina' => $this->input->post('telephone_ina'),
				'telephone_eng' => $this->input->post('telephone_eng'),
				'nohp_ina' => $this->input->post('nohp_ina'),
				'nohp_eng' => $this->input->post('nohp_eng'),
				'nik_ina' => $this->input->post('nik_ina'),
				'nik_eng' => $this->input->post('nik_eng'),
				'label_header_pj_ina' => $this->input->post('label_header_pj_ina'),
				'label_header_pj_eng' => $this->input->post('label_header_pj_eng'),
				'nama_pj_ina' => $this->input->post('nama_pj_ina'),
				'nama_pj_eng' => $this->input->post('nama_pj_eng'),
				'ttl_pj_ina' => $this->input->post('ttl_pj_ina'),
				'ttl_pj_eng' => $this->input->post('ttl_pj_eng'),
				'umur_pj_ina' => $this->input->post('umur_pj_ina'),
				'umur_pj_eng' => $this->input->post('umur_pj_eng'),
				'jk_pj_ina' => $this->input->post('jk_pj_ina'),
				'jk_pj_eng' => $this->input->post('jk_pj_eng'),
				'pekerjaan_pj_ina' => $this->input->post('pekerjaan_pj_ina'),
				'pekerjaan_pj_eng' => $this->input->post('pekerjaan_pj_eng'),
				'pendidikan_pj_ina' => $this->input->post('pendidikan_pj_ina'),
				'pendidikan_pj_eng' => $this->input->post('pendidikan_pj_eng'),
				'agama_pj_ina' => $this->input->post('agama_pj_ina'),
				'agama_pj_eng' => $this->input->post('agama_pj_eng'),
				'alamat_pj_ina' => $this->input->post('alamat_pj_ina'),
				'alamat_pj_eng' => $this->input->post('alamat_pj_eng'),
				'provinsi_pj_ina' => $this->input->post('provinsi_pj_ina'),
				'provinsi_pj_eng' => $this->input->post('provinsi_pj_eng'),
				'kab_pj_ina' => $this->input->post('kab_pj_ina'),
				'kab_pj_eng' => $this->input->post('kab_pj_eng'),
				'kec_pj_ina' => $this->input->post('kec_pj_ina'),
				'kec_pj_eng' => $this->input->post('kec_pj_eng'),
				'kel_pj_ina' => $this->input->post('kel_pj_ina'),
				'kel_pj_eng' => $this->input->post('kel_pj_eng'),
				'kode_pos_pj_ina' => $this->input->post('kode_pos_pj_ina'),
				'kode_pos_pj_eng' => $this->input->post('kode_pos_pj_eng'),
				'rt_pj_ina' => $this->input->post('rt_pj_ina'),
				'rt_pj_eng' => $this->input->post('rt_pj_eng'),
				'rw_pj_ina' => $this->input->post('rw_pj_ina'),
				'rw_pj_eng' => $this->input->post('rw_pj_eng'),
				'wn_pj_ina' => $this->input->post('wn_pj_ina'),
				'wn_pj_eng' => $this->input->post('wn_pj_eng'),
				'telephone_pj_ina' => $this->input->post('telephone_pj_ina'),
				'telephone_pj_eng' => $this->input->post('telephone_pj_eng'),
				'nohp_pj_ina' => $this->input->post('nohp_pj_ina'),
				'nohp_pj_eng' => $this->input->post('nohp_pj_eng'),
				'nik_pj_ina' => $this->input->post('nik_pj_ina'),
				'nik_pj_eng' => $this->input->post('nik_pj_eng'),
				'hubungan_pj_ina' => $this->input->post('hubungan_pj_ina'),
				'hubungan_pj_eng' => $this->input->post('hubungan_pj_eng'),
				'detail_pendaftaran_ina' => $this->input->post('detail_pendaftaran_ina'),
				'detail_pendaftaran_eng' => $this->input->post('detail_pendaftaran_eng'),
				'tipe_pasien_ina' => $this->input->post('tipe_pasien_ina'),
				'tipe_pasien_eng' => $this->input->post('tipe_pasien_eng'),
				// 'asal_pasien_ina' => $this->input->post('asal_pasien_ina'),
				// 'asal_pasien_eng' => $this->input->post('asal_pasien_eng'),
				// 'poliklinik_ina' => $this->input->post('poliklinik_ina'),
				// 'poliklinik_eng' => $this->input->post('poliklinik_eng'),
				// 'tgl_pendaftaran_ina' => $this->input->post('tgl_pendaftaran_ina'),
				// 'tgl_pendaftaran_eng' => $this->input->post('tgl_pendaftaran_eng'),
				// 'kasus_ina' => $this->input->post('kasus_ina'),
				// 'kasus_eng' => $this->input->post('kasus_eng'),
				// 'dirawat_ke_ina' => $this->input->post('dirawat_ke_ina'),
				// 'dirawat_ke_eng' => $this->input->post('dirawat_ke_eng'),
				// 'cara_masuk_ina' => $this->input->post('cara_masuk_ina'),
				// 'cara_masuk_eng' => $this->input->post('cara_masuk_eng'),
				'kel_pasien_ina' => $this->input->post('kel_pasien_ina'),
				'kel_pasien_eng' => $this->input->post('kel_pasien_eng'),
				'dpjp_utama_ina' => $this->input->post('dpjp_utama_ina'),
				'dpjp_utama_eng' => $this->input->post('dpjp_utama_eng'),
				'ruangan_ina' => $this->input->post('ruangan_ina'),
				'ruangan_eng' => $this->input->post('ruangan_eng'),
				'kelas_ina' => $this->input->post('kelas_ina'),
				'kelas_eng' => $this->input->post('kelas_eng'),
				'bed_ina' => $this->input->post('bed_ina'),
				'bed_eng' => $this->input->post('bed_eng'),
				// 'pernyataan_persetujuan_ina' => $this->input->post('pernyataan_persetujuan_ina'),
				// 'pernyataan_persetujuan_eng' => $this->input->post('pernyataan_persetujuan_eng'),
				// 'identifikasi_privasi_ina' => $this->input->post('identifikasi_privasi_ina'),
				// 'identifikasi_privasi_eng' => $this->input->post('identifikasi_privasi_eng'),
				// 'khusus_asuransi_ina' => $this->input->post('khusus_asuransi_ina'),
				// 'khusus_asuransi_eng' => $this->input->post('khusus_asuransi_eng'),
				// 'lain_lain_ina' => $this->input->post('lain_lain_ina'),
				// 'lain_lain_eng' => $this->input->post('lain_lain_eng'),
				// 'ttd_pendaftaran_ina' => $this->input->post('ttd_pendaftaran_ina'),
				// 'ttd_pendaftaran_eng' => $this->input->post('ttd_pendaftaran_eng'),
				'ttd_pasien_ina' => $this->input->post('ttd_pasien_ina'),
				'ttd_pasien_eng' => $this->input->post('ttd_pasien_eng'),
				'paragraf_1_ina' => $this->input->post('paragraf_1_ina'),
				'paragraf_1_eng' => $this->input->post('paragraf_1_eng'),
				'paragraf_2_ina' => $this->input->post('paragraf_2_ina'),
				'paragraf_2_eng' => $this->input->post('paragraf_2_eng'),
				'paragraf_3_ina' => $this->input->post('paragraf_3_ina'),
				'paragraf_3_eng' => $this->input->post('paragraf_3_eng'),

			);
			if (isset($_FILES['logo_form'])) {
				// print_r('sini');exit;
				$config['upload_path'] = './assets/upload/logo_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('logo_form')) {
                    $image_upload = $this->upload->data();
                    $data['logo_form'] = $image_upload['file_name'];
					
                } 
			}
			$hasil=$this->db->update('setting_ranap_pernyataan',$data);
			if ($hasil){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data tandatangan telah disimpan.');
				redirect('setting_ranap_kunjungan/index/5','location');
			}
	}
	
	public function upload_header_logo($update = false)
    {
        if (!file_exists('assets/upload/logo_setting')) {
            mkdir('assets/upload/logo_setting', 0755, true);
        }

        if (isset($_FILES['logo_form'])) {
            if ($_FILES['logo_form']['name'] != '') {
                $config['upload_path'] = './assets/upload/logo_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('logo_form')) {
                    $image_upload = $this->upload->data();
                    $this->logo_form = $image_upload['file_name'];

                    return  $this->logo_form;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
			// print_r('TIDAK ADA');exit;
                return true;
            }
					// print_r($this->foto);exit;
        } else {
			// print_r('TIDAK ADA');exit;
            return true;
        }
		
    }
	function load_kunjungan()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*,MP.nama as nama_poli , MD.nama as nama_dokter
						FROM setting_ranap_kunjungan H
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli
						LEFT JOIN mdokter MD ON MD.id=H.iddokter
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array();
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->asal_pasien=='0'?text_default('SEMUA'):GetAsalPasien($r->asal_pasien));
          $result[] = ($r->idpoli=='0'?text_default('SEMUA'):$r->nama_poli);
          $result[] = ($r->iddokter=='0'?text_default('SEMUA'):$r->nama_dokter);
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_kunjungan('.$r->id.')" type="button" title="Edit Logic" class="btn btn-success btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_kunjungan('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
    function hapus_kunjungan(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_ranap_kunjungan');
	  
	  json_encode($hasil);
	  
  }
  function edit_kunjungan(){
	  $id=$this->input->post('id');
	   
	   $q="SELECT *FROM setting_ranap_kunjungan WHERE id='$id'";
	   $hasil=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($hasil));
	  
  }
  //TIPE
  
  function simpan_tipe(){
		$user_id=$this->session->userdata('user_id');
		$asal_pasien = $this->input->post('asal_pasien');
		$idpoli = $this->input->post('idpoli');
		$iddokter = $this->input->post('iddokter');
		$kunjungan_id = $this->input->post('kunjungan_tipe_id');
		$idkelompokpasien = $this->input->post('idkelompokpasien');
		$tipe_rekanan = $this->input->post('tipe_rekanan');
		$idrekanan = $this->input->post('idrekanan');
		$idtujuan = $this->input->post('idtujuan');
		$idtipepasien = $this->input->post('idtipepasien');
		
		$q="SELECT * FROM setting_ranap_tipe 
		WHERE 
		asal_pasien='".$asal_pasien."' AND 
		idpoli='".$idpoli."' AND 
		iddokter='".$iddokter."' AND 
		idkelompokpasien='".$idkelompokpasien."' AND 
		tipe_rekanan='".$tipe_rekanan."' AND 
		idrekanan='".$idrekanan."' AND
		idtujuan='".$idtujuan."' AND 
		idtipepasien='".$idtipepasien."' AND 
		id !='$kunjungan_id'";
		$row=$this->db->query($q)->row();
		if ($row){
			$hasil=false;
		}else{
			$data=array(
				'asal_pasien' => $asal_pasien,
				'idpoli' => $idpoli,
				'iddokter' => $iddokter,
				'idkelompokpasien' => $idkelompokpasien,
				'tipe_rekanan' => $tipe_rekanan,
				'idrekanan' => $idrekanan,
				'idtujuan' => $idtujuan,
				'idtipepasien' => $idtipepasien,
				'created_by' => $user_id,
				'created_date' => date('Y-m-d H:i:s'),
			);
			if ($kunjungan_id==''){
				$hasil=$this->db->insert('setting_ranap_tipe',$data);
				
			}else{
				$this->db->where('id',$kunjungan_id);
				$hasil=$this->db->update('setting_ranap_tipe',$data);
			}
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	
	function load_tipe()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*,MP.nama as nama_poli , MD.nama as nama_dokter,MK.nama as nama_kelompok,R.nama as nama_rekanan,TR.ref as nama_tipe_rekanan
						FROM setting_ranap_tipe H
						LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli
						LEFT JOIN mdokter MD ON MD.id=H.iddokter
						LEFT JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
						LEFT JOIN merm_referensi TR ON TR.nilai=H.tipe_rekanan AND TR.ref_head_id='117'
						LEFT JOIN mrekanan R ON R.id=H.idrekanan
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array();
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->asal_pasien=='0'?text_default('SEMUA'):GetAsalPasien($r->asal_pasien));
          $result[] = ($r->idpoli=='0'?text_default('SEMUA'):$r->nama_poli);
          $result[] = ($r->iddokter=='0'?text_default('SEMUA'):$r->nama_dokter);
          $result[] = ($r->idkelompokpasien=='0'?text_default('SEMUA'):$r->nama_kelompok);
          $result[] = ($r->tipe_rekanan=='0'?text_default('SEMUA'):$r->nama_tipe_rekanan);
          $result[] = ($r->idrekanan=='0'?text_default('SEMUA'):$r->nama_rekanan);
          $result[] = ($r->idtujuan=='0'?text_default('SEMUA'):GetTujuanBerkas($r->idtujuan));
          $result[] = ($r->idtipepasien=='0'?text_default('SEMUA'):GetTipePasienRanap($r->idtipepasien));
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_tipe('.$r->id.')" type="button" title="Edit Logic" class="btn btn-success btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_tipe('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
    function hapus_tipe(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_ranap_tipe');
	  
	  json_encode($hasil);
	  
  }
  function edit_tipe(){
	  $id=$this->input->post('id');
	   
	   $q="SELECT *FROM setting_ranap_tipe WHERE id='$id'";
	   $hasil=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($hasil));
	  
  }
  
  //FORM
  function load_terima_all(){
	  load_sound();
	  load_terima();
	  
  }
  function simpan_form(){
		$user_id=$this->session->userdata('user_id');
		$idtujuan = $this->input->post('idtujuan');
		$iddokter = $this->input->post('iddokter');
		$st_lembar_masuk = $this->input->post('st_lembar_masuk');
		$st_pernyataan = $this->input->post('st_pernyataan');
		$st_general = $this->input->post('st_general');
		$st_hak_kewajiban = $this->input->post('st_hak_kewajiban');
		// print_r($this->input->post());exit;
		$kunjungan_id = $this->input->post('kunjungan_form_id');

		
		$q="SELECT * FROM setting_ranap_form 
		WHERE 
		iddokter='".$iddokter."' AND 
		idtujuan='".$idtujuan."' AND 
		st_lembar_masuk='".$st_lembar_masuk."' AND 
		st_pernyataan='".$st_pernyataan."' AND 
		st_general='".$st_general."' AND 
		st_hak_kewajiban='".$st_hak_kewajiban."' AND
		id !='$kunjungan_id'";
		$row=$this->db->query($q)->row();
		if ($row){
			$hasil=false;
		}else{
			$data=array(
				'idtujuan' => $idtujuan,
				'iddokter' => $iddokter,
				'st_lembar_masuk' => $st_lembar_masuk,
				'st_pernyataan' => $st_pernyataan,
				'st_general' => $st_general,
				'st_hak_kewajiban' => $st_hak_kewajiban,
				'created_by' => $user_id,
				'created_date' => date('Y-m-d H:i:s'),
			);
			if ($kunjungan_id==''){
				$hasil=$this->db->insert('setting_ranap_form',$data);
				
			}else{
				$this->db->where('id',$kunjungan_id);
				$hasil=$this->db->update('setting_ranap_form',$data);
			}
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	
	function load_form()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*, MD.nama as nama_dokter
						FROM setting_ranap_form H
						LEFT JOIN mdokter MD ON MD.id=H.iddokter
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array();
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->idtujuan=='0'?text_default('SEMUA'):GetTujuanBerkas($r->idtujuan));
          $result[] = ($r->iddokter=='0'?text_default('SEMUA'):$r->nama_dokter);
          $result[] = (($r->st_lembar_masuk=='1'?'YA':'TIDAK'));
          $result[] = (($r->st_pernyataan=='1'?'YA':'TIDAK'));
          $result[] = (($r->st_general=='1'?'YA':'TIDAK'));
          $result[] = (($r->st_hak_kewajiban=='1'?'YA':'TIDAK'));
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_form('.$r->id.')" type="button" title="Edit Logic" class="btn btn-success btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_form('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
    function hapus_form(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_ranap_form');
	  
	  json_encode($hasil);
	  
  }
  function edit_form(){
	  $id=$this->input->post('id');
	   
	   $q="SELECT *FROM setting_ranap_form WHERE id='$id'";
	   $hasil=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($hasil));
	  
  }
  function get_asuransi(){
	  $tipe_rekanan_tipe=$this->input->post('tipe_rekanan_tipe');
	  if ($tipe_rekanan_tipe!='0'){
		  $q="SELECT *FROM mrekanan WHERE tipe_rekanan='$tipe_rekanan_tipe' AND status='1'";
	  }else{
		  $q="SELECT *FROM mrekanan WHERE status='1'";
	  }
	  // print_r($q);exit;
	  $list_data=$this->db->query($q)->result();
	  $opsi='';
	if ($list_data){
			$opsi .='<option value="0">SEMUA</option>';
		foreach ($list_data as $r){
			$opsi .='<option value='.$r->id.'>'.$r->nama.'</option>';
		}
	}
	$data['detail']=$opsi;
	$this->output->set_output(json_encode($data));
  }
  //TERIMA
  
  function simpan_terima(){
		$user_id=$this->session->userdata('user_id');
		$idtujuan = $this->input->post('idtujuan');
		$idruangan = $this->input->post('idruangan');
		$idkelas = $this->input->post('idkelas');
		$st_terima_ranap = $this->input->post('st_terima_ranap');
		$idtrx = $this->input->post('idtrx');
		
		$q="SELECT * FROM setting_ranap_terima 
		WHERE 
		idtujuan='".$idtujuan."' AND 
		idruangan='".$idruangan."' AND 
		idkelas='".$idkelas."' AND 
		id !='$idtrx'";
		$row=$this->db->query($q)->row();
		if ($row){
			$hasil=false;
		}else{
			$data=array(
				'idtujuan' => $idtujuan,
				'idruangan' => $idruangan,
				'idkelas' => $idkelas,
				'st_terima_ranap' => $st_terima_ranap,
				'created_by' => $user_id,
				'created_date' => date('Y-m-d H:i:s'),
			);
			if ($idtrx==''){
				$hasil=$this->db->insert('setting_ranap_terima',$data);
				
			}else{
				$this->db->where('id',$idtrx);
				$hasil=$this->db->update('setting_ranap_terima',$data);
			}
		}
		$this->output->set_output(json_encode($hasil));
	}
	
	
	function load_terima()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*, MR.nama as nama_ruangan,MK.nama as nama_kelas
						FROM setting_ranap_terima H
						LEFT JOIN mruangan MR ON MR.id=H.idruangan
						LEFT JOIN mkelas MK ON MK.id=H.idkelas
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array();
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->idtujuan=='0'?text_default('SEMUA'):GetTujuanBerkas($r->idtujuan));
          $result[] = ($r->idruangan=='0'?text_default('SEMUA'):$r->nama_ruangan);
          $result[] = ($r->idkelas=='0'?text_default('SEMUA'):$r->nama_kelas);
          $result[] = (($r->st_terima_ranap=='1'?'Otomatis Terima':'Manual'));
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_terima('.$r->id.')" type="button" title="Edit Logic" class="btn btn-success btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_terima('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
    function hapus_terima(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_ranap_terima');
	  
	  json_encode($hasil);
	  
  }
  function edit_terima(){
	  $id=$this->input->post('id');
	   
	   $q="SELECT *FROM setting_ranap_terima WHERE id='$id'";
	   $hasil=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($hasil));
	  
  }
  //content
  function load_content(){
		$tabel='';
		$q="SELECT *FROM (
				SELECT 
				LPAD(H.`no`,3,'0') as  nourut
				,H.*,C.`name` as user_created,E.`name` as user_edited,GROUP_CONCAT(R.ref SEPARATOR ' / ') as ref FROM setting_ranap_lembar_masuk_isi H
							LEFT JOIN musers C ON C.id=H.created_by
							LEFT JOIN musers E ON E.id=H.edited_by
							LEFT JOIN setting_ranap_lembar_masuk_isi_jawaban M ON M.general_isi_id=H.id
							LEFT JOIN merm_referensi R ON R.id=M.ref_id
							WHERE H.`status`='1' AND H.header_id=0
							
							GROUP BY H.id
							
							UNION ALL
							
							SELECT 
				CONCAT(LPAD(HH.`no`,3,'0'),'-',LPAD(H.`no`,3,'0')) as  nourut
				,H.*,C.`name` as user_created,E.`name` as user_edited,GROUP_CONCAT(R.ref SEPARATOR ' / ') as ref FROM setting_ranap_lembar_masuk_isi H
							LEFT JOIN musers C ON C.id=H.created_by
							LEFT JOIN musers E ON E.id=H.edited_by
							LEFT JOIN setting_ranap_lembar_masuk_isi_jawaban M ON M.general_isi_id=H.id
							LEFT JOIN merm_referensi R ON R.id=M.ref_id
							LEFT JOIN setting_ranap_lembar_masuk_isi HH ON HH.id=H.header_id
							WHERE H.`status`='1' AND H.header_id !=0
							
							GROUP BY H.id
							
			) T ORDER BY T.nourut ASC";
		$hasil=$this->db->query($q)->result();
		$no=1;
		foreach($hasil as $r){
			$tabel .='<tr>';
			$tabel .='<td class="text-center '.($r->header_id=='0'?'bg-warning text-white':'').'">'.$r->no.'</td>';
			$tabel .='<td class="'.($r->header_id=='0'?'bg-warning text-white':'').'">'.$r->isi.'</td>';
			$tabel .='<td class="hidden-xs '.($r->header_id=='0'?'bg-warning text-white':'').'">'.($r->jenis_isi=='1'?text_danger(jenis_isi($r->jenis_isi)).'<br>'.$r->ref:text_primary(jenis_isi($r->jenis_isi))).'</td>';
			$tabel .='<td class="hidden-xs '.($r->header_id=='0'?'bg-warning text-white':'').'">'.$r->user_created.'<br>'.($r->created_date?HumanDateLong($r->created_date):'').'</td>';
			$tabel .='<td class="text-center '.($r->header_id=='0'?'bg-warning text-white':'').'">';
			$tabel .='					<div class="btn-group">';
			if ($r->header_id=='0'){
				$tabel .='						<button class="btn btn-xs btn-primary" type="button" onclick="add_detail('.$r->id.')" data-toggle="tooltip" title="Tambah" data-original-title="Edit"><i class="fa fa-plus"></i></button>';
			}
			
			$tabel .='						<button class="btn btn-xs btn-success" type="button" onclick="edit_content('.$r->id.','.$r->header_id.')" data-toggle="tooltip" title="Edit" data-original-title="Tambah"><i class="fa fa-pencil"></i></button>';
			$tabel .='						<button class="btn btn-xs btn-danger" type="button" onclick="hapus('.$r->id.')" data-toggle="tooltip" title="" data-original-title="Remove "><i class="fa fa-times"></i></button>';
			$tabel .='					</div>';
			$tabel .='				</td>';
			$tabel .='</tr>';
			$no=$no+1;
		}
		
				
		$this->output->set_output(json_encode($tabel));
	}
	function simpan_content(){
		// $data['header_id']=1;
		$header_id=$this->input->post('header_id');
		$id=$this->input->post('idcontent');
		$data['header_id']=$this->input->post('header_id');
		$data['no']=$this->input->post('no');
		$data['isi']=$this->input->post('isi');
		$data['jenis_isi']=$this->input->post('jenis_isi');
		if ($this->input->post('idcontent')){
			$general_isi_id=$id;
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$id);
			$hasil=$this->db->update('setting_ranap_lembar_masuk_isi',$data);
		}else{
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('setting_ranap_lembar_masuk_isi',$data);
			$general_isi_id=$this->db->insert_id();
		}
		$this->db->where('general_isi_id',$general_isi_id);		
		$this->db->delete('setting_ranap_lembar_masuk_isi_jawaban');
		$ref_id=$this->input->post('ref_id');
		// print_r($ref_id);exit;
		if ($ref_id){
			foreach($ref_id as $index=>$val){
				$data_det['general_isi_id']=$general_isi_id;
				$data_det['ref_id']=$val;
				$hasil=$this->db->insert('setting_ranap_lembar_masuk_isi_jawaban',$data_det);
			}
		}
		$this->output->set_output(json_encode($hasil));
	}
	function hapus(){
	  $id=$this->input->post('id');
	  $this->deleted_by = $this->session->userdata('user_id');
	  $this->deleted_date = date('Y-m-d H:i:s');
	  $this->status=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('setting_ranap_lembar_masuk_isi',$this);
	  
	  json_encode($hasil);
	  
	}
	function get_edit(){
	  $id=$this->input->post('id');
	  $q="SELECT *FROM setting_ranap_lembar_masuk_isi H WHERE H.id='$id'";
	  $hasil= $this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($hasil));
	  
	}
	function load_jawaban(){
	  $id=$this->input->post('id');
	  $q="SELECT H.id as ref_id,H.ref,CASE WHEN M.ref_id IS NOT NULL THEN 'selected' ELSE '' END as pilih 
			FROM merm_referensi H
			LEFT JOIN setting_ranap_lembar_masuk_isi_jawaban M ON M.ref_id=H.id AND M.general_isi_id='$id'
			WHERE H.ref_head_id='16' AND H.status='1'";
		
	  $hasil= $this->db->query($q)->result();
	  $data='';
	  foreach($hasil as $r){
		  $data .='<option '.$r->pilih.' value="'.$r->ref_id.'">'.$r->ref.'</option>';
	  }
	  $this->output->set_output(json_encode($data));
	  
	}
	function simpan_sound(){
		$idsound=$this->input->post('idsound');
		$sound_id=$this->input->post('sound_id');
		$nourut=$this->input->post('nourut');
		$data['nourut']=$nourut;
		$data['sound_id']=$sound_id;
		if ($idsound==''){
			// print_r($data);exit;
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('setting_ranap_terima_sound',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$idsound);
			$hasil=$this->db->update('setting_ranap_terima_sound',$data);
		}
		// $this->created_by = $this->session->userdata('user_id');
		// $this->created_date = date('Y-m-d H:i:s');
		
		  
		  json_encode($hasil);
	}
	function load_sound(){
		$this->select = array();
	  $from="(
		SELECT H.id, H.nourut,H.sound_id,M.nama_asset,M.file_sound 
		
			FROM setting_ranap_terima_sound H
			LEFT JOIN antrian_asset_sound M ON H.sound_id=M.id
			 WHERE H.status='1' ORDER BY nourut
		) as tbl";
		// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('sound_id');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
		$no++;
		$result = array();

		$result[] = $no;
		$result[] = $r->nourut;
		$result[] = $r->nama_asset.' ('.$r->file_sound.')';

		$aksi = '<div class="btn-group">';
		$aksi .= '<button onclick="edit_sound('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="si si-pencil"></i></button>';	

		$aksi .= '<button onclick="hapus_sound('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		 $aksi .= '</div>';
		  
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
	}
	function edit_sound(){
		$id=$this->input->post('id');
		$q="select *FROM setting_ranap_terima_sound H WHERE H.id='$id'";
		$data=$this->db->query($q)->row_array();
		echo json_encode($data);
	}
	function load_data_sound(){
		$q="SELECT GROUP_CONCAT(A.file_sound SEPARATOR ':') file
			FROM `setting_ranap_terima_sound` H
			LEFT JOIN antrian_asset_sound A ON A.id=H.sound_id
			WHERE H.status='1'
			ORDER BY H.nourut ASC";
		$data=$this->db->query($q)->row_array();
		echo json_encode($data);
	}
	function hapus_sound(){
		$id=$this->input->post('id');
		$data['status']=0;
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		// print_r($data);exit;
		$this->db->where('id',$id);
		$hasil=$this->db->update('setting_ranap_terima_sound',$data);
		json_encode($hasil);
	}
}
