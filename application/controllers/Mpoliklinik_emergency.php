<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpoliklinik_emergency extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpoliklinik_emergency_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// print_r($data);exit;
		if (UserAccesForm($user_acces_form,array('1784'))){
			
			$data['tab'] 			= $tab;
			$data['idtipe'] 			= '#';
			$data['idpoli'] 			= '#';
			$data['error'] 			= '';
			$data['title'] 			= 'Referensi Emergency Department';
			$data['content'] 		= 'Mpoliklinik_emergency/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Referensi Emergency Department",'mpoliklinik_emergency')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	

  function simpan_poli(){
		$idpoliklinik=$this->input->post('idpoli');
		$data=array(
			'idpoliklinik'=>$this->input->post('idpoli'),
		);
		$data['created_by']=$this->session->userdata('user_id');
		$data['created_date']=date('Y-m-d H:i:s');
		$hasil=$this->db->insert('mpoliklinik_emergency',$data);

		json_encode($hasil);
	}
	
	
	function load_poli()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.idpoliklinik,M.nama,M.idtipe FROM mpoliklinik_emergency H
						INNER JOIN mpoliklinik M ON M.id=H.idpoliklinik
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('idpoliklinik','nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = GetTipePasienPiutang($r->idtipe);
          $result[] = $r->nama;
          $aksi = '<div class="btn-group">';
		  
		   if (UserAccesForm($user_acces_form,array('1787'))){
		  $aksi .= '<button onclick="hapus_poli('.$r->idpoliklinik.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  function hapus_poli(){
	  $id=$this->input->post('id');
	 
		$this->db->where('idpoliklinik',$id);
		$hasil=$this->db->delete('mpoliklinik_emergency');
	  
	  json_encode($hasil);
	  
  }


  function find_poli($idtipe){
	  $q="SELECT H.id,H.nama FROM mpoliklinik H
			WHERE H.`status`='1' AND H.idtipe='$idtipe' AND H.id NOT IN (SELECT idpoliklinik FROM mpoliklinik_emergency)";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($idtipe!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
  
  
}
