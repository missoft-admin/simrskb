<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mbed extends CI_Controller {

	/**
	 * Bed controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mbed_model');
  }

	function index(){

		$data = array(
			'idruangan' => '',
			'idkelas' => '',
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Bed';
		$data['content'] 		= 'Mbed/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Bed",'#'),
									    			array("List",'mbed')
													);

		$data['list_ruangan'] = $this->Mbed_model->getAllRuangan();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function filter(){
		$data = array(
			'idruangan' => $this->input->post('idruangan'),
			'idkelas' => $this->input->post('idkelas'),
		);

		$this->session->set_userdata($data);

		$data['error'] 			= '';
		$data['title'] 			= 'Bed';
		$data['content'] 		= 'Mbed/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Bed",'#'),
									    			array("List",'mbed')
													);

		$data['list_ruangan'] = $this->Mbed_model->getAllRuangan();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){

		$data = array(
			'id' 				=> '',
			'idruangan' => '',
			'idkelas' 	=> '',
			'nama' 			=> '',
			'status' 		=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Bed';
		$data['content'] 		= 'Mbed/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Bed",'#'),
									    			array("Tambah",'mbed')
													);

		$data['list_ruangan'] = $this->Mbed_model->getAllRuangan();
		$data['list_kelas'] = $this->Mbed_model->getAllKelas();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){

		if($id != ''){
			$row = $this->Mbed_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 				=> $row->id,
					'idruangan'	=> $row->idruangan,
					'idkelas'		=> $row->idkelas,
					'nama' 			=> $row->nama,
					'status' 		=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Bed';
				$data['content']	 	= 'Mbed/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Bed",'#'),
											    			array("Ubah",'mbed')
															);

				$data['list_ruangan'] = $this->Mbed_model->getAllRuangan();
				$data['list_kelas'] = $this->Mbed_model->getAllKelas();

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mbed/index','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mbed/index');
		}
	}

	function enable($id){
		$this->Mbed_model->changeStatus($id, 1);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Bed berhasil diaktifkan.');
		redirect('mbed/index','location');
	}

	function disable($id){
		$this->Mbed_model->changeStatus($id, 0);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Bed berhasil di non aktifkan.');
		redirect('mbed/index','location');
	}

	function save(){
		$this->form_validation->set_rules('idruangan', 'Ruangan', 'trim|required');
		$this->form_validation->set_rules('idkelas', 'Kelas', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mbed_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mbed/index','location');
				}
			} else {
				if($this->Mbed_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mbed/index','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 					= validation_errors();
		$data['content'] 				= 'Mbed/manage';

		$data['list_kategori'] 	= $this->Mbed_model->getAllKategori();
		$data['list_satuan'] 		= $this->Mbed_model->getAllSatuan();

		if($id==''){
			$data['title'] = 'Tambah Bed';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Bed",'#'),
															array("Tambah",'mbed')
													);
		}else{
			$data['title'] = 'Ubah Bed';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Bed",'#'),
															array("Ubah",'mbed')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex($uri)
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			$this->select = array('mbed.*', 'mruangan.nama AS namaruangan', 'mkelas.nama AS namakelas');
			$this->from   = 'mbed';
			$this->join 	= array(
				array('mruangan', 'mruangan.id = mbed.idruangan', ''),
				array('mkelas', 'mkelas.id = mbed.idkelas', 'LEFT')
			);

			// FILTER
			if($uri == 'filter'){
				$this->where  = array();
				if ($this->session->userdata('idruangan') != "#") {
					$this->where = array_merge($this->where, array('mbed.idruangan' => $this->session->userdata('idruangan')));
				}
				if ($this->session->userdata('idkelas') != "#") {
					$this->where = array_merge($this->where, array('mbed.idkelas' => $this->session->userdata('idkelas')));
				}
			}else{
				$this->where  = array(
					'mbed.status' => '1'
				);
			}

			$this->order  = array(
				'mbed.id' => 'DESC'
			);
			$this->group  = array();
			if (UserAccesForm($user_acces_form,array('253'))){
				$this->column_search   = array('mruangan.nama', 'mkelas.nama', 'mbed.nama');
			}else{
				$this->column_search   = array();
			}

			$this->column_order    = array('mruangan.nama', 'mkelas.nama', 'mbed.nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$aksi = '<div class="btn-group">';

					if($r->status == 1){
			            if (UserAccesForm($user_acces_form,array('193'))){
			                $aksi .= '<a href="'.site_url().'mbed/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
			            }
			            if (UserAccesForm($user_acces_form,array('194'))){
			                $aksi .= '<a href="'.site_url().'mbed/disable/'.$r->id.'" data-toggle="tooltip" title="Non Aktifkan" class="btn btn-danger btn-sm"><i class="fa fa-ban"></i></a>';
			            }
					}else{
						if (UserAccesForm($user_acces_form,array('193'))){
			                $aksi .= '<a href="'.site_url().'mbed/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
			            }
			            if (UserAccesForm($user_acces_form,array('254'))){
			                $aksi .= '<a href="'.site_url().'mbed/enable/'.$r->id.'" data-toggle="tooltip" title="Aktifkan" class="btn btn-success btn-sm"><i class="fa fa-reply"></i></a>';
			            }
					}

		            $aksi .= '</div>';

					$row[] = $no;
					$row[] = $r->namaruangan;
					$row[] = $r->namakelas;
					$row[] = $r->nama;
					$row[] = '<div class="btn-group">'.$aksi.'</div>';

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}
}
