<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mjenis_pengajuan extends CI_Controller {

	/**
	 * Master Jenis Pengajuan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mjenis_pengajuan_model');
		$this->load->model('Mnomorakuntansi_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Master Jenis Pengajuan';
		$data['content'] 		= 'Mjenis_pengajuan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Master Jenis Pengajuan",'#'),
									    			array("List",'mjenis_pengajuan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'deskripsi'			=> '',
			'st_berjenjang'			=> '0',
			'idakun'			=> '0',
			'status' 				=> ''
		);

		$data['list_akun'] 			= $this->Mnomorakuntansi_model->list_akun();
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Master Jenis Pengajuan';
		$data['content'] 		= 'Mjenis_pengajuan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Master Jenis Pengajuan",'#'),
									    			array("Tambah",'mjenis_pengajuan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mjenis_pengajuan_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 					=> $row->id,
					'nama' 					=> $row->nama,
					'deskripsi' 			=> $row->deskripsi,
					'st_berjenjang' 		=> $row->st_berjenjang,
					'idakun' 				=> $row->idakun,
					'status' 				=> $row->status
				);
				$data['error'] 			= '';
				$data['list_akun'] 			= $this->Mnomorakuntansi_model->list_akun();
				$data['title'] 			= 'Ubah Master Jenis Pengajuan';
				$data['content']	 	= 'Mjenis_pengajuan/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Master Jenis Pengajuan",'#'),
											    			array("Ubah",'mjenis_pengajuan')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mjenis_pengajuan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mjenis_pengajuan');
		}
	}

	function delete($id){
		$this->Mjenis_pengajuan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mjenis_pengajuan','location');
	}
	function aktifkan($id){
		$this->Mjenis_pengajuan_model->aktifkan($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah diaktifkan.');
		redirect('mjenis_pengajuan','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mjenis_pengajuan_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mjenis_pengajuan','location');
				}
			} else {
				if($this->Mjenis_pengajuan_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mjenis_pengajuan','location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}
	
	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mjenis_pengajuan/manage';

		if($id==''){
			$data['title'] = 'Tambah Master Jenis Pengajuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Jenis Pengajuan",'#'),
															array("Tambah",'mjenis_pengajuan')
													);
		}else{
			$data['title'] = 'Ubah Master Jenis Pengajuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Master Jenis Pengajuan",'#'),
															array("Ubah",'mjenis_pengajuan')
													);
		}
		$data['list_akun'] 			= $this->Mnomorakuntansi_model->list_akun();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
			$from="
				(
					SELECT M.*,A.noakun,A.namaakun FROM mjenis_pengajuan_rka M LEFT JOIN makun_nomor A ON A.id=M.idakun
				)tbl
			";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array(
				'id' => 'DESC'
			);
			$this->group  = array();

      $this->column_search   = array('nama');
      $this->column_order    = array('nama');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					$aksi = '<div class="btn-group">';
					// if (UserAccesForm($user_acces_form,array('131'))){
					// }
					// if (UserAccesForm($user_acces_form,array('132'))){
						if ($r->status=='1'){
						$aksi .= '<a href="'.site_url().'mjenis_pengajuan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mjenis_pengajuan" data-urlremove="'.site_url().'mjenis_pengajuan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
							
						}else{
						
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mjenis_pengajuan" data-urlremove="'.site_url().'mjenis_pengajuan/aktifkan/'.$r->id.'" data-toggle="tooltip" title="Aktifkan" class="btn btn-success btn-sm aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
						}
					// }
					
			        $aksi .= '</div>';

          $row[] = $no;
          $row[] = $r->nama;
		  $row[] = ($r->st_berjenjang=='1'?'<span class="label label-primary">BERJENJANG</span>':'<span class="label label-danger">TIDAK</span>');
          $row[] = $r->noakun.' - '.$r->namaakun;
          $row[] = $r->deskripsi;
		  $row[] = ($r->status=='1'?'<span class="label label-primary">ACTIVE</span>':'<span class="label label-danger">NOT ACTIVE</span>');
          $row[] = $aksi;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }

}
