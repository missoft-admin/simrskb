<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mbagi_hasil extends CI_Controller {

	/**
	 * Pegawai controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mbagi_hasil_model');
		$this->load->model('Mtarif_rawatjalan_model');
		$this->load->model('Mtarif_rawatinap_model');
		$this->load->model('Mtarif_fisioterapi_model');
		$this->load->model('Tpoliklinik_verifikasi_model');
		$this->load->model('Trawatinap_tindakan_model');
		$this->load->model('Trawatinap_verifikasi_model');
		$this->load->helper('path');
		
  }

	function index($idkategori='0',$tipe_pemilik='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('2'))){
			$data = array();
			$data['idkategori'] 			= $idkategori;
			$data['tipe_pemilik'] 			= $tipe_pemilik;
			$data['tipe_pemilik'] 			= '#';
			$data['status'] 			= '#';
			$data['error'] 			= '';
			$data['title'] 			= 'Bagi Hasil';
			$data['content'] 		= 'Mbagi_hasil/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Bagi Hasil",'#'),
												  array("List",'mbagi_hasil')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}

	function create($config=''){
		
		$data = array(
			'id'            => '',
			'nama' 					=> '',
			'tanggal_mulai' 					=> '',
			'bagian_rs' 	=> '0',
			'bagian_ps' 	=> '0',
			'tanggal_mulai' 				=> '',
			'keterangan' 	    => '',
			'status_langsung' 	    => '#',
		);

		$data['config'] 			= $config;
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Bagi Hasil';
		$data['content'] 		= 'Mbagi_hasil/manage';
		$data['breadcrum']	= array(
								            array("RSKB Halmahera",'#'),
								            array("Bagi Hasil",'#'),
								            array("Tambah",'mbagi_hasil')
								        	);

		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id,$config=''){
		
		if($id != ''){
			$data= $this->Mbagi_hasil_model->getSpecified($id);
			$data['list_ps']= $this->Mbagi_hasil_model->list_ps();
			
			
			
			$data['config'] 		= $config;
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Bagi Hasil';
			$data['content']    = 'Mbagi_hasil/manage';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Bagi Hasil",'#'),
										array("Ubah",'mbagi_hasil')
										);

			// $data['statusAvailableApoteker'] = $this->Mbagi_hasil_model->getStatusAvailableApoteker();
			
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mbagi_hasil');
		}
	}
	function layanan($id,$config=''){
		
		if($id != ''){
			$data= $this->Mbagi_hasil_model->getSpecified($id);
			
			$data['config'] 		= $config;
			$data['idpath_rajal'] 			= '#';
			$data['error'] 			= '';
			$data['title'] 			= 'Tambah Tarif Layanan';
			$data['content']    = 'Mbagi_hasil/manage_layanan';
			$data['breadcrum'] 	= array(
										array("RSKB Halmahera",'#'),
										array("Bagi Hasil",'#'),
										array("Ubah",'mbagi_hasil')
										);

			// $data['statusAvailableApoteker'] = $this->Mbagi_hasil_model->getStatusAvailableApoteker();
			$data['list_ruangan'] = $this->Mtarif_rawatinap_model->getAllRuangan();
			$data['list_level0'] = $this->Mtarif_rawatjalan_model->getLevel0();
			$data['list_level0_fis'] = $this->Mtarif_fisioterapi_model->getLevel0();
			$data['list_jenis_ko'] = $this->Mbagi_hasil_model->list_jenis_ko();
			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
			
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mbagi_hasil');
		}
	}
	
	function delete($id){
		
		$result=$this->Mbagi_hasil_model->softDelete($id);
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		// $this->session->set_flashdata('confirm',true);
		// $this->session->set_flashdata('message_flash','data telah terhapus.');
		// redirect('mbagi_hasil','location');
	}
	function aktifkan($id){
		
		$result=$this->Mbagi_hasil_model->aktifkan($id);
		if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}

	function save(){
	
		if($this->input->post('id') == '' ) {
			$id=$this->Mbagi_hasil_model->saveData();
			if($id){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mbagi_hasil/update/'.$id,'location');
			}
		} else {
			if($this->Mbagi_hasil_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('mbagi_hasil','location');
			}
		}
		
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mbagi_hasil/manage';

		if($id==''){
			$data['title'] = 'Tambah Bagi Hasil';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Bagi Hasil",'#'),
							               array("Tambah",'mbagi_hasil')
								           );
		}else{
			$data['title'] = 'Ubah Bagi Hasil';
			$data['breadcrum'] = array(
							               array("RSKB Halmahera",'#'),
							               array("Bagi Hasil",'#'),
							               array("Ubah",'mbagi_hasil')
							             );
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}
	
	function getIndex($idkategori='0',$tipe_pemilik='0')
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$status = $this->input->post('status');
			$nama = $this->input->post('nama');
			
			if ($status !='#'){
				$where .=" AND status='$status'";
			}
			if ($nama !=''){
				$where .=" AND nama LIKE '%".$nama."%'";
			}
			$this->select = array();
			$from="
					(
						SELECT M.id,M.nama,M.bagian_rs,M.bagian_ps,M.tanggal_mulai,M.status
						,COUNT(DISTINCT D.idtransaksi) as jml_dok,COALESCE(SUM(PS.jumlah_lembar),0)  as jml_lembar
						,COUNT(DISTINCT L.id) as jml_layanan
						from mbagi_hasil M
						LEFT JOIN mbagi_hasil_dokumen D ON D.idtransaksi=M.id AND D.status='1'
						LEFT JOIN mbagi_hasil_pemilik_saham PS ON PS.mbagi_hasil_id=M.id AND PS.status='1'
						LEFT JOIN mbagi_hasil_layanan L ON L.mbagi_hasil_id=M.id AND L.status='1'
						GROUP BY M.id
					) as tbl WHERE id IS NOT NULL ".$where."
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $r->id;
          $result[] = $no;
          $result[] = $r->nama;
          $result[] = number_format($r->bagian_rs,1).' %';
          $result[] = number_format($r->bagian_ps,1).' %';
          $result[] = HumanDateShort($r->tanggal_mulai);
          $result[] = number_format($r->jml_lembar,0).' Lembar';
          $result[] = number_format($r->jml_dok,0).' Upload';
          $result[] = number_format($r->jml_layanan,0).' Tarif';
         $result[] = StatusBarang($r->status);
          $aksi = '<div class="btn-group">';
			if ($r->status=='1'){
				$aksi .= '<a href="'.site_url().'mbagi_hasil/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<a href="'.site_url().'mbagi_hasil/update/'.$r->id.'/config" data-toggle="tooltip" title="Ubah" class="btn btn-success btn-xs"><i class="si si-settings"></i></a>';
				$aksi .= '<a href="'.site_url().'mbagi_hasil/layanan/'.$r->id.'" data-toggle="tooltip" title="Tambah Layanan" class="btn btn-primary btn-xs"><i class="fa fa-search-plus"></i></a>';
				$aksi .= '<button title="Hapus" class="btn btn-danger btn-xs removeData"><i class="fa fa-trash-o"></i></button>';
			}else{
				$aksi .= '<button title="Aktifkan" class="btn btn-success btn-xs aktifData"><i class="fa fa-check"></i> Aktifkan Kembali</a>';
			}
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  //TANGGAL
  function load_tanggal()
    {
		$mbagi_hasil_id     		= $this->input->post('mbagi_hasil_id');


		$this->select = array();
		$this->join 	= array();
		$this->where  = array();


		$from="(SELECT M.* from mbagi_hasil_tanggal M
				WHERE M.mbagi_hasil_id='$mbagi_hasil_id' AND M.status='1'
				ORDER BY M.tanggal_hari ASC
			) as tbl  ORDER BY tanggal_hari ASC";

		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();
		$url        = site_url('mpasien_kelompok/');
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

           $row[] = $r->id;//0
			$row[] = $r->tanggal_hari;//1
            $row[] = $r->deskripsi;
            $row[] =$r->updated_nama.'<br>'.HumanDateLong($r->updated_date);

			$aksi       = '<div class="btn-group">';
			$aksi .= '<button class="btn btn-xs btn-primary edit_tanggal" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button class="btn btn-xs btn-danger hapus_tanggal" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';

			$aksi.='</div>';
			$row[] = $aksi;
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
  public function save_tanggal()
    {
		
        $mbagi_hasil_id = $this->input->post('mbagi_hasil_id');
        $tanggal_hari = $this->input->post('tanggal_hari');
        $deskripsi = $this->input->post('deskripsi');
      
		$data =array(
            'mbagi_hasil_id'=>$mbagi_hasil_id,
            'tanggal_hari'=>$tanggal_hari,
            'deskripsi'=>$deskripsi,
            'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
			'created_date'=>date('Y-m-d H:i:s'),
			'updated_by'=>$this->session->userdata('user_id'),
			'updated_nama'=>$this->session->userdata('user_name'),
			'updated_date'=>date('Y-m-d H:i:s'),


        );
		$result=$this->db->insert('mbagi_hasil_tanggal',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function simpan_layanan()
    {
		// data: {arr_id: arr_id,arr_tabel: arr_tabel,arr_js: arr_js,arr_jp: arr_jp,arr_bhp: arr_bhp,arr_bp: arr_bp},
        $arr_id = $this->input->post('arr_id');
        $arr_tabel = $this->input->post('arr_tabel');
        $arr_js = $this->input->post('arr_js');
        $arr_jp = $this->input->post('arr_jp');
        $arr_bhp = $this->input->post('arr_bhp');
        $arr_bp = $this->input->post('arr_bp');
        $mbagi_hasil_id = $this->input->post('mbagi_hasil_id');
        // $table_id=;
		foreach($arr_id as $index=>$value){
			$data =array(
				'mbagi_hasil_id'=>$mbagi_hasil_id,
				'tabel'=>$arr_tabel[$index],
				'table_id'=>get_table_id($arr_tabel[$index]),
				'idlayanan'=>$arr_id[$index],
				'jasasarana'=>$arr_js[$index],
				'jasapelayanan'=>$arr_jp[$index],
				'bhp'=>$arr_bhp[$index],
				'biayaperawatan'=>$arr_bp[$index],
				'status'=>1,
				'created_by'=>$this->session->userdata('user_id'),
				'created_date'=>date('Y-m-d H:i:s'),
			);
			$result=$this->db->insert('mbagi_hasil_layanan',$data);
		}
		

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function update_tanggal()
    {
		
        $tanggal_id = $this->input->post('tanggal_id');
        $mbagi_hasil_id = $this->input->post('mbagi_hasil_id');
        $tanggal_hari = $this->input->post('tanggal_hari');
        $deskripsi = $this->input->post('deskripsi');
      
		$data =array(
            'tanggal_hari'=>$tanggal_hari,
            'deskripsi'=>$deskripsi,
			'updated_by'=>$this->session->userdata('user_id'),
			'updated_nama'=>$this->session->userdata('user_name'),
			'updated_date'=>date('Y-m-d H:i:s'),


        );
		$this->db->where('id',$tanggal_id);
		$result=$this->db->update('mbagi_hasil_tanggal',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function hapus_tanggal()
    {
        $tanggal_id = $this->input->post('tanggal_id');

		$data =array(
            'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_nama'=>$this->session->userdata('user_name'),
			'deleted_date'=>date('Y-m-d H:i:s'),


        );
		$this->db->where('id',$tanggal_id);;
		$result=$this->db->update('mbagi_hasil_tanggal',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	function update_realtime(){
		// data: {id: $id,urutan: $urutan,nilai: $nilai},
		$id = $this->input->post('id');
		$urutan = $this->input->post('urutan');
		$nilai = $this->input->post('nilai');
		$data =array(
			'edited_by'=>$this->session->userdata('user_id'),
			'edited_date'=>date('Y-m-d H:i:s'),
        );
		if ($urutan=='1'){
			$data['jasasarana']=$nilai;			
		}
		if ($urutan=='2'){
			$data['jasapelayanan']=$nilai;			
		}
		if ($urutan=='3'){
			$data['bhp']=$nilai;			
		}
		if ($urutan=='4'){
			$data['biayaperawatan']=$nilai;			
		}
		$this->db->where('id',$id);;
		$result=$this->db->update('mbagi_hasil_layanan',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
		
	}
	public function hapus_layanan()
    {
        $id = $this->input->post('id');

		$data =array(
            'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_date'=>date('Y-m-d H:i:s'),


        );
		$this->db->where('id',$id);;
		$result=$this->db->update('mbagi_hasil_layanan',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	function cek_duplicate_tanggal($mbagi_hasil_id,$tanggal_hari,$tanggal_id=''){
		if ($tanggal_id ==''){
			$q="SELECT COUNT(M.id)  as jml From mbagi_hasil_tanggal M
				WHERE M.mbagi_hasil_id='$mbagi_hasil_id' AND M.tanggal_hari='$tanggal_hari' AND M.status='1'";			
		}else{
			$q="SELECT COUNT(M.id)  as jml From mbagi_hasil_tanggal M
				WHERE M.mbagi_hasil_id='$mbagi_hasil_id' AND M.tanggal_hari='$tanggal_hari' AND M.id !='$tanggal_id' AND M.status='1'";
		}
			
		
		
		$arr['detail'] =$this->db->query($q)->row_array();		
		// $data['jml']=$row;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($data));
		
	}
	
	//SAHAM
	 function load_pemilik_saham()
    {
		$mbagi_hasil_id     		= $this->input->post('mbagi_hasil_id');


		$this->select = array();
		$this->join 	= array();
		$this->where  = array();


		$from="(
				SELECT M.id,M.mbagi_hasil_id,M.id_pemilik_saham,M.jumlah_lembar,M.updated_nama,M.updated_date
				,CASE WHEN PS.tipe_pemilik='1' THEN MP.nama WHEN PS.tipe_pemilik='2' THEN MD.nama ELSE PS.nama END as nama
				,CASE WHEN PS.tipe_pemilik='1' THEN 'PEGAWAI' WHEN PS.tipe_pemilik='2' THEN 'DOKTER' ELSE 'NON PEGAWAI' END as tipe_nama
				from mbagi_hasil_pemilik_saham M
				LEFT JOIN mpemilik_saham PS ON PS.id=M.id_pemilik_saham
				LEFT JOIN mpegawai MP ON MP.id=PS.idpeg_dok AND PS.tipe_pemilik='1'
				LEFT JOIN mdokter MD ON MD.id=PS.idpeg_dok AND PS.tipe_pemilik='2'
				WHERE M.mbagi_hasil_id='$mbagi_hasil_id' AND M.status='1'
				ORDER BY M.id_pemilik_saham ASC
			) as tbl  ORDER BY nama ASC";

		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();

           $row[] = $r->id;//0
			$row[] = $r->id_pemilik_saham;//1
            $row[] = $r->nama;
            $row[] = $r->tipe_nama;
            $row[] = $r->jumlah_lembar;
            $row[] =$r->updated_nama.'<br>'.HumanDateLong($r->updated_date);

			$aksi       = '<div class="btn-group">';
			$aksi .= '<button class="btn btn-xs btn-primary edit_pemilik_saham" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			$aksi .= '<button class="btn btn-xs btn-danger hapus_pemilik_saham" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';

			$aksi.='</div>';
			$row[] = $aksi;
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
  public function save_pemilik_saham()
    {
		
        $mbagi_hasil_id = $this->input->post('mbagi_hasil_id');
        $id_pemilik_saham = $this->input->post('id_pemilik_saham');
        $jumlah_lembar = $this->input->post('jumlah_lembar');
      
		$data =array(
            'mbagi_hasil_id'=>$mbagi_hasil_id,
            'id_pemilik_saham'=>$id_pemilik_saham,
            'jumlah_lembar'=>$jumlah_lembar,
            'created_by'=>$this->session->userdata('user_id'),
			'created_nama'=>$this->session->userdata('user_name'),
			'created_date'=>date('Y-m-d H:i:s'),
			'updated_by'=>$this->session->userdata('user_id'),
			'updated_nama'=>$this->session->userdata('user_name'),
			'updated_date'=>date('Y-m-d H:i:s'),


        );
		$result=$this->db->insert('mbagi_hasil_pemilik_saham',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function update_pemilik_saham()
    {
		
        $ps_id = $this->input->post('ps_id');
        $mbagi_hasil_id = $this->input->post('mbagi_hasil_id');
        $id_pemilik_saham = $this->input->post('id_pemilik_saham');
        $jumlah_lembar = $this->input->post('jumlah_lembar');
      
		$data =array(
            'id_pemilik_saham'=>$id_pemilik_saham,
            'jumlah_lembar'=>$jumlah_lembar,
			'updated_by'=>$this->session->userdata('user_id'),
			'updated_nama'=>$this->session->userdata('user_name'),
			'updated_date'=>date('Y-m-d H:i:s'),


        );
		$this->db->where('id',$ps_id);
		$result=$this->db->update('mbagi_hasil_pemilik_saham',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	public function hapus_pemilik_saham()
    {
        $ps_id = $this->input->post('ps_id');

		$data =array(
            'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_nama'=>$this->session->userdata('user_name'),
			'deleted_date'=>date('Y-m-d H:i:s'),


        );
		$this->db->where('id',$ps_id);;
		$result=$this->db->update('mbagi_hasil_pemilik_saham',$data);

        if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
    }
	function cek_duplicate_pemilik_saham($mbagi_hasil_id,$id_pemilik_saham,$ps_id=''){
		if ($ps_id ==''){
			$q="SELECT COUNT(M.id)  as jml From mbagi_hasil_pemilik_saham M
				WHERE M.mbagi_hasil_id='$mbagi_hasil_id' AND M.id_pemilik_saham='$id_pemilik_saham' AND M.status='1'";			
		}else{
			$q="SELECT COUNT(M.id)  as jml From mbagi_hasil_pemilik_saham M
				WHERE M.mbagi_hasil_id='$mbagi_hasil_id' AND M.id_pemilik_saham='$id_pemilik_saham' AND M.id !='$ps_id' AND M.status='1'";
		}
			
		
		
		$arr['detail'] =$this->db->query($q)->row_array();		
		// $data['jml']=$row;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($data));
		
	}
	function get_tipe_pemilik_saham($id_pemilik_saham){
		$q="SELECT 
			M.id,M.tipe_pemilik
			,CASE WHEN M.tipe_pemilik='1' THEN MP.nama WHEN M.tipe_pemilik='2' THEN MD.nama ELSE M.nama END as nama
			,CASE WHEN M.tipe_pemilik='1' THEN 'PEGAWAI' WHEN M.tipe_pemilik='2' THEN 'DOKTER' ELSE 'NON PEGAWAI' END as tipe_nama
			,M.npwp,M.keterangan,M.`status`

			from mpemilik_saham M

			LEFT JOIN mpegawai MP ON MP.id=M.idpeg_dok AND M.tipe_pemilik='1'
			LEFT JOIN mdokter MD ON MD.id=M.idpeg_dok AND M.tipe_pemilik='2'
			WHERE M.id='$id_pemilik_saham'
			";
		$arr['detail'] =$this->db->query($q)->row_array();		
		// $data['jml']=$row;
		$this->output->set_output(json_encode($arr));
		// $this->output->set_output(json_encode($data));
		
	}
	
	public function upload_files()
    {
        $files = array();
        foreach ($_FILES as $file) {
            if ($file['name'] != '') {
                $config['upload_path'] = './assets/upload/mbagi_hasil/';
                $config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';

                $files[] = $file['name'];
				$new_name = time().'-'.$file['name'];
				$config['file_name'] = $new_name;
                $this->upload->initialize($config);

                if ($this->upload->do_upload('file')) {
                    $image_upload = $this->upload->data();
                    $table = 'mbagi_hasil_dokumen';

                    $data = array();
                    $data['idtransaksi']  = $this->input->post('idtransaksi');
                    // $data['keterangan']  = $this->input->post('keterangan');
                    $data['upload_by']  = $this->session->userdata('user_id');
                    $data['upload_by_nama']  = $this->session->userdata('user_name');
                    $data['upload_date']  = date('Y-m-d H:i:s');
                    $data['filename'] = $image_upload['file_name'];
                    $data['size'] = formatSizeUnits($file['size']);

                    $this->db->insert($table, $data);

                    return true;
                } else {
                    print_r($this->upload->display_errors());
                    exit();

                    return false;
                }
            } else {
                return true;
            }
        }
    }
	function refresh_image($id){		
		$arr['detail'] = $this->Mbagi_hasil_model->refresh_image($id);
		$this->output->set_output(json_encode($arr));
	}
	function removeFile(){
		$id=$this->input->post('id');
		$this->db->where('id', $id);
        $query = $this->db->get('mbagi_hasil_dokumen');
        $row = $query->row();
		if(file_exists('./assets/upload/mbagi_hasil/'.$row->filename) && $row->filename !='') {
			unlink('./assets/upload/mbagi_hasil/'.$row->filename);
		}else{
			
		}
		$result=$this->db->query("delete from mbagi_hasil_dokumen WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	 // public function delete_file($idfile)
    // {
        // $this->db->where('id', $idfile);
        // $query = $this->db->get('mbagi_hasil_dokumen');
        // $row = $query->row();
        // if ($query->num_rows() > 0) {
            // $this->db->where('id', $idfile);
            // if ($this->db->delete('mbagi_hasil_dokumen')) {
                // if (file_exists('./assets/upload/mbagi_hasil/'.$row->filename) && $row->filename !='') {
                    // unlink('./assets/upload/mbagi_hasil/'.$row->filename);
                // }
            // }
        // }

        // return true;
    // }
	function load_administrasi()
    {
		$tipe_adm     		= $this->input->post('tipe_adm');
		$jenis_adm     		= $this->input->post('jenis_adm');
		$nama_adm     		= $this->input->post('nama_adm');
		$where='';
		if ($tipe_adm!='#'){
			$where .=" AND M.idtipe='$tipe_adm'";
		}
		if ($jenis_adm!='#'){
			$where .=" AND M.idjenis='$jenis_adm'";
		}
		if ($nama_adm!=''){
			$where .=" AND M.nama LIKE '%".$nama_adm."%'";
		}
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		$from="(SELECT M.id,M.idtipe
				,CASE WHEN M.idtipe='1' THEN 'RAWAT JALAN' ELSE 'RAWAT INAP' END as tipe_nama
				,M.nama,M.idjenis
				FROM mtarif_administrasi M
				WHERE M.`status`='1' AND M.id NOT IN(SELECT L.idlayanan FROM mbagi_hasil_layanan L WHERE L.`status`='1' AND L.table_id='1') 
				".$where."
			) as tbl  ORDER BY idtipe,nama ASC";

		// print_r($from);exit();
		$this->from   = $from;
		$this->order  = array();$this->group  = array();$this->column_search   = array('tipe_nama','nama');$this->column_order    = array();		
        $list = $this->datatable->get_datatables(true);$data = array();$no = $_POST['start'];
        foreach ($list as $r) {
            $no++;$row = array();
            $row[] = "mtarif_administrasi";
            $row[] = $r->id;//1
			 $row[] = $r->id;//1
            $row[] =($r->idtipe=='1'?'<span class="label label-primary">Rawat Jalan</span>':'<span class="label label-success">Rawat Inap</span>');
            $row[] = $r->nama;
            $row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="js_adm"><span></span></label><input type="hidden" width="100%" class="input_js" value="0">';
            $row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jp_adm"><span></span></label><input type="hidden" class="input_jp" value="0">';
            $row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bhp_adm"><span></span></label><input type="hidden" class="input_bhp" value="0">';
            $row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bp_adm"><span></span></label><input type="hidden" class="input_tabel" value="mtarif_administrasi"><input type="hidden" class="input_bp" value="0"><input type="hidden" class="input_tot" value="0"><input type="hidden" class="input_id" value="'.$r->id.'">';
			
			$aksi       = '<div class="btn-group">';
			// $aksi .= '<button class="btn btn-xs btn-primary edit_tanggal" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			// $aksi .= '<button class="btn btn-xs btn-danger hapus_tanggal" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';

			$aksi.='</div>';
			$row[] = '';
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	function load_rajal()
    {
		$idpath_rajal     		= $this->input->post('idpath_rajal');
		$panjang=strlen($idpath_rajal);
		$nama_rajal     		= $this->input->post('nama_rajal');
		
		$where='';
		if ($idpath_rajal!='#'){
			$where .=" AND left(H.path,".$panjang.")='$idpath_rajal'";
		}
		// if ($jenis_adm!='#'){
			// $where .=" AND M.idjenis='$jenis_adm'";
		// }
		if ($nama_rajal!=''){
			$where .=" AND H.nama LIKE '%".$nama_rajal."%'";
		}
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		$from="(SELECT H.id,H.nama,H.`level`,H.path,H.idkelompok FROM mtarif_rawatjalan H
				WHERE H.`status`='1' AND H.path IS NOT NULL AND H.path !=''  AND H.id NOT IN(SELECT L.idlayanan FROM mbagi_hasil_layanan L WHERE L.`status`='1' AND L.table_id='2') ".$where."
			) as tbl  ORDER BY path ASC";

		// print_r($from);exit();
		$this->from   = $from;
		$this->order  = array();$this->group  = array();$this->column_search   = array('tipe_nama','nama');$this->column_order    = array();		
        $list = $this->datatable->get_datatables(true);$data = array();$no = $_POST['start'];
        foreach ($list as $r) {
            $no++;$row = array();
			$nama_tabel="mtarif_rawatjalan";
            $row[] = $nama_tabel;
            $row[] = $r->id;//1
			$row[] = $no;//2
            $row[] ='Rawat Jalan';
            // $row[] = $r->nama;
			$row[] = TreeView($r->level, $r->nama);
			if ($r->idkelompok=='0'){
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="js_adm"><span></span></label><input type="hidden" width="100%" class="input_js" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jp_adm"><span></span></label><input type="hidden" class="input_jp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bhp_adm"><span></span></label><input type="hidden" class="input_bhp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bp_adm"><span></span></label><input type="hidden" class="input_tabel" value="'.$nama_tabel.'"><input type="hidden" class="input_bp" value="0"><input type="hidden" class="input_tot" value="0"><input type="hidden" class="input_id" value="'.$r->id.'">';
			}else{
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
			
			}
			$aksi       = '<div class="btn-group">';
			// $aksi .= '<button class="btn btn-xs btn-primary edit_tanggal" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			// $aksi .= '<button class="btn btn-xs btn-danger hapus_tanggal" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';

			$aksi.='</div>';
			$row[] = '';
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	function load_rad()
    {
		$idpath_rad     		= $this->input->post('idpath_rad');
		$panjang=strlen($idpath_rad);
		$idtipe_rad     		= $this->input->post('idtipe_rad');
		$nama_rad     		= $this->input->post('nama_rad');
		
		$where='';
		if ($idpath_rad!='#'){
			$where .=" AND left(H.path,".$panjang.")='$idpath_rad'";
		}
		if ($idtipe_rad!='#'){
			$where .=" AND H.idtipe='$idtipe_rad'";
		}
		if ($nama_rad!=''){
			$where .=" AND H.nama LIKE '%".$nama_rad."%'";
		}
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		$from="(SELECT H.id,H.nama,H.path,H.`level`,H.idtipe,H.idkelompok
				,(CASE WHEN H.idtipe = 1 AND H.idkelompok = 0 THEN
						  CONCAT(H.nama,  ' (', E.nama, F.nama, ')')
						ELSE
						  H.nama
						END) AS namatarif

				FROM `mtarif_radiologi` H
				LEFT JOIN mtarif_radiologi_film F ON F.id=H.idfilm
				LEFT JOIN mtarif_radiologi_expose E ON E.id=H.idexpose
				WHERE H.`status` = '1'  AND H.id NOT IN(SELECT L.idlayanan FROM mbagi_hasil_layanan L WHERE L.`status`='1' AND L.table_id='5') ".$where."
			) as tbl  ORDER BY path ASC";

		// print_r($from);exit();
		$this->from   = $from;
		$this->order  = array();$this->group  = array();$this->column_search   = array('tipe_nama','nama');$this->column_order    = array();		
        $list = $this->datatable->get_datatables(true);$data = array();$no = $_POST['start'];
        foreach ($list as $r) {
            $no++;$row = array();
			$nama_tabel="mtarif_radiologi";
            $row[] = $nama_tabel;
            $row[] = $r->id;//1
			$row[] = $r->id;//2
            $row[] ='Radiologi';
            // $row[] = $r->nama;
			 $row[] = TreeView($r->level, $r->namatarif);
			if ($r->idkelompok=='0'){
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="js_adm"><span></span></label><input type="hidden" width="100%" class="input_js" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jp_adm"><span></span></label><input type="hidden" class="input_jp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bhp_adm"><span></span></label><input type="hidden" class="input_bhp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bp_adm"><span></span></label><input type="hidden" class="input_tabel" value="'.$nama_tabel.'"><input type="hidden" class="input_bp" value="0"><input type="hidden" class="input_tot" value="0"><input type="hidden" class="input_id" value="'.$r->id.'">';
			}else{
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
			
			}
			$aksi       = '<div class="btn-group">';
			// $aksi .= '<button class="btn btn-xs btn-primary edit_tanggal" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			// $aksi .= '<button class="btn btn-xs btn-danger hapus_tanggal" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';

			$aksi.='</div>';
			$row[] = '';
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	function load_ko()
    {
		
		$idjenis_ko     		= $this->input->post('idjenis_ko');
		$idtipe_ko     		= $this->input->post('idtipe_ko');
		$nama_ko     		= $this->input->post('nama_ko');
		
		$where='';
		
		if ($idjenis_ko!='#'){
			$where .=" AND H.idjenis='$idjenis_ko'";
		}
		if ($idtipe_ko!='#'){
			$where .=" AND H.idtipe='$idtipe_ko'";
		}
		if ($nama_ko!=''){
			$where .=" AND H.nama LIKE '%".$nama_ko."%'";
		}
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		$from="(
				SELECT H.id,H.idtipe,H.idjenis,H.nama FROM mtarif_operasi H
				WHERE H.`status`='1'  AND H.id NOT IN(SELECT L.idlayanan FROM mbagi_hasil_layanan L WHERE L.`status`='1' AND L.table_id='9') ".$where."
			) as tbl  ORDER BY idjenis,nama ASC";

		// print_r($from);exit();
		$this->from   = $from;
		$this->order  = array();$this->group  = array();$this->column_search   = array('tipe_nama','nama');$this->column_order    = array();		
        $list = $this->datatable->get_datatables(true);$data = array();$no = $_POST['start'];
        foreach ($list as $r) {
            $no++;$row = array();
			$nama_tabel="mtarif_operasi";
            $row[] = $nama_tabel;
            $row[] = $r->id;//1
            $row[] = $r->id;//1
			// $row[] = $no;//2
            $row[] ='Kamar Operasi';
            // $row[] = $r->nama;
			 $row[] =  $r->nama;
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="js_adm"><span></span></label><input type="hidden" width="100%" class="input_js" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jp_adm"><span></span></label><input type="hidden" class="input_jp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bhp_adm"><span></span></label><input type="hidden" class="input_bhp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bp_adm"><span></span></label><input type="hidden" class="input_tabel" value="'.$nama_tabel.'"><input type="hidden" class="input_bp" value="0"><input type="hidden" class="input_tot" value="0"><input type="hidden" class="input_id" value="'.$r->id.'">';
			
			
			$aksi       = '<div class="btn-group">';
			// $aksi .= '<button class="btn btn-xs btn-primary edit_tanggal" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			// $aksi .= '<button class="btn btn-xs btn-danger hapus_tanggal" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';

			$aksi.='</div>';
			$row[] = '';
			$data[] = $row;
		}
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	function load_lab()
    {
		$idpath_lab     		= $this->input->post('idpath_lab');
		$panjang=strlen($idpath_lab);
		$idtipe_lab     		= $this->input->post('idtipe_lab');
		$nama_lab     		= $this->input->post('nama_lab');
		
		$where='';
		if ($idpath_lab!='#'){
			$where .=" AND left(H.path,".$panjang.")='$idpath_lab'";
		}
		if ($idtipe_lab!='#'){
			$where .=" AND H.idtipe='$idtipe_lab'";
		}
		if ($nama_lab!=''){
			$where .=" AND H.nama LIKE '%".$nama_lab."%'";
		}
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		$from="(SELECT H.id,H.idtipe,H.idkelompok,H.path,H.`level`,H.idpaket,H.nama FROM mtarif_laboratorium H
				WHERE H.`status`='1' AND  H.id NOT IN(SELECT L.idlayanan FROM mbagi_hasil_layanan L WHERE L.`status`='1' AND L.table_id='6') ".$where."
			) as tbl  ORDER BY path ASC";

		// print_r($from);exit();
		$this->from   = $from;
		$this->order  = array();$this->group  = array();$this->column_search   = array('tipe_nama','nama');$this->column_order    = array();		
        $list = $this->datatable->get_datatables(true);$data = array();$no = $_POST['start'];
        foreach ($list as $r) {
            $no++;$row = array();
			$nama_tabel="mtarif_laboratorium";
            $row[] = $nama_tabel;
            $row[] = $r->id;//1
			$row[] = $r->id;//2
            $row[] ='Laboratorium';
            // $row[] = $r->nama;
			 $row[] = TreeView($r->level, $r->nama);
			if ($r->idkelompok=='0'){
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="js_adm"><span></span></label><input type="hidden" width="100%" class="input_js" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jp_adm"><span></span></label><input type="hidden" class="input_jp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bhp_adm"><span></span></label><input type="hidden" class="input_bhp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bp_adm"><span></span></label><input type="hidden" class="input_tabel" value="'.$nama_tabel.'"><input type="hidden" class="input_bp" value="0"><input type="hidden" class="input_tot" value="0"><input type="hidden" class="input_id" value="'.$r->id.'">';
			}else{
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
			
			}
			$aksi       = '<div class="btn-group">';
			// $aksi .= '<button class="btn btn-xs btn-primary edit_tanggal" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			// $aksi .= '<button class="btn btn-xs btn-danger hapus_tanggal" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';

			$aksi.='</div>';
			$row[] = '';
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	function load_fis()
    {
		$idpath_fis     		= $this->input->post('idpath_fis');
		$panjang=strlen($idpath_fis);		
		$nama_fis     		= $this->input->post('nama_fis');
		
		$where='';
		if ($idpath_fis!='#'){
			$where .=" AND left(H.path,".$panjang.")='$idpath_fis'";
		}
		
		if ($nama_fis!=''){
			$where .=" AND H.nama LIKE '%".$nama_fis."%'";
		}
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		$from="(SELECT H.id,H.idkelompok,H.path,H.`level`,H.nama FROM mtarif_fisioterapi H
				WHERE H.`status`='1' AND  H.id NOT IN(SELECT L.idlayanan FROM mbagi_hasil_layanan L WHERE L.`status`='1' AND L.table_id='7') ".$where."
			) as tbl  ORDER BY path ASC";

		// print_r($from);exit();
		$this->from   = $from;
		$this->order  = array();$this->group  = array();$this->column_search   = array('tipe_nama','nama');$this->column_order    = array();		
        $list = $this->datatable->get_datatables(true);$data = array();$no = $_POST['start'];
        foreach ($list as $r) {
            $no++;$row = array();
			$nama_tabel="mtarif_fisioterapi";
            $row[] = $nama_tabel;
			$row[] = $no;//2
            $row[] = $r->id;//1
            $row[] ='Fisioterapi';
            // $row[] = $r->nama;
			 $row[] = TreeView($r->level, $r->nama);
			if ($r->idkelompok=='0'){
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="js_adm"><span></span></label><input type="hidden" width="100%" class="input_js" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jp_adm"><span></span></label><input type="hidden" class="input_jp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bhp_adm"><span></span></label><input type="hidden" class="input_bhp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bp_adm"><span></span></label><input type="hidden" class="input_tabel" value="'.$nama_tabel.'"><input type="hidden" class="input_bp" value="0"><input type="hidden" class="input_tot" value="0"><input type="hidden" class="input_id" value="'.$r->id.'">';
			}else{
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
			
			}
			$aksi       = '<div class="btn-group">';
			// $aksi .= '<button class="btn btn-xs btn-primary edit_tanggal" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			// $aksi .= '<button class="btn btn-xs btn-danger hapus_tanggal" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';

			$aksi.='</div>';
			$row[] = '';
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	function load_sewa_alat()
    {
		$nama_sewa_alat     		= $this->input->post('nama_sewa_alat');
		
		$where='';
		
		if ($nama_sewa_alat!=''){
			$where .=" AND H.nama LIKE '%".$nama_sewa_alat."%'";
		}
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		$from="(SELECT H.id,H.idkelompok,H.path,H.`level`,H.nama FROM mtarif_operasi_sewaalat H
				WHERE H.`status`='1' AND  H.id NOT IN(SELECT L.idlayanan FROM mbagi_hasil_layanan L WHERE L.`status`='1' AND L.table_id='8') ".$where."
			) as tbl  ORDER BY path ASC";

		// print_r($from);exit();
		$this->from   = $from;
		$this->order  = array();$this->group  = array();$this->column_search   = array('tipe_nama','nama');$this->column_order    = array();		
        $list = $this->datatable->get_datatables(true);$data = array();$no = $_POST['start'];
        foreach ($list as $r) {
            $no++;$row = array();
			$nama_tabel="mtarif_operasi_sewaalat";
            $row[] = $nama_tabel;
            $row[] = $r->id;//1
			$row[] = $r->id;//2
            $row[] ='Sewa Alat';
            // $row[] = $r->nama;
			 $row[] = TreeView($r->level, $r->nama);
			if ($r->idkelompok=='0'){
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="js_adm"><span></span></label><input type="hidden" width="100%" class="input_js" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jp_adm"><span></span></label><input type="hidden" class="input_jp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bhp_adm"><span></span></label><input type="hidden" class="input_bhp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bp_adm"><span></span></label><input type="hidden" class="input_tabel" value="'.$nama_tabel.'"><input type="hidden" class="input_bp" value="0"><input type="hidden" class="input_tot" value="0"><input type="hidden" class="input_id" value="'.$r->id.'">';
			}else{
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
			
			}
			$aksi       = '<div class="btn-group">';
			// $aksi .= '<button class="btn btn-xs btn-primary edit_tanggal" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			// $aksi .= '<button class="btn btn-xs btn-danger hapus_tanggal" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';

			$aksi.='</div>';
			$row[] = '';
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	function load_ranap()
    {
		
		$idtipe_ranap     		= $this->input->post('idtipe_ranap');
		$nama_ranap     		= $this->input->post('nama_ranap');
		$idruangan_ranap     		= $this->input->post('idruangan_ranap');
		$where='';
		
		if ($idtipe_ranap!='#'){
			$where .=" AND H.idtipe='$idtipe_ranap'";
		}
		if ($idruangan_ranap!='#'){
			$where .=" AND H.idruangan='$idruangan_ranap'";
		}
		if ($nama_ranap!=''){
			$where .=" AND H.nama LIKE '%".$nama_ranap."%'";
		}
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		$from="(SELECT H.id,H.idtipe,H.idruangan,H.idkelompok,H.nama,H.`level`,H.path FROM `mtarif_rawatinap` H
					WHERE H.`status`='1'  AND H.id NOT IN(SELECT L.idlayanan FROM mbagi_hasil_layanan L WHERE L.`status`='1' AND L.table_id='3') ".$where." 
			) as tbl  ORDER BY path ASC";

		// print_r($from);exit();
		$this->from   = $from;
		$this->order  = array();$this->group  = array();$this->column_search   = array('tipe_nama','nama');$this->column_order    = array();		
        $list = $this->datatable->get_datatables(true);$data = array();$no = $_POST['start'];
        foreach ($list as $r) {
            $no++;$row = array();
			$nama_tabel="mtarif_rawatinap";
            $row[] = $nama_tabel;
            $row[] = $r->id;//1
            $row[] = $r->id;//1
			// $row[] = $no;//2
            $row[] ='Rawat Inap';
            // $row[] = $r->nama;
			$row[] = TreeView($r->level, $r->nama);
			if ($r->idkelompok=='0'){
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="js_adm"><span></span></label><input type="hidden" width="100%" class="input_js" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jp_adm"><span></span></label><input type="hidden" class="input_jp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bhp_adm"><span></span></label><input type="hidden" class="input_bhp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bp_adm"><span></span></label><input type="hidden" class="input_tabel" value="'.$nama_tabel.'"><input type="hidden" class="input_bp" value="0"><input type="hidden" class="input_tot" value="0"><input type="hidden" class="input_id" value="'.$r->id.'">';
			}else{
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
				$row[] = '<i class="si si-close fa-2x text-danger"></i>';
			
			}
			$aksi       = '<div class="btn-group">';
			// $aksi .= '<button class="btn btn-xs btn-primary edit_tanggal" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			// $aksi .= '<button class="btn btn-xs btn-danger hapus_tanggal" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';

			$aksi.='</div>';
			$row[] = '';
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	function load_ranap_visite()
    {
		
		$nama_ranap_visite     		= $this->input->post('nama_ranap_visite');
		$idruangan_ranap_visite     		= $this->input->post('idruangan_ranap_visite');
		
		$where='';
		
		// if ($idtipe_ranap!='#'){
			// $where .=" AND H.idtipe='$idtipe_ranap'";
		// }
		if ($idruangan_ranap_visite!='#'){
			$where .=" AND H.idruangan='$idruangan_ranap_visite'";
		}
		if ($nama_ranap_visite!=''){
			$where .=" AND H.nama LIKE '%".$nama_ranap_visite."%'";
		}
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		$from="(
				SELECT H.id,H.idruangan,H.nama FROM mtarif_visitedokter H
				WHERE H.`status`='1'  AND H.id NOT IN(SELECT L.idlayanan FROM mbagi_hasil_layanan L WHERE L.`status`='1' AND L.table_id='4') ".$where." 
			) as tbl  ORDER BY nama ASC";

		// print_r($from);exit();
		$this->from   = $from;
		$this->order  = array();$this->group  = array();$this->column_search   = array('tipe_nama','nama');$this->column_order    = array();		
        $list = $this->datatable->get_datatables(true);$data = array();$no = $_POST['start'];
        foreach ($list as $r) {
            $no++;$row = array();
			$nama_tabel="mtarif_visitedokter";
            $row[] = $nama_tabel;
            $row[] = $r->id;//1
            $row[] = $r->id;//1
			// $row[] = $no;//2
            $row[] ='Visit Dokter';
            // $row[] = $r->nama;
			$row[] = $r->nama;
			// if ($r->idkelompok=='0'){
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="js_adm"><span></span></label><input type="hidden" width="100%" class="input_js" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="jp_adm"><span></span></label><input type="hidden" class="input_jp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bhp_adm"><span></span></label><input type="hidden" class="input_bhp" value="0">';
				$row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" class="bp_adm"><span></span></label><input type="hidden" class="input_tabel" value="'.$nama_tabel.'"><input type="hidden" class="input_bp" value="0"><input type="hidden" class="input_tot" value="0"><input type="hidden" class="input_id" value="'.$r->id.'">';
			// }else{
				// $row[] = '<i class="si si-close fa-2x text-danger"></i>';
				// $row[] = '<i class="si si-close fa-2x text-danger"></i>';
				// $row[] = '<i class="si si-close fa-2x text-danger"></i>';
				// $row[] = '<i class="si si-close fa-2x text-danger"></i>';
			
			// }
			$aksi       = '<div class="btn-group">';
			// $aksi .= '<button class="btn btn-xs btn-primary edit_tanggal" type="button"  title="Edit"><i class="fa fa-pencil"></i></button>';
			// $aksi .= '<button class="btn btn-xs btn-danger hapus_tanggal" type="button"  title="Hapus"><i class="fa fa-trash-o"></i></button>';

			$aksi.='</div>';
			$row[] = '';
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	function load_pelayanan()
    {
		$mbagi_hasil_id     		= $this->input->post('mbagi_hasil_id');
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		$from="(
				SELECT 
				CASE WHEN H.table_id='1' THEN adm.nama 
				WHEN H.table_id='2' THEN rj.nama 
				WHEN H.table_id='3' THEN ri.nama 
				WHEN H.table_id='4' THEN riv.nama 
				WHEN H.table_id='5' THEN CASE WHEN rad.idtipe = 1 AND rad.idkelompok = 0 THEN
										  CONCAT(rad.nama,  ' (', E.nama, F.nama, ')')
										ELSE
										  rad.nama
										END 
				WHEN H.table_id='6' THEN lab.nama 
				WHEN H.table_id='7' THEN fis.nama 
				WHEN H.table_id='8' THEN sa.nama 
				WHEN H.table_id='9' THEN ko.nama 
				ELSE 'NOT FOUDN' END as nama_layanan
				,ref.nama_tabel as nama_tipe
				,H.id as iddet,H.mbagi_hasil_id,H.tabel,H.table_id,H.idlayanan,H.jasasarana,H.jasapelayanan,H.bhp,H.biayaperawatan
				FROM `mbagi_hasil_layanan` H
				LEFT JOIN mbagi_hasil_tabel ref ON ref.id=H.table_id
				LEFT JOIN mtarif_administrasi adm ON adm.id=H.idlayanan AND H.table_id='1'
				LEFT JOIN mtarif_rawatjalan rj ON rj.id=H.idlayanan AND H.table_id='2'
				LEFT JOIN mtarif_rawatinap ri ON ri.id=H.idlayanan AND H.table_id='3'
				LEFT JOIN mtarif_visitedokter riv ON riv.id=H.idlayanan AND H.table_id='4'
				LEFT JOIN mtarif_radiologi rad ON rad.id=H.idlayanan AND H.table_id='5'
				LEFT JOIN mtarif_radiologi_film F ON F.id=rad.idfilm
				LEFT JOIN mtarif_radiologi_expose E ON E.id=rad.idexpose
				LEFT JOIN mtarif_laboratorium lab ON lab.id=H.idlayanan AND H.table_id='6'
				LEFT JOIN mtarif_fisioterapi fis ON fis.id=H.idlayanan AND H.table_id='7'
				LEFT JOIN mtarif_operasi_sewaalat sa ON sa.id=H.idlayanan AND H.table_id='8'
				LEFT JOIN mtarif_operasi ko ON ko.id=H.idlayanan AND H.table_id='9'
				WHERE H.`status`='1' AND H.mbagi_hasil_id='$mbagi_hasil_id'
				ORDER BY H.table_id ASC
			) as tbl  ORDER BY table_id,nama_layanan ASC";

		// print_r($from);exit();
		$this->from   = $from;
		$this->order  = array();$this->group  = array();$this->column_search   = array();$this->column_order    = array();		
        $list = $this->datatable->get_datatables(true);$data = array();$no = $_POST['start'];
        foreach ($list as $r) {
            $no++;$row = array();
            $row[] = $r->iddet;
            $row[] = $r->table_id;
			$row[] = $no;//2
            $row[] = $r->nama_tipe;
            $row[] = $r->nama_layanan;
            $row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" '.($r->jasasarana=='1'?'checked':'').' class="js_check"><span></span></label>';
            $row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" '.($r->jasapelayanan=='1'?'checked':'').' class="jp_check"><span></span></label>';
            $row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" '.($r->bhp=='1'?'checked':'').' class="bhp_check"><span></span></label>';
            $row[] = '<label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" '.($r->biayaperawatan=='1'?'checked':'').' class="bp_check"><span></span></label>';
			
			$aksi       = '<div class="btn-group">';
			$aksi .= '<button class="btn btn-xs btn-danger" type="button"  title="Hapus" onclick="hapus_layanan('.$r->iddet.')"><i class="fa fa-trash-o"></i></button>';

			$aksi.='</div>';
			$row[] = $aksi;
			$data[] = $row;
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	//TEST
	public function updateStatus($id, $tipe, $status)
    {
        // $this->model->updateStatus($id, $tipe, $status);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'Status Verifikasi berhasil dirubah.');
        if ($tipe == 'deposit') {
            redirect('tverifikasi_transaksi/index_deposit', 'location');
        } else {
            if ($tipe == 'poliklinik' && $status == '1') {
                $this->db->select("tpoliklinik_pendaftaran.id AS idpendaftaran,
                  tpoliklinik_tindakan.id AS idtindakan, tkasir.id AS idkasir,
                  IF(tpoliklinik_pendaftaran.idtipepasien = 1, 'Pasien RS', 'Pasien Pribadi') AS jenispasien,
                  tpoliklinik_pendaftaran.idkelompokpasien AS idkelompok,
                  mpasien_kelompok.nama AS namakelompok,
                  tpoliklinik_pendaftaran.idrekanan,
                  mrekanan.nama AS namarekanan,
                  tpoliklinik_pendaftaran.tanggaldaftar AS tanggal_pemeriksaan");
                $this->db->join("tpoliklinik_tindakan", "tpoliklinik_tindakan.id = tkasir.idtindakan AND idtipe IN (1,2)");
                $this->db->join("tpoliklinik_pendaftaran", "tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran");
                $this->db->join("mpasien_kelompok", "mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien", "LEFT");
                $this->db->join("mrekanan", "mrekanan.id = tpoliklinik_pendaftaran.idrekanan", "LEFT");
                $this->db->where("tkasir.id", $id);
                $this->db->where("tkasir.status", 2);
                $this->db->limit(1);
                $query = $this->db->get("tkasir");
                $row = $query->row();

                $dataPendaftaran = array(
                  'tanggal_pemeriksaan' => $row->tanggal_pemeriksaan,
                  'idpendaftaran' => $row->idpendaftaran,
                  'jenis_pasien' => $row->jenispasien,
                  'idkelompok' => $row->idkelompok,
                  'namakelompok' => $row->namakelompok,
                  'idrekanan' => $row->idrekanan,
                  'namarekanan' => $row->namarekanan,
                  'status_asuransi' => ($row->idkelompok != 5 ? 1 : 0),
                );

                if ($row->idpendaftaran) {
                    $this->Mbagi_hasil_model->generateTransaksiHonorDokterRawatJalan($dataPendaftaran);
                }
            } elseif ($tipe == 'rawatinap' && $status == '1') {
                $this->db->select("trawatinap_pendaftaran.id AS idpendaftaran,
                	trawatinap_pendaftaran.idtipepasien,
                	IF(trawatinap_pendaftaran.idtipepasien = 1, 'Pasien RS', 'Pasien Pribadi') AS jenispasien,
                	trawatinap_pendaftaran.idkelompokpasien AS idkelompok,
                	mpasien_kelompok.nama AS namakelompok,
                	trawatinap_pendaftaran.idrekanan,
                	mrekanan.nama AS namarekanan,
                	trawatinap_pendaftaran.tanggaldaftar AS tanggal_pemeriksaan,
                  mdokterperujuk.idkategori AS idkategori_perujuk,
                  mdokterperujuk_kategori.nama AS namakategori_perujuk,
                  mdokterperujuk.id AS iddokter_perujuk,
                  mdokterperujuk.nama AS namadokter_perujuk,
                  mdokterperujuk.pajakranap AS pajak_perujuk,
                  mdokterperujuk.potonganrsranap AS potonganrs_perujuk,
                  tpoliklinik_pendaftaran.idtipe AS idasalpasien,
                  tpoliklinik_pendaftaran.idpoliklinik AS idpoliklinik");
                $this->db->join("trawatinap_pendaftaran", "trawatinap_pendaftaran.id = trawatinap_tindakan_pembayaran.idtindakan");
                $this->db->join("tpoliklinik_pendaftaran", "tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik");
                $this->db->join("mpasien_kelompok", "mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien", "LEFT");
                $this->db->join("mrekanan", "mrekanan.id = trawatinap_pendaftaran.idrekanan", "LEFT");
                $this->db->join('mdokter mdokterperujuk', 'mdokterperujuk.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
                $this->db->join('mdokter_kategori mdokterperujuk_kategori', 'mdokterperujuk_kategori.id = mdokterperujuk.idkategori', 'LEFT');
                $this->db->where("trawatinap_tindakan_pembayaran.id", $id);
                $this->db->where("trawatinap_tindakan_pembayaran.statusbatal", 0);
                $this->db->limit(1);
                $query = $this->db->get("trawatinap_tindakan_pembayaran");
                $row = $query->row();

                // Get Status Pembayaran Kasir Rawat Jalan
                $statusKasirRajal = $this->Trawatinap_tindakan_model->getDataPoliTindakan($row->idpendaftaran)->status_kasir;

                $dataPendaftaran = array(
                  'tanggal_pemeriksaan' => $row->tanggal_pemeriksaan,
                  'idpendaftaran' => $row->idpendaftaran,
                  'idtipepasien' => $row->idtipepasien,
                  'idasalpasien' => $row->idasalpasien,
                  'idpoliklinik' => $row->idpoliklinik,
                  'jenis_pasien' => $row->jenispasien,
                  'idkelompok' => $row->idkelompok,
                  'namakelompok' => $row->namakelompok,
                  'idrekanan' => $row->idrekanan,
                  'namarekanan' => $row->namarekanan,
                  'idkategori_perujuk' => $row->idkategori_perujuk,
                  'namakategori_perujuk' => $row->namakategori_perujuk,
                  'iddokter_perujuk' => $row->iddokter_perujuk,
                  'namadokter_perujuk' => $row->namadokter_perujuk,
                  'potonganrs_perujuk' => $row->potonganrs_perujuk,
                  'pajak_perujuk' => $row->pajak_perujuk,
                  'status_asuransi' => ($row->idkelompok != 5 ? 1 : 0),
                  'status_kasir_rajal' => $statusKasirRajal,
                );

                if ($row->idpendaftaran) {
                    $this->Mbagi_hasil_model->generateTransaksiHonorDokterRawatInap($dataPendaftaran);
                }
            }

            $tipe = ($tipe == 'obatbebas' ? 'poliklinik' : $tipe);
            redirect('tverifikasi_transaksi/index/'.$tipe, 'location');
        }
    }
	
}
