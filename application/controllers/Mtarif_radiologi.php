<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mtarif_radiologi extends CI_Controller
{
    /**
     * Tarif Radiologi controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mtarif_radiologi_model');
        $this->load->model('Mtarif_administrasi_model');
    }

    public function index($idtipe = '1', $idparent = '0', $idsubparent = '-'): void
    {
        $data = [
            'idtipe' => $idtipe,
            'idparent' => $idparent,
            'idsubparent' => $idsubparent,
        ];

        $data['error'] = '';
        $data['idtipe'] = $idtipe;
        $data['title'] = 'Tarif Radiologi';
        $data['content'] = 'Mtarif_radiologi/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Tarif Radiologi', 'mtarif_radiologi/index'],
            ['List', '#'],
        ];

        $data['list_parent'] = $this->Mtarif_radiologi_model->find_index_parent($idtipe);
        $data['list_subparent'] = $this->Mtarif_radiologi_model->find_index_subparent($idparent);

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create($idtipe = '1', $level0 = '0', $headerpath = '0'): void
    {
        $data = [
            'id' => '',
            'kode' => '',
            'nama' => '',
            'nama_english' => '',
            'idexpose' => '',
            'idfilm' => '',
            'harga' => '',
            'idtipe' => $idtipe,
            'idkelompok' => '1',
            'level0' => $level0,
            'old_level0' => '',
            'headerpath' => $headerpath,
            'old_headerpath' => '',
            'path' => '',
            'level' => '',
            'status' => '',
        ];

        $data['error'] = '';
        $data['title'] = 'Tambah Tarif Radiologi';
        $data['content'] = 'Mtarif_radiologi/manage';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Tarif Radiologi', 'mtarif_radiologi/index'],
            ['Tambah', 'mtarif_radiologi'],
        ];

        $data['list_level0'] = $this->Mtarif_radiologi_model->find_update_parent($idtipe);
        $data['list_parent'] = $this->Mtarif_radiologi_model->find_update_subparent($level0);

        $data['list_expose'] = $this->Mtarif_radiologi_model->getAllExpose();
        $data['list_film'] = $this->Mtarif_radiologi_model->getAllFilm();

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($uri, $id): void
    {
        if ('' !== $id) {
            $row = $this->Mtarif_radiologi_model->getSpecified($id);
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'kode' => $row->kode,
                    'nama' => $row->nama,
                    'nama_english' => $row->nama_english,
                    'idexpose' => $row->idexpose,
                    'idfilm' => $row->idfilm,
                    'idkelompok' => $row->idkelompok,
                    'idtipe' => $row->idtipe,
                    'level0' => $row->level0,
                    'old_level0' => $row->level0,
                    'headerpath' => $row->headerpath,
                    'old_headerpath' => $row->headerpath,
                    'path' => $row->path,
                    'level' => $row->level,
                    'status' => $row->status,
                ];

                $data['error'] = '';
                $data['title'] = 'Ubah Tarif Radiologi';
                $data['content'] = 'Mtarif_radiologi/manage';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Tarif Radiologi', 'mtarif_radiologi/index'],
                    ['Ubah', 'mtarif_radiologi'],
                ];

                $level0 = $row->level0 ? $row->level0 : getLevelZero($row->headerpath);
                $data['list_level0'] = $this->Mtarif_radiologi_model->find_update_parent($row->idtipe);
                $data['list_parent'] = $this->Mtarif_radiologi_model->find_update_subparent($level0);
                $data['list_expose'] = $this->Mtarif_radiologi_model->getAllExpose();
                $data['list_film'] = $this->Mtarif_radiologi_model->getAllFilm();
                $data['list_detailtarif'] = $this->Mtarif_radiologi_model->getDetailTarif($row->id);

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mtarif_radiologi/index/'.$uri, 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mtarif_radiologi/index/'.$uri, 'location');
        }
    }

    public function delete($uri, $id): void
    {
        $this->Mtarif_radiologi_model->softDelete($id);
        $this->session->set_flashdata('confirm', false);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mtarif_radiologi/index/'.$uri, 'location');
    }

    public function save(): void
    {
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');

        if (true == $this->form_validation->run()) {
            if ('' == $this->input->post('id')) {
                if ($this->Mtarif_radiologi_model->saveData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mtarif_radiologi/index/'.$this->input->post('idtipe'), 'location');
                }
            } else {
                if ($this->Mtarif_radiologi_model->updateData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mtarif_radiologi/index/'.$this->input->post('idtipe'), 'location');
                }
            }
        } else {
            $this->failed_save($this->input->post('id'));
        }
    }

    public function failed_save($id): void
    {
        $data = $this->input->post();
        $data['error'] = validation_errors();
        $data['content'] = 'Mtarif_radiologi/manage';

        $data['list_parent'] = $this->Mtarif_radiologi_model->getAllParent($this->input->post('idtipe'));
        $data['list_expose'] = $this->Mtarif_radiologi_model->getAllExpose();
        $data['list_film'] = $this->Mtarif_radiologi_model->getAllFilm();

        if ('' == $id) {
            $data['title'] = 'Tambah Satuan';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Tarif Radiologi', 'mtarif_radiologi/index'],
                ['Tambah', 'mtarif_radiologi'],
            ];
        } else {
            $data['title'] = 'Ubah Satuan';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Tarif Radiologi', 'mtarif_radiologi/index'],
                ['Ubah', 'mtarif_radiologi'],
            ];
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function get_parent($idtipe): void
    {
        $arr = $this->Mtarif_radiologi_model->getParent($idtipe);
        $this->output->set_output(json_encode($arr));
    }

    public function get_child_level($headerpath): void
    {
        $arr = $this->Mtarif_radiologi_model->getPathLevel($headerpath);
        $this->output->set_output(json_encode($arr));
    }

    public function getIndex($idtipe, $idparent, $idsubparent): void
    {
        $data_user = get_acces();
        $user_acces_form = $data_user['user_acces_form'];

        $this->select = ['mtarif_radiologi.*',
        '(CASE WHEN mtarif_radiologi.idtipe = 1 AND mtarif_radiologi.idkelompok = 0 THEN
            CONCAT(mtarif_radiologi.nama,  " (", mtarif_radiologi_expose.nama, mtarif_radiologi_film.nama, ")")
        ELSE
            mtarif_radiologi.nama
        END) AS namatarif', ];
        $this->from = 'mtarif_radiologi';
        $this->join = [
            ['mtarif_radiologi_expose', 'mtarif_radiologi_expose.id = mtarif_radiologi.idexpose', 'LEFT'],
            ['mtarif_radiologi_film', 'mtarif_radiologi_film.id = mtarif_radiologi.idfilm', 'LEFT'],
        ];

        $whereArray = [
            'mtarif_radiologi.status' => '1',
        ];
        $orWhereArray = [];

        if ($idtipe != '0') {
            $whereArray = array_merge($whereArray, [
                'mtarif_radiologi.idtipe' => $idtipe,
            ]);
        }

        if ($idsubparent == '-') {
            if ($idparent != '0') {
                $whereArray = array_merge($whereArray, [
                    'left(mtarif_radiologi.path,' . strlen($idparent) . ')' => $idparent,
                ]);
            }
        } else {
            $idsubparentArray = explode("_", $idsubparent);
            foreach ($idsubparentArray as $val) {
                $orWhereArray[] = array("LEFT(mtarif_radiologi.path, " . strlen($val . '.') . ")" => $val . '.');
            }
        }

        $this->where = $whereArray;
        $this->or_where = $orWhereArray;

        $this->order = [
            'mtarif_radiologi.path' => 'ASC',
        ];

        $this->group = [];

        $this->column_search = ['mtarif_radiologi.nama'];
        $this->column_order = ['mtarif_radiologi.nama'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = TreeView($r->level, $r->namatarif);
            $aksi = '<div class="btn-group">';
            if (UserAccesForm($user_acces_form, ['151'])) {
                $aksi .= '<a href="'.site_url().'mtarif_radiologi/update/'.$idtipe.'/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
            }
            if (UserAccesForm($user_acces_form, ['152'])) {
                $aksi .= '<a href="#" data-urlindex="'.site_url().'mtarif_radiologi/index/'.$idtipe.'" data-urlremove="'.site_url().'mtarif_radiologi/delete/'.$idtipe.'/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
            }
            if (0 == $r->idkelompok) {
                $aksi .= '<a href="'.site_url().'mtarif_radiologi/setting/'.$idtipe.'/'.$r->id.'" data-toggle="tooltip" title="Setting" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>';
            }
            $aksi .= '<a href="'.site_url().'mtarif_radiologi/create/'.($r->idtipe ? $r->idtipe : 0).'/'.($r->level0 ? $r->level0 : 0).'/'.($r->headerpath ? $r->headerpath : 0).'" data-toggle="tooltip" title="Tambah Tarif Child" class="btn btn-warning btn-sm"><i class="fa fa-plus"></i></a>';
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];

        echo json_encode($output);
    }

    public function filter(): void
    {
        if ('' !== $this->input->post('idtipe')) {
            $idtipe = $this->input->post('idtipe');
            $idpath = $this->input->post('idpath');
            redirect("mtarif_radiologi/index/{$idtipe}/{$idpath}", 'location');
        } else {
            redirect('mtarif_radiologi', 'location');
        }
    }

    public function find_headparent($idtipe): void
    {
        $arr['detail'] = $this->Mtarif_radiologi_model->find_headparent($idtipe);
        $this->output->set_output(json_encode($arr));
    }

    public function setting($uri, $id): void
    {
        if ('' !== $id) {
            $row = $this->Mtarif_radiologi_model->getSpecified($id);
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'idtipe' => $row->idtipe,
                    'kode' => $row->kode,
                    'nama' => $row->nama,
                    'nama_english' => $row->nama_english,
                    'idexpose' => $row->idexpose,
                    'idfilm' => $row->idfilm,
                    'idkelompok' => $row->idkelompok,
                    'level0' => $row->level0,
                    'old_level0' => $row->level0,
                    'headerpath' => $row->headerpath,
                    'old_headerpath' => $row->headerpath,
                    'path' => $row->path,
                    'level' => $row->level,
                    'status' => $row->status,
                    'group_diskon_all' => $row->group_diskon_all,
                ];

                $data['error'] = '';
                $data['title'] = 'Setting Group Pembayaran Tarif Radiologi';
                $data['content'] = 'Mtarif_radiologi/setting';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Tarif Radiologi', 'mtarif_radiologi/index'],
                    ['Setting', 'mtarif_radiologi'],
                ];

                $data['list_level0'] = $this->Mtarif_radiologi_model->find_update_parent($row->idtipe);
                $data['list_expose'] = $this->Mtarif_radiologi_model->getAllExpose();
                $data['list_film'] = $this->Mtarif_radiologi_model->getAllFilm();
                $data['list_detailtarif'] = $this->Mtarif_radiologi_model->getDetailTarif($row->id);
                $data['list_group_pembayaran'] = $this->Mtarif_administrasi_model->getGroupPembayaran();

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mtarif_radiologi/index/'.$uri, 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mtarif_radiologi/index/'.$uri, 'location');
        }
    }

    public function save_setting(): void
    {
        if ($this->Mtarif_radiologi_model->updateSetting()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'data telah disimpan.');
            redirect('mtarif_radiologi/index/'.$this->input->post('idtipe'), 'location');
        }
    }
    
    // # Update Document ERM
    public function find_index_parent($idtipe): void
    {
        $data = $this->Mtarif_radiologi_model->find_index_parent($idtipe);

        $arr = '';
        foreach ($data as $row) {
            $arr = $arr.'<option value="'.$row->path.'">'.TreeView($row->level, $row->nama).'</option>';
        }

        $this->output->set_output(json_encode($arr));
    }

    public function find_index_subparent($idparent): void
    {
        $data = $this->Mtarif_radiologi_model->find_index_subparent($idparent);

        $arr = '';
        foreach ($data as $row) {
            $arr = $arr.'<option value="'.$row->path.'">'.TreeView($row->level, $row->nama).'</option>';
        }

        $this->output->set_output(json_encode($arr));
    }

    public function find_manage_parent($idtipe): void
    {
        $arr = $this->Mtarif_radiologi_model->find_manage_parent($idtipe);
        $this->output->set_output(json_encode($arr));
    }

    public function find_manage_subparent($idparent): void
    {
        $arr = $this->Mtarif_radiologi_model->find_manage_subparent($idparent);
        $this->output->set_output(json_encode($arr));
    }
}
