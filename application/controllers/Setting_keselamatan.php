<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_keselamatan extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Setting_keselamatan_model');
		$this->load->helper('path');
		
  }


	function index($tab='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('2231'))){
				$tab='1';
			}elseif (UserAccesForm($user_acces_form,array('2233'))){
				$tab='2';
			}
		}
		if (UserAccesForm($user_acces_form,array('2231','2233'))){
			$data = $this->Setting_keselamatan_model->get_assesmen_setting();
			// $data_label = $this->Setting_keselamatan_model->get_assesmen_label();
			// $data_user = $this->Setting_keselamatan_model->get_assesmen_user();
			// print_r($data_spesifik);exit;
			$data['tab'] 			= $tab;
			$data['spesialisasi_id'] 			= '#';
			$data['profesi_id'] 			= '#';
			$data['jenis_isi'] 			= '0';
			$data['mppa_id'] 			= '#';
			$data['st_lihat'] 			= '0';
			$data['st_input'] 			= '0';
			$data['st_edit'] 			= '0';
			$data['st_hapus'] 			= '0';
			$data['st_cetak'] 			= '0';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Cheklist Setting_keselamatan';
			$data['content'] 		= 'Setting_keselamatan/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("Pengkajian Cheklist Setting_keselamatan Setting",'setting_keselamatan/index')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function load_hak_akses()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.id,H.profesi_id,P.ref as profesi
							,H.spesialisasi_id,CASE WHEN H.spesialisasi_id=0 THEN 'SEMUA' ELSE S.ref END as spesialisasi 
							,H.mppa_id,M.nama as nama_ppa,H.st_lihat,H.st_input,H.st_edit,H.st_hapus,H.st_cetak
							FROM `setting_keselamatan_akses` H
							LEFT JOIN merm_referensi P ON P.nilai=H.profesi_id AND P.ref_head_id='21' AND P.`status`='1'
							LEFT JOIN merm_referensi S ON S.nilai=H.spesialisasi_id AND S.ref_head_id='22'  AND S.`status`='1'
							LEFT JOIN mppa M ON M.id=H.mppa_id AND M.staktif='1'
							ORDER BY H.profesi_id,H.spesialisasi_id



					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_ppa','spesialisasi','profesi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->profesi);
          $result[] = ($r->spesialisasi_id=='0'?text_default($r->spesialisasi):$r->spesialisasi);
          $result[] = ($r->mppa_id=='0'?text_default('SEMUA'):$r->nama_ppa);
          $result[] = ($r->st_lihat?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_input?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_edit?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_hapus?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_cetak?'IZINKAN':text_danger('TIDAK'));
		  // if (UserAccesForm($user_acces_form,array('1603'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_hak_akses('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  // }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function save_assesmen(){
		if ($this->Setting_keselamatan_model->save_assesmen()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_keselamatan/index/1','location');
		}
	}
	
	
  function find_mppa($profesi_id){
	  $q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='$profesi_id' AND H.staktif='1'";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($profesi_id!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
  //content
  function load_content(){
		$tabel_1='';
		$q="SELECT *FROM (
				SELECT 
				LPAD(H.`no`,3,'0') as  nourut
				,H.*,C.`name` as user_created,E.`name` as user_edited,GROUP_CONCAT(R.ref SEPARATOR ' / ') as ref FROM setting_keselamatan_isi H
							LEFT JOIN musers C ON C.id=H.created_by
							LEFT JOIN musers E ON E.id=H.edited_by
							LEFT JOIN setting_keselamatan_isi_jawaban M ON M.general_isi_id=H.id
							LEFT JOIN merm_referensi R ON R.id=M.ref_id
							WHERE H.`status`='1' AND H.header_id=0 AND H.jenis_header='1'
							
							GROUP BY H.id
							
							UNION ALL
							
							SELECT 
				CONCAT(LPAD(HH.`no`,3,'0'),'-',LPAD(H.`no`,3,'0')) as  nourut
				,H.*,C.`name` as user_created,E.`name` as user_edited,GROUP_CONCAT(R.ref SEPARATOR ' / ') as ref FROM setting_keselamatan_isi H
							LEFT JOIN musers C ON C.id=H.created_by
							LEFT JOIN musers E ON E.id=H.edited_by
							LEFT JOIN setting_keselamatan_isi_jawaban M ON M.general_isi_id=H.id
							LEFT JOIN merm_referensi R ON R.id=M.ref_id
							LEFT JOIN setting_keselamatan_isi HH ON HH.id=H.header_id
							WHERE H.`status`='1' AND H.header_id !=0 AND H.jenis_header='1'
							
							GROUP BY H.id
							
			) T ORDER BY T.nourut ASC";
		$hasil=$this->db->query($q)->result();
		$no=1;
		foreach($hasil as $r){
			$tabel_1 .='<tr>';
			$tabel_1 .='<td class="text-center '.($r->header_id=='0'?'bg-info':'').'">'.$r->no.'</td>';
			$tabel_1 .='<td class="'.($r->header_id=='0'?'bg-info':'').'">'.$r->isi.'</td>';
			$tabel_1 .='<td class="hidden-xs '.($r->header_id=='0'?'bg-info':'').'">'.($r->jenis_isi=='1'?text_danger(jenis_isi($r->jenis_isi)).'<br>'.$r->ref:text_primary(jenis_isi($r->jenis_isi))).'</td>';
			$tabel_1 .='<td class="hidden-xs '.($r->header_id=='0'?'bg-info':'').'">'.$r->user_created.'<br>'.($r->created_date?HumanDateLong($r->created_date):'').'</td>';
			$tabel_1 .='<td class="text-center '.($r->header_id=='0'?'bg-info':'').'">';
			$tabel_1 .='					<div class="btn-group">';
			if ($r->header_id=='0'){
				$tabel_1 .='						<button class="btn btn-xs btn-primary" type="button" onclick="add_detail('.$r->id.','.$r->jenis_header.')" data-toggle="tooltip" title="Tambah" data-original-title="Edit"><i class="fa fa-plus"></i></button>';
			}
			
			$tabel_1 .='						<button class="btn btn-xs btn-success" type="button" onclick="edit_content('.$r->id.','.$r->header_id.','.$r->jenis_header.')" data-toggle="tooltip" title="Edit" data-original-title="Tambah"><i class="fa fa-pencil"></i></button>';
			$tabel_1 .='						<button class="btn btn-xs btn-danger" type="button" onclick="hapus('.$r->id.')" data-toggle="tooltip" title="" data-original-title="Remove "><i class="fa fa-times"></i></button>';
			$tabel_1 .='					</div>';
			$tabel_1 .='				</td>';
			$tabel_1 .='</tr>';
			$no=$no+1;
		}
		$data['tabel_1']=$tabel_1;
		
		$tabel_2='';
		$q="SELECT *FROM (
				SELECT 
				LPAD(H.`no`,3,'0') as  nourut
				,H.*,C.`name` as user_created,E.`name` as user_edited,GROUP_CONCAT(R.ref SEPARATOR ' / ') as ref FROM setting_keselamatan_isi H
							LEFT JOIN musers C ON C.id=H.created_by
							LEFT JOIN musers E ON E.id=H.edited_by
							LEFT JOIN setting_keselamatan_isi_jawaban M ON M.general_isi_id=H.id
							LEFT JOIN merm_referensi R ON R.id=M.ref_id
							WHERE H.`status`='1' AND H.header_id=0 AND H.jenis_header='2'
							
							GROUP BY H.id
							
							UNION ALL
							
							SELECT 
				CONCAT(LPAD(HH.`no`,3,'0'),'-',LPAD(H.`no`,3,'0')) as  nourut
				,H.*,C.`name` as user_created,E.`name` as user_edited,GROUP_CONCAT(R.ref SEPARATOR ' / ') as ref FROM setting_keselamatan_isi H
							LEFT JOIN musers C ON C.id=H.created_by
							LEFT JOIN musers E ON E.id=H.edited_by
							LEFT JOIN setting_keselamatan_isi_jawaban M ON M.general_isi_id=H.id
							LEFT JOIN merm_referensi R ON R.id=M.ref_id
							LEFT JOIN setting_keselamatan_isi HH ON HH.id=H.header_id
							WHERE H.`status`='1' AND H.header_id !=0 AND H.jenis_header='2'
							
							GROUP BY H.id
							
			) T ORDER BY T.nourut ASC";
		$hasil=$this->db->query($q)->result();
		$no=1;
		foreach($hasil as $r){
			$tabel_2 .='<tr>';
			$tabel_2 .='<td class="text-center '.($r->header_id=='0'?'bg-info':'').'">'.$r->no.'</td>';
			$tabel_2 .='<td class="'.($r->header_id=='0'?'bg-info':'').'">'.$r->isi.'</td>';
			$tabel_2 .='<td class="hidden-xs '.($r->header_id=='0'?'bg-info':'').'">'.($r->jenis_isi=='1'?text_danger(jenis_isi($r->jenis_isi)).'<br>'.$r->ref:text_primary(jenis_isi($r->jenis_isi))).'</td>';
			$tabel_2 .='<td class="hidden-xs '.($r->header_id=='0'?'bg-info':'').'">'.$r->user_created.'<br>'.($r->created_date?HumanDateLong($r->created_date):'').'</td>';
			$tabel_2 .='<td class="text-center '.($r->header_id=='0'?'bg-info':'').'">';
			$tabel_2 .='					<div class="btn-group">';
			if ($r->header_id=='0'){
				$tabel_2 .='						<button class="btn btn-xs btn-primary" type="button" onclick="add_detail('.$r->id.','.$r->jenis_header.')" data-toggle="tooltip" title="Tambah" data-original-title="Edit"><i class="fa fa-plus"></i></button>';
			}
			
			$tabel_2 .='						<button class="btn btn-xs btn-success" type="button" onclick="edit_content('.$r->id.','.$r->header_id.','.$r->jenis_header.')" data-toggle="tooltip" title="Edit" data-original-title="Tambah"><i class="fa fa-pencil"></i></button>';
			$tabel_2 .='						<button class="btn btn-xs btn-danger" type="button" onclick="hapus('.$r->id.')" data-toggle="tooltip" title="" data-original-title="Remove "><i class="fa fa-times"></i></button>';
			$tabel_2 .='					</div>';
			$tabel_2 .='				</td>';
			$tabel_2 .='</tr>';
			$no=$no+1;
		}
		
		$tabel_3='';
		$q="SELECT *FROM (
				SELECT 
				LPAD(H.`no`,3,'0') as  nourut
				,H.*,C.`name` as user_created,E.`name` as user_edited,GROUP_CONCAT(R.ref SEPARATOR ' / ') as ref FROM setting_keselamatan_isi H
							LEFT JOIN musers C ON C.id=H.created_by
							LEFT JOIN musers E ON E.id=H.edited_by
							LEFT JOIN setting_keselamatan_isi_jawaban M ON M.general_isi_id=H.id
							LEFT JOIN merm_referensi R ON R.id=M.ref_id
							WHERE H.`status`='1' AND H.header_id=0 AND H.jenis_header='3'
							
							GROUP BY H.id
							
							UNION ALL
							
							SELECT 
				CONCAT(LPAD(HH.`no`,3,'0'),'-',LPAD(H.`no`,3,'0')) as  nourut
				,H.*,C.`name` as user_created,E.`name` as user_edited,GROUP_CONCAT(R.ref SEPARATOR ' / ') as ref FROM setting_keselamatan_isi H
							LEFT JOIN musers C ON C.id=H.created_by
							LEFT JOIN musers E ON E.id=H.edited_by
							LEFT JOIN setting_keselamatan_isi_jawaban M ON M.general_isi_id=H.id
							LEFT JOIN merm_referensi R ON R.id=M.ref_id
							LEFT JOIN setting_keselamatan_isi HH ON HH.id=H.header_id
							WHERE H.`status`='1' AND H.header_id !=0 AND H.jenis_header='3'
							
							GROUP BY H.id
							
			) T ORDER BY T.nourut ASC";
		$hasil=$this->db->query($q)->result();
		$no=1;
		foreach($hasil as $r){
			$tabel_3 .='<tr>';
			$tabel_3 .='<td class="text-center '.($r->header_id=='0'?'bg-info':'').'">'.$r->no.'</td>';
			$tabel_3 .='<td class="'.($r->header_id=='0'?'bg-info':'').'">'.$r->isi.'</td>';
			$tabel_3 .='<td class="hidden-xs '.($r->header_id=='0'?'bg-info':'').'">'.($r->jenis_isi=='1'?text_danger(jenis_isi($r->jenis_isi)).'<br>'.$r->ref:text_primary(jenis_isi($r->jenis_isi))).'</td>';
			$tabel_3 .='<td class="hidden-xs '.($r->header_id=='0'?'bg-info':'').'">'.$r->user_created.'<br>'.($r->created_date?HumanDateLong($r->created_date):'').'</td>';
			$tabel_3 .='<td class="text-center '.($r->header_id=='0'?'bg-info':'').'">';
			$tabel_3 .='					<div class="btn-group">';
			if ($r->header_id=='0'){
				$tabel_3 .='						<button class="btn btn-xs btn-primary" type="button" onclick="add_detail('.$r->id.','.$r->jenis_header.')" data-toggle="tooltip" title="Tambah" data-original-title="Edit"><i class="fa fa-plus"></i></button>';
			}
			
			$tabel_3 .='						<button class="btn btn-xs btn-success" type="button" onclick="edit_content('.$r->id.','.$r->header_id.','.$r->jenis_header.')" data-toggle="tooltip" title="Edit" data-original-title="Tambah"><i class="fa fa-pencil"></i></button>';
			$tabel_3 .='						<button class="btn btn-xs btn-danger" type="button" onclick="hapus('.$r->id.')" data-toggle="tooltip" title="" data-original-title="Remove "><i class="fa fa-times"></i></button>';
			$tabel_3 .='					</div>';
			$tabel_3 .='				</td>';
			$tabel_3 .='</tr>';
			$no=$no+1;
		}
		$data['tabel_1']=$tabel_1;
		$data['tabel_2']=$tabel_2;
		$data['tabel_3']=$tabel_3;
				
		$this->output->set_output(json_encode($data));
	}
	function simpan_content(){
		// $data['header_id']=1;
		$jenis_header=$this->input->post('jenis_header');
		$header_id=$this->input->post('header_id');
		$id=$this->input->post('idcontent');
		$data['jenis_header']=$this->input->post('jenis_header');
		$data['header_id']=$this->input->post('header_id');
		$data['no']=$this->input->post('no');
		$data['isi']=$this->input->post('isi');
		$data['jenis_isi']=$this->input->post('jenis_isi');
		if ($this->input->post('idcontent')){
			$general_isi_id=$id;
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$id);
			$hasil=$this->db->update('setting_keselamatan_isi',$data);
		}else{
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
			$hasil=$this->db->insert('setting_keselamatan_isi',$data);
			$general_isi_id=$this->db->insert_id();
		}
		$this->db->where('general_isi_id',$general_isi_id);		
		$this->db->delete('setting_keselamatan_isi_jawaban');
		$ref_id=$this->input->post('ref_id');
		// print_r($ref_id);exit;
		if ($ref_id){
			foreach($ref_id as $index=>$val){
				$data_det['general_isi_id']=$general_isi_id;
				$data_det['ref_id']=$val;
				$hasil=$this->db->insert('setting_keselamatan_isi_jawaban',$data_det);
			}
		}
		$this->output->set_output(json_encode($hasil));
	}
	function hapus(){
	  $id=$this->input->post('id');
	  $this->deleted_by = $this->session->userdata('user_id');
	  $this->deleted_date = date('Y-m-d H:i:s');
	  $this->status=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('setting_keselamatan_isi',$this);
	  
	  json_encode($hasil);
	  
	}
	function update_label(){
	  $data=array(
		'label_sig_in_ina' => $this->input->post('label_sig_in_ina'),
		'label_sig_in_eng' => $this->input->post('label_sig_in_eng'),
		'label_time_out_ina' => $this->input->post('label_time_out_ina'),
		'label_time_out_eng' => $this->input->post('label_time_out_eng'),
		'label_sign_out_ina' => $this->input->post('label_sign_out_ina'),
		'label_sign_out_eng' => $this->input->post('label_sign_out_eng'),

	  );
	  
	  $hasil=$this->db->update('setting_keselamatan',$data);
	  
	  json_encode($hasil);
	  
	}
	function get_edit(){
	  $id=$this->input->post('id');
	  $q="SELECT *FROM setting_keselamatan_isi H WHERE H.id='$id'";
	  $hasil= $this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($hasil));
	  
	}
	function load_jawaban(){
	  $id=$this->input->post('id');
	  $q="SELECT H.id as ref_id,H.ref,CASE WHEN M.ref_id IS NOT NULL THEN 'selected' ELSE '' END as pilih 
			FROM merm_referensi H
			LEFT JOIN setting_keselamatan_isi_jawaban M ON M.ref_id=H.id AND M.general_isi_id='$id'
			WHERE H.ref_head_id='388' AND H.status='1'";
		
	  $hasil= $this->db->query($q)->result();
	  $data='';
	  foreach($hasil as $r){
		  $data .='<option '.$r->pilih.' value="'.$r->ref_id.'">'.$r->ref.'</option>';
	  }
	  $this->output->set_output(json_encode($data));
	  
	}
}
