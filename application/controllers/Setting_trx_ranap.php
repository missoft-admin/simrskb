<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_trx_ranap extends CI_Controller {

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Setting_trx_ranap_model');
		$this->load->model('Antrian_poli_kode_model');
		$this->load->helper('path');
		
  }


	function index($tab='1'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='1'){		
			if (UserAccesForm($user_acces_form,array('2629'))){
				$tab='7';
			}
			if (UserAccesForm($user_acces_form,array('2628'))){
				$tab='5';
			}
			if (UserAccesForm($user_acces_form,array('2627'))){
				$tab='5';
			}
			if (UserAccesForm($user_acces_form,array('2626'))){
				$tab='4';
			}
			if (UserAccesForm($user_acces_form,array('2625'))){
				$tab='3';
			}
			if (UserAccesForm($user_acces_form,array('2624'))){
				$tab='2';
			}
			if (UserAccesForm($user_acces_form,array('2623'))){
				$tab='1';
			}
		}
		// print_r($tab);exit;
		if (UserAccesForm($user_acces_form,array('2623','2624','26235','2626','2627','2628','2629'))){
			$q="SELECT * FROM setting_trx_ranap";
			$data=$this->db->query($q)->row_array();
				$q="SELECT * FROM setting_trx_ranap_label_kwitansi";
				$data_kwitansi=$this->db->query($q)->row_array();
				$data = array_merge($data,$data_kwitansi);
				$q="SELECT * FROM setting_trx_ranap_label_card";
				$data_card=$this->db->query($q)->row_array();
				$data = array_merge($data,$data_card);
		
			$data['tab'] 			= $tab;
			
			$data['jenis_isi'] 			= '0';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting Billing Ranap';
			$data['content'] 		= 'Setting_trx_ranap/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Pengaturan Billing Ranap",'#'),
												  array("Setting",'setting_trx_ranap/index')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function simpan_general(){
		$data=array(
			'st_edit_tgl_bayar' => $this->input->post('st_edit_tgl_bayar'),
			'st_simpan' => $this->input->post('st_simpan'),
			'st_simpan_proses' => $this->input->post('st_simpan_proses'),
			'st_batal' => $this->input->post('st_batal'),
			'st_selesai' => $this->input->post('st_selesai'),
			'st_selesai_verifikasi' => $this->input->post('st_selesai_verifikasi'),
			'email_generate_rincian' => $this->input->post('email_generate_rincian'),
			'email_generate_kwitansi' => $this->input->post('email_generate_kwitansi'),
			'waktu_batal' => $this->input->post('waktu_batal'),
			'st_qrcode' => $this->input->post('st_qrcode'),
			'st_verifikasi_disabled' => $this->input->post('st_verifikasi_disabled'),
			'st_edit_tgl_setoran' => $this->input->post('st_edit_tgl_setoran'),
			'st_kwitansi_all' => $this->input->post('st_kwitansi_all'),
			'st_kwitansi_kontraktor' => $this->input->post('st_kwitansi_kontraktor'),
			'st_kwitansi_exces' => $this->input->post('st_kwitansi_exces'),
			'st_simpan_kwitansi' => $this->input->post('st_simpan_kwitansi'),
			'st_simpan_generate_kwitansi' => $this->input->post('st_simpan_generate_kwitansi'),
			'st_edit_tgl_kwitansi' => $this->input->post('st_edit_tgl_kwitansi'),
			'st_default_nama' => $this->input->post('st_default_nama'),
			'label_kode_kwitansi' => $this->input->post('label_kode_kwitansi'),
			'st_qrcode_kwitansi' => $this->input->post('st_qrcode_kwitansi'),
			'waktu_batal_kwitansi' => $this->input->post('waktu_batal_kwitansi'),
			'waktu_maksimum_proses' => $this->input->post('waktu_maksimum_proses'),
			'st_edit_tgl_card' => $this->input->post('st_edit_tgl_card'),
			'email_card_pass' => $this->input->post('email_card_pass'),
			'label_deskripsi_kwitansi' => $this->input->post('label_deskripsi_kwitansi'),
		);
		// print_r($data);exit;
		$hasil=$this->db->update('setting_trx_ranap',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_label_menu(){
		$data=array(
			'label_ranap_ruangan_section' => $this->input->post('label_ranap_ruangan_section'),
			'label_ranap_ruangan_detail' => $this->input->post('label_ranap_ruangan_detail'),
			'label_ranap_fc_section' => $this->input->post('label_ranap_fc_section'),
			'label_ranap_fc_detail' => $this->input->post('label_ranap_fc_detail'),
			'label_ranap_ecg_section' => $this->input->post('label_ranap_ecg_section'),
			'label_ranap_ecg_detail' => $this->input->post('label_ranap_ecg_detail'),
			'label_ranap_visite_section' => $this->input->post('label_ranap_visite_section'),
			'label_ranap_visite_detail' => $this->input->post('label_ranap_visite_detail'),
			'label_ranap_alat_section' => $this->input->post('label_ranap_alat_section'),
			'label_ranap_alat_detail' => $this->input->post('label_ranap_alat_detail'),
			'label_ranap_ambulance_section' => $this->input->post('label_ranap_ambulance_section'),
			'label_ranap_ambulance_detail' => $this->input->post('label_ranap_ambulance_detail'),
			'label_ranap_obat_section' => $this->input->post('label_ranap_obat_section'),
			'label_ranap_obat_detail' => $this->input->post('label_ranap_obat_detail'),
			'label_ranap_alkes_section' => $this->input->post('label_ranap_alkes_section'),
			'label_ranap_alkes_detail' => $this->input->post('label_ranap_alkes_detail'),
			'label_ranap_lain_section' => $this->input->post('label_ranap_lain_section'),
			'label_ranap_lain_detail' => $this->input->post('label_ranap_lain_detail'),
			'label_igd_layanan_section' => $this->input->post('label_igd_layanan_section'),
			'label_igd_layanan_detail' => $this->input->post('label_igd_layanan_detail'),
			'label_igd_obat_section' => $this->input->post('label_igd_obat_section'),
			'label_igd_obat_detail' => $this->input->post('label_igd_obat_detail'),
			'label_igd_alkes_section' => $this->input->post('label_igd_alkes_section'),
			'label_igd_alkes_detail' => $this->input->post('label_igd_alkes_detail'),
			'label_lab_umum_section' => $this->input->post('label_lab_umum_section'),
			'label_lab_umum_detail' => $this->input->post('label_lab_umum_detail'),
			'label_lab_anatomi_section' => $this->input->post('label_lab_anatomi_section'),
			'label_lab_anatomi_detail' => $this->input->post('label_lab_anatomi_detail'),
			'label_lab_pmi_section' => $this->input->post('label_lab_pmi_section'),
			'label_lab_pmi_detail' => $this->input->post('label_lab_pmi_detail'),
			'label_rad_xray_section' => $this->input->post('label_rad_xray_section'),
			'label_rad_xray_detail' => $this->input->post('label_rad_xray_detail'),
			'label_rad_usg_section' => $this->input->post('label_rad_usg_section'),
			'label_rad_usg_detail' => $this->input->post('label_rad_usg_detail'),
			'label_rad_ct_section' => $this->input->post('label_rad_ct_section'),
			'label_rad_ct_detail' => $this->input->post('label_rad_ct_detail'),
			'label_rad_mri_section' => $this->input->post('label_rad_mri_section'),
			'label_rad_mri_detail' => $this->input->post('label_rad_mri_detail'),
			'label_rad_bmd_section' => $this->input->post('label_rad_bmd_section'),
			'label_rad_bmd_detail' => $this->input->post('label_rad_bmd_detail'),
			'label_fis_section' => $this->input->post('label_fis_section'),
			'label_fis_detail' => $this->input->post('label_fis_detail'),
			'label_ko_sewa_section' => $this->input->post('label_ko_sewa_section'),
			'label_ko_sewa_detail' => $this->input->post('label_ko_sewa_detail'),
			'label_ko_alkes_section' => $this->input->post('label_ko_alkes_section'),
			'label_ko_alkes_detail' => $this->input->post('label_ko_alkes_detail'),
			'label_ko_obat_section' => $this->input->post('label_ko_obat_section'),
			'label_ko_obat_detail' => $this->input->post('label_ko_obat_detail'),
			'label_ko_narcose_section' => $this->input->post('label_ko_narcose_section'),
			'label_ko_narcose_detail' => $this->input->post('label_ko_narcose_detail'),
			'label_ko_implan_section' => $this->input->post('label_ko_implan_section'),
			'label_ko_implan_detail' => $this->input->post('label_ko_implan_detail'),
			'label_ko_ruangan_section' => $this->input->post('label_ko_ruangan_section'),
			'label_ko_ruangan_detail' => $this->input->post('label_ko_ruangan_detail'),
			'label_ko_do_section' => $this->input->post('label_ko_do_section'),
			'label_ko_do_detail' => $this->input->post('label_ko_do_detail'),
			'label_ko_da_section' => $this->input->post('label_ko_da_section'),
			'label_ko_da_detail' => $this->input->post('label_ko_da_detail'),
			'label_ko_dao_section' => $this->input->post('label_ko_dao_section'),
			'label_ko_dao_detail' => $this->input->post('label_ko_dao_detail'),
			'label_adm_ranap_section' => $this->input->post('label_adm_ranap_section'),
			'label_adm_ranap_detail' => $this->input->post('label_adm_ranap_detail'),
			'label_adm_rajal_section' => $this->input->post('label_adm_rajal_section'),
			'label_adm_rajal_detail' => $this->input->post('label_adm_rajal_detail'),
		);
		// print_r($data);exit;
		$hasil=$this->db->update('setting_trx_ranap',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function simpan_auto_verifikasi(){
		$data=array(
			'hari_ranap_fc' => $this->input->post('hari_ranap_fc'),
			'hari_ranap_ecg' => $this->input->post('hari_ranap_ecg'),
			'hari_ranap_visite' => $this->input->post('hari_ranap_visite'),
			'hari_ranap_sewa' => $this->input->post('hari_ranap_sewa'),
			'hari_ranap_ambulance' => $this->input->post('hari_ranap_ambulance'),
			'hari_ranap_lain' => $this->input->post('hari_ranap_lain'),
			'hari_ranap_obat' => $this->input->post('hari_ranap_obat'),
			'hari_ranap_alkes' => $this->input->post('hari_ranap_alkes'),
			'hari_igd_pelayanan' => $this->input->post('hari_igd_pelayanan'),
			'hari_igd_obat' => $this->input->post('hari_igd_obat'),
			'hari_igd_alkes' => $this->input->post('hari_igd_alkes'),
			'hari_lab_umum' => $this->input->post('hari_lab_umum'),
			'hari_lab_anatomi' => $this->input->post('hari_lab_anatomi'),
			'hari_lab_pmi' => $this->input->post('hari_lab_pmi'),
			'hari_rab_xray' => $this->input->post('hari_rab_xray'),
			'hari_rab_usg' => $this->input->post('hari_rab_usg'),
			'hari_rab_ct' => $this->input->post('hari_rab_ct'),
			'hari_rab_mri' => $this->input->post('hari_rab_mri'),
			'hari_rab_bmd' => $this->input->post('hari_rab_bmd'),
			'hari_fis' => $this->input->post('hari_fis'),
			'hari_ko_sewa' => $this->input->post('hari_ko_sewa'),
			'hari_ko_alkes' => $this->input->post('hari_ko_alkes'),
			'hari_ko_obat' => $this->input->post('hari_ko_obat'),
			'hari_ko_narcose' => $this->input->post('hari_ko_narcose'),
			'hari_ko_implan' => $this->input->post('hari_ko_implan'),
			'hari_ko_ruangan' => $this->input->post('hari_ko_ruangan'),
			'hari_ko_do' => $this->input->post('hari_ko_do'),
			'hari_ko_da' => $this->input->post('hari_ko_da'),
			'hari_ko_dao' => $this->input->post('hari_ko_dao'),
			'hari_adm_ranap' => $this->input->post('hari_adm_ranap'),
			'hari_adm_rajal' => $this->input->post('hari_adm_rajal'),
			'lama_auto_verif' => $this->input->post('lama_auto_verif'),
		);
		// print_r($data);exit;
		$hasil=$this->db->update('setting_trx_ranap',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function save_kwitansi(){
		// print_r($this->input->post());exit;
			$data=array(
				'alamat_kwitansi' => $this->input->post('alamat_kwitansi'),
				'telepone_kwitansi' => $this->input->post('telepone_kwitansi'),
				'email_kwitansi' => $this->input->post('email_kwitansi'),
				'judul_kwitansi_ina' => $this->input->post('judul_kwitansi_ina'),
				'judul_kwitansi_eng' => $this->input->post('judul_kwitansi_eng'),
				'no_register_kwitansi_ina' => $this->input->post('no_register_kwitansi_ina'),
				'no_register_kwitansi_eng' => $this->input->post('no_register_kwitansi_eng'),
				'no_kwitansi_ina' => $this->input->post('no_kwitansi_ina'),
				'no_kwitansi_eng' => $this->input->post('no_kwitansi_eng'),
				'tanggal_kwitansi_ina' => $this->input->post('tanggal_kwitansi_ina'),
				'tanggal_kwitansi_eng' => $this->input->post('tanggal_kwitansi_eng'),
				'sudah_terima_kwitansi_ina' => $this->input->post('sudah_terima_kwitansi_ina'),
				'sudah_terima_kwitansi_eng' => $this->input->post('sudah_terima_kwitansi_eng'),
				'banyaknya_kwitansi_ina' => $this->input->post('banyaknya_kwitansi_ina'),
				'banyaknya_kwitansi_eng' => $this->input->post('banyaknya_kwitansi_eng'),
				'untuk_kwitansi_ina' => $this->input->post('untuk_kwitansi_ina'),
				'untuk_kwitansi_eng' => $this->input->post('untuk_kwitansi_eng'),
				'jumlah_kwitansi_ina' => $this->input->post('jumlah_kwitansi_ina'),
				'jumlah_kwitansi_eng' => $this->input->post('jumlah_kwitansi_eng'),
				'yg_menerima_kwitansi_ina' => $this->input->post('yg_menerima_kwitansi_ina'),
				'yg_menerima_kwitansi_eng' => $this->input->post('yg_menerima_kwitansi_eng'),
				'footer_kwitansi_ina' => $this->input->post('footer_kwitansi_ina'),
				'footer_kwitansi_eng' => $this->input->post('footer_kwitansi_eng'),
				'generated_kwitansi_ina' => $this->input->post('generated_kwitansi_ina'),
				'generated_kwitansi_eng' => $this->input->post('generated_kwitansi_eng'),


			);
			if (isset($_FILES['logo_kwitansi'])) {
				// print_r('sini');exit;
				$config['upload_path'] = './assets/upload/logo_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('logo_kwitansi')) {
                    $image_upload = $this->upload->data();
                    $data['logo_kwitansi'] = $image_upload['file_name'];
					
                } 
			}
			$hasil=$this->db->update('setting_trx_ranap_label_kwitansi',$data);
			if ($hasil){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data tandatangan telah disimpan.');
				redirect('Setting_trx_ranap/index/6','location');
			}
	}
	function save_label_card(){
		// print_r($this->input->post());exit;
			$data=array(
				'alamat_card' => $this->input->post('alamat_card'),
				'telepone_card' => $this->input->post('telepone_card'),
				'email_card' => $this->input->post('email_card'),
				'judul_card_ina' => $this->input->post('judul_card_ina'),
				'judul_card_eng' => $this->input->post('judul_card_eng'),
				'no_register_card_ina' => $this->input->post('no_register_card_ina'),
				'no_register_card_eng' => $this->input->post('no_register_card_eng'),
				'paragraf1_card_ina' => $this->input->post('paragraf1_card_ina'),
				'paragraf1_card_eng' => $this->input->post('paragraf1_card_eng'),
				'nama_card_ina' => $this->input->post('nama_card_ina'),
				'nama_card_eng' => $this->input->post('nama_card_eng'),
				'deskripsi_card_ina' => $this->input->post('deskripsi_card_ina'),
				'deskripsi_card_eng' => $this->input->post('deskripsi_card_eng'),
				'paragraf2_card_ina' => $this->input->post('paragraf2_card_ina'),
				'paragraf2_card_eng' => $this->input->post('paragraf2_card_eng'),
				'yg_menerima_card_ina' => $this->input->post('yg_menerima_card_ina'),
				'yg_menerima_card_eng' => $this->input->post('yg_menerima_card_eng'),
				'footer_card_ina' => $this->input->post('footer_card_ina'),
				'footer_card_eng' => $this->input->post('footer_card_eng'),
				'generated_card_ina' => $this->input->post('generated_card_ina'),
				'generated_card_eng' => $this->input->post('generated_card_eng'),

			);
			if (isset($_FILES['logo_card'])) {
				// print_r('sini');exit;
				$config['upload_path'] = './assets/upload/logo_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('logo_card')) {
                    $image_upload = $this->upload->data();
                    $data['logo_card'] = $image_upload['file_name'];
					
                } 
			}
			$hasil=$this->db->update('setting_trx_ranap_label_card',$data);
			if ($hasil){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data tandatangan telah disimpan.');
				redirect('Setting_trx_ranap/index/7','location');
			}
	}
	

	public function upload_header_logo($update = false)
    {
        if (!file_exists('assets/upload/logo_setting')) {
            mkdir('assets/upload/logo_setting', 0755, true);
        }

        if (isset($_FILES['logo_form'])) {
            if ($_FILES['logo_form']['name'] != '') {
                $config['upload_path'] = './assets/upload/logo_setting/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('logo_form')) {
                    $image_upload = $this->upload->data();
                    $this->logo_form = $image_upload['file_name'];

                    return  $this->logo_form;
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
			// print_r('TIDAK ADA');exit;
                return true;
            }
					// print_r($this->foto);exit;
        } else {
			// print_r('TIDAK ADA');exit;
            return true;
        }
		
    }
	
 function get_edit_batal_kwitansi(){
		$id     = $this->input->post('id');
		$q="SELECT D.*
			,CASE WHEN D.jenis='0' THEN 'DEPOSIT' WHEN D.jenis='1' THEN 'OBAT' ELSE 'TRANSAKSI' END as tipe_nama
			,U.`name` as user_nama FROM `mlogic_trx_ranap` D
				LEFT JOIN musers U ON U.id=D.iduser
				WHERE D.id='$id'";
		$arr['detail'] =$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($arr['detail']));
	}
	function list_user($step,$jenis='0')
	{
		 $q="SELECT *FROM musers M
				WHERE M.id NOT IN (SELECT iduser from mlogic_trx_ranap WHERE mlogic_trx_ranap.step='$step' 
				AND mlogic_trx_ranap.jenis='$jenis' 
				AND mlogic_trx_ranap.status='1') AND M.status='1'";
		$arr['detail'] = $this->db->query($q)->result();
		$this->output->set_output(json_encode($arr));
	}
	
	function simpan_batal_kwitansi()
	{
		$id_edit=$this->input->post('id_edit_batal_kwitansi');
		$data=array(
				'iduser'=>$this->input->post('iduser'),
				'step'=>$this->input->post('step'),
				'proses_setuju'=>$this->input->post('proses_setuju'),
				'proses_tolak'=>$this->input->post('proses_tolak'),
				'jenis'=>$this->input->post('jenis'),
				'deskrpisi'=>$this->input->post('deskrpisi'),
				
				
			);
		
		if ($id_edit==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');			
			$result = $this->db->insert('mlogic_trx_ranap',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');		
			$this->db->where('id',$id_edit);
			$result = $this->db->update('mlogic_trx_ranap',$data);
		}
       
		
		$this->output->set_output(json_encode($result));
	}
	
	function hapus_det_kwitansi()
	{
		$id=$this->input->post('id');
		$this->db->where('id',$this->input->post('id'));	
		$data=array(
			'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_date'=>date('Y-m-d H:i:s'),
		);
       // print_r($data);
	   
		$result = $this->db->update('mlogic_trx_ranap',$data);
		echo json_encode($result);
	}
	
	
	function load_batal_kwitansi()
    {
		
		$idlogic     = $this->input->post('idlogic');
		
		$iduser=$this->session->userdata('user_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT D.*,U.`name` as user_nama,
				J.jenis_kwitansi as tipe_nama
				FROM `mlogic_trx_ranap` D
				LEFT JOIN mjenis_kwitansi J ON J.id=D.jenis
				LEFT JOIN musers U ON U.id=D.iduser
				WHERE  D.status='1'
				ORDER BY D.jenis,D.step
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->step;
            
            $row[] = $r->tipe_nama;
            $row[] = $r->deskrpisi;
            $aksi       = '<div class="btn-group">';	
			
			$aksi 		.= '<button  type="button" class="btn btn-xs btn-primary edit_batal_kwitansi btn_batal_kwitansi" title="Edit"><i class="fa fa-pencil"></i></button>';				
			$aksi 		.= '<button type="button" class="btn btn-xs btn-danger hapus_det_batal_kwitansi btn_batal_kwitansi" title="Hapus"><i class="fa fa-close"></i></button>';				
			
			$aksi.='</div>';			
            $row[] = $r->user_nama;
			$row[] = ($r->proses_setuju=='1'?'Langsung':'Menunggu User');
            $row[] = ($r->proses_tolak=='1'?'Langsung':'Menunggu User');
			$row[] = $aksi;			
            $row[] = $r->id;
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	
 function get_edit_card(){
		$id     = $this->input->post('id');
		$q="SELECT D.*
			,CASE WHEN D.jenis='0' THEN 'DEPOSIT' WHEN D.jenis='1' THEN 'OBAT' ELSE 'TRANSAKSI' END as tipe_nama
			,U.`name` as user_nama FROM `mlogic_trx_ranap_card` D
				LEFT JOIN musers U ON U.id=D.idrekanan
				WHERE D.id='$id'";
		$arr['detail'] =$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($arr['detail']));
	}
	function list_rekanan($kelompok_id,$jenis='0')
	{
		 $q="SELECT * FROM `mrekanan` H
WHERE H.tipe_rekanan='$jenis'";
		$arr['detail'] = $this->db->query($q)->result();
		$this->output->set_output(json_encode($arr));
	}
	
	function simpan_card()
	{
		$id_edit=$this->input->post('id_edit_card');
		if ($this->input->post('kelompok_id')=='1'){
			if ($this->input->post('tipe_rekanan')=='0'){
				$tipe_rekanan=0;
				$idrekanan=0;
			}else{
				$tipe_rekanan=$this->input->post('tipe_rekanan');
				$idrekanan=$this->input->post('idrekanan');
			}
		}else{
			$tipe_rekanan=0;
			$idrekanan=0;
			
		}
		$data=array(
				'kelompok_id'=>$this->input->post('kelompok_id'),
				'tipe_rekanan'=>$tipe_rekanan,
				'idrekanan'=>$idrekanan,
				'status_card'=>$this->input->post('status_card'),
				
				
			);
		
		if ($id_edit==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');			
			$result = $this->db->insert('mlogic_trx_ranap_card',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');		
			$this->db->where('id',$id_edit);
			$result = $this->db->update('mlogic_trx_ranap_card',$data);
		}
       
		
		$this->output->set_output(json_encode($result));
	}
	
	function hapus_det_card()
	{
		$id=$this->input->post('id');
		$this->db->where('id',$this->input->post('id'));	
		$data=array(
			'status'=>0,
			'deleted_by'=>$this->session->userdata('user_id'),
			'deleted_date'=>date('Y-m-d H:i:s'),
		);
       // print_r($data);
	   
		$result = $this->db->update('mlogic_trx_ranap_card',$data);
		echo json_encode($result);
	}
	
	
	function load_card()
    {
		
		$idlogic     = $this->input->post('idlogic');
		
		$idrekanan=$this->session->userdata('user_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT MP.nama as nama_kelompok,MR.nama as nama_rekanan,D.*
				
				FROM `mlogic_trx_ranap_card` D
				INNER JOIN mpasien_kelompok MP ON MP.id=D.kelompok_id
				LEFT JOIN mrekanan MR ON MR.id=D.idrekanan
				WHERE  D.status='1'
				ORDER BY D.kelompok_id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->nama_kelompok;
			if ($r->tipe_rekanan=='0'){
				$row[] = text_danger('TIDAK DITENTUKAN');
			}else{
				
				$row[] = ($r->tipe_rekanan=='1'?'ASURANSI':'REKANAN');
			}
			if ($r->idrekanan=='0'){
				$row[] = text_danger('TIDAK DITENTUKAN');
			}else{
				
            $row[] = $r->nama_rekanan;
			}
            $aksi       = '<div class="btn-group">';	
			
			// $aksi 		.= '<button  type="button" class="btn btn-xs btn-primary edit_card btn_card" title="Edit"><i class="fa fa-pencil"></i></button>';				
			$aksi 		.= '<button type="button" class="btn btn-xs btn-danger hapus_det_card btn_card" title="Hapus"><i class="fa fa-close"></i></button>';				
			
			$aksi.='</div>';			
            $row[] = ($r->status_card=='1'?'LUNAS':'BELUM LUNAS');
			$row[] = $aksi;			
            $row[] = $r->id;
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	
}
