<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtarif_ruangperawatan extends CI_Controller {

	/**
	 * Tarif Ruang Perawatan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mtarif_ruangperawatan_model');
		$this->load->model('Mtarif_administrasi_model');
  }

	function index($idtipe=1)
	{
		$data = array(
			'idtipe' => $idtipe,
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tarif Ruang Perawatan';
		$data['content'] 		= 'Mtarif_ruangperawatan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif Ruang Perawatan",'mtarif_ruangperawatan/index'),
														array("List",'#')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function filter()
	{
		$data = array(
			'idtipe' => $this->input->post('idtipe'),
		);

		$this->session->set_userdata($data);

		$data['error'] 			= '';
		$data['title'] 			= 'Tarif Ruang Perawatan';
		$data['content'] 		= 'Mtarif_ruangperawatan/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif Ruang Perawatan",'mtarif_ruangperawatan/index'),
														array("List",'#')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create($idtipe){
		$data = array(
			'id' 							=> '',
			'idtipe' 					=> $idtipe,
			'nama' 						=> '',
			'status' 					=> ''
		);

		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Tarif Ruang Perawatan';
		$data['content'] 		= 'Mtarif_ruangperawatan/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Tarif Ruang Perawatan",'#'),
									    			array("Tambah",'mtarif_ruangperawatan')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function update($id){
		if($id != ''){
			$row = $this->Mtarif_ruangperawatan_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 							=> $row->id,
					'idtipe' 					=> $row->idtipe,
					'nama' 						=> $row->nama,
					'status' 					=> $row->status
				);
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Tarif Ruang Perawatan';
				$data['content']	 	= 'Mtarif_ruangperawatan/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Tarif Ruang Perawatan",'#'),
											    			array("Ubah",'mtarif_ruangperawatan')
															);

				$data['list_tarif'] = $this->Mtarif_ruangperawatan_model->getAllTarif($row->id);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtarif_ruangperawatan/index', 'location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtarif_ruangperawatan/index', 'location');
		}
	}

	function delete($id){
		$this->Mtarif_ruangperawatan_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mtarif_ruangperawatan/index', 'location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->Mtarif_ruangperawatan_model->saveData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_ruangperawatan/index/'.$this->input->post('idtipe'), 'location');
				}
			} else {
				if($this->Mtarif_ruangperawatan_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtarif_ruangperawatan/index/'.$this->input->post('idtipe'), 'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 			 = validation_errors();
		$data['content'] 		 = 'Mtarif_ruangperawatan/manage';

		if($id==''){
			$data['title'] = 'Tambah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Tambah",'mtarif_ruangperawatan')
													);
		}else{
			$data['title'] = 'Ubah Satuan';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Satuan",'#'),
															array("Ubah",'mtarif_ruangperawatan')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex($uri)
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];

			$this->select = array();
			$this->from   = 'mtarif_ruangperawatan';
			$this->join 	= array();

			// FILTER
			if($uri == 'filter'){
				$this->where  = array();
				if ($this->session->userdata('idtipe') != "#") {
					$this->where = array_merge($this->where, array('idtipe' => $this->session->userdata('idtipe')));
				}
				$this->where = array_merge($this->where, array('status' => '1'));
			}else{
				$this->where  = array(
					'status' => '1'
				);
			}

			$this->order  = array(
				'id' => 'DESC'
			);
			$this->group  = array();

			if (UserAccesForm($user_acces_form,array('179'))){
				$this->column_search   = array('nama');
			}else{
				$this->column_search   = array();
			}
			$this->column_order    = array('nama');

			$list = $this->datatable->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $r) {
					$no++;
					$row = array();

					$row[] = $no;
					$row[] = $r->nama;
					$aksi = '<div class="btn-group">';
		            if (UserAccesForm($user_acces_form,array('181'))){
		                $aksi .= '<a href="'.site_url().'mtarif_ruangperawatan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
		            }
		            if (UserAccesForm($user_acces_form,array('182'))){
		                $aksi .= '<a href="#" data-urlindex="'.site_url().'mtarif_ruangperawatan" data-urlremove="'.site_url().'mtarif_ruangperawatan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
		            }
		            $aksi .= '<a href="'.site_url().'mtarif_ruangperawatan/setting/'.$r->id.'" data-toggle="tooltip" title="Setting" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>';
		            $aksi .= '</div>';
  					$row[] = $aksi;

					$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datatable->count_all(),
				"recordsFiltered" => $this->datatable->count_all(),
				"data" => $data
			);
			echo json_encode($output);
	}

	public function setting($id)
	{
			if ($id != '') {
					$row = $this->Mtarif_ruangperawatan_model->getSpecified($id);
					if (isset($row->id)) {
							$data = array(
								'id' 							=> $row->id,
								'idtipe' 					=> $row->idtipe,
								'nama' 						=> $row->nama,
								'group_diskon_all' 					=> $row->group_diskon_all,
								'status' 					=> $row->status
							);

							$data['error']      = '';
							$data['title']      = 'Setting Group Pembayaran Tarif Fisioterapi';
							$data['content']    = 'Mtarif_ruangperawatan/setting';
							$data['breadcrum']  = array(
									array("RSKB Halmahera",'#'),
									array("Tarif Fisioterapi",'mtarif_ruangperawatan/index'),
									array("Setting",'mtarif_ruangperawatan')
							);

							$data['list_tarif'] = $this->Mtarif_ruangperawatan_model->getAllTarif($row->id);
							$data['list_group_pembayaran'] = $this->Mtarif_administrasi_model->getGroupPembayaran();

							$data = array_merge($data, backend_info());
							$this->parser->parse('module_template', $data);
					} else {
							$this->session->set_flashdata('error', true);
							$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
							redirect('mtarif_ruangperawatan/index/'.$uri, 'location');
					}
			} else {
					$this->session->set_flashdata('error', true);
					$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
					redirect('mtarif_ruangperawatan/index/'.$uri, 'location');
			}
	}

	function save_setting() {
		if($this->Mtarif_ruangperawatan_model->updateSetting()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mtarif_ruangperawatan', 'location');
		}
	}
}
