<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_assesmen_askep extends CI_Controller {

	/**
	 * Apps Setting controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Setting_assesmen_askep_model');
		$this->load->helper('path');
		
  }


	function index($tab='0'){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('2227'))){
				$tab='1';
			}elseif (UserAccesForm($user_acces_form,array('2228'))){
				$tab='2';
			}elseif (UserAccesForm($user_acces_form,array('2229'))){
				$tab='3';
			}elseif (UserAccesForm($user_acces_form,array('2230'))){
				$tab='4';
			}
		}
		if (UserAccesForm($user_acces_form,array('2227','2228','2229','2230'))){
			$data = $this->Setting_assesmen_askep_model->get_assesmen_setting();
			$data['tab'] 			= $tab;
			$data['spesialisasi_id'] 			= '#';
			$data['profesi_id'] 			= '#';
			$data['mppa_id'] 			= '#';
			$data['st_lihat'] 			= '0';
			$data['st_input'] 			= '0';
			$data['st_edit'] 			= '0';
			$data['st_hapus'] 			= '0';
			$data['st_cetak'] 			= '0';
			$data['error'] 			= '';
			$data['title'] 			= 'Setting ASKEP PERIOPERATIF';
			$data['content'] 		= 'Setting_assesmen_askep/index';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Master ERM",'#'),
												  array("ASKEP PERIOPERATIF Setting",'setting_assesmen_askep/index')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	function load_hak_akses()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.id,H.profesi_id,P.ref as profesi
							,H.spesialisasi_id,CASE WHEN H.spesialisasi_id=0 THEN 'SEMUA' ELSE S.ref END as spesialisasi 
							,H.mppa_id,M.nama as nama_ppa,H.st_lihat,H.st_input,H.st_edit,H.st_hapus,H.st_cetak
							FROM `setting_assesmen_askep_akses` H
							LEFT JOIN merm_referensi P ON P.nilai=H.profesi_id AND P.ref_head_id='21' AND P.`status`='1'
							LEFT JOIN merm_referensi S ON S.nilai=H.spesialisasi_id AND S.ref_head_id='22'  AND S.`status`='1'
							LEFT JOIN mppa M ON M.id=H.mppa_id AND M.staktif='1'
							ORDER BY H.profesi_id,H.spesialisasi_id



					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_ppa','spesialisasi','profesi');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = ($r->profesi);
          $result[] = ($r->spesialisasi_id=='0'?text_default($r->spesialisasi):$r->spesialisasi);
          $result[] = ($r->mppa_id=='0'?text_default('SEMUA'):$r->nama_ppa);
          $result[] = ($r->st_lihat?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_input?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_edit?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_hapus?'IZINKAN':text_danger('TIDAK'));
          $result[] = ($r->st_cetak?'IZINKAN':text_danger('TIDAK'));
		  // if (UserAccesForm($user_acces_form,array('1603'))){
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="hapus_hak_akses('.$r->id.')" type="button" title="Hapus Logic" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
		  // }
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
	function save_assesmen(){
		if ($this->Setting_assesmen_askep_model->save_assesmen()==true){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('setting_assesmen_askep/index/1','location');
		}
	}
	
	
	function simpan_param(){
		$param_id=$this->input->post('param_id');
		$data=array(
			'parameter'=>$this->input->post('parameter'),
			'staktif'=>1,
		);
		if ($param_id==''){
		    $hasil=$this->db->insert('setting_assesmen_askep_param',$data);
		}else{
			$this->db->where('id',$param_id);
		    $hasil=$this->db->update('setting_assesmen_askep_param',$data);
		}
		  
		  json_encode($hasil);
	}
	function load_param()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
						
						FROM `setting_assesmen_askep_param` H
						

						where H.staktif='1'
						GROUP BY H.id
						ORDER BY H.id ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('parameter');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
	  $id_param='';
	  $param_skor='';
	  $st_cetak='0';
      foreach ($list as $r) {
		  // if ($id_param!=$r->id){
			  // $param_skor .=($r->jenis=='1'?text_success('EDUKASI'):text_primary('MENGERTI')).($r->jawaban?'<br>'.$this->render_jawaban($r->jawaban):'');
		  // }else{
			  
		  // }
		  // if ($st_cetak=='1'){
			  $no++;
			  $result = array();
			  // $param_skor .=''.($r->jenis=='1'?text_success('EDUKASI'):text_primary('MENGERTI')).($r->jawaban?'<br>'.$this->render_jawaban($r->jawaban):'');
			  $result[] = $no;
				 $aksi='';
			  $result[] ='<strong>'.$r->parameter.'</strong>';
			  $result[] =$this->render_jawaban($r->id);
			 
			  $aksi = '<div class="btn-group">';
			  $aksi .= '<button onclick="add_jawaban('.$r->id.')" type="button" title="Add Value" class="btn btn-warning btn-xs "><i class="fa fa-plus"></i></button>';	
			  $aksi .= '<button onclick="edit_param('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
			  $aksi .= '<button onclick="hapus_param('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			  $aksi .= '</div>';
			  $result[] = $aksi;
			  $param_skor='';
			  $data[] = $result;
		  // }
		  $id_param=$r->id;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
   function render_jawaban($parameter_id){
	   $q="SELECT M.ref as nama_jenis,GROUP_CONCAT(H.deskripsi_nama,' (',H.skor,')' SEPARATOR '#') as jawaban FROM `setting_assesmen_askep_param_skor` H
			LEFT JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='386'
			WHERE H.parameter_id='$parameter_id' AND H.staktif='1'
			GROUP BY H.jenis";
	  $hasil='';
	  $list_data=$this->db->query($q)->result();
	  foreach ($list_data as $r){
		  $jawaban=$r->jawaban;
		  $arr=explode('#',$jawaban);
		  $hasil .=($r->nama_jenis).' : ';
		  foreach($arr as $index=>$val){
			  $hasil .='<span class="badge badge-primary">'.$val.'</span> ';
		  }
		  $hasil .='<br>';
		  $hasil .='<br>';
	  }
	  return $hasil;
  }
  function hapus_param(){
	  $param_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		// $data['deleted_by']=$this->session->userdata('user_id');
		// $data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$param_id);
		$hasil=$this->db->update('setting_assesmen_askep_param',$data);
	  
	  json_encode($hasil);
	  
  }
	function find_jawaban(){
	  $skor_id=$this->input->post('id');
	  $q="SELECT *FROM setting_assesmen_askep_param_skor H WHERE H.id='$skor_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function find_param(){
	  $param_id=$this->input->post('id');
	  $q="SELECT *FROM setting_assesmen_askep_param H WHERE H.id='$param_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function load_jawaban()
	{
		$parameter_id=$this->input->post('parameter_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*,M.ref as nama_jenis 
							FROM `setting_assesmen_askep_param_skor` H
							LEFT JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='386'
							where H.staktif='1' AND H.parameter_id='$parameter_id'
							ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('deskripsi_nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->skor;
          $result[] = ($r->nama_jenis);
          $result[] = $r->deskripsi_nama;
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_jawaban('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_jawaban('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function simpan_jawaban(){
		$skor_id=$this->input->post('skor_id');
		$parameter_id=$this->input->post('parameter_id');
		$data=array(
			'parameter_id'=>$this->input->post('parameter_id'),
			'skor'=>$this->input->post('skor'),
			'jenis'=>$this->input->post('jenis'),
			'deskripsi_nama'=>$this->input->post('deskripsi_nama'),
			'staktif'=>1,
		);
		if ($skor_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('setting_assesmen_askep_param_skor',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$skor_id);
		    $hasil=$this->db->update('setting_assesmen_askep_param_skor',$data);
		}
		  
		  json_encode($hasil);
	}
	 function hapus_jawaban(){
	  $skor_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$skor_id);
		$hasil=$this->db->update('setting_assesmen_askep_param_skor',$data);
	  
	  json_encode($hasil);
	  
  }

  //NILAI
  
  
  function simpan_hak_akses(){
		$hasil=$this->cek_duplicate_hak_akses($this->input->post('profesi_id'),$this->input->post('spesialisasi_id'),$this->input->post('mppa_id'));
		if ($hasil==null){	
			$this->profesi_id=$this->input->post('profesi_id');
			$this->spesialisasi_id=$this->input->post('spesialisasi_id');
			$this->mppa_id=$this->input->post('mppa_id');
			$this->st_lihat=$this->input->post('st_lihat');
			$this->st_input=$this->input->post('st_input');
			$this->st_edit=$this->input->post('st_edit');
			$this->st_hapus=$this->input->post('st_hapus');
			$this->st_cetak=$this->input->post('st_cetak');
			
			$this->created_by = $this->session->userdata('user_id');
			$this->created_date = date('Y-m-d H:i:s');
		
			$hasil=$this->db->insert('setting_assesmen_askep_akses',$this);
		}else{
			$hasil=null;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate_hak_akses($profesi_id,$spesialisasi_id,$mppa_id){
		$q="SELECT *FROM setting_assesmen_askep_akses WHERE  profesi_id='$profesi_id' AND spesialisasi_id='$spesialisasi_id' AND mppa_id='$mppa_id'";
		$hasil=$this->db->query($q)->row('id');
		return $hasil;
	}
	
  function hapus_hak_akses(){
	  $id=$this->input->post('id');
	  $this->db->where('id',$id);
	  $hasil=$this->db->delete('setting_assesmen_askep_akses',$this);
	  
	  json_encode($hasil);
	  
  }
  
  function find_mppa($profesi_id){
	  $q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='$profesi_id' AND H.staktif='1'";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($profesi_id!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
  function find_poli($idtipe){
	  $q="SELECT *FROM mpoliklinik H WHERE H.idtipe='$idtipe' AND H.`status`='1'";
	  $opsi='<option value="#" selected>-SEMUA-</option>';
	  if ($idtipe!='#'){
		  $hasil=$this->db->query($q)->result();
		  foreach ($hasil as $r){
			$opsi .='<option value="'.$r->id.'">'.$r->nama.'</option>';
			  
		  }
	  }
	  $this->output->set_output(json_encode($opsi));
  }
  
  function simpan_nilai(){
		$parameter_id=$this->input->post('nilai_id');
		$skor_1=$this->input->post('skor_1');
		$skor_2=$this->input->post('skor_2');
		$kesimpulan_gizi_id=$this->input->post('kesimpulan_gizi_id');
		$tindakan_id=$this->input->post('tindakan_id');
		$data=array(
			'skor_1'=>$this->input->post('skor_1'),
			'skor_2'=>$this->input->post('skor_2'),
			'kesimpulan_gizi_id'=>$this->input->post('kesimpulan_gizi_id'),
			'tindakan_id'=>$this->input->post('tindakan_id'),
			'staktif'=>1,
		);
		if ($parameter_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('setting_assesmen_askep_nilai',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$parameter_id);
		    $hasil=$this->db->update('setting_assesmen_askep_nilai',$data);
		}
		  
		  json_encode($hasil);
	}
	function hapus_nilai(){
	  $parameter_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$parameter_id);
		$hasil=$this->db->update('setting_assesmen_askep_nilai',$data);
	  
	  json_encode($hasil);
	  
  }
  function find_nilai(){
	  $parameter_id=$this->input->post('id');
	  $q="SELECT *FROM setting_assesmen_askep_nilai H WHERE H.id='$parameter_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function load_nilai()
	{
			$mrisiko_id=$this->input->post('mrisiko_id');
		
			$this->select = array();
			$from="
					(
						SELECT H.*
						,MK.ref as nama_kesimpulan,MT.ref as nama_tindakan						
						FROM `setting_assesmen_askep_nilai` H
						LEFT JOIN merm_referensi MK ON MK.nilai=H.kesimpulan_mpp_id AND MK.ref_head_id='300'
						LEFT JOIN merm_referensi MT ON MT.nilai=H.tindakan_id AND MT.ref_head_id='301'
						where H.staktif='1'
						GROUP BY H.id
						ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('ref_nilai');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->skor_1.' - '.$r->skor_2;
          $result[] = $r->nama_kesimpulan;
          $result[] = $r->nama_tindakan;
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_nilai('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_nilai('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  
  //OPER
  
  function simpan_param_opr(){
		$param_id=$this->input->post('param_id');
		$data=array(
			'parameter'=>$this->input->post('parameter'),
			'staktif'=>1,
		);
		if ($param_id==''){
		    $hasil=$this->db->insert('setting_assesmen_askep_param_opr',$data);
		}else{
			$this->db->where('id',$param_id);
		    $hasil=$this->db->update('setting_assesmen_askep_param_opr',$data);
		}
		  
		  json_encode($hasil);
	}
	function load_param_opr()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*
						
						FROM `setting_assesmen_askep_param_opr` H
						

						where H.staktif='1'
						GROUP BY H.id
						ORDER BY H.id ASC
					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('parameter');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
	  $id_param_opr='';
	  $param_skor='';
	  $st_cetak='0';
      foreach ($list as $r) {
		  // if ($id_param_opr!=$r->id){
			  // $param_skor .=($r->jenis=='1'?text_success('EDUKASI'):text_primary('MENGERTI')).($r->jawaban_opr?'<br>'.$this->render_jawaban_opr($r->jawaban_opr):'');
		  // }else{
			  
		  // }
		  // if ($st_cetak=='1'){
			  $no++;
			  $result = array();
			  // $param_skor .=''.($r->jenis=='1'?text_success('EDUKASI'):text_primary('MENGERTI')).($r->jawaban_opr?'<br>'.$this->render_jawaban_opr($r->jawaban_opr):'');
			  $result[] = $no;
				 $aksi='';
			  $result[] ='<strong>'.$r->parameter.'</strong>';
			  $result[] =$this->render_jawaban_opr($r->id);
			 
			  $aksi = '<div class="btn-group">';
			  $aksi .= '<button onclick="add_jawaban_opr('.$r->id.')" type="button" title="Add Value" class="btn btn-warning btn-xs "><i class="fa fa-plus"></i></button>';	
			  $aksi .= '<button onclick="edit_param_opr('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
			  $aksi .= '<button onclick="hapus_param_opr('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			  $aksi .= '</div>';
			  $result[] = $aksi;
			  $param_skor='';
			  $data[] = $result;
		  // }
		  $id_param_opr=$r->id;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
   function render_jawaban_opr($parameter_id){
	   $q="SELECT M.ref as nama_jenis,GROUP_CONCAT(H.deskripsi_nama,' (',H.skor,')' SEPARATOR '#') as jawaban_opr FROM `setting_assesmen_askep_param_opr_skor` H
			LEFT JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='387'
			WHERE H.parameter_id='$parameter_id' AND H.staktif='1'
			GROUP BY H.jenis";
	  $hasil='';
	  $list_data=$this->db->query($q)->result();
	  foreach ($list_data as $r){
		  $jawaban_opr=$r->jawaban_opr;
		  $arr=explode('#',$jawaban_opr);
		  $hasil .=($r->nama_jenis).' : ';
		  foreach($arr as $index=>$val){
			  $hasil .='<span class="badge badge-primary">'.$val.'</span> ';
		  }
		  $hasil .='<br>';
		  $hasil .='<br>';
	  }
	  return $hasil;
  }
  function hapus_param_opr(){
	  $param_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		// $data['deleted_by']=$this->session->userdata('user_id');
		// $data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$param_id);
		$hasil=$this->db->update('setting_assesmen_askep_param_opr',$data);
	  
	  json_encode($hasil);
	  
  }
	function find_jawaban_opr(){
	  $skor_id=$this->input->post('id');
	  $q="SELECT *FROM setting_assesmen_askep_param_opr_skor H WHERE H.id='$skor_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function find_param_opr(){
	  $param_id=$this->input->post('id');
	  $q="SELECT *FROM setting_assesmen_askep_param_opr H WHERE H.id='$param_id'";
	  $hasil=$this->db->query($q)->row_array();
	  // print_r($hasil);exit;
	  $this->output->set_output(json_encode($hasil));
  }
  function load_jawaban_opr()
	{
		$parameter_id=$this->input->post('parameter_id');
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			
			$this->select = array();
			$from="
					(
						SELECT H.*,M.ref as nama_jenis 
							FROM `setting_assesmen_askep_param_opr_skor` H
							LEFT JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='387'
							where H.staktif='1' AND H.parameter_id='$parameter_id'
							ORDER BY H.id


					) as tbl
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('deskripsi_nama');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
			 $aksi='';
          $result[] = $r->skor;
          $result[] = ($r->nama_jenis);
          $result[] = $r->deskripsi_nama;
        
          $aksi = '<div class="btn-group">';
		  $aksi .= '<button onclick="edit_jawaban_opr('.$r->id.')" type="button" title="Edit" class="btn btn-primary btn-xs "><i class="fa fa-pencil"></i></button>';	
		  $aksi .= '<button onclick="hapus_jawaban_opr('.$r->id.')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function simpan_jawaban_opr(){
		$skor_id=$this->input->post('skor_id');
		$parameter_id=$this->input->post('param_id');
		$data=array(
			'parameter_id'=>$this->input->post('param_id'),
			'skor'=>$this->input->post('skor'),
			'jenis'=>$this->input->post('jenis'),
			'deskripsi_nama'=>$this->input->post('deskripsi_nama'),
			'staktif'=>1,
		);
		if ($skor_id==''){
			$data['created_by']=$this->session->userdata('user_id');
			$data['created_date']=date('Y-m-d H:i:s');
		    $hasil=$this->db->insert('setting_assesmen_askep_param_opr_skor',$data);
		}else{
			$data['edited_by']=$this->session->userdata('user_id');
			$data['edited_date']=date('Y-m-d H:i:s');
			$this->db->where('id',$skor_id);
		    $hasil=$this->db->update('setting_assesmen_askep_param_opr_skor',$data);
		}
		  
		  json_encode($hasil);
	}
	 function hapus_jawaban_opr(){
	  $skor_id=$this->input->post('id');
	  $data=array(
			'staktif'=>0,
		);
		
		$data['deleted_by']=$this->session->userdata('user_id');
		$data['deleted_date']=date('Y-m-d H:i:s');
		$this->db->where('id',$skor_id);
		$hasil=$this->db->update('setting_assesmen_askep_param_opr_skor',$data);
	  
	  json_encode($hasil);
	  
  }
	
}
