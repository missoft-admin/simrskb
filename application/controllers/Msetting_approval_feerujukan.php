<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_approval_feerujukan extends CI_Controller {

	/**
	 * Setting Approval Fee Rujukan controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Msetting_approval_feerujukan_model', 'model');
	}

	function index() {
		$data['error'] = '';
		$data['title'] = 'Setting Approval Fee Rujukan';
		$data['content'] = 'Msetting_approval_feerujukan/index';
		$data['breadcrum'] 	= array(
			array("RSKB Halmahera",'#'),
			array("Setting Approval Fee Rujukan",'#'),
			array("List",'msetting_honor_dokter')
		);

		$data['setting'] = $this->model->getSettingApproval();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function save() {
		if ($this->model->insertData()) {
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message_flash','data berhasil disimpan.');
			redirect('msetting_approval_feerujukan');
		}
	}

	function delete($id) {
		if ($this->model->deleteData($id)) {
			$this->session->set_flashdata('success',true);
			$this->session->set_flashdata('message_flash','data berhasil dihapus.');
			redirect('msetting_approval_feerujukan');
		}
	}
}
