<?php

defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Trujukan_rumahsakit_approval extends CI_Controller
{
	/**
	 * Approval Tagihan Fee Rujukan controller.
	 * Developer @gunalirezqimauludi
	 */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Trujukan_rumahsakit_approval_model', 'model');
	}

	public function index()
	{
		$data = [
			'notransaksi' => '',
			'idrujukan' => '',
			'tanggaljatuhtempo_dari' => date('d/m/Y'),
			'tanggaljatuhtempo_sampai' => date('d/m/Y'),
			'status' => '',
		];

		$data['error'] = '';
		$data['title'] = 'Approval Tagihan Fee Rujukan';
		$data['content'] = 'Trujukan_rumahsakit_approval/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Approval Tagihan Fee Rujukan', '#'],
			['List', 'trujukan_rumahsakit_approval']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter()
	{
		$data = [
			'notransaksi' => $this->input->post('notransaksi'),
			'idrujukan' => $this->input->post('idrujukan'),
			'tanggaljatuhtempo_dari' => $this->input->post('tanggaljatuhtempo_dari'),
			'tanggaljatuhtempo_sampai' => $this->input->post('tanggaljatuhtempo_sampai'),
			'status' => $this->input->post('status'),
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Approval Tagihan Fee Rujukan';
		$data['content'] = 'Trujukan_rumahsakit_approval/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Approval Tagihan Fee Rujukan', '#'],
			['List', 'trujukan_rumahsakit_approval']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function approval($id)
	{
		if ($this->model->approval($id)) {
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'Pengajuan Fee Rujukan telah di Approve.');
			redirect('Trujukan_rumahsakit_approval', 'location');
		}
	}

	public function reject($id)
	{
		if ($this->model->reject($id)) {
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'Pengajuan Fee Rujukan telah di Reject.');
			redirect('Trujukan_rumahsakit_approval', 'location');
		}
	}

	public function getIndex($uri = 'index')
	{
		$this->select = [
			'tfeerujukan_rumahsakit.*', 'mrumahsakit.jenis_berekanan'
		];
		$this->from = 'tfeerujukan_rumahsakit';
		$this->join = [
			['mrumahsakit', 'mrumahsakit.id = tfeerujukan_rumahsakit.idrujukan', '']
		];

		$this->where = [];
		$this->where = array_merge($this->where, ['status_approval !=' => 0]);

		if ($uri == 'filter') {
			if ($this->session->userdata('notransaksi') != null) {
				$this->where = array_merge($this->where, ['notransaksi' => $this->session->userdata('notransaksi')]);
			}
			if ($this->session->userdata('idrujukan') != 0) {
				$this->where = array_merge($this->where, ['idrujukan' => $this->session->userdata('idrujukan')]);
			}
			if ($this->session->userdata('tanggaljatuhtempo_dari') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal_jatuhtempo) >=' => YMDFormat($this->session->userdata('tanggaljatuhtempo_dari'))]);
			}
			if ($this->session->userdata('tanggaljatuhtempo_sampai') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal_jatuhtempo) <=' => YMDFormat($this->session->userdata('tanggaljatuhtempo_sampai'))]);
			}
			if ($this->session->userdata('status') != 0) {
				$this->where = array_merge($this->where, ['status' => $this->session->userdata('status')]);
			}
		}

		$this->order = [
			'id' => 'DESC'
		];

		$this->group = [];

		$this->column_search = ['notransaksi', 'namarujukan', 'tanggal_jatuhtempo', 'nominal'];
		$this->column_order = ['notransaksi', 'namarujukan', 'tanggal_jatuhtempo', 'nominal'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $row) {
			$no++;

			$result = [];

			$action = '<div class="btn-group">';
			if ($row->status_approval == 2) {
				$action .= '<a href="' . site_url() . 'trujukan_rumahsakit_approval/approval/' . $row->id . '" title="Setujui Pengajuan Fee Rujukan" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Setujui</a>';
				$action .= '<a href="' . site_url() . 'trujukan_rumahsakit_approval/reject/' . $row->id . '" title="Tolak Pengajuan Fee Rujukan" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Tolak</a>';
			}
			$action .= '<a href="' . site_url() . 'trujukan_rumahsakit_rincian/detail/' . $row->id . '" target="_blank" title="Rincian Pengajuan Fee Rujukan" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> Rincian</a>';
			$action .= '</div>';

			$result[] = $no;
			$result[] = $row->notransaksi;
			$result[] = $row->namarujukan;
			$result[] = DMYFormat($row->tanggal_jatuhtempo);
			$result[] = number_format($row->nominal);
			$result[] = status_feerujukan($row->status_approval);
			$result[] = $action;

			$data[] = $result;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		echo json_encode($output);
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
