<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tpiutang_history extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpiutang_history_model','model');
	}

	function index() {
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		$tgl_terakhir = date('Y-m-d', strtotime($hari_ini));
		$data=array(
			'tipe'=>'#',
			'cara_bayar'=>'#',
			'no_po'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'#',
			'tgl_trx1'=>$tgl_pertama,
			'tgl_trx2'=>date('d-m-Y'),
			'tanggaldaftar1'=>'',
			'tanggaldaftar2'=>'',
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		// $data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		// $data['list_poli'] 	= $this->model->list_poli();
		// $data['list_dokter'] 	= $this->model->list_dokter();
		// $data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['title'] 			= 'History Piutang';
		$data['content'] 		= 'Tpiutang_history/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("History Transaksi",'tpiutang_history/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('');
		
		
		
		$tipe_tagihan=$this->input->post('tipe_tagihan');
		$peg_id=$this->input->post('peg_id');
		
		$where1='';
		$where2='';
		$where='';
		
		if ($tipe_tagihan !='#'){
			$where .=" AND tipepegawai='$tipe_tagihan' ";
		}
		
		if ($peg_id !=''){
			if ($tipe_tagihan=='1'){
				$where .=" AND idpegawai='$peg_id' ";
				// $where2 .=" AND TP.idpegawai='$peg_id' ";
			}elseif ($tipe_tagihan=='2'){
				$where .=" AND iddokter='$peg_id' ";
			}
			
		}
		
		
		
        $from = "(
					SELECT 
					D.id,D.tipepegawai,D.idpegawai,D.iddokter
					,CASE WHEN D.tipepegawai='1' THEN 'PEGAWAI' ELSE 'DOKTER' END as tipe_nama
					,CASE WHEN D.tipepegawai='1' THEN MP.nama ELSE MD.nama END as nama
					,CASE WHEN D.tipepegawai='1' THEN MP.nip ELSE MD.nip END as nip
					,SUM(D.nominal) as total

					FROM tpiutang_detail D
					LEFT JOIN mpegawai MP ON MP.id=D.idpegawai AND D.tipepegawai='1'
					LEFT JOIN mdokter MD ON MD.id=D.iddokter AND D.tipepegawai='2'
					WHERE D.id is not null
					".$where."
					GROUP BY tipepegawai,idpegawai,iddokter
					ORDER BY D.tipepegawai ASC
				) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tpiutang_history/');
            $row[] = $r->id;
            $row[] = $r->tipepegawai;
            $row[] = $no;
            $row[] = ($r->tipepegawai=='1' ? '<span class="label label-primary">'.$r->tipe_nama.'</span>':'<span class="label label-success">'.$r->tipe_nama.'</span>');
            $row[] = $r->nip;
            $row[] = $r->nama;
            $row[] = number_format($r->total,0);
			 $idpeg_dok='';
			if ($r->tipepegawai=='1'){
				$idpeg_dok=$r->idpegawai;
			}else{
				$idpeg_dok=$r->iddokter;				
			}
			
			$aksi .= '<a title="Lihat Detail" href="'.$url.'detail/'.$r->tipepegawai.'/'.$idpeg_dok.'" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-th-list"></i></a>';
			$aksi .= '<a title="Lihat Detail" href="#" target="_blank" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';
			$aksi.='</div>';
            $row[] = $aksi;
           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	
	function js_pegawai_dokter()
	{
		if ($this->input->post('tipe')=='1' || $this->input->post('tipe')=='2'){
				
		$arr = $this->model->js_pegawai_dokter();
		}else{
			$arr='';
		}
		echo json_encode($arr);
	}
	function detail($tipe,$id) {
		$data=$this->model->get_header($tipe,$id);
		
		$data['error'] 			= '';
		$data['title'] 			= 'History Tagihan Karyawan';
		$data['content'] 		= 'Tpiutang_history/detail';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("History Tagihan Detail",'tpiutang_history'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function load_left()
	{
		$tahun 	= $this->input->post('tahun');
		$id_peg 	= $this->input->post('id_peg');
		$tipepegawai 	= $this->input->post('tipepegawai');
		$arr = $this->model->load_left($tipepegawai,$id_peg,$tahun);
		$this->output->set_output(json_encode($arr));
	}
	function load_detail_trx(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		
		$disabel=$this->input->post('disabel');
		// $piutang_id=$this->input->post('piutang_id');
		$tipepegawai=$this->input->post('tipepegawai');
		$id_peg=$this->input->post('id_peg');
		$ym=$this->input->post('ym');
		$where='';
		if ($tipepegawai=='1'){
			$where .=" AND TD.idpegawai='$id_peg'";
		}else{
			$where .=" AND TD.iddokter='$id_peg'";
		}
		
		$where .=" AND DATE_FORMAT(TD.tanggal_tagihan,'%Y%m')='$ym'";
		
		$where1='';
		
		$from = "(
					SELECT TH.no_piutang,TD.id,TD.pendaftaran_id,TD.kasir_id,TP.tanggaldaftar,TP.nopendaftaran,TP.no_medrec,CONCAT(TP.title,'. ',TP.namapasien) as namapasien 
					,TD.nominal
					,TH.tanggal_tagihan,TD.tipe,TD.jml_cicilan,K.tanggal as tanggal_kasir,'Rawat Jalan' as tipe_nama,TD.status_lunas,TD.piutang_id,TD.tipepegawai
					,TD.cicilan_ke
					from tpiutang_detail TD
					LEFT JOIN tpiutang TH ON TH.id=TD.piutang_id
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TD.pendaftaran_id
					LEFT JOIN mpoliklinik M ON TP.idpoliklinik=M.id
					LEFT JOIN mdokter D ON D.id=TP.iddokter
					LEFT JOIN tkasir K ON K.id=TD.kasir_id
					WHERE TD.tipe='1' AND TD.tipepegawai='$tipepegawai' ".$where."
					
					UNION ALL
					
					SELECT TH.no_piutang,TD.id,TD.pendaftaran_id,TD.kasir_id,TP.tanggal as tanggaldaftar,TP.nopenjualan as nopendaftaran,TP.nomedrec as no_medrec,TP.nama as namapasien 
					,TD.nominal
					,TH.tanggal_tagihan,TD.tipe,TD.jml_cicilan,K.tanggal as tanggal_kasir,'Pembelian Obat' as tipe_nama,TD.status_lunas,TD.piutang_id,TD.tipepegawai
					,TD.cicilan_ke
					from tpiutang_detail TD
					LEFT JOIN tpiutang TH ON TH.id=TD.piutang_id
					LEFT JOIN tpasien_penjualan TP ON TP.id=TD.pendaftaran_id					
					LEFT JOIN tkasir K ON K.id=TD.kasir_id
					WHERE TD.tipe='3' AND TD.tipepegawai='$tipepegawai' ".$where."
					GROUP BY TP.id
					
					UNION ALL
					
					SELECT TH.no_piutang,TD.id,TD.pendaftaran_id,TD.kasir_id,TP.tanggaldaftar,TP.nopendaftaran,TP.no_medrec,CONCAT(TP.title,'. ',TP.namapasien) as namapasien 
					,TBD.nominal,TD.tanggal_tagihan,TD.tipe,TD.jml_cicilan,TB.tanggal as tanggal_kasir,'RI/ODS' as tipe_nama,TD.status_lunas,TD.piutang_id,TD.tipepegawai
					,TD.cicilan_ke
					from tpiutang_detail TD
					LEFT JOIN tpiutang TH ON TH.id=TD.piutang_id
					LEFT JOIN trawatinap_pendaftaran TP ON TP.id=TD.pendaftaran_id
				    LEFT JOIN trawatinap_tindakan_pembayaran TB ON TB.idtindakan=TP.id AND TB.statusbatal=0
				    LEFT JOIN trawatinap_tindakan_pembayaran_detail TBD ON TBD.idtindakan=TB.id
					WHERE TD.tipe='2'  AND TD.tipepegawai='$tipepegawai'  ".$where."
					

				) as tbl ";
				
		
        $this->from             = $from;
		// print_r($from);exit();
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tpiutang_rincian/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->id;
            $row[] = $r->tipe;
            $row[] = ($r->no_piutang);
            $row[] = ($r->nopendaftaran);
            // $row[] = HumanDateShort($r->tanggal_kasir);
			 $row[] = ($r->tipe=='1' ? '<span class="label label-primary">'.$r->tipe_nama.'</span>':'<span class="label label-success">'.$r->tipe_nama.'</span>');
            $row[] = number_format($r->nominal,0);
            $row[] = ($r->cicilan_ke=='1' ? '<span class="label label-info">TAGIHAN BARU</span>':'<span class="label label-success">TAGIHAN LAMA</span>');
            $row[] = ($r->jml_cicilan=='1' ? '<span class="label label-danger">SATU KALI</span>':'<span class="label label-primary">CICILAN</span>');
            $row[] = ($r->jml_cicilan=='1' ? '<span class="label label-danger">-</span>':'<span class="label label-primary">CICILAN '.$r->cicilan_ke.'/'.$r->jml_cicilan.'</span>');
			$row[] = ($r->status_lunas=='1' ? '<span class="label label-success">LUNAS</span>':'<span class="label label-danger">BELUM LUNAS</span>');
           
			$aksi .= '<button '.$disabel.' class="view btn btn-xs btn-success" type="button"  title="Detail"><i class="fa fa-print"></i></button>';
			
			$aksi.='</div>';
            $row[] = $aksi;           
            $data[] = $row;
			$aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_trx/'.$r->piutang_id.'/'.$r->tipepegawai.'" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-th-list"></i></a>';
			// if ($r->tipe=='1'){
			// $aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_kasir.'print_transaksi/'.$r->pendaftaran_id.'/'.$r->kasir_id.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-print"></i></a>';
			// }
			// $aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_tagihan/'.$r->id.'/'.$r->tipe.'" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></a>';
		
        }
	
        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(true),
            "recordsFiltered" => $this->datatable->count_all(true),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
}
