<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Trm_layanan_berkas extends CI_Controller {

    /**
     * Pelayanan Berkas Rekam Medis controller
     * Developer Acep Kursina
     */

    public function __construct() {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('trm_layanan_berkas_model','model');
    }
    
    public function index($iddariunit='#') {
        // print_r(date('d'));exit();
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// print_r($user_acces_form);exit();
		if (UserAccesForm($user_acces_form,array('1155'))){
			$data=array();
			
			$data['error'] = '';
			$data['title'] = 'Pelayanan Berkas Rekam Medis';
			$data['list_poli'] = $this->model->poli();
			$data['list_dokter'] = $this->model->list_dokter();
			if (UserAccesForm($user_acces_form,array('1155'))==false && UserAccesForm($user_acces_form,array('1161'))==true){
			redirect('trm_layanan_berkas/kembali');
				
			}else{
			$data['content'] = 'Trm_layanan_berkas/index';
			}
			$data['tombol'] = 'index';
			$data['breadcrum'] = [
				["RSKB Halmahera",'#'],
				["Pelayanan Berkas Rekam Medis",'trm_layanan_berkas/index'],
				["List",'#']
			];

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		
		}else{
			redirect('page404');
		}
    }
	function print_document($id, $tujuan,$id_trx){
      $dompdf = new Dompdf();
      $data = $this->model->getPrintInfo($id, $tujuan,$id_trx);
      $data['asal_pasien']=$tujuan;
		// print_r($data);exit();
      if($tujuan == 1 || $tujuan == 2 || $tujuan == 3 || $tujuan == 4){
        $html = $this->load->view('Trm_layanan_berkas/print_permintaan_dot', $data, TRUE);
      }else if($tujuan == 5){
        $html = $this->load->view('Trm_layanan_berkas/print_pinjam_dot', $data, TRUE);
      }
	  // print_r($html);exit();
  		$dompdf->loadHtml($html);

  	
  		// Render the HTML as PDF
  		$dompdf->render();

  		// // Output the generated PDF to Browser
  		$dompdf->stream('Cetak Faktur.pdf', array("Attachment"=>0));
    }
	function print_document_jasa($id, $tujuan,$id_trx){
      $dompdf = new Dompdf();
      $data = $this->model->getPrintInfo($id, $tujuan,$id_trx);
		// print_r($tujuan);exit();
      
      if($tujuan == 1 || $tujuan == 2 || $tujuan == 3 || $tujuan == 4){
        $html = $this->load->view('Trm_layanan_berkas/print_format_dot', $data, TRUE);
      }
	  // print_r($html);exit();
  		$dompdf->loadHtml($html);

  	
  		// Render the HTML as PDF
  		$dompdf->render();

  		// // Output the generated PDF to Browser
  		$dompdf->stream('Cetak Faktur.pdf', array("Attachment"=>0));
    }
	public function kembali() {
        // print_r(date('d'));exit();
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('267'))){
			$data=array();
			
			$data['error'] = '';
			$data['title'] = 'Pelayanan Berkas Rekam Medis';
			$data['list_poli'] = $this->model->poli();
			$data['list_dokter'] = $this->model->list_dokter();
			$data['content'] = 'Trm_layanan_berkas/kembali';
			$data['tombol'] = 'kembali';
			$data['breadcrum'] = [
				["RSKB Halmahera",'#'],
				["Pelayanan Berkas Rekam Medis",'trm_layanan_berkas/kembali'],
				["List",'#']
			];

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		
		}else{
			redirect('page404');
		}
    }
	
	function getIndex()
    {
		
		$tanggalsampai    	= $this->input->post('tanggalsampai');	
		$tanggaldari    	= $this->input->post('tanggaldari');	
		$idjenis     	= $this->input->post('idjenis');		
		$status_pasien     	= $this->input->post('status_pasien');		
		$tujuan     	= $this->input->post('tujuan');		
		$nomedrec     	= $this->input->post('nomedrec');		
		$namapasien     	= $this->input->post('namapasien');		
		$idpoliklinik     	= $this->input->post('idpoliklinik');		
		$iddokter     	= $this->input->post('iddokter');		
		$status_kirim     	= $this->input->post('status_kirim');		

		$iduser=$this->session->userdata('user_id');
		
		// $data_user=get_acces();
		// $user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		$where='';
		if ($idjenis  != '#'){
			$where .=" AND REF.jenis_pelayanan ='".$idjenis."'";
		}
		if ($status_pasien  != '#'){
			$where .=" AND REF.statuspasienbaru ='".$status_pasien."'";
		}
		if ($tujuan  != '#'){
			$where .=" AND REF.tujuan ='".$tujuan."'";
		}
		if ($idpoliklinik  != '#'){
			$where .=" AND REF.idpoliklinik ='".$idpoliklinik."'";
		}
		if ($iddokter  != '#'){
			$where .=" AND REF.iddokter ='".$iddokter."'";
		}
		if ($status_kirim  != '#'){
			$where .=" AND REF.status_kirim ='".$status_kirim."'";
		}
		if ($nomedrec  != ''){
			$where .=" AND REF.no_medrec LIKE '%".$nomedrec."%' ";
		}
		if ($namapasien  != ''){
			$where .=" AND REF.namapasien LIKE '%".$namapasien."%' ";
		}
		if ('' != $tanggaldari) {
			$where .= " AND (DATE(REF.tanggal_trx) >='".YMDFormat($tanggaldari)."'";
		}
		if ('' != $tanggalsampai) {
			$where .= " AND DATE(REF.tanggal_trx) <='".YMDFormat($tanggalsampai)."')";
		}
		$from="(SELECT REF.*,
				CASE 
					WHEN REF.tujuan='3'THEN 
						CONCAT(K.nama,'/',B.nama,' - ',D.nama) 
					WHEN REF.tujuan='5'THEN 
						REF.catatan 
					ELSE 
						CONCAT(P.nama,' - ',D.nama) 
					END as detail,
				CASE WHEN REF.statuspasienbaru='0' THEN 'PASIEN LAMA' ELSE 'PASIEN BARU' END as status_pasien, 
				U.name as nama_user_trx,M.lokasi_berkas
				FROM trm_layanan_berkas as REF
				LEFT JOIN mfpasien M ON M.id=REF.idpasien
				LEFT JOIN mdokter D ON D.id=REF.iddokter
				LEFT JOIN mpoliklinik P ON P.id=REF.idpoliklinik
				LEFT JOIN mbed B ON B.id=REF.bed
				LEFT JOIN musers U ON U.id=REF.user_trx
				LEFT JOIN mkelas K ON K.id=REF.kelas WHERE REF.id<>'' ".$where." ORDER BY tanggal_trx DESC) as tbl";
		
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;
        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        foreach ($list as $r) {
            $no++;
            $row = array();			
            $row[] = $no;
            $row[] =HumanDateLong($r->tanggal_trx);
            $row[] = ($r->jenis_pelayanan=='1' ? text_success('PELAYANAN') : text_primary('PINJAM'));
            $row[] = ($r->statuspasienbaru=='1' ? text_danger('PASIEN BARU') : text_info('PASIEN LAMA'));
            $row[] = $r->no_medrec.'<br>'.$r->title.' '.$r->namapasien;;
            $row[] = $r->lokasi_berkas;
            $row[] = $this->tujuan($r->tujuan);
            $row[] = $r->detail;
			$aksi='<div class="btn-group">';
			if ($r->status=='1'){
				if ($r->status_kirim=='0'){
					if (UserAccesForm($user_acces_form,array('1158'))){
						$aksi.='<button class="btn btn-xs btn-primary kirim" title="Kirim Berkas"><i class="fa fa-send"></i></button>';
					}
				}
				if (UserAccesForm($user_acces_form,array('1159'))){
						$aksi .= '
							<div class="btn-group dropup">
							  <button class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown">
								<span class="fa fa-print"></span>
							  </button>
							<ul class="dropdown-menu">
							  <li>
								<a tabindex="-1" target="_blank" href="'.site_url().'trm_layanan_berkas/print_document/'.$r->id.'/'.$r->tujuan.'/'.$r->id_trx.'">Pinjam Berkas</a>
								<a tabindex="-1" target="_blank" href="'.site_url().'tpoliklinik_pendaftaran/print_document/'.$r->id_trx.'/1">Pendaftaran</a>
								<a tabindex="-1" target="_blank" href="'.site_url().'tpoliklinik_pendaftaran/print_document/'.$r->id_trx.'/7">Pendaftaran DM</a>
								<a tabindex="-1" target="_blank" href="'.site_url().'tpoliklinik_pendaftaran/print_document/'.$r->id_trx.'/2">Tracer</a>
								<a tabindex="-1" target="_blank" href="'.site_url().'tpoliklinik_pendaftaran/print_document/'.$r->id_trx.'/3">Kartu Status</a>
								<a tabindex="-1" target="_blank" href="'.site_url().'tpoliklinik_pendaftaran/print_document/'.$r->id_trx.'/4">KIB</a>
								<a tabindex="-1" target="_blank" href="'.site_url().'tpoliklinik_pendaftaran/manage_print/'.$r->id_trx.'">Sticker ID</a>
								<a tabindex="-1" target="_blank" href="'.site_url().'tpoliklinik_pendaftaran/print_document/'.$r->id_trx.'/6">SK Diagnosa</a>
							  </li>
							</ul>
							</div>
						  ';
					if ($r->tujuan=='1' || $r->tujuan=='2') {
						$aksi.='<a href="'.site_url().'trm_layanan_berkas/print_document_jasa/'.$r->id.'/'.$r->tujuan.'/'.$r->id_trx.'" target="_blank" class="btn btn-xs btn-info cetak" class="button" title="Format Layanan Jasa Dokter"><i class="si si-printer"></i></a>';
						
					}
				}
				if ($r->tujuan=='5'){
					if ($r->status_kirim=='0'){
						if (UserAccesForm($user_acces_form,array('1218'))){
						$aksi.='<button class="btn btn-xs btn-danger batal" title="Batal Berkas"><i class="fa fa-close"></i></button>';
						}
					}
				}
			}
			$aksi.='</div>';
			if ($r->status=='1'){
				$row[] = ($r->status_kirim=='0' ? text_danger('BELUM DIKIRIM') : text_success('TERKIRIM'));;
            }else{
				$row[] =  text_danger('DIBATALKAN') ;
				
			}
			$row[] =  ($r->status_kirim=='1' ? getDurasi($r->tanggal_trx,$r->tanggal_kirim) : getDurasi($r->tanggal_trx,date('Y-m-d H:i:s')));
            $row[] = $r->nama_user_trx.'<br>'.HumanDateLong($r->tanggal_trx);
            $row[] = $r->user_kirim_nama.'<br>'.HumanDateLong($r->tanggal_kirim);
            $row[] = $aksi;
            $row[] = $r->id;
            $row[] = $r->status_kirim;
            $data[] = $row;			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	function getKembali()
    {
		
		$tanggalsampai    	= $this->input->post('tanggalsampai');	
		$tanggaldari    	= $this->input->post('tanggaldari');	
		$idjenis     	= $this->input->post('idjenis');		
		$status_pasien     	= $this->input->post('status_pasien');		
		$tujuan     	= $this->input->post('tujuan');		
		$nomedrec     	= $this->input->post('nomedrec');		
		$namapasien     	= $this->input->post('namapasien');		
		$idpoliklinik     	= $this->input->post('idpoliklinik');		
		$iddokter     	= $this->input->post('iddokter');		
		$status_kirim     	= $this->input->post('status_kirim');		

		$iduser=$this->session->userdata('user_id');
		
		// $data_user=get_acces();
		// $user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		$where='';
		if ($idjenis  != '#'){
			$where .=" AND REF.jenis_pelayanan ='".$idjenis."'";
		}
		if ($status_pasien  != '#'){
			$where .=" AND REF.statuspasienbaru ='".$status_pasien."'";
		}
		if ($tujuan  != '#'){
			$where .=" AND REF.tujuan ='".$tujuan."'";
		}
		if ($idpoliklinik  != '#'){
			$where .=" AND REF.idpoliklinik ='".$idpoliklinik."'";
		}
		if ($iddokter  != '#'){
			$where .=" AND REF.iddokter ='".$iddokter."'";
		}
		if ($status_kirim  != '#'){
			$where .=" AND REF.status_kirim ='".$status_kirim."'";
		}else{
			$where .=" AND REF.status_kirim IN ('1','2')";
		}
		if ($nomedrec  != ''){
			$where .=" AND REF.no_medrec LIKE '%".$nomedrec."%' ";
		}
		if ($namapasien  != ''){
			$where .=" AND REF.namapasien LIKE '%".$namapasien."%' ";
		}
		if ('' != $tanggaldari) {
			$where .= " AND (DATE(REF.tanggal_trx) >='".YMDFormat($tanggaldari)."'";
		}
		if ('' != $tanggalsampai) {
			$where .= " AND DATE(REF.tanggal_trx) <='".YMDFormat($tanggalsampai)."')";
		}
		$from="(SELECT REF.*,
					CASE 
					WHEN REF.tujuan='3'THEN 
						CONCAT(K.nama,'/',B.nama,' - ',D.nama) 
					WHEN REF.tujuan='5'THEN 
						REF.catatan 
					ELSE 
						CONCAT(P.nama,' - ',D.nama) 
					END as detail,
				CASE WHEN REF.statuspasienbaru='0' THEN 'PASIEN LAMA' ELSE 'PASIEN BARU' END as status_pasien, 
				U.name as nama_user_trx
				FROM trm_layanan_berkas as REF
				LEFT JOIN mdokter D ON D.id=REF.iddokter
				LEFT JOIN mpoliklinik P ON P.id=REF.idpoliklinik
				LEFT JOIN mbed B ON B.id=REF.bed
				LEFT JOIN musers U ON U.id=REF.user_trx
				LEFT JOIN mkelas K ON K.id=REF.kelas WHERE REF.id <>'' ".$where." ORDER BY tanggal_trx DESC) as tbl";
		
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;
        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        foreach ($list as $r) {
            $no++;
            $row = array();			
            $row[] = $no;
            $row[] =HumanDateLong($r->tanggal_trx);
            $row[] = ($r->jenis_pelayanan=='1' ? text_success('PELAYANAN') : text_primary('PINJAM'));
            $row[] = ($r->statuspasienbaru=='1' ? text_danger('PASIEN BARU') : text_info('PASIEN LAMA'));
            $row[] = $r->no_medrec;
            $row[] = $r->title.' '.$r->namapasien;
            $row[] = $this->tujuan($r->tujuan);
            $row[] = $r->detail;
			$aksi='<div class="btn-group">';
			if ($r->status_kirim=='1'){
				if ($r->status=='1'){
					if (UserAccesForm($user_acces_form,array('1162'))){
					$aksi.='<button class="btn btn-xs btn-primary kembali" title="Kembali Berkas"><i class="si si-arrow-left"></i></button>';
					}
					if (UserAccesForm($user_acces_form,array('1163'))){
					$aksi.='<button class="btn btn-xs btn-info edit" title="Edit Berkas"><i class="fa fa-pencil"></i></button>';
					}
				}
			}
			if ($r->status=='1'){
				if (UserAccesForm($user_acces_form,array('1164'))){
			$aksi.='<button class="btn btn-xs btn-success cetak" title="Print Berkas"><i class="fa fa-print"></i></button>';
				}
			}
			$aksi.='</div>';
			if ($r->status=='1'){
            $row[] = ($r->status_kirim=='1' ? text_danger('BELUM KEMBALI') : text_success('TELAH KEMBALI'));;
            $row[] =  ($r->status_kirim=='2' ? getDurasi($r->tanggal_kirim,$r->tanggal_kembali) : getDurasi($r->tanggal_kirim,date('Y-m-d H:i:s')));
            // $row[] =  getDurasi($r->tanggal_kirim,date('Y-m-d H:i:s'));
			}else{
				 $row[] = text_danger('DIBATALKAN');
				 $row[] = '';
			}
            $row[] = $r->nama_user_trx;
            $row[] = $r->user_kirim_nama;
            $row[] = $aksi;
            $row[] = $r->id;
            $row[] = $r->status_kirim;
            $data[] = $row;			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	function tujuan($id){
		if ($id=='1'){
			return '<span class="label label-success">POLIKLINIK</span>';
		}elseif($id=='2'){
			return '<span class="label label-danger">IGD</span>';
		}elseif($id=='3'){
			return '<span class="label label-primary">RAWAT INAP</span>';
		}elseif($id=='4'){
			return '<span class="label label-default">ODS</span>';
		}elseif($id=='5'){
			return '<span class="label label-warning">PINJAM</span>';
		}
	}
	public function kirim() {
				
        $id     = $this->input->post('id');
        $data =array(
			'status_kirim'=>'1',
			'user_kirim_id'=>$this->session->userdata('user_id'),
			'user_kirim_nama'=>$this->session->userdata('user_name'),
			'tanggal_kirim'=>date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		$result=$this->db->update('trm_layanan_berkas',$data);
		if ($result) {
			return true;
		}else{
			return false;
		}
    }
	
	public function set_batal(){
				
        $id     = $this->input->post('id');
        $data =array(
			'status'=>'0',
			'user_batal_id'=>$this->session->userdata('user_id'),
			'user_batal_nama'=>$this->session->userdata('user_name'),
			'tanggal_batal'=>date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		$result=$this->db->update('trm_layanan_berkas',$data);
		if ($result) {
			return true;
		}else{
			return false;
		}
    }
	public function kirim_semua() {
				
        $id     = $this->input->post('id');
        $data	= array(
			'status_kirim'=>'1',
			'user_kirim_id'=>$this->session->userdata('user_id'),
			'user_kirim_nama'=>$this->session->userdata('user_name'),
			'tanggal_kirim'=>date('Y-m-d H:i:s'),
		);
		
		$this->db->where_in('id', $id);
		$result=$this->db->update('trm_layanan_berkas',$data);
		if ($result) {
			return true;
		}else{
			return false;
		}
    }
	public function set_kembali(){
				
        $id     = $this->input->post('id');
        $data =array(
			'status_kirim'=>'2',
			'user_kembali'=>$this->session->userdata('user_id'),
			'user_kembali_nama'=>$this->session->userdata('user_name'),
			'tanggal_kembali'=>date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		$result=$this->db->update('trm_layanan_berkas',$data);
		if ($result) {
			return true;
		}else{
			return false;
		}
    }
	public function set_kembali_edit(){
				
        $id     = $this->input->post('id');
        $tanggalkembali     = $this->input->post('tanggalkembali');
        $data =array(
			'status_kirim'=>'2',
			'user_kembali'=>$this->session->userdata('user_id'),
			'user_kembali_nama'=>$this->session->userdata('user_name'),
			'tanggal_kembali'=>YMDTimeFormat($tanggalkembali),
		);
		$this->db->where('id',$id);
		$result=$this->db->update('trm_layanan_berkas',$data);
		if ($result) {
			return true;
		}else{
			return false;
		}
    }
	public function kembali_semua() {
				
        $id     = $this->input->post('id');
        $data =array(
			'status_kirim'=>'2',
			'user_kembali'=>$this->session->userdata('user_id'),
			'user_kembali_nama'=>$this->session->userdata('user_name'),
			'tanggal_kembali'=>date('Y-m-d H:i:s'),
		);
		// $names = array('Frank', 'Todd', 'James');
		$this->db->where_in('id', $id);
		// $this->db->where('id',$id);
		$result=$this->db->update('trm_layanan_berkas',$data);
		if ($result) {
			return true;
		}else{
			return false;
		}
    }
	
	//BATAS DELETE
	
}
