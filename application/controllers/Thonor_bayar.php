<?php defined('BASEPATH') or exit('No direct script access allowed');

class Thonor_bayar extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Thonor_bayar_model', 'model');
		$this->load->model('Tbagi_hasil_bayar_model');
		$this->load->model('Mrka_bendahara_model');
		$this->load->model('Thonor_dokter_model');
		$this->load->model('Tvalidasi_model');
	}

	public function InsertJurnalKas_HonorDokter($id)
	{
		$this->Tvalidasi_model->InsertJurnalKas_HonorDokter($id);
	}

	public function index()
	{
		$data = [
			'status_approval' => '2',
			'status_pembayaran' => '#',
			'deskripsi' => '',
			'no_terima' => '',
			'tanggal_pembayaran1' => '',
			'tanggal_pembayaran2' => '',
			'tanggal_jatuhtempo1' => '',
			'tanggal_jatuhtempo2' => '',
		];
		$data['error'] = '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		// $data['list_mbagi_hasil'] 	= $this->model->list_mbagi_hasil();
		$data['title'] = 'Pembayaran Honor Dokter';
		$data['content'] = 'Thonor_bayar/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Honor Dokter Transaksi', 'thonor_bayar/index'],
			['List', '#']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function get_index()
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		$userid = $this->session->userdata('user_id');
		$iduser = $this->session->userdata('user_id');

		$notransaksi = $this->input->post('notransaksi');
		$iddokter = $this->input->post('iddokter');
		$status_approval = $this->input->post('status_approval');
		$status_pembayaran = $this->input->post('status_pembayaran');

		$tanggal_jatuhtempo1 = $this->input->post('tanggal_jatuhtempo1');
		$tanggal_jatuhtempo2 = $this->input->post('tanggal_jatuhtempo2');
		$tanggal_pembayaran1 = $this->input->post('tanggal_pembayaran1');
		$tanggal_pembayaran2 = $this->input->post('tanggal_pembayaran2');

		$where = '';
		if ($notransaksi != '') {
			$where .= " AND H.notransaksi='$notransaksi' ";
		}
		if ($iddokter != '#') {
			$where .= " AND H.iddokter='$iddokter' ";
		}
		if ($status_approval != '#') {
			$where .= " AND H.status_approval='$status_approval' ";
		}
		if ($status_pembayaran != '#') {
			$where .= " AND H.status_pembayaran='$status_pembayaran' ";
		}

		if ('' != $tanggal_jatuhtempo1) {
			$where .= " AND DATE(H.tanggal_jatuhtempo) >='" . YMDFormat($tanggal_jatuhtempo1) . "' AND DATE(H.tanggal_jatuhtempo) <='" . YMDFormat($tanggal_jatuhtempo2) . "'";
		}
		if ('' != $tanggal_pembayaran1) {
			$where .= " AND DATE(H.tanggal_pembayaran) >='" . YMDFormat($tanggal_pembayaran1) . "' AND DATE(H.tanggal_pembayaran) <='" . YMDFormat($tanggal_pembayaran2) . "'";
		}

		$from = "(
					SELECT H.status_email, H.id,H.notransaksi,H.namadokter,H.tanggal_pembayaran,H.tanggal_jatuhtempo,H.nominal,H.status_pembayaran,H.status_approval,H.st_verifikasi 
					FROM thonor_dokter H 
					WHERE H.`status`='1' " . $where . '
				) as tbl ';

		// print_r($from);exit();
		$this->from = $from;
		$this->join = [];
		$this->where = [];
		$this->where_in = [];
		$this->order = [];
		$this->group = [];
		$this->column_search = ['notransaksi', 'namadokter'];
		$this->column_order = [];
		$this->select = [];
		$this->load->library('datatables');
		$list = $this->datatable->get_datatables();
		$data = [];
		$no = null;
		if (isset($_POST['start'])) {
			$no = $_POST['start'];
		}

		foreach ($list as $r) {
			$no++;
			$row = [];
			$aksi = '<div class="btn-group">';
			if ($r->status_approval == '2') {
				if ($r->status_pembayaran == '1') {
					if ($r->st_verifikasi == '1') {
						$aksi .= '<a href="' . site_url('thonor_bayar') . '/detail_bayar/' . $r->id . '/disabled" title="Pembayaran" class="btn btn-xs btn-default"><i class="fa fa-credit-card"></i></a>';
					} else {
						$aksi .= '<a href="' . site_url('thonor_bayar') . '/detail_bayar/' . $r->id . '" title="Pembayaran" class="btn btn-xs btn-default"><i class="fa fa-credit-card"></i></a>';
						$aksi .= '<button title="Pembayaran" class="btn btn-xs btn-danger verif" onclick="verif(' . $r->id . ')"><i class="fa fa-check"></i> Verifikasi</button>';
					}
				} else {
					$aksi .= '<a href="' . site_url('thonor_bayar') . '/detail_bayar/' . $r->id . '" title="Pembayaran" class="btn btn-xs btn-primary"><i class="fa fa-credit-card"></i></a>';
				}
			}
			$aksi .= '<button title="Print" class="btn btn-xs btn-success" disabled><i class="fa fa-print"></i></button>';

			$aksi .= '<a href="' . site_url() . 'thonor_bayar/upload_document/' . $r->id . '" title="Upload Dokumen" class="btn btn-danger btn-xs"><i class="fa fa-upload"></i></a>';

			if ($r->status_email == 0) {
				$aksi .= '<a href="' . site_url() . 'thonor_email/send/' . $r->id . '" target="_blank" title="Kirim Dokumen Honor Dokter" class="btn btn-warning btn-xs"><i class="fa fa-envelope"></i></a>';
			}

			$row[] = $r->id;
			$row[] = $no;
			$row[] = $r->notransaksi;
			$row[] = $r->namadokter;
			$row[] = HumanDateShort($r->tanggal_pembayaran);
			$row[] = number_format($r->nominal, 2);
			$row[] = HumanDateShort($r->tanggal_jatuhtempo);
			$row[] = ($r->status_pembayaran == '1' ? text_success('TELAH DIBAYAR') : text_default('BELUM DIBAYAR')) . ($r->st_verifikasi == '1' ? '<br><br>' . text_primary('TELAH DIVERIFIKASI') : '');
			$row[] = ($r->status_approval == '0' ? text_danger('BELUM DIPROSES') : status_approval_HD($r->status_approval, ''));

			$aksi .= '</div>';
			$row[] = $aksi; //8
			$data[] = $row;
		}

		$draw = null;
		if (isset($_POST['draw'])) {
			$draw = $_POST['draw'];
		}

		$output = [
			'draw' => $draw,
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data
		];
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($output, JSON_PRETTY_PRINT));
	}

	public function detail_bayar($id, $disabel = '')
	{
		$data = $this->model->get_header($id);
		// print_r($data);exit();
		$data['list_rekening'] = $this->model->list_rekening($data['iddokter']);
		$data['list_pembayaran'] = $this->model->list_pembayaran($id);
		$data['list_jenis_kas'] = $this->Mrka_bendahara_model->list_jenis_kas();
		$data['idmetode'] = '#';
		$data['st_edit'] = '0';
		$data['error'] = '';
		$data['title'] = 'Pembayaran';
		$data['disabel'] = $disabel;
		$data['content'] = 'Thonor_bayar/manage_bayar';
		$data['breadcrum'] = [['RSKB Halmahera', '#'], ['Bagi Hasil Pembayaran', '#'], ['Tambah', '#']];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function save_pembayaran()
	{
		$idhonor = $this->input->post('idhonor');
		// $id=$this->model->save_pembayaran();
		if ($this->model->save_pembayaran()) {
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah disimpan.');
			redirect('thonor_bayar/detail_bayar/' . $idhonor, 'location');
		}
	}

	public function verifikasi($id)
	{
		$data_detail = [
			'verifikasi_at' => date('Y-m-d H:i:s'),
			'verifikasi_by' => $this->session->userdata('user_id'),
			'varifikasi_nama' => $this->session->userdata('user_name'),
			'st_verifikasi' => 1,
		];

		$this->db->where('id', $id);
		$result = $this->db->update('thonor_dokter', $data_detail);
		$this->Tvalidasi_model->InsertJurnalKas_HonorDokter($id);
		$this->output->set_output(json_encode($result));
	}

	// Upload Dokumen
	public function upload_document($idhonor)
	{
		$row = $this->model->getHeadDokumenUpload($idhonor);

		$data = [
			'idhonor' => $row->id,
			'notransaksi' => $row->notransaksi,
			'namadokter' => $row->namadokter,
			'tanggal_pembayaran' => $row->tanggal_pembayaran,
			'tanggal_jatuhtempo' => $row->tanggal_jatuhtempo,
        ];

		$data['error'] = '';
		$data['title'] = 'Upload Dokumen';
		$data['content'] = 'Thonor_bayar/upload_dokumen';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Pembayaran Honor Dokter', 'Thonor_dokter/index'],
			['Upload Dokumen', '#']
		];

		$data['listFiles'] = $this->model->getListUploadedDocument($idhonor);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function upload_files()
	{
		if (!file_exists('assets/upload/honor_pembayaran')) {
			mkdir('assets/upload/honor_pembayaran', 0755, true);
		}

		$files = [];
		foreach ($_FILES as $file) {
			if ($file['name'] != '') {
				$config['upload_path'] = './assets/upload/honor_pembayaran/';
				$config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';

				$files[] = $file['name'];

				$this->upload->initialize($config);

				if ($this->upload->do_upload('file')) {
					$file_upload = $this->upload->data();

					$data = [];
					$data['idhonor'] = $this->input->post('idhonor');
					$data['filename'] = $file_upload['file_name'];
					$data['size'] = formatSizeUnits($file['size']);

					$this->db->insert('thonor_pembayaran_dokumen', $data);

					return true;
				} else {
					print_r($this->upload->display_errors());
					exit();

					return false;
				}
			} else {
				return true;
			}
		}
	}

	public function delete_file($idfile)
	{
		$this->db->where('id', $idfile);
		$query = $this->db->get('thonor_pembayaran_dokumen');
		$row = $query->row();

		if ($query->num_rows() > 0) {
			$this->db->where('id', $idfile);
			if ($this->db->delete('thonor_pembayaran_dokumen')) {
				if (file_exists('./assets/upload/honor_pembayaran/' . $row->filename) && $row->filename != '') {
					unlink('./assets/upload/honor_pembayaran/' . $row->filename);
				}
			}
		}

		return true;
	}
}
