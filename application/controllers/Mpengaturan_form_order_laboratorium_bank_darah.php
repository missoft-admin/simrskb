<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpengaturan_form_order_laboratorium_bank_darah extends CI_Controller {

	/**
	 * Pengaturan Form Permintaan Laboratorium Bank Darah controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpengaturan_form_order_laboratorium_bank_darah_model');
		$this->load->helper('path');
  }

	function index()
	{
		$row = $this->Mpengaturan_form_order_laboratorium_bank_darah_model->getSpecified(1);
		if(isset($row->id)){
			$data = array(
				'id' => $row->id,
				'pesan_informasi_berhasil' => $row->pesan_informasi_berhasil,
				'label_judul' => $row->label_judul,
				'label_judul_eng' => $row->label_judul_eng,
				'label_tujuan_laboratorium' => $row->label_tujuan_laboratorium,
				'label_tujuan_laboratorium_eng' => $row->label_tujuan_laboratorium_eng,
				'required_tujuan_laboratorium' => $row->required_tujuan_laboratorium,
				'label_dokter_peminta_pemeriksaan' => $row->label_dokter_peminta_pemeriksaan,
				'label_dokter_peminta_pemeriksaan_eng' => $row->label_dokter_peminta_pemeriksaan_eng,
				'required_dokter_peminta_pemeriksaan' => $row->required_dokter_peminta_pemeriksaan,
				'label_diagnosa' => $row->label_diagnosa,
				'label_diagnosa_eng' => $row->label_diagnosa_eng,
				'required_diagnosa' => $row->required_diagnosa,
				'label_catatan_pemeriksaan' => $row->label_catatan_pemeriksaan,
				'label_catatan_pemeriksaan_eng' => $row->label_catatan_pemeriksaan_eng,
				'required_catatan_pemeriksaan' => $row->required_catatan_pemeriksaan,
				'label_waktu_permintaan' => $row->label_waktu_permintaan,
				'label_waktu_permintaan_eng' => $row->label_waktu_permintaan_eng,
				'required_waktu_permintaan' => $row->required_waktu_permintaan,
				'label_prioritas' => $row->label_prioritas,
				'label_prioritas_eng' => $row->label_prioritas_eng,
				'required_prioritas' => $row->required_prioritas,
				'label_indikasi_transfusi' => $row->label_indikasi_transfusi,
				'label_indikasi_transfusi_eng' => $row->label_indikasi_transfusi_eng,
				'required_indikasi_transfusi' => $row->required_indikasi_transfusi,
				'label_hb' => $row->label_hb,
				'label_hb_eng' => $row->label_hb_eng,
				'required_hb' => $row->required_hb,
				'label_riwayat_transfusi_sebelumnya' => $row->label_riwayat_transfusi_sebelumnya,
				'label_riwayat_transfusi_sebelumnya_eng' => $row->label_riwayat_transfusi_sebelumnya_eng,
				'required_riwayat_transfusi_sebelumnya' => $row->required_riwayat_transfusi_sebelumnya,
				'label_riwayat_kehamilan_sebelumnya' => $row->label_riwayat_kehamilan_sebelumnya,
				'label_riwayat_kehamilan_sebelumnya_eng' => $row->label_riwayat_kehamilan_sebelumnya_eng,
				'required_riwayat_kehamilan_sebelumnya' => $row->required_riwayat_kehamilan_sebelumnya,
				'label_keterangan_lainnya' => $row->label_keterangan_lainnya,
				'label_keterangan_lainnya_eng' => $row->label_keterangan_lainnya_eng,
				'required_keterangan_lainnya' => $row->required_keterangan_lainnya,
				'label_catatan' => $row->label_catatan,
				'label_catatan_eng' => $row->label_catatan_eng,
				'required_catatan' => $row->required_catatan,
			);
			$data['error'] 			= '';
			$data['title'] 			= 'Ubah Pengaturan Form Permintaan Laboratorium Bank Darah';
			$data['content']	 	= 'Mpengaturan_form_order_laboratorium_bank_darah/index';
			$data['breadcrum'] 	= array(
															array("RSKB Halmahera",'#'),
															array("Pengaturan Form Permintaan Laboratorium Bank Darah",'#'),
															array("Ubah",'mpengaturan_form_order_laboratorium_bd')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mpengaturan_form_order_laboratorium_bd/index','location');
		}
	}

	function save()
	{
		if($this->Mpengaturan_form_order_laboratorium_bank_darah_model->saveData()){
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			redirect('mpengaturan_form_order_laboratorium_bd/index','location');
		}
	}
}
