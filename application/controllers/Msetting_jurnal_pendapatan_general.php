<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_jurnal_pendapatan_general extends CI_Controller {

	/**
	 * Setting Jurnal Pendapatan General controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_jurnal_pendapatan_general_model');
  }

	function index(){
		
		$data = $this->msetting_jurnal_pendapatan_general_model->getSpecified('1');
		$data['list_akun'] = $this->msetting_jurnal_pendapatan_general_model->list_akun();
		$data['list_group'] = $this->msetting_jurnal_pendapatan_general_model->list_group();
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Jurnal Pendapatan General';
		$data['content'] 		= 'Msetting_jurnal_pendapatan_general/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Jurnal Pendapatan General",'#'),
									    			array("List",'mlogic_kasbon')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function list_kategori($idrekanan){
		// $idrekanan=$this->input->post('idrekanan');
		$q="SELECT * FROM mdata_kategori M
			WHERE M.`status`='1' AND M.idrekanan='$idrekanan'
			ORDER BY M.nama ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}	
	function list_akun_group(){
		$q="SELECT H.id,H.nama,D.idakun,CONCAT(A.noakun,'-',A.namaakun) as akun FROM mgroup_pembayaran H
			LEFT JOIN mgroup_pembayaran_detail D ON H.id=D.idgroup AND D.`status`='1'
			LEFT JOIN makun_nomor A ON A.id=D.idakun
			WHERE H.`status`='1' AND A.id IS NOT NULL ";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.' ( '.$r->akun.' )</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	function list_rekanan($tipe){
			$opsi='';
		if ($tipe=='1'){
			$q="SELECT * FROM mrekanan M WHERE M.`status`='1' ORDER BY M.nama ASC";		
			$row=$this->db->query($q)->result();
			foreach($row as $r){
				$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
			}
		}else{
			// $opsi .='<option value="#">- All Rekanan -</option>';
		}
		
		
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}		
	public function list_kelompok()
    {
       $q="SELECT * FROM mpasien_kelompok M
			WHERE M.`status`='1' ORDER BY M.id ASC";
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->id.'">'.$r->nama.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
    }
	
	//RAJAL
	function update_rajal(){
		$id_edit=1;
		$data=array(
			'st_auto_posting_rajal'=>$this->input->post('st_auto_posting_rajal'),
			'idakun_tunai_rajal'=>$this->input->post('idakun_tunai_rajal'),
			'group_tuslah_racikan'=>$this->input->post('group_tuslah_racikan'),
			'batas_batal_rajal'=>$this->input->post('batas_batal_rajal'),
			'group_diskon_rajal'=>$this->input->post('group_diskon_rajal'),
			'group_tidak_tertagih_rajal'=>$this->input->post('group_tidak_tertagih_rajal'),
			'group_karyawan_rajal'=>$this->input->post('group_karyawan_rajal'),
			'group_round_rajal'=>$this->input->post('group_round_rajal'),
		);
		// print_r($data);exit();
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_pendapatan_general',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	function load_rajal()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.id,H.idkelompokpasien,H.idrekanan,K.nama as kelompokpasien,R.nama as rekanan 
				,CONCAT(A.noakun,' - ',A.namaakun) as akun,G.idakun,GH.nama as nama_group,H.idgroup
				FROM `msetting_jurnal_pendapatan_general_rajal` H
				LEFT JOIN mpasien_kelompok K ON K.id=H.idkelompokpasien
				LEFT JOIN mrekanan R ON R.id=H.idrekanan
				LEFT JOIN mgroup_pembayaran_detail G ON G.idgroup=H.idgroup
				LEFT JOIN mgroup_pembayaran GH ON GH.id=G.idgroup
				LEFT JOIN makun_nomor A ON A.id=G.idakun
				ORDER BY H.idkelompokpasien,H.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','rekanan','kelompokpasien');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = $r->kelompokpasien;
            $row[] = ($r->idrekanan=='0'?text_default('All Rekanan'):$r->rekanan);
            $row[] = $r->nama_group.' ('.$r->akun.') '.$r->idgroup;
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_rajal('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	function load_rajal_piutang()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.id,H.idkelompokpasien,H.idrekanan,K.nama as kelompokpasien,R.nama as rekanan 
				,CONCAT(A.noakun,' - ',A.namaakun) as akun,G.idakun,GH.nama as nama_group,H.idgroup
				FROM `msetting_jurnal_pendapatan_general_rajal_piutang` H
				LEFT JOIN mpasien_kelompok K ON K.id=H.idkelompokpasien
				LEFT JOIN mrekanan R ON R.id=H.idrekanan
				LEFT JOIN mgroup_pembayaran_detail G ON G.idgroup=H.idgroup
				LEFT JOIN mgroup_pembayaran GH ON GH.id=G.idgroup
				LEFT JOIN makun_nomor A ON A.id=G.idakun
				ORDER BY H.idkelompokpasien,H.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','rekanan','kelompokpasien');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = $r->kelompokpasien;
            $row[] = ($r->idrekanan=='0'?text_default('All Rekanan'):$r->rekanan);
            $row[] = $r->nama_group.' ('.$r->akun.') '.$r->idgroup;
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_rajal_piutang('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_rajal(){
		$id_edit=$this->input->post('id_edit');
		$idrekanan=($this->input->post('idrekanan')=='#'?0:$this->input->post('idrekanan'));
		$data=array(
			'setting_id'=>1,
			'idrekanan'=>$idrekanan,
			'idkelompokpasien'=>$this->input->post('idkelompokpasien'),
			'idgroup'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_rajal($data['idkelompokpasien'],$idrekanan)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pendapatan_general_rajal',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	function simpan_rajal_piutang(){
		$id_edit=$this->input->post('id_edit');
		$idrekanan=($this->input->post('idrekanan')=='#'?0:$this->input->post('idrekanan'));
		$data=array(
			'setting_id'=>1,
			'idrekanan'=>$idrekanan,
			'idkelompokpasien'=>$this->input->post('idkelompokpasien'),
			'idgroup'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_rajal_piutang($data['idkelompokpasien'],$idrekanan)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pendapatan_general_rajal_piutang',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_rajal_piutang($idkelompokpasien,$idrekanan){
		
		$gabung=$idkelompokpasien.'-'.$idrekanan;
		$q="SELECT *FROM msetting_jurnal_pendapatan_general_rajal_piutang S
			WHERE CONCAT(S.idkelompokpasien,'-',S.idrekanan)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}
	function cek_duplicate_rajal($idkelompokpasien,$idrekanan){
		
		$gabung=$idkelompokpasien.'-'.$idrekanan;
		$q="SELECT *FROM msetting_jurnal_pendapatan_general_rajal S
			WHERE CONCAT(S.idkelompokpasien,'-',S.idrekanan)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_rajal($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_pendapatan_general_rajal');
		echo json_encode($result);
	}
	function hapus_rajal_piutang($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_pendapatan_general_rajal_piutang');
		echo json_encode($result);
	}
	//RANAP
	function update_ranap(){
		$id_edit=1;
		$data=array(
			'st_auto_posting_ranap'=>$this->input->post('st_auto_posting_ranap'),
			'idakun_tunai_ranap'=>$this->input->post('idakun_tunai_ranap'),
			'batas_batal_ranap'=>$this->input->post('batas_batal_ranap'),
			'group_diskon_ranap'=>$this->input->post('group_diskon_ranap'),
			'group_tidak_tertagih_ranap'=>$this->input->post('group_tidak_tertagih_ranap'),
			'group_karyawan_ranap'=>$this->input->post('group_karyawan_ranap'),
			'group_round_ranap'=>$this->input->post('group_round_ranap'),
		);
		// print_r($data);exit();
		$this->db->where('id',$id_edit);
		$result = $this->db->update('msetting_jurnal_pendapatan_general',$data);		
		$this->output->set_output(json_encode($result));
		
	}
	function load_ranap()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.id,H.idkelompokpasien,H.idrekanan,K.nama as kelompokpasien,R.nama as rekanan 
				,CONCAT(A.noakun,' - ',A.namaakun) as akun,G.idakun,GH.nama as nama_group
				FROM `msetting_jurnal_pendapatan_general_ranap` H
				LEFT JOIN mpasien_kelompok K ON K.id=H.idkelompokpasien
				LEFT JOIN mrekanan R ON R.id=H.idrekanan
				LEFT JOIN mgroup_pembayaran_detail G ON G.idgroup=H.idgroup
				LEFT JOIN mgroup_pembayaran GH ON GH.id=G.idgroup
				LEFT JOIN makun_nomor A ON A.id=G.idakun
				ORDER BY H.idkelompokpasien,H.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','rekanan','kelompokpasien');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = $r->kelompokpasien;
            $row[] = ($r->idrekanan=='0'?text_default('All Rekanan'):$r->rekanan);
            $row[] = $r->nama_group.' ('.$r->akun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_ranap('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_ranap(){
		$id_edit=$this->input->post('id_edit');
		$idrekanan=($this->input->post('idrekanan')=='#'?0:$this->input->post('idrekanan'));
		$data=array(
			'setting_id'=>1,
			'idrekanan'=>$idrekanan,
			'idkelompokpasien'=>$this->input->post('idkelompokpasien'),
			'idgroup'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_ranap($data['idkelompokpasien'],$idrekanan)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pendapatan_general_ranap',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_ranap($idkelompokpasien,$idrekanan){
		
		$gabung=$idkelompokpasien.'-'.$idrekanan;
		$q="SELECT *FROM msetting_jurnal_pendapatan_general_ranap S
			WHERE CONCAT(S.idkelompokpasien,'-',S.idrekanan)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_ranap($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_pendapatan_general_ranap');
		echo json_encode($result);
	}
	//RANAP
	
	function load_ranap_piutang()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.id,H.idkelompokpasien,H.idrekanan,K.nama as kelompokpasien,R.nama as rekanan 
				,CONCAT(A.noakun,' - ',A.namaakun) as akun,G.idakun,GH.nama as nama_group
				FROM `msetting_jurnal_pendapatan_general_ranap_piutang` H
				LEFT JOIN mpasien_kelompok K ON K.id=H.idkelompokpasien
				LEFT JOIN mrekanan R ON R.id=H.idrekanan
				LEFT JOIN mgroup_pembayaran_detail G ON G.idgroup=H.idgroup
				LEFT JOIN mgroup_pembayaran GH ON GH.id=G.idgroup
				LEFT JOIN makun_nomor A ON A.id=G.idakun
				ORDER BY H.idkelompokpasien,H.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','rekanan','kelompokpasien');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = $r->kelompokpasien;
            $row[] = ($r->idrekanan=='0'?text_default('All Rekanan'):$r->rekanan);
            $row[] = $r->nama_group.' ('.$r->akun.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_ranap_piutang('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_ranap_piutang(){
		$id_edit=$this->input->post('id_edit');
		$idrekanan=($this->input->post('idrekanan')=='#'?0:$this->input->post('idrekanan'));
		$data=array(
			'setting_id'=>1,
			'idrekanan'=>$idrekanan,
			'idkelompokpasien'=>$this->input->post('idkelompokpasien'),
			'idgroup'=>$this->input->post('idakun')			
		);
		
		if ($this->cek_duplicate_ranap_piutang($data['idkelompokpasien'],$idrekanan)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pendapatan_general_ranap_piutang',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_ranap_piutang($idkelompokpasien,$idrekanan){
		
		$gabung=$idkelompokpasien.'-'.$idrekanan;
		$q="SELECT *FROM msetting_jurnal_pendapatan_general_ranap_piutang S
			WHERE CONCAT(S.idkelompokpasien,'-',S.idrekanan)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_ranap_piutang($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_pendapatan_general_ranap_piutang');
		echo json_encode($result);
	}
	function load_ranap_adm()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT H.id,H.idkelompokpasien,H.idrekanan,K.nama as kelompokpasien,R.nama as rekanan 
				,CONCAT(A.noakun,' - ',A.namaakun) as akun,G.idakun,GH.nama as nama_group
				,CONCAT(A2.noakun,' - ',A2.namaakun) as akun_diskon,G2.idakun as idakun_diskon,GH2.nama as nama_group_diskon
				FROM `msetting_jurnal_pendapatan_general_ranap_adm` H
				LEFT JOIN mpasien_kelompok K ON K.id=H.idkelompokpasien
				LEFT JOIN mrekanan R ON R.id=H.idrekanan
				
				LEFT JOIN mgroup_pembayaran_detail G ON G.idgroup=H.idgroup
				LEFT JOIN mgroup_pembayaran GH ON GH.id=G.idgroup
				LEFT JOIN makun_nomor A ON A.id=G.idakun
				
				LEFT JOIN mgroup_pembayaran_detail G2 ON G2.idgroup=H.idgroup_diskon
				LEFT JOIN mgroup_pembayaran GH2 ON GH2.id=G2.idgroup
				LEFT JOIN makun_nomor A2 ON A2.id=G2.idakun
				
				ORDER BY H.idkelompokpasien,H.id
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('akun','rekanan','kelompokpasien');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->id;            
            $row[] = $r->kelompokpasien;
            $row[] = ($r->idrekanan=='0'?text_default('All Rekanan'):$r->rekanan);
            $row[] = $r->nama_group.' ('.$r->akun.')';
            $row[] = $r->nama_group_diskon.' ('.$r->akun_diskon.')';
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_ranap_adm('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_ranap_adm(){
		$id_edit=$this->input->post('id_edit');
		$idrekanan=($this->input->post('idrekanan')=='#'?0:$this->input->post('idrekanan'));
		$data=array(
			'setting_id'=>1,
			'idrekanan'=>$idrekanan,
			'idkelompokpasien'=>$this->input->post('idkelompokpasien'),
			'idgroup'=>$this->input->post('idakun'),			
			'idgroup_diskon'=>$this->input->post('idakun_diskon')			
		);
		
		if ($this->cek_duplicate_ranap_adm($data['idkelompokpasien'],$idrekanan)){
			$this->output->set_output(json_encode(false));
		}else{
			$result = $this->db->insert('msetting_jurnal_pendapatan_general_ranap_adm',$data);		
			$this->output->set_output(json_encode($result));
		}
	}
	
	function cek_duplicate_ranap_adm($idkelompokpasien,$idrekanan){
		
		$gabung=$idkelompokpasien.'-'.$idrekanan;
		$q="SELECT *FROM msetting_jurnal_pendapatan_general_ranap_adm S
			WHERE CONCAT(S.idkelompokpasien,'-',S.idrekanan)='$gabung'";
		// print_r($q);exit();
		return $this->db->query($q)->row('id');
	}	
	function hapus_ranap_adm($id)
	{
		
		$this->db->where('id',$id);
		$result =$this->db->delete('msetting_jurnal_pendapatan_general_ranap_adm');
		echo json_encode($result);
	}
}
