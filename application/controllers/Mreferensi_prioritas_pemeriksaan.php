<?php

declare(strict_types=1);
defined('BASEPATH') || exit('No direct script access allowed');

class Mreferensi_prioritas_pemeriksaan extends CI_Controller
{
    /**
     * Referensi Prioritas Pemeriksaan controller.
     * Developer @gunalirezqimauludi.
     */
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<label>', '</label>');
        $this->load->model('Mreferensi_prioritas_pemeriksaan_model');
    }

    public function index(): void
    {
        $data = [];
        $data['error'] = '';
        $data['title'] = 'Referensi Prioritas Pemeriksaan';
        $data['content'] = 'Mreferensi_prioritas_pemeriksaan/index';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Referensi Prioritas Pemeriksaan', '#'],
            ['List', 'mreferensi_prioritas_pemeriksaan'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function create(): void
    {
        $data = [
            'id' => '',
            'noid' => '',
            'nama' => '',
            'status_default' => '',
            'status' => '',
        ];

        $data['error'] = '';
        $data['title'] = 'Tambah Referensi Prioritas Pemeriksaan';
        $data['content'] = 'Mreferensi_prioritas_pemeriksaan/manage';
        $data['breadcrum'] = [
            ['RSKB Halmahera', '#'],
            ['Referensi Prioritas Pemeriksaan', '#'],
            ['Tambah', 'mreferensi_prioritas_pemeriksaan'],
        ];

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function update($id): void
    {
        if ('' !== $id) {
            $row = $this->Mreferensi_prioritas_pemeriksaan_model->getSpecified($id);
            if (isset($row->id)) {
                $data = [
                    'id' => $row->id,
                    'noid' => $row->noid,
                    'nama' => $row->nama,
                    'status_default' => $row->status_default,
                    'status' => $row->status,
                ];
                $data['error'] = '';
                $data['title'] = 'Ubah Referensi Prioritas Pemeriksaan';
                $data['content'] = 'Mreferensi_prioritas_pemeriksaan/manage';
                $data['breadcrum'] = [
                    ['RSKB Halmahera', '#'],
                    ['Referensi Prioritas Pemeriksaan', '#'],
                    ['Ubah', 'mreferensi_prioritas_pemeriksaan'],
                ];

                $data = array_merge($data, backend_info());
                $this->parser->parse('module_template', $data);
            } else {
                $this->session->set_flashdata('error', true);
                $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
                redirect('mreferensi_prioritas_pemeriksaan', 'location');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
            redirect('mreferensi_prioritas_pemeriksaan');
        }
    }

    public function delete($id): void
    {
        $this->Mreferensi_prioritas_pemeriksaan_model->softDelete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('mreferensi_prioritas_pemeriksaan', 'location');
    }

    public function save(): void
    {
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');

        if (true === $this->form_validation->run()) {
            if ('' === $this->input->post('id')) {
                if ($this->Mreferensi_prioritas_pemeriksaan_model->saveData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mreferensi_prioritas_pemeriksaan', 'location');
                }
            } else {
                if ($this->Mreferensi_prioritas_pemeriksaan_model->updateData()) {
                    $this->session->set_flashdata('confirm', true);
                    $this->session->set_flashdata('message_flash', 'data telah disimpan.');
                    redirect('mreferensi_prioritas_pemeriksaan', 'location');
                }
            }
        } else {
            $this->failed_save($this->input->post('id'));
        }
    }

    public function failed_save($id): void
    {
        $data = $this->input->post();
        $data['error'] = validation_errors();
        $data['content'] = 'Mreferensi_prioritas_pemeriksaan/manage';

        if ('' === $id) {
            $data['title'] = 'Tambah Referensi Prioritas Pemeriksaan';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Referensi Prioritas Pemeriksaan', '#'],
                ['Tambah', 'mreferensi_prioritas_pemeriksaan'],
            ];
        } else {
            $data['title'] = 'Ubah Referensi Prioritas Pemeriksaan';
            $data['breadcrum'] = [
                ['RSKB Halmahera', '#'],
                ['Referensi Prioritas Pemeriksaan', '#'],
                ['Ubah', 'mreferensi_prioritas_pemeriksaan'],
            ];
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function getIndex(): void
    {
        $data_user = get_acces();
        $user_acces_form = $data_user['user_acces_form'];

        $this->select = [];
        $this->from = 'merm_referensi_prioritas_pemeriksaan';
        $this->join = [];
        $this->where = [
            'status' => '1',
        ];
        $this->order = [
            'nama' => 'ASC',
        ];
        $this->group = [];

        $this->column_search = ['nama'];
        $this->column_order = ['nama'];

        $list = $this->datatable->get_datatables();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $r) {
            ++$no;
            $row = [];

            $row[] = $no;
            $row[] = $r->noid;
            $row[] = $r->nama;
            $row[] = StatusRow($r->status);
            $aksi = '<div class="btn-group">';
						$aksi .= '<a href="'.site_url().'mreferensi_prioritas_pemeriksaan/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
						$aksi .= '<a href="#" data-urlindex="'.site_url().'mreferensi_prioritas_pemeriksaan" data-urlremove="'.site_url().'mreferensi_prioritas_pemeriksaan/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
            $aksi .= '</div>';
            $row[] = $aksi;

            $data[] = $row;
        }
        $output = [
            'draw' => $_POST['draw'],
            'recordsTotal' => $this->datatable->count_all(),
            'recordsFiltered' => $this->datatable->count_all(),
            'data' => $data,
        ];
        echo json_encode($output);
    }
}
