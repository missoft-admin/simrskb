<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Tverifikasi_transaksi extends CI_Controller
{
	/**
	 * Verifikasi Transaksi controller.
	 * Developer @gunalirezqimauludi
	 */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tverifikasi_transaksi_model', 'model');
		$this->load->model('Trawatinap_tindakan_model');
		$this->load->model('Tpoliklinik_verifikasi_model');
		$this->load->model('Tpasien_penjualan_model');
		$this->load->model('Trawatinap_verifikasi_model');
		$this->load->model('Tkasir_model');
		$this->load->model('Thonor_dokter_model');
		$this->load->model('Mbagi_hasil_model');
		$this->load->model('Tvalidasi_model'); //Kumpulan Validasi disini
	}

	// public function insert_deposit($idpendaftaran,$idvalidasi='1'){
	// $this->Tvalidasi_model->insert_deposit($idpendaftaran,$idvalidasi);
	// }

	// function GenerateValidasiPendapatanRanap($idpendaftaran,$kasir_id,$tipe_trx='2',$tanggal_transaksi=''){
	// $tanggal_transaksi=date('Y-m-d');
	// $this->Tvalidasi_model->GenerateValidasiPendapatanRanap($idpendaftaran,$kasir_id,$tipe_trx,$tanggal_transaksi);
	// }

	// public function GenerateValidasiPendapatanRanapRefund($idrefund, $tipe_refund)
	// {
		// $this->Tvalidasi_model->GenerateValidasiPendapatanRanapRefund($idrefund, $tipe_refund);
	// }

	public function index($tipe)
	{
		$data = [
			'uri' => $this->uri->segment(2),
			'tipetransaksi' => $tipe,
			'tanggal_pendaftaran_awal' => $this->session->userdata('tanggal_pendaftaran_awal') != null ? $this->session->userdata('tanggal_pendaftaran_awal') : '',
			'tanggal_pendaftaran_akhir' => $this->session->userdata('tanggal_pendaftaran_akhir') != null ? $this->session->userdata('tanggal_pendaftaran_akhir') : '',
			'tanggal_pembayaran_awal' => $this->session->userdata('tanggal_pembayaran_awal') != null ? $this->session->userdata('tanggal_pembayaran_awal') : '',
			'tanggal_pembayaran_akhir' => $this->session->userdata('tanggal_pembayaran_akhir') != null ? $this->session->userdata('tanggal_pembayaran_akhir') : '',
			'idtipe' => $this->session->userdata('idtipe') != null ? $this->session->userdata('idtipe') : '',
			'idpoliklinik' => $this->session->userdata('idpoliklinik') != null ? $this->session->userdata('idpoliklinik') : '',
			'nomedrec' => $this->session->userdata('nomedrec') != null ? $this->session->userdata('nomedrec') : '',
			'namapasien' => $this->session->userdata('namapasien') != null ? $this->session->userdata('namapasien') : '',
			'idkelompokpasien' => $this->session->userdata('idkelompokpasien') != null ? $this->session->userdata('idkelompokpasien') : '',
			'idrekanan' => $this->session->userdata('idrekanan') != null ? $this->session->userdata('idrekanan') : '',
			'namapasien' => $this->session->userdata('namapasien') != null ? $this->session->userdata('namapasien') : '',
			'iduser' => $this->session->userdata('iduser') != null ? $this->session->userdata('iduser') : '',
			'iddokter' => $this->session->userdata('iddokter') != null ? $this->session->userdata('iddokter') : '',
			'nopendaftaran' => $this->session->userdata('nopendaftaran') != null ? $this->session->userdata('nopendaftaran') : '',
			'nokasir' => $this->session->userdata('nokasir') != null ? $this->session->userdata('nokasir') : '',
		];

		$data['error'] = '';
		$data['title'] = 'Verifikasi Transaksi';
		$data['content'] = 'Tverifikasi_transaksi/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Verifikasi Transaksi', 'tverifikasi_transaksi/index'],
			['List', '#']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter($tipe)
	{
		$data = [
			'uri' => $this->uri->segment(2),
			'tipetransaksi' => $tipe,
			'tanggal_pendaftaran_awal' => $this->input->post('tanggal_pendaftaran_awal'),
			'tanggal_pendaftaran_akhir' => $this->input->post('tanggal_pendaftaran_akhir'),
			'tanggal_pembayaran_awal' => $this->input->post('tanggal_pembayaran_awal'),
			'tanggal_pembayaran_akhir' => $this->input->post('tanggal_pembayaran_akhir'),
			'idtipe' => $this->input->post('idtipe'),
			'idpoliklinik' => $this->input->post('idpoliklinik'),
			'nomedrec' => $this->input->post('nomedrec'),
			'namapasien' => $this->input->post('namapasien'),
			'idkelompokpasien' => $this->input->post('idkelompokpasien'),
			'idrekanan' => $this->input->post('idrekanan'),
			'namapasien' => $this->input->post('namapasien'),
			'iduser' => $this->input->post('iduser'),
			'iddokter' => $this->input->post('iddokter'),
			'nopendaftaran' => $this->input->post('nopendaftaran'),
			'nokasir' => $this->input->post('nokasir'),
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Verifikasi Transaksi';
		$data['content'] = 'Tverifikasi_transaksi/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Verifikasi Transaksi', 'tverifikasi_transaksi/index'],
			['List', '#']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function index_deposit()
	{
		$data = [
			'tipetransaksi' => 'index',
			'tanggal_awal' => date('d/m/Y'),
			'tanggal_akhir' => date('d/m/Y'),
			'nomedrec' => '',
			'namapasien' => '',
			'iduserinput' => '',
			'idmetode' => '',
			'idbank' => '',
			'idtipe' => '',
		];

		$data['error'] = '';
		$data['title'] = 'Verifikasi Transaksi';
		$data['content'] = 'Tverifikasi_transaksi/index_deposit';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Verifikasi Transaksi', 'tverifikasi_transaksi/index'],
			['List', '#']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function index_refund()
	{
		$data = [
			'tipetransaksi' => 'index_refund',
			'tanggal_awal' => date('d/m/Y'),
			'tanggal_akhir' => date('d/m/Y'),
			'nomedrec' => '',
			'namapasien' => '',
			'iduserinput' => '',
			'idmetode' => '',
			'idbank' => '',
			'tipe' => '#',
		];
		$this->session->set_userdata($data);
		$data['error'] = '';
		$data['title'] = 'Verifikasi Transaksi';
		$data['content'] = 'Tverifikasi_transaksi/index_refund';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Verifikasi Transaksi', 'tverifikasi_transaksi/index'],
			['List', '#']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter_deposit()
	{
		$data = [
			'tipetransaksi' => 'filter',
			'tanggal_awal' => $this->input->post('tanggal_awal'),
			'tanggal_akhir' => $this->input->post('tanggal_akhir'),
			'nomedrec' => $this->input->post('nomedrec'),
			'namapasien' => $this->input->post('namapasien'),
			'iduserinput' => $this->input->post('iduserinput'),
			'idmetode' => $this->input->post('idmetode'),
			'idbank' => $this->input->post('idbank'),
			'idtipe' => $this->input->post('idtipe'),
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Verifikasi Transaksi';
		$data['content'] = 'Tverifikasi_transaksi/index_deposit';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Verifikasi Transaksi', 'tverifikasi_transaksi/index'],
			['List', '#']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter_refund()
	{
		$data = [
			'tipetransaksi' => 'filter',
			'tanggal_awal' => $this->input->post('tanggal_awal'),
			'tanggal_akhir' => $this->input->post('tanggal_akhir'),
			'nomedrec' => $this->input->post('nomedrec'),
			'namapasien' => $this->input->post('namapasien'),
			'iduserinput' => $this->input->post('iduserinput'),
			'tipe' => $this->input->post('tipe'),
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Verifikasi Transaksi';
		$data['content'] = 'Tverifikasi_transaksi/index_refund';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Verifikasi Transaksi', 'tverifikasi_transaksi/index'],
			['List', '#']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function edit_transaksi_rajal($idpendaftaran, $idkasir, $stedit = 1)
	{
		$data = [
			'id' => $idpendaftaran,
			'idkasir' => $idkasir,
			'stedit' => $stedit,
			'diskon_rp' => 0,
			'diskon_persen' => 0,
			'bayar_rp' => 0,
			'sisa_rp' => 0,
		];

		$data_header = $this->Tkasir_model->get_header_poli($idpendaftaran);
		if ($data_header) {
			$data['nopendaftaran'] = $data_header->nopendaftaran;
			$data['nomedrec'] = $data_header->no_medrec;
			$data['namapasien'] = $data_header->nama;
			$data['idtipepasien'] = $data_header->idtipepasien;
			$data['tipependaftaran'] = ($data_header->idtipe == '1' ? 'Poliklinik' : 'Instalasi Gawat Darurat');
			$data['poliklinik'] = $data_header->poli;
			$data['idkelompokpasien'] = $data_header->idkelompokpasien;
			$data['kelompokpasien'] = $data_header->kelompok;
			$data['bpjs_kesehatan'] = $data_header->tarif_bpjs_kes;
			$data['bpjs_ketenagakerjaan'] = $data_header->tarif_bpjs_ket;
			$data['namarekanan'] = $data_header->rekanan;
			$data['usertransaksi'] = $data_header->nama;
			$data['tanggaltransaksi'] = $data_header->tanggaldaftar;
			$data['namadokter'] = $data_header->dokter;
		}

		$data_header = $this->Tkasir_model->get_kasir($idkasir);
		if ($data_header) {
			$data['diskon_rp'] = $data_header->diskon;
			$data['bayar_rp'] = $data_header->total_bayar;
		}

		$data['error'] = '';
		$data['title'] = 'Detail Transaksi';
		$data['content'] = 'Tverifikasi_transaksi/edit_transaksi_rajal';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Verifikasi Transaksi', 'tverifikasi_transaksi/index'],
			['Detail Transaksi', '#']
		];

		$data['list_bank'] = $this->Tkasir_model->getBank();
		$data['detail_pembayaran'] = $this->Tkasir_model->list_pembayaran($idkasir);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function edit_transaksi_obatbebas($idpendaftaran, $idkasir, $stedit = 1)
	{
		$data = [
			'id' => $idpendaftaran,
			'idkasir' => $idkasir,
			'stedit' => $stedit,
			'diskon_rp' => 0,
			'diskon_persen' => 0,
			'bayar_rp' => 0,
			'sisa_rp' => 0,
		];

		$data_header = $this->Tpasien_penjualan_model->get_dataPenjualan($idpendaftaran);
		if ($data_header) {
			$data['tanggaltransaksi'] = $data_header->tanggal;
			$data['notransaksi'] = $data_header->nopenjualan;
			$data['nomedrec'] = ($data_header->nomedrec ? $data_header->nomedrec : '-');
			$data['namapasien'] = $data_header->nama;

			if ($data_header->idkategori == 1) {
				$data['kategori'] = 'Pasien RS';
			} elseif ($data_header->idkategori == 2) {
				$data['kategori'] = 'Pegawai';
			} elseif ($data_header->idkategori == 3) {
				$data['kategori'] = 'Dokter';
			}

			if ($data_header->statusresep == 0) {
				$data['statusresep'] = 'Tidak Ada';
			} elseif ($data_header->statusresep == 1) {
				$data['statusresep'] = 'Ada';
			}

			$data_header = $this->Tkasir_model->get_kasir($idkasir);
			if ($data_header) {
				$data['diskon_rp'] = $data_header->diskon;
				$data['bayar_rp'] = $data_header->total_bayar;
			}
		}

		$data['error'] = '';
		$data['title'] = 'Detail Transaksi';
		$data['content'] = 'Tverifikasi_transaksi/edit_transaksi_obatbebas';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Verifikasi Transaksi', 'tverifikasi_transaksi/index'],
			['Detail Transaksi', '#']
		];

		$data['list_bank'] = $this->Tkasir_model->getBank();
		$data['detail_pembayaran'] = $this->Tkasir_model->list_pembayaran($idkasir);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function edit_transaksi_ranap($idpendaftaran, $stedit = 1)
	{
		$row = $this->Trawatinap_tindakan_model->getDataRawatInap($idpendaftaran);
		if ($row->id) {
			$pembayaran = $this->Trawatinap_tindakan_model->pembayaran($idpendaftaran);
			$totaldeposit = $this->Trawatinap_tindakan_model->getTotalDeposit($idpendaftaran)->totaldeposit;

			$data = [
				'idtipe' => $row->idtipe,
				'idtipepasien' => $row->idtipepasien,
				'idpendaftaran' => $row->id,
				'nomedrec' => $row->nomedrec,
				'idpasien' => $row->idpasien,
				'namapasien' => $row->namapasien,
				'alamatpasien' => $row->alamatpasien,
				'nohp' => ($row->hp ? $row->hp : $row->telepon),
				'namapenanggungjawab' => $row->nama_keluarga,
				'telppenanggungjawab' => $row->telepon_keluarga,
				'idkelompokpasien' => $row->idkelompokpasien,
				'namakelompok' => $row->namakelompok,
				'namadokterpenanggungjawab' => $row->namadokterpenanggungjawab,
				'idruangan' => $row->idruangan,
				'idkelas' => $row->idkelas,
				'namaruangan' => $row->namaruangan,
				'namakelas' => $row->namakelas,
				'namaperusahaan' => ($row->idkelompokpasien == 1 && $row->namarekanan ? $row->namarekanan : '-'),
				'namabed' => $row->namabed,
				'statuscheckout' => $row->statuscheckout,
				'statuskasir' => $row->statuskasir,
				'statusvalidasi' => $row->statusvalidasi,
				'statustransaksi' => $row->statustransaksi,
				'statuspembayaran' => $row->statuspembayaran,
				'catatan' => $row->catatan,
				'totaldeposit' => $totaldeposit,
				'page' => ($row->idtipe == 1 ? 'rawatinap' : 'ods'),
				'stedit' => $stedit,
			];

			$data['error'] = '';
			$data['title'] = 'Detail Transaksi';
			$data['content'] = 'Tverifikasi_transaksi/edit_transaksi_ranap';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['Verifikasi Transaksi', 'tverifikasi_transaksi/index'],
				['Detail Transaksi', '#']
			];

			$data['list_bank'] = $this->Trawatinap_tindakan_model->getBank();
			$data['list_kontraktor'] = $this->Trawatinap_tindakan_model->list_kontraktor($idpendaftaran);

			if ($pembayaran) {
				$idPembayaran = $pembayaran->id;

				$data['tanggal_pembayaran'] = DMYFormat($pembayaran->tanggal);
				$data['waktu_pembayaran'] = HISTimeFormat($pembayaran->tanggal);
				$data['total_pembayaran'] = $this->Trawatinap_tindakan_model->sum_pembayaran($pembayaran->idtindakan);
				$data['detail_pembayaran'] = $this->Trawatinap_tindakan_model->detail_pembayaran($pembayaran->id);
			} else {
				$idPembayaran = 0;

				$data['tanggal_pembayaran'] = date('Y-m-d');
				$data['waktu_pembayaran'] = date('H:i:s');
				$data['total_pembayaran'] = 0;
				$data['detail_pembayaran'] = [];
			}

			$data['pembayaran_tunai'] = $this->Trawatinap_tindakan_model->count_metode_pembayaran(1, $idPembayaran);
			$data['pembayaran_debit'] = $this->Trawatinap_tindakan_model->count_metode_pembayaran(2, $idPembayaran);
			$data['pembayaran_kredit'] = $this->Trawatinap_tindakan_model->count_metode_pembayaran(3, $idPembayaran);
			$data['pembayaran_transfer'] = $this->Trawatinap_tindakan_model->count_metode_pembayaran(4, $idPembayaran);
			$data['pembayaran_karyawan_pegawai'] = $this->Trawatinap_tindakan_model->count_metode_pembayaran(5, $idPembayaran);
			$data['pembayaran_karyawan_dokter'] = $this->Trawatinap_tindakan_model->count_metode_pembayaran(6, $idPembayaran);
			$data['pembayaran_tidak_tertagihkan'] = $this->Trawatinap_tindakan_model->count_metode_pembayaran(7, $idPembayaran);
			$data['pembayaran_kontraktor'] = $this->Trawatinap_tindakan_model->count_metode_pembayaran(8, $idPembayaran);

			$data['deposit_tunai'] = $this->Trawatinap_tindakan_model->count_metode_pembayaran_deposit(1, $idpendaftaran);
			$data['deposit_debit'] = $this->Trawatinap_tindakan_model->count_metode_pembayaran_deposit(2, $idpendaftaran);
			$data['deposit_kredit'] = $this->Trawatinap_tindakan_model->count_metode_pembayaran_deposit(3, $idpendaftaran);
			$data['deposit_transfer'] = $this->Trawatinap_tindakan_model->count_metode_pembayaran_deposit(4, $idpendaftaran);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}
	}

	public function detail_tindakan($table, $iddetail)
	{
		$tindakan = $this->model->getDetailTindakanRanap($table, $iddetail);
		if ($tindakan->id) {
			$itemTindakan = [
				[
					'nama' => 'Jasa Sarana',
					'tarif_master' => $tindakan->jasasarana_master,
					'tarif_transaksi' => $tindakan->jasasarana_tarif,
					'diskon_rupiah' => $tindakan->jasasarana_disc,
					'diskon_persen' => calcDiscountPercent($tindakan->jasasarana_disc, $tindakan->jasasarana_tarif),
					'total' => ($tindakan->jasasarana_tarif - $tindakan->jasasarana_disc),
					'status' => $tindakan->jasasarana_status,
					'group_pembayaran' => $tindakan->jasasarana_group,
					'keputusan' => $tindakan->jasasarana_keputusan,
				],
				[
					'nama' => 'Jasa Pelayanan',
					'tarif_master' => $tindakan->jasapelayanan_master,
					'tarif_transaksi' => $tindakan->jasapelayanan_tarif,
					'diskon_rupiah' => $tindakan->jasapelayanan_disc,
					'diskon_persen' => calcDiscountPercent($tindakan->jasapelayanan_disc, $tindakan->jasapelayanan_tarif),
					'total' => ($tindakan->jasapelayanan_tarif - $tindakan->jasapelayanan_disc),
					'status' => $tindakan->jasapelayanan_status,
					'group_pembayaran' => $tindakan->jasapelayanan_group,
					'keputusan' => $tindakan->jasapelayanan_keputusan,
				],
				[
					'nama' => 'BHP',
					'tarif_master' => $tindakan->bhp_master,
					'tarif_transaksi' => $tindakan->bhp_tarif,
					'diskon_rupiah' => $tindakan->bhp_disc,
					'diskon_persen' => calcDiscountPercent($tindakan->bhp_disc, $tindakan->bhp_tarif),
					'total' => ($tindakan->bhp_tarif - $tindakan->bhp_disc),
					'status' => $tindakan->bhp_status,
					'group_pembayaran' => $tindakan->bhp_group,
					'keputusan' => $tindakan->bhp_keputusan,
				],
				[
					'nama' => 'Biaya Perawatan',
					'tarif_master' => $tindakan->biayaperawatan_master,
					'tarif_transaksi' => $tindakan->biayaperawatan_tarif,
					'diskon_rupiah' => $tindakan->biayaperawatan_disc,
					'total' => ($tindakan->biayaperawatan_tarif - $tindakan->biayaperawatan_disc),
					'diskon_persen' => calcDiscountPercent($tindakan->biayaperawatan_disc, $tindakan->biayaperawatan_tarif),
					'status' => $tindakan->biayaperawatan_status,
					'group_pembayaran' => $tindakan->biayaperawatan_group,
					'keputusan' => $tindakan->biayaperawatan_keputusan,
				],
			];

			$data = [
				'table' => $table,
				'table_tarif' => $tindakan->table_tarif,
				'iddetail' => $iddetail,
				'idtarif' => $tindakan->idtarif,
				'namatarif' => $tindakan->namatarif,
				'jenistarif' => '-',
				'iddokter' => $tindakan->iddokter,
				'namadokter' => $tindakan->namadokter,
				'userinput' => $tindakan->namauserinput,
				'itemtindakan' => $itemTindakan
			];

			$data['error'] = '';
			$data['title'] = 'Detail Tindakan';
			$data['content'] = 'Tverifikasi_transaksi/detail_tindakan';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['Verifikasi Transaksi', 'tverifikasi_transaksi/index'],
				['Detail Tindakan', '#']
			];

			$data['list_group_pembayaran'] = $this->model->getGroupPembayaran();

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}
	}

	public function edit_jasadokter_rajal($idpendaftaran, $idkasir)
	{
		$data = [
			'id' => $idpendaftaran,
			'idkasir' => $idkasir,
			'status_verifikasi' => $this->model->getStatusVerifikasiKasir('poliklinik', $idkasir),
		];

		$data_header = $this->Tkasir_model->get_header_poli($idpendaftaran);
		if ($data_header) {
			$data['nopendaftaran'] = $data_header->nopendaftaran;
			$data['nomedrec'] = $data_header->no_medrec;
			$data['namapasien'] = $data_header->nama;
			$data['idtipepasien'] = $data_header->idtipepasien;
			$data['tipependaftaran'] = ($data_header->idtipe == '1' ? 'Poliklinik' : 'Instalasi Gawat Darurat');
			$data['poliklinik'] = $data_header->poli;
			$data['idkelompokpasien'] = $data_header->idkelompokpasien;
			$data['kelompokpasien'] = $data_header->kelompok;
			$data['bpjs_kesehatan'] = $data_header->tarif_bpjs_kes;
			$data['bpjs_ketenagakerjaan'] = $data_header->tarif_bpjs_ket;
			$data['namarekanan'] = $data_header->rekanan;
			$data['usertransaksi'] = $data_header->nama;
			$data['tanggaltransaksi'] = $data_header->tanggaldaftar;
			$data['namadokter'] = $data_header->dokter;
		}

		$data['error'] = '';
		$data['title'] = 'Detail Jasa Dokter';
		$data['content'] = 'Tverifikasi_transaksi/edit_jasadokter_rajal';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Verifikasi Transaksi', 'tverifikasi_transaksi/index'],
			['Detail Jasa Dokter', '#']
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function edit_jasadokter_ranap($idpendaftaran)
	{
		$row = $this->Trawatinap_tindakan_model->getDataRawatInap($idpendaftaran);
		if ($row->id) {
			$data = [
				'id' => $row->id,
				'idtipe' => $row->idtipe,
				'idtipepasien' => $row->idtipepasien,
				'idasalpasien' => $row->idasalpasien,
				'idpoliklinik' => $row->idpoliklinik,
				'nomedrec' => $row->nomedrec,
				'namapasien' => $row->namapasien,
				'alamatpasien' => $row->alamatpasien,
				'nohp' => ($row->hp ? $row->hp : $row->telepon),
				'namapenanggungjawab' => $row->nama_keluarga,
				'telppenanggungjawab' => $row->telepon_keluarga,
				'namakelompok' => $row->namakelompok,
				'namadokterpenanggungjawab' => $row->namadokterpenanggungjawab,
				'namaperusahaan' => ($row->idkelompokpasien == 1 && $row->namarekanan ? $row->namarekanan : '-'),
				'namaruangan' => $row->namaruangan,
				'namakelas' => $row->namakelas,
				'namabed' => $row->namabed,
				'iddokterperujuk' => $row->iddokterperujuk,
				'idkategoriperujuk' => $row->idkategoriperujuk,
				'namadokterperujuk' => $row->namadokterperujuk,
				'potonganrs_perujuk' => $row->potonganrs_perujuk,
				'pajak_perujuk' => $row->pajak_perujuk,
				'status_verifikasi' => $this->model->getStatusVerifikasiKasir('rawatinap', $idpendaftaran),
			];

			$data['error'] = '';
			$data['title'] = 'Detail Jasa Dokter';
			$data['content'] = 'Tverifikasi_transaksi/edit_jasadokter_ranap';
			$data['breadcrum'] = [
				['RSKB Halmahera', '#'],
				['Verifikasi Transaksi', 'tverifikasi_transaksi/index'],
				['Detail Jasa Dokter', '#']
			];

			$FeeJasaDokterVisite = [];
			$dataVisite = $this->Trawatinap_verifikasi_model->viewRincianRanapVisite($idpendaftaran);
			$dataLogicRange = $this->Trawatinap_verifikasi_model->viewFeeJasaDokterVisite($idpendaftaran);

			foreach ($dataVisite as $index => $row) {
				if (COUNT($dataLogicRange) > 0) {
					foreach ($dataLogicRange as $logicRange) {
						$list_dokter = explode(',', $logicRange->iddokter);
						$FeeJasaDokterVisite[$index]['iddetail'] = $row->iddetail;
						$FeeJasaDokterVisite[$index]['idtarif'] = $row->idtarif;
						$FeeJasaDokterVisite[$index]['namatarif'] = $row->namatarif;
						$FeeJasaDokterVisite[$index]['namaruangan'] = $row->namaruangan;
						$FeeJasaDokterVisite[$index]['tanggal'] = $row->tanggal;
						$FeeJasaDokterVisite[$index]['iddokter'] = $row->iddokter;
						$FeeJasaDokterVisite[$index]['idkategori'] = $row->idkategori;
						$FeeJasaDokterVisite[$index]['namakategori'] = $row->namakategori;
						$FeeJasaDokterVisite[$index]['namadokter'] = $row->namadokter;
						$FeeJasaDokterVisite[$index]['persentaseperujuk'] = $row->persentaseperujuk;
						$FeeJasaDokterVisite[$index]['jasamedis'] = $row->jasamedis;
						$FeeJasaDokterVisite[$index]['potongan_rs'] = $row->potongan_rs;
						$FeeJasaDokterVisite[$index]['potonganfeers'] = $row->potonganfeers;
						$FeeJasaDokterVisite[$index]['pajak_dokter'] = $row->pajak_dokter;
						$FeeJasaDokterVisite[$index]['periode_pembayaran'] = $row->periode_pembayaran;
						$FeeJasaDokterVisite[$index]['periode_jatuhtempo'] = $row->periode_jatuhtempo;
						$FeeJasaDokterVisite[$index]['status_jasamedis'] = $row->status_jasamedis;
						$FeeJasaDokterVisite[$index]['diskon'] = $row->diskon;

						if (in_array($row->iddokter, $list_dokter)) {
							if ($logicRange->urutan_operasi == 1) {
								if ($row->tanggal <= $logicRange->tanggal_next) {
									$FeeJasaDokterVisite[$index]['urutan_operasi'] = $logicRange->urutan_operasi;
									$FeeJasaDokterVisite[$index]['urutan_visite'] = $logicRange->urutan_operasi;
								}
							} else {
								if ($row->tanggal >= $logicRange->tanggal && $row->tanggal <= $logicRange->tanggal_next) {
									$FeeJasaDokterVisite[$index]['urutan_operasi'] = $logicRange->urutan_operasi;
									$FeeJasaDokterVisite[$index]['urutan_visite'] = $logicRange->urutan_operasi;
								}
							}
						} else {
							$FeeJasaDokterVisite[$index]['urutan_operasi'] = 0;
							$FeeJasaDokterVisite[$index]['urutan_visite'] = 0;
						}
					}
				} else {
					$FeeJasaDokterVisite[$index]['iddetail'] = $row->iddetail;
					$FeeJasaDokterVisite[$index]['idtarif'] = $row->idtarif;
					$FeeJasaDokterVisite[$index]['namatarif'] = $row->namatarif;
					$FeeJasaDokterVisite[$index]['namaruangan'] = $row->namaruangan;
					$FeeJasaDokterVisite[$index]['tanggal'] = $row->tanggal;
					$FeeJasaDokterVisite[$index]['iddokter'] = $row->iddokter;
					$FeeJasaDokterVisite[$index]['idkategori'] = $row->idkategori;
					$FeeJasaDokterVisite[$index]['namakategori'] = $row->namakategori;
					$FeeJasaDokterVisite[$index]['namadokter'] = $row->namadokter;
					$FeeJasaDokterVisite[$index]['persentaseperujuk'] = $row->persentaseperujuk;
					$FeeJasaDokterVisite[$index]['jasamedis'] = $row->jasamedis;
					$FeeJasaDokterVisite[$index]['potongan_rs'] = $row->potongan_rs;
					$FeeJasaDokterVisite[$index]['potonganfeers'] = $row->potonganfeers;
					$FeeJasaDokterVisite[$index]['pajak_dokter'] = $row->pajak_dokter;
					$FeeJasaDokterVisite[$index]['periode_pembayaran'] = $row->periode_pembayaran;
					$FeeJasaDokterVisite[$index]['periode_jatuhtempo'] = $row->periode_jatuhtempo;
					$FeeJasaDokterVisite[$index]['status_jasamedis'] = $row->status_jasamedis;
					$FeeJasaDokterVisite[$index]['diskon'] = $row->diskon;
					$FeeJasaDokterVisite[$index]['urutan_operasi'] = 0;
					$FeeJasaDokterVisite[$index]['urutan_visite'] = 0;
				}
			}

			$data['statusKasirRajal'] = $this->Trawatinap_tindakan_model->getDataPoliTindakan($idpendaftaran)->status_kasir;
			$data['FeeJasaDokterVisite'] = $FeeJasaDokterVisite;

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}
	}

	public function upload_document($reference, $idkasir)
	{
		if ($reference == 'tkasir') {
			$row = $this->model->getHeadDokumenRawatJalan($idkasir);
		} elseif ($reference == 'trawatinap_tindakan_pembayaran') {
			$row = $this->model->getHeadDokumenRawatInap($idkasir);
		}

		$data = [
			'reference' => $reference,
			'idkasir' => $row->idkasir,
			'nopendaftaran' => $row->nopendaftaran,
			'tipependaftaran' => $row->tipe,
			'nomedrec' => $row->nomedrec,
			'namapasien' => $row->namapasien,
		];

		$data['error'] = '';
		$data['title'] = 'Upload Dokumen';
		$data['content'] = 'Tverifikasi_transaksi/upload_dokumen';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Verifikasi Transaksi', 'tverifikasi_transaksi/index'],
			['Upload Dokumen', '#']
		];

		$data['listFiles'] = $this->model->getListUploadedDocument($reference, $idkasir);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function updateStatus($id, $tipe, $status)
	{
		$this->model->updateStatus($id, $tipe, $status);
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'Status Verifikasi berhasil dirubah.');
		if ($tipe == 'deposit') {
			redirect('tverifikasi_transaksi/index_deposit', 'location');
		} else {
			if ($tipe == 'poliklinik' && $status == '1') {
				$this->db->select("tpoliklinik_pendaftaran.id AS idpendaftaran,
					tpoliklinik_tindakan.id AS idtindakan, tkasir.id AS idkasir,
					IF(tpoliklinik_pendaftaran.idtipepasien = 1, 'Pasien RS', 'Pasien Pribadi') AS jenispasien,
					tpoliklinik_pendaftaran.idkelompokpasien AS idkelompok,
					mpasien_kelompok.nama AS namakelompok,
					tpoliklinik_pendaftaran.idrekanan,
					mrekanan.nama AS namarekanan,DATE(tkasir.tanggal) as tanggal_kasir,tkasir.idtipe,
					tpoliklinik_pendaftaran.tanggaldaftar AS tanggal_pemeriksaan,
					tpoliklinik_pendaftaran.nopendaftaran,
					tpoliklinik_pendaftaran.idpasien,
					tpoliklinik_pendaftaran.title,
					tpoliklinik_pendaftaran.namapasien,
					tpoliklinik_pendaftaran.no_medrec,
					tpoliklinik_pendaftaran.idpoliklinik,
					tpoliklinik_pendaftaran.created_date,
					tkasir.nokasir,
					tkasir.created_by
				");

				$this->db->join('tpoliklinik_tindakan', 'tpoliklinik_tindakan.id = tkasir.idtindakan AND idtipe IN (1,2)');
				$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran');
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = tpoliklinik_pendaftaran.idkelompokpasien', 'LEFT');
				$this->db->join('mrekanan', 'mrekanan.id = tpoliklinik_pendaftaran.idrekanan', 'LEFT');
				$this->db->where('tkasir.id', $id);
				$this->db->where('tkasir.status', 2);
				$this->db->limit(1);
				$query = $this->db->get('tkasir');
				$row = $query->row();

				$dataPendaftaran = [
					'tanggal_kasir' => $row->tanggal_kasir,
					'tanggal_pemeriksaan' => $row->tanggal_pemeriksaan,
					'idpendaftaran' => $row->idpendaftaran,
					'jenis_pasien' => $row->jenispasien,
					'idkelompok' => $row->idkelompok,
					'namakelompok' => $row->namakelompok,
					'idrekanan' => $row->idrekanan,
					'namarekanan' => $row->namarekanan,
					'kasir_id' => $row->idkasir,
					'nopendaftaran' => $row->nopendaftaran,
					'idpasien' => $row->idpasien,
					'title' => $row->title,
					'namapasien' => $row->namapasien,
					'no_medrec' => $row->no_medrec,
					'idtipe' => $row->idtipe,
					'idpoliklinik' => $row->idpoliklinik,
					'created_by' => $row->created_by,
					'nokasir' => $row->nokasir,
					'created_date' => $row->created_date,
					'status_asuransi' => ($row->idkelompok != 5 ? 1 : 0),
				];
				// print_r($dataPendaftaran);exit;
				if ($row->idpendaftaran) {
					$this->Thonor_dokter_model->generateTransaksiHonorDokterRawatJalan($dataPendaftaran);
				}
			} elseif ($tipe == 'rawatinap' && $status == '1') {
				$this->db->select("trawatinap_pendaftaran.id AS idpendaftaran,
					trawatinap_pendaftaran.nopendaftaran,
					trawatinap_pendaftaran.idpasien,
					trawatinap_pendaftaran.no_medrec,
					trawatinap_pendaftaran.title,
					trawatinap_pendaftaran.namapasien,
					trawatinap_pendaftaran.idtipe,
					trawatinap_pendaftaran.idtipepasien,
					trawatinap_pendaftaran.idkelas,
					trawatinap_pendaftaran.idruangan,
					trawatinap_pendaftaran.idbed,
					IF(trawatinap_pendaftaran.idtipepasien = 1, 'Pasien RS', 'Pasien Pribadi') AS jenispasien,
					trawatinap_pendaftaran.idkelompokpasien AS idkelompok,
					mpasien_kelompok.nama AS namakelompok,
					trawatinap_pendaftaran.idrekanan,
					mrekanan.nama AS namarekanan,
					trawatinap_pendaftaran.tanggaldaftar AS tanggal_pemeriksaan,
					mdokterperujuk.idkategori AS idkategori_perujuk,
					mdokterperujuk_kategori.nama AS namakategori_perujuk,
					mdokterperujuk.id AS iddokter_perujuk,
					mdokterperujuk.nama AS namadokter_perujuk,
					mdokterperujuk.pajakranap AS pajak_perujuk,
					mdokterperujuk.potonganrsranap AS potonganrs_perujuk,
					tpoliklinik_pendaftaran.idtipe AS idasalpasien,
					tpoliklinik_pendaftaran.idpoliklinik AS idpoliklinik");
				$this->db->join('trawatinap_pendaftaran', 'trawatinap_pendaftaran.id = trawatinap_tindakan_pembayaran.idtindakan');
				$this->db->join('tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik');
				$this->db->join('mpasien_kelompok', 'mpasien_kelompok.id = trawatinap_pendaftaran.idkelompokpasien', 'LEFT');
				$this->db->join('mrekanan', 'mrekanan.id = trawatinap_pendaftaran.idrekanan', 'LEFT');
				$this->db->join('mdokter mdokterperujuk', 'mdokterperujuk.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
				$this->db->join('mdokter_kategori mdokterperujuk_kategori', 'mdokterperujuk_kategori.id = mdokterperujuk.idkategori', 'LEFT');
				$this->db->where('trawatinap_tindakan_pembayaran.id', $id);
				$this->db->where('trawatinap_tindakan_pembayaran.statusbatal', 0);
				$this->db->limit(1);
				$query = $this->db->get('trawatinap_tindakan_pembayaran');
				$row = $query->row();

				// Get Status Pembayaran Kasir Rawat Jalan
				$statusKasirRajal = $this->Trawatinap_tindakan_model->getDataPoliTindakan($row->idpendaftaran)->status_kasir;
				$dataPendaftaran = [
					'tanggal_pemeriksaan' => $row->tanggal_pemeriksaan,
					'idpasien' => $row->idpasien,
					'no_medrec' => $row->no_medrec,
					'title' => $row->title,
					'namapasien' => $row->namapasien,
					'kasir_id' => $id,
					'idkelas' => $row->idkelas,
					'idruangan' => $row->idruangan,
					'idbed' => $row->idbed,
					'idtipe' => $row->idtipe,
					'nopendaftaran' => $row->nopendaftaran,
					'idpendaftaran' => $row->idpendaftaran,
					'idtipepasien' => $row->idtipepasien,
					'idasalpasien' => $row->idasalpasien,
					'idpoliklinik' => $row->idpoliklinik,
					'jenis_pasien' => $row->jenispasien,
					'idkelompok' => $row->idkelompok,
					'namakelompok' => $row->namakelompok,
					'idrekanan' => $row->idrekanan,
					'namarekanan' => $row->namarekanan,
					'idkategori_perujuk' => $row->idkategori_perujuk,
					'namakategori_perujuk' => $row->namakategori_perujuk,
					'iddokter_perujuk' => $row->iddokter_perujuk,
					'namadokter_perujuk' => $row->namadokter_perujuk,
					'potonganrs_perujuk' => $row->potonganrs_perujuk,
					'pajak_perujuk' => $row->pajak_perujuk,
					'status_asuransi' => ($row->idkelompok != 5 ? 1 : 0),
					'status_kasir_rajal' => $statusKasirRajal,
				];

				if ($row->idpendaftaran) {
					$this->Thonor_dokter_model->generateTransaksiHonorDokterRawatInap($dataPendaftaran);
				}
			}

			$tipe = ($tipe == 'obatbebas' ? 'poliklinik' : $tipe);
			redirect('tverifikasi_transaksi/index/' . $tipe, 'location');
		}
	}

	public function verif_refund($id, $tipe, $status)
	{
		if ($status == '1') {
			if ($tipe != '0') {
				$this->Tvalidasi_model->GenerateValidasiPendapatanRajalRefund($id, $tipe);
			} else {
				$this->Tvalidasi_model->GenerateValidasiPendapatanRanapRefund($id, $tipe);
			}
		} else {
			if ($tipe != '0') {
				$this->model->remove_jurnal_pendapatan($id, 4);
			}
		}
		$this->model->verif_refund($id, $tipe, $status);
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'Status Verifikasi berhasil dirubah.');

		// $tipe = ($tipe == 'obatbebas' ? 'poliklinik' : $tipe);
		redirect('tverifikasi_transaksi/index_refund/' . $tipe, 'location');
	}

	public function updateStatusAll()
	{
		$this->model->updateStatusAll();
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'Status Verifikasi berhasil dirubah.');
		redirect('tverifikasi_transaksi/index/' . $this->input->post('tipetransaksi'), 'location');
	}

	public function updateTipePasien()
	{
		if ($this->model->updateTipePasien()) {
			$output = [
				'status' => true
			];
		} else {
			$output = [
				'status' => false
			];
		}

		echo json_encode($output);
	}

	public function noticeTindakan()
	{
		$table = $this->input->post('table');
		$iddetail = $this->input->post('iddetail');
		$idtarif = $this->input->post('idtarif');
		$iddokter = $this->input->post('iddokter');
		$data = json_decode($this->input->post('data'));

		if ($this->model->noticeTindakan($table, $iddetail, $idtarif, $iddokter, $data)) {
			$output = ['status' => true];
		} else {
			$output = ['status' => false];
		}

		echo json_encode($output);
	}

	public function noticeAllTindakan()
	{
		$table = $this->input->post('table');
		$iddetail = $this->input->post('iddetail');
		$idtarif = $this->input->post('idtarif');
		$idkeputusan = $this->input->post('idkeputusan');
		$data = json_decode($this->input->post('data'));

		if ($this->model->noticeAllTindakan($table, $iddetail, $idtarif, $idkeputusan, $data)) {
			$output = ['status' => true];
		} else {
			$output = ['status' => false];
		}

		echo json_encode($output);
	}

	public function upload_files()
	{
		if (!file_exists('assets/upload/dokumen_verifikasi')) {
			mkdir('assets/upload/dokumen_verifikasi', 0755, true);
		}

		$files = [];
		foreach ($_FILES as $file) {
			if ($file['name'] != '') {
				$config['upload_path'] = './assets/upload/dokumen_verifikasi/';
				$config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';

				$files[] = $file['name'];

				$this->upload->initialize($config);

				if ($this->upload->do_upload('file')) {
					$image_upload = $this->upload->data();
					$table = $this->input->post('reference') . '_dokumen';

					$data = [];
					$data['idkasir'] = $this->input->post('idkasir');
					$data['filename'] = $image_upload['file_name'];
					$data['size'] = formatSizeUnits($file['size']);

					$this->db->insert($table, $data);

					return true;
				} else {
					print_r($this->upload->display_errors());
					exit();

					return false;
				}
			} else {
				return true;
			}
		}
	}

	public function delete_file($reference, $idfile)
	{
		$this->db->where('id', $idfile);
		$query = $this->db->get($reference . '_dokumen');
		$row = $query->row();
		if ($query->num_rows() > 0) {
			$this->db->where('id', $idfile);
			if ($this->db->delete($reference . '_dokumen')) {
				if (file_exists('./assets/upload/dokumen_verifikasi/' . $row->filename) && $row->filename != '') {
					unlink('./assets/upload/dokumen_verifikasi/' . $row->filename);
				}
			}
		}

		return true;
	}

	public function simpan_pembayaran_poliklinik()
	{
		return $this->Trawatinap_tindakan_model->simpan_transaksi();
	}

	public function getIndex($uri, $tipe)
	{
		switch ($tipe) {
			case 'poliklinik':
				$this->select = ['*'];
				$this->from = 'view_index_verifikasi_transaksi_rajal';
				$this->join = [];

				$this->order = [
				'tanggal' => 'DESC'
				];
				$this->group = [];

				break;

			case 'rawatinap':
				$this->select = ['*'];
				$this->from = 'view_index_verifikasi_transaksi_ranap';
				$this->join = [];

				$this->order = [
				'tanggal' => 'DESC'
				];
				$this->group = [];

				break;

			case 'deposit':
				$this->select = ['*'];
				$this->from = 'view_index_verifikasi_transaksi_deposit';
				$this->join = [];

				$this->order = [
					'tanggal' => 'DESC'
				];
				$this->group = [];

				break;
		}

		// FILTER
		$this->where = [];

		if (($this->session->userdata('tanggal_pendaftaran_awal') != null || $this->session->userdata('tanggal_pendaftaran_akhir') != null)) {
			if ($this->session->userdata('tanggal_pendaftaran_awal') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggalpendaftaran) >=' => YMDFormat($this->session->userdata('tanggal_pendaftaran_awal'))]);
			}
			if ($this->session->userdata('tanggal_pendaftaran_akhir') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggalpendaftaran) <=' => YMDFormat($this->session->userdata('tanggal_pendaftaran_akhir'))]);
			}
		}

		if (($this->session->userdata('tanggal_pembayaran_awal') != null || $this->session->userdata('tanggal_pembayaran_akhir') != null)) {
			if ($this->session->userdata('tanggal_pembayaran_awal') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal) >=' => YMDFormat($this->session->userdata('tanggal_pembayaran_awal'))]);
			}
			if ($this->session->userdata('tanggal_pembayaran_akhir') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal) <=' => YMDFormat($this->session->userdata('tanggal_pembayaran_akhir'))]);
			}
		}

		if (($this->session->userdata('tanggal_pendaftaran_awal') == null || $this->session->userdata('tanggal_pendaftaran_akhir') == null) && ($this->session->userdata('tanggal_pembayaran_awal') == null || $this->session->userdata('tanggal_pembayaran_akhir') == null)) {
			$this->where = array_merge($this->where, ['DATE(tanggalpendaftaran)' => date('Y-m-d')]);
		}

		if ($this->session->userdata('idtipe') != 0) {
			$this->where = array_merge($this->where, ['idtipe' => $this->session->userdata('idtipe')]);
		}
		if ($this->session->userdata('idpoliklinik') != 0) {
			$this->where = array_merge($this->where, ['idpoliklinik' => $this->session->userdata('idpoliklinik')]);
		}
		if ($this->session->userdata('nomedrec') != '') {
			$this->where = array_merge($this->where, ['nomedrec' => $this->session->userdata('nomedrec')]);
		}
		if ($this->session->userdata('namapasien') != '') {
			$this->where = array_merge($this->where, ['namapasien LIKE' => '%' . $this->session->userdata('namapasien') . '%']);
		}
		if ($this->session->userdata('idkelompokpasien') != 0) {
			$this->where = array_merge($this->where, ['idkelompokpasien' => $this->session->userdata('idkelompokpasien')]);
		}
		if ($this->session->userdata('idrekanan') != 0) {
			$this->where = array_merge($this->where, ['idrekanan' => $this->session->userdata('idrekanan')]);
		}
		if ($this->session->userdata('iduser_input') != 0) {
			$this->where = array_merge($this->where, ['iduser_input' => $this->session->userdata('iduser_input')]);
		}
		if ($this->session->userdata('iddokter') != 0) {
			$this->where = array_merge($this->where, ['iddokter' => $this->session->userdata('iddokter')]);
		}
		if ($this->session->userdata('nopendaftaran') != '') {
			$this->where = array_merge($this->where, ['nopendaftaran LIKE' => '%' . $this->session->userdata('nopendaftaran') . '%']);
		}
		if ($this->session->userdata('nokasir') != '') {
			$this->where = array_merge($this->where, ['nokasir LIKE' => '%' . $this->session->userdata('nokasir') . '%']);
		}

		$this->column_search = ['nopendaftaran', 'tipe', 'nomedrec', 'namapasien'];

		$this->column_order = ['nopendaftaran', 'tipe', 'nomedrec', 'namapasien'];

		$list = $this->datatable->get_datatables();

		$data = [];
		foreach ($list as $row) {
			$posting = 0;
			if ($row->status_verifikasi) {
				$actionVerifikasi = '<a href="' . site_url() . 'tverifikasi_transaksi/updateStatus/' . $row->id . '/' . $tipe . '/0" title="Batalkan Verifikasi" class="btn btn-danger btn-sm loading"><i class="fa fa-ban"></i></a>';
			} else {
				$actionVerifikasi = '<a href="' . site_url() . 'tverifikasi_transaksi/updateStatus/' . $row->id . '/' . $tipe . '/1" title="Verifikasi Transaksi" class="btn btn-primary btn-sm loading"><i class="fa fa-check"></i></a>';
			}

			if ($tipe == 'poliklinik') {
				$posting = $this->model->get_status_posting_rajal($row->id, $row->idtipe);
				if ($row->idtipe == 3) {
					if ($row->status_verifikasi) {
						$actionVerifikasi = '<a href="' . site_url() . 'tverifikasi_transaksi/updateStatus/' . $row->id . '/obatbebas/0" title="Batalkan Verifikasi" class="btn btn-danger btn-sm loading"><i class="fa fa-ban"></i></a>';
					} else {
						$actionVerifikasi = '<a href="' . site_url() . 'tverifikasi_transaksi/updateStatus/' . $row->id . '/obatbebas/1" title="Verifikasi Transaksi" class="btn btn-primary btn-sm loading"><i class="fa fa-check"></i></a>';
					}
					$actionEdit = '<a href="' . site_url() . 'tverifikasi_transaksi/edit_transaksi_obatbebas/' . $row->idpendaftaran . '/' . $row->id . '" title="Edit Transaksi" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>';
					$actionDetail = '<a href="' . site_url() . 'tverifikasi_transaksi/edit_transaksi_obatbebas/' . $row->idpendaftaran . '/' . $row->id . '/0" title="Detail Transaksi" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
					$actionJasaDokter = '';
				} else {
					$actionEdit = '<a href="' . site_url() . 'tverifikasi_transaksi/edit_transaksi_rajal/' . $row->idpendaftaran . '/' . $row->id . '" title="Edit Transaksi" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>';
					$actionDetail = '<a href="' . site_url() . 'tverifikasi_transaksi/edit_transaksi_rajal/' . $row->idpendaftaran . '/' . $row->id . '/0" title="Detail Transaksi" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
					$actionJasaDokter = '<a href="' . site_url() . 'tverifikasi_transaksi/edit_jasadokter_rajal/' . $row->idpendaftaran . '/' . $row->id . '" title="Jasa Dokter" class="btn btn-success btn-sm"><i class="fa fa-file"></i></a>';
				}
				$actionUploadDokumen = '<a href="' . site_url() . 'tverifikasi_transaksi/upload_document/tkasir/' . $row->id . '" title="Upload Dokumen" class="btn btn-danger btn-sm"><i class="fa fa-upload"></i></a>';
			} elseif ($tipe == 'rawatinap') {
				$posting = $this->model->get_status_posting_ranap($row->id);
				$actionEdit = '<a href="' . site_url() . 'tverifikasi_transaksi/edit_transaksi_ranap/' . $row->idrawatinap . '" title="Edit Transaksi" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>';
				$actionDetail = '<a href="' . site_url() . 'tverifikasi_transaksi/edit_transaksi_ranap/' . $row->idrawatinap . '/0" title="Detail Transaksi" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
				$actionJasaDokter = '<a href="' . site_url() . 'tverifikasi_transaksi/edit_jasadokter_ranap/' . $row->idrawatinap . '" title="Jasa Dokter" class="btn btn-success btn-sm"><i class="fa fa-file"></i></a>';
				$actionUploadDokumen = '<a href="' . site_url() . 'tverifikasi_transaksi/upload_document/trawatinap_tindakan_pembayaran/' . $row->id . '" title="Upload Dokumen" class="btn btn-danger btn-sm"><i class="fa fa-upload"></i></a>';
			} elseif ($tipe == 'deposit') {
				$actionEdit = '<a href="' . site_url() . 'tverifikasi_transaksi/edit_transaksi_deposit/' . $row->idrawatinap . '" title="Edit Transaksi" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>';
				$actionDetail = '<a href="' . site_url() . 'tverifikasi_transaksi/edit_transaksi_deposit/' . $row->idrawatinap . '/0" title="Detail Transaksi" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
				$actionJasaDokter = '';
				$actionUploadDokumen = '<a href="' . site_url() . 'tverifikasi_transaksi/upload_document/trawatinap_deposit/' . $row->id . '" title="Upload Dokumen" class="btn btn-danger btn-sm"><i class="fa fa-upload"></i></a>';
			}

			$action = '<div class="btn-group">';
			if ($posting == '0') {
				$action .= $actionVerifikasi;
			}
			if ($row->status_verifikasi == '0') {
				$action .= $actionEdit;
			}
			$action .= $actionDetail;
			$action .= $actionJasaDokter;
			$action .= $actionUploadDokumen;
			$action .= '<a href="' . site_url() . 'tverifikasi_transaksi/print/' . $row->id . '" title="Print" class="btn btn-default btn-sm"><i class="fa fa-print"></i></a>';
			$action .= '</div>';

			$rows = [];
			$rows[] = '<label class="css-input css-checkbox css-checkbox-success"><input type="checkbox" class="checkboxTrx" name="check_status[]" value="' . $row->id . '"><span></span></label>';
			$rows[] = $row->tanggal;
			$rows[] = $row->nopendaftaran;
			$rows[] = TipeTransaksiVerifikasi($row->idtipe, $row->tipe);
			$rows[] = ($row->nomedrec ? $row->nomedrec : '-');
			$rows[] = $row->namapasien;
			$rows[] = number_format($row->jumlah);
			$rows[] = StatusEditVerifikasi($row->status_edit);
			$rows[] = StatusKasirVerifikasi($row->status_verifikasi);
			$rows[] = $action;

			$data[] = $rows;
		}

		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data,
		];

		echo json_encode($output);
	}

	public function getIndexDeposit($uri)
	{
		$this->select = ['*'];
		$this->from = 'view_index_verifikasi_transaksi_deposit';
		$this->join = [];

		$this->order = [
			'tanggal' => 'DESC'
		];
		$this->group = [];

		// FILTER
		$this->where = [];
		if ($uri == 'filter') {
			if ($this->session->userdata('tanggal_awal') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal) >=' => YMDFormat($this->session->userdata('tanggal_awal'))]);
			}
			if ($this->session->userdata('tanggal_akhir') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal) <=' => YMDFormat($this->session->userdata('tanggal_akhir'))]);
			}
			if ($this->session->userdata('nomedrec') != '') {
				$this->where = array_merge($this->where, ['nomedrec' => $this->session->userdata('nomedrec')]);
			}
			if ($this->session->userdata('namapasien') != '') {
				$this->where = array_merge($this->where, ['namapasien LIKE' => '%' . $this->session->userdata('namapasien') . '%']);
			}
			if ($this->session->userdata('iduserinput') != 0) {
				$this->where = array_merge($this->where, ['iduserinput' => $this->session->userdata('iduserinput')]);
			}
			if ($this->session->userdata('idmetode') != 0) {
				$this->where = array_merge($this->where, ['idmetode' => $this->session->userdata('idmetode')]);
			}
			if ($this->session->userdata('idbank') != 0) {
				$this->where = array_merge($this->where, ['idbank' => $this->session->userdata('idbank')]);
			}
			if ($this->session->userdata('idtipe') != 0) {
				$this->where = array_merge($this->where, ['idtipe' => $this->session->userdata('idtipe')]);
			}
		} else {
			$this->where = array_merge($this->where, ['DATE(tanggal)' => date('Y-m-d')]);
		}

		$this->column_search = ['nomedrec', 'namapasien'];

		$this->column_order = ['nomedrec', 'namapasien'];

		$list = $this->datatable->get_datatables();
		$data = [];
		foreach ($list as $row) {
			$action = '<div class="btn-group">';
			if ($row->statusverifikasi) {
				if ($row->st_posting == '0') {
					$action .= '<a href="' . site_url() . 'tverifikasi_transaksi/updateStatus/' . $row->id . '/deposit/0" title="Batalkan Verifikasi" class="btn btn-danger btn-sm"><i class="fa fa-ban"></i></a>';
				} else {
					$action .= '<button type="button" disabled title="Batalkan Verifikasi" class="btn btn-danger btn-sm"><i class="fa fa-ban"></i> Posted</button>';
				}
			} else {
				$action .= '<a href="' . site_url() . 'tverifikasi_transaksi/updateStatus/' . $row->id . '/deposit/1" title="Verifikasi Transaksi" class="btn btn-primary btn-sm"><i class="fa fa-check"></i></a>';
			}
			$action .= '<a href="' . site_url() . 'tverifikasi_transaksi/print/' . $row->id . '" title="Print" class="btn btn-default btn-sm"><i class="fa fa-print"></i></a>';
			
			$action .= '<button onclick="show_modal_deposit_header_add('.$row->idrawatinap.','.$row->head_id.')"  type="button" data-toggle="tooltip" title="TRANSAKSI DEPOSIT"  class="btn btn-warning btn-sm"><i class="fa fa-list-ul"></i> </button>';
			$action .= '</div>';

			$rows = [];
			$rows[] = '<label class="css-input css-checkbox css-checkbox-success"><input type="checkbox" class="checkboxTrx" name="check_status[]" value="' . $row->id . '"><span></span></label>';
			$rows[] = $row->tanggal;
			$rows[] = $row->nomedrec;
			$rows[] = $row->namapasien;
			$rows[] = $row->tipe;
			$rows[] = metodePembayaran($row->idmetode);
			$rows[] = $row->bank;
			$rows[] = number_format($row->nominal);
			$rows[] = $row->userinput;
			$rows[] = StatusKasirVerifikasi($row->statusverifikasi);
			$rows[] = $action;

			$data[] = $rows;
		}

		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data,
		];

		echo json_encode($output);
	}

	public function getIndexRefund($uri)
	{
		// $this->select = array('*');

		$this->join = [];

		$this->order = [
			'tanggal' => 'DESC'
		];
		$this->group = [];

		// FILTER
		$this->where = [];
		if ($uri == 'filter') {
			if ($this->session->userdata('tanggal_awal') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal) >=' => YMDFormat($this->session->userdata('tanggal_awal'))]);
			}
			if ($this->session->userdata('tanggal_akhir') != null) {
				$this->where = array_merge($this->where, ['DATE(tanggal) <=' => YMDFormat($this->session->userdata('tanggal_akhir'))]);
			}
			if ($this->session->userdata('nomedrec') != '') {
				$this->where = array_merge($this->where, ['nomedrec' => $this->session->userdata('nomedrec')]);
			}
			if ($this->session->userdata('namapasien') != '') {
				$this->where = array_merge($this->where, ['namapasien LIKE' => '%' . $this->session->userdata('namapasien') . '%']);
			}
			if ($this->session->userdata('iduserinput') != 0) {
				$this->where = array_merge($this->where, ['created_by' => $this->session->userdata('iduserinput')]);
			}
		} else {
			$this->where = array_merge($this->where, ['DATE(tanggal)' => date('Y-m-d')]);
		}

		$this->column_search = ['nomedrec', 'namapasien'];
		if ($this->session->userdata('tipe') != '#') {
			// if ($this->session->userdata('tipe') == '2') {
			// $where =" AND H.tipe=".$this->session->userdata('tipe')." AND H.tipetransaksi='rawatjalan'";
			// }else{
			// }
			$where = ' AND H.tipe=' . $this->session->userdata('tipe');
		} else {
			// $where =" AND (H.tipe='1' OR (H.tipe='2' AND H.tipetransaksi='rawatjalan'))";
			$where = ' AND H.tipe IN (0,1,2)';
		}
		$this->column_order = ['nomedrec', 'namapasien'];
		$from = "
			(
				SELECT H.id,H.tanggal,H.norefund,H.idtransaksi,H.notransaksi,H.tipe,MP.no_medrec as nomedrec,MP.nama as namapasien,H.totalrefund,H.st_verifikasi as statusverifikasi
				,H.created_by,H.tipetransaksi 
				FROM trefund H
				LEFT JOIN mfpasien MP ON MP.id=H.idpasien
				WHERE H.metode='1' AND H.status='1' " . $where . '
			) as tbl
		';
		
		$this->from = $from;
		$this->select = [];
		$list = $this->datatable->get_datatables();
		$data = [];
		$no = 0;
		foreach ($list as $row) {
			$posting = 0;
			if ($row->tipe == '0') {
				$posting = $this->model->get_status_posting_ranap($row->idtransaksi, '0');
			} else {
				$posting = $this->model->get_status_posting_rajal($row->id, 4);
			}
			$no++;
			$aksi = '<div class="btn-group">';
			if ($posting == '0') {
				if ($row->statusverifikasi) {
					$aksi = '<a href="' . site_url() . 'tverifikasi_transaksi/verif_refund/' . $row->id . '/' . $row->tipe . '/0" title="Batalkan Verifikasi" class="btn btn-danger btn-sm loading"><i class="fa fa-ban"></i></a>';
				} else {
					$aksi = '<a href="' . site_url() . 'tverifikasi_transaksi/verif_refund/' . $row->id . '/' . $row->tipe . '/1" title="Verifikasi Transaksi" class="btn btn-primary btn-sm loading"><i class="fa fa-check"></i></a>';
				}
			}
			$aksi .= '<a href="' . site_url() . 'trefund_kas/pembayaran/' . $row->id . '/disabled" title="Detail Transaksi" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>';

			switch ($row->tipe) {
				case 0:
					$tipe = '<label class="label label-primary text-uppercase">Deposit</label>';
					// if (UserAccesForm($user_acces_form, array('1098'))) {
						$aksi .= '<a href="' . base_url() . 'Trefund/fakturRefundDeposit/' . $row->id . '" class="btn btn-sm btn-primary print-refund" target="_blank"><i class="fa fa-print"></i></a>';
					// }
					break;
				case 1:
					$tipe = '<label class="label label-success text-uppercase">Retur Obat</label>';
					// if (UserAccesForm($user_acces_form, array('1098'))) {
						$aksi .= '<a href="' . base_url() . 'Trefund/fakturRefundObat/' . $row->id . '" class="btn btn-sm btn-primary print-refund" target="_blank"><i class="fa fa-print"></i></a>';
					// }
					break;
				case 2:
					$tipe = '<label class="label label-primary text-uppercase">Transaksi</label>';
					// if (UserAccesForm($user_acces_form, array('1098'))) {
						$aksi .= '<a href="' . base_url() . 'Trefund/fakturRefundTransaksi/' . $row->id . '" class="btn btn-sm btn-primary print-refund" target="_blank"><i class="fa fa-print"></i></a>';
					// }
				break;
			}
			$aksi .= '</div>';
			$rows = [];
			$rows[] = $no;
			$rows[] = $row->tanggal;
			$rows[] = $row->norefund;
			$rows[] = $row->notransaksi . ' ' . $row->tipetransaksi;
			$rows[] = $tipe;
			$rows[] = $row->nomedrec;
			$rows[] = $row->namapasien;
			$rows[] = number_format($row->totalrefund);
			$rows[] = $aksi;

			$data[] = $rows;
		}

		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data,
		];

		echo json_encode($output);
	}

	public function GetPeriodePembayaranHonorDokter($iddokter, $tanggal = '', $bulan = '', $tahun = '')
	{
		if ($tanggal && $bulan && $tahun) {
			$tanggalPembayaranSelected = $tanggal . '/' . $bulan . '/' . $tahun;
			$query = $this->db->query("SELECT tanggal_jatuhtempo FROM thonor_dokter WHERE iddokter = $iddokter AND tanggal_pembayaran = $tanggalPembayaranSelected");
			if ($query->num_rows() > 0) {
				$tanggalJatuhTempoSelected = $row->tanggal_jatuhtempo;	
			} else {
				$tanggalJatuhTempoSelected = '';
			}
		} else {
			$tanggalPembayaranSelected = '';
			$tanggalJatuhTempoSelected = '';
		}

		$query = $this->db->query("SELECT DATE_FORMAT(result.xtanggal_next_pembayaran,'%d/%m/%Y') AS tanggal_pembayaran,DATE_FORMAT(result.xtanggal_next_jatuhtempo,'%d/%m/%Y') AS tanggal_jatuhtempo FROM (
        SELECT mdokter_setting.*,thonor_dokter.tanggal_pembayaran,thonor_dokter.tanggal_jatuhtempo FROM (
        SELECT CONCAT(YEAR (CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE (),'%m'),'-',LPAD(mdokter_pembayaran.tanggal,2,'0')) AS xtanggal_pembayaran,(CASE WHEN (
        SELECT xtanggal_pembayaran)< CURRENT_DATE THEN DATE_ADD((
        SELECT xtanggal_pembayaran),INTERVAL 1 MONTH) ELSE (
        SELECT xtanggal_pembayaran) END) AS xtanggal_next_pembayaran,CONCAT(YEAR (CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE (),'%m'),'-',LPAD(mdokter_pembayaran.jatuh_tempo,2,'0')) AS xtanggal_jatuh_tempo,(CASE WHEN (
        SELECT xtanggal_jatuh_tempo)< CURRENT_DATE THEN DATE_ADD((
        SELECT xtanggal_jatuh_tempo),INTERVAL 1 MONTH) ELSE (
        SELECT xtanggal_jatuh_tempo) END) AS xtanggal_next_jatuhtempo FROM mdokter_pembayaran WHERE mdokter_pembayaran.iddokter=$iddokter AND mdokter_pembayaran.STATUS='1') AS mdokter_setting LEFT JOIN thonor_dokter ON mdokter_setting.xtanggal_next_pembayaran=thonor_dokter.tanggal_pembayaran AND thonor_dokter.iddokter=$iddokter AND thonor_dokter.status_stop = 1 ORDER BY mdokter_setting.xtanggal_next_pembayaran ASC) result WHERE result.tanggal_pembayaran IS NULL");

		$result = $query->result();

		$exist = false;
		if (in_array($tanggalPembayaranSelected, array_column($result, 'tanggal_pembayaran'))) {
			$exist = true;
		}

		$option = '';
		foreach ($result as $row) {
			if ($exist == false) {
				$option .= '<option value="' . $tanggalPembayaranSelected . '" data-jatuhtempo="' . $tanggalJatuhTempoSelected . '" selected>' . $tanggalPembayaranSelected . '</option>';	
			}
			$option .= '<option value="' . $row->tanggal_pembayaran . '" data-jatuhtempo="' . $row->tanggal_jatuhtempo . '" ' . ($row->tanggal_pembayaran == $tanggalPembayaranSelected ? 'selected' : '') . '>' . $row->tanggal_pembayaran . '</option>';
		}

		$arr['list_option'] = $option;
		$this->output->set_output(json_encode($arr));
	}

	public function GetPotonganDokter($iddokter)
	{
		$query = $this->db->query("SELECT potonganrspagi AS potongan_rs, pajak AS pajak_dokter, potonganfeers AS potonganfee_rs FROM mdokter WHERE id = $iddokter");
		$dokter = $query->row();

		$arr['potongan_rs'] = $dokter->potongan_rs;
		$arr['potonganfee_rs'] = $dokter->potonganfee_rs;
		$arr['pajak_dokter'] = $dokter->pajak_dokter;
		$this->output->set_output(json_encode($arr));
	}
	
	public function getPotonganDokterRanap($idrawatinap, $iddokter)
	{
		$dokter = GetPotonganDokterRanap($idrawatinap, $iddokter);

		$arr['potongan_rs'] = $dokter->potongan_rs;
		$arr['potonganfee_rs'] = $dokter->potonganfee_rs;
		$arr['pajak_dokter'] = $dokter->pajak_dokter;
		$this->output->set_output(json_encode($arr));
	}

	// ACTION MODAL JASA DOKTER
	public function saveDataJasaDokter()
	{
		$dataTindakan = [
			'table' => $this->input->post('table'),
			'idrawatinap' => $this->input->post('idrawatinap'),
			'iddetail' => $this->input->post('iddetail'),
			'idkategori' => $this->input->post('idkategori'),
			'namakategori' => $this->input->post('namakategori'),
			'iddokter' => $this->input->post('iddokter'),
			'namadokter' => $this->input->post('namadokter'),
			'jasamedis' => $this->input->post('jasamedis'),
			'potongan_rs' => $this->input->post('potongan_rs'),
			'pajak_dokter' => $this->input->post('pajak_dokter'),
			'nominal_potongan_rs' => $this->input->post('nominal_potongan_rs'),
			'nominal_pajak_dokter' => $this->input->post('nominal_pajak_dokter'),
			'jasamedis_netto' => $this->input->post('jasamedis_netto'),
			'periode_pembayaran' => YMDFormat($this->input->post('periode_pembayaran')),
			'periode_jatuhtempo' => YMDFormat($this->input->post('periode_jatuhtempo')),
			'status_verifikasi' => $this->input->post('status_verifikasi'),
		];

		if ($dataTindakan['table'] == 'tfeerujukan_dokter') {
			$this->updateJasaFeeRujukanDokter($dataTindakan);
		} elseif ($dataTindakan['table'] == 'tkamaroperasi_jasado') {
			$this->updateJasaDokterOperator($dataTindakan);
		} elseif ($dataTindakan['table'] == 'tkamaroperasi_jasada') {
			$this->updateJasaDokterAnesthesi($dataTindakan);
		} elseif ($dataTindakan['table'] == 'tpoliklinik_pelayanan') {
			$this->updateJasaDokterRawatJalan($dataTindakan);
		} else {
			$this->updateJasaDokter($dataTindakan);
		}

		// Proses Adjust Honor Dokter
		if ($dataTindakan['status_verifikasi'] == '1') {
			$idhonor = $this->Thonor_dokter_model->checkStatusHonorDokter($dataTindakan['iddokter'], $dataTindakan['periode_pembayaran']);
			if ($idhonor == null) {
				$dataHonorDokter = [
					'iddokter' => $dataTindakan['iddokter'],
					'namadokter' => $dataTindakan['namadokter'],
					'tanggal_pembayaran' => $dataTindakan['periode_pembayaran'],
					'tanggal_jatuhtempo' => $dataTindakan['periode_jatuhtempo'],
					'nominal' => 0,
					'created_at' => date('Y-m-d H:i:s'),
					'created_by' => $this->session->userdata('user_id')
				];

				if ($this->db->insert('thonor_dokter', $dataHonorDokter)) {
					$idhonor = $this->db->insert_id();
				}
			}

			$this->db->set('idhonor', $idhonor);
			$this->db->set('idkategori', $dataTindakan['idkategori']);
			$this->db->set('namakategori', $dataTindakan['namakategori']);
			$this->db->set('iddokter', $dataTindakan['iddokter']);
			$this->db->set('namadokter', $dataTindakan['namadokter']);
			$this->db->set('potongan_rs', $dataTindakan['potongan_rs']);
			$this->db->set('nominal_potongan_rs', $dataTindakan['nominal_potongan_rs']);
			$this->db->set('pajak_dokter', $dataTindakan['pajak_dokter']);
			$this->db->set('nominal_pajak_dokter', $dataTindakan['nominal_pajak_dokter']);
			$this->db->set('jasamedis_netto', $dataTindakan['jasamedis_netto']);
			$this->db->set('tanggal_pembayaran', $dataTindakan['periode_pembayaran']);
			$this->db->set('tanggal_jatuhtempo', $dataTindakan['periode_jatuhtempo']);
			if ($dataTindakan['iddetail']) {
				$this->db->where('iddetail', $dataTindakan['iddetail']);
			} else {
				$this->db->where('idtransaksi', $dataTindakan['idrawatinap']);
			}
			$this->db->where('reference_table', $dataTindakan['table']);
			$this->db->update('thonor_dokter_detail');
		}
	}

	public function updateJasaFeeRujukanDokter($dataTindakan)
	{
		$data = [];
		$data['idrawatinap'] = $dataTindakan['idrawatinap'];
		$data['iddokter'] = $dataTindakan['iddokter'];
		$data['namadokter'] = $dataTindakan['namadokter'];
		$data['jasamedis'] = $dataTindakan['jasamedis'];
		$data['pajak_dokter'] = $dataTindakan['pajak_dokter'];
		$data['potongan_rs'] = $dataTindakan['potongan_rs'];
		$data['periode_pembayaran'] = $dataTindakan['periode_pembayaran'];
		$data['periode_jatuhtempo'] = $dataTindakan['periode_jatuhtempo'];
		$data['status_jasamedis'] = 1;

		if ($this->db->replace($dataTindakan['table'], $data)) {
			return true;
		} else {
			return false;
		}
	}

	public function updateJasaDokterOperator($dataTindakan)
	{
		$data = [];
		$data['iddokteroperator'] = $dataTindakan['iddokter'];
		$data['namadokteroperator'] = $dataTindakan['namadokter'];
		$data['pajak'] = $dataTindakan['pajak_dokter'];
		$data['potongan'] = $dataTindakan['potongan_rs'];
		$data['periode_pembayaran'] = $dataTindakan['periode_pembayaran'];
		$data['periode_jatuhtempo'] = $dataTindakan['periode_jatuhtempo'];
		$data['status_jasamedis'] = 1;

		$this->db->where('id', $dataTindakan['iddetail']);
		if ($this->db->update($dataTindakan['table'], $data)) {
			return true;
		} else {
			return false;
		}
	}

	public function updateJasaDokterAnesthesi($dataTindakan)
	{
		$data = [];
		$data['iddokteranastesi'] = $dataTindakan['iddokter'];
		$data['namadokteranastesi'] = $dataTindakan['namadokter'];
		$data['pajak'] = $dataTindakan['pajak_dokter'];
		$data['potongan'] = $dataTindakan['potongan_rs'];
		$data['periode_pembayaran'] = $dataTindakan['periode_pembayaran'];
		$data['periode_jatuhtempo'] = $dataTindakan['periode_jatuhtempo'];
		$data['status_jasamedis'] = 1;

		$this->db->where('id', $dataTindakan['iddetail']);
		if ($this->db->update($dataTindakan['table'], $data)) {
			return true;
		} else {
			return false;
		}
	}

	public function updateJasaDokterRawatJalan($dataTindakan)
	{
		$data = [];
		$data['iddokter'] = $dataTindakan['iddokter'];
		$data['pajak_dokter'] = $dataTindakan['pajak_dokter'];
		$data['pot_rs'] = $dataTindakan['potongan_rs'];
		$data['periode_pembayaran'] = $dataTindakan['periode_pembayaran'];
		$data['periode_jatuhtempo'] = $dataTindakan['periode_jatuhtempo'];
		$data['status_jasamedis'] = 1;

		$this->db->where('id', $dataTindakan['iddetail']);
		if ($this->db->update($dataTindakan['table'], $data)) {
			return true;
		} else {
			return false;
		}
	}

	public function updateJasaDokter($dataTindakan)
	{
		$data = [];
		$data['iddokter'] = $dataTindakan['iddokter'];
		$data['pajak_dokter'] = $dataTindakan['pajak_dokter'];
		$data['potongan_rs'] = $dataTindakan['potongan_rs'];
		$data['periode_pembayaran'] = $dataTindakan['periode_pembayaran'];
		$data['periode_jatuhtempo'] = $dataTindakan['periode_jatuhtempo'];
		$data['status_jasamedis'] = 1;

		$this->db->where('id', $dataTindakan['iddetail']);
		if ($this->db->update($dataTindakan['table'], $data)) {
			// STATUS VERIFICATION [TRUE]
			// 'table' => 'trawatinap_tindakan',
			// 'table' => 'trujukan_laboratorium_detail',
			// 'table' => 'trujukan_radiologi_detail',
			// 'table' => 'trujukan_fisioterapi_detail',
			// 'table' => 'trawatinap_visite',

			return true;
		} else {
			return false;
		}
	}

	public function resetFeeRujukan($idrawatinap)
	{
		$this->db->where('idrawatinap', $idrawatinap);
		if ($this->db->delete('tfeerujukan_dokter')) {
			redirect('tverifikasi_transaksi/edit_jasadokter_ranap/' . $idrawatinap, 'location');
		} else {
			return false;
		}
	}
}
