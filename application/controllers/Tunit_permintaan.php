<?php defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
/*
|--------------------------------------------------------------------------
| DEVELOPER     : IYAN ISYANTO
| EMAIL         : POSDARING@GMAIL.COM
|--------------------------------------------------------------------------
|
*/

class Tunit_permintaan extends CI_Controller {
    public function __construct() {
        parent::__construct();
		PermissionUserLoggedIn($this->session);
        $this->load->library('form_validation');
        $this->load->model('tunit_permintaan_model', 'model');
        $this->load->model('munitpelayanan_model', 'unitpelayanan');
    }

    public function _list($data) {
        
    }
	public function cetak_pengiriman($id) {
        // $id = $id;
        if ($id) {
            $data['error']      = '';
            $data['title']      = 'Cetak Pengiriman';
            $data['content']    = 'Tunit_permintaan/cetak_pengiriman_index';
            $data['list_cetak_pengiriman']  = $this->model->list_cetak_pengiriman($id);
            // $data['detail_permintaan']  = $this->model->detail_permintaan($id);
            $data['head_permintaan']    = $this->model->head_permintaan($id);
            // $data['history_alihan']    = $this->model->history_alihan($id);
			// print_r($data['list_cetak_pengiriman']);exit();
            $data['breadcrum'] = array(
                array("RSKB Halmahera", '#'),
                array("Permintaan Unit", '#'),
                array("List", 'tunit_permintaan')
            );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        }
    }
    public function index() {
		$q="SELECT K.id,H.idunitpenerima,K.kuantitas,K.kuantitas_terima,K.kuantitas-K.kuantitas_terima as sisa,U.st_otomatis_terima 
		,U.jml_hari_terima,HOUR(TIMEDIFF(NOW(),K.tanggal_kirim))/24 as lama_hari,K.tanggal_kirim
		FROM `tunit_pengiriman` K
		INNER JOIN tunit_permintaan H ON H.id=K.idpermintaan
		INNER JOIN munitpelayanan U ON U.id=H.idunitpenerima
		WHERE K.kuantitas != K.kuantitas_terima AND U.st_otomatis_terima='1'";
		$row_otomatis=$this->db->query($q)->result();
		foreach($row_otomatis as $row){
			$st_update='0';
			if ($row->jml_hari_terima=='0'){
				$st_update='1';
			}else{
				if ($row->lama_hari >= $row->jml_hari_terima){
					$st_update='1';
				}
			}
			if ($st_update=='1'){				
				$data['tanggal_terima']=date('Y-m-d H:i:s');
				$data['kuantitas_terima']=$row->sisa;
				$data['user_terima']=0;
				$data['user_nama_terima']='System';			
				$this->db->where('id',$row->id);
				$this->db->update('tunit_pengiriman', $data);
			}
		}
		
        $data = array(
            'iddariunit' => '#',
            'idkeunit' => '#',
            'nopermintaan' => '',
            'statuspenerimaan' => '#',
            'tanggaldari'=> date('d/m/Y', strtotime("-1 day")),
            'tanggalsampai' => date("d/m/Y"),
        );
		$data['idunittampil']=$this->model->user_array_dari_unit();
		$this->session->set_userdata($data);
		$data['array_dari']=$this->model->user_array();
		
		// print_r($data['array_dari']);exit();
		$data['list_tampil']=$this->model->list_tampil();
		$data['list_dari']=$this->model->selectdariunit_index();
		$data['list_ke']=array();
        $data['error'] = '';
        $data['title'] = 'Permintaan Unit';
        $data['content'] = 'Tunit_permintaan/index';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Permintaan Unit", '#'),
            array("List", 'tunit_permintaan')
        );
		// print_r($data);exit();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
        // $this->_list($data);
    }

    public function filter() {
		$user_id=$this->session->userdata('user_id');
		$dariunit=$this->input->post('iddariunit');
		$idunittampil=$this->input->post('idunittampil');
		if ($idunittampil && $idunittampil!='0'){			
			$idunittampil=implode(",", $idunittampil);
			$this->db->query("UPDATE musers set dari_unit='$idunittampil' where id='$user_id'");
			// print_r($this->session->userdata('user_id'));exit();
		}else{
			$this->db->query("UPDATE musers set dari_unit='' where id='$user_id'");
		}
		// print_r($dariunit);exit();
        $data = array(
            'iddariunit' => $dariunit,
            'nopermintaan' => $this->input->post('nopermintaan'),
            'idkeunit' => $this->input->post('idkeunit'),
            'statuspenerimaan' => $this->input->post('statuspenerimaan'),
            'tanggaldari' => $this->input->post('tanggaldari'),
            'tanggalsampai' => $this->input->post('tanggalsampai'),
        );
		
		$data['idunittampil']=$idunittampil;
		$this->session->set_userdata($data);
		$data['list_tampil']=$this->model->list_tampil();
		$data['list_dari']=$this->model->selectdariunit_index();
		$data['list_ke']=$this->model->selectkeunit_index($dariunit,'1');
        // if ($this->input->post('iddariunit')!="#" && $idunittampil) {
            
        // }else{
			// $data['list_ke']=array();
		// }
		// if ($dariunit!=''){		
		// print_r('sini'.$dariunit);exit();
		if ($this->input->post('idunittampil')!=''){
			$data['array_dari']=$this->input->post('idunittampil');
		}else{			
			$data['array_dari']=array();
		}
		// print_r($data['array_dari']);exit();
		// }else{
			// $data['array_dari']=array();
		// }
		// print_r($this->input->post('iddariunit'));exit();
        // if ($this->input->post('idkeunit')!="") {
            // $data['nmkeunit']=$this->unitpelayanan->getSpecified($this->input->post('idkeunit'))->nama;
        // }
        $data['error'] = '';
        $data['title'] = 'Permintaan Unit';
        $data['content'] = 'Tunit_permintaan/index';
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Permintaan Unit", '#'),
            array("List", 'tunit_permintaan')
        );
		// print_r($data);exit();
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    public function add() {
        $data['error']      = '';
        $data['statusstok']      = '#';
        $data['title']      = 'Tambah Permintaan';
        $data['content']    = 'Tunit_permintaan/manage';
        $data['unitpelayanandef']    = $this->model->getDefaultUnitPelayananUser();
        $data['list_unit_pelayanan_user']    = $this->model->list_unit_pelayanan_user();
		$data['list_tipe_barang']    = $this->model->list_tipe_barang($data['unitpelayanandef']);
		$data['list_unit_ke']    = $this->model->list_unit_ke($data['unitpelayanandef']);
        $data['list_user']    = $this->model->get_user();
		// print_r( $data['list_unit_pelayanan_user'] );exit();
        $data['breadcrum'] = array(
            array("RSKB Halmahera", '#'),
            array("Permintaan Unit", '#'),
            array("List", 'tunit_permintaan')
        );
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
	function print_pengiriman($id,$tanggal='',$jenis='1'){
		// print_r(replace_string($tanggal,'_',' '));exit();
		$tanggal=replace_string($tanggal,'_',' ');
		$tanggal=YMDMenitFormat($tanggal);
		// print_r($tanggal);exit();
		$data=array();
		$data['detail_pengiriman']  = $this->model->get_report_detail_pengiriman($id,$tanggal);
		$row   = $this->model->get_report_head_permintaan($id);
		if ($row){
			$data['nopermintaan']=$row->nopermintaan;
			$data['unit_dari']=$row->unit_dari;
			$data['unit_ke']=$row->unit_ke;
			// $data['namauser']=$row->namauser;
			$data['datetime']=DMYTimeFormat($tanggal);
		}
		$user=$this->model->get_user_kirim_terima($id,$tanggal);
		// print_r($tanggal);exit();
		if ($user){
			$data['user_nama_kirim']=$user->user_nama_kirim;
			$data['user_nama_terima']=$user->user_nama_terima;
		}else{
			$data['user_nama_kirim']='';
			$data['user_nama_terima']='';
		}
		// print_r($data['user_nama_kirim']);exit();
		$options = new Options();


        $options->set('isRemoteEnabled', true);

		$options->set('enable-javascript', true);
		$options->set('javascript-delay', 13500);
		$options->set('enable-smart-shrinking', true);
		$options->set('no-stop-slow-scripts', true);
        $dompdf = new Dompdf($options);
		// print_r($jenis);exit();
		if ($jenis=='1'){
			$html = $this->parser->parse('Tunit_permintaan/cetak_pengiriman',array_merge($data,backend_info()),TRUE);
			$html = $this->parser->parse_string($html,$data);
			// print_r($html);exit();
			$dompdf->loadHtml($html);
			$dompdf->setPaper('A4', 'portrait');
		}else{
			$html = $this->parser->parse('Tunit_permintaan/cetak_pengiriman_thermal',array_merge($data,backend_info()),TRUE);
			$html = $this->parser->parse_string($html,$data);
			// print_r($html);exit();
			$dompdf->loadHtml($html);
			// $dompdf->setPaper('A4', 'portrait');
			$customPaper = array(0,0,226,4000);
			$dompdf->set_paper($customPaper);
		}

        $dompdf->render();
        $dompdf->stream('Cetak Pengiriman.pdf', array("Attachment"=>0));

	}
	function cetak_permintaan($id,$jenis='2'){
		// $q="UPDATE tunit_permintaan_detail INNER JOIN
			// (
				// SELECT D.id,S.stok FROM tunit_permintaan H
				// INNER JOIN tunit_permintaan_detail D ON D.idpermintaan=H.id
				// INNER JOIN mgudang_stok S ON S.idunitpelayanan=H.idunitpenerima AND S.idtipe=D.idtipe AND S.idbarang=D.idbarang
				// WHERE H.id='$id' AND D.sisa_stok_unit IS NULL
			// ) T ON tunit_permintaan_detail.id=T.id
			// SET sisa_stok_unit=T.stok";
		// $this->db->query($q);
		$data=array();
		$data['detail_permintaan']  = $this->model->get_report_detail_permintaan($id);
		$row   = $this->model->get_report_head_permintaan($id);
		if ($row){
			$data['nopermintaan']=$row->nopermintaan;
			$data['unit_dari']=$row->unit_dari;
			$data['unit_ke']=$row->unit_ke;
			$data['namauser']=$row->namauser;
			$data['datetime']=DMYTimeFormat($row->datetime);
		}
		$options = new Options();


        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);
		$dompdf->set_option('isRemoteEnabled', TRUE);
		// $this->parser->parse('Tunit_permintaan/cetak_permintaan',array_merge($data,backend_info()));
		$html = $this->parser->parse('Tunit_permintaan/cetak_permintaan',array_merge($data,backend_info()),TRUE);
        $html = $this->parser->parse_string($html,$data);
        // print_r($html);exit();
		$dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
		$dompdf->set_option('isFontSubsettingEnabled', true);
		$dompdf->output();
        $dompdf->render();
        $dompdf->stream('Cetak Permintaan.pdf', array("Attachment"=>0));

	}
	function cetak_permintaan_thermal($id,$jenis='2'){
		$data=array();
		$data['detail_permintaan']  = $this->model->get_report_detail_permintaan($id);
		$row   = $this->model->get_report_head_permintaan($id);
		if ($row){
			$data['nopermintaan']=$row->nopermintaan;
			$data['unit_dari']=$row->unit_dari;
			$data['unit_ke']=$row->unit_ke;
			$data['namauser']=$row->namauser;
			$data['datetime']=DMYTimeFormat($row->datetime);
		}
		$options = new Options();


        $options->set('isRemoteEnabled', true);

		$options->set('enable-javascript', true);
		$options->set('javascript-delay', 13500);
		$options->set('enable-smart-shrinking', true);
		$options->set('no-stop-slow-scripts', true);
        $dompdf = new Dompdf($options);

		 $html = $this->parser->parse('Tunit_permintaan/cetak_permintaan_thermal',array_merge($data,backend_info()),TRUE);
        $html = $this->parser->parse_string($html,$data);
        // print_r($html);exit();
		$dompdf->loadHtml($html);
        // $dompdf->setPaper('A4', 'portrait');
        $customPaper = array(0,0,226,4000);
        $dompdf->set_paper($customPaper);
        $dompdf->render();
        $dompdf->stream('Cetak Permintaan.pdf', array("Attachment"=>0));

	}
	public function get_user() {
		$user=$this->session->userdata('user_id');
		$q="SELECT id, name as text, TRUE as selected from musers WHERE id='$user'";
		// print_r($q);exit();
		$query=$this->db->query($q);
		$res=$query->result_array();
		// $this->db->select('id, name as text, TRUE as selected');
		// $this->db->where('id',$this->session->userdata('user_id'));
		// $res = $this->db->get('musers')->result_array();
		$this->output->set_output(json_encode($res));
	}
    public function acc_pengiriman($id) {
        // $id = $id;
        if ($id) {
            $data['error']      = '';
            $data['title']      = 'Konfirmasi Pengiriman';
            $data['content']    = 'Tunit_permintaan/acc_pengiriman';
            $data['detail_kirim']  = $this->model->detail_kirim($id);
            $data['detail_permintaan']  = $this->model->detail_permintaan($id);
            $data['head_permintaan']    = $this->model->head_permintaan($id);
            $data['history_alihan']    = $this->model->history_alihan($id);
			// print_r($data['detail_permintaan']);exit();
            $data['breadcrum'] = array(
                array("RSKB Halmahera", '#'),
                array("Permintaan Unit", '#'),
                array("List", 'tunit_permintaan')
            );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        }
    }
	public function edit($id) {
        // $id = $id;
        if ($id) {
            $data['error']      = '';
            $data['title']      = 'Edit Barang';
            $data['content']    = 'Tunit_permintaan/edit';
            // $data['detail_kirim']  = $this->model->detail_kirim($id);
            $data['detail_permintaan']  = $this->model->detail_permintaan($id);
            $data['head_permintaan']    = $this->model->head_permintaan($id);
			// print_r($data['head_permintaan']['idunitpeminta']);exit();
            $data['get_list_tipe']    = $this->model->get_list_tipe($data['head_permintaan']['idunitpenerima'],$data['head_permintaan']['idunitpeminta']);
            // $data['history_alihan']    = $this->model->history_alihan($id);
            $data['breadcrum'] = array(
                array("RSKB Halmahera", '#'),
                array("Permintaan Unit", '#'),
                array("List", 'tunit_permintaan')
            );
			// print_r($data);exit();
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        }
    }
	public function alihkan() {
        $id = $this->uri->segment(3);
        if ($id) {
            $data['error']      = '';
            $data['title']      = 'Konfirmasi Pengalihan';
            $data['idpermintaan']    = $id;
            $data['content']    = 'Tunit_permintaan/pengalihan';
            $data['detail_kirim']  = $this->model->detail_kirim($id);
            $data['detail_permintaan']  = $this->model->detail_permintaan($id);
            $data['head_permintaan']    = $this->model->head_permintaan($id);
            $data['idkeunit']    = $data['head_permintaan']['idunitpeminta'];
            $data['list_barang_alihan']    = $this->model->list_barang_alihan($id);
			// print_r($data['head_permintaan']);exit();
            $data['list_unit']    = $this->model->list_unit_alihan($data['head_permintaan']['idunitpenerima'],$data['head_permintaan']['idunitpeminta']);
            // $data['list_unit']    = $this->model->select_ke_unit(1,3,1,0);
            $data['breadcrum'] = array(
                array("RSKB Halmahera", '#'),
                array("Permintaan Unit", '#'),
                array("List", 'tunit_permintaan')
            );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        }
    }

    public function unit_penerimaan() {
        $id = $this->uri->segment(3);
        if ($id) {
            $data['error']      = '';
            $data['title']      = 'Unit Penerimaan';
            $data['content']    = 'Tunit_permintaan/unit_penerimaan';
            $data['detail_permintaan']  = $this->model->detail_penerimaan($id);
            $data['head_permintaan']    = $this->model->head_permintaan($id);
            $data['histori_terima']    = $this->model->histori_terima($id);
            $data['breadcrum'] = array(
                array("RSKB Halmahera", '#'),
                array("Permintaan Unit", '#'),
                array("List", 'tunit_permintaan')
            );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        }
    }

    public function view_detail() {
        $id = $this->uri->segment(3);
        if ($id) {
            $data['error']      = '';
            $data['title']      = 'Lihat Detail';
            $data['content']    = 'Tunit_permintaan/view_detail';
            $data['detail_permintaan']  = $this->model->detail_permintaan($id);
            $data['head_permintaan']    = $this->model->head_permintaan($id);
            $data['breadcrum'] = array(
                array("RSKB Halmahera", '#'),
                array("Permintaan Unit", '#'),
                array("List", 'tunit_permintaan')
            );
            $data = array_merge($data, backend_info());
            $this->parser->parse('module_template', $data);
        }
    }
	public function save_edit(){
		if ($this->model->save_edit()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
            redirect('tunit_permintaan/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tunit_permintaan/index', 'refresh');
        }
		// print_r($this->input->post());exit();
	}
    public function save() {
		
        if ($this->model->save()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
            redirect('tunit_permintaan/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tunit_permintaan/index', 'refresh');
        }
    }
	public function save_alihkan() {
        if ($this->model->save_alihkan()) {
            $this->session->set_flashdata('confirm', true);
            $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
            redirect('tunit_permintaan/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tunit_permintaan/index', 'refresh');
        }
    }

    public function save_acc_pengiriman() {
        $this->form_validation->set_rules('id[]', 'ID', 'trim|required');
        $this->form_validation->set_rules('kuantitaskirim[]', 'Kuantitas Kirim', 'trim|required');
        if ($this->form_validation->run() == true) {
            if ($this->model->save_acc_pengiriman()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
                redirect('tunit_permintaan/index', 'refresh');
            }
        } else {
            $this->session->set_flashdata('error', true);
            $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            redirect('tunit_permintaan/index', 'refresh');
        }
    }

    public function save_pengalihan_permintaan() {
        if ($this->model->save_pengalihan_permintaan()) {
            $data['status'] = '200';
        } else {
            $data['status'] = '200';
        }
        $this->output->set_output(json_encode($data));
    }

    public function save_pemesanan_distributor() {
        if ($this->model->save_pemesanan_distributor()) {
            $data['status'] = '200';
        } else {
            $data['status'] = '400';
        }
        $this->output->set_output(json_encode($data));
    }

    public function save_penerimaan() {
        // $this->form_validation->set_rules('idbarang[]', 'Barang', 'trim|required');
        // $this->form_validation->set_rules('idtipe[]', 'Tipe', 'trim|required');
        // $this->form_validation->set_rules('kuantitasterima[]', 'Kuantitas Kirim', 'trim|required');
        // if ($this->form_validation->run() == true) {
            if ($this->model->save_penerimaan()) {
                $this->session->set_flashdata('confirm', true);
                $this->session->set_flashdata('message_flash', 'Data berhasil disimpan.');
                redirect('tunit_permintaan/index', 'refresh');
            }
        // } else {
            // $this->session->set_flashdata('error', true);
            // $this->session->set_flashdata('message_flash', 'Data gagal disimpan.');
            // redirect('tunit_permintaan/index', 'refresh');
        // }
    }
    public function delete($id) {
        $this->model->delete($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah terhapus.');
        redirect('tunit_permintaan/index', 'refresh');
    }

	public function tolak($id) {
        $this->model->tolak($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'data telah ditolak.');
        redirect('tunit_permintaan/index', 'refresh');
    }
	public function selesaikan($id) {
        $this->model->selesaikan($id);
        $this->session->set_flashdata('confirm', true);
        $this->session->set_flashdata('message_flash', 'Permintaan Telah Diselesaikan.');
        redirect('tunit_permintaan/index', 'refresh');
    }


    // additional
    public function selectbarang() {
        $this->model->selectbarang();
    }
    public function ajax_cek_stok() {
        $this->model->ajax_cek_stok();
    }
	public function ajax_cek_stok2() {
        $this->model->ajax_cek_stok2();
    }
    public function selected_barang_kode() {
        $this->model->selected_barang_kode();
    }
    public function selected_barang_harga() {
        $this->model->selected_barang_harga();
    }
    public function selectbarang_edit() {
        $this->model->selectbarang_edit();
    }
    public function selected_barang() {
        $this->model->selected_barang();
    }
    public function view_barang() {
        $this->model->view_barang();
    }
    public function dtpermintaan_1() {
        echo json_encode($this->model->where_in_unit());
    }
    public function dtpermintaan() {
        $this->model->dtpermintaan();
    }
    public function dtpermintaan_detail() {
        $this->model->dtpermintaan_detail();
    }
    public function dtpermintaan_detail_penerimaan() {
        $this->model->dtpermintaan_detail_penerimaan();
    }
    public function dtbarang() {
        $this->model->dtbarang();
    }
    public function filter_distributor() {
        $this->model->filter_distributor();
    }
    public function ajax_stokunit() {
        $this->model->ajax_stokunit();
    }
    public function ajax_distributor_tersering() {
        $this->model->ajax_distributor_tersering();
    }
    public function ajax_namabarang() {
        $this->model->ajax_namabarang();
    }
    public function ajax_pernah_terima() {
        $this->model->ajax_pernah_terima();
    }
    public function ajax_cekstatuspermintaan() {
        $this->model->ajax_cekstatuspermintaan();
    }
    public function selectdariunit() {
        $this->model->selectdariunit();
    }
    public function get_tipe($idunit) {
        $this->model->get_tipe($idunit);
    }
	public function get_unit_ke($idunit) {
        $this->model->list_unit_ke_json($idunit);
    }
     public function selectkeunit() {
        $this->model->selectkeunit();
    }
    public function selectkeunit_array() {
	
        $this->model->selectkeunit_array();
    }
    public function selectkeunit_index($id) {
	
        $this->model->selectkeunit_index($id,'0');
    }
    public function selectkeunit_validasi() {
        $this->model->selectkeunit_validasi();
    }
    public function selectkeunit_tipe() {
        $this->model->selectkeunit_tipe();
    }
    public function selectdariunit_alihan() {
        $this->model->selectdariunit_alihan();
    }
    public function ke_unit_list_detail() {
        $this->model->ke_unit_list_detail();
    }
    public function selectdistributor() {
        $this->model->selectdistributor();
    }
    public function konversi_satuan() {
        $this->model->konversi_satuan();
    }

    public function select_tipebarang() {
        $this->model->select_tipebarang();
    }

    public function get_index() {
        $idkeunit = $this->session->userdata('idkeunit');
        $iddariunit = $this->session->userdata('iddariunit');
        $idunittampil = $this->session->userdata('idunittampil');
		// print_r($iddariunit);exit();
        $nopermintaan = $this->session->userdata('nopermintaan');
        $status= $this->session->userdata('statuspenerimaan');
        $tanggaldari = $this->session->userdata('tanggaldari');
        $tanggalsampai = $this->session->userdata('tanggalsampai');
		$where1='';
		$where='';
		$user_login=$this->session->userdata('user_id');
		if ('' != $tanggaldari) {
            $where1 .= " AND DATE(H.datetime) >='".YMDFormat($tanggaldari)."'";
        }
        if ('' != $tanggalsampai) {
            $where1 .= " AND DATE(H.datetime) <='".YMDFormat($tanggalsampai)."'";
        }
		if ($idkeunit!="#" && $idkeunit!="") {
            $where1.=" AND H.idunitpeminta=".$idkeunit;
        }
        if ($iddariunit!='' && $iddariunit!='#') {
            $where1.=" AND H.idunitpenerima IN (".$iddariunit.")";
        }
		if ($nopermintaan!="") {
            $where1.=" AND H.nopermintaan='".$nopermintaan."'";
        }
		if ($idkeunit=='#' && $iddariunit=='#' && $idunittampil !=''){
			$where1 .=" AND (H.idunitpeminta IN (".$idunittampil.") OR H.idunitpenerima IN (".$idunittampil."))";
			// $where.=" AND (iddariunit IN (".$idunittampil.") OR idkeunit IN (".$idunittampil."))";
		}
		$where_status='';
        if ($status!="#") {
			if ($status=='11' || $status=='12'){
				if ($status=='11'){
					$where_status.=" WHERE status='0' AND st_hapus='1'";//BATALAKN
				}else{
					$where_status.=" WHERE status='0' AND st_hapus='2'";//TOLAK
				}
			}else{
				$where_status.=" WHERE status='".$status."'";
			}
        }
        // $from = "(SELECT T.*,CASE
			// WHEN (T.tot_kirim > tot_terima) AND (T.tot_kirim < T.totalpesan) THEN '2'
			// WHEN (T.tot_kirim > tot_terima) AND (T.tot_kirim = T.totalpesan) THEN '3'
			 // WHEN (T.tot_kirim = tot_terima) AND (totalpesan=tot_terima) THEN '4'
			// WHEN (T.totalpesan = totalalih) THEN '5'
			// WHEN (T.totalalih > 0) AND (T.tot_kirim > 0) THEN '6'
			// ELSE T.status_asal
		// END as status FROM(SELECT v_tunit_permintaan.*
		// ,COALESCE(D.userid,0) as D_user,COALESCE(K.userid,0) as K_user
		// from v_tunit_permintaan
		// LEFT JOIN munitpelayanan_user D ON  D.idunitpelayanan=v_tunit_permintaan.iddariunit AND D.userid='$user_login'
		// LEFT JOIN munitpelayanan_user K ON K.idunitpelayanan=v_tunit_permintaan.idkeunit AND K.userid='$user_login' WHERE (D.userid IS NOT NULL OR K.userid IS NOT NULL)  ";
		$from="(SELECT T.* 
				,CASE
							WHEN (T.tot_kirim > tot_terima) AND (T.tot_kirim < T.totalpesan) THEN '2'
							WHEN (T.tot_kirim > tot_terima) AND (T.tot_kirim = T.totalpesan) THEN '3'
							 WHEN (T.tot_kirim = tot_terima) AND (totalpesan=tot_terima) THEN '4'
							WHEN (T.totalpesan = totalalih) THEN '5'
							WHEN (T.totalalih > 0) AND (T.tot_kirim > 0) THEN '6'
							ELSE T.status_asal
						END as status
				FROM (
				SELECT H.id,H.nopermintaan
				,UD.id as iddariunit,UK.id as idkeunit
				,H.totalbarang as totalpesan,H.totalbarangalih as totalalih
				,IFNULL(SUM(TK.kuantitas),0) as tot_kirim
				,IFNULL(SUM(TK.kuantitas_terima),0) as tot_terima,H.`status` as status_asal,H.st_hapus,H.datetime
				,(SELECT group_concat( `munitpelayanan_user`.`userid` SEPARATOR ',' ) FROM `munitpelayanan_user` WHERE ( `munitpelayanan_user`.`idunitpelayanan` = `H`.`idunitpenerima` ) ) AS `user_dariunit`
				,(SELECT group_concat( `munitpelayanan_user`.`userid` SEPARATOR ',' ) FROM `munitpelayanan_user` WHERE ( `munitpelayanan_user`.`idunitpelayanan` = `H`.`idunitpeminta` ) ) AS `user_keunit` 
				,COALESCE ( DR.userid, 0 ) AS D_user
				,COALESCE ( K.userid, 0 ) AS K_user 
				,CASE WHEN H.idunitpenerima='0' THEN 'Gudang Farmasi' ELSE  UD.nama END as dariunit
			,CASE WHEN H.idunitpeminta='0' THEN 'Gudang Farmasi' ELSE UK.nama END as keunit
				FROM tunit_permintaan H
				LEFT JOIN munitpelayanan UD ON UD.id=H.idunitpenerima
				LEFT JOIN munitpelayanan UK ON UK.id=H.idunitpeminta
				LEFT JOIN tunit_pengiriman TK ON TK.idpermintaan=H.id 
				LEFT JOIN munitpelayanan_user DR ON DR.idunitpelayanan = H.idunitpenerima AND DR.userid = '$user_login'
				LEFT JOIN munitpelayanan_user K ON K.idunitpelayanan = H.idunitpeminta AND K.userid = '$user_login' 
				WHERE H.status IS NOT NULL 
				".$where1."				
				AND ( DR.userid IS NOT NULL OR K.userid IS NOT NULL ) 
				GROUP BY H.id
				ORDER BY H.id DESC
				) T )tbl ".$where_status;
        
		
        
        // $from .=" GROUP BY v_tunit_permintaan.id)T) as tbl ".$where_status;
		// print_r($from);exit();
        $this->select           = array();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array('id' => 'desc');
        $this->group            = array();
        $this->column_search    = array('nopermintaan','dariunit','keunit');
        $this->column_order     = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];

        foreach ($list as $r) {
            $no++;
            $row = array();

            $aksi       = '<div class="btn-group">';
            $status     = '';
            $url        = site_url('tunit_permintaan/');

			$st_terima=$this->cek_user($r->user_dariunit);
			$st_kirim=$this->cek_user($r->user_keunit);
			$tombol_kirim='';$tombol_terima='';
			if ($st_terima=='1'){
				if (UserAccesForm($user_acces_form,array('1204'))){
				$tombol_terima='<li class="dropdown-header">Permintaan</li>
									<li>
										<a tabindex="-1" href="'.$url.'cetak_permintaan/'.$r->id.'" target="blank">Bukti Permintaan</a>
									</li>
									<li>
										<a tabindex="-1" href="'.$url.'cetak_permintaan_thermal/'.$r->id.'"  target="blank">Bukti Permintaan Thermal</a>
									</li>
									';
				}
			}
			if ($st_kirim=='1'){
				if ($r->status != 5){
					if (UserAccesForm($user_acces_form,array('1205'))){
					$tombol_kirim='<li class="dropdown-header">Pengiriman</li>
									<li>
										<a tabindex="-1" href="'.$url.'cetak_pengiriman/'.$r->id.'"  target="blank">Bukti Pengiriman</a>
									</li>';
					}
				}else{
					$tombol_kirim='';
				}

			}
				$tombol_print='';
			if ($st_terima =='0' && $st_kirim=='0'){
				$tombol_print='';
			}else{
				if (UserAccesForm($user_acces_form,array('1204','1205'))){
					$tombol_print='<div class="btn-group" role="group">
								<button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<span class="fa fa-print"></span>
								</button>
								<ul class="dropdown-menu dropdown-menu-right">
									'.$tombol_terima.$tombol_kirim.'
								</ul>
							</div>';
				}
			}




            if ($r->status == 0) { 
				if ($r->st_hapus=='1'){//Dibatalkan
					$status .= '<label class="label label-danger">Dibatalkan</label>';
				 }else{
					$status .= '<label class="label label-danger">Ditolak</label>';
				 }
				 $aksi   .= '<a href="'.$url.'view_detail/'.$r->id.'" class="btn btn-xs btn-info"  title="Lihat"><i class="fa fa-eye"></i></a>';
             } elseif  ($r->status == 1){//READY
                $status .= '<label class="label label-warning">Proses Permintaan</label>';
                $aksi   .= '<a href="'.$url.'view_detail/'.$r->id.'" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>'.$tombol_print;
				if (UserAccesForm($user_acces_form,array('1106'))){
				$aksi   .= '<a href="'.$url.'edit/'.$r->id.'" class="btn btn-xs btn-success"  title="Edit"><i class="fa fa-pencil"></i></a>';
				}
                if ($st_kirim){
					if (UserAccesForm($user_acces_form,array('1104'))){
					$aksi   .= '<a href="'.$url.'acc_pengiriman/'.$r->id.'" class="btn btn-xs btn-success"  title="Kirim">Kirim</a>';
					}
				}
				if ($st_kirim && $r->tot_kirim > 0){
					$aksi   .= '<a href="'.$url.'selesaikan/'.$r->id.'" class="btn btn-xs btn-info"  title="Selesaikan">Selesaikan</a>';
				}
				if ($st_kirim=='1' && $r->tot_kirim == '0'){
					if (UserAccesForm($user_acces_form,array('1107'))){
					$aksi   .= '<a href="'.$url.'tolak/'.$r->id.'" class="btn btn-xs btn-danger" title="Tolak">Tolak</a>';
					}
				}
				if ($st_terima=='1' && $r->tot_kirim == '0'){
					if (UserAccesForm($user_acces_form,array('1108'))){
					$aksi   .= '<a href="'.$url.'delete/'.$r->id.'" class="btn btn-xs btn-danger" title="Batalkan"><i class="fa fa-times"></i></a>';
					}
				}  
			} elseif ($r->status == 2) {
				if ($st_kirim=='1' && $st_terima=='1'){
					$status .= '<label class="label label-warning">Proses Permintaan</label><br><br><label class="label label-info">Proses Kirim</label><br><br><label class="label label-info">Proses Terima</label>';					
				}elseif($st_kirim=='1'){
					$status .= '<label class="label label-warning">Proses Permintaan</label><br><br><label class="label label-info">Proses Kirim</label>';	
				}else{
					$status .= '<label class="label label-warning">Proses Permintaan</label><br><br><label class="label label-info">Proses Terima</label>';
				}
                $aksi   .= '<a href="'.$url.'view_detail/'.$r->id.'" class="btn btn-xs btn-info"  title="Lihat"><i class="fa fa-eye"></i></a>'.$tombol_print;
                if ($st_kirim=='1'){
					if (UserAccesForm($user_acces_form,array('1106'))){
					$aksi   .= '<a href="'.$url.'edit/'.$r->id.'" class="btn btn-xs btn-success"  title="Edit"><i class="fa fa-pencil"></i></a>';
					}
					if (UserAccesForm($user_acces_form,array('1104'))){
						$aksi   .= '<a href="'.$url.'acc_pengiriman/'.$r->id.'" class="btn btn-xs btn-success">Kirim</a>';
					}
                }
				if ($st_terima=='1'){
					if (UserAccesForm($user_acces_form,array('1203'))){
				$aksi   .= '<a href="'.$url.'unit_penerimaan/'.$r->id.'" class="btn btn-xs btn-warning">Terima</a>';
					}
				}
            //}
           			
            } elseif ($r->status == 3) {//SUDAH DIKIRIM
				if ($st_kirim=='1' && $st_terima=='1'){
					$status .= '<label class="label label-info">Proses Kirim</label><br><br><label class="label label-info">Proses Terima</label>';					
				}elseif($st_kirim=='1'){
					$status .= '<label class="label label-info">Proses Kirim</label>';	
				}else{
					$status .= '<label class="label label-info">Proses Terima</label>';
				}
                $aksi   .= '<a href="'.$url.'view_detail/'.$r->id.'" class="btn btn-xs btn-info"  title="Lihat"><i class="fa fa-eye"></i></a>'.$tombol_print;
				
				if ($st_terima){
					if (UserAccesForm($user_acces_form,array('1203'))){
						$aksi   .= '<a href="'.$url.'unit_penerimaan/'.$r->id.'" class="btn btn-xs btn-warning"  title="Terima">Terima</a>';
					}
				 }
            
             } elseif  ($r->status == 4){//SUDAH SELESAI
				 $status .= '<label class="label label-success">Selesai</label>';
                $aksi   .= '<a href="'.$url.'view_detail/'.$r->id.'" class="btn btn-xs btn-info"  title="Lihat"><i class="fa fa-eye"></i></a>'.$tombol_print;
			 } elseif  ($r->status == 5){//Alihkan Semua
				 $status .= '<label class="label label-primary">Alihkan Semua</label>';
                 $aksi   .= '<a href="'.$url.'view_detail/'.$r->id.'" class="btn btn-xs btn-info"  title="Lihat"><i class="fa fa-eye"></i></a>'.$tombol_print;
			 } elseif  ($r->status == 6){//Alihkan Sebagian
				 $status .= '<label class="label label-primary">Alihkan Sebagian</label>';
                 $aksi   .= '<a href="'.$url.'view_detail/'.$r->id.'" class="btn btn-xs btn-info"  title="Lihat"><i class="fa fa-eye"></i></a>'.$tombol_print;
			 } elseif  ($r->status == 7){//Alihkan Selesai
				 $status .= '<label class="label label-primary">Alihkan Selesai</label>';
                 $aksi   .= '<a href="'.$url.'view_detail/'.$r->id.'" class="btn btn-xs btn-info"  title="Lihat"><i class="fa fa-eye"></i></a>'.$tombol_print;
			}
            $aksi .= '</div>';

            $row[] = $no;
            $row[] = $r->datetime;
            $row[] = $r->nopermintaan;
            $row[] = $r->dariunit;
            $row[] = $r->keunit;
            $row[] = $r->totalpesan;
            $row[] = $r->tot_kirim;
            $row[] = $r->totalalih;
            $row[] = $r->tot_terima;
            $row[] = $status;
            // $row[] = $r->status;
			// $row[] = $st_kirim;
            // $row[] = $r->user_dariunit;
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function cek_user($str=''){
		// $str="1,2,8";
		$arr=explode(',',$str);
		// print_r($arr);exit();
		if (in_array($this->session->userdata('user_id'), $arr)){
			return "1";
		}else{
			return "0";
		}

	}
    private function _cek_user_unit_pelayanan($idunitpelayanan) {
        $this->db->where('userid', $this->session->userdata('user_id'));
        $this->db->where('idunitpelayanan', $idunitpelayanan);
        $get = $this->db->get('munitpelayanan_user');
        if ($get->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
	function getBarang()
    {
		$iddariunit     = $this->input->post('idunit');
		$id     		= $this->input->post('id');
		$idtipe     	= $this->input->post('idtipe');
		$idtipe=implode(",", $idtipe);
		$idkategori    = $this->input->post('idkategori');
		$statusstok    = $this->input->post('statusstok');
		$iduser=$this->session->userdata('user_id');
		
		// $data_user=get_acces();
		// $user_acces_form=$data_user['user_acces_form'];
			$this->select = array();
    		$this->join 	= array();
    		$this->where  = array();
		$where='';
        if ($idtipe){
			$where .=" AND TT.idtipe IN($idtipe)";
		}else{
			$where .=" AND TT.idtipe='$idtipe'";
		}
		if ($idkategori  != '#'){
			
			$where .=" AND TT.idkategori IN (
							SELECT id FROM (														
									SELECT *FROM view_kategori_alkes
									UNION
									SELECT *FROM view_kategori_implan
									UNION
									SELECT *FROM view_kategori_obat
									UNION
									SELECT *FROM view_kategori_logistik
							) T1
							where T1.path like '$idkategori%'
			)";
		}
		if ($statusstok  != '#'){
			if ($statusstok=='1'){
				$where .=" AND TT.stok > TT.stokreorder";
				
			}
			if ($statusstok=='2'){
				$where .=" AND (TT.stok <= TT.stokreorder AND TT.stok >= TT.stokminimum)";				
			}
			if ($statusstok=='3'){
				$where .=" AND TT.stok <= TT.stokminimum";				
			}
		}
			$from="(SELECT *FROM (SELECT S.id,B.idtipe,B.id as idbarang,B.nama as namabarang,
					B.kode,B.namatipe,B.idkategori, K.nama as namakategori,
					B.idsatuan,Sat.nama as namasatuan,COALESCE(S.stok,0) as stok,B.stokreorder,B.stokminimum,B.hargabeli as hpp,S.idunitpelayanan 
					FROM view_barang B					
					LEFT JOIN mgudang_stok S ON S.idtipe=B.idtipe AND B.id=S.idbarang AND S.idunitpelayanan='$iddariunit'
					LEFT JOIN mdata_kategori K ON K.id=B.idkategori
					LEFT JOIN msatuan Sat ON Sat.id=B.idsatuan 

					ORDER BY B.idtipe,B.nama) as TT WHERE TT.namabarang <> '' ".$where.") as tbl";
			
    		$this->order  = array();
    		$this->group  = array();
    		$this->from   = $from;
        $this->column_search   = array('namabarang','namakategori','namatipe','kode');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->namatipe;
            $row[] = $r->kode;
            $row[] = $r->namabarang.' '.($r->id?text_success('RO'):text_danger('new'));
            $row[] = ($r->stok < 0) ? text_danger(number_format($r->stok,0)):number_format($r->stok,0);
            $row[] = $r->namasatuan;
            
			if ($r->stok > $r->stokreorder){
				$row[] = text_success('Available');				
			}else{
				if ($r->stok > $r->stokminimum){
					$row[] = text_warning('Back To Order');	
				}else{
					$row[] = text_danger('Stok Minimum');						
				}
			}
			
			$aksi       = '<div class="btn-group">';			
			$aksi 		.= '<a href="#" class="btn btn-xs btn-success">Pilih Barang</a>';
			if ($r->id){				
				$aksi 		.= '<a target="_blank" href="'.site_url().'lgudang_stok/kartustok/'.$r->idtipe.'/'.$r->idbarang.'/'.$r->idunitpelayanan.'" class="btn btn-xs btn-danger ks" title="Kartu Stok"><i class="fa fa-database"></i></a>';
			}
            $aksi.='</div>';
			$row[] = $aksi;
			$row[] = $r->idbarang;
			$row[] = $r->idtipe;
			$data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
}

/* End of file Tunit_permintaan.php */
/* Location: ./application/controllers/Tunit_permintaan.php */
