<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msetting_piutang extends CI_Controller {

	/**
	 * Setting Piutang Tidak Tertagih controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('msetting_piutang_model');
  }

	function index(){
		
		$data['error'] 			= '';
		$data['title'] 			= 'Setting Piutang Tidak Tertagih';
		$data['content'] 		= 'Msetting_piutang/manage';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Setting Piutang Tidak Tertagih",'#'),
									    			array("List",'mlogic_kasbon')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function load_setting()
    {
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(
				SELECT idtipe,nama_tipe,jatuh_tempo,reminder FROM msetting_piutang M
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('nama_tipe');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->idtipe;            
            $row[] = ($r->nama_tipe);
            $row[] = ($r->jatuh_tempo);
            $row[] = ($r->reminder);
				$aksi       = '<div class="btn-group">';			
				$aksi 		.= '<button type="button" class="btn btn-xs btn-success edit"><i class="fa fa-pencil"></i></button>';				
				// $aksi 		.= '<button type="button" class="btn btn-xs btn-danger" onclick="hapus_setting('.$r->id.')" title="Hapus"><i class="fa fa-trash-o"></i></button>';				
			$aksi.='</div>';			
			$row[] = $aksi;			
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    }
	
	function simpan_setting(){
		$idtipe=$this->input->post('idtipe');
		$jatuh_tempo=$this->input->post('jatuh_tempo');
		$reminder=$this->input->post('reminder');
		$data=array(
			// 'setting_id'=>1,
			'jatuh_tempo'=>$jatuh_tempo,
			'reminder'=>$reminder,
		);
		$this->db->where('idtipe',$idtipe);
		$result = $this->db->update('msetting_piutang',$data);		
		
		$this->output->set_output(json_encode($result));
	}
	
}
