<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtindakan2icd extends CI_Controller {

	/**
	 * Tindakan to ICD 9 controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('mtindakan2icd_model');
  }

	function index(){
		
		$data = array(
			'id' 						=> '',
			'lokasi_tubuh_id' 					=> '#',
			'kelompok_operasi_id' 			=> '#',
			'status_aktif' 			=> '#',
			'status' 				=> '1'
		);
		$data['kelompok_operasi_list'] = $this->mtindakan2icd_model->kelompok_operasi_list();
		$data['error'] 			= '';
		$data['title'] 			= 'Tindakan to ICD 9';
		$data['content'] 		= 'Mtindakan2icd/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("Tindakan to ICD 9",'#'),
									    			array("List",'Mtindakan2icd')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create(){
		
		$data = array(
			'id' 						=> '',
			'nama' 					=> '',
			'kelompok_operasi_id' 			=> '#',
			'status' 				=> '1'
		);

		$data['lokasi_tubuh_list'] = $this->mtindakan2icd_model->lokasi_tubuh_list();
		$data['kelompok_operasi_list'] = $this->mtindakan2icd_model->kelompok_operasi_list();
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Tindakan to ICD 9';
		$data['content'] 		= 'Mtindakan2icd/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Tindakan to ICD 9",'#'),
									    			array("Tambah",'Mtindakan2icd')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function js_icd()
	{
		$arr = $this->mtindakan2icd_model->js_icd();
		echo json_encode($arr);
	}
	function js_tindakan()
	{
		$arr = $this->mtindakan2icd_model->js_tindakan();
		echo json_encode($arr);
	}
	function simpan_add()
	{
		$data=array(
			'tindakan_id'=>$this->input->post('id'),
			'icd9_id'=>$this->input->post('icd_id'),
			'jenis_id'=>$this->input->post('jenis_id')
			
		);
		
       // print_r($data);
		$result = $this->db->insert('mtindakan2icd_icd',$data);
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function simpan_edit()
	{
		$data=array(
			'tindakan_id'=>$this->input->post('id'),
			'icd9_id'=>$this->input->post('icd_id'),
			'jenis_id'=>$this->input->post('jenis_id')
			
		);
		$this->db->where('id',$this->input->post('tedit'));
		
       // print_r($data);
		$result = $this->db->update('mtindakan2icd_icd',$data);
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function hapus_icd()
	{
		$this->db->where('id',$this->input->post('tedit'));		
       // print_r($data);
		$result = $this->db->delete('mtindakan2icd_icd');
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function hapus_tarif()
	{
		$this->db->where('id',$this->input->post('id'));		
       // print_r($data);
		$result = $this->db->update('mtindakan2icd_layanan',array('status'=>'0'));
		if ($result) {
		  return true;
		}else{
			return false;
		}
	}
	function cek_duplicate()
	{
		$tipe_tarif=$this->input->post('tipe_tarif');
		$tarif_id=$this->input->post('tarif_id');
		$tindakan_id=$this->input->post('tindakan_id');
		$q="select *from mtindakan2icd_layanan where tipe_id='$tipe_tarif' AND tarif_id='$tarif_id' AND status='1'";
		$query=$this->db->query($q);
		$result=$query->row_array();
		if ($result){
			$result=false;
			echo json_encode($result);
		}else{
			$data=array(
			'tindakan_id'=>$this->input->post('tindakan_id'),
			'tipe_id'=>$this->input->post('tipe_tarif'),
			'tarif_id'=>$this->input->post('tarif_id'),
			'nama_tarif'=>$this->input->post('nama_tarif'),
			
			);
			
		   // print_r($data);
			$result = $this->db->insert('mtindakan2icd_layanan',$data);
			if ($result) {
				echo json_encode($result);
			}else{
				echo json_encode($result);
			}
		}
	}
	function load_icd()
    {
		
		$id     = $this->input->post('id');
		
		$iduser=$this->session->userdata('user_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT *FROM (
				SELECT H.id,I.deskripsi,I.kode,H.icd9_id,H.jenis_id,
				CASE WHEN H.jenis_id='1' THEN 'Primary' ELSE 'Secondary' END as jenis from mtindakan2icd_icd H
				INNER JOIN icd_9 I ON I.id=H.icd9_id 
				WHERE H.tindakan_id='$id' ORDER BY H.jenis_id,H.id ASC
				) as T1) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('jenis','deskripsi','kode');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->kode.'-'.$r->deskripsi;
            $row[] = $r->jenis;
            $aksi       = '<div class="btn-group">';	
			
			$aksi 		.= '<button class="btn btn-xs btn-danger hapus" title="Hapus"><i class="fa fa-close"></i></button>';
				
			
			$aksi.='</div>';
			
			$row[] = $aksi;			
            $row[] = $r->id;
            $row[] = $r->icd9_id;
            $row[] = $r->jenis_id;
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	function load_tarif()
    {
		
		$id     = $this->input->post('id');
		
		$iduser=$this->session->userdata('user_id');
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT H.id,V.tipe,V.nama_pelayanan,H.tipe_id,H.tarif_id from mtindakan2icd_layanan H
					LEFT JOIN view_tarif_tindakan2icd V ON V.tipe_id=H.tipe_id AND V.tarif_id=H.tarif_id

					WHERE H.`status`='1' AND H.tindakan_id='$id' ORDER BY H.id ASC
				) as tbl";
			
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('nama_pelayanan','tipe');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $r->tipe;
            $row[] = $r->nama_pelayanan;
            $aksi       = '<div class="btn-group">';	
			
			$aksi 		.= '<button class="btn btn-xs btn-danger hapus_tarif" title="Hapus"><i class="fa fa-close"></i></button>';				
			
			$aksi.='</div>';			
			$row[] = $aksi;			
            $row[] = $r->id;
            $row[] = $r->tipe_id;
            $row[] = $r->tarif_id;
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
    }
	function update($id){
		
		if($id != ''){
			$row = $this->mtindakan2icd_model->getSpecified($id);
			if(isset($row->id)){
				$data = array(
					'id' 						=> $row->id,
					'nama' 					=> $row->nama,
					'kelompok_operasi_id' 					=> $row->kelompok_operasi_id,
					'status' 				=> $row->status
				);
				$data['lokasi_tubuh_list'] = $this->mtindakan2icd_model->lokasi_tubuh_list();
				$data['kelompok_operasi_list'] = $this->mtindakan2icd_model->kelompok_operasi_list();
				$data['error'] 			= '';
				$data['title'] 			= 'Ubah Tindakan to ICD 9';
				$data['content']	 	= 'Mtindakan2icd/manage';
				$data['breadcrum'] 	= array(
																array("RSKB Halmahera",'#'),
																array("Tindakan to ICD 9",'#'),
											    			array("Ubah",'mtindakan2icd')
															);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','data tidak ditemukan.');
				redirect('mtindakan2icd','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','data tidak ditemukan.');
			redirect('mtindakan2icd');
		}
	}

	function delete($id){
		// print_r($id);exit();
		$this->mtindakan2icd_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('mtindakan2icd','location');
	}

	function save(){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				$id=$this->mtindakan2icd_model->saveData();
				if($id){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtindakan2icd/update/'.$id,'location');
				}
			} else {
				if($this->mtindakan2icd_model->updateData()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					redirect('mtindakan2icd/update/'.$this->input->post('id'),'location');
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}
	}

	function failed_save($id){
		$data = $this->input->post();
		$data['error'] 		= validation_errors();
		$data['content'] 	= 'Mtindakan2icd/manage';

		if($id==''){
			$data['title'] = 'Tambah Tindakan to ICD 9';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Tindakan to ICD 9",'#'),
															array("Tambah",'Mtindakan2icd')
													);
		}else{
			$data['title'] = 'Ubah Tindakan to ICD 9';
			$data['breadcrum'] = array(
															array("RSKB Halmahera",'#'),
															array("Tindakan to ICD 9",'#'),
															array("Ubah",'Mtindakan2icd')
													);
		}

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template',$data);
	}

	function getIndex()
  {
		$status_aktif     = $this->input->post('status_aktif');
		$kelompok_operasi_id     = $this->input->post('kelompok_operasi_id');
		
		$iduser=$this->session->userdata('user_id');
		
		$where='';
		if ($status_aktif !='#'){
			if ($status_aktif=='1') {
				$where .=" AND iddet IS NOT NULL";				
			}else{
				$where .=" AND iddet IS NULL";					
			}
		}
		if ($kelompok_operasi_id !='#'){			
			$where .=" AND kelompok_operasi_id ='$kelompok_operasi_id'";					
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		$from="(SELECT *FROM (SELECT *FROM (SELECT H.id,H.nama,
					H.kelompok_operasi_id,
					K.nama as kel_operasi,H.status,GROUP_CONCAT(D.id)  as iddet FROM mtindakan2icd H
					LEFT JOIN mkelompok_operasi K ON K.id=H.kelompok_operasi_id
					LEFT JOIN mtindakan2icd_icd D ON D.tindakan_id=H.id
					WHERE H.`status`='1'
					GROUP BY H.id
					ORDER BY H.id DESC) as T1
					WHERE id <> '' ".$where."
					) as T1) as tbl";
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('nama','kel_operasi');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
			
            $row[] = $no;
          $row[] = $r->nama;
          $row[] = $r->kel_operasi;
		  $status='';
		  if ($r->iddet){
			  $status='<span class="label label-success">AKTIF</span>';
		  }else{
			$status='<span class="label label-danger">TIDAK AKTIF</span>';
		  }
          $row[] = $status;
          $aksi = '<div class="btn-group">';
          
		  if (UserAccesForm($user_acces_form,array('315'))){
          	$aksi .= '<a href="'.site_url().'mtindakan2icd/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
          }
          
		  if (UserAccesForm($user_acces_form,array('316'))){
          	$aksi .= '<a href="#" data-urlindex="'.site_url().'mtindakan2icd" data-urlremove="'.site_url().'mtindakan2icd/delete/'.$r->id.'" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>';
          }
          $aksi .= '</div>';
          $row[] = $aksi;
            $data[] = $row;
			
        }
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->datatable->count_all(),
          "recordsFiltered" => $this->datatable->count_all(),
          "data" => $data
        );
        echo json_encode($output);
				  
		
  }
	function ajaxSave(){
		if ($this->mtindakan2icd_model->saveData()) {
			$this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','data telah disimpan.');
			$data['response']="data telah di simpan";
			$data['code']=200;
			$data['id']=$this->db->insert_id();
			$data['nama']=$this->input->post("nama");
			echo json_encode($data);
		}
	}
}
