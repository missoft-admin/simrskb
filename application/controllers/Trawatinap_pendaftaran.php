<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use Dompdf\Dompdf;
use Dompdf\Options;

class Trawatinap_pendaftaran extends CI_Controller
{
	/**
	 * Rawat Inap Controller.
	 * Developer @RendyIchtiarSaputra + @GunaliRezqiMauludi
	 */
	public function __construct()
	{
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Trawatinap_pendaftaran_model');
		$this->load->model('Tpoliklinik_pendaftaran_model');
	}

	public function index()
	{
		$data = [
			'iddokter' => '#',
			'idrekanan' => '#',
			'nomedrec' => '',
			'idkelompokpasien' => '#',
			'namapasien' => '',
			'tanggaldaftar' => date('d/m/Y'),
			'tanggaldaftar2' => date('d/m/Y'),
			'idkelas' => '#',
			'idbed' => '#',
			'idstatus' => '#',
			'idalasan' => '7',
		];

		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form, ['361'])) {
			$data['idtipe'] = '1';
		}
		if (UserAccesForm($user_acces_form, ['362'])) {
			$data['idtipe'] = '2';
		}
		if (UserAccesForm($user_acces_form, ['1334'])) {
			$data['idtipe'] = '#';
		}
		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Rawat Inap';
		$data['content'] = 'Trawatinap_pendaftaran/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rawat Inap', '#'],
			['List', 'Trawatinap_pendaftaran'],
		];

		$data['list_dokter'] = $this->Trawatinap_pendaftaran_model->getDokter();
		$data['list_alasan'] = $this->Tpoliklinik_pendaftaran_model->getAlasan();
		$data['list_kelompok_pasien'] = $this->Trawatinap_pendaftaran_model->getKelompokPasien();
		$data['list_rekanan'] = $this->Trawatinap_pendaftaran_model->getRekanan();
		$data['list_kelas'] = $this->Trawatinap_pendaftaran_model->getKelas();
		$data['list_bed'] = $this->Trawatinap_pendaftaran_model->getIndexBed();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function filter()
	{
		$data = [
			'iddokter' => $this->input->post('iddokter'),
			'idrekanan' => $this->input->post('idrekanan'),
			'nomedrec' => $this->input->post('nomedrec'),
			'idkelompokpasien' => $this->input->post('idkelompokpasien'),
			'namapasien' => $this->input->post('namapasien'),
			'tanggaldaftar' => $this->input->post('tanggaldaftar'),
			'tanggaldaftar2' => $this->input->post('tanggaldaftar2'),
			'idkelas' => $this->input->post('idkelas'),
			'idbed' => $this->input->post('idbed'),
			'idstatus' => $this->input->post('idstatus'),
			'idalasan' => $this->input->post('idalasan'),
			'idtipe' => $this->input->post('idtipe'),
		];

		$this->session->set_userdata($data);

		$data['error'] = '';
		$data['title'] = 'Rawat Inap';
		$data['content'] = 'Trawatinap_pendaftaran/index';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rawat Inap', '#'],
			['List', 'Trawatinap_pendaftaran'],
		];

		$data['list_dokter'] = $this->Trawatinap_pendaftaran_model->getDokter();
		$data['list_alasan'] = $this->Tpoliklinik_pendaftaran_model->getAlasan();
		$data['list_kelompok_pasien'] = $this->Trawatinap_pendaftaran_model->getKelompokPasien();
		$data['list_rekanan'] = $this->Trawatinap_pendaftaran_model->getRekanan();
		$data['list_kelas'] = $this->Trawatinap_pendaftaran_model->getKelas();
		$data['list_bed'] = $this->Trawatinap_pendaftaran_model->getIndexBed();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function create()
	{
		$data = [
			'id' => '',
			'idpoliklinik' => '',
			'tanggaldaftar' => date('d/m/Y'),
			'waktudaftar' => date('H:i:s'),
			'nomedrec' => '',
			'namapasien' => '',
			'alamatpasien' => '',
			'tanggallahir' => '',
			'umur' => '',
			'rujukan' => '',
			'poliklinik' => '',
			'idtipepasien' => '',
			'idkelompokpasien' => '0',
			'idrekanan' => '',
			'idtarifbpjskesehatan' => '',
			'idtarifbpjstenagakerja' => '',
			'idjenispasien' => '',
			'idkelompokpasienCOB' => '',
			'idrekananCOB' => '',
			'idtarifbpjskesehatanCOB' => '',
			'idtarifbpjstenagakerjaCOB' => '',
			'dokterperujuk' => '',
			'iddokterpenanggungjawab' => '',
			'idruangan' => '',
			'idkelas' => '',
			'idbed' => '',
			'datapasien' => [
				'idpasien' => '',
				'title' => '',
				'namapasien' => '',
				'no_medrec' => '',
				'nohp' => '',
				'telepon' => '',
				'tempat_lahir' => '',
				'tanggal_lahir' => '',
				'alamatpasien' => '',
				'provinsi_id' => '',
				'kabupaten_id' => '',
				'kecamatan_id' => '',
				'kelurahan_id' => '',
				'kodepos' => '',
				'namapenanggungjawab' => '',
				'hubungan' => '',
				'umurhari' => '',
				'umurbulan' => '',
				'umurtahun' => '',
				'jenis_kelamin' => ''
			],
		];

		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form, ['1335'])) {
			$data['idtipe'] = '2';
		}
		if (UserAccesForm($user_acces_form, ['368'])) {
			$data['idtipe'] = '1';
		}

		$tanggal = date('Y-m-d', strtotime(' - 60 days'));

		$data['error'] = '';
		$data['title'] = 'Daftar Rawat Inap';
		$data['content'] = 'Trawatinap_pendaftaran/manage';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Rawat Inap', '#'],
			['List', 'Trawatinap_pendaftaran'],
		];

		$data['stgl1'] = DMYFormat($tanggal);
		$data['stgl2'] = date('d/m/Y');

		$data['list_dokter'] = $this->Trawatinap_pendaftaran_model->getDokter();

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function update($id)
	{
		if ($id != '') {
			$row = $this->Trawatinap_pendaftaran_model->getSpecified($id);
			$pasien = get_by_field('id', $row->idpasien, 'mfpasien');

			if (isset($row->id)) {
				$data = [
					'id' => $row->id,
					'idtipe' => $row->idtipe,
					'idpoliklinik' => $row->idpoliklinik,
					'tanggaldaftar' => DMYFormat($row->tanggaldaftar),
					'waktudaftar' => HISTimeFormat($row->tanggaldaftar),
					'idpasien' => $pasien->id,
					'nomedrec' => $pasien->no_medrec,
					'namapasien' => $pasien->nama,
					'alamatpasien' => $pasien->alamat_jalan,
					'tanggallahir' => $pasien->tanggal_lahir,
					'umur' => $pasien->umur_tahun . ' Th ' . $pasien->umur_bulan . ' Bln ' . $pasien->umur_hari . ' Hr',
					'rujukan' => ($row->idtipe == 1 ? 'Poliklinik' : 'Instalasi Gawat Darurat (IGD)'),
					'poliklinik' => $row->poliklinik,
					'idtipepasien' => $row->idtipepasien,
					'idkelompokpasien' => $row->idkelompokpasien,
					'idrekanan' => $row->idrekanan,
					'idtarifbpjskesehatan' => $row->idtarifbpjskesehatan,
					'idtarifbpjstenagakerja' => $row->idtarifbpjstenagakerja,
					'idjenispasien' => $row->idjenispasien,
					'idkelompokpasienCOB' => $row->idkelompokpasien2,
					'idrekananCOB' => $row->idrekanan2,
					'idtarifbpjskesehatanCOB' => $row->idtarifbpjskesehatan2,
					'idtarifbpjstenagakerjaCOB' => $row->idtarifbpjstenagakerja2,
					'dokterperujuk' => $row->dokterperujuk,
					'iddokterpenanggungjawab' => $row->iddokterpenanggungjawab,
					'idruangan' => $row->idruangan,
					'idkelas' => $row->idkelas,
					'idbed' => $row->idbed,
					'datapasien' => json_encode([
						'idpasien' => $pasien->id,
						'title' => $pasien->title,
						'namapasien' => $pasien->nama,
						'no_medrec' => $pasien->no_medrec,
						'nohp' => $pasien->hp,
						'telepon' => $pasien->telepon,
						'tempat_lahir' => $pasien->tempat_lahir,
						'tanggal_lahir' => $pasien->tanggal_lahir,
						'alamatpasien' => $pasien->alamat_jalan,
						'provinsi_id' => $pasien->provinsi_id,
						'kabupaten_id' => $pasien->kabupaten_id,
						'kecamatan_id' => $pasien->kecamatan_id,
						'kelurahan_id' => $pasien->kelurahan_id,
						'kodepos' => $pasien->kodepos,
						'namapenanggungjawab' => $row->namapenanggungjawab,
						'hubungan' => $row->hubungan,
						'umurhari' => $pasien->umur_hari,
						'umurbulan' => $pasien->umur_bulan,
						'umurtahun' => $pasien->umur_tahun,
						'jenis_kelamin' => $pasien->jenis_kelamin
					]),
				];

				$data['error'] = '';
				$data['title'] = 'Daftar Rawat Inap';
				$data['content'] = 'Trawatinap_pendaftaran/manage';
				$data['breadcrum'] = [
					['RSKB Halmahera', '#'],
					['Daftar Rawat Inap', '#'],
					['Ubah', 'Trawatinap_pendaftaran'],
				];

				$string = $pasien->tanggal_lahir;
				$timestamp = strtotime($string);
				$data['tgl_hari'] = date('d', $timestamp);
				$data['tgl_bulan'] = date('m', $timestamp);
				$data['tgl_tahun'] = date('Y', $timestamp);

				$data['list_bed'] = $this->Trawatinap_pendaftaran_model->getIndexBed();
				$data['historykunjungan'] = $this->Tpoliklinik_pendaftaran_model->getHistoryKunjungan($row->idpasien);

				$data = array_merge($data, backend_info());
				$this->parser->parse('module_template', $data);
			} else {
				$this->session->set_flashdata('error', true);
				$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
				redirect('tpoliklinik_pendaftaran/index', 'location');
			}
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('message_flash', 'data tidak ditemukan.');
			redirect('tpoliklinik_pendaftaran/index');
		}
	}

	public function save()
	{
		$id = $this->input->post('id');
		if ($id == '') {
			if ($this->Trawatinap_pendaftaran_model->saveData()) {
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect('trawatinap_pendaftaran/index', 'location');
			}
		} else {
			if ($this->Trawatinap_pendaftaran_model->updateData($id)) {
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah diupdate.');
				redirect('trawatinap_pendaftaran/index', 'location');
			}
		}
	}

	public function delete()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		$idalasan = $this->input->post('idalasan');

		$data = [
			'idalasan' => $idalasan,
			'status' => 0,
			'deleted_by' => $this->session->userdata('user_id'),
			'deleted_date' => date('Y-m-d H:i:s'),
		];

		$this->db->where('id', $idpendaftaran);
		if ($this->db->update('trawatinap_pendaftaran', $data)) {
			return true;
		} else {
			$this->error_message = 'Penyimpanan Gagal';
			return false;
		}
	}

	public function printForm($id)
	{
		$data = [
			'start_awal' => 1,
			'jml' => 10,
			'idpendaftaran' => $id,
			'tab' => 0,
		];
		if ($this->session->userdata('session_print_sticker')) {
			$data['margin'] = $this->session->userdata('session_print_sticker');
		} else {
			$data['margin'] = 'tengah';
		}
		// print_r($data['margin']);exit();
		$data['error'] = '';
		$data['title'] = 'Print E-Ticket';
		$data['content'] = 'Trawatinap_pendaftaran/print';
		$data['breadcrum'] = [
			['RSKB Halmahera', '#'],
			['Farmasi', 'tpasien_penjualan'],
			['Print', 'tpasien_penjualan/print'],
		];

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function printEticket()
	{
		$idpendaftaran = $this->input->post('idpendaftaran');
		$row = $this->Trawatinap_pendaftaran_model->getPrintInfo($idpendaftaran);

		$data = [];
		$data = [
			'nomedrec' => $row->nomedrec,
			'namapasien' => $row->namapasien,
			'title' => $row->title,
			'jeniskelamin' => ($row->jeniskelamin == 1 ? '(L)' : '(P)'),
			'namakelompok' => ($row->idkelompokpasien == 1 ? $row->namarekanan : $row->namakelompok),
			'tanggallahir' => $row->tanggallahir,
			'umurtahun' => $row->umurtahun,
			'umurbulan' => $row->umurbulan,
			'umurhari' => $row->umurhari,
			'tanggaldaftar' => $row->tanggaldaftar,
			'namakelas' => $row->namakelas,
			'namabed' => $row->namabed,
			'namadokter' => $row->namadokter,
			'namarekanan' => $row->namarekanan,
		];

		$data_margin = ['session_print_sticker' => $this->input->post('margin')];
		$this->session->set_userdata($data_margin);

		$data['margin'] = $this->input->post('margin');
		$data['start_awal'] = $this->input->post('start_awal');
		$data['jml'] = $this->input->post('jml');
		$data['start_akhir'] = $this->input->post('jml') + $this->input->post('start_awal') - 1;

		$data_array = [];

		$data_akhir = cek_akhir_row($data['start_akhir']);

		for ($i = 1; $i <= $data_akhir; $i++) {
			if ($i < $data['start_awal']) {
				$data_array[$i] = '0';
			} else {
				if ($i > $data['start_akhir']) {
					$data_array[$i] = '0';
				} else {
					$data_array[$i] = '1';
				}
			}
		}
		$data['data_array'] = $data_array;
		$data['data_akhir'] = $data_akhir;
		$options = new Options();
		$options->set('isRemoteEnabled', true);
		$dompdf = new Dompdf($options);

		$html = $this->parser->parse('Trawatinap_pendaftaran/print/print_eticket', array_merge($data, backend_info()), true);
		$html = $this->parser->parse_string($html, $data);
		// print_r($html);exit();
		$dompdf->loadHtml($html);

		// $customPaper = array(0, 0, 462, 600);
		// $dompdf->set_paper($customPaper, 'portrait');
		$dompdf->setPaper('A4', 'portrait');
		$dompdf->render();
		$dompdf->stream('E-Ticket.pdf', ['Attachment' => 0]);
	}

	public function printDiagnosa($idpendaftaran, $idtipe)
	{
		$dompdf = new Dompdf();

		$row = $this->Trawatinap_pendaftaran_model->getPrintInfo($idpendaftaran);
		$data = [
			'nomedrec' => $row->nomedrec,
			'namapasien' => $row->namapasien,
			'jeniskelamin' => ($row->jeniskelamin == 1 ? 'L' : 'P'),
			'namakelompok' => ($row->idkelompokpasien == 1 ? $row->namarekanan : $row->namakelompok),
			'tanggallahir' => $row->tanggallahir,
			'umurtahun' => $row->umurtahun,
			'umurbulan' => $row->umurbulan,
			'tanggaldaftar' => HumanDateShort($row->tanggaldaftar),
			'tanggalcheckout' => HumanDateShort($row->tanggalcheckout),
			'namakelas' => $row->namakelas,
			'namabed' => $row->namabed,
			'namadokter' => $row->namadokter,
			'namarekanan' => $row->namarekanan,
			'pekerjaan' => $row->namapekerjaan,
			'kab' => $row->kab,
			'kec' => $row->kec,
			'desa' => $row->desa,
			'alamat' => $row->alamat,
			'idtipe' => $row->idtipe,
		];

		if ($idtipe == 1) {
			$html = $this->load->view('Trawatinap_pendaftaran/print/print_diagnosa', $data, true);
		}

		$dompdf->loadHtml($html);
		if ($idtipe == 1) {
			$dompdf->setPaper('A4', 'portrait');
		}
		$dompdf->render();
		$dompdf->stream('Cetak Faktur.pdf', ['Attachment' => 0]);
	}

	public function printDaftarPasien()
	{
		$dompdf = new Dompdf();

		$data = [];
		$data['report'] = $this->Trawatinap_pendaftaran_model->getPrintInfoAll();

		$html = $this->load->view('Trawatinap_pendaftaran/print/print_daftar_pasien', $data, true);

		$dompdf->loadHtml($html);
		$dompdf->setPaper('A4', 'landscape');
		$dompdf->render();
		$dompdf->stream('Informasi Pasien Rawat Inap.pdf', ['Attachment' => 0]);
	}

	public function getListPasien()
	{
		$this->select = ['mfpasien.*',
			'tpoliklinik_pendaftaran.id AS idpendaftaran',
			'tpoliklinik_pendaftaran.nopendaftaran',
			'tpoliklinik_pendaftaran.rencana',
			'tpoliklinik_pendaftaran.tanggaldaftar',
			'tpoliklinik_pendaftaran.idtipe',
			'mdokter.nama as namadokter',
			'tpoliklinik_pendaftaran.id As idpendaftaran',
			'tkamaroperasi_pendaftaran.tanggaloperasi', 'tkamaroperasi_pendaftaran.waktumulaioperasi',
			'tkamaroperasi_pendaftaran.waktuselesaioperasi', 'tkamaroperasi_pendaftaran.diagnosa',
			'tkamaroperasi_pendaftaran.operasi', 'tkamaroperasi_pendaftaran.namapetugas', 'tkamaroperasi_pendaftaran.catatan
		    '];
		$this->join = [
			['tpoliklinik_tindakan', 'tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id', 'LEFT'],
			['mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT'],
			['mdokter', 'mdokter.id = tpoliklinik_pendaftaran.iddokter', 'LEFT'],
			['tkamaroperasi_pendaftaran', 'tkamaroperasi_pendaftaran.idpendaftaran = tpoliklinik_pendaftaran.id AND tkamaroperasi_pendaftaran.idasalpendaftaran = 1', 'LEFT'],
			['trawatinap_pendaftaran', 'trawatinap_pendaftaran.idpoliklinik = tpoliklinik_pendaftaran.id AND trawatinap_pendaftaran.status = 1', 'LEFT'],
			['tkasir', 'tkasir.idtipe IN (1,2) AND tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.status <> 2', 'LEFT'],
		];

		$this->where = [
			'tpoliklinik_pendaftaran.status' => 1
		];

		if (isset($_POST['snamapasien']) && $_POST['snamapasien'] != '') {
			$this->where = array_merge($this->where, ['mfpasien.nama LIKE' => '%' . $_POST['snamapasien'] . '%']);
		}
		if (isset($_POST['sasalpasien']) && $_POST['sasalpasien'] != '' && $_POST['sasalpasien'] != '#') {
			$this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.idtipe' => $_POST['sasalpasien']]);
		}
		if (isset($_POST['srencana']) && $_POST['srencana'] != '' && $_POST['srencana'] != '#') {
			$this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.rencana' => $_POST['srencana']]);
		}
		if (isset($_POST['sdokter']) && $_POST['sdokter'] != '' && $_POST['sdokter'] != '#') {
			$this->where = array_merge($this->where, ['tpoliklinik_pendaftaran.iddokter' => $_POST['sdokter']]);
		}

		if (isset($_POST['stgl1']) && $_POST['stgl1'] != '' && isset($_POST['stgl2']) && $_POST['stgl2'] != '') {
			$this->where = array_merge($this->where, ['DATE(tpoliklinik_pendaftaran.tanggaldaftar) >=' => YMDFormat($_POST['stgl1'])]);
			$this->where = array_merge($this->where, ['DATE(tpoliklinik_pendaftaran.tanggaldaftar) <=' => YMDFormat($_POST['stgl2'])]);
		}

		$this->where = array_merge($this->where, ['(trawatinap_pendaftaran.id IS NULL OR trawatinap_pendaftaran.status != 1)' => null]);

		$this->order = [
			'tpoliklinik_pendaftaran.id' => 'DESC',
		];

		$this->group = ['tpoliklinik_pendaftaran.id'];
		$this->from = 'tpoliklinik_pendaftaran';

		$this->column_search = ['tpoliklinik_pendaftaran.tanggaldaftar', 'tpoliklinik_pendaftaran.nopendaftaran', 'mfpasien.no_medrec', 'mfpasien.nama', 'mfpasien.alamat_jalan', 'tpoliklinik_pendaftaran.idtipe'];
		$this->column_order = ['tpoliklinik_pendaftaran.tanggaldaftar', 'tpoliklinik_pendaftaran.nopendaftaran', 'mfpasien.no_medrec', 'mfpasien.nama', 'mfpasien.alamat_jalan', 'tpoliklinik_pendaftaran.idtipe'];

		$list = $this->datatable->get_datatables();

		// print_r($this->db->last_query());exit();

		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];

			$row[] = "<a href='#' class='selectPasien' data-idpoliklinik='" . $r->idpendaftaran . "' data-dismiss='modal'>" . HumanDateLong($r->tanggaldaftar) . '</a>';
			$row[] = $r->no_medrec;
			$row[] = $r->nama;
			$row[] = $r->namadokter;
			$row[] = '<span class="label label-success">' . GetAsalRujukan($r->idtipe) . '</span>';
			$row[] = GetStatusRencana($r->rencana);
			$row[] = $r->diagnosa;
			$row[] = $r->operasi;
			$row[] = HumanDateShort_exp($r->tanggaloperasi) . '<br>' . $r->waktumulaioperasi;
			$row[] = $r->namapetugas;
			$row[] = $r->catatan;

			$data[] = $row;
		}

		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data,
		];
		echo json_encode($output);
	}

	public function getIndex($uri = '')
	{
		$data_user = get_acces();
		$user_acces_form = $data_user['user_acces_form'];

		$this->select = ['trawatinap_pendaftaran.id',
			'trawatinap_pendaftaran.*',
			'mfpasien.no_medrec AS nomedrec',
			'mfpasien.nama AS namapasien',
			'mfpasien.alamat_jalan AS alamat',
			'mkelas.nama AS namakelas',
			'mbed.nama AS namabed',
			'mdokterpenanggungjawab.nama AS namadokter', 'tkamaroperasi_pendaftaran.status as status_ods',
		];

		$this->from = 'trawatinap_pendaftaran';

		$this->join = [
			['tpoliklinik_pendaftaran', 'tpoliklinik_pendaftaran.id = trawatinap_pendaftaran.idpoliklinik', 'LEFT'],
			['tkamaroperasi_pendaftaran', 'trawatinap_pendaftaran.id = tkamaroperasi_pendaftaran.idpendaftaran AND tkamaroperasi_pendaftaran.idasalpendaftaran="2"', 'LEFT'],
			['mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT'],
			['mbed', 'mbed.id = trawatinap_pendaftaran.idbed', 'LEFT'],
			['mkelas', 'mkelas.id = trawatinap_pendaftaran.idkelas', 'LEFT'],
			['mdokter mdokterpenanggungjawab', 'mdokterpenanggungjawab.id = trawatinap_pendaftaran.iddokterpenanggungjawab', 'LEFT'],
		];

		$this->where = [];
		if ($this->session->userdata('nomedrec') != '') {
			$this->where = array_merge($this->where, ['mfpasien.no_medrec' => $this->session->userdata('nomedrec')]);
		}
		if ($this->session->userdata('namapasien') != '') {
			$this->where = array_merge($this->where, ['mfpasien.nama LIKE' => '%' . $this->session->userdata('namapasien') . '%']);
		}
		if ($this->session->userdata('idtipe') != '#' && $this->session->userdata('idtipe') != '') {
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idtipe' => $this->session->userdata('idtipe')]);
		}
		if ($this->session->userdata('idstatus') != '#') {
			switch ($this->session->userdata('idstatus')) {
				case '10':
					//RANAP
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idtipe' => '1']);
					break;
				case '11':
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idtipe' => '1']);
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.statuscheckout' => '0']);
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.status' => '1']);
					break;
				case '12':
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idtipe' => '1']);
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.statuskasir' => '1']);
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.statuscheckout' => '0']);
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.status' => '1']);
					break;
				case '13':
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idtipe' => '1']);
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.statuscheckout' => '1']);
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.status' => '1']);
					break;
				case '14':
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idtipe' => '1']);
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.status' => '0']);
					break;
				case '20': //ODS ALL
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idtipe' => '2']);
					break;
				case '21':
					//ODS MENUNGU TINDAKAN
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idtipe' => '2']);
					$this->where = array_merge($this->where, ['tkamaroperasi_pendaftaran.status' => null]);
					break;
				case '22':
					//ODS SUDAH DITINDAK
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idtipe' => '2']);
					$this->where = array_merge($this->where, ['tkamaroperasi_pendaftaran.status' => '4']);
					break;
				case '23':
					//ODS BATAL
					$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idtipe' => '2']);
					$this->where = array_merge($this->where, ['tkamaroperasi_pendaftaran.status' => '0']);
					break;
				default:
			}
		}
		if ($this->session->userdata('iddokter') != '#' && $this->session->userdata('iddokter') != '') {
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.iddokterpenanggungjawab' => $this->session->userdata('iddokter')]);
		}
		if ($this->session->userdata('idkelompokpasien') != '#') {
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idkelompokpasien' => $this->session->userdata('idkelompokpasien')]);
		}
		if ($this->session->userdata('idrekanan') != '#') {
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idrekanan' => $this->session->userdata('idrekanan')]);
		}
		if ($this->session->userdata('tanggaldaftar') != '' && $this->session->userdata('tanggaldaftar2') != '') {
			$this->where = array_merge($this->where, ['DATE(trawatinap_pendaftaran.tanggaldaftar) >=' => YMDFormat($this->session->userdata('tanggaldaftar'))]);
			$this->where = array_merge($this->where, ['DATE(trawatinap_pendaftaran.tanggaldaftar) <=' => YMDFormat($this->session->userdata('tanggaldaftar2'))]);
		}
		if ($this->session->userdata('idkelas') != '#' && $this->session->userdata('idkelas') != '') {
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idkelas' => $this->session->userdata('idkelas')]);
		}
		if ($this->session->userdata('idbed') != '#' && $this->session->userdata('idbed') != '') {
			$this->where = array_merge($this->where, ['trawatinap_pendaftaran.idbed' => $this->session->userdata('idbed')]);
		}

		$this->order = [
			'trawatinap_pendaftaran.id' => 'DESC',
			'trawatinap_pendaftaran.statuscheckout' => '0',
		];

		$this->group = ['trawatinap_pendaftaran.id'];
		$this->column_search = ['trawatinap_pendaftaran.tanggaldaftar', 'trawatinap_pendaftaran.nopendaftaran', 'mfpasien.no_medrec', 'mfpasien.nama', 'mkelas.nama', 'mbed.nama', 'mdokterpenanggungjawab.nama'];
		$this->column_order = ['trawatinap_pendaftaran.tanggaldaftar', 'trawatinap_pendaftaran.nopendaftaran', 'mfpasien.no_medrec', 'mfpasien.nama', 'mkelas.nama', 'mbed.nama', 'mdokterpenanggungjawab.nama'];

		$list = $this->datatable->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = [];
			$tujuan = ($r->id == '1' ? '3' : '4');
			if ($r->status == '0') {
				$action = '';
			} else {
				$action = '';
				$action .= '<div class="btn-group">
                     <div class="btn-group dropup">
                			  <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
            				      <span class="fa fa-print"></span>
                			  </button>
                       <ul class="dropdown-menu">
                          <li>';
				$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trm_layanan_berkas/print_document/' . $r->id . '/' . $tujuan . '/' . $r->id . '">Pinjam Berkas Pelayanan</a>';
				if (UserAccesForm($user_acces_form, ['365', '366'])) {
					if (UserAccesForm($user_acces_form, ['365'])) {
						$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trawatinap_pendaftaran/printForm/' . $r->id . '">Sticker ID</a>';
					}

					if (UserAccesForm($user_acces_form, ['366'])) {
						$action .= '<a tabindex="-1" target="_blank" href="' . site_url() . 'trawatinap_pendaftaran/printDiagnosa/' . $r->id . '/1">SK Diagnosa</a>';
					}
				}

				$action .= '</li>
                             </ul>
                         </div>
                      </div>';

				if (UserAccesForm($user_acces_form, ['363'])) {
					$action .= '<a href="' . site_url() . 'trawatinap_pendaftaran/update/' . $r->id . '" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form, ['364'])) {
					$action .= '<button class="btn btn-danger btn-sm delete" title="Hapus" onclick="deleteTranscation(' . $r->id . ')"><i class="fa fa-trash-o"></i></button>';
				}
			}

			$row[] = $no;
			$row[] = "<span data-idpendaftaran='" . $r->id . "'>" . HumanDateLong($r->tanggaldaftar) . '</span>';
			$row[] = $r->nopendaftaran;
			$row[] = $r->nomedrec;
			$row[] = $r->namapasien;
			$row[] = $r->alamat;
			$row[] = ($r->namakelas ? $r->namakelas : '-');
			$row[] = ($r->namabed ? $r->namabed : '-');
			$row[] = $r->namadokter;
			$row[] = GetTipeRujukanRawatInap($r->idtipe);

			if ($r->status == '0') {
				$row[] = StatusOK($r->status);
			} else {
				if ($r->idtipe == '1') {
					if ($r->statuscheckout == '1') {
						//PULANG
						$row[] = StatusRawatInap('13');
					} elseif ($r->statuskasir == '1') {
						//PEMBAYARAN
						$row[] = StatusRawatInap('12');
					} else {
						//DIRAWAT
						$row[] = StatusRawatInap('11');
					}
				} else {
					if ($r->status_ods == '0') {
						$row[] = StatusRawatInap('23');
					} elseif ($r->status_ods == '4') {
						$row[] = StatusRawatInap('22');
					} else {
						$row[] = StatusRawatInap('21');
					}
				}
			}

			$row[] = '<div class="btn-group btn-xs">' . $action . '</div>';
			$data[] = $row;
		}
		$output = [
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->datatable->count_all(),
			'recordsFiltered' => $this->datatable->count_all(),
			'data' => $data,
		];
		echo json_encode($output);
	}

	public function getPasienPoliklinik($idpendaftaran)
	{
		$this->db->select('tpoliklinik_pendaftaran.*,
        mfpasien.id AS idpasien, mfpasien.nama AS namapasien, mfpasien.jenis_kelamin,
        mdokter.id AS iddokter, mdokter.nama AS namadokter,
        mpoliklinik.nama AS namapoliklinik,DATE_FORMAT(tpoliklinik_pendaftaran.tanggal_lahir,"%d-%m-%Y") as tgllahir');
		$this->db->join('mfpasien', 'mfpasien.id = tpoliklinik_pendaftaran.idpasien', 'LEFT');
		$this->db->join('mdokter', 'mdokter.id = tpoliklinik_pendaftaran.iddokter', 'LEFT');
		$this->db->join('mpoliklinik', 'mpoliklinik.id = tpoliklinik_pendaftaran.idpoliklinik', 'LEFT');
		$this->db->where('tpoliklinik_pendaftaran.id', $idpendaftaran);
		$query = $this->db->get('tpoliklinik_pendaftaran');
		$this->output->set_output(json_encode($query->row()));
	}

	public function getHistory($id)
	{
		$query = $this->Trawatinap_pendaftaran_model->getHistory($id);
		$this->output->set_output(json_encode($query));
	}

	public function getRekanan()
	{
		$data = '{"rekanan":[';
		foreach (get_all('mrekanan') as $r) {
			$data .= '{"id":"' . $r->id . '","nama":"' . $r->nama . '"},';
		}
		$data = substr($data, 0, strlen($data) - 1);
		$data .= ']}';
		echo $data;
	}

	public function getDokter()
	{
		$idpoli = $this->input->post('idpoli');
		$data = '{"dokter":[';
		foreach (get_all('mpoliklinik_dokter', ['idpoliklinik' => $idpoli]) as $r) {
			foreach (get_all('mdokter', ['id' => $r->idpoliklinik]) as $s) {
				$data .= '{"id":"' . $s->id . '","nama":"' . $s->nama . '"},';
			}
		}
		$data = substr($data, 0, strlen($data) - 1);
		$data .= ']}';
		echo $data;
	}

	public function getBpjsKesehatan()
	{
		$data = '{"bpjskesehatan":[';
		foreach (get_all('mtarif_bpjskesehatan') as $r) {
			$data .= '{"id":"' . $r->id . '","kode":"' . $r->kode . '"},';
		}
		$data = substr($data, 0, strlen($data) - 1);
		$data .= ']}';
		echo $data;
	}

	public function getBpjsTenagakerja()
	{
		$data = '{"bpjstenaga":[';
		foreach (get_all('mtarif_bpjstenagakerja') as $r) {
			$data .= '{"id":"' . $r->id . '","nama":"' . $r->nama . '"},';
		}
		$data = substr($data, 0, strlen($data) - 1);
		$data .= ']}';
		echo $data;
	}

	public function getKelompok()
	{
		$kelompok = $this->input->post('kelompok');
		$data = '{"kelompok":[';
		$datas = $this->db->query("SELECT * FROM mpasien_kelompok WHERE id NOT IN('0',$kelompok)")->result();
		foreach ($datas as $r) {
			$data .= '{"id":"' . $r->id . '","nama":"' . $r->nama . '"},';
		}
		$data = substr($data, 0, strlen($data) - 1);
		$data .= ']}';
		echo $data;
	}

	public function getAllDokter()
	{
		$this->db->select('mpoliklinik_dokter.iddokter, mdokter.nama');
		$this->db->join('mdokter', 'mdokter.id = mpoliklinik_dokter.iddokter', 'LEFT');
		$this->db->join('mpoliklinik', 'mpoliklinik.id = mpoliklinik_dokter.idpoliklinik', 'LEFT');
		$this->db->group_by('mpoliklinik_dokter.iddokter');
		$query = $this->db->get('mpoliklinik_dokter');
		$this->output->set_output(json_encode($query->result()));
	}

	public function getDokterPoli()
	{
		$this->db->select('mpoliklinik_dokter.iddokter, mdokter.nama');
		$this->db->join('mdokter', 'mdokter.id = mpoliklinik_dokter.iddokter', 'LEFT');
		$this->db->join('mpoliklinik', 'mpoliklinik.id = mpoliklinik_dokter.idpoliklinik', 'LEFT');
		$this->db->group_by('mpoliklinik_dokter.iddokter');
		$this->db->where('mpoliklinik.idtipe', 1);
		$query = $this->db->get('mpoliklinik_dokter');
		$this->output->set_output(json_encode($query->result()));
	}

	public function getDokterIgd($iddokter)
	{
		$this->db->select('mpoliklinik_dokter.iddokter, mdokter.nama');
		$this->db->join('mdokter', 'mdokter.id = mpoliklinik_dokter.iddokter', 'LEFT');
		$this->db->join('mpoliklinik', 'mpoliklinik.id = mpoliklinik_dokter.idpoliklinik', 'LEFT');
		$this->db->group_by('mpoliklinik_dokter.iddokter');
		$this->db->where('mpoliklinik.idtipe', 2);
		$this->db->where_not_in('mpoliklinik_dokter.iddokter', 'SELECT iddokter FROM mpoliklinik_dokter WHERE iddokter = ' . $iddokter, false);
		$query = $this->db->get('mpoliklinik_dokter');
		$this->output->set_output(json_encode($query->result()));
	}

	public function getBed($idruangan, $idkelas, $id = '')
	{
		$result = $this->Trawatinap_pendaftaran_model->getBed($idruangan, $idkelas, $id);
		$this->output->set_output(json_encode($result));
	}

	public function getKelas()
	{
		$data = get_all('mkelas');
		$this->output->set_output(json_encode($data));
	}

	public function getStatusExistTransaction($idpasien, $idtipe)
	{
		$result = $this->Trawatinap_pendaftaran_model->getStatusExistTransaction($idpasien, $idtipe);
		$this->output->set_output($result);
	}

	public function getIndexBed()
	{
		$idkelas = $this->input->post('idkelas');
		$data = $this->Trawatinap_pendaftaran_model->getIndexBed($idkelas);
		$this->output->set_output(json_encode($data));
	}
}

/* End of file Trawatinap_pendaftaran.php */
/* Location: ./application/controllers/Trawatinap_pendaftaran.php */
