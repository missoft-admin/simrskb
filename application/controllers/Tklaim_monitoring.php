<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tklaim_monitoring extends CI_Controller {

	function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tklaim_monitoring_model','model');
	}

	function index() {
		$hari_ini = date("Y-m-d");
		$tgl_pertama = date('01-m-Y', strtotime($hari_ini));
		$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
		$data=array(
			'tipe'=>'#',
			'cara_bayar'=>'#',
			'no_po'=>'',
			'no_terima'=>'',
			'no_fakur'=>'',
			'status'=>'#',
			'jatuh_tempo_bayar'=>'',
			'jatuh_tempo_bayar2'=>'',
			'tgl_kirim'=>'',
			'tgl_kirim2'=>'',
			// 'tgl_kirim'=>$tgl_pertama,
			// 'tgl_kirim2'=>date('d-m-Y'),
		);
		$data['error'] 			= '';
		// $data['list_distributor'] 	= $this->model->list_distributor();
		$data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['list_bank'] 	= $this->model->list_bank();
		$data['title'] 			= 'Monitoring Tagihan';
		$data['content'] 		= 'Tklaim_monitoring/index';
		$data['breadcrum'] 		= array(
									array("RSKB Halmahera",'#'),
									array("Monitoring Tagihan",'tklaim_monitoring/index'),
									array("List",'#')
								);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function get_index(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$no_klaim=$this->input->post('no_klaim');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$idrekanan=$this->input->post('idrekanan');
		$tipe=$this->input->post('tipe');
		$status_lunas=$this->input->post('status_lunas');
		$umur=$this->input->post('umur');
		$status_pengiriman=$this->input->post('status_pengiriman');
		$jatuh_tempo_bayar2=$this->input->post('jatuh_tempo_bayar2');
		$jatuh_tempo_bayar=$this->input->post('jatuh_tempo_bayar');
		$tgl_kirim=$this->input->post('tgl_kirim');
		$tgl_kirim2=$this->input->post('tgl_kirim2');
		
		$where1='';
		$where2='';
		$where='';
		
		if ($no_klaim !=''){
			$where1 .=" AND TK.no_klaim LIKE '%".$no_klaim."%' ";
		}
		
		if ($idkelompokpasien !='#'){
			$where1 .=" AND TK.idkelompokpasien='$idkelompokpasien' ";
		}
		if ($idrekanan !='#'){
			$where1 .=" AND TK.idrekanan='$idrekanan' ";
		}
		if ($tipe !='#'){
			$where1 .=" AND TK.tipe='$tipe' ";
		}
		if ($status_lunas !='#'){
			if ($status_lunas=='0'){
				$where1 .=" AND TK.status_lunas='$status_lunas' ";
			}
			if ($status_lunas=='1'){
				$where1 .=" AND TK.status_lunas='1' AND TK.status_other_loss='0' ";
			}
			if ($status_lunas=='2'){
				$where1 .=" AND TK.status_lunas='1' AND TK.status_other_loss='1' ";
			}
			if ($status_lunas=='3'){//Perlu Folloup Ulang
				$where1 .=" AND TK.status_lunas='0' AND DATE(TK.tanggal_reminder) <='".date('Y-m-d')."'";
			}
			
		}
		if ($umur !=''){
				$where .=" WHERE umur <=".$umur."";
		}
		
		
		if ('' != $jatuh_tempo_bayar) {
            $where1 .= " AND DATE(TK.jatuh_tempo_bayar) >='".YMDFormat($jatuh_tempo_bayar)."' AND DATE(TK.jatuh_tempo_bayar) <='".YMDFormat($jatuh_tempo_bayar2)."'";
        }
		if ('' != $tgl_kirim) {
            $where1 .= " AND DATE(TK.tanggal_kirim) >='".YMDFormat($tgl_kirim)."' AND DATE(TK.tanggal_kirim) <='".YMDFormat($tgl_kirim)."'";
        }
		
        $from = "(
					SELECT TK.id,TK.no_klaim,TK.tipe,CASE WHEN TK.idkelompokpasien !='1' THEN kel.nama ELSE R.nama END as rekanan_nama,TK.idkelompokpasien
					,TK.idrekanan,TK.jml_faktur,TK.total_tagihan,(TK.total_tagihan - TK.total_bayar) as sisa,TK.tanggal_tagihan					
					,CASE WHEN TK.status_lunas=0 THEN ceil(DATEDIFF(CURRENT_DATE, TK.tanggal_kirim) / 30) ELSE ceil(DATEDIFF(TK.tanggal_pelunasan, TK.jatuh_tempo_bayar) / 30) END as umur					
					,TK.tanggal_kirim,TK.jatuh_tempo_bayar,TK.kirim_date,TK.noresi
					,TK.`status`,TK.status_kirim,TK.status_lunas,TK.last_fu,TK.user_fu,TK.last_koreksi,TK.user_koreksi,TK.status_other_loss,TK.total_bayar
					FROM tklaim TK
					LEFT JOIN mpasien_kelompok kel ON kel.id=TK.idkelompokpasien
					LEFT JOIN mrekanan R ON R.id=TK.idrekanan
					WHERE TK.`status` = 3 ".$where1."
					GROUP BY TK.id
					ORDER BY TK.tanggal_tagihan,TK.id DESC
				) as tbl ".$where." ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tklaim_monitoring/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->tipe;
            $row[] = $r->id;
            $row[] = $no;
            $row[] = $r->no_klaim;
            $row[] = $r->rekanan_nama;
            $row[] = ($r->tipe=='1' ? '<span class="label label-primary">RJ</span>':'<span class="label label-success">RI/ODS</span>');
            $row[] = $r->jml_faktur;
            $row[] = number_format($r->total_tagihan,2);
            $row[] = number_format($r->sisa,2);
            $row[] = HumanDateShort($r->tanggal_kirim);
            $row[] = HumanDateShort($r->jatuh_tempo_bayar);
            $row[] = ($r->umur);
			if ($r->status_lunas=='1'){
				if ($r->status_other_loss=='0'){
				$row[] = '<span class="label label-primary">LUNAS</span>';
				}else{
				$row[] = '<span class="label label-primary">LUNAS</span> <span class="label label-warning">OTHER LOSS</span>';
					
				}
			}else{
				$row[] = '<span class="label label-danger">BELUM LUNAS</span>';
			}
            $row[] = ($r->user_fu ? $r->user_fu.' - '.HumanDateShort($r->last_fu):'');
            $row[] = ($r->user_koreksi ? $r->user_koreksi.' - '.HumanDateShort($r->last_koreksi):'');

			
			$aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_tagihan/'.$r->id.'/'.$r->tipe.'/'.$r->status_kirim.'" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-th-list"></i></a>';
			if ($r->status_lunas=='0'){
				if ($r->total_bayar=='0'){
				// $aksi .= '<button title="Lunaskan" class="btn btn-xs btn-success lunas"><i class="fa fa-check"></i></button>';
				}
			$aksi .= '<button title="Follow Up"  class="btn btn-xs btn-danger followup"><i class="si si-arrow-right"></i></button>';
			}
			$aksi .= '<a title="Koreksi" href="'.$url.'koreksi_tagihan/'.$r->id.'/'.$r->tipe.'" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>';
			$aksi .= '<button disabled class="view btn btn-xs btn-success" type="button"  title="Detail"><i class="fa fa-print"></i></button>';
			
			$aksi.='</div>';
            $row[] = $aksi;           
            $row[] = $r->total_tagihan;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(true),
            "recordsFiltered" => $this->datatable->count_all(true),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_informasi(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$klaim_id=$this->input->post('klaim_id');
		
        $from = "(
					select *from tklaim_fu where klaim_id='$klaim_id' ORDER BY id DESC
				) as tbl ";
				
		
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('keterangan');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $row[] = $r->id;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggal_info);
            $row[] = $r->keterangan;
            $row[] = $r->created_user;
            $row[] = HumanDateShort($r->tanggal_reminder);
			$aksi .= '<button title="Hapus" class="btn btn-xs btn-danger fu_hapus"><i class="fa fa-trash"></i></button>';
           
			
			$aksi.='</div>';
            $row[] = $aksi;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_upload(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		$klaim_id=$this->input->post('klaim_id');
		
        $from = "(
					select *from tklaim_upload where klaim_id='$klaim_id' ORDER BY id DESC
				) as tbl ";
				
		
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('file_name');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $row[] = $r->id;
            $row[] = $no;
            $row[] = '<a href="'.base_url().'assets/upload/piutang/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a>';
            $row[] = $r->size;
			$aksi .= '<button title="Hapus" class="btn btn-xs btn-danger hapus_file"><i class="fa fa-trash"></i></button>';     
			
			$aksi.='</div>';
            $row[] = $aksi;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_rajal(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		
		$disabel=$this->input->post('disabel');
		$klaim_id=$this->input->post('klaim_id');
		$no_reg=$this->input->post('no_reg');
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$tgl_trx=($this->input->post('tgl_trx'));
		$tgl_trx2=($this->input->post('tgl_trx2'));
		
		$where1='';
		
		if ($no_medrec !=''){
			$where1 .=" AND TP.no_medrec='$no_medrec' ";
		}
		if ($nama_pasien !=''){
			$where1 .=" AND TP.namapasien='$nama_pasien' ";
		}
		if ($no_reg !=''){
			$where1 .=" AND TP.nopendaftaran='$no_reg' ";
		}
				
		if ($tgl_trx!='') {
            $where1 .= " AND DATE(TP.tanggaldaftar) >='".YMDFormat($tgl_trx)."' AND DATE(TP.tanggaldaftar) <='".YMDFormat($tgl_trx2)."'";
        }
		
        $from = "(
					SELECT TD.id,TD.pendaftaran_id,TD.kasir_id,TP.tanggaldaftar,TP.nopendaftaran,TP.no_medrec,CONCAT(TP.title,'. ',TP.namapasien) as namapasien 
					,M.nama as poli,D.nama as namadokter,K.total,TD.nominal as tot_asuransi
					,TH.idkelompokpasien,TH.idrekanan,TH.tanggal_tagihan,TH.tipe
					from tklaim_detail TD
					LEFT JOIN tklaim TH ON TH.id=TD.klaim_id
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TD.pendaftaran_id
					LEFT JOIN mpoliklinik M ON TP.idpoliklinik=M.id
					LEFT JOIN mdokter D ON D.id=TP.iddokter
					LEFT JOIN tkasir K ON K.id=TD.kasir_id
					WHERE TD.klaim_id='$klaim_id' ".$where1."
					GROUP BY TP.id

				) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('tklaim_rincian/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->id;
            $row[] = '';
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggaldaftar);
            $row[] = $r->nopendaftaran;
            $row[] = $r->no_medrec;
            $row[] = $r->namapasien;
            $row[] = $r->poli;
            $row[] = $r->namadokter;
           
            $row[] = number_format($r->total,0);
            $row[] = number_format($r->tot_asuransi,0);
            $row[] = number_format($r->total-$r->tot_asuransi,0);
			
			$aksi .= '<button title="Memajukan Tagihan" '.$disabel.' class="btn btn-xs btn-danger ganti"><i class="si si-arrow-right"></i></button>';
			$aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_kasir.'print_transaksi/'.$r->pendaftaran_id.'/'.$r->kasir_id.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-print"></i></a>';
			// $aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_tagihan/'.$r->id.'/'.$r->tipe.'" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></a>';
		
			
			
			$aksi.='</div>';
            $row[] = $aksi;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_koreksi_rajal(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		
		$disabel=$this->input->post('disabel');
		$klaim_id=$this->input->post('klaim_id');
		$no_reg=$this->input->post('no_reg');
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$tgl_trx=($this->input->post('tgl_trx'));
		$tgl_trx2=($this->input->post('tgl_trx2'));
		
		$where1='';
		
		if ($no_medrec !=''){
			$where1 .=" AND TP.no_medrec='$no_medrec' ";
		}
		if ($nama_pasien !=''){
			$where1 .=" AND TP.namapasien='$nama_pasien' ";
		}
		if ($no_reg !=''){
			$where1 .=" AND TP.nopendaftaran='$no_reg' ";
		}
				
		if ($tgl_trx!='') {
            $where1 .= " AND DATE(TP.tanggaldaftar) >='".YMDFormat($tgl_trx)."' AND DATE(TP.tanggaldaftar) <='".YMDFormat($tgl_trx2)."'";
        }
		
        $from = "(
					SELECT TD.id,TD.pendaftaran_id,TD.kasir_id,TP.tanggaldaftar,TP.nopendaftaran,TP.no_medrec,CONCAT(TP.title,'. ',TP.namapasien) as namapasien 
					,M.nama as poli,D.nama as namadokter,TD.nominal as tot_asuransi,TH.tipe,TD.status_lunas,TD.nominal_bayar,TD.tidak_terbayar
					from tklaim_detail TD
					LEFT JOIN tklaim TH ON TH.id=TD.klaim_id
					LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TD.pendaftaran_id
					LEFT JOIN mpoliklinik M ON TP.idpoliklinik=M.id
					LEFT JOIN mdokter D ON D.id=TP.iddokter
					
					WHERE TD.klaim_id='$klaim_id' ".$where1."
					GROUP BY TP.id

				) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('Tklaim_monitoring/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->id;
            $row[] = '';
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggaldaftar);
            $row[] = $r->nopendaftaran;
            $row[] = $r->no_medrec;
            $row[] = $r->namapasien;
            $row[] = $r->poli;
            $row[] = $r->namadokter;			
            $row[] = number_format($r->tot_asuransi,2);
            $row[] = number_format($r->nominal_bayar,2);
			if ($r->status_lunas=='1'){
				if ($r->nominal_bayar==$r->tot_asuransi){
					$row[] = '<span class="label label-primary">LUNAS</span>';							
				}else{
					$row[] = '<span class="label label-primary">LUNAS</span> <span class="label label-danger">OTHER LOSS</span> ';							
					
				}
			}else{		
				if ($r->nominal_bayar>0){
					$row[] = '<span class="label label-warning">SEBAGIAN</span>';		
					
				}else{					
					$row[] = '<span class="label label-danger">BELUM DIBAYAR</span>';				
				}
			}
			if ($r->nominal_bayar>0){
			$aksi .= '<button title="List Bayar" class="btn btn-xs btn-default list_bayar"><i class="fa fa-money"></i></button>';
			}
			if ($r->status_lunas=='0'){
				$aksi .= '<button title="Pelunasan" '.$disabel.' class="btn btn-xs btn-primary lunas"><i class="fa fa-check"></i></button>';
				$aksi .= '<button title="Set Other Loss" '.$disabel.' class="btn btn-xs btn-danger selesai"><i class="fa fa-minus-circle"></i></button>';
			}
			$aksi .= '<a class="view btn btn-xs btn-success" href="'.$url_kasir.'print_transaksi/'.$r->pendaftaran_id.'/'.$r->kasir_id.'" target="_blank"  type="button"  title="Detail"><i class="fa fa-print"></i></a>';
			// $aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_tagihan/'.$r->id.'/'.$r->tipe.'" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></a>';
		
			
			
			$aksi.='</div>';
            $row[] = $aksi;           
            $row[] = $r->tidak_terbayar;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function get_pembayaran(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		
		$disabel=$this->input->post('disabel');
		$id=$this->input->post('id');
		
        $from = "(
					SELECT D.id,D.tanggal_pembayaran,D.sisa_tagihan_awal,(D.nominal_bayar + D.other_income)as nominal_bayar,D.sisa_tagihan_next
					,CASE WHEN D.metode_bayar=1 THEN CONCAT('TRANSFER',' - ',B.nama) ELSE 'CASH' END as cara_bayar,D.keterangan 
					,D.created_user,D.created_date,D.st_validasi
					FROM tklaim_pembayaran D
					LEFT JOIN mbank B ON B.id=D.bankid
					WHERE D.klaim_detail_id='$id' AND D.status='1'
					ORDER BY id ASC
				) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $row[] = $r->id;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggal_pembayaran);
            $row[] = number_format($r->sisa_tagihan_awal,0);
            $row[] = number_format($r->nominal_bayar,0);
            $row[] = ($r->sisa_tagihan_next==0 ? '<span class="label label-primary">LUNAS</span>' : number_format($r->sisa_tagihan_next,0));
            $row[] = $r->cara_bayar;
            $row[] = $r->created_user.' - '.HumanDateLong($r->created_date);
			if ($r->st_validasi=='0'){
			$aksi .= '<button title="List Bayar" class="btn btn-xs btn-danger hapus_bayar"><i class="fa fa-trash"></i></button>';
			}else{
				$aksi .='<span class="label label-primary">SUDAH DIVALIDASI</span>';
			}
			$aksi.='</div>';
            $row[] = $aksi;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_koreksi_ranap(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		
		$disabel=$this->input->post('disabel');
		$klaim_id=$this->input->post('klaim_id');
		$no_reg=$this->input->post('no_reg');
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$tgl_trx=($this->input->post('tgl_trx'));
		$tgl_trx2=($this->input->post('tgl_trx2'));
		
		$where1='';
		
		if ($no_medrec !=''){
			$where1 .=" AND TP.no_medrec='$no_medrec' ";
		}
		if ($nama_pasien !=''){
			$where1 .=" AND TP.namapasien='$nama_pasien' ";
		}
		if ($no_reg !=''){
			$where1 .=" AND TP.nopendaftaran='$no_reg' ";
		}
				
		if ($tgl_trx!='') {
            $where1 .= " AND DATE(TP.tanggaldaftar) >='".YMDFormat($tgl_trx)."' AND DATE(TP.tanggaldaftar) <='".YMDFormat($tgl_trx2)."'";
        }
		
		
        $from = "(
					SELECT TD.id,TD.pendaftaran_id,TP.tanggaldaftar,TP.nopendaftaran,TP.no_medrec,CONCAT(TP.title,'. ',TP.namapasien) as namapasien 
					,K.nama as kelas,R.nama as ruangan,B.nama as bed,TR.tanggaldari,TR.tanggalsampai,TD.nominal as tot_asuransi,TD.tidak_terbayar
					,TH.tipe,TD.status_lunas,TD.nominal_bayar
					from tklaim_detail TD
					LEFT JOIN tklaim TH ON TH.id=TD.klaim_id
					LEFT JOIN trawatinap_pendaftaran TP ON TP.id=TD.pendaftaran_id
					LEFT JOIN mkelas K ON K.id=TP.idkelas
					LEFT JOIN mruangan R ON R.id=TP.idruangan
					LEFT JOIN mbed B ON B.id=TP.idbed
					LEFT JOIN trawatinap_ruangan TR ON TR.idrawatinap=TP.id
					LEFT JOIN trawatinap_tindakan_pembayaran TB ON TB.idtindakan=TP.id AND TB.statusbatal=0
					WHERE TD.klaim_id='$klaim_id' ".$where1."
					GROUP BY TP.id

				) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('Tklaim_monitoring/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->id;
            $row[] = '';
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggaldaftar);
            $row[] = $r->nopendaftaran;
            $row[] = $r->no_medrec;
            $row[] = $r->namapasien;
             $row[] = $r->kelas.'  -  '.$r->bed;
            $row[] = HumanDateShort($r->tanggaldari).' s/d '.HumanDateShort($r->tanggalsampai);
            $row[] = number_format($r->tot_asuransi,0);
            $row[] = number_format($r->nominal_bayar,0);
			if ($r->status_lunas=='1'){
				if ($r->nominal_bayar==$r->tot_asuransi){
					$row[] = '<span class="label label-primary">LUNAS</span>';							
				}else{
					$row[] = '<span class="label label-primary">LUNAS</span> <span class="label label-danger">OTHER LOSS</span> ';							
					
				}
			}else{		
				if ($r->nominal_bayar>0){
					$row[] = '<span class="label label-warning">SEBAGIAN</span>';		
					
				}else{					
					$row[] = '<span class="label label-danger">BELUM DIBAYAR</span>';				
				}
			}
			if ($r->nominal_bayar>0){
			$aksi .= '<button title="List Bayar" class="btn btn-xs btn-default list_bayar"><i class="fa fa-money"></i></button>';
			}
			if ($r->status_lunas=='0'){
				$aksi .= '<button title="Pelunasan" '.$disabel.' class="btn btn-xs btn-primary lunas"><i class="fa fa-check"></i></button>';
				$aksi .= '<button title="Set Other Loss" '.$disabel.' class="btn btn-xs btn-danger selesai"><i class="fa fa-minus-circle"></i></button>';
			}
			$aksi .= '<a class="view btn btn-xs btn-success" disabled href="#" target="_blank"  type="button"  title="Detail"><i class="fa fa-print"></i></a>';
			// $aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_tagihan/'.$r->id.'/'.$r->tipe.'" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></a>';
		
			
			
			$aksi.='</div>';
            $row[] = $aksi;           
            $row[] = $r->tidak_terbayar;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_ranap(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
        $userid = $this->session->userdata('user_id');
		
		
		$disabel=$this->input->post('disabel');
		$klaim_id=$this->input->post('klaim_id');
		$no_reg=$this->input->post('no_reg');
		$no_medrec=$this->input->post('no_medrec');
		$nama_pasien=$this->input->post('nama_pasien');
		$tgl_trx=($this->input->post('tgl_trx'));
		$tgl_trx2=($this->input->post('tgl_trx2'));
		
		$where1='';
		
		if ($no_medrec !=''){
			$where1 .=" AND TP.no_medrec='$no_medrec' ";
		}
		if ($nama_pasien !=''){
			$where1 .=" AND TP.namapasien='$nama_pasien' ";
		}
		if ($no_reg !=''){
			$where1 .=" AND TP.nopendaftaran='$no_reg' ";
		}
				
		if ($tgl_trx!='') {
            $where1 .= " AND DATE(TP.tanggaldaftar) >='".YMDFormat($tgl_trx)."' AND DATE(TP.tanggaldaftar) <='".YMDFormat($tgl_trx2)."'";
        }
		
        $from = "(
					SELECT TD.id,TD.pendaftaran_id,TP.tanggaldaftar,TP.nopendaftaran,TP.no_medrec,CONCAT(TP.title,'. ',TP.namapasien) as namapasien 
					,K.nama as kelas,R.nama as ruangan,B.nama as bed,TR.tanggaldari,TR.tanggalsampai,TB.total		
					from tklaim_detail TD
					LEFT JOIN trawatinap_pendaftaran TP ON TP.id=TD.pendaftaran_id
					LEFT JOIN mkelas K ON K.id=TP.idkelas
					LEFT JOIN mruangan R ON R.id=TP.idruangan
					LEFT JOIN mbed B ON B.id=TP.idbed
					LEFT JOIN trawatinap_ruangan TR ON TR.idrawatinap=TP.id
					LEFT JOIN trawatinap_tindakan_pembayaran TB ON TB.idtindakan=TP.id AND TB.statusbatal=0
					WHERE TD.klaim_id='$klaim_id' ".$where1."
					GROUP BY TP.id

				) as tbl ";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array();
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }


        foreach ($list as $r) {
            $no++;
            $row = array();
			$date_setting='1';
            $aksi       = '<div class="btn-group">';
            $status     = '';
            $status_retur     = '';
            $url        = site_url('Tklaim_monitoring/');
            $url_kasir        = site_url('tkasir/');
            $url_batal       = site_url('tgudang_pengembalian/');
			// $status_retur=$this->status_tipe_kembali($r->tipe_kembali);
            $row[] = $r->id;
            $row[] = $no;
            $row[] = HumanDateShort($r->tanggaldaftar);
            $row[] = $r->nopendaftaran;
            $row[] = $r->no_medrec;
            $row[] = $r->namapasien;
            $row[] = $r->kelas.'  -  '.$r->bed;
            $row[] = HumanDateShort($r->tanggaldari);
            $row[] = HumanDateShort($r->tanggalsampai);
            $row[] = number_format($r->total,0);
           
			$aksi .= '<button title="Memajukan Tagihan" '.$disabel.' class="btn btn-xs btn-danger ganti"><i class="si si-arrow-right"></i></button>';
			$aksi .= '<button disabled class="view btn btn-xs btn-success" type="button"  title="Detail"><i class="fa fa-print"></i></button>';
			// $aksi .= '<a title="Lihat Detail" href="'.$url.'rincian_tagihan/'.$r->id.'/'.$r->tipe.'" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></a>';
		
			
			
			$aksi.='</div>';
            $row[] = $aksi;           
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function rincian_tagihan($id,$tipe,$disabel='0'){
		
		$data=$this->model->detail($id);
		// print_r($data);exit();
		$data['grandtotalnominal'] 				= '0';
		$data['totalnominal'] 				= '0';
		$data['tgl_trx'] 				= '';
		$data['tgl_trx2'] 				= '';
		$data['st_kbo'] 				= '';
		if ($disabel!='0'){
			$data['disabel'] 				= 'disabled';
			
		}else{
			$data['disabel'] 				= '';
		}
		$data['error'] 				= '';
		$data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		
		if ($tipe=='1'){
			$data['title'] 				= 'Rekapitulasi Piutang <h3><span class="label label-primary">Rawat Jalan</span></h3>';
			$data['content'] 			= 'Tklaim_monitoring/detail_rincian';
		}else{
			$data['title'] 				= 'Rekapitulasi Piutang <h3><span class="label label-success">Rawat Inap</span></h3>';
			$data['content'] 			= 'Tklaim_monitoring/detail_rincian_ri';
		}
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'), 
										array("Klaim Rincian",'tKlaim Rincian/index'), 
										array("List",'#') 
									);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		// print_r($id);
	}
	function koreksi_tagihan($id,$tipe,$disabel='0'){
		
		$data=$this->model->detail($id);
		// print_r($data);exit();
		$data['grandtotalnominal'] 				= '0';
		$data['totalnominal'] 				= '0';
		$data['tgl_trx'] 				= '';
		$data['tgl_trx2'] 				= '';
		$data['st_kbo'] 				= '';
		if ($disabel!='0'){
			$data['disabel'] 				= 'disabled';
			
		}else{
			$data['disabel'] 				= '';
		}
		$data['error'] 				= '';
		$data['list_kelompok_pasien'] 	= $this->model->list_kelompok_pasien();
		$data['list_rekanan'] 	= $this->model->list_rekanan();
		$data['list_bank'] 	= $this->model->list_bank();
		
		if ($tipe=='1'){
			$data['title'] 				= 'Koreksi Piutang <h3><span class="label label-primary">Rawat Jalan</span></h3>';
			$data['content'] 			= 'Tklaim_monitoring/koreksi_rajal';
		}else{
			$data['title'] 				= 'Koreksi Piutang <h3><span class="label label-success">Rawat Inap</span></h3>';
			$data['content'] 			= 'Tklaim_monitoring/koreksi_ranap';
		}
		$data['breadcrum'] 			= array(
										array("RSKB Halmahera",'#'), 
										array("Klaim Rincian",'tKlaim Rincian/index'), 
										array("List",'#') 
									);
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		// print_r($id);
	}
	function cek_date(){
		$tgl='2011-20-01';
		$tgl_asli=(int)substr($tgl,-2);
		print_r($tgl_asli);
	}
	
	function list_tanggal($idkelompokpasien='1',$idrekanan='#',$tipe='1'){
		$st_cari='2';
		if ($idkelompokpasien=='1'){
			$q_cari="SELECT COUNT(S.id) as jml FROM mrekanan_setting S WHERE S.idrekanan='$idrekanan' AND S.`status`='1'";
			$hasil=$this->db->query($q_cari)->row('jml');
			if ($hasil > 0){
				$st_cari='1';
			}
		}
		if ($st_cari=='1'){//FOKUS KE SETTING REKANAN
			$q="SELECT DATE_FORMAT(T.tanggal_next,'%d-%m-%Y') as tanggal_next FROM (SELECT TS.*,TK.tanggal_tagihan FROM (
					SELECT '1' as tipe,RS.tanggal_hari 
									,CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-',LPAD(RS.tanggal_hari,2,'0')) as tgl
									,CASE WHEN (SELECT tgl) < CURRENT_DATE THEN DATE_ADD((SELECT tgl), INTERVAL 1 MONTH) ELSE (SELECT tgl) END as tanggal_next
									from mrekanan_setting RS 
									WHERE RS.idrekanan=$idrekanan AND RS.`status`='1'
					) TS LEFT JOIN tklaim TK ON TS.tanggal_next=TK.tanggal_tagihan AND TK.idrekanan='$idrekanan' AND TK.tipe='$tipe' AND TK.`status` > 1
					ORDER BY TS.tanggal_next ASC
				) T WHERE T.tanggal_tagihan IS NULL";
		}else{//FOKUS KE SETTING KELOMPOK PASIEN
			$q="SELECT DATE_FORMAT(T.tanggal_next,'%d-%m-%Y') as tanggal_next FROM (SELECT TS.*,TK.tanggal_tagihan
					FROM (
					SELECT '2' as tipe,KS.tanggal_hari 
					,CONCAT(YEAR(CURRENT_DATE),'-',DATE_FORMAT(CURRENT_DATE(),'%m'),'-',LPAD(KS.tanggal_hari,2,'0')) as tgl
					,CASE WHEN (SELECT tgl) < CURRENT_DATE THEN DATE_ADD((SELECT tgl), INTERVAL 1 MONTH) ELSE (SELECT tgl) END as tanggal_next
					FROM mpasien_kelompok_setting KS 
					WHERE KS.idkelompokpasien='$idkelompokpasien' AND KS.`status`='1'
				) TS  LEFT JOIN tklaim TK ON TS.tanggal_next=TK.tanggal_tagihan AND TK.idkelompokpasien='$idkelompokpasien' AND TK.tipe='$tipe' AND TK.`status` > 1
				ORDER BY TS.tanggal_next ASC
			) T WHERE T.tanggal_tagihan IS NULL";
		}
		$row=$this->db->query($q)->result();
		$opsi='';
		foreach($row as $r){
			$opsi .='<option value="'. $r->tanggal_next.'">'.$r->tanggal_next.'</option>';
		}
		$arr['detail']=$opsi;
		$this->output->set_output(json_encode($arr));
	}
	
	function lunas_all($id){
		$data=array(
			'status_kirim' =>'1',
			'status' =>'3',
			'noresi' =>$noresi,
			'tanggal_kirim' =>$tanggal_kirim,
			'keterangan' =>$keterangan,			
			'kirim_user' =>$this->session->userdata('user_name'),
			'kirim_date' =>date('Y-m-d H:i:s'),
		);
		$this->db->where('klaim_id', $id);
		$result=$this->db->update('tklaim', $data);		
		$this->output->set_output(json_encode($result));
	}
	function tambah_fu(){
		
		$klaim_id=$this->input->post('klaim_id');
		$tanggal_info=YMDFormat($this->input->post('tanggal_info'));
		$tanggal_reminder=YMDFormat($this->input->post('tanggal_reminder'));
		$keterangan=$this->input->post('keterangan');
		$data=array(
			'klaim_id' =>$klaim_id,
			'tanggal_info' =>$tanggal_info,
			'tanggal_reminder' =>$tanggal_reminder,
			'keterangan' =>$keterangan,			
			'created_date' =>date('Y-m-d H:i:s'),
			'created_user' =>$this->session->userdata('user_name'),
			'created_user_id' =>$this->session->userdata('user_ide'),
		);
		// $this->db->where('id', $id);
		$result=$this->db->insert('tklaim_fu', $data);		
		$this->output->set_output(json_encode($result));
	}
	function update_pembayaran(){
		$id=$this->input->post('id');
		$klaim_id=$this->input->post('klaim_id');
		$tanggal_pembayaran=YMDFormat($this->input->post('tanggal_pembayaran'));
		$metode_bayar=$this->input->post('metode_bayar');
		$bankid=$this->input->post('bankid');
		$harus_dibayar=RemoveComma($this->input->post('harus_dibayar'));
		$nominal_bayar=RemoveComma($this->input->post('nominal_bayar'));
		$tidak_terbayar=RemoveComma($this->input->post('tidak_terbayar'));
		$other_income=RemoveComma($this->input->post('other_income'));
		$keterangan=$this->input->post('keterangan');
		if ($tidak_terbayar > 0){
			$status_lunas='0';
			// $status_other_loss='1';
		}else{
			$status_lunas='1';
			$status_other_loss='0';
			
		}
		if ($nominal_bayar > $harus_dibayar){
			$nominal_bayar=$harus_dibayar;
		}

		$data_bayar=array(
			'klaim_detail_id' =>$id,
			'tanggal_pembayaran' =>$tanggal_pembayaran,
			'metode_bayar' =>$metode_bayar,
			'bankid' =>$bankid,			
			'keterangan' =>$keterangan,				
			'sisa_tagihan_awal' =>$harus_dibayar,			
			'nominal_bayar' =>$nominal_bayar,			
			'other_income' =>$other_income,			
			'sisa_tagihan_next' =>$tidak_terbayar,			
			'created_date' =>date('Y-m-d H:i:s'),
			'created_user' =>$this->session->userdata('user_name'),			
			'created_user_id' =>$this->session->userdata('user_id'),
		);
		$this->db->insert('tklaim_pembayaran', $data_bayar);	
		
		$data=array(
			'tanggal_pembayaran' =>$tanggal_pembayaran,
			'metode_bayar' =>$metode_bayar,
			'bankid' =>$bankid,			
			// 'nominal_bayar' =>$nominal_bayar,			
			'tidak_terbayar' =>$tidak_terbayar,			
			// 'other_income' =>$other_income,			
			'keterangan' =>$keterangan,			
			'status_lunas' =>$status_lunas,			
			// 'status_other_loss' =>$status_other_loss,			
			'user_pelunasan' =>$this->session->userdata('user_name'),
			'date_pelunasan' =>date('Y-m-d H:i:s'),
			// 'created_user_id' =>$this->session->userdata('user_ide'),
		);
		if ($tidak_terbayar==0){
			$data['status_other_loss']='0';
		}
		$this->db->where('id', $id);
		$this->db->update('tklaim_detail', $data);	
		
		$data_koreksi=array(
			'user_koreksi' =>$this->session->userdata('user_name'),
			'last_koreksi' =>date('Y-m-d H:i:s'),
			'tanggal_pelunasan' =>date('Y-m-d'),

		);
		$this->db->where('id', $klaim_id);
		$result=$this->db->update('tklaim', $data_koreksi);	
		
		$this->output->set_output(json_encode($result));
	}
	function load_data_bayar($id){
		$arr['bayar']=$this->
		$this->output->set_output(json_encode($result));
	}
	function update_pembayaran_all(){
		$id=$this->input->post('id');
		$tanggal_pembayaran=YMDFormat($this->input->post('tanggal_pembayaran'));
		$metode_bayar=$this->input->post('metode_bayar');
		$bankid=$this->input->post('bankid');
		$nominal_bayar=RemoveComma($this->input->post('nominal_bayar'));
		$tidak_terbayar=RemoveComma($this->input->post('tidak_terbayar'));
		$keterangan=$this->input->post('keterangan');
		$status_lunas='1';
		$status_other_loss='0';
		$data_detail=$this->db->query("SELECT *from tklaim_detail D WHERE D.klaim_id='$id'")->result();
		foreach ($data_detail as $r){
			$data=array(
				'tanggal_pembayaran' =>$tanggal_pembayaran,
				'metode_bayar' =>$metode_bayar,
				'bankid' =>$bankid,			
				'nominal_bayar' =>$r->nominal,			
				'tidak_terbayar' =>0,			
				'keterangan' =>$keterangan,			
				'status_lunas' =>$status_lunas,			
				'status_other_loss' =>$status_other_loss,			
				'user_pelunasan' =>$this->session->userdata('user_name'),
				'date_pelunasan' =>date('Y-m-d H:i:s'),
				// 'created_user_id' =>$this->session->userdata('user_ide'),
			);
			$this->db->where('id', $r->id);
			$this->db->update('tklaim_detail', $data);	
			
			$data_bayar=array(
				'klaim_detail_id' =>$r->id,
				'tanggal_pembayaran' =>$tanggal_pembayaran,
				'metode_bayar' =>$metode_bayar,
				'bankid' =>$bankid,			
				'keterangan' =>$keterangan,				
				'sisa_tagihan_awal' =>$r->nominal,			
				'nominal_bayar' =>$r->nominal,			
				'sisa_tagihan_next' =>0,			
				'created_date' =>date('Y-m-d H:i:s'),
				'created_user' =>$this->session->userdata('user_name'),			
				'created_user_id' =>$this->session->userdata('user_id'),
			);
			$this->db->insert('tklaim_pembayaran', $data_bayar);	
			
		}
		$data_koreksi=array(
			'user_koreksi' =>$this->session->userdata('user_name'),
			'last_koreksi' =>date('Y-m-d H:i:s'),
			'tanggal_pelunasan' =>date('Y-m-d'),

		);
		$this->db->where('id', $id);
		$result=$this->db->update('tklaim', $data_koreksi);	
		
		$this->output->set_output(json_encode($result));
	}
	function update_other_loss(){
		
		
		$id=$this->input->post('id');
		$klaim_id=$this->input->post('klaim_id');
		$tanggal_pembayaran=date('Y-m-d');		
		$status_lunas='1';
		$status_other_loss='1';
		$this->model->insert_jurnal_other_loss($id);
		$data=array(
			'tanggal_pembayaran' =>$tanggal_pembayaran,
			'status_lunas' =>$status_lunas,			
			'status_other_loss' =>$status_other_loss,			
			'user_pelunasan' =>$this->session->userdata('user_name'),
			'date_pelunasan' =>date('Y-m-d H:i:s'),
			// 'created_user_id' =>$this->session->userdata('user_ide'),
		);
		$this->db->where('id', $id);
		$result=$this->db->update('tklaim_detail', $data);	

		$data_koreksi=array(
			'user_koreksi' =>$this->session->userdata('user_name'),
			'last_koreksi' =>date('Y-m-d H:i:s'),
			'tanggal_pelunasan' =>date('Y-m-d'),

		);
		$this->db->where('id', $klaim_id);
		$result=$this->db->update('tklaim', $data_koreksi);	
		
		$this->output->set_output(json_encode($result));
	}
	function hapus_bayar(){
		
		
		$id=$this->input->post('id');
		
		$result=$this->db->query("DELETE FROM tklaim_pembayaran WHERE id='$id'");	
		
		$this->output->set_output(json_encode($result));
	}
	public function upload_bukti() {
      $uploadDir = './assets/upload/piutang';
		if (!empty($_FILES)) {
			 $klaim_id = $this->input->post('upload_klaim_id');
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$detail['klaim_id'] 	= $klaim_id;
			$detail['file_name']		= $file_name;
			$detail['size']		= formatSizeUnits($size);
			$detail['tanggal_upload']	= date('Y-m-d H:i:s');
			$detail['user_upload']	= $this->session->userdata('user_name');
			$this->db->insert('tklaim_upload', $detail);
		}
		
    }
	function hapus_file(){
		$id=$this->input->post('id');
		$row =$this->db->query("select file_name from tklaim_upload where id='$id'")->row();
		if(file_exists('./assets/upload/piutang/'.$row->file_name) && $row->file_name !='') {
			unlink('./assets/upload/piutang/'.$row->file_name);
		}else{
			
		}
		$result=$this->db->query("delete from tklaim_upload WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	function hapus_informasi(){
		$id=$this->input->post('id');
		
		$result=$this->db->query("delete from tklaim_fu WHERE id='$id'");
		 if ($result) {
            $this->output->set_output(json_encode($result));
        } else {
            $this->output->set_output(json_encode($result));
        }
	}
	public function insert_jurnal_other_loss($id){
		$this->model->insert_jurnal_other_loss($id);
		
		print_r('SINI');exit();
	}
}
