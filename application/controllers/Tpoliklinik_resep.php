<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpoliklinik_resep extends CI_Controller {

	function __construct()  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tpoliklinik_resep_model');
		$this->load->model('Tpendaftaran_poli_ttv_model');
		$this->load->model('Tpendaftaran_poli_model');
		$this->load->helper('path');
		
  }
	function get_head_parent($path){
		// $path='14.1.11';
		$arr=explode(".",$path);
		return $arr[0];
	}
	function tindakan($pendaftaran_id='',$menu_atas='',$menu_kiri='',$trx_id='#',$versi_edit='0'){
		$data_login_ppa=get_ppa_login();
		// print_r($this->session->userdata());exit;
		$array_ri=array(
			'input_eresep_ri'
		);
		
		if (in_array($menu_kiri,$array_ri)){
			$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id,1);
			$tipe_rj_ri='3';
			$st_ranap='1';
		}else{
			$data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id,0);
			$tipe_rj_ri='1';
			$st_ranap='0';
		}
		// $data=$this->Tpendaftaran_poli_ttv_model->logic_akses_form_erm($pendaftaran_id);
		
		$data['tittle']='E-RESEP';
		$def_image='';
		if ($data['jenis_kelamin']=='1'){
			  $def_image='def_1.png';
		  }else{
			  $def_image='def_2.png';
			  
		  }
		$data['def_image'] 			= $def_image;
		$data['url_back'] 			= site_url().'tpendaftaran_poli_ttv';
		$data['url_utama'] 			= site_url().'tpoliklinik_resep/tindakan/'.$pendaftaran_id.'/';
		$data['menu_atas'] 			= $menu_atas;
		$data['menu_kiri'] 			= $menu_kiri;
		
		
		$data['error'] 			= '';
		$data['st_hide_breadcam'] 			= '1';
		$data['title'] 			= 'Tindakan';
		$data['content'] 		= 'Tpendaftaran_poli_ttv/menu/_app_header';
		$data['breadcrum'] 	= array(
			  array("RSKB Halmahera",'#'),
			  array("Poliklinik Tindakan ",'#'),
			  array("Input E-RESEP",'tpoliklinik_trx')
			);
		$data['tanggal_transaksi']=date('d-m-Y H:i:s');
		// $data['pendaftaran_id']=$pendaftaran_id;
		
		if ($menu_kiri=='input_eresep' || $menu_kiri=='input_eresep_ri' || $menu_kiri=='his_eresep'){
			$data['breadcrum'] 	= array(
			  array("RSKB Halmahera",'#'),
			  array("Poliklinik Tindakan ",'#'),
			  array("E-RESEP",'tpoliklinik_trx')
			);

			$data_label=$this->Tpoliklinik_resep_model->setting_label();
			$data = array_merge($data,$data_label);
			// print_r($data);exit;
			if ($tipe_rj_ri=='3'){
				$q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='1' AND H.tipepegawai='2' AND pegawai_id='".$data['iddokterpenanggungjawab']."'";
				
			}else{
				$q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='1' AND H.tipepegawai='2' AND pegawai_id='".$data['iddokter']."'";
				
			}
			$data_ppa=$this->db->query($q)->row();
			
			if ($data_ppa){
				$iddokter_resep=$data_ppa->id;
			}else{
				$iddokter_resep=0;
				
			}
			
			if ($trx_id!='#'){
				
					$data_assemen=$this->Tpoliklinik_resep_model->get_data_assesmen_trx($trx_id);
					
					$data['list_ppa'] = $this->Tpoliklinik_resep_model->list_ppa(1);
					$data['list_tujuan_eresep'] = $this->Tpoliklinik_resep_model->list_tujuan_eresep_all();
					
			}else{
				$data_assemen=$this->Tpoliklinik_resep_model->get_data_assesmen($pendaftaran_id,$tipe_rj_ri);
				$data['list_ppa'] = $this->Tpoliklinik_resep_model->list_ppa(1);
				$data['list_ppa_all'] = $this->Tpoliklinik_resep_model->list_ppa_all();
				$data['list_tujuan_eresep'] = $this->Tpoliklinik_resep_model->list_tujuan_eresep($data['idtipe_poli'],$data['idpoliklinik'],$iddokter_resep);
			}
			if ($data_assemen){
				if ($data_assemen['namapasien']==''){
					$data_assemen['namapasien']=$data['namapasien'];
					$data_assemen['nopendaftaran']=$data['nopendaftaran'];
					$data_assemen['nopendaftaran']=$data['nopendaftaran'];
				}
				$data['racikan_id']='';
				$q="SELECT racikan_id,nama_racikan,jumlah_racikan,jenis_racikan,interval_racikan,rute_racikan,aturan_tambahan_racikan,iter_racikan,status_racikan
					FROM tpoliklinik_e_resep_obat_racikan H WHERE H.assesmen_id='".$data_assemen['assesmen_id']."' AND H.status_racikan='1'";	
				$data_racikan=$this->db->query($q)->row_array();
				if ($data_racikan){}else{
					$data_racikan['nama_racikan']='';
					$data_racikan['jumlah_racikan']='';
					$data_racikan['jenis_racikan']='';
					$data_racikan['interval_racikan']='';
					$data_racikan['rute_racikan']='';
					$data_racikan['aturan_tambahan_racikan']='';
					$data_racikan['iter_racikan']='';
					$data_racikan['status_racikan']='';
				}
					$data = array_merge($data,$data_racikan);
			}else{
				$data_assemen['template_assesmen_id']='';
				$data_assemen['assesmen_id']='';
				$data_assemen['racikan_id']='';
				$data_assemen['status_assemen']='0';
				$data_assemen['st_edited']='0';
				$data_assemen['jml_edit']='0';
				
			}
			// print_r($data_assemen);exit;
			$data_assemen['trx_id']=$trx_id;
			$setting_assesmen=$this->Tpoliklinik_resep_model->setting_eresep();
			// print_r($data_assemen);exit;
			$data = array_merge($data,$data_assemen,$setting_assesmen);
		}
		if ($menu_kiri=='riwayat_eresep'){
			$data['breadcrum'] 	= array(
			  array("RSKB Halmahera",'#'),
			  array("Poliklinik Tindakan ",'#'),
			  array("E-RESEP",'tpoliklinik_trx')
			);
			$data['tab']='1';

			if ($trx_id!='#'){
				
					$data_assemen=$this->Tpoliklinik_resep_model->get_data_assesmen_trx($trx_id);
					$data['list_ppa'] = $this->Tpoliklinik_resep_model->list_ppa(1);
					$data['list_tujuan_eresep'] = $this->Tpoliklinik_resep_model->list_tujuan_eresep_all();
					
			}else{
				$data_assemen=$this->Tpoliklinik_resep_model->get_data_assesmen($pendaftaran_id);
				// $data['list_ppa'] = $this->Tpoliklinik_resep_model->list_ppa(1);
				// $data['list_ppa_all'] = $this->Tpoliklinik_resep_model->list_ppa_all();
				// $data['list_tujuan_eresep'] = $this->Tpoliklinik_resep_model->list_tujuan_eresep($data['idtipe_poli'],$data['idpoliklinik'],$iddokter_resep);
			}
			
			// $data = array_merge($data,$data_assemen);
		}
		if ($menu_kiri=='input_telaah'){
			// print_r('sini');exit;
			$data['breadcrum'] 	= array(
			  array("RSKB Halmahera",'#'),
			  array("Poliklinik Tindakan ",'#'),
			  array("VERIFIKASI RESEP",'tpoliklinik_trx')
			);

			$data_label=$this->Tpoliklinik_resep_model->setting_label();
			$data = array_merge($data,$data_label);
			
		
			
			if ($trx_id!='#'){
				$data_assemen=$this->Tpoliklinik_resep_model->get_data_assesmen_trx($trx_id);
				$data['list_ppa'] = $this->Tpoliklinik_resep_model->list_ppa(1);
				$data['list_tujuan_eresep'] = $this->Tpoliklinik_resep_model->list_tujuan_eresep_all();
				$data['list_telaah']=$this->Tpoliklinik_resep_model->get_telaah($data_assemen['assesmen_id']);
				$data['list_telaah_obat']=$this->Tpoliklinik_resep_model->get_telaah_obat($data_assemen['assesmen_id']);
				
				$q="SELECT racikan_id,nama_racikan,jumlah_racikan,jenis_racikan,interval_racikan,rute_racikan,aturan_tambahan_racikan,iter_racikan,status_racikan
					FROM tpoliklinik_e_resep_obat_racikan H WHERE H.assesmen_id='".$data_assemen['assesmen_id']."' AND H.status_racikan='1'";	
				$data_racikan=$this->db->query($q)->row_array();
				if ($data_racikan){}else{
					$data_racikan['racikan_id']='';
					$data_racikan['nama_racikan']='';
					$data_racikan['jumlah_racikan']='';
					$data_racikan['jenis_racikan']='';
					$data_racikan['interval_racikan']='';
					$data_racikan['rute_racikan']='';
					$data_racikan['aturan_tambahan_racikan']='';
					$data_racikan['iter_racikan']='';
					$data_racikan['status_racikan']='';
				}
				$data = array_merge($data,$data_racikan);
					
			}
			
			$data_assemen['trx_id']=$trx_id;
			$data['list_ppa_all'] = $this->Tpoliklinik_resep_model->list_ppa_all();
			$setting_assesmen=$this->Tpoliklinik_resep_model->setting_eresep();
			$data = array_merge($data,$data_assemen,$setting_assesmen);
		}
		$data['trx_id']=$trx_id;
		$data['st_ranap']=$st_ranap;
		$pegawai_id=$this->session->userdata('login_pegawai_id');
		$data['list_template_assement']=array();
		$data['list_dokter'] 			= $this->Tpendaftaran_poli_model->list_dokter();
		$data['list_poli'] 			= $this->Tpendaftaran_poli_ttv_model->list_poli();
		$data = array_merge($data, backend_info());
		// print_r($data);exit;
		$this->parser->parse('module_template', $data);
	}
	function simpan_ttd_penerima(){
		
		$assesmen_id = $this->input->post('assesmen_id');
		$penerima_ttd = $this->input->post('penerima_ttd');
		// $ttd_petugas = $this->input->post('ttd_petugas');
		$data=array(
			'penerima_ttd' =>$penerima_ttd,
			'date_ttd' =>date('Y-m-d H:i:s'),
		);
		// print_r($id);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update('tpoliklinik_e_resep',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function save_eresep(){
		// $pendaftaran['tanggaldaftar'] = YMDFormat($this->input->post('tglpendaftaran')) . ' ' . $this->input->post('waktupendaftaran');
		$tanggal_permintaan= YMDFormat($this->input->post('tanggalpermintaan')). ' ' .$this->input->post('waktupermintaan');
		// $waktudaftar= $this->input->post('waktudaftar'),
		$assesmen_id=$this->input->post('assesmen_id');
		$st_edited=$this->input->post('st_edited');
		$status_assemen=$this->input->post('status_assemen');
		$jml_edit=$this->input->post('jml_edit');
		$data=array(
			'tanggal_permintaan'=> $tanggal_permintaan,
			'status_assemen' => $this->input->post('status_assemen'),
			'tujuan_farmasi_id' => $this->input->post('tujuan_farmasi_id'),
			'iddokter_resep' => $this->input->post('iddokter_resep'),
			'berat_badan' => $this->input->post('berat_badan'),
			'tinggi_badan' => $this->input->post('tinggi_badan'),
			'luas_permukaan' => $this->input->post('luas_permukaan'),
			'diagnosa' => $this->input->post('diagnosa'),
			'menyusui' => $this->input->post('menyusui'),
			'gangguan_ginjal' => $this->input->post('gangguan_ginjal'),
			'st_puasa' => $this->input->post('st_puasa'),
			'st_alergi' => $this->input->post('st_alergi'),
			'alergi' => $this->input->post('alergi'),
			'prioritas_cito' => $this->input->post('prioritas_cito'),
			'resep_pulang' => $this->input->post('resep_pulang'),
			'catatan_resep' => $this->input->post('catatan_resep'),


		);
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		
		if ($st_edited=='0'){
			$data['created_ppa']=$login_ppa_id;
			$data['created_date']=date('Y-m-d H:i:s');
		}else{
			$jml_edit=$this->db->query("SELECT COUNT(assesmen_id) as jml_edit FROM tpoliklinik_assesmen_his WHERE assesmen_id='$assesmen_id'")->row('jml_edit');
			// IF (){
				
			// }
			// $jml_edit=$jml_edit +1;
			$data['jml_edit']=$jml_edit;
			$data['edited_ppa']=$login_ppa_id;
			$data['edited_date']=date('Y-m-d H:i:s');
		}
		if ($status_assemen=='2'){
			$data['st_edited']=0;
			$data['tanggal_kirim']=date('Y-m-d H:i:s');
			
		}
		// print_r($assesmen_id);
		// exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_e_resep',$data);
		// $ttv_id=$this->db->insert_id();
		
		if ($result){
			$hasil=$data;
		}else{
			$hasil=false;
		}
		$this->output->set_output(json_encode($hasil));
	}
	function create_eresep(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $tinggi_badan=$this->input->post('tinggi_badan');
	  $berat_badan=$this->input->post('berat_badan');
	  $idtipe_poli=$this->input->post('idtipe_poli');
	  $iddokter=$this->input->post('iddokter');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
	  $asalrujukan=$this->input->post('asalrujukan');
	  $st_ranap=$this->input->post('st_ranap');
	  $pendaftaran_id_ranap=($this->input->post('pendaftaran_id_ranap')?$this->input->post('pendaftaran_id_ranap'):'');
	  $iddokter_resep='0';
		$q="SELECT *FROM mppa H WHERE H.jenis_profesi_id='1' AND H.tipepegawai='2' AND pegawai_id='".$iddokter."'";
		$data_ppa=$this->db->query($q)->row();
		if ($data_ppa){
			$iddokter_resep=$data_ppa->id;
		}
		if ($st_ranap=='1'){
			$diagnosa=$this->Tpoliklinik_resep_model->get_dianosa_utama_ranap($pendaftaran_id_ranap);
		}else{
			$diagnosa=$this->Tpoliklinik_resep_model->get_dianosa_utama($pendaftaran_id,$idtipe_poli);
		}
	  
	  $q="SELECT compare_value_4(
			MAX(IF(H.asal_pasien = '$idtipe_poli' AND H.idpoli_kelas='$idpoliklinik' AND H.iddokter_ppa='$iddokter_resep',H.tujuan_id,NULL))
			,MAX(IF(H.asal_pasien = '$idtipe_poli' AND H.idpoli_kelas='$idpoliklinik' AND H.iddokter_ppa='0',H.tujuan_id,NULL))
			,MAX(IF(H.asal_pasien = '$idtipe_poli' AND H.idpoli_kelas='0' AND H.iddokter_ppa='0',H.tujuan_id,NULL))
			,MAX(IF(H.asal_pasien = '0' AND H.idpoli_kelas='0' AND H.iddokter_ppa='0',H.tujuan_id,NULL))
			) as tujuan_farmasi_id FROM `mtujuan_farmasi_logic` H
			WHERE H.jenis_order='1'";
			// print_r($q);exit;
			$tujuan_farmasi_id=$this->db->query($q)->row('tujuan_farmasi_id');
			$data=array(
				'pendaftaran_id' => $pendaftaran_id,
				'pendaftaran_id_ranap' => $pendaftaran_id_ranap,
				'asalrujukan' => $asalrujukan,
				'idpasien' => $idpasien,
				'diagnosa' => $diagnosa,
				'iddokter_resep' => $iddokter_resep,
				'berat_badan' => $berat_badan,
				'tinggi_badan' => $tinggi_badan,
				'tujuan_farmasi_id' => $tujuan_farmasi_id,
				'created_ppa' => $login_ppa_id,
				'tanggal_permintaan' => date('Y-m-d H:i:s'),
				'created_date' => date('Y-m-d H:i:s'),
				'status_assemen' => 1,
			);
	  $hasil=$this->db->insert('tpoliklinik_e_resep',$data);
	  $assesmen_id=$this->db->insert_id();
	  
	  
	  $this->output->set_output(json_encode($hasil));
    }
	function create_racikan(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $pendaftaran_id=$this->input->post('pendaftaran_id');
	  $idpasien=$this->input->post('idpasien');
	  $iter=$this->input->post('iter');
	  $data=array(
		'assesmen_id' => $assesmen_id,
		'pendaftaran_id' => $pendaftaran_id,
		'idpasien' => $idpasien,
		'iter_racikan' => $iter,
		'created_ppa' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_racikan' => 1,
		'jumlah_racikan' => 1,
	  );
	  $hasil=$this->db->insert('tpoliklinik_e_resep_obat_racikan',$data);
	  if ($hasil){
		  $racikan_id=$this->db->insert_id();
		// $data['racikan_id']=$this->db->insert_id();
	  }else{
		  $data=null;
	  }
	  $q="SELECT *from tpoliklinik_e_resep_obat_racikan where racikan_id='$racikan_id'";
	  $data=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($data));
    }
	
	function simpan_heaad_racikan(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $racikan_id=$this->input->post('racikan_id');
	  $data=array(
		'nama_racikan' => $this->input->post('nama_racikan'),
		'jumlah_racikan' => $this->input->post('jumlah_racikan'),
		'kuantitas' => $this->input->post('jumlah_racikan'),
		'jenis_racikan' => $this->input->post('jenis_racikan'),
		'interval_racikan' => $this->input->post('interval_racikan'),
		'rute_racikan' => $this->input->post('rute_racikan'),
		'aturan_tambahan_racikan' => $this->input->post('aturan_tambahan_racikan'),
		'iter_racikan' => $this->input->post('iter_racikan'),
		'status_racikan' => $this->input->post('status_racikan'),
	  );
	  $this->db->where('racikan_id',$racikan_id);
	  $hasil=$this->db->update('tpoliklinik_e_resep_obat_racikan',$data);
	  
	 
		$q="
			UPDATE tpoliklinik_e_resep_obat_racikan_obat U 
			SET U.totalharga=null WHERE U.racikan_id='$racikan_id'
		";
		$this->db->query($q);
		
	  $this->output->set_output(json_encode($data));
    }
	function simpan_heaad_racikan_telaah(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  
	  
	  $assesmen_id=$this->input->post('assesmen_id');
	  $racikan_id=$this->input->post('racikan_id');
	  $st_manual_resep=$this->input->post('st_manual_resep');
	  $jumlah_racikan = $this->input->post('jumlah_racikan');
	  $data=array(
		'nama_racikan' => $this->input->post('nama_racikan'),
		'jumlah_racikan' => $this->input->post('jumlah_racikan'),
		'kuantitas' => $this->input->post('jumlah_racikan'),
		'jenis_racikan' => $this->input->post('jenis_racikan'),
		'interval_racikan' => $this->input->post('interval_racikan'),
		'rute_racikan' => $this->input->post('rute_racikan'),
		'aturan_tambahan_racikan' => $this->input->post('aturan_tambahan_racikan'),
		'iter_racikan' => $this->input->post('iter_racikan'),
		'status_racikan' => $this->input->post('status_racikan'),
		'st_perubahan' => ($st_manual_resep=='0'?2:0),
		'user_perubahan' => $login_ppa_id,
		'tanggal_perubahan' => date('Y-m-d H:i:s'),
	  );
	  $this->db->where('racikan_id',$racikan_id);
	  $hasil=$this->db->update('tpoliklinik_e_resep_obat_racikan',$data);
	  $q="SELECT SUM(CASE WHEN T.st_perubahan=0 THEN 0 ELSE 1 END) as st_perubahan FROM (
			SELECT H.assesmen_id, H.st_perubahan FROM tpoliklinik_e_resep_obat H WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT H.assesmen_id, H.st_perubahan FROM tpoliklinik_e_resep_obat_racikan_obat H WHERE H.assesmen_id='$assesmen_id'
			) T ";
		$st_perubahan=$this->db->query($q)->row('st_perubahan');
		// print_r($st_perubahan);exit;
		
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_e_resep',array('st_perubahan'=>$st_perubahan));
		$q="
			UPDATE tpoliklinik_e_resep_obat_racikan_obat U 
			SET U.totalharga=null WHERE U.racikan_id='$racikan_id'
		";
		$this->db->query($q);
		// // $q="
			// // UPDATE tpoliklinik_e_resep_obat_racikan H 
			// // INNER JOIN (
			// // SELECT racikan_id,SUM(totalharga) as totalharga FROM `tpoliklinik_e_resep_obat_racikan_obat` WHERE racikan_id='$racikan_id'
			// // ) T ON T.racikan_id=H.racikan_id
			// // SET H.totalharga=T.totalharga
		// // ";
		// // $this->db->query($q);
	  $this->output->set_output(json_encode($data));
    }
	function simpan_detail_racikan(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  
	  
	  $id=$this->input->post('id');
	  $data=array(
		'dosis' => $this->input->post('dosis'),
		'jumlah' => $this->input->post('jumlah'),
		'p1' => $this->input->post('p1'),
		'p2' => $this->input->post('p2'),
	  );
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_e_resep_obat_racikan_obat',$data);
	  
	  $this->output->set_output(json_encode($data));
    }
	function simpan_detail_racikan_telaah(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $id=$this->input->post('id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'idtipe' => $this->input->post('idtipe'),
		'idbarang' => $this->input->post('idbarang'),
		'nama' => $this->input->post('nama'),
		'dosis' => $this->input->post('dosis'),
		'dosis_master' => $this->input->post('dosis_master'),
		'jumlah' => $this->input->post('jumlah'),
		'harga_jual' => null,
		'p1' => $this->input->post('p1'),
		'p2' => $this->input->post('p2'),
		'alasan_perubahan' => $this->input->post('alasan_perubahan'),
		'st_perubahan' => 1,
		'user_perubahan' => $login_ppa_id,
		'tanggal_perubahan' => date('Y-m-d H:i:s'),
	  );
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('tpoliklinik_e_resep_obat_racikan_obat',$data);
	  $q="SELECT SUM(CASE WHEN T.st_perubahan=0 THEN 0 ELSE 1 END) as st_perubahan FROM (
			SELECT H.assesmen_id, H.st_perubahan FROM tpoliklinik_e_resep_obat H WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT H.assesmen_id, H.st_perubahan FROM tpoliklinik_e_resep_obat_racikan_obat H WHERE H.assesmen_id='$assesmen_id'
			) T ";
		$st_perubahan=$this->db->query($q)->row('st_perubahan');
		// print_r($st_perubahan);exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_e_resep',array('st_perubahan'=>$st_perubahan));
	  $this->output->set_output(json_encode($data));
    }
	function simpan_detail_racikan_telaah_add(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $id=$this->input->post('id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $data=array(
		'racikan_id' => $this->input->post('racikan_id'),
		'assesmen_id' => $this->input->post('assesmen_id'),
		'pendaftaran_id' => $this->input->post('pendaftaran_id'),
		'idpasien' => $this->input->post('idpasien'),
		'idtipe' => $this->input->post('idtipe'),
		'idbarang' => $this->input->post('idbarang'),
		'nama' => $this->input->post('nama'),
		'dosis' => $this->input->post('dosis'),
		'dosis_master' => $this->input->post('dosis_master'),
		'jumlah' => $this->input->post('jumlah'),
		'harga_jual' => null,
		'p1' => $this->input->post('p1'),
		'p2' => $this->input->post('p2'),
		'cover' => $this->input->post('st_cover'),
		'alasan_perubahan' => $this->input->post('alasan_perubahan'),
		'st_perubahan' => 1,
		'user_perubahan' => $login_ppa_id,
		'tanggal_perubahan' => date('Y-m-d H:i:s'),
	  );
	  // $this->db->where('id',$id);
	  $hasil=$this->db->insert('tpoliklinik_e_resep_obat_racikan_obat',$data);
	  $q="SELECT SUM(CASE WHEN T.st_perubahan=0 THEN 0 ELSE 1 END) as st_perubahan FROM (
			SELECT H.assesmen_id, H.st_perubahan FROM tpoliklinik_e_resep_obat H WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT H.assesmen_id, H.st_perubahan FROM tpoliklinik_e_resep_obat_racikan_obat H WHERE H.assesmen_id='$assesmen_id'
			) T ";
		$st_perubahan=$this->db->query($q)->row('st_perubahan');
		// print_r($st_perubahan);exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_e_resep',array('st_perubahan'=>$st_perubahan));
	  $this->output->set_output(json_encode($data));
    }
	function info_obat(){
		$idtipe=$this->input->post('idtipe');
		$idbarang=$this->input->post('idbarang');
		if ($idtipe=='3'){
			$q="SELECT H.id,H.nama,H.nama_generik,H.dosis,H.merk,H.high_alert,H.nama_industri,H.komposisi,H.indikasi,H.bentuk_sediaan,H.idtipe as jenis_penggunaan,H.gol_obat,H.idsatuankecil
				,MS.nama as nama_satuan

				FROM mdata_obat H
				LEFT JOIN msatuan MS ON MS.id=H.idsatuankecil
				WHERE H.id='$idbarang'";
		}
		if ($idtipe=='2'){
			$q="
			SELECT H.id,H.nama,H.nama_generik,null dosis,H.merk,null as high_alert,H.nama_industri,null as komposisi,null as indikasi,null as bentuk_sediaan,null as idtipe,null as jenis_penggunaan,null  as gol_obat,H.idsatuan,MS.nama as nama_satuan
			FROM mdata_implan H
			LEFT JOIN msatuan MS ON MS.id=H.idsatuan
			WHERE H.id='$idbarang'";
		}
		if ($idtipe=='1'){
			$q="
			SELECT H.id,H.nama,H.nama_generik,H.dosis,H.merk,H.high_alert,H.nama_industri,H.komposisi,H.indikasi,H.bentuk_sediaan,H.idtipe as jenis_penggunaan,H.gol_obat,H.idsatuankecil 
			,MS.nama as nama_satuan

			FROM mdata_alkes H
			LEFT JOIN msatuan MS ON MS.id=H.idsatuankecil
			WHERE H.id='$idbarang'
			";
		}
		$hasil=$this->db->query($q)->row_array();
		$this->output->set_output(json_encode($hasil));
	}
	function get_info_minum_obat(){
		$id=$this->input->post('id');
		$tipe=$this->input->post('tipe');
		if ($tipe=='1'){
			
		$q="SELECT waktu_minum FROM tpoliklinik_e_resep_obat WHERE id='$id'";
		}else{
		$q="SELECT waktu_minum FROM tpoliklinik_e_resep_obat_racikan WHERE racikan_id='$id'";
			
		}
		
		$hasil=$this->db->query($q)->row();
		$data['waktu_minum']=$hasil->waktu_minum;
		$this->output->set_output(json_encode($data));
	}
	function add_obat(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$idpasien=$this->input->post('idpasien');
		$data=array(
			'assesmen_id' => $assesmen_id,
			'pendaftaran_id' => $pendaftaran_id,
			'idpasien' => $idpasien,
			'idbarang' => $this->input->post('idbarang'),
			'idtipe' => $this->input->post('idtipe'),
			'nama' => $this->input->post('nama'),
			'jumlah' => $this->input->post('jumlah'),
			'dosis' => $this->input->post('dosis'),
			'interval' => $this->input->post('interval'),
			'rute' => $this->input->post('rute'),
			'aturan_tambahan' => $this->input->post('aturan_tambahan'),
			'iter' => $this->input->post('iter'),
			'cover' => $this->input->post('cover'),

			'created_ppa' => $login_ppa_id,
			'created_date' => date('Y-m-d H:i:s'),
		);
	  
		$hasil=$this->db->insert('tpoliklinik_e_resep_obat',$data);
		$this->output->set_output(json_encode($hasil));
  }
  function add_obat_edit(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$st_manual_resep=$this->input->post('st_manual_resep');
		$idpasien=$this->input->post('idpasien');
		$data=array(
			'assesmen_id' => $assesmen_id,
			'pendaftaran_id' => $pendaftaran_id,
			'idpasien' => $idpasien,
			'idbarang' => $this->input->post('idbarang'),
			'idtipe' => $this->input->post('idtipe'),
			'nama' => $this->input->post('nama'),
			'jumlah' => $this->input->post('jumlah'),
			'dosis' => $this->input->post('dosis'),
			'interval' => $this->input->post('interval'),
			'rute' => $this->input->post('rute'),
			'aturan_tambahan' => $this->input->post('aturan_tambahan'),
			'iter' => $this->input->post('iter'),
			'cover' => $this->input->post('cover'),

			'created_ppa' => $login_ppa_id,
			'created_date' => date('Y-m-d H:i:s'),
			'st_perubahan' => ($st_manual_resep=='0'?2:0),
			'user_perubahan' => $login_ppa_id,
			'tanggal_perubahan' => date('Y-m-d H:i:s'),
		);
	  
		$hasil=$this->db->insert('tpoliklinik_e_resep_obat',$data);
		$q="SELECT SUM(CASE WHEN T.st_perubahan=0 THEN 0 ELSE 1 END) as st_perubahan FROM (
			SELECT H.assesmen_id, H.st_perubahan FROM tpoliklinik_e_resep_obat H WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT H.assesmen_id, H.st_perubahan FROM tpoliklinik_e_resep_obat_racikan_obat H WHERE H.assesmen_id='$assesmen_id'
			) T ";
		$st_perubahan=$this->db->query($q)->row('st_perubahan');
		// print_r($st_perubahan);exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_e_resep',array('st_perubahan'=>$st_perubahan));
		$this->output->set_output(json_encode($hasil));
  }
  
  function add_obat_racikan(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$racikan_id=$this->input->post('racikan_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$idpasien=$this->input->post('idpasien');
		$data=array(
			'racikan_id' => $racikan_id,
			'assesmen_id' => $assesmen_id,
			'pendaftaran_id' => $pendaftaran_id,
			'idpasien' => $idpasien,
			'idbarang' => $this->input->post('idbarang'),
			'idtipe' => $this->input->post('idtipe'),
			'nama' => $this->input->post('nama'),
			'jumlah' => $this->input->post('jumlah'),
			'harga_jual' => null,
			'dosis' => $this->input->post('dosis'),
			'dosis_master' => $this->input->post('dosis_master'),
			'p1' => $this->input->post('p1'),
			'p2' => $this->input->post('p2'),
			'cover' => $this->input->post('cover'),
			'created_ppa' => $login_ppa_id,
			'created_date' => date('Y-m-d H:i:s'),
		);
	  
		$hasil=$this->db->insert('tpoliklinik_e_resep_obat_racikan_obat',$data);
		$this->output->set_output(json_encode($hasil));
  }
  function save_edit_non_racikan(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$transaksi_id=$this->input->post('id');
		$assesmen_id=$this->input->post('assesmen_id');
		
		$data=array(
			'idbarang' => $this->input->post('idbarang'),
			'idtipe' => $this->input->post('idtipe'),
			'nama' => $this->input->post('nama'),
			'cover' => $this->input->post('cover'),
			'jumlah' => $this->input->post('jumlah'),
			'dosis' => $this->input->post('dosis'),
			'interval' => $this->input->post('interval'),
			'rute' => $this->input->post('rute'),
			'aturan_tambahan' => $this->input->post('aturan_tambahan'),
			'iter' => $this->input->post('iter'),
			'alasan_perubahan' => $this->input->post('alasan_perubahan'),
			'st_perubahan' => 1,
			'harga_jual' => null,
			'stok_asal' => null,
			'user_perubahan' => $login_ppa_id,
			'tanggal_perubahan' => date('Y-m-d H:i:s'),
		);

		$this->db->where('id',$transaksi_id);
		$hasil=$this->db->update('tpoliklinik_e_resep_obat',$data);
		
		$this->update_stok($assesmen_id);
		
		$q="SELECT SUM(CASE WHEN T.st_perubahan=0 THEN 0 ELSE 1 END) as st_perubahan FROM (
			SELECT H.assesmen_id, H.st_perubahan FROM tpoliklinik_e_resep_obat H WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT H.assesmen_id, H.st_perubahan FROM tpoliklinik_e_resep_obat_racikan_obat H WHERE H.assesmen_id='$assesmen_id'
			) T ";
		$st_perubahan=$this->db->query($q)->row('st_perubahan');
		// print_r($st_perubahan);exit;
		$this->db->where('assesmen_id',$assesmen_id);
		$this->db->update('tpoliklinik_e_resep',array('st_perubahan'=>$st_perubahan));
		$this->output->set_output(json_encode($hasil));
  }
  function update_stok($assesmen_id){
		$q="
			UPDATE tpoliklinik_e_resep_obat M INNER JOIN (
				SELECT G.stok,H.* 
			FROM tpoliklinik_e_resep_obat H
			INNER JOIN tpoliklinik_e_resep M ON M.assesmen_id=H.assesmen_id
			LEFT JOIN mgudang_stok G ON G.idunitpelayanan=H.idunit AND G.idtipe=H.idtipe AND G.idbarang=H.idbarang 
			
			WHERE H.assesmen_id='$assesmen_id' AND M.st_ambil='0' AND M.status_assemen='2'
			) T ON T.id=M.id
			SET M.stok_asal=T.stok
		";
		$this->db->query($q);
	}
  function set_diambil(){
	  $user_id=$this->session->userdata('user_id');
	  $user_nama_login=$this->session->userdata('user_name');
		$tanggal_proses_resep= YMDFormat($this->input->post('tanggalproses_resep')). ' ' .$this->input->post('waktuproses_resep');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$st_ambil_sebagian=$this->input->post('st_ambil_sebagian');
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$q="
			SELECT H.pendaftaran_id,H.pendaftaran_id_ranap,H.asalrujukan FROM tpoliklinik_e_resep H WHERE H.assesmen_id='$assesmen_id'
		";
		$data_assemen=$this->db->query($q)->row();
		$asalrujukan=$data_assemen->asalrujukan;
		$pendaftaran_id_ranap=$data_assemen->pendaftaran_id_ranap;
		
		$nopenjualan= createKode('tpasien_penjualan', 'nopenjualan', 'FR');
		$q="SELECT SUM(1) as totalbarang,SUM(T.totalharga) as totalharga FROM (
			SELECT H.assesmen_id, H.totalharga FROM tpoliklinik_e_resep_obat H WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT H.assesmen_id, H.totalharga FROM tpoliklinik_e_resep_obat_racikan H WHERE H.assesmen_id='$assesmen_id'
			) T GROUP BY T.assesmen_id";
		$harga_data=$this->db->query($q)->row();
		$totalbarang=$harga_data->totalbarang;
		$totalharga=$harga_data->totalharga;
		//Create Tindakan
		if ($asalrujukan=='3'){
			$idtindakan=$pendaftaran_id_ranap;
			$q="
				INSERT INTO tpasien_penjualan 
				(tanggal,nopenjualan,asalrujukan,idtindakan,idkelompokpasien,idkategori,idpegawaiapoteker,idpasien,nomedrec,nama,alamat,notelp,tgl_lahir,umurhari,umurbulan,umurtahun,totalbarang,totalharga,wakturujukan,waktutindakan,waktupenyerahan,statusresep,statuspasien
				,st_rujukan_non_rujukan,closed_by_kasir,status,iddokter,pendaftaran_id,created_by,created_nama,created_date)

				SELECT '$tanggal_proses_resep','$nopenjualan' as nopenjualan,'$asalrujukan', '$idtindakan' idtindakan 
				,MP.idkelompokpasien,1 as idkategori,null idpegawaiapoteker,H.idpasien,MP.no_medrec as nomedrec
				,MP.namapasien as nama,MP.alamatpasien as alamat,MP.telepon as notelp,MP.tanggal_lahir as tgl_lahir
				,MP.umurhari,MP.umurbulan,MP.umurtahun,'$totalbarang' as totalbarang,'$totalharga' as totalharga,H.tanggal_kirim as wakturujukan,H.tanggal_proses_resep as waktutindakan
				,H.tanggal_penyerah as waktupenyerahan,1 as statusresep,1 as statuspasien,'R' as st_rujukan_non_rujukan,'0' as closed_by_kasir
				,2 as `status`,MP.iddokter,'$pendaftaran_id','$user_id','$user_nama_login',NOW()
				FROM tpoliklinik_e_resep H
				INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
				WHERE H.assesmen_id='$assesmen_id'
			";
		}else{
			$q="SELECT *FROM tpoliklinik_tindakan H WHERE H.idpendaftaran='$pendaftaran_id'";
			$data_tindakan=$this->db->query($q)->row();
			if ($data_tindakan){
				$idtindakan=$data_tindakan->id;
			}else{
				$q="INSERT INTO tpoliklinik_tindakan (idpendaftaran,iddokter1,rujukfarmasi,iduser_rujuk_farmasi,tanggal_rujuk_farmasi)
				SELECT H.pendaftaran_id,MP.iddokter,1 as rujukfarmasi,'' as iduser_rujuk_farmasi,H.tanggal_kirim as tanggal_rujuk_farmasi FROM tpoliklinik_e_resep H 
				LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
				WHERE H.assesmen_id='$assesmen_id'";
				$this->db->query($q);
				$idtindakan=$this->db->insert_id();
			}
			
			$q="
				INSERT INTO tpasien_penjualan 
				(tanggal,nopenjualan,asalrujukan,idtindakan,idkelompokpasien,idkategori,idpegawaiapoteker,idpasien,nomedrec,nama,alamat,notelp,tgl_lahir,umurhari,umurbulan,umurtahun,totalbarang,totalharga,wakturujukan,waktutindakan,waktupenyerahan,statusresep,statuspasien
				,st_rujukan_non_rujukan,closed_by_kasir,status,iddokter,pendaftaran_id,created_by,created_nama,created_date)

				SELECT '$tanggal_proses_resep','$nopenjualan' as nopenjualan,MP.idtipe, '$idtindakan' idtindakan 
				,MP.idkelompokpasien,1 as idkategori,null idpegawaiapoteker,H.idpasien,MP.no_medrec as nomedrec
				,MP.namapasien as nama,MP.alamatpasien as alamat,MP.telepon as notelp,MP.tanggal_lahir as tgl_lahir
				,MP.umurhari,MP.umurbulan,MP.umurtahun,'$totalbarang' as totalbarang,'$totalharga' as totalharga,H.tanggal_kirim as wakturujukan,H.tanggal_proses_resep as waktutindakan
				,H.tanggal_penyerah as waktupenyerahan,1 as statusresep,1 as statuspasien,'R' as st_rujukan_non_rujukan,'0' as closed_by_kasir
				,2 as `status`,MP.iddokter,'$pendaftaran_id','$user_id','$user_nama_login',NOW()
				FROM tpoliklinik_e_resep H
				INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
				WHERE H.assesmen_id='$assesmen_id'
			";
		}
		$this->db->query($q);
		$idpenjualan=$this->db->insert_id();
		// $q="
			// INSERT INTO tpasien_penjualan_nonracikan
			// (idpenjualan,idunit,idtipe,idbarang,carapakai,jenispenggunaan,waktu_minum,kuantitas,harga_dasar,margin,harga
			// ,diskon,diskon_rp,expire_date,tuslah,totalharga,status,statusverifikasi,kegunaan)
			// SELECT '$idpenjualan' as idpenjualan,MT.idunit,H.idtipe,H.idbarang,H.carapakai,H.jenispenggunaan,H.waktu_minum
			// ,H.kuantitas,H.harga_dasar,H.margin,H.harga_jual as harga
			// ,H.diskon,H.diskon_rp,H.expire_date,H.tuslah,H.totalharga,1 as `status`,0 as statusverifikasi,H.kegunaan
			// ,CONCAT(COALESCE(MI.nama,' '),' ',H.dosis,' ',SD.nama) as interval_dosis
			// FROM tpoliklinik_e_resep_obat H
			// LEFT JOIN tpoliklinik_e_resep H1 ON H1.assesmen_id=H.assesmen_id
			// LEFT JOIN minterval MI ON MI.id=H.`interval`
			// LEFT JOIN mtujuan_farmasi MT ON MT.id=H1.tujuan_farmasi_id
			// WHERE H.assesmen_id='$assesmen_id' AND H.kuantitas > 0
		// ";
		$q="
				INSERT INTO tpasien_penjualan_nonracikan
				(idpenjualan,idunit,idtipe,idbarang,carapakai,jenispenggunaan,waktu_minum,kuantitas,harga_dasar,margin,harga
				,diskon,diskon_rp,expire_date,tuslah,totalharga,status,statusverifikasi,kegunaan,interval_dosis,assesmen_det_id)
				SELECT '$idpenjualan' as idpenjualan,MT.idunit,H.idtipe,H.idbarang,H.carapakai,H.jenispenggunaan,H.waktu_minum
				,H.kuantitas,H.harga_dasar,H.margin,H.harga_jual as harga
				,H.diskon,H.diskon_rp,H.expire_date,H.tuslah,H.totalharga,1 as `status`,0 as statusverifikasi,H.kegunaan
				,CONCAT(COALESCE(MI.nama,' '),' ',H.dosis,' ',SD.nama) as interval_dosis,H.id
				FROM tpoliklinik_e_resep_obat H
				LEFT JOIN tpoliklinik_e_resep H1 ON H1.assesmen_id=H.assesmen_id
				LEFT JOIN view_barang_farmasi_resep B ON B.id=H.idbarang AND B.idtipe=H.idtipe
				LEFT JOIN minterval MI ON MI.id=H.`interval`
				LEFT JOIN mtujuan_farmasi MT ON MT.id=H1.tujuan_farmasi_id
				LEFT JOIN msatuan SD ON SD.id=B.idsatuandosis
				WHERE H.assesmen_id='$assesmen_id' AND H.kuantitas > 0
		";
		$this->db->query($q);
		
		$q="
			SELECT '$idpenjualan' as idpenjualan,H.nama_racikan as namaracikan,H.jenis_racikan as jenisracikan
			,MI.nama as carapakai,H.jenispenggunaan,H.waktu_minum,H.kuantitas,H.harga,H.tuslah,H.expire_date
			,H.totalharga,1 as `status`,0 as statusverifikasi,H.racikan_id,H.kegunaan,H.racikan_id
			FROM tpoliklinik_e_resep_obat_racikan H
			INNER JOIN minterval MI ON MI.id=H.interval_racikan
			WHERE H.assesmen_id='$assesmen_id' AND H.kuantitas > 0
		";
		
		$list_racikan_head=$this->db->query($q)->result();
		foreach($list_racikan_head as $r){
			$data_racikan=array(
				'idpenjualan' => $r->idpenjualan,
				'namaracikan' => $r->namaracikan,
				'jenisracikan' => $r->jenisracikan,
				'carapakai' => $r->carapakai,
				'jenispenggunaan' => $r->jenispenggunaan,
				'waktu_minum' => $r->waktu_minum,
				'kuantitas' => $r->kuantitas,
				'harga' => $r->harga,
				'tuslah' => $r->tuslah,
				'expire_date' => $r->expire_date,
				'totalharga' => $r->totalharga,
				'kegunaan' => $r->kegunaan,
				'assesmen_det_id' => $r->racikan_id,
				
				'status' => 1,
				'statusverifikasi' =>0,
				);
			$this->db->insert('tpasien_penjualan_racikan',$data_racikan);
			$idracikan=$this->db->insert_id();
			$racikan_id=$r->racikan_id;
			$q="SELECT 
			H1.tujuan_farmasi_id,MT.idunit
			,H.*
			FROM tpoliklinik_e_resep_obat_racikan_obat H
			INNER JOIN tpoliklinik_e_resep H1 ON H1.assesmen_id=H.assesmen_id
			INNER JOIN  mtujuan_farmasi MT ON MT.id=H1.tujuan_farmasi_id
			WHERE H.racikan_id='$racikan_id'";
			$list_detail=$this->db->query($q)->result();
			foreach ($list_detail as $row){
				$data_detail=array(
					'idracikan' => $idracikan,
					'idunit' => $row->idunit,
					'idtipe' => $row->idtipe,
					'idbarang' => $row->idbarang,
					'kuantitas' => $row->tqty,
					'harga' => $row->harga_jual,
					'diskon' => $row->diskon,
					'diskon_rp' => $row->diskon_rp,
					'totalharga' => $row->totalharga,
					'status' => 1,
					'harga_dasar' => $row->harga_dasar,
					'margin' => $row->margin
				);
				$this->db->insert('tpasien_penjualan_racikan_obat',$data_detail);
			}
			
		}
		
		
		$data=array(
			'st_ambil' => 1,
			'st_panggil' => 1,
			'st_proses_resep' => 1,
			'status_tindakan' => 2,
			'tanggal_proses_resep' => $tanggal_proses_resep,
			'user_proses' => $login_ppa_id,
			'idpenjualan' => $idpenjualan,
			'st_ambil_sebagian' => $st_ambil_sebagian,
			
		);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update('tpoliklinik_e_resep',$data);
		$this->output->set_output(json_encode($hasil));
  }
  function update_validasi(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$user_ambil=$this->input->post('user_ambil');
		$user_etiket=$this->input->post('user_etiket');
		$tanggal_proses_resep= YMDFormat($this->input->post('tanggalproses_resep')). ' ' .$this->input->post('waktuproses_resep');
		$data=array(
			'st_validasi' => 1,
			'status_tindakan' => 3,
			'tanggal_validasi' => date('Y-m-d H:i:s'),
			'user_validasi' => $login_ppa_id,
			'user_etiket' => $user_etiket,
			'user_ambil' => $user_ambil,
			
		);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update('tpoliklinik_e_resep',$data);
		$this->output->set_output(json_encode($hasil));
  }
  function setuju_obat(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$id=$this->input->post('id');
		$tipe=$this->input->post('tipe');
		$data=array(
			'st_setuju' => 1,
			'user_setuju' => $login_ppa_id,
			'tanggal_setuju' => date('Y-m-d H:i:s'),
			
		);
		if ($tipe=='1'){
			$this->db->where('id',$id);
			$hasil=$this->db->update('tpoliklinik_e_resep_obat',$data);
		}
		if ($tipe=='2'){
			$this->db->where('id',$id);
			$hasil=$this->db->update('tpoliklinik_e_resep_obat_racikan_obat',$data);
		}
		
		$this->output->set_output(json_encode($hasil));
  }
  function tolak_obat(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$id=$this->input->post('id');
		$tipe=$this->input->post('tipe');
		$data=array(
			'st_setuju' => 2,
			'user_setuju' => $login_ppa_id,
			'tanggal_setuju' => date('Y-m-d H:i:s'),
			
		);
		if ($tipe=='1'){
			$this->db->where('id',$id);
			$hasil=$this->db->update('tpoliklinik_e_resep_obat',$data);
		}
		if ($tipe=='2'){
			$this->db->where('id',$id);
			$hasil=$this->db->update('tpoliklinik_e_resep_obat_racikan_obat',$data);
		}
		
		$this->output->set_output(json_encode($hasil));
  }
  function update_serahkan(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$assesmen_id_resep=$this->input->post('assesmen_id_resep');
		$st_direct=$this->input->post('st_direct');
		$st_perubahan='0';
		
		
		
		//Pengambilan ITER
		if ($assesmen_id_resep){
			$q="UPDATE tpoliklinik_e_resep_obat H INNER JOIN (
				SELECT H.iditer_obat,H.ambil_iter FROM tpoliklinik_e_resep_obat H WHERE H.assesmen_id='$assesmen_id' AND H.ambil_iter='1'
				) T ON H.id=T.iditer_obat
				SET H.ambil_iter=H.ambil_iter+1";
			$this->db->query($q);
			$q="UPDATE tpoliklinik_e_resep_obat_racikan H INNER JOIN (
				SELECT H.iditer_obat,H.ambil_iter FROM tpoliklinik_e_resep_obat_racikan H WHERE H.assesmen_id='$assesmen_id' AND H.ambil_iter='1'
				) T ON H.racikan_id=T.iditer_obat
				SET H.ambil_iter=H.ambil_iter+1";
			$this->db->query($q);
			
			$q="
				SELECT SUM(H.ambil_iter) as ambil_iter  FROM (
					SELECT H.ambil_iter FROM tpoliklinik_e_resep_obat H WHERE H.assesmen_id='$assesmen_id_resep'
					UNION ALL
					SELECT H.ambil_iter as iter FROM tpoliklinik_e_resep_obat_racikan H WHERE H.assesmen_id='$assesmen_id_resep'
				) H 
			";
			$jumlah_iter_ambil=$this->db->query($q)->row('ambil_iter');
			// print_r($jumlah_iter_ambil);exit;
			if ($jumlah_iter_ambil){
				$q="UPDATE tpoliklinik_e_resep set jumlah_iter_ambil='$jumlah_iter_ambil' WHERE assesmen_id='$assesmen_id_resep'";
				$this->db->query($q);
			}
		}
		$tanggal_penyerah= YMDFormat($this->input->post('tanggalpenyerahan')). ' ' .$this->input->post('waktupenyerahan');
		$q="SELECT SUM(CASE WHEN T.st_perubahan=0 THEN 0 ELSE 1 END) as st_perubahan FROM (
			SELECT H.assesmen_id, H.st_perubahan FROM tpoliklinik_e_resep_obat H WHERE H.assesmen_id='$assesmen_id'
			UNION ALL
			SELECT H.assesmen_id, H.st_perubahan FROM tpoliklinik_e_resep_obat_racikan_obat H WHERE H.assesmen_id='$assesmen_id'
			) T ";
		$st_perubahan=$this->db->query($q)->row('st_perubahan');
		$data=array(
			'status_tindakan' => 4,
			'st_perubahan' => $st_perubahan,
			'tanggal_penyerah' =>$tanggal_penyerah,
			'user_penyerah' => $login_ppa_id,
			
		);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update('tpoliklinik_e_resep',$data);
		$q="UPDATE tpoliklinik_e_resep set st_ambil_sebagian='2' WHERE assesmen_id='$assesmen_id_resep' AND st_ambil_sebagian='1'";
		$this->db->query($q);
		
		$this->output->set_output(json_encode($hasil));
  }
  function set_tidak_diambil(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$alasan_tidak_diambil=$this->input->post('alasan_tidak_diambil');
		$tanggal_proses_resep= YMDFormat($this->input->post('tanggalproses_resep')). ' ' .$this->input->post('waktuproses_resep');
		$data=array(
			'st_ambil' => 2,
			'st_panggil' => 1,
			'alasan_tidak_diambil' => $alasan_tidak_diambil,
			'status_tindakan' => 5,
			'st_proses_resep' => 1,
			'tanggal_proses_resep' => $tanggal_proses_resep,
			'user_proses' => $login_ppa_id,
			
		);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update('tpoliklinik_e_resep',$data);
		$this->output->set_output(json_encode($hasil));
  }
  function update_record_obat(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$transaksi_id=$this->input->post('transaksi_id');
		
		$dosis=$this->input->post('dosis');
		$jumlah=$this->input->post('jumlah');
		$aturan_tambahan=$this->input->post('aturan_tambahan');
		$iter=$this->input->post('iter');
		$rute=$this->input->post('rute');
		$data=array(
			
			'jumlah' => $this->input->post('jumlah'),
			'dosis' => $this->input->post('dosis'),
			'interval' => $this->input->post('interval'),
			'rute' => $this->input->post('rute'),
			'aturan_tambahan' => $this->input->post('aturan_tambahan'),
			'iter' => $this->input->post('iter'),
		);
		$this->db->where('id',$transaksi_id);
		$hasil=$this->db->update('tpoliklinik_e_resep_obat',$data);
		$this->output->set_output(json_encode($hasil));
  }
  function update_minum_obat(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$id=$this->input->post('id');
		$arr_waktu=$this->input->post('arr_waktu');
		$waktu_minum=(json_encode($arr_waktu));;
		$data=array(
			'waktu_minum'=>$waktu_minum
		);
		
		$this->db->where('id',$id);
		$hasil=$this->db->update('tpoliklinik_e_resep_obat',$data);
		$this->output->set_output(json_encode($hasil));
  }
  function update_minum_obat_racikan(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$id=$this->input->post('id');
		$arr_waktu=$this->input->post('arr_waktu');
		$waktu_minum=(json_encode($arr_waktu));;
		$data=array(
			'waktu_minum'=>$waktu_minum
		);
		
		$this->db->where('racikan_id',$id);
		$hasil=$this->db->update('tpoliklinik_e_resep_obat_racikan',$data);
		$this->output->set_output(json_encode($hasil));
  }
  function update_record_review_obat(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$transaksi_id=$this->input->post('transaksi_id');
		
		$data=array(
			'jenispenggunaan' => $this->input->post('jenispenggunaan'),
			'kuantitas' => $this->input->post('kuantitas'),
			'harga_dasar' => $this->input->post('harga_dasar'),
			'margin' => $this->input->post('margin'),
			'harga_jual' => $this->input->post('harga_jual'),
			'diskon' => $this->input->post('diskon'),
			'diskon_rp' => $this->input->post('diskon_rp'),
			'expire_date' => ($this->input->post('expire_date')?YMDFormat($this->input->post('expire_date')):null),
			'tuslah' => $this->input->post('tuslah'),
			'totalharga' => $this->input->post('totalharga'),
			'kegunaan' => $this->input->post('kegunaan'),

		);
		// print_r($data);exit;
		$this->db->where('id',$transaksi_id);
		$hasil=$this->db->update('tpoliklinik_e_resep_obat',$data);
		$this->output->set_output(json_encode($hasil));
  }
  function update_record_review_obat_racikan(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$transaksi_id=$this->input->post('transaksi_id');
		
		$data=array(
			'jenispenggunaan' => $this->input->post('jenispenggunaan'),
			'kuantitas' => $this->input->post('kuantitas'),
			// 'harga' => $this->input->post('harga_jual'),
			'expire_date' => ($this->input->post('expire_date')?YMDFormat($this->input->post('expire_date')):null),
			'tuslah' => $this->input->post('tuslah'),
			'totalharga' => $this->input->post('totalharga'),
			'kegunaan' => $this->input->post('kegunaan'),

		);
		// print_r($data);exit;
		$this->db->where('racikan_id',$transaksi_id);
		$hasil=$this->db->update('tpoliklinik_e_resep_obat_racikan',$data);
		
		$this->db->where('racikan_id',$transaksi_id);
		$this->db->update('tpoliklinik_e_resep_obat_racikan_obat',array('totalharga'=>null));
		$this->output->set_output(json_encode($hasil));
  }
  function update_jawaban_resep(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$id=$this->input->post('id');
		
		$data=array(
			'jawaban_resep' => $this->input->post('jawaban_resep'),
			'edited_date' => date('Y-m-d H:i:s'),
			'edited_ppa' => $login_ppa_id,
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('tpoliklinik_e_resep_telaah',$data);
		$this->output->set_output(json_encode($hasil));
  }
  function update_jawaban_obat(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$id=$this->input->post('id');
		
		$data=array(
			'jawaban_obat' => $this->input->post('jawaban_obat'),
			'edited_date' => date('Y-m-d H:i:s'),
			'edited_ppa' => $login_ppa_id,
		);
		// print_r($id);exit;
		$this->db->where('id',$id);
		$hasil=$this->db->update('tpoliklinik_e_resep_telaah',$data);
		$this->output->set_output(json_encode($hasil));
  }
  function simpan_telaah(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$q="
			INSERT INTO tpoliklinik_e_resep_obat_original
			(id,assesmen_id,pendaftaran_id,idpasien,idbarang,idtipe,nama,dosis,jumlah,`interval`,rute,aturan_tambahan,iter,cover,created_ppa,created_date,paket_id,paket_id_detail,template_id,template_id_detail,asal_data_id,st_proses)
			SELECT id,assesmen_id,pendaftaran_id,idpasien,idbarang,idtipe,nama,dosis,jumlah,`interval`,rute,aturan_tambahan,iter,cover,created_ppa,created_date,paket_id,paket_id_detail,template_id,template_id_detail,asal_data_id,st_proses
			FROM tpoliklinik_e_resep_obat WHERE assesmen_id='$assesmen_id'

		";
		$this->db->query($q);
		
		$q="
			INSERT INTO tpoliklinik_e_resep_obat_racikan_original 
			(racikan_id,assesmen_id,pendaftaran_id,idpasien,nama_racikan,jumlah_racikan,jenis_racikan,interval_racikan,rute_racikan,aturan_tambahan_racikan,iter_racikan,status_racikan,created_ppa,created_date)
			SELECT 
			racikan_id,assesmen_id,pendaftaran_id,idpasien,nama_racikan,jumlah_racikan,jenis_racikan,interval_racikan,rute_racikan,aturan_tambahan_racikan,iter_racikan,status_racikan,created_ppa,created_date
			FROM tpoliklinik_e_resep_obat_racikan WHERE assesmen_id='$assesmen_id'
		";
		$this->db->query($q);
		
		$q="INSERT INTO tpoliklinik_e_resep_obat_racikan_obat_original
			(id,racikan_id,assesmen_id,pendaftaran_id,idpasien,idbarang,idtipe,nama,dosis_master,dosis,jumlah,p1,p2,cover,created_ppa,created_date)
			SELECT 
			id,racikan_id,assesmen_id,pendaftaran_id,idpasien,idbarang,idtipe,nama,dosis_master,dosis,jumlah,p1,p2,cover,created_ppa,created_date
			FROM tpoliklinik_e_resep_obat_racikan_obat WHERE assesmen_id='$assesmen_id'";
		$this->db->query($q);
		
		$data=array(
			'status_tindakan' => 1,
			'st_telaah_resep' => 1,
			'tanggal_telaah_resep' => date('Y-m-d H:i:s'),
			'user_telaah_resep' => $login_ppa_id,
		);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update('tpoliklinik_e_resep',$data);
		$this->output->set_output(json_encode($hasil));
  }
  
  function terapkan_iter_all(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$iter=$this->input->post('iter');
		
		$data=array(
			
			'iter' => $this->input->post('iter'),
			
		);
		$this->db->where('assesmen_id',$assesmen_id);
		$hasil=$this->db->update('tpoliklinik_e_resep_obat',$data);
		$this->output->set_output(json_encode($hasil));
  }
	function list_index_obat(){
		
		$btn_disabel_edit='';
		$max_iter=$this->input->post('max_iter');
		$assesmen_id=$this->input->post('assesmen_id');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$tujuan_id=$this->input->post('tujuan_id');
		
		// $q="SELECT idunit FROM mtujuan_farmasi WHERE id='$tujuan_id'";
		// $idunit=$this->db->query($q)->row('idunit');
		$this->select = array();
		$from="
			(
				SELECT H.*,B.nama as nama_barang,B.hargadasar
				,BS.ref as sediaan_nama
				,MR.ref as nama_rute
				,MI.nama as nama_interval,G.stok
				,(CASE 
					WHEN $idkelompokpasien=5 THEN ((B.marginumum/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=4 THEN ((B.marginbpjstenagakerja/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=3 THEN ((B.marginbpjskesehatan/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=2 THEN ((B.marginjasaraharja/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=1 THEN ((B.marginasuransi/100) + 1) * B.hargadasar 
					END) as harga
				FROM tpoliklinik_e_resep_obat H 
				LEFT JOIN view_barang_farmasi_resep B ON B.id=H.idbarang AND B.idtipe=H.idtipe
				LEFT JOIN merm_referensi BS ON BS.nilai=B.bentuk_sediaan AND BS.ref_head_id='94'
				LEFT JOIN merm_referensi MR ON MR.nilai=H.rute AND MR.ref_head_id='74'
				LEFT JOIN minterval MI ON MI.id=H.interval 
				LEFT JOIN mgudang_stok G ON G.idunitpelayanan=H.idunit AND G.idtipe=H.idtipe AND G.idbarang=H.idbarang 
				WHERE H.assesmen_id='$assesmen_id'
				ORDER BY H.id ASC
			) as tbl
		";
		$this->from   = $from;
		$this->join 	= array();$this->order  = array();$this->group  = array();
		$this->column_search = array('nama');$this->column_order  = array();
		$list = $this->datatable->get_datatables(true);
		$data = array();$no = $_POST['start'];
		$data_interval=get_all('minterval');
		$data_rute=list_variable_ref(74);
		foreach ($list as $r) {	
			$no++;$result = array();
			$btn_info='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_info_obat('.$r->idtipe.','.$r->idbarang.')" type="button" data-toggle="tooltip" title="Informasi Obat" data-original-title="Informasi Obat"><i class="fa fa-info"></i></button>';
			$input='<input type="hidden" class="cls_idtipe" value="'.$r->idtipe.'">';
			$input .='<input type="hidden" class="cls_idobat" value="'.$r->idbarang.'">';
			$input .='<input type="hidden" class="cls_idtrx" value="'.$r->id.'">';
			$aksi='';
			$aksi = '<div class="btn-group">';
			$aksi .= '<button  onclick="hapus_obat('.$r->id.')" type="button" title="Hapus" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			$aksi .= '</div>';
			$result[] = ($no);
			$result[] = '<span>'.($r->nama_barang).$btn_info.'</span><br><span class="text-primary">Sediaan : '.($r->sediaan_nama?$r->sediaan_nama:'-').' | Rp. '.number_format(ceiling($r->harga,100)).' </span>'.status_cover($r->cover).' '.status_stok($r->stok,$r->jumlah);
			$result[] = '<input type="text" class="dosis_nr number cls_racikan_update text-center" value="'.$r->dosis.'">';
			$result[] = '<input type="text" class="jumlah_nr number cls_racikan_update text-center" value="'.$r->jumlah.'">';
			$result[] = $this->opsi_header($data_interval,$r->interval,'interval');
			$result[] = $this->opsi_header($data_rute,$r->rute,'rute');
			$result[] = '<input type="text" class="aturan_tambahan_nr cls_racikan_update text-center" value="'.$r->aturan_tambahan.'">';
			$result[] = (opsi_iter_record($max_iter,$r->iter,'','iter'));
			$result[] = $aksi.$input;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
	}
	function list_index_obat_telaah(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$btn_disabel_edit='';
		$tab=$this->input->post('tab');
		$where='';
		if ($tab>=4){
			$where .=" AND H.kuantitas > 0";
		}
		$max_iter=$this->input->post('max_iter');
		$assesmen_id=$this->input->post('assesmen_id');
		$this->update_stok($assesmen_id);
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$this->select = array();
		$from="
			(
				SELECT H.*,B.nama as nama_barang,B.hargadasar
				,BS.ref as sediaan_nama
				,MR.ref as nama_rute
				,MI.nama as nama_interval
				,(CASE 
					WHEN $idkelompokpasien=5 THEN ((B.marginumum/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=4 THEN ((B.marginbpjstenagakerja/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=3 THEN ((B.marginbpjskesehatan/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=2 THEN ((B.marginjasaraharja/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=1 THEN ((B.marginasuransi/100) + 1) * B.hargadasar 
					END) as harga
					,UE.nama as nama_user_perubahan
				FROM tpoliklinik_e_resep_obat H 
				LEFT JOIN view_barang_farmasi_resep B ON B.id=H.idbarang AND B.idtipe=H.idtipe
				LEFT JOIN merm_referensi BS ON BS.nilai=B.bentuk_sediaan AND BS.ref_head_id='94'
				LEFT JOIN merm_referensi MR ON MR.nilai=H.rute AND MR.ref_head_id='74'
				LEFT JOIN minterval MI ON MI.id=H.interval 
				LEFT JOIN mppa UE ON UE.id=H.user_perubahan
				WHERE H.assesmen_id='$assesmen_id' ".$where."
				ORDER BY H.id ASC
			) as tbl
		";
		$this->from   = $from;
		// print_r($from);exit;
		$this->join 	= array();$this->order  = array();$this->group  = array();
		$this->column_search = array('nama');$this->column_order  = array();
		$list = $this->datatable->get_datatables(true);
		$data = array();$no = $_POST['start'];
		// $data_interval=get_all('minterval');
		// $data_rute=list_variable_ref(74);
		foreach ($list as $r) {	
		$btn_edit='';
		$btn_hapus='';
			$no++;
			$result = array();
			$btn_info='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_info_obat('.$r->idtipe.','.$r->idbarang.')" type="button" data-toggle="tooltip" title="Informasi Obat" data-original-title="Informasi Obat"><i class="fa fa-info"></i></button>';
			if (UserAccesForm($user_acces_form,array('1839'))){
				$btn_edit='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_edit_obat('.$r->id.')" type="button" data-toggle="tooltip" title="Edit Obat"><i class="fa fa-pencil text-danger"></i></button>';
			}
			$input='<input type="hidden" class="cls_idtipe" value="'.$r->idtipe.'">';
			$input .='<input type="hidden" class="cls_idobat" value="'.$r->idbarang.'">';
			$input .='<input type="hidden" class="cls_idtrx" value="'.$r->id.'">';
			$aksi='';
			if (UserAccesForm($user_acces_form,array('1841'))){
			$btn_hapus = '<button  onclick="hapus_obat('.$r->id.')" type="button" title="Hapus" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			}
			$label_perubahan='';
			if ($r->st_setuju=='0'){
				$label_setuju='<button onclick="lihat_perubahan_obat('.$r->id.')" type="button" class="btn btn-default btn-xs btn_lihat_perubahan"><i class="fa fa-user"></i> MENUNGGU</button>';
			}
			if ($r->st_setuju=='1'){
				$label_setuju='<button onclick="lihat_perubahan_obat('.$r->id.')" type="button" class="btn btn-primary btn-xs btn_lihat_perubahan"><i class="fa fa-user"></i> SETUJU</button>';
			}
			if ($r->st_setuju=='2'){
				$label_setuju='<button onclick="lihat_perubahan_obat('.$r->id.')" type="button" class="btn btn-danger btn-xs btn_lihat_perubahan"><i class="fa fa-times"></i> DITOLAK</button>';
			}
			if ($r->st_perubahan!='0'){
				$aksi .= $label_setuju;	
				$aksi .= '<br><br><button onclick="lihat_perubahan_obat('.$r->id.')" type="button" class="btn btn-'.($r->st_perubahan=='1'?'warning':'success').' btn-xs btn_lihat_perubahan">'.($r->st_perubahan=='1'?'PERUBAHAN':'PENAMBAHAN').'</button>';	
				$label_perubahan='<br><span class="text-danger"><strong>Perubahan : '.$r->nama_user_perubahan.' | '.HumanDateLong($r->tanggal_perubahan).'</strong></span>';
			}
			if ($tab=='1'){
				$btn_edit='';
				$btn_hapus='';
			}
			$result[] = ($no);
			$result[] = '<span>'.($r->nama_barang).$btn_info.'</span><br><span class="text-primary">Sediaan : '.($r->sediaan_nama?$r->sediaan_nama:'-').' | Rp. '.number_format(ceiling($r->harga,100)).' </span>'.status_cover($r->cover).' '.status_stok($r->stok_asal,$r->jumlah).$btn_edit.$btn_hapus.$label_perubahan;
			$result[] = $r->dosis.$input;
			$result[] = $r->jumlah;
			$result[] = $r->nama_interval;
			$result[] = $r->nama_rute;
			$result[] = $r->aturan_tambahan;
			$result[] = $r->iter;
			$result[] = $aksi;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
	}
	function list_index_obat_telaah_perubahan(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$btn_disabel_edit='';
		$tab=$this->input->post('tab');
		$max_iter=$this->input->post('max_iter');
		$assesmen_id=$this->input->post('assesmen_id');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$this->select = array();
		$from="
			(
				SELECT H.*,B.nama as nama_barang,B.hargadasar
				,BS.ref as sediaan_nama
				,MR.ref as nama_rute
				,MI.nama as nama_interval
				,(CASE 
					WHEN $idkelompokpasien=5 THEN ((B.marginumum/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=4 THEN ((B.marginbpjstenagakerja/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=3 THEN ((B.marginbpjskesehatan/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=2 THEN ((B.marginjasaraharja/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=1 THEN ((B.marginasuransi/100) + 1) * B.hargadasar 
					END) as harga
					,UE.nama as nama_user_perubahan
				FROM tpoliklinik_e_resep_obat H 
				LEFT JOIN view_barang_farmasi_resep B ON B.id=H.idbarang AND B.idtipe=H.idtipe
				LEFT JOIN merm_referensi BS ON BS.nilai=B.bentuk_sediaan AND BS.ref_head_id='94'
				LEFT JOIN merm_referensi MR ON MR.nilai=H.rute AND MR.ref_head_id='74'
				LEFT JOIN minterval MI ON MI.id=H.interval 
				LEFT JOIN mppa UE ON UE.id=H.user_perubahan
				WHERE H.assesmen_id='$assesmen_id'
				ORDER BY H.id ASC
			) as tbl
		";
		$this->from   = $from;
		
		$this->join 	= array();$this->order  = array();$this->group  = array();
		$this->column_search = array('nama');$this->column_order  = array();
		$list = $this->datatable->get_datatables(true);
		$data = array();$no = $_POST['start'];
		// $data_interval=get_all('minterval');
		// $data_rute=list_variable_ref(74);
		foreach ($list as $r) {	
		$btn_edit='';
		$btn_hapus='';
			$no++;$result = array();
			$btn_info='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_info_obat('.$r->idtipe.','.$r->idbarang.')" type="button" data-toggle="tooltip" title="Informasi Obat" data-original-title="Informasi Obat"><i class="fa fa-info"></i></button>';
			if (UserAccesForm($user_acces_form,array('1839'))){
				$btn_edit='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_edit_obat('.$r->id.')" type="button" data-toggle="tooltip" title="Edit Obat"><i class="fa fa-pencil text-danger"></i></button>';
			}
			$input='<input type="hidden" class="cls_idtipe" value="'.$r->idtipe.'">';
			$input .='<input type="hidden" class="cls_idobat" value="'.$r->idbarang.'">';
			$input .='<input type="hidden" class="cls_idtrx" value="'.$r->id.'">';
			$aksi='';
			if (UserAccesForm($user_acces_form,array('1841'))){
			$btn_hapus = '<button  onclick="hapus_obat('.$r->id.')" type="button" title="Hapus" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			}
			$label_perubahan='';
			if ($r->st_setuju=='0'){
				$label_setuju='<button onclick="lihat_perubahan_obat('.$r->id.')" type="button" class="btn btn-default btn-xs btn_lihat_perubahan"><i class="fa fa-user"></i> MENUNGGU</button>';
			}
			if ($r->st_setuju=='1'){
				$label_setuju='<button onclick="lihat_perubahan_obat('.$r->id.')" type="button" class="btn btn-primary btn-xs btn_lihat_perubahan"><i class="fa fa-user"></i> SETUJU</button>';
			}
			if ($r->st_setuju=='2'){
				$label_setuju='<button onclick="lihat_perubahan_obat('.$r->id.')" type="button" class="btn btn-danger btn-xs btn_lihat_perubahan"><i class="fa fa-times"></i> DITOLAK</button>';
			}
			if ($r->st_perubahan!='0'){
				$aksi .= $label_setuju;	
				$aksi .= '<br><br><button onclick="lihat_perubahan_obat('.$r->id.')" type="button" class="btn btn-'.($r->st_perubahan=='1'?'warning':'success').' btn-xs btn_lihat_perubahan">'.($r->st_perubahan=='1'?'PERUBAHAN':'PENAMBAHAN').'</button>';	
				$label_perubahan='<br><span class="text-danger"><strong>Perubahan : '.$r->nama_user_perubahan.' | '.HumanDateLong($r->tanggal_perubahan).'</strong></span>';
			}
			if ($tab=='1'){
				$btn_edit='';
				$btn_hapus='';
			}
			$result[] = ($no);
			$result[] = '<span>'.($r->nama_barang).$btn_info.'</span><br><span class="text-primary">Sediaan : '.($r->sediaan_nama?$r->sediaan_nama:'-').' | Rp. '.number_format(ceiling($r->harga,100)).' </span>'.status_cover($r->cover).$btn_edit.$btn_hapus.$label_perubahan;
			$result[] = $r->dosis.$input;
			$result[] = $r->jumlah;
			$result[] = $r->nama_interval;
			$result[] = $r->nama_rute;
			$result[] = $r->aturan_tambahan;
			$result[] = $r->iter;
			$result[] = $aksi;
			$aksi_persetujuan=($r->st_perubahan=='0'?'':'
				<div class="btn-group btn-group-xs" role="group">
					<button class="btn btn-success btn_persetujuan" type="button" onclick="setuju_obat('.$r->id.',1)" title="Setuju"><i class="fa fa-check"></i> SETUJU</button>
					<button class="btn btn-danger btn_persetujuan" type="button" onclick="tolak_obat('.$r->id.',1)" title="Tolak"><i class="si si-ban" ></i> TOLAK</button>
				</div>');
			$result[] = ($r->st_setuju==0?$aksi_persetujuan:$label_setuju);
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
	}
	function lihat_perubahan_obat(){
		$id=$this->input->post('id');
		$q="
			SELECT C.nama as nama_current,C.dosis as dosis_current,C.jumlah as jumlah_current,C.`interval` as interval_current,C.rute as rute_current,C.aturan_tambahan as aturan_tambahan_current
			,MRC.ref as nama_rute_current,MIC.nama as nama_interval_current
			,MRO.ref as nama_rute_original,MIO.nama as nama_interval_original
			,UE.nama as nama_pengaju,UE.nip as nip_pengaju,C.alasan_perubahan
			,US.nama as nama_setuju,US.nip as nip_setuju
			,C.user_perubahan,C.user_setuju
			,O.nama as nama_original,O.dosis as dosis_original,O.jumlah as jumlah_original,O.`interval` as interval_original,O.rute as rute_original,O.aturan_tambahan as aturan_tambahan_original
			,C.st_setuju,C.st_perubahan
			FROM `tpoliklinik_e_resep_obat` C 
			LEFT JOIN tpoliklinik_e_resep_obat_original O ON C.id=O.id
			LEFT JOIN mppa UE ON UE.id=C.user_perubahan
			LEFT JOIN mppa US ON US.id=C.user_setuju
			LEFT JOIN merm_referensi MRC ON MRC.nilai=C.rute AND MRC.ref_head_id='74'
			LEFT JOIN minterval MIC ON MIC.id=C.`interval` 
			LEFT JOIN merm_referensi MRO ON MRO.nilai=O.rute AND MRO.ref_head_id='74'
			LEFT JOIN minterval MIO ON MIO.id=O.`interval` 
			WHERE C.id='$id'
		";
		$data=$this->db->query($q)->row();
		$tabel_1='';
		if ($data){
			$tabel_1 .='<tr>';
			$tabel_1 .='<td>'.$data->nama_original.'</td>';
			$tabel_1 .='<td>'.$data->dosis_original.'</td>';
			$tabel_1 .='<td>'.$data->jumlah_original.'</td>';
			$tabel_1 .='<td>'.$data->nama_interval_original.'</td>';
			$tabel_1 .='<td>'.$data->nama_rute_original.'</td>';
			$tabel_1 .='<td>'.$data->aturan_tambahan_original.'</td>';
			$tabel_1 .='</tr>';
		}
		$tabel_2='';
		if ($data){
			$tabel_2 .='<tr>';
			$tabel_2 .='<td class="'.($data->nama_original<>$data->nama_current?'text-danger':'').'"><strong>'.$data->nama_current.'</strong></td>';
			$tabel_2 .='<td class="'.($data->dosis_original<>$data->dosis_current?'text-danger':'').'"><strong>'.$data->dosis_current.'</strong></td>';
			$tabel_2 .='<td class="'.($data->jumlah_original<>$data->jumlah_current?'text-danger':'').'"><strong>'.$data->jumlah_current.'</strong></td>';
			$tabel_2 .='<td class="'.($data->nama_interval_original<>$data->nama_interval_current?'text-danger':'').'"><strong>'.$data->nama_interval_current.'</strong></td>';
			$tabel_2 .='<td class="'.($data->nama_rute_original<>$data->nama_rute_current?'text-danger':'').'"><strong>'.$data->nama_rute_current.'</strong></td>';
			$tabel_2 .='<td class="'.($data->aturan_tambahan_original<>$data->aturan_tambahan_current?'text-danger':'').'"><strong>'.$data->aturan_tambahan_current.'</strong></td>';
			$tabel_2 .='</tr>';
		}
		$nama_setuju=$data->nama_setuju.'<br>'.$data->nip_setuju;
		$nama_pengaju=$data->nama_pengaju.'-'.$data->nip_pengaju;
		$ttd_setuju='';
		$ttd_pengaju='';
		if ($data->user_setuju){
			$ttd_setuju='<img class="" style="width:100px;height:100px; text-align: center;" src="'.base_url().'qrcode/qr_code_ttd/'.$data->user_setuju.'" alt="" title="">';
		}
		if ($data->user_perubahan){
			$ttd_pengaju='<img style="width:100px;height:100px; text-align: center;" src="'.base_url().'qrcode/qr_code_ttd/'.$data->user_perubahan.'" alt="" title="">';
		}
		$tabel_3='<tr>';
		$tabel_3 .='<td>';
		$tabel_3 .='<div class="block-content block-content-full text-center">
					<div>
						'.$ttd_pengaju.'
					</div>
					<div class="h5 push-15-t push-5">'.($data->user_perubahan?$data->nama_pengaju:'').'</div>
					<div class="text-muted">'.($data->user_perubahan?$data->nip_pengaju:'').'</div>
				</div>';
		$tabel_3 .='</td><td></td><td>';
		$tabel_3 .='<div class="block-content block-content-full text-center">
					<div>
						'.$ttd_setuju.'
					</div>
					<div class="h5 push-15-t push-5">'.($data->nama_setuju?$data->nama_setuju:'').'</div>
					<div class="text-muted">'.($data->nama_setuju?$data->nip_setuju:'').'</div>
				</div></td></tr>';
		// if ($data){
			// $tabel_3 .='<tr>';
			// $tabel_3 .='<td class="text-left"><img class="img-avatar" style="width:100px;height:100px; text-align: center;" src="'.base_url().'qrcode/qr_code_ttd/'.$data->user_perubahan.'" alt="" title=""></td>';
			// $tabel_3 .='<td>&nbsp;</td>';
			// $tabel_3 .='<td>'.$ttd_setuju.'</td>';
			// $tabel_3 .='</tr>';
			// $tabel_3 .='<tr>';
			// $tabel_3 .='<td class="text-left">'.$data->nama_pengaju.'<br>'.$data->nip_pengaju.'</td>';
			// $tabel_3 .='<td></td>';
			// $tabel_3 .='<td class="text-right">'.$data->nama_setuju.'<br>'.$data->nip_setuju.'</td>';
			// $tabel_3 .='</tr>';
		// }
		if ($data->st_setuju=='0'){
			$label_perubahan='MENUNGGU';
		}
		if ($data->st_setuju=='1'){
			$label_perubahan='DISETUJUI';
		}
		if ($data->st_setuju=='2'){
			$label_perubahan='DITOLAK';
		}
		$data_obat['tabel_1']=$tabel_1;
		$data_obat['tabel_2']=$tabel_2;
		$data_obat['tabel_3']=$tabel_3;
		$data_obat['label_perubahan']=$label_perubahan;
		$data_obat['alasan_perubahan']=$data->alasan_perubahan;
		
		$this->output->set_output(json_encode($data_obat));
	}
	function lihat_perubahan_obat_racikan(){
		$id=$this->input->post('id');
		$q="
			SELECT 
			H.jumlah_racikan,
			C.nama as nama_current,C.dosis_master as dosis_master_current,C.dosis as dosis_current,C.jumlah as jumlah_current,C.p1 as p1_current,C.p2 as p2_current 
			,O.nama as nama_original,O.dosis_master as dosis_master_original,O.dosis as dosis_original,O.jumlah as jumlah_original,O.p1 as p1_original,O.p2 as p2_original
			,UE.nama as nama_pengaju,UE.nip as nip_pengaju,C.alasan_perubahan
			,US.nama as nama_setuju,US.nip as nip_setuju
			,C.user_perubahan,C.user_setuju,C.st_setuju,C.st_perubahan
			,(C.p1/C.p2 * H.jumlah_racikan) as total_current
			,(O.p1/O.p2 * H.jumlah_racikan) as total_original
			FROM tpoliklinik_e_resep_obat_racikan_obat C
			INNER JOIN tpoliklinik_e_resep_obat_racikan H ON H.racikan_id=C.racikan_id
			LEFT JOIN tpoliklinik_e_resep_obat_racikan_obat_original O ON O.id=C.id
			LEFT JOIN mppa UE ON UE.id=C.user_perubahan
			LEFT JOIN mppa US ON US.id=C.user_setuju
			WHERE C.id='$id'
		";
		$data=$this->db->query($q)->row();
		$tabel_1='';
		if ($data){
			$t_qty_original=ceiling($data->total_original * $data->jumlah_racikan,1);
			$t_qty_current=ceiling($data->total_current * $data->jumlah_racikan,1);
			$tabel_1 .='<tr>';
			$tabel_1 .='<td>'.$data->nama_original.'</td>';
			$tabel_1 .='<td>'.$data->dosis_original.'</td>';
			$tabel_1 .='<td>'.$data->jumlah_original.'</td>';
			$tabel_1 .='<td>'.$data->p1_original.'</td>';
			$tabel_1 .='<td>'.$data->p2_original.'</td>';
			$tabel_1 .='<td>'.$t_qty_original.'</td>';
			$tabel_1 .='</tr>';
		}
		$tabel_2='';
		if ($data){
			$tabel_2 .='<tr>';
			$tabel_2 .='<td class="'.($data->nama_original<>$data->nama_current?'text-danger':'').'"><strong>'.$data->nama_current.'</strong></td>';
			$tabel_2 .='<td class="'.($data->dosis_original<>$data->dosis_current?'text-danger':'').'"><strong>'.$data->dosis_current.'</strong></td>';
			$tabel_2 .='<td class="'.($data->jumlah_original<>$data->jumlah_current?'text-danger':'').'"><strong>'.$data->jumlah_current.'</strong></td>';
			$tabel_2 .='<td class="'.($data->p1_original<>$data->p1_current?'text-danger':'').'"><strong>'.$data->p1_current.'</strong></td>';
			$tabel_2 .='<td class="'.($data->p2_original<>$data->p2_current?'text-danger':'').'"><strong>'.$data->p2_current.'</strong></td>';
			$tabel_2 .='<td class="'.($t_qty_original<>$t_qty_current?'text-danger':'').'"><strong>'.$t_qty_current.'</strong></td>';
			$tabel_2 .='</tr>';
		}
		$nama_setuju=$data->nama_setuju.'<br>'.$data->nip_setuju;
		$nama_pengaju=$data->nama_pengaju.'-'.$data->nip_pengaju;
		$ttd_setuju='';
		$ttd_pengaju='';
		if ($data->user_setuju){
			$ttd_setuju='<img class="" style="width:100px;height:100px; text-align: center;" src="'.base_url().'qrcode/qr_code_ttd/'.$data->user_setuju.'" alt="" title="">';
		}
		if ($data->user_perubahan){
			$ttd_pengaju='<img style="width:100px;height:100px; text-align: center;" src="'.base_url().'qrcode/qr_code_ttd/'.$data->user_perubahan.'" alt="" title="">';
		}
		$tabel_3='<tr>';
		$tabel_3 .='<td>';
		$tabel_3 .='<div class="block-content block-content-full text-center">
					<div>
						'.$ttd_pengaju.'
					</div>
					<div class="h5 push-15-t push-5">'.($data->user_perubahan?$data->nama_pengaju:'').'</div>
					<div class="text-muted">'.($data->user_perubahan?$data->nip_pengaju:'').'</div>
				</div>';
		$tabel_3 .='</td><td></td><td>';
		$tabel_3 .='<div class="block-content block-content-full text-center">
					<div>
						'.$ttd_setuju.'
					</div>
					<div class="h5 push-15-t push-5">'.($data->nama_setuju?$data->nama_setuju:'').'</div>
					<div class="text-muted">'.($data->nama_setuju?$data->nip_setuju:'').'</div>
				</div></td></tr>';
		
		if ($data->st_setuju=='0'){
			$label_perubahan='MENUNGGU';
		}
		if ($data->st_setuju=='1'){
			$label_perubahan='DISETUJUI';
		}
		if ($data->st_setuju=='2'){
			$label_perubahan='DITOLAK';
		}
		$data_obat['tabel_1']=$tabel_1;
		$data_obat['tabel_2']=$tabel_2;
		$data_obat['tabel_3']=$tabel_3;
		$data_obat['label_perubahan']=$label_perubahan;
		$data_obat['alasan_perubahan']=$data->alasan_perubahan;
		
		$this->output->set_output(json_encode($data_obat));
	}
	function riwayat_obat(){
		$btn_disabel_edit='';
		$iddokter=$this->input->post('iddokter');
		$where='';
		if ($iddokter){
			$where .=" AND H.iddokter_resep='$iddokter'";
		}
		$idpasien=$this->input->post('idpasien');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$this->select = array();
		$from="
			(
				SELECT H.noresep,H.tanggal_permintaan,H.iddokter_resep,M.nama as nama_dokter
					,D.*
					,B.nama as nama_barang,B.hargadasar
				,BS.ref as sediaan_nama
				,MR.ref as nama_rute
				,MI.nama as nama_interval
				,(CASE 
					WHEN $idkelompokpasien=5 THEN ((B.marginumum/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=4 THEN ((B.marginbpjstenagakerja/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=3 THEN ((B.marginbpjskesehatan/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=2 THEN ((B.marginjasaraharja/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=1 THEN ((B.marginasuransi/100) + 1) * B.hargadasar 
					END) as harga
					FROM tpoliklinik_e_resep H
					INNER JOIN tpoliklinik_e_resep_obat D ON D.assesmen_id=H.assesmen_id
					LEFT JOIN view_barang_farmasi_resep B ON B.id=D.idbarang AND B.idtipe=D.idtipe
					LEFT JOIN merm_referensi BS ON BS.nilai=B.bentuk_sediaan AND BS.ref_head_id='94'
					LEFT JOIN merm_referensi MR ON MR.nilai=D.rute AND MR.ref_head_id='74'
					LEFT JOIN minterval MI ON MI.id=D.interval 
					INNER JOIN mppa M ON M.id=H.iddokter_resep
					WHERE H.idpasien='$idpasien' AND H.status_assemen='2' ".$where."
					ORDER BY H.assesmen_id DESC
			) as tbl
		";
		$this->from   = $from;
		$this->join 	= array();$this->order  = array();$this->group  = array();
		$this->column_search = array('nama_dokter','nama','noresep','nama_rute','nama_interval','sediaan_nama','aturan_tambahan');$this->column_order  = array();
		$list = $this->datatable->get_datatables(true);
		$data = array();$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;$result = array();
			$btn_info='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_info_obat('.$r->idtipe.','.$r->idbarang.')" type="button" data-toggle="tooltip" title="Informasi Obat" data-original-title="Informasi Obat"><i class="fa fa-info"></i></button>';
			$input='<input type="hidden" class="cls_idtipe" value="'.$r->idtipe.'">';
			$input .='<input type="hidden" class="cls_idobat" value="'.$r->idbarang.'">';
			$input .='<input type="hidden" class="cls_idtrx" value="'.$r->id.'">';
			$aksi='';
			$aksi = '<div class="btn-group">';
			$aksi .= '<button  onclick="terapkan_history_obat('.$r->id.')" type="button" title="Terapakan Resep" '.$btn_disabel_edit.' class="btn btn-warning btn-xs btn_terapkan"><i class="fa fa-copy"></i> Apply</button>';	
			$aksi .= '</div>';
			$result[] = ($no);
			$result[] = '<span>'.($r->noresep).'</span><br><span class="text-primary">'.HumanDateLong($r->tanggal_permintaan).'</span>';
			$result[] = $r->nama_dokter;
			$result[] = '<span>'.($r->nama_barang).$btn_info.'</span><br><span class="text-primary">Sediaan : '.($r->sediaan_nama?$r->sediaan_nama:'-').' | Rp. '.number_format(ceiling($r->harga,100)).' </span>'.status_cover($r->cover);
			$result[] = $r->dosis;
			$result[] = $r->jumlah;
			$result[] = $r->nama_rute;
			$result[] = $r->nama_rute;
			$result[] = $r->aturan_tambahan;
			$result[] = $aksi.$input;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
	}
	
	function list_index_obat_racikan(){
		$btn_disabel_edit='';
		$max_iter=$this->input->post('max_iter');
		$assesmen_id=$this->input->post('assesmen_id');
		$q_head="SELECT * FROM `tpoliklinik_e_resep_obat_racikan` H WHERE H.assesmen_id='$assesmen_id' AND H.status_racikan='2'";
		$list_head=$this->db->query($q_head)->result();
		$q_detail="SELECT * FROM `tpoliklinik_e_resep_obat_racikan_obat` H WHERE H.assesmen_id='$assesmen_id'";
		$list_detail=$this->db->query($q_detail)->result_array();
		
		$data_interval=get_all('minterval');
		$data_rute=list_variable_ref(74);
		$data_jenis_racik=list_variable_ref(105);
		$no=0;
		$tabel='';
		foreach ($list_head as $r) {	
			$no ++;
			$tabel .='<table id="tabel_div_racikan" style="width:100%">';
				$tabel .='<tr>';
					$tabel .='<td>';
						$tabel .='<div class="form-group">';
							$tabel .='<div class="col-md-12 ">';
								$tabel .='<label class="h5 text-primary"><strong>RACIKAN '.$no.'</strong></label>';
							$tabel .='</div>';
						$tabel .='</div>';
						$tabel .='<div class="form-group">';
							$tabel .='<div class="col-md-4 ">';
								$tabel .='<label>Nama Racik</label>';
								$tabel .='<input type="text" class="form-control tabel_racikan nama_racikan"  value="'.$r->nama_racikan.'" >';
								$tabel .='<input type="hidden" class="form-control tabel_racikan racikan_id"  value="'.$r->racikan_id.'" >';
							$tabel .='</div>';
							$tabel .='<div class="col-md-2 ">';
								$tabel .='<label>Jumlah Racik</label>';
								$tabel .='<input type="text" class="form-control tabel_racikan jumlah_racikan number"  value="'.$r->jumlah_racikan.'" >';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Jenis Racik</label>';
								$tabel .='<select class="js-select2 form-control tabel_racikan_opsi jenis_racikan" style="width: 100%;">'.$this->data_option($data_jenis_racik,$r->jenis_racikan).'</select>';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Frekuensi / Interval</label>';
								$tabel .='<select class="js-select2 form-control tabel_racikan_opsi interval_racikan" style="width: 100%;">'.$this->data_option($data_interval,$r->interval_racikan).'</select>';
							$tabel .='</div>';
						$tabel .='</div>';
						$tabel .='<div class="form-group">';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Rute Pemberian</label>';
								$tabel .='<select class="js-select2 form-control tabel_racikan_opsi rute_racikan" style="width: 100%;">'.$this->data_option($data_rute,$r->rute_racikan).'</select>';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Aturan Tambahan</label>';
								$tabel .='<input type="text" class="form-control tabel_racikan aturan_tambahan_racikan"  value="'.$r->aturan_tambahan_racikan.'" >';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>ITER</label>';
								$tabel .='<select class="js-select2 form-control tabel_racikan_opsi iter_racikan" style="width: 100%;">'.$this->data_option_iter($max_iter,$r->iter_racikan).'</select>';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>&nbsp;&nbsp;</label>';
								$tabel .='<button class="btn btn-danger btn-block" onclick="hapus_racikan_head('.$r->racikan_id.')" type="button"><i class="fa fa-trash"></i> Hapus Racikan '.$no.'</button>';
							$tabel .='</div>';
						$tabel .='</div>';
						$tabel .='<div class="form-group" style="margin-top:-10px">';
							$tabel .='<div class="col-md-12 ">';
								$tabel .='<label class="h5 text-primary">DAFTAR OBAT RACIKAN '.$no.'</label>';
								$tabel .='<table class="table table-condensed table-bordered" style="background-color:#fff">';
									$tabel .='<thead>';
										$tabel .='<tr>';
											$tabel .='<th class="text-center" style="width: 5%;">NO</th>';
											$tabel .='<th class="text-center" style="width: 30%;">NAMA</th>';
											$tabel .='<th class="text-center" style="width: 10%;">QTY</th>';
											$tabel .='<th class="text-center" style="width: 10%;">P1</th>';
											$tabel .='<th class="text-center" style="width: 10%;">P2</th>';
											$tabel .='<th class="text-center" style="width: 10%;">DOSIS</th>';
											$tabel .='<th class="text-center" style="width: 10%;">T-QTY</th>';
											$tabel .='<th class="text-center" style="width: 10%;">AKSI</th>';
										$tabel .='</tr>';
									$tabel .='</thead>';
									$tabel .='<tbody>';
									$tabel .=$this->detail_obat_racikan($list_detail,$r->racikan_id,$r->jumlah_racikan);
									$tabel .='</tbody>';
								$tabel .='</table>';
							$tabel .='</div>';
						$tabel .='</div>';
					$tabel .='</td>';
				$tabel .='</tr>';
			$tabel .='</table>';
		}
		
		$this->output->set_output(json_encode($tabel));
	}
	function detail_obat_racikan($list_data,$racikan_id,$jumlah_racikan){
		$tabel='';
		$data = array_filter($list_data, function($var) use ($racikan_id) { 
			return ($var['racikan_id'] == $racikan_id);
		  });	
		if ($data){
			$no=0;
			foreach ($data as $r){
				$btn_hapus='<button  onclick="hapus_racikan_detail_final('.$r['id'].')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';
				$btn_info='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_info_obat('.$r['idtipe'].','.$r['idbarang'].')" type="button" data-toggle="tooltip" title="Informasi Obat" data-original-title="Informasi Obat"><i class="fa fa-info"></i></button>';
		  // print_r($r['idtipe']);exit;
				$no++;
				$input='<input type="hidden" class="cls_idtipe" value="'.$r['idtipe'].'">';
				$input .='<input type="hidden" class="cls_idobat" value="'.$r['idbarang'].'">';
				$input .='<input type="hidden" class="cls_idtrx" value="'.$r['id'].'">';
				$input .='<input type="hidden" class="jumlah_racikan" value="'.$jumlah_racikan.'">';
				$tabel .='<tr>';
				$tabel .='<td>'.$no.$input.'</td>';
				$tabel .='<td>'.$r['nama'].$btn_info.status_cover($r['cover']).'</td>';
				$tabel .='<td><input  type="text" class="dosis_nrd dosis_nrd_jumlah decimal-2 jumlah text-center" value="'.$r['jumlah'].'"></td>';
				$tabel .='<td><input  type="text" class="dosis_nrd dosis_nrd_p1_p2 decimal-2 p1 text-center" value="'.$r['p1'].'"></td>';
				$tabel .='<td><input  type="text" class="dosis_nrd dosis_nrd_p1_p2 ecimal-2 p2 text-center" value="'.$r['p2'].'"></td>';
				$tabel .='<td><input  type="text" class="dosis_nrd dosis_nrd_dosis number dosis text-center" value="'.$r['dosis'].'"><input  type="hidden" class="dosis_nrd number dosis_master text-center" value="'.$r['dosis_master'].'"></td>';
				$tqty_koma=number_format(($r['p1'] / $r['p2'] * $jumlah_racikan),1);
				$tqty=ceiling(($r['p1'] / $r['p2'] * $jumlah_racikan),1);
				// $tqty=number_format($r['p1'] / $r['p2'],2).' '.$r['p1'] .'/'. $r['p2'];
				
				$tabel .='<td class="text-center"><input  type="hidden" class="dosis_nrd decimal-2 total text-center" disabled value="'.$tqty.'"><label class="label_tqty">'.$tqty_koma.' ('.$tqty.')'.'</label></td>';
				$tabel .='<td class="text-center">'.$btn_hapus.'</td>';
				$tabel .='</tr>';
			}
		}
		
		return $tabel;

	}
	function list_index_obat_racikan_telaah(){
		$btn_disabel_edit='';
		$max_iter=$this->input->post('max_iter');
		$assesmen_id=$this->input->post('assesmen_id');
		$tab=$this->input->post('tab');
		
		$where='';
		if ($tab>=4){
			$where .=" AND H.kuantitas > 0";
		}
		
		
		$q_head="SELECT * FROM `tpoliklinik_e_resep_obat_racikan` H WHERE H.assesmen_id='$assesmen_id' AND H.status_racikan='2' ".$where;
		$list_head=$this->db->query($q_head)->result();
		$q_detail="SELECT H.*,UE.nama as nama_user_perubahan FROM `tpoliklinik_e_resep_obat_racikan_obat` H 
LEFT JOIN mppa UE ON UE.id=H.user_perubahan
WHERE H.assesmen_id='$assesmen_id'";
		$list_detail=$this->db->query($q_detail)->result_array();
		
		$data_interval=get_all('minterval');
		$data_rute=list_variable_ref(74);
		$data_jenis_racik=list_variable_ref(105);
		$no=0;
		$tabel='';
		foreach ($list_head as $r) {	
			$no ++;
			$btn_add_racikan='';
			$btn_aksi='&nbsp;&nbsp;&nbsp;<div class="btn-group btn-group-xs" role="group">';
			$btn_aksi .='<button class="btn btn-default edit" type="button"><i class="fa fa-pencil"></i></button>';
			$btn_aksi .='<button class="btn btn-default hapus" type="button"><i class="fa fa-trash"></i></button>';
			$btn_aksi .='</div>';
			$btn_add_racikan .='&nbsp;&nbsp;&nbsp;<button class="btn btn-xs btn-info add_racikan" title="Tambah Obat" onclick="add_racikan('.$r->racikan_id.')" type="button"><i class="fa fa-plus"></i> Obat</button><br>';
			$tabel .='<table id="tabel_div_racikan" style="width:100%">';
				$tabel .='<tr>';
					$tabel .='<td>';
						$tabel .='<div class="form-group">';
							$tabel .='<div class="col-md-12 ">';
								$tabel .='<label class="h5 text-primary"><strong>RACIKAN '.$no.'</strong></label>'.$btn_aksi;
							$tabel .='</div>';
						$tabel .='</div>';
						$tabel .='<div class="form-group">';
							$tabel .='<div class="col-md-4 ">';
								$tabel .='<label>Nama Racik</label>';
								$tabel .='<input disabled type="text" class="form-control tabel_racikan nama_racikan"  value="'.$r->nama_racikan.'" >';
								$tabel .='<input disabled type="hidden" class="form-control tabel_racikan racikan_id"  value="'.$r->racikan_id.'" >';
							$tabel .='</div>';
							$tabel .='<div class="col-md-2 ">';
								$tabel .='<label>Jumlah Racik</label>';
								$tabel .='<input disabled type="text" class="form-control tabel_racikan jumlah_racikan number"  value="'.$r->jumlah_racikan.'" >';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Jenis Racik</label>';
								$tabel .='<select disabled class="js-select2 form-control tabel_racikan_opsi jenis_racikan" style="width: 100%;">'.$this->data_option($data_jenis_racik,$r->jenis_racikan).'</select>';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Frekuensi / Interval</label>';
								$tabel .='<select disabled class="js-select2 form-control tabel_racikan_opsi interval_racikan" style="width: 100%;">'.$this->data_option($data_interval,$r->interval_racikan).'</select>';
							$tabel .='</div>';
						$tabel .='</div>';
						$tabel .='<div class="form-group">';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Rute Pemberian</label>';
								$tabel .='<select disabled class="js-select2 form-control tabel_racikan_opsi rute_racikan" style="width: 100%;">'.$this->data_option($data_rute,$r->rute_racikan).'</select>';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Aturan Tambahan</label>';
								$tabel .='<input disabled type="text" class="form-control tabel_racikan aturan_tambahan_racikan"  value="'.$r->aturan_tambahan_racikan.'" >';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>ITER</label>';
								$tabel .='<select disabled class="js-select2 form-control tabel_racikan_opsi iter_racikan" style="width: 100%;">'.$this->data_option_iter($max_iter,$r->iter_racikan).'</select>';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label><br></label>';
								$tabel .='';
							$tabel .='</div>';
						$tabel .='</div>';
						$tabel .='<div class="form-group" style="margin-top:-10px">';
							$tabel .='<div class="col-md-12 ">';
								$tabel .='<label class="h5 text-primary">DAFTAR OBAT RACIKAN '.$no.'</label>'.$btn_add_racikan;
								$tabel .='<br><table class="table table-condensed table-bordered" style="background-color:#fff">';
									$tabel .='<thead>';
										$tabel .='<tr>';
											$tabel .='<th class="text-center" style="width: 5%;">NO</th>';
											$tabel .='<th class="text-center" style="width: 30%;">NAMA</th>';
											$tabel .='<th class="text-center" style="width: 10%;">QTY</th>';
											$tabel .='<th class="text-center" style="width: 10%;">P1</th>';
											$tabel .='<th class="text-center" style="width: 10%;">P2</th>';
											$tabel .='<th class="text-center" style="width: 10%;">DOSIS</th>';
											$tabel .='<th class="text-center" style="width: 10%;">T-QTY</th>';
											$tabel .='<th class="text-center" style="width: 10%;">STATUS EDIT</th>';
										$tabel .='</tr>';
									$tabel .='</thead>';
									$tabel .='<tbody>';
									$tabel .=$this->detail_obat_racikan_telaah($list_detail,$r->racikan_id,$r->jumlah_racikan);
									$tabel .='</tbody>';
								$tabel .='</table>';
							$tabel .='</div>';
						$tabel .='</div>';
					$tabel .='</td>';
				$tabel .='</tr>';
			$tabel .='</table>';
		}
		
		$this->output->set_output(json_encode($tabel));
	}
	function list_index_obat_racikan_telaah_perubahan(){
		$btn_disabel_edit='';
		$max_iter=$this->input->post('max_iter');
		$assesmen_id=$this->input->post('assesmen_id');
		$q_head="SELECT * FROM `tpoliklinik_e_resep_obat_racikan` H WHERE H.assesmen_id='$assesmen_id' AND H.status_racikan='2'";
		$list_head=$this->db->query($q_head)->result();
		$q_detail="SELECT H.*,UE.nama as nama_user_perubahan FROM `tpoliklinik_e_resep_obat_racikan_obat` H 
LEFT JOIN mppa UE ON UE.id=H.user_perubahan
WHERE H.assesmen_id='$assesmen_id'";
		$list_detail=$this->db->query($q_detail)->result_array();
		
		$data_interval=get_all('minterval');
		$data_rute=list_variable_ref(74);
		$data_jenis_racik=list_variable_ref(105);
		$no=0;
		$tabel='';
		foreach ($list_head as $r) {	
			$no ++;
			$btn_aksi='&nbsp;&nbsp;&nbsp;<div class="btn-group btn-group-xs" role="group">';
			$btn_aksi .='<button class="btn btn-default edit" type="button"><i class="fa fa-pencil"></i></button>';
			$btn_aksi .='<button class="btn btn-default hapus" type="button"><i class="fa fa-trash"></i></button>';
			$btn_aksi .='</div>';
			$tabel .='<table id="tabel_div_racikan" style="width:100%">';
				$tabel .='<tr>';
					$tabel .='<td>';
						$tabel .='<div class="form-group">';
							$tabel .='<div class="col-md-12 ">';
								$tabel .='<label class="h5 text-primary"><strong>RACIKAN '.$no.'</strong></label>'.$btn_aksi;
							$tabel .='</div>';
						$tabel .='</div>';
						$tabel .='<div class="form-group">';
							$tabel .='<div class="col-md-4 ">';
								$tabel .='<label>Nama Racik</label>';
								$tabel .='<input disabled type="text" class="form-control tabel_racikan nama_racikan"  value="'.$r->nama_racikan.'" >';
								$tabel .='<input disabled type="hidden" class="form-control tabel_racikan racikan_id"  value="'.$r->racikan_id.'" >';
							$tabel .='</div>';
							$tabel .='<div class="col-md-2 ">';
								$tabel .='<label>Jumlah Racik</label>';
								$tabel .='<input disabled type="text" class="form-control tabel_racikan jumlah_racikan number"  value="'.$r->jumlah_racikan.'" >';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Jenis Racik</label>';
								$tabel .='<select disabled class="js-select2 form-control tabel_racikan_opsi jenis_racikan" style="width: 100%;">'.$this->data_option($data_jenis_racik,$r->jenis_racikan).'</select>';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Frekuensi / Interval</label>';
								$tabel .='<select disabled class="js-select2 form-control tabel_racikan_opsi interval_racikan" style="width: 100%;">'.$this->data_option($data_interval,$r->interval_racikan).'</select>';
							$tabel .='</div>';
						$tabel .='</div>';
						$tabel .='<div class="form-group">';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Rute Pemberian</label>';
								$tabel .='<select disabled class="js-select2 form-control tabel_racikan_opsi rute_racikan" style="width: 100%;">'.$this->data_option($data_rute,$r->rute_racikan).'</select>';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Aturan Tambahan</label>';
								$tabel .='<input disabled type="text" class="form-control tabel_racikan aturan_tambahan_racikan"  value="'.$r->aturan_tambahan_racikan.'" >';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>ITER</label>';
								$tabel .='<select disabled class="js-select2 form-control tabel_racikan_opsi iter_racikan" style="width: 100%;">'.$this->data_option_iter($max_iter,$r->iter_racikan).'</select>';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label><br></label>';
								$tabel .='';
							$tabel .='</div>';
						$tabel .='</div>';
						$tabel .='<div class="form-group" style="margin-top:-10px">';
							$tabel .='<div class="col-md-12 ">';
								$tabel .='<label class="h5 text-primary">DAFTAR OBAT RACIKAN '.$no.'</label>';
								$tabel .='<table class="table table-condensed table-bordered" style="background-color:#fff">';
									$tabel .='<thead>';
										$tabel .='<tr>';
											$tabel .='<th class="text-center" style="width: 5%;">NO</th>';
											$tabel .='<th class="text-center" style="width: 20%;">NAMA</th>';
											$tabel .='<th class="text-center" style="width: 8%;">QTY</th>';
											$tabel .='<th class="text-center" style="width: 5%;">P1</th>';
											$tabel .='<th class="text-center" style="width: 5%;">P2</th>';
											$tabel .='<th class="text-center" style="width: 10%;">DOSIS</th>';
											$tabel .='<th class="text-center" style="width: 10%;">T-QTY</th>';
											$tabel .='<th class="text-center" style="width: 10%;">STATUS EDIT</th>';
											$tabel .='<th class="text-center" style="width: 12%;">AKSI</th>';
										$tabel .='</tr>';
									$tabel .='</thead>';
									$tabel .='<tbody>';
									$tabel .=$this->detail_obat_racikan_telaah_perubahan($list_detail,$r->racikan_id,$r->jumlah_racikan);
									$tabel .='</tbody>';
								$tabel .='</table>';
							$tabel .='</div>';
						$tabel .='</div>';
					$tabel .='</td>';
				$tabel .='</tr>';
			$tabel .='</table>';
		}
		
		$this->output->set_output(json_encode($tabel));
	}
	
	
	function detail_obat_racikan_telaah_perubahan($list_data,$racikan_id,$jumlah_racikan){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$tabel='';
		$data = array_filter($list_data, function($var) use ($racikan_id) { 
			return ($var['racikan_id'] == $racikan_id);
		  });	
		if ($data){
			$no=0;
			foreach ($data as $r){
				$aksi='';
				
				$label_perubahan='';
				if ($r['st_setuju']=='0'){
					$label_setuju='<button onclick="lihat_perubahan_obat_racikan('.$r['id'].')" type="button" class="btn btn-default btn-xs btn_lihat_perubahan"><i class="fa fa-user"></i> MENUNGGU</button>';
				}
				if ($r['st_setuju']=='1'){
					$label_setuju='<button onclick="lihat_perubahan_obat_racikan('.$r['id'].')" type="button" class="btn btn-primary btn-xs btn_lihat_perubahan"><i class="fa fa-user"></i> SETUJU</button>';
				}
				if ($r['st_setuju']=='2'){
					$label_setuju='<button onclick="lihat_perubahan_obat_racikan('.$r['id'].')" type="button" class="btn btn-danger btn-xs btn_lihat_perubahan"><i class="fa fa-times"></i> DITOLAK</button>';
				}
				if ($r['st_perubahan']!='0'){
					$aksi .= $label_setuju;	
					$aksi .= '<br><br><button onclick="lihat_perubahan_obat_racikan('.$r['id'].')" type="button" class="btn btn-'.($r['st_perubahan']=='1'?'warning':'success').' btn-xs btn_lihat_perubahan">'.($r['st_perubahan']=='1'?'PERUBAHAN':'PENAMBAHAN').'</button>';	
					$label_perubahan='<br><span class="text-danger"><strong>Perubahan : '.$r['nama_user_perubahan'].' | '.HumanDateLong($r['tanggal_perubahan']).'</strong></span>';
				}
				
				$btn_edit='';
				$btn_hapus='';
				if (UserAccesForm($user_acces_form,array('1843'))){
					$btn_edit='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_edit_obat_racikan_detail('.$r['id'].')" type="button" data-toggle="tooltip" title="Edit Obat"><i class="fa fa-pencil text-danger"></i></button>';
				}
				if (UserAccesForm($user_acces_form,array('1844'))){
					$btn_hapus='<button  onclick="hapus_racikan_detail_final('.$r['id'].')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';
				}
				$btn_info='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_info_obat('.$r['idtipe'].','.$r['idbarang'].')" type="button" data-toggle="tooltip" title="Informasi Obat" data-original-title="Informasi Obat"><i class="fa fa-info"></i></button>';
		  // print_r($r['idtipe']);exit;
				$no++;
				$input='<input type="hidden" class="cls_idtipe" value="'.$r['idtipe'].'">';
				$input .='<input type="hidden" class="cls_idobat" value="'.$r['idbarang'].'">';
				$input .='<input type="hidden" class="cls_idtrx" value="'.$r['id'].'">';
				$input .='<input type="hidden" class="jumlah_racikan" value="'.$jumlah_racikan.'">';
				$tabel .='<tr>';
				$tabel .='<td>'.$no.$input.'</td>';
				$tabel .='<td>'.$r['nama'].$btn_info.status_cover($r['cover']).$btn_edit.$btn_hapus.$label_perubahan.'</td>';
				$tabel .='<td><input disabled type="text" class="dosis_nrd number jumlah text-center" value="'.$r['jumlah'].'"></td>';
				$tabel .='<td><input disabled type="text" class="dosis_nrd decimal-2 p1 text-center" value="'.$r['p1'].'"></td>';
				$tabel .='<td><input disabled type="text" class="dosis_nrd decimal-2 p2 text-center" value="'.$r['p2'].'"></td>';
				$tabel .='<td><input disabled type="text" class="dosis_nrd number dosis text-center" value="'.$r['dosis'].'"></td>';
				$tabel .='<td><input disabled type="text" class="dosis_nrd number total text-center" disabled value="'.($r['p1'] / $r['p2'] * $jumlah_racikan).'"></td>';
		
			$aksi_persetujuan=($r['st_perubahan']==0?'':'
				<div class="btn-group btn-group-xs" role="group">
					<button class="btn btn-success btn_persetujuan" type="button" onclick="setuju_obat('.$r['id'].',2)" title="Setuju"><i class="fa fa-check"></i> SETUJU</button>
					<button class="btn btn-danger btn_persetujuan" type="button" onclick="tolak_obat('.$r['id'].',2)" title="Tolak"><i class="si si-ban" ></i> TOLAK</button>
				</div>');
			$aksi_persetujuan= ($r['st_setuju']==0?$aksi_persetujuan:$label_setuju);
				$tabel .='<td class="text-center">'.$aksi.'</td>';
				$tabel .='<td class="text-center">'.$aksi_persetujuan.'</td>';
				$tabel .='</tr>';
			}
		}
		
		return $tabel;

	}
	function detail_obat_racikan_telaah($list_data,$racikan_id,$jumlah_racikan){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$tabel='';
		$data = array_filter($list_data, function($var) use ($racikan_id) { 
			return ($var['racikan_id'] == $racikan_id);
		  });	
		if ($data){
			$no=0;
			foreach ($data as $r){
				$aksi='';
				
				$label_perubahan='';
				if ($r['st_setuju']=='0'){
					$label_setuju='<button onclick="lihat_perubahan_obat_racikan('.$r['id'].')" type="button" class="btn btn-default btn-xs btn_lihat_perubahan"><i class="fa fa-user"></i> MENUNGGU</button>';
				}
				if ($r['st_setuju']=='1'){
					$label_setuju='<button onclick="lihat_perubahan_obat_racikan('.$r['id'].')" type="button" class="btn btn-primary btn-xs btn_lihat_perubahan"><i class="fa fa-user"></i> SETUJU</button>';
				}
				if ($r['st_setuju']=='2'){
					$label_setuju='<button onclick="lihat_perubahan_obat_racikan('.$r['id'].')" type="button" class="btn btn-danger btn-xs btn_lihat_perubahan"><i class="fa fa-times"></i> DITOLAK</button>';
				}
				if ($r['st_perubahan']!='0'){
					$aksi .= $label_setuju;	
					$aksi .= '<br><br><button onclick="lihat_perubahan_obat_racikan('.$r['id'].')" type="button" class="btn btn-'.($r['st_perubahan']=='1'?'warning':'success').' btn-xs btn_lihat_perubahan">'.($r['st_perubahan']=='1'?'PERUBAHAN':'PENAMBAHAN').'</button>';	
					$label_perubahan='<br><span class="text-danger"><strong>Perubahan : '.$r['nama_user_perubahan'].' | '.HumanDateLong($r['tanggal_perubahan']).'</strong></span>';
				}
				
				$btn_edit='';
				$btn_hapus='';
				if (UserAccesForm($user_acces_form,array('1843'))){
					$btn_edit='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_edit_obat_racikan_detail('.$r['id'].')" type="button" data-toggle="tooltip" title="Edit Obat"><i class="fa fa-pencil text-danger"></i></button>';
				}
				if (UserAccesForm($user_acces_form,array('1844'))){
					$btn_hapus='<button  onclick="hapus_racikan_detail_final('.$r['id'].')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';
				}
				$btn_info='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_info_obat('.$r['idtipe'].','.$r['idbarang'].')" type="button" data-toggle="tooltip" title="Informasi Obat" data-original-title="Informasi Obat"><i class="fa fa-info"></i></button>';
		  // print_r($r['idtipe']);exit;
				$no++;
				$input='<input type="hidden" class="cls_idtipe" value="'.$r['idtipe'].'">';
				$input .='<input type="hidden" class="cls_idobat" value="'.$r['idbarang'].'">';
				$input .='<input type="hidden" class="cls_idtrx" value="'.$r['id'].'">';
				$input .='<input type="hidden" class="jumlah_racikan" value="'.$jumlah_racikan.'">';
				$tabel .='<tr>';
				$tabel .='<td>'.$no.$input.'</td>';
				$tabel .='<td>'.$r['nama'].$btn_info.status_cover($r['cover']).$btn_edit.$btn_hapus.$label_perubahan.'</td>';
				$tabel .='<td><input disabled type="text" class="dosis_nrd decimal-2 jumlah text-center" value="'.$r['jumlah'].'"></td>';
				$tabel .='<td><input disabled type="text" class="dosis_nrd decimal-2 p1 text-center" value="'.$r['p1'].'"></td>';
				$tabel .='<td><input disabled type="text" class="dosis_nrd decimal-2 p2 text-center" value="'.$r['p2'].'"></td>';
				$tabel .='<td><input disabled type="text" class="dosis_nrd number dosis text-center" value="'.$r['dosis'].'"></td>';
				$tqty_koma=number_format(($r['p1'] / $r['p2'] * $jumlah_racikan),1);
				$tqty=ceiling(($r['p1'] / $r['p2'] * $jumlah_racikan),1);
				$tabel .='<td class="text-center"><input  type="hidden" class="dosis_nrd decimal-2 total text-center" disabled value="'.$tqty.'"><label class="label_tqty">'.$tqty_koma.' ('.$tqty.')'.'</label></td>';
				// $tabel .='<td><input disabled type="text" class="dosis_nrd number total text-center" disabled value="'.$tqty.'"></td>';
				$tabel .='<td class="text-center">'.$aksi.'</td>';
				$tabel .='</tr>';
			}
		}
		
		return $tabel;

	}
	function data_option($arr,$id){
		$opsi='';
		foreach($arr as $row){
			$opsi .='<option value="'.$row->id.'" '.($row->id==$id?'selected':'').'>'.$row->nama.'</option>';
		}
		return $opsi;
	}
	function data_option_iter($max_iter,$id){
		$opsi='';
		for ($i=0;$i<=$max_iter;$i++){
			$opsi .='<option value="'.$i.'" '.($i==$id?'selected':'').'>'.$i.'</option>';
		}
		return $opsi;
	}
	function list_index_racikan_tampung(){
		$btn_disabel_edit='';
		$racikan_id=$this->input->post('racikan_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$this->select = array();
		$from="
			(
				SELECT D.id,D.nama,D.jumlah,D.p1,D.p2,D.dosis,(D.p1/D.p2 * H.jumlah_racikan) as total 
				,D.idbarang,D.idtipe
				FROM `tpoliklinik_e_resep_obat_racikan` H
				INNER JOIN tpoliklinik_e_resep_obat_racikan_obat D ON D.racikan_id=H.racikan_id
				WHERE H.racikan_id='$racikan_id'
				ORDER BY H.racikan_id ASC
			) as tbl
		";
		$this->from   = $from;
		$this->join 	= array();$this->order  = array();$this->group  = array();
		$this->column_search = array('nama');$this->column_order  = array();
		$list = $this->datatable->get_datatables(true);
		$data = array();$no = $_POST['start'];
		$data_interval=get_all('minterval');
		$data_rute=list_variable_ref(74);
		foreach ($list as $r) {	
			$no++;$result = array();
			$btn_info='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_info_obat('.$r->idtipe.','.$r->idbarang.')" type="button" data-toggle="tooltip" title="Informasi Obat" data-original-title="Informasi Obat"><i class="fa fa-info"></i></button>';
			$input='<input type="hidden" class="cls_idtipe" value="'.$r->idtipe.'">';
			$input .='<input type="hidden" class="cls_idobat" value="'.$r->idbarang.'">';
			$input .='<input type="hidden" class="cls_idtrx" value="'.$r->id.'">';
			$aksi='';
			$aksi = '<div class="btn-group">';
			$aksi .= '<button  onclick="hapus_racikan_detail('.$r->id.')" type="button" title="Hapus" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			$aksi .= '</div>';
			$result[] = ($no);
			$result[] = '<span class="text-danger">'.($r->nama).'</span>';
			$result[] =($r->jumlah);
			$result[] =($r->p1.' / '.$r->p2);
			$result[] =($r->dosis);
			$result[] =number_format($r->total,1).' ('.ceiling($r->total,1).')';
			$result[] = $aksi.$input;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
	}
	
	function opsi_header($list_data,$id,$nama_class){
		$opsi='<select tabindex="8"  width="100%" class="js-select2 form-control '.$nama_class.' opsi_change_nr" style="width: 100%;" data-placeholder="Pilih Opsi" required>';
		foreach($list_data as $row){
			$opsi .='<option value="'.$row->id.'" '.($row->id==$id?'selected':'').'>'.$row->nama.'</option>';
		}
		$opsi .='</select>';
		return $opsi;
	}
	
	function hapus_obat(){
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$hasil=$this->db->delete('tpoliklinik_e_resep_obat');
		$this->output->set_output(json_encode($hasil));
	}
	
	function batalkan_racikan(){
		$racikan_id=$this->input->post('racikan_id');
		$this->db->where('racikan_id',$racikan_id);
		$hasil=$this->db->delete('tpoliklinik_e_resep_obat_racikan');
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_racikan_detail(){
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$hasil=$this->db->delete('tpoliklinik_e_resep_obat_racikan_obat');
		$this->output->set_output(json_encode($hasil));
	}
	public function get_obat_filter()
	{
		$iduser=$this->session->userdata('user_id');
		$merk     = $this->input->post('merk');
		$jenis_penggunaan     = $this->input->post('jenis_penggunaan');
		$bentuk_sediaan     = $this->input->post('bentuk_sediaan');
		$cls     = $this->input->post('cls');
		$idkelompokpasien     = $this->input->post('idkelompokpasien');
		$nama_barang     = $this->input->post('nama_barang');
		$idunit     = $this->input->post('idunit');
		$idtipe     	= $this->input->post('idtipe');
		$idkategori    = $this->input->post('idkategori');
		
		$idtipe_poli    = $this->input->post('idtipe_poli');
		$idpoliklinik    = $this->input->post('idpoliklinik');
		$idkelompokpasien    = $this->input->post('idkelompokpasien');
		$idrekanan    = $this->input->post('idrekanan');

		$where='';
		if ($merk!=''){
			$merk=implode(", ", $merk);
			 $where .=" AND B.merk IN(".$merk.")";
		}
		if ($jenis_penggunaan!=''){
			$jenis_penggunaan=implode(", ", $jenis_penggunaan);
			 $where .=" AND B.jenis_penggunaan IN(".$jenis_penggunaan.")";
		}
		if ($bentuk_sediaan!=''){
			$bentuk_sediaan=implode(", ", $bentuk_sediaan);
			 $where .=" AND B.bentuk_sediaan IN(".$bentuk_sediaan.")";
		}
		if ($nama_barang!=''){
			 $where .=" AND B.nama LIKE '%".ReplaceKutip($nama_barang)."%'";
		}
		// if ($idtipe  != '#') {
			// $where .=" AND S.idtipe='$idtipe'";
		// } else {
			// $where .=" AND S.idtipe IN( SELECT H.idtipe from munitpelayanan_tipebarang H
										// LEFT JOIN mdata_tipebarang T ON H.idtipe=T.id
										// WHERE H.idunitpelayanan='$idunit'
					// )";
		// }
		if ($idkategori  != '#') {
			$where .=" AND B.idkategori IN (
							SELECT id FROM (
									SELECT *FROM view_kategori_alkes
									UNION
									SELECT *FROM view_kategori_implan
									UNION
									SELECT *FROM view_kategori_obat
									UNION
									SELECT *FROM view_kategori_logistik
							) T1
							where T1.path like '$idkategori%'
			)";
		}
		// $from="(
			// SELECT S.idunitpelayanan,S.idtipe,S.idbarang as id,B.nama,B.idkategori,B.kode,B.namatipe,B.catatan
			// ,B.stokminimum,B.stokreorder,S.stok as stok_asli,msatuan.nama as satuan 
			// ,MR.ref as nama_merk,BS.ref as nama_bentuk_sediaan
			// ,(CASE 
					// WHEN $idkelompokpasien=5 THEN ((B.marginumum/100) + 1) * B.hargadasar 
					// WHEN $idkelompokpasien=4 THEN ((B.marginbpjstenagakerja/100) + 1) * B.hargadasar 
					// WHEN $idkelompokpasien=3 THEN ((B.marginbpjskesehatan/100) + 1) * B.hargadasar 
					// WHEN $idkelompokpasien=2 THEN ((B.marginjasaraharja/100) + 1) * B.hargadasar 
					// WHEN $idkelompokpasien=1 THEN ((B.marginasuransi/100) + 1) * B.hargadasar 
					// END) as harga
			// from mgudang_stok S
			// INNER JOIN view_barang_farmasi_resep B ON B.idtipe=S.idtipe AND B.id=S.idbarang
			// INNER JOIN msatuan ON msatuan.id=B.idsatuankecil
			// LEFT JOIN merm_referensi MR ON MR.nilai=B.merk AND MR.ref_head_id='93'
			// LEFT JOIN merm_referensi BS ON BS.nilai=B.bentuk_sediaan AND BS.ref_head_id='94'
			// WHERE S.idunitpelayanan=(SELECT idunit FROM mtujuan_farmasi WHERE id='1') 
			// ".$where."
			// ORDER BY B.idtipe,B.nama
		
		// ) as tbl";
		$from="(
			SELECT *FROM (
				SELECT B.kode,S.*,B.idkategori,B.nama,MS.nama as satuan,B.hargadasar,B.marginasuransi 
				,(CASE 
				WHEN 1=5 THEN ((B.marginumum/100) + 1) * B.hargadasar 
				WHEN 1=4 THEN ((B.marginbpjstenagakerja/100) + 1) * B.hargadasar 
				WHEN 1=3 THEN ((B.marginbpjskesehatan/100) + 1) * B.hargadasar 
				WHEN 1=2 THEN ((B.marginjasaraharja/100) + 1) * B.hargadasar 
				WHEN 1=1 THEN ((B.marginasuransi/100) + 1) * B.hargadasar 
				END) as harga
				,CASE WHEN BS.nilai THEN BS.ref ELSE '-' END as nama_bentuk_sediaan,
				H.idtipe_barang,MR.ref as nama_merk,B.stokreorder,B.stokminimum,
				compare_value_12(
				MAX(IF(H.idkategori_barang=B.idkategori AND H.idbarang=S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='$idpoliklinik' AND H.idkelompokpasien='$idkelompokpasien' AND H.idrekanan='$idrekanan',H.cover,null))
				,MAX(IF(H.idkategori_barang=B.idkategori AND H.idbarang=S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='$idpoliklinik' AND H.idkelompokpasien='$idkelompokpasien' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang=B.idkategori AND H.idbarang=S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='$idpoliklinik' AND H.idkelompokpasien='0' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang=B.idkategori AND H.idbarang=S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang=B.idkategori AND H.idbarang=S.idbarang AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang=B.idkategori AND H.idbarang='0' AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang='0' AND H.idbarang=S.idbarang AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang='0' AND H.idbarang=S.idbarang AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang='0' AND H.idbarang=S.idbarang AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND (H.idpoli_kelas='0' OR H.idpoli_kelas='$idpoliklinik') AND H.idkelompokpasien='0' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang='0' AND (H.idbarang=S.idbarang OR H.idbarang='0') AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND (H.idpoli_kelas='0' OR H.idpoli_kelas='$idpoliklinik') AND (H.idkelompokpasien='0' OR H.idkelompokpasien='$idkelompokpasien' ) AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang='0' AND (H.idbarang=S.idbarang OR H.idbarang='0') AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND (H.idpoli_kelas='0' OR H.idpoli_kelas='$idpoliklinik') AND (H.idkelompokpasien='0' OR H.idkelompokpasien='$idkelompokpasien' ) AND (H.idrekanan='0' OR H.idrekanan='$idrekanan'),H.cover,null))
				,MAX(IF(H.idkategori_barang='0' AND  H.idbarang='0' AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0' ,H.cover,null))
				) as cover
				FROM mtujuan_farmasi HTF
				INNER JOIN mgudang_stok S ON S.idunitpelayanan=HTF.idunit
				INNER JOIN view_barang_farmasi_resep B ON B.id=S.idbarang AND B.idtipe=S.idtipe
				LEFT JOIN merm_referensi BS ON BS.nilai=B.bentuk_sediaan AND BS.ref_head_id='94'
				LEFT JOIN merm_referensi MR ON MR.nilai=B.merk AND MR.ref_head_id='93'
				LEFT JOIN msatuan MS ON MS.id=B.idsatuankecil
				LEFT JOIN setting_cover_barang  H ON
				H.idtipe_barang = S.idtipe AND
				(
				(H.idkategori_barang = B.idkategori AND H.idbarang = S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='$idpoliklinik' AND H.idkelompokpasien='$idkelompokpasien' AND H.idrekanan='$idrekanan') OR #1
				(H.idkategori_barang = B.idkategori AND H.idbarang = S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='$idpoliklinik' AND H.idkelompokpasien='$idkelompokpasien' AND H.idrekanan='0') OR #2
				(H.idkategori_barang = B.idkategori AND H.idbarang = S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='$idpoliklinik' AND H.idkelompokpasien='0' AND H.idrekanan='0') OR #3
				(H.idkategori_barang = B.idkategori AND H.idbarang = S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0') OR #4
				(H.idkategori_barang = B.idkategori AND H.idbarang = S.idbarang AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0') OR #5
				(H.idkategori_barang = B.idkategori AND H.idbarang = '0' 			 AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0') OR #6
				(H.idkategori_barang = '0' 		  		AND H.idbarang = S.idbarang AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0') OR #7
				(H.idkategori_barang = '0' 		  		AND H.idbarang = S.idbarang AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0') OR #8
				(H.idkategori_barang = '0' 		  		AND H.idbarang = S.idbarang AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND (H.idpoli_kelas='0' OR H.idpoli_kelas='$idpoliklinik') 
																						AND H.idkelompokpasien='0' AND H.idrekanan='0') OR #9
				(H.idkategori_barang='0' 			 		  AND (H.idbarang = S.idbarang OR H.idbarang='0') AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND (H.idpoli_kelas='0' OR H.idpoli_kelas='$idpoliklinik') 	
																						AND (H.idkelompokpasien='0' OR H.idkelompokpasien='$idkelompokpasien' ) AND H.idrekanan='0') OR #10
				(H.idkategori_barang='0'	 		  		AND (H.idbarang=S.idbarang OR H.idbarang='0') AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND (H.idpoli_kelas='0' OR H.idpoli_kelas='$idpoliklinik') 
																						AND (H.idkelompokpasien='0' OR H.idkelompokpasien='$idkelompokpasien' ) AND (H.idrekanan='0' OR H.idrekanan='$idrekanan')) OR
				(H.idkategori_barang='0' 						AND  H.idbarang='0' AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0')
				) 

				WHERE HTF.id='$idunit' AND S.status='1' AND S.idtipe='$idtipe'
				
				".$where."
				GROUP BY S.idbarang
				) T 
				WHERE (T.cover IS NULL OR T.cover='2' OR T.cover='1')
		) as tbl";


		$this->select = array();
		$this->join 	= array();
		$this->where  = array();

		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;
		// print_r($from);exit();
		$this->column_search   = array('kode','nama');
		$this->column_order    = array('kode','nama');

		$list = $this->datatable->get_datatables();
		$data = array();
		$no = $_POST['start'];
		// print_r($list);exit();
		foreach ($list as $r) {
			$no++;
			$row = array();

			$row[] = $no;
			$label_cover='';
			if ($r->cover){
				$label_cover='&nbsp;&nbsp;&nbsp;'.status_cover($r->cover);
			}
			if ($r->cover=='2'){
				$row[] =$r->kode;
				$row[] =$r->nama.$label_cover;
			}else{
				if ($r->stok>0){
					$row[] = "<a href='#' class='".$cls."' data-idobat='".$r->idbarang."' data-nama='".$r->nama."' data-idtipe='".$r->idtipe."' data-st_cover='".$r->cover."' data-dismiss='modal'>".$r->kode."</span></a>";
					$row[] = "<a href='#' class='".$cls."' data-idobat='".$r->idbarang."' data-nama='".$r->nama."' data-idtipe='".$r->idtipe."' data-st_cover='".$r->cover."' data-dismiss='modal'>".$r->nama.$label_cover."</span></a>";
				}else{
					// $row[] =$r->kode;
					// $row[] =$r->nama;
					$row[] = "<a href='#' class='".$cls."' data-idobat='".$r->idbarang."' data-nama='".$r->nama."' data-idtipe='".$r->idtipe."' data-st_cover='".$r->cover."' data-dismiss='modal'>".$r->kode."</span></a>";
					$row[] = "<a href='#' class='".$cls."' data-idobat='".$r->idbarang."' data-nama='".$r->nama."' data-idtipe='".$r->idtipe."' data-st_cover='".$r->cover."' data-dismiss='modal'>".$r->nama.$label_cover."</span></a>";
				}
			}
			
			$row[] = $r->nama_merk;
			$row[] = $r->nama_bentuk_sediaan;
			$row[] = $r->satuan;

			$row[] = WarningStok($r->stok, $r->stokreorder, $r->stokminimum);
			$row[] = number_format(ceiling($r->harga,100),0);
			
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_all(),
			"data" => $data
		);
		echo json_encode($output);
	}
  function batal_eresep(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $st_edited=$this->input->post('st_edited');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		
	  );
	   if ($jml_edit=='0'){
		  $data['edited_ppa']=null;
		  $data['edited_date']=null;
		  $data['alasan_edit_id']='';
		  $data['keterangan_edit']='';
		  
	  }
	  if ($st_edited=='1'){
		  $data['status_assemen']='2';
	  }else{
		  $data['status_assemen']='0';
		  
	  }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_e_resep',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function update_batal_tidak_diambil(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	 
	  $data['status_tindakan']='1';
	 
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_e_resep',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  function hapus_assesmen(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
	  $st_edited=$this->input->post('st_edited');
	  $jml_edit=$this->input->post('jml_edit');
	  $data=array(
		'deleted_ppa' => $login_ppa_id,
		'deleted_date' => date('Y-m-d H:i:s'),
		'status_assemen' => 0,
		'st_proses' => 0,
		
	  );
	   // if ($jml_edit=='0'){
		  // $data['edited_ppa']=null;
		  // $data['edited_date']=null;
		  // $data['alasan_edit_id']='';
		  // $data['keterangan_edit']='';
		  
	  // }
	  // if ($st_edited=='1'){
		  // $data['status_assemen']='2';
	  // }else{
		  // $data['status_assemen']='0';
		  
	  // }
	  $this->db->where('assesmen_id',$assesmen_id);
	  $hasil=$this->db->update('tpoliklinik_e_resep',$data);
	  $this->output->set_output(json_encode($hasil));
  }
  public function get_obat()
    {
        $cari 	= $this->input->post('search');
        $tujuan_farmasi_id 	= $this->input->post('tujuan_farmasi_id');
        $idtipe_poli 	= $this->input->post('idtipe_poli');
        $idpoliklinik 	= $this->input->post('idpoliklinik');
        $idkelompokpasien 	= $this->input->post('idkelompokpasien');
        $idrekanan 	= $this->input->post('idrekanan');
        // $data_obat = $this->tpaspen->get_obat($cari);
		// $q="SELECT B.kode,S.*,B.nama,MS.nama as nama_satuan,B.hargadasar,B.marginasuransi 
			// ,(CASE 
			// WHEN $idkelompokpasien=5 THEN ((B.marginumum/100) + 1) * B.hargadasar 
			// WHEN $idkelompokpasien=4 THEN ((B.marginbpjstenagakerja/100) + 1) * B.hargadasar 
			// WHEN $idkelompokpasien=3 THEN ((B.marginbpjskesehatan/100) + 1) * B.hargadasar 
			// WHEN $idkelompokpasien=2 THEN ((B.marginjasaraharja/100) + 1) * B.hargadasar 
			// WHEN $idkelompokpasien=1 THEN ((B.marginasuransi/100) + 1) * B.hargadasar 
			// END) as harga
			// ,CASE WHEN BS.nilai THEN BS.ref ELSE '-' END as bentuk_sediaan
			// ,'".'<span class="badge badge-success pull-right">1</span>'."' as notif
			// FROM mtujuan_farmasi H
			// INNER JOIN mgudang_stok S ON S.idunitpelayanan=H.idunit
			// INNER JOIN view_barang_farmasi_resep B ON B.id=S.idbarang AND B.idtipe=S.idtipe
			// LEFT JOIN merm_referensi BS ON BS.nilai=B.bentuk_sediaan AND BS.ref_head_id='94'
			// LEFT JOIN msatuan MS ON MS.id=B.idsatuankecil
			// WHERE H.id='$tujuan_farmasi_id' AND S.`status`='1' AND B.nama LIKE '%".$cari."%'";
		$q="SELECT *FROM (
				SELECT B.kode,S.*,B.idkategori,B.nama,MS.nama as nama_satuan,B.hargadasar,B.marginasuransi 
				,(CASE 
				WHEN 1=5 THEN ((B.marginumum/100) + 1) * B.hargadasar 
				WHEN 1=4 THEN ((B.marginbpjstenagakerja/100) + 1) * B.hargadasar 
				WHEN 1=3 THEN ((B.marginbpjskesehatan/100) + 1) * B.hargadasar 
				WHEN 1=2 THEN ((B.marginjasaraharja/100) + 1) * B.hargadasar 
				WHEN 1=1 THEN ((B.marginasuransi/100) + 1) * B.hargadasar 
				END) as harga
				,CASE WHEN BS.nilai THEN BS.ref ELSE '-' END as bentuk_sediaan,
				H.idtipe_barang,
				compare_value_12(
				MAX(IF(H.idkategori_barang=B.idkategori AND H.idbarang=S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='$idpoliklinik' AND H.idkelompokpasien='$idkelompokpasien' AND H.idrekanan='$idrekanan',H.cover,null))
				,MAX(IF(H.idkategori_barang=B.idkategori AND H.idbarang=S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='$idpoliklinik' AND H.idkelompokpasien='$idkelompokpasien' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang=B.idkategori AND H.idbarang=S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='$idpoliklinik' AND H.idkelompokpasien='0' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang=B.idkategori AND H.idbarang=S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang=B.idkategori AND H.idbarang=S.idbarang AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang=B.idkategori AND H.idbarang='0' AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang='0' AND H.idbarang=S.idbarang AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang='0' AND H.idbarang=S.idbarang AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang='0' AND H.idbarang=S.idbarang AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND (H.idpoli_kelas='0' OR H.idpoli_kelas='$idpoliklinik') AND H.idkelompokpasien='0' AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang='0' AND (H.idbarang=S.idbarang OR H.idbarang='0') AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND (H.idpoli_kelas='0' OR H.idpoli_kelas='$idpoliklinik') AND (H.idkelompokpasien='0' OR H.idkelompokpasien='$idkelompokpasien' ) AND H.idrekanan='0',H.cover,null))
				,MAX(IF(H.idkategori_barang='0' AND (H.idbarang=S.idbarang OR H.idbarang='0') AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND (H.idpoli_kelas='0' OR H.idpoli_kelas='$idpoliklinik') AND (H.idkelompokpasien='0' OR H.idkelompokpasien='$idkelompokpasien' ) AND (H.idrekanan='0' OR H.idrekanan='$idrekanan'),H.cover,null))
				,MAX(IF(H.idkategori_barang='0' AND  H.idbarang='0' AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0' ,H.cover,null))
				) as cover
				FROM mtujuan_farmasi HTF
				INNER JOIN mgudang_stok S ON S.idunitpelayanan=HTF.idunit
				INNER JOIN view_barang_farmasi_resep B ON B.id=S.idbarang AND B.idtipe=S.idtipe
				LEFT JOIN merm_referensi BS ON BS.nilai=B.bentuk_sediaan AND BS.ref_head_id='94'
				LEFT JOIN msatuan MS ON MS.id=B.idsatuankecil
				LEFT JOIN setting_cover_barang  H ON
				H.idtipe_barang = S.idtipe AND
				(
				(H.idkategori_barang = B.idkategori AND H.idbarang = S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='$idpoliklinik' AND H.idkelompokpasien='$idkelompokpasien' AND H.idrekanan='$idrekanan') OR #1
				(H.idkategori_barang = B.idkategori AND H.idbarang = S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='$idpoliklinik' AND H.idkelompokpasien='$idkelompokpasien' AND H.idrekanan='0') OR #2
				(H.idkategori_barang = B.idkategori AND H.idbarang = S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='$idpoliklinik' AND H.idkelompokpasien='0' AND H.idrekanan='0') OR #3
				(H.idkategori_barang = B.idkategori AND H.idbarang = S.idbarang AND H.asal_pasien='$idtipe_poli' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0') OR #4
				(H.idkategori_barang = B.idkategori AND H.idbarang = S.idbarang AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0') OR #5
				(H.idkategori_barang = B.idkategori AND H.idbarang = '0' 			 AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0') OR #6
				(H.idkategori_barang = '0' 		  		AND H.idbarang = S.idbarang AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0') OR #7
				(H.idkategori_barang = '0' 		  		AND H.idbarang = S.idbarang AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0') OR #8
				(H.idkategori_barang = '0' 		  		AND H.idbarang = S.idbarang AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND (H.idpoli_kelas='0' OR H.idpoli_kelas='$idpoliklinik') 
																						AND H.idkelompokpasien='0' AND H.idrekanan='0') OR #9
				(H.idkategori_barang='0' 			 		  AND (H.idbarang = S.idbarang OR H.idbarang='0') AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND (H.idpoli_kelas='0' OR H.idpoli_kelas='$idpoliklinik') 	
																						AND (H.idkelompokpasien='0' OR H.idkelompokpasien='$idkelompokpasien' ) AND H.idrekanan='0') OR #10
				(H.idkategori_barang='0'	 		  		AND (H.idbarang=S.idbarang OR H.idbarang='0') AND (H.asal_pasien='0' OR H.asal_pasien='$idtipe_poli') AND (H.idpoli_kelas='0' OR H.idpoli_kelas='$idpoliklinik') 
																						AND (H.idkelompokpasien='0' OR H.idkelompokpasien='$idkelompokpasien' ) AND (H.idrekanan='0' OR H.idrekanan='$idrekanan')) OR
				(H.idkategori_barang='0' 						AND  H.idbarang='0' AND H.asal_pasien='0' AND H.idpoli_kelas='0' AND H.idkelompokpasien='0' AND H.idrekanan='0')
				) 

				WHERE HTF.id='$tujuan_farmasi_id' AND S.status='1' 
				
				AND B.nama LIKE '%".$cari."%'
				GROUP BY S.idbarang
				) T 
				WHERE (T.cover IS NULL OR T.cover='2' OR T.cover='1')
				
				";
		$data_obat=$this->db->query($q)->result_array();
        $this->output->set_output(json_encode($data_obat));
    }
	public function get_obat_detail()
    {
		$id=$this->input->post('idbarang');
		$idtipe=$this->input->post('idtipe');
		$tujuan_id=$this->input->post('tujuan_id');
		$q="SELECT H.*,S.stok,S.idunitpelayanan FROM view_barang_farmasi_resep H
			LEFT JOIN mgudang_stok S ON S.idtipe=H.idtipe AND S.idbarang=H.id
			WHERE H.idtipe='$idtipe' AND H.id='$id' AND S.idunitpelayanan=(SELECT T.idunit FROM mtujuan_farmasi T WHERE T.id='$tujuan_id')";
		// print_r($q);exit;
		$data_obat=$this->db->query($q)->row_array();
        $this->output->set_output(json_encode($data_obat));
    }
	public function get_info_paket()
    {
		$q="SELECT * FROM tpoliklinik_e_resep_paket H WHERE H.created_by='14' AND H.status_paket='1'";
		$data=$this->db->query($q)->row_array();
		if (!($data)){
			$data=array(
				'paket_id' =>'', 
				'nama_paket' =>'', 
			);
		}
		
        $this->output->set_output(json_encode($data));
    }
	function create_paket(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  
	  
	  $nama_paket=$this->input->post('nama_paket');
	 
	  $data=array(
		'nama_paket' => $nama_paket,
		'created_by' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_paket' => 1,
	  );
	  $hasil=$this->db->insert('tpoliklinik_e_resep_paket',$data);
	  if ($hasil){
		  $paket_id=$this->db->insert_id();
		  $data['paket_id']=$paket_id;
	  }else{
		  $data=null;
	  }
	  
	  $this->output->set_output(json_encode($data));
    }
	function batal_paket(){
		$paket_id=$this->input->post('paket_id');
	 
	  $data=array(
		'status_paket' => 0,
	  );
	  $this->db->where('paket_id',$paket_id);
	  $hasil=$this->db->update('tpoliklinik_e_resep_paket',$data);
	  if ($hasil){
		  // $paket_id=$this->db->insert_id();
		  // $data['paket_id']=$paket_id;
	  }else{
		  $data=null;
	  }
	  
	  $this->output->set_output(json_encode($data));
	}
	function gunakan_paket(){
	   $login_ppa_id=$this->session->userdata('login_ppa_id');
		$paket_id=$this->input->post('paket_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$idpasien=$this->input->post('idpasien');
		$q="SELECT H.* FROM `tpoliklinik_e_resep_paket_obat` H  
			LEFT JOIN tpoliklinik_e_resep_obat D ON D.idtipe=H.idtipe AND D.idbarang=H.idbarang AND D.assesmen_id='$assesmen_id'
			WHERE H.paket_id='$paket_id' AND D.id IS NULL";
		// print_r($q);exit;
		$list_barang=$this->db->query($q)->result();
		$hasil=false;
		foreach($list_barang as $r){
			$data=array(
				'assesmen_id' => $assesmen_id,
				'pendaftaran_id' => $pendaftaran_id,
				'idpasien' => $idpasien,
				'idbarang' => $r->idbarang,
				'idtipe' => $r->idtipe,
				'nama' => $r->nama,
				'jumlah' => $r->jumlah,
				'dosis' => $r->dosis,
				'interval' => $r->interval,
				'rute' => $r->rute,
				'aturan_tambahan' => $r->aturan_tambahan,
				'iter' => $r->iter,
				'cover' => $r->cover,
				'paket_id' => $r->paket_id,
				'paket_id_detail' => $r->id,

				'created_ppa' => $login_ppa_id,
				'created_date' => date('Y-m-d H:i:s'),
			);

			$hasil=$this->db->insert('tpoliklinik_e_resep_obat',$data);
		}
		$this->output->set_output(json_encode($hasil));
  }
  function copy_history_eresep(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	   $login_ppa_id=$this->session->userdata('login_ppa_id');
		$template_assesmen_id=$this->input->post('template_assesmen_id');
		$paket_id=$this->input->post('paket_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$idpasien=$this->input->post('idpasien');
		$q="INSERT INTO tpoliklinik_e_resep 
			(pendaftaran_id,idpasien,created_date,created_ppa,template_id,status_assemen,tanggal_permintaan,tujuan_farmasi_id,iddokter_resep,berat_badan,tinggi_badan,luas_permukaan,diagnosa,menyusui,gangguan_ginjal,st_puasa,st_alergi,alergi,prioritas_cito,resep_pulang,catatan_resep)
			SELECT '$pendaftaran_id','$idpasien',NOW(),'$login_ppa_id','$template_assesmen_id',1,NOW(),tujuan_farmasi_id,iddokter_resep,berat_badan,tinggi_badan,luas_permukaan,diagnosa,menyusui,gangguan_ginjal,st_puasa,st_alergi,alergi,prioritas_cito,resep_pulang,catatan_resep
			FROM tpoliklinik_e_resep WHERE assesmen_id='$template_assesmen_id'";
		$this->db->query($q);
		$assesmen_id=$this->db->insert_id();
		
		$q="SELECT H.* FROM `tpoliklinik_e_resep_obat` H  WHERE H.assesmen_id='$template_assesmen_id'";
		// print_r($q);exit;
		$list_barang=$this->db->query($q)->result();
		$hasil=false;
		foreach($list_barang as $r){
			$data=array(
				'assesmen_id' => $assesmen_id,
				'pendaftaran_id' => $pendaftaran_id,
				'idpasien' => $idpasien,
				'idbarang' => $r->idbarang,
				'idtipe' => $r->idtipe,
				'nama' => $r->nama,
				'jumlah' => $r->jumlah,
				'dosis' => $r->dosis,
				'interval' => $r->interval,
				'rute' => $r->rute,
				'aturan_tambahan' => $r->aturan_tambahan,
				'iter' => $r->iter,
				'cover' => $r->cover,
				'created_ppa' => $login_ppa_id,
				'created_date' => date('Y-m-d H:i:s'),
			);

			$hasil=$this->db->insert('tpoliklinik_e_resep_obat',$data);
		}
		
		$q="SELECT H.* FROM `tpoliklinik_e_resep_obat_racikan` H  WHERE H.assesmen_id='$template_assesmen_id'";
		// print_r($q);exit;
		$list_barang=$this->db->query($q)->result();
		$hasil=false;
		foreach($list_barang as $r){
			$data=array(
				'assesmen_id' => $assesmen_id,
				'pendaftaran_id' => $pendaftaran_id,
				'idpasien' => $idpasien,
				'nama_racikan' => $r->nama_racikan,
				'jumlah_racikan' => $r->jumlah_racikan,
				'jenis_racikan' => $r->jenis_racikan,
				'interval_racikan' => $r->interval_racikan,
				'rute_racikan' => $r->rute_racikan,
				'aturan_tambahan_racikan' => $r->aturan_tambahan_racikan,
				'iter_racikan' => $r->iter_racikan,
				'status_racikan' => 2,
				'created_ppa' => $login_ppa_id,
				'created_date' => date('Y-m-d H:i:s'),
			);
			$racikan_id_asal=$r->racikan_id;
			$hasil=$this->db->insert('tpoliklinik_e_resep_obat_racikan',$data);
			$racikan_id=$this->db->insert_id();
			
			$q="SELECT H.* FROM `tpoliklinik_e_resep_obat_racikan_obat` H  WHERE H.racikan_id='$racikan_id_asal'";
			// print_r($q);exit;
			$list_barang_detail=$this->db->query($q)->result();
			foreach($list_barang_detail as $row){
				$data_detail=array(
					'assesmen_id' => $assesmen_id,
					'pendaftaran_id' => $pendaftaran_id,
					'idpasien' => $idpasien,
					'racikan_id' => $racikan_id,
					'idbarang' => $row->idbarang,
					'idtipe' => $row->idtipe,
					'nama' => $row->nama,
					'dosis_master' => $row->dosis_master,
					'dosis' => $row->dosis,
					'jumlah' => $row->jumlah,
					'p1' => $row->p1,
					'p2' => $row->p2,
					'cover' => $row->cover,
					'created_ppa' => $login_ppa_id,
					'created_date' => date('Y-m-d H:i:s'),
				);
				$this->db->insert('tpoliklinik_e_resep_obat_racikan_obat',$data_detail);
			}
		}
		$this->output->set_output(json_encode($hasil));
  }
  function terapkan_history_obat(){
	   $login_ppa_id=$this->session->userdata('login_ppa_id');
		$id=$this->input->post('id');
		$paket_id=$this->input->post('paket_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$idpasien=$this->input->post('idpasien');
		$q="SELECT H.* FROM `tpoliklinik_e_resep_obat` H  
			LEFT JOIN tpoliklinik_e_resep_obat D ON D.idtipe=H.idtipe AND D.idbarang=H.idbarang AND D.assesmen_id='$assesmen_id'
			WHERE H.id='$id' AND D.id IS NULL";
		// print_r($q);exit;
		$list_barang=$this->db->query($q)->result();
		$hasil=0;
		foreach($list_barang as $r){
			$data=array(
				'assesmen_id' => $assesmen_id,
				'pendaftaran_id' => $pendaftaran_id,
				'idpasien' => $idpasien,
				'idbarang' => $r->idbarang,
				'idtipe' => $r->idtipe,
				'nama' => $r->nama,
				'jumlah' => $r->jumlah,
				'dosis' => $r->dosis,
				'interval' => $r->interval,
				'rute' => $r->rute,
				'aturan_tambahan' => $r->aturan_tambahan,
				'iter' => $r->iter,
				'cover' => $r->cover,
				'asal_data_id' => $id,
				'created_ppa' => $login_ppa_id,
				'created_date' => date('Y-m-d H:i:s'),
			);

			$this->db->insert('tpoliklinik_e_resep_obat',$data);
			$hasil=$hasil+1;
		}
		$this->output->set_output(json_encode($hasil));
  }
  function add_barang_paket(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$paket_id=$this->input->post('paket_id');
		$data=array(
			'paket_id' => $paket_id,
			'idbarang' => $this->input->post('idbarang'),
			'idtipe' => $this->input->post('idtipe'),
			'nama' => $this->input->post('nama'),
			'jumlah' => $this->input->post('jumlah'),
			'dosis' => $this->input->post('dosis'),
			'interval' => $this->input->post('interval'),
			'rute' => $this->input->post('rute'),
			'aturan_tambahan' => $this->input->post('aturan_tambahan'),
			'iter' => $this->input->post('iter'),
			'cover' => $this->input->post('cover'),

			'created_ppa' => $login_ppa_id,
			'created_date' => date('Y-m-d H:i:s'),
		);
	  
		$hasil=$this->db->insert('tpoliklinik_e_resep_paket_obat',$data);
		$this->output->set_output(json_encode($hasil));
  }
  function update_record_obat_paket(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$transaksi_id=$this->input->post('transaksi_id');
		
		$dosis=$this->input->post('dosis');
		$jumlah=$this->input->post('jumlah');
		$aturan_tambahan=$this->input->post('aturan_tambahan');
		$iter=$this->input->post('iter');
		$rute=$this->input->post('rute');
		$data=array(
			
			'jumlah' => $this->input->post('jumlah'),
			'dosis' => $this->input->post('dosis'),
			'interval' => $this->input->post('interval'),
			'rute' => $this->input->post('rute'),
			'aturan_tambahan' => $this->input->post('aturan_tambahan'),
			'iter' => $this->input->post('iter'),
		);
		$this->db->where('id',$transaksi_id);
		$hasil=$this->db->update('tpoliklinik_e_resep_paket_obat',$data);
		$this->output->set_output(json_encode($hasil));
  }
  function list_index_obat_paket(){
		$btn_disabel_edit='';
		$max_iter=$this->input->post('max_iter');
		$paket_id=$this->input->post('paket_id');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$this->select = array();
		$from="
			(
				SELECT H.*,B.nama as nama_barang,B.hargadasar
				,BS.ref as sediaan_nama
				,MR.ref as nama_rute
				,MI.nama as nama_interval
				,(CASE 
					WHEN $idkelompokpasien=5 THEN ((B.marginumum/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=4 THEN ((B.marginbpjstenagakerja/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=3 THEN ((B.marginbpjskesehatan/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=2 THEN ((B.marginjasaraharja/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=1 THEN ((B.marginasuransi/100) + 1) * B.hargadasar 
					END) as harga
				FROM tpoliklinik_e_resep_paket_obat H 
				LEFT JOIN view_barang_farmasi_resep B ON B.id=H.idbarang AND B.idtipe=H.idtipe
				LEFT JOIN merm_referensi BS ON BS.nilai=B.bentuk_sediaan AND BS.ref_head_id='94'
				LEFT JOIN merm_referensi MR ON MR.nilai=H.rute AND MR.ref_head_id='74'
				LEFT JOIN minterval MI ON MI.id=H.interval 
				WHERE H.paket_id='$paket_id'
				ORDER BY H.id ASC
			) as tbl
		";
		$this->from   = $from;
		$this->join 	= array();$this->order  = array();$this->group  = array();
		$this->column_search = array('nama');$this->column_order  = array();
		$list = $this->datatable->get_datatables(true);
		$data = array();$no = $_POST['start'];
		$data_interval=get_all('minterval');
		$data_rute=list_variable_ref(74);
		foreach ($list as $r) {	
			$no++;$result = array();
			$btn_info='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_info_obat('.$r->idtipe.','.$r->idbarang.')" type="button" data-toggle="tooltip" title="Informasi Obat" data-original-title="Informasi Obat"><i class="fa fa-info"></i></button>';
			$input='<input type="hidden" class="cls_idtipe" value="'.$r->idtipe.'">';
			$input .='<input type="hidden" class="cls_idobat" value="'.$r->idbarang.'">';
			$input .='<input type="hidden" class="cls_idtrx" value="'.$r->id.'">';
			$aksi='';
			$aksi = '<div class="btn-group">';
			$aksi .= '<button  onclick="hapus_obat_paket('.$r->id.')" type="button" title="Hapus" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			$aksi .= '</div>';
			$result[] = ($no);
			$result[] = '<span>'.($r->nama_barang).$btn_info.'</span><br><span class="text-primary">Sediaan : '.($r->sediaan_nama?$r->sediaan_nama:'-').' | Rp. '.number_format(ceiling($r->harga,100)).' </span>'.status_cover($r->cover);
			$result[] = '<input type="text" class="dosis_paket number cls_racikan_update text-center" value="'.$r->dosis.'">';
			$result[] = '<input type="text" class="jumlah_paket number cls_racikan_update text-center" value="'.$r->jumlah.'">';
			$result[] = $this->opsi_header_paket($data_interval,$r->interval,'interval');
			$result[] = $this->opsi_header_paket($data_rute,$r->rute,'rute');
			$result[] = '<input type="text" class="aturan_tambahan_paket cls_racikan_update text-center" value="'.$r->aturan_tambahan.'">';
			$result[] = (opsi_iter_record_paket($max_iter,$r->iter,'','iter'));
			$result[] = $aksi.$input;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
	}
	function list_cari_rersep(){
		$btn_disabel_edit='';
		$idpasien=$this->input->post('idpasien');
		$nopendaftaran=$this->input->post('nopendaftaran');
		$tanggal_kirim_1=$this->input->post('tanggal_kirim_1');
		$tanggal_kirim_2=$this->input->post('tanggal_kirim_2');
		$tanggal_1=$this->input->post('tanggal_1');
		$tanggal_2=$this->input->post('tanggal_2');
		$idpoliklinik=$this->input->post('idpoliklinik');
		$iddokter=$this->input->post('iddokter');
		$tujuan_farmasi_id=$this->input->post('tujuan_farmasi_id');
		$where='';
		if ($nopendaftaran!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$nopendaftaran."%'";
		}
		if ($tanggal_kirim_1!=''){
			$where .=" AND H.tanggal_kirim >='".YMDFormat($tanggal_kirim_1)."' AND H.tanggal_kirim <='".YMDFormat($tanggal_kirim_2)."'";
		}
		if ($tanggal_1!=''){
			$where .=" AND DATE(MP.tanggal) >='".YMDFormat($tanggal_1)."' AND MP.tanggal <='".YMDFormat($tanggal_2)."'";
		}
		if ($idpoliklinik!=''){
			$where .=" AND MP.idpoliklinik = '".$idpoliklinik."'";
		}
		if ($iddokter!=''){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($tujuan_farmasi_id!=''){
			$where .=" AND H.tujuan_farmasi_id = '".$tujuan_farmasi_id."'";
		}
		$this->select = array();
		$from="
			(
				SELECT H.assesmen_id,MP.nopendaftaran,MP.idtipe,H.tanggal_kirim,H.noresep 
				,MD.nama as nama_dokter,poli.nama as nama_poli,H.diagnosa
				,TF.nama_tujuan,C.ref as cito,H.prioritas_cito
				,CASE WHEN H.status_tindakan='5' THEN 1 WHEN H.st_ambil_sebagian=1 THEN 2 END as st_pemeriksaan,MD2.nama as nama_dokter_peminta
				,MU.nama as user_created,H.created_date
				FROM tpoliklinik_e_resep H
				INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
				INNER JOIN mdokter MD ON MD.id=MP.iddokter
				INNER JOIN mpoliklinik poli ON poli.id=MP.idpoliklinik
				INNER JOIN mtujuan_farmasi TF ON TF.id=H.tujuan_farmasi_id
				INNER JOIN merm_referensi C ON C.nilai=H.prioritas_cito AND C.ref_head_id='103'
				INNER JOIN mppa MD2 ON MD2.id=H.iddokter_resep
				INNER JOIN mppa MU ON MU.id=H.created_ppa
				WHERE (H.status_tindakan='5' OR H.st_ambil_sebagian='1') AND H.assesmen_id_resep IS NULL AND H.idpasien='$idpasien' ".$where."
				
				UNION ALL
				
				SELECT H.assesmen_id,MP.nopendaftaran,MP.idtipe,H.tanggal_kirim,H.noresep 
				,MD.nama as nama_dokter,poli.nama as nama_poli,H.diagnosa
				,TF.nama_tujuan,C.ref as cito,H.prioritas_cito
				,3 as st_pemeriksaan,MD2.nama as nama_dokter_peminta
				,MU.nama as user_created,H.created_date
				FROM tpoliklinik_e_resep H
				INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
				INNER JOIN mdokter MD ON MD.id=MP.iddokter
				INNER JOIN mpoliklinik poli ON poli.id=MP.idpoliklinik
				INNER JOIN mtujuan_farmasi TF ON TF.id=H.tujuan_farmasi_id
				INNER JOIN merm_referensi C ON C.nilai=H.prioritas_cito AND C.ref_head_id='103'
				INNER JOIN mppa MD2 ON MD2.id=H.iddokter_resep
				INNER JOIN mppa MU ON MU.id=H.created_ppa
				WHERE H.status_tindakan > 3 AND H.st_direct='0' AND (H.jumlah_iter > jumlah_iter_ambil) AND H.idpasien='$idpasien' ".$where."
			) as tbl
		";
		// print_r($from);exit;
		$this->from   = $from;
		$this->join 	= array();$this->order  = array();$this->group  = array();
		$this->column_search = array('noresep','nopendaftaran','nama_dokter_peminta');$this->column_order  = array();
		$list = $this->datatable->get_datatables(true);
		$data = array();$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;$result = array();
			$aksi='';
			$aksi='<button class="btn btn-xs btn-success	" onclick="ambil_resep('.$r->assesmen_id.','.$r->st_pemeriksaan.')" type="button" data-toggle="tooltip" title="Tambah"><i class="fa fa-plus"></i></button>';
			
			$result[] = ($aksi);
			$result[] = $r->nopendaftaran;
			$result[] = $r->noresep.'<br>'.HumanDateLong($r->tanggal_kirim);
			$result[] = GetTipePasienPiutangBiasa($r->idtipe).' - '.$r->nama_poli.'<br>'.$r->nama_dokter;
			$result[] = $r->nama_dokter.'<br>'.$r->diagnosa;
			$result[] = $r->nama_tujuan.'<br>'.($r->prioritas_cito=='1'?text_success($r->cito):text_danger($r->cito));
			$result[] = $this->text_status_pemeriksaan($r->st_pemeriksaan);
			$result[] = $r->user_created.'<br>'.HumanDateLong($r->created_date);
			
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
	}
	function text_status_pemeriksaan($id){
		$text='';
		if ($id=='1'){
			$text =text_default('BELUM DIPROSES');
		}
		if ($id=='2'){
			$text =text_warning('SEBAGIAN');
		}
		if ($id=='3'){
			$text =text_primary('ITER');
		}
		return $text;
	}
	function ambil_resep(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$idpasien=$this->input->post('idpasien');
		$assesmen_id=$this->input->post('assesmen_id');
		$assesmen_id_resep=$this->input->post('assesmen_id_resep');
		$st_pemeriksaan=$this->input->post('st_pemeriksaan');
		if ($st_pemeriksaan=='1'){//BELUM PENGAMBILAN 
			$q="SELECT *FROM tpoliklinik_e_resep_obat H WHERE H.assesmen_id='$assesmen_id_resep'";
			$list_data=$this->db->query($q)->result();
			foreach($list_data as $r){
				$data=array(
					'assesmen_id' => $assesmen_id,
					'pendaftaran_id' => $pendaftaran_id,
					'idpasien' => $idpasien,
					'idbarang' => $r->idbarang,
					'idtipe' => $r->idtipe,
					'nama' => $r->nama,
					'dosis' => $r->dosis,
					'jumlah' => $r->jumlah,
					'interval' => $r->interval,
					'rute' => $r->rute,
					'aturan_tambahan' => $r->aturan_tambahan,
					'iter' => $r->iter,
					'cover' => $r->cover,
					'created_ppa' => $login_ppa_id,
					'created_date' =>date('Y-m-d H:i:s'),
					'idunit' => $r->idunit,
					'carapakai' => $r->carapakai,
					'jenispenggunaan' => $r->jenispenggunaan,
					'waktu_minum' => $r->waktu_minum,
					'kuantitas' => $r->kuantitas,
					'harga_dasar' => $r->harga_dasar,
					'margin' => $r->margin,
					'harga_jual' => $r->harga_jual,
					'diskon' => $r->diskon,
					'diskon_rp' => $r->diskon_rp,
					'expire_date' => $r->expire_date,
					'tuslah' => $r->tuslah,
					'totalharga' => $r->totalharga,
					'sisa_obat' => 0,
				);
				$hasil=$this->db->insert('tpoliklinik_e_resep_obat',$data);
			}
			$q="SELECT *FROM tpoliklinik_e_resep_obat_racikan H WHERE H.assesmen_id='$assesmen_id_resep'";
			$list_data=$this->db->query($q)->result();
			foreach($list_data as $r){
				$racikan_id=$r->racikan_id;
				$data=array(
					'assesmen_id' => $assesmen_id,
					'pendaftaran_id' => $pendaftaran_id,
					'idpasien' => $idpasien,
					'nama_racikan' => $r->nama_racikan,
					'jumlah_racikan' => $r->jumlah_racikan,
					'jenis_racikan' => $r->jenis_racikan,
					'interval_racikan' => $r->interval_racikan,
					'rute_racikan' => $r->rute_racikan,
					'aturan_tambahan_racikan' => $r->aturan_tambahan_racikan,
					'iter_racikan' => $r->iter_racikan,
					'status_racikan' => $r->status_racikan,
					'created_ppa' => $r->created_ppa,
					'created_date' => $r->created_date,
					// 'st_perubahan' => $r->st_perubahan,
					// 'user_perubahan' => $r->user_perubahan,
					// 'alasan_perubahan' => $r->alasan_perubahan,
					'tanggal_perubahan' => $r->tanggal_perubahan,
					'st_setuju' => $r->st_setuju,
					'user_setuju' => $r->user_setuju,
					'tanggal_setuju' => $r->tanggal_setuju,
					'jenispenggunaan' => $r->jenispenggunaan,
					'waktu_minum' => $r->waktu_minum,
					'kuantitas' => $r->kuantitas,
					'harga' => $r->harga,
					'tuslah' => $r->tuslah,
					'expire_date' => $r->expire_date,
					'totalharga' => $r->totalharga,
					'sisa_obat' => $r->sisa_obat,
					'sisa_iter' => $r->sisa_iter,
				);
				$hasil=$this->db->insert('tpoliklinik_e_resep_obat_racikan',$data);
				$racikan_id_new=$this->db->insert_id();
				$q="SELECT *FROM tpoliklinik_e_resep_obat_racikan_obat H WHERE H.racikan_id='$racikan_id'";
				$list_data_detail=$this->db->query($q)->result();
				foreach($list_data_detail as $row){
					$data=array(
						'racikan_id' => $racikan_id_new,
						'assesmen_id' => $assesmen_id,
						'pendaftaran_id' => $pendaftaran_id,
						'idpasien' => $idpasien,
						'idbarang' => $row->idbarang,
						'idtipe' => $row->idtipe,
						'nama' => $row->nama,
						'dosis_master' => $row->dosis_master,
						'dosis' => $row->dosis,
						'jumlah' => $row->jumlah,
						'p1' => $row->p1,
						'p2' => $row->p2,
						'cover' => $row->cover,
						'created_ppa' => $row->created_ppa,
						'created_date' => $row->created_date,
						// 'st_perubahan' => $row->st_perubahan,
						// 'user_perubahan' => $row->user_perubahan,
						// 'alasan_perubahan' => $row->alasan_perubahan,
						'tanggal_perubahan' => $row->tanggal_perubahan,
						'st_setuju' => $row->st_setuju,
						'user_setuju' => $row->user_setuju,
						'tanggal_setuju' => $row->tanggal_setuju,
						'kuantitas' => $row->kuantitas,
						'harga_jual' => $row->harga_jual,
						'diskon' => $row->diskon,
						'diskon_rp' => $row->diskon_rp,
						'totalharga' => $row->totalharga,
						'harga_dasar' => $row->harga_dasar,
						'margin' => $row->margin,
						'sisa_obat' => $row->sisa_obat,

					);
					$hasil=$this->db->insert('tpoliklinik_e_resep_obat_racikan_obat',$data);
				}
			}
			$hasil=$this->db->query("UPDATE tpoliklinik_e_resep SET status_tindakan='6' WHERE assesmen_id='$assesmen_id_resep'");
			$hasil=$this->db->query("UPDATE tpoliklinik_e_resep SET assesmen_id_resep='$assesmen_id_resep' WHERE assesmen_id='$assesmen_id'");
			$this->output->set_output(json_encode($hasil));
			
		}
		if ($st_pemeriksaan=='2'){//SISA OBAT 
			$q="DELETE FROM tpoliklinik_e_resep_obat WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			$q="SELECT *FROM tpoliklinik_e_resep_obat H WHERE H.assesmen_id='$assesmen_id_resep' AND sisa_obat > 0";
			$list_data=$this->db->query($q)->result();
			foreach($list_data as $r){
				$data=array(
					'assesmen_id' => $assesmen_id,
					'pendaftaran_id' => $pendaftaran_id,
					'idpasien' => $idpasien,
					'idbarang' => $r->idbarang,
					'idtipe' => $r->idtipe,
					'nama' => $r->nama,
					'dosis' => $r->dosis,
					'jumlah' => $r->sisa_obat,
					'interval' => $r->interval,
					'rute' => $r->rute,
					'aturan_tambahan' => $r->aturan_tambahan,
					'iter' => $r->iter,
					'cover' => $r->cover,
					'created_ppa' => $login_ppa_id,
					'created_date' =>date('Y-m-d H:i:s'),
					'idunit' => $r->idunit,
					'carapakai' => $r->carapakai,
					'jenispenggunaan' => $r->jenispenggunaan,
					'waktu_minum' => $r->waktu_minum,
					'kuantitas' => $r->sisa_obat,
					'harga_dasar' => $r->harga_dasar,
					'margin' => $r->margin,
					'harga_jual' => $r->harga_jual,
					'diskon' => $r->diskon,
					'diskon_rp' => $r->diskon_rp,
					'expire_date' => $r->expire_date,
					'tuslah' => $r->tuslah,
					'totalharga' => ($r->sisa_obat * $r->harga_jual) - ($r->sisa_obat * $r->diskon_rp) + $r->tuslah,
					'sisa_obat' => 0,
					'idresep_diambil' => $r->id,
				);
				$hasil=$this->db->insert('tpoliklinik_e_resep_obat',$data);
			}
			$q="DELETE FROM tpoliklinik_e_resep_obat_racikan WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			$q="SELECT *FROM tpoliklinik_e_resep_obat_racikan H WHERE H.assesmen_id='$assesmen_id_resep' AND sisa_obat > 0";
			$list_data=$this->db->query($q)->result();
			foreach($list_data as $r){
				$racikan_id=$r->racikan_id;
				$data=array(
					'assesmen_id' => $assesmen_id,
					'pendaftaran_id' => $pendaftaran_id,
					'idpasien' => $idpasien,
					'nama_racikan' => $r->nama_racikan,
					'jumlah_racikan' => $r->sisa_obat,
					'jenis_racikan' => $r->jenis_racikan,
					'interval_racikan' => $r->interval_racikan,
					'rute_racikan' => $r->rute_racikan,
					'aturan_tambahan_racikan' => $r->aturan_tambahan_racikan,
					'iter_racikan' => $r->iter_racikan,
					'status_racikan' => $r->status_racikan,
					'created_ppa' => $r->created_ppa,
					'created_date' => $r->created_date,
					// 'st_perubahan' => $r->st_perubahan,
					// 'user_perubahan' => $r->user_perubahan,
					// 'alasan_perubahan' => $r->alasan_perubahan,
					'tanggal_perubahan' => $r->tanggal_perubahan,
					'st_setuju' => $r->st_setuju,
					'user_setuju' => $r->user_setuju,
					'tanggal_setuju' => $r->tanggal_setuju,
					'jenispenggunaan' => $r->jenispenggunaan,
					'waktu_minum' => $r->waktu_minum,
					'kuantitas' => $r->sisa_obat,
					'harga' => $r->harga,
					'tuslah' => $r->tuslah,
					'expire_date' => $r->expire_date,
					'totalharga' => ($r->harga * $r->sisa_obat) + $r->tuslah,
					'sisa_obat' => 0,
					'sisa_iter' => 0,
					'idresep_diambil' => $r->racikan_id,
				);
				$hasil=$this->db->insert('tpoliklinik_e_resep_obat_racikan',$data);
				$racikan_id_new=$this->db->insert_id();
				$q="SELECT *FROM tpoliklinik_e_resep_obat_racikan_obat H WHERE H.racikan_id='$racikan_id'";
				$list_data_detail=$this->db->query($q)->result();
				foreach($list_data_detail as $row){
					$data=array(
						'racikan_id' => $racikan_id_new,
						'assesmen_id' => $assesmen_id,
						'pendaftaran_id' => $pendaftaran_id,
						'idpasien' => $idpasien,
						'idbarang' => $row->idbarang,
						'idtipe' => $row->idtipe,
						'nama' => $row->nama,
						'dosis_master' => $row->dosis_master,
						'dosis' => $row->dosis,
						'jumlah' => $row->jumlah,
						'p1' => $row->p1,
						'p2' => $row->p2,
						'cover' => $row->cover,
						'created_ppa' => $row->created_ppa,
						'created_date' => $row->created_date,
						// 'st_perubahan' => $row->st_perubahan,
						// 'user_perubahan' => $row->user_perubahan,
						// 'alasan_perubahan' => $row->alasan_perubahan,
						'tanggal_perubahan' => $row->tanggal_perubahan,
						'st_setuju' => $row->st_setuju,
						'user_setuju' => $row->user_setuju,
						'tanggal_setuju' => $row->tanggal_setuju,
						'kuantitas' => $row->kuantitas,
						'harga_jual' => $row->harga_jual,
						'diskon' => $row->diskon,
						'diskon_rp' => $row->diskon_rp,
						'totalharga' => $row->totalharga,
						'harga_dasar' => $row->harga_dasar,
						'margin' => $row->margin,
						'sisa_obat' => $row->sisa_obat,

					);
					$hasil=$this->db->insert('tpoliklinik_e_resep_obat_racikan_obat',$data);
				}
			}
			$hasil=$this->db->query("UPDATE tpoliklinik_e_resep SET assesmen_id_resep='$assesmen_id_resep' WHERE assesmen_id='$assesmen_id'");
			$this->output->set_output(json_encode($hasil));
		}
		if ($st_pemeriksaan=='3'){//ITER 
			$q="DELETE FROM tpoliklinik_e_resep_obat WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			$q="SELECT *FROM tpoliklinik_e_resep_obat H WHERE H.assesmen_id='$assesmen_id_resep' AND H.iter > H.ambil_iter";
			$list_data=$this->db->query($q)->result();
			foreach($list_data as $r){
				$totalharga=($r->jumlah * $r->harga_jual) - ($r->jumlah * $r->diskon_rp) + $r->tuslah;
				$data=array(
					'assesmen_id' => $assesmen_id,
					'pendaftaran_id' => $pendaftaran_id,
					'idpasien' => $idpasien,
					'idbarang' => $r->idbarang,
					'idtipe' => $r->idtipe,
					'nama' => $r->nama,
					'dosis' => $r->dosis,
					'jumlah' => $r->jumlah,
					'interval' => $r->interval,
					'rute' => $r->rute,
					'aturan_tambahan' => $r->aturan_tambahan,
					'iter' => $r->iter,
					'cover' => $r->cover,
					'created_ppa' => $login_ppa_id,
					'created_date' =>date('Y-m-d H:i:s'),
					'idunit' => $r->idunit,
					'carapakai' => $r->carapakai,
					'jenispenggunaan' => $r->jenispenggunaan,
					'waktu_minum' => $r->waktu_minum,
					'kuantitas' => $r->jumlah,
					'harga_dasar' => $r->harga_dasar,
					'margin' => $r->margin,
					'harga_jual' => $r->harga_jual,
					'diskon' => $r->diskon,
					'diskon_rp' => $r->diskon_rp,
					'expire_date' => $r->expire_date,
					'tuslah' => $r->tuslah,
					'totalharga' => $totalharga,
					'sisa_obat' => 0,
					'ambil_iter' => 1,
					'iditer_obat' => $r->id,
				);
				$hasil=$this->db->insert('tpoliklinik_e_resep_obat',$data);
			}
			$q="DELETE FROM tpoliklinik_e_resep_obat_racikan WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			$q="SELECT *FROM tpoliklinik_e_resep_obat_racikan H WHERE H.assesmen_id='$assesmen_id_resep' AND H.iter_racikan > H.ambil_iter ";
			$list_data=$this->db->query($q)->result();
			foreach($list_data as $r){
				$racikan_id=$r->racikan_id;
				$totalharga=($r->jumlah_racikan * $r->harga) + $r->tuslah;
				$data=array(
					'assesmen_id' => $assesmen_id,
					'pendaftaran_id' => $pendaftaran_id,
					'idpasien' => $idpasien,
					'nama_racikan' => $r->nama_racikan,
					'jumlah_racikan' => $r->jumlah_racikan,
					'jenis_racikan' => $r->jenis_racikan,
					'interval_racikan' => $r->interval_racikan,
					'rute_racikan' => $r->rute_racikan,
					'aturan_tambahan_racikan' => $r->aturan_tambahan_racikan,
					'iter_racikan' => $r->iter_racikan,
					'status_racikan' => $r->status_racikan,
					'created_ppa' => $r->created_ppa,
					'created_date' => $r->created_date,
					// 'st_perubahan' => $r->st_perubahan,
					// 'user_perubahan' => $r->user_perubahan,
					// 'alasan_perubahan' => $r->alasan_perubahan,
					// 'tanggal_perubahan' => $r->tanggal_perubahan,
					'st_setuju' => $r->st_setuju,
					'user_setuju' => $r->user_setuju,
					'tanggal_setuju' => $r->tanggal_setuju,
					'jenispenggunaan' => $r->jenispenggunaan,
					'waktu_minum' => $r->waktu_minum,
					'kuantitas' => $r->jumlah_racikan,
					'harga' => $r->harga,
					'tuslah' => $r->tuslah,
					'expire_date' => $r->expire_date,
					'totalharga' => $r->totalharga,
					'sisa_obat' => 0,
					'ambil_iter' => 1,
					'iditer_obat' => $r->racikan_id,
				);
				$hasil=$this->db->insert('tpoliklinik_e_resep_obat_racikan',$data);
				$racikan_id_new=$this->db->insert_id();
				$q="SELECT *FROM tpoliklinik_e_resep_obat_racikan_obat H WHERE H.racikan_id='$racikan_id'";
				$list_data_detail=$this->db->query($q)->result();
				foreach($list_data_detail as $row){
					$data=array(
						'racikan_id' => $racikan_id_new,
						'assesmen_id' => $assesmen_id,
						'pendaftaran_id' => $pendaftaran_id,
						'idpasien' => $idpasien,
						'idbarang' => $row->idbarang,
						'idtipe' => $row->idtipe,
						'nama' => $row->nama,
						'dosis_master' => $row->dosis_master,
						'dosis' => $row->dosis,
						'jumlah' => $row->jumlah,
						'p1' => $row->p1,
						'p2' => $row->p2,
						'cover' => $row->cover,
						'created_ppa' => $row->created_ppa,
						'created_date' => $row->created_date,
						// 'st_perubahan' => $row->st_perubahan,
						// 'user_perubahan' => $row->user_perubahan,
						// 'alasan_perubahan' => $row->alasan_perubahan,
						// 'tanggal_perubahan' => $row->tanggal_perubahan,
						'st_setuju' => $row->st_setuju,
						'user_setuju' => $row->user_setuju,
						'tanggal_setuju' => $row->tanggal_setuju,
						'kuantitas' => $row->kuantitas,
						'harga_jual' => $row->harga_jual,
						'diskon' => $row->diskon,
						'diskon_rp' => $row->diskon_rp,
						'totalharga' => $row->totalharga,
						'harga_dasar' => $row->harga_dasar,
						'margin' => $row->margin,
						'sisa_obat' => $row->sisa_obat,

					);
					$hasil=$this->db->insert('tpoliklinik_e_resep_obat_racikan_obat',$data);
				}
			}
			// $hasil=$this->db->query("UPDATE tpoliklinik_e_resep SET status_tindakan='6' WHERE assesmen_id='$assesmen_id_resep'");
			$hasil=$this->db->query("UPDATE tpoliklinik_e_resep SET assesmen_id_resep='$assesmen_id_resep' WHERE assesmen_id='$assesmen_id'");
			$this->output->set_output(json_encode($hasil));
			
		}
		
	}
	function list_index_paket(){
		$btn_disabel_edit='';
		$this->select = array();
		$from="
			(
				SELECT H.*,MC.nama as user_created
				,ME.nama as user_edited

				FROM tpoliklinik_e_resep_paket H 
				LEFT JOIN mppa MC ON MC.id=H.created_by
				LEFT JOIN mppa ME ON ME.id=H.edited_by
				WHERE H.status_paket='2'
				ORDER BY H.paket_id DESC
			) as tbl
		";
		$this->from   = $from;
		$this->join 	= array();$this->order  = array();$this->group  = array();
		$this->column_search = array('nama_paket');$this->column_order  = array();
		$list = $this->datatable->get_datatables(true);
		$data = array();$no = $_POST['start'];
		$data_interval=get_all('minterval');
		$data_rute=list_variable_ref(74);
		foreach ($list as $r) {	
			$no++;$result = array();
			$btn_info='<button class="btn btn-xs btn-success	" onclick="list_data_paket_obat('.$r->paket_id.')" type="button" data-toggle="tooltip" title="Informasi Obat" data-original-title="Informasi Obat"><i class="fa fa-eye"></i></button>';
			$btn_info .='<button class="btn btn-xs btn-warning	" onclick="gunakan_paket('.$r->paket_id.')" type="button" data-toggle="tooltip" title="Gunakan Paket" data-original-title="Gunakan Paket"><i class="fa fa-copy"></i></button>';
			$btn_info .='<button class="btn btn-xs btn-danger	" onclick="batal_paket('.$r->paket_id.')" type="button" data-toggle="tooltip" title="Hapus Paket" data-original-title="Gunakan Paket"><i class="fa fa-trash"></i></button>';
			$user_created=($r->user_created?'<br>Created By : <span class="text-primary">'.$r->user_created.' | '.HumanDateLong($r->created_date).'</span>':'');
			$user_edited=($r->user_edited?'<br>Edited By : <span class="text-primary">'.$r->user_edited.' | '.HumanDateLong($r->edited_date).'</span>':'');
			$result[] = ($no);
			$result[] = '<span>'.($r->nama_paket).'</span>'.$user_created.$user_edited;
			
			$result[] = $btn_info;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
	}
	function opsi_header_paket($list_data,$id,$nama_class){
		$opsi='<select tabindex="8"  width="100%" class="js-select2 form-control '.$nama_class.' opsi_change_paket" style="width: 100%;" data-placeholder="Pilih Opsi" required>';
		foreach($list_data as $row){
			$opsi .='<option value="'.$row->id.'" '.($row->id==$id?'selected':'').'>'.$row->nama.'</option>';
		}
		$opsi .='</select>';
		return $opsi;
	}
	function hapus_obat_paket(){
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$hasil=$this->db->delete('tpoliklinik_e_resep_paket_obat');
		$this->output->set_output(json_encode($hasil));
	}
	function close_paket(){
		$paket_id=$this->input->post('paket_id');
		$this->db->where('paket_id',$paket_id);
		$hasil=$this->db->update('tpoliklinik_e_resep_paket',array('status_paket'=>2));
		$this->output->set_output(json_encode($hasil));
	}
	
	//TEMPLATE
	
	function get_info_template()
    {
		$q="SELECT * FROM tpoliklinik_e_resep_template H WHERE H.created_by='14' AND H.status_template='1'";
		$data=$this->db->query($q)->row_array();
		if (!($data)){
			$data=array(
				'template_id' =>'', 
				'nama_template' =>'', 
			);
		}
		
        $this->output->set_output(json_encode($data));
    }
	function create_template(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  
	  
	  $nama_template=$this->input->post('nama_template');
	 
	  $data=array(
		'nama_template' => $nama_template,
		'created_by' => $login_ppa_id,
		'created_date' => date('Y-m-d H:i:s'),
		'status_template' => 1,
	  );
	  $hasil=$this->db->insert('tpoliklinik_e_resep_template',$data);
	  if ($hasil){
		  $template_id=$this->db->insert_id();
		  $data['template_id']=$template_id;
	  }else{
		  $data=null;
	  }
	  
	  $this->output->set_output(json_encode($data));
    }
	function batal_template(){
		$template_id=$this->input->post('template_id');
	 
	  $data=array(
		'status_template' => 0,
	  );
	  $this->db->where('template_id',$template_id);
	  $hasil=$this->db->update('tpoliklinik_e_resep_template',$data);
	  if ($hasil){
		  // $template_id=$this->db->insert_id();
		  // $data['template_id']=$template_id;
	  }else{
		  $data=null;
	  }
	  
	  $this->output->set_output(json_encode($data));
	}
	function refresh_interval(){
		$q="SELECT * FROM `minterval` H WHERE H.`status`='1' ORDER BY H.nama ASC";
		$opsi='<option value="" selected>Pilih Opsi</option>';
		$list_data=$this->db->query($q)->result();
		foreach($list_data as $r){
			$opsi .='<option value="'.$r->id.'" >'.$r->nama.'</option>';
		}
		$data['opsi']=$opsi;
	  $this->output->set_output(json_encode($data));
	}
	function gunakan_template(){
	   $login_ppa_id=$this->session->userdata('template_id');
		$template_id=$this->input->post('template_id');
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$idpasien=$this->input->post('idpasien');
		$q="SELECT H.* FROM `tpoliklinik_e_resep_template_obat` H  
			LEFT JOIN tpoliklinik_e_resep_obat D ON D.idtipe=H.idtipe AND D.idbarang=H.idbarang AND D.assesmen_id='$assesmen_id'
			WHERE H.template_id='$template_id' AND D.id IS NULL";
		// print_r($q);exit;
		$list_barang=$this->db->query($q)->result();
		$hasil=false;
		foreach($list_barang as $r){
			$data=array(
				'assesmen_id' => $assesmen_id,
				'pendaftaran_id' => $pendaftaran_id,
				'idpasien' => $idpasien,
				'idbarang' => $r->idbarang,
				'idtipe' => $r->idtipe,
				'nama' => $r->nama,
				'jumlah' => $r->jumlah,
				'dosis' => $r->dosis,
				'interval' => $r->interval,
				'rute' => $r->rute,
				'aturan_tambahan' => $r->aturan_tambahan,
				'iter' => $r->iter,
				'cover' => $r->cover,
				'template_id' => $r->template_id,
				'template_id_detail' => $r->id,

				'created_ppa' => $login_ppa_id,
				'created_date' => date('Y-m-d H:i:s'),
			);

			$hasil=$this->db->insert('tpoliklinik_e_resep_obat',$data);
		}
		$this->output->set_output(json_encode($hasil));
  }
  function add_barang_template(){
		$login_ppa_id=$this->session->userdata('template_id');
		$template_id=$this->input->post('template_id');
		$data=array(
			'template_id' => $template_id,
			'idbarang' => $this->input->post('idbarang'),
			'idtipe' => $this->input->post('idtipe'),
			'nama' => $this->input->post('nama'),
			'jumlah' => $this->input->post('jumlah'),
			'dosis' => $this->input->post('dosis'),
			'interval' => $this->input->post('interval'),
			'rute' => $this->input->post('rute'),
			'aturan_tambahan' => $this->input->post('aturan_tambahan'),
			'iter' => $this->input->post('iter'),
			'cover' => $this->input->post('cover'),

			'created_ppa' => $login_ppa_id,
			'created_date' => date('Y-m-d H:i:s'),
		);
	  
		$hasil=$this->db->insert('tpoliklinik_e_resep_template_obat',$data);
		$this->output->set_output(json_encode($hasil));
  }
  function update_record_obat_template(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$transaksi_id=$this->input->post('transaksi_id');
		
		$dosis=$this->input->post('dosis');
		$jumlah=$this->input->post('jumlah');
		$aturan_tambahan=$this->input->post('aturan_tambahan');
		$iter=$this->input->post('iter');
		$rute=$this->input->post('rute');
		$data=array(
			
			'jumlah' => $this->input->post('jumlah'),
			'dosis' => $this->input->post('dosis'),
			'interval' => $this->input->post('interval'),
			'rute' => $this->input->post('rute'),
			'aturan_tambahan' => $this->input->post('aturan_tambahan'),
			'iter' => $this->input->post('iter'),
		);
		$this->db->where('id',$transaksi_id);
		$hasil=$this->db->update('tpoliklinik_e_resep_template_obat',$data);
		$this->output->set_output(json_encode($hasil));
  }
  function list_index_obat_template(){
		$btn_disabel_edit='';
		$max_iter=$this->input->post('max_iter');
		$template_id=$this->input->post('template_id');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		$this->select = array();
		$from="
			(
				SELECT H.*,B.nama as nama_barang,B.hargadasar
				,BS.ref as sediaan_nama
				,MR.ref as nama_rute
				,MI.nama as nama_interval
				,(CASE 
					WHEN $idkelompokpasien=5 THEN ((B.marginumum/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=4 THEN ((B.marginbpjstenagakerja/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=3 THEN ((B.marginbpjskesehatan/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=2 THEN ((B.marginjasaraharja/100) + 1) * B.hargadasar 
					WHEN $idkelompokpasien=1 THEN ((B.marginasuransi/100) + 1) * B.hargadasar 
					END) as harga
				FROM tpoliklinik_e_resep_template_obat H 
				LEFT JOIN view_barang_farmasi_resep B ON B.id=H.idbarang AND B.idtipe=H.idtipe
				LEFT JOIN merm_referensi BS ON BS.nilai=B.bentuk_sediaan AND BS.ref_head_id='94'
				LEFT JOIN merm_referensi MR ON MR.nilai=H.rute AND MR.ref_head_id='74'
				LEFT JOIN minterval MI ON MI.id=H.interval 
				WHERE H.template_id='$template_id'
				ORDER BY H.id ASC
			) as tbl
		";
		$this->from   = $from;
		$this->join 	= array();$this->order  = array();$this->group  = array();
		$this->column_search = array('nama');$this->column_order  = array();
		$list = $this->datatable->get_datatables(true);
		$data = array();$no = $_POST['start'];
		$data_interval=get_all('minterval');
		$data_rute=list_variable_ref(74);
		foreach ($list as $r) {	
			$no++;$result = array();
			$btn_info='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_info_obat('.$r->idtipe.','.$r->idbarang.')" type="button" data-toggle="tooltip" title="Informasi Obat" data-original-title="Informasi Obat"><i class="fa fa-info"></i></button>';
			$input='<input type="hidden" class="cls_idtipe" value="'.$r->idtipe.'">';
			$input .='<input type="hidden" class="cls_idobat" value="'.$r->idbarang.'">';
			$input .='<input type="hidden" class="cls_idtrx" value="'.$r->id.'">';
			$aksi='';
			$aksi = '<div class="btn-group">';
			$aksi .= '<button  onclick="hapus_obat_template('.$r->id.')" type="button" title="Hapus" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			$aksi .= '</div>';
			$result[] = ($no);
			$result[] = '<span>'.($r->nama_barang).$btn_info.'</span><br><span class="text-primary">Sediaan : '.($r->sediaan_nama?$r->sediaan_nama:'-').' | Rp. '.number_format(ceiling($r->harga,100)).' </span>'.status_cover($r->cover);
			$result[] = '<input type="text" class="dosis_template number cls_racikan_update text-center" value="'.$r->dosis.'">';
			$result[] = '<input type="text" class="jumlah_template number cls_racikan_update text-center" value="'.$r->jumlah.'">';
			$result[] = $this->opsi_header_template($data_interval,$r->interval,'interval');
			$result[] = $this->opsi_header_template($data_rute,$r->rute,'rute');
			$result[] = '<input type="text" class="aturan_tambahan_template cls_racikan_update text-center" value="'.$r->aturan_tambahan.'">';
			$result[] = (opsi_iter_record_paket($max_iter,$r->iter,'','iter'));
			$result[] = $aksi.$input;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
	}
	function list_index_template(){
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$btn_disabel_edit='';
		$this->select = array();
		$from="
			(
				SELECT H.*,MC.nama as user_created
				,ME.nama as user_edited

				FROM tpoliklinik_e_resep_template H 
				LEFT JOIN mppa MC ON MC.id=H.created_by
				LEFT JOIN mppa ME ON ME.id=H.edited_by
				WHERE H.status_template='2' AND H.created_by='$login_ppa_id'
				ORDER BY H.template_id DESC 
			) as tbl
		";
		$this->from   = $from;
		$this->join 	= array();$this->order  = array();$this->group  = array();
		$this->column_search = array('nama_template');$this->column_order  = array();
		$list = $this->datatable->get_datatables(true);
		$data = array();$no = $_POST['start'];
		$data_interval=get_all('minterval');
		$data_rute=list_variable_ref(74);
		foreach ($list as $r) {	
			$no++;$result = array();
			$btn_info='<button class="btn btn-xs btn-success	" onclick="list_data_template_obat('.$r->template_id.')" type="button" data-toggle="tooltip" title="Informasi Obat" data-original-title="Informasi Obat"><i class="fa fa-eye"></i></button>';
			$btn_info .='<button class="btn btn-xs btn-warning	" onclick="gunakan_template('.$r->template_id.')" type="button" data-toggle="tooltip" title="Gunakan Paket" data-original-title="Gunakan Paket"><i class="fa fa-copy"></i></button>';
			$btn_info .='<button class="btn btn-xs btn-danger	" onclick="batal_template('.$r->template_id.')" type="button" data-toggle="tooltip" title="Hapus Paket" data-original-title="Gunakan Paket"><i class="fa fa-trash"></i></button>';
			$user_created=($r->user_created?'<br>Created By : <span class="text-primary">'.$r->user_created.' | '.HumanDateLong($r->created_date).'</span>':'');
			$user_edited=($r->user_edited?'<br>Edited By : <span class="text-primary">'.$r->user_edited.' | '.HumanDateLong($r->edited_date).'</span>':'');
			$result[] = ($no);
			$result[] = '<span>'.($r->nama_template).'</span>'.$user_created.$user_edited;
			
			$result[] = $btn_info;
			$data[] = $result;
		}
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->datatable->count_all(true),
		"recordsFiltered" => $this->datatable->count_all(true),
		"data" => $data
		);
		echo json_encode($output);
	}
	function opsi_header_template($list_data,$id,$nama_class){
		$opsi='<select tabindex="8"  width="100%" class="js-select2 form-control '.$nama_class.' opsi_change_template" style="width: 100%;" data-placeholder="Pilih Opsi" required>';
		foreach($list_data as $row){
			$opsi .='<option value="'.$row->id.'" '.($row->id==$id?'selected':'').'>'.$row->nama.'</option>';
		}
		$opsi .='</select>';
		return $opsi;
	}
	function hapus_obat_template(){
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$hasil=$this->db->delete('tpoliklinik_e_resep_template_obat');
		$this->output->set_output(json_encode($hasil));
	}
	function close_template(){
		$template_id=$this->input->post('template_id');
		$this->db->where('template_id',$template_id);
		$hasil=$this->db->update('tpoliklinik_e_resep_template',array('status_template'=>2));
		$this->output->set_output(json_encode($hasil));
	}
	function list_my_order(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_eresep($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		$akses_general=$this->db->query("SELECT *FROM setting_eresep")->row_array();
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_permintaan) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_permintaan) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT  
					DATEDIFF(NOW(), H.tanggal_permintaan) as selisih,
					MP.tanggaldaftar,MP.tanggal,CASE WHEN MP.idtipe=1 THEN 'POLIKLLINIK' ELSE 'IGD' END as tipe_poli,P.nama as nama_poli
					,MD.nama as nama_dokter,mppa.nama as nama_pemberi_resep,MT.nama_tujuan,UC.nama as user_created
					,PR.ref as nama_prioritas,mruangan.nama as nama_ruangan,mkelas.nama as nama_kelas,mbed.nama as nama_bed
					,H.* 
					,CASE WHEN H.pendaftaran_id_ranap IS NULL OR H.pendaftaran_id_ranap='0' THEN 0 ELSE 1 END st_ranap 
					,CASE WHEN H.pendaftaran_id_ranap IS NULL OR H.pendaftaran_id_ranap='0' THEN MP.nopendaftaran ELSE RI.nopendaftaran  END nopendaftaran_new 
					,CASE WHEN H.pendaftaran_id_ranap IS NULL OR H.pendaftaran_id_ranap='0' THEN MP.tanggaldaftar ELSE RI.tanggaldaftar  END tanggaldaftar_new 
					FROM `tpoliklinik_e_resep` H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id 
					LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.pendaftaran_id_ranap 
					LEFT JOIN mruangan on mruangan.id=RI.idruangan
					LEFT JOIN mkelas on mkelas.id=RI.idkelas
					LEFT JOIN mbed on mbed.id=RI.idbed
					
					LEFT JOIN mpoliklinik P ON P.id=MP.idpoliklinik
					LEFT JOIN mdokter MD ON MD.id=MP.iddokter
					LEFT JOIN mppa ON mppa.id=H.iddokter_resep
					LEFT JOIN mtujuan_farmasi MT ON MT.id=H.tujuan_farmasi_id
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN merm_referensi PR ON PR.nilai=H.prioritas_cito AND PR.ref_head_id='103'
					WHERE H.status_assemen='2' AND H.created_ppa='$login_ppa_id' AND H.idpasien='$idpasien' ".$where."
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_assesmen('.$r->assesmen_id.','.$r->pendaftaran_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_eresep('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
		$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		if ($r->st_ranap=='1'){
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_resep/tindakan/'.$r->pendaftaran_id_ranap.'/erm_e_resep/input_eresep_ri/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}else{
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_resep/tindakan/'.$r->pendaftaran_id.'/erm_e_resep/input_eresep/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}
		// $btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
		$btn_ttd='';
		 if ($akses_general['st_edit_catatan']=='0'){
			  $btn_edit='';
		  }else{
			  if ($akses_general['lama_edit']>0){
				  if ($r->selisih>$akses_general['lama_edit']){
					  $btn_edit='';
				  }else{
					  
				  }
				  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_edit='';
					  }
				  }
			  }
		  }
		  if ($akses_general['st_duplikasi_catatan']=='0'){
			  $btn_duplikasi='';
		  }else{
			  if ($akses_general['lama_duplikasi']>0){
				  if ($r->selisih>$akses_general['lama_duplikasi']){
					  $btn_duplikasi='';
				  }else{
					 
				  }
			  }
			   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id!=$r->created_ppa){
						$btn_duplikasi='';
				  }
			   }
		  }
		  if ($akses_general['st_hapus_catatan']=='0'){
			  $btn_hapus='';
		  }else{
			  if ($akses_general['lama_hapus']>0){
				  if ($r->selisih>$akses_general['lama_hapus']){
					  $btn_hapus='';
				  }else{
					 
				  }
				   
			  }
			  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id <> $r->created_ppa){
						$btn_hapus='';
				  }else{
					  
				  }
						// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
			  }
		  }
		if ($logic_akses_assesmen['st_edit_eresep']=='0'){
		  $btn_edit='';
		}
		if ($logic_akses_assesmen['st_hapus_eresep']=='0'){
		  $btn_hapus='';
		}
		
		$result[] = $no;
		$aksi = '<div class="btn-group">';
		if ($r->status_tindakan <'2'){
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			
		}
		$aksi .= $btn_duplikasi;	
		if ($logic_akses_assesmen['st_lihat_eresep']=='1'){
		$aksi .= $btn_lihat;	
		}
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nopendaftaran_new.'<br>'.HumanDateLong($r->tanggaldaftar_new));
		$result[] = '<span>'.($r->noresep).'</span><br><span class="text-primary">'.HumanDateLong($r->tanggal_permintaan).'</span>';
		if ($r->st_ranap=='1'){
		$result[] = ($r->nama_ruangan).'<br>'.($r->nama_kelas).' - '.$r->nama_bed;
		}else{
		$result[] = '<span>'.($r->tipe_poli.' - '.$r->nama_poli).'</span><br><span class="text-success">'.($r->nama_dokter).'</span>';
		}
		$result[] = ($r->nama_pemberi_resep);
		$result[] = ($r->diagnosa);
		$result[] = ($r->nama_tujuan).'<br>'.($r->prioritas_cito=='1'?text_danger($r->nama_prioritas):text_info($r->nama_prioritas));
		$result[] = status_tindakan_farmasi($r->status_tindakan);
		$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date);;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function edit_eresep(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $assesmen_id=$this->input->post('assesmen_id');
		$data=array(
			// 'st_edited' => 1,
			'status_assemen' => 1,
			'edited_ppa' => $login_ppa_id,
			'edited_date' => date('Y-m-d H:i:s'),
		);
		
		$this->db->where('assesmen_id',$assesmen_id);
		$result=$this->db->update('tpoliklinik_e_resep',$data);
		
		$this->output->set_output(json_encode($result));
  }
  function edit_obat(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $id=$this->input->post('id');
	  $q="SELECT *FROM tpoliklinik_e_resep_obat WHERE id='$id'";
	  $result=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($result));
  }
  function edit_obat_racikan(){
	  $login_ppa_id=$this->session->userdata('login_ppa_id');
	  $id=$this->input->post('id');
	  $q="SELECT *FROM tpoliklinik_e_resep_obat_racikan_obat WHERE id='$id'";
	  $result=$this->db->query($q)->row_array();
	  $this->output->set_output(json_encode($result));
  }
  function list_history_order(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		$logic_akses_assesmen=$this->Tpendaftaran_poli_ttv_model->logic_akses_eresep($login_ppa_id,$login_spesialisasi_id,$login_profesi_id);
		$akses_general=$this->db->query("SELECT *FROM setting_eresep")->row_array();
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_permintaan) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_permintaan) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT  
					DATEDIFF(NOW(), H.tanggal_permintaan) as selisih,
					MP.tanggaldaftar,MP.tanggal,CASE WHEN MP.idtipe=1 THEN 'POLIKLLINIK' ELSE 'IGD' END as tipe_poli,P.nama as nama_poli
					,MD.nama as nama_dokter,mppa.nama as nama_pemberi_resep,MT.nama_tujuan,UC.nama as user_created
					,PR.ref as nama_prioritas,mruangan.nama as nama_ruangan,mkelas.nama as nama_kelas,mbed.nama as nama_bed
					,H.* 
					,CASE WHEN H.pendaftaran_id_ranap IS NULL OR H.pendaftaran_id_ranap='0' THEN 0 ELSE 1 END st_ranap 
					,CASE WHEN H.pendaftaran_id_ranap IS NULL OR H.pendaftaran_id_ranap='0' THEN MP.nopendaftaran ELSE RI.nopendaftaran  END nopendaftaran_new 
					,CASE WHEN H.pendaftaran_id_ranap IS NULL OR H.pendaftaran_id_ranap='0' THEN MP.tanggaldaftar ELSE RI.tanggaldaftar  END tanggaldaftar_new 
					FROM `tpoliklinik_e_resep` H
					LEFT JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id 
					LEFT JOIN trawatinap_pendaftaran RI ON RI.id=H.pendaftaran_id_ranap 
					LEFT JOIN mruangan on mruangan.id=RI.idruangan
					LEFT JOIN mkelas on mkelas.id=RI.idkelas
					LEFT JOIN mbed on mbed.id=RI.idbed
					
					LEFT JOIN mpoliklinik P ON P.id=MP.idpoliklinik
					LEFT JOIN mdokter MD ON MD.id=MP.iddokter
					LEFT JOIN mppa ON mppa.id=H.iddokter_resep
					LEFT JOIN mtujuan_farmasi MT ON MT.id=H.tujuan_farmasi_id
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN merm_referensi PR ON PR.nilai=H.prioritas_cito AND PR.ref_head_id='103'
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran_new','nama_dokter','nama_poli');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
	foreach ($list as $r) {	
		$no++;
		$btn_disabel_edit='';
		
		$result = array();
		$btn_disabel_edit='';
		if ($assesmen_id){
			$btn_disabel_edit='disabled';
		}
		$result = array();
		$btn_edit='<button class="btn btn-default btn-xs" '.$btn_disabel_edit.' type="button" title="Edit" onclick="edit_assesmen('.$r->assesmen_id.','.$r->pendaftaran_id.')" type="button"><i class="fa fa-pencil text-primary"></i></button>';
		$btn_duplikasi='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Duplikasi" onclick="copy_history_eresep('.$r->assesmen_id.')" type="button"><i class="fa fa-copy text-success"></i></button>';
		$btn_hapus='<button class="btn btn-default  btn-xs" '.$btn_disabel_edit.' type="button" title="Hapus" onclick="hapus_assesmen('.$r->assesmen_id.')" type="button"><i class="fa fa-trash-o	text-danger"></i></button>';
		if ($r->st_ranap=='1'){
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_resep/tindakan/'.$r->pendaftaran_id_ranap.'/erm_e_resep/input_eresep_ri/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}else{
		$btn_lihat='<a class="btn btn-default  btn-xs" href="'.site_url().'tpoliklinik_resep/tindakan/'.$r->pendaftaran_id.'/erm_e_resep/input_eresep/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			
		}
		// $btn_ttd='<br><button class="btn btn-success  btn-xs" type="button" title="Tandatangan" onclick="set_ttd_assesmen_manual('.$r->assesmen_id.')" type="button"><i class="fa fa-paint-brush"></i> '.($r->ttd_nama?$r->ttd_nama:'').'</button>';
		$btn_ttd='';
		 if ($akses_general['st_edit_catatan']=='0'){
			  $btn_edit='';
		  }else{
			  if ($akses_general['lama_edit']>0){
				  if ($r->selisih>$akses_general['lama_edit']){
					  $btn_edit='';
				  }else{
					  
				  }
				  if ($akses_general['orang_edit']=='0'){//Tidak DIizinkan Orang Lain
					  if ($login_ppa_id!=$r->created_ppa){
							$btn_edit='';
					  }
				  }
			  }
		  }
		  if ($akses_general['st_duplikasi_catatan']=='0'){
			  $btn_duplikasi='';
		  }else{
			  if ($akses_general['lama_duplikasi']>0){
				  if ($r->selisih>$akses_general['lama_duplikasi']){
					  $btn_duplikasi='';
				  }else{
					 
				  }
			  }
			   if ($akses_general['orang_duplikasi']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id!=$r->created_ppa){
						$btn_duplikasi='';
				  }
			   }
		  }
		  if ($akses_general['st_hapus_catatan']=='0'){
			  $btn_hapus='';
		  }else{
			  if ($akses_general['lama_hapus']>0){
				  if ($r->selisih>$akses_general['lama_hapus']){
					  $btn_hapus='';
				  }else{
					 
				  }
				   
			  }
			  if ($akses_general['orang_hapus']=='0'){//Tidak DIizinkan Orang Lain
				  if ($login_ppa_id <> $r->created_ppa){
						$btn_hapus='';
				  }else{
					  
				  }
						// $btn_hapus=$login_ppa_id .' VS '. $r->created_ppa;
			  }
		  }
		if ($logic_akses_assesmen['st_edit_eresep']=='0'){
		  $btn_edit='';
		}
		if ($logic_akses_assesmen['st_hapus_eresep']=='0'){
		  $btn_hapus='';
		}
		
		$result[] = $no;
		$aksi = '<div class="btn-group">';
		if ($r->status_tindakan<'2'){
			$aksi .= $btn_edit;	
			$aksi .= $btn_hapus;	
			
		}
		$aksi .= $btn_duplikasi;	
		if ($logic_akses_assesmen['st_lihat_eresep']=='1'){
		$aksi .= $btn_lihat;	
		}
		$aksi .= '</div>';
		$result[] = $aksi;
		$result[] = ($r->nopendaftaran_new.'<br>'.HumanDateLong($r->created_date));
		$result[] = '<span>'.($r->noresep).'</span><br><span class="text-primary">'.HumanDateLong($r->tanggal_permintaan).'</span>';
		if ($r->st_ranap=='1'){
		$result[] = ($r->nama_ruangan).'<br>'.($r->nama_kelas).' - '.$r->nama_bed;
		}else{
		$result[] = '<span>'.($r->tipe_poli.' - '.$r->nama_poli).'</span><br><span class="text-success">'.($r->nama_dokter).'</span>';
		}
		$result[] = ($r->nama_pemberi_resep);
		$result[] = ($r->diagnosa);
			
		$result[] = ($r->nama_tujuan).'<br>'.($r->prioritas_cito=='1'?text_danger($r->nama_prioritas):text_info($r->nama_prioritas));
			
		$result[] = status_tindakan_farmasi($r->status_tindakan);
		$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date);;
		$data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  // function list_index_obat_review_trx(){
		
		// $btn_disabel_edit='';
		// $max_iter=$this->input->post('max_iter');
		// $assesmen_id=$this->input->post('assesmen_id');
		// $idkelompokpasien=$this->input->post('idkelompokpasien');
		// $this->select = array();
		// $from="
			// (
				// SELECT H.id
				// ,B.nama as nama_barang,B.hargadasar
				// ,H.idtipe,H.idbarang,H.nama,H.dosis,H.jumlah,H.`interval`,MI.nama as nama_interval 
				// ,MR.ref as nama_rute,H.aturan_tambahan
				// ,CASE WHEN H.kuantitas IS NOT NULL THEN H.kuantitas ELSE H.jumlah END as kuantitas
				// ,H.waktu_minum
				// ,H.diskon,H.diskon_rp,H.harga_jual,H.harga_dasar,H.tuslah,H.expire_date,H.totalharga
				// ,(CASE 
				// WHEN $idkelompokpasien=5 THEN ((B.marginumum/100) + 1) * B.hargadasar 
				// WHEN $idkelompokpasien=4 THEN ((B.marginbpjstenagakerja/100) + 1) * B.hargadasar 
				// WHEN $idkelompokpasien=3 THEN ((B.marginbpjskesehatan/100) + 1) * B.hargadasar 
				// WHEN $idkelompokpasien=2 THEN ((B.marginjasaraharja/100) + 1) * B.hargadasar 
				// WHEN $idkelompokpasien=1 THEN ((B.marginasuransi/100) + 1) * B.hargadasar 
				// END) as harga
				// ,(CASE 
				// WHEN $idkelompokpasien=5 THEN (B.marginumum/100)
				// WHEN $idkelompokpasien=4 THEN B.marginbpjstenagakerja
				// WHEN $idkelompokpasien=3 THEN  B.marginbpjskesehatan/100
				// WHEN $idkelompokpasien=2 THEN B.marginjasaraharja/100 
				// WHEN $idkelompokpasien=1 THEN B.marginasuransi/100 
				// END) as margin,H.jenispenggunaan
				// FROM tpoliklinik_e_resep_obat H
				// INNER JOIN view_barang_farmasi_resep B ON B.id=H.idbarang AND B.idtipe=H.idtipe
				// INNER JOIN minterval MI ON MI.id=H.`interval`
				// LEFT JOIN merm_referensi MR ON MR.nilai=H.rute AND MR.ref_head_id='74'
				// WHERE H.assesmen_id='$assesmen_id'
				// ORDER BY H.id ASC

			// ) as tbl
		// ";
		// $this->from   = $from;
		// $this->join 	= array();$this->order  = array();$this->group  = array();
		// $this->column_search = array('nama');$this->column_order  = array();
		// $list = $this->datatable->get_datatables(true);
		// $data = array();$no = $_POST['start'];
		// $data_aturan_minum=list_variable_ref(110);
		// foreach ($list as $r) {	
			// $no++;$result = array();
			// $harga_jual=ceiling($r->harga,100);
			// $hargadasar=$r->harga;
			// $margin=$r->margin;
			// // $btn_info='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_info_obat('.$r->idtipe.','.$r->idbarang.')" type="button" data-toggle="tooltip" title="Informasi Obat" data-original-title="Informasi Obat"><i class="fa fa-info"></i></button>';
			
			
			// $aksi='';
			// // $aksi = '<div class="btn-group">';
			// // $aksi .= '<button  onclick="hapus_obat('.$r->id.')" type="button" title="Hapus" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			// // $aksi .= '</div>';
			// $result[] = ($r->nama_barang);
			// $result[] = ($r->dosis);
			// $result[] = number_format($r->dosis,0);
			// $result[] = ($r->nama_interval);
			// $result[] = ($r->nama_rute);
			// $result[] = ($r->aturan_tambahan);
			
			// $harga_dasar=$r->hargadasar;
			// $kuantitas=$r->kuantitas;
			// $tuslah=$r->tuslah;
			// $diskon=$r->diskon;
			// $diskon_rp=$diskon * $harga_jual / 100;
			// $totalharga=($harga_jual*$kuantitas) - ($diskon_rp*$kuantitas) + $tuslah;
			// $result[] = $this->opsi_aturan($data_aturan_minum,$r->jenispenggunaan,'jenispenggunaan');
			// $result[] = '<input type="text" class="kuantitas number text-center input_review" value="'.$kuantitas.'">';
			// $result[] = '<input type="text" class="diskon number text-center input_review" value="'.$diskon.'">';
			// $result[] = '<input type="text" class="tuslah number text-center input_review" value="'.$tuslah.'">';
			// $result[] = '<input type="text" class="expire_date  text-center input_review" value="'.$r->expire_date.'">';
			// $result[] = '<input type="text" class="totalharga number text-right input_review" disabled value="'.$totalharga.'">';
			
			// $input='<input type="hidden" class="cls_idtipe" value="'.$r->idtipe.'">';
			// $input .='<input type="hidden" class="cls_idobat" value="'.$r->idbarang.'">';
			// $input .='<input type="hidden" class="cls_idtrx" value="'.$r->id.'">';
			// $input .='<input type="hidden" class="harga_dasar number text-center" value="'.$harga_dasar.'">';
			// $input .='<input type="hidden" class="diskon_rp number text-center" value="'.$diskon_rp.'">';
			// $input .='<input type="hidden" class="harga_jual number text-center" value="'.$harga_jual.'">';
			// $input .='<input type="hidden" class="margin number text-center" value="'.$margin.'">';
			// $result[] = $aksi.$input;
			// if ($r->harga_jual==null){
				// $data=array(
					// 'jenispenggunaan' => 'SBM',
					// 'kuantitas' => $kuantitas,
					// 'harga_dasar' => $harga_dasar,
					// 'margin' => $margin,
					// 'harga_jual' => $harga_jual,
					// 'diskon' => $diskon,
					// 'diskon_rp' => $diskon_rp,
					// 'expire_date' => $expire_date,
					// 'tuslah' => $tuslah,
					// 'totalharga' => $totalharga,

				// );
				// $this->db->where('id',$r->id);
				// $this->db->update('tpoliklinik_e_resep_obat',$data);
			// }
			// $data[] = $result;
		// }
		// $output = array(
		// "draw" => $_POST['draw'],
		// "recordsTotal" => $this->datatable->count_all(true),
		// "recordsFiltered" => $this->datatable->count_all(true),
		// "data" => $data
		// );
		// echo json_encode($output);
	// }
	function list_index_obat_review_trx(){
		
		$btn_disabel_edit='';
		$max_iter=$this->input->post('max_iter');
		$assesmen_id=$this->input->post('assesmen_id');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		
		$this->update_stok($assesmen_id);
		$q="
			
				SELECT H.id
				,B.nama as nama_barang,B.hargadasar
				,H.idtipe,H.idbarang,H.nama,H.dosis,H.jumlah,H.`interval`,MI.nama as nama_interval 
				,MR.ref as nama_rute,H.aturan_tambahan
				,CASE WHEN H.kuantitas IS NOT NULL THEN H.kuantitas ELSE H.jumlah END as kuantitas
				,H.waktu_minum,BS.ref sediaan_nama
				,H.diskon,H.diskon_rp,H.harga_jual,H.harga_dasar,H.tuslah,H.expire_date,H.totalharga,H.cover,H.stok_asal
				,(CASE 
				WHEN $idkelompokpasien=5 THEN ((B.marginumum/100) + 1) * B.hargadasar 
				WHEN $idkelompokpasien=4 THEN ((B.marginbpjstenagakerja/100) + 1) * B.hargadasar 
				WHEN $idkelompokpasien=3 THEN ((B.marginbpjskesehatan/100) + 1) * B.hargadasar 
				WHEN $idkelompokpasien=2 THEN ((B.marginjasaraharja/100) + 1) * B.hargadasar 
				WHEN $idkelompokpasien=1 THEN ((B.marginasuransi/100) + 1) * B.hargadasar 
				END) as harga
				,(CASE 
				WHEN $idkelompokpasien=5 THEN (B.marginumum)
				WHEN $idkelompokpasien=4 THEN B.marginbpjstenagakerja
				WHEN $idkelompokpasien=3 THEN  B.marginbpjskesehatan
				WHEN $idkelompokpasien=2 THEN B.marginjasaraharja 
				WHEN $idkelompokpasien=1 THEN B.marginasuransi 
				END) as margin,H.jenispenggunaan,H.sisa_obat,H.kegunaan
				FROM tpoliklinik_e_resep_obat H
				LEFT JOIN view_barang_farmasi_resep B ON B.id=H.idbarang AND B.idtipe=H.idtipe
				LEFT JOIN minterval MI ON MI.id=H.`interval`
				LEFT JOIN merm_referensi BS ON BS.nilai=B.bentuk_sediaan AND BS.ref_head_id='94'
				LEFT JOIN merm_referensi MR ON MR.nilai=H.rute AND MR.ref_head_id='74'
				WHERE H.assesmen_id='$assesmen_id'
				ORDER BY H.id ASC
				
		";
		// print_r($q);exit;
		$list=$this->db->query($q)->result();
		
		$data_aturan_minum=list_variable_ref(110);
		$tabel='';
		$gt='0';
		$total='0';
		$jml_stok_kosong='0';
		$total_sisa_obat='0';
		foreach ($list as $r) {	
			
			$harga_jual=ceiling($r->harga,100);
			$stok_asal=$r->stok_asal;
			$hargadasar=$r->harga;
			$margin=$r->margin;
			$harga_dasar=$r->hargadasar;
			$kuantitas=$r->kuantitas;
			if ($kuantitas > $stok_asal){
				if ($stok_asal<1){
					$kuantitas=0;
					
				}else{
					
					$kuantitas=$stok_asal;
				}
			}
			if ($kuantitas < 0){
				$kuantitas=0;
			}
			$tuslah=$r->tuslah;
			$diskon=$r->diskon;
			if ($r->harga_jual==null){
				$diskon_rp=ceiling($diskon * $harga_jual / 100,100);
				$totalharga=($harga_jual*$kuantitas) - ($diskon_rp*$kuantitas) + $tuslah;
			}else{
				$diskon_rp=$r->diskon_rp;
				$totalharga=$r->totalharga;
			}

			// if ($r->harga_jual==null){
				$data=array(
					'jenispenggunaan' => 'SBM',
					'kuantitas' => $kuantitas,
					'harga_dasar' => $harga_dasar,
					'margin' => $margin,
					'harga_jual' => $harga_jual,
					'diskon' => $diskon,
					'diskon_rp' => $diskon_rp,
					'tuslah' => $tuslah,
					'totalharga' => $totalharga,

				);
				$this->db->where('id',$r->id);
				$this->db->update('tpoliklinik_e_resep_obat',$data);
			// }
			
			
			$btn_info='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_info_obat('.$r->idtipe.','.$r->idbarang.')" type="button" data-toggle="tooltip" title="Informasi Obat" data-original-title="Informasi Obat"><i class="fa fa-info"></i></button>';
			$tabel .='<tr>';
			
			$aksi='';
			
			// $aksi = '<div class="btn-group">';
			// $aksi .= '<button  onclick="hapus_obat('.$r->id.')" type="button" title="Hapus" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			// $aksi .= '</div>';
			if ($r->stok_asal < 1){
				$jml_stok_kosong=$jml_stok_kosong+1;
			}
			$tabel .='<td class="text-left"><span>'.($r->nama_barang).$btn_info.'</span><br><span class="text-primary">Sediaan : '.($r->sediaan_nama?$r->sediaan_nama:'-').' | Rp. '.number_format($harga_jual,0).' </span>'.status_cover($r->cover).' '.status_stok($r->stok_asal,$r->jumlah).'</td>';
			$tabel .='<td class="text-center">'.($r->dosis).'</td>';
			$tabel .='<td class="text-center">'.($r->jumlah).'</td>';
			$tabel .='<td class="text-center">'.($r->nama_interval).'</td>';
			$tabel .='<td class="text-center">'.($r->nama_rute).'</td>';
			$tabel .='<td class="text-center">'.($r->aturan_tambahan).'</td>';
			$input='<input type="hidden" class="idtipe" value="'.$r->idtipe.'">';
			$input .='<input type="hidden" class="idobat" value="'.$r->idbarang.'">';
			$input .='<input type="hidden" class="cls_idtrx" value="'.$r->id.'">';
			$input .='<input type="hidden" class="stok_asal  text-center" value="'.$stok_asal.'">';
			$input .='<input type="hidden" class="harga_dasar  text-center" value="'.$harga_dasar.'">';
			$input .='<input type="hidden" class="diskon_rp  text-center" value="'.$diskon_rp.'">';
			$input .='<input type="hidden" class="harga_jual  text-center" value="'.$harga_jual.'">';
			$input .='<input type="hidden" class="margin  text-center" value="'.$margin.'">';
			$input .='<input type="hidden" class="jumlah  text-center" value="'.$r->jumlah.'">';
			// $waktu_minum=implode_all($r->waktu_minum);
			$waktu_minum=$this->conver_waktu_minum($r->waktu_minum);
			
			// $waktu_minum=($waktu_minum[0]);
			$tabel .='<td class="text-center">
						<div id="" class="input-group">'.$this->opsi_aturan($data_aturan_minum,$r->jenispenggunaan,'jenispenggunaan').'
							<span class="input-group-btn">
								<button onclick="tambah_waktu('.$r->id.',1)" title="Waktu / Jam Minum Obat" class="btn btn-info" type="button" id="add_waktu"><i class="fa fa-calendar-plus-o"></i></button>
							</span>
						</div>
						'.$waktu_minum.'
							</td>';
			$tabel .='<td class="text-center"><input type="text" class="kegunaan input_review  text-center" value="'.$r->kegunaan.'"></td>';
			$tabel .='<td class="text-center "><input type="text" class="kuantitas input_review number text-right '.($r->kuantitas!= $r->jumlah?'text-red':'').'" value="'.$r->kuantitas.'"></td>';
			$tabel .='<td class="text-right"><input type="text" class="diskon input_review  text-right" value="'.$r->diskon.'"></td>';
			$tabel .='<td class="text-right"><input type="text" class="tuslah input_review  text-right" value="'.$r->tuslah.'"></td>';
			$tabel .='<td class="text-center"><input type="text" class="expire_date input_review  text-center " value="'.format_tanggal_short($r->expire_date).'"></td>';
			$tabel .='<td class="text-right"><input readonly type="text" class="totalharga number input_review  text-right" value="'.$totalharga.'"></td>';
			$tabel .='<td class="text-center">'.($aksi.$input).'<input type="hidden" class="sisa_obat  text-center" value="'.$r->sisa_obat.'"></td>';
			
			$tabel .='</tr>';
			$total_sisa_obat=$total_sisa_obat+$r->sisa_obat;
			$total=$total+$totalharga;
			$gt=$gt+$totalharga;
		}
		$tabel .='<tr>';
		
		$tabel .='<td colspan="11" class="text-right"></td>';
		$tabel .='<td  class="text-right"><strong>TOTAL</strong></td>';
		$tabel .='<td  class="text-right"><strong><input readonly type="text" id="totalharga_obat" class="totalharga_obat number input_review  text-right" value="'.$total.'"></strong></td>';
		$tabel .='<td ><input type="hidden" class="total_sisa_obat  text-center" id="total_sisa_obat" value="'.$total_sisa_obat.'"></td>';
		$tabel .='</tr>';
		$data['tabel']=$tabel;
		$data['total']=$total;
		$data['jml_stok_kosong']=$jml_stok_kosong;
		$data['gt']=$gt;
		
		$this->output->set_output(json_encode($data));
		
	}
	function conver_waktu_minum($str){
		$arr=json_decode($str);
		$waktu_minum='';
		if ($arr){
			
			foreach ($arr as list($a, $b,$c)) {
				if ($waktu_minum !=''){
					$waktu_minum=$waktu_minum.','.$b;
				}else{
					$waktu_minum=$b;
				}
			}
			if ($waktu_minum !=''){
				$waktu_minum='<br><strong>('.$waktu_minum.')</strong>';
			}
		}
		return $waktu_minum;
	}
	function list_index_obat_review_racikan_trx(){
		
		$btn_disabel_edit='';
		$max_iter=$this->input->post('max_iter');
		$assesmen_id=$this->input->post('assesmen_id');
		$idkelompokpasien=$this->input->post('idkelompokpasien');
		
		$q="UPDATE tpoliklinik_e_resep_obat_racikan_obat H INNER JOIN (
			SELECT T.*,T.kuantitas * T.harga_jual as totalharga FROM (
			SELECT D.id,D.idtipe,B.hargadasar,0 as diskon,0 as diskon_rp
			,CASE WHEN D.kuantitas IS NOT NULL THEN D.kuantitas ELSE D.jumlah END as kuantitas
			,(CASE 
				WHEN $idkelompokpasien=5 THEN round_100(((B.marginumum/100) + 1) * B.hargadasar) 
				WHEN $idkelompokpasien=4 THEN round_100(((B.marginbpjstenagakerja/100) + 1) * B.hargadasar)
				WHEN $idkelompokpasien=3 THEN round_100(((B.marginbpjskesehatan/100) + 1) * B.hargadasar) 
				WHEN $idkelompokpasien=2 THEN round_100(((B.marginjasaraharja/100) + 1) * B.hargadasar) 
				WHEN $idkelompokpasien=1 THEN round_100(((B.marginasuransi/100) + 1) * B.hargadasar) 
				END) as harga_jual
				,(CASE 
				WHEN $idkelompokpasien=5 THEN (B.marginumum)
				WHEN $idkelompokpasien=4 THEN B.marginbpjstenagakerja
				WHEN $idkelompokpasien=3 THEN  B.marginbpjskesehatan
				WHEN $idkelompokpasien=2 THEN B.marginjasaraharja 
				WHEN $idkelompokpasien=1 THEN B.marginasuransi 
				END) as margin
			FROM tpoliklinik_e_resep_obat_racikan_obat D
			INNER JOIN view_barang_farmasi_resep B ON B.id=D.idbarang AND B.idtipe=D.idtipe
			WHERE D.harga_jual IS NULL AND D.assesmen_id='$assesmen_id'
			) T
			) D ON D.id=H.id
			SET H.kuantitas=D.kuantitas,H.harga_dasar=D.hargadasar,
			H.harga_jual=D.harga_jual,H.margin=D.margin,H.diskon=D.diskon,H.diskon_rp=D.diskon_rp
		";
		// $this->db->query($q);
		$this->db->query($q);
		// print_r($q);exit;
		$q="SELECT 
			MI.nama as nama_interval,MR.ref as nama_rute,JR.ref as nama_jenis
			,COUNT(D.id) as jumlah_obat_racikan
			,CASE WHEN H.kuantitas IS NULL THEN H.jumlah_racikan ELSE H.kuantitas END as jumlah
			,H.* 
			FROM `tpoliklinik_e_resep_obat_racikan` H
			INNER JOIN tpoliklinik_e_resep_obat_racikan_obat D ON D.racikan_id=H.racikan_id
			LEFT JOIN minterval MI ON MI.id=H.interval_racikan
			LEFT JOIN merm_referensi MR ON MR.nilai=H.rute_racikan AND MR.ref_head_id='74'
			LEFT JOIN merm_referensi JR ON JR.nilai=H.jenis_racikan AND JR.ref_head_id='105'
			WHERE H.assesmen_id='$assesmen_id'
			GROUP BY H.racikan_id
				
		";
		$list=$this->db->query($q)->result();
		
		$data_aturan_minum=list_variable_ref(110);
		$tabel='';
		$gt='0';
		$total='0';
		$total_sisa_racikan='0';
		foreach ($list as $r) {	
			
			$tabel .='<tr>';
			
			$input='';
			$aksi='';
			// $aksi = '<div class="btn-group">';
			// $aksi .= '<button  onclick="hapus_obat('.$r->id.')" type="button" title="Hapus" '.$btn_disabel_edit.' class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';	
			// $aksi .= '</div>';
			$btn_nama_racikan='
				<div class="input-group">
					<input class="form-control nama_racikan input-sm" readonly type="text"  value="'.$r->nama_racikan.'">
					<span class="input-group-btn">
						<button class="btn btn-sm btn-success	" onclick="show_detail_racikan('.$r->racikan_id.')" type="button" data-toggle="tooltip" title="Informasi Racikan" data-original-title="Informasi Obat">	LIST OBAT ('.$r->jumlah_obat_racikan.')</button>
					</span>
				</div>
			';
			$tabel .='<td class="text-left">'.$btn_nama_racikan.'</td>';
			$tabel .='<td class="text-center">'.($r->nama_jenis).'</td>';
			$tabel .='<td class="text-center">'.($r->jumlah_racikan).'</td>';
			$tabel .='<td class="text-center">'.($r->nama_interval).'</td>';
			$tabel .='<td class="text-center">'.($r->nama_rute).'</td>';
			$tabel .='<td class="text-center">'.($r->aturan_tambahan_racikan).'</td>';
			
				$kuantitas=$r->jumlah;
			$harga=$r->harga;
			if ($r->totalharga==null){
				$tuslah=0;
				$totalharga=($harga*$kuantitas) + $tuslah;
			}else{
				$kuantitas=$r->kuantitas;
				$totalharga=$r->totalharga;
				$tuslah=$r->tuslah;
			}
			
			
			
			$input .='<input type="hidden" class="cls_idtrx" value="'.$r->racikan_id.'">';
			$input .='<input type="hidden" class="harga  text-center" value="'.$harga.'">';
			$input .='<input type="hidden" class="jumlah  text-center" value="'.$r->jumlah_racikan.'">';
			$waktu_minum=$this->conver_waktu_minum($r->waktu_minum);
			
			// $waktu_minum=($waktu_minum[0]);
			$tabel .='<td class="text-center">
						<div id="" class="input-group">'.$this->opsi_aturan($data_aturan_minum,$r->jenispenggunaan,'jenispenggunaan').'
							<span class="input-group-btn">
								<button onclick="tambah_waktu('.$r->racikan_id.',2)" title="Waktu / Jam Minum Obat" class="btn btn-info" type="button" id="add_waktu"><i class="fa fa-calendar-plus-o"></i></button>
							</span>
						</div>
						'.$waktu_minum.'
							</td>';
			// $tabel .='<td class="text-center">'.$this->opsi_aturan($data_aturan_minum,$r->jenispenggunaan,'jenispenggunaan').'</td>';
			$tabel .='<td class="text-center"><input type="text" class="kegunaan input_review  text-center" value="'.$r->kegunaan.'"></td>';
			$tabel .='<td class="text-center "><input type="text" class="kuantitas input_review reload_update number text-right '.($kuantitas!= $r->jumlah_racikan?'text-red':'').'" value="'.$kuantitas.'"></td>';
			$tabel .='<td class="text-right"><input type="text" class="tuslah input_review number text-right" value="'.$tuslah.'"></td>';
			$tabel .='<td class="text-right"><input type="text" class="expire_date input_review  text-right" value="'.format_tanggal_short($r->expire_date).'"></td>';
			// $tabel .='<td class="text-center"><input type="text" class="expire_date input_review  text-center " value="'.($r->expire_date?HumanDateShort($r->expire_date):'').'"></td>';
			$tabel .='<td class="text-right"><input readonly type="text" class="totalharga number input_review  text-right" value="'.$totalharga.'"></td>';
			$tabel .='<td class="text-center">'.($aksi.$input).'<input type="hidden" class="sisa_obat  text-center" value="'.$r->sisa_obat.'"></td>';
			if ($r->totalharga==null){
				$data=array(
					'jenispenggunaan' => 'SBM',
					'kuantitas' => $kuantitas,
					'expire_date' => null,
					'tuslah' => $tuslah,
					'totalharga' => $totalharga,

				);
				$this->db->where('racikan_id',$r->racikan_id);
				$this->db->update('tpoliklinik_e_resep_obat_racikan',$data);
			}
			$tabel .='</tr>';
			$total_sisa_racikan=$total_sisa_racikan+$r->sisa_obat;
			$total=$total+$totalharga;
			$gt=$gt+$totalharga;
		}
		$total_obat=0;
		$q="SELECT SUM(H.totalharga) as total_obat FROM tpoliklinik_e_resep_obat H WHERE H.assesmen_id='$assesmen_id'";
		$total_obat=$this->db->query($q)->row('total_obat');
		$gt_all=$total_obat + $total;
		$tabel .='<tr>';
		$tabel .='<td colspan="10" class="text-right"></td>';
		$tabel .='<td  class="text-right"><strong>TOTAL RACIKAN</strong></td>';
		$tabel .='<td  class="text-right"><strong><input readonly type="text" id="totalharga_obat_racikan" class="totalharga_obat_racikan number input_review  text-right" value="'.$total.'"></strong></td>';
		$tabel .='<td ></td>';
		$tabel .='</tr>';
		$tabel .='<tr>';
		$tabel .='<td colspan="10" class="text-right"></td>';
		$tabel .='<td  class="text-right"><strong>GRAND TOTAL</strong></td>';
		$tabel .='<td  class="text-right"><strong><input readonly type="text" id="totalharga_all" class="totalharga_all number input_review  text-right" value="'.$gt_all.'"></strong></td>';
		$tabel .='<td ><input type="hidden" class="total_sisa_racikan  text-center" id="total_sisa_racikan" value="'.$total_sisa_racikan.'"></td>';
		$tabel .='</tr>';
		
		$data['tabel']=$tabel;
		
		$this->output->set_output(json_encode($data));
		
	}
	function opsi_aturan($list_data,$id,$nama_class){
		$opsi='<select tabindex="8"  width="100%" class="js-select2 form-control '.$nama_class.' opsi_review" style="width: 100%;" data-placeholder="Pilih Opsi" required>';
		foreach($list_data as $row){
			$opsi .='<option value="'.$row->id.'" '.($id=='' && $row->st_default=='1'?'selected':'').' '.($row->id==$id?'selected':'').'>'.$row->nama.'</option>';
		}
		$opsi .='</select>';
		return $opsi;
	}
	function show_detail_racikan(){
		$btn_disabel_edit='';
		$max_iter=$this->input->post('max_iter');
		$assesmen_id=$this->input->post('assesmen_id');
		$racikan_id=$this->input->post('id');
		$q_head="SELECT * FROM `tpoliklinik_e_resep_obat_racikan` H WHERE H.assesmen_id='$assesmen_id' AND H.racikan_id='$racikan_id' AND H.status_racikan='2'";
		$list_head=$this->db->query($q_head)->result();
		$q_detail="SELECT H.*,UE.nama as nama_user_perubahan FROM `tpoliklinik_e_resep_obat_racikan_obat` H 
LEFT JOIN mppa UE ON UE.id=H.user_perubahan
WHERE H.assesmen_id='$assesmen_id' AND H.racikan_id='$racikan_id' ";
		$list_detail=$this->db->query($q_detail)->result_array();
		$data_interval=get_all('minterval');
		$data_rute=list_variable_ref(74);
		$data_jenis_racik=list_variable_ref(105);
		$no=0;
		$tabel='';
		foreach ($list_head as $r) {	
			$no ++;
			$btn_aksi='&nbsp;&nbsp;&nbsp;<div class="btn-group btn-group-xs" role="group">';
			$btn_aksi .='<button class="btn btn-default edit" type="button"><i class="fa fa-pencil"></i></button>';
			$btn_aksi .='<button class="btn btn-default hapus" type="button"><i class="fa fa-trash"></i></button>';
			$btn_aksi .='</div>';
			$tabel .='<table id="tabel_div_racikan" style="width:100%">';
				$tabel .='<tr>';
					$tabel .='<td>';
						
						$tabel .='<div class="form-group">';
							$tabel .='<div class="col-md-4 ">';
								$tabel .='<label>Nama Racik</label>';
								$tabel .='<input disabled type="text" class="form-control tabel_racikan nama_racikan"  value="'.$r->nama_racikan.'" >';
								$tabel .='<input disabled type="hidden" class="form-control tabel_racikan racikan_id"  value="'.$r->racikan_id.'" >';
							$tabel .='</div>';
							$tabel .='<div class="col-md-2 ">';
								$tabel .='<label>QTY Ambil</label>';
								$tabel .='<input disabled type="text" class="form-control tabel_racikan jumlah_racikan number"  value="'.$r->kuantitas.'" >';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Jenis Racik</label>';
								$tabel .='<select disabled class="js-select2 form-control tabel_racikan_opsi jenis_racikan" style="width: 100%;">'.$this->data_option($data_jenis_racik,$r->jenis_racikan).'</select>';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Frekuensi / Interval</label>';
								$tabel .='<select disabled class="js-select2 form-control tabel_racikan_opsi interval_racikan" style="width: 100%;">'.$this->data_option($data_interval,$r->interval_racikan).'</select>';
							$tabel .='</div>';
						$tabel .='</div>';
						$tabel .='<div class="form-group">';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Rute Pemberian</label>';
								$tabel .='<select disabled class="js-select2 form-control tabel_racikan_opsi rute_racikan" style="width: 100%;">'.$this->data_option($data_rute,$r->rute_racikan).'</select>';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Aturan Tambahan</label>';
								$tabel .='<input disabled type="text" class="form-control tabel_racikan aturan_tambahan_racikan"  value="'.$r->aturan_tambahan_racikan.'" >';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>ITER</label>';
								$tabel .='<select disabled class="js-select2 form-control tabel_racikan_opsi iter_racikan" style="width: 100%;">'.$this->data_option_iter($max_iter,$r->iter_racikan).'</select>';
							$tabel .='</div>';
							$tabel .='<div class="col-md-3 ">';
								$tabel .='<label>Tuslah</label>';
								$tabel .='<input disabled type="text" class="form-control tabel_racikan tuslah"  value="'.$r->tuslah.'" >';
							$tabel .='</div>';
						$tabel .='</div>';
						$tabel .='<div class="form-group" style="margin-top:-10px">';
							$tabel .='<div class="col-md-12 ">';
								$tabel .='<label class="h5 text-primary">DAFTAR OBAT RACIKAN '.$no.'</label>';
								$tabel .='<table class="table table-condensed table-bordered" style="background-color:#fff">';
									$tabel .='<thead>';
										$tabel .='<tr>';
											$tabel .='<th class="text-center" style="width: 5%;">NO</th>';
											$tabel .='<th class="text-center" style="width: 30%;">NAMA</th>';
											$tabel .='<th class="text-center" style="width: 10%;">QTY</th>';
											$tabel .='<th class="text-center" style="width: 7%;">P1</th>';
											$tabel .='<th class="text-center" style="width: 7%;">P2</th>';
											$tabel .='<th class="text-center" style="width: 10%;">DOSIS</th>';
											$tabel .='<th class="text-center" style="width: 10%;">T-QTY</th>';
											$tabel .='<th class="text-center" style="width: 10%;">HARGA SATUAN</th>';
											$tabel .='<th class="text-center" style="width: 10%;">JUMLAH HARGA</th>';
										$tabel .='</tr>';
									$tabel .='</thead>';
									$tabel .='<tbody>';
									$tabel .=$this->detail_obat_racikan_review($list_detail,$r->racikan_id,$r->kuantitas,$r->jumlah_racikan,$r->tuslah);
									$tabel .='</tbody>';
								$tabel .='</table>';
							$tabel .='</div>';
						$tabel .='</div>';
					$tabel .='</td>';
				$tabel .='</tr>';
			$tabel .='</table>';
		}
		
		$this->output->set_output(json_encode($tabel));
	}
	function detail_obat_racikan_review($list_data,$racikan_id,$jumlah_racikan,$jumlah_asal,$tuslah){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$tabel='';
		$data = array_filter($list_data, function($var) use ($racikan_id) { 
			return ($var['racikan_id'] == $racikan_id);
		  });	
		if ($data){
			$no=0;
			$total=0;
			
			foreach ($data as $r){
				$aksi='';
				
				$label_perubahan='';
				if ($r['st_setuju']=='0'){
					$label_setuju='<button onclick="lihat_perubahan_obat_racikan('.$r['id'].')" type="button" class="btn btn-default btn-xs "><i class="fa fa-user"></i> MENUNGGU</button>';
				}
				if ($r['st_setuju']=='1'){
					$label_setuju='<button onclick="lihat_perubahan_obat_racikan('.$r['id'].')" type="button" class="btn btn-primary btn-xs "><i class="fa fa-user"></i> SETUJU</button>';
				}
				if ($r['st_setuju']=='2'){
					$label_setuju='<button onclick="lihat_perubahan_obat_racikan('.$r['id'].')" type="button" class="btn btn-danger btn-xs "><i class="fa fa-times"></i> DITOLAK</button>';
				}
				if ($r['st_perubahan']!='0'){
					$aksi .= $label_setuju;	
					$aksi .= '<br><br><button onclick="lihat_perubahan_obat_racikan('.$r['id'].')" type="button" class="btn btn-'.($r['st_perubahan']=='1'?'warning':'success').' btn-xs ">'.($r['st_perubahan']=='1'?'PERUBAHAN':'PENAMBAHAN').'</button>';	
					$label_perubahan='<br><span class="text-danger"><strong>Perubahan : '.$r['nama_user_perubahan'].' | '.HumanDateLong($r['tanggal_perubahan']).'</strong></span>';
				}
				
				$btn_edit='';
				$btn_hapus='';
				if (UserAccesForm($user_acces_form,array('1843'))){
					$btn_edit='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_edit_obat_racikan_detail('.$r['id'].')" type="button" data-toggle="tooltip" title="Edit Obat"><i class="fa fa-pencil text-danger"></i></button>';
				}
				if (UserAccesForm($user_acces_form,array('1844'))){
					$btn_hapus='<button  onclick="hapus_racikan_detail_final('.$r['id'].')" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></button>';
				}
				$btn_info='&nbsp;&nbsp; <button class="btn btn-xs btn-default	" onclick="show_info_obat('.$r['idtipe'].','.$r['idbarang'].')" type="button" data-toggle="tooltip" title="Informasi Obat" data-original-title="Informasi Obat"><i class="fa fa-info"></i></button>';
		  // print_r($r['idtipe']);exit;
				$no++;
				$input='<input type="hidden" class="cls_idtipe" value="'.$r['idtipe'].'">';
				$input .='<input type="hidden" class="cls_idobat" value="'.$r['idbarang'].'">';
				$input .='<input type="hidden" class="cls_idtrx" value="'.$r['id'].'">';
				$input .='<input type="hidden" class="jumlah_racikan" value="'.$jumlah_racikan.'">';
				$tabel .='<tr>';
				$tabel .='<td>'.$no.$input.'</td>';
				$tabel .='<td>'.$r['nama'].$btn_info.status_cover($r['cover']).'</td>';
				$tabel .='<td><input disabled type="text" class="dosis_nrd decimal-2 jumlah text-center" value="'.$r['jumlah'].'"></td>';
				$tabel .='<td><input disabled type="text" class="dosis_nrd decimal-2 p1 text-center" value="'.$r['p1'].'"></td>';
				$tabel .='<td><input disabled type="text" class="dosis_nrd decimal-2 p2 text-center" value="'.$r['p2'].'"></td>';
				$tabel .='<td><input disabled type="text" class="dosis_nrd decimal-2 dosis text-center" value="'.$r['dosis'].'"></td>';
				$tqty_koma=number_format(($r['p1'] / $r['p2'] * $jumlah_racikan),1);
				// $tqty=ceiling(($r['p1'] / $r['p2'] * $jumlah_racikan),1);
				$tabel .='<td class="text-center"><input disabled type="hidden" class="dosis_nrd number total text-center" disabled value="'.($r['p1'] / $r['p2'] * $jumlah_racikan).'"><label class="label_tqty">'.$tqty_koma.' ('.$r['tqty'].')'.'</label></td>';
				$tabel .='<td><input disabled type="text" class="dosis_nrd number dosis text-right" value="'.$r['harga_jual'].'"></td>';
				$tabel .='<td><input disabled type="text" class="dosis_nrd number dosis text-right" value="'.$r['totalharga'].'"></td>';
				$tabel .='</tr>';
				$total=$total + $r['totalharga'];
			}
			$gt=0;
			$gt=$total*$jumlah_racikan;
			$gt_all=$total+$tuslah;
			$tabel .='<tr>';
			$tabel .='<td colspan="8" class="text-right"><strong>TOTAL PER RACIKAN</strong></td>';
			$tabel .='<td><input disabled type="text" class="dosis_nrd number gt_racikan text-right" value="'.$total.'"></td>';
			$tabel .='</tr>';
			$tabel .='<tr>';
			$tabel .='<td colspan="8" class="text-right"><strong>TUSLAH</strong></td>';
			$tabel .='<td><input disabled type="text" class="dosis_nrd number tuslah_racikan text-right" value="'.$tuslah.'"></td>';
			$tabel .='</tr>';
			$tabel .='<tr>';
			$tabel .='<td colspan="8" class="text-right"><strong>GRAND TOTAL</strong></td>';
			$tabel .='<td><input disabled type="text" class="dosis_nrd number gt_racikan_all text-right" value="'.($gt_all).'"></td>';
			$tabel .='</tr>';
		}
		
		return $tabel;

	}
	public function upload_resep() {
       $uploadDir = './assets/upload/resep';
		if (!empty($_FILES)) {
			 $assesmen_id = $this->input->post('assesmen_resep');
			 // print_r($assesmen_id);exit;
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $size=$_FILES['file']['size'];
			 $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
			 $file_name = time().'-'. $_FILES['file']['name'];
			 move_uploaded_file($tmpFile,$filename);
			 
			$detail 						= array();
			$tanggal_upload_resep =date('Y-m-d H:i:s');
			$q="UPDATE tpoliklinik_e_resep SET st_manual_resep='1',tanggal_upload_resep='$tanggal_upload_resep',resep_image='$file_name' WHERE assesmen_id='$assesmen_id'";
			$this->db->query($q);
			// $this->db->where('assesmen_id',1);
			// $this->db->upate('tpoliklinik_e_resep', $detail);
		}
    }
	function riwayat_1(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_permintaan) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_permintaan) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT  
					DATEDIFF(NOW(), H.tanggal_permintaan) as selisih,
					MP.tanggaldaftar,MP.tanggal,CASE WHEN MP.idtipe=1 THEN 'POLIKLLINIK' ELSE 'IGD' END as tipe_poli,P.nama as nama_poli
					,MD.nama as nama_dokter,mppa.nama as nama_pemberi_resep,MT.nama_tujuan,UC.nama as user_created
					,PR.ref as nama_prioritas
					,H.* FROM `tpoliklinik_e_resep` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					INNER JOIN mpoliklinik P ON P.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					INNER JOIN mppa ON mppa.id=H.iddokter_resep
					INNER JOIN mtujuan_farmasi MT ON MT.id=H.tujuan_farmasi_id
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN merm_referensi PR ON PR.nilai=H.prioritas_cito AND PR.ref_head_id='103'
					WHERE H.status_assemen='2' AND H.idpasien='$idpasien' ".$where."
					ORDER BY assesmen_id DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli','noresep');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;
			$btn_disabel_edit='';
			
			$result = array();
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tfarmasi_tindakan/tindakan/'.$r->pendaftaran_id.'/erm_e_resep/input_telaah/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			$btn_cetak='<button class="btn btn-success  btn-xs" href=""  type="button" title="Lihat Readonly" type="button"><i class="fa fa-print"></i></button>';
			
			$aksi = '<div class="btn-group">';
				$aksi .= $btn_lihat;	
				$aksi .= $btn_cetak;	
			$aksi .= '</div>';
			$result[] = $aksi;
			$result[] = ($r->nopendaftaran.'<br>'.HumanDateLong($r->tanggaldaftar));
			$result[] = '<span>'.($r->noresep).'</span><br><span class="text-primary">'.HumanDateLong($r->tanggal_permintaan).'</span>';
			$result[] = '<span>'.($r->tipe_poli.' - '.$r->nama_poli).'</span><br><span class="text-success">'.($r->nama_dokter).'</span>';
			$result[] = ($r->nama_pemberi_resep);
			$result[] = ($r->diagnosa);
			$result[] = ($r->nama_tujuan).'<br>'.($r->prioritas_cito=='1'?text_danger($r->nama_prioritas):text_info($r->nama_prioritas));
			$result[] = status_tindakan_farmasi($r->status_tindakan);
			$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date).'<br>'.$this->status_ambil($r->st_ambil);
			$data[] = $result;
		}
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function status_ambil($id){
	  $label='';
	  if ($id=='0'){
		  $label=text_default('MENUNGGU');
	  }
	  if ($id=='1'){
		  $label=text_success('DIAMBIL');
	  }
	  if ($id=='2'){
		  $label=text_danger('TIDAK DIAMBIL');
	  }
	  return $label;
  }
  function riwayat_2(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_permintaan) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_permintaan) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT  
					DATEDIFF(NOW(), H.tanggal_permintaan) as selisih,
					MP.tanggaldaftar,MP.tanggal,CASE WHEN MP.idtipe=1 THEN 'POLIKLLINIK' ELSE 'IGD' END as tipe_poli,P.nama as nama_poli
					,MD.nama as nama_dokter,mppa.nama as nama_pemberi_resep,MT.nama_tujuan,UC.nama as user_created
					,PR.ref as nama_prioritas
					,H.*
					,O.nama as nama_obat,O.dosis,O.jumlah,MI.nama as nama_interval,MR.ref as nama_rute,O.aturan_tambahan,O.kuantitas,O.iter
					FROM `tpoliklinik_e_resep` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					INNER JOIN mpoliklinik P ON P.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					INNER JOIN mppa ON mppa.id=H.iddokter_resep
					INNER JOIN mtujuan_farmasi MT ON MT.id=H.tujuan_farmasi_id
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN merm_referensi PR ON PR.nilai=H.prioritas_cito AND PR.ref_head_id='103'
					INNER JOIN (
						SELECT O.assesmen_id,O.nama,O.dosis,O.jumlah,O.`interval`,O.rute,O.aturan_tambahan,O.kuantitas,O.iter FROM tpoliklinik_e_resep_obat O
						UNION ALL
						SELECT O.assesmen_id,O.nama_racikan as nama,'RACIKAN' as dosis,O.jumlah_racikan jumlah,O.interval_racikan as `interval`
						,O.rute_racikan as rute,O.aturan_tambahan_racikan as aturan_tambahan,O.kuantitas,O.iter_racikan as iter FROM tpoliklinik_e_resep_obat_racikan O

					) O ON O.assesmen_id=H.assesmen_id
					
					LEFT JOIN merm_referensi MR ON MR.nilai=O.rute AND MR.ref_head_id='74'
					LEFT JOIN minterval MI ON MI.id=O.`interval` 
					WHERE H.status_assemen='2' AND H.idpasien='166' ".$where."
					ORDER BY H.assesmen_id DESC
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli','noresep','nama_obat');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;
			$btn_disabel_edit='';
			
			$result = array();
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tfarmasi_tindakan/tindakan/'.$r->pendaftaran_id.'/erm_e_resep/input_telaah/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			$btn_cetak='<button class="btn btn-success  btn-xs" href=""  type="button" title="Lihat Readonly" type="button"><i class="fa fa-print"></i></button>';
			
			$aksi = '<div class="btn-group">';
				$aksi .= $btn_lihat;	
				$aksi .= $btn_cetak;	
			$aksi .= '</div>';
			$result[] = $aksi;
			$result[] = ($r->nopendaftaran.'<br>'.HumanDateLong($r->tanggaldaftar));
			$result[] = '<span>'.($r->noresep).'</span><br><span class="text-primary">'.HumanDateLong($r->tanggal_permintaan).'</span>';
			$result[] = '<span>'.($r->tipe_poli.' - '.$r->nama_poli).'</span><br><span class="text-success">'.($r->nama_dokter).'</span>';
			$result[] = ($r->nama_pemberi_resep);
			$result[] = ($r->nama_obat);
			$result[] = ($r->jumlah);
			$result[] = ($r->nama_interval);
			$result[] = ($r->nama_rute);
			$result[] = ($r->aturan_tambahan);
			$result[] = ($r->iter);
			$result[] = ($r->kuantitas);
			$data[] = $result;
		}
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function riwayat_3(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_permintaan) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_permintaan) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT  
					DATEDIFF(NOW(), H.tanggal_permintaan) as selisih,
					MP.tanggaldaftar,MP.tanggal,CASE WHEN MP.idtipe=1 THEN 'POLIKLLINIK' ELSE 'IGD' END as tipe_poli,P.nama as nama_poli
					,MD.nama as nama_dokter,mppa.nama as nama_pemberi_resep,MT.nama_tujuan,UC.nama as user_created
					,PR.ref as nama_prioritas
					,H.*
					,O.nama as nama_obat,O.dosis,O.jumlah,MI.nama as nama_interval,MR.ref as nama_rute,O.aturan_tambahan,O.kuantitas
					,SUM(O.total_ambil) as total_ambil,SUM(O.total_ambil/O.kuantitas*O.jumlah) as total_resep,TPP.nopenjualan
					FROM `tpoliklinik_e_resep` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					INNER JOIN mpoliklinik P ON P.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					INNER JOIN mppa ON mppa.id=H.iddokter_resep
					INNER JOIN mtujuan_farmasi MT ON MT.id=H.tujuan_farmasi_id
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN merm_referensi PR ON PR.nilai=H.prioritas_cito AND PR.ref_head_id='103'
					INNER JOIN (
						SELECT O.assesmen_id,O.nama,O.dosis,O.jumlah,O.`interval`,O.rute,O.aturan_tambahan,O.kuantitas,O.iter,O.totalharga as total_ambil FROM tpoliklinik_e_resep_obat O
						UNION ALL
						SELECT O.assesmen_id,O.nama_racikan as nama,'RACIKAN' as dosis,O.jumlah_racikan jumlah,O.interval_racikan as `interval`
						,O.rute_racikan as rute,O.aturan_tambahan_racikan as aturan_tambahan,O.kuantitas,O.iter_racikan as iter,O.totalharga as total_ambil
						FROM tpoliklinik_e_resep_obat_racikan O

					) O ON O.assesmen_id=H.assesmen_id
					
					LEFT JOIN merm_referensi MR ON MR.nilai=O.rute AND MR.ref_head_id='74'
					LEFT JOIN minterval MI ON MI.id=O.`interval` 
					LEFT JOIN tpasien_penjualan TPP ON TPP.id=H.idpenjualan
					WHERE H.status_assemen='2' AND H.idpasien='166' ".$where."
					GROUP BY H.assesmen_id
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli','noresep','nama_obat');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;
			$btn_disabel_edit='';
			
			$result = array();
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tfarmasi_tindakan/tindakan/'.$r->pendaftaran_id.'/erm_e_resep/input_telaah/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			$btn_cetak='<button class="btn btn-success  btn-xs" href=""  type="button" title="Lihat Readonly" type="button"><i class="fa fa-print"></i></button>';
			
			$aksi = '<div class="btn-group">';
				$aksi .= $btn_lihat;	
				$aksi .= $btn_cetak;	
			$aksi .= '</div>';
			$result[] = $aksi;
			$result[] = ($r->nopendaftaran.'<br>'.HumanDateLong($r->tanggaldaftar));
			$result[] = '<span>'.($r->noresep).'</span><br><span class="text-primary">'.HumanDateLong($r->tanggal_permintaan).'</span>';
			$result[] = '<span>'.($r->tipe_poli.' - '.$r->nama_poli).'</span><br><span class="text-success">'.($r->nama_dokter).'</span>';
			$result[] = ($r->nama_pemberi_resep);
			$result[] = ($r->nama_tujuan).'<br>'.($r->prioritas_cito=='1'?text_danger($r->nama_prioritas):text_info($r->nama_prioritas));
			$result[] = ($r->user_created).'<br>'.HumanDateLong($r->created_date).'<br>'.$this->status_ambil($r->st_ambil);
			$result[] = ($r->nopenjualan);
			$result[] = number_format($r->total_resep,0);
			$result[] = number_format($r->total_ambil,0);
			$data[] = $result;
		}
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function riwayat_4(){
		$login_tipepegawai=$this->session->userdata('login_tipepegawai');
		$login_pegawai_id=$this->session->userdata('login_pegawai_id');
		$login_ppa_id=$this->session->userdata('login_ppa_id');
		$login_spesialisasi_id=$this->session->userdata('login_spesialisasi_id');
		$login_profesi_id=$this->session->userdata('login_profesi_id');
		$iddokter_dari='';
		if ($login_tipepegawai=='2'){
			$iddokter_dari=$login_pegawai_id;
			
		}
		
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$where='';
		$assesmen_id=$this->input->post('assesmen_id');
		$pendaftaran_id=$this->input->post('pendaftaran_id');
		$tanggal_input_2=$this->input->post('tanggal_input_2');
		$tanggal_input_1=$this->input->post('tanggal_input_1');
		$tgl_daftar_2=$this->input->post('tgl_daftar_2');
		$tgl_daftar_1=$this->input->post('tgl_daftar_1');
		$mppa_id=$this->input->post('mppa_id');
		$iddokter=$this->input->post('iddokter');
		$idpoli=$this->input->post('idpoli');
		$notransaksi=$this->input->post('notransaksi');
		$idpasien=$this->input->post('idpasien');
		$st_jawab=$this->input->post('st_jawab');
		if ($notransaksi!=''){
			$where .=" AND MP.nopendaftaran LIKE '%".$notransaksi."%'";
		}
		
		if ($iddokter!='#'){
			$where .=" AND MP.iddokter = '".$iddokter."'";
		}
		if ($idpoli!='#'){
			$where .=" AND MP.idpoliklinik = '".$idpoli."'";
		}
		
		if ($tanggal_input_1 !=''){
			$where .=" AND DATE(H.tanggal_permintaan) >='".YMDFormat($tanggal_input_1)."' AND DATE(H.tanggal_permintaan) <='".YMDFormat($tanggal_input_2)."'";
		}
		if ($tgl_daftar_1 !=''){
			$where .=" AND DATE(MP.tanggaldaftar) >='".YMDFormat($tgl_daftar_1)."' AND DATE(MP.tanggaldaftar) <='".YMDFormat($tgl_daftar_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT  
					DATEDIFF(NOW(), H.tanggal_permintaan) as selisih,
					MP.tanggaldaftar,MP.tanggal,CASE WHEN MP.idtipe=1 THEN 'POLIKLLINIK' ELSE 'IGD' END as tipe_poli,P.nama as nama_poli
					,MD.nama as nama_dokter,mppa.nama as nama_pemberi_resep,MT.nama_tujuan,UC.nama as user_created
					,PR.ref as nama_prioritas
					,H.*
					,O.nama as nama_obat,O.dosis,O.jumlah,MI.nama as nama_interval,MR.ref as nama_rute,O.aturan_tambahan,O.kuantitas
					,(O.total_ambil) as total_ambil,(O.total_ambil/O.kuantitas*O.jumlah) as total_resep,TPP.nopenjualan
					FROM `tpoliklinik_e_resep` H
					INNER JOIN tpoliklinik_pendaftaran MP ON MP.id=H.pendaftaran_id
					INNER JOIN mpoliklinik P ON P.id=MP.idpoliklinik
					INNER JOIN mdokter MD ON MD.id=MP.iddokter
					INNER JOIN mppa ON mppa.id=H.iddokter_resep
					INNER JOIN mtujuan_farmasi MT ON MT.id=H.tujuan_farmasi_id
					LEFT JOIN mppa UC ON UC.id=H.created_ppa
					LEFT JOIN merm_referensi PR ON PR.nilai=H.prioritas_cito AND PR.ref_head_id='103'
					INNER JOIN (
						SELECT O.assesmen_id,O.nama,O.dosis,O.jumlah,O.`interval`,O.rute,O.aturan_tambahan,O.kuantitas,O.iter,O.totalharga as total_ambil FROM tpoliklinik_e_resep_obat O
						UNION ALL
						SELECT O.assesmen_id,O.nama_racikan as nama,'RACIKAN' as dosis,O.jumlah_racikan jumlah,O.interval_racikan as `interval`
						,O.rute_racikan as rute,O.aturan_tambahan_racikan as aturan_tambahan,O.kuantitas,O.iter_racikan as iter,O.totalharga as total_ambil
						FROM tpoliklinik_e_resep_obat_racikan O

					) O ON O.assesmen_id=H.assesmen_id
					
					LEFT JOIN merm_referensi MR ON MR.nilai=O.rute AND MR.ref_head_id='74'
					LEFT JOIN minterval MI ON MI.id=O.`interval` 
					LEFT JOIN tpasien_penjualan TPP ON TPP.id=H.idpenjualan
					WHERE H.status_assemen='2' AND H.idpasien='166' ".$where."
					ORDER BY H.assesmen_id
				) as tbl
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nopendaftaran','nama_dokter','nama_poli','noresep','nama_obat');
		$this->column_order  = array();

		$list = $this->datatable->get_datatables(true);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {	
			$no++;
			$btn_disabel_edit='';
			
			$result = array();
			$btn_disabel_edit='';
			if ($assesmen_id){
				$btn_disabel_edit='disabled';
			}
			$result = array();
			$btn_lihat='<a class="btn btn-default  btn-xs" target="_blank" href="'.site_url().'tfarmasi_tindakan/tindakan/'.$r->pendaftaran_id.'/erm_e_resep/input_telaah/'.$r->assesmen_id.'"  type="button" title="Lihat Readonly" type="button"><i class="fa fa-eye"></i></a>';
			$btn_cetak='<button class="btn btn-success  btn-xs" href=""  type="button" title="Lihat Readonly" type="button"><i class="fa fa-print"></i></button>';
			
			$aksi = '<div class="btn-group">';
				$aksi .= $btn_lihat;	
				$aksi .= $btn_cetak;	
			$aksi .= '</div>';
			$result[] = $aksi;
			$result[] = ($r->nopendaftaran.'<br>'.HumanDateLong($r->tanggaldaftar));
			$result[] = '<span>'.($r->noresep).'</span><br><span class="text-primary">'.HumanDateLong($r->tanggal_permintaan).'</span>';
			$result[] = '<span>'.($r->tipe_poli.' - '.$r->nama_poli).'</span><br><span class="text-success">'.($r->nama_dokter).'</span>';
			$result[] = ($r->nama_pemberi_resep);
			$result[] = ($r->nama_obat);
			$result[] = ($r->dosis);
			$result[] = ($r->jumlah);
			$result[] = ($r->kuantitas);
			$result[] = number_format($r->total_resep,0);
			$result[] = number_format($r->total_ambil,0);
			$data[] = $result;
		}
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
}	
