<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use Hashids\Hashids;
class Tbooking extends CI_Controller {

	/**
	 * Booking controller.
	 * Developer @Acep Kursina
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tbooking_rehab_model');
		$this->load->model('Tbooking_model');
		$this->load->model('Tpoliklinik_pendaftaran_model');
		$this->load->helper('path');
		
  }


	function poliklinik($tab='0'){
		$q="UPDATE app_reservasi_tanggal_pasien SET st_validasi=1,status_reservasi='2' WHERE st_validasi=0 AND st_auto_validasi=1";
		$this->db->query($q);
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if ($tab=='0'){			
			if (UserAccesForm($user_acces_form,array('1497'))){
				$tab='1';
			}elseif (UserAccesForm($user_acces_form,array('1504'))){
				$tab='2';
			}elseif (UserAccesForm($user_acces_form,array('1506'))){
				$tab='3';
			}
		}
		if (UserAccesForm($user_acces_form,array('1497','1504','1506'))){
			$data = array();
			$date2=date_create(date('Y-m-d'));
			date_add($date2,date_interval_create_from_date_string("7 days"));
			$date2= date_format($date2,"d/m/Y");
			
			$data['list_reservasi_cara'] 			= list_reservasi_cara();
			// print_r($data['list_reservasi_cara']);exit;
			
			$data['list_poli'] 			= $this->Tbooking_model->list_poli();
			$data['list_dokter'] 			= $this->Tbooking_model->list_dokter();
			$data['tanggal_1'] 			= date('d/m/Y');
			$data['tanggal_2'] 			= $date2;
			$data['waktu_reservasi_1'] 			= '';
			$data['waktu_reservasi_2'] 			= '';
			$data['namapasien'] 			= '';
			$data['no_medrec'] 			= '';
			$data['present'] 			= '2';
			$data['tab'] 			= $tab;
			$tahun=date('Y');
			$bulan=date('m');
			// $q="SELECT D.tanggal,DATE_ADD(D.tanggal,INTERVAL 0 DAY) as tanggal_next   FROM date_row D

// WHERE D.tanggal BETWEEN DATE_ADD(CURRENT_DATE(),INTERVAL -2 DAY) AND CURRENT_DATE() AND D.hari='2'";
			// $tanggal=$this->db->query($q)->row();
			$data['tanggaldari'] 			= DMYFormat(date('Y-m-d'));
			$data['tanggalsampai'] 			= DMYFormat($date2);
			$data['error'] 			= '';
			$data['title'] 			= 'Booking Poliklinik';
			$data['content'] 		= 'Tbooking/poliklinik';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Booking System",'#'),
												  array("Poliklinik Reservasi Dokter",'tbooking/poliklinik')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	//Poliklinik
	function add_reservasi($idpoli='',$iddokter='',$jadwal_id='',$tanggal=''){
		$hashids = new Hashids('RSKBHALMAHERA', 0, 'ABCEFGHJKLMNPRSTUVWXYZ123456789');
		$notransaksi=($this->Tbooking_rehab_model->get_notransaksi());
		$nobooking=substr($notransaksi,-8);
		$kode_booking =  $hashids->encode($nobooking);
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		if (UserAccesForm($user_acces_form,array('1498'))){
			$data = array();
			$data['st_gc'] 			= 0;
			$data['st_sc'] 			= 0;
			$data['st_sp'] 			= 0;
			$data['tab'] 			= 2;
			$data['notransaksi']=$notransaksi;
			$data['nobooking']=$nobooking;
			$data['kode_booking']=$kode_booking;
			$data['tgl_hari'] 			= date('d-m-Y');
			$data['tgl_bulan'] 			= date('d-m-Y');
			$data['tgl_tahun'] 			= date('d-m-Y');
			$data['tanggaldaftar'] 			= date('d-m-Y');
			$data['waktudaftar'] 			= date('H:i:s');
			$data['hari']='';
			$data['reservasi_tipe_id']='1';
			$data['reservasi_cara']='';
			$data['statuspasienbaru']='';
			$data['waktu_reservasi']='';
			$data['idpoli']=$idpoli;
			$data['iddokter']=$iddokter;
			$data['jadwal_id']=$jadwal_id;
			$data['tanggal']=$tanggal;
			$data['kodehari']='';
			$data['jam_id']='';
			$data['catatan']='';
			$data['idruangan']='';
			$data['idpasien']='';
			$data['title']='';
			$data['namapasien']='';
			$data['no_medrec']='';
			$data['nohp']='';
			$data['telepon']='';
			$data['tempat_lahir']='';
			$data['tanggal_lahir']='';
			$data['alamatpasien']='';
			$data['provinsi_id']='';
			$data['kabupaten_id']='';
			$data['kecamatan_id']='';
			$data['kelurahan_id']='';
			$data['kodepos']='';
			$data['idasalpasien']='';
			$data['idrujukan']='';
			$data['idtipepasien']='';
			$data['idkelompokpasien']='';
			$data['idrekanan']='';
			$data['idtarifbpjskesehatan']='';
			$data['idtarifbpjstenagakerja']='';
			$data['idkelompokpasien2']='';
			$data['idrekanan2']='';
			$data['idtarifbpjskesehatan2']='';
			$data['idtarifbpjstenagakerja2']='';
			$data['idpoliklinik']='';
			$data['idjenispertemuan']='';
			$data['status_reservasi']='';
			$data['namapenanggungjawab']='';
			$data['teleponpenanggungjawab']='';
			$data['noidentitaspenanggungjawab']='';
			$data['umurhari']='';
			$data['umurbulan']='';
			$data['umurtahun']='';
			$data['rencana']='';
			$data['iduserinput']='';
			$data['statustindakan']='';
			$data['statusrencana']='';
			$data['hubungan']='';
			$data['idalasan']='';
			$data['email']='';
			$data['suku']='';
			$data['jenis_id']='1';
			$data['jenis_kelamin']='';
			$data['ktp']='';
			$data['hp']='';
			$data['alamat_keluarga']='';
			$data['hubungan']='';
			$data['golongan_darah']='';
			$data['agama_id']='';
			$data['warganegara']='';
			$data['suku_id']='';
			$data['statuskawin']='';
			$data['pendidikan']='';
			$data['pekerjaan']='';
			$data['golongan_darah']='';
			$data['pertemuan_id']='';
			$data['idasalpasien']='';
			$data['idtipepasien']='';
			$data['kelompokpasien']='';
			$data['idrekanan']='';
			$data['foto_kartu']='';
			$data['reservasi_cara']='';
			$data['suku_id']='2';

			$data['error']='';
			$data['id']='';
			
			$data['list_poli'] 			= $this->Tbooking_model->list_poli();
			$data['list_asuransi'] 			= $this->Tbooking_model->list_asuransi();
			$data['list_cara_bayar'] 			= $this->Tbooking_model->list_cara_bayar();
			if ($idpoli!=''){
				$data['list_dokter'] 			= $this->Tbooking_model->list_dokter();
				$data['list_tanggal'] 			= $this->Tbooking_model->list_tanggal($idpoli,$iddokter);
				$data['list_jadwal'] 			= $this->Tbooking_model->list_jadwal($idpoli,$iddokter,$tanggal);
			}else{
				$data['list_dokter']=array();
				$data['list_tanggal']=array();
				$data['list_jadwal']=array();
			}
			$data['content'] 		= 'Tbooking/add_reservasi';
			$data['breadcrum'] 	= array(
												  array("RSKB Halmahera",'#'),
												  array("Booking",'#'),
												  array("Poliklinik Reservasi",'tbooking/poliklinik')
												);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		}else{
			redirect('page404');
		}
	}
	public function get_nomedrec()
	{
		
		$hasil=$this->Tpoliklinik_pendaftaran_model->get_nomedrec();
		$arr['no_medrec']=$hasil;
		$this->output->set_output(json_encode($arr));
	}
	function edit_poli($id){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$data=$this->Tbooking_model->load_edit_poli($id);
		// print_r($data);exit;
		$tanggallahir=DMYFormat2($data['tanggal_lahir']);
		$data['tgl_hari']=substr($tanggallahir,0,2);
		$data['tgl_bulan']=substr($tanggallahir,3,2);
		// print_r($tanggallahir.'-'.$data['tgl_bulan']);exit;
		$data['tgl_tahun']=substr($tanggallahir,6,4);
		$data['tanggaldaftar']=DMYFormat($data['waktu_reservasi']);
		$data['waktudaftar']=HISTimeFormat($data['waktu_reservasi']);
		$data['ktp']=($data['noidentitas']);
		$data['kelompokpasien']=($data['idkelompokpasien']);
		// $data['ktp']=($data['noidentitas']);
		$data['list_poli'] 			= $this->Tbooking_model->list_poli();
		$data['list_cara_bayar'] 			= $this->Tbooking_model->list_cara_bayar();
		$data['list_asuransi'] 			= $this->Tbooking_model->list_asuransi();
		$data['list_dokter'] 			= $this->Tbooking_model->list_dokter();
		$data['content'] 		= 'Tbooking/edit_reservasi';
		$data['breadcrum'] 	= array(
							  array("RSKB Halmahera",'#'),
							  array("Booking",'#'),
							  array("Poliklinik Reservasi",'tbooking/poliklinik')
							);
								
		$data['tab']='3';
		// if ($data['st_sp']=='1'){			
			// $data['tab']='5';
		// }
		// if ($data['st_sc']=='1'){			
			// $data['tab']='6';
		// }
		
		
		if ($data['st_sc']=='1'){	
			
			$data_sc=$this->Tbooking_model->get_sc($id);

			$data = array_merge($data, $data_sc);
		}
		if ($data['st_sp']=='1'){	
			
			$data_sp=$this->Tbooking_model->get_sp($id);

			$data = array_merge($data, $data_sp);
		}
		if ($data['st_gc']){	
			
			$data_gc=$this->Tbooking_model->get_gc($id);

			$data = array_merge($data, $data_gc);
		}
		
		if ($data['st_sc']=='1' && $data['st_covid']=='0' && $data['st_skrining']=='1'){	
			
			$data['tab']='6';
		}
		if ($data['st_sp']=='1' && $data['st_general']=='1' && $data['st_skrining']=='0'){	
			
			$data['tab']='5';
		}
		if ($data['st_gc']=='1' && $data['st_general']=='0'){	
			
			$data['tab']='4';
		}
		// print_r($data['tab']);exit;
		$data['error']='';
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		
	}
	function load_index_gc($id){
	  $q_nilai="SELECT *FROM merm_referensi WHERE ref_head_id='16' ORDER BY merm_referensi.id ASC";
	  $list_nilai=$this->db->query($q_nilai)->result();
	  $q="SELECT  H.*
		,CASE 
			WHEN H.jenis_isi=1 THEN H.jawaban_id
			WHEN H.jenis_isi=2 THEN H.jawaban_freetext
			WHEN H.jenis_isi=3 THEN H.jawaban_ttd
		 END as jawaban
		
	  
	  FROM app_reservasi_tanggal_pasien_gc H WHERE H.app_id='$id'";
	  $list_data=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_data as $row){
		  
		  $tabel .='<tr>';
		  $tabel .='<td width="3%" ><input type="hidden" class="approval_id" value="'.$row->id.'">'.$row->no.'</td>';
		  $tabel .='<td width="60%" >'.($row->pertanyaan).'</td>';
	  
		  $tabel .='<td width="37%" >'.$this->opsi_nilai($list_nilai,$row->group_jawaban_id,$row->jawaban,$row->jenis_isi,$row->id).'</td>';
			  
		  
		  $tabel .='</tr>';
	  }
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function opsi_nilai($list_nilai,$group_nilai,$jawaban_id,$jenis_isi,$id){
		if ($jenis_isi=='1'){
			  $arr_group_nilai=explode(",",$group_nilai);
			  $opsi='
											   
											  
			  <select class="js-select2 full-width nilai_gc has-error2" style="width:100%;" data-placeholder="Pilih Opsi">';
			  $opsi .='<option value="" '.($jawaban_id==0?'selected':'').'>Opsi Jawaban</option>'; 
			  foreach($list_nilai as $row){
				  if (in_array($row->id,$arr_group_nilai)){
					 $opsi .='<option value="'.$row->id.'" '.($jawaban_id==$row->id?'selected':'').'>'.$row->ref.'</option>'; 
				  }
			  }
			  $opsi .="</select>";
			  return $opsi;
		}
		if ($jenis_isi=='2'){
			  $jawaban_free_text='';
			  $jawaban_free_text='
				<textarea class="form-control nilai_gc_free" placeholder="Isi" required>'.$jawaban_id.'</textarea>
			  ';
			  return $jawaban_free_text;
		}
		if ($jenis_isi=='3'){
			$btn_paraf='';
			$nama_tabel="'app_reservasi_tanggal_pasien_gc'";
			  if ($jawaban_id==''){
				  $btn_paraf= '<button type="button" onclick="modal_faraf('.$id.','.$nama_tabel.')"  data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i></button>';
			  }else{
				  $btn_paraf="<img style='display:block; width:100px;height:100px;' id='base64image' src='".$jawaban_id."' />";
				  $btn_paraf .= '<br><div class="btn-group btn-group-sm " role="group"><button  type="button" onclick="hapus_paraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Hapus Tanda tangan" class="btn btn-danger btn-xs btn_ttd"><i class="fa fa-trash"></i></button>';
				  $btn_paraf .= '<button onclick="modal_faraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd"><i class="fa fa-paint-brush"></i></button>';
				  $btn_paraf .= '</div>';
			  }
			  return $btn_paraf;
		}
	}
	
	function update_nilai_gc(){
	  $approval_id=$this->input->post('approval_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_id' =>$jawaban_id
	  );
	  $this->db->where('id',$approval_id);
	  $result=$this->db->update('app_reservasi_tanggal_pasien_gc',$data);
	  $this->output->set_output(json_encode($result));
	}
	function update_persetujuan_gc(){
	  $app_id=$this->input->post('app_id');
	  $jawaban_perssetujuan=$this->input->post('jawaban_perssetujuan');
	  $data=array(
		'jawaban_perssetujuan' =>$jawaban_perssetujuan
	  );
	  $this->db->where('app_id',$app_id);
	  $result=$this->db->update('app_reservasi_tanggal_pasien_gc_head',$data);
	  $this->output->set_output(json_encode($result));
	}
	function update_nilai_gc_free(){
	  $approval_id=$this->input->post('approval_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_freetext' =>$jawaban_id
	  );
	  $this->db->where('id',$approval_id);
	  $result=$this->db->update('app_reservasi_tanggal_pasien_gc',$data);
	  $this->output->set_output(json_encode($result));
	}
	function load_index_sp($id){
	  $q_nilai="SELECT *FROM merm_referensi WHERE ref_head_id='17' ORDER BY merm_referensi.id ASC";
	  $list_nilai=$this->db->query($q_nilai)->result();
	  $q="SELECT  H.*
			,CASE 
						WHEN H.jenis_isi=1 THEN H.jawaban_id
						WHEN H.jenis_isi=2 THEN H.jawaban_freetext
						WHEN H.jenis_isi=3 THEN H.jawaban_ttd
					 END as jawaban
		
	  FROM app_reservasi_tanggal_pasien_sp H
	  
	  WHERE H.app_id='$id'";
	  $list_data=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_data as $row){
		  
		  $tabel .='<tr>';
		  $tabel .='<td width="50%" ><input type="hidden" class="sp_id" value="'.$row->id.'">'.($row->pertanyaan).'</td>';
	  
		// $tabel .='<td width="50%" >'.$this->opsi_nilai_sp($list_nilai,$row->group_jawaban_id,$row->jawaban_id).'</td>';
			  $tabel .='<td width="37%" >'.$this->opsi_nilai_sp($list_nilai,$row->group_jawaban_id,$row->jawaban,$row->jenis_isi,$row->id).'</td>';  
		  
		  $tabel .='</tr>';
	  }
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	function opsi_nilai_sp($list_nilai,$group_nilai,$jawaban_id,$jenis_isi,$id){
		if ($jenis_isi=='1'){
			  $arr_group_nilai=explode(",",$group_nilai);
			  $opsi='
											   
											  
			  <select class="js-select2 full-width nilai_sp has-error2" style="width:100%;" data-placeholder="Pilih Opsi">';
			  $opsi .='<option value="" '.($jawaban_id==0?'selected':'').'>Opsi Jawaban</option>'; 
			  foreach($list_nilai as $row){
				  if (in_array($row->id,$arr_group_nilai)){
					 $opsi .='<option value="'.$row->id.'" '.($jawaban_id==$row->id?'selected':'').'>'.$row->ref.'</option>'; 
				  }
			  }
			  $opsi .="</select>";
			  return $opsi;
		}
		if ($jenis_isi=='2'){
			  $jawaban_free_text='';
			  $jawaban_free_text='
				<textarea class="form-control nilai_sp_free" placeholder="Isi" required>'.$jawaban_id.'</textarea>
			  ';
			  return $jawaban_free_text;
		}
		if ($jenis_isi=='3'){
			$btn_paraf='';
			$nama_tabel="'app_reservasi_tanggal_pasien_sp'";
			  if ($jawaban_id==''){
				  $btn_paraf= '<button onclick="modal_faraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd_sp"><i class="fa fa-paint-brush"></i></button>';
			  }else{
				  $btn_paraf="<img style='display:block; width:100px;height:100px;' id='base64image' src='".$jawaban_id."' />";
				  $btn_paraf .= '<br><div class="btn-group btn-group-sm" role="group"><button onclick="hapus_paraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Hapus Tanda tangan" class="btn btn-danger btn-xs btn_ttd_sp"><i class="fa fa-trash"></i></button>';
				  $btn_paraf .= '<button onclick="modal_faraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd_sp"><i class="fa fa-paint-brush"></i></button>';
				  $btn_paraf .= '</div>';
			  }
			  return $btn_paraf;
		}
	}
	function update_nilai_sp_free(){
	  $approval_id=$this->input->post('approval_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_freetext' =>$jawaban_id
	  );
	  // print_r($approval_id);exit;
	  $this->db->where('id',$approval_id);
	  $result=$this->db->update('app_reservasi_tanggal_pasien_sp',$data);
	  $this->output->set_output(json_encode($result));
	}
	function update_nilai_sc_free(){
	  $approval_id=$this->input->post('approval_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_freetext' =>$jawaban_id
	  );
	  // print_r($approval_id);exit;
	  $this->db->where('id',$approval_id);
	  $result=$this->db->update('app_reservasi_tanggal_pasien_sc',$data);
	  $this->output->set_output(json_encode($result));
	}
	
	function load_index_sc($id){
	  $q_nilai="SELECT *FROM merm_referensi WHERE ref_head_id='17' ORDER BY merm_referensi.id ASC";
	  $list_nilai=$this->db->query($q_nilai)->result();
	  $q="SELECT  H.*			,CASE 
						WHEN H.jenis_isi=1 THEN H.jawaban_id
						WHEN H.jenis_isi=2 THEN H.jawaban_freetext
						WHEN H.jenis_isi=3 THEN H.jawaban_ttd
					 END as jawaban
 FROM app_reservasi_tanggal_pasien_sc H WHERE H.app_id='$id'";
	  $list_data=$this->db->query($q)->result();
	  $tabel='';
	  foreach ($list_data as $row){
		  
		  $tabel .='<tr>';
		  $tabel .='<td width="50%" ><input type="hidden" class="sc_id" value="'.$row->id.'">'.($row->pertanyaan).'</td>';
	  
		$tabel .='<td width="50%" >'.$this->opsi_nilai_sc($list_nilai,$row->group_jawaban_id,$row->jawaban,$row->jenis_isi,$row->id).'</td>';
			  
		  
		  $tabel .='</tr>';
	  }
	  $arr['tabel']=$tabel;
	  $this->output->set_output(json_encode($arr));
	}
	// function opsi_nilai_sc($list_nilai,$group_nilai,$jawaban_id){
	  // $arr_group_nilai=explode(",",$group_nilai);
	  // $opsi='<select class="js-select2 full-width nilai_sc" style="width:100%" data-placeholder="Pilih Opsi">';
	  // $opsi .='<option value="" '.($jawaban_id==0?'selected':'').' class="has-danger">Opsi Jawaban</option>'; 
	  // foreach($list_nilai as $row){
		  // if (in_array($row->id,$arr_group_nilai)){
			 // $opsi .='<option value="'.$row->id.'" '.($jawaban_id==$row->id?'selected':'').'>'.$row->ref.'</option>'; 
		  // }
	  // }
	  // $opsi .="</select>";
	  // return $opsi;
	// }
	function opsi_nilai_sc($list_nilai,$group_nilai,$jawaban_id,$jenis_isi,$id){
		if ($jenis_isi=='1'){
			  $arr_group_nilai=explode(",",$group_nilai);
			  $opsi='
											   
											  
			  <select class="js-select2 full-width nilai_sc has-error2" style="width:100%;" data-placeholder="Pilih Opsi">';
			  $opsi .='<option value="" '.($jawaban_id==0?'selected':'').'>Opsi Jawaban</option>'; 
			  foreach($list_nilai as $row){
				  if (in_array($row->id,$arr_group_nilai)){
					 $opsi .='<option value="'.$row->id.'" '.($jawaban_id==$row->id?'selected':'').'>'.$row->ref.'</option>'; 
				  }
			  }
			  $opsi .="</select>";
			  return $opsi;
		}
		if ($jenis_isi=='2'){
			  $jawaban_free_text='';
			  $jawaban_free_text='
				<textarea class="form-control nilai_sc_free" placeholder="Isi" required>'.$jawaban_id.'</textarea>
			  ';
			  return $jawaban_free_text;
		}
		if ($jenis_isi=='3'){
			$btn_paraf='';
			$nama_tabel="'app_reservasi_tanggal_pasien_sc'";
			  if ($jawaban_id==''){
				  $btn_paraf= '<button onclick="modal_faraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd_sc"><i class="fa fa-paint-brush"></i></button>';
			  }else{
				  $btn_paraf="<img style='display:block; width:100px;height:100px;' id='base64image' src='".$jawaban_id."' />";
				  $btn_paraf .= '<br><div class="btn-group btn-group-sm" role="group"><button onclick="hapus_paraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Hapus Tanda tangan" class="btn btn-danger btn-xs btn_ttd_sc"><i class="fa fa-trash"></i></button>';
				  $btn_paraf .= '<button onclick="modal_faraf('.$id.','.$nama_tabel.')"  type="button" data-toggle="tooltip" title="Tanda tangan" class="btn btn-success btn-xs btn_ttd_sc"><i class="fa fa-paint-brush"></i></button>';
				  $btn_paraf .= '</div>';
			  }
			  return $btn_paraf;
		}
	}
	function update_nilai_sc(){
	  $sc_id=$this->input->post('sc_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_id' =>$jawaban_id
	  );
	  $this->db->where('id',$sc_id);
	  $result=$this->db->update('app_reservasi_tanggal_pasien_sc',$data);
	  $this->output->set_output(json_encode($result));
	}
	function update_nilai_sp(){
	  $sp_id=$this->input->post('sp_id');
	  $jawaban_id=$this->input->post('nilai_id');
	  $data=array(
		'jawaban_id' =>$jawaban_id
	  );
	  $this->db->where('id',$sp_id);
	  $result=$this->db->update('app_reservasi_tanggal_pasien_sp',$data);
	  $this->output->set_output(json_encode($result));
	}
	public function get_dokter_poli()
	{
		$idpoli=$this->input->post('idpoli');
		$q="SELECT MP.id,MP.nama FROM `mjadwal_dokter` H
			LEFT JOIN mdokter MP ON MP.id=H.iddokter
			WHERE MP.`status`='1'  AND H.idpoliklinik='$idpoli'
			GROUP BY H.iddokter";
		$data = $this->db->query($q)->result();
		$this->output->set_output(json_encode($data));
	}
	public function get_jadwal_dokter_poli()
	{
		$idpoli=$this->input->post('idpoli');
		$iddokter=$this->input->post('iddokter');
		$q="SELECT H.tanggal,CASE WHEN ML.id IS NOT NULL THEN 'LIBUR NASIONAL' ELSE MC.sebab END sebab,CASE WHEN ML.id IS NOT NULL THEN ML.id ELSE MC.id END as cuti_id
				,CASE 
				WHEN MC.id IS NOT NULL THEN 'disabled' 
				WHEN ML.id IS NOT NULL THEN 'disabled' 

				ELSE '' END st_disabel 
				,CONCAT(MR.namahari,', ', DATE_FORMAT(H.tanggal,'%d-%m-%Y') )as tanggal_nama,H.kodehari
				FROM `app_reservasi_tanggal` H
				LEFT JOIN mcuti_dokter MC ON MC.iddokter=H.iddokter AND H.tanggal BETWEEN MC.tanggal_dari AND MC.tanggal_sampai AND MC.`status`='1'
				LEFT JOIN mholiday ML ON H.tanggal  BETWEEN ML.tgl_libur_awal AND ML.tgl_libur_akhir
				LEFT JOIN merm_hari MR ON MR.kodehari=H.kodehari
				WHERE H.reservasi_tipe_id='1' AND H.iddokter='$iddokter' AND H.idpoli='$idpoli' AND H.tanggal >= CURRENT_DATE AND H.tanggal<DATE_ADD(CURRENT_DATE, INTERVAL 7 DAY) 
				GROUP BY H.tanggal";
		// print_r($q);exit;
		$data = $this->db->query($q)->result();
		$this->output->set_output(json_encode($data));
	}
	public function get_jam_dokter_poli()
	{
		$idpoli=$this->input->post('idpoli');
		$iddokter=$this->input->post('iddokter');
		$tanggal=$this->input->post('tanggal');
		$q="SELECT H.jadwal_id,CONCAT(DATE_FORMAT(JD.jam_dari,'%H:%i') ,' - ',DATE_FORMAT(JD.jam_sampai,'%H:%i') ) jam_nama
			,H.saldo_kuota,CASE WHEN H.saldo_kuota > 0 THEN '' ELSE 'disabled' END st_disabel
			FROM `app_reservasi_tanggal` H
			LEFT JOIN mjadwal_dokter JD ON JD.id=H.jadwal_id
			WHERE H.reservasi_tipe_id='1' AND H.iddokter='$iddokter' AND H.idpoli='$idpoli' AND H.tanggal = '$tanggal'
			GROUP BY H.tanggal,H.jadwal_id";
		$data = $this->db->query($q)->result();
		$this->output->set_output(json_encode($data));
	}
	function save_register_poli(){
		// print_r($this->input->post());exit;
		$btn_simpan = $this->input->post('btn_simpan');
		$tahun = $this->input->post('tahun_lahir');
		$bulan = $this->input->post('bulan_lahir');
		$hari = $this->input->post('tgl_lahir');
		$tanggallahir = date("$tahun-$bulan-$hari");
		$nomedrec = ($this->input->post('no_medrec') == '' ? null : $this->input->post('no_medrec'));
		$statuspasienbaru=$this->input->post('statuspasienbaru');
		if ($statuspasienbaru=='1'){//baru
			$idpasien = null;
			$statuspasienbaru = 1;
			$nomedrec = $this->Tpoliklinik_pendaftaran_model->get_nomedrec();
		}else{
			$idpasien = $this->input->post('idpasien');
			$statuspasienbaru = 0;
		}
		
		$pasien = [];
		$pasien['id'] = $idpasien;
		$pasien['no_medrec'] = $nomedrec;
		$pasien['title'] = $this->input->post('title');
		$pasien['nama'] =strtoupper($this->input->post('namapasien'));
		$pasien['jenis_kelamin'] = $this->input->post('jenis_kelamin');
		$pasien['alamat_jalan'] = strtoupper($this->input->post('alamatpasien'));
		$pasien['provinsi_id'] = $this->input->post('provinsi');
		$pasien['kabupaten_id'] = $this->input->post('kabupaten');
		$pasien['kecamatan_id'] = $this->input->post('kecamatan');
		$pasien['kelurahan_id'] = $this->input->post('kelurahan');
		$pasien['kodepos'] = $this->input->post('kodepos');
		$pasien['jenis_id'] = $this->input->post('jenis_id');
		$pasien['ktp'] = $this->input->post('noidentitas');
		$pasien['hp'] = $this->input->post('nohp');
		$pasien['telepon'] = $this->input->post('telprumah');//Belum
		$pasien['email'] = $this->input->post('email');
		$pasien['tempat_lahir'] = strtoupper($this->input->post('tempatlahir'));
		$pasien['tanggal_lahir'] = $tanggallahir;
		$pasien['umur_tahun'] = $this->input->post('umurtahun');
		$pasien['umur_bulan'] = $this->input->post('umurbulan');
		$pasien['umur_hari'] = $this->input->post('umurhari');
		$pasien['golongan_darah'] = $this->input->post('golongan_darah');
		$pasien['agama_id'] = $this->input->post('agama_id');
		$pasien['warganegara'] = $this->input->post('warganegara');
		$pasien['suku'] = $this->input->post('suku');
		$pasien['suku_id'] = $this->input->post('suku_id');
		
		$pasien['status_kawin'] = $this->input->post('statuskawin');
		$pasien['pendidikan_id'] = $this->input->post('pendidikan');
		$pasien['pekerjaan_id'] = $this->input->post('pekerjaan');
		$pasien['nama_keluarga'] = strtoupper($this->input->post('namapenanggungjawab'));
		$pasien['hubungan_dengan_pasien'] = $this->input->post('hubungan');
		$pasien['alamat_keluarga'] = strtoupper($this->input->post('alamatpenanggungjawab'));
		$pasien['telepon_keluarga'] = $this->input->post('telepon');
		$pasien['ktp_keluarga'] = $this->input->post('noidentitaspenanggung');
		

		if ($statuspasienbaru == 1) {
			$pasien['created_by'] = $this->session->userdata('user_id');
			$pasien['created'] = date('Y-m-d H:i:s');
		}
		if ($statuspasienbaru=='1'){//baru
			$this->db->insert('mfpasien', $pasien);
		}else{
			$this->db->where('id',$idpasien);
			$this->db->update('mfpasien', $pasien);
		}
		// if ($this->db->replace('mfpasien', $pasien)) {
			if ($idpasien==null){
				$idpasien=$this->db->insert_id();
			}
			if (!empty($this->input->post('notif_email'))){
				$notif_email='1';			
			}else{			
				$notif_email='0';
			}
			if (!empty($this->input->post('notif_wa'))){
				$notif_wa='1';			
			}else{			
				$notif_wa='0';
			}
			$hashids = new Hashids('RSKBHALMAHERA', 0, 'ABCEFGHJKLMNPRSTUVWXYZ123456789');
			$notransaksi=($this->Tbooking_rehab_model->get_notransaksi());
			$nobooking=substr($notransaksi,-8);
			$kode_booking =  $hashids->encode($nobooking);
			$data=array(
				'waktu_reservasi' =>date('Y-m-d H:i:s'),
				'reservasi_tipe_id' =>$this->input->post('reservasi_tipe_id'),
				'reservasi_cara' => $this->input->post('reservasi_cara'),
				'statuspasienbaru' => $this->input->post('statuspasienbaru'),
				'tanggal' => $this->input->post('tanggal'),
				'idpoli' => $this->input->post('idpoli'),
				'jadwal_id' => $this->input->post('jadwal_id'),
				'jam_id' => null,
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'catatan' => $this->input->post('catatan'),
				'idruangan' => $this->input->post('idruangan'),
				'idpasien' => $idpasien,
				'title' => $this->input->post('title'),
				'namapasien' => strtoupper($this->input->post('namapasien')),
				'no_medrec' => $nomedrec,
				'nohp' => $this->input->post('nohp'),
				'telepon' => $this->input->post('telprumah'),
				'email' => $this->input->post('email'),
				'golongan_darah' => $this->input->post('golongan_darah'),
				'tempat_lahir' => strtoupper($this->input->post('tempatlahir')),
				'tanggal_lahir' => $tanggallahir,
				'alamatpasien' => strtoupper($this->input->post('alamatpasien')),
				'agama_id' => $this->input->post('agama_id'),
				'statuskawin' => $this->input->post('statuskawin'),
				'pendidikan_id' => $this->input->post('pendidikan_id'),
				'pekerjaan_id' => $this->input->post('pekerjaan_id'),
				'warganegara' => $this->input->post('warganegara'),
				'provinsi_id' => $this->input->post('provinsi'),
				'kabupaten_id' => $this->input->post('kabupaten'),
				'kecamatan_id' => $this->input->post('kecamatan'),
				'kelurahan_id' => $this->input->post('kelurahan'),
				'kodepos' => $this->input->post('kodepos'),
				'idasalpasien' => $this->input->post('idasalpasien'),
				'idrujukan' => $this->input->post('idrujukan'),
				'idtipepasien' => $this->input->post('idtipepasien'),
				'idkelompokpasien' => $this->input->post('kelompokpasien'),
				'idrekanan' => ($this->input->post('kelompokpasien')=='1'?$this->input->post('idrekanan'):null),
				'pendidikan_id' => $this->input->post('pendidikan'),
				'pekerjaan_id' => $this->input->post('pekerjaan'),
				'jenis_id' => $this->input->post('jenis_id'),
				'noidentitas' => $this->input->post('noidentitas'),
				'idpoliklinik' => $this->input->post('idpoli'),
				'iddokter' => $this->input->post('iddokter'),
				'status_reservasi' => 1,
				'status_kehadiran' => 0,
				'namapenanggungjawab' => strtoupper($this->input->post('namapenanggungjawab')),
				'teleponpenanggungjawab' => $this->input->post('telepon'),
				'noidentitaspenanggungjawab' => $this->input->post('noidentitaspenanggung'),
				'alamatpenanggungjawab' => strtoupper($this->input->post('alamatpenanggungjawab')),
				'umurhari' => $this->input->post('umurhari'),
				'umurbulan' => $this->input->post('umurbulan'),
				'umurtahun' => $this->input->post('umurtahun'),
				'hubungan' => $this->input->post('hubungan'),
				'suku_id' => $this->input->post('suku_id'),
				'suku' => $this->input->post('suku'),
				'pertemuan_id' => $this->input->post('pertemuan_id'),
				'st_gc' => $this->input->post('st_gc'),
				'st_general' => ($this->input->post('st_gc')=='1'?0:1),
				'st_sp' => $this->input->post('st_sp'),
				'st_skrining' => ($this->input->post('st_sp')=='1'?0:1),
				'st_sc' => $this->input->post('st_sc'),
				'st_covid' => ($this->input->post('st_sc')=='1'?0:1),
				'notif_wa' => $notif_wa,
				'notif_email' => $notif_email,
				'notransaksi' => $notransaksi,
				'nobooking' => $nobooking,
				'kode_booking' => $kode_booking,
				'created_by' => $this->session->userdata('user_id'),
				'created_date' => date('Y-m-d H:i:s'),
				
			);
			$foto_kartu=$this->upload_foto_kartu(false);
			if ($foto_kartu){
				$data['foto_kartu']=$foto_kartu;
				
			}
			// print_r($data['foto_kartu']);exit;
				$hasil=$this->db->insert('app_reservasi_tanggal_pasien',$data);
				$app_id=$this->db->insert_id();
				if($hasil){
					insertPoliklinikTindakan_All();
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','data telah disimpan.');
					if ($btn_simpan=='1'){
						redirect('tbooking/poliklinik','location');						
					}else{
						redirect('tbooking/edit_poli/'.$app_id,'location');						
					}
				}
		// }
			// print_r($data);exit;
	}
	public function upload_foto_kartu($update = false)
    {
        if (!file_exists('assets/upload/foto_kartu')) {
            mkdir('assets/upload/foto_kartu', 0755, true);
        }

        if (isset($_FILES['foto_kartu'])) {
            if ($_FILES['foto_kartu']['name'] != '') {
                $config['upload_path'] = './assets/upload/foto_kartu/';
				// $config['upload_path'] = './assets/upload/asset_pemindahan/';
               	$config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
				$config['encrypt_name']  = TRUE;
				$config['overwrite']  = FALSE;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				// $this->load->library('myimage');	
				

                $this->load->library('upload', $config);
				// print_r	($config['upload_path']);exit;
                if ($this->upload->do_upload('foto_kartu')) {
                    $image_upload = $this->upload->data();
                    $data['foto_kartu'] = $image_upload['file_name'];
					// print_r($data);exit;
                    if ($update == true) {
                        $this->remove_image_foto_kartu($this->input->post('id'));
                    }
                    return $data['foto_kartu'];
                } else {
					print_r	($this->upload->display_errors());exit;
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
            } else {
                return false;
            }
					// print_r($this->foto);exit;
        } else {
            return false;
        }
		
    }
	public function remove_image_foto_kartu($id)
    {
		$q="select foto_kartu From merm_general H WHERE H.id='$id'";
        $row = $this->db->query($q)->row();
        if (file_exists('./assets/upload/app_setting/'.$row->foto_kartu) && $row->foto_kartu !='') {
            unlink('./assets/upload/app_setting/'.$row->foto_kartu);
        }
    }
	function save_register_sp(){//Skrining pasien
		// print_r($this->input->post());exit;
		$id = $this->input->post('id');
		$btn_simpan = $this->input->post('btn_simpan');
		$data=array(
			'st_skrining' =>1,
			'user_sp' =>$this->session->userdata('user_name'),
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('app_reservasi_tanggal_pasien',$data);
		if ($hasil){			
			if ($btn_simpan=='2'){//Selesai
				redirect('tbooking/poliklinik','location');						
			}else{
				redirect('tbooking/edit_poli/'.$id,'location');						
			}
		}
		
	}
	function save_register_gc(){//Skrining pasien
		// print_r($this->input->post());exit;
		$id = $this->input->post('id');
		$btn_simpan = $this->input->post('btn_simpan');
		$data=array(
			'st_general' =>1,
			'user_gc' =>$this->session->userdata('user_name'),
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('app_reservasi_tanggal_pasien',$data);
		if ($hasil){			
			if ($btn_simpan=='2'){//Selesai
				redirect('tbooking/poliklinik','location');						
			}else{
				redirect('tbooking/edit_poli/'.$id,'location');						
			}
		}
		
	}
	function save_register_sc(){//Skrining Covid
		// print_r($this->input->post());exit;
		$id = $this->input->post('id');
		$btn_simpan = $this->input->post('btn_simpan');
		$data=array(
			'st_covid' =>1,
			'user_sc' =>$this->session->userdata('user_name'),
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('app_reservasi_tanggal_pasien',$data);
		if ($hasil){			
			if ($btn_simpan=='2'){//Selesai
				redirect('tbooking/poliklinik','location');						
			}else{
				redirect('tbooking/edit_poli/'.$id,'location');						
			}
		}
		
	}
	function save_register_rincian(){//Skrining Covid
		// print_r($this->input->post());exit;
		$id = $this->input->post('id');
		$idkelompokpasien = $this->input->post('kelompokpasien2');
		$idrekanan = ($idkelompokpasien=='1'?$this->input->post('idrekanan2'):0);
		$pertemuan_id = $this->input->post('pertemuan_id2');
		$data=array(
			'idkelompokpasien' =>$idkelompokpasien,
			'idrekanan' =>$idrekanan,
			'pertemuan_id' =>$pertemuan_id,
			'edited_by' =>$this->session->userdata('user_id'),
			'edited_date' =>date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id);
		$hasil=$this->db->update('app_reservasi_tanggal_pasien',$data);
		if ($hasil){			
			redirect('tbooking/edit_poli/'.$id,'location');	
		}
		
	}
	function getIndex_proses()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$reservasi_cara =$this->input->post('reservasi_cara');
			$statuspasienbaru =$this->input->post('statuspasienbaru');
			$status_reservasi =$this->input->post('status_reservasi');
			$status_kehadiran =$this->input->post('status_kehadiran');
			$tanggal_1 =$this->input->post('tanggal_1');
			$tanggal_2 =$this->input->post('tanggal_2');
			$no_medrec =$this->input->post('no_medrec');
			$namapasien =$this->input->post('namapasien');
			$idpoli =$this->input->post('idpoli');
			$iddokter =$this->input->post('iddokter');
			$waktu_reservasi_1 =$this->input->post('waktu_reservasi_1');
			$waktu_reservasi_2 =$this->input->post('waktu_reservasi_2');
			if ($namapasien !=''){
				$where .=" AND (H.namapasien) LIKE '%".($namapasien)."%'";
			}
			if ($no_medrec !=''){
				$where .=" AND (H.no_medrec) LIKE '%".($no_medrec)."%'";
			}
			if ($idpoli !='#'){
				$where .=" AND (H.idpoli) ='".($idpoli)."'";
			}
			if ($iddokter !='#'){
				$where .=" AND (H.iddokter) ='".($iddokter)."'";
			}
			if ($reservasi_cara !='#'){
				$where .=" AND (H.reservasi_cara) ='".($reservasi_cara)."'";
			}
			if ($statuspasienbaru !='#'){
				$where .=" AND (H.statuspasienbaru) ='".($statuspasienbaru)."'";
			}
			if ($status_kehadiran !='#'){
				$where .=" AND (H.status_kehadiran) ='".($status_kehadiran)."'";
			}
			if ($status_reservasi !='#'){
				if ($status_reservasi=='2'){
				$where .=" AND (H.status_reservasi) >='".($status_reservasi)."'";
					
				}else{
					
				$where .=" AND (H.status_reservasi) ='".($status_reservasi)."'";
				}
			}
			
			
			if ($tanggal_1 !=''){
				$where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_2)."'";
			}else{
				$where .=" AND DATE(H.tanggal) >='".date('Y-m-d')."'";
			}
			if ($waktu_reservasi_1 !=''){
				$where .=" AND DATE(H.waktu_reservasi) >='".YMDFormat($waktu_reservasi_1)."' AND DATE(H.waktu_reservasi) <='".YMDFormat($waktu_reservasi_2)."'";
			}
			
			$this->select = array();
			$from="
					(
						SELECT H.id,H.reservasi_tipe_id,H.reservasi_cara,H.statuspasienbaru,H.waktu_reservasi,H.tanggal,H.st_validasi 
						,H.no_medrec,H.namapasien,P.nama as nama_poli,MD.nama as nama_dokter,H.status_reservasi,H.status_kehadiran
						,CONCAT(DATE_FORMAT(MJ.jam_dari,'%H:%i'),' - ',DATE_FORMAT(MJ.jam_sampai,'%H:%i')) as jam,MH.namahari,H.kodehari,H.noantrian
						,(H.st_general+H.st_skrining+H.st_covid) as jml_skrining
						,H.st_gc,H.st_general,H.st_sp,H.st_skrining,H.st_sc,H.st_covid
						FROM app_reservasi_tanggal_pasien H
						INNER JOIN mpoliklinik P ON P.id=H.idpoli
						INNER JOIN mdokter MD ON MD.id=H.iddokter
						INNER JOIN mjadwal_dokter MJ ON MJ.id=H.jadwal_id
						LEFT JOIN merm_hari MH ON MH.kodehari=H.kodehari
						WHERE H.reservasi_tipe_id='1' ".$where."
						ORDER BY H.tanggal ASC,H.id ASC
					) as tbl WHERE id IS NOT NULL 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('nama_dokter');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();
		  $label_skrining='';
		  $disabel_verif='';
		  // if ($r->jml_skrining<3){
			  // $label_skrining='<br><br>'.'<a href="'.base_url().'tbooking/edit_poli/'.$r->id.'" type="button" data-toggle="tooltip" title="BELUM SKRINING" class="btn btn-danger btn-xs"><i class="fa fa-eye"></i> BELUM SKRINING</a>';;
			  // // $disabel_verif='disabled';
		  // }
		  $label_skrining='<br><br>'.label_skrining($r->st_gc,$r->st_general,$r->st_sp,$r->st_skrining,$r->st_sc,$r->st_covid);
          $result[] = $no;
          $result[] = jenis_reservasi($r->reservasi_cara);
          $result[] = HumanDateLong($r->waktu_reservasi);
          $result[] = ($r->statuspasienbaru=='0'?text_primary('Pasien Lama'):text_danger('Pasien Baru'));
          $result[] = $r->no_medrec.'<br>'.$r->namapasien;
          $result[] = '<strong>'.$r->nama_poli.'</strong><br>'.$r->nama_dokter;
          $result[] ='<strong>'. $r->namahari.', '.HumanDateShort_exp($r->tanggal).'</strong><br>'.text_primary($r->jam);
          $result[] = $r->noantrian;
          $result[] = status_reservasi($r->status_reservasi).$label_skrining;
          $result[] = ($r->status_kehadiran=='0'?text_default('Menunggu Kehadiran'):text_success('Sudah Hadir'));
          $aksi = '<div class="btn-group">';
			if ($r->status_reservasi !='0'){
				
			
				if (UserAccesForm($user_acces_form,array('1499'))){
				if ($r->st_validasi=='0'){
					$aksi .= '<button onclick="verifikasi_poli('.$r->id.')" '.$disabel_verif.' type="button" data-toggle="tooltip" title="Verifikasi" class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>';
				}
				}
				
				
				if (UserAccesForm($user_acces_form,array('1501'))){
				$aksi .= '<a href="'.base_url().'tbooking/edit_poli/'.$r->id.'" type="button" data-toggle="tooltip" title="Lihat Detail" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1502'))){
				$aksi .= '<a href="'.base_url().'tbooking/print_poli" type="button" data-toggle="tooltip" title="Print Bukti" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
				}
				if (UserAccesForm($user_acces_form,array('1500'))){
					if ($r->status_reservasi=='2'){
						$aksi .= '<button onclick="batalkan_poli('.$r->id.')" type="button" data-toggle="tooltip" title="Batalkan Verifikasi" class="btn btn-danger btn-xs"><i class="si si-ban"></i></button>';
					}
				}
				if (UserAccesForm($user_acces_form,array('1587'))){
					if ($r->status_reservasi!='3'){
						$aksi .= '<button onclick="hapus_poli('.$r->id.')" type="button" data-toggle="tooltip" title="Hapus " class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>';
					}
				}
	  }else{
		 $aksi .=text_danger('DIHAPUS');
	  }
		  $aksi .= '</div>';
          $result[] = $aksi;

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function getIndex_daftar()
	{
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		// tipe_pemilik:tipe_pemilik,status:status,nama:nama
		$where='';
		$reservasi_cara =$this->input->post('reservasi_cara');
		$statuspasienbaru =$this->input->post('statuspasienbaru');
		$status_kehadiran =$this->input->post('status_kehadiran');
		$tanggal_1 =$this->input->post('tanggal_1');
		$tanggal_2 =$this->input->post('tanggal_2');
		$no_medrec =$this->input->post('no_medrec');
		$namapasien =$this->input->post('namapasien');
		$idpoli =$this->input->post('idpoli');
		$iddokter =$this->input->post('iddokter');
		$waktu_reservasi_1 =$this->input->post('waktu_reservasi_1');
		$waktu_reservasi_2 =$this->input->post('waktu_reservasi_2');
		$where .=" AND (H.status_reservasi) >='2'";
		if ($namapasien !=''){
			$where .=" AND (H.namapasien) LIKE '%".($namapasien)."%'";
		}
		if ($no_medrec !=''){
			$where .=" AND (H.no_medrec) LIKE '%".($no_medrec)."%'";
		}
		if ($idpoli !='#'){
			$where .=" AND (H.idpoli) ='".($idpoli)."'";
		}
		if ($iddokter !='#'){
			$where .=" AND (H.iddokter) ='".($iddokter)."'";
		}
		if ($reservasi_cara !='#'){
			$where .=" AND (H.reservasi_cara) ='".($reservasi_cara)."'";
		}
		if ($statuspasienbaru !='#'){
			$where .=" AND (H.statuspasienbaru) ='".($statuspasienbaru)."'";
		}
		if ($status_kehadiran !='#'){
			$where .=" AND (H.status_kehadiran) ='".($status_kehadiran)."'";
		}
		
		if ($tanggal_1 !=''){
			$where .=" AND DATE(H.tanggal) >='".YMDFormat($tanggal_1)."' AND DATE(H.tanggal) <='".YMDFormat($tanggal_2)."'";
		}else{
			$where .=" AND DATE(H.tanggal) >='".date('Y-m-d')."'";
		}
		if ($waktu_reservasi_1 !=''){
			$where .=" AND DATE(H.waktu_reservasi) >='".YMDFormat($waktu_reservasi_1)."' AND DATE(H.waktu_reservasi) <='".YMDFormat($waktu_reservasi_2)."'";
		}
		
		$this->select = array();
		$from="
				(
					SELECT H.id,H.reservasi_tipe_id,H.reservasi_cara,H.statuspasienbaru,H.waktu_reservasi,H.tanggal 
					,H.no_medrec,H.namapasien,P.nama as nama_poli,MD.nama as nama_dokter,H.status_reservasi,H.status_kehadiran
					,CONCAT(DATE_FORMAT(MJ.jam_dari,'%H:%i'),' - ',DATE_FORMAT(MJ.jam_sampai,'%H:%i')) as jam,MH.namahari,H.kodehari,H.noantrian
					,CASE WHEN H.idkelompokpasien=1 THEN MR.nama ELSE MK.nama END as asuransi,H.idpoliklinik,H.idkelompokpasien
					,H.st_gc,H.st_general,H.st_sp,H.st_skrining,H.st_sc,H.st_covid
					FROM app_reservasi_tanggal_pasien H
					INNER JOIN mpoliklinik P ON P.id=H.idpoli
					INNER JOIN mpasien_kelompok MK ON MK.id=H.idkelompokpasien
					LEFT JOIN mrekanan MR ON MR.id=H.idrekanan
					INNER JOIN mdokter MD ON MD.id=H.iddokter
					INNER JOIN mjadwal_dokter MJ ON MJ.id=H.jadwal_id
					LEFT JOIN merm_hari MH ON MH.kodehari=H.kodehari
					WHERE H.reservasi_tipe_id='1' ".$where."
					ORDER BY H.tanggal ASC,H.noantrian ASC
				) as tbl WHERE id IS NOT NULL 
			";
		// print_r($from);exit();
		$this->from   = $from;
		$this->join 	= array();
		
		
		$this->order  = array();
		$this->group  = array();
		$this->column_search = array('nama_dokter','nama_poli','namapasien');
		$this->column_order  = array();

	  $list = $this->datatable->get_datatables(true);
	  $data = array();
	  $no = $_POST['start'];
	  foreach ($list as $r) {
		  $no++;
		  $result = array();
			$label_skrining='<br><br>'.label_skrining($r->st_gc,$r->st_general,$r->st_sp,$r->st_skrining,$r->st_sc,$r->st_covid);
		  $result[] = $no;
		  $result[] = jenis_reservasi($r->reservasi_cara);
		  $result[] = HumanDateLong($r->waktu_reservasi);
		  $result[] = ($r->statuspasienbaru=='0'?text_primary('Pasien Lama'):text_danger('Pasien Baru'));
		  $result[] = $r->no_medrec.'<br>'.$r->namapasien;
		  $result[] = '<strong>'.$r->nama_poli.'</strong><br>'.$r->nama_dokter;
		  $result[] ='<strong>'. $r->namahari.', '.HumanDateShort_exp($r->tanggal).'</strong><br>'.text_primary($r->jam);
		  $result[] = $r->noantrian;
		  $result[] = status_reservasi($r->status_reservasi).$label_skrining;
		  $result[] = $r->asuransi;
		  // $result[] = ($r->status_kehadiran=='0'?text_default('Menunggu Kehadiran'):text_success('Sudah Hadir'));
		  $aksi = '<div class="btn-group">';
			  
				
				$aksi .= '<a href="'.base_url().'tbooking/edit_poli/'.$r->id.'" type="button" data-toggle="tooltip" title="Lihat Detail" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
				$aksi .= '<a href="'.base_url().'tbooking/print_poli" type="button" data-toggle="tooltip" title="Print Bukti" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
				
		  $aksi .= '</div>';
		  $result[] = $aksi;

		  $data[] = $result;
	  }
	  $output = array(
		  "draw" => $_POST['draw'],
		  "recordsTotal" => $this->datatable->count_all(true),
		  "recordsFiltered" => $this->datatable->count_all(true),
		  "data" => $data
	  );
	  echo json_encode($output);
  }
  function load_index_history()
	{
			$data_user=get_acces();
			$user_acces_form=$data_user['user_acces_form'];
			// tipe_pemilik:tipe_pemilik,status:status,nama:nama
			$where='';
			$id =$this->input->post('id');
			
			$this->select = array();
			$from="
					(
						SELECT * FROM app_reservasi_tanggal_pasien_activity WHERE app_id='$id' ORDER BY id ASC
					) as tbl WHERE id IS NOT NULL 
				";
			// print_r($from);exit();
			$this->from   = $from;
			$this->join 	= array();
			
			
			$this->order  = array();
			$this->group  = array();
			$this->column_search = array('aktifitas');
			$this->column_order  = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $result = array();

          $result[] = $no;
          $result[] = ($r->aktifitas);
          $result[] = ($r->detail_aktifitas);
          $result[] = $r->usernama.'<br>'.HumanDateLong($r->tanggal_transaksi);
         

          $data[] = $result;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function verifikasi_poli(){
	   $id=$this->input->post('id');
	  $this->validated_by = $this->session->userdata('user_id');
	  $this->validated_date = date('Y-m-d H:i:s');
	  $this->status_reservasi=2;
	  $this->st_validasi=1;
	  
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('app_reservasi_tanggal_pasien',$this);
	  
	  json_encode($hasil);
  }
  function batal_poli(){
	   $id=$this->input->post('id');
	  $this->canceled_by = $this->session->userdata('user_id');
	  $this->canceled_date = date('Y-m-d H:i:s');
	  $this->status_reservasi=1;
	  $this->st_validasi=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('app_reservasi_tanggal_pasien',$this);
	  
	  json_encode($hasil);
  }
  function hapus_poli(){
	   $id=$this->input->post('id');
	  $this->deleted_by = $this->session->userdata('user_id');
	  $this->deleted_date = date('Y-m-d H:i:s');
	  $this->status_reservasi=0;
	  // $this->st_validasi=0;
	  $this->db->where('id',$id);
	  $hasil=$this->db->update('app_reservasi_tanggal_pasien',$this);
	  
	  json_encode($hasil);
  }
  function get_logic_pendaftaran_poli(){
	  $jenis_pertemuan_id=$this->input->post('jenis_pertemuan_id');
	  $idpoliklinik=$this->input->post('idpoliklinik');
	  $iddokter=$this->input->post('iddokter');
	  $st_gc='0';$st_sp='0';$st_sc='0';
	  $hasil=get_logic_reservasi_poli_petugas($jenis_pertemuan_id,$idpoliklinik,$iddokter);
	  if ($hasil){
		  $st_gc=$hasil->st_gc;
		  $st_sp=$hasil->st_sp;
		  $st_sc=$hasil->st_sc;
	  }
	  $arr['st_gc']=$st_gc;
	  $arr['st_sp']=$st_sp;
	  $arr['st_sc']=$st_sc;
	  $this->output->set_output(json_encode($arr));
  }
  function hasil_generate(){
			$idpoli=$this->input->post('idpoli');
			$iddokter=$this->input->post('iddokter');
		  $tanggaldari=YMDFormat($this->input->post('tanggaldari'));
		  $tanggalsampai=YMDFormat($this->input->post('tanggalsampai'));
		  $where_dokter='';
		  if ($iddokter){
			  $iddokter=implode(", ", $iddokter);
			  $where_dokter .=" AND H.iddokter IN($iddokter)";
		  }
		  if ($idpoli){
			  $idpoli=implode(", ", $idpoli);
			  $where_dokter .=" AND H.idpoliklinik IN ($idpoli)";
		  }
		 
		  $q_dokter="SELECT H.iddokter,MD.nama,MD.foto,MD.jeniskelamin,MD.nip,MP.nama as poli,H.idpoliklinik FROM mjadwal_dokter H
				LEFT JOIN mdokter MD ON MD.id=H.iddokter
				LEFT JOIN mpoliklinik MP ON MP.id=H.idpoliklinik
				WHERE H.kuota >= 0 ".$where_dokter."
				GROUP BY H.idpoliklinik,H.iddokter";
		  $dokter=$this->db->query($q_dokter)->result();
		  
		  $q="SELECT D.tanggal,D.idpoli,D.iddokter,D.kodehari,D.jadwal_id,CONCAT(DATE_FORMAT(MJ.jam_dari,'%H:%i'),' - ',DATE_FORMAT(MJ.jam_sampai,'%H:%i')) as jam,D.kuota,D.jumlah_reservasi,D.saldo_kuota,MH.namahari
,CD.sebab,CASE WHEN CD.id IS NOT NULL THEN 1 ELSE 0 END as st_cuti,CD.alasan,D.catatan
					FROM app_reservasi_tanggal D
					INNER JOIN mjadwal_dokter MJ ON MJ.id=D.jadwal_id
					LEFT JOIN merm_hari MH ON MH.kodehari=D.kodehari
					LEFT JOIN mcuti_dokter CD ON D.tanggal BETWEEN CD.tanggal_dari AND CD.tanggal_sampai AND CD.status='1' AND CD.iddokter=D.iddokter
					WHERE (D.tanggal BETWEEN '$tanggaldari' AND '$tanggalsampai') AND D.reservasi_tipe_id='1'

					ORDER BY D.tanggal ASC
				";
			$record_detail=$this->db->query($q)->result_array();

			$q="SELECT D.id,D.tanggal,D.idpoli,D.iddokter,D.kodehari,D.jadwal_id,CONCAT(DATE_FORMAT(MJ.jam_dari,'%H:%i'),' - ',DATE_FORMAT(MJ.jam_sampai,'%H:%i')) as jam,MH.namahari
				,D.idpasien,D.no_medrec,D.namapasien,D.idkelompokpasien,KP.nama as kelompok_pasien
				,CASE WHEN D.idkelompokpasien = '1' THEN MR.nama ELSE '' END as asuransi 
				,D.status_reservasi,D.status_kehadiran,D.noantrian
									FROM app_reservasi_tanggal_pasien D
									INNER JOIN mjadwal_dokter MJ ON MJ.id=D.jadwal_id
									LEFT JOIN merm_hari MH ON MH.kodehari=D.kodehari
									LEFT JOIN mpasien_kelompok KP ON KP.id=D.idkelompokpasien
									LEFT JOIN mrekanan MR ON MR.id=D.idrekanan
									WHERE (D.tanggal BETWEEN '$tanggaldari' AND '$tanggalsampai') AND D.reservasi_tipe_id='1'

									ORDER BY D.tanggal,D.noantrian ASC
				";
			$record_pasien=$this->db->query($q)->result_array();
			
		 $q="
				SELECT D.*,MH.namahari,CASE WHEN ML.id IS NOT NULL THEN 1 ELSE 0 END st_libur 
				FROM date_row D 
				LEFT JOIN mholiday ML ON D.tanggal BETWEEN ML.tgl_libur_awal AND ML.tgl_libur_akhir
				LEFT JOIN merm_hari MH ON MH.kodehari=D.hari 
				WHERE D.tanggal BETWEEN '$tanggaldari' AND '$tanggalsampai'";
		  $record=$this->db->query($q)->result();
		  
		  $tabel='<table class="table table table-bordered table-condensed" id="index_ls">';
		  $tabel .='<thead>';
		  $header='<tr>';
				$header .='<td class="text-center column-day" style="width:20%"></td>';
		  foreach($record as $r){
				if ($r->st_libur){
				$header .='<td class="text-center column-libur">'.$r->namahari.'<br>'.DMYFormat2($r->tanggal).'</td>';
					
				}else{
				$header .='<td class="text-center column-day">'.$r->namahari.'<br>'.DMYFormat2($r->tanggal).'</td>';
					
				}
		  }
		  $header .='</tr>';
		  $tabel .='</thead>';
		  $tabel .='<tbody>';
		  
		  $tabel .=$this->generate_detail_ls($dokter,$record_detail,$record,$header,$record_pasien);
		  $tabel .='';
		  
		  $tabel .='</tbody>';
		  $tabel .='</tabel>';
		 
		 $arr['tabel']=$tabel;
		$this->output->set_output(json_encode($arr));
	}
	function generate_detail_ls($dokter,$record_detail,$record,$header,$record_pasien){
		// print_r($record_detail);exit;
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		$jml_colspan=count($record);
		$tabel ='';
		foreach($dokter as $r){
			$iddokter=$r->iddokter;
			$idpoli=$r->idpoliklinik;
			$foto=$r->foto;
			$nama=$r->nama;
			$nip=$r->nip;
			$jeniskelamin=$r->jeniskelamin;
			$poli=strtoupper($r->poli);
			$tabel .=$header;
			$tabel .='<tr>';
			$tabel .='<td class="column-poli text-center" colspan="'.($jml_colspan+1).'">'.$poli.'</td>';
								for($i=1;$i<$jml_colspan;$i++){
									$tabel .='<td style="display: none"></td>';
								}
			// $tabel .='';
			$tabel .='</tr>';
			$tabel .='<tr>';
			$tabel .='<td class="text-center ">
							<div class="profile-dockter">
								<div class="image">
                                    <img src="'.base_url().'assets/upload/dokter/default-man.jpeg" style="width:30%;height:30%">
                                </div>
								<div class="information">
                                    <div class="name">'.$nama.'</div>
                                    <div class="uniqueId">'.$nip .'</div>
                                </div>
                            </div>
								</td>';
			foreach($record as $tgl){
				$tgl_harian=$tgl->tanggal;
				$tgl_harian_str="'".$tgl->tanggal."'";
				$list_kuota='';
				$list_saldo='';
				$data_kuota = array_filter($record_detail, function($var) use ($tgl_harian,$idpoli,$iddokter) { 
				return ($var['tanggal'] == $tgl_harian && $var['idpoli'] == $idpoli && $var['iddokter'] == $iddokter);
				});
				// $tgl="'".$tgl->tanggal."'";
				$btn_add='';
				$btn_edit_slot='';
				$class_libur='';
				if ($tgl->st_libur){
					$class_libur=' default ';
				}else{
					
					$class_libur=' success ';
				}
				if ($data_kuota){
					foreach($data_kuota as $data){
						$catatan=($data['catatan']?' '.text_default($data['catatan']):'');
						$jadwal_id=$data['jadwal_id'];
						$saldo_kuota=$data['saldo_kuota'];
						if (UserAccesForm($user_acces_form,array('1570'))){
							if ($tgl_harian>=date('Y-m-d')){
								$btn_edit_slot .='&nbsp;&nbsp; <button type="button" onclick="add_kuota('.$tgl_harian_str.','.$iddokter.','.$jadwal_id.')" title="Edit Slot" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></button>';
							}
						}
						if (UserAccesForm($user_acces_form,array('1571'))){
							if ($tgl_harian>=date('Y-m-d')){
								$btn_edit_slot .='<a href="'.site_url().'mjadwal_dokter/manage_slot/'.$iddokter.'/'.$idpoli.'" data-toggle="tooltip" target="_blank" title="Slot Harian" class="btn btn-primary btn-xs"><i class="si si-settings"></i></a>';
							}
						}
						$btn_edit_slot .='<br><br>';
						if ($tgl_harian>=date('Y-m-d')){
							if (UserAccesForm($user_acces_form,array('1498'))){
								if ($saldo_kuota>0){
									$btn_add='&nbsp;&nbsp; <a href="'.site_url().'tbooking/add_reservasi/'.$idpoli.'/'.$iddokter.'/'.$jadwal_id.'/'.$tgl_harian.'" title="Tambah Pasien" target="_blank" class="btn btn-xs btn-success"><i class="fa fa-plus"></i> '.$saldo_kuota.'</a><br>';
								}else{
									$btn_add=" ".text_danger('FULL');
								}
							}
						}
						if ($tgl->st_libur){
							$btn_add='';
								$btn_edit_slot ='';
						}
									// $btn_add=" ".text_danger('FULL');
						
						// if ($saldo_kuota<=0){
							// $list_saldo='<br>'.text_danger('FULL');
						// }else{
							// $list_saldo='<br>'.text_primary('SISA KUOTA '.$saldo_kuota);
							
						// }
						$data_pasien = array_filter($record_pasien, function($var) use ($tgl_harian,$idpoli,$iddokter,$jadwal_id) { 
						return ($var['tanggal'] == $tgl_harian && $var['idpoli'] == $idpoli && $var['iddokter'] == $iddokter && $var['jadwal_id'] == $jadwal_id);
						});
						if ($data['st_cuti']=='1'){
							$list_kuota .='<div class="timetable danger">
									<div class="time">'. strtoupper($data['sebab']).'</div>
									<div class="room"> <br>('.$data['alasan'].')</div>
								</div> ';
						}else{
							$list_kuota .='
								<div class="block block-link-hover3" >
                         
										<div class="timetable '.$class_libur.' " >
											<div class="time">'.$data['jam'].$btn_add.$catatan.'</div>
											<div class="room"> <br>('.$saldo_kuota.' / '.$data['kuota'].' SLOT)'.''.$btn_edit_slot.'</div>';
							$list_pasien='';
								if ($data_pasien){
									$nourut=1;
									foreach($data_pasien as $pasien){
										$str_pasien=$nourut.'. '.$pasien['no_medrec'].' - '.$pasien['namapasien'].' | '.($pasien['asuransi']?$pasien['asuransi']:$pasien['kelompok_pasien']);
										$list_pasien .=$this->data_pasien($str_pasien,$pasien['status_reservasi'],$pasien['id']);
										$nourut=$nourut+1;
									}						
								}else{
									$list_pasien='';
								}		
							$list_kuota .=$list_pasien;				
							$list_kuota .='				
										</div> 
								</div>
							';
						}
						
										
					}
				}else{
					$list_kuota .='';
				}
				
				$tabel .='<td class="text-center">'.$list_kuota.'</td>';
				
			}
		$tabel .='</tr>';
		}
		return $tabel;
	}
	function data_pasien($pasien,$status_reservasi,$id){
		// $hasil='<p class="nice-copy2">';
			$hasil='';		
		if ($status_reservasi=='0'){
			$hasil .='<a href="'.site_url().'tbooking/edit_poli/'.$id.'" target="_blank" class="btn btn-xs btn-danger">'.$pasien.'</a><br>';
		}
		if ($status_reservasi=='1'){
			$hasil .='<a href="'.site_url().'tbooking/edit_poli/'.$id.'" target="_blank" class="btn btn-xs btn-default">'.$pasien.'</a><br>';
		}
		if ($status_reservasi=='2'){
			$hasil .='<a href="'.site_url().'tbooking/edit_poli/'.$id.'" target="_blank" class="btn btn-xs btn-default">'.$pasien.'</a><br>';
		}
		if ($status_reservasi=='3'){
			$hasil .='<a href="'.site_url().'tbooking/edit_poli/'.$id.'" target="_blank" class="btn btn-xs btn-primary">'.$pasien.'</a><br>';
		}
		if ($status_reservasi=='4'){
			$hasil .='<a href="'.site_url().'tbooking/edit_poli/'.$id.'" target="_blank" class="btn btn-xs btn-success">'.$pasien.'</a><br>';
		}
		// $hasil .='</p>';
		return $hasil;
	}
	function get_barcode($id)
    {
        $this->load->library('Barcode_generator');
        $this->barcode_generator->output_image('png', 'qr-h',$id,array());
    }
	function save_ttd(){
		
		$id = $this->input->post('app_id');
		$nama_tabel = $this->input->post('nama_tabel');
		$jawaban_ttd = $this->input->post('signature64');
		$data=array(
			'jawaban_ttd' =>$jawaban_ttd,
		);
		// print_r($id);
		$this->db->where('id',$id);
		$hasil=$this->db->update($nama_tabel,$data);
		$this->output->set_output(json_encode($hasil));
	}
	function save_ttd_2(){
		
		$id = $this->input->post('app_id');
		$nama_tabel = 'app_reservasi_tanggal_pasien_gc_head';
		$jawaban_ttd = $this->input->post('signature64');
		$data=array(
			'jawaban_ttd' =>$jawaban_ttd,
		);
		// print_r($id);
		$this->db->where('app_id',$id);
		$hasil=$this->db->update($nama_tabel,$data);
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_paraf(){
		
		$id = $this->input->post('app_id');
		$nama_tabel = $this->input->post('nama_tabel');
		
		$data=array(
			'jawaban_ttd' =>'',
		);
		// print_r($id);
		$this->db->where('id',$id);
		$hasil=$this->db->update($nama_tabel,$data);
		$this->output->set_output(json_encode($hasil));
	}
	function hapus_ttd(){
		
		$id = $this->input->post('app_id');
		// $nama_tabel = $this->input->post('nama_tabel');
		
		$data=array(
			'jawaban_ttd' =>'',
		);
		// print_r($id);
		$this->db->where('app_id',$id);
		$hasil=$this->db->update('app_reservasi_tanggal_pasien_gc_head',$data);
		$this->output->set_output(json_encode($hasil));
	}
	function cek_duplicate(){
		$reservasi_tipe_id='1';
		$jadwal_id=$this->input->post('jadwal_id');
		$tanggal=$this->input->post('tanggal');
		$iddokter=$this->input->post('iddokter');
		$idpasien=$this->input->post('idpasien');
		$idpoli=$this->input->post('idpoli');
		$q="SELECT * FROM `app_reservasi_tanggal_pasien` H
				WHERE H.reservasi_tipe_id='1' AND H.iddokter='$iddokter' AND H.jadwal_id='$jadwal_id' 
				AND H.idpoli='$idpoli' AND H.idpasien='$idpasien' AND H.tanggal='$tanggal' AND H.status_reservasi != 0";
		// print_r($q);exit;
		$data=$this->db->query($q)->row_array();
		// if ($data){
		$this->output->set_output(json_encode($data));
		// }
	}
	function cek_duplicate_rehab(){
		$reservasi_tipe_id='2';
		$jam_id=$this->input->post('jam_id');
		$tanggal=$this->input->post('tanggal');
		$idpasien=$this->input->post('idpasien');
		$idpoli=$this->input->post('idpoli');
		$q="SELECT * FROM `app_reservasi_tanggal_pasien` H
				WHERE H.reservasi_tipe_id='$reservasi_tipe_id' 
				AND H.idpoli='$idpoli' AND H.idpasien='$idpasien' AND H.tanggal='$tanggal' AND H.jam_id='$jam_id' AND H.status_reservasi != 0";
		// print_r($q);exit;
		$data=$this->db->query($q)->row_array();
		// if ($data){
		$this->output->set_output(json_encode($data));
		// }
	}
	public function select2_pasien()
    {
        $cari 	= $this->input->post('search');
        $data_obat = $this->Tbooking_model->select2_pasien($cari);
        $this->output->set_output(json_encode($data_obat));
    }
	function load_catatan(){
		$iddokter=$this->input->post('iddokter');
		  $tanggal=$this->input->post('tanggal');
		  $jadwal_id=$this->input->post('jadwal_id');
				$this->select = array();
				$from="
						(
							SELECT * FROM `app_reservasi_tanggal_catatan` H
							WHERE H.iddokter='$iddokter' AND H.tanggal='$tanggal' AND H.jadwal_id='$jadwal_id'
							ORDER BY H.edited_date DESC
						) as tbl
					";
				// print_r($from);exit();
				$this->from   = $from;
				$this->join 	= array();
				
				
				$this->order  = array();
				$this->group  = array();
				$this->column_search = array('catatan');
				$this->column_order  = array();

		  $list = $this->datatable->get_datatables(true);
		  $data = array();
		  $no = $_POST['start'];
		  foreach ($list as $r) {
			  $no++;
			  $result = array();

			  $result[] = $no;
			 
			  $result[] = $r->catatan;
			  $result[] = $r->edited_name.'<br>'.DMYTimeFormat($r->edited_date);
			 
			  $data[] = $result;
		  }
		  $output = array(
			  "draw" => $_POST['draw'],
			  "recordsTotal" => $this->datatable->count_all(true),
			  "recordsFiltered" => $this->datatable->count_all(true),
			  "data" => $data
		  );
		  echo json_encode($output);
	}
}
