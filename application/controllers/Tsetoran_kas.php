<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tsetoran_kas extends CI_Controller {

	/**
	 * Setoran Kas controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tsetoran_kas_model');
		$this->load->model('Mrka_kegiatan_model');
		$this->load->model('Mrka_bendahara_model');
		$this->load->helper('path');
		// print_r('sini');exit();
  }

	function index(){
		$data = array(
			'tanggal_setoran2'=>'',
			'tanggal_setoran1'=>'',
			'tanggal_trx1'=>'',
			'tanggal_trx2'=>'',
			'status'=>'#',
			'notransaksi'=>'',
		);
		// $data['list_unit'] 		= $this->Mrka_kegiatan_model->list_unit();
		// $data['list_jenis'] 		= $this->Tsetoran_kas_model->list_jenis_all();
		$data['error'] 			= '';
		$data['title'] 			= 'Setoran Kas';
		$data['content'] 		= 'Tsetoran_kas/index';
		$data['breadcrum'] 	= array(
														array("RSKB Halmahera",'#'),
														array("SETORAN KAS",'#'),
									    			array("List",'Tsetoran_kas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	function create($disabel=''){
		// $tanggal_trx=$this->input->post('tgl_trx');
		// $tanggal_setoran=$this->input->post('tanggal_setoran');
		$data = array(
			'id' 						=> '',			
			'nominal' 						=> '0',			
			'cash_piutang' 						=> '0',			
			'cash_retur' 						=> '0',			
			'cash_rajal' 						=> '0',			
			'cash_ranap' 						=> '0',			
			'cash_pendapatan' 						=> '0',			
			'tanggal_trx' 	=> date('d-m-Y'),
			// 'tanggal_trx' 	=> HumanDateShort('2021-01-12'),
			'tanggal_setoran' 	=> date('d-m-Y'),
			
		);


		$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
		$data['list_pembayaran'] 			= array();
		$data['disabel'] 			= $disabel;
		$data['error'] 			= '';
		$data['title'] 			= 'Tambah Pengajuan Kegiatan';
		$data['content'] 		= 'Tsetoran_kas/manage';
		$data['breadcrum']	= array(
														array("RSKB Halmahera",'#'),
														array("Setoran Kas",'#'),
									    			array("Tambah",'Tsetoran_kas')
													);

		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function load_awal_rajal(){
		$tanggal_trx=$this->input->post('tanggal_trx');
		$q="SELECT SUM(T.tunai) as tunai,SUM(T.debit) as debit ,SUM(T.kredit) as kredit,SUM(T.tf) as tf FROM (
				SELECT 
							SUM(CASE WHEN KB.idmetode='1' THEN KB.nominal ELSE 0 END)-SUM(K.kembalian) as tunai
							,SUM(CASE WHEN KB.idmetode='2' THEN KB.nominal ELSE 0 END) as debit
							,SUM(CASE WHEN KB.idmetode='3' THEN KB.nominal ELSE 0 END) as kredit
							,SUM(CASE WHEN KB.idmetode='4' THEN KB.nominal ELSE 0 END) as tf
							FROM tkasir K
							LEFT JOIN tkasir_pembayaran KB ON KB.idkasir=K.id
							WHERE K.`status`='2' AND DATE(K.tanggal)='".YMDFormat($tanggal_trx)."'
							GROUP BY  DATE(K.tanggal)		
				UNION ALL
				SELECT 
				SUM(CASE WHEN KB.idmetode='1' THEN KB.nominal ELSE 0 END) as tunai
							,SUM(CASE WHEN KB.idmetode='2' THEN KB.nominal ELSE 0 END) as debit
							,SUM(CASE WHEN KB.idmetode='3' THEN KB.nominal ELSE 0 END) as kredit
							,SUM(CASE WHEN KB.idmetode='4' THEN KB.nominal ELSE 0 END) as tf
				FROM trm_pengajuan_informasi H
				LEFT JOIN trm_pembayaran_informasi_detail KB ON KB.idpengajuan=H.id
				WHERE H.tanggal='".YMDFormat($tanggal_trx)."' AND H.status_pembayaran='2'
				) T";
		$arr=$this->db->query($q)->row_array();
		
		$q="SELECT SUM(D.nominal_bayar) as total from tbendahara_pendapatan H
			LEFT JOIN tbendahara_pendapatan_detail D ON D.idtransaksi=H.id
			WHERE DATE(H.tanggal)='".YMDFormat($tanggal_trx)."' AND H.`status`='1' AND D.idmetode='2'";//SECARA TUNAI
		$arr['total_pendapatan_lain']=$this->db->query($q)->row('total');
		
		$q="SELECT SUM(B.nominal_bayar) as total
					FROM tretur_penerimaan H 
					LEFT JOIN tretur_penerimaan_pembayaran B ON B.idretur=H.id
					WHERE H.`status`='2' AND H.status_penerimaan = ('1') AND B.tanggal_pencairan='".YMDFormat($tanggal_trx)."'";
		
		$arr['total_retur']=$this->db->query($q)->row('total');
		
		$q="SELECT SUM(B.nominal_bayar) as total
					FROM tretur_penerimaan H 
					LEFT JOIN tretur_penerimaan_pembayaran B ON B.idretur=H.id
					WHERE H.`status`='2' AND H.status_penerimaan = ('1') AND B.tanggal_pencairan='".YMDFormat($tanggal_trx)."'";
		
		$arr['total_retur']=$this->db->query($q)->row('total');
		
		$q="SELECT SUM(B.nominal_bayar) as total FROM `tpengelolaan` H
			LEFT JOIN tpengelolaan_bayar_terima B ON B.tpengelolaan_id=H.id
			WHERE H.st_verifikasi='1' AND H.total_terima > 0 AND B.tanggal_pencairan='".YMDFormat($tanggal_trx)."'";
		
		$arr['total_piutang']=$this->db->query($q)->row('total');
		$this->output->set_output(json_encode($arr));
	}
	function load_awal_ranap(){
		$tanggal_trx=YMDFormat($this->input->post('tanggal_trx'));
		// $q="SELECT SUM(CASE WHEN D.idmetode='1' THEN D.nominal ELSE 0 END) as tunai
			// ,SUM(CASE WHEN D.idmetode='2' THEN D.nominal ELSE 0 END) as debit
			// ,SUM(CASE WHEN D.idmetode='3' THEN D.nominal ELSE 0 END) as kredit
			// ,SUM(CASE WHEN D.idmetode='4' THEN D.nominal ELSE 0 END) as tf
			// FROM 
			// trawatinap_tindakan_pembayaran H
			// LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON D.idtindakan=H.id
			// LEFT JOIN trawatinap_pendaftaran P ON P.id=H.idtindakan

			// WHERE DATE(H.tanggal)='$tanggal_trx' AND H.statusbatal='0'
			// GROUP BY DATE(H.tanggal)";
		// $data1=$this->db->query($q)->row_array();
		// $q="SELECT 
			 // SUM(CASE WHEN H.idmetodepembayaran='1' THEN H.nominal ELSE 0 END) as dep_tunai
						// ,SUM(CASE WHEN H.idmetodepembayaran='2' THEN H.nominal ELSE 0 END) as dep_debit
						// ,SUM(CASE WHEN H.idmetodepembayaran='3' THEN H.nominal ELSE 0 END) as dep_kredit
						// ,SUM(CASE WHEN H.idmetodepembayaran='4' THEN H.nominal ELSE 0 END) as dep_tf
			// FROM trawatinap_deposit H
			// LEFT JOIN trawatinap_pendaftaran P ON P.id=H.idrawatinap
			// WHERE H.tanggal='$tanggal_trx' AND H.`status`='1'";
		// $data=$this->db->query($q)->row_array();
		// $arr['dep_tunai']=$data['dep_tunai'];
		// $arr['dep_debit']=$data['dep_debit'];
		// $arr['dep_kredit']=$data['dep_kredit'];
		// $arr['dep_tf']=$data['dep_tf'];
		$q="SELECT IFNULL(SUM(T.tunai),0) as tunai,IFNULL(SUM(T.debit),0) as debit,IFNULL(SUM(T.kredit),0) as kredit,IFNULL(SUM(T.tf),0) as tf FROM (
				SELECT SUM(CASE WHEN D.idmetode='1' THEN D.nominal ELSE 0 END) as tunai
							,SUM(CASE WHEN D.idmetode='2' THEN D.nominal ELSE 0 END) as debit
							,SUM(CASE WHEN D.idmetode='3' THEN D.nominal ELSE 0 END) as kredit
							,SUM(CASE WHEN D.idmetode='4' THEN D.nominal ELSE 0 END) as tf
							FROM 
							trawatinap_tindakan_pembayaran H
							LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON D.idtindakan=H.id
							LEFT JOIN trawatinap_pendaftaran P ON P.id=H.idtindakan

							WHERE DATE(H.tanggal)='$tanggal_trx' AND H.statusbatal='0'
							GROUP BY DATE(H.tanggal)
							
							UNION ALL
							
							SELECT 
							 SUM(CASE WHEN H.idmetodepembayaran='1' THEN H.nominal ELSE 0 END) as tunai
										,SUM(CASE WHEN H.idmetodepembayaran='2' THEN H.nominal ELSE 0 END) as debit
										,SUM(CASE WHEN H.idmetodepembayaran='3' THEN H.nominal ELSE 0 END) as kredit
										,SUM(CASE WHEN H.idmetodepembayaran='4' THEN H.nominal ELSE 0 END) as tf
							FROM trawatinap_deposit H
							LEFT JOIN trawatinap_pendaftaran P ON P.id=H.idrawatinap
							WHERE H.tanggal='$tanggal_trx' AND H.`status`='1'
							) T";
			
		$arr=$this->db->query($q)->row_array();
		$totalrefund=$this->db->query("SELECT IFNULL(SUM(T.totalrefund),0) as totalrefund FROM trefund T	WHERE T.`status`='1' AND T.tipe='0' AND DATE(T.tanggal)='$tanggal_trx' AND T.metode='1'")->row('totalrefund');
		$arr['tunai']=$arr['tunai']-$totalrefund;
		$this->output->set_output(json_encode($arr));
	}
	function update($id,$disabel=''){
		
			$data = $this->Tsetoran_kas_model->getSpecified($id);
			$data['rj_tunai_real']=($data['cash_rajal']);
			$data['ranap_tunai_real']=($data['cash_ranap']);
			// print_r($data);exit();
			$data['cash_pendapatan']=($data['cash_pendapatan']);
			$data['tanggal_setoran']=HumanDateShort($data['tanggal_setoran']);
			$data['tanggal_trx']=HumanDateShort($data['tanggal_trx']);
			
			$data['list_jenis_kas'] 	= $this->Mrka_bendahara_model->list_jenis_kas();
			$data['list_pembayaran'] 		= $this->Tsetoran_kas_model->list_pembayaran($id);
			
			$data['disabel'] 			= $disabel;
			$data['error'] 			= '';
			$data['title'] 			= 'Tambah Pengajuan Kegiatan';
			$data['content'] 		= 'Tsetoran_kas/manage';
			$data['breadcrum']	= array(
															array("RSKB Halmahera",'#'),
															array("Setoran Kas",'#'),
														array("Tambah",'Tsetoran_kas')
														);

			$data = array_merge($data, backend_info());
			$this->parser->parse('module_template', $data);
		
	}

	function delete($id){
		$this->Tsetoran_kas_model->softDelete($id);
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','data telah terhapus.');
		redirect('Tsetoran_kas','location');
	}
	function update_status($id,$status){
		$result=$this->Tsetoran_kas_model->update_status($id,$status);
		echo json_encode($result);
	}
	function cek_sudah_ada(){
		$tanggal_trx=YMDFormat($this->input->post('tanggal_trx'));
		$q="SELECT id from tsetoran_kas H WHERE H.tanggal_trx='$tanggal_trx' AND H.`status`='1'";
		$id=$this->db->query($q)->row('id');
		if ($id==null){
			$id=0;
		}
		$result=$id;
		echo json_encode($result);
	}
	function simpan_proses_peretujuan($id){
		
		$result=$this->Tsetoran_kas_model->simpan_proses_peretujuan($id);
		echo json_encode($result);
	}

	function save(){
		// print_r($this->input->post());exit();
		if($this->input->post('id') == '' ) {
			$id=$this->Tsetoran_kas_model->saveData();
			if($id){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tsetoran_kas','location');
			}
		} else {
			if($this->Tsetoran_kas_model->updateData()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','data telah disimpan.');
				redirect('tsetoran_kas','location');
			}
		}

	}
	
	

	function getIndex()
    {
	  $data_user=get_acces();
	  $user_acces_form=$data_user['user_acces_form'];
	
	  $notransaksi=$this->input->post('notransaksi');
	  $tanggal_setoran2=$this->input->post('tanggal_setoran2');
	  $tanggal_setoran1=$this->input->post('tanggal_setoran1');
	  $tanggal_trx1=$this->input->post('tanggal_trx1');
	  $tanggal_trx2=$this->input->post('tanggal_trx2');
	  $status=$this->input->post('status');
	 
	  $iduser=$this->session->userdata('user_id');
	  $where='';
	  if ($notransaksi !=''){
		  $where .=" AND H.notransaksi='$notransaksi'";
	  }
	  if ($status !='#'){
		  if ($status=='1'){
			  $where .=" AND H.st_verifikasi='1'";
		  }
		  if ($status=='2'){
			  $where .=" AND H.st_verifikasi='0'";
		  }
		  if ($status=='3'){
			  $where .=" AND H.status='0'";
		  }
		 
	  }
	   if ($tanggal_trx1 !='' || $tanggal_trx2 !=''){
		  $where .=" AND (H.tanggal_trx >='".YMDFormat($tanggal_trx1)."' AND H.tanggal_trx <='".YMDFormat($tanggal_trx2)."' )";
	  }
	  if ($tanggal_setoran1 !='' || $tanggal_setoran2 !=''){
		  $where .=" AND (H.tanggal_setoran >='".YMDFormat($tanggal_setoran1)."' AND H.tanggal_setoran <='".YMDFormat($tanggal_setoran2)."' )";
	  }
	 
	  $from="(
				SELECT *FROM tsetoran_kas H  WHERE H.id is not null ".$where."
				ORDER BY H.tanggal_trx DESC
				) as tbl";
	// print_r($tanggal_setoran1);exit();
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('nama_kegiatan','no_pengajuan');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
            $no++;
            $row = array();

            $action = '';
            $status = '';
			if ($r->status=='1'){
				$action .= '<a href="'.site_url().'tsetoran_kas/update/'.$r->id.'/disabled" data-toggle="tooltip" title="Lihat" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>';
			
				if ($r->st_verifikasi=='0'){
					$action .= '<button class="btn btn-primary btn-sm verif"><i class="fa fa-check"></i></button>';
					$action .= '<a href="'.site_url().'tsetoran_kas/update/'.$r->id.'" data-toggle="tooltip" title="Ubah" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>';
					$action .= '<button class="btn btn-danger btn-sm hapus"><i class="fa fa-trash"></i></button>';
					$status='<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">BELUM DIVERIFIKASI</span>';
				}else{
					$status='<span class="label label-success" data-toggle="tooltip" title="DIBATALKAN">SUDAH DIVERIFIKASI</span>';
				}
				$action .= '<a href="'.site_url().'tsetoran_kas/kwitansi/'.$r->id.'" target="_blank" data-toggle="tooltip" title="Cetak Kwitansi" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>';
			}else{
				$action='<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">DIBATALKAN</span>';
				$status='<span class="label label-danger" data-toggle="tooltip" title="DIBATALKAN">DIBATALKAN</span>';
			}
  					
            $row[] = $r->id;
            $row[] = $no;
  			$row[] = $r->notransaksi;
  			$row[] = HumanDateShort($r->tanggal_trx);
  			$row[] = HumanDateShort($r->tanggal_setoran);
            $row[] = number_format($r->nominal);
            $row[] = $status;
            $row[] = '<div class="btn-group">'.$action.'</div>';
            $data[] = $row;
        }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function verif($id){
		// $id=$this->input->post('id');
		$result=$this->db->query("UPDATE tsetoran_kas set st_verifikasi='1' WHERE id='$id'");
		$this->Tsetoran_kas_model->insert_jurnal_setoran($id);
		echo json_encode($result);
	}
	function hapus($id){
		// $id=$this->input->post('idtsetoran_kas
		$result=$this->db->query("UPDATE tsetoran_kas set status='0' WHERE id='$id'");
		echo json_encode($result);
	}
  function get_rajal_detail()
  {
	 
	  $tanggal_trx=YMDFormat($this->input->post('tanggal_trx'));
	  
	 
	  $from="(
				SELECT K.id,K.tanggal,K.idtipe, SUM(IFNULL(K.kembalian,0)) as kembalian,CASE WHEN K.idtipe='1' THEN 'POLIKLINIK' 
				  WHEN K.idtipe='2' THEN 'IGD' 
				  WHEN K.idtipe='3' THEN 'OBAT BEBAS' END tipe_nama 
					,SUM(KB.nominal) as total
					from tkasir K
				LEFT JOIN tkasir_pembayaran KB ON KB.idkasir=K.id
				WHERE K.`status`='2' AND DATE(K.tanggal)='$tanggal_trx' AND KB.idmetode='1'
				GROUP BY K.idtipe
				
				UNION ALL

				SELECT H.id,H.tanggal,'99' as idtipe,0 as kembalian,'INFORMASI MEDIS' as tipe_nama
				,SUM(CASE WHEN KB.idmetode='1' THEN KB.nominal ELSE 0 END) as total			
				FROM trm_pengajuan_informasi H
				LEFT JOIN trm_pembayaran_informasi_detail KB ON KB.idpengajuan=H.id
				WHERE H.tanggal='$tanggal_trx' AND H.status_pembayaran='2'
				GROUP BY H.tanggal
				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $total=0;
	  $total2=0;
	  $kembalian=0;
      foreach ($list as $r) {
		$no++;
		$row = array();
			
		$aksi = '<div class="btn-group">';
		$aksi .= '	  <button class="btn btn-info btn-sm "  type="button" ><i class="fa fa-list-ul"></i></button>';
		$aksi .= '	  <button class="btn btn-success btn-sm "  type="button" ><i class="fa fa-print"></i></button>';
		$aksi .= '</div>';
			  
		$row[] = $r->tipe_nama;
		$row[] = number_format($r->total,0);
		$total=$total + $r->total;
		$total2=$total2 + $r->total;
		// $kembalian=$kembalian + $r->kembalian;
		$row[] = number_format($r->total,0);
		$row[] = $aksi;

		$data[] = $row;
      }
	  $q="SELECT SUM(H.kembalian) as kembalian FROM `tkasir` H WHERE DATE(H.tanggal)='$tanggal_trx' AND H.`status`='2'";
	  $kembalian=$this->db->query($q)->row('kembalian');
		$row = array();
		$row[] = '<strong>KEMBALIAN</strong>';
		$row[] = '<strong>-'.number_format($kembalian,0).'</strong>';
		$row[] = '<strong>-'.number_format($kembalian,0).'</strong>';
		$row[] ='';
		$data[] = $row;
		
		$row = array();
		$row[] = '<strong>TOTAL ALL</strong>';
		$row[] = '<strong>'.number_format($total-$kembalian,0).'</strong>';
		$row[] = '<strong>'.number_format($total2-$kembalian,0).'</strong>';
		$row[] ='';

		$data[] = $row;
	  
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function get_ranap_detail(){
	 $tanggal_trx=YMDFormat($this->input->post('tanggal_trx'));
	 $q="SELECT P.idtipe,D.id,
		H.tanggal,H.idtindakan,P.no_medrec,P.title,P.namapasien,D.nominal
		,P.id as idpendaftaran
		FROM 
		trawatinap_tindakan_pembayaran H
		LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON D.idtindakan=H.id
		LEFT JOIN trawatinap_pendaftaran P ON P.id=H.idtindakan

		WHERE DATE(H.tanggal)='$tanggal_trx' AND D.idmetode='1' AND H.statusbatal='0' AND P.idtipe='1'

		ORDER BY P.idtipe,P.idpasien"; 
		
		
		
		$rows=$this->db->query($q)->result();
		$tabel='';
		$total=0;
		$total_all=0;
		foreach($rows as $r){
			$deskripsi='';
			$tabel .='<tr>';
			if ($r->idtipe=='1'){
				$deskripsi='RAWAT INAP PELUNASAN';
			}else{
				$deskripsi='ODS PELUNASAN';
			}
			$aksi = '<div class="btn-group">';
			$aksi .= '	  <a href="'.site_url().'trawatinap_tindakan/verifikasi/'.$r->idpendaftaran.'" target="_blank" class="btn btn-info btn-sm " type="button"><i class="fa fa-list-ul"></i></a>';
			$aksi .= '	  <button class="btn btn-success btn-sm "  type="button" ><i class="fa fa-print"></i></button>';
			$aksi .= '</div>';
			
			$tabel .='<td class="text-center">'.$deskripsi.'</td>';
			$tabel .='<td class="text-center">'.$r->no_medrec.' - '.$r->title.' '.$r->namapasien.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
			$tabel .='<td>'.$aksi.'</td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
			$total=$total + $r->nominal;
			$total_all=$total_all + $r->nominal;
		}
		if ($rows){
		$tabel .='<tr>';
		$tabel .='<td class="text-center" colspan="4"><strong>TOTAL RAWAT INAP</strong></td>';
		$tabel .='<td class="text-right" ><strong>'.number_format($total).'</strong></td>';
		$tabel .='<td class="text-center" ></td>';
		$tabel .='</tr>';
		}
		$q="SELECT P.idtipe,D.id,
		H.tanggal,H.idtindakan,P.no_medrec,P.title,P.namapasien,D.nominal
		FROM 
		trawatinap_tindakan_pembayaran H
		LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON D.idtindakan=H.id
		LEFT JOIN trawatinap_pendaftaran P ON P.id=H.idtindakan

		WHERE DATE(H.tanggal)='$tanggal_trx' AND D.idmetode='1' AND H.statusbatal='0'  AND P.idtipe='2'

		ORDER BY P.idtipe,P.idpasien"; 
		$rows=$this->db->query($q)->result();
		// $tabel='';
		$total=0;
		foreach($rows as $r){
			$deskripsi='';
			$tabel .='<tr>';
			if ($r->idtipe=='1'){
				$deskripsi='RAWAT INAP PELUNASAN';
			}else{
				$deskripsi='ODS PELUNASAN';
			}
			$tabel .='<td class="text-center">'.$deskripsi.'</td>';
			$tabel .='<td class="text-center">'.$r->no_medrec.' - '.$r->title.' '.$r->namapasien.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
				$aksi = '<div class="btn-group">';
				$aksi .= '	  <a href="'.site_url().'trawatinap_tindakan/print_kwitansi_deposit/'.$r->id.'"  type="button"  class="btn btn-success btn-sm "><i class="fa fa-print"></i></a>';
				$aksi .= '</div>';
				
			$tabel .='<td>'.$aksi.'</td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
			$total=$total + $r->nominal;
			$total_all=$total_all + $r->nominal;
		}
		if ($rows){
		$tabel .='<tr>';
		$tabel .='<td class="text-center" colspan="4"><strong>TOTAL ODS</strong></td>';
		$tabel .='<td class="text-right" ><strong>'.number_format($total).'</strong></td>';
		$tabel .='<td class="text-center" ></td>';
		$tabel .='</tr>';
		}
		$q="SELECT 
			H.id,H.tanggal,P.no_medrec,P.title, P.namapasien,H.nominal,H.idrawatinap
			FROM trawatinap_deposit H
			LEFT JOIN trawatinap_pendaftaran P ON P.id=H.idrawatinap
			WHERE H.tanggal='$tanggal_trx' AND H.`status`='1' AND H.idmetodepembayaran='1'"; 
		$rows=$this->db->query($q)->result();
		// $tabel='';
		$total=0;
		foreach($rows as $r){
			$deskripsi='';
			$tabel .='<tr>';
				$deskripsi='DEPOSIT';
			$tabel .='<td class="text-center">'.$deskripsi.'</td>';
			$tabel .='<td class="text-center">'.$r->no_medrec.' - '.$r->title.' '.$r->namapasien.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
				$aksi = '<div class="btn-group">';
				$aksi .= '<button onclick="getHistoryDeposit('.$r->idrawatinap.')" type="button" data-toggle="tooltip" title="DEPOSIT" class="btn btn-primary btn-sm"><i class="fa fa-credit-card-alt"></i></button>';
				$aksi .= '<a href="'.site_url().'trawatinap_tindakan/print_kwitansi_deposit/'.$r->id.'" target="_blank" type="button" class="btn btn-success btn-sm "><i class="fa fa-print"></i></a>';
				$aksi .= '</div>';
			$tabel .='<td>'.$aksi.'</td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
			$total=$total + $r->nominal;
			$total_all=$total_all + $r->nominal;
		}
		if ($rows){
		$tabel .='<tr>';
		$tabel .='<td class="text-center" colspan="4"><strong>TOTAL DEPOSIT</strong></td>';
		$tabel .='<td class="text-right" ><strong>'.number_format($total).'</strong></td>';
		$tabel .='<td class="text-center" ></td>';
		$tabel .='</tr>';
		}
		$arr['detail'] = $tabel;
		$arr['tot_tunai_ranap'] = $total_all;
		// $arr['nominal_bayar'] = $total;
		$this->output->set_output(json_encode($arr));
  }
  // function get_ranap_detail()
  // {
	 
	  // $tanggal_trx=YMDFormat($this->input->post('tanggal_trx'));
	  
	 
	  // $from="(
				// SELECT K.id,K.tanggal,K.idtipe,CASE WHEN K.idtipe='1' THEN 'POLIKLINIK' 
				  // WHEN K.idtipe='2' THEN 'IGD' 
				  // WHEN K.idtipe='3' THEN 'OBAT BEBAS' END tipe_nama 
					// ,SUM(KB.nominal) as total
					// from tkasir K
				// LEFT JOIN tkasir_pembayaran KB ON KB.idkasir=K.id
				// WHERE K.`status`='2' AND DATE(K.tanggal)='$tanggal_trx' AND KB.idmetode='1'
				// GROUP BY K.idtipe
				// ) as tbl";
			// $this->select = array();
			// $this->from   = $from;
			// $this->join 	= array();
			// $this->where  = array();
			// $this->order  = array();
			// $this->group  = array();

      // $this->column_search   = array();
      // $this->column_order    = array();

      // $list = $this->datatable->get_datatables();
      // $data = array();
      // $no = $_POST['start'];
	  // $total=0;
	  // $total2=0;
      // foreach ($list as $r) {
		// $no++;
		// $row = array();
			
		// $aksi = '<div class="btn-group">';
		// $aksi .= '	  <button class="btn btn-info btn-sm "><i class="fa fa-list-ul"></i></button>';
		// $aksi .= '	  <button class="btn btn-success btn-sm "><i class="fa fa-print"></i></button>';
		// $aksi .= '</div>';
			  
		// $row[] = $r->tipe_nama;
		// $row[] = number_format($r->total,0);
		// $total=$total + $r->total;
		// $total2=$total2 + $r->total;
		// $row[] = number_format($r->total,0);
		// $row[] = $aksi;

		// $data[] = $row;
      // }
		// $row = array();
		
		// $row[] = '<strong>TOTAL ALL</strong>';
		// $row[] = '<strong>'.number_format($total,0).'</strong>';
		// $row[] = '<strong>'.number_format($total2,0).'</strong>';
		// $row[] ='';

		// $data[] = $row;
	  
      // $output = array(
	      // "draw" => $_POST['draw'],
	      // "recordsTotal" => $this->datatable->count_all(),
	      // "recordsFiltered" => $this->datatable->count_all(),
	      // "data" => $data
      // );
      // echo json_encode($output);
  // }
  
  function get_rajal_refund()
  {
	 //REFUND SECARA TUNAI
	  $tanggal_trx=YMDFormat($this->input->post('tanggal_trx'));
	  $from="(
				SELECT T.id,T.norefund,CASE WHEN T.tipe='0' THEN 'DEPOSIT' WHEN T.tipe='1' THEN 'OBAT' WHEN T.tipe='2' THEN 'TRANSAKSI' END as tipe_refund,T.tipe,T.tipetransaksi
				,M.no_medrec,M.nama as nama_pasien,T.totalrefund
				 FROM trefund T
				 LEFT JOIN mfpasien M ON M.id=T.idpasien
				WHERE T.`status`='1' AND T.tipe='2' AND T.tipetransaksi='rawatjalan' AND DATE(T.tanggal)='".$tanggal_trx."' AND T.metode='1'

				UNION ALL

				SELECT T.id,T.norefund,CASE WHEN T.tipe='0' THEN 'DEPOSIT' WHEN T.tipe='1' THEN 'OBAT' WHEN T.tipe='2' THEN 'TRANSAKSI' END as tipe_refund,T.tipe,T.tipetransaksi
				,M.no_medrec,M.nama as nama_pasien,T.totalrefund
				 FROM trefund T
				 LEFT JOIN mfpasien M ON M.id=T.idpasien
				WHERE T.`status`='1' AND T.tipe='1' AND DATE(T.tanggal)='".$tanggal_trx."' AND T.metode='1'

				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $total=0;
      foreach ($list as $r) {
		$no++;
		$row = array();
			
		$aksi = '<div class="btn-group">';
		if ($r->tipe=='1'){//OBAT
			$aksi .= '	  <a href="'.site_url().'Trefund/fakturRefundObat/'.$r->id.'" target="_blank" type="button" class="btn btn-success btn-sm "><i class="fa fa-print"></i></a>';
		}elseif($r->tipe='2'){//TRANSAKSI
			$aksi .= '	  <a href="'.site_url().'Trefund/fakturRefundTransaksi/'.$r->id.'" target="_blank" type="button" class="btn btn-success btn-sm "><i class="fa fa-print"></i></a>';
		}
		$aksi .= '</div>';
			  
		$row[] = $r->norefund;
		$row[] = $r->tipe_refund;
		$row[] = 'TUNAI';
		$row[] = $r->no_medrec;
		$row[] = $r->nama_pasien;
		$row[] = number_format($r->totalrefund,0);
		$total=$total + $r->totalrefund;
		$row[] = $aksi;

		$data[] = $row;
      }
		$row = array();
		
		$row[] = '<strong>TOTAL ALL</strong>';
		$row[] = '';
		$row[] = '';
		$row[] = '';
		$row[] = '';
		$row[] = '<strong>'.number_format($total,0).'</strong>';
		$row[] ='';

		$data[] = $row;
	  
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function get_ranap_refund()
  {
	 //REFUND SECARA TUNAI
	  $tanggal_trx=YMDFormat($this->input->post('tanggal_trx'));
	  $from="(
				SELECT T.id,T.norefund,CASE WHEN T.tipe='0' THEN 'DEPOSIT' WHEN T.tipe='1' THEN 'OBAT' WHEN T.tipe='2' THEN 'TRANSAKSI' END as tipe_refund,T.tipe,T.tipetransaksi
				,M.no_medrec,M.nama as nama_pasien,T.totalrefund
				 FROM trefund T
				 LEFT JOIN mfpasien M ON M.id=T.idpasien
				WHERE T.`status`='1' AND T.tipe='2' AND T.tipetransaksi='rawatinap' AND DATE(T.tanggal)='".$tanggal_trx."' AND T.metode='1'

				UNION ALL

				SELECT T.id,T.norefund,CASE WHEN T.tipe='0' THEN 'DEPOSIT' WHEN T.tipe='1' THEN 'OBAT' WHEN T.tipe='2' THEN 'TRANSAKSI' END as tipe_refund,T.tipe,T.tipetransaksi
				,M.no_medrec,M.nama as nama_pasien,T.totalrefund
				 FROM trefund T
				 LEFT JOIN mfpasien M ON M.id=T.idpasien
				WHERE T.`status`='1' AND T.tipe='1' AND DATE(T.tanggal)='".$tanggal_trx."' AND T.metode='1'
				
				UNION ALL

				SELECT T.id,T.norefund,CASE WHEN T.tipe='0' THEN 'DEPOSIT' WHEN T.tipe='1' THEN 'OBAT' WHEN T.tipe='2' THEN 'TRANSAKSI' END as tipe_refund,T.tipe,T.tipetransaksi
				,M.no_medrec,M.nama as nama_pasien,T.totalrefund
				 FROM trefund T
				 LEFT JOIN mfpasien M ON M.id=T.idpasien
				WHERE T.`status`='1' AND T.tipe='0' AND DATE(T.tanggal)='".$tanggal_trx."' AND T.metode='1'

				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $total=0;
      foreach ($list as $r) {
		$no++;
		$row = array();
			
		$aksi = '<div class="btn-group">';
		if ($r->tipe=='1'){//OBAT
			$aksi .= '	  <a href="'.site_url().'Trefund/fakturRefundObat/'.$r->id.'" target="_blank" type="button" class="btn btn-success btn-sm "><i class="fa fa-print"></i></a>';
		}elseif($r->tipe='2'){//TRANSAKSI
			$aksi .= '	  <a href="'.site_url().'Trefund/fakturRefundTransaksi/'.$r->id.'" target="_blank" type="button" class="btn btn-success btn-sm "><i class="fa fa-print"></i></a>';
		}
		$aksi .= '</div>';
			  
		$row[] = $r->norefund;
		$row[] = $r->tipe_refund;
		$row[] = 'TUNAI';
		$row[] = $r->no_medrec;
		$row[] = $r->nama_pasien;
		$row[] = number_format($r->totalrefund,0);
		$total=$total + $r->totalrefund;
		$row[] = $aksi;

		$data[] = $row;
      }
		$row = array();
		
		$row[] = '<strong>TOTAL ALL</strong>';
		$row[] = '';
		$row[] = '';
		$row[] = '';
		$row[] = '';
		$row[] = '<strong>'.number_format($total,0).'</strong>';
		$row[] ='';

		$data[] = $row;
	  
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_pendapatan_lain()
  {
	 //REFUND SECARA TUNAI
	  $tanggal_trx=YMDFormat($this->input->post('tanggal_trx'));
	  $from="(
				SELECT H.id,H.notransaksi,H.tanggal,M.nama as dari,MP.keterangan as pendapatan,D.idmetode,D.nominal_bayar from tbendahara_pendapatan H
				LEFT JOIN tbendahara_pendapatan_detail D ON D.idtransaksi=H.id
				LEFT JOIN mdari M ON M.id=H.terimadari
				LEFT JOIN mpendapatan MP ON MP.id=H.idpendapatan
				WHERE DATE(H.tanggal)='$tanggal_trx' AND H.`status`='1'
				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $total=0;
      foreach ($list as $r) {
		$no++;
		$row = array();
			
		$aksi = '<div class="btn-group">';
		// if ($r->tipe=='1'){//OBAT
			// $aksi .= '	  <a href="'.site_url().'tpendapatan/update/'.$r->id.'" target="_blank" type="button" class="btn btn-success btn-sm "><i class="fa fa-print"></i></a>';
			$aksi .= '	  <a href="'.site_url().'tpendapatan/update/'.$r->id.'/disabled" target="_blank" type="button" class="btn btn-default btn-sm "><i class="fa fa-eye"></i></a>';
		// }elseif($r->tipe='2'){//TRANSAKSI
			// $aksi .= '	  <a href="'.site_url().'Trefund/fakturRefundTransaksi/'.$r->id.'" target="_blank" type="button" class="btn btn-success btn-sm "><i class="fa fa-print"></i></a>';
		// }
		$aksi .= '</div>';
			  
		$row[] = $r->id;
		$row[] = '';
		$row[] = $r->notransaksi;
		$row[] =HumanDateShort($r->tanggal);
		$row[] = $r->pendapatan;
		$row[] = $r->dari;
		$row[] = number_format($r->nominal_bayar,0);
		$row[] = cara_bayar2($r->idmetode);
		$row[] = $aksi;

		$data[] = $row;
      }
		
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_detail_pasien_rajal()
  {
	 //REFUND SECARA TUNAI
	  $cara_bayar=($this->input->post('cara_bayar'));
	  $tanggal_trx=YMDFormat($this->input->post('tanggal_trx'));
	  $from="(
				SELECT B.id,K.tanggal,K.idtindakan,K.idtipe
				,CASE WHEN K.idtipe='3' THEN FR.nopenjualan ELSE TP.nopendaftaran END as notrx
				,CASE WHEN K.idtipe='3' THEN FR.nomedrec ELSE TP.no_medrec END as nomedrec
				,CASE WHEN K.idtipe='3' THEN FR.nama ELSE CONCAT(TP.title,' ',TP.namapasien) END as namapasien
				,K.total,B.nominal,B.idmetode,mbank.nama as nama_bank
				from tkasir K
				LEFT JOIN  tkasir_pembayaran B ON B.idkasir=K.id
				LEFT JOIN mbank ON mbank.id=B.idbank
				LEFT JOIN tpasien_penjualan FR ON FR.id=K.idtindakan AND K.idtipe='3'
				LEFT JOIN tpoliklinik_tindakan PT ON PT.id=K.idtindakan AND K.idtipe!='3'
				LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=PT.idpendaftaran
				WHERE K.`status` !='0' AND B.idmetode='$cara_bayar' AND DATE(K.tanggal)='$tanggal_trx'
				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('notrx','namapasien','nama_bank');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $total=0;
      foreach ($list as $r) {
		$no++;
		$row = array();
			
		$aksi = '<div class="btn-group">';
			$aksi .= '	  <a href="'.site_url().'tpendapatan/update/'.$r->id.'/disabled" target="_blank" type="button" class="btn btn-default btn-sm "><i class="fa fa-eye"></i></a>';
		$aksi .= '</div>';
			  
		$row[] = $r->id;
		$row[] = $r->notrx;
		$row[] = $r->nomedrec;
		$row[] = $r->namapasien;
		$row[] = JenisTransaksi($r->idtipe);
		$row[] = number_format($r->total,0);
		$row[] = number_format($r->nominal,0);
		$row[] = metodePembayaran($r->idmetode).'<br>'.$r->nama_bank;
		$row[] = $aksi;
		$data[] = $row;
      }
		
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function load_detail_pasien_rajal_bank()
  {
	 //REFUND SECARA TUNAI
	  $idmetode=($this->input->post('idmetode'));
	  $idtipe=($this->input->post('idtipe'));
	  $idbank=($this->input->post('idbank'));
	  $tanggal_trx=YMDFormat($this->input->post('tanggal_trx'));
	  $from="(
				SELECT B.id,K.tanggal,K.idtindakan,K.idtipe
				,CASE WHEN K.idtipe='3' THEN FR.nopenjualan ELSE TP.nopendaftaran END as notrx
				,CASE WHEN K.idtipe='3' THEN FR.nomedrec ELSE TP.no_medrec END as nomedrec
				,CASE WHEN K.idtipe='3' THEN FR.nama ELSE CONCAT(TP.title,' ',TP.namapasien) END as namapasien
				,K.total,B.nominal,B.idmetode,mbank.nama as nama_bank
				from tkasir K
				LEFT JOIN  tkasir_pembayaran B ON B.idkasir=K.id
				LEFT JOIN mbank ON mbank.id=B.idbank
				LEFT JOIN tpasien_penjualan FR ON FR.id=K.idtindakan AND K.idtipe='3'
				LEFT JOIN tpoliklinik_tindakan PT ON PT.id=K.idtindakan AND K.idtipe!='3'
				LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=PT.idpendaftaran
				WHERE K.`status` !='0' AND B.idmetode='$idmetode' AND DATE(K.tanggal)='$tanggal_trx' AND K.idtipe='$idtipe' AND B.idbank='$idbank'
				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array('notrx','namapasien');
      $this->column_order    = array();

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
	  $total=0;
      foreach ($list as $r) {
		$no++;
		$row = array();
			
		$aksi = '<div class="btn-group">';
			// $aksi .= '	  <a href="'.site_url().'tpendapatan/update/'.$r->id.'/disabled" target="_blank" type="button" class="btn btn-default btn-sm "><i class="fa fa-eye"></i></a>';
		$aksi .= '</div>';
			  
		$row[] = $r->id;
		$row[] = $r->notrx;
		$row[] = $r->nomedrec;
		$row[] = $r->namapasien;
		$row[] = JenisTransaksi($r->idtipe);
		$row[] = number_format($r->total,0);
		$row[] = number_format($r->nominal,0);
		$row[] = metodePembayaran($r->idmetode).'<br>'.$r->nama_bank;
		// $row[] = $aksi;
		$data[] = $row;
      }
		
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function get_rajal_detail_non_tunai()
  {
	  $idmetode=$this->input->post('idmetode');
	  $tanggal_trx=YMDFormat($this->input->post('tanggal_trx'));	 
	  $from="(
				SELECT K.id,K.tanggal,K.idtipe,CASE WHEN K.idtipe='1' THEN 'POLIKLINIK' 
				  WHEN K.idtipe='2' THEN 'IGD' 
				  WHEN K.idtipe='3' THEN 'OBAT BEBAS' END tipe_nama 
					,SUM(KB.nominal) as total,M.nama as nama_bank,KB.idbank,KB.idmetode
					from tkasir K
				LEFT JOIN tkasir_pembayaran KB ON KB.idkasir=K.id
				LEFT JOIN mbank M ON KB.idbank=M.id
				WHERE K.`status`='2' AND DATE(K.tanggal)='".YMDFormat($tanggal_trx)."' AND KB.idmetode='$idmetode'
				GROUP BY K.idtipe,KB.idbank
				
				UNION ALL
				SELECT H.id,H.tanggal,'1' as idtipe,'INFORMASI MEDIS' as tipe_nama
				,SUM(KB.nominal) as total,M.nama as nama_bank,KB.idbank,KB.idmetode			
				FROM trm_pengajuan_informasi H
				LEFT JOIN trm_pembayaran_informasi_detail KB ON KB.idpengajuan=H.id
				LEFT JOIN mbank M ON KB.idbank=M.id
				WHERE H.tanggal='".YMDFormat($tanggal_trx)."' AND H.status_pembayaran='2' AND KB.idmetode='$idmetode'
				GROUP BY H.tanggal
				) as tbl";
			$this->select = array();
			$this->from   = $from;
			$this->join 	= array();
			$this->where  = array();
			$this->order  = array();
			$this->group  = array();

      $this->column_search   = array();
      $this->column_order    = array();

      $list = $this->datatable->get_datatables(true);
      $data = array();
      $no = $_POST['start'];
	   $total=0;
	   $total2=0;
      foreach ($list as $r) {
          $no++;
          $row = array();

		$aksi = '<div class="btn-group">';
		$aksi .= '	  <button class="btn btn-info btn-sm" onclick="load_detail_pasien_rajal_bank('.$r->idtipe.','.$r->idbank.','.$r->idmetode.')" type="button"><i class="fa fa-list-ul"></i></button>';
		$aksi .= '	  <button class="btn btn-success btn-sm "  type="button"><i class="fa fa-print"></i></button>';
		$aksi .= '</div>';
		
		  $total=$total + $r->total;
		$total2=$total2 + $r->total;
          $row[] = $r->tipe_nama;
          $row[] = $r->nama_bank;
          $row[] = number_format($r->total,0);
          $row[] = number_format($r->total,0);
          $row[] = $aksi;

          $data[] = $row;
      }
	  $row = array();
		
		$row[] = '<strong>TOTAL ALL</strong>';
		$row[] = '';
		$row[] = '<strong>'.number_format($total,0).'</strong>';
		$row[] = '<strong>'.number_format($total2,0).'</strong>';
		$row[] = '';

		$data[] = $row;
		
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(true),
	      "recordsFiltered" => $this->datatable->count_all(true),
	      "data" => $data
      );
      echo json_encode($output);
  }
  function get_ranap_detail_non_tunai()
  {
	  $idmetode=$this->input->post('idmetode');
	  $tanggal_trx=YMDFormat($this->input->post('tanggal_trx'));	 
	 $q="SELECT P.idtipe,D.id,
		H.tanggal,H.idtindakan,P.no_medrec,P.title,P.namapasien,D.nominal,P.id as idpendaftaran
		FROM 
		trawatinap_tindakan_pembayaran H
		LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON D.idtindakan=H.id
		LEFT JOIN trawatinap_pendaftaran P ON P.id=H.idtindakan

		WHERE DATE(H.tanggal)='$tanggal_trx' AND D.idmetode='$idmetode' AND H.statusbatal='0' AND P.idtipe='1'

		ORDER BY P.idtipe,P.idpasien"; 
		
		
		
		$rows=$this->db->query($q)->result();
		$tabel='';
		$total=0;
		$total_all=0;
		foreach($rows as $r){
			$deskripsi='';
			$tabel .='<tr>';
			if ($r->idtipe=='1'){
				$deskripsi='RAWAT INAP PELUNASAN';
			}else{
				$deskripsi='ODS PELUNASAN';
			}
			$aksi = '<div class="btn-group">';
			$aksi .= '	  <a href="'.site_url().'trawatinap_tindakan/verifikasi/'.$r->idpendaftaran.'" target="_blank" class="btn btn-info btn-sm " type="button"><i class="fa fa-list-ul"></i></a>';
			$aksi .= '	  <button class="btn btn-success btn-sm " type="button"><i class="fa fa-print"></i></button>';
			$aksi .= '</div>';
			
			$tabel .='<td class="text-center">'.$deskripsi.'</td>';
			$tabel .='<td class="text-center">'.$r->no_medrec.' - '.$r->title.' '.$r->namapasien.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
			$tabel .='<td>'.$aksi.'</td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
			$total=$total + $r->nominal;
			$total_all=$total_all + $r->nominal;
		}
		if ($rows){
		$tabel .='<tr>';
		$tabel .='<td class="text-center" colspan="4"><strong>TOTAL RAWAT INAP</strong></td>';
		$tabel .='<td class="text-right" ><strong>'.number_format($total).'</strong></td>';
		$tabel .='<td class="text-center" ></td>';
		$tabel .='</tr>';
		}
		$q="SELECT P.idtipe,D.id,
		H.tanggal,H.idtindakan,P.no_medrec,P.title,P.namapasien,D.nominal
		FROM 
		trawatinap_tindakan_pembayaran H
		LEFT JOIN trawatinap_tindakan_pembayaran_detail D ON D.idtindakan=H.id
		LEFT JOIN trawatinap_pendaftaran P ON P.id=H.idtindakan

		WHERE DATE(H.tanggal)='$tanggal_trx' AND D.idmetode='$idmetode' AND H.statusbatal='0'  AND P.idtipe='2'

		ORDER BY P.idtipe,P.idpasien"; 
		$rows=$this->db->query($q)->result();
		// $tabel='';
		$total=0;
		foreach($rows as $r){
			$deskripsi='';
			$tabel .='<tr>';
			if ($r->idtipe=='1'){
				$deskripsi='RAWAT INAP PELUNASAN';
			}else{
				$deskripsi='ODS PELUNASAN';
			}
			$tabel .='<td class="text-center">'.$deskripsi.'</td>';
			$tabel .='<td class="text-center">'.$r->no_medrec.' - '.$r->title.' '.$r->namapasien.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
				$aksi = '<div class="btn-group">';
				$aksi .= '	  <a href="'.site_url().'trawatinap_tindakan/print_kwitansi_deposit/'.$r->id.'" target="_blank" class="btn btn-success btn-sm "><i class="fa fa-print"></i></a>';
				$aksi .= '</div>';
				
			$tabel .='<td>'.$aksi.'</td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
			$total=$total + $r->nominal;
			$total_all=$total_all + $r->nominal;
		}
		if ($rows){
		$tabel .='<tr>';
		$tabel .='<td class="text-center" colspan="4"><strong>TOTAL ODS</strong></td>';
		$tabel .='<td class="text-right" ><strong>'.number_format($total).'</strong></td>';
		$tabel .='<td class="text-center" ></td>';
		$tabel .='</tr>';
		}
		$q="SELECT 
			H.id,H.tanggal,P.no_medrec,P.title, P.namapasien,H.nominal
			FROM trawatinap_deposit H
			LEFT JOIN trawatinap_pendaftaran P ON P.id=H.idrawatinap
			WHERE H.tanggal='$tanggal_trx' AND H.`status`='1' AND H.idmetodepembayaran='$idmetode'"; 
		$rows=$this->db->query($q)->result();
		// $tabel='';
		$total=0;
		foreach($rows as $r){
			$deskripsi='';
			$tabel .='<tr>';
				$deskripsi='DEPOSIT';
			$tabel .='<td class="text-center">'.$deskripsi.'</td>';
			$tabel .='<td class="text-center">'.$r->no_medrec.' - '.$r->title.' '.$r->namapasien.'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
			$tabel .='<td class="text-right">'.number_format($r->nominal).'</td>';
				$aksi = '<div class="btn-group">';
				$aksi .= '	  <a href="'.site_url().'trawatinap_tindakan/print_kwitansi_deposit/'.$r->id.'" target="_blank" class="btn btn-success btn-sm "><i class="fa fa-print"></i></a>';
				$aksi .= '</div>';
			$tabel .='<td>'.$aksi.'</td>';
			$tabel .='<td class="text-left" style="display:none">'.$r->id.'</td>';
			$tabel .='</tr>';
			$total=$total + $r->nominal;
			$total_all=$total_all + $r->nominal;
		}
		if ($rows){
		$tabel .='<tr>';
		$tabel .='<td class="text-center" colspan="4"><strong>TOTAL DEPOSIT</strong></td>';
		$tabel .='<td class="text-right" ><strong>'.number_format($total).'</strong></td>';
		$tabel .='<td class="text-center" ></td>';
		$tabel .='</tr>';
		}
		$arr['detail'] = $tabel;
		// $arr['tot_tunai_ranap'] = $total_all;
		// $arr['nominal_bayar'] = $total;
		$this->output->set_output(json_encode($arr));
  }
  function load_retur(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        $userid = $this->session->userdata('user_id');
		
		$tanggal_trx=$this->input->post('tanggal_trx');
		
		$where='';
		
		
		
		if ('' != $tanggal_trx) {
            $where .= " AND DATE(B.tanggal_pencairan) = '".YMDFormat($tanggal_trx)."'";
        }
        $from = "(
					SELECT H.id,H.nopenerimaan_asal as nopenerimaan,H.nopengembalian,H.jenis_retur,H.tanggal_pengembalian,H.distributor,H.nominal_retur,H.nominal_ganti
					,(H.nominal_retur-H.nominal_ganti) as nomianl_trx,B.nominal_bayar as nominal,H.`status`,H.idpengembalian,H.st_trx
					FROM tretur_penerimaan H 
					LEFT JOIN tretur_penerimaan_pembayaran B ON B.idretur=H.id
					WHERE H.`status`='2' AND H.status_penerimaan = ('1') ".$where."

				) as tbl";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nopenerimaan','nopengembalian','distributor');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

			
        foreach ($list as $r) {
            $no++;
            $row = array();
			$disabel_btn='';
			
			// if ($r->umur_posting>$batas_batal){
				// $disabel_btn='disabled';
			// }
            $aksi       = '<div class="btn-group">';
           
          
            $row[] = $r->id;
            $row[] = $r->nopenerimaan;
            $row[] = $r->nopengembalian;
            $row[] = tipe_pemesanan_kbo(1,$r->jenis_retur);
            $row[] = HumanDateShort($r->tanggal_pengembalian);
            $row[] = $r->distributor;
            $row[] = number_format($r->nominal,2);           
					$url_pengembalian        = site_url('tgudang_pengembalian/');
					$url_retur        = site_url('tretur_terima/');
					$url_batal       = site_url('tgudang_pengembalian/');
					$aksi .= '<a href="'.$url_pengembalian.'detail/'.$r->idpengembalian.'" target="_blank" title="History Transaksi" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
					$aksi .= '<a href="'.$url_retur.'detail/'.$r->id.'" target="_blank" title="Transaksi" class="btn btn-xs btn-info"><i class="fa fa-list-ol"></i></a>';
					$aksi .= '<a href="'.$url_retur.'bayar/'.$r->id.'/disabled" target="_blank" title="Pembayaran" class="btn btn-xs btn-default"><i class="fa fa-credit-card"></i></a>';
					$aksi .= '<button title="Print" disabled class="btn btn-xs btn-success"><i class="fa fa-print"></i></button>';
					$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
	function load_hutang(){
		$data_user=get_acces();
		$user_acces_form=$data_user['user_acces_form'];
		
        $userid = $this->session->userdata('user_id');
		
		$tanggal_trx=$this->input->post('tanggal_trx');
		
		$where='';
		
		
		
		if ('' != $tanggal_trx) {
            $where .= " AND DATE(B.tanggal_pencairan) = '".YMDFormat($tanggal_trx)."'";
        }
        $from = "(
					SELECT H.id,H.notransaksi,H.nama_pengelolaan,H.deskripsi_pengelolaan,H.tanggal_hitung,B.nominal_bayar as nominal FROM `tpengelolaan` H
					LEFT JOIN tpengelolaan_bayar_terima B ON B.tpengelolaan_id=H.id
					WHERE H.st_verifikasi='1' AND H.total_terima > 0 ".$where."

				) as tbl";
				
		
		// print_r($from);exit();
        $this->from             = $from;
        $this->join             = array();
        $this->where            = array();
        $this->where_in         = array();
        $this->order            = array();
        $this->group            = array();
        $this->column_search    = array('nopenerimaan','nopengembalian','distributor');
        $this->column_order     = array();
        $this->select           = array();
		$this->load->library('datatables'); 
        $list = $this->datatable->get_datatables();
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

			
        foreach ($list as $r) {
            $no++;
            $row = array();
			$disabel_btn='';
			
			// if ($r->umur_posting>$batas_batal){
				// $disabel_btn='disabled';
			// }
            $aksi       = '<div class="btn-group">';
           
          
            $row[] = $r->id;
            $row[] = $r->notransaksi;
            $row[] = $r->nama_pengelolaan;
            $row[] = $r->deskripsi_pengelolaan;
            $row[] = HumanDateShort($r->tanggal_hitung);
            $row[] = number_format($r->nominal,2);           
					$aksi .= '<a href="'.site_url().'tpengelolaan/update/'.$r->id.'/disabled" target="_blank" data-toggle="tooltip" title="Lihat" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
					$aksi .= '<a href="'.site_url().'tpengelolaan/proses_terima/'.$r->id.'/disabled"  target="_blank" data-toggle="tooltip" title="Terima Pembayaran" class="btn btn-default btn-xs"><i class="fa fa-credit-card"></i></a>';
					$aksi.='</div>';
            $row[] = $aksi;
            $data[] = $row;
        }

        $draw = null;
        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_all(),
            "data" => $data
        );
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
  
}
